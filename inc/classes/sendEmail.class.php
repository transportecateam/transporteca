<?php
ini_set('memory_limit', '-1');

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) ); 
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH_CLASSES__ . "/booking.class.php");
require_once( __APP_PATH_CLASSES__ . "/customerLogs.class.php");
 
class cSendEmail extends cDatabase
{
    var $pdfFileNameAry = array();
    var $emailToAry = array();
    var $szFriendlyName;
    var $szDescription;
    var $szDescriptionHTML ;
    var $t_base="management/Error/";

    function __construct()
    {
        parent::__construct();
        return true;
    } 
    
    function createEmailTemplate($data)
    {
        if(!empty($data))
        {
            $specialTemplateAry = array('__CUSTOMER_CANCEL_BOOKING_EMAIL__','__CUSTOMER_CANCEL_NOT_CONFIRMED_BOOKING_EMAIL__','__BOOKING_CONFIRMATION_EMAIL__','__CUSTOMER_BOOKING_ACKNOWLEDGEMENT_EMAIL__','__UPDATE_CONTACT_INFORMATION__','__USER_INVITATION__','__INVITATION_TO_CONNECT_ON_TRANSPORTECA__','__CREATE_USER_ACCOUNT__','__SERVICE_NOTIFICATION_EMAIL__', '__RATE_FORWARDER__','__SHARE_THIS_SITE__','__BOOKING_BY_BANK_TRANSFER_CONFIRMATION_EMAIL__','__BANK_TRANSFER_RECEIVED_CONFIRMATION_EMAIL__');
            $iBookingLanguage = getLanguageId();
            if($data['iBookingLanguage']>0)
            {
                $iBookingLanguage = $data['iBookingLanguage'] ;
            }
            else if($data['iLanguage']>0)
            {
                $iBookingLanguage = $data['iLanguage'] ;
            }
            
            $query = "
                SELECT
                    cmm.section_description, 
                    cmm.subject,
                    ce.iUserType
                FROM
                    ".__DBC_SCHEMATA_CMS_EMAIL__." AS ce
                INNER JOIN
                    ".__DBC_SCHEMATA_CMS_MAPPING__." AS cmm
                ON        
                    cmm.idEmailTemplate=ce.id        
                WHERE
                   ce.section_title = '".mysql_escape_custom($data['szEmailTemplate'])."'
                AND
                    cmm.idLanguage='".(int)$iBookingLanguage."'                
            "; 
            $emailMessageAry = array();
            if ($result = $this->exeSQL($query))
            {
                if ($this->iNumRows > 0)
                {
                    $row = $this->getAssoc($result);
                    $iUserType = $row['iUserType'] ;  
                     
                    if(mb_detect_encoding($row['section_description'], "UTF-8", true) == "UTF-8")
                    {
                        $message = utf8_decode($row['section_description']); 
                    }
                    else
                    {
                        $message = $row['section_description']; 
                    }
                    if(!empty($row['subject']))
                    {
                        if(mb_detect_encoding($row['subject'], "UTF-8", true) == "UTF-8")
                        {
                            $subject = utf8_decode($row['subject']);
                        }
                        else
                        {
                            $subject = $row['subject'];
                        }
                    } 
                    
                    $emailMessageAry['szEmailBody'] = $message;
                    $emailMessageAry['szEmailSubject'] = $subject;
                    
                    $BookingFileName= __APP_PATH_ROOT__."/logs/EmailTemplateLog".date('Ymd').".log";
                    $strdata=array();
                    $strdata[0]=" \n\n ***Email Fetch Time ".date("Y-m-d H:i:s")."\n\n"; 
                    $strdata[1]="section_title: ".$data['szEmailTemplate']."\n" ;
                    $strdata[2]="Language: ".$iBookingLanguage."\n" ;
                    $strdata[3]=print_r($emailMessageAry,true)."\n";
                    file_log(true, $strdata, $BookingFileName); 
                    return $emailMessageAry;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "input", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    
    function sendEmail($mailBasicDetails)
    {
        if(!empty($mailBasicDetails))
        { 
            if($mailBasicDetails['bAttachments'] && $mailBasicDetails['idBooking'])
            {
                /*
                *  This is an exception case where we receive this request
                */
                $attachmentAry = array();
                $attachmentAry = $this->getBookingAttachments($mailBasicDetails['idBooking']);
                $mailBasicDetails['szAttachmentFiles'] = $attachmentAry['szAttachmentFiles'];
                $mailBasicDetails['szAttachmentFileName'] = $attachmentAry['szAttachmentFileName'];
            } 
            
            if(!empty($mailBasicDetails['szAttachmentFiles']))
            {
                if(is_array($attachmentAry))
                {
                    foreach($mailBasicDetails['szAttachmentFiles'] as $szAttachmentFiles)
                    {
                        $szFileBaseName = basename($szAttachmentFiles); 
                        $szAttachmentFullPath = __APP_PATH_EMAIL_ATTACHMENT__."/".$szFileBaseName; 

                        if(file_exists($szAttachmentFullPath))
                        {
                            /*
                            * If is already available in our system's attachments folder then do nothing
                            */ 
                            //@TO DO 
                        } 
                        else
                        {
                            /*
                            * If file is not exists in our attachments folder then we copy the file to our attachment folder.
                            */
                            if(file_exists($szAttachmentFiles))
                            {
                                @copy($szAttachmentFiles, $szAttachmentFullPath);
                            }
                        }
                    }
                } 
                else
                {
                    $szAttachmentFiles = $mailBasicDetails['szAttachmentFiles'];
                    $szFileBaseName = basename($szAttachmentFiles); 
                    $szAttachmentFullPath = __APP_PATH_EMAIL_ATTACHMENT__."/".$szFileBaseName; 

                    if(file_exists($szAttachmentFullPath))
                    {
                        /*
                        * If is already available in our system's attachments folder then do nothing
                        */ 
                        //@TO DO 
                    } 
                    else
                    {
                        /*
                        * If file is not exists in our attachments folder then we copy the file to our attachment folder.
                        */
                        if(file_exists($szAttachmentFiles))
                        {
                            @copy($szAttachmentFiles, $szAttachmentFullPath);
                        }
                    }
                }
            }
            
            $iBookingLanguage = getLanguageId();
            if($mailBasicDetails['iBookingLanguage']>0)
            {
                $iBookingLanguage = $mailBasicDetails['iBookingLanguage'];
            }
            
            $kConfig = new cConfig();
            $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$iBookingLanguage); 
            $szReference = $configLangArr[1]['szReference']; 
             
            $szEmailBodyTrailing = '';
            if(!empty($mailBasicDetails['szBookingRef']))
            { 
                $szBookingReference = $mailBasicDetails['szBookingRef'];
            }
            else if($mailBasicDetails['idBooking']>0)
            {
                $kBooking = new cBooking();
                $postSearchAry = array();
                
                $idBooking = $mailBasicDetails['idBooking'];
                $postSearchAry = $kBooking->getBookingDetails($idBooking);
                if(!empty($postSearchAry['szBookingRef']))
                { 
                    $szBookingReference = $postSearchAry['szBookingRef'];
                } 
                else if($postSearchAry['idFile']>0)
                {
                    $kBooking = new cBooking();
                    $kBooking->loadFile($postSearchAry['idFile']);
                    if(!empty($kBooking->szFileRef))
                    {
                        $szBookingReference = $kBooking->szFileRef;
                    }
                }
            }  
            
            $szEmailMessage = $mailBasicDetails['szEmailMessage'] ;
            if(!empty($szBookingReference))
            {
                //$szEmailMessage = $this->removeReferenceText($szEmailMessage);
                $szEmailBodyTrailing = " <br /><br />".$szReference.": ".$szBookingReference."";
            }
            
            
            /*
            * Setting Request Variables for Sendgrid
            */
            $this->setEmailTo($mailBasicDetails['szEmailTo']);
            $this->setPdfFileName($mailBasicDetails['szAttachmentFiles']); 
            require_once(__APP_PATH__.'/inc/SendGrid_loader.php');
            
            $sendgrid = new SendGrid(__SEND_GRID_USERNAME__,__SEND_GRID_PASSWARD__);
            $smtp = $sendgrid->smtp;
            
            $emailToAry = $this->getEmailTo();
            $attachableFilesAry = $this->getPdfFileName();
           
            $szEmailKey = md5($to."_".time()."_".mt_rand(0,9999));
             
            if(!empty($szEmailBodyTrailing))
            {
                $szEmailMessage .= $szEmailBodyTrailing;
            }  
            
            //$mailBasicDetails['szFromUserName'] = "Morten Lærkholm";
            
            $to_ctr = 0;
            $customerEmailToAry = array();
            $additionalCCAry = array();
            
            if(count($emailToAry)>1)
            {
                foreach($emailToAry as $emailToArys)
                {
                    if($to_ctr==0)
                    {
                        $customerEmailToAry[] = $emailToArys;
                    }
                    else
                    { 
                        $additionalCCAry[] = $emailToArys;
                    }
                    $to_ctr++;
                }
                if(!empty($mailBasicDetails['szCCEmailAddress']) && !empty($additionalCCAry))
                {
                    $mailBasicDetails['szCCEmailAddress'] = array_merge($additionalCCAry, $mailBasicDetails['szCCEmailAddress']);
                }
                else if(!empty($additionalCCAry))
                {
                    $mailBasicDetails['szCCEmailAddress'] = $additionalCCAry;
                }
            }
            else
            {
                $customerEmailToAry = $emailToAry;
            } 
             
            if(!empty($mailBasicDetails['szCCEmailAddress']))
            {
                $szCCEmailAddress = implode(",",$mailBasicDetails['szCCEmailAddress']);
            }
            if(!empty($mailBasicDetails['szBCCEmailAddress']))
            {
                $szBCCEmailAddress = implode(",",$mailBasicDetails['szBCCEmailAddress']);
            }
            if(__ENVIRONMENT__ == "DEV_LIVE")
            {
                $mailBasicDetails['szEmailSubject'] = "TEST SITE: ".$mailBasicDetails['szEmailSubject'];  
            }
            $emailLogsAry = array();
            $emailLogsAry['szFromEmail'] = $mailBasicDetails['szEmailFrom'];
            $emailLogsAry['szEmailBody'] = $szEmailMessage ;
            $emailLogsAry['szEmailSubject'] = $mailBasicDetails['szEmailSubject'] ;
            $emailLogsAry['idUser'] = $mailBasicDetails['idUser'] ;
            $emailLogsAry['idBooking'] = $mailBasicDetails['idBooking'] ;
            $emailLogsAry['szEmailKey'] = $szEmailKey ;
            $emailLogsAry['iQuoteEmail'] = $mailBasicDetails['iQuoteEmail'];
            $emailLogsAry['iMode'] = $mailBasicDetails['szUserLevel'];
            $emailLogsAry['iIgnoreTracking'] = $mailBasicDetails['iIgnoreTracking'];
            $emailLogsAry['szCCEmailAddress'] = $szCCEmailAddress;
            $emailLogsAry['szBCCEmailAddress'] = $szBCCEmailAddress;
             
            $attachmentFileNameAry = array();
            if(!empty($mailBasicDetails['szAttachmentFileName']))
            {
                if(is_array($mailBasicDetails['szAttachmentFileName']))
                {
                    $attachmentFileNameAry = $mailBasicDetails['szAttachmentFileName'];
                }
                else
                {
                    $attachmentFileNameAry[] = $mailBasicDetails['szAttachmentFileName'];
                }
            } 
            
            if(!empty($customerEmailToAry))
            {
                foreach($customerEmailToAry as $emailToArys)
                { 
                    $emailLogsAry['szToAddress'] = $emailToArys ;
                    if(!empty($attachableFilesAry))
                    {
                        $emailLogsAry['szAttachmentFiles'] = serialize($attachableFilesAry);
                        $emailLogsAry['iHasAttachments'] = 1;
                        $emailLogsAry['szAttachmentFileName'] = serialize($attachmentFileNameAry);
                    }
                    
                    if($mailBasicDetails['iDonotLogEmail']==1)
                    {
                        //This means we don't log email to tblemaillogs
                        //@TO DO
                    }
                    else
                    {
                        $idEmailLog = $this->logEmails($emailLogsAry);  
                    }
                    
                    try
                    {  
                        $message1 = new SendGrid\Mail();  
                        $message1->addTo($emailToArys);
                        $message1->setFrom($mailBasicDetails['szEmailFrom']);
                        if(!empty($mailBasicDetails['szFromUserName']))
                        {
                            $message1->setFromName($mailBasicDetails['szFromUserName']);
                        } 
                        $message1->setSubject($mailBasicDetails['szEmailSubject']);
                        $message1->setHtml($szEmailMessage); 
                        $message1->addUniqueArgument('email_key',$szEmailKey);

                        if(!empty($attachableFilesAry))
                        {
                            $message1->setAttachments($attachableFilesAry);
                        } 
                        if(!empty($mailBasicDetails['szEmailCC']))
                        {
                            //$message1->setCc($mailBasicDetails['szEmailCC']); 
                        }
                        if(!empty($mailBasicDetails['szCCEmailAddress']))
                        {
                            $message1->setCcs($mailBasicDetails['szCCEmailAddress']); 
                        }
                        if(!empty($mailBasicDetails['szBCCEmailAddress']))
                        {
                            $message1->setBccs($mailBasicDetails['szBCCEmailAddress']); 
                        }
                        //__ENVIRONMENT__ == "DEV_LIVE" || 
                        //
                        if(__ENVIRONMENT__ == "LIVE" || __ENVIRONMENT__ == "AWS_STAGGING")
                        {
                            $smtp->send($message1);
                        } 
                    } 
                    catch (Exception $ex) 
                    {
                        //$szMessage = print_r($ex,true);
                        $szExceptionMessage = " System registered exception in sending this email. \n\n ";
                        $szExceptionMessage .= 'Exception Message: ' .$ex->getMessage();
                        $szExceptionMessage .= 'File: ' .$ex->getFile();
                        $szExceptionMessage .= 'Line: ' .$ex->getLine();
                        $szExceptionMessage .= 'Trace: ' .$ex->getTraceAsString();
                    } 
                    
                    $updateEmailLog = array();
                    $updateEmailLog['iSuccess'] = 1;
                    $updateEmailLog['idEmailLog'] = $idEmailLog;
                    if(!empty($szExceptionMessage))
                    {
                        $updateEmailLog['szSendgridResponseLogs'] = $szExceptionMessage;
                    }
                    else
                    {
                        $updateEmailLog['szSendgridResponseLogs'] = print_R($message1,true); 
                    }
                    if($mailBasicDetails['iDonotLogEmail']==1)
                    {
                        //This means we don't log email to tblemaillogs
                        //@TO DO
                    }
                    else
                    {
                        $this->updateEmailLogs($updateEmailLog);
                    } 
                }
            } 
        }
    } 
    
    function setEmailTo($szEmailTo)
    {
        if(!empty($szEmailTo))
        {
            if(is_array($szEmailTo))
            {
                $this->emailToAry = $szEmailTo;
            }
            else
            {
                $this->emailToAry[] = $szEmailTo;
            } 
        } 
    } 
    function getEmailTo()
    {
        return $this->emailToAry;
    } 
    function setPdfFileName($szFileName)
    {
        if(!empty($szFileName))
        {
            if(is_array($szFileName))
            {
                $this->pdfFileNameAry = $szFileName;
            }
            else
            {
                $this->pdfFileNameAry[] = $szFileName;
            }
        } 
    }
    
    function getPdfFileName()
    {
        return $this->pdfFileNameAry;
    }

    function logEmails($emailLogsAry)
    {
        if(!empty($emailLogsAry))
        {    
            $iAlreadyUtf8Encoded=0; 
            if((int)$emailLogsAry['iGmailFlag']==1)
            {
                if(mb_detect_encoding($emailLogsAry['szEmailBody']) == "UTF-8")
                {
                   $iAlreadyUtf8Encoded=1;
                   $szEmailBody = ($emailLogsAry['szEmailBody']);
                }          
                else
                {
                    $szEmailBody = utf8_encode($emailLogsAry['szEmailBody']);
                }
            }
            else
            {
                $szEmailBody = utf8_encode($emailLogsAry['szEmailBody']);
                $kBookingNew = new cBooking();
                $emailLogsAry['dtSent'] = $kBookingNew->getRealNow(); 
            }
            
            if(empty($emailLogsAry['dtSent']))
            {
                $kBookingNew = new cBooking();
                $emailLogsAry['dtSent'] = $kBookingNew->getRealNow(); 
            } 
             
            $query = "
                INSERT INTO
                    ".__DBC_SCHEMATA_EMAIL_LOG__."
                (
                    idUser,
                    iMode,
                    szEmailBody,
                    szEmailSubject,
                    szToAddress,
                    szFromEmail,
                    idBooking,
                    szEmailKey,
                    dtSent,
                    iSuccess,
                    iQuoteEmail,
                    szAttachmentFiles,
                    iHasAttachments,
                    szAttachmentFileName,
                    iIgnoreTracking,
                    iIncomingEmail,
                    szCCEmailAddress,
                    szBCCEmailAddress,
                    iCRMMail,
                    szFromUserName,
                    szToUserName,
                    iEmailNumber,
                    iAlreadyUtf8Encoded,
                    szDraftEmailBody,
                    szDraftEmailSubject,
                    iDSTDifference,
                    iInvalidHtmlTags
                )
                VALUES
                (
                    ".(int)$emailLogsAry['idUser'].",
                    ".(int)$emailLogsAry['iMode'].",
                    '".mysql_escape_custom($szEmailBody)."',
                    '".mysql_escape_custom(utf8_encode($emailLogsAry['szEmailSubject']))."',
                    '".mysql_escape_custom($emailLogsAry['szToAddress'])."',
                    '".mysql_escape_custom($emailLogsAry['szFromEmail'])."', 
                    '".mysql_escape_custom($emailLogsAry['idBooking'])."', 
                    '".mysql_escape_custom($emailLogsAry['szEmailKey'])."',  
                    '".mysql_escape_custom($emailLogsAry['dtSent'])."',
                    ".(int)$success.",
                    '".mysql_escape_custom($emailLogsAry['iQuoteEmail'])."',
                    '".mysql_escape_custom($emailLogsAry['szAttachmentFiles'])."',
                    '".mysql_escape_custom($emailLogsAry['iHasAttachments'])."',
                    '".mysql_escape_custom($emailLogsAry['szAttachmentFileName'])."',
                    '".mysql_escape_custom($emailLogsAry['iIgnoreTracking'])."',
                    '".mysql_escape_custom($emailLogsAry['iIncomingEmail'])."',
                    '".mysql_escape_custom($emailLogsAry['szCCEmailAddress'])."',
                    '".mysql_escape_custom($emailLogsAry['szBCCEmailAddress'])."',
                    '".mysql_escape_custom($emailLogsAry['iCRMMail'])."',
                    '".mysql_escape_custom($emailLogsAry['szFromUserName'])."',
                    '".mysql_escape_custom($emailLogsAry['szToUserName'])."',
                    '".mysql_escape_custom($emailLogsAry['iEmailNumber'])."',
                    '".(int)$iAlreadyUtf8Encoded."',
                    '',
                    '',
                    '".mysql_escape_custom($emailLogsAry['iDSTDifference'])."',
                    '".mysql_escape_custom($emailLogsAry['iInvalidHtmlTags'])."' 
                )
            "; 
            
           //echo "<br>".$query;
//           die;
            if ($this->exeSQL($query))
            {
                $idEmailLog = $this->iLastInsertID;
                
                /*
                * Copy email data to customer log snippet
                */
                $kCustomerLogs = new cCustomerLogs();
                $emailLogsAry = array();
                $emailLogsAry = $this->getEmailLogDetails($idEmailLog);
                $kCustomerLogs->addEmailDataToCustomerLogSnippet($emailLogsAry); 
                return $idEmailLog;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "input", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    
    function updateEmailLogs($data)
    { 
        if(!empty($data))
        {  
            $query = "
                UPDATE
                    ".__DBC_SCHEMATA_EMAIL_LOG__."
                SET 
                    iSuccess = '".mysql_escape_custom($data['iSuccess'])."',
                    szSendgridResponseLogs = '".mysql_escape_custom($data['szSendgridResponseLogs'])."',
                    dtSent = now()
                WHERE
                    id = '".$data['idEmailLog']."'
            ";
            //echo $query;
            if ($this->exeSQL($query))
            { 
                return true;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "input", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }  
        }
    }
    
    function getEmailLogDetails($idEmailLog)
    {	 
        if($idEmailLog>0)
        {	 
            /*
             * id,
                idBooking,
                idUser, 
                szToAddress,
                szEmailSubject,
                szEmailBody,
                szFromEmail,
                dtSent,
                szEmailKey,
                szEmailStatus,
                iSuccess,
                dtEmailStatusUpdated,
                iIncomingEmail,
                iEmailNumber,
                iSnoozeEmail,
                dtSnoozeDate,
                iClosed,
                szCCEmailAddress,
                szBCCEmailAddress,
                iHasAttachments,
                szAttachmentFiles,
                szAttachmentFileName,
                szToUserName,
                szFromUserName,
                iSeen,
                iAlreadyUtf8Encoded,
                szDraftEmailBody,
                szDraftEmailSubject,
                iInvalidHtmlTags,
                iDSTDifference
             */
            $query="
                SELECT
                    *
                FROM
                    ".__DBC_SCHEMATA_EMAIL_LOG__." b 
                WHERE  
                    id = '".(int)$idEmailLog."'  
                ORDER BY 
                    dtSent DESC 
            "; 
            //echo $query;
            if($result = $this->exeSQL($query))
            {
                if($this->getRowCnt()>0)
                { 
                    $row = array();
                    $row = $this->getAssoc($result); 
                    return $row; 
                }
                else
                {
                    return array() ;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return array();
            }
        }
        else
        {
            return false;
        }
    }
    
    function getBookingAttachments($idBooking)
    {  
        if($idBooking>0)
        {
            $kBooking = new cBooking(); 
            $postSearchAry = $kBooking->getBookingDetails($idBooking);

            $szBookingRef = $postSearchAry['szBookingRef'];
            $attachmentFilesAry = array();
            $attachmentFileNameAry = array();
            $ctr=0; 
            $iSendInsuranceInvoice = 0;
            if($postSearchAry['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_3__ && $postSearchAry['iTransferConfirmed']==1)
            {
                $invoice_filename = "bookingInvoice_".$szBookingRef.".pdf";  
                if($postSearchAry['iFinancialVersion']==2)
                {
                    $invoice_pdf_file = getInvoiceConfirmationPdfFileHTML_v2($idBooking,false,true);
                }
                else
                {
                    $invoice_pdf_file = getInvoiceConfirmationPdfFileHTML($idBooking,false,true);
                }
                $attachmentFilesAry[$ctr] = $invoice_pdf_file;
                $attachmentFileNameAry[$ctr] = 'BOOKING_INVOICE';
                $ctr++;
                /*
                if($postSearchAry['iInsuranceIncluded']==1)
                {
                    $insurance_invoice_filename = "bookingInsuranceInvoice_".$szBookingRef.".pdf";    
                    $insurance_invoice_filename = getBookingInsuranceInvoicePdf($idBooking,false,true);
                    $iSendInsuranceInvoice=1;
                    $attachmentFilesAry[$ctr] = $insurance_invoice_filename;
                    $attachmentFileNameAry[$ctr] = 'BOOKING_INSURANCE_INVOICE';
                    $ctr++;
                }
                 * 
                 */
            }
            else if($postSearchAry['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_3__)
            {
                $invoice_filename = "bookingConfirmation_".$szBookingRef.".pdf"; 
                $invoice_pdf_file = getBookingConfirmationPdfFileHTML($idBooking,false,true) ;
                $attachmentFilesAry[$ctr] = $invoice_pdf_file;
                $attachmentFileNameAry[$ctr] = 'BOOKING_CONFIRMATION';
                $ctr++;
            }
            else
            {
                $invoice_filename = "bookingInvoice_".$szBookingRef.".pdf";    	
                if($postSearchAry['iFinancialVersion']==2)
                {
                    $invoice_pdf_file = getInvoiceConfirmationPdfFileHTML_v2($idBooking,false,true); 
                }
                else
                {
                    $invoice_pdf_file = getInvoiceConfirmationPdfFileHTML($idBooking,false,true); 
                }

                $attachmentFilesAry[$ctr] = $invoice_pdf_file;
                $attachmentFileNameAry[$ctr] = 'BOOKING_INVOICE';
                $ctr++;

                $confirmation_filename = "bookingConfirmation_".$szBookingRef.".pdf";    
                $confirmation_pdf_file = getBookingConfirmationPdfFileHTML($idBooking,false,true) ;

                $attachmentFilesAry[$ctr] = $confirmation_pdf_file;
                $attachmentFileNameAry[$ctr] = 'BOOKING_CONFIRMATION';
                
                $ctr++;
/*
                if($postSearchAry['iInsuranceIncluded']==1)
                {
                    $insurance_invoice_filename = "bookingInsuranceInvoice_".$szBookingRef.".pdf";    
                    $insurance_invoice_filename = getBookingInsuranceInvoicePdf($idBooking,false,true);
                    $iSendInsuranceInvoice=1;

                    $attachmentFilesAry[$ctr] = $insurance_invoice_filename;
                    $attachmentFileNameAry[$ctr] = 'BOOKING_INSURANCE_INVOICE';
                    $ctr++;
                }
 * 
 */
            }   
            $final_ret_ary = array();
            $final_ret_ary['szAttachmentFiles'] = $attachmentFilesAry;
            $final_ret_ary['szAttachmentFileName'] = $attachmentFileNameAry;
            return $final_ret_ary;
        } 
    }
    
    function deleteCrmEmail($data)
    {
        if(!empty($data))
        {
            $this->set_szCrmEmail(sanitize_all_html_input($data['szCrmEmail']));
            $this->set_idCrmEmail(sanitize_all_html_input($data['idCrmEmail']));
            
            if($this->error === true)
            {
                return false;
            }
            $idBookingFile = $data['idBookingFile'];
            $emailLogsAry = array();
            $emailLogsAry = $this->getEmailLogDetails($this->idCrmEmail);
             
            $kBooking = new cBooking(); 
            $kBooking->loadFile($idBookingFile);
            $idBooking = $kBooking->idBooking;
            $idUser = $kBooking->idUser; 
            $iNoReferenceFound = $kBooking->iNoReferenceFound;
            $szPreviousTask = $kBooking->szPreviousTask;
            $szCurrentTask = $kBooking->szTaskStatus;
            $bookingDetailsAry = array();
            if($idBooking>0)
            {
                $bookingDetailsAry = $kBooking->getBookingDetails($idBooking);
                $szBookingRef = $bookingDetailsAry['szBookingRef']; 
            }
            
            if($bookingDetailsAry['iBookingLanguage']>0)
            {
                $iBookingLanguage = $bookingDetailsAry['iBookingLanguage'];
            }
            else
            {
                $iBookingLanguage = getLanguageId();
            }
            $kQuote = new cQuote();
            $emailSenderDetailsAry = array();
            $emailSenderDetailsAry = $kQuote->getReplyEmailText($this->idCrmEmail,$iBookingLanguage,true);
             
            $kConfig = new cConfig();
            $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$iBookingLanguage);
                
            $szFromText = $configLangArr[1]['szFromText_pt']; 
            $szToText = $configLangArr[1]['szToText_pt']; 
            $szSent = $configLangArr[1]['szSent']; 
            $szSubject = $configLangArr[1]['szSubject']; 
            
            /*
            * In case of incoming emails we have swapped from and to addresses while adding to pending tray so same swapping will be applied here
            */ 
            $szToEmailAddressStr = $emailLogsAry['szFromEmail'];
            
            $szForwardedEmailHeader = $szFromText.": ".$emailSenderDetailsAry['szCustomerEmail'].PHP_EOL;
            $szForwardedEmailHeader .= $szSent.": ".$emailSenderDetailsAry['szCrmEmailDate']." ".$emailSenderDetailsAry['szCrmEmailTime'].PHP_EOL;
            $szForwardedEmailHeader .= $szToText.": ".$szToEmailAddressStr.PHP_EOL;
            $szForwardedEmailHeader .= $szSubject.": ".$emailLogsAry['szEmailSubject'].PHP_EOL;
               
            $this->iRefreshPendingTrays = false;
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_EMAIL_LOG__."
                SET
                    isDeleted = 1,
                    iSeen = '1'
                WHERE
                    id = '".mysql_escape_custom($this->idCrmEmail)."'	
            "; 
            if($result = $this->exeSQL( $query ))
            {
                $kCrm = new cCRM();
                $iNumUnseenEmails = $kCrm->getUnreadEmailsByBookingID($idBooking,true); 
                $this->iRefreshPendingTrays = 1;
                if($iNumUnseenEmails>0)
                { 
                    /*
                    * If a file has more than 1 unread message then we reassign next CRM email task to the file
                    */
                    $kCrm->updateReadMesageTask($idBookingFile,true);
                }
                else
                {
                    $kBookingNew = new cBooking();
                    $dtResponseTime = $kBookingNew->getRealNow(); 

                    $fileLogsAry = array(); 
                    if($szCurrentTask=='T210')
                    {
                        $fileLogsAry['szTransportecaTask'] = $szPreviousTask; 
                    }
                    $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime; 

                    if(!empty($fileLogsAry))
                    {
                        $file_log_query = '';
                        foreach($fileLogsAry as $key=>$value)
                        {
                            $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                        } 
                        $file_log_query = rtrim($file_log_query,","); 
                        $kBookingNew->updateBookingFileDetails($file_log_query,$idBookingFile); 
                    }
                }
                
                if(!empty($this->szCrmEmail))
                {   
                    $idAdmin = $_SESSION['admin_id'] ; 
                    $kAdmin = new cAdmin();
                    $kAdmin->getAdminDetails($idAdmin);
                    $szFromEmail = $kAdmin->szEmail;
                    $szFromUserName = $kAdmin->szFirstName." ".$kAdmin->szLastName ;
                        
                    if(empty($szFromEmail))
                    {
                        $szFromEmail = __STORE_SUPPORT_EMAIL__;
                    }
                    if(empty($szFromUserName))
                    {
                        $szFromUserName = __STORE_SUPPORT_NAME__;
                    }
                    
                    $szAttachmentFileAry = array();
                    $attachmentFilesAry = array();
                    $attachmentFileNameAry = array();
                    $attachment = false;
                    if(!empty($emailLogsAry['szAttachmentFiles']) && $emailLogsAry['iIncomingEmail']==1)
                    {
                        $attachment = false;
                        $szAttachmentFileAry = unserialize($emailLogsAry['szAttachmentFiles']);
                       
                        if(!empty($szAttachmentFileAry))
                        { 
                            foreach($szAttachmentFileAry as $szAttachmentFileArys)
                            {
                                $szAttachmetFolderPath = __APP_PATH_GMAIL_ATTACHMENT__."/".$emailLogsAry['iEmailNumber']."/".$szAttachmentFileArys;

                                $szNewName = md5(time()."_".mt_rand(0,99999))."_".$szAttachmentFileArys;

                                $szFilePath = __APP_PATH__."/attachments/".$szNewName;
                                if(file_exists($szAttachmetFolderPath))
                                { 
                                    if(file_exists($szFilePath))
                                    {
                                        @unlink($szFilePath);
                                    } 
                                    if(copy($szAttachmetFolderPath,$szFilePath))
                                    {
                                        $attachmentFilesAry[$ctr] = $szFilePath;
                                        $attachmentFileNameAry[$ctr] = $szNewName;
                                        $ctr++;
                                    }   
                                } 
                            }
                        }
                    }
                    if(mb_detect_encoding($emailLogsAry['szEmailBody']) == "UTF-8" && (int)$emailLogsAry['iAlreadyUtf8Encoded']==0)
                    {
                        $szEmailBody = $szForwardedEmailHeader.PHP_EOL.trim(utf8_decode($emailLogsAry['szEmailBody']));
                    }
                    else
                    {
                        $szEmailBody = $szForwardedEmailHeader.PHP_EOL.trim($emailLogsAry['szEmailBody']);
                    }
                    
                    $kSendEmail = new cSendEmail();      
                    $mailBasicDetailsAry = array();
                    $mailBasicDetailsAry['szEmailTo'] = $this->szCrmEmail;
                    $mailBasicDetailsAry['szEmailFrom'] = $szFromEmail; 
                    $mailBasicDetailsAry['szEmailSubject'] = utf8_decode($emailLogsAry['szEmailSubject']);
                    $mailBasicDetailsAry['szEmailMessage'] = $szEmailBody; 
                    $mailBasicDetailsAry['idUser'] = $idUser;
                    $mailBasicDetailsAry['szBookingRef'] = $szBookingRef;
                    $mailBasicDetailsAry['idBooking'] = $idBooking;
                    $mailBasicDetailsAry['szUserLevel'] = $emailLogsAry['iMode']; 
                    $mailBasicDetailsAry['bBookingLogs'] = false;
                    $mailBasicDetailsAry['szFromUserName'] = $szFromUserName;
                    $mailBasicDetailsAry['bAttachments'] = $attachment; 
                    $mailBasicDetailsAry['szAttachmentFiles'] = $attachmentFilesAry;  
                    $mailBasicDetailsAry['szAttachmentFileName'] = $attachmentFileNameAry;  
                    $mailBasicDetailsAry['iQuoteEmail'] = $emailLogsAry['iQuoteEmail'];   
                    $mailBasicDetailsAry['iBookingLanguage'] = $iBookingLanguage; 
                    $mailBasicDetailsAry['iDonotLogEmail'] = 1;  
                    $kSendEmail->sendEmail($mailBasicDetailsAry); 
                }  
                return true;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }	
        }
    }
    
    function removeReferenceText($szEmailString,$szFromPage='SEND_EMAIL')
    { 
        if(!empty($szEmailString))
        {
            $szEmailString = trim($szEmailString);
            $szEmailString = str_replace("<br />", "<br>", $szEmailString);
            
            $emailWordAry = array();
            $emailWordAry = preg_split('/[\s]+/', $szEmailString, -1, PREG_SPLIT_NO_EMPTY); 

            $iDebugFlag = false;
            $idBooking = 0;
            $kBooking = new cBooking(); 
            $replaceAry = array();
            $iCheckForReference = 0;
            $this->iReferenceTextExists = false;
            $kConfig = new cConfig();
            $referenceValuesAry = $kConfig->getLanguageConfigValuesByFieldName('szReference','__SITE_STANDARD_TEXT__'); 
               
            if($iDebugFlag)
            {
                $filename = __APP_PATH_ROOT__."/logs/reply_email_breakups_".date('Ymd').".log";
                $strdata=array();
                $strdata[0]=" \n\n Original HTML BODY : ".$szEmailString;   
                file_log(true, $strdata, $filename);
            } 
                    
            $counter = 0;
            if(!empty($emailWordAry))
            {
                foreach ($emailWordAry as $emailWordArys)
                {
                    $szOriginalWord = $emailWordArys;
                    $emailWordArys = strip_tags($emailWordArys);
                    $emailWordArys = trim($emailWordArys); 
                    
                    if($iDebugFlag)
                    {
                        $strdata=array();
                        $strdata[0]=" \n\n Original: ".$szOriginalWord; 
                        $strdata[1]=" \n Stripped: ".$emailWordArys; 
                        file_log(true, $strdata, $filename);
                    }
                    
                    if(!empty($referenceValuesAry) && in_array($emailWordArys,$referenceValuesAry))
                    {
                        $iCheckForReference = 1; 
                        $szNeedle = $emailWordArys;
                        
                        if($iDebugFlag)
                        {
                            $strdata=array();
                            $strdata[0]=" \n\n Reference text found: ".$emailWordArys;  
                            file_log(true, $strdata, $filename);
                        }
                    } 
                    else if($iCheckForReference==1 && $this->isValidReference($emailWordArys))
                    { 
                        $replaceAry[$counter] = $szNeedle." ".$emailWordArys;
                        $counter++;
                        $replaceAry[$counter] = $szNeedle."".$emailWordArys;
                        
                        $this->iReferenceTextExists = 1;
                        $iCheckForReference = 0; 
                        $szNeedle = '';
                        if($iDebugFlag)
                        {
                            $strdata=array();
                            $strdata[0]=" \n\n File found: ".$emailWordArys." Val: ".$szNeedle."".$emailWordArys;
                            file_log(true, $strdata, $filename);
                        } 
                        $counter++;
                    }
                    else
                    {
                        $iCheckForReference = 0; 
                        $szNeedle = '';
                    }
                } 
            } 
            if(!empty($replaceAry))
            {
                if($szFromPage=='SEND_EMAIL')
                {
                    $szEmailString = nl2br($szEmailString);
                } 
                $szEmailString = str_replace($replaceAry, "", $szEmailString);
            }
            return $szEmailString;
        }
    }
    
    function isValidReference($emailWordArys)
    {
        $idBooking = 0;
        $kBooking = new cBooking();
        if(strlen($emailWordArys)>=9 && strlen($emailWordArys)<=12)
        { 
            $szWordsInitials = substr($emailWordArys,0,4);
            $szTransportecaInitial = substr($emailWordArys,4,1); 
            if(is_numeric($szWordsInitials) && strtoupper($szTransportecaInitial)=='T')
            {
                $idBooking = $kBooking->getBookingIdByBookingRef($emailWordArys);
                if($idBooking<=0)
                {
                    $fileLogsAry = array();
                    $fileLogsAry = $kBooking->getBookingFileIdByFileRef($emailWordArys); 
                    if($fileLogsAry['idBooking']>0)
                    {
                        $idBooking = $fileLogsAry['idBooking']; 
                    }
                }
            }
        } 
        if($idBooking>0)
        {
            return true;
        }
        else
        {
            return false;
        }
        return $idBooking;
    } 
    
    function set_szCrmEmail ( $value,$bFlag=false )
    {
        $this->szCrmEmail = $this->validateInput($value , __VLD_CASE_EMAIL__ , "szCrmEmail" , t( $this->t_base."fields/email" ),false, 255 ,$bFlag );
    }
    function set_idCrmEmail ( $value,$bFlag=true )
    {
        $this->idCrmEmail = $this->validateInput($value , __VLD_CASE_NUMERIC__ , "idCrmEmail" , t( $this->t_base."fields/email" ),false, false ,$bFlag );
    }
}

?>