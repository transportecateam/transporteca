<?php
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );

require_once( __APP_PATH__ . "/includes/constants.php" );
require_once( __APP_PATH__ . "/includes/functions.php" );

Class cArtist extends cDatabase
{
	function __construct()
	{
            parent::__construct();

            //$borrowerSeesionAry = get_borrower_session_data();
            $this->idBorrowerSession = $borrowerSeesionAry['idBorrower'];
            $this->iBorrowerTypeSession = $borrowerSeesionAry['iUserType'];

            return true;
	}	

	/**
	 * This function is used to fetch all category from database.
	 * @access public
	 * @param string
	 * @return boolean 
	 * @author Ajay
	 */

	public function getAllArtistDetails($ret_count=false,$pageLimit=false,$paging=false,$searchAry=array())
	{	
		if($paging==2)
		{
                    $query_limit = " Limit 0,10 ";	
		}
		else if($paging==3)
		{
                    $query_limit = " Limit 0,10 ";	
		}
		else if($paging==4) // This is called from Index page for displaying Featured Booking
		{
                    $query_limit = " Limit 0,5 ";	
		}
		else if($paging==5) // This is called from All Featured Page Listing
		{
                    $query_limit = " Limit $pageLimit,".DJ_PER_PAGE ;	
		}
		else if($paging>0)
		{
                    $query_limit = " Limit $pageLimit,".ARTIST_PER_PAGE ;	
		}		
		
		if(!empty($searchAry))
		{
                    if($searchAry['iComplete']==2)
                    {
                        $searchAry['iComplete'] = 0;
                    }
                    if($searchAry['iAdminVerified']==2)
                    {
                        $searchAry['iAdminVerified'] = 0;
                    }
                    if($searchAry['iFeatured']==2)
                    {
                        $searchAry['iFeatured'] = 0;
                    }
                    if($searchAry['iAccredited']==2)
                    {
                        $searchAry['iAccredited'] = 0;
                    }
                    if($searchAry['iActive']==2)
                    {
                        $searchAry['iActive'] = 0;
                    }

                    if(!empty($searchAry['szArtistFullName']))
                    {
                        $query_and .=" AND (szFirstName LIKE :szFirstName OR szLastName LIKE :szLastName OR szArtistName LIKE :szArtistName )";
                    }			
                    if(!empty($searchAry['szKeyword']))
                    {
                        $query_and .=" AND (szFirstName LIKE :szFirstName OR szLastName LIKE :szLastName OR szArtistName LIKE :szArtistName )";
                    }	
                    if(!empty($searchAry['szEmail']))
                    {
                        $query_and .=" AND szEmail LIKE :szEmail ";
                    }
                    if(!empty($searchAry['szPostcode']))
                    {
                        $query_and .=" AND szPostcode LIKE :szPostcode ";
                    }

                    if($searchAry['idPrimaryArtType'])
                    {
                        $query_and .=" AND idPrimaryArtType = :idPrimaryArtType ";
                    }
                    if($searchAry['idSecondaryArtType'])
                    {
                        $query_and .=" AND idSecondaryArtType = :idSecondaryArtType ";
                    }

                    if(isset($searchAry['iFeatured']) && ($searchAry['iFeatured']!=10))
                    {
                        $query_and .=" AND iFeatured = '".(int)$searchAry['iFeatured']."'";
                    }
                    if(isset($searchAry['iAdminVerified']) && ($searchAry['iAdminVerified']!=10))
                    {
                        $query_and .=" AND iAdminVerified = '".(int)$searchAry['iAdminVerified']."' ";
                    }
                    if(isset($searchAry['iComplete']) && ($searchAry['iComplete']!=10))
                    {
                        $query_and .=" AND iComplete = '".(int)$searchAry['iComplete']."' ";
                    }
                    if(isset($searchAry['iAccredited']) && ($searchAry['iAccredited']!=10))
                    {
                        $query_and .=" AND iAccredited = '".(int)$searchAry['iAccredited']."' ";
                    }	
                    if(isset($searchAry['iActive']) && ($searchAry['iActive']!=10))
                    {
                        $query_and .=" AND iActive = '".(int)$searchAry['iActive']."' ";
                    }			

                    if($searchAry['fAvgRating']>0)
                    {
                        $query_and .=" AND fAvgRating >= '".(int)$searchAry['fAvgRating']."' ";
                    }
                    if(isset($searchAry['iMemberShipType']) && ($searchAry['iMemberShipType']!=10))
                    {
                        $query_and .=" AND iMemberShipType = '".(int)$searchAry['iMemberShipType']."' ";
                    } 
                    if($searchAry['iEquipment']==1)
                    {
                        $query_and .=" AND iEquipment = '1' ";
                    }

                    if($searchAry['dtRegistrationDateFrom'])
                    {
                        $query_and .=" AND DATE(dtCreatedOn) >= :dtRegistrationDateFrom ";
                    }
                    if($searchAry['dtRegistrationDateTo'])
                    {
                        $query_and .=" AND DATE(dtCreatedOn) <= :dtRegistrationDateTo ";
                    }	
                    if($searchAry['dtNextPaymentDate'])
                    {
                        $query_and .=" AND DATE(dtNextPaymentDate) <= :dtNextPaymentDate ";
                    }
                    if($searchAry['iMemberShipType_1']>0)
                    {
                        $query_and .=" AND iMemberShipType != '".$searchAry['iMemberShipType_1']."' ";
                    }  
		}
		
		if(!empty($searchAry['szSortFeild']) && !empty($searchAry['szSortOrder']))
		{
			$szSortFiled = sanitize_all_html_input(trim($searchAry['szSortFeild']));
			$szSortOrder = sanitize_all_html_input(trim($searchAry['szSortOrder']));
			
			if($szSortFiled=='idPrimaryArtType')
			{
				$query_order_by = " ORDER BY idPrimaryArtType ".$szSortOrder.", idSecondaryArtType ".$szSortOrder;
			}
			else if($szSortFiled=='szPhone')
			{
				$query_order_by = " ORDER BY szPhone ".$szSortOrder.", szAlternatePhone ".$szSortOrder;
			}
			else if($szSortFiled=='szAddress')
			{
				$query_order_by = " ORDER BY szAddress ".$szSortOrder.", szAddress2 ".$szSortOrder.", szCity ".$szSortOrder.", szPostcode ".$szSortOrder.", szCounty ".$szSortOrder;
			}
			else if($szSortFiled=='iNumBookings')
			{
				$query_order_by = " ORDER BY szArtistName ".$szSortOrder;
			}
			else if($szSortFiled=='BookingAmount' || $szSortFiled=='fBookingAmount')
			{
				$query_order_by = " ORDER BY fBaseFee ".$szSortOrder;
			}
			else if($szSortFiled=='Accredited')
			{
				$query_order_by = " ORDER BY iAccredited ".$szSortOrder;
			}
			else if($szSortFiled=='AvgRating')
			{
				$query_order_by = " ORDER BY fAvgRating ".$szSortOrder;
			}
			else
			{
				$query_order_by = " ORDER BY ".$szSortFiled." ".$szSortOrder;
			}
		}
		else
		{
			$query_order_by = " ORDER BY szArtistName ASC ";
		}
		
		if($paging==4) // This is called from Index page for displaying Featured Booking
		{
			$query_order_by = " ORDER BY rand() ";
		}
		if($searchAry['iGroupBy']==1)
		{
			$query_group_by = " GROUP BY DATE(dtCreatedOn) ";
		}
		if($ret_count)
		{
			$query_select = " COUNT(id) as iNumArtist,dtCreatedOn ";
		}
		else
		{
			$query_select  ="
					id,
					szFirstName,
					szLastName,
					szArtistName,
					dtDateOfBirth,
					szEmail,
					szPassword,
					szPhone,
					szAlternatePhone,
					szAddress,
					szAddress2,
					szPostcode,
					szCity,
					szCounty,
					szLatitute,
					szLongitute,
					iDistanceWillingToTravel,
					idPrimaryArtType,
					idSecondaryArtType,
					szDescription,
					szReferences,
					fBaseFee,
					fFeePerHour,
					szVideo1,
					szVideo2,
					szVideo3,
					szVideo4,
					szPhoto1,
					szPhoto2,
					szPhoto3,
					szPhoto4,
					szConfirmationKey,
					dtCreatedOn,
                    dtNextPaymentDate,
					iActive,					
					iAddedByAdmin,
					iFeatured,
					fTotalEarning,
					iAdminVerified,
					iComplete,
					iAccredited,
					iVerified,
					szArtistKey,
					dtVerificationLinkSent,
					iMemberShipType,
					iMaxPerformanceLenght,
					fAvgRating,
                    iMemberShipType,
                    szFeaturedQuote,
                    szIPAddress,
					(SELECT  at1.szArtName FROM ".__DBC_SCHEMATA_ART_TYPE__." at1 WHERE at1.id = idPrimaryArtType ) as szPrimaryArtType,
					(SELECT  at2.szArtName FROM ".__DBC_SCHEMATA_ART_TYPE__." at2 WHERE at2.id = idSecondaryArtType ) as szSecondaryArtType,
					(SELECT COUNT(b.id) FROM ".__DBC_SCHEMATA_BOOKING__." b WHERE b.idArtist = art.id AND b.iBookingStatus='".__BOOKING_STATUS_CONFIRMED__."' AND b.isDeleted='0') as iNumBookings
			";	
		}
		
		$query="
			SELECT
				$query_select
			FROM
				".__DBC_SCHEMATA_ARTIST__." art
			WHERE
				isDeleted = '0'
			  $query_and
			  $query_group_by
			  $query_order_by
			$query_limit
		";	
		//echo "<br>".$query."<br>";
		if($this->preapreStatement($query))
		{
                    if(!empty($searchAry['szArtistFullName']))
                    {
                        $this->bindSQL(':szFirstName',"%".$searchAry['szArtistFullName']."%",PDO::PARAM_STR);
                        $this->bindSQL(':szLastName',"%".$searchAry['szArtistFullName']."%",PDO::PARAM_STR);
                        $this->bindSQL(':szArtistName',"%".$searchAry['szArtistFullName']."%",PDO::PARAM_STR);
                    }	
                    if(!empty($searchAry['szKeyword']))
                    {
                        $this->bindSQL(':szFirstName',"%".$searchAry['szKeyword']."%",PDO::PARAM_STR);
                        $this->bindSQL(':szLastName',"%".$searchAry['szKeyword']."%",PDO::PARAM_STR);
                        $this->bindSQL(':szArtistName',"%".$searchAry['szKeyword']."%",PDO::PARAM_STR);
                    }	
                    if(!empty($searchAry['szEmail']))
                    {
                        $this->bindSQL(':szEmail',"%".$searchAry['szEmail']."%",PDO::PARAM_STR);
                    }
                    if(!empty($searchAry['szPostcode']))
                    {
                        $this->bindSQL(':szPostcode',"%".$searchAry['szPostcode']."%",PDO::PARAM_STR);
                    }
                    if($searchAry['idPrimaryArtType'])
                    {
                        $this->bindSQL(':idPrimaryArtType',(int)$searchAry['idPrimaryArtType'],PDO::PARAM_STR);
                    }
                    if($searchAry['idSecondaryArtType'])
                    {
                        $this->bindSQL(':idSecondaryArtType',(int)$searchAry['idSecondaryArtType'],PDO::PARAM_STR);
                    }
                    if($searchAry['dtRegistrationDateFrom'])
                    {
                        $this->bindSQL(':dtRegistrationDateFrom',$searchAry['dtRegistrationDateFrom'],PDO::PARAM_STR);
                    }
                    if($searchAry['dtRegistrationDateTo'])
                    {
                        $this->bindSQL(':dtRegistrationDateTo',$searchAry['dtRegistrationDateTo'],PDO::PARAM_STR);
                    }
                    if($searchAry['dtNextPaymentDate'])
                    {
                        $this->bindSQL(':dtNextPaymentDate',$searchAry['dtNextPaymentDate'],PDO::PARAM_STR);
                    } 
                    if(($result = $this->executeSQL()))
                    {
                        if($this->iNumRows>0)
                        {
                            if($searchAry['iGroupBy']==1)
                            {
                                $ret_ary = array();
                                $ctr = 0;
                                while($row = $this->getAssoc())
                                {
                                    if($searchAry['szView']=='WEEKLY')
                                    {
                                        $key = date('Ymd',strtotime($row['dtCreatedOn'])) ;
                                    }
                                    else
                                    {
                                        $key = date('Ym01',strtotime($row['dtCreatedOn'])) ;
                                    }

                                    $ret_ary[$key] = $row;
                                    $ctr++;
                                }
                                return $ret_ary ;
                            }
                            else if($ret_count)
                            {
                                $row = $this->getAssoc();
                                return $row['iNumArtist'];
                            }
                            else
                            {
                                $ret_ary = array();
                                $ctr = 0;
                                while($row = $this->getAssoc())
                                {
                                    $ret_ary[$ctr] = $row;
                                    $ctr++;
                                }
                                if($szSortFiled=='iNumBookings')
                                {
                                    $sortDesc=false;
                                    if($szSortOrder=='DESC')
                                    {
                                        $sortDesc = 'DESC';
                                    }

                                    $final_ret_ary = sortArray($ret_ary,'iNumBookings',$sortDesc);
                                    return $final_ret_ary;
                                }
                                else
                                {
                                    return $ret_ary;
                                }
                            }					
                        }
                        else
                        {
                                return false;
                        }
                    }
                    else
                    {
                        $this->error = true;
                        $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                        $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                        return false;
                    }
		}
	}
	 
	/**
	 * This function used to add Artists details from Admin section 
	 * @access public
	 * @param array
	 * @return boolean 
	 * @author Ajay
	 */	 
	function addArtistDetails($data,$iAdminFlag=1)
	{
		if(!empty($data))
		{
			$this->validateArtistDetails($data);
			
			if($this->error==true)
			{
				return false;
			}
			
			if($this->isUserNameExist($this->szEmail))
			{
				$this->addError('szEmail','Email already exits.');
				return false;
			}			
			$kBuyer = new cBuyer();
			if($kBuyer->isBuyerEmailExist($this->szEmail))
			{
				$this->addError('szEmail','Email already exits.');
				return false;
			}
			
			$this->szArtistKey = CleanTitle($this->szArtistName);			
			if($this->isUserNameExist(false,false,$this->szArtistKey))
			{
				$this->addError('szArtistName','DJ name already exits.');
				return false;
			}
			
			$dtArtistDOBDateTime = time() - strtotime($this->dtDateOfBirth);
			$iArtistAgeInYear = floor((float)$dtArtistDOBDateTime/(60*60*24*365));
			
			if($iArtistAgeInYear<16)
			{
				$this->addError('dtDateOfBirth','You must be at least 18 years old.');
				return false;
			}
            if($this->szPostcode !='')
            {
                $postcodeAry = array();
                $postcodeAry = getAddressDetailsByPostcode($this->szPostcode,true);

                if(!empty($postcodeAry['szLatitute']) && !empty($postcodeAry['szLongitute']))
                {
                    $this->szLatitute = $postcodeAry['szLatitute'];
                    $this->szLongitute = $postcodeAry['szLongitute'];
                }
                else
                {
                    $this->addError('szPostcode','This postcode not exists in UK');
                    return false;
                }
            }
			$this->szPassword = $this->generateUniqueToken(8);
						
			$szConfirmKey = md5($this->szEmail."_".date('YmdHis'));
			$this->szConfirmationKey = $szConfirmKey;
			
			$photo_counter = count($this->szUploadFileName);		
			if($photo_counter>0)
			{
				$kImageCropper = new cImageCrop();
				for($i=1;$i<=$photo_counter;$i++)
				{
					if(!empty($this->szUploadFileName[$i]))
					{
						$szTempFilePath = __APP_PATH_ARTIST_IMAGES_TEMP__."/".$this->szUploadFileName[$i] ;
						$szDestinationFilePath = __APP_PATH_ARTIST_IMAGES__."/".$this->szUploadFileName[$i] ;
						if(file_exists($szTempFilePath))
						{	
							if(file_exists($szDestinationFilePath))
							{
								@unlink($szDestinationFilePath);
							}
							if(copy($szTempFilePath, $szDestinationFilePath)) 
							{
								//$kImageCropper->crop_center($szTempFilePath, $szDestinationFilePath,$this->szUploadFileName[$i], 500, 500);
							}
							@unlink($szTempFilePath);
						}
						else
						{
							$this->szUploadFileName[$i] = "";						
						}
					}
				}
			}
			
			$video_counter = count($data['szVideo']);				
			if($video_counter>0)
			{
				for($i=1;$i<=$video_counter;$i++)
				{
					if(!empty($this->szVideo[$i]))
					{
						$szYouTubeVideoID = getYoutubeId($this->szVideo[$i],true);
						if(!empty($szYouTubeVideoID))
						{
							$szFileName = "youtube_video_".$szYouTubeVideoID.".jpg";
							$szFilePath = __APP_PATH_YOUTUBE_IMAGES__."/".$szFileName ;
							if(file_exists($szFilePath))
							{
								@unlink($szFilePath);
							}
							download_youtube_thumnail($szYouTubeVideoID);
						}
					}
				}
			}
		
			$kBooking = new cBooking();
			$this->szArtistNumber = $kBooking->generateBookingNumber(2);
			$iMaxNumBooking = $kBooking->iMaxNumBooking ;
			
			//$this->szArtistKey = md5($this->szEmail."_".$kBooking->szArtistNumber."_".mt_rand(0,1000));
			
			$dtSubscriptonStartDate = date('Y-m-d');
			$dtSubscriptonEndDate = date('Y-m-d',strtotime("+1 MONTH")); 
            if($this->iMemberShipType!='')
            {
                if($this->iMemberShipType!=__MEMBERSHIP_TYPE_BASIC__)
                {
                    $idMemberShipType = $kArtist->iMemberShipType;

                    $memberShipDetailsAry = array();
                    $memberShipDetailsAry = $kArtist_new->getAllMembershipDetails($idMemberShipType);
                    $this->szMembershipType = $memberShipDetailsAry[0]['szMemberShipType'] ;
                    $this->fMembershipCost = $memberShipDetailsAry[0]['fMembershipCost'] ;

                    $szRemarkText = " Type: ".$this->szMembershipType.", cost: ".$this->fMembershipCost." start date: ".$dtSubscriptonStartDate.",end date: ".$dtSubscriptonEndDate;
                }
                else
                {
                    $szRemarkText = " Type: BASIC, cost: 0.00 start date: ".$dtSubscriptonStartDate.",end date: ".$dtSubscriptonEndDate;
                }
            }
			$query="
				INSERT INTO
					".__DBC_SCHEMATA_ARTIST__."
				(
					szArtistKey,
					szArtistNumber,
					szFirstName,
					szLastName,
					szArtistName,
					dtDateOfBirth,
					szEmail,
					szPassword,
					szPhone,
					szAlternatePhone,
					szAddress,
					szAddress2,
					szPostcode,
					szCity,
					szCounty,
					szLatitute,
					szLongitute,
					iDistanceWillingToTravel,
					idPrimaryArtType,
					idSecondaryArtType,
					szDescription,
					szReferences,
					fBaseFee,
					fFeePerHour,
					szVideo1,
					szVideo2,
					szVideo3,
					szVideo4,
					szPhoto1,
					szPhoto2,
					szPhoto3,
					szPhoto4,				
					iYearStarted,
					iEquipment,
					szConfirmationKey,
					szReferer,
					szIPAddress,
					szUserAgent,
					dtCreatedOn,
					iActive,	
					iComplete,				
					iAddedByAdmin,
					iStep1Completed,
					iStep2Completed,
					iStep3Completed,
					iStep4Completed,
					iStep5Completed,
					dtVerificationLinkSent,
					iMaxPerformanceLenght
				)
				VALUES
				(
					:szArtistKey,
					:szArtistNumber,
					:szFirstName,
					:szLastName,
					:szArtistName,
					:dtDateOfBirth,
					:szEmail,
					:szPassword,
					:szPhone,
					:szAlternatePhone,
					:szAddress,
					:szAddress2,
					:szPostcode,
					:szCity,
					:szCounty,
					:szLatitute,
					:szLongitute,
					:iDistanceWillingToTravel,
					:idPrimaryArtType,
					:idSecondaryArtType,
					:szDescription,
					:szReferences,
					:fBaseFee,
					:fFeePerHour,
					:szVideo1,
					:szVideo2,
					:szVideo3,
					:szVideo4,
					:szPhoto1,
					:szPhoto2,
					:szPhoto3,
					:szPhoto4,			
					:iYearStarted,
					:iEquipment,
					:szConfirmationKey,
					:szReferer,
					:szIPAddress,
					:szUserAgent,
					now(),
					'1',	
					:iComplete,				
					'".(int)$iAdminFlag."',
					'1',	
					'1',
					'1',	
					'1',
					'1',
					now(),
					'".(int)$this->iMaxPerformanceLenght."'
				)
			";
			if($this->preapreStatement($query))
			{	
                $iComplete=true;
				$this->bindSQL(':szArtistKey',$this->szArtistKey,PDO::PARAM_STR);
				$this->bindSQL(':szArtistNumber',$this->szArtistNumber,PDO::PARAM_STR);
				$this->bindSQL(':szFirstName',$this->szFirstName,PDO::PARAM_STR);
				$this->bindSQL(':szLastName',$this->szLastName,PDO::PARAM_STR);				
				$this->bindSQL(':szArtistName',$this->szArtistName,PDO::PARAM_STR);
				$this->bindSQL(':dtDateOfBirth',$this->dtDateOfBirth,PDO::PARAM_STR);
				$this->bindSQL(':szEmail',$this->szEmail,PDO::PARAM_STR);
				$this->bindSQL(':szPassword',md5($this->szPassword),PDO::PARAM_STR);				
				$this->bindSQL(':szPhone',$this->szPhone,PDO::PARAM_STR);				
				$this->bindSQL(':szAlternatePhone',$this->szAlternatePhone,PDO::PARAM_STR);
				$this->bindSQL(':szAddress',$this->szAddress,PDO::PARAM_STR);
				$this->bindSQL(':szAddress2',$this->szAddress2,PDO::PARAM_STR);				
				$this->bindSQL(':szPostcode',$this->szPostcode,PDO::PARAM_STR);
				$this->bindSQL(':szCity',$this->szCity,PDO::PARAM_STR);				
				$this->bindSQL(':szCounty',$this->szCounty,PDO::PARAM_STR);				
				$this->bindSQL(':szLatitute',$this->szLatitute,PDO::PARAM_STR);				
				$this->bindSQL(':szLongitute',$this->szLongitute,PDO::PARAM_STR);				
				$this->bindSQL(':iDistanceWillingToTravel',$this->iDistanceWillingToTravel,PDO::PARAM_INT);
				$this->bindSQL(':idPrimaryArtType',$this->idPrimaryArtType,PDO::PARAM_INT);
				$this->bindSQL(':idSecondaryArtType',$this->idSecondaryArtType,PDO::PARAM_INT);
				$this->bindSQL(':szDescription',$this->szDescription,PDO::PARAM_STR);				
				$this->bindSQL(':szReferences',$this->szReferences,PDO::PARAM_STR);
				$this->bindSQL(':fBaseFee',$this->fBaseFee,PDO::PARAM_INT);
				$this->bindSQL(':fFeePerHour',$this->fFeePerHour,PDO::PARAM_INT);				
				$this->bindSQL(':szVideo1',$this->szVideo[1],PDO::PARAM_STR);				
				$this->bindSQL(':szVideo2',$this->szVideo[2],PDO::PARAM_STR);
				$this->bindSQL(':szVideo3',$this->szVideo[3],PDO::PARAM_STR);				
				$this->bindSQL(':szVideo4',$this->szVideo[4],PDO::PARAM_STR);
				$this->bindSQL(':szPhoto1',$this->szUploadFileName[1],PDO::PARAM_STR);				
				$this->bindSQL(':szPhoto2',$this->szUploadFileName[2],PDO::PARAM_STR);
				$this->bindSQL(':szPhoto3',$this->szUploadFileName[3],PDO::PARAM_STR);				
				$this->bindSQL(':szPhoto4',$this->szUploadFileName[4],PDO::PARAM_STR);
				$this->bindSQL(':szConfirmationKey',$this->szConfirmationKey,PDO::PARAM_STR);
				
				$this->bindSQL(':iYearStarted',$this->iYearStarted,PDO::PARAM_STR);
				$this->bindSQL(':iEquipment',$this->iEquipment,PDO::PARAM_INT);		
				$this->bindSQL(':szReferer',__HTTP_REFERER__,PDO::PARAM_STR);				
				$this->bindSQL(':szIPAddress',__REMOTE_ADDR__,PDO::PARAM_STR);
				$this->bindSQL(':szUserAgent',__HTTP_USER_AGENT__,PDO::PARAM_STR);
//				if($this->szFirstName && $this->szLastName && $this->szArtistName)
				if(($result = $this->executeSQL(true)))
				{				
					$idArtist = $this->iLastInsertID ;
					
					$this->idArtistLastAdded = $idArtist ;
					
					$this->addLastLocationPlayed($idArtist);
					$this->addArtistUnavailableDate($idArtist);					
					$this->addArtistCertification($idArtist);					
					$this->addArtistAwards($idArtist);
					$this->addArtistReferences($idArtist);
					
					if($this->iEquipment==1)
					{
						$this->addArtistEquipmentDetails($idArtist);
					}
					
					$bookingIdLogsAry = array();
					$bookingIdLogsAry['iMaxNumBooking'] = $iMaxNumBooking+1 ;
					$bookingIdLogsAry['szBookingNumber'] = $this->szArtistNumber;
					$bookingIdLogsAry['iType'] = 2 ;
					$kBooking->addBookingIDLogs($bookingIdLogsAry);
					
					/*
					* Adding Admin Activity logs
					*/
					$kAdmin = new cAdmin();
					$adminActivityLogsAry = array();
					$adminActivityLogsAry['szType'] = 'ARTIST';
					$adminActivityLogsAry['szActionType'] = 'iMemberShipType';
					$adminActivityLogsAry['iActionValue'] = $this->iMemberShipType;
					$adminActivityLogsAry['idPrimaryTable'] = $this->id;
					$adminActivityLogsAry['szRemarks'] = 'Admin has added membership type of DJ account with following values: '.$szRemarkText;
					$kAdmin->addAdminActivityLogs($adminActivityLogsAry);
					
					$replace_arr = array();
					$szLinkValue=__URL_BASE__."/?action=confirmEmail&key=".$this->szConfirmationKey;							
					
					$replace_arr['szLink']="<a href='".$szLinkValue."'>Click here to verify account </a>";
					$replace_arr['szHttpsLink'] = $szLinkValue ;
					$replace_arr['szEmail'] = $this->szEmail;
					$replace_arr['szSiteUrl'] = __URL_BASE__ ;	
					$replace_arr['szUserName'] = $this->szEmail;
					$replace_arr['szPassword'] = $this->szPassword;
					$replace_arr['szSiteName'] = SITE_NAME ;				

					if($iAdminFlag==1)
					{
						createEmail('__CREATE_ARTIST_ACCOUNT_BY_ADMIN__',$replace_arr,$this->szEmail,__CREATE_USER_ACCOUNT_SUBJECT__,__STORE_SUPPORT_EMAIL__);	
					}
					else
					{
						createEmail('__CREATE_ARTIST_ACCOUNT__',$replace_arr,$this->szEmail,__CREATE_USER_ACCOUNT_SUBJECT__,__STORE_SUPPORT_EMAIL__);
					}
					
					return true;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	/**
	 * This function used to update Artists details from Admin section 
	 * @access public
	 * @param array
	 * @return boolean 
	 * @author Ajay
	 */
	 
	public function updateArtistDetails($data,$idArtist)
	{
		if(!empty($data) && (int)$idArtist>0)
		{
			$kArtist_new = new cArtist();
			$kArtist_new->loadTyni($idArtist);
			$iMemberShipType = $kArtist_new->iMemberShipType ;
			if($kArtist_new->id<=0)
			{
				$this->addError('szFirstName','DJ you are trying to update is not exists in our database.');
				return false;
			}
			$this->validateArtistDetails($data,$kArtist_new);
            
            if($this->error==true)
			{
				return false;
			}
			
			if($this->isUserNameExist($this->szEmail,$idArtist))
			{
				$this->addError('szEmail','Email already exits.');
				return false;
			}
			
			$kBuyer = new cBuyer();
			if($kBuyer->isBuyerEmailExist($this->szEmail))
			{
				$this->addError('szEmail','Another user has already registered with this email.');
				return false;
			}
			
			$this->szArtistKey = CleanTitle($this->szArtistName);			
			if($this->isUserNameExist(false,$idArtist,$this->szArtistKey))
			{
				$this->addError('szArtistName','DJ name already exits.');
				return false;
			}
			
			$dtArtistDOBDateTime = time() - strtotime($this->dtDateOfBirth);
			$iArtistAgeInYear = floor((float)$dtArtistDOBDateTime/(60*60*24*365));
			
			if($iArtistAgeInYear<16)
			{
				$this->addError('dtDateOfBirth','You must be at least 18 years old.');
				return false;
			}
			
            if($this->szPostcode!='')
            {
                $postcodeAry = array();
                $postcodeAry = getAddressDetailsByPostcode($this->szPostcode,true);

                if(!empty($postcodeAry['szLatitute']) && !empty($postcodeAry['szLongitute']))
                {
                    $this->szLatitute = $postcodeAry['szLatitute'];
                    $this->szLongitute = $postcodeAry['szLongitute'];
                }
                else
                {
                    $this->addError('szPostcode','This postcode not exists in UK');
                    return false;
                }
            }
			$photo_counter = count($this->szUploadFileName);
			
			if($photo_counter>0)
			{
				$kImageCropper = new cImageCrop();
				for($i=1;$i<=$photo_counter;$i++)
				{
					if(!empty($this->szUploadFileName[$i]) && $this->iFileUpload[$i]==1)
					{
						$szTempFilePath = __APP_PATH_ARTIST_IMAGES_TEMP__."/".$this->szUploadFileName[$i] ;
						$szDestinationFilePath = __APP_PATH_ARTIST_IMAGES__."/".$this->szUploadFileName[$i] ;
						if(file_exists($szTempFilePath))
						{	
							if(file_exists($szDestinationFilePath))
							{
								@unlink($szDestinationFilePath);
							}
							if(copy($szTempFilePath, $szDestinationFilePath)) 
							{
								//$kImageCropper->crop_center($szTempFilePath, $szDestinationFilePath,$this->szUploadFileName[$i], 500, 500);
							}
							@unlink($szTempFilePath);
						}
						else
						{
							$this->szUploadFileName[$i] = "";						
						}
					}
				}
			}
			
			$video_counter = count($data['szVideo']);				
			if($video_counter>0)
			{
				for($i=1;$i<=$video_counter;$i++)
				{
					if(!empty($this->szVideo[$i]))
					{
						$szYouTubeVideoID = display_youtube_video_for_slider($this->szVideo[$i],true);
						if(!empty($szYouTubeVideoID))
						{
							$szFileName = "youtube_video_".$szYouTubeVideoID.".jpg";
							$szFilePath = __APP_PATH_YOUTUBE_IMAGES__."/".$szFileName ;
							if(file_exists($szFilePath))
							{
								@unlink($szFilePath);
							} 
							download_youtube_thumnail($szYouTubeVideoID); 
						}
					}
				}
			}
			
			$isMemberShipTypeChanged = false;
			if($iMemberShipType!=$this->iMemberShipType)
			{
				$isMemberShipTypeChanged = true ;
			}
			
           
            
                if($isMemberShipTypeChanged)
                {				
                    $this->dtSubscriptionStartingDate = date('Y-m-d');
                    $this->dtNextPaymentDate = date('Y-m-d',strtotime("+1 MONTH")); 
                    if($this->iMemberShipType!='')
                    {
                        if($this->iMemberShipType!=__MEMBERSHIP_TYPE_BASIC__)
                        {
                            $idMemberShipType = $this->iMemberShipType;

                            $memberShipDetailsAry = array();
                            $memberShipDetailsAry = $kArtist_new->getAllMembershipDetails($idMemberShipType);

                            $this->szMembershipType = $memberShipDetailsAry[0]['szMemberShipType'] ;
                            $this->fMembershipCost = $memberShipDetailsAry[0]['fMemberShipCost'] ;

                            $szRemarkText = " Type: ".$this->szMembershipType.", cost: ".$this->fMembershipCost." start date: ".$this->dtSubscriptionStartingDate.",end date: ".$this->dtNextPaymentDate;
                        }
                        else
                        { 
                            $this->szMembershipType = 'BASIC';
                            $this->fMembershipCost = '0';

                            $szRemarkText = " Type: BASIC, cost: 0.00 start date: ".$this->dtSubscriptionStartingDate.",end date: ".$this->dtNextPaymentDate;
                        } 
                    }        
                    $query_update = " iMemberShipType = :iMemberShipType, fMembershipCost = :fMembershipCost, dtSubscriptionStartingDate = :dtSubscriptionStartingDate, dtNextPaymentDate = :dtNextPaymentDate, "; 
                
                }
			$updateszFeaturedQuote='';
            if($kArtist_new->iFeatured=='1')
            {
                $updateszFeaturedQuote="szFeaturedQuote = :szFeaturedQuote,";
            }
            
			$query="
				UPDATE
					".__DBC_SCHEMATA_ARTIST__."
				SET
					szFirstName = :szFirstName,
					szLastName = :szLastName,
					szArtistName = :szArtistName,
					dtDateOfBirth = :dtDateOfBirth,
					szEmail = :szEmail,
					szPhone = :szPhone,
					szAlternatePhone = :szAlternatePhone,
					szAddress = :szAddress,
					szAddress2 = :szAddress2,
					szPostcode = :szPostcode,
					szCity = :szCity,
					szCounty = :szCounty,
					szLatitute = :szLatitute,
					szLongitute = :szLongitute,
					iDistanceWillingToTravel = :iDistanceWillingToTravel,
					idPrimaryArtType = :idPrimaryArtType,
					idSecondaryArtType = :idSecondaryArtType,
					szDescription = :szDescription,
					szReferences = :szReferences,
					fBaseFee = :fBaseFee,
					fFeePerHour = :fFeePerHour,
					szVideo1 = :szVideo1,
					szVideo2 = :szVideo2,
					szVideo3 = :szVideo3,
					szVideo4 = :szVideo4,
					szPhoto1 = :szPhoto1,
					szPhoto2 = :szPhoto2,
					szPhoto3 = :szPhoto3,
					szPhoto4 = :szPhoto4,				
					iYearStarted = :iYearStarted,
					iEquipment = :iEquipment,
					iComplete = 1,
					iStep1Completed = 1,
					iStep2Completed = 1,
					iStep3Completed = 1,
					iStep4Completed = 1,
					iStep5Completed = 1,
					iMaxPerformanceLenght = '".(int)$this->iMaxPerformanceLenght."',
					$query_update
                    $updateszFeaturedQuote    
					dtUpdatedOn = now()
				WHERE
					id = :idArtist
				AND
					isDeleted = '0'
			";
//			echo "query: ".$query ;
//            print_r($this);
//            die();
			if($this->preapreStatement($query))
			{	
				$this->bindSQL(':szFirstName',$this->szFirstName,PDO::PARAM_STR);
				$this->bindSQL(':szLastName',$this->szLastName,PDO::PARAM_STR);				
				$this->bindSQL(':szArtistName',$this->szArtistName,PDO::PARAM_STR);
				$this->bindSQL(':dtDateOfBirth',$this->dtDateOfBirth,PDO::PARAM_STR);
				$this->bindSQL(':szEmail',$this->szEmail,PDO::PARAM_STR);
				
				$this->bindSQL(':szPhone',$this->szPhone,PDO::PARAM_STR);				
				$this->bindSQL(':szAlternatePhone',$this->szAlternatePhone,PDO::PARAM_STR);
				$this->bindSQL(':szAddress',$this->szAddress,PDO::PARAM_STR);
				$this->bindSQL(':szAddress2',$this->szAddress2,PDO::PARAM_STR);				
				$this->bindSQL(':szPostcode',$this->szPostcode,PDO::PARAM_STR);
				$this->bindSQL(':szCity',$this->szCity,PDO::PARAM_STR);				
				$this->bindSQL(':szCounty',$this->szCounty,PDO::PARAM_STR);				
				$this->bindSQL(':szLatitute',$this->szLatitute,PDO::PARAM_STR);				
				$this->bindSQL(':szLongitute',$this->szLongitute,PDO::PARAM_STR);				
				$this->bindSQL(':iDistanceWillingToTravel',$this->iDistanceWillingToTravel,PDO::PARAM_INT);
				$this->bindSQL(':idPrimaryArtType',$this->idPrimaryArtType,PDO::PARAM_INT);
				$this->bindSQL(':idSecondaryArtType',$this->idSecondaryArtType,PDO::PARAM_INT);
				$this->bindSQL(':szDescription',$this->szDescription,PDO::PARAM_STR);				
				$this->bindSQL(':szReferences',$this->szReferences,PDO::PARAM_STR);
				$this->bindSQL(':fBaseFee',$this->fBaseFee,PDO::PARAM_INT);
				$this->bindSQL(':fFeePerHour',$this->fFeePerHour,PDO::PARAM_INT);				
				$this->bindSQL(':szVideo1',$this->szVideo[1],PDO::PARAM_STR);				
				$this->bindSQL(':szVideo2',$this->szVideo[2],PDO::PARAM_STR);
				$this->bindSQL(':szVideo3',$this->szVideo[3],PDO::PARAM_STR);				
				$this->bindSQL(':szVideo4',$this->szVideo[4],PDO::PARAM_STR);
				$this->bindSQL(':szPhoto1',$this->szUploadFileName[1],PDO::PARAM_STR);				
				$this->bindSQL(':szPhoto2',$this->szUploadFileName[2],PDO::PARAM_STR);
				$this->bindSQL(':szPhoto3',$this->szUploadFileName[3],PDO::PARAM_STR);				
				$this->bindSQL(':szPhoto4',$this->szUploadFileName[4],PDO::PARAM_STR);	
				$this->bindSQL(':iEquipment',$this->iEquipment,PDO::PARAM_INT);		
											
				$this->bindSQL(':iYearStarted',$this->iYearStarted,PDO::PARAM_STR);				
				$this->bindSQL(':idArtist',(int)$idArtist,PDO::PARAM_INT);
				if($isMemberShipTypeChanged)
				{ 
					$this->bindSQL(':iMemberShipType',$this->iMemberShipType,PDO::PARAM_INT);
					$this->bindSQL(':fMembershipCost',$this->fMembershipCost,PDO::PARAM_STR);	
					$this->bindSQL(':dtSubscriptionStartingDate',$this->dtSubscriptionStartingDate,PDO::PARAM_STR);	
					$this->bindSQL(':dtNextPaymentDate',$this->dtNextPaymentDate,PDO::PARAM_STR);		
				}
                if($kArtist_new->iFeatured=='1')
                {
                   $this->bindSQL(':szFeaturedQuote',$this->szFeaturedQuote,PDO::PARAM_STR);		
                }
				if(($result = $this->executeSQL()))
				{		
					if($isMemberShipTypeChanged)
					{
						/*
						* Adding Admin Activity logs
						*/
						$kAdmin = new cAdmin();
						$adminActivityLogsAry = array();
						$adminActivityLogsAry['szType'] = 'ARTIST';
						$adminActivityLogsAry['szActionType'] = 'iMemberShipType';
						$adminActivityLogsAry['iActionValue'] = $this->iMemberShipType;
						$adminActivityLogsAry['idPrimaryTable'] = $idArtist;
						$adminActivityLogsAry['szRemarks'] = 'Admin has updated membership type of DJ account with following values: '.$szRemarkText;
						$kAdmin->addAdminActivityLogs($adminActivityLogsAry);
					}
					$this->addArtistUnavailableDate($idArtist);	
					$this->addLastLocationPlayed($idArtist);	
					$this->addArtistCertification($idArtist);	
					$this->addArtistAwards($idArtist);
					$this->addArtistReferences($idArtist,true);
					if($this->iEquipment==1)
					{
						$this->addArtistEquipmentDetails($idArtist);
					}
					else
					{
						$this->deleteEquipmentDetails($idArtist);
					}
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
			
		}
	}
	
	public function validateArtistDetails($data,$kArtist_new)
	{
		if(!empty($data))
		{
			if(!empty($data['dtDateOfBirth']))
			{
				$data['dtDateOfBirth'] = format_date_time($data['dtDateOfBirth']);
			} 
			$this->set_szFirstName(sanitize_all_html_input($data['szFirstName']),false);
			$this->set_szLastName(sanitize_all_html_input($data['szLastName']),false);
			$this->set_szArtistName(sanitize_all_html_input($data['szArtistName']));
			$this->set_szEmail(sanitize_all_html_input($data['szEmail']));
			$this->set_idPrimaryArtType(sanitize_all_html_input($data['idPrimaryArtType']),false);
			$this->set_idSecondaryArtType(sanitize_all_html_input($data['idSecondaryArtType']));
			$this->set_szPhone(sanitize_all_html_input($data['szPhone']),false);
			$this->set_szAlternatePhone(sanitize_all_html_input($data['szAlternatePhone']),false);
			$this->set_dtDateOfBirth(sanitize_all_html_input($data['dtDateOfBirth']),false);
			$this->set_iDistanceWillingToTravel(sanitize_all_html_input($data['iDistanceWillingToTravel']),false);			
			$this->set_szDescription(sanitize_all_html_input($data['szDescription']),false);		
			$this->set_szReferences(sanitize_all_html_input($data['szReferences']));
			$this->set_szPostcode(sanitize_all_html_input($data['szPostcode']),false);				
			$this->set_szAddress(sanitize_all_html_input($data['szAddress']),false);		
			$this->set_szAddress2(sanitize_all_html_input($data['szAddress2']));
			$this->set_szCity(sanitize_all_html_input($data['szCity']),false);		
			$this->set_szCounty(sanitize_all_html_input($data['szCounty']),false);					
			$this->set_fBaseFee(sanitize_all_html_input($data['fBaseFee']),false);		
			$this->set_fFeePerHour(sanitize_all_html_input($data['fFeePerHour']),false);		
			$this->set_iYearStarted(sanitize_all_html_input($data['iYearStarted']),false);			
			$this->set_iEquipment(sanitize_all_html_input($data['iEquipment']),false);		
			$this->set_iMaxPerformanceLenght(sanitize_all_html_input($data['iMaxPerformanceLenght']),false);					
			$this->set_iMemberShipType(sanitize_all_html_input($data['iMemberShipType']),false);				
            if($kArtist_new->iFeatured=='1')
            {
                $this->set_szFeaturedQuote(sanitize_all_html_input($data['szFeaturedQuote']),false);					
            }		
			for($i=1;$i<=3;$i++)
			{				
				$flag_cert = false;
				$flag_award = false;
				if(!empty($data['szCertification'][$i]) || !empty($data['dtCertificateExpiry'][$i]))
				{
					$flag_cert = true;
				}
				
				if(!empty($data['szAwardTitle'][$i]) || !empty($data['dtAwardDate'][$i]))
				{
					$flag_award = true;
				}
				
				if(!empty($data['dtAwardDate'][$i]))
				{
					$data['dtAwardDate'][$i] = format_date_time($data['dtAwardDate'][$i]);
				}
				if(!empty($data['dtCertificateExpiry'][$i]))
				{
					$data['dtCertificateExpiry'][$i] = format_date_time($data['dtCertificateExpiry'][$i]);
				}
				
				$this->set_szCertification(sanitize_all_html_input($data['szCertification'][$i]),$i,$flag_cert);		
				$this->set_dtCertificateExpiry(sanitize_all_html_input($data['dtCertificateExpiry'][$i]),$i,$flag_cert);
				$this->set_idCertificate(sanitize_all_html_input($data['idCertificate'][$i]),$i,$flag_cert);

				$this->set_szAwardTitle(sanitize_all_html_input($data['szAwardTitle'][$i]),$i,$flag_award);	
				$this->set_dtAwardDate(sanitize_all_html_input($data['dtAwardDate'][$i]),$i,$flag_award);	
				$this->set_idAward(sanitize_all_html_input($data['idAward'][$i]),$i,$flag_award);
			}
			
			if($this->iEquipment==1)
			{			
				$this->set_szSpeakerDescription(sanitize_all_html_input($data['szSpeakerDescription']));
				$this->set_szDeckDescription(sanitize_all_html_input($data['szDeckDescription']));
				$this->set_szOtherDescription(sanitize_all_html_input($data['szOtherDescription']));
				$this->set_fEquipmentCharge(sanitize_all_html_input($data['fEquipmentCharge']));
				$this->set_iPATCertificate(sanitize_all_html_input($data['iPATCertificate']));
				
				if($this->iPATCertificate==1)
				{
					if(!empty($data['dtPATExpiryDate']))
					{
						$data['dtPATExpiryDate'] = format_date_time($data['dtPATExpiryDate']);
					}				
					$this->set_dtPATExpiryDate(sanitize_all_html_input($data['dtPATExpiryDate']));
				}
				
				$equipmentPhoto = 1 ;
				for($i=1;$i<4;$i++)
				{
					$this->set_szUploadEquipmentPhoto(trim(sanitize_all_html_input($data['szUploadEquipmentPhoto'][$i])),$i);
					$this->set_iEquipentFileUpload(trim(sanitize_all_html_input($data['iEquipentFileUpload'][$i])),$i);	
					
					if(!empty($this->szUploadEquipmentPhoto[$i]))
					{
						$equipmentPhoto = 2;
					}
				}
				
				if($equipmentPhoto==1)
				{
					$this->addError('szEquipentPhoto_1','You must select at least 1 equipment photo');
				}
			}
			if($this->idPrimaryArtType>0 && ($this->idPrimaryArtType == $this->idSecondaryArtType))
			{
				$this->addError('idSecondaryArtType','You can select same Music Genre as Primary and secondary.');
			}
			
			$address_counter = count($data['szVenueName']);	
			if($address_counter>0)
			{
				for($i=0;$i<$address_counter;$i++)
				{
					/*
						These fields were in use
 
						$this->set_szVenuePhone(trim(sanitize_all_html_input($data['szVenuePhone'][$i])),$i);
						$this->set_szVAddress(trim(sanitize_all_html_input($data['szVAddress'][$i])),$i);
						$this->set_szVPostcode(trim(sanitize_all_html_input($data['szVPostcode'][$i])),$i);					
						$this->set_szVCity(trim(sanitize_all_html_input($data['szVCity'][$i])),$i);
						$this->set_szVenueAlternatePhone(trim(sanitize_all_html_input($data['szVenueAlternatePhone'][$i])),$i);
						$this->set_szVenueDescription(trim(sanitize_all_html_input($data['szVenueDescription'][$i])),$i);
					*/
					$flag=false;
					if(!empty($data['szVenueName'][$i]) || !empty($data['szVCounty'][$i]) || !empty($data['dtStartDate'][$i]))
					{
						$flag = true;
					}
					$this->set_szVenueName(trim(sanitize_all_html_input($data['szVenueName'][$i])),$i,$flag);					
					$this->set_szVCounty(trim(sanitize_all_html_input($data['szVCounty'][$i])),$i,$flag);
					$this->set_idLastLocationPlayed(trim(sanitize_all_html_input($data['idLastLocationPlayed'][$i])),$i);
					$this->set_iTopLocation(trim(sanitize_all_html_input($data['iTopLocation'][$i])),$i);
					
					if(!empty($data['dtStartDate'][$i]))
					{
						$data['dtStartDate'][$i] = format_date_time($data['dtStartDate'][$i]);
					}
					$this->set_dtStartDate(trim(sanitize_all_html_input($data['dtStartDate'][$i])),$i,$flag);	
				}
			}
			
			if(!empty($data['dtUnavailableFromDate']))
			{
				$dtUnavailableDateAry = explode(',',$data['dtUnavailableFromDate']) ;
			}
			
			if(!empty($dtUnavailableDateAry))
			{
				$ctr=1;
				foreach($dtUnavailableDateAry as $dtUnavailableDateArys)
				{
					$dtUnavailableDateArys_formated = format_date_time($dtUnavailableDateArys);
					
					$this->dtUnavailableFromDate[$ctr] = $dtUnavailableDateArys_formated ;
					$this->iStartHour[$ctr] = '00';
					$this->iStartMinute[$ctr] = '00';
					
					$this->dtUnavailableToDate[$ctr] = $dtUnavailableDateArys_formated ;
					$this->iEndHour[$ctr] = '23';
					$this->iEndMinute[$ctr] = '59';					
					$ctr++;
				}
			}
			/*
			$unavailable_counter = count($data['dtUnavailableFromDate']);	
			if($unavailable_counter>0)
			{
				for($i=1;$i<=$unavailable_counter;$i++)
				{	
					if(!empty($data['dtUnavailableFromDate'][$i]))
					{
						$data['dtUnavailableFromDate'][$i] = format_date_time($data['dtUnavailableFromDate'][$i]);
					}
					if(!empty($data['dtUnavailableToDate'][$i]))
					{
						$data['dtUnavailableToDate'][$i] = format_date_time($data['dtUnavailableToDate'][$i]);
					}
					
					$this->set_dtUnavailableFromDate(trim(sanitize_all_html_input($data['dtUnavailableFromDate'][$i])),$i);	
					$this->set_dtUnavailableToDate(trim(sanitize_all_html_input($data['dtUnavailableToDate'][$i])),$i);
					
					$this->set_iStartHour(trim(sanitize_all_html_input($data['iStartHour'][$i])),$i);	
					$this->set_iStartMinute(trim(sanitize_all_html_input($data['iStartMinute'][$i])),$i);					
					$this->set_iEndHour(trim(sanitize_all_html_input($data['iEndHour'][$i])),$i);	
					$this->set_iEndMinute(trim(sanitize_all_html_input($data['iEndMinute'][$i])),$i);
					
					$this->set_idUnavailableDates(trim(sanitize_all_html_input($data['idUnavailableDates'][$i])),$i);
					
				}
			}
*/
			$video_counter = count($data['szVideo']);
			$video_valid = 1;		
			if($video_counter>0)
			{
				for($i=1;$i<=$video_counter;$i++)
				{
					$this->set_szVideo(trim(sanitize_all_html_input($data['szVideo'][$i])),$i);
					
					if(!empty($this->szVideo[$i]))
					{
						$video_valid = 2;
					}
				}
			}
			
//			if($video_valid==1)
//			{
//				$this->addError('szVideo_1','You must add at least 1 youtube video url');
//			}
//			else
//			{
				$video_counter = count($data['szVideo']);		
				$validVideoFormat = array('http://www.youtube.com','//www.youtube.com','http://youtu.be/','https://www.youtube.com/watch?v=');
				$invalidPlaylistFormat="list=";
				if($video_counter>0)
				{	
					for($i=1;$i<=$video_counter;$i++)
					{
						if(!empty($this->szVideo[$i]))
						{
							$szSearchPattern1 = substr($this->szVideo[$i],0,22);
							$szSearchPattern2 = substr($this->szVideo[$i],0,17);
							$szSearchPattern3 = substr($this->szVideo[$i],0,16);
							$szSearchPattern4 = substr($this->szVideo[$i],0,32);
							
							if(in_array($szSearchPattern1,$validVideoFormat) || in_array($szSearchPattern2,$validVideoFormat) || in_array($szSearchPattern3,$validVideoFormat) || in_array($szSearchPattern4,$validVideoFormat))
							{
                                $posPlaylistURL = strpos($this->szVideo[$i], $invalidPlaylistFormat);
                                if(!empty($posPlaylistURL))
                                {
                                    $this->addError('szVideo_'.$i,"Youtube playlist link cannot be added");
                                }
								//valid vedio format
							}
							else
							{
								$this->addError('szVideo_'.$i,"Invalid Link, Youtube url must start with http://www.youtube.com, //www.youtube.com or http://youtu.be/");
							}
						}
					}
				}
//			}
			
			$photo_counter = count($data['szUploadFileName']);		
			if($photo_counter>0)
			{
				for($i=1;$i<=$photo_counter;$i++)
				{
					$photo_flag=false;
//					if($i<4)
//					{
//						$photo_flag = true;
//					}
					$this->set_szUploadFileName(trim(sanitize_all_html_input($data['szUploadFileName'][$i])),$i,$photo_flag);
					$this->set_iFileUpload(trim(sanitize_all_html_input($data['iFileUpload'][$i])),$i);
				}
			}
			
			if($this->error==true)
			{
				return false;
			}
			else
			{ 
				$address_counter = count($data['szVenueName']);	
				if($address_counter>0)
				{
					for($i=0;$i<=$address_counter;$i++)
					{
						if(!empty($this->dtStartDate[$i]))
						{
							$dtEventStarted = strtotime($this->dtStartDate[$i]);
							$todayTime = strtotime(date('Y-m-d'));
							
							if($dtEventStarted>=$todayTime)
							{
								$this->addError('dtStartDate_'.$i,'Event date must be less than today');
							}
						}
					}
				}				
			
				$iReferAdded = 1;
				for($i=1;$i<=3;$i++)
				{
					$flag_ref = false;
					if(!empty($data['szRefName'][$i]) || !empty($data['szRefEmail'][$i]) || !empty($data['szRefBussinessName'][$i]) || !empty($data['szRefPosition'][$i]))
					{
//						$flag_ref = true;
						$iReferAdded = 2;
					}
								
					$this->set_szRefName(sanitize_all_html_input($data['szRefName'][$i]),$i,$flag_ref);		
					$this->set_szRefEmail(sanitize_all_html_input($data['szRefEmail'][$i]),$i,$flag_ref);				
					$this->set_szRefBussinessName(sanitize_all_html_input($data['szRefBussinessName'][$i]),$i,$flag_ref);	
					$this->set_szRefPosition(sanitize_all_html_input($data['szRefPosition'][$i]),$i,$flag_ref);				
					$this->set_idReference(sanitize_all_html_input($data['idReference'][$i]),$i);
					
				}
//				if($iReferAdded==1)
//				{
//					$this->addError('szRefName_1','You must add atleast one reference');
//				}
				
				for($i=1;$i<=3;$i++)
				{
					if(!empty($this->dtCertificateExpiry[$i]) && ($this->dtCertificateExpiry[$i]!='0000-00-00 00:00:00'))
					{
						$dtCertificateExpiryTime = strtotime($this->dtCertificateExpiry[$i]);
						
						$todayTime = strtotime(date('Y-m-d'));
						
						if($todayTime>=$dtCertificateExpiryTime)
						{
							$this->addError('dtCertificateExpiry_'.$i,'Expiry date must be greater than or equal to today.');
							//return false;
						}
					}
					
					if(!empty($this->dtAwardDate[$i]) && ($this->dtAwardDate[$i]!='0000-00-00 00:00:00'))
					{
						$dtAwardDateTime = strtotime($this->dtAwardDate[$i]);
						
						$todayTime = strtotime(date('Y-m-d'));
						
						if($todayTime<=$dtAwardDateTime)
						{
							$this->addError('dtAwardDate_'.$i,'Award date must be less than or equal to today.');
							//return false;
						}
					}
				}
				
				if($this->iYearStarted>0 && ($this->iYearStarted>2014))
				{
					$this->addError('iYearStarted','Year started must be less than or equal to current year.');
					return false;
				}
				
				if($this->iPATCertificate==1)
				{
					$dtPATExpiryDateTime = strtotime($this->dtPATExpiryDate);						
					$todayTime = strtotime(date('Y-m-d'));	
									
					if($todayTime>=$dtPATExpiryDateTime)
					{
						$this->addError('dtPATExpiryDate','date must be greater or equal to today.');
						return false;
					}
				}
				return true;
			}
		}
	}
	
	/**
	 * This function checks if any artist with the given email is exists in databse or not.
	 * @access public
	 * @param string
	 * @return boolean 
	 * @author Ajay
	 */
	 
	public function isUserNameExist($szEmail=false,$idArtist=false,$szArtistKey=false,$szArtistName=false)
	{
		if(!empty($szEmail) || !empty($szArtistKey) || !empty($szArtistName))
		{
			if(!empty($szEmail))
			{
				$query_and = "AND szEmail = :szEmail " ;
			}
			else if(!empty($szArtistKey))
			{
				$query_and = "AND szArtistKey = :szArtistKey " ;
			}
			else if(!empty($szArtistName))
			{
				$query_and = "AND szArtistName = :szArtistName " ;
			}
			
			if($idArtist>0)
			{
				$query_and .= " AND id != :id ";
			}
			
			$query="
				SELECT
					id,
					szFirstName,
					iFacebook,
					iComplete
				FROM
					".__DBC_SCHEMATA_ARTIST__."
				WHERE
					isDeleted = '0' 
				$query_and
			";			
			//echo "<br>".$query;
			if($this->preapreStatement($query))
			{
				if(!empty($szEmail))
				{
					$this->bindSQL(':szEmail',$szEmail,PDO::PARAM_STR);
				}
				else if(!empty($szArtistKey))
				{
					$this->bindSQL(':szArtistKey',$szArtistKey,PDO::PARAM_STR);
				}
				else if(!empty($szArtistName))
				{
					$this->bindSQL(':szArtistName',$szArtistName,PDO::PARAM_STR);
				}
				
				if($idArtist>0)
				{
					$this->bindSQL(':id',(int)$idArtist,PDO::PARAM_STR);
				}
				
				if( ( $result = $this->executeSQL() ) )
				{
					if($this->iNumRows>0)
					{ 
						$this->set_szPdoMode(PDO::FETCH_ASSOC);
						$row = $this->getAssoc();
						$this->idLoadedArtist = $row['id'];
						$this->szArtistFirstName = $row['szFirstName'];
						$this->iFacebookSignedUp = $row['iFacebook'];
						$this->iProfileCompleted = $row['iComplete'];
						return true;
					}
					else
					{
						return false;
					}
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	/**
	 * This function checks if any location is already exists than it simply update existing record otherwise insert new record into database
	 * @access public
	 * @param string
	 * @return boolean 
	 * @author Ajay
	 */
	
	function addLastLocationPlayed($idArtist)
	{	
		$address_counter = count($this->szVenueName);
		
		if($address_counter>0)
		{	
			$ctr_ep = 0;
			$usedLocationIdAry = array();
			$query_emp = "";
			
			for($i=0;$i<$address_counter;$i++)
			{	
				if(!empty($this->szVenueName[$i]))
				{
					$update_flag = false;
					$added_flag = false;
					if(((int)$this->idLastLocationPlayed[$i]>0) && ($this->isLastLocationIdExist($this->idLastLocationPlayed[$i],$idArtist)))
					{
						$query_emp="
							UPDATE
								".__DBC_SCHEMATA_ARTIST_LAST_LOCATION_PLAYED__."
							SET	
								szVenueName = :szVenueName,
								szAddress = :szAddress,
								szAddress2 = :szAddress2,
								szCity = :szCity,
								szCounty = :szCounty,
								szPhone = :szPhone,
								szPostcode = :szPostcode,	
								dtStartDate = :dtStartDate,
								iTopLocation = :iTopLocation,		
								szAlternatePhone = :szAlternatePhone,
								szDescription = :szDescription,							
								dtUpdated = now()
							WHERE
								id=:id		
							AND
								idArtist=:idArtist
						";
						$update_flag = true;					
					}
					else
					{
						$query_emp = "
							INSERT INTO
								".__DBC_SCHEMATA_ARTIST_LAST_LOCATION_PLAYED__."
							(
								idArtist,
								iTopLocation,
								szVenueName,
								szAddress,
								szAddress2,
								szPostcode,
								szCity,
								szCounty,
								szPhone,
								szAlternatePhone,
								szDescription,							
								dtStartDate,
								iActive,
								dtCreated
							)
							VALUES
							(
								:idArtist,
								:iTopLocation,
								:szVenueName,
								:szAddress,
								:szAddress2,
								:szPostcode,
								:szCity,
								:szCounty,
								:szPhone,
								:szAlternatePhone,
								:szDescription,							
								:dtStartDate,
								'1',
								now()
							)
						";
						$added_flag = true;
					}
						
					if($this->preapreStatement($query_emp))
					{	
						if($update_flag)
						{
							$this->bindSQL(':id',(int)$this->idLastLocationPlayed[$i],PDO::PARAM_INT);
						}
						
						$this->bindSQL(':idArtist',(int)$idArtist,PDO::PARAM_INT);	
						$this->bindSQL(':iTopLocation',(int)$this->iTopLocation[$i],PDO::PARAM_INT);
						$this->bindSQL(':szVenueName',$this->szVenueName[$i],PDO::PARAM_STR);					
						$this->bindSQL(':szAddress',$this->szVAddress[$i],PDO::PARAM_STR);
						$this->bindSQL(':szAddress2',$this->szVAddress2[$i],PDO::PARAM_STR);
						$this->bindSQL(':szPostcode',$this->szVPostcode[$i],PDO::PARAM_STR);
						$this->bindSQL(':szCity',$this->szVCity[$i],PDO::PARAM_STR);
						$this->bindSQL(':szCounty',$this->szVCounty[$i],PDO::PARAM_STR);
						$this->bindSQL(':szPhone',$this->szVenuePhone[$i],PDO::PARAM_STR);
						$this->bindSQL(':szAlternatePhone',$this->szVenueAlternatePhone[$i],PDO::PARAM_STR);
						$this->bindSQL(':szDescription',$this->szVenueDescription[$i],PDO::PARAM_STR);					
						$this->bindSQL(':dtStartDate',$this->dtStartDate[$i],PDO::PARAM_STR);
									
						if(($result = $this->executeSQL($added_flag)))
						{
							// TO DO 
							if($added_flag)
							{
								$usedLocationIdAry[$ctr_ep] = $this->iLastInsertID ;
								
								//echo " Added ID ".$usedLocationIdAry[$ctr_ep]." artist id ".$idArtist;
							}
							else
							{
								$usedLocationIdAry[$ctr_ep] = $this->idLastLocationPlayed[$i] ;
								//echo " Updated ID ".$usedLocationIdAry[$ctr_ep]." artist id ".$idArtist;
							}			
							$ctr_ep++;
						}
					}
					else
					{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					}
				}
			}
		}
		else
		{
			$this->deleteLastLocationHistory($usedLocationIdAry,$idArtist) ;
		}
	}
	

	public function deleteLastLocationHistory($usedLocationIdAry,$idArtist,$iDeleteAll=false)
	{
		if($idArtist>0)
		{
			$idLocation_str = implode(',',$usedLocationIdAry);
			
			if(!empty($idLocation_str))
			{
				$query_and = " AND id NOT IN (".$idLocation_str.")	 ";
			}
			else if(!$iDeleteAll)
			{
				return false;
			}
			
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_ARTIST_LAST_LOCATION_PLAYED__."
				WHERE
					idArtist = :idArtist	
					$query_and
			";
			
			if($this->preapreStatement($query))
			{	
				$this->bindSQL(':idArtist',(int)$idArtist,PDO::PARAM_INT);
				
				if( ( $result = $this->executeSQL() ) )
				{
					return true ;
				}
			}
			else 
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	

	function isLastLocationIdExist($idLastLocationPlayed,$idArtist)
	{	
		if($idLastLocationPlayed>0 && $idArtist>0)
		{
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_ARTIST_LAST_LOCATION_PLAYED__."
				WHERE
					id='".(int)$idLastLocationPlayed."'		
				AND
					idArtist = '".(int)$idArtist."'	
			";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					return false ;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	function getAllLastLocationPlayed($idArtist,$iTopLocation=false)
	{		
		if($idArtist>0)
		{
			if($iTopLocation==1)
			{
				$query_and = " AND iTopLocation = '".(int)$iTopLocation."' " ;
			}
			else if($iTopLocation==2)
			{
				$query_and = " AND iTopLocation = '0' " ;
			}
			
			$query="
				SELECT
					id,
					idArtist,
					szVenueName,
					szAddress,
					szAddress2,
					szCity,
					szCounty,
					szPhone,
					szAlternatePhone,
					szDescription,
					szPostcode,
					dtStartDate,
					dtEndDate,
					iActive,
					dtCreated,
					iTopLocation
				FROM
					".__DBC_SCHEMATA_ARTIST_LAST_LOCATION_PLAYED__."
				WHERE
					idArtist = '".(int)$idArtist."'	
				AND
					isDeleted = '0'
				$query_and
			";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
						$ret_ary = array();
						$ctr = 0;
						while($row = $this->getAssoc())
						{
							$ret_ary[$ctr] = $row;
							$ctr++;
						}
						return $ret_ary;
				}
				else
				{
					return false ;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * This function checks if any location is already exists than it simply update existing record otherwise insert new record into database
	 * @access public
	 * @param string
	 * @return boolean 
	 * @author Ajay
	 */
	
	function addArtistUnavailableDate($idArtist,$calender_flag=false)
	{	
		$address_counter = count($this->dtUnavailableFromDate);
		if($address_counter>0)
		{	
			$ctr_ep = 0;
			$usedLocationIdAry = array();
			$query_emp = "";
			
			for($i=1;$i<=$address_counter;$i++)
			{	
				$update_flag = false;
				$added_flag = false;
                if($this->dtUnavailableFromDate[$i]==$this->dtUnavailableToDate[$i])
                {
                    if(!$calender_flag && ((int)$this->idUnavailableDates[$i]>0) && ($this->isUnavailableDatesIdExist($this->idUnavailableDates[$i],$idArtist)))
                    {
                        $query_emp="
                            UPDATE
                                ".__DBC_SCHEMATA_ARTIST_UNAVAILABLE_DATES__."
                            SET	
                                dtUnavailableFromDate = :dtUnavailableFromDate,
                                iStartHour = :iStartHour,
                                iStartMinute = :iStartMinute,
                                dtUnavailableToDate = :dtUnavailableToDate,
                                iEndHour = :iEndHour,
                                iEndMinute = :iEndMinute,
                                dtStartDateTime = :dtStartDateTime,
                                dtEndDateTime = :dtEndDateTime,
                                dtUpdatedOn = now()
                            WHERE
                                id=:id		
                            AND
                                idArtist=:idArtist
                        ";
                        $update_flag = true;					
                    }
                    else
                    {				
                        $iCurrent = 0;				
                        if($i==1)
                        {
                            $iCurrent = 1;
                        }

                        $query_emp = "
                            INSERT INTO
                                ".__DBC_SCHEMATA_ARTIST_UNAVAILABLE_DATES__."
                            (
                                idArtist,
                                dtUnavailableFromDate,
                                iStartHour,
                                iStartMinute,
                                dtUnavailableToDate,
                                iEndHour,
                                iEndMinute,
                                dtStartDateTime,
                                dtEndDateTime,
                                iActive,
                                dtCreatedOn
                            )
                            VALUES
                            (
                                :idArtist,
                                :dtUnavailableFromDate,
                                :iStartHour,
                                :iStartMinute,
                                :dtUnavailableToDate,
                                :iEndHour,
                                :iEndMinute,	
                                :dtStartDateTime,
                                :dtEndDateTime,
                                '1',
                                NOW()
                            )
                        ";
                        $added_flag = true;
                    }
//                    echo "<br> query ".$query_emp;
                    //die;				
                    if($this->preapreStatement($query_emp))
                    {	
                        if($update_flag)
                        {
                            $this->bindSQL(':id',(int)$this->idUnavailableDates[$i],PDO::PARAM_INT);
                        }

                        $dtStartDateTime = $this->dtUnavailableFromDate[$i]." ".$this->iStartHour[$i].":".$this->iStartMinute[$i].":00" ;
                        $dtEndDateTime = $this->dtUnavailableToDate[$i]." ".$this->iEndHour[$i].":".$this->iEndMinute[$i].":00" ;

                        $this->bindSQL(':idArtist',(int)$idArtist,PDO::PARAM_INT);					
                        $this->bindSQL(':dtUnavailableFromDate',$this->dtUnavailableFromDate[$i],PDO::PARAM_STR);					
                        $this->bindSQL(':iStartHour',$this->iStartHour[$i],PDO::PARAM_STR);
                        $this->bindSQL(':iStartMinute',$this->iStartMinute[$i],PDO::PARAM_STR);
                        $this->bindSQL(':dtUnavailableToDate',$this->dtUnavailableToDate[$i],PDO::PARAM_STR);
                        $this->bindSQL(':iEndHour',$this->iEndHour[$i],PDO::PARAM_STR);
                        $this->bindSQL(':iEndMinute',$this->iEndMinute[$i],PDO::PARAM_STR);

                        $this->bindSQL(':dtStartDateTime',$dtStartDateTime,PDO::PARAM_STR);
                        $this->bindSQL(':dtEndDateTime',$dtEndDateTime,PDO::PARAM_STR);

                        if(($result = $this->executeSQL($added_flag)))
                        {
                            if($calender_flag)
                            {
                                //echo "returnning ".$this->iLastInsertID ;;
                                return true;
                            }
                            else
                            {
                                // TO DO 
                                if($added_flag)
                                {
                                    $usedLocationIdAry[$ctr_ep] = $this->iLastInsertID ;
                                }
                                else
                                {
                                    $usedLocationIdAry[$ctr_ep] = $this->idUnavailableDates[$i] ;
                                }			
                                $ctr_ep++;
                            }
                        }
                    }
                    else
                    {
                        $this->error = true;
                        $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                        $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                        return false;
                    }
                }
                else
                {
                    $start_date=$this->dtUnavailableFromDate[$i];
                    $end_date=$this->dtUnavailableToDate[$i];
                    while($start_date <= $end_date)
                    {
                        $query_emp = "
                            INSERT INTO
                                ".__DBC_SCHEMATA_ARTIST_UNAVAILABLE_DATES__."
                            (
                                idArtist,
                                dtUnavailableFromDate,
                                iStartHour,
                                iStartMinute,
                                dtUnavailableToDate,
                                iEndHour,
                                iEndMinute,
                                dtStartDateTime,
                                dtEndDateTime,
                                iActive,
                                dtCreatedOn
                            )
                            VALUES
                            (
                                :idArtist,
                                :dtUnavailableFromDate,
                                '0',
                                '0',
                                :dtUnavailableToDate,
                                '23',
                                '59',
                                :dtStartDateTime,
                                :dtEndDateTime,
                                '1',
                                NOW()
                            )
                        ";
                        
                        if($this->preapreStatement($query_emp))
                        {	
                            if($update_flag)
                            {
                                $this->bindSQL(':id',(int)$this->idUnavailableDates[$i],PDO::PARAM_INT);
                            }
                            $end_date_time=$start_date.' 23:59:00';
                            $this->bindSQL(':idArtist',(int)$idArtist,PDO::PARAM_INT);					
                            $this->bindSQL(':dtUnavailableFromDate',$start_date,PDO::PARAM_STR);					
                            $this->bindSQL(':dtUnavailableToDate',$start_date,PDO::PARAM_STR);
                           
                            $this->bindSQL(':dtStartDateTime',$start_date,PDO::PARAM_STR);
                            $this->bindSQL(':dtEndDateTime',$end_date_time,PDO::PARAM_STR);

                            if(($result = $this->executeSQL($added_flag)))
                            {
                                $start_date=date('Y-m-d',strtotime($start_date . ' + 1 day'));
//                                
                            }
                        }
                        else
                        {
                            $this->error = true;
                            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                            return false;
                        }
                        
                    }
                }
			}
			if(!$calender_flag)
			{			
				$this->deleteUnavailableDatesHistory($usedLocationIdAry,$idArtist) ;
			}
		}
	}
	
	/**
	 * This function used to check unavailabe date id is exists or not.
	 * @access public
	 * @param int
	 * @return boolean 
	 * @author Ajay
	 */
	function isUnavailableDatesIdExist($idUnavailableDates,$idArtist)
	{	
		if($idUnavailableDates>0 && $idArtist>0)
		{
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_ARTIST_UNAVAILABLE_DATES__."
				WHERE
					id='".(int)$idUnavailableDates."'		
				AND
					idArtist = '".(int)$idArtist."'	
			";
			//echo " quety ".$query ;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					return false ;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * This function used to delete Unavailable dates 
	 * @access public
	 * @param int
	 * @return boolean 
	 * @author Ajay
	 */
	 
	public function deleteUnavailableDatesHistory($usedLocationIdAry,$idArtist,$flag=false)
	{
		if($idArtist>0 && !empty($usedLocationIdAry))
		{
			$idLocation_str = implode(',',$usedLocationIdAry);
			
			if(!empty($idLocation_str))
			{
				if($flag)
				{
					$query_and = " AND id IN (".$idLocation_str.")";
				}
				else
				{
					$query_and = " AND id NOT IN (".$idLocation_str.")";
				}
				
				$query="
					DELETE FROM
						".__DBC_SCHEMATA_ARTIST_UNAVAILABLE_DATES__."
					WHERE
						idArtist = :idArtist	
						$query_and
				";
				
				if($this->preapreStatement($query))
				{	
					$this->bindSQL(':idArtist',(int)$idArtist,PDO::PARAM_INT);
					
					if( ( $result = $this->executeSQL() ) )
					{
						return true ;
					}
				}
				else 
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
			}
		}
	}
	
	function getAllUnavailableDatesByArtist($idArtist,$dtUnavailableFromDate=false)
	{		
		if($idArtist>0)
		{
			if(!empty($dtUnavailableFromDate))
			{
				$query_and = " AND DATE(dtUnavailableFromDate)='".sanitize_all_html_input(trim($dtUnavailableFromDate))."' ";
			}
			
			$query="
				SELECT
					id,
					idArtist,
					dtUnavailableFromDate,
					iStartHour,
					iStartMinute,
					dtUnavailableToDate,
					iEndHour,
					iEndMinute,
					dtStartDateTime,
					dtEndDateTime,
					iActive,
					dtCreatedOn
				FROM
					".__DBC_SCHEMATA_ARTIST_UNAVAILABLE_DATES__."
				WHERE
					idArtist = '".(int)$idArtist."'	
				AND
					isDeleted = '0'
				$query_and
			";
			//echo "<br> ".$query."<br> ";
			
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
						$ret_ary = array();
						$ctr = 0;
						while($row = $this->getAssoc())
						{
							$ret_ary[$ctr] = $row;
							$ctr++;
						}
						return $ret_ary;
				}
				else
				{
					return false ;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}


	/**
	 * This function checks if any equipment is already exists than it simply update existing record otherwise insert new record into database
	 * @access public
	 * @param string
	 * @return boolean 
	 * @author Ajay
	 */
	
	function addArtistEquipmentDetails($idArtist)
	{	
		$equipment_counter = count($this->szSpeakerDescription);
		$this->deleteEquipmentDetails($idArtist);
		
		if($equipment_counter>0)
		{	
			$kImageCropper = new cImageCrop();
			for($i=1;$i<=3;$i++)
			{
				if(!empty($this->szUploadEquipmentPhoto[$i]) && $this->iEquipentFileUpload[$i])
				{
					$szTempFilePath = __APP_PATH_ARTIST_IMAGES_TEMP__."/".$this->szUploadEquipmentPhoto[$i];
					$szDestinationFilePath = __APP_PATH_ARTIST_IMAGES_EQUIPMENT__."/".$this->szUploadEquipmentPhoto[$i];
					if(file_exists($szTempFilePath))
					{	
						if(file_exists($szDestinationFilePath))
						{
							@unlink($szDestinationFilePath);
						}
						if(copy($szTempFilePath, $szDestinationFilePath)) 
						{
							//$kImageCropper->crop_center($szTempFilePath, $szDestinationFilePath,$this->szUploadEquipmentPhoto[$i], 500, 500);
						}
						@unlink($szTempFilePath);
					}
				}
			}
			
			$query_emp = "
				INSERT INTO
					".__DBC_SCHEMATA_EQUIPMENT_DETAILS__."
				(
					idArtist,
					szSpeakerDescription,
					szDeckDescription,
					szOtherDescription,
					fEquipmentCharge,
					szEquipentPhoto1,
					szEquipentPhoto2,
					szEquipentPhoto3,
					dtPATExpiryDate,
					iPATCertificate,
					iActive,
					dtCreatedOn
				)
				VALUES
				(
					:idArtist,
					:szSpeakerDescription,
					:szDeckDescription,
					:szOtherDescription,
					:fEquipmentCharge,
					:szEquipentPhoto1,
					:szEquipentPhoto2,
					:szEquipentPhoto3,
					:dtPATExpiryDate,
					:iPATCertificate,
					'1',
					NOW()
				)
			";
			if($this->preapreStatement($query_emp))
			{	
				$this->bindSQL(':idArtist',(int)$idArtist,PDO::PARAM_INT);					
				$this->bindSQL(':szSpeakerDescription',$this->szSpeakerDescription,PDO::PARAM_STR);

				$this->bindSQL(':szDeckDescription',$this->szDeckDescription,PDO::PARAM_STR);
				$this->bindSQL(':szOtherDescription',$this->szOtherDescription,PDO::PARAM_STR);
				$this->bindSQL(':fEquipmentCharge',$this->fEquipmentCharge,PDO::PARAM_STR);
				$this->bindSQL(':szEquipentPhoto1',$this->szUploadEquipmentPhoto[1],PDO::PARAM_STR);
				$this->bindSQL(':szEquipentPhoto2',$this->szUploadEquipmentPhoto[2],PDO::PARAM_STR);
				$this->bindSQL(':szEquipentPhoto3',$this->szUploadEquipmentPhoto[3],PDO::PARAM_STR);
				$this->bindSQL(':dtPATExpiryDate',$this->dtPATExpiryDate,PDO::PARAM_STR);
				$this->bindSQL(':iPATCertificate',$this->iPATCertificate,PDO::PARAM_STR);
				
				if(($result = $this->executeSQL($added_flag)))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	/**
	 * This function used to delete Unavailable dates 
	 * @access public
	 * @param int
	 * @return boolean 
	 * @author Ajay
	 */
	 
	public function deleteEquipmentDetails($idArtist)
	{
		if($idArtist>0)
		{
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_EQUIPMENT_DETAILS__."
				WHERE
					idArtist = :idArtist	
			";
			
			if($this->preapreStatement($query))
			{	
				$this->bindSQL(':idArtist',(int)$idArtist,PDO::PARAM_INT);
				
				if( ( $result = $this->executeSQL() ) )
				{
					return true ;
				}
			}
			else 
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function getAllEquipmentDetailsByArtist($idArtist)
	{		
		if($idArtist>0)
		{ 
			$query="
				SELECT
					id,
					idArtist,
					szSpeakerDescription,
					szDeckDescription,
					szOtherDescription,
					fEquipmentCharge,
					szEquipentPhoto1,
					szEquipentPhoto2,
					szEquipentPhoto3,
					dtPATExpiryDate,
					iPATCertificate,
					iActive,
					dtCreatedOn
				FROM
					".__DBC_SCHEMATA_EQUIPMENT_DETAILS__."
				WHERE
					idArtist = '".(int)$idArtist."'	
				AND
					isDeleted = '0'
			";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$ret_ary = array();
					$ctr = 0;
					while($row = $this->getAssoc())
					{
						$ret_ary[$ctr] = $row;
						$ctr++;
					}
					return $ret_ary;
				}
				else
				{
					return false ;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	/**
	 * This function checks if any certification is already exists than it simply update existing record otherwise insert new record into database
	 * @access public
	 * @param string
	 * @return boolean 
	 * @author Ajay
	 */
	
	function addArtistCertification($idArtist)
	{	
		$certif_counter = count($this->szCertification);
		if($certif_counter>0)
		{	
			$ctr_ep = 0;
			$usedLocationIdAry = array();
			$query_emp = "";
			
			for($i=1;$i<=$certif_counter;$i++)
			{	
				if(!empty($this->szCertification[$i]))
				{
					$update_flag = false;
					$added_flag = false;
					if(((int)$this->idCertificate[$i]>0) && ($this->isCertificationIdExist($this->idCertificate[$i],$idArtist)))
					{
						$query_emp="
							UPDATE
								".__DBC_SCHEMATA_CETIFICATION__."
							SET	
								szLicenseNumber = :szLicenseNumber,
								dtCertificateExpiry = :dtCertificateExpiry,
								dtUpdatedOn = now()
							WHERE
								id=:id		
							AND
								idArtist=:idArtist
						";
						$update_flag = true;					
					}
					else
					{				
						$iCurrent = 0;				
						if($i==1)
						{
							$iCurrent = 1;
						}
						
						$query_emp = "
							INSERT INTO
								".__DBC_SCHEMATA_CETIFICATION__."
							(
								idArtist,
								szLicenseNumber,
								dtCertificateExpiry,
								iActive,
								dtCreatedOn
							)
							VALUES
							(
								:idArtist,
								:szLicenseNumber,
								:dtCertificateExpiry,
								'1',
								now()
							)
						";
						$added_flag = true;
					}
									
					if($this->preapreStatement($query_emp))
					{	
						if($update_flag)
						{
							$this->bindSQL(':id',(int)$this->idCertificate[$i],PDO::PARAM_INT);
						}
						
						$this->bindSQL(':idArtist',(int)$idArtist,PDO::PARAM_INT);					
						$this->bindSQL(':szLicenseNumber',$this->szCertification[$i],PDO::PARAM_STR);					
						$this->bindSQL(':dtCertificateExpiry',$this->dtCertificateExpiry[$i],PDO::PARAM_STR);
											
						if(($result = $this->executeSQL($added_flag)))
						{
							// TO DO 
							if($added_flag)
							{
								$usedLocationIdAry[$ctr_ep] = $this->iLastInsertID ;
							}
							else
							{
								$usedLocationIdAry[$ctr_ep] = $this->idCertificate[$i] ;
							}			
							$ctr_ep++;
						}
					}
					else
					{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					}
				}
			}			
			$this->deleteCertificationHistory($usedLocationIdAry,$idArtist) ;
		}
	}
	
	/**
	 * This function used to check unavailabe date id is exists or not.
	 * @access public
	 * @param int
	 * @return boolean 
	 * @author Ajay
	 */
	function isCertificationIdExist($idCertificate,$idArtist)
	{	
		if($idCertificate>0 && $idArtist>0)
		{
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_CETIFICATION__."
				WHERE
					id='".(int)$idCertificate."'		
				AND
					idArtist = '".(int)$idArtist."'	
			";
			//echo " quety ".$query ;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					return false ;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * This function used to delete Unavailable dates 
	 * @access public
	 * @param int
	 * @return boolean 
	 * @author Ajay
	 */
	 
	public function deleteCertificationHistory($usedLocationIdAry,$idArtist)
	{
		if($idArtist>0 && !empty($usedLocationIdAry))
		{
			$idLocation_str = implode(',',$usedLocationIdAry);
			
			if(!empty($idLocation_str))
			{
				$query="
					DELETE FROM
						".__DBC_SCHEMATA_CETIFICATION__."
					WHERE
						id NOT IN (".$idLocation_str.")	
					AND
						idArtist = :idArtist	
				";
				
				if($this->preapreStatement($query))
				{	
					$this->bindSQL(':idArtist',(int)$idArtist,PDO::PARAM_INT);
					
					if( ( $result = $this->executeSQL() ) )
					{
						return true ;
					}
				}
				else 
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
			}
		}
	}
	
	function getAllCerticationByArtist($idArtist)
	{		
		if($idArtist>0)
		{
			$query="
				SELECT
					id,
					idArtist,
					szLicenseNumber,
					dtCertificateExpiry,
					iActive,
					dtCreatedOn
				FROM
					".__DBC_SCHEMATA_CETIFICATION__."
				WHERE
					idArtist = '".(int)$idArtist."'	
				AND
					dtCertificateExpiry>now()
				AND
					isDeleted = '0'
			";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$ret_ary = array();
					$ctr = 0;
					while($row = $this->getAssoc())
					{
						$ret_ary[$ctr] = $row;
						$ctr++;
					}
					return $ret_ary;
				}
				else
				{
					return false ;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	/**
	 * This function checks if any awards is already exists than it simply update existing record otherwise insert new record into database
	 * @access public
	 * @param string
	 * @return boolean 
	 * @author Ajay
	 */
	
	function addArtistAwards($idArtist)
	{	
		$award_counter = count($this->szAwardTitle);
		 
		if($award_counter>0)
		{	
			$ctr_ep = 0;
			$usedLocationIdAry = array();
			$query_emp = "";
			
			for($i=1;$i<=$award_counter;$i++)
			{ 
				if(!empty($this->szAwardTitle[$i]))
				{
					$update_flag = false;
					$added_flag = false;
					if(((int)$this->idAward[$i]>0) && ($this->isAwardsIdExist($this->idAward[$i],$idArtist)))
					{
						$query_emp="
							UPDATE
								".__DBC_SCHEMATA_AWARDS__."
							SET	
								szAwardTitle = :szAwardTitle,
								dtAwardDate = :dtAwardDate,
								dtUpdatedOn = now()
							WHERE
								id=:id		
							AND
								idArtist=:idArtist
						";
						$update_flag = true;					
					}
					else
					{				
						$iCurrent = 0;				
						if($i==1)
						{
							$iCurrent = 1;
						}
						
						$query_emp = "
							INSERT INTO
								".__DBC_SCHEMATA_AWARDS__."
							(
								idArtist,
								szAwardTitle,
								dtAwardDate,
								iActive,
								dtCreatedOn
							)
							VALUES
							(
								:idArtist,
								:szAwardTitle,
								:dtAwardDate,
								'1',
								now()
							)
						";
						$added_flag = true;
					} 			
					if($this->preapreStatement($query_emp))
					{	
						if($update_flag)
						{
							$this->bindSQL(':id',(int)$this->idAward[$i],PDO::PARAM_INT);
						}
						
						$this->bindSQL(':idArtist',(int)$idArtist,PDO::PARAM_INT);					
						$this->bindSQL(':szAwardTitle',$this->szAwardTitle[$i],PDO::PARAM_STR);					
						$this->bindSQL(':dtAwardDate',$this->dtAwardDate[$i],PDO::PARAM_STR);
											
						if(($result = $this->executeSQL($added_flag)))
						{
							// TO DO 
							if($added_flag)
							{
								$usedLocationIdAry[$ctr_ep] = $this->iLastInsertID ;
							}
							else
							{
								$usedLocationIdAry[$ctr_ep] = $this->idAward[$i] ;
							}			
							$ctr_ep++;
						}
					}
					else
					{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					}
				}
			}			
			$this->deleteAwardsHistory($usedLocationIdAry,$idArtist) ;
		}
	}
	
	/**
	 * This function used to check unavailabe date id is exists or not.
	 * @access public
	 * @param int
	 * @return boolean 
	 * @author Ajay
	 */
	function isAwardsIdExist($idAwards,$idArtist)
	{	
		if($idAwards>0 && $idArtist>0)
		{
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_AWARDS__."
				WHERE
					id='".(int)$idAwards."'		
				AND
					idArtist = '".(int)$idArtist."'	
			";
			//echo " quety ".$query ;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					return false ;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * This function used to delete Unavailable dates 
	 * @access public
	 * @param int
	 * @return boolean 
	 * @author Ajay
	 */
	 
	public function deleteAwardsHistory($usedLocationIdAry,$idArtist)
	{
		if($idArtist>0 && !empty($usedLocationIdAry))
		{
			$idLocation_str = implode(',',$usedLocationIdAry);
			
			if(!empty($idLocation_str))
			{
				$query="
					DELETE FROM
						".__DBC_SCHEMATA_AWARDS__."
					WHERE
						id NOT IN (".$idLocation_str.")	
					AND
						idArtist = :idArtist	
				";
				
				if($this->preapreStatement($query))
				{	
					$this->bindSQL(':idArtist',(int)$idArtist,PDO::PARAM_INT);
					
					if( ( $result = $this->executeSQL() ) )
					{
						return true ;
					}
				}
				else 
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
			}
		}
	}
	
	function getAllAwardsByArtist($idArtist)
	{		
		if($idArtist>0)
		{
			$query="
				SELECT
					id,
					idArtist,
					szAwardTitle,
					dtAwardDate,
					iActive,
					dtCreatedOn
				FROM
					".__DBC_SCHEMATA_AWARDS__."
				WHERE
					idArtist = '".(int)$idArtist."'	
				AND
					isDeleted = '0'
			";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$ret_ary = array();
					$ctr = 0;
					while($row = $this->getAssoc())
					{
						$ret_ary[$ctr] = $row;
						$ctr++;
					}
					return $ret_ary;
				}
				else
				{
					return false ;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * This function used to toggle Artists account status
	 * @access public
	 * @param int
	 * @return boolean 
	 * @author Ajay
	 */
	 
	function toggleArtistAccount($idArtist,$iValue,$field_name='iActive')
	{
		if($idArtist>0)
		{
			$this->loadTyni($idArtist);
			if($this->id<=0)
			{
				return false;
			}
			
			$query_update = "";
			if($field_name=='iAdminVerified')
			{
				$liquorAdminAry = get_admin_session_data();
				$idArtistFinderAdmin = $liquorAdminAry['idLiquorAdmin'];
				$kAdmin = new cAdmin();
				$kAdmin->load($idArtistFinderAdmin);
				$szAdminFullName = $kAdmin->szFirstName." ".$kAdmin->szLastName;
				
				$query_update = ", idAdminAuthorizedBy = '".(int)$idArtistFinderAdmin."', szAuthorizedBY = '".trim($szAdminFullName)."', dtAuthorizedDate=now() ";
			}
						
			$query = "
	    		UPDATE	    		
	    			".__DBC_SCHEMATA_ARTIST__."
	    		SET
	    			".$field_name." = :iActive
	    			$query_update
	    		WHERE
	    			id = :idArtist
	    		AND
				    isDeleted = '0'
	    	";
			//echo "<br>".$query."<br>";
			//die;
	    	if($this->preapreStatement($query))
			{
				$this->bindSQL(':iActive',(int)$iValue,PDO::PARAM_INT);	
				$this->bindSQL(':idArtist',(int)$this->id,PDO::PARAM_INT);				
				if(($result = $this->executeSQL()))
				{
					/*
					* Adding Admin Activity logs
					*/
					$kAdmin = new cAdmin();
					$adminActivityLogsAry = array();
					$adminActivityLogsAry['szType'] = 'ARTIST';
					$adminActivityLogsAry['szActionType'] = $field_name;
					$adminActivityLogsAry['iActionValue'] = $iValue;
					$adminActivityLogsAry['idPrimaryTable'] = $this->id;
					$adminActivityLogsAry['szRemarks'] = 'Admin has toggled DJ account';
					$kAdmin->addAdminActivityLogs($adminActivityLogsAry);
					
					if($field_name=='iAdminVerified')
					{
						$this->loadTyni($idArtist);
						
						$replace_arr = array();								
						$replace_arr['szFullName'] = $this->szFirstName." ".$this->szLastName;
						$replace_arr['szArtistName'] = $this->szArtistName;
						$replace_arr['szSiteName'] = SITE_NAME;	
						$replace_arr['szArtistNumber'] = $this->szArtistNumber;
						
						if($iValue==1)
						{
							createEmail(__ARTIST_ACCOUNT_PUBLISHED_CONFIRMATION__,$replace_arr,$this->szEmail,__VERIFY_ARTIST_ACCOUNT_BY_ADMIN_SUBJECT__,__STORE_SUPPORT_FROM_EMAIL__);
						}
						else
						{
							createEmail(__ARTIST_ACCOUNT_REJECTED_CONFIRMATION__,$replace_arr,$this->szEmail,__VERIFY_ARTIST_ACCOUNT_BY_ADMIN_SUBJECT__,__STORE_SUPPORT_FROM_EMAIL__);
						}
								
						$referencesAry = array();
						$referencesAry = $this->getAllReferencesByArtist($idArtist);
						
						if(!empty($referencesAry))
						{
							$cert_count = 1;
							$idReferenceAry = array();
							foreach($referencesAry as $referencesArys)
							{
								if($referencesArys['iEmailSent']!=1)
								{
									$szLinkValue=__BASE_DJ_REFERENCE_RATING_URL__."".$this->szArtistKey."/".$referencesArys['szReferenceKey']."/?action=Email";	
									
									$replace_arr = array();	
									$replace_arr['szLink']="<a href='".$szLinkValue."'>Click here to rate ".$this->szArtistName."</a>";
									$replace_arr['szHttpsLink'] = $szLinkValue ;					
									$replace_arr['szRefName'] = $referencesArys['szName'];	
									$replace_arr['szFullName'] = $this->szFirstName." ".$this->szLastName;
									$replace_arr['szArtistName'] = $this->szArtistName;
									$replace_arr['szSiteName'] = SITE_NAME;
									createEmail(__REFERENCE_REQUEST_OF_DJ__,$replace_arr,$referencesArys['szEmail'],__REFERENCE_REQUEST_OF_DJ_SUBJECT__,__STORE_SUPPORT_EMAIL__,'','','','',true);
									$idReferenceAry[] = $referencesArys['id'];
								} 
							}
							if(!empty($idReferenceAry))
							{
								$this->updateReferalEmails($idReferenceAry,$idArtist,1);
							}
						}
						return true;
					}
					else
					{
						return true;
					}
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function deleteArtisAccount($idArtist)
	{	
		if($idArtist>0)
		{
			$this->loadTyni($idArtist);
			if($this->id<=0)
			{
				return false;
			}
			
			$query = "
	    		UPDATE	    		
	    			".__DBC_SCHEMATA_ARTIST__."
	    		SET
	    			isDeleted = 1
	    		WHERE
	    			id = :idArtist
	    		AND
				    isDeleted = '0'
	    	";
			//echo "<br>".$query."<br>";
	    	if($this->preapreStatement($query))
			{
				$this->bindSQL(':idArtist',(int)$this->id,PDO::PARAM_INT);				
				if(($result = $this->executeSQL()))
				{
					return true;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}

	/**
	 * function to log user in to system
	 * @access public
	 * @param $key
	 * @return bool
	 * @author Ajay
	*/
	public function autoLogin( $data ,$iFacebookLogin=false)
	{
		if(is_array($data))
		{
			if($iFacebookLogin)
			{
				$this->set_szLoginUserName(sanitize_all_html_input($data['szUserName']));	
				$this->set_iUserType(sanitize_all_html_input($data['iUserType']));
			}
			else
			{
				$this->set_szLoginUserName(sanitize_all_html_input($data['szUserName']));			
				$this->set_szLoginPassword(sanitize_all_html_input($data['szPassword']));
				$this->set_iUserType(sanitize_all_html_input($data['iUserType']));
				
				$query_where = " AND szPassword = :password ";
			}
			
			if ($this->error === true)
			{
				return false;
			}
			
			if($this->iUserType==__USER_TYPE_ARTIST__)
			{
				$szTableName = __DBC_SCHEMATA_ARTIST__ ;
			}
			else
			{
				$szTableName = __DBC_SCHEMATA_BUYER__ ;
			}
			
			$query = "
				SELECT
					id,
					szEmail
				FROM
					". $szTableName ."
				WHERE
					szEmail = :userName
					$query_where
				AND
					iActive = '1'
				AND
					isDeleted = '0'
			";
			//echo "<br />".$query ;
			if($this->preapreStatement($query))
			{
				$this->bindSQL(':userName',$this->szUserName,PDO::PARAM_STR);
				if(!$iFacebookLogin)
				{
					$this->bindSQL(':password',md5($this->szPassword),PDO::PARAM_STR);
				}
				
				if(($result = $this->executeSQL()))
				{
					if($this->iNumRows>0)
					{
						$row = $this->getAssoc();
						
						$_SESSION['user_id'] = $row['id'];
						$_SESSION['iUserType'] = $this->iUserType;
						
						$this->id = $row['id'];
						$this->iUserType = $this->iUserType;					
						$this->szUserName = $row['szEmail'];
						
						//adding a unique token in session and cookie
						$token = sha1(uniqid(rand(), true));
						$_SESSION[__AUTH_TOKEN__] = $token;
						
						if ($data['iRememberMe'] == 1)
						{
							setcookie(  __AUTH_TOKEN__ , $token, time() + __COOKIE_TIME_CONSTANT__, __COOKIE_PATH__);
							$_COOKIE[__AUTH_TOKEN__] = $token;
							
							$this->saveCookie( $token );
						}
										
						$this->set_iSuccess('1');
						$this->userLoginLog();	
						$this->update_last_login($row['id']);						
						return true;
					}
					else
					{
						$this->set_iSuccess('0');
						$this->userLoginLog();					
						$this->addError("account_login_password", "Invalid User Name or Password.");
						return false;
					}
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
			}
		}
		else
		{	
			return false;
		}
	}
	
	/**
	 * This function loads Artist object from database
	 * @access public
	 * @param int
	 * @return boolean 
	 * @author Ajay
	 */
	 
	public function load($id=false,$szArtistKey=false)
	{	
		if((int)$id>0 || !empty($szArtistKey))
		{
			if(!empty($szArtistKey))
			{
				$query_where = " WHERE szArtistKey = :szArtistKey AND isDeleted = '0'";
			}
			else if((int)$id>0)
			{
				$query_where = " WHERE id = :id AND isDeleted = '0'";
			}
			else
			{
				return false;
			}
			
			$query="
				SELECT 
					id,
					szArtistKey,
					szArtistNumber,
					szFirstName,
					szLastName,
					szArtistName,
					dtDateOfBirth,
					szEmail,
					szPassword,
					szPhone,
					szAlternatePhone,
					szAddress,
					szAddress2,
					szPostcode,
					szCity,
					szCounty,					
					szSAddress,
					szSPostcode,
					szSCity,
					szSCounty,					
					szLatitute,
					szLongitute,
					iDistanceWillingToTravel,
					idPrimaryArtType,
					idSecondaryArtType,
					szDescription,
					szReferences,
					fBaseFee,
					fFeePerHour,
					szVideo1,
					szVideo2,
					szVideo3,
					szVideo4,
					szPhoto1,
					szPhoto2,
					szPhoto3,
					szPhoto4,
					szConfirmationKey,
					szReferer,
					szIPAddress,
					szUserAgent,
					dtCreatedOn,
					iActive,					
					iAddedByAdmin,
					iComplete,
					isDeleted,
					iEquipment,					
					iYearStarted,
					iStep1Completed,
					iStep2Completed,
					iStep3Completed,
					iStep4Completed,
					iStep5Completed,
					fTotalEarning,
					dtVerificationLinkSent,
					fAvgRating,
					iMemberShipType,
					fMembershipCost,
					iAccredited,
					dtNextPaymentDate,
					dtSubscriptionStartingDate,
					szSubscriptionProfileID,
					idSubscription,
					iMaxPerformanceLenght,
					iFeatured,
					iAdminVerified,
                    szFeaturedQuote
				FROM
					".__DBC_SCHEMATA_ARTIST__."
				$query_where
			";
			//echo "<br /> ".$query."<br /> key ".$szArtistKey;
			//die;
			
			if($this->preapreStatement($query))
			{
				if(!empty($szArtistKey))
				{
					$this->bindSQL(':szArtistKey',$szArtistKey,PDO::PARAM_STR);
				}
				else
				{
					$this->bindSQL(':id',(int)$id,PDO::PARAM_INT);
				}
				
				if( ( $result = $this->executeSQL() ) )
				{
					if($this->iNumRows > 0)
					{
						$row = $this->getAssoc();
						$this->id = sanitize_all_html_input($row['id']);
						$this->szArtistKey = sanitize_all_html_input($row['szArtistKey']);		
						$this->szArtistNumber = sanitize_all_html_input($row['szArtistNumber']);
						$this->szFirstName = sanitize_all_html_input($row['szFirstName']);
						$this->szLastName = sanitize_all_html_input($row['szLastName']);						
						$this->szArtistName = sanitize_all_html_input($row['szArtistName']);
						$this->dtDateOfBirth = sanitize_all_html_input($row['dtDateOfBirth']);
						$this->szEmail = sanitize_all_html_input($row['szEmail']);
						$this->szPhone = sanitize_all_html_input($row['szPhone']);
						$this->szAlternatePhone = sanitize_all_html_input($row['szAlternatePhone']);
						$this->szAddress = sanitize_all_html_input($row['szAddress']);
						$this->szAddress2 = sanitize_all_html_input($row['szAddress2']);						
						$this->szPostcode = sanitize_all_html_input($row['szPostcode']);
						$this->szCity = sanitize_all_html_input($row['szCity']);
						$this->szCounty = sanitize_all_html_input($row['szCounty']);
						$this->szLatitute = sanitize_all_html_input($row['szLatitute']);
						$this->szLongitute = sanitize_all_html_input($row['szLongitute']);
						
						$this->szSAddress = sanitize_all_html_input($row['szSAddress']);
						$this->szSPostcode = sanitize_all_html_input($row['szSPostcode']);
						$this->szSCity = sanitize_all_html_input($row['szSCity']);
						$this->szSCounty = sanitize_all_html_input($row['szSCounty']);
						
						$this->iDistanceWillingToTravel = sanitize_all_html_input($row['iDistanceWillingToTravel']);
						$this->idPrimaryArtType = sanitize_all_html_input($row['idPrimaryArtType']);
						$this->idSecondaryArtType = sanitize_all_html_input($row['idSecondaryArtType']);
						$this->szDescription = sanitize_all_html_input($row['szDescription']);
						$this->szReferences = sanitize_all_html_input($row['szReferences']);
						$this->fBaseFee = sanitize_all_html_input($row['fBaseFee']);						
						$this->fFeePerHour = sanitize_all_html_input($row['fFeePerHour']);						
						$this->szVideo1 = sanitize_all_html_input($row['szVideo1']);
						$this->szVideo2 = sanitize_all_html_input($row['szVideo2']);
						$this->szVideo3 = sanitize_all_html_input($row['szVideo3']);
						$this->szVideo4 = sanitize_all_html_input($row['szVideo4']);
						$this->szPhoto1 = sanitize_all_html_input($row['szPhoto1']);
						$this->szPhoto2 = sanitize_all_html_input($row['szPhoto2']);
						$this->szPhoto3 = sanitize_all_html_input($row['szPhoto3']);
						$this->szPhoto4 = sanitize_all_html_input($row['szPhoto4']);
						$this->szConfirmationKey = sanitize_all_html_input($row['szConfirmationKey']);
						$this->iComplete = sanitize_all_html_input($row['iComplete']);	
						$this->iAccredited = sanitize_all_html_input($row['iAccredited']);		
						
						$this->iActive = sanitize_all_html_input($row['iActive']);		
						$this->isDeleted = sanitize_all_html_input($row['isDeleted']);		
						$this->dtCreatedOn = sanitize_all_html_input($row['dtCreatedOn']);	
						
						$this->szReferer = sanitize_all_html_input($row['szReferer']);
						$this->szIPAddress = sanitize_all_html_input($row['szIPAddress']);
						$this->szUserAgent = sanitize_all_html_input($row['szUserAgent']);		

						$this->iStep1Completed = sanitize_all_html_input($row['iStep1Completed']);
						$this->iStep2Completed = sanitize_all_html_input($row['iStep2Completed']);
						$this->iStep3Completed = sanitize_all_html_input($row['iStep3Completed']);
						$this->iStep4Completed = sanitize_all_html_input($row['iStep4Completed']);
						$this->iStep5Completed = sanitize_all_html_input($row['iStep5Completed']);
						$this->dtVerificationLinkSent = sanitize_all_html_input($row['dtVerificationLinkSent']);
						$this->fAvgRating = sanitize_all_html_input($row['fAvgRating']);
						
						$this->iMemberShipType = sanitize_all_html_input($row['iMemberShipType']);
						$this->fMembershipCost = sanitize_all_html_input($row['fMembershipCost']);
						$this->iMaxPerformanceLenght = sanitize_all_html_input($row['iMaxPerformanceLenght']);
						$this->iFeatured = sanitize_all_html_input($row['iFeatured']);
						$this->iAdminVerified = sanitize_all_html_input($row['iAdminVerified']);
						$this->szFeaturedQuote = sanitize_all_html_input($row['szFeaturedQuote']);
						
						
						$awardsAry = array();
						$awardsAry = $this->getAllAwardsByArtist($this->id);
						
						$szAwardReceivedText='';
						if(!empty($awardsAry))
						{
							$awrd_count = 1;
							$iAwardAryCount = count($awardsAry);
							foreach($awardsAry as $awardsArys)
							{								
								$this->szAwardTitle[$awrd_count] = sanitize_all_html_input($awardsArys['szAwardTitle']);	
								$this->dtAwardDate[$awrd_count] = sanitize_all_html_input($awardsArys['dtAwardDate']);
								$this->idAward[$awrd_count] = sanitize_all_html_input($awardsArys['id']);
								
								if(!empty($this->szAwardTitle[$awrd_count]))
								{
									$szAwardReceivedText .= $this->szAwardTitle[$awrd_count]." (".date('Y',strtotime($this->dtAwardDate[$awrd_count])).")";
									if($iAwardAryCount!=$awrd_count)
									{
										$szAwardReceivedText .= "<br>";
									}
								}
								$awrd_count++;
							}
						}
						$this->szAwardReceivedText = $szAwardReceivedText;
						
						$certificationAry = array();
						$certificationAry = $this->getAllCerticationByArtist($this->id);
						$szCertificationDetailsText = '';
						if(!empty($certificationAry))
						{
							$cert_count = 1;
							$cert_ary_count = count($certificationAry);
							foreach($certificationAry as $certificationArys)
							{
								$this->szCertification[$cert_count] = sanitize_all_html_input($certificationArys['szLicenseNumber']);	
								$this->dtCertificateExpiry[$cert_count] = sanitize_all_html_input($certificationArys['dtCertificateExpiry']);
								$this->idCertificate[$cert_count] = sanitize_all_html_input($certificationArys['id']);
								if(!empty($this->szCertification[$cert_count]))
								{
									$szCertificationDetailsText .= $this->szCertification[$cert_count] ;
									if($cert_ary_count!=$cert_count)
									{
										$szCertificationDetailsText .="<br>";
									}
								}
								
								$cert_count++;
							}
						}
						$this->szCertificationDetailsText = $szCertificationDetailsText;
						
						$equipmentDetailsAry = array();
						$equipmentDetailsAry = $this->getAllEquipmentDetailsByArtist($this->id);
						if(!empty($equipmentDetailsAry))
						{
							$cert_count = 1;
							foreach($equipmentDetailsAry as $equipmentDetailsArys)
							{
								$this->szSpeakerDescription = sanitize_all_html_input($equipmentDetailsArys['szSpeakerDescription']);	
								$this->szDeckDescription = sanitize_all_html_input($equipmentDetailsArys['szDeckDescription']);
								$this->szOtherDescription = sanitize_all_html_input($equipmentDetailsArys['szOtherDescription']);	
								$this->fEquipmentCharge = sanitize_all_html_input($equipmentDetailsArys['fEquipmentCharge']);
								$this->szEquipentPhoto1 = sanitize_all_html_input($equipmentDetailsArys['szEquipentPhoto1']);	
								$this->szEquipentPhoto2 = sanitize_all_html_input($equipmentDetailsArys['szEquipentPhoto2']);
								$this->szEquipentPhoto3 = sanitize_all_html_input($equipmentDetailsArys['szEquipentPhoto3']);
								if(strtotime($equipmentDetailsArys['dtPATExpiryDate'])>=time())
								{
									$this->dtPATExpiryDate = sanitize_all_html_input($equipmentDetailsArys['dtPATExpiryDate']);
									$this->iPATCertificate = sanitize_all_html_input($equipmentDetailsArys['iPATCertificate']);
								}								
								$this->idEquipmentDetails = sanitize_all_html_input($equipmentDetailsArys['id']);
								$cert_count++;
							}
						}
						
						$referencesAry = array();
						$referencesAry = $this->getAllReferencesByArtist($this->id);
						if(!empty($referencesAry))
						{
							$cert_count = 1;
							foreach($referencesAry as $referencesArys)
							{
								$this->szRefName[$cert_count] = sanitize_all_html_input($referencesArys['szName']);	
								$this->szRefEmail[$cert_count] = sanitize_all_html_input($referencesArys['szEmail']);
								$this->szRefPosition[$cert_count] = sanitize_all_html_input($referencesArys['szPosition']);	
								$this->szRefBussinessName[$cert_count] = sanitize_all_html_input($referencesArys['szBussinessName']);
								$this->idReference[$cert_count] = sanitize_all_html_input($referencesArys['id']);
								$cert_count++;
							}
						}
						
						$this->iEquipment = sanitize_all_html_input($row['iEquipment']);
						$this->iYearStarted = sanitize_all_html_input($row['iYearStarted']);
						$this->fTotalEarning = sanitize_all_html_input($row['fTotalEarning']);	

						$this->dtNextPaymentDate = sanitize_all_html_input($row['dtNextPaymentDate']);
						$this->dtSubscriptionStartingDate = sanitize_all_html_input($row['dtSubscriptionStartingDate']);
						$this->szSubscriptionProfileID = sanitize_all_html_input($row['szSubscriptionProfileID']);
						$this->idSubscription = sanitize_all_html_input($row['idSubscription']);
						return $row;			
					}
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
/**
	 * This function loads DJ object from database
	 * @access public
	 * @param int
	 * @return boolean 
	 * @author Ajay
	 */
	 
	public function loadTyni($id=false,$szArtistKey=false)
	{	
		if((int)$id>0 || !empty($szArtistKey))
		{
			if(!empty($szArtistKey))
			{
				$query_where = " WHERE szArtistKey = :szArtistKey AND isDeleted = '0' ";
			}
			else if((int)$id>0)
			{
				$query_where = " WHERE id = :id AND isDeleted = '0'";
			}
			else
			{
				return false;
			}
			
			$query="
				SELECT 
					id,
					szArtistKey,
					szArtistNumber,
					szFirstName,
					szLastName,
					szArtistName,
					szEmail,
					iStep1Completed,
					iStep2Completed,
					iStep3Completed,
					iStep4Completed,
					iStep5Completed,
					iActive,	
					iComplete,
					isDeleted,
					dtVerificationLinkSent,
					iVerified,
					iMemberShipType,
					fMembershipCost,
					dtNextPaymentDate,
					dtSubscriptionStartingDate,
					szSubscriptionProfileID,
					idSubscription,
					dtDateOfBirth,
					szPhoto1,
					szPhoto2,
					szPhoto3,
					szPhoto4,
					szVideo1,
					szVideo2,
					szVideo3,
					szVideo4,
					iAdminVerified,
					iMemberShipType,
                    iFeatured
				FROM
					".__DBC_SCHEMATA_ARTIST__."
				$query_where
			";
//			echo "<br /> ".$query."<br /> key ".$szArtistKey;
			//die;
			
			if($this->preapreStatement($query))
			{
				if(!empty($szArtistKey))
				{   
					$this->bindSQL(':szArtistKey',$szArtistKey,PDO::PARAM_STR);
				}
				else
				{
                    $this->bindSQL(':id',(int)$id,PDO::PARAM_INT);
				}
                    
				if( ( $result = $this->executeSQL() ) )
				{  if($this->iNumRows > 0)
					{   
//                        echo '3';
						$row = $this->getAssoc();
                        $this->id = sanitize_all_html_input($row['id']);
						$this->szArtistKey = sanitize_all_html_input($row['szArtistKey']);		
						$this->szArtistNumber = sanitize_all_html_input($row['szArtistNumber']);				
						$this->szFirstName = sanitize_all_html_input($row['szFirstName']);
						$this->szLastName = sanitize_all_html_input($row['szLastName']);						
						$this->szArtistName = sanitize_all_html_input($row['szArtistName']);
						$this->szEmail = sanitize_all_html_input($row['szEmail']);						
						$this->iComplete = sanitize_all_html_input($row['iComplete']);
						$this->iActive = sanitize_all_html_input($row['iActive']);		
						$this->isDeleted = sanitize_all_html_input($row['isDeleted']);	
						
						$this->iStep1Completed = sanitize_all_html_input($row['iStep1Completed']);
						$this->iStep2Completed = sanitize_all_html_input($row['iStep2Completed']);
						$this->iStep3Completed = sanitize_all_html_input($row['iStep3Completed']);
						$this->iStep4Completed = sanitize_all_html_input($row['iStep4Completed']);
						$this->iStep5Completed = sanitize_all_html_input($row['iStep5Completed']);
						$this->dtVerificationLinkSent = sanitize_all_html_input($row['dtVerificationLinkSent']);
						$this->iVerified = sanitize_all_html_input($row['iVerified']);			

						$this->iMemberShipType = sanitize_all_html_input($row['iMemberShipType']);
						$this->fMembershipCost = sanitize_all_html_input($row['fMembershipCost']);	
						$this->dtNextPaymentDate = sanitize_all_html_input($row['dtNextPaymentDate']);
						$this->dtSubscriptionStartingDate = sanitize_all_html_input($row['dtSubscriptionStartingDate']);
						$this->szSubscriptionProfileID = sanitize_all_html_input($row['szSubscriptionProfileID']);
						$this->idSubscription = sanitize_all_html_input($row['idSubscription']);
						$this->dtDateOfBirth = sanitize_all_html_input($row['dtDateOfBirth']);
						
						$this->szPhoto1 = sanitize_all_html_input($row['szPhoto1']);
						$this->szPhoto2 = sanitize_all_html_input($row['szPhoto2']);
						$this->szPhoto3 = sanitize_all_html_input($row['szPhoto3']);
						$this->szPhoto4 = sanitize_all_html_input($row['szPhoto4']);
						
						$this->szVideo1 = sanitize_all_html_input($row['szVideo1']);
						$this->szVideo2 = sanitize_all_html_input($row['szVideo2']);
						$this->szVideo3 = sanitize_all_html_input($row['szVideo3']);
						$this->szVideo4 = sanitize_all_html_input($row['szVideo4']);
						$this->iAdminVerified = sanitize_all_html_input($row['iAdminVerified']);
						$this->iFeatured = sanitize_all_html_input($row['iFeatured']);
						return $row;			
					}
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	/**
	 * This function checks authentication of user
	 * @access public
	 * @param string
	 * @return boolean 
	 * @author Ajay
	 */
	 
	function checkAuth()
	{
		// check session variable
		if (!isset($_SESSION[__AUTH_TOKEN__]))
		{
			if ($this->loginFromCookie())
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return true;
		}
	}	
	
	/**
	 * function to log user in to system
	 * @access public
	 * @param $key
	 * @return bool
	 * @author Ajay
	*/
	public function login( $data )
	{
            if( is_array( $data ) )
            {
                    $this->set_szLoginUserName(sanitize_all_html_input($data['szUserName']));
                    $this->set_szLoginPassword(sanitize_all_html_input($data['szPassword']));

                    if ($this->error === true)
                    {
                            return false;
                    }

                    if($this->isUserNameExist($this->szUserName))
                    {
                            $data['iUserType'] = __USER_TYPE_ARTIST__;
                            $this->iUserType = __USER_TYPE_ARTIST__;
                            $szTableName = __DBC_SCHEMATA_ARTIST__ ;
                    }
                    else
                    {
                            $data['iUserType'] = __USER_TYPE_CLIENT__;
                            $this->iUserType = __USER_TYPE_CLIENT__;
                            $szTableName = __DBC_SCHEMATA_BUYER__ ;
                    }
			
			$query = "
				SELECT
					id,
					szEmail
				FROM
					". $szTableName ."
				WHERE
					szEmail = :userName
				AND
					szPassword = :password
				AND
					iActive = '1'
				AND
					isDeleted = '0'
			";
//			echo "<br />".$query ;
			if($this->preapreStatement($query))
			{
				$this->bindSQL(':userName',$this->szUserName,PDO::PARAM_STR);
				$this->bindSQL(':password',md5($this->szPassword),PDO::PARAM_STR);
				
				if( ( $result = $this->executeSQL() ) )
				{
					if($this->iNumRows>0)
					{
						$row = $this->getAssoc();
						
						$_SESSION['user_id'] = $row['id'];
						$_SESSION['iUserType'] = $this->iUserType;
						
						$this->id = $row['id'];
						$this->iUserType = $this->iUserType;					
						$this->szUserName = $row['szEmail'];
						
						//adding a unique token in session and cookie
						$token = sha1(uniqid(rand(), true));
						$_SESSION[__AUTH_TOKEN__] = $token;
						
						if ($data['iRememberMe'] == 1)
						{
							setcookie(  __AUTH_TOKEN__ , $token, time() + __COOKIE_TIME_CONSTANT__, __COOKIE_PATH__);
							$_COOKIE[__AUTH_TOKEN__] = $token;
							
							$this->saveCookie( $token );
						}
						setcookie(  __USER_LAST_LOGGED_IN_EMAIL__ , $this->szUserName, time() + __COOKIE_TIME_CONSTANT__, __COOKIE_PATH__);				
						$this->set_iSuccess('1');
						$this->userLoginLog();	
						$this->update_last_login($row['id']);		
						//print_R($_SESSION);				
						return true;
					}
					else
					{
						$this->set_iSuccess('0');
						$this->userLoginLog();					
						$this->addError("account_login_password", "Invalid User Name or Password.");
						return false;
					}
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
			}
		}
		else
		{	
			return false;
		}
	}
	

	/**
	 * writes the user log history to the database
	 *
	 * @access public
	 * @return bool
	 * @author Ajay
	 */
	 
	function userLoginLog($cookieFlag=false,$iAdmin=false)
	{
        $cookieValue=0;
        if($cookieFlag===true)
        {
           $cookieValue=1; 
        }
        
		$query = "
				INSERT INTO
					".__DBC_SCHEMATA_LOGIN_LOG__."
					(
						 idBorrower,
						 iUserType,
						 szUserName,
						 dtLogin,
						 iSuccess,
						 szIP,
						 szReferer,
						 szUserAgent,
                         isFromCookie,
                         iAdminFlag
					)
				VALUES
					(
						:idBorrower,
						:iUserType,
						:szUserName,
						NOW(),
						:iSuccess,
						:szIP,
						:szReferer,
						:szUserAgent,
                        :isFromCookie,
                        :iAdminFlag 
					)
			";
			//echo "<br />".$query."<br/>";
					
			if($this->preapreStatement($query))
			{                         
				//$szReferer = "�������";
				
				$this->bindSQL(':idBorrower',(int)$this->id,PDO::PARAM_INT);
				$this->bindSQL(':iUserType',(int)$this->iUserType,PDO::PARAM_INT);
				$this->bindSQL(':szUserName',$this->szUserName,PDO::PARAM_STR);				
				$this->bindSQL(':iSuccess',(int)$this->iSuccess,PDO::PARAM_INT);
				$this->bindSQL(':szIP',__REMOTE_ADDR__,PDO::PARAM_STR);
				$this->bindSQL(':szReferer',__HTTP_REFERER__,PDO::PARAM_STR);
				$this->bindSQL(':szUserAgent',__HTTP_USER_AGENT__,PDO::PARAM_STR);
				$this->bindSQL(':isFromCookie',(int)$cookieValue,PDO::PARAM_INT);
				$this->bindSQL(':iAdminFlag',(int)$iAdminFlag,PDO::PARAM_INT);
				
				if( ( $result = $this->executeSQL(true) ) )
				{
					/*
					if($this->iLastInsertID<=0)
					{
						$this->iLastInsertID = $this->getLastInsertedId(__DBC_SCHEMATA_LOGIN_LOG__);
					}
					*/
					return true;
				}
			}
	}
	
	/**
	 * writes the contact us log history to the database
	 *
	 * @access public
	 * @return bool
	 * @author Ajay
	 */
	 
	function addContactUsLogs($data)
	{
		if(!empty($data))
		{
			$this->set_szName(sanitize_all_html_input($data['szName']));
			$this->set_szEmail(sanitize_all_html_input($data['szEmail']));
			$this->set_szPhone(sanitize_all_html_input($data['szPhone']));
			$this->set_szComment(sanitize_all_html_input($data['szComment']));
			$this->set_szCaptchaCode(sanitize_all_html_input($data['szCaptchaCode']));
					
			if (!empty($this->szCaptchaCode)) 
		 	{		      
			      $kSecureImage = new Securimage();		
			      if ($kSecureImage->check($this->szCaptchaCode) == false) 
			      {
			        $this->addError('szCaptchaCode','Incorrect security code entered');
			      }
		    }
		    
			if($this->error ==true)
			{
				return false;
			}
		
			$query = "
				INSERT INTO
					".__DBC_SCHEMATA_CONTACT_US__."
				(
					 szName,
					 szEmail,
					 szPhone,
					 dtContacted,
					 szComment,
					 szIP,
					 szReferer,
					 szUserAgent
				)
				VALUES
				(
					:szName,
					:szEmail,
					:szPhone,
					NOW(),
					:szComment,
					:szIP,
					:szReferer,
					:szUserAgent
				)
			";
			//echo "<br />".$query."<br/>";
					
			if($this->preapreStatement($query))
			{	
				$this->bindSQL(':szName',$this->szName,PDO::PARAM_STR);
				$this->bindSQL(':szEmail',$this->szEmail,PDO::PARAM_STR);
				$this->bindSQL(':szPhone',$this->szPhone,PDO::PARAM_STR);				
				$this->bindSQL(':szComment',$this->szComment,PDO::PARAM_STR);
				$this->bindSQL(':szIP',__REMOTE_ADDR__,PDO::PARAM_STR);
				$this->bindSQL(':szReferer',__HTTP_REFERER__,PDO::PARAM_STR);
				$this->bindSQL(':szUserAgent',__HTTP_USER_AGENT__,PDO::PARAM_STR);
				
				if(($result = $this->executeSQL(true)))
				{
					$replace_arr = array();
										
					$replace_arr['szName'] = $this->szName;
					$replace_arr['szEmail'] = $this->szEmail;
					$replace_arr['szPhone'] = $this->szPhone;
					$replace_arr['szComment'] = $this->szComment;
					
					createEmail(__IBORROWER_CONTACT_US__,$replace_arr,__STORE_CONTACT_EMAIL__,__IBORROWER_CONTACT_US_SUBJECT_,$replace_arr['szEmail']);
					return true;					
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * This function used to delete borrower account from the website
	 * @access public
	 * @param int
	 * @return boolean 
	 * @author Ajay
	 */
	 
	function deleteBorrowerAccount($idBorrower)
	{
		if($idBorrower>0)
		{
			$this->loadTyni($idBorrower);
			if($this->id<=0)
			{
				return false;
			}
			
			$query = "
	    		UPDATE	    		
	    			".__DBC_SCHEMATA_ARTIST__."
	    		SET
	    			isDeleted = '1'
	    		WHERE
	    			id = :idBorrower 
	    		AND
				    isDeleted = '0'
	    	";
			//echo "<br>".$query."<br>";
	    	if($this->preapreStatement($query))
			{
				$this->bindSQL(':idBorrower',(int)$this->id,PDO::PARAM_INT);				
				if(($result = $this->executeSQL()))
				{
					$kQuestionaire = new cQuestionaire();
					if($this->iUserType==__EMPOWER_MORTGAGE_USER_TYPE_BORROWER__) 
					{
						$kQuestionaire->deleteQuestionnaire($this->id);
						$this->deleteLoanApplications($this->id);
					}
					else
					{
						$this->deleteLoanApplications($this->id,'LO');
					}
					return true;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
		
	/**
	 * saving user cookie to the database
	 *
	 * @param $token
	 * @return bool
	 * @author Ajay
	 */
	function saveCookie($token)
	{
        $expireTime=time() + __COOKIE_TIME_CONSTANT__;
        $expireDate=date('Y-m-d',$expireTime);
     
		$query = "
			INSERT INTO
				".__DBC_SCHEMATA_USER_COOKIE__."
			(
				 szToken,
				 iUserType,
				 szUserName,
				 idUser,
                 dtCookieExpired,
                 dtCreatedOn
			)
			VALUES
			(
				 :szToken,
				 :iUserType,
				 :szUserName,
				 :idUser,
                 :dtCookieExpired,
                 now()   
			)
		";
		
		if($this->preapreStatement($query))
		{
			$this->bindSQL(':szToken',$token,PDO::PARAM_STR);
			$this->bindSQL(':iUserType',$this->iUserType,PDO::PARAM_INT);
			$this->bindSQL(':szUserName',$this->szUserName,PDO::PARAM_STR);
			$this->bindSQL(':idUser',$this->id,PDO::PARAM_INT);
			$this->bindSQL(':dtCookieExpired',$expireDate,PDO::PARAM_STR);
			
			if( ( $result = $this->executeSQL() ) )
			{
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	/**
	 * function to update last login of borrower
	 * @access public
	 * @param $key
	 * @return bool
	 * @author Ajay
	*/
	
	public function update_last_login($idUser)
	{
		if($idUser>0)
		{			
			$query="
				UPDATE
					".__DBC_SCHEMATA_ARTIST__."
				SET
					dtLastLogin = now()
				WHERE
					id = :idUser
				AND
					isDeleted = '0'
			";
			
			//echo "<br />".$query."<br />";
			
			if($this->preapreStatement($query))
			{
				$this->bindSQL(':idUser',$idUser,PDO::PARAM_INT);
				
				if( ( $result = $this->executeSQL() ) )
				{
					return true;
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
			}
		}
	}
	

	/**
	 * logout the user
	 *
	 * @access public
	 * @redirects to the base URL
	 * @author Ajay
	 */
	function logout( $id = 0)
	{
		if ((int)$id > 0)
		{
			$this->id = $id;
		}

		if ($this->id <= 0)
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() user id required!";
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}

		$query = "
    		DELETE FROM
    			".__DBC_SCHEMATA_USER_COOKIE__."
    		WHERE
    			idUser = :idUser 
    	";
    	if($this->preapreStatement($query))
		{
			$this->bindSQL(':idUser',(int)$this->id,PDO::PARAM_INT);
			
			if( ( $result = $this->executeSQL() ) )
			{
				$_SESSION['user_id']='';
				$_SESSION['iUserType'] = '';
				$_SESSION[__AUTH_TOKEN__]='';
	
				unset($_SESSION['user_id']);
				unset($_SESSION['iUserType']);
				unset($_SESSION[__AUTH_TOKEN__]);
	
				if ($_COOKIE[  __AUTH_TOKEN__ ])
				{
					$_COOKIE[ __AUTH_TOKEN__ ] = "";
					unset($_COOKIE[ __AUTH_TOKEN__ ]);
					setcookie( __AUTH_TOKEN__, NULL, time() - __COOKIE_TIME_CONSTANT__, __COOKIE_PATH__);
	            }
				return true;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	/**
	 * function to insert fogot key
	 * @access public
	 * @param $key
	 * @return bool
	 * @author Ajay
	*/
	public function insertForgotKey($data)
	{
		$this->set_szFPUserName(sanitize_all_html_input($data['szUserName']),true);
		
		if($this->error==true)
		{
			return false;
		}
		
		$iErrorCounter = 1;
		$iDJExists = 0;
		$kBuyer = new cBuyer();
		if(!$this->isUserNameExist($this->szUserName))
		{
			$iErrorCounter += 1;			
		}
		else
		{
			$iDJExists = 1 ;
			$szTableName = __DBC_SCHEMATA_ARTIST__ ;
		}
		
		if($iDJExists!=1)
		{
			if($kBuyer->isBuyerEmailExist($this->szUserName))
			{
				$iErrorCounter = 1;
				$szTableName = __DBC_SCHEMATA_BUYER__;
				$iBuyerExists = 1;
			}
			else
			{
				$iErrorCounter += 1;
			}
		}  
		
		if($iErrorCounter!=1)
		{			
			$this->addError('account_fp_username','This email does not registered with us. ');
			return false;
		}
		
		$forgot_key="f".create_login_key();		
		$this->szForgotPasswordKey = $forgot_key;		
		$dateTime=date("YmdHis");
		
		$query="
            UPDATE
            	".$szTableName."
            SET
            	szForgotPasswordKey = :szForgotPasswordKey,
            	szForgotDateTime = :szForgotDateTime 
			WHERE
				szEmail = :szUserName
			AND
				isDeleted = '0'
		";
				
		if($this->preapreStatement($query))
		{
			$this->bindSQL(':szForgotPasswordKey',$forgot_key,PDO::PARAM_STR);
			$this->bindSQL(':szForgotDateTime',$dateTime,PDO::PARAM_STR);
			$this->bindSQL(':szUserName',$this->szUserName,PDO::PARAM_STR);
			
			if(($result = $this->executeSQL()))
			{ 
				$forgot_key= $this->szForgotPasswordKey ;
				if($iDJExists==1)
				{
					$idUser = $this->idLoadedArtist;
					$szLinkValue=__URL_BASE__."/?action=forgotPassword&type=DJ&key=".$forgot_key; 
					$this->load($idUser);
				}
				else
				{
					$idUser = $kBuyer->idLoadedClient;
					$szLinkValue=__URL_BASE__."/?action=forgotPassword&type=US&key=".$forgot_key; 
					$kBuyer->load($idUser);
					
					$this->szFirstName = $kBuyer->szFirstName ;
					$this->szEmail = $kBuyer->szEmail ; 
				}
				
				$replace_arr = array();
				$replace_arr['szFirstName'] = $this->szFirstName ;
				
				$replace_arr['szForgotPasswordLink']="<a href='".$szLinkValue."'>Click here for new password</a>";
				$replace_arr['szForgotPasswordText'] = $szLinkValue ;
				$replace_arr['szEmail'] = $this->szEmail;
				$replace_arr['szSiteName'] = SITE_NAME ;
				
				createEmail(__FORGOT_PASSWORD__,$replace_arr,$this->szEmail,__FORGOT_PASSWORD_SUBJECT__,__STORE_SUPPORT_EMAIL__);
				return true;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to update because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	/**
	 * This function used to update new password of borrower
	 * @access public
	 * @param var $login_key
	 * @return array
	 * @author Ajay
	 */
	 
	public function updatePassword($data)
	{ 
		if(!empty($data))
		{		
			$szUserType = $data['szUserType'];
			if($szUserType=='US')
			{
				$kBuyer = new cBuyer();
				$szUserName = trim($data['szUserName']); 
				$kBuyer->isBuyerEmailExist($szUserName);
				
				$kBuyer->load($kBuyer->idLoadedClient); 
				
				$this->id = $kBuyer->id ;
				$this->szEmail = $kBuyer->szEmail ;
			}
			else
			{
				// for extra bit of security we have used following 2 steps.
				$szArtistKey = $data['szArtistKey'];
				$this->loadTyni(false,$szArtistKey);
			}
			
			
			//security step-1
			if($this->id<=0)
			{
				$this->addError('szNewPassword','Internal server error.');
				return false;
			}
						
			//security step-2
			if((trim($this->szEmail) != trim($data['szUserName'])) || ((int)$this->id<=0))
			{
				$this->addError('szNewPassword','Internal server error.');
				return false;
			} 
			$this->set_szNewPassword(trim(sanitize_all_html_input($data['szNewPassword'])));
			$this->set_szConNewPassword(trim(sanitize_all_html_input($data['szConNewPassword'])));
			
			if ($this->error == true)
			{
				return false;
			}	
			
			if(!empty($this->szNewPassword) && $this->szNewPassword!=$this->szConNewPassword)
			{
				$this->addError( "szConNewPassword" , "Password does not match");
				return false;
			}
			
			if($szUserType=='US')
			{
				$szTableName = __DBC_SCHEMATA_BUYER__ ;
			}
			else
			{
				$szTableName = __DBC_SCHEMATA_ARTIST__ ;
			}
			
			$query="
				UPDATE
					".$szTableName."
				SET
					szPassword = :szPassword ,
					szForgotPasswordKey = '',
					szForgotDateTime = ''
				WHERE
					id = '".(int)$this->id."'
				AND
					isDeleted = '0'
			";			
			//echo $query ;
			//die;
			if($this->preapreStatement($query))
			{
				$this->bindSQL(':szPassword',md5(trim($this->szNewPassword)),PDO::PARAM_STR);
				
				if( ( $result = $this->executeSQL() ) )
				{
					return true;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to update because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	/**
	 * This function used to get borrower id by forgot key
	 * @access public
	 * @param var $login_key
	 * @return array
	 * @author Ajay
	 */
	public function load_borrower_id_by_forgot_key($forgot_key,$szUserType=false)
	{
		if($forgot_key)
		{
			if($szUserType=='US')
			{
				$szTableName = __DBC_SCHEMATA_BUYER__ ;
			}
			else
			{
				$szTableName = __DBC_SCHEMATA_ARTIST__ ;
				$query_select = ", szArtistKey";
			}
			
			$query="
				SELECT 
					id,
					szEmail,				
					szForgotDateTime 
					$query_select
				FROM
					".$szTableName."
				WHERE
					szForgotPasswordKey = :szForgotPasswordKey
				AND
					isDeleted = '0'
			";
			//echo "<br /> ".$query;
			
			if($this->preapreStatement($query))
			{
				$this->bindSQL(':szForgotPasswordKey',$forgot_key,PDO::PARAM_STR);
				if( ( $result = $this->executeSQL() ) )
				{
					if($this->iNumRows>0)
					{
						$row = $this->getAssoc();
						return $row;			
					}
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	/**
	 * This function used to change forgot password
	 * @access public
	 * @param array
	 * @return bool
	 * @author Ajay
	 */
	public function changeBorrowerPassword($data)
	{
		if(is_array($data))
		{			
			$this->set_szOldPassword(trim(sanitize_all_html_input($data['szOldPassword'])));
			$this->set_szNewPassword(trim(sanitize_all_html_input($data['szNewPassword'])));
			$this->set_szConNewPassword(trim(sanitize_all_html_input($data['szConNewPassword'])));
			
			$userSessionData = array();
			$userSessionData = get_user_session_data();
		
			if($userSessionData['idUser']<=0)
			{
				$this->addError('szOldPassword','You must login to change your password.');
				return false;
			}
			
			if ($this->error == true)
			{
				return false;
			}	
			
			if(!empty($this->szNewPassword) && $this->szNewPassword!=$this->szConNewPassword)
			{
				$this->addError( "szConNewPassword" , "Password does not match");
				return false;
			}
			
			if(!$this->isValidPassword($this->szOldPassword))
			{
				$this->addError("szOldPassword", "Invalid Password.");
				return false;
			}
			
			$userSeesionAry = array();
			$userSeesionAry = get_user_session_data();
			
			if($userSessionData['iUserType']==__USER_TYPE_ARTIST__)
			{
				$szTableName = __DBC_SCHEMATA_ARTIST__ ;
			}
			else
			{
				$szTableName = __DBC_SCHEMATA_BUYER__ ;
			}
			
			$query="
            	UPDATE
            		".$szTableName."
            	SET
            		szPassword = :szPassword,
            		szForgotPasswordKey = '',
            		szForgotDateTime = ''
				WHERE
					id='".(int)$userSeesionAry['idUser']."'
				AND
					isDeleted = '0'
			";			
			if($this->preapreStatement($query))
			{
				$this->bindSQL(':szPassword',md5($this->szNewPassword),PDO::PARAM_STR);
				if( ( $result = $this->executeSQL() ) )
				{
					return true;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	/**
	 * This function used to chek given password is valid or not
	 * @access public
	 * @param array
	 * @return bool
	 * @author Ajay
	 */
	public function isValidPassword($szOldPassword)
	{	
		if(!empty($szOldPassword))
		{
			$this->set_szOldPassword(sanitize_all_html_input($szOldPassword));

			if ($this->error === true)
			{
				return false;
			}
			
			$userSeesionAry = array();
			$userSeesionAry = get_user_session_data();
			
			if($userSessionData['iUserType']==__USER_TYPE_ARTIST__)
			{
				$szTableName = __DBC_SCHEMATA_ARTIST__ ;
			}
			else
			{
				$szTableName = __DBC_SCHEMATA_BUYER__ ;
			}
			
			$query = "
				SELECT
					id
				FROM
					".$szTableName."
				WHERE
					szPassword = :szPassword
				AND
					id = :id
				AND
					iActive = '1'
				AND
					isDeleted = '0'
			";
			//echo "<br /> ".$query."<br/>";
			
			if($this->preapreStatement($query))
			{
				$userSessionData = array();
				$userSessionData = get_user_session_data();
				$id = (int)$userSessionData['idUser'];
				
				$this->bindSQL(':szPassword',md5($this->szOldPassword),PDO::PARAM_STR);
				$this->bindSQL(':id',$id,PDO::PARAM_INT);
				
				if( ( $result = $this->executeSQL() ) )
				{
					if($this->iNumRows>0)
					{
						return true;
					}
				}
				else
				{					
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{	
			return false;
		}
	}
	
	/**
	 * This function used to logs user into our system if there is any cookie saved.
	 * @access public
	 * @return bool
	 * @author Ajay
	 */
	 
	public function loginFromCookie()
	{
		// check cookie variable
		if (isset($_COOKIE[ __AUTH_TOKEN__ ]))
		{
			$query = "
				SELECT
					iUserType,
					idUser 	  
				FROM
					". __DBC_SCHEMATA_USER_COOKIE__ ."
				WHERE
					szToken  = :szToken
			";
			
			//echo "<br /> ".$query."<br />";
			
			if($this->preapreStatement($query))
			{
				$szToken = $_COOKIE[ __AUTH_TOKEN__ ] ;
				$this->bindSQL(':szToken',$szToken,PDO::PARAM_STR);
				
				if(($result = $this->executeSQL()))
				{
					if($this->iNumRows>0)
					{
						while($row = $this->getAssoc())
						{
							if($this->loadTyni((int)$row['idUser']))
							{                            
	                            setcookie(  __AUTH_TOKEN__ , $_COOKIE[ __AUTH_TOKEN__ ], time() + __COOKIE_TIME_CONSTANT__, __COOKIE_PATH__);
	                            
	                            $expireTime=time() + __COOKIE_TIME_CONSTANT__;
	                            $expireDate=date('Y-m-d',$expireTime);         
	                            
	                            $query_update="
	                                UPDATE
	                                    ". __DBC_SCHEMATA_USER_COOKIE__ ."
	                                SET
	                                    dtCookieExpired = :dtCookieExpired 
	                                WHERE
	                                    szToken  = :szToken
	                                AND
	                                    idUser = :idUser
	                            ";
	                             //echo "<br /> ".$query_update."<br />";
	                             
	                            if($this->preapreStatement($query_update))
								{
									$szToken = $_COOKIE[ __AUTH_TOKEN__ ] ;
									$idBorrower = (int)$this->id ;
									
									$this->bindSQL(':dtCookieExpired',$expireDate,PDO::PARAM_STR);
									$this->bindSQL(':szToken',$szToken,PDO::PARAM_STR);
									$this->bindSQL(':idUser',$idBorrower,PDO::PARAM_INT);
									
									if( ( $result = $this->executeSQL() ) )
									{	                            
			                            //create user email & userid sessions
										$_SESSION['user_id'] = $this->id;
										$_SESSION['iUserType'] = $this->iUserType;
										
			                            $this->userLoginLog(true);
			                            
										$token = sha1(uniqid(rand(), true));
										$_SESSION[ __AUTH_TOKEN__ ] = $token;
										
										return true;
									}								
								}	
							}
							else
							{
								return false;
							}
						}
					}
				}
				else
				{
					$this->error = true;
					$szErrorMessage = "Login session expired. Please login below.";
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");

					if ($_COOKIE[  __AUTH_TOKEN__ ])
					{
						$_COOKIE[ __AUTH_TOKEN__ ] = "";
						unset($_COOKIE[ __AUTH_TOKEN__ ]);
						setcookie( __AUTH_TOKEN__, NULL, time() - __COOKIE_TIME_CONSTANT__, __COOKIE_PATH__);
					}

					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "input", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	/**
	 * This function used to update account status
	 * @access public
	 * @param int $idBorrower
	 * @return bool
	 * @author Ajay
	*/
	public function update_account_status($idUser,$iUserType)
	{
		if($idUser>0)
		{
			if($iUserType==__USER_TYPE_ARTIST__)
			{
				$szTable = __DBC_SCHEMATA_ARTIST__ ;
			}
			else
			{
				$szTable = __DBC_SCHEMATA_BUYER__ ;
			}
			
			$query="
				UPDATE 
					".$szTable."
				SET
					szConfirmationKey = '',
					iVerified = '1'
				WHERE
					id = :id
				AND
					isDeleted = '0'
			";
			if($this->preapreStatement($query))
			{
				$this->bindSQL(':id',(int)$idUser,PDO::PARAM_INT);
				
				if( ( $result = $this->executeSQL() ) )
				{
					return true;
				}			
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function generateUniqueToken($number)
	{
	    $arr = array('a', 'b', 'c', 'd', 'e', 'f',
	                 'g', 'h', 'i', 'j', 'k', 'l',
	                 'm', 'n', 'o', 'p', 'r', 's',
	                 't', 'u', 'v', 'x', 'y', 'z',
	                 'A', 'B', 'C', 'D', 'E', 'F',
	                 'G', 'H', 'I', 'J', 'K', 'L',
	                 'M', 'N', 'O', 'P', 'R', 'S',
	                 'T', 'U', 'V', 'X', 'Y', 'Z',
	                 '1', '2', '3', '4', '5', '6',
	                 '7', '8', '9', '0');
	    $token = "";
	    for ($i = 0; $i < $number; $i++) {
	        $index = rand(0, count($arr) - 1);
	        $token .= $arr[$index];
		}
		return $token ;
	}
	/**
	 * This function used to get profile id by email key
	 * @access public
	 * @param var $email_key
	 * @return array
	 * @author Anil
	 */
	public function load_user_id_by_confirmation_key($szConfirmKey)
	{
		if($szConfirmKey)
		{
			$query="
				SELECT 
					id
				FROM
					".__DBC_SCHEMATA_ARTIST__."
				WHERE
					szConfirmationKey = :szConfirmationKey
				AND
					isDeleted = '0'
			";			
			
			if($this->preapreStatement($query))
			{
				$this->bindSQL(':szConfirmationKey',$szConfirmKey,PDO::PARAM_STR);
				if( ( $result = $this->executeSQL() ) )
				{
					if($this->iNumRows > 0)
					{
						$row = $this->getAssoc();
						$row['iUserType'] = __USER_TYPE_ARTIST__ ;
						return $row;			
					}
					else
					{
						$ret_ary = array();
						$ret_ary = $this->load_buyer_id_by_confirmation_key($szConfirmKey);
						if(!empty($ret_ary))
						{
							return $ret_ary ;
						}
						else
						{
							return false;
						}
					}
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	/**
	 * This function used to get profile id by email key
	 * @access public
	 * @param var $email_key
	 * @return array
	 * @author Ajay
	 */
	public function load_buyer_id_by_confirmation_key($szConfirmKey)
	{
		if($szConfirmKey)
		{
			$query="
				SELECT 
					id
				FROM
					".__DBC_SCHEMATA_BUYER__."
				WHERE
					szConfirmationKey = :szConfirmationKey
				AND
					isDeleted = '0'
			";			
			
			if($this->preapreStatement($query))
			{
				$this->bindSQL(':szConfirmationKey',$szConfirmKey,PDO::PARAM_STR);
				if( ( $result = $this->executeSQL() ) )
				{
					if($this->iNumRows > 0)
					{
						$row = $this->getAssoc();
						$row['iUserType'] = __USER_TYPE_CLIENT__ ;
						return $row;			
					}
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	/**
	 * This function used to prepare data for Admin dashboard graph
	 * @access public
	 * @param int
	 * @return boolean 
	 * @author Ajay
	 */
	function prepareDataForDashBoardGraph($data)
	{
		if(!empty($data))
		{
			$borrowerListingAry = array();
			$borrowerListingAry = $this->getAllActiveBorrower(false,false,true,$data);
			
			$loanApplicationAry = array();
			$loanApplicationAry = $this->getAllLoanApplicationForAdmin($data,true);
			$fromDate = strtotime($data['dtFromDate']);
			$toDate = strtotime($data['dtToDate']);
			$counter = $fromDate ;
			
			$ret_ary = array();
			while($counter < $toDate)
			{
				$key = date('Ym01',$counter);
				if(empty($borrowerListingAry[$key]))
				{
					$ret_ary[$key]['iTotalNumBorrower'] = 0;
					$ret_ary[$key]['iTotalNumLoanOfficer'] = 0 ;
				}	
				else
				{
					$ret_ary[$key]['iTotalNumBorrower'] = $borrowerListingAry[$key]['iTotalNumBorrower'] ;
					$ret_ary[$key]['iTotalNumLoanOfficer'] = $borrowerListingAry[$key]['iTotalNumLoanOfficer'] ;
				}
				
				if(empty($loanApplicationAry[$key]))
				{
					$ret_ary[$key]['iTotalNumApplication'] = 0 ;
				}
				else
				{
					$ret_ary[$key]['iTotalNumApplication'] = $loanApplicationAry[$key]['iTotalNumApplication'] ;
				}			
				$counter = $counter + (30*24*60*60);
			}
			
			if(!empty($ret_ary))
			{
				ksort($ret_ary);
			}		
			return $ret_ary ;
		}
	}
	
	function getAllArtType($iCategoryLevel=false,$idNotEqualTo=false)
	{
		if($iCategoryLevel>0)
		{
			$query_and = ' AND iCategoryLevel = :iCategoryLevel ';
		}
		
		if($idNotEqualTo>0)
		{
			$query_and .= ' AND id != :idArtType ';
		}
		
		$query="
			SELECT
				id,
				szArtName,
				szArtSeoName,
				iCategoryLevel,
				szDescription
			FROM
				".__DBC_SCHEMATA_ART_TYPE__."
			WHERE
				isDeleted = '0'
			AND
				iActive = '1'
			$query_and
			ORDER BY
				szDescription ASC, szArtName ASC
		";
		if($this->preapreStatement($query))
		{
			if($iCategoryLevel>0)
			{
				$this->bindSQL(':iCategoryLevel',(int)$iCategoryLevel,PDO::PARAM_INT);
			}			
			
			if($idNotEqualTo>0)
			{
				$this->bindSQL(':idArtType',(int)$idNotEqualTo,PDO::PARAM_INT);
			}
			
			if(($result = $this->executeSQL()))
			{
				if($this->iNumRows > 0)
				{
					$ret_ary = array();
					$ctr = 0;
					
					while($row = $this->getAssoc())
					{
						$ret_ary[$ctr] = $row;
						$ctr++;
					}
					return $ret_ary;			
				}
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	

	/**
	 * This function is used to fetch all category from database.
	 * @access public
	 * @param string
	 * @return boolean 
	 * @author Ajay
	 */

	public function getAllArts($iCategoryLevel=false,$ret_id_ary=false,$pageLimit=false,$paging=false,$searchAry=array())
	{	
		if($searchAry['szSearchString']=='Search by Art Name')
		{
			$searchAry['szSearchString'] = '';
		}	
		
		if(!empty($searchAry['szSearchString']))
		{
			//$query_and .= " AND szArtName LIKE :szArtName1 ";
		}	 
		
		if(!empty($searchAry['szArtName']))
		{
			$query_and .= " AND szArtName LIKE :szArtName ";
		}		
		
		if((int)$searchAry['idArt']>0)
		{
			$query_and .= " AND id = '".(int)$searchAry['idArt']."' ";
		}
		
		if($paging>0)
		{
			$query_limit = " Limit $pageLimit,".ART_TYPE_PER_PAGE ;	
		}
		
		if(!empty($searchAry['szSortFeild']) && !empty($searchAry['szSortOrder']))
		{
			$szSortFiled = sanitize_all_html_input(trim($searchAry['szSortFeild']));
			$szSortOrder = sanitize_all_html_input(trim($searchAry['szSortOrder']));
			
			$query_order_by = " ORDER BY ".$szSortFiled." ".$szSortOrder;
		}
		else
		{
			$query_order_by = " ORDER BY szArtName ASC";
		}
		
		$query="
			SELECT
				id,
				szArtName,
				szArtSeoName,
				iCategoryLevel,
				szDescription,
				iActive
			FROM
				".__DBC_SCHEMATA_ART_TYPE__."
			WHERE
				isDeleted = '0'
			  $query_and
			  $query_order_by
			$query_limit
		";	
		//echo "<br> ".	$query ;
		if($this->preapreStatement($query))
		{
			if($iCategoryLevel !== 'ALL')
			{
				$this->bindSQL(':iCategoryLevel',(int)$iCategoryLevel,PDO::PARAM_INT);
			}	
			if(!empty($searchAry['szArtName']))
			{
				$this->bindSQL(':szArtName',"%".trim($searchAry['szArtName'])."%",PDO::PARAM_STR);
			}			
			if(!empty($searchAry['szSearchString']))
			{
				//$this->bindSQL(':szArtName1',"%".trim($searchAry['szSearchString'])."%",PDO::PARAM_STR);
			}
			
			if( ( $result = $this->executeSQL() ) )
			{
				if($this->iNumRows>0)
				{
					$ret_ary = array();
					$ctr = 0;
					while($row = $this->getAssoc())
					{
						if($ret_id_ary)
						{
							$ret_ary[$ctr] = $row['id'];
						}
						else
						{
							$ret_ary[$ctr] = $row;
						}						
						$ctr++;
					}
					return $ret_ary;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	/**
	 * This function is used to add all category from database.
	 * @access public
	 * @param string
	 * @return boolean 
	 * @author Ajay
	 */
	function addArtType($data)
	{
		if(!empty($data))
		{
			$this->validateArtTypeDetails($data);
			if($this->error==true)
			{
				return false;
			}
			$this->szArtSeoName = CleanTitle($this->szArtName);			
			$query="
				INSERT INTO
					".__DBC_SCHEMATA_ART_TYPE__."
				(
					szArtName,
					szArtSeoName,
					iCategoryLevel,
					szDescription,
					dtCreatedOn,
					iActive
				)
				VALUES
				(
					:szArtName,
					:szArtSeoName,
					:iCategoryLevel,
					:szDescription,
					now(),
					1
				)
			";	
			
			if($this->preapreStatement($query))
			{				
				$this->bindSQL(':szArtName',$this->szArtName,PDO::PARAM_STR);
				$this->bindSQL(':szArtSeoName',$this->szArtSeoName,PDO::PARAM_STR);			
				$this->bindSQL(':iCategoryLevel',(int)$this->iCategoryLevel,PDO::PARAM_INT);	
				$this->bindSQL(':szDescription',$this->szDescription,PDO::PARAM_STR);				
			
				if(($result = $this->executeSQL()))
				{				
					$idArtType = $this->iLastInsertID ;
					$this->idArtTypeLastAdded = $idArtType ;			
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}		
	}
	
	/**
	 * This function is used to add all category from database.
	 * @access public
	 * @param string
	 * @return boolean 
	 * @author Ajay
	 */
	function updateArtType($data,$idArtType)
	{
		if(!empty($data) && $idArtType>0)
		{
			$searchAry = array();
			$priaryArtAry = array();
			$searchAry['idArt'] = $idArtType ;
			$priaryArtAry = $this->getAllArts(false,false,false,false,$searchAry);
			
			if(empty($priaryArtAry))
			{
				$this->addError('szArtName','Music Generes doesnot exists.');
				return false;
			}
			$this->validateArtTypeDetails($data);
			if($this->error==true)
			{
				return false;
			}
			$this->szArtSeoName = CleanTitle($this->szArtName);		
				
			$query="
				UPDATE
					".__DBC_SCHEMATA_ART_TYPE__."
				SET
					szArtName = :szArtName,
					szArtSeoName = :szArtSeoName,
					iCategoryLevel = :iCategoryLevel,
					szDescription = :szDescription,
					dtUpdatedOn = now()
				WHERE
					id = :idArtType				
			";
			
			if($this->preapreStatement($query))
			{				
				$this->bindSQL(':idArtType',(int)$idArtType,PDO::PARAM_INT);
				$this->bindSQL(':szArtName',$this->szArtName,PDO::PARAM_STR);
				$this->bindSQL(':szArtSeoName',$this->szArtSeoName,PDO::PARAM_STR);			
				$this->bindSQL(':iCategoryLevel',(int)$this->iCategoryLevel,PDO::PARAM_INT);	
				$this->bindSQL(':szDescription',$this->szDescription,PDO::PARAM_STR);				
			
				if(($result = $this->executeSQL()))
				{						
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}		
	}
	
	function validateArtTypeDetails($data)
	{
		if(!empty($data))
		{
			$this->set_szArtName(sanitize_all_html_input($data['szArtName']));
			$this->set_iCategoryLevel(sanitize_all_html_input($data['iCategoryLevel']));
			$this->set_iDisplayOrder(sanitize_all_html_input($data['szDescription']),true);
			
			if($this->error==true)
			{
				return false;
			}
			else
			{
				$this->szDescription = $this->iDisplayOrder ;
				return true;
			}
		}
	}
	


	/**
	 * This function used to toggle Art type status
	 * @access public
	 * @param int
	 * @return boolean 
	 * @author Ajay
	 */
	 
	function toggleArtType($idArtType,$iValue)
	{
		if($idArtType>0)
		{
			$searchAry = array();
			$priaryArtAry = array();
			$searchAry['idArt'] = $idArtType ;
			$priaryArtAry = $this->getAllArts(false,false,false,false,$searchAry);
			
			if(empty($priaryArtAry))
			{
				$this->addError('szArtName','Music Genre doesnot exists.');
				return false;
			}
			
			$query = "
	    		UPDATE	    		
	    			".__DBC_SCHEMATA_ART_TYPE__."
	    		SET
	    			iActive = :iActive
	    		WHERE
	    			id = :idArtType
	    		AND
				    isDeleted = '0'
	    	";
			//echo "<br>".$query."<br>";
	    	if($this->preapreStatement($query))
			{
				$this->bindSQL(':iActive',(int)$iValue,PDO::PARAM_INT);	
				$this->bindSQL(':idArtType',(int)$idArtType,PDO::PARAM_INT);				
				if(($result = $this->executeSQL()))
				{
					return true;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	/**
	 * This function used to delete Art type from system
	 * @access public
	 * @param int
	 * @return boolean 
	 * @author Ajay
	 */
	function deleteArtType($idArtType)
	{	
		if($idArtType>0)
		{
			$searchAry = array();
			$priaryArtAry = array();
			$searchAry['idArt'] = $idArtType ;
			$priaryArtAry = $this->getAllArts(false,false,false,false,$searchAry);
			
			if(empty($priaryArtAry))
			{
				$this->addError('szArtName','Music Genre doesnot exists.');
				return false;
			}
			
			$query = "
	    		UPDATE	    		
	    			".__DBC_SCHEMATA_ART_TYPE__."
	    		SET
	    			isDeleted = 1
	    		WHERE
	    			id = :idArtType
	    		AND
				    isDeleted = '0'
	    	";
			//echo "<br>".$query."<br>";
	    	if($this->preapreStatement($query))
			{
				$this->bindSQL(':idArtType',(int)$idArtType,PDO::PARAM_INT);				
				if(($result = $this->executeSQL()))
				{
					return true;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}

	function curl_get_file_contents($URL) 
	{
		$c = curl_init();
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($c, CURLOPT_URL, $URL);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, FALSE);
		$contents = curl_exec($c);
		$err  = curl_getinfo($c,CURLINFO_HTTP_CODE);
		curl_close($c);
		if ($contents) return $contents;
		else return FALSE;
	 }
	 
	 function exportCsvDataToCsv($searchAry)
	 {
	 	if(!empty($searchAry))
	 	{
	 		$artistsListingAry = array();
			$artistsListingAry = $this->getAllArtistDetails(false,false,0,$searchAry);
			
			$csvHeaderAry = array();
			$csvHeaderAry[0] = 'Sr No.';
			$csvHeaderAry[1] = 'DJ Full Name';
			$csvHeaderAry[2] = 'DJ Name';
			$csvHeaderAry[3] = 'D.O.B';
			$csvHeaderAry[4] = 'Primary Music Genre';
			$csvHeaderAry[5] = 'Secondary Music Genre';
			$csvHeaderAry[6] = 'Email';			
			$csvHeaderAry[7] = 'Phone';
			$csvHeaderAry[8] = 'Alternate Phone';
			$csvHeaderAry[9] = 'Street Address';
			$csvHeaderAry[10] = 'Postcode';			
			$csvHeaderAry[11] = 'City';
			$csvHeaderAry[12] = 'County';			
			$csvHeaderAry[13] = 'Base Fee';
			$csvHeaderAry[14] = 'Fee Per Hour';
			$csvHeaderAry[15] = 'Distance (miles) willing to travel';
			$csvHeaderAry[16] = 'Status';			
			$csvHeaderAry[17] = 'Verified';
			$csvHeaderAry[18] = 'Featured';
			$csvHeaderAry[19] = 'Registration Date';
			$csvHeaderAry[20] = 'IP Address';
			$csvHeaderAry[21] = 'Description';
			$csvHeaderAry[22] = '#Booking';
			$csvHeaderAry[23] = 'Membership Type';
			$csvHeaderAry[24] = 'Maximum Hours'; 
			
			$csvString = implode(",",$csvHeaderAry)."\n";
			
			if(!empty($artistsListingAry))
			{
				$ctr=1;
				foreach($artistsListingAry as $artistsListingArys)
				{
					if($artistsListingArys['iActive']==1)
					{
						$szStatus = 'Active';
					}
					else
					{
						$szStatus = 'Inactive';
					}
					if($artistsListingArys['iVerified']==1)
					{
						$iVerified = 'Yes';
					}
					else
					{
						$iVerified = 'No';
					}
					if($artistsListingArys['iFeatured']==1)
					{
						$iFeatured = 'Yes';
					}
					else
					{
						$iFeatured = 'No';
					}
					  
					if($artistsListingArys['iMemberShipType']==1)
					{
						$szMembershipType = 'BASIC';
					}
					else if($artistsListingArys['iMemberShipType']==2)
					{
						$szMembershipType = 'PREMIUM';
					}
					else if($artistsListingArys['iMemberShipType']==3)
					{
						$szMembershipType = 'MANAGEMENT';
					}
					$csvDataAry = array();
					$csvDataAry[0] = $ctr;
					$csvDataAry[1] = str_replace(",","",$artistsListingArys['szFirstName']." ".$artistsListingArys['szLastName']);
					$csvDataAry[2] = str_replace(",","",$artistsListingArys['szArtistName']);
					$csvDataAry[3] = date('d/m/Y',strtotime($artistsListingArys['dtDateOfBirth']));
					$csvDataAry[4] = $artistsListingArys['szPrimaryArtType'];
					$csvDataAry[5] = $artistsListingArys['szSecondaryArtType'];
					$csvDataAry[6] = $artistsListingArys['szEmail'];		
					$csvDataAry[7] = $artistsListingArys['szPhone'];
					$csvDataAry[8] = $artistsListingArys['szAlternatePhone'];
					$csvDataAry[9] = str_replace(",","",$artistsListingArys['szAddress']." ".$artistsListingArys['szAddress2']);
					$csvDataAry[10] = $artistsListingArys['szPostcode'];			
					$csvDataAry[11] = str_replace(",","",$artistsListingArys['szCity']);
					$csvDataAry[12] = $artistsListingArys['szCounty'];		
					$csvDataAry[13] = $artistsListingArys['fBaseFee'];
					$csvDataAry[14] = $artistsListingArys['fFeePerHour'];
					$csvDataAry[15] = $artistsListingArys['iDistanceWillingToTravel'];
					$csvDataAry[16] = $szStatus;			
					$csvDataAry[17] = $iVerified;
					$csvDataAry[18] = $iFeatured;
					$csvDataAry[19] = date('d/m/Y',strtotime($artistsListingArys['dtCreatedOn']));
					$csvDataAry[20] = $artistsListingArys['szIPAddress'];
					$csvDataAry[21] = str_replace(",","",$artistsListingArys['szDescription']);
					$csvDataAry[22] = $artistsListingArys['iNumBookings']; 
					$csvDataAry[23] = $szMembershipType;
					$csvDataAry[24] = $artistsListingArys['iMaxPerformanceLenght'];
					
					$csvString .= implode(",",$csvDataAry)." \n";
					$ctr++;
				}
			}
					
			$szUniqueKey = time()."_".mt_rand(0,1000);
			$szFileName = "af-artists-".$szUniqueKey.".csv" ;
			$file_write=fopen(__APP_PATH_TEMP_CSV__."/".$szFileName, "w");
			
    		fwrite($file_write, $csvString); 
    		fclose($file_write);
			return $szFileName;
	 	}
	 }
	 
	 function addArtistStep1($data)
	 {
	 	if(!empty($data))
	 	{
	 		$this->set_szFirstName(sanitize_all_html_input($data['szFirstName']));
			$this->set_szLastName(sanitize_all_html_input($data['szLastName']));
			$this->set_szArtistName(sanitize_all_html_input($data['szArtistName']));
			$this->set_szEmail(sanitize_all_html_input($data['szEmail']));
			
			if(!empty($data['szPassword']))
			{
				$this->set_szPassword(sanitize_all_html_input($data['szPassword']));
			}
			else
			{
				$this->addError('szPassword_1','Password is required.');
			}
			
			$this->set_szCPassword(sanitize_all_html_input($data['szCPassword']));
			
			if($this->error==true)
			{
				return false;
			}
			
	 		if($this->isUserNameExist($this->szEmail))
			{
				$this->addError('szEmail_1','A user with this email already registered with us.');
				return false;
			}
	 		if($this->isUserNameExist(false,false,false,$this->szArtistName))
			{
				$this->addError('szArtistName_1','We already have a DJ with this name. Please choose another.');
				return false;
			}
	 		$kBuyer = new cBuyer();
			if($kBuyer->isBuyerEmailExist($this->szEmail))
			{
				$this->addError('szEmail_1','A user with this email already registered with us.');
				return false;
			}
						
	 		$this->szArtistKey = CleanTitle($this->szArtistName);
	 		
			if($this->szPassword != $this->szCPassword)
			{
				$this->addError('szCPassword_1','Password does not match');
				return false;
			}
			
			$szConfirmKey = md5($this->szEmail."_".date('YmdHis'));
			$this->szConfirmationKey = $szConfirmKey;

			$this->iSex = $data['iSex'];
			$this->dtDateOfBirth = $data['dtDateOfBirth'];
			
			$kBooking = new cBooking();
			$this->szArtistNumber = $kBooking->generateBookingNumber(2);
			$iMaxNumBooking = $kBooking->iMaxNumBooking ;
			
			//$this->szArtistKey = md5($this->szEmail."_".$kBooking->szArtistNumber."_".mt_rand(0,1000));
						
			$query = "
				INSERT INTO
					".__DBC_SCHEMATA_ARTIST__."
				(
					szArtistKey,
					szArtistNumber,
					szFirstName,
					szLastName,
					szArtistName,
					szEmail,
					szPassword,
					szReferer,
					szIPAddress,
					szUserAgent,
					iSex,
					dtDateOfBirth,
					szConfirmationKey,
					dtCreatedOn,
					iActive,
					iStep1Completed,
					dtVerificationLinkSent
				)
				VALUES
				(
					:szArtistKey,
					:szArtistNumber,
					:szFirstName,
					:szLastName,
					:szArtistName,
					:szEmail,
					:szPassword,
					:szReferer,
					:szIPAddress,
					:szUserAgent,
					:iSex,
					:dtDateOfBirth,
					:szConfirmationKey,
					now(),
					'1',
					'1',
					now()
				)
			";
	 	
			if($this->preapreStatement($query))
			{				
				$this->bindSQL(':szArtistKey',$this->szArtistKey,PDO::PARAM_STR);
				$this->bindSQL(':szArtistNumber',$this->szArtistNumber,PDO::PARAM_STR);
				$this->bindSQL(':szFirstName',$this->szFirstName,PDO::PARAM_STR);
				$this->bindSQL(':szLastName',$this->szLastName,PDO::PARAM_STR);
				$this->bindSQL(':szArtistName',$this->szArtistName,PDO::PARAM_STR);
				$this->bindSQL(':dtDateOfBirth',$this->dtDateOfBirth,PDO::PARAM_STR);
				$this->bindSQL(':szEmail',$this->szEmail,PDO::PARAM_STR);
				$this->bindSQL(':iSex',$this->iSex,PDO::PARAM_STR);
				$this->bindSQL(':szConfirmationKey',$this->szConfirmationKey,PDO::PARAM_STR);				
				$this->bindSQL(':szPassword',md5($this->szPassword),PDO::PARAM_STR);
				$this->bindSQL(':szReferer',__HTTP_REFERER__,PDO::PARAM_STR);				
				$this->bindSQL(':szIPAddress',__REMOTE_ADDR__,PDO::PARAM_STR);
				$this->bindSQL(':szUserAgent',__HTTP_USER_AGENT__,PDO::PARAM_STR);
				
				if(($result = $this->executeSQL(true)))
				{				
					$idArtist = $this->iLastInsertID ;					
					$this->idArtistLastAdded = $idArtist ;
					
					$replace_arr = array();
					$szLinkValue=__URL_BASE__."/?action=confirmEmail&key=".$this->szConfirmationKey;							
					
					$replace_arr['szLink']="<a href='".$szLinkValue."'>Click here to verify account </a>";
					$replace_arr['szHttpsLink'] = $szLinkValue ;
					$replace_arr['szEmail'] = $this->szEmail;
					$replace_arr['szSiteUrl'] = __URL_BASE__ ;	
					$replace_arr['szUserName'] = $this->szEmail;
					$replace_arr['szPassword'] = $this->szPassword;
					$replace_arr['szSiteName'] = SITE_NAME ;
					
					$bookingIdLogsAry = array();
					$bookingIdLogsAry['iMaxNumBooking'] = $iMaxNumBooking+1 ;
					$bookingIdLogsAry['szBookingNumber'] = $this->szArtistNumber;
					$bookingIdLogsAry['iType'] = 2 ;
					$kBooking->addBookingIDLogs($bookingIdLogsAry);
					
					$autologinAry = array();
					$autologinAry['szUserName'] = $this->szEmail ;
					$autologinAry['szPassword'] = $this->szPassword ;
					$autologinAry['iUserType'] = __USER_TYPE_ARTIST__ ;
					$this->autoLogin($autologinAry);	
					
					createEmail('__CREATE_ARTIST_ACCOUNT__',$replace_arr,$this->szEmail,__CREATE_USER_ACCOUNT_SUBJECT__,__STORE_SUPPORT_EMAIL__);
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	 	}
	 }
	 
	 function getDjAccountDataForDashboard($data,$szType='WEEKLY')
	 {
	 	if(!empty($data))
	 	{
	 		$searchAry = array();
	 		$searchAry['dtRegistrationDateFrom'] = $data['dtStartDate'];
	 		$searchAry['dtRegistrationDateTo'] = $data['dtEndDate'];
	 		$searchAry['iGroupBy'] = 1;
	 		$searchAry['szView'] = $szType ; 
	 		
	 		$artistDetailsAry = array();
	 		$artistDetailsAry = $this->getAllArtistDetails(true,false,false,$searchAry);
	 			 		
	 		$bookingRevenueAry = array();
	 		$bookingRevenueAry = $this->getBookingRevenue($data,false,false,$szType);
		
			$fromDate = strtotime($data['dtStartDate']);
			$toDate = strtotime($data['dtEndDate']);
			$counter = $fromDate ;
					
			$graph_data_ary = array();
	
		 	while($counter <= $toDate)
			{
				if($szType=='WEEKLY')
				{
					$key = date('Ymd',$counter);
				}
				else
				{
					$key = date('Ym01',$counter);
				}
				
				if(!empty($artistDetailsAry[$key]))
					$graph_data_ary[$key]['iNumDJRegistered'] = $artistDetailsAry[$key]['iNumArtist'];	
				else
					$graph_data_ary[$key]['iNumDJRegistered'] = 0;	
				
				if(!empty($bookingRevenueAry[$key]))
				{
					$graph_data_ary[$key]['fTotalBookingRevenue'] = $bookingRevenueAry[$key]['fTotalDailyEarning'];
					$graph_data_ary[$key]['fTotalBookingDeposit'] = $bookingRevenueAry[$key]['fTotalBookingDeposit'];
				}
				else
				{
					$graph_data_ary[$key]['fTotalBookingRevenue'] = 0;
					$graph_data_ary[$key]['fTotalBookingDeposit'] = 0;
				}
				if($szType=='WEEKLY')
				{
					$counter = $counter + (24*60*60);
				}
				else
				{
					$counter = $counter + (24*60*60*30);
				} 
			}
	 		return $graph_data_ary ;
	 	}
	 }
	 
	 function getBookingRevenue($data,$get_total=false,$iWeeklyView=false,$szType=false)
	 {
	 	if(!empty($data))
	 	{
	 		if($data['idArtist'])
			{
				$query_and .=" AND idArtist = '".(int)$data['idArtist']."' ";
			}
			if(!empty($data['dtStartDate']) && !empty($data['dtEndDate']))
			{
				if($iWeeklyView)
				{
					$query_and .= " AND 
							DATE(dtPaymentDate) >= '".$data['dtStartDate']."'
						AND 
							DATE(dtPaymentDate) <= '".$data['dtEndDate']."'
					";
				}
				else
				{
					$query_and .= " AND 
							DATE(dtPaymentDate) >= '".$data['dtStartDate']."'
						AND 
							DATE(dtPaymentDate) <= '".$data['dtEndDate']."'			
						GROUP BY 	
							DATE(dtPaymentDate)	
					";
				}
			}
			
			$query="
				SELECT
					count(id) as iTotalDailyBookings,
					sum(fTotalBookingAmount) as fTotalDailyEarning,
					sum(fReferalAmount) as fTotalAdminEarning,
					sum(fBookingDeposit) as fTotalBookingDeposit,
					dtPaymentDate,
					dtStartDate 
				FROM
					".__DBC_SCHEMATA_BOOKING__."
				WHERE
					iActive = 1		
				AND
					iBookingStatus = '".__BOOKING_STATUS_CONFIRMED__."'
				$query_and		
			";
			//echo " <br><br> query: ".$query ;
		 	if($this->preapreStatement($query))
			{
				if(($result = $this->executeSQL()))
				{
					if($this->iNumRows>0)
					{
						if($get_total || $iWeeklyView)
						{
							$ret_ary = array();
							$this->set_szPdoMode(PDO::FETCH_ASSOC);
							$row = $this->getAssoc();
							$ret_ary = $row;
						}
						else
						{
							$ret_ary = array();
							$ctr = 0;
							$this->set_szPdoMode(PDO::FETCH_ASSOC);						
							while($row = $this->getAssoc())
							{					
								if(!empty($row['dtPaymentDate']) && $row['dtPaymentDate']!='0000-00-00 00:00:00')
								{
									if($szType=='MONTHLY')
									{
										$key = date('Ym01',strtotime($row['dtPaymentDate']));
									}
									else
									{
										$key = date('Ymd',strtotime($row['dtPaymentDate']));
									}
									$ret_ary[$key] = $row;
									$ctr++;		
								}	
							}
						}
						return $ret_ary ;		
					}
					else
					{
						return false;
					}
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
		 	}
		 	else
		 	{
		 		$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
		 	}
		 }
	 }

	function getReferalRevenueDataForAdminDashboard($data)
	{
	 	if(!empty($data))
	 	{	 
	 		$iMonthlyWeeklyView = 1;
			$fromDate = strtotime($data['dtStartDate']);
			$toDate = strtotime($data['dtEndDate']);
			$counter = $fromDate ;
					
			$graph_data_ary = array();
	
		 	while($counter <= $toDate)
			{
				$key = date('Ym01',$counter);
				$dtFirstDateOfMonth = date('Ym01',$counter);
				$dtLastDateOfMonth = date('Ymt',$counter);
				
				$monthlyDataAry = array();
				$monthlyDataAry['dtStartDate'] = $dtFirstDateOfMonth ;
				$monthlyDataAry['dtEndDate'] = $dtLastDateOfMonth;
				
				$bookingRevenueAry = array();
	 			$bookingRevenueAry = $this->getBookingRevenue($monthlyDataAry,false,true);
	 			
				if(!empty($bookingRevenueAry))
					$graph_data_ary[$key]['fTotalBookingRevenue'] = $bookingRevenueAry['fTotalAdminEarning'];
				else
					$graph_data_ary[$key]['fTotalBookingRevenue'] = 0;
					
				$counter = $counter + (24*60*60*30);
			}
	 		return $graph_data_ary ;
	 	}
	 }
	/**
	 * This function used to update Artists details from front section 
	 * @access public
	 * @param array
	 * @return boolean 
	 * @author Ajay
	 */
	 
	public function updateArtistDetailsStep2($data,$idArtist)
	{
		if(!empty($data) && (int)$idArtist>0)
		{
			$kArtist_new = new cArtist();
			$kArtist_new->loadTyni($idArtist);			
			$iAdminVerified = $kArtist_new->iAdminVerified ;
			if($kArtist_new->id<=0)
			{
				$this->addError('szFirstName','DJ you are trying to update is not exists in our database.');
				return false;
			}
			$data['fBaseFee']=(int)str_replace("£",'',$data['fBaseFee']);
			$data['fFeePerHour']=(int)str_replace("£",'',$data['fFeePerHour']);
			$this->validateArtistDetailsStep2($data,$kArtist_new);
			
			if($this->error==true)
			{
				return false;
			}
			
//			$dtArtistDOBDateTime = time() - strtotime($this->dtDateOfBirth);
//			$iArtistAgeInYear = floor((float)$dtArtistDOBDateTime/(60*60*24*365));
//			
//			if($iArtistAgeInYear<16)
//			{
//				$this->addError('dtDateOfBirth','You must be at least 16 years old.');
//				return false;
//			}
			
			$postcodeAry = array();
			$postcodeAry = getAddressDetailsByPostcode($this->szPostcode,true);
			
			if(!empty($postcodeAry['szLatitute']) && !empty($postcodeAry['szLongitute']))
			{
				$this->szLatitute = $postcodeAry['szLatitute'];
				$this->szLongitute = $postcodeAry['szLongitute'];
			}
			else
			{
				$this->addError('szPostcode','This postcode not exists in UK');
				return false;
			}
			
			
			
			if($kArtist_new->iComplete!=1)
			{
//				$query_complete = " 
//					szFirstName = :szFirstName,
//					szLastName = :szLastName,
//					szArtistName = :szArtistName,
//					dtDateOfBirth = :dtDateOfBirth,
//				";
				$query_complete = " 
					szFirstName = :szFirstName,
					szLastName = :szLastName,
					szArtistName = :szArtistName,
				";
			}
			$this->szPostcode = strtoupper($this->szPostcode);
			  
			$query="
				UPDATE
					".__DBC_SCHEMATA_ARTIST__."
				SET			
					$query_complete		
					szPhone = :szPhone,
					szAlternatePhone = :szAlternatePhone,
					szAddress = :szAddress,
					szAddress2 = :szAddress2,
					szPostcode = :szPostcode,
					szCity = :szCity,
					szCounty = :szCounty,
					szLatitute = :szLatitute,
					szLongitute = :szLongitute,
					idPrimaryArtType = :idPrimaryArtType,
					idSecondaryArtType = :idSecondaryArtType,
					iDistanceWillingToTravel = :iDistanceWillingToTravel,
					iMaxPerformanceLenght = :iMaxPerformanceLenght,
					fBaseFee = :fBaseFee,
					fFeePerHour = :fFeePerHour,
					iStep2Completed = '1',
					iAdminVerified = '".(int)$iAdminVerified."',
					dtUpdatedOn = now()
				WHERE
					id = :idArtist
				AND
					isDeleted = '0'
			";
//			echo "<br>".$query ;
			//die;
			if($this->preapreStatement($query))
			{	
				if($kArtist_new->iComplete!=1)
				{
					$this->bindSQL(':szFirstName',$this->szFirstName,PDO::PARAM_STR);
					$this->bindSQL(':szLastName',$this->szLastName,PDO::PARAM_STR);				
					$this->bindSQL(':szArtistName',$this->szArtistName,PDO::PARAM_STR);
//					$this->bindSQL(':dtDateOfBirth',$this->dtDateOfBirth,PDO::PARAM_STR);
				}				
				
				$this->bindSQL(':szPhone',$this->szPhone,PDO::PARAM_STR);				
				$this->bindSQL(':szAlternatePhone',$this->szAlternatePhone,PDO::PARAM_STR);
				$this->bindSQL(':szAddress',$this->szAddress,PDO::PARAM_STR);
				$this->bindSQL(':szAddress2',$this->szAddress2,PDO::PARAM_STR);				
				$this->bindSQL(':szPostcode',$this->szPostcode,PDO::PARAM_STR);
				$this->bindSQL(':szCity',$this->szCity,PDO::PARAM_STR);				
				$this->bindSQL(':szCounty',$this->szCounty,PDO::PARAM_STR);				
				$this->bindSQL(':szLatitute',$this->szLatitute,PDO::PARAM_STR);				
				$this->bindSQL(':szLongitute',$this->szLongitute,PDO::PARAM_STR);
				$this->bindSQL(':idPrimaryArtType',$this->idPrimaryArtType,PDO::PARAM_INT);
				$this->bindSQL(':idSecondaryArtType',$this->idSecondaryArtType,PDO::PARAM_INT);							
				$this->bindSQL(':iDistanceWillingToTravel',$this->iDistanceWillingToTravel,PDO::PARAM_STR);		
				$this->bindSQL(':iMaxPerformanceLenght',$this->iMaxPerformanceLenght,PDO::PARAM_STR);		
					
				$this->bindSQL(':fBaseFee',(float)$this->fBaseFee,PDO::PARAM_INT);				
				$this->bindSQL(':fFeePerHour',(float)$this->fFeePerHour,PDO::PARAM_STR);	
				$this->bindSQL(':idArtist',(int)$idArtist,PDO::PARAM_INT);
				
				if(($result = $this->executeSQL()))
				{		
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
			
		}
	}

	public function validateArtistDetailsStep2($data,$kArtist_new)
	{
		if(!empty($data))
		{
			if($kArtist_new->iComplete!=1)
			{
				//User can update their First Name, Last Name, DJ Name and Date of Birth only for first time.
//				if(!empty($data['dtDateOfBirth']))
//				{
//					$data['dtDateOfBirth'] = format_date_time($data['dtDateOfBirth'],true);
//				}
				
				$this->set_szFirstName(sanitize_all_html_input($data['szFirstName']));
				$this->set_szLastName(sanitize_all_html_input($data['szLastName']));
				$this->set_szArtistName(sanitize_all_html_input($data['szArtistName']));
//				$this->set_dtDateOfBirth(sanitize_all_html_input($data['dtDateOfBirth']));
			}
			
			$this->set_idPrimaryArtType(sanitize_all_html_input($data['idPrimaryArtType']));
			$this->set_idSecondaryArtType(sanitize_all_html_input($data['idSecondaryArtType']));
            
            $this->set_iDistanceWillingToTravel(sanitize_all_html_input($data['iDistanceWillingToTravel']));
            $this->set_fBaseFee(sanitize_all_html_input($data['fBaseFee']));
            $this->set_fFeePerHour(sanitize_all_html_input($data['fFeePerHour']));
            $this->set_iMaxPerformanceLenght(sanitize_all_html_input($data['iMaxPerformanceLenght']));	

			$this->set_szPhone(sanitize_all_html_input($data['szPhone']));
			$this->set_szAlternatePhone(sanitize_all_html_input($data['szAlternatePhone']));
			
			$this->set_szPostcode(sanitize_all_html_input($data['szPostcode']));			
			$this->set_szAddress(sanitize_all_html_input($data['szAddress']));
			$this->set_szAddress2(sanitize_all_html_input($data['szAddress2']));
			$this->set_szCity(sanitize_all_html_input($data['szCity']));
			$this->set_szCounty(sanitize_all_html_input($data['szCounty']));			
			
			if($this->idPrimaryArtType>0 && ($this->idPrimaryArtType == $this->idSecondaryArtType))
			{
				$this->addError('idSecondaryArtType','You can not select same Music Genre as Primary and secondary.');
			}			
			
			if($this->error==true)
			{
				return false;
			}
			else
			{		
				if($this->szPhone==$this->szAlternatePhone)
				{
					$this->addError('szAlternatePhone',"Alternate phone can not be same as phone number.");	
				} 
				return true;
			}
		}
	}
	
	/**
	 * This function used to update Artists details from Admin section 
	 * @access public
	 * @param array
	 * @return boolean 
	 * @author Ajay
	 */
	 
	public function updateArtistDetailsStep3($data,$idArtist)
	{
		if(!empty($data) && (int)$idArtist>0)
		{
			$kArtist_new = new cArtist();
			$kArtist_new->loadTyni($idArtist);
            $iAdminVerified = $kArtist_new->iAdminVerified ;
            if($kArtist_new->iComplete==1)
			{
				$szOldVideoAry[1] = $kArtist_new->szVideo1;
				$szOldVideoAry[2] = $kArtist_new->szVideo2;
				$szOldVideoAry[3] = $kArtist_new->szVideo3;
				$szOldVideoAry[4] = $kArtist_new->szVideo4;
				
				$szOldPhotoAry[1] = $kArtist_new->szPhoto1;
				$szOldPhotoAry[2] = $kArtist_new->szPhoto2;
				$szOldPhotoAry[3] = $kArtist_new->szPhoto3;
				$szOldPhotoAry[4] = $kArtist_new->szPhoto4;
				
			}
			if($kArtist_new->id<=0)
			{
				$this->addError('szFirstName','DJ you are trying to update is not exists in our database.');
				return false;
			}
		
			$this->validateArtistDetailsStep3($data,$kArtist_new);
			
			if($this->error==true)
			{
				return false;
			}
			
            $photo_counter = count($this->szUploadFileName);
			$iPhotoVideoChangedFlag = false ;
			if($photo_counter>0)
			{
				for($i=1;$i<=$photo_counter;$i++)
				{
					$kImageCropper = new cImageCrop();
					if(!empty($this->szUploadFileName[$i]) && $this->iFileUpload[$i]==1)
					{
						$szTempFilePath = __APP_PATH_ARTIST_IMAGES_TEMP__."/".$this->szUploadFileName[$i] ;
						$szDestinationFilePath = __APP_PATH_ARTIST_IMAGES__."/".$this->szUploadFileName[$i] ;
						
						if(file_exists($szTempFilePath))
						{	
							if(file_exists($szDestinationFilePath))
							{
								@unlink($szDestinationFilePath);
							}
							
							if(copy($szTempFilePath, $szDestinationFilePath)) 
							{
								//$kImageCropper->crop_center($szTempFilePath, $szDestinationFilePath,$this->szUploadFileName[$i], 500, 500);
							}
							@unlink($szTempFilePath);
						}
						else
						{	
							$this->szUploadFileName[$i] = "";						
						} 
						$iPhotoVideoChangedFlag = true;
					}
				}
			}
			
			$video_counter = count($data['szVideo']);				
			if($video_counter>0)
			{
				for($i=1;$i<=$video_counter;$i++)
				{
					if(!empty($this->szVideo[$i]))
					{
						$szYouTubeVideoID = display_youtube_video_for_slider($this->szVideo[$i],true);
						if(!empty($szYouTubeVideoID))
						{
							$szFileName = "youtube_video_".$szYouTubeVideoID.".jpg";
							$szFilePath = __APP_PATH_YOUTUBE_IMAGES__."/".$szFileName ;
							if(file_exists($szFilePath))
							{
								@unlink($szFilePath);
							} 
							download_youtube_thumnail($szYouTubeVideoID); 
						}
						 
						if($this->szVideo[$i]!=$szOldVideoAry[$i])
						{
							$iPhotoVideoChangedFlag = true;
						}
					}
				}
			}
			
            
			$query="
				UPDATE
					".__DBC_SCHEMATA_ARTIST__."
				SET
					iYearStarted = :iYearStarted,
					szDescription = :szDescription,
                    szVideo1 = :szVideo1,
					szVideo2 = :szVideo2,
					szVideo3 = :szVideo3,
					szVideo4 = :szVideo4,
					szPhoto1 = :szPhoto1,
					szPhoto2 = :szPhoto2,
					szPhoto3 = :szPhoto3,
					szPhoto4 = :szPhoto4,
					iStep3Completed = 1,
					dtUpdatedOn = now()
				WHERE
					id = :idArtist
				AND
					isDeleted = '0'
			";
			//echo "<br>".$query ;
			//die;
			if($this->preapreStatement($query))
			{	
				$this->bindSQL(':iYearStarted',$this->iYearStarted,PDO::PARAM_STR);				
				$this->bindSQL(':szDescription',$this->szDescription,PDO::PARAM_STR);			
				$this->bindSQL(':idArtist',(int)$idArtist,PDO::PARAM_INT);
				$this->bindSQL(':szVideo1',$this->szVideo[1],PDO::PARAM_STR);				
				$this->bindSQL(':szVideo2',$this->szVideo[2],PDO::PARAM_STR);
				$this->bindSQL(':szVideo3',$this->szVideo[3],PDO::PARAM_STR);				
				$this->bindSQL(':szVideo4',$this->szVideo[4],PDO::PARAM_STR);
				$this->bindSQL(':szPhoto1',$this->szUploadFileName[1],PDO::PARAM_STR);				
				$this->bindSQL(':szPhoto2',$this->szUploadFileName[2],PDO::PARAM_STR);
				$this->bindSQL(':szPhoto3',$this->szUploadFileName[3],PDO::PARAM_STR);				
				$this->bindSQL(':szPhoto4',$this->szUploadFileName[4],PDO::PARAM_STR);	
				if(($result = $this->executeSQL()))
				{		
                    if($iPhotoVideoChangedFlag)
					{						
						$this->loadTyni($idArtist);
						if($kArtist_new->iComplete==1)
						{
							$replace_arr = array();		
							
							$szLinkValue=__BASE_ADMIN_ARTIST_DETAILS_URL__."".$this->szArtistKey."/?action=Email";							
						
							$replace_arr['szLink']="<a href='".$szLinkValue."'>Click here to view profile</a>";
							$replace_arr['szHttpsLink'] = $szLinkValue ;								
							$replace_arr['szFullName'] = $this->szFirstName." ".$this->szLastName;
							$replace_arr['szArtistName'] = $this->szArtistName;
							$replace_arr['szArtistNumber'] = $this->szArtistNumber;
							$replace_arr['szSiteName'] = SITE_NAME; 
						
							createEmail(__VERIFY_ARTIST_PHOTO_VIDEO_BY_ADMIN__,$replace_arr,__STORE_SUPPORT_EMAIL__,__ARTIST_ACCOUNT_COMPLETE_CONFIRMATION_SUBJECT__,__STORE_SUPPORT_EMAIL__);
						}
					}
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	

	public function validateArtistDetailsStep3($data,$kArtist_new)
	{
		if(!empty($data))
		{		
			$this->set_szDescription(sanitize_all_html_input($data['szDescription']));
			$this->set_iYearStarted(sanitize_all_html_input($data['iYearStarted']));
					
			$video_counter = count($data['szVideo']);
			$video_valid = 1;		
			if($video_counter>0)
			{
				for($i=1;$i<=$video_counter;$i++)
				{
					$this->set_szVideo(trim(sanitize_all_html_input($data['szVideo'][$i])),$i);
					
					if(!empty($this->szVideo[$i]))
					{
						$video_valid = 2;
					}
				}
			}
			
			if($video_valid==1)
			{
				$this->addError('szVideo_1','You must add at least 1 youtube video url');
			}
			else
			{
				$video_counter = count($data['szVideo']);		
				$validVideoFormat = array('http://www.youtube.com','//www.youtube.com','http://youtu.be/','https://www.youtube.com/watch?v=');
				$invalidPlaylistFormat="list=";
				if($video_counter>0)
				{	
					for($i=1;$i<=$video_counter;$i++)
					{
						if(!empty($this->szVideo[$i]))
						{
							$szSearchPattern1 = substr($this->szVideo[$i],0,22);
							$szSearchPattern2 = substr($this->szVideo[$i],0,17);
							$szSearchPattern3 = substr($this->szVideo[$i],0,16);
							$szSearchPattern4 = substr($this->szVideo[$i],0,32);
							if(in_array($szSearchPattern1,$validVideoFormat) || in_array($szSearchPattern2,$validVideoFormat) || in_array($szSearchPattern3,$validVideoFormat) || in_array($szSearchPattern4,$validVideoFormat))
							{
                                $posPlaylistURL = strpos($this->szVideo[$i], $invalidPlaylistFormat);
                                
                                if(!empty($posPlaylistURL))
                                {
                                    $this->addError('szVideo_'.$i,"Youtube playlist link cannot be added");
                                }
							}
							else
							{
								$this->addError('szVideo_'.$i,"Invalid Link, Youtube url must start with http://www.youtube.com, //www.youtube.com or http://youtu.be/");
							}
						}
					}
				}
			}
			
			$photo_counter = count($data['szUploadFileName']);		
			if($photo_counter>0)
			{
				for($i=1;$i<=$photo_counter;$i++)
				{
					$photo_flag=false;
					if($i<4)
					{
						$photo_flag = true;
					}
					$this->set_szUploadFileName(trim(sanitize_all_html_input($data['szUploadFileName'][$i])),$i,$photo_flag);
					$this->set_iFileUpload(trim(sanitize_all_html_input($data['iFileUpload'][$i])),$i);
				}
			}
			
			if($this->error==true)
			{
				return false;
			}
			else
			{			
				
				$address_counter = count($data['szVenueName']);	
				if($address_counter>0)
				{
					for($i=0;$i<=$address_counter;$i++)
					{
						if(!empty($this->dtStartDate[$i]))
						{
							$dtEventStarted = strtotime($this->dtStartDate[$i]);
							$todayTime = strtotime(date('Y-m-d'));
							
							if($dtEventStarted>=$todayTime)
							{
								$this->addError('dtStartDate_'.$i,'Event date must be less than today');
							}
						}
					}
				}
								
				if($this->iYearStarted>0 && ($this->iYearStarted>2014))
				{
					$this->addError('iYearStarted','Year started must be less than or equal to current year.');
					return false;
				}
				return true;
			}
		}
	}

	/**
	 * This function used to update Artists details from Admin section 
	 * @access public
	 * @param array
	 * @return boolean 
	 * @author Ajay
	 */
	 
	public function updateArtistDetailsStep4($data,$idArtist)
	{
		if(!empty($data) && (int)$idArtist>0)
		{
			$kArtist_new = new cArtist();
			$kArtist_new->loadTyni($idArtist);
			if($kArtist_new->id<=0)
			{
				$this->addError('szFirstName','DJ you are trying to update is not exists in our database.');
				return false;
			}
			$this->validateArtistDetailsStep4($data,$kArtist_new);
			
			if($this->error==true)
			{
				return false;
			}
			$quoteLength='';
            
            if($kArtist_new->iMemberShipType == __MEMBERSHIP_TYPE_PREMIUM__ || $kArtist_new->iMemberShipType == __MEMBERSHIP_TYPE_MANAGEMENT__)
            {
                $quoteLength=" szFeaturedQuote = :szFeaturedQuote,";
            }
			$query="
				UPDATE
					".__DBC_SCHEMATA_ARTIST__."
				SET
					iEquipment = :iEquipment,
                    $quoteLength
					iStep4Completed = 1,
					dtUpdatedOn = now()
				WHERE
					id = :idArtist
				AND
					isDeleted = '0'
			";
//			echo $query;

			if($this->preapreStatement($query))
			{	
                $this->bindSQL(':iEquipment',$this->iEquipment,PDO::PARAM_STR);	
                if($kArtist_new->iMemberShipType == __MEMBERSHIP_TYPE_PREMIUM__ || $kArtist_new->iMemberShipType == __MEMBERSHIP_TYPE_MANAGEMENT__)
                {

                    $this->bindSQL(':szFeaturedQuote',$this->szFeaturedQuote,PDO::PARAM_STR);
                }
                $this->bindSQL(':idArtist',(int)$idArtist,PDO::PARAM_INT);
				
				if(($result = $this->executeSQL()))
				{		
					if($this->iEquipment==1)
					{
						$this->addArtistEquipmentDetails($idArtist);
					}
					else
					{
						$this->deleteEquipmentDetails($idArtist);
					}
                    $this->addLastLocationPlayed($idArtist);	
					$this->addArtistCertification($idArtist);	
					$this->addArtistAwards($idArtist);
					if($kArtist_new->iComplete!=1)
					{
						$this->addArtistReferences($idArtist);
					}
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	

	public function validateArtistDetailsStep4($data,$kArtist_new)
	{
		if(!empty($data))
		{	
            $this->set_iEquipment(sanitize_all_html_input($data['iEquipment']));
			if($this->iEquipment==1)
			{			
				$this->set_szSpeakerDescription(sanitize_all_html_input($data['szSpeakerDescription']));
				$this->set_szDeckDescription(sanitize_all_html_input($data['szDeckDescription']));
				$this->set_szOtherDescription(sanitize_all_html_input($data['szOtherDescription']));
				$this->set_fEquipmentCharge(sanitize_all_html_input($data['fEquipmentCharge']));
				$this->set_iPATCertificate(sanitize_all_html_input($data['iPATCertificate']));
				
				
				if($this->iPATCertificate==1)
				{
					if(!empty($data['dtPATExpiryDate']))
					{
						$data['dtPATExpiryDate'] = format_date_time($data['dtPATExpiryDate'],true);
					}				
					$this->set_dtPATExpiryDate(sanitize_all_html_input($data['dtPATExpiryDate']));
				}
				
				$equipmentPhoto = 1 ;
				for($i=1;$i<4;$i++)
				{
					$this->set_szUploadEquipmentPhoto(trim(sanitize_all_html_input($data['szUploadEquipmentPhoto'][$i])),$i);
					$this->set_iEquipentFileUpload(trim(sanitize_all_html_input($data['iEquipentFileUpload'][$i])),$i);	
					
					if(!empty($this->szUploadEquipmentPhoto[$i]))
					{
						$equipmentPhoto = 2;
					}
				}
				
				if($equipmentPhoto==1)
				{
					$this->addError('szEquipentPhoto_1','You must select at least 1 equipment photo');
				}
			}
            
            if($kArtist_new->iComplete!=1)
			{
				$iReferAdded = 1;
				$reference_counter = count($data['szRefName']);	
				if($reference_counter>0)
				{
					for($i=1;$i<=$reference_counter;$i++)
					{
						$flag_ref = false;
						if(!empty($data['szRefName'][$i]) || !empty($data['szRefEmail'][$i]) || !empty($data['szRefBussinessName'][$i]) || !empty($data['szRefPosition'][$i]))
						{
							$flag_ref = true;
							$iReferAdded = 2;
						}
									
						$this->set_szRefName(sanitize_all_html_input($data['szRefName'][$i]),$i,$flag_ref);		
						$this->set_szRefEmail(sanitize_all_html_input($data['szRefEmail'][$i]),$i,$flag_ref);				
						$this->set_szRefBussinessName(sanitize_all_html_input($data['szRefBussinessName'][$i]),$i,$flag_ref);	
						$this->set_szRefPosition(sanitize_all_html_input($data['szRefPosition'][$i]),$i,$flag_ref);				
						$this->set_idReference(sanitize_all_html_input($data['idReference'][$i]),$i);
					}
				}
				
				if($iReferAdded==1)
				{
					$this->addError('szRefName_1','You must add atleast one reference');
				}
			}
			
			$certificate_counter = count($data['szCertification']);	
			if($certificate_counter>0)
			{
				for($i=1;$i<=$certificate_counter;$i++)
				{				
					$flag_cert = false;
					if(!empty($data['szCertification'][$i]) || !empty($data['dtCertificateExpiry'][$i]))
					{
						$flag_cert = true;
					}
					if(!empty($data['dtCertificateExpiry'][$i]))
					{
						$data['dtCertificateExpiry'][$i] = format_date_time($data['dtCertificateExpiry'][$i],true);
					}
					
					$this->set_szCertification(sanitize_all_html_input($data['szCertification'][$i]),$i,$flag_cert);		
					$this->set_dtCertificateExpiry(sanitize_all_html_input($data['dtCertificateExpiry'][$i]),$i,$flag_cert);
					$this->set_idCertificate(sanitize_all_html_input($data['idCertificate'][$i]),$i,$flag_cert);
				}
			}  
			$award_counter = count($data['szAwardTitle']);	
			if($award_counter>0)
			{
				for($i=1;$i<=$award_counter;$i++)
				{				
					$flag_award = false;				
					if(!empty($data['szAwardTitle'][$i]) || !empty($data['dtAwardDate'][$i]))
					{
						$flag_award = true;
					}
					
					if(!empty($data['dtAwardDate'][$i]))
					{
						$data['dtAwardDate'][$i] = format_date_time($data['dtAwardDate'][$i],true);
					}
					$this->set_szAwardTitle(sanitize_all_html_input($data['szAwardTitle'][$i]),$i,$flag_award);	
					$this->set_dtAwardDate(sanitize_all_html_input($data['dtAwardDate'][$i]),$i,$flag_award);	
					$this->set_idAward(sanitize_all_html_input($data['idAward'][$i]),$i,$flag_award);
				}
			} 
			$address_counter = count($data['szVenueName']);	
			if($address_counter>0)
			{
				for($i=0;$i<$address_counter;$i++)
				{
					$flag=false;
					if(!empty($data['szVenueName'][$i]) || !empty($data['szVCounty'][$i]) || !empty($data['dtStartDate'][$i]))
					{
						$flag = true;
					}
					$this->set_szVenueName(trim(sanitize_all_html_input($data['szVenueName'][$i])),$i,$flag);					
					$this->set_szVCounty(trim(sanitize_all_html_input($data['szVCounty'][$i])),$i,$flag);
					$this->set_idLastLocationPlayed(trim(sanitize_all_html_input($data['idLastLocationPlayed'][$i])),$i);
					$this->set_iTopLocation(trim(sanitize_all_html_input($data['iTopLocation'][$i])),$i);
					
					if(!empty($data['dtStartDate'][$i]))
					{
						$data['dtStartDate'][$i] = format_date_time($data['dtStartDate'][$i],true);
					}
					$this->set_dtStartDate(trim(sanitize_all_html_input($data['dtStartDate'][$i])),$i,$flag);	
				}
			}
           
            if($kArtist_new->iMemberShipType == __MEMBERSHIP_TYPE_PREMIUM__ || $kArtist_new->iMemberShipType == __MEMBERSHIP_TYPE_MANAGEMENT__)
            {
                
                $this->set_szFeaturedQuote(sanitize_all_html_input($data['szFeaturedQuote']));
			
            }
            if($this->error==true)
			{
				return false;
			}
			else
			{		
				if($this->iPATCertificate==1)
				{
					$dtPATExpiryDateTime = strtotime($this->dtPATExpiryDate);						
					$todayTime = strtotime(date('Y-m-d'));	
									
					if($todayTime>=$dtPATExpiryDateTime)
					{
						$this->addError('dtPATExpiryDate','date must be greater or equal to today.');
						return false;
					}
				}
                $address_counter = count($data['szVenueName']);	
				if($address_counter>0)
				{
					for($i=0;$i<=$address_counter;$i++)
					{
						if(!empty($this->dtStartDate[$i]))
						{
							$dtEventStarted = strtotime($this->dtStartDate[$i]);
							$todayTime = strtotime(date('Y-m-d'));
							
							if($dtEventStarted>=$todayTime)
							{
								$this->addError('dtStartDate_'.$i,'Event date must be less than today');
							}
						}
					}
				}
								
				for($i=1;$i<=3;$i++)
				{
					if(!empty($this->dtCertificateExpiry[$i]) && ($this->dtCertificateExpiry[$i]!='0000-00-00 00:00:00'))
					{
						$dtCertificateExpiryTime = strtotime($this->dtCertificateExpiry[$i]);
						
						$todayTime = strtotime(date('Y-m-d'));
						
						if($todayTime>=$dtCertificateExpiryTime)
						{
							$this->addError('dtCertificateExpiry_'.$i,'Expiry date must be greater than or equal to today.');
						}
					}
					
					if(!empty($this->dtAwardDate[$i]) && ($this->dtAwardDate[$i]!='0000-00-00 00:00:00'))
					{
						$dtAwardDateTime = strtotime($this->dtAwardDate[$i]);
						
						$todayTime = strtotime(date('Y-m-d'));
						
						if($todayTime<=$dtAwardDateTime)
						{
							$this->addError('dtAwardDate_'.$i,'Award date must be less than or equal to today.');
							//return false;
						}
					}
				}
				
				return true;
			}
		}
	}

	/**
	 * This function used to update Artists details from Admin section 
	 * @access public
	 * @param array
	 * @return boolean 
	 * @author Ajay
	 */
	 
	public function updateArtistDetailsStep5($data,$idArtist,$szMembershipType)
	{
		if(!empty($data) && (int)$idArtist>0)
		{
			$kArtist_new = new cArtist();
			$kArtist_new->loadTyni($idArtist);
			
			if($kArtist_new->id<=0)
			{
				$this->addError('szFirstName','DJ you are trying to update is not exists in our database.');
				return false;
			}
			
			$iSendEmail = 0;
			if($kArtist_new->iComplete!=1)
			{
				$iSendEmail=1 ;
			}
				
			$this->validateArtistDetailsStep5($data);
			$iCompleted=0;
			if($kArtist_new->iStep1Completed==1 && $kArtist_new->iStep2Completed==1 && $kArtist_new->iStep3Completed==1 && $kArtist_new->iStep4Completed==1)
			{
				$iCompleted=1 ;
			}	
			else
			{
				//returning to complete backsteps then come to update here 	
				return true;
			}
			if($this->error==true)
			{
				return false;
			}
						
			$iMemberShipType = __MEMBERSHIP_TYPE_BASIC__ ;
			$fMembershipCost = 0;
				
			$query="
				UPDATE
					".__DBC_SCHEMATA_ARTIST__."
				SET
					iMemberShipType = :iMemberShipType,
					fMembershipCost = :fMembershipCost,
					iStep5Completed = '1',
					iComplete = :iComplete,
					dtUpdatedOn = now()
				WHERE
					id = :idArtist
				AND
					isDeleted = '0'
			";
			if($this->preapreStatement($query))
			{						
				$this->bindSQL(':idArtist',(int)$idArtist,PDO::PARAM_INT);
				$this->bindSQL(':iMemberShipType',(int)$iMemberShipType,PDO::PARAM_INT);
				$this->bindSQL(':fMembershipCost',(float)$fMembershipCost,PDO::PARAM_INT);
				
				$this->bindSQL(':iComplete',(int)$iCompleted,PDO::PARAM_INT);
				
				if(($result = $this->executeSQL()))
				{
					if(($iCompleted==1) && ($iSendEmail==1) && $iMemberShipType==__MEMBERSHIP_TYPE_BASIC__)
					{
						$this->loadTyni($idArtist);
						$replace_arr = array();		
						
						$szLinkValue=__BASE_ADMIN_ARTIST_DETAILS_URL__."".$this->szArtistKey."/?action=Email";							
					
						$replace_arr['szLink']="<a href='".$szLinkValue."'>Click here to view profile</a>";
						$replace_arr['szHttpsLink'] = $szLinkValue ;								
						$replace_arr['szFullName'] = $this->szFirstName." ".$this->szLastName;
						$replace_arr['szArtistName'] = $this->szArtistName;
						$replace_arr['szArtistNumber'] = $this->szArtistNumber;
						$replace_arr['szSiteName'] = SITE_NAME;						
						$this->iFirstTimeCompleted = 1;
					
						createEmail(__ARTIST_ACCOUNT_COMPLETE_CONFIRMATION__,$replace_arr,$this->szEmail,__ARTIST_ACCOUNT_COMPLETE_CONFIRMATION_SUBJECT__,__STORE_SUPPORT_EMAIL__);
						createEmail(__VERIFY_ARTIST_ACCOUNT_BY_ADMIN__,$replace_arr,__STORE_SUPPORT_EMAIL__,__VERIFY_ARTIST_ACCOUNT_BY_ADMIN_SUBJECT__,__STORE_SUPPORT_FROM_EMAIL__);
					}
					else
					{
						
					}
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	public function validateArtistDetailsStep5($data)
	{
		if(!empty($data))
		{
				return true;
		}
	}
	
	/**
	 * This function checks if any certification is already exists than it simply update existing record otherwise insert new record into database
	 * @access public
	 * @param string
	 * @return boolean 
	 * @author Ajay
	 */
	
	function addArtistReferences($idArtist,$iAdminFlag=false)
	{	
		$certif_counter = count($this->szRefName);
		if($certif_counter>0)
		{	
			$ctr_ep = 0;
			$usedIdAry = array();
			$addedReferenceAry = array();
			for($i=1;$i<=$certif_counter;$i++)
			{	
				if(!empty($this->szRefName[$i]) || !empty($this->szRefEmail[$i]) || !empty($this->szRefBussinessName[$i]) || !empty($this->szRefPosition[$i]))
				{
					$update_flag = false;
					$added_flag = false;
					if(((int)$this->idReference[$i]>0) && ($this->isReferenceIdExist($this->idReference[$i],$idArtist)))
					{
						$query_emp="
							UPDATE
								".__DBC_SCHEMATA_REFERENCES__."
							SET	
								szName = :szName,
								szEmail = :szEmail,
								szPosition = :szPosition,
								szBussinessName = :szBussinessName,
								dtUpdatedOn = now()
							WHERE
								id=:id		
							AND
								idArtist=:idArtist
						";
						$update_flag = true;					
					}
					else
					{		
						$szReferenceKey = md5($this->szRefEmail[$i]."_".$this->szRefName[$i]."_".time()."_".mt_rand(0,1000));
						
						$query_emp = "
							INSERT INTO
								".__DBC_SCHEMATA_REFERENCES__."
							(
								idArtist,
								szName,
								szReferenceKey,
								szEmail,
								szBussinessName,
								szPosition,
								iActive,
								dtCreatedOn
							)
							VALUES
							(
								:idArtist,
								:szName,
								:szReferenceKey,
								:szEmail,
								:szBussinessName,
								:szPosition,
								'1',
								now()
							)
						";
						$added_flag = true;
					}
								
					if($this->preapreStatement($query_emp))
					{	
						if($update_flag)
						{
							$this->bindSQL(':id',(int)$this->idReference[$i],PDO::PARAM_INT);
						}
						else
						{
							$this->bindSQL(':szReferenceKey',$szReferenceKey,PDO::PARAM_STR);	
						}
						
						$this->bindSQL(':idArtist',(int)$idArtist,PDO::PARAM_INT);					
						$this->bindSQL(':szName',$this->szRefName[$i],PDO::PARAM_STR);					
						$this->bindSQL(':szEmail',$this->szRefEmail[$i],PDO::PARAM_STR);
						
						$this->bindSQL(':szBussinessName',$this->szRefBussinessName[$i],PDO::PARAM_STR);					
						$this->bindSQL(':szPosition',$this->szRefPosition[$i],PDO::PARAM_STR);				
						if(($result = $this->executeSQL($added_flag)))
						{
							// TO DO 
							if($added_flag)
							{
								$usedIdAry[$ctr_ep] = $this->iLastInsertID ;
								if($iAdminFlag)
								{
									$addedReferenceAry[] = $this->iLastInsertID ;
								}
							}
							else
							{
								$usedIdAry[$ctr_ep] = $this->idReference[$i] ;
								
								if($iAdminFlag)
								{
									if($this->szRefereceEmail != $this->szRefEmail[$i])
									{
										$addedReferenceAry[] = $this->idReference[$i];
									}
								}
							}			
							$ctr_ep++;
						}
					}
					else
					{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					}
				}
			}			
			$this->deleteReferencesByDJ($usedIdAry,$idArtist) ;
			if($iAdminFlag && !empty($addedReferenceAry))
			{
				$referencesAry = array();
				$referencesAry = $this->getAllReferencesByArtist($idArtist);
			 
				if(!empty($referencesAry))
				{
					$cert_count = 1;
					$idReferenceAry = array();
					foreach($referencesAry as $referencesArys)
					{
						if(in_array($referencesArys['id'],$addedReferenceAry))
						{
							$szLinkValue=__BASE_DJ_REFERENCE_RATING_URL__."".$this->szArtistKey."/".$referencesArys['szReferenceKey']."/?action=Email";	
							
							$replace_arr = array();	
							$replace_arr['szLink']="<a href='".$szLinkValue."'>Click here to rate ".$this->szArtistName."</a>";
							$replace_arr['szHttpsLink'] = $szLinkValue ;					
							$replace_arr['szRefName'] = $referencesArys['szName'];	
							$replace_arr['szFullName'] = $this->szFirstName." ".$this->szLastName;
							$replace_arr['szArtistName'] = $this->szArtistName;
							$replace_arr['szSiteName'] = SITE_NAME;
							createEmail(__REFERENCE_REQUEST_OF_DJ__,$replace_arr,$referencesArys['szEmail'],__REFERENCE_REQUEST_OF_DJ_SUBJECT__,__STORE_SUPPORT_EMAIL__,'','','','',true);
							$idReferenceAry[] = $referencesArys['id'];
						} 
					}
					if(!empty($idReferenceAry))
					{
						$this->updateReferalEmails($idReferenceAry,$idArtist,1);
					}
				}
			}
		}
	}
	
	/**
	 * This function used to check unavailabe date id is exists or not.
	 * @access public
	 * @param int
	 * @return boolean 
	 * @author Ajay
	 */
	function isReferenceIdExist($idReferences,$idArtist)
	{	
		if($idReferences>0 && $idArtist>0)
		{
			$query="
				SELECT
					id,
					szEmail
				FROM
					".__DBC_SCHEMATA_REFERENCES__."
				WHERE
					id='".(int)$idReferences."'		
				AND
					idArtist = '".(int)$idArtist."'	
			";
			//echo " quety ".$query ;
			//die;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row = $this->getAssoc();
					$this->szRefereceEmail = $row['szEmail'];
					return true;
				}
				else
				{
					$this->szRefereceEmail = '';
					return false ;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * This function used to delete Unavailable dates 
	 * @access public
	 * @param int
	 * @return boolean 
	 * @author Ajay
	 */
	 
	public function deleteReferencesByDJ($usedLocationIdAry,$idArtist)
	{
		if($idArtist>0 && !empty($usedLocationIdAry))
		{
			$idLocation_str = implode(',',$usedLocationIdAry);
			
			if(!empty($idLocation_str))
			{
				$query="
					DELETE FROM
						".__DBC_SCHEMATA_REFERENCES__."
					WHERE
						id NOT IN (".$idLocation_str.")	
					AND
						idArtist = :idArtist	
				";
				
				if($this->preapreStatement($query))
				{	
					$this->bindSQL(':idArtist',(int)$idArtist,PDO::PARAM_INT);
					
					if( ( $result = $this->executeSQL() ) )
					{
						return true ;
					}
				}
				else 
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
			}
		}
	}
	function getAllReferencesByArtist($idArtist,$szReferenceKey=false)
	{		
		if($idArtist>0)
		{
			if(!empty($szReferenceKey))
			{
				$query_and = " AND szReferenceKey = :szReferenceKey ";
			}
			
			$query="
				SELECT
					id,
					idArtist,
					szName,
					szEmail,
					szPosition,
					szBussinessName,
					szReferenceKey,
					iEmailSent,
					iActive,
					dtCreatedOn
				FROM
					".__DBC_SCHEMATA_REFERENCES__."
				WHERE
					idArtist = '".(int)$idArtist."'	
				AND
					isDeleted = '0'
				$query_and
			";	
			//echo $query ;		
			if($this->preapreStatement($query))
			{
				if(!empty($szReferenceKey))
				{
					$this->bindSQL(':szReferenceKey',$szReferenceKey,PDO::PARAM_STR);	
				}
				
				if( ( $result = $this->executeSQL() ) )
				{
					if($this->iNumRows>0)
					{
						$ret_ary = array();
						$ctr = 0;
						while($row = $this->getAssoc())
						{
							$ret_ary[$ctr] = $row;
							$ctr++;
						}
						return $ret_ary;
					}
					else
					{
						return false;
					}
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
			} 
		}
		else
		{
			return false;
		}
	}
	
	function updateReferalEmails($idReferenceAry,$idArtist,$iValue=false)
	{
		if(!empty($idReferenceAry))
		{
			$ids = implode(",",$idReferenceAry);
			
			$query = "
				UPDATE
					".__DBC_SCHEMATA_REFERENCES__."
				SET
					iEmailSent = '".(int)$iValue."'
				WHERE
					id IN (".$ids.")
				AND
					idArtist = '".(int)$idArtist."'
			";
			//echo "query: ".$query ;
			if($this->preapreStatement($query))
			{	
				if( ( $result = $this->executeSQL() ) )
				{
					return true ;
				}
			}
			else 
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	public function validateArtistSearch($data)
	{	
		if(!empty($data))
		{
			if(!empty($data['dtRegistrationDateFrom']))
			{
				$data['dtRegistrationDateFrom'] = format_date_time($data['dtRegistrationDateFrom']);
			}
			if(!empty($data['dtRegistrationDateTo']))
			{
				$data['dtRegistrationDateTo'] = format_date_time($data['dtRegistrationDateTo']);
			}
			
			$this->set_dtCreatedDateFrom(sanitize_all_html_input($data['dtRegistrationDateFrom']));	
			$this->set_dtCreatedDateTo(sanitize_all_html_input($data['dtRegistrationDateTo']));
			
			if($this->error==true)
			{
				return "ERROR";
			}
			else
			{
				if((!empty($this->dtCreatedDateFrom) && !empty($this->dtCreatedDateTo)) && ($this->dtCreatedDateFrom>=$this->dtCreatedDateTo))
				{
					$this->addError('dtCreatedDateFrom','Registered From date must be less then Registered To date');
					return 'ERROR';
				}
				return 'SUCCESS';
			}
		}
		else
		{
			return 'ERROR';
		}
	}
	
	function getAllReviewabaleItemsByAdmin()
	{
		$reviewableItemsAry = array();
		$djProfileAry = array();
		$djProfileAry = $this->getAllDJAccountToReview();
		
		$iClientToReview = $this->getAllClientAccountToReview();
		$iBookingToReview = $this->getAllBookingToReview();
		
		if($djProfileAry['iCompleteProfile']>0)
		{
			$reviewableItemsAry['iCompleteProfile'] = $djProfileAry['iCompleteProfile'] ;
		}
		if($djProfileAry['iIncompleteProfile']>0)
		{
			$reviewableItemsAry['iIncompleteProfile'] = $djProfileAry['iIncompleteProfile'] ;
		}		
		if($iClientToReview>0)
		{
			$reviewableItemsAry['iClientToReview'] = $iClientToReview ;
		}
		if($iBookingToReview>0)
		{
			$reviewableItemsAry['iBookingToReview'] = $iBookingToReview ;
		}				
		return $reviewableItemsAry;
	}
	
	function getAllDJAccountToReview()
	{
		$query="
			SELECT
				(SELECT count(a1.id) FROM ".__DBC_SCHEMATA_ARTIST__." a1 WHERE a1.iComplete='1' AND a1.isDeleted='0' AND a1.iAdminVerified NOT IN('1','2')) as iCompleteProfile,
				(SELECT count(a2.id) FROM ".__DBC_SCHEMATA_ARTIST__." a2 WHERE a2.iComplete='0' AND a2.isDeleted='0' AND a2.iAdminVerified NOT IN('1','2')) as iIncompleteProfile
		";
		//echo "query: ".$query ;
		if($this->preapreStatement($query))
		{
			if( ( $result = $this->executeSQL() ) )
			{
				if($this->iNumRows>0)
				{
					$this->set_szPdoMode(PDO::FETCH_ASSOC);
					$row = $this->getAssoc();
					return $row;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
	}
	function getAllClientAccountToReview()
	{
		$query="
				SELECT
					count(id) as iClientToReview
				FROM 
					".__DBC_SCHEMATA_BUYER__."  
				WHERE 
					isDeleted='0' 
				AND 
					iReview!='1'
			";
			//echo "query: ".$query ;
			if($this->preapreStatement($query))
			{
				if( ( $result = $this->executeSQL() ) )
				{
					if($this->iNumRows>0)
					{
						$this->set_szPdoMode(PDO::FETCH_ASSOC);
						$row = $this->getAssoc();
						return $row['iClientToReview'];
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
	}

	function getAllBookingToReview()
	{
		$query="
				SELECT
					count(id) as iBookingToReview
				FROM 
					".__DBC_SCHEMATA_BOOKING__."  
				WHERE 
					isDeleted='0' 
				AND 
					iReview!='1'
				AND
					iBookingStatus = 1
			";
			//echo "query: ".$query ;
			if($this->preapreStatement($query))
			{
				if( ( $result = $this->executeSQL() ) )
				{
					if($this->iNumRows>0)
					{
						$this->set_szPdoMode(PDO::FETCH_ASSOC);
						$row = $this->getAssoc();
						return $row['iBookingToReview'];
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
	}
	
	function sendVerificationLink($idUser,$iUserType)
	{
		if($idUser>0 && $iUserType>0)
		{
			if($iUserType==__USER_TYPE_ARTIST__)
			{
				$kUser = new cArtist();
				$kUser->loadTyni($idUser);
				$szTableName = __DBC_SCHEMATA_ARTIST__ ;
			}
			else 
			{
				$kUser = new cBuyer();
				$kUser->load($idUser);
				$szTableName = __DBC_SCHEMATA_BUYER__ ;
			}
			if($kUser->id>0)
			{
				$szConfirmKey = md5($kUser->szEmail."_".date('YmdHis'));
				
				$query="
					UPDATE
						".$szTableName."
					SET
						dtVerificationLinkSent = now(),
						szConfirmationKey = :szConfirmationKey
					WHERE
						id = :idUser
				";
				
				if($this->preapreStatement($query))
				{	
					$this->bindSQL(':szConfirmationKey',$szConfirmKey,PDO::PARAM_STR);							
					$this->bindSQL(':idUser',(int)$kUser->id,PDO::PARAM_INT);
					
					if(($result = $this->executeSQL()))
					{				
						$replace_arr = array();
						$szLinkValue=__URL_BASE__."/?action=confirmEmail&key=".$szConfirmKey;							
						
						$replace_arr['szLink']="<a href='".$szLinkValue."'>Click here to verify account </a>";
						$replace_arr['szHttpsLink'] = $szLinkValue ;
						$replace_arr['szEmail'] = $kUser->szEmail;
						$replace_arr['szSiteUrl'] = __URL_BASE__ ;	
						$replace_arr['szUserName'] = $kUser->szEmail;
						$replace_arr['szPassword'] = $kUser->szPassword;
						$replace_arr['szSiteName'] = SITE_NAME ;		
						createEmail('__USER_ACCOUNT_VERIFICATION__',$replace_arr,$kUser->szEmail,__CREATE_USER_ACCOUNT_SUBJECT__,__STORE_SUPPORT_EMAIL__);
						return true;
					}
				}
			}
		}
	}
	

	function getAllMembershipDetails($idMemberShipType=false)
	{	
		if((int)$idMemberShipType>0)
		{
			$query_and = " AND id = '".(int)$idMemberShipType."' ";
		}
		$query="
			SELECT
				id,
				szMemberShipType,
				fMemberShipCost,
				fFeePerBooking,
				iUniqueDJPage,
				iAutoEventContract,
				iHigherSearchRanking,
				iAccredited,
				iProWriteup,
				iEventDepositPayment,
				iEventPaymentManagement,
				iRyderManagement,
				iDirectClientManagement
			FROM
				".__DBC_SCHEMATA_ADMIN_MEMBERSHIP_DETAILS__."
			WHERE
				iActive = '1'
			$query_and
			ORDER BY
				fMemberShipCost ASC
		";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$ret_ary = array();
				$ctr = 0;
				while($row = $this->getAssoc())
				{
					$ret_ary[$ctr] = $row;
					$ctr++;
				}
				return $ret_ary;
			}
			else
			{
				return false ;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	function updateKey($szArtistKey,$id)
	{
		$query="
			UPDATE
				".__DBC_SCHEMATA_ARTIST__."
			SET
				szArtistKey = :szArtistKey
			WHERE
				id = :idBooking
			AND
				isDeleted = '0'
		";
	
		if($this->preapreStatement($query))
		{
			$this->bindSQL(':szArtistKey',$szArtistKey,PDO::PARAM_STR);
			$this->bindSQL(':idBooking',(int)$id,PDO::PARAM_INT);
			
			if($result = $this->executeSQL())
			{
			
			}
		}
	}
	
	function updateAvgRating($fAvgRating,$idArtist)
	{
		$query="
			UPDATE
				".__DBC_SCHEMATA_ARTIST__."
			SET
				fAvgRating = :fAvgRating,
				dtUpdatedOn = now()
			WHERE
				id = :idArtist
			AND
				isDeleted = '0'
		";
	
		if($this->preapreStatement($query))
		{
			$this->bindSQL(':fAvgRating',(float)$fAvgRating,PDO::PARAM_INT);
			$this->bindSQL(':idArtist',(int)$idArtist,PDO::PARAM_INT);
			
			if($result = $this->executeSQL())
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	/**
	 * This function used to update Artists details from Admin section 
	 * @access public
	 * @param array
	 * @return boolean 
	 * @author Ajay
	 */
	 
	public function updateArtistDetailsStep6($data,$idArtist)
	{
		if(!empty($data) && (int)$idArtist>0)
		{
			$kArtist_new = new cArtist();
			$kArtist_new->loadTyni($idArtist);
			
			if($kArtist_new->id<=0)
			{
				$this->addError('szSPostcode','DJ you are trying to update is not exists in our database.');
				return false;
			}
			
			$this->validateArtistDetailsStep6($data);
			if($this->error==true)
			{
				return false;
			}
			
			$query="
				UPDATE
					".__DBC_SCHEMATA_ARTIST__."
				SET
					szSPostcode = :szSPostcode,
					szSAddress = :szSAddress,
					szSCity = :szSCity,
					szSCounty = :szSCounty
				WHERE
					id = :idArtist
				AND
					isDeleted = '0'
			";
		
			if($this->preapreStatement($query))
			{
				$this->bindSQL(':szSPostcode',$this->szSPostcode,PDO::PARAM_STR);
				$this->bindSQL(':szSAddress',$this->szSAddress,PDO::PARAM_STR);
				$this->bindSQL(':szSCity',$this->szSCity,PDO::PARAM_STR);
				$this->bindSQL(':szSCounty',$this->szSCounty,PDO::PARAM_STR);
				$this->bindSQL(':idArtist',(int)$idArtist,PDO::PARAM_INT);
				
				if($result = $this->executeSQL())
				{
					$kSubscription = new cSubscription();
					
					$this->load($idArtist);
					$addSubscriptionAry = array();
					$addSubscriptionAry['idArtist'] = $idArtist ;
					$addSubscriptionAry['fSubscriptionAmount'] = $this->fMembershipCost ;
					$addSubscriptionAry['dtSubscriptionStartingDate'] = date('Y-m-d H:i:s');
					$addSubscriptionAry['dtSubscriptionExpiryDate'] = date('Y-m-d H:i:s',strtotime("+1 MONTH"));					
					$addSubscriptionAry['szFirstName'] = $this->szFirstName ;
					$addSubscriptionAry['szLastName'] = $this->szLastName ;
					
					$addSubscriptionAry['szAddress'] = $this->szAddress ;
					$addSubscriptionAry['szPostcode'] = $this->szPostcode ;
					$addSubscriptionAry['szCity'] = $this->szCity ;
					$addSubscriptionAry['szCounty'] = $this->szCounty ;
					$addSubscriptionAry['szSAddress'] = $this->szSAddress ;
					$addSubscriptionAry['szSPostcode'] = $this->szSPostcode ;
					$addSubscriptionAry['szSCity'] = $this->szSCity ;
					$addSubscriptionAry['szSCounty'] = $this->szSCounty ;
					
					$kSubscription->addSubscriptions($addSubscriptionAry);
					$this->szSubscriptionRandomKey = $kSubscription->szSubscriptionRandonKey ;
					return true;
				
				}
			}
			
		}
	}
	public function validateArtistDetailsStep6($data)
	{
		if(!empty($data))
		{
			$this->set_szSPostcode(sanitize_all_html_input($data['szSPostcode']));			
			$this->set_szSAddress(sanitize_all_html_input($data['szSAddress']));
			$this->set_szSCity(sanitize_all_html_input($data['szSCity']));
			$this->set_szSCounty(sanitize_all_html_input($data['szSCounty']));
		}
	}			
	

	/**
	 * This function used to update Artists details from Admin section 
	 * @access public
	 * @param array
	 * @return boolean 
	 * @author Ajay
	 */
	 
	public function updateArtistSubscriptionDetails($data,$idArtist,$iSendEmail=true)
	{
		if(!empty($data) && (int)$idArtist>0)
		{
			$kArtist_new = new cArtist();
			$kArtist_new->loadTyni($idArtist);
			$iProfileCompleted = 0; 
			if($kArtist_new->iComplete!=1)
			{
				$iProfileCompleted = 1; 
			}
			
			if($kArtist_new->id<=0)
			{
				$this->addError('szFirstName','DJ you are trying to update is not exists in our database.');
				return false;
			}
			
			if($this->error==true)
			{
				return false;
			}
			
			$query="
                            UPDATE
                                ".__DBC_SCHEMATA_ARTIST__."
                            SET
                                idSubscription = :idSubscription,
                                szSubscriptionProfileID = :szSubscriptionProfileID,
                                dtSubscriptionStartingDate = :dtSubscriptionStartingDate,
                                dtNextPaymentDate = :dtNextPaymentDate,
                                iMemberShipType = :iMemberShipType,
                                fMembershipCost = :fMembershipCost,					
                                iComplete = :iComplete,
                                iAccredited = :iAccredited,
                                dtUpdatedOn = now()
                            WHERE
                                id = :idArtist
                            AND
                                isDeleted = '0'
			";
			if($this->preapreStatement($query))
			{						
                            $this->bindSQL(':idArtist',(int)$idArtist,PDO::PARAM_INT);				
                            $this->bindSQL(':idSubscription',(int)$data['idSubscription'],PDO::PARAM_INT);
                            $this->bindSQL(':szSubscriptionProfileID',$data['szSubscriptionProfileID'],PDO::PARAM_STR);				
                            $this->bindSQL(':dtSubscriptionStartingDate',$data['dtSubscriptionStartingDate'],PDO::PARAM_STR);
                            $this->bindSQL(':dtNextPaymentDate',$data['dtNextPaymentDate'],PDO::PARAM_STR);				
                            $this->bindSQL(':iMemberShipType',(int)$data['iMemberShipType'],PDO::PARAM_INT);
                            $this->bindSQL(':fMembershipCost',(float)$data['fMembershipCost'],PDO::PARAM_INT);
                            $this->bindSQL(':iComplete',(int)$data['iCompleted'],PDO::PARAM_INT);
                            $this->bindSQL(':iAccredited',(int)$data['iAccredited'],PDO::PARAM_INT); 

                            if(($result = $this->executeSQL()))
                            {
                                if($iProfileCompleted==1 && $iSendEmail)
                                {
                                    $this->loadTyni($idArtist);
                                    $replace_arr = array();		

                                    $szLinkValue=__BASE_ADMIN_ARTIST_DETAILS_URL__."".$this->szArtistKey."/?action=Email";							

                                    $replace_arr['szLink']="<a href='".$szLinkValue."'>Click here to view profile </a>";
                                    $replace_arr['szHttpsLink'] = $szLinkValue ;								
                                    $replace_arr['szFullName'] = $this->szFirstName." ".$this->szLastName;
                                    $replace_arr['szArtistName'] = $this->szArtistName;
                                    $replace_arr['szArtistNumber'] = $this->szArtistNumber;
                                    $replace_arr['szSiteName'] = SITE_NAME;						
                                    $this->iFirstTimeCompleted = 1;
                                     
                                    createEmail(__ARTIST_ACCOUNT_COMPLETE_CONFIRMATION__,$replace_arr,$this->szEmail,__ARTIST_ACCOUNT_COMPLETE_CONFIRMATION_SUBJECT__,__STORE_SUPPORT_EMAIL__);
                                    createEmail(__VERIFY_ARTIST_ACCOUNT_BY_ADMIN__,$replace_arr,__STORE_SUPPORT_EMAIL__,__VERIFY_ARTIST_ACCOUNT_BY_ADMIN_SUBJECT__,__STORE_SUPPORT_FROM_EMAIL__);
                                } 
                                return true;
                            }
                            else
                            {
                                    return false;
                            }
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}

	/**
	 * This function is used to add all category from database.
	 * @access public
	 * @param string
	 * @return boolean 
	 * @author Ajay
	 */
	public function sendQuestionAboutDJ($data,$idArtist)
	{
		if(!empty($data) && $idArtist>0)
		{
			$this->set_szFullName(sanitize_all_html_input($data['szFullName']));			
			$this->set_szEmail(sanitize_all_html_input($data['szEmail']));
			$this->set_szComment(sanitize_all_html_input($data['szComment']));
			
			if($this->error==true)
			{
				return false;
			}
				
			$kArtist_new = new cArtist();
			$kArtist_new->loadTyni($idArtist);
			if(!empty($data['szBookingKey']))
			{
				$szBookingKey = $data['szBookingKey'];
				$kBooking = new cBooking();
				$kBooking->load(false,$szBookingKey); 
			} 
			if($kArtist_new->id<=0)
			{
				$this->addError('szFullName','DJ you have selected is either deleted or does not exists in our database. Please select other DJ from list.');
				return false;
			}
			$query="
				INSERT INTO
					".__DBC_SCHEMATA_ARTIST_COMMUNICATION__."
				(
					szFullName,
					szEmail,
					szComment,
					idArtist,
					szIPAddress,
					szReferer,
					szUserAgent,
					dtCreatedOn,
					iActive
				)
				VALUES
				(
					:szFullName,
					:szEmail,
					:szComment,
					:idArtist,
					:szIPAddress,
					:szReferer,
					:szUserAgent,
					now(),
					1
				)
			";	
			
			if($this->preapreStatement($query))
			{				
				$this->bindSQL(':szFullName',$this->szFullName,PDO::PARAM_STR);
				$this->bindSQL(':szEmail',$this->szEmail,PDO::PARAM_STR);			
				$this->bindSQL(':idArtist',(int)$idArtist,PDO::PARAM_INT);	
				$this->bindSQL(':szComment',$this->szComment,PDO::PARAM_STR);
				$this->bindSQL(':szReferer',__HTTP_REFERER__,PDO::PARAM_STR);				
				$this->bindSQL(':szIPAddress',__REMOTE_ADDR__,PDO::PARAM_STR);
				$this->bindSQL(':szUserAgent',__HTTP_USER_AGENT__,PDO::PARAM_STR);
				
				if(($result = $this->executeSQL()))
				{				
					$idArtType = $this->iLastInsertID ;
					$szLinkValue=__BASE_ADMIN_ARTIST_DETAILS_URL__."".$kArtist_new->szArtistKey."/?action=Email";	
						
					$replace_arr = array();	
					$replace_arr['szLink']="<a href='".$szLinkValue."'>Click here to view profile details of ".$kArtist_new->szArtistName."</a>";
					$replace_arr['szHttpsLink'] = $szLinkValue ;					
					$replace_arr['szFullName'] = $this->szFullName;	
					$replace_arr['szEmail'] = $this->szEmail;
					$replace_arr['szArtistName'] = $kArtist_new->szArtistName;
					$replace_arr['szQuestionText'] = $this->szComment; 
					$replace_arr['szSiteName'] = SITE_NAME;
					
					if($kBooking->id>0)
					{
						$replace_arr['szPostCode'] = $kBooking->szPostcode;
						$replace_arr['dtEventDate'] = date('d. F Y',strtotime($kBooking->dtStartDate));
						$replace_arr['szEventTiming'] = $kBooking->szPerformanceStartTime." - ".$kBooking->szPerformanceEndTime; 
						createEmail(__SEND_BOOKING_QUERY_DJ__,$replace_arr,__STORE_SUPPORT_EMAIL__,__REFERENCE_REQUEST_OF_DJ_SUBJECT__,__STORE_SUPPORT_EMAIL__);
					}
					else
					{
						createEmail(__SEND_QUESTION_ABOUT_DJ__,$replace_arr,__STORE_SUPPORT_EMAIL__,__REFERENCE_REQUEST_OF_DJ_SUBJECT__,__STORE_SUPPORT_EMAIL__);
					}  
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}		
	
	function getRevenueDataForDJDashboard($data,$iWeeklyView=false)
	{
	 	if(!empty($data))
	 	{	 		
	 		if($iWeeklyView)
	 		{
	 			$iMonthlyWeeklyView = 1;
	 		}
	 		else
	 		{
	 			$iMonthlyWeeklyView = 1;
	 		}
	 		
			$fromDate = strtotime($data['dtStartDate']);
			$toDate = strtotime($data['dtEndDate']);
			$counter = $fromDate ;
					
			$graph_data_ary = array();
	
		 	while($counter <= $toDate)
			{
				$key = date('Ym01',$counter);
				$dtFirstDateOfMonth = date('Ym01',$counter);
				$dtLastDateOfMonth = date('Ymt',$counter);
				
				$monthlyDataAry = array();
				$monthlyDataAry['dtStartDate'] = $dtFirstDateOfMonth ;
				$monthlyDataAry['dtEndDate'] = $dtLastDateOfMonth;
				$monthlyDataAry['idArtist'] = $data['idArtist'];
				
				$bookingRevenueAry = array();
	 			$bookingRevenueAry = $this->getBookingRevenue($monthlyDataAry,false,true);
	 			
				if(!empty($bookingRevenueAry))
					$graph_data_ary[$key]['fTotalBookingRevenue'] = $bookingRevenueAry['fTotalDailyEarning'];
				else
					$graph_data_ary[$key]['fTotalBookingRevenue'] = 0;
					
				$counter = $counter + (24*60*60*30);
			}
	 		return $graph_data_ary ;
	 	}
	 }
	 

	/**
	 * This function is used to add compared DJ to list
	 * @access public
	 * @param string
	 * @return boolean 
	 * @author Ajay
	 */
	function addArtistToCompare($kArtist,$kBooking)
	{
		if($kArtist->id>0 && $kBooking->id>0)
		{
			$userSessionData = array();
			$userSessionData = get_user_session_data(true);
			
			if($userSessionData['idUser']>0 && $userSessionData['iUserType']==__USER_TYPE_CLIENT__)
			{
				$idBuyer = $userSessionData['idUser'] ;
			}
			else
			{
				$szComparedDJKey = $_COOKIE['buyer_key_for_compare_dj'] ;
				if(empty($szComparedDJKey))
				{
					$szComparedDJKey = $this->generateUniqueToken(10);
					$szComparedDJKey = strtoupper($szComparedDJKey);
					setcookie('buyer_key_for_compare_dj' , $szComparedDJKey, time() + __COOKIE_TIME_CONSTANT__, __COOKIE_PATH__);
				}
			}
			
			$idArtist = $kArtist->id;
			$szBookingID = $kBooking->szBookingRandomKey ;
			$szPostcode = $kBooking->szPostcode;
			$dtEventDate = $kBooking->dtEventDate;
			$iStartHour = $kBooking->iStartHour;
			$iEndHour = $kBooking->iEndHour;
			
			$query="
				INSERT INTO
					".__DBC_SCHEMATA_COMPARED_DJ_LIST__."
				(
					szBuyerRandomKey,
					idBuyer,
					idArtist,
					szBookingID,
					szPostcode,
					dtEventDate,
					iStartHour,
					iEndHour,
					dtCreatedOn,
					iActive
				)
				VALUES
				(
					:szBuyerRandomKey,
					:idBuyer,
					:idArtist,
					:szBookingID,
					:szPostcode,
					:dtEventDate,
					:iStartHour,
					:iEndHour,
					now(),
					1
				)
			";	
			
			if($this->preapreStatement($query))
			{				
				$this->bindSQL(':szBuyerRandomKey',$szComparedDJKey,PDO::PARAM_STR);
				$this->bindSQL(':idBuyer',(int)$idBuyer,PDO::PARAM_INT);			
				$this->bindSQL(':idArtist',(int)$idArtist,PDO::PARAM_INT);	
				$this->bindSQL(':szBookingID',$szBookingID,PDO::PARAM_STR);		
						
				$this->bindSQL(':szPostcode',$szPostcode,PDO::PARAM_STR);	
				$this->bindSQL(':dtEventDate',$dtEventDate,PDO::PARAM_STR);	
				$this->bindSQL(':iStartHour',$iStartHour,PDO::PARAM_STR);	
				$this->bindSQL(':iEndHour',$iEndHour,PDO::PARAM_STR);	
				
				if(($result = $this->executeSQL()))
				{					
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}		
	}
	
	public function removeArtistFromCompare($kArtist,$kBooking)
	{
		if($kArtist->id>0 && $kBooking->id>0)
		{
			$userSessionData = array();
			$userSessionData = get_user_session_data(true);
				
			if($userSessionData['idUser']>0 && $userSessionData['iUserType']==__USER_TYPE_CLIENT__)
			{
				$idBuyer = $userSessionData['idUser'] ;
				$query_and = " AND  idBuyer = '".(int)$idBuyer."' ";
			}
			else
			{
				$szComparedDJKey = $_COOKIE['buyer_key_for_compare_dj'] ;
				$szComparedDJKey = sanitize_all_html_input(trim($szComparedDJKey));
				$query_and = " AND szBuyerRandomKey = '".$szComparedDJKey."' ";
			}
			
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_COMPARED_DJ_LIST__."
				WHERE
					idArtist = :idArtist	
					$query_and
			";
			
			if($this->preapreStatement($query))
			{	
				$this->bindSQL(':idArtist',(int)$kArtist->id,PDO::PARAM_INT);
				
				if( ( $result = $this->executeSQL() ) )
				{
					return true ;
				}
			}
			else 
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function getAllComparedDJList($idArtist=false,$get_count=false,$szBookingID=false)
	{
		$userSessionData = array();
		$userSessionData = get_user_session_data(true);
			
		if($userSessionData['idUser']>0 && $userSessionData['iUserType']==__USER_TYPE_CLIENT__)
		{
			$idBuyer = $userSessionData['idUser'] ;
			$query_and = " AND  cdl.idBuyer = '".(int)$idBuyer."' ";
		}
		else
		{
			$szComparedDJKey = $_COOKIE['buyer_key_for_compare_dj'] ;
			$szComparedDJKey = sanitize_all_html_input(trim($szComparedDJKey));
			$query_and = " AND cdl.szBuyerRandomKey = '".$szComparedDJKey."' ";
		}
		
		if($idArtist>0)
		{
			$query_and .= " AND cdl.idArtist = '".(int)$idArtist."' ";
		}
		
		if(!empty($szBookingID))
		{
			$query_and .= " AND cdl.szBookingID = '".$szBookingID."' ";
		}
		
		
		if($get_count)
		{
			$query_select = " count(cdl.id) iNumshortlisteCount ";
		}
		else
		{
			$query_select = "
				cdl.id,
				cdl.szBuyerRandomKey,
				cdl.idBuyer,
				cdl.idArtist,
				cdl.szBookingID,
				cdl.szPostcode,
				cdl.dtEventDate,
				cdl.iStartHour,
				cdl.iEndHour,
				cdl.dtCreatedOn,
				cdl.iActive,
				art.szArtistKey,
				art.szArtistNumber,
				art.szArtistName,
				art.fBaseFee,
				art.fFeePerHour,
				art.fTotalEarning,
				art.szPhoto1,
				art.szPhoto2,
				art.szPhoto3,
				art.szPhoto4,
				art.iYearStarted,
				art.iEquipment,
				art.fAvgRating,
				(SELECT  at1.szArtName FROM ".__DBC_SCHEMATA_ART_TYPE__." at1 WHERE at1.id = art.idPrimaryArtType ) as szPrimaryArtType,
				(SELECT  at2.szArtName FROM ".__DBC_SCHEMATA_ART_TYPE__." at2 WHERE at2.id = art.idSecondaryArtType ) as szSecondaryArtType
			";
			$query_join = " INNER JOIN ".__DBC_SCHEMATA_ARTIST__." art ON cdl.idArtist = art.id ";
			$query_and .= " AND art.isDeleted='0' AND art.iActive='1' ";
		}
		$query="
			SELECT
				$query_select
			FROM
				".__DBC_SCHEMATA_COMPARED_DJ_LIST__." cdl
				$query_join
			WHERE
				cdl.iActive = '1'
			AND
				cdl.isDeleted = '0'
			$query_and
		";
		//echo "<br>".$query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$ret_ary = array();
				$ctr = 0;
				while($row = $this->getAssoc())
				{
					$ret_ary[$ctr] = $row;
					$ctr++;
				}
				return $ret_ary;
			}
			else
			{
				return false ;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
    
    /**
     * Function to Get List of all Reviews
     * @param string $sortby
     * @param string $orderBy
     * @param Array $search_fields
     * @return array $get_all_review
     */
    function get_all_reviews($sortby,$orderBy,$search_fields='',$page=0)
    {
        $get_all_review=array();
        $serach_query='';
        $limitQuery='';
	    if($page>=1)
	    {
	        
			$countervalue=$page-1;
			$from=($countervalue*REVIEW_PER_PAGE);
		 	$to=REVIEW_PER_PAGE;
		 			
		 	$limitQuery="
		 		LIMIT
				$from,$to
		 	";
		}
        if($search_fields!='')
        {
            if($search_fields['szDJName']!='')
            {
                $serach_query .=" AND (a.szFirstName LIKE :firstName OR a.szLastName LIKE :lastName)";
            }
            if($search_fields['szReviewerName']!='')
            {
                $serach_query .=" AND r.szReviewerFullName LIKE :reviewerFullName";
            }
            if($search_fields['iRating']!='')
            {
                $serach_query .=" AND r.iRating= :rating";
            }
            
        }
		$query="
				SELECT 
					r.id,
                    r.idBooking,
                    r.idArtist,
                    r.idBuyer,
                    r.idReference,
                    r.szReviewerFullName,
                    r.iRating,
                    r.szReview,
                    r.szSuggestion,
                    r.dtCreated,
                    r.dtUpdated,
                    r.isDeleted,
                    a.szFirstName,
                    a.szLastName,
                    a.szArtistName
				FROM
					".__DBC_SCHEMATA_RATING_REVIEW__." AS r
                INNER JOIN
                    ".__DBC_SCHEMATA_ARTIST__." AS  a    
                ON
                    a.id=r.idArtist        
                WHERE
                    r.isDeleted=0
                AND
                    a.isDeleted=0
                    $serach_query
                        
				";
//		echo $query ;
		if($sortby!='')
		{
            if($sortby=='szArtistName')
            {
                $sortby="a.szArtistName ".$orderBy;
            }
            else
            {
                $sortby='r.'.$sortby." ".$orderBy;
            }
			$query.=" ORDER BY ".$sortby;		
		}
        if($limitQuery!='')
        {
            $query.=$limitQuery;
        }
//		echo $query ;
        if($this->preapreStatement($query))
		{	
            if($search_fields['szReviewerName']!='')
            {
                $this->bindSQL(':reviewerFullName',"%".$search_fields['szReviewerName']."%",PDO::PARAM_STR);
            }
            if($search_fields['iRating']!='')
            {
                $this->bindSQL(':rating',(int)$search_fields['iRating'],PDO::PARAM_INT);
            }
            if($search_fields['szDJName']!='')
            {
                $this->bindSQL(':firstName',$search_fields['szDJName'],PDO::PARAM_STR);
                $this->bindSQL(':lastName',$search_fields['szDJName'],PDO::PARAM_STR);
            }
            if( ( $result=$this->executeSQL($query)))
            {
                   if ($this->getRowCnt() > 0)
                    {
                        while($row = $this->getAssoc($result))		
                                $get_all_review[] = $row;	

                            return $get_all_review;
                    }
            }
            else 
            {
                $this->addError( "name", "There is no Review for you..." );
                return array();
            }
        }
    }
    
    /**
     * Function to Delete Review By Its Id
     * @param int $idReview
     * @return boolean
     */
    function deleteReview($idReview)
    {
        if($idReview>0)
		{
			$query="
				UPDATE 
					".__DBC_SCHEMATA_RATING_REVIEW__."
				SET
					isDeleted=1,
                    dtUpdated=NOW()
                WHERE
                    id=:idReview
			";
			
			if($this->preapreStatement($query))
			{	
				$this->bindSQL(':idReview',(int)$idReview,PDO::PARAM_INT);
				
				if( ( $result = $this->executeSQL() ) )
				{
					return true ;
				}
			}
			else 
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
    }
	
    /**
     * Function to Load Particular Review by Its Id
     * @param int $idReview
     * @return Array $review
     */
    function load_review_by_id($idReview)
    {
        $review=array();
        $query="
				SELECT 
					*
				FROM
					".__DBC_SCHEMATA_RATING_REVIEW__."
                WHERE
                    id=:idReview
                AND    
                    isDeleted=0
				";
//		echo $query ;
        if($this->preapreStatement($query))
		{	
			$this->bindSQL(':idReview',(int)$idReview,PDO::PARAM_INT);
            
            if( ( $result=$this->executeSQL($query)))
            {
                   if ($this->getRowCnt() > 0)
                    {
                        $review = $this->getAssoc($result);

                            return $review;
                    }
            }
        }
        else 
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
    }
    
	/**
	 * following functions are used validate and set object variable of this class 
	 */
	 
	/**
	 * This function used to validate User name
	 */
	public function set_szUserName($value)
	{
		$this->szUserName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szUserName", "User Name", 6, 32, true );
	}
	public function set_dtCreatedDateFrom($value)
	{
		$this->dtCreatedDateFrom = $this->validateInput( $value, __VLD_CASE_DATE__, "dtCreatedDateFrom", "Registered From Date", false, false, false );
	}
	public function set_dtCreatedDateTo($value)
	{
		$this->dtCreatedDateTo = $this->validateInput( $value, __VLD_CASE_DATE__, "dtCreatedDateTo", "Registered To Date", false, false, false );
	}
	/**
	 * This function used to validate Email
	 */
	public function set_szEmail($value)
	{
		$this->szEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szEmail", "Email Address", false, 255, true );
	}
	/**
	 * This function used to validate Password
	 */
	public function set_szPassword($value)
	{
		$this->szPassword = $this->validateInput( $value, __VLD_CASE_PASSWORD__, "szPassword", "Password", 6, 16, true );
	}
	/**
	 * This function used to validate Confirm Password
	 */
	public function set_szCPassword($value)
	{
		$this->szCPassword = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCPassword", "Confirm Password", false, false, true );
	}
	public function set_iUserType($value)
	{
		$this->iUserType = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iUserType", "User Type", 1, 2, true );
	}
	/**
	 * This function used to validate Login name
	 */
	public function set_szLoginUserName( $value )
	{
		$this->szUserName = $this->validateInput( $value, __VLD_CASE_EMAIL__, "account_login_username", "User Name", false, false, true );
	}	
	
	public function set_szFPUserName( $value ,$email_flag=false)
	{
		if($email_flag)
		{		
			$this->szUserName = $this->validateInput( $value, __VLD_CASE_EMAIL__, "account_fp_username", "Email", false, false, true );
		}
		else
		{
			$this->szUserName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "account_fp_username", "User Name", false, false, true );
		}
	}	
	
	/**
	 * This function used to validate Password
	 */
	public function set_szLoginPassword($value)
	{
		$this->szPassword = $this->validateInput( $value, __VLD_CASE_PASSWORD__, "account_login_password", "Password", 6, 16, true );
	}
	
	/**
	 * This function used to validate Password
	 */
	public function set_szOldPassword($value)
	{
		$this->szOldPassword = $this->validateInput( $value, __VLD_CASE_PASSWORD__, "szOldPassword", "Current Password", 6, 16, true );
	}
	
	/**
	 * This function used to validate Password
	 */
	public function set_szNewPassword($value)
	{
		$this->szNewPassword = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szNewPassword", "New Password", 6, 16, true );
	}
	
	/**
	 * This function used to validate Password
	 */
	public function set_szConNewPassword($value)
	{
		$this->szConNewPassword = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConNewPassword", "Confirm Password", 6, 16, true );
	}
	
	public function set_szFirstName($value,$flag=true)
	{
        
        $this->szFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szFirstName", "First Name", false, 255, $flag );
        
	}
	public function set_szFullName($value)
	{
		$this->szFullName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szFullName", "Full Name", false, 255, true );
	}
	public function set_szComment($value)
	{
		$this->szComment = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szComment", "Your Question", false, false, true );
	}
	public function set_szLastName($value,$flag=true)
	{
		$this->szLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLastName", "Last Name", false, 255, $flag );
	}
	
	public function set_szArtistName($value)
	{
		$this->szArtistName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szArtistName", "DJ Name", false, 255, true );
	}
	
	public function set_idPrimaryArtType($value,$flag=true)
	{
		$this->idPrimaryArtType = $this->validateInput( $value, __VLD_CASE_WHOLE_NUM__, "idPrimaryArtType", "Primary Music Genre", 1, false, $flag );
	}
	public function set_idSecondaryArtType($value)
	{
		$this->idSecondaryArtType = $this->validateInput( $value, __VLD_CASE_WHOLE_NUM__, "idSecondaryArtType", "Secondary Music Genre", 1, false, false );
	}
	
	public function set_szPhone($value,$flag=true)
	{
		$this->szPhone = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPhone", "Phone", 8, 15, $flag );
	}
	public function set_szAlternatePhone($value,$flag=true)
	{
		$this->szAlternatePhone = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAlternatePhone", "Backup Phone", 8, 15, $flag );
	}	
	public function set_dtDateOfBirth($value,$flag=true)
	{
		$this->dtDateOfBirth = $this->validateInput( $value, __VLD_CASE_DATE__, "dtDateOfBirth", "DOB", false, 255, $flag );
	}
	public function set_iDistanceWillingToTravel($value,$flag=true)
	{
		$this->iDistanceWillingToTravel = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iDistanceWillingToTravel", "Distance Wiling to Travel", 1, 100, $flag);
    }	
	public function set_szCaptchaCode($value)
	{
		$this->szCaptchaCode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCaptchaCode", "Security code", false, 255, true );
	}	
	public function set_szDescription($value,$flag=true)
	{
		$this->szDescription = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDescription", "Describe Yourself", false, 200, $flag );
	}
	public function set_szReferences($value)
	{
		$this->szReferences = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szReferences", "References", false, false, false );
	}
	
	public function set_szPostcode($value,$flag=true)
	{
		$this->szPostcode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPostcode", "Postcode", false, false, $flag );
	}
	public function set_szAddress($value,$flag=true)
	{
		$this->szAddress = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAddress", "Street Address", false, false, $flag );
	}
	public function set_szAddress2($value)
	{
		$this->szAddress2 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAddress2", "Street Address", false, false, false );
	}
	public function set_szCity($value,$flag=true)
	{
		$this->szCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCity", "City", false, false, $flag );
	}
	public function set_szCounty($value,$flag=true)
	{
		$this->szCounty = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCounty", "County", false, false, $flag );
	}	
	
	public function set_szSPostcode($value)
	{
		$this->szSPostcode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSPostcode", "Postcode", false, false, true );
	}
	public function set_szSAddress($value)
	{
		$this->szSAddress = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSAddress", "Street Address", false, false, true );
	}
	public function set_szSCity($value)
	{
		$this->szSCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSCity", "City", false, false, true );
	}
	public function set_szSCounty($value)
	{
		$this->szSCounty = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSCounty", "County", false, false, true );
	}	
	
	public function set_szCertification($value,$counter,$flag=false)
	{
		$this->szCertification[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCertification_".$counter, "Certification Number", false, false, $flag );
	}
	public function set_dtCertificateExpiry($value,$counter,$flag=false)
	{
		$this->dtCertificateExpiry[$counter] = $this->validateInput( $value, __VLD_CASE_DATE__, "dtCertificateExpiry_".$counter, "Certificate Expiry", false, false, $flag );
	}
	public function set_idCertificate($value,$counter)
	{
		$this->idCertificate[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idCertificate_".$counter, "Certificate ID", false, false, false );
	}
	
	public function set_szAwardTitle($value,$counter,$flag=false)
	{
		$this->szAwardTitle[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAwardTitle_".$counter, "Award Title", false, false, $flag );
	}
	public function set_dtAwardDate($value,$counter,$flag=false)
	{
		$this->dtAwardDate[$counter] = $this->validateInput( $value, __VLD_CASE_DATE__, "dtAwardDate_".$counter, "Award Date", false, false, $flag );
	}
	public function set_idAward($value,$counter)
	{
		$this->idAward[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idAward_".$counter, "Awards ID", false, false, false );
	}	
	public function set_szEquipmentName($value,$counter)
	{
		$this->szEquipmentName[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szEquipmentName_".$counter, "Equipment Name", false, false, false );
	}
	public function set_szEquipmentDescription($value,$counter)
	{
		$this->szEquipmentDescription[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szEquipmentDescription_".$counter, "Equipment Description", false, false, false );
	}
	
	public function set_szSpeakerDescription($value)
	{
		$this->szSpeakerDescription = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSpeakerDescription", "Description", 0, 100, true );
	}
	public function set_szDeckDescription($value)
	{
		$this->szDeckDescription = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDeckDescription", "Description", 0, 100, true );
	}
	public function set_szOtherDescription($value)
	{
		$this->szOtherDescription = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szOtherDescription", "Description", 0, 200, false );
	}
	public function set_fEquipmentCharge($value)
	{
		$this->fEquipmentCharge = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fEquipmentCharge", "Equipment Charge", 1, false, true );
	}
	public function set_dtPATExpiryDate($value)
	{
		$this->dtPATExpiryDate = $this->validateInput( $value, __VLD_CASE_DATE__, "dtPATExpiryDate", "Expiry Date", false, false, true );
	}	
	public function set_iPATCertificate($value)
	{
		$this->iPATCertificate = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iPATCertificate", "PAT Certificate", false, false, false );
	}		
	public function set_iMaxPerformanceLenght($value,$flag=true)
	{
		$this->iMaxPerformanceLenght = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iMaxPerformanceLenght", "Max Performance Lenght", false, false, $flag );
	}	
	public function set_iMemberShipType($value,$flag=true)
	{
		$this->iMemberShipType = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iMemberShipType", "Membership Type", false, false, $flag );
	}		
	
	public function set_szUploadEquipmentPhoto($value,$counter)
	{
		$this->szUploadEquipmentPhoto[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szUploadEquipmentPhoto_".$counter, "Equipment Photo", false, false, false );
	}
	public function set_iEquipentFileUpload($value,$counter)
	{
		$this->iEquipentFileUploads[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iEquipentFileUpload_".$counter, "Equipment Photo", false, false, false );
	}
	public function set_iYearStarted($value,$flag=true)
	{
		$this->iYearStarted = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iYearStarted", "Year Started", false, date('Y'), $flag );
	}
	public function set_iEquipment($value,$flag=true)
	{
		$this->iEquipment = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iEquipment", "Equipment", false, false, $flag);
	}

	public function set_fBaseFee($value,$flag=true)
	{
		$this->fBaseFee = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fBaseFee", "Fixed Fee", 50, false, $flag);
	}
	public function set_fFeePerHour($value,$flag=true)
	{
		$this->fFeePerHour = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fFeePerHour", "Fee Per Hour", 10, false, $flag);
	}
	
	public function set_szVenueName($value,$counter,$flag=false)
	{
		$this->szVenueName[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szVenueName_".$counter, "Location Name", false, false, $flag );
	}
	public function set_szVenuePhone($value,$counter)
	{
		$this->szVenuePhone[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szVenuePhone_".$counter, "Venue Phone", 8, 15, true );
	}	
	public function set_szVenueAlternatePhone($value,$counter)
	{
		$this->szVenueAlternatePhone[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szVenueAlternatePhone_".$counter, "Venue Alternate Phone", 8, 15, false );
	}
	public function set_szVenueDescription($value,$counter)
	{
		$this->szVenueDescription[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szVenueDescription_".$counter, "Description", false, false, false);
	}
	
	public function set_iTopLocation($value,$counter)
	{
		$this->iTopLocation[$counter] = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iTopLocation_".$counter, "Top Location", false, false, false );
	}	
	public function set_szVPostcode($value,$counter)
	{
		$this->szVPostcode[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szVPostcode_".$counter, "Postcode", false, false, true );
	}
	public function set_szVAddress($value,$counter)
	{
		$this->szVAddress[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szVAddress_".$counter, "Street Address", false, false, true );
	}	
	public function set_szVCity($value,$counter)
	{
		$this->szVCity[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szVCity_".$counter, "City", false, false, true );
	}
	public function set_szVCounty($value,$counter,$flag=false)
	{
		$this->szVCounty[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szVCounty_".$counter, "County", false, false, $flag );
	}	
	public function set_idLastLocationPlayed($value,$counter)
	{
		$this->idLastLocationPlayed[$counter] = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idLastLocationPlayed_".$counter, "County", false, false, false );
	}	
	public function set_dtStartDate($value,$counter,$flag=true)
	{
		$this->dtStartDate[$counter] = $this->validateInput( $value, __VLD_CASE_DATE__, "dtStartDate_".$counter, "Event Date", false, false, $flag );
	}
	public function set_dtEndDate($value,$counter)
	{
		$this->dtEndDate[$counter] = $this->validateInput( $value, __VLD_CASE_DATE__, "dtEndDate_".$counter, "End Date", false, false, true );
	}
	
	public function set_dtUnavailableFromDate($value,$counter)
	{
		$this->dtUnavailableFromDate[$counter] = $this->validateInput( $value, __VLD_CASE_DATE__, "dtUnavailableFromDate_".$counter, "Start Date", false, false, true );
	}
	public function set_dtUnavailableToDate($value,$counter)
	{
		$this->dtUnavailableToDate[$counter] = $this->validateInput( $value, __VLD_CASE_DATE__, "dtUnavailableToDate_".$counter, "End Date", false, false, true );
	}
		
	public function set_iStartHour($value,$counter)
	{
		$this->iStartHour[$counter] = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iStartHour_".$counter, "Start Hour", false, false, false );
	}		
	public function set_iStartMinute($value,$counter)
	{
		$this->iStartMinute[$counter] = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iStartMinute_".$counter, "Start Minute", false, false, false );
	}
	
	public function set_iEndHour($value,$counter)
	{
		$this->iEndHour[$counter] = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iEndHour_".$counter, "End Hour", false, false, false );
	}		
	public function set_iEndMinute($value,$counter)
	{
		$this->iEndMinute[$counter] = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iEndMinute_".$counter, "End Minute", false, false, false );
	}
	public function set_idUnavailableDates($value,$counter)
	{
		$this->idUnavailableDates[$counter] = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idUnavailableDates_".$counter, "ID", false, false, false );
	}
	
	public function set_szVideo($value,$counter)
	{
		$this->szVideo[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szVideo_".$counter, "Video", false, false, false );
	}
	public function set_szUploadFileName($value,$counter,$photo_flag=false)
	{
		$this->szUploadFileName[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPhoto_".$counter, "Photo", false, false, $photo_flag);
	}
	
	function set_iFileUpload( $value,$counter )
	{
		$this->iFileUpload[$counter] = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iFileUpload_".$counter, "File Upload", false, false, false );
	}	
	public function set_szArtName($value,$counter)
	{
		$this->szArtName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szArtName", "Art Name", false, false, true );
	}
	function set_iCategoryLevel( $value )
	{
		$this->iCategoryLevel = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iCategoryLevel", "Music Genre", 1, 2, true );
	}
	function set_iDisplayOrder( $value )
	{
		$this->iDisplayOrder = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "szDescription", "Display order", 1, false, true );
	}
	
	function set_iSuccess( $value )
	{
		$this->iSuccess = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iSuccess", "Success Value", false, false, true );
	}	
	function set_id( $value )
	{
		$this->id = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "id", "User ID", false, false, true );
	}
	
	public function set_szRefName($value,$counter,$flag=false)
	{
		$this->szRefName[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szRefName_".$counter, "Name", false, false, $flag );
	}
	public function set_szRefBussinessName($value,$counter,$flag=false)
	{
		$this->szRefBussinessName[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szRefBussinessName_".$counter, "Business Name", false, false, $flag );
	}
	public function set_szRefPosition($value,$counter,$flag=false)
	{
		$this->szRefPosition[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szRefPosition_".$counter, "Position", false, false, $flag );
	}	
	public function set_szRefEmail($value,$counter,$flag=false)
	{
		$this->szRefEmail[$counter] = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szRefEmail_".$counter, "Email", false, false, $flag );
	}	
	function set_idReference( $value,$counter )
	{
		$this->idReference[$counter] = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idReference_".$counter, "Reference ID", false, false, false );
	}					
				
	public function set_szFeaturedQuote($value,$flag=true)
	{
		$this->szFeaturedQuote = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szFeaturedQuote", "Feature Quote", false, false, $flag );
	}			
				
				
}
?>