<?php
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
//error_reporting (E_ALL);
//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 1);
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/forwarders/listExcelData.php" );
require_once(__APP_PATH_CLASSES__."/warehouseSearch.class.php");
require_once(__APP_PATH_CLASSES__."/rss.class.php");

Class cExport_Import extends cDatabase
{
	var $successLine;
	var $errorLine;
	var $deleteLine;
	var $t_base='BulkUpload/';
	
	function __construct()
	{
		parent::__construct();
		return true;
	}
	
	function exportData($objPHPExcel,$sheetindex,$fileName,$kConfig,$desarr,$orginarr,$idForwarder)
	{
			date_default_timezone_set('UTC');	
			if($sheetindex>0)
			{
                            $objPHPExcel->createSheet();
			}
			
			$objPHPExcel->setActiveSheetIndex($sheetindex);
						
			$styleArray = array(
				'font' => array(
					'bold' => false,
					'size' =>12,
				),
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				),
				'borders' => array(
					'top' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'bottom' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'right' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'left' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
				),
			);
			
			
			$styleArray1 = array(
				'font' => array(
					'bold' => false,
					'size' =>12,
				),
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
				),
				'borders' => array(
					'top' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'bottom' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'right' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'left' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
				),
			);
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(10);			
			$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(15);
			
			
		
			$objPHPExcel->getActiveSheet()->getStyle('A1:AA1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$objPHPExcel->getActiveSheet()->getStyle('A1:AA1')->getFill()->getStartColor()->setARGB('FFddd9c3');
			
							
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			
			$objPHPExcel->getActiveSheet()->mergeCells('A1:C1');
			$objPHPExcel->getActiveSheet()->setCellValue('A1','Origin')->getStyle('A1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->applyFromArray($styleArray);
		//	$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->mergeCells('D1:F1');
			$objPHPExcel->getActiveSheet()->setCellValue('D1','Destination')->getStyle('D1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('D1:F1')->applyFromArray($styleArray);
			//$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->mergeCells('G1:J1');
			$objPHPExcel->getActiveSheet()->setCellValue('G1','Origin Charges(CFS receiving to Port)')->getStyle('G1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('G1:J1')->applyFromArray($styleArray);
			//$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->mergeCells('K1:N1');
			$objPHPExcel->getActiveSheet()->setCellValue('K1','Freight and Surcharges(Port to Port)')->getStyle('K1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('K1:N1')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->mergeCells('O1:R1');
			$objPHPExcel->getActiveSheet()->setCellValue('O1','Destination Charges(Port to available at CFS)')->getStyle('O1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('O1:R1')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('S1','Service')->getStyle('S1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->mergeCells('T1:U1');
			$objPHPExcel->getActiveSheet()->setCellValue('T1','Origin CFS Cut Off')->getStyle('T1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('T1:U1')->applyFromArray($styleArray);
			//$objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('V1','Transit Time')->getStyle('V1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('V1')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->mergeCells('W1:X1');
			$objPHPExcel->getActiveSheet()->setCellValue('W1','Destination Cargo Available')->getStyle('W1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('W1:X1')->applyFromArray($styleArray);
			//$objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('Y1','Booking Cut Off')->getStyle('Y1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('Y1')->applyFromArray($styleArray);
			
			
			$objPHPExcel->getActiveSheet()->mergeCells('Z1:AA1');
			$objPHPExcel->getActiveSheet()->setCellValue('Z1','Validity')->getStyle('Z1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('Z1:AA1')->applyFromArray($styleArray);
			//$objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('A2','Country')->getStyle('A2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('B2','City')->getStyle('B2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('C2','Origin CFS')->getStyle('B2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('C2')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			
			$objPHPExcel->getActiveSheet()->setCellValue('D2','Country')->getStyle('D2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('E2','City')->getStyle('E2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('F2','Destination CFS')->getStyle('F2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('F2')->applyFromArray($styleArray);
			
			
			$objPHPExcel->getActiveSheet()->setCellValue('G2','Per W/M')->getStyle('G2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('G2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('H2','Minimum')->getStyle('H2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('H2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('I2','Per Booking')->getStyle('I2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('I2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('J2','Currency')->getStyle('J2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('J2')->applyFromArray($styleArray);
						
			$objPHPExcel->getActiveSheet()->setCellValue('K2','Per W/M')->getStyle('K2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('K2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('L2','Minimum')->getStyle('L2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('L2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('M2','Per Booking')->getStyle('M2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('M2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('N2','Currency')->getStyle('N2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('N2')->applyFromArray($styleArray);
			
			
			$objPHPExcel->getActiveSheet()->setCellValue('O2','Per W/M')->getStyle('O2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('O2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('P2','Minimum')->getStyle('P2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('P2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('Q2','Per Booking')->getStyle('Q2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('Q2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('R2','Currency')->getStyle('R2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('R2')->applyFromArray($styleArray);			
			
			
			$objPHPExcel->getActiveSheet()->setCellValue('S2','Frequency')->getStyle('S2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('S2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('T2','Day')->getStyle('T2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('T2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('U2','Local Time')->getStyle('U2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('U2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('V2','Day')->getStyle('V2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('V2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('W2','Day')->getStyle('W2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('W2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('X2','Local Time')->getStyle('X2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('X2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('Y2','Days')->getStyle('Y2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('Y2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('Z2','From date')->getStyle('Z2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('Z2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('AA2','Expiry date')->getStyle('AA2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('AA2')->applyFromArray($styleArray);
							
                        $query="
                            SELECT
                                id
                            FROM
                                ".__DBC_SCHEMATA_WAREHOUSES__."
                            WHERE
                                idForwarder='".(int)$idForwarder."'	 
                        ";
                        if( ( $result = $this->exeSQL( $query ) ) )
                        {
                            if ($this->getRowCnt() > 0)
                            {	 
                                require_once(__APP_PATH_CLASSES__."/PHPExcel/Cell/AdvancedValueBinder.php"); 

                                PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_AdvancedValueBinder() );

                                //$fileName=__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/Transporteca-LCL-Services-upload-sheet_1.xlsx";
                                $this->exportListData($objPHPExcel,'1',$fileName);
                                $i=0;		
                                $rowcounter=2;
                                foreach($orginarr as $orginarrs)
                                {		 
                                    $result_array =array(); 
                                    $array_orign = array();
                                    foreach($desarr as $desarrs)
                                    { 
                                      if($desarrs!=$orginarrs)
                                      {
								$query="
									SELECT
										szCutOffLocalTime,
										szAvailableLocalTime,
										fOriginChargeRateWM,
										fOriginChargeMinRateWM,
										fOriginChargeBookingRate,
										szOriginChargeCurrency,
										fFreightRateWM as fRateWM,
										fFreightMinRateWM as fMinRateWM,
										fFreightBookingRate as fRate,
										fDestinationChargeRateWM,
										fDestinationChargeMinRateWM,
										fDestinationChargeBookingRate,
										szDestinationChargeCurrency,
										dtExpiry,
										dtValidFrom,
										iBookingCutOffHours,
										idCutOffDay,
										idAvailableDay,
										idFrequency,
										szFreightCurrency
									FROM
										".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
									WHERE
										idWarehouseFrom='".(int)$orginarrs."'
									AND
										idWarehouseTo='".(int)$desarrs."'
									AND
										iActive='1'
									ORDER BY
										idCutOffDay ASC		
									";
									if( ( $result1 = $this->exeSQL( $query ) ) )
									{										
										if ($this->getRowCnt() > 0)
										{
											while($row1 = $this->getAssoc($result1))
											{
												$query="
                                                                                                        SELECT
                                                                                                            szCity,
                                                                                                            idCountry,
                                                                                                            szWareHouseName
                                                                                                        FROM
                                                                                                            ".__DBC_SCHEMATA_WAREHOUSES__."
                                                                                                        WHERE
                                                                                                            id='".(int)$orginarrs."' 
													";
												$result2 = $this->exeSQL( $query );
												$row2 = $this->getAssoc($result2);
												$array_orign[$i]['orgin_city']=$row2['szCity'];
												$array_orign[$i]['orgin_country']=$kConfig->getCountryName($row2['idCountry']);
												$array_orign[$i]['orgin_cfs']=$row2['szWareHouseName'];
												$array_orign[$i]['from_id']=$orginarrs;
												$query="
														SELECT
															szCity,
															idCountry,
															szWareHouseName
														FROM
															".__DBC_SCHEMATA_WAREHOUSES__."
														WHERE
                                                                                                                    id='".(int)$desarrs."'	 
													";
												$result3 = $this->exeSQL( $query );
												$row3 = $this->getAssoc($result3);
												$array_orign[$i]['des_city']=$row3['szCity'];
												$array_orign[$i]['des_country']=$kConfig->getCountryName($row3['idCountry']);
												$array_orign[$i]['des_cfs']=$row3['szWareHouseName'];
												$array_orign[$i]['to_id']=$desarrs;
												
																																		
											$array_orign[$i]['szCutOffLocalTime']=$row1['szCutOffLocalTime'];
											$array_orign[$i]['szAvailableLocalTime']=$row1['szAvailableLocalTime'];
											$array_orign[$i]['fRateWM']=$row1['fRateWM'];
											$array_orign[$i]['fMinRateWM']=$row1['fMinRateWM'];
											$array_orign[$i]['fRate']=$row1['fRate'];
											$array_orign[$i]['dtExpiry']=$row1['dtExpiry'];
											$array_orign[$i]['dtValidFrom']=$row1['dtValidFrom'];
											$array_orign[$i]['iTransitHours']=$row1['iTransitDays'];
											
											$array_orign[$i]['fOriginChargeRateWM']=$row1['fOriginChargeRateWM'];
											$array_orign[$i]['fOriginChargeMinRateWM']=$row1['fOriginChargeMinRateWM'];
											$array_orign[$i]['fOriginChargeBookingRate']=$row1['fOriginChargeBookingRate'];
											$array_orign[$i]['szOriginChargeCurrency']=$row1['szOriginChargeCurrency'];
											
											$array_orign[$i]['fDestinationChargeRateWM']=$row1['fDestinationChargeRateWM'];
											$array_orign[$i]['fDestinationChargeMinRateWM']=$row1['fDestinationChargeMinRateWM'];
											$array_orign[$i]['fDestinationChargeBookingRate']=$row1['fDestinationChargeBookingRate'];
											$array_orign[$i]['szDestinationChargeCurrency']=$row1['szDestinationChargeCurrency'];
											
											if($row1['iBookingCutOffHours']>'48')
											{
												$array_orign[$i]['iBookingCutOffHours']=ceil(($row1['iBookingCutOffHours']/24))." days";
											}
											else
											{
												$array_orign[$i]['iBookingCutOffHours']=$row1['iBookingCutOffHours']." hours";
											}
											$array_orign[$i]['idCutOffDay']=$this->getWeekday($row1['idCutOffDay']);
											$array_orign[$i]['idAvailableDay']=$this->getWeekday($row1['idAvailableDay']);
											$array_orign[$i]['idFrequency']=$this->getFrequency($row1['idFrequency']);
											$array_orign[$i]['szFreightCurrency']=$this->getFreightCurrency($row1['szFreightCurrency']);
												++$i;
											}
											
										}
										else
										{
											$query="
													SELECT
														szCity,
														idCountry,
														szWareHouseName
													FROM
														".__DBC_SCHEMATA_WAREHOUSES__."
													WHERE
														id='".(int)$orginarrs."'
												";
											$result2 = $this->exeSQL( $query );
											$row2 = $this->getAssoc($result2);
											$array_orign[$i]['orgin_city']=$row2['szCity'];
											$array_orign[$i]['orgin_country']=$kConfig->getCountryName($row2['idCountry']);
											$array_orign[$i]['orgin_cfs']=$row2['szWareHouseName'];
											$array_orign[$i]['from_id']=$orginarrs;
											$query="
													SELECT
                                                                                                            szCity,
                                                                                                            idCountry,
                                                                                                            szWareHouseName
													FROM
                                                                                                            ".__DBC_SCHEMATA_WAREHOUSES__."
													WHERE
                                                                                                            id='".(int)$desarrs."'
												";
											$result3 = $this->exeSQL( $query );
											$row3 = $this->getAssoc($result3);
											$array_orign[$i]['des_city']=$row3['szCity'];
											$array_orign[$i]['des_country']=$kConfig->getCountryName($row3['idCountry']);
											$array_orign[$i]['des_cfs']=$row3['szWareHouseName'];
											$array_orign[$i]['to_id']=$desarrs;
											
											++$i;
												
										}
										
									}
								}								
							}
							
					   		$result_array = $array_orign;
								
						   	if(!empty($result_array))
						   	{			   		
						   		foreach($result_array as $result_arrays)
						   		{
						   			$validdate='';
						   			$expirydate=' ';
						   			if(isset($result_arrays['dtValidFrom'])!='0000-00-00 00:00:00' && isset($result_arrays['dtValidFrom'])!="")
						   			{
						   				$validdate_arr=explode('-',$result_arrays['dtValidFrom']);
										$validdate = gmmktime(0,0,0,$validdate_arr[1],$validdate_arr[2],$validdate_arr[0]);
						   			}
						   			else
						   			{
						   				$validdate=gmmktime(0,0,0,date('m'),date('d'),date('Y'));
						   			}
						   			
						   			if(isset($result_arrays['dtExpiry'])!='0000-00-00 00:00:00' && isset($result_arrays['dtExpiry'])!="")
						   			{
									
										$expirytime_arr=explode('-',$result_arrays['dtExpiry']);
										$expirydate= gmmktime(0,0,0,$expirytime_arr[1],$expirytime_arr[2],$expirytime_arr[0]);
							   			
						   			}
						   			
						   			if(isset($result_arrays['fRateWM'])!='')
						   			{
						   				$fRateWM=$result_arrays['fRateWM'];
						   			}
						   			else
						   			{
						   				$fRateWM='';
						   			}
						   			
						   			if(isset($result_arrays['fMinRateWM'])!='')
						   			{
						   				$fMinRateWM=$result_arrays['fMinRateWM'];
						   			}
						   			else
						   			{
						   				$fMinRateWM='';
						   			}
						   			
						   			if(isset($result_arrays['fRate'])!='')
						   			{
						   				$fRate=$result_arrays['fRate'];
						   			}
						   			else
						   			{
						   				$fRate='';
						   			}
						   			
						   			if(isset($result_arrays['szFreightCurrency'])!='')
						   			{
						   				$szFreightCurrency=$result_arrays['szFreightCurrency'];
						   			}
						   			else
						   			{
						   				$szFreightCurrency='';
						   			}
						   			
						   		    if(isset($result_arrays['fOriginChargeRateWM'])!='')
						   			{
						   				$fOriginChargeRateWM=$result_arrays['fOriginChargeRateWM'];
						   			}
						   			else
						   			{
						   				$fOriginChargeRateWM='';
						   			}
						   			
						   			if(isset($result_arrays['fOriginChargeMinRateWM'])!='')
						   			{
						   				$fOriginChargeMinRateWM=$result_arrays['fOriginChargeMinRateWM'];
						   			}
						   			else
						   			{
						   				$fOriginChargeMinRateWM='';
						   			}
						   			
						   			if(isset($result_arrays['fOriginChargeBookingRate'])!='')
						   			{
						   				$fOriginChargeBookingRate = $result_arrays['fOriginChargeBookingRate'];
						   			}
						   			else
						   			{
						   				$fOriginChargeBookingRate='';
						   			}
						   			
						   			if(isset($result_arrays['szOriginChargeCurrency'])!='')
						   			{
						   				$szOriginChargeCurrency = $result_arrays['szOriginChargeCurrency'];
						   			}
						   			else
						   			{
						   				$szOriginChargeCurrency = '';
						   			}
						   			
						   			if(isset($result_arrays['fDestinationChargeRateWM'])!='')
						   			{
						   				$fDestinationChargeRateWM = $result_arrays['fDestinationChargeRateWM'];
						   			}
						   			else
						   			{
						   				$fDestinationChargeRateWM = '';
						   			}
						   			
						   			if(isset($result_arrays['fDestinationChargeMinRateWM'])!='')
						   			{
						   				$fDestinationChargeMinRateWM = $result_arrays['fDestinationChargeMinRateWM'];
						   			}
						   			else
						   			{
						   				$fDestinationChargeMinRateWM = '';
						   			}
						   			
						   			if(isset($result_arrays['fDestinationChargeBookingRate'])!='')
						   			{
						   				$fDestinationChargeBookingRate = $result_arrays['fDestinationChargeBookingRate'];
						   			}
						   			else
						   			{
						   				$fDestinationChargeBookingRate = '';
						   			}
						   			
						   			if(isset($result_arrays['szDestinationChargeCurrency'])!='')
						   			{
						   				$szDestinationChargeCurrency = $result_arrays['szDestinationChargeCurrency'];
						   			}
						   			else
						   			{
						   				$szDestinationChargeCurrency = '';
						   			}
						   			
						   			if(isset($result_arrays['idFrequency'])!='')
						   			{
						   				$idFrequency=$result_arrays['idFrequency'];
						   			}
						   			else
						   			{
						   				$idFrequency='';
						   			}
						   			
						   			if(isset($result_arrays['idCutOffDay'])!='')
						   			{
						   				$idCutOffDay=$result_arrays['idCutOffDay'];
						   			}
						   			else
						   			{
						   				$idCutOffDay='';
						   			}
						   			
						   			if(isset($result_arrays['szCutOffLocalTime'])!='')
						   			{
						   				$szCutOffLocalTime=$result_arrays['szCutOffLocalTime'];
						   			}
						   			else
						   			{
						   				$szCutOffLocalTime='';
						   			}
						   			
						   			if(isset($result_arrays['iTransitHours'])!='')
						   			{
						   				$iTransitHours=$result_arrays['iTransitHours'];
						   			}
						   			else
						   			{
						   				$iTransitHours='';
						   			}
						   			
						   			if(isset($result_arrays['idAvailableDay'])!='')
						   			{
						   				$idAvailableDay=$result_arrays['idAvailableDay'];
						   			}
						   			else
						   			{
						   				$idAvailableDay='';
						   			}
						   			
						   			if(isset($result_arrays['szAvailableLocalTime'])!='')
						   			{
						   				$szAvailableLocalTime=$result_arrays['szAvailableLocalTime'];
						   			}
						   			else
						   			{
						   				$szAvailableLocalTime='';
						   			}
						   			
						   			if(isset($result_arrays['iBookingCutOffHours'])!='')
						   			{
						   				$iBookingCutOffHours=$result_arrays['iBookingCutOffHours'];
						   			}
						   			else
						   			{
						   				$iBookingCutOffHours='';
						   			}
						   			
						   								   			
						   			//echo $expirydate."<br/>";
						   			
									$colcounter=0;
									$rowcounter++;
									$fillColor='A'.$rowcounter.':F'.$rowcounter;
									$fill_color='A'.$rowcounter.':S'.$rowcounter;
									$objPHPExcel->getActiveSheet()->getStyle($fillColor)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
									$objPHPExcel->getActiveSheet()->getStyle($fillColor)->getFill()->getStartColor()->setARGB('FFddd9c3');
									//$objPHPExcel->getActiveSheet()->getStyle($fillColor)->applyFromArray($styleArray);
									
									$objPHPExcel->getActiveSheet()->getStyle('O'.$rowcounter)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
									$objPHPExcel->getActiveSheet()->getStyle('O'.$rowcounter)->getFill()->getStartColor()->setARGB('FFddd9c3');
									$objPHPExcel->getActiveSheet()->getStyle('O'.$rowcounter)->applyFromArray($styleArray);
									
									$objPHPExcel->getActiveSheet()->getStyle('G'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0.00');
									$objPHPExcel->getActiveSheet()->getStyle('H'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0.00');
									$objPHPExcel->getActiveSheet()->getStyle('I'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0.00');
									
									$objPHPExcel->getActiveSheet()->getStyle('K'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0.00');
									$objPHPExcel->getActiveSheet()->getStyle('L'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0.00');
									$objPHPExcel->getActiveSheet()->getStyle('M'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0.00');
									//$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
									
									$objPHPExcel->getActiveSheet()->getStyle('O'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0.00');
									$objPHPExcel->getActiveSheet()->getStyle('P'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0.00');
									$objPHPExcel->getActiveSheet()->getStyle('Q'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0.00');
																											
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$result_arrays['orgin_country'])->getStyle($colcounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('A'.$rowcounter)->applyFromArray($styleArray);
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$result_arrays['orgin_city'])->getStyle($colcounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('B'.$rowcounter)->applyFromArray($styleArray);
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$result_arrays['orgin_cfs'])->getStyle($colcounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('C'.$rowcounter)->applyFromArray($styleArray);
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$result_arrays['des_country'])->getStyle($colcounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('D'.$rowcounter)->applyFromArray($styleArray);
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$result_arrays['des_city'])->getStyle($colcounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('E'.$rowcounter)->applyFromArray($styleArray);
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$result_arrays['des_cfs'])->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('F'.$rowcounter)->applyFromArray($styleArray);
									
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$fOriginChargeRateWM)->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('G'.$rowcounter)->applyFromArray($styleArray1);
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$fOriginChargeMinRateWM)->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('H'.$rowcounter)->applyFromArray($styleArray1);
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$fOriginChargeBookingRate)->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('I'.$rowcounter)->applyFromArray($styleArray1);
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$szOriginChargeCurrency)->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('J'.$rowcounter)->applyFromArray($styleArray);
									
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$fRateWM)->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('K'.$rowcounter)->applyFromArray($styleArray1);
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$fMinRateWM)->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('L'.$rowcounter)->applyFromArray($styleArray1);
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$fRate)->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('M'.$rowcounter)->applyFromArray($styleArray1);
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$szFreightCurrency)->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('N'.$rowcounter)->applyFromArray($styleArray);
									
								    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$fDestinationChargeRateWM)->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('O'.$rowcounter)->applyFromArray($styleArray1);
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$fDestinationChargeMinRateWM)->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('P'.$rowcounter)->applyFromArray($styleArray1);
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$fDestinationChargeBookingRate)->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('Q'.$rowcounter)->applyFromArray($styleArray1);
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$szDestinationChargeCurrency)->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('R'.$rowcounter)->applyFromArray($styleArray);
									
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$idFrequency)->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('S'.$rowcounter)->applyFromArray($styleArray);
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$idCutOffDay)->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('T'.$rowcounter)->applyFromArray($styleArray);
									$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($colcounter,$rowcounter)->getNumberFormat()->setFormatCode('hh:mm');
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$szCutOffLocalTime)->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('U'.$rowcounter)->applyFromArray($styleArray);
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$iTransitHours)->getStyle($colcounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('V'.$rowcounter)->applyFromArray($styleArray);
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$idAvailableDay)->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('W'.$rowcounter)->applyFromArray($styleArray);
									$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($colcounter,$rowcounter)->getNumberFormat()->setFormatCode('hh:mm');
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$szAvailableLocalTime)->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('X'.$rowcounter)->applyFromArray($styleArray);
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$iBookingCutOffHours)->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									$objPHPExcel->getActiveSheet()->getStyle('Y'.$rowcounter)->applyFromArray($styleArray);
									//$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($colcounter,$rowcounter)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,PHPExcel_Shared_Date::PHPToExcel($validdate))->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);						
									$objPHPExcel->getActiveSheet()->getStyle('Z'.$rowcounter)->applyFromArray($styleArray);
									
									$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($colcounter,$rowcounter)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);
									if($expirydate=='')
									{
										$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,'')->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									}
									else
									{
										$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,PHPExcel_Shared_Date::PHPToExcel($expirydate))->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									}
									$objPHPExcel->getActiveSheet()->getStyle('AA'.$rowcounter)->applyFromArray($styleArray);
									
									
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,'')->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									//$objPHPExcel->getActiveSheet()->getStyle('T'.$rowcounter)->applyFromArray($styleArray);
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,'')->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									//$objPHPExcel->getActiveSheet()->getStyle('U'.$rowcounter)->applyFromArray($styleArray);
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,'')->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									//$objPHPExcel->getActiveSheet()->getStyle('V'.$rowcounter)->applyFromArray($styleArray);
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,'')->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									//$objPHPExcel->getActiveSheet()->getStyle('W'.$rowcounter)->applyFromArray($styleArray);
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,'')->getStyle($colcounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
									//$objPHPExcel->getActiveSheet()->getStyle('X'.$rowcounter)->applyFromArray($styleArray);
									
									
									//validation for n col Transit time
									/*$val_n='N'.$rowcounter;
									$val_transit_n='List!$G$1:$G$7';									
																				
							   		if($rowcounter=='3')
									{											
										$objValidationN = $objPHPExcel->getActiveSheet()->getCell($val_n)->getDataValidation();
									    $objValidationN->setType( PHPExcel_Cell_DataValidation::TYPE_WHOLE );
										$objValidationN->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
									    $objValidationN->setAllowBlank(true);
									    $objValidationN->setShowInputMessage(true);
									    $objValidationN->setShowErrorMessage(true);
									    $objValidationN->setShowDropDown(true);
									    $objValidationN->setErrorTitle('Transit time');
										$objValidationN->setError('Please fill in total CFS to CFS transit time in full number of days. Validation has been set to be between 0 and 100 days.');
										$objValidationN->setFormula1(1);
										$objValidationN->setFormula2(100);
									}
									else
									{
										$objPHPExcel->getActiveSheet()->getCell($val_n)->setDataValidation(clone $objValidationN);						
									}
															
									$currencyArr=$this->getFreightCurrency();
									$countCurrency=count($currencyArr);
									$val_j="J".$rowcounter;
									$val_currency='List!$A$1:$A$'.$countCurrency;
									$objValidation = $objPHPExcel->getActiveSheet()->getCell($val_j)->getDataValidation();
								    $objValidation->setFormula1($val_currency);
								    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
								    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
								    $objValidation->setAllowBlank(true);
								    $objValidation->setShowInputMessage(true);
								    $objValidation->setShowErrorMessage(true);
								    $objValidation->setShowDropDown(true);
								    $objValidation->setErrorTitle('Currency');					   
									$objValidation->setError('Please select one of the currencies.');
									//$objValidation->setPromptTitle('Pick from list');
									//$objValidation->setPrompt('Please pick a value from the drop-down list.');
									
									$val_k="K".$rowcounter;
									$val_freq='List!$B$1:$B$2';
									
									$objValidation = $objPHPExcel->getActiveSheet()->getCell($val_k)->getDataValidation();
								    $objValidation->setFormula1($val_freq);
								    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
								    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
								    $objValidation->setAllowBlank(true);
								    $objValidation->setShowInputMessage(true);
								    $objValidation->setShowErrorMessage(true);
								    $objValidation->setShowDropDown(true);
								    $objValidation->setErrorTitle('Frequency');
									$objValidation->setError('Please select either weekly or biweekly (every two weeks) service.');
									//$objValidation->setPromptTitle('Pick from list');
									//$objValidation->setPrompt('Please pick a value from the drop-down list.');
									
									
									$val_l="L".$rowcounter;
									$val_day_l='List!$C$1:$C$7';
									
									$objValidation = $objPHPExcel->getActiveSheet()->getCell($val_l)->getDataValidation();
								    $objValidation->setFormula1($val_day_l);
								    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
								    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
								    $objValidation->setAllowBlank(true);
								    $objValidation->setShowInputMessage(true);
								    $objValidation->setShowErrorMessage(true);
								    $objValidation->setShowDropDown(true);
								    $objValidation->setErrorTitle('Cut off day');
									$objValidation->setError('Please select one of the weekdays available.');
									//$objValidation->setPromptTitle('Pick from list');
									//$objValidation->setPrompt('Please pick a value from the drop-down list.');
									
									/*$val_o="O".$rowcounter;
									$val_day_o='List!$E$1:$E$7';
									
									$objValidation = $objPHPExcel->getActiveSheet()->getCell($val_o)->getDataValidation();
								    $objValidation->setFormula1($val_day_o);
								    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
								    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
								    $objValidation->setAllowBlank(true);
								    $objValidation->setShowInputMessage(true);
								    $objValidation->setShowErrorMessage(true);
								    $objValidation->setShowDropDown(true);
								    $objValidation->setErrorTitle('Input error');
									$objValidation->setError('Please pick a value from the drop-down list.');
									//$objValidation->setPromptTitle('Pick from list');
									//$objValidation->setPrompt('Please pick a value from the drop-down list.');*/
									
									/*$val_q="Q".$rowcounter;
									$val_day_q='List!$G$1:$G$25';
									
									$objValidation = $objPHPExcel->getActiveSheet()->getCell($val_q)->getDataValidation();
								    $objValidation->setFormula1($val_day_q);
								    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
								    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
								    $objValidation->setAllowBlank(true);
								    $objValidation->setShowInputMessage(true);
								    $objValidation->setShowErrorMessage(true);
								    $objValidation->setShowDropDown(true);
								    $objValidation->setErrorTitle('Booking cut off');
									$objValidation->setError('Please fill in how long before the CFS cut off you latest need to receive the booking. Select an option from the list.');
									//$objValidation->setPromptTitle('Pick from list');
									//$objValidation->setPrompt('Please pick a value from the drop-down list.');
									
									$val_m="M".$rowcounter;
									$val_day_m='List!$F$1:$F$48';
									
									$objValidation = $objPHPExcel->getActiveSheet()->getCell($val_m)->getDataValidation();
								    $objValidation->setFormula1($val_day_m);
								    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
								    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
								    $objValidation->setAllowBlank(true);
								    $objValidation->setShowInputMessage(true);
								    $objValidation->setShowErrorMessage(true);
								    $objValidation->setShowDropDown(true);
								    $objValidation->setErrorTitle('Local time');
									$objValidation->setError('Please fill in the time of the CFS cargo cut off. Please use 24 hours clock (hh:mm) and only full and half hours.');
									//$objValidation->setPromptTitle('Pick from list');
									//$objValidation->setPrompt('Please pick a value from the drop-down list.');
									
									$val_p="P".$rowcounter;
									$objValidation = $objPHPExcel->getActiveSheet()->getCell($val_p)->getDataValidation();
								    $objValidation->setFormula1($val_day_m);
								    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
								    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
								    $objValidation->setAllowBlank(true);
								    $objValidation->setShowInputMessage(true);
								    $objValidation->setShowErrorMessage(true);
								    $objValidation->setShowDropDown(true);
								    $objValidation->setErrorTitle('Local time');
									$objValidation->setError('Please fill in the time of day when the cargo is available to the customer at the destination CFS. Please use 24 hours clock and only full and half hours.');
									
									$val_g="G".$rowcounter;
									$val_h="H".$rowcounter;
									$val_i="I".$rowcounter;
									
									$objValidation = $objPHPExcel->getActiveSheet()->getCell($val_g)->getDataValidation();
									$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL );
									$objValidation->setOperator( PHPExcel_Cell_DataValidation::OPERATOR_GREATERTHAN);		
									$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
									$objValidation->setAllowBlank(true);
								    $objValidation->setShowInputMessage(true);
								    $objValidation->setShowErrorMessage(true);
								    $objValidation->setShowDropDown(true);
								    $objValidation->setErrorTitle('Freight rate per W/M');
									$objValidation->setError('Please fill in freight rate per weight (mt)/measure (cbm) in selected currency. This should be a positive decimal number.');
									$objValidation->setFormula1(0);
									
									
									$objValidation = $objPHPExcel->getActiveSheet()->getCell($val_h)->getDataValidation();
									$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL );
									$objValidation->setOperator( PHPExcel_Cell_DataValidation::OPERATOR_GREATERTHAN);		
									$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
									$objValidation->setAllowBlank(true);
								    $objValidation->setShowInputMessage(true);
								    $objValidation->setShowErrorMessage(true);
								    $objValidation->setShowDropDown(true);
								    $objValidation->setErrorTitle('Minimum freight charged');
									$objValidation->setError('Please fill in minimum freight charged in the selected currency. This should be zero or a positive decimal number.');
									$objValidation->setFormula1(0);
									
									$objValidation = $objPHPExcel->getActiveSheet()->getCell($val_i)->getDataValidation();
									$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL );
									$objValidation->setOperator( PHPExcel_Cell_DataValidation::OPERATOR_GREATERTHAN);		
									$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
									$objValidation->setAllowBlank(true);
								    $objValidation->setShowInputMessage(true);
								    $objValidation->setShowErrorMessage(true);
								    $objValidation->setShowDropDown(true);
								    $objValidation->setErrorTitle('Rate per booking');
									$objValidation->setError('Please fill in any fixed rate charged per booking in addition to the freight per W/M and minimum. This should be zero or a positive decimal number.');
									$objValidation->setFormula1(0);
									
									
									
									
									$val_r="R".$rowcounter;
									$val_s="S".$rowcounter;
									
									$objValidation = $objPHPExcel->getActiveSheet()->getCell($val_r)->getDataValidation();
									$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_DATE );
									$objValidation->setOperator( PHPExcel_Cell_DataValidation::OPERATOR_GREATERTHAN);
									$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
									$objValidation->setAllowBlank(true);
								    $objValidation->setShowInputMessage(true);
								    $objValidation->setShowErrorMessage(true);
								    $objValidation->setShowDropDown(true);
								    $objValidation->setErrorTitle('Valid from');
									$objValidation->setError('Please fill in the date, which this rate is valid from.');
									$objValidation->setFormula1(40908);
									
									$objValidation = $objPHPExcel->getActiveSheet()->getCell($val_s)->getDataValidation();
									$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_DATE );
									$objValidation->setOperator( PHPExcel_Cell_DataValidation::OPERATOR_GREATERTHAN);
									$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
									$objValidation->setAllowBlank(true);
								    $objValidation->setShowInputMessage(true);
								    $objValidation->setShowErrorMessage(true);
								    $objValidation->setShowDropDown(true);
								    $objValidation->setErrorTitle('Expiry from');
									$objValidation->setError('Please fill in the date this rate will expire.');
									$objValidation->setFormula1(40908);
									
									*/
									
									$formula_o="O".$rowcounter;
									$formula='=IF(L'.$rowcounter.'="","",IF(N'.$rowcounter.'="","",VLOOKUP(IF(ROUND((N'.$rowcounter.'/7-ROUNDDOWN(N'.$rowcounter.'/7,0))*7,0)+VLOOKUP(L'.$rowcounter.',List!C$1:D$7,2,FALSE)>7,ROUND((N'.$rowcounter.'/7-ROUNDDOWN(N'.$rowcounter.'/7,0))*7,0)+VLOOKUP(L'.$rowcounter.',List!C$1:D$7,2,FALSE)-7,ROUND((N'.$rowcounter.'/7-ROUNDDOWN(N'.$rowcounter.'/7,0))*7,0)+VLOOKUP(L'.$rowcounter.',List!C$1:D$7,2,FALSE)),List!D$1:E$7,2)))';
									$objPHPExcel->getActiveSheet()->setCellValue($formula_o,$formula)->getStyle($formula_o)->getProtection()->setHidden(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
														
						   		}
						   	}
						}
					}
					else
					{
						return false;
					}
			   	}
			   	else
			   	{
			   		return false;
			   	}
			   
				
			//define('FORMAT_DEFAULT_SYSTEM_DATE_FORMAT','dd-mm-yyyy');
				
			   	
			   	
			   	
				$filter='A2:AA'.$rowcounter;
			   	$objPHPExcel->getActiveSheet()->setAutoFilter($filter);
			   	 	
			   //	echo $rowcounter;
			  	$row_count=count($result_array);
			  	$cell_open_rows='A'.($rowcounter+1).':S1000';
			  	$cell_open_cols='S1000';
			  	//$val_n='N4:N'.$rowcounter;
			  	
			  	$objPHPExcel->getActiveSheet()->freezePane('G3');
			   	$objPHPExcel->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(3, 3);
			   			
			  	
			  	
			  	//$objPHPExcel->getActiveSheet()->setCellValue('O3','=IF(L3="","",IF(N3="","",VLOOKUP(IF(ROUND((N3/7-ROUNDDOWN(N3/7,0))*7,0)+VLOOKUP(L3,List!C$1:D$7,2,FALSE)>7,ROUND((N3/7-ROUNDDOWN(N3/7,0))*7,0)+VLOOKUP(L3,List!C$1:D$7,2,FALSE)-7,ROUND((N3/7-ROUNDDOWN(N3/7,0))*7,0)+VLOOKUP(L3,List!C$1:D$7,2,FALSE)),List!D$1:E$7,2)))');
				//$objPHPExcel->getActiveSheet()->getStyle($cell_open_rows)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
				
				
				//$objPHPExcel->getActiveSheet()->freezePane('A');
				//$objPHPExcel->getActiveSheet()->freezePane('B');
				//$objPHPExcel->getActiveSheet()->freezePane('C');
				
			 	$objPHPExcel->getActiveSheet()->setTitle('Sheet1');
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				$objPHPExcel->setActiveSheetIndex(0);
				//$ext = end(explode('.', $fileName));
				//$fileName=__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/Transporteca-LCL-Services-upload-sheet_1.xlsx";
				$objWriter->save($fileName);
				return true;
	}
	
	
	function exportListData($objPHPExcel,$sheetindex,$fileName)
	{
			$sheetindex=1;
			if($sheetindex>0)
			{
				$objPHPExcel->createSheet();
			}
			//Worksheets("List").Visible(False); 
			$objPHPExcel->setActiveSheetIndex($sheetindex);
			
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			
			$currencyArr=$this->getFreightCurrency();
			
			if(!empty($currencyArr))
			{
				$c=1;
				foreach($currencyArr as $currencyArrs)
				{
					$val_a='A'.$c;
					$objPHPExcel->getActiveSheet()->setCellValue($val_a,$currencyArrs);
					++$c;
				}
			}
			$bColumnData=bColumnData();
			if(!empty($bColumnData))
			{
				$b=1;
				for($i=0;$i<count($bColumnData);++$i)
				{
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$b,$bColumnData[$i]);
					++$b;
				}
			}
			
			
			$cColumnData=cColumnData();
			if(!empty($cColumnData))
			{
				$c=1;
				for($i=0;$i<count($cColumnData);++$i)
				{
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$c,$cColumnData[$i]);
					++$c;
				}
			}
			
			
			$dColumnData=dColumnData();
			if(!empty($dColumnData))
			{
				$d=1;
				for($i=0;$i<count($dColumnData);++$i)
				{
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$d,$dColumnData[$i]);
					++$d;
				}
			}
			
			
			$eColumnData=eColumnData();
			if(!empty($eColumnData))
			{
				$e=1;
				for($i=0;$i<count($eColumnData);++$i)
				{
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$e,$eColumnData[$i]);
					++$e;
				}
			}
			
			$fColumnData=fColumnData();
			if(!empty($fColumnData))
			{
				$f=1;
				for($i=0;$i<count($fColumnData);++$i)
				{
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$f,$fColumnData[$i]);
					++$f;
				}
			}
						
			$gColumnData=gColumnData();
					
			if(!empty($gColumnData))
			{
				$g=1;
				for($i=0;$i<count($gColumnData);++$i)
				{
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$g,$gColumnData[$i]);
					++$g;
				}
			}
			
			
			$objPHPExcel->getActiveSheet()->setSheetState(PHPExcel_Worksheet::SHEETSTATE_HIDDEN);
			
			//$objPHPExcel->getActiveSheet()->setHidden(true);
			$objPHPExcel->getActiveSheet()->setTitle('List');
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objPHPExcel->setActiveSheetIndex(0);
			//$ext = end(explode('.', $fileName));
			//$fileName=__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/Transporteca-LCL-Services-upload-sheet_1.xlsx";
			$objWriter->save($fileName);
			return true;
			
	}
	
	
	function getWeekday($idCutOffDay)
	{
		$query="
			SELECT
				szWeekDay
			FROM
				".__DBC_SCHEMATA_WEEK_DAYS__."
			WHERE
				id='".(int)$idCutOffDay."'
		";
		if( ( $result1 = $this->exeSQL( $query ) ) )
		{
			$row1 = $this->getAssoc($result1);
			
			return $row1['szWeekDay'];
		}
	}
	
	
	function getFrequency($idFrequency)
	{
		$query="
			SELECT
				szDescription
			FROM
				".__DBC_SCHEMATA_FREQUENCY__."
			WHERE
				id='".(int)$idFrequency."'
		";
		if( ( $result1 = $this->exeSQL( $query ) ) )
		{
			$row1 = $this->getAssoc($result1);
			
			return $row1['szDescription'];
		}
	}
	
	
	function getFreightCurrency($idFreightCurrency=0,$flag=false)
	{
		$sql="";
		$col='';
		if((int)$idFreightCurrency>0)
		{
			$sql .="
				WHERE
				id='".(int)$idFreightCurrency."'";
		}
		else
		{
			$sql .="
				WHERE
				iPricing='1'";
		}
		if($flag)
		{
			$col="id,";
		}
		$query="
			SELECT
				".$col."
				szCurrency
			FROM
				".__DBC_SCHEMATA_CURRENCY__."
				".$sql."
			ORDER BY
				szCurrency ASC
		";
		if( ( $result1 = $this->exeSQL( $query ) ) )
		{
			if($flag)
			{
				while($row1 = $this->getAssoc($result1))
				{
					$szCurrencyArr[]=$row1;
				}
				
				return $szCurrencyArr;
			}
			else
			{
				if((int)$idFreightCurrency>0)
				{
					$row1 = $this->getAssoc($result1);
				
					return $row1['szCurrency'];
				}
				else
				{
					while($row1 = $this->getAssoc($result1))
					{
						$szCurrencyArr[]=$row1['szCurrency'];
					}
					
					return $szCurrencyArr;
				}
			}
		}
	}
	
	
	
	function importLCLServicesData($objPHPExcel,$sheetindex,$colcount,$rowcount,$idForwarder,$iWarehouseType=__WAREHOUSE_TYPE_CFS__)
	{
		date_default_timezone_set('UTC');
		$ctr=0;
		if($rowcount>2)
		{
                    for($i=3;$i<=$rowcount;$i++)
                    {
                        for($j=0;$j<$colcount;$j++)
                        {
                            if($objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0,$i)->getValue()!="")
                            {
                                if(is_numeric($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($j,$i)->getValue()) && ($j==26 || $j==25 || $j==23 || $j==20))
                                {
                                    $LCLServiceData[$ctr][] = PHPExcel_Shared_Date::ExcelToPHP($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($j,$i)->getValue());
                                }
                                else
                                {
                                    $LCLServiceData[$ctr][] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($j,$i)->getValue();
                                }
                            } 
                        }
                        ++$ctr;
                    }
		}		
                
                //setting error messages based on iWarehouseType
                if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
                {
                    $szOriginCfs = t($this->t_base.'messages/origin_aiport_warehouse_is_required'); 
                    $szDestinationCfs = t($this->t_base.'messages/des_airport_is_required');
                    $szOriginChargePmRequired = t($this->t_base.'messages/air_origin_charge_per_wm_is_required');
                    $szOriginChargePmPositive = t($this->t_base.'messages/air_origin_charge_per_wm_should_be_positive_value');
                    
                    $szFreightChargePmRequired = t($this->t_base.'messages/air_per_wm_is_required'); 
                    $szFreightChargePmPositive = t($this->t_base.'messages/air_per_wm_should_be_positive_value');  
                    
                    $szDestinationChargePmRequired = t($this->t_base.'messages/air_destination_charge_per_wm_is_required'); 
                    $szDestinationChargePmPositive = t($this->t_base.'messages/air_destination_charge_per_wm_should_be_positive_value');  

                }
                else
                {
                    $szOriginCfs = t($this->t_base.'messages/origin_cfs_is_required');
                    $szDestinationCfs = t($this->t_base.'messages/des_cfs_is_required'); 
                    $szOriginChargePmRequired = t($this->t_base.'messages/origin_charge_per_wm_is_required');
                    $szOriginChargePmPositive = t($this->t_base.'messages/origin_charge_per_wm_should_be_positive_value');
                    
                    $szFreightChargePmRequired = t($this->t_base.'messages/per_wm_is_required'); 
                    $szFreightChargePmPositive = t($this->t_base.'messages/per_wm_should_be_positive_value'); 
                    
                    $szDestinationChargePmRequired = t($this->t_base.'messages/destination_charge_per_wm_is_required'); 
                    $szDestinationChargePmPositive = t($this->t_base.'messages/destination_charge_per_wm_should_be_positive_value');  
                }
                
		//print_r($LCLServiceData);
		if(!empty($LCLServiceData))
		{
			$k=3;
			$l_err=0;
			$l_success=0;
			$d=0;
			foreach($LCLServiceData as $LCLServiceDatas)
			{ 
				$error_flag=false;
				$err_msg=array();
				$checkErrorFlag=false;
				if($LCLServiceDatas[0]=="")
				{
                                    $error_flag=true;
                                    $err_msg['origin_country']=t($this->t_base.'messages/origin_country_is_required');
				}
				
				if($LCLServiceDatas[1]=="")
				{
                                    $error_flag=true;
                                    $err_msg['origin_city']=t($this->t_base.'messages/origin_city_is_required'); 
				}
				if($LCLServiceDatas[2]=="")
				{
                                    $error_flag=true; 
                                    $err_msg['origin_cfs']= $szOriginCfs; 
				}
				if($LCLServiceDatas[3]=="")
				{
                                    $error_flag=true;
                                    $err_msg['destination_country']=t($this->t_base.'messages/des_country_is_required'); 
				}
				if($LCLServiceDatas[4]=="")
				{
                                    $error_flag=true;
                                    $err_msg['destination_city']=t($this->t_base.'messages/des_city_is_required'); 
				}
				if($LCLServiceDatas[5]=="")
				{
                                    $error_flag=true;
                                    $err_msg['destination_cfs']= $szDestinationCfs; 
				}
				
				if($LCLServiceDatas[5]!='' && $LCLServiceDatas[0]!='' && $LCLServiceDatas[4]!='' && $LCLServiceDatas[3]!='' && $LCLServiceDatas[2]!='' && $LCLServiceDatas[1]!='')
				{ 
					$originCountry_Id=$this->getCountryId(sanitize_all_html_input($LCLServiceDatas[0])); 
					$desCountry_Id=$this->getCountryId(sanitize_all_html_input($LCLServiceDatas[3]));
					$query="
                                            SELECT
                                                id
                                            FROM
                                                ".__DBC_SCHEMATA_WAREHOUSES__."
                                            WHERE
                                                szWareHouseName='".mysql_escape_custom(sanitize_all_html_input($LCLServiceDatas[2]))."' 	
                                            AND
                                                szCity='".mysql_escape_custom(sanitize_all_html_input($LCLServiceDatas[1]))."'
                                            AND
                                                idCountry='".(int)$originCountry_Id."'
                                            AND
                                                idForwarder='".(int)$idForwarder."' 
                                            AND
                                                iWarehouseType = '".(int)$iWarehouseType."' 
                                            AND
                                               iActive = '1'
					";
					if($result=$this->exeSQL($query))
					{
                                            if($this->iNumRows>0)
                                            {
                                                $row=$this->getAssoc($result);
                                                $fromWareHouse_Id=$row['id'];
                                            }
                                            else
                                            {
                                                $fromWareHouse_Id=0;
                                            }
					}
					
					$query="
                                            SELECT
                                                id
                                            FROM
                                                ".__DBC_SCHEMATA_WAREHOUSES__."
                                            WHERE
                                                szWareHouseName='".mysql_escape_custom(sanitize_all_html_input($LCLServiceDatas[5]))."' 	
                                            AND
                                                szCity='".mysql_escape_custom(sanitize_all_html_input($LCLServiceDatas[4]))."'
                                            AND
                                                idCountry='".(int)$desCountry_Id."'
                                            AND
                                                idForwarder='".(int)$idForwarder."' 
                                            AND
                                                iWarehouseType = '".(int)$iWarehouseType."'
                                            AND
                                               iActive = '1'
					";
					//echo $query."<br/>";
					if($result=$this->exeSQL($query))
					{
                                            if($this->iNumRows>0)
                                            { 
                                                $row=$this->getAssoc($result);
                                                $toWareHouse_Id=$row['id'];
                                            }
                                            else
                                            {
                                                $toWareHouse_Id=0;
                                            }
					}
					
					if($toWareHouse_Id>0 && $fromWareHouse_Id>0)
					{
                                            $query="
                                                SELECT
                                                    *
                                                FROM
                                                    ".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
                                                WHERE
                                                    idWarehouseFrom='".(int)$fromWareHouse_Id."'
                                                AND
                                                    idWarehouseTo='".(int)$toWareHouse_Id."'
                                                AND
                                                    iActive='1'
                                            ";
                                            if($result=$this->exeSQL($query))
                                            {
                                                if($this->iNumRows>0)
                                                {
                                                    $checkErrorFlag=true;
                                                    if($LCLServiceDatas[6]=='' && $LCLServiceDatas[7]=='' && $LCLServiceDatas[8]=='' && $LCLServiceDatas[9]=='' && $LCLServiceDatas[10]=='' && $LCLServiceDatas[11]=='' && $LCLServiceDatas[13]=='' && $LCLServiceDatas[15]=='' && $LCLServiceDatas[16]=='' && $LCLServiceDatas[17]=='')
                                                    {
                                                        // if complete row is empty and warehouse combination exists in Pricing table then do not perform any check.
                                                        $checkErrorFlag=false;
                                                        $deleteWarehouseCombination[$d]['toWareHouse']=$toWareHouse_Id;
                                                        $deleteWarehouseCombination[$d]['fromWareHouse']=$fromWareHouse_Id;
                                                        ++$d;
                                                    }								
                                                }
                                                else
                                                {
                                                    // If active warehouse combination is not exists in tblPricingWTW and any single column is filled then we just give an error message.
                                                    if($LCLServiceDatas[6]!='')
                                                    {
                                                        $checkErrorFlag=true;
                                                    }

                                                    if($LCLServiceDatas[7]!='')
                                                    {
                                                        $checkErrorFlag=true;
                                                    }

                                                    if($LCLServiceDatas[8]!='')
                                                    {
                                                        $checkErrorFlag=true;
                                                    }

                                                    if($LCLServiceDatas[9]!='')
                                                    {
                                                        $checkErrorFlag=true;
                                                    }

                                                    if($LCLServiceDatas[10]!='')
                                                    {
                                                        $checkErrorFlag=true;
                                                    }

                                                    if($LCLServiceDatas[11]!='')
                                                    {
                                                        $checkErrorFlag=true;
                                                    }

                                                if($LCLServiceDatas[12]!='')
                                                {
                                                        $checkErrorFlag=true;
                                                }

                                                if($LCLServiceDatas[13]!='')
                                                {
                                                        $checkErrorFlag=true;
                                                }

                                                if($LCLServiceDatas[15]!='')
                                                {
                                                        $checkErrorFlag=true;
                                                }

                                                if($LCLServiceDatas[16]!='')
                                                {
                                                        $checkErrorFlag=true;
                                                }

                                                if($LCLServiceDatas[17]!='')
                                                {
                                                        $checkErrorFlag=true;
                                                }

                                                if($LCLServiceDatas[18]!='')
                                                {
                                                        $checkErrorFlag=true;
                                                }

                                                if($LCLServiceDatas[19]!='')
                                                {
                                                        $checkErrorFlag=true;
                                                }

                                                if($LCLServiceDatas[20]!='')
                                                {
                                                        $checkErrorFlag=true;
                                                }

                                                if($LCLServiceDatas[21]!='')
                                                {
                                                        $checkErrorFlag=true;
                                                }

                                                /*if($LCLServiceDatas[22]!='')
                                                {
                                                        $checkErrorFlag=true;
                                                }*/

                                                if($LCLServiceDatas[23]!='')
                                                {
                                                        $checkErrorFlag=true;
                                                }
                                                if($LCLServiceDatas[24]!='')
                                                {
                                                        $checkErrorFlag=true;
                                                }
							}
						}
					}
				}
				else
				{
					$checkErrorFlag=true;
				}
				
				if($checkErrorFlag)
				{
					$allowedOriginChargesFlag = 0;
					$allowedDestinationChargesFlag = 0;
					
					if($LCLServiceDatas[6]!=="" || $LCLServiceDatas[7]!=="" || $LCLServiceDatas[8]!=="" || $LCLServiceDatas[9]!="")
					{
						//If any one out of 4 fields of Origin charges is filled then all fields are required. otherwise we just assumed that there is origin charges applicable on this service.  
						if($LCLServiceDatas[6]==="")
						{					
                                                    $error_flag=true;
                                                    $err_msg['origin_charge_per_wm']= $szOriginChargePmRequired;
						}
						else if((float)$LCLServiceDatas[6]<0)
						{
                                                    $error_flag=true;
                                                    $err_msg['origin_charge_per_wm']= $szOriginChargePmPositive;
						}
						if($LCLServiceDatas[7]==="")
						{						
                                                    $error_flag=true;
                                                    $err_msg['origin_charge_minimum']=t($this->t_base.'messages/origin_charge_minimum_rate_is_required');
						}
						else if((float)$LCLServiceDatas[7]<0)
						{
                                                    $error_flag=true;
                                                    $err_msg['origin_charge_minimum']=t($this->t_base.'messages/origin_charge_mini_rate_should_positive_value');
						}
						if($LCLServiceDatas[8]==="")
						{						
                                                    $error_flag=true;
                                                    $err_msg['origin_charge_perBooking']=t($this->t_base.'messages/origin_charge_per_booking_rate_is_required');
						}
						else if((float)$LCLServiceDatas[8]<0)
						{
                                                    $error_flag=true;
                                                    $err_msg['origin_charge_perBooking']=t($this->t_base.'messages/origin_charge_per_booking_rate_should_be_positive_value');
						}
						if($LCLServiceDatas[9]=="")
						{					
							$error_flag=true;
							$err_msg['origin_charge_currency']=t($this->t_base.'messages/origin_charge_currency_is_required');
						}
						else 
						{
							$currencyArr=$this->getFreightCurrency();
							if(!empty($currencyArr))
							{
								if(!in_array(strtoupper($LCLServiceDatas[9]),$currencyArr))
								{
									$error_flag=true;
									$err_msg['origin_charge_currency']=t($this->t_base.'messages/origin_charge_invalid_currency_type');
								}
							}
						}
						$allowedOriginChargesFlag = 1;
					}
					if($LCLServiceDatas[10]=="")
					{					
                                            //$error_flag=true;
                                            //$err_msg['per_wm']=t($this->t_base.'messages/per_wm_is_required');
					}
					else if((float)$LCLServiceDatas[10]<0)
					{
                                            $error_flag=true;
                                            $err_msg['per_wm'] = $szFreightChargePmPositive;
					}
									
					if($LCLServiceDatas[11]==="")
					{
					
						$error_flag=true;
						$err_msg['minimum']=t($this->t_base.'messages/minimum_rate_is_required');
					}
					else if((float)$LCLServiceDatas[11]<0)
					{
						$error_flag=true;
						$err_msg['minimum']=t($this->t_base.'messages/mini_rate_should_positive_value');
					}
					if($LCLServiceDatas[12]==="")
					{					
						$error_flag=true;
						$err_msg['perBooking']=t($this->t_base.'messages/per_booking_rate_is_required');
					}
					else if((float)$LCLServiceDatas[12]<0)
					{
						$error_flag=true;
						$err_msg['perBooking']=t($this->t_base.'messages/per_booking_rate_should_be_positive_value');
					}
					if($LCLServiceDatas[13]=="")
					{					
						$error_flag=true;
						$err_msg['currency']=t($this->t_base.'messages/currency_is_required');
					}
					else 
					{
                                            $currencyArr=$this->getFreightCurrency();
                                            if(!empty($currencyArr))
                                            {
                                                if(!in_array(strtoupper($LCLServiceDatas[13]),$currencyArr))
                                                {
                                                    $error_flag=true;
                                                    $err_msg['currency']=t($this->t_base.'messages/invalid_currency_type');
                                                }
                                            }
					}
					if($LCLServiceDatas[14]!="" || $LCLServiceDatas[15]!="" || $LCLServiceDatas[16]!="" || $LCLServiceDatas[17]!="")
					{
						$allowedDestinationChargesFlag = 1;
						//If any one out of 4 fields of Destination charges is filled then all fields are required. otherwise we just assumed that there is Destination charges applicable on this service.
						if($LCLServiceDatas[14]==="")
						{ 
                                                    $error_flag=true;
                                                    $err_msg['destination_charge_per_wm']= $szDestinationChargePmRequired;
						}
						else if((float)$LCLServiceDatas[14]<0)
						{
                                                    $error_flag=true;
                                                    $err_msg['destination_charge_per_wm'] = $szDestinationChargePmPositive;
						}
						
						if($LCLServiceDatas[15]==="")
						{					
                                                    $error_flag=true;
                                                    $err_msg['destination_charge_minimum']=t($this->t_base.'messages/destination_charge_minimum_rate_is_required');
						}
						else if((float)$LCLServiceDatas[15]<0)
						{
                                                    $error_flag=true;
                                                    $err_msg['destination_charge_minimum']=t($this->t_base.'messages/destination_charge_mini_rate_should_positive_value');
						}
						
						if($LCLServiceDatas[16]==="")
						{
						
							$error_flag=true;
							$err_msg['destination_charge_perBooking']=t($this->t_base.'messages/destination_charge_per_booking_rate_is_required');
						}
						else if((float)$LCLServiceDatas[16]<0)
						{
							$error_flag=true;
							$err_msg['destination_charge_perBooking']=t($this->t_base.'messages/destination_charge_per_booking_rate_should_be_positive_value');
						}
						
						
						if($LCLServiceDatas[17]=="")
						{
						
							$error_flag=true;
							$err_msg['destination_charge_currency']=t($this->t_base.'messages/destination_charge_currency_is_required');
						}
						else 
						{
							$currencyArr=$this->getFreightCurrency();
							if(!empty($currencyArr))
							{
								if(!in_array(strtoupper($LCLServiceDatas[17]),$currencyArr))
								{
									$error_flag=true;
									$err_msg['destination_charge_currency']=t($this->t_base.'messages/destination_charge_invalid_currency_type');
								}
							}
						}
					}
					if($LCLServiceDatas[18]=="")
					{
						$error_flag=true;
						$err_msg['frequency']=t($this->t_base.'messages/frequency_is_required');
					}
					else 
					{
						$bColumnDataArr=bColumnData();
						if(!empty($bColumnDataArr))
						{
							if(!in_array(ucfirst(strtolower($LCLServiceDatas[18])),$bColumnDataArr))
							{
								$error_flag=true;
								$err_msg['frequency']=t($this->t_base.'messages/invalid_frequency_type');
							}
						}
					}
					if($LCLServiceDatas[19]=="")
					{
						$error_flag=true;
						$err_msg['cutoff']=t($this->t_base.'messages/origin_cutoff_day_is_required');
					}
					else 
					{
						$cColumnDataArr=cColumnData();
						if(!empty($cColumnDataArr))
						{
							if(!in_array(ucfirst(strtolower($LCLServiceDatas[19])),$cColumnDataArr))
							{
								$error_flag=true;
								$err_msg['cutoff']=t($this->t_base.'messages/invalid_origin_cutoff_day');
							}
						}
					}
								
					if($LCLServiceDatas[20]=="")
					{
					
						$error_flag=true;
						$err_msg['orign_l_t']=t($this->t_base.'messages/origin_local_time_is_required');
					}
					else 
					{
						$fColumnDataArr=fColumnData();
						if(!empty($fColumnDataArr))
						{
												    
						   $cutOff_Time=date('H:i',$LCLServiceDatas[20]);
						 	if(!in_array($cutOff_Time,$fColumnDataArr))
							{
								$error_flag=true;
								$err_msg['orign_l_t']=t($this->t_base.'messages/invalid_origin_local_time');
							}
						}
					}
					
					
					if($LCLServiceDatas[21]=="")
					{
					
						$error_flag=true;
						$err_msg['trans_day']=t($this->t_base.'messages/transit_day_is_required');
					}
					else 
					{
						
						if($LCLServiceDatas[21]<=0 || $LCLServiceDatas[21]>100)
						{
							$error_flag=true;
							$err_msg['trans_day']=t($this->t_base.'messages/transit_day_should_between_1_to_100');
						}
					}
					
					
					if($LCLServiceDatas[23]=="")
					{
					
						$error_flag=true;
						$err_msg['des_l_t']=t($this->t_base.'messages/destination_local_time_is_required');
					}
					else 
					{
						$fColumnDataArr=fColumnData();
						if(!empty($fColumnDataArr))
						{
							 $des_Time=date('H:i',$LCLServiceDatas[23]);
							if(!in_array($des_Time,$fColumnDataArr))
							{
								$error_flag=true;
								$err_msg['des_l_t']=t($this->t_base.'messages/invalid_destination_local_time');
							}
						}
					}
					
					
					if($LCLServiceDatas[24]=="")
					{
					
						$error_flag=true;
						$err_msg['booking_cut_off']=t($this->t_base.'messages/booking_cut_off_day_is_required');
					}
					else 
					{
						$gColumnDataArr=gColumnData();
						if(!empty($gColumnDataArr))
						{
							if(!in_array(strtolower($LCLServiceDatas[24]),$gColumnDataArr))
							{
								$error_flag=true;
								$err_msg['booking_cut_off']=t($this->t_base.'messages/invalid_booking_cut_off_day');
							}
						}
					}
					
					if($LCLServiceDatas[25]=="")
					{
						$error_flag=true;
						$err_msg['v_date']=t($this->t_base.'messages/validity_from_date_is_required');
					}
					
					
					if($LCLServiceDatas[26]=="")
					{
						$error_flag=true;
						$err_msg['e_date']=t($this->t_base.'messages/expiry_date_is_required');
					}
					else
					{
						if($LCLServiceDatas[26]<=strtotime(date('Y-m-d')))
						{
							$error_flag=true;
							$err_msg['e_date']=t($this->t_base.'messages/expiry_date_should_be_greater_than_today');
						}
					}
					
					
					if($LCLServiceDatas[26]!="" && $LCLServiceDatas[25]!="")
					{
						if($LCLServiceDatas[25]>$LCLServiceDatas[26])
						{
							$error_flag=true;
							$err_msg['e_date']=t($this->t_base.'messages/validity_from_date_should_be_less_than_expiry_date');
						}
					}
					
					if(!$error_flag)
					{
                                            $newLCLServiceData[$k]=$LCLServiceDatas;
                                            $newLCLServiceData[$k]['iOriginChargesApplicable'] = $allowedOriginChargesFlag;
                                            $newLCLServiceData[$k]['iDestinationChargesApplicable'] = $allowedDestinationChargesFlag;
					}
					else
					{
                                            $err_msg_arr=implode(", ",$err_msg);
                                            $errmsg_arr[]=t($this->t_base.'messages/following_error_in_row_number')." ".$k." - ".$LCLServiceDatas[0]."/".$LCLServiceDatas[1]."/".$LCLServiceDatas[2]."/  ".$LCLServiceDatas[3]."/".$LCLServiceDatas[4]."/".$LCLServiceDatas[5].""." : ".$err_msg_arr;
                                            ++$l_err;
					}
			}
				
				++$k;
			}
		}
		/*print_r($newLCLServiceData);
		echo "----------<br/><br/>";
		print_r($deleteWarehouseCombination);	
		echo "----------<br/><br/>";
		print_r($errmsg_arr);
		die;*/
		
		$l_del_success=0;
		
		if(!empty($deleteWarehouseCombination))
		{
			$i=0;
			$deleteFromWarehouse=array();
			$deleteToWarehouse=array();
			foreach($deleteWarehouseCombination as $deleteWarehouseCombinations)
			{
				$query="
                                    UPDATE
                                        ".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
                                    SET
                                        iActive='0',
                                        dtUpdatedOn=NOW()
                                    WHERE
                                        idWarehouseFrom='".(int)$deleteWarehouseCombinations['fromWareHouse']."'
                                    AND
                                        idWarehouseTo='".(int)$deleteWarehouseCombinations['toWareHouse']."'
				";		
				//echo $query."<br/>";
				$result=$this->exeSQL($query);
				++$i;
				++$l_del_success;
				if(!in_array($deleteWarehouseCombinations['fromWareHouse'],$deleteFromWarehouse) && !in_array($deleteWarehouseCombinations['toWareHouse'],$deleteToWarehouse))
				{
					$deleteToWarehouse[]=$deleteWarehouseCombinations['toWareHouse'];
					$deleteFromWarehouse[]=$deleteWarehouseCombinations['fromWareHouse'];
					
				}
			}
		}
		if(!empty($newLCLServiceData))
		{	
			$updateFromWarehouseArr=array();	
			$updateToWarehouseArr=array();
			$rss_counter = 1;
			$totRowcount = count($newLCLServiceData);
			//echo ""
			$randomIndex = rand(1,$totRowcount);
			$szTradeDescription = '';
			
			foreach($newLCLServiceData as $keys=>$newLCLServiceDatas)
			{
				$error_flag=false;
				$err_msg=array();
				$toWareHouseId=0;
				$fromWareHouseId=0;
				$originCountry=sanitize_all_html_input($newLCLServiceDatas[0]);
				$originCity=sanitize_all_html_input($newLCLServiceDatas[1]);
				$originCFS=sanitize_all_html_input($newLCLServiceDatas[2]);
				
				$desCountry=sanitize_all_html_input($newLCLServiceDatas[3]);
				$desCity=sanitize_all_html_input($newLCLServiceDatas[4]);
				$desCFS=sanitize_all_html_input($newLCLServiceDatas[5]);
				
				$originCountryId=$this->getCountryId($originCountry);
				
				$desCountryId=$this->getCountryId($desCountry);
				
				if($randomIndex == $rss_counter)
				{
                                    $szOriginCountryName = $originCountry ;
                                    $szDestinationCountryName = $desCountry ;
				}
				$query="
					SELECT
                                            id
					FROM
                                            ".__DBC_SCHEMATA_WAREHOUSES__."
					WHERE
                                            szWareHouseName='".mysql_escape_custom($originCFS)."' 	
					AND
                                            szCity='".mysql_escape_custom($originCity)."'
                                        AND
                                            idCountry='".(int)$originCountryId."'
					AND
                                            idForwarder='".(int)$idForwarder."' 
                                        AND
                                            iWarehouseType = '".(int)$iWarehouseType."'
                                        AND
                                            iActive = '1'
				";
				if($result=$this->exeSQL($query))
				{
                                    if($this->iNumRows<=0)
                                    {
                                        $error_flag=true;
                                        //$err_msg=$newLCLServiceDatas[0]."".$newLCLServiceDatas[1]."/".$newLCLServiceDatas[2]."/".$newLCLServiceDatas[3]."/".$newLCLServiceDatas[4]."/".$newLCLServiceDatas[5]."/ To Warehouse name does not related to forwarder on row ".$k;
                                        $err_msg['from_warehouse']=t($this->t_base.'messages/from_warehouse_name_does_not_related_to_forwarder');

                                    }
                                    else
                                    {
                                        $row=$this->getAssoc($result);
                                        $fromWareHouseId=$row['id'];
                                    }
				}
				
				$query="
                                    SELECT
                                        id
                                    FROM
                                        ".__DBC_SCHEMATA_WAREHOUSES__."
                                    WHERE
                                        szWareHouseName='".mysql_escape_custom($desCFS)."' 	
                                    AND
                                        szCity='".mysql_escape_custom($desCity)."'
                                    AND
                                        idCountry='".(int)$desCountryId."'
                                    AND
                                        idForwarder='".(int)$idForwarder."' 
                                    AND
                                        iWarehouseType = '".(int)$iWarehouseType."'
                                    AND
                                        iActive = '1'
				";
				//echo $query."<br/>";
				if($result=$this->exeSQL($query))
				{
					if($this->iNumRows<=0)
					{ 
                                            $error_flag=true;
                                            //$err_msg=$newLCLServiceDatas[0]."".$newLCLServiceDatas[1]."/".$newLCLServiceDatas[2]."/".$newLCLServiceDatas[3]."/".$newLCLServiceDatas[4]."/".$newLCLServiceDatas[5]."/ To Warehouse name does not related to forwarder on row ".$k;
                                            $err_msg['to_warehouse']=t($this->t_base.'messages/to_warehouse_name_does_not_related_to_forwarder');
						
					}
					else
					{
						$row=$this->getAssoc($result);
						$toWareHouseId=$row['id'];
					}
				}
				
				if(!$error_flag)
				{
				
					$fromDate='';
					//$fromDateArr=explode("/",$newLCLServiceDatas[17]);
					$fromDate=date("Y-m-d",trim(sanitize_all_html_input($newLCLServiceDatas[25])));
					
					$expiryDate='';
					//$expiryDateArr=explode("/",$newLCLServiceDatas[18]);
					$expiryDate=date("Y-m-d",trim(sanitize_all_html_input($newLCLServiceDatas[26])));
					
					$bookingHours='';
					$bookingHourArr=explode(' ',strtolower(sanitize_all_html_input($newLCLServiceDatas[24])));
				
					
					if($bookingHourArr[1]=='days')
					{
						$bookingHours=($bookingHourArr[0]*24);
					}
					else if($bookingHourArr[1]=='hours')
					{
						$bookingHours=$bookingHourArr[0];
					}
					
					$iTransitHours='';
					$iTransitHours=(sanitize_all_html_input($newLCLServiceDatas[21])*24);
				
					$idFrequency = '';
					$idFrequency=$this->getFrequencyId(ucfirst(strtolower(sanitize_all_html_input($newLCLServiceDatas[18]))));
					//echo $idFrequency;
					
					$iCutOffDay='';
					$iCutOffDay=$this->getDayId(ucfirst(strtolower(sanitize_all_html_input($newLCLServiceDatas[19]))));
					
					$idOriginChangeCurrency='';
					$idOriginChangeCurrency=$this->getFreightCurrencyId(strtoupper(sanitize_all_html_input($newLCLServiceDatas[9])));
					
					$currencyId='';
					$currencyId=$this->getFreightCurrencyId(strtoupper(sanitize_all_html_input($newLCLServiceDatas[13])));
					
					$idDestinationCurrency = '';
					$idDestinationCurrency = $this->getFreightCurrencyId(strtoupper(sanitize_all_html_input($newLCLServiceDatas[17])));
						//swap variables for both  
					 $des_Time=date('H:i',sanitize_all_html_input($newLCLServiceDatas[23]));					  
					 $cutoff_Time=date('H:i',sanitize_all_html_input($newLCLServiceDatas[20]));
					
					 $availableDay=$this->getAvailableDay(sanitize_all_html_input($newLCLServiceDatas[21]),$iCutOffDay);
					
					$query="
						SELECT
							wtw.id,
							wtw.idWarehouseFrom,
							wtw.idWarehouseTo,
							wtw.iBookingCutOffHours,
							wtw.idCutOffDay,
							wtw.szCutOffLocalTime,
							wtw.idAvailableDay,
							wtw.szAvailableLocalTime,
							wtw.iTransitHours,
							wtw.idFrequency,
							wtw.fOriginChargeRateWM,
							wtw.fOriginChargeMinRateWM,
							wtw.fOriginChargeBookingRate,
							wtw.szOriginChargeCurrency,
							wtw.fFreightRateWM as fRateWM,
							wtw.fFreightMinRateWM as fMinRateWM,
							wtw.fFreightBookingRate as fRate,
							wtw.fDestinationChargeRateWM,
							wtw.fDestinationChargeMinRateWM,
							wtw.fDestinationChargeBookingRate,
							wtw.szDestinationChargeCurrency,
							wtw.szFreightCurrency,
							wtw.dtValidFrom,
							wtw.dtExpiry,
							wtw.iTransitDays
						FROM
							".__DBC_SCHEMATA_WAREHOUSES_PRICING__." wtw
						WHERE
							idWarehouseFrom='".(int)$fromWareHouseId."'
						AND
							idWarehouseTo='".(int)$toWareHouseId."'
						AND
							idCutOffDay='".mysql_escape_custom($iCutOffDay)."'
						AND
							dtExpiry='".mysql_escape_custom($expiryDate)."'
						";
						//echo $query."<br />";
						if($result=$this->exeSQL($query))
						{
							if($this->iNumRows>0)
							{
								$row=$this->getAssoc($result);
								
								$kWhs_origin=new cWHSSearch();
								$kWhs_destination = new cWHSSearch();
								
								$kWhs_origin->load($fromWareHouseId);
								$kWhs_destination->load($toWareHouseId);
									
								$cutoffTimeAry = explode(':',$cutoff_Time);
								$cutoffTime = $cutoffTimeAry[0]+($cutoffTimeAry[1]/60) ;
									
								$availabelTimeAry = explode(':',$des_Time);
								$availabelTime = $availabelTimeAry[0]+($availabelTimeAry[1]/60) ;
									
								$transitHours = (int)(($iTransitHours) + ($availabelTime - $cutoffTime ) +(($kWhs_origin->szUTCOffset - $kWhs_destination->szUTCOffset)/(1000*60*60)));
										
										
								
								$updateflag=false;
								if($row['iBookingCutOffHours']!=$bookingHours)
								{
									///echo "hello1<br />";
									$updateflag=true;							
								}
								
								if($row['idCutOffDay']!=$iCutOffDay)
								{
									//echo "hello2<br />";
									$updateflag=true;							
								}
								
								if($row['szCutOffLocalTime']!=$cutoff_Time)
								{
									//echo "hello3<br />";
									$updateflag=true;							
								}
								
								if($row['idAvailableDay']!=$availableDay)
								{
									//echo "hello4<br />";
									$updateflag=true;							
								}
								
								if($row['szAvailableLocalTime']!=$des_Time)
								{
									//echo "hello5<br />";
									$updateflag=true;							
								}
								
								if($row['iTransitHours']!=$transitHours)
								{
									//echo "hello6<br />";
									$updateflag=true;							
								}
								
								if($row['idFrequency']!=$idFrequency)
								{
									//echo "hello7<br />";
									$updateflag=true;							
								}
								
								if($row['fOriginChargeRateWM']!=(float)$newLCLServiceDatas[6])
								{
									//echo $newLCLServiceDatas[6];
									//echo $row['fOriginChargeRateWM'];
									//echo "hello8<br />";
									$updateflag=true;							
								}
								
								if($row['fOriginChargeMinRateWM']!=(float)$newLCLServiceDatas[7])
								{
									//echo "hello9<br />";
									$updateflag=true;							
								}
								
								if($row['fOriginChargeBookingRate']!=(float)$newLCLServiceDatas[8])
								{
									//echo "hello10<br />";
									$updateflag=true;							
								}
								
								if($row['szOriginChargeCurrency']!=(int)$idOriginChangeCurrency)
								{
									//echo $idOriginChangeCurrency;
									
									//echo $row['szOriginChargeCurrency'];
									////echo "hello11<br />";
									$updateflag=true;							
								}
								
								
								if($row['fRateWM']!=(float)$newLCLServiceDatas[10])
								{
									//echo "hello12<br />";
									$updateflag=true;							
								}
								
								if($row['fMinRateWM']!=(float)$newLCLServiceDatas[11])
								{
									//echo "hello13<br />";
									$updateflag=true;							
								}
								
								if($row['fRate']!=(float)$newLCLServiceDatas[12])
								{
									//echo "hello14<br />";
									$updateflag=true;							
								}
								
								if($row['szFreightCurrency']!=(int)$currencyId)
								{
									//echo "hello15<br />";
									$updateflag=true;							
								}
								
								if($row['fDestinationChargeRateWM']!=(float)$newLCLServiceDatas[14])
								{
									//echo "hello16<br />";
									$updateflag=true;							
								}
								
								if($row['fDestinationChargeMinRateWM']!=(float)$newLCLServiceDatas[15])
								{
									//echo "hello17<br />";
									$updateflag=true;							
								}								
								if($row['fDestinationChargeBookingRate']!=(float)$newLCLServiceDatas[16])
								{
									//echo "hello18<br />";
									$updateflag=true;							
								}								
								if($row['szDestinationChargeCurrency']!=(int)$idDestinationCurrency)
								{
									//echo "hello19<br />";
									$updateflag=true;							
								}
								
								if(date('Y-m-d',strtotime($row['dtValidFrom']))!=$fromDate)
								{
									//echo "hello20<br />";
									$updateflag=true;							
								}
								if(date('Y-m-d',strtotime($row['dtExpiry']))!=$expiryDate)
								{
									//echo "hello21<br />";
									$updateflag=true;							
								}
								if($updateflag)
								{
									//die;
									$fromtoid=$fromWareHouseId."_".$toWareHouseId;
									if(!in_array($fromtoid,$updateFromWarehouseArr))
									{
										$updateFromWarehouseArr[]=$fromWareHouseId."_".$toWareHouseId;
										//$updateToWarehouseArr[]=;
										$query="
											UPDATE
												".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
											SET
												iActive='0'
											WHERE
										   		idWarehouseFrom='".(int)$fromWareHouseId."'
										   	AND
										   		idWarehouseTo='".(int)$toWareHouseId."'
										";	
										//echo $query."<br/>";
										$result=$this->exeSQL($query);
									}
									
									if((int)$_SESSION['forwarder_admin_id']>0)
									{
										$idAdmin = (int)$_SESSION['forwarder_admin_id'];
										$idForwarderContact = 0 ;
									}
									else
									{
										$idAdmin = 0;
										$idForwarderContact = (int)$_SESSION['forwarder_user_id'];
									}
															
									$query="
										INSERT INTO
											".__DBC_SCHEMATA_FORWARDER_WTW_ARCHIVE__."
										(
												idWarehouseFrom,
												idWarehouseTo,
												iBookingCutOffHours,
												idCutOffDay,
												szCutOffLocalTime,
												idAvailableDay,
												szAvailableLocalTime,
												iTransitHours,
												idFrequency,												
												fOriginChargeRateWM,
												fOriginChargeMinRateWM,
												fOriginChargeBookingRate,
												szOriginChargeCurrency,												
												fFreightRateWM,
												fFreightMinRateWM,
												fFreightBookingRate,
												szFreightCurrency,												
												fDestinationChargeRateWM,
												fDestinationChargeMinRateWM,
												fDestinationChargeBookingRate,
												szDestinationChargeCurrency,																							
												dtValidFrom,
												dtExpiry,
												idForwarder,
												idForwarderContact,
												idAdmin,
												dtUploaded
											)
												VALUES
											(
												'".(int)$fromWareHouseId."',
												'".(int)$toWareHouseId."',
												'".mysql_escape_custom($row['iBookingCutOffHours'])."',
												'".mysql_escape_custom($row['idCutOffDay'])."',
												'".mysql_escape_custom($row['szCutOffLocalTime'])."',
												'".mysql_escape_custom($row['idAvailableDay'])."',
												'".mysql_escape_custom($row['szAvailableLocalTime'])."',
												'".mysql_escape_custom($row['iTransitHours'])."',
												'".mysql_escape_custom($row['idFrequency'])."',												
												'".(float)$row['fOriginChargeRateWM']."',
												'".(float)$row['fOriginChargeMinRateWM']."',
												'".(float)$row['fOriginChargeBookingRate']."',
												'".(int)$row['szOriginChargeCurrency']."',												
												'".(float)$row['fRateWM']."',
												'".(float)$row['fMinRateWM']."',
												'".(float)$row['fRate']."',
												'".(int)$row['szFreightCurrency']."',												
												'".(float)$row['fDestinationChargeRateWM']."',
												'".(float)$row['fDestinationChargeMinRateWM']."',
												'".(float)$row['fDestinationChargeBookingRate']."',
												'".(int)$row['szDestinationChargeCurrency']."',												
												'".mysql_escape_custom($row['dtValidFrom'])."',
												'".mysql_escape_custom($row['dtExpiry'])."',
												'".(int)$idForwarder."',
												'".(int)$idForwarderContact."',
												'".(int)$idAdmin."',
												NOW()						
											)
										";
										//echo "<br /> ".$query."<br />";die;
										$result=$this->exeSQL($query);
										
										
										$query="
                                                                                    UPDATE
                                                                                        ".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
                                                                                    SET
                                                                                        iBookingCutOffHours='".mysql_escape_custom($bookingHours)."',
                                                                                        idCutOffDay='".mysql_escape_custom($iCutOffDay)."',
                                                                                        szCutOffLocalTime='".mysql_escape_custom($cutoff_Time)."',
                                                                                        idAvailableDay='".mysql_escape_custom($availableDay)."',
                                                                                        szAvailableLocalTime='".mysql_escape_custom($des_Time)."',
                                                                                        iTransitHours='".mysql_escape_custom($transitHours)."',
                                                                                        iTransitDays = '".(int)($iTransitHours/24)."',
                                                                                        idFrequency='".(int)$idFrequency."',												
                                                                                        fOriginChargeRateWM='".(float)$newLCLServiceDatas[6]."',
                                                                                        fOriginChargeMinRateWM='".(float)$newLCLServiceDatas[7]."',
                                                                                        fOriginChargeBookingRate='".(float)$newLCLServiceDatas[8]."',
                                                                                        szOriginChargeCurrency='".(int)$idOriginChangeCurrency."',												
                                                                                        iOriginChargesApplicable = '".(int)$newLCLServiceDatas['iOriginChargesApplicable']."',
                                                                                        fFreightRateWM='".(float)$newLCLServiceDatas[10]."',
                                                                                        fFreightMinRateWM='".(float)$newLCLServiceDatas[11]."',
                                                                                        fFreightBookingRate='".(float)$newLCLServiceDatas[12]."',
                                                                                        szFreightCurrency='".(int)$currencyId."',		
                                                                                        fDestinationChargeRateWM='".(float)$newLCLServiceDatas[14]."',
                                                                                        fDestinationChargeMinRateWM='".(float)$newLCLServiceDatas[15]."',
                                                                                        fDestinationChargeBookingRate='".(float)$newLCLServiceDatas[16]."',
                                                                                        szDestinationChargeCurrency='".(int)$idDestinationCurrency."',	
                                                                                        iDestinationChargesApplicable = '".(int)$newLCLServiceDatas['iDestinationChargesApplicable']."',											
                                                                                        dtValidFrom='".mysql_escape_custom($fromDate)."',
                                                                                        dtExpiry='".mysql_escape_custom($expiryDate)."',
                                                                                        iActive='1',
                                                                                        dtUpdatedOn=NOW()
                                                                                    WHERE
                                                                                        idWarehouseFrom='".(int)$fromWareHouseId."'
                                                                                    AND
                                                                                        idWarehouseTo='".(int)$toWareHouseId."'
                                                                                    AND
                                                                                        idCutOffDay='".mysql_escape_custom($iCutOffDay)."'
                                                                                    AND
                                                                                        dtExpiry='".mysql_escape_custom($expiryDate)."'        
										";
									   //echo "<br />".$query."<br/>";
                                                                            if($result=$this->exeSQL($query))
                                                                            {
                                                                                if($this->getRowCnt()>0)
                                                                                {
                                                                                    ++$l_success;
                                                                                    $szTradeCountryAry[] = 'From '.$originCountry.' to '.$desCountry ;
                                                                                    //$szTradeDescription .= 'From '.$originCountry.' to '.$desCountry."<br />" ;
                                                                                }
                                                                            }
								}
								else
								{ 
									$fromtoid=$fromWareHouseId."_".$toWareHouseId;
									if(!in_array($fromtoid,$updateFromWarehouseArr))
									{
										$updateFromWarehouseArr[]=$fromWareHouseId."_".$toWareHouseId;

										$query="
                                                                                    UPDATE
                                                                                        ".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
                                                                                    SET
                                                                                        iActive='0'
                                                                                    WHERE
                                                                                        idWarehouseFrom='".(int)$fromWareHouseId."'
                                                                                    AND
                                                                                        idWarehouseTo='".(int)$toWareHouseId."'
										";	
										//echo $query."<br/>";
										$result=$this->exeSQL($query);
									}
								
									$query="
                                                                            UPDATE
                                                                                ".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
                                                                            SET
                                                                                iActive='1',
                                                                                dtUpdatedOn=NOW()
									    WHERE
                                                                                idWarehouseFrom='".(int)$fromWareHouseId."'
                                                                            AND
                                                                                idWarehouseTo='".(int)$toWareHouseId."'
                                                                            AND
                                                                                idCutOffDay='".mysql_escape_custom($iCutOffDay)."'
                                                                            AND
                                                                                DATE(dtExpiry)='".mysql_escape_custom($expiryDate)."'
                                                                        ";
									//echo $query;
									$result=$this->exeSQL($query); 
								}
							}
							else
							{
							
							$fromtoid=$fromWareHouseId."_".$toWareHouseId;
							if(!in_array($fromtoid,$updateFromWarehouseArr))
							{
								$updateFromWarehouseArr[]=$fromWareHouseId."_".$toWareHouseId;
								
								$query="
									UPDATE
										".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
									SET
										iActive='0'
									WHERE
								   		idWarehouseFrom='".(int)$fromWareHouseId."'
								   	AND
								   		idWarehouseTo='".(int)$toWareHouseId."'
								";	
								//echo $query."<br/>";
								$result=$this->exeSQL($query);
							}
							
							$kWhs_origin=new cWHSSearch();
							$kWhs_destination = new cWHSSearch();
							
							$kWhs_origin->load($fromWareHouseId);
							$kWhs_destination->load($toWareHouseId);
							
							$cutoffTimeAry = explode(':',$cutoff_Time);
							$cutoffTime = $cutoffTimeAry[0]+($cutoffTimeAry[1]/60) ;
							
							$availabelTimeAry = explode(':',$des_Time);
							$availabelTime = $availabelTimeAry[0]+($availabelTimeAry[1]/60) ;
							
							$transitHours = (int)(($iTransitHours) + ($availabelTime - $cutoffTime ) +(($kWhs_origin->szUTCOffset - $kWhs_destination->szUTCOffset)/(1000*60*60)));
										
							
							
								$query="
									INSERT INTO
										".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
									(
											idWarehouseFrom,
											idWarehouseTo,
											iBookingCutOffHours,
											idCutOffDay,
											szCutOffLocalTime,
											idAvailableDay,
											szAvailableLocalTime,											
											iTransitHours,
											iTransitDays,
											idFrequency,											
											fOriginChargeRateWM,
											fOriginChargeMinRateWM,
											fOriginChargeBookingRate,
											szOriginChargeCurrency,	
											iOriginChargesApplicable,										
											fFreightRateWM,
											fFreightMinRateWM,
											fFreightBookingRate,
											szFreightCurrency,											
											fDestinationChargeRateWM,
											fDestinationChargeMinRateWM,
											fDestinationChargeBookingRate,
											szDestinationChargeCurrency,	
											iDestinationChargesApplicable,										
											dtValidFrom,
											dtExpiry,
											iActive,
											dtCreatedOn,
											dtUpdatedOn								
											
										)
                                                                                VALUES
										(
											'".(int)$fromWareHouseId."',
											'".(int)$toWareHouseId."',
											'".mysql_escape_custom($bookingHours)."',
											'".mysql_escape_custom($iCutOffDay)."',
											'".mysql_escape_custom($cutoff_Time)."',
											'".mysql_escape_custom($availableDay)."',
											'".mysql_escape_custom($des_Time)."',
											'".(int)$transitHours."',
											'".(int)mysql_escape_custom($iTransitHours/24)."',
											'".(int)$idFrequency."',
											'".(float)$newLCLServiceDatas[6]."',
											'".(float)$newLCLServiceDatas[7]."',
											'".(float)$newLCLServiceDatas[8]."',
											'".(int)$idOriginChangeCurrency."',
											'".(int)$newLCLServiceDatas['iOriginChargesApplicable']."',
											'".(float)$newLCLServiceDatas[10]."',
											'".(float)$newLCLServiceDatas[11]."',
											'".(float)$newLCLServiceDatas[12]."',
											'".(int)$currencyId."',
											'".(float)$newLCLServiceDatas[14]."',
											'".(float)$newLCLServiceDatas[15]."',
											'".(float)$newLCLServiceDatas[16]."',
											'".(int)$idDestinationCurrency."',
											'".(int)$newLCLServiceDatas['iDestinationChargesApplicable']."',
											'".mysql_escape_custom($fromDate)."',
											'".mysql_escape_custom($expiryDate)."',
											'1',
											NOW(),
											NOW()						
										)
									";
									//echo "<br />".$query."<br />";die;
									$result=$this->exeSQL($query);
									++$l_success;
									
									$szTradeCountryAry[] = 'From '.$originCountry.' to '.$desCountry ;
							}
                                                        
						}
						
                                                
				}
				else
				{
					$err_msg_arr=implode(", ",$err_msg);
					$errmsg_arr[]=t($this->t_base.'messages/following_error_in_row_number')." ".$keys." - ".$newLCLServiceDatas[0]."/".$newLCLServiceDatas[1]."/".$newLCLServiceDatas[2]."/  ".$newLCLServiceDatas[3]."/".$newLCLServiceDatas[4]."/".$newLCLServiceDatas[5]." : ".$err_msg_arr;
					++$l_err;
				}
				$rss_counter++;
			}
		}	
		
		if(!empty($szTradeCountryAry))
		{
			$szTradeCountryArys = array_unique($szTradeCountryAry);
		}
		if(!empty($szTradeCountryArys))
		{
			$szTradeDescription = implode("<br />",$szTradeCountryArys) ;
		}
		
		$this->successLine=$l_success;
		$this->errorLine=$l_err;
		$this->deleteLine=$l_del_success;
		//print_r($errmsg_arr);
		$updateLine=$this->successLine+$this->deleteLine;
		$err_str="";
		 
                if((int)$_SESSION['forwarder_admin_id']>0)
                {
                    $idAdmin = (int)$_SESSION['forwarder_admin_id'];  
                    $kForwarderContact = new cAdmin();
                    $kForwarderContact->getAdminDetails($idAdmin);
                }
                else
                { 
                    $idForwarderContact = (int)$_SESSION['forwarder_user_id'];
                    $kForwarderContact = new cForwarderContact();
                    $kForwarderContact->load($idForwarderContact);
                }
		
		$this->szEmail=$kForwarderContact->szEmail;
		
		if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
                { 
                    $szServiceName = "airfreight";
                }
                else
                {
                    $szServiceName = "LCL";
                }
		$SuccessCount="You have successfully updated ".$updateLine." ".$szServiceName." service lines.";
		
		$kForwarder = new cForwarder();
		$idForwarder = $_SESSION['forwarder_id'];
		$kForwarder->load($idForwarder);
		
		if(($kForwarder->iAgreeTNC==1) && ($this->successLine>0))
		{
                    $replace_ary = array();
                    $replace_ary['szForwarderDisplayName'] = $kForwarder->szDisplayName ;
                    $replace_ary['szOriginCountry'] = $szOriginCountryName;
                    $replace_ary['szDestinationCountry'] = $szDestinationCountryName;
                    $replace_ary['iTradeCount'] = $updateLine-1 ;
                    $replace_ary['szTradeName'] = $szTradeDescription ;
                    $replace_ary['szSiteUrl'] = __MAIN_SITE_HOME_PAGE_SECURE_URL__ ;
                    if($updateLine==1)
                    {
                        $replace_ary['szTradeCountText'] =""; 
                    }
                    else 
                    {
                        $replace_ary['szTradeCountText'] = ' and '.($updateLine-1).' other trades';
                    }


                    $content_ary = array();
                    $content_ary = $this->replaceRssContent('__BULK_LCL_SERVICES__',$replace_ary);

                    $rssDataAry=array();
                    $rssDataAry['idForwarder'] = $kForwarder->id ;
                    $rssDataAry['szFeedType'] = '__BULK_LCL_SERVICES__';
                    $rssDataAry['szLink'] = __RSS_FEED_PAGE_URL__ ;
                    $rssDataAry['szTitle'] = $content_ary['szTitle'];
                    $rssDataAry['szDescription'] = $content_ary['szDescription'];
                    $rssDataAry['szDescriptionHTML'] = $content_ary['szDescriptionHTML'];

                    $kRss = new cRSS();
                    $kRss->addRssFeed($rssDataAry);
		}
				
		//echo $this->szEmail;
		if(!empty($errmsg_arr))
		{
                    $l=1;
                    $err_str .="We found errors in following ".$this->errorLine." ".$szServiceName." service lines in your upload file:";
                    foreach($errmsg_arr as $errmsg_arrs)
                    {
                        $err_str .="<br/>".$l.". ".$errmsg_arrs."<br/>";
                        ++$l;
                    }
                    $err_str .="<br/>Please correct the errors and upload the file again.";
		}
		$replace_ary['SuccessCount']=$SuccessCount; 
		$replace_ary['szErrorMsg']=$err_str;
		$replace_ary['szEmail']=$kForwarderContact->szEmail;
                $replace_ary['szName'] = $kForwarderContact->szFirstName." ".$kForwarderContact->szLastName;
                 
                createEmail(__FORWARDER_BULK_UPLOAD_EMAIL__, $replace_ary,$this->szEmail, __FORWARDER_BULK_UPLOAD_EMAIL_SUBJECT__, __STORE_SUPPORT_EMAIL__,$_SESSION['forwarder_id'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__);            
		return true;
	}
	
	function getCountryId($szCounrtyName)
	{
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_COUNTRY__."
			WHERE
				szCountryName='".mysql_escape_custom($szCounrtyName)."'				
		";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$row=$this->getAssoc($result);
				return $row['id'];
			}
		}	
	}
	
	function checkDateFormat($date)
	{
	  //match the format of the date
	  if (preg_match ("/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/", $date, $parts))
	  {
	    //check weather the date is valid of not
	        if(checkdate($parts[2],$parts[1],$parts[3]))
	          return true;
	        else
	         return false;
	  }
	  else
	    return false;
	}
	
	
	function getForwaderCountries($idForwarder,$iWarehouseType=false)
	{
            if((int)$idForwarder>0)
            {
                if($iWarehouseType>0)
                {
                    $query_and = " AND w.iWarehouseType = '".(int)$iWarehouseType."' ";
                }
                $query="
                    SELECT
                        c.szCountryName,
                        c.id as idCountry
                    FROM
                        ".__DBC_SCHEMATA_WAREHOUSES__." AS w
                    INNER JOIN
                        ".__DBC_SCHEMATA_COUNTRY__." AS c
                    ON
                        c.id=w.idCountry
                    WHERE
                        w.idForwarder='".(int)$idForwarder."'
                    AND
                        w.iActive = '1'
                    AND
                        w.isDeleted = '0'
                        $query_and
                    GROUP BY
                        w.idCountry
                    ORDER BY
                        c.szCountryName ASC
                ";
                //echo $query;
                if($result=$this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        while($row=$this->getAssoc($result))
                        {
                            $forwarderCountries[]=$row; 
                        }				
                        return $forwarderCountries;
                    }
                }	
            }
            else
            {
                    return array();
            }
	}
	
	function getForwaderWarehouses($idForwarder,$idCountry=0,$szCity=false,$orderby=false,$iWarehouseType=false,$key_value_pair=false)
	{
            if((int)$idForwarder>0)
            {
                $sql="";
                if($idCountry!='0' && $idCountry!='')
                {
                    $sql .="
                        AND
                            w.idCountry IN (".$idCountry.")	
                    ";
                }
                if(!empty($szCity))
                {
                    $sql .="
                        AND
                            w.szCity = '".mysql_escape_custom(trim($szCity))."'	
                    ";
                } 
                if($iWarehouseType>0)
                {
                    $sql .=" AND  w.iWarehouseType = '".$iWarehouseType."'  ";
                }
                
                if($orderby)
                {
                    $sort_order="
                        w.szWareHouseName ASC
                    ";
                }
                else
                {
                    $sort_order="
                        c.szCountryName ASC,
                        w.szWareHouseName ASC
                    ";
                }

                $query="
                    SELECT
                        c.szCountryName,
                        w.szWareHouseName,
                        w.id,
                        w.id as idWarehouse,
                        w.iWarehouseType,
                        c.id as idCountry
                    FROM
                        ".__DBC_SCHEMATA_WAREHOUSES__." AS w
                    INNER JOIN
                        ".__DBC_SCHEMATA_COUNTRY__." AS c
                    ON
                        c.id=w.idCountry
                    WHERE
                        w.idForwarder='".(int)$idForwarder."'
                    AND
                        w.iActive = '1'	
                    AND
                        w.isDeleted = '0'
                        ".$sql."
                    ORDER BY
                        ".$sort_order."
                ";
                //echo "<br>".$query."<br>";
                if($result=$this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        while($row=$this->getAssoc($result))
                        {
                            if($key_value_pair)
                            {
                                $forwarderWarehouses[$row['idWarehouse']] = $row['iWarehouseType']; 
                            }
                            else if($bNameValuePair)
                            {
                                $forwarderWarehouses[$row['id']] = $row['iWarehouseType']; 
                            }
                            else
                            {
                                $forwarderWarehouses[]=$row; 
                            } 
                        }				
                        return $forwarderWarehouses;
                    }
                }	
            }
            else
            {
                return array();
            }
	}
	
	
	function getForwaderWarehousesCity($idForwarder,$idCountry,$iWarehouseType=false)
	{
            if((int)$idForwarder>0)
            {
                if($iWarehouseType>0)
                {
                    $query_and = " AND  w.iWarehouseType = '".$iWarehouseType."'  ";
                }
                $query="
                    SELECT
                        DISTINCT w.szCity
                    FROM
                        ".__DBC_SCHEMATA_WAREHOUSES__." w
                    INNER JOIN
                        ".__DBC_SCHEMATA_COUNTRY__." c
                    ON
                        c.id=w.idCountry
                    WHERE
                        w.idForwarder='".(int)$idForwarder."'
                    AND
                        w.idCountry = '".(int)$idCountry."'
                    AND
                        w.iActive = '1' 
                        $query_and
                    ORDER BY
                        w.szCity ASC
                ";
                //echo $query;
                if($result=$this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        $ctr=0;
                        while($row=$this->getAssoc($result))
                        {
                            $forwarderWarehouseCityAry[$ctr]['szCity']=$row['szCity'];
                            $ctr++;
                        }		
                        return $forwarderWarehouseCityAry;
                    }
                }	
            }
            else
            {
                return array();
            }
	}
	
	
	function getDayId($szDay)
	{
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_WEEK_DAYS__."
			WHERE
				szWeekDay='".mysql_escape_custom($szDay)."'
		";
		if( ( $result1 = $this->exeSQL( $query ) ) )
		{
			$row1 = $this->getAssoc($result1);
			
			return $row1['id'];
		}
	}
	
	
	function getFrequencyId($szFrequency)
	{
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_FREQUENCY__."
			WHERE
				szDescription='".mysql_escape_custom($szFrequency)."'
		";
		if( ( $result1 = $this->exeSQL( $query ) ) )
		{
			$row1 = $this->getAssoc($result1);
			
			return $row1['id'];
		}
	}
	
	function getFreightCurrencyId($szFreightCurrency)
	{
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_CURRENCY__."
			WHERE
				szCurrency='".mysql_escape_custom($szFreightCurrency)."'
		";
		if( ( $result1 = $this->exeSQL( $query ) ) )
		{
			
			$row1 = $this->getAssoc($result1);
		
			return $row1['id'];
			
		}
	}
	
	function getAvailableDay($iTransitDays,$iCutoffDay)
	{
	     if($iTransitDays>0 && $iCutoffDay>0)
	     {	   
                $rawDayId = (int)(((($iTransitDays/7) - floor($iTransitDays/7)) *7) + 0.0001) ;
                $dayid = floor($rawDayId);

                if($iCutoffDay > 7)
                {
                    $AvailableDay = ($dayid + $iCutoffDay) - 7 ;
                }
                else
                {
                      $AvailableDay = ($dayid + $iCutoffDay) ;
                }

                if($AvailableDay>7)
                {
                    $AvailableDay = $AvailableDay-7 ;
                }
                return $AvailableDay;
            }
	}
	
	function warehouseHaulageDataExport($objPHPExcel,$sheetindex,$fileName,$kConfig,$warehouseArr,$idForwarder,$downloadtype,$inActiveService=0,$idCurrency=0)
	{ 
		date_default_timezone_set('UTC');	
			if($sheetindex>0)
			{
				$objPHPExcel->createSheet();
			}
			
			$objPHPExcel->setActiveSheetIndex($sheetindex);
						
			$styleArray = array(
				'font' => array(
					'bold' => false,
					'size' =>12,
				),
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				),
				'borders' => array(
					'top' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'bottom' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'right' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'left' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
				),
			);
			
			$styleArray1 = array(
				'font' => array(
					'bold' => false,
					'size' =>12,
				),
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
				),
				'borders' => array(
					'top' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'bottom' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'right' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'left' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
				),
			);
			
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
			
			$objPHPExcel->getActiveSheet()->getStyle('A1:N2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$objPHPExcel->getActiveSheet()->getStyle('A1:N2')->getFill()->getStartColor()->setARGB('FFddd9c3');
			
							
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			
			$objPHPExcel->getActiveSheet()->mergeCells('A1:C1');
			$objPHPExcel->getActiveSheet()->setCellValue('A1','CFS or Airport Warehouse')->getStyle('A1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->applyFromArray($styleArray);
		//	$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->mergeCells('D1:F1');
			$objPHPExcel->getActiveSheet()->setCellValue('D1','Haulage')->getStyle('D1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('D1:F1')->applyFromArray($styleArray);
			
			
		
			
			$objPHPExcel->getActiveSheet()->setCellValue('G1','Weight')->getStyle('G1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->mergeCells('H1:K1');
			$objPHPExcel->getActiveSheet()->setCellValue('H1','Haulage Rate')->getStyle('J1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('H1:K1')->applyFromArray($styleArray);
				
			$objPHPExcel->getActiveSheet()->mergeCells('L1:N1');
			$objPHPExcel->getActiveSheet()->setCellValue('L1','Details (can not be updated here)')->getStyle('L1:M1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('L1:N1')->applyFromArray($styleArray);
			
			
			
			$objPHPExcel->getActiveSheet()->setCellValue('A2','Country')->getStyle('A2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('B2','City')->getStyle('B2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('C2','Name')->getStyle('B2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('C2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('D2','Direction')->getStyle('D2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('E2','Model')->getStyle('D2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('F2','Name or reference')->getStyle('D2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('F2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('G2','Up to Kg')->getStyle('E2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('G2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('H2','Per 100 kg')->getStyle('F2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('H2')->applyFromArray($styleArray);
			
			
			$objPHPExcel->getActiveSheet()->setCellValue('I2','Minimum')->getStyle('G2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('I2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('J2','Per Booking')->getStyle('H2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('J2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('K2','Currency')->getStyle('I2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('K2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('L2','Fuel surcharge')->getStyle('J2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('L2')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('M2','W/M factor')->getStyle('K2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('M2')->applyFromArray($styleArray);
			
					
			$objPHPExcel->getActiveSheet()->setCellValue('N2','Tranist time')->getStyle('M2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('N2')->applyFromArray($styleArray);

			$directionArr=array('1','2');
			$newwarehouseArr=array();
			$warehouseIdArr=array();
			if(!empty($warehouseArr))
			{
				foreach($warehouseArr as $warehouseArrs)
				{
					if(!in_array($warehouseArrs['idWarehouse'],$warehouseIdArr))
					{
						$warehouseIdArr[]=$warehouseArrs['idWarehouse'];
						$ctr=0;
					}
					else
					{
						$ctr=count($newwarehouseArr[$warehouseArrs['idWarehouse']]);
					}
					$newwarehouseArr[$warehouseArrs['idWarehouse']][$ctr]=$warehouseArrs['idPricingModel'];
					
				}
			}
			//print_r($newwarehouseArr);
			//die;
			//print_r($warehouseArr);
			if(!empty($warehouseArr))
			{
				$p=0;
				$sqlService='';
				$sqlService1='';
				$innerjoinquery='';
				if($inActiveService==1)
				{
					$sqlService .="ph.iActive='0'
						AND
							ph.idCurrency='".(int)$idCurrency."'	
					";
					$sqlService1="
						AND
							ph.iActive='0'
						AND
							ph.idCurrency='".(int)$idCurrency."'	
					";
					$innerjoinquery="
						INNER JOIN
							".__DBC_SCHEMATA_HAULAGE_PRICING__." AS ph
						ON
							phm.id=ph.idHaulagePricingModel			
						";
				}
				else
				{
					$sqlService .="ph.iActive='1'";
					$innerjoinquery='';
					$sqlService1='';
				}
				foreach($newwarehouseArr as $key=>$newwarehouseArrs)
				{
					
					foreach($directionArr as $directionArrs)
					{
						
						if(!empty($newwarehouseArrs))
						{
							$warehouseHaulageModelArr=$this->getHaulageModelPriority($key,$newwarehouseArrs,$directionArrs);
							//print_r($warehouseHaulageModelArr);	
							//echo "<br/><br/>";
							foreach($warehouseHaulageModelArr as $values)
							{
	
								$sortquery="";

								if($values=='4')
								{
									$sortquery .="phm.iDistanceUpToKm ASC";
								}
								else
								{
									$sortquery .="phm.szName ASC";	
								}
								$query="
									SELECT
										w.szWareHouseName,
										w.szCity,
										w.idCountry,
										ht.iHours,
										phm.szName,
										phm.iDistanceUpToKm,
										phm.idHaulageModel,
										phm.fWmFactor,
										phm.fFuelPercentage,
										phm.iDirection,
										phm.id as idPricingHaulageModel
									FROM
										".__DBC_SCHEMATA_WAREHOUSES__." AS w
									INNER JOIN
										".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." AS phm
									ON
										phm.idWarehouse=w.id
									INNER JOIN
										".__DBC_SCHEMATA_HAULAGE_TRANSIT_TIME__." AS ht
									ON
										phm.idHaulageTransitTime=ht.id
									INNER JOIN
										".__DBC_SCHEMATA_COUNTRY__." c
									ON
										c.id = w.idCountry
										".$innerjoinquery."
									WHERE
										w.idForwarder='".(int)$idForwarder."'
									AND
										phm.idHaulageModel='".(int)$values."'
									AND
										w.id='".(int)$key."'
									AND
										phm.iDirection='".(int)$directionArrs."' 
										$sqlService1
									ORDER BY
										c.szCountryName ASC,
										w.szCity ASC,
										w.szWareHouseName ASC,
										phm.idHaulageModel,
										phm.szName ASC,
										".$sortquery."
								";
								//echo $query."<br/><br/>";
								if( ( $result1 = $this->exeSQL( $query ) ) )
								{
											
									if ($this->getRowCnt() > 0)
									{
										$count=0;
										while($row1 = $this->getAssoc($result1))
										{
	
											$query="
												SELECT
													ph.iUpToKg,
													ph.fPricePer100Kg,
													ph.fMinimumPrice,
													ph.fPricePerBooking,
													ph.idCurrency,
													w.szWareHouseName,
													w.szCity,
													w.idCountry,
													ht.iHours,
													phm.szName,
													phm.iDistanceUpToKm,
													phm.idHaulageModel,
													phm.fWmFactor,
													phm.fFuelPercentage,
													phm.iDirection
												FROM
													".__DBC_SCHEMATA_WAREHOUSES__." AS w
												INNER JOIN
													".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." AS phm
												ON
													phm.idWarehouse=w.id
												INNER JOIN
													".__DBC_SCHEMATA_HAULAGE_PRICING__." AS ph
												ON
													ph.idHaulagePricingModel=phm.id
												INNER JOIN
													".__DBC_SCHEMATA_HAULAGE_TRANSIT_TIME__." AS ht
												ON
													phm.idHaulageTransitTime=ht.id
												INNER JOIN
													".__DBC_SCHEMATA_COUNTRY__." c
												ON
													c.id = w.idCountry
												WHERE
													w.idForwarder='".(int)$idForwarder."'
												AND
													phm.idWarehouse='".(int)$key."'
												AND
													phm.idHaulageModel='".(int)$values."'
												AND
													phm.id='".(int)$row1['idPricingHaulageModel']."' 
												AND
													".$sqlService."
												ORDER BY
													c.szCountryName ASC,
													w.szCity ASC,
													w.szWareHouseName ASC,
													phm.idHaulageModel,
													".$sortquery.",
													ph.iUpToKg ASC
													;	
											";
											//echo $query."<br/><br/>";
											//die;
											if( ( $result = $this->exeSQL( $query ) ) )
											{
																
												if ($this->getRowCnt() > 0)
												{
													$i=0;
													while($row = $this->getAssoc($result))
													{	
														$haulagDataArr[][$i]=$row;	
														$haulagDataCountCityNameArr['name'][$p]=$row['szWareHouseName'];
														$haulagDataCountCityNameArr['city'][$p]=$row['szCity'];
														$haulagDataCountCityNameArr['fWmFactor'][$p]=$row['fWmFactor'];
														$haulagDataCountCityNameArr['country'][$p]=$kConfig->getCountryName($row['idCountry']);
														if($row1['idHaulageModel']=='4')
															$haulagDataCountCityNameArr['szNameModel'][$p]="Up to ".number_format((float)$row['iDistanceUpToKm'])." km";
														else	
															$haulagDataCountCityNameArr['szNameModel'][$p]=$row['szName'];
														++$i;			
													}
												}
												else
												{
												
													$count=count($haulagDataArr);
													$haulagDataArr[][$count]=$row1;	
													$haulagDataCountCityNameArr['name'][$p]=$row1['szWareHouseName'];
													$haulagDataCountCityNameArr['city'][$p]=$row1['szCity'];
													$haulagDataCountCityNameArr['fWmFactor'][$p]=$row1['fWmFactor'];
													$haulagDataCountCityNameArr['country'][$p]=$kConfig->getCountryName($row1['idCountry']);
													if($row1['idHaulageModel']=='4')
														$haulagDataCountCityNameArr['szNameModel'][$p]="Up to ".number_format((float)$row1['iDistanceUpToKm'])." km";
													else	
														$haulagDataCountCityNameArr['szNameModel'][$p]=$row1['szName'];							
												}
												++$p;
											}
										}
									}
								}
							}
						}
					}
				}
			}
			//print_r($haulagDataArr);
			//die;
			/*if(!empty($warehouseArr))
			{
				$p=0;
				foreach($warehouseArr as $warehouseArrs)
				{
					$query="
						SELECT
							ph.*,
							w.szWareHouseName,
							w.szCity,
							w.idCountry,
							ht.iHours
						FROM
							".__DBC_SCHEMATA_WAREHOUSES__." AS w
						INNER JOIN
							".__DBC_SCHEMATA_PRICING_HAULAGE__." AS ph
						ON
							ph.idWarehouse=w.id
						INNER JOIN
							".__DBC_SCHEMATA_HAULAGE_TRANSIT_TIME__." AS ht
						ON
							ph.idHaulageTransitTime=ht.id
						WHERE
							w.idForwarder='".(int)$idForwarder."'
						AND
							w.id='".(int)$warehouseArrs."'
						AND
							ph.iActive='1'
						AND
							ph.idDirection='2'
						ORDER BY
							ph.iUpToKM ASC
						;
					";
					//echo $query;
					if( ( $result1 = $this->exeSQL( $query ) ) )
					{
										
						if ($this->getRowCnt() > 0)
						{
							$i=0;
							while($row1 = $this->getAssoc($result1))
							{
								$haulagDataArr[][$i]=$row1;	
								
								$haulagDataCountCityNameArr['name'][$p]=$row1['szWareHouseName'];
								$haulagDataCountCityNameArr['city'][$p]=$row1['szCity'];
								$haulagDataCountCityNameArr['country'][$p]=$kConfig->getCountryName($row1['idCountry']);
								++$i;			
							}
						}
						else
						{
							$query="
								SELECT
									w.szWareHouseName,
									w.szCity,
									w.idCountry
								FROM
									".__DBC_SCHEMATA_WAREHOUSES__." AS w
								WHERE
									w.idForwarder='".(int)$idForwarder."'
								AND
									w.id='".(int)$warehouseArrs."'	
							";
							//echo $query;
							if( ( $result1 = $this->exeSQL( $query ) ) )
							{
										
								if ($this->getRowCnt() > 0)
								{
									$count=0;
									$row1 = $this->getAssoc($result1);	
									$haulagDataArr[][0]=$row1;	
									$count=count($haulagDataArr)-1;
									$haulagDataArr[$count][0]['idDirection']='2';
									$haulagDataCountCityNameArr['name'][$p]=$row1['szWareHouseName'];
									$haulagDataCountCityNameArr['city'][$p]=$row1['szCity'];
									$haulagDataCountCityNameArr['country'][$p]=$kConfig->getCountryName($row1['idCountry']);
								}
							}
						}
					}
					++$p;
				}
			}*/
			//print_r($haulagDataArr);
			//die;
			$this->exportListHaulageData($objPHPExcel,'1',$fileName,$haulagDataCountCityNameArr);
			
			$rowcounter=2;
			if(!empty($haulagDataArr))
			{
				foreach($haulagDataArr as $haulagDataArrs)
				{
					if(!empty($haulagDataArrs))
					{
						foreach($haulagDataArrs as $haulagDataArray)
						{
						
							$colcounter=0;
							$rowcounter++;
							$fillColor='A'.$rowcounter.':F'.$rowcounter;
							$fill_color='A'.$rowcounter.':L'.$rowcounter;
							$objPHPExcel->getActiveSheet()->getStyle($fillColor)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
							$objPHPExcel->getActiveSheet()->getStyle($fillColor)->getFill()->getStartColor()->setARGB('FFddd9c3');
							$objPHPExcel->getActiveSheet()->getStyle($fillColor)->applyFromArray($styleArray);
							
							$fillColor_col='L'.$rowcounter.':N'.$rowcounter;
							$objPHPExcel->getActiveSheet()->getStyle($fillColor_col)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
							$objPHPExcel->getActiveSheet()->getStyle($fillColor_col)->getFill()->getStartColor()->setARGB('FFddd9c3');
							$objPHPExcel->getActiveSheet()->getStyle($fillColor_col)->applyFromArray($styleArray);
							
							$objPHPExcel->getActiveSheet()->getStyle('G'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0');
							$objPHPExcel->getActiveSheet()->getStyle('H'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0.00');
							$objPHPExcel->getActiveSheet()->getStyle('I'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0.00');
							$objPHPExcel->getActiveSheet()->getStyle('J'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0.00');
							
							$szCountry='';
							$currency='';
							if($haulagDataArray['idCurrency']!='')
							{
								$currency=$this->getFreightCurrency($haulagDataArray['idCurrency']);
							}
							if($haulagDataArray['idCountry']!='')
								$szCountry=$kConfig->getCountryName($haulagDataArray['idCountry']);
							
							//$expirydate='';
				   			$szName='';
				   			if($haulagDataArray['idHaulageModel']=='4')
				   			{
				   				$szName="Up to ".number_format((float)$haulagDataArray['iDistanceUpToKm'])." km";
				   			}
				   			else
				   			{
				   				$szName=$haulagDataArray['szName'];
				   			}
				   			$haulageModelName='';
				   			if($haulagDataArray['idHaulageModel']=='4')
				   			{
				   				$haulageModelName="Distance";			   			
				   			}
				   			else if($haulagDataArray['idHaulageModel']=='3')
				   			{
				   				$haulageModelName="Postcode";
				   			}
				   			else if($haulagDataArray['idHaulageModel']=='2')
				   			{
				   				$haulageModelName="City";
				   			}
				   			else if($haulagDataArray['idHaulageModel']=='1')
				   			{
				   				$haulageModelName="Zone";
				   			}
				   			/*if($haulagDataArray['dtExpiry']!='0000-00-00 00:00:00' && $haulagDataArray['dtExpiry']!="")
				   			{
							
								$expirytime_arr=explode('-',$haulagDataArray['dtExpiry']);
								$expirydate= gmmktime(0,0,0,$expirytime_arr[1],$expirytime_arr[2],$expirytime_arr[0]);
					   			
				   			}
				   			
				   			$iCrossBorder='';
				   			if((int)$haulagDataArray['iCrossBorder']=='1')
				   			{
				   				$iCrossBorder="Yes";
				   			}
				   			else if($haulagDataArray['iCrossBorder']=='0')
				   			{
				   				$iCrossBorder="No";
				   			}*/
				   			
				   			$idHaulageTransitTime='';
				   			if(!empty($haulagDataArray['iHours']))
				   			{
								if($haulagDataArray['iHours']>24)
					   			{
					   				$idHaulageTransitTime="< ".ceil($haulagDataArray['iHours']/24)." days";
					   			}
					   			else
					   			{
					   				$idHaulageTransitTime="< ".$haulagDataArray['iHours']." hours";
					   			}
				   			}
				   			
				   			$idDirection='';
				   			if($haulagDataArray['iDirection']==1)
				   			{
				   				$idDirection='Export';
				   			}
				   			else if($haulagDataArray['iDirection']==2)
				   			{
				   				$idDirection='Import';
				   			}
				   			
				   			$fWmFactor='';
				   			if((int)$haulagDataArray['fWmFactor']>0)
				   			{
				   				$fWmFactor=$haulagDataArray['fWmFactor']." kg/cbm";
				   			}
				   			
							$fFuelPercentage='';
				   			if((int)$haulagDataArray['fFuelPercentage']>0)
				   			{
				   				$fFuelPercentage=$haulagDataArray['fFuelPercentage']." %";
				   			}
				   			else
				   			{
				   				$fFuelPercentage="0.00 %";
				   			}
				   			
				   			//$objPHPExcel->getActiveSheet()->setCellValue($colcounter,$rowcounter)->getProtection(true)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);							
                                                        $objPHPExcel->getActiveSheet()->setCellValue('A'.$rowcounter,$szCountry)->getStyle('A'.$rowcounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                                                        $objPHPExcel->getActiveSheet()->getStyle('A'.$rowcounter)->applyFromArray($styleArray);
							
							$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowcounter,$haulagDataArray['szCity'])->getStyle('B'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
							$objPHPExcel->getActiveSheet()->getStyle('B'.$rowcounter)->applyFromArray($styleArray);
							
							$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowcounter,$haulagDataArray['szWareHouseName'])->getStyle('C'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
							$objPHPExcel->getActiveSheet()->getStyle('C'.$rowcounter)->applyFromArray($styleArray);
							
							$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowcounter,$idDirection)->getStyle('D'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
							$objPHPExcel->getActiveSheet()->getStyle('D'.$rowcounter)->applyFromArray($styleArray);
							
							$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowcounter,$haulageModelName)->getStyle('E'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
							$objPHPExcel->getActiveSheet()->getStyle('E'.$rowcounter)->applyFromArray($styleArray);
							
							$objPHPExcel->getActiveSheet()->getStyle('F'.$rowcounter)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
							$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowcounter,$szName)->getStyle('F'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
							$objPHPExcel->getActiveSheet()->getStyle('F'.$rowcounter)->applyFromArray($styleArray);
							
							$objPHPExcel->getActiveSheet()->setCellValue('G'.$rowcounter,$haulagDataArray['iUpToKg'])->getStyle('G'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
							$objPHPExcel->getActiveSheet()->getStyle('G'.$rowcounter)->applyFromArray($styleArray1);
							$objPHPExcel->getActiveSheet()->setCellValue('H'.$rowcounter,$haulagDataArray['fPricePer100Kg'])->getStyle('H'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
							$objPHPExcel->getActiveSheet()->getStyle('H'.$rowcounter)->applyFromArray($styleArray1);
							$objPHPExcel->getActiveSheet()->setCellValue('I'.$rowcounter,$haulagDataArray['fMinimumPrice'])->getStyle('I'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
							$objPHPExcel->getActiveSheet()->getStyle('I'.$rowcounter)->applyFromArray($styleArray1);
							$objPHPExcel->getActiveSheet()->setCellValue('J'.$rowcounter,$haulagDataArray['fPricePerBooking'])->getStyle('J'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
							$objPHPExcel->getActiveSheet()->getStyle('J'.$rowcounter)->applyFromArray($styleArray1);
							$objPHPExcel->getActiveSheet()->setCellValue('K'.$rowcounter,$currency)->getStyle('K'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
							$objPHPExcel->getActiveSheet()->getStyle('K'.$rowcounter)->applyFromArray($styleArray);
							
							$objPHPExcel->getActiveSheet()->setCellValue('L'.$rowcounter,$fFuelPercentage)->getStyle('L'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
							$objPHPExcel->getActiveSheet()->getStyle('L'.$rowcounter)->applyFromArray($styleArray);
							
							$objPHPExcel->getActiveSheet()->setCellValue('M'.$rowcounter,$fWmFactor)->getStyle('M'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
							$objPHPExcel->getActiveSheet()->getStyle('M'.$rowcounter)->applyFromArray($styleArray);
							
							$objPHPExcel->getActiveSheet()->setCellValue('N'.$rowcounter,$idHaulageTransitTime)->getStyle('N'.$rowcounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
							$objPHPExcel->getActiveSheet()->getStyle('N'.$rowcounter)->applyFromArray($styleArray);

							//$col_m_xfd='M'.$rowcounter.':AZ'.$rowcounter;
							//$objPHPExcel->getActiveSheet()->getStyle($col_m_xfd)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
							
							$val_g="G".$rowcounter;
							$val_h="H".$rowcounter;
							$val_i="I".$rowcounter;
							$val_j="J".$rowcounter;
							if($rowcounter=='3')
							{
								$objValidationG = $objPHPExcel->getActiveSheet()->getCell($val_g)->getDataValidation();
								$objValidationG->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL );
								$objValidationG->setOperator( PHPExcel_Cell_DataValidation::OPERATOR_GREATERTHAN);		
								$objValidationG->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
								$objValidationG->setAllowBlank(true);
							    $objValidationG->setShowInputMessage(true);
							    $objValidationG->setShowErrorMessage(true);
							    $objValidationG->setShowDropDown(true);
							    $objValidationG->setErrorTitle('Weight up to');
								$objValidationG->setError('Please type in the maximum weight in kg this rate is applicable for. This would be a whole number larger than zero.');
								$objValidationG->setFormula1(0);
								
							
								$objValidationH = $objPHPExcel->getActiveSheet()->getCell($val_h)->getDataValidation();
								$objValidationH->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL );
								$objValidationH->setOperator( PHPExcel_Cell_DataValidation::OPERATOR_GREATERTHANOREQUAL);		
								$objValidationH->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
								$objValidationH->setAllowBlank(true);
							    $objValidationH->setShowInputMessage(true);
							    $objValidationH->setShowErrorMessage(true);
							    $objValidationH->setShowDropDown(true);
							    $objValidationH->setErrorTitle('Haulage rate per 100 kg');
								$objValidationH->setError('Please fill in haulage rate per 100 kg in selected currency. This should be zero or a positive decimal number.');
								$objValidationH->setFormula1(0);
							
							
								$objValidationI = $objPHPExcel->getActiveSheet()->getCell($val_i)->getDataValidation();
								$objValidationI->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL );
								$objValidationI->setOperator( PHPExcel_Cell_DataValidation::OPERATOR_GREATERTHANOREQUAL);		
								$objValidationI->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
								$objValidationI->setAllowBlank(true);
							    $objValidationI->setShowInputMessage(true);
							    $objValidationI->setShowErrorMessage(true);
							    $objValidationI->setShowDropDown(true);
							    $objValidationI->setErrorTitle('Minimum freight charged');
								$objValidationI->setError('Please fill in minimum freight charged in the selected currency. This should be zero or a positive decimal number.');
								$objValidationI->setFormula1(0);
							
							
							
								$objValidationJ = $objPHPExcel->getActiveSheet()->getCell($val_j)->getDataValidation();
								$objValidationJ->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL );
								$objValidationJ->setOperator( PHPExcel_Cell_DataValidation::OPERATOR_GREATERTHANOREQUAL);		
								$objValidationJ->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
								$objValidationJ->setAllowBlank(true);
							    $objValidationJ->setShowInputMessage(true);
							    $objValidationJ->setShowErrorMessage(true);
							    $objValidationJ->setShowDropDown(true);
							    $objValidationJ->setErrorTitle('Per Booking');
								$objValidationJ->setError('Please fill in any fixed rate charged per booking in addition to the haulage rate per 100 kg and minimum. This should be zero or a positive decimal number.');
								$objValidationJ->setFormula1(0);
							
								/*
								$val_l="L".$rowcounter;
								
								
								$objValidation = $objPHPExcel->getActiveSheet()->getCell($val_l)->getDataValidation();
								$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_DATE );
								$objValidation->setOperator( PHPExcel_Cell_DataValidation::OPERATOR_GREATERTHAN);
								$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
								$objValidation->setAllowBlank(true);
							    $objValidation->setShowInputMessage(true);
							    $objValidation->setShowErrorMessage(true);
							    $objValidation->setShowDropDown(true);
							    $objValidation->setErrorTitle('Expiry date');
								$objValidation->setError('Please fill in the date when this rate will expire.');
								$objValidation->setFormula1(40908);
								*/
								//$objPHPExcel->getActiveSheet()->getCell($val_m)->setDataValidation(clone $objValidation);
								
								$currencyArr=$this->getFreightCurrency();
								$countCurrency=count($currencyArr);
								$val_k="K".$rowcounter;
								$val_currency='List!$A$1:$A$'.$countCurrency;
								$objValidationK = $objPHPExcel->getActiveSheet()->getCell($val_k)->getDataValidation();
							    $objValidationK->setFormula1($val_currency);
							    $objValidationK->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
							    $objValidationK->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
							    $objValidationK->setAllowBlank(true);
							    $objValidationK->setShowInputMessage(true);
							    $objValidationK->setShowErrorMessage(true);
							    $objValidationK->setShowDropDown(true);
							    $objValidationK->setErrorTitle('Currency');
								$objValidationK->setError('Please select one of the currencies available.');
								
								
								$countName=count(array_unique($haulagDataCountCityNameArr['name']));
								$val_c="C".$rowcounter;
								$val_name='List!$D$1:$D$'.$countName;
								$objValidationC = $objPHPExcel->getActiveSheet()->getCell($val_c)->getDataValidation();
							    $objValidationC->setFormula1($val_name);
							    $objValidationC->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
							    $objValidationC->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
							    $objValidationC->setAllowBlank(true);
							    $objValidationC->setShowInputMessage(true);
							    $objValidationC->setShowErrorMessage(true);
							    $objValidationC->setShowDropDown(true);
							    $objValidationC->setErrorTitle('CFS name');
								$objValidationC->setError('Please type a valid CFS name. The list shows all the CFSs you have chosen to download to this file.');
								
								
								$countCity=count(array_unique($haulagDataCountCityNameArr['city']));
								$val_b="B".$rowcounter;
								$val_city='List!$C$1:$C$'.$countCity;
								$objValidationB = $objPHPExcel->getActiveSheet()->getCell($val_b)->getDataValidation();
							    $objValidationB->setFormula1($val_city);
							    $objValidationB->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
							    $objValidationB->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
							    $objValidationB->setAllowBlank(true);
							    $objValidationB->setShowInputMessage(true);
							    $objValidationB->setShowErrorMessage(true);
							    $objValidationB->setShowDropDown(true);
							    $objValidationB->setErrorTitle('CFS city');
								$objValidationB->setError('Please type in a valid city name. The list shows all the cities of the CFSs you have chosen to download to this file.');
								
								
								$countCountry=count(array_unique($haulagDataCountCityNameArr['country']));
								$val_a="A".$rowcounter;
								$val_country='List!$B$1:$B$'.$countCountry;
								$objValidationA = $objPHPExcel->getActiveSheet()->getCell($val_a)->getDataValidation();
							    $objValidationA->setFormula1($val_country);
							    $objValidationA->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
							    $objValidationA->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
							    $objValidationA->setAllowBlank(true);
							    $objValidationA->setShowInputMessage(true);
							    $objValidationA->setShowErrorMessage(true);
							    $objValidationA->setShowDropDown(true);
							    $objValidationA->setErrorTitle('CFS country');
								$objValidationA->setError('Please type a valid country name. The list shows all the countries of the CFSs you have chosen to download to this file.');
								
								
								$countszNameModel=count(array_unique($haulagDataCountCityNameArr['szNameModel']));
								$val_f="F".$rowcounter;
								$val_szNameModel='List!$G$1:$G$'.$countszNameModel;
								$objValidationF = $objPHPExcel->getActiveSheet()->getCell($val_f)->getDataValidation();
							    $objValidationF->setFormula1($val_szNameModel);
							    $objValidationF->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
							    $objValidationF->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
							    $objValidationF->setAllowBlank(true);
							    $objValidationF->setShowInputMessage(true);
							    $objValidationF->setShowErrorMessage(true);
							    $objValidationF->setShowDropDown(true);
							    $objValidationF->setErrorTitle('Name or reference');
								$objValidationF->setError('For Transporteca to recognise this line, you must select a relevant name or reference. It is recommended to copy an entire row to ensure that all selections are accurate.');
								
								$fColumnHaulageDataArr=array('City','Distance','Postcode','Zone');
								$countModel=count(array_unique($fColumnHaulageDataArr));
								$val_e="E".$rowcounter;
								$val_szModel='List!$F$1:$F$'.$countModel;
								$objValidationE = $objPHPExcel->getActiveSheet()->getCell($val_e)->getDataValidation();
							    $objValidationE->setFormula1($val_szModel);
							    $objValidationE->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
							    $objValidationE->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
							    $objValidationE->setAllowBlank(true);
							    $objValidationE->setShowInputMessage(true);
							    $objValidationE->setShowErrorMessage(true);
							    $objValidationE->setShowDropDown(true);
							    $objValidationE->setErrorTitle('Pricing model');
								$objValidationE->setError('For Transporteca to recognise this line, you must select a relevant pricing model. It is recommended to copy an entire row to ensure that all selections are accurate.');
								
								$dColumnHaulageDataArr=array('Export','Import');
								$countName=count($dColumnHaulageDataArr);
								$val_d="D".$rowcounter;
								$vald_name='List!$E$1:$E$'.$countName;
								$objValidationD = $objPHPExcel->getActiveSheet()->getCell($val_d)->getDataValidation();
							    $objValidationD->setFormula1($vald_name);
							    $objValidationD->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
							    $objValidationD->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
							    $objValidationD->setAllowBlank(true);
							    $objValidationD->setShowInputMessage(true);
							    $objValidationD->setShowErrorMessage(true);
							    $objValidationD->setShowDropDown(true);
							    $objValidationD->setErrorTitle('Haulage direction');
								$objValidationD->setError('For Transporteca to recognise this line, you must select a relevant haulage direction. It is recommended to copy an entire row to ensure that all selections are accurate.');
								
							/*	$countfWmFactor=count(array_unique($haulagDataCountCityNameArr['fWmFactor']));
								$val_m="M".$rowcounter;
								$val_fWmFactor='List!$H$1:$H$'.$countfWmFactor;
								$objValidationM = $objPHPExcel->getActiveSheet()->getCell($val_m)->getDataValidation();
							    $objValidationM->setFormula1($val_fWmFactor);
							    $objValidationM->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
							    $objValidationM->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
							    $objValidationM->setAllowBlank(true);
							    $objValidationM->setShowInputMessage(true);
							    $objValidationM->setShowErrorMessage(true);
							    $objValidationM->setShowDropDown(true);
							    $objValidationM->setErrorTitle('W/M factor');
								$objValidationM->setError('For Transporteca to recognise this line, you must select a relevant W/M factor. It is recommended to copy an entire row to ensure that all selections are accurate.');
								
								$gColumnDataArr=gColumnData();
								$countTransitTime=count(array_unique($gColumnDataArr));
								$val_n="N".$rowcounter;
								$val_iTransitTime='List!$I$1:$I$'.$countTransitTime;
								$objValidationN = $objPHPExcel->getActiveSheet()->getCell($val_n)->getDataValidation();
							    $objValidationN->setFormula1($val_iTransitTime);
							    $objValidationN->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
							    $objValidationN->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
							    $objValidationN->setAllowBlank(true);
							    $objValidationN->setShowInputMessage(true);
							    $objValidationN->setShowErrorMessage(true);
							    $objValidationN->setShowDropDown(true);
							    $objValidationN->setErrorTitle('Transit Time');
								$objValidationN->setError('For Transporteca to recognise this line, you must select a relevant transit time. It is recommended to copy an entire row to ensure that all selections are accurate.');*/
								
							}
						}
					}
				}
			}
			
				if($rowcounter>3)
			   	{		
			   	
			   	  $objPHPExcel->getActiveSheet()->setDataValidation('A4:A'.$rowcounter, $objValidationA);
			   	  $objPHPExcel->getActiveSheet()->setDataValidation('B4:B'.$rowcounter, $objValidationB);
			   	  $objPHPExcel->getActiveSheet()->setDataValidation('C4:C'.$rowcounter, $objValidationC);
			   	  $objPHPExcel->getActiveSheet()->setDataValidation('E4:E'.$rowcounter, $objValidationE);
			      $objPHPExcel->getActiveSheet()->setDataValidation('F4:F'.$rowcounter, $objValidationF);	
			   	  $objPHPExcel->getActiveSheet()->setDataValidation('G4:G'.$rowcounter, $objValidationG);
				  $objPHPExcel->getActiveSheet()->setDataValidation('H4:H'.$rowcounter, $objValidationH);
				  $objPHPExcel->getActiveSheet()->setDataValidation('I4:I'.$rowcounter, $objValidationI);
				  $objPHPExcel->getActiveSheet()->setDataValidation('J4:J'.$rowcounter, $objValidationJ);
				  $objPHPExcel->getActiveSheet()->setDataValidation('K4:K'.$rowcounter, $objValidationK);
				  $objPHPExcel->getActiveSheet()->setDataValidation('D4:D'.$rowcounter, $objValidationD);
				//  $objPHPExcel->getActiveSheet()->setDataValidation('M4:M'.$rowcounter, $objValidationM);
				  //$objPHPExcel->getActiveSheet()->setDataValidation('N4:N'.$rowcounter, $objValidationN);
				  
			   	}
				  
			$rowcounter=$rowcounter+1;
			$col_row='A'.$rowcounter.":L".$rowcounter;
			$objPHPExcel->getActiveSheet()->getStyle($col_row)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);			
						
			$filter='A2:N'.$rowcounter;
			   	$objPHPExcel->getActiveSheet()->setAutoFilter($filter);								
											
			
			$objPHPExcel->getActiveSheet()->freezePane('G3');
			   	$objPHPExcel->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(3, 3);
			//$objPHPExcel->getActiveSheet()->setHidden(true);
			$objPHPExcel->getActiveSheet()->setTitle('Sheet1');
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objPHPExcel->setActiveSheetIndex(0);
			
			$objWriter->save($fileName);
			return true;
			
	}
	
	
	function exportListHaulageData($objPHPExcel,$sheetindex,$fileName,$haulagDataCountCityNameArr)
	{
			$sheetindex=1;
			if($sheetindex>0)
			{
				$objPHPExcel->createSheet();
			}
			//Worksheets("List").Visible(False); 
			$objPHPExcel->setActiveSheetIndex($sheetindex);
			
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			
			$currencyArr=$this->getFreightCurrency();
			
			if(!empty($currencyArr))
			{
				$c=1;
				foreach($currencyArr as $currencyArrs)
				{
					$val_a='A'.$c;
					$objPHPExcel->getActiveSheet()->setCellValue($val_a,$currencyArrs);
					++$c;
				}
			}
			$newCountry=array();
			if(!empty($haulagDataCountCityNameArr['country']))
			{
				$c=1;
				foreach($haulagDataCountCityNameArr['country'] as $country)
				{
					if(!in_array($country,$newCountry))
					{
						$val_b='B'.$c;
						$objPHPExcel->getActiveSheet()->setCellValue($val_b,$country);
						$newCountry[]=$country;
						++$c;
					}
				}
				
			}
			
			if(!empty($haulagDataCountCityNameArr['city']))
			{
				$c=1;
				$newCity=array();
				foreach($haulagDataCountCityNameArr['city'] as $city)
				{
					if(!in_array($city,$newCity))
					{
						$val_c='C'.$c;
						$objPHPExcel->getActiveSheet()->setCellValue($val_c,$city);
						$newCity[]=$city;
						++$c;
					}
				}
			}
			
			$cfsNameArr=array();
			if(!empty($haulagDataCountCityNameArr['name']))
			{
				$c=1;
				foreach($haulagDataCountCityNameArr['name'] as $name)
				{
					if(!in_array($name,$cfsNameArr))
					{
						$val_d='D'.$c;	
						$objPHPExcel->getActiveSheet()->setCellValue($val_d,$name);
						$cfsNameArr[]=$name;
						++$c;
					}
				}
			}
			
			
			$eColumnHaulageDataArr=eColumnHaulageData();
			if(!empty($eColumnHaulageDataArr))
			{
				$c=1;
				foreach($eColumnHaulageDataArr as $eColumnHaulageDataArrs)
				{
					$val_e='E'.$c;
					$objPHPExcel->getActiveSheet()->setCellValue($val_e,$eColumnHaulageDataArrs);
					++$c;
				}
			}
			
			//$fColumnHaulageDataArr=fColumnHaulageData();
			$fColumnHaulageDataArr=array('City','Distance','Postcode','Zone');
			if(!empty($fColumnHaulageDataArr))
			{
				$c=1;
				foreach($fColumnHaulageDataArr as $fColumnHaulageDataArrs)
				{
					$val_f='F'.$c;
					$objPHPExcel->getActiveSheet()->setCellValue($val_f,$fColumnHaulageDataArrs);
					++$c;
				}
			}
			
			
			if(!empty($haulagDataCountCityNameArr['szNameModel']))
			{
				$c=1;
				foreach($haulagDataCountCityNameArr['szNameModel'] as $szNameModel)
				{
					$val_g='G'.$c;
					$objPHPExcel->getActiveSheet()->getStyle($val_g)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
					$objPHPExcel->getActiveSheet()->setCellValue($val_g,$szNameModel);
					++$c;
				}
			}
			/*$newfWmFactor=array();
			if(!empty($haulagDataCountCityNameArr['fWmFactor']))
			{
				$c=1;
				foreach($haulagDataCountCityNameArr['fWmFactor'] as $fWmFactor)
				{
				
					if(!in_array($fWmFactor,$newfWmFactor))
					{
						$val_d='H'.$c;
						$new_fWmFactor=$fWmFactor." kg/cbm";
						$objPHPExcel->getActiveSheet()->setCellValue($val_d,$new_fWmFactor);
						++$c;
						$newfWmFactor[]=$fWmFactor;
					}
				}
			}
			
			$gColumnData=gColumnData();
					
			if(!empty($gColumnData))
			{
				$g=1;
				for($i=0;$i<count($gColumnData);++$i)
				{
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$g,$gColumnData[$i]);
					++$g;
				}
			}*/
			
			
			$objPHPExcel->getActiveSheet()->setSheetState(PHPExcel_Worksheet::SHEETSTATE_HIDDEN);
			
			//$objPHPExcel->getActiveSheet()->setHidden(true);
			$objPHPExcel->getActiveSheet()->setTitle('List');
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objPHPExcel->setActiveSheetIndex(0);
			//$ext = end(explode('.', $fileName));
			//$fileName=__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/Transporteca-LCL-Services-upload-sheet_1.xlsx";
			$objWriter->save($fileName);
			return true;
			
	}
	
	function gColumnHaulageData($flag=false)
	{
		$query="
			SELECT
				*
			FROM
				".__DBC_SCHEMATA_HAULAGE_TRANSIT_TIME__."				
		";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				while($row=$this->getAssoc($result))
				{
					if($flag)
					{
						$gColHaulageArr[]=$row;
					}
					else
					{
						$iHours='';
						if($row['iHours']>24)
						{
							$iHours="< ".($row['iHours']/24)." days";
						}
						else
						{
							$iHours="< ".$row['iHours']." hours";
						}
						$gColHaulageArr[]=$iHours;
					}
				}
				return $gColHaulageArr;
			}
		}
	}
	
	function importHaulageServicesData($objPHPExcel,$sheetindex,$colcount,$rowcount,$idForwarder)
	{
		date_default_timezone_set('UTC');
		$ctr=0;
		if($rowcount>2)
		{
			for($i=3;$i<=$rowcount;$i++)
			{
				for($j=0;$j<$colcount;$j++)
				{
					if($j<=14)
					{
						if($objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0,$i)->getValue()!="")
						{
							$HaulageServiceData[$ctr][] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($j,$i)->getValue();
							
						}
					}
					
				}
				++$ctr;
			}
		}	
		
		//print_r($HaulageServiceData);
		//die;
		if(!empty($HaulageServiceData))
		{
			$k=3;
			$l_err=0;
			$l_success=0;
			$d=0;
			$deleteWarehouseCombinations=array();
			foreach($HaulageServiceData as $HaulageServiceDatas)
			{
				$error_flag=false;
				$err_msg=array();
				$checkErrorFlag=false;
				if($HaulageServiceDatas[0]=='')
				{
					$error_flag=true;
					$err_msg['country']=t($this->t_base.'messages/country_is_required');
				}				
				
				if($HaulageServiceDatas[1]=='')
				{
					$error_flag=true;
					$err_msg['city']=t($this->t_base.'messages/city_is_required');
				}
				
				if($HaulageServiceDatas[2]=='')
				{
					$error_flag=true;
					$err_msg['name']=t($this->t_base.'messages/name_is_required');
				}
				
				if($HaulageServiceDatas[3]=='')
				{
					$error_flag=true;
					$err_msg['direction']=t($this->t_base.'messages/direction_is_required');
				}
				else
				{
					$eColumnHaulageDataArr=eColumnHaulageData();
					if(!in_array($HaulageServiceDatas[3],$eColumnHaulageDataArr))
					{
						$error_flag=true;
						$err_msg['direction']=t($this->t_base.'messages/direction_invalid_type');
					}
				}
				
				if($HaulageServiceDatas[4]=='')
				{
					$error_flag=true;
					$err_msg['zoneModel']=t($this->t_base.'messages/pricing_model_is_required');
				}
				else
				{
					$columnHaulageDataArr=array('City','Distance','Postcode','Zone');
					if(!in_array($HaulageServiceDatas[4],$columnHaulageDataArr))
					{
						$error_flag=true;
						$err_msg['zoneModel']=t($this->t_base.'messages/invalid_pricing_name');
					}
				}
				
				
				if($HaulageServiceDatas[5]=='')
				{
					$error_flag=true;
					$err_msg['zoneModel']=t($this->t_base.'messages/name_or_reference_is_required');
				}
				
				
				if($HaulageServiceDatas[2]!="" && $HaulageServiceDatas[1]!="" && $HaulageServiceDatas[0]!="" && $HaulageServiceDatas[3]!="" && $HaulageServiceDatas[4]!='' && $HaulageServiceDatas[5]!='')
				{
					$haulage_Country=sanitize_all_html_input($this->getCountryId($HaulageServiceDatas[0]));
					$haulage_City=sanitize_all_html_input($HaulageServiceDatas[1]);
					$haulage_CFS=sanitize_all_html_input($HaulageServiceDatas[2]);
					
					$idPricingModel=$this->getPricingModelId(sanitize_all_html_input($HaulageServiceDatas[4]));
				
					$query="
                                            SELECT
                                                id
                                            FROM
                                                ".__DBC_SCHEMATA_WAREHOUSES__."
                                            WHERE
                                                szWareHouseName='".mysql_escape_custom($haulage_CFS)."' 	
                                            AND
                                                szCity='".mysql_escape_custom($haulage_City)."'
                                            AND
                                                idCountry='".(int)$haulage_Country."'
                                            AND
                                                idForwarder='".(int)$idForwarder."' 
                                            AND
                                                iActive = '1'
					";
					//echo $query."<br/>";
					if($result=$this->exeSQL($query))
					{
						if($this->iNumRows>0)
						{
							$row=$this->getAssoc($result);
							$WareHouse_Id=$row['id'];
						}
						else
						{
							$WareHouse_Id=0;
						}
					}
						
					if((int)$WareHouse_Id>0 && $idPricingModel>0)
					{
						$haulage_Direction='';
						if($HaulageServiceDatas[3]=='Export')
						{
							$haulage_Direction='1';
						}
						else if($HaulageServiceDatas[3]=='Import')
						{
							$haulage_Direction='2';
						}
						$dataArr=array();
						$dataArr[0]['idWarehouse']=$WareHouse_Id;
						$dataArr[0]['idPricingModel']=$idPricingModel;
						
						$haulagePricingModelName=sanitize_all_html_input($HaulageServiceDatas[5]);
						
						if($idPricingModel=='4')
						{
                                                    $replaceArr=array("Up to","km",",");
                                                    //echo $haulagePricingModelName;
                                                    $haulagePricingModelName=trim(str_replace($replaceArr,"",$haulagePricingModelName));
                                                    //echo $haulagePricingModelName."<br/>";
						}
						$idHaulaePricing=$this->getHaulagePricingModelId($haulagePricingModelName,$idPricingModel,$WareHouse_Id,$haulage_Direction);
						//echo $count;	
						if($idHaulaePricing>0)
						{
							$count=$this->getAllUpdatedHaulageServices($dataArr,$idHaulaePricing);	
							if((int)$count>0)
							{
								//echo "hello1";	
								if($HaulageServiceDatas[6]=='' && $HaulageServiceDatas[7]=='' && $HaulageServiceDatas[8]=='' && $HaulageServiceDatas[9]=='' && $HaulageServiceDatas[10]=='')
								{
									$checkErrorFlag=false;
									$deleteWarehouseCombination[$d]['idHaulaePricing']=$idHaulaePricing;
									$error_flag=true;
									++$d;
								}
								else
								{
									if($HaulageServiceDatas[6]!='')
									{
										$checkErrorFlag=true;
									}
									
									if($HaulageServiceDatas[7]!='')
									{
										$checkErrorFlag=true;
									}
									
									if($HaulageServiceDatas[8]!='')
									{
										$checkErrorFlag=true;
									}
									
									if($HaulageServiceDatas[9]!='')
									{
										$checkErrorFlag=true;
									}
									
									if($HaulageServiceDatas[10]!='')
									{
										$checkErrorFlag=true;
									}
								}
							}
							else
							{
								//echo "hello2";	
								if($HaulageServiceDatas[6]==='' && $HaulageServiceDatas[7]=='' && $HaulageServiceDatas[8]=='' && $HaulageServiceDatas[9]=='' && $HaulageServiceDatas[10]=='')
								{
									//echo "hello3";	
									$error_flag=true;
								}
								else
								{
									//echo "hello4";
									//echo $HaulageServiceDatas[6]."dd";
									if($HaulageServiceDatas[6]!=='')
									{
										$checkErrorFlag=true;
									}
									
									if($HaulageServiceDatas[7]!='')
									{
										$checkErrorFlag=true;
									}
									
									if($HaulageServiceDatas[8]!='')
									{
										$checkErrorFlag=true;
									}
									
									if($HaulageServiceDatas[9]!='')
									{
										$checkErrorFlag=true;
									}
									
									if($HaulageServiceDatas[10]!='')
									{
										$checkErrorFlag=true;
									}
								}
							}
						}
						else
						{
							$error_flag=true;
							$err_msg_model['model_name']=t($this->t_base.'messages/name_or_reference_is_invalid');
							$err_msg['model_name']=t($this->t_base.'messages/name_or_reference_is_invalid');
							//print_r($err_msg);
						}
					}
				}
				
				if($checkErrorFlag)
				{	
					//echo "helloasdf";
					if((float)$HaulageServiceDatas[6]=="")
					{
						$error_flag=true;
						$err_msg['weight_up_to']=t($this->t_base.'messages/weight_up_to');
					}
					else if((float)$HaulageServiceDatas[6]<=0)
					{
						$error_flag=true;
						$err_msg['weight_up_to']=t($this->t_base.'messages/weight_up_to_valid');
					}
					
					if($HaulageServiceDatas[7]==="")
					{
						$error_flag=true;
						$err_msg['per_100_kg']=t($this->t_base.'messages/per_100_kg');
					}
					else if((float)$HaulageServiceDatas[7]<0)
					{
						$error_flag=true;
						$err_msg['per_100_kg']=t($this->t_base.'messages/per_100_kg_valid');
					}
				
					if($HaulageServiceDatas[8]==="")
					{
					
						$error_flag=true;
						$err_msg['minimum']=t($this->t_base.'messages/minimum_rate_is_required');
					}
					else if((float)$HaulageServiceDatas[8]<0)
					{
						$error_flag=true;
						$err_msg['minimum']=t($this->t_base.'messages/mini_rate_should_positive_value');
					}
									
					
					if($HaulageServiceDatas[9]==="")
					{
						$error_flag=true;
						$err_msg['perBooking']=t($this->t_base.'messages/per_booking_rate_is_required');
					}
					else if((float)$HaulageServiceDatas[9]<0)
					{
						$error_flag=true;
						$err_msg['perBooking']=t($this->t_base.'messages/per_booking_rate_should_be_positive_value');
					}
					
					if($HaulageServiceDatas[10]=="")
					{
					
						$error_flag=true;
						$err_msg['currency']=t($this->t_base.'messages/currency_is_required');
					}
					else 
					{
						$currencyArr=$this->getFreightCurrency();
						if(!empty($currencyArr))
						{
							if(!in_array($HaulageServiceDatas[10],$currencyArr))
							{
								$error_flag=true;
								$err_msg['currency']=t($this->t_base.'messages/invalid_currency_type');
							}
						}
					}
				}

				if(!$error_flag)
				{
					$newHaulageServiceDatas[$k]=$HaulageServiceDatas;
				}				
				if(!empty($err_msg))
				{
					$err_msg_arr=implode(", ",$err_msg);
					$errmsg_arr[]=t($this->t_base.'messages/following_error_in_row_number') ." ".$k." - ".$HaulageServiceDatas[0]."/".$HaulageServiceDatas[1]."/".$HaulageServiceDatas[2]."/".$HaulageServiceDatas[3]."/".$HaulageServiceDatas[4]."/".$HaulageServiceDatas[5]." : ".$err_msg_arr;
					++$l_err;
				}
				++$k;
			}
			
			/*print_r($newHaulageServiceDatas);
			echo "----------------<br/><br/>";
			print_r($deleteWarehouseCombination);
			echo "----------------<br/><br/>";
			print_r($err_msg);
			die;*/
			if(!empty($deleteWarehouseCombination))
			{
				$i=0;
				$l_del_success=0;
				$deleteFromWarehouse=array();
				$deleteDirectionWarehouse=array();
				foreach($deleteWarehouseCombination as $deleteWarehouseCombinations)
				{
					$query="
						UPDATE
							".__DBC_SCHEMATA_HAULAGE_PRICING__."
						SET
							iActive='0',
						 	dtUpdatedOn=NOW() 
						WHERE
					   		idHaulagePricingModel='".(int)$deleteWarehouseCombinations['idHaulaePricing']."'
					";		
					//echo $query."<br/>";
					$result=$this->exeSQL($query);
					++$i;
					++$l_del_success;
				}
			}
			//print_r($err_msg);
			//print_r($newHaulageServiceDatas);
			//die;
			if(!empty($newHaulageServiceDatas))
			{
				$allPricingModelIdArr=array();
				$allPricingModelCountArr=array();
				foreach($newHaulageServiceDatas as $keys=>$newHaulageServiceDataArr)
				{
					//print_r($newHaulageServiceDataArr);
					$error_flag=false;
					$err_msg=array();
					$szHaulageCountry = $newHaulageServiceDataArr[0] ;
					$haulageCountry=$this->getCountryId(sanitize_all_html_input($newHaulageServiceDataArr[0]));
					$haulageCity=sanitize_all_html_input($newHaulageServiceDataArr[1]);
					$haulageCFS=sanitize_all_html_input($newHaulageServiceDataArr[2]);
					
					$idPricingModel=$this->getPricingModelId(sanitize_all_html_input($newHaulageServiceDataArr[4]));
					
					$haulagePricingModelName=sanitize_all_html_input($newHaulageServiceDataArr[5]);
					
					
					if($newHaulageServiceDataArr[3]=='Export')
					{
						$haulageDirection='1';
					}
					else if($newHaulageServiceDataArr[3]=='Import')
					{
						$haulageDirection='2';
					}
					$haulageWeightUpto=0;
					$haulageWeightUpto=sanitize_all_html_input($newHaulageServiceDataArr[6]);
					
					$haulagePerWM=sanitize_all_html_input($newHaulageServiceDataArr[7]);
					
					$haulageMiniRate=sanitize_all_html_input($newHaulageServiceDataArr[8]);
					
					$haulagePerBookinRate=sanitize_all_html_input($newHaulageServiceDataArr[9]);
					
					$haulageCurrency=$this->getFreightCurrencyId(sanitize_all_html_input($newHaulageServiceDataArr[10]));
					
					
					$query="
						SELECT
                                                    id
						FROM
                                                    ".__DBC_SCHEMATA_WAREHOUSES__."
						WHERE
                                                    szWareHouseName='".mysql_escape_custom($haulageCFS)."' 	
						AND
                                                    szCity='".mysql_escape_custom($haulageCity)."'
						AND
                                                    idCountry='".(int)$haulageCountry."'
						AND
                                                    idForwarder='".(int)$idForwarder."' 
                                                AND
                                                    iActive = '1'
					";
					//echo $query."<br/><br/>";
					if($result=$this->exeSQL($query))
					{
						if($this->iNumRows<=0)
						{							
							$error_flag=true;
							$err_msg['warehouse']=t($this->t_base.'messages/warehouse_name_does_not_related_to_forwarder');
							
						}
						else
						{
							$row=$this->getAssoc($result);
							$WareHouseId=$row['id'];
						}
					}
					
					if($idPricingModel=='4')
					{
						$replaceArr=array("Up to","km",",");
						//echo $haulagePricingModelName;
						$haulagePricingModelName=trim(str_replace($replaceArr,"",$haulagePricingModelName));
						//echo $haulagePricingModelName."<br/>";
					}
					
					$idHaulaePricing=$this->getHaulagePricingModelId($haulagePricingModelName,$idPricingModel,$WareHouseId,$haulageDirection);
					
					if(!$error_flag)
					{
										
						$query="
							SELECT
								hp.id,
								hp.iUpToKg,
								hp.fPricePer100Kg,
								hp.fMinimumPrice,
								hp.fPricePerBooking,
								hp.idCurrency
							FROM
								".__DBC_SCHEMATA_HAULAGE_PRICING__." AS hp
							WHERE
								hp.idHaulagePricingModel='".(int)$idHaulaePricing."'
							AND
								hp.iUpToKg='".(int)$haulageWeightUpto."'
						";
						//echo $query."<br/><br/>";
						if($result=$this->exeSQL($query))
						{
							if($this->iNumRows<=0)
							{
								//echo "hello";
								if(!in_array($idHaulaePricing,$allPricingModelIdArr))
								{
								 	$allPricingModelIdArr[]=$idHaulaePricing;
								 	$totalcount=0;
								 	$query="
										UPDATE
											".__DBC_SCHEMATA_HAULAGE_PRICING__." AS hp
										SET
											iActive='0'
										WHERE
											hp.idHaulagePricingModel='".(int)$idHaulaePricing."'
										";
								 //echo $query."<br/><br/>";
										$result=$this->exeSQL($query);
								}
								$query="
									INSERT INTO
										".__DBC_SCHEMATA_HAULAGE_PRICING__."
									(	
										idHaulagePricingModel,
										iUpToKg,
										fPricePer100Kg,
										fMinimumPrice,
										fPricePerBooking,
										idCurrency,
										dtCreatedOn
									)
										VALUES
									(
										'".(int)$idHaulaePricing."',
										'".(int)$haulageWeightUpto."',
										'".(float)$haulagePerWM."',
										'".(float)$haulageMiniRate."',
										'".(float)$haulagePerBookinRate."',
										'".(int)$haulageCurrency."',
										NOW()
									)
								";
								//echo $query."<br/><br/>";
								if($result=$this->exeSQL($query))
								{
									++$l_success;
									$szHaulageCountriesAry[] = $szHaulageCountry ;
								}
							}
							else
							{
								$row=$this->getAssoc($result);
										
								$updateflag=false;
								
								if((int)$row['iUpToKg']!=$haulageWeightUpto)
								{
									
									$updateflag=true;							
								}
					
								if((float)$row['fPricePer100Kg']!=$haulagePerWM)
								{
									$updateflag=true;							
								}								
																	
								if((float)$row['fMinimumPrice']!=$haulageMiniRate)
								{
									$updateflag=true;							
								}
																		
								if($row['fPricePerBooking']!=$haulagePerBookinRate)
								{
									$updateflag=true;							
								}
																		
								if($row['idCurrency']!=$haulageCurrency)
								{
									$updateflag=true;							
								}
								
								if($updateflag)
								{
										if(!in_array($idHaulaePricing,$allPricingModelIdArr))
										{
										 	$allPricingModelIdArr[]=$idHaulaePricing;
										 	$totalcount=0;
										 	$query="
												UPDATE
													".__DBC_SCHEMATA_HAULAGE_PRICING__." AS hp
												SET
													iActive='0'
												WHERE
													hp.idHaulagePricingModel='".(int)$idHaulaePricing."'
												";
										 	//echo $query."<br/><br/>";
												$result=$this->exeSQL($query);
										}
										
										$query="
											INSERT INTO
												".__DBC_SCHEMATA_HAULAGE_PRICING_ARCHIVE__."
											(	
												idHaulagePricingModel,
												iUpToKg,
												fPricePer100Kg,
												fMinimumPrice,
												fPricePerBooking,
												idCurrency,
												dtUpdated,
												idWarehouse,
												idHaulageModel,
												idDirection
											)
												VALUES
											(
												'".(int)$idHaulaePricing."',
												'".(int)$row['iUpToKg']."',
												'".(float)$row['fPricePer100Kg']."',
												'".(float)$row['fMinimumPrice']."',
												'".(float)$row['fPricePerBooking']."',
												'".(int)$row['idCurrency']."',
												NOW(),
												'".(int)$WareHouseId."',
												'".(int)$idPricingModel."',
												'".(int)$haulageDirection."'
											)
										";
									//echo $query."<br/><br/>";
									//	$result=$this->exeSQL($query);
									
										$query="
											UPDATE
												".__DBC_SCHEMATA_HAULAGE_PRICING__."
											SET
												iUpToKg='".(int)$haulageWeightUpto."',
												fPricePer100Kg='".(float)$haulagePerWM."',
												fMinimumPrice='".(float)$haulageMiniRate."',
												fPricePerBooking='".(float)$haulagePerBookinRate."',
												idCurrency='".(int)$haulageCurrency."',
												iActive='1'
											WHERE
												idHaulagePricingModel='".(int)$idHaulaePricing."'
											AND
												iUpToKg='".(int)$row['iUpToKg']."'
										";
									//	echo $query."<br/><br/>";
											if($result=$this->exeSQL($query))
											{
												if($this->getRowCnt()>0)
												{
													++$l_success;
													$szHaulageCountriesAry[] = $szHaulageCountry ;
												}
											}
								}
								else
								{
									if(!in_array($idHaulaePricing,$allPricingModelIdArr))
									{
									 	$allPricingModelIdArr[]=$idHaulaePricing;
									 	$totalcount=0;
									 	$query="
											UPDATE
												".__DBC_SCHEMATA_HAULAGE_PRICING__." AS hp
											SET
												iActive='0'
											WHERE
												hp.idHaulagePricingModel='".(int)$idHaulaePricing."'
											";
									 	//echo $query."<br/><br/>";
											$result=$this->exeSQL($query);
									}
									
									$query="
										UPDATE
											".__DBC_SCHEMATA_HAULAGE_PRICING__."
										SET
											iActive='1'
									    WHERE
											idHaulagePricingModel='".(int)$idHaulaePricing."'
										AND
											iUpToKg='".(int)$haulageWeightUpto."'
										";
									//echo $query."<br/><br/>";										
									$result=$this->exeSQL($query);
								}
										
							}
						}
					}
					else
					{
						$err_msg_arr=implode(", ",$err_msg);
						$errmsg_arr[]=t($this->t_base.'messages/following_error_in_row_number')." ".$keys." - ".$newHaulageServiceDataArr[0]."/".$newHaulageServiceDataArr[1]."/".$newHaulageServiceDataArr[2]."/".$newHaulageServiceDataArr[3]."/".$newHaulageServiceDataArr[4]."/".$newHaulageServiceDataArr[5]." : ".$err_msg_arr;
						++$l_err;
					}
					
				}
			}
			
			if(!empty($szHaulageCountriesAry))
			{
				$szHaulageCountriesArs = array_unique($szHaulageCountriesAry);
			}
			if(!empty($szHaulageCountriesArs))
			{
				$szHaulageCountriesStr = implode(', ',$szHaulageCountriesArs);
			}
			
			
			if($allPricingModelCountArr)
			{
				foreach($allPricingModelCountArr as $key=>$values)
				{
					$idHaulaePricing=$key;
					$query="
						SELECT
							count(hp.id) as total
						FROM
							".__DBC_SCHEMATA_HAULAGE_PRICING__." AS hp
						WHERE
							hp.idHaulagePricingModel='".(int)$idHaulaePricing."'
						";
						if($result=$this->exeSQL($query))
						{
							if($this->iNumRows>0)
							{
								$row=$this->getAssoc($result);
								$totalcount	=$row['total'];
							}
						}
				}
			}
			
			
			
			$this->successLine=$l_success;
			$this->errorLine=$l_err;
			$this->deleteLine=$l_del_success;
			$updateLine=$this->deleteLine+$this->successLine;
			$err_str="";
                        if((int)$_SESSION['forwarder_admin_id']>0)
                        {
                            $idAdmin = (int)$_SESSION['forwarder_admin_id'];  
                            $kForwarderContact = new cAdmin();
                            $kForwarderContact->getAdminDetails($idAdmin);
                        }
                        else
                        { 
                            $idForwarderContact = (int)$_SESSION['forwarder_user_id'];
                            $kForwarderContact = new cForwarderContact();
                            $kForwarderContact->load($idForwarderContact);
                        } 
			$this->szEmail=$kForwarderContact->szEmail;
			$inc_error_msg='';
			if(!empty($err_msg_model['model_name']))
			{
				$inc_error_msg="If you are trying to upload a new pricing model for your CFS, please ensure that it is defined in the Haulage One by One update screen <a href='".__FORWARDER_OBO_HAULAGE_URL__."'>here</a> before uploading again.<br /><br />";
			}
			
			if($updateLine>1)
				$lines="lines";
			else
				$lines="line";	

			$SuccessCount="You have successfully updated ".$updateLine." Haulage ".$lines.".<br/><br/>";
			
			$kForwarder = new cForwarder();
			$idForwarder = $_SESSION['forwarder_id'];
			$kForwarder->load($idForwarder);
			
			if(($kForwarder->iAgreeTNC==1) && ($this->successLine>0) && ($kForwarder->iUpdateRss == 2))
			{
				/*
				$kConfig = new cConfig();
				$kWhsSearch = new cWHSSearch();
				
				$kWhsSearch->load($this->idOriginWarehouse);
				$szOriginCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
				
				$kWhsSearch->load($this->idDestinationWarehouse);
				$szDestinationCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
				*/
				
				$replace_ary = array();
				$replace_ary['szForwarderDisplayName'] = $kForwarder->szDisplayName ;
				$replace_ary['szCountryName'] = $szHaulageCountryName ;
				$replace_ary['szHaulageCountryList'] = $szHaulageCountriesStr ;
				$replace_ary['szSiteUrl'] = __MAIN_SITE_HOME_PAGE_SECURE_URL__ ;
				
				if($updateLine==1)
				{
					$replace_ary['szTradeCountText'] = "";
				}
				else 
				{
					$replace_ary['szTradeCountText'] = $updateLine.' additional countries, including ' ;
				}
				
				$content_ary = array();
				$content_ary = $this->replaceRssContent('__BULK_HAULAGE__',$replace_ary);
				
				$rssDataAry=array();
				$rssDataAry['idForwarder'] = $kForwarder->id ;
				$rssDataAry['szFeedType'] = '__BULK_HAULAGE__';
				$rssDataAry['szLink'] = __RSS_FEED_PAGE_URL__ ;
				$rssDataAry['szTitle'] = $content_ary['szTitle'];
				$rssDataAry['szDescription'] = $content_ary['szDescription'];
				$rssDataAry['szDescriptionHTML'] = $content_ary['szDescriptionHTML'];
				
				$kRss = new cRSS();
				$kRss->addRssFeed($rssDataAry);
			}
			
			//echo $this->szEmail;
			if(!empty($errmsg_arr))
			{
				$l=1;
				$err_str .="We found errors in following ".$this->errorLine." Haulage service lines in your upload file:<br/>";
				foreach($errmsg_arr as $errmsg_arrs)
				{
					$err_str .="<br/>".$l.". ".$errmsg_arrs."<br/>";
					++$l;
				}
				$err_str .="<br/>Please correct the errors and upload the file again.<br/><br/>";
			}
			//print_r($deleteWarehouseCombination);
			$replace_ary['SuccessCount']=$SuccessCount;
			//$replace_ary['ErrorCount']=$this->errorLine;
			$replace_ary['szErrorMsg']=$err_str;
			$replace_ary['szEmail']=$kForwarderContact->szEmail;
	        $replace_ary['szName'] = $kForwarderContact->szFirstName." ".$kForwarderContact->szLastName;
	        $replace_ary['szIncMsgError']=$inc_error_msg;
	        //print_r($replace_ary);
	       	createEmail(__FORWARDER_BULK_UPLOAD_HAULAGE_EMAIL__, $replace_ary,$this->szEmail, __FORWARDER_BULK_UPLOAD_HAULAGE_EMAIL_SUBJECT__, __STORE_SUPPORT_EMAIL__,$_SESSION['forwarder_user_id'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__);            
			return true;
		}
		else
		{
			return false;
		}					
	}
	
	
	function transitTimeId($time)
	{
		$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_HAULAGE_TRANSIT_TIME__."
				WHERE
					iHours='".(int)$time."'			
			";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					
					return $row['id'];
				}
			}
	}
	
	
	function exportDataCustomClearance($objPHPExcel,$sheetindex,$fileName,$kConfig,$desarr,$orginarr,$idForwarder,$idCurrency=0,$inActiveService=0,$downloadType)
	{
		date_default_timezone_set('UTC');	
                if($sheetindex>0)
                {
                    $objPHPExcel->createSheet();
                }
			
                $objPHPExcel->setActiveSheetIndex($sheetindex);

                $styleArray = array(
                        'font' => array(
                                'bold' => false,
                                'size' =>12,
                        ),
                        'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                        ),
                        'borders' => array(
                                'top' => array(
                                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                                ),
                                'bottom' => array(
                                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                                ),
                                'right' => array(
                                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                                ),
                                'left' => array(
                                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                                ),
                        ),
                );
			
                $styleArray1 = array(
                        'font' => array(
                                'bold' => false,
                                'size' =>12,
                        ),
                        'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                        ),
                        'borders' => array(
                                'top' => array(
                                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                                ),
                                'bottom' => array(
                                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                                ),
                                'right' => array(
                                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                                ),
                                'left' => array(
                                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                                ),
                        ),
                );
			
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);

                $objPHPExcel->getActiveSheet()->getStyle('A1:J2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('A1:J2')->getFill()->getStartColor()->setARGB('FFddd9c3');


                $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);

                $objPHPExcel->getActiveSheet()->mergeCells('A1:C1');
                $objPHPExcel->getActiveSheet()->setCellValue('A1','Origin')->getStyle('A1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->applyFromArray($styleArray);
        //	$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);

                $objPHPExcel->getActiveSheet()->mergeCells('D1:F1');
                $objPHPExcel->getActiveSheet()->setCellValue('D1','Destination')->getStyle('D1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                $objPHPExcel->getActiveSheet()->getStyle('D1:F1')->applyFromArray($styleArray);
                //$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);

                $objPHPExcel->getActiveSheet()->mergeCells('G1:H1');
                $objPHPExcel->getActiveSheet()->setCellValue('G1','Export Customs Clearance')->getStyle('G1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                $objPHPExcel->getActiveSheet()->getStyle('G1:H1')->applyFromArray($styleArray);
                //$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($styleArray);

                $objPHPExcel->getActiveSheet()->mergeCells('I1:J1');
                $objPHPExcel->getActiveSheet()->setCellValue('I1','Import Customs Clearance')->getStyle('K1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                $objPHPExcel->getActiveSheet()->getStyle('I1:J1')->applyFromArray($styleArray);


                $objPHPExcel->getActiveSheet()->setCellValue('A2','Country')->getStyle('A2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray);

                $objPHPExcel->getActiveSheet()->setCellValue('B2','City')->getStyle('B2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($styleArray);

                $objPHPExcel->getActiveSheet()->setCellValue('C2','Origin CFS/Airport Warehouse')->getStyle('B2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                $objPHPExcel->getActiveSheet()->getStyle('C2')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

                $objPHPExcel->getActiveSheet()->setCellValue('D2','Country')->getStyle('D2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                $objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($styleArray);

                $objPHPExcel->getActiveSheet()->setCellValue('E2','City')->getStyle('E2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                $objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray($styleArray);

                $objPHPExcel->getActiveSheet()->setCellValue('F2','Destination CFS/Airport Warehouse')->getStyle('F2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                $objPHPExcel->getActiveSheet()->getStyle('F2')->applyFromArray($styleArray);


                $objPHPExcel->getActiveSheet()->setCellValue('G2','Per File')->getStyle('G2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                $objPHPExcel->getActiveSheet()->getStyle('G2')->applyFromArray($styleArray);

                $objPHPExcel->getActiveSheet()->setCellValue('H2','Currency')->getStyle('H2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                $objPHPExcel->getActiveSheet()->getStyle('H2')->applyFromArray($styleArray);

                $objPHPExcel->getActiveSheet()->setCellValue('I2','Per File')->getStyle('I2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                $objPHPExcel->getActiveSheet()->getStyle('I2')->applyFromArray($styleArray);

                $objPHPExcel->getActiveSheet()->setCellValue('J2','Currency')->getStyle('J2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                $objPHPExcel->getActiveSheet()->getStyle('J2')->applyFromArray($styleArray);
			
                $sql="";
                if((int)$inActiveService>0)
                {
                    $sql .="
                        AND
                            iActive='0'
                        AND
                            szCurrency='".(int)$idCurrency."'
                    ";
                }
                else
                {
                    $sql .=" AND iActive='1' ";
                } 
                if(!empty($orginarr))
                {
						$i=0;
						foreach($orginarr as $orginarrs)
						{	
							if(!empty($desarr))
							{	
								
								foreach($desarr as $desarrs)
								{
                                                                    if($desarrs!=$orginarrs)
                                                                    {
                                                                            $query="
                                                                                SELECT
                                                                                    *
                                                                                FROM
                                                                                    ".__DBC_SCHEMATA_PRICING_CC__."
                                                                                WHERE
                                                                                    idWarehouseFrom='".(int)$orginarrs."'
                                                                                AND
                                                                                    idWarehouseTo='".(int)$desarrs."'
                                                                                    ".$sql."
                                                                            ";
                                                                            //echo $query."<br/>";
                                                                            if( ( $result1 = $this->exeSQL( $query ) ) )
                                                                            { 
                                                                                if ($this->getRowCnt() > 0)
                                                                                {
                                                                                    while($row1 = $this->getAssoc($result1))
                                                                                    {
                                                                                        $query="
                                                                                            SELECT
                                                                                                *
                                                                                            FROM
                                                                                                ".__DBC_SCHEMATA_WAREHOUSES__."
                                                                                            WHERE
                                                                                                id='".(int)$orginarrs."' 
                                                                                        ";
                                                                                        //echo $query."<br/>";
                                                                                        $result2 = $this->exeSQL( $query );
                                                                                        $row2 = $this->getAssoc($result2);
                                                                                        $array_orign[$i]['orgin_city']=$row2['szCity'];
                                                                                        $array_orign[$i]['orgin_country']=$kConfig->getCountryName($row2['idCountry']);
                                                                                        $array_orign[$i]['orgin_cfs']=$row2['szWareHouseName'];
                                                                                        $array_orign[$i]['origWarehouseType']=$row2['iWarehouseType'];
                                                                                        $array_orign[$i]['from_id']=$orginarrs;

                                                                                        $query="
                                                                                            SELECT
                                                                                                *
                                                                                            FROM
                                                                                                ".__DBC_SCHEMATA_WAREHOUSES__."
                                                                                            WHERE
                                                                                                id='".(int)$desarrs."' 
                                                                                        ";
                                                                                            //	echo $query."<br/>";
                                                                                        $result3 = $this->exeSQL( $query );
                                                                                        $row3 = $this->getAssoc($result3);
                                                                                        $array_orign[$i]['des_city']=$row3['szCity'];
                                                                                        $array_orign[$i]['des_country']=$kConfig->getCountryName($row3['idCountry']);
                                                                                        $array_orign[$i]['des_cfs']=$row3['szWareHouseName'];
                                                                                        $array_orign[$i]['desWarehouseType']=$row3['iWarehouseType'];
                                                                                        $array_orign[$i]['to_id']=$desarrs;


                                                                                        if($row1['szOriginDestination']=='1')
                                                                                        {
                                                                                            if($row1['fPrice']>=0 && $row1['szCurrency']!='')
                                                                                            {
                                                                                                $array_orign[$i]['export_value']=$row1['fPrice'];
                                                                                                $array_orign[$i]['export_currency']=$this->getFreightCurrency($row1['szCurrency']);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                $array_orign[$i]['export_value']='';
                                                                                                $array_orign[$i]['export_currency']='';
                                                                                            }
                                                                                        }
                                                                                        else if($row1['szOriginDestination']=='2')
                                                                                        {
                                                                                            if($row1['fPrice']>=0 && $row1['szCurrency']!='')
                                                                                            {
                                                                                                $array_orign[$i]['import_value']=$row1['fPrice'];
                                                                                                $array_orign[$i]['import_currency']=$this->getFreightCurrency($row1['szCurrency']);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                $array_orign[$i]['import_value']='';
                                                                                                $array_orign[$i]['import_currency']='';
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    ++$i; 
                                                                                }
                                                                                else
                                                                                { 
                                                                                        if($downloadType!='1' && $inActiveService==0)
                                                                                        {
                                                                                            $query="
                                                                                                SELECT
                                                                                                    *
                                                                                                FROM
                                                                                                    ".__DBC_SCHEMATA_WAREHOUSES__."
                                                                                                WHERE
                                                                                                    id='".(int)$orginarrs."' 
                                                                                            ";
                                                                                            //echo $query."<br/>";
                                                                                            $result2 = $this->exeSQL( $query );
                                                                                            $row2 = $this->getAssoc($result2);
                                                                                            $array_orign[$i]['orgin_city']=$row2['szCity'];
                                                                                            $array_orign[$i]['orgin_country']=$kConfig->getCountryName($row2['idCountry']);
                                                                                            $array_orign[$i]['orgin_cfs']=$row2['szWareHouseName'];
                                                                                            $array_orign[$i]['origWarehouseType']=$row2['iWarehouseType'];
                                                                                            $array_orign[$i]['from_id']=$orginarrs;
                                                                                            $query="
                                                                                                SELECT
                                                                                                    *
                                                                                                FROM
                                                                                                    ".__DBC_SCHEMATA_WAREHOUSES__."
                                                                                                WHERE
                                                                                                    id='".(int)$desarrs."' 
                                                                                            ";
                                                                                                //	echo $query."<br/>";
                                                                                            $result3 = $this->exeSQL( $query );
                                                                                            $row3 = $this->getAssoc($result3);
                                                                                            $array_orign[$i]['des_city']=$row3['szCity'];
                                                                                            $array_orign[$i]['des_country']=$kConfig->getCountryName($row3['idCountry']);
                                                                                            $array_orign[$i]['des_cfs']=$row3['szWareHouseName'];
                                                                                            $array_orign[$i]['desWarehouseType']=$row3['iWarehouseType'];
                                                                                            $array_orign[$i]['to_id']=$desarrs;
                                                                                        }
                                                                                    }
                                                                                }
											++$i;
									}
								}
							}
						}
					}
					$result_array=$array_orign;
					$rowcounter=2;
					
					$this->exportCustomClearanceListData($objPHPExcel,'1',$fileName);
                                        
				if(!empty($result_array))
			   	{
			   		foreach($result_array as $result_arrays)
			   		{ 	   
                                            if($result_arrays['origWarehouseType']!=$result_arrays['desWarehouseType'])
                                            {
                                                /*
                                                * iWarehouseType: 1 means CFS
                                                * iWarehouseType: 2 means Aiport warehouse
                                                * 
                                                * We only allowed to add custom clearance combination for same warehouse type. I mean we should only have custom clearance for either CFS or Airport warehouses. @Ajay
                                                */
                                                continue;
                                            }
                                            $colcounter=0;
                                            $rowcounter++;
                                            $fillColor='A'.$rowcounter.':F'.$rowcounter;
                                            $fill_color='A'.$rowcounter.':J'.$rowcounter;
                                            $objPHPExcel->getActiveSheet()->getStyle($fillColor)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                                            $objPHPExcel->getActiveSheet()->getStyle($fillColor)->getFill()->getStartColor()->setARGB('FFddd9c3');
                                            //$objPHPExcel->getActiveSheet()->getStyle($fillColor)->applyFromArray($styleArray);



                                            $objPHPExcel->getActiveSheet()->getStyle('G'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0.00');

                                            $objPHPExcel->getActiveSheet()->getStyle('I'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0.00');


                                            $objPHPExcel->getActiveSheet()->setCellValue('A'.$rowcounter,$result_arrays['orgin_country'])->getStyle('A'.$rowcounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                                            $objPHPExcel->getActiveSheet()->getStyle('A'.$rowcounter)->applyFromArray($styleArray);
                                            
                                            $objPHPExcel->getActiveSheet()->setCellValue('B'.$rowcounter,$result_arrays['orgin_city'])->getStyle('B'.$rowcounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                                            $objPHPExcel->getActiveSheet()->getStyle('B'.$rowcounter)->applyFromArray($styleArray);
                                            $objPHPExcel->getActiveSheet()->setCellValue('C'.$rowcounter,$result_arrays['orgin_cfs'])->getStyle('C'.$rowcounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                                            $objPHPExcel->getActiveSheet()->getStyle('C'.$rowcounter)->applyFromArray($styleArray);
                                            $objPHPExcel->getActiveSheet()->setCellValue('D'.$rowcounter,$result_arrays['des_country'])->getStyle('D'.$rowcounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                                            $objPHPExcel->getActiveSheet()->getStyle('D'.$rowcounter)->applyFromArray($styleArray);
                                            $objPHPExcel->getActiveSheet()->setCellValue('E'.$rowcounter,$result_arrays['des_city'])->getStyle('E'.$rowcounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                                            $objPHPExcel->getActiveSheet()->getStyle('E'.$rowcounter)->applyFromArray($styleArray);
                                            $objPHPExcel->getActiveSheet()->setCellValue('F'.$rowcounter,$result_arrays['des_cfs'])->getStyle('F'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                            $objPHPExcel->getActiveSheet()->getStyle('F'.$rowcounter)->applyFromArray($styleArray);
                                            $objPHPExcel->getActiveSheet()->setCellValue('G'.$rowcounter,$result_arrays['export_value'])->getStyle('G'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                            $objPHPExcel->getActiveSheet()->getStyle('G'.$rowcounter)->applyFromArray($styleArray1);
                                            $objPHPExcel->getActiveSheet()->setCellValue('H'.$rowcounter,$result_arrays['export_currency'])->getStyle('H'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                            $objPHPExcel->getActiveSheet()->getStyle('H'.$rowcounter)->applyFromArray($styleArray);
                                            $objPHPExcel->getActiveSheet()->setCellValue('I'.$rowcounter,$result_arrays['import_value'])->getStyle('I'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                            $objPHPExcel->getActiveSheet()->getStyle('I'.$rowcounter)->applyFromArray($styleArray1);
                                            $objPHPExcel->getActiveSheet()->setCellValue('J'.$rowcounter,$result_arrays['import_currency'])->getStyle('J'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                            $objPHPExcel->getActiveSheet()->getStyle('J'.$rowcounter)->applyFromArray($styleArray);

                                            $currencyArr=$this->getFreightCurrency();
                                            $countCurrency=count($currencyArr);
                                            $val_h="H".$rowcounter;
                                            $val_currency='List!$A$1:$A$'.$countCurrency;
                                            $objValidation = $objPHPExcel->getActiveSheet()->getCell($val_h)->getDataValidation();
                                            $objValidation->setFormula1($val_currency);
                                            $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
                                            $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
                                            $objValidation->setAllowBlank(true);
                                            $objValidation->setShowInputMessage(true);
                                            $objValidation->setShowErrorMessage(true);
                                            $objValidation->setShowDropDown(true);
                                            $objValidation->setErrorTitle('Currency');					   
                                            $objValidation->setError('Please select one of the currencies.');

                                            $val_j="J".$rowcounter;
                                            $objPHPExcel->getActiveSheet()->getCell($val_j)->setDataValidation(clone $objValidation);

                                            $val_g="G".$rowcounter;

                                            $val_i="I".$rowcounter;

                                            $objValidation = $objPHPExcel->getActiveSheet()->getCell($val_g)->getDataValidation();
                                            $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL );
                                            $objValidation->setOperator( PHPExcel_Cell_DataValidation::OPERATOR_GREATERTHANOREQUAL);		
                                            $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
                                            $objValidation->setAllowBlank(true);
                                            $objValidation->setShowInputMessage(true);
                                            $objValidation->setShowErrorMessage(true);
                                            $objValidation->setShowDropDown(true);
                                            $objValidation->setErrorTitle('Rate per file');
                                            $objValidation->setError('Please fill in the rate charged per booking in the selected currency. This should be a positive decimal number.');
                                            $objValidation->setFormula1(0);

                                            $objPHPExcel->getActiveSheet()->getCell($val_i)->setDataValidation(clone $objValidation);
						
			   		}
			   	}
				$filter='A2:J'.$rowcounter;
			   	$objPHPExcel->getActiveSheet()->setAutoFilter($filter);
			   	 	
			   
			  	
			  	$objPHPExcel->getActiveSheet()->freezePane('G3');
			   //	$objPHPExcel->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(3, 3);
			   			
			  	
			  	
			  	
				
			 	$objPHPExcel->getActiveSheet()->setTitle('Sheet1');
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				$objPHPExcel->setActiveSheetIndex(0);
				//$ext = end(explode('.', $fileName));
				//$fileName=__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/Transporteca-LCL-Services-upload-sheet_1.xlsx";
				$objWriter->save($fileName);
				return true;
		
	}
	
	function exportCustomClearanceListData($objPHPExcel,$sheetindex,$fileName)
	{
			$sheetindex=1;
			if($sheetindex>0)
			{
				$objPHPExcel->createSheet();
			}
			//Worksheets("List").Visible(False); 
			$objPHPExcel->setActiveSheetIndex($sheetindex);
			
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			
			$currencyArr=$this->getFreightCurrency();
			
			if(!empty($currencyArr))
			{
				$c=1;
				foreach($currencyArr as $currencyArrs)
				{
					$val_a='A'.$c;
					$objPHPExcel->getActiveSheet()->setCellValue($val_a,$currencyArrs);
					++$c;
				}
			}
			
			
			$objPHPExcel->getActiveSheet()->setSheetState(PHPExcel_Worksheet::SHEETSTATE_HIDDEN);
			
			//$objPHPExcel->getActiveSheet()->setHidden(true);
			$objPHPExcel->getActiveSheet()->setTitle('List');
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objPHPExcel->setActiveSheetIndex(0);
			//$ext = end(explode('.', $fileName));
			//$fileName=__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/Transporteca-LCL-Services-upload-sheet_1.xlsx";
			$objWriter->save($fileName);
			return true;
			
	}
	
	
	function importCustomClearanceData($objPHPExcel,$sheetindex,$colcount,$rowcount,$idForwarder)
	{
            date_default_timezone_set('UTC');
            $ctr=0;
            if($rowcount>2)
            {
                for($i=3;$i<=$rowcount;$i++)
                {
                    for($j=0;$j<$colcount;$j++)
                    {
                        if($objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0,$i)->getValue()!="")
                        { 
                            $customClearanceData[$ctr][] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($j,$i)->getValue(); 
                        } 
                    }
                    ++$ctr;
                }
            }
		
            //$idForwarder='2';
            if(!empty($customClearanceData))
            {
                $k=3;
                $l_err=0;
                $l_success=0;
                $d=0;
                foreach($customClearanceData as $customClearanceDatas)
                {
                    $error_flag=false;
                    $err_msg=array();
                    $checkErrorFlag=false;
                    if($customClearanceDatas[0]=="")
                    {
                        $error_flag=true;
                        $err_msg['origin_country']=t($this->t_base.'messages/origin_country_is_required');
                    }

                    if($customClearanceDatas[1]=="")
                    {
                        $error_flag=true;
                        $err_msg['origin_city']=t($this->t_base.'messages/origin_city_is_required'); 
                    }
                    if($customClearanceDatas[2]=="")
                    {
                        $error_flag=true;
                        $err_msg['origin_cfs']=t($this->t_base.'messages/origin_cfs_is_required'); 
                    }
                    if($customClearanceDatas[3]=="")
                    {
                        $error_flag=true;
                        $err_msg['destination_country']=t($this->t_base.'messages/des_cfs_is_required'); 
                    }
                    if($customClearanceDatas[4]=="")
                    {
                        $error_flag=true;
                        $err_msg['destination_city']=t($this->t_base.'messages/des_cfs_is_required'); 
                    }
                    if($customClearanceDatas[5]=="")
                    {
                        $error_flag=true;
                        $err_msg['destination_cfs']=t($this->t_base.'messages/des_cfs_is_required'); 
                    }

                    if($customClearanceDatas[5]!='' && $customClearanceDatas[0]!='' && $customClearanceDatas[4]!='' && $customClearanceDatas[3]!='' && $customClearanceDatas[2]!='' && $customClearanceDatas[1]!='')
                    { 
                        $originCountry_Id=$this->getCountryId(sanitize_all_html_input($customClearanceDatas[0])); 
                        $desCountry_Id=$this->getCountryId(sanitize_all_html_input($customClearanceDatas[3]));

                        $query="
                            SELECT
                                id,
                                iWarehouseType
                            FROM
                                ".__DBC_SCHEMATA_WAREHOUSES__."
                            WHERE
                                szWareHouseName='".mysql_escape_custom(sanitize_all_html_input($customClearanceDatas[2]))."' 	
                            AND
                                szCity='".mysql_escape_custom(sanitize_all_html_input($customClearanceDatas[1]))."'
                            AND
                                idCountry='".(int)$originCountry_Id."'
                            AND
                                idForwarder='".(int)$idForwarder."'
                            AND
                                iActive='1'
                            ";
                            //echo $query."<br/><br/>";
                            if($result=$this->exeSQL($query))
                            {
                                if($this->iNumRows>0)
                                {
                                    $row=$this->getAssoc($result);
                                    $fromWareHouse_Id=$row['id'];
                                    $iOriginWarehouseType = $row['iWarehouseType'];
                                }
                                else
                                {
                                    $fromWareHouse_Id=0;
                                }
                            }

                            $query="
                                    SELECT
                                        id,
                                        iWarehouseType
                                    FROM
                                        ".__DBC_SCHEMATA_WAREHOUSES__."
                                    WHERE
                                        szWareHouseName='".mysql_escape_custom($customClearanceDatas[5])."' 	
                                    AND
                                        szCity='".mysql_escape_custom($customClearanceDatas[4])."'
                                    AND
                                        idCountry='".(int)$desCountry_Id."'
                                    AND
                                        idForwarder='".(int)$idForwarder."'
                                    AND
                                        iActive='1'        
                            ";
                            //echo $query."<br/>";
                            if($result=$this->exeSQL($query))
                            {
                                if($this->iNumRows>0)
                                { 
                                    $row=$this->getAssoc($result);
                                    $toWareHouse_Id=$row['id'];
                                    $iDestinationWarehouseType = $row['iWarehouseType'];
                                }
                                else
                                {
                                    $toWareHouse_Id=0;
                                }
                            }

                            if($iOriginWarehouseType!=$iDestinationWarehouseType)
                            {
                                /*
                                * Warehouse at origin and warehouse at destination must have same warehouse type. either Type: CFS or Airport warehouse
                                */
                                continue;
                            }
                            if($toWareHouse_Id>0 && $fromWareHouse_Id>0)
                            {
                                $query="
                                    SELECT
                                        *
                                    FROM
                                        ".__DBC_SCHEMATA_PRICING_CC__."
                                    WHERE
                                        idWarehouseFrom='".(int)$fromWareHouse_Id."'
                                    AND
                                        idWarehouseTo='".(int)$toWareHouse_Id."'
                                    AND
                                        iActive='1'
                                ";
                                //echo $query;
                                if($result=$this->exeSQL($query))
                                {
                                    if($this->iNumRows>0)
                                    {
                                        $checkErrorFlag=true;
                                        if($customClearanceDatas[6]=='' && $customClearanceDatas[7]=='' && $customClearanceDatas[8]=='' && $customClearanceDatas[9]=='')
                                        {
                                            $checkErrorFlag=false;
                                            $deleteWarehouseCombination[$d]['toWareHouse']=$toWareHouse_Id;
                                            $deleteWarehouseCombination[$d]['fromWareHouse']=$fromWareHouse_Id;
                                            ++$d;
                                        }								
                                    }
                                    else
                                    {
                                        if($customClearanceDatas[6]!='')
                                        {
                                            $checkErrorFlag=true;
                                        }

                                        if($customClearanceDatas[7]!='')
                                        {
                                            $checkErrorFlag=true;
                                        }

                                        if($customClearanceDatas[8]!='')
                                        {
                                            $checkErrorFlag=true;
                                        }

                                        if($customClearanceDatas[9]!='')
                                        {
                                            $checkErrorFlag=true;
                                        } 
                                    }
                                }
                            }
				}
				else
				{
					$checkErrorFlag=true;
				}
				
				if($checkErrorFlag)
				{
				
					if((float)$customClearanceDatas[6]>0 || $customClearanceDatas[7]!=0)
					{
						if($customClearanceDatas[6]=="")
						{ 
//                                                    $error_flag=true;
//                                                    $err_msg['export_per_wm']=t($this->t_base.'messages/export_per_wm_is_required');
						}
						else if((float)$customClearanceDatas[6]<0 )
						{
							$error_flag=true;
							$err_msg['export_per_wm']=t($this->t_base.'messages/export_per_wm_should_be_positive_value');
						}
						
						if($customClearanceDatas[7]=="")
						{
						
							$error_flag=true;
							$err_msg['export_currency']=t($this->t_base.'messages/export_currency_is_required');
						}
						else 
						{
							$currencyArr=$this->getFreightCurrency();
							if(!empty($currencyArr))
							{
								if(!in_array(strtoupper($customClearanceDatas[7]),$currencyArr))
								{
									$error_flag=true;
									$err_msg['export_currency']=t($this->t_base.'messages/invalid_export_currency_type');
								}
							}
						}
					}
					
					if((float)$customClearanceDatas[8]>=0 || $customClearanceDatas[9]!="")
					{
						if($customClearanceDatas[8]=="")
						{
						
//							$error_flag=true;
//							$err_msg['import_per_wm']=t($this->t_base.'messages/import_per_wm_is_required');
						}
						else if((float)$customClearanceDatas[8]<0)
						{
                                                    $error_flag=true;
                                                    $err_msg['import_per_wm']=t($this->t_base.'messages/import_per_wm_should_be_positive_value');
						}
						
						
						if($customClearanceDatas[9]=="")
						{
						
							$error_flag=true;
							$err_msg['import_currency']=t($this->t_base.'messages/ipmort_currency_is_required');
						}
						else
						{
							$currencyArr=$this->getFreightCurrency();
							if(!empty($currencyArr))
							{
								if(!in_array(strtoupper($customClearanceDatas[9]),$currencyArr))
								{
									$error_flag=true;
									$err_msg['import_currency']=t($this->t_base.'messages/invalid_import_currency_type');
								}
							}
						}
					}
					
					
					if(!$error_flag)
					{
						$newcustomClearanceData[$k]=$customClearanceDatas;
					}
					else
					{
						$err_msg_arr=implode(", ",$err_msg);
						$errmsg_arr[]=t($this->t_base.'messages/following_error_in_row_number')." ".$k." - ".$customClearanceDatas[0]."/".$customClearanceDatas[1]."/".$customClearanceDatas[2]."/  ".$customClearanceDatas[3]."/".$customClearanceDatas[4]."/".$customClearanceDatas[5].""." : ".$err_msg_arr;
						++$l_err;
					}
					
				}
				
				++$k;
			}
		}
		if(!empty($deleteWarehouseCombination))
		{
			$i=0;
			$deleteFromWarehouse=array();
			$deleteToWarehouse=array();
			foreach($deleteWarehouseCombination as $deleteWarehouseCombinations)
			{
				$query="
					UPDATE
						".__DBC_SCHEMATA_PRICING_CC__."
					SET
						iActive='0',
						dtUpdateOn=NOW()
					WHERE
				   		idWarehouseFrom='".(int)$deleteWarehouseCombinations['fromWareHouse']."'
				   	AND
				   		idWarehouseTo='".(int)$deleteWarehouseCombinations['toWareHouse']."'
				";		
				//echo $query."<br/>";
				$result=$this->exeSQL($query);
				++$i;
				++$l_del_success;
				if(!in_array($deleteWarehouseCombinations['fromWareHouse'],$deleteFromWarehouse) && !in_array($deleteWarehouseCombinations['toWareHouse'],$deleteToWarehouse))
				{
					$deleteToWarehouse[]=$deleteWarehouseCombinations['toWareHouse'];
					$deleteFromWarehouse[]=$deleteWarehouseCombinations['fromWareHouse'];
					
				}
			}
		}
		//print_r($newcustomClearanceData);
		if(!empty($newcustomClearanceData))
		{
			$updateFromWarehouseArr=array();	
			$updateToWarehouseArr=array();
			$totRowcount = count($newcustomClearanceData);
			$randomIndex = rand(1,$totRowcount);
			$rss_counter = 1;
			foreach($newcustomClearanceData as $keys=>$newcustomClearanceDatas)
			{
				$error_flag=false;
				$err_msg=array();
				$toWareHouseId=0;
				$fromWareHouseId=0;
				$originCountry=sanitize_all_html_input($newcustomClearanceDatas[0]);
				$originCity=sanitize_all_html_input($newcustomClearanceDatas[1]);
				$originCFS=sanitize_all_html_input($newcustomClearanceDatas[2]);
				
				$desCountry=sanitize_all_html_input($newcustomClearanceDatas[3]);
				$desCity=sanitize_all_html_input($newcustomClearanceDatas[4]);
				$desCFS=sanitize_all_html_input($newcustomClearanceDatas[5]);
				
				$originCountryId=$this->getCountryId($originCountry);
				
				$desCountryId=$this->getCountryId($desCountry);
				
				if($randomIndex == $rss_counter)
				{
					$szOriginCountryName = $originCountry ;
					$szDestinationCountryName = $desCountry ;
				}
			
				$rss_counter++;
				
				$query="
					SELECT
						id
					FROM
						".__DBC_SCHEMATA_WAREHOUSES__."
					WHERE
						szWareHouseName='".mysql_escape_custom($originCFS)."' 	
					AND
						szCity='".mysql_escape_custom($originCity)."'
					AND
						idCountry='".(int)$originCountryId."'
					AND
						idForwarder='".(int)$idForwarder."' 
				";
				if($result=$this->exeSQL($query))
				{
					if($this->iNumRows<=0)
					{
						$error_flag=true;
						//$err_msg=$newLCLServiceDatas[0]."".$newLCLServiceDatas[1]."/".$newLCLServiceDatas[2]."/".$newLCLServiceDatas[3]."/".$newLCLServiceDatas[4]."/".$newLCLServiceDatas[5]."/ To Warehouse name does not related to forwarder on row ".$k;
						$err_msg['from_warehouse']=t($this->t_base.'messages/from_warehouse_name_does_not_related_to_forwarder');
						
					}
					else
					{
						$row=$this->getAssoc($result);
						$fromWareHouseId=$row['id'];
					}
				}
				
				$query="
					SELECT
						id
					FROM
						".__DBC_SCHEMATA_WAREHOUSES__."
					WHERE
						szWareHouseName='".mysql_escape_custom($desCFS)."' 	
					AND
						szCity='".mysql_escape_custom($desCity)."'
					AND
						idCountry='".(int)$desCountryId."'
					AND
						idForwarder='".(int)$idForwarder."' 
				";
				//echo $query."<br/>";
				if($result=$this->exeSQL($query))
				{
					if($this->iNumRows<=0)
					{
						
						$error_flag=true;
						//$err_msg=$newLCLServiceDatas[0]."".$newLCLServiceDatas[1]."/".$newLCLServiceDatas[2]."/".$newLCLServiceDatas[3]."/".$newLCLServiceDatas[4]."/".$newLCLServiceDatas[5]."/ To Warehouse name does not related to forwarder on row ".$k;
						$err_msg['to_warehouse']=t($this->t_base.'messages/to_warehouse_name_does_not_related_to_forwarder');
						
					}
					else
					{
						$row=$this->getAssoc($result);
						$toWareHouseId=$row['id'];
					}
				}
				
				if(!$error_flag)
				{
					$currencyImportId='';
					$currencyImportId=$this->getFreightCurrencyId(sanitize_all_html_input(ucwords($newcustomClearanceDatas[9])));
					
					
					$currencyExportId='';
					$currencyExportId=$this->getFreightCurrencyId(sanitize_all_html_input(ucwords($newcustomClearanceDatas[7])));
					
					$customClearArr=array();
					
					$customClearArr['export']['per_value']=sanitize_all_html_input($newcustomClearanceDatas[6]);
					$customClearArr['export']['currency']=$currencyExportId;
					$customClearArr['import']['per_value']=sanitize_all_html_input($newcustomClearanceDatas[8]);
					$customClearArr['import']['currency']=$currencyImportId;
					$successFlag=false;
					if(!empty($customClearArr))
					{
						foreach($customClearArr as $key=>$customClearArrs)
						{
							$szOriginDestination='';
							if($key=='export')
							{
								$szOriginDestination='1';
							}
							else if($key=='import')
							{
								$szOriginDestination='2';
							}
							
							$query="
								SELECT
									*
								FROM
									".__DBC_SCHEMATA_PRICING_CC__."
								WHERE
									idWarehouseFrom='".(int)$fromWareHouseId."'
								AND
									idWarehouseTo='".(int)$toWareHouseId."'
								AND
									szOriginDestination='".mysql_escape_custom($szOriginDestination)."'
								";
						//	echo $query."<br/>";
								if($result=$this->exeSQL($query))
								{
									if($this->iNumRows>0)
									{
										$row=$this->getAssoc($result);
										
										$updateflag=false;
										if($row['fPrice']!=$customClearArrs['per_value'])
										{
											$updateflag=true;							
										}
										
										if($row['szCurrency']!=$customClearArrs['currency'])
										{
											$updateflag=true;							
										}
										
										
										//echo $updateflag."hello";
										if($updateflag)
										{
										
										
											$fromtoid=$fromWareHouseId."_".$toWareHouseId;
											if(!in_array($fromtoid,$updateFromWarehouseArr))
											{
												$updateFromWarehouseArr[]=$fromWareHouseId."_".$toWareHouseId;
										
												$query="
													UPDATE
														".__DBC_SCHEMATA_PRICING_CC__."
													SET
														iActive='0'
													WHERE
												   		idWarehouseFrom='".(int)$fromWareHouseId."'
												   	AND
												   		idWarehouseTo='".(int)$toWareHouseId."'
												";	
												//echo $query."<br/>";
												$result=$this->exeSQL($query);
											}
											
											if((int)$_SESSION['forwarder_admin_id']>0)
											{
												$idAdmin = (int)$_SESSION['forwarder_admin_id'];
												$idForwarderContact = 0 ;
											}
											else
											{
												$idAdmin = 0;
												$idForwarderContact = (int)$_SESSION['forwarder_user_id'];
											}
																												
											$query="
												INSERT INTO
													".__DBC_SCHEMATA_FORWARDER_PRCIING_CC_ARCHIVE__."
												(
														idWarehouseFrom,
														idWarehouseTo,
														szOriginDestination,
														fPrice,
														szCurrency, 
														dtUpdateOn,
														iActive,
														idForwarder,
														idForwarderContact,
														idAdmin,
														dtCreatedOn
														
													)
													VALUES
													(
														'".(int)$fromWareHouseId."',
														'".(int)$toWareHouseId."',
														'".mysql_escape_custom($row['szOriginDestination'])."',
														'".(float)$row['fPrice']."',
														'".(int)$row['szCurrency']."',
														NOW(),
														'".(int)$row['iActive']."',
														'".(int)$idForwarder."',
														'".(int)$idForwarderContact."',
														'".(int)$idAdmin."',
														'".mysql_escape_custom($row['dtCreatedOn'])."'					
													)
												";
												//echo $query."<br/>";
												$result=$this->exeSQL($query);
												
												  $iActive='1';		
												  if($customClearArrs['currency']==0)
												  {
												  	 $iActive='0';
												  }
												
												$query="
													UPDATE
														".__DBC_SCHEMATA_PRICING_CC__."
													SET
														fPrice='".(float)$customClearArrs['per_value']."',
														szCurrency='".(int)$customClearArrs['currency']."',
														iActive='".$iActive."',
														dtUpdateOn=NOW()
												    WHERE
												   		idWarehouseFrom='".(int)$fromWareHouseId."'
												   	AND
												   		idWarehouseTo='".(int)$toWareHouseId."'
												   	AND
												   		szOriginDestination='".(int)$szOriginDestination."'
												";
										//echo $query."<br/>";
												if($result=$this->exeSQL($query))
												{
													if($this->getRowCnt()>0)
													{
														$successFlag=true;
														if($szOriginDestination==1)
														{
															$exportCountryAry[] = $originCountry ;
															/*
															if(!empty($exportCountry))
															{
																$exportCountry .=', '.$originCountry;
															}
															else
															{
																$exportCountry = $originCountry ;
															}
															*/
														}		
													}
												}
										}
										else
										{
										
										$fromtoid=$fromWareHouseId."_".$toWareHouseId;
										if(!in_array($fromtoid,$updateFromWarehouseArr))
										{
											$updateFromWarehouseArr[]=$fromWareHouseId."_".$toWareHouseId;
											
											$query="
												UPDATE
													".__DBC_SCHEMATA_PRICING_CC__."
												SET
													iActive='0'
												WHERE
											   		idWarehouseFrom='".(int)$fromWareHouseId."'
											   	AND
											   		idWarehouseTo='".(int)$toWareHouseId."'
											";	
											//echo $query."<br/>";
											$result=$this->exeSQL($query);
										}
										
											$query="
												UPDATE
													".__DBC_SCHEMATA_PRICING_CC__."
												SET
													iActive='1',
													dtUpdateOn=NOW()
											    WHERE
											   		idWarehouseFrom='".(int)$fromWareHouseId."'
											   	AND
											   		idWarehouseTo='".(int)$toWareHouseId."'
											   	AND
											   		szOriginDestination='".(int)$szOriginDestination."'
											 ";
											//echo $query."<br/>";
											$result=$this->exeSQL($query);
												
										}
									}
									else
									{
									
									$fromtoid=$fromWareHouseId."_".$toWareHouseId;
									if(!in_array($fromtoid,$updateFromWarehouseArr))
									{
										$updateFromWarehouseArr[]=$fromWareHouseId."_".$toWareHouseId;
										
										$query="
											UPDATE
												".__DBC_SCHEMATA_PRICING_CC__."
											SET
												iActive='0'
											WHERE
										   		idWarehouseFrom='".(int)$fromWareHouseId."'
										   	AND
										   		idWarehouseTo='".(int)$toWareHouseId."'
										";	
										//echo $query."<br/>";
										$result=$this->exeSQL($query);
									}
									  $iActive='1';		
									  if($customClearArrs['currency']==0)
									  {
									  	 $iActive='0';
									  }
									
										$query="
											INSERT INTO
												".__DBC_SCHEMATA_PRICING_CC__."
											(
													idWarehouseFrom,
													idWarehouseTo,
													szOriginDestination,
													fPrice,
													szCurrency,
													iActive,
													dtCreatedOn, 
													dtUpdateOn							
													
												)
													VALUES
												(
													'".(int)$fromWareHouseId."',
													'".(int)$toWareHouseId."',
													'".(int)$szOriginDestination."',
													'".(float)$customClearArrs['per_value']."',
													'".(int)$customClearArrs['currency']."',
													'".$iActive."',
													NOW(),
													NOW()						
												)
											";
									//	echo $query."<br/>";
											$result=$this->exeSQL($query);
											$successFlag=true;
									}
								}
							}
							if($successFlag)
							{
								++$l_success;								
														
								if($szOriginDestination==2)
								{
									$importCountryAry[] = $desCountry ;
									/*
									if(!empty($importCountry))
									{
										$importCountry .=', '.$desCountry;
									}
									else
									{
										$importCountry = $desCountry ;
									}
									*/
								}
							}
					}
				}
				else
				{
					$err_msg_arr=implode(", ",$err_msg);
					$errmsg_arr[]=t($this->t_base.'messages/following_error_in_row_number')." ".$keys." - ".$newcustomClearanceDatas[0]."/".$newcustomClearanceDatas[1]."/".$newcustomClearanceDatas[2]."/  ".$newLCLServiceDatas[3]."/".$newcustomClearanceDatas[4]."/".$newcustomClearanceDatas[5]." : ".$err_msg_arr;
					++$l_err;
				}
			}
		}
		
		if(!empty($exportCountryAry))
		{
			$exportCountryArys = array_unique($exportCountryAry);
		}
		
		if(!empty($exportCountryArys))
		{
			$exportCountryFormatedAry = format_fowarder_emails($exportCountryArys);
			
			$exportCountry = $exportCountryFormatedAry[1];
			
			// $exportCountry = implode(',',$exportCountryArys) ;
		}			
		
		if(!empty($importCountryAry))
		{
			$importCountryArys = array_unique($importCountryAry);
		}
		
		if(!empty($importCountryArys))
		{
			$importCountryFormatedAry = format_fowarder_emails($importCountryArys);
			
			$importCountry = $importCountryFormatedAry[1];
			
			// $importCountry = implode(',',$importCountryArys) ;
		}
		
		$this->successLine=$l_success;
		$this->errorLine=$l_err;
		$this->deleteLine=$l_del_success;
		$updateLine = $this->successLine+$this->deleteLine;
		$err_str="";
		 
                if((int)$_SESSION['forwarder_admin_id']>0)
                {
                    $idAdmin = (int)$_SESSION['forwarder_admin_id'];  
                    $kForwarderContact = new cAdmin();
                    $kForwarderContact->getAdminDetails($idAdmin);
                }
                else
                { 
                    $idForwarderContact = (int)$_SESSION['forwarder_user_id'];
                    $kForwarderContact = new cForwarderContact();
                    $kForwarderContact->load($idForwarderContact);
                }
		$this->szEmail=$kForwarderContact->szEmail;
		
		
		$SuccessCount="You have successfully updated ".$updateLine." customs clearance lines.";
		
		$kForwarder = new cForwarder();
		$idForwarder = $_SESSION['forwarder_id'];
		$kForwarder->load($idForwarder);
		
		if(($kForwarder->iAgreeTNC==1) && ($this->successLine>0) && ($kForwarder->iUpdateRss==2))
		{
			/*
			$kConfig = new cConfig();
			$kWhsSearch = new cWHSSearch();
			
			$kWhsSearch->load($this->idOriginWarehouse);
			$szOriginCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
			
			$kWhsSearch->load($this->idDestinationWarehouse);
			$szDestinationCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
			*/
			
			$replace_ary = array();
			$replace_ary['szForwarderDisplayName'] = $kForwarder->szDisplayName ;
			
			$replace_ary['szOriginCountry'] = $szOriginCountryName;
			$replace_ary['szDestinationCountry'] = $szDestinationCountryName;
			
			if($updateLine==1)
			{
				if(!empty($szOriginCountryName))
				{
					$szCountryName = $szOriginCountryName;
				}
				else
				{
					$szCountryName = $szDestinationCountryName;
				}
				
				$replace_ary['szTradeCountStr'] = $szCountryName ;
				$replace_ary['szTradeCountMinus1Str'] = "";
			}
			else
			{
				$replace_ary['szTradeCountStr'] = $updateLine.' more countries';
				$replace_ary['szTradeCountMinus1Str'] = ' and in '.($updateLine-1).' other trades';
			}
			
			if(!empty($exportCountry))
			{
				$ccServiceDescriptionStr = " You can now book export customs clearance with ".$kForwarder->szDisplayName." in ".$exportCountry.". " ;
				$replace_ary['szExportCCText'] = $ccServiceDescriptionStr ;
			}
			else
			{
				$replace_ary['szExportCCText']='';
			}
			
			if(!empty($importCountry) && !empty($ccServiceDescriptionStr))
			{
				$ccServiceDescriptionStr = " You can also book import customs clearance with ".$kForwarder->szDisplayName." in ".$importCountry."." ;
				$replace_ary['szImportCCText '] = $ccServiceDescriptionStr ;
			}
			else if(!empty($importCountry))
			{
				$ccServiceDescriptionStr = " You can now book import customs clearance with ".$kForwarder->szDisplayName." in ".$importCountry."." ;
				$replace_ary['szImportCCText '] = $ccServiceDescriptionStr ;
			}
			
			$replace_ary['szSiteUrl'] = __MAIN_SITE_HOME_PAGE_SECURE_URL__ ;
			//print_r($replace_ary);
			$content_ary = array();
			$content_ary = $this->replaceRssContent('__BULK_CUSTOM_CLEARANCE__',$replace_ary);
			
			
			$rssDataAry=array();
			$rssDataAry['idForwarder'] = $kForwarder->id ;
			$rssDataAry['szFeedType'] = '__BULK_CUSTOM_CLEARANCE__';
			$rssDataAry['szLink'] = __RSS_FEED_PAGE_URL__ ;
			$rssDataAry['szTitle'] =$content_ary['szTitle'];
			$rssDataAry['szDescription'] =$content_ary['szDescription'];
			$rssDataAry['szDescriptionHTML'] =$content_ary['szDescriptionHTML'];	
			
			$kRss = new cRSS();
			$kRss->addRssFeed($rssDataAry);
		}
		
		if(!empty($errmsg_arr))
		{
                    $l=1;
                    $err_str .="We found errors in following ".$this->errorLine." customs clearance lines in your upload file:";
                    foreach($errmsg_arr as $errmsg_arrs)
                    {
                        $err_str .="<br/>".$l.". ".$errmsg_arrs."<br/>";
                        ++$l;
                    }
                    $err_str .="<br/>Please correct the errors and upload the file again.";
		}
		$replace_ary['SuccessCount']=$SuccessCount;
		$replace_ary['szErrorMsg']=$err_str;
		$replace_ary['szEmail']=$kForwarderContact->szEmail;
                $replace_ary['szName'] = $kForwarderContact->szFirstName." ".$kForwarderContact->szLastName;
                createEmail(__FORWARDER_BULK_UPLOAD_CUSTOM_CLEARANCE_EMAIL__, $replace_ary,$this->szEmail, __FORWARDER_BULK_UPLOAD_EMAIL_CUSTOM_CLEARANCE_SUBJECT__, __STORE_SUPPORT_EMAIL__,$_SESSION['forwarder_id'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__);            
		return true;
	}
	
	
	function haulageOBOData($idForwarder,$idCountry,$idDirection,$warehouseArrs='',$szCity='')
	{
			$sql='';
			if((int)$warehouseArrs>0)
			{
				$sql .="
					AND
						w.id='".(int)$warehouseArrs."'
				";
			}
			
			
			if(!empty($szCity))
			{
				$sql .="
				AND
					w.szCity='".mysql_escape_custom($szCity)."'
				";
			}
			$haulageDataArr=array();
			$query="
				SELECT
					ph.*,
					w.szWareHouseName,
					w.szCity,
					w.idCountry,
					ht.iHours,
					c.szCurrency
				FROM
					".__DBC_SCHEMATA_WAREHOUSES__." AS w
				INNER JOIN
					".__DBC_SCHEMATA_PRICING_HAULAGE__." AS ph
				ON
					ph.idWarehouse=w.id
				INNER JOIN
					".__DBC_SCHEMATA_HAULAGE_TRANSIT_TIME__." AS ht
				ON
					ph.idHaulageTransitTime=ht.id
				INNER JOIN
					".__DBC_SCHEMATA_CURRENCY__." AS c
				ON
					ph.szHaulageCurrency=c.id
				WHERE
					w.idForwarder='".(int)$idForwarder."'
				AND
					w.idCountry='".(int)$idCountry."'
				AND
					ph.idDirection='".(int)$idDirection."'
				AND
					ph.iActive='1' 
					".$sql."
				ORDER BY
					ph.iCrossBorder ASC,
					ph.iUpToKM ASC;	
			";
			//echo $query;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					while($row=$this->getAssoc($result))
					{
						$haulageDataArr[]=$row;
					}
					return $haulageDataArr;
				}
				else
				{
					return array();
				}
			}
			else
			{
				return array();
			}
	}
	
	function deleteHaulageData($idHaulageData)
	{
		$query="
			UPDATE 
				".__DBC_SCHEMATA_PRICING_HAULAGE__."
			SET
				iActive='0',
				dtUpdatedOn=NOW()
			WHERE
				id='".(int)$idHaulageData."'
		";
		if($result=$this->exeSQL($query))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
	function haulagePricingData($id)
	{
			
			$haulageDataArr=array();
			$query="
				SELECT
					ph.*
				FROM
					".__DBC_SCHEMATA_PRICING_HAULAGE__." AS ph
				WHERE
					ph.id='".(int)$id."';	
			";
			//echo $query;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					while($row=$this->getAssoc($result))
					{
						$haulageDataArr[]=$row;
					}
					return $haulageDataArr;
				}
				else
				{
					return array();
				}
			}
			else
			{
				return array();
			}
	}
	
	
	function saveHaulageData($data,$idHaulageData)
	{
		if(is_array($data) && ($idHaulageData)>0)
		{
			$this->set_szCurrency(sanitize_all_html_input(trim($data['idCurrency'])));
			$this->set_iCrosssBorder(sanitize_all_html_input(trim($data['idCrossBorder'])));
			$this->set_PerWM(sanitize_all_html_input(trim($data['per_wm'])));
			$this->set_Minimum(sanitize_all_html_input(trim($data['minimum'])));
			$this->set_PerBooking(sanitize_all_html_input(trim($data['per_booking'])));
			$this->set_Distance(sanitize_all_html_input(trim($data['upto'])));
			$this->set_idTransitTime(sanitize_all_html_input(trim($data['idTransitTime'])));
			$this->set_expiryDate(sanitize_all_html_input(trim($data['datepicker1'])));
			
			$expiryDateArr=explode("/",$this->expiryDate);
			
			if($this->expiryDate=='dd/mm/yyyy' || (strlen($expiryDateArr[0])!=2 || strlen($expiryDateArr[1])!=2 || strlen($expiryDateArr[2])!=4))
			{
				$this->addError('datepicker1',"Expiry date is invalid");
				return false;
			}
			
			if($this->iCrosssBorder=='no')
			{
				$iCrosssBorder='0';
			}
			else if($this->iCrosssBorder=='yes')
			{
				$iCrosssBorder='1';
			}
			
			if($this->error === true)
			{
				return false;
			}
			$expiryDate=$expiryDateArr[2]."-".$expiryDateArr[1]."-".$expiryDateArr[0];
			$idForwarder = $_SESSION['forwarder_id'] ;
			
			$query="
				SELECT
					*
				FROM
					".__DBC_SCHEMATA_PRICING_HAULAGE__."
				WHERE
					id='".(int)$idHaulageData."';
				";
				//echo $query."<br/>";
				if($result=$this->exeSQL($query))
				{
					if($this->iNumRows>0)
					{
						$row=$this->getAssoc($result);
						$add_archive_flag = false;
						if($row['iUpToKM']!=$this->distance)
						{
							$add_archive_flag = true;
						}
						else if($row['idHaulageTransitTime']!=$this->idTransitTime)
						{
							$add_archive_flag = true;
						}
						else if($row['idHaulageTransitTime']!=$this->idTransitTime)
						{
							$add_archive_flag = true;
						}
						else if($row['fRateWM_Km']!=$this->fPerWM)
						{
							$add_archive_flag = true;
						}
						else if($row['fMinRateWM_Km']!=$this->fMinimum)
						{
							$add_archive_flag = true;
						}
						
						if((int)$_SESSION['forwarder_admin_id']>0)
						{
							$idAdmin = (int)$_SESSION['forwarder_admin_id'];
							$idForwarderContact = 0 ;
						}
						else
						{
							$idAdmin = 0;
							$idForwarderContact = (int)$_SESSION['forwarder_user_id'];
						}
						
						$query="
							INSERT INTO
								".__DBC_SCHEMATA_FORWARDER_HAULAGE_ARCHIVE__."
							(
								idForwarder,
								idForwarderContact,
								idAdmin,
								idWarehouse,
								idDirection,
								iUpToKM,
								idHaulageTransitTime,
								fRateWM_Km,
								fMinRateWM_Km,
								fRate,
								szHaulageCurrency,
								iCrossBorder,
								dtCreateOn,
								dtUpdatedOn,
								dtExpiry,
								dtDataUpdated
							)
							VALUES
							(
								'".(int)$idForwarder."',
								'".(int)$idForwarderContact."',
								'".(int)$idAdmin."',
								'".(int)$row['idWarehouse']."',
								'".(int)$row['idDirection']."',
								'".mysql_escape_custom($row['iUpToKM'])."',
								'".mysql_escape_custom($row['idHaulageTransitTime'])."',
								'".(float)$row['fRateWM_Km']."',
								'".(float)$row['fMinRateWM_Km']."',
								'".(float)$row['fRate']."',
								'".(int)$row['szHaulageCurrency']."',
								'".mysql_escape_custom($row['iCrossBorder'])."',
								'".mysql_escape_custom($row['dtCreateOn'])."',
								'".mysql_escape_custom($row['dtUpdatedOn'])."',
								'".mysql_escape_custom($row['dtExpiry'])."',
								NOW()						
							)
						";
						//echo $query."<br/>";
						$result=$this->exeSQL($query);
						
						$idDirection = $row['idDirection'] ;
						$idOriginWhs = $row['idWarehouse'] ;
					}
				}
				
			$query="
				UPDATE
					".__DBC_SCHEMATA_PRICING_HAULAGE__."
				SET
					idHaulageTransitTime='".(int)$this->idTransitTime."',
					fRateWM_Km='".(float)$this->fPerWM."',
					fMinRateWM_Km='".(float)$this->fMinimum."',
					fRate='".(float)$this->fPerBooking."',
					szHaulageCurrency='".(int)$this->szCurrency."',
					iCrossBorder='".(int)$iCrosssBorder."',
					dtUpdatedOn=NOW(),
					dtExpiry='".mysql_escape_custom($expiryDate)."',
					iUpToKM='".(int)$this->distance."'
			    WHERE
			    	id='".(int)$idHaulageData."';
			   ";
			//echo $query;
				if($result=$this->exeSQL($query))
				{
					$kForwarder = new cForwarder();
					$idForwarder = $_SESSION['forwarder_id'];
					$kForwarder->load($idForwarder);
					
					if(($kForwarder->iAgreeTNC==1) && ($kForwarder->iUpdateRss==2))
					{
						
						$kConfig = new cConfig();
						$kWhsSearch = new cWHSSearch();
						
						$kWhsSearch->load($idOriginWhs);
						$szOriginCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
						/*
						$kWhsSearch->load($this->idDestinationWarehouse);
						$szDestinationCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
						*/
						
						if($idDirection=='1')
						{
							$szDirection = 'Export';
						}
						else if($idDirection=='2')
						{
							$szDirection = 'Import';
						}
						
						$replace_ary = array();
						$replace_ary['szForwarderDisplayName'] = $kForwarder->szDisplayName ;
						$replace_ary['szCountryName'] = $szOriginCountry ;
						$replace_ary['szImportExportText'] = $szDirection ;
						
						$content_ary = array();
						$content_ary = $this->replaceRssContent('__OBO_HAULAGE__',$replace_ary);
						
						$rssDataAry=array();
						$rssDataAry['idForwarder'] = $kForwarder->id ;
						$rssDataAry['szFeedType'] = '__OBO_HAULAGE__';
						$rssDataAry['szLink'] = __RSS_FEED_PAGE_URL__ ;
						$rssDataAry['szTitle'] = $content_ary['szTitle'];
						$rssDataAry['szDescription'] = $content_ary['szDescription'];
						$rssDataAry['szDescriptionHTML'] = $content_ary['szDescriptionHTML'];
						
						$kRss = new cRSS();
						$kRss->addRssFeed($rssDataAry);
					}
					return true;
				}
				else
				{
					return false;
				}
		}
		else
		{
			return false;
		}
	}
	
	
	function addHaulageData($data)
	{
		if(is_array($data))
		{
			$this->set_szCurrency(sanitize_all_html_input(trim($data['idCurrency'])));
			$this->set_iCrosssBorder(sanitize_all_html_input(trim($data['idCrossBorder'])));
			$this->set_PerWM(sanitize_all_html_input(trim($data['per_wm'])));
			$this->set_Minimum(sanitize_all_html_input(trim($data['minimum'])));
			$this->set_PerBooking(sanitize_all_html_input(trim($data['per_booking'])));
			$this->set_Distance(sanitize_all_html_input(trim($data['upto'])));
			$this->set_idTransitTime(sanitize_all_html_input(trim($data['idTransitTime'])));
			$this->set_expiryDate(sanitize_all_html_input(trim($data['datepicker1'])));
			
			$country=sanitize_all_html_input($data['idCountry']);
			$Direction=sanitize_all_html_input($data['Direction']);
			$City=sanitize_all_html_input($data['City']);
			$idWareHouse=sanitize_all_html_input($data['idWareHouse']);
			$idForwarder=sanitize_all_html_input($data['idForwarder']);
			
			$expiryDateArr=explode("/",$this->expiryDate);
			
			if((int)$idWareHouse==0)
			{
				$this->addError('idWareHouse',"Warehouse id is required");
				return false;
			}
			
			if($this->expiryDate=='dd/mm/yyyy' || (strlen($expiryDateArr[0])!=2 || strlen($expiryDateArr[1])!=2 || strlen($expiryDateArr[2])!=4))
			{
				$this->addError('datepicker1',"Expiry date is invalid");
				return false;
			}
			
			if($this->error === true)
			{
				return false;
			}
			
			if($this->iCrosssBorder=='no')
			{
				$iCrosssBorder='0';
			}
			else if($this->iCrosssBorder=='yes')
			{
				$iCrosssBorder='1';
			}
			$idWarehousArr=array();
			$expiryDate=$expiryDateArr[2]."-".$expiryDateArr[1]."-".$expiryDateArr[0];
			if((int)$idWareHouse>0)
			{
				$idWarehousArr[]=$idWareHouse;
			}
			/*else
			{
				$idWarehousArray=$this->getForwaderWarehouses($idForwarder,$country,$City);
				if(!empty($idWarehousArray))
				{
					foreach($idWarehousArray as $idWarehousArrays)
					{
						$idWarehousArr[]=$idWarehousArrays['id'];
					}
				}
			}*/
			
			if(!empty($idWarehousArr))
			{
				foreach($idWarehousArr as $idWarehousArrs)
				{
					$query="
						INSERT INTO
							".__DBC_SCHEMATA_PRICING_HAULAGE__."
						(
							idWarehouse,
							idDirection,
							idHaulageTransitTime,
							fRateWM_Km,
							fMinRateWM_Km,
							fRate,
							szHaulageCurrency,
							iCrossBorder,
							dtUpdatedOn,
							dtExpiry,
							iUpToKM,
							dtCreateOn,
							iActive
						)
						VALUES
						(
							'".(int)$idWarehousArrs."',
							'".(int)$Direction."',
							'".(int)$this->idTransitTime."',
							'".(float)$this->fPerWM."',
							'".(float)$this->fMinimum."',
							'".(float)$this->fPerBooking."',
							'".(int)$this->szCurrency."',
							'".(int)$iCrosssBorder."',
							NOW(),
							'".mysql_escape_custom($expiryDate)."',
							'".(int)$this->distance."',
							NOW(),
							'1'
					    );
					";
					$result=$this->exeSQL($query);
					$idOriginWhs = $idWarehousArrs ;
				}
				
				$kForwarder = new cForwarder();
				$idForwarder = $_SESSION['forwarder_id'];
				$kForwarder->load($idForwarder);
				
				if(($kForwarder->iAgreeTNC==1) && ($kForwarder->iUpdateRss==2))
				{
					
					$kConfig = new cConfig();
					$kWhsSearch = new cWHSSearch();
					
					$kWhsSearch->load($idOriginWhs);
					$szOriginCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
					/*
					$kWhsSearch->load($this->idDestinationWarehouse);
					$szDestinationCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
					*/
					
					if($Direction=='1')
					{
						$szDirection = 'Export';
					}
					else if($Direction=='2')
					{
						$szDirection = 'Import';
					}
					
					$replace_ary = array();
					$replace_ary['szForwarderDisplayName'] = $kForwarder->szDisplayName ;
					$replace_ary['szCountryName'] = $szOriginCountry ;
					$replace_ary['szImportExportText'] = $szDirection ;
					
					$content_ary = array();
					$content_ary = $this->replaceRssContent('__OBO_HAULAGE__',$replace_ary);
					
					$rssDataAry=array();
					$rssDataAry['idForwarder'] = $kForwarder->id ;
					$rssDataAry['szFeedType'] = '__OBO_HAULAGE__';
					$rssDataAry['szLink'] = __RSS_FEED_PAGE_URL__ ;
					$rssDataAry['szTitle'] = $content_ary['szTitle'];
					$rssDataAry['szDescription'] = $content_ary['szDescription'];
					$rssDataAry['szDescriptionHTML'] = $content_ary['szDescriptionHTML'];
					
					$kRss = new cRSS();
					$kRss->addRssFeed($rssDataAry);
				}
					
				return true;
			}
			else
			{
				return false;
			}
			
			//print_r($idWarehousArr);
		}
		else
		{
			return false;
		}
	}
	function getDataForCC($data)
	{
		if(!empty($data))
		{
			$this->set_idOriginWarehouse(sanitize_all_html_input(trim($data['idOriginWarehouse'])));
			$this->set_idOriginCountry(sanitize_all_html_input(trim($data['idOriginCountry'])));
			$this->set_szOriginCity(sanitize_all_html_input(trim($data['szOriginCity'])));
			$this->set_idDestinationWarehouse(sanitize_all_html_input(trim($data['idDestinationWarehouse'])));
			$this->set_idDestinationCountry(sanitize_all_html_input(trim($data['idDestinationCountry'])));
			$this->set_szDestinationCity(sanitize_all_html_input(trim($data['szDestinationCity'])));
			
			if($this->error==true)
			{
				return false;
			}
			
			if(!$this->isWarehousebelongsTocountry($this->idOriginWarehouse,$this->idOriginCountry,$this->idOriginCity))
			{
				$this->addError('szOriginWarehouse',t($this->t_base.'messages/origin_warehouse_information_invalid'));
				return false;
			}
			$szOriginCountry = $this->szCountryName ;
			if(!$this->isWarehousebelongsTocountry($this->idDestinationWarehouse,$this->idDestinationCountry,$this->idDestinationCity))
			{
				$this->addError('szDestinationWarehouse',t($this->t_base.'messages/destination_warehouse_information_invalid'));
				return false;
			}
			$szDestinationCountry = $this->szCountryName ;
			$query="
				SELECT
					cc.idWarehouseFrom,
					cc.idWarehouseTo,
					cc.szOriginDestination,
					cc.fPrice,
					cc.szCurrency
				FROM
					".__DBC_SCHEMATA_PRICING_CC__." cc
				WHERE
					idWarehouseFrom = '".(int)$this->idOriginWarehouse."'
				AND
					idWarehouseTo = '".(int)$this->idDestinationWarehouse."'	
				AND
					iActive = '1'	
			";
			//echo "<br>".$query."<br>";
			if($result=$this->exeSQL($query))
			{
				$ccDataAry = array();
				if($this->iNumRows>0)
				{
					while($row=$this->getAssoc($result))
					{
						if($row['szOriginDestination']==1)
						{
							$ccDataAry['export']= $row ;
							$ccDataAry['export']['szCountryName'] = $szOriginCountry ;
						}
						if($row['szOriginDestination']==2)
						{
							$ccDataAry['import']= $row ;
							$ccDataAry['import']['szCountryName'] = $szDestinationCountry ;							
						}
					}
					return $ccDataAry ;
				}
				else
				{
					return $ccDataAry ;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function isWarehousebelongsTocountry($idWarehouse,$idCountry,$szCity=false)
	{
		if($idWarehouse>0)
		{
                    if(!empty($szcity))
                    {
                        $query_and = " AND
                                whs.city = '".mysql_escape_custom(trim($szcity))."'
                        ";
                    }
                    $query="
                        SELECT
                            whs.id,
                            c.szCountryName
                        FROM
                            ".__DBC_SCHEMATA_WAREHOUSES__."	whs
                        INNER JOIN
                            ".__DBC_SCHEMATA_COUNTRY__." c
                        ON
                            c.id = whs.idCountry			
                        WHERE
                            whs.id='".(int)$idWarehouse."'	
                        AND
                            whs.idCountry = '".(int)$idCountry."' 
                            ".$query_and."	
                    ";
                    //echo $query ;
                    if($result=$this->exeSQL($query))
                    {
                        if($this->iNumRows>0)
                        {
                            $row=$this->getAssoc($result);
                            $this->szCountryName = $row['szCountryName'];
                            return true ;
                        }
                        else
                        {
                            return false ;
                        }
                    }
                    else
                    {
                        $this->error = true;
                        $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                        $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                        return false;
                    }
		}
	}
	
	function updateOBOCustomClearance($data)
	{
		$update_oring_cc = false;
		$update_destination_cc = false;
		
		$this->set_idOriginWarehouse(sanitize_all_html_input(trim($data['idOriginWarehouse'])));
		$this->set_idDestinationWarehouse(sanitize_all_html_input(trim($data['idDestinationWarehouse'])));
	
		if((float)$data['fOriginPrice']>0 || (int)$data['idOriginCurrency']>0)
		{
			$this->set_fOriginPrice(sanitize_all_html_input(trim($data['fOriginPrice'])));
			$this->set_idOriginCurrency(sanitize_all_html_input(trim($data['idOriginCurrency'])));
			$update_oring_cc = true;
		}
		
		if((float)$data['fDestinationPrice']>0 || (int)$data['idDestinationCurrency']>0)
		{
			$this->set_fDestinationPrice(sanitize_all_html_input(trim($data['fDestinationPrice'])));
			$this->set_idDestinationCurrency(sanitize_all_html_input(trim($data['idDestinationCurrency'])));
			$update_destination_cc = true;
		}
		
		/*if($update_destination_cc==false && $update_oring_cc==false)
		{
			$this->addError('fDestinationPrice','At least one line should be filled');
			return false;
		}
		*/
		if($this->error==true)
		{
			return false;
		}		
		if($this->idOriginWarehouse == $this->idDestinationWarehouse)
		{
			$this->addError('szDestinationWarehouse',t($this->t_base.'messages/same_whs_error_message'));
			return false;
		}		
		$success = false;	
		$idForwarder = $_SESSION['forwarder_id'] ;	
		
		if($update_oring_cc)
		{
			$query="
				SELECT
					*
				FROM
					".__DBC_SCHEMATA_PRICING_CC__."
				WHERE
					idWarehouseFrom='".(int)$this->idOriginWarehouse."'
				AND
					idWarehouseTo='".(int)$this->idDestinationWarehouse."'
				AND
					szOriginDestination='1'
				AND
					iActive = '1'	
				";
				//echo $query."<br/>";
				if($result=$this->exeSQL($query))
				{
					if($this->iNumRows>0)
					{
						$row=$this->getAssoc($result);	
							
						if((int)$_SESSION['forwarder_admin_id']>0)
						{
							$idAdmin = (int)$_SESSION['forwarder_admin_id'];
							$idForwarderContact = 0 ;
						}
						else if((int)$_SESSION['admin_id']>0)
						{
							$idAdmin = (int)$_SESSION['admin_id'];
							$idForwarderContact = 0 ;
						}
						else
						{
							$idAdmin = 0;
							$idForwarderContact = (int)$_SESSION['forwarder_user_id'];
						}
									
						$query="
							INSERT INTO
								".__DBC_SCHEMATA_FORWARDER_PRCIING_CC_ARCHIVE__."
							(
								idWarehouseFrom,
								idWarehouseTo,
								szOriginDestination,
								fPrice,
								szCurrency, 
								dtUpdateOn,
								iActive,
								idForwarder,
								idForwarderContact,
								idAdmin,
								dtCreatedOn
							)
							VALUES
							(
								'".(int)$this->idOriginWarehouse."',
								'".(int)$this->idDestinationWarehouse."',
								'1',
								'".(float)$row['fPrice']."',
								'".(int)$row['szCurrency']."',
								NOW(),
								'".(int)$row['iActive']."',
								'".(int)$idForwarder."',
								'".(int)$idForwarderContact."',
								'".(int)$idAdmin."',
								'".mysql_escape_custom($row['dtCreatedOn'])."'					
							)
						";
						//echo $query."<br/>";
						$result=$this->exeSQL($query);
						$query="
						UPDATE
							".__DBC_SCHEMATA_PRICING_CC__."
						SET
							fPrice = '".(float)$this->fOriginPrice."',
							szCurrency = '".(int)$this->idOriginCurrency."'
						WHERE
							idWarehouseFrom = '".(int)$this->idOriginWarehouse."'	
						AND
							idWarehouseTo = '".(int)$this->idDestinationWarehouse."'	
						AND
							szOriginDestination = '1'	
						AND
							iActive = '1'	
					";
					//echo "<br>".$query."<br>";
					if($reusult=$this->exeSQL($query))
					{
						$success = true;
					}
					else
					{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					}
				}
				else
				{
					$addOboCCAry = array();
					$addOboCCAry['idOriginWarehouse'] = $this->idOriginWarehouse ;
					$addOboCCAry['idDestinationWarehouse'] = $this->idDestinationWarehouse ;
					$addOboCCAry['fOriginPrice'] = $this->fOriginPrice ;
					$addOboCCAry['idOriginCurrency'] = $this->idOriginCurrency ;
					$this->addOBOCustomClearance($addOboCCAry);
					//$success = true;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}			
		}
		else
		{
			$this->inactivateCustomClearance($this->idOriginWarehouse,$this->idDestinationWarehouse,1);
		}
		
		if($update_destination_cc)
		{
			$query="
				SELECT
					*
				FROM
					".__DBC_SCHEMATA_PRICING_CC__."
				WHERE
					idWarehouseFrom='".(int)$this->idOriginWarehouse."'
				AND
					idWarehouseTo='".(int)$this->idDestinationWarehouse."'
				AND
					szOriginDestination='2'
				AND
					iActive='1'	
				";
				//echo $query."<br/>";
				if($result=$this->exeSQL($query))
				{
					if($this->iNumRows>0)
					{
						if((int)$_SESSION['forwarder_admin_id']>0)
						{
							$idAdmin = (int)$_SESSION['forwarder_admin_id'];
							$idForwarderContact = 0 ;
						}
						else
						{
							$idAdmin = 0;
							$idForwarderContact = (int)$_SESSION['forwarder_user_id'];
						}
						
						$row=$this->getAssoc($result);					
						$query="
							INSERT INTO
								".__DBC_SCHEMATA_FORWARDER_PRCIING_CC_ARCHIVE__."
							(
								idWarehouseFrom,
								idWarehouseTo,
								szOriginDestination,
								fPrice,
								szCurrency, 
								dtUpdateOn,
								iActive,
								idForwarder,
								idForwarderContact,
								idAdmin,
								dtCreatedOn
							)
							VALUES
							(
								'".(int)$this->idOriginWarehouse."',
								'".(int)$this->idDestinationWarehouse."',
								'2',
								'".(float)$row['fPrice']."',
								'".(int)$row['szCurrency']."',
								NOW(),
								'".(int)$row['iActive']."',
								'".(int)$idForwarder."',
								'".(int)$idForwarderContact."',
								'".(int)$idAdmin."',
								'".mysql_escape_custom($row['dtCreatedOn'])."'					
							)
						";
						//echo $query."<br/>";
						$result=$this->exeSQL($query);
						$query="
							UPDATE
								".__DBC_SCHEMATA_PRICING_CC__."
							SET
								fPrice = '".(float)$this->fDestinationPrice."',
								szCurrency = '".(int)$this->idDestinationCurrency."'
							WHERE
								idWarehouseFrom = '".(int)$this->idOriginWarehouse."'	
							AND
								idWarehouseTo = '".(int)$this->idDestinationWarehouse."'	
							AND
								szOriginDestination = '2'	
							AND
								iActive = '1'	
						";
						//echo "<br>".$query."<br>";
						if($reusult=$this->exeSQL($query))
						{
							$success = true;
						}
						else
						{
							$this->error = true;
							$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
							$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
							return false;
						}
					}
					else
					{
						$addOboCCAry = array();
						$addOboCCAry['idOriginWarehouse'] = $this->idOriginWarehouse ;
						$addOboCCAry['idDestinationWarehouse'] = $this->idDestinationWarehouse ;
						$addOboCCAry['fDestinationPrice'] = $this->fDestinationPrice ;
						$addOboCCAry['idDestinationCurrency'] = $this->idDestinationCurrency ;
						$this->addOBOCustomClearance($addOboCCAry);
						//$success = true;
					}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			$this->inactivateCustomClearance($this->idOriginWarehouse,$this->idDestinationWarehouse,2);
		}
		
		$kForwarder = new cForwarder();
		$idForwarder = $_SESSION['forwarder_id'];
		$kForwarder->load($idForwarder);
		
		if(($kForwarder->iAgreeTNC==1) && ($kForwarder->iUpdateRss==2))
		{
			
			$kConfig = new cConfig();
			$kWhsSearch = new cWHSSearch();
			
			$kWhsSearch->load($this->idOriginWarehouse);
			$szOriginCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
		
			$kWhsSearch->load($this->idDestinationWarehouse);
			$szDestinationCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
			
			$replace_ary = array();
			$replace_ary['szForwarderDisplayName'] = $kForwarder->szDisplayName ;
			$replace_ary['szCountryName'] = $szOriginCountry ;
			$replace_ary['szImportExportText'] = $szDirection ;
			$replace_ary['szOriginCountry'] = $szOriginCountry ;
			$replace_ary['szDestinationCountry'] = $szDestinationCountry ;
			
			$content_ary = array();
			$content_ary = $this->replaceRssContent('__OBO_CUSTOM_CLEARANCE__',$replace_ary);
			
			$rssDataAry=array();
			$rssDataAry['idForwarder'] = $kForwarder->id ;
			$rssDataAry['szFeedType'] = '__OBO_CUSTOM_CLEARANCE__';
			$rssDataAry['szLink'] = __RSS_FEED_PAGE_URL__ ;
			$rssDataAry['szTitle'] = $content_ary['szTitle'];
			$rssDataAry['szDescription'] = $content_ary['szDescription'];
			$rssDataAry['szDescriptionHTML'] = $content_ary['szDescriptionHTML'];
			
			$kRss = new cRSS();
			$kRss->addRssFeed($rssDataAry);
		}
		return true;
	}
	
	function addOBOCustomClearance($data)
	{
		//print_r($data);	
		$update_oring_cc = false ;
		$update_destination_cc = false ;
		$this->set_idOriginWarehouse(sanitize_all_html_input(trim($data['idOriginWarehouse'])));
		$this->set_idDestinationWarehouse(sanitize_all_html_input(trim($data['idDestinationWarehouse'])));
		
		if((float)$data['fOriginPrice']>0 || (int)$data['idOriginCurrency']>0)
		{
			$this->set_fOriginPrice(sanitize_all_html_input(trim($data['fOriginPrice'])));
			$this->set_idOriginCurrency(sanitize_all_html_input(trim($data['idOriginCurrency'])));
			//$this->
			$update_oring_cc = true;
		}		
		if((float)$data['fDestinationPrice']>0 || (int)$data['idDestinationCurrency']>0)
		{
			$this->set_fDestinationPrice(sanitize_all_html_input(trim($data['fDestinationPrice'])));
			$this->set_idDestinationCurrency(sanitize_all_html_input(trim($data['idDestinationCurrency'])));
			$update_destination_cc = true;
		}
		
		if($update_destination_cc==false && $update_oring_cc==false)
		{
			$this->addError('fDestinationPrice','At least one line should be filled');
			return false;
		}
		/*
		$this->set_fOriginPrice(sanitize_all_html_input(trim($data['fOriginPrice'])));
		$this->set_fDestinationPrice(sanitize_all_html_input(trim($data['fDestinationPrice'])));
		
		$this->set_idOriginCurrency(sanitize_all_html_input(trim($data['idOriginCurrency'])));
		$this->set_idDestinationCurrency(sanitize_all_html_input(trim($data['idDestinationCurrency'])));
		*/
		
		if($this->error === true)
		{
			return false;
		}
			
		if($this->idOriginWarehouse == $this->idDestinationWarehouse)
		{
			$this->addError('szDestinationWarehouse',t($this->t_base.'messages/same_whs_error_message'));
			return false;
		}
		$success = false;
		if($update_oring_cc)
		{
			$query="
				INSERT INTO
					".__DBC_SCHEMATA_PRICING_CC__."
				(
					idWarehouseFrom,
					idWarehouseTo,
					szOriginDestination,
					fPrice,
					szCurrency,
					iActive
				)	
				VALUES
				(
					'".(int)$this->idOriginWarehouse."',
					'".(int)$this->idDestinationWarehouse."',
					'1',
					'".(float)$this->fOriginPrice."',
					'".(float)$this->idOriginCurrency."',
					'1'
				)
			";
			//echo "<br>".$query."<br>";
			if($reusult=$this->exeSQL($query))
			{
				$success = true;
				$kForwarder = new cForwarder();
				$idForwarder = $_SESSION['forwarder_id'];
				$kForwarder->load($idForwarder);
				
				if(($kForwarder->iAgreeTNC==1) && ($kForwarder->iUpdateRss==2))
				{
					$kConfig = new cConfig();
					$kWhsSearch = new cWHSSearch();
					
					$kWhsSearch->load($this->idOriginWarehouse);
					$szOriginCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
					
					$kWhsSearch->load($this->idDestinationWarehouse);
					$szDestinationCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
					
					$replace_ary = array();
					$replace_ary['szForwarderDisplayName'] = $kForwarder->szDisplayName ;
					$replace_ary['szCountryName'] = $szOriginCountry ;
					$replace_ary['szOriginCountry'] = $szOriginCountry ;
					$replace_ary['szDestinationCountry'] = $szDestinationCountry ;
						
					$content_ary = array();
					$content_ary = $this->replaceRssContent('__OBO_CUSTOM_CLEARANCE__',$replace_ary);
						
					$rssDataAry=array();
					$rssDataAry['idForwarder'] = $kForwarder->id ;
					$rssDataAry['szFeedType'] = '__OBO_CUSTOM_CLEARANCE__';
					$rssDataAry['szLink'] = __RSS_FEED_PAGE_URL__ ;
					$rssDataAry['szTitle'] = $content_ary['szTitle'];
					$rssDataAry['szDescription'] = $content_ary['szDescription'];
					$rssDataAry['szDescriptionHTML'] = $content_ary['szDescriptionHTML'];
					
					$kRss = new cRSS();
					$kRss->addRssFeed($rssDataAry);
				}
				//return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		
		if($update_destination_cc)
		{
			$query="
				INSERT INTO
					".__DBC_SCHEMATA_PRICING_CC__."
				(
					idWarehouseFrom,
					idWarehouseTo,
					szOriginDestination,
					fPrice,
					szCurrency,
					iActive
				)	
				VALUES
				(
					'".(int)$this->idOriginWarehouse."',
					'".(int)$this->idDestinationWarehouse."',
					'2',
					'".(float)$this->fDestinationPrice."',
					'".(float)$this->idDestinationCurrency."',
					'1'
				)
			";
			//echo "<br>".$query."<br>";
			if($reusult=$this->exeSQL($query))
			{
				$success = true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		return $success;
	}
	
	function getDataForLCL($data,$idWTW=false)
	{
		if(!empty($data) || (int)$idWTW>0)
		{
			if((int)$idWTW>0)
			{
                            $this->loadPricingWtw($idWTW);
                            $this->idOriginWarehouse = $this->idWarehouseFrom ;
                            $this->idDestinationWarehouse = $this->idWarehouseTo ;
			}
			else
			{
                            $this->set_idOriginWarehouse(sanitize_all_html_input(trim($data['idOriginWarehouse'])));
                            $this->set_idOriginCountry(sanitize_all_html_input(trim($data['idOriginCountry'])));
                            $this->set_szOriginCity(sanitize_all_html_input(trim($data['szOriginCity'])));

                            $this->set_idDestinationWarehouse(sanitize_all_html_input(trim($data['idDestinationWarehouse'])));
                            $this->set_idDestinationCountry(sanitize_all_html_input(trim($data['idDestinationCountry'])));
                            $this->set_szDestinationCity(sanitize_all_html_input(trim($data['szDestinationCity'])));

                            if(!$this->isWarehousebelongsTocountry($this->idOriginWarehouse,$this->idOriginCountry,$this->idOriginCity))
                            {
                                $this->addError('szOriginWarehouse',t($this->t_base.'messages/origin_warehouse_information_invalid'));
                                return false;
                            }
                            $szOriginCountry = $this->szCountryName ;
                            if(!$this->isWarehousebelongsTocountry($this->idDestinationWarehouse,$this->idDestinationCountry,$this->idDestinationCity))
                            {
                                $this->addError('szDestinationWarehouse',t($this->t_base.'messages/destination_warehouse_information_invalid'));
                                return false;
                            }
                            $szDestinationCountry = $this->szCountryName ;
			}
			
			if($this->error==true)
			{
                            return false;
			}
			
			
			$query="
				SELECT
                                    wtw.id,
                                    wtw.idWarehouseFrom,
                                    wtw.idWarehouseTo,
                                    wtw.iBookingCutOffHours,
                                    wtw.idCutOffDay,
                                    wtw.szCutOffLocalTime,
                                    wtw.idAvailableDay,
                                    wtw.szAvailableLocalTime,
                                    wtw.iTransitHours,
                                    wtw.idFrequency,
                                    wtw.fFreightRateWM as fRateWM,
                                    wtw.fFreightMinRateWM as fMinRateWM,
                                    wtw.fFreightBookingRate as fRate,
                                    wtw.szFreightCurrency,
                                    wtw.dtValidFrom,
                                    wtw.dtExpiry,
                                    cur.szCurrency ,
                                    wtw.iTransitDays,
                                    wtw.iOriginChargesApplicable,
                                    wtw.iDestinationChargesApplicable,
                                    ( SELECT szWeekDay FROM ".__DBC_SCHEMATA_WEEK_DAYS__." wd WHERE wd.id = wtw.idAvailableDay ) szAvailableDay,
                                    ( SELECT szWeekDay FROM ".__DBC_SCHEMATA_WEEK_DAYS__." wd WHERE wd.id = wtw.idCutOffDay ) szCutOffDay,
                                    f.szDescription as szFrequency
				FROM
                                    ".__DBC_SCHEMATA_WAREHOUSES_PRICING__." wtw
				INNER JOIN
                                    ".__DBC_SCHEMATA_FREQUENCY__." f	
				ON
                                    f.id=wtw.idFrequency	
				INNER JOIN
                                    ".__DBC_SCHEMATA_CURRENCY__." cur
				ON
                                    cur.id = wtw.szFreightCurrency		
				WHERE
                                    idWarehouseFrom = '".(int)$this->idOriginWarehouse."'
				AND
                                    idWarehouseTo = '".(int)$this->idDestinationWarehouse."'	
				AND
                                    wtw.iActive = '1'	
				ORDER BY
				   wtw.idCutOffDay
			";
			//echo "<br>".$query."<br>";
			if($result=$this->exeSQL($query))
			{
                            $lclDataAry = array();
                            if($this->iNumRows>0)
                            {
                                $ctr = 0;
                                while($row=$this->getAssoc($result))
                                {
                                    $lclDataAry[$ctr] = $row;
                                    $ctr++;
                                }
                                return $lclDataAry ;
                            }
                            else
                            {
                                return $lclDataAry ;
                            }
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function loadPricingWtw($idPricingWTW)
	{
		if($idPricingWTW>0)
		{			
			$query="
				SELECT
					wtw.id,
					wtw.idWarehouseFrom,
					wtw.idWarehouseTo,
					wtw.iOriginChargesApplicable,
					wtw.iDestinationChargesApplicable,
					dtValidFrom,
					dtExpiry
				FROM
					".__DBC_SCHEMATA_WAREHOUSES_PRICING__." wtw
				WHERE
					wtw.id = '".(int)$idPricingWTW."'
				AND
					wtw.iActive = '1'	
			";
			//echo "<br>".$query."<br>";
			if($result=$this->exeSQL($query))
			{
				$lclDataAry = array();
				if($this->iNumRows>0)
				{
					$ctr = 0;
					$row=$this->getAssoc($result);
					$this->idPricingWtw = $row['id'];
					$this->idWarehouseFrom = $row['idWarehouseFrom'];
					$this->idWarehouseTo = $row['idWarehouseTo'];
					$this->iOriginChargesApplicable = $row['iOriginChargesApplicable'];
					$this->iDestinationChargesApplicable = $row['iDestinationChargesApplicable'];
					$this->dtValidFrom = $row['dtValidFrom'];
					$this->dtExpiry = $row['dtExpiry'];
					return true ;
				}
				else
				{
					return false ;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function getDataForLCLById($idPricingWTW = null)
	{
		if((int)$idPricingWTW>0)
		{
		$add_query = "	
					AND
						wtw.id = '".(int)$idPricingWTW."'
					";
		}
		else
		{
			$add_query = '';
		}			
		$query="
			SELECT
				wtw.id,
				wtw.idWarehouseFrom,
				wtw.idWarehouseTo,
				wtw.iBookingCutOffHours,
				wtw.idCutOffDay,
				wtw.szCutOffLocalTime,
				wtw.idAvailableDay,
				wtw.szAvailableLocalTime,
				wtw.iTransitHours,
				wtw.idFrequency,					
				wtw.fOriginChargeRateWM as fOriginRateWM,
				wtw.fOriginChargeMinRateWM as fOriginMinRateWM,
				wtw.fOriginChargeBookingRate as fOriginRate,
				wtw.szOriginChargeCurrency as szOriginRateCurrency,					
				wtw.fDestinationChargeRateWM as fDestinationRateWM,
				wtw.fDestinationChargeMinRateWM as fDestinationMinRateWM,
				wtw.fDestinationChargeBookingRate as fDestinationRate,
				wtw.szDestinationChargeCurrency as szDestinationRateCurrency,					
				wtw.fFreightRateWM as fRateWM,
				wtw.fFreightMinRateWM as fMinRateWM,
				wtw.fFreightBookingRate as fRate,
				wtw.szFreightCurrency,
				wtw.dtValidFrom,
				wtw.dtExpiry,
				wtw.iTransitDays,
				wtw.iOriginChargesApplicable,
				wtw.iDestinationChargesApplicable,
				( SELECT szWeekDay FROM ".__DBC_SCHEMATA_WEEK_DAYS__." wd WHERE wd.id = wtw.idAvailableDay ) szAvailableDay,
				( SELECT szWeekDay FROM ".__DBC_SCHEMATA_WEEK_DAYS__." wd WHERE wd.id = wtw.idCutOffDay ) szCutOffDay,
				f.szDescription as szFrequency
			FROM
				".__DBC_SCHEMATA_WAREHOUSES_PRICING__." wtw
			INNER JOIN
				".__DBC_SCHEMATA_FREQUENCY__." f	
			ON
				f.id=wtw.idFrequency	
			WHERE
				iActive = '1'	
			".$add_query."	
		";
		//echo "<br>".$query."<br>";
		if($result=$this->exeSQL($query))
		{
			$lclDataAry = array();
			if($this->iNumRows>0)
			{
				$ctr = 0;
				while($row=$this->getAssoc($result))
				{
					$lclDataAry[$ctr] = $row;
					$ctr++;
				}
				return $lclDataAry ;
			}
			else
			{
				return $lclDataAry ;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function getDataForLCLExpiredLastThreeMonth($idPricingWTW = null,$idForWarehouses=0)
	{
            $add_query="";
            if((int)$idPricingWTW>0)
            {
                $add_query = " AND wtw.id = '".(int)$idPricingWTW."' ";
            } 
            if((int)$idForWarehouses>0)
            {
                $add_query = " AND wtw.id IN (".$idForWarehouses.") ";
            }
            
            $query="
                SELECT
                    wtw.id,
                    fwd.szDisplayName,
                    w.szCity as szCityFrom,
                    wh.szCity as szCityTo, 
                    c1.szCountryISO AS ISOCodeFrom,
                    c2.szCountryISO AS ISOCodeTo,
                    wtw.idWarehouseFrom,
                    wtw.idWarehouseTo,
                    wtw.iBookingCutOffHours,
                    wtw.idCutOffDay,
                    wtw.szCutOffLocalTime,
                    wtw.idAvailableDay,
                    wtw.szAvailableLocalTime,
                    wtw.iTransitHours,
                    wtw.idFrequency,					
                    wtw.fOriginChargeRateWM as fOriginRateWM,
                    wtw.fOriginChargeMinRateWM as fOriginMinRateWM,
                    wtw.fOriginChargeBookingRate as fOriginRate,
                    wtw.szOriginChargeCurrency as szOriginRateCurrency,					
                    wtw.fDestinationChargeRateWM as fDestinationRateWM,
                    wtw.fDestinationChargeMinRateWM as fDestinationMinRateWM,
                    wtw.fDestinationChargeBookingRate as fDestinationRate,
                    wtw.szDestinationChargeCurrency as szDestinationRateCurrency,					
                    wtw.fFreightRateWM as fRateWM,
                    wtw.fFreightMinRateWM as fMinRateWM,
                    wtw.fFreightBookingRate as fRate,
                    wtw.szFreightCurrency,
                    wtw.dtValidFrom,
                    wtw.dtExpiry,
                    wtw.iTransitDays,
                    wtw.iOriginChargesApplicable,
                    wtw.iDestinationChargesApplicable,
                    ( SELECT szWeekDay FROM ".__DBC_SCHEMATA_WEEK_DAYS__." wd WHERE wd.id = wtw.idAvailableDay ) szAvailableDay,
                    ( SELECT szWeekDay FROM ".__DBC_SCHEMATA_WEEK_DAYS__." wd WHERE wd.id = wtw.idCutOffDay ) szCutOffDay,
                    f.szDescription as szFrequency
                FROM
                    ".__DBC_SCHEMATA_WAREHOUSES_PRICING__." AS wtw
                INNER JOIN
                    ".__DBC_SCHEMATA_FREQUENCY__." AS f	
                ON
                    f.id=wtw.idFrequency	
                INNER JOIN 
                    ".__DBC_SCHEMATA_WAREHOUSES__." AS w
                ON
                    w.id  = wtw.idWarehouseFrom	
                INNER JOIN 
                    ".__DBC_SCHEMATA_WAREHOUSES__." AS wh
                ON
                    wh.id  = wtw.idWarehouseTo	
                INNER JOIN 
                    ".__DBC_SCHEMATA_COUNTRY__." AS c1
                ON
                        w.idCountry  =	c1.id
                INNER JOIN 
                        ".__DBC_SCHEMATA_COUNTRY__." AS c2
                ON
                    wh.idCountry  =	c2.id		
                INNER JOIN
                    ".__DBC_SCHEMATA_FROWARDER__." AS fwd
                ON
                    fwd.id  = w.idForwarder				
                WHERE
                    wtw.dtExpiry> NOW() - INTERVAL 3 MONTH
                AND
                    wtw.iActive='1' 
                    ".$add_query."	
                ORDER BY 
                    fwd.szDisplayName ASC,
                    c1.szCountryISO ASC,
                    c2.szCountryISO ASC
            ";
            //echo "<br>".$query."<br>";die;
            if($result=$this->exeSQL($query))
            {
                $lclDataAry = array();
                if($this->iNumRows>0)
                {
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {
                        $lclDataAry[$ctr] = $row;
                        $ctr++;
                    }
                    return $lclDataAry ;
                }
                else
                {
                    return $lclDataAry ;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
        
	function updateOBOLCL($data)
	{
		if(!empty($data))
		{
			//(sanitize_all_html_input(trim($data['iOriginChargesApplicable']))==1) || 
			//(sanitize_all_html_input(trim($data['iDestinationChargesApplicable']))==1) || 
			$this->set_idOriginWarehouse(sanitize_all_html_input(trim($data['idOriginWarehouse'])));
			$this->set_idDestinationWarehouse(sanitize_all_html_input(trim($data['idDestinationWarehouse'])));
			
			if($data['fOriginRateWM']!='' || $data['fOriginMinRateWM']!='' || $data['fOriginRate']!='')
			{
				$this->set_fOriginRateWM(sanitize_all_html_input(trim($data['fOriginRateWM'])),true);
				$this->set_fOriginMinRateWM(sanitize_all_html_input(trim($data['fOriginMinRateWM'])),true);
				$this->set_fOriginRate(sanitize_all_html_input(trim($data['fOriginRate'])),true);
				$this->set_szOriginFreightCurrency(sanitize_all_html_input(trim($data['szOriginFreightCurrency'])),true);
				$data['iOriginChargesApplicable'] = 1 ;
			}
			else
			{
				$this->fOriginRateWM = 0;
				$this->fOriginMinRateWM = 0;
				$this->fOriginRate = 0;
				$this->szOriginFreightCurrency = 0;
				$data['iOriginChargesApplicable'] = 0 ;
			}
			$this->set_fRateWM(sanitize_all_html_input(trim($data['fRateWM'])));
			$this->set_fMinRateWM(sanitize_all_html_input(trim($data['fMinRateWM'])));
			$this->set_fRate(sanitize_all_html_input(trim($data['fRate'])));
			$this->set_szFreightCurrency(sanitize_all_html_input(trim($data['szFreightCurrency'])));
			
			if(($data['fDestinationRateWM']!='' || $data['fDestinationMinRateWM']!='' || $data['fDestinationRate']!='' ))
			{
				$this->set_fDestinationRateWM(sanitize_all_html_input(trim($data['fDestinationRateWM'])),true);
				$this->set_fDestinationMinRateWM(sanitize_all_html_input(trim($data['fDestinationMinRateWM'])),true);
				$this->set_fDestinationRate(sanitize_all_html_input(trim($data['fDestinationRate'])),true);
				$this->set_szDestinationFreightCurrency(sanitize_all_html_input(trim($data['szDestinationFreightCurrency'])),true);
				$data['iDestinationChargesApplicable'] = 1;
			}
			else
			{
				$this->fDestinationRateWM = 0;
				$this->fDestinationMinRateWM = 0;
				$this->fDestinationRate = 0;
				$this->szDestinationFreightCurrency = 0;
				$data['iDestinationChargesApplicable'] = 0;
			}
			
			$this->set_dtValidFrom(sanitize_all_html_input(trim($data['dtValidFrom'])));
			$this->set_dtExpiry(sanitize_all_html_input(trim($data['dtExpiry'])));
			$this->set_idFrequency(sanitize_all_html_input(trim($data['idFrequency'])));
			$this->set_iBookingCutOffHours(sanitize_all_html_input(trim($data['iBookingCutOffHours'])));
			$this->set_iTransitHours(sanitize_all_html_input(trim($data['iTransitHours'])));
			$this->set_idCutOffDay(sanitize_all_html_input(trim($data['idCutOffDay'])));
			$this->set_szCutOffLocalTime(sanitize_all_html_input(trim($data['szCutOffLocalTime'])));
			$this->set_idAvailableDay(sanitize_all_html_input(trim($data['idAvailableDay'])));
			$this->set_szAvailableLocalTime(sanitize_all_html_input(trim($data['szAvailableLocalTime'])));
			$this->set_id(sanitize_all_html_input(trim($data['id'])));
			if($this->error==true)
			{
				return false;
			}	
			if(!empty($this->dtValidFrom))
			{
				 $dateAry = explode("/",$this->dtValidFrom); 
				 $dtTiming_millisecond = strtotime($dateAry[2]."-".$dateAry[1]."-".$dateAry[0]);  // converting date 
				 $dtDate = date("d/m/Y",$dtTiming_millisecond);
				 $vaildity_timestam = $dtTiming_millisecond ;
				 if(!checkdate($dateAry[1],$dateAry[0],$dateAry[2]))
                                {
                                   $this->addError("dtValidFrom",t($this->t_base.'fields/validity')." ".t($this->t_base.'fields/from')." ".t('Error/valid_date_landing_page'));
                                   return false;
                                }
                                else if(($vaildity_timestam < strtotime(date('Y-m-d'))))
                                {
                                    $this->dtValidFrom =date('Y-m-d');
                                   //$this->addError("dtValidFrom",t($this->t_base.'fields/validity')." ".t($this->t_base.'fields/from')." ".t('Error/must_be_greater_than_today'));
                                   //return false;
                                }
                                else
                                {
                                   $this->dtValidFrom = $dateAry[2]."-".$dateAry[1]."-".$dateAry[0]." 00:00:00";  // YYYY/MM/DD H:I:S
                                }
			}	
			if(!empty($this->dtExpiry))
			{
				 $dateAry = explode("/",$this->dtExpiry); 
				 $dtTiming_millisecond = strtotime($dateAry[2]."-".$dateAry[1]."-".$dateAry[0]);  // converting date 
				 $dtDate = date("d/m/Y",$dtTiming_millisecond);
				 	 
				 if(!checkdate($dateAry[1],$dateAry[0],$dateAry[2]))
	             {
	                $this->addError("dtExpiry",t($this->t_base.'fields/validity')." ".t($this->t_base.'fields/to')." ".t('Error/valid_date_landing_page'));
	                return false;
	             }
				 else if(($dtTiming_millisecond < strtotime(date('Y-m-d'))))
	             {
	             	$this->addError("dtExpiry",t($this->t_base.'fields/validity')." ".t($this->t_base.'fields/to')." ".t('Error/must_be_greater_than_today'));
	             	return false;
	             }
				 else if($vaildity_timestam > $dtTiming_millisecond)
	             {
	             	$this->addError("dtExpiry",t($this->t_base.'messages/validity_from_date_should_be_less_than_expiry_date'));
	             	return false;
	             }
	             else
	             {
	             	$this->dtExpiry = $dateAry[2]."-".$dateAry[1]."-".$dateAry[0]." 00:00:00";  // YYYY/MM/DD H:I:S
	             }
			}
			
			$iAvailableDay = $this->getAvailableDay($this->iTransitHours,$this->idCutOffDay);
			if($iAvailableDay!=$this->idAvailableDay)
			{
				$this->addError('szDestinationWarehouse',t($this->t_base.'messages/invalid_available_day'));
				return false;
			}

			if($this->isPricingAlreadyExists($this->idOriginWarehouse,$this->idDestinationWarehouse,$this->idCutOffDay,$this->id,$this->dtExpiry))
			{
				$this->addError('pricing',t($this->t_base.'messages/pricing_already_exists_for_cut_off_day'));
				return false;
			}
			
			if($this->idOriginWarehouse == $this->idDestinationWarehouse)
			{
				$this->addError('szDestinationWarehouse',t($this->t_base.'messages/same_whs_error_message'));
				return false;
			}
			
			$kWhs_origin=new cWHSSearch();
			$kWhs_destination = new cWHSSearch();
			
			$kWhs_origin->load($this->idOriginWarehouse);
			$kWhs_destination->load($this->idDestinationWarehouse);
			
			$cutoffTimeAry = explode(':',$this->szCutOffLocalTime);
			$cutoffTime = $cutoffTimeAry[0]+($cutoffTimeAry[1]/60) ;
			
			$availabelTimeAry = explode(':',$this->szAvailableLocalTime);
			$availabelTime = $availabelTimeAry[0]+($availabelTimeAry[1]/60) ; 
			
			$transitHours = (int)(($this->iTransitHours * 24) + ($availabelTime - $cutoffTime ) +(($kWhs_origin->szUTCOffset - $kWhs_destination->szUTCOffset)/(1000*60*60)));
			$success = false;
			$pricingWTWAry = array();
			$pricingWTWAry = $this->getDataForLCLById($this->id);
			if(!empty($pricingWTWAry[0]))
			{
				$pricingWTWArys = $pricingWTWAry[0] ;
				$add_archived_flag = true;
				if($pricingWTWArys['iBookingCutOffHours'] !=$this->iBookingCutOffHours)
				{
					$add_archived_flag = true;
				}
				else if($pricingWTWArys['idCutOffDay'] != $this->idCutOffDay)
				{
					$add_archived_flag = true;
				}
				else if($pricingWTWArys['szCutOffLocalTime'] != $this->szCutOffLocalTime)
				{
					$add_archived_flag = true;
				}				
				else if($pricingWTWArys['idAvailableDay'] != $this->idAvailableDay)
				{
					$add_archived_flag = true;
				}
				else if($pricingWTWArys['szAvailableLocalTime'] != $this->szAvailableLocalTime)
				{
					$add_archived_flag = true;
				}
				
			    elseif($pricingWTWArys['iTransitHours'] !=$this->iTransitHours)
				{
					$add_archived_flag = true;
				}
				else if($pricingWTWArys['idFrequency'] != $this->idFrequency)
				{
					$add_archived_flag = true;
				}
				else if($pricingWTWArys['fRateWM'] != $this->fRateWM)
				{
					$add_archived_flag = true;
				}				
				else if($pricingWTWArys['fMinRateWM'] != $this->fMinRateWM)
				{
					$add_archived_flag = true;
				}
				else if($pricingWTWArys['fRate'] != $this->fRate)
				{
					$add_archived_flag = true;
				}
				else if($pricingWTWArys['szFreightCurrency'] != $this->szFreightCurrency)
				{
					$add_archived_flag = true;
				}
				
				if($add_archived_flag)
				{
					if((int)$_SESSION['forwarder_admin_id']>0)
					{
						$idAdmin = (int)$_SESSION['forwarder_admin_id'];
						$idForwarderContact = 0 ;
					}
					else if((int)$_SESSION['forwarder_user_id']>0)
					{
						$idAdmin = 0;
						$idForwarderContact = (int)$_SESSION['forwarder_user_id'];
					}
					elseif((int)$_SESSION['admin_id']>0)
					{
						$idAdmin = (int)$_SESSION['admin_id'];
						$idForwarderContact = 0;
					}
					
					$query="
						INSERT INTO
							".__DBC_SCHEMATA_FORWARDER_WTW_ARCHIVE__."
							(
								idWarehouseFrom,
								idWarehouseTo,
								iBookingCutOffHours,
								idCutOffDay,
								szCutOffLocalTime,
								idAvailableDay,
								szAvailableLocalTime,
								iTransitHours,
								idFrequency,								
								fOriginChargeRateWM,
								fOriginChargeMinRateWM,
								fOriginChargeBookingRate,
								szOriginChargeCurrency,								
								fFreightRateWM,
								fFreightMinRateWM,
								fFreightBookingRate,
								szFreightCurrency,								
								fDestinationChargeRateWM,
								fDestinationChargeMinRateWM,
								fDestinationChargeBookingRate,
								szDestinationChargeCurrency,								
								dtValidFrom,
								dtExpiry,
								idForwarder,
								idForwarderContact,
								idAdmin,
								dtUploaded
								
							)
							VALUES
							(
								'".(int)$pricingWTWArys['idWarehouseFrom']."',
								'".(int)$pricingWTWArys['idWarehouseTo']."',
								'".mysql_escape_custom($pricingWTWArys['iBookingCutOffHours'])."',
								'".mysql_escape_custom($pricingWTWArys['idCutOffDay'])."',
								'".mysql_escape_custom($pricingWTWArys['szCutOffLocalTime'])."',
								'".mysql_escape_custom($pricingWTWArys['idAvailableDay'])."',
								'".mysql_escape_custom($pricingWTWArys['szAvailableLocalTime'])."',
								'".mysql_escape_custom($pricingWTWArys['iTransitHours'])."',
								'".mysql_escape_custom($pricingWTWArys['idFrequency'])."',
								'".(float)$pricingWTWArys['fOriginRateWM']."',
								'".(float)$pricingWTWArys['fOriginMinRateWM']."',
								'".(float)$pricingWTWArys['fOriginRate']."',
								'".(int)$pricingWTWArys['szOriginFreightCurrency']."',								
								'".(float)$pricingWTWArys['fRateWM']."',
								'".(float)$pricingWTWArys['fMinRateWM']."',
								'".(float)$pricingWTWArys['fRate']."',
								'".(int)$pricingWTWArys['szFreightCurrency']."',								
								'".(float)$pricingWTWArys['fDestinationRateWM']."',
								'".(float)$pricingWTWArys['fDestinationMinRateWM']."',
								'".(float)$pricingWTWArys['fDestinationRate']."',
								'".(int)$pricingWTWArys['szDestinationFreightCurrency']."',								
								'".mysql_escape_custom($pricingWTWArys['dtValidFrom'])."',
								'".mysql_escape_custom($pricingWTWArys['dtExpiry'])."',
								'".(int)$_SESSION['forwarder_id']."',
								'".(int)$idForwarderContact."',
								'".(int)$idAdmin."',
								NOW()						
							)
						";
						$result=$this->exeSQL($query);
					}
				}	
			
				$query="
					UPDATE
						".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
					SET
						fFreightRateWM = '".(float)$this->fRateWM."',
						fFreightMinRateWM = '".(float)$this->fMinRateWM."',
						fFreightBookingRate = '".(float)$this->fRate."',
						szFreightCurrency = '".mysql_escape_custom(trim($this->szFreightCurrency))."',						
						fOriginChargeRateWM = '".(float)$this->fOriginRateWM."',
						fOriginChargeMinRateWM = '".(float)$this->fOriginMinRateWM."',
						fOriginChargeBookingRate = '".(float)$this->fOriginRate."',
						szOriginChargeCurrency = '".mysql_escape_custom(trim($this->szOriginFreightCurrency))."',						
						fDestinationChargeRateWM = '".(float)$this->fDestinationRateWM."',
						fDestinationChargeMinRateWM = '".(float)$this->fDestinationMinRateWM."',
						fDestinationChargeBookingRate = '".(float)$this->fDestinationRate."',
						szDestinationChargeCurrency = '".mysql_escape_custom(trim($this->szDestinationFreightCurrency))."',						
						dtValidFrom = '".mysql_escape_custom(trim($this->dtValidFrom))."',				
						dtExpiry = '".mysql_escape_custom(trim($this->dtExpiry))."',
						idFrequency = '".mysql_escape_custom(trim($this->idFrequency))."',
						iBookingCutOffHours = '".mysql_escape_custom(trim($this->iBookingCutOffHours))."',
						iTransitHours = '".(int)mysql_escape_custom(trim($transitHours))."',				
						idCutOffDay = '".mysql_escape_custom(trim($this->idCutOffDay))."',
						szCutOffLocalTime = '".mysql_escape_custom(trim($this->szCutOffLocalTime))."',
						idAvailableDay = '".mysql_escape_custom(trim($this->idAvailableDay))."',
						szAvailableLocalTime = '".mysql_escape_custom(trim($this->szAvailableLocalTime))."',
						dtUpdatedOn = now(),
						iTransitDays = '".(int)$this->iTransitHours."',
						iDestinationChargesApplicable = '".(int)$data['iDestinationChargesApplicable']."',
						iOriginChargesApplicable = '".(int)$data['iOriginChargesApplicable']."'
					WHERE
						idWarehouseFrom = '".(int)$this->idOriginWarehouse."'	
					AND
						idWarehouseTo = '".(int)$this->idDestinationWarehouse."'	
					AND
						id = '".(int)$this->id."'	
					AND
						iActive = '1'	
				";
				//echo "<br>".$query."<br>";
				//die;
				if($reusult=$this->exeSQL($query))
				{
					$success = true;		
					$kForwarder = new cForwarder();
					$idForwarder = $_SESSION['forwarder_id'];
					$kForwarder->load($idForwarder);
					
					if(($kForwarder->iAgreeTNC==1) && ($kForwarder->iUpdateRss==2))
					{
						$kConfig = new cConfig();
						$kWhsSearch = new cWHSSearch();
						
						$kWhsSearch->load($this->idOriginWarehouse);
						$szOriginCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
						
						$kWhsSearch->load($this->idDestinationWarehouse);
						$szDestinationCountry = $kConfig->getCountryName($kWhsSearch->idCountry);

						$replace_ary = array();
						$replace_ary['szForwarderDisplayName'] = $kForwarder->szDisplayName ;
						$replace_ary['szOriginCountry'] = $szOriginCountry ;
						$replace_ary['szDestinationCountry'] = $szDestinationCountry ;
						
						$content_ary = array();
						$content_ary = $this->replaceRssContent('__OBO_LCL_SERVICES__',$replace_ary);
						
						$rssDataAry=array();
						$rssDataAry['idForwarder'] = $kForwarder->id ;
						$rssDataAry['szFeedType'] = '__OBO_LCL_SERVICES__';
						$rssDataAry['szLink'] = __RSS_FEED_PAGE_URL__ ;
						$rssDataAry['szTitle'] = $content_ary['szTitle'];
						$rssDataAry['szDescription'] = $content_ary['szDescription'];
						$rssDataAry['szDescriptionHTML'] = $content_ary['szDescriptionHTML'];
						
						$kRss = new cRSS();
						$kRss->addRssFeed($rssDataAry);
					}			
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
				return $success;
		}
		else
		{
			return false;
		}
	}
	
	function replaceRssContent($szFeedType,$replace_ary)
	{
		if(!empty($szFeedType))
		{
			$query = "
		        SELECT
		            szTitle,
		            szDescription,
					szDescriptionHTML
		        FROM
		            ".__DBC_SCHEMATA_RSS_TEMPLATES__."
		        WHERE
		            szFeedType = '".mysql_escape_custom($szFeedType)."'
		    ";
			
			if ($result = $this->exeSQL($query))
			{
				if ($this->iNumRows > 0)
				{
					$row = $this->getAssoc($result);
					
					$szTitle = $row['szTitle'];
					$szDescription = $row['szDescription'];
					$szDescriptionHTML = $row['szDescriptionHTML'];
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "input", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
			 
			if (!empty($replace_ary))
			{
				foreach ($replace_ary as $replace_key => $replace_value)
				{
					$szTitle = str_replace($replace_key, $replace_value, $szTitle);
					$szDescription= str_replace($replace_key, $replace_value, $szDescription);
					$szDescriptionHTML= str_replace($replace_key, $replace_value, $szDescriptionHTML);
				}
			}
			
			$ret_ary = array();
			$ret_ary['szTitle'] = $szTitle;
			$ret_ary['szDescription'] = $szDescription;
			$ret_ary['szDescriptionHTML'] = $szDescriptionHTML;			
			return $ret_ary ;
		}
	}
	
	function addOBOLCL($data)
	{
		if(!empty($data))
		{
			$this->set_idOriginWarehouse(sanitize_all_html_input(trim($data['idOriginWarehouse'])));
			$this->set_idDestinationWarehouse(sanitize_all_html_input(trim($data['idDestinationWarehouse'])));
			
			if(sanitize_all_html_input(trim($data['iOriginChargesApplicable']))==1)
			{
				$this->set_fOriginRateWM(sanitize_all_html_input(trim($data['fOriginRateWM'])),true);
				$this->set_fOriginMinRateWM(sanitize_all_html_input(trim($data['fOriginMinRateWM'])),true);
				$this->set_fOriginRate(sanitize_all_html_input(trim($data['fOriginRate'])),true);
				$this->set_szOriginFreightCurrency(sanitize_all_html_input(trim($data['szOriginFreightCurrency'])),true);
			}
			else
			{
				$this->fOriginRateWM = 0;
				$this->fOriginMinRateWM = 0;
				$this->fOriginRate = 0;
				$this->szOriginFreightCurrency = 0;
			}
			
			$this->set_fRateWM(sanitize_all_html_input(trim($data['fRateWM'])));
			$this->set_fMinRateWM(sanitize_all_html_input(trim($data['fMinRateWM'])));
			$this->set_fRate(sanitize_all_html_input(trim($data['fRate'])));
			$this->set_szFreightCurrency(sanitize_all_html_input(trim($data['szFreightCurrency'])));
			
			if(sanitize_all_html_input(trim($data['iDestinationChargesApplicable']))==1)
			{
				$this->set_fDestinationRateWM(sanitize_all_html_input(trim($data['fDestinationRateWM'])),true);
				$this->set_fDestinationMinRateWM(sanitize_all_html_input(trim($data['fDestinationMinRateWM'])),true);
				$this->set_fDestinationRate(sanitize_all_html_input(trim($data['fDestinationRate'])),true);
				$this->set_szDestinationFreightCurrency(sanitize_all_html_input(trim($data['szDestinationFreightCurrency'])),true);
			}
			else
			{
				$this->fDestinationRateWM = 0;
				$this->fDestinationMinRateWM = 0;
				$this->fDestinationRate = 0;
				$this->szDestinationFreightCurrency = 0;
			}
			
			$this->set_dtValidFrom(sanitize_all_html_input(trim($data['dtValidFrom'])));
			$this->set_dtExpiry(sanitize_all_html_input(trim($data['dtExpiry'])));
			$this->set_idFrequency(sanitize_all_html_input(trim($data['idFrequency'])));
			$this->set_iBookingCutOffHours(sanitize_all_html_input(trim($data['iBookingCutOffHours'])));
			$this->set_iTransitHours(sanitize_all_html_input(trim($data['iTransitHours'])));
			$this->set_idCutOffDay(sanitize_all_html_input(trim($data['idCutOffDay'])));
			$this->set_szCutOffLocalTime(sanitize_all_html_input(trim($data['szCutOffLocalTime'])));
			$this->set_idAvailableDay(sanitize_all_html_input(trim($data['idAvailableDay'])));
			$this->set_szAvailableLocalTime(sanitize_all_html_input(trim($data['szAvailableLocalTime'])));
			
			if($this->error==true)
			{
				return false;
			}
				
			if(!$this->isWarehouseBelongsToForwarder($this->idOriginWarehouse,$this->idDestinationWarehouse))
			{
				return false;
			}

			
			
			
			if(!empty($this->dtValidFrom))
			{
				 $dateAry = explode("/",$this->dtValidFrom); 
				 $dtTiming_millisecond = strtotime($dateAry[2]."-".$dateAry[1]."-".$dateAry[0]);  // converting date 
				 $dtDate = date("d/m/Y",$dtTiming_millisecond);
				 $vaildity_timestam = $dtTiming_millisecond ;	 
				 if(!checkdate($dateAry[1],$dateAry[0],$dateAry[2]))
	             {
	                $this->addError("dtValidFrom",t($this->t_base.'fields/validity')." ".t($this->t_base.'fields/from')." ".t('Error/valid_date_landing_page'));
	                return false;
	             }
			 	 else if(($vaildity_timestam < strtotime(date('Y-m-d'))))
	             {
	             	$this->addError("dtValidFrom",t($this->t_base.'fields/validity')." ".t($this->t_base.'fields/from')." ".t('Error/must_be_greater_than_today'));
	             	return false;
	             }
	             else
	             {
	             	$this->dtValidFrom = $dateAry[2]."-".$dateAry[1]."-".$dateAry[0]." 00:00:00";  // YYYY/MM/DD H:I:S
	             }
			}	
			if(!empty($this->dtExpiry))
			{
				 $dateAry = explode("/",$this->dtExpiry); 
				 $dtTiming_millisecond = strtotime($dateAry[2]."-".$dateAry[1]."-".$dateAry[0]);  // converting date 
				 $dtDate = date("d/m/Y",$dtTiming_millisecond);
				 	 
				 if(!checkdate($dateAry[1],$dateAry[0],$dateAry[2]))
	             {
	                $this->addError("dtExpiry",t($this->t_base.'fields/validity')." ".t($this->t_base.'fields/to')." ".t('Error/valid_date_landing_page'));
	                return false;
	             }
				 else if(($dtTiming_millisecond < strtotime(date('Y-m-d'))))
	             {
	             	$this->addError("dtExpiry",t($this->t_base.'fields/validity')." ".t($this->t_base.'fields/to')." ".t('Error/must_be_greater_than_today'));
	             	return false;
	             }
	             else if($vaildity_timestam > $dtTiming_millisecond)
	             {
	             	$this->addError("dtExpiry",t($this->t_base.'messages/validity_from_date_should_be_less_than_expiry_date'));
	             	return false;
	             }
	             else
	             {
	             	$this->dtExpiry = $dateAry[2]."-".$dateAry[1]."-".$dateAry[0]." 00:00:00";  // YYYY/MM/DD H:I:S
	             }
			}
			$iAvailableDay = $this->getAvailableDay($this->iTransitHours,$this->idCutOffDay);
			if($iAvailableDay!=$this->idAvailableDay)
			{
				$this->addError('szDestinationWarehouse',t($this->t_base.'messages/invalid_available_day'));
				return false;
			}
			
			if($this->isPricingAlreadyExists($this->idOriginWarehouse,$this->idDestinationWarehouse,$this->idCutOffDay,0,$this->dtExpiry))
			{
				$this->addError('pricing',t($this->t_base.'messages/pricing_already_exists_for_cut_off_day'));
				return false;
			}
			
			if($this->idOriginWarehouse == $this->idDestinationWarehouse)
			{
				$this->addError('szDestinationWarehouse',t($this->t_base.'messages/same_whs_error_message'));
				return false;
			}
			
			$kWhs_origin=new cWHSSearch();
			$kWhs_destination = new cWHSSearch();
			
			$kWhs_origin->load($this->idOriginWarehouse);
			$kWhs_destination->load($this->idDestinationWarehouse);
			
			$cutoffTimeAry = explode(':',$this->szCutOffLocalTime);
			$cutoffTime = $cutoffTimeAry[0]+($cutoffTimeAry[1]/60) ;
			
			$availabelTimeAry = explode(':',$this->szAvailableLocalTime);
			$availabelTime = $availabelTimeAry[0]+($availabelTimeAry[1]/60) ; 
			
			$transitHours = (int)(($this->iTransitHours * 24) + ($availabelTime - $cutoffTime ) +(($kWhs_origin->szUTCOffset - $kWhs_destination->szUTCOffset)/(1000*60*60)));
			$success = false;
                        
                        $insertFlag=true;
                        
                        $query="
                            SELECT
                                id
                            FROM
                                ".__DBC_SCHEMATA_WAREHOUSES_PRICING__."	
                            WHERE
                                idWarehouseFrom = '".(int)$this->idOriginWarehouse."'	
                            AND
                                idWarehouseTo = '".(int)$this->idDestinationWarehouse."'	
                            AND
                                idCutOffDay = '".(int)$this->idCutOffDay."'
                            AND
                                dtExpiry = '".mysql_escape_custom(trim($this->dtExpiry))."'
			";
                        //echo "<br>".$query."<br>";
                        if($result=$this->exeSQL($query))
                        {
                            if($this->iNumRows>0)
                            {
                                $row = $this->getAssoc($result);
                                $this->id = $row['id'] ;
                                $insertFlag=false;
                            }
                        }
			if($insertFlag)
                        {
                            $query="
                                    INSERT INTO
                                            ".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
                                    (
                                            idWarehouseFrom,
                                            idWarehouseTo,					
                                            fOriginChargeRateWM,
                                            fOriginChargeMinRateWM,
                                            fOriginChargeBookingRate,
                                            szOriginChargeCurrency,								
                                            fFreightRateWM,
                                            fFreightMinRateWM,
                                            fFreightBookingRate,
                                            szFreightCurrency,								
                                            fDestinationChargeRateWM,
                                            fDestinationChargeMinRateWM,
                                            fDestinationChargeBookingRate,
                                            szDestinationChargeCurrency,
                                            dtValidFrom,
                                            dtExpiry,
                                            idFrequency,
                                            iBookingCutOffHours,
                                            iTransitHours,
                                            iTransitDays,
                                            idCutOffDay,
                                            szCutOffLocalTime,
                                            idAvailableDay,
                                            szAvailableLocalTime,
                                            dtCreatedOn,
                                            iActive,
                                            iOriginChargesApplicable,
                                            iDestinationChargesApplicable
                                    )
                                    VALUES
                                    (
                                             '".(int)$this->idOriginWarehouse."',
                                             '".(int)$this->idDestinationWarehouse."',	
                                              '".(float)$this->fOriginRateWM."',
                                             '".(float)$this->fOriginMinRateWM."',
                                             '".(float)$this->fOriginRate."',
                                             '".mysql_escape_custom(trim($this->szOriginFreightCurrency))."',					 
                                             '".(float)$this->fRateWM."',
                                             '".(float)$this->fMinRateWM."',
                                             '".(float)$this->fRate."',
                                             '".mysql_escape_custom(trim($this->szFreightCurrency))."',					 
                                             '".(float)$this->fDestinationRateWM."',
                                             '".(float)$this->fDestinationMinRateWM."',
                                             '".(float)$this->fDestinationRate."',
                                             '".mysql_escape_custom(trim($this->szDestinationFreightCurrency))."',					 
                                             '".mysql_escape_custom(trim($this->dtValidFrom))."',				
                                             '".mysql_escape_custom(trim($this->dtExpiry))."',
                                             '".mysql_escape_custom(trim($this->idFrequency))."',
                                             '".mysql_escape_custom(trim($this->iBookingCutOffHours))."',
                                             '".(int)mysql_escape_custom(trim($transitHours))."',	
                                             '".(int)mysql_escape_custom(trim($this->iTransitHours))."',	
                                             '".mysql_escape_custom(trim($this->idCutOffDay))."',
                                             '".mysql_escape_custom(trim($this->szCutOffLocalTime))."',
                                             '".mysql_escape_custom(trim($this->idAvailableDay))."',
                                             '".mysql_escape_custom(trim($this->szAvailableLocalTime))."',
                                             now(),
                                             '1',
                                             '".(int)$data['iOriginChargesApplicable']."',
                                             '".(int)$data['iDestinationChargesApplicable']."'
                                 ) 
                            ";
                        }
                        else
                        {
                            $pricingWTWAry = array();
                            $pricingWTWAry = $this->getDataForLCLById($this->id);
                            if(!empty($pricingWTWAry[0]))
                            {
                                $pricingWTWArys = $pricingWTWAry[0] ;
                                $add_archived_flag = true;
                                if($pricingWTWArys['iBookingCutOffHours'] !=$this->iBookingCutOffHours)
                                {
                                        $add_archived_flag = true;
                                }
                                else if($pricingWTWArys['idCutOffDay'] != $this->idCutOffDay)
                                {
                                        $add_archived_flag = true;
                                }
                                else if($pricingWTWArys['szCutOffLocalTime'] != $this->szCutOffLocalTime)
                                {
                                        $add_archived_flag = true;
                                }				
                                else if($pricingWTWArys['idAvailableDay'] != $this->idAvailableDay)
                                {
                                        $add_archived_flag = true;
                                }
                                else if($pricingWTWArys['szAvailableLocalTime'] != $this->szAvailableLocalTime)
                                {
                                        $add_archived_flag = true;
                                }

                                elseif($pricingWTWArys['iTransitHours'] !=$this->iTransitHours)
                                {
                                        $add_archived_flag = true;
                                }
                                else if($pricingWTWArys['idFrequency'] != $this->idFrequency)
                                {
                                        $add_archived_flag = true;
                                }
                                else if($pricingWTWArys['fRateWM'] != $this->fRateWM)
                                {
                                        $add_archived_flag = true;
                                }				
                                else if($pricingWTWArys['fMinRateWM'] != $this->fMinRateWM)
                                {
                                        $add_archived_flag = true;
                                }
                                else if($pricingWTWArys['fRate'] != $this->fRate)
                                {
                                        $add_archived_flag = true;
                                }
                                else if($pricingWTWArys['szFreightCurrency'] != $this->szFreightCurrency)
                                {
                                        $add_archived_flag = true;
                                }

                            if($add_archived_flag)
                            {
                                if((int)$_SESSION['forwarder_admin_id']>0)
                                {
                                        $idAdmin = (int)$_SESSION['forwarder_admin_id'];
                                        $idForwarderContact = 0 ;
                                }
                                else if((int)$_SESSION['forwarder_user_id']>0)
                                {
                                        $idAdmin = 0;
                                        $idForwarderContact = (int)$_SESSION['forwarder_user_id'];
                                }
                                elseif((int)$_SESSION['admin_id']>0)
                                {
                                        $idAdmin = (int)$_SESSION['admin_id'];
                                        $idForwarderContact = 0;
                                }

                                    $query="
                                        INSERT INTO
                                            ".__DBC_SCHEMATA_FORWARDER_WTW_ARCHIVE__."
                                        (
                                            idWarehouseFrom,
                                            idWarehouseTo,
                                            iBookingCutOffHours,
                                            idCutOffDay,
                                            szCutOffLocalTime,
                                            idAvailableDay,
                                            szAvailableLocalTime,
                                            iTransitHours,
                                            idFrequency,								
                                            fOriginChargeRateWM,
                                            fOriginChargeMinRateWM,
                                            fOriginChargeBookingRate,
                                            szOriginChargeCurrency,								
                                            fFreightRateWM,
                                            fFreightMinRateWM,
                                            fFreightBookingRate,
                                            szFreightCurrency,								
                                            fDestinationChargeRateWM,
                                            fDestinationChargeMinRateWM,
                                            fDestinationChargeBookingRate,
                                            szDestinationChargeCurrency,								
                                            dtValidFrom,
                                            dtExpiry,
                                            idForwarder,
                                            idForwarderContact,
                                            idAdmin,
                                            dtUploaded

                                        )
                                        VALUES
                                        (
                                            '".(int)$pricingWTWArys['idWarehouseFrom']."',
                                            '".(int)$pricingWTWArys['idWarehouseTo']."',
                                            '".mysql_escape_custom($pricingWTWArys['iBookingCutOffHours'])."',
                                            '".mysql_escape_custom($pricingWTWArys['idCutOffDay'])."',
                                            '".mysql_escape_custom($pricingWTWArys['szCutOffLocalTime'])."',
                                            '".mysql_escape_custom($pricingWTWArys['idAvailableDay'])."',
                                            '".mysql_escape_custom($pricingWTWArys['szAvailableLocalTime'])."',
                                            '".mysql_escape_custom($pricingWTWArys['iTransitHours'])."',
                                            '".mysql_escape_custom($pricingWTWArys['idFrequency'])."',
                                            '".(float)$pricingWTWArys['fOriginRateWM']."',
                                            '".(float)$pricingWTWArys['fOriginMinRateWM']."',
                                            '".(float)$pricingWTWArys['fOriginRate']."',
                                            '".(int)$pricingWTWArys['szOriginFreightCurrency']."',								
                                            '".(float)$pricingWTWArys['fRateWM']."',
                                            '".(float)$pricingWTWArys['fMinRateWM']."',
                                            '".(float)$pricingWTWArys['fRate']."',
                                            '".(int)$pricingWTWArys['szFreightCurrency']."',								
                                            '".(float)$pricingWTWArys['fDestinationRateWM']."',
                                            '".(float)$pricingWTWArys['fDestinationMinRateWM']."',
                                            '".(float)$pricingWTWArys['fDestinationRate']."',
                                            '".(int)$pricingWTWArys['szDestinationFreightCurrency']."',								
                                            '".mysql_escape_custom($pricingWTWArys['dtValidFrom'])."',
                                            '".mysql_escape_custom($pricingWTWArys['dtExpiry'])."',
                                            '".(int)$_SESSION['forwarder_id']."',
                                            '".(int)$idForwarderContact."',
                                            '".(int)$idAdmin."',
                                            NOW()						
                                        )
                                    ";
                                    $result=$this->exeSQL($query);
                                }
                            }
                            
                            $query="
                                UPDATE
                                        ".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
                                SET
                                        fFreightRateWM = '".(float)$this->fRateWM."',
                                        fFreightMinRateWM = '".(float)$this->fMinRateWM."',
                                        fFreightBookingRate = '".(float)$this->fRate."',
                                        szFreightCurrency = '".mysql_escape_custom(trim($this->szFreightCurrency))."',						
                                        fOriginChargeRateWM = '".(float)$this->fOriginRateWM."',
                                        fOriginChargeMinRateWM = '".(float)$this->fOriginMinRateWM."',
                                        fOriginChargeBookingRate = '".(float)$this->fOriginRate."',
                                        szOriginChargeCurrency = '".mysql_escape_custom(trim($this->szOriginFreightCurrency))."',						
                                        fDestinationChargeRateWM = '".(float)$this->fDestinationRateWM."',
                                        fDestinationChargeMinRateWM = '".(float)$this->fDestinationMinRateWM."',
                                        fDestinationChargeBookingRate = '".(float)$this->fDestinationRate."',
                                        szDestinationChargeCurrency = '".mysql_escape_custom(trim($this->szDestinationFreightCurrency))."',						
                                        dtValidFrom = '".mysql_escape_custom(trim($this->dtValidFrom))."',				
                                        dtExpiry = '".mysql_escape_custom(trim($this->dtExpiry))."',
                                        idFrequency = '".mysql_escape_custom(trim($this->idFrequency))."',
                                        iBookingCutOffHours = '".mysql_escape_custom(trim($this->iBookingCutOffHours))."',
                                        iTransitHours = '".(int)mysql_escape_custom(trim($transitHours))."',				
                                        idCutOffDay = '".mysql_escape_custom(trim($this->idCutOffDay))."',
                                        szCutOffLocalTime = '".mysql_escape_custom(trim($this->szCutOffLocalTime))."',
                                        idAvailableDay = '".mysql_escape_custom(trim($this->idAvailableDay))."',
                                        szAvailableLocalTime = '".mysql_escape_custom(trim($this->szAvailableLocalTime))."',
                                        dtUpdatedOn = now(),
                                        iTransitDays = '".(int)$this->iTransitHours."',
                                        iDestinationChargesApplicable = '".(int)$data['iDestinationChargesApplicable']."',
                                        iOriginChargesApplicable = '".(int)$data['iOriginChargesApplicable']."',
                                        iActive = '1'    
                                    WHERE
                                            idWarehouseFrom = '".(int)$this->idOriginWarehouse."'	
                                    AND
                                            idWarehouseTo = '".(int)$this->idDestinationWarehouse."'	
                                    AND
                                            id = '".(int)$this->id."'
				";
                        }
			//echo "<br>".$query."<br>";
			//die;
			if($reusult=$this->exeSQL($query))
			{
                                if($insertFlag)
                                {
                                    $this->id = $this->iLastInsertID ;
                                }
				$success = true;
				$kForwarder=new cForwarder();
				$idForwarder = $_SESSION['forwarder_id'];
				$kForwarder->load($idForwarder);
				
				if(($kForwarder->iAgreeTNC==1) && ($kForwarder->iUpdateRss==2))
				{
					$kConfig = new cConfig();
					$kWhsSearch = new cWHSSearch();
					
					$kWhsSearch->load($this->idOriginWarehouse);
					$szOriginCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
					
					$kWhsSearch->load($this->idDestinationWarehouse);
					$szDestinationCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
					
					$replace_ary = array();
					$replace_ary['szForwarderDisplayName'] = $kForwarder->szDisplayName ;
					$replace_ary['szOriginCountry'] = $szOriginCountry ;
					$replace_ary['szDestinationCountry'] = $szDestinationCountry ;
					
					$content_ary = array();
					$content_ary = $this->replaceRssContent('__OBO_LCL_SERVICES__',$replace_ary);
					
					$rssDataAry=array();
					$rssDataAry['idForwarder'] = $kForwarder->id ;
					$rssDataAry['szFeedType'] = '__OBO_LCL_SERVICES__';
					$rssDataAry['szLink'] = __RSS_FEED_PAGE_URL__ ;
					$rssDataAry['szTitle'] = $content_ary['szTitle'];
					$rssDataAry['szDescription'] = $content_ary['szDescription'];
					$rssDataAry['szDescriptionHTML'] = $content_ary['szDescriptionHTML'];
					
					$kRss = new cRSS();
					$kRss->addRssFeed($rssDataAry);
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
			return $success;
		}
		else
		{
			return false;
		}
	}
	
	function isWarehouseBelongsToForwarder($idOriginWhs=false,$idDestinationWhs=false)
	{
		$success=false;
		if($idOriginWhs>0)
		{
			$idForwarder = $_SESSION['forwarder_id'];
			$query="
                            SELECT
                                id
                            FROM
                                ".__DBC_SCHEMATA_WAREHOUSES__."	
                            WHERE
                                id = '".(int)$idOriginWhs."'	
                            AND
                                idForwarder = '".(int)$idForwarder."'	
                            AND
                                iActive = '1'	 
			";
			//echo "<br>".$query."<br>";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$success=true;
				}
				else
				{
					$this->addError('idOriginws',"origin ware house doesnot belongs to this forwarder");
					$success = false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		
		if($idDestinationWhs>0)
		{
                    $idForwarder = $_SESSION['forwarder_id'];
                    $query="
                        SELECT
                            id
                        FROM
                            ".__DBC_SCHEMATA_WAREHOUSES__."	
                        WHERE
                            id = '".(int)$idDestinationWhs."'	
                        AND
                            idForwarder = '".(int)$idForwarder."'	
                        AND
                            iActive = '1'	 
                    ";
                    //echo "<br>".$query."<br>";
                    if($result=$this->exeSQL($query))
                    {
                        if($this->iNumRows>0)
                        {
                            $success=true;
                        }
                        else
                        {
                            $this->addError('idDestinationws',t($this->t_base.'error/detination_warehouse'));
                            $success = false;
                        }
                    }
                    else
                    {
                        $this->error = true;
                        $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                        $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                        return false;
                    }
		}
		return $success ;
	}
	
	function isPricingAlreadyExists($idOriginWhs,$idDestinationWhs,$idCutOffDay=false,$idPricingWTW = false,$dtExpiry='')
	{
		if($idOriginWhs>0 && $idDestinationWhs>0)
		{
			if((int)$idCutOffDay>0)
			{
				$query_and = " AND
					idCutOffDay = '".(int)$idCutOffDay."'
				";
			}
			if($idPricingWTW>0)
			{
				$query_and .= " AND id!='".(int)$idPricingWTW."' ";
			}
			
			if($dtExpiry!='' && $dtExpiry!='0000-00-00 00:00:00')
			{
				$query_and .= " AND dtExpiry='".mysql_escape_custom($dtExpiry)."' ";
			}
			
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_WAREHOUSES_PRICING__."	
				WHERE
					idWarehouseFrom = '".(int)$idOriginWhs."'	
				AND
				    idWarehouseTo = '".(int)$idDestinationWhs."'	
				AND
					iActive = '1'
				AND
					dtExpiry > now()
				$query_and	
			";
			//echo "<br>".$query."<br>";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function inactivatePricingWTW($idPricingWtw)
	{
		if($idPricingWtw > 0)
		{
			$query="
				UPDATE
					".__DBC_SCHEMATA_WAREHOUSES_PRICING__."	
				SET
					iActive = '0'	
				WHERE
					iActive = '1'
				AND
					id = '".(int)$idPricingWtw."'		
			";
			//echo "<br>".$query."<br>";
			if($result=$this->exeSQL($query))
			{
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function inactivateCustomClearance($idOriginWhs,$idDestinationWhs,$iOriginDestination)
	{
		if($idOriginWhs > 0 && $idDestinationWhs>0)
		{
			$query="
				UPDATE
					".__DBC_SCHEMATA_PRICING_CC__."	
				SET
					iActive = '0'	
				WHERE
					idWarehouseFrom = '".(int)$this->idOriginWarehouse."'	
				AND
					idWarehouseTo = '".(int)$this->idDestinationWarehouse."'	
				AND
					szOriginDestination = '".(int)$iOriginDestination."'
				AND
					iActive='1'				
			";
			//echo "<br>".$query."<br>";
			if($result=$this->exeSQL($query))
			{
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function getAllWareHouses($iWarehouseType=__WAREHOUSE_TYPE_CFS__)
	{		
		$query="
			SELECT 
			   whs.id, 
			   whs.idCountry,
			   cont.szCountryName			   
			FROM 
				".__DBC_SCHEMATA_WAREHOUSES__." whs
			INNER JOIN
				".__DBC_SCHEMATA_COUNTRY__." cont
			ON
				cont.id = whs.idCountry				
			INNER JOIN
				".__DBC_SCHEMATA_FORWARDERS__."	fbd
			ON
				fbd.id = whs.idForwarder	
			WHERE
				whs.iActive = '1'	
			AND
			    fbd.iAgreeTNC = '1'	
                        AND
                            whs.iWarehouseType = '".$iWarehouseType."'
			ORDER BY
				cont.szCountryName ASC
		";	
		//echo "<br>".$query."<br>";
		//die;
		if($result = $this->exeSQL($query))
		{
			$wareHouseAry = array();
			$ctr = 0;
			while($row=$this->getAssoc($result))
			{
				$wareHouseAry[$row['id']]['szCountry'] = $row['szCountryName'];					
				$wareHouseAry[$row['id']]['idForwarder'] = $row['idForwarder'];	
				$wareHouseAry[$row['id']]['idCountry'] = $row['idCountry'];	
				$ctr++;
			}
			return $wareHouseAry;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}		
	}
	
	function getAllPricingWtwForStartingNewTrade()
	{
		$wareHouseFromAry = $this->getAllWareHouses(__WAREHOUSE_TYPE_CFS__);
		$wareHouseToAry = $this->getAllWareHouses(__WAREHOUSE_TYPE_CFS__);
		$ret_ary = array();
		$query = "TRUNCATE TABLE ".__DBC_SCHEMATA_STARTING_NEW_TRADES__;
			
		$result = $this->exeSQL( $query );
		foreach($wareHouseFromAry as $idWHSFrom=>$iDistanceWHSFrom)
		{
			foreach($wareHouseToAry as $idWHSTo=>$iDistanceWHSTo)
			{
				if($idWHSTo!=$idWHSFrom)
				{
					$query="
						SELECT
							wtw.id 	
						FROM
							".__DBC_SCHEMATA_WAREHOUSES_PRICING__."	wtw
						WHERE
							wtw.idWarehouseTo ='".mysql_escape_custom($idWHSTo)."'	
						AND
							wtw.idWarehouseFrom ='".mysql_escape_custom($idWHSFrom)."'
						AND
							wtw.iActive = '1'
						AND
							wtw.dtValidFrom	< now()
						AND
							wtw.dtExpiry > now()	
					";
					//echo "<br>".$query."<br>" ;
					//die;					
					if($result=$this->exeSQL($query))
					{
						if($this->iNumRows>0)
						{
							$key = $iDistanceWHSFrom['idCountry'].'_'.$iDistanceWHSTo['idCountry'] ;
							if(!array_key_exists($key,$ret_ary))
							{
								$ret_ary[$key] = "From ".$iDistanceWHSFrom['szCountry']." to ".$iDistanceWHSTo['szCountry'] ;
								
								$query1="
									INSERT INTO
										".__DBC_SCHEMATA_STARTING_NEW_TRADES__."
									(
										szFromCountry,
										szToCountry,
										dtCreated
									)
										VALUES
									(
										'".mysql_escape_custom($iDistanceWHSFrom['szCountry'])."',
										'".mysql_escape_custom($iDistanceWHSTo['szCountry'])."',
										NOW()
									)
								";
								//echo $query1;
								$result1=$this->exeSQL($query1);
							}
						}
					}
				}
			}
		}
		//return $ret_ary;
	}
	
	function getStartNewTrades()
	{
		$ret_ary = array();
		$query="
			SELECT
				szFromCountry,
				szToCountry
			FROM
				".__DBC_SCHEMATA_STARTING_NEW_TRADES__."			
		";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				while($row=$this->getAssoc($result))
				{
					$key = $row['szFromCountry'].'_'.$row['szToCountry'] ;
					if(!array_key_exists($key,$ret_ary))
					{
						if(!array_key_exists($key,$ret_ary))
						{
							$ret_ary[$key] = "From ".$row['szFromCountry']." to ".$row['szToCountry'] ;
						}
					}
				}
				return $ret_ary;
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function getAllServicingCountry($iOrderBy='s.szOriginCountry',$iLanguage=false)
	{
		$ret_ary = array();
		if(!empty($iOrderBy))
		{
			$query_order = " ORDER BY ".mysql_escape_custom(trim($iOrderBy))." ASC";
		}
		
		if($iLanguage !=__LANGUAGE_ID_ENGLISH__)
		{
			$query_select = "
				(SELECT cnm.szName FROM ".__DBC_SCHEMATA_COUNTRY__." c INNER JOIN ".__DBC_SCHEMATA_COUNTRY_NAME_MAPPING__." AS cnm ON cnm.idCountry=c.id WHERE cnm.idLanguage='".(int)$iLanguage."' AND c.id = s.idOriginCountry) szOriginCountry,
                                (SELECT cnm.szName  FROM ".__DBC_SCHEMATA_COUNTRY__." c1 INNER JOIN ".__DBC_SCHEMATA_COUNTRY_NAME_MAPPING__." AS cnm ON cnm.idCountry=c1.id WHERE cnm.idLanguage='".(int)$iLanguage."' AND c1 WHERE c1.id = s.idDestinationCountry) szDestinationCountry,
			";
		}
		else
		{
			$query_select = " s.szOriginCountry, s.szDestinationCountry," ;
		}
		
		$query="
			SELECT			
				$query_select	
				s.idOriginCountry,
				s.idDestinationCountry,
				(SELECT f.isOnline FROM ".__DBC_SCHEMATA_FROWARDER__." f WHERE f.id = s.idForwarder) iOnlineForwarder,
				(SELECT c.iActive FROM ".__DBC_SCHEMATA_COUNTRY__." c WHERE c.id = s.idOriginCountry) iOriginActive,
				(SELECT c1.iActive FROM ".__DBC_SCHEMATA_COUNTRY__." c1 WHERE c1.id = s.idDestinationCountry) iDestinationActive						
			FROM
				".__DBC_SCHEMATA_SERVICE_AVAILABLE__." s	
			WHERE
				s.iStatus = '1'	
			HAVING
			(
				iOriginActive = '1'
			AND
				iDestinationActive = '1'
			AND
				iOnlineForwarder = '1'
			)
			$query_order
		";
		//echo "<br /> ".$query."<br /> ";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				while($row=$this->getAssoc($result))
				{
					$key = $row['idOriginCountry'].'_'.$row['idDestinationCountry'] ;
					if($iOrderBy == 's.szOriginCountry')
					{
						if(!array_key_exists($key,$ret_ary))
						{
							if(!array_key_exists($key,$ret_ary))
							{
								$ret_ary[$key] = t($this->t_base.'title/From')." ".$row['szOriginCountry']." ".t($this->t_base.'title/to')." ".$row['szDestinationCountry'] ;
							}
						}
					}
					else if($iOrderBy == 's.szDestinationCountry')
					{
						if(!array_key_exists($key,$ret_ary))
						{
							if(!array_key_exists($key,$ret_ary))
							{
								$ret_ary[$key] = t($this->t_base.'title/To')." ".$row['szDestinationCountry']." ".t($this->t_base.'title/from')." ".$row['szOriginCountry'] ;
							}
						}
					}
				}
				return $ret_ary;
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	
	function exportDataTest($objPHPExcel,$sheetindex,$fileName,$kConfig,$desarr,$orginarr,$idForwarder,$downloadType,$idCurrency=0,$inActiveService=0,$iWarehouseType=__WAREHOUSE_TYPE_CFS__)
	{ 
            date_default_timezone_set('UTC');	
            if($sheetindex>0)
            {
                $objPHPExcel->createSheet();
            } 
            $objPHPExcel->setActiveSheetIndex($sheetindex);
						
            $styleArray = array(
                    'font' => array(
                            'bold' => false,
                            'size' =>12,
                    ),
                    'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    ),
                    'borders' => array(
                            'top' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'bottom' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'right' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'left' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                    ),
            );
			
			
            $styleArray1 = array(
                    'font' => array(
                            'bold' => false,
                            'size' =>12,
                    ),
                    'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    ),
                    'borders' => array(
                            'top' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'bottom' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'right' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'left' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                    ),
            );
			
            $styleArray2 = array(
                    'borders' => array(
                            'top' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'bottom' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                    ),
            );
			
            if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
            {
                $szHeadingG1 = 'Origin Charges (Receiving to Departure)';
                $szHeadingK1 = 'Freight and Surcharges (Airport to Airport)';
                $szHeadingO1 = 'Destination Charges (Arrival to available at warehouse)';
                $szHeadingT1 = 'Origin Warehouse Cut Off';
                $szHeadingPerWm = 'Per KG';
                $szHeadingC2 = 'Origin Airport Warehouse';
                $szHeadingF2 = 'Destination Airport Warehouse';
            }
            else
            {
                $szHeadingG1 = 'Origin Charges (CFS receiving to Port)';
                $szHeadingK1 = 'Freight and Surcharges (Port to Port)';
                $szHeadingO1 = 'Destination Charges (Port to available at CFS)';
                $szHeadingT1 = 'Origin CFS Cut Off';
                $szHeadingPerWm = 'Per W/M';
                $szHeadingC2 = 'Origin CFS';
                $szHeadingF2 = 'Destination CFS'; 
            } 
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(15);



            $objPHPExcel->getActiveSheet()->getStyle('A1:AA2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A1:AA2')->getFill()->getStartColor()->setARGB('FFddd9c3');


            $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);

            $objPHPExcel->getActiveSheet()->mergeCells('A1:C1');
            $objPHPExcel->getActiveSheet()->setCellValue('A1','Origin')->getStyle('A1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->applyFromArray($styleArray);
    //	$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->mergeCells('D1:F1');
            $objPHPExcel->getActiveSheet()->setCellValue('D1','Destination')->getStyle('D1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('D1:F1')->applyFromArray($styleArray);
            //$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->mergeCells('G1:J1');
             
            $objPHPExcel->getActiveSheet()->setCellValue('G1',$szHeadingG1)->getStyle('G1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('G1:J1')->applyFromArray($styleArray);
            //$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->mergeCells('K1:N1');
            $objPHPExcel->getActiveSheet()->setCellValue('K1',$szHeadingK1)->getStyle('K1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('K1:N1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->mergeCells('O1:R1');
            $objPHPExcel->getActiveSheet()->setCellValue('O1',$szHeadingO1)->getStyle('O1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('O1:R1')->applyFromArray($styleArray);

            //$objPHPExcel->getActiveSheet()->mergeCells('G1:J1');
            $objPHPExcel->getActiveSheet()->setCellValue('S1','Service')->getStyle('S1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->mergeCells('T1:U1');
            $objPHPExcel->getActiveSheet()->setCellValue('T1',$szHeadingT1)->getStyle('T1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('T1:U1')->applyFromArray($styleArray);
            //$objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('V1','Transit Time')->getStyle('V1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('V1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->mergeCells('W1:X1');
            $objPHPExcel->getActiveSheet()->setCellValue('W1','Destination Cargo Available')->getStyle('W1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('W1:X1')->applyFromArray($styleArray);
            //$objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('Y1','Booking Cut Off')->getStyle('Y1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('Y1')->applyFromArray($styleArray);


            $objPHPExcel->getActiveSheet()->mergeCells('Z1:AA1');
            $objPHPExcel->getActiveSheet()->setCellValue('Z1','Validity')->getStyle('Z1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('Z1:AA1')->applyFromArray($styleArray);
            //$objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('A2','Country')->getStyle('A2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray);

            //$objPHPExcel->getActiveSheet()->setCellValue('A2','Country')->getStyle('A2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            //$objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('B2','City')->getStyle('B2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('C2',$szHeadingC2)->getStyle('B2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('C2')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

            $objPHPExcel->getActiveSheet()->setCellValue('D2','Country')->getStyle('D2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('E2','City')->getStyle('E2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('F2',$szHeadingF2)->getStyle('F2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('F2')->applyFromArray($styleArray);


            $objPHPExcel->getActiveSheet()->setCellValue('G2',$szHeadingPerWm)->getStyle('G2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('G2')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('H2','Minimum')->getStyle('H2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('H2')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('I2','Per Booking')->getStyle('I2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('I2')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('J2','Currency')->getStyle('J2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('J2')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('K2',$szHeadingPerWm)->getStyle('K2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('K2')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('L2','Minimum')->getStyle('L2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('L2')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('M2','Per Booking')->getStyle('M2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('M2')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('N2','Currency')->getStyle('N2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('N2')->applyFromArray($styleArray);


            $objPHPExcel->getActiveSheet()->setCellValue('O2',$szHeadingPerWm)->getStyle('O2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('O2')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('P2','Minimum')->getStyle('P2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('P2')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('Q2','Per Booking')->getStyle('Q2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('Q2')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('R2','Currency')->getStyle('R2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('R2')->applyFromArray($styleArray);			


            $objPHPExcel->getActiveSheet()->setCellValue('S2','Frequency')->getStyle('S2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('S2')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('T2','Day')->getStyle('T2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('T2')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('U2','Local Time')->getStyle('U2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('U2')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('V2','Days')->getStyle('V2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('V2')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('W2','Day')->getStyle('W2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('W2')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('X2','Local Time')->getStyle('X2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('X2')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('Y2','Days')->getStyle('Y2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('Y2')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('Z2','From date')->getStyle('Z2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('Z2')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('AA2','Expiry date')->getStyle('AA2')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('AA2')->applyFromArray($styleArray);
			 
            $sql="";
            if((int)$inActiveService>0)
            {
                $sql .="
                    AND
                        iActive='0'
                    AND
                    (
                        szOriginChargeCurrency = ".$idCurrency."
                    OR
                        szFreightCurrency= ".$idCurrency."
                    OR
                        szDestinationChargeCurrency = ".$idCurrency."									
                    )
                ";
            }
            else
            {
                $sql .=" AND iActive='1' ";
            }
			
            $currencyArr=$this->getFreightCurrency();
							
            $query="
                SELECT
                    id
                FROM
                    ".__DBC_SCHEMATA_WAREHOUSES__."
                WHERE
                    idForwarder='".(int)$idForwarder."'	
                AND
                    iWarehouseType = '".(int)$iWarehouseType."'
            ";
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                if ($this->getRowCnt() > 0)
                {	  
                    require_once(__APP_PATH_CLASSES__."/PHPExcel/Cell/AdvancedValueBinder.php");  
                    PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_AdvancedValueBinder() );

                    //$fileName=__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/Transporteca-LCL-Services-upload-sheet_1.xlsx";
                    $this->exportListData($objPHPExcel,'1',$fileName);
                    $i=0;		
                    $rowcounter=2;
                    foreach($orginarr as $orginarrs)
                    {	 	
                        $result_array =array(); 
                        $array_orign = array();
                        foreach($desarr as $desarrs)
                        { 	
                            if($desarrs!=$orginarrs)
                            {
                                $query="
                                    SELECT
                                        szCutOffLocalTime,
                                        szAvailableLocalTime,
                                        fOriginChargeRateWM,
                                        fOriginChargeMinRateWM,
                                        fOriginChargeBookingRate,
                                        szOriginChargeCurrency,
                                        fFreightRateWM as fRateWM,
                                        fFreightMinRateWM as fMinRateWM,
                                        fFreightBookingRate as fRate,
                                        fDestinationChargeRateWM,
                                        fDestinationChargeMinRateWM,
                                        fDestinationChargeBookingRate,
                                        szDestinationChargeCurrency,
                                        dtExpiry,
                                        dtValidFrom,
                                        iBookingCutOffHours,
                                        idCutOffDay,
                                        idAvailableDay,
                                        idFrequency,
                                        szFreightCurrency,
                                        iTransitDays,
                                        iDestinationChargesApplicable,
                                        iOriginChargesApplicable
                                    FROM
                                        ".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
                                    WHERE
                                        idWarehouseFrom='".(int)$orginarrs."'
                                    AND
                                        idWarehouseTo='".(int)$desarrs."'
                                        $sql
                                    ORDER BY
                                        idCutOffDay ASC		
                                ";
                                //echo $query;
                                //die;
                                if( ( $result1 = $this->exeSQL( $query ) ) )
                                { 				
                                    if ($this->getRowCnt() > 0)
                                    {
                                        while($row1 = $this->getAssoc($result1))
                                        {
                                            $query="
                                                SELECT
                                                    szCity,
                                                    idCountry,
                                                    szWareHouseName
                                                FROM
                                                    ".__DBC_SCHEMATA_WAREHOUSES__."
                                                WHERE
                                                    id='".(int)$orginarrs."'	
                                                AND
                                                    iWarehouseType = '".(int)$iWarehouseType."'
                                            ";
                                            $result2 = $this->exeSQL( $query );
                                            $row2 = $this->getAssoc($result2);
                                            $array_orign[$i]['orgin_city']=$row2['szCity'];
                                            $array_orign[$i]['orgin_country']=$kConfig->getCountryName($row2['idCountry']);
                                            $array_orign[$i]['orgin_cfs']=$row2['szWareHouseName'];
                                            $array_orign[$i]['from_id']=$orginarrs;
                                            
                                            $query="
                                                SELECT
                                                    szCity,
                                                    idCountry,
                                                    szWareHouseName
                                                FROM
                                                    ".__DBC_SCHEMATA_WAREHOUSES__."
                                                WHERE
                                                    id='".(int)$desarrs."'	
                                                AND
                                                    iWarehouseType = '".(int)$iWarehouseType."'
                                            ";
                                            $result3 = $this->exeSQL( $query );
                                            $row3 = $this->getAssoc($result3);
                                            $array_orign[$i]['des_city']=$row3['szCity'];
                                            $array_orign[$i]['des_country']=$kConfig->getCountryName($row3['idCountry']);
                                            $array_orign[$i]['des_cfs']=$row3['szWareHouseName'];
                                            $array_orign[$i]['to_id']=$desarrs;
												
																																		
                                            $array_orign[$i]['szCutOffLocalTime']=$row1['szCutOffLocalTime'];
                                            $array_orign[$i]['szAvailableLocalTime']=$row1['szAvailableLocalTime'];

                                            $array_orign[$i]['fOriginChargeRateWM']=$row1['fOriginChargeRateWM'];
                                            $array_orign[$i]['fOriginChargeMinRateWM']=$row1['fOriginChargeMinRateWM'];
                                            $array_orign[$i]['fOriginChargeBookingRate']=$row1['fOriginChargeBookingRate'];
                                            $array_orign[$i]['iOriginChargesApplicable']=$row1['iOriginChargesApplicable'];


                                            $array_orign[$i]['fRateWM']=$row1['fRateWM'];
                                            $array_orign[$i]['fMinRateWM']=$row1['fMinRateWM'];
                                            $array_orign[$i]['fRate']=$row1['fRate'];

                                            $array_orign[$i]['fDestinationChargeRateWM']=$row1['fDestinationChargeRateWM'];
                                            $array_orign[$i]['fDestinationChargeMinRateWM']=$row1['fDestinationChargeMinRateWM'];
                                            $array_orign[$i]['fDestinationChargeBookingRate']=$row1['fDestinationChargeBookingRate'];
                                            $array_orign[$i]['iDestinationChargesApplicable']=$row1['iDestinationChargesApplicable'];

                                            $array_orign[$i]['dtExpiry']=$row1['dtExpiry'];
                                            $array_orign[$i]['dtValidFrom']=$row1['dtValidFrom'];
                                            if(isset($row1['iTransitDays']))
                                            {
                                                $iTransitDays=$row1['iTransitDays'];
                                            }
                                            else
                                            {
                                                    $iTransitDays='';
                                            }
                                            $array_orign[$i]['iTransitHours']=$row1['iTransitDays'];
                                            if($row1['iBookingCutOffHours']>'48')
                                            {
                                                $array_orign[$i]['iBookingCutOffHours']=ceil(($row1['iBookingCutOffHours']/24))." days";
                                            }
                                            else
                                            {
                                                $array_orign[$i]['iBookingCutOffHours']=$row1['iBookingCutOffHours']." hours";
                                            }
                                            $array_orign[$i]['idCutOffDay']=$this->getWeekday($row1['idCutOffDay']);
                                            $array_orign[$i]['idAvailableDay']=$this->getWeekday($row1['idAvailableDay']);
                                            $array_orign[$i]['idFrequency']=$this->getFrequency($row1['idFrequency']);

                                            $array_orign[$i]['szFreightCurrency']=$this->getFreightCurrency($row1['szFreightCurrency']);
                                            $array_orign[$i]['szOriginChargeCurrency'] = $this->getFreightCurrency($row1['szOriginChargeCurrency']);
                                            $array_orign[$i]['szDestinationChargeCurrency'] = $this->getFreightCurrency($row1['szDestinationChargeCurrency']);
                                            $i++;
                                        } 				
                                    }
                                    else
                                    {	 
                                        if($downloadType!='1' && $inActiveService==0)
                                        {
                                            $query="
                                                SELECT
                                                    szCity,
                                                    idCountry,
                                                    szWareHouseName
                                                FROM
                                                    ".__DBC_SCHEMATA_WAREHOUSES__."
                                                WHERE
                                                    id='".(int)$orginarrs."'	
                                                AND
                                                    iWarehouseType = '".(int)$iWarehouseType."'
                                            ";
                                            $result2 = $this->exeSQL( $query );
                                            $row2 = $this->getAssoc($result2);
                                            $array_orign[$i]['orgin_city']=$row2['szCity'];
                                            $array_orign[$i]['orgin_country']=$kConfig->getCountryName($row2['idCountry']);
                                            $array_orign[$i]['orgin_cfs']=$row2['szWareHouseName'];
                                            $array_orign[$i]['from_id']=$orginarrs;
                                            $query="
                                                SELECT
                                                    szCity,
                                                    idCountry,
                                                    szWareHouseName
                                                FROM
                                                    ".__DBC_SCHEMATA_WAREHOUSES__."
                                                WHERE
                                                    id='".(int)$desarrs."'	
                                                AND
                                                    iWarehouseType = '".(int)$iWarehouseType."'
                                            ";
                                            $result3 = $this->exeSQL( $query );
                                            $row3 = $this->getAssoc($result3);
                                            $array_orign[$i]['des_city']=$row3['szCity'];
                                            $array_orign[$i]['des_country']=$kConfig->getCountryName($row3['idCountry']);
                                            $array_orign[$i]['des_cfs']=$row3['szWareHouseName'];
                                            $array_orign[$i]['to_id']=$desarrs; 
                                            ++$i;
                                        } 						
                                    } 			
                                }
                            }								
                        }
                        $result_array = $array_orign;

                        if(!empty($result_array))
                        {			   		
                                foreach($result_array as $result_arrays)
                                {						   			
                                                    $validdate='';
                                                    $expirydate=' ';
                                                    if(isset($result_arrays['dtValidFrom'])!='')
                                                    {
                                                        if($result_arrays['dtValidFrom']!='0000-00-00 00:00:00' && $result_arrays['dtValidFrom']!="")
                                                        {
                                                            $validdate_arr=explode('-',$result_arrays['dtValidFrom']);
                                                            $validdate = gmmktime(0,0,0,$validdate_arr[1],$validdate_arr[2],$validdate_arr[0]);
                                                        }
                                                        else
                                                        {
                                                            $validdate=gmmktime(0,0,0,date('m'),date('d'),date('Y'));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        $validdate=gmmktime(0,0,0,date('m'),date('d'),date('Y'));
                                                    }

                                                    if(isset($result_arrays['dtExpiry'])!='')
                                                    {
                                                        if($result_arrays['dtExpiry']!='0000-00-00 00:00:00' && $result_arrays['dtExpiry']!="")
                                                        {
                                                            $expirytime_arr=explode('-',$result_arrays['dtExpiry']);
                                                            $expirydate= gmmktime(0,0,0,$expirytime_arr[1],$expirytime_arr[2],$expirytime_arr[0]);
                                                        }	
                                                    }
                                                    if($result_arrays['fRateWM']!='')
                                                    {
                                                        $fRateWM=$result_arrays['fRateWM'];
                                                    }
                                                    else
                                                    {
                                                        $fRateWM='';
                                                    }

                                                    if((float)$result_arrays['fMinRateWM']!=='')
                                                    {
                                                        $fMinRateWM=$result_arrays['fMinRateWM'];
                                                    }
                                                    else
                                                    {
                                                        $fMinRateWM='';
                                                    }

                                                    if((float)$result_arrays['fRate']!=='')
                                                    {
                                                            $fRate=$result_arrays['fRate'];
                                                    }
                                                    else
                                                    {
                                                        $fRate='';
                                                    }

                                                    if(isset($result_arrays['szFreightCurrency'])!='')
                                                    {
                                                        $szFreightCurrency=$result_arrays['szFreightCurrency'];
                                                    }
                                                    else
                                                    {
                                                        $szFreightCurrency='';
                                                    }						   			
                                                    if((float)$result_arrays['fOriginChargeRateWM']>0)
                                                    {
                                                        $fOriginChargeRateWM=$result_arrays['fOriginChargeRateWM'];
                                                    }
                                                    else
                                                    {
                                                        if($result_arrays['iOriginChargesApplicable']=='1')
                                                            $fOriginChargeRateWM='0.00';
                                                        else
                                                            $fOriginChargeRateWM='';
                                                    }

                                                    if((float)$result_arrays['fOriginChargeMinRateWM']>0)
                                                    {
                                                        $fOriginChargeMinRateWM = $result_arrays['fOriginChargeMinRateWM'];
                                                    }
                                                    else
                                                    {
                                                            if($result_arrays['iOriginChargesApplicable']=='1')
                                                                $fOriginChargeMinRateWM='0.00';
                                                            else
                                                                $fOriginChargeMinRateWM='';
                                                    }

                                                    if((float)$result_arrays['fOriginChargeBookingRate']>0)
                                                    {
                                                        $fOriginChargeBookingRate = $result_arrays['fOriginChargeBookingRate'];
                                                    }
                                                    else
                                                    {
                                                        if($result_arrays['iOriginChargesApplicable']=='1')
                                                            $fOriginChargeBookingRate='0.00';			
                                                        else
                                                            $fOriginChargeBookingRate='';
                                                    }

                                                    if(isset($result_arrays['szOriginChargeCurrency']))
                                                    {
                                                        $szOriginChargeCurrency = $result_arrays['szOriginChargeCurrency'];
                                                    }
                                                    else
                                                    {
                                                        $szOriginChargeCurrency = '';
                                                    }

                                                    if((float)$result_arrays['fDestinationChargeRateWM']>0)
                                                    {
                                                        $fDestinationChargeRateWM = $result_arrays['fDestinationChargeRateWM'];
                                                    }
                                                    else
                                                    {
                                                        if($result_arrays['iDestinationChargesApplicable']=='1')
                                                            $fDestinationChargeRateWM = '0.00';	
                                                        else		
                                                            $fDestinationChargeRateWM = '';
                                                    }

                                                    if((float)$result_arrays['fDestinationChargeMinRateWM']>0)
                                                    {
                                                        $fDestinationChargeMinRateWM = $result_arrays['fDestinationChargeMinRateWM'];
                                                    }
                                                    else
                                                    {
                                                        if($result_arrays['iDestinationChargesApplicable']=='1')
                                                            $fDestinationChargeMinRateWM = '0.00';
                                                        else
                                                            $fDestinationChargeMinRateWM = '';
                                                    }

                                                    if((float)$result_arrays['fDestinationChargeBookingRate']>0)
                                                    {
                                                        $fDestinationChargeBookingRate = $result_arrays['fDestinationChargeBookingRate'];
                                                    }
                                                    else
                                                    {
                                                        if($result_arrays['iDestinationChargesApplicable']=='1')
                                                            $fDestinationChargeBookingRate = '0.00';
                                                        else			
                                                            $fDestinationChargeBookingRate = '';
                                                    }

                                                    if(isset($result_arrays['szDestinationChargeCurrency'])!='')
                                                    {
                                                        $szDestinationChargeCurrency = $result_arrays['szDestinationChargeCurrency'];
                                                    }
                                                    else
                                                    {
                                                        $szDestinationChargeCurrency = '';
                                                    }

                                                    if(isset($result_arrays['idFrequency'])!='')
                                                    {
                                                        $idFrequency=$result_arrays['idFrequency'];
                                                    }
                                                    else
                                                    {
                                                        $idFrequency='';
                                                    }

                                                    if(isset($result_arrays['idCutOffDay'])!='')
                                                    {
                                                        $idCutOffDay=$result_arrays['idCutOffDay'];
                                                    }
                                                    else
                                                    {
                                                        $idCutOffDay='';
                                                    }

                                                    if(isset($result_arrays['szCutOffLocalTime'])!='')
                                                    {
                                                        $szCutOffLocalTime=$result_arrays['szCutOffLocalTime'];
                                                    }
                                                    else
                                                    {
                                                        $szCutOffLocalTime='';
                                                    }

                                                    if(isset($result_arrays['iTransitHours'])!='')
                                                    {
                                                        $iTransitHours=$result_arrays['iTransitHours'];
                                                    }
                                                    else
                                                    {
                                                        $iTransitHours='';
                                                    }

                                                    if(isset($result_arrays['idAvailableDay'])!='')
                                                    {
                                                        $idAvailableDay=$result_arrays['idAvailableDay'];
                                                    }
                                                    else
                                                    {
                                                        $idAvailableDay='';
                                                    }

                                                    if(isset($result_arrays['szAvailableLocalTime'])!='')
                                                    {
                                                        $szAvailableLocalTime=$result_arrays['szAvailableLocalTime'];
                                                    }
                                                    else
                                                    {
                                                        $szAvailableLocalTime='';
                                                    }

                                                    if(isset($result_arrays['iBookingCutOffHours'])!='')
                                                    {
                                                        $iBookingCutOffHours=$result_arrays['iBookingCutOffHours'];
                                                    }
                                                    else
                                                    {
                                                        $iBookingCutOffHours='';
                                                    }


                                                    //echo $expirydate."<br/>";

                                                    $colcounter=0;
                                                    $rowcounter++;
                                                    $fillColor='A'.$rowcounter.':F'.$rowcounter;
                                                    $fill_color='A'.$rowcounter.':S'.$rowcounter;
                                                    $objPHPExcel->getActiveSheet()->getStyle($fillColor)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                                                    $objPHPExcel->getActiveSheet()->getStyle($fillColor)->getFill()->getStartColor()->setARGB('FFddd9c3');
                                                    //$objPHPExcel->getActiveSheet()->getStyle($fillColor)->applyFromArray($styleArray);

                                                    $objPHPExcel->getActiveSheet()->getStyle('W'.$rowcounter)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                                                    $objPHPExcel->getActiveSheet()->getStyle('W'.$rowcounter)->getFill()->getStartColor()->setARGB('FFddd9c3');
                                                    $objPHPExcel->getActiveSheet()->getStyle('W'.$rowcounter)->applyFromArray($styleArray);

                                                    $objPHPExcel->getActiveSheet()->getStyle('G'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0.00');
                                                    $objPHPExcel->getActiveSheet()->getStyle('H'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0.00');
                                                    $objPHPExcel->getActiveSheet()->getStyle('I'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0.00');

                                                    $objPHPExcel->getActiveSheet()->getStyle('K'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0.00');
                                                    $objPHPExcel->getActiveSheet()->getStyle('L'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0.00');
                                                    $objPHPExcel->getActiveSheet()->getStyle('M'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0.00');
                                                    //$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);

                                                    $objPHPExcel->getActiveSheet()->getStyle('O'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0.00');
                                                    $objPHPExcel->getActiveSheet()->getStyle('P'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0.00');
                                                    $objPHPExcel->getActiveSheet()->getStyle('Q'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0.00');
                                                    $objPHPExcel->getActiveSheet()->getStyle('U'.$rowcounter)->getNumberFormat()->setFormatCode('hh:mm');

                                                    //$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);

                                                    $objPHPExcel->getActiveSheet()->getStyle('A'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('B'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('C'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('D'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('E'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('F'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('G'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('H'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('I'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('J'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('K'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('L'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('M'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('N'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('O'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('P'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('Q'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('R'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('S'.$rowcounter)->applyFromArray($styleArray2);

                                                    $objPHPExcel->getActiveSheet()->getStyle('T'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('U'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('V'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('W'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('X'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('Y'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('Z'.$rowcounter)->applyFromArray($styleArray2);
                                                    $objPHPExcel->getActiveSheet()->getStyle('AA'.$rowcounter)->applyFromArray($styleArray2);

                                                    $objPHPExcel->getActiveSheet()->setCellValue('A'.$rowcounter,$result_arrays['orgin_country'])->getStyle('A'.$rowcounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                                                    $objPHPExcel->getActiveSheet()->setCellValue('B'.$rowcounter,$result_arrays['orgin_city'])->getStyle('B'.$rowcounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                                                    $objPHPExcel->getActiveSheet()->setCellValue('C'.$rowcounter,$result_arrays['orgin_cfs'])->getStyle('C'.$rowcounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                                                    $objPHPExcel->getActiveSheet()->setCellValue('D'.$rowcounter,$result_arrays['des_country'])->getStyle('D'.$rowcounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                                                    $objPHPExcel->getActiveSheet()->setCellValue('E'.$rowcounter,$result_arrays['des_city'])->getStyle('E'.$rowcounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                                                    $objPHPExcel->getActiveSheet()->setCellValue('F'.$rowcounter,$result_arrays['des_cfs'])->getStyle('F'.$rowcounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                                                    
                                                    
                                                    $objPHPExcel->getActiveSheet()->setCellValue('G'.$rowcounter,$fOriginChargeRateWM)->getStyle('G'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                                    $objPHPExcel->getActiveSheet()->setCellValue('H'.$rowcounter,$fOriginChargeMinRateWM)->getStyle('H'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                                    $objPHPExcel->getActiveSheet()->setCellValue('I'.$rowcounter,$fOriginChargeBookingRate)->getStyle('I'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                                    $objPHPExcel->getActiveSheet()->setCellValue('J'.$rowcounter,$szOriginChargeCurrency)->getStyle('J'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                                    $objPHPExcel->getActiveSheet()->setCellValue('K'.$rowcounter,$fRateWM)->getStyle('K'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                                    
                                                    $objPHPExcel->getActiveSheet()->setCellValue('L'.$rowcounter,$fMinRateWM)->getStyle('L'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                                    $objPHPExcel->getActiveSheet()->setCellValue('M'.$rowcounter,$fRate)->getStyle('M'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                                    $objPHPExcel->getActiveSheet()->setCellValue('N'.$rowcounter,$szFreightCurrency)->getStyle('N'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                                    $objPHPExcel->getActiveSheet()->setCellValue('O'.$rowcounter,$fDestinationChargeRateWM)->getStyle('O'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                                    
                                                    
                                                    $objPHPExcel->getActiveSheet()->setCellValue('P'.$rowcounter,$fDestinationChargeMinRateWM)->getStyle('P'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                                    $objPHPExcel->getActiveSheet()->setCellValue('Q'.$rowcounter,$fDestinationChargeBookingRate)->getStyle('Q'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                                    $objPHPExcel->getActiveSheet()->setCellValue('R'.$rowcounter,$szDestinationChargeCurrency)->getStyle('R'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                                    $objPHPExcel->getActiveSheet()->setCellValue('S'.$rowcounter,$idFrequency)->getStyle('S'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                                    
                                                    
                                                    $objPHPExcel->getActiveSheet()->setCellValue('T'.$rowcounter,$idCutOffDay)->getStyle('T'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                                    
                                                    $objPHPExcel->getActiveSheet()->setCellValue('U'.$rowcounter,$szCutOffLocalTime)->getStyle('U'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                                    $objPHPExcel->getActiveSheet()->setCellValue('V'.$rowcounter,$iTransitHours)->getStyle('V'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                                    $objPHPExcel->getActiveSheet()->setCellValue('W'.$rowcounter,$idAvailableDay)->getStyle('W'.$rowcounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                                                    
                                                    $objPHPExcel->getActiveSheet()->getStyle('X'.$rowcounter)->getNumberFormat()->setFormatCode('hh:mm');
                                                    $objPHPExcel->getActiveSheet()->setCellValue('X'.$rowcounter,$szAvailableLocalTime)->getStyle('X'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                                    $objPHPExcel->getActiveSheet()->setCellValue('Y'.$rowcounter,$iBookingCutOffHours)->getStyle('Y'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                                    $objPHPExcel->getActiveSheet()->getStyle('Z'.$rowcounter)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);
                                                    $objPHPExcel->getActiveSheet()->setCellValue('Z'.$rowcounter,PHPExcel_Shared_Date::PHPToExcel($validdate))->getStyle('Z'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
//                                                                                                        
//                                                    
                                                    $objPHPExcel->getActiveSheet()->getStyle('AA'.$rowcounter)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);
                                                    if($expirydate=='')
                                                    {
                                                            $objPHPExcel->getActiveSheet()->setCellValue('AA'.$rowcounter,'')->getStyle('AA'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                                    }
                                                    else
                                                    {
                                                            $objPHPExcel->getActiveSheet()->setCellValue('AA'.$rowcounter,PHPExcel_Shared_Date::PHPToExcel($expirydate))->getStyle('AA'.$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                                            //$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,PHPExcel_Shared_Date::PHPToExcel($expirydate))->getStyleByColumnAndRow($colcounter,$rowcounter)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                                    }

                                                    
                                                    $val_g="G".$rowcounter;
                                                    $val_h="H".$rowcounter;
                                                    $val_i="I".$rowcounter;
                                                    if($rowcounter=='3')
                                                    {
                                                        $objValidationG = $objPHPExcel->getActiveSheet()->getCell($val_g)->getDataValidation();
                                                        $objValidationG->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL );
                                                        $objValidationG->setOperator( PHPExcel_Cell_DataValidation::OPERATOR_GREATERTHANOREQUAL);		
                                                        $objValidationG->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
                                                        $objValidationG->setAllowBlank(true);
                                                        $objValidationG->setShowInputMessage(true);
                                                        $objValidationG->setShowErrorMessage(true);
                                                        $objValidationG->setShowDropDown(true);
                                                        if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
                                                        {
                                                            $objValidationG->setErrorTitle('Origin charges per KG');
                                                            $objValidationG->setError('Please fill in origin charges per weight (mt)/measure (cbm) in selected currency. This should be zero or a positive decimal number.');
                                                        }
                                                        else
                                                        {
                                                            $objValidationG->setErrorTitle('Origin charges per W/M');
                                                            $objValidationG->setError('Please fill in origin charges per weight (mt)/measure (cbm) in selected currency. This should be zero or a positive decimal number.');
                                                        } 
                                                        $objValidationG->setFormula1();
                                                    }

                                                    if($rowcounter=='3')
                                                    {
                                                        $objValidationH = $objPHPExcel->getActiveSheet()->getCell($val_h)->getDataValidation();
                                                        $objValidationH->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL );
                                                        $objValidationH->setOperator( PHPExcel_Cell_DataValidation::OPERATOR_GREATERTHANOREQUAL);		
                                                        $objValidationH->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
                                                        $objValidationH->setAllowBlank(true);
                                                        $objValidationH->setShowInputMessage(true);
                                                        $objValidationH->setShowErrorMessage(true);
                                                        $objValidationH->setShowDropDown(true);
                                                        $objValidationH->setErrorTitle('Minimum origin charges');
                                                        $objValidationH->setError('Please fill in minimum origin charges in the selected currency. This should be zero or a positive decimal number.');
                                                        $objValidationH->setFormula1(0);
                                                    }
                                                    if($rowcounter=='3')
                                                    {
                                                        $objValidationI = $objPHPExcel->getActiveSheet()->getCell($val_i)->getDataValidation();
                                                        $objValidationI->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL );
                                                        $objValidationI->setOperator( PHPExcel_Cell_DataValidation::OPERATOR_GREATERTHANOREQUAL);		
                                                        $objValidationI->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
                                                        $objValidationI->setAllowBlank(true);
                                                        $objValidationI->setShowInputMessage(true);
                                                        $objValidationI->setShowErrorMessage(true);
                                                        $objValidationI->setShowDropDown(true);
                                                        $objValidationI->setErrorTitle('Origin charges per booking');
                                                        if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
                                                        {
                                                            $objValidationI->setError('Please fill in origin charges per booking in addition to the freight per kg and minimum. This should be zero or a positive decimal number.');
                                                        }
                                                        else
                                                        {
                                                            $objValidationI->setError('Please fill in origin charges per booking in addition to the freight per W/M and minimum. This should be zero or a positive decimal number.');
                                                        }
                                                        
                                                        $objValidationI->setFormula1(0);
                                                    }

                                                    $countCurrency=count($currencyArr);
                                                    $val_j="J".$rowcounter;
                                                    $val_currency='List!$A$1:$A$'.$countCurrency;

                                                    if($rowcounter=='3')
                                                    {																			
                                                        $objValidationJ = $objPHPExcel->getActiveSheet()->getCell($val_j)->getDataValidation();
                                                        $objValidationJ->setFormula1($val_currency);
                                                        $objValidationJ->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
                                                        $objValidationJ->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
                                                        $objValidationJ->setAllowBlank(true);
                                                        $objValidationJ->setShowInputMessage(true);
                                                        $objValidationJ->setShowErrorMessage(true);
                                                        $objValidationJ->setShowDropDown(true);
                                                        $objValidationJ->setErrorTitle('Currency');					   
                                                        $objValidationJ->setError('Please select one of the origin charge currencies.');
                                                        //$objValidation->setPromptTitle('Pick from list');
                                                        //$objValidation->setPrompt('Please pick a value from the drop-down list.');
                                                    }

                                                    $val_k="K".$rowcounter;
                                                    $val_l="L".$rowcounter;
                                                    $val_m="M".$rowcounter;
                                                    if($rowcounter=='3')
                                                    {
                                                        $objValidationK = $objPHPExcel->getActiveSheet()->getCell($val_k)->getDataValidation();
                                                        $objValidationK->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL );
                                                        $objValidationK->setOperator( PHPExcel_Cell_DataValidation::OPERATOR_GREATERTHANOREQUAL);		
                                                        $objValidationK->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
                                                        $objValidationK->setAllowBlank(true);
                                                        $objValidationK->setShowInputMessage(true);
                                                        $objValidationK->setShowErrorMessage(true);
                                                        $objValidationK->setShowDropDown(true);
                                                        
                                                        if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
                                                        {
                                                            $objValidationK->setErrorTitle('Freight rate per KG');
                                                            $objValidationK->setError('Please fill in freight rate per weight (mt)/measure (cbm) in selected currency. This should be a positive decimal number.');
                                                        }
                                                        else
                                                        {
                                                            $objValidationK->setErrorTitle('Freight rate per W/M');
                                                            $objValidationK->setError('Please fill in freight rate per weight (mt)/measure (cbm) in selected currency. This should be a positive decimal number.');
                                                        } 
                                                        $objValidationK->setFormula1(0);
                                                    }

                                                    if($rowcounter=='3')
                                                    {
                                                        $objValidationL = $objPHPExcel->getActiveSheet()->getCell($val_l)->getDataValidation();
                                                        $objValidationL->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL );
                                                        $objValidationL->setOperator( PHPExcel_Cell_DataValidation::OPERATOR_GREATERTHANOREQUAL);		
                                                        $objValidationL->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
                                                        $objValidationL->setAllowBlank(true);
                                                        $objValidationL->setShowInputMessage(true);
                                                        $objValidationL->setShowErrorMessage(true);
                                                        $objValidationL->setShowDropDown(true);
                                                        $objValidationL->setErrorTitle('Minimum freight charged');
                                                        $objValidationL->setError('Please fill in minimum freight rate in the selected currency. This should be zero or a positive decimal number.');
                                                        $objValidationL->setFormula1(0);
                                                    }
                                                    if($rowcounter=='3')
                                                    {
                                                        $objValidationM = $objPHPExcel->getActiveSheet()->getCell($val_m)->getDataValidation();
                                                        $objValidationM->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL );
                                                        $objValidationM->setOperator( PHPExcel_Cell_DataValidation::OPERATOR_GREATERTHANOREQUAL);		
                                                        $objValidationM->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
                                                        $objValidationM->setAllowBlank(true);
                                                        $objValidationM->setShowInputMessage(true);
                                                        $objValidationM->setShowErrorMessage(true);
                                                        $objValidationM->setShowDropDown(true);
                                                        $objValidationM->setErrorTitle('Rate per booking');
                                                        if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
                                                        {
                                                            $objValidationM->setError('Please fill in any fixed rate charged per booking in addition to the freight per kg and minimum. This should be zero or a positive decimal number.');
                                                        }
                                                        else
                                                        {
                                                            $objValidationM->setError('Please fill in any fixed rate charged per booking in addition to the freight per W/M and minimum. This should be zero or a positive decimal number.');
                                                        } 
                                                        $objValidationM->setFormula1(0);
                                                    }

                                                    $countCurrency=count($currencyArr);
                                                    $val_n="N".$rowcounter;
                                                    $val_currency='List!$A$1:$A$'.$countCurrency;

                                                    if($rowcounter=='3')
                                                    {

                                                        $objValidationN = $objPHPExcel->getActiveSheet()->getCell($val_n)->getDataValidation();
                                                        $objValidationN->setFormula1($val_currency);
                                                        $objValidationN->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
                                                        $objValidationN->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
                                                        $objValidationN->setAllowBlank(true);
                                                        $objValidationN->setShowInputMessage(true);
                                                        $objValidationN->setShowErrorMessage(true);
                                                        $objValidationN->setShowDropDown(true);
                                                        $objValidationN->setErrorTitle('Currency');					   
                                                        $objValidationN->setError('Please select one of the currencies.');
                                                        //$objValidation->setPromptTitle('Pick from list');
                                                        //$objValidation->setPrompt('Please pick a value from the drop-down list.');
                                                    }


                                                    $val_o="O".$rowcounter;
                                                    $val_p="P".$rowcounter;
                                                    $val_q="Q".$rowcounter;
                                                    if($rowcounter=='3')
                                                    {
                                                        $objValidationO = $objPHPExcel->getActiveSheet()->getCell($val_o)->getDataValidation();
                                                        $objValidationO->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL );
                                                        $objValidationO->setOperator( PHPExcel_Cell_DataValidation::OPERATOR_GREATERTHANOREQUAL);		
                                                        $objValidationO->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
                                                        $objValidationO->setAllowBlank(true);
                                                        $objValidationO->setShowInputMessage(true);
                                                        $objValidationO->setShowErrorMessage(true);
                                                        $objValidationO->setShowDropDown(true);
                                                        if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
                                                        {
                                                            $objValidationO->setErrorTitle('Destination charges per KG');
                                                            $objValidationO->setError('Please fill in destination charges per weight (mt)/measure (cbm) in selected currency. This should be zero or a positive decimal number.');
                                                        }
                                                        else
                                                        {
                                                            $objValidationO->setErrorTitle('Destination charges per W/M');
                                                            $objValidationO->setError('Please fill in destination charges per weight (mt)/measure (cbm) in selected currency. This should be zero or a positive decimal number.');
                                                        } 
                                                        $objValidationO->setFormula1(0);
                                                    }

                                                    if($rowcounter=='3')
                                                    {
                                                        $objValidationP = $objPHPExcel->getActiveSheet()->getCell($val_p)->getDataValidation();
                                                        $objValidationP->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL );
                                                        $objValidationP->setOperator( PHPExcel_Cell_DataValidation::OPERATOR_GREATERTHANOREQUAL);		
                                                        $objValidationP->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
                                                        $objValidationP->setAllowBlank(true);
                                                        $objValidationP->setShowInputMessage(true);
                                                        $objValidationP->setShowErrorMessage(true);
                                                        $objValidationP->setShowDropDown(true);
                                                        $objValidationP->setErrorTitle('Minimum destination charges');
                                                        $objValidationP->setError('Please fill in minimum destination charges in the selected currency. This should be zero or a positive decimal number.');
                                                        $objValidationP->setFormula1(0);
                                                    }
                                                    if($rowcounter=='3')
                                                    {
                                                        $objValidationQ = $objPHPExcel->getActiveSheet()->getCell($val_q)->getDataValidation();
                                                        $objValidationQ->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL );
                                                        $objValidationQ->setOperator( PHPExcel_Cell_DataValidation::OPERATOR_GREATERTHANOREQUAL);		
                                                        $objValidationQ->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
                                                        $objValidationQ->setAllowBlank(true);
                                                        $objValidationQ->setShowInputMessage(true);
                                                        $objValidationQ->setShowErrorMessage(true);
                                                        $objValidationQ->setShowDropDown(true);
                                                        if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
                                                        {
                                                            $objValidationQ->setErrorTitle('Destination charges per booking');
                                                            $objValidationQ->setError('Please fill in destination charges per booking in addition to the freight per kg and minimum. This should be zero or a positive decimal number.');
                                                        } 
                                                        else
                                                        {
                                                            $objValidationQ->setErrorTitle('Destination charges per booking');
                                                            $objValidationQ->setError('Please fill in destination charges per booking in addition to the freight per W/M and minimum. This should be zero or a positive decimal number.');
                                                        }
                                                        $objValidationQ->setFormula1(0);
                                                    }

                                                    $countCurrency=count($currencyArr);
                                                    $val_r="R".$rowcounter;
                                                    $val_currency='List!$A$1:$A$'.$countCurrency;

                                                    if($rowcounter=='3')
                                                    {

                                                        $objValidationR = $objPHPExcel->getActiveSheet()->getCell($val_r)->getDataValidation();
                                                        $objValidationR->setFormula1($val_currency);
                                                        $objValidationR->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
                                                        $objValidationR->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
                                                        $objValidationR->setAllowBlank(true);
                                                        $objValidationR->setShowInputMessage(true);
                                                        $objValidationR->setShowErrorMessage(true);
                                                        $objValidationR->setShowDropDown(true);
                                                        $objValidationR->setErrorTitle('Currency');					   
                                                        $objValidationR->setError('Please select one of the destination charge currencies.');
                                                        //$objValidation->setPromptTitle('Pick from list');
                                                        //$objValidation->setPrompt('Please pick a value from the drop-down list.');
                                                    }									
                                                    $val_s="S".$rowcounter;
                                                    $val_freq='List!$B$1:$B$2';
                                                    if($rowcounter=='3')
                                                    {
                                                        $objValidationS = $objPHPExcel->getActiveSheet()->getCell($val_s)->getDataValidation();
                                                        $objValidationS->setFormula1($val_freq);
                                                        $objValidationS->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
                                                        $objValidationS->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
                                                        $objValidationS->setAllowBlank(true);
                                                        $objValidationS->setShowInputMessage(true);
                                                        $objValidationS->setShowErrorMessage(true);
                                                        $objValidationS->setShowDropDown(true);
                                                        $objValidationS->setErrorTitle('Frequency');
                                                        $objValidationS->setError('Please select either weekly or biweekly (every two weeks) service.');
                                                        //$objValidation->setPromptTitle('Pick from list');
                                                        //$objValidation->setPrompt('Please pick a value from the drop-down list.');
                                                    }

                                                    $val_t="T".$rowcounter;
                                                    $val_day_l='List!$C$1:$C$7';
                                                    if($rowcounter=='3')
                                                    {
                                                        $objValidationT = $objPHPExcel->getActiveSheet()->getCell($val_t)->getDataValidation();
                                                        $objValidationT->setFormula1($val_day_l);
                                                        $objValidationT->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
                                                        $objValidationT->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
                                                        $objValidationT->setAllowBlank(true);
                                                        $objValidationT->setShowInputMessage(true);
                                                        $objValidationT->setShowErrorMessage(true);
                                                        $objValidationT->setShowDropDown(true);
                                                        $objValidationT->setErrorTitle('Cut off day');
                                                        $objValidationT->setError('Please select one of the weekdays available.');
                                                        //$objValidation->setPromptTitle('Pick from list');
                                                        //$objValidation->setPrompt('Please pick a value from the drop-down list.');
                                                    }

                                                    $val_u="U".$rowcounter;
                                                    $val_day_u='List!$F$1:$F$48';

                                                    if($rowcounter=='3')
                                                    {
                                                        $objValidationU = $objPHPExcel->getActiveSheet()->getCell($val_u)->getDataValidation();
                                                        $objValidationU->setFormula1($val_day_u);
                                                        $objValidationU->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
                                                        $objValidationU->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
                                                        $objValidationU->setAllowBlank(true);
                                                        $objValidationU->setShowInputMessage(true);
                                                        $objValidationU->setShowErrorMessage(true);
                                                        $objValidationU->setShowDropDown(true);
                                                        $objValidationU->setErrorTitle('Local time');
                                                        $objValidationU->setError('Please fill in the time of the CFS cargo cut off. Please use 24 hours clock (hh:mm) and only full and half hours.');
                                                        //$objValidation->setPromptTitle('Pick from list');
                                                        //$objValidation->setPrompt('Please pick a value from the drop-down list.');
                                                    }

                                                    $val_v='V'.$rowcounter;
                                                    if($rowcounter=='3')
                                                    { 										
                                                        $objValidationV = $objPHPExcel->getActiveSheet()->getCell($val_v)->getDataValidation();
                                                        $objValidationV->setType( PHPExcel_Cell_DataValidation::TYPE_WHOLE );
                                                        $objValidationV->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
                                                        $objValidationV->setAllowBlank(true);
                                                        $objValidationV->setShowInputMessage(true);
                                                        $objValidationV->setShowErrorMessage(true);
                                                        $objValidationV->setShowDropDown(true);
                                                        $objValidationV->setErrorTitle('Transit time');
                                                        $objValidationV->setError('Please fill in total CFS to CFS transit time in full number of days. Validation has been set to be between 0 and 100 days.');
                                                        $objValidationV->setFormula1(1);
                                                        $objValidationV->setFormula2(100);
                                                    }	

                                                    $val_x="X".$rowcounter;
                                                    $val_day_x='List!$F$1:$F$48';
                                                    if($rowcounter=='3')
                                                    {
                                                        $objValidationX = $objPHPExcel->getActiveSheet()->getCell($val_x)->getDataValidation();
                                                        $objValidationX->setFormula1($val_day_x);
                                                        $objValidationX->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
                                                        $objValidationX->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
                                                        $objValidationX->setAllowBlank(true);
                                                        $objValidationX->setShowInputMessage(true);
                                                        $objValidationX->setShowErrorMessage(true);
                                                        $objValidationX->setShowDropDown(true);
                                                        $objValidationX->setErrorTitle('Local time');
                                                        $objValidationX->setError('Please fill in the time of day when the cargo is available to the customer at the destination CFS. Please use 24 hours clock and only full and half hours.');
                                                    }									

                                                    $val_y="Y".$rowcounter;
                                                    $val_day_y='List!$G$1:$G$25';

                                                    if($rowcounter=='3')
                                                    {
                                                        $objValidationY = $objPHPExcel->getActiveSheet()->getCell($val_y)->getDataValidation();
                                                        $objValidationY->setFormula1($val_day_y);
                                                        $objValidationY->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
                                                        $objValidationY->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
                                                        $objValidationY->setAllowBlank(true);
                                                        $objValidationY->setShowInputMessage(true);
                                                        $objValidationY->setShowErrorMessage(true);
                                                        $objValidationY->setShowDropDown(true);
                                                        $objValidationY->setErrorTitle('Booking cut off');
                                                        $objValidationY->setError('Please fill in how long before the CFS cut off you latest need to receive the booking. Select an option from the list.');
                                                        //$objValidation->setPromptTitle('Pick from list');
                                                        //$objValidation->setPrompt('Please pick a value from the drop-down list.');
                                                    }

                                                    $val_z="Z".$rowcounter;
                                                    $val_aa="AA".$rowcounter;
                                                    if($rowcounter=='3')
                                                    {
                                                        $objValidationZ = $objPHPExcel->getActiveSheet()->getCell($val_z)->getDataValidation();
                                                        $objValidationZ->setType( PHPExcel_Cell_DataValidation::TYPE_DATE );
                                                        $objValidationZ->setOperator( PHPExcel_Cell_DataValidation::OPERATOR_GREATERTHAN);
                                                        $objValidationZ->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
                                                        $objValidationZ->setAllowBlank(true);
                                                        $objValidationZ->setShowInputMessage(true);
                                                        $objValidationZ->setShowErrorMessage(true);
                                                        $objValidationZ->setShowDropDown(true);
                                                        $objValidationZ->setErrorTitle('Valid from');
                                                        $objValidationZ->setError('Please fill in the date, which this rate is valid from.');
                                                        $objValidationZ->setFormula1(40908);
                                                    }
                                                    if($rowcounter=='3')
                                                    {
                                                        $objValidationAA = $objPHPExcel->getActiveSheet()->getCell($val_aa)->getDataValidation();
                                                        $objValidationAA->setType( PHPExcel_Cell_DataValidation::TYPE_DATE );
                                                        $objValidationAA->setOperator( PHPExcel_Cell_DataValidation::OPERATOR_GREATERTHAN);
                                                        $objValidationAA->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
                                                        $objValidationAA->setAllowBlank(true);
                                                        $objValidationAA->setShowInputMessage(true);
                                                        $objValidationAA->setShowErrorMessage(true);
                                                        $objValidationAA->setShowDropDown(true);
                                                        $objValidationAA->setErrorTitle('Expiry from');
                                                        $objValidationAA->setError('Please fill in the date this rate will expire.');
                                                        $objValidationAA->setFormula1(40908);
                                                    } 
                                                    $formula_W="W".$rowcounter;
                                                    $formula='=IF(T'.$rowcounter.'="","",IF(V'.$rowcounter.'="","",VLOOKUP(IF(ROUND((V'.$rowcounter.'/7-ROUNDDOWN(V'.$rowcounter.'/7,0))*7,0)+VLOOKUP(T'.$rowcounter.',List!C$1:D$7,2,FALSE)>7,ROUND((V'.$rowcounter.'/7-ROUNDDOWN(V'.$rowcounter.'/7,0))*7,0)+VLOOKUP(T'.$rowcounter.',List!C$1:D$7,2,FALSE)-7,ROUND((V'.$rowcounter.'/7-ROUNDDOWN(V'.$rowcounter.'/7,0))*7,0)+VLOOKUP(T'.$rowcounter.',List!C$1:D$7,2,FALSE)),List!D$1:E$7,2)))';
                                                    $objPHPExcel->getActiveSheet()->setCellValue($formula_W,$formula)->getStyle($formula_W)->getProtection()->setHidden(PHPExcel_Style_Protection::PROTECTION_PROTECTED);									
						   		}
						   	}
						}
					}
					else
					{
						return false;
					}
			   	}
			   	else
			   	{
			   		return false;
			   	}
			   	
			  /* 	$fillColor='A3:F'.$rowcounter;
				$fill_color='A'.$rowcounter.':S'.$rowcounter;
				$objPHPExcel->getActiveSheet()->getStyle($fillColor)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$objPHPExcel->getActiveSheet()->getStyle($fillColor)->getFill()->getStartColor()->setARGB('FFddd9c3');*/
			    if($rowcounter>2)
			   	{					
			   				
					$objPHPExcel->getActiveSheet()->getStyle('A3:A'.$rowcounter)->applyFromArray($styleArray);
					
					$objPHPExcel->getActiveSheet()->getStyle('B3:B'.$rowcounter)->applyFromArray($styleArray);
					
					$objPHPExcel->getActiveSheet()->getStyle('C3:C'.$rowcounter)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('D3:D'.$rowcounter)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('E3:E'.$rowcounter)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('F3:F'.$rowcounter)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('G3:G'.$rowcounter)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('H3:H'.$rowcounter)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('I3:I'.$rowcounter)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('J3:J'.$rowcounter)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('K3:K'.$rowcounter)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('L3:L'.$rowcounter)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('M3:M'.$rowcounter)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('N3:N'.$rowcounter)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('O3:O'.$rowcounter)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('P3:P'.$rowcounter)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('Q3:Q'.$rowcounter)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('R3:R'.$rowcounter)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('S3:S'.$rowcounter)->applyFromArray($styleArray);
					
					$objPHPExcel->getActiveSheet()->getStyle('T3:T'.$rowcounter)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('U3:U'.$rowcounter)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('V3:V'.$rowcounter)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('W3:W'.$rowcounter)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('X3:X'.$rowcounter)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('Y3:Y'.$rowcounter)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('Z3:Z'.$rowcounter)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('AA3:AA'.$rowcounter)->applyFromArray($styleArray);
			   	}	
				if($rowcounter>3)
			   	{		
			   	
			   	  $objPHPExcel->getActiveSheet()->setDataValidation('G4:G'.$rowcounter, $objValidationG);
				  $objPHPExcel->getActiveSheet()->setDataValidation('H4:H'.$rowcounter, $objValidationH);
				  $objPHPExcel->getActiveSheet()->setDataValidation('I4:I'.$rowcounter, $objValidationI);
				  $objPHPExcel->getActiveSheet()->setDataValidation('J4:J'.$rowcounter, $objValidationJ);
				  $objPHPExcel->getActiveSheet()->setDataValidation('K4:K'.$rowcounter, $objValidationK);	
				  $objPHPExcel->getActiveSheet()->setDataValidation('L4:L'.$rowcounter, $objValidationL);
				  $objPHPExcel->getActiveSheet()->setDataValidation('M4:M'.$rowcounter, $objValidationM);
				  $objPHPExcel->getActiveSheet()->setDataValidation('N4:N'.$rowcounter, $objValidationN);
				  $objPHPExcel->getActiveSheet()->setDataValidation('O4:O'.$rowcounter, $objValidationO);
				  $objPHPExcel->getActiveSheet()->setDataValidation('P4:P'.$rowcounter, $objValidationP);
				  $objPHPExcel->getActiveSheet()->setDataValidation('Q4:Q'.$rowcounter, $objValidationQ);
				  $objPHPExcel->getActiveSheet()->setDataValidation('R4:R'.$rowcounter, $objValidationR);
				  $objPHPExcel->getActiveSheet()->setDataValidation('S4:S'.$rowcounter, $objValidationS);				    	
				  $objPHPExcel->getActiveSheet()->setDataValidation('T4:T'.$rowcounter, $objValidationT);
				  $objPHPExcel->getActiveSheet()->setDataValidation('U4:U'.$rowcounter, $objValidationU);
				  $objPHPExcel->getActiveSheet()->setDataValidation('V4:V'.$rowcounter, $objValidationV);
				  $objPHPExcel->getActiveSheet()->setDataValidation('X4:X'.$rowcounter, $objValidationX);				
				  $objPHPExcel->getActiveSheet()->setDataValidation('Y4:Y'.$rowcounter, $objValidationY);
				  $objPHPExcel->getActiveSheet()->setDataValidation('Z4:Z'.$rowcounter, $objValidationZ);				  
				  $objPHPExcel->getActiveSheet()->setDataValidation('AA4:AA'.$rowcounter,$objValidationAA);
			   	}												
				
                $filter='A2:AA'.$rowcounter;
                $objPHPExcel->getActiveSheet()->setAutoFilter($filter);

                //define('FORMAT_DEFAULT_SYSTEM_DATE_FORMAT','dd-mm-yyyy');

                $objPHPExcel->getActiveSheet()->freezePane('G3');
                $objPHPExcel->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(3, 3);   	

                $objPHPExcel->getActiveSheet()->setTitle('Sheet1');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                $objPHPExcel->setActiveSheetIndex(0);
                //$ext = end(explode('.', $fileName));
                //$fileName=__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/Transporteca-LCL-Services-upload-sheet_1.xlsx";
                $objWriter->save($fileName);
                return true;
	}
	
	
	function getAllUpdatedServices($orginarr,$desarr)
	{
            if(!empty($orginarr))
            {
                $i=0;
                foreach($orginarr as $orginarrs)
                {		 
                    $result_array =array(); 
                    $array_orign = array();
                    foreach($desarr as $desarrs)
                    { 
                        if($desarrs!=$orginarrs)
                        {
                            $query="
                                SELECT
                                    szCutOffLocalTime,
                                    szAvailableLocalTime,
                                    fFreightRateWM as fRateWM,
                                    fFreightMinRateWM as fMinRateWM,
                                    fFreightBookingRate as fRate,
                                    dtExpiry,
                                    dtValidFrom,
                                    iBookingCutOffHours,
                                    idCutOffDay,
                                    idAvailableDay,
                                    idFrequency,
                                    szFreightCurrency,
                                    iTransitDays
                                FROM
                                    ".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
                                WHERE
                                    idWarehouseFrom='".(int)$orginarrs."'
                                AND
                                    idWarehouseTo='".(int)$desarrs."'
                                AND
                                    iActive='1'
                                ORDER BY
                                    idCutOffDay ASC		
                            ";
                            if( ( $result1 = $this->exeSQL( $query ) ) )
                            { 
                                if ($this->getRowCnt() > 0)
                                {
                                    while($row1 = $this->getAssoc($result1))
                                    {
                                        ++$i;
                                    } 
                                }							
                            }
                        }								
                    }
                }
                return $i;
            }
            else
            {
                return 0;
            }
	}
	
	function downloadCFSLocationTemplate($iWarehouseType)
	{			 
		require( __APP_PATH_CLASSES__ . "/PHPExcel.php" );
		$objPHPExcel=new PHPExcel();
			$styleArray = array(
				'font' => array(
					'bold' => false,
					'size' =>12,
				),
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				),
				'fill' => array(
         	 	'type' => PHPExcel_Style_Fill::FILL_SOLID,
            	'color' => array('rgb' => 'DDD9C3')
        		),
				'borders' => array(
					'top' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
						'color' => array('rgb' => '000000'),
					),
					'bottom' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
						'color' => array('rgb' => '000000'),
					),
					'right' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
						'color' => array('rgb' => '000000'),
					),
					'left' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
						'color' => array('rgb' => '000000'),
					),
				),
			);
			/*
			* FETCHING ALL COUNTRY LIST
			*/	
			
			$getAllCountryList	=	array(); 
			$getAllCountryList	=	$this->getAllCountry();
                        
                        if($iWarehouseType==2)
                        {
                            $szCfsName = "Airport Warehouse Name";
                            $szTitle = "Airport Warehouse Locations";
                            $szGeneralTitle = "airport warehouse";
                            $szGeneralTitleCol = "Airport Warehouse";
                        }
                        else
                        {
                            $szCfsName = "CFS Name";
                            $szTitle = "CFS Locations";
                            $szGeneralTitle = "CFS";
                            $szGeneralTitleCol = "CFS";
                        }
			
			/*
			* ENDING FETCHING ALL COUNTRY LIST
			*/	
			
			/*
			 * CFSLOCATION SHEET START
			 */ 
			 
			$objPHPExcel->getActiveSheet()->setTitle($szTitle);
			
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
			
			$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->setCellValue('A1',$szCfsName)->getStyle('A1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->setCellValue('B1','Address Line 1')->getStyle('B1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->setCellValue('C1','Address Line 2')->getStyle('C1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->setCellValue('D1','Address Line 3')->getStyle('D1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->setCellValue('E1','Postcode')->getStyle('E1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->setCellValue('F1','City')->getStyle('F1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->setCellValue('G1','Province/Region/State')->getStyle('G1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->setCellValue('H1','Country')->getStyle('H1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->setCellValue('I1','Phone Number')->getStyle('I1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->setCellValue('J1','Contact Person')->getStyle('J1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->setCellValue('K1','E-mail')->getStyle('K1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->setCellValue('L1','Latitude')->getStyle('L1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->setCellValue('M1','Longitude')->getStyle('M1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
			$objPHPExcel->getActiveSheet()->getStyle('A1:M1')->applyFromArray($styleArray); 
			
			for($rowcounter=2;$rowcounter<=100;$rowcounter++ )
			{										      
                            $col_m_xfd='A'.$rowcounter.':M'.$rowcounter;
                            $objPHPExcel->getActiveSheet()->getStyle($col_m_xfd)->getProtection(false)->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
		
			    $objPHPExcel->getActiveSheet()->getStyle('E'.$rowcounter)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
			    
			    
                            $countCountry=count($getAllCountryList);
                            $val_h="H".$rowcounter;
                            $valCountry='List!$A$1:$A$'.$countCountry;

                            $objValidationH = $objPHPExcel->getActiveSheet()->getCell($val_h)->getDataValidation();
                            $objValidationH->setFormula1($valCountry);
                            $objValidationH->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
                            $objValidationH->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
                            $objValidationH->setAllowBlank(true);
                            $objValidationH->setShowInputMessage(true);
                            $objValidationH->setShowErrorMessage(true);
                            $objValidationH->setShowDropDown(true);
                            $objValidationH->setErrorTitle('Country not recognised');					   
                            $objValidationH->setError('Please select a country from the drop down. If the country you are searching for is not available, contact Transporteca.');			    

                            $objPHPExcel->getActiveSheet()->getStyle('I'.$rowcounter)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);


                            $objPHPExcel->getActiveSheet()->getStyle('L'.$rowcounter.':M'.$rowcounter.'')->getNumberFormat()->setFormatCode("0.000000");

                            $val_j='L'.$rowcounter;

                            $objValidationJ = $objPHPExcel->getActiveSheet()->getCell($val_j)->getDataValidation();
			    $objValidationJ->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL );
                            $objValidationJ->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP);
			    $objValidationJ->setAllowBlank(true);
			    $objValidationJ->setShowInputMessage(true);
			    $objValidationJ->setShowErrorMessage(true);
			    $objValidationJ->setShowDropDown(true);
			    $objValidationJ->setErrorTitle('Latitude out of range');
                            $objValidationJ->setError('Latitude must be a decimal number between -90 (South Pole) and 90 (North Pole). To assist in finding this, there is a tool available on the '.$szGeneralTitle.' locations page on Transporteca Control Panel.');
                            $objValidationJ->setFormula1(-90.0);
                            $objValidationJ->setFormula2(+90.0);

                            $val_k='M'.$rowcounter;

                            $objValidationK = $objPHPExcel->getActiveSheet()->getCell($val_k)->getDataValidation();
			    $objValidationK->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL );
                            $objValidationK->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP);
			    $objValidationK->setAllowBlank(true);
			    $objValidationK->setShowInputMessage(true);
			    $objValidationK->setShowErrorMessage(true);
			    $objValidationK->setShowDropDown(true);
			    $objValidationK->setErrorTitle('Longitude out of range');
                            $objValidationK->setError('Longitude must be a decimal number between -180 and 180. To assist in finding this, there is a tool available on the '.$szGeneralTitle.' locations page on Transporteca Control Panel.');
                            $objValidationK->setFormula1(-180.0);
                            $objValidationK->setFormula2(+180.0);

                            $objPHPExcel->getActiveSheet()->getStyle('A'.$rowcounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                            $objPHPExcel->getActiveSheet()->getStyle('B'.$rowcounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                            $objPHPExcel->getActiveSheet()->getStyle('C'.$rowcounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                            $objPHPExcel->getActiveSheet()->getStyle('D'.$rowcounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                            $objPHPExcel->getActiveSheet()->getStyle('E'.$rowcounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                            $objPHPExcel->getActiveSheet()->getStyle('F'.$rowcounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                            $objPHPExcel->getActiveSheet()->getStyle('G'.$rowcounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                            //$objPHPExcel->getActiveSheet()->getStyle('H'.$rowcounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                            $objPHPExcel->getActiveSheet()->getStyle('I'.$rowcounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                            $objPHPExcel->getActiveSheet()->getStyle('J'.$rowcounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                            $objPHPExcel->getActiveSheet()->getStyle('K'.$rowcounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                            $objPHPExcel->getActiveSheet()->getStyle('L'.$rowcounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                            $objPHPExcel->getActiveSheet()->getStyle('M'.$rowcounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			}	
                        $obj = $objPHPExcel->getActiveSheet();

                        $filter='A1:M'.$rowcounter;
                        $objPHPExcel->getActiveSheet()->setAutoFilter($filter);

                        // FREEZING FIRST ROW OF THE SHEET
                        $objPHPExcel->getActiveSheet()->freezePane('A2');
                        $objPHPExcel->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(2,2);
			/*
                        * CFSLOCATION SHEET ENDS
                        */  
			 
			/*
                        * Example SHEET START
                        */		
			  
			$objPHPExcel->createSheet();
			$objPHPExcel->setActiveSheetIndex(1);
			$objPHPExcel->getActiveSheet()->setTitle('Example');
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			
			$objPHPExcel->getActiveSheet()->getStyle('E2:E4')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
			
			$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
			
			
			$objPHPExcel->getActiveSheet()->setCellValue('A1',$szCfsName)->getStyle('A1');
			$objPHPExcel->getActiveSheet()->setCellValue('B1','Address Line 1')->getStyle('B1');
			$objPHPExcel->getActiveSheet()->setCellValue('C1','Address Line 2')->getStyle('C1');
			$objPHPExcel->getActiveSheet()->setCellValue('D1','Address Line 3')->getStyle('D1');
			$objPHPExcel->getActiveSheet()->setCellValue('E1','Postcode')->getStyle('E1');
			$objPHPExcel->getActiveSheet()->setCellValue('F1','City')->getStyle('F1');
			$objPHPExcel->getActiveSheet()->setCellValue('G1','Province/Region/State')->getStyle('G1');
			$objPHPExcel->getActiveSheet()->setCellValue('H1','Country')->getStyle('H1');
			$objPHPExcel->getActiveSheet()->setCellValue('I1','Phone Number')->getStyle('I1');
			$objPHPExcel->getActiveSheet()->setCellValue('J1','Contact Person')->getStyle('J1');
			$objPHPExcel->getActiveSheet()->setCellValue('K1','E-mail')->getStyle('K1');
			$objPHPExcel->getActiveSheet()->setCellValue('L1','Latitude')->getStyle('J1');
			$objPHPExcel->getActiveSheet()->setCellValue('M1','Longitude')->getStyle('K1');
			 
			$objPHPExcel->getActiveSheet()->setCellValue('A2','ShipMe Shanghai '.$szGeneralTitleCol);
			$objPHPExcel->getActiveSheet()->setCellValue('B2','8 Zhendong Road');
			$objPHPExcel->getActiveSheet()->setCellValue('C2','Pudong');
			$objPHPExcel->getActiveSheet()->setCellValue('D2','');
			$objPHPExcel->getActiveSheet()->setCellValue('E2','2395313');
			$objPHPExcel->getActiveSheet()->setCellValue('F2','Shanghai');
			$objPHPExcel->getActiveSheet()->setCellValue('G2','Shanghai');
			$objPHPExcel->getActiveSheet()->setCellValue('H2','China');
			$objPHPExcel->getActiveSheet()->setCellValue('I2','+86 10 82025838');
			$objPHPExcel->getActiveSheet()->setCellValue('J2','Mr. Xiao wei');
			$objPHPExcel->getActiveSheet()->setCellValue('K2','Xiao@shipme.com');
			$objPHPExcel->getActiveSheet()->setCellValue('L2','30.946402');
			$objPHPExcel->getActiveSheet()->setCellValue('M2','121.89949');
			
			$objPHPExcel->getActiveSheet()->setCellValue('A3','Common Cargo Consol');
			$objPHPExcel->getActiveSheet()->setCellValue('B3','53A Beneluxweg');
			$objPHPExcel->getActiveSheet()->setCellValue('C3','Vlaardingen');
			$objPHPExcel->getActiveSheet()->setCellValue('D3','Exit 16');
			$objPHPExcel->getActiveSheet()->setCellValue('E3','3127 AK');
			$objPHPExcel->getActiveSheet()->setCellValue('F3','Rotterdam');
			$objPHPExcel->getActiveSheet()->setCellValue('G3','');
			$objPHPExcel->getActiveSheet()->setCellValue('H3','Netherlands');
			$objPHPExcel->getActiveSheet()->setCellValue('I3','+31 14 4345325'); 
			$objPHPExcel->getActiveSheet()->setCellValue('J3','Ms Helge Van Holk');
			$objPHPExcel->getActiveSheet()->setCellValue('K3','hvh@common.nl');
			$objPHPExcel->getActiveSheet()->setCellValue('L3','51.888359');
			$objPHPExcel->getActiveSheet()->setCellValue('M3','4.302521');
			
			$objPHPExcel->getActiveSheet()->setCellValue('A4','Big Box Freight Center');
			$objPHPExcel->getActiveSheet()->setCellValue('B4','4215 South Drive');
			$objPHPExcel->getActiveSheet()->setCellValue('C4','Block 4');
			$objPHPExcel->getActiveSheet()->setCellValue('D4','');
			$objPHPExcel->getActiveSheet()->setCellValue('E4','60412');
			$objPHPExcel->getActiveSheet()->setCellValue('F4','Chicago');
			$objPHPExcel->getActiveSheet()->setCellValue('G4','Illinois');
			$objPHPExcel->getActiveSheet()->setCellValue('H4','United States');
			$objPHPExcel->getActiveSheet()->setCellValue('I4','+1 (312) 920 0414');
			$objPHPExcel->getActiveSheet()->setCellValue('J4','Ms Louisa Knight');
			$objPHPExcel->getActiveSheet()->setCellValue('K4','louise@bigbox.com'); 
			$objPHPExcel->getActiveSheet()->setCellValue('L4','41.735454');
			$objPHPExcel->getActiveSheet()->setCellValue('M4','-87.430573');
			
			$objPHPExcel->getActiveSheet()->mergeCells('A7:M7');
			$objPHPExcel->getActiveSheet()->setCellValue('A7','This is an example of how this file should be populated. Do not update your '.$szGeneralTitle.' locations in this sheet, as it will not be read by Transporteca. Instead, update your '.$szGeneralTitle.' locations in the \''.$szTitle.'\' sheet!')->getStyle('A7')->getFont()->getColor()->setRGB('FF0000');
			
			// FREEZING FIRST ROW OF THE SHEET
			$objPHPExcel->getActiveSheet()->freezePane('A2');
			
			//SELECTING FIRST ACTIVE CELL OF THE EXCEL FILE OF CURRENT SHEET
			//$objPHPExcel->getActiveSheet()->setSelectedCell('A2');
			
			/*
			 * Example SHEET ENDS
			 */
			 
			/*
			 * LIST SHEET START
			 */ 
			
			$objPHPExcel->createSheet();
			$objPHPExcel->setActiveSheetIndex(2);
			$objPHPExcel->getActiveSheet()->setTitle('List');
			
			$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
			
			foreach($getAllCountryList as $key=>$value)
			{
				$objPHPExcel->getActiveSheet()->setCellValue('A'.($key+1),$value);	
			}
			$objPHPExcel->getActiveSheet()->setSheetState(PHPExcel_Worksheet::SHEETSTATE_HIDDEN);
			
			/*
			 * LIST SHEET ENDS
			 */ 
			
			$objPHPExcel->setActiveSheetIndex(0);
			//SELECTING FIRST ACTIVE CELL OF THE EXCEL FILE OF CURRENT SHEET
			//$objPHPExcel->getActiveSheet()->setSelectedCell('A2');
                        if($iWarehouseType==2)
                        {
                            $fileName=__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/Transporteca_airport_warehouse_locations_upload_sheet.xlsx";
                        }
                        else
                        {
                            $fileName=__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/Transporteca_CFS_locations_upload_sheet.xlsx";
                        }
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save($fileName);
			return $fileName;
	}	
	
	function getAllCountry()
	{
		$query="
			SELECT 
				DISTINCT(szCountryName)
			FROM 
				".__DBC_SCHEMATA_COUNTRY__."
			WHERE
				iActive = 1		
			ORDER BY 
				szCountryName ASC
				";
		//echo $query ;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$warehouseDataArr = array();
				while($row=$this->getAssoc($result))
				{
					$warehouseDataArr[]=utf8_encode($row['szCountryName']);
				}
				return $warehouseDataArr;
			}
			else
			{
				return array() ;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function uploadCFSLocationTemplate($idForwarder,$idForwarderContact,$file,$iWarehouseType)
	{
            $idForwarder = (int)sanitize_all_html_input($idForwarder);

            if((int)$_SESSION['forwarder_admin_id'])
            {
                $idForwarderContact = 0;
                $idAdminUpdatedBy = $_SESSION['forwarder_admin_id'] ;
            }
            else
            {
                $idForwarderContact = (int)sanitize_all_html_input($idForwarderContact);
            }
            if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
            {
                $szDataRelatedTo = "Airport Warehouses";
                $szComments = "These are the remaining Airport Warehouses Locations I did not process immediately from my bulk upload";
            }
            else
            {
                $szDataRelatedTo = "CFS Locations";
                $szComments = "These are the remaining CFS Locations I did not process immediately from my bulk upload";
            }
            if($idForwarder>0)
            { 
                $fileName  = trim(sanitize_all_html_input($file["name"])); 
                $fileType  = trim(sanitize_all_html_input($file["type"])); 
                $fileSize  = trim(sanitize_all_html_input($file["size"]));
                $fileError = trim(sanitize_all_html_input($file["error"])); 
                $fileTemp  = trim(sanitize_all_html_input($file["tmp_name"]));

                if( empty($fileType)  )
                {
                    $this->addError('szFile',t($this->t_base.'fields/no_file_uploaded'));
                    return false;
                }
                if( $fileError >0 )
                {
                    $this->addError('szFile',t($this->t_base.'fields/error_in_uploading_file'));
                    return false;
                }
                if( $fileType!='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' && $fileType!='application/vnd.ms-excel')
                {
                    $this->addError('szFile',t($this->t_base.'fields/unsupported_file_format'));
                    return false;
                }
                if(file_exists($fileTemp))
                {
                    require( __APP_PATH_CLASSES__ . "/PHPExcel/IOFactory.php" );
                    $objPHPExcel=new PHPExcel();

                    $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                    $objPHPExcel 	= $objReader->load($fileTemp);
                    $objWorksheet 	= $objPHPExcel->getActiveSheet();
                    //$highestRow 	= $objWorksheet->getHighestRow();
                    $highestColoumn = $objWorksheet->getHighestColumn();

                    for($j=0;$j<13;$j++)
                    {
                        $CFSServiceHeading[$j] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($j,1)->getValue();
                        if($CFSServiceHeading[$j]=='')
                        {
                            $this->addError('szContent',t($this->t_base.'error/the_file_proper_format'));
                            return false;
                        }
                    }
                    unset($j); 
                    if($highestColoumn > 'M')
                    {
                        $this->addError('szContent',t($this->t_base.'error/the_file_proper_format'));
                        return false;
                    }
                    $ctr = 0;
                    for($i=2;$i<=100;$i++)
                    {
                        for($j=0;$j<13;$j++)
                        {
                            if($objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0,$i)->getValue()!="")
                            {
                                $CFSServiceData[$ctr][] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($j,$i)->getValue();
                            }
                        }
                        $ctr++;
                    }
                    $CountData = count($CFSServiceData);
                    if($CountData==0)
                    {
                        $this->addError('szContent',t($this->t_base.'error/the_file_doesnt'));
                        return false;
                    }
					
                    $query = "
                        INSERT INTO
                            ".__DBC_SCHEMATA_AWATING_APPROVAL__."
                        (
                            `idForwarder`,
                            `idForwarderContact` ,
                             idAdminUpdateBy,
                            `szData` ,
                            `szCostApprovedBy` ,
                            `szDataRelatedTo` ,
                            `iRecords`,
                            `iProcessingCost` ,
                            `szComments` ,
                            `dtCompletedOn`
                        )
                        VALUES 
                        (
                            ".mysql_escape_custom($idForwarder).",
                            ".mysql_escape_custom($idForwarderContact).",
                            '".(int)$idAdminUpdatedBy."',
                            'N/A',
                            'N/A',
                            '".mysql_escape_custom($szDataRelatedTo)."',
                            '".(int)$CountData."',
                            '0.00',
                            '".mysql_escape_custom($szComments)."', 
                            NOW()
                        )								
                    "; 	
                    if($result=$this->exeSQL($query))
                    {
                        $BatchId = $this->iLastInsertID;
                        if($BatchId>0)
                        {	
                            if($CFSServiceData!= array())
                            {
                                foreach($CFSServiceData as $CFSdata)
                                {
                                    static $countNums=0;
                                    ++$countNums; 
                                    $country_Id=$this->getCountryId(sanitize_all_html_input($CFSdata['7']));

                                    $query = "
                                        INSERT INTO
                                            ".__DBC_SCHEMATA_WAREHOUSE_TEMP_DATA__."
                                            (
                                                `szWareHouseName` ,
                                                `szAddress1` ,
                                                `szAddress2` ,
                                                `szAddress3` ,
                                                `szPostCode` ,
                                                `szCity` ,
                                                `szState` ,
                                                `idCountry`,
                                                `szPhone` ,
                                                 szContactPerson,
                                                 szEmail,
                                                `szLatitude` ,
                                                `szLongitude` ,
                                                `dtCreatedOn` ,
                                                `idBatch`,
                                                 idForwarder,
                                                 iWarehouseType
                                            )
                                            VALUES 
                                            (
                                                '".mysql_escape_custom($CFSdata['0'])."',
                                                '".mysql_escape_custom($CFSdata['1'])."',
                                                '".mysql_escape_custom($CFSdata['2'])."',
                                                '".mysql_escape_custom($CFSdata['3'])."',
                                                '".mysql_escape_custom($CFSdata['4'])."',
                                                '".mysql_escape_custom($CFSdata['5'])."',
                                                '".mysql_escape_custom($CFSdata['6'])."',
                                                '".mysql_escape_custom($country_Id)."',
                                                '".mysql_escape_custom($CFSdata['8'])."',
                                                '".mysql_escape_custom($CFSdata['9'])."',
                                                '".mysql_escape_custom($CFSdata['10'])."',
                                                '".mysql_escape_custom($CFSdata['11'])."',
                                                '".mysql_escape_custom($CFSdata['12'])."',
                                                NOW(),
                                                '".$BatchId."',
                                                '".$idForwarder."',
                                                '".$iWarehouseType."'
                                            )
                                        ";
                                        //echo $query; 
                                        if(!($result=$this->exeSQL($query)))
                                        {
                                            $this->error = true;
                                            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                                            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                                            return false; 
                                        } 
                                    }
                                }
                                if($CountData==$countNums && $CountData>0)
                                {
                                    return array(true,$BatchId);
                                }
                                else
                                {
                                    return false;
                                }
                            }
                        }
                        else
                        {
                            $this->error = true;
                            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                            return false;
                        }
                    }
		}
	}
	
	function getWTWIdByCfs($idOriginWarehouse,$idDestinationWarehouse)
	{
		if($idOriginWarehouse>0 && $idDestinationWarehouse>0)
		{
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
				WHERE	
					idWarehouseFrom = '".(int)$idOriginWarehouse."'
				AND
					idWarehouseTo = '".(int)$idDestinationWarehouse."'
				AND
					iActive = '1'
			";
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row = $this->getAssoc($result);
					return $row['id'];
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	
	function getAllUpdatedHaulageServices($idhaulageWarehouseArr,$id=0)
	{
		if(!empty($idhaulageWarehouseArr))
		{
			$i=0;
			$sql='';
			if($id)
			{
				$sql .="
					AND
						hpm.id='".(int)$id."'
				";
			}
			foreach($idhaulageWarehouseArr as $idhaulageWarehouseArrs)
			{		
				$query="
					SELECT
						hp.id
					FROM
						".__DBC_SCHEMATA_HAULAGE_PRICING__." AS hp
					INNER JOIN
						".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." AS hpm
					ON
						hp.idHaulagePricingModel=hpm.id
					WHERE
						hpm.idWarehouse='".(int)$idhaulageWarehouseArrs['idWarehouse']."'
					AND
						hpm.idHaulageModel='".(int)$idhaulageWarehouseArrs['idPricingModel']."'
					AND
						hp.iActive='1'
					".$sql."
				";
				//echo $query."<br/>";
				if( ( $result1 = $this->exeSQL( $query ) ) )
				{
					if ($this->getRowCnt() > 0)
					{
						while($row1 = $this->getAssoc($result1))
						{
							++$i;
						}
					}
				}	
			}
			return $i;
		}
		else
		{
			return 0;
		}
	}

	
	function getPricingModelId($szName)
	{
		if(!empty($szName))
		{
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_HAULAGE_MODELS__."
				WHERE
				 	szModelShortName='".mysql_escape_custom($szName)."'
			";
			//echo $query."<br/>";
			if( ( $result1 = $this->exeSQL( $query ) ) )
			{
				if ($this->getRowCnt() > 0)
				{
					$row1 = $this->getAssoc($result1);
					return $row1['id'];
				}
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}
	
	
	
	function getHaulagePricingModelId($szHaulageName,$idHaulageModel,$idWarehouse,$iDirection)
	{
		if(!empty($szHaulageName))
		{
			$sql='';
			if($idHaulageModel==4)
			{
				$sql .="iDistanceUpToKm='".mysql_escape_custom($szHaulageName)."'";
			}
			else
			{
				$sql .="szName='".mysql_escape_custom($szHaulageName)."'";
			}
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
				WHERE
				 	$sql
				 AND
			 	 	idHaulageModel='".$idHaulageModel."'
			 	 AND
			 	 	idWarehouse='".$idWarehouse."'
			 	 AND
			 	 	iDirection='".$iDirection."'
			";
			if( ( $result1 = $this->exeSQL( $query ) ) )
			{
				if ($this->getRowCnt() > 0)
				{
					$row1 = $this->getAssoc($result1);
					return $row1['id'];
				}
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}
	
	function getForwaderWarehousesPricingModel($idForwarder,$idCountry=0,$idWarehouse=0,$flag="")
	{
		if((int)$idForwarder>0)
		{
			$sql="";
			if($idCountry!='0' && $idCountry!='')
			{
				$sql .="
					AND
					    w.idCountry IN (".$idCountry.")	
				";
			}
			if((int)$idWarehouse>0)
			{
				$sql .="
					AND
					    w.id='".(int)$idWarehouse."'	
				";
			}
			if(!empty($szCity))
			{
				$sql .="
					AND
					    w.szCity = '".mysql_escape_custom(trim($szCity))."'	
				";
			}
			if($orderby)
			{
                            $sort_order="
                                w.szWareHouseName ASC
                            ";
			}
			else
			{
                            $sort_order="
                                c.szCountryName ASC,
                                w.szWareHouseName ASC
                            ";
			}
			$idForwarderArr=array();
			$haulageModelArr=array();
			$query="
                            SELECT
                                c.szCountryName,
                                w.szWareHouseName,
                                w.id,
                                c.id as idCountry,
                                hpm.id AS idPricingModel,
                                hpm.idHaulageModel
                            FROM
                                ".__DBC_SCHEMATA_WAREHOUSES__." AS w
                            INNER JOIN
                                ".__DBC_SCHEMATA_COUNTRY__." AS c
                            ON
                                c.id=w.idCountry
                            INNER JOIN
                                ".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." AS hpm
                            ON
                                w.id=hpm.idWarehouse
                            WHERE
                                w.idForwarder='".(int)$idForwarder."'
                            AND
                                w.iActive = '1'	 
                                ".$sql."
                            ORDER BY
                                ".$sort_order."
			";
			//echo $query;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					while($row=$this->getAssoc($result))
					{	
						if($flag=='pricingModel')
						{
							if(!in_array($row['idHaulageModel'],$haulageModelArr))
							{
								$forwarderWarehouses[]=$row['idHaulageModel'];
								$haulageModelArr[]=$row['idHaulageModel'];
							}
						}
						else
						{
							if(!in_array($row['id'],$idForwarderArr))
							{				
								$forwarderWarehouses[]=$row;
								$idForwarderArr[]=$row['id'];
							}
						}
					}				
					return $forwarderWarehouses;
				}
			}	
		}
		else
		{
			return array();
		}
	}
	function countForwaderHaulage($idForwarder)
	{
		if((int)$idForwarder>0)
		{
			$query="
				SELECT
					count(hp.id) as total
				FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING__." AS hp
				INNER JOIN							
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." AS hpm
				ON
					hp.idHaulagePricingModel=hpm.id
				INNER JOIN
					".__DBC_SCHEMATA_WAREHOUSES__." AS w
				ON
					w.id=hpm.idWarehouse
				WHERE
					w.idForwarder='".(int)$idForwarder."'
				AND
					w.iActive = '1'	
				AND
					hpm.iActive = '1'
				AND
					hp.iActive='1' 
			";
			//echo $query;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);				
					return $row['total'];
				}
			}	
		}
		else
		{
			return 0;
		}
	}
	function countRcordsList($fileName,$iFileType,$idForwarder)
	{
            //echo $fileName;
            $successFlag=false;
            require_once( __APP_PATH_CLASSES__ . "/PHPExcel/IOFactory.php" );
            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
            $sheetindex=0;
            $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load($fileName);
            $objPHPExcel->setActiveSheetIndex($sheetindex);
            $val1 = ($objPHPExcel->getActiveSheet()->getHighestColumn()); //CELL
            $colcount = PHPExcel_Cell::columnIndexFromString($val1);
            $rowcount = ($objPHPExcel->getActiveSheet()->getHighestRow());

            $errmsg='2';
            $kUploadBulkService = new cUploadBulkService();
            //echo $iFileType."<br/>";
            if($iFileType=='Customs Clearance')
            {
                if($kUploadBulkService->addDataForApprovalPricingCC($objPHPExcel,$sheetindex,$colcount,$rowcount,$idForwarder))
                {
                    $successFlag=true;
                    $errmsg='2';
                }
                else
                {
                    $errmsg="1";
                }
            } 
            else if($iFileType=='LCL Services' || $iFileType=='Airfreight Services')
            {
                if($iFileType=='Airfreight Services')
                {
                    $iWarehouseType = __WAREHOUSE_TYPE_AIR__;
                }
                else
                {
                    $iWarehouseType = __WAREHOUSE_TYPE_CFS__;
                }
                if($kUploadBulkService->addDataForApprovalLCLServicesData($objPHPExcel,$sheetindex,$colcount,$rowcount,$idForwarder,$iWarehouseType))
                {
                    $successFlag=true;
                    $errmsg='2';
                }
                else
                {
                    $errmsg="1";
                }
            }
            else if($iFileType=='Haulage')
            {
                if($kUploadBulkService->addDataForApprovalHaulageServicesData($objPHPExcel,$sheetindex,$colcount,$rowcount,$idForwarder))
                {
                    $successFlag=true;
                    $errmsg='2';
                }
                else
                {
                    $errmsg="1";
                }
            }
            else if($iFileType=='CFS locations' || $iFileType=='Airport Warehouses')
            {
                if($iFileType=='Airport Warehouses')
                {
                    $iWarehouseType = __WAREHOUSE_TYPE_AIR__;
                }
                else
                {
                    $iWarehouseType = __WAREHOUSE_TYPE_CFS__;
                }
                if($kUploadBulkService->bulkUploadCFS($objPHPExcel,$sheetindex,$colcount,$rowcount,$idForwarder,true,$iWarehouseType))
                {
                    $successFlag=true;
                    $errmsg='2';
                }
                else
                {
                    $errmsg="1";
                }
            }
            $rowcount=$kUploadBulkService->rowCountUploaded;
            $errorRowCount=(int)$kUploadBulkService->rowErrorCount; 
            return $rowcount."_".$errmsg."_".$errorRowCount; 
	}
	
	function getPricingModelName($id)
	{
		if((int)$id>0)
		{
			$query="
				SELECT
					szModelLongName,
					szModelShortName
				FROM
					".__DBC_SCHEMATA_HAULAGE_MODELS__."
				WHERE
				 	id='".(int)$id."'
			";
			//echo $query."<br/>";
			if( ( $result1 = $this->exeSQL( $query ) ) )
			{
				if ($this->getRowCnt() > 0)
				{
					$row1 = $this->getAssoc($result1);
					$res_arr[]=$row1;
					return $res_arr;
				}
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	
	function getAllUpdatedHaulageModelExists($idhaulageWarehouseArr,$flag=false)
	{
		if(!empty($idhaulageWarehouseArr))
		{
			$i=0;
			$sql='';
			/*if($flag)
			{
				$sql .="
					AND
						hp.iActive='1'
				";
			}*/
			foreach($idhaulageWarehouseArr as $idhaulageWarehouseArrs)
			{		
				$query="
					SELECT
						hpm.id
					FROM
						".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." AS hpm
					LEFT JOIN
						".__DBC_SCHEMATA_HAULAGE_PRICING__." AS hp
					ON
						(hp.idHaulagePricingModel=hpm.id AND hp.iActive='1') 
					WHERE
						hpm.idWarehouse='".(int)$idhaulageWarehouseArrs['idWarehouse']."'
					AND
						hpm.idHaulageModel='".(int)$idhaulageWarehouseArrs['idPricingModel']."'
					AND
						hpm.iActive='1'
				";
				//echo $query."<br/>";
				if( ( $result1 = $this->exeSQL( $query ) ) )
				{
					if ($this->getRowCnt() > 0)
					{
						while($row1 = $this->getAssoc($result1))
						{
							++$i;
						}
					}
				}	
			}
			return $i;
		}
		else
		{
			return 0;
		}
	}
	function getAllwarehouseDetails($arr)
	{	
            if($arr!=array())
            {
                $add_query2 = "";
                if($arr['idOriginCountry'])
                {
                    $add_query .= "  AND w1.idCountry = ".$arr['idOriginCountry']." ";
                }
                if($arr['idDestinationCountry'])
                {
                    $add_query .= " AND w2.idCountry = ".$arr['idDestinationCountry']." ";
                }
                if($arr['idForwarder'])
                {
                    $add_query .= "  AND  w1.idForwarder = ".$arr['idForwarder']."  ";
                }
                if($arr['iWarehouseType']>0)
                {
                    $add_query .= "  AND  w1.iWarehouseType = '".$arr['iWarehouseType']."'  AND w2.iWarehouseType = '".$arr['iWarehouseType']."' ";
                }
                if($arr['iActive'] == 0)
                {
                    $add_query1 .= "
                            wtw.dtExpiry> NOW() - INTERVAL 3 MONTH	
                        AND
                            wtw.dtExpiry < NOW()	
                    ";
                }
                else
                {
                    $add_query1 .= " wtw.dtExpiry > NOW() ";
                    $add_query2 = " AND  f.isOnline = 1 ";
                }
            }
            $query="
                SELECT
                    DISTINCT(wtw.id)
                FROM
                    ".__DBC_SCHEMATA_WAREHOUSES_PRICING__."	 AS wtw
                INNER JOIN
                    ".__DBC_SCHEMATA_WAREHOUSES__." AS w1
                ON 
                    (w1.id = wtw.idWarehouseFrom && w1.iActive = 1 )
                INNER JOIN
                    ".__DBC_SCHEMATA_WAREHOUSES__." AS w2
                ON 
                    ( w2.id = wtw.idWarehouseTo	&& w2.iActive = 1 )	
                INNER JOIN
                    ".__DBC_SCHEMATA_FORWARDERS__." AS f
                ON 
                    f.id = w1.idForwarder		
                INNER JOIN 
                    ".__DBC_SCHEMATA_COUNTRY__." AS c1
                ON
                    ( c1.id = w1.idCountry	&& c1.iActive  = 1	)	
                INNER JOIN 
                    ".__DBC_SCHEMATA_COUNTRY__." AS c2
                ON		
                    ( c2.id = w2.idCountry	&& c2.iActive  = 1 )		 
                WHERE
                    ".$add_query1."
                AND
                    f.iActive  = 1	 
                ".$add_query2."
                " .$add_query. " 
                ORDER BY 
                    c1.szCountryISO ASC,
                    c2.szCountryISO ASC
        ";
        //echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        {	
            $arrid = array();
            while($row = $this->getAssoc($result))
            {
                $arrid[]=$row['id'];
            }
            if($arrid!= array())
            {
                return implode(',',$arrid);
            }
            else
            {
                return 0;
            }
        }
    }


	function getHaulageModelPriority($key,$newwarehouseArrs,$idDirection)
	{
		$query="
			SELECT
				iPriority,
				idHaulageModel
			FROM
				".__DBC_SCHEMATA_HAULAGE_MODELS_CFS_MAPPING__."
			WHERE
				idWarehouse='".(int)$key."'
			AND
				iDirection='".(int)$idDirection."'
			ORDER BY
				iPriority ASC
		";
		if( ( $result = $this->exeSQL( $query ) ) )
		{	
			while($row = $this->getAssoc($result))
			{
				$arr_res[$row['iPriority']]=$row['idHaulageModel'];
			}
			$new_array=array();
			if(!empty($arr_res))
			{
				foreach($arr_res as $key=>$value)
				{
					if(in_array($value,$newwarehouseArrs))
					{
						$new_array[]=$value;
					}
				}
			}
			return $new_array;
		}
	}
	function updateOBOCustomClearanceByAdmin($data)
	{
		$update_oring_cc = false;
		$update_destination_cc = false;
		
		$this->set_idOriginWarehouse(sanitize_all_html_input(trim($data['idOriginWarehouse'])));
		$this->set_idDestinationWarehouse(sanitize_all_html_input(trim($data['idDestinationWarehouse'])));
	
		if((float)$data['fOriginPrice']>0 || (int)$data['idOriginCurrency']>0)
		{
			$this->set_fOriginPrice(sanitize_all_html_input(trim($data['fOriginPrice'])));
			$this->set_idOriginCurrency(sanitize_all_html_input(trim($data['idOriginCurrency'])));
			$update_oring_cc = true;
		}
		
		if((float)$data['fDestinationPrice']>0 || (int)$data['idDestinationCurrency']>0)
		{
			$this->set_fDestinationPrice(sanitize_all_html_input(trim($data['fDestinationPrice'])));
			$this->set_idDestinationCurrency(sanitize_all_html_input(trim($data['idDestinationCurrency'])));
			$update_destination_cc = true;
		}
		
		if($update_destination_cc==false && $update_oring_cc==false)
		{
			$this->addError('fDestinationPrice','At least one line should be filled');
			return false;
		}
		if($this->error==true)
		{
			return false;
		}		
		if($this->idOriginWarehouse == $this->idDestinationWarehouse)
		{
			$this->addError('szDestinationWarehouse',t($this->t_base.'messages/same_whs_error_message'));
			return false;
		}		
		$success = false;	
		$idForwarder = $_SESSION['forwarder_id'] ;	
		
		if($update_oring_cc)
		{
			$query="
				SELECT
					*
				FROM
					".__DBC_SCHEMATA_PRICING_CC__."
				WHERE
					idWarehouseFrom='".(int)$this->idOriginWarehouse."'
				AND
					idWarehouseTo='".(int)$this->idDestinationWarehouse."'
				AND
					szOriginDestination='1'
				";
				//echo $query."<br/>";
				if($result=$this->exeSQL($query))
				{
					if($this->iNumRows>0)
					{
						$row=$this->getAssoc($result);	
							
						
							$idAdmin = (int)$_SESSION['admin_id'];
							$idForwarderContact = 0 ;
						
									
						$query="
							INSERT INTO
								".__DBC_SCHEMATA_FORWARDER_PRCIING_CC_ARCHIVE__."
							(
								idWarehouseFrom,
								idWarehouseTo,
								szOriginDestination,
								fPrice,
								szCurrency, 
								dtUpdateOn,
								iActive,
								idForwarder,
								idForwarderContact,
								idAdmin,
								dtCreatedOn
							)
							VALUES
							(
								'".(int)$this->idOriginWarehouse."',
								'".(int)$this->idDestinationWarehouse."',
								'1',
								'".(float)$row['fPrice']."',
								'".(int)$row['szCurrency']."',
								NOW(),
								'".(int)$row['iActive']."',
								'".(int)$idForwarder."',
								'".(int)$idForwarderContact."',
								'".(int)$idAdmin."',
								'".mysql_escape_custom($row['dtCreatedOn'])."'					
							)
						";
						//echo $query."<br/>";
						$result=$this->exeSQL($query);
						$query="
						UPDATE
							".__DBC_SCHEMATA_PRICING_CC__."
						SET
							fPrice = '".(float)$this->fOriginPrice."',
							szCurrency = '".(int)$this->idOriginCurrency."',
							iActive = '1'
						WHERE
							idWarehouseFrom = '".(int)$this->idOriginWarehouse."'	
						AND
							idWarehouseTo = '".(int)$this->idDestinationWarehouse."'	
						AND
							szOriginDestination = '1'	
					";
					//echo "<br>".$query."<br>";
					if($reusult=$this->exeSQL($query))
					{
						$success = true;
					}
					else
					{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					}
				}
				else
				{
					$addOboCCAry = array();
					$addOboCCAry['idOriginWarehouse'] = $this->idOriginWarehouse ;
					$addOboCCAry['idDestinationWarehouse'] = $this->idDestinationWarehouse ;
					$addOboCCAry['fOriginPrice'] = $this->fOriginPrice ;
					$addOboCCAry['idOriginCurrency'] = $this->idOriginCurrency ;
					$this->addOBOCustomClearance($addOboCCAry);
					$success = true;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}			
		}
		else
		{
			$this->inactivateCustomClearance($this->idOriginWarehouse,$this->idDestinationWarehouse,1);
		}
		
		if($update_destination_cc)
		{
			$query="
				SELECT
					*
				FROM
					".__DBC_SCHEMATA_PRICING_CC__."
				WHERE
					idWarehouseFrom='".(int)$this->idOriginWarehouse."'
				AND
					idWarehouseTo='".(int)$this->idDestinationWarehouse."'
				AND
					szOriginDestination='2'
				";
				//echo $query."<br/>";
				if($result=$this->exeSQL($query))
				{
					if($this->iNumRows>0)
					{
						$idAdmin = (int)$_SESSION['admin_id'];
							$idForwarderContact = 0 ;
						
						$row=$this->getAssoc($result);					
						$query="
							INSERT INTO
								".__DBC_SCHEMATA_FORWARDER_PRCIING_CC_ARCHIVE__."
							(
								idWarehouseFrom,
								idWarehouseTo,
								szOriginDestination,
								fPrice,
								szCurrency, 
								dtUpdateOn,
								iActive,
								idForwarder,
								idForwarderContact,
								idAdmin,
								dtCreatedOn
							)
							VALUES
							(
								'".(int)$this->idOriginWarehouse."',
								'".(int)$this->idDestinationWarehouse."',
								'2',
								'".(float)$row['fPrice']."',
								'".(int)$row['szCurrency']."',
								NOW(),
								'".(int)$row['iActive']."',
								'".(int)$idForwarder."',
								'".(int)$idForwarderContact."',
								'".(int)$idAdmin."',
								'".mysql_escape_custom($row['dtCreatedOn'])."'					
							)
						";
						//echo $query."<br/>";
						$result=$this->exeSQL($query);
						$query="
							UPDATE
								".__DBC_SCHEMATA_PRICING_CC__."
							SET
								fPrice = '".(float)$this->fDestinationPrice."',
								szCurrency = '".(int)$this->idDestinationCurrency."',
								iActive = '1'	
							WHERE
								idWarehouseFrom = '".(int)$this->idOriginWarehouse."'	
							AND
								idWarehouseTo = '".(int)$this->idDestinationWarehouse."'	
							AND
								szOriginDestination = '2'	
								
						";
						//echo "<br>".$query."<br>";
						if($reusult=$this->exeSQL($query))
						{
							$success = true;
						}
						else
						{
							$this->error = true;
							$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
							$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
							return false;
						}
					}
					else
					{
						$addOboCCAry = array();
						$addOboCCAry['idOriginWarehouse'] = $this->idOriginWarehouse ;
						$addOboCCAry['idDestinationWarehouse'] = $this->idDestinationWarehouse ;
						$addOboCCAry['fDestinationPrice'] = $this->fDestinationPrice ;
						$addOboCCAry['idDestinationCurrency'] = $this->idDestinationCurrency ;
						$this->addOBOCustomClearance($addOboCCAry);
						$success = true;
					}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			$this->inactivateCustomClearance($this->idOriginWarehouse,$this->idDestinationWarehouse,2);
		}
		
		$kForwarder = new cForwarder();
		$idForwarder = $_SESSION['forwarder_id'];
		$kForwarder->load($idForwarder);
		
		if(($kForwarder->iAgreeTNC==1) && ($kForwarder->iUpdateRss==2))
		{
			
			$kConfig = new cConfig();
			$kWhsSearch = new cWHSSearch();
			
			$kWhsSearch->load($this->idOriginWarehouse);
			$szOriginCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
		
			$kWhsSearch->load($this->idDestinationWarehouse);
			$szDestinationCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
			
			$replace_ary = array();
			$replace_ary['szForwarderDisplayName'] = $kForwarder->szDisplayName ;
			$replace_ary['szCountryName'] = $szOriginCountry ;
			$replace_ary['szOriginCountry'] = $szOriginCountry ;
			$replace_ary['szDestinationCountry'] = $szDestinationCountry ;
			
			$content_ary = array();
			$content_ary = $this->replaceRssContent('__OBO_CUSTOM_CLEARANCE__',$replace_ary);
			
			$rssDataAry=array();
			$rssDataAry['idForwarder'] = $kForwarder->id ;
			$rssDataAry['szFeedType'] = '__OBO_CUSTOM_CLEARANCE__';
			$rssDataAry['szLink'] = __RSS_FEED_PAGE_URL__ ;
			$rssDataAry['szTitle'] = $content_ary['szTitle'];
			$rssDataAry['szDescription'] = $content_ary['szDescription'];
			$rssDataAry['szDescriptionHTML'] = $content_ary['szDescriptionHTML'];
						
			$kRss = new cRSS();
			$kRss->addRssFeed($rssDataAry);
		}
		return $success;
	}
	
	
	function getWarehouseHaulagModelDetail($idForwarder,$idCurrency)
	{
			$wareHouseHaulageModelArr=array();
			$query="
				SELECT 
                                    hpm.idHaulageModel,
                                    w.id as idWarehouse,
                                    w.szWareHouseName,
                                    w.idCountry
				FROM
                                    ".__DBC_SCHEMATA_HAULAGE_PRICING__." AS h
				INNER JOIN
                                    ".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." AS hpm
				ON
                                    hpm.id=h.idHaulagePricingModel
				INNER JOIN
                                    ".__DBC_SCHEMATA_WAREHOUSES__." AS w
				ON
                                    w.id=hpm.idWarehouse
				INNER JOIN
                                    ".__DBC_SCHEMATA_FORWARDERS__." AS f
				ON
                                    w.idForwarder=f.id
				WHERE
                                    w.idForwarder='".(int)$idForwarder."'
				AND
				 	h.idCurrency = ".$idCurrency."				
				 AND
					h.iActive = '0'
				AND
					hpm.iActive='1'
				AND
					f.iActive='1'
				AND
					f.isOnline='1' 
				GROUP BY
					h.idHaulagePricingModel
				ORDER BY
					w.szWareHouseName ASC			
				";
			//echo $query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				while($row=$this->getAssoc($result))
				{
					$wareid_haulage=$row['idWarehouse']."_".$row['idHaulageModel'];
					if(!in_array($wareid_haulage,$wareHouseHaulageModelArr))
					{
						$wareHouseHaulageModelArr[]=$wareid_haulage;
						$res_arr[]=$row;
					}
				}
				
				return $res_arr;
			}
	}
	
	function getAllUpdatedHaulageModelExistsInActiveService($idhaulageWarehouseArr,$idCurrency)
	{
		if(!empty($idhaulageWarehouseArr))
		{
			$i=0;
			$sql='';
			/*if($flag)
			{
				$sql .="
					AND
						hp.iActive='1'
				";
			}*/
			foreach($idhaulageWarehouseArr as $idhaulageWarehouseArrs)
			{		
				$query="
					SELECT
						hp.id
					FROM
						".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." AS hpm
					INNER JOIN
						".__DBC_SCHEMATA_HAULAGE_PRICING__." AS hp
					ON
						hp.idHaulagePricingModel=hpm.id  
					WHERE
						hpm.idWarehouse='".(int)$idhaulageWarehouseArrs['idWarehouse']."'
					AND
						hpm.idHaulageModel='".(int)$idhaulageWarehouseArrs['idPricingModel']."'
					AND
						hpm.iActive='1'
					AND 
						hp.iActive='0'
					AND 
						hp.idCurrency='".$idCurrency."'
				";
				//echo $query."<br/>";
				if( ( $result1 = $this->exeSQL( $query ) ) )
				{
					if ($this->getRowCnt() > 0)
					{
						while($row1 = $this->getAssoc($result1))
						{
							++$i;
						}
					}
				}	
			}
			//echo $i;
			return $i;
		}
		else
		{
			return 0;
		}
	}
	
	function getInactiveCCData($idForwarder,$idCurrency)
	{
			$ccArr=array();
			$query="
				SELECT 
					cc.idWarehouseFrom,
					cc.idWarehouseTo	
				FROM
					".__DBC_SCHEMATA_PRICING_CC__." AS cc
				INNER JOIN
					".__DBC_SCHEMATA_WAREHOUSES__." AS w
				ON
					w.id=cc.idWarehouseFrom
				INNER JOIN
					".__DBC_SCHEMATA_FORWARDERS__." AS f
				ON
					f.id=w.idForwarder
				WHERE
				 	cc.szCurrency = ".$idCurrency."	
				 AND
					cc.iActive = 0
				 AND
					f.iActive='1'
				 AND
					f.isOnline='1'
				AND
					w.idForwarder='".(int)$idForwarder."'	 
				";
			//echo "<br/><br/>".$query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{					
				while($row=$this->getAssoc($result))
				{
					$wf_wt=$row['idWarehouseFrom']."_".$row['idWarehouseTo'];
					if(!in_array($wf_wt,$ccArr))
					{
						$res_arr[]=$row;
						
						$ccArr[]=$wf_wt;
					}
				}
				return 	$res_arr;
			}
			else
			{
				return array();
			}
	}
	
	function getInactiveLCLData($idForwarder,$idCurrency)
	{
		$lclArr=array();
		$query="
			SELECT 
				wtw.idWarehouseFrom,
				wtw.idWarehouseTo 	
			FROM
				".__DBC_SCHEMATA_WAREHOUSES_PRICING__." AS wtw
			INNER JOIN
				".__DBC_SCHEMATA_WAREHOUSES__." AS w
			ON
				w.id=wtw.idWarehouseFrom
			INNER JOIN
				".__DBC_SCHEMATA_FORWARDERS__." AS f
			ON
				f.id=w.idForwarder
			WHERE
			(
					wtw.szOriginChargeCurrency = ".$idCurrency."
				OR
					wtw.szFreightCurrency= ".$idCurrency."
				OR
			 		wtw.szDestinationChargeCurrency = ".$idCurrency."
			 )
			AND
				wtw.iActive = 0
			AND
				f.iActive='1'
			 AND
				f.isOnline='1'
			AND
				w.idForwarder='".(int)$idForwarder."'				
			";
		//echo $query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{	
			while($row=$this->getAssoc($result))
			{
				$wf_wt=$row['idWarehouseFrom']."_".$row['idWarehouseTo'];
				if(!in_array($wf_wt,$lclArr))
				{
					$res_arr[]=$row;
					
					$lclArr[]=$wf_wt;
				}
			}
			return 	$res_arr;
		}
		else
		{
			return array();
		}
	}
	
	function checkCurrencyValid($idCurrency)
	{
		if((int)$idCurrency>0)
		{
			$query="
				SELECT
					id
				FROM	
					".__DBC_SCHEMATA_CURRENCY__."
				WHERE
					id='".(int)$idCurrency."'
				AND
					iPricing='0'		
			";
			if( ( $result = $this->exeSQL( $query ) ) )
			{
				if ($this->getRowCnt() > 0)
				{
					$row=$this->getAssoc($result);
					if($row['id']>0)
					{
						return true;
					}
				}
				else
				{
					return false;
				}
			}
		}
		else
		{
			return false;
		}
	}
	
	function updateLCLServiceVideoFlag($idForwarderContact,$updateFlag)
	{
		if((int)$idForwarderContact>0)
		{
			$query="
				UPDATE
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
				SET
					iLCLServiceVideo='".(int)$updateFlag."'
				WHERE
					id='".(int)$idForwarderContact."'
			";
			//echo $query;
			$result=$this->exeSQL($query);
		}
	}
        
        function getAllUpdatedServicesCC($orginarr,$desarr)
	{
		if(!empty($orginarr))
		{
			$i=0;
			foreach($orginarr as $orginarrs)
			{		
			
                            $result_array =array(); 
                            $array_orign = array();
                            foreach($desarr as $desarrs)
                            {

                                if($desarrs!=$orginarrs)
                                {
                                    $query="SELECT
						id
                                            FROM
                                                    ".__DBC_SCHEMATA_PRICING_CC__."
                                            WHERE
                                                idWarehouseFrom='".(int)$orginarrs."'
                                            AND
                                                idWarehouseTo='".(int)$desarrs."'
                                            AND
                                                iActive='1'
                                            ";
                                   // echo $query;
                                    if( ( $result1 = $this->exeSQL( $query ) ) )
                                    {

                                            if ($this->getRowCnt() > 0)
                                            {
                                                    $row1 = $this->getAssoc($result1);
                                                    if((int)$row1['id']>0)
                                                    {
                                                        ++$i;
                                                    }

                                            }							
                                    }
                                }
                            }
                        }
                        
                        return $i;
                }
        }
	function set_id( $value )
	{
		$this->id = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "id", t($this->t_base.'fields/lcl_id'), false, false, true );
	}
	function set_idOriginWarehouse( $value )
	{   
		$this->idOriginWarehouse = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idOriginWarehouse", t($this->t_base.'fields/origin')." ".t($this->t_base.'fields/cfs_name'), false, false, true );
	}
	function set_idOriginCountry( $value )
	{   
		$this->idOriginCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idOriginCountry", t($this->t_base.'fields/origin')." ".t($this->t_base.'fields/country'), false, false, true );
	}
	function set_szOriginCity( $value )
	{   
		$this->szOriginCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idOriginCity", t($this->t_base.'fields/origin')." ".t($this->t_base.'fields/city'), false, false, false );
	}
	
	function set_idDestinationWarehouse( $value )
	{   
		$this->idDestinationWarehouse = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idDestinationWarehouse", t($this->t_base.'fields/destination')." ".t($this->t_base.'fields/cfs_name'), false, false, true );
	}
	function set_idDestinationCountry( $value )
	{   
		$this->idDestinationCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idDestinationCountry", t($this->t_base.'fields/destination')." ".t($this->t_base.'fields/country'), false, false, true );
	}
	function set_szDestinationCity( $value )
	{   
		$this->szDestinationCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idDestinationCity", t($this->t_base.'fields/destination')." ".t($this->t_base.'fields/city'), false, false, false );
	}
	
	function set_fOriginPrice( $value )
	{   
		$this->fOriginPrice = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fOriginPrice", t($this->t_base.'fields/export')." ".t($this->t_base.'fields/price'), false, false, true );
	}
	function set_idOriginCurrency( $value )
	{   
		$this->idOriginCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idOriginCurrency", t($this->t_base.'fields/export')." ".t($this->t_base.'fields/currency'), false, false, true );
	}
	function set_fDestinationPrice( $value )
	{   
		$this->fDestinationPrice = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fDestinationPrice", t($this->t_base.'fields/import')." ".t($this->t_base.'fields/price'), false, false, true );
	}
	function set_idDestinationCurrency( $value )
	{   
		$this->idDestinationCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idDestinationCurrency", t($this->t_base.'fields/import')." ".t($this->t_base.'fields/currency'), false, false, true );
	}
	
	function set_szCurrency( $value )
	{
		$this->szCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "szCurrency", t($this->t_base.'fields/currency'), 1, false, true );
	}
	
	function set_iCrosssBorder( $value )
	{
		$this->iCrosssBorder = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iCrosssBorder", t($this->t_base.'fields/cross_border'), 0, false, true );
	}
	
	function set_PerWM( $value )
	{
		$this->fPerWM = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "per_wm", t($this->t_base.'fields/per_wm'), 0, false, true );
	}
	
	function set_Minimum( $value )
	{
		$this->fMinimum = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "minimum", t($this->t_base.'fields/minimum'), 0, false, true );
	}
	
	function set_PerBooking( $value )
	{
		$this->fPerBooking = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "per_booking", t($this->t_base.'fields/per_booking'), 0, false, true );
	}
	
	function set_Distance( $value )
	{
		$this->distance = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "upto", t($this->t_base.'fields/distance_up_to'), 0, false, true );
	}	
	function set_idTransitTime( $value )
	{
		$this->idTransitTime = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idTransitTime", t($this->t_base.'fields/haulage_transit_time'), 0, false, true );
	}	
	function set_expiryDate( $value )
	{
		$this->expiryDate = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "datepicker1", t($this->t_base.'fields/expiry_date'), false, 255, true );
	}
	function set_iBookingCutOffHours( $value )
	{
		$this->iBookingCutOffHours = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iBookingCutOffHours", t($this->t_base.'fields/booking_cut_off'), 0, false, true );
	}
	function set_szCutOffLocalTime( $value )
	{
		$this->szCutOffLocalTime = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCutOffLocalTime", t($this->t_base.'fields/cut_off_local_time'), false, 255, true );
	}
	function set_idCutOffDay( $value )
	{
		$this->idCutOffDay = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idCutOffDay", t($this->t_base.'fields/cut_off_day'), 0, false, true );
	}
	function set_szAvailableLocalTime( $value )
	{
		$this->szAvailableLocalTime = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAvailableLocalTime", t($this->t_base.'fields/available_local_time'), false, 255, true );
	}
	function set_idAvailableDay( $value )
	{
		$this->idAvailableDay = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idAvailableDay", t($this->t_base.'fields/available_day'), 0, false, true );
	}
	function set_iTransitHours( $value )
	{
		$this->iTransitHours = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iTransitHours", t($this->t_base.'fields/transit_hour'), 1, 100, true );
	}
	function set_idFrequency( $value )
	{
		$this->idFrequency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idFrequency", t($this->t_base.'fields/service_frequency'), 0, false, true );
	}

	function set_fOriginRateWM( $value,$flag=false)
	{
		$this->fOriginRateWM = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fOriginRateWM", t($this->t_base.'fields/origin_freight_rate')." ".t($this->t_base.'fields/per_wm'), 0, false, $flag );
	}
	function set_fOriginMinRateWM( $value,$flag=false )
	{
		$this->fOriginMinRateWM = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fOriginMinRateWM", t($this->t_base.'fields/origin_freight_rate')." ".t($this->t_base.'fields/minimum'), 0, false, $flag );
	}
	function set_fOriginRate( $value ,$flag=false)
	{
		$this->fOriginRate = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fOriginRate", t($this->t_base.'fields/origin_freight_rate')." ".t($this->t_base.'fields/per_booking'), 0, false, $flag );
	}
	function set_szOriginFreightCurrency( $value, $flag=false )
	{
		$this->szOriginFreightCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "szOriginFreightCurrency", t($this->t_base.'fields/origin_freight_rate')." ".t($this->t_base.'fields/currency'), 0, false, $flag );
	}	
	
	function set_fRateWM( $value )
	{
		$this->fRateWM = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fRateWM", t($this->t_base.'fields/freight_rate')." ".t($this->t_base.'fields/per_wm'), 0, false, true );
	}
	function set_fMinRateWM( $value )
	{
		$this->fMinRateWM = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fMinRateWM", t($this->t_base.'fields/freight_rate')." ".t($this->t_base.'fields/minimum'), 0, false, true );
	}
	function set_fRate( $value )
	{
		$this->fRate = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fRate", t($this->t_base.'fields/freight_rate')." ".t($this->t_base.'fields/per_booking'), 0, false, true );
	}
	function set_szFreightCurrency( $value )
	{
		$this->szFreightCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "szFreightCurrency", t($this->t_base.'fields/freight_rate')." ".t($this->t_base.'fields/currency'), 0, false, true );
	}	
	function set_fDestinationRateWM( $value ,$flag=false )
	{
		$this->fDestinationRateWM = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fDestinationRateWM", t($this->t_base.'fields/destination_freight_rate')." ".t($this->t_base.'fields/per_wm'), 0, false, $flag );
	}
	function set_fDestinationMinRateWM( $value,$flag=false )
	{
		$this->fDestinationMinRateWM = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fDestinationMinRateWM", t($this->t_base.'fields/destination_freight_rate')." ".t($this->t_base.'fields/minimum'), 0, false, $flag );
	}
	function set_fDestinationRate( $value,$flag=false )
	{
		$this->fDestinationRate = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fDestinationRate", t($this->t_base.'fields/destination_freight_rate')." ".t($this->t_base.'fields/per_booking'), 0, false, $flag );
	}
	function set_szDestinationFreightCurrency( $value ,$flag=false)
	{
		$this->szDestinationFreightCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "szDestinationFreightCurrency", t($this->t_base.'fields/destination_freight_rate')." ".t($this->t_base.'fields/currency'), 0, false, $flag );
	}	
	function set_dtValidFrom( $value )
	{   
		$this->dtValidFrom = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "dtValidFrom", t($this->t_base.'fields/validity')." ".t($this->t_base.'fields/from'), false, false, true );
	}
	function set_dtExpiry( $value )
	{   
		$this->dtExpiry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "dtExpiry", t($this->t_base.'fields/validity')." ".t($this->t_base.'fields/to'), false, false, true );
	}	
}