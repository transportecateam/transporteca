<?php
/**
 * This file is the container for all customer's paypal payment related functionality.
 * All functionality related to user payment should be contained in this class.
 *
 * paypal.class.php
 *
 * @copyright Copyright (C) 2012 Transport-ece
 * @author Ashish
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

class cPayPal extends cDatabase
{
	
	function __construct()
	{
		parent::__construct();
		return true;
	}
	
	
	public function add($paypalPaymentLog)
	{
		//print_r($paypalPaymentLog);
		if(!empty($paypalPaymentLog))
		{
			$paymentdate=date('Y-m-d H:i:s',strtotime($paypalPaymentLog['payment_date']));
			
			$insertFlag=true;
			$query="
				SELECT
                                    id
				FROM
                                    ".__DBC_SCHEMATA_PAYMENT_PAYPAL_LOG__."
				WHERE
                                    idTxn='".mysql_escape_custom($paypalPaymentLog['txn_id'])."'
			";
			if($result=$this->exeSQL($query))
			{
                            if($this->iNumRows>0)
                            {
                                $row=$this->getAssoc($result);
                                if($row['id']>0)
                                {
                                    $insertFlag=false;
                                }
                            }
			}
			
			if($insertFlag)
			{
				$query="
					INSERT INTO
						".__DBC_SCHEMATA_PAYMENT_PAYPAL_LOG__."
						(
							szBookingReference,
							szTransactionSubject,
							szTxnType,
							dtPayment,
							szFirstName,
							szLastName,
							szResidenceCountry,
							szPendingReason,
							fPaymentGross,
							szMcCurrency,
							szBusiness,
							szPaymentType,
							szProtectionElegebility,
							szVerifySign,
							iTestIPN,
							fTax,
							szPayerEmail,
							idTxn,
							iQuantity,
							szReceiverEmail,
							idPayer,
							idReceiver,
							fHandlingAmount,
							szPaymentStatus,
							fShipping,
							fMcGross,
							szCustom,
							szCharSet,
							szNotifyVersion,
							idIpnTracker
						)
							VALUES
						(
							'".mysql_escape_custom($paypalPaymentLog['item_number'])."',
							'".mysql_escape_custom($paypalPaymentLog['transaction_subject'])."',
							'".mysql_escape_custom($paypalPaymentLog['txn_type'])."',
							'".mysql_escape_custom($paymentdate)."',
							'".mysql_escape_custom($paypalPaymentLog['first_name'])."',
							'".mysql_escape_custom($paypalPaymentLog['last_name'])."',
							'".mysql_escape_custom($paypalPaymentLog['residence_country'])."',
							'".mysql_escape_custom($paypalPaymentLog['pending_reason'])."',
							'".mysql_escape_custom($paypalPaymentLog['payment_gross'])."',
							'".mysql_escape_custom($paypalPaymentLog['mc_currency'])."',
							'".mysql_escape_custom($paypalPaymentLog['business'])."',
							'".mysql_escape_custom($paypalPaymentLog['payment_type'])."',
							'".mysql_escape_custom($paypalPaymentLog['protection_eligibility'])."',
							'".mysql_escape_custom($paypalPaymentLog['verify_sign'])."',
							'".mysql_escape_custom($paypalPaymentLog['test_ipn'])."',
							'".mysql_escape_custom($paypalPaymentLog['tax'])."',
							'".mysql_escape_custom($paypalPaymentLog['payer_email'])."',
							'".mysql_escape_custom($paypalPaymentLog['txn_id'])."',
							'".mysql_escape_custom($paypalPaymentLog['quantity'])."',
							'".mysql_escape_custom($paypalPaymentLog['receiver_email'])."',
							'".mysql_escape_custom($paypalPaymentLog['payer_id'])."',
							'".mysql_escape_custom($paypalPaymentLog['receiver_id'])."',
							'".mysql_escape_custom($paypalPaymentLog['handling_amount'])."',
							'".mysql_escape_custom($paypalPaymentLog['payment_status'])."',
							'".mysql_escape_custom($paypalPaymentLog['shipping'])."',
							'".mysql_escape_custom($paypalPaymentLog['mc_gross'])."',
							'".mysql_escape_custom($paypalPaymentLog['custom'])."',
							'".mysql_escape_custom($paypalPaymentLog['charset'])."',
							'".mysql_escape_custom($paypalPaymentLog['notify_version'])."',
							'".mysql_escape_custom($paypalPaymentLog['ipn_track_id'])."'
						)			
				";
				//echo $query;
				if($result = $this->exeSQL($query))
				{
					if(!empty($paypalPaymentLog['item_number']))
					{
                                            $query="
                                                SELECT
                                                    fTotalPriceCustomerCurrency,
                                                    id,
                                                    idForwarder
                                                FROM
                                                    ".__DBC_SCHEMATA_BOOKING__."
                                                 WHERE
                                                    szBookingRef='".mysql_escape_custom(trim($paypalPaymentLog['item_number']))."'
                                            ";
                                            if($result=$this->exeSQL($query))
                                            {
                                                if($this->iNumRows>0)
                                                {
                                                    $row=$this->getAssoc($result);
                                                    $totalamount=$row['fTotalPriceCustomerCurrency'];
                                                    $idForwarder=$row['idForwarder'];
                                                    $idBooking=$row['id'];
                                                }
                                            }

                                            $kBooking = new cBooking();
                                            $postSearchAry = $kBooking->getExtendedBookingDetails($idBooking);

                                            $bQuickQuoteBooking = false;
                                            if($postSearchAry['iQuickQuote']==__QUICK_QUOTE_MAKE_BOOKING__)
                                            {
                                                $bQuickQuoteBooking = true;
                                            }
                                            if($postSearchAry['szPaymentStatus']=='' && ($postSearchAry['idBookingStatus']!=3 || $bQuickQuoteBooking))
                                            {
                                                if($bQuickQuoteBooking)
                                                {
                                                    //In case of Quick quote bookings we already have update Invoice numer for the booking
                                                }
                                                else
                                                {
                                                    $invoiceNumber = '';
                                                    if($postSearchAry['iInsuranceIncluded']==1)
                                                    {
                                                        $kAdmin  =new cAdmin();
                                                        //$invoiceNumber=$kAdmin->generateInvoiceForPayment();
                                                        $invoiceNumber = $postSearchAry['szInvoice'];
                                                    }
                                                    $query_update = ", szInsuranceInvoice = '".mysql_escape_custom($invoiceNumber)."' ";
                                                }
							
                                                $query="
                                                    UPDATE
                                                        ".__DBC_SCHEMATA_BOOKING__."
                                                    SET
                                                        idTransaction='".mysql_escape_custom($paypalPaymentLog['txn_id'])."',
                                                        szPaymentStatus='".mysql_escape_custom($paypalPaymentLog['payment_status'])."',
                                                        fCustomerPaidAmount= '".mysql_escape_custom((float)$paypalPaymentLog['mc_gross'])."'
                                                        $query_update
                                                     WHERE
                                                        szBookingRef='".mysql_escape_custom(trim($paypalPaymentLog['item_number']))."'
                                                ";
                                                $result = $this->exeSQL($query);

                                                if($paypalPaymentLog['payment_status']=='Completed')
                                                {
                                                    $query="
                                                        UPDATE
                                                            ".__DBC_SCHEMATA_BOOKING__."
                                                        SET
                                                            idBookingStatus='3'
                                                         WHERE
                                                            szBookingRef='".mysql_escape_custom(trim($paypalPaymentLog['item_number']))."'
                                                    ";
                                                    $result = $this->exeSQL($query);
								 
                                                    //$max_number_used_for_forwarder = $kForwarder->getMaximumUsedNumberByForwarder($postSearchAry['idForwarder']);
                                                    if($bQuickQuoteBooking)
                                                    {
                                                        //for quick quote we already have updated invoice number when we created this
                                                    }
                                                    else
                                                    {
                                                        //$kBooking->generateInvoice($idForwarder,true);
                                                        $kAdmin = new cAdmin();
                                                        $szTransportecInvoice = $kAdmin->generateInvoiceForPayment(true);
                                                        $updateAry['szInvoice'] = $szTransportecInvoice;
                                                    } 
                                                    $updateAry['dtBookingConfirmed'] = date('Y-m-d H:i:s');
                                                    $updateAry['idBookingStatus'] = 3 ;
                                                    $updateAry['iBookingStep'] = 14 ;

                                                    if(!empty($updateAry))
                                                    {
                                                        foreach($updateAry as $key=>$value)
                                                        {
                                                            $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                                                        }
                                                    }
                                                    $update_query = rtrim($update_query,",");
                                                    $kBooking->updateDraftBooking($update_query,$idBooking);	
                                                    $bookingDataArr=$kBooking->getExtendedBookingDetails($idBooking);

                                                    if(!$bQuickQuoteBooking)
                                                    {
                                                        $kBooking->addrForwarderTranscaction($bookingDataArr);
                                                    } 
                                                }
                                            }
						/*else
						{
							$query="
								UPDATE
									".__DBC_SCHEMATA_BOOKING__."
								SET
								 	szPaymentStatus='Failed'
								 WHERE
								 	szBookingRef='".$paypalPaymentLog['item_number']."'
							";
							$result = $this->exeSQL($query);
						}*/
					}
					return true;
				}
			}
		}
	}
	
	
	public function getBookingStatusPaypalPayment($item_number)
	{
			$res_arr=array();
			$query="
				SELECT
					szPaymentStatus,
					idBookingStatus
				FROM
					".__DBC_SCHEMATA_BOOKING__."
				 WHERE
				 	szBookingRef='".mysql_escape_custom(trim($item_number))."'
			";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					$res_arr[]=$row;
					
					return $res_arr;
				}
				else
				{
					return $res_arr;
				}
			}
			else
			{
				return $res_arr;
			}
	}

}