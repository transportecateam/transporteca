<?php
/**
 * This file is the container for all billing related functionality.
 * All functionality related to billing details should be contained in this class.
 *
 * billing.class.php
 *
 * @copyright Copyright (C) 2012 Transport-ece
 * @author Ajay
 */
if(!isset($_SESSION))
{
    session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/classes/parser.class.php" );

class cCapsuleCrm extends cDatabase
{  
    function __construct()
    {
        parent::__construct();
        return true;
    }   
    
    /**
     * This function is used to add customer details to Capsule CRM
     * @access public
     * @param string
     * @return array 
     * @author Ajay
     */ 
    
    public function addCustomerDetailsToCapsule($szCustomerXml,$szCapsulePrtyID=false)
    {	
        if(!empty($szCustomerXml))
        {
            if(!empty($szCapsulePrtyID))
            {
                $szCapsuleHostUrl =  __CAPSULE_API_HOST_URL__."/api/person/".$szCapsulePrtyID; 
            }
            else
            {
                $szCapsuleHostUrl =  __CAPSULE_API_HOST_URL__."/api/person"; 
            }
            
            $kAdmin = new cAdmin();
            $szCapsuleAPIUserName = $kAdmin->getCapsuleCrmAPIKey();
             
            $szCapsuleAPIPassword = __CAPSULE_API_PASSWORD__ ;

            $szCustomerXml = trim($szCustomerXml);
            $ch = curl_init($szCapsuleHostUrl);
            $options = array(
                CURLOPT_USERPWD => $szCapsuleAPIUserName.':'.$szCapsuleAPIPassword,
                CURLOPT_HTTPHEADER => array('Content-Type: application/xml'), 
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HEADER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $szCustomerXml
            );
             
            $cUrlResponseAry = array();
            try
            { 
                curl_setopt_array($ch, $options);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
                $response = curl_exec($ch);
                
                print_R($response);
                
                $szHttpResponseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE) ; 
            }
            catch (Exception $e) { 
                
                $szErrorString = "\n\n EXCEPTION:: Function::addCustomerDetailsToCapsule Timestamp: ".date('Y-m-d H:i:s')."\n\n";
                $szErrorString .= " Response XML: \n\n ".print_R($e->getMessage(),true)."\n\n" ; 

                $filename = __APP_PATH__."/logs/capsule_success.log";

                $strdata=array();
                $strdata[0]= $szErrorString;
                file_log(true, $strdata, $filename);
            }   
            
//            if(trim($szHttpResponseCode)=='400')
//            { 
//                $curlResponseInfoAry = array();
//                $curlResponseInfoAry = curl_getinfo($ch);
//                echo "<br><br>XML:  ".$szCustomerXml."<br><br>" ;
//                print_R($curlResponseInfoAry);
//                curl_close($ch); 
//                die;
//            }
            /* 
             * Status Code: 201 means user created successfully
             */
            if(trim($szHttpResponseCode)=='201')
            {  
                $szErrorString = "\n\n SUCCESS:: Function::addCustomerDetailsToCapsule Timestamp: ".date('Y-m-d H:i:s')."\n\n";
                $szErrorString .= " Response XML: \n\n ".print_R($response,true)."\n\n" ; 

                $filename = __APP_PATH__."/logs/capsule_success.log";

                $strdata=array();
                $strdata[0]= $szErrorString;
                file_log(true, $strdata, $filename);
            
                list($headers) = explode("\r\n\r\n", $response);
                
                // $headers now has a string of the HTTP headers 
                $headers = explode("\n", $headers);
                $szCapsulePrtyID = '';
                  
                foreach($headers as $header) 
                {
                    if(stripos($header, 'Location:') !== false) 
                    {
                        $urlAry = explode(":",$header) ;  
                        if(!empty($urlAry[2]))
                        {
                            $urlAry_1 = explode("/",$urlAry[2]); 
                            $iCount = count($urlAry_1);  
                            $szCapsulePrtyID = $urlAry_1[$iCount-1] ; 
                        } 
                    }
                } 
                
                curl_close($ch);
                if(empty($szCapsulePrtyID))
                {
                    $pos = strpos($response, "Location:"); 
                    $szSUbString = substr($response,$pos);
                    if(!empty($szSUbString))
                    {
                        $urlAry = explode(" ",$szSUbString) ; 
                        if(!empty($urlAry[1]))
                        { 
                            $pos = strpos($response, "Content-Length:"); 
                            $szSubString = substr($response,0,$pos);
                            if(!empty($szSubString))
                            {
                                $urlAry_1 = explode("/",$szSubString); 
                                $iCount = count($urlAry_1);  
                                $szCapsulePrtyID = $urlAry_1[$iCount-1] ;
                            }
                        } 
                    }
                }    
                return $szCapsulePrtyID ;
            }
            else
            {
                $szErrorString = "\n\n ERROR:: Function::addCustomerDetailsToCapsule Timestamp: ".date('Y-m-d H:i:s')."\n\n";
                $szErrorString .= " Request XML: \n\n ".$szCustomerXml."\n\n" ;

                $curlResponseInfoAry = array();
                $curlResponseInfoAry = curl_getinfo($ch);
                $szErrorString .= "Curl Response: \n\n ".curl_error($ch)."\n\n Curl Info: ".print_R($curlResponseInfoAry,true)."\n\n ";
  
                $filename = __APP_PATH__."/logs/capsule_error.log";
					
                $strdata=array();
                $strdata[0]= $szErrorString;
                file_log(true, $strdata, $filename);
                
                curl_close($ch);
                return false;
            }  
        }
    }
    
    /**
     * This function is used to create Request XML based on Customer details
     * @access public
     * @param int
     * @return string 
     * @author Ajay
     */
    public function createXmlForCapsuleCRM($idUser)
    {
        if($idUser>0)
        { 
            $kUser = new cUser();
            $kUser->getUserDetails($idUser);

            $kConfig_new = new cConfig();
            $kConfig_new->loadCountry($kUser->szCountry);
            $szCountryName = $kConfig_new->szCountryName; 

            $kConfig_new->loadCountry($kUser->idInternationalDialCode);
            $iInternationDialCode = $kConfig_new->iInternationDialCode; 
/*
            $crmXmlAry = array();
            $crmXmlAry['person']['title'] = '';
            $crmXmlAry['person']['firstName'] = $kUser->szFirstName ;
            $crmXmlAry['person']['lastName'] = $kUser->szLastName  ;
            $crmXmlAry['person']['organisationName'] = $kUser->szCompanyName ;
            $crmXmlAry['person']['jobTitle'] = '';
            $crmXmlAry['person']['about'] = "";
            $crmXmlAry['person']['contacts']['address']['type'] = "Office";
            $crmXmlAry['person']['contacts']['address']['street'] = $kUser->szAddress1." ".$kUser->szAddress2." ".$kUser->szAddress3;
            $crmXmlAry['person']['contacts']['address']['city'] = $kUser->szCity;
            $crmXmlAry['person']['contacts']['address']['state'] = $kUser->szState;
            $crmXmlAry['person']['contacts']['address']['zip'] = $kUser->szPostCode;
            $crmXmlAry['person']['contacts']['address']['country'] = $szCountryName; 
            $crmXmlAry['person']['contacts']['email']['type'] = 'Home';
            $crmXmlAry['person']['contacts']['email']['emailAddress'] = $kUser->szEmail;
            $crmXmlAry['person']['contacts']['phone']['type'] = "Mobile";
            $crmXmlAry['person']['contacts']['phone']['phoneNumber'] = "+".$iInternationDialCode." ".$kUser->szPhoneNumber; 

            print_R($crmXmlAry);
            echo "<br><br><br>";
            $crmXmlString = cArray2XML::createXML($crmXmlAry);
* *
*/

            $szStreetAddress = $kUser->szAddress1 ;
            if(!empty($kUser->szAddress2))
            {
                $szStreetAddress .= " ".$kUser->szAddress2 ;
            }
            if(!empty($kUser->szAddress3))
            {
                $szStreetAddress .= " ".$kUser->szAddress3 ;
            }
            $szPhoneNumber = "+".$iInternationDialCode." ".$kUser->szPhoneNumber;

            if(empty($kUser->szCompanyName))
            {
                $kUser->szCompanyName = 'Private';
            }
            $szPhoneNumber = trim($szPhoneNumber);
            $szStreetAddress = trim($szStreetAddress); 
            $szCountryName = trim($szCountryName);
            
            $szDefaultString = '""';
            
            if(empty($szStreetAddress))
            {
                $szStreetAddress = $szDefaultString;
            }
            if(empty($kUser->szCity))
            {
                $kUser->szCity = $szDefaultString;
            }
            if(empty($kUser->szState))
            {
                $kUser->szState = $szDefaultString;
            }
            if(empty($kUser->szPostCode))
            {
                $kUser->szPostCode = $szDefaultString;
            }
            if(empty($szCountryName))
            {
                $szCountryName = $szDefaultString;
            } 
            if(empty($szPhoneNumber))
            {
                $szPhoneNumber = $szDefaultString;
            }  
            if(empty($kUser->szFirstName))
            {
                $kUser->szFirstName = $szDefaultString;
            }
            if(empty($kUser->szLastName))
            {
                $kUser->szLastName = $szDefaultString;
            }
            
            $crmXmlString = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <person> 
                  <contacts>
                    <address>
                      <type>Office</type>
                      <street>'.$szStreetAddress.'</street>
                      <city>'.$kUser->szCity.'</city>
                      <state>'.$kUser->szState.'</state>
                      <zip>'.$kUser->szPostCode.'</zip>
                      <country>'.$szCountryName.'</country>
                    </address>
                    <email>
                      <type>Home</type>
                      <emailAddress>'.$kUser->szEmail.'</emailAddress>
                    </email>
                    <phone>
                      <type>Mobile</type>
                      <phoneNumber>'.$szPhoneNumber.'</phoneNumber>
                    </phone> 
                    <website>
                      <type>work</type>
                      <webService>URL</webService>
                      <webAddress>NA</webAddress>
                    </website>
                  </contacts>   
                  <firstName>'.$kUser->szFirstName.'</firstName>
                  <lastName>'.$kUser->szLastName.'</lastName> 
                  <organisationName>'.$kUser->szCompanyName.'</organisationName>
                  <about>Added User From Transporteca</about>  
                </person>
            ';  
            $crmXmlString = str_replace("&", "&amp;", $crmXmlString);
            return $crmXmlString ;
        }
    }
    
    function createCustomFieldsXML($idUser)
    { 
        if($idUser>0)
        { 
            $kUser = new cUser();
            $kUser->getUserDetails($idUser);
            $bLiveFlag = true;
            
            if($kUser->iPrivate==1)
            {
                $szPrivate = 'Yes';
            }
            else
            {
                $szPrivate = 'No';
            }

            $kUser_new = new cUser();
            if($kUser->szCurrency>0)
            {
                $kUser_new->loadCurrency($kUser->szCurrency);
                $szCurrency = $kUser_new->szCurrencyName;
            }
            else
            {
                $szCurrency = 'DKK';
            }
            $kConfig = new cConfig();
            $langArr=$kConfig->getLanguageDetails('',$kUser->iLanguage,'','','','','',false);
//            if($kUser->iLanguage==2)
//            {
//                $szLanguage = 'Danish';
//            }
//            else
//            {
                $szLanguage = $langArr[0]['szName'];
            //}

            if($kUser->iAcceptNewsUpdate==1)
            {
                $szNewsLetter = 'Yes';
            }
            else
            {
                $szNewsLetter = 'No';
            }

            $customerOwnerAry = array();
            $customerOwnerAry['1'] = 'Morten Find Lærkholm';
            $customerOwnerAry['5'] = 'Thorsten Boeck';
            $customerOwnerAry['10'] = 'Berit Hansen';

            $szCustomerOwner = '';
            if($kUser->idCustomerOwner>0)
            {
                $szCustomerOwner = $customerOwnerAry[$kUser->idCustomerOwner];
            }
            if($bLiveFlag)
            { 
                $crmXmlString = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                    <customFields>
                        <customField>  
                          <id>253796</id>
                          <label>Private</label>
                          <text>'.$szPrivate.'</text>
                        </customField>  
                        <customField>  
                          <id>268778</id>
                          <label>Currency</label>
                          <text>'.$szCurrency.'</text>
                        </customField>
                        <customField>  
                          <id>240790</id>
                          <label>Language</label>
                          <text>'.$szLanguage.'</text>
                        </customField>
                        <customField>  
                          <id>240789</id>
                          <label>Newsletter</label>
                          <text>'.$szNewsLetter.'</text>
                        </customField>
                        <customField>  
                          <id>228782</id>
                          <label>Customer Owner</label>
                          <text>'.$szCustomerOwner.'</text>
                        </customField>
                        <customField>  
                          <id>240791</id>
                          <label>Relationship</label>
                          <text>Customer</text>
                        </customField> 
                    </customFields>
                ';   
            }
            else
            {
                $crmXmlString = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                    <customFields>
                        <customField>  
                          <id>312607</id>
                          <label>Currency</label>
                          <text>'.$szCurrency.'</text>
                        </customField>
                        <customField>  
                          <id>1312608</id>
                          <label>Language</label>
                          <text>'.$szLanguage.'</text>
                        </customField>
                        <customField>  
                          <id>2312609</id>
                          <label>NewsLetter</label>
                          <boolean>'.$szNewsLetter.'</boolean>
                        </customField> 
                    </customFields>
                ';   
            }
            
            $crmXmlString = str_replace("&", "&amp;", $crmXmlString);
            return $crmXmlString ;
        }
    }
    
    function xml_encode($mixed, $domElement=null, $DOMDocument=null) 
    {
        if (is_null($DOMDocument)) {
            $DOMDocument =new DOMDocument;
            $DOMDocument->formatOutput = true;
            $this->xml_encode($mixed, $DOMDocument, $DOMDocument);
            return $DOMDocument->saveXML();
        }
        else {
            if (is_array($mixed)) {
                foreach ($mixed as $index => $mixedElement) {
                    if (is_int($index)) {
                        if ($index === 0) {
                            $node = $domElement;
                        }
                        else {
                            $node = $DOMDocument->createElement($domElement->tagName);
                            $domElement->parentNode->appendChild($node);
                        }
                    }
                    else {
                        $plural = $DOMDocument->createElement($index);
                        $domElement->appendChild($plural);
                        $node = $plural;
                        if (!(rtrim($index, 's') === $index)) {
                            $singular = $DOMDocument->createElement(rtrim($index, 's'));
                            $plural->appendChild($singular);
                            $node = $singular;
                        }
                    } 
                    $this->xml_encode($mixedElement, $node, $DOMDocument);
                }
        }
        else {
            $mixed = is_bool($mixed) ? ($mixed ? 'true' : 'false') : $mixed;
            $domElement->appendChild($DOMDocument->createTextNode($mixed));
        }
    }
}
    
    /**
     * This function is used to create Request XML based on Customer details
     * @access public
     * @param int
     * @return string 
     * @author Ajay
     */
    public function createXmlToUpdateCapsuleCRM($idUser)
    {
        if($idUser>0)
        { 
            $kUser = new cUser();
            $kUser->getUserDetails($idUser);

            $kConfig_new = new cConfig();
            $kConfig_new->loadCountry($kUser->szCountry);
            $szCountryName = $kConfig_new->szCountryName; 

            $kConfig_new->loadCountry($kUser->idInternationalDialCode);
            $iInternationDialCode = $kConfig_new->iInternationDialCode;  
             
            $capsuleCrmAry = array();
            $capsuleCrmAry = $kUser->getAllCrmIdsLogByUserid($idUser,$kUser->szCapsulePrtyID);
              
            $idCrmAddress = $capsuleCrmAry['szCapsuleAddressID'];
            $idCrmEmail = $capsuleCrmAry['szCapsuleEmailID'];
            $idCrmPhone = $capsuleCrmAry['szCapsulePhoneID'];
            
            $szStreetAddress = $kUser->szAddress1 ;
            if(!empty($kUser->szAddress2))
            {
                $szStreetAddress .= " ".$kUser->szAddress2 ;
            }
            if(!empty($kUser->szAddress3))
            {
                $szStreetAddress .= " ".$kUser->szAddress3 ;
            }
            $szPhoneNumber = "+".$iInternationDialCode." ".$kUser->szPhoneNumber;
            
            $szPersonIdStr="";
            if(!empty($kUser->szCapsulePrtyID))
            {
                $szPersonIdStr = '<id>'.$kUser->szCapsulePrtyID.'</id>';
            }
            
            $szAddressIdStr="";
            if(!empty($idCrmAddress))
            {
                $szAddressIdStr = '<id>'.$idCrmAddress.'</id>';
            }
            
            $szEmailIdStr="";
            if(!empty($idCrmEmail))
            {
                $szEmailIdStr = '<id>'.$idCrmEmail.'</id>';
            }
            
            $szPhoneIdStr="";
            if(!empty($idCrmPhone))
            {
                $szPhoneIdStr = '<id>'.$idCrmPhone.'</id>';
            } 
            
            $szPhoneNumber = trim($szPhoneNumber);
            $szStreetAddress = trim($szStreetAddress); 
            $szCountryName = trim($szCountryName);
            
            if(empty($kUser->szCompanyName))
            {
                $kUser->szCompanyName = 'Private';
            } 
            $szDefaultString = '""';
            if(empty($szStreetAddress))
            {
                $szStreetAddress = $szDefaultString;
            }
            if(empty($kUser->szCity))
            {
                $kUser->szCity = $szDefaultString;
            }
            if(empty($kUser->szState))
            {
                $kUser->szState = $szDefaultString;
            }
            if(empty($kUser->szPostCode))
            {
                $kUser->szPostCode = $szDefaultString;
            }
            if(empty($szCountryName))
            {
                $szCountryName = $szDefaultString;
            } 
            if(empty($szPhoneNumber))
            {
                $szPhoneNumber = $szDefaultString;
            }
            if(empty($kUser->szFirstName))
            {
                $kUser->szFirstName = $szDefaultString;
            }
            if(empty($kUser->szLastName))
            {
                $kUser->szLastName = $szDefaultString;
            }
            
            $crmXmlString = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <person>
                  '.$szPersonIdStr.' 
                    <firstName>'.$kUser->szFirstName.'</firstName>
                    <lastName>'.$kUser->szLastName.'</lastName> 
                    <organisationName>'.$kUser->szCompanyName.'</organisationName>
                    <about>Added User From Transporteca</about>
                    <contacts>
                      <address>
                        '.$szAddressIdStr.' 
                        <type>Office</type>
                        <street>'.$szStreetAddress.'</street>
                        <city>'.$kUser->szCity.'</city>
                        <state>'.$kUser->szState.'</state>
                        <zip>'.$kUser->szPostCode.'</zip>
                        <country>'.$szCountryName.'</country>
                      </address>
                      <email>
                        '.$szEmailIdStr.' 
                        <type>Home</type>
                        <emailAddress>'.$kUser->szEmail.'</emailAddress>
                      </email>
                      <phone>
                        '.$szPhoneIdStr.'
                        <type>Mobile</type>
                        <phoneNumber>'.$szPhoneNumber.'</phoneNumber>
                      </phone> 
                    </contacts>   
                </person>
            '; 
            $crmXmlString = str_replace("&", "&amp;", $crmXmlString);
            return $crmXmlString ;
        }
    }
    
    function createXMLForAddNote($szHistoryNote)
    {
        if(!empty($szHistoryNote))
        {
            $crmXmlString = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <historyItem>
                    <type>Email</type>
                    <subject>This is test Subject</subject> 
                    <note>'.$szHistoryNote.'</note>
                </historyItem>';
            $crmXmlString = str_replace("&", "&amp;", $crmXmlString);
            return $crmXmlString; 
        }
    }
    function createXMLForAddTask($data)
    {
        if(!empty($data))
        {
            $crmXmlString = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                <task>
                  <description>'.$data['szTaskDesc'].'</description>
                  <dueDateTime>'.$data['dtTaskDate'].'</dueDateTime>
                </task>';
            $crmXmlString = str_replace("&", "&amp;", $crmXmlString);
            return $crmXmlString; 
        }
    }
    
    /**
     * This function is used to add customer details to Capsule CRM
     * @access public
     * @param string
     * @return array 
     * @author Ajay
     */ 
    
    public function addCustomerHistoryNotesToCapsule($szCustomerXml,$szCapsulePrtyID,$szUpdateType=false)
    {	 
        if(!empty($szCustomerXml) && !empty($szCapsulePrtyID))
        {
            if($szUpdateType=='TASK')
            {
                $szCapsuleHostUrl =  __CAPSULE_API_HOST_URL__."/api/party/".trim($szCapsulePrtyID)."/task"; 
                //$szCapsuleHostUrl =  __CAPSULE_API_HOST_URL__."/api/task";  
            }
            else
            {
                $szCapsuleHostUrl =  __CAPSULE_API_HOST_URL__."/api/party/".trim($szCapsulePrtyID)."/history"; 
            }
            
            echo "<br> Posting History notes<br><br> URL: ".$szCapsuleHostUrl ;
            
            $kAdmin = new cAdmin();
            $szCapsuleAPIUserName = $kAdmin->getCapsuleCrmAPIKey();
            
            $szCapsuleAPIPassword = __CAPSULE_API_PASSWORD__ ;
  
            $ch = curl_init($szCapsuleHostUrl);
            $options = array(
                CURLOPT_USERPWD => $szCapsuleAPIUserName.':'.$szCapsuleAPIPassword,
                CURLOPT_HTTPHEADER => array('Content-Type: application/xml'), 
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HEADER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => trim($szCustomerXml)
            );
            
            curl_setopt_array($ch, $options);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 

            $cUrlResponseAry = array();
            $cUrlResponseXml = curl_exec($ch); 
              
            $szHttpResponseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE) ; 
            
//            if(trim($szHttpResponseCode)=='400')
//            { 
//                $curlResponseInfoAry = array();
//                $curlResponseInfoAry = curl_getinfo($ch);
//                echo "XML:  ".$szCustomerXml ;
//                print_R($curlResponseInfoAry);
//                curl_close($ch); 
//                die;
//            }
            /* 
             * Status Code: 201 means user created successfully
             */
            if(trim($szHttpResponseCode)=='201')
            {   
                echo "<br>Status: SUCCESS <br>";
                $szErrorString = "\n\n SUCCESS:: ".$szUpdateType." Function::addCustomerHistoryNotesToCapsule Timestamp: ".date('Y-m-d H:i:s')."\n\n"; 
                $szErrorString .= " \n\n Request XML: \n\n ".$szCustomerXml."\n\n" ; 
                $szErrorString .= " Response XML: \n\n ".$cUrlResponseXml."\n\n" ; 

                $filename = __APP_PATH__."/logs/capsule_success.log";

                echo $szErrorString;
                $strdata=array();
                $strdata[0]= $szErrorString;
                file_log(true, $strdata, $filename);
                curl_close($ch); 
                return true ;
            }
            else
            {
                echo "<br>Status: Error <br>";
                echo " Request XML: \n\n ".$szCustomerXml."\n\n" ;
                
                $szErrorString = "\n\n Function::addCustomerDetailsToCapsule Timestamp: ".date('Y-m-d H:i:s')."\n\n";
                $szErrorString .= " Request XML: \n\n ".$szCustomerXml."\n\n" ;

                $curlResponseInfoAry = array();
                $curlResponseInfoAry = curl_getinfo($ch);
                $szErrorString .= "Curl Response: \n\n ".curl_error($ch)."\n\n Curl Info: ".print_R($curlResponseInfoAry,true)."\n\n ";
  
                $filename = __APP_PATH__."/logs/capsule_error.log";
					
                $strdata=array();
                $strdata[0]= $szErrorString;
                file_log(true, $strdata, $filename);
                
                curl_close($ch);
                return false;
            }  
        }
    }
    
    function addCapsuleCrmDataFroCronjob($data)
    {
        return false;
        /*
        if(!empty($data))
        {
            if($data['id']>0)
            {
                 $query = " 
                    UPDATE  
                      ".__DBC_SCHEMATA_CAPSULE_CRM_DATA__." 
                    SET
                        szFirstName = '".mysql_escape_custom(trim($data['szFirstName']))."',
                        szLastName = '".mysql_escape_custom(trim($data['szLastName']))."',
                        szCompanyName = '".mysql_escape_custom(trim($data['szCompanyName']))."',
                        szAddress = '".mysql_escape_custom(trim($data['szAddress']))."',
                        szCity = '".mysql_escape_custom(trim($data['szCity']))."',
                        szState = '".mysql_escape_custom(trim($data['szState']))."',
                        szCountryName = '".mysql_escape_custom(trim($data['szCountryName']))."',
                        szPostCode = '".mysql_escape_custom(trim($data['szPostCode']))."', 
                        szPhone = '".mysql_escape_custom(trim($data['szPhone']))."',
                        szEmail = '".mysql_escape_custom(trim($data['szEmail']))."',
                        szNotes = '".mysql_escape_custom(trim($data['szNotes']))."',
                        iDataType = '".mysql_escape_custom(trim($data['iDataType']))."',
                        iCreateTask = '".mysql_escape_custom(trim($data['iCreateTask']))."',
                        dtUpdatedON = now()
                    WHERE 
                        id = '".$data['id']."' 
                " ; 
            }
            else
            {
                $query = " 
                    INSERT INTO
                      ".__DBC_SCHEMATA_CAPSULE_CRM_DATA__." 
                    (
                        idUser,
                        szFirstName,
                        szLastName,
                        szCompanyName,
                        szAddress,
                        szPostCode,
                        szCity,
                        szState,
                        szCountryName,
                        szPhone,
                        szEmail,
                        szNotes,
                        iDataType,
                        iCreateTask,
                        dtCreatedOn,
                        dtDateTime
                    )
                    VALUES
                    (
                        '".mysql_escape_custom(trim($data['idUser']))."',
                        '".mysql_escape_custom(trim($data['szFirstName']))."',
                        '".mysql_escape_custom(trim($data['szLastName']))."',
                        '".mysql_escape_custom(trim($data['szCompanyName']))."',
                        '".mysql_escape_custom(trim($data['szAddress']))."', 
                        '".mysql_escape_custom(trim($data['szPostCode']))."',
                        '".mysql_escape_custom(trim($data['szCity']))."',
                        '".mysql_escape_custom(trim($data['szState']))."',
                        '".mysql_escape_custom(trim($data['szCountryName']))."', 
                        '".mysql_escape_custom(trim($data['szPhone']))."',
                        '".mysql_escape_custom(trim($data['szEmail']))."',
                        '".mysql_escape_custom(trim($data['szNotes']))."',
                        '".mysql_escape_custom(trim($data['iDataType']))."',  
                        '".mysql_escape_custom(trim($data['iCreateTask']))."',   
                        now(),
                        now()
                    )
                " ; 
            } 
            //echo $query ;
            //die;
            if($result=$this->exeSQL($query))
            {
                return true;
            }
            else
            {
                //echo " not updated ";
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        } 
         * 
         */
    }
    
    
    function addCapsuleCrmIdLogsFroCronjob($data)
    {
        if(!empty($data))
        { 
            $query = " 
                INSERT INTO
                  ".__DBC_SCHEMATA_CAPSULE_CRM_ID_LOGS__." 
                (
                    idUser,
                    szCapsulePrimaryID,
                    szCapsuleAddressID,
                    szCapsuleEmailID,
                    szCapsulePhoneID, 
                    dtCreatedOn,
                    iActive
                )
                VALUES
                (
                    '".mysql_escape_custom(trim($data['idUser']))."',
                    '".mysql_escape_custom(trim($data['szCapsulePrimaryID']))."',
                    '".mysql_escape_custom(trim($data['szCapsuleAddressID']))."',
                    '".mysql_escape_custom(trim($data['szCapsuleEmailID']))."',
                    '".mysql_escape_custom(trim($data['szCapsulePhoneID']))."',  
                    now(),
                    '1'
                )
            " ;  
           // echo $query ;
           // return false;
            if($result=$this->exeSQL($query))
            {
                return true;
            }
            else
            {
                //echo " not updated ";
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        } 
    }
    
    function getAllUnPostedDataForCronjob($data=array(),$bMautic=false)
    {
        /*
        if(!empty($data))
        {
            $idUser = $data['idUser'];
            $dtDateTime = $data['dtDateTime'];
            $iDataType = $data['iDataType'] ;
            
            if(!empty($dtDateTime))
            {
                $query_and = " AND DATE(dtDateTime) = '".mysql_escape_custom(trim($dtDateTime))."' ";
            }
            
            if($idUser>0)
            {
                $query_and .= " AND idUser = '".mysql_escape_custom(trim($idUser))."' ";
            }
            
            if($iDataType>0)
            {
                $query_and .= " AND iDataType = '".mysql_escape_custom(trim($iDataType))."' ";
            } 
        } 
        
        if($bMautic)
        {
            $query_where = " iPostedToMautic= '0' AND iDataType = '1' AND iNumAttempt<10 ";
        }
        else
        {
            $query_where = " iPosted = '0'";
        }
        
        $query = " 
            SELECT 
                id,
                idUser,
                szFirstName,
                szLastName,
                szCompanyName,
                szAddress,
                szPostCode,
                szCity,
                szState,
                szCountryName,
                szPhone,
                szEmail,
                szNotes,
                iDataType,
                dtCreatedOn,
                iCreateTask
            FROM
                ".__DBC_SCHEMATA_CAPSULE_CRM_DATA__."
            WHERE 
                $query_where
                $query_and 
        ";
        //echo $query ;
        if($result=$this->exeSQL($query))
        {
            $retAry=array(); 
            while($row=$this->getAssoc($result))
            { 
                $retAry[]=$row; 
            }
            return $retAry ;
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }	
         * 
         */
    } 
    
    function updatedPostedFlag($idCapsuleAry,$bMauticApi=false,$bEmptyResponse=false)
    {
        /*
        if(!empty($idCapsuleAry))
        {
            $idCapsuleAry_str = implode(",",$idCapsuleAry);            
            if($bMauticApi)
            { 
                if($bEmptyResponse)
                {
                    $query_update = " iPostedToMautic = '0', iNumAttempt = (iNumAttempt + 1), ";
                } 
                else
                {
                    $query_update = " iPostedToMautic = '1', ";
                }
            }
            else
            {
                $query_update = " iPosted = '1', ";
            } 
            $query = "  
                UPDATE 
                  ".__DBC_SCHEMATA_CAPSULE_CRM_DATA__."   
                SET
                  $query_update
                  dtPostedDate = now()
                WHERE 
                  id IN (".$idCapsuleAry_str.")
            ";
            //echo $query ;
            if($result=$this->exeSQL($query))
            {
                return true;
            }
            else
            {
                //echo " not updated ";
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
         * 
         */
    }
    
    function deleteOldData()
    {
        /*
        $dtDateTime = date("Y-m-d H:i:s",  strtotime("-1 WEEK")); 
        $query = "  
            DELETE FROM 
              ".__DBC_SCHEMATA_CAPSULE_CRM_DATA__."    
            WHERE 
              iPosted = '1'
            AND
                iPostedToMautic = '1'
            AND
               dtDateTime <= '".  mysql_escape_custom($dtDateTime)."'
        ";
        //echo $query ;
        if($result=$this->exeSQL($query))
        {
            return true;
        }
        else
        {
            //echo " not updated ";
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
         * 
         */
    }
    
    function displayUserDetailsFromCrm($szCapsulePrtyID=false,$getAllUsers=false,$get_custom_fields=false)
    {
        if(!empty($szCapsulePrtyID) || $getAllUsers || $get_custom_fields)
        {  
            if($getAllUsers)
            {
                $szCapsuleHostUrl =  __CAPSULE_API_HOST_URL__."/api/party";  
            }
            else if($get_custom_fields==2)
            {
                 $szCapsuleHostUrl =  __CAPSULE_API_HOST_URL__."/api/party/".$szCapsulePrtyID."/customfield";
            }
            else if($get_custom_fields)
            {
                 $szCapsuleHostUrl =  __CAPSULE_API_HOST_URL__."/api/party/customfield/definitions";
            }
            else
            {
                $szCapsuleHostUrl =  __CAPSULE_API_HOST_URL__."/api/party/".trim($szCapsulePrtyID);  
            }  
            //echo "<br><br> HOST: ".$szCapsuleHostUrl; 
            
            $kAdmin = new cAdmin();
            $szCapsuleAPIUserName = $kAdmin->getCapsuleCrmAPIKey();
            
            $szCapsuleAPIPassword = __CAPSULE_API_PASSWORD__ ;
  
            $ch = curl_init($szCapsuleHostUrl);
            $options = array(
                CURLOPT_USERPWD => $szCapsuleAPIUserName.':'.$szCapsuleAPIPassword,
                CURLOPT_HTTPHEADER => array('Content-Type: application/xml'), 
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HEADER => true 
            );
            
            curl_setopt_array($ch, $options);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 

            $cUrlResponseAry = array();
            $cUrlResponseXml = curl_exec($ch);
            //print_R($cUrlResponseXml); 
            
            $szHttpResponseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE) ; 
            
            /* 
             * Status Code: 201 means user created successfully
             */
            if(trim($szHttpResponseCode)=='200')
            {   
                $szErrorString = "\n\n SUCCESS:: ".$szUpdateType." Function::addCustomerHistoryNotesToCapsule Timestamp: ".date('Y-m-d H:i:s')."\n\n";
                $szErrorString .= " Response XML: \n\n ".$cUrlResponseXml."\n\n" ; 

                $filename = __APP_PATH__."/logs/capsule_success.log"; 
                parse_str($cUrlResponseXml, $returned_items);
		
		$ResponseXML = substr($cUrlResponseXml, strpos($cUrlResponseXml, "<" . "?xml"));
                
                $responseAry = array();
                $responseAry = cXmlParser::createArray($ResponseXML);
                
                $strdata=array();
                $strdata[0]= $ResponseXML;
                file_log(true, $strdata, $filename);
                curl_close($ch); 
                return $responseAry ;
            }
            else
            {
                $szErrorString = "\n\n Function::addCustomerDetailsToCapsule Timestamp: ".date('Y-m-d H:i:s')."\n\n";
                $szErrorString .= " Request XML: \n\n ".$szCustomerXml."\n\n" ;

                //echo $szErrorString;
                
                $curlResponseInfoAry = array();
                $curlResponseInfoAry = curl_getinfo($ch);
                $szErrorString .= "Curl Response: \n\n ".curl_error($ch)."\n\n Curl Info: ".print_R($curlResponseInfoAry,true)."\n\n ";
 
                $filename = __APP_PATH__."/logs/capsule_error.log";
					
                $strdata=array();
                $strdata[0]= $szErrorString;
                file_log(true, $strdata, $filename);
                
                curl_close($ch);
                return false;
            }  
        }
    }
    
     /**
     * This function is used to add customer details to Capsule CRM
     * @access public
     * @param string
     * @return array 
     * @author Ajay
     */ 
    
    public function addCustomFieldsDetailsToCapsule($szCustomerXml,$szCapsulePrtyID)
    {	 
        if(!empty($szCustomerXml) && !empty($szCapsulePrtyID))
        { 
            $szCapsuleHostUrl =  __CAPSULE_API_HOST_URL__."/api/party/".trim($szCapsulePrtyID)."/customfields";   
            //echo "<br> Posting History notes<br><br> URL: ".$szCapsuleHostUrl ;
            
            $kAdmin = new cAdmin();
            $szCapsuleAPIUserName = $kAdmin->getCapsuleCrmAPIKey();
            
            $szCapsuleAPIPassword = __CAPSULE_API_PASSWORD__ ;
  
            $ch = curl_init($szCapsuleHostUrl);
            $options = array(
                CURLOPT_USERPWD => $szCapsuleAPIUserName.':'.$szCapsuleAPIPassword,
                CURLOPT_HTTPHEADER => array('Content-Type: application/xml'), 
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HEADER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => trim($szCustomerXml)
            );
            
            curl_setopt_array($ch, $options);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 

            $cUrlResponseAry = array();
            $cUrlResponseXml = curl_exec($ch); 
               
            $szHttpResponseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE) ;  
            /* 
             * Status Code: 201 means user created successfully
             */
            if(trim($szHttpResponseCode)=='201')
            {   
                echo "<br>Status:".$szHttpResponseCode." SUCCESS <br>";
                $szErrorString = "\n\n SUCCESS:: ".$szUpdateType." Function::addCustomFieldsDetailsToCapsule Timestamp: ".date('Y-m-d H:i:s')."\n\n"; 
                $szErrorString .= " \n\n Request XML: \n\n ".$szCustomerXml."\n\n" ; 
                $szErrorString .= " Response XML: \n\n ".$cUrlResponseXml."\n\n" ; 

                $filename = __APP_PATH__."/logs/capsule_success.log";

                //echo $szErrorString;
                $strdata=array();
                $strdata[0]= $szErrorString;
                file_log(true, $strdata, $filename);
                curl_close($ch); 
                return true ;
            }
            else
            { 
                echo "<br>Status:".$szHttpResponseCode." Error <br>";
                //echo " Request XML: \n\n ".$szCustomerXml."\n\n" ;
                
                $szErrorString = "\n\n Function::addCustomerDetailsToCapsule Timestamp: ".date('Y-m-d H:i:s')."\n\n";
                $szErrorString .= " Request XML: \n\n ".$szCustomerXml."\n\n" ;

                $curlResponseInfoAry = array();
                $curlResponseInfoAry = curl_getinfo($ch);
                $szErrorString .= "Curl Response: \n\n ".curl_error($ch)."\n\n Curl Info: ".print_R($curlResponseInfoAry,true)."\n\n ";
  
                $filename = __APP_PATH__."/logs/capsule_error.log";
					
                $strdata=array();
                $strdata[0]= $szErrorString;
                file_log(true, $strdata, $filename);
                
                curl_close($ch);
                return false;
            }  
        }
    }
    
    function insert_crm_temp_data($data)
    {
        if(!empty($data))
        {
            $kUser = new cUser();
            $idUser = $kUser->getAllUsersCapsuleCrmID($data['szCapsuleCrmID']);
            if($idUser>0)
            {
                $idUser = $idUser;
            }
            else
            {
                $idUser = 0; 
            }
            $query = " 
                INSERT INTO
                  tbltempcapsuledata
                (
                    idUser,
                    szCapsuleCrmID,
                    szFirstName,
                    szLastName,
                    szEmail,
                    szPhone
                )
                VALUES
                (
                    '".mysql_escape_custom(trim($idUser))."',
                    '".mysql_escape_custom(trim($data['szCapsuleCrmID']))."',
                    '".mysql_escape_custom(trim($data['szFirstName']))."',
                    '".mysql_escape_custom(trim($data['szLastName']))."',
                    '".mysql_escape_custom(trim($data['szEmail']))."',
                    '".mysql_escape_custom(trim($data['szPhone']))."'
                )
            " ;  
            echo "<br><br>".$query ;
            //die;
            if($result=$this->exeSQL($query))
            {
                return true;
            }
            else
            {
                //echo " not updated ";
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    } 
    
    function getAllTempCrmCustomers($not_mapped_users=false)
    {  
        if($not_mapped_users)
        {
            $query_and = " WHERE idUser=0 ";
        }
        
        $query = " 
            SELECT 
                idUser,
                szCapsuleCrmID,
                szFirstName,
                szLastName,
                szEmail,
                szPhone,
                szNewLetterValue
            FROM
                tbltempcapsuledata  
                $query_and
        ";
        echo $query ;
        if($result=$this->exeSQL($query))
        {
            $retAry=array(); 
            $ctr=0;
            while($row=$this->getAssoc($result))
            { 
                $retAry[$ctr]=$row; 
                $ctr++;
            }
            return $retAry ;
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }	
    } 
    function updateNewletterFields($data,$szCapsuleCrmID)
    {
        if(!empty($szCapsuleCrmID))
        { 
            $szNewLetter = $data['szNewLetter'];
            $szLanguage = $data['szLanguage'];
            $szCustomerOwner = $data['szCustomerOwner'];
            $szCurrency = $data['szCurrency'];
             
            $query = "  
                UPDATE 
                  tbltempcapsuledata  
                SET
                  szNewLetterValue = '".mysql_escape_custom($szNewLetter)."',
                  szLanguage = '".mysql_escape_custom($szLanguage)."',
                  szCustomerOwner = '".mysql_escape_custom($szCustomerOwner)."',
                  szCurrency = '".mysql_escape_custom($szCurrency)."
                WHERE 
                  szCapsuleCrmID = '".  mysql_escape_custom($szCapsuleCrmID)."'
            ";
            echo "<br><br> ".$query ;
            if($result=$this->exeSQL($query))
            {
                return true;
            }
            else
            { 
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
}