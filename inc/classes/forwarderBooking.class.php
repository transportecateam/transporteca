<?php
/**
 * This file is the container for partner api related functionality. 
 *
 * api.class.php
 *
 * @copyright Copyright (C) 2015 Transporteca
 * @author Ajay
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" ); 

class cForwarderBooking extends cDatabase
{    
    var $t_base_booking = 'Booking/MyBooking/';
    function __construct()
    {
        parent::__construct();
        return true;
    }
    
    function search_forwarder_bookings($data,$order_by='',$order='')
    {
        if(!empty($data))
        {
            $idForwarder =$_SESSION['forwarder_id'];
            $kBooking = new cBooking();
            $query_and = '';
            if(!empty($data['iBookingYear']))
            {
                $query_and .= " AND YEAR(b.dtBookingConfirmed) = '".(int)$data['iBookingYear']."'" ;
            }
            if(!empty($data['iBookingMonth']))
            {	
                $month=sanitize_all_html_input($data['iBookingMonth']);
                $query_and .= " AND MONTH(b.dtBookingConfirmed) = '".(int)$data['iBookingMonth']."'" ;
            } 
            if(!empty($data['dtFromDate']))
            {
                $from_date = $kBooking->convert_date($data['dtFromDate']);
                $query_and .= " AND DATE(b.dtBookingConfirmed) >= '".mysql_escape_custom(trim($from_date))."'" ;
            }
            if(!empty($data['dtToDate']))
            {
                $to_date = $kBooking->convert_date($data['dtToDate']);
                $query_and .= " AND DATE(b.dtBookingConfirmed) <= '".mysql_escape_custom(trim($to_date))."'" ;
            }

            if($month)
            {
                if(isset($from_date))
                {
                    $arr=explode('/',$from_date);	
                    if($arr[1]!=$month)	
                    {	
                        $this->addError( 'fromDate' , t($this->t_base_booking.'Error/select_date_of_similar_Month') );
                        return false;
                    }			
                }
                if(isset($to_date)) 
                {
                    $arrs=explode('/',$to_date);	
                    if($arrs[1]!=$month)	
                    {	
                        $this->addError( 'toDate' , t($this->t_base_booking.'Error/select_date_of_similar_Month') );
                        return false;
                    }			
                }
            }
            if(isset($from_date) && isset($to_date) )
            {
                if(strtotime($from_date) > strtotime($to_date))
                {
                    $this->addError( 'fromTo' , t($this->t_base_booking.'Error/from_grt_than_to_date') );	
                    return false;
                }
            }
            if(!empty($data['szBookingRef']))
            {
                $query_and .= " AND b.szBookingRef LIKE '%".mysql_escape_custom(trim($data['szBookingRef']))."%'" ;
            }
            if(!empty($data['szInvoice']))
            {
                $query_and .= " AND b.szInvoice LIKE '%".mysql_escape_custom(trim($data['szInvoice']))."%'" ;
            }
            if(!empty($data['szForwarderReference']))
            {
                $query_and .= " AND b.szForwarderReference LIKE '%".mysql_escape_custom(trim($data['szForwarderReference']))."%'" ;
            }
            //$query_and='';
            if(!empty($data['id']))
            {
                $query_and .= " AND b.id = '".(int)$data['id']."'" ;
            }
            $orderBY='';
            if($order_by!='')
            {
                $orderBY .="
                        ORDER BY
                                ".$order_by." ".$order."
                ";	
            }
            else
            {
                $orderBY .="
                        ORDER BY
                                b.dtBookingConfirmed DESC,
                                b.szBookingRef DESC
                ";
            }

            $query = "
                SELECT
                    b.id,
                    b.dtBookingConfirmed,
                    b.szBookingRef,
                    b.dtCutOff,
                    b.dtAvailable,
                    b.idServiceType,
                    b.szWarehouseFromCity,
                    b.szWarehouseToCity,
                    b.fCargoVolume,
                    b.fCargoWeight,
                    b.szForwarderReference,
                    b.idForwarder,
                    b.idBookingStatus, 
                    b.idWarehouseFromCountry,
                    b.idWarehouseToCountry,
                    b.szCustomerCompanyName,
                    b.iBookingLanguage,
                    b.iQuotesStatus,
                    b.idOriginCountry,
                    b.idDestinationCountry,
                    b.szTransportMode,
                    b.idTransportMode,
                    b.iCourierBooking,
                    b.dtLabelDownloaded,
                    b.iBookingType,
                    b.dtShipmentDate,
                    b.dtTimingDate,
                    b.szLastName,
                    b.szFirstName,
                    b.iHandlingFeeApplicable,
                    b.fTotalForwarderManualFee,
                    (SELECT sp1.szShipperCity FROM ".__DBC_SCHEMATA_SHIPPER_CONSIGNEE__. " sp1 WHERE sp1.idBooking = b.id ) as szShipperCity,
                    (SELECT sp2.szConsigneeCity FROM ".__DBC_SCHEMATA_SHIPPER_CONSIGNEE__. " sp2 WHERE sp2.idBooking = b.id ) as szConsigneeCity,
                    (SELECT count(id) as iNumBooking FROM ".__DBC_SCHEMATA_BOOKING_ACKNOWLEDMENT__. " ba WHERE ba.idBooking = b.id ) as iBookingAcknowledge
                FROM
                    ".__DBC_SCHEMATA_BOOKING__." b 
                WHERE
                    b.idForwarder = '".(int)$idForwarder."'	
                AND
                    b.iTransferConfirmed = '0'
                AND
                  b.idBookingStatus IN ('3','4')	
                  ".$query_and."
                  ".$orderBY."	
            ";
            
            //echo "<br>".$query."<br>";
            if($result=$this->exeSQL($query))
            {
                $bookingAry = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    $bookingAry[$ctr]=$row; 
                    $ctr++;
                }  
                return $bookingAry;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    
    function getForwarderNotificationForLabel($idForwarder,$bBlueIcon=false,$flag=__LABEL_NOT_SENT__)
    {
        if($flag==__LABEL_SENT__)
        {
            $query_and = " AND b.idBookingStatus IN ('3','4') AND  (b.idServiceProvider>0 OR isManualCourierBooking=1)  AND fl.iLabelStatus=".__LABEL_SEND_FLAG__."";

            if($bBlueIcon)
            {
                $query_and .= " AND DATE(b.dtLabelSent) > '".date('Y-m-d',strtotime('-1 DAYS'))."' AND b.dtSnooze < '".date('Y-m-d')."' ";
            }
            else
            {
                $query_and .= " AND DATE(b.dtLabelSent) < '".date('Y-m-d',strtotime('-1 DAYS'))."' ";
            }
        }
        else if($flag==__LABEL_DOWNLOAD__)
        {
            $query_and = " AND b.idBookingStatus IN ('3','4') AND
                (b.idServiceProvider>0 OR isManualCourierBooking=1) AND fl.iLabelStatus=".__LABEL_DOWNLOAD_FLAG__.""; 
        }
        else
        {
            $query_and = " AND b.idBookingStatus IN ('3','4') AND
                (b.idServiceProvider>0 OR isManualCourierBooking=1)";

            if($bBlueIcon)
            {
                $query_and .= " AND b.dtBookingConfirmed > '".date('Y-m-d H:i:s',strtotime('-24 HOURS'))."' ";
            }
            else
            {
                $query_and .= " AND b.dtBookingConfirmed < '".date('Y-m-d H:i:s',strtotime('-24 HOURS'))."' ";
            } 
            $query_and .= " AND (fl.iLabelStatus=".__LABEL_NOT_SEND_FLAG__." || (fl.szForwarderBookingTask = 'T140' && fl.iLabelStatus!=".__LABEL_SEND_FLAG__." && fl.iLabelStatus!=".__LABEL_DOWNLOAD_FLAG__."))";
        } 
        
        $query = "
            SELECT
                count(b.id) iNumBookings
            FROM
                ".__DBC_SCHEMATA_BOOKING__." as b
            INNER JOIN
                ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__." cb
            ON
                cb.idBooking = b.id
            LEFT JOIN
                ".__DBC_SCHEMATA_FILE_LOGS__." AS fl
            ON        
                fl.id=b.idFile
                ".$inner_query."		
            WHERE
                idForwarder = '".(int)$idForwarder."'
            AND
                cb.iCourierAgreementIncluded = '1'
                $query_and
                $queryWhere 
            ORDER BY
                b.dtBookingConfirmed DESC	 
        ";
        //echo "<br>".$query;
        if($result=$this->exeSQL($query))
        { 
            $row = $this->getAssoc($result);
            return $row['iNumBookings']; 
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
}

?>