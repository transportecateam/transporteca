<?php
/**
 * This file is the container for all admin related functionality.
 * All functionality related to admin details should be contained in this class.
 *
 * admin.class.php
 *
 * @copyright Copyright (C) 2012 Transporteca, LLC
 * @author Anil
 * @package Transporteca Development
 */
if(!isset($_SESSION))
{
	session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_INC__ . "/functions.php" );
require_once( __APP_PATH_CLASSES__ . "/database.class.php");

class cMetics extends cDatabase
{
    /**
     * These are the variables for each admin.
     */
	var $id;
	var $szFirstName;
	var $szLastName;
	var $szEmail;
	var $szPassword;
	var $iActive;
	var $dtCreatedOn;
	var $dtUpdatedOn;
	var $szOldEmail;
	var $szOldPassword;
	var $currencyId;
	var $szCurrency;
	var $iPricing;
	var $iBooking;
	var $iSettling;
	var $szFeed;
	var $idCountries;
	var $szCountryName;
	var $szCountryISO;
	var $fStandardTruckRate;
	var $iActiveFromDropdown;
	var $iActiveToDropdown;
	var $szHeading;
	var $szDescription;
	var $iId;
	var $iValue;
	var $szNewFirstName;
	var $szNewLastName;
	var $szNewEmail;
	var $szNewPassword;
	var $dtCreateOn;
	var $szComment;
	var $version;
	var $szTemplateSubject;
	var $iAllAccess;
	var $t_base="management/Error/";
	var $szHeadingArr = array();
	var $iValueArr = array();
	var $iIdArr = array();
	var $iAuto;
	var $iPasswordUpdated;
	var $szUrl;
	var $searchField;
	var $idOriginCountry;
	var $idDestinationCountry;
	var $idForwarder;
	var $szLatitude;
	var $szLongitude;
	
	function __construct()
	{
            parent::__construct();
            return true;
	}
	
	function sanitizeData($value)
	{
            $checked=sanitize_all_html_input(trim($value));
            return $checked;
	}
	
	function metricsLogin($data)
	{	
            $this->set_szEmail($this->sanitizeData($data['szEmail']));
            $this->set_szPassword($this->sanitizeData($data['szPassword']));

            if ($this->error === true)
            {
                return false;
            }
		
            $query="
                SELECT 
                    id,
                    szEmail,
                    szPassword
                FROM 
                    ".__DBC_SCHEMATA_INVESTORS__."
                WHERE 
                    szEmail='".mysql_escape_custom($this->szEmail)."' 
                AND
                    szPassword = '".mysql_escape_custom(trim(md5($this->szPassword)))."'
                AND
                    iActive= '1'	
            ";
            //echo "Query: ".$query ;
            if(($result = $this->exeSQL($query)))
            {
                if($this->getRowCnt()>0)
                {
                    $row=$this->getAssoc($result);
                    $this->id = $row['id']; 
                    $this->szEmail = $row['szEmail'];  
                    $_SESSION['metrics_user_id']=$this->id;  
                    $_SESSION['logged_in_as'] = 1; //Investor
                    
                    if(__ENVIRONMENT__ == "LIVE")
                    {
                        setcookie("__METRICS_EMAIL_COOKIE__", $this->szEmail, time()+(3600*24*90),'/',__BASE_URL_SECURE__,true);
                    }
                    else
                    {
                        setcookie("__METRICS_EMAIL_COOKIE__", $this->szEmail, time()+(3600*24*90),'/');
                    }
                    
                    $query="
                        UPDATE
                            ".__DBC_SCHEMATA_INVESTORS__."
                        SET
                            dtLastLogin=now()
                        WHERE
                            id = '".(int)$row['id']."'		
                    ";
                    $resul=$this->exeSQL($query);
                    return true;
                }
                else
                { 
                    $query="
                        SELECT
                            id
                        FROM
                            ".__DBC_SCHEMATA_MANAGEMENT__."
                        WHERE
                            szEmail='".mysql_escape_custom($this->szEmail)."'
                        AND
                            szPassword = '".mysql_escape_custom(trim(md5($this->szPassword)))."'
                        AND
                        	iActive='1'    	
                    ";
                    //echo $query."<br />" ;
                    //die;
                    if( ( $result = $this->exeSQL( $query ) ) )
                    {
                        if($this->getRowCnt()>0)
                        { 
                            $row=$this->getAssoc($result);
                            $_SESSION['metrics_user_id'] = $row['id'];  
                            $_SESSION['logged_in_as'] = 2; //Management
                            return true ;  
                        }
                        else
                        {
                            $this->addError( "szEmail" , t($this->t_base.'messages/wrong_username_password') );
                            return false;
                        }
                    }
                    else
                    {
                        $this->addError( "szEmail" , t($this->t_base.'messages/wrong_username_password') );
                        return false;
                    }
                }  
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	} 
        
	function getInvestorsDetails($id)
	{
            if((int)$id>0)
            {
                $query="
                    SELECT
                        id,
                        szEmail,
                        szFirstName,
                        szLastName,
                        iActive, 
                        dtCreatedOn,
                        dtUpdatedOn,
                        iPasswordUpdated 
                    FROM
                        ".__DBC_SCHEMATA_INVESTORS__."
                    WHERE
                        id='".(int)$id."'		
                ";
                //echo "<br /> ".$query."<br />";

                if( ( $result = $this->exeSQL( $query ) ) )
                {
                    if( $this->getRowCnt() > 0 )
                    {
                        $row=$this->getAssoc($result);
                        $this->szEmail=$row['szEmail'];
                        $this->id=$row['id'];
                        $this->szFirstName=ucwords(strtolower($row['szFirstName']));
                        $this->szLastName=ucwords(strtolower($row['szLastName']));
                        $this->iActive=$row['iActive'];
                        $this->dtCreateOn=$row['dtCreateOn'];
                        $this->dtUpdatedOn = $row['dtUpdatedOn'];
                        $this->iPasswordUpdated = $row['iPasswordUpdated'];
                        //$this->szCapsule = $row['szCapsule']; 
                        $this->szTitle = $row['szTitle'];  
                        $this->idInternationalDialCode = $row['idInternationalDialCode']; 
                        $this->szPhone = $row['szPhone'];  
                        return $row;
                    }
                    else
                    {
                        return array();
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
            else
            {
                return false;
            }
	}
	 
	function changePassword($data,$id)
	{
            if(is_array($data) && (int)$id>0)
            {
                $this->set_szOldPassword(trim(sanitize_all_html_input($data['szOldPassword'])));
                $this->set_szPassword(trim(sanitize_all_html_input($data['szNewPassword'])));
                $this->set_szConNewPassword(trim(sanitize_all_html_input($data['szConPassword'])));
                $this->set_id(trim(sanitize_all_html_input($id)));

                if ($this->error === true)
                {
                    return false;
                }

                $query="
                    SELECT
                        szPassword
                    FROM
                        ".__DBC_SCHEMATA_INVESTORS__."
                    WHERE
                        id='".(int)$this->id."'
                ";
                if( ( $result = $this->exeSQL( $query ) ) )
                {
	            if( $this->getRowCnt() > 0 )
	            {
	            	$row=$this->getAssoc($result);
	            	$password=$row['szPassword'];
	            }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
			
                if($password!=md5($this->szOldPassword))
                {
                    $this->addError( "szOldPassword" , t($this->t_base.'messages/wrong_old_password') );
                    return false;
                }

                if(!empty($this->szPassword) && $this->szPassword!=$this->szConNewPassword)
                {
                    $this->addError( "szConfirmPassword" , t($this->t_base.'messages/match_password') );
                    return false;
                }

                $query="
                    UPDATE
            		".__DBC_SCHEMATA_INVESTORS__."
                    SET
            		szPassword='".mysql_escape_custom(md5($this->szPassword))."'
                    WHERE
                        id='".(int)$this->id."'
                "; 
                if( $result = $this->exeSQL( $query ) )
                {
                    return true;	
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
            else
            {
                return false;
            }
	}
	 			 
        function isInvestorsEmailExist( $email,$id=0 )
	{
            $query="
                SELECT
                    id
                FROM
                    ".__DBC_SCHEMATA_INVESTORS__."
                WHERE
                    szEmail = '".mysql_escape_custom($email)."'	
            ";
            if($id>0)
            {
                $query .="
                    AND
                        id<>'".(int)$id."'		
                ";
            }
            //echo $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
        	// Check if the user exists
                if( $this->getRowCnt() > 0 )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	} 
        
	function forgotPassword($szEmail)
	{
            $this->set_szEmail(sanitize_all_html_input(strtolower($szEmail)));
            if ($this->error === true)
            {
                return false;
            }
            
            $query="
                SELECT
                    id,
                    szFirstName,
                    szLastName
                FROM
                    ".__DBC_SCHEMATA_INVESTORS__."
                WHERE
                    szEmail = '".mysql_escape_custom($this->szEmail)."'
            "; 
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                if( $this->getRowCnt() > 0 )
                {
                    $row=$this->getAssoc($result);
                    $this->id=$row['id'];
                    $password=create_password();
                    
                    $query="
            		UPDATE
                            ".__DBC_SCHEMATA_INVESTORS__."
            		SET
                            szPassword='".mysql_escape_custom(md5($password))."',
                            iPasswordUpdated='1' 
            		WHERE
                            szEmail = '".mysql_escape_custom($this->szEmail)."'	
                    "; 
                    if($result = $this->exeSQL( $query ))
                    {
                        $replace_ary['szFirstName']=$row['szFirstName'];
                        $replace_ary['szLastName']=$row['szLastName'];
                        $replace_ary['szPassword']=$password;
                        $replace_ary['szEmail']=$this->szEmail;
                       
                        createEmail(__FORGOT_PASSWORD_METRICS__, $replace_ary,$this->szEmail, __FORGOT_PASSWORD_SUBJECT__, __STORE_SUPPORT_EMAIL__,$this->id, __STORE_SUPPORT_EMAIL__,__FLAG_FOR_MANAGEMENT__); 
                        return true;
                    }
	            else
                    {
                        $this->error = true;
                        $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                        $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                        return false;
                    }		
                }
                else
                {
                    $this->addError( "szEmail" , t($this->t_base.'messages/email_not_exists') );
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
	   
        function getMetricsDashboardDataGraph()
        {
            $kDashboardAdmin = new cDashboardAdmin(); 
            $adminDashboardDataAry = array();
            $imgArr = array();
            
            $adminDashboardDataAry = $kDashboardAdmin->listAllDashboardDetails(true); 
            
            $investorDetailsAry = array();
            $investorDetailsAry = $this->getInvestorDashboardDetails();
             
            $uniqueCustomerData=$this->uniqueCustomerDoneBooking(); 
            if(!empty($adminDashboardDataAry))
            {
                $newDashBoardArr = $adminDashboardDataAry['dashboardData'];
                $workingCaps = $adminDashboardDataAry['workingCapital'];
                
                if(!empty($newDashBoardArr))
                {
                    foreach($newDashBoardArr as $key=>$value)
                    { 
                    	$avarageSales=$avarageSalesRevenue[$dtCreatedOn];
                    	$salesSales=$totalSalesRevenue[$dtCreatedOn]; 
                        
                        $dtCreatedOn = '';
                        $dtCreatedOn = strtoupper($key);  
                        
                      
                        $fTurnover[$dtCreatedOn] = (float)($value['fTurnover']+$value['fInsuranceTurnOver'])/1000;
                        $fRevenue[$dtCreatedOn] = (int)($value['fRevenue']+$value['fInsuranceRevenue']+$value['fMarkupRevenue']);
                        $iBookings[$dtCreatedOn] = (int)$value['iBookings'];
                        $iLCLServices[$dtCreatedOn] = (int)$value['iLCLServices'];
                        $iSearches[$dtCreatedOn] = (int)$value['iSearches'];
                        $iCfsCC[$dtCreatedOn] = (int)$value['iCfsCC'];
                        $iCustomerLoggedIn[$dtCreatedOn] = (int)$value['iCustomerLoggedIn'];
                        $iCfsHaulage[$dtCreatedOn] = (int)$value['iCfsHaulage'];  
                        $salesRevenue[$dtCreatedOn] = $value['fSaleRevenue'] ;  
                        $cummulativeBookingAry[$dtCreatedOn] = $investorDetailsAry['cummulativeBookingAry'][$dtCreatedOn] ; 
                        $uniqueCustomerAry[$dtCreatedOn] = $investorDetailsAry['uniqueCustomerAry'][$dtCreatedOn] ; 
                        //$multipleBookingCustomerAry[$dtCreatedOn] = $investorDetailsAry['multipleBookingCustomerAry'][$dtCreatedOn] ;  
                        $totalSalesRevenue[$dtCreatedOn]=$value['fSaleRevenue']+$salesSales; 
                        $totalSalesRevenue[$dtCreatedOn]=$investorDetailsAry['totalRevenueUSD'][$dtCreatedOn];
                        
                        $iInvestorNumBooking[$dtCreatedOn] = $value['iInvestorNumBooking'] ;
                        $fInvestorSaleTurnOver[$dtCreatedOn] = $value['fInvestorSaleTurnOver'] ;
                       
                        $uniqueCustomer[$dtCreatedOn]=$uniqueCustomerData['uniqueUser'][$dtCreatedOn];
                        $multipleBookingCustomerAry[$dtCreatedOn] = $uniqueCustomerData['uniqueRepeart'][$dtCreatedOn]; 
                        //$avarageSalesRevenue[$dtCreatedOn] = round((float)($totalSalesRevenue[$dtCreatedOn]/$cummulativeBookingAry[$dtCreatedOn]));
                        
                        if($iInvestorNumBooking[$dtCreatedOn]>0)
                        {
                            $avarageSalesRevenue[$dtCreatedOn] = round((float)($fInvestorSaleTurnOver[$dtCreatedOn]/$iInvestorNumBooking[$dtCreatedOn])); 
                        }
                        else
                        {
                            $avarageSalesRevenue[$dtCreatedOn] = 0.00;
                        } 
                    }
                }
                 
                $imgArr[1] = $this->createImageDashBoard($iBookings,1);
                $imgArr[2] = $this->createImageDashBoard($fTurnover,2,true);
                $imgArr[3] = $this->createImageDashBoard($cummulativeBookingAry,3);
                $imgArr[4] = $this->createImageDashBoard($salesRevenue,4);  
                $imgArr[5] = $this->createImageDashBoard($uniqueCustomer,5);   
                $imgArr[6] = $this->createImageDashBoard($iSearches,6);
                $imgArr[7] = $this->createImageDashBoard($multipleBookingCustomerAry,7);
                $imgArr[8] = $this->createImageDashBoard($iCustomerLoggedIn,8);
                $imgArr[9] = $this->createImageDashBoard($workingCaps,9,true);
                $imgArr[10] = $this->createImageDashBoard($avarageSalesRevenue,10);
            }  
            return $imgArr ;
        }
        
        function getInvestorDashboardDetails()
        {
            $cummulativeBookingAry = array();
            $uniqueCustomerAry = array();
            $multipleBookingCustomerAry = array();
            $iNumberOfBookingOld=0;
            
        	$query="  
                SELECT	
                    COUNT(id) iNumBookingReceived,
                    dtBookingConfirmed
                FROM
                    ".__DBC_SCHEMATA_BOOKING__." as b1
                WHERE 
                    date_format(dtBookingConfirmed,'%Y-%m') <= '".date('Y-m',strtotime("-11 MONTH"))."'
                AND 
                    idBookingStatus IN(3,4) 
                AND
                    iTransferConfirmed = '0' 
                ORDER BY
                    dtBookingConfirmed ASC
            ";
            //echo $query;
            if(($result = $this->exeSQL($query)))
            { 
                $ctr=0;
                $iNumBookingReceived = 0;
                $row=$this->getAssoc($result);
                $iNumberOfBookingOld=$row['iNumBookingReceived'];
                 
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
            
            $query="  
                SELECT	
                    COUNT(id) iNumBookingReceived,
                    dtBookingConfirmed
                FROM
                    ".__DBC_SCHEMATA_BOOKING__." as b1
                WHERE 
                    date_format(dtBookingConfirmed,'%Y-%m') > '".date('Y-m',strtotime("-11 MONTH"))."'
                AND 
                    idBookingStatus IN(3,4) 
                AND
                    iTransferConfirmed = '0' 
                GROUP BY
                    MONTH(dtBookingConfirmed)
                ORDER BY
                    dtBookingConfirmed ASC
            ";
            //echo $query;
            if(($result = $this->exeSQL($query)))
            { 
                $ctr=0;
                $iNumBookingReceived = 0;
                while($row=$this->getAssoc($result))
                {
                    $key = date('Ym01',strtotime($row['dtBookingConfirmed']));
                    $iNumBookingReceived = $row['iNumBookingReceived'];
                    $cummulativeBookingAry[$key] = $iNumBookingReceived ;
                    $ctr++;
                } 
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
            
        $iNumberofUniqueCustomerOld=0;   
        $query="  
                SELECT	
                    COUNT(DISTINCT(idUser)) iNumUniqueReceived,
                    dtBookingConfirmed
                FROM
                    ".__DBC_SCHEMATA_BOOKING__." as b1
                WHERE 
                    date_format(dtBookingConfirmed,'%Y-%m') <= '".date('Y-m',strtotime("-11 MONTH"))."'
                AND 
                    idBookingStatus IN(3,4) 
                AND
                    iTransferConfirmed = '0'     
               ORDER BY
                    dtBookingConfirmed ASC
            ";
            //echo $query;
            if(($result = $this->exeSQL($query)))
            { 
                $ctr=0;
                $iNumUniqueReceived = 0;
                $row=$this->getAssoc($result);
               $iNumberofUniqueCustomerOld=$row['iNumUniqueReceived'];
                   
                
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
            
            $query="  
                SELECT	
                    COUNT(DISTINCT(idUser)) iNumUniqueReceived,
                    dtBookingConfirmed
                FROM
                    ".__DBC_SCHEMATA_BOOKING__." as b1
                WHERE 
                    date_format(dtBookingConfirmed,'%Y-%m') > '".date('Y-m',strtotime("-11 MONTH"))."'
                AND 
                    idBookingStatus IN(3,4) 
                AND
                    iTransferConfirmed = '0' 
                GROUP BY
                    idUser
                ORDER BY
                    dtBookingConfirmed ASC
            ";
            //echo $query;
            if(($result = $this->exeSQL($query)))
            { 
                $ctr=0;
                $iNumUniqueReceived = 0;
                while($row=$this->getAssoc($result))
                {
                    $iNumUniqueOldReceived=0;
                    $key = date('Ym01',strtotime($row['dtBookingConfirmed']));
                    $iNumUniqueOldReceived=$uniqueCustomerAry[$key];
                    $iNumUniqueReceived = $row['iNumUniqueReceived']+$iNumUniqueOldReceived;
                    $uniqueCustomerAry[$key] = $iNumUniqueReceived ;
                    $ctr++;
                } 
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
            
            $iNumberofCustomerWithMultipleBookingOld=0;
            $query="  
                SELECT	
                    COUNT(DISTINCT(idUser)) iNumCustomerReceived,
                    COUNT(id) as iNumBooking,
                    dtBookingConfirmed
                FROM
                    ".__DBC_SCHEMATA_BOOKING__." as b1
                WHERE 
                    date_format(dtBookingConfirmed,'%Y-%m') < '".date('Y-m',strtotime("-11 MONTH"))."'
                AND 
                    idBookingStatus IN(3,4) 
                AND
                    iTransferConfirmed = '0'
                HAVING
                    iNumBooking > 1
                ORDER BY
                    dtBookingConfirmed ASC
            ";
           //echo $query."<br/>";
            if(($result = $this->exeSQL($query)))
            { 
                $ctr=0;
                $iNumCustomerReceived = 0;
                $row=$this->getAssoc($result); 
                $iNumberofCustomerWithMultipleBookingOld=$row['iNumCustomerReceived'];
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
            
            $query="  
                SELECT	
                    COUNT(DISTINCT(idUser)) iNumCustomerReceived,
                    COUNT(id) as iNumBooking,
                    dtBookingConfirmed
                FROM
                    ".__DBC_SCHEMATA_BOOKING__." as b1
                WHERE 
                    date_format(dtBookingConfirmed,'%Y-%m') > '".date('Y-m',strtotime("-11 MONTH"))."'
                AND 
                    idBookingStatus IN(3,4) 
                AND
                    iTransferConfirmed = '0' 
                GROUP BY
                    MONTH(dtBookingConfirmed), idUser
                HAVING
                    iNumBooking > 1
                ORDER BY
                    dtBookingConfirmed ASC
            ";
            //echo $query;
            if(($result = $this->exeSQL($query)))
            { 
                $ctr=0;
                $iNumCustomerReceived = 0;
                while($row=$this->getAssoc($result))
                {
                    $key = date('Ym01',strtotime($row['dtBookingConfirmed']));
                    $iNumCustomerReceived = $row['iNumCustomerReceived'];
                    $multipleBookingCustomerAry[$key] = $iNumCustomerReceived ;
                    $ctr++;
                } 
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
            
            
            
            $fTotalInsuranceSellPriceOld=0;
            $query="
                 SELECT	
                        SUM(fTotalInsuranceCostForBookingUSD) AS fTotalInsuranceSellPriceUSD,
                        dtBookingConfirmed		
                    FROM
                            ".__DBC_SCHEMATA_BOOKING__."
                    WHERE
                    	date_format(dtBookingConfirmed,'%Y-%m') <= '".date('Y-m',strtotime("-11 MONTH"))."'
                	AND 
                        idBookingStatus IN(3,4) 
                    AND
                        iInsuranceIncluded = '1'
                    AND
                        iInsuranceStatus !='3'
                     AND
                        iTransferConfirmed = '0'
                    
                ORDER BY
                    dtBookingConfirmed ASC    
                       
                    
            "; 	  
            //echo $query."<br/>";
            if($result=$this->exeSQL($query))
            {
                 //$row=$this->getAssoc($result);  
                   // $row['fSaleRevenue'] = ($row['fTotalInsuranceSellPrice'] - $row['fTotalInsuranceBuyPrice']) + $row['fTotalReferalAmount'] ;
                    $ctr=0;
	                $fTotalInsuranceSellPriceOld = 0;
	                $row=$this->getAssoc($result);
	                $fTotalInsuranceSellPriceOld=$row['fTotalInsuranceSellPriceUSD'];
                
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }  
            
            $query="
                 SELECT	
                    SUM(fTotalInsuranceCostForBookingUSD_buyRate) AS fTotalInsuranceBuyPriceUSD,
                    dtBookingConfirmed		
                FROM
                        ".__DBC_SCHEMATA_BOOKING__."
                WHERE
                   date_format(dtBookingConfirmed,'%Y-%m') <= '".date('Y-m',strtotime("-11 MONTH"))."'
                    AND
                    idBookingStatus IN(3,4) 
                AND
                    iInsuranceIncluded = '1'
                AND
                    iInsuranceStatus !='3'
                AND
                    iTransferConfirmed = '0'

                    ORDER BY
                    dtBookingConfirmed ASC     
            "; 	  
            //secho $query."<br/>";
            if($result=$this->exeSQL($query))
            {
                //$row=$this->getAssoc($result);  
                // $row['fSaleRevenue'] = ($row['fTotalInsuranceSellPrice'] - $row['fTotalInsuranceBuyPrice']) + $row['fTotalReferalAmount'] ;
                 $ctr=0;
                $fTotalInsuranceBuyPriceUSD = 0;
                $row=$this->getAssoc($result);
                $fTotalInsuranceBuyPriceUSDOld=$row['fTotalInsuranceBuyPriceUSD']; 
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
            
            $query="
                SELECT	
                      SUM(fReferalAmountUSD) AS fTotalReferalAmountUSD,
                      dtBookingConfirmed		
                  FROM
                      ".__DBC_SCHEMATA_BOOKING__."
                  WHERE 
                      date_format(dtBookingConfirmed,'%Y-%m') <= '".date('Y-m',strtotime("-11 MONTH"))."'
                      AND
                      idBookingStatus IN(3,4) 
                  AND
                      iTransferConfirmed = '0' 
                  ORDER BY
                      dtBookingConfirmed ASC     
            "; 	  
           // echo $query."<br/>";
            if($result=$this->exeSQL($query))
            {
                $ctr=0;
                $fTotalReferalAmountUSD = 0;
                $row=$this->getAssoc($result);
                $fTotalReferalAmountUSDOld=$row['fTotalReferalAmountUSD']; 
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
            
            $oldRevenue=($fTotalInsuranceSellPriceOld-$fTotalInsuranceBuyPriceUSDOld)+$fTotalReferalAmountUSDOld;
            
            $fTotalInsuranceSellPriceAry=array();
            $query="
                 SELECT	
                    SUM(fTotalInsuranceCostForBookingUSD) AS fTotalInsuranceSellPriceUSD,
                    dtBookingConfirmed		
                FROM
                    ".__DBC_SCHEMATA_BOOKING__."
                WHERE
                    date_format(dtBookingConfirmed,'%Y-%m') > '".date('Y-m',strtotime("-11 MONTH"))."'
                AND 
                    idBookingStatus IN(3,4) 
                AND
                    iInsuranceIncluded = '1'
                AND
                    iInsuranceStatus !='3'
                 AND
                    iTransferConfirmed = '0'
                GROUP BY
                    MONTH(dtBookingConfirmed)
                ORDER BY
                    dtBookingConfirmed ASC     
            "; 	  
            if($result=$this->exeSQL($query))
            {
                //$row=$this->getAssoc($result);  
                // $row['fSaleRevenue'] = ($row['fTotalInsuranceSellPrice'] - $row['fTotalInsuranceBuyPrice']) + $row['fTotalReferalAmount'] ;
                $ctr=0;
                $fTotalInsuranceSellPrice = 0;
                while($row=$this->getAssoc($result))
                {
                    $key = date('Ym01',strtotime($row['dtBookingConfirmed']));
                    $fTotalInsuranceSellPrice = $row['fTotalInsuranceSellPriceUSD'];
                    $fTotalInsuranceSellPriceAry[$key] = $fTotalInsuranceSellPrice ;
                    $ctr++;
                }  
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
            
            //print_r($fTotalInsuranceSellPriceAry);  
            $fTotalInsuranceBuyPriceUSDAry=array();
            $query="
                 SELECT	
                    SUM(fTotalInsuranceCostForBookingUSD_buyRate) AS fTotalInsuranceBuyPriceUSD,
                    dtBookingConfirmed		
                FROM
                    ".__DBC_SCHEMATA_BOOKING__."
                WHERE
                   date_format(dtBookingConfirmed,'%Y-%m') > '".date('Y-m',strtotime("-11 MONTH"))."'
                AND
                    idBookingStatus IN(3,4) 
                AND
                    iInsuranceIncluded = '1'
                AND
                    iInsuranceStatus !='3'
                AND
                    iTransferConfirmed = '0'
                GROUP BY
                    MONTH(dtBookingConfirmed)
                ORDER BY
                    dtBookingConfirmed ASC 
            "; 	  
            if($result=$this->exeSQL($query))
            {
                //$row=$this->getAssoc($result);  
                // $row['fSaleRevenue'] = ($row['fTotalInsuranceSellPrice'] - $row['fTotalInsuranceBuyPrice']) + $row['fTotalReferalAmount'] ;
                 $ctr=0;
                 $fTotalInsuranceBuyPriceUSD = 0;
                 while($row=$this->getAssoc($result))
                 {
                    $key = date('Ym01',strtotime($row['dtBookingConfirmed']));
                    $fTotalInsuranceBuyPriceUSD = $row['fTotalInsuranceBuyPriceUSD'];
                    $fTotalInsuranceBuyPriceUSDAry[$key] = $fTotalInsuranceBuyPriceUSD ;
                    $ctr++;
                 }  
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }  
            $fTotalReferalAmountUSDAry=array();
            
            $query="
                SELECT	
                    SUM(fReferalAmountUSD) AS fTotalReferalAmountUSD,
                    dtBookingConfirmed		
                FROM
                    ".__DBC_SCHEMATA_BOOKING__."
                WHERE 
                    date_format(dtBookingConfirmed,'%Y-%m') > '".date('Y-m',strtotime("-11 MONTH"))."'
                AND
                    idBookingStatus IN(3,4) 
                AND
                    iTransferConfirmed = '0'
                GROUP BY
                    MONTH(dtBookingConfirmed)
                ORDER BY
                    dtBookingConfirmed ASC     
            "; 	  
           // echo $query;
            if($result=$this->exeSQL($query))
            {
                $ctr=0;
                $fTotalReferalAmountUSD = 0;
                while($row=$this->getAssoc($result))
                {
                    $key = date('Ym01',strtotime($row['dtBookingConfirmed']));
                    $fTotalReferalAmountUSD = $row['fTotalReferalAmountUSD'];
                    $fTotalReferalAmountUSDAry[$key] = $fTotalReferalAmountUSD ;
                    $ctr++;
                }  
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }  
            $ret_ary = array();
            
            $dateSearchAry['dtDateFrom'] = date('Y-m-01',strtotime("-11 MONTH"));
            $dateSearchAry['dtDateTo'] = date('Y-m-01');
            
            $fromDate = strtotime($dateSearchAry['dtDateFrom']);
            $toDate = strtotime($dateSearchAry['dtDateTo']);
            $counter = $fromDate ;
            
            $iNumberOfBooking = $iNumberOfBookingOld;
            $iNumberofUniqueCustomer = $iNumberofUniqueCustomerOld;
            $iNumberofCustomerWithMultipleBooking = $iNumberofCustomerWithMultipleBookingOld;
            $totalRevenueUSD=$oldRevenue;
            while($counter <= $toDate)
            { 
                $key = date('Ym01',$counter);    
                $keyStr = strtoupper(date('M',$counter)); 
                $revUSD=0;
                $ret_ary['cummulativeBookingAry'][$keyStr] = $iNumberOfBooking + $cummulativeBookingAry[$key];
                $ret_ary['uniqueCustomerAry'][$keyStr] = $iNumberofUniqueCustomer + $uniqueCustomerAry[$key]; 
                $ret_ary['multipleBookingCustomerAry'][$keyStr] = $iNumberofCustomerWithMultipleBooking + $multipleBookingCustomerAry[$key];
                
                $revUSD=($fTotalInsuranceSellPriceAry[$key] - $fTotalInsuranceBuyPriceUSDAry[$key])+$fTotalReferalAmountUSDAry[$key];
                $ret_ary['totalRevenueUSD'][$keyStr] = $totalRevenueUSD + $revUSD;
                
                
                $iNumberOfBooking = $ret_ary['cummulativeBookingAry'][$keyStr] ;
                $iNumberofUniqueCustomer = $ret_ary['uniqueCustomerAry'][$keyStr] ;
                $iNumberofCustomerWithMultipleBooking = $ret_ary['multipleBookingCustomerAry'][$keyStr] ;
                $totalRevenueUSD=$ret_ary['totalRevenueUSD'][$keyStr]; 
                $counter = strtotime("+1 MONTH",$counter);
            } 
            return $ret_ary ;
        }
        
        function getLastHundresBookings($dtDateTime=false)
        {
            if(!empty($dtDateTime))
            {
                $query_date = " date_format(dtBookingConfirmed,'%Y-%m-%d') <= '".  mysql_escape_custom(date('Y-m-d',strtotime($dtDateTime)))."' ";
            }
            else
            {
                $query_date = " date_format(dtBookingConfirmed,'%Y-%m-%d') <= date_format(now()  , '%Y-%m-%d' )  ";
            }
            
            $query=" 
                SELECT	
                    id
                FROM
                    ".__DBC_SCHEMATA_BOOKING__."
                WHERE 
                    $query_date
                AND 
                    idBookingStatus IN(3,4)  
                 AND
                    iTransferConfirmed = '0' 
                ORDER BY
                    dtBookingConfirmed DESC
                LIMIT
                    0,300
            ";
            
            $this->lastHundreQuery = $query;
            
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                $ret_ary = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    $ret_ary[$ctr] = $row['id'];
                    $ctr++;
                } 
                return $ret_ary;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
        }
        
        function getLastMonthCumulativeRevenue($dtDateTime=false)
        { 
            $bookingIdsAry = $this->getLastHundresBookings($dtDateTime);
            if(!empty($bookingIdsAry))
            {
                $bookingIdsStr = implode(",",$bookingIdsAry);
                $query_and = " AND id IN (".$bookingIdsStr.") ";
                $query_label_and = " AND bt.id IN (".$bookingIdsStr.") ";
                $query_label_and_trans = " AND idBooking IN (".$bookingIdsStr.") ";
                
                if(!empty($dtDateTime))
                {
                    $query_date = " date_format(dtBookingConfirmed,'%Y-%m-%d') <= '".  mysql_escape_custom(date('Y-m-d',strtotime($dtDateTime)))."' ";
                     $query_date_payment = " date_format(dtPaymentConfirmed,'%Y-%m') <= '".  mysql_escape_custom(date('Y-m',strtotime($dtDateTime)))."' ";
                }
                else
                {
                    $query_date = " date_format(dtBookingConfirmed,'%Y-%m-%d') <= date_format(now()  , '%Y-%m-%d' )  ";
                    $query_date_payment = " date_format(dtPaymentConfirmed,'%Y-%m') <= date_format(now()  , '%Y-%m' )  ";
                }
                $query="
                    SELECT * FROM
                    (
                        SELECT	
                            SUM(fTotalInsuranceCostForBookingUSD) AS fTotalInsuranceSellPriceUSD		
                        FROM
                                ".__DBC_SCHEMATA_BOOKING__."
                        WHERE 
                            $query_date
                        AND 
                            idBookingStatus IN(3,4) 
                        AND
                            iInsuranceIncluded = '1'
                        AND
                            iInsuranceStatus !='3'
                         AND
                            iTransferConfirmed = '0' 
                            $query_and 
                    ) AS fTotalInsuranceSellPrice, 
                    (
                        SELECT	
                            SUM(fTotalInsuranceCostForBookingUSD_buyRate) AS fTotalInsuranceBuyPriceUSD		
                        FROM
                                ".__DBC_SCHEMATA_BOOKING__."
                        WHERE 
                            $query_date
                        AND 
                            idBookingStatus IN(3,4) 
                        AND
                            iInsuranceIncluded = '1'
                        AND
                            iInsuranceStatus !='3'
                        AND
                            iTransferConfirmed = '0' 
                        $query_and
                    ) AS fTotalInsuranceBuyPrice,  
                    (
                        SELECT	
                            SUM(fReferalAmountUSD) AS fTotalReferalAmountUSD		
                        FROM
                            ".__DBC_SCHEMATA_BOOKING__."
                        WHERE
                            $query_date
                        AND 
                            idBookingStatus IN(3,4) 
                        AND
                            iTransferConfirmed = '0' 
                        $query_and
                    ) AS fTotalReferalAmount,
                    (
                        SELECT	
                            SUM(cbd.fBookingLabelFeeRate * cbd.fBookingLabelFeeROE ) AS fTotalLabelFeeAmountUSD		
                        FROM
                            ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__." AS cbd
                        INNER JOIN
                            ".__DBC_SCHEMATA_BOOKING__." as bt
                        ON
                            bt.id=cbd.idBooking
                        WHERE  
                            $query_date
                        AND
                            bt.idBookingStatus IN(3,4) 
                        AND
                            bt.iTransferConfirmed = '0'
                        AND
                            cbd.iCourierAgreementIncluded = '0'
                        AND
                        (
                            bt.iCourierBooking='1'
                        OR
                            bt.isManualCourierBooking='1'
                        )
                        $query_label_and 
                    ) AS fTotalLabelFeeAmount,
                    (
                    SELECT	
                        SUM(fTotalHandlingFeeUSD) AS fTotalBookingHandlingFeeUSD		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__."
                    WHERE 
                        $query_date
                    AND
                        idBookingStatus IN(3,4)
                    AND
                        iHandlingFeeApplicable=1
                    AND
                        iTransferConfirmed = '0'
                    $query_and 
                ) AS fTotalBookingHandlingFeeUSD,
                (
                    SELECT	
                        SUM(fPriceMarkupCustomerCurrency * fCustomerExchangeRate) AS fPriceMarkupUSD		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__." as b4
                    WHERE 
                        $query_date
                    AND
                        idBookingStatus IN(3,4) 
                    AND
                        iTransferConfirmed = '0' 
                    $query_and 
                ) AS fPriceMarkupUSD,
                (
                    SELECT	
                        SUM(fTotalPriceUSD) AS fMarkupInvoiceAmount		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__." as b5
                    WHERE
                        $query_date 
                    AND 
                        idBookingStatus IN(3,4) 
                    AND
                        iTransferConfirmed = '0'  
                    AND
                        iForwarderGPType = '".__FORWARDER_PROFIT_TYPE_MARK_UP__."'
                    $query_and        
                ) AS fMarkupInvoiceAmt,
                (
                    SELECT	
                        SUM(fTotalPriceForwarderCurrency) AS fMarkupInvoicePaid		
                    FROM
                        ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                    WHERE
                         $query_date_payment
                    AND 
                        iDebitCredit = '".__TRANSACTION_DEBIT_CREDIT_FLAG_10__."'
                    $query_label_and_trans
                ) AS fMarkupInvoiceAmountPaid                
                ";
                //echo $query."<br><br>"; 
                $this->revenueQuery = $query;
                
                if( ( $result = $this->exeSQL( $query ) ) )
                {
                    $row=$this->getAssoc($result);    
                    $ret_ary = array();
                    $ret_ary['fInvestorSaleTurnOver'] =($row['fMarkupInvoiceAmount'] - $row['fMarkupInvoicePaid']) +  ($row['fTotalInsuranceSellPriceUSD'] - $row['fTotalInsuranceBuyPriceUSD']) + $row['fTotalReferalAmountUSD'] + $row['fTotalLabelFeeAmountUSD'] + $row['fTotalBookingHandlingFeeUSD'] + $row['fPriceMarkupUSD'];
                    $ret_ary['iInvestorNumBooking'] = 300;
                    return $ret_ary;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                } 
            }
        } 
        function getAccumulatedBookingReports()
        { 
            $query="
                SELECT *
                    FROM
                    (
                        SELECT	
                            ROUND(IFNULL(SUM(fTotalPriceUSD),0),1) AS fTurnover		
                        FROM
                            ".__DBC_SCHEMATA_BOOKING__." as b1
                        WHERE
                            ( 
                                date_format(dtBookingConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."'
                            AND 
                                idBookingStatus IN(3,4) 
                            AND
                                iTransferConfirmed = '0'
                            )
                    ) AS fTurnoverTable, 
                    (
                        SELECT	
                            IFNULL(COUNT(id),0) AS iBookings		
                        FROM
                            ".__DBC_SCHEMATA_BOOKING__." as b2 
                        WHERE
                        ( 
                            date_format(dtBookingConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."'
                        AND 
                            idBookingStatus IN(3,4)  
                        AND
                            iTransferConfirmed = '0'
                        )
                    ) AS iBookingsTable, 
                    (
                        SELECT	
                            SUM(fTotalInsuranceCostForBookingUSD) AS fTotalInsuranceSellPriceUSD		
                        FROM
                                ".__DBC_SCHEMATA_BOOKING__." as b2 
                        WHERE
                        ( 
                            date_format(dtBookingConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."'
                        AND 
                            idBookingStatus IN(3,4) 
                        AND
                            iInsuranceIncluded = '1'
                        AND
                            iInsuranceStatus !='3'
                         AND
                            iTransferConfirmed = '0'
                        )
                    ) AS fTotalInsuranceSellPrice,
                    (
                        SELECT	
                            SUM(fTotalInsuranceCostForBookingUSD_buyRate) AS fTotalInsuranceBuyPriceUSD		
                        FROM
                            ".__DBC_SCHEMATA_BOOKING__." as b3
                        WHERE
                        ( 
                            date_format(dtBookingConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."' 
                        AND 
                            idBookingStatus IN(3,4) 
                        AND
                            iInsuranceIncluded = '1'
                        AND
                            iInsuranceStatus !='3'
                        AND
                            iTransferConfirmed = '0'
                        )
                    ) AS fTotalInsuranceBuyPrice, 
                    (
                        SELECT	
                            SUM(fReferalAmountUSD) AS fTotalReferalAmountUSD		
                        FROM
                            ".__DBC_SCHEMATA_BOOKING__." as b4
                        WHERE
                            date_format(dtBookingConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."' 
                        AND 
                            idBookingStatus IN(3,4) 
                        AND
                            iTransferConfirmed = '0' 
                    ) AS fTotalReferalAmount,
                    (
                        SELECT	
                            ROUND(IFNULL(SUM(fTotalInsuranceCostForBookingUSD),0),1) AS fInsuranceTurnOver		
                        FROM
                            ".__DBC_SCHEMATA_BOOKING__." as b5
                        WHERE
                        (  
                            date_format(dtBookingConfirmed,'%Y-%m') =  '".date('Y-m',strtotime($dtDateTime))."'
                        AND 
                            idBookingStatus IN(3,4) 
                        AND
                            iInsuranceIncluded = '1'
                        AND
                            iInsuranceStatus !='3'
                        AND
                            iTransferConfirmed = '0'
                        )
                    ) AS fInsuranceTurnoverTable
                ";
                //echo $query;
                if( ( $result = $this->exeSQL( $query ) ) )
                {
                    $row=$this->getAssoc($result); 
                    $row['fSaleRevenue'] = ($row['fTotalInsuranceSellPriceUSD'] - $row['fTotalInsuranceBuyPriceUSD']) + $row['fTotalReferalAmountUSD'] ;  
                    return $row;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
        } 
        
	private function createImageDashBoard($values,$id,$working_capital_graph=false)
	{
            # ------- The graph values in the form of associative array 
            $img_width=460;
            $img_height=260; 
            $margins=30;
            $imageName="images".$id.".png";
            $imageAppPath=__APP_PATH_ROOT__."/metrics/graph/dashboard/".$imageName;
            if (file_exists($imageAppPath)) 
            {
                unlink($imageAppPath);
            }

            # ---- Find the size of graph by substracting the size of borders
            //$graph_width=$img_width - $margins * 2;
            $graph_width = $img_width ;
            $graph_height=$img_height - $margins * 2; 
            $img=imagecreate($img_width,$img_height);


            $bar_width=25;
            $total_bars=count($values);
            $gap= ($graph_width- $total_bars * $bar_width ) / ($total_bars +1);
            $gap = 10;
            //echo $gap ;

            # -------  Define Colors ----------------
            $bar_color=imagecolorallocatealpha($img,179,179,152,5);
            $border_color=imagecolorallocate($img,255,255,255);
            $base_line= imagecolorallocatealpha($img,125,116,104,5);
            $base_line_string = imagecolorallocatealpha($img,0,0,0,5);
            # ------ Create the border around the graph ------
            imagesetthickness($img, 2);
            imagefilledrectangle($img,0,0,$img_width,$img_height,$border_color);


            # ------- Max value is required to adjust the scale	-------
            $max_value=max($values) + 2;
            $ratio= $graph_height/$max_value;
            imageline ( $img ,20,15, 20 , 230, $base_line);
            imageline ( $img ,20,15, 25 , 25, $base_line);
            imageline ( $img ,20,15, 15 , 25, $base_line);
            imageline ( $img ,440,230, 20 , 230, $base_line);

            #-------- Center align the value in the graph -------
            $position = strlen('"'.$max_value.'"');

            # ----------- Draw the bars here ------
            //$font = 'arial.ttf';
            $margins2= 20 ;
            $font_file_name = __APP_PATH__.'/fonts/font.ttf';
            $font_file_name2 = __APP_PATH__.'/fonts/Calibri.ttf';
            for($i=0;$i< $total_bars; $i++)
            { 
                # ------ Extract key and value pair from the current pointer position
                list($key,$value)=each($values);

                $x1= $margins2 + $gap + $i * ($gap+$bar_width) ;
                $x2= $x1 + $bar_width; 
                $y1=$margins +$graph_height- intval($value * $ratio) ;
                $y2=$img_height-$margins;

                # ----------- Setting the format for to view ------
                if($working_capital_graph)
                {
                    $showValue = number_format($value,1);
                }
                else
                {
                    $showValue = number_format($value);
                } 

                if($value<0) 
                {
                    imagettftext($img, 10, 0, $x1-10, 50, $base_line_string, $font_file_name,$showValue);
                }
                else
                {
                    imagettftext($img, 10, 0, $x1-$position+10, $y1-5, $base_line_string, $font_file_name,$showValue);
                }

                //imagestring($img,3,$x1+15,$y1-15,number_format($value),$bar_color);
                //imagestring($img,4,$x1+4,$img_height-20,$key,$base_line_string);		
                imagettftext($img, 10, 0, $x1+2, $img_height-10, $base_line_string, $font_file_name2, $key);
                imagefilledrectangle($img,$x1,$y1,$x2,$y2,$base_line);
            }  
            imagepng($img,"graph/dashboard/images".$id.".png");
            imagedestroy($img); 
            return "graph/dashboard/images".$id.".png";
	}
	
	function updatePassword($data,$id)
	{
            if(is_array($data) && (int)$id>0)
            {
                $this->set_szPassword(trim(sanitize_all_html_input($data['szNewPassword'])));
                $this->set_szConNewPassword(trim(sanitize_all_html_input($data['szConPassword'])));
                $this->set_id(trim(sanitize_all_html_input($id)));

                if ($this->error === true)
                {
                    return false;
                }

                if(!empty($this->szPassword) && $this->szPassword!=$this->szConNewPassword)
                {
                    $this->addError( "szConfirmPassword" , t($this->t_base.'fields/metrics_password_mismatch') );
                    return false;
                }

                $query="
                    UPDATE
            		".__DBC_SCHEMATA_INVESTORS__."
                    SET
            		szPassword='".mysql_escape_custom(md5($this->szPassword))."',
            		iPasswordUpdated='0' 
                    WHERE
                        id='".(int)$this->id."'
                "; 
                if( $result = $this->exeSQL( $query ) )
                {
                    return true;	
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
            else
            {
                return false;
            }
	}
	
	function uniqueCustomerDoneBooking()
	{ 
            $dateSearchAry['dtDateFrom'] = date('Y-m-01',strtotime("-11 MONTH"));
            $dateSearchAry['dtDateTo'] = date('Y-m-01');
            
            $fromDate = strtotime($dateSearchAry['dtDateFrom']);
            $toDate = strtotime($dateSearchAry['dtDateTo']);
            $counter = $fromDate ;
            
            $iNumberOfBooking = $iNumberOfBookingOld;
            $iNumberofUniqueCustomer = $iNumberofUniqueCustomerOld;
            $iNumberofCustomerWithMultipleBooking = $iNumberofCustomerWithMultipleBookingOld;
            $totalRevenueUSD=$oldRevenue;
            while($counter <= $toDate)
            { 
                $key = date('Ym01',$counter);    
                $keyStr = strtoupper(date('M',$counter));   
                
                $newDate=date('Y-m',$counter);	
                
                $query="
                    SELECT
	            	count(DISTINCT(b.idUser)) iNumUniqueUser
	            FROM
	            	".__DBC_SCHEMATA_BOOKING__." b
	            WHERE
	                b.idBookingStatus IN (3,4)
	            AND 
	                b.iTransferConfirmed = '0'
	            AND
	            	 date_format(dtBookingConfirmed,'%Y-%m') <= '".$newDate."'   
	            ORDER BY
	                    dtBookingConfirmed ASC
                ";
                //echo $query."<br/><br/>";
                if($result=$this->exeSQL($query))
                {  
                    while($row=$this->getAssoc($result))
                    {
                        $key=strtoupper(date('M',strtotime($newDate)));
                        $uniqueArr['uniqueUser'][$key]=$row['iNumUniqueUser'];
                    }               
                }
            
                $query="  
                    SELECT	
                        COUNT(DISTINCT(idUser)) iNumCustomerReceived,
                        COUNT(id) as iNumBooking,
                        dtBookingConfirmed
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__." as b1
                    WHERE 
                        date_format(dtBookingConfirmed,'%Y-%m') <= '".$newDate."'
                    AND 
                        idBookingStatus IN(3,4) 
                    AND
                        iTransferConfirmed = '0'
                    GROUP BY    
                            idUser    
                    HAVING
                        iNumBooking > 1
                    ORDER BY
                        dtBookingConfirmed ASC
                ";
               // echo $query."<br/>";
                if($result=$this->exeSQL($query))
                {  
                    $res_arr=array();
                    while($row=$this->getAssoc($result))
                    {
                            $res_arr[]=$row;	
                    } 
                    $key=strtoupper(date('M',strtotime($newDate)));
                    $uniqueArr['uniqueRepeart'][$key]=count($res_arr);
                } 
                $counter = strtotime("+1 MONTH",$counter); 
            }
            return $uniqueArr;
	}
        
        function getLastMonthCumulativeRevenueScript($dtDateTime=false)
        {   
             if(!empty($dtDateTime))
            {
                $query_date = " date_format(dtBookingConfirmed,'%Y-%m-%d') <= '".  mysql_escape_custom(date('Y-m-d',strtotime($dtDateTime)))."' ";
            }
            else
            {
                $query_date = " date_format(dtBookingConfirmed,'%Y-%m-%d') <= date_format(now()  , '%Y-%m-%d' )  ";
            }
            
            $query=" 
                SELECT	
                    fTotalInsuranceCostForBookingUSD  AS fTotalInsuranceSellPriceUSD,
                    fTotalInsuranceCostForBookingUSD_buyRate,
                    iInsuranceIncluded,
                    fReferalAmountUSD,
                    iInsuranceStatus,
                    szBookingRef,
                    dtBookingConfirmed,
                    (SELECT SUM(fBookingLabelFeeRate*fBookingLabelFeeROE) AS fTotalLabelFeeAmountUSD FROM ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__." cb WHERE cb.idBooking = b.id AND cb.iCourierAgreementIncluded = '0' AND (b.iCourierBooking='1' OR b.isManualCourierBooking='1')) as fLabelFee
                FROM
                    ".__DBC_SCHEMATA_BOOKING__." b
                WHERE  
                   $query_date
                AND 
                    idBookingStatus IN(3,4)  
                 AND
                    iTransferConfirmed = '0' 
                ORDER BY
                    dtBookingConfirmed DESC
                LIMIT
                    0,100
            ";
            //echo $query."<br><br>";
            //die;
            if($result = $this->exeSQL($query))
            {
                $ctr = 0;
                $ret_ary = array();
                while($row=$this->getAssoc($result))
                {
                    $ret_ary[$ctr] = $row;
                    $ctr++;
                }  
                return $ret_ary;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }  
        }
        
        function downloadAverageSalesRevenue()
        { 
            $kBooking = new cBooking(); 
            require_once( __APP_PATH_CLASSES__ . "/PHPExcel.php" );

            $objPHPExcel=new PHPExcel(); 

            $sheetIndex=0;
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->createSheet();
            $objPHPExcel->setActiveSheetIndex($sheetIndex);
            $sheet = $objPHPExcel->getActiveSheet(); 

            $styleArray = array(
                'font' => array(
                        'bold' => false,
                        'size' =>12,
                ),
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                ),
                'borders' => array(
                    'top' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                    ),
                    'bottom' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                    ),
                    'right' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                    ),
                    'left' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                    ),
                ),
            ); 
            $styleArray1 = array(
                    'font' => array(
                        'bold' => false,
                        'size' =>12,
                    ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    ),
                    'borders' => array(
                            'top' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'bottom' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'right' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'left' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                    ),
            ); 

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);

            $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->getStartColor()->setARGB('FFddd9c3');
            //$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);

            $objPHPExcel->getActiveSheet()->setCellValue('A1','Booking')->getStyle('A1');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);  
            
            $objPHPExcel->getActiveSheet()->setCellValue('B1','Date')->getStyle('B1');
            $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('C1','Insurance Sale Price')->getStyle('C1');
            $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setCellValue('D1','Insurance Buy Price')->getStyle('D1');
            $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setCellValue('E1','Referal Fee')->getStyle('E1');
            $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setCellValue('F1','Label Fee')->getStyle('F1');
            $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray); 
            $col = 2;
             
            $dtDateTime_1 = date('Y-m-d');
            $last100BookingDataAry = array();
            $last100BookingDataAry = $this->getLastMonthCumulativeRevenueScript($dtDateTime_1);
             
            if(!empty($last100BookingDataAry)) 
            {
                foreach($last100BookingDataAry as $last100BookingDataArys)
                {  
                    $fTotalInsuranceSellPriceUSD = 0;
                    $fTotalInsuranceCostForBookingUSD_buyRate = 0;
                    if($last100BookingDataArys['iInsuranceIncluded']==1 && $last100BookingDataArys['iInsuranceStatus']!=3)
                    {
                        $fTotalInsuranceSellPriceUSD = $last100BookingDataArys['fTotalInsuranceSellPriceUSD'];
                        $fTotalInsuranceCostForBookingUSD_buyRate = $last100BookingDataArys['fTotalInsuranceCostForBookingUSD_buyRate'];
                        
                        $fGrandTotalInsuranceSellPriceUSD += $fTotalInsuranceSellPriceUSD; 
                        $fGrandTotalInsuranceCostForBookingUSD_buyRate += $fTotalInsuranceCostForBookingUSD_buyRate;  
                    } 
                    $fTotalReferalAmountUSD += $last100BookingDataArys['fReferalAmountUSD'];
                    $fTotalLabelFee += $last100BookingDataArys['fLabelFee'];
                    
                    $objPHPExcel->getActiveSheet()->setCellValue("A".$col,$last100BookingDataArys['szBookingRef']);
                    $objPHPExcel->getActiveSheet()->setCellValue("B".$col,date('M Y',strtotime($last100BookingDataArys['dtBookingConfirmed'])));
                    $objPHPExcel->getActiveSheet()->setCellValue("C".$col,number_format((float)$fTotalInsuranceSellPriceUSD,2));
                    $objPHPExcel->getActiveSheet()->setCellValue("D".$col,number_format((float)$fTotalInsuranceCostForBookingUSD_buyRate,2));
                    $objPHPExcel->getActiveSheet()->setCellValue("E".$col,number_format((float)$last100BookingDataArys['fReferalAmountUSD'],2));
                    $objPHPExcel->getActiveSheet()->setCellValue("F".$col,number_format((float)$last100BookingDataArys['fLabelFee'],2));
                    $col++; 
                }    
            }
//            echo "<br> ISP: ".$fGrandTotalInsuranceSellPriceUSD." <br> IBP: ".$fGrandTotalInsuranceCostForBookingUSD_buyRate."<br> RF: ".$fTotalReferalAmountUSD."<br> LF: ".$fTotalLabelFee;
//            die;
            $col++;   
            $objPHPExcel->getActiveSheet()->getStyle('A'.$col.':F'.$col)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$col.':F'.$col)->getFill()->getStartColor()->setARGB('FFFF00');
            $objPHPExcel->getActiveSheet()->setCellValue("A".$col,'Total');
            $objPHPExcel->getActiveSheet()->setCellValue("B".$col,'');
            $objPHPExcel->getActiveSheet()->setCellValue("C".$col,number_format((float)$fGrandTotalInsuranceSellPriceUSD,2));
            $objPHPExcel->getActiveSheet()->setCellValue("D".$col,number_format((float)$fGrandTotalInsuranceCostForBookingUSD_buyRate,2));
            $objPHPExcel->getActiveSheet()->setCellValue("E".$col,number_format((float)$fTotalReferalAmountUSD,2));
            $objPHPExcel->getActiveSheet()->setCellValue("F".$col,number_format((float)$fTotalLabelFee,2));
            
            $title = "_Transporteca_average_sales_revenue";
            //$objPHPExcel->getActiveSheet()->setTitle('Transporteca Average Sales Revenue');
          
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->removeSheetByIndex(1);
            $file=date('Ymdhs').$title;
            $fileName=__APP_PATH_ROOT__."/insuranceSheets/".$file.".xlsx"; 
            $objWriter->save($fileName); 
            return $fileName;  
        }
        
	function set_iManageid($value)
	{
            $this->iId = $this->validateInput($value,__VLD_CASE_NUMERIC__,'iId',t( $this->t_base."fields/id" ),false, 255 ,true );
	}
	function set_szManageHeading($value)
	{
            $this->szHeading = $this->validateInput($value,__VLD_CASE_NAME__,'szHeading',t( $this->t_base."fields/heading" ),false, 255 ,true );
	}
	function set_iManageValue($value)
	{
            $this->iValue = $this->validateInput($value,__VLD_CASE_ANYTHING__,'iValue',t( $this->t_base."fields/value" ),false, false ,true );
	}
	function set_szEmail ( $value )
	{
            $this -> szEmail = $this->validateInput($value , __VLD_CASE_EMAIL__ , "szEmail" , t( $this->t_base."fields/email" ),false, 255 ,true );
	}
	function set_szPassword ( $value )
	{
            $this->szPassword = $this->validateInput($value , __VLD_CASE_ANYTHING__ , "szPassword" , t( $this->t_base."fields/password" ),false, 255 ,true );
	}
	
	function set_szConNewPassword ( $value )
	{
            $this->szConNewPassword = $this->validateInput($value , __VLD_CASE_ANYTHING__ , "szConPassword" , t( $this->t_base."fields/repeat_password" ),false, 255 ,true );
	}
	function set_id( $value )
	{   
            $this->id = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "id", t($this->t_base.'fields/customer_id'), false, false, true );
	}
	function set_szFirstName( $value )
	{ 
            $this->szFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szFirstName", t($this->t_base.'fields/f_name'), false, 255, true );
	}
	function set_szLastName( $value )
	{
            $this->szLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLastName", t($this->t_base.'fields/l_name'), false, 255, true );
	}
        function set_szTitle( $value )
	{
            $this->szTitle = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTitle", t($this->t_base.'fields/title'), false, 255, true );
	}
        
	function set_szOldEmail( $value )
	{
            $this->szOldEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szOldEmail", t($this->t_base.'fields/email'), false, 255, true );
	}
	function set_szOldPassword( $value )
	{
            $this->szOldPassword = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szOldPassword", t($this->t_base.'fields/current_password'), false, 255, true );
	}
	function set_szRetypePassword( $value )
	{
            $this->szRetypePassword = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szReTypePassword", t($this->t_base.'fields/re_password'), false, 255, true );
	}
	function set_szCurrency( $value )
	{
            $this->szCurrency = $this->validateInput( $value, __VLD_CASE_NAME__, "szCurrency", t($this->t_base.'fields/currency'), false, 255, true );
	}
	function set_iPricing( $value )
	{
            $this->iPricing = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iPricing", t($this->t_base.'fields/pricing'), false, 255, true );
	}
	function set_iBooking( $value )
	{
            $this->iBooking = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iBooking", t($this->t_base.'fields/booking'), false, 255, true );
	}
	function set_iSettling( $value )
	{
            $this->iSettling = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iSettling", t($this->t_base.'fields/settling'), false, 255, true );
	}
	function set_szFeed( $value )
	{
            $this->szFeed = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szFeed", t($this->t_base.'fields/feed'), false, 255, true );
	}
	function set_szBankName( $value,$fag=false )
	{ 
            $this->szBankName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szBankName", t($this->t_base.'fields/bank'), false, 255, $fag );
	}
	
	function set_szSortCode( $value,$fag=false )
	{
            $this->szSortCode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSortCode", t($this->t_base.'fields/sort_code'), false, 255, $fag );
	}
	function set_szAccountNumber( $value,$fag=false )
	{
            $this->szAccountNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAccountNumber", t($this->t_base.'fields/account_number'), false, 255, $fag );
	}
        function set_szIBANNumber( $value,$fag=false )
	{
            $this->szIBANNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szIBANNumber", t($this->t_base.'fields/iban_number'), false, 255, $fag );
	}
        
	function set_szSwiftNumber( $value,$fag=false )
	{
		$this->szSwiftNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSwiftNumber", t($this->t_base.'fields/swift'), false, 255, $fag );
	}
	function set_szNameOnAccount( $value,$fag=false )
	{
		$this->szNameOnAccount = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szNameOnAccount", t($this->t_base.'fields/account_name'), false, 255, $fag );
	}

	function set_szCountryName( $value )
	{
		$this->szCountryName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCountryName", t($this->t_base.'fields/CountryName'), false, 255, true );
	}
	
	function set_szCountryDanishName( $value )
	{
		$this->szCountryDanishName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCountryDanishName", t($this->t_base.'fields/CountryName_danish'), false, 255, true );
	}
	
	function set_szCountryISO( $value )
	{
		$this->szCountryISO = $this->validateInput( $value, __VLD_CASE_NAME__, "szCountryISO", t($this->t_base.'fields/iso'), false, 255, true );
	}
	function set_fStandardTruckRate( $value )
	{
		$this->fStandardTruckRate = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fStandardTruckRate", t($this->t_base.'fields/truckRates'), false, 255, true );
	}
	function set_iActiveFromDropdown( $value )
	{
            $this->iActiveFromDropdown = $this->validateInput( $value, __VLD_CASE_BOOL__, "iActiveFromDropdown", t($this->t_base.'fields/fromDropDown'), false, 255, true );
	}
	function set_iActiveToDropdown( $value )
	{
            $this->iActiveToDropdown = $this->validateInput( $value, __VLD_CASE_BOOL__, "iActiveToDropdown", t($this->t_base.'fields/toDropDown'), false, 255, true );
	}
        
	function set_szHeading( $value )
	{
		$this->szHeading = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szHeading", t($this->t_base.'fields/heading'), false, 255, true );
	}
	function set_szDescription( $value )
	{
		$this->szDescription = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDescription", t($this->t_base.'fields/description'), false, false, true );
	}
	function set_szNewFirstName($value)
	{
		$this->szNewFirstName = $this->validateInput($value,__VLD_CASE_NAME__,'szNewFirstName',t( $this->t_base."fields/f_name" ),false, 255 ,true );
	}
        
        function set_szCapsule($value)
	{
            $this->szCapsule = $this->validateInput($value,__VLD_CASE_NAME__,'szCapsule',t( $this->t_base."fields/capsule" ),false, 255 ,false );
	}
        
	function set_szNewLastName($value)
	{
		$this->szNewLastName = $this->validateInput($value,__VLD_CASE_NAME__,'szNewLastName',t( $this->t_base."fields/l_name" ),false, 255 ,true );
	}
	function set_szNewEmail($value)
	{
		$this->szNewEmail = $this->validateInput($value,__VLD_CASE_EMAIL__,'szNewEmail',t( $this->t_base."fields/email" ),false, 255 ,true );
	}
	function set_szNewPassword($value)
	{
		$this->szNewPassword = $this->validateInput($value,__VLD_CASE_ANYTHING__,'szNewPassword',t( $this->t_base."fields/password" ),false, 255 ,true );
	}
	function set_szComment($value)
	{
		$this->szComment = $this->validateInput($value,__VLD_CASE_ANYTHING__,'szComment',t( $this->t_base."fields/comment" ),false, false ,true );
	}
	function set_iActive( $value )
	{
            $this->iActive = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iActive", t($this->t_base.'fields/iActive'), false, false, true );
	}
	
	function set_szSubject( $value )
	{
		$this->szSubject = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSubject", t($this->t_base.'fields/subject'), false, 255, true );
	}
	function set_szTemplateSubject( $value )
	{
		$this->szTemplateSubject = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSubject", t($this->t_base.'fields/subject'), false, 255, false );
	}
	function set_szMessage( $value )
	{
		$this->szMessage = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szMessage", t($this->t_base.'fields/message'), false, false, true );
	}
	function set_iAllAccess( $value )
	{
		$this->iAllAccess = $this->validateInput( $value, __VLD_CASE_BOOL__, "iAllAccess", t($this->t_base.'fields/iAllAccess'), false, 1, true );
	}
	function set_iManageidArr($value)
	{
		$this->iIdArr[] = $this->validateInput($value,__VLD_CASE_NUMERIC__,'iId',t( $this->t_base."fields/all" ),false, 255 ,true );
	}
	function set_szManageHeadingArr($value)
	{
		$this->szHeadingArr[] = $this->validateInput($value,__VLD_CASE_NAME__,'all',t( $this->t_base."fields/all" ),false, 255 ,true );
	}
	function set_iManageValueArr($value)
	{	
		$this->iValueArr[] = $this->validateInput($value,__VLD_CASE_ANYTHING__,'all',t( $this->t_base."fields/all" ),false, false ,true );
	}
	function set_iAuto($value)
	{	
		$this->iAuto = $this->validateInput($value,__VLD_CASE_ANYTHING__,'iAuto',t( $this->t_base."fields/iAuto" ),false, false ,true );
	}
	
	function set_iSmallCountry($value)
	{	
		$this->iSmallCountry = $this->validateInput($value,__VLD_CASE_ANYTHING__,'iSmallCountry',t( $this->t_base."fields/small_country" ),false, false ,false );
	}
	
	function set_szUrl($value)
	{	
		$this->szUrl = $this->validateInput($value,__VLD_CASE_URL__,'szUrl',t( $this->t_base."fields/szUrl" ),false, false ,true );
	}
	function set_dtValidFrom( $value )
	{   
		$this->dtValidFrom = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "dtValidFrom", t($this->t_base.'fields/validity')." ".t($this->t_base.'fields/from'), false, false, true );
	}
	function set_dtExpiry( $value )
	{   
		$this->dtExpiry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "dtExpiry", t($this->t_base.'fields/validity')." ".t($this->t_base.'fields/to'), false, false, true );
	}	
	function set_searchField( $value )
	{   
		$this->searchField = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "searchField", t($this->t_base.'fields/searchField')." ".t($this->t_base.'fields/to'), false, false, false );
	}
	function set_idOriginCountry($value)
	{
		$this->idOriginCountry = $this->validateInput($value,__VLD_CASE_NUMERIC__,'idOriginCountry',t( $this->t_base."fields/idOriginCountry" ),false, 255 ,false );
	}
	function set_idDestinationCountry($value)
	{
		$this->idDestinationCountry = $this->validateInput($value,__VLD_CASE_NUMERIC__,'idDestinationCountry',t( $this->t_base."fields/idDestinationCountry" ),false, 255 ,false );
	}
	function set_idForwarder($value)
	{
		$this->idForwarder = $this->validateInput($value,__VLD_CASE_NUMERIC__,'idForwarder',t( $this->t_base."fields/idForwarder" ),false, 255 ,false );
	}	
	function set_szLatitude($value)
	{
		$this->szLatitude = $this->validateInput($value,__VLD_CASE_ANYTHING__,'szLatitude',t( $this->t_base."fields/szLatitude" ),false, 255 ,true );
	}
	function set_szLongitude($value)
	{
		$this->szLongitude = $this->validateInput($value,__VLD_CASE_ANYTHING__,'szLongitude',t( $this->t_base."fields/szLongitude" ),false, 255 ,true );
	}
        function set_fMaxFobDistance($value)
	{
            $this->fMaxFobDistance = $this->validateInput($value,__VLD_CASE_NUMERIC__,'fMaxFobDistance',t( $this->t_base."fields/fMaxFobDistance" ),false, false ,false );
	}
        
	function set_iLanguage( $value )
	{
		$this->iLanguage = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iLanguage", t($this->t_base.'fields/language'), false, false, true );
	}
        function set_idInternationalDialCode( $value )
	{
            $this->idInternationalDialCode = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idInternationalDialCode", t($this->t_base.'fields/dial_code'), false, false, true );
	}
        function set_szPhoneNumber( $value )
	{
            $this->szPhoneNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPhoneNumber", t($this->t_base.'fields/phone'), false, false, true );
	}
        function set_szRegionName( $value )
	{
            $this->szRegionName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szRegionName", t($this->t_base.'fields/region_name'), false, false, true );
	}
        function set_szRegionShortName( $value )
	{
            $this->szRegionShortName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szRegionShortName", t($this->t_base.'fields/region_short_name'), false, false, false );
	}
        
        
}?>