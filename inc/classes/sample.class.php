<?php
        function getDistanceFromAllWareHouses($distanceMeasureAry)
	{ 
            if(!empty($distanceMeasureAry))
            {
                $arrDescriptions = array("'__TOTAL_NUM_WAREHOUSE__'","'__MAX_WAREHOUSE_DISTANCE_TO_BE_CONSIDER__'"); 
                $bulkManagemenrVarAry = array();
                $bulkManagemenrVarAry = $this->getBulkManageMentVariableByDescription($arrDescriptions);      

                $iWareHouseLimit = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE__'];
                $maxWarehouseDistance = $bulkManagemenrVarAry['__MAX_WAREHOUSE_DISTANCE_TO_BE_CONSIDER__']; 
                
                $query_and = '';
		if((int)$distanceMeasureAry['idWareHouse'])
		{
                    $query_and = " AND whs.id = '".(int)$distanceMeasureAry['idWareHouse']."'" ;
		}
		if((int)$distanceMeasureAry['idForwarder']>0)
		{
                    $query_and .= " AND fbd.iAgreeTNC IN (0,1)" ;
		}
		else
		{
                    $query_and .= " AND fbd.isOnline = '1' " ;
		}
		$iDirection = $distanceMeasureAry['iDirection'];
                
                if($iDirection==1) //Origin CFS
                {
                    //$query_and .= " AND fbd.isOnline = '1' " ;
                }
                
		if($_SESSION['user_id']>0)
		{
                    $kUser = new cUser();
                    $kForwarder = new cForwarder();
                    $kUser->getUserDetails($_SESSION['user_id']);
                    $idForwarderAry = $kForwarder->getAllNonAcceptedForwarderByEmail($kUser->szEmail);
                    if(!empty($idForwarderAry))
                    {
                        $query_and .=" AND whs.idForwarder NOT IN (".implode(',',$idForwarderAry).")";
                    }
		}
		
		if((int)$distanceMeasureAry['idCountry'])
		{
                    //$query_and = " AND whs.idCountry = '".(int)$distanceMeasureAry['idCountry']."'" ;
		}
                
		/*
                    GetDistance('".$distanceMeasureAry['fTotalLatitude'].",".$distanceMeasureAry['fTotalLongitude']."',CONCAT_WS(',',whs.szLatitude,whs.szLongitude))
		*/ 
		
		$query="
                    SELECT 
                       whs.id, 
                       whs.szWareHouseName,
                       whs.idForwarder,
                       whs.szUTCOffset ,
                       whs.idCountry,
                       whs.szCity,
                       whs.szPostCode,
                       fbd.szLogo,
                       fbd.szDisplayName,
                       fbd.szCurrency,
                       cont.fStandardTruckRate,
                       cont.szCountryName,		
                       cont.szCountryISO,
                       cont.fMaxFobDistance,
                       whs.szLatitude,
                       whs.szLongitude,
                       whs.szContactPerson,
                       whs.szEmail, 
                       fbd.dtReferealFeeValid, 
                       (( 3959 * acos( cos( radians(".(float)$distanceMeasureAry['fTotalLatitude'].") ) * cos( radians( whs.szLatitude ) ) 
                       * cos( radians(whs.szLongitude) - radians(".(float)$distanceMeasureAry['fTotalLongitude'].")) + sin(radians(".(float)$distanceMeasureAry['fTotalLatitude'].")) 
                       * sin( radians(whs.szLatitude)))) * 1.609344) AS distance 
                    FROM 
                        ".__DBC_SCHEMATA_WAREHOUSES__." whs
                    INNER JOIN
                        ".__DBC_SCHEMATA_COUNTRY__." cont
                    ON
                        cont.id = whs.idCountry				
                    INNER JOIN
                        ".__DBC_SCHEMATA_FORWARDERS__."	fbd
                    ON
                        fbd.id = whs.idForwarder	 
                    WHERE
                        whs.iActive = '1'			
                            ".$query_and."		
                    HAVING 
                        distance < ".(int)$maxWarehouseDistance."
                    ORDER BY 
                        distance
                    LIMIT
                        0,$iWareHouseLimit	
		";	
		//echo "<br>".$query."<br>";
		//die; 
		if($result = $this->exeSQL($query))
		{
                    $wareHouseAry = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    { 
                        if((int)$row['fMaxFobDistance']>0 && $iDirection==1 && $row['fMaxFobDistance']<$row['distance']) //Origin CFS
                        {
                            //if fMaxFobDistance is defined for origin country and cfs distance is greater the defined FOB distance then we do not show that option. 
                        }
                        else
                        { 
                            $wareHouseAry[$row['id']]['iDistance'] = ceil($row['distance']);
                            $wareHouseAry[$row['id']]['szWareHouseName'] = $row['szWareHouseName'];
                            $wareHouseAry[$row['id']]['idForwarder'] = $row['idForwarder'];
                            $wareHouseAry[$row['id']]['fStandardTruckRate'] = $row['fStandardTruckRate'];	
                            $wareHouseAry[$row['id']]['szLogo'] = $row['szLogo'];	
                            $wareHouseAry[$row['id']]['szCountry'] = $row['szCountryName'];		
                            $wareHouseAry[$row['id']]['szCity'] = $row['szCity'];	
                            $wareHouseAry[$row['id']]['idForwarder'] = $row['idForwarder'];	
                            $wareHouseAry[$row['id']]['szDisplayName'] = $row['szDisplayName'];	
                            $wareHouseAry[$row['id']]['szUTCOffset'] = $row['szUTCOffset'];	
                            $wareHouseAry[$row['id']]['idCountry'] = $row['idCountry'];	
                            $wareHouseAry[$row['id']]['szPostCode'] = $row['szPostCode'];	
                            $wareHouseAry[$row['id']]['szCountryISO'] = $row['szCountryISO'];	
                            $wareHouseAry[$row['id']]['szEmail'] = $row['szEmail'];	
                            $wareHouseAry[$row['id']]['szContactPerson'] = $row['szContactPerson']; 
                            $wareHouseAry[$row['id']]['id'] = $row['id'];
                            $wareHouseAry[$row['id']]['szLatitude'] = $row['szLatitude'];	
                            $wareHouseAry[$row['id']]['szLongitude'] = $row['szLongitude'];
                            $wareHouseAry[$row['id']]['szForwarderCurrency'] = $row['szCurrency'];	
                            $wareHouseAry[$row['id']]['fReferalFee'] = $row['fReferalFee'];
                            $wareHouseAry[$row['id']]['dtReferealFeeValid'] = $row['dtReferealFeeValid'];

                            $forwarderCurrencyAry = array();
                            if($row['szCurrency']==1)
                            {
                                $wareHouseAry[$row['id']]['szForwarderCurrencyName'] = 'USD';
                                $wareHouseAry[$row['id']]['fForwarderCurrencyExchangeRate'] = 1.00;
                            }
                            else
                            {
                                if(!empty($alreadyUsedCurrency[$row['szCurrency']]))
                                {
                                    $forwarderCurrencyAry = $alreadyUsedCurrency[$row['szCurrency']];
                                }
                                else
                                {
                                    $forwarderCurrencyAry = $this->getCurrencyDetails($row['szCurrency']);		
                                    $alreadyUsedCurrency[$row['szCurrency']] = $forwarderCurrencyAry ;
                                } 
                                if(!empty($forwarderCurrencyAry))
                                {					
                                    $wareHouseAry[$row['id']]['szForwarderCurrencyName'] = $forwarderCurrencyAry['szCurrency'];
                                    $wareHouseAry[$row['id']]['fForwarderCurrencyExchangeRate'] = $forwarderCurrencyAry['fUsdValue'];
                                }
                            }	
                            $ctr++;
                        } 
                    }

                    if(!empty($wareHouseAry))
                    {
                        $idCountry = $distanceMeasureAry['idCountry']; 
                        $ret_ary = array();
                        $globalCountryServicedAry = array();
                        $idServiceType = $distanceMeasureAry['idServiceType'];

                        foreach($wareHouseAry as $key=>$wareHouseArys)
                        {
                            $countryServicedAry = array();
                            $idWhsCountry = $wareHouseAry[$key]['idCountry'];

                            /*
                             * by default we are taking warehouse country as a serviced by WHS
                            if($idCountry == $idWhsCountry)
                            {
                                    $ret_ary[$key] = $wareHouseArys ;
                            }
                            else
                            {
                            */
                                if(!empty($globalCountryServicedAry) && array_key_exists($key,$globalCountryServicedAry))
                                {
                                    $countryServicedAry = $globalCountryServicedAry[$key];
                                    if(!empty($countryServicedAry) && in_array($idCountry,$countryServicedAry))
                                    {
                                        $ret_ary[$key] = $wareHouseArys ;
                                    }
                                }
                                else
                                {
                                    $countryServicedAry = $this->getAllCountryServiceByWHS($key);
                                    $haulageCountryServicedAry = array();
                                    if($iDirection==1 && ($idServiceType==__SERVICE_TYPE_DTP__ || $idServiceType==__SERVICE_TYPE_DTW__ || $idServiceType==__SERVICE_TYPE_DTD__))
                                    {								
                                        $haulageCountryServicedAry = $this->getAllHaulageCountryByCFS($key,$iDirection);
                                    }
                                    else if($iDirection==2 && ($idServiceType==__SERVICE_TYPE_PTD__ || $idServiceType==__SERVICE_TYPE_WTD__ || $idServiceType==__SERVICE_TYPE_DTD__))
                                    {								
                                        $haulageCountryServicedAry = $this->getAllHaulageCountryByCFS($key,$iDirection);
                                    }

                                    if(!empty($haulageCountryServicedAry))
                                    {
                                        $countryServicedAry = array_merge($countryServicedAry,$haulageCountryServicedAry);
                                    }
                                    $globalCountryServicedAry[$key] = $countryServicedAry ;	

                                    if(!empty($countryServicedAry) && in_array($idCountry,$countryServicedAry))
                                    {
                                        $ret_ary[$key] = $wareHouseArys ;
                                    }
                                }
                            //}
                        }
                    }	
                    return $ret_ary;
		}
		else
		{
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
		} 
            } 
	} 
        
        function get_search_result($data,$cargoAry,$mode=false,$idBooking=false)
	{ 
		if(!empty($data))
		{  
                    $postSearchAry = $data ;
                    if(trim($data['szOriginCity'])==t($this->t_base_homepage.'fields/type_name'))
                    {
                        $data['szOriginCity'] = '';
                    }
                    if(trim($data['szOriginPostCode'])==t($this->t_base_homepage.'fields/optional') || trim($data['szOriginPostCode'])==t($this->t_base_homepage.'fields/type_code'))
                    {
                        $data['szOriginPostCode'] = '';
                    }			

                    if(trim($data['szDestinationCity'])==t($this->t_base_homepage.'fields/type_name'))
                    {
                        $data['szDestinationCity'] = '';
                    }
                    if(trim($data['szDestinationPostCode'])==t($this->t_base_homepage.'fields/optional') || trim($data['szDestinationPostCode'])==t($this->t_base_homepage.'fields/type_code'))
                    {
                        $data['szDestinationPostCode'] = '';
                    }
                    $kConfig  = new cConfig();
                    if(empty($data['szOriginPostCode']))
                    {
                        $postcodeCheckAry=array();
                        $postcodeResultAry = array();

                        $postcodeCheckAry['idCountry'] = $data['idOriginCountry'] ;
                        $postcodeCheckAry['szLatitute'] = $data['fOriginLatitude'] ;
                        $postcodeCheckAry['szLongitute'] = $data['fOriginLongitude'] ;

                        $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry);

                        if(!empty($postcodeResultAry))
                        {
                            $data['szOriginPostCode'] = $postcodeResultAry['szPostCode']; 
                        }
                    }
                    if(empty($data['szDestinationPostCode']))
                    {
                        $postcodeCheckAry=array();
                        $postcodeResultAry = array();

                        $postcodeCheckAry['idCountry'] = $data['idDestinationCountry'] ;
                        $postcodeCheckAry['szLatitute'] = $data['fDestinationLatitude'] ;
                        $postcodeCheckAry['szLongitute'] = $data['fDestinationLongitude'] ;

                        $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry);

                        if(!empty($postcodeResultAry))
                        {
                            $data['szDestinationPostCode'] = $postcodeResultAry['szPostCode']; 
                        }
                    }
                    
                    $szBookingRandomNum = $data['szBookingRandomNum'];
                    if($mode=='RECALCULATE_PRICING' || $mode=='FORWARDER_TRY_IT_OUT' || $mode=='MANAGEMENT_TRY_IT_OUT')
                    {
                        $kConfig = new cConfig();

                        if(empty($data['fOriginLatitude']) || empty($data['fOriginLongitude']))
                        {
                            $originPotcodeAry = array();
                            $originPotcodeAry = $kConfig->getPostCodeDetails_requirement($data['idOriginCountry'],$data['szOriginCity'],$data['szOriginPostCode']);
                            if(!empty($originPotcodeAry))
                            {
                                $data['fOriginLatitude'] = $originPotcodeAry['szLatitude'];
                                $data['fOriginLongitude'] = $originPotcodeAry['szLongitude'];
                            }
                        }

                        if(empty($data['fDestinationLatitude']) || empty($data['fDestinationLongitude']))
                        {
                            $destinationPotcodeAry= array();
                            $destinationPotcodeAry = $kConfig->getPostCodeDetails_requirement($data['idDestinationCountry'],$data['szDestinationCity'],$data['szDestinationPostCode']);
                            if(!empty($destinationPotcodeAry))
                            {
                                    $data['fDestinationLatitude'] = $destinationPotcodeAry['szLatitude'];
                                    $data['fDestinationLongitude'] = $destinationPotcodeAry['szLongitude'];
                            }
                        }
                        /*
                        $filename = __APP_PATH_ROOT__."/logs/recalculate_price.log";

                        $strdata=array();
                        $strdata[0]=" \n\n config object ".print_R($kConfig,true)." return array  ".print_R($destinationPotcodeAry,true)."\n\n";
                        file_log(true, $strdata, $filename);
                        */
                    }
			
                    $wareHouseFromAry = array();
                    $wareHouseToAry = array(); 
                    if($mode=='RECALCULATE_PRICING')
                    {
                            /*
                            $filename = __APP_PATH_ROOT__."/logs/recalculate_price.log";				
                            $strdata=array();
                            $strdata[0]=" \n\n *** Starting search for Recalculate pricing \n\n";
                            file_log(true, $strdata, $filename);
                            */
                            $wareHouseFromAry = $this->get_warehouse_distance_search($data,'FROM',$data['idWarehouseFrom']);
                            $wareHouseToAry = $this->get_warehouse_distance_search($data,'TO',$data['idWarehouseTo']);
                            $query_where = " AND wtw.id = '".(int)$data['idPricingWtw']."'";
			}
			if($mode=='MANAGEMENT_TRY_IT_OUT')
			{	
				$filename = __APP_PATH_ROOT__."/logs/debug.log";				
				$wareHouseFromAry = $this->get_warehouse_distance_search($data,'FROM',false,false);
				$wareHouseToAry = $this->get_warehouse_distance_search($data,'TO',false,false);
			}
			if($mode=='FORWARDER_TRY_IT_OUT')
			{ 
				$filename = __APP_PATH_ROOT__."/logs/debug.log";				
				$wareHouseFromAry = $this->get_warehouse_distance_search($data,'FROM',false,$data['forwarder_id']);
				$wareHouseToAry = $this->get_warehouse_distance_search($data,'TO',false,$data['forwarder_id']);
			}
			else
			{
				$filename = __APP_PATH_ROOT__."/logs/debug.log";
				
				$wareHouseFromAry = $this->get_warehouse_distance_search($data,'FROM');
				$wareHouseToAry = $this->get_warehouse_distance_search($data,'TO');
			}
			
		//	print_r($wareHouseFromAry);
			//echo "<br/><br/>";
			//print_r($wareHouseToAry);
			/*
				$strdata=array();
				$strdata[0]=" \n\n Search criteria \n\n";
				$strdata[1] = print_r($data,true)."\n\n cargo details \n\n".print_r($cargoAry,true)." \n\n mode ".$mode." booking id: ".$idBooking;
				file_log(true, $strdata, $filename);
				
			
				$strdata=array();
				$strdata[0]=" \n\n ware house from \n\n";
				$strdata[1] = print_r($wareHouseFromAry,true)."\n\n Ware house to \n\n".print_r($wareHouseToAry,true);
				file_log(true, $strdata, $filename);
		
			echo "<br><br> from <br><br>";
			print_r($wareHouseFromAry);
			echo "<br><br> to <br><br>";
			print_r($wareHouseToAry);
			die;
*/	
		
			if(!empty($wareHouseToAry) && !empty($wareHouseFromAry))
			{ 
                            $resultAry=array();
                            $ctr=0;
                            if(!empty($data['dtTimingDate']))
                            {
                                $date_str = $data['dtTimingDate'] ;
                                if($data['idTimingType']==1) // ready at origin
                                {
                                    $curr_date_time = time();
                                    $date_str_time = strtotime($date_str);

                                    if($curr_date_time > $date_str_time)
                                    {
                                        $date_str = date('Y-m-d H:i:s');
                                    }
                                }
                            }

                            if($data['idTimingType']==1) // ready at origin
                            {				
                                    //$query_where = " AND DATE_ADD(now() , INTERVAL(7 - (wtw.iBookingCutOffHours/24) ) DAY) > now()" ;
                                    //$query_select .= " DATE_SUB( (DATE_ADD( '".$date_str."' , INTERVAL(7 - wtw.idCutOffDay ) DAY)) , INTERVAL wtw.iBookingCutOffHours/24 DAY) as dtCutOffDate " ;
                                    //$query_where = " AND DATE_SUB( (DATE_ADD( '".$date_str."' , INTERVAL(7 - wtw.idCutOffDay ) DAY)) , INTERVAL wtw.iBookingCutOffHours/24 DAY) > '".mysql_escape_custom($date_str)."'";

                                    $query_select1 = " , 
                                            ( SELECT szWeekDay FROM ".__DBC_SCHEMATA_WEEK_DAYS__." wd WHERE wd.id = wtw.idCutOffDay ) szWeekDay,
                                            ( SELECT szWeekDay FROM ".__DBC_SCHEMATA_WEEK_DAYS__." wd WHERE wd.id = wtw.idAvailableDay ) szAvailableDay
                                    " ;
							
				}
				else if($data['idTimingType']==2) // Available at destination
				{
                                    //$query_where = " AND DATE_ADD(now() , INTERVAL(7 - ((wtw.iBookingCutOffHours+wtw.iTransitHours)/24) ) DAY) > now()" ;
                                    //$query_where = " AND DATE_SUB( (DATE_ADD( '".$date_str."' , INTERVAL(7 - wtw.idAvailableDay ) DAY)) , INTERVAL wtw.iTransitDay/24 DAY) > '".mysql_escape_custom($date_str)."'"; 
                                    $query_select1 = " ,
                                        ( SELECT szWeekDay FROM ".__DBC_SCHEMATA_WEEK_DAYS__." wd WHERE wd.id = wtw.idCutOffDay ) szWeekDay,
                                        ( SELECT szWeekDay FROM ".__DBC_SCHEMATA_WEEK_DAYS__." wd WHERE wd.id = wtw.idAvailableDay ) szAvailableDay
                                    ";
				}			
				//print_r($cargoAry);
				$counter_cargo = count($cargoAry);
				$totalVolume=0;
				
				$fTotalVolume = $data['fCargoVolume'];
				$fTotalWeight = $data['fCargoWeight']/1000 ;  // convrting weight from kg to meteric ton (mt)
				
				// we are calculating price on MAX('total weight','total volume')
				if($fTotalWeight > $fTotalVolume)
				{
                                    $fPriceFactor = ceil($fTotalWeight) ;
				}
				else
				{
                                    $fPriceFactor = ceil($fTotalVolume) ;
				} 
				if($data['iOriginCC']==1 || $data['iDestinationCC']==2)
				{
                                    $quryCC_having = " HAVING idWTW>0 " ;
                                    $queryCC = '';
                                    if($data['iOriginCC']==1)  // OriginCustomClearance
                                    {
//                                        $queryCC="
//                                            ,(
//                                                    SELECT
//                                                            count(cc.id)
//                                                    FROM
//                                                            ".__DBC_SCHEMATA_PRICING_CC__."	cc
//                                                    WHERE
//                                                            cc.idWarehouseFrom = wtw.idWarehouseFrom
//                                                    AND
//                                                            cc.idWarehouseTo = wtw.idWarehouseTo 
//                                                    AND
//                                                            cc.szOriginDestination = '1'
//                                                    AND
//                                                       cc.iActive = '1'
//                                            ) countOriginCC				
//                                        ";
//                                        $quryCC_having .= " AND countOriginCC > 0 " ;
                                    }
                                    if($data['iDestinationCC']==2)  // DestinationCustomClearance
                                    {
                                        $queryCC .="
                                            ,(
                                                    SELECT
                                                            count(cc.id)
                                                    FROM
                                                            ".__DBC_SCHEMATA_PRICING_CC__."	cc
                                                    WHERE
                                                            cc.idWarehouseFrom = wtw.idWarehouseFrom
                                                    AND
                                                            cc.idWarehouseTo = wtw.idWarehouseTo 
                                                    AND
                                                            cc.szOriginDestination = '2'
                                               AND
                                                       cc.iActive = '1'
                                            ) countDestinationCC				
                                        ";
                                        $quryCC_having .= " AND countDestinationCC > 0 " ;
                                    }
				}
				//echo "sadnasdfas";
				foreach($wareHouseFromAry as $idWHSFrom=>$iDistanceWHSFrom)
				{
					foreach($wareHouseToAry as $idWHSTo=>$iDistanceWHSTo)
					{
						if($idWHSTo!=$idWHSFrom)
						{
						
						//echo "sadnasdfas2121";
						$query="
							SELECT
								wtw.id idWTW,
								wtw.idWarehouseFrom,
								wtw.idWarehouseTo,
								wtw.iBookingCutOffHours,
								wtw.idCutOffDay,
								wtw.szCutOffLocalTime,
								wtw.idAvailableDay,
								wtw.szAvailableLocalTime,
								wtw.iTransitHours,
								wtw.idFrequency,
								wtw.fOriginChargeRateWM as fOriginRateWM,
								wtw.fOriginChargeMinRateWM as fOriginMinRateWM,
								wtw.fOriginChargeBookingRate as fOriginRate,
								wtw.szOriginChargeCurrency as szOriginRateCurrency,					
								wtw.fDestinationChargeRateWM as fDestinationRateWM,
								wtw.fDestinationChargeMinRateWM as fDestinationMinRateWM,
								wtw.fDestinationChargeBookingRate as fDestinationRate,
								wtw.szDestinationChargeCurrency as szDestinationRateCurrency,					
								wtw.fFreightRateWM as fRateWM,
								wtw.fFreightMinRateWM as fMinRateWM,
								wtw.fFreightBookingRate as fRate,
								wtw.dtValidFrom,
								wtw.dtExpiry,
								wtw.szFreightCurrency,
								wtw.iOriginChargesApplicable,
								wtw.iDestinationChargesApplicable,	
								wtw.iTransitDays,		
								c.szCurrency szFreightCurrencyName,					
								( SELECT iDays FROM ".__DBC_SCHEMATA_FREQUENCY__." freq WHERE freq.id = wtw.idFrequency ) iFrequency
								".$query_select." 
								$query_select1
								$queryCC
							FROM				
								".__DBC_SCHEMATA_WAREHOUSES_PRICING__." wtw
							INNER JOIN
								".__DBC_SCHEMATA_CURRENCY__." c
							ON
								wtw.szFreightCurrency = c.id		
							WHERE
								wtw.idWarehouseTo ='".mysql_escape_custom($idWHSTo)."'	
							AND
								wtw.idWarehouseFrom ='".mysql_escape_custom($idWHSFrom)."'
							AND
								wtw.iActive = '1'	
							".$query_where."
							".$quryCC_having."
						";
						//echo "<br>".$query."<br>" ;
						//die; 
						if($result=$this->exeSQL($query))
						{
							if($this->iNumRows>0)
							{	
								//echo "<br>".$query."<br>" ;
								//echo "<br> success <br> ";
								/*
								// logging data to log file for testing purpose 
								$filename = __APP_PATH_ROOT__."/logs/recalculate_price.log";
								
								$strdata=array();
								$strdata[0]=" \n\n found record for whs from ".$idWHSFrom." to whs to ".$idWHSTo."\n\n";
								file_log(true, $strdata, $filename);	
								*/
								while($row=$this->getAssoc($result))
								{
									$fCustomAmount = 0;	
									/**
									* Converting transit days into hours
									**/
									$fTransitHours = 0;
									$fTransitHours = $row['iTransitHours'];
									$fTransiDays_hour = ($row['iTransitDays']*24); //This is the actual Transit Days
									$fTruckingRateTo=0;
									$fTruckingRateFrom = 0;
									$fTotalStandardTruckingRate = 0;
									$fTotalHaulagePrice = 0;	
									$fTotalTransitHours = 0;
									$dtTransportTime_ms = 0;
									$dtTotalTransportDays = 0;
									$iBookingCutOffHours = 0;
									/**
									 * calculating Origin port freight price
									*/	
									if($row['szOriginRateCurrency']==1)  // USD
									{
                                                                            $fOriginPrice = $row['fOriginRateWM'] * $fPriceFactor ;
                                                                            $fOriginBookingRate = $row['fOriginRate'];
                                                                            $fOriginMinRateWM = $row['fOriginMinRateWM'];
                                                                            $fOriginFreightExchangeRate = 1.00 ;
                                                                            $szOriginPortCurrency = 'USD';
                                                                            if($fOriginPrice < $fOriginMinRateWM)
                                                                            {
                                                                                $fOriginPrice = $row['fOriginMinRateWM'];								
                                                                            }
                                                                            $fTotalOriginPortPrice = $fOriginPrice + $fOriginBookingRate ;	
									}
									else
									{
                                                                            /**
                                                                            * getting USD factor
                                                                            */  
                                                                            $currencyAry = array();
                                                                            $currencyAry = $this->getCurrencyDetails($row['szOriginRateCurrency']);

                                                                            $fUSDValue = $currencyAry['fUsdValue'];
                                                                            $szOriginPortCurrency = $currencyAry['szCurrency'];
                                                                            /**
                                                                            * converting price to USD
                                                                            */	

                                                                            $fOriginPrice = $row['fOriginRateWM'] * $fPriceFactor ;
                                                                            $fOriginBookingRate = $row['fOriginRate'];
                                                                            $fOriginMinRateWM = $row['fOriginMinRateWM'];

                                                                            if($fOriginPrice < $fOriginMinRateWM)
                                                                            {
                                                                                $fOriginPrice = $row['fOriginMinRateWM'];								
                                                                            }		

                                                                            $fOriginPrice = $fOriginPrice * $fUSDValue ;
                                                                            $fOriginBookingRate = $row['fOriginRate'] * $fUSDValue;
                                                                            $fOriginMinRateWM = $row['fOriginMinRateWM'] * $fUSDValue ;
                                                                            $fOriginFreightExchangeRate = $fUSDValue;

                                                                            $fTotalOriginPortPrice = $fOriginPrice + $fOriginBookingRate ;
									}
									/**
									 * calculating Destination port freight price
									*/
									if($row['szDestinationRateCurrency']==1)  // USD
									{
                                                                            $fDestinationPrice = $row['fDestinationRateWM'] * $fPriceFactor ;
                                                                            $fDestinationBookingRate = $row['fDestinationRate'];
                                                                            $fDestinationMinRateWM = $row['fDestinationMinRateWM'];
                                                                            $fDestinationFreightExchangeRate = 1 ; 	
                                                                            $szDestinationPortCurrency = 'USD';
                                                                            if($fDestinationPrice < $fDestinationMinRateWM)
                                                                            {
                                                                                $fDestinationPrice = $row['fDestinationMinRateWM'];								
                                                                            }
                                                                            $fTotalDestinationPortPrice = $fDestinationPrice + $fDestinationBookingRate ;
									}
									else
									{
                                                                            /**
                                                                            * getting USD factor
                                                                            */  
                                                                            $currencyAry = array();
                                                                            $currencyAry = $this->getCurrencyDetails($row['szDestinationRateCurrency']);

                                                                            $fUSDValue = $currencyAry['fUsdValue'];
                                                                            $szDestinationPortCurrency = $currencyAry['szCurrency'];
                                                                            /**
                                                                            * converting price to USD
                                                                            */	

                                                                            $fDestinationPrice = $row['fDestinationRateWM'] * $fPriceFactor ;
                                                                            $fDestinationBookingRate = $row['fDestinationRate'];
                                                                            $fDestinationMinRateWM = $row['fDestinationMinRateWM'];

                                                                            if($fDestinationPrice < $fDestinationMinRateWM)
                                                                            {
                                                                                $fDestinationPrice = $row['fDestinationMinRateWM'];								
                                                                            }		

                                                                            $fDestinationPrice = $fDestinationPrice * $fUSDValue ;
                                                                            $fDestinationBookingRate = $row['fDestinationRate'] * $fUSDValue;
                                                                            $fDestinationMinRateWM = $row['fDestinationMinRateWM'] * $fUSDValue ;
                                                                            $fDestinationFreightExchangeRate = $fUSDValue ; 	

                                                                            $fTotalDestinationPortPrice = $fDestinationPrice + $fDestinationBookingRate ;										
										
									}
									
									$fTotalPortPrice = 0 ;
									if($data['idServiceType']==__SERVICE_TYPE_DTD__) //DTD
									{
                                                                            if($row['iDestinationChargesApplicable']!=1 || $row['iOriginChargesApplicable']!=1)
                                                                            {
                                                                                continue;
                                                                            }
                                                                            /**
                                                                            * For DTD Haulage price is chargable for both Origin and Destination
                                                                            */  

                                                                            /**
                                                                            * calculating the Haulage price at Origin in USD
                                                                            **/

                                                                            $fTotalPortPrice = $fTotalOriginPortPrice + $fTotalDestinationPortPrice;									

                                                                            $houlageData=array();
                                                                            $fFromWareHouseHaulageAry = array();
                                                                            $houlageData['idWarehouse'] = $idWHSFrom ;
                                                                            $houlageData['iDistance'] = $iDistanceWHSFrom['iDistance'];
                                                                            $houlageData['iDirection'] = 1; // origin haulage		

                                                                            $houlageData['szLatitude'] = $data['fOriginLatitude'] ;
                                                                            $houlageData['szLongitude'] = $data['fOriginLongitude'] ;

                                                                            if(!empty($data['szOriginPostCode']))
                                                                            {
                                                                                    $szHaulagePostCode = $data['szOriginPostCode'] ;
                                                                            }
                                                                            else
                                                                            {
                                                                                    $szHaulagePostCode = $data['szOriginCity'] ;
                                                                            }

                                                                            $houlageData['fTotalWeight'] = $data['fCargoWeight'];
                                                                            $houlageData['fVolume'] = $data['fCargoVolume'];
                                                                            $houlageData['szPostcode'] = $szHaulagePostCode;
                                                                            $houlageData['idCountry'] = $data['idOriginCountry'];		

                                                                            $kHaulagePricing = new cHaulagePricing();
                                                                            $fFromWareHouseHaulageAry = $kHaulagePricing->getHaulageDetails($houlageData);

                                                                            $houlageData=array();
                                                                            $fToWareHouseHaulageAry = array();
                                                                            $houlageData['idWarehouse'] = $idWHSTo ;
                                                                            $houlageData['iDistance'] = $iDistanceWHSTo['iDistance'];
                                                                            $houlageData['iDirection'] = 2; // origin haulage		

                                                                            $houlageData['szLatitude'] = $data['fDestinationLatitude'] ;
                                                                            $houlageData['szLongitude'] = $data['fDestinationLongitude'];

                                                                            if(!empty($data['szDestinationPostCode']))
                                                                            {
                                                                                    $szHaulagePostCode = $data['szDestinationPostCode'] ;
                                                                            }
                                                                            else
                                                                            {
                                                                                    $szHaulagePostCode = $data['szDestinationCity'] ;
                                                                            }

                                                                            $houlageData['fTotalWeight'] = $data['fCargoWeight'];
                                                                            $houlageData['fVolume'] = $data['fCargoVolume'];
                                                                            $houlageData['szPostcode'] = $szHaulagePostCode;
                                                                            $houlageData['idCountry'] = $data['idDestinationCountry'];

                                                                            $kHaulagePricing = new cHaulagePricing();
                                                                            $fToWareHouseHaulageAry = $kHaulagePricing->getHaulageDetails($houlageData);

                                                                            //echo " <br><br>resulted data <br>";
                                                                            //print_r($fToWareHouseHaulageAry);
                                                                            $fTotalHaulagePrice = $fFromWareHouseHaulageAry['fTotalHaulgePrice'] + $fToWareHouseHaulageAry['fTotalHaulgePrice'];

                                                                            /**
                                                                            * Adding haulage transit time from Origing and Destination with wtw transit time 
                                                                            **/ 
                                                                            if($data['idTimingType']==1) //Ready at origin  DTD
                                                                            {
                                                                                    $fTotalTransitHours = $fTransitHours + $fToWareHouseHaulageAry['iHaulageTransitTime'] + $fFromWareHouseHaulageAry['iHaulageTransitTime'];
                                                                                    $fTotalTransitDays_Hours = $fTransiDays_hour + $fToWareHouseHaulageAry['iHaulageTransitTime'] + $fFromWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                                                    $iBookingCutOffHours = (($row['iBookingCutOffHours'] + $fFromWareHouseHaulageAry['iHaulageTransitTime'])*60*60);											
                                                                            }
                                                                            else
                                                                            {
                                                                                    $fTotalTransitHours = $fTransitHours + $fToWareHouseHaulageAry['iHaulageTransitTime'] + $fFromWareHouseHaulageAry['iHaulageTransitTime'];
                                                                                    $fTotalTransitDays_Hours = $fTransiDays_hour + $fToWareHouseHaulageAry['iHaulageTransitTime'] + $fFromWareHouseHaulageAry['iHaulageTransitTime'];
                                                                                    $iBookingCutOffHours = (($row['iBookingCutOffHours'] + $fToWareHouseHaulageAry['iHaulageTransitTime'])*60*60);
                                                                            }
									}																	
									else if($data['idServiceType']==(int)__SERVICE_TYPE_DTW__) //DTW
									{
                                                                            if($row['iDestinationChargesApplicable']!=1 || $row['iOriginChargesApplicable']!=1)
                                                                            {
                                                                                    continue;
                                                                            }

                                                                            /**
                                                                            * for DTW Standard Trucking rate is applicable at Destination and Haulage Price is applicable at Origin
                                                                            */ 
                                                                            $fTotalPortPrice = $fTotalOriginPortPrice + $fTotalDestinationPortPrice;

                                                                            /**
                                                                            * calculating the Haulage price at Origin in USD
                                                                            **/ 

                                                                            $houlageData=array();
                                                                            $fFromWareHouseHaulageAry = array();
                                                                            $houlageData['idWarehouse'] = $idWHSFrom ;
                                                                            $houlageData['iDistance'] = $iDistanceWHSFrom['iDistance'];
                                                                            $houlageData['iDirection'] = 1; // origin haulage		

                                                                            $houlageData['szLatitude'] = $data['fOriginLatitude'] ;
                                                                            $houlageData['szLongitude'] = $data['fOriginLongitude'] ;

                                                                            if(!empty($data['szOriginPostCode']))
                                                                            {
                                                                                $szHaulagePostCode = $data['szOriginPostCode'] ;
                                                                            }
                                                                            else
                                                                            {
                                                                                $szHaulagePostCode = $data['szOriginCity'] ;
                                                                            }

                                                                            $houlageData['fTotalWeight'] = $data['fCargoWeight'];
                                                                            $houlageData['fVolume'] = $data['fCargoVolume'];
                                                                            $houlageData['szPostcode'] = $szHaulagePostCode;
                                                                            $houlageData['idCountry'] = $data['idOriginCountry'];		

                                                                            $kHaulagePricing = new cHaulagePricing();
                                                                            $fFromWareHouseHaulageAry = $kHaulagePricing->getHaulageDetails($houlageData);

                                                                            $fTotalHaulagePrice = $fFromWareHouseHaulageAry['fTotalHaulgePrice'] ;	
                                                                            $fTotalTransitHours = $fTransitHours + $fFromWareHouseHaulageAry['iHaulageTransitTime'];
                                                                            $fTotalTransitDays_Hours = $fTransiDays_hour + $fFromWareHouseHaulageAry['iHaulageTransitTime'];
                                                                            /**
                                                                             * calculating standard trucking rate at Destination in USD
                                                                            */	
                                                                            $fTruckingRateTo = $iDistanceWHSTo['iDistance'] * $iDistanceWHSTo['fStandardTruckRate'];
                                                                            $fTotalStandardTruckingRate = $fTruckingRateTo ;

                                                                            $iBookingCutOffHours = (($row['iBookingCutOffHours']+$fFromWareHouseHaulageAry['iHaulageTransitTime'])*60*60);

                                                                            $iBookingCutOffHours_available = (($row['iBookingCutOffHours']+$fFromWareHouseHaulageAry['iHaulageTransitTime'])*60*60);


                                                                            /**
                                                                            * Adding haulage transit time from Origing and Destination with wtw transit time 
                                                                            **/ 
                                                                            if($data['idTimingType']==1) //Ready at origin  DTW
                                                                            {
                                                                                $fTotalTransitHours = $fTransitHours + $fFromWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                                                $fTotalTransitDays_Hours = $fTransiDays_hour + $fFromWareHouseHaulageAry['iHaulageTransitTime'] ;											
                                                                                $iBookingCutOffHours = (($row['iBookingCutOffHours']+$fFromWareHouseHaulageAry['iHaulageTransitTime'])*60*60);										
                                                                            }
                                                                            else
                                                                            {
                                                                                $fTotalTransitHours = $fTransitHours + $fFromWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                                                $fTotalTransitDays_Hours = $fTransiDays_hour + $fFromWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                                                $iBookingCutOffHours = (($row['iBookingCutOffHours']*60*60));
                                                                            }
									}								
									elseif($data['idServiceType']==(int)__SERVICE_TYPE_WTD__) //WTD
									{
                                                                            if($row['iDestinationChargesApplicable']!=1 || $row['iOriginChargesApplicable']!=1)
                                                                            {
                                                                                    continue;
                                                                            }
                                                                            /**
                                                                            * for WTD Standard Trucking rate is applicable at Origin and Haulage Price is applicable at Destination
                                                                            */  
                                                                            $fTotalPortPrice = $fTotalOriginPortPrice + $fTotalDestinationPortPrice;

                                                                            /**
                                                                             * calculating standard trucking rate at Origin in USD
                                                                            */										

                                                                            $fTruckingRateFrom = $iDistanceWHSFrom['iDistance'] * $iDistanceWHSFrom['fStandardTruckRate'];
                                                                            $fTotalStandardTruckingRate = $fTruckingRateFrom ;	

                                                                            /**
                                                                            * calculating To Haulage price at Destination in USD
                                                                            */

                                                                            $houlageData=array();
                                                                            $fFromWareHouseHaulageAry = array();
                                                                            $houlageData['idWarehouse'] = $idWHSTo ;
                                                                            $houlageData['iDistance'] = $iDistanceWHSTo['iDistance'];
                                                                            $houlageData['iDirection'] = 2; // origin haulage		

                                                                            $houlageData['szLatitude'] = $data['fDestinationLatitude'] ;
                                                                            $houlageData['szLongitude'] = $data['fDestinationLongitude'] ;

                                                                            if(!empty($data['szDestinationPostCode']))
                                                                            {
                                                                                    $szHaulagePostCode = $data['szDestinationPostCode'] ;
                                                                            }
                                                                            else
                                                                            {
                                                                                    $szHaulagePostCode = $data['szDestinationCity'] ;
                                                                            }

                                                                            $houlageData['fTotalWeight'] = $data['fCargoWeight'];
                                                                            $houlageData['fVolume'] = $data['fCargoVolume'];
                                                                            $houlageData['szPostcode'] = $szHaulagePostCode;
                                                                            $houlageData['idCountry'] = $data['idDestinationCountry'];

                                                                            $kHaulagePricing = new cHaulagePricing();
                                                                            $fToWareHouseHaulageAry = $kHaulagePricing->getHaulageDetails($houlageData);

                                                                            //$fToWareHouseHaulageAry = $this->getHaulageDetails($houlageData);

                                                                            $fTotalHaulagePrice = $fToWareHouseHaulageAry['fTotalHaulgePrice'];

                                                                            //$fTotalTransitHours = $fTransitHours + $fToWareHouseHaulageAry['iHaulageTransitTime'];

                                                                            /**
                                                                            * Adding haulage transit time from Origing and Destination with wtw transit time 
                                                                            **/ 
                                                                            if($data['idTimingType']==1) //Ready at origin  WTD
                                                                            {
                                                                                $fTotalTransitHours = $fTransitHours + $fToWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                                                $fTotalTransitDays_Hours = $fTransiDays_hour + $fToWareHouseHaulageAry['iHaulageTransitTime'] ;											
                                                                                $iBookingCutOffHours = ($row['iBookingCutOffHours']*60*60);											
                                                                            }
                                                                            else
                                                                            {
                                                                                $fTotalTransitHours = $fTransitHours + $fToWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                                                $fTotalTransitDays_Hours = $fTransiDays_hour + $fToWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                                                $iBookingCutOffHours = (($row['iBookingCutOffHours'] + $fToWareHouseHaulageAry['iHaulageTransitTime'])*60*60);
                                                                            }
									}								
									else if($data['idServiceType']==(int)__SERVICE_TYPE_WTW__) // WTW
									{
                                                                            if($row['iDestinationChargesApplicable']!=1 || $row['iOriginChargesApplicable']!=1)
                                                                            {
                                                                                continue;
                                                                            }
                                                                            /**
                                                                            * Haulage Price and Haulage transit time is not applicable for WTW 
                                                                            */ 
                                                                            $fTotalPortPrice = $fTotalOriginPortPrice + $fTotalDestinationPortPrice;

                                                                            $fTotalHaulagePrice = 0;

                                                                            /**
                                                                             * calculating standard trucking rate
                                                                            */
                                                                            $fTruckingRateTo = $iDistanceWHSTo['iDistance'] * $iDistanceWHSTo['fStandardTruckRate'];

                                                                            $fTruckingRateFrom = $iDistanceWHSFrom['iDistance'] * $iDistanceWHSFrom['fStandardTruckRate'];

                                                                            $fTotalStandardTruckingRate = $fTruckingRateFrom + $fTruckingRateTo ;

                                                                            $fTotalTransitHours = $fTransitHours ;
                                                                            $fTotalTransitDays_Hours = $fTransiDays_hour ;
                                                                            $iBookingCutOffHours = (($row['iBookingCutOffHours'])*60*60);
									}
									else if($data['idServiceType']==(int)__SERVICE_TYPE_DTP__) //DTP
									{			
                                                                            if($row['iOriginChargesApplicable']!=1)
                                                                            {
                                                                                    continue;
                                                                            }							
                                                                            /**
                                                                            * for DTP Haulage Price and origin port price is applicable at Origin 
                                                                            */ 

                                                                            $fTotalDestinationPortPrice = 0;
                                                                            $fDestinationFreightExchangeRate = 0;

                                                                            $fTotalPortPrice = $fTotalOriginPortPrice ;

                                                                            /**
                                                                            * calculating the Haulage price at Origin in USD
                                                                            **/ 

                                                                            $houlageData=array();
                                                                            $fFromWareHouseHaulageAry = array();
                                                                            $houlageData['idWarehouse'] = $idWHSFrom ;
                                                                            $houlageData['iDistance'] = $iDistanceWHSFrom['iDistance'];
                                                                            $houlageData['iDirection'] = 1; // origin haulage		

                                                                            $houlageData['szLatitude'] = $data['fOriginLatitude'] ;
                                                                            $houlageData['szLongitude'] = $data['fOriginLongitude'] ;

                                                                            if(!empty($data['szOriginPostCode']))
                                                                            {
                                                                                    $szHaulagePostCode = $data['szOriginPostCode'] ;
                                                                            }
                                                                            else
                                                                            {
                                                                                    $szHaulagePostCode = $data['szOriginCity'] ;
                                                                            }

                                                                            $houlageData['fTotalWeight'] = $data['fCargoWeight'];
                                                                            $houlageData['fVolume'] = $data['fCargoVolume'];
                                                                            $houlageData['szPostcode'] = $szHaulagePostCode;
                                                                            $houlageData['idCountry'] = $data['idOriginCountry'];		

                                                                            $kHaulagePricing = new cHaulagePricing();
                                                                            $fFromWareHouseHaulageAry = $kHaulagePricing->getHaulageDetails($houlageData);

                                                                            /**
                                                                             * calculating standard trucking rate at Destination in USD
                                                                            */	
                                                                            $fTruckingRateTo = $iDistanceWHSTo['iDistance'] * $iDistanceWHSTo['fStandardTruckRate'];
                                                                            $fTotalStandardTruckingRate = $fTruckingRateTo ;

                                                                            $fTotalHaulagePrice = $fFromWareHouseHaulageAry['fTotalHaulgePrice'] ;	
                                                                            $fTotalTransitHours = $fTransitHours + $fFromWareHouseHaulageAry['iHaulageTransitTime'];
                                                                            $fTotalTransitDays_Hours = $fTransiDays_hour + $fFromWareHouseHaulageAry['iHaulageTransitTime'];

                                                                            $iBookingCutOffHours = (($row['iBookingCutOffHours']+$fFromWareHouseHaulageAry['iHaulageTransitTime'])*60*60);

                                                                            $iBookingCutOffHours_available = (($row['iBookingCutOffHours']+$fFromWareHouseHaulageAry['iHaulageTransitTime'])*60*60);


                                                                            /**
                                                                            * Adding haulage transit time from Origing and Destination with wtw transit time 
                                                                            **/ 
                                                                            if($data['idTimingType']==1) //Ready at origin  DTW
                                                                            {
                                                                                $fTotalTransitHours = $fTransitHours + $fFromWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                                                $fTotalTransitDays_Hours = $fTransiDays_hour + $fFromWareHouseHaulageAry['iHaulageTransitTime'] ;											
                                                                                $iBookingCutOffHours = (($row['iBookingCutOffHours']+$fFromWareHouseHaulageAry['iHaulageTransitTime'])*60*60);										
                                                                            }
                                                                            else
                                                                            {
                                                                                $fTotalTransitHours = $fTransitHours + $fFromWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                                                $fTotalTransitDays_Hours = $fTransiDays_hour + $fFromWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                                                $iBookingCutOffHours = (($row['iBookingCutOffHours']*60*60));
                                                                            }
									}
									elseif($data['idServiceType']==(int)__SERVICE_TYPE_PTD__) //PTD
									{
                                                                            if($row['iDestinationChargesApplicable']!=1)
                                                                            {
                                                                                    continue;
                                                                            }
                                                                            /**
                                                                            * for WTD Standard Trucking rate is applicable at Origin and Haulage Price is applicable at Destination
                                                                            */  

                                                                            /**
                                                                             * calculating standard trucking rate at Origin in USD
                                                                             **/										

                                                                            $fTruckingRateFrom = $iDistanceWHSFrom['iDistance'] * $iDistanceWHSFrom['fStandardTruckRate'];
                                                                            $fTotalStandardTruckingRate = $fTruckingRateFrom ;

                                                                            $fTotalPortPrice = $fTotalDestinationPortPrice;
                                                                            $resultAry[$ctr]['fTotalDestinationPortPrice'] = $fTotalDestinationPortPrice;	
                                                                            $resultAry[$ctr]['fDestinationFreightExchangeRate'] = $fDestinationFreightExchangeRate;	

                                                                            /**
                                                                            * calculating To Haulage price at Destination in USD
                                                                            */

                                                                            $houlageData=array();
                                                                            $fFromWareHouseHaulageAry = array();
                                                                            $houlageData['idWarehouse'] = $idWHSTo ;
                                                                            $houlageData['iDistance'] = $iDistanceWHSTo['iDistance'];
                                                                            $houlageData['iDirection'] = 2; // origin haulage		

                                                                            $houlageData['szLatitude'] = $data['fDestinationLatitude'] ;
                                                                            $houlageData['szLongitude'] = $data['fDestinationLongitude'] ;

                                                                            if(!empty($data['szDestinationPostCode']))
                                                                            {
                                                                                    $szHaulagePostCode = $data['szDestinationPostCode'] ;
                                                                            }
                                                                            else
                                                                            {
                                                                                    $szHaulagePostCode = $data['szDestinationCity'] ;
                                                                            }

                                                                            $houlageData['fTotalWeight'] = $data['fCargoWeight'];
                                                                            $houlageData['fVolume'] = $data['fCargoVolume'];
                                                                            $houlageData['szPostcode'] = $szHaulagePostCode;
                                                                            $houlageData['idCountry'] = $data['idDestinationCountry'];

                                                                            $filename = __APP_PATH_ROOT__."/logs/debug.log";

//                                                                            $strdata=array();
//                                                                            $strdata[0]=" \n\n *** sending data to fetch warehaouses \n\n";
//                                                                            $strdata[1] = print_R($houlageData,true)."\n\n"; 
//                                                                            file_log(true, $strdata, $filename);
                                                                            
                                                                            $kHaulagePricing = new cHaulagePricing();
                                                                            $fToWareHouseHaulageAry = $kHaulagePricing->getHaulageDetails($houlageData);

//                                                                            $strdata=array();
//                                                                            $strdata[0]=" \n\n *** FOUND HAULAGE SERVICE \n\n";
//                                                                            $strdata[1] = print_R($fToWareHouseHaulageAry,true)."\n\n"; 
//                                                                            file_log(true, $strdata, $filename);

                                                                            $fTotalHaulagePrice = $fToWareHouseHaulageAry['fTotalHaulgePrice'];

                                                                            //$fTotalTransitHours = $fTransitHours + $fToWareHouseHaulageAry['iHaulageTransitTime'];

                                                                            /**
                                                                            * Adding haulage transit time from Origing and Destination with wtw transit time 
                                                                            **/ 
                                                                            if($data['idTimingType']==1) //Ready at origin  WTD
                                                                            {
                                                                                    $fTotalTransitHours = $fTransitHours + $fToWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                                                    $fTotalTransitDays_Hours = $fTransiDays_hour + $fToWareHouseHaulageAry['iHaulageTransitTime'] ;											
                                                                                    $iBookingCutOffHours = ($row['iBookingCutOffHours']*60*60);											
                                                                            }
                                                                            else
                                                                            {
                                                                                    $fTotalTransitHours = $fTransitHours + $fToWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                                                    $fTotalTransitDays_Hours = $fTransiDays_hour + $fToWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                                                    $iBookingCutOffHours = (($row['iBookingCutOffHours'] + $fToWareHouseHaulageAry['iHaulageTransitTime'])*60*60);
                                                                            }
									}
									elseif($data['idServiceType']==(int)__SERVICE_TYPE_WTP__) //WTP
									{
										if($row['iOriginChargesApplicable']!=1)
										{
                                                                                    continue;
										}
									
										/**
										* for WTD Standard Trucking rate is applicable at Origin and Haulage Price is applicable at Destination
										*/  
										$fTotalDestinationPortPrice = 0;
										$fDestinationFreightExchangeRate = 0;
										
										$fTotalPortPrice = $fTotalOriginPortPrice ;
																				
										/**
										 * calculating standard trucking rate at Origin in USD
										*/										
										
										$fTruckingRateFrom = $iDistanceWHSFrom['iDistance'] * $iDistanceWHSFrom['fStandardTruckRate'];
										
										/**
										 * calculating standard trucking rate at Destination in USD
										*/	
										$fTruckingRateTo = $iDistanceWHSTo['iDistance'] * $iDistanceWHSTo['fStandardTruckRate'];
										
										$fTotalStandardTruckingRate = $fTruckingRateFrom + $fTruckingRateTo ;
										
										
										//$fTotalTransitHours = $fTransitHours + $fToWareHouseHaulageAry['iHaulageTransitTime'];
										
										/**
										* Adding haulage transit time from Origing and Destination with wtw transit time 
										**/ 
										if($data['idTimingType']==1) //Ready at origin  WTD
										{
											$fTotalTransitHours = $fTransitHours ;
											$fTotalTransitDays_Hours = $fTransiDays_hour ;											
											$iBookingCutOffHours = ($row['iBookingCutOffHours']*60*60);											
										}
										else
										{
											$fTotalTransitHours = $fTransitHours ;
											$fTotalTransitDays_Hours = $fTransiDays_hour ;
											$iBookingCutOffHours = (($row['iBookingCutOffHours'])*60*60);
										}		
									}
									else if($data['idServiceType']==(int)__SERVICE_TYPE_PTW__) //PTW
									{
                                                                            if($row['iDestinationChargesApplicable']!=1)
                                                                            {
                                                                                    continue;
                                                                            }
                                                                            /**
                                                                            * for DTW Standard Trucking rate is applicable at Destination and Haulage Price is applicable at Origin
                                                                            */ 
                                                                            $fTotalOriginPortPrice = 0;
                                                                            $fOriginFreightExchangeRate = 0;

                                                                            $fTotalPortPrice = $fTotalDestinationPortPrice;

                                                                            $fTotalTransitHours = $fTransitHours ;
                                                                            $fTotalTransitDays_Hours = $fTransiDays_hour ;

                                                                            /**
                                                                             * calculating standard trucking rate at Origin in USD
                                                                             **/										

                                                                            $fTruckingRateFrom = $iDistanceWHSFrom['iDistance'] * $iDistanceWHSFrom['fStandardTruckRate'];

                                                                            /**
                                                                             * calculating standard trucking rate at Destination in USD
                                                                            */	
                                                                            $fTruckingRateTo = $iDistanceWHSTo['iDistance'] * $iDistanceWHSTo['fStandardTruckRate'];

                                                                            $fTotalStandardTruckingRate = $fTruckingRateFrom + $fTruckingRateTo ;

                                                                            $iBookingCutOffHours = (($row['iBookingCutOffHours'])*60*60);

                                                                            $iBookingCutOffHours_available = (($row['iBookingCutOffHours'])*60*60);


                                                                            /**
                                                                            * Adding haulage transit time from Origing and Destination with wtw transit time 
                                                                            **/ 
                                                                            if($data['idTimingType']==1) //Ready at origin  DTW
                                                                            {
                                                                                $fTotalTransitHours = $fTransitHours ;
                                                                                $fTotalTransitDays_Hours = $fTransiDays_hour ;											
                                                                                $iBookingCutOffHours = (($row['iBookingCutOffHours'] )*60*60);										
                                                                            }
                                                                            else
                                                                            {
                                                                                $fTotalTransitHours = $fTransitHours ;
                                                                                $fTotalTransitDays_Hours = $fTransiDays_hour;
                                                                                $iBookingCutOffHours = (($row['iBookingCutOffHours']*60*60));
                                                                            }
									}	
									else if($data['idServiceType']==(int)__SERVICE_TYPE_PTP__) // PTP
									{
										/**
										 * calculating standard trucking rate
										*/
										$fTruckingRateTo = $iDistanceWHSTo['iDistance'] * $iDistanceWHSTo['fStandardTruckRate'];
										
										$fTruckingRateFrom = $iDistanceWHSFrom['iDistance'] * $iDistanceWHSFrom['fStandardTruckRate'];
																				
										$fTotalStandardTruckingRate = $fTruckingRateFrom + $fTruckingRateTo ;
										
										$fTotalTransitHours = $fTransitHours ;
										$fTotalTransitDays_Hours = $fTransiDays_hour ;
										$iBookingCutOffHours = (($row['iBookingCutOffHours'])*60*60);
									}
									if((($data['idServiceType']==__SERVICE_TYPE_DTD__) && !empty($fToWareHouseHaulageAry) && !empty($fFromWareHouseHaulageAry)) || (($data['idServiceType']==__SERVICE_TYPE_DTW__ || $data['idServiceType']==__SERVICE_TYPE_DTP__ ) && !empty($fFromWareHouseHaulageAry)) || (($data['idServiceType']==__SERVICE_TYPE_WTD__ || $data['idServiceType']==__SERVICE_TYPE_PTD__ ) && !empty($fToWareHouseHaulageAry)) || ($data['idServiceType']==__SERVICE_TYPE_WTW__ || $data['idServiceType']==__SERVICE_TYPE_WTP__ || $data['idServiceType']==__SERVICE_TYPE_PTP__|| $data['idServiceType']==__SERVICE_TYPE_PTW__ ))
									{					
										$resultAry[$ctr]=$row;
										if(($data['idServiceType']==__SERVICE_TYPE_DTD__) || ($data['idServiceType']==__SERVICE_TYPE_DTW__) || ($data['idServiceType']==__SERVICE_TYPE_DTP__))
										{
											$resultAry[$ctr]['fOriginHaulageDistance'] = $iDistanceWHSFrom['iDistance'] ;
											$resultAry[$ctr]['fOriginHaulageTransitTime'] = $fFromWareHouseHaulageAry['iHaulageTransitTime'];
											$resultAry[$ctr]['fOriginHaulageRateWM_KM'] = $fFromWareHouseHaulageAry['fPricePer100Kg'];
											$resultAry[$ctr]['fOriginHaulageMinRateWM_KM'] = $fFromWareHouseHaulageAry['fMinimumPrice'];
											$resultAry[$ctr]['fOriginHaulageBookingRateWM_KM'] = $fFromWareHouseHaulageAry['fPricePerBooking'];
											$resultAry[$ctr]['fOriginHaulageExchangeRate'] = $fFromWareHouseHaulageAry['fExchangeRate'];
											$resultAry[$ctr]['fOriginHaulagePrice'] = $fFromWareHouseHaulageAry['fTotalHaulgePrice'];
											$resultAry[$ctr]['szOriginHaulageCurrency'] = $fFromWareHouseHaulageAry['szHaulageCurrency'];
											$resultAry[$ctr]['idOriginHaulageCurrency'] = $fFromWareHouseHaulageAry['idCurrency'];
											
											$resultAry[$ctr]['fOriginHaulageFuelSurcharge'] = $fFromWareHouseHaulageAry['fFuelSurcharge'];
											$resultAry[$ctr]['iOriginHaulageDistanceUptoKm'] = $fFromWareHouseHaulageAry['iDistanceUptoKm'];
											$resultAry[$ctr]['szOriginHaulageModelName'] = $fFromWareHouseHaulageAry['szModelName'];
											
											$resultAry[$ctr]['fOriginHaulageWmFactor'] = $fFromWareHouseHaulageAry['fWmFactor'];
											$resultAry[$ctr]['fOriginHaulageChargableWeight'] = $fFromWareHouseHaulageAry['fChargableWeight'];
											$resultAry[$ctr]['fOriginHaulageFuelPercentage'] = $fFromWareHouseHaulageAry['fFuelPercentage'];										
											
										}
										if(($data['idServiceType']==__SERVICE_TYPE_DTD__) || ($data['idServiceType']==__SERVICE_TYPE_WTD__) || ($data['idServiceType']==__SERVICE_TYPE_PTD__))
										{
											$resultAry[$ctr]['fDestinationHaulageDistance'] = $iDistanceWHSTo['iDistance'] ;
											$resultAry[$ctr]['fDestinationHaulageTransitTime'] = $fToWareHouseHaulageAry['iHaulageTransitTime'];
											$resultAry[$ctr]['fDestinationHaulageRateWM_KM'] = $fToWareHouseHaulageAry['fPricePer100Kg'];
											$resultAry[$ctr]['fDestinationHaulageMinRateWM_KM'] = $fToWareHouseHaulageAry['fMinimumPrice'];
											$resultAry[$ctr]['fDestinationHaulageBookingRateWM_KM'] = $fToWareHouseHaulageAry['fPricePerBooking'];
											$resultAry[$ctr]['fDestinationHaulageExchangeRate'] = $fToWareHouseHaulageAry['fExchangeRate'];
											$resultAry[$ctr]['fDestinationHaulagePrice'] = $fToWareHouseHaulageAry['fTotalHaulgePrice'];
											$resultAry[$ctr]['szDestinationHaulageCurrency'] = $fToWareHouseHaulageAry['szHaulageCurrency'];
											$resultAry[$ctr]['idDestinationHaulageCurrency'] = $fFromWareHouseHaulageAry['idCurrency'];
											$resultAry[$ctr]['fDestinationHaulageFuelSurcharge'] = $fToWareHouseHaulageAry['fFuelSurcharge'];
											$resultAry[$ctr]['iDestinationHaulageDistanceUptoKm'] = $fToWareHouseHaulageAry['iDistanceUptoKm'];
											$resultAry[$ctr]['szDestinationHaulageModelName'] = $fToWareHouseHaulageAry['szModelName'];
											
											$resultAry[$ctr]['fDestinationHaulageWmFactor'] = $fToWareHouseHaulageAry['fWmFactor'];
											$resultAry[$ctr]['fDestinationHaulageChargableWeight'] = $fToWareHouseHaulageAry['fChargableWeight'];
											$resultAry[$ctr]['fDestinationHaulageFuelPercentage'] = $fToWareHouseHaulageAry['fFuelPercentage'];	
										}
										
										if(($data['idServiceType']==__SERVICE_TYPE_WTW__) || ($data['idServiceType']==__SERVICE_TYPE_DTW__) || ($data['idServiceType']==__SERVICE_TYPE_PTW__) || ($data['idServiceType']==__SERVICE_TYPE_PTP__) || ($data['idServiceType']==__SERVICE_TYPE_DTP__) || ($data['idServiceType']==__SERVICE_TYPE_WTP__))
										{
											$resultAry[$ctr]['fDestinationTruckingRate'] = $fTruckingRateTo ;
										}
										if(($data['idServiceType']==__SERVICE_TYPE_WTW__) || ($data['idServiceType']==__SERVICE_TYPE_WTD__) || ($data['idServiceType']==__SERVICE_TYPE_WTP__) || ($data['idServiceType']==__SERVICE_TYPE_PTP__) || ($data['idServiceType']==__SERVICE_TYPE_PTW__) || ($data['idServiceType']==__SERVICE_TYPE_PTD__))
										{
											$resultAry[$ctr]['fOriginTruckingRate'] = $fTruckingRateFrom ;
										}
										
										$resultAry[$ctr]['fTotalDestinationPortPrice'] = $fTotalDestinationPortPrice;
										$resultAry[$ctr]['szDestinationPortCurrency'] = $szDestinationPortCurrency;
										
										if($fDestinationFreightExchangeRate>0)
										{
											$resultAry[$ctr]['fDestinationFreightExchangeRate'] = round((float)(1/$fDestinationFreightExchangeRate),6);
										}
										else
										{
											$resultAry[$ctr]['fDestinationFreightExchangeRate'] = 0 ;	
										}
										
										$resultAry[$ctr]['fTotalOriginPortPrice'] = $fTotalOriginPortPrice;
										$resultAry[$ctr]['szOriginPortCurrency'] = $szOriginPortCurrency;
										
										if($fOriginFreightExchangeRate>0)
										{
											$resultAry[$ctr]['fOriginFreightExchangeRate'] = round((float)(1/$fOriginFreightExchangeRate),6);
										}
										else
										{
											$resultAry[$ctr]['fOriginFreightExchangeRate'] = 0 ;
										}
										
										if($row['szFreightCurrency']==1)  // USD
										{
											$fPrice = $row['fRateWM'] * $fPriceFactor ;
											$fBookingRate = $row['fRate'];
											$fMinRateWM = $row['fMinRateWM'];
											$resultAry[$ctr]['fFreightExchangeRate'] = 1 ; 	
											
											if($fPrice < $fMinRateWM)
											{
												$fPrice = $row['fMinRateWM'];								
											}						
										}
										else
										{
											/**
											* getting USD factor
											*/  
											$fUSDValue = $this->getCurrencyFactor($row['szFreightCurrency']);
											
											/**
											* converting price to USD
											*/	
													
											$fPrice = $row['fRateWM'] * $fPriceFactor ;
											$fBookingRate = $row['fRate'];
											$fMinRateWM = $row['fMinRateWM'];
											
											if($fPrice < $fMinRateWM)
											{
												$fPrice = $row['fMinRateWM'];								
											}		
																				
											$fPrice = $fPrice * $fUSDValue ;
											$fBookingRate = $row['fRate'] * $fUSDValue;
											$fMinRateWM = $row['fMinRateWM'] * $fUSDValue ;
											$resultAry[$ctr]['fFreightExchangeRate'] = $fUSDValue;
										}
										//if(!empty($data['iOriginCC']))
										//{
											/**
											* calculating custom clearance amount in USD
											**/
											if($data['iOriginCC']==1)  // OriginCustomClearance
											{
												$originCCAry=array();
												$originCCAry = $this->getCustomClearanceDetails($idWHSTo,$idWHSFrom,$data['iOriginCC']);
												$resultAry[$ctr]['fOriginCCPrice'] = $originCCAry['fPrice'];
												$resultAry[$ctr]['fOriginCCCurrency'] = $originCCAry['idCurrency'];
												$resultAry[$ctr]['szOriginCCCurrency'] = $originCCAry['szCurrency'];
												$resultAry[$ctr]['fOriginCCExchangeRate'] = $originCCAry['fExchangeRate'];
											}
											if($data['iDestinationCC']==2)  // DestinationCustomClearance
											{
												$DestinationCCAry=array();
												$DestinationCCAry = $this->getCustomClearanceDetails($idWHSTo,$idWHSFrom,$data['iDestinationCC']);
												
												$resultAry[$ctr]['fDestinationCCPrice'] = $DestinationCCAry['fPrice'];
												$resultAry[$ctr]['fDestinationCCCurrency'] = $DestinationCCAry['idCurrency'];
												$resultAry[$ctr]['szDestinationCCCurrency'] = $DestinationCCAry['szCurrency'];
												$resultAry[$ctr]['fDestinationCCExchangeRate'] = $DestinationCCAry['fExchangeRate'];
											}
											$fCustomAmount = $originCCAry['fPrice'] + $DestinationCCAry['fPrice'] ;
										//}
										
										/**
										 * calculating total price for shipment.
										 * 
										 * Total price = WTW Price + custom clearance amount + rate per booking  
										 *               + standard trucking rate ( if applicable ) + Haulage price ( if applicable ) 
										*/
										
										$fPrice = round((float)($fPrice + $fTotalPortPrice),6);
										$fCustomAmount = round((float)$fCustomAmount,6);
										$fBookingRate = round((float)$fBookingRate,6);
										$fTotalStandardTruckingRate = round((float)($fTotalStandardTruckingRate * $fPriceFactor),6);
										$fTotalHaulagePrice = round((float)$fTotalHaulagePrice,6);
										
										$fTotalPrice = $fPrice + $fCustomAmount + $fBookingRate + ($fTotalStandardTruckingRate) + $fTotalHaulagePrice ;
										$fPriceUSD = $fTotalPrice - $fTotalStandardTruckingRate ;
										
										$resultAry[$ctr]['fPTPPrice'] = $fPrice + $fBookingRate -$fTotalPortPrice ;
										$resultAry[$ctr]['fWTWPrice'] = $fPrice + $fBookingRate;
										// this the price which are display for the user 
										if($data['fExchangeRate']>0)
										{
											$fPrice = round((float)$fPrice/$data['fExchangeRate'],6);
											$fCustomAmount = round((float)$fCustomAmount/$data['fExchangeRate'],6);
											$fBookingRate = round((float)$fBookingRate/$data['fExchangeRate'],6);
											$fTotalStandardTruckingRate = round((float)(($fTotalStandardTruckingRate)/$data['fExchangeRate']),6);
											$fTotalHaulagePrice = round((float)$fTotalHaulagePrice/$data['fExchangeRate'],6);
											
//											/$resultAry[$ctr]['fDisplayPrice'] =number_format((double)( ($fPrice + $fCustomAmount + $fBookingRate + $fTotalHaulagePrice) / $data['fExchangeRate']),6,'.','') ;
											$resultAry[$ctr]['fDisplayPrice'] =number_format((double)( ($fPrice + $fCustomAmount + $fBookingRate + $fTotalHaulagePrice)),6,'.','') ;
										}
										else
										{
											$resultAry[$ctr]['fDisplayPrice'] =number_format((double)(($fPrice + $fCustomAmount + $fBookingRate + $fTotalHaulagePrice)),6,'.','') ;
										}
										$fTotalMarkupPrice = 0.00;
										$fMarkupPriceUSD = 0.00 ;
										
										if($iDistanceWHSFrom['szForwarderCurrency']==$data['idCurrency'])
										{
                                                                                    $resultAry[$ctr]['fTotalMarkupPrice'] = 0.00 ;
										}
										else
										{
                                                                                    $fTotalDisplayPrice = $resultAry[$ctr]['fDisplayPrice'] ;
                                                                                    $fPriceMarkUp = $this->getManageMentVariableByDescription('__MARK_UP_PRICING_PERCENTAGE__');
                                                                                    $fMarkupPriceUSD = (($fPriceUSD* $fPriceMarkUp)/100);

                                                                                    $fTotalMarkupPrice = (($fTotalDisplayPrice * $fPriceMarkUp)/100);
                                                                                    $fTotalDisplayPrice = $fTotalDisplayPrice + $fTotalMarkupPrice ;
                                                                                    $resultAry[$ctr]['fTotalMarkupPrice'] = round((float)$fTotalMarkupPrice,6) ;
                                                                                    $resultAry[$ctr]['fMarkupPercentage'] = $fPriceMarkUp ;
                                                                                    $resultAry[$ctr]['fDisplayPrice'] = round((float)$fTotalDisplayPrice,6);
										} 
                                                                                
										$resultAry[$ctr]['fBookingPriceUSD'] = $fMarkupPriceUSD + $fPriceUSD ;
										$resultAry[$ctr]['fTotalHaulagePrice'] = $fTotalHaulagePrice ;
										
										$resultAry[$ctr]['idForwarderCurrency'] = $iDistanceWHSFrom['szForwarderCurrency'] ;
										$resultAry[$ctr]['szForwarderCurrency'] = $iDistanceWHSFrom['szForwarderCurrencyName'];
										$resultAry[$ctr]['fForwarderCurrencyExchangeRate'] = $iDistanceWHSFrom['fForwarderCurrencyExchangeRate'] ;
										
										//This is customer currency price without XE mark-up
										$fCustomerCurrencyPrice_XE =  $resultAry[$ctr]['fDisplayPrice'] - $resultAry[$ctr]['fTotalMarkupPrice'];
										
										/*
										* converting price from customer currency to forwarder currency
										*/
										if($iDistanceWHSFrom['szForwarderCurrency'] == $data['idCurrency'])
										{
                                                                                    /*
                                                                                    * If customer currency and forwarder currency is same then no need of any conversion
                                                                                    */
                                                                                    $resultAry[$ctr]['fForwarderCurrencyPrice'] = round((float)$resultAry[$ctr]['fDisplayPrice'],6);
                                                                                    $resultAry[$ctr]['fForwarderCurrencyPrice_XE'] = round((float)$fCustomerCurrencyPrice_XE,6);
										}
										else
										{
                                                                                    /*
                                                                                    * converting price from customer currency to forwarder currency.
                                                                                    * to findout price i first convert customer price in USD then convert it into forwarder currency 
                                                                                    */
                                                                                    $tempCustomerUSDPrice = 0;
                                                                                    if($data['idCurrency']==1)
                                                                                    {
                                                                                        /*
                                                                                        * if customer price is in USD then no need of converstion
                                                                                        */
                                                                                        $tempCustomerUSDPrice = $resultAry[$ctr]['fDisplayPrice'] ;
                                                                                        $tempCustomerUSDPrice_XE = $fCustomerCurrencyPrice_XE ;
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        /*
                                                                                        * now converting customer price into USD
                                                                                        */												
                                                                                        $tempCustomerUSDPrice = $resultAry[$ctr]['fDisplayPrice'] * $data['fExchangeRate'] ;
                                                                                        $tempCustomerUSDPrice_XE = $fCustomerCurrencyPrice_XE * $data['fExchangeRate'] ;
                                                                                    }
                                                                                    if($resultAry[$ctr]['idForwarderCurrency']==1)
                                                                                    {
                                                                                        /*
                                                                                        * if forwarder price is in USD then no need of converstion
                                                                                        */
                                                                                        $resultAry[$ctr]['fForwarderCurrencyPrice'] =round((float)$tempCustomerUSDPrice,6);
                                                                                        $resultAry[$ctr]['fForwarderCurrencyPrice_XE'] = round((float)$tempCustomerUSDPrice_XE,6);
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        $tempCustomerUSDPrice = round((float)($tempCustomerUSDPrice/$resultAry[$ctr]['fForwarderCurrencyExchangeRate']),6);
                                                                                        $tempCustomerUSDPrice_XE = round((float)($tempCustomerUSDPrice_XE/$resultAry[$ctr]['fForwarderCurrencyExchangeRate']),6);
                                                                                        $resultAry[$ctr]['fForwarderCurrencyPrice'] = round((float)$tempCustomerUSDPrice,6);
                                                                                        $resultAry[$ctr]['fForwarderCurrencyPrice_XE'] = round((float)$tempCustomerUSDPrice_XE,6);
                                                                                    }
										}
										/*
										There no need to checking valid date of referral fee.13-12-2012
										$dtReferealFeeValid_time = strtotime($iDistanceWHSFrom['dtReferealFeeValid']);
										if($dtReferealFeeValid_time > time())
										{
											$fReferralPercentage = $iDistanceWHSFrom['fReferalFee'];
										}
										else
										{
											$fReferralPercentage = $this->getManageMentVariableByDescription('__TRANSPORTECA_REFERAL_FEE__');
										}*/
										$fReferralPercentage = $iDistanceWHSFrom['fReferalFee'];
										if(($fReferralPercentage>0) && ($resultAry[$ctr]['fForwarderCurrencyPrice_XE']>0))
										{
                                                                                    $fReferralAmount = ($resultAry[$ctr]['fForwarderCurrencyPrice_XE'] * $fReferralPercentage)/100 ;
                                                                                    $resultAry[$ctr]['fReferalAmount'] = $fReferralAmount ;
                                                                                    $resultAry[$ctr]['fReferalPercentage'] = $fReferralPercentage ;
                                                                                    
                                                                                    $fForwarderExchangeRate = $resultAry[$ctr]['fForwarderCurrencyExchangeRate'] ;
                                                                                    
                                                                                    if($fForwarderExchangeRate>0)
                                                                                    {
                                                                                        $resultAry[$ctr]['fReferalAmountUSD'] = ($fReferralAmount * $fForwarderExchangeRate) ;
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        $resultAry[$ctr]['fReferalAmountUSD'] = 0 ;
                                                                                    }
										}
										
										//if($resultAry[$ctr]['fForwarderCurrencyExchangeRate']>0)
										//{
											//$resultAry[$ctr]['fForwarderCurrencyExchangeRate'] = $iDistanceWHSFrom['fForwarderCurrencyExchangeRate'] ;
										//}//	
										/**
										* Converting price from USD to Customer currency 
										*/
										//echo "<br> curr ".$data['idCurrency'] ;
										
										//if((int)$data['idCurrency']==1) // USD
										//{										
											$resultAry[$ctr]['idCurrency'] =$data['idCurrency'];
											$resultAry[$ctr]['szCurrency'] =$data['szCurrency'];
											$resultAry[$ctr]['fUsdValue'] = $data['fExchangeRate'] ;
										    if((float)$data['fExchangeRate']>0)
										    {							
												$resultAry[$ctr]['fTotalPriceCustomerCurrency'] =number_format((double)(($fTotalPrice / $data['fExchangeRate'])+$fTotalMarkupPrice),6,'.','') ;
										    }
										    else
										    {
										    	$resultAry[$ctr]['fTotalPriceCustomerCurrency'] =number_format((double)($fTotalPrice+$fTotalMarkupPrice),6,'.','');
										    }
										    
										/*}
										else
										{
											$fExchangeRate = 0;
											$currencyExchangeRateAry = $this->getCurrencyDetails($data['idCurrency']);
											$resultAry[$ctr]['idCurrency'] = $currencyExchangeRateAry['idCurrency'];
											$resultAry[$ctr]['szCurrency'] = $currencyExchangeRateAry['szCurrency'];
											$resultAry[$ctr]['fUsdValue'] = $currencyExchangeRateAry['fUsdValue'];
									
											$resultAry[$ctr]['fTotalPriceCustomerCurrency'] =number_format((double)( $fTotalPrice * $currencyExchangeRateAry['fUsdValue']),2,'.','') ;
										}
										*/
                                                                            /**
                                                                            * calculating total transportation time
                                                                            **/ 
                                                                            //to get transportation time in millisecond we need to convert transit day to millisecond and then add szUTCOffset
                                                                            $dtTransportTime_ms = ($fTotalTransitHours*60*60*1000) ;
                                                                            $iTransitDays_time_ms = ($fTotalTransitDays_Hours*60*60*1000) ; // converting actual day value to millisecond
                                                                            if($data['idTimingType']==1)
                                                                            {						
                                                                               if($data['idServiceType']==__SERVICE_TYPE_DTD__ || $data['idServiceType']== __SERVICE_TYPE_DTW__ || $data['idServiceType']== __SERVICE_TYPE_DTP__) // 1.DTD 2.DTW
                                                                               {
                                                                                       // in case of DTD and DTW substracting origin haulage from cutoff day for calculating expacted pick-up date
                                                                                       $WhsOffHours = $iBookingCutOffHours - ($fFromWareHouseHaulageAry['iHaulageTransitTime']*60*60);
                                                                                       $dtExpactedCuttoff = time() + ($WhsOffHours); 
                                                                               } 
                                                                               else
                                                                               {
                                                                                       $dtExpactedCuttoff = time() + ($iBookingCutOffHours);
                                                                               }

                                                                               $dtReadyAtOrigin = strtotime($date_str);

                                                                               if($dtReadyAtOrigin>$dtExpactedCuttoff)
                                                                               {
                                                                                       $total_date_str = $dtReadyAtOrigin;
                                                                               }
                                                                               else
                                                                               {
                                                                                       $total_date_str = $dtExpactedCuttoff ;
                                                                               }
                                                                               $total_date_str = $total_date_str + ($fFromWareHouseHaulageAry['iHaulageTransitTime']*60*60);
                                                                               if(date('l',$total_date_str)==$row['szWeekDay'])
                                                                               {
                                                                                       $dtCutOffDateTime = date('Y-m-d',$total_date_str)." ".$row['szCutOffLocalTime'] ;										 		
                                                                               }
                                                                               else
                                                                               {
                                                                                       $dtCutOffDateTime = date('Y-m-d',strtotime("next ".strtolower($row['szWeekDay']),$total_date_str.'+1DAY'))." ".$row['szCutOffLocalTime'] ;										 		
                                                                               }

                                                                               // we are taking transit day -1 day 
                                                                               if($row['iTransitHours']>=24)
                                                                               {
                                                                                       $iTransitHour_1 = $row['iTransitHours']-24;
                                                                               }
                                                                               else
                                                                               {
                                                                                       $iTransitHour_1 = 0;
                                                                               }

                                                                               // calculating actual warehouse cutoff date there is no haulage included in it
                                                                               $fTotalWTWTransitHour = strtotime($dtCutOffDateTime) + ($iTransitHour_1*60*60);
                                                                               $resultAry[$ctr]['dtWhsCutOff'] = date('d/m/Y H:i',strtotime($dtCutOffDateTime)); 
                                                                               $resultAry[$ctr]['dtWhsCutOff_time'] = strtotime($dtCutOffDateTime);

                                                                               // now calculating cutoff date which is shown on services page. 
                                                                               if($data['idServiceType']==__SERVICE_TYPE_DTD__ || $data['idServiceType']== __SERVICE_TYPE_DTW__ || $data['idServiceType']== __SERVICE_TYPE_DTP__) // 1.DTD 2.DTW 3.DTP
                                                                               {
                                                                                       // in case of DTD and DTW substracting origin haulage from cutoff day for calculating pick-up date
                                                                                       $iCutOffHours = strtotime($dtCutOffDateTime) - ($fFromWareHouseHaulageAry['iHaulageTransitTime']*60*60); 
                                                                               }
                                                                               else
                                                                               {
                                                                                       $iCutOffHours = strtotime($dtCutOffDateTime); 
                                                                               }

                                                                               $resultAry[$ctr]['dtCutOffDate'] = date('d/m/Y H:i',$iCutOffHours);
                                                                               $resultAry[$ctr]['dtCuttOffDate_formated'] = date('Y-m-d',$iCutOffHours);

                                                                               // calculating actual warehouse available date there is no haulage included in it
                                                                               $dtAvailableDateTime = strtotime($dtCutOffDateTime) + ($iTransitHour_1*60*60);


                                                                               if(date('l',$dtAvailableDateTime)==$row['szAvailableDay'])
                                                                               {
                                                                                       $resultAry[$ctr]['dtWhsAvailable'] = date('d/m/Y',$dtAvailableDateTime)." ".$row['szAvailableLocalTime'];
                                                                                       $resultAry[$ctr]['dtWhsAvailable_formated'] = date('Y-m-d',$dtAvailableDateTime)." ".$row['szAvailableLocalTime'];
                                                                               }
                                                                               else
                                                                               {
                                                                                   //$dtCutOffDateTime = date('Y-m-d',strtotime("next ".strtolower($row['szWeekDay']),$total_date_str.'+1DAY'))." ".$row['szCutOffLocalTime'] ;
                                                                                   $nextAvailableDay = date('Y-m-d',strtotime("next ".strtolower($row['szAvailableDay']),$dtAvailableDateTime.'+1DAY'))." ".$row['szAvailableLocalTime'] ;
                                                                                   $dtAvailableDateTime = strtotime($nextAvailableDay);
                                                                                   $resultAry[$ctr]['dtWhsAvailable'] = date('d/m/Y',$dtAvailableDateTime)." ".$row['szAvailableLocalTime'];
                                                                                   $resultAry[$ctr]['dtWhsAvailable_formated'] = date('Y-m-d',$dtAvailableDateTime)." ".$row['szAvailableLocalTime'];
                                                                               }
                                                                               // now calculating available which is shown on services page. 
                                                                               if($data['idServiceType']==__SERVICE_TYPE_DTD__ || $data['idServiceType']==__SERVICE_TYPE_WTD__ || $data['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD 3.WTD
                                                                               {
                                                                                   // in case of DTD and WTD adding destination haulage into available day for calculating delivery date.
                                                                                   $dtAvailableTimes = strtotime($resultAry[$ctr]['dtWhsAvailable_formated']) + ($fToWareHouseHaulageAry['iHaulageTransitTime']*60*60); 
                                                                               }
                                                                               else
                                                                               {
                                                                                   $dtAvailableTimes = strtotime($resultAry[$ctr]['dtWhsAvailable_formated']); 
                                                                               }
                                                                               
                                                                               /*
                                                                                * when displaying services which end with D (e.g. PTD or DTD), if the delivery date is either Saturday or Sunday, then we show the following Monday instead. 
                                                                                */
                                                                                if($data['idServiceType']==__SERVICE_TYPE_DTD__ || $data['idServiceType']==__SERVICE_TYPE_WTD__ || $data['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD 3.WTD
                                                                                {
                                                                                   $szDayName = strtolower(date('l',$dtAvailableTimes)) ; 
                                                                                   $weekendAry = array('saturday','sunday');
                                                                                   
                                                                                   if(!empty($weekendAry) && in_array($szDayName, $weekendAry))
                                                                                   { 
                                                                                        $szNextMondayDate = date('Y-m-d H:i:s', strtotime('next Monday'." ".date('Y-m-d',$dtAvailableTimes))); 
                                                                                        $dtAvailableTimes = strtotime($szNextMondayDate);
                                                                                   }
                                                                                }

                                                                                   //This is the available date time which is shown on services page.
                                                                               $resultAry[$ctr]['dtAvailableDate']= date('d/m/Y H:i',$dtAvailableTimes) ;
                                                                               $resultAry[$ctr]['dtAvailabeleDate_formated'] = date('Y-m-d',$dtAvailableTimes);

                                                                                // we are taking cutoff date and Available date in time and php date format (Y-m-d H:i) so that we do not need any furthure calculation for searching
                                                                               $resultAry[$ctr]['iCuttOffDate_time'] = strtotime($resultAry[$ctr]['dtCuttOffDate_formated']);
                                                                               $resultAry[$ctr]['iAvailabeleDate_time'] = strtotime($resultAry[$ctr]['dtAvailabeleDate_formated']);
                                                                               $resultAry[$ctr]['dtCuttOffDate_formated'] = convert_time_to_UTC_date($resultAry[$ctr]['iCuttOffDate_time']);
                                                                               $resultAry[$ctr]['iAvailabeleDate_formated'] = convert_time_to_UTC_date($resultAry[$ctr]['iAvailabeleDate_time']); 
                                                                            }
                                                                            else if($data['idTimingType']==2)
                                                                            {
                                                                               $total_date_str = strtotime($date_str) ;

                                                                               if(date('l',$total_date_str)==$row['szAvailableDay'])
                                                                               {
                                                                                   $dtAvailableDate = date('Y-m-d',$total_date_str)." ".$row['szAvailableLocalTime'] ;										 		
                                                                               }
                                                                               else
                                                                               {
                                                                                   $dtAvailableDate = date('Y-m-d',strtotime("last ".$row['szAvailableDay'],$total_date_str.'+1DAY'))." ".$row['szAvailableLocalTime'] ;										 		
                                                                               }

                                                                               // calculating actual warehouse cutoff and available date there is no haulage included in it
                                                                               //$fTotalWTWTransitHour = strtotime($dtAvailableDateTime) - ($row['iTransitHours']*60*60);
                                                                               $resultAry[$ctr]['dtWhsAvailable'] = date('d/m/Y H:i',strtotime($dtAvailableDate));									 	
                                                                               $resultAry[$ctr]['dtWhsAvailable_time'] = strtotime($dtAvailableDate);

                                                                             // now calculating available which is shown on services page. 
                                                                                if($data['idServiceType']==__SERVICE_TYPE_DTD__ || $data['idServiceType']==__SERVICE_TYPE_WTD__ || $data['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD 3.WTD
                                                                                {
                                                                                        // in case of DTD and WTD adding destination haulage into available day for calculating delivery date.
                                                                                        $dtAvailableDateTime = strtotime($dtAvailableDate) + ($fToWareHouseHaulageAry['iHaulageTransitTime']*60*60); 
                                                                                }
                                                                                else
                                                                                {
                                                                                        $dtAvailableDateTime = strtotime($dtAvailableDate); 
                                                                                }
                                                                                
                                                                                /*
                                                                                * when displaying services which end with D (e.g. PTD or DTD), if the delivery date is either Saturday or Sunday, then we show the following Monday instead. 
                                                                                */
                                                                                if($data['idServiceType']==__SERVICE_TYPE_DTD__ || $data['idServiceType']==__SERVICE_TYPE_WTD__ || $data['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD 3.WTD
                                                                                {
                                                                                   $szDayName = strtolower(date('l',$dtAvailableDateTime)) ; 
                                                                                   $weekendAry = array('saturday','sunday');
                                                                                   
                                                                                   if(!empty($weekendAry) && in_array($szDayName, $weekendAry))
                                                                                   { 
                                                                                        $szNextMondayDate = date('Y-m-d H:i:s', strtotime('next Monday'." ".date('Y-m-d',$dtAvailableDateTime))); 
                                                                                        $dtAvailableDateTime = strtotime($szNextMondayDate);
                                                                                   }
                                                                                }

                                                                                $resultAry[$ctr]['dtAvailableDate'] = date('d/m/Y H:i',$dtAvailableDateTime);
                                                                                $resultAry[$ctr]['dtAvailabeleDate_formated'] = date('Y-m-d',$dtAvailableDateTime);

                                                                                // we are taking transit day -1 day 
                                                                                if($row['iTransitHours']>=24)
                                                                                {
                                                                                        $iTransitHour_1 = $row['iTransitHours'] - 24 ;
                                                                                }
                                                                                else
                                                                                {
                                                                                        $iTransitHour_1 = 0;
                                                                                }

                                                                                $dtCutOffDateTime = $resultAry[$ctr]['dtWhsAvailable_time'] - ($iTransitHour_1*60*60);
                                                                                //echo "<br /> transit hour ".$row['iTransitHours'] ;
                                                                                if(date('l',$dtCutOffDateTime) == $row['szWeekDay'])
                                                                                {
                                                                                    $resultAry[$ctr]['dtWhsCutOff'] = date('d/m/Y',$dtCutOffDateTime)." ".$row['szCutOffLocalTime'] ;
                                                                                    $resultAry[$ctr]['dtWhsCutOff_format'] = date('Y-m-d',$dtCutOffDateTime)." ".$row['szCutOffLocalTime'] ;										 		
                                                                                }
                                                                                else
                                                                                {
                                                                                    $resultAry[$ctr]['dtWhsCutOff'] = date('d/m/Y',strtotime("last ".strtolower($row['szWeekDay']),$dtCutOffDateTime.'+1DAY'))." ".$row['szCutOffLocalTime'] ;
                                                                                    $resultAry[$ctr]['dtWhsCutOff_format'] = date('Y-m-d',strtotime("last ".strtolower($row['szWeekDay']),$dtCutOffDateTime.'+1DAY'))." ".$row['szCutOffLocalTime'] ;									 		
                                                                                }
                                                                                //echo "<br /> whs cutoff  ".$resultAry[$ctr]['dtWhsCutOff']
                                                                                 // now calculating cutoff date which is shown on services page. 
                                                                                if($data['idServiceType']==__SERVICE_TYPE_DTD__ || $data['idServiceType']==__SERVICE_TYPE_DTW__ || $data['idServiceType']==__SERVICE_TYPE_DTP__) // 1.DTD 2.DTW
                                                                                {
                                                                                    // in case of DTD and DTW substracting origin haulage from cutoff day for calculating pick-up date
                                                                                    $dtCutOffDateTime = strtotime($resultAry[$ctr]['dtWhsCutOff_format']) - ($fFromWareHouseHaulageAry['iHaulageTransitTime']*60*60); 
                                                                                }
                                                                                else
                                                                                {
                                                                                    $dtCutOffDateTime = strtotime($resultAry[$ctr]['dtWhsCutOff_format']); 
                                                                                }

                                                                                $resultAry[$ctr]['dtCutOffDate'] = date('d/m/Y H:i',$dtCutOffDateTime);
                                                                                $resultAry[$ctr]['dtCuttOffDate_formated'] = date('Y-m-d H:i',$dtCutOffDateTime);


                                                                                // we are taking cutoff date and Available date in time and php date format (Y-m-d H:i) so that we do not need any furthure calculation for searching
                                                                                $resultAry[$ctr]['iCuttOffDate_time'] = strtotime($resultAry[$ctr]['dtCuttOffDate_formated']);
                                                                                $resultAry[$ctr]['iAvailabeleDate_time'] = strtotime($resultAry[$ctr]['dtAvailabeleDate_formated']);	

                                                                                $resultAry[$ctr]['iAvailabeleDate_formated'] = convert_time_to_UTC_date($resultAry[$ctr]['iAvailabeleDate_time']);
                                                                                //print_r($resultAry);
                                                                            }								
									
										// now convert $dtTransportTime_ms to day to get actual transportation time. 
										$dtTotalTransportDays =floor((double)($dtTransportTime_ms/(1000*60*60*24)));
									
										// at the following line we are finding reminder hours 
										$dtTotalTransportHours =($dtTransportTime_ms -($dtTotalTransportDays * (1000*60*60*24)))/(1000*60*60);
									
										$szTotalTransportationTime = ""; 
										$szTotalTransportationTime = (int)$dtTotalTransportDays." ".t($this->t_base.'fields/days');
										$szTotalTransportationTime .= "<br>".(int)$dtTotalTransportHours." ".t($this->t_base.'fields/hours');
																				
										$resultAry[$ctr]['idTimingType'] = $data['idTimingType'] ;
										// taking total transportation time in hours for left slider at select services page .
										$resultAry[$ctr]['iTransitHour'] = ($dtTotalTransportDays*24)+$dtTotalTransportHours ;
										
										$resultAry[$ctr]['szTotalTransportationTime'] = $szTotalTransportationTime ;
										//$resultAry[$ctr]['iDistanceFromWHS'] = number_format((float)$iDistanceWHSFrom['iDistance']);  // distance from warehouse
										//$resultAry[$ctr]['iDistanceToWHS'] = number_format((float)$iDistanceWHSTo['iDistance']);      // distance to warehouse
										 
										$resultAry[$ctr]['iDistanceFromWHS'] = round((float)$iDistanceWHSFrom['iDistance']);  // distance from warehouse
										$resultAry[$ctr]['iDistanceToWHS'] = round((float)$iDistanceWHSTo['iDistance']); 
										
										$resultAry[$ctr]['szFromWHSName'] = $iDistanceWHSFrom['szWareHouseName'];     //To WareHouse Name;
										$resultAry[$ctr]['szToWHSName'] = $iDistanceWHSTo['szWareHouseName'];        // To WareHouse Name
										
										$resultAry[$ctr]['szFromWHSContactPerson'] = $iDistanceWHSFrom['szContactPerson'];     //To WareHouse Name;
										$resultAry[$ctr]['szToWHSContactPerson'] = $iDistanceWHSTo['szContactPerson'];        // To WareHouse Name
										$resultAry[$ctr]['szFromWHSEmail'] = $iDistanceWHSFrom['szEmail'];     //To WareHouse Name;
										$resultAry[$ctr]['szToWHSEmail'] = $iDistanceWHSTo['szEmail'];        // To WareHouse Name
										
										$resultAry[$ctr]['szFromWHSPostCode'] = $iDistanceWHSFrom['szPostCode'];     //To WareHouse Name;
										$resultAry[$ctr]['szToWHSPostCode'] = $iDistanceWHSTo['szPostCode'];        // To WareHouse Name
										
										$resultAry[$ctr]['szFromCountry'] = $iDistanceWHSFrom['szCountry'];     //To WareHouse Name;
										$resultAry[$ctr]['szToCountry'] = $iDistanceWHSTo['szCountry']; 
										
										$resultAry[$ctr]['idForwardTo'] = $iDistanceWHSTo['idForwarder'];
										$resultAry[$ctr]['idForwardFrom'] = $iDistanceWHSFrom['idForwarder'];
										$resultAry[$ctr]['fPrice'] = round((float)$fTotalPrice,2);
										$resultAry[$ctr]['idForwarder'] = $iDistanceWHSFrom['idForwarder'] ;
										$resultAry[$ctr]['szDisplayName'] = $iDistanceWHSFrom['szDisplayName'] ;
										
										$resultAry[$ctr]['szFromWHSCity'] = $iDistanceWHSFrom['szCity'];     //To WareHouse Name;
										$resultAry[$ctr]['szToWHSCity'] = $iDistanceWHSTo['szCity'];        // To WareHouse Name
										$resultAry[$ctr]['szFromWHSCountryISO'] = $iDistanceWHSFrom['szCountryISO'];     //To WareHouse Name;
										$resultAry[$ctr]['szToWHSCountryISO'] = $iDistanceWHSTo['szCountryISO'];        // To WareHouse Name
										
										$resultAry[$ctr]['szFromWHSCountry'] = $iDistanceWHSFrom['szCountry'];     //To WareHouse Name;
										$resultAry[$ctr]['szToWHSCountry'] = $iDistanceWHSTo['szCountry'];        // To WareHouse Name
										
										$resultAry[$ctr]['idFromWHS'] = $iDistanceWHSFrom['id'];     //To WareHouse Name;
										$resultAry[$ctr]['idToWHS'] = $iDistanceWHSTo['id'];   
										
										$resultAry[$ctr]['szFromWHSLat'] = $iDistanceWHSFrom['szLatitude'];     //To WareHouse Name;
										$resultAry[$ctr]['szToWHSLat'] = $iDistanceWHSTo['szLatitude'];        // To WareHouse Name
										$resultAry[$ctr]['szFromWHSLong'] = $iDistanceWHSFrom['szLongitude'];     //To WareHouse Name;
										$resultAry[$ctr]['szToWHSLong'] = $iDistanceWHSTo['szLongitude'];
										$resultAry[$ctr]['szLogo'] = $iDistanceWHSFrom['szLogo'];
										$resultAry[$ctr]['dtValidFrom'] = $row['dtValidFrom'];     //To WareHouse Name;
										$resultAry[$ctr]['dtExpiry'] = $row['dtExpiry'];
										$resultAry[$ctr]['idServiceType'] = $data['idServiceType'];
										$resultAry[$ctr]['fCargoVolume'] = $data['fCargoVolume'];
										$resultAry[$ctr]['fCargoWeight'] = $data['fCargoWeight'];
										
										$resultAry[$ctr]['dtTransportTime_ms'] = $dtTransportTime_ms;
										$resultAry[$ctr]['iTransitDays_time_ms'] = $iTransitDays_time_ms;
										
										$resultAry[$ctr]['unique_id'] = $resultAry[$ctr]['iCuttOffDate_time'].'_'.$row['idWTW'].'_'.$resultAry[$ctr]['iAvailabeleDate_time'];
										$ctr++;
									}
								}
							}
						}
					    else
						{
							$this->error = true;
							$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
							$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
							return false;
						}
						$innerCtr++;
						}
					}
					$outerCtr++;
				} 
                                
//				$filename = __APP_PATH_ROOT__."/logs/debug.log";
//					
//				$strdata=array();
//				$strdata[0]=" \n\n result array\n".print_R($resultAry,true)."\n";
//				file_log(true, $strdata, $filename);
                        
				if(!empty($resultAry))
				{
                                    $final_ary=array();
                                    $res_ary = array();

                                    $iCutOffNDays =(int) $this->getManageMentVariableByDescription('__CUTOFF_N_DAYS__');
                                    $iAvailableNDays =(int) $this->getManageMentVariableByDescription('__PICK_UP_AFTER_M_DAYS__');

                                    $landing_page_time = strtotime(date('y-m-d',strtotime($date_str)));
                                    $minus_14_days = strtotime ( '-'.$iAvailableNDays.' day' , $landing_page_time ) ;
                                    $plus_14_days = strtotime ('+'.$iCutOffNDays.' day' , $landing_page_time ) ;

                                    $finalResultAry = array();
                                    foreach($resultAry as $resultArys)
                                    {		
						$dtValidFromDate = 	strtotime($resultArys['dtValidFrom']);
						$dtExpiryFromDate = strtotime($resultArys['dtExpiry']);
						if($data['idTimingType']==1) //Ready at origin
						{
							$landing_page_time = strtotime(date('y-m-d',strtotime($date_str)));
							$landing_page_time =$landing_page_time;
							
							if(($resultArys['iCuttOffDate_time'] >= $landing_page_time) && ($resultArys['iCuttOffDate_time']>=$dtValidFromDate) && ($resultArys['iCuttOffDate_time']  < $dtExpiryFromDate))
							{
								$tempResultAry = array();
								$days = ($plus_14_days - $resultArys['iCuttOffDate_time']) / (60 * 60 * 24);
								if(($resultArys['iFrequency'] < $days))
								{
									$finalResultAry[$countrr]=$resultArys ;
									$countrr++;
									
								 	$dtCutOffDateTime = date('Y-m-d',strtotime("next ".strtolower($resultArys['szWeekDay']),$resultArys['dtWhsCutOff_time'].'+1DAY'))." ".$resultArys['szCutOffLocalTime'] ;
								 	
								 	$tempResultAry['dtWhsCutOff'] = date('d/m/Y H:i',strtotime($dtCutOffDateTime)); 
								 	$tempResultAry['dtWhsCutOff_time'] = strtotime($dtCutOffDateTime);
										 	
								 	// now calculating available which is shown on services page. 
								 	if($resultArys['idServiceType']==__SERVICE_TYPE_DTD__ || $resultArys['idServiceType']==__SERVICE_TYPE_DTW__ || $data['idServiceType']==__SERVICE_TYPE_DTP__) // 1.DTD 2.DTD
								 	{
								 		// in case of DTD and DTD adding origin haulage into cutoff day for calculating pick-up date.
								 		$iCutOffHours = strtotime($dtCutOffDateTime) - ($resultArys['fOriginHaulageTransitTime']*60*60); 
								 	}
								 	else
								 	{
								 		$iCutOffHours = strtotime($dtCutOffDateTime);
								 	}							 								 	
								 	$tempResultAry['dtCutOffDate'] = date('d/m/Y H:i',$iCutOffHours);
								 	$tempResultAry['dtCuttOffDate_formated'] = date('Y-m-d',$iCutOffHours);
								 
								 	// finding Available at time 
								 	//$dtAvailableDateTime = $iCutOffHours + ($resultArys['iTransitDays_time_ms']/1000);
									
								 	// calculating actual warehouse available date there is no haulage included in it
									// we are taking transit day -1 day 
								 	if($resultArys['iTransitHours']>=24)
								 	{
								 		$iTransitHour_1 = $resultArys['iTransitHours'] - 24 ;
								 	}
								 	else
								 	{
								 		$iTransitHour_1 = 0;
								 	}
								 	$dtAvailableDateTime = strtotime($dtCutOffDateTime) + ($iTransitHour_1*60*60);
								 	
								 	if(date('l',$dtAvailableDateTime) == $resultArys['szAvailableDay'])
								 	{
								 		$tempResultAry['dtWhsAvailable'] = date('d/m/Y',$dtAvailableDateTime)." ".$resultArys['szAvailableLocalTime'];
								 		$tempResultAry['dtWhsAvailable_formated'] = date('Y-m-d',$dtAvailableDateTime)." ".$resultArys['szAvailableLocalTime'];
								 	}
								    else
								    {
								    	$nextAvailableDay = date('Y-m-d',strtotime("next ".strtolower($resultArys['szAvailableDay']),$dtAvailableDateTime))." ".$resultArys['szAvailableLocalTime'] ;
								       	$dtAvailableDateTime = strtotime($nextAvailableDay);
								    	
								    	$tempResultAry['dtWhsAvailable'] = date('d/m/Y',$dtAvailableDateTime)." ".$resultArys['szAvailableLocalTime'];
								 		$tempResultAry['dtWhsAvailable_formated'] = date('Y-m-d',$dtAvailableDateTime)." ".$resultArys['szAvailableLocalTime'];
								    }
									// now calculating available which is shown on services page. 
								 	if($resultArys['idServiceType']==__SERVICE_TYPE_DTD__ || $resultArys['idServiceType']==__SERVICE_TYPE_WTD__ || $resultArys['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD 3.WTD
								 	{
								 		// in case of DTD and WTD adding destination haulage into available day for calculating delivery date.
								 		$dtAvailableTimes = strtotime($tempResultAry['dtWhsAvailable_formated']) + ($resultArys['fDestinationHaulageTransitTime']*60*60); 
								 	}
								 	else
								 	{
								 		$dtAvailableTimes = strtotime($tempResultAry['dtWhsAvailable_formated']); 
								 	}
								 	
                                                                        /*
                                                                        * when displaying services which end with D (e.g. PTD or DTD), if the delivery date is either Saturday or Sunday, then we show the following Monday instead. 
                                                                        */
                                                                        if($resultArys['idServiceType']==__SERVICE_TYPE_DTD__ || $resultArys['idServiceType']==__SERVICE_TYPE_WTD__ || $resultArys['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD 3.WTD
                                                                        {
                                                                           $szDayName = strtolower(date('l',$dtAvailableTimes)) ; 
                                                                           $weekendAry = array('saturday','sunday');

                                                                           if(!empty($weekendAry) && in_array($szDayName, $weekendAry))
                                                                           { 
                                                                                $szNextMondayDate = date('Y-m-d H:i:s', strtotime('next Monday'." ".date('Y-m-d',$dtAvailableTimes))); 
                                                                                $dtAvailableTimes = strtotime($szNextMondayDate);
                                                                           }
                                                                        }
                                                                        
							 		$tempResultAry['dtAvailableDate'] = date('d/m/Y H:i',$dtAvailableTimes);
							 		$tempResultAry['dtAvailabeleDate_formated'] = date('Y-m-d',$dtAvailableTimes);
								 

								     // we are taking cutoff date and Available date in time and php date format (Y-m-d H:i) so that we do not need any furthure calculation for searching
									$tempResultAry['iCuttOffDate_time'] = strtotime($tempResultAry['dtCuttOffDate_formated']);
									$tempResultAry['iAvailabeleDate_time'] = strtotime($tempResultAry['dtAvailabeleDate_formated']);
									$tempResultAry['dtCuttOffDate_formated'] = convert_time_to_UTC_date($tempResultAry['iCuttOffDate_time']);
									$tempResultAry['iAvailabeleDate_formated'] = convert_time_to_UTC_date($tempResultAry['iAvailabeleDate_time']);
									
											
									if(($tempResultAry['iCuttOffDate_time'] >= $landing_page_time) && ($tempResultAry['iCuttOffDate_time']>=$dtValidFromDate) && ($tempResultAry['iCuttOffDate_time']  < $dtExpiryFromDate))
									{	
										$finalResultAry[$countrr]=$resultArys ;
										
										$finalResultAry[$countrr]['dtWhsCutOff'] = $tempResultAry['dtWhsCutOff'];
										$finalResultAry[$countrr]['dtWhsCutOff_time'] = $tempResultAry['dtWhsCutOff_time'];
										$finalResultAry[$countrr]['dtWhsAvailable'] = $tempResultAry['dtWhsAvailable'];
										$finalResultAry[$countrr]['dtWhsAvailable_formated'] = $tempResultAry['dtAvailabeleDate_formated'];
										
										
										$finalResultAry[$countrr]['dtAvailableDate'] = $tempResultAry['dtAvailableDate'];
										$finalResultAry[$countrr]['dtAvailabeleDate_formated'] = $tempResultAry['dtAvailabeleDate_formated'];
										$finalResultAry[$countrr]['dtCutOffDate'] = $tempResultAry['dtCutOffDate'];
										$finalResultAry[$countrr]['dtCuttOffDate_formated'] = $tempResultAry['dtCuttOffDate_formated'];
										$finalResultAry[$countrr]['iCuttOffDate_time'] = $tempResultAry['iCuttOffDate_time'];
										$finalResultAry[$countrr]['iAvailabeleDate_time'] = $tempResultAry['iAvailabeleDate_time'];
										$finalResultAry[$countrr]['iAvailabeleDate_formated'] = $tempResultAry['iAvailabeleDate_formated'];										
										$finalResultAry[$countrr]['unique_id'] = $tempResultAry['iCuttOffDate_time'].'_'.$resultArys['idWTW'].'_'.$tempResultAry['iAvailabeleDate_time'];
										$countrr++;
									}
								}
								else
								{
									$finalResultAry[$countrr]=$resultArys ;
									$countrr++;
								}
							}
						}
						else
						{
							// Available date should in between the date selected at landing page and and date before its 14 days 
							if(($resultArys['iAvailabeleDate_time'] <= $landing_page_time) && ($resultArys['iAvailabeleDate_time'] >= $minus_14_days) && ($resultArys['iCuttOffDate_time']>=time()) && ($resultArys['iCuttOffDate_time']>=$dtValidFromDate) && ($resultArys['iCuttOffDate_time']  <= $dtExpiryFromDate))
							{		
								$days = ($resultArys['iAvailabeleDate_time'] - $minus_14_days) / (60 * 60 * 24);
								//echo " days ".$days." ferquency ".$resultArys['iFrequency'] ;
								if(($resultArys['iFrequency'] < $days))
								{				
									$finalResultAry[$countrr]=$resultArys ;
									$countrr++;
									
									$tempResultAry = array();
																		 	
									$dtAvailableDate = date('Y-m-d',strtotime("last ".$resultArys['szAvailableDay'],$resultArys['iAvailabeleDate_time'].'+1DAY'))." ".$resultArys['szAvailableLocalTime'] ;
									
									//echo "<br /> Available date ".$dtAvailableDate ;
									// calculating actual warehouse cutoff and available date there is no haulage included in it
								 	//$fTotalWTWTransitHour = strtotime($dtAvailableDateTime) - ($row['iTransitHours']*60*60);
								 	$tempResultAry['dtWhsAvailable'] = date('d/m/Y H:i',strtotime($dtAvailableDate));										 	
								 	$tempResultAry['dtWhsAvailable_time'] = strtotime($dtAvailableDate);
								 	
								 	// now calculating available which is shown on services page. 
								 	if($resultArys['idServiceType']==__SERVICE_TYPE_DTD__ || $resultArys['idServiceType']==__SERVICE_TYPE_WTD__ || $resultArys['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD 3.WTD
								 	{
								 		// in case of DTD and WTD adding destination haulage into available day for calculating delivery date.
								 		$dtAvailableDateTime = strtotime($dtAvailableDate) + ($resultArys['fDestinationHaulageTransitTime']*60*60); 
								 	}
								 	else
								 	{
								 		$dtAvailableDateTime = strtotime($dtAvailableDate); 
								 	}
                                                                        
                                                                        /*
                                                                        * when displaying services which end with D (e.g. PTD or DTD), if the delivery date is either Saturday or Sunday, then we show the following Monday instead. 
                                                                        */
                                                                        if($resultArys['idServiceType']==__SERVICE_TYPE_DTD__ || $resultArys['idServiceType']==__SERVICE_TYPE_WTD__ || $resultArys['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD 3.WTD
                                                                        {
                                                                           $szDayName = strtolower(date('l',$dtAvailableDateTime)) ; 
                                                                           $weekendAry = array('saturday','sunday');

                                                                           if(!empty($weekendAry) && in_array($szDayName, $weekendAry))
                                                                           { 
                                                                                $szNextMondayDate = date('Y-m-d H:i:s', strtotime('next Monday'." ".date('Y-m-d',$dtAvailableDateTime))); 
                                                                                $dtAvailableDateTime = strtotime($szNextMondayDate);
                                                                           }
                                                                        }
								 	
									$tempResultAry['dtAvailableDate'] = date('d/m/Y H:i',$dtAvailableDateTime);
								 	$tempResultAry['dtAvailabeleDate_formated'] = date('Y-m-d',$dtAvailableDateTime);
									
									if($resultArys['iTransitHours']>=24)
								 	{
								 		$iTransitHour_1 = $resultArys['iTransitHours'] - 24 ;
								 	}
								 	else
								 	{
								 		$iTransitHour_1 = 0;
								 	}
								 	
								 	$dtCutOffDateTime = $tempResultAry['dtWhsAvailable_time'] - ($iTransitHour_1*60*60);
									if(date('l',$dtCutOffDateTime) == $resultArys['szWeekDay'])
								 	{
								 		$tempResultAry['dtWhsCutOff'] = date('d/m/Y',$dtCutOffDateTime)." ".$resultArys['szCutOffLocalTime'] ;
								 		$tempResultAry['dtWhsCutOff_format'] = date('Y-m-d',$dtCutOffDateTime)." ".$resultArys['szCutOffLocalTime'] ;										 		
								 	}
								 	else
								 	{
								 		$tempResultAry['dtWhsCutOff'] = date('d/m/Y',strtotime("last ".strtolower($resultArys['szWeekDay']),$dtCutOffDateTime.'+1DAY'))." ".$resultArys['szCutOffLocalTime'] ;
								 		$tempResultAry['dtWhsCutOff_format'] = date('Y-m-d',strtotime("last ".strtolower($resultArys['szWeekDay']),$dtCutOffDateTime.'+1DAY'))." ".$resultArys['szCutOffLocalTime'] ;									 		
								 	}
								 	
									// now calculating cutoff date which is shown on services page. 
								 	if($resultArys['idServiceType']==__SERVICE_TYPE_DTD__ || $resultArys['idServiceType']==__SERVICE_TYPE_DTW__) // 1.DTD 2.DTW
								 	{
								 		// in case of DTD and DTW substracting origin haulage from cutoff day for calculating pick-up date
								 		$dtCutOffDateTime = strtotime($tempResultAry['dtWhsCutOff_format']) - ($resultArys['fOriginHaulageTransitTime']*60*60); 
								 	}
								 	else
								 	{
								 		$dtCutOffDateTime = strtotime($tempResultAry['dtWhsCutOff_format']); 
								 	}
								 	
								 	$tempResultAry['dtCutOffDate'] = date('d/m/Y H:i',$dtCutOffDateTime);
								 	$tempResultAry['dtCuttOffDate_formated'] = date('Y-m-d H:i',$dtCutOffDateTime);
								 	
								 	
								    // we are taking cutoff date and Available date in time and php date format (Y-m-d H:i) so that we do not need any furthure calculation for searching
									$tempResultAry['iCuttOffDate_time'] = strtotime($tempResultAry['dtCuttOffDate_formated']);
									$tempResultAry['iAvailabeleDate_time'] = strtotime($tempResultAry['dtAvailabeleDate_formated']);	
									
									$tempResultAry['iAvailabeleDate_formated'] = convert_time_to_UTC_date($tempResultAry['iAvailabeleDate_time']);
									
									/*
										print_r($tempResultAry);
										
										echo "<br/> Available date time ".$tempResultAry['iAvailabeleDate_time'] ;
										echo "<br /> Landing page date ".$landing_page_time ;
										
										echo "<br/> Minus 14 days  ".date('Y-m-d H:i',$minus_14_days);
										echo "<br /> cutoff time  ".date('Y-m-d H:i',$tempResultAry['iCuttOffDate_time']);
										echo "<br /> Valid date ".date('Y-m-d H:i',$dtValidFromDate) ;
										echo "<br /> Expiry date ".date('Y-m-d H:i',$dtExpiryFromDate);
										echo "<br /> curr time ".date('Y-m-d H:i',time());
										if(($tempResultAry['iAvailabeleDate_time'] <= $landing_page_time))
										{
											echo "<br /> condition 1 ";
										}
										if($tempResultAry['iCuttOffDate_time']>time())
										{
											echo "<br /> condition 2 ";
										}
										if($tempResultAry['iAvailabeleDate_time'] >= $minus_14_days)
										{
											echo "<br /> condition 3 ";
										}
										if($tempResultAry['iAvailabeleDate_time']>=$dtValidFromDate)
										{
											echo "<br /> condition 4 ";
										}
										
										if($tempResultAry['iAvailabeleDate_time']  < $dtExpiryFromDate)
										{
											echo "<br /> condition 5 ";
										}
                                                                    */
                                                                    if(($tempResultAry['iAvailabeleDate_time'] <= $landing_page_time) && ($tempResultAry['iAvailabeleDate_time'] >= $minus_14_days) && ($tempResultAry['iCuttOffDate_time']>time()) && ($tempResultAry['iCuttOffDate_time']>=$dtValidFromDate) && ($tempResultAry['iCuttOffDate_time']  < $dtExpiryFromDate))
                                                                    {	
                                                                        $finalResultAry[$countrr]=$resultArys ;

                                                                        $finalResultAry[$countrr]['dtWhsCutOff'] = $tempResultAry['dtWhsCutOff'];
                                                                        $finalResultAry[$countrr]['dtWhsCutOff_time'] = $tempResultAry['dtWhsCutOff_format'];
                                                                        $finalResultAry[$countrr]['dtWhsAvailable'] = $tempResultAry['dtWhsAvailable'];
                                                                        $finalResultAry[$countrr]['dtWhsAvailable_formated'] = $tempResultAry['dtWhsAvailable_time'];

                                                                        $finalResultAry[$countrr]['dtAvailableDate'] = $tempResultAry['dtAvailableDate'];
                                                                        $finalResultAry[$countrr]['dtAvailabeleDate_formated'] = $tempResultAry['dtAvailabeleDate_formated'];
                                                                        $finalResultAry[$countrr]['dtCutOffDate'] = $tempResultAry['dtCutOffDate'];
                                                                        $finalResultAry[$countrr]['dtCuttOffDate_formated'] = $tempResultAry['dtCuttOffDate_formated'];
                                                                        $finalResultAry[$countrr]['iCuttOffDate_time'] = $tempResultAry['iCuttOffDate_time'];
                                                                        $finalResultAry[$countrr]['iAvailabeleDate_time'] = $tempResultAry['iAvailabeleDate_time'];
                                                                        $finalResultAry[$countrr]['iAvailabeleDate_formated'] = $tempResultAry['iAvailabeleDate_formated'];
                                                                        $finalResultAry[$countrr]['unique_id'] = $tempResultAry['iCuttOffDate_time'].'_'.$resultArys['idWTW'].'_'.$tempResultAry['iAvailabeleDate_time'];
                                                                        $countrr++;
                                                                        //print_r($finalResultAry);
                                                                    }
								}
								else
								{
									$finalResultAry[$countrr]=$resultArys ;
									$countrr++;
								}
							}	
						}
					}
					$final_ary = sortArray($finalResultAry,'fTotalPriceCustomerCurrency');  // sorting array ASC to fTotalPriceCustomerCurrency 
					/*
					$filename = __APP_PATH_ROOT__."/logs/recalculate_price.log";
					
					$strdata=array();
					$strdata[0]=" \n\n final result array\n".print_R($final_ary,true)."\n";
					file_log(true, $strdata, $filename);
					*/
					$iSearchResultLimit =(int) $this->getManageMentVariableByDescription('__TOTAL_NUM_SEARCH_RESULT__');
					
					/*
					 * we are showing max 5 record of a forwarder.
					 * so suppose forwarder X have 15 relevant result but we only show 5 of them. 
					 */
					$final_sliced_ary = $this->sliceArrayByForwarder($final_ary);
                                        $res_ary = $final_sliced_ary; 
                                        $final_ary_to_return = $res_ary ;
                                        
					/*
					$strdata=array();
					$strdata[0]=" \n\n final sliced result array\n".print_R($final_sliced_ary,true)."\n";
					file_log(true, $strdata, $filename);
					*/
                                        /*
					$countrr = count($final_sliced_ary);
					if($iSearchResultLimit > ($countrr))
					{
                                            $iSearchResultLimit = $countrr ;
					}
					
					if(!empty($final_sliced_ary))
					{
                                            $res_ary = array_slice($final_sliced_ary,0,$iSearchResultLimit); 
                                            $final_ary_to_return = $res_ary ;
					}
					else
					{
                                            return array();
					}
                                         */
					$final_ary=array();
					$final_ary = sortArray($res_ary,'fDisplayPrice');								
					
					$this->fMinPriceForSlider = $final_ary[0]['fDisplayPrice'];
					
					$this->fMaxPriceForSlider = $final_ary[$iSearchResultLimit-1]['fDisplayPrice'];
					
					// sorting resulting array according to Transit hour to get min and max transit hours 
					$final_ary=array();
					$final_ary = sortArray($res_ary,'iTransitHour');					
					$this->fMinTransitHourForSlider = $final_ary[0]['iTransitHour'];
					$this->fMaxTransitHourForSlider = $final_ary[$iSearchResultLimit-1]['iTransitHour'];
                        
					if($data['idTimingType']==1)
					{					
                                            $final_ary=array();					
                                            $final_ary = sortArray($res_ary,'iCuttOffDate_time');					
                                            $this->fMinCutOffForSlider = $final_ary[0]['iCuttOffDate_time'];
                                            $this->fMaxCutOffForSlider = $final_ary[$iSearchResultLimit-1]['iCuttOffDate_time'];
					}
					else
					{
                                            $final_ary=array();					
                                            $final_ary = sortArray($res_ary,'iAvailabeleDate_time');					
                                            $this->fMinCutOffForSlider = $final_ary[0]['iAvailabeleDate_time'];
                                            $this->fMaxCutOffForSlider = $final_ary[$iSearchResultLimit-1]['iAvailabeleDate_time'];
					}
					
					if(!empty($res_ary))
					{
                                            $counter=0;
                                            $forwarderRatingAry = array();
                                            $ratingReviewAry=array();
                                            $forwarderReviewsAry=array();
                                            foreach($final_ary_to_return as $res_arys)
                                            {							
                                                if(!empty($this->forwarderAry) && array_key_exists($res_arys['idForwarder'],$this->forwarderAry))
                                                {
                                                    $final_ary_to_return[$counter]['fAvgRating'] = $forwarderRatingAry[$res_arys['idForwarder']]['fAvgRating'];
                                                    $final_ary_to_return[$counter]['iNumRating'] = $forwarderRatingAry[$res_arys['idForwarder']]['iNumRating'];
                                                    $final_ary_to_return[$counter]['iNumReview'] = $forwarderReviewsAry[$res_arys['idForwarder']]['iNumReview'];
                                                    if($res_arys['idForwarder']==5)
                                                    {
                                                         //print_r($forwarderRatingAry);
                                                    }
                                                    $ifCtr++;
                                                }
                                                else
                                                {
                                                    $forwarderRatingAry[$res_arys['idForwarder']] = $this->getForwarderAvgRating($res_arys['idForwarder']);
                                                    $forwarderReviewsAry[$res_arys['idForwarder']] = $this->getForwarderAvgReviews($res_arys['idForwarder']);

                                                    $final_ary_to_return[$counter]['fAvgRating'] = $forwarderRatingAry[$res_arys['idForwarder']]['fAvgRating'];
                                                    $final_ary_to_return[$counter]['iNumRating'] = $forwarderRatingAry[$res_arys['idForwarder']]['iNumRating'];
                                                    $final_ary_to_return[$counter]['iNumReview'] = $forwarderReviewsAry[$res_arys['idForwarder']]['iNumReview'];
                                                    if($res_arys['idForwarder']==5)
                                                    {
                                                            //print_r($forwarderRatingAry);
                                                    }
                                                    $elseCtr++;
                                                }
                                                $this->forwarderAry[$res_arys['idForwarder']] = $res_arys['szDisplayName'] ;								
                                                $counter++;							
                                            }
					}
					// sorting resulting array according to Avarage rating to get min and max transit hours 
					$final_ary=array();
					$final_ary = sortArray($final_ary_to_return,'fAvgRating');					
					$this->fMinAvgRatingForSlider = $final_ary[0]['fAvgRating'];
					$this->fMaxAvgRatingForSlider = $final_ary[$iSearchResultLimit-1]['fAvgRating'];
				}
				$final_resulting_arr=array();
				$tempAry = array(); 
				if(!empty($final_ary_to_return))
				{
                                    foreach ($final_ary_to_return as $final_ary_to_returns) 
                                    {
                                        $start=date("Y-m-d",strtotime(str_replace("/","-",trim($final_ary_to_returns['dtCutOffDate']))));
                                        $end=date("Y-m-d",strtotime(str_replace("/","-",trim($final_ary_to_returns['dtAvailableDate']))));
                                        $days_between = ceil(abs(strtotime($end) - strtotime($start)) / 86400);
                                        
                                        $final_ary_to_returns['iDaysBetween'] = $days_between ;
                                        
                                        $unique_id = $final_ary_to_returns['unique_id'];						
                                        if (!empty($tempAry) && !in_array($unique_id,$tempAry))
                                        {
                                            array_push($final_resulting_arr,$final_ary_to_returns);
                                            $tempAry[]=$unique_id ; 
                                        }
                                        else if(empty($tempAry))
                                        {
                                            array_push($final_resulting_arr,$final_ary_to_returns);
                                            $tempAry[]=$unique_id ; 
                                        }
                                    }
				}  
                    }
                        
                    $idCustomerCurrencye = $postSearchAry['idCustomerCurrency'] ;
                    $courierServiceNewArr = array();
                    $courierServiceNewArr=$this->getCourierProviderData($postSearchAry,$idCustomerCurrencye);
                        
                    $this->szCourierCalculationLogString = $this->szCalculationLogString;
                    if(!empty($final_resulting_arr) && !empty($courierServiceNewArr))
                    {
                        $iSearchResultType = 1; //Both LCL and Courier Options are available
                        $final_resulting_arr=array_merge($final_resulting_arr,$courierServiceNewArr);
                    }
                    else if(!empty($courierServiceNewArr) && empty($final_resulting_arr))
                    {
                        $iSearchResultType = 3; //Only Courier Options are available
                        $final_resulting_arr = $courierServiceNewArr;
                    }
                    else if(!empty($final_resulting_arr))
                    {
                        $iSearchResultType = 2; //Only LCL Options are available
                    } 
                    if($mode=='FORWARDER_TRY_IT_OUT')
                    {	
                        // adding resulting data into temperary table 
                        $this->addTempSearchData_forwarder($final_resulting_arr);
                        $forwarder_try_ret_ary = array();
                        if(!empty($final_resulting_arr))
                        {
                            $ctrr=0;
                            foreach($final_resulting_arr as $final_resulting_arrs)
                            {
                                if($final_resulting_arrs['idForwarder'] == $data['forwarder_id'])
                                {
                                    $forwarder_try_ret_ary[$ctrr] = $final_resulting_arrs ;
                                    $ctrr++;
                                }
                            }
                        }
                        return $forwarder_try_ret_ary;
                    }				 
                    else if($mode=='MANAGEMENT_TRY_IT_OUT')
                    {
                        // adding resulting data into temperary table 
                        $this->addTempSearchData_forwarder($final_resulting_arr);
                        return $final_resulting_arr;
                    }
                    else if($mode!='RECALCULATE_PRICING')
                    {   
                        // adding resulting data into temperary table 
                        $this->addTempSearchData($final_resulting_arr,$idBooking,$iSearchResultType);
                    }
                    return $final_resulting_arr ;
		}		
	}