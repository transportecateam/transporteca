<?php
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH_CLASSES__ . "/customerLogs.class.php");
/*
 *This class file contains all reporting related stuff
 *@author: Ajay Jha
 *@access: Public
 * */

class cReport extends cDatabase
{
	function __construct()
	{
            parent::__construct();
            return true;
	}
	
	function getVisuaalizationReport($data=array(),$return_array_flag = false)
	{
		/*
		 	Booking steps description:
		 	
			1. clickked on compare button
			2. Trade available	
			3. Complete service step
			4. Complete location step
			5. Complete timing step	
			6. Complete cargo step	
			7. Complete currency selection
			8. Service options displayed
			9. Click BOOK	tblbooking
			10. Submit e-mail	
			11. Complete booking details
			12. Confirm booking	tblbooking
			13. Sent to payment gateway	tblbooking
			14. Complete payment
			
			
			My assumption while writing this function is if booking is higher step then it must went through lower stepe i.e
			
			if iBookingStep = 5 then this booking must have 1 to 4 steps too.
		 */
		
		if(!empty($data))
		{
			if(!empty($data['dtFromDate']))
			{
				$dtFromDateTime = format_date($data['dtFromDate']);
				if(!empty($dtFromDateTime))
				{
					$query_and .= " AND DATE(dtBookingInitiated)>= '".mysql_escape_custom(trim($dtFromDateTime))."'";
				}
			}
			
			if(!empty($data['dtToDate']))
			{
				$dtToDateTime = format_date($data['dtToDate']);
				if(!empty($dtToDateTime))
				{
					$query_and .= " AND DATE(dtBookingInitiated) <= '".mysql_escape_custom(trim($dtToDateTime))."'";
				}
			}
			
			if((int)$data['idOriginCountry']>0)
			{
				$query_and .= " AND idOriginCountry = '".(int)$data['idOriginCountry']."'";
			}
			
			if((int)$data['idDestinationCountry']>0)
			{
				$query_and .= " AND idDestinationCountry = '".(int)$data['idDestinationCountry']."'";
			}
		}
		
		$query="
			SELECT
				id,
				iBookingStep
			FROM
				".__DBC_SCHEMATA_BOOKING__."
			WHERE
				iBookingStep > 0
			$query_and
		";
		//echo "<br /> ".$query."<br /> ";
		if($result=$this->exeSQL($query))
		{
			$icountStep_1 = 0;
			$icountStep_2 = 0;
			$icountStep_3 = 0;
			$icountStep_4 = 0;
			$icountStep_5 = 0;
			$icountStep_6 = 0;
			$icountStep_7 = 0;
			$icountStep_8 = 0;
			$icountStep_9 = 0;
			$icountStep_10 = 0;
			$icountStep_11 = 0;
			$icountStep_12 = 0;
			$icountStep_13 = 0;
			$icountStep_14 = 0;
			
			while($row=$this->getAssoc($result))
			{
				if($row['iBookingStep']==14)
				{
					$icountStep_1++;
					$icountStep_2++;
					$icountStep_3++;
					$icountStep_4++;
					$icountStep_5++;
					$icountStep_6++;
					$icountStep_7++;
					$icountStep_8++;
					$icountStep_9++;
					$icountStep_10++;
					$icountStep_11++;
					$icountStep_12++;
					$icountStep_13++;
					$icountStep_14++;
				}
				else if($row['iBookingStep']==13)
				{
					$icountStep_1++;
					$icountStep_2++;
					$icountStep_3++;
					$icountStep_4++;
					$icountStep_5++;
					$icountStep_6++;
					$icountStep_7++;
					$icountStep_8++;
					$icountStep_9++;
					$icountStep_10++;
					$icountStep_11++;
					$icountStep_12++;
					$icountStep_13++;
				}
				else if($row['iBookingStep']==12)
				{
					$icountStep_1++;
					$icountStep_2++;
					$icountStep_3++;
					$icountStep_4++;
					$icountStep_5++;
					$icountStep_6++;
					$icountStep_7++;
					$icountStep_8++;
					$icountStep_9++;
					$icountStep_10++;
					$icountStep_11++;
					$icountStep_12++;
				}
				else if($row['iBookingStep']==11)
				{
					$icountStep_1++;
					$icountStep_2++;
					$icountStep_3++;
					$icountStep_4++;
					$icountStep_5++;
					$icountStep_6++;
					$icountStep_7++;
					$icountStep_8++;
					$icountStep_9++;
					$icountStep_10++;
					$icountStep_11++;
				}
				else if($row['iBookingStep']==10)
				{
					$icountStep_1++;
					$icountStep_2++;
					$icountStep_3++;
					$icountStep_4++;
					$icountStep_5++;
					$icountStep_6++;
					$icountStep_7++;
					$icountStep_8++;
					$icountStep_9++;
					$icountStep_10++;
				}
				else if($row['iBookingStep']==9)
				{
					$icountStep_1++;
					$icountStep_2++;
					$icountStep_3++;
					$icountStep_4++;
					$icountStep_5++;
					$icountStep_6++;
					$icountStep_7++;
					$icountStep_8++;
					$icountStep_9++;
				}
				else if($row['iBookingStep']==8)
				{
					$icountStep_1++;
					$icountStep_2++;
					$icountStep_3++;
					$icountStep_4++;
					$icountStep_5++;
					$icountStep_6++;
					$icountStep_7++;
					$icountStep_8++;
				}
				else if($row['iBookingStep']==7)
				{
					$icountStep_1++;
					$icountStep_2++;
					$icountStep_3++;
					$icountStep_4++;
					$icountStep_5++;
					$icountStep_6++;
					$icountStep_7++;
				}
				else if($row['iBookingStep']==6)
				{
					$icountStep_1++;
					$icountStep_2++;
					$icountStep_3++;
					$icountStep_4++;
					$icountStep_5++;
					$icountStep_6++;
				}
				else if($row['iBookingStep']==5)
				{
					$icountStep_1++;
					$icountStep_2++;
					$icountStep_3++;
					$icountStep_4++;
					$icountStep_5++;
				}
				else if($row['iBookingStep']==4)
				{
					$icountStep_1++;
					$icountStep_2++;
					$icountStep_3++;
					$icountStep_4++;
				}
				else if($row['iBookingStep']==3)
				{
					$icountStep_1++;
					$icountStep_2++;
					$icountStep_3++;
				}
				else if($row['iBookingStep']==2)
				{
					$icountStep_1++;
					$icountStep_2++;
				}
				else if($row['iBookingStep']==1)
				{
					$icountStep_1++;
				}
			}
			
			$icountStep_1 = (int)$this->getStep1Count($data);
			$ret_ary=array();
			$ret_ary['icountStep'][1] = $icountStep_1 ;
			$ret_ary['icountStep'][2] = $icountStep_2 ;
			$ret_ary['icountStep'][3] = $icountStep_3 ;
			$ret_ary['icountStep'][4] = $icountStep_4 ;
			$ret_ary['icountStep'][5] = $icountStep_5 ;
			$ret_ary['icountStep'][6] = $icountStep_6 ;
			$ret_ary['icountStep'][7] = $icountStep_7 ;
			$ret_ary['icountStep'][8] = $icountStep_8 ;
			$ret_ary['icountStep'][9] = $icountStep_9 ;
			$ret_ary['icountStep'][10] = $icountStep_10 ;
			$ret_ary['icountStep'][11] = $icountStep_11 ;
			$ret_ary['icountStep'][12] = $icountStep_12 ;
			$ret_ary['icountStep'][13] = $icountStep_13 ;
			$ret_ary['icountStep'][14] = $icountStep_14 ;
			
			//calculation percentage.
			if($icountStep_1>0)
			{
				$ret_ary['fPercentageStep'][1] = 100 ;
			}
			else
			{
				$ret_ary['fPercentageStep'][1] = 0 ;
			}
			
			if($icountStep_1>0)
			{
				$ret_ary['fPercentageStep'][2] = ($icountStep_2 * 100)/$icountStep_1 ;
			}
			else
			{
				$ret_ary['fPercentageStep'][2] = 0;
			}
			
			if($icountStep_1>0)
			{
				$ret_ary['fPercentageStep'][3] = ($icountStep_3 * 100)/$icountStep_1 ;
			}
			else
			{
				$ret_ary['fPercentageStep'][3] = 0;
			}
			
			if($icountStep_1>0)
			{
				$ret_ary['fPercentageStep'][4] = ($icountStep_4 * 100)/$icountStep_1 ;
			}
			else
			{
				$ret_ary['fPercentageStep'][4] = 0;
			}
			
			if($icountStep_1>0)
			{
				$ret_ary['fPercentageStep'][5] = ($icountStep_5 * 100)/$icountStep_1 ;
			}
			else
			{
				$ret_ary['fPercentageStep'][5] = 0;
			}
			
			if($icountStep_1>0)
			{
				$ret_ary['fPercentageStep'][6] = ($icountStep_6 * 100)/$icountStep_1 ;
			}
			else
			{
				$ret_ary['fPercentageStep'][6] = 0;
			}
			
			if($icountStep_1>0)
			{
				$ret_ary['fPercentageStep'][7] = ($icountStep_7 * 100)/$icountStep_1 ;
			}
			else
			{
				$ret_ary['fPercentageStep'][7] = 0;
			}
			if($icountStep_1>0)
			{
				$ret_ary['fPercentageStep'][8] = ($icountStep_8 * 100)/$icountStep_1 ;
			}
			else
			{
				$ret_ary['fPercentageStep'][8] = 0;
			}
			
			if($icountStep_1>0)
			{
				$ret_ary['fPercentageStep'][9] = ($icountStep_9 * 100)/$icountStep_1 ;
			}
			else
			{
				$ret_ary['fPercentageStep'][9] = 0;
			}
			
			if($icountStep_1>0)
			{
				$ret_ary['fPercentageStep'][10] = ($icountStep_10 * 100)/$icountStep_1 ;
			}
			else
			{
				$ret_ary['fPercentageStep'][10] = 0;
			}
			
			if($icountStep_1>0)
			{
				$ret_ary['fPercentageStep'][11] = ($icountStep_11 * 100)/$icountStep_1 ;
			}
			else
			{
				$ret_ary['fPercentageStep'][11] = 0;
			}
			if($icountStep_1>0)
			{
				$ret_ary['fPercentageStep'][12] = ($icountStep_12 * 100)/$icountStep_1 ;
			}
			else
			{
				$ret_ary['fPercentageStep'][12] = 0;
			}
			if($icountStep_1>0)
			{
				$ret_ary['fPercentageStep'][13] = ($icountStep_13 * 100)/$icountStep_1 ;
			}
			else
			{
				$ret_ary['fPercentageStep'][13] = 0;
			}
			
			if($icountStep_1>0)
			{
				$ret_ary['fPercentageStep'][14] = ($icountStep_14 * 100)/$icountStep_1 ;
			}
			else
			{
				$ret_ary['fPercentageStep'][14] = 0;
			}
			
			if($return_array_flag)
			{
				return $ret_ary ;
			}
			else
			{
				$szImageName = $this->createVisualizationGraphImage($ret_ary);
				return $szImageName ;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	function createVisualizationGraphImage($data)
	{
		# ------- The graph values in the form of associative array
		
		$img_width=700;
		$img_height=400;
		$margins=30;
		
		
		# ---- Find the size of graph by substracting the size of borders
		//$graph_width=$img_width - $margins * 2;
		$graph_width = $img_width ;
		$graph_height=$img_height - $margins * 2;
		$img=imagecreate($img_width,$img_height);
		
		 
		$bar_width=30;
		$total_bars=count($data['icountStep']);
		$gap= ($graph_width- $total_bars * $bar_width ) / ($total_bars +1);
		$gap = 15;
		//echo $gap ;
		
		# -------  Define Colors ----------------
		$bar_color=imagecolorallocatealpha($img,179,179,152,5);
		$border_color=imagecolorallocate($img,255,255,255);
		$base_line= imagecolorallocatealpha($img,125,116,104,5);
		$base_line_string = imagecolorallocatealpha($img,0,0,0,5);
		# ------ Create the border around the graph ------
		imagesetthickness($img, 2);
		imagefilledrectangle($img,0,0,$img_width,$img_height,$border_color);
		
		
		# ------- Max value is required to adjust the scale	-------
		$max_value=max($data['icountStep']) + 10;
		$ratio= $graph_height/$max_value;
		imageline ( $img ,20,15, 20 , 370, $base_line);
		
		imageline ( $img ,20,15, 25 , 25, $base_line);
		imageline ( $img ,20,15, 15 , 25, $base_line);
		
		imageline ( $img ,680,370, 20 , 370, $base_line);
		
		imageline ( $img ,680,370, 670 , 365, $base_line);
		imageline ( $img ,680,370, 670 , 375, $base_line);
		
		# ----------- Draw the bars here ------
		//$font = 'arial.ttf';
		$margins2= 20 ;
		$font_file_name = __APP_PATH__.'/fonts/font.ttf';
		$font_file_name2 = __APP_PATH__.'/fonts/Calibri.ttf';
		$graph_counter = 1;
		for($i=0;$i< $total_bars; $i++)
		{
			# ------ Extract key and value pair from the current pointer position
			$icountStep = round((int)$data['icountStep'][$graph_counter]);
			$fPercentageStep = round((float)$data['fPercentageStep'][$graph_counter],2);
			
			$x1= $margins2 + $gap + $i * ($gap+$bar_width) ;
			$x2= $x1 + $bar_width;
			$y1=$margins +$graph_height- intval($icountStep * $ratio) ;
			$y2=$img_height-$margins;
				
			imagettftext($img, 10, 0, $x1+15, $y1-5, $base_line_string, $font_file_name, number_format($icountStep));
			imagettftext($img, 10, 0, $x1+10, $img_height-15, $base_line_string, $font_file_name2, $graph_counter.".");
			imagettftext($img, 10, 0, $x1+3, $img_height, $base_line_string, $font_file_name2, $fPercentageStep);
			
			imagefilledrectangle($img,$x1,$y1,$x2,$y2,$base_line);
			$graph_counter++;
		}
		
		$id = mt_rand(0,9999);
		imagepng($img,"graph/images_".$id.".png");
		imagedestroy($img);
		
		return "graph/images_".$id.".png";
	}
	
	function getUniqueUserCount($data)
	{
		if(!empty($data))
		{
			$query_where = " WHERE " ;
			if(!empty($data['dtFromDate']))
			{
				$dtFromDateTime = format_date($data['dtFromDate']);
				if(!empty($dtFromDateTime))
				{
					$query_and = $query_where . " DATE(dtDateTime)>= '".mysql_escape_custom(trim($dtFromDateTime))."'";
				}
			}
				
			if(!empty($data['dtToDate']))
			{
				$dtToDateTime = format_date($data['dtToDate']);
				if(!empty($dtToDateTime))
				{
					if(!empty($query_and))
					{
						$query_and .= " AND DATE(dtDateTime) <= '".mysql_escape_custom(trim($dtToDateTime))."'";
					}
					else {
						$query_and = $query_where . " DATE(dtDateTime) <= '".mysql_escape_custom(trim($dtToDateTime))."'";
					}
					
				}
			}
				
			if((int)$data['idOriginCountry']>0)
			{
				if(!empty($query_and))
				{
					$query_and .= " AND idOriginCountry = '".(int)$data['idOriginCountry']."'";
				}
				else 
				{
					$query_and =$query_where . " idOriginCountry = '".(int)$data['idOriginCountry']."'";
				}
			}
				
			if((int)$data['idDestinationCountry']>0)
			{
				if(!empty($query_and))
				{
					$query_and .= " AND idDestinationCountry = '".(int)$data['idDestinationCountry']."'";
				}
				else
				{
					$query_and .= $query_where . " idDestinationCountry = '".(int)$data['idDestinationCountry']."'";
				}
			}
		}
		
		$query="
			SELECT
				COUNT(DISTINCT(szBrowserDetails)) iUniqueUserCount
			FROM
				".__DBC_SCHEMATA_COMPARE_BOOKING__."
			$query_and
		";
		//echo "<br /> ".$query."<br /> ";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$row = $this->getAssoc($result);
				return $row['iUniqueUserCount'] ;
			}
			else
			{
				return false;
			}	
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	function getStep1Count($data)
	{
		if(!empty($data))
		{
			$query_where = " WHERE " ;
			if(!empty($data['dtFromDate']))
			{
				$dtFromDateTime = format_date($data['dtFromDate']);
				if(!empty($dtFromDateTime))
				{
					$query_and = $query_where . " DATE(dtDateTime)>= '".mysql_escape_custom(trim($dtFromDateTime))."'";
				}
			}
	
			if(!empty($data['dtToDate']))
			{
				$dtToDateTime = format_date($data['dtToDate']);
				if(!empty($dtToDateTime))
				{
					if(!empty($query_and))
					{
						$query_and .= " AND DATE(dtDateTime) <= '".mysql_escape_custom(trim($dtToDateTime))."'";
					}
					else {
						$query_and = $query_where . " DATE(dtDateTime) <= '".mysql_escape_custom(trim($dtToDateTime))."'";
					}
						
				}
			}
	
			if((int)$data['idOriginCountry']>0)
			{
				if(!empty($query_and))
				{
					$query_and .= " AND idOriginCountry = '".(int)$data['idOriginCountry']."'";
				}
				else
				{
					$query_and =$query_where . " idOriginCountry = '".(int)$data['idOriginCountry']."'";
				}
			}
	
			if((int)$data['idDestinationCountry']>0)
			{
				if(!empty($query_and))
				{
					$query_and .= " AND idDestinationCountry = '".(int)$data['idDestinationCountry']."'";
				}
				else
				{
					$query_and .= $query_where . " idDestinationCountry = '".(int)$data['idDestinationCountry']."'";
				}
			}
		}
	
		$query="
			SELECT
				COUNT(id) iNumCount
			FROM
				".__DBC_SCHEMATA_COMPARE_BOOKING__."
					$query_and
					";
		//echo "<br /> ".$query."<br /> ";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$row = $this->getAssoc($result);
				return $row['iNumCount'] ;
			}
			else
			{
				return false;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	
	/*
	 * This function returns booking related report for last six months  
	 */
	
	function getMonthlyReport($iBookingStep=false)
	{
		$ret_ry = array();
		for($i=0;$i<6;$i++)
		{
			
			
			$currentMonthDate=date('Y-m-01');
			$newdate = strtotime ( "-".$i." MONTH" , strtotime ( $currentMonthDate ) ) ;
			
			$szMonthName = date('F',$newdate);
			
			$dtFirstDateOfMonth = date('01/m/Y',$newdate);
			$dtLastDateOfMonth = date('t/m/Y',$newdate);
			
			$inputDataAry = array();
			$inputDataAry['dtFromDate'] = $dtFirstDateOfMonth ;
			$inputDataAry['dtToDate'] = $dtLastDateOfMonth ;
			
			$monthlyDataAry = array();
			$monthlyDataAry = $this->getVisuaalizationReport($inputDataAry,true);
			
			if($iBookingStep>0)
			{
				$monthlyDataAry = $this->formatIndexedPercentage($monthlyDataAry,$iBookingStep);
			}
			$ret_ry[$szMonthName] = $monthlyDataAry ;
		}
		return $ret_ry ;
	}
	
	function formatIndexedPercentage($data,$iBookingStep)
	{
		if(!empty($data))			
		{
			$ret_ary = array();
			$loop_counter = count($data['fPercentageStep']);
			$icountStep = $data['icountStep'][$iBookingStep];
			
			for($i=1;$i<=$loop_counter;$i++)
			{
				if($i==$iBookingStep)
				{
					if($icountStep>0)
					{
						$ret_ary['fPercentageStep'][$i] = 100;
					}
					else 
					{
						$ret_ary['fPercentageStep'][$i] = 0;
					}
				}
				else
				{
					if($icountStep>0)
					{
						$icountStep_value = $data['icountStep'][$i];
						$ret_ary['fPercentageStep'][$i] = ($icountStep_value * 100)/$icountStep ;
					}
					else
					{
						$ret_ary['fPercentageStep'][$i] = 0;
					}
				}
			}
			return $ret_ary ;
		}
	}
	function getSellingTradeReport($data)
	{ 
            if(!empty($data))
            {
                $query_where = " AND " ;
                if(!empty($data['dtFromDate']))
                {
                    $dtFromDateTime = format_date($data['dtFromDate']);
                    if(!empty($dtFromDateTime))
                    {
                        $query_and = $query_where . " DATE(dtBookingConfirmed)>= '".mysql_escape_custom(trim($dtFromDateTime))."'";
                    }
                }

                if(!empty($data['dtToDate']))
                {
                    $dtToDateTime = format_date($data['dtToDate']);
                    if(!empty($dtToDateTime))
                    {
                        if(!empty($query_and))
                        {
                            $query_and .= " AND DATE(dtBookingConfirmed) <= '".mysql_escape_custom(trim($dtToDateTime))."'";
                        }
                        else 
                        {
                            $query_and = $query_where . " DATE(dtBookingConfirmed) <= '".mysql_escape_custom(trim($dtToDateTime))."'";
                        } 
                    }
                }

                if((int)$data['idOriginCountry']>0)
                {
                    if(!empty($query_and))
                    {
                        $query_and .= " AND idOriginCountry = '".(int)$data['idOriginCountry']."'";
                    }
                    else 
                    {
                        $query_and =$query_where . " idOriginCountry = '".(int)$data['idOriginCountry']."'";
                    }
                }

                if((int)$data['idDestinationCountry']>0)
                {
                    if(!empty($query_and))
                    {
                        $query_and .= " AND idDestinationCountry = '".(int)$data['idDestinationCountry']."'";
                    }
                    else
                    {
                        $query_and .= $query_where . " idDestinationCountry = '".(int)$data['idDestinationCountry']."'";
                    }
                }
            }
		
            $query="
                SELECT
                    id,
                    idOriginCountry,
                    idDestinationCountry, 
                    fTotalPriceUSD,
                    idServiceType,
                    fReferalAmount,
                    iBookingStep,
                    fReferalPercentage,
                    fReferalAmount,
                    fForwarderExchangeRate,
                    ( SELECT cont1.szCountryName FROM ".__DBC_SCHEMATA_COUNTRY__." cont1 WHERE cont1.id = b.idOriginCountry ) as szOriginCountry,
                    ( SELECT cont2.szCountryName FROM ".__DBC_SCHEMATA_COUNTRY__." cont2 WHERE cont2.id = b.idDestinationCountry ) as szDestinationCountry
                FROM
                    ".__DBC_SCHEMATA_BOOKING__." as b
                WHERE
                    idBookingStatus IN (".__BOOKING_STATUS_CONFIRMED__.",".__BOOKING_STATUS_ARCHIVED__.")
                    $query_and
            ";
            //echo "<br /> ".$query."<br /> ";
            if($result=$this->exeSQL($query))
            {
                    $ret_ary = array();
                    $iPTPCount = 0;
                    $iPTWCount = 0;
                    $iPTDCount = 0;
                    $iDTPCount = 0;
                    $iDTWCount = 0;
                    $iDTDCount = 0;
                    $iWTDCount = 0;
                    $iWTPCount = 0;
                    $iWTWCount = 0;
                    $fTotalTurnOver = 0;
                    $fTotalRevenue = 0;
                    $iBookingCounter = 0;

                    $tempSellingTradeAry = array();
                    while($row=$this->getAssoc($result))
                    {
                            $trade_key = $row['idOriginCountry']."_".$row['idDestinationCountry'] ;
                            if(!empty($tempSellingTradeAry) && array_key_exists($trade_key,$tempSellingTradeAry))
                            {
                                    if($row['idServiceType']==__SERVICE_TYPE_PTP__)
                                    {
                                            $ret_ary[$trade_key]['iPTPCount'] += 1 ;
                                    }
                                    else if($row['idServiceType']==__SERVICE_TYPE_PTW__)
                                    {
                                            $ret_ary[$trade_key]['iPTWCount'] += 1 ;
                                    }
                                    else if($row['idServiceType']==__SERVICE_TYPE_PTD__)
                                    {
                                            $ret_ary[$trade_key]['iPTDCount'] += 1 ;
                                    }
                                    else if($row['idServiceType']==__SERVICE_TYPE_DTP__)
                                    {
                                            $ret_ary[$trade_key]['iDTPCount'] += 1 ;
                                    }
                                    else if($row['idServiceType']==__SERVICE_TYPE_DTW__)
                                    {
                                            $ret_ary[$trade_key]['iDTWCount'] += 1 ;
                                    }
                                    else if($row['idServiceType']==__SERVICE_TYPE_DTD__)
                                    {
                                            $ret_ary[$trade_key]['iDTDCount'] += 1 ;
                                    }
                                    else if($row['idServiceType']==__SERVICE_TYPE_WTD__)
                                    {
                                            $ret_ary[$trade_key]['iWTDCount'] += 1 ;
                                    }
                                    else if($row['idServiceType']==__SERVICE_TYPE_WTP__)
                                    {
                                            $ret_ary[$trade_key]['iWTPCount'] += 1 ;
                                    }
                                    else if($row['idServiceType']==__SERVICE_TYPE_WTW__)
                                    {
                                            $ret_ary[$trade_key]['iWTWCount'] += 1 ;
                                    }

                                    if($row['fReferalPercentage']>0)
                                    {
                                            $fTotalRevenue = ($row['fReferalAmount'] * $row['fForwarderExchangeRate']);
                                            $ret_ary[$trade_key]['fTotalRevenue'] += round($fTotalRevenue,2);
                                    }

                                    $ret_ary[$trade_key]['fTotalTurnOver'] += $row['fTotalPriceUSD'];;
                                    $ret_ary[$trade_key]['iBookingCounter'] +=1 ;
                                    $ret_ary[$trade_key]['szTradeName'] = $row['szOriginCountry']." - ".$row['szDestinationCountry'];
                            }
                            else
                            {
                                if($row['idServiceType']==__SERVICE_TYPE_PTP__)
                                {
                                    $ret_ary[$trade_key]['iPTPCount'] += 1 ;
                                }
                                else if($row['idServiceType']==__SERVICE_TYPE_PTW__)
                                {
                                    $ret_ary[$trade_key]['iPTWCount'] += 1 ;
                                }
                                else if($row['idServiceType']==__SERVICE_TYPE_PTD__)
                                {
                                    $ret_ary[$trade_key]['iPTDCount'] += 1 ;
                                }
                                else if($row['idServiceType']==__SERVICE_TYPE_DTP__)
                                {
                                    $ret_ary[$trade_key]['iDTPCount'] += 1 ;
                                }
                                else if($row['idServiceType']==__SERVICE_TYPE_DTW__)
                                {
                                    $ret_ary[$trade_key]['iDTWCount'] += 1 ;
                                }
                                else if($row['idServiceType']==__SERVICE_TYPE_DTD__)
                                {
                                    $ret_ary[$trade_key]['iDTDCount'] += 1 ;
                                }
                                else if($row['idServiceType']==__SERVICE_TYPE_WTD__)
                                {
                                    $ret_ary[$trade_key]['iWTDCount'] += 1 ;
                                }
                                else if($row['idServiceType']==__SERVICE_TYPE_WTP__)
                                {
                                    $ret_ary[$trade_key]['iWTPCount'] += 1 ;
                                }
                                else if($row['idServiceType']==__SERVICE_TYPE_WTW__)
                                {
                                    $ret_ary[$trade_key]['iWTWCount'] += 1 ;
                                }

                                if($row['fReferalPercentage']>0)
                                {
                                    $fTotalRevenue = ($row['fReferalAmount'] * $row['fForwarderExchangeRate']);
                                    $ret_ary[$trade_key]['fTotalRevenue'] += round($fTotalRevenue,2);
                                }

                                $ret_ary[$trade_key]['fTotalTurnOver'] += $row['fTotalPriceUSD'];
                                $ret_ary[$trade_key]['iBookingCounter'] +=1 ;
                                $ret_ary[$trade_key]['szTradeName'] = $row['szOriginCountry']." - ".$row['szDestinationCountry'];
                            }
                            $tempSellingTradeAry[$trade_key] = $counter ;
                            $counter++;
                    }

                    /*
                    $ret_ary['iPTPCount'] = $iPTPCount;
                    $ret_ary['iPTWCount'] = $iPTWCount ;
                    $ret_ary['iPTDCount'] = $iPTDCount ;
                    $ret_ary['iDTPCount'] = $iDTPCount ;
                    $ret_ary['iDTWCount'] = $iDTWCount ;
                    $ret_ary['iDTDCount'] = $iDTDCount ;
                    $ret_ary['iWTDCount'] = $iWTDCount ;
                    $ret_ary['iWTPCount'] = $iWTPCount;
                    $ret_ary['iWTWCount'] = $iWTWCount ;
                    $ret_ary['fTotalRevenue'] = $fTotalRevenue ;
                    $ret_ary['fTotalTurnOver'] = $fTotalTurnOver ;
                    $ret_ary['iNumBooking'] = $iBookingCounter;
                    */
                    $final_sorted_ary=array();
                    if(!empty($ret_ary))
                    {
                        $final_sorted_ary = sortArray($ret_ary,'fTotalTurnOver','DESC');
                    }
                    return $final_sorted_ary ;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	/* This function do following things: 
	 * 
	 *  a.	Searches - 30 days: Count rows in tblcomparebutton last 30 days
	 *  b.	Searches - 90 days: (Count rows in tblcomparebutton last 90 days) divided by 3
	 *  c.	Searches - 360 days: (Count rows in tblcomparebutton last 365 days) divided by 365/30
            d.	Unique users column - same as above, but only count unique IP in the last 30 / 90 / 365 days
            e.	Conversion is number of confirmed bookings with created date (note: created date � no confirmed date) in last 90 days divided by the total number of searches (all searches, not unique) in the last 90 days multiplied by 100. Display this with 2 decimals and a % sign after (right after the number � no spacebar between)
            f.	Current available is Yes if this trade is currently listed in tblservices with iStatus=1 and No if it not
	*/
	
	function getAllSearchedTrade($sort_feild='iSearched_30_days',$sort_order=false)
	{
            $date_before_30days = strtotime("-30 days");
            $date_before_90days = strtotime("-90 days");
            $date_before_365days = strtotime("-365 days");
            $today_time = time();
                
            $query="
                SELECT 
                    b.id,
                    b.idOriginCountry,
                    b.idDestinationCountry
                FROM 
                    ".__DBC_SCHEMATA_BOOKING__." b 
                WHERE
                    idBookingStatus IN (".__BOOKING_STATUS_CONFIRMED__.",".__BOOKING_STATUS_ARCHIVED__.")
                AND
                    DATE(dtBookingConfirmed) >= '".mysql_escape_custom(date('Y-m-d',$date_before_90days))."'
                ";
                if($result=$this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        $ctr = 0;
                        while($row=$this->getAssoc($result))
                        {
                            $key=$row['idOriginCountry']."_".$row['idDestinationCountry'];
                            $resBookingNewArr[$key][$ctr]=1;
                            $count=count($resBookingNewArr[$key]);
                            $resBookingArr[$key]=$count;
                            ++$ctr;
                        }
                    }
                }
                
                $query="
                    SELECT 
                        s.iPTP,
                        s.idOriginCountry,
                        s.idDestinationCountry
                    FROM 
                        ".__DBC_SCHEMATA_SERVICE_AVAILABLE__." s
                    WHERE
                        s.dtValidTo > now()
                    AND
                        s.iStatus = '1'
                ";
                if($result=$this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        $outerCtr = 0;
                        while($row=$this->getAssoc($result))
                        {
                            $key=$row['idOriginCountry']."_".$row['idDestinationCountry'];
                            $count=count($resServiceArr[$key]);
                            $resServiceArr[$key]=$count+1;
                        }
                    }
                }

            $query="
                SELECT
                    cb.id,
                    cb.idOriginCountry,
                    cb.szOriginCountry,
                    cb.idDestinationCountry,
                    cb.szDestinationCountry,
                    cb.dtDateTime,
                    cb.szBrowserDetails
                FROM
                    ".__DBC_SCHEMATA_COMPARE_BOOKING__." cb
                WHERE
                    DATE(cb.dtDateTime) >= '".mysql_escape_custom(trim(date('Y-m-d',$date_before_365days)))."' 
            ";
            //echo "<br /> ".$query."<br /> ";
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $outerCtr = 0;
                    while($row=$this->getAssoc($result))
                    {
                        $trade_key = $row['idOriginCountry']."_".$row['idDestinationCountry'];
                        $dtDateTime = strtotime($row['dtDateTime']);
                        $iNumConfirmedBooking=$resBookingArr[$trade_key];
                        $iServiceAvailable=$resServiceArr[$trade_key];
                        if(!empty($tempSellingTradeAry) && array_key_exists($trade_key,$tempSellingTradeAry))
                        {  
                            //Count rows in tblcomparebutton last 30 days
                            if(($dtDateTime<=$today_time) && ($dtDateTime>=$date_before_30days))
                            {
                                $ret_ary[$trade_key]['iSearched_30_days'] += 1 ;
                            } 
                            //Count rows in tblcomparebutton last 90 days
                            if(($dtDateTime<=$today_time) && ($dtDateTime>=$date_before_90days))
                            {
                                $ret_ary[$trade_key]['iSearched_90_days'] += 1 ;
                            } 
                            //Count rows in tblcomparebutton last 90 days
                            if(($dtDateTime<=$today_time) && ($dtDateTime>=$date_before_365days))
                            {
                                $ret_ary[$trade_key]['iSearched_365_days'] += 1 ;
                            }

                            if($iServiceAvailable>=1)
                            {
                                $ret_ary[$trade_key]['iServiceAvailable'] = '1';
                            }
                            else
                            {
                                $ret_ary[$trade_key]['iServiceAvailable'] = '0';
                            } 

                            $szBrowserDetails = $row['szBrowserDetails'];

                            if((!empty($tempBrowswerDetailAry) && !$this->inMultiArray($szBrowserDetails,$tempBrowswerDetailAry[$trade_key])) || (empty($tempBrowswerDetailAry)))
                            {
                                //Count rows in tblcomparebutton last 30 days
                                if(($dtDateTime<=$today_time) && ($dtDateTime>=$date_before_30days))
                                {
                                    $ret_ary[$trade_key]['iUnique_user_30_days'] += 1 ;
                                }

                                //Count rows in tblcomparebutton last 90 days
                                if(($dtDateTime<=$today_time) && ($dtDateTime>=$date_before_90days))
                                {
                                    $ret_ary[$trade_key]['iUnique_user_90_days'] += 1 ;
                                }							
                                //Count rows in tblcomparebutton last 90 days
                                if(($dtDateTime<=$today_time) && ($dtDateTime>=$date_before_365days))
                                {
                                    $ret_ary[$trade_key]['iUnique_user_365_days'] += 1 ;
                                }
                                $tempBrowswerDetailAry[$trade_key][$szBrowserDetails] = $counter ;
                            }
                            //print_r($tempBrowswerDetailAry[$trade_key]);
                            $ret_ary[$trade_key]['szTradeName'] = $row['szOriginCountry']." - ".$row['szDestinationCountry'];

                            $ret_ary[$trade_key]['szOriginCountry'] = $row['szOriginCountry'];
                            $ret_ary[$trade_key]['szDestinationCountry'] = $row['szDestinationCountry'];
                            $ret_ary[$trade_key]['idOriginCountry'] = $row['idOriginCountry'];
                            $ret_ary[$trade_key]['idDestinationCountry'] = $row['idDestinationCountry'];

                            $ret_ary[$trade_key]['iNumConfirmedBooking'] = $iNumConfirmedBooking;
                        }
                        else 
                        {
                            $outerCtr++; 
                            //Count rows in tblcomparebutton last 30 days
                            if(($dtDateTime<=$today_time) && ($dtDateTime>=$date_before_30days))
                            {
                                $ret_ary[$trade_key]['iSearched_30_days'] = 1 ;
                            }

                            //Count rows in tblcomparebutton last 90 days
                            if(($dtDateTime<=$today_time) && ($dtDateTime>=$date_before_90days))
                            {
                                $ret_ary[$trade_key]['iSearched_90_days'] = 1 ;
                            }

                            //Count rows in tblcomparebutton last 90 days
                            if(($dtDateTime<=$today_time) && ($dtDateTime>=$date_before_365days))
                            {
                                $ret_ary[$trade_key]['iSearched_365_days'] = 1 ;
                            } 
                            if($iServiceAvailable>=1)
                            {
                                $ret_ary[$trade_key]['iServiceAvailable'] = 'Yes';
                            }
                            else
                            {
                                $ret_ary[$trade_key]['iServiceAvailable'] = 'No';
                            } 
                            $szBrowserDetails = $row['szBrowserDetails'];

                            //Count rows in tblcomparebutton last 30 days
                            if(($dtDateTime<=$today_time) && ($dtDateTime>=$date_before_30days))
                            {
                                $ret_ary[$trade_key]['iUnique_user_30_days'] += 1 ;
                            }

                            //Count rows in tblcomparebutton last 90 days
                            if(($dtDateTime<=$today_time) && ($dtDateTime>=$date_before_90days))
                            {
                                $ret_ary[$trade_key]['iUnique_user_90_days'] += 1 ;
                            }							
                            //Count rows in tblcomparebutton last 90 days
                            if(($dtDateTime<=$today_time) && ($dtDateTime>=$date_before_365days))
                            {
                                $ret_ary[$trade_key]['iUnique_user_365_days'] += 1 ;
                            }
                            $tempBrowswerDetailAry[$trade_key][$szBrowserDetails] = $counter ; 

                            $ret_ary[$trade_key]['szTradeName'] = $row['szOriginCountry']." - ".$row['szDestinationCountry'];
                            $ret_ary[$trade_key]['iNumConfirmedBooking'] = $iNumConfirmedBooking;

                            $ret_ary[$trade_key]['szOriginCountry'] = $row['szOriginCountry'];
                            $ret_ary[$trade_key]['szDestinationCountry'] = $row['szDestinationCountry'];
                            
                            $ret_ary[$trade_key]['idOriginCountry'] = $row['idOriginCountry'];
                            $ret_ary[$trade_key]['idDestinationCountry'] = $row['idDestinationCountry'];
                        }					
                        $tempSellingTradeAry[$trade_key] = $counter ; 
                        $counter++;
                    }
                    //print_r($ret_ary);
                    if(!empty($ret_ary))
                    {
                        foreach($ret_ary as $key=>$ret_arys)
                        {
                            if($ret_arys['iSearched_90_days']>0)
                            {
                                $fConversionRate = round((float)(($ret_arys['iNumConfirmedBooking']/$ret_arys['iSearched_90_days'])*100),2);
                            }
                            else
                            {
                                $fConversionRate = '0.00';
                            }
                            $ret_ary[$key]['fConversionRate'] = $fConversionRate ;
                            $ret_ary[$key]['iSearched_90_days'] = (int)($ret_arys['iSearched_90_days']/3 );
                            $ret_ary[$key]['iSearched_365_days'] = (int)($ret_arys['iSearched_365_days']/12.167 );

                            $ret_ary[$key]['iUnique_user_90_days'] = (int)($ret_arys['iUnique_user_90_days']/3 );
                            $ret_ary[$key]['iUnique_user_365_days'] = (int)($ret_arys['iUnique_user_365_days']/12.167 );
                        }
                    }

                    if($sort_feild=='szTradeName')
                    {
                        if($sort_order=='ORIGIN')
                        {
                            $sort_feild = 'szOriginCountry';
                        }
                        else
                        {
                            $sort_feild = 'szDestinationCountry';
                        }
                        $final_sorted_array = array();
                        $final_sorted_array = sortArray($ret_ary,$sort_feild);
                    }
                    else
                    {
                        $final_sorted_array = array();
                        $final_sorted_array = sortArray($ret_ary,$sort_feild,'DESC');
                    }
                    return $final_sorted_array ;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
	
	function getNotificationCount($data)
	{
		if(!empty($data))
		{
			$query_where = " AND " ;
			if(!empty($data['dtFromDate']))
			{
				$dtFromDateTime = format_date($data['dtFromDate']);
				if(!empty($dtFromDateTime))
				{
					$query_and = $query_where . " DATE(dtCreatedOn)>= '".mysql_escape_custom(trim($dtFromDateTime))."'";
				}
			}
			if(!empty($data['dtToDate']))
			{
				$dtToDateTime = format_date($data['dtToDate']);
				if(!empty($dtToDateTime))
				{
					if(!empty($query_and))
					{
						$query_and .= " AND DATE(dtCreatedOn) <= '".mysql_escape_custom(trim($dtToDateTime))."'";
					}
					else {
						$query_and = $query_where . " DATE(dtDateTime) <= '".mysql_escape_custom(trim($dtToDateTime))."'";
					}
						
				}
			}
	
			if((int)$data['idOriginCountry']>0)
			{
				if(!empty($query_and))
				{
					$query_and .= " AND idOriginCountry = '".(int)$data['idOriginCountry']."'";
				}
				else
				{
					$query_and =$query_where . " idOriginCountry = '".(int)$data['idOriginCountry']."'";
				}
			}
	
			if((int)$data['idDestinationCountry']>0)
			{
				if(!empty($query_and))
				{
					$query_and .= " AND idDestinationCountry = '".(int)$data['idDestinationCountry']."'";
				}
				else
				{
					$query_and .= $query_where . " idDestinationCountry = '".(int)$data['idDestinationCountry']."'";
				}
			}
		}
	
		$query="
			SELECT
				( SELECT COUNT(n1.id) FROM ".__DBC_SCHEMATA_NOTIFICATION__." n1 WHERE idNotificationType = '".__NOTIFICATION_TYPE_NO_SERVICE_COUNTRY_TO_COUNTRY___."' $query_and ) iCountNotificationType_1,
				( SELECT COUNT(n2.id) FROM ".__DBC_SCHEMATA_NOTIFICATION__." n2 WHERE idNotificationType IN ('".__NOTIFICATION_TYPE_NO_DELIVERY_TO_DESTINATION_COUNTRY___."','".__NOTIFICATION_TYPE_NO_PICKUP_FROM_ORIGIN_COUNTRY___."','".__NOTIFICATION_TYPE_NO_PICKUP_AND_NO_DELIVERY___."' ) $query_and ) iCountNotificationType_2_3_4,
				( SELECT COUNT(n3.id) FROM ".__DBC_SCHEMATA_NOTIFICATION__." n3 WHERE idNotificationType = '".__NOTIFICATION_TYPE_RP1_RP2___."' $query_and ) iCountNotificationType_5,
				( SELECT COUNT(n4.id) FROM ".__DBC_SCHEMATA_NOTIFICATION__." n4 WHERE idNotificationType IN ('".__NOTIFICATION_TYPE_S1_S2___."','".__NOTIFICATION_TYPE_T1_T2___."') $query_and ) iCountNotificationType_6_7
			FROM
				".__DBC_SCHEMATA_NOTIFICATION__."
			LIMIT 0,1
		";
		//echo "<br /> ".$query."<br /> ";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$row = $this->getAssoc($result);
				return $row;
			}
			else
			{
				return false;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	function getExpectedCargoNotificationCount($data)
	{
		if(!empty($data))
		{
			$query_where = " WHERE " ;
			if(!empty($data['dtFromDate']))
			{
				$dtFromDateTime = format_date($data['dtFromDate']);
				if(!empty($dtFromDateTime))
				{
					$query_and = $query_where . " DATE(dtCreatedOn)>= '".mysql_escape_custom(trim($dtFromDateTime))."'";
				}
			}
			if(!empty($data['dtToDate']))
			{
				$dtToDateTime = format_date($data['dtToDate']);
				if(!empty($dtToDateTime))
				{
					if(!empty($query_and))
					{
						$query_and .= " AND DATE(dtCreatedOn) <= '".mysql_escape_custom(trim($dtToDateTime))."'";
					}
					else {
						$query_and = $query_where . " DATE(dtDateTime) <= '".mysql_escape_custom(trim($dtToDateTime))."'";
					}
		
				}
			}
		
			if((int)$data['idOriginCountry']>0)
			{
				if(!empty($query_and))
				{
					$query_and .= " AND idOriginCountry = '".(int)$data['idOriginCountry']."'";
				}
				else
				{
					$query_and =$query_where . " idOriginCountry = '".(int)$data['idOriginCountry']."'";
				}
			}
		
			if((int)$data['idDestinationCountry']>0)
			{
				if(!empty($query_and))
				{
					$query_and .= " AND idDestinationCountry = '".(int)$data['idDestinationCountry']."'";
				}
				else
				{
					$query_and .= $query_where . " idDestinationCountry = '".(int)$data['idDestinationCountry']."'";
				}
			}
		}
		
		$query="
			SELECT
				count(id) iCountExpactedEacgo
			FROM
				".__DBC_SCHEMATA_EXPACTED_CARGO__."
				$query_and
		";
		
		//echo "<br /> ".$query."<br /> ";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$row = $this->getAssoc($result);
				return $row;
			}
			else
			{
				return false;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function inMultiArray($key,$array)
	{
		if($array[$key]>0)
		{
			return true;
		}
		else
		{
			return false;
		}
    }
    
    function addSearchedTradeSnapshot($data)
    {
        if(!empty($data))
        { 
            $query="
                INSERT INTO
                    ".__DBC_SCHEMATA_SEARCHED_TRADES__."
                (
                    idOriginCountry,
                    szOriginCountry,
                    idDestinationCountry,
                    szDestinationCountry,
                    iSearched_30_days,
                    iSearched_90_days,
                    iSearched_365_days,
                    iUnique_user_30_days,
                    iUnique_user_90_days,
                    iUnique_user_365_days,
                    fConversionRate,
                    iServiceAvailable,
                    dtDateTime,
                    iActive,
                    iStatus
                )
                VALUES
                (
                    '".(int)$data['idOriginCountry']."',
                    '".mysql_escape_custom(trim($data['szOriginCountry']))."',
                    '".(int)$data['idDestinationCountry']."',
                    '".mysql_escape_custom(trim($data['szDestinationCountry']))."',
                    '".mysql_escape_custom(trim($data['iSearched_30_days']))."',
                    '".mysql_escape_custom(trim($data['iSearched_90_days']))."',
                    '".mysql_escape_custom(trim($data['iSearched_365_days']))."',
                    '".mysql_escape_custom(trim($data['iUnique_user_30_days']))."',
                    '".mysql_escape_custom(trim($data['iUnique_user_90_days']))."',
                    '".mysql_escape_custom(trim($data['iUnique_user_365_days']))."',
                    '".mysql_escape_custom(trim($data['fConversionRate']))."',
                    '".mysql_escape_custom(trim($data['iServiceAvailable']))."',
                    now(), 
                    '1',
                    '1'
                )
            "; 
            echo "<br /><br /> ".$query; 
            //die;
            if($result=$this->exeSQL($query))
            {
                return true;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }	
        }
    }
    function updateStatus()
    {
        $query="
            UPDATE
                ".__DBC_SCHEMATA_SEARCHED_TRADES__."
            SET
                iStatus = '2'
            WHERE
                iStatus = '1' 
        "; 
        //echo "<br /><br /> ".$query; 
        //die;
        if($result=$this->exeSQL($query))
        {
            return true;
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
    
    function deleteOldSearchedTrades()
    {
        $query="
            DELETE FROM
                ".__DBC_SCHEMATA_SEARCHED_TRADES__." 
            WHERE
                iStatus = '2' 
        "; 
        //echo "<br /><br /> ".$query; 
        //die;
        if($result=$this->exeSQL($query))
        {
            return true;
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
    
    function getAllSearchedTradeSnapshot($szSortFeildName=false,$szSortBy=false)
    { 
        if(!empty($szSortFeildName))
        {
            if($szSortFeildName=='szTradeName')
            {
                $query_order_by = " ORDER BY szOriginCountry ".$szSortBy.", szDestinationCountry ".$szSortBy;
            }
            else
            {
                $query_order_by = " ORDER BY ".mysql_escape_custom($szSortFeildName)." ".$szSortBy;
            }
        }
        else
        {
            $query_order_by = " ORDER BY iSearched_30_days DESC ";
        }
        
        $query="
            SELECT
                id,
                idOriginCountry,
                szOriginCountry,
                idDestinationCountry,
                szDestinationCountry,
                iSearched_30_days,
                iSearched_90_days,
                iSearched_365_days,
                iUnique_user_30_days,
                iUnique_user_90_days,
                iUnique_user_365_days,
                fConversionRate,
                iServiceAvailable,
                dtDateTime,
                iActive,
                iStatus
            FROM
                ".__DBC_SCHEMATA_SEARCHED_TRADES__." 
            WHERE
                iStatus = '1' 
                $query_order_by
        ";

        //echo "<br /> ".$query."<br /> ";
        if($result=$this->exeSQL($query))
        {
            if($this->iNumRows>0)
            {
                $ret_ary = array();
                $ctr = 0;
                while($row = $this->getAssoc($result))
                {
                    $ret_ary[$ctr] = $row;
                    $ret_ary[$ctr]['szTradeName'] = $row['szOriginCountry']." - ".$row['szDestinationCountry'];
                    if($row['iServiceAvailable']==1)
                    {
                        $ret_ary[$ctr]['iServiceAvailable'] = 'Yes';
                    }
                    else
                    {
                        $ret_ary[$ctr]['iServiceAvailable'] = 'No';
                    }
                    $ctr++;
                }
                return $ret_ary;
            }
            else
            {
                return false;
            }
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
    
    function updateOperationStatisticsStatus()
    {
        $query="
            UPDATE
                ".__DBC_SCHEMATA_OPERATION_STATISTICS__."
            SET
                iStatus = '2'
            WHERE
                iStatus = '1' 
        "; 
        //echo "<br /><br /> ".$query; 
        //die;
        if($result=$this->exeSQL($query))
        {
            return true;
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
    
    function deleteOldOperationStatistics() 
    {
        $query="
            DELETE FROM
                ".__DBC_SCHEMATA_OPERATION_STATISTICS__." 
            WHERE
                iStatus = '2' 
        "; 
        //echo "<br /><br /> ".$query; 
        //die;
        if($result=$this->exeSQL($query))
        {
            return true;
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
    
     
    function addOperationStatisticsSnapshot($data)
    {
        if(!empty($data))
        {  
            $query="
                INSERT INTO
                    ".__DBC_SCHEMATA_OPERATION_STATISTICS__."
                (
                    szDailySummery,
                    szWeeklySummery,
                    szMonthlySummery,
                    szForwarderBookingSummery,
                    szCustomerLocationSummery,
                    szTopTradeSummery, 
                    dtCreatedOn,
                    iActive,
                    iStatus
                )
                VALUES
                ( 
                    '".mysql_escape_custom(trim($data['szDailySummery']))."', 
                    '".mysql_escape_custom(trim($data['szWeeklySummery']))."',
                    '".mysql_escape_custom(trim($data['szMonthlySummery']))."',
                    '".mysql_escape_custom(trim($data['szForwarderBookingSummery']))."',
                    '".mysql_escape_custom(trim($data['szCustomerLocationSummery']))."',
                    '".mysql_escape_custom(trim($data['szTopTradeSummery']))."', 
                    now(), 
                    '1',
                    '1'
                )
            "; 
            //echo "<br /><br /> ".$query; 
            //die;
            if($result=$this->exeSQL($query))
            {
                return true;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }	
        }
    }
    
    function getAllOperationStatisticsSnapshot()
    {   
        $query="
            SELECT
                id,
                szDailySummery,
                szWeeklySummery,
                szMonthlySummery,
                szForwarderBookingSummery,
                szCustomerLocationSummery,
                szTopTradeSummery, 
                dtCreatedOn,
                iActive,
                iStatus
            FROM
                ".__DBC_SCHEMATA_OPERATION_STATISTICS__." 
            WHERE
                iStatus = '1' 
                $query_order_by
        ";

        //echo "<br /> ".$query."<br /> ";
        if($result=$this->exeSQL($query))
        {
            if($this->iNumRows>0)
            {
                $ret_ary = array(); 
                $row = $this->getAssoc($result);  
                return $row;
            }
            else
            {
                return false;
            }
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
    function getAllCurrentDynamicData()
    { 
        $kReport = new cReport();
        $kBooking = new cBooking();
        $dailySummeryAry = array();

        for($i=0;$i<1;$i++)
        {  
            $date = date("l",strtotime("d")-24*60*60*$i);
            $datePassing = date("Y-m-d",strtotime("d")-24*60*60*$i); 
            $date_key = date('Ymd',  strtotime($datePassing));
            $searchBookingDailyData = array();
            $searchBookingDailyData=$kReport->searchDatewiseDetails($idAdmin,$datePassing);
            $dailySummeryAry[$date_key] = $searchBookingDailyData ;
        } 

        $weeklySummeryAry = array();
        $weeklyCount = 0;
        for($i=0;$i<1;$i++)
        {	 
            $year = date('Y',strtotime("-$i week"));
            SWITCH ($i)
            {
                CASE (0):
                {
                    $daynumber =date('w');
                    if($daynumber>0)
                    {
                        $daynumberFrom=$daynumber-1;
                        $dayToDate=6-$daynumber+1;
                    }
                    else
                    {
                        $dayToDate=$daynumber;
                        $daynumber=6;
                        $daynumberFrom=$daynumber;
                    }
                    $dateFrom=date('Y-m-d',strtotime('-'.$daynumberFrom.'DAY' )); 
                    $dateTo=date('Y-m-d',strtotime('+'.$dayToDate.'DAY')); 
                    $WeekHeading ="This week"; 
                    BREAK;
                } 
            } 
            $searchBookingWeekData = array();
            $searchBookingWeekData=$kReport->searchWeekwiseDetails($idAdmin,$dateFrom,$dateTo);

            $weeklySummeryAry[$weeklyCount] = $searchBookingWeekData ;
            $weeklySummeryAry[$weeklyCount]['szHeading'] = $WeekHeading ;
            $weeklySummeryAry[$weeklyCount]['dtFromDate'] = $dateFrom ;
            $weeklySummeryAry[$weeklyCount]['dtToDate'] = $dateTo ;
            $weeklyCount++;
        } 
        $monthlySummeryAry = array();
        for( $i = 0; $i < 1; $i++ )
        {            
            $dateValue = DATE('M-Y');
            $date = new DateTime($dateValue);
            $date->modify('-'.$i.' month');
            $year = $date->format('Y');    
            $month_key = $date->format('Ymd');
            $months[ $i ] = $date->format('F');
                
            $searchBookingMonthData = array();
            $searchBookingMonthData = $kReport->searchMonthwiseDetails($idAdmin,$months[ $i ],$year);
            $monthlySummeryAry[$i] = $searchBookingMonthData ;
        } 

        $addOperationStatisticsAry = array(); 
        if(!empty($dailySummeryAry))
        {
            $addOperationStatisticsAry['szDailySummery'] = $dailySummeryAry;
        }
        if(!empty($weeklySummeryAry))
        {
            $addOperationStatisticsAry['szWeeklySummery'] = $weeklySummeryAry;
        }
        if(!empty($monthlySummeryAry))
        {
            $addOperationStatisticsAry['szMonthlySummery'] = $monthlySummeryAry;
        }   
        return $addOperationStatisticsAry; 
    }
    
    function updateOperationStatisticsScreen($bReturnAry=false)
    { 
        $idAdmin = 1; 
        $f = fopen(__APP_PATH_LOGS__."/operationStatsCronjob.log", "a");
        fwrite($f, "\n\n###################Cronjob Started on: ".date("d-m-Y h:i:s")."#######################\n");

        $kReport = new cReport();
        $kBooking = new cBooking();
        $dailySummeryAry = array();

        for($i=0;$i<=7;$i++)
        { 
            //echo date('Y-m-d H:i:s');
            $date = date("l",strtotime("d")-24*60*60*$i);
            $datePassing = date("Y-m-d",strtotime("d")-24*60*60*$i); 
            $date_key = date('Ymd',  strtotime($datePassing));
            $searchBookingDailyData = array();
            $searchBookingDailyData=$kReport->searchDatewiseDetails($idAdmin,$datePassing);
            $dailySummeryAry[$date_key] = $searchBookingDailyData ;
        } 

        $weeklySummeryAry = array();
        $weeklyCount = 0;
        for($i=0;$i<=4;$i++)
        {	 
            $year = date('Y',strtotime("-$i week"));
            SWITCH ($i)
            {
                CASE (0):
                {
                    $daynumber =date('w');
                    if($daynumber>0)
                    {
                        $daynumberFrom=$daynumber-1;
                        $dayToDate=6-$daynumber+1;
                    }
                    else
                    {
                        $dayToDate=$daynumber;
                        $daynumber=6;
                        $daynumberFrom=$daynumber;
                    }
                    $dateFrom=date('Y-m-d',strtotime('-'.$daynumberFrom.'DAY' )); 
                    $dateTo=date('Y-m-d',strtotime('+'.$dayToDate.'DAY')); 
                    $WeekHeading ="This week"; 
                    BREAK;
                }
                CASE (1):
                {
                    $daynumber =date('w');
                    if($daynumber>0)
                    {
                        $daynumberFrom=$daynumber+6;
                    }
                    else
                    {
                        $daynumber=7;
                        $daynumberFrom=$daynumber+6;
                    }
                    $dateTo=date('Y-m-d',strtotime('-'.$daynumber.'DAY' ));  
                    $dateFrom=date('Y-m-d',strtotime('-'.$daynumberFrom.'DAY' )); 
                    $WeekHeading ="Last week"; 
                    BREAK;
                }
                CASE (2):
                {
                    $daynumber =date('w');
                    if($daynumber>0)
                    {
                        $daynumberToDate=$daynumber+7;
                        $daynumberFrom=$daynumberToDate+6;
                    }
                    else
                    {
                        $daynumber=7;
                        $daynumberToDate=$daynumber+7;

                        $daynumberFrom=$daynumber+6;
                        $daynumberFrom=$daynumberToDate+6;
                    }

                    $dateTo=date('Y-m-d',strtotime('-'.$daynumberToDate.'DAY' )); 
                    $dateFrom=date('Y-m-d',strtotime('-'.$daynumberFrom.'DAY' )); 
                    $WeekHeading ="Two weeks ago";
                    BREAK;
                }
                CASE (3):
                { 
                    $daynumber =date('w');
                    if($daynumber>0)
                    {
                        $daynumberToDate=$daynumber+14;
                        $daynumberFrom=$daynumberToDate+6;
                    }
                    else
                    {
                        $daynumber=7;
                        $daynumberToDate=$daynumber+14;
                        $daynumberFrom=$daynumberToDate+6;
                    }

                    $dateTo=date('Y-m-d',strtotime('-'.$daynumberToDate.'DAY' )); 
                    $dateFrom=date('Y-m-d',strtotime('-'.$daynumberFrom.'DAY' )); 
                    $WeekHeading ="Three weeks ago";
                    BREAK;
                }
                CASE (4):
                {
                    $daynumber =date('w');
                    if($daynumber>0)
                    {
                        $daynumberToDate=$daynumber+21;
                        $daynumberFrom=$daynumberToDate+6;
                    }
                    else
                    {
                        $daynumber=7;
                        $daynumberToDate=$daynumber+21;
                        $daynumberFrom=$daynumberToDate+6;
                    } 
                    $dateTo=date('Y-m-d',strtotime('-'.$daynumberToDate.'DAY' )); 
                    $dateFrom=date('Y-m-d',strtotime('-'.$daynumberFrom.'DAY' )); 
                    $WeekHeading ="Four weeks ago";
                    BREAK;
                }
            }
            $searchBookingWeekData = array();
            $searchBookingWeekData=$kReport->searchWeekwiseDetails($idAdmin,$dateFrom,$dateTo);

            $weeklySummeryAry[$weeklyCount] = $searchBookingWeekData ;
            $weeklySummeryAry[$weeklyCount]['szHeading'] = $WeekHeading ;
            $weeklySummeryAry[$weeklyCount]['dtFromDate'] = $dateFrom ;
            $weeklySummeryAry[$weeklyCount]['dtToDate'] = $dateTo ;
            $weeklyCount++;
        }

        $monthlySummeryAry = array();
        for( $i = 0; $i <= 12; $i++ )
        {
            $dateValue = DATE('M-Y');
            $date = new DateTime($dateValue);
            $date->modify('-'.$i.' month');
            
            $year = $date->format('Y');
            $month_key = $date->format('Ymd');
            $months[ $i ] = $date->format('F');

            $searchBookingMonthData = array();
            $searchBookingMonthData = $kReport->searchMonthwiseDetails($idAdmin,$months[ $i ],$year);
            $monthlySummeryAry[$month_key] = $searchBookingMonthData ;
        }

        $searchBookingEachForwarder = $kReport->getForwarderOperationsStatistics();
                
        $forwarderBookingSummery = array();
        $fwd_ctr =0;
        if(!empty($searchBookingEachForwarder))
        {
            foreach($searchBookingEachForwarder as $EachForwarder)
            {
                $perForwarderBookingDetails = $kReport->getForwardermonthYearStatistics($EachForwarder['id']);
                $perYearForwarderBookingDetails = $kReport->getForwarderYearStatistics($EachForwarder['id']);

                $showDataFlag=false;
                if($EachForwarder['fTotalBookings']>0)
                { 
                    $showDataFlag=true;
                }
                else if($perForwarderBookingDetails['fTotalBookings']>0)
                { 
                    $showDataFlag=true;
                }
                else if($perYearForwarderBookingDetails['fTotalBookings']>0)
                { 
                    $showDataFlag=true;
                } 

                if($showDataFlag)
                { 
                    $forwarderBookingSummery[$fwd_ctr] = $EachForwarder ;
                    $forwarderBookingSummery[$fwd_ctr]['szPerForwarderBookingDetails'] = $perForwarderBookingDetails ;
                    $forwarderBookingSummery[$fwd_ctr]['szPerYearForwarderBookingDetails'] = $perYearForwarderBookingDetails ;
                    $fwd_ctr++;
                }
            }
        }

        $searchCountryStatisticsCountryCoustomer = $kReport->getCountryOperationsStatisticsCountryDetails();
        $customerLocationSummeryAry = array();			
        $cust_ctr = 0;

        if($searchCountryStatisticsCountryCoustomer!=array())
        {	 
            foreach($searchCountryStatisticsCountryCoustomer as $eachCountryArr)
            {
                $idCountry =  (int)$eachCountryArr['szCustomerCountry'];
                if($idCountry > 0)
                {
                    $szCountryName = $kBooking->findCountryName($idCountry);
                    $searchCountryStatistics = $kReport->getCountryOperationsStatistics($idCountry);
                    $perCountryBookingDetails = $kReport->getCountrymonthYearStatistics($idCountry);
                    $perYearCountryBookingDetails = $kReport->getCountryYearStatistics($idCountry); 

                    $showDataFlag=false;
                    if($searchCountryStatistics['fTotalBookings']>0)
                    {
                        $showDataFlag=true;
                    }
                    else if($perCountryBookingDetails['fTotalBookings']>0)
                    {
                        $showDataFlag=true;
                    }
                    else if($perYearCountryBookingDetails['fTotalBookings']>0)
                    {
                        $showDataFlag=true;
                    } 
                    if($showDataFlag)
                    {
                        $customerLocationSummeryAry[$cust_ctr] = $eachCountryArr ;
                        $customerLocationSummeryAry[$cust_ctr]['szCountryName'] = $szCountryName ;
                        $customerLocationSummeryAry[$cust_ctr]['searchCountryStatistics'] = $searchCountryStatistics ;
                        $customerLocationSummeryAry[$cust_ctr]['perCountryBookingDetails'] = $perCountryBookingDetails ;
                        $customerLocationSummeryAry[$cust_ctr]['perYearCountryBookingDetails'] = $perYearCountryBookingDetails ; 
                        $cust_ctr++;
                    }
                }
            }
        } 
        $originDestinationCountry = $kReport->getSourceDestinationOperationsStatisticsCountryDetails(); 
        $topTradeSummeryAry = array();
        $top_ctr = 0;
        if($originDestinationCountry!=array())
        {
            $i = 0;
            $arrData3 = array();	
            foreach( $originDestinationCountry as $originDestinationStatistics)
            {	
                $j = 0;
                $countryFrom  = $kBooking->findCountryName($originDestinationStatistics['idOriginCountry']);
                $countryTo  = $kBooking->findCountryName($originDestinationStatistics['idDestinationCountry']);

                $originDestinationSevenDaysStatistics	= $kReport->getSourceDestinationOperationsStatistics($originDestinationStatistics['idOriginCountry'],$originDestinationStatistics['idDestinationCountry']);
                $OriginDestinationMonthBookingDetails	= $kReport->getOriginDestinationmonthStatistics($originDestinationStatistics['idOriginCountry'],$originDestinationStatistics['idDestinationCountry']);
                $OriginDestinationYearBookingDetails	= $kReport->getOriginDestinationYearStatistics($originDestinationStatistics['idOriginCountry'],$originDestinationStatistics['idDestinationCountry']);

                $topTradeSummeryAry[$top_ctr] = $eachCountryArr ;
                $topTradeSummeryAry[$top_ctr]['szOriginCountry'] = $countryFrom ;
                $topTradeSummeryAry[$top_ctr]['szDestinationCountry'] = $countryTo ;
                $topTradeSummeryAry[$top_ctr]['originDestinationSevenDaysStatistics'] = $originDestinationSevenDaysStatistics ;
                $topTradeSummeryAry[$top_ctr]['originDestinationMonthBookingDetails'] = $OriginDestinationMonthBookingDetails ;
                $topTradeSummeryAry[$top_ctr]['originDestinationYearBookingDetails'] = $OriginDestinationYearBookingDetails ;
                $top_ctr++; 
            }
        }

        $addOperationStatisticsAry = array(); 
        if(!empty($dailySummeryAry))
        {
            $addOperationStatisticsAry['szDailySummery'] = serialize($dailySummeryAry);
        }
        if(!empty($weeklySummeryAry))
        {
            $addOperationStatisticsAry['szWeeklySummery'] = serialize($weeklySummeryAry);
        }
        if(!empty($monthlySummeryAry))
        {
            $addOperationStatisticsAry['szMonthlySummery'] = serialize($monthlySummeryAry);
        }
        if(!empty($forwarderBookingSummery))
        {
            $addOperationStatisticsAry['szForwarderBookingSummery'] = serialize($forwarderBookingSummery);
        }
        if(!empty($customerLocationSummeryAry))
        {
            $addOperationStatisticsAry['szCustomerLocationSummery'] = serialize($customerLocationSummeryAry);
        }
        if(!empty($customerLocationSummeryAry))
        {
            $addOperationStatisticsAry['szTopTradeSummery'] = serialize($topTradeSummeryAry);
        }   
        
        if($bReturnAry)
        {
            return $addOperationStatisticsAry;
        }
        if(!empty($addOperationStatisticsAry))
        {
           $kReport->updateOperationStatisticsStatus(); 
           $kReport->addOperationStatisticsSnapshot($addOperationStatisticsAry);     
           $kReport->deleteOldOperationStatistics();
        } 
        
        $kQuote = new cQuote();
        $cronLogsAry = array();
        $cronLogsAry['iType'] = 2;
        $kQuote->addCronScheduleLogs($cronLogsAry);
        
        fwrite($f, "\n\n###################Cronjob End:".date("d-m-Y h:i:s")."#######################\n"); 
        $f = fopen(__APP_PATH_LOGS__."/operationStatsCronjob.log", "a");  
        return true ;
    }
    
    function searchDatewiseDetails($id,$date)
    { 
        $date = sanitize_all_html_input($date); 
        $szSearchBookingCount = $this->getBookingSearchCount($date,false,'DAILY'); 
        
            $query="
                SELECT 
                    COUNT(id) AS szBookingCount,
                    COALESCE(SUM(fCargoVolume),0.00) AS szVolume,
                    COALESCE(SUM(fCargoWeight),0.00) AS szWeight,
                    COALESCE(SUM(fTotalPriceUSD),0.00) AS szUSD,
                    COUNT(DISTINCT(idUser)) as iUser,
                    (
                        SELECT	
                            IFNULL(SUM(fTotalInsuranceCostForBookingUSD),0) AS fInsuranceRevenue		
                        FROM
                            ".__DBC_SCHEMATA_BOOKING__."
                        WHERE
                    (  
                            idBookingStatus IN(3,4) 
                        AND
                            iInsuranceIncluded = '1'
                        AND
                            iInsuranceStatus !='3'
                        AND 
                            iTransferConfirmed='0' 
                        AND
                            DATE(dtBookingConfirmed) =  '".mysql_escape_custom($date)."'
                        AND
                            idUser !=0
                    )
                ) as fTotalInsuranceRevenue
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE
                idUser !=0
            AND	
                DATE(dtBookingConfirmed) =  '".mysql_escape_custom($date)."'
            AND	
                idBookingStatus IN(3,4)	
            AND
                iTransferConfirmed='0' 
        ";
        //echo "<br>".$query."<br>";
        if($result=$this->exeSQL($query))
        {
            if($this->iNumRows>0)
            {	
                $row=$this->getAssoc($result);
                $row['szSearchBookingCount'] = $szSearchBookingCount;
                return $row;
            }
            else
            {	
                $row = array();
                $row['szSearchBookingCount'] = $szSearchBookingCount;
                return $row;
            }
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        } 
    }

    function searchWeekwiseDetails($id,$dtFrom,$dtTo)
    {
        $id = sanitize_all_html_input($id);
        $dtFrom = sanitize_all_html_input($dtFrom);
        $dtTo = sanitize_all_html_input($dtTo);
        $szSearchBookingCount = $this->getBookingSearchCount($dtFrom,$dtTo,'MONTHLY'); 
        
        $query="
            SELECT 
                COUNT(id) AS szBookingCount,
                COALESCE(SUM(fCargoVolume),0.00) AS szVolume,
                COALESCE(SUM(fCargoWeight),0.00) AS szWeight,
                COALESCE(SUM(fTotalPriceUSD),0.00) AS szUSD,
                COUNT(DISTINCT(idUser)) as iUser,
                (
                    SELECT	
                        IFNULL(SUM(fTotalInsuranceCostForBookingUSD),0) AS fInsuranceRevenue		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__."
                    WHERE
                        (   
                            idBookingStatus IN(3,4) 
                        AND
                            iInsuranceIncluded = '1'
                        AND
                            iInsuranceStatus !='3'
                        AND 
                            iTransferConfirmed='0' 
                        AND
                            DATE(dtBookingConfirmed) >=  '".mysql_escape_custom($dtFrom)."'
                        AND
                            DATE(dtBookingConfirmed) <=  '".mysql_escape_custom($dtTo)."'
                        )
                    ) as fTotalInsuranceRevenue
                FROM
                    ".__DBC_SCHEMATA_BOOKING__."
                WHERE
                    DATE(dtBookingConfirmed) >=  '".mysql_escape_custom($dtFrom)."'
                AND
                    DATE(dtBookingConfirmed) <=  '".mysql_escape_custom($dtTo)."'
                AND	
                    idBookingStatus IN(3,4)		
                AND 
                    iTransferConfirmed='0'

        ";
        //echo "<br><br/>".$query."<br/><br/>";
        if($result=$this->exeSQL($query))
        {
            if($this->iNumRows>0)
            {	
                $row = $this->getAssoc($result);
                $row['szSearchBookingCount'] = $szSearchBookingCount;
                return $row;
            } 
        } 
        if(empty($row))
        {
            $row['szSearchBookingCount'] = $szSearchBookingCount;
            return $row; 
        }  
    }

    function getBookingSearchCount($dtFrom,$dtTo,$szType)
    {  
        if($szType=='DAILY')
        {
            $query_and = " DATE(dtDateTime) =  '".mysql_escape_custom($dtFrom)."' ";
        }
        else if($szType=='MONTHLY')
        {
            $query_and = "
                    DATE(dtDateTime) >=  '".mysql_escape_custom($dtFrom)."'	
                AND
                    DATE(dtDateTime) <=  '".mysql_escape_custom($dtTo)."'
            ";
        }
        else
        {
            return false;
        }
        $dtFrom = sanitize_all_html_input($dtFrom);
        $dtTo = sanitize_all_html_input($dtTo);

        $query=" 
            SELECT
                COUNT(id) as szSearchBookingCount
            FROM
                ".__DBC_SCHEMATA_COMPARE_BOOKING__."
            WHERE
                $query_and
        ";
        //echo "<br><br/>".$query."<br/><br/>";
        if($result=$this->exeSQL($query))
        { 
            $row = $this->getAssoc($result);
            return $row['szSearchBookingCount']; 
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        } 
    }

    function searchMonthwiseDetails($id,$month,$year)
    {
        $id = sanitize_all_html_input($id);
        $month = sanitize_all_html_input($month);
        $year = sanitize_all_html_input($year);

        $query="
            SELECT
            (
                SELECT
                    COUNT(id)
                FROM
                    ".__DBC_SCHEMATA_COMPARE_BOOKING__."
                WHERE
                    monthname(dtDateTime) =  '".mysql_escape_custom($month)."'	
                AND
                    YEAR(dtDateTime) =  '".mysql_escape_custom($year)."'	

                ) as szSearchBookingCount,
                COUNT(id) AS szBookingCount,
                COALESCE(SUM(fCargoVolume),0.00) AS szVolume,
                COALESCE(SUM(fCargoWeight),0.00) AS szWeight,
                COALESCE(SUM(fTotalPriceUSD),0.00) AS szUSD,
                COUNT(DISTINCT(idUser)) as iUser,
                (
                    SELECT	
                        IFNULL(SUM(fTotalInsuranceCostForBookingUSD),0) AS fInsuranceRevenue		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__."
                    WHERE
                    (   
                            idBookingStatus IN(3,4) 
                        AND
                            iInsuranceIncluded = '1'
                        AND
                            iInsuranceStatus !='3'
                        AND 
                            iTransferConfirmed='0' 
                        AND
                            monthname(dtBookingConfirmed) =  '".mysql_escape_custom($month)."'
                        AND
                            YEAR(dtBookingConfirmed) =  '".mysql_escape_custom($year)."'
                    )
                ) as fTotalInsuranceRevenue
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE
                monthname(dtBookingConfirmed) =  '".mysql_escape_custom($month)."'
            AND
                YEAR(dtBookingConfirmed) =  '".mysql_escape_custom($year)."'
            AND	
                idBookingStatus IN(3,4)	
            AND 
                iTransferConfirmed='0'
        ";
        //echo "<br>".$query."<br>";
        if($result=$this->exeSQL($query))
        {
            if($this->iNumRows>0)
            {	
                    return $row=$this->getAssoc($result);
            }
            else
            {	
                    return array();
            }
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }  
    }

    function getForwarderOperationsStatistics()
    {
        $query="
            SELECT
                f.id,
                f.szDisplayName,
                f.szForwarderAlias,
                count(b.id) as fTotalBookings,
                SUM(b.fTotalPriceUSD) AS fTotalPriceUSD,
                b.iBookingLanguage,
                (
                    SELECT	
                        IFNULL(SUM(fTotalInsuranceCostForBookingUSD),0) AS fInsuranceRevenue		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__."
                    WHERE
                    (   
                            idBookingStatus IN(3,4) 
                        AND
                            iInsuranceIncluded = '1'
                        AND
                            iInsuranceStatus !='3'
                        AND 
                            iTransferConfirmed='0' 
                        AND
                           dtBookingConfirmed >= (DATE_SUB(CURDATE(), INTERVAL 7 DAY))
                        AND
                            idForwarder = f.id
                    )
                ) as fTotalInsuranceRevenue
             FROM
                ".__DBC_SCHEMATA_FROWARDER__." as f
             LEFT JOIN 
                ".__DBC_SCHEMATA_BOOKING__." AS b
             ON	
                (
                    b.idForwarder = f.id 
                AND 
                    b.idBookingStatus IN(3,4)
                AND	
                    b.`dtBookingConfirmed`>=(DATE_SUB(CURDATE(), INTERVAL 7 DAY))	
                AND 
                    iTransferConfirmed='0' 
                )	
             WHERE
                f.iActive = 1
             AND
                f.szDisplayName != '' OR f.szDisplayName != null		
             GROUP BY
                f.id
             ORDER BY 	
                count(b.id)
             DESC		
        ";
        //echo $query;
        if($result=$this->exeSQL($query))
        {
            if($this->iNumRows>0)
            {
                $forwarderbooking_ary = array();
                while($row=$this->getAssoc($result))
                {
                    $forwarderbooking_ary[] = $row;
                }
                return $forwarderbooking_ary;
            }
            else
            {
                return array();
            }
        }
    }


    function getForwardermonthYearStatistics($idForwarder)
    {
        $idForwarder = (int)sanitize_all_html_input($idForwarder);
        if($idForwarder>0)
        {
            $query="
                SELECT
                    count(b.id) as fTotalBookings,
                    SUM(b.fTotalPriceUSD) AS fTotalPriceUSD,
                    (
                    SELECT	
                        IFNULL(SUM(fTotalInsuranceCostForBookingUSD),0) AS fInsuranceRevenue		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__."
                    WHERE
                        (  
                            idBookingStatus IN(3,4) 
                        AND
                            iInsuranceIncluded = '1'
                        AND
                            iInsuranceStatus !='3'
                        AND 
                            iTransferConfirmed='0' 
                        AND
                            `dtBookingConfirmed`>=(DATE_SUB(CURDATE(), INTERVAL 30 DAY))
                        AND
                            idForwarder = '".(int)$idForwarder."'
                        )
                    ) as fTotalInsuranceRevenue
                 FROM
                    ".__DBC_SCHEMATA_BOOKING__." AS b
                WHERE
                    b.idForwarder = '".(int)$idForwarder."'
                AND 
                    b.idBookingStatus IN(3,4)
                AND	
                    b.`dtBookingConfirmed`>=(DATE_SUB(CURDATE(), INTERVAL 30 DAY))
                AND 
                    b.iTransferConfirmed='0'
            ";
            //echo $query."<br>";
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    //$forwarderbooking_ary = array();
                    while($row=$this->getAssoc($result))
                    {
                        $forwarderbooking_ary = $row;
                    }
                    return $forwarderbooking_ary;
                }
                else
                {
                    return array();
                }
            }
        }
    }


    function getForwarderYearStatistics($idForwarder)
    {
        $idForwarder = (int)sanitize_all_html_input($idForwarder);
        $daysThisMonth = date('d', strtotime ( DATE('Y-m-d')) );
        $daysFrom = 365;
        //+ $daysThisMonth;
        $dateFrom = date('Y-m-d',strtotime ( '- '.$daysFrom.' days ' , strtotime ( DATE('Y-m-d') )));
        $dateTo = date('Y-m-d',strtotime ( '- '.$daysThisMonth.' days' , strtotime ( DATE('Y-m-d') )));
        if($idForwarder>0)
        {
            $query="
                SELECT
                    count(b.id) as fTotalBookings,
                    SUM(b.fTotalPriceUSD) AS fTotalPriceUSD,
                    (
                    SELECT	
                        IFNULL(SUM(fTotalInsuranceCostForBookingUSD),0) AS fInsuranceRevenue		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__."
                    WHERE
                        (   
                            idBookingStatus IN(3,4) 
                        AND
                            iInsuranceIncluded = '1'
                        AND
                            iInsuranceStatus !='3'
                        AND 
                            iTransferConfirmed='0' 
                        AND	
                            DATE_FORMAT(dtBookingConfirmed,'%Y-%m-%d') <= DATE_FORMAT(now(),'%Y-%m-%d')
                        AND	
                            DATE_FORMAT(dtBookingConfirmed,'%Y-%m-%d') >= '".mysql_escape_custom($dateFrom)."'
                        AND
                            idForwarder = '".(int)$idForwarder."'
                        )
                    ) as fTotalInsuranceRevenue
                FROM
                    ".__DBC_SCHEMATA_BOOKING__." AS b
                WHERE
                    b.idForwarder = '".(int)$idForwarder."'
                AND 
                    b.idBookingStatus IN(3,4)
                AND	
                    DATE_FORMAT(dtBookingConfirmed,'%Y-%m-%d') <= DATE_FORMAT(now(),'%Y-%m-%d')
                AND	
                    DATE_FORMAT(dtBookingConfirmed,'%Y-%m-%d') >= '".mysql_escape_custom($dateFrom)."'
                AND
                    iTransferConfirmed='0' 
            ";
            //echo $query."<br>";
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    //$forwarderbooking_ary = array();
                    while($row=$this->getAssoc($result))
                    {
                        $forwarderbooking_ary = $row;
                    }
                    return $forwarderbooking_ary;
                }
                else
                {
                    return array();
                }
            }
        }
    }


    function getCountryOperationsStatisticsCountryDetails()
    {
            /*$query="
            SELECT
                c.id AS szCustomerCountry
             FROM	
                ".__DBC_SCHEMATA_COUNTRY__." AS c
             LEFT JOIN
                ".__DBC_SCHEMATA_BOOKING__." AS b	
            ON
                (b.szCustomerCountry = c.id
            AND
                b.idBookingStatus IN(3,4)
            AND	
                b.`dtBookingConfirmed`>=(DATE_SUB(CURDATE(), INTERVAL 365 DAY))	
            AND 
                (
                    b.szCustomerCountry	!=NULL 
                OR 
                    b.szCustomerCountry	!=''
                )
                )	
            ";*/
        $query="
            SELECT
                DISTINCT(b.szCustomerCountry) AS szCustomerCountry
             FROM
               
                ".__DBC_SCHEMATA_BOOKING__." AS b	
            WHERE
                b.idBookingStatus IN(3,4)
            AND	
                b.`dtBookingConfirmed`>=(DATE_SUB(CURDATE(), INTERVAL 365 DAY))
            ";
            //echo $query;
            if($result=$this->exeSQL($query))
            {
                    if($this->iNumRows>0)
                    {
                            $forwarderbooking_ary = array();
                            $ctr=0;
                            while($row=$this->getAssoc($result))
                            {
                                    
                                    //$count=count($forwarderbookingArr[$row['szCustomerCountry']]);
                                    $forwarderbookingArr[$row['szCustomerCountry']][$ctr]=$row;
                                    $count=count($forwarderbookingArr[$row['szCustomerCountry']]);
                                    $forwarderbookingNewArr[$row['szCustomerCountry']]['counter']=$count;
                                    //$forwarderbooking_ary[$ctr]['counter']=count($forwarderbookingArr[$row['szCustomerCountry']]);
                                    
                                    //$forwarderbooking_ary[$ctr]=$row;
                                    ++$ctr;
                            }
                            //print_r($forwarderbookingNewArr);
                            if(count($forwarderbookingNewArr)>0)
                            {
                                $ctr=0;
                               foreach($forwarderbookingNewArr as $key=>$value)
                               {
                                   $forwarderbooking_ary[$ctr]['szCustomerCountry']=$key;
                                   $forwarderbooking_ary[$ctr]['counter']=$value;
                                   ++$ctr;
                               }
                               $forwarderbooking_ary=  sortArray($forwarderbooking_ary,'counter',true);
                               $iTotalCount=count($forwarderbooking_ary);
                               if($iTotalCount>15)
                               {
                                   $forwarderbooking_ary = array_slice($forwarderbooking_ary, 0, 15, true);
                               }
                               // print_r($forwarderbooking_ary);
                                return $forwarderbooking_ary;
                            }
                            return $forwarderbooking_ary;
                    }
                    else
                    {
                            return array();
                    }
            }
    } 
    function getCountryOperationsStatistics($idCountry)
    {
        $query="
            SELECT
                count(b.id) as fTotalBookings,
                SUM(b.fTotalPriceUSD) AS fTotalPriceUSD,
                (
                SELECT	
                    IFNULL(SUM(fTotalInsuranceCostForBookingUSD),0) AS fInsuranceRevenue		
                FROM
                    ".__DBC_SCHEMATA_BOOKING__."
                WHERE
                (   
                        idBookingStatus IN(3,4) 
                    AND
                        iInsuranceIncluded = '1'
                    AND
                        iInsuranceStatus !='3'
                    AND 
                        iTransferConfirmed='0' 
                    AND
                        szCustomerCountry = '".(int)$idCountry."'
                    AND
                        `dtBookingConfirmed`>=(DATE_SUB(CURDATE(), INTERVAL 7 DAY))
                    )
                ) as fTotalInsuranceRevenue
            FROM
                ".__DBC_SCHEMATA_BOOKING__." AS b
            WHERE
                b.szCustomerCountry = '".(int)$idCountry."'
            AND 
                b.idBookingStatus IN(3,4)
            AND	
                b.`dtBookingConfirmed`>=(DATE_SUB(CURDATE(), INTERVAL 7 DAY))
            AND 
                iTransferConfirmed='0'
        ";
        //echo $query;
        if($result=$this->exeSQL($query))
        {
            if($this->iNumRows>0)
            {
                while($row=$this->getAssoc($result))
                {
                    $forwarderbooking_ary = $row;
                }
                return $forwarderbooking_ary;
            }
            else
            {
                return array();
            }
        }
    }

    function getCountrymonthYearStatistics($idCountry)
    {
        $idCountry = (int)sanitize_all_html_input($idCountry);
        if($idCountry>0)
        {
            $query="
                SELECT
                    count(b.id) as fTotalBookings,
                    SUM(b.fTotalPriceUSD) AS fTotalPriceUSD,
                    (
                    SELECT	
                        IFNULL(SUM(fTotalInsuranceCostForBookingUSD),0) AS fInsuranceRevenue		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__."
                    WHERE
                        (   
                            idBookingStatus IN(3,4) 
                        AND
                            iInsuranceIncluded = '1'
                        AND
                            iInsuranceStatus !='3'
                        AND 
                            iTransferConfirmed='0' 
                        AND
                            szCustomerCountry = '".(int)$idCountry."'
                        AND
                           `dtBookingConfirmed`>=(DATE_SUB(CURDATE(), INTERVAL 30 DAY))
                        )
                    ) as fTotalInsuranceRevenue
                 FROM
                    ".__DBC_SCHEMATA_BOOKING__." AS b
                WHERE
                    b.szCustomerCountry = '".(int)$idCountry."'
                AND 
                    b.idBookingStatus IN(3,4)
                AND	
                    b.`dtBookingConfirmed`>=(DATE_SUB(CURDATE(), INTERVAL 30 DAY))
                AND 
                    iTransferConfirmed='0' 
            ";
            //echo $query."<br>";
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    //$forwarderbooking_ary = array();
                    while($row=$this->getAssoc($result))
                    {
                        $forwarderbooking_ary = $row;
                    }
                    return $forwarderbooking_ary;
                }
                else
                {
                    return array();
                }
            }
        }
    }
    function getCountryYearStatistics($idCountry)
    {
        $idCountry = (int)sanitize_all_html_input($idCountry);
        $daysThisMonth = date('d', strtotime ( DATE('Y-m-d')) );
        $daysFrom = 365;
        //+ $daysThisMonth
        $dateFrom = date('Y-m-d',strtotime ( '- '.$daysFrom.' days ' , strtotime ( DATE('Y-m-d') )));
        $dateTo = date('Y-m-d',strtotime ( '- '.$daysThisMonth.' days' , strtotime ( DATE('Y-m-d') )));

        if($idCountry>0)
        {
            $query="
                SELECT
                    count(b.id) as fTotalBookings,
                    SUM(b.fTotalPriceUSD) AS fTotalPriceUSD,
                    (
                        SELECT	
                            IFNULL(SUM(fTotalInsuranceCostForBookingUSD),0) AS fInsuranceRevenue		
                        FROM
                            ".__DBC_SCHEMATA_BOOKING__."
                        WHERE
                        (   
                            idBookingStatus IN(3,4) 
                        AND
                            iInsuranceIncluded = '1'
                        AND
                            iInsuranceStatus !='3'
                        AND 
                            iTransferConfirmed='0' 
                        AND	
                            DATE_FORMAT(dtBookingConfirmed,'%Y-%m-%d') <= DATE_FORMAT(now(),'%Y-%m-%d')
                        AND	
                            DATE_FORMAT(dtBookingConfirmed,'%Y-%m-%d') >= '".mysql_escape_custom($dateFrom)."'
                        AND
                            szCustomerCountry = '".(int)$idCountry."'
                        )
                    ) as fTotalInsuranceRevenue
                 FROM
                    ".__DBC_SCHEMATA_BOOKING__." AS b
                 WHERE
                    b.szCustomerCountry = '".(int)$idCountry."'
                AND 
                    b.idBookingStatus IN(3,4)
                AND	
                    DATE_FORMAT(dtBookingConfirmed,'%Y-%m-%d') <= DATE_FORMAT(now(),'%Y-%m-%d')
                AND	
                    DATE_FORMAT(dtBookingConfirmed,'%Y-%m-%d') >= '".mysql_escape_custom($dateFrom)."' 
                AND 
                    iTransferConfirmed='0'
            ";
            //echo $query."<br>";
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    //$forwarderbooking_ary = array();
                    while($row=$this->getAssoc($result))
                    {
                        $forwarderbooking_ary = $row;
                    }
                    return $forwarderbooking_ary;
                }
                else
                {
                    return array();
                }
            }
        }
    }

    function getSourceDestinationOperationsStatisticsCountryDetails()
    {
        $query="
            SELECT
                b2.id,
                b2.idDestinationCountry,
                b2.idOriginCountry
            FROM
                ".__DBC_SCHEMATA_BOOKING__." AS b2
            WHERE 
                b2.idBookingStatus IN ( 3, 4 ) 
            AND 
                b2.`dtBookingConfirmed` >= ( DATE_SUB( CURDATE( ) , INTERVAL 30 DAY ) )
            ";
        //echo $query;
        if($result=$this->exeSQL($query))
        {
            if($this->iNumRows>0)
            {
                $outerCtr = 0;
                $ctr=0;
                while($row=$this->getAssoc($result))
                {
                    $key=$row['idOriginCountry']."_".$row['idDestinationCountry'];
                    $resBookingNewArr[$key][$ctr]=1;
                    $count=count($resBookingNewArr[$key]);
                    $resBookingArr[$key]=$count;
                    ++$ctr;
                }
            }
        }
        
        $query="
            SELECT
                idDestinationCountry,
                idOriginCountry
            FROM	
                ".__DBC_SCHEMATA_BOOKING__." AS b
            WHERE
                b.idBookingStatus IN(3,4)
            AND	
                b.`dtBookingConfirmed`>=(DATE_SUB(CURDATE(), INTERVAL 365 DAY))	 
             GROUP BY
                b.idDestinationCountry,
                b.idOriginCountry
        ";
        // echo $query;
        if($result=$this->exeSQL($query))
        {
            if($this->iNumRows>0)
            {
                $ctr=0;
                $forwarderbooking_ary = array();
                while($row=$this->getAssoc($result))
                {
                    $key=$row['idOriginCountry']."_".$row['idDestinationCountry'];
                    
                    $forwarderbooking_ary[$ctr] = $row;
                    $forwarderbooking_ary[$ctr]['iLastWeekCount'] = $resBookingArr[$key];
                    ++$ctr;
                }
                if(count($forwarderbooking_ary)>0)
                {
                   $forwarderbooking_ary=  sortArray($forwarderbooking_ary,'iLastWeekCount',true);
                   $iTotalCount=count($forwarderbooking_ary);
                   if($iTotalCount>15)
                   {
                       $forwarderbooking_ary = array_slice($forwarderbooking_ary, 0, 15, true);
                   }
                   //print_r($forwarderbooking_ary);
                    return $forwarderbooking_ary;
                }
                
            }
            else
            {
                return array();
            }
        }
    }  
    function getSourceDestinationOperationsStatistics($idOrigin,$idDestination)
    {
        $idOrigin = (int)sanitize_all_html_input($idOrigin);
        $idDestination = (int)sanitize_all_html_input($idDestination);
        if($idOrigin>0 && $idDestination>0)
        {
            $query="
                SELECT
                    count(b.id) as fTotalBookings,
                    SUM(b.fTotalPriceUSD) AS fTotalPriceUSD,
                    (
                        SELECT	
                            IFNULL(SUM(fTotalInsuranceCostForBookingUSD),0) AS fInsuranceRevenue		
                        FROM
                            ".__DBC_SCHEMATA_BOOKING__."
                        WHERE
                        (  
                            idBookingStatus IN(3,4) 
                        AND
                            iInsuranceIncluded = '1'
                        AND
                            iInsuranceStatus !='3'
                        AND 
                            iTransferConfirmed='0' 
                        AND	
                            `dtBookingConfirmed`>=(DATE_SUB(CURDATE(), INTERVAL 7 DAY))
                        AND 
                            idOriginCountry = ".$idOrigin."
                        AND	
                            idDestinationCountry = ".$idDestination." 
                        )
                    ) as fTotalInsuranceRevenue
                FROM	
                    ".__DBC_SCHEMATA_BOOKING__." AS b
                WHERE
                    b.idBookingStatus IN(3,4)
                AND	
                    b.`dtBookingConfirmed`>=(DATE_SUB(CURDATE(), INTERVAL 7 DAY))
                AND 
                    idOriginCountry = ".$idOrigin."
                AND	
                    b.idDestinationCountry = ".$idDestination." 	
                AND 
                    iTransferConfirmed='0' 
                 GROUP BY
                    b.idOriginCountry
                    ,
                    b.idDestinationCountry
            ";
            //echo $query."<br>";
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    //$forwarderbooking_ary = array();
                    while($row=$this->getAssoc($result))
                    {
                        $forwarderbooking_ary = $row;
                    }
                    return $forwarderbooking_ary;
                }
                else
                {
                    return array();
                }
            }
        }
    }

    function getOriginDestinationmonthStatistics($idOrigin,$idDestination)
    {
        $idOrigin = (int)sanitize_all_html_input($idOrigin);
        $idDestination = (int)sanitize_all_html_input($idDestination);
        if($idOrigin>0 && $idDestination>0)
        {
            $query="
                SELECT
                    count(b.id) as fTotalBookings,
                    SUM(b.fTotalPriceUSD) AS fTotalPriceUSD,
                    (
                        SELECT	
                            IFNULL(SUM(fTotalInsuranceCostForBookingUSD),0) AS fInsuranceRevenue		
                        FROM
                            ".__DBC_SCHEMATA_BOOKING__."
                        WHERE
                        (   
                            idBookingStatus IN(3,4) 
                        AND
                            iInsuranceIncluded = '1'
                        AND
                            iInsuranceStatus !='3'
                        AND 
                            iTransferConfirmed='0' 
                        AND	
                            `dtBookingConfirmed`>=(DATE_SUB(CURDATE(), INTERVAL 30 DAY))
                        AND 
                            idOriginCountry	= ".$idOrigin."
                        AND	
                            idDestinationCountry = ".$idDestination." 
                        )
                    ) as fTotalInsuranceRevenue
                FROM	
                    ".__DBC_SCHEMATA_BOOKING__." AS b
                WHERE
                    b.idBookingStatus IN(3,4)
                AND	
                    b.`dtBookingConfirmed`>=(DATE_SUB(CURDATE(), INTERVAL 30 DAY))
                AND 
                    idOriginCountry	= ".$idOrigin."
                AND	
                    b.idDestinationCountry = ".$idDestination." 	
                AND 
                    iTransferConfirmed='0'
                GROUP BY
                    b.idOriginCountry
                    ,
                    b.idDestinationCountry
            ";
            //echo $query."<br>";
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    //$forwarderbooking_ary = array();
                    while($row=$this->getAssoc($result))
                    {
                        $forwarderbooking_ary = $row;
                    }
                    return $forwarderbooking_ary;
                }
                else
                {
                    return array();
                }
            }
        }
    }
    function getOriginDestinationYearStatistics($idOrigin,$idDestination)
    {
        $idOrigin = (int)sanitize_all_html_input($idOrigin);
        $idDestination = (int)sanitize_all_html_input($idDestination);

        if($idOrigin>0 && $idDestination>0)
        {
            $query="
                SELECT
                    count(b.id) as fTotalBookings,
                    SUM(b.fTotalPriceUSD) AS fTotalPriceUSD,
                    (
                        SELECT	
                            IFNULL(SUM(fTotalInsuranceCostForBookingUSD),0) AS fInsuranceRevenue		
                        FROM
                            ".__DBC_SCHEMATA_BOOKING__."
                        WHERE
                        (   
                                idBookingStatus IN(3,4) 
                            AND
                                iInsuranceIncluded = '1'
                            AND
                                iInsuranceStatus !='3'
                            AND 
                                iTransferConfirmed='0' 
                            AND 
                                idOriginCountry = ".$idOrigin."
                            AND	
                                idDestinationCountry = ".$idDestination." 		
                            AND	
                               dtBookingConfirmed < NOW()
                            AND	
                                `dtBookingConfirmed` >= (DATE_SUB(CURDATE(), INTERVAL 365 DAY))  
                        )
                    ) as fTotalInsuranceRevenue
                 FROM	
                    ".__DBC_SCHEMATA_BOOKING__." AS b
                WHERE
                    b.idBookingStatus IN(3,4)
                AND 
                    idOriginCountry = ".$idOrigin."
                AND	
                    b.idDestinationCountry = ".$idDestination." 		
                AND	
                   dtBookingConfirmed < NOW()
                AND	
                    b.`dtBookingConfirmed`>=(DATE_SUB(CURDATE(), INTERVAL 365 DAY)) 
                AND 
                    iTransferConfirmed='0'
                GROUP BY
                    b.idOriginCountry,
                    b.idDestinationCountry
            ";
            //echo $query."<br>";
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    //$forwarderbooking_ary = array();
                    while($row=$this->getAssoc($result))
                    {
                            $forwarderbooking_ary = $row;
                    }
                    return $forwarderbooking_ary;
                }
                else
                {
                    return array();
                }
            }
        }
    }
    
    function isEmailKeyExists($szEmailKey)
    {
        if(!empty($szEmailKey))
        {
            $szEmailKey = trim($szEmailKey); 
            
            $query="
                SELECT
                    id
                FROM
                    ".__DBC_SCHEMATA_EMAIL_LOG__."
                WHERE
                    szEmailKey = '".mysql_escape_custom($szEmailKey)."'	
            "; 
            //echo $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
        	// Check if the user exists
                if( $this->getRowCnt() > 0 )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    
    function updateEmailLogs($data)
    { 
        if(!empty($data))
        {  
            $szEmailKey = $data['email_key']; 
            $szEmailStatus = $data['event'];   
            
            if($szEmailStatus=='delivered')
            {
                $iSuccess = 2;
            }
            else if($szEmailStatus=='open')
            {
                $iSuccess = 3;
            }
            else 
            {
                $iSuccess = 4;
            } 
            $emailLogAry = $this->loadEmailLogs($szEmailKey);
            
            if($emailLogAry['iSuccess']!=3)
            {
                $kBooking = new cBooking();
                $dtCurrentMysqlDateTime = $kBooking->getRealNow();
                //If email is already opened then we don't update status again. 
                $query = "
                    UPDATE
                        ".__DBC_SCHEMATA_EMAIL_LOG__."
                    SET 
                        iSuccess = '".mysql_escape_custom($iSuccess)."',
                        szEmailStatus = '".mysql_escape_custom($szEmailStatus)."',
                        dtEmailStatusUpdated = '".mysql_escape_custom($dtCurrentMysqlDateTime)."'
                    WHERE
                        szEmailKey = '".mysql_escape_custom($szEmailKey)."'
                ";
                //echo $query; 
                if ($this->exeSQL($query))
                {
                    $kBooking = new cBooking();  
                    $emaillogAry = $this->loadEmailLogs($szEmailKey);
                    $iQuoteEmail = $emaillogAry['iQuoteEmail']; 
                    $idCrmEmailLogs = $emaillogAry['id'];
                    
                    if($idCrmEmailLogs>0)
                    {
                        /*
                        * Updating seen flag in customer log snippet
                        */ 
                        $customerSnippetAry = array();
                        $customerSnippetAry['dtEmailStatusUpdated'] = $dtCurrentMysqlDateTime;
                        $customerSnippetAry['szEmailStatus'] = $szEmailStatus;
                        $customerSnippetAry['iSuccess'] = $iSuccess;
                        
                        $kCustomerLogs = new cCustomerLogs();
                        $kCustomerLogs->updateCustomerLogSnippet($customerSnippetAry,$idCrmEmailLogs,__CUSTOMER_LOG_SNIPPET_TYPE_EMAIL__); 
                    }
                    
                    if($emaillogAry['idBooking']>0)
                    {
                        $idBooking = $emaillogAry['idBooking'] ;
                        $bookingDataArr = array();
                        $bookingDataArr =  $kBooking->getBookingDetails($idBooking);
                        if($bookingDataArr['idFile']>0)
                        {
                            $idBookingFile = $bookingDataArr['idFile']; 
                            if($iSuccess==3 && $iQuoteEmail==1) //Quote Email Opened
                            {
                                $kBooking->updateRemindForwarder(4,$idBookingFile);
                            }
                            else if($iSuccess==4 && (int)$emaillogAry['iIgnoreTracking']==0)
                            { 
                                $dtResponseTime = $kBooking->getRealNow();
                                $fileLogsAry = array();  
                                $fileLogsAry['szTransportecaTask'] = 'T170'; 
                                $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime;

                                if(!empty($fileLogsAry))
                                {
                                    foreach($fileLogsAry as $key=>$value)
                                    {
                                        $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                                    }
                                } 
                                $file_log_query = rtrim($file_log_query,",");  
                                $kBooking->updateBookingFileDetails($file_log_query,$idBookingFile);  
                            } 
                        } 
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            } 
            else
            {
                return true;
            }
        }
    }
    
    function loadEmailLogs($szEmailKey)
    {
        if(!empty($szEmailKey))
        {
            $query="
                SELECT
                    id,
                    iMode,
                    idUser,
                    szToAddress,
                    idBooking,
                    szEmailStatus,
                    iSuccess,
                    iQuoteEmail,
                    iIgnoreTracking
                FROM
                    ".__DBC_SCHEMATA_EMAIL_LOG__."
                WHERE
                    szEmailKey = '".mysql_escape_custom($szEmailKey)."'	
            "; 
            //echo $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
        	// Check if the user exists
                if( $this->getRowCnt() > 0 )
                {
                    $row = $this->getAssoc($result);
                    return $row;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    
    function getAllStandardBooking()
    { 
        $query="
            SELECT 
                * 
            FROM 
                tblbookings b1 
            WHERE  
                b1.idBookingStatus IN (3,4,7) 
            AND
                iStandardPricing = '1' 
            AND
                idQuotePricingDetails>0
        "; 
        //echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            // Check if the user exists
            if( $this->getRowCnt() > 0 )
            {
                $ret_ary = array();
                $ctr=0;
                while($row = $this->getAssoc($result))
                {
                    $ret_ary[] = $row;
                    $ctr++;
                }
                return $ret_ary;
            }
            else
            {
                return false;
            }
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        } 
    }
    
    function getAllErronousbookings($idBooking)
    {  
        if($idBooking>0)
        {
            $query="
                SELECT qp.* 
                FROM `tblquotepricingdetails` qp
                WHERE `fTotalVatForwarderAccountCurrency` = 25
                AND `idCustomerCurrency` >0 AND dtQuoteValidTo>=now() AND qp.idBooking = '".$idBooking."'
            "; 
            //echo $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                // Check if the user exists
                if( $this->getRowCnt() > 0 )
                {
                    $ret_ary = array();
                    $ctr=0;
                    while($row = $this->getAssoc($result))
                    {
                        $ret_ary[] = $row;
                        $ctr++;
                    }
                    return $ret_ary;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
        } 
    }
    
    function getAllPendingTrayBookings()
    { 
        $query="
            SELECT qp.*, (SELECT b.id FROM tblbookings b WHERE b.id = qp.idBooking AND b.idBookingStatus NOT IN (3,4,7) ) as idPrimaryBooking
            FROM `tblquotepricingdetails` qp
            WHERE DATE(qp.`dtQuoteValidTo`) >= '2016-05-05' AND DATE(qp.dtCreatedOn)>='2016-01-01'
            AND qp.`idForwarderAccountCurrency` =0 HAVING idPrimaryBooking>0	
        "; 
        //echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            // Check if the user exists
            if( $this->getRowCnt() > 0 )
            {
                $ret_ary = array();
                $ctr=0;
                while($row = $this->getAssoc($result))
                {
                    $ret_ary[] = $row;
                    $ctr++;
                }
                return $ret_ary;
            }
            else
            {
                return false;
            }
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        } 
    }
    
    function getAllMarkupbookings()
    { 
        $query="
            SELECT id AS idBooking
            FROM `tblbookings` AS b
            WHERE b.`iForwarderGPType` =2
            AND b.`idBookingStatus`
            IN ( 3, 4, 7 )	
        "; 
        //echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            // Check if the user exists
            if( $this->getRowCnt() > 0 )
            {
                $ret_ary = array();
                $ctr=0;
                while($row = $this->getAssoc($result))
                {
                    $ret_ary[] = $row['idBooking'];
                    $ctr++;
                }
                return $ret_ary;
            }
            else
            {
                return false;
            }
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        } 
    }
    function getForwarderCurrency($idQuote)
    {
        $query = " 
            SELECT  
                f.szCurrency as idCurrency,
                f.id as idForwarder
            FROM
                ".__DBC_SCHEMATA_BOOKING_QUOTES__." q
            INNER JOIN
                ".__DBC_SCHEMATA_FORWARDERS__." f 
            ON
                q.idForwarder = f.id
            WHERE
                q.id = '".$idQuote."' 
        ";
        //echo "<br><br>".$query;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            // Check if the user exists
            if( $this->getRowCnt() > 0 )
            {
                $ret_ary = array();
                $ctr=0;
                $row = $this->getAssoc($result); 
                return $row;
            }
            else
            {
                return false;
            }
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        } 
    }
    
    function getCurrencyData($idCurrency,$dtExchangeRate)
    {
        $query = "
            SELECT
                rate.fUsdValue,
                cur.szCurrency,
                rate.idCurrency	 
            FROM
                ".__DBC_SCHEMATA_EXCHANGE_RATE__." rate
            INNER JOIN
                ".__DBC_SCHEMATA_CURRENCY__." cur
            ON
                cur.id = rate.idCurrency	
            WHERE
                rate.idCurrency = '".(int)$idCurrency."' 
            AND
                DATE(rate.dtDate) = '".  mysql_escape_custom($dtExchangeRate)."'
        ";
        //echo "<br>".$query ;

        if($result=$this->exeSQL($query))
        {
            if(!empty($idCurrencyAry))
            { 
                $row=$this->getAssoc($result);
                return $row; 
            }
            else
            {
                $row=$this->getAssoc($result);
                return $row;
            } 
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        } 
    }
    
    function testUpdatePendingQuote($quotePricingDetailsAry)
    {
        $kBooking = new cBooking();
        $kWHSSearch = new cWHSSearch();
        if(!empty($quotePricingDetailsAry))
        {
            foreach($quotePricingDetailsAry as $quotePricingDetailsArys)
            {
                $idBooking = $quotePricingDetailsArys['idBooking'];
                $idBookingFile = $quotePricingDetailsArys['idFile'];
                $idBookingQuote = $quotePricingDetailsArys['idQuote'];
                $idQuotePricingDetails = $quotePricingDetailsArys['id'];
                $iHandlingFeeApplicable = $quotePricingDetailsArys['iHandlingFeeApplicable'];

                $postSearchAry = array();
                $postSearchAry = $kBooking->getBookingDetails($idBooking);
                
                $bookingQuoteAry = array();
                $bookingQuoteAry = $kBooking->getAllBookingQuotesByFile(false,false,$idBookingQuote); 
                $bookingQuoteArys = $bookingQuoteAry['1'];

                $ret_ary = array();
                $ret_ary['idQuotePricingDetails'] = $idQuotePricingDetails ;
                $ret_ary['idFile'] = $idBookingFile ;
                $ret_ary['idTransportMode'] = $bookingQuoteArys['idTransportMode'];
                $ret_ary['szTransportMode'] = $bookingQuoteArys['szTransportMode'];
                $ret_ary['idForwarder'] = $bookingQuoteArys['idForwarder'];  

                if($ret_ary['idTransportMode']==__BOOKING_TRANSPORT_MODE_COURIER__) //Courier
                {
                    $ret_ary['isManualCourierBooking'] = 1;
                    $ret_ary['dtCutOff'] = $postSearchAry['dtTimingDate'];
                }
                else
                {
                    $ret_ary['isManualCourierBooking'] = 0;
                }

                $kForwarder = new cForwarder();
                $kForwarder->load($bookingQuoteArys['idForwarder']);
                $ret_ary['szForwarderDispName'] = $kForwarder->szDisplayName;
                $ret_ary['szForwarderRegistName'] = $kForwarder->szCompanyName;
                $ret_ary['szForwarderAddress']=$kForwarder->szAddress;
                $ret_ary['szForwarderAddress2']=$kForwarder->szAddress2;
                $ret_ary['szForwarderAddress3']=$kForwarder->szAddress3;
                $ret_ary['szForwarderPostCode']=$kForwarder->szPostCode;
                $ret_ary['szForwarderCity']=$kForwarder->szCity;
                $ret_ary['szForwarderState']=$kForwarder->szState;
                $ret_ary['idForwarderCountry']=$kForwarder->idCountry;
                $ret_ary['szForwarderLogo']=$kForwarder->szLogo; 
                $ret_ary['idForwarderCurrency'] = $kForwarder->szCurrency ;
                $ret_ary['iForwarderGPType'] = $kForwarder->iProfitType ; 

                if((int)$postSearchAry['iBookingStep']<9)
                {
                    $ret_ary['iBookingStep'] = 9;
                } 
                if($kForwarder->szCurrency==$quotePricingDetailsArys['idForwarderAccountCurrency'])
                {
                    $ret_ary['szForwarderCurrency'] = $quotePricingDetailsArys['szForwarderAccountCurrency'];
                    $ret_ary['fForwarderExchangeRate'] = $quotePricingDetailsArys['fForwarderAccountCurrencyExchangeRate'];
                }
                else
                {
                    if($kForwarder->szCurrency==1)
                    { 
                        $ret_ary['szForwarderCurrency'] = 'USD';
                        $ret_ary['fForwarderExchangeRate'] = 1;
                    }
                    else
                    {
                        $resultAry = $kWHSSearch->getCurrencyDetails($kForwarder->szCurrency);		 
                        $ret_ary['szForwarderCurrency'] = $resultAry['szCurrency'];
                        $ret_ary['fForwarderExchangeRate'] = $resultAry['fUsdValue'];
                    }
                }

                $fTotalPriceIncludingVat = $quotePricingDetailsArys['fTotalPriceCustomerCurrency'] + $quotePricingDetailsArys['fTotalVatCustomerCurrency'];
                $fTotalPriceIncludingVatUsd = ($fTotalPriceIncludingVat * $postSearchAry['fExchangeRate']) ;

                if($kForwarder->szCurrency==$postSearchAry['idCurrency'])
                {
                    $fCurrencyMarkupPrice = 0;
                    $fMarkupPercentage = 0;

                    $fTotalPriceNoMarkup = $quotePricingDetailsArys['fTotalPriceCustomerCurrency'];
                }
                else
                {
                    $fPriceMarkUp = $kWHSSearch->getManageMentVariableByDescription('__MARK_UP_PRICING_PERCENTAGE__'); 

                    /*
                     * We are calculating no currency markup price from price with mark-up for e.g
                     * fTotalPriceCustomerCurrency = 400
                     * fMarkupRate = 2.5
                     * fPriceNoMarkup = (400 * 100 )/(100+2.5) = (40000/102.5) = 390.2439
                     */
                    $fTotalPriceNoMarkup = ($quotePricingDetailsArys['fTotalPriceCustomerCurrency'] * 100 ) / (100+$fPriceMarkUp);
                    $fTotalVatNoMarkup = ($quotePricingDetailsArys['fTotalVatCustomerCurrency'] * 100 ) / (100+$fPriceMarkUp);

                    $fMarkupPercentage = $fPriceMarkUp ;
                    $fCurrencyMarkupPrice = ($quotePricingDetailsArys['fTotalPriceCustomerCurrency'] - $fTotalPriceNoMarkup) + ($quotePricingDetailsArys['fTotalVatCustomerCurrency'] - $fTotalVatNoMarkup) ;
                    $fCurrencyMarkupPriceUSD = ($fCurrencyMarkupPrice * $postSearchAry['fExchangeRate']) ;
                }
                if($kForwarder->szCurrency==$quotePricingDetailsArys['idForwarderAccountCurrency'])
                { 
                    $fTotalPriceForwarderAccountCurrency=$quotePricingDetailsArys['fTotalPriceForwarderAccountCurrency']+$quotePricingDetailsArys['fTotalVatForwarderAccountCurrency'];
                    $ret_ary['fTotalPriceForwarderCurrency'] = round((float)$fTotalPriceForwarderAccountCurrency,2);
                    $ret_ary['fTotalPriceNoMarkupForwarderCurrency'] = round((float)$fTotalPriceForwarderAccountCurrency,2);
                    $ret_ary['fTotalPriceNoMarkupForwarderCurrency_new'] = round((float)$fTotalPriceForwarderAccountCurrency,2);
                }
                else
                {
                    if($kForwarder->szCurrency==$postSearchAry['idCurrency'])
                    {
                        $ret_ary['fTotalPriceForwarderCurrency'] = round((float)$fTotalPriceIncludingVat,2);
                        $ret_ary['fTotalPriceNoMarkupForwarderCurrency'] = round((float)$fTotalPriceIncludingVat,2);
                        $ret_ary['fTotalPriceNoMarkupForwarderCurrency_new'] = round((float)$fTotalPriceIncludingVat,2);
                    }
                    else
                    { 
                        if($ret_ary['fForwarderExchangeRate']>0)
                        {
                            $ret_ary['fTotalPriceForwarderCurrency'] = ($fTotalPriceIncludingVatUsd/$ret_ary['fForwarderExchangeRate']);
                            $ret_ary['fTotalPriceNoMarkupForwarderCurrency'] = ( ($fTotalPriceIncludingVatUsd-$fCurrencyMarkupPriceUSD)/$ret_ary['fForwarderExchangeRate']);
                            $ret_ary['fTotalPriceNoMarkupForwarderCurrency_new'] = (($fTotalPriceIncludingVatUsd-$fCurrencyMarkupPriceUSD)/$ret_ary['fForwarderExchangeRate']);
                        }  
                    }
                } 
                $fTotalPriceUSD = $postSearchAry['fExchangeRate'] * $quotePricingDetailsArys['fTotalPriceCustomerCurrency']; 
                $fTotalPriceNoMarkupUSD = $fTotalPriceNoMarkup * $postSearchAry['fExchangeRate'] ;

                $ret_ary['iIncludeComments'] = $quotePricingDetailsArys['iInsuranceComments'];
                $ret_ary['szForwarderComment'] = $quotePricingDetailsArys['szForwarderComment']; 

                if($quotePricingDetailsArys['iInsuranceComments']==1)
                {
                    $ret_ary['szOtherComments'] = $quotePricingDetailsArys['szForwarderComment']; 
                } 

                $kConfig = new cConfig();
                $transportModeListAry = array();
                $transportModeListAry = $kConfig->getAllTransportMode($bookingQuoteArys['idTransportMode']);  

                $iBookingLanguage = getLanguageId();
                $kConfig = new cConfig();
                $configLangArr=$kConfig->getConfigurationLanguageData('__TABLE_TRANSPORT_MODE__',$iBookingLanguage);
                
                $ret_ary['szFrequency'] = $configLangArr[$bookingQuoteArys['idTransportMode']]['szFrequency']; 
                
//                if($iBookingLanguage==__LANGUAGE_ID_DANISH__)
//                {
//                    $ret_ary['szFrequency'] = $transportModeListAry[0]['szFrequencyDanish']; 
//                }
//                else
//                {
//                    $ret_ary['szFrequency'] = $transportModeListAry[0]['szFrequency']; 
//                }

                $ret_ary['idForwarderQuoteCurrency'] = $quotePricingDetailsArys['idForwarderCurrency'];
                $ret_ary['szForwarderQuoteCurrency'] = $quotePricingDetailsArys['szForwarderCurrency'];
                $ret_ary['fForwarderQuoteCurrencyExchangeRate'] = $quotePricingDetailsArys['fForwarderCurrencyExchangeRate']; 
                $ret_ary['fForwarderTotalQuotePrice'] = $quotePricingDetailsArys['fTotalPriceForwarderCurrency']; 

                $ret_ary['fPriceMarkupCustomerCurrency'] = $fCurrencyMarkupPrice;
                $ret_ary['fMarkupPercentage'] = $fMarkupPercentage;
                $ret_ary['fTotalPriceUSD'] = $fTotalPriceUSD ;
                $ret_ary['fTotalPriceNoMarkupUSD'] = $fTotalPriceNoMarkupUSD ;
                $ret_ary['fTotalVatForwarderCurrency'] = $quotePricingDetailsArys['fTotalVat'];
                $ret_ary['iTransitHours'] = $quotePricingDetailsArys['iTransitDays']; 
                $ret_ary['fReferalPercentage'] = $quotePricingDetailsArys['fReferalPercentage'];

                //$ret_ary['fReferalAmount'] = round((float)(($ret_ary['fTotalPriceForwarderCurrency'] * $quotePricingDetailsArys['fReferalPercentage'])/100),2); 

                if($ret_ary['iForwarderGPType']==__FORWARDER_PROFIT_TYPE_MARK_UP__)
                {
                    if($postSearchAry['fExchangeRate']>0)
                    {
                        $ret_ary['fReferalAmountUSD'] = ($quotePricingDetailsArys['fReferalAmount'] * $postSearchAry['fExchangeRate']) ; 
                    }
                    if($ret_ary['fForwarderExchangeRate']>0)
                    {
                        $ret_ary['fReferalAmount'] = ($ret_ary['fReferalAmountUSD'] / $ret_ary['fForwarderExchangeRate']) ; 
                    }
                }
                else
                {
                    $fReferalQuoteCurrency = ($ret_ary['fForwarderTotalQuotePrice'] + $ret_ary['fTotalVatForwarderCurrency']) * .01 * $ret_ary['fReferalPercentage'];
                    $ret_ary['fReferalAmountUSD'] = ($fReferalQuoteCurrency * $ret_ary['fForwarderQuoteCurrencyExchangeRate']) ;
                    if($ret_ary['fForwarderExchangeRate']>0)
                    {
                        $ret_ary['fReferalAmount'] = ($ret_ary['fReferalAmountUSD'] / $ret_ary['fForwarderExchangeRate']) ; 
                    } 
                }

                $ret_ary['fTotalPriceCustomerCurrency'] = $quotePricingDetailsArys['fTotalPriceCustomerCurrency'];
                $ret_ary['fTotalVat'] = $quotePricingDetailsArys['fTotalVatCustomerCurrency']; 
                $ret_ary['dtQuoteValidTo'] = $quotePricingDetailsArys['dtQuoteValidTo'] ; 
                $ret_ary['fTotalInsuranceCostForBooking'] = $quotePricingDetailsArys['fTotalInsuranceCostForBooking']; ;
                $ret_ary['fTotalInsuranceCostForBookingCustomerCurrency'] = $quotePricingDetailsArys['fTotalInsuranceCostForBookingCustomerCurrency']; ;
                $ret_ary['fTotalInsuranceCostForBookingUSD'] = $quotePricingDetailsArys['fTotalInsuranceCostForBookingUSD']; ;
                $ret_ary['fTotalAmountInsured'] = $quotePricingDetailsArys['fTotalAmountInsured'];
                $res_ary['iMinrateApplied'] = $quotePricingDetailsArys['iMinrateApplied'];
                $ret_ary['idInsuranceRate'] = $quotePricingDetailsArys['idInsuranceSellCurrency'];
                $ret_ary['fInsuranceUptoPrice'] = $quotePricingDetailsArys['fInsuranceUptoPrice'];
                $ret_ary['idInsuranceUptoCurrency'] = $quotePricingDetailsArys['idInsuranceUptoCurrency'];
                $ret_ary['szInsuranceUptoCurrency'] = $quotePricingDetailsArys['szInsuranceUptoCurrency'];
                $ret_ary['fInsuranceUptoExchangeRate'] = $quotePricingDetailsArys['fInsuranceUptoExchangeRate'];
                $ret_ary['fInsuranceRate'] = $quotePricingDetailsArys['fInsuranceRate'];
                $ret_ary['fInsuranceMinPrice'] = $quotePricingDetailsArys['fInsuranceMinPrice'];
                $ret_ary['idInsuranceMinCurrency'] = $quotePricingDetailsArys['idInsuranceMinCurrency']; 
                $ret_ary['szInsuranceMinCurrency'] = $quotePricingDetailsArys['szInsuranceMinCurrency'];
                $ret_ary['fInsuranceMinExchangeRate'] = $quotePricingDetailsArys['fInsuranceMinExchangeRate'];
                $ret_ary['fInsuranceUptoPriceUSD'] = $quotePricingDetailsArys['fInsuranceUptoPriceUSD'];
                $ret_ary['fInsuranceMinPriceUSD'] = $quotePricingDetailsArys['fInsuranceMinPriceUSD'];

                $ret_ary['idInsuranceRate_buyRate'] = $quotePricingDetailsArys['idInsuranceRate_buyRate'];
                $ret_ary['fInsuranceUptoPrice_buyRate'] = $quotePricingDetailsArys['fInsuranceUptoPrice_buyRate'];
                $ret_ary['idInsuranceUptoCurrency_buyRate'] = $quotePricingDetailsArys['idInsuranceUptoCurrency_buyRate'];
                $ret_ary['szInsuranceUptoCurrency_buyRate'] = $quotePricingDetailsArys['szInsuranceUptoCurrency_buyRate'];
                $ret_ary['fInsuranceUptoExchangeRate_buyRate'] = $quotePricingDetailsArys['fInsuranceUptoExchangeRate_buyRate'];
                $ret_ary['fInsuranceRate_buyRate'] = $quotePricingDetailsArys['fInsuranceRate_buyRate'];
                $ret_ary['fInsuranceMinPrice_buyRate'] = $quotePricingDetailsArys['fInsuranceMinPrice_buyRate'];
                $ret_ary['idInsuranceMinCurrency_buyRate'] = $quotePricingDetailsArys['idInsuranceMinCurrency_buyRate'];
                $ret_ary['szInsuranceMinCurrency_buyRate'] = $quotePricingDetailsArys['szInsuranceMinCurrency_buyRate'];
                $ret_ary['fInsuranceMinExchangeRate_buyRate'] = $quotePricingDetailsArys['fInsuranceMinExchangeRate_buyRate'];
                $ret_ary['fInsuranceUptoPriceUSD_buyRate'] = $quotePricingDetailsArys['fInsuranceUptoPriceUSD_buyRate'];
                $ret_ary['fInsuranceMinPriceUSD_buyRate'] = $quotePricingDetailsArys['fInsuranceMinPriceUSD_buyRate'];

                $ret_ary['fTotalInsuranceCostForBooking_buyRate'] = $quotePricingDetailsArys['fTotalInsuranceCostForBooking_buyRate'];
                $ret_ary['fTotalInsuranceCostForBookingCustomerCurrency_buyRate'] = $quotePricingDetailsArys['fTotalInsuranceCostForBookingCustomerCurrency_buyRate'];
                $ret_ary['fTotalInsuranceCostForBookingUSD_buyRate'] = $quotePricingDetailsArys['fTotalInsuranceCostForBookingUSD_buyRate'];
                $ret_ary['iHandlingFeeApplicable'] = $iHandlingFeeApplicable;
                $ret_ary['fTotalHandlingFeeUSD'] = $quotePricingDetailsArys['fTotalHandlingFeeUSD']; 

                if($postSearchAry['fExchangeRate']>0)
                {
                    $ret_ary['fTotalHandlingFeeCustomerCurrency'] = round((float)($quotePricingDetailsArys['fTotalHandlingFeeUSD'] / $postSearchAry['fExchangeRate']),2);
                }
                if($ret_ary['fForwarderQuoteCurrencyExchangeRate']>0)
                {
                    $ret_ary['fTotalHandlingFeeQuoteCurrency'] = round((float)($quotePricingDetailsArys['fTotalHandlingFeeUSD'] / $ret_ary['fForwarderQuoteCurrencyExchangeRate']),2);
                }
                if($ret_ary['fForwarderExchangeRate']>0)
                {
                    $ret_ary['fTotalHandlingFeeForwarderCurrency'] = round((float)($quotePricingDetailsArys['fTotalHandlingFeeUSD'] / $ret_ary['fForwarderExchangeRate']),2);
                }  
                if(!empty($ret_ary))
                {
                    $update_query = '';
                    foreach($ret_ary as $key=>$value)
                    {
                        $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                }   
                $update_query = rtrim($update_query,",");
                if($kBooking->updateDraftBooking($update_query,$idBooking))
                {  
                    $postSearchAry = array();
                    $postSearchAry = $kBooking->getBookingDetails($idBooking);
                    $isManualCourierBooking = $postSearchAry['isManualCourierBooking'];

                    $szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
                    $kConfig = new cConfig();
                    $kConfig->loadCountry($postSearchAry['idOriginCountry']);
                    $szCountryStrUrl = $kConfig->szCountryISO ;
                    $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
                    $szCountryStrUrl .= $kConfig->szCountryISO ;

                    if($iHandlingFeeApplicable==1)
                    {
                        $addHandlingFeeDetailsAry = array();
                        $addHandlingFeeDetailsAry['idBooking'] = $idBooking;
                        $addHandlingFeeDetailsAry['idForwarder'] = $bookingQuoteArys['idForwarder'];
                        $addHandlingFeeDetailsAry['iProductType'] = $quotePricingDetailsArys['iProductType']; 
                        $addHandlingFeeDetailsAry['idCourierProvider'] = $quotePricingDetailsArys['idCourierProvider'];
                        $addHandlingFeeDetailsAry['idCourierProviderProduct'] = $quotePricingDetailsArys['idCourierProviderProduct'];
                        $addHandlingFeeDetailsAry['idHandlingCurrency'] = $quotePricingDetailsArys['idHandlingCurrency'];
                        $addHandlingFeeDetailsAry['szHandlingCurrency'] = $quotePricingDetailsArys['szHandlingCurrency'];
                        $addHandlingFeeDetailsAry['fHandlingCurrencyExchangeRate'] = $quotePricingDetailsArys['fHandlingCurrencyExchangeRate'];
                        $addHandlingFeeDetailsAry['fHandlingFeePerBooking'] = $quotePricingDetailsArys['fHandlingFeePerBooking'];
                        $addHandlingFeeDetailsAry['fHandlingMarkupPercentage'] = $quotePricingDetailsArys['fHandlingMarkupPercentage']; 
                        $addHandlingFeeDetailsAry['idHandlingMinMarkupCurrency'] = $quotePricingDetailsArys['idHandlingMinMarkupCurrency'];
                        $addHandlingFeeDetailsAry['szHandlingMinMarkupCurrency'] = $quotePricingDetailsArys['szHandlingMinMarkupCurrency'];
                        $addHandlingFeeDetailsAry['fHandlingMinMarkupCurrencyExchangeRate'] = $quotePricingDetailsArys['fHandlingMinMarkupCurrencyExchangeRate'];
                        $addHandlingFeeDetailsAry['fHandlingMinMarkupPrice'] = $quotePricingDetailsArys['fHandlingMinMarkupPrice']; 

                        $kBooking->updateBookingsHandlingFeeDetails($addHandlingFeeDetailsAry,$idBooking);
                    }

                    if($isManualCourierBooking==1)
                    {
                        $kCourierService = new cCourierServices();
                        $iLabelCreatedByForwarder = $kCourierService->checkAllCourierProductPricing($postSearchAry); 
                        if($iLabelCreatedByForwarder)
                        {
                            $iBookingIncluded = 1;
                        }
                        else
                        {
                            $iBookingIncluded = 0;
                        }
                        $updateBookingAry = array();
                        $updateBookingAry['szCurrencyCode'] = $postSearchAry['szCurrency'];
                        $updateBookingAry['iBookingIncluded'] = $iBookingIncluded;
                        $kBooking->updateCourierServiceDetails($updateBookingAry,$idBooking); 	
                    }   
                }   
            }
        }
    }
    
    function convertBookingFinancialV1toV2($idBooking,$szBookingType)
    {
        if($idBooking>0)
        {
            if($szBookingType=='RFQ')
            { 
                $kBooking = new cBooking(); 
                $postSearchAry = array();
                $postSearchAry = $kBooking->getBookingDetails($idBooking);

                if($postSearchAry['idQuotePricingDetails']>0)
                {
                    $data = array();
                    $data['idBookingQuotePricing'] = $postSearchAry['idQuotePricingDetails'];
                    $quotePricingDetailsAry = array();
                    $quotePricingDetailsAry = $kBooking->getAllBookingQuotesPricingDetails($data);

                    $ret_ary = array();
                    $ret_ary['iFinancialVersion'] = __TRANSPORTECA_FINANCIAL_VERSION__;
                    
                    if(!empty($quotePricingDetailsAry))
                    {
                        foreach($quotePricingDetailsAry as $quotePricingDetailsArys)
                        {
                            $fTotalSelfInvoiceAmountQuoteCurrency = $quotePricingDetailsArys['fTotalPriceForwarderCurrency'];
                            
                            /*
                            * Rounding the value when forwarder and customer currency are same. 
                            */
                            if($quotePricingDetailsArys['idForwarderCurrency']==$postSearchAry['idCustomerCurrency'])
                            { 
                                $fTotalSelfInvoiceAmountQuoteCurrency = round((float)$fTotalSelfInvoiceAmountQuoteCurrency);
                            }
                            
                            $ret_ary['fReferalPercentage'] = $quotePricingDetailsArys['fReferalPercentage'];
                            $fReferalAmount = round((float)($fTotalSelfInvoiceAmountQuoteCurrency * $ret_ary['fReferalPercentage'] * 0.01),2); 
                            $fForwarderCurrencyExchangeRate = $quotePricingDetailsArys['fForwarderCurrencyExchangeRate'];
                            $fForwarderExchangeRate = $ret_ary['fForwarderExchangeRate'];
                            
                            if($quotePricingDetailsArys['idForwarderAccountCurrency']==$quotePricingDetailsArys['idForwarderCurrency'])
                            {
                                $ret_ary['fReferalAmount'] = $fReferalAmount;
                                $ret_ary['fReferalAmountUSD'] = ($fReferalAmount * $fForwarderCurrencyExchangeRate) ;

                                //Deducting Referal fee from Self invoiced amount
                                $fTotalSelfInvoiceAmountQuoteCurrency = ($fTotalSelfInvoiceAmountQuoteCurrency - $fReferalAmount);
                                $ret_ary['fTotalSelfInvoiceAmount'] = $fTotalSelfInvoiceAmountQuoteCurrency;
                            }
                            else 
                            {
                                $ret_ary['fReferalAmountUSD'] = ($fReferalAmount * $fForwarderCurrencyExchangeRate) ;
                
                                $fTotalSelfInvoiceAmountUSD = $fTotalSelfInvoiceAmountQuoteCurrency * $fForwarderCurrencyExchangeRate;

                                if($ret_ary['fForwarderExchangeRate']>0)
                                {
                                    $ret_ary['fReferalAmount'] = ($ret_ary['fReferalAmountUSD'] / $ret_ary['fForwarderExchangeRate']) ;  
                                    $fTotalForwarderPriceWithoutHandlingFee = $fTotalForwarderPriceWithoutHandlingFeeUSD / $ret_ary['fForwarderExchangeRate'];
                                    $fTotalSelfInvoiceAmount = $fTotalSelfInvoiceAmountUSD / $ret_ary['fForwarderExchangeRate'];

                                    //deducting Referral Fee from self invoice amount
                                    $fTotalSelfInvoiceAmount = $fTotalSelfInvoiceAmount - $ret_ary['fReferalAmount'];
                                    $ret_ary['fTotalSelfInvoiceAmount'] = $fTotalSelfInvoiceAmount;
                                }
                            }
                        } 
                    } 

                    if(!empty($ret_ary))
                    {
                        $update_query = '';
                        foreach($ret_ary as $key=>$value)
                        {
                            $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                        }
                    }   
                    $update_query = rtrim($update_query,",");
                    if($kBooking->updateDraftBooking($update_query,$idBooking))
                    {

                    }
                } 
            }
        }  
    }
    
    function download_billing_table($data,$dateRange,$idForwarder,$path,$amount,$date,$dateAry)
    {	
        if(!empty($data))
        {
            $kBooking = new cBooking();
            $billing_searched_data = $kBooking->download_forwarder_billings($data,$idForwarder);
                
            if(empty($dateAry['dtFromDate']))
            {
                $dateAry['dtFromDate'] = "2012-01-01"; //Transporteca's Launch date
            }
            
            $kBilling= new cBilling();    
            //for current balance//
            $currenctBalaceArr = array();
            $currenctBalaceArr = $kBilling->getNetBalancePerForwarder($idForwarder,$dateAry);
 
            if(!empty($currenctBalaceArr))
            {
                foreach($currenctBalaceArr as $szCurrency=>$currenctBalaceArrs)
                {
                    if(!empty($szCurrency))
                    {
                        $currentAvailableAmountAry[$szCurrency]['fTotalBalance'] = $currenctBalaceArrs['fTotalBalance'];
                    }
                }
            } 
            $sheetIndex=0;
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->createSheet();
            $objPHPExcel->setActiveSheetIndex($sheetIndex);
            if(!empty($billing_searched_data))
            {	

                $styleArray = array(
                                    'font' => array(
                                        'bold' => false,
                                        'size' =>12,
                                    ),
                                    'alignment' => array(
                                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                                    ),
                                    'borders' => array(
                                        'allborders' => array(
                                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                                        'color' => array('argb' => '000000'),
                                    ),
                                ),
                                'fill' => array(
                                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                    'startcolor' => array(
                                    'argb' => 'DDD9C3',
                                    ),
                                'endcolor' => array(
                                'argb' => 'FFFFFF',
                                ),
                            ),
                        );

                        $styleArray1 = array(
                        'font' => array(
                                'bold' => false,
                                'size' =>12,
                        ),
                        'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                        ),
                );		
                $objPHPExcel->getActiveSheet()->mergeCells('A1:C1');
                $objPHPExcel->getActiveSheet()->mergeCells('D1:G1');
                $objPHPExcel->getActiveSheet()->mergeCells('H1:K1');
                $objPHPExcel->getActiveSheet()->mergeCells('L1:X1');
                $objPHPExcel->getActiveSheet()->mergeCells('Y1:AA1');
                $objPHPExcel->getActiveSheet()->mergeCells('AB1:AE1');
                $objPHPExcel->getActiveSheet()->mergeCells('AF1:AI1');
                $objPHPExcel->getActiveSheet()->mergeCells('AJ1:AK1');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,1,$dateRange);//5 sub fields
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,1,'Invoice');//5 sub fields
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,1,'Balance');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11,1,'Customer');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(24,1,'Service');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(27,1,'Origin CFS');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(31,1,'Destination CFS');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(35,1,'Cargo');

                $objPHPExcel->getActiveSheet()->getStyle('A1:AK1')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('A2:AK2')->applyFromArray($styleArray);
                $col=2;
                $width=20;
                for($i = 'A'; $i !== 'AL'; $i++) 
                {
                        $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setWidth($width);
                }
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);	
                $objPHPExcel->getActiveSheet()->freezePane('A3');

                //$objPHPExcel->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1,1);

                $colcounter=0;
                $rowcounter=2;



                $heading = array ('Date/Time (GMT)','Description','Your reference','Number','Value','Currency','ROE','Credit','Debit','Actual Value','Currency','Company','Registration Number','Address Line 1','Address Line 2','Address Line 3','Postcode','City','Provice/Region/State','Country','First Name','Last Name','E-mail','Phone Number','Transportation','Origin Customs Clearance','Destination Customs Clearance','CFS Name','City','Country','Cut off (local time)','CFS Name','City','Country','Availability (local time)','Volume (cbm)','Weight (kg)');

                foreach($heading as $title)
                {
                    static $count=0;
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($count,2,$title);

                    $count++;
                }		
                $col=3; 
                $amount = $amount['iCreditBalance']; 

                $kAdmin = new cAdmin();
                $billingDataAry = array();
                $billingFinalDataAry = array();
                foreach($billing_searched_data as $billing_searched_datas)
                {  			 
                    $szServiceType=''; 
                    if($billing_searched_datas['idServiceType']==__SERVICE_TYPE_DTD__) //DTD
                    {
                        $szServiceType = "DTD" ;
                    }
                    if($billing_searched_datas['idServiceType']==__SERVICE_TYPE_DTW__) //DTW
                    {
                        $szServiceType = "DTW" ;
                    }
                    if($billing_searched_datas['idServiceType']==__SERVICE_TYPE_WTD__) //WTD
                    {
                        $szServiceType = "WTD" ;
                    }
                    if($billing_searched_datas['idServiceType']==__SERVICE_TYPE_WTW__) //WTW
                    {
                        $szServiceType = "WTW" ;
                    }
                    if($billing_searched_datas['idServiceType']==__SERVICE_TYPE_DTP__) //DTP
                    {
                        $szServiceType = "DTP" ;
                    }
                    if($billing_searched_datas['idServiceType']==__SERVICE_TYPE_PTD__) //PTD
                    {
                        $szServiceType = "PTD" ;
                    } 
                    if((int)$billing_searched_datas['idServiceType']==__SERVICE_TYPE_WTP__) //WTP
                    { 
                        $szServiceType = "WTP" ;
                    }
                    if($billing_searched_datas['idServiceType']==__SERVICE_TYPE_PTW__) //PTW
                    { 
                        $szServiceType = "PTW" ;
                    }
                    if($billing_searched_datas['idServiceType']==__SERVICE_TYPE_PTP__) //PTP
                    {
                        $szServiceType = "PTP" ;
                    } 

                    $billingDataAry['szValueA'] = $billing_searched_datas['dtPaymentConfirmed'];
                    $billingDataAry['szValueB'] = $billing_searched_datas['szBooking'];
                    $billingDataAry['szValueC'] = $billing_searched_datas['szBookingNotes'];


                    if($billing_searched_datas['iDebitCredit']==2 || $billing_searched_datas['iFinancialVersion']==2) // Automatic transfer and Financial Version: 2 Invoice fields are always empty.
                    { 
                        $billingDataAry['szValueD'] = "";
                        $billingDataAry['szValueE'] = "";
                        $billingDataAry['szValueF'] = "";
                        $billingDataAry['szValueG'] = "";
                    }
                    else
                    {
                        if(trim($billing_searched_datas['szBooking'])=='Transporteca referral fee' || $billing_searched_datas['szBooking']=='Transporteca upload service' || $billing_searched_datas['szBooking']=='Courier Booking and Labels Invoice' || $billing_searched_datas['szBooking']=='Courier Booking and Labels Credit')
                        {
                            $szForwarderInvoice = "'".$billing_searched_datas['szForwarderInvoice']."";
                        }
                        else
                        {
                            $szForwarderInvoice = $billing_searched_datas['szForwarderInvoice'];
                        }
                        $billingDataAry['szValueD'] = $szForwarderInvoice;  
                        if(trim($billing_searched_datas['szBooking'])=='Transporteca referral fee' || trim($billing_searched_datas['szBooking'])=='Automatic transfer' || $billing_searched_datas['szBooking']=='Transporteca upload service' || $billing_searched_datas['szBooking']=='Courier Booking and Labels Invoice' || $billing_searched_datas['szBooking']=='Courier Booking and Labels Credit')
                        {
                            if($billing_searched_datas['szBooking']=='Courier Booking and Labels Credit')
                            {
                                $billing_searched_datas['fTPriceForwarderCurrency']=str_replace("-","",$billing_searched_datas['fTPriceForwarderCurrency']);
                            }    
                            $billingDataAry['szValueE'] = round($billing_searched_datas['fTPriceForwarderCurrency'],2); 
                        }
                        else if($billing_searched_datas['iDebitCredit']==6)
                        { 
                            $cancelled_value = round($billing_searched_datas['fTAmount1'],2); 
                            $billingDataAry['szValueE'] = $cancelled_value; 
                        }
                        else
                        {
                            if($billing_searched_datas['fTAmount1']>0)
                            {
                                $fTotalAmount = round($billing_searched_datas['fTAmount1'],2);
                            }
                            else
                            {
                                $fTotalAmount = '';
                            }
                            $billingDataAry['szValueE'] = $fTotalAmount;  
                        }				
                        // for Starting Balance
                        if($billing_searched_datas['iDebitCredit']==4)
                        { 
                            $billingDataAry['szValueF'] = "";  
                        }
                        else if($billing_searched_datas['iDebitCredit']==5)
                        {
                            $billingDataAry['szValueF'] = $billing_searched_datas['szFCurrency'];   
                        }
                        else
                        {
                            $billingDataAry['szValueF'] = $billing_searched_datas['szCurrencyName'];   
                        }

                        if($billing_searched_datas['iDebitCredit']==3 || $billing_searched_datas['iDebitCredit']==5 || $billing_searched_datas['iDebitCredit']==8) // Transporteca refferal fee ROE must always be 1
                        { 
                            $billingDataAry['szValueG'] = '1.0000';   
                        }
                        else
                        {
                            if($billing_searched_datas['fERate']>0)
                            {
                                $fTotalROE = number_format($billing_searched_datas['fERate'],4);
                            }
                            else
                            {
                                $fTotalROE = '';
                            }		
                            $billingDataAry['szValueG'] = $fTotalROE;    
                        }
                    }
                    if($billing_searched_datas['iDebitCredit']==1 || $billing_searched_datas['szBooking']=='Courier Booking and Labels Credit')
                    { 
                        $billingDataAry['szValueH'] = round($billing_searched_datas['fTPriceForwarderCurrency'],2);    
                    }
                    else
                    {
                        $billingDataAry['szValueH'] = "";
                    }
                    if($billing_searched_datas['iDebitCredit']==2 || $billing_searched_datas['iDebitCredit']==3 || $billing_searched_datas['iDebitCredit']==5 || $billing_searched_datas['iDebitCredit']==6 || $billing_searched_datas['szBooking']=='Courier Booking and Labels Invoice')
                    {
                        $billingDataAry['szValueI'] = round($billing_searched_datas['fTPriceForwarderCurrency'],2);    
                    }  
                    else
                    {
                        $billingDataAry['szValueI'] = "";
                    }
                    if($billing_searched_datas['iDebitCredit']==2 || $billing_searched_datas['iDebitCredit']==3)
                    {
                        $idfbdCurency = $billing_searched_datas['szCurrencyName'] ; 
                    }	
                    else
                    {
                        $idfbdCurency = $billing_searched_datas['szFCurrency'] ; 						
                    }

                    $billing_searched_datas['fTotalPriceForwarderCurrency'] = $billing_searched_datas['fTPriceForwarderCurrency'];
                    if($billing_searched_datas['iDebitCredit']==1 || $billing_searched_datas['iDebitCredit']==13)
                    {
                        $idCurency = $billing_searched_datas['szFCurrency'] ;
                        $fTotalPriceForwarderCurrency = $billing_searched_datas['fTPriceForwarderCurrency'];  
                        if($billing_searched_datas['iDebitCredit']==13)
                        {
                            $sumAry[$idCurency] = (float)(round((float)$currentAvailableAmountAry[$idCurency]['fTotalBalance'],2) + $fTotalPriceForwarderCurrency);
                        }
                        else
                        {
                            $sumAry[$idCurency] = (float)($currentAvailableAmountAry[$idCurency]['fTotalBalance'] + $fTotalPriceForwarderCurrency);
                        }
                        $currentAvailableAmountAry[$idCurency]['fTotalBalance'] = $sumAry[$idCurency];
                    }
                    else if(($billing_searched_datas['iDebitCredit']==2) || (($billing_searched_datas['iDebitCredit']==3 || $billing_searched_datas['iDebitCredit']==8 || $billing_searched_datas['iDebitCredit']==12) && $billing_searched_datas['iStatus']==2))
                    {
                        $idCurency = $billing_searched_datas['szCurrencyName'];
                        $fTotalCourierLabelPaid = 0;
                        if($billing_searched_datas['iDebitCredit']==2)
                        {
                            //Incase of Automatic Transfer We are deducting Label Fee
                            $CreateLabelBatch = $billing_searched_datas['szInvoice'] + 1;
                            $szInvoice_formatted = $kAdmin->getFormattedString($CreateLabelBatch,6);
                            $customer_code = $szInvoice_formatted ; 
                            $totalCourierLabelArr=$kAdmin->totalAmountCourierLabelFee($customer_code);  
                            
                            if(!empty($totalCourierLabelArr))
                            {
                                $fTotalCourierLabelPaid = $totalCourierLabelArr[0]['totalCurrencyValue']; 
                                $billing_searched_datas['fTotalPriceForwarderCurrency'] = $billing_searched_datas['fTotalPriceForwarderCurrency']-$fTotalCourierLabelPaid;
                            }
                        }
                        
                        $fTotalPriceForwarderCurrency = round($billing_searched_datas['fTotalPriceForwarderCurrency'],2);
                        $sumAry[$idCurency] = round($currentAvailableAmountAry[$idCurency]['fTotalBalance'],2) - $fTotalPriceForwarderCurrency;  
                        $currentAvailableAmountAry[$idCurency]['fTotalBalance'] = $sumAry[$idCurency];  
                    }
                    else if(($billing_searched_datas['iDebitCredit']==6 || $billing_searched_datas['iDebitCredit']==7)) // Cancelled booking
                    {
                        $idCurency = $billing_searched_datas['szFCurrency'];
                        
                        $fTotalPriceForwarderCurrency = round($billing_searched_datas['fTotalPriceForwarderCurrency'],2);
                        $sumAry[$idCurency] = round($currentAvailableAmountAry[$idCurency]['fTotalBalance'],2) - $fTotalPriceForwarderCurrency;   
                        $currentAvailableAmountAry[$idCurency]['fTotalBalance'] = $sumAry[$idCurency];  
                    }
                    else if($billing_searched_datas['iDebitCredit']=='5')
                    {
                        $idCurency = $billing_searched_datas['szFCurrency']; 
                        $sumAry[$idCurency] = (float)($currentAvailableAmountAry[$idCurency]['fTotalBalance'] - $billing_searched_datas['fTotalPriceForwarderCurrency']); 
                        $currentAvailableAmountAry[$idCurency]['fTotalBalance'] = $sumAry[$idCurency];
                    } 
                    else if($searchResults['iDebitCredit']=='4')
                    {
                        $idCurency = $billing_searched_datas['szFCurrency'];  
                        $sumAry[$idCurency] = "0.00";
                    }
                    //echo "<br><br> Currency: ".$idCurency." Price: ".$billing_searched_datas['fTotalPriceForwarderCurrency']." J: ".$sumAry[$idCurency]." curr: ".$currentAvailableAmountAry[$idCurency]['fTotalBalance'];
                    $billingDataAry['szValueJ'] = $sumAry[$idCurency];        
                    if($billing_searched_datas['iDebitCredit']==1)
                    {
                        $amount = $amount - $billing_searched_datas['fTPriceForwarderCurrency']; 
                    }
                    if($billing_searched_datas['iDebitCredit']!=1)
                    {
                        $amount = $amount + $billing_searched_datas['fTPriceForwarderCurrency'];
                    } 
                    
                    if(trim($billing_searched_datas['szBooking'])=='Transporteca referral fee' || trim($billing_searched_datas['szBooking'])=='Automatic transfer' || trim($billing_searched_datas['szBooking'])=='Transporteca upload service')
                    {
                        $billingDataAry['szValueK'] = $billing_searched_datas['szFCurrency'];    
                    } 
                    else
                    {
                        $billingDataAry['szValueK'] = $billing_searched_datas['szFCurrency'];    
                    } 

                    $billingDataAry['szValueL'] = $billing_searched_datas['szCustomerCompanyName'];    
                    $billingDataAry['szValueM'] = $billing_searched_datas['szCustomerCompanyRegNo'];    
                    $billingDataAry['szValueN'] = $billing_searched_datas['szCustomerAddress1'];   
                    $billingDataAry['szValueO'] = $billing_searched_datas['szCustomerAddress2'];    
                    $billingDataAry['szValueP'] = $billing_searched_datas['szCustomerAddress3'];    
                    $billingDataAry['szValueQ'] = $billing_searched_datas['szCustomerPostCode'];    

                    $billingDataAry['szValueR'] = $billing_searched_datas['szCustomerCity'];   
                    $billingDataAry['szValueS'] = $billing_searched_datas['szCustomerState'];    
                    $billingDataAry['szValueT'] = $kBooking->findCountryName($billing_searched_datas['szCustomerCountry']);    
                    $billingDataAry['szValueU'] = $billing_searched_datas['szFirstName'];

                    $billingDataAry['szValueV'] = $billing_searched_datas['szLastName'];
                    $billingDataAry['szValueW'] = $billing_searched_datas['szEmail'];
                    $billingDataAry['szValueX'] = $billing_searched_datas['szCustomerPhoneNumber'];
                    $billingDataAry['szValueY'] = $szServiceType; 

                    if(trim($billing_searched_datas['szBooking'])=='Transporteca referral fee' || trim($billing_searched_datas['szBooking'])=='Automatic transfer' || $billing_searched_datas['szBooking']=='Transporteca upload service')
                    {    
                        $billingDataAry['szValueZ'] = "";   
                    }
                    else
                    {
                        $billingDataAry['szValueZ'] = $billing_searched_datas['iOriginCC']?'Yes':'No';   
                    } 

                    if(trim($billing_searched_datas['szBooking'])=='Transporteca referral fee' || trim($billing_searched_datas['szBooking'])=='Automatic transfer' || $billing_searched_datas['szBooking']=='Transporteca upload service')
                    {
                        $billingDataAry['szValueAA'] = "";   
                    } 
                    else
                    {
                        $billingDataAry['szValueAA'] = $billing_searched_datas['iDestinationCC']?'Yes':'No'; 
                    } 

                    $billingDataAry['szValueAB'] = $billing_searched_datas['szWarehouseFromName'];
                    $billingDataAry['szValueAC'] = $billing_searched_datas['szWarehouseFromCity'];
                    $billingDataAry['szValueAD'] = $kBooking->findCountryName($billing_searched_datas['idWarehouseFromCountry']); 

                    if($billing_searched_datas['szBooking']=='Transporteca upload service')
                    {
                        $billingDataAry['szValueAE'] = "";
                    } 
                    else
                    {
                        $billingDataAry['szValueAE'] = $billing_searched_datas['dtWhsCutOff'];
                    } 
                    $billingDataAry['szValueAF'] = $billing_searched_datas['szWarehouseToName'];
                    $billingDataAry['szValueAG'] = $billing_searched_datas['szWarehouseToCity'];
                    $billingDataAry['szValueAH'] = $kBooking->findCountryName($billing_searched_datas['idWarehouseToCountry']);


                    if($billing_searched_datas['szBooking']=='Transporteca upload service')
                    {
                        $billingDataAry['szValueAI'] = "";
                    }
                    else
                    {
                        $billingDataAry['szValueAI'] = $billing_searched_datas['dtWhsAvailabe'];
                    }  

                    $volume=strval(round((float)$billing_searched_datas['fCargoVolume'],3));
                    if(trim($billing_searched_datas['szBooking'])=='Transporteca referral fee' || trim($billing_searched_datas['szBooking'])=='Automatic transfer' || $billing_searched_datas['szBooking']=='Transporteca upload service')
                    {
                        $billingDataAry['szValueAJ'] = "";
                    }
                    else
                    {
                        $billingDataAry['szValueAJ'] = format_volume($volume);
                    } 
                    $billingDataAry['szValueAK'] = $billing_searched_datas['fCargoWeight'];   
                    $billingDataAry['idForwarderTransaction'] = $billing_searched_datas['idForwarderTransaction'];  
                            
                    $counter++;
                    $billingFinalDataAry[$counter] = $billingDataAry;
                }    
                if(!empty($billingFinalDataAry))
                {
                    $col=3; 
                    $billingFinalDataAry = array_reverse($billingFinalDataAry); 
                    foreach($billingFinalDataAry as $billingFinalDataArys)
                    {
                        
                        $objPHPExcel->getActiveSheet()->getStyle('X'.$col)->getNumberFormat()->setFormatCode('#0');
                        $objPHPExcel->getActiveSheet()->getStyle('D'.$col)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);	
                
                
                        $objPHPExcel->getActiveSheet()->getStyle('G'.$col)->getNumberFormat()->setFormatCode('#,##0.0000');
                        $objPHPExcel->getActiveSheet()->getStyle('H'.$col)->getNumberFormat()->setFormatCode('#,##0.00');
                        $objPHPExcel->getActiveSheet()->getStyle('I'.$col)->getNumberFormat()->setFormatCode('#,##0.00');
                        $objPHPExcel->getActiveSheet()->getStyle('AJ'.$col)->getNumberFormat()->setFormatCode('#,##0.0');
                        
                        
                        $objPHPExcel->getActiveSheet()->setCellValue('A'.$col,$billingFinalDataArys['szValueA']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('B'.$col,$billingFinalDataArys['szValueB']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('C'.$col,$billingFinalDataArys['szValueC']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('D'.$col,$billingFinalDataArys['szValueD']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('E'.$col,$billingFinalDataArys['szValueE']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('F'.$col,$billingFinalDataArys['szValueF']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('G'.$col,$billingFinalDataArys['szValueG']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('H'.$col,$billingFinalDataArys['szValueH']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('I'.$col,$billingFinalDataArys['szValueI']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('J'.$col,round((float)$billingFinalDataArys['szValueJ'],2)); 
                        $objPHPExcel->getActiveSheet()->setCellValue('K'.$col,$billingFinalDataArys['szValueK']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('L'.$col,$billingFinalDataArys['szValueL']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('M'.$col,$billingFinalDataArys['szValueM']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('N'.$col,$billingFinalDataArys['szValueN']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('O'.$col,$billingFinalDataArys['szValueO']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('P'.$col,$billingFinalDataArys['szValueP']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('Q'.$col,$billingFinalDataArys['szValueQ']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('R'.$col,$billingFinalDataArys['szValueR']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('S'.$col,$billingFinalDataArys['szValueS']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('T'.$col,$billingFinalDataArys['szValueT']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('U'.$col,$billingFinalDataArys['szValueU']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('V'.$col,$billingFinalDataArys['szValueV']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('W'.$col,$billingFinalDataArys['szValueW']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('X'.$col,$billingFinalDataArys['szValueX']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('Y'.$col,$billingFinalDataArys['szValueY']);  
                        $objPHPExcel->getActiveSheet()->setCellValue('Z'.$col,$billingFinalDataArys['szValueZ']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('AA'.$col,$billingFinalDataArys['szValueAA']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('AB'.$col,$billingFinalDataArys['szValueAB']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('AC'.$col,$billingFinalDataArys['szValueAC']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('AD'.$col,$billingFinalDataArys['szValueAD']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('AE'.$col,$billingFinalDataArys['szValueAE']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('AF'.$col,$billingFinalDataArys['szValueAF']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('AG'.$col,$billingFinalDataArys['szValueAG']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('AH'.$col,$billingFinalDataArys['szValueAH']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('AI'.$col,$billingFinalDataArys['szValueAI']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('AJ'.$col,$billingFinalDataArys['szValueAJ']); 
                        $objPHPExcel->getActiveSheet()->setCellValue('AK'.$col,$billingFinalDataArys['szValueAK']); 
                        
                        $objPHPExcel->getActiveSheet()->getStyle('M'.$col)->applyFromArray($styleArray1);
                        $objPHPExcel->getActiveSheet()->getStyle('Q'.$col)->applyFromArray($styleArray1);
                        $objPHPExcel->getActiveSheet()->getStyle('X'.$col)->applyFromArray($styleArray1);
                        $col++; 
                    }
                    
                    $title = "Transporteca_Account_Details_";
                    $objPHPExcel->getActiveSheet()->setTitle('Transaction History');
                    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                    $objPHPExcel->setActiveSheetIndex(0);
                    $objPHPExcel->removeSheetByIndex(1);
                    $file=$title.DATE('Ymd');
                    $fileName=__APP_PATH_ROOT__."/".$path."/forwarderBulkExportImport/forwarderDetails/".$file.".xlsx";
                    $objWriter->save($fileName);
                    return $file;
                } 
            }
        }
    }
    
    function getGrossProfitByBooking($idBooking)
    {
        if($idBooking>0)
        {
            $query="
                SELECT *
                    FROM  
                    (
                    SELECT	
                        SUM(fTotalInsuranceCostForBookingUSD) AS fTotalInsuranceSellPriceUSD		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__."
                    WHERE
                    (  
                        idBookingStatus IN(3,4) 
                    AND
                        iInsuranceIncluded = '1'
                    AND
                        iInsuranceStatus !='3' 
                    AND
                        id='".(int)$idBooking."'

                    )
                ) AS fTotalInsuranceSellPrice,
                (
                    SELECT	
                        SUM(fTotalInsuranceCostForBookingUSD_buyRate) AS fTotalInsuranceBuyPriceUSD		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__."
                    WHERE
                    (  
                        idBookingStatus IN(3,4) 
                    AND
                        iInsuranceIncluded = '1'
                    AND
                        iInsuranceStatus !='3'
                    AND
                        id='".(int)$idBooking."'
                    )
                ) AS fTotalInsuranceBuyPrice, 
                (
                    SELECT	
                        SUM(fReferalAmountUSD) AS fTotalReferalAmountUSD		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__."
                    WHERE 
                        idBookingStatus IN(3,4) 
                    AND
                        id='".(int)$idBooking."'
                ) AS fTotalReferalAmount, 
                (
                    SELECT  
                        SUM(cbd.fBookingLabelFeeRate * cbd.fBookingLabelFeeROE ) AS fTotalLabelFeeAmountUSD     
                    FROM
                        ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__." AS cbd
                    INNER JOIN
                        ".__DBC_SCHEMATA_BOOKING__." as bt
                    ON
                        bt.id=cbd.idBooking
                    WHERE 
                        bt.idBookingStatus IN(3,4)  
                    AND
                        cbd.iCourierAgreementIncluded = '0'    
                    AND
                        bt.id='".(int)$idBooking."'
                    AND
                    (
                        bt.iCourierBooking='1'
                    OR
                        bt.isManualCourierBooking='1'
                    )    
                ) AS fTotalLabelFeeAmount, 
                (
                    SELECT  
                        SUM(fTotalHandlingFeeUSD) AS fTotalBookingHandlingFeeUSD        
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__."
                    WHERE 
                        idBookingStatus IN(3,4)
                    AND
                        iHandlingFeeApplicable='1'
                    AND
                        id='".(int)$idBooking."'
                ) AS fTotalBookingHandlingFeeUSD,
                (
                    SELECT	
                        SUM(fPriceMarkupCustomerCurrency * fCustomerExchangeRate) AS fPriceMarkupUSD		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__." as b4
                    WHERE 
                        idBookingStatus IN(3,4) 
                    AND
                        b4.id='".(int)$idBooking."'
                ) AS fTotalPriceMarkupCustomerCurrency 
            ";
            //echo $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                $row=$this->getAssoc($result);    
                $fSaleRevenue = round((float)(($row['fTotalInsuranceSellPriceUSD'] - $row['fTotalInsuranceBuyPriceUSD']) + $row['fTotalReferalAmountUSD'] + $row['fTotalLabelFeeAmountUSD'] + $row['fTotalBookingHandlingFeeUSD'] + $row['fPriceMarkupUSD']),2); 
                return $fSaleRevenue;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
}
?>