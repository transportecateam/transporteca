<?php
/**
 * This file is the container for Explain related functionality.
 * All functionality related to Explain details should be contained in this class.
 *
 * explain.class.php
 *
 * @copyright Copyright (C) 2012 Transporteca, LLC
 * @author Anil
 * @package Transporteca Development
 */
 
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );

require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_INC__ . "/functions.php" );
require_once( __APP_PATH_CLASSES__ . "/database.class.php");

class cExplain extends cDatabase
{
    /**
     * These are the variables for Explanaition Details.
     */
    var $id; 
    var $idExplain; 
    var $szUrl;
   	var $idAdmin;
   	var $maxOrder;
   	var $iOrderBy;
   	var $szComment;
   	var $szPageHeading;
   	var $szLink;
   	var $szMetaTitle;
   	var $szMetaKeyword;
   	var $szMetaDescription;
   	var $iActive;
 	var $iPublished;
 	var $szTextA; 
 	var $szTextB; 
	var $szTextC; 
	var $szTextD; 
	var $szTextE; 
	var $szTextF; 
 	var $szTextG; 
 	var $szTextH; 
 	var $szTextI; 
 	var $szTextJ; 
 	var $szTextK; 
 	var $szTextL; 
 	var $szTextM; 
	var $dtCreatedOn; 
 	   
   	var $t_base="management/Error/";
        var $t_base_search_notification="management/uploadService/";
   	
    function selectPageDetails($idExplain = 0,$iExplainPageType=false,$idLanguage=false)
	{
		$this->set_idExplain((int)$idExplain);
		
		if( $this->idExplain > 0 )
		{
			$add_query = "
					WHERE
						id = '".(int)$this->idExplain."'
				";
			if($iExplainPageType>0)
			{
				$add_query .=" AND
						iExplainPageType = '".(int)$iExplainPageType."' " ;
			}
		}
		else
		{
			if($iExplainPageType>0)
			{
				$add_query ="  WHERE iExplainPageType = '".(int)$iExplainPageType."' " ;
			}
			$query_order_by = "
					ORDER BY 
						iOrderBy ASC
				";
		}
		
		if($idLanguage>0)
		{
			$add_query .= " AND iLanguage = '".(int)$idLanguage."' "; 
		}
		
		$query = "
				SELECT
					`id`,
					`iOrderBy`,
					`szPageHeading`,
					`szLink`,
					`szMetaTitle`, 
					`szMetaKeyWord`, 
					`szMetaDescription`, 
					`iActive`,
					iLanguage,
					szSubTitle,
					szHeaderImage,
					iExplainPageType,
					szHeaderImage as szUploadFileName,
					szPictureTitle,
					szPictureDescription,
					szLinkTitle,
					szOpenGraphAuthor,
					szOpenGraphImage,
					szOpenGraphImage as szUploadOGFileName,
                                        szLinkedPageUrl
				FROM
					".__DBC_SCHEMATA_EXPLAIN_DATA__."	
				".$add_query."
				$query_order_by
			";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			$links = array();
			while($row=$this->getAssoc($result))
			{
				$links[] = $row;
			}
			return $links;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}	 
	function selectExplainId($szUrl,$iExplainPageType=1,$iLanguage=1,$idExplainData=false)
	{
            if($idExplainData<=0)
            {
		$this->set_szUrl(sanitize_all_html_input($szUrl));
            }
            
            if($szUrl != '' || $idExplainData>0)
            {
                if($idExplainData>0)
                {
                    $query_and = " id = '".(int)$idExplainData."' ";
                }
                else
                {
                    $query_and = "  
                              szLink = '".mysql_escape_custom($this->szUrl)."'	
                            AND
                                iExplainPageType = '".(int)$iExplainPageType."'
                            AND
                                iLanguage='".(int)$iLanguage."'	
                    ";
                }

                $query = "
                    SELECT
                        id,
                        szLink,
                        szPageHeading,
                        szMetaTitle,
                        szMetaKeyWord,
                        szMetaDescription,
                        iLanguage,
                        iExplainPageType,
                        szHeaderImage,
                        szSubTitle,
                        iOrderBy,
                        szPictureTitle,
                        szPictureDescription,
                        szLinkTitle,
                        szOpenGraphAuthor,
                        szOpenGraphImage,
                        szLinkedPageUrl
                    FROM
                        ".__DBC_SCHEMATA_EXPLAIN_DATA__."	
                    WHERE
                        $query_and
                    ORDER BY 
                        iOrderBy ASC					
                ";
            }
            else
            {
                $query = "
                    SELECT
                        szLink			
                    FROM
                            ".__DBC_SCHEMATA_EXPLAIN_DATA__."	
                    WHERE 
                            iExplainPageType = '".(int)$iExplainPageType."'
                    ORDER BY 
                            id
                    LIMIT 0,1 						
                ";
            }	
            //echo $query;
            if($result=$this->exeSQL($query))
            {
                    $row=$this->getAssoc($result);
                    return $row;
            }
            else
            {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
            }
	}
        function selectPageDetailsUserEndByType($iLanguage)
        { 
            if($iLanguage>0)
            {
                $query_and = "AND iLanguage = '".(int)$iLanguage."' ";
            } 

            $query = "
                SELECT
                    `szPageHeading`,
                    iExplainPageType,
                    `szLink`,
                    szMetaTitle,
                    szMetaKeyWord,
                    szMetaDescription,
                    dtCreatedOn,
                    dtUpdatedOn,
                    iLanguage,
                    id,
                    szLinkTitle,
                    szOpenGraphAuthor,
                    szOpenGraphImage,
                    szLinkedPageUrl,
                    szPictureTitle,
                    szHeaderImage
                FROM
                    ".__DBC_SCHEMATA_EXPLAIN_DATA__."	
                WHERE
                    iActive = '1'			
                    $query_and	
                ORDER BY 
                    iOrderBy ASC
            ";
           
            if($result=$this->exeSQL($query))
            {
                $links = array();
                while($row=$this->getAssoc($result))
                {
                    $links[$row['iExplainPageType']][] = $row;
                }
                return $links;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
	function selectPageDetailsUserEnd($iLanguage=false,$iExplainPageType=false,$idExplainData=false)
	{
		if($iLanguage>0)
		{
                    $query_and = "AND iLanguage = '".(int)$iLanguage."' ";
		}
		
		if($iExplainPageType>0)
		{
                    $query_and .= " AND iExplainPageType = '".(int)$iExplainPageType."' " ;
		}
		
		$query = "
                    SELECT
                        `szPageHeading`,
                        `szLink`,
                        szMetaTitle,
                        szMetaKeyWord,
                        szMetaDescription,
                        dtCreatedOn,
                        dtUpdatedOn,
                        iLanguage,
                        id,
                        szLinkTitle,
                        szOpenGraphAuthor,
                        szOpenGraphImage,
                        szLinkedPageUrl
                    FROM
                        ".__DBC_SCHEMATA_EXPLAIN_DATA__."	
                    WHERE
                        iActive = '1'			
                        $query_and	
                    ORDER BY 
                        iOrderBy ASC
                ";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			$links = array();
			while($row=$this->getAssoc($result))
			{
				$links[] = $row;
			}
			return $links;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}	 
	function text_editor($idAdmin,$idExplain,$id = 0)
	{
		$this->set_idAdmin((int)$idAdmin);
		$this->set_id((int)$id);
		$this->set_idExplain((int)$idExplain);
		if( $this->idAdmin > 0 )
		{	
			if($this->id == 0)
			{
				$query="
					SELECT
						szPageName,
						iOrderBy,
						szDraftPageName
					FROM 
						".__DBC_SCHEMATA_EXPLAIN_CONTENT__."
					WHERE
						idExplainData  = '".(int)$this->idExplain."'
					AND
						iOrderByDraft>0		
					ORDER BY
						iOrderByDraft
						";	
			}
			if($id>0)
			{
				$query="
					SELECT 
						id,
						szDraftHeading,
						szDraftDescription,
						szUrl,
						szDraftPictureTitle,
						szDraftMetaKeyWord,
						szDraftMetaDescription,
						szDraftPicture,
						szMetaTitle,
						szPageName,
						szBackgroundColor,
						szDraftPageName,
						szLinkColor
					FROM 
						".__DBC_SCHEMATA_EXPLAIN_CONTENT__."	
					WHERE 
						id='".(int)$this->id."'	
					AND
							idExplainData  = '".(int)$this->idExplain."'		
						";
			}
			//echo $query;	
			if( ( $result = $this->exeSQL( $query ) ) )
			{					
				$textEditor=array();
				if($this->id == 0)
				{
					$content = $this->findDraftExplainData($this->idExplain);
				}
				while($row=$this->getAssoc($result))
				{	
					if($this->id>0)
					{
						$textEditor=$row;
					}
					else if($this->id==0)
					{
						$textEditor[]=$row;
					}
				}
					if($this->id == 0)
					{
						$i=0;
						$NewArray = array();
						if(count($textEditor)> count($content))
						{	
							foreach($textEditor as $value) { 
							$NewArray[] = array_merge($value,($content[$i]?$content[$i]:array()));
							$i++;
							}
						}
						else
						{
							if($content!= array())
							{
								foreach($content as $value) {
									$NewArray[] = array_merge(($textEditor[$i]?$textEditor[$i]:array()),$content[$i]);
									    $i++;
									}
							}
						}
						return $NewArray;
					}
					else
					{
						return $textEditor;
					}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}	
	}
	function selectPublishStatus($idExplainContent)
	{
		$this->set_idExplain((int)$idExplainContent);	
	
	$query = "	
		SELECT 
			count(id)
		FROM
			".__DBC_SCHEMATA_EXPLAIN_CONTENT__."
		WHERE
			(
				iOrderByDraft='0' 
			OR
				iOrderBy= '0'
			OR 
				iUpdated = '1'		
			)	
			AND idExplainData = '".(int)$this->idExplain."'
			";
		
		//echo $query."<br>";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{	
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	function findExplainContent($idExplain)
	{
		$this->set_idExplain((int)$idExplain);	
		$query="
				SELECT
						id,
						iUpdated,
						szDraftHeading,
						iOrderByDraft
					FROM 
						".__DBC_SCHEMATA_EXPLAIN_CONTENT__."
					WHERE
						iOrderByDraft!='0'	
					AND
						idExplainData = '".(int)$this->idExplain."'	
					ORDER BY
						iOrderByDraft
						";
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				if( $this->getRowCnt() > 0 )
				{	
					$textEditor=array();
					while( $row=$this->getAssoc($result) )
					{	
						if( $this->idExplain>0 )
						{
							$textEditor=$row;
						}
						else if( $this->idExplain==0 )
						{
							$textEditor[]=$row;
						}
					}
						return $textEditor;
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function getMaxOrderExplain($idExplain)
	{	
		$this->set_idExplain((int)$idExplain);	
		$query="
			SELECT 
				MAX(iOrderByDraft)  as orders  
			FROM 
				".__DBC_SCHEMATA_EXPLAIN_CONTENT__."
			WHERE
				idExplainData = '".(int)$this->idExplain."'		
			";
		
		if($result=$this->exeSQL($query))
		{
			$row = $this->getAssoc( $result );
			if($row['orders']>0)
			{
				return $row['orders'];
			}
			else
			{
				return 0;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function validate_explain_data($createArr,$data,$id,$idExplain)
	{
		$this->set_id((int)$id);
		$heading=str_replace("'",'&#39;',$createArr['heading']);
	
		//$this->set_szMetaKeyword(sanitize_all_html_input($createArr['szMetaKeyword']));	
		//$this->set_szMetaDescription(sanitize_all_html_input($createArr['szMetaDescription'])); 
		//$this->set_szLink(sanitize_all_html_input($createArr['szUrl']));
		//$this->set_szMetaTitle(sanitize_all_html_input($createArr['szMetaTitle']));
		//$this->set_szPageName(sanitize_all_html_input($createArr['szPageName']));
		$this->set_szLinkTitle(sanitize_all_html_input($createArr['szPageName']));
		$this->set_szUrl(sanitize_all_html_input($createArr['szUrl']),true,true);
		$this->set_szBackgroundColor(sanitize_all_html_input($createArr['szBackgroundColor']));  
		$this->set_szHeading(sanitize_all_html_input($heading),false);
		$data=str_replace("'",'&#39;',$data);
		$this->set_szDescription($data);
		$this->set_idExplain((int)$idExplain);
		
		if($this->error=== true)
		{
			return false;	
		}
		else
		{
			$this->szPageName = $this->szLinkTitle ;
			$this->szLink = $this->szUrl ; 
			return true;
		}
	}
	function saveExplainData($createArr,$data,$id,$idExplain)
	{		
		$this->validate_explain_data($createArr,$data,$id,$idExplain);
		if($this->error=== true)
		{
			return false;	
		}
		else
		{	
			$data=$this->checkExistingHeadingExplainData($this->szHeading,$this->id,$this->idExplain);
			if($data==true)
			{	
				$this->addError( "szHeading" , t($this->t_base.'messages/Heading_exists') );
				return false;
			} 
			if($this->checkAlreadyUrlExist($this->szLink,$this->idExplain,$id))
			{
				$this->addError( "szLink" , t($this->t_base.'fields/szAchorTageAlreadyExists') );
			} 
			
			if($id>0)
			{	
			
				//$szCleanUrl=$this->explainDatasCleanTitle($this->szHeading,$id);
				$query="
					UPDATE 
						".__DBC_SCHEMATA_EXPLAIN_CONTENT__."	
					SET
						szDraftHeading='".mysql_escape_custom($this->szHeading)."',	
						szDraftDescription='".mysql_escape_custom($this->szDescription)."',
						iUpdated = '1',
						dtUpdated = NOW(),
						szUrl='".mysql_escape_custom($this->szLink)."',
						szDraftPictureTitle='".mysql_escape_custom($this->szPictureTitle)."',	
						szDraftMetaKeyWord='".mysql_escape_custom($this->szMetaKeyword)."',	
						szDraftMetaDescription='".mysql_escape_custom($this->szMetaDescription)."',	
						szDraftPicture ='".mysql_escape_custom($this->szPicture)."',						
						szMetaTitle ='".mysql_escape_custom($this->szMetaTitle)."',
						szDraftPageName ='".mysql_escape_custom($this->szPageName)."',
						szBackgroundColor ='".mysql_escape_custom($this->szBackgroundColor)."',
						szLinkColor='".mysql_escape_custom($this->szLinkColor)."'
					WHERE
						id='".(int)$this->id."'	
					";
			}
			
			if($id==0)
			{	
				//$szCleanUrl=$this->explainDatasCleanTitle($this->szHeading);
				$this->maxOrder=(int)$this->getMaxOrderExplain((int)$this->idExplain);
				$query="
					INSERT INTO
						".__DBC_SCHEMATA_EXPLAIN_CONTENT__."
					(
						szDraftHeading,	
						szDraftDescription,
						iActive,
						iUpdated,
						iOrderByDraft,
						idExplainData,
						dtUpdated,
						szUrl,
						szDraftPictureTitle,
						szDraftMetaKeyWord,
						szDraftMetaDescription,
						szDraftPicture,
						szMetaTitle,
						szDraftPageName,
						szBackgroundColor,
						szLinkColor
					)
					VALUES
					(
						'".mysql_escape_custom($this->szHeading)."',	
						'".mysql_escape_custom($this->szDescription)."',
						'0',
						'1',
						'".(int)($this->maxOrder+1)."',
						".(int)$this->idExplain.",
						NOW(),
						'".mysql_escape_custom($this->szLink)."',
						'".mysql_escape_custom($this->szPictureTitle)."',
						'".mysql_escape_custom($this->szMetaKeyword)."',
						'".mysql_escape_custom($this->szMetaDescription)."',
						'".mysql_escape_custom($this->szPicture)."',
						'".mysql_escape_custom($this->szMetaTitle)."',
						'".mysql_escape_custom($this->szPageName)."',
						'".mysql_escape_custom($this->szBackgroundColor)."',
						'".mysql_escape_custom($this->szLinkColor)."'
					)
				";
			}	
			//echo $query;die;
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				$kSEO = new cSEO();
				$kSEO->updateSitemapXmlFile();
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}	
	}
	
	//function validate_
	
	function checkAlreadyUrlExist($Title,$idExplain,$id=0)
	{
		$whereQuery='';
		if((int)$id>0)
		{
			$whereQuery="
				AND
					id<>'".(int)$id."'
			";		
		}		
			$query=
		 		"SELECT
		 			 `id`
		 		 FROM
		 		 	  ".__DBC_SCHEMATA_EXPLAIN_CONTENT__."
		 		 WHERE
		 			  `szUrl` = '".mysql_escape_custom($Title)."'
		 		AND
		 			idExplainData='".(int)$idExplain."'
		 			   $whereQuery	
		 	";
			//echo $query;
			
			$result = $this->exeSQL( $query );
		
			if ($this->getRowCnt() > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
			
		
	}  
	function checkExistingHeadingExplainData($heading,$id=0,$idExplain)
	{	
		$this->set_szHeading(sanitize_all_html_input($heading));
		$this->set_idExplain((int)$idExplain);
		$this->set_id((int)$id);
		if($this->heading)
		{
			$query="
				SELECT 
					id 
				FROM 
					".__DBC_SCHEMATA_EXPLAIN_CONTENT__."
				WHERE 
					szDraftHeading='".mysql_escape_custom($this->heading)."'
				AND 
					iActive='1'	
				AND
					idExplainData = '".(int)$this->idExplain."'	
					";
			
			if($this->id>0)
			{
				$query.="AND 
						id<>'".(int)$this->id."'
					";
			}		
			//echo $query;
			if($result = $this->exeSQL( $query ) )
			{
				if($this->getRowCnt() > 0)
				{
					return true;	
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function findDraftExplainData($idExplain)
	{
		$this->set_idExplain((int)$idExplain);
		$query="
				SELECT
					id,
					szHeading,
					szDraftHeading,
					szDescription,
					szDraftDescription,
					iOrderBy,
					iOrderByDraft,
					iUpdated,
					iActive,
					idExplainData,
					dtUpdated
				FROM 
						".__DBC_SCHEMATA_EXPLAIN_CONTENT__."
					WHERE
						iOrderByDraft!='0'
					AND
						idExplainData = '".(int)$this->idExplain."'			
					ORDER BY
						iOrderByDraft
						";
			//echo $query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				if( $this->getRowCnt() > 0 )
				{	
					$textEditor=array();
					
					while( $row=$this->getAssoc($result) )
					{	
						$textEditor[]=$row;
					}						
						return $textEditor;
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function moveUpDownExplain($id,$orderBy)
	{	
		$this->set_id((int)$id);
		$this->set_iOrderBy((int)$orderBy);
		$query="
   				SELECT
   					idExplainData,
   			 		iOrderByDraft
   			 	FROM
   			 		".__DBC_SCHEMATA_EXPLAIN_CONTENT__."
   			 	WHERE
   			 		id='".(int)$this->id."'
   				";
			//echo $query;
   			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				if ($this->getRowCnt() > 0)
				{
					$row = $this->getAssoc( $result);
					$order = $row['iOrderByDraft'];
					$this->idExplain = $row['idExplainData'];
					
					$query="
						UPDATE
							".__DBC_SCHEMATA_EXPLAIN_CONTENT__."
					 	SET
					 		iUpdated = '1',
					 		";
					if($this->iOrderBy==__ORDER_MOVE_DOWN__)
					{
						$query.="
								
								iOrderByDraft=iOrderByDraft-1
						 		";
					}
					if($this->iOrderBy==__ORDER_MOVE_UP__)
					{
						$query.="
								iOrderByDraft=iOrderByDraft+1
						 		";
					}
					$query.="
						WHERE
					 		id='".(int)$this->id."'
					 	AND
					 		idExplainData = '".(int)$this->idExplain."'	
						 ";
					//echo $query;
                	if($result = $this->exeSQL( $query ))
					{
						$query="
							UPDATE
								".__DBC_SCHEMATA_EXPLAIN_CONTENT__."
							SET
							iUpdated = '1',
							";
						if($this->iOrderBy==__ORDER_MOVE_DOWN__)
						{
							$query.="
									iOrderByDraft=iOrderByDraft+1
							 		";
						}
						if($this->iOrderBy==__ORDER_MOVE_UP__)
						{
							$query.="
									iOrderByDraft=iOrderByDraft-1
							 		";
						}
						$query.="
							WHERE
								id!='".$this->id."'
							AND
						 		idExplainData = '".(int)$this->idExplain."'		
							AND
								";
						if($this->iOrderBy==__ORDER_MOVE_DOWN__)
						{
							$query.="
									iOrderByDraft=$order-1
							 		";
						}
						if($this->iOrderBy==__ORDER_MOVE_UP__)
						{
							$query.="
									iOrderByDraft=$order+1
							 		";
						}
						//echo $query;
						if($result = $this->exeSQL( $query ))
						{
							//$this->updateversionTnc($option);
							return true;
						}
						else
						{
							$this->error = true;
							$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
							$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
							return false;
						}
					}
					else
					{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function deleteTermsExplain($id)
	{	
		$this->set_id((int)$id);
		if($this->id>0)
		{	
			$query="
   				SELECT
   					idExplainData,
   			 		iOrderByDraft
   			 	FROM
   			 		".__DBC_SCHEMATA_EXPLAIN_CONTENT__."
   			 	WHERE
   			 		id='".(int)$this->id."'
   				";
			//echo $query;
   			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				if ($this->getRowCnt() > 0)
				{
					$row = $this->getAssoc( $result);
					$order = $row['iOrderByDraft'];
					$this->idExpalin = $row['idExplainData'];
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
			
			$query="
				UPDATE
					".__DBC_SCHEMATA_EXPLAIN_CONTENT__."
				SET	
					szDraftHeading='',
					szDraftDescription='',
					iOrderByDraft = '0',
					iUpdated = '0'
				WHERE
					id='".(int)$this->id."'
				AND
					idExplainData = '".(int)$this->idExpalin."'
				";
			//echo $query;DIE;
			if($result = $this->exeSQL( $query ) )
			{
				if($this->iNumRows>0)
				{	
					$query="
						UPDATE
							".__DBC_SCHEMATA_EXPLAIN_CONTENT__."
					 	SET
							iOrderByDraft=iOrderByDraft-1
						 WHERE
					 		iOrderByDraft>'".(int)$order."'
					 	AND
							idExplainData = '".(int)$this->idExpalin."'		
				 ";
                		//echo $query;DIE;
					 if( $result = $this->exeSQL($query))
					 {
					 	/*
					 	 * Updating content of transportecaSitemap.xml file 
					 	 */
					 	$kSEO = new cSEO();
					 	$kSEO->updateSitemapXmlFile();
					 	
						//$this->updateversionTnc($option);	
						return true;
					 }
					 else
					 {
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					 }
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function findPubishStatus($idExplain)
	{
		$this->set_idExplain($idExplain);
		$query = "	
			SELECT 
				id	
			FROM
				".__DBC_SCHEMATA_EXPLAIN_CONTENT__."
			WHERE
				(
					iOrderByDraft=0 
				OR
					iOrderBy=0
				OR 
					iUpdated =1	
				)	
			AND
				idExplainData = '".(int)$this->idExplain."'	
				";
			
		//echo $query."<br>";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{	
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	function removeExplainDraftData($idExplain)
	{		
		$this->set_idExplain($idExplain);
		$query="
				UPDATE 
					".__DBC_SCHEMATA_EXPLAIN_CONTENT__."	
				SET
					szDraftHeading = szHeading,	
					szDraftDescription = szDescription,
					iUpdated ='0',
					iOrderByDraft = iOrderBy
				WHERE
					(szDescription!='' || szDescription!='NULL')
				AND
					(szHeading!='' || szHeading!='NULL')	
				AND 
					(iOrderBy!=0 || iOrderBy!='NULL')	
				AND 
					idExplainData = '".(int)$this->idExplain."'		
				";
			//echo $query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				$this->deleteExplainData($idExplain,'CANCEL');
				
				/*
				 * Updating content of transportecaSitemap.xml file
				*/
				$kSEO = new cSEO();
				$kSEO->updateSitemapXmlFile();
				
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}	
	}
	function deleteExplainData($idExplain,$mode= '')
	{	
		
		$this->set_idExplain((int)$idExplain);
		if($mode == 'CANCEL')
		{
			$addQuery = "		
							iOrderBy = '0'
						AND
				";
		}
		else if($mode == 'SUBMIT')
		{
			$addQuery = "	
							iOrderByDraft = '0'
						AND
				";
		}
		else
		{
			$addQuery = "";
		}
		$query="
			DELETE 
				FROM 
					".__DBC_SCHEMATA_EXPLAIN_CONTENT__."
				WHERE	
					".$addQuery."
					idExplainData = '".(int)$this->idExplain."'		
			";
		//echo $query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{	
			/*
			 * Updating content of transportecaSitemap.xml file
			*/
			$kSEO = new cSEO();
			$kSEO->updateSitemapXmlFile();
			
			return true;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}	
	}
	function submitExpalinDraftData($id)
	{	
		$id =$this->set_id((int)$id);
		if($this->id>0)
		{	
			$query="
				UPDATE 
					".__DBC_SCHEMATA_EXPLAIN_CONTENT__."	
				SET
					szHeading= szDraftHeading,	
					szDescription=szDraftDescription,
					dtUpdated = NOW(),
					iUpdated ='0',
					iOrderBy=iOrderByDraft,
					szPictureTitle=szDraftPictureTitle,	
					szMetaKeyWord=szDraftMetaKeyWord,	
					szMetaDescription=szDraftMetaDescription,	
					szPicture=szDraftPicture,
					szPageName=szDraftPageName,
					iActive ='1'
				WHERE
					(szDraftDescription!='' || szDraftDescription!='NULL')
				AND
					(szDraftHeading!='' || szDraftHeading!='NULL')	
				AND 
					(iOrderByDraft!=0 || iOrderByDraft!='NULL')	
				AND
					idExplainData = '".(int)$this->id."'				
				";
			//echo $query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				$this->deleteExplainData($this->id,'SUBMIT');
				
				/*
				 * Updating content of transportecaSitemap.xml file
				*/
				$kSEO = new cSEO();
				$kSEO->updateSitemapXmlFile();
				
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}	
		}
		else
		{
			return false;
		}
	}
	function selectQueryPreviewData($id)
	{	
		$this->set_id((int)$id);
		
		$query="
			SELECT
				 szDraftHeading,
				 szDraftDescription
			FROM
				".__DBC_SCHEMATA_EXPLAIN_CONTENT__."
			WHERE
				iActive = '1'
			AND 
				(iOrderByDraft!='' OR iOrderByDraft!=0)	
			AND
				idExplainData = '".(int)$this->id."'					
			ORDER BY 
				iOrderByDraft			
			";	
			if( ( $result = $this->exeSQL( $query ) ) )
			{
				$dataArr = array();
				while($row=$this->getAssoc($result))
				{
					$dataArr[] = $row;	
				}
				return $dataArr;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}	
			
			
	}
	function selectPreviewData($id)
	{
		$this->set_id((int)$id);
		$query="
			SELECT
				 szHeading,
				 szDescription
			FROM
				".__DBC_SCHEMATA_EXPLAIN_CONTENT__."
			WHERE
				iActive = '1'
			AND 
				(iOrderBy!='' OR iOrderBy!=0)	
			AND
				idExplainData = '".(int)$this->id."'					
			ORDER BY 
				iOrderBy			
			";	
			if( ( $result = $this->exeSQL( $query ) ) )
			{
				$dataArr = array();
				while($row=$this->getAssoc($result))
				{
					$dataArr[] = $row;	
				}
				return $dataArr;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}		
	}
	function searchExplanation($searchField,$iLanguage=false)
	{
		$this->set_searchField(sanitize_all_html_input($searchField));
		if($iLanguage>0)
		{
			$query_and = "  AND iLanguage = '".(int)$iLanguage."' ";
		}
		
		$query="
			SELECT
				 c.szDescription,
				 d.szPageHeading,
				 c.szHeading,
				 d.szLink,
				 d.szLinkTitle,
				 d.szOpenGraphAuthor,
				 d.szOpenGraphImage,
                                 d.szLinkedPageUrl
			FROM
				".__DBC_SCHEMATA_EXPLAIN_CONTENT__." AS c
			INNER JOIN 
				".__DBC_SCHEMATA_EXPLAIN_DATA__." AS d
			ON 	
				d.id = c.idExplainData
			WHERE
			(
				c.szDescription LIKE '%".mysql_escape_custom($this->searchField)."%' 
			 OR
			 	c.szHeading LIKE '%".mysql_escape_custom($this->searchField)."%'	
			 )			
			 $query_and
			AND	
				c.iActive = '1'
			AND
				d.iActive = '1'
			GROUP BY
				c.szHeading	
			ORDER BY 
				d.iOrderBy ASC			
			";	
			//echo $query;die;
			if( ( $result = $this->exeSQL( $query ) ) )
			{
				$dataArr = array();
				while($row=$this->getAssoc($result))
				{
					$dataArr[] = $row;	
				}
				return $dataArr;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}	
	}
	function serviceBlog($id)
	{
		$this->set_id((int)$id);
		$query="
			SELECT
				id,
				szContent
			FROM 
				".__DBC_SCHEMATA_BLOG__."	
			WHERE
				id = '".(int)$this->id."'	 		
			";	
			if( ( $result = $this->exeSQL( $query ) ) )
			{
				$row=$this->getAssoc($result);
				return $row;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function serviceBlogSearch($comment,$iLanguage=false)
	{
		$this->set_szComment(sanitize_all_html_input($comment));
		if($iLanguage>0)
		{
			$query_and = "  AND iLanguage = '".(int)$iLanguage."' ";
		}
		$query="
			SELECT
				szHeading,
				szContent,
				szLink,
				iLanguage
			FROM 
				".__DBC_SCHEMATA_BLOG__."	
			WHERE
			(
				szContent LIKE '%".mysql_escape_custom($this->szComment)."%'	
			 OR
			 	szHeading LIKE '%".mysql_escape_custom($this->szComment)."%'
			 )		
			 	$query_and 		
			";	
			//echo $query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{
				$textContent = array();
				while($row=$this->getAssoc($result))
				{	
					$textContent[] = $row;
				}
				//print_R($textContent[0]['szContent']);die;
				return $textContent;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function serviceFAQSearch($comment,$iLanguage=false)
	{
		$this->set_szComment(sanitize_all_html_input($comment));
		if($iLanguage>0)
		{
			$query_and = "  AND iLanguage = '".(int)$iLanguage."' ";
		}
		
		$query="
			SELECT
				szHeading,
				szDescription,
				iLanguage				
			FROM 
				".__DBC_SCHEMATA_MANAGEMENT_FAQ_CUSTOMER__."	
			WHERE
			(
			 	szDescription LIKE '%".mysql_escape_custom($this->szComment)."%'
			 OR
			 	szHeading LIKE '%".mysql_escape_custom($this->szComment)."%'
			 )
			 $query_and	
			AND
				iActive = 1		 		
			";	
		//echo $query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{
				$textContent = array();
				while($row=$this->getAssoc($result))
				{
					$textContent[] = $row;
				}
				return $textContent;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function submitSearchFields($searchField)
	{
		$this->set_szComment(sanitize_all_html_input($searchField));
		$query="
			INSERT INTO 
				".__DBC_SCHEMATA_SEARCH_PHRASE__."	
			(
				szPhrase,
				dtSearched
			)
			value
			(
				'".mysql_escape_custom($this->szComment)."',
				NOW()	
			) 		
		";	
		if( ( $result = $this->exeSQL( $query ) ) )
		{
			return true;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function selectSearchFields()
	{
		$query="
			SELECT
				COUNT(id) as total,
				szPhrase
			FROM	
				".__DBC_SCHEMATA_SEARCH_PHRASE__."	
			WHERE
				MONTH(dtSearched) > MONTH(NOW())-6
			GROUP BY
				szPhrase
			ORDER BY
				COUNT(id) DESC		
			";	
			//echo $query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{
				$searchArr = array();
				while($row=$this->getAssoc($result))
				{
					$searchArr[] = $row;
				}
				return $searchArr;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function createExplainPage($createArr)
	{ 
            $this->validate_explain($createArr);
            if($this->error == true)
            { 
                    return false;
            } 
            if($this->checkHeadingExists(false,$createArr['iExplainPageType']))
            {
                    $this->addError( "szPageHeading" , t($this->t_base.'fields/szPageHeadingAlreadyExists') );
            }

            if($this->checkUriExists('CUSTOMER',false,$createArr['iExplainPageType']))
            {
                    $this->addError( "szLink" , t($this->t_base.'fields/szLinkAlreadyExists') );
            }

            if($this->error == true)
            {
                    return false;
            }
            $maxOrder =$this->getMaxOrderExplainData($createArr['iExplainPageType'],$this->iLanguage);

            $this->szMetaTitle = str_replace("'",'&#39;',$this->szMetaTitle);
            $this->szMetaKeyword = str_replace("'",'&#39;',$this->szMetaKeyword);
            $this->szMetaDescription = str_replace("'",'&#39;',$this->szMetaDescription);

            if(!empty($this->szUploadOGFileName) && $createArr['iOGFileUpload']==1)
            {
                    $query_insrt = " szOpenGraphImage, ";
                    $query_insrt_val = " '".mysql_escape_custom($this->szUploadOGFileName)."', ";
            }

            $query = "
                INSERT INTO 
                    ".__DBC_SCHEMATA_EXPLAIN_DATA__."
                ( 
                    `szPageHeading`,
                    `szLink`,
                    `szMetaTitle`, 
                     szLinkTitle,
                    `szMetaKeyWord`, 
                    `szMetaDescription`, 
                    $query_insrt
                    szOpenGraphAuthor,
                    `iOrderBy`,
                    `iActive`,
                    iLanguage,
                    iExplainPageType, 
                    dtCreatedOn,
                    szSubTitle,
                    szPictureTitle,
                    szPictureDescription,
                    szLinkedPageUrl
                ) 
                VALUES 
                (
                    '".mysql_escape_custom($this->szPageHeading)."', 
                    '".mysql_escape_custom($this->szLink)."', 
                    '".mysql_escape_custom($this->szMetaTitle)."', 
                    '".mysql_escape_custom($this->szLinkTitle)."',  
                    '".mysql_escape_custom($this->szMetaKeyword)."', 
                    '".mysql_escape_custom($this->szMetaDescription)."', 
                    $query_insrt_val 
                    '".mysql_escape_custom($this->szOpenGraphAuthor)."',  
                    '". (int)($maxOrder+1) ."',
                    '".(int)$this->iActive."',
                    '".(int)$this->iLanguage."',
                    '".(int)$createArr['iExplainPageType']."',		 
                    now(),
                    '".mysql_escape_custom($this->szSubTitle)."',
                    '".mysql_escape_custom($this->szPictureTitle)."',
                    '".mysql_escape_custom($this->szPictureDescription)."',
                    '".mysql_escape_custom($this->szLinkedPageUrl)."' 
                )
            ";
            //echo $query;
            //die;
            if( ( $result = $this->exeSQL($query) ) )
            {
                    /*
                     * Updating content of transportecaSitemap.xml file
                    */
                    //$kSEO = new cSEO();
                    //$kSEO->updateSitemapXmlFile();
                    $idExplainPage = $this->iLastInsertID;
                    if($this->szUploadFileName!='' && $createArr['iFileUpload']==1)
                    {			
                            $this->insertLevel2Image($this->szUploadFileName,$idExplainPage);
                    }
                    return true;
            }
            else
            {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
            }
	}
	function editExplainPage($createArr)
	{
		$this->validate_explain($createArr,'EDIT');
		if($this->error == true)
		{
			return false;
		}
		
		if($this->checkHeadingExists($this->id))
		{
			$this->addError( "szPageHeading" , t($this->t_base.'fields/szPageHeadingAlreadyExists') );
		}
		
		if($this->checkUriExists('CUSTOMER',$this->id))
		{
			$this->addError( "szLink" , t($this->t_base.'fields/szLinkAlreadyExists') );
		}
		
		if($this->error == true)
		{
			return false;
		}		
		$this->szMetaTitle = str_replace("'",'&#39;',$this->szMetaTitle);
		$this->szMetaKeyword = str_replace("'",'&#39;',$this->szMetaKeyword);
		$this->szMetaDescription = str_replace("'",'&#39;',$this->szMetaDescription);
		
		if(!empty($this->szUploadOGFileName) && $createArr['iOGFileUpload']==1)
		{
			$query_updt = " szOpenGraphImage = '".mysql_escape_custom($this->szUploadOGFileName)."',"; 
		}
		
		$query = "
			UPDATE  
				".__DBC_SCHEMATA_EXPLAIN_DATA__."
			SET	
                            `szPageHeading` = '".mysql_escape_custom($this->szPageHeading)."',
                            `szLink` = '".mysql_escape_custom($this->szLink)."',
                            `szMetaTitle` = '".mysql_escape_custom($this->szMetaTitle)."', 
                            `szMetaKeyWord` = '".mysql_escape_custom($this->szMetaKeyword)."', 
                            `szMetaDescription` = '".mysql_escape_custom($this->szMetaDescription)."',	
                             szLinkTitle	= '".mysql_escape_custom($this->szLinkTitle)."',	
                             szOpenGraphAuthor	= '".mysql_escape_custom($this->szOpenGraphAuthor)."',	
                             $query_updt		
                            `iActive` = '".(int)$this->iActive."',
                            `iLanguage` = '".(int)$this->iLanguage."',
                            dtUpdatedOn = now(),
                            szSubTitle='".mysql_escape_custom($this->szSubTitle)."',
                            szPictureTitle='".mysql_escape_custom($this->szPictureTitle)."',
                            szPictureDescription='".mysql_escape_custom($this->szPictureDescription)."',
                            szLinkedPageUrl='".mysql_escape_custom($this->szLinkedPageUrl)."' 
			WHERE
				id='".(int)$this->id."'	
			";
			//echo $query;
			//die;
			if( ( $result = $this->exeSQL( $query ) ) )
			{
				/*
				 * Updating content of transportecaSitemap.xml file
				*/
				$kSEO = new cSEO();
				$kSEO->updateSitemapXmlFile();
				
				if($this->szUploadFileName!='' && $createArr['iFileUpload']==1)
				{			
					$this->insertLevel2Image($this->szUploadFileName,$this->id);
				}
				
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	
	function validate_explain($createArr,$action=false)
	{  
		if(!empty($createArr))
		{			
			if($action=='EDIT')
			{
				$this->set_id((int)$createArr['id']);
			}
			 
			if($action=='PREVIEW' && $createArr['id']>0)
			{
				$this->set_id((int)$createArr['id']);
			}
			
			$this->set_szPageHeading(sanitize_all_html_input($createArr['szPageHeading']));
			$this->set_szLink(sanitize_all_html_input($createArr['szLink']));
                        $this->set_szLinkedPageUrl(sanitize_all_html_input($createArr['szLinkedPageUrl']));
                        
			$this->set_szLinkTitle(sanitize_all_html_input($createArr['szLinkTitle']));
			
			$this->set_szMetaTitle(sanitize_all_html_input($createArr['szMetaTitle'])); 
			$this->set_szMetaDescription(sanitize_all_html_input($createArr['szMetaDescription'])); 
			
			$this->set_iActive((int)$createArr['iActive']);
			$this->set_iLanguage((int)$createArr['iLanguage']);
			$this->set_szSubTitle(sanitize_all_html_input($createArr['szSubTitle']));
			$this->set_szUploadFileName(sanitize_all_html_input($createArr['szUploadFileName']));
			$this->set_szUploadOGFileName(sanitize_all_html_input($createArr['szUploadOGFileName'])); 
			$this->set_szOpenGraphAuthor(sanitize_all_html_input($createArr['szOpenGraphAuthor'])); 
			$this->set_szPictureTitle(sanitize_all_html_input($createArr['szPictureTitle']));	
			$this->set_szPictureDescription(sanitize_all_html_input($createArr['szPictureDescription']));	    
			
			if($this->error==true)
			{
				return false;
			}
			else
			{
				if($action=='PREVIEW')
				{
					if($this->checkHeadingExists($this->id))
					{
						$this->addError( "szPageHeading" , t($this->t_base.'fields/szPageHeadingAlreadyExists') );
					}
					
					if($this->checkUriExists('CUSTOMER',$this->id))
					{
						$this->addError( "szLink" , t($this->t_base.'fields/szLinkAlreadyExists') );
					}
				}
				return true;
			}
		}
	}
	function insertLevel2Image($szFileName,$idExplain)
	{
		$query="
				UPDATE
					".__DBC_SCHEMATA_EXPLAIN_DATA__."
				SET					
					 szHeaderImage = '".mysql_escape_custom($szFileName)."'
				WHERE
					id = '".(int)$idExplain."'					
			";
		$result = $this->exeSQL( $query );
	}
	
	function checkUriExists($mode,$id = 0,$iExplainPageType=false)
	{
		if($mode  == 'CUSTOMER' )
		{
			$addQuery = "
					WHERE
						szLink = '".mysql_escape_custom($this->szLink)."'	
					AND
						iExplainPageType = '".(int)$iExplainPageType."'
					";
		}	
		if($id>0)
		{
			$addQuery .= "
						AND
							id<> '".(int)$this->id."' ";
		}
		$query = "
				SELECT
					id		
				FROM
					".__DBC_SCHEMATA_EXPLAIN_DATA__."	
					".$addQuery."				
			";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if( $this->iNumRows > 0 )
			{
				return true;
			}
			else
			{
				return false; 
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function checkHeadingExists($id = 0,$iExplainPageType=false)
	{
		if($id>0)
		{
			$addQuery = "
						AND
							id<> '".(int)$id."' ";
		}
		$query = "
				SELECT
					id		
				FROM
					".__DBC_SCHEMATA_EXPLAIN_DATA__."	
				WHERE
					szPageHeading = '".mysql_escape_custom($this->szPageHeading)."'
				AND
					iExplainPageType = '".(int)$iExplainPageType."'
				".$addQuery."							
			";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if( $this->iNumRows > 0 )
			{
				return true;
			}
			else
			{
				return false; 
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function moveUpDownExplainPagesLinks($id,$orderBy,$iExplainPageType,$iLanguage)
	{	
		$this->set_id((int)$id);
		$this->set_iOrderBy((int)$orderBy);
		if($iLanguage>0)
		{
			$query_lang = " AND	iLanguage = '".(int)$iLanguage."' ";
		}
		
		$query="
   				SELECT
   					id,
   			 		iOrderBy
   			 	FROM
   			 		".__DBC_SCHEMATA_EXPLAIN_DATA__."
   			 	WHERE
   			 		id='".(int)$this->id."'
   			 	AND
   			 		iExplainPageType = '".(int)$iExplainPageType."'
   			 	$query_lang
   				";
			//echo $query;
   			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				if ($this->getRowCnt() > 0)
				{
					$row = $this->getAssoc( $result);
					$order = $row['iOrderBy'];
					$this->id = $row['id'];
					
					$query="
						UPDATE
							".__DBC_SCHEMATA_EXPLAIN_DATA__."
					 	SET
					 		";
					if($this->iOrderBy==__ORDER_MOVE_DOWN__)
					{
						$query.="
								iOrderBy=iOrderBy-1
						 		";
					}
					if($this->iOrderBy==__ORDER_MOVE_UP__)
					{
						$query.="
								iOrderBy=iOrderBy+1
						 		";
					}
					
					$query.="
						WHERE
					 		id='".(int)$this->id."'
					 	AND
   			 				iExplainPageType = '".(int)$iExplainPageType."'
   			 			$query_lang
						 ";
					//echo $query; 
                	if($result = $this->exeSQL($query))
					{
						$query="
							UPDATE
								".__DBC_SCHEMATA_EXPLAIN_DATA__."
							SET
							";
						if($this->iOrderBy==__ORDER_MOVE_DOWN__)
						{
							$query.="
									iOrderBy=iOrderBy+1
							 		";
						}
						if($this->iOrderBy==__ORDER_MOVE_UP__)
						{
							$query.="
									iOrderBy=iOrderBy-1
							 		";
						}
						$query.="
							WHERE
								id!='".$this->id."'	
							AND
   			 					iExplainPageType = '".(int)$iExplainPageType."'
   			 				$query_lang
							AND
								";
						if($this->iOrderBy==__ORDER_MOVE_DOWN__)
						{
							$query.="
									iOrderBy=$order-1
							 		";
						}
						if($this->iOrderBy==__ORDER_MOVE_UP__)
						{
							$query.="
									iOrderBy=$order+1
							 		";
						}
						//echo $query;
						if($result = $this->exeSQL( $query ))
						{
							//$this->updateversionTnc($option);
							return true;
						}
						else
						{
							$this->error = true;
							$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
							$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
							return false;
						}
					}
					else
					{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
		
	function getMaxOrderExplainData($iExplainPageType=1,$iLanguage=false)
	{	 
		if($iLanguage>0)
		{
			$query_lang = " AND	iLanguage = '".(int)$iLanguage."' ";
		}
		
		$query="
			SELECT 
				MAX(iOrderBy)  as orders  
			FROM 
				".__DBC_SCHEMATA_EXPLAIN_DATA__."
			WHERE
				iExplainPageType = '".(int)$iExplainPageType."'
				$query_lang
			";
		
		if($result=$this->exeSQL($query))
		{
			$row = $this->getAssoc( $result );
			if($row['orders']>0)
			{
				return $row['orders'];
			}
			else
			{
				return 0;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function deleteExplainPage($id)
	{
		$this->set_id((int)$id);
		if($this->id>0)
		{
			$query="
   				SELECT
   			 		iOrderBy,
   			 		iLanguage,
   			 		iExplainPageType
   			 	FROM
   			 		".__DBC_SCHEMATA_EXPLAIN_DATA__."
   			 	WHERE
   			 		id='".(int)$id."'
   				";
			
   			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				if ($this->getRowCnt() > 0)
				{
					$row = $this->getAssoc( $result);
					$order = $row['iOrderBy'];
					$iLanguage=$row['iLanguage'];
					$iExplainPageType=$row['iExplainPageType'];
					
					$query="
						DELETE
						FROM 
							".__DBC_SCHEMATA_EXPLAIN_DATA__."
						WHERE
							id = '".(int)$this->id."'	
						";
					//echo $query;
					if($result=$this->exeSQL($query))
					{
						$this->deleteExplainData($this->id);
						
						$query="
							UPDATE
								".__DBC_SCHEMATA_EXPLAIN_DATA__."
						 	SET
								iOrderBy=iOrderBy-1
							 WHERE
						 		iOrderBy > '".(int)$order."'
						 	AND
						 		iExplainPageType='".(int)$iExplainPageType."'
						 	AND
						 		iLanguage='".(int)$iLanguage."'	
					 ";
	                		//echo $query;DIE;
						 if( $result = $this->exeSQL($query))
						 {
						 	return true;
						 }
					}
					else
					{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					}
				}
			}
		}
	}
	function selectBlogDetails($idBlog = 0,$iLanguage=false)
	{
		$this->set_id((int)$idBlog);
		$add_query = '';
		
		if( $this->id > 0 )
		{
			$add_query = "
					WHERE
						id = '".(int)$this->id."'
					";
		}
		
		if($iLanguage>0)
		{
			if(empty($add_query))
			{
				$add_query = " WHERE iLanguage = '".(int)$iLanguage."' ";
			}
			else
			{
				$add_query .= " AND iLanguage = '".(int)$iLanguage."' ";
			}
		}
		$query_order_by = "
				ORDER BY 
					iOrderBy ASC
			"; 
		$query = "
				SELECT
					`id`,
					`szHeading`,
					`szContent`,
					`szLink`, 
					`szMetaKeyWord`, 
					`szMetaDescription`, 
					`szMetaTitle`,
					`iActive`,
					`iOrderBy`,
					iLanguage				
				FROM
					".__DBC_SCHEMATA_BLOG__."	
				".$add_query."
				$query_order_by
			";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			$links = array();
			while($row=$this->getAssoc($result))
			{
				$links[] = $row;
			}
			return $links;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	function selectBlogDetailsActive($iLanguage=false)
	{
		if($iLanguage>0)
		{
			$query_and =" AND iLanguage = '".(int)$iLanguage."' ";
		}
		
		$query = "
				SELECT
					`id`,
					`szHeading`,
					`szLink`,
					szMetaTitle,
					szMetaKeyWord,
					szMetaDescription,
					dtCreatedOn,
					dtUpdatedOn,
					iLanguage
				FROM
					".__DBC_SCHEMATA_BLOG__."	
				WHERE
					iActive = 1
					$query_and
				ORDER BY
					iOrderBy ASC	
			";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			$links = array();
			while($row=$this->getAssoc($result))
			{
				$links[] = $row;
			}
			return $links;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function getMaxOrderBlogData()
	{	
		$query="
			SELECT 
				MAX(iOrderBy)  as orders  
			FROM 
				".__DBC_SCHEMATA_BLOG__."
			";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			$row = $this->getAssoc( $result );
			if($row['orders']>0)
			{
				return $row['orders'];
			}
			else
			{
				return 0;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function deleteBlogPage($id)
	{
		$this->set_id((int)$id);
		if($this->id>0)
		{
			$query="
   				SELECT
   			 		iOrderBy
   			 	FROM
   			 		".__DBC_SCHEMATA_BLOG__."
   			 	WHERE
   			 		id='".(int)$id."'
   				";
			//echo $query;
   			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				if ($this->getRowCnt() > 0)
				{
					$row = $this->getAssoc( $result);
					$order = $row['iOrderBy'];
				
					$query="
						DELETE
						FROM 
							".__DBC_SCHEMATA_BLOG__."
						WHERE
							id = '".(int)$this->id."'	
						";
					//echo $query;
					if($result=$this->exeSQL($query))
					{
						$query="
							UPDATE
								".__DBC_SCHEMATA_BLOG__."
						 	SET
								iOrderBy=iOrderBy-1
							 WHERE
						 		iOrderBy > '".(int)$order."'
					 	";
	                		//echo $query;DIE;
						 if( $result = $this->exeSQL($query))
						 {
						 	/*
						 	 * Updating content of transportecaSitemap.xml file
						 	*/
						 	$kSEO = new cSEO();
						 	$kSEO->updateSitemapXmlFile();
						 	
						 	return true;
						 }
					}
				}	
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function moveUpDownBlogPagesLinks($id,$orderBy)
	{	
		$this->set_id((int)$id);
		$this->set_iOrderBy((int)$orderBy);
		$query="
   				SELECT
   					id,
   			 		iOrderBy
   			 	FROM
   			 		".__DBC_SCHEMATA_BLOG__."
   			 	WHERE
   			 		id='".(int)$this->id."'
   				";
			//echo $query;
   			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				if ($this->getRowCnt() > 0)
				{
					$row = $this->getAssoc( $result);
					$order = $row['iOrderBy'];
					$this->id = $row['id'];
					
					$query="
						UPDATE
							".__DBC_SCHEMATA_BLOG__."
					 	SET
					 		";
					if($this->iOrderBy==__ORDER_MOVE_DOWN__)
					{
						$query.="
								iOrderBy=iOrderBy-1
						 		";
					}
					if($this->iOrderBy==__ORDER_MOVE_UP__)
					{
						$query.="
								iOrderBy=iOrderBy+1
						 		";
					}
					$query.="
						WHERE
					 		id='".(int)$this->id."'
						 ";
					//echo $query;
					
                	if($result = $this->exeSQL( $query ))
					{
						$query="
							UPDATE
								".__DBC_SCHEMATA_BLOG__."
							SET
							";
						if($this->iOrderBy==__ORDER_MOVE_DOWN__)
						{
							$query.="
									iOrderBy=iOrderBy+1
							 		";
						}
						if($this->iOrderBy==__ORDER_MOVE_UP__)
						{
							$query.="
									iOrderBy=iOrderBy-1
							 		";
						}
						$query.="
							WHERE
								id!='".(int)$this->id."'	
							AND
								";
						if($this->iOrderBy==__ORDER_MOVE_DOWN__)
						{
							$query.="
									iOrderBy=$order-1
							 		";
						}
						if($this->iOrderBy==__ORDER_MOVE_UP__)
						{
							$query.="
									iOrderBy=$order+1
							 		";
						}
						//echo $query;
						if($result = $this->exeSQL( $query ))
						{
							//$this->updateversionTnc($option);
							return true;
						}
						else
						{
							$this->error = true;
							$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
							$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
							return false;
						}
					}
					else
					{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function editBlogPage($createArr)
	{	
		$this->set_id(sanitize_all_html_input($createArr['id']));
		$this->set_szHeading(sanitize_all_html_input($createArr['szHeading']));
		//$data=sanitize_specific_tinymce_html_input($createArr['szContent']);
		//$data=str_replace("'",'&#39;',$createArr['szContent']);
		$this->set_szContent(trim($createArr['szContent']));
		$this->szContent=htmlspecialchars($this->szContent, ENT_QUOTES);
		//$this->set_szContent(sanitize_specific_tinymce_html_input($createArr['szContent']));
		$this->set_szLink(sanitize_all_html_input($createArr['szLink']));
		$this->set_szMetaTitle(sanitize_all_html_input($createArr['szMetaTitle']));
		$this->set_szMetaKeyword(sanitize_all_html_input($createArr['szMetaKeyword']));	
		$this->set_szMetaDescription(sanitize_all_html_input($createArr['szMetaDescription']));	
		$this->set_iActive((int)$createArr['iActive']);
		$this->set_iLanguage((int)$createArr['iLanguage']);
		
		if($this->error == true)
		{
			return false;
		}
		
		if($this->checkBlogHeadingExists($this->id))
		{
			$this->addError( "szHeading" , t($this->t_base.'fields/szPageHeadingAlreadyExists') );
		}
		
		if($this->checkBlogUriExists($this->id))
		{
			$this->addError( "szLink" , t($this->t_base.'fields/szLinkAlreadyExists') );
		}
		$this->szMetaTitle = str_replace("'",'&#39;',$this->szMetaTitle);
		$this->szMetaKeyword = str_replace("'",'&#39;',$this->szMetaKeyword);
		$this->szMetaDescription = str_replace("'",'&#39;',$this->szMetaDescription);
		$this->szContent = str_replace("'",'&#39;',$this->szContent);
		if($this->error == true)
		{
			return false;
		}
				
		$query = "UPDATE  
						".__DBC_SCHEMATA_BLOG__."
					SET	
						`szHeading` = '".mysql_escape_custom($this->szHeading)."',
						`szLink` = '".mysql_escape_custom($this->szLink)."',
						`szContent` = '".mysql_escape_custom($this->szContent)."',
						`szMetaTitle` = '".mysql_escape_custom($this->szMetaTitle)."', 
						`szMetaKeyWord` = '".mysql_escape_custom($this->szMetaKeyword)."', 
						`szMetaDescription` = '".mysql_escape_custom($this->szMetaDescription)."',					
						`iActive` = '".(int)$this->iActive."',
						`iLanguage` = '".(int)$this->iLanguage."',						
						dtUpdatedOn = now()
					WHERE
						id='".(int)$this->id."'	
					";
			//DIE;
			//echo $query;die;
			if( ( $result = $this->exeSQL( $query ) ) )
			{
				/*
				 * Updating content of transportecaSitemap.xml file
				*/
				$kSEO = new cSEO();
				$kSEO->updateSitemapXmlFile();
				
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function createBlogPage($createArr)
	{	
		$this->set_szHeading(sanitize_all_html_input($createArr['szHeading']));
		$this->set_szContent(trim($createArr['szContent']));
		$this->szContent=htmlspecialchars($this->szContent, ENT_QUOTES);
		$this->set_szLink(sanitize_all_html_input($createArr['szLink']));
		$this->set_szMetaTitle(sanitize_all_html_input($createArr['szMetaTitle']));
		$this->set_szMetaKeyword(sanitize_all_html_input($createArr['szMetaKeyword']));	
		$this->set_szMetaDescription(sanitize_all_html_input($createArr['szMetaDescription']));	
		$this->set_iActive((int)$createArr['iActive']);
		$this->set_iLanguage((int)$createArr['iLanguage']);
		
		if($this->error == true)
		{
			return false;
		}
		
		if($this->checkBlogHeadingExists())
		{
			$this->addError( "szPageHeading" , t($this->t_base.'fields/szPageHeadingAlreadyExists') );
		}
		
		if($this->checkBlogUriExists())
		{
			$this->addError( "szLink" , t($this->t_base.'fields/szLinkAlreadyExists') );
		}
		$maxOrder =$this->getMaxOrderBlogData();
		
		$this->szMetaTitle = str_replace("'",'&#39;',$this->szMetaTitle);
		$this->szMetaKeyword = str_replace("'",'&#39;',$this->szMetaKeyword);
		$this->szMetaDescription = str_replace("'",'&#39;',$this->szMetaDescription);
		$this->szContent = str_replace("'",'&#39;',$this->szContent);
		if($this->error == true)
		{
			return false;
		}
		$query = "INSERT INTO 
						".__DBC_SCHEMATA_BLOG__."
					(
						`id`,
						`szHeading`,
						`szLink`,
						`szContent`,
						`szMetaTitle`, 
						`szMetaKeyWord`, 
						`szMetaDescription`, 
						`iOrderBy`,
						`iActive`,
						iLanguage,
						dtCreatedOn
					) 
					VALUES 
					(
						NULL, 
						'".mysql_escape_custom($this->szHeading)."', 
						'".mysql_escape_custom($this->szLink)."', 
						'".mysql_escape_custom($this->szContent)."',
						'".mysql_escape_custom($this->szMetaTitle)."', 
						'".mysql_escape_custom($this->szMetaKeyword)."', 
						'".mysql_escape_custom($this->szMetaDescription)."', 
						'". (int)($maxOrder+1) ."',
						'".(int)$this->iActive."',
						'".(int)$this->iLanguage."',
						now()
					)
				";
			//echo $query;die;
			if( ( $result = $this->exeSQL( $query ) ) )
			{
				/*
				 * Updating content of transportecaSitemap.xml file
				*/
				$kSEO = new cSEO();
				$kSEO->updateSitemapXmlFile();
				
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function checkBlogHeadingExists($id = 0)
	{
		if($id>0)
		{
			$addQuery = "
						AND
							id<> '".(int)$this->id."' ";
		}
		$query = "
				SELECT
					id		
				FROM
					".__DBC_SCHEMATA_BLOG__."	
				WHERE
					szHeading = '".mysql_escape_custom($this->szHeading)."'
				".$addQuery."							
			";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if( $this->iNumRows > 0 )
			{
				return true;
			}
			else
			{
				return false; 
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function checkBlogUriExists($id = 0)
	{
		
		$addQuery = '';
		if($id>0)
		{
			$addQuery .= "
						AND
							id<> '".(int)$this->id."' ";
		}
		$query = "
				SELECT
					id		
				FROM
					".__DBC_SCHEMATA_BLOG__."	
					WHERE
						szLink = '".mysql_escape_custom($this->szLink)."'
						".$addQuery."				
			";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if( $this->iNumRows > 0 )
			{
				return true;
			}
			else
			{
				return false; 
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	function selectBlogLink($link = '',$iLanguage=false)
	{
		$this->set_szLink(sanitize_all_html_input($link));
		
		if($iLanguage>0)
		{
			$add_query = " AND iLanguage = '".(int)mysql_escape_custom($iLanguage)."' ";
		}
		
		if( $this->szLink != '' )
		{
			$add_query .= "
					AND
						szLink = '".mysql_escape_custom($this->szLink)."'
					";
		}
		else
		{
			$add_query .= "
					
					ORDER BY	
						iOrderBy
				";
		}		
		$query = "
				SELECT
					`id`,
					`szHeading`,
					`szLink`,
					`szContent`,
					`szMetaTitle`, 
					`szMetaKeyWord`, 
					`szMetaDescription`, 
					`iOrderBy`,
					`iActive`,
					iLanguage								
				FROM
					".__DBC_SCHEMATA_BLOG__."	
				WHERE
					iActive = '1'
				".$add_query."	
			";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			$row=$this->getAssoc($result);
			
			return $row;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	function addLandingPage($landingContent)
	{
		if(!empty($landingContent))
		{
			$this->validateLandingPageData($landingContent);
			
			if($this->error == true)
			{
				return false;
			}
			
			if($this->isUrlExists($this->szUrl))
			{
				$this->addError("szUrl", t($this->t_base.'fields/url_already_exists'));
				return false;
			}
			
			$iMaxOrder = $this->getMaxOrderLandingPageData();
			
			$query = "
				INSERT INTO 
					".__DBC_SCHEMATA_LANDING_PAGE_DATA__."
				(
					szSeoKeywords,
					szMetaTitle,
					szMetaKeywords,
					szMetaDescription,
					szUrl,
					iPublished,
					iLanguage,
					szTextA,
					szTextB,
					szTextC,
					szTextD,
					szTextE,
					szTextF,
					szTextG,
					szTextH,
					szTextI,
					szTextJ,
					szTextK,
					szTextL,
					szTextM,
					dtCreatedOn,
					iActive,
					iOrder
				) 
				VALUES 
				(
					'".mysql_escape_custom($this->szPageHeading)."', 
					'".mysql_escape_custom($this->szMetaTitle)."', 
					'".mysql_escape_custom($this->szMetaKeywords)."', 
					'".mysql_escape_custom($this->szMetaDescription)."',
					'".mysql_escape_custom($this->szUrl)."', 
					'".(int)mysql_escape_custom($this->iPublished)."', 
					'".(int)mysql_escape_custom($this->iLanguage)."', 
					'".mysql_escape_custom($this->szTextA)."', 
					'".mysql_escape_custom($this->szTextB)."',
				    '".mysql_escape_custom($this->szTextC)."',
				    '".mysql_escape_custom(trim($this->szTextD))."',
				    '".mysql_escape_custom($this->szTextE)."',
				    '".mysql_escape_custom($this->szTextF)."',
				    '".mysql_escape_custom($this->szTextG)."',
				    '".mysql_escape_custom($this->szTextH)."',
				    '".mysql_escape_custom($this->szTextI)."',
				    '".mysql_escape_custom($this->szTextJ)."',
				    '".mysql_escape_custom($this->szTextK)."',
				    '".mysql_escape_custom($this->szTextL)."',
				    '".mysql_escape_custom($this->szTextM)."',
				    now(),
				    '1',
				    '". (int)($iMaxOrder+1) ."'
				)
			";
			//echo $query;
			//die;
			if( ( $result = $this->exeSQL( $query ) ) )
			{
				/*
				 * Updating content of transportecaSitemap.xml file
				*/
				$kSEO = new cSEO();
				$kSEO->updateSitemapXmlFile();
				
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	} 
	
	function addNewLandingPage($landingContent)
	{
		if(!empty($landingContent))
		{
			$this->validateNewLandingPageData($landingContent);
			
			if($this->error == true)
			{
				return false;
			}
			
			if($this->isUrlExists($this->szUrl))
			{
				$this->addError("szUrl", t($this->t_base.'fields/url_already_exists'));
				return false;
			}
			$iMaxOrder = $this->getMaxOrderNewLandingPageData();
			
			$query = "
				INSERT INTO 
					".__DBC_SCHEMATA_CUSTOM_LANDING_PAGE__."
				(
					szSeoKeywords,
					szMetaTitle,
					szMetaKeywords,
					szMetaDescription,
					szUrl,
					iPublished,
					iLanguage,
					szTextA1,
					szTextA2,
					szTextA3,
					szTextB,
					szTextB2,
					szTextB3,
					szTextC,
					szTextD1,
					szTextD2,
					szTextD3,
					szTextD4,
					szTextD5,
					szTextD6,
					szTextD7,
					szTextD8,
					szTextD9,
					szTextE1,
					szTextE2,
					szTextE3,
					szTextE4,
					szTextE5,
					szTextE6,					
					szTextF1,
					szTextG1,
					szTextG2,
					szTextG3,
					szTextG4,				
					szTextG5,
					szTextG6,
					szTextG7,
					szTextG8,
					szTextG9,
					szTextH1,
					szTextH2,
					szTextH3,
					szTextI1,
					szTextI2,
					szTextI3,
					szTextI4,
					szTextI5,
					szTextI6,
					szTextI7,
					szTextJ1,
					szTextL1,
					szTextL2,
					szTextL3,
					szTextL4,
                                        iDisplaySeoText,
                                        szBackgroundColor,
                                        szHeading,
                                        szSeoDescription,
                                        iPageSearchType,
                                        fDefaultWeight,
					dtCreatedOn,
					iActive,
					iOrder,
                                        idSearchMini, 
                                        szMiddleText, 
                                        szContactMe,
                                        iHoldingPage,
                                        szHoldingHeading1,
                                        szHoldingHeading2,
                                        szHoldingTextBold1,
                                        szHoldingTextBold2,
                                        szHoldingTextBold3,
                                        szHoldingTextPlain1,
                                        szHoldingTextPlain2,
                                        szHoldingTextPlain3, 
                                        szHoldingEmailHeading,
                                        szHoldingFromHeading,
                                        szHoldingToHeading,
                                        szHoldingSubmitButtonText,
                                        szHoldingThanksButtonText,
                                        szYoutubeUrl,
                                        szFromInstruction,
                                        szToInstruction,
                                        szCargoType,
                                        iDefaultInsurance,
                                        idInsuranceCurrency
				) 
				VALUES 
				(
                                    '".mysql_escape_custom($this->szPageHeading)."', 
                                    '".mysql_escape_custom($this->szMetaTitle)."', 
                                    '".mysql_escape_custom($this->szMetaKeywords)."', 
                                    '".mysql_escape_custom($this->szMetaDescription)."',
                                    '".mysql_escape_custom($this->szUrl)."', 
                                    '".(int)mysql_escape_custom($this->iPublished)."', 
                                    '".(int)mysql_escape_custom($this->iLanguage)."', 
                                    '".mysql_escape_custom($this->szTextA[1])."', 
                                    '".mysql_escape_custom($this->szTextA[2])."', 
                                    '".mysql_escape_custom($this->szTextA[3])."', 
                                    '".mysql_escape_custom($this->szTextB[1])."',
                                    '".mysql_escape_custom($this->szTextB[2])."',
                                    '".mysql_escape_custom($this->szTextB[3])."', 
				    '".mysql_escape_custom($this->szTextC)."',
				    '".mysql_escape_custom(trim($this->szTextD[1]))."',
				    '".mysql_escape_custom(trim($this->szTextD[2]))."',
				    '".mysql_escape_custom(trim($this->szTextD[3]))."',
				    '".mysql_escape_custom(trim($this->szTextD[4]))."',
				    '".mysql_escape_custom(trim($this->szTextD[5]))."',
				    '".mysql_escape_custom(trim($this->szTextD[6]))."',
				    '".mysql_escape_custom(trim($this->szTextD[7]))."',
				    '".mysql_escape_custom(trim($this->szTextD[8]))."',
				    '".mysql_escape_custom(trim($this->szTextD[9]))."',
				    '".mysql_escape_custom($this->szTextE[1])."',
				    '".mysql_escape_custom($this->szTextE[2])."',
				    '".mysql_escape_custom($this->szTextE[3])."',
				    '".mysql_escape_custom($this->szTextE[4])."',
				    '".mysql_escape_custom($this->szTextE[5])."',
				    '".mysql_escape_custom($this->szTextE[6])."',
				    '".mysql_escape_custom($this->szTextF[1])."',
				    '".mysql_escape_custom($this->szTextG[1])."',
				    '".mysql_escape_custom($this->szTextG[2])."',
				    '".mysql_escape_custom($this->szTextG[3])."',
				    '".mysql_escape_custom($this->szTextG[4])."',
				    '".mysql_escape_custom($this->szTextG[5])."',
				    '".mysql_escape_custom($this->szTextG[6])."',
				    '".mysql_escape_custom($this->szTextG[7])."',
				    '".mysql_escape_custom($this->szTextG[8])."',
				    '".mysql_escape_custom($this->szTextG[9])."',
				    '".mysql_escape_custom($this->szTextH[1])."',
				    '".mysql_escape_custom($this->szTextH[2])."',
				    '".mysql_escape_custom($this->szTextH[3])."',
				    '".mysql_escape_custom($this->szTextI[1])."',
				    '".mysql_escape_custom($this->szTextI[2])."',
				    '".mysql_escape_custom($this->szTextI[3])."',
				    '".mysql_escape_custom($this->szTextI[4])."',
				    '".mysql_escape_custom($this->szTextI[5])."',
				    '".mysql_escape_custom($this->szTextI[6])."',
				    '".mysql_escape_custom($this->szTextI[7])."',
				    '".mysql_escape_custom($this->szTextJ[1])."', 
				    '".mysql_escape_custom($this->szTextL[1])."',
				    '".mysql_escape_custom($this->szTextL[2])."',
				    '".mysql_escape_custom($this->szTextL[3])."',
				    '".mysql_escape_custom($this->szTextL[4])."', 
                                    '".mysql_escape_custom($this->iDisplaySeoText)."',
				    '".mysql_escape_custom($this->szBackgroundColor)."',
				    '".mysql_escape_custom($this->szHeading)."',
				    '".mysql_escape_custom($this->szSeoDescription)."',
                                    '".mysql_escape_custom($this->iPageSearchType)."', 
                                    '".mysql_escape_custom($this->fDefaultWeight)."',  
				    now(),
				    '1',
				    '". (int)($iMaxOrder+1)."',
                                    '".mysql_escape_custom(trim($this->idSearchMini))."',
                                    '".mysql_escape_custom(trim($this->szMiddleText))."',
                                    '".mysql_escape_custom(trim($this->szContactMe))."',
                                    '".mysql_escape_custom(trim($this->iHoldingPage))."', 
                                    '".mysql_escape_custom($this->szHoldingHeading[1])."',
                                    '".mysql_escape_custom($this->szHoldingHeading[2])."',
                                    '".mysql_escape_custom($this->szHoldingTextBold[1])."',
                                    '".mysql_escape_custom($this->szHoldingTextBold[2])."',
                                    '".mysql_escape_custom($this->szHoldingTextBold[3])."',
                                    '".mysql_escape_custom($this->szHoldingTextPlain[1])."',
                                    '".mysql_escape_custom($this->szHoldingTextPlain[2])."',
                                    '".mysql_escape_custom($this->szHoldingTextPlain[3])."',
                                    '".mysql_escape_custom(trim($this->szHoldingEmailHeading))."',
                                    '".mysql_escape_custom(trim($this->szHoldingFromHeading))."',
                                    '".mysql_escape_custom(trim($this->szHoldingToHeading))."',
                                    '".mysql_escape_custom(trim($this->szHoldingSubmitButtonText))."',
                                    '".mysql_escape_custom(trim($this->szHoldingThanksButtonText))."',
                                    '".mysql_escape_custom(trim($this->szYoutubeUrl))."',
                                    '".mysql_escape_custom(trim($this->szFromInstruction))."',
                                    '".mysql_escape_custom(trim($this->szToInstruction))."',
                                    '".mysql_escape_custom(trim($this->szCargoType))."',
                                    '".mysql_escape_custom(trim($this->iDefaultInsurance))."',
                                    '".mysql_escape_custom(trim($this->idInsuranceCurrency))."'    
				)
			"; 
//			echo "Query: <br>".$query;
//                        die;
			if( ( $result = $this->exeSQL( $query ) ) )
			{
				$idLandingPage = $this->iLastInsertID;
				$this->addCustomerTestimonial($idLandingPage);
				$this->addPartnerLogo($idLandingPage); 
                                //if($this->iPageSearchType==__SEARCH_TYPE_VOGUE__)
                                    $this->addStandardShipperDetails($idLandingPage); 
				/*
				 * Updating content of transportecaSitemap.xml file
				*/
				$kSEO = new cSEO();
				$kSEO->updateSitemapXmlFile();
				
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function isUrlExists($szUrl,$id=false)
	{
		if(!empty($szUrl))
		{
			
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_LANDING_PAGE_DATA__."
				WHERE
					szUrl = '".mysql_escape_custom(trim($szUrl))."'
				AND
					iActive='1'
				AND
					iDefault = '0'
			";
			
			if($id>0)
			{
				$query.="AND
						id<>'".(int)$id."'
					";
			}
			
			//echo $query;
			if($result = $this->exeSQL( $query ) )
			{
				if($this->getRowCnt() > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function getAllLandingPageData($idLandingPage=false,$iLanguage=false,$flag=0)
	{
		if($idLandingPage>0)
		{
			$query_and= " WHERE
					id='".(int)$idLandingPage."'
				";
		}
		else
		{
			$query_and= " WHERE iActive = '1' ";
		}
		
		if($iLanguage>0)
		{
			$query_and .= " AND lpd.iLanguage='".(int)$iLanguage."' ";
		}
		
		$query="
			SELECT
				lpd.id,
				lpd.szSeoKeywords,
				lpd.szMetaTitle,
				lpd.szMetaKeywords,
				lpd.szMetaDescription,
				lpd.szUrl,
				lpd.iPublished,
				lpd.szTextA,
				lpd.szTextB,
				lpd.szTextC,
				lpd.szTextD,
				lpd.szTextE,
				lpd.szTextF,
				lpd.szTextG,
				lpd.szTextH,
				lpd.szTextI,
				lpd.szTextJ,
				lpd.szTextK,
				lpd.szTextL,
				lpd.szTextM,
				lpd.dtCreatedOn,
				lpd.iActive,
				lpd.iOrder,
				lpd.iLanguage,
				lpd.iDefault,
				(SELECT count(cb.id) FROM ".__DBC_SCHEMATA_COMPARE_BOOKING__." cb WHERE cb.idLandingPage = lpd.id AND ) iNumButtonClicked
			FROM
				".__DBC_SCHEMATA_LANDING_PAGE_DATA__." lpd
			$query_and
			ORDER BY
				iOrder ASC
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			$ret_ary = array();
			$ctr=0;
			while($row=$this->getAssoc($result))
			{
				$ret_ary[$ctr] = $row;
				$ctr++;
			}
			return $ret_ary;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function getLandingPageDataByUrl($szURL=false,$iDefaultPage=false,$iReturn_all_published_record=false,$iLanguage=false)
	{
		if(!empty($szURL))
		{
			$query_where = " WHERE
					szUrl = '".mysql_escape_custom(trim($szURL))."'
				AND
					iActive='1'
				AND
				   iPublished = '1'
				";
		}
		else if($iDefaultPage)
		{
			$query_where = " WHERE
					iDefault = '1'
				AND
					iActive='1' 
			";
		}
		else if($iReturn_all_published_record)
		{
			$query_where = " WHERE
					iDefault = '0'
				AND
					iActive = '1' 
				AND
					iPublished = '1'
				";
		}
		else
		{
			return false;
		}
		if($iLanguage>0)
		{
			$query_where .= " AND iLanguage = '".(int)$iLanguage."' ";
		}
		
		$query="
			SELECT
				id,
				szSeoKeywords,
				szMetaTitle,
				szMetaKeywords,
				szMetaDescription,
				szUrl,
				iPublished,
				szTextA,
				szTextB,
				szTextC,
				szTextD,
				szTextE,
				szTextF,
				szTextG,
				szTextH,
				szTextI,
				szTextJ,
				szTextK,
				szTextL,
				szTextM,
				dtCreatedOn,
				iActive,
				iOrder,
				dtUpdatedOn,
				iLanguage
			FROM
				".__DBC_SCHEMATA_LANDING_PAGE_DATA__."
			$query_where
			ORDER BY
				iOrder ASC
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			$ret_ary = array();
			if($iReturn_all_published_record)
			{
				$ctr=0;
				while($row=$this->getAssoc($result))
				{
					$ret_ary[$ctr] = $row;
					$ctr++;
				}
			}
			else
			{
				$row=$this->getAssoc($result);
				$ret_ary = $row;
			}
			return $ret_ary;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function validateLandingPageData($landingContent)
	{
		if(!empty($landingContent))
		{
			if($landingContent['iActive']==1)
			{
				$require_feild_flag = true;
			}
			else
			{
				$require_feild_flag = false;
			}
			
			$this->set_szPageHeading(sanitize_all_html_input($landingContent['szPageHeading']));
			$this->set_szMetaTitle(sanitize_all_html_input($landingContent['szMetaTitle']),$require_feild_flag);
			$this->set_szMetaKeywords(sanitize_all_html_input($landingContent['szMetaKeyword']),$require_feild_flag);
			$this->set_szMetaDescription(sanitize_all_html_input($landingContent['szMetaDescription']),$require_feild_flag);
			$this->set_szUrl(sanitize_all_html_input($landingContent['szLink']),$require_feild_flag);		
			$this->set_szTextA(sanitize_specific_tinymce_html_input($landingContent['szTextA']),$require_feild_flag);	
			$this->set_szTextB(sanitize_specific_tinymce_html_input($landingContent['szTextB']),$require_feild_flag);	
			$this->set_szTextC(sanitize_specific_tinymce_html_input($landingContent['szTextC']),$require_feild_flag);	
			$this->set_szTextD($landingContent['szTextD'],$require_feild_flag);	
			$this->set_szTextE(sanitize_specific_tinymce_html_input($landingContent['szTextE']),$require_feild_flag);	
			$this->set_szTextF(sanitize_specific_tinymce_html_input($landingContent['szTextF']),$require_feild_flag);	
			$this->set_szTextG(sanitize_specific_tinymce_html_input($landingContent['szTextG']),$require_feild_flag);	
			$this->set_szTextH(sanitize_specific_tinymce_html_input($landingContent['szTextH']),$require_feild_flag);	
			$this->set_szTextI(sanitize_specific_tinymce_html_input($landingContent['szTextI']),$require_feild_flag);	
			$this->set_szTextJ(sanitize_specific_tinymce_html_input($landingContent['szTextJ']),$require_feild_flag);
			$this->set_szTextK(sanitize_specific_tinymce_html_input($landingContent['szTextK']),$require_feild_flag);
			$this->set_szTextL(sanitize_specific_tinymce_html_input($landingContent['szTextL']),$require_feild_flag);
			$this->set_szTextM(sanitize_specific_tinymce_html_input($landingContent['szTextM']),$require_feild_flag);
			$this->set_iPublished((int)$landingContent['iActive']);
			$this->set_iLanguage((int)$landingContent['iLanguage']);
			
			if($this->error == true)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			return false;
		}
	}
	

	function validateNewLandingPageData($landingContent)
	{
		if(!empty($landingContent))
		{
                    if($landingContent['iActive']==1)
                    {
                        $require_feild_flag = true;
                    }
                    else
                    {
                        $require_feild_flag = false;
                    }

                    $this->set_szPageHeading(sanitize_all_html_input($landingContent['szPageHeading']));
                    $this->set_szMetaTitle(sanitize_all_html_input($landingContent['szMetaTitle']),$require_feild_flag);
                    $this->set_szMetaKeywords(sanitize_all_html_input($landingContent['szMetaKeyword']),$require_feild_flag);
                    $this->set_szMetaDescription(sanitize_all_html_input($landingContent['szMetaDescription']),$require_feild_flag);
                    $this->set_szUrl(sanitize_all_html_input($landingContent['szLink']),$require_feild_flag);
                    
                    /*
                    * Validating Holding page contents
                    */
                    $this->iHoldingPage = $landingContent['iHoldingPage'];
                    $bHoldingPageRequired = false;
                    $bNonHoldingPageVariableRequired=false;
                    if($landingContent['iActive']==1)
                    {
                        $bNonHoldingPageVariableRequired = true;
                    }
                    if($this->iHoldingPage==1 && $require_feild_flag)
                    {
                        $bHoldingPageRequired = true;
                        $bNonHoldingPageVariableRequired=false;
                    } 
                    
                    $this->set_szHoldingHeading(sanitize_all_html_input($landingContent['szHoldingHeading1']),1,$bHoldingPageRequired);
                    $this->set_szHoldingHeading(sanitize_all_html_input($landingContent['szHoldingHeading2']),2,$bHoldingPageRequired); 
                    
                    $this->set_szHoldingTextBold(sanitize_all_html_input($landingContent['szHoldingTextBold1']),1,$bHoldingPageRequired);
                    $this->set_szHoldingTextBold(sanitize_all_html_input($landingContent['szHoldingTextBold2']),2,$bHoldingPageRequired);
                    $this->set_szHoldingTextBold(sanitize_all_html_input($landingContent['szHoldingTextBold3']),3,$bHoldingPageRequired);
                    
                    $this->set_szHoldingTextPlain(sanitize_all_html_input($landingContent['szHoldingTextPlain1']),1,$bHoldingPageRequired);
                    $this->set_szHoldingTextPlain(sanitize_all_html_input($landingContent['szHoldingTextPlain2']),2,$bHoldingPageRequired);
                    $this->set_szHoldingTextPlain(sanitize_all_html_input($landingContent['szHoldingTextPlain3']),3,$bHoldingPageRequired);
                    
                    $this->set_szHoldingEmailHeading(sanitize_all_html_input($landingContent['szHoldingEmailHeading']),$bHoldingPageRequired);
                    $this->set_szHoldingFromHeading(sanitize_all_html_input($landingContent['szHoldingFromHeading']),$bHoldingPageRequired);
                    $this->set_szHoldingToHeading(sanitize_all_html_input($landingContent['szHoldingToHeading']),$bHoldingPageRequired);
                    $this->set_szHoldingSubmitButtonText(sanitize_all_html_input($landingContent['szHoldingSubmitButtonText']),$bHoldingPageRequired);
                    $this->set_szHoldingThanksButtonText(sanitize_all_html_input($landingContent['szHoldingThanksButtonText']),$bHoldingPageRequired);
                    $this->set_szYoutubeUrl(trim($landingContent['szYoutubeUrl']),$bHoldingPageRequired);
                     
                    $this->idSearchMini = $landingContent['idSearchMini'];
                    $require_flag=false;
                    $voga_automatic_require_flag = false;
                    $common_voga_required_flag = false;
                    $require_flag_insurance_vogue_page = false;
                    if((int)$landingContent['iPageSearchType']==__SEARCH_TYPE_VOGUE__ && $landingContent['iActive']==1)
                    { 
                        $require_flag = true;
                        $require_flag_insurance_vogue_page=true;
                    }
                    else if(((int)$landingContent['iPageSearchType']==__SEARCH_TYPE_VOGUE_AUTOMATIC_ || (int)$landingContent['iPageSearchType']==__SEARCH_TYPE_VOGUE_AUTOMATIC_SIMPLE_) && $landingContent['iActive']==1)
                    { 
                        $voga_automatic_require_flag = true;
                        $require_flag_insurance_vogue_page=true;
                    }
                    if($voga_automatic_require_flag && $require_flag && $require_flag_insurance_vogue_page)
                    {
                        $common_voga_required_flag = true;
                    } 
                    
                    /*
                    * Voga: Standard Cargo - Delivery Address
                    */
                    $this->set_szShipperCompanyName(sanitize_all_html_input($landingContent['szShipperCompanyName']),$require_flag);
                    $this->set_szShipperFirstName(sanitize_all_html_input($landingContent['szShipperFirstName']),$require_flag);
                    $this->set_szShipperLastName(sanitize_all_html_input($landingContent['szShipperLastName']),$require_flag);
                    $this->set_szShipperPhone(sanitize_all_html_input($landingContent['szShipperPhone']),$require_flag);
                    $this->set_szShipperAddress(sanitize_all_html_input($landingContent['szShipperAddress']),$require_flag);
                    $this->set_szShipperCity(sanitize_all_html_input($landingContent['szShipperCity']),$require_flag);
                    $this->set_idShipperCountry(sanitize_all_html_input($landingContent['idShipperCountry']),$require_flag);
                    $this->set_idShipperDialCode(sanitize_all_html_input($landingContent['idShipperDialCode']),$require_flag); 
                    $this->set_szShipperEmail(sanitize_all_html_input($landingContent['szShipperEmail']),$require_flag);
                    $this->set_szShipperPostcode(sanitize_all_html_input($landingContent['szShipperPostcode']),$require_flag);
                                           
                    $this->szShipperAddress_pickup = $this->szShipperAddress;
                    $this->szShipperPostcode_pickup = $this->szShipperPostcode;
                    $this->szShipperCity_pickup = $this->szShipperCity;
                    $this->idShipperCountry_pickup = $this->idShipperCountry;
                    
                    /*
                    * Voga: Standard Cargo - From and To
                    */
                    $this->set_szFromInstruction(sanitize_all_html_input($landingContent['szFromInstruction']),$voga_automatic_require_flag); 
                    $this->set_szToInstruction(sanitize_all_html_input($landingContent['szToInstruction']),$voga_automatic_require_flag); 
                    $this->set_szCargoType(sanitize_all_html_input($landingContent['szCargoType']),$voga_automatic_require_flag); 
                    
                    /*
                    * Voga: Standard Cargo
                    */
                    $this->set_iDefaultInsurance(sanitize_all_html_input($landingContent['iDefaultInsurance']),$voga_automatic_require_flag); 
                    $this->set_idInsuranceCurrency(sanitize_all_html_input($landingContent['idInsuranceCurrency']),$voga_automatic_require_flag); 
                     
                    /*
                    * Voga: Common fields
                    */
                    $this->set_szMiddleText(sanitize_all_html_input($landingContent['szMiddleText']),$common_voga_required_flag);
                    $this->set_szContactMe(sanitize_all_html_input($landingContent['szContactMe']),$common_voga_required_flag);
                            
                    $this->set_iDisplaySeoText(sanitize_all_html_input($landingContent['iDisplaySeoText']),$require_feild_flag);
                    if($this->iDisplaySeoText==1 && $require_feild_flag)
                    {
                        $require_feild_flag_seo = true ;
                    }
                    else
                    {
                        $require_feild_flag_seo = false ;
                    }
                    
                    $this->set_szBackgroundColor(sanitize_all_html_input($landingContent['szBackgroundColor']),$require_feild_flag_seo);
                    $this->set_szSeoHeading(sanitize_all_html_input($landingContent['szSeoHeading']),$require_feild_flag_seo);
                    $this->set_szSeoDescription(sanitize_specific_tinymce_html_input($landingContent['szSeoDescription']),$require_feild_flag_seo);
                    
                    for($i=1;$i<4;$i++)
                    {
                        if($i==1)
                        {
                            $szErrorMessage = " - ".t($this->t_base.'fields/image_url') ;
                        }
                        else if($i==2)
                        {
                            $szErrorMessage = " - ".t($this->t_base.'fields/image_description') ;
                        }
                        else
                        {
                            $szErrorMessage = " - ". t($this->t_base.'fields/image_title') ;
                        }

                        $this->set_szTextA(sanitize_specific_tinymce_html_input($landingContent['szTextA'.$i]),$require_feild_flag,$i,$szErrorMessage);	
                    }			
                    $szErrorMessage = " - Section 1 heading" ;
                    $this->set_szTextB(sanitize_specific_tinymce_html_input($landingContent['szTextB']),$bNonHoldingPageVariableRequired,1,$szErrorMessage);	
                    $szErrorMessage = " - From pre-fill" ;
                    if($landingContent['iDefault']!=2 && !$voga_automatic_require_flag)
                    {    
                        $this->set_szTextB(sanitize_specific_tinymce_html_input($landingContent['szTextB2']),$bNonHoldingPageVariableRequired,2,$szErrorMessage);
                    }
                    $szErrorMessage = " - Description" ;
                    $this->set_szTextB(sanitize_specific_tinymce_html_input($landingContent['szTextB3']),false,3,$szErrorMessage);
                    $this->set_szTextC(sanitize_specific_tinymce_html_input($landingContent['szTextC']),$bNonHoldingPageVariableRequired);	

                    for($i=1;$i<10;$i++)
                    {
                            if($i==1 || $i==2 || $i==3)
                            {
                                $szErrorMessage = " - ".t($this->t_base.'fields/left') ;
                            }
                            else if($i==4 || $i==5 || $i==6)
                            {
                                $szErrorMessage = " - ".t($this->t_base.'fields/center') ;
                            }
                            else
                            {
                                $szErrorMessage = " - ".t($this->t_base.'fields/right') ;
                            }

                            if($i%3==1)
                            {
                                $szErrorMessage .= " ".t($this->t_base.'fields/image_url') ;
                            }
                            else if($i%3==2)
                            {
                                $szErrorMessage .= " ".t($this->t_base.'fields/image_description') ;
                            }
                            else
                            {
                                $szErrorMessage .= " ". t($this->t_base.'fields/image_title') ;
                            }
                            $this->set_szTextD(sanitize_specific_tinymce_html_input($landingContent['szTextD'.$i]),$bNonHoldingPageVariableRequired,$i,$szErrorMessage);	
                    }	
                    for($i=1;$i<7;$i++)
                    {
                            if($i==1 || $i==2)
                            {
                                    $szErrorMessage = " - ".t($this->t_base.'fields/left') ;
                            }
                            else if($i==3 || $i==4)
                            {
                                    $szErrorMessage = " - ".t($this->t_base.'fields/center') ;
                            }
                            else
                            {
                                    $szErrorMessage = " - ".t($this->t_base.'fields/right') ;
                            }

                            if($i%2==1)
                            {
                                    $szErrorMessage .= " ".t($this->t_base.'fields/heading') ;
                            }
                            else
                            {
                                    $szErrorMessage .= " ". t($this->t_base.'fields/text') ;
                            }
                            $this->set_szTextE(sanitize_specific_tinymce_html_input($landingContent['szTextE'.$i]),$bNonHoldingPageVariableRequired,$i,$szErrorMessage);	
                    }

                    $this->set_szTextF(sanitize_specific_tinymce_html_input($landingContent['szTextF1']),$require_feild_flag,1);	
			
                    for($i=1;$i<10;$i++)
                    {
                            if($i==1 || $i==2 || $i==3)
                            {
                                    $szErrorMessage = " - ".t($this->t_base.'fields/left') ;
                            }
                            else if($i==4 || $i==5 || $i==6)
                            {
                                    $szErrorMessage = " - ".t($this->t_base.'fields/center') ;
                            }
                            else
                            {
                                    $szErrorMessage = " - ".t($this->t_base.'fields/right') ;
                            }

                            if($i%3==1)
                            {
                                    $szErrorMessage .= " ".t($this->t_base.'fields/image_url') ;
                            }
                            else if($i%3==2)
                            {
                                    $szErrorMessage .= " ".t($this->t_base.'fields/image_description') ;
                            }
                            else
                            {
                                    $szErrorMessage .= " ". t($this->t_base.'fields/image_title') ;
                            }
                            $this->set_szTextG(sanitize_specific_tinymce_html_input($landingContent['szTextG'.$i]),$require_feild_flag,$i,$szErrorMessage);	
                    }
                    for($i=1;$i<4;$i++)
                    {
                            if($i==1)
                            {
                                    $szErrorMessage = " - ".t($this->t_base.'fields/left') ;
                            }
                            else if($i==2)
                            {
                                    $szErrorMessage = " - ".t($this->t_base.'fields/center') ;
                            }
                            else
                            {
                                    $szErrorMessage = " - ".t($this->t_base.'fields/right') ;
                            }				
                            $szErrorMessage .= " ". t($this->t_base.'fields/text') ;

                            $this->set_szTextH(sanitize_specific_tinymce_html_input($landingContent['szTextH'.$i]),$require_feild_flag,$i,$szErrorMessage);	
                    }
                    for($i=1;$i<8;$i++)
                    {
                            if($i%3==1)
                            {
                                    $szErrorMessage = " - ".t($this->t_base.'fields/image_url') ;
                            }
                            else if($i%3==2)
                            {
                                    $szErrorMessage = " - ".t($this->t_base.'fields/image_description') ;
                            }
                            else
                            {
                                    $szErrorMessage = " - ". t($this->t_base.'fields/image_title') ;
                            }
                            $this->set_szTextI(sanitize_specific_tinymce_html_input($landingContent['szTextI'.$i]),$require_feild_flag,$i,$szErrorMessage);	
                    }
                    $this->set_szTextJ(sanitize_specific_tinymce_html_input($landingContent['szTextJ1']),$require_feild_flag,1);

                    for($i=1;$i<4;$i++)
                    {
                            if($i%4==1)
                            {
                                    $szErrorMessage = " - ".t($this->t_base.'fields/image_url') ;
                            }
                            else if($i%4==2)
                            {
                                    $szErrorMessage = " - ".t($this->t_base.'fields/image_description') ;
                            }
                            else if($i%4==3)
                            {
                                    $szErrorMessage = " - ".t($this->t_base.'fields/image_title') ;
                            }
                            else
                            {
                                    $szErrorMessage = " - ". t($this->t_base.'fields/video_script') ;
                            }
                            $this->set_szTextL(sanitize_specific_tinymce_html_input($landingContent['szTextL'.$i]),$require_feild_flag,$i,$szErrorMessage);	
                    }			
                    $szErrorMessage = " - ". t($this->t_base.'fields/video_script') ;
                    $this->set_szTextL($landingContent['szTextL4'],false,4,$szErrorMessage);

                    $logo_count = count($landingContent['szImageURL']);
                    for($i=1;$i<=$logo_count;$i++)
                    {
                        $szErrorMessage = t($this->t_base.'fields/testimonial')." ".$i ;
                        $this->set_szImageURL(sanitize_specific_tinymce_html_input($landingContent['szImageURL'][$i]),$require_feild_flag,$i,$szErrorMessage);
                        $this->set_szImageDesc(sanitize_specific_tinymce_html_input($landingContent['szImageDesc'][$i]),$require_feild_flag,$i,$szErrorMessage);
                        $this->set_szImageTitle(sanitize_specific_tinymce_html_input($landingContent['szImageTitle'][$i]),$require_feild_flag,$i,$szErrorMessage);
                        $this->set_szTestimonialHeading(sanitize_specific_tinymce_html_input($landingContent['szHeading'][$i]),$require_feild_flag,$i,$szErrorMessage);
                        $this->set_szTextDesc(sanitize_specific_tinymce_html_input($landingContent['szTextDesc'][$i]),$require_feild_flag,$i,$szErrorMessage);
                        $this->set_szTitleName(sanitize_specific_tinymce_html_input($landingContent['szTitleName'][$i]),$require_feild_flag,$i,$szErrorMessage);
                        $this->set_idTestimonial(sanitize_specific_tinymce_html_input($landingContent['idTestimonial'][$i]),$require_feild_flag,$i);
                    }	
                    $logo_count = count($landingContent['szLogoImageURL']); 

                    for($i=1;$i<=$logo_count;$i++)
                    {
                        $szErrorMessage = t($this->t_base.'fields/logo')." ".$i ;
                        $this->set_szLogoImageURL(sanitize_specific_tinymce_html_input($landingContent['szLogoImageURL'][$i]),$require_feild_flag,$i,$szErrorMessage);
                        $this->set_szLogoImageTitle(sanitize_specific_tinymce_html_input($landingContent['szLogoImageTitle'][$i]),$require_feild_flag,$i,$szErrorMessage);
                        $this->set_szLogoImageDesc(sanitize_specific_tinymce_html_input($landingContent['szLogoImageDesc'][$i]),$require_feild_flag,$i,$szErrorMessage);
                        $this->set_szToolTipText(sanitize_specific_tinymce_html_input($landingContent['szToolTipText'][$i]),$require_feild_flag,$i,$szErrorMessage);

                        $szErrorMessage1 = t($this->t_base.'fields/white_logo')." ".$i ;
                        $this->set_szWhiteLogoImageURL(sanitize_specific_tinymce_html_input($landingContent['szWhiteLogoImageURL'][$i]),$require_feild_flag,$i,$szErrorMessage1);

                        $this->set_idPartnerLogo(sanitize_specific_tinymce_html_input($landingContent['idPartnerLogo'][$i]),$require_feild_flag,$i);
                    }
                    $this->set_iPublished((int)$landingContent['iActive']);
                    $this->set_iLanguage((int)$landingContent['iLanguage']);
                    
                    $this->iPageSearchType = (int)$landingContent['iPageSearchType'];
                    $this->fDefaultWeight = (float)$landingContent['fDefaultWeight']; 

                    if($this->error == true)
                    {
                            return false;
                    }
                    else
                    {
                            return true;
                    }
		}
		else
		{
			return false;
		}
	}	
	
	function getMaxOrderLandingPageData()
	{
		$query="
			SELECT
				MAX(iOrder)  as orders
			FROM
				".__DBC_SCHEMATA_LANDING_PAGE_DATA__."
			WHERE
				iActive='1'
			AND
				iDefault = '0'
			";
		//echo "<br /> ".$query ;
		if($result=$this->exeSQL($query))
		{
			$row = $this->getAssoc( $result );
			if($row['orders']>0)
			{
				return $row['orders'];
			}
			else
			{
				return 0;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}

	function getMaxOrderNewLandingPageData()
	{
		$query="
			SELECT
				MAX(iOrder)  as orders
			FROM
				".__DBC_SCHEMATA_CUSTOM_LANDING_PAGE__."
			WHERE
				iActive='1'
			AND
				iDefault = '0'
			";
		//echo "<br /> ".$query ;
		if($result=$this->exeSQL($query))
		{
			$row = $this->getAssoc( $result );
			if($row['orders']>0)
			{
				return $row['orders'];
			}
			else
			{
				return 0;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function editLandingPage($landingContent)
	{
		if(!empty($landingContent))
		{
			$this->validateLandingPageData($landingContent);
			$this->set_id((int)$landingContent['idLandingPage']);
			
			if($this->error == true)
			{
				return false;
			}
			
			if($this->isUrlExists($this->szUrl,$this->id))
			{
				$this->addError("szUrl", t($this->t_base.'fields/url_already_exists'));
				return false;
			}
			
			$query="
				UPDATE
					".__DBC_SCHEMATA_LANDING_PAGE_DATA__."
				SET
					szSeoKeywords = '".mysql_escape_custom(trim($this->szPageHeading))."',
					szMetaTitle = '".mysql_escape_custom(trim($this->szMetaTitle))."',
					szMetaKeywords = '".mysql_escape_custom(trim($this->szMetaKeywords))."',
					szMetaDescription = '".mysql_escape_custom(trim($this->szMetaDescription))."',
					szUrl = '".mysql_escape_custom(trim($this->szUrl))."',
					iPublished = '".(int)$this->iPublished."',
					iLanguage = '".(int)$this->iLanguage."',					
					szTextA = '".mysql_escape_custom(trim($this->szTextA))."',
					szTextB = '".mysql_escape_custom(trim($this->szTextB))."',
					szTextC = '".mysql_escape_custom(trim($this->szTextC))."',
					szTextD = '".mysql_escape_custom(trim($this->szTextD))."',
					szTextE = '".mysql_escape_custom(trim($this->szTextE))."',
					szTextF = '".mysql_escape_custom(trim($this->szTextF))."',
					szTextG = '".mysql_escape_custom(trim($this->szTextG))."',
					szTextH = '".mysql_escape_custom(trim($this->szTextH))."',
					szTextI = '".mysql_escape_custom(trim($this->szTextI))."',
					szTextJ = '".mysql_escape_custom(trim($this->szTextJ))."',
					szTextK = '".mysql_escape_custom(trim($this->szTextK))."',
					szTextL = '".mysql_escape_custom(trim($this->szTextL))."',
					szTextM = '".mysql_escape_custom(trim($this->szTextM))."',
					dtUpdatedOn = now()
				WHERE
					id = '".(int)$this->id."'
			";
			//echo "<br /> ".$query."<br /> ";
			if($result=$this->exeSQL($query))
			{
				/*
				 * Updating content of transportecaSitemap.xml file
				*/
				$kSEO = new cSEO();
				$kSEO->updateSitemapXmlFile();
				
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function moveUpDownLandingPagesLinks($id,$orderBy,$iFromPage=false)
	{
		$this->set_id((int)$id);
		$this->set_iOrderBy((int)$orderBy);
		
		if($iFromPage=='NEW_LANDING_PAGE')
		{
			$szTableName = __DBC_SCHEMATA_CUSTOM_LANDING_PAGE__ ;
		}
		else
		{
			$szTableName = __DBC_SCHEMATA_LANDING_PAGE_DATA__ ;
		}
		$query="
   				SELECT
   					id,
   			 		iOrder
   			 	FROM
   			 		".$szTableName."
   			 	WHERE
   			 		id='".(int)$this->id."'
   				";
		//echo $query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
			if ($this->getRowCnt() > 0)
			{
				$row = $this->getAssoc( $result);
				$order = $row['iOrder'];
				$this->id = $row['id'];
					
				$query="
						UPDATE
							".$szTableName."
					 	SET
					 		";
				if($this->iOrderBy==__ORDER_MOVE_DOWN__)
				{
					$query.= " 	iOrder = iOrder-1 ";
				}
				if($this->iOrderBy==__ORDER_MOVE_UP__)
				{
					$query .=" iOrder = iOrder+1 ";
				}
				
				$query.=" WHERE
					 		id='".(int)$this->id."'
				 		AND
					 		iActive='1'
						";
				
				//echo $query;
					
				if($result = $this->exeSQL( $query ))
				{
					$query="
							UPDATE
								".$szTableName."
							SET
							";
					if($this->iOrderBy==__ORDER_MOVE_DOWN__)
					{
						$query.=" iOrder = iOrder+1 ";
					}
					if($this->iOrderBy==__ORDER_MOVE_UP__)
					{
						$query.=" iOrder = iOrder-1 ";
					}
					
					$query.="
							WHERE
								id!='".$this->id."'
							AND
					 			iActive='1'
							AND
								";
					if($this->iOrderBy==__ORDER_MOVE_DOWN__)
					{
						$query.=" iOrder= ".(int)$order."-1 ";
					}
					if($this->iOrderBy==__ORDER_MOVE_UP__)
					{
						$query.=" iOrder= ".(int)$order."+1 ";
					}
					
					//echo $query;
					if($result = $this->exeSQL( $query ))
					{
						return true;
					}
					else
					{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					}
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
								return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	

	function deleteLandingPage($id,$iFromPage=false)
	{
		$this->set_id((int)$id);
		if($iFromPage=='NEW_LANDING_PAGE')
		{
			$szTableName = __DBC_SCHEMATA_CUSTOM_LANDING_PAGE__ ;
		}
		else
		{
			$szTableName = __DBC_SCHEMATA_LANDING_PAGE_DATA__ ;
		}
		
		if($this->id>0)
		{
			$query="
   				SELECT
   			 		iOrder
   			 	FROM
   			 		".$szTableName."
   			 	WHERE
   			 		id='".(int)$id."'
   				"; 
			if( ( $result = $this->exeSQL( $query ) ) )
			{
				if ($this->getRowCnt() > 0)
				{
					$row = $this->getAssoc( $result);
					$order = $row['iOrder'];
	
					$query="
						UPDATE
							".$szTableName."
						SET
						    iActive = '0'
						WHERE
							id = '".(int)$this->id."'
						";
					//echo $query;
					if($result=$this->exeSQL($query))
					{ 
						/*
						 * Updating content of transportecaSitemap.xml file
						*/
						//$kSEO = new cSEO();
						//$kSEO->updateSitemapXmlFile();
						
						$query="
							UPDATE
								".$szTableName."
						 	SET
								iOrder = iOrder-1
							 WHERE
						 		iOrder > '".(int)$order."'
						 	 AND
						 		iActive='1'
					 	";
						//echo $query;DIE;
						if( $result = $this->exeSQL($query))
						{
							return true;
						}
					}
					else
					{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					}
				}
			}
		}
	}
	

	function getAllNewLandingPageData($idLandingPage=false,$szUrl=false,$iDefaultPage=false,$iLanguage=false,$iPublished=false,$labelFlag=false,$bVogaPages=false,$iHoldingPage=false,$szAutomatedRfqResponseCode='')
	{ 
            if($idLandingPage>0)
            {
                $query_and= " WHERE
                            id='".(int)$idLandingPage."'
                ";
            }
            else
            {
                $query_and= " WHERE iActive = '1' ";
            }
            
            if($szAutomatedRfqResponseCode)
            {
                $query_and .= " AND szAutomatedRfqResponseCode = '".mysql_escape_custom($szAutomatedRfqResponseCode)."' ";
            }

            if($iPublished)
            {
                $query_and .= " AND iPublished = '1' ";
            }

            if(!empty($szUrl))
            {
                $query_and .= " AND lpd.szUrl = '".mysql_escape_custom(trim($szUrl))."' ";
            }
            if($iDefaultPage)
            {
                $query_and .= " AND lpd.iDefault = '1' ";
            }
            if($labelFlag)
            {
                $query_and .= " AND lpd.iDefault = '2' ";
            }
            if($iLanguage>0)
            {
                    $query_and .= " AND iLanguage = '".(int)$iLanguage."' ";
            } 
            
            if($iHoldingPage)
            {
                $query_and .= " AND iHoldingPage = '1' ";
            }
            if($bVogaPages)
            {
                /*
                * Fetching only Standard Cargo aka Voga pages
                */
                $query_and .= " AND (iPageSearchType = '4' OR iPageSearchType = '5' OR iPageSearchType = '6')";
                $query_order_by = " ORDER BY szSeoKeywords ASC ";
            }
            else
            {
                $query_order_by = " ORDER BY iOrder ASC ";
            }

            $query="
                SELECT
                    lpd.id,
                    lpd.szSeoKeywords,
                    lpd.szMetaTitle,
                    lpd.szMetaKeywords,
                    lpd.szMetaDescription,
                    lpd.szUrl,
                    lpd.iPublished,
                    lpd.szTextA1,
                    lpd.szTextA2,
                    lpd.szTextA3,
                    lpd.szTextB,
                    lpd.szTextB2,
                    lpd.szTextB3,
                    lpd.szTextC,
                    lpd.szTextD1,
                    lpd.szTextD2,
                    lpd.szTextD3,
                    lpd.szTextD4,
                    lpd.szTextD5,
                    lpd.szTextD6,
                    lpd.szTextD7,
                    lpd.szTextD8,
                    lpd.szTextD9,
                    lpd.szTextE1,
                    lpd.szTextE2,
                    lpd.szTextE3,
                    lpd.szTextE4,
                    lpd.szTextE5,
                    lpd.szTextE6,
                    lpd.szTextF1,
                    lpd.szTextG1,
                    lpd.szTextG2,
                    lpd.szTextG3,
                    lpd.szTextG4,				
                    lpd.szTextG5,
                    lpd.szTextG6,
                    lpd.szTextG7,
                    lpd.szTextG8,
                    lpd.szTextG9,
                    lpd.szTextH1,
                    lpd.szTextH2,
                    lpd.szTextH3,
                    lpd.szTextI1,
                    lpd.szTextI2,
                    lpd.szTextI3,
                    lpd.szTextI4,
                    lpd.szTextI5,
                    lpd.szTextI6,
                    lpd.szTextI7,
                    lpd.szTextJ1,
                    lpd.szTextL1,
                    lpd.szTextL2,
                    lpd.szTextL3,
                    lpd.szTextL4,
                    lpd.dtCreatedOn,
                    lpd.iActive,
                    lpd.iOrder,
                    lpd.iLanguage,
                    lpd.iDefault,	
                    lpd.iDisplaySeoText,
                    lpd.szBackgroundColor,
                    lpd.szHeading,
                    lpd.szSeoDescription,
                    lpd.iPageSearchType,
                    lpd.fDefaultWeight,
                    lpd.idSearchMini,
                    lpd.szMiddleText,
                    lpd.szContactMe, 
                    lpd.iHoldingPage,
                    lpd.szHoldingHeading1,
                    lpd.szHoldingHeading2,
                    lpd.szHoldingTextBold1,
                    lpd.szHoldingTextBold2,
                    lpd.szHoldingTextBold3,
                    lpd.szHoldingTextPlain1,
                    lpd.szHoldingTextPlain2,
                    lpd.szHoldingTextPlain3, 
                    lpd.szHoldingEmailHeading,
                    lpd.szHoldingFromHeading,
                    lpd.szHoldingToHeading,
                    lpd.szHoldingSubmitButtonText,
                    lpd.szHoldingThanksButtonText,
                    lpd.szYoutubeUrl,
                    lpd.iNumButtonClicked,
                    lpd.szFromInstruction,
                    lpd.szToInstruction,
                    lpd.szCargoType,
                    lpd.iDefaultInsurance,
                    lpd.idInsuranceCurrency,
                    lpd.szAutomatedRfqResponseCode
                FROM
                    ".__DBC_SCHEMATA_CUSTOM_LANDING_PAGE__." lpd
                $query_and
                $query_order_by 
            ";
            //echo $query."<br/><br/>";
            if($result=$this->exeSQL($query))
            {
                $ret_ary = array();
                $ctr=0;
                while($row=$this->getAssoc($result))
                {
                    $resArr=array();
                    $iNumButtonClicked=0;
                    if((int)$row['iHoldingPage']==0)
                    {
                        $iNumButtonClicked=$this->getBButtonClickData($row['id'],true);
                    }
                    else
                    {
                        $iNumButtonClicked=$this->getBButtonClickDataHoldingPages($row['id'],true);
                    }
                    if($row['iPageSearchType']==__SEARCH_TYPE_VOGUE__)
                    {
                        $resArr=$this->getStandardShipperDetails($row['id']);
                        $newArr=  array_merge($row,$resArr);
                        $ret_ary[$ctr] = $newArr;
                    }
                    else
                    {
                        $ret_ary[$ctr] = $row;
                    } 
                    $ret_ary[$ctr]['iNumButtonClicked'] = $row['iNumButtonClicked']+$iNumButtonClicked;
                    $ctr++;
                } 
                return $ret_ary;
            }
            else
            { 
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
	
	function editNewLandingPage($landingContent)
	{
		if(!empty($landingContent))
		{
			$this->validateNewLandingPageData($landingContent);
			$this->set_id((int)$landingContent['idLandingPage']);
			
			if($this->error == true)
			{
				return false;
			}
			
			if($this->isUrlExists($this->szUrl,$this->id))
			{
				$this->addError("szUrl", t($this->t_base.'fields/url_already_exists'));
				return false;
			} 		
			$query="
                            UPDATE
                                ".__DBC_SCHEMATA_CUSTOM_LANDING_PAGE__."
                            SET
                                szSeoKeywords = '".mysql_escape_custom(trim($this->szPageHeading))."',
                                szMetaTitle = '".mysql_escape_custom(trim($this->szMetaTitle))."',
                                szMetaKeywords = '".mysql_escape_custom(trim($this->szMetaKeywords))."',
                                szMetaDescription = '".mysql_escape_custom(trim($this->szMetaDescription))."',
                                szUrl = '".mysql_escape_custom(trim($this->szUrl))."',
                                iPublished = '".(int)$this->iPublished."',
                                iLanguage = '".(int)$this->iLanguage."',					
                                szTextA1 = '".mysql_escape_custom(trim($this->szTextA[1]))."',
                                szTextA2 = '".mysql_escape_custom(trim($this->szTextA[2]))."',
                                szTextA3 = '".mysql_escape_custom(trim($this->szTextA[3]))."',
                                szTextB = '".mysql_escape_custom(trim($this->szTextB[1]))."',
                                szTextB2 = '".mysql_escape_custom(trim($this->szTextB[2]))."',
                                szTextB3 = '".mysql_escape_custom(trim($this->szTextB[3]))."', 
                                szTextC = '".mysql_escape_custom(trim($this->szTextC))."',
                                szTextD1 = '".mysql_escape_custom(trim($this->szTextD[1]))."',
                                szTextD2 = '".mysql_escape_custom(trim($this->szTextD[2]))."',
                                szTextD3 = '".mysql_escape_custom(trim($this->szTextD[3]))."',
                                szTextD4 = '".mysql_escape_custom(trim($this->szTextD[4]))."',
                                szTextD5 = '".mysql_escape_custom(trim($this->szTextD[5]))."',
                                szTextD6 = '".mysql_escape_custom(trim($this->szTextD[6]))."',
                                szTextD7 = '".mysql_escape_custom(trim($this->szTextD[7]))."',
                                szTextD8 = '".mysql_escape_custom(trim($this->szTextD[8]))."',
                                szTextD9 = '".mysql_escape_custom(trim($this->szTextD[9]))."',					
                                szTextE1 = '".mysql_escape_custom(trim($this->szTextE[1]))."',
                                szTextE2 = '".mysql_escape_custom(trim($this->szTextE[2]))."',
                                szTextE3 = '".mysql_escape_custom(trim($this->szTextE[3]))."',
                                szTextE4 = '".mysql_escape_custom(trim($this->szTextE[4]))."',
                                szTextE5 = '".mysql_escape_custom(trim($this->szTextE[5]))."',
                                szTextE6 = '".mysql_escape_custom(trim($this->szTextE[6]))."',					
                                szTextF1 = '".mysql_escape_custom(trim($this->szTextF[1]))."',
                                szTextG1 = '".mysql_escape_custom(trim($this->szTextG[1]))."',
                                szTextG2 = '".mysql_escape_custom(trim($this->szTextG[2]))."',
                                szTextG3 = '".mysql_escape_custom(trim($this->szTextG[3]))."',
                                szTextG4 = '".mysql_escape_custom(trim($this->szTextG[4]))."',
                                szTextG5 = '".mysql_escape_custom(trim($this->szTextG[5]))."',
                                szTextG6 = '".mysql_escape_custom(trim($this->szTextG[6]))."',
                                szTextG7 = '".mysql_escape_custom(trim($this->szTextG[7]))."',
                                szTextG8 = '".mysql_escape_custom(trim($this->szTextG[8]))."',
                                szTextG9 = '".mysql_escape_custom(trim($this->szTextG[9]))."',					
                                szTextH1 = '".mysql_escape_custom(trim($this->szTextH[1]))."',
                                szTextH2 = '".mysql_escape_custom(trim($this->szTextH[2]))."',
                                szTextH3 = '".mysql_escape_custom(trim($this->szTextH[3]))."',					
                                szTextI1 = '".mysql_escape_custom(trim($this->szTextI[1]))."',
                                szTextI2 = '".mysql_escape_custom(trim($this->szTextI[2]))."',
                                szTextI3 = '".mysql_escape_custom(trim($this->szTextI[3]))."',
                                szTextI4 = '".mysql_escape_custom(trim($this->szTextI[4]))."',
                                szTextI5 = '".mysql_escape_custom(trim($this->szTextI[5]))."',
                                szTextI6 = '".mysql_escape_custom(trim($this->szTextI[6]))."',
                                szTextI7 = '".mysql_escape_custom(trim($this->szTextI[7]))."',					
                                szTextJ1 = '".mysql_escape_custom(trim($this->szTextJ[1]))."',					
                                szTextL1 = '".mysql_escape_custom(trim($this->szTextL[1]))."',
                                szTextL2 = '".mysql_escape_custom(trim($this->szTextL[2]))."',
                                szTextL3 = '".mysql_escape_custom(trim($this->szTextL[3]))."',
                                szTextL4 = '".mysql_escape_custom(trim($this->szTextL[4]))."',
                                iDisplaySeoText = '".mysql_escape_custom(trim($this->iDisplaySeoText))."',
                                szBackgroundColor ='".mysql_escape_custom(trim($this->szBackgroundColor))."',
                                szHeading ='".mysql_escape_custom(trim($this->szSeoHeading))."',
                                szSeoDescription ='".mysql_escape_custom(trim($this->szSeoDescription))."',
                                iPageSearchType = '".mysql_escape_custom(trim($this->iPageSearchType))."', 
                                fDefaultWeight = '".mysql_escape_custom(trim($this->fDefaultWeight))."', 
                                idSearchMini = '".mysql_escape_custom(trim($this->idSearchMini))."', 
                                szMiddleText = '".mysql_escape_custom(trim($this->szMiddleText))."', 
                                szContactMe = '".mysql_escape_custom(trim($this->szContactMe))."',   
                                iHoldingPage = '".mysql_escape_custom(trim($this->iHoldingPage))."',   
                                szHoldingHeading1 = '".mysql_escape_custom(trim($this->szHoldingHeading[1]))."',
                                szHoldingHeading2 = '".mysql_escape_custom(trim($this->szHoldingHeading[2]))."',
                                szHoldingTextBold1 = '".mysql_escape_custom(trim($this->szHoldingTextBold[1]))."',
                                szHoldingTextBold2 = '".mysql_escape_custom(trim($this->szHoldingTextBold[2]))."',
                                szHoldingTextBold3 = '".mysql_escape_custom(trim($this->szHoldingTextBold[3]))."', 
                                szHoldingTextPlain1 = '".mysql_escape_custom(trim($this->szHoldingTextPlain[1]))."',
                                szHoldingTextPlain2 = '".mysql_escape_custom(trim($this->szHoldingTextPlain[2]))."',
                                szHoldingTextPlain3 = '".mysql_escape_custom(trim($this->szHoldingTextPlain[3]))."',
                                szHoldingEmailHeading = '".mysql_escape_custom(trim($this->szHoldingEmailHeading))."',
                                szHoldingFromHeading = '".mysql_escape_custom(trim($this->szHoldingFromHeading))."',
                                szHoldingToHeading = '".mysql_escape_custom(trim($this->szHoldingToHeading))."',
                                szHoldingSubmitButtonText = '".mysql_escape_custom(trim($this->szHoldingSubmitButtonText))."',
                                szHoldingThanksButtonText = '".mysql_escape_custom(trim($this->szHoldingThanksButtonText))."',
                                szYoutubeUrl = '".mysql_escape_custom(trim($this->szYoutubeUrl))."', 
                                szFromInstruction = '".mysql_escape_custom(trim($this->szFromInstruction))."',
                                szToInstruction = '".mysql_escape_custom(trim($this->szToInstruction))."',
                                szCargoType = '".mysql_escape_custom(trim($this->szCargoType))."',
                                dtUpdatedOn = now(),
                                iDefaultInsurance = '".mysql_escape_custom(trim($this->iDefaultInsurance))."',
                                idInsuranceCurrency ='".mysql_escape_custom(trim($this->idInsuranceCurrency))."'
                            WHERE
                                id = '".(int)$this->id."'
			";
			//echo "<br /> ".$query."<br /> ";
			//die;
			if($result=$this->exeSQL($query))
			{
				$this->addCustomerTestimonial($this->id);
				$this->addPartnerLogo($this->id); 
                                //if($this->iPageSearchType==__SEARCH_TYPE_VOGUE__)
                                    $this->addStandardShipperDetails($this->id);
				/*
				 * Updating content of transportecaSitemap.xml file
				*/
				$kSEO = new cSEO();
				$kSEO->updateSitemapXmlFile();
				
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	
	/**
	 * This function checks if any testimonial is already exists than it simply update existing record otherwise insert new record into database
	 * @access public
	 * @param int
	 * @return boolean 
	 * @author Ajay
	 */
	
	function addCustomerTestimonial($idLandingPage)
	{	
            $testimonial_counter = count($this->szImageURL); 
            if($testimonial_counter>0)
            {	
                $ctr_ep = 0;
                $usedTestimonialIdAry = array();
                $query_emp = "";

                for($i=1;$i<=$testimonial_counter;$i++)
                {
                        $update_flag = false;
                        $added_flag = false;
                        if(((int)$this->idTestimonial[$i]>0) && ($this->isTestimonialIdExist($this->idTestimonial[$i],$idLandingPage)))
                        {
                                $query_emp="
                                        UPDATE
                                                ".__DBC_SCHEMATA_CUSTOMER_TESTIMONIAL__."
                                        SET	
                                                szImageURL = '".mysql_escape_custom(trim($this->szImageURL[$i]))."',
                                                szImageDesc = '".mysql_escape_custom(trim($this->szImageDesc[$i]))."',
                                                szImageTitle = '".mysql_escape_custom(trim($this->szImageTitle[$i]))."',
                                                szHeading = '".mysql_escape_custom(trim($this->szHeading[$i]))."',
                                                szTextDesc = '".mysql_escape_custom(trim($this->szTextDesc[$i]))."',
                                                szTitleName = '".mysql_escape_custom(trim($this->szTitleName[$i]))."',
                                                dtUpdatedOn = now()
                                        WHERE
                                                id = '".(int)$this->idTestimonial[$i]."' 		
                                        AND
                                                idLandingPage = '".(int)$idLandingPage."' 	
                                ";
                                $update_flag = true;					
                        }
                        else
                        {
                                $query_emp = "
                                        INSERT INTO
                                                ".__DBC_SCHEMATA_CUSTOMER_TESTIMONIAL__."
                                        (
                                                idLandingPage,
                                                szImageURL,
                                                szImageDesc,
                                                szImageTitle,
                                                szHeading,
                                                szTextDesc,
                                                szTitleName,
                                                iActive,
                                                dtCreatedOn
                                        )
                                        VALUES
                                        (
                                                '".mysql_escape_custom(trim($idLandingPage))."',
                                                '".mysql_escape_custom(trim($this->szImageURL[$i]))."',
                                                '".mysql_escape_custom(trim($this->szImageDesc[$i]))."',
                                                '".mysql_escape_custom(trim($this->szImageTitle[$i]))."',
                                                '".mysql_escape_custom(trim($this->szHeading[$i]))."',
                                                '".mysql_escape_custom(trim($this->szTextDesc[$i]))."',
                                                '".mysql_escape_custom(trim($this->szTitleName[$i]))."',
                                                '1',
                                                now()
                                        )
                                ";
                                $added_flag = true;
                        }				
                        //echo "<br> ".$query_emp;
                        //die;
                        if($result = $this->exeSQL($query_emp))
                        {	
                                // TO DO 
                                if($added_flag)
                                {
                                        $usedTestimonialIdAry[$ctr_ep] = $this->iLastInsertID ;
                                }
                                else
                                {
                                        $usedTestimonialIdAry[$ctr_ep] = $this->idTestimonial[$i] ;
                                }			
                                $ctr_ep++;
                        }
                        else
                        {
                                $this->error = true;
                                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                                return false;
                        }
                    }			
                    $this->deleteTestimonialHistory($usedTestimonialIdAry,$idLandingPage) ;
		}
		else
		{
			$this->deleteTestimonialHistory($usedTestimonialIdAry,$idLandingPage) ;
		}
            }
	

	public function deleteTestimonialHistory($usedTestimonialIdAry,$idLandingPage)
	{
		if($idLandingPage>0 && !empty($usedTestimonialIdAry))
		{
			$idLocation_str = implode(',',$usedTestimonialIdAry);
			
			if(!empty($idLocation_str))
			{
				$query_and = " AND id NOT IN (".$idLocation_str.")	 ";
			} 
			
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_CUSTOMER_TESTIMONIAL__."
				WHERE
					idLandingPage = '".(int)$idLandingPage."'	
					$query_and
			";
			
			if($result = $this->exeSQL( $query ) )
			{
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	

	function isTestimonialIdExist($idTestimonial,$idLandingPage)
	{	
		if($idTestimonial>0 && $idLandingPage>0)
		{
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_CUSTOMER_TESTIMONIAL__."
				WHERE
					id='".(int)$idTestimonial."'		
				AND
					idLandingPage = '".(int)$idLandingPage."'	
			"; 
			//echo $query;
			if($result = $this->exeSQL( $query ) )
			{
				if($this->getRowCnt()>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	function getAllTestimonials($idLandingPage,$order_by='ASC')
	{		
            if($idLandingPage>0)
            {			
                if($order_by=='DESC')
                {
                    $query_order_by = " ORDER BY id DESC";
                }
                else
                {
                    $query_order_by = " ORDER BY id ASC";
                }
                
                $query="
                    SELECT
                        id,
                        idLandingPage,
                        szImageURL,
                        szImageDesc,
                        szImageTitle,
                        szHeading,
                        szTextDesc,
                        szTitleName,
                        iActive,
                        dtCreatedOn,
                        dtUpdatedOn
                    FROM
                        ".__DBC_SCHEMATA_CUSTOMER_TESTIMONIAL__."
                    WHERE
                        idLandingPage = '".(int)$idLandingPage."'	
                    AND
                        iActive = '1'
                    $query_order_by
                ";
                
                if($result = $this->exeSQL( $query ) )
                {
                    if($this->getRowCnt()>0)
                    {
                        $ret_ary = array();
                        $ctr = 1;
                        while($row = $this->getAssoc($result))
                        {
                            $ret_ary[$ctr] = $row;
                            $ctr++;
                        }
                        return $ret_ary;
                    }
                    else
                    {
                        return false ;
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
            else
            {
                return false;
            }
	}
	
	

	/**
	 * This function checks if any testimonial is already exists than it simply update existing record otherwise insert new record into database
	 * @access public
	 * @param int
	 * @return boolean 
	 * @author Ajay
	 */
	
	function addPartnerLogo($idLandingPage)
	{	
		$logo_counter = count($this->szLogoImageURL); 
		if($logo_counter>0)
		{	
			$ctr_ep = 0;
			$usedLogoIdAry = array();
			$query_emp = "";
			
			for($i=1;$i<=$logo_counter;$i++)
			{
				$update_flag = false;
				$added_flag = false;
				if(((int)$this->idPartnerLogo[$i]>0) && ($this->isPartnerLogoIdExist($this->idPartnerLogo[$i],$idLandingPage)))
				{
					$query_emp="
						UPDATE
							".__DBC_SCHEMATA_PARTNER_LOGO__."
						SET	
							szLogoImageUrl = '".mysql_escape_custom(trim($this->szLogoImageURL[$i]))."',
							szImageDesc = '".mysql_escape_custom(trim($this->szLogoImageDesc[$i]))."',
							szImageTitle = '".mysql_escape_custom(trim($this->szLogoImageTitle[$i]))."',
							szToolTipText = '".mysql_escape_custom(trim($this->szToolTipText[$i]))."',
							szWhiteLogoImageURL = '".mysql_escape_custom(trim($this->szWhiteLogoImageURL[$i]))."',
							dtUpdatedOn = now()
						WHERE
							id = '".(int)$this->idPartnerLogo[$i]."' 		
						AND
							idLandingPage = '".(int)$idLandingPage."' 	
					";
					$update_flag = true;					
				}
				else
				{
					$query_emp = "
						INSERT INTO
							".__DBC_SCHEMATA_PARTNER_LOGO__."
						(
							idLandingPage,
							szLogoImageUrl,
							szImageDesc,
							szImageTitle,
							szToolTipText,
							szWhiteLogoImageURL,
							iActive,
							dtCreatedOn
						)
						VALUES
						(
							'".mysql_escape_custom(trim($idLandingPage))."',
							'".mysql_escape_custom(trim($this->szLogoImageURL[$i]))."',
							'".mysql_escape_custom(trim($this->szLogoImageDesc[$i]))."',
							'".mysql_escape_custom(trim($this->szLogoImageTitle[$i]))."',
							'".mysql_escape_custom(trim($this->szToolTipText[$i]))."',
							'".mysql_escape_custom(trim($this->szWhiteLogoImageURL[$i]))."',
							'1',
							now()
						)
					";
					$added_flag = true;
				}				
				//echo "<br> ".$query_emp;
				//die;
				if($result = $this->exeSQL($query_emp))
				{	
					// TO DO 
					if($added_flag)
					{
						$usedLogoIdAry[$ctr_ep] = $this->iLastInsertID ;
					}
					else
					{
						$usedLogoIdAry[$ctr_ep] = $this->idPartnerLogo[$i] ;
					}			
					$ctr_ep++;
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
			}
			$this->deletePartnerLogoHistory($usedLogoIdAry,$idLandingPage) ;
		}
		else
		{
                    $this->deletePartnerLogoHistory($usedLogoIdAry,$idLandingPage) ;
		}
	}
        
        function getWordpressMenuData($iLanguage)
        { 
            if($iLanguage==__LANGUAGE_ID_ENGLISH__)
            {
                
            } 
            $query="
                SELECT
                    id
                FROM
                    ".__DBC_SCHEMATA_PARTNER_LOGO__."
                WHERE
                    id='".(int)$idPartnerLogo."'		
                AND
                    idLandingPage = '".(int)$idLandingPage."'	
            "; 
            //echo $query;
            if($result = $this->exeSQL( $query ) )
            {
                    if($this->getRowCnt()>0)
                    {
                            return true;
                    }
                    else
                    {
                            return false;
                    }
            }
            else
            {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
            }
        }
        
        function importArticlesDataWordpress()
        {
            /*
             * Importing data for Article pages
             */
            $howDoesworksLinks = array();
            $iLanguage = 2;
            $kSEO =new cSEO(); 
            $howDoesworksLinks = $kSEO->getAllPublishedSeoPages($iLanguage,true);
            
            if($iLanguage==__LANGUAGE_ID_DANISH__)
            {
                $szLanguageCode=__DANISH_CODE_WP__;
            }else{
                $szLanguageCode=__ENGLISH_CODE_WP__;
            }
            $idLanguageValue=$this->getLanguageIdOfWordpress($szLanguageCode);
            
            $kBooking = new cBooking();
            $dtRealNow = $kBooking->getRealNow();
            if(!empty($howDoesworksLinks))
            {
                $ctr = 0;
                foreach($howDoesworksLinks as $howDoesworksLinkss)
                {
                    $wordPressPostData = array();
                    $wordPressPostData['post_author'] = 1;
                    $wordPressPostData['post_date'] = $dtRealNow;
                    $wordPressPostData['post_date_gmt'] = $dtRealNow;
                    $wordPressPostData['post_title'] = utf8_decode($howDoesworksLinkss['szPageHeading']);
                    $wordPressPostData['post_excerpt'] = '';
                    $wordPressPostData['post_status'] = 'publish';
                    $wordPressPostData['comment_status'] = 'closed';
                    $wordPressPostData['ping_status'] = 'closed';
                    $wordPressPostData['post_password'] = '';
                    $wordPressPostData['post_name'] = $howDoesworksLinkss['szUrl'];
                    $wordPressPostData['to_ping'] = '';
                    $wordPressPostData['pinged'] = '';
                    $wordPressPostData['post_parent'] = 1171;
                    $wordPressPostData['guid'] = '';
                    $wordPressPostData['menu_order'] = $ctr++;
                    $wordPressPostData['post_type'] = 'page'; 
                    $wordPressPostData['szMetaKeyword']=$howDoesworksLinkss['szMetaKeywords'];
                    $wordPressPostData['szMetaTitle']=$howDoesworksLinkss['szMetaTitle'];
                    $wordPressPostData['szMetaDescription']=$howDoesworksLinkss['szMetaDescription'];
                     
                    $wordPressPostData['szLinkTitle']=$howDoesworksLinkss['szLinkTitle'];
                    $wordPressPostData['szPictureDescription']=$howDoesworksLinkss['szPictureDescription'];
                    $wordPressPostData['szPictureTitle']=$howDoesworksLinkss['szPictureTitle'];
                    $wordPressPostData['szHeaderImage']=$howDoesworksLinkss['szHeaderImage'];
                    $wordPressPostData['post_content'] = utf8_decode($howDoesworksLinkss['szContent']);
                    $wordPressPostData['idLanguageValue']=$idLanguageValue;
                    
                    
                    $wordPressPostData['fCargoWeight']=$howDoesworksLinkss['fCargoWeight'];
                    $wordPressPostData['fCargoVolume']=$howDoesworksLinkss['fCargoVolume'];
                    $wordPressPostData['szToSelection']=$howDoesworksLinkss['szToSelection'];
                    $wordPressPostData['szFromSelection']=$howDoesworksLinkss['szFromSelection'];
                    $wordPressPostData['iFromIPLocation']=$howDoesworksLinkss['iFromIPLocation'];
                    $wordPressPostData['iToIPLocation']=$howDoesworksLinkss['iToIPLocation'];
                    
                    //$wordPressPostData['iArticleType']='Yes';
                    
                    
                    $idParentPage = $this->addWordpressPost($wordPressPostData); 
                }
            }
        }
        
        function importDataToWordpress()
        {
            $howDoesworksLinks = array();
            $companyLinksAry = array();
            $tranportpediaLinksAry = array(); 
            $iLanguage = 2;
            $leftmenuLinksAry = $this->selectPageDetailsUserEndByType($iLanguage); 
            
            if($iLanguage==__LANGUAGE_ID_DANISH__)
            {
                $szLanguageCode=__DANISH_CODE_WP__;
            }else{
                $szLanguageCode=__ENGLISH_CODE_WP__;
            }
            $idLanguageValue=$this->getLanguageIdOfWordpress($szLanguageCode);
            
            
            
            $howDoesworksLinks = $leftmenuLinksAry[__HOW_IT_WORKS__];
            $companyLinksAry = $leftmenuLinksAry[__COMPANY__];
            $tranportpediaLinksAry = $leftmenuLinksAry[__TRANSTPORTPEDIA__]; 
             
            $kBooking = new cBooking();
            $dtRealNow = $kBooking->getRealNow();
            if(!empty($howDoesworksLinks))
            {
                $ctr = 0;
                foreach($howDoesworksLinks as $howDoesworksLinkss)
                {
                    $wordPressPostData = array();
                    $wordPressPostData['post_author'] = 1;
                    $wordPressPostData['post_date'] = $dtRealNow;
                    $wordPressPostData['post_date_gmt'] = $dtRealNow;
                    $wordPressPostData['post_title'] = utf8_decode($howDoesworksLinkss['szPageHeading']);
                    $wordPressPostData['post_excerpt'] = '';
                    $wordPressPostData['post_status'] = 'publish';
                    $wordPressPostData['comment_status'] = 'closed';
                    $wordPressPostData['ping_status'] = 'closed';
                    $wordPressPostData['post_password'] = '';
                    $wordPressPostData['post_name'] = $howDoesworksLinkss['szLink'];
                    $wordPressPostData['to_ping'] = '';
                    $wordPressPostData['pinged'] = '';
                    $wordPressPostData['post_parent'] = 8;
                    $wordPressPostData['guid'] = '';
                    $wordPressPostData['menu_order'] = $ctr++;
                    $wordPressPostData['post_type'] = 'page';
                    $wordPressPostData['szBackgroundColor']=str_replace("#","",$howDoesworksLinkss['szBackgroundColor']);
                    $wordPressPostData['szMetaKeyword']=$howDoesworksLinkss['szMetaKeyWord'];
                    $wordPressPostData['szMetaTitle']=$howDoesworksLinkss['szMetaTitle'];
                    $wordPressPostData['szMetaDescription']=$howDoesworksLinkss['szMetaDescription'];
                    
                     $wordPressPostData['szOpenGraphImage']=$howDoesworksLinkss['szOpenGraphImage'];
                    $wordPressPostData['szLinkTitle']=$howDoesworksLinkss['szLinkTitle'];
                    $wordPressPostData['szPictureDescription']=$howDoesworksLinkss['szPictureDescription'];
                    $wordPressPostData['szPictureTitle']=$howDoesworksLinkss['szPictureTitle'];
                    $wordPressPostData['szHeaderImage']=$howDoesworksLinkss['szHeaderImage'];
                    $wordPressPostData['idLanguageValue']=$idLanguageValue;
                    $idParentPage = $this->addWordpressPost($wordPressPostData);
                    $explainContentAry = array();
                    $explainContentAry = $this->getAllExplainContainDataByIdExplain($howDoesworksLinkss['id']);
                     
                    if(!empty($explainContentAry))
                    {
                        $ctr_1 = 0;
                        foreach($explainContentAry as $explainContentArys)
                        {
                            $wordPressPostData = array();
                            $wordPressPostData['post_author'] = 1;
                            $wordPressPostData['post_date'] = $dtRealNow;
                            $wordPressPostData['post_date_gmt'] = $dtRealNow;
                            $wordPressPostData['post_content'] = utf8_decode($explainContentArys['szDescription']);
                            $wordPressPostData['post_title'] = utf8_decode($explainContentArys['szPageName']);
                            $wordPressPostData['post_excerpt'] = '';
                            $wordPressPostData['post_status'] = 'publish';
                            $wordPressPostData['comment_status'] = 'closed';
                            $wordPressPostData['ping_status'] = 'closed';
                            $wordPressPostData['post_password'] = '';
                            $wordPressPostData['post_name'] = $explainContentArys['szUrl'];
                            $wordPressPostData['to_ping'] = '';
                            $wordPressPostData['pinged'] = '';
                            $wordPressPostData['post_parent'] = $idParentPage;
                            $wordPressPostData['guid'] = '';
                            $wordPressPostData['menu_order'] = $ctr_1++;
                            $wordPressPostData['post_type'] = 'page'; 
                            $wordPressPostData['szBackgroundColor']=str_replace("#","",$explainContentArys['szBackgroundColor']);
                            $wordPressPostData['szMetaKeyword']=$explainContentArys['szMetaKeyWord'];
                            $wordPressPostData['szMetaTitle']=$explainContentArys['szMetaTitle'];
                            $wordPressPostData['szMetaDescription']=$explainContentArys['szMetaDescription'];
                            $wordPressPostData['idLanguageValue']=$idLanguageValue;    
                           $this->addWordpressPost($wordPressPostData);
                        }
                    }
                }
            }
            
            if(!empty($companyLinksAry))
            {
                $ctr = 0;
                foreach($companyLinksAry as $companyLinksArys)
                {
                    $wordPressPostData = array();
                    $wordPressPostData['post_author'] = 1;
                    $wordPressPostData['post_date'] = $dtRealNow;
                    $wordPressPostData['post_date_gmt'] = $dtRealNow;
                    $wordPressPostData['post_title'] = utf8_decode($companyLinksArys['szPageHeading']);
                    $wordPressPostData['post_excerpt'] = '';
                    $wordPressPostData['post_status'] = 'publish';
                    $wordPressPostData['comment_status'] = 'closed';
                    $wordPressPostData['ping_status'] = 'closed';
                    $wordPressPostData['post_password'] = '';
                    $wordPressPostData['post_name'] = $companyLinksArys['szLink'];
                    $wordPressPostData['to_ping'] = '';
                    $wordPressPostData['pinged'] = '';
                    $wordPressPostData['post_parent'] = 13;
                    $wordPressPostData['guid'] = '';
                    $wordPressPostData['menu_order'] = $ctr++;
                    $wordPressPostData['post_type'] = 'page';
                    $wordPressPostData['szBackgroundColor']=str_replace("#","",$companyLinksArys['szBackgroundColor']);
                    $wordPressPostData['szMetaKeyword']=$companyLinksArys['szMetaKeyWord'];
                    $wordPressPostData['szMetaTitle']=$companyLinksArys['szMetaTitle'];
                    $wordPressPostData['szMetaDescription']=$companyLinksArys['szMetaDescription'];
                     $wordPressPostData['szOpenGraphImage']=$companyLinksArys['szOpenGraphImage'];
                    $wordPressPostData['szLinkTitle']=$companyLinksArys['szLinkTitle'];
                    $wordPressPostData['szPictureDescription']=$companyLinksArys['szPictureDescription'];
                    $wordPressPostData['szPictureTitle']=$companyLinksArys['szPictureTitle'];
                    $wordPressPostData['idLanguageValue']=$idLanguageValue;
                    $wordPressPostData['szHeaderImage']=$companyLinksArys['szHeaderImage'];
                    $idParentPage = $this->addWordpressPost($wordPressPostData);
                    $explainContentAry = array();
                    $explainContentAry = $this->getAllExplainContainDataByIdExplain($companyLinksArys['id']);
                     
                    if(!empty($explainContentAry))
                    {
                        $ctr_1 = 0;
                        foreach($explainContentAry as $explainContentArys)
                        {
                            $wordPressPostData = array();
                            $wordPressPostData['post_author'] = 1;
                            $wordPressPostData['post_date'] = $dtRealNow;
                            $wordPressPostData['post_date_gmt'] = $dtRealNow;
                            $wordPressPostData['post_content'] = utf8_decode($explainContentArys['szDescription']);
                            $wordPressPostData['post_title'] = utf8_decode($explainContentArys['szPageName']);
                            $wordPressPostData['post_excerpt'] = '';
                            $wordPressPostData['post_status'] = 'publish';
                            $wordPressPostData['comment_status'] = 'closed';
                            $wordPressPostData['ping_status'] = 'closed';
                            $wordPressPostData['post_password'] = '';
                            $wordPressPostData['post_name'] = $explainContentArys['szUrl'];
                            $wordPressPostData['to_ping'] = '';
                            $wordPressPostData['pinged'] = '';
                            $wordPressPostData['post_parent'] = $idParentPage;
                            $wordPressPostData['guid'] = '';
                            $wordPressPostData['menu_order'] = $ctr_1++;
                            $wordPressPostData['post_type'] = 'page'; 
                            $wordPressPostData['szBackgroundColor']=str_replace("#","",$explainContentArys['szBackgroundColor']);
                            $wordPressPostData['szMetaKeyword']=$explainContentArys['szMetaKeyWord'];
                            $wordPressPostData['szMetaTitle']=$explainContentArys['szMetaTitle'];
                            $wordPressPostData['szMetaDescription']=$explainContentArys['szMetaDescription'];
                            $wordPressPostData['idLanguageValue']=$idLanguageValue;
                            $this->addWordpressPost($wordPressPostData);
                        }
                    }
                }
            }
            
            if(!empty($tranportpediaLinksAry))
            {
                $ctr = 0;
                foreach($tranportpediaLinksAry as $tranportpediaLinksArys)
                {

                    $wordPressPostData = array();
                    $wordPressPostData['post_author'] = 1;
                    $wordPressPostData['post_date'] = $dtRealNow;
                    $wordPressPostData['post_date_gmt'] = $dtRealNow;
                    $wordPressPostData['post_title'] = utf8_decode($tranportpediaLinksArys['szPageHeading']);
                    $wordPressPostData['post_excerpt'] = '';
                    $wordPressPostData['post_status'] = 'publish';
                    $wordPressPostData['comment_status'] = 'closed';
                    $wordPressPostData['ping_status'] = 'closed';
                    $wordPressPostData['post_password'] = '';
                    $wordPressPostData['post_name'] = $tranportpediaLinksArys['szLink'];
                    $wordPressPostData['to_ping'] = '';
                    $wordPressPostData['pinged'] = '';
                    $wordPressPostData['post_parent'] = 17;
                    $wordPressPostData['guid'] = '';
                    $wordPressPostData['menu_order'] = $ctr++;
                    $wordPressPostData['post_type'] = 'page';
                    $wordPressPostData['szBackgroundColor']=str_replace("#","",$tranportpediaLinksArys['szBackgroundColor']);
                    $wordPressPostData['szMetaKeyword']=$tranportpediaLinksArys['szMetaKeyWord'];
                    $wordPressPostData['szMetaTitle']=$tranportpediaLinksArys['szMetaTitle'];
                    $wordPressPostData['szMetaDescription']=$tranportpediaLinksArys['szMetaDescription'];

                    $wordPressPostData['szOpenGraphImage']=$tranportpediaLinksArys['szOpenGraphImage'];
                    $wordPressPostData['szLinkTitle']=$tranportpediaLinksArys['szLinkTitle'];
                    $wordPressPostData['szPictureDescription']=$tranportpediaLinksArys['szPictureDescription'];
                    $wordPressPostData['szPictureTitle']=$tranportpediaLinksArys['szPictureTitle'];
                    $wordPressPostData['idLanguageValue']=$idLanguageValue;
                    $wordPressPostData['szHeaderImage']=$tranportpediaLinksArys['szHeaderImage'];
                    $idParentPage = $this->addWordpressPost($wordPressPostData);
                    $explainContentAry = array();
                    $explainContentAry = $this->getAllExplainContainDataByIdExplain($tranportpediaLinksArys['id']);

                    if(!empty($explainContentAry))
                    {
                        $ctr_1 = 0;
                        foreach($explainContentAry as $explainContentArys)
                        {
                            $wordPressPostData = array();
                            $wordPressPostData['post_author'] = 1;
                            $wordPressPostData['post_date'] = $dtRealNow;
                            $wordPressPostData['post_date_gmt'] = $dtRealNow;
                            $wordPressPostData['post_content'] = utf8_decode($explainContentArys['szDescription']);
                            $wordPressPostData['post_title'] = utf8_decode($explainContentArys['szPageName']);
                            $wordPressPostData['post_excerpt'] = '';
                            $wordPressPostData['post_status'] = 'publish';
                            $wordPressPostData['comment_status'] = 'closed';
                            $wordPressPostData['ping_status'] = 'closed';
                            $wordPressPostData['post_password'] = '';
                            $wordPressPostData['post_name'] = $explainContentArys['szUrl'];
                            $wordPressPostData['to_ping'] = '';
                            $wordPressPostData['pinged'] = '';
                            $wordPressPostData['post_parent'] = $idParentPage;
                            $wordPressPostData['guid'] = '';
                            $wordPressPostData['menu_order'] = $ctr_1++;
                            $wordPressPostData['post_type'] = 'page'; 
                            $wordPressPostData['szBackgroundColor']=str_replace("#","",$explainContentArys['szBackgroundColor']);
                            $wordPressPostData['szMetaKeyword']=$explainContentArys['szMetaKeyWord'];
                            $wordPressPostData['szMetaTitle']=$explainContentArys['szMetaTitle'];
                            $wordPressPostData['szMetaDescription']=$explainContentArys['szMetaDescription'];
                            $wordPressPostData['idLanguageValue']=$idLanguageValue;
                            $this->addWordpressPost($wordPressPostData);
                        }
                    }
                    
                }
            }
            
        } 
        
        function addWordpressPost($data)
        {
            if(!empty($data))
            { 
                $query = "
                    INSERT INTO
                        ".__DBC_SCHEMATA_WP_POSTS__."
                    (
                        post_author,
                        post_date,
                        post_date_gmt,
                        post_content,
                        post_title,
                        post_excerpt,
                        post_status,
                        comment_status,
                        ping_status,
                        post_password,
                        post_name,
                        to_ping,
                        pinged,
                        post_modified,
                        post_modified_gmt,
                        post_content_filtered,
                        post_parent,
                        guid,
                        menu_order,
                        post_type,
                        post_mime_type,
                        comment_count
                    )
                    VALUES
                    (
                        '".mysql_escape_custom($data['post_author'])."',	
                        '".mysql_escape_custom($data['post_date'])."',	
                        '".mysql_escape_custom($data['post_date_gmt'])."',
                        '".mysql_escape_custom($data['post_content'])."', 
                        '".mysql_escape_custom($data['post_title'])."',	
                        '".mysql_escape_custom($data['post_excerpt'])."',	
                        '".mysql_escape_custom($data['post_status'])."',	
                        '".mysql_escape_custom($data['comment_status'])."', 
                        '".mysql_escape_custom($data['ping_status'])."',	 
                        '".mysql_escape_custom($data['post_password'])."', 
                        '".mysql_escape_custom($data['post_name'])."',	
                        '".mysql_escape_custom($data['to_ping'])."',	
                        '".mysql_escape_custom($data['pinged'])."',	
                        '".mysql_escape_custom($data['post_modified'])."',	
                        '".mysql_escape_custom($data['post_modified_gmt'])."',	
                        '".mysql_escape_custom($data['post_content_filtered'])."',	
                        '".mysql_escape_custom($data['post_parent'])."',	 
                        '".mysql_escape_custom($data['guid'])."',	 
                        '".mysql_escape_custom($data['menu_order'])."',
                        '".mysql_escape_custom($data['post_type'])."',
                        '".mysql_escape_custom($data['post_mime_type'])."',
                        '".mysql_escape_custom($data['comment_count'])."' 
                    )
                "; 
                //print_r($data);
                //echo "<br><br>". $query;
                //die;
                if(($result = $this->exeSQL($query)))
                {	 
                    $idPostPage = $this->iLastInsertID; 
                    $this->addPostMeta($idPostPage);
                    
                    
                    if($data['iArticleType']!="")
                    {
                        $this->addPostArticleOption($data['iArticleType'],$idPostPage,'Article Type');
                    }                    
                    
                    if($data['szFromSelection']!="")
                    {
                        $this->addPostArticleOption($data['szFromSelection'],$idPostPage,'szFromSelection');
                    }                    
                    
                    if($data['szToSelection']!="")
                    {
                        $this->addPostArticleOption($data['szToSelection'],$idPostPage,'szToSelection');
                    }                    
                    
                    if($data['fCargoVolume']!="")
                    {
                        $this->addPostArticleOption($data['fCargoVolume'],$idPostPage,'fCargoVolume');
                    }
                    
                    if($data['fCargoWeight']!="")
                    {
                        $this->addPostArticleOption($data['fCargoWeight'],$idPostPage,'fCargoWeight');
                    }
                    
                     if($data['szToSelection']!="")
                    {
                        $this->addPostArticleOption($data['szToSelection'],$idPostPage,'szToSelection');
                    }
                    
                    if($data['iFromIPLocation']!="")
                    {
                        $this->addPostArticleOption($data['iFromIPLocation'],$idPostPage,'iFromIPLocation');
                    }
                    
                    
                    
                    if($data['szBackgroundColor']!="")
                    {
                        $this->addPostMetaBackgroundColor($data['szBackgroundColor'],$idPostPage);
                    }
                    if($data['szMetaKeyword']!="")
                    {
                        $this->addPostMetaKeyword($data['szMetaKeyword'],$idPostPage);
                    }
                    if($data['szMetaTitle']!="")
                    {
                        $this->addPostMetaTitle($data['szMetaTitle'],$idPostPage);
                    }
                    if($data['szMetaDescription']!="")
                    {
                        $this->addPostMetaDescription($data['szMetaDescription'],$idPostPage);
                    }
                    if($data['szOpenGraphImage']!="")
                    {
                        $this->addPostOGDescription($data['szOpenGraphImage'],$idPostPage);
                    }
                    
                    if($data['szPictureTitle']!="")
                    {
                        $this->addPostOGTitle($data['szPictureTitle'],$idPostPage);
                    }
                    
                    if($data['szOpenGraphImage']!="")
                    {
                        $this->addPostOGImageUrl($data['szOpenGraphImage'],$idPostPage);
                    }
                    if($data['idLanguageValue']>0)
                    {
                        $this->addPostLanguage($data['idLanguageValue'],$idPostPage);
                    }
                    
                    if($data['post_name']!="")
                    {
                        $this->addPostPageURL($data['post_name'],$idPostPage);
                    } 
                    if($data['szHeaderImage']!='')
                    {
                        $idPostImage=$this->getImagePostId($data['szHeaderImage']);
                        if((int)$idPostImage>0)
                        {
                            $this->addPostPageHeaderImage($idPostPage,$idPostImage);
                        }
                    } 
                    $this->updateGUID($idPostPage); 
                    return $idPostPage;
                }
            }
        }
        
        function addPostArticleOption($metaValue,$idPostPage,$metaKey)
        {
            if($idPostPage>0)
            {
                $query = "
                    INSERT INTO
                        ".__DBC_SCHEMATA_WP_POST_META__."
                    (
                        post_id,
                        meta_key,
                        meta_value 
                    )
                    VALUES
                    (
                        '".mysql_escape_custom($idPostPage)."',	
                        '".mysql_escape_custom($metaKey)."',	
                        '".mysql_escape_custom($metaValue)."' 
                    )
                ";  
                //echo "<br><br>". $query;
                //die;
                if(($result = $this->exeSQL($query)))
                {

                }
            }
        }
        function addPostPageHeaderImage($idPostPage,$idPostImage)
        { 
            if($idPostPage>0)
            {
                $query = "
                    INSERT INTO
                        ".__DBC_SCHEMATA_WP_POST_META__."
                    (
                        post_id,
                        meta_key,
                        meta_value 
                    )
                    VALUES
                    (
                        '".mysql_escape_custom($idPostPage)."',	
                        '_thumbnail_id',	
                        '".$idPostImage."' 
                    )
                ";  
                //echo "<br><br>". $query;
                //die;
                if(($result = $this->exeSQL($query)))
                {

                }
            } 
        }
        
        function getImagePostId($szHeaderImage)
        {
            $query="
                SELECT
                    post_id 
                FROM    
                    ".__DBC_SCHEMATA_WP_POST_META__."
                WHERE
                    meta_key='_wp_attached_file'
                AND    
                    meta_value='".mysql_escape_custom($szHeaderImage)."'
            ";
            //echo $query."<br /><br />";
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                $row=$this->getAssoc($result);
                                
                return $row['post_id'];
            }
            else
            {
                    return 0;
            }
        }
        function addPostPageURL($post_name,$idPostPage)
        {
            $query="
                INSERT INTO
                    ".__DBC_SCHEMATA_WORDPRESS_PAGE_URL__."
                (
                    idPost,
                    szPageUrl,
                    dtCreated
                )
                    VALUES
                (
                    '".(int)$idPostPage."',
                    '".mysql_escape_custom($post_name)."',
                    NOW()     
                )
            ";
            if(($result = $this->exeSQL($query)))
            {

            }
        }
        
        function addPostLanguage($idLanguageValue,$idPostPage)
        {
            $query="
                INSERT INTO
                    ".__DBC_SCHEMATA_WP_TERMS_RELATIONSHIPS__."
                (
                    object_id,
                    term_taxonomy_id
                )
                    VALUES
                (
                    '".(int)$idPostPage."',
                    '".(int)$idLanguageValue."'     
                )
            ";
            if(($result = $this->exeSQL($query)))
            {

            }
        }
        
        function updateGUID($idPostPage)
        {
            if($idPostPage>0)
            {
                $szGUIDUrl = "http://www.a.transport-eca.com/wordpress/?page_id=".$idPostPage;  
                $query = "
                    UPDATE
                        ".__DBC_SCHEMATA_WP_POSTS__."
                    SET	
                        guid = '".mysql_escape_custom(trim($szGUIDUrl))."'
                    WHERE
                        id = '".(int)$idPostPage."' 
                "; 
                //echo "<br>".$query;
                if(($result = $this->exeSQL($query)))
                {
                    
                }
            } 
        }
        
        function addPostMeta($idPostPage)
        { 
            if($idPostPage>0)
            {
                $query = "
                    INSERT INTO
                        ".__DBC_SCHEMATA_WP_POST_META__."
                    (
                        post_id,
                        meta_key,
                        meta_value 
                    )
                    VALUES
                    (
                        '".mysql_escape_custom($idPostPage)."',	
                        '_wp_page_template',	
                        'template-fullwidth.php' 
                    )
                ";  
                //echo "<br><br>". $query;
                //die;
                if(($result = $this->exeSQL($query)))
                {

                }
            } 
        }
          
	public function deletePartnerLogoHistory($usedLogoIdAry,$idLandingPage)
	{
		if($idLandingPage>0 && !empty($usedLogoIdAry))
		{
			$idLocation_str = implode(',',$usedLogoIdAry);
			
			if(!empty($idLocation_str))
			{
				$query_and = " AND id NOT IN (".$idLocation_str.")	 ";
			} 
			
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_PARTNER_LOGO__."
				WHERE
					idLandingPage = '".(int)$idLandingPage."'	
					$query_and
			";
			//echo "<br> ".$query ;
			if($result = $this->exeSQL( $query ) )
			{
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	

	function isPartnerLogoIdExist($idPartnerLogo,$idLandingPage)
	{	
		if($idPartnerLogo>0 && $idLandingPage>0)
		{
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_PARTNER_LOGO__."
				WHERE
					id='".(int)$idPartnerLogo."'		
				AND
					idLandingPage = '".(int)$idLandingPage."'	
			"; 
			//echo $query;
			if($result = $this->exeSQL( $query ) )
			{
				if($this->getRowCnt()>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	function getAllPartnerLogo($idLandingPage)
	{		
		if($idLandingPage>0)
		{			
			$query="
				SELECT
					id,
					idLandingPage, 
					szLogoImageUrl,
					szImageDesc,
					szImageTitle,
					szToolTipText,
					szWhiteLogoImageURL, 
					iActive,
					dtCreatedOn
				FROM
					".__DBC_SCHEMATA_PARTNER_LOGO__."
				WHERE
					idLandingPage = '".(int)$idLandingPage."'	
				AND
					iActive = '1'
			";
			if($result = $this->exeSQL( $query ) )
			{
				if($this->getRowCnt()>0)
				{
					$ret_ary = array();
					$ctr = 1;
					while($row = $this->getAssoc($result))
					{
						$ret_ary[$ctr] = $row;
						$ctr++;
					}
					return $ret_ary;
				}
				else
				{
					return false ;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function insertDataFromScript($data)
	{
		if(!empty($data))
		{			
			$query = "
				INSERT INTO
					".__DBC_SCHEMATA_EXPLAIN_CONTENT__."
				(
					szHeading,
					szDraftHeading,
					szDescription,
					szDraftDescription,
					iOrderBy,
					iOrderByDraft,
					iUpdated,
					iActive,
					idExplainData,
					dtUpdated
				)
				VALUES
				(
					'".mysql_escape_custom($data['szHeading'])."',	
					'".mysql_escape_custom($data['szDraftHeading'])."',	
					'".mysql_escape_custom($data['szDescription'])."',	
					'".mysql_escape_custom($data['szDraftDescription'])."',	
					'".mysql_escape_custom($data['iOrderBy'])."',	
					'".mysql_escape_custom($data['iOrderByDraft'])."',	
					'".mysql_escape_custom($data['iUpdated'])."',	
					1,
					'".mysql_escape_custom($data['idExplainData'])."',	
					NOW()
				)
			";
			//echo $query;
			//return true;
			
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				return true;
			}
		}
	}
	
	function getAllExplainContainDataByIdExplain($idExplain,$flag=false)
	{
	
		if($flag==true)
		{
			$orderQuery=
				"ORDER BY 
					iOrderByDraft ASC";
			$andQuery="
				AND	
				iOrderByDraft>0	
				";	 
		}
		else
		{
			$orderQuery=
				"ORDER BY 
					iOrderBy ASC";
			$andQuery="
			AND	
				iActive='1'
			AND
				iOrderBy>0	
				";	
		}
		$this->set_idExplain((int)$idExplain);
		$query="
			SELECT 
				id,
				szHeading,
				szDescription,
				szUrl,
				szPicture,
				szPictureTitle,
				szMetaKeyWord,
				szMetaDescription,
				szPageName,
				szMetaTitle,
				szBackgroundColor,
				iOrderBy,
				szDraftHeading,
				szDraftDescription,
				szDraftPictureTitle,
				szDraftMetaKeyWord,
				szDraftMetaDescription,
				szDraftPicture,
				szDraftPageName
			FROM 
				".__DBC_SCHEMATA_EXPLAIN_CONTENT__."	
			WHERE 
				idExplainData  = '".(int)$this->idExplain."'	
			$andQuery	
			$orderQuery				
				";
				//echo $query;	
		if( ( $result = $this->exeSQL( $query ) ) )
		{
			$links=array();
			while($row=$this->getAssoc($result))
			{
				$links[] = $row;
			}
			return $links;
		}
		else
		{
			return array();
		}

	}
	
	function getExplainContentByUrl($szUrl,$idExplain)
	{
		$query="
                    SELECT 
                        id,
                        szHeading,
                        szDescription,
                        szUrl,
                        szPicture,
                        szPictureTitle,
                        szMetaKeyWord,
                        szMetaDescription,
                        szPageName,
                        szMetaTitle,
                        szBackgroundColor,
                        iOrderBy,
                        idExplainData,
                        szLinkColor
                FROM 
                        ".__DBC_SCHEMATA_EXPLAIN_CONTENT__."	
                WHERE 
                    iActive='1'		
                AND
                    szUrl  = '".mysql_escape_custom($szUrl)."'	
                AND
                    idExplainData='".(int)$idExplain."'						
                ";
				//echo $query;	
		if( ( $result = $this->exeSQL( $query ) ) )
		{
			$links=array();
			while($row=$this->getAssoc($result))
			{
				$links[] = $row;
			}
			return $links;
		}
		else
		{
			return array();
		}

	}
	
	
	function getExplainContentSearchByKeyword($szKeyword,$iLanguage=0)
	{
		
		if($iLanguage>0)
		{
                    $query_and = "  AND d.iLanguage = '".(int)$iLanguage."' ";
                    
                    $kConfig=  new cConfig();
                    $langArr=$kConfig->getLanguageDetails('',$iLanguage);
                    if(!empty($langArr))
                        $szLanguageCode=$langArr[0]['szLanguageCode'];
		}
		require(__APP_PATH_ROOT__."/".__WORDPRESS_FOLDER__.'/wp-config.php');
                if($szLanguageCode=='')
                    $szLanguageCode=__LANGUAGE_ID_ENGLISH_CODE__;
                
		$query="
                    SELECT
                        wpp.ID,
                        wpp.post_title AS szHeading,
                        wpp.post_name,
                        wpp.post_content AS szDescription,
                        wp2l.post_title AS szPageHeading
                    FROM
                        ".__DBC_SCHEMATA_WP_POSTS__." AS wpp
                    INNER JOIN
                        ".__DBC_SCHEMATA_WP_POSTS__." AS wp2l
                    ON
                        wp2l.ID=wpp.post_parent        
                    INNER JOIN
                        ".__DBC_SCHEMATA_WP_ICL_TRANSLATIONS__." AS wpit
                    ON
                        wpit.element_id=wpp.ID
                    WHERE
                        wpit.language_code='".  mysql_escape_custom($szLanguageCode)."'
                    AND
                        wpit.element_type='post_page'
                    AND
                        wpp.post_status='publish'
                    AND
                    (
                        wpp.post_title LIKE '%".mysql_escape_custom($szKeyword)."%'
                    OR
                        wpp.post_content LIKE '%".mysql_escape_custom($szKeyword)."%'
                    )
                    AND
                        wpp.post_parent>'0'  
                       
                    ORDER BY
                        wpp.menu_order ASC
                ";
		//echo $query;	
		if( ( $result = $this->exeSQL( $query ) ) )
		{
			$links=array();
			$ctr=0;
			while($row=$this->getAssoc($result))
			{
                            if($row['szPageHeading']!=''){                           
                                $redir_da_url = get_post_meta($row['ID'],'_custom_permalink',true);
                                if(empty($redir_da_url))
                                {
                                   $redir_da_url = get_post_meta($row['ID'],'custom_permalink',true); 
                                }
                                if($redir_da_url!='')
                                {
                                    $redir_da_url= __MAIN_SITE_HOME_PAGE_URL__.'/'.$redir_da_url;
                                }
                                else
                                {
                                    $redir_da_url = get_page_link( $row['ID'] );
                                }
                    
				$links[$ctr] = $row;
                                $links[$ctr]['szLink'] = $redir_da_url;
                                $links[$ctr]['szDescription'] = replaceSearchMiniConstants($row['szDescription']);
				$ctr++;
                            }
			}
			
			$blogDetaisAry = array();
			$blogDetaisAry = $this->getAllKnowledgeCenterSearchByKeyword($szKeyword,$iLanguage);
			
			$ret_ary=array();
			if(!empty($blogDetaisAry) && !empty($links))
			{
				$ret_ary = array_merge($links,$blogDetaisAry) ;
			}
			else if(!empty($links))
			{
				$ret_ary = $links ;
			}
			else
			{
				$ret_ary = $blogDetaisAry ;
			}			
			
//			$seoPageDetaisAry = array();
//			$seoPageDetaisAry = $this->getAllSeoPagesSearchByKeyword($szKeyword,$iLanguage); 
//			
//			$final_ret_ary=array();
//			if(!empty($seoPageDetaisAry) && !empty($ret_ary))
//			{
//				$final_ret_ary = array_merge($ret_ary,$seoPageDetaisAry) ;
//			}
//			else if(!empty($ret_ary))
//			{
//				$final_ret_ary = $ret_ary ;
//			}
//			else
//			{
//				$final_ret_ary = $seoPageDetaisAry ;
//			}
			
			return $ret_ary;
		}
		else
		{
			return array();
		}
	}
	
	function getAllKnowledgeCenterSearchByKeyword($szKeyword,$iLanguage=false)
	{
		if($iLanguage>0)
		{
			$query_and = "  AND b.iLanguage = '".(int)$iLanguage."' ";
		}
		
		$query="
			SELECT 
				b.id,
				b.szHeading,
				b.szContent as szDescription,
				b.szLink as szUrl,
				b.szMetaKeyWord,
				b.szMetaDescription, 
				b.szMetaTitle, 
				b.iOrderBy
			FROM 
				".__DBC_SCHEMATA_BLOG__." AS b 
			WHERE 
				b.iActive='1'	 
			AND
			(
					b.szHeading  LIKE '%".mysql_escape_custom($szKeyword)."%'
				OR
					b.szContent LIKE '%".mysql_escape_custom($szKeyword)."%'
				OR
					b.szLink LIKE '%".mysql_escape_custom($szKeyword)."%'	 
			)	
			$query_and					
				";
		//echo $query;	
		if( ( $result = $this->exeSQL( $query ) ) )
		{
			$links=array();
			$ctr=0;
			while($row=$this->getAssoc($result))
			{
				$links[$ctr] = $row;
				$links[$ctr]['iKnowledgeCenter'] = 1 ;
				$ctr++;
				
			}
			return $links;
		}
		else
		{
			return array();
		}
	}
	
	function getAllSeoPagesSearchByKeyword($szKeyword,$iLanguage=false)
	{
		if($iLanguage>0)
		{
			$query_and = "  AND b.iLanguage = '".(int)$iLanguage."' ";
		}
		
		$query="
			SELECT 
				b.id,
				b.szPageHeading as szHeading,
				b.szContent as szDescription,
				b.szUrl,
				b.szMetaKeywords as szMetaKeyWord,
				b.szMetaDescription, 
				b.szMetaTitle 
			FROM 
				".__DBC_SCHEMATA_SEO_PAGE_DATA__." AS b 
			WHERE 
				b.iActive='1'	 
			AND
			(
					b.szPageHeading  LIKE '%".mysql_escape_custom($szKeyword)."%'
				OR
					b.szContent LIKE '%".mysql_escape_custom($szKeyword)."%'
				OR
					b.szUrl LIKE '%".mysql_escape_custom($szKeyword)."%'	 
			)	
			$query_and					
				";
		//echo $query;	
		if( ( $result = $this->exeSQL( $query ) ) )
		{
			$links=array();
			$ctr=0;
			while($row=$this->getAssoc($result))
			{
				$links[$ctr] = $row;
				$links[$ctr]['iSeoPage'] = 1 ;
				$ctr++;
				
			}
			return $links;
		}
		else
		{
			return array();
		}
	}
        
        function addUpdateSearchAidWords($data)
        {
            if(!empty($data))
            { 
                $this->set_szOriginStr(sanitize_all_html_input($data['szOriginStr']));
                $this->set_szOriginKey(sanitize_all_html_input($data['szOriginKey']));
                $this->set_idSearchAdwords(sanitize_all_html_input($data['idSearchAdwords']));
                
                //$this->set_szDestinationStr(sanitize_all_html_input($data['szDestinationStr'])); 
                //$this->set_szDestinationKey(sanitize_all_html_input($data['szDestinationKey']));
                 
                if($this->error==true)
                {
                    return false;
                }
                $this->szSearchUrl = $this->szOriginKey ;
                
                if($this->idSearchAdwords>0)
                {
                    $query = "
                        UPDATE
                            ".__DBC_SCHEMATA_SEARCH_AID_WORDS__."
                        SET	
                            szOriginStr = '".mysql_escape_custom(trim($this->szOriginStr))."', 
                            szDestinationStr = '".mysql_escape_custom(trim($this->szDestinationStr))."', 
                            szOriginKey = '".mysql_escape_custom(trim($this->szOriginKey))."', 
                            szDestinationKey = '".mysql_escape_custom(trim($this->szDestinationKey))."', 
                            szSearchUrl = '".mysql_escape_custom(trim($this->szSearchUrl))."', 
                            dtUpdatedOn = now()
                        WHERE
                            id = '".(int)$this->idSearchAdwords."' 
                    "; 			
                }
                else
                {
                    $query = "
                        INSERT INTO
                            ".__DBC_SCHEMATA_SEARCH_AID_WORDS__."
                        (
                            szOriginStr,
                            szDestinationStr,
                            szOriginKey,
                            szDestinationKey,
                            szSearchUrl, 
                            iActive,
                            dtCreatedOn
                        )
                        VALUES
                        ( 
                            '".mysql_escape_custom(trim($this->szOriginStr))."',
                            '".mysql_escape_custom(trim($this->szDestinationStr))."',
                            '".mysql_escape_custom(trim($this->szOriginKey))."',
                            '".mysql_escape_custom(trim($this->szDestinationKey))."',
                            '".mysql_escape_custom(trim($this->szSearchUrl))."',
                            '1',
                            now()
                        )
                    "; 
                }	
//                echo "<br><br> ".$query;
//                die;
                if($result = $this->exeSQL($query))
                {	 
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
        } 
        
        function getAllsearchAidWords($idSearchAdwords=false,$szSearchUrl=false)
	{
            if($idSearchAdwords>0)
            {
                $query_and = "  AND id = '".(int)$idSearchAdwords."' ";
            }
            if(!empty($szSearchUrl))
            {
                $query_and .= "  AND szSearchUrl = '".  mysql_escape_custom(trim($szSearchUrl))."' ";
            }
		
            $query="
                SELECT 
                    id,
                    szOriginStr,
                    szDestinationStr,
                    szOriginKey,
                    szDestinationKey,
                    szSearchUrl, 
                    iActive,
                    dtCreatedOn
                FROM 
                    ".__DBC_SCHEMATA_SEARCH_AID_WORDS__." 
                WHERE 
                    iActive='1'	 
                AND
                    isDeleted = '0'
                    $query_and		
                ORDER BY
                    szOriginStr ASC
            ";
            //echo "<br><br> ".$query;	
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                $ret_ary = array();
                $ctr=0;
                while($row=$this->getAssoc($result))
                {
                    $ret_ary[$ctr] = $row; 
                    $ctr++; 
                }
                return $ret_ary;
            }
            else
            {
                return array();
            }
	}
        
        function deleteSearchAidWords($idSearchAdwords)
        {
            if($idSearchAdwords>0)
            { 
                $query = "
                    UPDATE
                        ".__DBC_SCHEMATA_SEARCH_AID_WORDS__."
                    SET	 
                        isDeleted = '1',
                        dtUpdatedOn = now()
                    WHERE
                        id = '".(int)$idSearchAdwords."' 
                "; 		 
                if($result = $this->exeSQL($query))
                {	 
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
        }
        
        function addStandardShipperDetails($idLandingPage)
        {
            if($this->szShipperCompanyName=='')
            {
               $queryDel="
                    DELETE FROM   
                        ".__DBC_SCHEMATA_STANDARD_SHIPPERS__."
                    WHERE
                      idLandingPage='".(int)$idLandingPage."'  
                    ";
               $result=$this->exeSQL($queryDel);
               
               return true;
            }
            $querySel="
                SELECT
                    id
                FROM    
                    ".__DBC_SCHEMATA_STANDARD_SHIPPERS__."
                WHERE
                  idLandingPage='".(int)$idLandingPage."'  
                ";
            if($result=$this->exeSQL($querySel))
            {
                if($this->iNumRows>0)
                {
                    $query="
                        UPDATE
                            ".__DBC_SCHEMATA_STANDARD_SHIPPERS__."
                        SET
                            szShipperCompanyName='".mysql_escape_custom(trim($this->szShipperCompanyName))."',
                            szShipperFirstName='".mysql_escape_custom(trim($this->szShipperFirstName))."',
                            szShipperLastName='".mysql_escape_custom(trim($this->szShipperLastName))."',
                            szShipperEmail='".mysql_escape_custom(trim($this->szShipperEmail))."',
                            szShipperPhone='".mysql_escape_custom(trim($this->szShipperPhone))."',
                            szShipperAddress='".mysql_escape_custom(trim($this->szShipperAddress))."',
                            szShipperPostcode='".mysql_escape_custom(trim($this->szShipperPostcode))."',
                            szShipperCity='".mysql_escape_custom(trim($this->szShipperCity))."',
                            idShipperCountry='".mysql_escape_custom(trim($this->idShipperCountry))."',
                            idShipperDialCode='".mysql_escape_custom(trim($this->idShipperDialCode))."',
                            szShipperAddress_pickup='".mysql_escape_custom(trim($this->szShipperAddress_pickup))."',
                            szShipperPostcode_pickup='".mysql_escape_custom(trim($this->szShipperPostcode_pickup))."',
                            szShipperCity_pickup='".mysql_escape_custom(trim($this->szShipperCity_pickup))."',
                            idShipperCountry_pickup='".mysql_escape_custom(trim($this->idShipperCountry_pickup))."',
                            dtUpdated=NOW()
                        WHERE
                            idLandingPage='".(int)$idLandingPage."'
                        ";
                    $result = $this->exeSQL($query);
                   // echo $query;
                }
                else
                {    
                    $query="
                        INSERT INTO
                            ".__DBC_SCHEMATA_STANDARD_SHIPPERS__."
                         (
                            idLandingPage,
                            szShipperCompanyName,
                            szShipperFirstName,
                            szShipperLastName,
                            szShipperEmail,
                            szShipperPhone,
                            szShipperAddress,
                            szShipperPostcode,
                            szShipperCity,
                            idShipperCountry,
                            idShipperDialCode,
                            szShipperAddress_pickup,
                            szShipperPostcode_pickup,
                            szShipperCity_pickup,
                            idShipperCountry_pickup,
                            dtCreated
                         )
                            VALUES
                         (
                            '".(int)$idLandingPage."',
                            '".mysql_escape_custom(trim($this->szShipperCompanyName))."',
                            '".mysql_escape_custom(trim($this->szShipperFirstName))."',
                            '".mysql_escape_custom(trim($this->szShipperLastName))."',
                            '".mysql_escape_custom(trim($this->szShipperEmail))."',
                            '".mysql_escape_custom(trim($this->szShipperPhone))."',
                            '".mysql_escape_custom(trim($this->szShipperAddress))."',
                            '".mysql_escape_custom(trim($this->szShipperPostcode))."',
                            '".mysql_escape_custom(trim($this->szShipperCity))."',
                            '".mysql_escape_custom(trim($this->idShipperCountry))."',
                            '".mysql_escape_custom(trim($this->idShipperDialCode))."',
                            '".mysql_escape_custom(trim($this->szShipperAddress_pickup))."',
                            '".mysql_escape_custom(trim($this->szShipperPostcode_pickup))."',
                            '".mysql_escape_custom(trim($this->szShipperCity_pickup))."',
                            '".mysql_escape_custom(trim($this->idShipperCountry_pickup))."',
                            NOW()    
                         )
                        ";
                   // echo $query;
                        $result = $this->exeSQL($query);
                }
            }
            
        }
        
        function getStandardShipperDetails($idLandingPage)
        {
            $query="
                SELECT
                    idLandingPage,
                    szShipperCompanyName,
                    szShipperFirstName,
                    szShipperLastName,
                    szShipperEmail,
                    szShipperPhone,
                    szShipperAddress,
                    szShipperPostcode,
                    szShipperCity,
                    idShipperCountry,
                    idShipperDialCode,
                    szShipperAddress_pickup,
                    szShipperPostcode_pickup,
                    szShipperCity_pickup,
                    idShipperCountry_pickup
                FROM
                    ".__DBC_SCHEMATA_STANDARD_SHIPPERS__."
                WHERE
                    idLandingPage='".(int)$idLandingPage."'        
            ";
            
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                   $row=$this->getAssoc($result);
                   return $row;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                return array();
            } 
        }
        
        function getQuoteHandlerByPageID($idLandingPage,$iActive=false,$adminFlag=false)
        {
            if($idLandingPage>0)
            {
                if(!$adminFlag){
                    $landingPageDataArys = $this->getAllNewLandingPageData($idLandingPage);
                    $szAutomatedRfqResponseCode=$landingPageDataArys[0]['szAutomatedRfqResponseCode'];
                    $automatedRfqResponseAry =$this->getAutomatedRfqResponseList('',$szAutomatedRfqResponseCode);
                    $idAutomateRFQResponse=$automatedRfqResponseAry[0]['id'];
                }
                else
                {
                    $idAutomateRFQResponse=$idLandingPage;
                }
                if($iActive)
                {
                    $query_and = " AND iActive = '1' ";
                }
                $query="
                    SELECT
                        id,
                        idLandingPage,
                        idFileOwner,
                        iQuoteValidity,
                        szQuoteEmailSubject,
                        szQuoteEmailBody, 
                        iActive,
                        idCreatedBy, 
                        dtCreatedOn
                    FROM
                        ".__DBC_SCHEMATA_STANDARD_CARGO_HANDLING__."
                    WHERE
                        idLandingPage='".(int)$idAutomateRFQResponse."'
                    AND
                        isDeleted = '0'
                        $query_and
                    ORDER BY
                        id ASC
                ";
                //echo $query;
                if($result=$this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                       $row=$this->getAssoc($result);
                       return $row;
                    }
                    else
                    {
                        return array();
                    }
                }
                else
                {
                    return array();
                } 
            }
        }
        
        function getStandardQuotePriceByPageID($idLandingPage,$iActive=false,$id=0,$bCaluclation=false,$adminFlag=false)
        {
            if($idLandingPage>0)
            { 
                if(!$adminFlag){
                $landingPageDataArys = $this->getAllNewLandingPageData($idLandingPage);
                $szAutomatedRfqResponseCode=$landingPageDataArys[0]['szAutomatedRfqResponseCode'];
                
                $automatedRfqResponseAry =$this->getAutomatedRfqResponseList('',$szAutomatedRfqResponseCode);
                
                $idAutomateRFQResponse=$automatedRfqResponseAry[0]['id'];
                }
                else
                {
                    $idAutomateRFQResponse=$idLandingPage;
                }
                $queryWhere='';
                if((int)$id>0)
                {
                    $queryWhere='AND
                        id="'.(int)$id.'"
                            ';
                }
                $ctr=0;
                $query="
                    SELECT
                        id,
                        idLandingPage,
                        idForwarder,
                        szForwarderDispName,
                        szForwarderAlias,
                        iBookingType,
                        idCourierProvider,
                        szCourierProviderName,
                        idCourierProviderProduct,
                        szCourierProductName,
                        szTransitTime,
                        fHandlingFeePerBooking,
                        idHandlingCurrency,
                        szHandlingCurrencyName,
                        fHandlingCurrencyExchangeRate,
                        fHandlingMarkupPercentage,
                        fHandlingMinMarkupPrice,
                        idHandlingMinMarkupCurrency,
                        szHandlingMinMarkupCurrencyName,
                        fHandlingMinMarkupCurrencyExchangeRate,
                        iSequence,
                        idCreatedBy,
                        iActive, 
                        dtCreatedOn
                    FROM
                        ".__DBC_SCHEMATA_STANDARD_PRICING__."
                    WHERE
                        idLandingPage='".(int)$idAutomateRFQResponse."' 
                    AND
                        iActive = '1'
                    AND
                        isDeleted = '0'
                    $queryWhere    
                    ORDER BY
                        iSequence ASC, szForwarderDispName ASC, szForwarderAlias ASC
                ";
                //echo $query;
                if($result=$this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                       $ret_ary = array();
                       while($row=$this->getAssoc($result))
                       {
                           if($bCaluclation)
                           {
                               if($row['iBookingType']==2) //Courier
                               {
                                   $key = $row['idForwarder']."_".$row['idCourierProvider']."_".$row['idCourierProviderProduct'];
                                   $ret_ary['courier'][$key] = $row;
                               }
                               else
                               {
                                   //LCL
                                   $key = $row['idForwarder'];
                                   $ret_ary['lcl'][$key] = $row;
                               }
                           }
                           else
                           {
                               $ret_ary[$ctr] = $row;
                               $ctr++;
                           } 
                       }
                       return $ret_ary;
                    }
                    else
                    {
                        return array();
                    }
                }
                else
                {
                    return array();
                } 
            }
        }
        
        function addStandardQuotePricing($data)
        {
            if(!empty($data))
            { 
                $this->set_idLandingPage(sanitize_all_html_input($data['idLandingPage']));
                $this->set_idForwarder(sanitize_all_html_input($data['idForwarder']));
                $this->set_iBookingType(sanitize_all_html_input($data['iBookingType']));
                
                if($this->iBookingType==2) //courier
                {
                    $this->set_idCourierProvider(sanitize_all_html_input($data['idCourierProvider']));
                    $this->set_idCourierProviderProduct(sanitize_all_html_input($data['idCourierProviderProduct']));
                } 
                $this->set_szTransitTime(sanitize_all_html_input($data['szTransitTime']));  
                $this->set_idHandlingCurrency(sanitize_all_html_input($data['idHandlingCurrency']));   
                $this->set_fHandlingFeePerBooking(sanitize_all_html_input($data['fHandlingFeePerBooking']));  
                $this->set_fHandlingMarkupPercentage(sanitize_all_html_input($data['fHandlingMarkupPercentage']));  
                $this->set_idHandlingMinMarkupCurrency(sanitize_all_html_input($data['idHandlingMinMarkupCurrency']));  
                $this->set_fHandlingMinMarkupPrice(sanitize_all_html_input($data['fHandlingMinMarkupPrice']));  
                $this->set_idStandardQuotePricing(sanitize_all_html_input($data['idStandardQuotePricing']));  
                 
                if($this->error==true)
                {
                    return false;
                } 
                $kForwarder = new cForwarder();
                $kForwarder->load($this->idForwarder);
                $szForwarderDispName = $kForwarder->szDisplayName;
                $szForwarderAlias = $kForwarder->szForwarderAlies;
                
                $kWhsSearch = new cWHSSearch();
                if($this->idHandlingCurrency==1) //USD
                {
                    $szHandlingCurrencyName = 'USD';
                    $fHandlingCurrencyExchangeRate = 1;
                }
                else
                {
                    $currencyAry = array();
                    $currencyAry = $kWhsSearch->getCurrencyDetails($this->idHandlingCurrency); 
                    $fHandlingCurrencyExchangeRate = $currencyAry['fUsdValue'];
                    $szHandlingCurrencyName = $currencyAry['szCurrency'];
                }  
                
                $kWhsSearch = new cWHSSearch();
                if($this->idHandlingMinMarkupCurrency==1) //USD
                {
                    $szHandlingMinMarkupCurrencyName = 'USD';
                    $fHandlingMinMarkupCurrencyExchangeRate = 1;
                }
                else
                {
                    $currencyAry = array();
                    $currencyAry = $kWhsSearch->getCurrencyDetails($this->idHandlingMinMarkupCurrency); 
                    $fHandlingMinMarkupCurrencyExchangeRate = $currencyAry['fUsdValue'];
                    $szHandlingMinMarkupCurrencyName = $currencyAry['szCurrency'];
                } 
                $iSequence = $this->getMaxOrder($this->idLandingPage);
                $iSequence = $iSequence +1; 
                
                if($this->iBookingType==2) //courier
                {
                    $kCourierServices = new cCourierServices();
                    
                   $courierProviderArr =$kCourierServices->getCourierProviderList($this->idCourierProvider);
                   $szCourierProviderName=$courierProviderArr[0]['szName'];
                   
                   $courierProviderProductArr =$kCourierServices->getCourierProductByIdForwarderIdCourierProvider($this->idCourierProvider,$this->idForwarder,$this->idCourierProviderProduct);
                   $szCourierProductName=$courierProviderProductArr[0]['szName'];
                }
                                                
                $idAdmin = $_SESSION['admin_id']; 
                if($this->idStandardQuotePricing>0)
                {
                    $query="
                        UPDATE
                            ".__DBC_SCHEMATA_STANDARD_PRICING__."
                        SET
                            idForwarder = '".mysql_escape_custom(trim($this->idForwarder))."',
                            szForwarderDispName = '".mysql_escape_custom(trim($szForwarderDispName))."',
                            szForwarderAlias = '".mysql_escape_custom(trim($szForwarderAlias))."',
                            iBookingType = '".mysql_escape_custom(trim($this->iBookingType))."',
                            idCourierProvider = '".mysql_escape_custom(trim($this->idCourierProvider))."', 
                            szCourierProviderName = '".mysql_escape_custom(trim($szCourierProviderName))."',
                            idCourierProviderProduct = '".mysql_escape_custom(trim($this->idCourierProviderProduct))."',
                            szCourierProductName = '".mysql_escape_custom(trim($szCourierProductName))."', 
                            szTransitTime = '".mysql_escape_custom(trim($this->szTransitTime))."',
                            fHandlingFeePerBooking = '".mysql_escape_custom(trim($this->fHandlingFeePerBooking))."',
                            idHandlingCurrency = '".mysql_escape_custom(trim($this->idHandlingCurrency))."', 
                            szHandlingCurrencyName = '".mysql_escape_custom(trim($szHandlingCurrencyName))."',
                            fHandlingCurrencyExchangeRate = '".mysql_escape_custom(trim($fHandlingCurrencyExchangeRate))."',
                            fHandlingMarkupPercentage = '".mysql_escape_custom(trim($this->fHandlingMarkupPercentage))."',
                            fHandlingMinMarkupPrice = '".mysql_escape_custom(trim($this->fHandlingMinMarkupPrice))."',
                            idHandlingMinMarkupCurrency = '".mysql_escape_custom(trim($this->idHandlingMinMarkupCurrency))."',
                            szHandlingMinMarkupCurrencyName = '".mysql_escape_custom(trim($szHandlingMinMarkupCurrencyName))."',
                            fHandlingMinMarkupCurrencyExchangeRate = '".mysql_escape_custom(trim($fHandlingMinMarkupCurrencyExchangeRate))."', 
                            idUpdatedBy = '".$idAdmin."',
                            iActive = '1',
                            dtUpdatedOn = now()
                        WHERE
                            id = '".(int)$this->idStandardQuotePricing."'
                    ";
                }
                else
                {
                    $query = "
                        INSERT INTO
                            ".__DBC_SCHEMATA_STANDARD_PRICING__."
                        (
                            idLandingPage,
                            idForwarder,
                            szForwarderDispName,
                            szForwarderAlias,
                            iBookingType,
                            idCourierProvider,
                            szCourierProviderName,
                            idCourierProviderProduct,
                            szCourierProductName,
                            szTransitTime,
                            fHandlingFeePerBooking,
                            idHandlingCurrency,
                            szHandlingCurrencyName,
                            fHandlingCurrencyExchangeRate,
                            fHandlingMarkupPercentage,
                            fHandlingMinMarkupPrice,
                            idHandlingMinMarkupCurrency,
                            szHandlingMinMarkupCurrencyName,
                            fHandlingMinMarkupCurrencyExchangeRate,
                            iSequence,
                            idCreatedBy,
                            iActive, 
                            dtCreatedOn
                        )
                        VALUES
                        (
                            '".mysql_escape_custom($this->idLandingPage)."',
                            '".mysql_escape_custom(trim($this->idForwarder))."',
                            '".mysql_escape_custom(trim($szForwarderDispName))."',
                            '".mysql_escape_custom(trim($szForwarderAlias))."',
                            '".mysql_escape_custom(trim($this->iBookingType))."',
                            '".mysql_escape_custom(trim($this->idCourierProvider))."',
                            '".mysql_escape_custom(trim($szCourierProviderName))."',
                            '".mysql_escape_custom(trim($this->idCourierProviderProduct))."',
                            '".mysql_escape_custom(trim($szCourierProductName))."', 
                            '".mysql_escape_custom(trim($this->szTransitTime))."',
                            '".mysql_escape_custom(trim($this->fHandlingFeePerBooking))."',
                            '".mysql_escape_custom(trim($this->idHandlingCurrency))."', 
                            '".mysql_escape_custom(trim($szHandlingCurrencyName))."',
                            '".mysql_escape_custom(trim($fHandlingCurrencyExchangeRate))."',
                            '".mysql_escape_custom(trim($this->fHandlingMarkupPercentage))."',
                            '".mysql_escape_custom(trim($this->fHandlingMinMarkupPrice))."',
                            '".mysql_escape_custom(trim($this->idHandlingMinMarkupCurrency))."',
                            '".mysql_escape_custom(trim($szHandlingMinMarkupCurrencyName))."',
                            '".mysql_escape_custom(trim($fHandlingMinMarkupCurrencyExchangeRate))."',
                            '".(int)$iSequence."',
                            '".mysql_escape_custom($idAdmin)."', 
                            '1',
                            now()
                        )
                    ";
                } 
//                echo "<br><br>". $query;
//                die; 
                
                if(($result = $this->exeSQL($query)))
                {	  
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
        } 
        
        function getMaxOrder($idLandingPage)
        {
            if($idLandingPage>0)
            {
                $query="
                    SELECT
                        MAX(iSequence) as iMaxOrder
                    FROM
                        ".__DBC_SCHEMATA_STANDARD_PRICING__."
                    WHERE
                        idLandingPage='".(int)$idLandingPage."'
                    AND
                        isDeleted='0'
                ";  
                if($result=$this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                       $row=$this->getAssoc($result);
                       return $row['iMaxOrder'];
                    }
                    else
                    {
                        return array();
                    }
                }
                else
                {
                    return array();
                } 
            } 
        }
        
        function addStandardCargoHandling($data)
        {
            if(!empty($data))
            { 
                $this->set_idLandingPage(sanitize_all_html_input($data['idLandingPage']));
                $this->set_idFileOwner(sanitize_all_html_input($data['idFileOwner']));
                $this->set_iQuoteValidity(sanitize_all_html_input($data['iQuoteValidity']));
                $this->set_szQuoteEmailSubject(sanitize_all_html_input($data['szQuoteEmailSubject']));
                $this->set_szQuoteEmailBody($data['szQuoteEmailBody']);
                $this->set_iActive(sanitize_all_html_input($data['iActive']));  
                $this->set_idStandardCargoHandler(sanitize_all_html_input($data['idStandardCargoHandler']));  
                
                if($this->error==true)
                {
                    return false;
                } 
                
                $idAdmin = $_SESSION['admin_id']; 
                if($this->idStandardCargoHandler>0)
                {
                    $query="
                        UPDATE
                            ".__DBC_SCHEMATA_STANDARD_CARGO_HANDLING__."
                        SET
                            idFileOwner = '".mysql_escape_custom(trim($this->idFileOwner))."',
                            iQuoteValidity = '".mysql_escape_custom(trim($this->iQuoteValidity))."',
                            szQuoteEmailSubject = '".mysql_escape_custom(trim($this->szQuoteEmailSubject))."',
                            szQuoteEmailBody = '".mysql_escape_custom(trim($this->szQuoteEmailBody))."',
                            iActive = '".mysql_escape_custom(trim($this->iActive))."',
                            idUpdatedBy = '".$idAdmin."',
                            dtUpdatedOn = now()
                        WHERE
                            id = '".(int)$this->idStandardCargoHandler."'
                    ";
                }
                else
                {
                    $query = "
                        INSERT INTO
                            ".__DBC_SCHEMATA_STANDARD_CARGO_HANDLING__."
                        (
                            idLandingPage,
                            idFileOwner,
                            iQuoteValidity,
                            szQuoteEmailSubject,
                            szQuoteEmailBody, 
                            iActive,
                            idCreatedBy, 
                            dtCreatedOn
                        )
                        VALUES
                        (
                            '".mysql_escape_custom($this->idLandingPage)."',
                            '".mysql_escape_custom($this->idFileOwner)."',
                            '".mysql_escape_custom($this->iQuoteValidity)."',
                            '".mysql_escape_custom($this->szQuoteEmailSubject)."',
                            '".mysql_escape_custom($this->szQuoteEmailBody)."',
                            '".mysql_escape_custom($this->iActive)."',
                            '".mysql_escape_custom($idAdmin)."', 
                            now()
                        )
                    ";
                }  
//                echo "<br><br>". $query;
//                die;
                if(($result = $this->exeSQL($query)))
                {	  
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
        }
        
        
        function addPostMetaBackgroundColor($szBackgroundColor,$idPostPage)
        { 
            if($idPostPage>0)
            {
                $query = "
                    INSERT INTO
                        ".__DBC_SCHEMATA_WP_POST_META__."
                    (
                        post_id,
                        meta_key,
                        meta_value 
                    )
                    VALUES
                    (
                        '".mysql_escape_custom($idPostPage)."',    
                        '_fully_background_color',  
                        '".  mysql_escape_custom($szBackgroundColor)."' 
                    )
                ";  
               // echo "<br><br>". $query;
                //die;
                if(($result = $this->exeSQL($query)))
                {

                }
            } 
        }
        
        
        function addPostMetaDescription($szMetaDiscription,$idPostPage)
        { 
            if($idPostPage>0)
            {
                $query = "
                    INSERT INTO
                        ".__DBC_SCHEMATA_WP_POST_META__."
                    (
                        post_id,
                        meta_key,
                        meta_value 
                    )
                    VALUES
                    (
                        '".mysql_escape_custom($idPostPage)."',    
                        '_su_description', 
                        '".  mysql_escape_custom($szMetaDiscription)."' 
                    )
                ";  
               // echo "<br><br>". $query;
                //die;
                if(($result = $this->exeSQL($query)))
                {

                }
            } 
        }
        
        function addPostMetaTitle($szMetaTitle,$idPostPage)
        { 
            if($idPostPage>0)
            {
                $query = "
                    INSERT INTO
                        ".__DBC_SCHEMATA_WP_POST_META__."
                    (
                        post_id,
                        meta_key,
                        meta_value 
                    )
                    VALUES
                    (
                        '".mysql_escape_custom($idPostPage)."',    
                        '_su_title',   
                        '".  mysql_escape_custom($szMetaTitle)."' 
                    )
                ";  
               // echo "<br><br>". $query;
                //die;
                if(($result = $this->exeSQL($query)))
                {

                }
            } 
        }
        
        
        function addPostOGTitle($szOGTitle,$idPostPage)
        { 
            if($idPostPage>0)
            {
                $query = "
                    INSERT INTO
                        ".__DBC_SCHEMATA_WP_POST_META__."
                    (
                        post_id,
                        meta_key,
                        meta_value 
                    )
                    VALUES
                    (
                        '".mysql_escape_custom($idPostPage)."',    
                        '_su_og_title',   
                        '".  mysql_escape_custom($szOGTitle)."' 
                    )
                ";  
               // echo "<br><br>". $query;
                //die;
                if(($result = $this->exeSQL($query)))
                {

                }
            } 
        }
        
        function addPostOGDescription($szOGDescription,$idPostPage)
        { 
            if($idPostPage>0)
            {
                $query = "
                    INSERT INTO
                        ".__DBC_SCHEMATA_WP_POST_META__."
                    (
                        post_id,
                        meta_key,
                        meta_value 
                    )
                    VALUES
                    (
                        '".mysql_escape_custom($idPostPage)."',    
                        '_su_og_description',   
                        '".  mysql_escape_custom($szOGDescription)."' 
                    )
                ";  
               // echo "<br><br>". $query;
                //die;
                if(($result = $this->exeSQL($query)))
                {

                }
            } 
        }
        
        function addPostOGImageUrl($szOGImageUrl,$idPostPage)
        { 
            if($idPostPage>0)
            {
                $query = "
                    INSERT INTO
                        ".__DBC_SCHEMATA_WP_POST_META__."
                    (
                        post_id,
                        meta_key,
                        meta_value 
                    )
                    VALUES
                    (
                        '".mysql_escape_custom($idPostPage)."',    
                        '_su_og_image',   
                        '".mysql_escape_custom($szOGImageUrl)."' 
                    )
                ";  
               // echo "<br><br>". $query;
                //die;
                if(($result = $this->exeSQL($query)))
                {

                }
            } 
        }
        
        function addPostMetaKeyword($szMetaKeyword,$idPostPage)
        { 
            if($idPostPage>0)
            {
                $query = "
                    INSERT INTO
                        ".__DBC_SCHEMATA_WP_POST_META__."
                    (
                        post_id,
                        meta_key,
                        meta_value 
                    )
                    VALUES
                    (
                        '".mysql_escape_custom($idPostPage)."',    
                        '_su_keywords',    
                        '".  mysql_escape_custom($szMetaKeyword)."' 
                    )
                ";  
               // echo "<br><br>". $query;
                //die;
                if(($result = $this->exeSQL($query)))
                {

                }
            } 
        }
        
        function getWordpressExplainPageDataForMenu($idMainPost)
        {
            $query="
                SELECT
                    post_title,
                    post_name,
                    id,
                    post_date,
                    post_modified
                FROM
                    ".__DBC_SCHEMATA_WP_POSTS__."
                WHERE
                    post_status='publish'
                AND
                    post_type='page'
                AND
                    post_parent='".(int)$idMainPost."'
                ORDER BY
                    menu_order ASC
            ";
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    while($row=$this->getAssoc($result))
                    {
                        $resArr[]=$row;
                    }
                    return $resArr;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                return array();
            }
        }       
        
        function getMainMenuParentData($szLanguageCode,$sitemapFlag=false,$flag=false)
        {            
            $query="
                SELECT
                    wpp.ID,
                    wpp.post_title,
                    wpp.post_name
                FROM
                    ".__DBC_SCHEMATA_WP_POSTS__." AS wpp
                INNER JOIN
                    ".__DBC_SCHEMATA_WP_ICL_TRANSLATIONS__." AS wpit
                ON
                    wpit.element_id=wpp.ID
                WHERE
                    wpit.language_code='".  mysql_escape_custom($szLanguageCode)."'
                AND
                    wpit.element_type='post_page'
                AND
                    wpp.post_status='publish'
                AND
                    wpp.post_parent='0'
                ORDER BY
                    wpp.menu_order ASC
            ";
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    while($row=$this->getAssoc($result))
                    {
                        $querySub="
                            SELECT
                                meta_id
                            FROM
                                ".__DBC_SCHEMATA_WP_POST_META__."
                            WHERE
                                post_id='".(int)$row['ID']."'
                            AND
                                meta_key='iDoNotShowInLeftMenu'
                            AND
                                meta_value<>''
                        ";
                        if($result1=$this->exeSQL($querySub))
                        {
                            if($this->iNumRows<=0)
                            {
                                if($sitemapFlag)
                                {
                                    $dataFlag=false;
                                    $querySub="
                                        SELECT
                                            meta_id
                                        FROM
                                            ".__DBC_SCHEMATA_WP_POST_META__."
                                        WHERE
                                            post_id='".(int)$row['ID']."'
                                        AND
                                            meta_key='Article Type'
                                        AND
                                            LOWER(meta_value)='yes'
                                    ";
                                    if($result1=$this->exeSQL($querySub))
                                    {
                                        if($this->iNumRows>0)
                                        {
                                            $dataFlag=true;
                                        }
                                    }


                                    if($flag)
                                    {
                                        if($dataFlag){
                                        $resArr[]=$row;
                                        }
                                    }
                                    else
                                    {
                                        if(!$dataFlag){
                                        $resArr[]=$row;
                                        }
                                    }
                                }
                                else
                                {
                                    $resArr[]=$row;
                                }
                            }
                        }
                    }
                    return $resArr;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                return array();
            }
        }
        
        
        function moveUpDownStandardPricingData($idStandardPricing,$iSequence,$szFlag,$idCustomLandingPage)
        {
            if($szFlag=='up')
            {
                $iNewSequence=$iSequence-1;
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_STANDARD_PRICING__."
                    SET
                        iSequence='".(int)$iNewSequence."'
                    WHERE
                        id='".(int)$idStandardPricing."'
                    AND
                        idLandingPage='".(int)$idCustomLandingPage."'        
                ";
                //echo $query;
                $result=$this->exeSQL($query);
                
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_STANDARD_PRICING__."
                    SET
                        iSequence='".(int)$iSequence."'
                    WHERE
                        id<>'".(int)$idStandardPricing."'
                    AND
                        iSequence='".(int)$iNewSequence."'
                    AND
                        idLandingPage='".(int)$idCustomLandingPage."'        
                ";
               //echo $query;
                $result=$this->exeSQL($query);
            }
            else if($szFlag=='down')
            {
                $iNewSequence=$iSequence+1;
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_STANDARD_PRICING__."
                    SET
                        iSequence='".(int)$iNewSequence."'
                    WHERE
                        id='".(int)$idStandardPricing."'
                    AND
                        idLandingPage='".(int)$idCustomLandingPage."'        
                ";
                //echo $query;
                $result=$this->exeSQL($query);
                
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_STANDARD_PRICING__."
                    SET
                        iSequence='".(int)$iSequence."'
                    WHERE
                        id<>'".(int)$idStandardPricing."'
                    AND
                        iSequence='".(int)$iNewSequence."'
                    AND
                        idLandingPage='".(int)$idCustomLandingPage."'        
                ";
               //echo $query;
                $result=$this->exeSQL($query);
            }
        }
        
        function deleteStandardPricingData($idStandardPricing,$iSequence,$idCustomLandingPage)
        {
            $query="
                    UPDATE
                        ".__DBC_SCHEMATA_STANDARD_PRICING__."
                    SET
                        isDeleted='1'
                    WHERE
                        id='".(int)$idStandardPricing."'
                    AND
                        idLandingPage='".(int)$idCustomLandingPage."'        
                ";
                //echo $query;
                $result=$this->exeSQL($query);
                
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_STANDARD_PRICING__."
                    SET
                        iSequence=iSequence-1
                    WHERE
                        id<>'".(int)$idStandardPricing."'
                    AND
                        idLandingPage='".(int)$idCustomLandingPage."'
                    AND
                        iSequence >'".(int)$iSequence."'
                ";
               //echo $query;
                $result=$this->exeSQL($query);
        }
        
        function getSearchNotificationListing($idSearchNoti=0,$dataSearchNotification=array(),$iNotificationType=false)
        {
            $queryWhere='';        
            if((int)$idSearchNoti>0)
            {
                $queryWhere="AND id = '".(int)$idSearchNoti."'";      
            }
            
            if(!empty($dataSearchNotification))
            {
                if($iNotificationType ==1 || $iNotificationType ==3)
                {
                   $queryWhere .="AND idFromCountry = '0'";  
                }
                else
                {
                    if((int)$dataSearchNotification['idFromCountry']>0)
                    {
                        $queryWhere .="AND idFromCountry = '".(int)$dataSearchNotification['idFromCountry']."'"; 
                    }
                }
                
                if($iNotificationType == 2 || $iNotificationType ==3)
                {
                    $queryWhere .="AND idToCountry = '0'";     
                }
                else
                {
                    if((int)$dataSearchNotification['idToCountry']>0)
                    {
                        $queryWhere .="AND idToCountry = '".(int)$dataSearchNotification['idToCountry']."'"; 
                    }
                }
                
                if((int)$dataSearchNotification['iLanguage']>0)
                {
                    $queryWhere .="AND iLanguage = '".(int)$dataSearchNotification['iLanguage']."'"; 
                }
            }
            $query="
                SELECT
                    id,
                    idFromCountry,
                    szFromCountry,
                    idToCountry,
                    szToCountry,
                    szFromContains,
                    szToContains,
                    iLanguage,
                    szHeading,
                    szMessage,
                    idButtonType,
                    szButtonText,
                    szButtonUrl,
                    isDeleted,
                    dtCreated,
                    dtUpdated
                FROM
                    ".__DBC_SCHEMATA_SEARCH_NOTIFICATION__."
                WHERE
                    isDeleted='0'
                $queryWhere    
                ORDER BY
                    szFromCountry ASC,szFromContains ASC
                        
            ";
            //echo $query;
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    if(!empty($dataSearchNotification))
                    {
                        $ctr=0;
                        while($row=$this->getAssoc($result))
                        {
                            $addFlag=false;
                            if($row['szFromContains']=='*')
                            {
                                $addFlag=true;
                                $type=1;
                            }
                            else
                            {
                                $addFlag=false;
                                $pos = strpos(strtolower($dataSearchNotification['szOriginCountry']),strtolower($row['szFromContains']));
                                if($pos ===false) 
                                {
                                    /// Match Not Found                              
                                }
                                else
                                {
                                    $addFlag=true;
                                    if(strtolower($dataSearchNotification['szOriginPostCode'])==strtolower($row['szFromContains']))
                                    {
                                        $type=3;
                                    }
                                    else
                                    {
                                        $type=2;
                                    }
                                }
                            }
                            
                            if($addFlag)
                            {
                                if($row['szToContains']=='*')
                                {
                                    $addFlag=true;
                                    $type=1;
                                }
                                else
                                {
                                    $addFlag=false;
                                    $pos = strpos(strtolower($dataSearchNotification['szDestinationCountry']),strtolower($row['szToContains']));
                                    //echo $pos."pos";
                                    if($pos===false) 
                                    {
                                       /// Match Not Found
                                    }
                                    else
                                    {
                                        $addFlag=true;
                                        if(strtolower($dataSearchNotification['szDestinationPostCode'])==strtolower($row['szToContains']))
                                        {
                                            $type=3;
                                        }
                                        else
                                        {
                                            $type=2;
                                        }
                                    }
                                }
                            } 
                            if($addFlag)
                            { 
                                $resArr[$ctr]=$row;
                                $resArr[$ctr]['iType']=$type;
                                ++$ctr;
                            }
                        }  
                        return $resArr;
                    }
                    else
                    {    
                        while($row=$this->getAssoc($result))
                        {
                            $resArr[]=$row;
                        }
                        return $resArr;
                    }
                }
                else
                {
                        return array();
                }
            }
            else
            {
                return array();
            }
        }
        
        function deleteSearchNotification($idSearchNoti)
        {
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_SEARCH_NOTIFICATION__."
                SET
                    isDeleted='1'
                WHERE
                    id='".(int)$idSearchNoti."'
            ";
            $result=$this->exeSQL($query);
                    
        }
        
        function deletePrivateCustomerSearchNotification($idSearchNoti)
        {
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_SEARCH_NOTIFICATION_PRIVATE_CUSTOMER__."
                SET
                    isDeleted='1'
                WHERE
                    id='".(int)$idSearchNoti."'
            ";
            $result=$this->exeSQL($query);
                    
        }
        
        function addUpdateSearchNotificationData($data)
        {
            if(!empty($data))
            { 
                if(trim($data['idFromCountry'])=='All')
                {
                    $this->idFromCountry=0;
                }
                else
                {
                    $this->set_idFromCountry(sanitize_all_html_input(trim($data['idFromCountry'])));
                }
                if(trim($data['idToCountry'])=='All')
                {
                    $this->idToCountry=0;
                }
                else
                {
                    $this->set_idToCountry(sanitize_all_html_input(trim($data['idToCountry'])));
                }
                $this->set_idLanguage(sanitize_all_html_input(trim($data['iLanguage'])));
                $this->set_szToContains(sanitize_all_html_input(trim($data['szToContains'])));
                $this->set_szFromContains(sanitize_all_html_input(trim($data['szFromContains'])));
                $this->set_szPopupHeading(sanitize_all_html_input(trim($data['szHeading'])));  
                $this->set_szMessage(sanitize_specific_html_input(trim($data['szMessage'])));  
                
                if(count($data['idButtonType'])==0)
                {
                    $this->addError("ButtonDivId", "Atleast select one Button type.");
                }
                else
                {
                    if(in_array(__GO_TO_BUTTON__, $data['idButtonType']))
                    {
                        $this->set_szButtonText(sanitize_all_html_input(trim($data['szButtonText'])));  
                        $this->set_szButtonUrl(sanitize_all_html_input(trim($data['szButtonUrl']))); 
                    }
                }
                
                if($this->error==true)
                {
                    return false;
                } 
                
                
                if(count($data['idButtonType'])>0)
                {
                    $idButtonType=  implode(";", $data['idButtonType']);
                }
                $kConfig= new cConfig();
                if((int)$this->idFromCountry>0)
                {             
                    $kConfig->loadCountry($this->idFromCountry);
                    $szFromCountry=$kConfig->szCountryName;
                }
                else
                {
                    $szFromCountry="Any";
                }
                if((int)$this->idToCountry>0)
                {
                    $kConfig->loadCountry($this->idToCountry);
                    $szToCountry=$kConfig->szCountryName;
                }
                else
                {
                    $szToCountry="Any";
                }
                
                
                if((int)$data['idSearchNoti']>0)
                {
                    $query="
                        UPDATE
                            ".__DBC_SCHEMATA_SEARCH_NOTIFICATION__."
                        SET
                            idFromCountry='".(int)$this->idFromCountry."',
                            szFromCountry='".mysql_escape_custom($szFromCountry)."',
                            idToCountry='".(int)$this->idToCountry."',
                            szToCountry='".mysql_escape_custom($szToCountry)."',
                            szFromContains='".mysql_escape_custom($this->szFromContains)."',
                            szToContains='".mysql_escape_custom($this->szToContains)."',
                            iLanguage='".(int)$this->idLanguage."',
                            szHeading='".mysql_escape_custom($this->szPopupHeading)."',
                            szMessage='".mysql_escape_custom($this->szMessage)."',
                            idButtonType='".mysql_escape_custom($idButtonType)."',
                            szButtonText='".mysql_escape_custom($this->szButtonText)."',
                            szButtonUrl='".mysql_escape_custom($this->szButtonUrl)."',
                            dtUpdated=NOW()
                        WHERE
                            id ='".(int)$data['idSearchNoti']."'
                    "; 
                }
                else
                {
                    $query="
                        INSERT INTO
                            ".__DBC_SCHEMATA_SEARCH_NOTIFICATION__."
                        (
                            idFromCountry,
                            szFromCountry,
                            idToCountry,
                            szToCountry,
                            szFromContains,
                            szToContains,
                            iLanguage,
                            szHeading,
                            szMessage,
                            idButtonType,
                            szButtonText,
                            szButtonUrl,
                            dtCreated
                        )
                            VALUES
                        (
                            '".(int)$this->idFromCountry."',
                            '".mysql_escape_custom($szFromCountry)."',    
                            '".(int)$this->idToCountry."',
                            '".mysql_escape_custom($szToCountry)."',
                            '".mysql_escape_custom($this->szFromContains)."',
                            '".mysql_escape_custom($this->szToContains)."',
                            '".(int)$this->idLanguage."',
                            '".mysql_escape_custom($this->szPopupHeading)."',
                            '".mysql_escape_custom($this->szMessage)."',
                            '".mysql_escape_custom($idButtonType)."',
                            '".mysql_escape_custom($this->szButtonText)."',
                            '".mysql_escape_custom($this->szButtonUrl)."',
                            NOW()    

                        )
                    "; 
                    //echo $query;
                }
                
                               
                if($result=$this->exeSQL($query))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
        function addUpdateSearchNotificationDataPrivateCustomer($data)
        {
            if(!empty($data))
            {   
                $this->set_idLanguage(sanitize_all_html_input(trim($data['iLanguage']))); 
                $this->set_szPopupHeading(sanitize_all_html_input(trim($data['szHeading'])));  
                $this->set_szMessage(sanitize_specific_html_input(trim($data['szMessage'])));  
                
                if(count($data['idButtonType'])==0)
                {
                    $this->addError("ButtonDivId", "Atleast select one Button type.");
                }
                else
                { 
                    if(in_array(__CONTINUE_BUTTON__, $data['idButtonType']))
                    {
                        $this->set_szContinueButtonText(sanitize_all_html_input(trim($data['szContinueButtonText'])));   
                    }
                    if(in_array(__CONTINUE_BUTTON_2__, $data['idButtonType']))
                    {
                        $this->set_szContinue2ButtonText(sanitize_all_html_input(trim($data['szContinue2ButtonText'])));   
                    }
                    if(in_array(__CONTINUE_BUTTON_3__, $data['idButtonType']))
                    {
                        $this->set_szContinue3ButtonText(sanitize_all_html_input(trim($data['szContinue3ButtonText'])));   
                    }
                    
                    if(in_array(__GO_TO_RFQ_BUTTON__, $data['idButtonType']))
                    {
                        $this->set_szButtonTextRfq(sanitize_all_html_input(trim($data['szButtonTextRfq'])));   
                    }
                    if(in_array(__GO_TO_BUTTON__, $data['idButtonType']))
                    { 
                        $this->set_szButtonText(sanitize_all_html_input(trim($data['szButtonText'])));  
                        $this->set_szButtonUrl(sanitize_all_html_input(trim($data['szButtonUrl']))); 
                    }
                    
                }
                
                if($this->error==true)
                {
                    return false;
                }  
                
                if(count($data['idButtonType'])>0)
                {
                    $idButtonType=  implode(";", $data['idButtonType']);
                }
                $kConfig= new cConfig(); 
                if((int)$data['idSearchNotification']>0)
                {
                    $query="
                        UPDATE
                            ".__DBC_SCHEMATA_SEARCH_NOTIFICATION_PRIVATE_CUSTOMER__."
                        SET
                            iLanguage = '".(int)$this->idLanguage."',
                            szHeading = '".mysql_escape_custom($this->szPopupHeading)."',
                            szMessage = '".mysql_escape_custom($this->szMessage)."',
                            idButtonType = '".mysql_escape_custom($idButtonType)."',
                            szContinueButtonText = '".mysql_escape_custom(trim($this->szContinueButtonText))."',  
                            szButtonText = '".mysql_escape_custom($this->szButtonText)."',
                            szButtonTextRfq = '".mysql_escape_custom($this->szButtonTextRfq)."',
                            szButtonUrl = '".mysql_escape_custom($this->szButtonUrl)."',
                            dtUpdated=NOW()
                        WHERE
                            id ='".(int)$data['idSearchNotification']."'
                    ";
                }
                else
                {
                    $query="
                        INSERT INTO
                            ".__DBC_SCHEMATA_SEARCH_NOTIFICATION_PRIVATE_CUSTOMER__."
                        (
                            iLanguage,
                            szHeading,
                            szMessage,
                            idButtonType,
                            szContinueButtonText, 
                            szButtonText,
                            szButtonTextRfq,
                            szButtonUrl,
                            dtCreated
                        )
                        VALUES
                        (
                            '".(int)$this->idLanguage."',
                            '".mysql_escape_custom($this->szPopupHeading)."',
                            '".mysql_escape_custom($this->szMessage)."',
                            '".mysql_escape_custom($idButtonType)."',
                            '".mysql_escape_custom($this->szContinueButtonText)."', 
                            '".mysql_escape_custom($this->szButtonText)."',
                            '".mysql_escape_custom($this->szButtonTextRfq)."', 
                            '".mysql_escape_custom($this->szButtonUrl)."',
                            NOW()     
                        )
                    ";  
                }
//                echo $query; 
//                die;
                
                if($result=$this->exeSQL($query))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
        
        function getSearchNotificationListingPrivateCustomer($idSearchNotification=0,$dataSearchNotification=array(),$iNotificationType=false)
        {
            $queryWhere='';        
            if((int)$idSearchNotification>0)
            {
                $queryWhere="AND id = '".(int)$idSearchNotification."'";      
            }
            
            if(!empty($dataSearchNotification))
            {  
                if((int)$dataSearchNotification['iLanguage']>0)
                {
                    $queryWhere .="AND iLanguage = '".(int)$dataSearchNotification['iLanguage']."'"; 
                }
            }
            $query="
                SELECT
                    id, 
                    iLanguage,
                    szHeading,
                    szMessage,
                    idButtonType,
                    szContinueButtonText, 
                    szButtonTextRfq,
                    szButtonText,
                    szButtonUrl,
                    isDeleted,
                    dtCreated,
                    dtUpdated
                FROM
                    ".__DBC_SCHEMATA_SEARCH_NOTIFICATION_PRIVATE_CUSTOMER__."
                WHERE
                    isDeleted='0'
                $queryWhere    
                ORDER BY
                    iLanguage ASC 
            ";
            //echo $query;
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    if(!empty($dataSearchNotification))
                    {
                        $ctr=0;
                        while($row=$this->getAssoc($result))
                        { 
                            $resArr[$ctr]=$row; 
                            $ctr++; 
                        }  
                        return $resArr;
                    }
                    else
                    {    
                        while($row=$this->getAssoc($result))
                        {
                            $resArr[]=$row;
                        }
                        return $resArr;
                    }
                }
                else
                {
                        return array();
                }
            }
            else
            {
                return array();
            }
        }
        
        function getLanguageIdOfWordpress($szLanguageCode)
        {
            $query="
                SELECT
                    term_id
                FROM
                    ".__DBC_SCHEMATA_WP_TERMS__."
                WHERE
                    slug='".mysql_escape_custom($szLanguageCode)."'
            ";
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $row=$this->getAssoc($result);
                    
                    return $row['term_id'];
                }
                else
                {
                    return 0;
                }
            }else
            {
                return 0;
            }
        }
        
        
        
        function checkWordPressUrl($szURLArr)
        {
            $checkOldFlag=false;
        	//$languageArr=array("da");
                
                $kConfig = new cConfig();

                $currentLangArr=$kConfig->getLanguageDetails('','','','',true);
                $languageArr=$currentLangArr;
        	$checkLanguageFlag=false;
            for($i=0;$i<count($szURLArr);++$i)
            {
                if(trim($szURLArr[$i])!='' && !in_array(trim($szURLArr[$i]),$languageArr))
                {
                    $newArr[]=$szURLArr[$i];
                }
                if($szURLArr[$i]=='old')
                {
                    $checkOldFlag=true;
                }

                if(!empty($languageArr) && in_array(trim($szURLArr[$i]),$languageArr))
                {
                	$checkLanguageFlag=true;
                }
            }
            if($checkOldFlag)
            {
                return false;
            }
            
            
            if(!empty($newArr))
            {
                $szURLStr=implode("/",$newArr);
                if(!$checkLanguageFlag)
                {
                    $szURLStrNew=$szURLStr;
                    $szURLStr=$szURLStr."/";
                }
                else
                {
                    $szURLStrNew="/".$szURLStr;
                    $szURLStr="/".$szURLStr."/";
                }
            }
            if($checkOldFlag)
            {
                return false;
            }
            
            if(count($newArr)>0){
                $queryWHERE="WHERE (";
                for($j=0;$j<count($newArr);++$j)
                {
                    if($j==0)
                    {
                        $queryWHERE .="szPageUrl ='".  mysql_escape_custom($newArr[$j])."'";
                    }

                    else
                    {
                        $queryWHERE .=" OR szPageUrl ='".  mysql_escape_custom($newArr[$j])."'";
                    }
                }
                $queryWHERE .=" OR szPageUrl ='".  mysql_escape_custom($szURLStr)."'";
                
                $queryWHERE .=" OR szPageUrl ='".  mysql_escape_custom($szURLStrNew)."'";
                
                $queryWHERE .=")";
                
                $query="
                SELECT
                    id
                FROM
                   ". __DBC_SCHEMATA_WORDPRESS_PAGE_URL__."
                $queryWHERE       
               ";
                
                if($result=$this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        return true;
                    }
                }
            
            }            
            return false;
                    
        }
        
        function getAllImagePost()
        {
            $query="
                SELECT
                    post_id,
                    meta_value
                FROM    
                    ".__DBC_SCHEMATA_WP_POST_META__."
                WHERE   
                    meta_key='_wp_attached_file'
            ";
            //echo $query."<br /><br />";
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                while($row=$this->getAssoc($result))
                {
                    $resArr[]=$row;
                }
                                
                return $resArr;
            }
            else
            {
                    return 0;
            }
        }
        
        function getAllComparePostContent($data)
        {
            //print_r($data);
            $query="
                SELECT
                    ID
                FROM    
                    ".__DBC_SCHEMATA_WP_POSTS__."
                WHERE   
                    post_content LIKE '%".mysql_escape_custom($data['meta_value'])."%'
            ";
           // echo $query."<br /><br />";
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                while($row=$this->getAssoc($result))
                {
                    $resArr[]=$row;
                }
                  //print_r($resArr);     
                   //echo "<br /><br />";
                if(!empty($resArr))
                {
                    foreach($resArr as $resArrs)
                    {
                        $query = "
                            INSERT INTO
                                ".__DBC_SCHEMATA_WP_POST_META__."
                            (
                                post_id,
                                meta_key,
                                meta_value 
                            )
                            VALUES
                            (
                                '".mysql_escape_custom($resArrs['ID'])."',	
                                '_thumbnail_id',	
                                '".$data['post_id']."' 
                            )
                        ";  
                       // echo "<br><br>". $query;
                        //die;
                        if(($result = $this->exeSQL($query)))
                        {

                        }
                    }
                }
                
            }
            else
            {
                    return 0;
            }
        }
        
        
        function addPostPageURLNewWordPress($post_name,$idPostPage,$languageCode)
        {
           $languageCode=$languageCode."/";
           $resultVar = substr($post_name, 0, 3);           
           if($languageCode==$resultVar)
           {
               $post_name=substr_replace($post_name,"/",0,3);
           }          
            
            $insertFlag=true;
            $query="
                SELECT
                    id
                FROM
                   ". __DBC_SCHEMATA_WORDPRESS_PAGE_URL__."
                WHERE
                    idPost='".(int)$idPostPage."'
               ";
            //echo $query;
            
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $insertFlag=false;
                }
            }
            
            if($insertFlag)
            {
                $query="
                    INSERT INTO
                        ".__DBC_SCHEMATA_WORDPRESS_PAGE_URL__."
                    (
                        idPost,
                        szPageUrl,
                        dtCreated
                    )
                        VALUES
                    (
                        '".(int)$idPostPage."',
                        '".mysql_escape_custom($post_name)."',
                        NOW()     
                    )
                ";
            }
            else
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_WORDPRESS_PAGE_URL__."
                    SET        
                        szPageUrl='".mysql_escape_custom($post_name)."'
                    WHERE
                        idPost='".(int)$idPostPage."'         
                ";
            }
           //echo $query;
            if(($result = $this->exeSQL($query)))
            {

            }
            //die();
        }
        
        
        function getTitleDescriptionForImage($szHeaderImage)
        { 
            
            $query = "
                SELECT
                    szPictureDescription,
                    szPictureTitle                    
                FROM
                    ".__DBC_SCHEMATA_EXPLAIN_DATA__."	
                WHERE
                    szHeaderImage='".mysql_escape_custom($szHeaderImage)."'
                ORDER BY 
                    iOrderBy ASC
            ";
           //echo $query;
            if($result=$this->exeSQL($query))
            {
                $links = array();
                while($row=$this->getAssoc($result))
                {
                    $links[] = $row;
                }
                return $links;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
        
        function updateContentSubPage()
        { 
           
            $query = "
                SELECT
                    szSubTitle,
                    szLink
                FROM
                    ".__DBC_SCHEMATA_EXPLAIN_DATA__."	
                WHERE
                    iActive = '1'
            ";
           
            if($result=$this->exeSQL($query))
            {
                $links = array();
                while($row=$this->getAssoc($result))
                {
                    $queryWp="
                        UPDATE
                            ".__DBC_SCHEMATA_WP_POSTS__."
                        SET
                            post_content='".  mysql_escape_custom($row['szSubTitle'])."'
                        WHERE
                            post_name='".mysql_escape_custom($row['szLink'])."'
                    ";
                    $result1=$this->exeSQL($queryWp);
                    //echo $queryWp."<br />";
                }
                return $links;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
        
        
        function getBButtonClickDataHoldingPages($idLandingPage,$flag=false)
        {
            $queryWhere='';
            if($flag)
            {
                $queryWhere ="AND DATE(dtSignup)='".  mysql_escape_custom(date('Y-m-d'))."'";
            }
            else
            {
                $queryWhere ="AND DATE(dtSignup)<'".  mysql_escape_custom(date('Y-m-d'))."'";
            }
            $query="
                SELECT 
                    count(cb.id) AS iTotal
                FROM 
                    ".__DBC_SCHEMATA_SIGNUPS_DATA__." cb 
                WHERE 
                    cb.idLandingPage = '".(int)$idLandingPage."'
                $queryWhere        
                ";
            //echo $query;
                if($result=$this->exeSQL($query))
                {
                    $row=$this->getAssoc($result);
                    return $row['iTotal'];
                }
        }
        
        
        function getBButtonClickData($idLandingPage,$flag=false)
        {
            $queryWhere='';
            if($flag)
            {
                $queryWhere ="AND DATE(dtDateTime)='".  mysql_escape_custom(date('Y-m-d'))."'";
            }
            else
            {
                $queryWhere ="AND DATE(dtDateTime)<'".  mysql_escape_custom(date('Y-m-d'))."'";
            }
            $query="
                SELECT 
                    count(cb.id) AS iTotal
                FROM 
                    ".__DBC_SCHEMATA_COMPARE_BOOKING__." cb 
                WHERE 
                    cb.idLandingPage = '".(int)$idLandingPage."' 
                AND 
                    iPageFlag='1'
                $queryWhere    
                ";
            //echo $query;
                if($result=$this->exeSQL($query))
                {
                    $row=$this->getAssoc($result);
                    return $row['iTotal'];
                }
        }
        
        function updateiNumButtonClicked($idLandingPage,$iNumButtonClicked)
        {
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_CUSTOM_LANDING_PAGE__."
                SET
                    iNumButtonClicked='".  mysql_escape_custom($iNumButtonClicked)."'
                WHERE
                    id='".mysql_escape_custom($idLandingPage)."'
            ";
            $result1=$this->exeSQL($query);
        }
        
        function getLandingPageType($idLanguage)
        {           
            $query="
                SELECT
                    iHoldingPage
                FROM
                    ".__DBC_SCHEMATA_CUSTOM_LANDING_PAGE__."
                WHERE 
                    iActive = '1'
                AND 
                    iDefault = '1'
                AND 
                    iLanguage = '".(int)$idLanguage."'    
            ";
            //echo 
            if($result=$this->exeSQL($query))
            {
                $row=$this->getAssoc($result);
                return $row['iHoldingPage'];
            }
        }
        
        
        function getMetaTitle($szTtitle,$idLanguage)
        {
            if($szTtitle=='__REQUIREMENT_PAGE_META_TITLE__')
            {
                $keywords='__REQUIREMENT_PAGE_META_KEYWORDS__';
                $desc='__REQUIREMENT_PAGE_META_DESCRIPTION__';
                
            }
            else if($szTtitle=='__SEARCH_PAGE_META_TITLE__')
            {
                $keywords='__SEARCH_PAGE_META_KEYWORDS__';
                $desc='__SEARCH_PAGE_META_DESCRIPTION__';
                
            }
            else if($szTtitle=='__CREATE_ACCOUNT_PAGE_META_TITLE__')
            {
                $keywords='__CREATE_ACCOUNT_PAGE_META_KEYWORDS__';
                $desc='__CREATE_ACCOUNT_PAGE_META_DESCRIPTION__';
                
            }
            else if($szTtitle=='__FORWARDER_SIGNUP_PAGE_META_TITLE__')
            {
                $keywords='__FORWARDER_SIGNUP_PAGE_META_KEYWORDS__';
                $desc='__FORWARDER_SIGNUP_PAGE_META_DESCRIPTION__';
                
            }
            else if($szTtitle=='__PARTNER_PROGRAM_PAGE_META_TITLE__')
            {
                $keywords='__PARTNER_PROGRAM_PAGE_META_KEYWORDS__';
                $desc='__PARTNER_PROGRAM_PAGE_META_DESCRIPTION__';
                
            }else if($szTtitle=='__RSS_FEED_PAGE_META_TITLE__')
            {
                $keywords='__RSS_FEED_PAGE_META_KEYWORDS__';
                $desc='__RSS_FEED_PAGE_META_DESCRIPTION__';
                
            }else if($szTtitle=='__TERMS_CONDITION_PAGE_META_TITLE__')
            {
                $keywords='__TERMS_CONDITION_PAGE_META_KEYWORDS__';
                $desc='__TERMS_CONDITION_PAGE_META_DESCRIPTION__';
                
            }else if($szTtitle=='__PRIVACY_NOTICE_PAGE_META_TITLE__')
            {
                $keywords='__PRIVACY_NOTICE_PAGE_META_KEYWORDS__';
                $desc='__PRIVACY_NOTICE_PAGE_META_DESCRIPTION__';
                
            }else if($szTtitle=='__BOOKING_CONFIRMATION_META_TITLE__')
            {
                $keywords='__BOOKING_CONFIRMATION_META_KEYWORDS__';
                $desc='__BOOKING_CONFIRMATION_META_DESCRIPTION__';
                
            }else if($szTtitle=='__BOOKING_RECEIPT_META_TITLE__')
            {
                $keywords='__BOOKING_RECEIPT_META_KEYWORDS__';
                $desc='__BOOKING_RECEIPT_META_DESCRIPTION__';
                
            }else if($szTtitle=='__BOOKING_DETAILS_META_TITLE__')
            {
                $keywords='__BOOKING_DETAILS_META_KEYWORDS__';
                $desc='__BOOKING_DETAILS_META_DESCRIPTION__';
                
            }else if($szTtitle=='__CARGO_READYNESS_META_TITLE__')
            {
                $keywords='__CARGO_READYNESS_META_KEYWORDS__';
                $desc='__CARGO_READYNESS_META_DESCRIPTION__';
                
            }else if($szTtitle=='__DELETE_ACCOUNT_META_TITLE__')
            {
                $keywords='__DELETE_ACCOUNT_META_KEYWORDS__';
                $desc='__DELETE_ACCOUNT_META_DESCRIPTION__';
                
            }else if($szTtitle=='__MY_ACCOUNT_META_TITLE__')
            {
                $keywords='__MY_ACCOUNT_META_KEYWORDS__';
                $desc='__MY_ACCOUNT_META_DESCRIPTION__';
                
            }else if($szTtitle=='__MY_BOOKING_META_TITLE__')
            {
                $keywords='__MY_BOOKING_META_KEYWORDS__';
                $desc='__MY_BOOKING_META_DESCRIPTION__';
                
            }else if($szTtitle=='__PRIVACY_META_TITLE__')
            {
                $keywords='__PRIVACY_META_KEYWORDS__';
                $desc='__PRIVACY_META_DESCRIPTION__';
                
            }else if($szTtitle=='__REGISTER_SHIPPER_CONSIGNEE_META_TITLE__')
            {
                $keywords='__REGISTER_SHIPPER_CONSIGNEE_META_KEYWORDS__';
                $desc='__REGISTER_SHIPPER_CONSIGNEE_META_DESCRIPTION__';
                
            }else if($szTtitle=='__SERVICES_PAGE_META_TITLE__')
            {
                $keywords='__SERVICES_PAGE_META_KEYWORDS__';
                $desc='__SERVICES_PAGE_META_DESCRIPTION__';
                
            }
            else if($szTtitle=='__SITE_MAP_META_TITLE__')
            {
                $keywords='__SITE_MAP_PAGE_META_KEYWORDS__';
                $desc='__SITE_MAP_PAGE_META_DESCRIPTION__';
                
            }
            else if($szTtitle=='__USER_MULTI_ACCESS_META_TITLE__')
            {
                $keywords='__USER_MULTI_ACCESS_META_KEYWORDS__';
                $desc='__USER_MULTI_ACCESS_META_DESCRIPTION__';
                
            }else if($szTtitle=='__LANDING_PAGE_META_TITLE__')
            {
                $keywords='__LANDING_PAGE_META_KEYWORDS__';
                $desc='__LANDING_PAGE_META_DESCRIPTION__';
                
            }else if($szTtitle=='__BOOKING_GET_QUOTE_META_TITLE__')
            {
                $keywords='__BOOKING_GET_QUOTE_META_KEYWORDS__';
                $desc='__BOOKING_GET_QUOTE_META_DESCRIPTION__';
                
            }else if($szTtitle=='__BOOKING_GET_RATE_META_TITLE__')
            {
                $keywords='__BOOKING_GET_RATE_META_KEYWORDS__';
                $desc='__BOOKING_GET_RATE_META_DESCRIPTION__';
                
            }else if($szTtitle=='__BOOKING_QUOTE_THANKS_PAGE_META_TITLE__')
            {
                $keywords='__BOOKING_QUOTE_THANKS_PAGE__META_KEYWORDS__';
                $desc='__BOOKING_QUOTE_THANKS_PAGE__META_DESCRIPTION__';
                
            }else if($szTtitle=='__COOKIES_META_TITLE__')
            {
                $keywords='__COOKIES_META_KEYWORDS__';
                $desc='__COOKIES_META_DESCRIPTION__';
            }            
            else if($szTtitle=='__BOOKING_QUOTE_SERVICE_PAGE_META_TITLE__')
            {
                $keywords='';
                $desc='';
            }
            
            $query="
                SELECT
                    id,
                    szValue,
                    szKey
                FROM
                    ".__DBC_SCHEMATA_LAUGUAGE_META_TAGS__."
                WHERE
                    szKey IN ('".$szTtitle."','".$keywords."','".$desc."')
                AND
                   idLanguage ='".(int)$idLanguage."'
                ";
            if($result=$this->exeSQL($query))
            {
                while($row=$this->getAssoc($result))
                {
                    if($row['szKey']==$szTtitle)
                    {
                        $resArr[0]['id']=$row['id'];
                        $resArr[0]['Title']=$row['szValue'];
                        
                    }
                    
                    if($row['szKey']==$keywords)
                    {
                        $resArr[1]['id']=$row['id'];
                        $resArr[1]['Keywords']=$row['szValue'];
                    }                    
                    
                    if($row['szKey']==$desc)
                    {
                        $resArr[2]['id']=$row['id'];
                        $resArr[2]['Description']=$row['szValue'];
                    }  
                }
                
                return $resArr;
            }
        }
        
        function updateLanguageConfigurationMetaData($data,$idLanguage,$idLangConfigType)
        {
            if(!empty($data['Title']))
            {
                $dataTitle=$data['Title'];
                if(!empty($dataTitle))
                {
                    foreach($dataTitle as $key=>$value)
                    {
                        $this->set_szTitle(sanitize_all_html_input(trim($value)),$key);
                        $this->idTitle=$key;
                    }
                }
            }
            
            if(!empty($data['Description']))
            {
                $dataDescription=$data['Description'];
                if(!empty($dataDescription))
                {
                    foreach($dataDescription as $key=>$value)
                    {
                        $this->set_szDesc(sanitize_all_html_input(trim($value)),$key);
                        $this->idDesc=$key;
                    }
                }
            }
            

            if(!empty($data['Keywords']))
            {
                $dataKeyword=$data['Keywords'];
                if(!empty($dataKeyword))
                {
                    foreach($dataKeyword as $key=>$value)
                    {
                        $this->set_szKey(sanitize_all_html_input(trim($value)),$key);
                        $this->idKeyword=$key;
                    }
                }
            }
            
            if($this->error==true)
            {
                return false;
            } 
            
            $updateTitle="
                UPDATE
                    ".__DBC_SCHEMATA_LAUGUAGE_META_TAGS__."
                SET
                    szValue='".  mysql_escape_custom($this->szTitle)."'
                WHERE
                    id='".(int)$this->idTitle."'
            ";
            $result=$this->exeSQL($updateTitle);
            
            $updateKey="
                UPDATE
                    ".__DBC_SCHEMATA_LAUGUAGE_META_TAGS__."
                SET
                    szValue='".  mysql_escape_custom($this->szKey)."'
                WHERE
                    id='".(int)$this->idKeyword."'
            ";
            $result=$this->exeSQL($updateKey);
            
            
            $updateDesc="
                UPDATE
                    ".__DBC_SCHEMATA_LAUGUAGE_META_TAGS__."
                SET
                    szValue='".  mysql_escape_custom($this->szDesc)."'
                WHERE
                    id='".(int)$this->idDesc."'
            ";
            $result=$this->exeSQL($updateDesc);
        }
            
        function isSearchNotificationExists($postSearchAry,$bOnlyPrivate=false,$bOnlyPublic=false)
        {
            $kExplain = new cExplain();
            if(!empty($postSearchAry))
            {
                $iLanguage = getLanguageId();
                
                $dataSearchNotification = array(); 
                $dataSearchNotification['idFromCountry'] = $postSearchAry['idOriginCountry'];
                $dataSearchNotification['idToCountry'] = $postSearchAry['idDestinationCountry']; 
                $dataSearchNotification['szOriginCountry'] = $postSearchAry['szOriginCountry'];
                $dataSearchNotification['szDestinationCountry'] = $postSearchAry['szDestinationCountry']; 
                $dataSearchNotification['szOriginCity'] = $postSearchAry['szOriginCity'];
                $dataSearchNotification['szDestinationCity'] = $postSearchAry['szDestinationCity']; 
                $dataSearchNotification['szOriginPostCode'] = $postSearchAry['szOriginPostCode'];
                $dataSearchNotification['szDestinationPostCode'] = $postSearchAry['szDestinationPostCode'];
                $dataSearchNotification['iLanguage'] = $iLanguage;
                   
                $searchNotificationArr = array();
                //getting data for defined countries 
                $searchNotificationArr=$kExplain->getSearchNotificationListing('0',$dataSearchNotification);
                if(empty($searchNotificationArr))
                {
                    //getting data for any to define country
                    $searchNotificationArr=$kExplain->getSearchNotificationListing('0',$dataSearchNotification,1); 
                    if(empty($searchNotificationArr))
                    {
                        //getting data for define to any country
                        $searchNotificationArr=$kExplain->getSearchNotificationListing('0',$dataSearchNotification,2); 
                        if(empty($searchNotificationArr))
                        {
                             //getting data for any to any country
                            $searchNotificationArr=$kExplain->getSearchNotificationListing('0',$dataSearchNotification,3); 
                        }
                    }
                }   
            } 
            if((int)$postSearchAry['iDoNotShowPrivatePopForVogaPage']==0){
                if($_SESSION['user_id']>0)
                { 
                    $kUser = new cUser();
                    $kUser->getUserDetails($_SESSION['user_id']); 
                    $iPrivateShipping = $kUser->iPrivate; 
                }

                $searchNotificationPrivateCustomerArr = array();
                if($iPrivateShipping==1)
                {
                    $privateNotificationSearchAry = array();
                    $privateNotificationSearchAry['iLanguage'] = $iLanguage;

                    $searchNotificationPrivateCustomerArr = $kExplain->getSearchNotificationListingPrivateCustomer(false,$privateNotificationSearchAry); 
                }
            }
            if($bOnlyPrivate)
            {
                return $searchNotificationPrivateCustomerArr[0];
            }
            else if(!empty($searchNotificationArr))
            { 
                $searchNotificationArr = sortArray($searchNotificationArr,'iType',true);
                $searchNotificationArr = $searchNotificationArr[0];

                if(!empty($searchNotificationPrivateCustomerArr))
                {
                    $searchNotificationArr['iPrivateNotificationAvailable'] = 1;
                }
                return $searchNotificationArr; 
            }
            else if($iPrivateShipping==1 && !$bOnlyPublic)
            {
                return $searchNotificationPrivateCustomerArr[0];
            }
        }
        
        function copyNewLandingPage($keyStr,$valuesStr)
	{
		
			
                $iMaxOrder = $this->getMaxOrderNewLandingPageData();

                $query = "
                        INSERT INTO 
                                ".__DBC_SCHEMATA_CUSTOM_LANDING_PAGE__."
                        (
                            $keyStr,
                            iOrder    
                        ) 
                        VALUES 
                        (
                            $valuesStr,
                            '6'    
                        )
                "; 
//			echo "Query: <br>".$query;
//                        die;
                if( ( $result = $this->exeSQL( $query ) ) )
                {
                    $idLandingPage = $this->iLastInsertID;
                    
                    $updateOrder="
                        UPDATE
                            ".__DBC_SCHEMATA_CUSTOM_LANDING_PAGE__."
                        SET
                            iOrder=iOrder+1
                        WHERE
                            iOrder >='6'
                        AND    
                            id<>'".(int)$idLandingPage."'
                    ";
                    $result=$this->exeSQL($updateOrder);

                        return $idLandingPage;
                }
	}
        
        function copyTestimonialData($keyStr,$valuesStr)
        {
            $query = "
                INSERT INTO 
                    ".__DBC_SCHEMATA_CUSTOMER_TESTIMONIAL__."
                (
                    $keyStr  
                ) 
                VALUES 
                (
                    $valuesStr    
                )
            "; 
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                    return true;
            }
	}
        
        function copyPartnerimonialData($keyStr,$valuesStr)
        {
            $query = "
                INSERT INTO 
                    ".__DBC_SCHEMATA_PARTNER_LOGO__."
                (
                    $keyStr  
                ) 
                VALUES 
                (
                    $valuesStr    
                )
            "; 
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                    return true;
            }
	}
        
        function getAutomatedRfqResponseList($idAutomatedRfq=0,$szCode='')
        {
            $queryWhere='';
            if((int)$idAutomatedRfq>0)
            {
                $queryWhere="AND
                    id='".(int)$idAutomatedRfq."'
                ";
            }
            if($szCode!='')
            {
                $queryWhere .="AND
                    szCode='".mysql_escape_custom($szCode)."'
                ";
            }
            $resArr=array();
            $query="
                SELECT
                    id,
                    szResponse,
                    szCode,
                    dtCreated
                FROM
                    ".__DBC_SCHEMATA_AUTOMATED_RFQ_RESPONSE__."
                WHERE
                    isDeleted='0'
                $queryWhere    
                ORDER BY
                    szResponse ASC
                ";
            //echo $query;
            if($result=$this->exeSQL($query))
            {
                while($row=$this->getAssoc($result))
                {
                    $resArr[]=$row;
                }
                return $resArr;
            }
            else
            {
                return array();
            }
        }
        
        function isAutomatedRfqResponseCodeExist($szAutomateRfqResponseCode)
        {
            if(!empty($szAutomateRfqResponseCode))
            {                
                $szAutomateRfqResponseCodeOld = $szAutomateRfqResponseCode;
                $unique_rfq = false;
                do
                {
                    $query="
                        SELECT
                            id
                        FROM
                            ".__DBC_SCHEMATA_AUTOMATED_RFQ_RESPONSE__."
                        WHERE
                            szCode='".mysql_escape_custom(trim($szAutomateRfqResponseCode))."'
                    ";
                    //echo $query."<br><br>";
                    if($result=$this->exeSQL($query))
                    {
                        if($this->iNumRows>0)
                        {
                            $szAutomateRfqResponseCode = $this->getAutomatedRfqResponseCode();
                        }
                        else
                        {
                            $unique_rfq = true;
                        }
                    }
                }while(!$unique_rfq);
                return $szAutomateRfqResponseCode;                
                
            }
        }
        
        function getAutomatedRfqResponseCode()
        {
            $kBooking = new cBooking(); 
            $number = 10; 
            $uniqueNumber = $kBooking->generateUniqueToken($number); 
            $szReferenceToken = "T".$uniqueNumber; 
            $szReferenceToken = strtoupper($szReferenceToken); 
            return $szReferenceToken;
        }
        
        function addUpdateAutomatedRfqResponse($szCommentText,$idAutomatedRfq)
        {
            if((int)$idAutomatedRfq>0)
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_AUTOMATED_RFQ_RESPONSE__."
                    SET
                       szResponse='".  mysql_escape_custom($szCommentText)."'
                    WHERE
                        id='".(int)$idAutomatedRfq."'
                    ";
            }
            else
            {
                $szAutomateRfqResponseCode = $this->getAutomatedRfqResponseCode();
                $szAutomateRfqResponseCode = $this->isAutomatedRfqResponseCodeExist($szAutomateRfqResponseCode);
                $query="
                    INSERT INTO
                        ".__DBC_SCHEMATA_AUTOMATED_RFQ_RESPONSE__."
                    (
                        szResponse,
                        szCode,
                        dtCreated
                    )
                        VALUES
                    (
                        '".  mysql_escape_custom($szCommentText)."',
                        '".  mysql_escape_custom($szAutomateRfqResponseCode)."',
                        NOW()    
                    )
                ";
            }
            $result=$this->exeSQL($query);
            if((int)$idAutomatedRfq==0)
            {
                $this->idAutomatedRFQ=$this->iLastInsertID;
                $this->szAutomateRfqResponseCode=$szAutomateRfqResponseCode;
            }
            return true;
        }
        
        function deleteAutomatedRfqResponse($idAutomatedRfq)
        {
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_AUTOMATED_RFQ_RESPONSE__."
                SET
                   isDeleted='1'
                WHERE
                    id='".(int)$idAutomatedRfq."'
                ";
            $result=$this->exeSQL($query);
            return true;
        }
        
        function updateIdAutomatedPricingEmailTemplateForStandardCargo($idAutomatedRfq,$idLandingPage,$szAutomatedRfqResponseCode)
        {
             $query="
                UPDATE
                    ".__DBC_SCHEMATA_STANDARD_CARGO_HANDLING__."
                SET
                   idLandingPage='".(int)$idAutomatedRfq."'
                WHERE
                    idLandingPage='".(int)$idLandingPage."'
                ";
            // echo $query;
            $result=$this->exeSQL($query);
            
             $query="
                UPDATE
                    ".__DBC_SCHEMATA_STANDARD_PRICING__."
                SET
                   idLandingPage='".(int)$idAutomatedRfq."'
                WHERE
                    idLandingPage='".(int)$idLandingPage."'
                ";
             //echo $query;
            $result=$this->exeSQL($query);
             
             
             $query="
                UPDATE
                    ".__DBC_SCHEMATA_CUSTOM_LANDING_PAGE__."
                SET
                   szAutomatedRfqResponseCode='".  mysql_escape_custom($szAutomatedRfqResponseCode)."'
                WHERE
                    id='".(int)$idLandingPage."'
                ";
             $result=$this->exeSQL($query);
             //echo $query;
            
        }
        
        function checkAutomatedRFQCodeExists($szAutomatedRfqResponseCode)
        {
            
        }
        
        function set_szDesc( $value,$key )
	{   
            $this->szDesc = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "Description_".$key, t($this->t_base_search_notification.'fields/to_contains'), false, false, true );
	}
        
        
        function set_szKey( $value,$key )
	{   
            $this->szKey = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "Keywords_".$key, t($this->t_base_search_notification.'fields/to_contains'), false, false, true );
	}
        
        
        function set_szTitle( $value,$key )
	{   
            $this->szTitle = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "Title_".$key, t($this->t_base_search_notification.'fields/to_contains'), false, false, true );
	}
        
        function set_idFromCountry( $value )
	{   
            $this->idFromCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idFromCountry", t($this->t_base_search_notification.'fields/from_country'), false, false, true );
	}        
        
        function set_idToCountry( $value )
	{   
            $this->idToCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idToCountry", t($this->t_base_search_notification.'fields/to_country'), false, false, true );
	}        
        
        function set_idLanguage( $value )
	{   
            $this->idLanguage = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iLanguage", t($this->t_base_search_notification.'fields/language'), false, false, true );
	}
        
        function set_szToContains( $value )
	{   
		$this->szToContains = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szToContains", t($this->t_base_search_notification.'fields/to_contains'), false, false, true );
	}
	function set_szFromContains( $value )
	{   
		$this->szFromContains = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szFromContains", t($this->t_base_search_notification.'fields/from_contains'), false, false, true );
	}
        
        function set_szPopupHeading( $value )
	{   
		$this->szPopupHeading = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szHeading", t($this->t_base_search_notification.'fields/popup_heading'), false, false, true );
	}
        
        function set_szButtonText( $value )
	{   
		$this->szButtonText = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szButtonText", "Button Text", false, false, true );
	}
        function set_szButtonTextRfq( $value )
	{   
            $this->szButtonTextRfq = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szButtonTextRfq", "Button Text", false, false, true );
	}
        
        function set_szContinueButtonText( $value )
	{   
            $this->szContinueButtonText = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szContinueButtonText", "Continue button Text", false, false, true );
	} 
        function set_szContinue2ButtonText( $value )
	{   
            $this->szContinue2ButtonText = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szContinue2ButtonText", "Continue button Text", false, false, true );
	}
        function set_szContinue3ButtonText( $value )
	{   
            $this->szContinue3ButtonText = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szContinue2ButtonText", "Continue button Text", false, false, true );
	}
        function set_szButtonUrl( $value )
	{   
		$this->szButtonUrl = $this->validateInput( $value, __VLD_CASE_URL__, "szButtonUrl", "Button Url", false, false, true );
	}
        
        function set_szMessage( $value )
	{   
		$this->szMessage = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szMessage", t($this->t_base_search_notification.'fields/popup_message'), false, false, true );
	}
        
	function set_szUrl($value, $require_feild_flag=true,$anchor_tag=false )
	{	
		if($anchor_tag)
		{
			$szErrorMessage = t( $this->t_base."fields/anchor_tag" ) ;
		}
		else
		{
			$szErrorMessage = t( $this->t_base."fields/szUrl" ) ;
		}
		$this->szUrl = $this->validateInput($value,__VLD_CASE_ANYTHING__,'szUrl',$szErrorMessage,false, false ,$require_feild_flag );
	}
	function set_searchField( $value )
	{   
		$this->searchField = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "searchField", t($this->t_base.'fields/searchField')." ".t($this->t_base.'fields/searchField'), false, false, false );
	}
	function set_idExplain( $value )
	{   
		$this->idExplain = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idExplain", t($this->t_base.'fields/idExplain')." ".t($this->t_base.'fields/idExplain'), false, false, false );
	}
	function set_id( $value )
	{   
            $this->id = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "id", t($this->t_base.'fields/id'), false, false, true );
	}
	function set_idAdmin( $value )
	{   
            $this->idAdmin = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idAdmin", t($this->t_base.'fields/idAdmin'), false, false, true );
	}
	function set_szHeading( $value ,$flag=false)
	{
            $this->szHeading = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szHeading", t($this->t_base.'fields/heading'), false, 255, $flag );
	}
        function set_szSeoDescription( $value ,$flag=false)
        {
            $this->szSeoDescription = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSeoDescription", "Seo ".t($this->t_base.'fields/description'), false, false, $flag );
	}
        function set_iDisplaySeoText( $value ,$flag=false)
        {
		$this->iDisplaySeoText = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iDisplaySeoText", t($this->t_base.'fields/iDisplaySeoText'), false, 255, $flag );
	}
	function set_szDescription( $value )
	{
		$this->szDescription = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDescription", t($this->t_base.'fields/description'), false, false, true );
	}
	function set_iOrderBy( $value )
	{
		$this->iOrderBy = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iOrderBy", t($this->t_base.'fields/iOrderBy'), false, false, true );
	}
	function set_szComment( $value )
	{
		$this->szComment = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szComment", t($this->t_base.'fields/szComment'), false, false, true );
	}
	function set_szPageHeading( $value )
	{
		$this->szPageHeading = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPageHeading", t($this->t_base.'fields/szPageHeading'), false, false, true );
	}
	function set_szLink( $value )
	{
		$this->szLink = $this->validateInput( $value, __VLD_CASE_URI__, "szLink", t($this->t_base.'fields/szLink'), false, false, true );
	}
        function set_szLinkedPageUrl( $value )
	{
            $this->szLinkedPageUrl = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLinkedPageUrl", t($this->t_base.'fields/szLink'), false, false, false );
	}	 
	function set_szLinkTitle( $value )
	{
            $this->szLinkTitle = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLinkTitle", t($this->t_base.'fields/szLinkTitle'), false, false, true );
	} 
	function set_szMetaKeywords( $value, $require_feild_flag=true )
	{
		$this->szMetaKeywords = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szMetaKeywords", t($this->t_base.'fields/szMetaKeywords'), false, false, $require_feild_flag );
	}
	function set_szMetaTitle( $value, $require_feild_flag=true )
	{
		$this->szMetaTitle = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szMetaTitle", t($this->t_base.'fields/szMetaTitle'), false, false, $require_feild_flag );
	}
	function set_szPageName( $value, $require_feild_flag=true )
	{
		$this->szPageName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPageName", t($this->t_base.'fields/pageheading'), false, false, $require_feild_flag );
	}
	function set_szBackgroundColor( $value, $require_feild_flag=true )
	{
		$this->szBackgroundColor = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szBackgroundColor", t($this->t_base.'fields/background_color'), false, false, $require_feild_flag );
	}	
        function set_szSeoHeading( $value, $require_feild_flag=true )
	{
		$this->szSeoHeading = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSeoHeading", t($this->t_base.'fields/heading'), false, false, $require_feild_flag );
	}	
        
	function set_szMetaKeyword( $value, $require_feild_flag=true )
	{
		$this->szMetaKeyword = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szMetaKeyword", t($this->t_base.'fields/szMetaKeyword'), false, false, $require_feild_flag );
	}
	function set_szMetaDescription( $value, $require_feild_flag=true )
	{
            $this->szMetaDescription = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szMetaDescription", t($this->t_base.'fields/szMetaDescription'), false, false, $require_feild_flag );
	}
	function set_iActive( $value )
	{
		$this->iActive = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iActive", t($this->t_base.'fields/iActive'), false, false, true );
	}
	function set_iLanguage( $value )
	{
		$this->iLanguage = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iLanguage", t($this->t_base.'fields/iLanguage'), false, false, true );
	}
	
	function set_szContent( $value, $require_feild_flag=true )
	{
		$this->szContent = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDescription", t($this->t_base.'fields/description'), false, false, $require_feild_flag );
	}
 	function set_iPublished( $value )
	{
		$this->iPublished = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iPublished", t($this->t_base.'fields/Published'), false, false, true );
	}
 	function set_szTextA( $value, $require_feild_flag=true,$counter=false,$szErrorMessage=false)
	{
		if($counter>0)
		{
			$this->szTextA[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextA".$counter, t($this->t_base.'fields/textAtA')."".$szErrorMessage, false, false, $require_feild_flag );
		}
		else
		{
			$this->szTextA = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextA", t($this->t_base.'fields/textAtA'), false, false, $require_feild_flag );
		}
	}
 	function set_szTextB( $value, $require_feild_flag=true ,$counter=false,$szErrorMessage=false)
	{
		if($counter>0)
		{
			$this->szTextB[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextB".$counter, t($this->t_base.'fields/textAtB')."".$szErrorMessage, false, false, $require_feild_flag );
		}
		else
		{
			$this->szTextB = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextB", t($this->t_base.'fields/textAtB'), false, false, $require_feild_flag );
		}
	} 
	function set_szTextC( $value, $require_feild_flag=true ,$counter=false)
	{
		if($counter>0)
		{
			$this->szTextC[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextC".$counter, t($this->t_base.'fields/textAtC'), false, false, $require_feild_flag );
		}
		else
		{
			$this->szTextC = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextC", t($this->t_base.'fields/textAtC'), false, false, $require_feild_flag );
		}
	}
	function set_szTextD( $value, $require_feild_flag=true,$counter=false,$szErrorMessage=false)
	{
		if($counter>0)
		{
			$this->szTextD[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextD".$counter, t($this->t_base.'fields/textAtD')."".$szErrorMessage, false, false, $require_feild_flag );
		}
		else
		{
			$this->szTextD = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextD", t($this->t_base.'fields/textAtD'), false, false, $require_feild_flag );
		}		
	}
	function set_szTextE( $value, $require_feild_flag=true ,$counter=false,$szErrorMessage=false)
	{
		if($counter>0)
		{
			$this->szTextE[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextE".$counter, t($this->t_base.'fields/textAtE')."".$szErrorMessage, false, false, $require_feild_flag );
		}
		else
		{
			$this->szTextE = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextE", t($this->t_base.'fields/textAtE'), false, false, $require_feild_flag );
		}		
	}
	function set_szTextF( $value, $require_feild_flag=true,$counter=false )
	{
		if($counter>0)
		{
			$this->szTextF[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextF".$counter, t($this->t_base.'fields/textAtF'), false, false, $require_feild_flag );
		}
		else
		{
			$this->szTextF = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextF", t($this->t_base.'fields/textAtF'), false, false, $require_feild_flag );
		}		
	}
 	function set_szTextG( $value, $require_feild_flag=true,$counter=false,$szErrorMessage=false)
	{
		if($counter>0)
		{
			$this->szTextG[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextG".$counter, t($this->t_base.'fields/textAtG')."".$szErrorMessage, false, false, $require_feild_flag );
		}
		else
		{
			$this->szTextG = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextG", t($this->t_base.'fields/textAtG'), false, false, $require_feild_flag );
		}
		
	}
 	function set_szTextH( $value, $require_feild_flag=true,$counter=false ,$szErrorMessage=false)
	{
		if($counter>0)
		{
			$this->szTextH[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextH".$counter, t($this->t_base.'fields/textAtH')."".$szErrorMessage, false, false, $require_feild_flag );
		}
		else
		{
			$this->szTextH = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextH", t($this->t_base.'fields/textAtH'), false, false, $require_feild_flag );
		}
	}
 	function set_szTextI( $value, $require_feild_flag=true ,$counter=false,$szErrorMessage=false)
	{
		if($counter>0)
		{
			$this->szTextI[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextI".$counter, t($this->t_base.'fields/textAtI')."".$szErrorMessage, false, false, $require_feild_flag );
		}
		else
		{
			$this->szTextI = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextI", t($this->t_base.'fields/textAtI'), false, false, $require_feild_flag );
		}
	}
 	function set_szTextJ( $value, $require_feild_flag=true ,$counter=false,$szErrorMessage=false)
	{
		if($counter>0)
		{
			$this->szTextJ[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextJ".$counter, t($this->t_base.'fields/textAtJ')."".$szErrorMessage, false, false, $require_feild_flag );
		}
		else
		{
			$this->szTextJ = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextJ", t($this->t_base.'fields/textAtJ'), false, false, $require_feild_flag );
		}
	}
 	function set_szTextK( $value, $require_feild_flag=true,$counter=false )
	{
		if($counter>0)
		{
			$this->szTextK[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextK".$counter, t($this->t_base.'fields/textAtK'), false, false, $require_feild_flag );
		}
		else
		{
			$this->szTextK = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextK", t($this->t_base.'fields/textAtK'), false, false, $require_feild_flag );
		} 
	}
 	function set_szTextL( $value, $require_feild_flag=true,$counter=false,$szErrorMessage=false )
	{
		if($counter>0)
		{
			$this->szTextL[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextL".$counter, t($this->t_base.'fields/textAtL')."".$szErrorMessage, false, false, $require_feild_flag );
		}
		else
		{
			$this->szTextL = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextL", t($this->t_base.'fields/textAtL'), false, false, $require_feild_flag );
		}
	} 
 	function set_szTextM( $value, $require_feild_flag=true )
	{
		$this->szTextM = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextM", t($this->t_base.'fields/textAtM'), false, false, $require_feild_flag );
	}
	
	function set_szImageURL( $value, $require_feild_flag=true,$counter=false,$szErrorMessage=false )
	{
		$this->szImageURL[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szImageURL_".$counter, $szErrorMessage." ".t($this->t_base.'fields/image_url'), false, false, $require_feild_flag );
	}
	function set_szImageDesc( $value, $require_feild_flag=true,$counter=false,$szErrorMessage=false )
	{
		$this->szImageDesc[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szImageDesc_".$counter, $szErrorMessage." ".t($this->t_base.'fields/image_description'), false, false, $require_feild_flag );
	}
	function set_szImageTitle( $value, $require_feild_flag=true,$counter=false,$szErrorMessage=false )
	{
		$this->szImageTitle[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szImageTitle_".$counter, $szErrorMessage." ".t($this->t_base.'fields/image_title'), false, false, $require_feild_flag );
	}	
	function set_szTestimonialHeading( $value, $require_feild_flag=true,$counter=false,$szErrorMessage=false )
	{
		$this->szHeading[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szHeading_".$counter, $szErrorMessage." ".t($this->t_base.'fields/heading'), false, false, $require_feild_flag );
	}	
	function set_szTextDesc( $value, $require_feild_flag=true,$counter=false,$szErrorMessage=false )
	{
		$this->szTextDesc[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTextDesc_".$counter, $szErrorMessage." ".t($this->t_base.'fields/heading'), false, false, $require_feild_flag );
	}
	function set_szTitleName( $value, $require_feild_flag=true,$counter=false,$szErrorMessage=false )
	{
		$this->szTitleName[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTitleName_".$counter, $szErrorMessage." ".t($this->t_base.'fields/name_title'), false, false, $require_feild_flag );
	} 
	function set_idTestimonial( $value, $require_feild_flag=true,$counter=false,$szErrorMessage=false )
	{
		$this->idTestimonial[$counter] = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idTestimonial_".$counter, $szErrorMessage." ".t($this->t_base.'fields/name_title'), false, false, false );
	} 
	
	function set_szSubTitle( $value )
	{
		$this->szSubTitle = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSubTitle", t($this->t_base.'fields/sub_title'), false, false, true );
	}
	function set_szHeaderImage( $value )
	{
		$this->szHeaderImage = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szHeaderImage", t($this->t_base.'fields/image_url'), false, false, true );
	}
	
	function set_szUploadFileName( $value )
	{
		$this->szUploadFileName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szUploadFileName", t($this->t_base.'fields/image_header'), false, false, true );
	}
	function set_szUploadOGFileName( $value )
	{
		$this->szUploadOGFileName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szUploadOGFileName", t($this->t_base.'fields/open_graph_image'), false, false, true );
	}
	function set_szOpenGraphAuthor( $value )
	{
		$this->szOpenGraphAuthor = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szOpenGraphAuthor", t($this->t_base.'fields/open_graph_author'), false, false, true );
	}
	
	function set_szPictureTitle( $value )
	{
		$this->szPictureTitle = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPictureTitle", t($this->t_base.'fields/picture_title'), false, false, true );
	}
	
	function set_szPicture( $value )
	{
		$this->szPicture = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPicture", t($this->t_base.'fields/picture'), false, false, true );
	} 
	function set_szLogoImageURL( $value, $require_feild_flag=true,$counter=false,$szErrorMessage=false )
	{
		$this->szLogoImageURL[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLogoImageURL_".$counter, $szErrorMessage." ".t($this->t_base.'fields/image_url'), false, false, $require_feild_flag );
	}
	function set_szLogoImageDesc( $value, $require_feild_flag=true,$counter=false,$szErrorMessage=false )
	{
		$this->szLogoImageDesc[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLogoImageDesc_".$counter, $szErrorMessage." ".t($this->t_base.'fields/image_description'), false, false, $require_feild_flag );
	}
	function set_szLogoImageTitle( $value, $require_feild_flag=true,$counter=false,$szErrorMessage=false )
	{
		$this->szLogoImageTitle[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLogoImageTitle_".$counter, $szErrorMessage." ".t($this->t_base.'fields/image_title'), false, false, $require_feild_flag );
	}	
	function set_szToolTipText( $value, $require_feild_flag=true,$counter=false,$szErrorMessage=false )
	{
		$this->szToolTipText[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szToolTipText_".$counter, $szErrorMessage." ".t($this->t_base.'fields/tool_tip_text'), false, false, $require_feild_flag );
	} 
	function set_idPartnerLogo( $value, $require_feild_flag=true,$counter=false,$szErrorMessage=false )
	{
		$this->idPartnerLogo[$counter] = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idPartnerLogo_".$counter, $szErrorMessage." ".t($this->t_base.'fields/name_title'), false, false, false );
	}		
	
	
	function set_szLinkColor( $value, $require_feild_flag=true )
	{
		$this->szLinkColor = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLinkColor", t($this->t_base.'fields/link_color'), false, false, $require_feild_flag );
	}
	
	
	function set_szPictureDescription( $value )
	{
		$this->szPictureDescription = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPictureDescription", t($this->t_base.'fields/picture_description'), false, false, true );
	}
	
	function set_szWhiteLogoImageURL( $value, $require_feild_flag=true,$counter=false,$szErrorMessage=false )
	{
		$this->szWhiteLogoImageURL[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szWhiteLogoImageURL_".$counter, $szErrorMessage." ".t($this->t_base.'fields/image_url'), false, false, $require_feild_flag );
	}
        
        function set_szOriginStr( $value)
	{
            $this->szOriginStr = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szOriginStr", t($this->t_base.'fields/szOriginStr'), false, 255, true );
	}
        
        function set_szDestinationStr( $value)
	{
            $this->szDestinationStr = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDestinationStr", t($this->t_base.'fields/szDestinationStr'), false, 255, true );
	}
        function set_szOriginKey( $value)
	{
            $this->szOriginKey = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szOriginKey", t($this->t_base.'fields/szOriginKey'), false, 255, true );
	}
        function set_szDestinationKey( $value)
	{
            $this->szDestinationKey = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDestinationKey", t($this->t_base.'fields/szDestinationKey'), false, 255, true );
	}
        function set_idSearchAdwords( $value)
	{
            $this->idSearchAdwords = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idSearchAdwords", t($this->t_base.'fields/idSearchAdwords'), false, false, false );
	} 
        
        function set_szMiddleText( $value, $require_feild_flag=true )
	{
		$this->szMiddleText = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szMiddleText", t($this->t_base.'fields/middle_text'), false, false, $require_feild_flag );
	}
        
        function set_szContactMe( $value, $require_feild_flag=true )
	{
		$this->szContactMe = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szContactMe", t($this->t_base.'fields/contact_me'), false, false, $require_feild_flag );
	}
        
        function set_szShipperCompanyName( $value, $require_feild_flag=true )
	{
		$this->szShipperCompanyName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperCompanyName", t($this->t_base.'fields/shipper_company_name'), false, false, $require_feild_flag );
	}
        
        function set_szShipperFirstName( $value, $require_feild_flag=true )
	{
		$this->szShipperFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperFirstName", t($this->t_base.'fields/shipper_first_name'), false, false, $require_feild_flag );
	}
        
        function set_szShipperLastName( $value, $require_feild_flag=true )
	{
		$this->szShipperLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperLastName", t($this->t_base.'fields/shipper_last_name'), false, false, $require_feild_flag );
	}
        
        function set_szShipperEmail( $value, $require_feild_flag=true )
	{
		$this->szShipperEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szShipperEmail", t($this->t_base.'fields/shipper_email_address'), false, false, $require_feild_flag );
	}
        
        function set_szShipperPhone( $value, $require_feild_flag=true )
	{
            $this->szShipperPhone = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperPhone", t($this->t_base.'fields/shipper_phone_number'), false, false, $require_feild_flag );
	}
        
        function set_szShipperAddress( $value, $require_feild_flag=true )
	{
		$this->szShipperAddress = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperAddress", t($this->t_base.'fields/shipper_address'), false, false, $require_feild_flag );
	}
        
        function set_szShipperPostcode( $value, $require_feild_flag=true )
	{
            $this->szShipperPostcode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperPostcode", t($this->t_base.'fields/shipper_postcode'), false, false, $require_feild_flag );
	}
        
        function set_szShipperCity( $value, $require_feild_flag=true )
	{
		$this->szShipperCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperCity", t($this->t_base.'fields/shipper_city'), false, false, $require_feild_flag );
	}
        
        function set_idShipperCountry( $value, $require_feild_flag=true )
	{
		$this->idShipperCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idShipperCountry", t($this->t_base.'fields/shipper_country'), false, false, $require_feild_flag );
	}
        
        function set_idShipperDialCode( $value, $require_feild_flag=true )
	{
		$this->idShipperDialCode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idShipperDialCode", t($this->t_base.'fields/shipper_dial_code'), false, false, $require_feild_flag );
	}
        
        function set_szShipperAddress_pickup( $value, $require_feild_flag=true )
	{
		$this->szShipperAddress_pickup = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperAddress_pickup", t($this->t_base.'fields/shipper_pickup_address'), false, false, $require_feild_flag );
	}
        
        
        function set_szShipperPostcode_pickup( $value, $require_feild_flag=true )
	{
		$this->szShipperPostcode_pickup = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperPostcode_pickup", t($this->t_base.'fields/shipper_pickup_postcode'), false, false, $require_feild_flag );
	}
        
        function set_szShipperCity_pickup( $value, $require_feild_flag=true )
	{
		$this->szShipperCity_pickup = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperCity_pickup", t($this->t_base.'fields/shipper_pickup_city'), false, false, $require_feild_flag );
	}
        
        function set_idShipperCountry_pickup( $value, $require_feild_flag=true )
	{
            $this->idShipperCountry_pickup = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idShipperCountry_pickup", t($this->t_base.'fields/shipper_pickup_country'), false, false, $require_feild_flag );
	}
        
        function set_idFileOwner( $value)
	{
            $this->idFileOwner = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idFileOwner",'', false, false, true);
	}
        function set_iQuoteValidity( $value)
	{
            $this->iQuoteValidity = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iQuoteValidity",'', 1, false, true);
	} 
        function set_szQuoteEmailSubject( $value)
	{
            $this->szQuoteEmailSubject = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szQuoteEmailSubject",'', false, false, true);
	} 
        function set_szQuoteEmailBody( $value)
	{
            $this->szQuoteEmailBody = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szQuoteEmailBody",'', false, false, true);
	}
        function set_idLandingPage( $value)
	{
            $this->idLandingPage = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idLandingPage",'', false, false, true);
	}
        function set_idStandardCargoHandler( $value)
	{
            $this->idStandardCargoHandler = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idStandardCargoHandler",'', false, false, false);
	} 
        
        function set_idForwarder( $value)
	{
            $this->idForwarder = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idForwarder",'', false, false, true);
	}
        function set_iBookingType( $value)
	{
            $this->iBookingType = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iBookingType",'', false, false, true);
	}
        function set_idCourierProvider( $value)
	{
            $this->idCourierProvider = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idCourierProvider",'', false, false, true);
	}
        function set_idCourierProviderProduct( $value)
	{
            $this->idCourierProviderProduct = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idCourierProviderProduct",'', false, false, true);
	}
        function set_szTransitTime( $value)
	{
            $this->szTransitTime = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTransitTime",'', false, false, true);
	}
        function set_idHandlingCurrency( $value)
	{
            $this->idHandlingCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idHandlingCurrency",'', false, false, true);
	}
        function set_fHandlingFeePerBooking( $value)
	{
            $this->fHandlingFeePerBooking = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fHandlingFeePerBooking",'', false, false, true);
	}
        function set_fHandlingMarkupPercentage( $value)
	{
            $this->fHandlingMarkupPercentage = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fHandlingMarkupPercentage",'', false, false, true);
	}
        function set_idHandlingMinMarkupCurrency( $value)
	{
            $this->idHandlingMinMarkupCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idHandlingMinMarkupCurrency",'', false, false, true);
	}
        function set_fHandlingMinMarkupPrice( $value)
	{
            $this->fHandlingMinMarkupPrice = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fHandlingMinMarkupPrice",'', false, false, true);
	}
        function set_idStandardQuotePricing( $value)
	{
            $this->idStandardQuotePricing = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idStandardQuotePricing",'', false, false, false);
	} 
        function set_szHoldingHeading( $value, $iNumber, $require_feild_flag=true )
	{
            $this->szHoldingHeading[$iNumber] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szHoldingHeading".$iNumber, t($this->t_base.'fields/szHoldingHeading')." ".$iNumber, false, false, $require_feild_flag );
	}
        function set_szHoldingTextBold( $value, $iNumber, $require_feild_flag=true )
	{
            $this->szHoldingTextBold[$iNumber] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szHoldingTextBold".$iNumber, t($this->t_base.'fields/text')." ".$iNumber." ".t($this->t_base.'fields/bold'), false, false, $require_feild_flag );
	}
        function set_szHoldingTextPlain( $value, $iNumber, $require_feild_flag=true )
	{
            $this->szHoldingTextPlain[$iNumber] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szHoldingTextPlain".$iNumber, t($this->t_base.'fields/text')." ".$iNumber." ".t($this->t_base.'fields/plain'), false, false, $require_feild_flag );
	}
        function set_szHoldingEmailHeading( $value, $require_feild_flag=true )
	{
            $this->szHoldingEmailHeading = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szHoldingEmailHeading", t($this->t_base.'fields/szHoldingEmailHeading'), false, false, $require_feild_flag );
	} 
        function set_szHoldingFromHeading( $value, $require_feild_flag=true )
	{
            $this->szHoldingFromHeading = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szHoldingFromHeading", t($this->t_base.'fields/szHoldingFromHeading'), false, false, $require_feild_flag );
	}
        function set_szHoldingToHeading( $value, $require_feild_flag=true )
	{
            $this->szHoldingToHeading = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szHoldingToHeading", t($this->t_base.'fields/szHoldingToHeading'), false, false, $require_feild_flag );
	}
        function set_szHoldingSubmitButtonText( $value, $require_feild_flag=true )
	{
            $this->szHoldingSubmitButtonText = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szHoldingSubmitButtonText", t($this->t_base.'fields/szHoldingSubmitButtonText'), false, false, $require_feild_flag );
	}
        function set_szHoldingThanksButtonText( $value, $require_feild_flag=true )
	{
            $this->szHoldingThanksButtonText = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szHoldingThanksButtonText", t($this->t_base.'fields/szHoldingThanksButtonText'), false, false, $require_feild_flag );
	}
        function set_szYoutubeUrl( $value, $require_feild_flag=true )
	{
            $this->szYoutubeUrl = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szYoutubeUrl", t($this->t_base.'fields/szYoutubeUrl'), false, false, $require_feild_flag );
	} 
        
        function set_szFromInstruction( $value, $require_feild_flag=true )
	{
            $this->szFromInstruction = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szFromInstruction", t($this->t_base.'fields/from_instruction'), false, false, $require_feild_flag );
	}
        function set_szToInstruction( $value, $require_feild_flag=true )
	{
            $this->szToInstruction = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szToInstruction", t($this->t_base.'fields/to_instruction'), false, false, $require_feild_flag );
	} 
        function set_szCargoType( $value, $require_feild_flag=true )
	{
            $this->szCargoType = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCargoType", t($this->t_base.'fields/cargo_type'), false, false, $require_feild_flag );
	}
        
        function set_iDefaultInsurance( $value, $require_feild_flag=true )
	{
            $this->iDefaultInsurance = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iDefaultInsurance", t($this->t_base.'fields/default_insurance'), false, false, $require_feild_flag );
	}
        
        function set_idInsuranceCurrency( $value, $require_feild_flag=true )
	{
            $this->idInsuranceCurrency = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idInsuranceCurrency", t($this->t_base.'fields/insurance_currency'), false, false, $require_feild_flag );
	}
}

?>