<?php
/**
 * This file is the container for all admin related functionality.
 * All functionality related to admin details should be contained in this class.
 *
 * admin.class.php
 *
 * @copyright Copyright (C) 2012 Transporteca, LLC
 * @author Anil
 * @package Transporteca Development
 */
if(!isset($_SESSION))
{
	session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_INC__ . "/functions.php" );
require_once( __APP_PATH_INC__ . "/courier_functions.php" );
require_once( __APP_PATH_CLASSES__ . "/database.class.php");

class cVatApplication extends cDatabase
{ 
    function __construct()
    {
        parent::__construct();
        return true;
    } 
    var $t_base_courier='management/vatApplication/';
    
    
    function validateForwarderCountry($data)
    {
    	
    	$this->set_idForwarderCountry(sanitize_specific_html_input(trim($data['idCountryForwarder'])));
    	
    	//exit if error exist.
		if ($this->error === true)
		{
			return false;
		}
		$kCourierServices= new cCourierServices();
		$forwarderCountryArr=explode("_",$this->idForwarderCountry);
		
    	if($forwarderCountryArr[1]=='r')
		{
			$idForwarderCountryArr=$kCourierServices->getCountriesByIdRegion($forwarderCountryArr[0]);
		}
		else
		{
			$idForwarderCountryArr[0]=$this->idForwarderCountry;
		}
		
		if(!empty($idForwarderCountryArr))
		{
			foreach($idForwarderCountryArr as $idForwarderCountryArrs)
			{
				if(!$this->checkForwarderCountryExists($idForwarderCountryArrs))
				{
					
					return true;
					
				}
			}
		}
		
		return false;	
    }
    
    function checkForwarderCountryExists($idCountryFrom,$idCountryTo)
    {
       	$query="
            SELECT
                id,
                idCountryFrom
            FROM
                ".__DBC_SCHEMATA_VAT_APPLICATION_DATA__."
            WHERE
                idCountryFrom = '".(int)$idCountryFrom."'
            AND
                idCountryTo = '".(int)$idCountryTo."'
            AND
                isDeleted='0'		
    	";
        //echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            { 
                return true; 
            }
            else
            {
            	return false;
            }
        }
    }
    
    function addForwarderCountryData($data)
    {
    	if($this->validateForwarderCountry($data))
    	{ 
            $forwarderCountryArr=explode("_",$this->idForwarderCountry);
            if($forwarderCountryArr[1]=='r')
            {
                $kCourierServices= new cCourierServices();
                $idForwarderCountryArr=$kCourierServices->getCountriesByIdRegion($forwarderCountryArr[0]);
            }
            else
            {
                $idForwarderCountryArr[0]=$this->idForwarderCountry;
            }
				
            if(!empty($idForwarderCountryArr))
            {
                foreach($idForwarderCountryArr as $idForwarderCountryArrs)
                {
                    if(!$this->checkForwarderCountryExists($idForwarderCountryArrs))
                    {
							
							$query="
									INSERT INTO
										".__DBC_SCHEMATA_VAT_APPLICATION_DATA__."
									(
										idCountryForwarder
									)
										VALUES
									(
										'".(int)$idForwarderCountryArrs."'
									)
								";
							
								$result = $this->exeSQL( $query );
							
						}
					}
				}
				
				return true;
    	}
    }
    
    function getForwarderCountryData($szSortField=false,$szSortOrder=false)
    {  
        if(!empty($szSortField))
        {
            if($szSortOrder=="DESC")
            {
                $query_order_by = $szSortField." DESC ";
            }
            else
            {
                $query_order_by = $szSortField." ASC ";
            }
        }
        else
        {
            $query_order_by = " cform.szCountryName ASC	";
        }
        
    	$query="
            SELECT
                va.id, 
                va.idCountryFrom,
                va.idCountryTo,
                cform.szCountryName as szFromCountryName,
                cform.fVATRate as fFromVatRate,
                cform.szVATRegistration as szFromVATRegistration,
                cto.szCountryName as szToCountryName,
                cto.fVATRate as fToVatRate,
                cto.szVATRegistration as szToVATRegistration
            FROM		
                ".__DBC_SCHEMATA_VAT_APPLICATION_DATA__." AS va
            INNER JOIN
                ".__DBC_SCHEMATA_COUNTRY__." AS cform
            ON
                cform.id=va.idCountryFrom
            INNER JOIN
                ".__DBC_SCHEMATA_COUNTRY__." AS cto
            ON
                cto.id=va.idCountryTo 
            WHERE
                va.isDeleted='0'
                $queryWhere	
            ORDER BY 
                $query_order_by
    	";
        //echo $query;
    	if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
                $ret_ary = array();
                $ctr = 0; 
                while($row=$this->getAssoc($result))
                { 
                    $ret_ary[$ctr] = $row;
                    $ctr++; 
                }
                return $ret_ary; 
            }
            else
            {
            	return array();
            }
        }
        else
        {
            return array();
        } 
    }
    
    function deleteForwarderCountryData($id)
    {
       	$query="
    		UPDATE
    			".__DBC_SCHEMATA_VAT_APPLICATION_DATA__."
    		SET
    			isDeleted='1'	
    		WHERE
    			idCountryForwarder IN ('".$id."')   					
    	";
       //	echo $query;
        //die();
   		if( ( $result = $this->exeSQL( $query ) ) )
		{
            if( $this->getRowCnt() > 0 )
            {
            	return true;
            }
            else
            {
            	return false;
            }
		}
    }
    
    function copyForwarderCountryData($id,$idCountryFrom,$idCountryTo)
    {
    	$query="
            INSERT INTO
                ".__DBC_SCHEMATA_VAT_APPLICATION_DATA__."
            ( 
                idCountryFrom,
                idCountryTo
            )
            VALUES
            ( 
                '".(int)$idCountryFrom."',
                '".(int)$idCountryTo."' 
            )
        ";

        $result = $this->exeSQL( $query );
        return true; 
    }
    
    
    function addVatTradeData($data)
    { 
    	$this->set_idFromCountry(sanitize_specific_html_input(trim($data['idCountryForm'])));
    	$this->set_idToCountry(sanitize_specific_html_input(trim($data['idToCountry'])));
    	
        //exit if error exist.
        if ($this->error === true)
        {
            return false;
        }
    	
    	if(!empty($data))
    	{
            $fromCountryArr=explode("_",$data['idCountryForm']);
            if($fromCountryArr[1]=='r')
            {
                $kCourierServices= new cCourierServices();
                $idFromCountryArr = $kCourierServices->getCountriesByIdRegion($fromCountryArr[0]);
            }
            else
            {
                $idFromCountryArr[0] = $data['idCountryForm'];
            }
			
            $toCountryArr = explode("_",$data['idToCountry']);
            if($toCountryArr[1]=='r')
            {
                $kCourierServices= new cCourierServices();
                $idToCountryArr=$kCourierServices->getCountriesByIdRegion($toCountryArr[0]);
            }
            else
            {
                $idToCountryArr[0]=$data['idToCountry'];
            } 
            if(!empty($idFromCountryArr) && !empty($idToCountryArr))
            {
                foreach($idToCountryArr as $idToCountryArrs)
                { 
                    foreach($idFromCountryArr as $idFromCountryArrs)
                    {     
                        $idCountryFrom = $idFromCountryArrs;
                        $idCountryTo = $idToCountryArrs;
                        
                        if($this->checkForwarderCountryExists($idCountryFrom,$idCountryTo))
                        {
                            continue;
                        }
                        else
                        {
                            $query="
                                INSERT INTO
                                    ".__DBC_SCHEMATA_VAT_APPLICATION_DATA__."
                                ( 
                                    idCountryFrom,
                                    idCountryTo
                                )
                                VALUES
                                ( 
                                    '".(int)$idCountryFrom."',
                                    '".(int)$idCountryTo."'
                                )
                            ";
                            $result = $this->exeSQL( $query ); 
                        } 
                    } 	
                    ++$ctr;
                } 	
            }
            return true;
        } 
    }
    
    function deleteForwarderCountryTradeData($id,$updateFlag=false)
    {
    	if($updateFlag)
    	{
	    	$query="
	    		UPDATE
	    			".__DBC_SCHEMATA_VAT_APPLICATION_DATA__."
	    		SET
	    			isDeleted='1'	
	    		WHERE
	    			id IN ('".$id."')   					
	    	";
    	}
    	else
    	{
    		$query="
	    		UPDATE
	    			".__DBC_SCHEMATA_VAT_APPLICATION_DATA__."
	    		SET
	    			idCountryTo=0,
	    			idCountryFrom=0	
	    		WHERE
	    			id='".(int)$id."'   					
	    	";
    		
    	}
//       	echo $query;
//        die;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
            	return true;
            }
            else
            {
            	return false;
            }
        } 
    }
    
    function getVatRate($data)
    {
       	$query="
            SELECT
                c.fVATRate
            FROM
                ".__DBC_SCHEMATA_VAT_APPLICATION_DATA__." AS vad
            INNER JOIN    
                ".__DBC_SCHEMATA_COUNTRY__." AS c
            ON
                c.id=vad.idCountryForwarder
            WHERE 
                vad.idCountryFrom='".(int)$data['idOriginCountry']."'
            AND
                vad.idCountryTo='".(int)$data['idDestinationCountry']."'
            AND        
                vad.isDeleted='0'		
        ";
       // $this->queryString = $query ;
        //echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
                $row=$this->getAssoc($result); 
                return $row['fVATRate'];
            }
            else
            {
                return 0;
            }
        }
    } 
    function getManualFeeListing($defaultFlag=true,$idManualFee=false,$data=array())
    {
        $queryWhere="";
        if($idManualFee>0)
        {
            $queryWhere = " AND mf.id ='".(int)$idManualFee."' ";
        }  
        else
        {
           $queryWhere = " AND isDefaultFee='0'"; 
        }
        
        if(!empty($data))
        {
            if($data['idForwarder']>0)
            {
                $queryWhere .= " AND idForwarder = '".(int)$data['idForwarder']."'"; 
            }
            if(!empty($data['szProduct']))
            {
                $queryWhere .= " AND szProduct = '".mysql_escape_custom($data['szProduct'])."' "; 
            }
            if(!empty($data['szCargo']))
            {
                $queryWhere .= " AND szCargo = '".mysql_escape_custom($data['szCargo'])."' "; 
            }
            if((int)$data['iAllForwarderFlag']==1)
            {
                $queryWhere = " AND mf.idForwarder ='0' AND mf.iAllForwarderFlag = '1' "; 
            }
        }
        
        $kConfig = new cConfig();
        $configLangArr=$kConfig->getConfigurationLanguageData('__TABLE_TRANSPORT_MODE__');
       
        $query="
            SELECT
                mf.id,
                mf.idForwarder,
                mf.szProduct,
                mf.szCargo,
                mf.idCurrency,
                mf.szCurrency,
                mf.fManualFee,
                mfct.szName,
                f.szDisplayName,
                f.szForwarderAlias,
                mf.iAllForwarderFlag
            FROM    
                ".__DBC_SCHEMATA_MANUAL_FEE__." AS mf
            INNER JOIN
                ".__DBC_SCHEMATA_MANUAL_FEE_CARGO_TYPE__." AS mfct
            ON
                mf.szCargo=mfct.szCode
            LEFT JOIN
                ".__DBC_SCHEMATA_FROWARDER__." AS f
            ON
                mf.idForwarder=f.id    
            WHERE
                mf.isDeleted='0' 
                $queryWhere
            ORDER BY
                szDisplayName ASC, szForwarderAlias ASC
        ";
        //echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
                $ctr=0;
                while($row=$this->getAssoc($result))
                {
                    $szProductName='';
                    if(!empty($configLangArr[$row['szProduct']]))
                    {
                        $szProductName=$configLangArr[$row['szProduct']]['szShortName'];
                    }
                    $resArr[$ctr]=$row;
                    $resArr[$ctr]['szProductName']=$szProductName;
                    $ctr++;
                } 
                return $resArr;
            }
            else
            {
                return array();
            }
        }
    } 
    function getDefaultManualFee()
    {  
        $query="
            SELECT
                mf.id,
                mf.idForwarder,
                mf.szProduct,
                mf.szCargo,
                mf.idCurrency,
                mf.szCurrency,
                mf.fManualFee 
            FROM    
                ".__DBC_SCHEMATA_MANUAL_FEE__." AS mf 
            WHERE
                mf.isDeleted='0' 
            AND 
                isDefaultFee='1' 
        ";
        //echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            { 
                $row=$this->getAssoc($result); 
                return $row;
            }
            else
            {
                return array();
            }
        }
    }
    function getManualFeeCaroTypeList($bInsuranceFlag=false,$szName='',$idCargoType=false)
    {
        $queryWHERE='';
        if(trim($szName)!='')
        {
            $queryWHERE="WHERE LOWER(szName)='".mysql_escape_custom(strtolower($szName))."'"; 
        }
        else if($idCargoType>0)
        {
            $queryWHERE=" WHERE id = '".(int)mysql_escape_custom($idCargoType)."' "; 
        }
            
        $query="
            SELECT
                id,
                szCode,
                szName
            FROM
                ".__DBC_SCHEMATA_MANUAL_FEE_CARGO_TYPE__."
            $queryWHERE        
            ORDER BY
                szName ASC
        ";
        //echo $query;
        if(($result = $this->exeSQL($query)))
        { 
            $resArr = array();
            $ctr = 0;
            while($row=$this->getAssoc($result))
            {
                if($bInsuranceFlag)
                {
                    if(trim($szName)!='' || $idCargoType>0)
                    {
                        return $row['szCode'];
                    }
                    else
                    {
                        $resArr[$row['szCode']]=$row; 
                    }
                }
                else
                {
                    $resArr[$ctr]=$row;
                    $ctr++;
                } 
            } 
            return $resArr; 
        }
    }
    
    function loadCargoTypes($szCode=false,$idCargoType=false)
    {
        if(!empty($szCode) || $idCargoType>0)
        {
            if(!empty($szCode))
            {
                $query_where = " szCode = '".mysql_escape_custom($szCode)."' ";
            }
            else if($idCargoType>0)
            {
                $query_where = " id = '".mysql_escape_custom($idCargoType)."' ";
            }
            else
            {
                return false;
            }
            
            $query="
                SELECT
                    id,
                    szCode,
                    szName
                FROM
                    ".__DBC_SCHEMATA_MANUAL_FEE_CARGO_TYPE__."
                WHERE
                   $query_where 
            ";
            //echo $query;
            if(($result = $this->exeSQL($query)))
            {  
                $row = $this->getAssoc($result); 
                $this->idCargoType = trim($row['id']);
                $this->szCargoTypeCode = trim($row['szCode']);
                $this->szCargoTypeName = trim($row['szName']);
                return $row; 
            }
        } 
    }
            
    function addUpdateManualFee($data)
    {
        if(!empty($data))
        { 
            $iAllForwarderFlag=0;
            if($data['iDefaultManualPricing']!=1)
            {
                
                if(sanitize_specific_html_input(trim($data['idForwarder']))=='AQ')
                {
                    $this->idForwarder='0';
                    $iAllForwarderFlag=1;
                }
                else
                {
                    $this->set_idForwarder(sanitize_specific_html_input(trim($data['idForwarder'])));
                }
                
                $this->set_szProduct(sanitize_specific_html_input(trim($data['szProduct'])));
                $this->set_szCargo(sanitize_specific_html_input(trim($data['szCargo']))); 
            } 
            
            $this->set_idCurrency(sanitize_specific_html_input(trim($data['idCurrency'])));
            $this->set_fManualFee(sanitize_specific_html_input(trim($data['fManualFee'])));
            $this->set_idManualFee(sanitize_specific_html_input(trim($data['idManualFee'])));

            $isDefaultFee = $data['iDefaultManualPricing'];
            //exit if error exist.
            if ($this->error === true)
            {
                return false;
            }
            if($this->idCurrency==1)
            {
                $szCurrency = "USD";
            }
            else
            {
                $kWhsSearch = new cWHSSearch();
                $resultAry = $kWhsSearch->getCurrencyDetails($this->idCurrency);
                $szCurrency = $resultAry['szCurrency']; 
            }
            
            if($this->idManualFee>0)
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_MANUAL_FEE__."
                    SET
                        idForwarder = '".(int)$this->idForwarder."',
                        szProduct = '".mysql_escape_custom($this->szProduct)."',
                        szCargo = '".mysql_escape_custom($this->szCargo)."',
                        idCurrency = '".mysql_escape_custom($this->idCurrency)."',
                        szCurrency = '".mysql_escape_custom($szCurrency)."',
                        fManualFee = '".mysql_escape_custom($this->fManualFee)."',
                        isDefaultFee = '".mysql_escape_custom($isDefaultFee)."',
                        dtUpdated = now(),
                        iAllForwarderFlag='".(int)$iAllForwarderFlag."'
                    WHERE
                        id='".(int)$this->idManualFee."'   					
	    	";
            }
            else
            {
                $query="
                    INSERT INTO
                        ".__DBC_SCHEMATA_MANUAL_FEE__."
                    (
                        idForwarder,
                        szProduct,
                        szCargo,
                        idCurrency,
                        szCurrency,
                        fManualFee,
                        isDefaultFee,
                        dtCreated,
                        iAllForwarderFlag
                    )
                    VALUES
                    (
                        '".(int)$this->idForwarder."',
                        '".mysql_escape_custom(trim($this->szProduct))."',
                        '".mysql_escape_custom(trim($this->szCargo))."',
                        '".(int)$this->idCurrency."',
                        '".mysql_escape_custom(trim($szCurrency))."',
                        '".mysql_escape_custom(trim($this->fManualFee))."',
                        '".(int)$isDefaultFee."',
                        now(),
                        '".(int)$iAllForwarderFlag."'
                    )
                ";
            }
            //echo "<br>".$query;
            //die;
            
            if(($result = $this->exeSQL($query)))
            { 
                return true;  
            } 
            else
            {
                return false;
            }
        }
    }
    
    function deleteManualFee($idManualFee)
    {
        if($idManualFee>0)
        {
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_MANUAL_FEE__."
                SET 
                    isDeleted = '1'
                WHERE
                    id='".(int)$idManualFee."'   					
            ";
            //echo $query;
            if(($result = $this->exeSQL($query)))
            { 
                return true;  
            } 
            else
            {
                return false;
            }
        }
    }
    
    /*
     * This is backup function of cBooking->updateInsuranceDetails
     * 
     * Feel free to update following function in booking.class.php if needed
     */
    function updateInsuranceDetails($data,$postSearchAry,$flag=false,$bParnerApi=false,$bQuickQuote=false)
    {
        if(!empty($data) && !empty($postSearchAry))
        {   
            if($flag=='REMOVE')
            {
                $this->iInsurance = 0;
            }
            else
            {
                $this->set_iInsurance(sanitize_all_html_input(trim($data['iInsurance'])));
                $this->set_idInsuranceCurrency(sanitize_all_html_input(trim($data['idInsuranceCurrency']))); 
                if(!is_numeric($data['fValueOfGoods']))
                {
                    $iUpdateInsuranceWithBlankValue = 1;
                    $data['fValueOfGoods'] = 0;
                }
                else
                {
                    $iUpdateInsuranceWithBlankValue = 0;
                } 
                $this->set_fValueOfGoods(sanitize_all_html_input(trim($data['fValueOfGoods']))); 
            } 
            if($this->error==true && !$bParnerApi)
            { 
                $this->iInsuranceValidationFailed = 1;
                return false;
            } 

            $kInsurance = new cInsurance();
            $kWarehouseSearch = new cWHSSearch();
            $idBooking = $postSearchAry['id'] ;

            /*
            * Booking Price in USD
            */
            $fTotalBookingPriceUsd = $postSearchAry['fTotalPriceUSD']; 
            if($this->iInsurance==1)
            {
                if($postSearchAry['idTransportMode']<=0)
                {
                    $postSearchAry['idTransportMode'] = 2; //LCL
                }

                $insuranceDataAry = array();
                $insuranceDataAry['idOriginCountry'] = $postSearchAry['idOriginCountry'];
                $insuranceDataAry['idDestinationCountry'] = $postSearchAry['idDestinationCountry']; 
                $insuranceDataAry['idTransportMode'] = $postSearchAry['idTransportMode']; 
                $insuranceDataAry['iPrivate'] = (int)$postSearchAry['isMoving'];  
                $idTransportMode = $postSearchAry['idTransportMode'];

                $kInsurance = new cInsurance();
                $buyRateListAry = array();
                $buyRateListAry = $kInsurance->getBuyrateCombination($insuranceDataAry,true);

                $insuranceSellRateAry = array();
                if(!empty($buyRateListAry))
                {
                    $buyRateListArys= $buyRateListAry[$idTransportMode];
                    $idInsuranceBuyRate = $buyRateListArys['id'] ; 

                    $insuranceSellRateAry = $kInsurance->getInsuranceRates(__INSURANCE_SELL_RATE_TYPE__,$idInsuranceBuyRate,true); 
                    if(empty($insuranceSellRateAry))
                    {
                        $this->iInsuranceOtherErrors = 1;
                        $this->addError('fValueOfGoods_1',t($this->t_base.'fields/maximum_insurance_value'));
                        return false;
                    }
                }  
                else
                {
                    $this->iInsuranceOtherErrors = 1;
                    $this->addError('fValueOfGoods_1',t($this->t_base.'fields/maximum_insurance_value')); 
                    return false;
                }

                /*
                * Fetching max sell rate type insurance
                */
                $insuranceDetailsAry = array();
                $insuranceDetailsAry = $kInsurance->getLargestInsuranceBuyRates(__INSURANCE_SELL_RATE_TYPE__,true,$idInsuranceBuyRate); 

                $fLargetSellPrice = $insuranceDetailsAry['fMaxInsurancePriceUSD'];
                $idInsuranceRate = $insuranceDetailsAry['id'];

                /*
                * Max amount to be insuranced = Max sell price - total booking price
                */
                $fMaxBookingAmountToBeInsurancedUSD = $fLargetSellPrice - $fTotalBookingPriceUsd ; 
                /*
                * geting exchange rate for goods currency
                */

                $idInsuranceCurrency = $this->idInsuranceCurrency ; 
                if($idInsuranceCurrency==1)
                {
                    $this->idGoodsInsuranceCurrency = 1;
                    $this->szGoodsInsuranceCurrency = 'USD';
                    $this->fGoodsInsuranceExchangeRate = 1; 

                    $this->fGoodsInsurancePriceUSD = $this->fValueOfGoods ;

                    $fMaxBookingAmountToBeInsuranced = $fMaxBookingAmountToBeInsurancedUSD ;
                }
                else
                {
                    $resultAry = $kWarehouseSearch->getCurrencyDetails($idInsuranceCurrency);		

                    $this->idGoodsInsuranceCurrency = $resultAry['idCurrency'];
                    $this->szGoodsInsuranceCurrency = $resultAry['szCurrency'];
                    $this->fGoodsInsuranceExchangeRate = $resultAry['fUsdValue'];

                    $this->fGoodsInsurancePriceUSD = $this->fValueOfGoods * $this->fGoodsInsuranceExchangeRate  ; 
                    if($this->fGoodsInsuranceExchangeRate>0)
                    {
                        $fMaxBookingAmountToBeInsuranced = ($fMaxBookingAmountToBeInsurancedUSD/$this->fGoodsInsuranceExchangeRate);
                    } 
                    else
                    {
                        $fMaxBookingAmountToBeInsuranced = 0;
                    }
                }

                //echo "<br><br> booking: ".$fTotalBookingPriceUsd." Goods: ".  $this->fGoodsInsurancePriceUSD." exc ".$this->fGoodsInsuranceExchangeRate." val goods ".$this->fValueOfGoods ;
                $fTotalBookingAmountToBeInsurancedUsd = $fTotalBookingPriceUsd + $this->fGoodsInsurancePriceUSD ;

                /*
                * If max amount to be insured is greater then max sell rate then we gives an error
                */ 
                //echo " usd val ".$fTotalBookingAmountToBeInsurancedUsd." lrg price ".$fLargetSellPrice ;
                if($fTotalBookingAmountToBeInsurancedUsd > $fLargetSellPrice)
                {   
                    /*
                    * Rounding down to nearest 1000
                    */
                    if($fMaxBookingAmountToBeInsuranced>0)
                    {
                        $fMaxBookingAmountToBeInsuranced = floor((float)$fMaxBookingAmountToBeInsuranced/1000) * 1000 ;
                        $fMaxBookingAmountToBeInsuranced = number_format((float)$fMaxBookingAmountToBeInsuranced);
                    }
                    else
                    {
                        $fMaxBookingAmountToBeInsuranced = 0 ;
                    }			
                    $this->iInsuranceOtherErrors = 2;
                    $this->addError('fValueOfGoods_1',t($this->t_base.'fields/maximum_insurance_value')." ".$this->szGoodsInsuranceCurrency." ".$fMaxBookingAmountToBeInsuranced); 
                }
                else
                {
                    /*
                    * Fetching nearest sell rate.
                    */

                    $insuranceDetailsAry = array();
                    $insuranceDetailsAry = $kInsurance->getNearestInsuranceSellRate(__INSURANCE_SELL_RATE_TYPE__,$fTotalBookingAmountToBeInsurancedUsd,$idInsuranceBuyRate);

                    if(!empty($insuranceDetailsAry))
                    {
                        $idInsuranceRate = $insuranceDetailsAry['id'];
                        $kInsurance->loadInsuranceRates(__INSURANCE_SELL_RATE_TYPE__,$idInsuranceRate);
                        if($kInsurance->idInsuranceCurrency=='1')
                        {
                            $kInsurance->fInsuranceExchangeRate='1.000';
                        }
                        else
                        {
                            $kWarehouseSearch = new cWHSSearch();                                          
                            $resultAry = $kWarehouseSearch->getCurrencyDetails($kInsurance->idInsuranceCurrency); 
                            $fInsuranceExchangeRate = $resultAry['fUsdValue']; 
                            $kInsurance->fInsuranceExchangeRate=$fInsuranceExchangeRate;
                        } 
                        $idCalculationInsuranceCurrency = $kInsurance->idInsuranceCurrency; 
                        if( $kInsurance->idInsuranceMinCurrency=='1')
                        {
                            $kInsurance->fInsuranceMinExchangeRate='1.000';
                        }
                        else
                        {
                              $kWarehouseSearch = new cWHSSearch();                                          
                              $resultAry = $kWarehouseSearch->getCurrencyDetails($kInsurance->idInsuranceMinCurrency); 
                              $fInsuranceMinExchangeRate = $resultAry['fUsdValue']; 
                              $kInsurance->fInsuranceMinExchangeRate=$fInsuranceMinExchangeRate;
                        }

                        $fTotalInsuranceCostUsd = ($fTotalBookingAmountToBeInsurancedUsd * $kInsurance->fInsuranceRate)/100;

                        if($kInsurance->fInsuranceExchangeRate>0)
                        {
                            $fTotalInsuranceCost =  ( $fTotalInsuranceCostUsd/$kInsurance->fInsuranceExchangeRate );
                        }
                        else
                        {
                            $fTotalInsuranceCost = 0 ;
                        }  
                        $fTotalInsuranceMinCost = $kInsurance->fInsuranceMinPrice ;
                        $fTotalInsuranceMinCostUsd = $kInsurance->fInsuranceMinPrice * $kInsurance->fInsuranceMinExchangeRate ;
                        $iMinrateApplied = 0;
                        if($fTotalInsuranceMinCostUsd>$fTotalInsuranceCostUsd)
                        {
                            $fTotalInsuranceCost = $fTotalInsuranceMinCost ;
                            $fTotalInsuranceCostUsd = $fTotalInsuranceMinCostUsd ;
                            $idCalculationInsuranceCurrency = $kInsurance->idInsuranceMinCurrency ;
                            $iMinrateApplied = 1;
                        }   
                        if($idCalculationInsuranceCurrency==$postSearchAry['idCurrency'])
                        {
                            $fTotalInsuranceCostForBookingCustomerCurrency = $fTotalInsuranceCost ;
                        }
                        else if($postSearchAry['fExchangeRate']!=0)
                        {
                            $fTotalInsuranceCostForBookingCustomerCurrency = round((float)($fTotalInsuranceCostUsd / $postSearchAry['fExchangeRate']),2);
                        }
                        else
                        {
                            $fTotalInsuranceCostForBookingCustomerCurrency = 0 ;
                        }

                        if($data['iIncludeInsuranceWithZeroCargo']==1 && $this->fValueOfGoods==0)
                        {
                            /*
                            * For manual booking, If insurance: Yes and cargo value is updated as 0 then we keep all other fields but only remove sell rate and buy rate
                            */
//                                $fTotalInsuranceCostForBookingCustomerCurrency = 0;
//                                $fTotalInsuranceCost = 0;
//                                $fTotalInsuranceCostUsd = 0;
                        }

                        $this->fTotalInsuranceCostForBookingCustomerCurrencyApi = $fTotalInsuranceCostForBookingCustomerCurrency;
                        $updateBookingAry = array();
                        $updateBookingAry['iInsuranceIncluded'] = 1 ;
                        $updateBookingAry['fValueOfGoods'] = $this->fValueOfGoods ;
                        $updateBookingAry['fValueOfGoodsUSD'] = $this->fGoodsInsurancePriceUSD ;
                        $updateBookingAry['idGoodsInsuranceCurrency'] = $this->idGoodsInsuranceCurrency ; 
                        $updateBookingAry['szGoodsInsuranceCurrency'] = $this->szGoodsInsuranceCurrency ;
                        $updateBookingAry['fGoodsInsuranceExchangeRate'] = $this->fGoodsInsuranceExchangeRate ;
                        $updateBookingAry['fTotalInsuranceCostForBooking'] = $fTotalInsuranceCost ;
                        $updateBookingAry['fTotalInsuranceCostForBookingCustomerCurrency'] = $fTotalInsuranceCostForBookingCustomerCurrency ;
                        $updateBookingAry['fTotalInsuranceCostForBookingUSD'] = $fTotalInsuranceCostUsd ;
                        $updateBookingAry['fTotalAmountInsured'] = $fTotalBookingAmountToBeInsurancedUsd ; 
                        $updateBookingAry['iMinrateApplied'] = $iMinrateApplied;

                        $updateBookingAry['idInsuranceRate'] = $kInsurance->id;
                        $updateBookingAry['fInsuranceUptoPrice'] = $kInsurance->fInsuranceUptoPrice; 
                        $updateBookingAry['idInsuranceUptoCurrency'] = $kInsurance->idInsuranceCurrency; 
                        $updateBookingAry['szInsuranceUptoCurrency'] = $kInsurance->szInsuranceCurrency;
                        $updateBookingAry['fInsuranceUptoExchangeRate'] = $kInsurance->fInsuranceExchangeRate;
                        $updateBookingAry['fInsuranceRate'] = $kInsurance->fInsuranceRate;
                        $updateBookingAry['fInsuranceMinPrice'] = $kInsurance->fInsuranceMinPrice;
                        $updateBookingAry['idInsuranceMinCurrency'] = $kInsurance->idInsuranceMinCurrency;

                        $updateBookingAry['szInsuranceMinCurrency'] = $kInsurance->szInsuranceMinCurrency;
                        $updateBookingAry['fInsuranceMinExchangeRate'] = $kInsurance->fInsuranceMinExchangeRate;
                        $updateBookingAry['fInsuranceUptoPriceUSD'] = $kInsurance->fInsurancePriceUSD;
                        $updateBookingAry['fInsuranceMinPriceUSD'] = $kInsurance->fInsuranceMinPriceUSD;
                        $updateBookingAry['iInsuranceUpdatedFlag'] = 1; 
                        $updateBookingAry['iUpdateInsuranceWithBlankValue'] = $iUpdateInsuranceWithBlankValue; 


                        /*
                        * Calculating buy rate for booking  
                        */ 
                        $insuranceDetailsAry = array();
                        $insuranceDetailsAry = $buyRateListArys;

                        if(!empty($insuranceDetailsAry))
                        {
                            $idInsuranceRate = $insuranceDetailsAry['id'];
                            $kInsurance->getInsuranceDetails($idInsuranceRate,true);

                            $fTotalInsuranceCostUsd = ($fTotalBookingAmountToBeInsurancedUsd * $kInsurance->fInsuranceRate)/100;    
                            $idCalculationInsuranceCurrency_buyrate = $kInsurance->idInsuranceCurrency; 

                            if($kInsurance->fInsuranceExchangeRate>0)
                            {
                                $fTotalInsuranceCost = ($fTotalInsuranceCostUsd/$kInsurance->fInsuranceExchangeRate);
                            }
                            else
                            {
                                $fTotalInsuranceCost = 0;
                            } 

                            $fTotalInsuranceMinCost = $kInsurance->fInsuranceMinPrice ;
                            $fTotalInsuranceMinCostUsd = $kInsurance->fInsuranceMinPrice * $kInsurance->fInsuranceMinExchangeRate ;

                            if($fTotalInsuranceMinCostUsd>$fTotalInsuranceCostUsd)
                            {
                                $fTotalInsuranceCost = $fTotalInsuranceMinCost ;
                                $fTotalInsuranceCostUsd = $fTotalInsuranceMinCostUsd ;
                                $idCalculationInsuranceCurrency_buyrate = $kInsurance->idInsuranceMinCurrency; 
                            }  

                            if($idCalculationInsuranceCurrency_buyrate == $postSearchAry['idCurrency'])
                            {
                                $fTotalInsuranceCostForBookingCustomerCurrency = $fTotalInsuranceCost ;
                            }
                            else if($postSearchAry['fExchangeRate']!=0)
                            {
                                $fTotalInsuranceCostForBookingCustomerCurrency = ceil((float)($fTotalInsuranceCostUsd / $postSearchAry['fExchangeRate']));
                            }
                            else
                            {
                                $fTotalInsuranceCostForBookingCustomerCurrency = 0 ;
                            } 

                            if($data['iIncludeInsuranceWithZeroCargo']==1 && $this->fValueOfGoods==0)
                            {
                                /*
                                * For manual booking, If insurance: Yes and cargo value is updated as 0 then we keep all other fields but only remove sell rate and buy rate
                                */
//                                    $fTotalInsuranceCostForBookingCustomerCurrency = 0 ;
//                                    $fTotalInsuranceCost = 0;
//                                    $fTotalInsuranceCostUsd = 0;
                            }

                            $updateBookingAry['idInsuranceRate_buyRate'] = $kInsurance->id;
                            $updateBookingAry['fInsuranceUptoPrice_buyRate'] = $kInsurance->fInsuranceUptoPrice; 
                            $updateBookingAry['idInsuranceUptoCurrency_buyRate'] = $kInsurance->idInsuranceCurrency; 
                            $updateBookingAry['szInsuranceUptoCurrency_buyRate'] = $kInsurance->szInsuranceCurrency;
                            $updateBookingAry['fInsuranceUptoExchangeRate_buyRate'] = $kInsurance->fInsuranceExchangeRate;
                            $updateBookingAry['fInsuranceRate_buyRate'] = $kInsurance->fInsuranceRate;
                            $updateBookingAry['fInsuranceMinPrice_buyRate'] = $kInsurance->fInsuranceMinPrice;
                            $updateBookingAry['idInsuranceMinCurrency_buyRate'] = $kInsurance->idInsuranceMinCurrency; 
                            $updateBookingAry['szInsuranceMinCurrency_buyRate'] = $kInsurance->szInsuranceMinCurrency;
                            $updateBookingAry['fInsuranceMinExchangeRate_buyRate'] = $kInsurance->fInsuranceMinExchangeRate;
                            $updateBookingAry['fInsuranceUptoPriceUSD_buyRate'] = $kInsurance->fInsurancePriceUSD;
                            $updateBookingAry['fInsuranceMinPriceUSD_buyRate'] = $kInsurance->fInsuranceMinPriceUSD;

                            $updateBookingAry['fTotalInsuranceCostForBooking_buyRate'] = $fTotalInsuranceCost ;
                            $updateBookingAry['fTotalInsuranceCostForBookingCustomerCurrency_buyRate'] = $fTotalInsuranceCostForBookingCustomerCurrency ;
                            $updateBookingAry['fTotalInsuranceCostForBookingUSD_buyRate'] = $fTotalInsuranceCostUsd ;
                        } 

                        $this->arrInsuranceUpdateAry = $updateBookingAry; 
                        if($bParnerApi || $bQuickQuote)
                        {
                            return $updateBookingAry;
                        }
                        else
                        {
                            /*
                            * updating booking from hold to draft booking 
                            */  
                            foreach($updateBookingAry as $key=>$value)
                            {
                                $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                            } 
                            $update_query = rtrim($update_query,",");   
                            if($this->updateDraftBooking($update_query,$idBooking))
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            } 
                        } 
                    }
                }
            }
            else
            {
                //Removing insurance from system 
               $updateBookingAry = array();
               $updateBookingAry['iInsuranceIncluded'] = 0; 
               $updateBookingAry['iInsuranceUpdatedFlag'] = 1; 
               if($data['iUpdateInsuranceChoice']==1)
               {
                   $updateBookingAry['iInsuranceChoice'] = 2; 
                   $res_ary['iInsuranceMandatory'] = 0; 
               } 

               /*
               * updating booking from hold to draft booking 
               */  

               foreach($updateBookingAry as $key=>$value)
               {
                       $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
               } 
               $update_query = rtrim($update_query,",");

               if($this->updateDraftBooking($update_query,$idBooking))
               {
                   return true;
               }
               else
               {
                   return false;
               } 
           }
        }
    }
    
    function addVatApplicationData($data)
    {
        if(!empty($data))
        { 
            $query="
                INSERT INTO
                    ".__DBC_SCHEMATA_VAT_APPLICATION_DATA__."
                ( 
                    idCountryFrom,
                    idCountryTo
                )
                VALUES
                ( 
                    '".(int)$data['idCountryFrom']."',
                    '".(int)$data['idCountryTo']."' 
                )
            "; 
            
            if($result = $this->exeSQL( $query ))
            {
                return true;
            }
            else
            {
                return false;
            }  
        }
    }
    
    function getTransportecaVat($data)
    {
        if(!empty($data))
        {
            $idCustomerCountry = $data['idCustomerCountry'];
            $idOriginCountry = $data['idOriginCountry'];
            $idDestinationCountry = $data['idDestinationCountry'];
            
            $kConfig = new cConfig();
            $kConfig->loadCountry($idCustomerCountry);
            
            $fVATRate = $kConfig->fVATRate;
            $szVATRegistration = $kConfig->szVATRegistration; 
            if($fVATRate>0 && !empty($szVATRegistration))
            {
                if($this->checkForwarderCountryExists($idOriginCountry,$idDestinationCountry))
                {
                    return $fVATRate;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        } 
    }
    
    function updateVatonBooking($idBooking,$bPartnerApi=false)
    {
        if($idBooking>0)
        {
            $kBooking = new cBooking();
            $postSearchAry = array();
            $postSearchAry = $kBooking->getBookingDetails($idBooking);
            $iPrivateShipping = $postSearchAry['iPrivateShipping'];
            
            if($bPartnerApi)
            {
                $idCustomer = $postSearchAry['idUser']; 
            }
            else
            {
                $idCustomer = $_SESSION['user_id']; 
            } 
            
            $kUser = new cUser();
            $kUser->getUserDetails($idCustomer);
            $idCustomerAccountCountry = $kUser->szCountry;
            $ret_ary = array();
            
            if(!$bPartnerApi && ($iPrivateShipping==$kUser->iPrivate) && ($idCustomerAccountCountry==$postSearchAry['szCustomerCountry']))
            {
                //Value for Private Flag same as we have on Booking then No need for any changes.
                return true;
            }
            else
            {
                if($kUser->iPrivate==1)
                { 
                    $vatInputAry = array();
                    $vatInputAry['idCustomerCountry'] = $idCustomerAccountCountry;
                    $vatInputAry['idOriginCountry'] = $postSearchAry['idOriginCountry'];
                    $vatInputAry['idDestinationCountry'] = $postSearchAry['idDestinationCountry'];

                    $fApplicableVatRate = $this->getTransportecaVat($vatInputAry);  
                    $fTotalVat = 0;
                    if($fApplicableVatRate>0)
                    {
                        $fTotalVat = round((float)$postSearchAry['fTotalPriceCustomerCurrency']) * $fApplicableVatRate * .01;
                    }

                    $ret_ary['fVATPercentage'] = $fApplicableVatRate;
                    $ret_ary['fTotalVat'] = $fTotalVat ; 
                    $ret_ary['iPrivateShipping'] = 1;
                }
                else
                {
                    $ret_ary['fTotalVat'] = 0;
                    $ret_ary['fVATPercentage'] = 0;
                    $ret_ary['iPrivateShipping'] = 0;
                } 
                if(!empty($ret_ary))
                { 
                    $update_query = '';
                    foreach($ret_ary as $key=>$value)
                    {
                        $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    } 
                    $update_query = rtrim($update_query,","); 
                    if($kBooking->updateDraftBooking($update_query,$idBooking))
                    { 
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            } 
        }
    }
    
    function set_idForwarderCountry( $value )
    {
        $this->idForwarderCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idForwarderCountry", t($this->t_base_courier.'fields/country'), false, false, true );
    }
    
    function set_idFromCountry( $value )
    {
        $this->idFromCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idCountryForm", t($this->t_base_courier.'fields/from_country'), false, false, true );
    }
    
    function set_idToCountry( $value )
    {
        $this->idToCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idToCountry", t($this->t_base_courier.'fields/to_country'), false, false, true );
    }
    function set_szProduct( $value ,$flag=true)
    {
        $this->szProduct = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szProduct", t($this->t_base_courier.'fields/product'), false, false, $flag );
    }
    function set_idForwarder( $value ,$flag=true)
    {
        $this->idForwarder = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idForwarder", t($this->t_base_courier.'fields/forwarder_id'), false, false, $flag );
    }
    function set_szCargo( $value ,$flag=true)
    {
        $this->szCargo = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCargo", t($this->t_base_courier.'fields/cargo'), false, false, $flag );
    }
    function set_idCurrency( $value ,$flag=true)
    {
        $this->idCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idCurrency", t($this->t_base_courier.'fields/currency'), false, false, $flag );
    }
    function set_fManualFee( $value ,$flag=true)
    {
        $this->fManualFee = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fManualFee", t($this->t_base_courier.'fields/manual_fee'), false, false, $flag );
    }
    function set_idManualFee( $value)
    {
        $this->idManualFee = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idManualFee", t($this->t_base_courier.'fields/manual_fee_id'), false, false, false );
    } 
}
?>