<?php
/**
 * This file is the container for all upload bulk services related functionality.
 * All functionality related to upload bulk services details should be contained in this class.
 *
 * uploadBulkService.class.php
 *
 * @copyright Copyright (C) 2012 Transport-ece
 * @author Ashish
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/forwarders/listExcelData.php" );

class cUploadBulkService extends cDatabase
{
	var $t_base = "BulkUpload/";
	var $t_base_approve="HaulaugePricing/";
	var $szRecordType;
	var $FileCount;
	var $rowCountUploaded;
	var $idForwarder;
	var $rowErrorCount;
	var $errorCount;
	function __construct()
	{
		parent::__construct();
		return true;
	}
	
	
	function uploadBulkData($data,$files='')
	{	
		$this->set_szFileUploadType(trim(sanitize_all_html_input($data['bulkFileType'])));
		$this->set_szComment(trim(sanitize_all_html_input($data['szComment'])));
		
		if($data['file_name']!= '' )
		{	$fileArr = array();
			$fileArr = explode(';',$data['file_name']);
		}
		
		if($fileArr != array())
		{
			$originalTempFile = array();
			foreach($fileArr as $fileArrs)
			{
				$originalTempFile[]  = explode('#####',$fileArrs);
				
			}
		}
		if($originalTempFile[0]=='')
		{
			$this->addError( "file_name" , t($this->t_base.'messages/upload_file_is_required'));
		}
		if($originalTempFile[0]!='')
		{
			
			$filecount=count($originalTempFile);
			if($filecount>__MAX_BULK_UPLOAD_FILE__)
			{
				$this->addError( "file_name" , t($this->t_base.'messages/you_can_uploald_max')." ".__MAX_BULK_UPLOAD_FILE__." ".t($this->t_base.'messages/files_at_a_time'));
			}
			
			$fileTypes = array('jpg','jpeg','gif','png','pdf','doc','docx','xlsx','pptx');
			if(!empty($originalTempFile))
			{
				for($i=0; $i<$filecount;++$i)
				{
					//print_r($originalTempFile[$i]);
					$fileParts = pathinfo($originalTempFile[$i][0]);
					//echo $fileParts;
					if(!in_array(strtolower($fileParts['extension']),$fileTypes)) 
					{
						$this->addError( "file_name" , ".".$fileParts['extension']." ".t($this->t_base.'messages/upload_invalid_file'));
						return false;
					}
				}
			}
			
		}
		//exit if error exist.
		if ($this->error === true)
		{
			return false;
		}
		$kForwarderContact = new cForwarderContact();
		if((int)$_SESSION['forwarder_user_id']>0)
		{
			$kForwarderContact->getUserDetails($_SESSION['forwarder_user_id']);
			$idForwarder=$kForwarderContact->idForwarder;
			$kForwarderContact->findForwarderCurrency($idForwarder);
			$iCurrency = $kForwarderContact->iOldCurrency;
			$kForwarderContact->szCurrency = $iCurrency;
			$kForwarderContact->findCurrencyName();
			$szCurrency = $kForwarderContact->szCurrencyName;
			$name=$kForwarderContact->szFirstName." ".$kForwarderContact->szLastName;
			$recordType = $this->returnRecordType($this->szFileUploadType);
		}
		$query="
			INSERT INTO
				".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
			(
				idForwarder,
				idForwarderContact,
				szForwarderContactName,
				iFileType,
				iFileCount,
				szRecordType,
				dtSubmitted,
				szCurrency,
				iCurrency,
				iStatus,
				iActive
			)
				VALUES
			(
				'".(int)$kForwarderContact->idForwarder."',
				'".(int)$kForwarderContact->id."',
				'".mysql_escape_custom($name)."',
				'".mysql_escape_custom($this->szFileUploadType)."',
				'".(int)$filecount."',
				'".$recordType."',
				NOW(),
				'".$szCurrency."',
				".(int)$iCurrency.",
				'".(int)__DATA_SUBMITTED__."',
				'1'
			)
		";
		if($result=$this->exeSQL($query))
		{
			
			$id= $this->iLastInsertID ;
			$kForwarder = new cForwarder();
			$kForwarder->load($idForwarder);
			$comment=$name.", ".$kForwarder->szDisplayName.", ". date('j. F Y',strtotime(date('Y-m-d')))."<br /><em>".$this->szComment."</em>";
			$query="
				INSERT INTO
					".__DBC_SCHEMATA_BULK_UPLOAD_COMMENT__."
				(
					idBulkUploadService,
					szComment,
					dtComment
				)
					VALUES
				(
					'".(int)$id."',
					'".mysql_escape_custom($comment)."',
					NOW()
				)
			";
			$result=$this->exeSQL($query);
			
			
			if(!empty($originalTempFile))
			{
				$this->FileCount=$filecount;
				for($i=0; $i<$filecount;++$i)
				{
					$tempFile='';
					$fileSavedName='';
					//$size=($files['file_upload']['size'][$i]/1024);
					//echo $size."test<br/>";
					$tempFile=$originalTempFile[$i][0];
					$tempFileName=$originalTempFile[$i][1];
					$size=round($originalTempFile[$i][2],2);
					$fileParts = pathinfo($originalTempFile[$i][0]);
					//$filenameArr=explode(".",$originalTempFile[$i][0]);
					$newFilename=str_replace(" ","-",$fileParts['filename']);
					$fileSavedName=$newFilename."-".$kForwarderContact->idForwarder."-".date('dmYHis').".".$fileParts['extension'];
					$targetFile=__UPLOAD_FORWARDER_BULK_SERVICES__."/".$fileSavedName;
					$originFile=__UPLOAD_FORWARDER_BULK_SERVICES_TEMP__."/".$tempFileName;
					rename($originFile,$targetFile);
					@unlink($originFile);
					$this->uploadFiles($id,$fileSavedName,$tempFile,$size);
				}
			}
			
			//send mail to all active users of management module
			$this->sendEmailUsersManagement($id);
			return true;
		}
		else
		{
			return 0;
		}
	}	
	function uploadFileContent($file)
	{
		if($files['file_upload']['name'][0]=='')
		{
			$this->addError( "file_name" , t($this->t_base.'messages/upload_file_is_required'));
		}
		if($files['file_upload']['name'][0]!='')
		{
			
			$filecount=count($files['file_upload']['name']);
			if($filecount>__MAX_BULK_UPLOAD_FILE__)
			{
				$this->addError( "file_name" , t($this->t_base.'messages/you_can_uploald_max')." ".__MAX_BULK_UPLOAD_FILE__." ".t($this->t_base.'messages/files_at_a_time'));
			}
			
			$fileTypes = array('jpg','jpeg','gif','png','pdf','doc','docx','xlsx');
			if(!empty($files['file_upload']['name']))
			{
				for($i=0; $i<$filecount;++$i)
				{
					$fileParts = pathinfo($files['file_upload']['name'][$i]);
					if(!in_array(strtolower($fileParts['extension']),$fileTypes)) 
					{
						$this->addError( "file_name" , ".".$fileParts['extension']." ".t($this->t_base.'messages/upload_invalid_file'));
						return false;
					}
				}
			}
		}
	
	}
	function uploadFileContentNew($data,$file)
	{
		if($data['file_name']!= '' )
		{	$fileArr = array();
			$fileArr = explode(',',$data['file_name']);
		}
		
		if($fileArr != array())
		{
			$originalTempFile = array();
			foreach($fileArr as $fileArrs)
			{
				$originalTempFile[]  = explode('|||||',$fileArrs);
				
			}
		}
		if($originalTempFile[0]=='')
		{
			$this->addError( "file_name" , t($this->t_base.'messages/upload_file_is_required'));
		}
		if($originalTempFile[0]!='')
		{
			
			$filecount=count($originalTempFile);
			if($filecount>__MAX_BULK_UPLOAD_FILE__)
			{
				$this->addError( "file_name" , t($this->t_base.'messages/you_can_uploald_max')." ".__MAX_BULK_UPLOAD_FILE__." ".t($this->t_base.'messages/files_at_a_time'));
			}
			
			$fileTypes = array('jpg','jpeg','gif','png','pdf','doc','docx','xlsx');
			if(!empty($originalTempFile))
			{
				for($i=0; $i<$filecount;++$i)
				{
					//$fileParts = pathinfo($originalTempFile[$i]);
					if(!in_array(strtolower($fileParts['extension']),$fileTypes)) 
					{
						$this->addError( "file_name" , ".".$fileParts['extension']." ".t($this->t_base.'messages/upload_invalid_file'));
						return false;
					}
				}
			}
		}
	
	}
	function returnRecordType($fileType)
	{	echo $fileType;
		$fileType = trim($fileType);
		$recordType = NULL;
		SWITCH ($fileType)
		{
			CASE 'CFS locations':
			{
                            $recordType = 'locations';
                            BREAK;
			}
                        CASE 'Airport Warehouses':
			{
                            $recordType = 'locations';
                            BREAK;
			}
			CASE 'LCL Services':
			{
                            $recordType = 'services';
                            BREAK;
			}
                        CASE 'Airfreight Services':
			{
                            $recordType = 'services';
                            BREAK;
			}
			CASE 'Haulage':
			{
                            $recordType = 'lines';
                            BREAK;
			}
			CASE 'Customs Clearance':
			{
                            $recordType = 'lines';
                            BREAK;
			}
			DEFAULT :
			{
                            $recordType = 'unKnown';
                            BREAK;
			}
		}
		return $recordType;
	}
	function uploadFiles($bulkUploadId,$fileSavedName,$szOriginalName,$size)
	{
		$query="
			INSERT INTO
				".__DBC_SCHEMATA_BULK_UPLOAD_FILES__."
			(
				idBulkUploadService,
				szFileName,
				szOriginalName,
				iSize
			)
				VALUES
			(
				'".(int)$bulkUploadId."',
				'".mysql_escape_custom($fileSavedName)."',
				'".mysql_escape_custom($szOriginalName)."',
				'".(float)$size."'
			)
		";
		$result=$this->exeSQL($query);
	}
	
	function getAllDataNotApprovedByForwarder($flag)
	{
		$sql='';
		$order='';
		if($flag=="bottomdata")
		{
                    $sql .="
                        WHERE
                        (
                            iStatus='".(int)__DATA_SUBMITTED__."'
                        OR
                            iStatus='".(int)__COST_APPROVED_BY_FORWARDER__."'	
                        OR
                            iStatus='".(int)__COST_AWAITING_APPROVAL__."'
                        )
                    ";
                    $order .=" ORDER BY dtSubmitted ASC ";
		}
		else if($flag=="topdata")
		{
                    $sql .="
                        WHERE
                            (
                                iStatus='".(int)__MANAGEMENT_COMPLETE_APPROVAL__."'
                            OR
                                iStatus='".(int)__DATA_REVIEWED_BY_FORWARDER__."'
                            )
                    ";
                    $order .=" ORDER BY dtCompleted ASC ";
		}
		$sql_forwarder="";
		if((int)$_SESSION['forwarder_user_id']>0)
		{ 
                    $kForwarderContact = new cForwarderContact();
                    $kForwarderContact->getUserDetails($_SESSION['forwarder_user_id']);
                    $idForwarder=$kForwarderContact->idForwarder; 
                    if($flag!='')
                    {
                        $sql_forwarder .=" AND";
                    }
                    else
                    {
                        $sql_forwarder .=" WHERE";
                    } 
                    $sql_forwarder .=" bulk.idForwarder='".(int)$idForwarder."' ";
		}
		$query="
			SELECT
                            bulk.id,
                            bulk.idForwarder,
                            bulk.idForwarderContact,
                            bulk.szForwarderContactName,
                            bulk.iFileType,
                            bulk.iFileCount,
                            bulk.szCurrency,
                            bulk.fProcessingCost,
                            bulk.szRecordType,
                            bulk.iTotalRecords,
                            bulk.iStatus,
                            bulk.dtSubmitted,
                            bulk.dtApproved,
                            bulk.dtCompleted,
                            bulk.iProcessingTime,
                            bulk.szComment,
                            bulk.dtCostApproval,
                            bulk.dtExpected,
                            bulk.szCostApprovalBy,
                            bulk.iActualRecords,
                            fwd.szDisplayName
			FROM
                            ".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__." AS bulk	
                        INNER JOIN
                            ".__DBC_SCHEMATA_FORWARDERS__." as fwd
                        ON (bulk.idForwarder = fwd.id ) 
                            ".$sql."
                            ".$sql_forwarder."
			AND
                            bulk.iActive = 1	
			".$order."		
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				while($row=$this->getAssoc($result))
				{
					$res_arr[]=$row;
				}
				
				return $res_arr;
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	function getAllDataNotApprovedByAdmin($flag)
	{
		$sql='';
		$order='';
		if($flag=="topdata")
		{
			$sql .="
				WHERE
					(
						iStatus='".(int)__DATA_SUBMITTED__."'
					)
			";
			$order .="
				ORDER BY
					dtSubmitted ASC
			";
		}
		else if($flag=="bottomdata")
		{
			$sql .="
				WHERE
					iStatus='".(int)__COST_AWAITING_APPROVAL__."'
			";
			$order .="
				ORDER BY
					dtApproved ASC
			";
		}
		else if($flag=="worktopdata")
		{
			$sql .="
				WHERE
					iStatus='".(int)__COST_APPROVED_BY_FORWARDER__."'
			";
			$order .="
				ORDER BY
					dtCostApproval ASC
			";
		}
		else if($flag=="workbottomdata")
		{
			$sql .="
				WHERE
					iStatus='".(int)__MANAGEMENT_COMPLETE_APPROVAL__."'
			";
			$order .="
				ORDER BY
					dtCompleted ASC
			";
		}
		$query="
			SELECT
				bulk.id,
				bulk.idForwarder,
				bulk.idForwarderContact,
				bulk.szForwarderContactName,
				bulk.iFileType,
				bulk.iFileCount,
				bulk.szCurrency,
				bulk.fProcessingCost,
				bulk.szRecordType,
				bulk.iTotalRecords,
				bulk.iStatus,
				bulk.dtSubmitted,
				bulk.dtApproved,
				bulk.dtCompleted,
				bulk.iProcessingTime,
				bulk.szComment,
				bulk.dtCostApproval,
				bulk.dtExpected,
				bulk.szCostApprovalBy,
				bulk.iActualRecords,
				fwd.szDisplayName
			FROM
				".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__." AS bulk	
				INNER JOIN
				".__DBC_SCHEMATA_FORWARDERS__." as fwd
				ON (bulk.idForwarder = fwd.id ) 
				".$sql."
			AND
				bulk.iActive = 1	
			".$order."		
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				while($row=$this->getAssoc($result))
				{
					$res_arr[]=$row;
				}
				
				return $res_arr;
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	function load($id)
	{
		if($id>0)
		{
			$query="
				SELECT
					id,
					idForwarder,
					idForwarderContact,
					szForwarderContactName,
					iFileType,
					szCurrency,
					iCurrency,
					szRecordType,
					iFileCount,
					fProcessingCost,
					iTotalRecords,
					iActualRecords,
					iStatus,
					dtSubmitted,
					dtApproved,
					dtCompleted,
					iProcessingTime,
					szComment,
					dtCostApproval,
					dtExpected,
					szCostApprovalBy,
					iCompleted
				FROM
					".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
				WHERE
					id='".(int)$id."'
				AND
					iActive = 1	
			";
			//echo $query;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					
					$row=$this->getAssoc($result);
					$this->id=sanitize_all_html_input(trim($row['id']));
					$this->fProcessingCost=sanitize_all_html_input(trim($row['fProcessingCost']));
					$this->iTotalRecords=sanitize_all_html_input(trim($row['iTotalRecords']));
					$this->iStatus=sanitize_all_html_input(trim($row['iStatus']));
					$this->dtSubmitted=sanitize_all_html_input(trim($row['dtSubmitted']));
					$this->dtApproved=sanitize_all_html_input(trim($row['dtApproved']));
					$this->dtCompleted=sanitize_all_html_input(trim($row['dtCompleted']));					
					$this->iProcessingTime=sanitize_all_html_input(trim($row['iProcessingTime']));					
					$this->szComment=sanitize_all_html_input(trim($row['szComment']));
					$this->iFileCount=sanitize_all_html_input(trim($row['iFileCount']));
					$this->iFileType=sanitize_all_html_input(trim($row['iFileType']));
					$this->szForwarderContactName=sanitize_all_html_input(trim($row['szForwarderContactName']));
					$this->idForwarderContact=sanitize_all_html_input(trim($row['idForwarderContact']));
					$this->idForwarder=sanitize_all_html_input(trim($row['idForwarder']));
					$this->dtExpected=sanitize_all_html_input(trim($row['dtExpected']));
					$this->szCostApprovalBy=sanitize_all_html_input(trim($row['szCostApprovalBy']));
					$this->iCurrency=sanitize_all_html_input(trim($row['iCurrency']));
					$this->szCurrency=sanitize_all_html_input(trim($row['szCurrency']));
					$this->szRecordType=sanitize_all_html_input(trim($row['szRecordType']));
					$this->iActualRecords=sanitize_all_html_input(trim($row['iActualRecords']));
					$this->iCompleted=sanitize_all_html_input(trim($row['iCompleted']));
					$this->dtCostApproval=sanitize_all_html_input(trim($row['dtCostApproval']));
					return $row;
				}
				else
				{
					return array();
				}
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function approvedProcessingCost($id)
	{
            if((int)$id>0)
            {
                $this->load($id);

                $kForwarder = new  cForwarder();
                $kForwarder->load($this->idForwarder);
                if((int)$_SESSION['forwarder_user_id']>0)
                {
                    $comment=$this->szForwarderContactName.", ".$kForwarder->szDisplayName.", ". date('j. F Y')."<br /> <em>Cost of ".$this->szCurrency." ".number_format((float)$this->fProcessingCost,2)." approved.</em>";

                    $kForwarderContact = new cForwarderContact;
                    $kForwarderContact->getUserDetails($_SESSION['forwarder_user_id']);
                    $name=$kForwarderContact->szFirstName." ".$kForwarderContact->szLastName;
                }
                else if((int)$_SESSION['admin_id']>0)
                {
                    $kAdmin= new cAdmin();
                    $kAdmin->getAdminDetails($_SESSION['admin_id']);
                    $comment=$kAdmin->szFirstName." ".$kAdmin->szLastName.", Transporteca, ". date('j. F Y')."<br /><em>Transporteca has decided to process your data free of charge.</em>";
                    $name=$kAdmin->szFirstName." ".$kAdmin->szLastName; 
                }
                //echo $comment;
                $totaldays=($this->iProcessingTime + 1);
                $dtExpected=getWorkingDays($totaldays);
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
                    SET
                        iStatus='".(int)__COST_APPROVED_BY_FORWARDER__."',
                        szCostApprovalBy = '".$name."',
                        dtCostApproval=NOW(),
                        dtExpected='".mysql_escape_custom($dtExpected)."'
                    WHERE
                        id='".(int)$id."'
                    AND
                        iActive = 1	
                ";
                //echo $query;
                if($result=$this->exeSQL($query))
                { 
                    $query="
                        INSERT INTO
                            ".__DBC_SCHEMATA_BULK_UPLOAD_COMMENT__."
                        (
                            idBulkUploadService,
                            szComment,
                            dtComment
                        )
                        VALUES
                        (
                            '".(int)$id."',
                            '".mysql_escape_custom($comment)."',
                            NOW()
                        )
                    ";
                    $result=$this->exeSQL($query);


                    $query = "
                            SELECT
                                szEmail
                            FROM
                                ".__DBC_SCHEMATA_MANAGEMENT__."		
                            WHERE
                                iActive = 1		
                    ";
                    if($result=$this->exeSQL($query))
                    {
                        $profileArr = array();
                        while($row=$this->getAssoc($result))
                        {
                            $profileArr[] = $row;
                        }	
                        if($profileArr!=array())
                        {	
                            $replace_ary['szDisplayName'] = $kForwarder->szDisplayName;
                            $replace_ary['dtExpected'] = date('j. F Y',strtotime($dtExpected));
                            foreach($profileArr as $profArr)
                            {	
                                createEmail(__COST_APPROVAL_BY_FORWARDER_EMAIL__, $replace_ary,$profArr['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$profArr['id'], __STORE_SUPPORT_EMAIL__,false);
                            }
                        }
                    }
                    return true;
                }
                else
                {
                    return false;
                } 
            }
	}
	
	
	function bulkUploadServiceFiles($id)
	{
		if((int)$id>0)
		{
			$query="
				SELECT
					szFileName,
					szOriginalName,
					iSize
				FROM
					".__DBC_SCHEMATA_BULK_UPLOAD_FILES__."
				WHERE
					idBulkUploadService='".(int)$id."'
			";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					while($row=$this->getAssoc($result))
					{
						$res_arr[]=$row;
					}
					return $res_arr;
				}
				else
				{
					return array();
				}
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	function bulkUploadServiceRecords($id)
	{
		if((int)$id>0)
		{
			$query="
				SELECT
					szFileName,
					szActualFileName,
					dtUploaded
				FROM
					".__DBC_SCHEMATA_UPLOAD_FILES_ADMIN__."
				WHERE
					idBulkUploadService='".(int)$id."'
			";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					
					return $row;
				}
				else
				{
					return array();
				}
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function bulkUploadServiceComments($id)
	{
		if((int)$id>0)
		{
			$query="
				SELECT
					buc.szComment,
					buc.dtComment,
					bu.szForwarderContactName,
					bu.idForwarder 
				FROM
					".__DBC_SCHEMATA_BULK_UPLOAD_COMMENT__." AS buc
				INNER JOIN
					".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__." AS bu
				ON
					bu.id=buc.idBulkUploadService
				WHERE
					buc.idBulkUploadService='".(int)$id."'
				AND
					bu.iActive = 1		
				ORDER BY
					buc.dtComment DESC
			";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					while($row=$this->getAssoc($result))
					{
						$res_arr[]=$row;
					}
					return $res_arr;
				}
				else
				{
					return array();
				}
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function addComment($id,$szComment)
	{
		  if((int)$id>0 && $szComment!='')
		  {	
		  		$kWHSSearch= new cWHSSearch();
				$idForwarderContact		=	(int)$_SESSION['forwarder_user_id'];
				$forwarderContactName	=	$kWHSSearch->findForwarderFirstLastName($idForwarderContact); 
				$kForwarderContact	= new 	cForwarderContact();
				$kForwarderContact->getUserDetails($_SESSION['forwarder_user_id']);
				$idForwarder=$kForwarderContact->idForwarder;
				$kForwarder= new cForwarder();
				$kForwarder->load($idForwarder);
				
				if((int)$_SESSION['forwarder_user_id']>0)
				{
					$szNewComment=$forwarderContactName.", ".$kForwarder->szDisplayName.", ". date('j. F Y',strtotime(date('Y-m-d')))."<br /><em>".$szComment."</em>";
				}
		  		else if((int)$_SESSION['admin_id']>0)
		  		{
		  			$kAdmin= new cAdmin();
					$kAdmin->getAdminDetails($_SESSION['admin_id']);
					
					$szNewComment=$kAdmin->szFirstName." ".$kAdmin->szLastName.", Transporteca, ". date('j. F Y',strtotime(date('Y-m-d')))."<br /><em>".$szComment."</em>";
		  		}
					//$szNewComment=$forwarderContactName.", Transporteca, ".date("j. F Y")."<br />".$szComment; 	
				$query="
					INSERT INTO
						".__DBC_SCHEMATA_BULK_UPLOAD_COMMENT__."
					(
						idBulkUploadService,
						szComment,
						dtComment
					)
						VALUES
					(
						'".(int)$id."',
						'".mysql_escape_custom(nl2br($szNewComment))."',
						NOW()
					)
				";
				$result=$this->exeSQL($query);
				return true;
		  }
	}
	
	function updateApprovedStatus($id)
	{
		$query="
			UPDATE
				".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
			SET
				iStatus='".(int)__DATA_APPROVED_BY_FORWARDER__."',
			 	dtApproved=NOW()
			WHERE
				id='".(int)$id."'
			AND
				iActive = 1		
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			
		}
	}
	function getDetailsPricingServicing($id)
	{
		if((int)$id>0)
		{
			$query="
					SELECT
						id,
						iTotalRecords,
						iProcessingTime,
						fProcessingCost,
						szRecordType,
						szCurrency
					FROM	
						".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
					WHERE
						id = ".(int)$id."	
					AND
						iActive = 1		
				";
				//echo $query;
				if($result=$this->exeSQL($query))
				{
					$row = array();
					$row=$this->getAssoc($result);
					if($row!=array())
					{	
						return $row;
					}
					else
					{
						return array();
					}
				}
		  }
	}
	function saveDetailsPricingServicing($updateArr,$id)
	{
		$this->set_id((int)sanitize_all_html_input($id));
		$this->set_iRecords((int)sanitize_all_html_input($updateArr['iRecords']));
		$this->set_szEstimatedTime((int)sanitize_all_html_input($updateArr['szEstimatedTime']));
		$this->set_szProcessingCost((float)sanitize_all_html_input($updateArr['szProcessingCost']));
		
		if((int)$id>0)
		{
			$query="
					UPDATE
						".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
					SET	
						iTotalRecords = ".(int)$this->iRecords.",
						iProcessingTime = ".(int)$this->szEstimatedTime.",
						fProcessingCost = ".(float)$this->szProcessingCost."				
					WHERE
						id = ".(int)$this->id."	
					AND
						iActive = 1		
				";
				//echo $query;
				if($result=$this->exeSQL($query))
				{
					return true;
				}
		  }
	}
	function findSubmitDtataProcessingCost($id)
	{
		$this->set_id((int)$id);
		if((int)$this->id>0)
		{
			$query="
					SELECT
						fProcessingCost
					FROM	
						".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
					WHERE
						id = ".(int)$this->id."	
					AND
						iActive = 1		
				";
			if($result=$this->exeSQL($query))
			{
				$row=$this->getAssoc($result);
				if((float)$row['fProcessingCost']==0.00)
				{
					return __COST_APPROVED_BY_FORWARDER__;
				}
				else if((int)$row['fProcessingCost']>0.00)
				{
					return __COST_AWAITING_APPROVAL__;
				}
			}
		}	
	}
	function findSubmitDtataProcessingCostApproval($id)
	{
		$this->set_id((int)$id);
		if((int)$this->id>0)
		{
			$query="
					SELECT
						fProcessingCost
					FROM	
						".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
					WHERE
						id = ".(int)$this->id."	
					AND
						iActive = 1		
				";
			if($result=$this->exeSQL($query))
			{
				$row=$this->getAssoc($result);
				return $row['fProcessingCost'];
			}
		}	
	}
	function bulkSubmitServiceFiles($id,$status)
	{
		$this->set_id((int)$id);
		if((int)$this->id>0)
		{
		if($status ==__COST_AWAITING_APPROVAL__)
		{	
		$sql = "
				,
				dtApproved = NOW()	
			";
		}
		if($status ==__COST_APPROVED_BY_FORWARDER__)
		{	
			$this->approvedProcessingCost($this->id);
			return true;
			exit;
		}
		
			$query="
					UPDATE
						".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
					SET	
						iStatus =".$status."
					".$sql."					
					WHERE
						id = ".(int)$this->id."
					AND
						iActive = 1			
				";
				if($result=$this->exeSQL($query))
				{	$this->load($this->id);
				
					$kAdmin= new cAdmin();
					$kAdmin->getAdminDetails($_SESSION['admin_id']);
					
					$this->szComment = $kAdmin->szFirstName." ".$kAdmin->szLastName.", Transporteca, ".date("j. F Y")."<br /><em>Cost estimate of ".$this->szCurrency." ".$this->fProcessingCost." submitted for ".$this->iTotalRecords." ".$this->szRecordType.".</em>";
					$query="
						INSERT INTO
							".__DBC_SCHEMATA_BULK_UPLOAD_COMMENT__."
						(
							idBulkUploadService,
							szComment,
							dtComment
						)
							VALUES
						(
							'".(int)$this->id."',
							'".mysql_escape_custom($this->szComment)."',
							NOW()
						)
							";
					if($result=$this->exeSQL($query))
					{
						return true;
					}
				}
		  }
	}
	function bulkMoveBackServiceFiles($id)
	{
		$this->set_id((int)$id);
		if((int)$this->id>0)
		{
			$query="
					UPDATE
						".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
					SET	
						iStatus =".__DATA_SUBMITTED__."				
					WHERE
						id = ".(int)$this->id."	
					AND
						iActive = 1				
				";
				if($result=$this->exeSQL($query))
				{	$this->load($this->id);
				
					$kAdmin= new cAdmin();
					$kAdmin->getAdminDetails($_SESSION['admin_id']);
					
					$this->szComment = $kAdmin->szFirstName." ".$kAdmin->szLastName.", Transporteca, ".date("j. F Y")."<br /><em>Submission has been taken back for review. New estimate will be provided.</em>";
					$query="
						INSERT INTO
							".__DBC_SCHEMATA_BULK_UPLOAD_COMMENT__."
						(
							idBulkUploadService,
							szComment,
							dtComment
						)
							VALUES
						(
							'".(int)$this->id."',
							'".mysql_escape_custom($this->szComment)."',
							NOW()
						)
							";
					if($result=$this->exeSQL($query))
					{
						return true;
					}
				}
		  }
	}
	function bulkMoveBackSubmissionFiles($id)
	{
		
		$this->set_id((int)$id);
		$this->load($this->id);
		if($this->fProcessingCost == 0.00)
		{
			$sql="
				,
					iStatus =".__DATA_SUBMITTED__."
			";	
		}
		else
		{
			$sql="
				,
					iStatus =".__DATA_SUBMITTED__."
			";
		}
		if((int)$this->id>0)
		{
			$query="
					UPDATE
						".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
					SET	
						iActualRecords = 0	
						".$sql."				
					WHERE
						id = ".(int)$this->id."	
					AND
						iActive = 1			
				";
				if($result=$this->exeSQL($query))
				{	$this->load($this->id);
					$this->szComment = $this->szForwarderContactName.", Transporteca, ".date("j. F Y")."<br /><em>Submission was taken back for review by Transporteca Upload Service. New estimate will be provided.</em>";
					$query="
						INSERT INTO
							".__DBC_SCHEMATA_BULK_UPLOAD_COMMENT__."
						(
							idBulkUploadService,
							szComment,
							dtComment
						)
							VALUES
						(
							'".(int)$this->id."',
							'".mysql_escape_custom($this->szComment)."',
							NOW()
						)
							";
					if($result=$this->exeSQL($query))
					{
						if($this->deleteRecords($this->id))
						{
							return true;
						}
					}
				}
		  }
	}
	function deleteRecords($id)
	{
		$this->set_id((int)$id);
		if($this->id>0)
		{
			$query = "
				DELETE FROM 
					".__DBC_SCHEMATA_UPLOAD_FILES_ADMIN__." 
				WHERE 
					id = ".$this->id.";
			";
			if($result=$this->exeSQL($query))
			{
				return true;
			}
		}
	
	}
	function bulkMoveBackCompletedFiles($id)
	{
		$this->set_id((int)$id);
		if((int)$this->id>0)
		{
			$query="
					UPDATE
						".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
					SET	
						iStatus =".__COST_APPROVED_BY_FORWARDER__."				
					WHERE
						id = ".(int)$this->id."	
					AND
						iActive = 1			
				";
				if($result=$this->exeSQL($query))
				{	$this->load($this->id);
				
					$kAdmin= new cAdmin();
					$kAdmin->getAdminDetails($_SESSION['admin_id']);
					
					$this->szComment = $kAdmin->szFirstName." ".$kAdmin->szLastName.", Transporteca, ".date("j. F Y")."<br /><em>Submission was taken back for review by Transporteca Upload Service. New estimate will be provided.</em>";
					$query="
						INSERT INTO
							".__DBC_SCHEMATA_BULK_UPLOAD_COMMENT__."
						(
							idBulkUploadService,
							szComment,
							dtComment
						)
							VALUES
						(
							'".(int)$this->id."',
							'".mysql_escape_custom($this->szComment)."',
							NOW()
						)
							";
					//echo $query;
					if($result=$this->exeSQL($query))
					{
						return true;
					}
				}
		  }
	}
	function completeSubmissionFiles($id)
	{
		$this->set_id((int)$id);
		if((int)$this->id>0)
		{
			$query="
					UPDATE
						".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
					SET	
						iStatus =".__MANAGEMENT_COMPLETE_APPROVAL__.",
						dtCompleted=NOW()				
					WHERE
						id = ".(int)$this->id."	
				";
				if($result=$this->exeSQL($query))
				{	
					$this->load($this->id);
					
					$kAdmin= new cAdmin();
					$kAdmin->getAdminDetails($_SESSION['admin_id']);
					
					$this->szComment = $kAdmin->szFirstName." ".$kAdmin->szLastName.", Transporteca, ".date("j. F Y")."<br /><em>Data processing completed and ".$this->iActualRecords." lines have been submitted for review and approval.</em>";
					$query="
						INSERT INTO
							".__DBC_SCHEMATA_BULK_UPLOAD_COMMENT__."
						(
							idBulkUploadService,
							szComment,
							dtComment
						)
							VALUES
						(
							'".(int)$this->id."',
							'".mysql_escape_custom($this->szComment)."',
							NOW()
						)
							";
					if($result=$this->exeSQL($query))
					{
							return true;
					}
				}
		  }
	}
	//used to insert content on the transaction table for particular forwarder upload service
	function forwarderTansaction($id)
	{	
		$kAdmin  =new cAdmin();
		$invoiceNumber=$kAdmin->generateInvoiceForPayment();
		$this->set_id((int)$id);
		$this->load($this->id);
		if($this->szCurrency == 'USD')
		{
			$roe = 1;
		}
		else
		{
			$kWHSSearch = new cWHSSearch();
			$roe = $kWHSSearch->getCurrencyFactor($this->iCurrency);
		}
		$szDescription="Transporteca upload service";
		
		$fUSDAmount=$this->fProcessingCost*$roe;
		$query="
			INSERT INTO
					".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
				(
					idForwarder, 
					fTotalPriceForwarderCurrency, 
					idForwarderCurrency, 
					szForwarderCurrency, 
					fForwarderExchangeRate, 
					iDebitCredit, 
					szBooking,				
					dtCreatedOn,
					dtPaymentConfirmed,
					dtInvoiceOn,
					iStatus,
					szInvoice,
					idBooking,
					fUSDReferralFee
				)
				VALUES
				(
					'".(int)$this->idForwarder."',
					'".(float)$this->fProcessingCost."',
					'".$this->iCurrency."',
					'".$this->szCurrency."',
					'".(float)$roe."',
					'5',
					'".mysql_escape_custom(trim($szDescription))."',
					NOW(),
					NOW(),
					NOW(),
					'1',
					'".mysql_escape_custom($invoiceNumber)."',
					'".(int)$this->id."',
					'".(float)$fUSDAmount."'
			 	)
		";
		//echo $query."";
		if($result=$this->exeSQL($query))
		{
			$query="
				UPDATE
					".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
				SET
					iCompleted='1'
				WHERE
					id='".(int)$this->id."'
			";
			$result=$this->exeSQL($query);
			$this->invoiceNumber = $invoiceNumber;
			if($this->sendEmailsToForwarderWorkingProfile($id,__COST_CONFIRMED_FOR_UPLOAD_SERVICE_FROM_TRANSPORTECA__))
			{
			//die;
				return true;
			}
		}
	}
	function uploadBulkServicesRecords($id,$files)
	{	
		$this->set_id((int)$id);
		if($files['file_upload']['name']=='')
		{
			$this->addError( "file_name" , "Upload File is required");
		}
		if($files['file_upload']['name']!='')
		{
			$fileTypes = array('xlsx');
			if(!empty($files['file_upload']['name']))
			{
				$fileParts = pathinfo($files['file_upload']['name']);
				if(!in_array(strtolower($fileParts['extension']),$fileTypes)) 
				{
					$this->addError( "file_name" , ".".$fileParts['extension']." Invalid Uploaded Flie Type.");
					return false;
				}
			}
		}
		//exit if error exist.
		if ($this->error === true)
		{
			return false;
		}
		if(!empty($files['file_upload']['name']))
		{
				$tempFile='';
				$fileSavedName='';
				$tempFile=$files['file_upload']['tmp_name'];
				$filenameArr=$files['file_upload']['name'];
				
				
				
				$fileParts = pathinfo($filenameArr);
				$newFilename=str_replace(" ","-",$fileParts['filename']);
				//$newFilenameArr=explode(".",$newFilename);
				$fileSavedName=$newFilename."-".date('dmYHis').".".$fileParts['extension'];
				$targetFile=__UPLOAD_FORWARDER_BULK_SERVICES__."/".$fileSavedName;
				move_uploaded_file($tempFile,$targetFile);
				//$this->uploadFiles($id,$fileSavedName,$files['file_upload']['name']);
				
				require_once( __APP_PATH_CLASSES__ . "/PHPExcel/IOFactory.php" );
				$objReader = PHPExcel_IOFactory::createReader('Excel2007');
				$sheetindex=0;
				$objReader->setReadDataOnly(true);
				$objPHPExcel = $objReader->load($targetFile);
				$objPHPExcel->setActiveSheetIndex($sheetindex);
				$val1 = ($objPHPExcel->getActiveSheet()->getHighestColumn()); //CELL
				$colcount = PHPExcel_Cell::columnIndexFromString($val1);
				$rowcount = ($objPHPExcel->getActiveSheet()->getHighestRow());
				$maxRowCount=__MAX_ROW_COUNT_UPLOAD_FILE_EXCEEDS__;
				if($rowcount>$maxRowCount)
				{
                                    $uploadErrorMsg='MAX_ROW_EXCEED';
                                    $this->maxRowCountUploaded=$rowcount;
                                    return $uploadErrorMsg;
				}
				$this->load($id);
				$Export_import = new cExport_Import();
			   	$iRecordsStr = $Export_import->countRcordsList($targetFile,$this->iFileType,$this->idForwarder);
			   	
			   		$iRecordsArr=explode("_",$iRecordsStr);
			   		$iRecords=$iRecordsArr[0];
			   		$uploadErrorMsg=$iRecordsArr[1];
			   		$errorRowCount=$iRecordsArr[2];
				   	$query="
			   			DELETE FROM
			   				".__DBC_SCHEMATA_UPLOAD_FILES_ADMIN__."
			   			WHERE
				   			idBulkUploadService='".(int)$id."'	
				   	";
				   //	echo $query;
				   	//die;
					$result=$this->exeSQL($query);
					
					$query="
						INSERT INTO
							".__DBC_SCHEMATA_UPLOAD_FILES_ADMIN__."
						(
							idBulkUploadService,
							szFileName,
							szActualFileName,
							dtUploaded
						)
							VALUES
						(
							'".(int)$id."',
							'".$fileSavedName."',
							'".$filenameArr."',
							NOW()
						)
					";
					if($result=$this->exeSQL($query))
					{
						$query="
							UPDATE
								".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
							SET	
								iActualRecords ='".(int)$iRecords."'				
							WHERE
								id = ".(int)$id."	
						";
						if($result=$this->exeSQL($query))
						{
							$this->errorRowCount=$errorRowCount;
							return $uploadErrorMsg;
						}
						else
						{
							$this->error = true;
							$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
							$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
							return false;
						}
					}
		}
		
	}
	
	
	function bulkUploadCFS($objPHPExcel,$sheetindex,$colcount,$rowcount,$idForwarder,$flag=false,$iWarehouseType=__WAREHOUSE_TYPE_CFS__)
	{
		$CFSServiceData=array();
		$ctr=0;
		if($rowcount>1)
		{
                    for($i=2;$i<=$rowcount;$i++)
                    {
                        for($j=0;$j<$colcount;$j++)
                        {
                            if($objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0,$i)->getValue()!="")
                            {

                                $CFSServiceData[$ctr][] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($j,$i)->getValue();
                            }
                        }
                        ++$ctr;
                    }
		}
                if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
                {
                    $szServiceType = "airport warehouse";
                }
                else
                {
                    $szServiceType = "CFS";
                }
		
		//print_r($CFSServiceData);
		$newCFSArr=array();
		$Excel_export_import= new cExport_Import();
                        
		if(!empty($CFSServiceData))
		{
                    $k=2;
                    foreach($CFSServiceData as $CFSServiceDatas)
                    { 
                            $error_flag=false;
                            $err_msg=array();
                            $checkErrorFlag=false;
                            if($CFSServiceDatas[0]=='')
                            {
                                $error_flag=true;
                                if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
                                {
                                    $err_msg['szCFSName']=t($this->t_base.'messages/airport_warehouse_name_is_required');
                                }
                                else
                                {
                                    $err_msg['szCFSName']=t($this->t_base.'messages/cfs_name_is_required');
                                } 
                            }

                            if($CFSServiceDatas[1]=='')
                            {
                                $error_flag=true;
                                $err_msg['szAddress1']=t($this->t_base.'messages/address_line_1_is_required');
                            }


                            if($CFSServiceDatas[5]=='')
                            {
                                $error_flag=true;
                                $err_msg['szCity']=t($this->t_base.'messages/cfs_city_is_required');
                            }

                            if($CFSServiceDatas[7]=='')
                            {
                                $error_flag=true;
                                $err_msg['szCountry']=t($this->t_base.'messages/cfs_country_is_required');
                            }

                            if($CFSServiceDatas[8]=='')
                            {
                                $error_flag=true;
                                $err_msg['szPhoneNumber']=t($this->t_base.'messages/cfs_phone_number_is_required');
                            }

                            if($CFSServiceDatas[11]=='')
                            {
                                $error_flag=true;
                                $err_msg['szLatitude']=t($this->t_base.'messages/cfs_latitude_is_required');
                            }
                            else
                            {
                                //echo $CFSServiceDatas[8];
                                if($CFSServiceDatas[11]>'90')
                                {
                                    $error_flag=true;
                                    $err_msg['szLatitude']=t($this->t_base.'messages/cfs_latitude_shuold_be_less_than_90_north_pole');
                                }
                                else if($CFSServiceDatas[11]<'-90')
                                {
                                    $error_flag=true;
                                    $err_msg['szLatitude']=t($this->t_base.'messages/cfs_latitude_shuold_be_greater_than_90_south_pole)');
                                }
                            }
                            if($CFSServiceDatas[12]=='')
                            {
                                $error_flag=true;
                                $err_msg['szLongitude']=t($this->t_base.'messages/cfs_longitude_is_required');
                            }
                            else
                            {
                                if($CFSServiceDatas[12]>'180')
                                {
                                    $error_flag=true;
                                    $err_msg['szLongitude']=t($this->t_base.'messages/cfs_longitude_shuold_be_less_than_180');
                                }
                                else if($CFSServiceDatas[12]<'-180')
                                {
                                    $error_flag=true;
                                    $err_msg['szLongitude']=t($this->t_base.'messages/cfs_longitude_shuold_be_greater_than_180');
                                }
                            }

                            $country=$Excel_export_import->getCountryId(sanitize_all_html_input($CFSServiceDatas[7]));
                            if($CFSServiceDatas[11]!=NULL && $CFSServiceDatas[12]!=NULL && $country!=NULL)
                            {
                                $countryApiDetailAry = array();
                                $countryApiDetailAry = getAddressDetails($CFSServiceDatas[11],$CFSServiceDatas[12],$country);
                                $countryISO = $countryApiDetailAry['szCountryCode'] ;

                                //$countryDetails = $this->setCountryNameISO();	
                                if(strtoupper(trim($countryDetails['szCountryISO'])) != strtoupper(trim($countryISO)))
                                {
                                    //$error_flag=true;
                                    //$err_msg['szCountry']=t($this->t_base.'messages/this_location_is_not_in')." ".$countryDetails['szCountryName'] .". ". t($this->t_base.'messages/please_update_your_lat_lang');
                                }
                            }

                            if(empty($err_msg))
                            {
                                $country=$Excel_export_import->getCountryId(sanitize_all_html_input($CFSServiceDatas[7]));
                                $query="
                                        SELECT 
                                            count(id) as value
                                        FROM
                                            ".__DBC_SCHEMATA_WAREHOUSES__."	
                                        WHERE
                                            idForwarder =  '".(int)$idForwarder."'	
                                        AND
                                            idCountry = '".(int)$country."'	
                                        AND	
                                            LOWER(szCity) = '".mysql_escape_custom(strtolower($CFSServiceDatas[5]))."'
                                        AND
                                            szWareHouseName='".mysql_escape_custom($CFSServiceDatas[0])."'	
                                        AND
                                            iWarehouseType = '".(int)$iWarehouseType."'
                                        AND
                                            iActive = '1'
                                ";
                                //echo $query."<br/><br/>";
                                if($result=$this->exeSQL($query))
                                {
                                    //echo $this->iNumRows."<br/>";	
                                    if($this->iNumRows>0)
                                    {
                                        $row=$this->getAssoc($result);
                                        $value=$row['value'];
                                        if((int)$value>0)
                                        {
                                            $error_flag=true;
                                            if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
                                            {
                                                $err_msg['szCFSName']=t($this->t_base.'messages/airport_warehouse_name_already_exists');
                                            }
                                            else
                                            {
                                                $err_msg['szCFSName']=t($this->t_base.'messages/cfs_name_already_exists');
                                            }
                                        }
                                    }
                                }
                            }

                            if(!empty($err_msg))
                            {
                                $err_msg_arr=implode(", ",$err_msg);
                                $errmsg_arr[]=t($this->t_base.'messages/following_error_in_row_number') ." ".$k." - ".$CFSServiceDatas[0].": ".$err_msg_arr;
                            }
                            else
                            {
                                $newCFSArr[]=$CFSServiceDatas;
                            }
                            ++$k;
			}
			$CFSServiceDataCount=count($newCFSArr);
			$this->rowCountUploaded=$CFSServiceDataCount;
			$this->rowErrorCount=count($errmsg_arr);
			if(!$flag)
			{
                            if((int)$_SESSION['forwarder_admin_id']>0)
                            {
                                $idAdmin = (int)$_SESSION['forwarder_admin_id'];
                                $idForwarderContact = 0 ;
                            }
                            else
                            {
                                $idAdmin = 0;
                                $idForwarderContact = (int)$_SESSION['forwarder_user_id'];
                            }

                            $kWHSSearch= new cWHSSearch();
                            ///$idForwarderContact		=	(int)$_SESSION['forwarder_user_id'];
                            $forwarderContactName = $kWHSSearch->findForwarderFirstLastName($idForwarderContact); 
                            if(!empty($newCFSArr))
                            {
                                foreach($newCFSArr as $newCFSArrs)
                                {
                                    $country=$Excel_export_import->getCountryId(sanitize_all_html_input($newCFSArrs[7]));

                                    $query="
                                        SELECT 
                                            count(id) as value
                                        FROM
                                            ".__DBC_SCHEMATA_WAREHOUSES__."	
                                        WHERE
                                            idForwarder = 	'".(int)$idForwarder."'	
                                        AND
                                            idCountry = '".(int)$country."'	
                                        AND	
                                            LOWER(szCity) = '".mysql_escape_custom(strtolower(($newCFSArrs[5])))."'
                                        AND
                                            szWareHouseName='".mysql_escape_custom($newCFSArrs[0])."'	
                                        AND
                                            iWarehouseType = '".(int)$iWarehouseType."'
                                        AND
                                            iActive = '1'
                                    ";
                                    //echo $query."<br/><br/>";
                                    if($result=$this->exeSQL($query))
                                    {
                                        //echo $this->iNumRows."<br/>";	
                                        if($this->iNumRows<=0)
                                        {	 
                                            $query="
                                                INSERT INTO
                                                    ".__DBC_SCHEMATA_WAREHOUSES__."
                                                (
                                                    szWareHouseName,
                                                    szAddress,
                                                    szAddress2,
                                                    szAddress3,
                                                    szPostCode,
                                                    szCity,
                                                    szState,
                                                    idCountry,
                                                    szPhone,
                                                    szContactPerson,
                                                    szEmail,
                                                    szLatitude,
                                                    szLongitude,
                                                    idForwarder,
                                                    dtUpdatedOn,
                                                    dtCreatedOn,
                                                    iActive,
                                                    idForwarderContact,
                                                    idAdmin,
                                                    szUpdatedBy,
                                                    iWarehouseType
                                                )	
                                                VALUES
                                                (	
                                                    '".mysql_escape_custom($newCFSArrs[0])."',
                                                    '".mysql_escape_custom($newCFSArrs[1])."',
                                                    '".mysql_escape_custom($newCFSArrs[2])."',
                                                    '".mysql_escape_custom($newCFSArrs[3])."',
                                                    '".mysql_escape_custom($newCFSArrs[4])."',
                                                    '".mysql_escape_custom(ucwords(strtolower($newCFSArrs[5])))."',
                                                    '".mysql_escape_custom($newCFSArrs[6])."',
                                                    '".(int)$country."',
                                                    '".mysql_escape_custom($newCFSArrs[8])."',
                                                    '".mysql_escape_custom($newCFSArrs[9])."',
                                                    '".mysql_escape_custom($newCFSArrs[10])."',
                                                    '".mysql_escape_custom($newCFSArrs[11])."',
                                                    '".mysql_escape_custom($newCFSArrs[12])."',
                                                    '".(int)$idForwarder."',
                                                    NOW(),
                                                    NOW(),
                                                    '1',
                                                    '".(int)$idForwarderContact."',
                                                    '".(int)$idAdmin."',
                                                    '".mysql_escape_custom($forwarderContactName)."',
                                                    '".mysql_escape_custom($iWarehouseType)."'
                                                )	
                                            ";
                                            $result=$this->exeSQL($query);
                                            $warehouseID=0;
                                            $warehouseID = (int)$this->iLastInsertID;
                                            if((int)$warehouseID>0)
                                            {
                                                $kWHSSearch->cfsModelMapping($warehouseID);
                                            }
					}
				}
			}
		}
				
                $this->errorLine=count($errmsg_arr);
                $this->successLine=count($newCFSArr);

                if(!empty($errmsg_arr))
                {
                    $l=1;
                    $err_str .="We found errors in following ".$this->errorLine." ".$szServiceType." loaction in your upload file:";
                    foreach($errmsg_arr as $errmsg_arrs)
                    {
                        $err_str .="<br/>".$l.". ".$errmsg_arrs."<br/>";
                        ++$l;
                    }
                    $err_str .="<br/>Please correct the errors and upload the file again.";
                }

                $kForwarderContact = new cForwarderContact();
                $kForwarderContact->load($_SESSION['forwarder_user_id']);

                $kForwarder = new cForwarder();
                $idForwarder = $_SESSION['forwarder_id'];
                $kForwarder->load($idForwarder);
                $SuccessCount="You have successfully updated ".$this->successLine." ".$szServiceType." location.";
                $replace_ary['SuccessCount']=$SuccessCount;
                //$replace_ary['ErrorCount']=$this->errorLine;
                $replace_ary['szErrorMsg']=$err_str;
                $replace_ary['szEmail']=$kForwarderContact->szEmail;
                $replace_ary['szName'] = $kForwarderContact->szFirstName." ".$kForwarderContact->szLastName;
                createEmail(__FARWARDER_BULK_UPLOAD_CFS_LOCATION_EMAIL__, $replace_ary,$kForwarderContact->szEmail, __BULK_UPLOAD_CFS_LOACTION_SUBJECT__, __STORE_SUPPORT_EMAIL__,$this->id, __STORE_SUPPORT_EMAIL__);            
                return true;
            }
            else
            {
                if(!empty($errmsg_arr))
                {
                    $l=1;
                    $err_str .="We found errors in following ".$this->errorLine." ".$szServiceType." loaction in your upload file:";
                    foreach($errmsg_arr as $errmsg_arrs)
                    {
                        $err_str .="<br/>".$l.". ".$errmsg_arrs."<br/>";
                        ++$l;
                    }
                    $err_str .="<br/>Please correct the errors and upload the file again.";
                    $idAdmin = $_SESSION['admin_id'];
                    $kAdmin=new cAdmin();
                    $kAdmin->getAdminDetails($idAdmin);
                    $replace_arr['szEmail']=$kAdmin->szEmail;
                    $replace_arr['szFirstName']=$kAdmin->szFirstName;
                    $replace_arr['szLastName']=$kAdmin->szLastName;
                    $replace_arr['szErrorMsg']=$err_str;
                    createEmail(__MANAGEMENT_BULK_UPLOAD_SERVICE_FILE_EMAIL__, $replace_arr,$kAdmin->szEmail, __MANAGEMENT_BULK_UPLOAD_SERVICE_FILE_SUBJECT__, __STORE_SUPPORT_EMAIL__,$this->id, __STORE_SUPPORT_EMAIL__);            
                    return false; 
                }
                else
                {
                    return true;
                }
            }
        }
        else
        {
            return false;
        } 
    }

	function deleteUploadService($id)
	{
		$this->set_id((int)sanitize_all_html_input($id));
		if($this->id>0)
		{
			$query = "
				DELETE FROM 
					".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__." 
				WHERE 
					id = ".$this->id.";
				";
				if($result=$this->exeSQL($query))
				{
					$query = "
						DELETE FROM 
							".__DBC_SCHEMATA_BULK_UPLOAD_FILES__." 
						WHERE 
							idBulkUploadService = ".$this->id.";
					";
					if($result=$this->exeSQL($query))
					{
						$query = "
							DELETE FROM 
								".__DBC_SCHEMATA_BULK_UPLOAD_COMMENT__." 
							WHERE 
								idBulkUploadService = ".$this->id.";
						";
						if($result=$this->exeSQL($query))
						{
							$query = "
								DELETE FROM 
									".__DBC_SCHEMATA_UPLOAD_FILES_ADMIN__." 
								WHERE 
									idBulkUploadService = ".$this->id.";
							";
							if($result=$this->exeSQL($query))
							{
								return true;
							}
						}
					}
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
		}
	}
	
	function curl_get_file_contents($URL) 
	{
		$c = curl_init();
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($c, CURLOPT_URL, $URL);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, FALSE);
		$contents = curl_exec($c);
		$err  = curl_getinfo($c,CURLINFO_HTTP_CODE);
		curl_close($c);
		if ($contents) return $contents;
		else return FALSE;
	 }
	function sendEmailsToForwarderAdminAndPricing($id)
	{
		$this->set_id((int)$id);
		$this->load($this->id);
		$query = "
			SELECT
				id,
				szFirstName,
				szLastName,
				szEmail
			FROM
				".__DBC_SCHEMATA_FORWARDERS_CONTACT__."		
			WHERE
					idForwarder = ".$this->idForwarder."
			AND
				idForwarderContactRole   IN (".__ADMINISTRATOR_PROFILE_ID__.",".__PRICING_PROFILE_ID__.")
			AND
				iActive='1'
			AND
				iConfirmed ='1'			
		";
		if($result=$this->exeSQL($query))
		{
			$profileArr = array();
			while($row=$this->getAssoc($result))
			{
				$profileArr[] = $row;
			}
			if(!empty($profileArr))
			{
				$kForwarder = new cForwarder();
				$kForwarder->load($this->idForwarder);
				$replace_ary['szDisplayName'] = $kForwarder->szDisplayName;
				$replace_ary['szNameBy'] = $this->szForwarderContactName;
				$replace_ary['szDate'] = date('d. F Y',strtotime($this->dtSubmitted));
				
				foreach($profileArr as $profArr)
				{	
					//$replace_ary['id'] = $profArr['id'];
					$replace_ary['szName'] = $profArr['szFirstName'];
					$replace_ary['szHere'] = '<a href="http://'.$kForwarder->szControlPanelUrl.'/BulkServiceApproval/">here</a>';
		       		createEmail(__COST_ESTIMATION_FOR_UPLOAD_SERVICE_FROM_TRANSPORTECA__, $replace_ary,$profArr['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$profArr['id'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__,false);
				}
			}
		}
	}
	function sendEmailsToForwarderAdminAndPricingFree($id)
	{
		$this->set_id((int)$id);
		$this->load($this->id);
		$query = "
			SELECT
				id,
				szFirstName,
				szEmail
			FROM
				".__DBC_SCHEMATA_FORWARDERS_CONTACT__."		
			WHERE
					idForwarder = ".$this->idForwarder."
			AND
				idForwarderContactRole   IN (".__ADMINISTRATOR_PROFILE_ID__.",".__PRICING_PROFILE_ID__.")
			AND
				iActive='1'
			AND
				iConfirmed ='1'			
		";
		if($result=$this->exeSQL($query))
		{
			$profileArr = array();
			while($row=$this->getAssoc($result))
			{
				$profileArr[] = $row;
			}
			if(!empty($profileArr))
			{
				$kForwarder = new cForwarder();
				$kForwarder->load($this->idForwarder);
				$replace_ary['szDisplayName'] = $kForwarder->szDisplayName;
				$replace_ary['szNameBy'] = utf8_encode($this->szForwarderContactName);
				$replace_ary['szDate'] = DATE('d. F Y',strtotime($this->dtSubmitted));
				
				foreach($profileArr as $profArr)
				{	
					//$replace_ary['id'] = $profArr['id'];
					//$replace_ary['szEmail'] = $profArr['szEmail'];
		       		createEmail(__COST_ESTIMATION_FOR_UPLOAD_SERVICE_FROM_TRANSPORTECA_FREE__, $replace_ary,$profArr['szEmail'],'', __STORE_SUPPORT_EMAIL__,$profArr['id'], __STORE_SUPPORT_EMAIL__,false);
				}
			}
		}
	}
	function sendEmailsToForwarderWorkingProfile($id,$template)
	{
		$this->set_id((int)$id);
		$this->load($this->id);
		$query = "
			SELECT
				id,
				szFirstName,
				szEmail
			FROM
				".__DBC_SCHEMATA_FORWARDERS_CONTACT__."		
			WHERE
					idForwarder = ".$this->idForwarder."
			AND
				idForwarderContactRole   IN (".__ADMINISTRATOR_PROFILE_ID__.",".__PRICING_PROFILE_ID__.")
			AND
				iActive='1'
			AND
			    iConfirmed ='1' 			
		";
		if($result=$this->exeSQL($query))
		{
			$profileArr = array();
			while($row=$this->getAssoc($result))
			{
				$profileArr[] = $row;
			}
			if(!empty($profileArr))
			{
				$kForwarder = new cForwarder();
				$kForwarder->load($this->idForwarder);
				$replace_ary['szDisplayName'] = $kForwarder->szDisplayName;
				$replace_ary['szNameBy'] = $this->szForwarderContactName;
				$replace_ary['szDate'] = date('d. F Y',strtotime($this->dtSubmitted));
				if($template ==__COST_CONFIRMED_FOR_UPLOAD_SERVICE_FROM_TRANSPORTECA__)
				{
					$replace_ary['szInvoiceNumber'] = $this->invoiceNumber;
					$replace_ary['szCurrency'] =  $this->szCurrency;
					$replace_ary['szFee'] =  $this->fProcessingCost;
					$replace_ary['szAgreedBy'] = $this->szCostApprovalBy;
					$replace_ary['dtFeeConfirm'] = date('d. F Y',strtotime($this->dtCostApproval));
				}
				foreach($profileArr as $profArr)
				{	
					//$replace_ary['id'] = $profArr['id'];
					$replace_ary['szName'] = $profArr['szFirstName'];
					//$replace_ary['szEmail'] = $profArr['szEmail'];
		       		createEmail($template, $replace_ary,$profArr['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$profArr['id'], __STORE_SUPPORT_EMAIL__,false);
				}
			}
		}
	}
	function sendEmailUsersManagement($id)
	{
		$this->set_id((int)$id);
		if($this->id>0)
		{
			$query = "
				SELECT
					szEmail
				FROM
					".__DBC_SCHEMATA_MANAGEMENT__."		
				WHERE
						iActive = 1		
			";
			if($result=$this->exeSQL($query))
			{
				$profileArr = array();
				while($row=$this->getAssoc($result))
				{
					$profileArr[] = $row;
				}	
				if($profileArr!=array())
				{	$this->load($this->id);
					$kForwarder = new cForwarder();
					$kForwarder->load($this->idForwarder);
					$replace_ary['szDisplayName'] = $kForwarder->szDisplayName;
					$replace_ary['szHere'] = ' <a href="'.__BASE_MANAGEMENT_URL__.'/servicingUrl/">here</a> ';
					foreach($profileArr as $profArr)
					{	
			       		createEmail(__FORWARDER_APPROVAL_UPLOAD_SERVICE__, $replace_ary,$profArr['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$profArr['id'], __STORE_SUPPORT_EMAIL__,false);
					}
				}
			}
		}
	
	} 
	
	function bulkUploadServiceFilesByManagement($id)
	{
		if((int)$id>0)
		{
			$query="
				SELECT
					szFileName,
					szActualFileName
				FROM
					".__DBC_SCHEMATA_UPLOAD_FILES_ADMIN__."
				WHERE
					idBulkUploadService='".(int)$id."'
			";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					while($row=$this->getAssoc($result))
					{
						$res_arr[]=$row;
					}
					return $res_arr;
				}
				else
				{
					return array();
				}
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function addDataForApprovalPricingCC($objPHPExcel,$sheetindex,$colcount,$rowcount,$idForwarder)
	{
		date_default_timezone_set('UTC');
		$ctr=0;
		if($rowcount>2)
		{
			for($i=3;$i<=$rowcount;$i++)
			{
				for($j=0;$j<$colcount;$j++)
				{
					if($objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0,$i)->getValue()!="")
					{
						
						$customClearanceData[$ctr][] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($j,$i)->getValue();
						
					}
					
				}
				++$ctr;
			}
		}
		
		
		
		//print_r($customClearanceData);
		$Excel_export_import= new cExport_Import();
		//print_r($customClearanceData);
		if(!empty($customClearanceData))
		{
			$k=3;
			$l_err=0;
			$l_success=0;
			$d=0;
			foreach($customClearanceData as $customClearanceDatas)
			{
				$error_flag=false;
				$err_msg=array();
				$checkErrorFlag=false;
				if($customClearanceDatas[0]=="")
				{
					$error_flag=true;
					$err_msg['origin_country']=t($this->t_base.'messages/origin_country_is_required');
				}
				
				if($customClearanceDatas[1]=="")
				{
					$error_flag=true;
					$err_msg['origin_city']=t($this->t_base.'messages/origin_city_is_required');
						
				}
				if($customClearanceDatas[2]=="")
				{
					$error_flag=true;
					$err_msg['origin_cfs']=t($this->t_base.'messages/origin_cfs_is_required');
						
				}
				if($customClearanceDatas[3]=="")
				{
					$error_flag=true;
					$err_msg['destination_country']=t($this->t_base.'messages/des_cfs_is_required');
						
				}
				if($customClearanceDatas[4]=="")
				{
					$error_flag=true;
					$err_msg['destination_city']=t($this->t_base.'messages/des_cfs_is_required');
						
				}
				if($customClearanceDatas[5]=="")
				{
					$error_flag=true;
					$err_msg['destination_cfs']=t($this->t_base.'messages/des_cfs_is_required');
						
				}
				
				if($customClearanceDatas[5]!='' && $customClearanceDatas[0]!='' && $customClearanceDatas[4]!='' && $customClearanceDatas[3]!='' && $customClearanceDatas[2]!='' && $customClearanceDatas[1]!='')
				{
				
					$originCountry_Id=$Excel_export_import->getCountryId(sanitize_all_html_input($customClearanceDatas[0]));
				
					$desCountry_Id=$Excel_export_import->getCountryId(sanitize_all_html_input($customClearanceDatas[3]));
					$query="
						SELECT
							id
						FROM
							".__DBC_SCHEMATA_WAREHOUSES__."
						WHERE
							szWareHouseName='".mysql_escape_custom(sanitize_all_html_input($customClearanceDatas[2]))."' 	
						AND
							szCity='".mysql_escape_custom(sanitize_all_html_input($customClearanceDatas[1]))."'
						AND
							idCountry='".(int)$originCountry_Id."'
						AND
							idForwarder='".(int)$idForwarder."'
                                                AND
                                                    iActive='1'
                                                AND
                                                    isDeleted='0'            
					";
					//echo $query."<br/><br/>";
					if($result=$this->exeSQL($query))
					{
						if($this->iNumRows>0)
						{
							$row=$this->getAssoc($result);
							$fromWareHouse_Id=$row['id'];
						}
						else
						{
							$fromWareHouse_Id=0;
						}
					}
					
					$query="
						SELECT
							id
						FROM
							".__DBC_SCHEMATA_WAREHOUSES__."
						WHERE
							szWareHouseName='".mysql_escape_custom($customClearanceDatas[5])."' 	
						AND
							szCity='".mysql_escape_custom($customClearanceDatas[4])."'
						AND
							idCountry='".(int)$desCountry_Id."'
						AND
							idForwarder='".(int)$idForwarder."'
                                                AND
                                                    iActive='1'
                                                AND
                                                    isDeleted='0'            
					";
					//echo $query."<br/>";
					if($result=$this->exeSQL($query))
					{
						if($this->iNumRows>0)
						{
							
							$row=$this->getAssoc($result);
							$toWareHouse_Id=$row['id'];
						}
						else
						{
							$toWareHouse_Id=0;
						}
					}
					
					if($toWareHouse_Id>0 && $fromWareHouse_Id>0)
					{
						$query="
							SELECT
								*
							FROM
								".__DBC_SCHEMATA_PRICING_CC__."
							WHERE
								idWarehouseFrom='".(int)$fromWareHouse_Id."'
							AND
								idWarehouseTo='".(int)$toWareHouse_Id."'
							AND
								iActive='1'
						";
						//echo $query;
						if($result=$this->exeSQL($query))
						{
							if($this->iNumRows>0)
							{
								$checkErrorFlag=true;
								if($customClearanceDatas[6]=='' && $customClearanceDatas[7]=='' && $customClearanceDatas[8]=='' && $customClearanceDatas[9]=='')
								{
									$checkErrorFlag=false;
									$deleteWarehouseCombination[$d]['toWareHouse']=$toWareHouse_Id;
									$deleteWarehouseCombination[$d]['fromWareHouse']=$fromWareHouse_Id;
									++$d;
								}								
							}
							else
							{
								if($customClearanceDatas[6]!='')
								{
									$checkErrorFlag=true;
								}
								
								if($customClearanceDatas[7]!='')
								{
									$checkErrorFlag=true;
								}
								
								if($customClearanceDatas[8]!='')
								{
									$checkErrorFlag=true;
								}
								
								if($customClearanceDatas[9]!='')
								{
									$checkErrorFlag=true;
								}
								
							}
						}
					}
					else
					{
						$error_flag=true;
						$err_msg['destination_origin_warehouse']=t($this->t_base.'messages/origin_des_both_valid');
					}
				}
				else
				{
					$checkErrorFlag=true;
				}
				
				if($checkErrorFlag)
				{
					//echo "hello";
					if((float)$customClearanceDatas[6]>0 || $customClearanceDatas[7]!='')
					{
						//echo "hello2";
						if($customClearanceDatas[6]=="")
						{
						
							$error_flag=true;
							$err_msg['export_per_wm']=t($this->t_base.'messages/export_per_wm_is_required');
						}
						else if((float)$customClearanceDatas[6]<=0 )
						{
							$error_flag=true;
							$err_msg['export_per_wm']=t($this->t_base.'messages/export_per_wm_should_be_positive_value');
						}
						
						if($customClearanceDatas[7]=="")
						{
						
							$error_flag=true;
							$err_msg['export_currency']=t($this->t_base.'messages/export_currency_is_required');
						}
						else 
						{
							$currencyArr=$Excel_export_import->getFreightCurrency();
							if(!empty($currencyArr))
							{
								if(!in_array(strtoupper($customClearanceDatas[7]),$currencyArr))
								{
									$error_flag=true;
									$err_msg['export_currency']=t($this->t_base.'messages/invalid_export_currency_type');
								}
							}
						}
					}
					
					if((float)$customClearanceDatas[8]>0 || $customClearanceDatas[9]!="")
					{
						if($customClearanceDatas[8]=="")
						{
						
							$error_flag=true;
							$err_msg['import_per_wm']=t($this->t_base.'messages/import_per_wm_is_required');
						}
						else if((float)$customClearanceDatas[8]<=0)
						{
							$error_flag=true;
							$err_msg['import_per_wm']=t($this->t_base.'messages/import_per_wm_should_be_positive_value');
						}
						
						
						if($customClearanceDatas[9]=="")
						{
						
							$error_flag=true;
							$err_msg['import_currency']=t($this->t_base.'messages/ipmort_currency_is_required');
						}
						else
						{
							$currencyArr=$Excel_export_import->getFreightCurrency();
							if(!empty($currencyArr))
							{
								if(!in_array(strtoupper($customClearanceDatas[9]),$currencyArr))
								{
									$error_flag=true;
									$err_msg['import_currency']=t($this->t_base.'messages/invalid_import_currency_type');
								}
							}
						}
					}
					
					
					if(!$error_flag)
					{
						$newcustomClearanceData[$k]=$customClearanceDatas;
					}					
				}
				
				if(!empty($err_msg))
				{
					$err_msg_arr=implode(", ",$err_msg);
					$errmsg_arr[]=t($this->t_base.'messages/following_error_in_row_number')." ".$k." - ".$customClearanceDatas[0]."/".$customClearanceDatas[1]."/".$customClearanceDatas[2]."/  ".$customClearanceDatas[3]."/".$customClearanceDatas[4]."/".$customClearanceDatas[5].""." : ".$err_msg_arr;
					++$l_err;
				}
				++$k;
			}
		}
		//print_r($newcustomClearanceData);
		$customClearanceDataCount=count($newcustomClearanceData);
		$this->rowCountUploaded=$customClearanceDataCount;
		$this->rowErrorCount=count($errmsg_arr);
		//print_r($errmsg_arr);
		//die;
		if(!empty($errmsg_arr))
		{
			$l=1;
			$err_str .="We found errors in following ".$this->errorLine." customs clearance lines in your upload file:";
			foreach($errmsg_arr as $errmsg_arrs)
			{
				$err_str .="<br/>".$l.". ".$errmsg_arrs."<br/>";
				++$l;
			}
			$err_str .="<br/>Please correct the errors and upload the file again.";
			
			$idAdmin = $_SESSION['admin_id'];
			$kAdmin=new cAdmin();
			$kAdmin->getAdminDetails($idAdmin);
			$replace_arr['szEmail']=$kAdmin->szEmail;
			$replace_arr['szFirstName']=$kAdmin->szFirstName;
			$replace_arr['szLastName']=$kAdmin->szLastName;
			$replace_arr['szErrorMsg']=$err_str;
			createEmail(__MANAGEMENT_BULK_UPLOAD_SERVICE_FILE_EMAIL__, $replace_arr,$kAdmin->szEmail, __MANAGEMENT_BULK_UPLOAD_SERVICE_FILE_SUBJECT__, __STORE_SUPPORT_EMAIL__,$this->id, __STORE_SUPPORT_EMAIL__);            
			return false;
		}
		else
		{
			
			return true;
		}
	}		

	
	function addDataForApprovalLCLServicesData($objPHPExcel,$sheetindex,$colcount,$rowcount,$idForwarder,$iWarehouseType=__WAREHOUSE_TYPE_CFS__)
	{
		date_default_timezone_set('UTC');
		$ctr=0;
		if($rowcount>2)
		{
                    for($i=3;$i<=$rowcount;$i++)
                    {
                        for($j=0;$j<$colcount;$j++)
                        {
                            if($objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0,$i)->getValue()!="")
                            {
                                if(is_numeric($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($j,$i)->getValue()) && ($j==26 || $j==25 || $j==23 || $j==20))
                                {
                                    $LCLServiceData[$ctr][] = PHPExcel_Shared_Date::ExcelToPHP($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($j,$i)->getValue());
                                }
                                else
                                {
                                    $LCLServiceData[$ctr][] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($j,$i)->getValue();
                                }
                            } 
                        }
                        ++$ctr;
                    }
		}		
		
                //setting error messages based on iWarehouseType
                if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
                {
                    $szOriginCfs = t($this->t_base.'messages/origin_aiport_warehouse_is_required'); 
                    $szDestinationCfs = t($this->t_base.'messages/des_airport_is_required');
                    $szOriginChargePmRequired = t($this->t_base.'messages/air_origin_charge_per_wm_is_required');
                    $szOriginChargePmPositive = t($this->t_base.'messages/air_origin_charge_per_wm_should_be_positive_value');
                    
                    $szFreightChargePmRequired = t($this->t_base.'messages/air_per_wm_is_required'); 
                    $szFreightChargePmPositive = t($this->t_base.'messages/air_per_wm_should_be_positive_value');  
                    
                    $szDestinationChargePmRequired = t($this->t_base.'messages/air_destination_charge_per_wm_is_required'); 
                    $szDestinationChargePmPositive = t($this->t_base.'messages/air_destination_charge_per_wm_should_be_positive_value');  

                }
                else
                {
                    $szOriginCfs = t($this->t_base.'messages/origin_cfs_is_required');
                    $szDestinationCfs = t($this->t_base.'messages/des_cfs_is_required'); 
                    $szOriginChargePmRequired = t($this->t_base.'messages/origin_charge_per_wm_is_required');
                    $szOriginChargePmPositive = t($this->t_base.'messages/origin_charge_per_wm_should_be_positive_value');
                    
                    $szFreightChargePmRequired = t($this->t_base.'messages/per_wm_is_required'); 
                    $szFreightChargePmPositive = t($this->t_base.'messages/per_wm_should_be_positive_value'); 
                    
                    $szDestinationChargePmRequired = t($this->t_base.'messages/destination_charge_per_wm_is_required'); 
                    $szDestinationChargePmPositive = t($this->t_base.'messages/destination_charge_per_wm_should_be_positive_value');  
                }
		///print_r($LCLServiceData);
		$Excel_export_import= new cExport_Import();
		
		if(!empty($LCLServiceData))
		{
			$k=3;
			$l_err=0;
			$l_success=0;
			$d=0;
			foreach($LCLServiceData as $LCLServiceDatas)
			{
				$error_flag=false;
				$err_msg=array();
				$checkErrorFlag=false;
				if($LCLServiceDatas[0]=="")
				{
                                    $error_flag=true;
                                    $err_msg['origin_country']=t($this->t_base.'messages/origin_country_is_required');
				}
				
				if($LCLServiceDatas[1]=="")
				{
                                    $error_flag=true;
                                    $err_msg['origin_city']=t($this->t_base.'messages/origin_city_is_required');
						
				}
				if($LCLServiceDatas[2]=="")
				{
                                    $error_flag=true;
                                    $err_msg['origin_cfs'] = $szOriginCfs;
						
				}
				if($LCLServiceDatas[3]=="")
				{
                                    $error_flag=true;
                                    $err_msg['destination_country']=t($this->t_base.'messages/des_country_is_required');
						
				}
				if($LCLServiceDatas[4]=="")
				{
                                    $error_flag=true;
                                    $err_msg['destination_city']=t($this->t_base.'messages/des_city_is_required');
						
				}
				if($LCLServiceDatas[5]=="")
				{
                                    $error_flag=true;
                                    $err_msg['destination_cfs'] = $szDestinationCfs; 
				}
				
				if($LCLServiceDatas[5]!='' && $LCLServiceDatas[0]!='' && $LCLServiceDatas[4]!='' && $LCLServiceDatas[3]!='' && $LCLServiceDatas[2]!='' && $LCLServiceDatas[1]!='')
				{
				
					$originCountry_Id=$Excel_export_import->getCountryId(sanitize_all_html_input($LCLServiceDatas[0]));
				
					$desCountry_Id=$Excel_export_import->getCountryId(sanitize_all_html_input($LCLServiceDatas[3]));
					$query="
						SELECT
                                                    id
						FROM
                                                    ".__DBC_SCHEMATA_WAREHOUSES__."
						WHERE
                                                    szWareHouseName='".mysql_escape_custom(sanitize_all_html_input($LCLServiceDatas[2]))."' 	
						AND
                                                    szCity='".mysql_escape_custom(sanitize_all_html_input($LCLServiceDatas[1]))."'
						AND
                                                    idCountry='".(int)$originCountry_Id."'
						AND
                                                    idForwarder='".(int)$idForwarder."'
                                                AND
                                                    iWarehouseType = '".(int)$iWarehouseType."'
                                                AND
                                                    iActive='1'
                                                AND
                                                    isDeleted='0'
					";
					if($result=$this->exeSQL($query))
					{
						if($this->iNumRows>0)
						{
							$row=$this->getAssoc($result);
							$fromWareHouse_Id=$row['id'];
						}
						else
						{
							$fromWareHouse_Id=0;
						}
					}
					
					$query="
						SELECT
							id
						FROM
							".__DBC_SCHEMATA_WAREHOUSES__."
						WHERE
							szWareHouseName='".mysql_escape_custom(sanitize_all_html_input($LCLServiceDatas[5]))."' 	
						AND
							szCity='".mysql_escape_custom(sanitize_all_html_input($LCLServiceDatas[4]))."'
						AND
							idCountry='".(int)$desCountry_Id."'
						AND
							idForwarder='".(int)$idForwarder."'
                                                AND
                                                    iWarehouseType = '".(int)$iWarehouseType."'
                                                AND
                                                    iActive='1'
                                                AND
                                                    isDeleted='0'        
					";
					//echo $query."<br/>";
					if($result=$this->exeSQL($query))
					{
						if($this->iNumRows>0)
						{
							
							$row=$this->getAssoc($result);
							$toWareHouse_Id=$row['id'];
						}
						else
						{
							$toWareHouse_Id=0;
						}
					}
					
					if($toWareHouse_Id>0 && $fromWareHouse_Id>0)
					{
						$query="
							SELECT
								*
							FROM
								".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
							WHERE
								idWarehouseFrom='".(int)$fromWareHouse_Id."'
							AND
								idWarehouseTo='".(int)$toWareHouse_Id."'
							AND
								iActive='1'
						";
						if($result=$this->exeSQL($query))
						{
							if($this->iNumRows>0)
							{
                                                            $checkErrorFlag=true;
                                                            if($LCLServiceDatas[6]=='' && $LCLServiceDatas[7]=='' && $LCLServiceDatas[8]=='' && $LCLServiceDatas[9]=='' && $LCLServiceDatas[10]=='' && $LCLServiceDatas[11]=='' && $LCLServiceDatas[13]=='' && $LCLServiceDatas[15]=='' && $LCLServiceDatas[16]=='' && $LCLServiceDatas[17]=='' && $LCLServiceDatas[18]=='' && $LCLServiceDatas[19]=='' && $LCLServiceDatas[20]=='' && $LCLServiceDatas[22]=='' && $LCLServiceDatas[23]=='' && $LCLServiceDatas[24]=='')
                                                            {
                                                                // if complete row is empty and warehouse combination exists in Pricing table then do not perform any check.
                                                                $checkErrorFlag=false;
                                                                $deleteWarehouseCombination[$d]['toWareHouse']=$toWareHouse_Id;
                                                                $deleteWarehouseCombination[$d]['fromWareHouse']=$fromWareHouse_Id;
                                                                ++$d;
                                                            }								
							}
							else
							{
								// If active warehouse combination is not exists in tblPricingWTW and any single column is filled then we just give an error message.
								if($LCLServiceDatas[6]!='')
								{
                                                                    $checkErrorFlag=true;
								}
								
								if($LCLServiceDatas[7]!='')
								{
                                                                    $checkErrorFlag=true;
								}
								
								if($LCLServiceDatas[8]!='')
								{
                                                                    $checkErrorFlag=true;
								}
								
								if($LCLServiceDatas[9]!='')
								{
                                                                    $checkErrorFlag=true;
								}
								
								if($LCLServiceDatas[10]!='')
								{
									$checkErrorFlag=true;
								}
								
								if($LCLServiceDatas[11]!='')
								{
									$checkErrorFlag=true;
								}
								
								if($LCLServiceDatas[12]!='')
								{
									$checkErrorFlag=true;
								}
															
								if($LCLServiceDatas[13]!='')
								{
									$checkErrorFlag=true;
								}
								
								if($LCLServiceDatas[15]!='')
								{
									$checkErrorFlag=true;
								}
								
								if($LCLServiceDatas[16]!='')
								{
									$checkErrorFlag=true;
								}
								
								if($LCLServiceDatas[17]!='')
								{
									$checkErrorFlag=true;
								}
								
								if($LCLServiceDatas[18]!='')
								{
									$checkErrorFlag=true;
								}
								
								if($LCLServiceDatas[19]!='')
								{
									$checkErrorFlag=true;
								}
								
								if($LCLServiceDatas[20]!='')
								{
									$checkErrorFlag=true;
								}
															
								if($LCLServiceDatas[21]!='')
								{
									$checkErrorFlag=true;
								}
								
								/*if($LCLServiceDatas[22]!='')
								{
									$checkErrorFlag=true;
								}*/
								
								if($LCLServiceDatas[23]!='')
								{
									$checkErrorFlag=true;
								}
								if($LCLServiceDatas[24]!='')
								{
									$checkErrorFlag=true;
								}
							}
						}
					}
					else
					{
						$error_flag=true;
						$err_msg['destination_origin_warehouse']=t($this->t_base.'messages/origin_des_both_valid');
					}
				}
				else
				{
					$checkErrorFlag=true;
				}
				
				if($checkErrorFlag)
				{
					$allowedOriginChargesFlag = 0;
					$allowedDestinationChargesFlag = 0;
					
					if($LCLServiceDatas[6]!=="" || $LCLServiceDatas[7]!=="" || $LCLServiceDatas[8]!=="" || $LCLServiceDatas[9]!="")
					{
						//If any one out of 4 fields of Origin charges is filled then all fields are required. otherwise we just assumed that there is origin charges applicable on this service.  
						if($LCLServiceDatas[6]==="")
						{					
                                                    $error_flag=true;
                                                    $err_msg['origin_charge_per_wm'] = $szOriginChargePmRequired;
						}
						else if((float)$LCLServiceDatas[6]<0)
						{
                                                    $error_flag=true;
                                                    $err_msg['origin_charge_per_wm'] = $szOriginChargePmPositive;
						}
						if($LCLServiceDatas[7]==="")
						{						
							$error_flag=true;
							$err_msg['origin_charge_minimum']=t($this->t_base.'messages/origin_charge_minimum_rate_is_required');
						}
						else if((float)$LCLServiceDatas[7]<0)
						{
							$error_flag=true;
							$err_msg['origin_charge_minimum']=t($this->t_base.'messages/origin_charge_mini_rate_should_positive_value');
						}
						if($LCLServiceDatas[8]==="")
						{						
							$error_flag=true;
							$err_msg['origin_charge_perBooking']=t($this->t_base.'messages/origin_charge_per_booking_rate_is_required');
						}
						else if((float)$LCLServiceDatas[8]<0)
						{
							$error_flag=true;
							$err_msg['origin_charge_perBooking']=t($this->t_base.'messages/origin_charge_per_booking_rate_should_be_positive_value');
						}
						if($LCLServiceDatas[9]=="")
						{					
							$error_flag=true;
							$err_msg['origin_charge_currency']=t($this->t_base.'messages/origin_charge_currency_is_required');
						}
						else 
						{
							$currencyArr=$Excel_export_import->getFreightCurrency();
							if(!empty($currencyArr))
							{
								if(!in_array(strtoupper($LCLServiceDatas[9]),$currencyArr))
								{
									$error_flag=true;
									$err_msg['origin_charge_currency']=t($this->t_base.'messages/origin_charge_invalid_currency_type');
								}
							}
						}
						$allowedOriginChargesFlag = 1;
					}
					if($LCLServiceDatas[10]=="")
					{					
                                            $error_flag=true;
                                            $err_msg['per_wm'] = $szFreightChargePmRequired;
					}
					else if((float)$LCLServiceDatas[10]<=0)
					{
                                            $error_flag=true;
                                            $err_msg['per_wm'] = $szFreightChargePmPositive;
					}
									
					if($LCLServiceDatas[11]==="")
					{ 
                                            $error_flag=true;
                                            $err_msg['minimum']=t($this->t_base.'messages/minimum_rate_is_required');
					}
					else if((float)$LCLServiceDatas[11]<0)
					{
						$error_flag=true;
						$err_msg['minimum']=t($this->t_base.'messages/mini_rate_should_positive_value');
					}
					if($LCLServiceDatas[12]==="")
					{					
						$error_flag=true;
						$err_msg['perBooking']=t($this->t_base.'messages/per_booking_rate_is_required');
					}
					else if((float)$LCLServiceDatas[12]<0)
					{
						$error_flag=true;
						$err_msg['perBooking']=t($this->t_base.'messages/per_booking_rate_should_be_positive_value');
					}
					if($LCLServiceDatas[13]=="")
					{					
						$error_flag=true;
						$err_msg['currency']=t($this->t_base.'messages/currency_is_required');
					}
					else 
					{
						$currencyArr=$Excel_export_import->getFreightCurrency();
						if(!empty($currencyArr))
						{
							if(!in_array(strtoupper($LCLServiceDatas[13]),$currencyArr))
							{
								$error_flag=true;
								$err_msg['currency']=t($this->t_base.'messages/invalid_currency_type');
							}
						}
					}
					if($LCLServiceDatas[14]!="" || $LCLServiceDatas[15]!="" || $LCLServiceDatas[16]!="" || $LCLServiceDatas[17]!="")
					{
						$allowedDestinationChargesFlag = 1;
						//If any one out of 4 fields of Destination charges is filled then all fields are required. otherwise we just assumed that there is Destination charges applicable on this service.
						if($LCLServiceDatas[14]==="")
						{ 
                                                    $error_flag=true;
                                                    $err_msg['destination_charge_per_wm'] = $szDestinationChargePmRequired;
						}
						else if((float)$LCLServiceDatas[14]<0)
						{
                                                    $error_flag=true;
                                                    $err_msg['destination_charge_per_wm'] = $szDestinationChargePmPositive;
						}
						
						if($LCLServiceDatas[15]==="")
						{					
							$error_flag=true;
							$err_msg['destination_charge_minimum']=t($this->t_base.'messages/destination_charge_minimum_rate_is_required');
						}
						else if((float)$LCLServiceDatas[15]<0)
						{
							$error_flag=true;
							$err_msg['destination_charge_minimum']=t($this->t_base.'messages/destination_charge_mini_rate_should_positive_value');
						}
						
						if($LCLServiceDatas[16]==="")
						{
						
							$error_flag=true;
							$err_msg['destination_charge_perBooking']=t($this->t_base.'messages/destination_charge_per_booking_rate_is_required');
						}
						else if((float)$LCLServiceDatas[16]<0)
						{
							$error_flag=true;
							$err_msg['destination_charge_perBooking']=t($this->t_base.'messages/destination_charge_per_booking_rate_should_be_positive_value');
						}
						
						
						if($LCLServiceDatas[17]=="")
						{
						
							$error_flag=true;
							$err_msg['destination_charge_currency']=t($this->t_base.'messages/destination_charge_currency_is_required');
						}
						else 
						{
							$currencyArr=$Excel_export_import->getFreightCurrency();
							if(!empty($currencyArr))
							{
								if(!in_array(strtoupper($LCLServiceDatas[17]),$currencyArr))
								{
									$error_flag=true;
									$err_msg['destination_charge_currency']=t($this->t_base.'messages/destination_charge_invalid_currency_type');
								}
							}
						}
					}
					if($LCLServiceDatas[18]=="")
					{
						$error_flag=true;
						$err_msg['frequency']=t($this->t_base.'messages/frequency_is_required');
					}
					else 
					{
						$bColumnDataArr=bColumnData();
						if(!empty($bColumnDataArr))
						{
							if(!in_array(ucfirst(strtolower($LCLServiceDatas[18])),$bColumnDataArr))
							{
								$error_flag=true;
								$err_msg['frequency']=t($this->t_base.'messages/invalid_frequency_type');
							}
						}
					}
					if($LCLServiceDatas[19]=="")
					{
						$error_flag=true;
						$err_msg['cutoff']=t($this->t_base.'messages/origin_cutoff_day_is_required');
					}
					else 
					{
						$cColumnDataArr=cColumnData();
						if(!empty($cColumnDataArr))
						{
							if(!in_array(ucfirst(strtolower($LCLServiceDatas[19])),$cColumnDataArr))
							{
								$error_flag=true;
								$err_msg['cutoff']=t($this->t_base.'messages/invalid_origin_cutoff_day');
							}
						}
					}
								
					if($LCLServiceDatas[20]=="")
					{
					
						$error_flag=true;
						$err_msg['orign_l_t']=t($this->t_base.'messages/origin_local_time_is_required');
					}
					else 
					{
						$fColumnDataArr=fColumnData();
						if(!empty($fColumnDataArr))
						{
												    
						   $cutOff_Time=date('H:i',$LCLServiceDatas[20]);
						 	if(!in_array($cutOff_Time,$fColumnDataArr))
							{
								$error_flag=true;
								$err_msg['orign_l_t']=t($this->t_base.'messages/invalid_origin_local_time');
							}
						}
					}
					
					
					if($LCLServiceDatas[21]=="")
					{
					
						$error_flag=true;
						$err_msg['trans_day']=t($this->t_base.'messages/transit_day_is_required');
					}
					else 
					{
						
						if($LCLServiceDatas[21]<=0 || $LCLServiceDatas[21]>100)
						{
							$error_flag=true;
							$err_msg['trans_day']=t($this->t_base.'messages/transit_day_should_between_1_to_100');
						}
					}
					
					
					if($LCLServiceDatas[23]=="")
					{
					
						$error_flag=true;
						$err_msg['des_l_t']=t($this->t_base.'messages/destination_local_time_is_required');
					}
					else 
					{
						$fColumnDataArr=fColumnData();
						if(!empty($fColumnDataArr))
						{
							 $des_Time=date('H:i',$LCLServiceDatas[23]);
							if(!in_array($des_Time,$fColumnDataArr))
							{
								$error_flag=true;
								$err_msg['des_l_t']=t($this->t_base.'messages/invalid_destination_local_time');
							}
						}
					}
					
					
					if($LCLServiceDatas[24]=="")
					{
					
						$error_flag=true;
						$err_msg['booking_cut_off']=t($this->t_base.'messages/booking_cut_off_day_is_required');
					}
					else 
					{
						$gColumnDataArr=gColumnData();
						if(!empty($gColumnDataArr))
						{
							if(!in_array(strtolower($LCLServiceDatas[24]),$gColumnDataArr))
							{
								$error_flag=true;
								$err_msg['booking_cut_off']=t($this->t_base.'messages/invalid_booking_cut_off_day');
							}
						}
					}
					
					if($LCLServiceDatas[25]=="")
					{
						$error_flag=true;
						$err_msg['v_date']=t($this->t_base.'messages/validity_from_date_is_required');
					}
					
					
					if($LCLServiceDatas[26]=="")
					{
						$error_flag=true;
						$err_msg['e_date']=t($this->t_base.'messages/expiry_date_is_required');
					}
					else
					{
						if($LCLServiceDatas[26]<=strtotime(date('Y-m-d')))
						{
							$error_flag=true;
							$err_msg['e_date']=t($this->t_base.'messages/expiry_date_should_be_greater_than_today');
						}
					}
					
					
					if($LCLServiceDatas[26]!="" && $LCLServiceDatas[25]!="")
					{
						if($LCLServiceDatas[25]>$LCLServiceDatas[26])
						{
							$error_flag=true;
							$err_msg['e_date']=t($this->t_base.'messages/validity_from_date_should_be_less_than_expiry_date');
						}
					}
					
					if(!$error_flag)
					{
						$newLCLServiceData[$k]=$LCLServiceDatas;
						$newLCLServiceData[$k]['iOriginChargesApplicable'] = $allowedOriginChargesFlag;
						$newLCLServiceData[$k]['iDestinationChargesApplicable'] = $allowedDestinationChargesFlag;
					}
					
			}
					if(!empty($err_msg))
					{
						$err_msg_arr=implode(", ",$err_msg);
						$errmsg_arr[]=t($this->t_base.'messages/following_error_in_row_number')." ".$k." - ".$LCLServiceDatas[0]."/".$LCLServiceDatas[1]."/".$LCLServiceDatas[2]."/  ".$LCLServiceDatas[3]."/".$LCLServiceDatas[4]."/".$LCLServiceDatas[5].""." : ".$err_msg_arr;
						++$l_err;
					}
				++$k;
			}
		}
		
		$LCLServiceDataCount=count($newLCLServiceData);
		$this->rowCountUploaded=$LCLServiceDataCount;
		$this->rowErrorCount=count($errmsg_arr);
                        
                
		if(!empty($errmsg_arr))
		{
                    $l=1;
                    if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
                    {
                        $err_str .="We found errors in following ".$this->errorLine." airfreight service lines in your upload file:";
                    }
                    else
                    {
                        $err_str .="We found errors in following ".$this->errorLine." LCL service lines in your upload file:";
                    }
                    
                    foreach($errmsg_arr as $errmsg_arrs)
                    {
                        $err_str .="<br/>".$l.". ".$errmsg_arrs."<br/>";
                        ++$l;
                    }
                    $err_str .="<br/>Please correct the errors and upload the file again.";
                    $idAdmin = $_SESSION['admin_id'];
                    $kAdmin=new cAdmin();
                    $kAdmin->getAdminDetails($idAdmin);
                    $replace_arr['szEmail']=$kAdmin->szEmail;
                    $replace_arr['szFirstName']=$kAdmin->szFirstName;
                    $replace_arr['szLastName']=$kAdmin->szLastName;
                    $replace_arr['szErrorMsg']=$err_str;
                    createEmail(__MANAGEMENT_BULK_UPLOAD_SERVICE_FILE_EMAIL__, $replace_arr,$kAdmin->szEmail, __MANAGEMENT_BULK_UPLOAD_SERVICE_FILE_SUBJECT__, __STORE_SUPPORT_EMAIL__,$this->id, __STORE_SUPPORT_EMAIL__);            
                    return false;
		}
		else
		{ 
                    return true;
		}
	}
	
	function addDataForApprovalHaulageServicesData($objPHPExcel,$sheetindex,$colcount,$rowcount,$idForwarder)
	{
	
		//echo "hello";
		date_default_timezone_set('UTC');
		$ctr=0;
		if($rowcount>2)
		{
			for($i=3;$i<=$rowcount;$i++)
			{
				for($j=0;$j<$colcount;$j++)
				{
					if($j<=13)
					{
						if($objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0,$i)->getValue()!="")
						{
							$HaulageServiceData[$ctr][] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($j,$i)->getValue();
							
						}
					}
					
				}
				++$ctr;
			}
		}
		//print_r($HaulageServiceData);
		$Excel_export_import= new cExport_Import();
		if(!empty($HaulageServiceData))
		{
			$k=3;
			$l_err=0;
			$l_success=0;
			$d=0;
			$deleteWarehouseCombinations=array();
			foreach($HaulageServiceData as $HaulageServiceDatas)
			{
				$error_flag=false;
				$err_msg=array();
				$checkErrorFlag=false;
				if($HaulageServiceDatas[0]=='')
				{
					$error_flag=true;
					$err_msg['country']=t($this->t_base.'messages/country_is_required');
				}				
				
				if($HaulageServiceDatas[1]=='')
				{
					$error_flag=true;
					$err_msg['city']=t($this->t_base.'messages/city_is_required');
				}
				
				if($HaulageServiceDatas[2]=='')
				{
					$error_flag=true;
					$err_msg['name']=t($this->t_base.'messages/name_is_required');
				}
				
				if($HaulageServiceDatas[3]=='')
				{
					$error_flag=true;
					$err_msg['direction']=t($this->t_base.'messages/direction_is_required');
				}
				else
				{
					$eColumnHaulageDataArr=eColumnHaulageData();
					if(!in_array($HaulageServiceDatas[3],$eColumnHaulageDataArr))
					{
						$error_flag=true;
						$err_msg['direction']=t($this->t_base.'messages/direction_invalid_type');
					}
				}
				
				if($HaulageServiceDatas[4]=='')
				{
					$error_flag=true;
					$err_msg['zoneModel']=t($this->t_base.'messages/pricing_model_is_required');
				}
				else
				{
					$columnHaulageDataArr=array('City','Distance','Postcode','Zone');
					if(!in_array($HaulageServiceDatas[4],$columnHaulageDataArr))
					{
						$error_flag=true;
						$err_msg['zoneModel']=t($this->t_base.'messages/invalid_pricing_name');
					}
				}
				
				
				if($HaulageServiceDatas[5]=='')
				{
					$error_flag=true;
					$err_msg['zoneModel']=t($this->t_base.'messages/name_or_reference_is_required');
				}
				
				
				if($HaulageServiceDatas[2]!="" && $HaulageServiceDatas[1]!="" && $HaulageServiceDatas[0]!="" && $HaulageServiceDatas[3]!="" && $HaulageServiceDatas[4]!='' && $HaulageServiceDatas[5]!='')
				{
					$haulage_Country=sanitize_all_html_input($Excel_export_import->getCountryId($HaulageServiceDatas[0]));
					$haulage_City=sanitize_all_html_input($HaulageServiceDatas[1]);
					$haulage_CFS=sanitize_all_html_input($HaulageServiceDatas[2]);
					
					$idPricingModel=$Excel_export_import->getPricingModelId(sanitize_all_html_input($HaulageServiceDatas[4]));
				
					$query="
						SELECT
							id
						FROM
							".__DBC_SCHEMATA_WAREHOUSES__."
						WHERE
							szWareHouseName='".mysql_escape_custom($haulage_CFS)."' 	
						AND
							szCity='".mysql_escape_custom($haulage_City)."'
						AND
							idCountry='".(int)$haulage_Country."'
						AND
							idForwarder='".(int)$idForwarder."'
                                                AND
                                                    iActive='1'
                                                AND
                                                    isDeleted='0'            
					";
					//echo $query."<br/>";
					if($result=$this->exeSQL($query))
					{
						if($this->iNumRows>0)
						{
							$row=$this->getAssoc($result);
							$WareHouse_Id=$row['id'];
						}
						else
						{
							$WareHouse_Id=0;
							$error_flag=true;
							$err_msg['cfs_name']=t($this->t_base.'messages/warehouse_name_is_invalid');
						}
					}
						
					if((int)$WareHouse_Id>0 && $idPricingModel>0)
					{
						$haulage_Direction='';
						if($HaulageServiceDatas[3]=='Export')
						{
							$haulage_Direction='1';
						}
						else if($HaulageServiceDatas[3]=='Import')
						{
							$haulage_Direction='2';
						}
						$dataArr=array();
						$dataArr[0]['idWarehouse']=$WareHouse_Id;
						$dataArr[0]['idPricingModel']=$idPricingModel;
						$count=$Excel_export_import->getAllUpdatedHaulageServices($dataArr);
						$haulagePricingModelName=sanitize_all_html_input($HaulageServiceDatas[5]);
						
						if($idPricingModel=='4')
						{
							$replaceArr=array("Up to","km",",");
							$haulagePricingModelName=trim(str_replace($replaceArr,"",$haulagePricingModelName));
						}
						$idHaulaePricing=$Excel_export_import->getHaulagePricingModelId($haulagePricingModelName,$idPricingModel,$WareHouse_Id,$haulage_Direction);
						//echo $idHaulaePricing."idHaulaePricing<br/>";	
						if($idHaulaePricing>0)
						{
							if((int)$count>0)
							{
								if($HaulageServiceDatas[6]=='' && $HaulageServiceDatas[7]=='' && $HaulageServiceDatas[8]=='' && $HaulageServiceDatas[9]=='' && $HaulageServiceDatas[10]=='')
								{
									$checkErrorFlag=false;
									$deleteWarehouseCombination[$d]['idHaulaePricing']=$idHaulaePricing;
									++$d;
								}
								else
								{
									if($HaulageServiceDatas[6]!='')
									{
										$checkErrorFlag=true;
									}
									
									if($HaulageServiceDatas[7]!='')
									{
										$checkErrorFlag=true;
									}
									
									if($HaulageServiceDatas[8]!='')
									{
										$checkErrorFlag=true;
									}
									
									if($HaulageServiceDatas[9]!='')
									{
										$checkErrorFlag=true;
									}
									
									if($HaulageServiceDatas[10]!='')
									{
										$checkErrorFlag=true;
									}
								}
							}
						}
						else
						{
							$error_flag=true;
							$err_msg_model['model_name']=t($this->t_base.'messages/name_or_reference_is_invalid');
							$err_msg['model_name']=t($this->t_base.'messages/name_or_reference_is_invalid');
							//print_r($err_msg);
						}
					}
					else
					{
						$error_flag=true;
						$err_msg['cfs_name']=t($this->t_base.'messages/warehouse_name_is_invalid');
					}
				}
				
				if($checkErrorFlag)
				{	
				
					if($HaulageServiceDatas[6]=="")
					{
						$error_flag=true;
						$err_msg['weight_up_to']=t($this->t_base.'messages/weight_up_to');
					}
					else if((float)$HaulageServiceDatas[6]<=0)
					{
						$error_flag=true;
						$err_msg['weight_up_to']=t($this->t_base.'messages/weight_up_to_valid');
					}
					
					if($HaulageServiceDatas[7]==="")
					{
						$error_flag=true;
						$err_msg['per_100_kg']=t($this->t_base.'messages/per_100_kg');
					}
					else if((float)$HaulageServiceDatas[7]<0)
					{
						$error_flag=true;
						$err_msg['per_100_kg']=t($this->t_base.'messages/per_100_kg_valid');
					}
				
					if($HaulageServiceDatas[8]==="")
					{
					
						$error_flag=true;
						$err_msg['minimum']=t($this->t_base.'messages/minimum_rate_is_required');
					}
					else if((float)$HaulageServiceDatas[8]<0)
					{
						$error_flag=true;
						$err_msg['minimum']=t($this->t_base.'messages/mini_rate_should_positive_value');
					}
									
					
					if($HaulageServiceDatas[9]==="")
					{
						$error_flag=true;
						$err_msg['perBooking']=t($this->t_base.'messages/per_booking_rate_is_required');
					}
					else if((float)$HaulageServiceDatas[9]<0)
					{
						$error_flag=true;
						$err_msg['perBooking']=t($this->t_base.'messages/per_booking_rate_should_be_positive_value');
					}
					
					if($HaulageServiceDatas[10]=="")
					{
					
						$error_flag=true;
						$err_msg['currency']=t($this->t_base.'messages/currency_is_required');
					}
					else 
					{
						$currencyArr=$Excel_export_import->getFreightCurrency();
						if(!empty($currencyArr))
						{
							if(!in_array($HaulageServiceDatas[10],$currencyArr))
							{
								$error_flag=true;
								$err_msg['currency']=t($this->t_base.'messages/invalid_currency_type');
							}
						}
					}
				}
				
				if(!$error_flag)
				{
					$newHaulageServiceDatas[$k]=$HaulageServiceDatas;
				}
				if(!empty($err_msg))
				{
					$err_msg_arr=implode(", ",$err_msg);
					$errmsg_arr[]=t($this->t_base.'messages/following_error_in_row_number') ." ".$k." - ".$HaulageServiceDatas[0]."/".$HaulageServiceDatas[1]."/".$HaulageServiceDatas[2]."/".$HaulageServiceDatas[3]."/".$HaulageServiceDatas[4]."/".$HaulageServiceDatas[5]." : ".$err_msg_arr;
					++$l_err;
				}
				++$k;
			}
		}
		//print_r($err_msg);
		//echo "<br/><br/>";
		//print_r($newHaulageServiceDatas);
		$HaulageServiceDataCount=count($newHaulageServiceDatas);
		$this->rowCountUploaded=$HaulageServiceDataCount;
		$this->rowErrorCount=count($errmsg_arr);
		//die;
		if(!empty($errmsg_arr))
		{
			$l=1;
			$err_str .="We found errors in following ".$this->errorLine." Haulage service lines in your upload file:";
			foreach($errmsg_arr as $errmsg_arrs)
			{
				$err_str .="<br/>".$l.". ".$errmsg_arrs."<br/>";
				++$l;
			}
			$err_str .="<br/>Please correct the errors and upload the file again.";
			$idAdmin = $_SESSION['admin_id'];
			$kAdmin=new cAdmin();
			$kAdmin->getAdminDetails($idAdmin);
			$replace_arr['szEmail']=$kAdmin->szEmail;
			$replace_arr['szFirstName']=$kAdmin->szFirstName;
			$replace_arr['szLastName']=$kAdmin->szLastName;
			$replace_arr['szErrorMsg']=$err_str;
			createEmail(__MANAGEMENT_BULK_UPLOAD_SERVICE_FILE_EMAIL__, $replace_arr,$kAdmin->szEmail, __MANAGEMENT_BULK_UPLOAD_SERVICE_FILE_SUBJECT__, __STORE_SUPPORT_EMAIL__,$this->id, __STORE_SUPPORT_EMAIL__);            
			return false;
		}
		else
		{
			
			return true;
		}
		
	}
	function FindOutUploadSevicesNotConfirmed80Days()
	{ 
		$query = "
			SELECT 
				idForwarder,
				dtCompleted
			FROM	
				".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
			WHERE
				DATE_FORMAT(dtCompleted,'%Y-%m-%d') =DATE_FORMAT(NOW() - INTERVAL 80 DAY,'%Y-%m-%d')
			AND
				iStatus = '".__MANAGEMENT_COMPLETE_APPROVAL__."'
			AND
				iActive = 1				
		";	
		//echo $query;
		if($result=$this->exeSQL($query))
		{	
			$contact = array();
			while($row=$this->getAssoc($result))
			{
				$contact[] = $row;	
			}	
			return $contact;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}	
	}
	function sendEmailsToForwarderAdminAndPricingAfter80Days($id,$dtCompleted)
	{
		$query = "
			SELECT
				id,
				szFirstName,
				szEmail
			FROM
				".__DBC_SCHEMATA_FORWARDERS_CONTACT__."		
			WHERE
					idForwarder = ".(int)$id."
			AND
				idForwarderContactRole   IN (".__ADMINISTRATOR_PROFILE_ID__.",".__PRICING_PROFILE_ID__.")
			AND
				iActive='1'
			AND
				iConfirmed ='1'			
		";
		if($result=$this->exeSQL($query))
		{
			$profileArr = array();
			while($row=$this->getAssoc($result))
			{
				$profileArr[] = $row;
			}
			if(!empty($profileArr))
			{
				$kForwarder = new cForwarder();
				$kForwarder->load($this->idForwarder);
				$replace_ary['szDate'] =date('d. F Y',strtotime($dtCompleted));
				
				foreach($profileArr as $profArr)
				{	
					//$replace_ary['id'] = $profArr['id'];
					$replace_ary['szFirstName'] = ($profArr['szFirstName']?$profArr['szFirstName']:'User');
					createEmail(__FORWARDER_UPLOAD_SERVICE_ON_80_DAYS__, $replace_ary,$profArr['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$profArr['id'], __STORE_SUPPORT_EMAIL__,false);
				}
			}
		}
	}
	
	function FindOutUploadSevicesNotConfirmed90Days()
	{ 
		$query = "
			SELECT 
				id
			FROM	
				".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
			WHERE
				DATE_FORMAT(dtCompleted,'%Y-%m-%d') <= DATE_FORMAT(NOW() - INTERVAL 90 DAY,'%Y-%m-%d')
			AND
				iStatus = '".__MANAGEMENT_COMPLETE_APPROVAL__."'
			AND
				iActive = 1				
		";	
		//echo $query;
		if($result=$this->exeSQL($query))
		{	
			$contact = array();
			while($row=$this->getAssoc($result))
			{
				$contact[] = $row;	
			}	
			return $contact;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}	
	}
	function updateUploadService($id)
	{
		$this->set_id((int)sanitize_all_html_input($id));
		if($this->id>0)
		{
			$query = "
				UPDATE
					".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__." 
				SET 
					iActive = 0	
				WHERE 
					id = ".$this->id.";
				";
				if($result=$this->exeSQL($query))
				{
					return true;
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
		}
	}
	
	
	function setCountryNameISO($szCountry)
	{
		if($szCountry!='')
		{
			$query="
				SELECT 
					szCountryISO,
					szCountryName
				FROM
					".__DBC_SCHEMATA_COUNTRY__."	
				WHERE
					id = '".(int)$szCountry."'
					";
			//echo $query;
			if($result=$this->exeSQL($query))
			{
				if($row=$this->getAssoc($result))
				{
					return $row;
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}	
	}
	
	function addDataForApprovalPricingCCTemp($objPHPExcel,$sheetindex,$colcount,$rowcount,$idForwarder,$idService)
	{
		date_default_timezone_set('UTC');
		$ctr=0;
		if($rowcount>2)
		{
			for($i=3;$i<=$rowcount;$i++)
			{
				for($j=0;$j<$colcount;$j++)
				{
					if($objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0,$i)->getValue()!="")
					{
						
						$customClearanceData[$ctr][] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($j,$i)->getValue();
						
					}
					
				}
				++$ctr;
			}
		}
		
		
		$Excel_export_import= new cExport_Import();
		
		if(!empty($customClearanceData))
		{
			foreach($customClearanceData as $customClearanceDatas)
			{
				$originCountry_Id=$Excel_export_import->getCountryId(sanitize_all_html_input($customClearanceDatas[0]));
			
				$desCountry_Id=$Excel_export_import->getCountryId(sanitize_all_html_input($customClearanceDatas[3]));
				$fromWareHouse_Id=0;
				$toWareHouse_Id=0;
				$query="
					SELECT
                                            id
					FROM
                                            ".__DBC_SCHEMATA_WAREHOUSES__."
					WHERE
                                            szWareHouseName='".mysql_escape_custom(sanitize_all_html_input($customClearanceDatas[2]))."' 	
					AND
                                            szCity='".mysql_escape_custom(sanitize_all_html_input($customClearanceDatas[1]))."'
					AND
                                            idCountry='".(int)$originCountry_Id."'
					AND
                                            idForwarder='".(int)$idForwarder."'
                                        AND
                                            iActive='1'        
				";
				//echo $query."<br/><br/>";
				if($result=$this->exeSQL($query))
				{
                                    if($this->iNumRows>0)
                                    {
                                        $row=$this->getAssoc($result);
                                        $fromWareHouse_Id=$row['id'];
                                    }
                                    else
                                    {
                                        $fromWareHouse_Id=0;
                                    }
				}
				
				$query="
                                    SELECT
                                        id
                                    FROM
                                        ".__DBC_SCHEMATA_WAREHOUSES__."
                                    WHERE
                                        szWareHouseName='".mysql_escape_custom($customClearanceDatas[5])."' 	
                                    AND
                                        szCity='".mysql_escape_custom($customClearanceDatas[4])."'
                                    AND
                                        idCountry='".(int)$desCountry_Id."'
                                    AND
                                        idForwarder='".(int)$idForwarder."'
                                    AND
                                        iActive='1'        
				";
				//echo $query."<br/>";
				if($result=$this->exeSQL($query))
				{
                                    if($this->iNumRows>0)
                                    { 
                                        $row=$this->getAssoc($result);
                                        $toWareHouse_Id=$row['id'];
                                    }
                                    else
                                    {
                                        $toWareHouse_Id=0;
                                    }
				}
				
				$currencyImportId='';
				$currencyImportId=$Excel_export_import->getFreightCurrencyId(sanitize_all_html_input(ucwords($customClearanceDatas[9])));
					
					
				$currencyExportId='';
				$currencyExportId=$Excel_export_import->getFreightCurrencyId(sanitize_all_html_input(ucwords($customClearanceDatas[7])));
				
				if((int)$fromWareHouse_Id>0 && ($toWareHouse_Id)>0)
				{
					//echo "hi";
					if(((float)$customClearanceDatas[6]>0 && $currencyImportId!='') || ((float)$customClearanceDatas[8]>0 || $currencyExportId!=''))
					{
						$exportprice='';
						$importprice='';
						if(is_numeric($customClearanceDatas[6]))
						{
                                                    $exportprice=(float)$customClearanceDatas[6];
						}
						
						if(is_numeric($customClearanceDatas[8]))
						{
                                                    $importprice=(float)$customClearanceDatas[8];
						}
						
						$query="
							INSERT INTO
								".__DBC_SCHEMATA_PRICING_CC_WAITING_FOR_APPROVAL__."
							(
                                                        idForwarder,
                                                        idWarehouseFrom,
                                                        idWarehouseTo,
                                                        fPriceImport,
                                                        szCurrencyImport,
                                                        iApproved,
                                                        dtCreatedOn,
                                                        szCityFrom,
                                                        idCountryFrom,
                                                        idCountryTo,
                                                        fPriceExport,
                                                        szCurrencyExport,
                                                        szCityTo,
                                                        idBulkUploadService				
                                                        )
									VALUES
								(
									'".(int)$idForwarder."',
									'".(int)$fromWareHouse_Id."',
									'".(int)$toWareHouse_Id."',
									'".$importprice."',
									'".(int)$currencyImportId."',
									'0',
									NOW(),
									'".mysql_escape_custom(sanitize_all_html_input($customClearanceDatas[1]))."',
									'".(int)$originCountry_Id."',
									'".(int)$desCountry_Id."',
									'".$exportprice."',
									'".$currencyExportId."',
									'".mysql_escape_custom(sanitize_all_html_input($customClearanceDatas[4]))."',
									'".(int)$idService."'						
								)
							";
						//echo $query."<br/><br/>";
						$result=$this->exeSQL($query);
					}
					
				}
			}
			
			$query="
				UPDATE
					".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
				SET
					iStatus='".(int)__DATA_REVIEWED_BY_FORWARDER__."'
				WHERE
					id='".(int)$idService."'		
			";
			$result=$this->exeSQL($query);
			return true;
		}		
	}
	
	function totalWaitingToBeApproved($idForwarder,$idService,$idData=0)
	{
		$sql='';
		if((int)$idData>0)
		{
			$sql="
				AND
				id='".(int)$idData."'
			";
		}
		$query="
			SELECT
				id,
				idWarehouseFrom,
				idWarehouseTo,
				fPriceImport,
				szCurrencyImport,
				iApproved,
				dtCreatedOn,
				szCityFrom,
				idCountryFrom,
				idCountryTo,
				fPriceExport,
				szCurrencyExport,
				szCityTo 	
			FROM
				".__DBC_SCHEMATA_PRICING_CC_WAITING_FOR_APPROVAL__."
			WHERE
				idForwarder='".(int)$idForwarder."'
			AND
				idBulkUploadService='".(int)$idService."'
			".$sql."
			ORDER BY
				id ASC
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				if((int)$idData==0)
				{
					while($row=$this->getAssoc($result))
					{
						$res_arr[]=$row['id'];
					}
					
					return $res_arr;
				}
				else
				{
					$row=$this->getAssoc($result);
					$res_arr[]=$row;
					return $res_arr;
				}
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function deletePricingCCUnApprovedData($id,$idForwarder,$idService)
	{
		$query="
			DElETE FROM
				".__DBC_SCHEMATA_PRICING_CC_WAITING_FOR_APPROVAL__."
			WHERE
				id='".(int)$id."'
			AND
				idForwarder='".(int)$idForwarder."'
		";
		if($result=$this->exeSQL($query))
		{
			/*$query="
				UPDATE
					".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
				SET
					iActualRecords=iActualRecords-1
				WHERE
					id='".(int)$idService."'
			";
			$result=$this->exeSQL($query);*/
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
	function addOBOCustomClearanceApprove($data)
	{
		//print_r($data);	
		$success=false;
		$update_oring_cc = false ;
		$update_destination_cc = false ;
		$this->set_idOriginWarehouse(sanitize_all_html_input(trim($data['idOriginWarehouse'])));
		$this->set_idDestinationWarehouse(sanitize_all_html_input(trim($data['idDestinationWarehouse'])));
		
		if((float)$data['fOriginPrice']>0 || (int)$data['idOriginCurrency']>0)
		{
		
			if(($data['idOriginCurrency']>0 && $data['fOriginPrice']=='') || ($data['idOriginCurrency']=='' && $data['fOriginPrice']!=''))
			{
				$this->addError('fOriginPrice',t($this->t_base.'messages/export_err_msg'));
			}
			//$this->set_fOriginPrice(sanitize_all_html_input(trim($data['fOriginPrice'])));
			//$this->set_idOriginCurrency(sanitize_all_html_input(trim($data['idOriginCurrency'])));
			//$this->
			$update_oring_cc = true;
		}		
		if((float)$data['fDestinationPrice']>0 || (int)$data['idDestinationCurrency']>0)
		{
		
			if(($data['idDestinationCurrency']>0 && $data['fDestinationPrice']=='') || ($data['idDestinationCurrency']=='' && $data['fDestinationPrice']!=''))
			{
				$this->addError('fOriginPrice',t($this->t_base.'messages/import_err_msg'));
			}
			//$this->set_fDestinationPrice(sanitize_all_html_input(trim($data['fDestinationPrice'])));
			//$this->set_idDestinationCurrency(sanitize_all_html_input(trim($data['idDestinationCurrency'])));
			$update_destination_cc = true;
		}
		
		if($update_destination_cc==false && $update_oring_cc==false)
		{
			$this->addError('fDestinationPrice','At least one line should be filled');
			return false;
		}
		/*
		$this->set_fOriginPrice(sanitize_all_html_input(trim($data['fOriginPrice'])));
		$this->set_fDestinationPrice(sanitize_all_html_input(trim($data['fDestinationPrice'])));
		
		$this->set_idOriginCurrency(sanitize_all_html_input(trim($data['idOriginCurrency'])));
		$this->set_idDestinationCurrency(sanitize_all_html_input(trim($data['idDestinationCurrency'])));
		*/
		
		if($this->error === true)
		{
			return false;
		}
			
		if($this->idOriginWarehouse == $this->idDestinationWarehouse)
		{
			$this->addError('szDestinationWarehouse',t($this->t_base.'messages/same_whs_error_message'));
			return false;
		}
		$success = false;
		
		if((int)$_SESSION['forwarder_admin_id']>0)
		{
			$idAdmin = (int)$_SESSION['forwarder_admin_id'];
			$idForwarderContact = 0 ;
		}
		else
		{
			$idAdmin = 0;
			$idForwarderContact = (int)$_SESSION['forwarder_user_id'];
		}
		$idForwarder = $_SESSION['forwarder_id'];
		
		$query="
			UPDATE
				".__DBC_SCHEMATA_PRICING_CC__."
			SET
				iActive='0'
			WHERE
				idWarehouseFrom='".(int)$this->idOriginWarehouse."'
			AND
				idWarehouseTo='".(int)$this->idDestinationWarehouse."'
		";
		$reusult=$this->exeSQL($query);
			
		if($update_oring_cc)
		{
					
			$query="
				SELECT
					id,
					fPrice,
					iActive,
					dtCreatedOn,
					szCurrency
				FROM
					".__DBC_SCHEMATA_PRICING_CC__."
				WHERE
					idWarehouseFrom='".(int)$this->idOriginWarehouse."'
				AND
					idWarehouseTo='".(int)$this->idDestinationWarehouse."'
				AND
					szOriginDestination='1'
			";
			if($reusult=$this->exeSQL($query))
			{
			
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($reusult);
					$id=$row['id'];
					
						$query="
							INSERT INTO
								".__DBC_SCHEMATA_FORWARDER_PRCIING_CC_ARCHIVE__."
							(
								idWarehouseFrom,
								idWarehouseTo,
								szOriginDestination,
								fPrice,
								szCurrency, 
								dtUpdateOn,
								iActive,
								idForwarder,
								idForwarderContact,
								idAdmin,
								dtCreatedOn
							)
							VALUES
							(
								'".(int)$this->idOriginWarehouse."',
								'".(int)$this->idDestinationWarehouse."',
								'1',
								'".(float)$row['fPrice']."',
								'".(int)$row['szCurrency']."',
								NOW(),
								'".(int)$row['iActive']."',
								'".(int)$idForwarder."',
								'".(int)$idForwarderContact."',
								'".(int)$idAdmin."',
								'".mysql_escape_custom($row['dtCreatedOn'])."'					
							)
						";
						//echo $query."<br/>";
						$result=$this->exeSQL($query);
					if((int)$id>0)
					{
						$query="
							UPDATE
								".__DBC_SCHEMATA_PRICING_CC__."
							SET
								fPrice='".(float)$this->fOriginPrice."',
								szCurrency='".(int)$this->idOriginCurrency."',
								dtUpdateOn=NOW(),
								iActive='1'
							WHERE
								idWarehouseFrom='".(int)$this->idOriginWarehouse."'
							AND
								idWarehouseTo='".(int)$this->idDestinationWarehouse."'
							AND
								szOriginDestination='1'
						";
						$reusult=$this->exeSQL($query);
						$success = true;
					}
				}
				else
				{
					$query="
						INSERT INTO
							".__DBC_SCHEMATA_PRICING_CC__."
						(
							idWarehouseFrom,
							idWarehouseTo,
							szOriginDestination,
							fPrice,
							szCurrency,
							iActive,
							dtCreatedOn
						)	
						VALUES
						(
							'".(int)$this->idOriginWarehouse."',
							'".(int)$this->idDestinationWarehouse."',
							'1',
							'".(float)$this->fOriginPrice."',
							'".(int)$this->idOriginCurrency."',
							'1',
							NOW()
						)
					";
					//echo "<br>".$query."<br>";
					if($reusult=$this->exeSQL($query))
					{
						$success = true;
						$kForwarder = new cForwarder();
						$idForwarder = $_SESSION['forwarder_id'];
						$kForwarder->load($idForwarder);
						
						if(($kForwarder->iAgreeTNC==1) && ($kForwarder->iUpdateRss==2))
						{
							$kConfig = new cConfig();
							$kWhsSearch = new cWHSSearch();
								
							$kWhsSearch->load($this->idOriginWarehouse);
							$szOriginCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
							
							$kWhsSearch->load($this->idDestinationWarehouse);
							$szDestinationCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
								
							$replace_ary = array();
							$replace_ary['szForwarderDisplayName'] = $kForwarder->szDisplayName ;
							$replace_ary['szCountryName'] = $szOriginCountry ;
							$replace_ary['szOriginCountry'] = $szOriginCountry ;
							$replace_ary['szDestinationCountry'] = $szDestinationCountry ;

							$kExport_Import = new cExport_Import();
							$content_ary = array();
							$content_ary = $kExport_Import->replaceRssContent('__OBO_CUSTOM_CLEARANCE__',$replace_ary);
								
							$rssDataAry=array();
							$rssDataAry['idForwarder'] = $kForwarder->id ;
							$rssDataAry['szFeedType'] = '__OBO_CUSTOM_CLEARANCE__';
							$rssDataAry['szLink'] = __RSS_FEED_PAGE_URL__ ;
							$rssDataAry['szTitle'] = $content_ary['szTitle'];
							$rssDataAry['szDescription'] = $content_ary['szDescription'];
							$rssDataAry['szDescriptionHTML'] = $content_ary['szDescriptionHTML'];
							
							$kRss = new cRSS();
							$kRss->addRssFeed($rssDataAry);
						}
						$success = true;
					}
					else
					{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					}
				}
			}
		}
		
		if($update_destination_cc)
		{
				
			$query="
				SELECT
					id,
					fPrice,
					iActive,
					dtCreatedOn,
					szCurrency
				FROM
					".__DBC_SCHEMATA_PRICING_CC__."
				WHERE
					idWarehouseFrom='".(int)$this->idOriginWarehouse."'
				AND
					idWarehouseTo='".(int)$this->idDestinationWarehouse."'
				AND
					szOriginDestination='2'
			";
			//echo $query;
			if($reusult=$this->exeSQL($query))
			{			
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($reusult);
					$id=$row['id'];
					
						$query="
							INSERT INTO
								".__DBC_SCHEMATA_FORWARDER_PRCIING_CC_ARCHIVE__."
							(
								idWarehouseFrom,
								idWarehouseTo,
								szOriginDestination,
								fPrice,
								szCurrency, 
								dtUpdateOn,
								iActive,
								idForwarder,
								idForwarderContact,
								idAdmin,
								dtCreatedOn
							)
							VALUES
							(
								'".(int)$this->idOriginWarehouse."',
								'".(int)$this->idDestinationWarehouse."',
								'2',
								'".(float)$row['fPrice']."',
								'".(int)$row['szCurrency']."',
								NOW(),
								'".(int)$row['iActive']."',
								'".(int)$idForwarder."',
								'".(int)$idForwarderContact."',
								'".(int)$idAdmin."',
								'".mysql_escape_custom($row['dtCreatedOn'])."'					
							)
						";
						//echo $query."<br/>";
						$result=$this->exeSQL($query);
					if((int)$id>0)
					{
						$query="
							UPDATE
								".__DBC_SCHEMATA_PRICING_CC__."
							SET
								fPrice='".(float)$this->fDestinationPrice."',
								szCurrency='".(int)$this->idDestinationCurrency."',
								dtUpdateOn=NOW(),
								iActive='1'
							WHERE
								idWarehouseFrom='".(int)$this->idOriginWarehouse."'
							AND
								idWarehouseTo='".(int)$this->idDestinationWarehouse."'
							AND
								szOriginDestination='2'
						";
						//echo $query;
						$reusult=$this->exeSQL($query);
						$success = true;
					}
				}
				else
				{
					$query="
						INSERT INTO
							".__DBC_SCHEMATA_PRICING_CC__."
						(
							idWarehouseFrom,
							idWarehouseTo,
							szOriginDestination,
							fPrice,
							szCurrency,
							iActive,
							dtCreatedOn
						)	
						VALUES
						(
							'".(int)$this->idOriginWarehouse."',
							'".(int)$this->idDestinationWarehouse."',
							'2',
							'".(float)$this->fDestinationPrice."',
							'".(float)$this->idDestinationCurrency."',
							'1',
							NOW()
						)
					";
					//echo "<br>".$query."<br>";
					if($reusult=$this->exeSQL($query))
					{
						$success = true;
					}
					else
					{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					}
				}
			}
		}
		return $success;
	}


	function addDataForApprovalPricingWTWTemp($objPHPExcel,$sheetindex,$colcount,$rowcount,$idForwarder,$idService,$iWarehouseType=false)
	{
            date_default_timezone_set('UTC');
            if($rowcount>2)
            {
                for($i=3;$i<=$rowcount;$i++)
                {
                    for($j=0;$j<$colcount;$j++)
                    {
                        if($objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0,$i)->getValue()!="")
                        {
                            if(is_numeric($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($j,$i)->getValue()) && ($j==26 || $j==25 || $j==23 || $j==20))
                            {
                                $LCLServiceData[$ctr][] = PHPExcel_Shared_Date::ExcelToPHP($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($j,$i)->getValue());
                            }
                            else
                            {
                                $LCLServiceData[$ctr][] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($j,$i)->getValue();
                            }
                        } 
                    }
                    ++$ctr;
                }
            }
            //print_r($LCLServiceData);
		
            $Excel_export_import= new cExport_Import();
            if(!empty($LCLServiceData))
            {
                foreach($LCLServiceData as $LCLServiceDatas)
                {								
                    $originCountry_Id=$Excel_export_import->getCountryId(sanitize_all_html_input($LCLServiceDatas[0]));

                    $desCountry_Id=$Excel_export_import->getCountryId(sanitize_all_html_input($LCLServiceDatas[3]));
                    $query="
                        SELECT
                            id
                        FROM
                            ".__DBC_SCHEMATA_WAREHOUSES__."
                        WHERE
                            szWareHouseName='".mysql_escape_custom(sanitize_all_html_input($LCLServiceDatas[2]))."' 	
                        AND
                            szCity='".mysql_escape_custom(sanitize_all_html_input($LCLServiceDatas[1]))."'
                        AND
                            idCountry='".(int)$originCountry_Id."'
                        AND
                            idForwarder='".(int)$idForwarder."'
                        AND
                            iWarehouseType='".(int)$iWarehouseType."'
                        AND
                            iActive='1'
                    ";
                    if($result=$this->exeSQL($query))
                    {
                        if($this->iNumRows>0)
                        {
                            $row=$this->getAssoc($result);
                            $fromWareHouse_Id=$row['id'];
                        }
                        else
                        {
                            $fromWareHouse_Id=0;
                        }
                    } 
                    $query="
                        SELECT
                            id
                        FROM
                            ".__DBC_SCHEMATA_WAREHOUSES__."
                        WHERE
                            szWareHouseName='".mysql_escape_custom(sanitize_all_html_input($LCLServiceDatas[5]))."' 	
                        AND
                            szCity='".mysql_escape_custom(sanitize_all_html_input($LCLServiceDatas[4]))."'
                        AND
                            idCountry='".(int)$desCountry_Id."'
                        AND
                            idForwarder='".(int)$idForwarder."'
                        AND
                            iWarehouseType='".(int)$iWarehouseType."'
                        AND
                            iActive='1'        
                    ";
                    //echo $query."<br/>";
                    if($result=$this->exeSQL($query))
                    {
                        if($this->iNumRows>0)
                        { 
                            $row=$this->getAssoc($result);
                            $toWareHouse_Id=$row['id'];
                        }
                        else
                        {
                            $toWareHouse_Id=0;
                        }
                    } 	
                    if($toWareHouse_Id>0 && $fromWareHouse_Id>0)
                    { 
                        $allowedOriginChargesFlag = 0;
                        $allowedDestinationChargesFlag = 0;

                        if($LCLServiceDatas[6]!=="" || $LCLServiceDatas[7]!=="" || $LCLServiceDatas[8]!=="" || $LCLServiceDatas[9]!="")
                        {
                            $allowedOriginChargesFlag=1;
                        }

                        if($LCLServiceDatas[14]!="" || $LCLServiceDatas[15]!="" || $LCLServiceDatas[16]!="" || $LCLServiceDatas[17]!="")
                        {
                            $allowedDestinationChargesFlag=1;
                        }
                        $fromDate='';
                        //$fromDateArr=explode("/",$newLCLServiceDatas[17]);
                        $fromDate=date("Y-m-d",trim(sanitize_all_html_input($LCLServiceDatas[25])));

                        $expiryDate=''; 
					
                        //$expiryDateArr=explode("/",$newLCLServiceDatas[18]);
                        if($LCLServiceDatas[26]!='')
                         $expiryDate=date("Y-m-d",trim(sanitize_all_html_input($LCLServiceDatas[26])));

                        $bookingHours='';
                        $bookingHourArr=explode(' ',strtolower(sanitize_all_html_input($LCLServiceDatas[24])));


                        if($bookingHourArr[1]=='days')
                        {
                            $bookingHours=($bookingHourArr[0]*24);
                        }
                        else if($bookingHourArr[1]=='hours')
                        {
                            $bookingHours=$bookingHourArr[0];
                        } 
                        
                        $iTransitHours='';
                        $iTransitHours=(sanitize_all_html_input($LCLServiceDatas[21])*24);

                        $idFrequency = '';
                        $idFrequency=$Excel_export_import->getFrequencyId(ucfirst(strtolower(sanitize_all_html_input($LCLServiceDatas[18]))));
                        //echo $idFrequency;

                        $iCutOffDay='';
                        $iCutOffDay=$Excel_export_import->getDayId(ucfirst(strtolower(sanitize_all_html_input($LCLServiceDatas[19]))));

                        $idOriginChangeCurrency='';
                        $idOriginChangeCurrency=$Excel_export_import->getFreightCurrencyId(strtoupper(sanitize_all_html_input($LCLServiceDatas[9])));

                        $currencyId='';
                        $currencyId=$Excel_export_import->getFreightCurrencyId(strtoupper(sanitize_all_html_input($LCLServiceDatas[13])));

                        $idDestinationCurrency = '';
                        $idDestinationCurrency = $Excel_export_import->getFreightCurrencyId(strtoupper(sanitize_all_html_input($LCLServiceDatas[17])));

                        if($LCLServiceDatas[23]!='')
                            $des_Time=date('H:i',sanitize_all_html_input($LCLServiceDatas[23]));					  

                         if($LCLServiceDatas[20]!='')
                            $cutoff_Time=date('H:i',sanitize_all_html_input($LCLServiceDatas[20]));

                         $availableDay=$Excel_export_import->getAvailableDay(sanitize_all_html_input($LCLServiceDatas[21]),$iCutOffDay);


					 $query="
						INSERT INTO
							".__DBC_SCHEMATA_WAREHOUSES_PRICING_APPROVAL_DATA__."
						(
                                                    idWarehouseFrom,
                                                    idWarehouseTo,
                                                    iBookingCutOffHours,
                                                    idCutOffDay,
                                                    szCutOffLocalTime,
                                                    idAvailableDay,
                                                    szAvailableLocalTime,											
                                                    iTransitHours,
                                                    iTransitDays,
                                                    idFrequency,											
                                                    fOriginChargeRateWM,
                                                    fOriginChargeMinRateWM,
                                                    fOriginChargeBookingRate,
                                                    szOriginChargeCurrency,										
                                                    fFreightRateWM,
                                                    fFreightMinRateWM,
                                                    fFreightBookingRate,
                                                    szFreightCurrency,											
                                                    fDestinationChargeRateWM,
                                                    fDestinationChargeMinRateWM,
                                                    fDestinationChargeBookingRate,
                                                    szDestinationChargeCurrency,											
                                                    dtValidFrom,
                                                    dtExpiry,
                                                    iActive,
                                                    dtCreatedOn,
                                                    idForwarder,
                                                    idCountryFrom,
                                                    idCountryTo,
                                                    szCityFrom,
                                                    szCityTo,
                                                    iOriginChargesApplicable,
                                                    iDestinationChargesApplicable,
                                                    idBulkUploadService
								
                                                )
                                                VALUES
                                                (
                                                    '".(int)$fromWareHouse_Id."',
                                                    '".(int)$toWareHouse_Id."',
                                                    '".mysql_escape_custom($bookingHours)."',
                                                    '".mysql_escape_custom($iCutOffDay)."',
                                                    '".mysql_escape_custom($cutoff_Time)."',
                                                    '".mysql_escape_custom($availableDay)."',
                                                    '".mysql_escape_custom($des_Time)."',
                                                    '".(int)$transitHours."',
                                                    '".(int)mysql_escape_custom($iTransitHours/24)."',
                                                    '".(int)$idFrequency."',
                                                    '".(float)$LCLServiceDatas[6]."',
                                                    '".(float)$LCLServiceDatas[7]."',
                                                    '".(float)$LCLServiceDatas[8]."',
                                                    '".(int)$idOriginChangeCurrency."',
                                                    '".(float)$LCLServiceDatas[10]."',
                                                    '".(float)$LCLServiceDatas[11]."',
                                                    '".(float)$LCLServiceDatas[12]."',
                                                    '".(int)$currencyId."',
                                                    '".(float)$LCLServiceDatas[14]."',
                                                    '".(float)$LCLServiceDatas[15]."',
                                                    '".(float)$LCLServiceDatas[16]."',
                                                    '".(int)$idDestinationCurrency."',
                                                    '".mysql_escape_custom($fromDate)."',
                                                    '".mysql_escape_custom($expiryDate)."',
                                                    '1',
                                                    NOW(),
                                                    '".(int)$idForwarder."',
                                                    '".(int)$originCountry_Id."',
                                                    '".(int)$desCountry_Id."',
                                                    '".mysql_escape_custom($LCLServiceDatas[1])."',
                                                    '".mysql_escape_custom($LCLServiceDatas[4])."',
                                                    '".(int)$allowedOriginChargesFlag."',
                                                    '".(int)$allowedDestinationChargesFlag."',
                                                    '".(int)$idService."'
                                                )
                                            ";
                                            //echo "<br />".$query."<br />";
                                            $result=$this->exeSQL($query);
				}
			}
			
			$query="
				UPDATE
					".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
				SET
					iStatus='".(int)__DATA_REVIEWED_BY_FORWARDER__."'
				WHERE
					id='".(int)$idService."'		
			";
			$result=$this->exeSQL($query);
			return true;
		}

	}
	
	
	function totalWaitingToBeApprovedPricingWTW($idForwarder,$idService,$idData=0)
	{
		$sql='';
		if((int)$idData>0)
		{
			$sql="
				AND
				wtw.id='".(int)$idData."'
			";
		}
		$query="
			SELECT
				wtw.id,
				wtw.idWarehouseFrom,
				wtw.idWarehouseTo,
				wtw.iBookingCutOffHours,
				wtw.idCutOffDay,
				wtw.szCutOffLocalTime,
				wtw.idAvailableDay,
				wtw.szAvailableLocalTime,											
				wtw.iTransitHours,
				wtw.iTransitDays,
				wtw.idFrequency,											
				wtw.fOriginChargeRateWM,
				wtw.fOriginChargeMinRateWM,
				wtw.fOriginChargeBookingRate,
				wtw.szOriginChargeCurrency,										
				wtw.fFreightRateWM,
				wtw.fFreightMinRateWM,
				wtw.fFreightBookingRate,
				wtw.szFreightCurrency,											
				wtw.fDestinationChargeRateWM,
				wtw.fDestinationChargeMinRateWM,
				wtw.fDestinationChargeBookingRate,
				wtw.szDestinationChargeCurrency,											
				wtw.dtValidFrom,
				wtw.dtExpiry,
				wtw.iActive,
				wtw.dtCreatedOn,
				wtw.idForwarder,
				wtw.idCountryFrom,
				wtw.idCountryTo,
				wtw.szCityFrom,
				wtw.szCityTo,
				wtw.iOriginChargesApplicable,
				wtw.iDestinationChargesApplicable,
				( SELECT szWeekDay FROM ".__DBC_SCHEMATA_WEEK_DAYS__." wd WHERE wd.id = wtw.idAvailableDay ) szAvailableDay,
				( SELECT szWeekDay FROM ".__DBC_SCHEMATA_WEEK_DAYS__." wd WHERE wd.id = wtw.idCutOffDay ) szCutOffDay   
			FROM
				".__DBC_SCHEMATA_WAREHOUSES_PRICING_APPROVAL_DATA__." AS wtw
			WHERE
				wtw.idForwarder='".(int)$idForwarder."'
			AND
				wtw.idBulkUploadService='".(int)$idService."'
			AND
				date(wtw.dtExpiry)>='".mysql_escape_custom(date('Y-m-d'))."'
			".$sql."
			ORDER BY
				wtw.id ASC
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				if((int)$idData==0)
				{
					while($row=$this->getAssoc($result))
					{
                                            $res_arr[]=$row['id'];                        
					}
					
					return $res_arr;
				}
				else
				{
					$row=$this->getAssoc($result);
					$res_arr[]=$row;
					return $res_arr;
				}
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	
	function deletePricingWTWUnApprovedData($id,$idForwarder,$idService)
	{
		$query="
			DElETE FROM
				".__DBC_SCHEMATA_WAREHOUSES_PRICING_APPROVAL_DATA__."
			WHERE
				id='".(int)$id."'
			AND
				idForwarder='".(int)$idForwarder."'
		";
		if($result=$this->exeSQL($query))
		{
			/*$query="
				UPDATE
					".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
				SET
					iActualRecords=iActualRecords-1
				WHERE
					id='".(int)$idService."'
			";
			$result=$this->exeSQL($query);*/
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function addOBOLCLApproveData($data)
	{
            $Excel_export_import= new cExport_Import();
            if(!empty($data))
            {
                $this->set_idOriginWarehouse(sanitize_all_html_input(trim($data['idOriginWarehouse'])));
                $this->set_idDestinationWarehouse(sanitize_all_html_input(trim($data['idDestinationWarehouse'])));

                if(sanitize_all_html_input(trim($data['iOriginChargesApplicable']))==1)
                {
                    $this->set_fOriginRateWM(sanitize_all_html_input(trim($data['fOriginRateWM'])),true);
                    $this->set_fOriginMinRateWM(sanitize_all_html_input(trim($data['fOriginMinRateWM'])),true);
                    $this->set_fOriginRate(sanitize_all_html_input(trim($data['fOriginRate'])),true);
                    $this->set_szOriginFreightCurrency(sanitize_all_html_input(trim($data['szOriginFreightCurrency'])),true);
                }
                else
                {
                    $this->fOriginRateWM = 0;
                    $this->fOriginMinRateWM = 0;
                    $this->fOriginRate = 0;
                    $this->szOriginFreightCurrency = 0;
                }

                $this->set_fRateWM(sanitize_all_html_input(trim($data['fRateWM'])));
                $this->set_fMinRateWM(sanitize_all_html_input(trim($data['fMinRateWM'])));
                $this->set_fRate(sanitize_all_html_input(trim($data['fRate'])));
                $this->set_szFreightCurrency(sanitize_all_html_input(trim($data['szFreightCurrency'])));

                if(sanitize_all_html_input(trim($data['iDestinationChargesApplicable']))==1)
                {
                    $this->set_fDestinationRateWM(sanitize_all_html_input(trim($data['fDestinationRateWM'])),true);
                    $this->set_fDestinationMinRateWM(sanitize_all_html_input(trim($data['fDestinationMinRateWM'])),true);
                    $this->set_fDestinationRate(sanitize_all_html_input(trim($data['fDestinationRate'])),true);
                    $this->set_szDestinationFreightCurrency(sanitize_all_html_input(trim($data['szDestinationFreightCurrency'])),true);
                }
                else
                {
                    $this->fDestinationRateWM = 0;
                    $this->fDestinationMinRateWM = 0;
                    $this->fDestinationRate = 0;
                    $this->szDestinationFreightCurrency = 0;
                }

                $this->set_dtValidFrom(sanitize_all_html_input(trim($data['dtValidFrom'])));
                $this->set_dtExpiry(sanitize_all_html_input(trim($data['dtExpiry'])));
                $this->set_idFrequency(sanitize_all_html_input(trim($data['idFrequency'])));
                $this->set_iBookingCutOffHours(sanitize_all_html_input(trim($data['iBookingCutOffHours'])));
                $this->set_iTransitHours(sanitize_all_html_input(trim($data['iTransitHours'])));
                $this->set_idCutOffDay(sanitize_all_html_input(trim($data['idCutOffDay'])));
                $this->set_szCutOffLocalTime(sanitize_all_html_input(trim($data['szCutOffLocalTime'])));
                $this->set_idAvailableDay(sanitize_all_html_input(trim($data['idAvailableDay'])));
                $this->set_szAvailableLocalTime(sanitize_all_html_input(trim($data['szAvailableLocalTime'])));

                if($this->error==true)
                {
                    return false;
                }

                if($this->idOriginWarehouse<=0)
                {
                    $this->addError('idOriginWarehouse',"Origin warehouse ID is required.");
                    return false;
                }
                if($this->idDestinationWarehouse<=0)
                {
                    $this->addError('idDestinationWarehouse',"Destination warehouse ID is required.");
                    return false;
                }
                if(!$Excel_export_import->isWarehouseBelongsToForwarder($this->idOriginWarehouse,$this->idDestinationWarehouse))
                {
                    return false;
                }

                /*
                if($this->isPricingAlreadyExists($this->idOriginWarehouse,$this->idDestinationWarehouse,$this->idCutOffDay))
                {
                    $this->addError('pricing',t($this->t_base.'messages/pricing_already_exists'));
                    return false;
                }
                */
                if(!empty($this->dtValidFrom))
                {
                    $dateAry = explode("/",$this->dtValidFrom); 
                    $dtTiming_millisecond = strtotime($dateAry[2]."-".$dateAry[1]."-".$dateAry[0]);  // converting date 
                    $dtDate = date("d/m/Y",$dtTiming_millisecond);
                    $vaildity_timestam = $dtTiming_millisecond ;	 
                    if(!checkdate($dateAry[1],$dateAry[0],$dateAry[2]))
                    {
                       $this->addError("dtValidFrom",t($this->t_base.'fields/validity')." ".t($this->t_base.'fields/from')." ".t('Error/valid_date_landing_page'));
                       return false;
                    }
                    else if(($vaildity_timestam < strtotime(date('Y-m-d'))))
                    {
                       $this->addError("dtValidFrom",t($this->t_base.'fields/validity')." ".t($this->t_base.'fields/from')." ".t('Error/must_be_greater_than_today'));
                       return false;
                    }
                    else
                    {
                       $this->dtValidFrom = $dateAry[2]."-".$dateAry[1]."-".$dateAry[0]." 00:00:00";  // YYYY/MM/DD H:I:S
                    }
                }	
                if(!empty($this->dtExpiry))
                {
                    $dateAry = explode("/",$this->dtExpiry); 
                    $dtTiming_millisecond = strtotime($dateAry[2]."-".$dateAry[1]."-".$dateAry[0]);  // converting date 
                    $dtDate = date("d/m/Y",$dtTiming_millisecond);

                    if(!checkdate($dateAry[1],$dateAry[0],$dateAry[2]))
                    {
                       $this->addError("dtExpiry",t($this->t_base.'fields/validity')." ".t($this->t_base.'fields/to')." ".t('Error/valid_date_landing_page'));
                       return false;
                    }
                    else if(($dtTiming_millisecond <= strtotime(date('Y-m-d'))))
                    {
	             	$this->addError("dtExpiry",t($this->t_base.'fields/validity')." ".t($this->t_base.'fields/to')." ".t('Error/must_be_greater_than_today'));
	             	return false;
                    }
                    else if($vaildity_timestam > $dtTiming_millisecond)
                    {
	             	$this->addError("dtExpiry",t($this->t_base.'messages/validity_from_date_should_be_less_than_expiry_date'));
	             	return false;
                    }
                    else
                    {
	             	$this->dtExpiry = $dateAry[2]."-".$dateAry[1]."-".$dateAry[0]." 00:00:00";  // YYYY/MM/DD H:I:S
                    }
                }
                $iAvailableDay = $Excel_export_import->getAvailableDay($this->iTransitHours,$this->idCutOffDay);
                if($iAvailableDay!=$this->idAvailableDay)
                {
                    $this->addError('szDestinationWarehouse',t($this->t_base.'messages/invalid_available_day'));
                    return false;
                }
			
                if($this->idOriginWarehouse == $this->idDestinationWarehouse)
                {
                    $this->addError('szDestinationWarehouse',t($this->t_base.'messages/same_whs_error_message'));
                    return false;
                }
			
                $kWhs_origin=new cWHSSearch();
                $kWhs_destination = new cWHSSearch();

                $kWhs_origin->load($this->idOriginWarehouse);
                $kWhs_destination->load($this->idDestinationWarehouse);

                $cutoffTimeAry = explode(':',$this->szCutOffLocalTime);
                $cutoffTime = $cutoffTimeAry[0]+($cutoffTimeAry[1]/60) ;

                $availabelTimeAry = explode(':',$this->szAvailableLocalTime);
                $availabelTime = $availabelTimeAry[0]+($availabelTimeAry[1]/60) ; 

                $transitHours = (int)(($this->iTransitHours * 24) + ($availabelTime - $cutoffTime ) +(($kWhs_origin->szUTCOffset - $kWhs_destination->szUTCOffset)/(1000*60*60)));
                $success = false;


                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
                    SET
                        iActive='0'
                    WHERE
                        idWarehouseFrom='".(int)$this->idOriginWarehouse."'
                    AND
                        idWarehouseTo='".(int)$this->idDestinationWarehouse."'
                ";
                $result=$this->exeSQL($query);
			
                $query="
                    SELECT
			wtw.id,
			wtw.idWarehouseFrom,
			wtw.idWarehouseTo,
			wtw.iBookingCutOffHours,
			wtw.idCutOffDay,
			wtw.szCutOffLocalTime,
			wtw.idAvailableDay,
			wtw.szAvailableLocalTime,
			wtw.iTransitHours,
			wtw.idFrequency,
			wtw.fOriginChargeRateWM,
			wtw.fOriginChargeMinRateWM,
			wtw.fOriginChargeBookingRate,
			wtw.szOriginChargeCurrency,
			wtw.fFreightRateWM as fRateWM,
			wtw.fFreightMinRateWM as fMinRateWM,
			wtw.fFreightBookingRate as fRate,
			wtw.fDestinationChargeRateWM,
			wtw.fDestinationChargeMinRateWM,
			wtw.fDestinationChargeBookingRate,
			wtw.szDestinationChargeCurrency,
			wtw.szFreightCurrency,
			wtw.dtValidFrom,
			wtw.dtExpiry,
			wtw.iTransitDays
		FROM
			".__DBC_SCHEMATA_WAREHOUSES_PRICING__." wtw
		WHERE
			idWarehouseFrom='".(int)$this->idOriginWarehouse."'
		AND
			idWarehouseTo='".(int)$this->idDestinationWarehouse."'
		AND
			idCutOffDay='".mysql_escape_custom($this->idCutOffDay)."'
		";
	//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$row=$this->getAssoc($result);
				if((int)$_SESSION['forwarder_admin_id']>0)
				{
					$idAdmin = (int)$_SESSION['forwarder_admin_id'];
					$idForwarderContact = 0 ;
				}
				else
				{
					$idAdmin = 0;
					$idForwarderContact = (int)$_SESSION['forwarder_user_id'];
				}
				
				//$kForwarder=new cForwarder();
				$idForwarder = $_SESSION['forwarder_id'];
				//$kForwarder->load($idForwarder);
										
				$query="
					INSERT INTO
						".__DBC_SCHEMATA_FORWARDER_WTW_ARCHIVE__."
					(
							idWarehouseFrom,
							idWarehouseTo,
							iBookingCutOffHours,
							idCutOffDay,
							szCutOffLocalTime,
							idAvailableDay,
							szAvailableLocalTime,
							iTransitHours,
							idFrequency,												
							fOriginChargeRateWM,
							fOriginChargeMinRateWM,
							fOriginChargeBookingRate,
							szOriginChargeCurrency,												
							fFreightRateWM,
							fFreightMinRateWM,
							fFreightBookingRate,
							szFreightCurrency,												
							fDestinationChargeRateWM,
							fDestinationChargeMinRateWM,
							fDestinationChargeBookingRate,
							szDestinationChargeCurrency,																							
							dtValidFrom,
							dtExpiry,
							idForwarder,
							idForwarderContact,
							idAdmin,
							dtUploaded
						)
						VALUES
						(
							'".(int)$this->idOriginWarehouse."',
							'".(int)$this->idDestinationWarehouse."',
							'".mysql_escape_custom($row['iBookingCutOffHours'])."',
							'".mysql_escape_custom($row['idCutOffDay'])."',
							'".mysql_escape_custom($row['szCutOffLocalTime'])."',
							'".mysql_escape_custom($row['idAvailableDay'])."',
							'".mysql_escape_custom($row['szAvailableLocalTime'])."',
							'".mysql_escape_custom($row['iTransitHours'])."',
							'".mysql_escape_custom($row['idFrequency'])."',												
							'".(float)$row['fOriginChargeRateWM']."',
							'".(float)$row['fOriginChargeMinRateWM']."',
							'".(float)$row['fOriginChargeBookingRate']."',
							'".(int)$row['szOriginChargeCurrency']."',												
							'".(float)$row['fRateWM']."',
							'".(float)$row['fMinRateWM']."',
							'".(float)$row['fRate']."',
							'".(int)$row['szFreightCurrency']."',												
							'".(float)$row['fDestinationChargeRateWM']."',
							'".(float)$row['fDestinationChargeMinRateWM']."',
							'".(float)$row['fDestinationChargeBookingRate']."',
							'".(int)$row['szDestinationChargeCurrency']."',												
							'".mysql_escape_custom($row['dtValidFrom'])."',
							'".mysql_escape_custom($row['dtExpiry'])."',
							'".(int)$idForwarder."',
							'".(int)$idForwarderContact."',
							'".(int)$idAdmin."',
							NOW()						
						)
					";
					//echo "<br /> ".$query."<br />";
					$result=$this->exeSQL($query);
					
							
				$query="
					UPDATE
						".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
					SET
						fFreightRateWM = '".(float)$this->fRateWM."',
						fFreightMinRateWM = '".(float)$this->fMinRateWM."',
						fFreightBookingRate = '".(float)$this->fRate."',
						szFreightCurrency = '".mysql_escape_custom(trim($this->szFreightCurrency))."',						
						fOriginChargeRateWM = '".(float)$this->fOriginRateWM."',
						fOriginChargeMinRateWM = '".(float)$this->fOriginMinRateWM."',
						fOriginChargeBookingRate = '".(float)$this->fOriginRate."',
						szOriginChargeCurrency = '".mysql_escape_custom(trim($this->szOriginFreightCurrency))."',						
						fDestinationChargeRateWM = '".(float)$this->fDestinationRateWM."',
						fDestinationChargeMinRateWM = '".(float)$this->fDestinationMinRateWM."',
						fDestinationChargeBookingRate = '".(float)$this->fDestinationRate."',
						szDestinationChargeCurrency = '".mysql_escape_custom(trim($this->szDestinationFreightCurrency))."',						
						dtValidFrom = '".mysql_escape_custom(trim($this->dtValidFrom))."',				
						dtExpiry = '".mysql_escape_custom(trim($this->dtExpiry))."',
						idFrequency = '".mysql_escape_custom(trim($this->idFrequency))."',
						iBookingCutOffHours = '".mysql_escape_custom(trim($this->iBookingCutOffHours))."',
						iTransitHours = '".(int)mysql_escape_custom(trim($transitHours))."',				
						idCutOffDay = '".mysql_escape_custom(trim($this->idCutOffDay))."',
						szCutOffLocalTime = '".mysql_escape_custom(trim($this->szCutOffLocalTime))."',
						idAvailableDay = '".mysql_escape_custom(trim($this->idAvailableDay))."',
						szAvailableLocalTime = '".mysql_escape_custom(trim($this->szAvailableLocalTime))."',
						dtUpdatedOn = now(),
						iTransitDays = '".(int)$this->iTransitHours."',
						iDestinationChargesApplicable = '".(int)$data['iDestinationChargesApplicable']."',
						iOriginChargesApplicable = '".(int)$data['iOriginChargesApplicable']."',
						iActive='1'
					WHERE
						idWarehouseFrom = '".(int)$this->idOriginWarehouse."'	
					AND
						idWarehouseTo = '".(int)$this->idDestinationWarehouse."'
					AND
						idCutOffDay='".mysql_escape_custom($this->idCutOffDay)."'	
				";
				
				$reusult=$this->exeSQL($query);
				$success = true;
			}
			else
			{

				$query="
					INSERT INTO
						".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
					(
						idWarehouseFrom,
						idWarehouseTo,					
						fOriginChargeRateWM,
						fOriginChargeMinRateWM,
						fOriginChargeBookingRate,
						szOriginChargeCurrency,								
						fFreightRateWM,
						fFreightMinRateWM,
						fFreightBookingRate,
						szFreightCurrency,								
						fDestinationChargeRateWM,
						fDestinationChargeMinRateWM,
						fDestinationChargeBookingRate,
						szDestinationChargeCurrency,
						dtValidFrom,
						dtExpiry,
						idFrequency,
						iBookingCutOffHours,
						iTransitHours,
						iTransitDays,
						idCutOffDay,
						szCutOffLocalTime,
						idAvailableDay,
						szAvailableLocalTime,
						dtCreatedOn,
						iActive,
						iOriginChargesApplicable,
						iDestinationChargesApplicable
					)
					VALUES
					(
						 '".(int)$this->idOriginWarehouse."',
						 '".(int)$this->idDestinationWarehouse."',	
						  '".(float)$this->fOriginRateWM."',
						 '".(float)$this->fOriginMinRateWM."',
						 '".(float)$this->fOriginRate."',
						 '".mysql_escape_custom(trim($this->szOriginFreightCurrency))."',					 
						 '".(float)$this->fRateWM."',
						 '".(float)$this->fMinRateWM."',
						 '".(float)$this->fRate."',
						 '".mysql_escape_custom(trim($this->szFreightCurrency))."',					 
						 '".(float)$this->fDestinationRateWM."',
						 '".(float)$this->fDestinationMinRateWM."',
						 '".(float)$this->fDestinationRate."',
						 '".mysql_escape_custom(trim($this->szDestinationFreightCurrency))."',					 
						 '".mysql_escape_custom(trim($this->dtValidFrom))."',				
						 '".mysql_escape_custom(trim($this->dtExpiry))."',
						 '".mysql_escape_custom(trim($this->idFrequency))."',
						 '".mysql_escape_custom(trim($this->iBookingCutOffHours))."',
						 '".(int)mysql_escape_custom(trim($transitHours))."',	
						 '".(int)mysql_escape_custom(trim($this->iTransitHours))."',	
						 '".mysql_escape_custom(trim($this->idCutOffDay))."',
						 '".mysql_escape_custom(trim($this->szCutOffLocalTime))."',
						 '".mysql_escape_custom(trim($this->idAvailableDay))."',
						 '".mysql_escape_custom(trim($this->szAvailableLocalTime))."',
						 now(),
						 '1',
						 '".(int)$data['iOriginChargesApplicable']."',
						 '".(int)$data['iDestinationChargesApplicable']."'
				     ) 
				";
				//echo "<br>".$query."<br>";
				//die;
					if($reusult=$this->exeSQL($query))
					{
						$this->id = $this->iLastInsertID ;
						$success = true;
						$kForwarder=new cForwarder();
						$idForwarder = $_SESSION['forwarder_id'];
						$kForwarder->load($idForwarder);
						
						if(($kForwarder->iAgreeTNC==1) && ($kForwarder->iUpdateRss==2))
						{
							$kConfig = new cConfig();
							$kWhsSearch = new cWHSSearch();
							
							$kWhsSearch->load($this->idOriginWarehouse);
							$szOriginCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
							
							$kWhsSearch->load($this->idDestinationWarehouse);
							$szDestinationCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
							
							$replace_ary = array();
							$replace_ary['szForwarderDisplayName'] = $kForwarder->szDisplayName ;
							$replace_ary['szOriginCountry'] = $szOriginCountry ;
							$replace_ary['szDestinationCountry'] = $szDestinationCountry ;
							$kExport_Import = new cExport_Import();
							$content_ary = array();
							$content_ary = $kExport_Import->replaceRssContent('__OBO_LCL_SERVICES__',$replace_ary);
							
							$rssDataAry=array();
							$rssDataAry['idForwarder'] = $kForwarder->id ;
							$rssDataAry['szFeedType'] = '__OBO_LCL_SERVICES__';
							$rssDataAry['szLink'] = __RSS_FEED_PAGE_URL__ ;
							$rssDataAry['szTitle'] = $content_ary['szTitle'];
							$rssDataAry['szDescription'] = $content_ary['szDescription'];
							$rssDataAry['szDescriptionHTML'] = $content_ary['szDescriptionHTML'];
							
							$kRss = new cRSS();
							$kRss->addRssFeed($rssDataAry);
					}
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
					
			}
			}
			return $success;
		}
		else
		{
			return false;
		}
	}
	
	
	function addDataForApprovalPricingHaulageTemp($objPHPExcel,$sheetindex,$colcount,$rowcount,$idForwarder,$id)
	{
		$Excel_export_import= new cExport_Import();
		
		date_default_timezone_set('UTC');
		$ctr=0;
		if($rowcount>2)
		{
			for($i=3;$i<=$rowcount;$i++)
			{
				for($j=0;$j<$colcount;$j++)
				{
					if($j<=14)
					{
						if($objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0,$i)->getValue()!="")
						{
							$HaulageServiceData[$ctr][] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($j,$i)->getValue();
							
						}
					}
					
				}
				++$ctr;
			}
		}
		$haulageNewArr=array();	
		//print_r($HaulageServiceData);
		//die;
		if(!empty($HaulageServiceData))
		{
			
			foreach($HaulageServiceData as $HaulageServiceDatas)
			{
				$WareHouse_Id=0;
				$idPricingModel=0;
				$haulage_Country=sanitize_all_html_input($Excel_export_import->getCountryId($HaulageServiceDatas[0]));
				$haulage_City=sanitize_all_html_input($HaulageServiceDatas[1]);
				$haulage_CFS=sanitize_all_html_input($HaulageServiceDatas[2]);
				
				$idPricingModel=$Excel_export_import->getPricingModelId(sanitize_all_html_input($HaulageServiceDatas[4]));
				$haulageCurrency=$Excel_export_import->getFreightCurrencyId(sanitize_all_html_input($HaulageServiceDatas[10]));
				
				$query="
					SELECT
						id
					FROM
						".__DBC_SCHEMATA_WAREHOUSES__."
					WHERE
						szWareHouseName='".mysql_escape_custom($haulage_CFS)."' 	
					AND
						szCity='".mysql_escape_custom($haulage_City)."'
					AND
						idCountry='".(int)$haulage_Country."'
					AND
						idForwarder='".(int)$idForwarder."'
                                        AND
                                            iActive='1'            
				";
				//echo $query."<br/>";
				if($result=$this->exeSQL($query))
				{
					if($this->iNumRows>0)
					{
						$row=$this->getAssoc($result);
						$WareHouse_Id=$row['id'];
					}
					else
					{
						$WareHouse_Id=0;
					}
				}
				
				if((int)$WareHouse_Id>0 && $idPricingModel>0)
				{
					$haulage_Direction='';
					if($HaulageServiceDatas[3]=='Export')
					{
						$haulage_Direction='1';
					}
					else if($HaulageServiceDatas[3]=='Import')
					{
						$haulage_Direction='2';
					}
										
					$haulagePricingModelName=sanitize_all_html_input($HaulageServiceDatas[5]);
					
					if($idPricingModel=='4')
					{
						$replaceArr=array("Up to","km",",");
						//echo $haulagePricingModelName;
						$haulagePricingModelName=trim(str_replace($replaceArr,"",$haulagePricingModelName));
						//echo $haulagePricingModelName."<br/>";
					}
					$idHaulaePricing=$Excel_export_import->getHaulagePricingModelId($haulagePricingModelName,$idPricingModel,$WareHouse_Id,$haulage_Direction);
					//echo $idHaulaePricing."<br/>";
					//$haulageNewArr=array();
					if($idHaulaePricing>0)
					{
						if((int)$HaulageServiceDatas[6]>0)
						{
							if(!in_array($idHaulaePricing,$haulageNewArr))
							{
								$haulageNewArr[]=$idHaulaePricing;
								//print_r($haulageNewArr);
								//echo "<br/><br/>";
								$query="
									INSERT INTO
										".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_MODEl_APPROVAL_DATA__."
									(
										idForwarder,
										idWarehouse,
										idHaulgePricingModel,
										iDirection,
										idHaulageModel,
										iActive,
										dtCreatedOn,
										idCountry,
										szCity,
										idBulkUploadService
										
									)
										VALUES
									(
										'".(int)$idForwarder."',
										'".(int)$WareHouse_Id."',
										'".(int)$idHaulaePricing."',
										'".(int)$haulage_Direction."',
										'".(int)$idPricingModel."',
										'1',
										NOW(),
										'".(int)$haulage_Country."',
										'".mysql_escape_custom($haulage_City)."',
										'".(int)$id."'
									)
								";
								//echo $query."<br/><br/>";
								$result=$this->exeSQL($query);
							}
						
							$query="
								INSERT INTO
									".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_APPROVAL_DATA__."
								(
									idHaulagePricingModel,
									iUpToKg,
									fPricePer100Kg,
									fMinimumPrice,
									fPricePerBooking,
									idCurrency,
									iActive,
									dtCreatedOn,
									idBulkUploadService 
								)
									VALUES
								(
									'".(int)$idHaulaePricing."',
									'".(int)$HaulageServiceDatas[6]."',
									'".(float)$HaulageServiceDatas[7]."',
									'".(float)$HaulageServiceDatas[8]."',
									'".(float)$HaulageServiceDatas[9]."',
									'".(int)$haulageCurrency."',
									'1',
									NOW(),
									'".(int)$id."'
								)
							";
							//echo $query."<br/><br/>";
							$result=$this->exeSQL($query);
						}
					}
				}
			}
			
			$query="
				UPDATE
					".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
				SET
					iStatus='".(int)__DATA_REVIEWED_BY_FORWARDER__."'
				WHERE
					id='".(int)$id."'		
			";
			$result=$this->exeSQL($query);
			return true;
		}
	}
	
	function totalWaitingToBeApprovedPricingHaulage($idForwarder,$idService,$idData=0)
	{
	
		$sql='';
		if((int)$idData>0)
		{
			$sql="
				AND
					idHaulgePricingModel='".(int)$idData."'
			";
		}
		$query="
			SELECT
				idHaulgePricingModel
			FROM
				".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_MODEl_APPROVAL_DATA__."
			WHERE
				idForwarder='".(int)$idForwarder."'
			AND
				idBulkUploadService='".(int)$idService."'
			".$sql."
			ORDER BY
				idHaulgePricingModel ASC
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				if((int)$idData>0)
				{
					$row=$this->getAssoc($result);
					$idHaulagePricingModel=$row['idHaulgePricingModel'];
					$query="
						SELECT
							iUpToKg,
							fPricePer100Kg,
							fMinimumPrice,
							fPricePerBooking,
							idCurrency
						FROM
							".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_APPROVAL_DATA__."
						WHERE
							idHaulagePricingModel='".(int)$idHaulagePricingModel."'
						AND
							idBulkUploadService='".(int)$idService."'
					";
					//echo $query;
					if($result=$this->exeSQL($query))
					{
						if($this->iNumRows>0)
						{
							while($row=$this->getAssoc($result))
							{
								$haulagePricingModelArr[$idHaulagePricingModel][]=$row;
							}
							return $haulagePricingModelArr;
						}
					}
				}
				else
				{
					while($row=$this->getAssoc($result))
					{
						$haulagePricingModelArr[]=$row;
					}
					return $haulagePricingModelArr;
				}
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	
	function selectTempHaulageModelData($idHaulagePricing)
	{
		$query="
			SELECT
				idWarehouse,
				iDirection,
				idHaulageModel,
				idCountry,
				szCity
			FROM
				".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_MODEl_APPROVAL_DATA__."
			WHERE
				idHaulgePricingModel='".(int)$idHaulagePricing."'
		";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				while($row=$this->getAssoc($result))
				{
					$res_arr[]=$row;
				}
				return $res_arr;
			}
		}
		else
		{
			return array();
		}
	}

	function getPricingModelDetail($dataId)
	{
		$query="
			SELECT
				models.szName,
				models.idCountry,
				models.iRadiousKM,
				models.iDistanceUpToKm,
				models.idHaulageTransitTime,
				models.fWmFactor,
				models.fFuelPercentage,
				tt.iHours as iHours
			FROM
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." AS models
			INNER JOIN
					".__DBC_SCHEMATA_HAULAGE_TRANSIT_TIME__." tt
				ON
					tt.id = models.idHaulageTransitTime
			WHERE
				models.id='".(int)$dataId."'
		";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				while($row=$this->getAssoc($result))
				{
					$res_arr[]=$row;
				}
				return $res_arr;
			}
		}
		else
		{
			return array();
		}
	}
	
	
	function getHaulagePricingZoneDetailDataTemp($id,$idService)
	{
		$query="
			SELECT
				hp.id,
				hp.iUpToKg,
				hp.fPricePer100Kg,
				hp.fMinimumPrice,
				hp.fPricePerBooking,
				c.szCurrency,
				hpm.fWmFactor,
				hpm.fFuelPercentage,
				hp.idHaulagePricingModel,
				hp.idCurrency
			FROM
				".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_APPROVAL_DATA__." AS hp
			INNER JOIN
				".__DBC_SCHEMATA_CURRENCY__." AS c
			ON
				hp.idCurrency=c.id
			INNER JOIN
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." AS hpm
			ON
				hp.idHaulagePricingModel=hpm.id
			WHERE
				hp.idHaulagePricingModel='".(int)$id."'
			AND
				hp.idBulkUploadService='".(int)$idService."'
			AND
				hp.iActive='1'
			ORDER BY
				hp.iUpToKg ASC
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				while($row=$this->getAssoc($result))
				{
					$zone_detail_arr[]=$row;
				}
				return $zone_detail_arr;
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function deleteHaulageZonePricingDataTemp($id)
	{
		if((int)$id>0)
		{
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_APPROVAL_DATA__."
				WHERE
					id='".(int)$id."'				
			";
			//echo $query;
			$result=$this->exeSQL($query);
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function haulageLoadPricing($id)
	{
		if((int)$id>0)
		{
			$query="
				SELECT
					id,
					iUpToKg,
					fPricePer100Kg,
					fMinimumPrice,
					fPricePerBooking,
					idCurrency
				FROM
					".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_APPROVAL_DATA__."
				WHERE
					id='".(int)$id."'
			";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					while($row=$this->getAssoc($result))
					{
						$zone_detail_arr[]=$row;
					}
					return $zone_detail_arr;
				}
				else
				{
					return array();
				}
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}		
	}
	
	
	function updateTempPricingLine($data)
	{
		$this->set_id(trim(sanitize_all_html_input($data['idEdit'])));
		$this->set_fPricePer100Kg(trim(sanitize_all_html_input($data['fPricePer100Kg'])));
		$this->set_iUpToKg(trim(sanitize_all_html_input($data['iUpToKg'])));
		$this->set_fMinimumPrice(trim(sanitize_all_html_input(urlencode($data['fMinimumPrice']))));
		$this->set_fPricePerBooking(trim(sanitize_all_html_input($data['fPricePerBooking'])));
		$this->set_idCurrency(trim(sanitize_all_html_input($data['idCurrency'])));
		
		
	   if(!empty($this->iUpToKg) && $this->isUpToKgWeightExist($this->id,$this->iUpToKg,$data['idHaulagePricingModel']))
	   {
			$this->addError( "iUpToKg" , t($this->t_base_approve.'messages/weight_already_exist'));
	   }
		
		//exit if error exist.
		if ($this->error === true)
		{
			return false;
		}
		
		$query="
			UPDATE
				".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_APPROVAL_DATA__."
			SET
				iUpToKg='".(int)$this->iUpToKg."',
				fPricePer100Kg='".(float)$this->fPricePer100Kg."',
				fMinimumPrice='".(float)$this->fMinimumPrice."',
				fPricePerBooking='".(float)$this->fPricePerBooking."',
				idCurrency='".(int)$this->idCurrency."'
			WHERE
				id='".(int)$this->id."'
		";
		if($result=$this->exeSQL($query))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
	function addTempPricingLine($data)
	{
		$this->set_fPricePer100Kg(trim(sanitize_all_html_input($data['fPricePer100Kg'])));
		$this->set_iUpToKg(trim(sanitize_all_html_input($data['iUpToKg'])));
		$this->set_fMinimumPrice(trim(sanitize_all_html_input(urlencode($data['fMinimumPrice']))));
		$this->set_fPricePerBooking(trim(sanitize_all_html_input($data['fPricePerBooking'])));
		$this->set_idCurrency(trim(sanitize_all_html_input($data['idCurrency'])));
		
		
	   if(!empty($this->iUpToKg) && $this->isUpToKgWeightExist(0,$this->iUpToKg,$data['idHaulagePricingModel']))
	   {
			$this->addError( "iUpToKg" , t($this->t_base_approve.'messages/weight_already_exist'));
	   }
		
		//exit if error exist.
		if ($this->error === true)
		{
			return false;
		}
		
		$query="
			INSERT INTO
				".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_APPROVAL_DATA__."
			(	
				idHaulagePricingModel,
				iUpToKg,
				fPricePer100Kg,
				fMinimumPrice,
				fPricePerBooking,
				idCurrency,
				dtCreatedOn,
				idBulkUploadService
			)
				VALUES
			(
				'".(int)$data['idHaulagePricingModel']."',
				'".(int)$this->iUpToKg."',
				'".(float)$this->fPricePer100Kg."',
				'".(float)$this->fMinimumPrice."',
				'".(float)$this->fPricePerBooking."',
				'".(int)$this->idCurrency."',
				NOW(),
				'".(int)$data['idServiceUpload']."'
			)
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
	function isUpToKgWeightExist($id=0,$iUpToKg,$idHaulagePricingModel)
	{
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_APPROVAL_DATA__."
			WHERE
				iUpToKg = '".mysql_escape_custom($iUpToKg)."'
			AND
				idHaulagePricingModel='".(int)$idHaulagePricingModel."'
			AND
				iActive ='1'
		";
		if((int)$id>0)
		{
			$query .="
				AND
				    id<>'".(int)$id."'		
			";
		}
		//echo "<br>".$query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
            if( $this->getRowCnt() > 0 )
            {
                return true;
            }
            else
            {
             	return false;
            }
		}
	}
	
	function deleteHaulageUnApprovedData($idForwarder,$idService,$idWarehouse,$iDirection,$idHaulageModel)
	{
		$j=0;
		$query="
			SELECT
				idHaulgePricingModel
			FROM
				".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_MODEl_APPROVAL_DATA__."
			WHERE
				idForwarder='".(int)$idForwarder."'
			AND
				idBulkUploadService='".(int)$idService."'
			AND
				idWarehouse='".(int)$idWarehouse."'
			AND
				iDirection='".(int)$iDirection."'
			AND
				idHaulageModel='".(int)$idHaulageModel."'
		";
		//echo $query."<br/><br/>";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{				
				while($row=$this->getAssoc($result))
				{
					$idHaulagepricingModel=$row['idHaulgePricingModel'];
					
					$query="
						DELETE FROM
							".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_APPROVAL_DATA__."
						WHERE
							idHaulagePricingModel='".(int)$idHaulagepricingModel."'				
					";
					//echo $query;
					$result1=$this->exeSQL($query);
					++$j;
				}
			}
		}
		$query="
			DELETE FROM
				".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_MODEl_APPROVAL_DATA__."
			WHERE
				idForwarder='".(int)$idForwarder."'
			AND
				idBulkUploadService='".(int)$idService."'
			AND
				idWarehouse='".(int)$idWarehouse."'
			AND
				iDirection='".(int)$iDirection."'
			AND
				idHaulageModel='".(int)$idHaulageModel."'				
		";
		//echo $query;
		$result=$this->exeSQL($query);
		
		/*$query="
			UPDATE
				".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
			SET
				iActualRecords=iActualRecords-$j
			WHERE
				id='".(int)$idService."'
		";
		$result=$this->exeSQL($query);*/
		
		return true;
	}
	
	
	function addOBOHaulageApproveData($idForwarder,$idService,$idWarehouse,$iDirection,$idHaulageModel)
	{
		$res_arr=array();
		$query="
			SELECT
				hpad.iUpToKg,
				hpad.fPricePer100Kg,
				hpad.fMinimumPrice,
				hpad.fPricePerBooking,
				hpad.idCurrency,
				hpad.idHaulagePricingModel
			FROM
				".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_APPROVAL_DATA__." AS hpad
			INNER JOIN
				".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_MODEl_APPROVAL_DATA__." AS hpmad
			ON
				hpmad.idHaulgePricingModel=hpad.idHaulagePricingModel
			WHERE
				hpmad.idForwarder='".(int)$idForwarder."'
			AND
				hpmad.idBulkUploadService='".(int)$idService."'
			AND
				hpmad.idWarehouse='".(int)$idWarehouse."'
			AND
				hpmad.iDirection='".(int)$iDirection."'
			AND
				hpmad.idHaulageModel='".(int)$idHaulageModel."'
			AND
				hpad.iUpToKg>0
		";
		//echo $query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
            if( $this->getRowCnt() > 0 )
            {
            	while($row=$this->getAssoc($result))
            	{
            		$res_arr[]=$row;
            	}
            	//print_r($res_arr);
            	if(!empty($res_arr))
            	{
            		$haulageModelArr=array();
            		foreach($res_arr as $res_arrs)
            		{
            		
            			if(!in_array($res_arrs['idHaulagePricingModel'],$haulageModelArr))
            			{
            				$haulageModelArr[]=$res_arrs['idHaulagePricingModel'];
	            			$query="
								UPDATE
									".__DBC_SCHEMATA_HAULAGE_PRICING__."
								SET
									iActive='0'
								WHERE
									idHaulagePricingModel='".(int)$res_arrs['idHaulagePricingModel']."'
							";	
	            			//echo $query;
	            			$result = $this->exeSQL( $query );
            			}
            			
            			$query="
							SELECT
								id
							FROM
								".__DBC_SCHEMATA_HAULAGE_PRICING__."
							WHERE
								iUpToKg = '".(int)$res_arrs['iUpToKg']."'
							AND
								idHaulagePricingModel='".(int)$res_arrs['idHaulagePricingModel']."'
						";
            			
            			
						//echo "<br>".$query;
						if( ( $result = $this->exeSQL( $query ) ) )
						{
				            if( $this->getRowCnt() > 0 )
				            {
				            	$query="
									UPDATE
										".__DBC_SCHEMATA_HAULAGE_PRICING__."
									SET
										fPricePer100Kg='".(float)$res_arrs['fPricePer100Kg']."',
										fMinimumPrice='".(float)$res_arrs['fMinimumPrice']."',
										fPricePerBooking='".(float)$res_arrs['fPricePerBooking']."',
										idCurrency='".(int)$res_arrs['idCurrency']."',
										iActive='1'
									WHERE
										idHaulagePricingModel='".(int)$res_arrs['idHaulagePricingModel']."'
									AND
										iUpToKg = '".(int)$res_arrs['iUpToKg']."'
									";
				            	//echo $query."<br/>";
				            	$result = $this->exeSQL( $query );
				            }
				            else
				            {
				            	$query="
									INSERT INTO
										".__DBC_SCHEMATA_HAULAGE_PRICING__."
									(	
										idHaulagePricingModel,
										iUpToKg,
										fPricePer100Kg,
										fMinimumPrice,
										fPricePerBooking,
										idCurrency,
										dtCreatedOn
									)
										VALUES
									(
										'".(int)$res_arrs['idHaulagePricingModel']."',
										'".(int)$res_arrs['iUpToKg']."',
										'".(float)$res_arrs['fPricePer100Kg']."',
										'".(float)$res_arrs['fMinimumPrice']."',
										'".(float)$res_arrs['fPricePerBooking']."',
										'".(int)$res_arrs['idCurrency']."',
										NOW()
									)
								";
				            	//echo $query."<br/>";
				            	$result = $this->exeSQL( $query );
				            }
						}
            		}
            	}
            	return true;
            }
            else
            {
            	return true;
            }
		}
		else
		{
			return true;
		}
	}
	
	function addDataForApprovalCFSLocationTemp($objPHPExcel,$sheetindex,$colcount,$rowcount,$idForwarder,$idService,$iWarehouseType=__WAREHOUSE_TYPE_CFS__)
	{
		date_default_timezone_set('UTC');
		$ctr=0;
		if($rowcount>1)
		{
                    for($i=2;$i<=$rowcount;$i++)
                    {
                        for($j=0;$j<$colcount;$j++)
                        {
                            if($j<=20)
                            {
                                if($objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0,$i)->getValue()!="")
                                {
                                    $CFSLocationData[$ctr][] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($j,$i)->getValue(); 
                                }
                            } 
                        }
                        ++$ctr;
                    }
		}
		
		if((int)$_SESSION['forwarder_admin_id'])
		{
                    $idForwarderContact = 0;
                    $idAdminUpdatedBy = $_SESSION['forwarder_admin_id'] ;
		}
		else
		{
                    $idAdminUpdatedBy=0;
                    $idForwarderContact=(int)$_SESSION['forwarder_user_id'];
		}
		$Excel_export_import= new cExport_Import();
		
		if(!empty($CFSLocationData))
		{
                    $CountData=count($CFSLocationData);
                    $query	="
                        INSERT INTO
                            ".__DBC_SCHEMATA_AWATING_APPROVAL__."
                        (
                            `idForwarder` ,
                            `idForwarderContact` ,
                            idAdminUpdateBy,
                            `iRecords`,
                            `dtCompletedOn`,
                            idBulkUploadService
                        )
                        VALUES 
                        (
                            ".(int)$idForwarder.",
                            ".(int)$idForwarderContact.",
                            '".(int)$idAdminUpdatedBy."',
                            '".(int)$CountData."',
                            NOW(),
                            '".(int)$idService."'
                        )								
                    ";
                    $result = $this->exeSQL( $query );

                    foreach($CFSLocationData as $CFSdata)
                    { 
                        $country_Id=$Excel_export_import->getCountryId(sanitize_all_html_input($CFSdata['7'])); 
                        $query = "
                            INSERT INTO
                                ".__DBC_SCHEMATA_WAREHOUSE_TEMP_DATA__."
                            (
                                szWareHouseName,
                                szAddress1,
                                szAddress2,
                                szAddress3,
                                szPostCode,
                                szCity,
                                szState,
                                idCountry,
                                szPhone,
                                szContactPerson,
                                szEmail,
                                szLatitude,
                                szLongitude,
                                dtCreatedOn,
                                idForwarder,
                                idBulkUploadService,
                                iWarehouseType
                            )
                            VALUES 
                            (
                                '".mysql_escape_custom($CFSdata['0'])."',
                                '".mysql_escape_custom($CFSdata['1'])."',
                                '".mysql_escape_custom($CFSdata['2'])."',
                                '".mysql_escape_custom($CFSdata['3'])."',
                                '".mysql_escape_custom($CFSdata['4'])."',
                                '".mysql_escape_custom($CFSdata['5'])."',
                                '".mysql_escape_custom($CFSdata['6'])."',
                                '".mysql_escape_custom($country_Id)."',
                                '".mysql_escape_custom($CFSdata['8'])."',
                                '".mysql_escape_custom($CFSdata['9'])."',
                                '".mysql_escape_custom($CFSdata['10'])."',
                                '".mysql_escape_custom($CFSdata['11'])."',
                                '".mysql_escape_custom($CFSdata['12'])."',
                                NOW(),
                                '".(int)$idForwarder."',
                                '".(int)$idService."',
                                '".(int)$iWarehouseType."'
                            )
                        ";
                        $result = $this->exeSQL( $query );
                    }

                    $query="
                        UPDATE
                            ".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
                        SET
                            iStatus='".(int)__DATA_REVIEWED_BY_FORWARDER__."'
                        WHERE
                            id='".(int)$idService."'		
                    ";
                    $result=$this->exeSQL($query);
                    return true;
		}
		else
		{
                    return false;
		}		
	} 
	function totalWaitingToBeApprovedCFSLocation($idForwarder,$idService=0,$idData=0,$idBatch=0,$iWarehouseType=false)
	{
            $sql='';
            if((int)$idData>0)
            {
                $sql .="  AND id='".(int)$idData."' ";
            }
            if((int)$idService>0)
            {
                $sql .=" AND idBulkUploadService='".(int)$idService."' ";
            }
            if((int)$idBatch>0)
            {
                $sql .=" AND idBatch='".(int)$idBatch."' ";
            }
            if((int)$iWarehouseType>0)
            {
                $sql .= " AND iWarehouseType = '".(int)$iWarehouseType."' ";
            }

            $query="
                SELECT
                    id,
                    szWareHouseName,
                    szAddress1,
                    szAddress2,
                    szAddress3,
                    szPostCode,
                    szCity,
                    szState,
                    idCountry,
                    szPhone,
                    szLatitude,
                    szLongitude,
                    iWarehouseType
                FROM
                    ".__DBC_SCHEMATA_WAREHOUSE_TEMP_DATA__."
                WHERE
                    idForwarder='".(int)$idForwarder."' 
                    ".$sql."	
            ";
            //echo $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                if( $this->getRowCnt() > 0 )
                {
                    $ctr = 0;
                    if((int)$idData>0)
                    {
                        while($row=$this->getAssoc($result))
                        {
                            $res_arr[$ctr]=$row;
                            $ctr++;
                        }
                    }
                    else
                    {
                        while($row=$this->getAssoc($result))
                        {
                            $res_arr[]=$row['id'];
                            $ctr++;
                        }
                    } 	
                    return $res_arr;
                }
            }
	}
        
	function deleteCFSLocationUnApprovedData($id,$idForwarder,$idService=0,$idBatch=0)
	{
		if((int)$id>0)
		{
			$sql='';
			$sql1='';
			if((int)$idService>0)
			{
				$sql .="
				AND
					idBulkUploadService='".(int)$idService."'
			";
				
				$sql1 .="
				AND
					idBulkUploadService='".(int)$idService."'
			";
			}
			if((int)$idBatch>0)
			{
				$sql .="
				AND
					idBatch='".(int)$idBatch."'
				";
				$sql1 .="
				AND
					id='".(int)$idBatch."'
				AND
					idBulkUploadService='0'
				";
			}
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_WAREHOUSE_TEMP_DATA__."
				WHERE
					id='".(int)$id."'
					".$sql." 
			";
			//echo $query."<br/>";
			$result = $this->exeSQL( $query );
			
			$query="
				SELECT
					iRecords
				FROM
					".__DBC_SCHEMATA_AWATING_APPROVAL__."
				WHERE
					idForwarder='".(int)$idForwarder."'
				".$sql1."
			";
			//echo $query."<br/>";
			if( ( $result = $this->exeSQL( $query ) ) )
			{
            	if( $this->getRowCnt() > 0 )
            	{
            		$row=$this->getAssoc($result);
            		$iRecords=$row['iRecords'];
            		if((int)$iRecords>0)
            		{
            			/*$query="
							UPDATE
								".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
							SET
								iActualRecords=iActualRecords-1
							WHERE
								id='".(int)$idService."'
						";
						$result=$this->exeSQL($query);*/
						
            			$query="
            				UPDATE
            					".__DBC_SCHEMATA_AWATING_APPROVAL__."
            				SET
            					iRecords=iRecords-1
            				WHERE
								idForwarder='".(int)$idForwarder."'
							".$sql1."
            			";
            			//echo $query."<br/>";
            			$result = $this->exeSQL( $query );
            			
            			if($iRecords==1)
            			{
            				$query="
            					DELETE FROM
            						".__DBC_SCHEMATA_AWATING_APPROVAL__."
            					WHERE
									idForwarder='".(int)$idForwarder."'
								".$sql1."
            				";
            				//echo $query."<br/>";
            				$result = $this->exeSQL( $query );
            			}
            		}
            	}
            	return true;
			}
		}
	}
	
	
	function getForwarderUploadCFSLocation($idForwarder)
	{
		$query="
                    SELECT
                        id,
                        iRecords,
                        idForwarderContact,
                        dtCompletedOn,
                        szDataRelatedTo,
                        idForwarder
                    FROM
                            ".__DBC_SCHEMATA_AWATING_APPROVAL__."
                    WHERE
                        idForwarder='".(int)$idForwarder."'
                    AND
                        idBulkUploadService='0'
                    AND
                        iTransfer='1'
		";
		if( ( $result = $this->exeSQL( $query ) ) )
		{
			if( $this->getRowCnt() > 0 )
			{
				while($row=$this->getAssoc($result))
				{
						$res_arr[]=$row;
				}
				return $res_arr;
			}
			else
			{
				return array();	
			}
		}
		else
		{
			return array();
		}
	}
	
	function deleteUploadedFileByForwarder($id)
	{
		$query="
			DELETE FROM
				".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
		    WHERE
		    	id='".(int)$id."'
		";
		if( ( $result = $this->exeSQL( $query ) ) )
		{
		
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_BULK_UPLOAD_COMMENT__."
				WHERE
					idBulkUploadService='".(int)$id."'
			";
			$result = $this->exeSQL( $query );
			
			$query="
				SELECT
					szFileName
				FROM
					".__DBC_SCHEMATA_BULK_UPLOAD_FILES__."
				WHERE
					idBulkUploadService='".(int)$id."'
			";
			if( ( $result = $this->exeSQL( $query ) ) )
			{
				if( $this->getRowCnt() > 0 )
				{
					while($row=$this->getAssoc($result))
					{
							$res_arr[]=$row['szFileName'];
					}
				}
			}
			
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_BULK_UPLOAD_FILES__."
				WHERE
					idBulkUploadService='".(int)$id."'
			";
			$result = $this->exeSQL( $query );
			if(!empty($res_arr))
			{
				foreach($res_arr as $res_arrs)
				{
					$fileurl=__UPLOAD_FORWARDER_BULK_SERVICES__."/$res_arrs";
					if(file_exists($fileurl) && $res_arrs!='')
					{
						unlink($fileurl);
					}
				}
			}
			return true;
		}
		else
		{
			return false;
		}
	}
	function findStatusOfCostApprovalAlarm($iStatus,$idForwarder,$iNewStatus=0)
	{
		$this->set_id((int)$iStatus);
		$this->set_idForwarder((int)$idForwarder);
		$sql='';
		if((int)$iNewStatus>0)
		{
			$sql .="
				(
					iStatus  =	".(int)$this->id."
				OR
					iStatus='".(int)$iNewStatus."'
				)	
			";
		}
		else
		{
			$sql .="
				iStatus  =	".$this->id."
			";
		}
		$query = "
				SELECT 
					iStatus
				FROM 
					".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
				WHERE
					".$sql."
				AND 
					iActive  = 1
				AND
					idForwarder = ".$this->idForwarder."		
		";
		//echo $query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
			if( $this->iNumRows > 0 )
			{
				return true;
			}
			else
			{
				return false; 
			}
		}
		else
  		{	
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
  		}	
	}
	function findStatusOfCostApprovalCheckAdmin($iStatus)
	{
		$this->set_id((int)$iStatus);
		$query = "
				SELECT 
					id
				FROM 
					".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
				WHERE
					iStatus  =	".$this->id."
				AND 
					iActive  = 1
		";
		//echo $query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
			if( $this->iNumRows > 0 )
			{
				return true;
			}
			else
			{
				return false; 
			}
		}
		else
  		{	
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
  		}	
	}
	
	function deleteLCLServiceData($id,$idForwarder)
	{
            $query="
                DElETE FROM
                    ".__DBC_SCHEMATA_WAREHOUSES_PRICING_APPROVAL_DATA__."
                WHERE
                    idBulkUploadService='".(int)$id."'
                AND
                    idForwarder='".(int)$idForwarder."'
            ";
            //echo $query;
            $result=$this->exeSQL($query);
            return true; 
	}
	
	function deleteCustomClearanceData($id,$idForwarder)
	{
		$query="
			DElETE FROM
				".__DBC_SCHEMATA_PRICING_CC_WAITING_FOR_APPROVAL__."
			WHERE
			 	idBulkUploadService='".(int)$id."'
			AND
				idForwarder='".(int)$idForwarder."'
		";
		//echo $query;
		$result=$this->exeSQL($query);
		return true;
	}
	
	function deleteHaulageData($id,$idForwarder)
	{
	
		$query="
			DELETE FROM
				".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_MODEl_APPROVAL_DATA__."
			WHERE
				idBulkUploadService='".(int)$id."'
			AND
				idForwarder='".(int)$idForwarder."'				
		";
		//echo $query;
		$result=$this->exeSQL($query);
		
		$query="
			DELETE FROM
				".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_APPROVAL_DATA__."
			WHERE
				idBulkUploadService='".(int)$id."'			
		";
		//echo $query;
		$result=$this->exeSQL($query);
				
		return true;
	}
	
	
	function deleteCFSLocationData($idService=0,$idForwarder,$idBatch=0,$iWarehouseType=__WAREHOUSE_TYPE_CFS__)
	{
            $sql='';
            $sql2='';
            if((int)$idService>0)
            {
                $sql .="idBulkUploadService='".(int)$idService."'";
                $sql2 .="idBulkUploadService='".(int)$idService."'";
            }

            if((int)$idBatch>0)
            {
                $sql2 .="idBatch='".(int)$idBatch."'";
                $sql .="id='".(int)$idBatch."'";
            }

            $query="
                DELETE FROM
                    ".__DBC_SCHEMATA_WAREHOUSE_TEMP_DATA__."
                WHERE
                    ".$sql2."
                AND
                    idForwarder='".(int)$idForwarder."'
                AND
                    iWarehouseType = '".(int)$iWarehouseType."'  
            ";
            //echo $query;
            $result=$this->exeSQL($query);

            $query="
                DELETE FROM
                    ".__DBC_SCHEMATA_AWATING_APPROVAL__."
                WHERE
                    ".$sql."
                AND
                    idForwarder='".(int)$idForwarder."'			
            ";
            //echo $query;
            $result=$this->exeSQL($query); 
            return true;
	}
	
	
	function totalWaitingToBeApprovedPricingHaulageTemp($idForwarder,$idService,$idWarehouse,$idHaulageModel,$iDirection)
	{
		$orderby='';
		$inner_query='';
		if($idHaulageModel=='4')
		{
			$orderby="
				ORDER BY
					hmp.iDistanceUpToKm ASC
			";
			
			$inner_query="
				INNER JOIN
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." AS hmp
				ON
					hmpa.idHaulgePricingModel=hmp.id
			";
			
		}
		$query="
			SELECT
				hmpa.idHaulgePricingModel
			FROM
				".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_MODEl_APPROVAL_DATA__." AS hmpa
				".$inner_query."
			WHERE
				hmpa.idForwarder='".(int)$idForwarder."'
			AND
				hmpa.idBulkUploadService='".(int)$idService."'
			AND
				hmpa.idWarehouse='".(int)$idWarehouse."'
			AND
				hmpa.iDirection='".(int)$iDirection."'
			AND
				hmpa.idHaulageModel='".(int)$idHaulageModel."'
			GROUP BY
				hmpa.idHaulgePricingModel
			".$orderby."
			
		";
		//echo $query."<br/><br/>";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{				
				while($row=$this->getAssoc($result))
				{
					$dataId=$row['idHaulgePricingModel'];
					$query="
						SELECT
							models.id,
							models.szName,
							models.idCountry,
							models.iRadiousKM,
							models.iDistanceUpToKm,
							models.idHaulageTransitTime,
							models.fWmFactor,
							models.fFuelPercentage,
							tt.iHours as iHours
						FROM
							".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." AS models
						INNER JOIN
								".__DBC_SCHEMATA_HAULAGE_TRANSIT_TIME__." tt
							ON
								tt.id = models.idHaulageTransitTime
						WHERE
							models.id='".(int)$dataId."'
					";
					//echo $query."<br/><br/>";
					if($result1=$this->exeSQL($query))
					{
						if($this->iNumRows>0)
						{
							while($row1=$this->getAssoc($result1))
							{
								$res_arr[]=$row1;
							}
							//return $res_arr;
						}
					}
					else
					{
						return array();
					}
				}
				return $res_arr;
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	
	function totalWaitingToBeApprovedPricingHaulageTempCount($idForwarder,$idService,$idWarehouse,$idHaulageModel,$iDirection)
	{
		$query="
			SELECT
				count(hp.id) as total
			FROM
				".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_MODEl_APPROVAL_DATA__." AS hpm
			INNER JOIN
				".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_APPROVAL_DATA__." AS hp
			ON
				hpm.idHaulgePricingModel=hp.idHaulagePricingModel
			WHERE
				hpm.idForwarder='".(int)$idForwarder."'
			AND
				hpm.idBulkUploadService='".(int)$idService."'
			AND
				hpm.idWarehouse='".(int)$idWarehouse."'
			AND
				hpm.iDirection='".(int)$iDirection."'
			AND
				hpm.idHaulageModel='".(int)$idHaulageModel."'
			AND
				hp.iUpToKg>0
		";
		//echo $query."<br/><br/>";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{				
				$row=$this->getAssoc($result);
				
				$res_data=$row['total'];	
				
				return $res_data;
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function totalHaulagePrincingCount($idForwarder,$idService)
	{
		$query="
			SELECT
				count(hpm.id) as total
			FROM
				".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_APPROVAL_DATA__." AS hpm
			WHERE
				hpm.idBulkUploadService='".(int)$idService."'";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$row=$this->getAssoc($result);
				
				$res_data=$row['total'];	
				
				return $res_data;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}
	
	function set_id( $value )
	{
		$this->id = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "id", "BulkUpload Id", false, false, true );
	}
	function set_szFileUploadType( $value )
	{
		$this->szFileUploadType = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "bulkFileType", "File Type", false, false, true );
	}
	function set_szFileName( $value )
	{
		$this->szFileName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "file_name", "File", false, false, true );
	}
	function set_szComment( $value )
	{
		$this->szComment = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szComment", "Comment",false, false, true );
	}
	function set_iRecords( $value )
	{
		$this->iRecords = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iRecords", t( $this->t_base."fields/iRecords" ),1, 10, true );
	}
	function set_szEstimatedTime( $value )
	{
		$this->szEstimatedTime = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szEstimatedTime", t( $this->t_base."fields/szEstimatedTime" ),false, 10, true );
	}
	function set_szProcessingCost( $value )
	{
		$this->szProcessingCost = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szProcessingCost", t( $this->t_base."fields/szProcessingCost" ),false, false, true );
	}
	
	function set_idOriginWarehouse( $value )
	{   
		$this->idOriginWarehouse = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idOriginWarehouse", t($this->t_base.'fields/origin')." ".t($this->t_base.'fields/cfs_name'), false, false, true );
	}
	function set_idOriginCountry( $value )
	{   
		$this->idOriginCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idOriginCountry", t($this->t_base.'fields/origin')." ".t($this->t_base.'fields/country'), false, false, true );
	}
	function set_szOriginCity( $value )
	{   
		$this->szOriginCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idOriginCity", t($this->t_base.'fields/origin')." ".t($this->t_base.'fields/city'), false, false, false );
	}
	
	function set_idDestinationWarehouse( $value )
	{   
		$this->idDestinationWarehouse = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idDestinationWarehouse", t($this->t_base.'fields/destination')." ".t($this->t_base.'fields/cfs_name'), false, false, true );
	}
	function set_idDestinationCountry( $value )
	{   
		$this->idDestinationCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idDestinationCountry", t($this->t_base.'fields/destination')." ".t($this->t_base.'fields/country'), false, false, true );
	}
	function set_szDestinationCity( $value )
	{   
		$this->szDestinationCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idDestinationCity", t($this->t_base.'fields/destination')." ".t($this->t_base.'fields/city'), false, false, false );
	}
	
	function set_fOriginPrice( $value )
	{   
		$this->fOriginPrice = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fOriginPrice", t($this->t_base.'fields/export')." ".t($this->t_base.'fields/price'), false, false, true );
	}
	function set_idOriginCurrency( $value )
	{   
		$this->idOriginCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idOriginCurrency", t($this->t_base.'fields/export')." ".t($this->t_base.'fields/currency'), false, false, true );
	}
	function set_fDestinationPrice( $value )
	{   
		$this->fDestinationPrice = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fDestinationPrice", t($this->t_base.'fields/import')." ".t($this->t_base.'fields/price'), false, false, true );
	}
	function set_idDestinationCurrency( $value )
	{   
		$this->idDestinationCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idDestinationCurrency", t($this->t_base.'fields/import')." ".t($this->t_base.'fields/currency'), false, false, true );
	}
	
	function set_szCurrency( $value )
	{
		$this->szCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "szCurrency", t($this->t_base.'fields/currency'), 1, false, true );
	}
	
	function set_iCrosssBorder( $value )
	{
		$this->iCrosssBorder = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iCrosssBorder", t($this->t_base.'fields/cross_border'), 0, false, true );
	}
	
	function set_PerWM( $value )
	{
		$this->fPerWM = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "per_wm", t($this->t_base.'fields/per_wm'), 0, false, true );
	}
	
	function set_Minimum( $value )
	{
		$this->fMinimum = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "minimum", t($this->t_base.'fields/minimum'), 0, false, true );
	}
	
	function set_PerBooking( $value )
	{
		$this->fPerBooking = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "per_booking", t($this->t_base.'fields/per_booking'), 0, false, true );
	}
	
	function set_Distance( $value )
	{
		$this->distance = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "upto", t($this->t_base.'fields/distance_up_to'), 0, false, true );
	}	
	function set_idTransitTime( $value )
	{
		$this->idTransitTime = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idTransitTime", t($this->t_base.'fields/haulage_transit_time'), 0, false, true );
	}	
	function set_expiryDate( $value )
	{
		$this->expiryDate = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "datepicker1", t($this->t_base.'fields/expiry_date'), false, 255, true );
	}
	function set_iBookingCutOffHours( $value )
	{
		$this->iBookingCutOffHours = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iBookingCutOffHours", t($this->t_base.'fields/booking_cut_off'), 0, false, true );
	}
	function set_szCutOffLocalTime( $value )
	{
		$this->szCutOffLocalTime = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCutOffLocalTime", t($this->t_base.'fields/cut_off_local_time'), false, 255, true );
	}
	function set_idCutOffDay( $value )
	{
		$this->idCutOffDay = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idCutOffDay", t($this->t_base.'fields/cut_off_day'), 0, false, true );
	}
	function set_szAvailableLocalTime( $value )
	{
		$this->szAvailableLocalTime = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAvailableLocalTime", t($this->t_base.'fields/available_local_time'), false, 255, true );
	}
	function set_idAvailableDay( $value )
	{
		$this->idAvailableDay = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idAvailableDay", t($this->t_base.'fields/available_day'), 0, false, true );
	}
	function set_iTransitHours( $value )
	{
		$this->iTransitHours = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iTransitHours", t($this->t_base.'fields/transit_hour'), 1, 100, true );
	}
	function set_idFrequency( $value )
	{
		$this->idFrequency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idFrequency", t($this->t_base.'fields/service_frequency'), 0, false, true );
	}

	function set_fOriginRateWM( $value,$flag=false)
	{
		$this->fOriginRateWM = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fOriginRateWM", t($this->t_base.'fields/origin_freight_rate')." ".t($this->t_base.'fields/per_wm'), 0, false, $flag );
	}
	function set_fOriginMinRateWM( $value,$flag=false )
	{
		$this->fOriginMinRateWM = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fOriginMinRateWM", t($this->t_base.'fields/origin_freight_rate')." ".t($this->t_base.'fields/minimum'), 0, false, $flag );
	}
	function set_fOriginRate( $value ,$flag=false)
	{
		$this->fOriginRate = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fOriginRate", t($this->t_base.'fields/origin_freight_rate')." ".t($this->t_base.'fields/per_booking'), 0, false, $flag );
	}
	function set_szOriginFreightCurrency( $value, $flag=false )
	{
		$this->szOriginFreightCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "szOriginFreightCurrency", t($this->t_base.'fields/origin_freight_rate')." ".t($this->t_base.'fields/currency'), 0, false, $flag );
	}	
	
	function set_fRateWM( $value )
	{
		$this->fRateWM = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fRateWM", t($this->t_base.'fields/freight_rate')." ".t($this->t_base.'fields/per_wm'), 0, false, true );
	}
	function set_fMinRateWM( $value )
	{
		$this->fMinRateWM = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fMinRateWM", t($this->t_base.'fields/freight_rate')." ".t($this->t_base.'fields/minimum'), 0, false, true );
	}
	function set_fRate( $value )
	{
		$this->fRate = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fRate", t($this->t_base.'fields/freight_rate')." ".t($this->t_base.'fields/per_booking'), 0, false, true );
	}
	function set_szFreightCurrency( $value )
	{
		$this->szFreightCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "szFreightCurrency", t($this->t_base.'fields/freight_rate')." ".t($this->t_base.'fields/currency'), 0, false, true );
	}	
	function set_fDestinationRateWM( $value ,$flag=false )
	{
		$this->fDestinationRateWM = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fDestinationRateWM", t($this->t_base.'fields/destination_freight_rate')." ".t($this->t_base.'fields/per_wm'), 0, false, $flag );
	}
	function set_fDestinationMinRateWM( $value,$flag=false )
	{
		$this->fDestinationMinRateWM = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fDestinationMinRateWM", t($this->t_base.'fields/destination_freight_rate')." ".t($this->t_base.'fields/minimum'), 0, false, $flag );
	}
	function set_fDestinationRate( $value,$flag=false )
	{
		$this->fDestinationRate = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fDestinationRate", t($this->t_base.'fields/destination_freight_rate')." ".t($this->t_base.'fields/per_booking'), 0, false, $flag );
	}
	function set_szDestinationFreightCurrency( $value ,$flag=false)
	{
		$this->szDestinationFreightCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "szDestinationFreightCurrency", t($this->t_base.'fields/destination_freight_rate')." ".t($this->t_base.'fields/currency'), 0, false, $flag );
	}	
	function set_dtValidFrom( $value )
	{   
		$this->dtValidFrom = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "dtValidFrom", t($this->t_base.'fields/validity')." ".t($this->t_base.'fields/from'), false, false, true );
	}
	function set_dtExpiry( $value )
	{   
		$this->dtExpiry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "dtExpiry", t($this->t_base.'fields/validity')." ".t($this->t_base.'fields/to'), false, false, true );
	}
	
	function set_fPricePer100Kg( $value )
	{
		$this->fPricePer100Kg = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fPricePer100Kg", t($this->t_base_approve.'fields/price_per_100_kg'), 0, false, true );
	}
	function set_iUpToKg( $value )
	{
		$this->iUpToKg = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iUpToKg", t($this->t_base_approve.'fields/weight_up_to'), 1, false, true );
	}
	function set_fMinimumPrice( $value )
	{
		$this->fMinimumPrice = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fMinimumPrice", t($this->t_base_approve.'fields/minimum_price'), 0, false, true );
	}
	function set_fPricePerBooking( $value )
	{
		$this->fPricePerBooking = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fPricePerBooking",  t($this->t_base_approve.'fields/price_per_booking'), 0, false, true );
	}
	function set_idCurrency( $value )
	{
		$this->idCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idCurrency",  t($this->t_base_approve.'fields/currency'), false, false, true );
	}
	function set_idForwarder( $value )
	{
		$this->idForwarder = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idForwarder", "Forwarder", false, false, true );
	}
	
}