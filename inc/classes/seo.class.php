<?php
/**
 * This file is the container for Explain related functionality.
 * All functionality related to Explain details should be contained in this class.
 *
 * explain.class.php
 *
 * @copyright Copyright (C) 2012 Transporteca, LLC
 * @author Anil
 * @package Transporteca Development
 */
 
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );

require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_INC__ . "/functions.php" );
require_once( __APP_PATH_CLASSES__ . "/database.class.php");

class cSEO extends cDatabase
{
    /**
     * These are the variables for Explanaition Details.
     */
    var $id; 
    var $idExplain; 
    var $szUrl;
   	var $idAdmin;
   	var $maxOrder;
   	var $iOrderBy;
   	var $szComment;
   	var $szPageHeading;
   	var $szLink;
   	var $szMetaTitle;
   	var $szMetaKeyword;
   	var $szMetaDescription;
   	var $iActive;
 	var $iPublished;
 	var $dtCreatedOn; 
 	   
   	var $t_base="management/Error/";
   	var $staticPageAry = array();
   	
   	function __construct()
   	{
   		parent::__construct();
   		return true;
   	}
   	
	function addSeoPageData($landingContent)
	{
            if(!empty($landingContent))
            {
                $this->validateSeoPageData($landingContent);
                if($this->error == true)
                {
                        return false;
                }

                if($this->isUrlExists($this->szUrl))
                {
                    $this->addError("szUrl", t($this->t_base.'fields/url_already_exists'));
                    return false;
                }

                $iMaxOrder = $this->getMaxOrderSeoPageData();

                $query = "
                    INSERT INTO 
                        ".__DBC_SCHEMATA_SEO_PAGE_DATA__."
                    (
                        szPageHeading,
                        szContent,
                        szMetaTitle,
                        szMetaKeywords,
                        szMetaDescription,
                        szUrl,
                        szLinkTitle,
                        iPublished,
                        iLanguage,
                        iAvailableInLeftMenu,
                        szFromSelection,
                        szToSelection,
                        iFromIPLocation,
                        iToIPLocation,
                        fCargoVolume,
                        fCargoWeight,
                        dtCreatedOn,
                        iActive,
                        iOrder
                    ) 
                    VALUES 
                    (
                        '".mysql_escape_custom(trim($this->szPageHeading))."', 
                        '".mysql_escape_custom(trim($this->szContent))."', 
                        '".mysql_escape_custom(trim($this->szMetaTitle))."', 
                        '".mysql_escape_custom(trim($this->szMetaKeywords))."', 
                        '".mysql_escape_custom(trim($this->szMetaDescription))."',
                        '".mysql_escape_custom(trim($this->szUrl))."', 
                        '".mysql_escape_custom(trim($this->szLinkTitle))."',  
                        '".(int)mysql_escape_custom(trim($this->iPublished))."', 
                        '".(int)mysql_escape_custom(trim($this->iLanguage))."', 
                        '".mysql_escape_custom(trim($this->iAvailableInLeftMenu))."',
                        '".mysql_escape_custom(trim($this->szFromSelection))."',
                        '".mysql_escape_custom(trim($this->szToSelection))."',
                        '".mysql_escape_custom(trim($this->iFromIPLocation))."',
                        '".mysql_escape_custom(trim($this->iToIPLocation))."',
                        '".mysql_escape_custom(trim($this->fCargoVolume))."',
                        '".mysql_escape_custom(trim($this->fCargoWeight))."', 
                        now(),
                        '1',
                        '". (int)($iMaxOrder+1) ."'
                    )
                ";
                //echo $query;
                //die;
                if( ( $result = $this->exeSQL( $query ) ) )
                {
                    $this->updateSitemapXmlFile();
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
            else
            {
                return false;
            }
	}
	
	function isUrlExists($szUrl,$id=false)
	{
		if(!empty($szUrl))
		{
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_SEO_PAGE_DATA__."
				WHERE
					szUrl = '".mysql_escape_custom(trim($szUrl))."'
				AND
					iActive='1'
			";
			
			if($id>0)
			{
				$query.="AND
						id<>'".(int)$id."'
					";
			}
			
			//echo $query;
			if($result = $this->exeSQL( $query ) )
			{
				if($this->getRowCnt() > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function getAllSEOPageData($idLandingPage=false,$iLanguage=false)
	{
            if($idLandingPage>0)
            {
                $query_and= " WHERE id='".(int)$idLandingPage."' ";
            }
            else
            {
                $query_and= " WHERE iActive = '1' ";
            }

            if($iLanguage>0)
            {
                $query_and .= " AND lpd.iLanguage = '".(int)$iLanguage."' ";
            }
            
            $query="
                SELECT
                    lpd.id,
                    lpd.szPageHeading,
                    lpd.szMetaTitle,
                    lpd.szMetaKeywords,
                    lpd.szMetaDescription,
                    lpd.szUrl,
                    lpd.iPublished,
                    lpd.szContent,
                    lpd.dtCreatedOn,
                    lpd.iActive,
                    lpd.iOrder,
                    lpd.iLanguage,
                    lpd.szLinkTitle,
                    lpd.iAvailableInLeftMenu,
                    lpd.szFromSelection,
                    lpd.szToSelection,
                    lpd.iFromIPLocation,
                    lpd.iToIPLocation,
                    lpd.fCargoVolume,
                    lpd.fCargoWeight,
                    (SELECT count(st.id) FROM ".__DBC_SCHEMATA_SEO_PAGE_TRACKER__." st WHERE st.idSeoPage = lpd.id) as iAquisionCount
                FROM
                    ".__DBC_SCHEMATA_SEO_PAGE_DATA__." lpd
                $query_and
                ORDER BY
                    iOrder ASC
            ";
            //echo $query;
            if($result=$this->exeSQL($query))
            {
                $ret_ary = array();
                $ctr=0;
                while($row=$this->getAssoc($result))
                {
                    $ret_ary[$ctr] = $row;
                    $ctr++;
                }
                return $ret_ary;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
	function getSeoPageDataByUrl($szURL=false,$iDefaultPage=false,$iReturn_all_published_record=false,$idSeoPage=false,$iLanguage=false)
	{
		if(!empty($szURL))
		{
                    $query_where = " AND szUrl = '".mysql_escape_custom(trim($szURL))."' ";
		} 
		else if($iReturn_all_published_record)
		{
                    $query_where = " AND  iPublished = '1' ";
                    $query_limit = " LIMIT 0,1 ";
		}
		else if($idSeoPage>0)
		{
                    $query_where = " AND id = '".(int)$idSeoPage."' "; 
		}
		else
		{
                    return false;
		}
		
		if($iLanguage>0)
		{
                    $query_where .= " AND iLanguage = '".(int)$iLanguage."' " ;
		}
		
		$query="
                    SELECT
                        lpd.id,
                        lpd.szPageHeading,
                        lpd.szMetaTitle,
                        lpd.szMetaKeywords,
                        lpd.szMetaDescription,
                        lpd.szUrl,
                        lpd.iPublished,
                        lpd.szContent,
                        lpd.dtCreatedOn,
                        lpd.iActive,
                        lpd.iOrder,
                        lpd.szLinkTitle
                    FROM
                        ".__DBC_SCHEMATA_SEO_PAGE_DATA__." lpd
                    WHERE
                        iActive='1' 
                        $query_where
                    ORDER BY 
                        iOrder ASC
                    $query_limit
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			$ret_ary = array();
			if($iReturn_all_published_record)
			{
				$ctr=0;
				while($row=$this->getAssoc($result))
				{
					$ret_ary[$ctr] = $row;
					$ctr++;
				}
			}
			else
			{
				$row=$this->getAssoc($result);
				$ret_ary = $row;
			}
			return $ret_ary;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	function getAllPublishedSeoPages($iLanguage=false,$iAvailableInLeftMenu=false)
	{	
            if($iLanguage>0)
            {
                $query_and = " AND lpd.iLanguage = '".(int)$iLanguage."' ";
            }
            if($iAvailableInLeftMenu)
            {
                $query_and .= " AND lpd.iAvailableInLeftMenu = '1' ";
            }

            $query="
                SELECT
                    lpd.id,
                    lpd.szPageHeading,
                    lpd.szMetaTitle,
                    lpd.szMetaKeywords,
                    lpd.szMetaDescription,
                    lpd.szUrl,
                    lpd.iPublished,
                    lpd.szContent,
                    lpd.dtCreatedOn,
                    lpd.iActive,
                    lpd.iOrder,
                    lpd.dtUpdatedOn,
                    lpd.iLanguage,
                    lpd.szLinkTitle,
                    lpd.iAvailableInLeftMenu,
                    lpd.szFromSelection,
                    lpd.szToSelection,
                    lpd.iFromIPLocation,
                    lpd.iToIPLocation,
                    lpd.fCargoVolume,
                    lpd.fCargoWeight
                FROM
                    ".__DBC_SCHEMATA_SEO_PAGE_DATA__." lpd
                WHERE
                    iActive= '1'
                AND
                    iPublished = '1'	
                    $query_and
                ORDER BY 
                    iOrder ASC
            ";
            //echo $query;
            if($result=$this->exeSQL($query))
            {
                $ret_ary = array();
                $ctr=0;
                while($row=$this->getAssoc($result))
                {
                    $ret_ary[$ctr] = $row;
                    $ctr++;
                }
                return $ret_ary;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
	
	function validateSeoPageData($landingContent)
	{ 
            if(!empty($landingContent))
            {
                if($landingContent['iPublished']==1)
                {
                    $require_feild_flag = true;
                }
                else
                {
                    $require_feild_flag = false;
                }
                $this->set_szPageHeading(sanitize_all_html_input($landingContent['szPageHeading']));
                $this->set_szMetaTitle(sanitize_all_html_input($landingContent['szMetaTitle']),$require_feild_flag);
                //$this->set_szMetaKeywords(sanitize_all_html_input($landingContent['szMetaKeyword']),$require_feild_flag);
                $this->set_szLinkTitle(sanitize_all_html_input($landingContent['szLinkTitle']),$require_feild_flag);

                $this->set_szMetaDescription(sanitize_all_html_input($landingContent['szMetaDescription']),$require_feild_flag);
                $this->set_szUrl(sanitize_all_html_input($landingContent['szLink']),$require_feild_flag);		
                $this->set_iPublished((int)$landingContent['iPublished']);
                $this->set_szContent($landingContent['szContent'],$require_feild_flag);
                $this->set_iLanguage((int)$landingContent['iLanguage']);	

                $this->set_iAvailableInLeftMenu((int)$landingContent['iAvailableInLeftMenu']);	
                $this->set_iFromIPLocation((int)$landingContent['iFromIPLocation']);	
                $this->set_iToIPLocation((int)$landingContent['iToIPLocation']); 
                $this->set_szFromSelection(sanitize_all_html_input($landingContent['szFromSelection'])); 
                $this->set_szToSelection(sanitize_all_html_input($landingContent['szToSelection']));
                $this->set_fCargoVolume(sanitize_all_html_input($landingContent['fCargoVolume']));
                $this->set_fCargoWeight(sanitize_all_html_input($landingContent['fCargoWeight']));

                if($this->error == true)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
	}
	
	function getMaxOrderSeoPageData()
	{
		$query="
			SELECT
				MAX(iOrder)  as orders
			FROM
				".__DBC_SCHEMATA_SEO_PAGE_DATA__."
			WHERE
				iActive='1'
			";
		//echo "<br /> ".$query."<br /> ";
		if($result=$this->exeSQL($query))
		{
			$row = $this->getAssoc( $result );
			if($row['orders']>0)
			{
				return $row['orders'];
			}
			else
			{
				return 0;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function editSeoPage($landingContent)
	{
            if(!empty($landingContent))
            {
                $this->validateSeoPageData($landingContent);
                $this->set_id((int)$landingContent['idSeoPage']);

                if($this->error == true)
                {
                    return false;
                }

                if($this->isUrlExists($this->szUrl,$this->id))
                {
                    $this->addError("szUrl", t($this->t_base.'fields/url_already_exists'));
                    return false;
                }

                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_SEO_PAGE_DATA__."
                    SET
                        szPageHeading = '".mysql_escape_custom(trim($this->szPageHeading))."',
                        szContent = '".mysql_escape_custom(trim($this->szContent))."',
                        szMetaTitle = '".mysql_escape_custom(trim($this->szMetaTitle))."',
                        szMetaKeywords = '".mysql_escape_custom(trim($this->szMetaKeywords))."',
                        szMetaDescription = '".mysql_escape_custom(trim($this->szMetaDescription))."',
                        szUrl = '".mysql_escape_custom(trim($this->szUrl))."',
                        szLinkTitle = '".mysql_escape_custom(trim($this->szLinkTitle))."',
                        iPublished = '".(int)$this->iPublished."',
                        iLanguage = '".(int)$this->iLanguage."',
                        iAvailableInLeftMenu = '".(int)$this->iAvailableInLeftMenu."',
                        szFromSelection = '".mysql_escape_custom(trim($this->szFromSelection))."',
                        szToSelection = '".mysql_escape_custom(trim($this->szToSelection))."',
                        iFromIPLocation = '".mysql_escape_custom(trim($this->iFromIPLocation))."',
                        iToIPLocation = '".mysql_escape_custom(trim($this->iToIPLocation))."',
                        fCargoVolume = '".mysql_escape_custom(trim($this->fCargoVolume))."',
                        fCargoWeight = '".mysql_escape_custom(trim($this->fCargoWeight))."',
                        dtUpdatedOn = now()
                    WHERE
                        id = '".(int)$this->id."'
                ";
//                echo "<br /> ".$query."<br /> ";
//                die;
                if($result=$this->exeSQL($query))
                {
                    $this->updateSitemapXmlFile();
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
	
	function moveUpDownSeoPagesLinks($id,$orderBy)
	{
		$this->set_id((int)$id);
		$this->set_iOrderBy((int)$orderBy);
		$query="
   				SELECT
   					id,
   			 		iOrder
   			 	FROM
   			 		".__DBC_SCHEMATA_SEO_PAGE_DATA__."
   			 	WHERE
   			 		id='".(int)$this->id."'
   				";
		//echo $query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
			if ($this->getRowCnt() > 0)
			{
				$row = $this->getAssoc( $result);
				$order = $row['iOrder'];
				$this->id = $row['id'];
					
				$query="
						UPDATE
							".__DBC_SCHEMATA_SEO_PAGE_DATA__."
					 	SET
					 		";
				if($this->iOrderBy==__ORDER_MOVE_DOWN__)
				{
					$query.= " 	iOrder = iOrder-1 ";
				}
				if($this->iOrderBy==__ORDER_MOVE_UP__)
				{
					$query .=" iOrder = iOrder+1 ";
				}
				
				$query.=" WHERE
					 		id='".(int)$this->id."'
				 		AND
					 		iActive='1'
						";
				
				//echo $query;
					
				if($result = $this->exeSQL( $query ))
				{
					$query="
							UPDATE
								".__DBC_SCHEMATA_SEO_PAGE_DATA__."
							SET
							";
					if($this->iOrderBy==__ORDER_MOVE_DOWN__)
					{
						$query.=" iOrder = iOrder+1 ";
					}
					if($this->iOrderBy==__ORDER_MOVE_UP__)
					{
						$query.=" iOrder = iOrder-1 ";
					}
					
					$query.="
							WHERE
								id!='".$this->id."'
							AND
					 			iActive='1'
							AND
								";
					if($this->iOrderBy==__ORDER_MOVE_DOWN__)
					{
						$query.=" iOrder= ".(int)$order."-1 ";
					}
					if($this->iOrderBy==__ORDER_MOVE_UP__)
					{
						$query.=" iOrder= ".(int)$order."+1 ";
					}
					
					//echo $query;
					if($result = $this->exeSQL( $query ))
					{
						return true;
					}
					else
					{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					}
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
								return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	

	function deleteSeoPage($id)
	{
		$this->set_id((int)$id);
		if($this->id>0)
		{
			$query="
   				SELECT
   			 		iOrder
   			 	FROM
   			 		".__DBC_SCHEMATA_SEO_PAGE_DATA__."
   			 	WHERE
   			 		id='".(int)$id."'
   				";
				
			if( ( $result = $this->exeSQL( $query ) ) )
			{
				if ($this->getRowCnt() > 0)
				{
					$row = $this->getAssoc( $result);
					$order = $row['iOrder'];
	
					$query="
						UPDATE
							".__DBC_SCHEMATA_SEO_PAGE_DATA__."
						SET
						    iActive = '0'
						WHERE
							id = '".(int)$this->id."'
						";
					//echo $query;
					if($result=$this->exeSQL($query))
					{
						$query="
							UPDATE
								".__DBC_SCHEMATA_SEO_PAGE_DATA__."
						 	SET
								iOrder = iOrder-1
							 WHERE
						 		iOrder > '".(int)$order."'
						 	 AND
						 		iActive='1'
					 	";
						//echo $query;DIE;
						if( $result = $this->exeSQL($query))
						{
							return true;
						}
						
						$this->updateSitemapXmlFile();
					}
					else
					{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					}
				}
			}
		}
	}
	
	function getSitemapVideo()
	{
		$query="
			SELECT
				id,
				szVideoCode,
				dtUpdatedOn
			FROM
				".__DBC_SCHEMATA_SITEMAP_VIDEO_DATA__."
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			$row = $this->getAssoc($result);
			return $row;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}		
	}
	
	function updateSitemapVideo($data)
	{
		if(!empty($data))
		{
			$this->set_szVideoCode(trim($data['szVideoCode']));
			$this->set_id((int)$data['idVideoCode']);
			
			if($this->error == true)
			{
				return false;
			}
				
			if($this->isUrlExists($this->szUrl,$this->id))
			{
				$this->addError("szUrl", t($this->t_base.'fields/url_already_exists'));
				return false;
			}
				
			$query="
				UPDATE
					".__DBC_SCHEMATA_SITEMAP_VIDEO_DATA__."
				SET
					szVideoCode = '".mysql_escape_custom(trim($this->szVideoCode))."',
					dtUpdatedOn = now()
				WHERE
					id = '".(int)$this->id."'
			";
			//echo "<br /> ".$query."<br /> ";
			if($result=$this->exeSQL($query))
			{
				$this->updateSitemapXmlFile();
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
			
		}
	}
	
	function add_xml_item($data)
	{		
            $locationUrl = $data['szLocationUrl'];
            $szMetaTitle = $data['szMetaTitle'];
            $szMetaDescription = $data['szMetaDescription'];
            $szMetaKeywords = $data['szMetaKeywords'];
            $dtCreatedOn = $data['dtCreatedOn'];
            $dtUpdatedOn = $data['dtUpdatedOn'];

            $szMetaTitle = preg_replace_callback('/&(?!#?[a-z0-9]+;)/', '&amp;', $data['szMetaTitle']);
            $szMetaDescription = preg_replace_callback('/&(?!#?[a-z0-9]+;)/', '&amp;', $data['szMetaDescription']);
            $szMetaKeywords = preg_replace_callback('/&(?!#?[a-z0-9]+;)/', '&amp;', $data['szMetaKeywords']);

            $SLASH    = '/';
            $title = strtolower($title);

            if(!empty($dtUpdatedOn) && $dtUpdatedOn!='0000-00-00 00:00:00')
            {
                    $dtLastmodified = date('Y-m-d',strtotime($dtUpdatedOn));
            }
            else if(!empty($dtCreatedOn) && $dtCreatedOn!='0000-00-00 00:00:00')
            {
                    $dtLastmodified = date('Y-m-d',strtotime($dtCreatedOn));
            }
            else
            {
                    $dtLastmodified = date('Y-m-d');
            }

            $result = "<url>"
                            .  "\n \t"."<loc>".$locationUrl.'</loc>'."\n"
                                    . "\t".'<lastmod>'.$dtLastmodified.'</lastmod>'."\n"
                                            . "\t".'<changefreq>weekly</changefreq>'."\n"
                                                            . '</url>'."\n";
            return $result;
	}
	
	function updateSitemapXmlFile()
	{
		$xml_header_text  = '<?xml version="1.0" encoding="UTF-8"?>'."\n".'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">';
		$xml_body_text = "\n";
		 
		/*
		$kExplain = new cExplain();
		$landingPageAry = $kExplain->getLandingPageDataByUrl(false,false,true);
		
		if(!empty($landingPageAry))
		{
			foreach($landingPageAry as $landingPageArys)
			{
				$szUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__.'/'.$landingPageArys['szUrl']."/" ;
				
				$data = array();
				$data['szLocationUrl'] = $szUrl;
				$data['szMetaTitle'] = $landingPageArys['szMetaTitle'];
				$data['szMetaDescription'] = $landingPageArys['szMetaDescription'];
				$data['szMetaKeywords'] = $landingPageArys['szMetaKeywords'];
				$data['dtCreatedOn'] = $landingPageArys['dtCreatedOn'];
				$data['dtUpdatedOn'] = $landingPageArys['dtUpdatedOn'];
				
				$xml_content = $this->add_xml_item($data);
				$xml_body_text .= $xml_content ;
			}
		}
		*/
		
		$kExplain = new cExplain();
		$landingPageAry=array();
		$landingPageAry = $kExplain->getAllNewLandingPageData(false,false,false,false,true);
		
		if(!empty($landingPageAry))
		{
                    foreach($landingPageAry as $landingPageArys)
                    {
                        /*
                         * We have excluded following 2 pages from sitemap.xml 
                         * 
                         * 14. /labels/
                         * 15. /da/labels/
                         */ 
                        if($landingPageArys['id']==14 || $landingPageArys['id']==15)
                        {
                            continue;
                        }
                        else
                        { 
                            if($landingPageArys['szUrl']!='/')
                            {
                                $szUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__.'/'.$landingPageArys['szUrl'] ;
                            }
                            else
                            { 
                                $szUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__ ;
                            }

                            $data = array();
                            $data['szLocationUrl'] = $szUrl;
                            $data['szMetaTitle'] = $landingPageArys['szMetaTitle'];
                            $data['szMetaDescription'] = $landingPageArys['szMetaDescription'];
                            $data['szMetaKeywords'] = $landingPageArys['szMetaKeywords'];
                            $data['dtCreatedOn'] = $landingPageArys['dtCreatedOn'];
                            $data['dtUpdatedOn'] = $landingPageArys['dtUpdatedOn'];

                            $xml_content = $this->add_xml_item($data);
                            $xml_body_text .= $xml_content ;
                        }
                    }
		}
		
		$staticPageAry = $this->getallStaticPages();
		if(!empty($staticPageAry))
		{
                    foreach($staticPageAry as $staticPageArys)
                    {
                        $xml_content = $this->add_xml_item($staticPageArys);
                        $xml_body_text .= $xml_content ;
                    }
		}
		
		//getting all active and published seopages
//		$seoPageAry = $this->getAllPublishedSeoPages();
//		
//		if(!empty($seoPageAry))
//		{
//			foreach($seoPageAry as $seoPageArys)
//			{
//				if($seoPageArys['iLanguage']==2)
//				{
//					$szUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__.'/da/information/'.$seoPageArys['szUrl'] ;
//				}
//				else
//				{
//					$szUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__.'/information/'.$seoPageArys['szUrl'] ;
//				}
//				$data = array();
//				$data['szLocationUrl'] = $szUrl;
//				$data['szMetaTitle'] = $seoPageArys['szMetaTitle'];
//				$data['szMetaDescription'] = $seoPageArys['szMetaDescription'];
//				$data['szMetaKeywords'] = $seoPageArys['szMetaKeywords'];
//				$data['dtCreatedOn'] = $seoPageArys['dtCreatedOn'];
//				$data['dtUpdatedOn'] = $seoPageArys['dtUpdatedOn'];
//				
//				$xml_content = $this->add_xml_item($data);
//				$xml_body_text .= $xml_content ;
//			}
//		}
                
                $kConfig = new cConfig();
                $languageArr=$kConfig->getLanguageDetails();
                //$languageArr =array(array("szLanguageCode"=>"en"),array("szLanguageCode"=>"da"));
                if(!empty($languageArr))
                {
                    require(__APP_PATH_ROOT__."/".__WORDPRESS_FOLDER__.'/wp-config.php');
                    foreach($languageArr as $languageArrs)
                    {
                        $szLanguageCode=  strtolower($languageArrs['szLanguageCode']);
                        $mainMenuArr=$kExplain->getMainMenuParentData($szLanguageCode);
                        
                        if(!empty($mainMenuArr))
                        {   
                            foreach($mainMenuArr as $mainMenuArrs)
                            {
                                $howDoesworksLinks=$kExplain->getWordpressExplainPageDataForMenu($mainMenuArrs['ID']);
                                if(!empty($howDoesworksLinks))
                                {
                                    foreach($howDoesworksLinks as $howDoesworksLinkss)
                                    {
                                        $redir_da_url='';
                                        $redir_da_url = get_post_meta($howDoesworksLinkss['id'],'_custom_permalink',true);
                                        if(empty($redir_da_url))
                                        {
                                           $redir_da_url = get_post_meta($howDoesworksLinkss['id'],'custom_permalink',true); 
                                        }
                                        if($redir_da_url!='')
                                        {
                                            $redir_da_url= __MAIN_SITE_HOME_PAGE_URL__.'/'.$redir_da_url;
                                        }
                                        else
                                        {
                                            $redir_da_url = get_page_link( $howDoesworksLinkss['id'] );
                                        }
                                        if($howDoesworksLinkss['post_title']!='')
                                        {
                                            $data = array();
                                            $data['szLocationUrl'] = $redir_da_url;
                                            $data['dtCreatedOn'] = $howDoesworksLinkss['post_date'];
                                            $data['dtUpdatedOn'] = $howDoesworksLinkss['post_modified'];
                                            $xml_content = $this->add_xml_item($data);
                                            $xml_body_text .= $xml_content ;
                                        }
                                    }
                                }
                            }
                        }
                        if($szLanguageCode=='en')
                            $menuRightFooter=__ENGLISH_RIGHT_FOOTER_MENU__;
                        else    
                            $menuRightFooter=__ENGLISH_RIGHT_FOOTER_MENU__."".strtoupper($szLanguageCode);
    
                        $menurightarr=wp_get_nav_menu_items($menuRightFooter);
                        //print_r($menuarr);
                        $menurightarr=(toArray($menurightarr));
                        if(!empty($menurightarr))
                        {
                            foreach($menurightarr as $menurightarrs)
                            {
                                $data = array();
                                $data['szLocationUrl'] = $menurightarrs['url'];
                                $data['dtCreatedOn'] = $howDoesworksLinkss['post_date'];
                                $data['dtUpdatedOn'] = $howDoesworksLinkss['post_modified'];
                                $xml_content = $this->add_xml_item($data);
                                $xml_body_text .= $xml_content ;
                                
                            }
                        }
                    }
                }
                
		
//			$explainPageAry = $kExplain->selectPageDetailsUserEnd($iLanguage,__HOW_IT_WORKS__);
//			if(!empty($explainPageAry))
//			{
//				foreach($explainPageAry as $explainPageArys)
//				{
//					$explainContentArr=array();
//					$explainContentArr=$kExplain->getAllExplainContainDataByIdExplain($explainPageArys['id']);
//					
//					if($explainPageArys['iLanguage']==2)
//					{
//						$szUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/da/".__HOW_IT_WORKS_URL_DA__."/".$explainPageArys['szLink'] ;
//					}
//					else
//					{
//						$szUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".__HOW_IT_WORKS_URL__."/".$explainPageArys['szLink'] ;
//					}
//					
//					$data = array();
//					$data['szLocationUrl'] = $szUrl;
//					$data['dtCreatedOn'] = $explainPageArys['dtCreatedOn'];
//					$data['dtUpdatedOn'] = $explainPageArys['dtUpdatedOn'];
//				 	$xml_content = $this->add_xml_item($data);
//					$xml_body_text .= $xml_content ;
//					/*
//					if(!empty($explainContentArr))
//					{
//						foreach($explainContentArr as $explainContentArrs)
//						{
//						
//							if($explainPageArys['iLanguage']==2)
//							{
//								$szUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/da/".__HOW_IT_WORKS_URL_DA__."/".$explainPageArys['szLink']."/".$explainContentArrs['szUrl']."/";
//							}
//							else
//							{
//								$szUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".__HOW_IT_WORKS_URL__."/".$explainPageArys['szLink']."/".$explainContentArrs['szUrl']."/";
//							}
//						
//							$data = array();
//							$data['szLocationUrl'] = $szUrl;
//							$data['szMetaTitle'] = $explainContentArrs['szMetaTitle'];
//							$data['szMetaDescription'] = $explainContentArrs['szMetaDescription'];
//							$data['szMetaKeywords'] = $explainContentArrs['szMetaKeywords'];
//							$data['dtCreatedOn'] = $explainContentArrs['dtCreatedOn'];
//							$data['dtUpdatedOn'] = $explainContentArrs['dtUpdatedOn'];
//							$xml_content = $this->add_xml_item($data);
//							$xml_body_text .= $xml_content ;
//						}
//					} */
//				}
//			}
//			
//			
//			
//			$explainPageAry = $kExplain->selectPageDetailsUserEnd($iLanguage,__COMPANY__);
//			if(!empty($explainPageAry))
//			{
//				foreach($explainPageAry as $explainPageArys)
//				{
//					$explainContentArr=array();
//					$explainContentArr=$kExplain->getAllExplainContainDataByIdExplain($explainPageArys['id']);
//					
//					if($explainPageArys['iLanguage']==2)
//					{
//						$szUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/da/".__COMPANY_URL_DA__."/".$explainPageArys['szLink'] ;
//					}
//					else
//					{
//						$szUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".__COMPANY_URL__."/".$explainPageArys['szLink'] ;
//					}
//					
//					$data = array();
//					$data['szLocationUrl'] = $szUrl;
//					$data['dtCreatedOn'] = $explainPageArys['dtCreatedOn'];
//					$data['dtUpdatedOn'] = $explainPageArys['dtUpdatedOn'];
//				 	$xml_content = $this->add_xml_item($data);
//					$xml_body_text .= $xml_content ;
//					
//					/*
//					if(!empty($explainContentArr))
//					{
//						foreach($explainContentArr as $explainContentArrs)
//						{
//						
//							if($explainPageArys['iLanguage']==2)
//							{
//								$szUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/da/".__COMPANY_URL_DA__."/".$explainPageArys['szLink']."/".$explainContentArrs['szUrl']."/";
//							}
//							else
//							{
//								$szUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".__COMPANY_URL__."/".$explainPageArys['szLink']."/".$explainContentArrs['szUrl']."/";
//							}
//						
//							$data = array();
//							$data['szLocationUrl'] = $szUrl;
//							$data['szMetaTitle'] = $explainContentArrs['szMetaTitle'];
//							$data['szMetaDescription'] = $explainContentArrs['szMetaDescription'];
//							$data['szMetaKeywords'] = $explainContentArrs['szMetaKeywords'];
//							$data['dtCreatedOn'] = $explainContentArrs['dtCreatedOn'];
//							$data['dtUpdatedOn'] = $explainContentArrs['dtUpdatedOn'];
//							$xml_content = $this->add_xml_item($data);
//							$xml_body_text .= $xml_content ;
//						}
//					}
//					*/
//				}
//			}
//			
//			
//			$explainPageAry = $kExplain->selectPageDetailsUserEnd($iLanguage,__TRANSTPORTPEDIA__);
//			if(!empty($explainPageAry))
//			{
//				foreach($explainPageAry as $explainPageArys)
//				{
//                                    $explainContentArr=array();
//                                    $explainContentArr=$kExplain->getAllExplainContainDataByIdExplain($explainPageArys['id']);
//
//                                    if($explainPageArys['iLanguage']==2)
//                                    {
//                                        $szUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/da/".__TRANSTPORTPEDIA_URL_DA__."/".$explainPageArys['szLink'] ;
//                                    }
//                                    else
//                                    {
//                                        $szUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".__TRANSTPORTPEDIA_URL__."/".$explainPageArys['szLink'] ;
//                                    }
//
//                                    $data = array();
//                                    $data['szLocationUrl'] = $szUrl;
//                                    $data['dtCreatedOn'] = $explainPageArys['dtCreatedOn'];
//                                    $data['dtUpdatedOn'] = $explainPageArys['dtUpdatedOn'];
//                                    $xml_content = $this->add_xml_item($data);
//                                    $xml_body_text .= $xml_content ;
//				 	
//				 	/*
//					if(!empty($explainContentArr))
//					{
//						foreach($explainContentArr as $explainContentArrs)
//						{
//						
//							if($explainPageArys['iLanguage']==2)
//							{
//								$szUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/da/".__TRANSTPORTPEDIA_URL_DA__."/".$explainPageArys['szLink']."/".$explainContentArrs['szUrl']."/";
//							}
//							else
//							{
//								$szUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".__TRANSTPORTPEDIA_URL__."/".$explainPageArys['szLink']."/".$explainContentArrs['szUrl']."/";
//							}
//						
//							$data = array();
//							$data['szLocationUrl'] = $szUrl;
//							$data['szMetaTitle'] = $explainContentArrs['szMetaTitle'];
//							$data['szMetaDescription'] = $explainContentArrs['szMetaDescription'];
//							$data['szMetaKeywords'] = $explainContentArrs['szMetaKeywords'];
//							$data['dtCreatedOn'] = $explainContentArrs['dtCreatedOn'];
//							$data['dtUpdatedOn'] = $explainContentArrs['dtUpdatedOn'];
//							$xml_content = $this->add_xml_item($data);
//							$xml_body_text .= $xml_content ;
//						}
//					}
//					*/
//				}
//			}
		/*
		//getting all active explainpages
		$explainPageAry = $kExplain->selectPageDetailsUserEnd();
		
		if(!empty($explainPageAry))
		{
			foreach($explainPageAry as $explainPageArys)
			{
				if($explainPageArys['iLanguage']==2)
				{
					$szUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/da/explain/".$explainPageArys['szLink']."/";
				}
				else
				{
					$szUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/explain/".$explainPageArys['szLink']."/";
				}
				
				$data = array();
				$data['szLocationUrl'] = $szUrl;
				$data['szMetaTitle'] = $explainPageArys['szMetaTitle'];
				$data['szMetaDescription'] = $explainPageArys['szMetaDescription'];
				$data['szMetaKeywords'] = $explainPageArys['szMetaKeyWord'];
				$data['dtCreatedOn'] = $explainPageArys['dtCreatedOn'];
				$data['dtUpdatedOn'] = $explainPageArys['dtUpdatedOn'];
				
				$xml_content = $this->add_xml_item($data);
				$xml_body_text .= $xml_content ;
			}
		}*/
		
		//getting all active blog pages
		$blogPageAry = $kExplain->selectBlogDetailsActive();
		
		if(!empty($blogPageAry))
		{
                    foreach($blogPageAry as $blogPageArys)
                    {
                        $kConfig =new cConfig();
                        $langArr=$kConfig->getLanguageDetails('',$blogPageArys['iLanguage']);
                        if(!empty($langArr) && $blogPageArys['iLanguage']!=__LANGUAGE_ID_ENGLISH__)
                        {
                            $szLanguageCode=$langArr[0]['szLanguageCode'];
                            $szUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".$szLanguageCode."/learn/".$blogPageArys['szLink'] ;
                        }
                        else
                        {
                            $szUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/learn/".$blogPageArys['szLink'];
                        }

                        $data = array();
                        $data['szLocationUrl'] = $szUrl;
                        $data['szMetaTitle'] = $blogPageArys['szMetaTitle'];
                        $data['szMetaDescription'] = $blogPageArys['szMetaDescription'];
                        $data['szMetaKeywords'] = $blogPageArys['szMetaKeyWord'];
                        $data['dtCreatedOn'] = $blogPageArys['dtCreatedOn'];
                        $data['dtUpdatedOn'] = $blogPageArys['dtUpdatedOn'];

                        $xml_content = $this->add_xml_item($data);
                        $xml_body_text .= $xml_content ;
                    }
		}
		
		//getting sitemap video
		$sitemapVideoAry = array();
		$sitemapVideoAry = $this->getSitemapVideo();
		
		/*
		 * .'<lastmod>'.date('d/m/Y H:i',strtotime($sitemapVideoAry['dtUpdatedOn'])).'</lastmod>'
		 * */
		
		if(!empty($sitemapVideoAry))
		{
                    $szUrl = __SITE_MAP_PAGE_URL__;
                    $xml_content = $sitemapVideoAry['szVideoCode'] ;
                    $xml_body_text .= $xml_content ;
		}
		
		$xml_footer_text = '</urlset>';
		$xml_file_ontent = $xml_header_text." ".$xml_body_text." ".$xml_footer_text;
		$xml_file_path = __APP_PATH__.'/sitemap/transportecaSitemap.xml';
				
		$file_pointer = fopen($xml_file_path,'w'); //opening file in write mod
		file_put_contents($xml_file_path, $xml_file_ontent);
		fclose($file_pointer);
		
		if(file_exists($xml_file_path))
		{
			$destination_file_path =  __APP_PATH__.'/sitemap.xml';
			
			if(file_exists($xml_file_path))
			{
				@unlink($destination_file_path);
			}
			
			@copy($xml_file_path, $destination_file_path);
		}
		
		return true;
	}
	
	function addSEOTracker($seoTrackerAry)
	{
		if(!empty($seoTrackerAry))
		{				
			$query = "
					INSERT INTO
						".__DBC_SCHEMATA_SEO_PAGE_TRACKER__."
					(
						szReferalUrl,
						idSeoPage,
						szSeoUrl,
						dtCreatedOn,
						iActive
					)
					VALUES
					(
						'".mysql_escape_custom(trim($seoTrackerAry['szReferalUrl']))."',
						'".(int)mysql_escape_custom(trim($seoTrackerAry['idSeoPage']))."',
						'".mysql_escape_custom(trim($seoTrackerAry['szSeoUrl']))."',
					    now(),
					    '1'
					)
				";
			//echo $query;
			//die;
			if( ( $result = $this->exeSQL( $query ) ) )
			{
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function getallStaticPages()
	{
		//following array contains title,description,and url of all static pages of site.
		$staticPageAry = array();
		
		define_constants('english',true); 
		/*
		//requirement page
		$staticPageAry[0]['szLocationUrl'] = __REQUIREMENT_PAGE_URL__."/" ;
		$staticPageAry[0]['szMetaTitle'] = __REQUIREMENT_PAGE_META_TITLE__;
		$staticPageAry[0]['szMetaDescription'] = __REQUIREMENT_PAGE_META_DESCRIPTION__;
		$staticPageAry[0]['szMetaKeywords'] = __REQUIREMENT_PAGE_META_KEYWORDS__;

		//Create Account page
		$staticPageAry[2]['szLocationUrl'] = __CREATE_ACCOUNT_PAGE_URL__ ;
		$staticPageAry[2]['szMetaTitle'] = __CREATE_ACCOUNT_PAGE_META_TITLE__;
		$staticPageAry[2]['szMetaDescription'] = __CREATE_ACCOUNT_PAGE_META_DESCRIPTION__;
		$staticPageAry[2]['szMetaKeywords'] = __CREATE_ACCOUNT_PAGE_META_KEYWORDS__;

		//Starting new trades page
		$staticPageAry[4]['szLocationUrl'] = __STARTING_NEW_TRADE_PAGE_URL__."/";
		$staticPageAry[4]['szMetaTitle'] = __STARTING_NEW_TRADE_PAGE_META_TITLE__."/";
		$staticPageAry[4]['szMetaDescription'] = __STARTING_NEW_TRADE_PAGE_META_DESCRIPTION__;
		$staticPageAry[4]['szMetaKeywords'] = __STARTING_NEW_TRADE_PAGE_META_KEYWORDS__;

		//FAQ page
		$staticPageAry[11]['szLocationUrl'] = __FAQ_PAGE_URL__ ;
		$staticPageAry[11]['szMetaTitle'] = __FAQ_PAGE_META_TITLE__;
		$staticPageAry[11]['szMetaDescription'] = __FAQ_PAGE_META_DESCRIPTION__;
		$staticPageAry[11]['szMetaKeywords'] = __FAQ_PAGE_META_KEYWORDS__;

		//Our company page
		$staticPageAry[3]['szLocationUrl'] = __OUR_COMPANY_PAGE_URL__."/";
		$staticPageAry[3]['szMetaTitle'] = __OUR_COMPANY_PAGE_META_TITLE__;
		$staticPageAry[3]['szMetaDescription'] = __OUR_COMPANY_PAGE_META_DESCRIPTION__;
		$staticPageAry[3]['szMetaKeywords'] = __OUR_COMPANY_PAGE_META_KEYWORDS__;

		//Instant search page
		$staticPageAry[1]['szLocationUrl'] = __LANDING_PAGE_URL__."/";
		$staticPageAry[1]['szMetaTitle'] = __SEARCH_PAGE_META_TITLE__;
		$staticPageAry[1]['szMetaDescription'] = __SEARCH_PAGE_META_DESCRIPTION__;
		$staticPageAry[1]['szMetaKeywords'] = __SEARCH_PAGE_META_KEYWORDS__;

		*/ 
		//How it work page
//		$staticPageAry[5]['szLocationUrl'] = __HOW_IT_WORK_PAGE_URL__ ;
//		$staticPageAry[5]['szMetaTitle'] = __HOW_IT_WORK_PAGE_META_TITLE__;
//		$staticPageAry[5]['szMetaDescription'] = __HOW_IT_WORK_PAGE_META_DESCRIPTION__;
//		$staticPageAry[5]['szMetaKeywords'] = __HOW_IT_WORK_PAGE_META_KEYWORDS__;
		
		//Forwarder signup page
		$staticPageAry[6]['szLocationUrl'] = __FORWARDER_SIGNUP_PAGE_URL__  ;
		$staticPageAry[6]['szMetaTitle'] = __FORWARDER_SIGNUP_PAGE_META_TITLE__;
		$staticPageAry[6]['szMetaDescription'] = __FORWARDER_SIGNUP_PAGE_META_DESCRIPTION__;
		$staticPageAry[6]['szMetaKeywords'] = __FORWARDER_SIGNUP_PAGE_META_KEYWORDS__;
		
		//Partner program page
		$staticPageAry[7]['szLocationUrl'] = __PARTNER_PROGRAM_PAGE_URL__ ;
		$staticPageAry[7]['szMetaTitle'] = __PARTNER_PROGRAM_PAGE_META_TITLE__;
		$staticPageAry[7]['szMetaDescription'] = __PARTNER_PROGRAM_PAGE_META_DESCRIPTION__;
		$staticPageAry[7]['szMetaKeywords'] = __PARTNER_PROGRAM_PAGE_META_KEYWORDS__;
		
		//Rss feed news page
		$staticPageAry[8]['szLocationUrl'] = __RSS_FEED_PAGE_URL__ ;
		$staticPageAry[8]['szMetaTitle'] = __RSS_FEED_PAGE_META_TITLE__;
		$staticPageAry[8]['szMetaDescription'] = __RSS_FEED_PAGE_META_DESCRIPTION__;
		$staticPageAry[8]['szMetaKeywords'] = __RSS_FEED_PAGE_META_KEYWORDS__;
		
		//Terms & Condition page
		/*$staticPageAry[9]['szLocationUrl'] = __TERMS_CONDITION_PAGE_URL__  ;
		$staticPageAry[9]['szMetaTitle'] = __TERMS_CONDITION_PAGE_META_TITLE__;
		$staticPageAry[9]['szMetaDescription'] = __TERMS_CONDITION_PAGE_META_DESCRIPTION__;
		$staticPageAry[9]['szMetaKeywords'] = __TERMS_CONDITION_PAGE_META_KEYWORDS__;*/
		
		//Privacy notice page
		/*$staticPageAry[10]['szLocationUrl'] = __PRIVACY_NOTICE_PAGE_URL__ ;
		$staticPageAry[10]['szMetaTitle'] = __PRIVACY_NOTICE_PAGE_META_TITLE__;
		$staticPageAry[10]['szMetaDescription'] = __PRIVACY_NOTICE_PAGE_META_DESCRIPTION__;
		$staticPageAry[10]['szMetaKeywords'] = __PRIVACY_NOTICE_PAGE_META_KEYWORDS__;*/
		 
		return $staticPageAry ;	
	} 
        
        function getLanguageNameByCode($szLanguageCode,$idLanguage)
        {
            if(!empty($szLanguageCode))
            {
                $kConfig = new cConfig();
                $languageArr=$kConfig->getLanguageDetails($szLanguageCode,'','','','','','',false);

                $szLanguage = strtolower($languageArr[0]['szName']);
            }
            else if($idLanguage>0)
            {
                $kConfig = new cConfig();
                $languageArr=$kConfig->getLanguageDetails(false,$idLanguage,'','','','','',false);

                $szLanguage = strtolower($languageArr[0]['szName']);
            }
            if(empty($szLanguage))
            {
                $szLanguage = 'english';
            }
            return $szLanguage; 
        }
        
        function getLanguageName($requestInputAry)
        {
            if(!empty($requestInputAry['szLanguage']))
            {
                $szLanguage = $this->getLanguageNameByCode($requestInputAry['szLanguage']);
                return $szLanguage;
            }
            else
            {
                $szLanguage = $this->getLanguageNameByCode(false,$requestInputAry['iLanguage']);
                return $szLanguage;
            }
        }
	function set_szUrl($value, $require_feild_flag=true)
	{	
		$this->szUrl = $this->validateInput($value,__VLD_CASE_ANYTHING__,'szUrl',t( $this->t_base."fields/szUrl" ),false, false ,$require_feild_flag );
	}
	function set_searchField( $value )
	{   
		$this->searchField = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "searchField", t($this->t_base.'fields/searchField')." ".t($this->t_base.'fields/searchField'), false, false, false );
	}
	function set_idExplain( $value )
	{   
		$this->idExplain = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idExplain", t($this->t_base.'fields/idExplain')." ".t($this->t_base.'fields/idExplain'), false, false, false );
	}
	function set_id( $value )
	{   
		$this->id = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "id", t($this->t_base.'fields/id'), false, false, true );
	}
	function set_idAdmin( $value )
	{   
		$this->idAdmin = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idAdmin", t($this->t_base.'fields/idAdmin'), false, false, true );
	}
	function set_szHeading( $value )
	{
		$this->szHeading = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szHeading", t($this->t_base.'fields/heading'), false, 255, true );
	}
	function set_szDescription( $value )
	{
		$this->szDescription = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDescription", t($this->t_base.'fields/description'), false, false, true );
	}
	function set_iOrderBy( $value )
	{
		$this->iOrderBy = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iOrderBy", t($this->t_base.'fields/iOrderBy'), false, false, true );
	}
	function set_szComment( $value )
	{
		$this->szComment = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szComment", t($this->t_base.'fields/szComment'), false, false, true );
	}
	function set_szPageHeading( $value )
	{
		$this->szPageHeading = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPageHeading", t($this->t_base.'fields/szPageHeading'), false, false, true );
	}
	function set_szLinkTitle( $value,$require_feild_flag=true  )
	{
		$this->szLinkTitle = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLinkTitle", t($this->t_base.'fields/szLinkTitle'), false, false, $require_feild_flag );
	}
	
	function set_szLink( $value, $require_feild_flag=true )
	{
		$this->szLink = $this->validateInput( $value, __VLD_CASE_URI__, "szLink", t($this->t_base.'fields/szLink'), false, false, $require_feild_flag );
	}	
	
	function set_szMetaKeywords( $value, $require_feild_flag=true )
	{
		$this->szMetaKeywords = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szMetaKeywords", t($this->t_base.'fields/szMetaKeywords'), false, false, $require_feild_flag );
	}
	function set_szMetaTitle( $value, $require_feild_flag=true)
	{
		$this->szMetaTitle = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szMetaTitle", t($this->t_base.'fields/szMetaTitle'), false, false, $require_feild_flag );
	}
	function set_szMetaKeyword( $value, $require_feild_flag=true )
	{
		$this->szMetaKeyword = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szMetaKeyword", t($this->t_base.'fields/szMetaKeyword'), false, false, $require_feild_flag );
	}
	function set_szMetaDescription( $value, $require_feild_flag=true )
	{
		$this->szMetaDescription = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szMetaDescription", t($this->t_base.'fields/szMetaDescription'), false, false, $require_feild_flag );
	}
	function set_iActive( $value )
	{
		$this->iActive = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iActive", t($this->t_base.'fields/iActive'), false, false, true );
	}
	function set_szContent( $value, $require_feild_flag=true )
	{
		$this->szContent = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDescription", t($this->t_base.'fields/description'), false, false, $require_feild_flag );
	}
	function set_szVideoCode( $value )
	{
		$this->szVideoCode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szVideoCode", t($this->t_base.'fields/video_code'), false, false, true );
	}
	function set_iLanguage( $value )
	{
            $this->iLanguage = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iLanguage", t($this->t_base.'fields/iLanguage'), false, false, true );
	}
	
 	function set_iPublished( $value )
	{
            $this->iPublished = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iPublished", t($this->t_base.'fields/Published'), false, false, true );
	}
        
        function set_iAvailableInLeftMenu( $value )
	{
            $this->iAvailableInLeftMenu = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iAvailableInLeftMenu", t($this->t_base.'fields/iAvailableInLeftMenu'), false, false, false );
	}
        function set_szFromSelection( $value )
	{
            $this->szFromSelection = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szFromSelection", t($this->t_base.'fields/szFromSelection'), false, false, false );
	}
        function set_szToSelection( $value )
	{
            $this->szToSelection = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szToSelection", t($this->t_base.'fields/szToSelection'), false, false, false );
	}
        function set_iFromIPLocation( $value )
	{
            $this->iFromIPLocation = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iFromIPLocation", t($this->t_base.'fields/iFromIPLocation'), false, false, false );
	}
        function set_iToIPLocation( $value )
	{
            $this->iToIPLocation = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iToIPLocation", t($this->t_base.'fields/iToIPLocation'), false, false, false );
	} 
        function set_fCargoVolume( $value )
	{
            $this->fCargoVolume = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fCargoVolume", t($this->t_base.'fields/fCargoVolume'), false, false, false );
	}
        function set_fCargoWeight( $value )
	{
            $this->fCargoWeight = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fCargoWeight", t($this->t_base.'fields/fCargoWeight'), false, false, false );
	} 
}	
?>