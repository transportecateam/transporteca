<?php 
/**
 * The Imap PHP class provides a wrapper for commonly used PHP IMAP functions.
 *     
 * @author Ajay
 * @package Transporteca
 */

session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" ); 
require_once( __APP_PATH__ . "/inc/functions.php" ); 

class cImap {

  private $host;
  private $user;
  private $pass;
  private $port;
  private $folder;
  private $ssl;

  private $baseAddress;
  private $address;
  public $mailbox;
  public $messageNumber;
  public $iInlineAttachments=false;
  public $charSetTypeValue;

  /**
   * Called when the Imap object is created.
   *
   * Sample of a complete address: {imap.gmail.com:993/imap/ssl}INBOX
   *
   * @param $host (string)
   *   The IMAP hostname. Example: imap.gmail.com
   * @param $port (int)
   *   Example: 933
   * @param $ssl (bool)
   *   TRUE to use SSL, FALSE for no SSL.
   * @param $folder (string)
   *   IMAP Folder to open.
   * @param $user (string)
   *   Username used for connection. Gmail uses full username@gmail.com, but
   *   many providers simply use username.
   * @param $pass (string)
   *   Account password.
   *
   * @return (empty)
   */
    public function __construct($ssl = true, $folder = 'INBOX') 
    { 
          $kWHSSearch=new cWHSSearch();
          $szGoogleUserName = $kWHSSearch->getManageMentVariableByDescription('__CUSTOMER_SUPPORT_GOOGLE_ACCOUNT__'); 
          $szGooglePassword = $kWHSSearch->getManageMentVariableByDescription('__CUSTOMER_SUPPORT_GOOGLE_ACCOUNT_PASSWORD__'); 

          $this->host = __GMAIL_HOST_NAME__;
          $this->user = $szGoogleUserName;
          $this->pass = $szGooglePassword; 
          $this->port = __GMAIL_HOST_PORT__;
          $this->folder = $folder;
          $this->ssl = $ssl;

          //$this->user = __GMAIL_USER_NAME__;
          //$this->pass = __GMAIL_PASSWORD__;

          $this->changeLoginInfo($ssl, $folder);
    }

  /**
   * Change IMAP folders and reconnect to the server.
   *
   * @param $folderName
   *   The name of the folder to change to.
   *
   * @return (empty)
   */
  public function changeFolder($folderName) {
    if ($this->ssl) {
      $address = '{' . $this->host . ':' . $this->port . '/imap/ssl}' . $folderName;
    } else {
      $address = '{' . $this->host . ':' . $this->port . '/imap}' . $folderName;
    }

    $this->address = $address;
    $this->reconnect();
  }

  /**
   * Log into an IMAP server.
   *
   * This method is called on the initialization of the class (see
   * __construct()), and whenever you need to log into a different account.
   *
   * Please see __construct() for parameter info.
   *
   * @return (empty)
   *
   * @throws Exception when IMAP can't connect.
   */
  public function changeLoginInfo($ssl, $folder) {
      $host = $this->host;
      $user = $this->user;
      $pass = $this->pass;
      $port = $this->port;
      
    if ($ssl) {
      $baseAddress = '{' . $host . ':' . $port . '/imap/ssl}';
      $address = $baseAddress . $folder;
    } else {
      $baseAddress = '{' . $host . ':' . $port . '/imap}';
      $address = $baseAddress . $folder;
    }

    // Set the new address and the base address.
    $this->baseAddress = $baseAddress;
    $this->address = $address;

    // Open new IMAP connection
    if ($mailbox = imap_open($address, $user, $pass)) {
      $this->mailbox = $mailbox;
    } else {
      throw new Exception("Error: " . imap_last_error());
    }
  }

  /**
   * Returns an associative array with detailed information about a given
   * message.
   *
   * @param $messageId (int)
   *   Message id.
   *
   * @return Associative array with keys (strings unless otherwise noted):
   *   raw_header
   *   to
   *   from
   *   cc
   *   bcc
   *   reply_to
   *   sender
   *   date_sent
   *   subject
   *   deleted (bool)
   *   answered (bool)
   *   draft (bool)
   *   body
   *   original_encoding
   *   size (int)
   *   auto_response (bool)
   *
   * @throws Exception when message with given id can't be found.
   */
    public function getMessage($messageId) 
    {
        $this->tickle();

        // Get message details.
        $details = imap_headerinfo($this->mailbox, $messageId);
        $detailsArr=  toArray($details);
        if (!empty($detailsArr))
        {
            // Get the raw headers.
            $raw_header = imap_fetchheader($this->mailbox, $messageId);
            
            
             
            // Detect whether the message is an autoresponse.
            $autoresponse = $this->detectAutoresponder($raw_header);
            
            
            $structure = @imap_fetchstructure($this->mailbox, $messageId);
            $structureArr=  toArray($structure);
            // Get some basic variables.
            $deleted = ($detailsArr['Deleted'] == 'D');
            $answered = ($detailsArr['Answered'] == 'A');
            $draft = ($detailsArr['Draft'] == 'X');
            
            
            // Get the message body.
            $body = $this->getEmailBody($messageId); 

            // Get the message body encoding.
            $encoding = $this->getEncodingType($messageId);
           

            $mimeHeaderSubjectAry = array();
            $mimeHeaderSubjectAry = imap_mime_header_decode($detailsArr['subject']);
            
            if(!empty($mimeHeaderSubjectAry))
            {
                $subject = '';
                foreach($mimeHeaderSubjectAry as $mimeHeaderSubjectArys)
                {
                    if(!empty($mimeHeaderSubjectArys->text))
                    {
                        $subject .= $mimeHeaderSubjectArys->text;
                    } 
                }
            } 
           
            //$body = quoted_printable_decode($body);
            // Decode body into plaintext (8bit, 7bit, and binary are exempt).
            if ($encoding == 'BASE64') {
              $body = $this->decodeBase64($body);
            }
            else if ($encoding == 'QUOTED-PRINTABLE') {
              $body = $this->decodeQuotedPrintable($body);
            }
            else if ($encoding == '8BIT') {
              $body = $this->decode8Bit($body);
            }
            else if ($encoding == '7BIT') {
              $body = $this->decode7Bit($body);
            }
            
            
            if($structureArr['encoding']==4 && strtolower($structureArr['parameters'][0]['value'])!='utf-8')
            {
                $body = utf8_encode($body);
            }
            
            $szFromEmailAddress = '';
            if(!empty($detailsArr['from']))
            {
                foreach($detailsArr['from'] as $fromAddressAry)
                { 
                    $szFromEmailAddress = imap_utf8($fromAddressAry['mailbox'])."@".imap_utf8($fromAddressAry['host']); 
                    $szFromUserName = imap_utf8($fromAddressAry['personal']);
                }  
            }  
            $szToEmailAddress = '';
            if(!empty($detailsArr['to']))
            {
                foreach($detailsArr['to'] as $toAddressAry)
                { 
                    $szToEmailAddress = imap_utf8($toAddressAry['mailbox'])."@".imap_utf8($toAddressAry['host']);
                    $szToUserName = imap_utf8($toAddressAry['personal']);
                }  
            }
            
            $szCCEmailAddress = array();
            if(!empty($detailsArr['cc']))
            {
                foreach($detailsArr['cc'] as $toAddressAry)
                { 
                    $szCCEmailAddress[] = imap_utf8($toAddressAry['mailbox'])."@".imap_utf8($toAddressAry['host']);
                    $szCCUserName[] = imap_utf8($toAddressAry['personal']);
                }
            }
            
            $szBCCEmailAddress = '';
            if(!empty($detailsArr['bcc']))
            {
                foreach($detailsArr['bcc'] as $toAddressAry)
                { 
                    $szBCCEmailAddress = imap_utf8($toAddressAry['mailbox'])."@".imap_utf8($toAddressAry['host']);
                    $szBCCUserName = imap_utf8($toAddressAry['personal']);
                }
            }   
            // Build the message.
            $message = array(
              'raw_header' => $raw_header,
              'uid' => $messageId,
              'message_id' => $detailsArr['message_id'],
              'to' => $detailsArr['toaddress'],
              'from' => $detailsArr['fromaddress'],
              'fromEmail' => $szFromEmailAddress,
              'szFromUserName' => $szFromUserName,
              'szToEmailAddress' => $szToEmailAddress,
              'szToUserName' => $szToUserName,
              'cc' => $szCCEmailAddress,
              'bcc' => isset($szBCCEmailAddress) ? $szBCCEmailAddress : '',
              'reply_to' => isset($detailsArr['reply_toaddress']) ? $detailsArr['reply_toaddress'] : '',
              'sender' => $detailsArr['senderaddress'],
              'date_sent' => $detailsArr['date'],
              'subject' => $subject,
              'deleted' => $deleted,
              'answered' => $answered,
              'draft' => $draft,
              'body' => $body,
              'original_encoding' => $encoding,
              'size' => $detailsArr['Size'],
              'auto_response' => $autoresponse,
            );   
        }
        else 
        {
            throw new Exception("Message could not be found: " . imap_last_error());
        } 
        return $message;
  }
  
  function getEmailBody($messageId)
  {
    /*
    * (empty) - Entire message
     0 - Message header
     1 - MULTIPART/ALTERNATIVE
     1.1 - TEXT/PLAIN
     1.2 - TEXT/HTML
     2 - file.ext
    */
      
    $szEmailBody = imap_fetchbody($this->mailbox, $messageId, 1.2); // Looking for TEXT/HTML
    if (empty($szEmailBody)) 
    {  
        $szEmailBody = imap_fetchbody($this->mailbox, $messageId,1); // Looking for MULTIPART/ALTERNATIVE 
    }
    if (empty($szEmailBody)) 
    {   
        $szEmailBody = imap_fetchbody($this->mailbox, $messageId,2); 
    }
    if (empty($szEmailBody)) 
    {  
        $szEmailBody = imap_fetchbody($this->mailbox, $messageId,2.2); 
    }  
    return $szEmailBody;
  }

  /**
   * Deletes an email matching the specified $messageId.
   *
   * @param $messageId (int)
   *   Message id.
   * @param $immediate (bool)
   *   Set TRUE if message should be deleted immediately. Otherwise, message
   *   will not be deleted until disconnect() is called. Normally, this is a
   *   bad idea, as other message ids will change if a message is deleted.
   *
   * @return (empty)
   *
   * @throws Exception when message can't be deleted.
   */
    public function deleteMessage($messageId, $immediate = FALSE) 
    {
        $this->tickle();

        // Mark message for deletion.
        if (!imap_delete($this->mailbox, $messageId)) {
          throw new Exception("Message could not be deleted: " . imap_last_error());
        }

        // Immediately delete the message if $immediate is TRUE.
        if ($immediate) {
          imap_expunge($this->mailbox);
        }
    } 
    public function getAttachments($messageId)
    {
        if(!empty($messageId))
        { 
            $body = imap_fetchstructure($this->mailbox, $messageId); 
            $attachments = '';
            $partArr=  toArray($body->parts);
           
            $att = count($partArr);
            $attachmentAry = array();
            
            if($att >=2)
            {
                for($a=0;$a<$att;$a++)
                {
                    
                    $attachMentListArr=array();
                    if($partArr[$a]['disposition'] == 'ATTACHMENT')
                    {
                        
                        $attachMentListArr=$partArr[$a]['dparameters'];
                        if(!empty($attachMentListArr))
                        {
                            foreach($attachMentListArr as $attachMentListArrs)
                            {
                                if(strtolower($attachMentListArrs['attribute'])=='filename')
                                {
                                    //echo $partArr[$a]['disposition']."dispositionAtt<br /><br />";
                                    $file = imap_base64(imap_fetchbody($this->mailbox, $messageId, $a+1)); 
                                    $szAttachmetFolderPath = __APP_PATH_GMAIL_ATTACHMENT__."/".$messageId;
                                    if(!file_exists($szAttachmetFolderPath))
                                    {
                                        $this->makeDirectory($szAttachmetFolderPath);   
                                    }
                                    $attachments .= $attachMentListArrs['value'].'[#]'.',';
                                    $szAttachmentName = $attachMentListArrs['value'];
                                    $szAttachmentNameArr=explode(".",$szAttachmentName);
                                    $ImageExt = substr($szAttachmentName, strrpos($szAttachmentName, '.'));
                                    if($ImageExt!='' && strtolower($szAttachmentName)!='utf-8' && $szAttachmentNameArr[1]!='')
                                    {
                                        //echo $szAttachmentName."szAttachmentName<br /><br />";
                                        $pos = strpos(strtolower($szAttachmentName), "=?utf-8");
                                        if ($pos !== false) {
                                            $ImageExt = substr($szAttachmentName, strrpos($szAttachmentName, '.'));
                                            $ImageExt = str_replace("?=", "", $ImageExt);
                                            $szAttachmentName = "Attachment-".$a."".$ImageExt;
                                        }
                                        $attachmentAry[] = $szAttachmentName;

                                        $szFullPath = $szAttachmetFolderPath."/".$szAttachmentName;
                                        if(file_exists($szFullPath) && filesize($szFullPath)==0)
                                        {
                                            @unlink($szFullPath);
                                        }  
                                        $this->saveAttachments($szFullPath,$file); 
                                    }
                                }
                            }
                        }
                    } 
                    else if(strtolower($partArr[$a]['parameters'][0]['attribute'])=='name')
                    {    
                        $szAttachmetFolderPath = __APP_PATH_GMAIL_ATTACHMENT__."/".$messageId;
                        if(!file_exists($szAttachmetFolderPath))
                        {
                            $this->makeDirectory($szAttachmetFolderPath);   
                        }
                        $attachments .= $partArr[$a]['dparameters'][0]['value'].'[#]'.',';
                        $szAttachmentName = $partArr[$a]['parameters'][0]['value'];
                        
                        //echo $iTotalAttachment."iTotalAttachment<br /><br />";
                        $szAttachmentNameArr=explode(".",$szAttachmentName);
                        $ImageExt = substr($szAttachmentName, strrpos($szAttachmentName, '.'));
                        if($ImageExt!='' && strtolower($szAttachmentName)!='utf-8' && $szAttachmentNameArr[1]!='')
                        {
                            $pos = strpos(strtolower($szAttachmentName), "=?utf-8");
                            if ($pos !== false) {
                                $ImageExt = substr($szAttachmentName, strrpos($szAttachmentName, '.'));
                                $ImageExt = str_replace("?=", "", $ImageExt);
                                $szAttachmentName = "Attachment-".$a."".$ImageExt;
                            }
                            //echo $szAttachmentName."szAttachmentName2<br /><br />";
                            $message = imap_fetchbody($this->mailbox, $messageId,$a+1); 
                            //echo $message."<br /><br />";
                            $decoded_data = base64_decode($message);  
                            $attachmentAry[] = $szAttachmentName; 
                            $szFileFullPath = $szAttachmetFolderPath."/".$szAttachmentName;

                            if(file_exists($szFileFullPath) && filesize($szFileFullPath)==0)
                            { 
                                @unlink($szFileFullPath);
                            }
                            $this->saveAttachments($szFileFullPath,$decoded_data);
                        }
                    }
                    else if(!empty($partArr[$a]['parts'][$a]['parameters'][0]['value']))
                    {    
                        //print_r($partArr[$a]['parts'][$a]['parameters']);
                        //echo $partArr[$a]['disposition']."dispositionOther<br /><br />"; 
                        $szAttachmetFolderPath = __APP_PATH_GMAIL_ATTACHMENT__."/".$messageId;
                        if(!file_exists($szAttachmetFolderPath))
                        {
                            $this->makeDirectory($szAttachmetFolderPath);   
                        }
                        $attachments .= $partArr[$a]['dparameters'][0]['value'].'[#]'.',';
                        $szAttachmentName = $partArr[$a]['parts'][$a]['parameters'][0]['value'];
                        
                        //echo $iTotalAttachment."iTotalAttachment<br /><br />";
                        $szAttachmentNameArr=explode(".",$szAttachmentName);
                        $ImageExt = substr($szAttachmentName, strrpos($szAttachmentName, '.'));
                        if($ImageExt!='' && strtolower($szAttachmentName)!='utf-8' && $szAttachmentNameArr[1]!='')
                        {
                            $pos = strpos(strtolower($szAttachmentName), "=?utf-8");
                            if ($pos !== false) {
                                $ImageExt = substr($szAttachmentName, strrpos($szAttachmentName, '.'));
                                $ImageExt = str_replace("?=", "", $ImageExt);
                                $szAttachmentName = "Attachment-".$a."".$ImageExt;
                            }
                            //echo $szAttachmentName."szAttachmentName2<br /><br />";
                            $message = imap_fetchbody($this->mailbox, $messageId,2.2); 
                            $decoded_data = base64_decode($message);  
                            $attachmentAry[] = $szAttachmentName; 
                            $szFileFullPath = $szAttachmetFolderPath."/".$szAttachmentName;

                            if(file_exists($szFileFullPath) && filesize($szFileFullPath)==0)
                            { 
                                @unlink($szFileFullPath);
                            }
                            $this->saveAttachments($szFileFullPath,$decoded_data);
                        }
                    }   
                }   
            }   
            
            $this->iTotalAttachment=count($attachmentAry);
            $this->attachmentAry = array();
            $this->getInlineAttachments($messageId); 
            
            if(!empty($this->attachmentAry) && !empty($attachmentAry))
            {
                $attachmentAry = array_merge($attachmentAry,$this->attachmentAry);
            }
            else if(!empty($this->attachmentAry))
            {
                $attachmentAry = $this->attachmentAry;
            }  
            if(!empty($attachmentAry))
            {
                $attachmentAry = array_unique($attachmentAry);
            }
            return $attachmentAry;
        }
    }    
    
    function saveAttachments($szFilePath,$szDataString)
    {
        if(!empty($szFilePath) && !empty($szDataString))
        {  
            file_put_contents($szFilePath,$szDataString);
            if(file_exists($szFilePath))
            { 
                //Write permission 128, 16, 2 are for writable for owner, group and other respectively.
                chmod($szFilePath, fileperms($szFilePath) | 128 + 16 + 2);				
            }
        }
    }
    
    function makeDirectory($szFilePath)
    {
        if(!empty($szFilePath))
        {
            mkdir($szFilePath,'0777',true);   
        }
    }
    
    function getInlineAttachments($messageId)
    {
        $this->fetch($messageId); 
        if($this->iInlineAttachments==1)
        {
            /*
            // match inline images in html content
            preg_match_all('/src="cid:(.*)"/Uims', $this->bodyHTML, $matches);

            // if there are any matches, loop through them and save to filesystem, change the src property
            // of the image to an actual URL it can be viewed at
            if(count($matches))
            {
                // search and replace arrays will be used in str_replace function below
                $search = array();
                $replace = array(); 
                foreach($matches[1] as $match)
                {
                    // work out some unique filename for it and save to filesystem etc
                    $uniqueFilename = "UNIQUE_FILENAME.extension";
                    // change /path/to/images to actual path
                    file_put_contents("/path/to/images/$uniqueFilename", $this->attachments[$match]['data']);
                    $search[] = "src=\"cid:$match\"";
                    // change www.example.com etc to actual URL
                    $replace[] = "src=\"http://www.example.com/images/$uniqueFilename\"";
                }
                // now do the replacements
                $this->bodyHTML = str_replace($search, $replace, $this->bodyHTML);  
            }
            * 
            */
        } 
    }

  /**
   * Moves an email into the given mailbox.
   *
   * @param $messageId (int)
   *   Message id.
   * @param $folder (string)
   *   The name of the folder (mailbox) into which messages should be moved.
   *   $folder could either be the folder name or 'INBOX.foldername'.
   *
   * @return (bool)
   *   Returns TRUE on success, FALSE on failure.
   */
  public function moveMessage($messageId, $folder) {
    $messageRange = $messageId . ':' . $messageId;
    return imap_mail_move($this->mailbox, $messageRange, '{sslmail.webguyz.net:143/imap}Questionable');
  }

  /**
   * Returns an associative array with email subjects and message ids for all
   * messages in the active $folder.
   *
   * @return Associative array with message id as key and subject as value.
   */
  public function getMessageIds() {
    $this->tickle();

    // Fetch overview of mailbox.
    $number_messages = imap_num_msg($this->mailbox);
    if ($number_messages) {
      $overviews = imap_fetch_overview($this->mailbox, "1:" . imap_num_msg($this->mailbox), 0);
    }
    else {
      $overviews = array();
    }
    $messageArray = array();

    // Loop through message overviews, build message array.
    foreach($overviews as $overview) {
      $messageArray[$overview->msgno] = $overview->subject;
    }

    return $messageArray;
  }

  /**
   * Return an associative array containing the number of recent, unread, and
   * total messages.
   *
   * @return Associative array with keys:
   *   unread
   *   recent
   *   total
   */
  public function getCurrentMailboxInfo() {
    $this->tickle();

    // Get general mailbox information.
    $info = imap_status($this->mailbox, $this->address, SA_ALL);
    $mailInfo = array(
      'unread' => $info->unseen,
      'recent' => $info->recent,
      'total' => $info->messages,
    );
    return $mailInfo;
  }

  /**
   * Return an array of objects containing mailbox information.
   *
   * @return Array of mailbox names.
   */
  public function getMailboxInfo() {
    $this->tickle();

    // Get all mailbox information.
    $mailboxInfo = imap_getmailboxes($this->mailbox, $this->baseAddress, '*');
    $mailboxes = array();
    foreach ($mailboxInfo as $mailbox) {
      // Remove baseAddress from mailbox name.
      $mailboxes[] = array(
        'mailbox' => $mailbox->name,
        'name' => str_replace($this->baseAddress, '', $mailbox->name),
      );
    }

    return $mailboxes;
  }

  /**
   * Decodes Base64-encoded text.
   *
   * @param $text (string)
   *   Base64 encoded text to convert.
   *
   * @return (string)
   *   Decoded text.
   */
  public function decodeBase64($text) {
    $this->tickle();
    return imap_base64($text);
  }

  /**
   * Decodes quoted-printable text.
   *
   * @param $text (string)
   *   Quoted printable text to convert.
   *
   * @return (string)
   *   Decoded text.
   */
  public function decodeQuotedPrintable($text) {
    return quoted_printable_decode($text);
  }

  /**
   * Decodes 8-Bit text.
   *
   * @param $text (string)
   *   8-Bit text to convert.
   *
   * @return (string)
   *   Decoded text.
   */
  public function decode8Bit($text) {
    return quoted_printable_decode(imap_8bit($text));
  }

  /**
   * Decodes 7-Bit text.
   *
   * PHP seems to think that most emails are 7BIT-encoded, therefore this
   * decoding method assumes that text passed through may actually be base64-
   * encoded, quoted-printable encoded, or just plain text. Instead of passing
   * the email directly through a particular decoding function, this method
   * runs through a bunch of common encoding schemes to try to decode everything
   * and simply end up with something *resembling* plain text.
   *
   * Results are not guaranteed, but it's pretty good at what it does.
   *
   * @param $text (string)
   *   7-Bit text to convert.
   *
   * @return (string)
   *   Decoded text.
   */
  public function decode7Bit($text) {
    // If there are no spaces on the first line, assume that the body is
    // actually base64-encoded, and decode it.
    $lines = explode("\r\n", $text);
    $first_line_words = explode(' ', $lines[0]);
    if ($first_line_words[0] == $lines[0]) {
        if($this->is_base64_encoded($text))
        {
            $text = base64_decode($text);
        } 
    }

    // Manually convert common encoded characters into their UTF-8 equivalents.
    $characters = array(
      '=20' => ' ', // space.
      '=2C' => ',', // comma.
      '=E2=80=99' => "'", // single quote.
      '=0A' => "\r\n", // line break.
      '=0D' => "\r\n", // carriage return.
      '=A0' => ' ', // non-breaking space.
      '=B9' => '$sup1', // 1 superscript.
      '=C2=A0' => ' ', // non-breaking space.
      "=\r\n" => '', // joined line.
      '=E2=80=A6' => '&hellip;', // ellipsis.
      '=E2=80=A2' => '&bull;', // bullet.
      '=E2=80=93' => '&ndash;', // en dash.
      '=E2=80=94' => '&mdash;', // em dash.
    );

    // Loop through the encoded characters and replace any that are found.
    foreach ($characters as $key => $value) {
      $text = str_replace($key, $value, $text);
    }

    return $text;
  }

    function is_base64_encoded($data)
    {
        if (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $data)) {
           return TRUE;
        } else {
           return FALSE;
        }
    }
  /**
   * Strips quotes (older messages) from a message body.
   *
   * This function removes any lines that begin with a quote character (>).
   * Note that quotes in reply bodies will also be removed by this function,
   * so only use this function if you're okay with this behavior.
   *
   * @param $message (string)
   *   The message to be cleaned.
   * @param $plain_text_output (bool)
   *   Set to TRUE to also run the text through strip_tags() (helpful for
   *   cleaning up HTML emails).
   *
   * @return (string)
   *   Same as message passed in, but with all quoted text removed.
   *
   * @see http://stackoverflow.com/a/12611562/100134
   */
   public function cleanReplyEmail($message, $plain_text_output = FALSE) {
     // Strip markup if $plain_text_output is set.
     if ($plain_text_output) {
       $message = strip_tags($message);
     }

     // Remove quoted lines (lines that begin with '>').
     $message = preg_replace_callback("/(^\w.+:\n)?(^>.*(\n|$))+/mi", '', $message);

     // Remove lines beginning with 'On' and ending with 'wrote:' (matches
     // Mac OS X Mail, Gmail).
     $message = preg_replace_callback("/^(On).*(wrote:).*$/sm", '', $message);

     // Remove lines like '----- Original Message -----' (some other clients).
     // Also remove lines like '--- On ... wrote:' (some other clients).
     $message = preg_replace_callback("/^---.*$/mi", '', $message);

     // Remove lines like '____________' (some other clients).
     $message = preg_replace_callback("/^____________.*$/mi", '', $message);

     // Remove blocks of text with formats like:
     //   - 'From: Sent: To: Subject:'
     //   - 'From: To: Sent: Subject:'
     //   - 'From: Date: To: Reply-to: Subject:'
     $message = preg_replace_callback("/From:.*^(To:).*^(Subject:).*/sm", '', $message);

     // Remove any remaining whitespace.
     $message = trim($message);

     return $message;
   }

  /**
   * Takes in a string of email addresses and returns an array of addresses
   * as objects. For example, passing in 'John Doe <johndoe@sample.com>'
   * returns the following array:
   *
   *     Array (   
   *       [0] => stdClass Object (
   *         [mailbox] => johndoe
   *         [host] => sample.com
   *         [personal] => John Doe
   *       )
   *     )
   *
   * You can pass in a string with as many addresses as you'd like, and each
   * address will be parsed into a new object in the returned array.
   *
   * @param $addresses (string)
   *   String of one or more email addresses to be parsed.
   *
   * @return (array)
   *   Array of parsed email addresses, as objects.
   *
   * @see imap_rfc822_parse_adrlist().
   */
  public function parseAddresses($addresses) {
    return imap_rfc822_parse_adrlist($addresses, '#');
  }

  /**
   * Create an email address to RFC822 specifications.
   *
   * @param $username (string)
   *   Name before the @ sign in an email address (example: 'johndoe').
   * @param $host (string)
   *   Address after the @ sign in an email address (example: 'sample.com').
   * @param $name (string)
   *   Name of the entity (example: 'John Doe').
   *
   * @return (string) Email Address in the following format:
   *  'John Doe <johndoe@sample.com>'
   */
  public function createAddress($username, $host, $name) {
    return imap_rfc822_write_address($username, $host, $name);
  }

  /**
   * Returns structured information for a given message id.
   *
   * @param $messageId
   *   Message id for which structure will be returned.
   *
   * @return (object)
   *   See imap_fetchstructure() return values for details.
   *
   * @see imap_fetchstructure().
   */
  public function getStructure($messageId) {
    return imap_fetchstructure($this->mailbox, $messageId);
  }

  /**
   * Returns the primary body type for a given message id.
   *
   * @param $messageId (int)
   *   Message id.
   * @param $numeric (bool)
   *   Set to true for a numerical body type.
   *
   * @return (mixed)
   *   Integer value of body type if numeric, string if not numeric.
   */
  public function getBodyType($messageId, $numeric = false) {
    // See imap_fetchstructure() documentation for explanation.
    $types = array(
      0 => 'Text',
      1 => 'Multipart',
      2 => 'Message',
      3 => 'Application',
      4 => 'Audio',
      5 => 'Image',
      6 => 'Video',
      7 => 'Other',
    );

    // Get the structure of the message.
    $structure = $this->getStructure($messageId);

    // Return a number or a string, depending on the $numeric value.
    if ($numeric) {
      return $structure->type;
    } else {
      return $types[$structure->type];
    }
  }

  /**
   * Returns the encoding type of a given $messageId.
   *
   * @param $messageId (int)
   *   Message id.
   * @param $numeric (bool)
   *   Set to true for a numerical encoding type.
   *
   * @return (mixed)
   *   Integer value of body type if numeric, string if not numeric.
   */
  public function getEncodingType($messageId, $numeric = false) {
    // See imap_fetchstructure() documentation for explanation.
    $encodings = array(
      0 => '7BIT',
      1 => '8BIT',
      2 => 'BINARY',
      3 => 'BASE64',
      4 => 'QUOTED-PRINTABLE',
      5 => 'OTHER',
    );

    // Get the structure of the message.
    $structure = $this->getStructure($messageId);

    // Return a number or a string, depending on the $numeric value.
    if ($numeric) {
      return $structure->encoding;
    } else {
      return $encodings[$structure->encoding];
    }
  }

  /**
   * Closes an active IMAP connection.
   *
   * @return (empty)
   */
  public function disconnect() {
    // Close the connection, deleting all messages marked for deletion.
    imap_close($this->mailbox, CL_EXPUNGE);
  }

  /**
   * Reconnect to the IMAP server.
   *
   * @return (empty)
   *
   * @throws Exception when IMAP can't reconnect.
   */
  private function reconnect() {
    $this->mailbox = imap_open($this->address, $this->user, $this->pass);
    if (!$this->mailbox) {
      throw new Exception("Reconnection Failure: " . imap_last_error());
    }
  }

  /**
   * Checks to see if the connection is alive. If not, reconnects to server.
   *
   * @return (empty)
   */
  private function tickle() {
    if (!imap_ping($this->mailbox)) {
        $this->reconnect;
    }
  }

  /**
   * Determines whether the given message is from an auto-responder.
   *
   * This method checks whether the header contains any auto response headers as
   * outlined in RFC 3834, and also checks to see if the subject line contains
   * certain strings set by different email providers to indicate an automatic
   * response.
   *
   * @see http://tools.ietf.org/html/rfc3834
   *
   * @param $header (string)
   *   Message header as returned by imap_fetchheader().
   *
   * @return (bool)
   *   TRUE if this message comes from an autoresponder.
   */
  private function detectAutoresponder($header) {
    $autoresponder_strings = array(
      'X-Autoresponse:', // Other email servers.
      'X-Autorespond:', // LogSat server.
      'Subject: Auto Response', // Yahoo mail.
      'Out of office', // Generic.
      'Out of the office', // Generic.
      'out of the office', // Generic.
      'Auto-reply', // Generic.
      'Autoreply', // Generic.
      'autoreply', // Generic.
    );

    // Check for presence of different autoresponder strings.
    foreach ($autoresponder_strings as $string) {
      if (strpos($header, $string) !== false) {
        return true;
      }
    }

    return false;
  } 
    public function fetch($messageId)
    { 
        if(!empty($messageId))
        {
            $this->connection = $this->mailbox;
            $this->messageNumber = $messageId;
            $structure = @imap_fetchstructure($this->connection, $this->messageNumber);
            if(!$structure)
            {
                return false;
            }
            else 
            {
                $this->recurse($structure->parts);
                return true;
            } 
        }
    }
    
    public function recurse($messageParts, $prefix = '', $index = 1, $fullPrefix = true) 
    { 
        $charSetTypeArr=array();
        $iTotalAttachment=$this->iTotalAttachment;
        foreach($messageParts as $part) 
        {   
            $messageId = $this->messageNumber;
            $partNumber = $prefix . $index;  
            $szAttachmentType = '';
            $szFileContent = '';
            $partArr=  toArray($part);
//            echo "<br /><br />";
//            print_r($partArr);
//            echo "<br /><br />";
            //echo $part->encoding."encoding type<br />";
            //echo  $part->type."type<br />";
            if($partArr['type'] == 0)
            { 
                if(!empty($partArr['parameters']))
                {
                    $charSetTypeArr[] = $partArr['parameters'][0]['value'];
                }
                //echo "<br/>".$partArr['subtype']."subtype<br><br>";
                if($partArr['subtype'] == 'PLAIN')
                {
                    $this->bodyPlain .= $this->getPart($partNumber, $partArr['encoding']);
                    if($partArr['encoding']==4 &&  strtolower($partArr['parameters'][0]['value'])!='utf-8')
                    {
                        $this->bodyPlain = utf8_encode($this->bodyPlain);
                    }
                }
                else 
                { 
                    if(strtolower($partArr['disposition'])=='attachment' && $this->bodyHTML=='')
                    {
                        $this->iAddPlainTextBody=true;
                    }
                    
                    //echo $part->parameters[0]->value."charset";
                    $bodyHTML = $this->getPart($partNumber, $partArr['encoding']); 
                    if($partArr['encoding']==4 &&  strtolower($partArr['parameters'][0]['value'])!='utf-8')
                    {
                        if(strtolower($partArr['disposition'])!='attachment')
                        {
                            $bodyHTML = utf8_encode($bodyHTML);
                        }
                    } 
                    $this->bodyHTML .=$bodyHTML;
                    $this->bodyHTML = preg_replace_callback("/<img[^>]+\>/i", "", $this->bodyHTML);  
                    
                }
            }
            elseif($partArr['type'] == 2)
            {   
                /*
                $msg = new cImap($this->connection, $this->messageNumber);
                $msg->getAttachments = $this->getAttachments;
                $msg->recurse($part->parts, $partNumber.'.', 0, false);
                $this->attachments[] = array(
                    'type' => $part->type,
                    'subtype' => $part->subtype,
                    'filename' => '',
                    'data' => $msg,
                    'inline' => false,
                );
                 * 
                 */
            }
            elseif(isset($partArr['parts']))
            {
                if($fullPrefix)
                {
                    $this->recurse($part->parts, $prefix.$index.'.');
                }
                else
                {
                    $this->recurse($part->parts, $prefix);
                }
            }
            elseif($partArr['type'] > 2)
            {
                $szAttachmentName='';
                if($partArr['disposition'] == 'INLINE')
                {
                    $inlinedParametersArr=array();
                    $inlinedParametersArr=$partArr['dparameters'];
                    if(!empty($inlinedParametersArr))
                    {
                        foreach($inlinedParametersArr as $inlinedParametersArrs)
                        {
                            if(strtolower($inlinedParametersArrs['attribute'])=='filename')
                            {
                                $szAttachmentName = $inlinedParametersArrs['value'];
                            }
                        }
                    }
                    
                    $szAttachmentType = $partArr['type'];
                    $szAttachmentSubType = $partArr['subtype'];
                    $szFileContent = $this->getPart($partNumber, $partArr['encoding']); 
                    $this->iInlineAttachments = 1;
                }
            } 
            if(!empty($szAttachmentName))
            {
                $szAttachmetFolderPath = __APP_PATH_GMAIL_ATTACHMENT__."/".$messageId; 
                if(!file_exists($szAttachmetFolderPath))
                {
                    $this->makeDirectory($szAttachmetFolderPath);
                } 
                $k=++$iTotalAttachment;
                $szAttachmentNameArr=explode(".",$szAttachmentName);
                //echo $szAttachmentName."szAttachmentName3<br /><br />";
                $ImageExt = substr($szAttachmentName, strrpos($szAttachmentName, '.'));
                if($ImageExt!='' && strtolower($szAttachmentName)!='utf-8' && $szAttachmentNameArr[1]!='')
                {
                    $pos = strpos(strtolower($szAttachmentName), "=?utf-8");
                    if ($pos !== false) {
                        $ImageExt = substr($szAttachmentName, strrpos($szAttachmentName, '.'));
                        $ImageExt = str_replace("?=", "", $ImageExt);
                        $szAttachmentName = "Attachment-".$k."".$ImageExt;
                    }
                    $this->attachmentAry[] = $szAttachmentName;

                    $szFullPath = $szAttachmetFolderPath."/".$szAttachmentName;
                    if(file_exists($szFullPath) && filesize($szFullPath)==0)
                    {
                        @unlink($szFullPath);
                    } 
                    $this->saveAttachments($szFullPath,$szFileContent);
                    
                }
                
            } 
            $index++; 
        }
        if(!empty($charSetTypeArr))
        {
            $this->charSetTypeValue=implode(";",$charSetTypeArr);
        }
        
        return true;
    }
	
    function getPart($partNumber, $encoding) 
    { 
        $data = imap_fetchbody($this->connection, $this->messageNumber, $partNumber);
        switch($encoding) 
        {
            case 0: return $data; // 7BIT
            case 1: return $data; // 8BIT
            case 2: return $data; // BINARY
            case 3: return base64_decode($data); // BASE64
            case 4: return quoted_printable_decode($data); // QUOTED_PRINTABLE
            case 5: return $data; // OTHER
        } 
    }
	
    function getFilenameFromPart($part) 
    { 
        $filename = ''; 
        if($part->ifdparameters) 
        {
            foreach($part->dparameters as $object) 
            {
                if(strtolower($object->attribute) == 'filename') 
                {
                    $filename = $object->value;
                }
            }
        }

        if(!$filename && $part->ifparameters) 
        {
            foreach($part->parameters as $object) 
            {
                if(strtolower($object->attribute) == 'name') 
                {
                    $filename = $object->value;
                }
            }
        } 
        return $filename; 
    } 
    public function _fetch($mail)
    {
        $mail = new cImap();
        $this->box = $this->$mailbox;        
        $structure = imap_fetchstructure($this->box, $mail->id);
        if ((!isset($structure->parts)) || (!is_array($structure->parts)))
        {
            $body = imap_body($this->box, $mail->id);
            $content = new stdClass();
            $content->type = 'content';
            $content->mime = $this->_fetchType($structure);
            $content->charset = $this->_fetchParameter($structure->parameters, 'charset');
            $content->data = $this->_decode($body, $structure->type);
            $content->size = strlen($content->data);
            $mail->content[] = $content;
            return $mail;
        }
        else
        {
            $parts = $this->_fetchPartsStructureRoot($mail, $structure);
            foreach ($parts as $part)
            {
                $content = new stdClass();
                $content->type = null;
                $content->data = null;
                $content->mime = $this->_fetchType($part->data);
                if ((isset($part->data->disposition))
                   && ((strcmp('attachment', $part->data->disposition) == 0)
                   || (strcmp('inline', $part->data->disposition) == 0)))
                {
                    $content->type = $part->data->disposition;
                    $content->name = null;
                    if (isset($part->data->dparameters))
                    {
                        $content->name = $this->_fetchParameter($part->data->dparameters, 'filename');
                    }
                    if (is_null($content->name))
                    {
                        if (isset($part->data->parameters))
                        {
                            $content->name = $this->_fetchParameter($part->data->parameters, 'name');
                        }
                    }
                    $mail->attachments[] = $content;
                }
                else if ($part->data->type == 0)
                {
                    $content->type = 'content';
                    $content->charset = null;
                    if (isset($part->data->parameters))
                    {
                        $content->charset = $this->_fetchParameter($part->data->parameters, 'charset');
                    }
                    $mail->content[] = $content;
                }
                $body = imap_fetchbody($this->box, $mail->id, $part->no);
                if (isset($part->data->encoding))
                {
                    $content->data = $this->_decode($body, $part->data->encoding);
                }
                else
                {
                    $content->data = $body;
                }
                $content->size = strlen($content->data);
            }
        }
        return $mail;
    }
    function getPartType($type)
    {
        if ($type == 0)
        {
            $type = "text/";
        }
        elseif ($type == 1)
        {
            $type = "multipart/";
        }
        elseif ($type == 2)
        {
            $type = "message/";
        }
        elseif ($type == 3)
        {
            $type = "application/";
        }
        elseif ($type == 4)
        {
            $type = "audio/";
        }
        elseif ($type == 5)
        {
            $type = "image/";
        }
        elseif ($type == 6)
        {
            $type = "video";
        }
        elseif($type == 7)
        {
            $type = "other/";
        } 
        return $type;
    }
}