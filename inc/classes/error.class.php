<?php
/**
 * error.php
 *
 * @copyright Copyright (C) 2012 Transporteca, LLC
 * @author Anil
 * @package Transporteca Development
 */
	if( !defined( "__APP_PATH__" ) )
		define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
	require_once( __APP_PATH__ . "/inc/constants.php" );
	require_once( __APP_PATH__ . "/inc/functions.php" );

	/**
	 * CError is interface for validating input and error reporting
	 *
	 * @package CR 3.0 Migration
	 */
	class CError
	{
		var $error = false;
		var $errMessages = array(); // legacy ....leave here

		var $arErrorMessages;
                var $invalidFormatError = array();
                
                var $iLessThenError = array();
                var $iGreaterThenError = array();
                var $isNumericError = array();
                var $isRequiredError = array();
		public $error_detail_ary = array(); 

		/**
		 * class constructor for php5
		 *
		 * @return bool
		 */
		function __construct()
		{
			$this->error = false;
			$this->arErrorMessages = array();
			return true;
		}

		/**
		 * class constructor for php4
		 *
		 * @return cDatabase
		 */
		function CError()
		{
			$argcv = func_get_args();
	 		call_user_func_array( array( &$this, '__construct' ), $argcv );
		}

		/**
		 * Takes the string/array passed to it and wraps the HTML styling around
		 * it, defined by __APP_ERROR_FORMAT_HTML__.
		 *
		 * @author Anil
		 * @access public
		 * @param mixed $message
		 * @return mixed
		 */
		function formatHTMLError( $message, $format = __VLD_ERROR_DISPLAY__ )
		{
			if( !empty( $message ) )
			{
				if( is_array( $message) )
				{
					$message = implode( "<br>", $message );
				}
                if(!empty($format))
                {
                    return str_replace( "{ERROR}", $message, $format );
                }
                else
                {
                    return $message;
                }
			}
			else
			{
				return false;
			}
		}
                
                /**
		 *  This function returns the error message type
		 *
		 * @author Ajay
		 * @access private
		 * @param string $szFieldKey
		 */
                private function getErrorType($szFieldKey)
                {
                    if($this->iLessThenError[$szFieldKey])
                    {
                        return 'LESS_THEN';
                    }
                    else if($this->iGreaterThenError[$szFieldKey])
                    {
                        return 'GREATER_THEN';
                    }
                    else if($this->isNumericError[$szFieldKey])
                    {
                        return 'NUMERIC';
                    }
                    else if($this->isRequiredError[$szFieldKey])
                    {
                        return 'REQUIRED';
                    }
                    else if($this->invalidFormatError[$szFieldKey])
                    {
                        return 'INVALID';
                    }
                }

		/**
		 * Adds the $message to the arErrorMessage[$error_field]. If it is not
		 * an array and has been set it will make it an array and add both the
		 * new message and what it was previously set to.
		 *
		 * @author Anil
		 * @access public
		 * @param string $error_field
		 * @param string $message
		 */
		function addError( $error_field, $message, $custome_add=true ,$counter=false)
		{ 
                    $this->error = true;
                    $custised_field_key = array();
                    $custised_field_key = array('fCargoDimention_landing_page','iQuantity_landing_page','fCargoWeight_landing_page');
                    //echo " error field ".$error_field ;
                    //print_r($custised_field_key) ;

                    if(!empty($custised_field_key) && in_array($error_field,$custised_field_key))
                    {
                        $custome_add = false ;
                    }			
                    if( isset( $this->arErrorMessages[$error_field] ) && $custome_add)
                    {		
                        if( is_array( $this->arErrorMessages[$error_field] ) )
                        {
                            if( !in_array( $message, $this->arErrorMessages[$error_field] ) )
                            {
                                $this->arErrorMessages[$error_field][] = $message;
                            }
                        }
                        else
                        {
                            if( $message != $this->arErrorMessages[$error_field] )
                            {
                                $this->arErrorMessages[$error_field] = $this->arErrorMessages[$error_field]."<br>".$message;
                            }
                        }
                    }
                    else
                    {
                        $this->arErrorMessages[$error_field] = $message;
                    }
                    if(cPartner::$bApiValidation)
                    {
                        $this->buildErrorDetailAry($error_field,false,$counter);
                    }
		}
                
                /*
                * 
                */
                public function buildErrorDetailAry($fieldKey,$szErrorType=false,$counter=false)
                { 
                    if(!empty($fieldKey))
                    {
                        if(empty($szErrorType))
                        {
                            $szErrorType = $this->getErrorType($fieldKey);  
                        }    
                        $kPartner = new cPartner();
                        $errorDetail = $kPartner->getErrorDetailArry($fieldKey,$szErrorType); 
                            
                        if(!empty($errorDetail))
                        {
                            $this->error = true;
                            if($counter > 0)
                            {
                                $fieldKey = $fieldKey."_".($counter+1);
                                $this->error_detail_ary[$fieldKey] = $errorDetail;
                            }
                            else
                            {
                                $this->error_detail_ary[$fieldKey] = $errorDetail;
                            } 
                        }  
                    } 
                }

		/**
		 * This will validate $value under the passed parameters. $validation is
		 * a string of what type of validation you want. $error_field is the
		 * array element that will be set in arErrorMessages. $error_message is
		 * the prefix to append to the error message. $min_length is the min value
		 * a number can have or min length a string can be. $max_length is the
		 * same.
		 *
		 * @author Anil
		 * @access public
		 * @param mixed $value
		 * @param string $validation type of validation
		 * @param string $error_field arErrorMessages array element
		 * @param string $error_message error message prefix
		 * @param int $min_length min value or min length
		 * @param int $max_length max value or max length
		 * @param bool $required
		 * @return mixed
		 */
		function validateInput( $value, $validation, $error_field, $error_message, $min_length = false, $max_length = false, $required = false, $counter=false )
		{
                    $this->iLessThenError[$error_field] = false;
                    $this->iGreaterThenError[$error_field] = false;
                    $this->isNumericError[$error_field] = false;
                    $this->isRequiredError[$error_field] = false;
                    $this->invalidFormatError[$error_field] = false;
                    
                    if (!is_array($value))
                    {
                        $value = trim( $value );
                    } 
                    
                    $szErrorMessage = $error_message;
                    $error = false; 
                    if( $required === true )
                    {	

                        if($validation == "__VLD_CASE_COMPANY_REG__" )
                        {	 
                            if($value=='')
                            {
                                $error = true;
                                $this->addError( "szCompanyRegNo" , t($this->t_base.'messages/company_reg_req'),true,$counter );
                                return false;
                            }
                            else
                            {
                                return $value;
                            }
                        }	
                        if($validation == "CARGO_MEASURE")
                        {
                            //$value = (float)$value ;
                            //echo " values ".$value ;
                            //echo " Max- ".$max_length ;
                            if(($value === "0" || $value === 0 || $value === 0.00 || empty( $value ) ) || ($value < 0))
                            {
                                $error = true;
                                $this->iGreaterThenError[$error_field] = true;
                                $this->addError( $error_field, $szErrorMessage . " ".t('Error/must_be_more_than_zero'),true,$counter );
                                return false;
                            } 
                            if($max_length !== false && $value > $max_length )
                            {
                                $error = true;
                                $this->iLessThenError[$error_field] = true;
                                $this->addError( $error_field, $szErrorMessage . " ".t('Error/must_be_more')." ". number_format((float)$max_length),true,$counter );
                                return false;
                            }
                        }	
							
                        if( empty( $value ) && $value !== "0" && $value !== 0 )
                        {
                            if( ( $validation != __VLD_CASE_BOOL__ || $validation != __VLD_CASE_STRICTBOOL__ ) && $value !== false )
                            {
                                $error = true;
                                if($error_field=="fMinimumPrice" || $error_field=="fPricePerBooking" || $error_field=="fPricePer100Kg")
                                {
                                    $this->iGreaterThenError[$error_field] = true;
                                    $this->addError( $error_field, $szErrorMessage . " ".t('Error/must_be')." ". number_format($min_length) . " ".t('Error/or_more_1'),true,$counter);	
                                }
                                else
                                {
                                    $this->isRequiredError[$error_field] = true;
                                    $this->addError( $error_field, $szErrorMessage ." ".t('Error/reqiured') ,true,$counter);
                                }
                                return false;
                            }
                        }
                        
                    } 
                    if( !empty( $value ) )
                    {
                        switch( $validation )
                        {
                            case "NUMERIC":
                                if( ( !is_numeric( $value )) )
                                {
                                    $error = true;
                                    if($error_field=="fCargoValue" || $error_field=="fMinimumPrice" || $error_field=="fPricePerBooking" || $error_field=="fPricePer100Kg")
                                    {
                                        $this->iGreaterThenError[$error_field] = true;
                                        $this->addError( $error_field, $szErrorMessage . " ".t('Error/must_be')." ". number_format($min_length) . " ".t('Error/or_more_1'),true,$counter);	
                                    }
                                    else
                                    {
                                        $this->isNumericError[$error_field]=true;
                                        $this->addError( $error_field, $szErrorMessage . " ".t('Error/number_only'),true,$counter );
                                    }
                                    return false;
                                }
                                elseif( $min_length !== false || $max_length !== false )
                                { 
                                    
                                    $numericDollarArray=array('Deposit Amount');
                                    if( $min_length !== false && $value  < $min_length )
                                    {
                                        if(in_array($szErrorMessage,$numericDollarArray))
                                        {
                                            $min_length="$".$min_length;
                                        }
                                        $error = true;
                                        if($error_field=='fWmFactor')
                                        {
                                            $this->iGreaterThenError[$error_field] = true;
                                            $this->addError( $error_field, $szErrorMessage . " ".t('Error/must_be')." ". number_format($min_length) . " ".t('Error/must_be'),true,$counter);
                                        }
                                        else if($error_field=="fMinimumPrice" || $error_field=="fPricePerBooking" || $error_field=="fPricePer100Kg")
                                        {
                                            $this->iGreaterThenError[$error_field] = true;
                                            $this->addError( $error_field, $szErrorMessage . " ".t('Error/must_be')." ". number_format($min_length) . " ".t('Error/or_more_1'),true,$counter);	
                                        }
                                        else
                                        {
                                            $this->iGreaterThenError[$error_field] = true;
                                            $this->addError( $error_field, $szErrorMessage . " ".t('Error/must_be')." ". $min_length . " ".t('Error/must_be'),true,$counter);
                                        }
                                        return false;
                                    }
                                    if( $max_length !== false && $value > $max_length )
                                    {
                                        if(in_array($szErrorMessage,$numericDollarArray))
                                        {
                                            $max_length="$".$max_length;
                                        }
                                        $error = true;
                                        if($error_field=='fWmFactor')
                                        {
                                            $this->iLessThenError[$error_field] = true;
                                            $this->addError( $error_field, $szErrorMessage . " ".t('Error/must_be_more')." ". number_format((float)$max_length),true,$counter );
                                        }
                                        else if($error_field=='iVolume')
                                        {
                                            $this->iLessThenError[$error_field] = true;
                                            $this->addError( $error_field, $szErrorMessage . " ".t('Error/must_be_more')." ". number_format((float)$max_length)." ".t('Error/cbm') ,true,$counter);
                                        }
                                        else if($error_field=='fCargoWeight_landing_page')
                                        {
                                            $this->iLessThenError[$error_field] = true;
                                            $this->addError( $error_field, $szErrorMessage . " ".t('Error/must_be_more')." ". number_format((float)$max_length)." ".t('Error/kg'),true,$counter );
                                        }
                                        else
                                        {
                                            $this->iLessThenError[$error_field] = true;
                                            $this->addError( $error_field, $szErrorMessage . " ".t('Error/must_be_more')." ". $max_length ,true,$counter);
                                        }
                                        return false;
                                    }
                                }
                                break;
                            case "CARD":
                                if( ( !is_numeric( $value ) && $required === true ) )
                                {
                                    $this->isNumericError[$error_field] = true;
                                    $error = true;
                                    $this->addError( $error_field, $szErrorMessage . " ".t('Error/number_only'),true,$counter );
                                    return false;
                                }
                                elseif( $min_length !== false || $max_length !== false )
                                {
                                    if( $min_length !== false && strlen( (string)$value ) < $min_length )
                                    {
                                        $error = true;
                                        $this->addError( $error_field, $szErrorMessage . " ".t('Error/must_be')." ". $min_length." ".t('Error/digit'),true,$counter );
                                        return false;
                                    }
                                    if( $max_length !== false && strlen( (string)$value ) > $max_length )
                                    {
                                        $error = true;
                                        $this->addError( $error_field, $szErrorMessage . " ".t('Error/must_be_more')." ". $max_length . " ".t('Error/digit') ,true,$counter);
                                        return false;
                                    }
                                }
                                break;
                            case "ALPHA":
                                if( !preg_match( "/^[a-z]+$/i", $value ) )
                                {
                                    $error = true;
                                    $this->invalidFormatError[$error_field] = true;
                                    $this->addError( $error_field, $szErrorMessage . " ".t('Error/only_letter'),true,$counter );
                                    return false;
                                }
                                break;
                            case "ALPHANUMERIC":
                                if( !preg_match( "/^[a-z0-9]+$/i", $value ) )
                                {
                                    $error = true;
                                    $this->invalidFormatError[$error_field] = true;
                                    $this->addError( $error_field, $szErrorMessage . " ".t('Error/num_letter'),true,$counter );
                                    return false;
                                }
                                break;
                            case "URI":
                                if( !preg_match( "/^[a-z0-9\-\_\.]+$/i", $value ) )
                                {
                                    $error = true;
                                    $this->invalidFormatError[$error_field] = true;
                                    $this->addError( $error_field, $szErrorMessage . " ".t('Error/num_letter'),true,$counter);
                                    return false;
                                }
                                break;
                            case "EMAIL":
                                if( !preg_match( "/^[_a-z0-9-\+\$\!\%\=\&\^]+(\.[_a-z0-9-\+\$\!\%\=\&\^]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z0-9]{2,4})$/i", $value ) )
                                {
                                    $error = true;
                                    $this->invalidFormatError[$error_field] = true;
                                    $this->addError( $error_field, $szErrorMessage . " ".t('Error/valid_email') ,true,$counter);
                                    return false;
                                }
                                break;
                            case "BOOL":
                                if( !is_bool( $value ) && !( $value != "true" || $value != "false" ) && !( $value != "1" || $value != "0" ) )
                                {
                                    $error = true;
                                    $this->invalidFormatError[$error_field] = true;
                                    $this->addError( $error_field, $szErrorMessage . " ".t('Error/must_boolean'),true,$counter);
                                    return false;
                                }
                                break;
                            case "STRICTBOOL":
                                if( !is_bool( $value ) )
                                {
                                    $error = true;
                                    $this->invalidFormatError[$error_field] = true;
                                    $this->addError( $error_field, $szErrorMessage . " ".t('Error/must_boolean') ,true,$counter);
                                    return false;
                                }
                                break;
                            case "ADDRESS":
                            case "NAME":
                                if( !preg_match( "/^[a-z0-9\,\.\#\-\_\s\']+$/i", $value ) )
                                {
                                    $error = true;
                                    $this->invalidFormatError[$error_field] = true;
                                    $this->addError( $error_field, $szErrorMessage . " ".t('Error/l_n_s_un_d') ,true,$counter);
                                    return false;
                                }
                                break;
                            case "URL":
                                if( !preg_match( "/^[a-zA-Z]+[:\/\/]+[A-Za-z0-9\-_]+\\.+[A-Za-z0-9\.\/%&=\?\-_]+$/i", $value ) )
                                {
                                    $error = true;
                                    $this->invalidFormatError[$error_field] = true;
                                    $this->addError( $error_field, $szErrorMessage . " ".t('Error/web_address') ,true,$counter);
                                    return false;
                                }
                                break;
                            case "USERNAME":
                                if( !preg_match( "/^[\S]+$/i", $value ) )
                                {
                                    $error = true;
                                    $this->invalidFormatError[$error_field] = true;
                                    $this->addError( $error_field, $szErrorMessage . " ".t('Error/not_spaces_sc') ,true,$counter);
                                    return false;
                                }
                                break;
                            case "PASSWORD":
                                if( !preg_match( "/^[\S]+$/i", $value ) )
                                {
                                    $error = true;
                                    $this->invalidFormatError[$error_field] = true;
                                    $this->addError( $error_field, $szErrorMessage . " ".t('Error/no_spaces'),true,$counter );
                                    return false;
                                }
                                else if(!preg_match( "/(?=^.{8,}$)(?=.*\d).*$/", $value ))
                                {
                                    $error = true;
                                    $this->invalidFormatError[$error_field] = true;
                                    $this->addError( $error_field, $szErrorMessage . " ".t('Error/password') ,true,$counter);
                                    return false;
                                }
                                break;
                            case "PASSWORD_NOFORCE":
                                if( !preg_match( "/^[\S]+$/i", $value ) )
                                {
                                    $error = true;
                                    $this->invalidFormatError[$error_field] = true;
                                    $this->addError( $error_field, $szErrorMessage . " ".t('Error/no_spaces'),true,$counter );
                                    return false;
                                }
                                break;
                            case "DATE":
                                if( !strtotime( $value ) || strtotime( $value ) == -1 )
                                {
                                    $error = true;
                                    $this->invalidFormatError[$error_field] = true;
                                    $this->addError( $error_field, $szErrorMessage . " ".t('Error/valid_date'),true,$counter );
                                    return false;
                                }
                                else
                                {
                                    $value = strtotime( $value );
                                }
                                break;
                            case "PHONE":
                                if( !preg_match( "/^\d{3}-\d{3}-\d{4}$/", $value ) )
                                {
                                    $error = true;
                                    $this->invalidFormatError[$error_field] = true;
                                    $this->addError( $error_field, $szErrorMessage . " must be a valid phone number." ,true,$counter);
                                    return false;
                                }
                                break;
                            case "PHONE_2": 
                                if( !preg_match( "/^\d{10}$/", $value ) || substr($value,0,3) == '555')
                                {
                                    $error = true;
                                    $this->invalidFormatError[$error_field] = true;
                                    $this->addError( $error_field, $szErrorMessage . " must be a valid 10 digit phone number." ,true,$counter);
                                    return false;
                                }                                                
                                break;
                            case "DOLLARS":
                            case "MONEY_US":
                                if( !preg_match( "/^[0-9]+(\.[0-9]{2})*$/", $value ) )
                                {
                                    $error = true;
                                    $this->invalidFormatError[$error_field] = true;
                                    $this->addError( $error_field, $szErrorMessage . " must be a valid money format. (Ex. 00.00 )" ,true,$counter);
                                    return false;
                                }
                                break;
                            case "CC_EXP":
                                if( !preg_match( "/^[0-9]{2}[-][0-9]{2}$/", $value ) )
                                {
                                    $error = true;
                                    $this->error = true;
                                    $this->invalidFormatError[$error_field] = true;
                                    $this->addError( $error_field, $szErrorMessage . " must be MM/YY." ,true,$counter);
                                    return false;
                                }
                                break;
                            case "ARRAY_TYPE":
                                if (!is_array($value))
                                {
                                    $error = true;
                                    $this->addError( $error_field, $szErrorMessage . " must be an array." ,true,$counter);
                                    return false;
                                }
                                elseif( $min_length !== false || $max_length !== false )
                                {
                                    if( $min_length !== false && count($value) < $min_length )
                                    {
                                        $error = true;
                                        $this->addError( $error_field, $szErrorMessage . " must be  greater then " . ($min_length - 1) . "." ,true,$counter);
                                        return false;
                                    }
                                    if( $max_length !== false && count($value) > $max_length )
                                    {
                                        $error = true;
                                        $this->addError( $error_field, $szErrorMessage . " must be no more than " . $max_length . "." ,true,$counter);
                                        return false;
                                    }
                                }
                                break;
                            case "PAYOUT_PERCENTAGE":
                                if( ( !is_numeric( $value )) )
                                {
                                    $error = true;
                                    $this->invalidFormatError[$error_field] = true;
                                    $this->addError( $error_field, $szErrorMessage . " must be only numbers.",true,$counter );
                                    return false;
                                }
                                elseif( $min_length !== false || $max_length !== false )
                                {
                                    if( $min_length !== false && $value  < $min_length )
                                    {
                                        $error = true;
                                        $this->addError( $error_field, $szErrorMessage . " must be greater then 0." ,true,$counter);
                                        return false;
                                    }
                                    if( $max_length !== false && $value > $max_length )
                                    {								
                                        $error = true;
                                        $this->addError( $error_field, $szErrorMessage . " must be no more than " . $max_length . "." ,true,$counter);
                                        return false;
                                    }
                                }
                                break;
                            case "CARGO_MEASURE":
                                //echo " err cls value ".$value ;
                                if( ( !is_numeric( $value )) )
                                {
                                    $error = true;
                                    $this->addError( $error_field, $szErrorMessage . " ".t('Error/number_only') ,true,$counter);
                                    return false;
                                }
                                elseif( $min_length !== false || $max_length !== false )
                                { 
                                    $numericDollarArray=array('Deposit Amount');
                                    if( $min_length !== false && $value  < $min_length )
                                    {
                                        if(in_array($szErrorMessage,$numericDollarArray))
                                        {
                                            $min_length="$".$min_length;
                                        }
                                        $error = true;
                                        $this->addError( $error_field, $szErrorMessage . " ".t('Error/must_be')." ". $min_length . " ".t('Error/must_be'),true,$counter);
                                        return false;
                                    }
                                    if( $max_length !== false && $value > $max_length )
                                    {
                                        if(in_array($szErrorMessage,$numericDollarArray))
                                        {
                                            $max_length="$".$max_length;
                                        }
                                        $error = true;
                                        $this->addError( $error_field, $szErrorMessage . " ".t('Error/must_be_more')." ". $max_length,true,$counter);
                                        return false;
                                    }
                                }
                                break;
                            case "DIMENSION_UNIT":
                                if(strtolower($value) != "cm" && strtolower($value) != "inch")
                                { 
                                    $error = true;
                                    $this->invalidFormatError[$error_field] = true;
                                    $this->addError( $error_field, $szErrorMessage . " ".t('Error/must_be')." in inch or cm.",true,$counter);
                                    return false; 
                                }
                                break;
                            case "WEIGHT_UNIT":
                                if(strtolower($value) != "kg" && strtolower($value) != "pounds")
                                { 
                                    $error = true;
                                    $this->invalidFormatError[$error_field] = true;
                                    $this->addError( $error_field, $szErrorMessage . " ".t('Error/must_be')." in kg or pounds.",true,$counter);
                                    return false; 
                                }
                                 break;
                            case "ANYTHING":
                                break;						
                            default:
                                $error = true;
                                $this->addError( "error", "Unknown validation type. I was sent this type: " . $validation,true,$counter );
                                break;
                        }
                    } 
                    if( $min_length !== false && $validation == "NUMERIC" &&  $value!="" )
                    {
                        if( $value < $min_length )
                        {
                            $error = true;
                            $this->iGreaterThenError[$error_field] = true;
                            $this->addError( $error_field, $szErrorMessage . " must be " . $min_length . " or more" ,true,$counter);
                            return false;
                        }
                    }

                    if( $min_length !== false && $validation != "NUMERIC" && !empty( $value ) && $validation != 'ARRAY_TYPE' )
                    {
                        if( strlen( $value ) < $min_length )
                        {
                            $error = true;
                            $this->addError( $error_field, $szErrorMessage . " must be at least " . $min_length . " characters in length",true,$counter );
                            return false;
                        }
                    }

                    if( $max_length !== false && $validation != "NUMERIC" && !empty( $value ) && $validation != 'ARRAY_TYPE' )
                    {
                        if( strlen( $value ) > $max_length )
                        {
                            $error = true;
                            $this->addError( $error_field, $szErrorMessage . " must not be longer than " . $max_length . " characters in length" ,true,$counter);
                            return false;
                        }
                    }
                    if( $error === true )
                    {
                        $this->addError( "error", "Unknown error validating field. The field was: " . $error_field . ". Validation: " . $validation . ". Value: " . $value ,true,$counter);
                        return false;
                    }
                    else
                    {
                        //return str_replace( "\$", "\\\$", $value );
                        return $value;
                    }
		}

		/**
		 * Resets the error and arErrorMessages members.
		 *
		 * @author Anil
		 * @access public
		 */
		function resetErrors()
		{
                    $this->error = false;
                    $this->arErrorMessages = array();
		}

		/**
		 * Return HTML formated error message.
		 *
		 * @deprecated since implementation of formatHTMLError
		 * @see formatHTMLErrror()
		 * @author Anil
		 * @access public
		 * @param int $message
		 * @param mixed $container
		 */
		function formatError( $message, &$container )
		{
                    $container .= str_replace( "{ERROR}", $message, __VLD_ERROR_DISPLAY__ );
		}

		/**
		 * This function will log errors to the appropriate log file.  The accepted error levels are as follows:
		 * info, warning, critical, stdout.  The function trys to be 'smart' and map other possible user inputs to
		 * the appropriate log file.  The default is logged to crit.  This function will also log ALL messages to a
		 * file called all_errors.csv.  It will also log the errors into a specific class error log so you can narrow
		 * down what you are looking for.
		 *
		 * @author Anil
		 * @param string $error_field
		 * @param string $message
		 * @param string $error_type
		 * @param string $class_name
		 * @param string $function
		 * @param int $line
		 * @param string $error_severity
		 * @return bool
		 */

		function logError( $error_field, $message, $error_type, $class_name, $function, $line, $error_severity="critical")
		{
			$error_severity = strtolower(trim($error_severity));
			$class_name = strtolower(trim($class_name));
			$function = trim($function);
			$line = (int)$line;
			$message = trim($message);
			$error_field = trim($error_field);
			
			$error_message_standard = "Internal server error, Please try again later.";
			$this->addError($error_field, $error_message_standard);
			
			switch ($error_severity)
			{
				case "warning":
					break;
				case "medium":
					$log_file = "warning";
				case "high":
					$log_file = "warning";
				case "crit":
					$log_file = "critical";
					break;
				case "critical":
					break;
				case "stdout":
					break;
				case "std_out":
					$log_file = "stdout";
					break;
				case "command_line":
					$log_file = "stdout";
				default:
					$log_file = "critical";
					break;
			}			
			$find_ary = array("{TIME}", "{SEVERITY}", "{ERROR_TYPE}", "{FILE}", "{FUNCTION}", "{LINE}", "{ERROR}");
			$replace_ary = array('TIME'=>date('Y-m-d H:i:s', time()),'LINE'=>$line,'ERROR'=>$message);
			//$error_string = str_replace($find_ary, $replace_ary, __LOG_LINE_FORMAT__);
			
			$error_string = print_r($replace_ary,true);
			
			if ($error_severity == 'critical')
			{
				/*	ob_start();
				debug_print_backtrace();
				$error_string .= ob_get_clean();
				*/
			}

			$log_file = __APP_PATH_LOGS__."/".$error_severity.".log";
			// $log_file_class = __APP_PATH_LOG_CLASSES__."/".$class_name."_".$error_severity.".log";
			//echo "<br><br>".$log_file ;
			if ($error_severity == "stdout")
			{
				echo $error_string;
			}
			else
			{
				$handle = fopen($log_file, "a+");
				fwrite($handle, $error_string);
				//fwrite($f, $value);
				fclose($handle);

				/*
				$handle = fopen ($log_file_class, "a+");
				fwrite($handle, $error_string);
				fclose($handle);
				*/
			}

			return true;
		}
                
                function whole_int($val)
                {
                    $val = strval($val);
                    $val = str_replace('-', '', $val);

                    if (ctype_digit($val))
                    {
                        if ($val === (string)0)
                            return true;
                        elseif(ltrim($val, '0') === $val)
                            return true;
                    }

                    return false;
                }
	}
?>
