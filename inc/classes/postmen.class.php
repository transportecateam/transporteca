<?php
/**
 * This file is the container for Postmen api related functionality. 
 *
 * postmen.class.php
 *
 * @copyright Copyright (C) 2016 Transporteca
 * @author Ajay
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" ); 

class cPostmen extends cDatabase
{       
    public static $standaradAgreementId = __STANDARD_AGREEMENT_ID__;
    
    private $szApiKey;
    private $szApiVersion;
    private $szErrorMessage; 
    private $szConfigAry; 
    
    // rate limiting attributes
    private $rateResponseAry;
    public static $globalPostMenApiResponse;
    public static $globalEasyPostApiResponse;
      
    var $t_base_courier='management/providerProduct/'; 
    var $t_base_services = "SERVICEOFFERING/"; 
    var $t_base_courier_label ="COURIERLABEL/";
    
    function __construct()
    { 
        parent::__construct();
        return true;
    }
     
    private function getPostmenConfig()
    {
        $this->szApiVersion = "1.0.0";
        $this->szApiKey = __POSTMEN_API_KEY__;
        $this->szConfigAry = array();
        $this->szConfigAry['endpoint'] = __POSTMEN_API_ENDPOINT__;
        $this->szConfigAry['retry'] = false;
        $this->szConfigAry['rate'] = true; 
        $this->szConfigAry['safe'] = true; 
        $this->szConfigAry = $this->mergeArray($config);
        
        // set attributes concerning ratelimiting and auto-retry
        $this->_delay = 1;
        $this->_retries = 0;
        $this->_max_retries = 5;
        $this->_calls_left = NULL;
    }
    
    function getCourierDataPostmenApi($data, $bCheckAuth=false)
    {
        if(!empty($data))
        {
            $kWhsSearch = new cWHSSearch();
            if(!empty($data['dtTimingDate']))
            { 
                $dtShippingDate = $data['dtTimingDate'];
            }
            else
            { 
                $dtShippingDate = date('c');
            } 
            $this->getPostmenConfig();
            $from_address = $this->createOriginAddressObject($data,$bCheckAuth);
            $to_address = $this->createDestinationAddressObject($data,$bCheckAuth);  
            $parcel = $this->createParcelObject($data,$bCheckAuth);
            
            $postmenRequestAry = array();
            $postmenRequestAry['async'] = false;
            $postmenRequestAry['is_document'] = false;
            $postmenRequestAry['shipment']['ship_from'] = $from_address;
            $postmenRequestAry['shipment']['ship_to'] = $to_address;
            $postmenRequestAry['shipment']['parcels'] = $parcel;
              
            try
            {
                $responseAry = array();
                $responseAry = $this->buildCurlParams($postmenRequestAry,$bCheckAuth);
                return $responseAry;
                
            } catch (Exception $ex) {
                $this->szApiResponseInformationText .= " <strong> Exception: </strong> ".$ex->getMessage();
                $this->iSuccessMessage = 2 ;
                return true;
            } 
        }
    } 
    public function buildCurlParams($searchParamsAry,$bCheckAuth=false) 
    {
        if(!empty($searchParamsAry))
        {
            if(!empty($searchParamsAry['dtTimingDate']))
            { 
                $dtShippingDate = $searchParamsAry['dtTimingDate'];
            }
            else
            { 
                $dtShippingDate = date('c');
            }
            $parameters = $this->szConfigAry; 
            $parameters['body'] = json_encode($searchParamsAry);
 
            $headers = array(
                "content-type: application/json",
                "postmen-api-key: $this->szApiKey",
                "x-postmen-agent: php-sdk-$this->szApiVersion"
            );  
            $url = $parameters['endpoint']; 

            $curl_params = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $url,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_HEADER => true,
                CURLOPT_POSTFIELDS => $parameters['body']
            ); 

            $curl = curl_init();
            curl_setopt_array($curl, $curl_params);
            // make call
            $response = curl_exec($curl);
            $err = curl_error($curl);  
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE); 
            $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
            curl_close($curl);
            
            // convert headers string to an array
            $response_headers = substr($response, 0, $header_size);
            $response_body = substr($response, $header_size);
               
            $fedexApiLogsAry = array(); 
            $fedexApiLogsAry['iWebServiceType'] = 7 ; //7. Means Postmen Api responses
            $fedexApiLogsAry['szRequestData'] = print_R($searchParamsAry,true);
            $fedexApiLogsAry['szResponseData'] = print_R($response_body,true);
            $kWebServices = new cWebServices();
            $kWebServices->addFedexApiLogs($fedexApiLogsAry); 
        
            $postMenApiResponseAry = array();
            if($httpCode == 200)
            { 
                $responseAry = array();
                $responseAry = json_decode($response_body,true);
                 
                $apiResponseAry = $responseAry['data']['rates'];
                $szShipmentId = $responseAry['data']['id']; 
                if($apiResponseAry) 
                {  
                    $TntPriceResponseAry = array();
                    $postMenApiResponseAry = array();

                    $szLogCarrierIdString .= "<br> Found Response for Following Carrier IDs: ";
                    if(!empty($apiResponseAry))
                    {  
                        $ctr = 0;
                        foreach($apiResponseAry as $apiResponseArys)
                        { 
                            $szCarrierAccountId = trim($apiResponseArys['shipper_account']['id']);  
                            if($bCheckAuth)
                            {
                                $TntPriceResponseAry[] = $szCarrierAccountId;
                                $postMenApiResponseAry[$szCarrierAccountId] = $apiResponseArys['carrier'];
                            } 
                            else
                            {
                                $postMenApiResponseAry[$szCarrierAccountId][$ctr] = $apiResponseArys;
                                $postMenApiResponseAry[$szCarrierAccountId][$ctr]['szCarrierAccounID'] = $szCarrierAccountId;
                                $postMenApiResponseAry[$szCarrierAccountId][$ctr]['shipment_id'] = $szShipmentId;
                                $postMenApiResponseAry[$szCarrierAccountId][$ctr]['dtShippingDate'] = $dtShippingDate; 
                                $ctr++;
                                $szLogCarrierIdString .= "<br>Shipper ID: ".$szCarrierAccountId ;
                            }
                        }
                        
                        if($bCheckAuth)
                        { 
                            return $TntPriceResponseAry;
                        }
                        else
                        {
                            self::$globalPostMenApiResponse = $postMenApiResponseAry;  
                        }  
                        $this->iSuccessMessage = 1 ;
                        $this->szFoundedCarrierAccountIDs = $szLogCarrierIdString; 
                    }
                } 
                else 
                {
                    $this->szApiResponseInformationText .= " <strong> ERROR: </strong> <br> No Response From API ";
                    $this->iSuccessMessage = 2 ;
                    return true;
                }
            }
            else
            {
                if($err)
                {
                    $this->handleError("failed to request: $err" , 100, true, array()); 
                }
                else
                {
                    $this->handleError("Somethin went wrong at postmen side. We are getting response with http code: $httpCode" , 100, true, array()); 
                }
            } 
            return $postMenApiResponseAry;
        } 
    }  
    
    public function handleError($err_message, $err_code, $err_retryable, $err_details, $parameters) 
    {
        $error = new PostmenException($err_message, $err_code, $err_retryable, $err_details);
        if ($parameters['safe']) {
            $this->_error = $error;
        } else {
            throw $error;
        }
        return NULL;
    } 
        
    /** takes an associative array $config as argument
    *  returns merged array with local $this->szConfigAry
    *  values from $config are prioritary
    */
    public function mergeArray($config) 
    {
        $parameters = $this->szConfigAry;
        if(!empty($config))
        {
            foreach ($config as $key => $value) 
            {
                $parameters[$key] = $value;
            }
        } 
        return $parameters;
    }
    
    public function getAllCarrierResponseFromPostmenAPI($data)
    {  
        $kCourierServices = new cCourierServices();   
        $kCourierServices->validateInputNewDetails($data); 
        $this->szPackageDetailsLogs = '';

        if($kCourierServices->error==true)
        {  
            return false;
        }  
        if($data['szAPICode']!='')
        {
            $validServiceTypeArr = explode(";",$data['szAPICode']);
        }  
        $validApiCodeAry = array();
        $ctr_1=0;
        if(!empty($validServiceTypeArr))
        {
            foreach($validServiceTypeArr as $validServiceTypeArrs)
            {
                if(!empty($validServiceTypeArrs))
                {
                    $validServiceTypeAry = explode("||||",$validServiceTypeArrs);
                    $validApiCodeAry[$ctr_1] = $validServiceTypeAry[0]; 

                    $validProviderIDAry[$validServiceTypeAry[0]]['idCourierProduct'][$ctr_1] = $validServiceTypeAry[1]; 
                    $validProviderIDAry[$validServiceTypeAry[0]]['idCourierAgreement'][$ctr_1] = $validServiceTypeAry[2];  
                    $ctr_1++;
                }
            }
        } 
        $apiCodeValuesAry = array();
        $apiCodeValuesAry = array_count_values($validApiCodeAry);
        $validServiceTypeArr = $validApiCodeAry ;
        $szCarrierAccountID = trim($data['szCarrierAccountID']); 
        if(empty($szCarrierAccountID))
        {
            $this->szApiResponseLog .= "<br> There is no carrier account id passed for this service <br> ". $data['szForwarderCompanyName']." - ".$szCarrierCompanyName." - ".$szCarrierAccounID;;
            return false;
        }  
        $apiResponseAry = array();
        $bAddressSwaped = false;
        $bExceptionalTradeFlag = false;
        if($data['szSCountry']=='GB' && $data['szCountry']!='DK')
        {
            $bExceptionalTradeFlag = true;
        } 
         
        if(!empty(self::$globalPostMenApiResponse))
        {
            $apiResponseAry = self::$globalPostMenApiResponse;  
        }
        else
        {
            $apiResponseAry = $this->getCourierDataPostmenApi($data); 
        }
         
        if(!empty($apiResponseAry[$szCarrierAccountID]))
        {
            $apiResponseAry = $apiResponseAry[$szCarrierAccountID];
        }   
        
        if(!empty($apiResponseAry))
        {
            $tnt_ctr = 0;
            $TntPriceResponseAry = array(); 
            foreach($apiResponseAry as $apiResponseArys)
            { 
                $szSericeCode = $apiResponseArys['service_type'];  
                $szSericeDescription = $apiResponseArys['service_name'];   
                $TotalNetCharge = $apiResponseArys['total_charge']['amount']; 

                $code = $szSericeCode;    
                if(!empty($validServiceTypeArr) && in_array($szSericeCode,$validServiceTypeArr))
                {
                    $iLoopCounter = $apiCodeValuesAry[$code]; 
                    if(!empty($validProviderIDAry[$code]['idCourierProduct']))
                    { 
                        foreach($validProviderIDAry[$code]['idCourierProduct'] as $key=>$validProviderIDArys)
                        { 
                            $idCourierProduct = $validProviderIDArys;
                            $idCourierAgreement = $validProviderIDAry[$code]['idCourierAgreement'][$key];
                            if($idCourierProduct>0)
                            { 
                                $TntPriceResponseAry[$tnt_ctr]['szServiceCode'] = $szSericeCode;
                                $TntPriceResponseAry[$tnt_ctr]['szServiceCode'] = $szSericeCode;
                                $TntPriceResponseAry[$tnt_ctr]['szServiceDescription'] = $szSericeDescription;
                                $TntPriceResponseAry[$tnt_ctr]['fTotalPrice'] = $apiResponseArys['total_charge']['amount'];
                                $TntPriceResponseAry[$tnt_ctr]['idProviderProduct'] = $idCourierProduct;
                                $TntPriceResponseAry[$tnt_ctr]['idCourierAgreement'] = $idCourierAgreement;  
                                $TntPriceResponseAry[$tnt_ctr]['szCurrency'] = $apiResponseArys['total_charge']['currency'];
                                $TntPriceResponseAry[$tnt_ctr]['szCarrierAccounID'] = $apiResponseArys['szCarrierAccounID'];
                                $TntPriceResponseAry[$tnt_ctr]['szEasypostShipmentID'] = $apiResponseArys['shipment_id'];
                                $TntPriceResponseAry[$tnt_ctr]['szEasyPostRateID'] = $apiResponseArys['id'];
                                
                                $iTransitDays = $apiResponseArys['transit_time']; 
                                if($apiResponseArys['transit_time']>0)
                                {
                                    $dtDeliveryDate = date('Y-m-d',strtotime("+".(int)$apiResponseArys['transit_time']." DAYS"));
                                }
                                $TntPriceResponseAry[$tnt_ctr]['dtDeliveryDate'] = $dtDeliveryDate;
                                $TntPriceResponseAry[$tnt_ctr]['iTransitDays'] = $iTransitDays;
                                $TntPriceResponseAry[$tnt_ctr]['dtShippingDate'] = $apiResponseArys['dtShippingDate'];
                                $tnt_ctr++;
                            }
                        }
                    } 
                }
            }
        }     
        
        $finalApiResponseAry = array();
        if(!empty($TntPriceResponseAry))
        {  
            $finalApiResponseAry = $this->processEasyPostApiResponse($data,$TntPriceResponseAry,$bAddressSwaped); 
            
            $this->szApiResponseLog .= $this->szFoundedCarrierAccountIDs;
            $this->szApiResponseLog .= "<br>Searched for Carrier Account ID: ".$szCarrierAccountID; 
        }
        return $finalApiResponseAry;
    }
    
    function processEasyPostApiResponse($data,$TntPriceResponseAry,$bAddressSwaped=false)
    {  
        if($TntPriceResponseAry)
        { 
            $kCourierService = new cCourierServices();
            $kForwarder_new = new cForwarder();
            $kWarehouseSearch = new cWHSSearch();
            $courierProductMapping = array();
            $courierProductMapping = $kCourierService->getAllProductByApiCodeMapping();
              
            $bVatApplicable = false;
            $kVatApplication = new cVatApplication();
            $idForwarderCountry = $data['idForwarderCountry'];
            $idForwarder = $data['idForwarder'];
            
            if((int)$_SESSION['user_id']>0)
            {
                $kUser = new cUser();
                $kUser->getUserDetails($_SESSION['user_id']); 
                $iPrivateShipping = $kUser->iPrivate; 
                $data['iPrivateShipping'] = $iPrivateShipping;
                $idCustomerAccountCountry = $kUser->szCountry;
            } 
            else
            {
                $idCustomerAccountCountry = $data['idDestinationCountry'];   
                $iPrivateShipping = $data['iPrivateShipping'];
            }
            
            if($iPrivateShipping==1)
            { 
                $vatInputAry = array();
                $vatInputAry['idCustomerCountry'] = $idCustomerAccountCountry;
                $vatInputAry['idOriginCountry'] = $data['idOriginCountry'];
                $vatInputAry['idDestinationCountry'] = $data['idDestinationCountry'];

                $fVatRate = $kVatApplication->getTransportecaVat($vatInputAry);
                if($fVatRate>0)
                {
                    $bVatApplicable = true;
                }
            } 
            $idLandingPage = $data['idLandingPage'];
            $iSearchMiniVersion = $data['iSearchMiniVersion'];
             
            $vogaHandlingFeeAry = array();
            if(($iSearchMiniVersion==7 || $iSearchMiniVersion==8 || $iSearchMiniVersion==9) && $idLandingPage>0)
            { 
                $kExplain = new cExplain();
                $vogaHandlingFeeAry = $kExplain->getStandardQuotePriceByPageID($idLandingPage,false,false,true);
            }   
            $tnt_ctr=0;
            $kCourierServices = new cCourierServices();
            $courierProviderComanyAry = array();
            $courierProviderComanyAry = $kCourierServices->getCourierProviderDetails();
            
            
            
            $stepCounter1 = 1;
            foreach($TntPriceResponseAry as $priceResponseArys)
            {  
                $iTransitDays = 0;   
                $szSericeCode = $priceResponseArys['szServiceCode'];  
                $szSericeDescription = $priceResponseArys['szServiceDescription'];   
                $idCourierProviderProductid = $priceResponseArys['idProviderProduct']; 
                $szCurrency = $priceResponseArys['szCurrency']; 
                $dtDeliveryDate = $priceResponseArys['dtDeliveryDate']; 
                $dtShippingDate = $priceResponseArys['dtShippingDate'];
                $szCarrierAccounID = $priceResponseArys['szCarrierAccounID'];
                $idCourierAgreement = $priceResponseArys['idCourierAgreement'];
                if(__isWeekend($dtShippingDate))
                {
                    $dtShippingDate = __nextMonday($dtShippingDate);
                }
                //echo "<br> Calculation for Agr: ".$idCourierAgreement." Prod: ".$idCourierProviderProductid;
                if(trim($szCurrency)=='RMB')
                {
                    $szCurrency = 'CNY';
                }
                $code = $szSericeCode;  
                $iTransitDays = $priceResponseArys['iTransitDays'];
                $szApiResponseLog .="<br>Transit Days Api Response - ".$iTransitDays." Days";
                
                $iDaysMin =(int)$courierProductMapping[$code]['iDaysMin']; 
                if($iTransitDays<=0)
                {
                    $iTransitDays = $courierProductMapping[$code]['iDaysStd']; 
                }              
                $szApiResponseLog .="<br>TransitDays Before Min Check - ".$iTransitDays ."Days";
                if($iTransitDays<$iDaysMin)
                {
                    $iTransitDays = $iDaysMin; 
                }
                $szApiResponseLog .="<br>TransitDays After Min Check - ".$iTransitDays ."Days";
                $pricingArr = $kCourierService->getCourierProductProviderPricingData($data,$szSericeCode,$idCourierProviderProductid,$idCourierAgreement); 
                
                $TotalNetCharge = $priceResponseArys['fTotalPrice'];
 
                $iTransitDays=$iTransitDays;
                //$szTransitDays = getBusinessDaysForCourierDeliveryDate($dtShippingDate, $iTransitDays)." ".$dtShipmentTime ;
                $szTransitDate = date('Y-m-d',strtotime($dtShippingDate . "+".$iTransitDays." days"));
                $szTransitDays = $szTransitDate." ".$dtShipmentTime;
                
                $dayNameTrasitDays=date('D',strtotime($szTransitDate));
                if($dayNameTrasitDays=='Sun' || $dayNameTrasitDays=='Sat')
                {
                   $szTransitDays = getBusinessDaysForCourierDeliveryDate($szTransitDate, 1)." ".$dtShipmentTime;
                }

                $szCarrierCompanyName = $courierProviderComanyAry[$data['idCourierProvider']]['szName'];  
                $szApiResponseLog .= "<br><br><strong> ".$data['szForwarderCompanyName']." - ".$szCarrierCompanyName." - ".$code." - ".$szCarrierAccounID." </strong>";

                $this->stepCounter1++;
                $szApiResponseLog .= "<br><u>API response:</u>";
                
                $szApiResponseLog .=" <br><br> Working On Sertive Type: ".$code; 
                if($bVatApplicable && ($data['idCourierProvider']==__TNT_API__ || $data['idCourierProvider']==__FEDEX_API__))
                {
                    $TotalNetChargeIncludingVat = $TotalNetCharge;
                    $TotalNetCharge = round((float)($TotalNetCharge / 1.25),4);
                    
                    $szApiResponseLog .=" <br> Shipment Price: ".$szCurrency." ".number_format((float)$TotalNetChargeIncludingVat,2);
                    $szApiResponseLog .=" <br> Shipment Price ExcludingVat: ".$szCurrency." ".number_format((float)$TotalNetCharge,2);
                }
                else
                {
                    $szApiResponseLog .=" <br> Shipment Price: ".$szCurrency." ".number_format((float)$TotalNetCharge,2);
                } 
                
                $szApiResponseLog .=" <br> Shipping date: ".$dtShippingDate; 
                $szApiResponseLog .=" <br> Delivery date: ".$szTransitDays; 
                $szApiResponseLog .=" <br> Transit Days: ".$iTransitDays;  
                $szApiResponseLog .=" <br> <br><u> Easypost Rate calculation: </u><br><br> " ;

                $fShipmentCost = $TotalNetCharge ;
                $szCurrencyCode = $szCurrency ;
                $bDonotCalculate = false;

                if($szCurrency==__TO_CURRENCY_CONVERSION__)
                {
                    $fShipmentAPIUSDCost = $fShipmentCost;
                    $fExchangeRate = 1;
                }
                else
                { 
                    $fExchangeRate = $kCourierService->getExchangeRateCourierPrice($szCurrency); 
                    if($kCourierService->iCurrencyCodeExists==1)
                    { 
                        if($fExchangeRate>0)
                        {
                            $fExchangeRate = 1/$fExchangeRate;
                            $fShipmentAPIUSDCost = $TotalNetCharge/$fExchangeRate; 
                        }
                        $szApiResponseLog .= "<br>Exchange rate available for ".$szCurrency.": Yes ";
                    }
                    else
                    { 
                        $szApiResponseLog .= "<br>Exchange rate available for ".$szCurrency.": No ";
                        $bDonotCalculate = true;
                    }
                }   
                if($bDonotCalculate)
                {
                    $szApiResponseLog .= "<br>Process ends... ";
                    continue;
                } 
                
                $TotalNetChargeUsd = $fShipmentAPIUSDCost;
                $szApiResponseLog .= "<br>Buy rate - ".$szCurrency." ".number_format((float)$TotalNetCharge,2);
                $szApiResponseLog .= "<br>ROE - ".round($fExchangeRate,4) ;
                $szApiResponseLog .= "<br>Base currency - USD ".$TotalNetChargeUsd ;  
                
                if($bAddressSwaped)
                {
                    $bAddressSwapTarrif = round_up((float)($fShipmentAPIUSDCost * .10),2);
                    if($bAddressSwapTarrif>15)
                    {
                        $bAddressSwapTarrif = 15;
                    }
                    $fShipmentAPIUSDCost = $fShipmentAPIUSDCost + $bAddressSwapTarrif;
                    $TotalNetChargeUsd = $fShipmentAPIUSDCost;
                    
                    $fShipmentCost = round((float)($fShipmentAPIUSDCost * $fExchangeRate),2); 
                    
                    $szApiResponseLog .=" <br><br> Import Control Tariff(USD): ".number_format((float)$bAddressSwapTarrif,2);
                    $szApiResponseLog .=" <br> Shipment Price After Import Control Tariff(USD): ".number_format((float)$fShipmentAPIUSDCost,2)."<br>";
                }

                $idForwarderCurrency = $data['idForwarderCurrency'];
                $miniMarkUpRate = $pricingArr[0]['fMinimumMarkup'];
                $fMarkupperShipment = $pricingArr[0]['fMarkupperShipment']; 

                $bNegtiveMarkupPercetage = false; 
                if($pricingArr[0]['fMarkupPercent']<0)
                {
                   $fMarkupPercetage = $pricingArr[0]['fMarkupPercent'] * (-1);
                   $markUpByRate = $fShipmentAPIUSDCost * .01 * $fMarkupPercetage; 
                   $markUpByRate = $markUpByRate * (-1);
                    
                   $bNegtiveMarkupPercetage = true;
                }
                else
                {
                    $markUpByRate = $fShipmentAPIUSDCost * .01 * $pricingArr[0]['fMarkupPercent']; 
                }  
                if($idForwarderCurrency==1)
                { 
                    $fMarkupperShipmentUSD = $fMarkupperShipment ;
                    $miniMarkUpRateUSD = $miniMarkUpRate ;
                    $szForwarderCurrency = 'USD';
                    $szAdditionalMarkupCurrency = 'USD';
                    $fForwarderExchangeRate = 1; 
                    $fApiPriceForwarderCurrency = $fShipmentAPIUSDCost;
                }
                else
                {
                    $resultAry = $kWarehouseSearch->getCurrencyDetails($idForwarderCurrency);	  
                    $szForwarderCurrency = $resultAry['szCurrency'];
                    $fForwarderExchangeRate = round((float)$resultAry['fUsdValue'],6);
                    $szAdditionalMarkupCurrency = $resultAry['szCurrency']; 
                    
                    $fMarkupperShipmentUSD = $fMarkupperShipment * $fForwarderExchangeRate ;
                    $miniMarkUpRateUSD = $miniMarkUpRate * $fForwarderExchangeRate ;  
                    
                    if($fForwarderExchangeRate>0)
                    {
                        $fApiPriceForwarderCurrency = $fShipmentAPIUSDCost/$fForwarderExchangeRate;
                    } 
                    else
                    {
                        $fApiPriceForwarderCurrency = 0;
                    }
                }
                //echo $miniMarkUpRateUSD."miniMarkUpRateUSD".$markUpByRate;
                
//                if($bNegtiveMarkupPercetage)
//                {
//                    if($miniMarkUpRateUSD<0)
//                    { 
//                        $miniMarkUpRateUSDPositive= (int)$miniMarkUpRateUSD * (-1);
//                        if($miniMarkUpRateUSDPositive<$markUpByRate)
//                        { 
//                            $fTotalMarkup = $miniMarkUpRateUSDPositive ;
//                            /*
//                            * If min rate applied then both values are same. As product mark-up pricing is always defined in forwarder currency
//                            */
//                            $fCourierPrductMarkupForwarderCurrency = $miniMarkUpRateUSDPositive;
//                        }
//                        else
//                        {
//                             //echo "Hello2";
//                             $fTotalMarkup =  $markUpByRate ;
//                             $fCourierPrductMarkupForwarderCurrency = round((float)($fTotalMarkup/$fForwarderExchangeRate),3);
//                        }
//                    }
//                    else
//                    {
//                        $fTotalMarkup = $miniMarkUpRateUSD ;
//                        $fCourierPrductMarkupForwarderCurrency = $miniMarkUpRate;
//                        $bNegtiveMarkupPercetage = false;
//                    }
//                }
//                else
//                {
                    if($miniMarkUpRateUSD>$markUpByRate)
                    {
                        $fTotalMarkup = $miniMarkUpRateUSD ;
                        /*
                        * If min rate applied then both values are same. As product mark-up pricing is always defined in forwarder currency
                        */
                        $fCourierPrductMarkupForwarderCurrency = $miniMarkUpRate;
                    }
                    else
                    {
                       $fTotalMarkup =  $markUpByRate ;
                       $fCourierPrductMarkupForwarderCurrency = round((float)($fTotalMarkup/$fForwarderExchangeRate),4);
                    }
                //}
                //echo " MK Perc: ".$pricingArr[0]['fMarkupPercent']." rate: ".$fTotalMarkup." Min: ".$miniMarkUpRate." Cost: ".$fShipmentAPIUSDCost;
                if($miniMarkUpRateUSD<0)
                {
                    $miniMarkUpRateUSDStr = "USD (".$miniMarkUpRateUSD * (-1).")";
                }
                else
                {
                    $miniMarkUpRateUSDStr ="USD ".$miniMarkUpRateUSD;
                }
                if($bNegtiveMarkupPercetage)
                {
                    $fPriceAfterMarkUp = $fShipmentAPIUSDCost + $fTotalMarkup ;
                    $szMarkPriceStr = "USD (".$fTotalMarkup.") ";
                    //$fTotalMarkup = $fTotalMarkup * (-1); 
                }
                else
                {
                    $fPriceAfterMarkUp = $fShipmentAPIUSDCost + $fTotalMarkup ;
                    $szMarkPriceStr = "USD ".$fTotalMarkup;
                }
                $szApiResponseLog .= "<br>Markup (".$pricingArr[0]['fMarkupPercent']."%, minimum ".$miniMarkUpRateUSDStr.") – ".$szMarkPriceStr;
                $szApiResponseLog .= "<br> Price After Mark-up – USD ".$fPriceAfterMarkUp ;
                $szApiResponseLog .= "<br> Additional fixed markup – USD ".$fMarkupperShipmentUSD ;

                $fBookingprice=0;
                $fLabelFeeForwarderCurrency = 0;
                if((int)$pricingArr[0]['iBookingIncluded']!=1)
                { 
                    if($data['iBookingPriceCurrency']==1)
                    {  
                        $iBookingPriceCurrency = 1;  
                        $iBookingPriceCurrencyROE = 1;
                        $szBookingPriceCurrency = 'USD';
                        $fBookingprice = $data['fPrice']; 
                    }
                    else
                    {
                        $resultAry = $kWarehouseSearch->getCurrencyDetails($data['iBookingPriceCurrency']); 
                        $iBookingPriceCurrencyROE = $resultAry['fUsdValue']; 
                        $szBookingPriceCurrency = $resultAry['szCurrency']; 
                        $iBookingPriceCurrency = $resultAry['idCurrency']; 
                        if($iBookingPriceCurrencyROE>0)
                        {
                            $fBookingprice = $data['fPrice']*$iBookingPriceCurrencyROE;
                        }
                        else
                        {
                            $fBookingprice = 0;
                        }
                    }  
                    $szApiResponseLog .= " <br> Booking and label fee for Transporteca: ".$szBookingPriceCurrency." ".$data['fPrice']."" ;
                    $szApiResponseLog .= " <br> Booking and label fee USD: ".$fBookingprice ;
                    $szApiResponseLog .= " <br> Booking and label fee exchange rate: ".$iBookingPriceCurrencyROE ;
                    
                    if($fForwarderExchangeRate>0)
                    {
                        $fLabelFeeForwarderCurrency = $fBookingprice/$fForwarderExchangeRate;
                    } 
                    else
                    {
                        $fLabelFeeForwarderCurrency = 0;
                    } 
                }  
                else
                {
                    $szApiResponseLog .= " <br> Booking and label fee for Transporteca: No ";
                } 
                /*
                * For self invoiced price we don't include Label fee, Handling Fee
                */
                $fTotalSelfInvoiceAmountUSD = $fPriceAfterMarkUp + $fMarkupperShipmentUSD;
                $fShipmentUSDCost = $fPriceAfterMarkUp + $fMarkupperShipmentUSD + $fBookingprice;  
                $fMarkUpRefferalFee=$fShipmentUSDCost*.01*$data['fReferalFee'];
                $fShipmentUSDCostForHandlingFee = $fPriceAfterMarkUp + $fMarkupperShipmentUSD;
 
                $bPrivateCustomerFeeApplied = false; 
                $szApiResponseLog = "<br><strong>H. Private Customer Fee </strong>";

                if($iPrivateShipping==1)
                {  
                    $szForwarderProduct = 'COURIER'; 
                    $privateCustomerFeeAry = array();
                    $privateCustomerFeeAry = $kForwarder_new->getAllPrivateCustomerPricing($data['idForwarder'],$szForwarderProduct,1);
 
                    if(!empty($privateCustomerFeeAry[$szForwarderProduct]))
                    {
                        $privateCustomerFeeArys = $privateCustomerFeeAry[$szForwarderProduct];
                        if($privateCustomerFeeArys['idCurrency']==1)
                        {
                            $idPrivateCustomerFeeCurrency = 1; //USD
                            $szPrivateCustomerFeeCurrency = "USD";
                            $fPrivateCustomerFeeExchangeRate = 1;

                            $fPrivateCustomerFee = $privateCustomerFeeArys['fCustomerFee'];
                            $fPrivateCustomerFeeUSD = $privateCustomerFeeArys['fCustomerFee'];
                        }
                        else
                        {
                            $privateCurrAry = array();
                            $privateCurrAry = $kWarehouseSearch->getCurrencyDetails($privateCustomerFeeArys['idCurrency']); 
                            $fPrivateCustomerFeeExchangeRate = $privateCurrAry['fUsdValue']; 
                            $szPrivateCustomerFeeCurrency = $privateCurrAry['szCurrency']; 
                            $idPrivateCustomerFeeCurrency = $privateCurrAry['idCurrency'];  

                            $fPrivateCustomerFee = $privateCustomerFeeArys['fCustomerFee'];

                            $fPrivateCustomerFeeUSD = $privateCustomerFeeArys['fCustomerFee'] * $fPrivateCustomerFeeExchangeRate;
                        } 
                        $bPrivateCustomerFeeApplied = true;  
                        $fShipmentUSDCost = $fShipmentUSDCost + $fPrivateCustomerFeeUSD;   
                        $fTotalSelfInvoiceAmountUSD = $fTotalSelfInvoiceAmountUSD + $fPrivateCustomerFeeUSD;
 
                        $szApiResponseLog .= "<br> Local Currency: ".$szPrivateCustomerFeeCurrency;
                        $szApiResponseLog .= "<br> Local ROE: ".$fPrivateCustomerFeeExchangeRate;
                        $szApiResponseLog .= " <br> Private customer fee for booking: USD ".$fPrivateCustomerFeeUSD;    
                    }  
                }
                else
                {
                    $szApiResponseLog .= " <br> Private customer fee applied: No ";
                }
                $bHandlingFeeApplied = false;
                $handlingFeeAry = array(); 
                if(!empty($vogaHandlingFeeAry) && ($iSearchMiniVersion==7 || $iSearchMiniVersion==8 || $iSearchMiniVersion==9))
                {
                    $key = $data['idForwarder']."_".$data['idCourierProvider']."_".$idCourierProviderProductid;   
                    if(!empty($vogaHandlingFeeAry['courier'][$key]))
                    {
                        $handlingFeeAry = $vogaHandlingFeeAry['courier'][$key];
                        $bHandlingFeeApplied = true;
                    }
                } 
                
                if(!empty($handlingFeeAry))
                {  
                    $szApiResponseLog .= " <br> Handling Price Applied: Yes ";
                    $szApiResponseLog .= " <br> Sales price calculated for Handling Fee: USD ".$fShipmentUSDCostForHandlingFee; 
                     
                    if($handlingFeeAry['idHandlingCurrency']==1)
                    {
                        $idHandlingCurrency = $handlingFeeAry['idHandlingCurrency'];
                        $szHandlingCurrencyName = $handlingFeeAry['szHandlingCurrencyName'];
                        $fHandlingCurrencyExchangeRate = $handlingFeeAry['fHandlingCurrencyExchangeRate'];
                        
                        $fHandlingFeePerBooking = $handlingFeeAry['fHandlingFeePerBooking'];
                    }
                    else
                    {
                        $resultAry = $kWarehouseSearch->getCurrencyDetails($handlingFeeAry['idHandlingCurrency']); 
                        $fHandlingCurrencyExchangeRate = $resultAry['fUsdValue']; 
                        $szHandlingCurrencyName = $resultAry['szCurrency']; 
                        $idHandlingCurrency = $resultAry['idCurrency'];  
                        
                        $fHandlingFeePerBooking = $handlingFeeAry['fHandlingFeePerBooking'] * $fHandlingCurrencyExchangeRate;
                    } 
                    if($handlingFeeAry['idHandlingMinMarkupCurrency']==1)
                    {
                        $idHandlingMinMarkupCurrency = $handlingFeeAry['idHandlingMinMarkupCurrency'];
                        $szHandlingMinMarkupCurrencyName = $handlingFeeAry['szHandlingMinMarkupCurrencyName'];
                        $fHandlingMinMarkupCurrencyExchangeRate = $handlingFeeAry['fHandlingMinMarkupCurrencyExchangeRate']; 
                        $fHandlingMinMarkupPrice = $handlingFeeAry['fHandlingMinMarkupPrice'];
                    }
                    else
                    {
                        $resultAry = $kWarehouseSearch->getCurrencyDetails($handlingFeeAry['idHandlingMinMarkupCurrency']); 
                        $fHandlingMinMarkupCurrencyExchangeRate = $resultAry['fUsdValue']; 
                        $szHandlingMinMarkupCurrencyName = $resultAry['szCurrency']; 
                        $idHandlingMinMarkupCurrency = $resultAry['idCurrency'];  
                        
                        $fHandlingMinMarkupPrice = $handlingFeeAry['fHandlingMinMarkupPrice'] * $fHandlingMinMarkupCurrencyExchangeRate;
                    }  
                    $fHandlingMarkupPercentage = $handlingFeeAry['fHandlingMarkupPercentage'];
                    $fHandlingMarkupPercentageValue = $fShipmentUSDCostForHandlingFee * $handlingFeeAry['fHandlingMarkupPercentage'] * 0.01;
                    
                    if($fHandlingMarkupPercentageValue>$fHandlingMinMarkupPrice)
                    {
                        $fHandlingMarkupFeeUsd = $fHandlingMarkupPercentageValue;
                    }
                    else
                    {
                        $fHandlingMarkupFeeUsd = $fHandlingMinMarkupPrice;
                    }
                    $szApiResponseLog .= " <br> Handling fee pre booking: USD ".$fHandlingFeePerBooking;                    
                    $szApiResponseLog .= "<br>Handling Markup (".$handlingFeeAry['fHandlingMarkupPercentage']."%, minimum USD ".$fHandlingMinMarkupPrice.") – USD ".$fHandlingMarkupFeeUsd;
                    
                    $fTotalHandlingFeeUSD = $fHandlingFeePerBooking + $fHandlingMarkupFeeUsd;
                    
                    $fShipmentUSDCost = $fShipmentUSDCost+ $fTotalHandlingFeeUSD;
                    $szApiResponseLog .= " <br> Sales price for customer: USD ".$fShipmentUSDCost;  
                }
                else
                {
                    $szApiResponseLog .= " <br> Handling Price Applied: No ";
                }
                
                $bManualFeeApplicable = false;
                if($data['iQuickQuote']==1 && $data['iQuickQuoteTryitout']!=1)
                { 
                    $szApiResponseLog .= " <br> Manual Fee Applied: Yes ";
                    $szApiResponseLog .= " <br> Sales price excluding Manual fee: USD ".number_format((float)$fShipmentUSDCost);
                    $manualFeeSearchAry = array();
                    $manualFeeSearchAry['idForwarder'] = $idForwarder; 
                    $manualFeeSearchAry['szProduct'] = __MANUAL_FEE_COURIER__; 
                    $manualFeeSearchAry['szCargo'] = $data['szCargoType'];
                    $kVatApplication = new cVatApplication();
                    $manualFeeAry = array();
                    $manualFeeAry = $kVatApplication->getManualFeeListing(false, false, $manualFeeSearchAry);
                    $manualFeeAry = $manualFeeAry[0];
                    
                    if(empty($manualFeeAry))
                    {
                        /*
                        * If There is no Manual fee defined for the Forwarder then we calculate based on All other forwarder manual fee rates
                        */
                        $manualFeeSearchAry = array();
                        $manualFeeSearchAry['iAllForwarderFlag'] = '1'; 
                        $manualFeeSearchAry['szProduct'] = __MANUAL_FEE_COURIER__; 
                        $manualFeeSearchAry['szCargo'] = $data['szCargoType'];
                        $manualFeeAry = $kVatApplication->getManualFeeListing(false, false, $manualFeeSearchAry);
                        $manualFeeAry = $manualFeeAry[0];
                    }
                    
                    if(empty($manualFeeAry))
                    {
                        /*
                        * If There is no Manual fee defined for the Trade then we calculate based on default manual fee rates
                        */
                        $manualFeeAry = $kVatApplication->getDefaultManualFee();
                    } 
                    if(!empty($manualFeeAry))
                    {
                        $bManualFeeApplicable = true;
                        $idManualFeeCurrency = $manualFeeAry['idCurrency'];
                        $fManualFee = $manualFeeAry['fManualFee'];
                        $szApiResponseLog .= " <br> Manual fee: ".$manualFeeAry['szCurrency']." ".number_format((float)$fManualFee,2);
                        if($idManualFeeCurrency==1)
                        {
                            $fTotalManualFeeUSD = $fManualFee;
                            $fManualFeeExchangeRates = 1;
                            $szManualFeeCurrency = 'USD';
                        }
                        else
                        {
                            $resultAry = $kWarehouseSearch->getCurrencyDetails($idManualFeeCurrency); 
                            $fManualFeeExchangeRates = $resultAry['fUsdValue']; 
                            $szManualFeeCurrency = $resultAry['szCurrency'];  

                            $fTotalManualFeeUSD = $fManualFee * $fManualFeeExchangeRates;
                        }
                    }
                    else
                    {
                        $szApiResponseLog .= " <br> There is no Manual fee defined for the trade.";
                    }
                }
                else
                {
                    $szApiResponseLog .= " <br> Manual Fee Applied: No ";
                }
                
                
                if($data['iProfitType']==2)
                { 
                    $subTotalForRefferalFee=$fShipmentUSDCost-$fBookingprice;
                    $fMarkUpRefferalFee = ((0.01 * $data['fReferalFee'])/( 1 - (0.01*$data['fReferalFee'])) ) * $subTotalForRefferalFee;
                    $fShipmentUSDCost = $fShipmentUSDCost + $fMarkUpRefferalFee ;
                    $szApiResponseLog .= " <br> GP Type: Mark-up ";
                    $szApiResponseLog .= " <br> GP Amount: USD ".($fMarkUpRefferalFee+$fBookingprice);
                }
                else
                {
                    $subTotalForRefferalFee=$fShipmentUSDCost-$fBookingprice;
                    $fMarkUpRefferalFee = $subTotalForRefferalFee*.01*$data['fReferalFee'];
                    $szApiResponseLog .= " <br> GP Type: Referral ";
                    $szApiResponseLog .= " <br> GP Amount: USD ".($fMarkUpRefferalFee+$fBookingprice);
                } 

                $szApiResponseLog .= "<br> Total sales price without VAT: USD ".number_format((float)$fShipmentUSDCost,2)."<br><br>";

                $serviceType =$szServiceType;
                if($iNumDaysAddedToDelivery>0)
                {
                    $deliveryDate = date('Y-m-d H:i:s',strtotime($szTransitDays . "+".$iNumDaysAddedToDelivery." days"));

                    $timeStamp=date('H:i:s',strtotime($deliveryDate));
                    $dayName=date('D',strtotime($deliveryDate));

                    if($dayName=='Sun' || $dayName=='Sat')
                    {
                       $deliveryDate = getBusinessDaysForCourierDeliveryDate($deliveryDate, 1)." ".$timeStamp;
                    }
                }
                else
                {
                    $deliveryDate = date('Y-m-d H:i:s',strtotime($szTransitDays)); 
                }  

                $szApiResponseLog .= "<br>Delivery date ".$deliveryDate;

                if((int)$_SESSION['user_id']>0)
                {
                    $kUser = new cUser();
                    $kUser->getUserDetails($_SESSION['user_id']); 
                    $idCustomerCurrency = $kUser->szCurrency;
                } 
                else
                {
                    $idCustomerCurrency = $data['idCustomerCurrency'];
                }
                
                if($idCustomerCurrency==1)
                {
                    $fVatRateAmount = round((float)$fShipmentUSDCost)*0.01*$fVatRate;   
                    $fShipmentVatUSDCost = $fShipmentUSDCost+$fVatRateAmount;

                    $szApiResponseLog .= "<br>VAT Percentage:".$fVatRate."%"; 
                    $szApiResponseLog .= "<br>VAT Amount: USD ".round($fVatRateAmount,2); 
                }
                else
                {
                    $szApiResponseLog .= "<br>VAT Percentage:".$fVatRate."%";
                    $resultAry = $kWarehouseSearch->getCurrencyDetails($idCustomerCurrency); 
                    $iVatPriceCurrencyROE = $resultAry['fUsdValue']; 
                    $szVatPriceCurrency = $resultAry['szCurrency']; 
                    $iVatPriceCurrency = $resultAry['idCurrency']; 
                    if($iVatPriceCurrencyROE>0)
                    {
                        $fVatCalAmuount = $fShipmentUSDCost/$iVatPriceCurrencyROE;
                    }
                    else
                    {
                        $fVatCalAmuount = 0;
                    }

                    $fVatRateAmount=$fVatCalAmuount*0.01*$fVatRate;

                    $fVatRateAmountUSD=$fVatRateAmount*$iVatPriceCurrencyROE;
                    $fShipmentVatUSDCost=$fShipmentUSDCost+$fVatRateAmountUSD;
                   // echo $fShipmentAPIUSDCost;
                    $szApiResponseLog .= "<br>VAT Amount: USD ".round($fVatRateAmountUSD,2); 
                } 
                $szApiResponseLog .= "<br> Total sales price including VAT: USD ".number_format((float)$fShipmentVatUSDCost,2);
                
                $ret_ary[$ctr]['szCarrierCompanyName'] = $szCarrierCompanyName;
                $ret_ary[$ctr]['szCarrierServiceName'] = $szSericeDescription; 
                $ret_ary[$ctr]['idCourierProviderProduct'] = $idCourierProviderProductid;
                $ret_ary[$ctr]['dtAvailableDate'] = $deliveryDate ;
                $ret_ary[$ctr]['szCurrencyCode'] = $szCurrencyCode ;
                $ret_ary[$ctr]['fShippingAmount'] = $fShipmentCost ;
                $ret_ary[$ctr]['szServiceType'] =  $code ; 
                $ret_ary[$ctr]['fShipmentAPIUSDCost'] =  round($fShipmentAPIUSDCost,4) ; 
                $ret_ary[$ctr]['fShipmentUSDCost'] =  round($fShipmentUSDCost,4) ; 
                $ret_ary[$ctr]['fDisplayPrice'] =  round($fShipmentCustomerCost,4) ; 
                $ret_ary[$ctr]['szCurrency'] =  $szCurrency ;
                $ret_ary[$ctr]['idCurrency'] =  $idCurrency ;  
                $ret_ary[$ctr]['markUpByRate'] =  $fTotalMarkup ; 
                $ret_ary[$ctr]['miniMarkUpRate'] = $miniMarkUpRate;
                $ret_ary[$ctr]['fPriceAfterMarkUp']=$fPriceAfterMarkUp;   
                $ret_ary[$ctr]['fMarkupperShipment']=$fMarkupperShipment; 
                $ret_ary[$ctr]['fMarkUpRefferalFee']=$fMarkUpRefferalFee;
                $ret_ary[$ctr]['fBookingprice']=$fBookingprice;
                $ret_ary[$ctr]['fReferalPercentage'] = $data['fReferalFee'] ; 
                $ret_ary[$ctr]['idForwarderCurrency'] = $idForwarderCurrency;
                $ret_ary[$ctr]['idProviderProduct'] = $idCourierProviderProductid;
                $ret_ary[$ctr]['idCourierAgreement'] = $idCourierAgreement; 
                $ret_ary[$ctr]['iBookingIncluded'] = $pricingArr[0]['iBookingIncluded'];  
                $ret_ary[$ctr]['szCarrierAccounID'] = $priceResponseArys['szCarrierAccounID'];
                $ret_ary[$ctr]['szEasypostShipmentID'] = $priceResponseArys['szEasypostShipmentID'];
                $ret_ary[$ctr]['szEasyPostRateID'] = $priceResponseArys['szEasyPostRateID'];
                $ret_ary[$ctr]['szBookingLabelPriceCurrency'] = $szBookingPriceCurrency; 
                $ret_ary[$ctr]['szAdditionalMarkupCurrency'] = $szAdditionalMarkupCurrency;
                $ret_ary[$ctr]['fCourierPrductMarkupForwarderCurrency'] = $fCourierPrductMarkupForwarderCurrency; 
                $ret_ary[$ctr]['fApiPriceForwarderCurrency'] = $fApiPriceForwarderCurrency;
                $ret_ary[$ctr]['fLabelFeeForwarderCurrency'] = $fLabelFeeForwarderCurrency;
                $ret_ary[$ctr]['fTotalSelfInvoiceAmountUSD'] =  round($fTotalSelfInvoiceAmountUSD,4) ; 
                 
                if($bPrivateCustomerFeeApplied)
                {
                    $ret_ary[$ctr]['iPrivateCustomerFeeApplicable'] = 1;
                    $ret_ary[$ctr]['idPrivateCustomerFeeCurrency'] = $idPrivateCustomerFeeCurrency;   
                    $ret_ary[$ctr]['szPrivateCustomerFeeCurrency'] = $szPrivateCustomerFeeCurrency;
                    $ret_ary[$ctr]['fPrivateCustomerFeeExchangeRate'] = $fPrivateCustomerFeeExchangeRate;  
                    $ret_ary[$ctr]['fPrivateCustomerFee'] = $fPrivateCustomerFee;  
                    $ret_ary[$ctr]['fPrivateCustomerFeeUSD'] = $fPrivateCustomerFeeUSD; 
                }
                else
                {
                    $ret_ary[$ctr]['iPrivateCustomerFeeApplicable'] = 0;
                }
                
                if($bHandlingFeeApplied)
                {
                    $ret_ary[$ctr]['iHandlingFeeApplicable'] = 1;
                    $ret_ary[$ctr]['iProductType'] = 2; //Courier
                    $ret_ary[$ctr]['idHandlingCurrency'] = $idHandlingCurrency;
                    $ret_ary[$ctr]['szHandlingCurrencyName'] = $szHandlingCurrencyName;
                    $ret_ary[$ctr]['fHandlingCurrencyExchangeRate'] = $fHandlingCurrencyExchangeRate; 
                    $ret_ary[$ctr]['fHandlingMarkupPercentage'] = $fHandlingMarkupPercentage;  
                    $ret_ary[$ctr]['idHandlingMinMarkupCurrency'] = $idHandlingMinMarkupCurrency;
                    $ret_ary[$ctr]['szHandlingMinMarkupCurrencyName'] = $szHandlingMinMarkupCurrencyName;
                    $ret_ary[$ctr]['fHandlingMinMarkupCurrencyExchangeRate'] = $fHandlingMinMarkupCurrencyExchangeRate;
                    $ret_ary[$ctr]['fHandlingMinMarkupPrice'] = $fHandlingMinMarkupPrice; 
                    $ret_ary[$ctr]['fHandlingFeePerBooking'] = $fHandlingFeePerBooking;  
                    $ret_ary[$ctr]['fTotalHandlingFeeUSD'] = $fTotalHandlingFeeUSD;  
                }
                else
                {
                    $ret_ary[$ctr]['iHandlingFeeApplicable'] = 0;
                } 
                if($iPrivateShipping==1)
                {
                    $ret_ary[$ctr]['szCustomerType'] = "Private"; 
                }
                else
                {
                    $ret_ary[$ctr]['szCustomerType'] = "Business"; 
                }
                if($bManualFeeApplicable)
                {
                    $ret_ary[$ctr]['iManualFeeApplicable'] = 1; 
                    $ret_ary[$ctr]['idManualFeeCurrency'] = $idManualFeeCurrency;
                    $ret_ary[$ctr]['szManualFeeCurrency'] = $szManualFeeCurrency;
                    $ret_ary[$ctr]['fManualFeeExchangeRates'] = $fManualFeeExchangeRates; 
                    $ret_ary[$ctr]['fTotalManualFeeUSD'] = $fTotalManualFeeUSD;
                    $ret_ary[$ctr]['fTotalManualFee'] = $fManualFee;
                }
                ++$ctr; 
            }
        }
        $this->szApiResponseLog .= $szApiResponseLog;  
        return $ret_ary;
    }
    
    function createOriginAddressObject($data,$bCheckAuth=false)
    {  
        if($bCheckAuth)
        {
            /*
            *  While checking authentication we check with hardcoded address value. 
            */
            // create addresses
            $from_address_params = array( 
                "postal_code"     => "2100",
                "country" => "DNK" 
            ); 
            return $from_address_params;
        } 
        else
        {  
            $kConfigNewCD= new cConfig();
            $kConfigNewCD->loadCountry(false,$data['szCountry']);            
            if($kConfigNewCD->szDefaultCourierCity!='' || $kConfigNewCD->szDefaultCourierPostcode!='')
            {
                $data['szCity']=$kConfigNewCD->szDefaultCourierCity;
                $data['szPostCode']=$kConfigNewCD->szDefaultCourierPostcode;
            }
            $bExceptionalTradeFlag = false;
            if($data['szCountry']=='GB' && $data['szSCountry']!='DK')
            {
                $bExceptionalTradeFlag = true;
            }
            if($data['szCountry']=='GB' && $data['idCourierAgreement']==self::$standaradAgreementId && $bExceptionalTradeFlag)
            {
                // create addresses
                $from_address_params = array( 
                    "postal_code"     => "2100",
                    "country" => "DK" 
                );  
            }
            else if($data['szCountry']=='IE' && $data['idCourierProvider']==__TNT_API__)
            {
                // create addresses
                $from_address_params = array(
                    "city"     => $data['szCity'],
                    "country" => $data['szCountryISO3Code'] 
                );  
            }
            else
            {
                // create addresses
                $from_address_params = array( 
                    "postal_code"     => $data['szPostCode'],
                    "country" => $data['szCountryISO3Code'] 
                );  

                if($data['szCountry']=='US')
                { 
                    $stateSearchAry = array();
                    $stateSearchAry = $from_address_params;
                    $stateSearchAry['city'] = $data['szCity']; 

                    $szState = $this->getUsaStates($stateSearchAry);
                    $from_address_params['state'] = $szState; 
                }
                else if(strtoupper($data['szPostCode'])=='CO12 4QG' || strtoupper($data['szPostCode']) == 'CO124QG' || strtoupper($data['szPostCode']) == '2850' || strtoupper($data['szPostCode']) == '43124')
                {
                    if(strtoupper($data['szPostCode']) == '2850' && $data['szCountry']=='DK')
                    {
                        $from_address_params['city'] = 'Nærum';
                    }
                    else if(strtoupper($data['szPostCode']) == '43124' && $data['szCountry']=='IT')
                    {
                        $from_address_params['city'] = 'Parma';
                    }
                    else
                    {
                        $from_address_params['city'] = $data['szCity'];
                    } 
                } 
            } 
            $this->szApiResponseInformationText .= "<br><strong>Searching for: ".print_R($from_address_params,true)."</strong>";
            return $from_address_params;
        } 
    }
    
    function createDestinationAddressObject($data,$bCheckAuth=false)
    {
        if($bCheckAuth)
        {
            /*
            *  While checking authentication we check with hardcoded address value.
            */
            // create addresses
            $to_address_params = array(  
               "postal_code"     => "111 22",
               "country" => "SWE" 
           );
           return $to_address_params;
        } 
        else
        {
            $kConfigNewSCD= new cConfig();
            $kConfigNewSCD->loadCountry(false,$data['szSCountry']);            
            if($kConfigNewSCD->szDefaultCourierCity!='' || $kConfigNewSCD->szDefaultCourierPostcode!='')
            {
                $data['szSCity']=$kConfigNewSCD->szDefaultCourierCity;
                $data['szSPostCode']=$kConfigNewSCD->szDefaultCourierPostcode;
            }
            
            $bExceptionalTradeFlag = false;
            if($data['szSCountry']=='GB' && $data['szCountry']!='DK')
            {
                $bExceptionalTradeFlag = true;
            }
            if($data['szSCountry']=='GB' && $data['idCourierAgreement']==self::$standaradAgreementId && $bExceptionalTradeFlag)
            {
                // create addresses
                $to_address_params = array( 
                    "postal_code"     => "2100",
                    "country" => "DK" 
                );  
            }
            else if($data['szSCountry']=='IE' && $data['idCourierProvider']==__TNT_API__)
            {
                // create addresses
                $to_address_params = array(
                    "city"     => $data['szSCity'],
                    "country" => $data['szSCountryISO3Code'] 
                );  
            }
            else
            {
                // create addresses
                $to_address_params = array( 
                   "postal_code"     => $data['szSPostCode'],
                   "country" => $data['szSCountryISO3Code'] 
                );

                if($data['szSCountry']=='US')
                { 
                    $stateSearchAry = array();
                    $stateSearchAry = $to_address_params;
                    $stateSearchAry['city'] = $data['szSCity']; 

                    $szState = $this->getUsaStates($stateSearchAry);
                    $to_address_params['state'] = $szState; 
                }
                else if(strtoupper($data['szSPostCode'])=='CO12 4QG' || strtoupper($data['szSPostCode']) == 'CO124QG' || strtoupper($data['szSPostCode']) == '2850' || strtoupper($data['szSPostCode']) == '43124')
                {
                    if(strtoupper($data['szSPostCode']) == '2850' && $data['szSCountry']=='DK')
                    {
                        $to_address_params['city'] = 'Nærum';
                    }
                    else if(strtoupper($data['szSPostCode']) == '43124' && $data['szSCountry']=='IT')
                    {
                        $to_address_params['city'] = 'Parma';
                    }
                    else
                    {
                        $to_address_params['city'] = $data['szSCity'];
                    }  
                }
            } 
            $this->szApiResponseInformationText .= "<br><strong>Searching for: ".print_R($to_address_params,true)."</strong>";
            return $to_address_params;
        } 
    }
    
    function createParcelObject($data,$bCheckAuth=false)
    { 
        if($bCheckAuth)
        { 
            $iLength = 25;
            $iWidth = 25;
            $iHeight = 25;
            $fWeight = 25;
            
            $parcel_params['parcel'][] = array(
                "length"             => $iLength,
                "width"              => $iWidth, 
                "height"             => $iHeight, 
                "weight"             => $fWeight
            );  
        }
        else
        {
            $idBooking =  $data['idBooking'];
            $bDefaultPackageFlag = true;
            $bPartnerPacking = false;
            if(cPartner::$bApiValidation && !empty(cPartner::$searchablePacketTypes) && (in_array(1, cPartner::$searchablePacketTypes) || in_array(2, cPartner::$searchablePacketTypes)))
            {
                $bPartnerPacking = true;
            }    
            if($idBooking>0 || $bPartnerPacking || $data['szSearchMode']=='FORWARDER_COURIER_TRY_IT_OUT')
            {
                $kBooking =new cBooking();
                if($bPartnerPacking)
                {  
                    $kPartner = new cPartner();
                    $bookingCargoAry = $kPartner->getCargoDeailsByPartnerApi();  
                }
                else if($data['szSearchMode']=='FORWARDER_COURIER_TRY_IT_OUT')
                {
                    $postSearchAry = $data;
                    $bookingCargoAry = cWHSSearch::$cForwarderTryitoutCargoDetails;
                }
                else
                {
                    $bookingCargoAry = $kBooking->getCargoDeailsByBookingId($idBooking,true); 
                    $postSearchAry = $kBooking->getBookingDetails($idBooking);
                }  
                $kWHSSearch = new cWHSSearch();
                if(!empty($bookingCargoAry) && ($bPartnerPacking || $postSearchAry['iCourierBooking']==1 || $postSearchAry['iSearchMiniVersion']==3 || $postSearchAry['iSearchMiniVersion']==4 || $postSearchAry['iSearchMiniVersion']==5 || $postSearchAry['iSearchMiniVersion']==6 || $postSearchAry['iSearchMiniVersion']==7 || $postSearchAry['iSearchMiniVersion']==8 || $postSearchAry['iSearchMiniVersion']==9 || $data['szSearchMode']=='FORWARDER_COURIER_TRY_IT_OUT'))
                {   
                    $ctr=1;
                    $iQuantity = 0;
                    $bDefaultPackageFlag = false;
                    foreach($bookingCargoAry as $bookingCargoArys)
                    {  
                        if($postSearchAry['iPalletType']>0 && (float)$bookingCargoArys['fHeight']<=0)
                        {
                            /*
                            * If shipment type is pallet and height is empty then we use standard height as per pallet type
                            */
                            if($postSearchAry['iPalletType']==1) //Euro Pallet
                            {
                                $bookingCargoArys['fHeight'] = __STANDARD_HEIGHT_EURO_PALLET__;
                            }
                            else if($postSearchAry['iPalletType']==2) //Half Pallet
                            {
                                $bookingCargoArys['fHeight'] = __STANDARD_HEIGHT_HALF_PALLET__;
                            }
                        }
                        if($bookingCargoArys['idWeightMeasure']==1)
                        {
                            $fKgFactor = 1;
                        }
                        else
                        { 
                            $fKgFactor = $kWHSSearch->getWeightFactor($bookingCargoArys['idWeightMeasure']); 
                        }

                        if($bookingCargoArys['idCargoMeasure']==1)
                        {
                            $fCmFactor = 1;
                        }
                        else
                        {
                            $fCmFactor = $kWHSSearch->getCargoFactor($bookingCargoArys['idCargoMeasure']);  
                        }

                        $iLenght = 0;
                        $iWidth = 0;
                        $iHeight = 0;
                        $fWeight = 0;

                        if($fCmFactor>0)
                        {
                            //Converting to Cm
                            $iLenghtCm = round($bookingCargoArys['fLength'] / $fCmFactor);
                            $iWidthCm = round($bookingCargoArys['fWidth'] / $fCmFactor);
                            $iHeightCm = round($bookingCargoArys['fHeight'] / $fCmFactor);

                            /*
                            * Coverting dimensions to INCHES, 1 Cm = 0.393701 Inch
                            */
                            $iLength = round_up(($iLenghtCm * 0.393701),1);
                            $iWidth = round_up(($iWidthCm * 0.393701),1);
                            $iHeight = round_up(($iHeightCm * 0.393701),1);
                        }

                        if($fKgFactor>0)
                        {
                            $fWeightKg = ceil(($bookingCargoArys['fWeight']/$fKgFactor)) ;

                            //Converting this OUNCES 1 Kg = 35.274 Ounce
                            $fWeight = round_up((35.274 * $fWeightKg),1);
                        }
                        $szCargoMeasureCode = 'CM';
                        $szCargoMeasure = 'Centimeters';

                        $szWeightMeasureCode = 'KGS';
                        $szWeightMeasure = 'Kgs';

                        $packageQuantity = $bookingCargoArys['iQuantity'];
                        $iTotalQuantity += $bookingCargoArys['iQuantity'];

                        for($i=0;$i<$packageQuantity;++$i)
                        { 
                            // create parcel
                            $parcel_params['parcel'][] = array(
                                "length"             => $iLenghtCm,
                                "width"              => $iWidthCm,
                                "height"             => $iHeightCm, 
                                "weight"             => $fWeightKg
                            ); 

                            $szCargoMeasure = "INCHES (IN)"; 
                            $szWeightMeasure = "OUNCES (OZ)";

                            $szPackageDetailsLogs .= "<br> Package: ".$iLenghtCm." X ".$iWidthCm." X ".$iHeightCm." cm, ".$fWeightKg." kg, Count: 1" ;
                            //$szPackageDetailsLogs .= "<br> Package: ".$iLength." X ".$iWidth." X ".$iHeight." ".$szCargoMeasure. ", ".$fWeight." ".$szWeightMeasure.", Count: 1 <br> " ;
                        } 
                    }   
                    $szPackageDetailsLogs .= "<br><br> Total package: ".$iTotalQuantity; 
                } 
            }

            if($bDefaultPackageFlag)
            {  
                $kCourierServices = new cCourierServices();
                $productProviderArr = $kCourierServices->getProductProviderLimitations($data['idGroup'],true);  
                $bWeightLimitationDefined = false; 
                if(!empty($productProviderArr))
                {
                    foreach($productProviderArr as $productProviderArrs)
                    {
                        if($productProviderArrs['idCargoLimitationType']==__WEIGHT_PER_COLI__)
                        {
                            if($productProviderArrs['szRestriction']=='Maximum')
                            {
                                $maxWeightPerColli = $productProviderArrs['szLimit']; 
                                $bWeightLimitationDefined = true;
                            }  
                        } 
                        if($productProviderArrs['idCargoLimitationType']==__CHARGEABLE_WEIGHT_PER_COLI__)
                        {
                            if($productProviderArrs['szRestriction']=='Maximum')
                            {
                                $maxChargeableWeight = $productProviderArrs['szLimit']; 
                                $bWeightLimitationDefined = true;
                            } 
                        }
                    }
                }

                $changeFlag=false; 
                $fCargoWeight = $data['fCargoWeight'];
                $fCargoVolume = $data['fCargoVolume']; 
                $cargeAbleFlag=false;

                $fRmaingChargeableWeight = 0;
                $fRmaingWeight = 0;

                $fCargoActualWeight = $data['fCargoWeight'];

                if(($fCargoVolume*200)>$fCargoWeight)
                {
                    $cargeAbleFlag=true;
                    $fCargoWeight = $data['fCargoVolume']*200 ; 
                }   
                $fCargoChargeableWeight = $fCargoWeight; 
                if($bWeightLimitationDefined)
                { 
                    $changeFlag=true;
                    $iTotalNumActulalColli=1;
                    if($maxWeightPerColli>0.00)
                    {
                        $fRmaingWeight = (float)($fCargoActualWeight % $maxWeightPerColli);
                        $iTotalNumActulalColli = (int)($fCargoActualWeight / $maxWeightPerColli);
                        $t=0;
                        if((int)$fRmaingWeight>0)
                        {
                            $iTotalNumActulalColli = $iTotalNumActulalColli + 1;
                        } 
                    }
                    $iTotalChargeNumColli=1;
                    if($maxChargeableWeight>0.00)
                    {                    
                        $fRmaingChargeableWeight = (float)($fCargoChargeableWeight % $maxChargeableWeight);
                        $iTotalChargeNumColli = (int)($fCargoChargeableWeight / $maxChargeableWeight);
                        $t=0;
                        if((int)$fRmaingChargeableWeight>0)
                        {
                            $iTotalChargeNumColli = $iTotalChargeNumColli + 1;
                        } 
                    }  
                    $fRmaingWeight = 0;  
                    if($iTotalChargeNumColli>=$iTotalNumActulalColli)
                    { 
                        $cargoWeight=$fCargoChargeableWeight/$iTotalChargeNumColli;
                        $iTotalNumColli=$iTotalChargeNumColli;
                        $fRmaingWeight = $fCargoChargeableWeight - ($cargoWeight*$iTotalNumColli);
                    }
                    else if($iTotalChargeNumColli<$iTotalNumActulalColli)
                    {
                        $cargoWeight=$fCargoActualWeight/$iTotalNumActulalColli;
                        $iTotalNumColli=$iTotalNumActulalColli;
                        $fRmaingWeight = $fCargoActualWeight - ($cargoWeight*$iTotalNumColli);
                    }
                    else
                    {
                        $changeFlag=true;
                        $iTotalNumColli = 1;
                        $cargoWeight = $fCargoWeight;
                        $fRmaingWeight = 0;
                    } 
                }
                else
                {
                    $changeFlag=true;
                    $iTotalNumColli = 1;
                    $cargoWeight = $fCargoWeight;
                    $fRmaingWeight = 0;
                }
 
                $totalPackage = $iTotalNumColli;  
                if($changeFlag)
                { 
                    if((float)$fRmaingWeight>0.00)
                    {
                        $totalMaxPackage = $iTotalNumColli-1;
                    }
                    else
                    {
                        $totalMaxPackage = $iTotalNumColli;
                    } 

                    if($iTotalNumColli>0)
                    {
                        $fCargoVolume = $fCargoVolume/$iTotalNumColli ;
                    }

                    /*
                    * Coverting dimensions to Centimeters(CM)
                    */
                    $fCalculativeValue = pow($fCargoVolume, 0.333333);
                    $iLengthCm = round((100*$fCalculativeValue),2);
                    $iWidthCm = $iLengthCm;
                    $iHeightCm = $iLengthCm;
                    $iLenghtCm = $iLengthCm ;
                    /*
                    * Coverting dimensions to INCHES, 1 Cm = 0.393701 Inch
                    
                    $iLength = round_up(($iLengthCm * 0.393701),1);
                    $iWidth = round_up(($iWidthCm * 0.393701),1);
                    $iHeight = round_up(($iHeightCm * 0.393701),1);
                    $szCargoMeasure = "INCHES (IN)"; 
                    $szWeightMeasure = "OUNCES (OZ)";
                    */
                    $t=0;
                    $szPackageDetailsLogs .= "<br> Number of Package: ".$totalMaxPackage.", Weight per package: ".$cargoWeight."<br>"; 
                    for($i=0;$i<$totalMaxPackage;++$i)
                    { 
                        $fWeightKg = $cargoWeight;
                        //$fWeight = 35.274 * $fWeightKg; 

                        // create parcel
                        $parcel_params['parcel'][] = array(
                            "length"             => $iLenghtCm,
                            "width"              => $iWidthCm,
                            "height"             => $iHeightCm, 
                            "weight"             => $fWeightKg
                        );  
                        $szPackageDetailsLogs .= "<br> Package: ".$iLenghtCm." X ".$iWidthCm." X ".$iHeightCm." cm, ".$fWeightKg." kg, Count: 1" ;
                        //$szPackageDetailsLogs .= "<br> Package: ".$iLength." X ".$iWidth." X ".$iHeight." ".$szCargoMeasure. ", ".$fWeight." ".$szWeightMeasure.", Count: 1 <br> " ;
                        ++$t;
                    } 

                    if((float)$fRmaingWeight>0.00)
                    {
                        $fWeightKg = $fRmaingWeight;
                        $fWeight = 35.274 * $fWeightKg;  

                        // create parcel
                        $parcel_params['parcel'][] = array(
                            "length"             => $iLength,
                            "width"              => $iWidth,
                            "height"             => $iHeight, 
                            "weight"             => $fWeight
                        );  
                        $szPackageDetailsLogs .= "<br> Package: ".$iLenghtCm." X ".$iWidthCm." X ".$iHeightCm." cm, ".$fWeightKg." kg, Count: 1" ;
                        $szPackageDetailsLogs .= "<br> Package: ".$iLength." X ".$iWidth." X ".$iHeight." ".$szCargoMeasure. ", ".$fWeight." ".$szWeightMeasure.", Count: 1 <br> " ; 
                    }
                }  
                $szPackageDetailsLogs .= "<br><br> Total package: ".$iTotalNumColli; 
            }
            $this->szPackageDetailsLogs = $szPackageDetailsLogs;
            $this->szPackageDetailsLogs .= "<br><br> <u>Shipper </u><br> ";
            $this->szPackageDetailsLogs .= "Address: ".$data['szAddress1'];
            $this->szPackageDetailsLogs .= "<br>Postcode: ".$data['szPostCode'];
            $this->szPackageDetailsLogs .= "<br>City: ".$data['szCity'];
            $this->szPackageDetailsLogs .= "<br>Country: ".$data['szCountry'];

            $this->szPackageDetailsLogs .= "<br><br> <u>Consignee </u><br> ";
            $this->szPackageDetailsLogs .= "Address: ".$data['szSAddress1'];
            $this->szPackageDetailsLogs .= "<br>Postcode: ".$data['szSPostCode'];
            $this->szPackageDetailsLogs .= "<br>City: ".$data['szSCity'];
            $this->szPackageDetailsLogs .= "<br>Country: ".$data['szSCountry'];  
        }
        
        $parcelAry = array();
        $ctr=0;
        if(!empty($parcel_params['parcel']))
        {
            foreach($parcel_params['parcel'] as $parcels)
            {
                $parcelAry[$ctr]['description'] = 'Goods';
                $parcelAry[$ctr]['box_type'] = 'custom';

                $weightAry = array();
                $weightAry['value'] = $parcels['weight'];
                $weightAry['unit'] = "kg";

                $dimensionAry = array();
                $dimensionAry['width'] = $parcels['width'];
                $dimensionAry['height'] = $parcels['height'];
                $dimensionAry['depth'] = $parcels['length'];
                $dimensionAry['unit'] = "cm";

                $itemsAry = array();
                $itemsAry['description'] = 'Goods';
                $itemsAry['origin_country'] = $data['szCountryISO3Code'];
                $itemsAry['quantity'] = 1; 
                $itemsAry['price']['amount'] = 1;
                $itemsAry['price']['currency'] = "USD"; 
                $itemsAry['weight']['value'] = 0.6;
                $itemsAry['weight']['unit'] = "kg"; 

                $parcelAry[$ctr]['weight'] = $weightAry;
                $parcelAry[$ctr]['dimension'] = $dimensionAry;
                $parcelAry[$ctr]['items'][] = $itemsAry;
                $ctr++;
            }
        } 
        return $parcelAry;
    } 
}
?>