<?php
/**
 * This file is the container for all easy post api related functionality. 
 *
 * easyPost.class.php
 *
 * @copyright Copyright (C) 2016 Transporteca
 * @author Ajay
 * @package Transporteca Development
 */
if(!isset($_SESSION))
{
    session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_INC__ . "/functions.php" );
require_once( __APP_PATH_INC__ . "/courier_functions.php" );
require_once( __APP_PATH_CLASSES__ . "/database.class.php");
require_once( __APP_PATH__ . "/inc/classes/parser.class.php" );
require_once(__APP_PATH_INC__."/lib/easypost.php");

class cEasyPost extends cDatabase
{  
    public static $globalEasyPostApiResponse = array();
    public static $globalEasyPostApiResponseTNT = array();
    public static $globalEasyPostApiResponseStandardAgreement = array();
    public static $globalEasyPostApiResponseStandardAgreementTNT = array();
    public static $carrierAccountIdAry = array(); 
    public static $tntCarrierAccountIdAry = array(); 
    public static $otherCarrierAccountIdAry = array();
    
    public static $iEasyPostApiCalled = false;
    public static $iEasyPostApiCalledTNT = false;
    public static $iEasyPostApiCalledStandardAgreementTNT = false;
    
    public static $standaradAgreementId = __STANDARD_AGREEMENT_ID__;
    
    function __construct()
    { 
        parent::__construct();
        return true;
    } 
    
    var $t_base_courier='management/providerProduct/'; 
    var $t_base_services = "SERVICEOFFERING/"; 
    var $t_base_courier_label ="COURIERLABEL/";
       
    function getCourierDataEasypostApi($data,$bCheckAuth=false,$bCreateLabel=false)
    {  
        if($bCheckAuth)
        {
            /*
            *  When we checking authentication we need to check passed Carrier Account ID is valid or not.
            *  We check authentication with hardcoded values that are described below.
            */
        } 
        
        if(!empty($data['dtTimingDate']))
        { 
            $dtShippingDate = $data['dtTimingDate'];
        }
        else
        { 
            $dtShippingDate = date('c');
        }
        $kWhsSearch = new cWHSSearch();
        $szEasypostApi = $kWhsSearch->getManageMentVariableByDescription('__EASYPOST_API_KEY__');
        \EasyPost\EasyPost::setApiKey($szEasypostApi);  
        //\EasyPost\EasyPost::setApiBase('https://easypost-vm:5000/v2');
           
        if($bCreateLabel)
        {  
            $filename = __APP_PATH_ROOT__."/logs/create_label_".date('Ymd').".log";
              
            $idBooking = $data['idBooking'];
            $kBooking = new cBooking();
            $bookingDetailsAry = $kBooking->getExtendedBookingDetails($idBooking); 
            
            $from_address = $this->createOriginAddressObject($bookingDetailsAry,$bCheckAuth,$bCreateLabel);
            $to_address = $this->createDestinationAddressObject($bookingDetailsAry,$bCheckAuth,$bCreateLabel);  
            $customInfo = $this->createCustomInforObject($bookingDetailsAry,true);    
            $parcel = $this->createParcelObject($data,$bCheckAuth,$bCreateLabel);   
               
            if($data['idCourierProvider']==__UPS_API__)
            {
                if($data['szCountry']=='DK')
                {
                    //Shipper will pay Label fee
                    // create shipment
                    $shipment_params = array(
                       "from_address" => $from_address,
                       "to_address"   =>  $to_address 
                    );
                }
                else  
                {  
                    if($data['szSCountry']=='DK')
                    {
                        $shipment_params = array(
                            "from_address" => $to_address,
                            "to_address"   => $from_address  
                        );
                        $shipment_params['is_return'] = true;    
                    }
                    else
                    {
                        $shipment_params = array(
                            "from_address" => $from_address,
                            "to_address"   =>  $to_address 
                        ); 
                    } 
                    //Cosignee will pay label fee
                    //$shipment_params['options'] = array('bill_third_party_account' => 'A12E28','bill_third_party_country'=>'DK','bill_third_party_postal_code'=>'2100');
                }  
            }
            else
            {
                $shipment_params = array(
                    "from_address" => $from_address,
                    "to_address"   =>  $to_address 
                ); 
                
                if($data['idCourierProvider']==__TNT_API__ && $data['szSCountry']=='DK')
                {
                    $shipment_params['is_return'] = true; 
                }
                //$shipment_params['options'] = array('bill_receiver_account' => '2013579'); 
            }
            
            $shipment_params = array_merge($shipment_params,$parcel); 
            $shipment_params['customs_info'] = $customInfo;   
            
            $strdata=array();
            $strdata[0]=" \n\n *** Shipment Params: \n\n".print_R($shipment_params,true)."\n\n"; 
            file_log(true, $strdata, $filename); 
            $iWebServiceType = 9;
        }
        else
        {
            $from_address = $this->createOriginAddressObject($data,$bCheckAuth);
            $to_address = $this->createDestinationAddressObject($data,$bCheckAuth);  
            $parcel = $this->createParcelObject($data,$bCheckAuth);   
            //$customInfo = $this->createCustomInforObject($bookingDetailsAry);
              
            $szLogCarrierIdString .= "FROM ADDRESS: <br>".print_R($from_address,true);
            $szLogCarrierIdString .= "<br><br> TO ADDRESS: <br>".print_R($to_address,true)."<br><br>";
            /*
             * when we call the Easypost API for rates for TNT, when the "To Country" = Denmark, then we perform following steps
             * 1. We swap shipper and consignee details in the API call, 
             * 2. The rate we get back, after cleaning for VAT, we add MIN (10% * rate ; USD 15).

                So if you have a search from Denmark to Italy, then normal process will follows as we do today.
                But if you have a search from Italy to Denmark, for TNT we call the rate from Denmark to Italy, and the response could be e.g. USD 200 - and we then clean this for VAT, so the Shipment Price ExcludingVat = USD 160. To that price we add 10% or maximum USD 15. So the new Shipment Price ExcludingVat is USD 175 (as 160+10%=176 which is more than 160+15=175). 
             */ 
            
            /*
             * The hardcoded UK solution, so it does not apply when people soeach UK to Denmark or Denmark to UK.
             */
            $bExceptionalTradeFlag = false;
            if($data['szSCountry']=='GB' && $data['szCountry']!='DK')
            {
                $bExceptionalTradeFlag = true;
            }
            $bAddressSwaped = false;
            $bEligibleSwapping = false;
            if($data['szSCountry']=='DK' || $data['szSCountry']=='SE')
            {
                $bEligibleSwapping = true;
            }
            else if($data['szSCountry']=='GB' && $data['idCourierAgreement']==self::$standaradAgreementId && $bExceptionalTradeFlag)
            {
                $this->szApiResponseInformationText .= "<br><strong>Eligible for special treatment :)</strong>";
                $bEligibleSwapping = true;
            } 
            if(!$bCheckAuth && $bEligibleSwapping && $data['idCourierProvider']==__TNT_API__)
            { 
                /*
                * Swapping shipper/consignee address
                */
                $szFromAddress = $from_address;
                $from_address = $to_address;
                $to_address = $szFromAddress;
                $bAddressSwaped = true;
                
                //$this->szApiResponseLog .= "<br>Address swapped for search: <br> From: <br>".print_R($from_address,true)." <br><br>To: <br> ".print_R($to_address,true)."<br>";
            }
            
            // create shipment
            $shipment_params = array(
               "from_address" => $from_address,
               "to_address"   => $to_address,
               "options" => array('currency' => 'DKK')
            );      
            $shipment_params = array_merge($shipment_params,$parcel);  
            
            if($data['szSCountry']=='DK')
            {
                if($data['idCourierProvider']==__TNT_API__)
                {
                    if(!empty(self::$tntCarrierAccountIdAry))
                    {
                        //$shipment_params['carrier_accounts'] = self::$tntCarrierAccountIdAry;
                      // $shipment_params['carrier_accounts'] = 'ca_5cbd1184e44447ddaf4926fcdb524635'; 
                    } 
                }
                else if(!empty(self::$otherCarrierAccountIdAry))
                {
                    //$shipment_params['carrier_accounts'] = self::$otherCarrierAccountIdAry;
                    //$shipment_params['carrier_accounts'] = 'ca_d50a146a286749f19704e664f7fdc3f1';
                }
            }
            else if(!empty(self::$carrierAccountIdAry))
            {
                //$shipment_params['carrier_accounts'] = self::$carrierAccountIdAry;
                //$shipment_params['carrier_accounts'] = array("id"=>"ca_5cbd1184e44447ddaf4926fcdb524635","id"=>"ca_d50a146a286749f19704e664f7fdc3f1");
            }
            if($bAddressSwaped)
            {
                $iWebServiceType = 102; //This means Address has been swapped 
            }
            else
            {
                $iWebServiceType = 101; 
            } 
        } 
        try
        {  
            //constinue to work on rates
            $shipment = \EasyPost\Order::create($shipment_params);    
            $kObject = new \EasyPost\Object();  
            $kObject->_values = $shipment; 
            $responseAry = array();
            $responseAry = $kObject->__toArray(true); 
            $szResponseLogsAry = $responseAry['_values']['shipments']; 
            $szResponseSerializedData = "";
            if(!empty($szResponseLogsAry))
            {
                $szResponseSerializedData = serialize($szResponseLogsAry);
            }
        } 
        catch(Exception $ex)
        { 
            $szResponseLogsAry = $ex; 
        }    
        
        $szReferenceToken = cPartner::$szGlobalToken; 
        
        $fedexApiLogsAry = array(); 
        $fedexApiLogsAry['idBooking'] = $data['id'] ;
        $fedexApiLogsAry['szReferenceToken'] = $szReferenceToken ;
        $fedexApiLogsAry['iWebServiceType'] = $iWebServiceType ;
        $fedexApiLogsAry['szRequestData'] = print_R($shipment_params,true);
        $fedexApiLogsAry['szResponseData'] = print_R($szResponseLogsAry,true);
        $fedexApiLogsAry['szResponseSerializedData'] = $szResponseSerializedData;
        $kWebServices = new cWebServices();
        $kWebServices->addFedexApiLogs($fedexApiLogsAry); 
        
        if(!empty($responseAry['_values']['shipments']))
        {  
            $responseMessageAry = array();
            $responseMessageAry = $responseAry['_values']['shipments'][0]['messages'] ;
            if(!empty($responseMessageAry))
            {
                $szErrorReturnStr =  '<h4>API Notification</h4><table class="format-2" border="1" style="width:90%">';
                $szErrorReturnStr .= '<tr><td style="width:15%;"><strong>Type</strong></td><td style="width:15%;"><strong>Carrier</strong></td><td style="width:70%;"><strong>Message</strong></td></tr>'; 
                foreach($responseMessageAry as $responseMessageArys)
                {
                    $szErrorReturnStr .= '<tr>';
                    $szErrorReturnStr .= '<td>'.$responseMessageArys['type'].' </td>';
                    $szErrorReturnStr .= '<td>'.$responseMessageArys['carrier'].'</td>';
                    $szErrorReturnStr .= '<td>'.$responseMessageArys['message'].'</td>'; 
                    $szErrorReturnStr .= '</tr>';
                }
            } 
            $szErrorReturnStr .= "</table>"; 
            $this->szApiResponseInformationText = $szErrorReturnStr;
            $this->iSuccessMessage = 2 ;
        } 
        if(count($shipment->rates) > 0)
        {
            $kObject->_values = $shipment->rates ;
            $apiResponseAry = array();
            $responseAry = array();
            $responseAry = $kObject->__toArray(true);

            $apiResponseAry = $responseAry['_values']; 
           
            if($bCreateLabel)
            {    
                $szApiLogString .= 'We have found rates for the shipment. Now goin to create Shipment Label for the booking id: '.$idBooking."".PHP_EOL;
                if(!empty($apiResponseAry))
                {  
                    $ctr = 0;
                    foreach($apiResponseAry as $apiResponseArys)
                    { 
                        $szShippingNumber = $apiResponseArys['shipment_id']; 
                        if(!empty($szShippingNumber))
                        {
                            break;
                        }
                    }
                } 
                 
                $szApiLogString .= 'Creating label for Shipment ID: '.$szShippingNumber.PHP_EOL;
                if(!empty($szShippingNumber))
                {
                    try
                    {
                        $bHradCodedCheck = false;
                        if($bHradCodedCheck)
                        {
                            $szShippingLabelUrl = "http://easypostdev.s3.amazonaws.com/postage_labels/labels/20160205/e4ebf33379894aa3beb8c62a43c41ec5.pdf";
                            $uploadedPdfFileAry = $this->downloadLabelFromEasypost($szShippingLabelUrl); 
                            $returnAry['szLabelPdfPath'] = $uploadedPdfFileAry['szLabelPdfFilePath'];
                            $returnAry['szLabelPdfName'] = $uploadedPdfFileAry['szLabelPdfFileName'];
                            $returnAry['szTrackingNumber'] = "547438580";
                            $this->iSuccessMessage = 1; 
                            return $returnAry;
                        }
                        $szApiLogString .= 'Retrieving Shipment details for ID: '.$szShippingNumber.PHP_EOL;
                        /*
                        *  Retrieving Shipment details from Easypost API by using Shipment ID
                        */
                        $shipment = \EasyPost\Shipment::retrieve($szShippingNumber);
 
                        /*
                        *  Courier Service provider and product what was selected for the booking
                        */
                        if(empty($data['szName']))
                        {
                            $this->szApiResponseInformationText = " Courier agreement is either deleted or not exists for this booking";
                            $this->iSuccessMessage = 2 ; 
                            return false;
                        }
                        if(empty($data['szCourierAPICode']))
                        {
                            $this->szApiResponseInformationText = " Courier product is either deleted or not exists for this booking";
                            $this->iSuccessMessage = 2 ; 
                            return false;
                        }
                        
                        if($data['szName']=='TNT')
                        {
                            $szCarrierName = 'TNTExpress';
                        }
                        else
                        {
                            $szCarrierName = $data['szName'];
                        } 
                        $szCourierAPICode = $data['szCourierAPICode']; 
                          
                        $carriers[0] = $szCarrierName;
                        $services[0] = $szCourierAPICode;
                        
                        $strdata=array();
                        $strdata[0]=" \n\n *** Buying Label for Carrier: ".$carriers[0]." Service: ".$services[0]."\n\n"; 
                        file_log(true, $strdata, $filename);
                        
                        $szApiLogString .= 'Buying Label for Carrier: '.$carriers[0]." Service: ".$services[0].PHP_EOL;
                        /*
                        *  Finnally we are buying Order from Easypost API
                        */
                        $shipment->buy(array('rate' => $shipment->lowest_rate($carriers,$services)));
 
                        $strdata=array();
                        $strdata[0]=" \n\n *** Buy API Response:\n\n ".print_r($shipment,true)."\n\n"; 
                        file_log(true, $strdata, $filename);
                        
                        $szShippingNumber = $shipment->tracker->shipment_id;
                        $szTrackingNumber = $shipment->tracker->tracking_code; 
                        $szLabelCreatedLogStr = 'Shipment label created successfully with Shipment ID: '.$szShippingNumber." and Tracking: ".$szTrackingNumber.PHP_EOL;
                        $szApiLogString .= $szLabelCreatedLogStr;
                        
                        if(!empty($szTrackingNumber))
                        {
                            $strdata=array();
                            $strdata[0]=" \n\n *** ".$szLabelCreatedLogStr."\n\n"; 
                            file_log(true, $strdata, $filename);
                        
                            $returnAry = array();
                            $returnAry['szTrackingNumber'] = $szTrackingNumber;

                            $shipment = \EasyPost\Shipment::retrieve($szShippingNumber);
                            $szApiLogString .= 'Retrieving PDF label from Easypost API With Shipment ID: '.$szShippingNumber." and Tracking: ".$szTrackingNumber.PHP_EOL;
                            $shipment->label(array('file_format' => 'pdf')); 
 
                            $strdata=array();
                            $strdata[0]=" \n\n *** Retrieving PDF Label from API Response: ".print_R($shipment,true)."\n\n"; 
                            file_log(true, $strdata, $filename);
                            
                            if(!empty($shipment->postage_label->label_pdf_url))
                            { 
                                //Here is Url for pdf file
                                $this->szShippingLabelUrl = $shipment->postage_label->label_pdf_url; 
                                $szApiLogString .= 'Successfully Retrieved PDF label from Easypost API. Pdf label url is: '.$this->szShippingLabelUrl.PHP_EOL;

                                $strdata=array();
                                $strdata[0]=" \n\n *** PDF Label Successfully retrieved with URL: ".$this->szShippingLabelUrl."\n\n"; 
                                file_log(true, $strdata, $filename);
                            
                                /*
                                * Downloading pdf file from Easypost API and putting that on our server
                                */ 
                                $szApiLogString .= 'Downloading pdf file from Easypost url '.$this->szShippingLabelUrl.PHP_EOL;
                                $uploadedPdfFileAry = $this->downloadLabelFromEasypost($this->szShippingLabelUrl);
                                  
                                if(!empty($uploadedPdfFileAry))
                                {
                                    $szDownloadedStr = 'Label has been successfully downloaded and stored in temp folder at '.$uploadedPdfFileAry['szLabelPdfFilePath'].PHP_EOL;
                                    $strdata=array();
                                    $strdata[0]=" \n\n ".$szDownloadedStr."\n\n"; 
                                    file_log(true, $strdata, $filename);
                                
                                    $szApiLogString .= $szDownloadedStr;
                                    $returnAry['szLabelPdfPath'] = $uploadedPdfFileAry['szLabelPdfFilePath'];
                                    $returnAry['szLabelPdfName'] = $uploadedPdfFileAry['szLabelPdfFileName'];
                                }
                            } 
                        } 
                        $this->iSuccessMessage = 1; 
                    }
                    catch (Exception $ex) 
                    { 
                        $szResponseLogsAry = $ex;
                        $szErrorMessageStr = $ex->prettyPrint(false);  
                        $this->szApiResponseInformationText = " <strong> ERROR: ".$szErrorMessageStr." </strong>";
                        $this->iSuccessMessage = 2 ; 
                    }
                }
                else
                {
                    $this->szApiResponseInformationText = " <strong> ERROR: </strong> <br> Unable to create label from Easypost API. No response from API ";
                    $this->iSuccessMessage = 2 ; 
                } 
                
                $fedexApiLogsAry = array(); 
                $fedexApiLogsAry['iWebServiceType'] = 10;
                $fedexApiLogsAry['szRequestData'] = print_R($shipment_params,true);
                $fedexApiLogsAry['szResponseData'] = print_R($szResponseLogsAry,true);
                $fedexApiLogsAry['szDeveloperNotes'] = $szApiLogString;
                $kWebServices = new cWebServices();
                $kWebServices->addFedexApiLogs($fedexApiLogsAry);
                return $returnAry; 
            }
            else
            {
                $TntPriceResponseAry = array();
                $carrierResponseAry = array();

                $szLogCarrierIdString .= "<br> Found Response for Following Carrier IDs: ";
                if(!empty($apiResponseAry))
                {  
                    $ctr = 0;
                    foreach($apiResponseAry as $apiResponseArys)
                    { 
                        $szCarrierAccountId = trim($apiResponseArys['carrier_account_id']); 
                        if($bCheckAuth)
                        {
                            $TntPriceResponseAry[] = $szCarrierAccountId;
                            $carrierResponseAry[$szCarrierAccountId] = $apiResponseArys['carrier'];
                        } 
                        else
                        {
                            $carrierResponseAry[$szCarrierAccountId][$ctr] = $apiResponseArys;
                            $carrierResponseAry[$szCarrierAccountId][$ctr]['dtShippingDate'] = $dtShippingDate; 
                            $ctr++;
                            $szLogCarrierIdString .= "<br>ID: ".$szCarrierAccountId ;
                        }
                    } 
                    if($bCheckAuth)
                    {
                        return $TntPriceResponseAry;
                    }
                    else
                    {
                        if($bAddressSwaped && $data['idCourierProvider']==__TNT_API__)
                        { 
                            self::$globalEasyPostApiResponseTNT = $carrierResponseAry; 
                        }
                        else
                        {
                            self::$globalEasyPostApiResponse = $carrierResponseAry; 
                        } 
                    }  
                    $this->iSuccessMessage = 1 ;
                    $this->szFoundedCarrierAccountIDs = $szLogCarrierIdString;
                    return $carrierResponseAry;
                }
                else
                {
                    $this->szApiResponseInformationText .= " <strong> ERROR: </strong> <br> No Response From API ";
                    $this->iSuccessMessage = 2 ;
                    return true;
                }
            } 
        } 
        else
        {
            $this->szApiResponseInformationText .= " <strong> ERROR: </strong> <br> No Response From API ";
            $this->iSuccessMessage = 2 ;
            return true;
        }
    } 
    
    public function getAllCarrierResponseFromEasypostAPI($data)
    { 
        $kCourierServices = new cCourierServices();   
        $kCourierServices->validateInputNewDetails($data); 
        $this->szPackageDetailsLogs = '';

        if($kCourierServices->error==true)
        {  
            return false;
        } 
        
        if($data['szAPICode']!='')
        {
            $validServiceTypeArr = explode(";",$data['szAPICode']);
        }  

        $validApiCodeAry = array();
        $ctr_1=0;
        if(!empty($validServiceTypeArr))
        {
            foreach($validServiceTypeArr as $validServiceTypeArrs)
            {
                if(!empty($validServiceTypeArrs))
                {
                    $validServiceTypeAry = explode("||||",$validServiceTypeArrs);
                    $validApiCodeAry[$ctr_1] = $validServiceTypeAry[0]; 

                    $validProviderIDAry[$validServiceTypeAry[0]]['idCourierProduct'][$ctr_1] = $validServiceTypeAry[1]; 
                    $validProviderIDAry[$validServiceTypeAry[0]]['idCourierAgreement'][$ctr_1] = $validServiceTypeAry[2];  
                    $ctr_1++;
                }
            }
        } 
        $apiCodeValuesAry = array();
        $apiCodeValuesAry = array_count_values($validApiCodeAry);
        $validServiceTypeArr = $validApiCodeAry ;
        $szCarrierAccountID = trim($data['szCarrierAccountID']); 
        if(empty($szCarrierAccountID))
        {
            $this->szApiResponseLog .= "<br> There is no carrier account id passed for this service <br> ". $data['szForwarderCompanyName']." - ".$szCarrierCompanyName." - ".$szCarrierAccounID;;
            return false;
        }  
        $apiResponseAry = array();
        $bAddressSwaped = false;
        $bExceptionalTradeFlag = false;
        if($data['szSCountry']=='GB' && $data['szCountry']!='DK')
        {
            $bExceptionalTradeFlag = true;
        }
        $bEligibleSwapping = false;
        if($data['szSCountry']=='DK' || $data['szSCountry']=='SE')
        {
            $this->szApiResponseLog .= "<br>Address swapping applied for country: ".$data['szSCountry'];
            $bEligibleSwapping = true;
        }
        else if($data['szSCountry']=='GB' && $data['idCourierAgreement']==self::$standaradAgreementId && $bExceptionalTradeFlag)
        {
            $bEligibleSwapping = true;
        } 
        if($bEligibleSwapping && $data['idCourierProvider']==__TNT_API__)
        {
            /*
            * Swapping shipper/consignee address
            */ 
            $bAddressSwaped = true;
        }  
        /*
         * 1. If Destination country is Denmark/Swedon then we have made 2 API calls one For TNT with swapped address and one for UPS and Fedex with actual address. This process takes approx 20 to 22 seconds to get results back on /service/ page.
           2. If destination country is not Denmark then we have made only 1 API call with actual Address and this process takes approx 12 to 14 seconds to get results back on /service/ page.  
         */
        
        if($bAddressSwaped && $data['idCourierProvider']==__TNT_API__)
        {
            if($data['szSCountry']=='GB' && $data['idCourierAgreement']==self::$standaradAgreementId && $bExceptionalTradeFlag)
            {
                if(self::$iEasyPostApiCalledStandardAgreementTNT)
                {
                    $apiResponseAry = self::$globalEasyPostApiResponseStandardAgreementTNT;  
                }
                else
                {
                    $apiResponseAry = $this->getCourierDataEasypostApi($data); 
                    self::$iEasyPostApiCalledStandardAgreementTNT = 1;
                }
            }
            else
            {
                if(self::$iEasyPostApiCalledTNT)
                {
                    $apiResponseAry = self::$globalEasyPostApiResponseTNT;  
                }
                else
                {
                    $apiResponseAry = $this->getCourierDataEasypostApi($data); 
                    self::$iEasyPostApiCalledTNT = 1;
                }
            } 
        }
        else
        {
            if(self::$iEasyPostApiCalled)
            {
                $apiResponseAry = self::$globalEasyPostApiResponse;  
            }
            else
            {
                $apiResponseAry = $this->getCourierDataEasypostApi($data); 
                self::$iEasyPostApiCalled = 1;
            }
        } 
        
        if(!empty($apiResponseAry[$szCarrierAccountID]))
        {
            $apiResponseAry = $apiResponseAry[$szCarrierAccountID];
        }  
        
        $BookingFileName = __APP_PATH_ROOT__."/logs/easypostDatalogLatest1.log"; 
         
        if(!empty($apiResponseAry))
        {
            $tnt_ctr = 0;
            $TntPriceResponseAry = array(); 
            foreach($apiResponseAry as $apiResponseArys)
            { 
                $szSericeCode = $apiResponseArys['service'];  
                $szSericeDescription = $apiResponseArys['service'];   
                $TotalNetCharge = $apiResponseArys['rate']; 

                $code = $szSericeCode;    
                if(!empty($validServiceTypeArr) && in_array($szSericeCode,$validServiceTypeArr))
                {
                    $iLoopCounter = $apiCodeValuesAry[$code]; 
                    if(!empty($validProviderIDAry[$code]['idCourierProduct']))
                    { 
                        foreach($validProviderIDAry[$code]['idCourierProduct'] as $key=>$validProviderIDArys)
                        { 
                            $idCourierProduct = $validProviderIDArys;
                            $idCourierAgreement = $validProviderIDAry[$code]['idCourierAgreement'][$key];
                            if($idCourierProduct>0)
                            { 
                                $TntPriceResponseAry[$tnt_ctr]['szServiceCode'] = $szSericeCode;
                                $TntPriceResponseAry[$tnt_ctr]['szServiceCode'] = $szSericeCode;
                                $TntPriceResponseAry[$tnt_ctr]['szServiceDescription'] = $szSericeDescription;
                                $TntPriceResponseAry[$tnt_ctr]['fTotalPrice'] = $apiResponseArys['rate'];
                                $TntPriceResponseAry[$tnt_ctr]['idProviderProduct'] = $idCourierProduct;
                                $TntPriceResponseAry[$tnt_ctr]['idCourierAgreement'] = $idCourierAgreement;  
                                $TntPriceResponseAry[$tnt_ctr]['szCurrency'] = $apiResponseArys['currency'];
                                $TntPriceResponseAry[$tnt_ctr]['szCarrierAccounID'] = $apiResponseArys['carrier_account_id'];
                                $TntPriceResponseAry[$tnt_ctr]['szEasypostShipmentID'] = $apiResponseArys['shipment_id'];
                                $TntPriceResponseAry[$tnt_ctr]['szEasyPostRateID'] = $apiResponseArys['id'];
                                
                                $iTransitDays = $apiResponseArys['delivery_days'];
                                if(!empty($apiResponseArys['delivery_date']))
                                {
                                    $dtDeliveryDate = date('Y-m-d',strtotime($apiResponseArys['delivery_date']));
                                }
                                else if($apiResponseArys['delivery_days']>0)
                                {
                                    $dtDeliveryDate = date('Y-m-d',strtotime("+".(int)$apiResponseArys['delivery_days']." DAYS"));
                                }
                                $TntPriceResponseAry[$tnt_ctr]['dtDeliveryDate'] = $dtDeliveryDate;
                                $TntPriceResponseAry[$tnt_ctr]['iTransitDays'] = $iTransitDays;
                                $TntPriceResponseAry[$tnt_ctr]['dtShippingDate'] = $apiResponseArys['dtShippingDate'];
                                $tnt_ctr++;
                            }
                        }
                    } 
                }
            }
        }    
         
        $finalApiResponseAry = array();
        if(!empty($TntPriceResponseAry))
        { 
           
            $finalApiResponseAry = $this->processEasyPostApiResponse($data,$TntPriceResponseAry,$bAddressSwaped); 
            
            $this->szApiResponseLog .= $this->szFoundedCarrierAccountIDs;
            $this->szApiResponseLog .= "<br>Searched for Carrier Account ID: ".$szCarrierAccountID; 
        }
        return $finalApiResponseAry;
    }
            
    function processEasyPostApiResponse($data,$TntPriceResponseAry,$bAddressSwaped=false)
    {  
        if($TntPriceResponseAry)
        { 
            $kCourierService = new cCourierServices();
            $kWarehouseSearch = new cWHSSearch();
            $courierProductMapping = array();
            $courierProductMapping = $kCourierService->getAllProductByApiCodeMapping();
              
            $bVatApplicable = false;
            $kVatApplication = new cVatApplication();
            $idForwarderCountry = $data['idForwarderCountry'];
            $idForwarder = $data['idForwarder'];
            
            $referralFeeAry = array();
            $referralFeeAry['idForwarder'] = $idForwarder;
            $referralFeeAry['idTransportMode'] = __BOOKING_TRANSPORT_MODE_COURIER__;
            
            $kReferralPricing = new cReferralPricing();
            $fReferralFee = $kReferralPricing->getForwarderReferralFee($referralFeeAry);
            $data['fReferalFee'] = $fReferralFee;
            
            if((int)$_SESSION['user_id']>0)
            {
                $kUser = new cUser();
                $kUser->getUserDetails($_SESSION['user_id']); 
                $iPrivateShipping = $kUser->iPrivate; 
                $data['iPrivateShipping'] = $iPrivateShipping;
                $idCustomerAccountCountry = $kUser->szCountry;
            } 
            else
            {
                $idCustomerAccountCountry = $data['idDestinationCountry'];   
                $iPrivateShipping = $data['iPrivateShipping'];
            }
                
            
            $szVATRegistration='';
            if($iPrivateShipping==1)
            {                
                $vatInputAry = array();
                $vatInputAry['idCustomerCountry'] = $idCustomerAccountCountry;
                $vatInputAry['idOriginCountry'] = $data['idOriginCountry'];
                $vatInputAry['idDestinationCountry'] = $data['idDestinationCountry'];

                $fVatRate = $kVatApplication->getTransportecaVat($vatInputAry);
                if($fVatRate>0)
                {
                    $bVatApplicable = true;
                    $kConfig_new1 = new cConfig();
                    $kConfig_new1->loadCountry($bookingDataArr['szCustomerCountry']);
                    $szVATRegistration = $kConfig_new1->szVATRegistration;
                }
            } 
            $idLandingPage = $data['idLandingPage'];
            $iSearchMiniVersion = $data['iSearchMiniVersion'];
            $iCheckHandlingFeeForAutomaticQuote = (int)$data['iCheckHandlingFeeForAutomaticQuote'];
             
            $vogaHandlingFeeAry = array();
            if(($iSearchMiniVersion==7 || $iSearchMiniVersion==8 || $iSearchMiniVersion==9 || $iCheckHandlingFeeForAutomaticQuote==1) && $idLandingPage>0)
            { 
                $kExplain = new cExplain();
                $iApiCall=false;
                if($iCheckHandlingFeeForAutomaticQuote==1)
                {
                    $iApiCall=true;
                }
                $vogaHandlingFeeAry = $kExplain->getStandardQuotePriceByPageID($idLandingPage,false,false,true,$iApiCall);
            }   
            $tnt_ctr=0;
            $kCourierServices = new cCourierServices();
            $courierProviderComanyAry = array();
            $courierProviderComanyAry = $kCourierServices->getCourierProviderDetails();
            
            
            
            $kForwarder_new = new cForwarder();
            $stepCounter1 = 1;
            foreach($TntPriceResponseAry as $priceResponseArys)
            { 
                
                $iTransitDays = 0;   
                $szSericeCode = $priceResponseArys['szServiceCode'];  
                $szSericeDescription = $priceResponseArys['szServiceDescription'];   
                $idCourierProviderProductid = $priceResponseArys['idProviderProduct']; 
                $szCurrency = $priceResponseArys['szCurrency']; 
                $dtDeliveryDate = $priceResponseArys['dtDeliveryDate']; 
                $dtShippingDate = $priceResponseArys['dtShippingDate'];
                $szCarrierAccounID = $priceResponseArys['szCarrierAccounID'];
                $idCourierAgreement = $priceResponseArys['idCourierAgreement'];
                if(__isWeekend($dtShippingDate))
                {
                    $dtShippingDate = __nextMonday($dtShippingDate);
                }
                //echo "<br> Calculation for Agr: ".$idCourierAgreement." Prod: ".$idCourierProviderProductid;
                if(trim($szCurrency)=='RMB')
                {
                    $szCurrency = 'CNY';
                }
                $code = $szSericeCode;  
                $iTransitDays = $priceResponseArys['iTransitDays'];
                $szApiResponseLog .="<br>Transit Days Api Response - ".$iTransitDays." Days";
                
                $iDaysMin =(int)$courierProductMapping[$code]['iDaysMin']; 
                if($iTransitDays<=0)
                {
                    $iTransitDays = $courierProductMapping[$code]['iDaysStd']; 
                }              
                $szApiResponseLog .="<br>TransitDays Before Min Check - ".$iTransitDays ."Days";
                if($iTransitDays<$iDaysMin)
                {
                    $iTransitDays = $iDaysMin; 
                }
                $szApiResponseLog .="<br>TransitDays After Min Check - ".$iTransitDays ."Days";
                $pricingArr = $kCourierService->getCourierProductProviderPricingData($data,$szSericeCode,$idCourierProviderProductid,$idCourierAgreement); 
                
                $TotalNetCharge = $priceResponseArys['fTotalPrice'];
 
                $iTransitDays=$iTransitDays;
                //$szTransitDays = getBusinessDaysForCourierDeliveryDate($dtShippingDate, $iTransitDays)." ".$dtShipmentTime ;
                $szTransitDate = date('Y-m-d',strtotime($dtShippingDate . "+".$iTransitDays." days"));
                $szTransitDays = $szTransitDate." ".$dtShipmentTime;
                
                $dayNameTrasitDays=date('D',strtotime($szTransitDate));
                if($dayNameTrasitDays=='Sun' || $dayNameTrasitDays=='Sat')
                {
                   $szTransitDays = getBusinessDaysForCourierDeliveryDate($szTransitDate, 1)." ".$dtShipmentTime;
                }

                $szCarrierCompanyName = $courierProviderComanyAry[$data['idCourierProvider']]['szName'];  
                $szApiResponseLog .= "<br><br><strong> ".$data['szForwarderCompanyName']." - ".$szCarrierCompanyName." - ".$code." - ".$szCarrierAccounID." </strong>";

                $this->stepCounter1++;
                $szApiResponseLog .= "<br><u>API response:</u>";
                
                $szApiResponseLog .=" <br><br> Working On Sertive Type: ".$code; 
                if($bVatApplicable && ($data['idCourierProvider']==__TNT_API__ || $data['idCourierProvider']==__FEDEX_API__))
                {
                    $TotalNetChargeIncludingVat = $TotalNetCharge;
                    $TotalNetCharge = round((float)($TotalNetCharge / 1.25),4);
                    
                    $szApiResponseLog .=" <br> Shipment Price: ".$szCurrency." ".number_format((float)$TotalNetChargeIncludingVat,2);
                    $szApiResponseLog .=" <br> Shipment Price ExcludingVat: ".$szCurrency." ".number_format((float)$TotalNetCharge,2);
                }
                else
                {
                    $szApiResponseLog .=" <br> Shipment Price: ".$szCurrency." ".number_format((float)$TotalNetCharge,2);
                } 
                
                $szApiResponseLog .=" <br> Shipping date: ".$dtShippingDate; 
                $szApiResponseLog .=" <br> Delivery date: ".$szTransitDays; 
                $szApiResponseLog .=" <br> Transit Days: ".$iTransitDays;  
                $szApiResponseLog .=" <br> <br><u> Easypost Rate calculation: </u><br><br> " ;

                $fShipmentCost = $TotalNetCharge ;
                $szCurrencyCode = $szCurrency ;
                $bDonotCalculate = false;

                if($szCurrency==__TO_CURRENCY_CONVERSION__)
                {
                    $fShipmentAPIUSDCost = $fShipmentCost;
                    $fExchangeRate = 1;
                }
                else
                { 
                    $fExchangeRate = $kCourierService->getExchangeRateCourierPrice($szCurrency); 
                    if($kCourierService->iCurrencyCodeExists==1)
                    { 
                        if($fExchangeRate>0)
                        {
                            $fExchangeRate = 1/$fExchangeRate;
                            $fShipmentAPIUSDCost = $TotalNetCharge/$fExchangeRate; 
                        }
                        $szApiResponseLog .= "<br>Exchange rate available for ".$szCurrency.": Yes ";
                    }
                    else
                    { 
                        $szApiResponseLog .= "<br>Exchange rate available for ".$szCurrency.": No ";
                        $bDonotCalculate = true;
                    }
                }   
                if($bDonotCalculate)
                {
                    $szApiResponseLog .= "<br>Process ends... ";
                    continue;
                } 
                
                $TotalNetChargeUsd = $fShipmentAPIUSDCost;
                $szApiResponseLog .= "<br>Buy rate - ".$szCurrency." ".number_format((float)$TotalNetCharge,2);
                $szApiResponseLog .= "<br>ROE - ".round($fExchangeRate,4) ;
                $szApiResponseLog .= "<br>Base currency - USD ".$TotalNetChargeUsd ;  
                
                if($bAddressSwaped)
                {
                    $bAddressSwapTarrif = round_up((float)($fShipmentAPIUSDCost * .10),2);
                    if($bAddressSwapTarrif>15)
                    {
                        $bAddressSwapTarrif = 15;
                    }
                    $fShipmentAPIUSDCost = $fShipmentAPIUSDCost + $bAddressSwapTarrif;
                    $TotalNetChargeUsd = $fShipmentAPIUSDCost;
                    
                    $fShipmentCost = round((float)($fShipmentAPIUSDCost * $fExchangeRate),2); 
                    
                    $szApiResponseLog .=" <br><br> Import Control Tariff(USD): ".number_format((float)$bAddressSwapTarrif,2);
                    $szApiResponseLog .=" <br> Shipment Price After Import Control Tariff(USD): ".number_format((float)$fShipmentAPIUSDCost,2)."<br>";
                }

                $idForwarderCurrency = $data['idForwarderCurrency'];
                $miniMarkUpRate = $pricingArr[0]['fMinimumMarkup'];
                $fMarkupperShipment = $pricingArr[0]['fMarkupperShipment']; 

                $bNegtiveMarkupPercetage = false; 
                if($pricingArr[0]['fMarkupPercent']<0)
                {
                   $fMarkupPercetage = $pricingArr[0]['fMarkupPercent'] * (-1);
                   $markUpByRate = $fShipmentAPIUSDCost * .01 * $fMarkupPercetage; 
                   $markUpByRate = $markUpByRate * (-1);
                    
                   $bNegtiveMarkupPercetage = true;
                }
                else
                {
                    $markUpByRate = $fShipmentAPIUSDCost * .01 * $pricingArr[0]['fMarkupPercent']; 
                }  
                if($idForwarderCurrency==1)
                { 
                    $fMarkupperShipmentUSD = $fMarkupperShipment ;
                    $miniMarkUpRateUSD = $miniMarkUpRate ;
                    $szForwarderCurrency = 'USD';
                    $szAdditionalMarkupCurrency = 'USD';
                    $fForwarderExchangeRate = 1; 
                    $fApiPriceForwarderCurrency = $fShipmentAPIUSDCost;
                }
                else
                {
                    $resultAry = $kWarehouseSearch->getCurrencyDetails($idForwarderCurrency);	  
                    $szForwarderCurrency = $resultAry['szCurrency'];
                    $fForwarderExchangeRate = round((float)$resultAry['fUsdValue'],6);
                    $szAdditionalMarkupCurrency = $resultAry['szCurrency']; 
                    
                    $fMarkupperShipmentUSD = $fMarkupperShipment * $fForwarderExchangeRate ;
                    $miniMarkUpRateUSD = $miniMarkUpRate * $fForwarderExchangeRate ;  
                    
                    if($fForwarderExchangeRate>0)
                    {
                        $fApiPriceForwarderCurrency = $fShipmentAPIUSDCost/$fForwarderExchangeRate;
                    } 
                    else
                    {
                        $fApiPriceForwarderCurrency = 0;
                    }
                }
                //echo $miniMarkUpRateUSD."miniMarkUpRateUSD".$markUpByRate;
                
//                if($bNegtiveMarkupPercetage)
//                {
//                    if($miniMarkUpRateUSD<0)
//                    { 
//                        $miniMarkUpRateUSDPositive= (int)$miniMarkUpRateUSD * (-1);
//                        if($miniMarkUpRateUSDPositive<$markUpByRate)
//                        { 
//                            $fTotalMarkup = $miniMarkUpRateUSDPositive ;
//                            /*
//                            * If min rate applied then both values are same. As product mark-up pricing is always defined in forwarder currency
//                            */
//                            $fCourierPrductMarkupForwarderCurrency = $miniMarkUpRateUSDPositive;
//                        }
//                        else
//                        {
//                             //echo "Hello2";
//                             $fTotalMarkup =  $markUpByRate ;
//                             $fCourierPrductMarkupForwarderCurrency = round((float)($fTotalMarkup/$fForwarderExchangeRate),3);
//                        }
//                    }
//                    else
//                    {
//                        $fTotalMarkup = $miniMarkUpRateUSD ;
//                        $fCourierPrductMarkupForwarderCurrency = $miniMarkUpRate;
//                        $bNegtiveMarkupPercetage = false;
//                    }
//                }
//                else
//                {
                    if($miniMarkUpRateUSD>$markUpByRate)
                    {
                        $fTotalMarkup = $miniMarkUpRateUSD ;
                        /*
                        * If min rate applied then both values are same. As product mark-up pricing is always defined in forwarder currency
                        */
                        $fCourierPrductMarkupForwarderCurrency = $miniMarkUpRate;
                    }
                    else
                    {
                       $fTotalMarkup =  $markUpByRate ;
                       $fCourierPrductMarkupForwarderCurrency = round((float)($fTotalMarkup/$fForwarderExchangeRate),4);
                    }
                //}
                //echo " MK Perc: ".$pricingArr[0]['fMarkupPercent']." rate: ".$fTotalMarkup." Min: ".$miniMarkUpRate." Cost: ".$fShipmentAPIUSDCost;
                if($miniMarkUpRateUSD<0)
                {
                    $miniMarkUpRateUSDStr = "USD (".$miniMarkUpRateUSD * (-1).")";
                }
                else
                {
                    $miniMarkUpRateUSDStr ="USD ".$miniMarkUpRateUSD;
                }
                if($bNegtiveMarkupPercetage)
                {
                    $fPriceAfterMarkUp = $fShipmentAPIUSDCost + $fTotalMarkup ;
                    $szMarkPriceStr = "USD (".$fTotalMarkup.") ";
                    //$fTotalMarkup = $fTotalMarkup * (-1); 
                }
                else
                {
                    $fPriceAfterMarkUp = $fShipmentAPIUSDCost + $fTotalMarkup ;
                    $szMarkPriceStr = "USD ".$fTotalMarkup;
                }
                $szApiResponseLog .= "<br>Markup (".$pricingArr[0]['fMarkupPercent']."%, minimum ".$miniMarkUpRateUSDStr.") – ".$szMarkPriceStr;
                $szApiResponseLog .= "<br> Price After Mark-up – USD ".$fPriceAfterMarkUp ;
                $szApiResponseLog .= "<br> Additional fixed markup – USD ".$fMarkupperShipmentUSD ;

                $fBookingprice=0;
                $fLabelFeeForwarderCurrency = 0;
                if((int)$pricingArr[0]['iBookingIncluded']!=1)
                { 
                    if($data['iBookingPriceCurrency']==1)
                    {  
                        $iBookingPriceCurrency = 1;  
                        $iBookingPriceCurrencyROE = 1;
                        $szBookingPriceCurrency = 'USD';
                        $fBookingprice = $data['fPrice']; 
                    }
                    else
                    {
                        $resultAry = $kWarehouseSearch->getCurrencyDetails($data['iBookingPriceCurrency']); 
                        $iBookingPriceCurrencyROE = $resultAry['fUsdValue']; 
                        $szBookingPriceCurrency = $resultAry['szCurrency']; 
                        $iBookingPriceCurrency = $resultAry['idCurrency']; 
                        if($iBookingPriceCurrencyROE>0)
                        {
                            $fBookingprice = $data['fPrice']*$iBookingPriceCurrencyROE;
                        }
                        else
                        {
                            $fBookingprice = 0;
                        }
                    }  
                    $szApiResponseLog .= " <br> Booking and label fee for Transporteca: ".$szBookingPriceCurrency." ".$data['fPrice']."" ;
                    $szApiResponseLog .= " <br> Booking and label fee USD: ".$fBookingprice ;
                    $szApiResponseLog .= " <br> Booking and label fee exchange rate: ".$iBookingPriceCurrencyROE ;
                    
                    if($fForwarderExchangeRate>0)
                    {
                        $fLabelFeeForwarderCurrency = $fBookingprice/$fForwarderExchangeRate;
                    } 
                    else
                    {
                        $fLabelFeeForwarderCurrency = 0;
                    } 
                }  
                else
                {
                    $szApiResponseLog .= " <br> Booking and label fee for Transporteca: No ";
                }
                /*
                * For self invoiced price we don't include Label fee, Handling Fee
                */
                $fTotalSelfInvoiceAmountUSD = $fPriceAfterMarkUp + $fMarkupperShipmentUSD;
                $fShipmentUSDCost = $fPriceAfterMarkUp + $fMarkupperShipmentUSD + $fBookingprice;  
                $fMarkUpRefferalFee=$fShipmentUSDCost*.01* $data['fReferalFee'];
                $fShipmentUSDCostForHandlingFee = $fPriceAfterMarkUp + $fMarkupperShipmentUSD;
 
                $bPrivateCustomerFeeApplied = false; 
                $szApiResponseLog = "<br><strong>H. Private Customer Fee </strong>";

                if($iPrivateShipping==1)
                {  
                    $szForwarderProduct = 'COURIER'; 
                    $privateCustomerFeeAry = array();
                    $privateCustomerFeeAry = $kForwarder_new->getAllPrivateCustomerPricing($data['idForwarder'],$szForwarderProduct,1);
 
                    if(!empty($privateCustomerFeeAry[$szForwarderProduct]))
                    {
                        $privateCustomerFeeArys = $privateCustomerFeeAry[$szForwarderProduct];
                        if($privateCustomerFeeArys['idCurrency']==1)
                        {
                            $idPrivateCustomerFeeCurrency = 1; //USD
                            $szPrivateCustomerFeeCurrency = "USD";
                            $fPrivateCustomerFeeExchangeRate = 1;

                            $fPrivateCustomerFee = $privateCustomerFeeArys['fCustomerFee'];
                            $fPrivateCustomerFeeUSD = $privateCustomerFeeArys['fCustomerFee'];
                        }
                        else
                        {
                            $privateCurrAry = array();
                            $privateCurrAry = $kWarehouseSearch->getCurrencyDetails($privateCustomerFeeArys['idCurrency']); 
                            $fPrivateCustomerFeeExchangeRate = $privateCurrAry['fUsdValue']; 
                            $szPrivateCustomerFeeCurrency = $privateCurrAry['szCurrency']; 
                            $idPrivateCustomerFeeCurrency = $privateCurrAry['idCurrency'];  

                            $fPrivateCustomerFee = $privateCustomerFeeArys['fCustomerFee'];

                            $fPrivateCustomerFeeUSD = $privateCustomerFeeArys['fCustomerFee'] * $fPrivateCustomerFeeExchangeRate;
                        } 
                        $bPrivateCustomerFeeApplied = true;  
                        $fShipmentUSDCost = $fShipmentUSDCost + $fPrivateCustomerFeeUSD;  
                        $fTotalSelfInvoiceAmountUSD = $fTotalSelfInvoiceAmountUSD + $fPrivateCustomerFeeUSD;
 
                        $szApiResponseLog .= "<br> Local Currency: ".$szPrivateCustomerFeeCurrency;
                        $szApiResponseLog .= "<br> Local ROE: ".$fPrivateCustomerFeeExchangeRate;
                        $szApiResponseLog .= " <br> Private customer fee for booking: USD ".$fPrivateCustomerFeeUSD;    
                    }  
                }
                else
                {
                    $szApiResponseLog .= " <br> Private customer fee applied: No ";
                }
                
                $bHandlingFeeApplied = false;
                $handlingFeeAry = array(); 
                if(!empty($vogaHandlingFeeAry) && ($iSearchMiniVersion==7 || $iSearchMiniVersion==8 || $iSearchMiniVersion==9 || $iCheckHandlingFeeForAutomaticQuote==1))
                {
                    $key = $data['idForwarder']."_".$data['idCourierProvider']."_".$idCourierProviderProductid;   
                    if(!empty($vogaHandlingFeeAry['courier'][$key]))
                    {
                        $handlingFeeAry = $vogaHandlingFeeAry['courier'][$key];
                        $bHandlingFeeApplied = true;
                    }
                }  
                if(!empty($handlingFeeAry))
                {  
                    $szApiResponseLog .= " <br> Handling Price Applied: Yes ";
                    $szApiResponseLog .= " <br> Sales price calculated for Handling Fee: USD ".$fShipmentUSDCostForHandlingFee; 
                     
                    if($handlingFeeAry['idHandlingCurrency']==1)
                    {
                        $idHandlingCurrency = $handlingFeeAry['idHandlingCurrency'];
                        $szHandlingCurrencyName = $handlingFeeAry['szHandlingCurrencyName'];
                        $fHandlingCurrencyExchangeRate = $handlingFeeAry['fHandlingCurrencyExchangeRate'];
                        
                        $fHandlingFeePerBooking = $handlingFeeAry['fHandlingFeePerBooking'];
                    }
                    else
                    {
                        $resultAry = $kWarehouseSearch->getCurrencyDetails($handlingFeeAry['idHandlingCurrency']); 
                        $fHandlingCurrencyExchangeRate = $resultAry['fUsdValue']; 
                        $szHandlingCurrencyName = $resultAry['szCurrency']; 
                        $idHandlingCurrency = $resultAry['idCurrency'];  
                        
                        $fHandlingFeePerBooking = $handlingFeeAry['fHandlingFeePerBooking'] * $fHandlingCurrencyExchangeRate;
                    } 
                    if($handlingFeeAry['idHandlingMinMarkupCurrency']==1)
                    {
                        $idHandlingMinMarkupCurrency = $handlingFeeAry['idHandlingMinMarkupCurrency'];
                        $szHandlingMinMarkupCurrencyName = $handlingFeeAry['szHandlingMinMarkupCurrencyName'];
                        $fHandlingMinMarkupCurrencyExchangeRate = $handlingFeeAry['fHandlingMinMarkupCurrencyExchangeRate']; 
                        $fHandlingMinMarkupPrice = $handlingFeeAry['fHandlingMinMarkupPrice'];
                    }
                    else
                    {
                        $resultAry = $kWarehouseSearch->getCurrencyDetails($handlingFeeAry['idHandlingMinMarkupCurrency']); 
                        $fHandlingMinMarkupCurrencyExchangeRate = $resultAry['fUsdValue']; 
                        $szHandlingMinMarkupCurrencyName = $resultAry['szCurrency']; 
                        $idHandlingMinMarkupCurrency = $resultAry['idCurrency'];  
                        
                        $fHandlingMinMarkupPrice = $handlingFeeAry['fHandlingMinMarkupPrice'] * $fHandlingMinMarkupCurrencyExchangeRate;
                    }  
                    $fHandlingMarkupPercentage = $handlingFeeAry['fHandlingMarkupPercentage'];
                    $fHandlingMarkupPercentageValue = $fShipmentUSDCostForHandlingFee * $handlingFeeAry['fHandlingMarkupPercentage'] * 0.01;
                    
                    if($fHandlingMarkupPercentageValue>$fHandlingMinMarkupPrice)
                    {
                        $fHandlingMarkupFeeUsd = $fHandlingMarkupPercentageValue;
                    }
                    else
                    {
                        $fHandlingMarkupFeeUsd = $fHandlingMinMarkupPrice;
                    }
                    $szApiResponseLog .= " <br> Handling fee pre booking: USD ".$fHandlingFeePerBooking;                    
                    $szApiResponseLog .= "<br>Handling Markup (".$handlingFeeAry['fHandlingMarkupPercentage']."%, minimum USD ".$fHandlingMinMarkupPrice.") – USD ".$fHandlingMarkupFeeUsd;
                    
                    $fTotalHandlingFeeUSD = $fHandlingFeePerBooking + $fHandlingMarkupFeeUsd;
                    
                    $fShipmentUSDCost = $fShipmentUSDCost+ $fTotalHandlingFeeUSD;
                    $szApiResponseLog .= " <br> Sales price for customer: USD ".$fShipmentUSDCost;  
                }
                else
                {
                    $szApiResponseLog .= " <br> Handling Price Applied: No ";
                }
                
                $bManualFeeApplicable = false;
                if($data['iQuickQuote']==1  && $data['iQuickQuoteTryitout']!=1)
                { 
                    $szApiResponseLog .= " <br> Manual Fee Applied: Yes ";
                    $szApiResponseLog .= " <br> Sales price excluding Manual fee: USD ".number_format((float)$fShipmentUSDCost);
                    $manualFeeSearchAry = array();
                    $manualFeeSearchAry['idForwarder'] = $idForwarder; 
                    $manualFeeSearchAry['szProduct'] = __MANUAL_FEE_COURIER__; 
                    $manualFeeSearchAry['szCargo'] = $data['szCargoType'];
                    $kVatApplication = new cVatApplication();
                    $manualFeeAry = array();
                    $manualFeeAry = $kVatApplication->getManualFeeListing(false, false, $manualFeeSearchAry);
                    $manualFeeAry = $manualFeeAry[0];
                    
                    if(empty($manualFeeAry))
                    {
                        /*
                        * If There is no Manual fee defined for the Forwarder then we calculate based on All other forwarder manual fee rates
                        */
                        $manualFeeSearchAry = array();
                        $manualFeeSearchAry['iAllForwarderFlag'] = '1'; 
                        $manualFeeSearchAry['szProduct'] = __MANUAL_FEE_COURIER__; 
                        $manualFeeSearchAry['szCargo'] = $data['szCargoType'];
                        $manualFeeAry = $kVatApplication->getManualFeeListing(false, false, $manualFeeSearchAry);
                        $manualFeeAry = $manualFeeAry[0];
                    }
                    
                    if(empty($manualFeeAry))
                    {
                        /*
                        * If There is no Manual fee defined for the Trade then we calculate based on default manual fee rates
                        */
                        $manualFeeAry = $kVatApplication->getDefaultManualFee();
                    } 
                    if(!empty($manualFeeAry))
                    {
                        $bManualFeeApplicable = true;
                        $idManualFeeCurrency = $manualFeeAry['idCurrency'];
                        $fManualFee = $manualFeeAry['fManualFee'];
                        $szApiResponseLog .= " <br> Manual fee: ".$manualFeeAry['szCurrency']." ".number_format((float)$fManualFee,2);
                        if($idManualFeeCurrency==1)
                        {
                            $fTotalManualFeeUSD = $fManualFee;
                            $fManualFeeExchangeRates = 1;
                            $szManualFeeCurrency = 'USD';
                        }
                        else
                        {
                            $resultAry = $kWarehouseSearch->getCurrencyDetails($idManualFeeCurrency); 
                            $fManualFeeExchangeRates = $resultAry['fUsdValue']; 
                            $szManualFeeCurrency = $resultAry['szCurrency'];  

                            $fTotalManualFeeUSD = $fManualFee * $fManualFeeExchangeRates;
                        }
                    }
                    else
                    {
                        $szApiResponseLog .= " <br> There is no Manual fee defined for the trade.";
                    }
                }
                else
                {
                    $szApiResponseLog .= " <br> Manual Fee Applied: No ";
                } 
                if($data['iProfitType']==2)
                { 
                    $subTotalForRefferalFee=$fShipmentUSDCost-$fBookingprice;
                    $fMarkUpRefferalFee = ((0.01 * $data['fReferalFee'])/( 1 - (0.01*$data['fReferalFee'])) ) * $subTotalForRefferalFee;
                    $fShipmentUSDCost = $fShipmentUSDCost + $fMarkUpRefferalFee ;
                    $szApiResponseLog .= " <br> GP Type: Mark-up ";
                    $szApiResponseLog .= " <br> GP Amount: USD ".($fMarkUpRefferalFee+$fBookingprice);
                }
                else
                {
                    $subTotalForRefferalFee=$fShipmentUSDCost-$fBookingprice;
                    $fMarkUpRefferalFee = $subTotalForRefferalFee*.01*$data['fReferalFee'];
                    $szApiResponseLog .= " <br> GP Type: Referral ";
                    $szApiResponseLog .= " <br> GP Amount: USD ".($fMarkUpRefferalFee+$fBookingprice);
                } 

                $szApiResponseLog .= "<br> Total sales price without VAT: USD ".number_format((float)$fShipmentUSDCost,2)."<br><br>";

                $serviceType =$szServiceType;
                if($iNumDaysAddedToDelivery>0)
                {
                    $deliveryDate = date('Y-m-d H:i:s',strtotime($szTransitDays . "+".$iNumDaysAddedToDelivery." days"));

                    $timeStamp=date('H:i:s',strtotime($deliveryDate));
                    $dayName=date('D',strtotime($deliveryDate));

                    if($dayName=='Sun' || $dayName=='Sat')
                    {
                       $deliveryDate = getBusinessDaysForCourierDeliveryDate($deliveryDate, 1)." ".$timeStamp;
                    }
                }
                else
                {
                    $deliveryDate = date('Y-m-d H:i:s',strtotime($szTransitDays)); 
                }  

                $szApiResponseLog .= "<br>Delivery date ".$deliveryDate;

                if((int)$_SESSION['user_id']>0)
                {
                    $kUser = new cUser();
                    $kUser->getUserDetails($_SESSION['user_id']); 
                    $idCustomerCurrency = $kUser->szCurrency;
                } 
                else
                {
                    $idCustomerCurrency = $data['idCustomerCurrency'];
                }
                
                if($idCustomerCurrency==1)
                {
                    $fVatRateAmount = round((float)$fShipmentUSDCost)*0.01*$fVatRate;   
                    $fShipmentVatUSDCost = $fShipmentUSDCost+$fVatRateAmount;

                    $szApiResponseLog .= "<br>VAT Percentage:".$fVatRate."%"; 
                    $szApiResponseLog .= "<br>VAT Amount: USD ".round($fVatRateAmount,2); 
                }
                else
                {
                    $szApiResponseLog .= "<br>VAT Percentage:".$fVatRate."%";
                    $resultAry = $kWarehouseSearch->getCurrencyDetails($idCustomerCurrency); 
                    $iVatPriceCurrencyROE = $resultAry['fUsdValue']; 
                    $szVatPriceCurrency = $resultAry['szCurrency']; 
                    $iVatPriceCurrency = $resultAry['idCurrency']; 
                    if($iVatPriceCurrencyROE>0)
                    {
                        $fVatCalAmuount = $fShipmentUSDCost/$iVatPriceCurrencyROE;
                    }
                    else
                    {
                        $fVatCalAmuount = 0;
                    }

                    $fVatRateAmount=$fVatCalAmuount*0.01*$fVatRate;

                    $fVatRateAmountUSD=$fVatRateAmount*$iVatPriceCurrencyROE;
                    $fShipmentVatUSDCost=$fShipmentUSDCost+$fVatRateAmountUSD;
                   // echo $fShipmentAPIUSDCost;
                    $szApiResponseLog .= "<br>VAT Amount: USD ".round($fVatRateAmountUSD,2); 
                } 
                $szApiResponseLog .= "<br> Total sales price including VAT: USD ".number_format((float)$fShipmentVatUSDCost,2);
                $ret_ary[$ctr]['szVATRegistration'] =$szVATRegistration;
                $ret_ary[$ctr]['szCarrierCompanyName'] = $szCarrierCompanyName;
                $ret_ary[$ctr]['szCarrierServiceName'] = $szSericeDescription; 
                $ret_ary[$ctr]['idCourierProviderProduct'] = $idCourierProviderProductid;
                $ret_ary[$ctr]['dtAvailableDate'] = $deliveryDate ;
                $ret_ary[$ctr]['szCurrencyCode'] = $szCurrencyCode ;
                $ret_ary[$ctr]['fShippingAmount'] = $fShipmentCost ;
                $ret_ary[$ctr]['szServiceType'] =  $code ; 
                $ret_ary[$ctr]['fShipmentAPIUSDCost'] =  round($fShipmentAPIUSDCost,4) ; 
                $ret_ary[$ctr]['fShipmentUSDCost'] =  round($fShipmentUSDCost,4) ; 
                $ret_ary[$ctr]['fDisplayPrice'] =  round($fShipmentCustomerCost,4) ; 
                $ret_ary[$ctr]['szCurrency'] =  $szCurrency ;
                $ret_ary[$ctr]['idCurrency'] =  $idCurrency ;  
                $ret_ary[$ctr]['markUpByRate'] =  $fTotalMarkup ; 
                $ret_ary[$ctr]['miniMarkUpRate'] = $miniMarkUpRate;
                $ret_ary[$ctr]['fPriceAfterMarkUp']=$fPriceAfterMarkUp;   
                $ret_ary[$ctr]['fMarkupperShipment']=$fMarkupperShipment; 
                $ret_ary[$ctr]['fMarkUpRefferalFee']=$fMarkUpRefferalFee;
                $ret_ary[$ctr]['fBookingprice']=$fBookingprice;
                $ret_ary[$ctr]['fReferalPercentage'] = $data['fReferalFee'] ; 
                $ret_ary[$ctr]['idForwarderCurrency'] = $idForwarderCurrency;
                $ret_ary[$ctr]['idProviderProduct'] = $idCourierProviderProductid;
                $ret_ary[$ctr]['idCourierAgreement'] = $idCourierAgreement; 
                $ret_ary[$ctr]['iBookingIncluded'] = $pricingArr[0]['iBookingIncluded'];  
                $ret_ary[$ctr]['szCarrierAccounID'] = $priceResponseArys['szCarrierAccounID'];
                $ret_ary[$ctr]['szEasypostShipmentID'] = $priceResponseArys['szEasypostShipmentID'];
                $ret_ary[$ctr]['szEasyPostRateID'] = $priceResponseArys['szEasyPostRateID'];
                $ret_ary[$ctr]['szBookingLabelPriceCurrency'] = $szBookingPriceCurrency; 
                $ret_ary[$ctr]['szAdditionalMarkupCurrency'] = $szAdditionalMarkupCurrency;
                $ret_ary[$ctr]['fCourierPrductMarkupForwarderCurrency'] = $fCourierPrductMarkupForwarderCurrency; 
                $ret_ary[$ctr]['fApiPriceForwarderCurrency'] = $fApiPriceForwarderCurrency;
                $ret_ary[$ctr]['fLabelFeeForwarderCurrency'] = $fLabelFeeForwarderCurrency;
                $ret_ary[$ctr]['fTotalSelfInvoiceAmountUSD'] =  round($fTotalSelfInvoiceAmountUSD,4) ; 
                
                if($bPrivateCustomerFeeApplied)
                {
                    $ret_ary[$ctr]['iPrivateCustomerFeeApplicable'] = 1;
                    $ret_ary[$ctr]['idPrivateCustomerFeeCurrency'] = $idPrivateCustomerFeeCurrency;   
                    $ret_ary[$ctr]['szPrivateCustomerFeeCurrency'] = $szPrivateCustomerFeeCurrency;
                    $ret_ary[$ctr]['fPrivateCustomerFeeExchangeRate'] = $fPrivateCustomerFeeExchangeRate;  
                    $ret_ary[$ctr]['fPrivateCustomerFee'] = $fPrivateCustomerFee;  
                    $ret_ary[$ctr]['fPrivateCustomerFeeUSD'] = $fPrivateCustomerFeeUSD; 
                }
                else
                {
                    $ret_ary[$ctr]['iPrivateCustomerFeeApplicable'] = 0;
                }
                
                if($bHandlingFeeApplied)
                {
                    $ret_ary[$ctr]['iHandlingFeeApplicable'] = 1;
                    $ret_ary[$ctr]['iProductType'] = 2; //Courier
                    $ret_ary[$ctr]['idHandlingCurrency'] = $idHandlingCurrency;
                    $ret_ary[$ctr]['szHandlingCurrencyName'] = $szHandlingCurrencyName;
                    $ret_ary[$ctr]['fHandlingCurrencyExchangeRate'] = $fHandlingCurrencyExchangeRate; 
                    $ret_ary[$ctr]['fHandlingMarkupPercentage'] = $fHandlingMarkupPercentage;  
                    $ret_ary[$ctr]['idHandlingMinMarkupCurrency'] = $idHandlingMinMarkupCurrency;
                    $ret_ary[$ctr]['szHandlingMinMarkupCurrencyName'] = $szHandlingMinMarkupCurrencyName;
                    $ret_ary[$ctr]['fHandlingMinMarkupCurrencyExchangeRate'] = $fHandlingMinMarkupCurrencyExchangeRate;
                    $ret_ary[$ctr]['fHandlingMinMarkupPrice'] = $fHandlingMinMarkupPrice; 
                    $ret_ary[$ctr]['fHandlingFeePerBooking'] = $fHandlingFeePerBooking;  
                    $ret_ary[$ctr]['fTotalHandlingFeeUSD'] = $fTotalHandlingFeeUSD;  
                }
                else
                {
                    $ret_ary[$ctr]['iHandlingFeeApplicable'] = 0;
                } 
                if($iPrivateShipping==1)
                {
                    $ret_ary[$ctr]['szCustomerType'] = "Private"; 
                }
                else
                {
                    $ret_ary[$ctr]['szCustomerType'] = "Business"; 
                }
                
                if($bManualFeeApplicable)
                {
                    $ret_ary[$ctr]['iManualFeeApplicable'] = 1; 
                    $ret_ary[$ctr]['idManualFeeCurrency'] = $idManualFeeCurrency;
                    $ret_ary[$ctr]['szManualFeeCurrency'] = $szManualFeeCurrency;
                    $ret_ary[$ctr]['fManualFeeExchangeRates'] = $fManualFeeExchangeRates; 
                    $ret_ary[$ctr]['fTotalManualFeeUSD'] = $fTotalManualFeeUSD;
                    $ret_ary[$ctr]['fTotalManualFee'] = $fManualFee;
                }
                ++$ctr; 
            }
        }
        $this->szApiResponseLog .= $szApiResponseLog;  
        return $ret_ary;
    }
    
    function createOriginAddressObject($data,$bCheckAuth=false,$bCreateLabel=false)
    {  
        if($bCheckAuth)
        {
            /*
            *  While checking authentication we check with hardcoded address value. 
            */
            // create addresses
            $from_address_params = array(
                "name"    => "",
                "street1" => "Band stand",
                "street2" => "",
                "city"    => "Mumbai",
                "zip"     => "400001",
                "country" => "IN" 
            ); 
            return $from_address_params;
        }
        else if($bCreateLabel)
        { 
            $szShipperFullName = $data['szShipperFirstName']." ".$data['szShipperLastName'];
            
            // create addresses
            $from_address_params = array(
                "name"    => $szShipperFullName,
                "company" => $data['szShipperCompanyName'],
                "street1" => $data['szShipperAddress'],
                "street2" => "",
                "city"    => $data['szShipperCity'],
                "zip"     => $data['szShipperPostCode'],
                "country" => $data['szShipperCountryCode'],
                'phone' => $data['szShipperPhone'],
                'email' => $data['szShipperEmail']
            ); 
            if($data['szShipperCountryCode']=='US')
            {
                $szState = $this->getUsaStates($from_address_params);
                $from_address_params['state'] = $szState;
            } 
            return $from_address_params;
        }
        else
        {  
            
            $kConfigNewCD= new cConfig();
            $kConfigNewCD->loadCountry(false,$data['szCountry']);            
            if($kConfigNewCD->szDefaultCourierCity!='' || $kConfigNewCD->szDefaultCourierPostcode!='')
            {
                $data['szCity']=$kConfigNewCD->szDefaultCourierCity;
                $data['szPostCode']=$kConfigNewCD->szDefaultCourierPostcode;
            }
            
            $bExceptionalTradeFlag = false;
            if($data['szCountry']=='GB' && $data['szSCountry']!='DK')
            {
                $bExceptionalTradeFlag = true;
            }
            if($data['szCountry']=='GB' && $data['idCourierAgreement']==self::$standaradAgreementId && $bExceptionalTradeFlag)
            {
                // create addresses
                $from_address_params = array( 
                    "zip"     => "2100",
                    "country" => "DK" 
                );  
            }
            else if($data['szCountry']=='IE' && $data['idCourierProvider']==__TNT_API__)
            {
                // create addresses
                $from_address_params = array(
                    "city"     => $data['szCity'],
                    "country" => $data['szCountry'] 
                );  
            }
            else
            {
                // create addresses
                $from_address_params = array( 
                    "zip"     => $data['szPostCode'],
                    "country" => $data['szCountry'] 
                );  

                if($data['szCountry']=='US')
                { 
                    $stateSearchAry = array();
                    $stateSearchAry = $from_address_params;
                    $stateSearchAry['city'] = $data['szCity']; 

                    $szState = $this->getUsaStates($stateSearchAry);
                    $from_address_params['state'] = $szState; 
                }
                else if(strtoupper($data['szPostCode'])=='CO12 4QG' || strtoupper($data['szPostCode']) == 'CO124QG' || strtoupper($data['szPostCode']) == '2850' || strtoupper($data['szPostCode']) == '43124')
                {
                    if(strtoupper($data['szPostCode']) == '2850' && $data['szCountry']=='DK')
                    {
                        $from_address_params['city'] = 'Nærum';
                    }
                    else if(strtoupper($data['szPostCode']) == '43124' && $data['szCountry']=='IT')
                    {
                        $from_address_params['city'] = 'Parma';
                    }
                    else
                    {
                        $from_address_params['city'] = $data['szCity'];
                    } 
                } 
            } 
            $this->szApiResponseInformationText .= "<br><strong>Searching for: ".print_R($from_address_params,true)."</strong>";
            return $from_address_params;
        } 
    } 
    
    function createCustomInforObject($data,$bCreateLabel=false)
    { 
        if($bCreateLabel)
        { 
            $fCargoWeight = 35.274 * $data['fCargoWeight']; 
            if($data['fValueOfGoods']<=0)
            {
                $data['fValueOfGoods'] = 1;
                $data['szGoodsInsuranceCurrency'] = 'USD';
            }
            $szCargoDescription = $data['szCargoDescription'];
            if(empty($szCargoDescription))
            {
                $szCargoDescription = "Cargo goods";
            } 
            
            $data['szShipperCountryCode'] = 'DK';
            // customs info form
            $customs_item_params = array(
                "description"   => $szCargoDescription,  
                "quantity" => $data['iNumColli'], 
                "weight" => $fCargoWeight,
                "value" => $data['fValueOfGoods'],
                "hs_tariff_number" => 610910,
                "origin_country"  => $data['szShipperCountryCode']
            ); 

            $customs_info_params = array(
                "eel_pfc" => 'NOEEI 30.37(a)',
                "customs_certify"      => true,
                "customs_signer"       => "Transporteca Ltd",
                "customs_items"        => array($customs_item_params),
                "contents_type"        => "merchandise",
                "contents_explanation" => $szCargoDescription 
            ); 
            return $customs_info_params;
        }
        else
        {
            $fCargoWeight = 35.274 * $data['fCargoWeight']; 
            if($data['fValueOfGoods']<=0)
            {
                $data['fValueOfGoods'] = 1;
                $data['szGoodsInsuranceCurrency'] = 'USD';
            }
            $szCargoDescription = $data['szCargoDescription'];
             if(empty($szCargoDescription))
             {
                 $szCargoDescription = "Cargo goods";
             } 
            // customs info form
            $customs_item_params = array(
                "description"   => $szCargoDescription,  
                "quantity" => $data['iNumColli'], 
                "weight" => $fCargoWeight,
                "value" => $data['fValueOfGoods'],
                "hs_tariff_number" => 610910,
                "origin_country"  => $data['szCountry'] 
            ); 

            $customs_info_params = array(
                "eel_pfc" => 'NOEEI 30.37(a)',
                "customs_certify"      => true,
                "customs_signer"       => "Transporteca Ltd",
                "customs_items"        => array($customs_item_params),
                "contents_type"        => "merchandise",
                "contents_explanation" => $szCargoDescription 
            ); 
            return $customs_info_params;
        } 
    }
    
    function createDestinationAddressObject($data,$bCheckAuth=false,$bCreateLabel=false)
    {
        if($bCheckAuth)
        {
            /*
            *  While checking authentication we check with hardcoded address value.
            */
            // create addresses
            $to_address_params = array( 
               "street1" => "Copenhagen", 
               "city"    => "Copenhagen",
               "zip"     => "2100",
               "country" => "DK" 
           );
           return $to_address_params;
        }
        else if($bCreateLabel)
        { 
            $szConsigneeFullName = $data['szConsigneeFirstName']." ".$data['szConsigneeLastName'];
             // create addresses
            $to_address_params = array(
                "name"    => $szConsigneeFullName,
                "company" => $data['szConsigneeCompanyName'],
                "street1" => $data['szConsigneeAddress'],
                "street2" => "",
                "city"    => $data['szConsigneeCity'],
                "zip"     => $data['szConsigneePostCode'],
                "country" => $data['szConsigneeCountryCode'],
                'phone' => $data['szConsigneePhone'],
                'email' => $data['szConsigneeEmail']
            ); 
            if($data['szConsigneeCountryCode']=='US')
            {
                $szState = $this->getUsaStates($to_address_params);
                $to_address_params['state'] = $szState;
            }
            return $to_address_params;
        }
        else
        {
            $kConfigNewSCD= new cConfig();
            $kConfigNewSCD->loadCountry(false,$data['szSCountry']);            
            if($kConfigNewSCD->szDefaultCourierCity!='' || $kConfigNewSCD->szDefaultCourierPostcode!='')
            {
                $data['szSCity']=$kConfigNewSCD->szDefaultCourierCity;
                $data['szSPostCode']=$kConfigNewSCD->szDefaultCourierPostcode;
            }
            $bExceptionalTradeFlag = false;
            if($data['szSCountry']=='GB' && $data['szCountry']!='DK')
            {
                $bExceptionalTradeFlag = true;
            }
            if($data['szSCountry']=='GB' && $data['idCourierAgreement']==self::$standaradAgreementId && $bExceptionalTradeFlag)
            {
                // create addresses
                $to_address_params = array( 
                    "zip"     => "2100",
                    "country" => "DK" 
                );  
            }
            else if($data['szSCountry']=='IE' && $data['idCourierProvider']==__TNT_API__)
            {
                // create addresses
                $to_address_params = array(
                    "city"     => $data['szSCity'],
                    "country" => $data['szSCountry'] 
                );  
            }
            else
            {
                // create addresses
                $to_address_params = array( 
                   "zip"     => $data['szSPostCode'],
                   "country" => $data['szSCountry'] 
                );

                if($data['szSCountry']=='US')
                { 
                    $stateSearchAry = array();
                    $stateSearchAry = $to_address_params;
                    $stateSearchAry['city'] = $data['szSCity']; 

                    $szState = $this->getUsaStates($stateSearchAry);
                    $to_address_params['state'] = $szState; 
                }
                else if(strtoupper($data['szSPostCode'])=='CO12 4QG' || strtoupper($data['szSPostCode']) == 'CO124QG' || strtoupper($data['szSPostCode']) == '2850' || strtoupper($data['szSPostCode']) == '43124')
                {
                    if(strtoupper($data['szSPostCode']) == '2850' && $data['szSCountry']=='DK')
                    {
                        $to_address_params['city'] = 'Nærum';
                    }
                    else if(strtoupper($data['szSPostCode']) == '43124' && $data['szSCountry']=='IT')
                    {
                        $to_address_params['city'] = 'Parma';
                    }
                    else
                    {
                        $to_address_params['city'] = $data['szSCity'];
                    }  
                }
            } 
            $this->szApiResponseInformationText .= "<br><strong>Searching for: ".print_R($to_address_params,true)."</strong>";
            return $to_address_params;
        } 
    }
    
    function createParcelObject($data,$bCheckAuth=false)
    { 
        if($bCheckAuth)
        {
            /*
            *  While checking we check with some hardcoded parcel value.
            
            if($data['idCourierProvider']==__FEDEX_API__)
            {
                $szPredefinedPackage = 'FedExPak' ;
            }
            else if($data['idCourierProvider']==__UPS_API__)
            {
                $szPredefinedPackage = 'Pak' ;
            }
            else
            { 
                $szPredefinedPackage = 'Parcel' ;  
            }
            */
            $iLength = 25;
            $iWidth = 25;
            $iHeight = 25;
            $fWeight = 100;
            
            $parcel_params['parcel'] = array(
                "length"             => $iLength,
                "width"              => $iWidth,
                "predefined_package" => $szPredefinedPackage,
                "height"             => $iHeight, 
                "weight"             => $fWeight
            );
            $packageArr['shipments'][] = $parcel_params; 
            return $packageArr;
        }
        else
        {
            $idBooking =  $data['idBooking'];
            $bDefaultPackageFlag = true;
            $bPartnerPacking = false;
            if(cPartner::$bApiValidation && !empty(cPartner::$searchablePacketTypes) && (in_array(1, cPartner::$searchablePacketTypes) || in_array(2, cPartner::$searchablePacketTypes)))
            { 
                $bPartnerPacking = true;
            }    
            if($idBooking>0 || $bPartnerPacking || $data['szSearchMode']=='FORWARDER_COURIER_TRY_IT_OUT')
            {
                $kBooking =new cBooking();
                if($bPartnerPacking)
                { 
                    $kPartner = new cPartner();
                    $bookingCargoAry = $kPartner->getCargoDeailsByPartnerApi();  
                }
                else if($data['szSearchMode']=='FORWARDER_COURIER_TRY_IT_OUT')
                {
                    $postSearchAry = $data;
                    $bookingCargoAry = cWHSSearch::$cForwarderTryitoutCargoDetails;
                }
                else
                {
                    $bookingCargoAry = $kBooking->getCargoDeailsByBookingId($idBooking,true); 
                    $postSearchAry = $kBooking->getBookingDetails($idBooking);
                }  
                $kWHSSearch = new cWHSSearch();
                if(!empty($bookingCargoAry) && ($bPartnerPacking || $postSearchAry['iCourierBooking']==1 || $postSearchAry['iSearchMiniVersion']==3 || $postSearchAry['iSearchMiniVersion']==4 || $postSearchAry['iSearchMiniVersion']==5 || $postSearchAry['iSearchMiniVersion']==6 || $postSearchAry['iSearchMiniVersion']==7 || $postSearchAry['iSearchMiniVersion']==8 || $postSearchAry['iSearchMiniVersion']==9 || $data['szSearchMode']=='FORWARDER_COURIER_TRY_IT_OUT'))
                {   
                    $ctr=1;
                    $iQuantity = 0;
                    $bDefaultPackageFlag = false;
                    foreach($bookingCargoAry as $bookingCargoArys)
                    {  
                        if($postSearchAry['iPalletType']>0 && (float)$bookingCargoArys['fHeight']<=0)
                        {
                            /*
                            * If shipment type is pallet and height is empty then we use standard height as per pallet type
                            */
                            if($postSearchAry['iPalletType']==1) //Euro Pallet
                            {
                                $bookingCargoArys['fHeight'] = __STANDARD_HEIGHT_EURO_PALLET__;
                            }
                            else if($postSearchAry['iPalletType']==2) //Half Pallet
                            {
                                $bookingCargoArys['fHeight'] = __STANDARD_HEIGHT_HALF_PALLET__;
                            }
                        }
                        if($bookingCargoArys['idWeightMeasure']==1)
                        {
                            $fKgFactor = 1;
                        }
                        else
                        { 
                            $fKgFactor = $kWHSSearch->getWeightFactor($bookingCargoArys['idWeightMeasure']); 
                        }

                        if($bookingCargoArys['idCargoMeasure']==1)
                        {
                            $fCmFactor = 1;
                        }
                        else
                        {
                            $fCmFactor = $kWHSSearch->getCargoFactor($bookingCargoArys['idCargoMeasure']);  
                        }

                        $iLenght = 0;
                        $iWidth = 0;
                        $iHeight = 0;
                        $fWeight = 0;

                        /*
                         * So, please update so that when we send measures to TNT API, then we make: 
                            L = max(L,W)
                            W = min(L,W) 
                            Thereby we ensure that the smallest of the two are always sent as width. As as such, length and width can be interchanged, depending on how you view the actual piece.
                         */
                        $bookingCargoArys['fLength'] = $this->getMinMax($bookingCargoArys['fLength'],$bookingCargoArys['fWidth'],"MAX");
                        $bookingCargoArys['fWidth'] = $this->getMinMax($bookingCargoArys['fLength'],$bookingCargoArys['fWidth'],"MIN");
                        if($fCmFactor>0)
                        {
                            //Converting to Cm
                            $iLenghtCm = round($bookingCargoArys['fLength'] / $fCmFactor);
                            $iWidthCm = round($bookingCargoArys['fWidth'] / $fCmFactor);
                            $iHeightCm = round($bookingCargoArys['fHeight'] / $fCmFactor);

                            /*
                            * Coverting dimensions to INCHES, 1 Cm = 0.393701 Inch
                            */
                            $iLength = round_up(($iLenghtCm * 0.393701),1);
                            $iWidth = round_up(($iWidthCm * 0.393701),1);
                            $iHeight = round_up(($iHeightCm * 0.393701),1);
                        }

                        if($fKgFactor>0)
                        {
                            $fWeightKg = ceil(($bookingCargoArys['fWeight']/$fKgFactor)) ;

                            //Converting this OUNCES 1 Kg = 35.274 Ounce
                            $fWeight = round_up((35.274 * $fWeightKg),1);
                        }
                        $szCargoMeasureCode = 'CM';
                        $szCargoMeasure = 'Centimeters';

                        $szWeightMeasureCode = 'KGS';
                        $szWeightMeasure = 'Kgs';

                        $packageQuantity = $bookingCargoArys['iQuantity'];
                        $iTotalQuantity += $bookingCargoArys['iQuantity'];

                        for($i=0;$i<$packageQuantity;++$i)
                        { 
                            // create parcel
                            $parcel_params['parcel'] = array(
                                "length"             => $iLength,
                                "width"              => $iWidth,
                                "height"             => $iHeight, 
                                "weight"             => $fWeight
                            );
                            $packageArr['shipments'][] = $parcel_params;

                            $szCargoMeasure = "INCHES (IN)"; 
                            $szWeightMeasure = "OUNCES (OZ)";

                            $szPackageDetailsLogs .= "<br> Package: ".$iLenghtCm." X ".$iWidthCm." X ".$iHeightCm." cm, ".$fWeightKg." kg, Count: 1" ;
                            $szPackageDetailsLogs .= "<br> Package: ".$iLength." X ".$iWidth." X ".$iHeight." ".$szCargoMeasure. ", ".$fWeight." ".$szWeightMeasure.", Count: 1 <br> " ;
                        } 
                    }  
                    $parcel_params = $packageArr;
                    $szPackageDetailsLogs .= "<br><br> Total package: ".$iTotalQuantity; 
                } 
            }

            if($bDefaultPackageFlag)
            { 
                $kCourierServices = new cCourierServices();
                $productProviderArr = $kCourierServices->getProductProviderLimitations($data['idGroup'],true);  
                $bWeightLimitationDefined = false; 
                if(!empty($productProviderArr))
                {
                    foreach($productProviderArr as $productProviderArrs)
                    {
                        if($productProviderArrs['idCargoLimitationType']==__WEIGHT_PER_COLI__)
                        {
                            if($productProviderArrs['szRestriction']=='Maximum')
                            {
                                $maxWeightPerColli = $productProviderArrs['szLimit']; 
                                $bWeightLimitationDefined = true;
                            }  
                        } 
                        if($productProviderArrs['idCargoLimitationType']==__CHARGEABLE_WEIGHT_PER_COLI__)
                        {
                            if($productProviderArrs['szRestriction']=='Maximum')
                            {
                                $maxChargeableWeight = $productProviderArrs['szLimit']; 
                                $bWeightLimitationDefined = true;
                            } 
                        }
                    }
                }

                $changeFlag=false; 
                $fCargoWeight = $data['fCargoWeight'];
                $fCargoVolume = $data['fCargoVolume']; 
                $cargeAbleFlag=false;

                $fRmaingChargeableWeight = 0;
                $fRmaingWeight = 0;

                $fCargoActualWeight = $data['fCargoWeight'];

                if(($fCargoVolume*200)>$fCargoWeight)
                {
                    $cargeAbleFlag=true;
                    $fCargoWeight = $data['fCargoVolume']*200 ; 
                }   
                $fCargoChargeableWeight = $fCargoWeight; 
                if($bWeightLimitationDefined)
                { 
                    $changeFlag=true;
                    $iTotalNumActulalColli=1;
                    if($maxWeightPerColli>0.00)
                    {
                        $fRmaingWeight = (float)($fCargoActualWeight % $maxWeightPerColli);
                        $iTotalNumActulalColli = (int)($fCargoActualWeight / $maxWeightPerColli);
                        $t=0;
                        if((int)$fRmaingWeight>0)
                        {
                            $iTotalNumActulalColli = $iTotalNumActulalColli + 1;
                        } 
                    }
                    $iTotalChargeNumColli=1;
                    if($maxChargeableWeight>0.00)
                    {                    
                        $fRmaingChargeableWeight = (float)($fCargoChargeableWeight % $maxChargeableWeight);
                        $iTotalChargeNumColli = (int)($fCargoChargeableWeight / $maxChargeableWeight);
                        $t=0;
                        if((int)$fRmaingChargeableWeight>0)
                        {
                            $iTotalChargeNumColli = $iTotalChargeNumColli + 1;
                        } 
                    }  
                    $fRmaingWeight = 0;  
                    if($iTotalChargeNumColli>=$iTotalNumActulalColli)
                    { 
                        $cargoWeight=$fCargoChargeableWeight/$iTotalChargeNumColli;
                        $iTotalNumColli=$iTotalChargeNumColli;
                        $fRmaingWeight = $fCargoChargeableWeight - ($cargoWeight*$iTotalNumColli);
                    }
                    else if($iTotalChargeNumColli<$iTotalNumActulalColli)
                    {
                        $cargoWeight=$fCargoActualWeight/$iTotalNumActulalColli;
                        $iTotalNumColli=$iTotalNumActulalColli;
                        $fRmaingWeight = $fCargoActualWeight - ($cargoWeight*$iTotalNumColli);
                    }
                    else
                    {
                        $changeFlag=true;
                        $iTotalNumColli = 1;
                        $cargoWeight = $fCargoWeight;
                        $fRmaingWeight = 0;
                    } 
                }
                else
                {
                    $changeFlag=true;
                    $iTotalNumColli = 1;
                    $cargoWeight = $fCargoWeight;
                    $fRmaingWeight = 0;
                }

                $totalPackage = $iTotalNumColli;  
                if($changeFlag)
                { 
                    if((float)$fRmaingWeight>0.00)
                    {
                        $totalMaxPackage = $iTotalNumColli-1;
                    }
                    else
                    {
                        $totalMaxPackage = $iTotalNumColli;
                    } 

                    if($iTotalNumColli>0)
                    {
                        $fCargoVolume = $fCargoVolume/$iTotalNumColli ;
                    }

                    /*
                    * Coverting dimensions to Centimeters(CM)
                    */
                    $fCalculativeValue = pow($fCargoVolume, 0.333333);
                    $iLengthCm = round((100*$fCalculativeValue),2);
                    $iWidthCm = $iLengthCm;
                    $iHeightCm = $iLengthCm;
                    $iLenghtCm = $iLengthCm ;
                    /*
                    * Coverting dimensions to INCHES, 1 Cm = 0.393701 Inch
                    */
                    $iLength = round_up(($iLengthCm * 0.393701),1);
                    $iWidth = round_up(($iWidthCm * 0.393701),1);
                    $iHeight = round_up(($iHeightCm * 0.393701),1);
                    $szCargoMeasure = "INCHES (IN)"; 
                    $szWeightMeasure = "OUNCES (OZ)";

                    $t=0;
                    $szPackageDetailsLogs .= "<br> Number of Package: ".$totalMaxPackage.", Weight per package: ".$cargoWeight."<br>"; 
                    for($i=0;$i<$totalMaxPackage;++$i)
                    { 
                        $fWeightKg = $cargoWeight;
                        $fWeight = 35.274 * $fWeightKg; 

                        // create parcel
                        $parcel_params['parcel'] = array(
                            "length"             => $iLength,
                            "width"              => $iWidth,
                            "height"             => $iHeight, 
                            "weight"             => $fWeight
                        );
                        $packageArr['shipments'][] = $parcel_params; 

                        $szPackageDetailsLogs .= "<br> Package: ".$iLenghtCm." X ".$iWidthCm." X ".$iHeightCm." cm, ".$fWeightKg." kg, Count: 1" ;
                        $szPackageDetailsLogs .= "<br> Package: ".$iLength." X ".$iWidth." X ".$iHeight." ".$szCargoMeasure. ", ".$fWeight." ".$szWeightMeasure.", Count: 1 <br> " ;
                        ++$t;
                    } 

                    if((float)$fRmaingWeight>0.00)
                    {
                        $fWeightKg = $fRmaingWeight;
                        $fWeight = 35.274 * $fWeightKg;  

                        // create parcel
                        $parcel_params['parcel'] = array(
                            "length"             => $iLength,
                            "width"              => $iWidth,
                            "height"             => $iHeight, 
                            "weight"             => $fWeight
                        );
                        $packageArr['shipments'][] = $parcel_params; 

                        $szPackageDetailsLogs .= "<br> Package: ".$iLenghtCm." X ".$iWidthCm." X ".$iHeightCm." cm, ".$fWeightKg." kg, Count: 1" ;
                        $szPackageDetailsLogs .= "<br> Package: ".$iLength." X ".$iWidth." X ".$iHeight." ".$szCargoMeasure. ", ".$fWeight." ".$szWeightMeasure.", Count: 1 <br> " ; 
                    }
                } 
                $parcel_params = $packageArr;
                $szPackageDetailsLogs .= "<br><br> Total package: ".$iTotalNumColli; 
            }

            $this->szPackageDetailsLogs = $szPackageDetailsLogs;
            $this->szPackageDetailsLogs .= "<br><br> <u>Shipper </u><br> ";
            $this->szPackageDetailsLogs .= "Address: ".$data['szAddress1'];
            $this->szPackageDetailsLogs .= "<br>Postcode: ".$data['szPostCode'];
            $this->szPackageDetailsLogs .= "<br>City: ".$data['szCity'];
            $this->szPackageDetailsLogs .= "<br>Country: ".$data['szCountry'];

            $this->szPackageDetailsLogs .= "<br><br> <u>Consignee </u><br> ";
            $this->szPackageDetailsLogs .= "Address: ".$data['szSAddress1'];
            $this->szPackageDetailsLogs .= "<br>Postcode: ".$data['szSPostCode'];
            $this->szPackageDetailsLogs .= "<br>City: ".$data['szSCity'];
            $this->szPackageDetailsLogs .= "<br>Country: ".$data['szSCountry'];  
            return $parcel_params; 
        } 
    } 
    
    function getMinMax($fValue1,$fValue2,$mode)
    {
        if($mode=='MAX')
        {
            if($fValue1>$fValue2)
            {
                return $fValue1;
            }
            else
            {
                return $fValue2;
            }
        }
        else if($mode=='MIN')
        {
            if($fValue2>$fValue1)
            {
                return $fValue1;
            }
            else
            {
                return $fValue2;
            }
        }
    }
    //function calculate
    function getUsaStates($data)
    {
        if(!empty($data))
        {
            $szState = ''; 
            $szPostcode = $data['zip'];
            $szState = $this->getUsaStatesByZip($szPostcode);    
            
            if(!empty($szState))
            {
                return $szState;
            }
            else
            {
                $szAddressString = $data['zip'].", ".$data['city'].", ".$data['country']; 
                $addressDetailsAry = reverse_geocode($szAddressString); 
                if(!empty($addressDetailsAry))
                {
                    $szState = $addressDetailsAry['szStateCode'];
                    return $szState;
                } 
            }
        }
    }
    
    function getUsaStatesByZip($szPostcode)
    {
        if(!empty($szPostcode))
        {
            /*
            * We check first 3 characters of postcode
            */
            $szPostcode = substr($szPostcode,0,3);
            $query="
                SELECT
                    szState
                FROM	
                    ".__DBC_SCHEMATA_USA_STATES__."
                WHERE
                    szPostcode = '".mysql_escape_custom($szPostcode)."'
            "; 
            if($result=$this->exeSQL($query))
            {
                $szState = '';
                if($this->iNumRows>0)
                {
                    $row=$this->getAssoc($result);
                    $szState = $row['szState'];
                }
                return $szState; 
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false; 
            }
        }
    }
    
    public static function setCarrierProviders($data)
    {
        self::$carrierAccountIdAry = $data['all'];
        self::$tntCarrierAccountIdAry = $data['tnt']; 
        self::$otherCarrierAccountIdAry = $data['others'];
    }
    
    function addUsaStates($data)
    {
        $query="
            INSERT INTO
                ".__DBC_SCHEMATA_USA_STATES__."
            (
                szPostcode,
                szState,
                iActive
            )
            VALUES
            (
                '".mysql_escape_custom($data['szPostcode'])."',
                '".mysql_escape_custom($data['szState'])."',
                1
            )
        ";
        //echo "<br><br>".$query;
        //$result1=$this->exeSQL($query);
    }
    
    public function downloadLabelFromEasypost($szEasypostLabelUrl)
    { 
        if(!empty($szEasypostLabelUrl))
        { 
            $ch = curl_init ("$szEasypostLabelUrl");
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
            $rawdata = curl_exec($ch);
            curl_close($ch);

            if(!empty($rawdata))
            {			
                $szRandomKey = time()."_".mt_rand(9,9999);
                $szFileName = "transporteca_courier_label_".$szRandomKey.".pdf";
                $szFilePath = __UPLOAD_COURIER_LABEL_PDF__."/temp/".$szFileName ; 
                
                if(file_exists($szFilePath))
                {
                    @unlink($szFilePath);
                } 
                // Using fwrite to save the above
                $fp = fopen($szFilePath, 'w+'); 
                // Write the file
                fwrite($fp, $rawdata); 
                // And then close it.
                fclose($fp); 
                
                if(file_exists($szFilePath))
                {
                    $szNewFilePath = __UPLOAD_COURIER_LABEL_PDF__."/".$szFileName ; 
                    @copy($szFilePath, $szNewFilePath);
                } 
                $uploadedPdfFileAry = array();
                $uploadedPdfFileAry['szLabelPdfFilePath'] = $szNewFilePath;
                $uploadedPdfFileAry['szLabelPdfFileName'] = $szFileName;
                return $uploadedPdfFileAry;
            }
            else
            {
                return false;
            }
        } 
    }
    
    function createShipmentLabel($data)
    {
        if($data['idCourierProvider']==__FEDEX_API__) //Fedex
        {
            //@ TO DO
        }
        else if($data['idCourierProvider']==__UPS_API__) //Fedex
        {
            //@ TO DO
        }
        else if($data['idCourierProvider']==__TNT_API__) //Fedex
        {
            $this->createShipmentLabelForTNT($data);
        }
    }
    
    function createShipmentLabelForTNT($data)
    {
        $szTNTHostUrl = "http://iconnection.tnt.com:81/shippergate2.asp"; 
        
        $szCustomerXml = $this->buildLabelRequestForTNT($data); 
        try
        {
            $ch = curl_init($szTNTHostUrl); 
            $szAuthString = $data['szUsername'].":".$data['szPassword'] ;
            //,                'Authorization: Basic '. base64_encode($szAuthString)
            $headers = array(
                'Content-Type: application/x-www-form-urlencoded'
            );

            $options = array(  
                CURLOPT_HTTPHEADER => $headers,  
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => trim($szCustomerXml),
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_TIMEOUT => '60'
            ); 
 
            $proxy = "164.39.122.33";
            curl_setopt_array($ch, $options);
            curl_setopt($ch, CURLOPT_PROXY, $proxy);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 

            $cUrlResponseAry = array();
            $cUrlResponseXml = curl_exec($ch);  
            $culrInfoAry = curl_getinfo($ch);  
            
//            echo "Response <br><br> ";
//            print_R($cUrlResponseXml);
//            echo "<br><br> Header <br><br>";
//            print_R($culrInfoAry);
            
            $fedexApiLogsAry = array(); 
            $fedexApiLogsAry['iWebServiceType'] = 11 ;
            $fedexApiLogsAry['szRequestData'] = $szCustomerXml;
            $fedexApiLogsAry['szResponseData'] = "Http Code: ".$culrInfoAry['http_code']."\n\n".$cUrlResponseXml;
            $kWebServices = new cWebServices();
            $kWebServices->addFedexApiLogs($fedexApiLogsAry);
        }
        catch (Exception $ex)
        {
            echo "Exception<br> ";
            print_R($ex);
        }
    }
    
    function buildLabelRequestForTNT($data)
    { 
        $szConsignmentNumber = $this->generateNumericToken(9);   
        
        $szTntRequestXml = '
            <?xml version="1.0" encoding="UTF-8"?>
                <ESHIPPER>
                    <LOGIN>
                        <COMPANY>'.$data['szUsername'].'</COMPANY>
                        <PASSWORD>'.$data['szPassword'].'</PASSWORD>
                        <APPID>IN</APPID>
                        <APPVERSION>2</APPVERSION>
                    </LOGIN>
                <CONSIGNMENTBATCH>
                    <SENDER>
			<COMPANYNAME>Sender Co</COMPANYNAME>
			<STREETADDRESS1>TEST DO NOT COLLECT1</STREETADDRESS1>
			<STREETADDRESS2>TEST DO NOT COLLECT2</STREETADDRESS2>
			<STREETADDRESS3>TEST DO NOT COLLECT3</STREETADDRESS3>
			<CITY>Atherstone</CITY>
			<PROVINCE>Warwickshire</PROVINCE>
			<POSTCODE>CV9 2ry</POSTCODE>
			<COUNTRY>DK</COUNTRY>
			<ACCOUNT>'.$data['szAccountNumber'].'</ACCOUNT>
			<VAT/>
			<CONTACTNAME>Mr Contact</CONTACTNAME>
			<CONTACTDIALCODE>01827</CONTACTDIALCODE>
			<CONTACTTELEPHONE>717733</CONTACTTELEPHONE>
			<CONTACTEMAIL>contact@tnt.com</CONTACTEMAIL>
			<COLLECTION>
                            <COLLECTIONADDRESS>
                                <COMPANYNAME>Collection Name</COMPANYNAME>
                                <STREETADDRESS1>TEST DO NOT COLLECT4</STREETADDRESS1>
                                <STREETADDRESS2>TEST DO NOT COLLECT5</STREETADDRESS2>
                                <STREETADDRESS3>TEST DO NOT COLLECT6</STREETADDRESS3>
                                <CITY>Ann Arbor</CITY>
                                <PROVINCE>MI</PROVINCE>
                                <POSTCODE>48113</POSTCODE>
                                <COUNTRY>US</COUNTRY>
                                <VAT/>
                                <CONTACTNAME>Mr Contact</CONTACTNAME>
                                <CONTACTDIALCODE>00000</CONTACTDIALCODE>
                                <CONTACTTELEPHONE>700005</CONTACTTELEPHONE>
                                <CONTACTEMAIL>contact@tnt.com</CONTACTEMAIL>
                            </COLLECTIONADDRESS>
                            <SHIPDATE>04/03/2016</SHIPDATE>
				<PREFCOLLECTTIME>
                                    <FROM>09:00</FROM>
                                    <TO>10:00</TO>
				</PREFCOLLECTTIME>
				<ALTCOLLECTTIME>
                                    <FROM>11:00</FROM>
                                    <TO>12:00</TO>
				</ALTCOLLECTTIME>
                            <COLLINSTRUCTIONS>Go to Denamrk</COLLINSTRUCTIONS>
			</COLLECTION>
                    </SENDER>
                    <CONSIGNMENT>
			<CONREF>ref2</CONREF>
			<DETAILS>
                            <RECEIVER>
                                <COMPANYNAME>Receiver Name</COMPANYNAME>
                                <STREETADDRESS1>TEST DO NOT COLLECT7</STREETADDRESS1>
                                <STREETADDRESS2>TEST DO NOT COLLECT8</STREETADDRESS2>
                                <STREETADDRESS3>TEST DO NOT COLLECT9</STREETADDRESS3>
                                <CITY>Arhus</CITY>
                                <PROVINCE/>
                                <POSTCODE>8000</POSTCODE>
                                <COUNTRY>DK</COUNTRY>
                                <VAT/>
                                <CONTACTNAME>Mr Frank</CONTACTNAME>
                                <CONTACTDIALCODE>1672</CONTACTDIALCODE>
                                <CONTACTTELEPHONE>987432</CONTACTTELEPHONE>
                                <CONTACTEMAIL>email@mail.com</CONTACTEMAIL>
                            </RECEIVER>
                            <DELIVERY>
                                <COMPANYNAME>Delivery Name</COMPANYNAME>
                                <STREETADDRESS1>TEST DO NOT COLLECT10</STREETADDRESS1>
                                <STREETADDRESS2>TEST DO NOT COLLECT11</STREETADDRESS2>
                                <STREETADDRESS3>TEST DO NOT COLLECT12</STREETADDRESS3>
                                <CITY>Arhus</CITY>
                                <PROVINCE/>
                                <POSTCODE>8000</POSTCODE>
                                <COUNTRY>DK</COUNTRY>
                                <VAT/>
                                <CONTACTNAME>Mr Frank</CONTACTNAME>
                                <CONTACTDIALCODE>1672</CONTACTDIALCODE>
                                <CONTACTTELEPHONE>987432</CONTACTTELEPHONE>
                                <CONTACTEMAIL>email@mail.com</CONTACTEMAIL>
                            </DELIVERY>
                            <CUSTOMERREF>DISKS</CUSTOMERREF>
                            <CONTYPE>N</CONTYPE>
                            <PAYMENTIND>S</PAYMENTIND>
                            <ITEMS>2</ITEMS>
                            <TOTALWEIGHT>1.4</TOTALWEIGHT>
                            <TOTALVOLUME>1.0</TOTALVOLUME>
                            <CURRENCY>GBP</CURRENCY>
                            <GOODSVALUE>180.00</GOODSVALUE>
                            <INSURANCEVALUE>150.00</INSURANCEVALUE>
                            <INSURANCECURRENCY>GBP</INSURANCECURRENCY>
                            <SERVICE>15N</SERVICE>
                            <OPTION>PR</OPTION>
                            <DESCRIPTION>assorted office accessories</DESCRIPTION>
                            <DELIVERYINST>ggg</DELIVERYINST>
                            <PACKAGE>
                                <ITEMS>3</ITEMS>
                                <DESCRIPTION>box 1</DESCRIPTION>
                                <LENGTH>0.1</LENGTH>
                                <HEIGHT>0.2</HEIGHT>
                                <WIDTH>0.3</WIDTH>
                                <WEIGHT>0.4</WEIGHT> 
                            </PACKAGE>
                            <PACKAGE>
                                <ITEMS>3</ITEMS>
                                <DESCRIPTION>box 2</DESCRIPTION>
                                <LENGTH>0.5</LENGTH>
                                <HEIGHT>0.6</HEIGHT>
                                <WIDTH>0.7</WIDTH>
                                <WEIGHT>0.8</WEIGHT> 
                            </PACKAGE>
			</DETAILS>
                    </CONSIGNMENT>
                </CONSIGNMENTBATCH>
                <ACTIVITY>
                    <PRINT>
			<CONNOTE>
                            <CONREF>ref2</CONREF>
			</CONNOTE>
                    </PRINT>
                </ACTIVITY>
            </ESHIPPER>
        ';   
        return $szTntRequestXml;
    }
    
    function getAllCarrierAccountEasypost()
    { 
        $kWhsSearch = new cWHSSearch();
        $szEasypostApi = $kWhsSearch->getManageMentVariableByDescription('__EASYPOST_API_KEY__');
        if(!empty($szEasypostApi))
        {
            $carrierAccountAry = array();
            try
            {
                \EasyPost\EasyPost::setApiKey($szEasypostApi);  
                $cas = \EasyPost\CarrierAccount::all();
                $kObject = new \EasyPost\Object();  
                $kObject->_values = $cas; 
                $responseAry = array();
                $responseAry = $kObject->__toArray(true); 
                
                $responseAry = $responseAry['_values'];
                if(!empty($responseAry))
                {
                    foreach($responseAry as $responseArys)
                    {
                        $carrierAccountAry[] = $responseArys['id']; 
                    }
                }  
                if(!empty($carrierAccountAry))
                {
                    /*
                    * Replacing array just minimize logs
                    */
                    $responseAry = $carrierAccountAry;
                }
            } 
            catch (Exception $ex) 
            {
                $responseAry = $ex;
            }  
            $fedexApiLogsAry = array(); 
            $fedexApiLogsAry['iWebServiceType'] = 12;
            $fedexApiLogsAry['szRequestData'] = "Fetching Carrier account list";
            $fedexApiLogsAry['szResponseData'] = print_R($responseAry,true);
            $kWebServices = new cWebServices();
            $kWebServices->addFedexApiLogs($fedexApiLogsAry);
            return $carrierAccountAry;
        } 
    }
    
    function generateNumericToken($number)
    {
        $arr = array('1', '2', '3', '4', '5', '6',
                     '7', '8', '9', '0');
        $token = "";
        for ($i = 0; $i < $number; $i++){
            $index = rand(0, count($arr) - 1);
            $token .= $arr[$index];
        }
        return $token ;
    }
}
?>