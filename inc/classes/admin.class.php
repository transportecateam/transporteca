<?php
/**
 * This file is the container for all admin related functionality.
 * All functionality related to admin details should be contained in this class.
 *
 * admin.class.php
 *
 * @copyright Copyright (C) 2012 Transporteca, LLC
 * @author Anil
 * @package Transporteca Development
 */
if(!isset($_SESSION))
{
	session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_INC__ . "/functions.php" );
require_once( __APP_PATH_CLASSES__ . "/database.class.php");

class cAdmin extends cDatabase
{
    /**
     * These are the variables for each admin.
     */
	var $id;
	var $szFirstName;
	var $szLastName;
	var $szEmail;
	var $szPassword;
	var $iActive;
	var $dtCreatedOn;
	var $dtUpdatedOn;
	var $szOldEmail;
	var	$szOldPassword;
	var $currencyId;
	var $szCurrency;
	var $iPricing;
	var $iBooking;
	var $iSettling;
	var $szFeed;
	var $idCountries;
	var $szCountryName;
	var $szCountryISO;
	var $fStandardTruckRate;
	var $iActiveFromDropdown;
	var $iActiveToDropdown;
	var $szHeading;
	var $szDescription;
	var $iId;
	var $iValue;
	var $szNewFirstName;
	var $szNewLastName;
	var $szNewEmail;
	var $szNewPassword;
	var $dtCreateOn;
	var $szComment;
	var $version;
	var $szTemplateSubject;
	var $iAllAccess;
	var $t_base="management/Error/";
	var $szHeadingArr = array();
	var $iValueArr = array();
	var $iIdArr = array();
	var $iAuto;
	var $iPasswordUpdated;
	var $szUrl;
	var $searchField;
	var $idOriginCountry;
	var $idDestinationCountry;
	var $idForwarder;
	var $szLatitude;
	var $szLongitude;
	
	function __construct()
	{
		parent::__construct();
		return true;
	}
	
	function sanitizeData($value)
	{
            $checked=sanitize_all_html_input(trim($value));
            return $checked;
	}
	
	function adminLogin($data)
	{	
		$this->set_szEmail($this->sanitizeData($data['szEmail']));
		$this->set_szPassword($this->sanitizeData($data['szPassword']));
	
		if ($this->error === true)
		{
			return false;
		}
		
		$query="
			SELECT 
				id,
				szEmail,
				szPassword,
                                szProfileType
			FROM 
				".__DBC_SCHEMATA_MANAGEMENT__."
			WHERE 
				szEmail='".mysql_escape_custom($this->szEmail)."'
			AND 
				iActive= 1	
			";
                if( ( $result = $this->exeSQL( $query ) ) )
                {
                        if( $this->getRowCnt() > 0 )
                        {
                                    $row=$this->getAssoc($result);
                                    $this->id = $row['id'];
                                    $password = $row['szPassword'];
                                    $this->szEmail = $row['szEmail'];
                                    $this->szProfileType = $row['szProfileType'];
                                    $iActive = $row['iActive'];
                        }
			else
                        {	
                            $this->addError( "szEmail" , t($this->t_base.'messages/wrong_username_password') );
                            return false;
                        }
			if($password!=md5( $this->szPassword ))
			{	
				$this->addError( "szEmail" , t($this->t_base.'messages/wrong_username_password') );
				return false;
			}
			if($iActive!=1 && (int)$this->id>0)
			{
				$_SESSION['admin_id']=$this->id; 
                                $_SESSION['szProfileType']=$this->szProfileType; 
				return false;
			}
			else if((int)$this->id>0)
			{
                            $_SESSION['admin_id'] = $this->id;
                            $_SESSION['szProfileType']=$this->szProfileType; 
                            return true;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
        
        function searchAdminDetails($szSearchTerms)
        {
            if(!empty($szSearchTerms))
            { 
                $searchTermsAry = array();
                $searchTermsAry = explode(" ",$szSearchTerms);
                $query_and = ' AND ';
                $ctr = 0;
                if(!empty($searchTermsAry))
                {
                    foreach($searchTermsAry as $searchTermsArys)
                    {
                        $query_and .= "
                            (
                                szFirstName LIKE '%".mysql_escape_custom(trim($searchTermsArys))."%'
                            OR
                                szLastName LIKE '%".mysql_escape_custom(trim($searchTermsArys))."%'
                            OR
                                szEmail LIKE '%".mysql_escape_custom(trim($searchTermsArys))."%' 
                            )
                        "; 
                        $ctr++;
                        if(count($searchTermsAry)!=$ctr)
                        {
                            $query_and .= " AND ";
                        }
                        
                    }
                }
                
		$query="
                    SELECT
                        id, 
                        szEmail,
                        szFirstName,
                        szLastName 
                    FROM 
                        ".__DBC_SCHEMATA_MANAGEMENT__."
                    WHERE 
                        iActive = '1'
                     $query_and 
                ";
//		echo $query;
//                die;
		if($result = $this->exeSQL($query))
		{ 
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {
                        $ret_ary[$ctr] = $row;
                        $ret_ary[$ctr]['szCustomerCompanyName'] = 'Transporteca';
                        $ctr++;
                    }  
                    return $ret_ary; 
		}
		else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
        }
	
	function getAdminDetails($id)
	{
            if((int)$id>0)
            {
                //szCapsule,
                $query="
                    SELECT
                        id,
                        szEmail,
                        szFirstName,
                        szLastName,
                        iActive,
                        iPasswordUpdated,
                        dtCreatedOn,
                        dtUpdatedOn,
                        szTitle,
                        idInternationalDialCode,
                        szDirectPhone as szPhone,
                        szProfileType
                    FROM
                        ".__DBC_SCHEMATA_MANAGEMENT__."
                    WHERE
                        id='".(int)$id."'		
                ";
                //echo "<br /> ".$query."<br />";

                if( ( $result = $this->exeSQL( $query ) ) )
                {
                    if( $this->getRowCnt() > 0 )
                    {
                        $row=$this->getAssoc($result);
                        $this->szEmail=$row['szEmail'];
                        $this->id=$row['id'];
                        $this->szFirstName=ucwords(strtolower($row['szFirstName']));
                        $this->szLastName=ucwords(strtolower($row['szLastName']));
                        $this->iActive=$row['iActive'];
                        $this->dtCreateOn=$row['dtCreateOn'];
                        $this->dtUpdatedOn = $row['dtUpdatedOn'];
                        $this->iPasswordUpdated = $row['iPasswordUpdated'];
                        //$this->szCapsule = $row['szCapsule']; 
                        $this->szTitle = $row['szTitle'];  
                        $this->idInternationalDialCode = $row['idInternationalDialCode']; 
                        $this->szPhone = $row['szPhone'];  
                        $this->szProfileType = $row['szProfileType'];
                        return $row;
                    }
                    else
                    {
                        return array();
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
            else
            {
                return false;
            }
	}
	
        function getAllActiveStoreAdmin($idAdminNotEqualTo=false,$iShowInDropdownFlag=false, $iNonCustomerServiceAgent=false)
	{ 
            if($idAdminNotEqualTo>0)
            {
                $query_and = " AND id != '".$idAdminNotEqualTo."' ";
                
            }
            
            if($iShowInDropdownFlag)
            {
                $query_and .= " AND iCustomerServiceAgent = '1' ";
            }
            
            if($iNonCustomerServiceAgent)
            {
                $query_and .= " AND iCustomerServiceAgent = '0' ";
            }
            //szCapsule,
            $column='';
            if(!$iNonCustomerServiceAgent)
            {
                $column=",
                    szEmail,
                    szFirstName,
                    szLastName,
                    iActive,
                    iPasswordUpdated,
                    dtCreatedOn,
                    dtUpdatedOn,
                    szTitle,
                    idInternationalDialCode,
                    szDirectPhone as szPhone";
            }
            $query="
                SELECT
                    id
                    $column
                FROM
                    ".__DBC_SCHEMATA_MANAGEMENT__."
                WHERE
                    iActive ='1'		
                 $query_and
                ORDER BY
                    szFirstName, szLastName
            ";
            //echo "<br /> ".$query."<br />"; 
            if(($result = $this->exeSQL($query)))
            {
                if( $this->getRowCnt() > 0 )
                {
                    $ret_ary = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {
                        if(!$iNonCustomerServiceAgent)
                        {
                            $ret_ary[$ctr] = $row ;
                        }
                        else
                        {
                            $ret_ary['idFileOwnerAry'][$ctr] = $row['id'] ;
                        }
                        $ctr++;
                    }
                    return $ret_ary;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
	}
        
	function changePassword($data,$id)
	{
		if(is_array($data) && (int)$id>0)
		{
			$this->set_szOldPassword(trim(sanitize_all_html_input($data['szOldPassword'])));
			$this->set_szPassword(trim(sanitize_all_html_input($data['szNewPassword'])));
			$this->set_szConNewPassword(trim(sanitize_all_html_input($data['szConPassword'])));
			$this->set_id(trim(sanitize_all_html_input($id)));
			
			if ($this->error === true)
			{
				return false;
			}
			
			$query="
				SELECT
					szPassword
				FROM
					".__DBC_SCHEMATA_MANAGEMENT__."
				WHERE
					id='".(int)$this->id."'
				";
			if( ( $result = $this->exeSQL( $query ) ) )
			{
	            if( $this->getRowCnt() > 0 )
	            {
	            	$row=$this->getAssoc($result);
	            	$password=$row['szPassword'];
	            }
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
			
			if($password!=md5($this->szOldPassword))
			{
				$this->addError( "szOldPassword" , t($this->t_base.'messages/wrong_old_password') );
				return false;
			}
			
			if(!empty($this->szPassword) && $this->szPassword!=$this->szConNewPassword)
			{
				$this->addError( "szConfirmPassword" , t($this->t_base.'messages/match_password') );
				return false;
			}

			$query="
            	UPDATE
            		".__DBC_SCHEMATA_MANAGEMENT__."
            	SET
            		szPassword='".mysql_escape_custom(md5($this->szPassword))."'
				WHERE
					id='".(int)$this->id."'
			";
		
			if( $result = $this->exeSQL( $query ) )
			{
				return true;	
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function updateAdmininfo($data,$id)
	{		
            if(is_array($data) && (int)$id>0)
            {
                $this->set_szFirstName(trim(sanitize_all_html_input($data['szFirstName'])));
                $this->set_szLastName(trim(sanitize_all_html_input($data['szLastName'])));
                $this->set_szTitle(trim(sanitize_all_html_input($data['szTitle'])));

                $this->set_id($id);
                if($this->error==true)
                {
                    return false;
                }
                
                $query="
                    UPDATE 
                        ".__DBC_SCHEMATA_MANAGEMENT__." 
                    SET 
                        `szFirstName`='".mysql_escape_custom(ucwords(strtolower($this->szFirstName)))."',
                        `szLastName`='".mysql_escape_custom(ucwords(strtolower($this->szLastName)))."',
                        `szTitle` = '".mysql_escape_custom(ucwords(strtolower($this->szTitle)))."' 
                    WHERE 
                        `id`='".(int)$this->id."'  
                ";		

                if($result=$this->exeSQL($query))
                {
                    return true;  	 									
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }		
            }
            else
            {
                return false;
            }	
	}
		
	function updateAdminContact_info($data,$id)
	{
            if(is_array($data) && (int)$id>0)
            {	
                $this->set_szOldEmail(trim(sanitize_all_html_input(strtolower($data['szOldEmail']))));
                $this->set_idInternationalDialCode(trim(sanitize_all_html_input(strtolower($data['idInternationalDialCode']))));
                $this->set_szPhoneNumber(trim(sanitize_all_html_input(strtolower($data['szPhoneNumber']))));
                
                $this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szEmail']))));
                $this->set_id($id);
			
                if(!empty($this->szEmail) && $this->isAdminEmailExist($this->szEmail,$this->id) )
                {
                    $this->addError( "szEmail" , t($this->t_base.'messages/already_exists') );
                }
                if($this->error==true)
                {
                        return false;
                }
			
                if($this->szEmail != $this->szOldEmail)
                {   
                    $query="
            		UPDATE
                            ".__DBC_SCHEMATA_MANAGEMENT__."
            		SET
                            iActive='0'
            		WHERE
                            id='".(int)$this->id."'
                    ";
				
                    if($result = $this->exeSQL( $query ))
                    {
                        $this->resendActivationCode($this->id);
                    }
                    else
                    {
                        $this->error = true;
                        $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                        $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                        return false;
                    }
                }					
                
                $query="
                    UPDATE 
                        ".__DBC_SCHEMATA_MANAGEMENT__." 
                    SET 
                        `szEmail`='".mysql_escape_custom($this->szEmail)."',
                        `idInternationalDialCode`='".mysql_escape_custom($this->idInternationalDialCode)."',
                        `szDirectPhone`='".mysql_escape_custom($this->szPhoneNumber)."'
                    WHERE 
                        `id`='".(int)$this->id."'  
                ";		

                if($result=$this->exeSQL($query))
                {
                    return true;  	 									
                }
                else
                {	
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }		
            }
            else
            {
                return false;
            }	
	}
	
	function resendActivationCode($id)
	{
		$query="
			SELECT
				id,
				szEmail,
				szFirstName
			FROM
			".__DBC_SCHEMATA_MANAGEMENT__."
			WHERE
				id='".(int)$id."'
			AND
				iActive='0'
			";
		if( ( $result = $this->exeSQL( $query ) ) )
		{
            if( $this->getRowCnt() > 0 )
            {	
			    $row=$this->getAssoc($result);        
            	$szEmail=$row['szEmail'];
            	$szFirstName=$row['szFirstName'];
            	$id=$row['id'];
            	$ConfirmKey=md5(date("Y-m-d h:i:s").random_number_ConfirmKey());
            	
            	$query="
            		UPDATE
            			".__DBC_SCHEMATA_MANAGEMENT__."
            		SET
            			szActivationKey='".mysql_escape_custom($ConfirmKey)."',
            			dtActivationCodeSent = now()
            		WHERE
            			id='".(int)$id."'
            		";
            	
            		if( $result = $this->exeSQL( $query ))
            		{
	            		$replace_ary['szFirstName']=$this->szFirstName;
						$replace_ary['szEmail']=$this->szEmail;
						$confirmationLink=__MANAGEMENT_HOME_PAGE_URL__."/confirmation.php?confirmationKey=".$ConfirmKey;
						$confirmationLinkHttps=__MANAGEMENT_HOME_PAGE_URL_SECURE__."/confirmation.php?confirmationKey=".$ConfirmKey;
						$replace_ary['szLink']="<a href='".$confirmationLink."'>CLICK TO VERIFY E-MAIL ADDRESS</a>";
						$replace_ary['szHttpsLink']=$confirmationLinkHttps;
	                    $replace_ary['supportEmail'] = __STORE_SUPPORT_EMAIL__;
	                    createEmail(__CREATE_MANAGEMENT_ACCOUNT__, $replace_ary,$this->szEmail, __CREATE_MANAGEMENT_ACCOUNT_SUBJECT__, __STORE_SUPPORT_EMAIL__,$this->id, __STORE_SUPPORT_EMAIL__,__FLAG_FOR_MANAGEMENT__);
	                    return true;
            		}
            		else
            		{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
            		}      
            }
            else
            {
            	return false;
            }
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
										
													
	function isAdminEmailExist( $email,$id=0, $active_user=false )
	{
            $query="
                SELECT
                    id
                FROM
                    ".__DBC_SCHEMATA_MANAGEMENT__."
                WHERE
                    szEmail = '".mysql_escape_custom($email)."' 
            ";
            if($id>0)
            {
                $query .="
                    AND
                        id<>'".(int)$id."'		
                ";
            }
            if($active_user)
            {
                $query .= " AND iActive = '1' AND iCustomerServiceAgent = '1' ";
            }
            //echo $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
        	// Check if the user exists
                if( $this->getRowCnt() > 0 )
                {
                    while($row=$this->getAssoc($result))
                    { 
                        $this->idTransportecaAdmin = $row['id']; 
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
        
        function isInvestorsEmailExist( $email,$id=0 )
	{
            $query="
                SELECT
                    id
                FROM
                    ".__DBC_SCHEMATA_INVESTORS__."
                WHERE
                    szEmail = '".mysql_escape_custom($email)."'	
            ";
            if($id>0)
            {
                $query .="
                    AND
                        id<>'".(int)$id."'		
                ";
            }
            //echo $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
        	// Check if the user exists
                if( $this->getRowCnt() > 0 )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
	
	function updateAdminPassword_info($data,$forwarderId)
	{
		$this->set_szOldPassword(trim(sanitize_all_html_input($data['szOldPassword'])));
		$this->set_szPassword(trim(sanitize_all_html_input($data['szNewPassword'])));
		$this->set_szReTypePassword(trim(sanitize_all_html_input($data['szConPassword'])));
		$this->set_id(trim(sanitize_all_html_input($forwarderId)));
			
		if ($this->error == true)
		{
			return false;
		}
		$query="
			SELECT
				szPassword
			FROM
				".__DBC_SCHEMATA_MANAGEMENT__."
			WHERE
				id='".(int)$this->id."'
			";
			
		if( ( $result = $this->exeSQL( $query ) ) )
		{
            if( $this->getRowCnt() > 0 )
            {
            	$row=$this->getAssoc($result);
            	$password=$row['szPassword'];
            }
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
		
		if($password != md5($this->szOldPassword))
		{	
			$this->addError( "szOldPassword" , t($this->t_base.'messages/wrong_old_password') );
			return false;
		}
		if(!empty($this->szPassword) && $this->szPassword!=$this->szRetypePassword)
		{
			$this->addError( "szRetypePassword" , t($this->t_base.'messages/match_password') );
			return false;
		}
		if($password == md5($this->szOldPassword))
		{
			$query="
	            UPDATE
	            	".__DBC_SCHEMATA_MANAGEMENT__."
	            SET
	            	szPassword='".mysql_escape_custom(md5($this->szPassword))."'
				WHERE
					id='".(int)$this->id."'
				";
			
			if($result = $this->exeSQL( $query ))
			{
				return true;
			}	
			else
			{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
			}
		}
		else
		{
			return false;
		}
		
	}
	
	function forgotPassword($szEmail)
	{
            $this->set_szEmail(sanitize_all_html_input(strtolower($szEmail)));
            if ($this->error === true)
            {
                return false;
            }
            $query="
                SELECT
                    id,
                    szFirstName,
                    szLastName
                FROM
                    ".__DBC_SCHEMATA_MANAGEMENT__."
                WHERE
                    szEmail = '".mysql_escape_custom($this->szEmail)."'
            "; 
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                if( $this->getRowCnt() > 0 )
                {
                    $row=$this->getAssoc($result);
                    $this->id=$row['id'];
                    $password=create_password();
                    $query="
            		UPDATE
                            ".__DBC_SCHEMATA_MANAGEMENT__."
            		SET
                            szPassword='".mysql_escape_custom(md5($password))."',
                            iPasswordUpdated = 1
            		WHERE
                            szEmail = '".mysql_escape_custom($this->szEmail)."'	
                    "; 
                    if($result = $this->exeSQL( $query ))
                    {
                        $replace_ary['szFirstName']=$row['szFirstName'];
                        $replace_ary['szLastName']=$row['szLastName'];
                        $replace_ary['szPassword']=$password;
                        $replace_ary['szEmail']=$this->szEmail;
                        createEmail(__FORGOT_PASSWORD__, $replace_ary,$this->szEmail, __FORGOT_PASSWORD_SUBJECT__, __STORE_SUPPORT_EMAIL__,$this->id, __STORE_SUPPORT_EMAIL__,__FLAG_FOR_MANAGEMENT__); 
                        return true;
                    }
	            else
                    {
                        $this->error = true;
                        $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                        $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                        return false;
                    }		
                }
                else
                {
                    $this->addError( "szEmail" , t($this->t_base.'messages/email_not_exists') );
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
	
	function excangeRates($id,$flag=0)
	{
            if($id>0)
            {
                $query="
                    SELECT 
                        c.id,
                        c.szCurrency,
                        c.iPricing,
                        c.iBooking,
                        c.iSettling,
                        c.szFeed,
                        c.szBankName,
                        c.szSortCode,
                        c.szAccountNumber,
                        c.szIBANNumber,
                        c.szSwiftNumber,
                        c.szNameOnAccount
                ";
                if($flag==0)
                {
                    $query.="
                            ,
                        e.fUsdValue,
                        e.dtDate as dtDate
                    ";
                }	
                $query.="
                    FROM
                            ".__DBC_SCHEMATA_CURRENCY__." AS c
                    LEFT JOIN 
                            ".__DBC_SCHEMATA_CURRENCY_EXCHANGE_RATE__." AS e
                    ON (c.id=e.idCurrency )
                    LEFT OUTER JOIN ".__DBC_SCHEMATA_CURRENCY_EXCHANGE_RATE__." c2 
                    ON ( c.id = c2.idCurrency AND 
                    (
                        e.dtDate < c2.dtDate
                    OR 
                        e.dtDate = c2.dtDate
                    AND 
                        e.id < c2.id) )
                    WHERE 
                        c2.id IS NULL
                ";	
				
                if($flag>0)
                {
                    $query.="
                        AND
                            c.id='".(int)$flag."' 			
                    "; 
                }
                $query.="
                    AND
                        c.iActive='1'	
                    ORDER BY 
                        c.szCurrency		
                "; 
                //echo $query ;
                if( ( $result = $this->exeSQL( $query ) ) )
                {	
                    $excangeRate=array();
                    if($this->iNumRows>0)
                    {
                        $ctr=0;
                        while($row=$this->getAssoc($result))
                        {	
                            if($flag==0)
                            {
                                $excangeRate[]=$row;
                            }
                            else if($flag>0)
                            {	
                                $this->id=$row['id'];
                                $this->szCurrency=$row['szCurrency'];
                                $this->iPricing=$row['iPricing'];
                                $this->iBooking=$row['iBooking'];
                                $this->iSettling=$row['iSettling'];
                                $this->szFeed=$row['szFeed'];
                                $excangeRate=$row;
                                break;
                            }
                            $ctr++;
                        }
                        return $excangeRate ;
                    }
                    else
                    {
                        return array();
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                } 
            }
            else
            {
                return false;
            }
	}
	
	function vildate_currency_details($data)
	{ 
            if(!empty($data))
            {
                $this->set_szCurrency($this->sanitizeData($data['currency']));
                $this->set_iPricing($this->sanitizeData($data['pricing']));
                $this->set_iBooking($this->sanitizeData($data['booking']));
                $this->set_iSettling($this->sanitizeData($data['settling']));
                $this->set_szFeed($this->sanitizeData($data['feed']));
                $bFieldRequired = false;
                if($this->iBooking==1)
                {
                    $bFieldRequired = true;
                }

                $this->set_szBankName($this->sanitizeData($data['szBankName']),$bFieldRequired);
                $this->set_szSortCode($this->sanitizeData($data['szSortCode']),false);
                $this->set_szAccountNumber($this->sanitizeData($data['szAccountNumber']),$bFieldRequired);
                $this->set_szIBANNumber($this->sanitizeData($data['szIBANNumber']),$bFieldRequired);
                $this->set_szSwiftNumber($this->sanitizeData($data['szSwiftNumber']),$bFieldRequired);
                $this->set_szNameOnAccount($this->sanitizeData($data['szNameOnAccount']),$bFieldRequired); 
                
                if($this->error=== true)
                {
                    return false;
                }
                else
                {
                    return false;
                }
            }
	}
        
	function addRates($id,$rates)
	{	 
            if($id>0 && (!empty($rates)))
            {
                $this->vildate_currency_details($rates);
                if($this->error=== true)
                {
                    return false;
                }
                $this->validateCurrencyName(0);
                if($this->error=== true)
                {
                    return false;
                }

                if($ratesId==0)
                {
                    $query="
                        INSERT INTO 	
                            ".__DBC_SCHEMATA_CURRENCY__."
                        SET
                            szCurrency ='".mysql_escape_custom($this->szCurrency)."',
                            iPricing ='".mysql_escape_custom($this->iPricing)."',
                            iBooking ='".mysql_escape_custom($this->iBooking)."',
                            iSettling ='".mysql_escape_custom($this->iSettling)."',
                            szFeed ='".mysql_escape_custom($this->szFeed)."', 
                            szBankName ='".mysql_escape_custom($this->szBankName)."',
                            szSortCode ='".mysql_escape_custom($this->szSortCode)."',
                            szAccountNumber = '".mysql_escape_custom($this->szAccountNumber)."',
                            szIBANNumber = '".mysql_escape_custom($this->szIBANNumber)."',
                            szSwiftNumber ='".mysql_escape_custom($this->szSwiftNumber)."',
                            szNameOnAccount ='".mysql_escape_custom($this->szNameOnAccount)."',
                            iActive ='1'
                    ";
                } 
               // echo "<br>".$query."<br>";
               // die;
                if( ( $result = $this->exeSQL( $query ) ) )
                {	
                    $idCurrency=mysql_insert_id();
                    if($idCurrency>0)
                    {
                        $query="
                            INSERT INTO
                                ".__DBC_SCHEMATA_CURRENCY_EXCHANGE_RATE__."
                            SET
                                idCurrency='".(int)$idCurrency."',
                                fUsdValue=''
                        ";
                    }
                    else
                    {
                        return false;
                    }
                    if( ( $result = $this->exeSQL( $query ) ) )
                    {
                        return true;
                    }
                    else
                    {
                        $this->error = true;
                        $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                        $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                        return false;
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }			
            }	
            else
            {
                return false;
            }
	}
	function validateCurrencyName($id)
	{
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_CURRENCY__."
			WHERE
				szCurrency ='".mysql_escape_custom($this->szCurrency)."'
			AND	
				iActive='1'
			";
		
		if($id>0)
		{
			$query.="
				AND 
					id<>'".$id."'
				";
		}
		
		if( ( $result = $this->exeSQL( $query ) ) )
		{	
				if($this->iNumRows>0)
			{
				$this->addError( 'szCurrency' , t($this->t_base.'messages/currency_already_exists') );
               	return false;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
			
	}
	function checkExchangeRatesPricing($idCurrency)
	{
			$query="
				SELECT 
					count(w.id)+
					count(cc.id)+
					count(h.id) as number
				FROM
					".__DBC_SCHEMATA_WAREHOUSES_PRICING__." AS w,
					".__DBC_SCHEMATA_PRICING_CC__." AS cc,
					".__DBC_SCHEMATA_PRICING_HAULAGE__." AS h
				WHERE
					w.iOriginChargesApplicable =".$idCurrency."
				OR
					w.szOriginChargeCurrency = ".$idCurrency."
				OR
					w.szFreightCurrency= ".$idCurrency."
				OR
					w.iDestinationChargesApplicable = ".$idCurrency."
				OR
				 	w.szDestinationChargeCurrency = ".$idCurrency."	
				OR
				 	cc.szCurrency = ".$idCurrency."	
				OR 
				 	h.szHaulageCurrency = ".$idCurrency."
				 AND
					w.iActive = 1				
				 AND
					cc.iActive = 1				
				 AND
					h.iActive = 1				
					";
			//echo $query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				return	$row=$this->getAssoc($result);
				
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function checkExchangeRatesBooking($idCurrency)
	{
			$query="
				SELECT 
					count(id) as number
				FROM
					".__DBC_SCHEMATA_USERS__."
				WHERE
					szCurrency =".$idCurrency."
				AND
					iActive = 1	
					";
			//echo $query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
					return $row=$this->getAssoc($result);
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function checkExchangeRatesSettling($idCurrency)
	{
			$query="
				SELECT 
					count(id) as number
				FROM
					".__DBC_SCHEMATA_FROWARDER__."
				WHERE
					szCurrency =".$idCurrency."
				AND
					iActive = 1	
					";
			//echo $query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				return	$row = $this->getAssoc($result);
									
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function saveRates($id,$rates,$ratesId=0,$flag=false)
	{	
		if($id>0 && (!empty($rates)) && $ratesId>0)
		{	
			$this->currencyId=$ratesId;
			$this->vildate_currency_details($rates);    
			
			if($this->error=== true)
			{
				return false;
			}
			
			if(!$flag)
			{
				if(!$this->iPricing)
				{
				 	$pricing = $this->checkExchangeRatesPricing($this->currencyId);
				 	$pricingNotExpired = $this->checkExchangeRatesLCLServicePricing($this->currencyId);
					if((int)$pricing['number'] > 0 || (int)$pricingNotExpired>0)
					{
						$this->addError( "iPricing" , t($this->t_base.'fields/used_by_forwarder_for_pricing') );
					}
				}
				/*if(!$this->iBooking)
				{
					$booking = $this->checkExchangeRatesBooking($this->currencyId);
					if($booking['number'])
					{
						$this->addError( "iBooking" , t($this->t_base.'fields/used_by_customer') );
					}
				}*/
				if(!$this->iSettling)
				{
					$settling = $this->checkExchangeRatesSettling($this->currencyId);
					if($settling['number'])
					{
						$this->addError( "iSettling" , t($this->t_base.'fields/used_by_forwarder') );
					}
				}
			}
			$this->validateCurrencyName($ratesId);
			if($this->error=== true)
			{
				return false;
			}
				
			$query="
                            UPDATE
                                ".__DBC_SCHEMATA_CURRENCY__." AS c
                            SET
                                c.szCurrency ='".mysql_escape_custom($this->szCurrency)."',
                                c.iPricing   ='".mysql_escape_custom($this->iPricing)."',
                                c.iBooking   ='".mysql_escape_custom($this->iBooking)."',
                                c.iSettling  ='".mysql_escape_custom($this->iSettling)."',
                                c.szFeed     ='".mysql_escape_custom($this->szFeed)."',
                                c.szBankName     ='".mysql_escape_custom($this->szBankName)."',
                                c.szSortCode     ='".mysql_escape_custom($this->szSortCode)."',
                                c.szAccountNumber     ='".mysql_escape_custom($this->szAccountNumber)."',
                                c.szIBANNumber     ='".mysql_escape_custom($this->szIBANNumber)."',
                                c.szSwiftNumber     ='".mysql_escape_custom($this->szSwiftNumber)."',
                                c.szNameOnAccount     ='".mysql_escape_custom($this->szNameOnAccount)."'
                            WHERE	
                                c.id='".$ratesId."'	
                        ";
			//echo "<br> ".$query ;
			//die;
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
			
				if($flag)
				{
					return true;
				}
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	function validateExcahngeRates($id)
	{
		if($id>0)
		{
			$query="
				SELECT 
					(SELECT 
					 	COUNT(id)
					 FROM
					 	".__DBC_SCHEMATA_BOOKING__." 
					 WHERE
					 	idCurrency='".(int)$id."')
					 +
					 (SELECT 
					 	COUNT(id)
					 FROM
					 ".__DBC_SCHEMATA_USERS__."
					 WHERE 
					 szCurrency='".(int)$id."')
					 +
					 (SELECT 
					 	COUNT(id)
					 FROM
					 ".__DBC_SCHEMATA_FORWARDERS__."
					 WHERE 
						 szCurrency='".(int)$id."') as currencySum
					";
			
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				if( $this->getRowCnt() > 0 )
				{	
					$row=$this->getAssoc($result);
					if($row['currencySum']>0)
					{	
						return false;
					}
					else if($row['currencySum']==0)
					{
						$query="
							SELECT
								(SELECT 
								 	COUNT(id)
								 FROM
								 ".__DBC_SCHEMATA_PRICING_CC__."
								 WHERE 
									 szCurrency='".(int)$id."')
								 +
								 (SELECT 
								 	COUNT(id)
								 FROM
								 ".__DBC_SCHEMATA_PRICING_HAULAGE__."
								 WHERE 
									 szHaulageCurrency='".(int)$id."')
									+
								 (SELECT 
								 	COUNT(id)
								 FROM
								 ".__DBC_SCHEMATA_FORWARDER_HAULAGE_ARCHIVE__."
								 WHERE 
									 szHaulageCurrency='".(int)$id."') AS finalCurrencyCheckSum			
								";
						
						
						if( ( $result = $this->exeSQL( $query ) ) )
						{	
							if( $this->getRowCnt() > 0 )
							{	
								$row=$this->getAssoc($result);
								if($row['finalCurrencyCheckSum']>0)
								{
									return false;	
								}
								else if($row['finalCurrencyCheckSum']==0)
								{
									return true;
								}
							}
						}
						else
						{
							$this->error = true;
							$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
							$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
							return false;
						}
					}
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}	
	}
	
	function deleteExchangeRates($adminId,$id)
	{
		if($adminId && $id)
		{
			$status=$this->validateExcahngeRates($id);
			if($status)
			{
				$query="
					UPDATE
						".__DBC_SCHEMATA_CURRENCY__."
					SET
						iActive='0'
					WHERE 
						id='".(int)$id."'		
				";
				if( ( $result = $this->exeSQL( $query ) ) )
				{	
					return true;
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
        
	function excangeRatesDownload($id)
	{
            if($id>0)
            {
                $dtBefore90Day = date("Y-m-d h:i:s", strtotime('-90 days'));
                $query="
                    SELECT 
                        c.szCurrency,
                        c.szBankName,
                        c.szSortCode,
                        c.szAccountNumber,
                        c.szSwiftNumber,
                        c.szNameOnAccount,
                        c.szIBANNumber,
                        e.fUsdValue,
                        e.dtDate
                    FROM 
                        ".__DBC_SCHEMATA_CURRENCY__."	AS c
                    INNER JOIN
                        ".__DBC_SCHEMATA_CURRENCY_EXCHANGE_RATE__." AS e
                    ON
                        (c.id=e.idCurrency)	
                    WHERE	
                        c.iActive='1'
                    AND 
                        e.dtDate<= NOW() 
                    AND 
                        e.dtDate>='".$dtBefore90Day."'	
                ";
//                echo $query;
                if( ( $result = $this->exeSQL( $query ) ) )
                {	
                    $excangeRate=array();
                    if($this->iNumRows>0)
                    {
                        $ctr=0;
                        while($row=$this->getAssoc($result))
                        {	
                            $excangeRate[]=$row; 
                        }
                        return $excangeRate ;
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
            else
            {
                return false;
            }
	}
	
	function downloadExchangeRates($id)
	{
		if($id>0)
		{
			require( __APP_PATH_CLASSES__ . "/PHPExcel.php" );
			$data=$this->excangeRatesDownload($id);
                        
			$sheetIndex=0;
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->createSheet();
			$objPHPExcel->setActiveSheetIndex($sheetIndex);
			$styleArray = array(
				'font' => array(
					'bold' => false,
					'size' =>12,
				),
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				),
				'borders' => array(
					'top' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'bottom' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'right' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'left' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
				),
			);
			
			$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->getStartColor()->setARGB('FFddd9c3');
		
			if(!empty($data))
			{	$col=2;
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);;
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
				
				
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,1,'Currency Description ');
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1,1,'Exchange Rate');
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2,1,'Update date/time');
					foreach($data as $key=>$value)
					{   
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$col,$value['szCurrency']);
						$objPHPExcel->getActiveSheet()->getStyle('A'.$col)->applyFromArray($styleArray);
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$col,$value['fUsdValue']);
						$objPHPExcel->getActiveSheet()->getStyle('B'.$col)->applyFromArray($styleArray);
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$col,$value['dtDate']);
						$objPHPExcel->getActiveSheet()->getStyle('C'.$col)->applyFromArray($styleArray);				
						$col++;
					}
				$title="Exchage_Rates_For_Last_90_Days_";
				$objPHPExcel->getActiveSheet()->setTitle($title);
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				$objPHPExcel->setActiveSheetIndex(0);
				$file=$title.$id;
				$fileName=__APP_PATH_ROOT__."/management/exchangeRate/$file.xlsx";
				$objWriter->save($fileName);
				return $file;	
			}
			else
			{
                            return array();
			}
		}
		else
		{
			return false;
		}
	
	}
	
        function getAllRegions($data=array())
	{  
            if(!empty($data))
            { 
                if($data['idRegion']>0)
                {
                    $query_and = " AND id = '".(int)$data['idRegion']."' ";
                } 
                
                if(!empty($data['iActive']))
                {
                    $query_and .= " AND iActive = '".(int)$data['iActive']."' ";
                }  
            }
            
            $query="
                SELECT
                    r.id,
                    r.szRegionName,
                    r.szRegionName as szCountryName,
                    r.szRegionShortName,
                    r.dtCreatedOn,
                    r.szUniqueRegionId as idCountry,
                    r.dtUpdatedOn,
                    r.iActive
                FROM
                    ".__DBC_SCHEMATA_REGIONS__." AS r 
                WHERE
                    r.isDeleted = '0' 
                    $query_and
                ORDER BY
                    r.szRegionName  ASC	
            ";  
            if($result = $this->exeSQL($query))
            {	
                $ret_ary = array();
                if($this->iNumRows>0)
                {	
                    $crt=0;
                    while($row=$this->getAssoc($result))
                    {	
                        $ret_ary[$crt] = $row;	 
                        $crt++;	
                    }
                    return $ret_ary ;
                }
                else
                {
                    return array();
                }	
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
	}
        
        function updateRegionName($data)
        {
            if(!empty($data))
            {
                $this->set_szRegionName($this->sanitizeData($data['szRegionName']));
                $this->set_szRegionShortName($this->sanitizeData($data['szRegionShortName']));
                $this->set_iActive($this->sanitizeData($data['iActive'])); 
                
                if($this->error==true)
                {
                    return false;
                }
                
                if((int)$data['idRegion']>0)
                {
                    $query = " 
                        UPDATE 
                            ".__DBC_SCHEMATA_REGIONS__."
                        SET
                            szRegionName = '".mysql_escape_custom(trim($this->szRegionName))."',
                            szRegionShortName = '".  mysql_escape_custom(trim($this->szRegionShortName))."',
                            iActive = '".  mysql_escape_custom(trim($this->iActive))."', 
                            dtUpdatedOn = now()
                        WHERE
                            id = '".(int)$data['idRegion']."'
                    ";
                }
                else
                {
                    $iMaxRegionId = $this->getMaxRegionId(); 
                    $szUniqueRegionId = 3000 + $iMaxRegionId + 1 ;
                    
                    $query = "  
                       INSERT INTO
                            ".__DBC_SCHEMATA_REGIONS__."
                        (
                            szRegionName,
                            szRegionShortName,
                            szUniqueRegionId,
                            iActive,
                            dtCreatedOn
                        )
                        VALUES
                        (
                            '".mysql_escape_custom(trim($this->szRegionName))."',
                            '".mysql_escape_custom(trim($this->szRegionShortName))."',
                            '".(int)$szUniqueRegionId."',
                            '".mysql_escape_custom(trim($this->iActive))."',
                            now()
                        )
                     ";
                } 
                //echo $query; 
                //die;
                if(($result = $this->exeSQL($query)))
                {	
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
        }
        
        function getMaxRegionId()
        {  
            $query="
                SELECT
                    max(id) iMaxPreferenceId
                FROM
                    ".__DBC_SCHEMATA_REGIONS__." 
            "; 	 
            //echo $query ;
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                { 
                    $row=$this->getAssoc($result); 
                    return $row['iMaxPreferenceId'];
                }
                else
                {
                    return array();
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
        }
        
	function countries($id,$flag=0)
	{
            if($id>0)
            {
                $query="
                    SELECT
                        c.id,
                        c.szCountryName,
                        c.szCountryISO,
                        c.fStandardTruckRate, 
                        c.iAuto,
                        c.iActive,
                        c.iSmallCountry,
                        c.szLatitude,
                        c.szLongitude,
                        r.szRegionName,
                        r.szRegionShortName,
                        r.szUniqueRegionId,
                        c.iDefaultCurrency,
                        c.fVATRate,
                        c.szVATRegistration
                    FROM
                        ".__DBC_SCHEMATA_COUNTRY__." AS c	
                    LEFT JOIN
                        ".__DBC_SCHEMATA_REGIONS__." r
                    ON
                        r.id = c.idRegion
                    GROUP BY 
                            c.id
                    ORDER BY
                        c.szCountryName 
                    ASC	
                ";			
                if( ( $result = $this->exeSQL( $query ) ) )
                {	
                    $excangeRate=array();
                    if($this->iNumRows>0)
                    {	$crt=0;
                        while($row=$this->getAssoc($result))
                        {	
                            $excangeRate[]=$row;	
                            $idCountry=$row['id']; 
                            $crt++;	
                        }
                        return $excangeRate ;
                    }
                    else
                    {
                        return array();
                    }	
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }	
            else
            {
                return false;
            }
	}
	
	function countriesView($id = 0,$flag = 0)
	{
		if($id>0)
		{
                    $query="
                        SELECT
                            c.szCountryName,
                            c.szCountryISO,
                            c.idRegion,
                            c.fMaxFobDistance,
                            c.fVATRate,
                            c.szVATRegistration,
                            c.szDefaultCourierPostcode,
                            c.szDefaultCourierCity,
                            c.iUsePostcodes
                        ";

                    if($flag!=0)
                    {
                        $query.="
                                ,
                                c.id,
                                c.fStandardTruckRate,
                                c.iActive, 
                                c.iAuto,
                                c.iSmallCountry,
                                c.szLatitude,
                                c.szLongitude,
                                c.iDefaultCurrency,
                                c.iDefaultLanguage
                            ";
			}
			
			$query.="
				FROM
					".__DBC_SCHEMATA_COUNTRY__." AS c					
					";
			
			if($flag!=0)
			{
				$query.="	
				WHERE
					c.id='".(int)$flag."'	
				GROUP BY 
					c.id
					";
			}
                        //echo $query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				if($this->iNumRows>0)
				{	
					if($flag==0)
					{	
						$excangeRate=array();
						while($row=$this->getAssoc($result))
						{	
							$excangeRate[]=$row;
                                                        $ctr1=count($excangeRate)-1;
                                                        $otherArr=$this->getOtherLanguageName($row['id'],true);
                                                        $excangeRate[$ctr1]=$otherArr;
						}
						return $excangeRate;
					
					}
					if($flag!=0)
					{
						$row=$this->getAssoc($result);
						$this->idCountries=$row['idCountries'];
						$this->iActive=$row['iActive'];
						$this->szCountryName=$row['szCountryName'];
						//$this->szCountryDanishName=$row['szCountryDanishName'];						
						$this->szCountryISO=$row['szCountryISO'];
						$this->fStandardTruckRate=$row['fStandardTruckRate'];
						$this->iActiveFromDropdown=$row['iActiveFromDropdown'];
						$this->iActiveToDropdown=$row['iActiveToDropdown'];
						$this->iSmallCountry=$row['iSmallCountry'];
						$this->szLatitude=$row['szLatitude'];
						$this->szLongitude=$row['szLongitude'];
                                                $this->iDefaultCurrency=$row['iDefaultCurrency'];
                                                
                                                $excangeRate[]=$row;
                                                $ctr1=count($excangeRate)-1;
                                                $otherArr=$this->getOtherLanguageName($row['id'],true);
                                                if(!empty($otherArr))
                                                {
                                                    $newArray=  array_merge($row,$otherArr);
                                                }else{
                                                $newArray=$row;
                                                }
						return $newArray;
					}
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function downloadCountryDetails($id)
	{
		if($id>0)
		{
			require( __APP_PATH_CLASSES__ . "/PHPExcel.php" );
			$data=$this->countriesView($id,0);
			$sheetIndex=0;
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->createSheet();
			$objPHPExcel->setActiveSheetIndex($sheetIndex);
			$styleArray = array(
				'font' => array(
					'bold' => false,
					'size' =>12,
				),
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				),
				'borders' => array(
					'top' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'bottom' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'right' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'left' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
				),
			);
			
			$objPHPExcel->getActiveSheet()->getStyle('A1:B1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$objPHPExcel->getActiveSheet()->getStyle('A1:B1')->getFill()->getStartColor()->setARGB('FFddd9c3');
		
			if(!empty($data))
			{	$col=2;
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(50);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);;
				
				
				
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,1,'Country Name ');
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1,1,'ISO');
					
					foreach($data as $key=>$value)
					{   
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$col,$value['szCountryName']);
						$objPHPExcel->getActiveSheet()->getStyle('A'.$col)->applyFromArray($styleArray);
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$col,$value['szCountryISO']);
						$objPHPExcel->getActiveSheet()->getStyle('B'.$col)->applyFromArray($styleArray);				
						$col++;
					}
				$title="Country_Details_";
				$objPHPExcel->getActiveSheet()->setTitle($title);
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				$objPHPExcel->setActiveSheetIndex(0);
				$file=$title.$id;
				$fileName=__APP_PATH_ROOT__."/management/exchangeRate/$file.xlsx";
				$objWriter->save($fileName);
				return $file;	
			}
		}
		else
		{
			return false;
		}
	}
	function validateCountryName($id)
	{
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_COUNTRY__."
			WHERE
				szCountryName='".mysql_escape_custom($this->szCountryName)."'
			AND	
				iActive='1'
			";
		
		if($id>0)
		{
			$query.="
				AND 
					id<>'".$id."'
				";
		}
		
		if( ( $result = $this->exeSQL( $query ) ) )
		{	
				if($this->iNumRows>0)
			{
				$this->addError( 'szCountryName' , t($this->t_base.'messages/country_already_exists') );
               	return false;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}	
	}
	
	function saveCountry($id,$country,$countryId)
	{	 
            if($id>0  && $countryId>0)
            {	
                $this->idCountries=$countryId;
                $this->set_iActive($this->sanitizeData($country['iActive']));
                $this->set_szCountryName($this->sanitizeData($country['szCountryName']));			
                $this->set_szCountryISO($this->sanitizeData($country['szCountryISO']));
                $this->set_fStandardTruckRate($this->sanitizeData($country['fStandardTruckRate']));
                //$this->set_iActiveFromDropdown($this->sanitizeData($country['iActiveFromDropdown']));
                //$this->set_iActiveToDropdown($this->sanitizeData($country['iActiveToDropdown']));
                $this->set_iSmallCountry($this->sanitizeData($country['iSmallCountry']));
                $this->set_szLatitude((float)$country['szLatitude']);
                $this->set_szLongitude((float)$country['szLongitude']);
                $this->set_fMaxFobDistance($country['fMaxFobDistance']);
                $this->set_fVATRate($country['fVATRate']);
                $this->set_szVATRegistration($country['szVATRegistration']);
                $this->set_iDefaultCurrency($country['iDefaultCurrency']);
                $this->set_iDefaultLanguage($country['iDefaultLanguage']);
                $this->set_szDefaultCourierPostcode($country['szDefaultCourierPostcode']);
                $this->set_szDefaultCourierCity($country['szDefaultCourierCity']);
                
                
                $kConfig = new cConfig();
                $languageArr=$kConfig->getLanguageDetails('','',true);
                if(!empty($languageArr))
                {
                    
                    foreach($languageArr as $languageArrs)
                    {
                        $szOtherCountryName="szCountryDanishName_".$languageArrs['id'];
                        
                        $this->set_szCountryDanishName($this->sanitizeData($country[''.$szOtherCountryName.'']),$languageArrs['szName'],$szOtherCountryName);
                    }
                }
                
                
                if($this->szLatitude!=NULL)
                {
                    if($this->szLatitude< -90 || $this->szLatitude>90)
                    {
                        $this->addError( "szLatitude" , t($this->t_base.'fields/latitude_must_be'));
                    } 
                }
                if($this->szLongitude!=NULL)
                {
                    if($this->szLongitude< -180 || $this->szLongitude>180)
                    {
                        $this->addError( "szLongitude" , t($this->t_base.'fields/longitude_must_be'));
                    }

                }
                if($this->error=== true)
                {
                        return false;
                }
                $this->validateCountryName($countryId);
                if($this->error=== true)
                {
                        return false;
                }
                //c.iAuto='".mysql_escape_custom($this->iAuto)."',

                $query="
                    UPDATE
                            ".__DBC_SCHEMATA_COUNTRY__." AS c
                    SET
                        c.szCountryName='".mysql_escape_custom($this->szCountryName)."',						
                        c.szCountryISO='".mysql_escape_custom($this->szCountryISO)."',
                        c.fStandardTruckRate='".mysql_escape_custom($this->fStandardTruckRate)."',
                        c.iActive='".mysql_escape_custom($this->iActive)."',  
                        c.iSmallCountry = '".(int)$country['iSmallCountry']."',
                        c.idRegion = '".(int)$country['idRegion']."',
                        c.szLatitude = '".mysql_escape_custom(number_format((float)$this->szLatitude,6))."',
                        c.szLongitude = '".mysql_escape_custom(number_format((float)$this->szLongitude,6))."',
                        c.fMaxFobDistance = '".mysql_escape_custom($this->fMaxFobDistance)."',
                        c.fMaxFobDistance = '".mysql_escape_custom($this->fMaxFobDistance)."',
                        fVATRate='".(float)$this->fVATRate."',
                        szVATRegistration = '".mysql_escape_custom($this->szVATRegistration)."',
                        iDefaultCurrency = '".(int)$this->iDefaultCurrency."',
                        iDefaultLanguage = '".(int)$this->iDefaultLanguage."',
                        szDefaultCourierCity = '".  mysql_escape_custom($this->szDefaultCourierCity)."',
                        szDefaultCourierPostcode = '".  mysql_escape_custom($this->szDefaultCourierPostcode)."',
                        iUsePostcodes = '".(int)$country['iUsePostcodes']."'    
                    WHERE	
                        c.id='".(int)$this->idCountries."'	
                ";
                //echo "<br /> ".$query."<br />";
                if( ( $result = $this->exeSQL( $query ) ) )
                {	
                    if(!empty($languageArr))
                    {
                    
                        $query="
                                UPDATE
                                    ".__DBC_SCHEMATA_COUNTRY_NAME_MAPPING__."
                                SET
                                    szName='".mysql_escape_custom($this->szCountryName)."'  
                                WHERE	
                                    idCountry='".(int)$this->idCountries."'
                                AND
                                    idLanguage='".(int)__LANGUAGE_ID_ENGLISH__."'
                            ";
                            $result = $this->exeSQL( $query );
                        foreach($languageArr as $languageArrs)
                        {
                            $szOtherCountryName="szCountryDanishName_".$languageArrs['id'];
                            
                            $queryCheck="
                                SELECT
                                    id
                                FROM
                                    ".__DBC_SCHEMATA_COUNTRY_NAME_MAPPING__."
                                WHERE	
                                    idCountry='".(int)$this->idCountries."'
                                AND
                                    idLanguage='".(int)$languageArrs['id']."'
                            ";
                            //echo $query."<br />";
                            if( ( $resultCheck = $this->exeSQL( $queryCheck ) ) )
                            {	
                                if($this->iNumRows>0)
                                {
                                    $query="
                                        UPDATE
                                            ".__DBC_SCHEMATA_COUNTRY_NAME_MAPPING__."
                                        SET
                                            szName='".mysql_escape_custom($this->$szOtherCountryName)."'  
                                        WHERE	
                                            idCountry='".(int)$this->idCountries."'
                                        AND
                                            idLanguage='".(int)$languageArrs['id']."'
                                    ";
                                    $result = $this->exeSQL( $query );
                                }
                                else
                                {
                                    $query="
                                        INSERT INTO
                                            ".__DBC_SCHEMATA_COUNTRY_NAME_MAPPING__."
                                        (
                                            idCountry,
                                            idLanguage,
                                            szName
                                        )
                                        VALUES
                                        (
                                            '".(int)$this->idCountries."',
                                            '".(int)$languageArrs['id']."',
                                            '".mysql_escape_custom($this->$szOtherCountryName)."'    
                                        )
                                    ";
                                    $result = $this->exeSQL( $query );
                                }
                            }
                            
                            

                        }
                    }
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
            else
            {
                    return false;
            }
	}
	
	function addCountry($id,$country)
	{	
            if($id>0 && (!empty($country)))
            {	
                $this->set_iActive($this->sanitizeData($country['iActive']));
                $this->set_szCountryName($this->sanitizeData($country['szCountryName']));               
                
                //$this->set_szCountryDanishName($this->sanitizeData($country['szCountryDanishName']));			
                $this->set_szCountryISO($this->sanitizeData($country['szCountryISO']));
                $this->set_fStandardTruckRate($this->sanitizeData($country['fStandardTruckRate']));
                //$this->set_iActiveFromDropdown($this->sanitizeData($country['iActiveFromDropdown']));
                //$this->set_iActiveToDropdown($this->sanitizeData($country['iActiveToDropdown']));
                $this->set_iSmallCountry($this->sanitizeData($country['iSmallCountry']));
                $this->set_szLatitude($this->sanitizeData($country['szLatitude']));
                $this->set_szLongitude($this->sanitizeData($country['szLongitude']));
                $this->set_fMaxFobDistance($country['fMaxFobDistance']);
                $this->set_fVATRate($country['fVATRate']);
                $this->set_szVATRegistration($country['szVATRegistration']);
                $this->set_iDefaultCurrency($country['iDefaultCurrency']);
                $this->set_iDefaultLanguage($country['iDefaultLanguage']);                    
                $this->set_szDefaultCourierPostcode($country['szDefaultCourierPostcode']);
                $this->set_szDefaultCourierCity($country['szDefaultCourierCity']);
                if($this->szLatitude!=NULL)
                {
                    if($this->szLatitude< -90 || $this->szLatitude>90)
                    {
                        $this->addError( "szLatitude" , t($this->t_base.'fields/latitude_must_be'));
                    }

                }
                if($this->szLongitude!=NULL)
                {
                        if($this->szLongitude< -180 || $this->szLongitude>180)
                        {
                                $this->addError( "szLongitude" , t($this->t_base.'fields/longitude_must_be'));
                        }

                }
                
                $kConfig = new cConfig();
                $languageArr=$kConfig->getLanguageDetails('','',true);
                if(!empty($languageArr))
                {
                    
                    foreach($languageArr as $languageArrs)
                    {
                        $szOtherCountryName="szCountryDanishName_".$languageArrs['id'];
                        
                        $this->set_szCountryDanishName($this->sanitizeData($country[''.$szOtherCountryName.'']),$languageArrs['szName'],$szOtherCountryName);
                    }
                }
                
                if($this->error=== true)
                {
                        return false;
                }

                $this->validateCountryName(0);
                if($this->error=== true)
                {
                        return false;
                }
                $query="
                    INSERT INTO 
                        ".__DBC_SCHEMATA_COUNTRY__."
                    SET
                        szCountryName='".mysql_escape_custom($this->szCountryName)."', 				
                        szCountryISO='".mysql_escape_custom($this->szCountryISO)."',
                        fStandardTruckRate='".mysql_escape_custom($this->fStandardTruckRate)."', 
                        iSmallCountry = '".(int)$country['iSmallCountry']."',
                        idRegion = '".(int)$country['idRegion']."', 
                        iActive='".$this->iActive."',
                        szLatitude = '".mysql_escape_custom((float)$this->szLatitude)."',
                        szLongitude = '".mysql_escape_custom((float)$this->szLongitude)."',
                        fMaxFobDistance = '".mysql_escape_custom((float)$this->fMaxFobDistance)."',  
                        fVATRate='".(float)$this->fVATRate."',
                        szVATRegistration = '".mysql_escape_custom($this->szVATRegistration)."',
                        iDefaultCurrency = '".(int)$this->iDefaultCurrency."',
                        iDefaultLanguage = '".(int)$this->iDefaultLanguage."',
                        szDefaultCourierCity = '".  mysql_escape_custom($this->szDefaultCourierCity)."',
                        szDefaultCourierPostcode = '".  mysql_escape_custom($this->szDefaultCourierPostcode)."',
                        iUsePostcodes='".(int)$country['iUsePostcodes']."'    
                ";
//                echo $query."<br>";
//                die;
                if( ( $result = $this->exeSQL( $query ) ) )
                {	
                    
                    $idCountry=$this->iLastInsertID;
                    
                    if(!empty($languageArr))
                    {
                        
                        $query="
                            INSERT INTO
                                ".__DBC_SCHEMATA_COUNTRY_NAME_MAPPING__."
                            SET
                                szName='".mysql_escape_custom($this->szCountryName)."',	
                                idCountry='".(int)$idCountry."',
                                idLanguage='".(int)__LANGUAGE_ID_ENGLISH__."'
                        ";
                        $result = $this->exeSQL( $query );
                        foreach($languageArr as $languageArrs)
                        {
                                $szOtherCountryName="szCountryDanishName_".$languageArrs['id'];
                                $query="
                                    INSERT INTO
                                        ".__DBC_SCHEMATA_COUNTRY_NAME_MAPPING__."
                                    SET
                                        szName='".mysql_escape_custom($this->$szOtherCountryName)."',	
                                        idCountry='".(int)$idCountry."',
                                        idLanguage='".(int)$languageArrs['id']."'
                                ";
                                $result = $this->exeSQL( $query );
                        }
                    }
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
            else
            {
                return false;
            }	
	}
	function validateCountryNotInUse($id)
	{	
		if($id>0)
		{
			$query="
				SELECT 
					((SELECT
						COUNT(id)
					FROM
						".__DBC_SCHEMATA_WAREHOUSES__." AS w
						WHERE w.idCountry='".(int)$id."' ) 
						+
					(SELECT		
						COUNT(id)
					FROM
						".__DBC_SCHEMATA_FROWARDER__." AS fwd
					WHERE fwd.idCountry='".(int)$id."') 
						+
					(SELECT			
						COUNT(id)
					FROM
						".__DBC_SCHEMATA_USERS__." AS cus
						WHERE cus.idCountry='".(int)$id."')
						+
					(SELECT					
						COUNT(id)
					FROM
						".__DBC_SCHEMATA_BOOKING__." AS book
					WHERE( 
						book.idOriginCountry='".(int)$id."' OR
						book.idDestinationCountry='".(int)$id."' OR
						book.idForwarderCountry='".(int)$id."' OR
						book.idWarehouseFromCountry='".(int)$id."' OR
						book.idWarehouseToCountry='".(int)$id."'
						))) AS countrySum 
					";
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				if( $this->getRowCnt() > 0 )
				{	
					$row=$this->getAssoc($result);
					if($row['countrySum']>0)
					{	
						return false;
					}
					else if($row['countrySum']==0)
					{
						$query="
							SELECT 
								((SELECT
										COUNT(id)
									FROM
										tbltruckingrate AS tr
										WHERE tr.idCountry='".(int)$id."')
										
										+
									(SELECT		
										COUNT(id)
									FROM
										".__DBC_SCHEMATA_SHIPPER_CONSIGNEE__." AS sc
										WHERE (
											sc.idShipperCountry='".(int)$id."' OR
											sc.idShipperCountry_pickup='".(int)$id."' OR
											sc.idConsigneeCountry='".(int)$id."' OR
											sc.idConsigneeCountry_pickuP='".(int)$id."'
										)) 
										+
									(SELECT		
										COUNT(id)
									FROM
										".__DBC_SCHEMATA_REGISTER_SHIPPER_CONSIGNEES__." AS rs
										WHERE rs.idCountry='".(int)$id."') 
										+
									(SELECT		
										COUNT(id)
									FROM
										".__DBC_SCHEMATA_FORWARDERS_CONTACT__." AS fwdc
										WHERE fwdc.idCountry='".(int)$id."')) AS finalCountryCheckSum			
								";
						
						if( ( $result = $this->exeSQL( $query ) ) )
						{	
							if( $this->getRowCnt() > 0 )
							{	
								$row=$this->getAssoc($result);
								if($row['finalCountryCheckSum']>0)
								{
									return false;	
								}
								else if($row['finalCountryCheckSum']==0)
								{
									return true;
								}
							}
						}
						else
						{
							$this->error = true;
							$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
							$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
							return false;
						}
					}
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}	
	}
	
	function deleteCountry($adminId,$id)
	{	
		if($adminId && $id)
		{		
			$status=$this->validateCountryNotInUse($id);
			if($status)
			{
				$query="
					UPDATE
						".__DBC_SCHEMATA_COUNTRY__."
					SET
						iActive='0' 
					WHERE 
						id='".(int)$id."'		
				";
				
				if( ( $result = $this->exeSQL( $query ) ) )
				{	
					return true;
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function textEditor($idValidate,$id,$mode)
	{
		if($idValidate)
		{	
			$id=sanitize_all_html_input($id);
			$table=$this->setTable($mode);
			if($table==__DBC_SCHEMATA_MANAGEMENT_FAQ_CUSTOMER__ || __DBC_SCHEMATA_MANAGEMENT_TERMS_CUSTOMER__)
			{
				$query_select = ',iLanguage';
			}
			
			if($id==0)
			{
				$query="
					SELECT
						szHeading,
						iOrderBy
						$query_select
					FROM 
						".$table."
					WHERE
						iOrderBy!=0	
					ORDER BY
						iOrderBy
						";	
			}
			if($id>0)
			{
				$query="
					SELECT 
						id,
						szDraftHeading,
						szDraftDescription
						$query_select
					FROM 
						".mysql_escape_custom($table)."	
					WHERE 
						iActive='1'	
						AND 
							id='".(int)$id."'	
				";
			}
			//echo $query;	
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				if( $this->getRowCnt() > 0 )
				{	
					$textEditor=array();
					if($id==0)
					{
						$content = $this->findDraftData($table);
					}
					while($row=$this->getAssoc($result))
					{	
						if($id>0)
						{
							$textEditor=$row;
						}
						else if($id==0)
						{
							$textEditor[]=$row;
						}
					}
						if($id==0)
						{
						$i=0;
							$NewArray = array();
							if(count($textEditor)> count($content))
							{
								foreach($textEditor as $value) {
								$NewArray[] = array_merge($value,($content[$i]?$content[$i]:array()));
								    $i++;
								}
							}
							else
							{
							foreach($content as $value) {
								$NewArray[] = array_merge(($textEditor[$i]?$textEditor[$i]:array()),$content[$i]);
								    $i++;
								}
							}
							return $NewArray;
						}
						else
						{
							return $textEditor;
						}
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}	
	}
	function findDraftData($table)
	{
		if($table==__DBC_SCHEMATA_MANAGEMENT_FAQ_CUSTOMER__ || __DBC_SCHEMATA_MANAGEMENT_TERMS_CUSTOMER__)
		{
			$query_select = ',iLanguage';
		}
		$query="
				SELECT
						id,
						iUpdated,
						szDraftHeading,
						iOrderByDraft
						$query_select
					FROM 
						".$table."
					WHERE
						iOrderByDraft!=0	
					ORDER BY
						iOrderByDraft
						";
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				if( $this->getRowCnt() > 0 )
				{	
					$textEditor=array();
					while($row=$this->getAssoc($result))
					{	
						if($id>0)
						{
							$textEditor=$row;
						}
						else if($id==0)
						{
							$textEditor[]=$row;
						}
					}
						return $textEditor;
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function textEditorFAQ($idValidate,$id,$mode)
	{
		if($idValidate)
		{	
			$id=sanitize_all_html_input($id);
			$table=$this->setTable($mode);
			if($id==0)
			{
				$query="
					SELECT 
						id,
						szDraftHeading,
						iOrderBy
					FROM 
						".mysql_escape_custom($table)."	
					WHERE 
						iActive='1'	
						ORDER BY
							iOrderBy ASC
						";			
			}
			if($id>0)
			{
			$query="
				SELECT 
					id,
					szDraftHeading,
					szDraftDescription
				FROM 
					".mysql_escape_custom($table)."	
				WHERE 
					iActive='1'	
					AND 
						id='".(int)$id."'	
					";
			}
				//echo $query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				if( $this->getRowCnt() > 0 )
				{	
					$textEditor=array();
					while($row=$this->getAssoc($result))
					{	
						if($id>0)
						{
							$textEditor=$row;
						}
						else if($id==0)
						{
							$textEditor[]=$row;
						}
					}
						return $textEditor;
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}	
	}
	function saveTextEditData($idAdmin,$heading,$data,$id,$option,$iLanguage=false)
	{	
		$option =(int) $option;
		if($idAdmin>0)
		{	
			$this->set_id($this->sanitizeData($id));
			$heading=str_replace("'",'&#39;',$heading);
			$this->set_szHeading($this->sanitizeData($heading));
			$data=str_replace("'",'&#39;',$data);
			$this->set_szDescription(sanitize_specific_tinymce_html_script_input($data));
			
			if($option==__TEXT_EDIT_TERMS_CUSTOMER__ || $option==__TEXT_EDIT_FAQ_CUSTOMER__)
			{
				$this->set_iLanguage((int)$iLanguage);
			}
			
			if($this->error=== true)
			{
				return false;	
			}
			else
			{	
				$data=$this->validateHeadingExists($this->szHeading,$this->id,$option);
				if($data==true)
				{	
					$this->addError( "szHeading" , t($this->t_base.'messages/Heading_exists') );
					return false;
				}
				
				$table=$this->setTable($option);
				
				if($option==__TEXT_EDIT_TERMS_CUSTOMER__ || $option==__TEXT_EDIT_FAQ_CUSTOMER__)
				{
					$query_update = " iLanguage = '".(int)mysql_escape_custom($this->iLanguage)."', ";
				}
					
				if($id>0)
				{
					$query="
						UPDATE 
							".mysql_escape_custom($table)."	
						SET
							szDraftHeading='".mysql_escape_custom($this->szHeading)."',	
							szDraftDescription='".mysql_escape_custom($this->szDescription)."',
							iUpdated = 1,
							$query_update
							dtUpdated	  = NOW()
						WHERE
							id='".(int)$this->id."'	
						";
				}
				
				if($id==0)
				{	
					$maxOrder=(int)$this->getMaxOrder($option);
					$query=" INSERT INTO
							".mysql_escape_custom($table)."
						SET
							szDraftHeading	  = '".mysql_escape_custom($this->szHeading)."',	
							szDraftDescription = '".mysql_escape_custom($this->szDescription)."',
							iActive		  = '1',
							dtUpdated	  = NOW(),
							iUpdated	  = 1,
							$query_update
							iOrderByDraft = $maxOrder+1
						";
				}	
				//echo $query;
				//die;
				if( ( $result = $this->exeSQL( $query ) ) )
				{	
					return true;
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
			}	
		}
		else
		{
			return false;
		}
	}
	function saveTextEditDataConfirm($idAdmin,$option)
	{	
		$option =(int)sanitize_all_html_input($option);
		if($idAdmin>0)
		{	
			$table=$this->setTable($option);
			$query="
				UPDATE 
					".mysql_escape_custom($table)."	
				SET
					szHeading= szDraftHeading,	
					szDescription=szDraftDescription,
					dtUpdated	  = NOW(),
					iUpdated =0,
					iOrderBy=iOrderByDraft
				WHERE
					(szDraftDescription!='' || szDraftDescription!='NULL')
				AND
					(szDraftHeading!='' || szDraftHeading!='NULL')	
				AND 
					(iOrderByDraft!=0 || iOrderByDraft!='NULL')			
				";
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				$this->deleteTextEditDataConfirm($idAdmin,$option);
				$this->updateversionTnc($option);
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}	
		}
		else
		{
			return false;
		}
	}
	function deleteTextEditDataConfirm($idAdmin,$option)
	{	
		$option =(int)sanitize_all_html_input($option);
		if($idAdmin>0)
		{	
			$table=$this->setTable($option);
			$query="
				DELETE 
				FROM 
					".mysql_escape_custom($table)."
				WHERE		
					iOrderByDraft = 0
				";
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}	
		}
		else
		{
			return false;
		}
	}
	function removeDraftData($idAdmin,$option)
	{	
		$option =(int)sanitize_all_html_input($option);
		if($idAdmin>0)
		{	
			$table=$this->setTable($option);
			$query="
				UPDATE 
					".mysql_escape_custom($table)."	
				SET
					szDraftHeading = szHeading,	
					szDraftDescription = szDescription,
					iUpdated =0,
					iOrderByDraft = iOrderBy
				WHERE
					(szDescription!='' || szDescription!='NULL')
				AND
					(szHeading!='' || szHeading!='NULL')	
				AND 
					(iOrderBy!=0 || iOrderBy!='NULL')			
				";
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				$this->deleteTextData($idAdmin,$option);
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}	
		}
		else
		{
			return false;
		}
	}
	function deleteTextData($idAdmin,$option)
	{	
		$option =(int)sanitize_all_html_input($option);
		if($idAdmin>0)
		{	
			$table=$this->setTable($option);
			$query="
				DELETE 
				FROM 
					".mysql_escape_custom($table)."
				WHERE		
					iOrderBy = 0
				";
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}	
		}
		else
		{
			return false;
		}
	}
	function saveTextEditDataFAQ($idAdmin,$heading,$data,$id,$option)
	{	
		$option =(int) $option;
		if($idAdmin>0)
		{	
			$this->set_id($this->sanitizeData($id));
			$heading=str_replace("'",'&#39;',$heading);
			$this->set_szHeading($this->sanitizeData($heading));
			$data=str_replace("'",'&#39;',$data);
			$this->set_szDescription(sanitize_specific_tinymce_html_input($data));
			
			if($this->error=== true)
			{
				return false;	
			}
			else
			{	
				$data=$this->validateHeadingExists($this->szHeading,$this->id,$option);
				if($data==true)
				{	
					$this->addError( "szHeading" , t($this->t_base.'messages/Heading_exists') );
					return false;
				}
				
				$table=$this->setTable($option);
				
				if($id>0)
				{	
					$query="
						UPDATE 
							".mysql_escape_custom($table)."	
						SET
							szDraftHeading='".mysql_escape_custom($this->szHeading)."',	
							szDraftDescription='".mysql_escape_custom($this->szDescription)."',
							dtUpdated	  = NOW()
						WHERE
							id='".(int)$this->id."'	
						";
				}
				
				if($id==0)
				{	
					$maxOrder=(int)$this->getMaxOrder($option);
					$query="
						INSERT 
							INTO
							".mysql_escape_custom($table)."
						SET
							szHeading	  = '".mysql_escape_custom($this->szHeading)."',	
							szDescription = '".mysql_escape_custom($this->szDescription)."',
							iActive		  = '1',
							dtUpdated	  = NOW(),
							iOrderBy	  = $maxOrder+1
						";
				}	
				
				if( ( $result = $this->exeSQL( $query ) ) )
				{	
					return true;
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
			}	
		}
		else
		{
			return false;
		}
	}
	function updateversionTnc($option)
	{	
		$flag = 1;
		if($option==__TEXT_EDIT_TERMS_FORWARDER__)
		{	
			createHtml();
			$table=__DBC_SCHEMATA_MANAGEMENT_TERMS_VERSION__;
			$flag=0;
		}
		if($option==__TEXT_EDIT_FAQ_FORWARDER__)
		{
			$table=__DBC_SCHEMATA_MANAGEMENT_FAQ_VERSION__;
		}
		if($option==__TEXT_EDIT_TERMS_CUSTOMER__)
		{
			$table=__DBC_SCHEMATA_MANAGEMENT_TERMS_VERSION_CUSTOMER__;
		}
		if($option==__TEXT_EDIT_FAQ_CUSTOMER__)
		{
			$table=__DBC_SCHEMATA_MANAGEMENT_FAQ_VERSION_CUSTOMER__;
		}
		if($flag)
		{
			return true;
		}
		$query="
			UPDATE
				 ".mysql_escape_custom($table)."
			SET	
				dtVersionUpdated	  = NOW()
			WHERE 	id ='1'	
				";
		
		if( ( $result = $this->exeSQL( $query ) ) )
		{
			if($option==__TEXT_EDIT_TERMS_FORWARDER__)
			{		
				$query="
					UPDATE
						".__DBC_SCHEMATA_FROWARDER__."
					SET	
						iVersionUpdate	  = '1',
						dtVersionUpdate	  = NOW()
					WHERE
						iAgreeTNC		  = '1'	
					";
				if( ( $result = $this->exeSQL( $query ) ) )
				{		
					return true;
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
			}	
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}	
	}
	
	function setTable($option)
	{
		if($option==__TEXT_EDIT_TERMS_FORWARDER__)
		{
			$table=__DBC_SCHEMATA_MANAGEMENT_TERMS__;
		}
		if($option==__TEXT_EDIT_FAQ_FORWARDER__)
		{
			$table=__DBC_SCHEMATA_MANAGEMENT_FAQ__;
		}
		if($option==__TEXT_EDIT_TERMS_CUSTOMER__)
		{
			$table=__DBC_SCHEMATA_MANAGEMENT_TERMS_CUSTOMER__;
		}
		if($option==__TEXT_EDIT_FAQ_CUSTOMER__)
		{
			$table=__DBC_SCHEMATA_MANAGEMENT_FAQ_CUSTOMER__;
		}
		return $table;
	}
	
	function getMaxOrder($option)
	{	
		$table=$this->setTable($option);
		
		$query="
			SELECT 
				MAX(iOrderByDraft)  as orders  
			FROM 
				".$table."
			";
		
		if($result=$this->exeSQL($query))
		{
			$row = $this->getAssoc( $result );
			if($row['orders']>0)
			{
				return $row['orders'];
			}
			else
			{
				return 0;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	function validateHeadingExists($heading,$id=0,$option)
	{	
		$table=$this->setTable($option);
		if($heading)
		{
			$query="
				SELECT 
					id 
				FROM 
					".$table."
				WHERE 
					szDraftHeading='".mysql_escape_custom($heading)."'
				AND 
					iActive='1'	
					";
			
			if($id>0)
			{
				$query.="AND 
						id<>'".(int)$id."'
					";
			}		

			if($result = $this->exeSQL( $query ) )
			{
				if($this->getRowCnt() > 0)
				{
					return true;	
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function deleteTermsForwarder($idAdmin,$id,$option)
	{	
		$table=$this->setTable($option);
		if($idAdmin>0)
		{	
			$query="
   				SELECT
   			 		iOrderByDraft
   			 	FROM
   			 		".$table."
   			 	WHERE
   			 		id='".(int)$id."'
   				";
			
   			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				if ($this->getRowCnt() > 0)
				{
					$row = $this->getAssoc( $result);
					$order = $row['iOrderByDraft'];
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
			
			$query="
				UPDATE
					".$table."
				SET	
					szDraftHeading='',
					szDraftDescription='',
					iOrderByDraft = 0,
					iUpdated = 0
				WHERE
					id='".(int)$id."'
				";
			//echo $query;DIE;
			if($result = $this->exeSQL( $query ) )
			{
				if($this->iNumRows>0)
				{	
					$query="
						UPDATE
							".$table."
					 	SET
							iOrderByDraft=iOrderByDraft-1
						 WHERE
					 		iOrderByDraft>'".(int)$order."'
				 ";
                		//echo $query;DIE;
					 if( $result = $this->exeSQL($query))
					 {
						//$this->updateversionTnc($option);	
						return true;
					 }
					 else
					 {
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					 }
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function moveUpDown($id,$orderBy,$option)
	{	
		$table=$this->setTable($option);
		
		$query="
   				SELECT
   			 		iOrderByDraft
   			 	FROM
   			 		".$table."
   			 	WHERE
   			 		id='".(int)$id."'
   				";
			
   			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				if ($this->getRowCnt() > 0)
				{
					$row = $this->getAssoc( $result);
					$order = $row['iOrderByDraft'];
					
					$query="
						UPDATE
							".$table."
					 	SET
					 		iUpdated = 1,
					 		";
					if($orderBy==__ORDER_MOVE_DOWN__)
					{
						$query.="
								
								iOrderByDraft=iOrderByDraft-1
						 		";
					}
					if($orderBy==__ORDER_MOVE_UP__)
					{
						$query.="
								iOrderByDraft=iOrderByDraft+1
						 		";
					}
					$query.="
						WHERE
					 		id='".(int)$id."'
						 ";
					//echo $query;
                	if($result = $this->exeSQL( $query ))
					{
						$query="
							UPDATE
								".$table."
							SET
							iUpdated = 1,
							";
						if($orderBy==__ORDER_MOVE_DOWN__)
						{
							$query.="
									iOrderByDraft=iOrderByDraft+1
							 		";
						}
						if($orderBy==__ORDER_MOVE_UP__)
						{
							$query.="
									iOrderByDraft=iOrderByDraft-1
							 		";
						}
						$query.="
							WHERE
								id!='".(int)$id."'
							AND
								";
						if($orderBy==__ORDER_MOVE_DOWN__)
						{
							$query.="
									iOrderByDraft=$order-1
							 		";
						}
						if($orderBy==__ORDER_MOVE_UP__)
						{
							$query.="
									iOrderByDraft=$order+1
							 		";
						}
						//echo $query;
						if($result = $this->exeSQL( $query ))
						{
							//$this->updateversionTnc($option);
							return true;
						}
						else
						{
							$this->error = true;
							$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
							$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
							return false;
						}
					}
					else
					{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}

	function gerenalSelectTextEditData($idAdmin,$mode,$iLanguage=0)
	{	
		if($idAdmin>0)
		{	
			$this->set_szDescription($this->sanitizeData($mode));
			
			if($iLanguage>0)
			{
				$query_and = " AND iLanguage = '".(int)$iLanguage."' ";
			}
			$query="
				SELECT
					id,
					szValue
				FROM	
					".__DBC_SCHEMATA_MANAGEMENT_VARIABLE__." 
				WHERE 
					szDescription='".mysql_escape_custom($this->szDescription)."'
                                $query_and            
				";
                        //echo $query;
			if($result = $this->exeSQL( $query ))
			{
				if ($this->getRowCnt() > 0)
				{
					$row = $this->getAssoc( $result);	
					return $row;
				}
				else
				{
					return array();
				}	
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	function gerenalSelectEditorDescription($id)
	{	
		if($id>0)
		{	
			$id = (int)$id;
			$query="
				SELECT
					szDescription
				FROM	
					".__DBC_SCHEMATA_MANAGEMENT_VARIABLE__." 
				WHERE 
					id='".(int)$id."'	
				";
			if($result = $this->exeSQL( $query ))
			{
				if ($this->getRowCnt() > 0)
				{	
					$row = $this->getAssoc( $result);	
					return $row['szDescription'];
				}
				else
				{
					return array();
				}	
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	function gerenalTextEditData($idAdmin,$id,$description)
	{	
		if($idAdmin>0)
		{	
			$this->set_id($this->sanitizeData($id));
			$szDescription = $this->gerenalSelectEditorDescription($id);
			$description = trim($description); 
			
			/*
			if($szDescription == "__EXPLAIN_INTRODUCTION__")
			{	
				//$html = str_replace("(", "&#40;", $description);
				//$html = str_replace(")", "&#41;", $description);
				
				//$description =  sanitize_specific_tinymce_html_script_input($description);
			}
			else
			{
				$description=sanitize_specific_tinymce_html_input($description);
			}*/ 
			$this->set_szDescription($description);
			$this->szDescription=htmlspecialchars($this->szDescription, ENT_QUOTES);  
                        
                        if(mb_detect_encoding($this->szDescription) == "UTF-8")
                        {
                            $this->szDescription=($this->szDescription);
                        }
                        else
                        {
                            $this->szDescription=utf8_encode($this->szDescription);
                        }
			
			if($this->error=== true)
			{
				return false; 
			}
			$query="
				UPDATE 
					".__DBC_SCHEMATA_MANAGEMENT_VARIABLE__." 
				SET 
					szValue = '".mysql_escape_custom(trim($this->szDescription))."'
				WHERE 
					id='".(int)$this->id."'	
				";
			//echo $query ;
			//die;
			if($result = $this->exeSQL( $query ))
			{
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	function selectMaxEmail($email='',$date='')
	{
		$query="
			SELECT 	
				COUNT(id) as id
			FROM
				tblemaillog
				";
			if($email or $date)
			{
				$query.="
					WHERE 
					";
			}
			if($email)
			{
				$query.="
					szToAddress like '".mysql_escape_custom($email)."%'	
					";
			}
			if($email && $date)
			{
				$query.="
					AND
					";
			}
			if($date)
			{
				$query.="
					DATE_FORMAT(dtSent,'%d/%m/%Y') like '".mysql_escape_custom($date)."%'	
					";
			}	
			
		if($result = $this->exeSQL( $query ))
		{
			$row=$this->getAssoc($result);
			RETURN $row['id'];
		}	
	}
	function emailLog($min=1)
	{
		$query="
			SELECT 
				id,
				szEmailSubject,
				szToAddress,
				DATE_FORMAT(dtSent,'%d/%m/%Y') as dtSents
			FROM 
				".__DBC_SCHEMATA_EMAIL_LOG__."	
				ORDER BY dtSent DESC
				";
		
		
			$query.="
				LIMIT
					".(($min -1 ) * __PER_PAGE__ ).",". __PER_PAGE__ ."
				";
			//echo $query;
		
		if($result = $this->exeSQL( $query ))
		{
			if ($this->getRowCnt() > 0)
			{	
				$textEditor=array();
				while($row=$this->getAssoc($result))
				{	
						$textEditor[]=$row;
				}
				return $textEditor;
			}
			else
			{
				return array();
			}	
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}	
	}
	
	function viewBodyContent($idAdmin,$id)
	{
		if($idAdmin)
		{
			$query="
			SELECT 
				szEmailBody
			FROM 
				tblemaillog	
			WHERE
				id='".(int)$id."'	
				";
		if($result = $this->exeSQL( $query ))
		{
			if ($this->getRowCnt() > 0)
			{	
				
				$row=$this->getAssoc($result);
				return $row;
			}
			else
			{
				return array();
			}	
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}	
		}
	}
	function searchEmailLogContent($idAdmin,$email,$date,$min=1)
	{
		if($idAdmin)
		{
			$query="
			SELECT 
				id,
				szEmailSubject,
				szToAddress,
				DATE_FORMAT(dtSent,'%d/%m/%Y') as dtSent
			FROM 
				tblemaillog	
			WHERE";
			if($email)
			{
				$query.="
					szToAddress like '".mysql_escape_custom($email)."%'	
					";
			}
			if($email && $date)
			{
				$query.="
					AND
					";
			}
			if($date)
			{
				$query.="
					DATE_FORMAT(dtSent,'%d/%m/%Y') like '".mysql_escape_custom($date)."%'	
					";
			}	
			$query.="
				ORDER BY 
					szToAddress
				LIMIT
					".(($min -1 ) * __PER_PAGE__ ).",". __PER_PAGE__ ."
				";

			if($result = $this->exeSQL( $query ))
			{
				if ($this->getRowCnt() > 0)
				{	
					$textEditor=array();
					while($row=$this->getAssoc($result))
					{	
							$textEditor[]=$row;
					}
					return $textEditor;
				}
				else
				{
					return array();
				}	
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}	
		}
	}
	function manageVAriables($id)
	{
		$id = (int)$id;
		$query="
			SELECT
				id,
				szFriendlyName,
				szValue,
				szDescription
			FROM
				".__DBC_SCHEMATA_MANAGEMENT_VARIABLE__."
			WHERE 
				szDescription  IN (
				";
		if($id == 1)
		{
			$query1 =" 
						'__MAX_CARGO_VOLUME__',
						'__MAX_CARGO_WEIGHT__'
						
					";
		}
		if($id == 2)
		{
			$query1 =" 			
						'__CUTOFF_N_DAYS__',
						'__PICK_UP_AFTER_M_DAYS__',
						'__MINIMUM_RATING_TO_SHOW_RATING_STARS__',
						'__FORWARDER_RATING__',
						'__FORWARDER_RATING_REVIEW__',
						'__RECALCULATE_PRICE_INTERVAL__'
					 ";
		}
		if($id == 3)
		{
			$query1 =" 			
						'__MAX_ALLOWED_DRAFT__'
					  ";
		}
		if($id == 4)
		{
			$query1 =" 
						'__MAX_ROWS_EXCEED__',
						'__MAX_DEFAULT_ROWS_CURRENT_SERVICES_SELECTION__',
						'__CHANGE_DROP_DOWN_OPTION__'
					  ";
		}
		if($id == 5)
		{
                    $query1 ="	'__MAX_W/M_FACTOR__',
                                    '__MINI_W/M_FACTOR__',
                                    '__MAX_FUEL__',
                                    '__MINI_FUEL__',
                                    '__MAX_DISTANCE__',
                                    '__MAX_RADIUS_CIRCLE__',
                                    '__NUM_POINT_TO_TEST__',
                                    '__MAX_DISTANCE_FOR_ZONE__',
                                    '__MAX_NUMBER_POINT_FOR_ZONE__',
                                    '__MAX_DISTANCE_FOR_COUNTRY_INCLUDED__'
                            ";
		}
		if($id == 6)
		{
                    
                            //'__CAPSULE_OWNER_OF_NEW_CUSTOMER__',
                    $query1 ="
                            '__USER_DETAILS_REQUIRED_FOR_SHIPMENT_CBM__',
                            '__USER_DETAILS_REQUIRED_FOR_SHIPMENT_KG__',
                            '__MARK_UP_PRICING_PERCENTAGE__',
                            '__MIMI_DAYS_FOR_NPS_FROM_CUSTOM__',
                            '__TRANSPORTECA_CUSTOMER_CARE__',
                            '__TRANSPORTECA_CUSTOMER_CARE_DANISH__',
                            '__PAYMENT_GATEWAY_STATUS__',
                            '__PAYPAL_STATUS__',
                            '__CREDIT_DAYS_FOR_INSURANCE_VENODR__'
                     ";
		}
		if($id == 7)
		{
                    $query1 ="		
                        '__CONTACT_EMAIL__',
                        '__FORWARDER_SIGNUP_EMAIL__',
                        '__FINANCE_CONTACT_EMAIL__',
                        '__SEND_UPDATES_CUSTOMER_EMAIL__',
                        '__FORWARDER_BOOKING_CONFIRMATION_EMAIL__',
                        '__FACEBOOK_URL__',
                        '__TWITTER_URL__',
                        '__LINKEDIN_URL__',
                        '__YOUTUBE_URL__',
                         '__GOOGLE_PLUS_URL__',
                        '__MINIMUM_TIME_RSS_NEWS_FLASH__',
                        '__NUMBER_RSS_NEWS_FLASH__',
                        '__NUMBER_OF_FEEDS_TO_DISPLAY__',
                        '__NUMBER_OF_RSS_TO_DISPLAY__',
                        '__FORWARDER_RESPONSE_TIME__',
                        '__CUSTOMER_RESPONSE_TIME__',
                        '__MAX_NUMBER_OF_QUOTES_PER_FILE__',
                        '__CUSTOMER_SUPPORT_GOOGLE_ACCOUNT__',
                        '__CUSTOMER_SUPPORT_GOOGLE_ACCOUNT_PASSWORD__'
                    ";
		}	
		if($id == 8)
		{
                    $query1 ="		
                        '__OPTION_EXPORT_HAULAGE_NOT_DEFINED__',
                        '__OPTION_IMPORT_HAULAGE_NOT_DEFINED__',
                        '__NEED_OF_EXPORT_CUSTOM_CLEARENCE__',
                        '__NEED_OF_IMPORT_CUSTOM_CLEARENCE__',
                        '__WHO_SHOULD_PAY_ORIGN_CHARGES__',
                        '__WHO_SHOULD_PAY_DESTINATION_CHARGES__',
                        '__IS_DANGEROUS_CARGO__',
                        '__TRANSPORTECA_EXPLAIN_PAGE_LINK__',
                        '__OPTION_EXPORT_HAULAGE_NOT_DEFINED_DANISH__',
                        '__OPTION_IMPORT_HAULAGE_NOT_DEFINED_DANISH__',
                        '__NEED_OF_EXPORT_CUSTOM_CLEARENCE_DANISH__',
                        '__NEED_OF_IMPORT_CUSTOM_CLEARENCE_DANISH__',
                        '__WHO_SHOULD_PAY_ORIGN_CHARGES_DANISH__',
                        '__WHO_SHOULD_PAY_DESTINATION_CHARGES_DANISH__',
                        '__IS_DANGEROUS_CARGO_DANISH__',
                        '__TRANSPORTECA_EXPLAIN_PAGE_LINK_DANISH__',
                        '__CHINA_EXPORT_LICENSE_HERE_DANISH__',
                        '__CHINA_EXPORT_LICENSE_HERE__'
                    ";
		}
		
		if($id == 9)
		{
                    $query1 ="
                            '__GOOGLE_MAP_V3_API_KEY__',
                            '__EASYPOST_API_KEY__'
                    ";
		} 
		if($id == 10)
		{
                    $query1 ="
                        '__TRUSTED_PARTNER__',
                        '__REQUEST__',
                        '__USERS__',
                        '__ROUTES__'						
                    ";
		}
		if($id==11)
		{
                    $query1 ="
			'__TOTAL_NUM_SEARCH_RESULT__',
                        '__TOTAL_NUM_RAIL_SERVICE_DISPLAY__',
                        '__TOTAL_NUM_AIR_FREIGHT_SEARCH_RESULT__',
			'__MAX_WAREHOUSE_DISTANCE_TO_BE_CONSIDER__',
                        '__MAX_DTD_DISTANCE_TO_BE_CONSIDER__',
			'__MAX_DISTANCE_FROM_GOOGLE_CORDINATE__',
			'__TOTAL_NUM_WAREHOUSE__',
                        '__TOTAL_NUM_WAREHOUSE_D_SERVICE__',
			'__MAX_NUM_RECORD_PER_FORWARDER__',
                        '__MINIMUM_VOLUME_TO_DISPLAY_RAIL_TRANSPORT__'
                    ";
		}
		
		if($id==12)
		{
                    $query1 =" 
                        '__NUMBER_OF_CHEAPEST_OPTION__',
                        '__NUMBER_OF_FASTEST_OPTION__',
                        '__NUMBER_OF_DAYS_ADDED_TO_DELIVERY_DATE__',
                        '__MAXIMUM_CHARGEABLE_WEIGHT_FOR_AIRFREIGHT__'
                    ";
		}
		
		if($id == 13)
		{
                    $query1 ="
                        '__OVER_WEIGHT_MEASURE_TOLERANCE_WITHOUT_PRICE_ADJUSTMENT__',
                        '__NUMBER_OF_WEEKDAYS_PAYMENT_REQUIRED_BEFORE_REQUESTED_PICKUP__',
                        '__TNT_BOOKING_API_VOLUME_REDUCTION__'
                    ";
		}
		
                if($id == 14)
		{
                    $query1 ="
                        '__LCL_SERVICE_VIDEO__',
                        '__HAULAGE_VIDEO__',
                        '__LCL_SERVICE_BULK_VIDEO__',
                        '__HAULAGE_BULK_VIDEO__',
                        '__CUSTOM_CLEARANCE_BULK_VIDEO__'
                    ";
		}
			$query .=$query1; 
			
			$query .=" )";
			
			$query .= "ORDER BY 
						FIELD(szDescription,
						".$query1.")";
			if($id == 7)
			{	 		
				//ECHO $query;
			}
			if($result = $this->exeSQL( $query ))
			{
				if ($this->getRowCnt() > 0)
				{	
					$manageVar=array();
					$tempAry = array();
					while($row=$this->getAssoc($result))
					{	
						if($id == 5)
						{
							$tempAry[] = $row ;
						}
						else
						{
							$manageVar[]=$row;
						}
					}
					
					//print_R()
					if($id == 5 && !empty($tempAry))
					{
						$manageVar = array_merge($manageVar,$tempAry);
					}
					return $manageVar;
				}
				else
				{
					return array();
				}	
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}	
	}
	
	function updateManageVar($id,$value)
	{
		$count = count($id);

		if($count>0)
		{
			for($i=0;$i<$count;$i++)
			{ 	
                            $this->set_iManageidArr($this->sanitizeData($id[$i]));
                            //$this->set_szManageHeadingArr($this->sanitizeData($heading[$i]));
                            $this->set_iManageValueArr($this->sanitizeData($value[$i]));
			}
		
			if($this->error===true)
			{
                            return false;
			}
			//	szFriendlyName='".mysql_escape_custom($this->szHeadingArr[$i])."',
			for($i=0;$i<$count;$i++)
			{ 	
				$query="
					UPDATE
						".__DBC_SCHEMATA_MANAGEMENT_VARIABLE__."
					SET
                                            szValue='".mysql_escape_custom($this->iValueArr[$i])."'
					WHERE
                                            id='".(int)$this->iIdArr[$i]."'		
					";
				//echo $query;
				if($result = $this->exeSQL( $query ))
				{
					continue;
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
			}
			return true;	
		}
	}
	
	function showBookingConfirmed()
	{
		$query="
			SELECT 
				b.id,
				b.szForwarderDispName,
				b.szInvoice,
				b.szBookingRef,
				b.fTotalPriceNoMarkupUSD*b.fForwarderExchangeRate AS forwarderPrice,
				b.szForwarderCurrency,
				b.dtBookingConfirmed
			FROM
				".__DBC_SCHEMATA_BOOKING__." AS b
			WHERE 
				idBookingStatus IN (3,4)
			AND 
				iManagementRecieved	= '0'
			ORDER BY dtBookingConfirmed	 DESC	
		";
		//echo $query;
		if($result = $this->exeSQL( $query ))
		{
			if ($this->getRowCnt() > 0)
			{	
				$manageVar=array();
				while($row=$this->getAssoc($result))
				{	
					$manageVar[]=$row;
				}
				return $manageVar;
			}
			else
			{
				return array();
			}	
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	function AddNewCustomerForwarderTranscaction($idBooking,$adminId=false)
	{	
		if((int)$idBooking>0)
		{
			$query="
				INSERT INTO
					".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
					(
					`idBooking`,
					`idForwarder`, 
					`fTotalAmount`, 
					`fTotalPriceForwarderCurrency`, 
					`idCurrency`, 
					`szCurrency`, 
					`fExchangeRate`, 
					`idForwarderCurrency`, 
					`szForwarderCurrency`, 
					`fForwarderExchangeRate`, 
					`iDebitCredit`, 
					`szBookingNotes`, 
					`szInvoice`, 
					`szBooking`, 
					`iStatus`, 
					`dtInvoiceOn`,
					`dtCreatedOn`, 
					`dtUpdatedOn`, 
					`dtCreditOn`, 
					`dtDebitOn`, 
					`iBatchNo`
					)
				SELECT 
					id,
					idForwarder,
					fTotalPriceCustomerCurrency,
					fTotalPriceNoMarkupUSD*fForwarderExchangeRate,
					idCurrency,
					szCurrency,
					fExchangeRate,	
					idForwarderCurrency,
					szForwarderCurrency,
					fForwarderExchangeRate as fForwarderCurrencyExchangeRate,
					'1',
					szForwarderReference,
					szInvoice,
					szBookingRef,
					'0',
					dtBookingConfirmed,
					NOW(),
					NOW(),
					NOW(),
					'0000-00-00 00:00:00',
					'0'
				FROM
					".__DBC_SCHEMATA_BOOKING__."
				WHERE
					id='".(int)$idBooking."'
					
						
			";
			//echo $query;
			if($result = $this->exeSQL( $query ))
			{
				$query="
					UPDATE
						".__DBC_SCHEMATA_BOOKING__."
					SET
						iManagementRecieved='1'
					WHERE
						id='".(int)$idBooking."'
				";
				
				if($result = $this->exeSQL( $query ))
				{
					return true;
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function showBookingConfirmedTransaction($idBooking=false)
	{
            $query_and = "";
            if((int)$idBooking>0)
            {
                $query_and = " AND f.id = '".(int)$idBooking."' ";
            }
            $query="
                SELECT 
                    b.id,
                    b.szInvoice,
                    b.szBooking,
                    b.fTotalAmount,
                    b.szCurrency,
                    b.dtInvoiceOn,
                    f.dtCutOff,
                    f.iBookingCutOffHours,
                    f.dtWhsCutOff,
                    f.iPaymentType,
                    f.szEmail,
                    f.szBookingRef,
                    f.id as BookingId,
                    f.iInsuranceIncluded,
                    f.fTotalInsuranceCostForBookingCustomerCurrency,
                    f.fTotalVat,
                    f.iBookingQuotes,
                    f.iCourierBooking,
                    f.iPartnerBooking,
                    f.szBookingRandomNum,
                    f.iTransferConfirmed,
                    f.dtSnooze,
                    f.iBookingType,
                    f.iQuickQuote,
                    f.idServiceType,
                    f.iPaymentTypePartner
                FROM
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS b
                INNER JOIN
                    ".__DBC_SCHEMATA_BOOKING__." AS f	
                ON
                    (f.id=b.idBooking)	
                WHERE 
                    b.iStatus = '0'
                AND
                    f.idBookingStatus IN ('3','4') 
                $query_and
                ORDER BY b.id	 DESC	
            ";
            //echo $query;
            if($result = $this->exeSQL( $query ))
            {
                if ($this->getRowCnt() > 0)
                {	
                    $manageVar=array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {	
                        $manageVar[$ctr]=$row;
                        $ctr++;
                    }
                    return $manageVar;
                }
                else
                {
                    return array();
                }	
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
	
	function changePaymentRecievedStatus($id,$check,$date,$comment,$bMarkupBooking=false,$iUpdateBankTransfer=false,$szSelfInvoice='')
	{
            if($check > 0)
            {
                if($bMarkupBooking)
                {
                    $query_update = " ,iForwarderGPType=2 ";
                }  
                if($iUpdateBankTransfer)
                {
                    $query_update = " ,szSelfInvoice='".  mysql_escape_custom($szSelfInvoice)."' ";
                }
                $kBilling = new cBilling();
                $transactionDetailsAry = array();
                $transactionDetailsAry = $kBilling->getTransactionDetails($id);
                
                if($transactionDetailsAry['iTransferConfirmed']==1)
                { 
                    $dtInvoiceOn = $date;
                }
                else
                {
                    $dtInvoiceOn = $transactionDetailsAry['dtInvoiceOn'];  
                } 
                
		$query="
                    UPDATE
                        ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                    SET
                        iStatus='1',
                        szComment='".mysql_escape_custom($comment)."',
                        dtPaymentConfirmed ='".mysql_escape_custom($dtInvoiceOn)."',
                        dtPaymentSettled = now(),
                        iTransferConfirmed = '0'
                        $query_update
                    WHERE
                        id='".$id."'
                ";
//                echo $query;
//                die;
                if($result = $this->exeSQL( $query ))
                {
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
	
	function viewCustomerDetails()
	{
            $query="
		SELECT 
			c.dtCreateOn,
			c.iActive,
			c.szEmail,
			c.iConfirmed,
			co.szCountryName,
			COUNT(b.id)
			
		FROM 
			".__DBC_SCHEMATA_USERS__."	as c
		INNER JOIN 
			".__DBC_SCHEMATA_COUNTRY__." as co	
		ON 
			(co.id=c.idCountry)	
		LEFT JOIN 
			".__DBC_SCHEMATA_BOOKING__." as b	
		ON (c.id=b.idUser AND idBookingStatus IN (3,4))	
		GROUP BY c.id
		";
		//ECHO $query;
		if($result = $this->exeSQL( $query ))
		{
			return true;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
		
	}
        function getCapsuleCrmAPIKey()
        {
            $kWHSSearch = new cWHSSearch();
            $idCapsuleAdminID = $kWHSSearch->getManageMentVariableByDescription('__CAPSULE_OWNER_OF_NEW_CUSTOMER__');
            $this->getAdminDetails($idCapsuleAdminID);
            $szCapsuleApiKey =  $this->szCapsule ;
            if(empty($szCapsuleApiKey))
            {
                $szCapsuleApiKey = __CAPSULE_API_USER_NAME__ ;
            }
             
            return $szCapsuleApiKey ;
        }
        
	function viewManagementDetails($id = 0,$iCapsuleFlag=false,$pendTrayFlag=false)
	{
            /*
            * Notes from Ajay (26-08-2015) - we have changed the column from iShowInDropDown > iCustomerServiceAgent, so to ensure things works correctly i have picked both iShowInDropDown and iCustomerServiceAgent in select query.
            */
            //szCapsule,
                    
            $query="
		SELECT 
                    m.id,
                    szFirstName,
                    szLastName,
                    szEmail,
                    iActive,
                    iAllAccess,
                    szDirectPhone as szPhone,
                    idInternationalDialCode,
                    szProfileType,
                    iCustomerServiceAgent,
                    iCustomerServiceAgent as iShowInDropDown,
                    (SELECT szFriendlyName FROM ".__DBC_SCHEMATA_ADMIN_ROLES__." ar WHERE ar.szRoleCode = m.szProfileType) as szProfileName
		FROM
                    ".__DBC_SCHEMATA_MANAGEMENT__." m	
                WHERE
                    isDeleted = 0
		";
            
		if((int)$id>0)
		{
                    $query .= "
                        AND 
                           id = ".(int)$id."
                    ";
		}
                /*else if($iCapsuleFlag)
                {
                    $query .= "
                        AND 
                            szCapsule != ''
                        AND
                            iActive = '1'
                    ";
                }*/ 
                
                if($pendTrayFlag)
                { 
                    $query .= "
                            AND
                                iCustomerServiceAgent='1'
                            AND
                                iActive = '1'
                            ";
                }
                $query .= " ORDER BY iActive DESC, szFirstName ASC, szLastName ASC ";
                
		//ECHO $query;
		if($result = $this->exeSQL( $query ))
		{
			if ($this->getRowCnt() > 0)
			{	
				$manageVar=array();
				while($row=$this->getAssoc($result))
				{	
					$manageVar[]=$row;
				}
				return $manageVar;
			}
			else
			{
				return array();
			}	
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
		
	}
        
        function getAllAdminRoles($id=0)
        { 
            $queryWhere='';
            if((int)$id>0)
            {
                $queryWhere="WHERE id='".(int)$id."'";
            }
            $query="
		SELECT 
                    id,
                    szRoleCode,
                    szFriendlyName 
		FROM
                    ".__DBC_SCHEMATA_ADMIN_ROLES__."
                $queryWhere         
            "; 
            if($result = $this->exeSQL( $query ))
            { 
                $adminRolesAry = array();
                while($row=$this->getAssoc($result))
                {	
                    $adminRolesAry[] = $row;
                }
                return $adminRolesAry; 
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
        }
        
	function viewInvestorsDetails($idInvestor= false)
	{ 
            $query="
		SELECT 
                    id,
                    szFirstName,
                    szLastName,
                    szEmail,
                    iActive,
                    dtLastLogin 
		FROM
                    ".__DBC_SCHEMATA_INVESTORS__."	
            ";

            if((int)$idInvestor>0)
            {
                $query .= "
                    WHERE 
                        id = ".(int)$idInvestor."
                    ";
            } 
            //ECHO $query;
            if($result = $this->exeSQL( $query ))
            {
                if ($this->getRowCnt() > 0)
                {	
                    $manageVar=array();
                    while($row=$this->getAssoc($result))
                    {	
                        $manageVar[]=$row;
                    }
                    return $manageVar;
                }
                else
                {
                    return array();
                }	
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
	}
        
	function showForwarderPendingPayment($bReferralBookings=false,$idForwarder=false,$bBillingScreen=false)
	{
            if($bReferralBookings)
            {
                $query_and = " AND ft.iForwarderGPType !='".__FORWARDER_PROFIT_TYPE_MARK_UP__."' ";
            } 
            if($idForwarder>0)
            {
                $query_and .= " AND f.id = '".(int)$idForwarder."' ";
            }
            $canelBookingPaymentArr = array();
            $serviceUploadPaymentArr = array();
            if($bBillingScreen)
            {
                $serviceUploadPaymentArr = $this->showForwarderPendingPaymentServiceUpload($idForwarder);
                $canelBookingPaymentArr = $this->showForwarderPendingPaymentCancelBooking($idForwarder); 
            }
            
            $query="
                SELECT 
                    f.id,
                    f.szDisplayName,
                    f.iAutomaticPaymentInvoicing, 
                    f.szForwarderAlias,
                    c.szCurrency,
                    ft.szForwarderCurrency,
                    ft.idForwarderCurrency,
                    SUM(ft.fTotalPriceForwarderCurrency) AS fForwarderTotalPrice
                FROM 
                    ". __DBC_SCHEMATA_FORWARDERS__ ." AS f	
                LEFT JOIN 	
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS ft
                ON
                    (ft.idForwarder=f.id and iBatchNo='0' and (ft.iStatus='0' OR ft.iStatus='1') AND ft.iTransferConfirmed=0)
                LEFT JOIN 
                    ".__DBC_SCHEMATA_CURRENCY__." AS c
                ON 
                    (f.szCurrency=c.id)
                WHERE
                    f.szDisplayName!=''	 
                AND 
                    iForwarderGPType != '".__FORWARDER_PROFIT_TYPE_MARK_UP__."' 
                    $query_and
                GROUP BY 
                        f.id,ft.szForwarderCurrency	
                ORDER BY 
                        f.szDisplayName
            ";
            //echo $query;
            if($result = $this->exeSQL( $query ))
            {
                if ($this->getRowCnt() > 0)
                {	
                    $manageVar=array();
                    $ctr=0;
                    while($row=$this->getAssoc($result))
                    {	
                        if($bBillingScreen)
                        {
                            if(!empty($row['szForwarderCurrency']))
                            {
                                $fForwarderTotalPrice = $row['fForwarderTotalPrice']; 
                                if(!empty($canelBookingPaymentArr[$idForwarder][$row['szForwarderCurrency']]))
                                {
                                    $fTotalCancelledAmount = (2*$canelBookingPaymentArr[$idForwarder][$row['szForwarderCurrency']]);  
                                    $fForwarderTotalPrice = $fForwarderTotalPrice - $fTotalCancelledAmount;
                                }
                                if(!empty($serviceUploadPaymentArr[$idForwarder][$row['szForwarderCurrency']]))
                                {
                                    $fServiceUploadPrice = (2*$serviceUploadPaymentArr[$idForwarder][$row['szForwarderCurrency']]); 
                                    $fForwarderTotalPrice = $fForwarderTotalPrice - $fServiceUploadPrice;
                                }
                                $manageVar[$row['szForwarderCurrency']]['fTotalBalance'] = $fForwarderTotalPrice;
                            }
                        }
                        else
                        {
                            $manageVar[]=$row;
                            //$manageVar[$ctr]['fForwarderTotalPrice'] = $row['fTotalBookingPrice'] - $row['fTotalCancelledBookingPrice'];
                            $ctr++;
                        }  
                    }
                    return $manageVar;
                }
                else
                {
                    return array();
                }	
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
           
	}
        
        function showPendingMarkupPayments($bReferralBookings=false)
	{ 
            if($bReferralBookings)
            {
                $query_and = " AND b.iForwarderGPType !='".__FORWARDER_PROFIT_TYPE_MARK_UP__."' ";
            }
            $query="
                SELECT 
                    f.id,
                    f.szDisplayName,
                    f.iAutomaticPaymentInvoicing, 
                    f.szForwarderAlias,
                    c.szCurrency,
                    ft.szForwarderCurrency,
                    ft.idForwarderCurrency,
                    SUM(ft.fTotalPriceForwarderCurrency) AS fForwarderTotalPrice
                FROM 
                    ". __DBC_SCHEMATA_FORWARDERS__ ." AS f	
                LEFT JOIN 	
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS ft
                ON
                    (ft.idForwarder=f.id and iBatchNo='0' and ft.iStatus='1')
                LEFT JOIN 
                    ".__DBC_SCHEMATA_CURRENCY__." AS c
                ON 
                    (f.szCurrency=c.id)
                WHERE
                    f.szDisplayName!=''	 
                    $query_and
                GROUP BY 
                        f.id,ft.szForwarderCurrency	
                ORDER BY 
                        f.szDisplayName
            ";
            //ECHO $query;
            if($result = $this->exeSQL( $query ))
            {
                if ($this->getRowCnt() > 0)
                {	
                    $manageVar=array();
                    $ctr=0;
                    while($row=$this->getAssoc($result))
                    {	
                        $manageVar[]=$row;
                        //$manageVar[$ctr]['fForwarderTotalPrice'] = $row['fTotalBookingPrice'] - $row['fTotalCancelledBookingPrice'];
                        $ctr++;
                    }
                    return $manageVar;
                }
                else
                {
                    return array();
                }	
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
        
	function showForwarderPendingPaymentDetails($iPaidInvoices=false)
	{
            if($iPaidInvoices)
            {
                $query_and = " AND iMarkupPaid = 1 ";
                $query_select = " ,b.dtMarkupPaid as dtBookingConfirmed";
            }
            else
            {
                $query_and = " AND iMarkupPaid = 0 ";
                $query_select = " ,b.dtBookingConfirmed";
            }
            
            $query="
                SELECT 
                    b.id as idBooking,
                    b.szBookingRef, 
                    b.fTotalPriceForwarderCurrency,
                    b.idForwarderCurrency,
                    b.szForwarderCurrency,
                    b.fForwarderExchangeRate,
                    b.idCustomerCurrency,
                    b.szCustomerCurrency,
                    b.fCustomerExchangeRate,
                    b.fTotalVatForwarderCurrency,
                    b.fTotalVat,
                    b.szTransportMode,
                    b.idTransportMode,
                    b.iBookingType,
                    b.szForwarderQuoteCurrency,
                    b.idForwarderQuoteCurrency,
                    b.fForwarderQuoteCurrencyExchangeRate,
                    b.fForwarderTotalQuotePrice,
                    b.szBookingRandomNum,
                    f.szDisplayName,
                    f.szForwarderAlias 
                    $query_select
                FROM 
                    ". __DBC_SCHEMATA_BOOKING__ ." AS b	
                INNER JOIN
                    ".__DBC_SCHEMATA_FROWARDER__." AS f	
                ON
                    f.id = b.idForwarder
                WHERE  
                    iTransferConfirmed = 0
                AND
                    idBookingStatus IN (3,4)  
                AND
                    iForwarderGPType = '".__FORWARDER_PROFIT_TYPE_MARK_UP__."'
                    $query_and
                ORDER BY 
                    f.szDisplayName ASC,f.szForwarderAlias ASC, b.szBookingRef ASC
            ";
            //echo $query;
            if($result = $this->exeSQL( $query ))
            {
                if ($this->getRowCnt() > 0)
                {	
                    $manageVar=array();
                    while($row=$this->getAssoc($result))
                    {	
                        $manageVar[]=$row;
                    }
                    return $manageVar;
                }
                else
                {
                    return array();
                }	
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
        
	function lastTransactionToForwarderByAdmin($idForwarder)
	{
            $query="
		SELECT 
			dtPaymentConfirmed
		FROM
			".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."	
		WHERE
			idForwarder = ".(int)$idForwarder."	
		AND
			iDebitCredit = '2'
		AND
			iStatus = '2'
		AND
			(iBatchNo != '' || iBatchNo!= NULL)	
		ORDER BY 
			dtPaymentConfirmed DESC
		LIMIT 0,1			
		";
		//ECHO $query;
		if($result = $this->exeSQL( $query ))
		{
			if ($this->getRowCnt() > 0)
			{	
				
				$row=$this->getAssoc($result);
				return date('d/m/Y',strtotime($row['dtPaymentConfirmed']));
			}
			else
			{
				return 'N/A';
			}	
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
		
	}
	function showEachForwarderPendingPayment($id,$idForwarderCurrency,$bReferralBookings=false)
	{
            if($bReferralBookings)
            {
                $query_and = " AND b.iForwarderGPType != '".__FORWARDER_PROFIT_TYPE_MARK_UP__."' ";
            }
            
            $query="
                SELECT 
                    ft.id,
                    ft.dtCreditOn,
                    ft.szBooking,
                    ft.fTotalPriceForwarderCurrency,
                    ft.szCurrency,
                    ft.fReferalPercentage,
                    ft.fReferalAmount,
                    ft.szForwarderCurrency,
                    b.szBookingRef,
                    b.szInvoice as szCustomerInvoice,
                    ft.iDebitCredit,
                    b.iCourierBooking,
                    b.id AS idBooking,
                    b.iHandlingFeeApplicable,
                    b.fTotalHandlingFeeUSD,
                    b.fForwarderExchangeRate,
                    ft.fLabelFeeUSD,
                    b.fTotalHandlingFeeForwarderCurrency,
                    b.fVATPercentage,
                    b.fTotalForwarderPriceWithoutHandlingFee,
                    b.fTotalSelfInvoiceAmount,
                    b.iFinancialVersion,
                    ft.szSelfInvoice,
                   (SELECT cb.iCourierAgreementIncluded FROM ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__. " cb WHERE cb.idBooking = b.id ) as iCourierAgreementIncluded 
                FROM 
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS ft
                INNER JOIN
                    ".__DBC_SCHEMATA_BOOKING__." AS b
                ON
                    b.id=ft.idBooking
                WHERE 
                    ft.idForwarder = '".mysql_escape_custom(trim($id))."'
                AND
                    b.iTransferConfirmed = '0'
                AND
                    (ft.iStatus='1' OR (ft.iStatus='0' AND ft.iTransferConfirmed = '0'))
                AND
                    ft.idForwarderCurrency='".(int)$idForwarderCurrency."'
                AND
                    iBatchNo = 0
                AND
                    iDebitCredit!='5'
                    $query_and
                ORDER BY
                    ft.dtCreditOn DESC
            ";
            //echo "<br/>".$query."<br />";
            if($result = $this->exeSQL( $query ))
            {
                if ($this->getRowCnt() > 0)
                {	
                    $manageVar=array();
                    while($row=$this->getAssoc($result))
                    {	
                        $manageVar[]=$row;
                    }
                    return $manageVar;
                }
                else
                {
                    return array();
                }	
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
	
	function showBookingConfirmedSettledTransaction()
	{
		$query="
                    SELECT 
                        b.id,
                        b.idBooking,
                        f.szDisplayName,
                        b.szInvoice,
                        b.szBooking,
                        b.fTotalAmount,
                        bk.szEmail,
                        b.szComment,
                        b.dtPaymentConfirmed,
                        b.szCurrency,
                        bk.szBookingRef,
                        b.iDebitCredit,
                        b.dtPaymentSettled,
                        bk.iInsuranceIncluded,
                        bk.fTotalInsuranceCostForBookingCustomerCurrency,
                        bk.fTotalVat,
                        bk.szBookingRandomNum
                    FROM
                        ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS b
                    INNER JOIN
                        ".__DBC_SCHEMATA_FROWARDER__." AS f	
                    ON
                        (f.id=b.idForwarder)
                    INNER JOIN
                        ".__DBC_SCHEMATA_BOOKING__." AS bk	
                    ON
                        (bk.id=b.idBooking)	
                    WHERE 
                        iStatus<>'0' 
                    AND
                        iDebitCredit IN ('1','6')
                    ORDER BY b.dtPaymentSettled DESC	
		";
		//echo $query;
		if($result = $this->exeSQL( $query ))
		{
                    if ($this->getRowCnt() > 0)
                    {	
                        $manageSettledVar=array();
                        while($row=$this->getAssoc($result))
                        {	
                            $manageSettledVar[]=$row;
                        }
                        return $manageSettledVar;
                    }
                    else
                    {
                        return array();
                    }	
		}
		else
		{
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
		}
	}
	
	function updatePaymentComment($szComment,$idPayment)
	{
		if((int)$idPayment>0)
		{
			$query="
				UPDATE
					".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
				SET
					szComment='".mysql_escape_custom($szComment)."'
				WHERE
					id='".(int)mysql_escape_custom($idPayment)."'
			";
			if($result = $this->exeSQL( $query ))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;			
		}
	}
	
	
	function getReferralFeeForwarderAmount($transferArr)
	{
            if($transferArr!='')
            {
                $query="
                    SELECT 
                        SUM(fTotalPriceForwarderCurrency) AS totalCurrencyValue,
                        SUM(fReferalAmount) AS referralfee,
                        (SUM(ROUND(fTotalPriceForwarderCurrency,2))-SUM(ROUND(fReferalAmount,2))) AS fPaidToForwarder,
                        szForwarderCurrency,
                        idForwarder,
                        idForwarderCurrency				
                    FROM 
                        ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                    WHERE 
                        id IN (".mysql_escape_custom(trim($transferArr)).")
                    AND
                        (iStatus='1' OR (iStatus='0' AND iTransferConfirmed = '0'))
                ";
                // echo $query;
                if($result = $this->exeSQL( $query ))
                {
                    if ($this->getRowCnt() > 0)
                    {	
                        $amountValueArr=array();
                        while($row=$this->getAssoc($result))
                        {
                            $amountValueArr[]=$row;
                        }
                        return $amountValueArr;
                    }
                    else
                    {
                        return array();
                    }
                }
            }
            else
            {
                return array();
            }
	}
	
	function generateInvoiceForPayment($bSelfBilledInvoice=false)
	{	
            $szInvoice = $this->getMaxInvoiceNumberForPayment();
            $szInvoice_formatted = $this->getFormattedString($szInvoice+1,6);
            $customer_code = $szInvoice_formatted ;

            if($bSelfBilledInvoice)
            {
                $customer_code = $customer_code;
            }
            $bookingInvoiceAry = array();
            $bookingInvoiceAry['iMaxNumInvoice'] = $szInvoice+1 ;
            $bookingInvoiceAry['szInvoice'] = $customer_code;
            //$bookingInvoiceAry['idForwarder'] = $idForwarder;
            $this->addPaymentInvoiceLogs($bookingInvoiceAry);
            /*
            if (($check = $this->alreadyUsed("INVOICE", $customer_code)) === true)
            {
                    $szInvoice = $szInvoice+1 ;
                    $customer_code = $this->generateInvoice($szInvoice);
            }	
            */	
            return $customer_code;
	}
	
	function getMaxInvoiceNumberForPayment()
	{
		$query="
			SELECT
				MAX(iMaxNumInvoice) iMaxInvoice
			FROM
				".__DBC_SCHEMATA_PAYMENT_INVOICE_LOGS__."
		";
		if($result=$this->exeSQL($query))
		{
			$row=$this->getAssoc($result);
			return $row['iMaxInvoice'];
		}
		else
		{
			return 0;
		}
	}
	
	function getFormattedString($num_str,$length=3)
	{
		$iNumLength = strlen($num_str);
		if($iNumLength==$length)
		{
			return $num_str ;
		}
		elseif($iNumLength<$length)
		{
			$zero_str='';
			for($i=0;$i<($length-$iNumLength);$i++)
			{
				$zero_str .="0"; 
			}
			return $zero_str.$num_str ;
		}
		else
		{
			return substr($num_str,0,($length-1));
		}
	}
	
	function addPaymentInvoiceLogs($data)
	{
		if(!empty($data))
		{
			$query = "
				INSERT INTO
					".__DBC_SCHEMATA_PAYMENT_INVOICE_LOGS__."
				(
					iMaxNumInvoice,
					szInvoice,
					dtCreatedOn
				)
				VALUES
				(
					'".mysql_escape_custom($data['iMaxNumInvoice'])."',
					'".mysql_escape_custom($data['szInvoice'])."',
					now()
				)	
			";
			//echo  $query;
			if($result=$this->exeSQL($query))
			{
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	
	function insertReferralFee($data,$bMarkupFee=false)
	{ 
            if(!empty($data))
            {
                $iDebitCredit = 3;
                if($bMarkupFee)
                {
                    $iDebitCredit = 10 ;
                }
                
                $query="
                    INSERT INTO
                        ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                    (
                        idBooking,
                        idForwarder,
                        fTotalPriceForwarderCurrency,
                        szCurrency,
                        szForwarderCurrency,
                        szInvoice,
                        szBooking,
                        dtCreatedOn,
                        iDebitCredit,
                        iStatus,
                        iBatchNo,
                        dtInvoiceOn,
                        dtPaymentConfirmed,
                        fUSDReferralFee,
                        fExchangeRate
                    )
                    VALUES
                    (
                        '".(int)mysql_escape_custom($data['idBooking'])."',
                        '".(int)mysql_escape_custom($data['idForwarder'])."',
                        '".(float)mysql_escape_custom($data['referralFee'])."',
                        '".mysql_escape_custom($data['currencyName'])."',
                        '".mysql_escape_custom($data['currencyName'])."',
                        '".mysql_escape_custom($data['invoiceNumber'])."',
                        '".mysql_escape_custom($data['szDescription'])."',
                        NOW(),
                        '".(int)$iDebitCredit."',
                        '2',
                        '".mysql_escape_custom($data['invoiceNumber'])."',
                        NOW(),
                        NOW(),
                        '".mysql_escape_custom($data['fUSDReferralFee'])."',
                        '".mysql_escape_custom($data['fExchangeRate'])."'
                    )	
                ";
                //echo $query;
                if($result=$this->exeSQL($query))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            } 
	}
	
	function insertForarderPayment($data)
	{ 
            if(!empty($data))
            {
                $kBooking = new cBooking();
                $dtCurrentDate = $kBooking->getRealNow();
                
                if(!empty($data['dtInvoiceOn']))
                {
                    $dtInvoiceOn = $data['dtInvoiceOn'];
                }
                else
                {
                    $dtInvoiceOn = $dtCurrentDate;
                }
                
                $query="
                    INSERT INTO
                        ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                    (
                        idForwarder,
                        fTotalPriceForwarderCurrency,
                        szCurrency,
                        szForwarderCurrency,
                        szInvoice,
                        szBooking,
                        dtCreatedOn,
                        iDebitCredit,
                        iStatus,
                        iBatchNo,
                        dtInvoiceOn,
                        fExchangeRate,
                        dtPaymentScheduled,
                        iFinancialVersion,
                        iTransferEmailTobeSent
                    )
                    VALUES
                    (
                        '".(int)mysql_escape_custom($data['idForwarder'])."',
                        '".(float)mysql_escape_custom($data['forwarderAmount'])."',
                        '".mysql_escape_custom($data['currencyName'])."',
                        '".mysql_escape_custom($data['currencyName'])."',
                        '".mysql_escape_custom($data['invoiceNumber'])."',
                        '".mysql_escape_custom($data['szDescription'])."',
                        NOW(),
                        '2',
                        '1',
                        '".mysql_escape_custom($data['invoiceNumber'])."',
                        '".mysql_escape_custom($dtInvoiceOn)."', 
                        '".mysql_escape_custom($data['fExchangeRate'])."',
                        '".mysql_escape_custom($data['dtPaymentScheduled'])."',
                        '2',
                        '".mysql_escape_custom($data['iTransferEmailTobeSent'])."'
                    )	
                ";
                //echo $query;
                if($result=$this->exeSQL($query))
                {
                        return true;
                }
                else
                {
                        return false;
                }
            } 
	}
	
	
	function updateForwarderPaymentStatus($transArr,$invoiceNumber)
	{
            if($transArr!='')
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                    SET
                        iBatchNo='".mysql_escape_custom($invoiceNumber)."'
                    WHERE
                        id IN (".mysql_escape_custom(trim($transArr)).")
                ";  
                if($result=$this->exeSQL($query))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
	}
	
	function showForwarderTransferPayment($idForwarderTransaction=false,$idForwarder=false)
	{
            if($idForwarderTransaction>0)
            {
                $query_and = " WHERE ft.id = '".(int)$idForwarderTransaction."' ";
            }
            if($idForwarder>0)
            {
                $query_and .= " WHERE ft.idForwarder = '".(int)$idForwarder."' ";
                $query_order_by = " ORDER BY ft.dtPaymentScheduled DESC LIMIT 0,1";
            }
            else
            {
                $query_order_by = " ORDER BY ft.dtCreatedOn ASC ";
            }
            
            $query="
                SELECT 
                    ft.id,
                    ft.idForwarder,
                    f.szDisplayName,
                    f.szBankName,
                    f.szSwiftCode,
                    f.iAccountNumber, 
                    f.szForwarderAlias,
                    ft.szCurrency,
                    ft.fTotalPriceForwarderCurrency AS fForwarderTotalPrice,
                    ft.dtCreatedOn,
                    ft.iBatchNo,
                    ft.idForwarder,
                    ft.szBooking,
                    ft.dtPaymentScheduled,
                    ft.dtInvoiceOn
                FROM 
                    ". __DBC_SCHEMATA_FORWARDERS__ ." AS f	
                INNER JOIN 	
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS ft
                ON
                    (ft.idForwarder=f.id and iBatchNo<>'0' and ft.iStatus='1' and ft.iDebitCredit='2')	
                    $query_and
                    $query_order_by
            ";
            //ECHO $query;
            if($result = $this->exeSQL( $query ))
            {
                if ($this->getRowCnt() > 0)
                {	
                    if($idForwarderTransaction>0)
                    {
                        $row = $this->getAssoc($result);  
                        return $row;
                    }
                    else if($idForwarder>0)
                    {
                        $row = $this->getAssoc($result);  
                        return $row;
                    }
                    else
                    {
                        $transferPaymentVar=array();
                        while($row=$this->getAssoc($result))
                        {	
                            $transferPaymentVar[]=$row;
                        }
                        return $transferPaymentVar;
                    } 
                }
                else
                {
                    return array();
                }	
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
	
	function totalAmountConfirm($batchNumber)
	{
            $query="
                SELECT 
                    SUM(fTotalPriceForwarderCurrency) AS totalCurrencyValue,
                    SUM(fReferalAmount) AS referralfee,
                    (SUM(fTotalPriceForwarderCurrency)-SUM(fReferalAmount)) AS fPaidToForwarder,
                    szForwarderCurrency,
                    fForwarderExchangeRate,
                    szForwarderCurrency,
                    idForwarder
                FROM 
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                WHERE
                    iBatchNo='".mysql_escape_custom($batchNumber)."'
                AND
                    iStatus='2'
                AND
                    iDebitCredit='1'
            ";
            //echo $query;
            if($result = $this->exeSQL( $query ))
            {
                if ($this->getRowCnt() > 0)
                {
                    $transferPaymentVar=array();
                    while($row=$this->getAssoc($result))
                    {	
                        $transferPaymentVar[]=$row;
                    }
                    return $transferPaymentVar;
                }
            }
            else
            {
                return array();
            }
	}
	
	function updateForwarderPayment($id)
	{ 
            if((int)$id>0)
            { 
                $billingTransferDetail = $this->showForwarderTransferPayment($id);

                $kBooking = new cBooking();
                $dtCurrentDate = $kBooking->getRealNow();
                    
                if(!empty($billingTransferDetail['dtPaymentScheduled']) && $billingTransferDetail['dtPaymentScheduled']!='0000-00-00 00:00:00')
                {
                    $dtTransferDueDate = date('Y-m-d',strtotime($billingTransferDetail['dtPaymentScheduled']));
                     
                    /*
                    * If due date is less then today then we update due date to today
                    */
                    if(strtotime($dtCurrentDate)>strtotime($dtTransferDueDate))
                    {
                        $dtTransferDueDate = date('Y-m-d H:i:s',strtotime($dtCurrentDate));
                    }
                }
                else
                {
                    $dtTransferDueDate = $dtCurrentDate;
                } 
                /*
                * In Financial version - 2, we update Payment date as when it was invoiced i mean the day when it was added to forwarder /billing/ screen
                */
                $dtPaymentConfirmed = $billingTransferDetail['dtInvoiceOn'];
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                    SET
                        iStatus='2',
                        dtPaymentConfirmed= '".mysql_escape_custom($dtPaymentConfirmed)."',
                        dtPaymentScheduled = '".mysql_escape_custom($dtTransferDueDate)."',
                        iFinancialVersion = '2'
                    WHERE
                        id='".(int)$id."'
                ";
                //echo $query;
                if($result = $this->exeSQL( $query ))
                { 
                    $query="
                            SELECT
                                iBatchNo
                            FROM
                                ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                            WHERE
                                id='".(int)$id."'
                    ";
                    if($result = $this->exeSQL( $query ))
                    {
                        if ($this->getRowCnt() > 0)
                        {
                            $row=$this->getAssoc($result);
                            $iBatchNo=$row['iBatchNo'];

                            $query="
                                UPDATE
                                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                                SET
                                    iStatus='2'
                                WHERE
                                    iBatchNo='".mysql_escape_custom($iBatchNo)."'
                                AND
                                    iStatus='1'
                                AND
                                    iDebitCredit='1'
                                ";
                                $result = $this->exeSQL( $query );
                        }
                    }
                    return true;
                }
                else
                {
                    return true;
                }
            }
	}
	
	function getAllCustomers($sortBy='',$sortOrder='',$iPage=false,$row_count=false)
	{
            $sql='';
            if($sortBy!='' && $sortOrder!='')
            {
                $sql=" ".$sortBy." ".$sortOrder." ";
            }
            else
            {
                $sql=" u.dtCreateOn DESC  ";
            } 
            if($iPage>0)
            {
                $query_limit .= "
                    LIMIT
                        ".(($iPage -1 ) * __MAX_CUSTOMER_PER_PAGE__ ).",". __MAX_CUSTOMER_PER_PAGE__ ."
                ";
            }
            
            if($row_count)
            {
                $query_select = " count(u.id) as iNumCustomer ";
            }
            else
            {
                $userBookingDataAry = array();
                $userBookingDataAry = $this->getAllBookingForUser(false,true);
                  
                $query_select = " 
                    u.id,
                    u.szEmail,
                    u.dtCreateOn,
                    u.dtLastLogin,
                    u.iConfirmed,
                    u.iActive,
                    c.szCountryISO,
                    u.iNumBookings as total,
                    u.iLastestScore as iScore,
                    u.fTotalRevenue as totalAmount,
                    u.dtLastBooking AS dtBookingConfirmed
                ";
                $query_group_by = " GROUP BY u.id ";
            } 
            
            $query="
                SELECT
                  $query_select  
                FROM
                    ".__DBC_SCHEMATA_USERS__." AS u 
                LEFT JOIN
                    ".__DBC_SCHEMATA_COUNTRY__." AS c
                ON 
                    u.idCountry=c.id
                $query_group_by
                ORDER BY
                    ".$sql."
                    $query_limit
            ";
            //echo "<br><br><br>".$query; 
            if($result = $this->exeSQL( $query ))
            {
                if ($this->getRowCnt() > 0)
                {
                    if($row_count)
                    {  
                        $row = $this->getAssoc($result);
                        return $row['iNumCustomer'];
                    }
                    else
                    {
                        $userDetailArr=array();
                        $ctr = 0;
                        while($row=$this->getAssoc($result))
                        {	
                            $userDetailArr[$ctr]=$row;
                            if(!empty($userBookingDataAry[$row['id']]))
                            {
                                $userDetailArr[$ctr]['total'] = $userBookingDataAry[$row['id']]['total'];
                                $userDetailArr[$ctr]['totalAmount'] = $userBookingDataAry[$row['id']]['totalAmount'];
                                $userDetailArr[$ctr]['dtBookingConfirmed'] = $userBookingDataAry[$row['id']]['dtBookingConfirmed'];
                                $userDetailArr[$ctr]['dtBookingConfirmedTimeStamp'] = strtotime($userBookingDataAry[$row['id']]['dtBookingConfirmed']);
                            }
                            $ctr++;
                        }
                        
                        if($sortBy=='total')
                        {
                            if($sortOrder=='DESC')
                            {
                                $userDetailArr = sortArray($userDetailArr,'total','DESC');
                            }
                            else
                            {
                                $userDetailArr = sortArray($userDetailArr,'total');
                            }
                        }
                        else if($sortBy=='totalAmount')
                        {
                            if($sortOrder=='DESC')
                            {
                                $userDetailArr = sortArray($userDetailArr,'totalAmount','DESC');
                            }
                            else
                            {
                                $userDetailArr = sortArray($userDetailArr,'totalAmount');
                            }
                        } 
                        else if($sortBy=='dtBookingConfirmed')
                        {
                            if($sortOrder=='DESC')
                            {
                                $userDetailArr = sortArray($userDetailArr,'dtBookingConfirmedTimeStamp','DESC');
                            }
                            else
                            {
                                $userDetailArr = sortArray($userDetailArr,'dtBookingConfirmedTimeStamp');
                            }
                        } 
                        return $userDetailArr;
                    } 
                }
            }
            else
            {
                return array();
            }
	}
	
	function getAllBookingForUser($idUser=false,$bGetAll=false)
	{
            if((int)$idUser>0 || $bGetAll)
            {
                if($bGetAll)
                {
                    $query_and = "";
                    $query_group_by = " GROUP BY idUser ";
                }
                else
                {
                    $query_and = " AND idUser='".(int)mysql_escape_custom($idUser)."' ";
                }
                
                $query="
                    SELECT
                        idUser,
                        count(id) as total,
                        SUM(fTotalPriceUSD ) as totalAmount,
                        MAX(dtBookingConfirmed) AS dtBookingConfirmed
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__."
                    WHERE 
                        idBookingStatus IN ('3','4')
                        $query_group_by
                    ORDER BY
                        dtBookingConfirmed ASC
                ";
                //echo "<br><br>".$query;
                if($result = $this->exeSQL( $query ))
                {
                    if ($this->getRowCnt() > 0)
                    {
                        $userBookingArr=array();
                        while($row=$this->getAssoc($result))
                        {	
                            if($bGetAll)
                            {
                                $userBookingArr[$row['idUser']] = $row;
                            }
                            else
                            {
                                $userBookingArr[] = $row;
                            } 
                        }
                        return $userBookingArr;
                    }
                    else
                    {
                        return array();
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
	
	function changeAdminStatus($id,$status)
	{
		$id 	=	(int)sanitize_all_html_input($id);
		$status	=	(int)sanitize_all_html_input($status);
		if($id>0)
		{
			$query="
				UPDATE
					".__DBC_SCHEMATA_MANAGEMENT__."
				SET
					iActive=(!iActive)
				WHERE 
					id=".mysql_escape_custom($id)."
			";
			if($result = $this->exeSQL($query))
			{
                            if($status==1)
                            {
                                $queryCustomerOwer="
                                    UPDATE
                                        ".__DBC_SCHEMATA_USERS__."
                                    SET
                                        idCustomerOwner='0'
                                    WHERE
                                        idCustomerOwner=".mysql_escape_custom($id)."
                                ";
                                $result = $this->exeSQL($queryCustomerOwer);
                                
                                
                                $selOwnerFileId="
                                    SELECT
                                        bf.id
                                    FROM
                                        ".__DBC_SCHEMATA_FILE_LOGS__." AS bf
                                    INNER JOIN
                                        ".__DBC_SCHEMATA_BOOKING__." AS b
                                    ON        
                                        bf.id=b.idFile                                    
                                    WHERE
                                        bf.idCustomerOwner='".(int)$id."'
                                    AND
                                        b.idBookingStatus NOT IN ('3','4','7')                                        
                                   "
                                   ;
                                    if($resultFileId = $this->exeSQL( $selOwnerFileId ))
                                    {
                                        if ($this->getRowCnt() > 0)
                                        {
                                            $resArr=array();
                                            while($row=$this->getAssoc($resultFileId))
                                            {
                                                $resArr[]=$row['id'];
                                            }
                                            
                                            if(count($resArr)>0)
                                            {
                                                $strCustOwnerId=implode("','",$resArr);
                                                
                                                $queryCustomerOwer="
                                                    UPDATE
                                                        ".__DBC_SCHEMATA_FILE_LOGS__."
                                                    SET
                                                        idCustomerOwner='0'
                                                    WHERE
                                                        id IN  ('".$strCustOwnerId."')
                                                ";
                                                $result = $this->exeSQL($queryCustomerOwer);
                                            }
                                        }
                                    }
                                    
                                     $selOwnerFileId="
                                    SELECT
                                        bf.id
                                    FROM
                                        ".__DBC_SCHEMATA_FILE_LOGS__." AS bf
                                    INNER JOIN
                                        ".__DBC_SCHEMATA_BOOKING__." AS b
                                    ON        
                                        bf.id=b.idFile                                    
                                    WHERE
                                        bf.idFileOwner='".(int)$id."'
                                    AND
                                        b.idBookingStatus NOT IN ('3','4','7')                                        
                                   "
                                   ;
                                    if($resultFileId = $this->exeSQL( $selOwnerFileId ))
                                    {
                                        if ($this->getRowCnt() > 0)
                                        {
                                            $resArr=array();
                                            while($row=$this->getAssoc($resultFileId))
                                            {
                                                $resArr[]=$row['id'];
                                            }
                                            
                                            if(count($resArr)>0)
                                            {
                                                $strCustOwnerId=implode("','",$resArr);
                                                
                                                $queryCustomerOwer="
                                                    UPDATE
                                                        ".__DBC_SCHEMATA_FILE_LOGS__."
                                                    SET
                                                        idFileOwner='0'
                                                    WHERE
                                                        id IN  ('".$strCustOwnerId."')
                                                ";
                                                $result = $this->exeSQL($queryCustomerOwer);
                                            }
                                        }
                                    }
                                    
                                    
                                   
                            }
                                return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			} 		
		}
	}
        
        //This function is called from cronjob/updateCustomerRevenue.php
        function getAllCustomersCronjob()
	{  
            $query="
                SELECT
                    u.id,
                    u.szEmail, 
                    count(b.id) as total, 
                    SUM(b.fTotalPriceUSD) as totalAmount,
                    MAX(b.dtBookingConfirmed) AS dtBookingConfirmed
                FROM
                    ".__DBC_SCHEMATA_USERS__." AS u
                LEFT JOIN	
                    ".__DBC_SCHEMATA_BOOKING__." AS b
                ON
                    (b.idUser=u.id and b.idBookingStatus IN ('3','4') AND b.iTransferConfirmed = '0') 
                GROUP BY
                    u.id	 
            ";
            //echo $query;
            if($result = $this->exeSQL( $query ))
            {
                if ($this->getRowCnt() > 0)
                {
                    $userDetailArr=array();
                    while($row=$this->getAssoc($result))
                    {	
                        $userDetailArr[]=$row;
                    }
                    return $userDetailArr;
                }
            }
            else
            {
                return array();
            }
	}
        
	function createNewAdminProfile($createProfile,$id)
	{
		$id=(int)sanitize_all_html_input($id);
		if($id>0)
		{	
			$this->set_szNewFirstName($this->sanitizeData($createProfile['fName']));
			$this->set_szNewLastName($this->sanitizeData($createProfile['lName']));
			$this->set_szNewEmail($this->sanitizeData($createProfile['eMail']));
			$this->set_szNewPassword($this->sanitizeData($createProfile['password']));
			$this->set_iAllAccess($this->sanitizeData($createProfile['allAccess']));
                        //$this->set_szCapsule($this->sanitizeData($createProfile['szCapsule']));
                        $this->set_szProfileType($this->sanitizeData($createProfile['szProfileType'])); 
                        $this->set_idInternationalDialCode(trim(sanitize_all_html_input(strtolower($createProfile['idInternationalDialCode']))));
                        $this->set_szPhoneNumber(trim(sanitize_all_html_input(strtolower($createProfile['szPhone']))));
                 
			if($this->error === true)
			{
                            return false;
			}
			$validateEmail=$this->isAdminEmailExist($this->szNewEmail);
			if($validateEmail==true)
			{
                            $this->addError( "szNewEmail" , t($this->t_base.'messages/already_exists') );
                            return false;
			}
			if($this->error === false)
			{
				$query="
                                    INSERT INTO
                                        ".__DBC_SCHEMATA_MANAGEMENT__."
                                    (
                                        `szFirstName` ,
                                        `szLastName` ,
                                        `szEmail` ,
                                        `szPassword` ,
                                        `idInternationalDialCode`,
                                        `szDirectPhone`,
                                        `iActive` ,
                                        `dtCreatedOn` ,
                                        `dtUpdatedOn`,
                                        `iAllAccess`,
                                        szProfileType,
                                        iCustomerServiceAgent
                                    )
                                    VALUES 
                                    (
                                        '".mysql_escape_custom($this->szNewFirstName)."',
                                        '".mysql_escape_custom($this->szNewLastName)."',
                                        '".mysql_escape_custom($this->szNewEmail)."',
                                        '".mysql_escape_custom(md5($this->szNewPassword))."',
                                        '".mysql_escape_custom($this->idInternationalDialCode)."',
                                        '".mysql_escape_custom($this->szPhoneNumber)."',
                                        1,
                                        NOW(),
                                        NOW(),
                                        '".mysql_escape_custom($this->iAllAccess)."',
                                        '".mysql_escape_custom($this->szProfileType)."',
                                        '".(int)$createProfile['iShowInDropDown']."'    
                                    )
				";
				//ECHO $query;
                                //die;
				if($result = $this->exeSQL($query))
				{
                                    return true;
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				} 
			}
			
		}
	}
	function editAdminProfile($createProfile,$id)
	{
		$id=(int)sanitize_all_html_input($id);
		if($id>0)
		{	
			$this->set_szNewFirstName($this->sanitizeData($createProfile['fName']));
			$this->set_szNewLastName($this->sanitizeData($createProfile['lName']));
			$this->set_szNewEmail($this->sanitizeData($createProfile['eMail']));
			$this->set_id($this->sanitizeData($createProfile['id']));
			$this->set_iAllAccess($this->sanitizeData($createProfile['allAccess']));
                        //$this->set_szCapsule($this->sanitizeData($createProfile['szCapsule']));
                        $this->set_szProfileType($this->sanitizeData($createProfile['szProfileType']));
                        $this->set_idInternationalDialCode(trim(sanitize_all_html_input($createProfile['idInternationalDialCode'])));
                        $this->set_szPhoneNumber(trim(sanitize_all_html_input($createProfile['szPhone'])));
                        
			if($this->error === true)
			{
				return false;
			}
			$validateEmail=$this->isAdminEmailExist($this->szNewEmail,$this->id);
			if($validateEmail==true)
			{
                            $this->addError( "szNewEmail" , t($this->t_base.'messages/already_exists') );
                            return false;
			}
			if($this->error === false)
			{
                            //`szCapsule` = '".mysql_escape_custom($this->szCapsule)."'
                                    
                            $query=
                                "
                                UPDATE
                                    ".__DBC_SCHEMATA_MANAGEMENT__."
                                SET
                                    `szFirstName`='".mysql_escape_custom($this->szNewFirstName)."',
                                    `szLastName` ='".mysql_escape_custom($this->szNewLastName)."',
                                    `szEmail` ='".mysql_escape_custom($this->szNewEmail)."',
                                    `iAllAccess` = '".mysql_escape_custom($this->iAllAccess)."',
                                    `szDirectPhone` = '".mysql_escape_custom($this->szPhoneNumber)."', 
                                    `szProfileType` = '".mysql_escape_custom($this->szProfileType)."',  
                                    `idInternationalDialCode` = '".mysql_escape_custom($this->idInternationalDialCode)."',  
                                    `dtUpdatedOn` = NOW(),
                                    `iCustomerServiceAgent`='".(int)$createProfile['iShowInDropDown']."'
                                WHERE
                                    id =".$this->id."
                            ";
//                            echo $query;
//                            die;
                            
                            if($result = $this->exeSQL($query))
                            {
                            return true;
                            }
                            else
                            {
                                    $this->error = true;
                                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                                    return false;
                            } 
			}
			
		}
	}
        
        function changeInvestorStatus($id,$status)
	{
            $id = (int)sanitize_all_html_input($id);
            $status	= (int)sanitize_all_html_input($status);
            if($id>0)
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_INVESTORS__."
                    SET
                        iActive=(!iActive)
                    WHERE 
                        id=".mysql_escape_custom($id)." 
                ";
                if($result = $this->exeSQL($query))
                {
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                } 		
            }
	}
        function editInvestorProfile($createProfile,$id)
	{
            $id=(int)sanitize_all_html_input($id);
            if($id>0)
            {	
                $this->set_szNewFirstName($this->sanitizeData($createProfile['fName']));
                $this->set_szNewLastName($this->sanitizeData($createProfile['lName']));
                $this->set_szNewEmail($this->sanitizeData($createProfile['eMail']));
                $this->set_id($this->sanitizeData($createProfile['id']));
                 
                if($this->error === true)
                {
                    return false;
                }
                $validateEmail=$this->isInvestorsEmailExist($this->szNewEmail,$this->id);
                if($validateEmail==true)
                {
                    $this->addError( "szNewEmail" , t($this->t_base.'messages/already_exists') );
                    return false;
                }
                if($this->error === false)
                {
                    $query=
                        "
                        UPDATE
                            ".__DBC_SCHEMATA_INVESTORS__."
                        SET
                            `szFirstName`='".mysql_escape_custom($this->szNewFirstName)."',
                            `szLastName` ='".mysql_escape_custom($this->szNewLastName)."',
                            `szEmail` ='".mysql_escape_custom($this->szNewEmail)."', 
                            `dtUpdatedOn` = NOW()
                        WHERE
                            id =".$this->id."
                    ";
                    //echo $query;die;
                    if($result = $this->exeSQL($query))
                    {
                        return true;
                    }
                    else
                    {
                        $this->error = true;
                        $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                        $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                        return false;
                    } 
                } 
            }
	}
        function createNewInvestorProfile($createProfile,$id)
	{
            $id=(int)sanitize_all_html_input($id);
            if($id>0)
            {	
                $this->set_szNewFirstName($this->sanitizeData($createProfile['fName']));
                $this->set_szNewLastName($this->sanitizeData($createProfile['lName']));
                $this->set_szNewEmail($this->sanitizeData($createProfile['eMail']));
                $this->set_szNewPassword($this->sanitizeData($createProfile['password']));
                
                if($this->error === true)
                {
                    return false;
                }
                $validateEmail=$this->isInvestorsEmailExist($this->szNewEmail);
                if($validateEmail==true)
                {
                    $this->addError( "szNewEmail" , t($this->t_base.'messages/already_exists') );
                    return false;
                }
                if($this->error === false)
                {
                    $query= "
                        INSERT INTO
                            ".__DBC_SCHEMATA_INVESTORS__."
                        (
                            `szFirstName` ,
                            `szLastName` ,
                            `szEmail` ,
                            `szPassword` , 
                            `iActive` ,
                            `dtCreatedOn` ,
                            `dtUpdatedOn`
                        )
                        VALUES 
                        (
                            '".mysql_escape_custom($this->szNewFirstName)."',
                            '".mysql_escape_custom($this->szNewLastName)."',
                            '".mysql_escape_custom($this->szNewEmail)."',
                            '".mysql_escape_custom(md5($this->szNewPassword))."', 
                            1,
                            NOW(),
                            NOW() 
                        )
                    ";
                    //echo $query;
                    if($result = $this->exeSQL($query))
                    {
                        return true;
                    }
                    else
                    {
                        $this->error = true;
                        $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                        $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                        return false;
                    } 
                } 
            }
	}
        
	function getAllForwarderContacts($idAdmin,$flag='',$sort='')
	{	
		if($flag!='')
		{
			$flag = (int)$this->sanitizeData($flag);	
		} 
		if($sort!='')
		{
			$sort = (string)$this->sanitizeData($sort);		
		}
		$idAdmin=(int)$this->sanitizeData($idAdmin);
		if($idAdmin==$this->id)
		{
			$query="
				SELECT 
					fc.id,
					CONCAT(fc.szFirstName,' ',fc.szLastName) AS szName,
					fc.szEmail,
					DATE_FORMAT(fc.dtLastLogin,'%d/%m/%Y %k:%i') AS dtLastLogin,
					fc.idForwarderContactRole,
					f.szDisplayName,
                                        f.szForwarderAlias
				FROM
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__." AS fc
				INNER JOIN 
					".__DBC_SCHEMATA_FORWARDERS__."	AS f
				ON
					(f.id=fc.idForwarder)
				WHERE
					fc.idForwarderContactRole IN (1,3,5,7)
				AND 
					fc.iActive = 1	
			";
			if($flag=='')
			{
				$query.="
					ORDER BY 	
						if(f.szDisplayName = '' or f.szDisplayName is null,1,0),f.szDisplayName ASC,
						if(f.szForwarderAlias = '' or f.szForwarderAlias is null,1,0),f.szForwarderAlias ASC,
                                                fc.idForwarderContactRole = 3,
                                                fc.idForwarderContactRole = 7,
						fc.idForwarderContactRole = 5,                                                
						fc.idForwarderContactRole = 1,                                                
						CONCAT(fc.szFirstName,' ',fc.szLastName)
				";
			}
			if($flag==1 && $sort=='DESC')
			{
				$query.="
					ORDER BY 	
						if(f.szDisplayName = '' or f.szDisplayName is null,1,0),f.szDisplayName DESC,
						if(f.szForwarderAlias = '' or f.szForwarderAlias is null,1,0),f.szForwarderAlias DESC,
                                                fc.idForwarderContactRole = 3,
                                                fc.idForwarderContactRole = 7,
						fc.idForwarderContactRole = 5,
						fc.idForwarderContactRole = 1,
						CONCAT(fc.szFirstName,' ',fc.szLastName) ASC
				";
			}
			if($flag==1 && $sort=='ASC')
			{
				$query.="
					ORDER BY 	
						if(f.szDisplayName = '' or f.szDisplayName is null,1,0),f.szDisplayName ASC,
                                                if(f.szForwarderAlias = '' or f.szForwarderAlias is null,1,0),f.szForwarderAlias ASC,
						fc.idForwarderContactRole = 3,
                                                fc.idForwarderContactRole = 7,
						fc.idForwarderContactRole = 5,
						fc.idForwarderContactRole = 1,
						CONCAT(fc.szFirstName,' ',fc.szLastName) ASC
				";
			}
			if($flag==2 && $sort=='DESC')
			{
				$query.="
					ORDER BY 	
						fc.idForwarderContactRole = 1,
						fc.idForwarderContactRole = 5,
                                                fc.idForwarderContactRole = 7,
						fc.idForwarderContactRole = 3,
						if(f.szDisplayName = '' or f.szDisplayName is null,1,0),f.szDisplayName,
                                                if(f.szForwarderAlias = '' or f.szForwarderAlias is null,1,0),f.szForwarderAlias,
						CONCAT(fc.szFirstName,' ',fc.szLastName) ASC
				";
			}
			if($flag==2 && $sort=='ASC')
			{
				$query.="
					ORDER BY 	
						fc.idForwarderContactRole = 3,
                                                fc.idForwarderContactRole = 7,
						fc.idForwarderContactRole = 5,
						fc.idForwarderContactRole = 1,
						if(f.szDisplayName = '' or f.szDisplayName is null,1,0),f.szDisplayName,
                                                if(f.szForwarderAlias = '' or f.szForwarderAlias is null,1,0),f.szForwarderAlias,
						CONCAT(fc.szFirstName,' ',fc.szLastName) ASC
				";
			}
			if($flag==3 && $sort=='ASC')
			{
				
				$query.="
					ORDER BY 
						if(fc.dtLastLogin = '0000-00-00 00:00:00' or fc.dtLastLogin is null,1,0),fc.dtLastLogin	 ASC							
				";
			}
			if($flag==3 && $sort=='DESC')
			{
				
				$query.="
					ORDER BY 
						if(fc.dtLastLogin = '0000-00-00 00:00:00' or fc.dtLastLogin is null,1,0),fc.dtLastLogin	 DESC							
				";
			}
			//echo $query;
			if($result = $this->exeSQL( $query ))
			{
				if ($this->getRowCnt() > 0)
				{
					$forwarderProfileArr=array();
					while($row=$this->getAssoc($result))
					{	
						$forwarderProfileArr[]=$row;
					}
					return $forwarderProfileArr;
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
function deleteTempData($idUser)
	{
		if((int)$idUser>0)
		{
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_EDIT_CUSTOMER_TEMP_DATA__."
				WHERE
					idUser='".(int)$idUser."'
			";
			$result = $this->exeSQL( $query );
		}
	}
	
	function sendCustomerPassword($idUser)
	{
		$password=create_password();
		
		$kUser = new cUser();
		$kUser->getUserDetails($idUser);
		$query="
			UPDATE
				".__DBC_SCHEMATA_USERS__."
			SET
				szPassword='".mysql_escape_custom(md5($password))."'
			WHERE
				id='".(int)$idUser."'		
		";
		//echo
		$result = $this->exeSQL( $query );
		$replace_ary['szFirstName']=$kUser->szFirstName;
		$replace_ary['szLastName']=$kUser->szLastName;
		$replace_ary['szEmail']=$kUser->szEmail;
		$replace_ary['szPassword']=$password;
		createEmail(__CHANGE_PASSWORD__, $replace_ary,$kUser->szEmail, '', __STORE_SUPPORT_EMAIL__,$kUser->id, __STORE_SUPPORT_EMAIL__,__FLAG_FOR_CUSTOMER__);
		return true;
	}
	
function addTempdata($value,$oldFlag,$idUser)
	{
		
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_EDIT_CUSTOMER_TEMP_DATA__."
			WHERE
				idUser='".(int)$idUser."'
			AND
				szFlag='".mysql_escape_custom($oldFlag)."'
		";
		if($result = $this->exeSQL( $query ))
		{
			if ($this->getRowCnt() > 0)
			{
				$query="
					UPDATE
						".__DBC_SCHEMATA_EDIT_CUSTOMER_TEMP_DATA__."
					SET
						szData='".serialize($value)."'
					WHERE
						idUser='".(int)$idUser."'
					AND
						szFlag='".mysql_escape_custom($oldFlag)."'
				";
				$result = $this->exeSQL( $query );
			}
			else
			{
				//print_r($value);
				$query="
					INSERT INTO
						".__DBC_SCHEMATA_EDIT_CUSTOMER_TEMP_DATA__."
					(
						szData,
						idUser,
						szFlag
					)
						VALUES
					(
						'".serialize($value)."',
						'".(int)mysql_escape_custom($idUser)."',
						'".mysql_escape_custom($oldFlag)."'
					)
				";
				//echo $query;
				$result = $this->exeSQL( $query );
			}
		}
	}
	
	function getTempdata($oldFlag,$idUser)
	{
		$query="
			SELECT
				szData
			FROM
				".__DBC_SCHEMATA_EDIT_CUSTOMER_TEMP_DATA__."
			WHERE
				idUser='".(int)mysql_escape_custom($idUser)."'
			AND
				szFlag='".mysql_escape_custom($oldFlag)."'
		";
		if($result = $this->exeSQL( $query ))
		{
			if ($this->getRowCnt() > 0)
			{
				while($row=$this->getAssoc($result))
				{
					$tempArr=unserialize($row['szData']);
				}
				return $tempArr;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	function getUserAllBookings($idUser)
	{
            if((int)$idUser>0)
            {
                    
                $kConfig = new cConfig();
                $configLangArr=$kConfig->getConfigurationLanguageData('__TABLE_SERVICE_TYPE__');
                
                $query="
                    SELECT
                        b.idBookingStatus,
                        b.szCustomerCurrency as szCurrency,
                        b.fTotalPriceCustomerCurrency,
                        b.szBookingRef,
                        b.dtCreatedOn,
                        b.dtBookingConfirmed,
                        b.szForwarderDispName,
                        b.idServiceType,
                        b.idOriginCountry,
                        b.idDestinationCountry
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__." As b
                    INNER JOIN
                        ".__DBC_SCHEMATA_SERVICE_TYPE__." AS st
                    ON
                        st.id=b.idServiceType
                    WHERE
                        idUser='".(int)mysql_escape_custom($idUser)."'
                    AND
                        idBookingStatus!= ".(int)__BOOKING_STATUS_ZERO_LEVEL__."	
                    ORDER BY
                        b.id DESC	
	        ";
		//echo $query;
                if($result = $this->exeSQL( $query ))
                {
                        if ($this->getRowCnt() > 0)
                        {
                                while($row=$this->getAssoc($result))
                                {
                                    if(!empty($configLangArr[$row['idServiceType']]))
                                    {
                                        $configLangArr[$row['idServiceType']]['szServiceName']=$configLangArr[$row['idServiceType']]['szDescription'];
                                        $userBookingArr[] = array_merge($configLangArr[$row['idServiceType']],$row);
                                    }else
                                    {
                                        $userBookingArr[] = $row;
                                    }
                                }
                                return $userBookingArr;
                        }
                        else
                        {
                                return array();
                        }
                }
                else
                {
                        return array();
                }
            }
	}	
	
	function getUserEmailLog($idUser,$flag=false)
	{
		if((int)$idUser>0)
		{
			$query_and="";
			if($flag==true)
			{
				$query_and="
					AND
						iMode='1'
				";
			}
			
			$query="
				SELECT
					id,
					szEmailBody,
					szEmailSubject,
				 	dtSent
				FROM
					".__DBC_SCHEMATA_EMAIL_LOG__."
				WHERE
					idUser='".(int)mysql_escape_custom($idUser)."'
					$query_and
				ORDER BY
					dtSent DESC
			";
			//echo $query;
			if($result = $this->exeSQL( $query ))
			{
				if ($this->getRowCnt() > 0)
				{
					while($row=$this->getAssoc($result))
					{
						$userEmailArr[]=$row;
					}
					return $userEmailArr;
				}
				else
				{
					return array();
				}
			}
			else
			{
				return array();
			}
			
		}
		
	}
	
	function getEmailLogDetailById($idEmailLog)
	{
            $query="
                SELECT
                    id,
                    szEmailBody,
                    szEmailSubject,
                    dtSent,
                    szToAddress,
                    iMode,
                    szEmailKey,
                    szEmailStatus,
                    dtEmailStatusUpdated,
                    iSuccess,
                    iAlreadyUtf8Encoded,
                    szAttachmentFiles,
                    szAttachmentFileName,
                    iDSTDifference
                FROM
                    ".__DBC_SCHEMATA_EMAIL_LOG__."
                WHERE
                        id='".(int)mysql_escape_custom($idEmailLog)."'
            ";
            if($result = $this->exeSQL( $query ))
            {
                if ($this->getRowCnt() > 0)
                {
                    while($row=$this->getAssoc($result))
                    {
                        $userEmailArr[]=$row;
                    }
                    return $userEmailArr;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                return array();
            }
	}
        
	function updateVersion($comment,$mode)
	{	
		$mode= trim(sanitize_all_html_input($mode));
		$this->set_szComment(trim(sanitize_specific_tinymce_html_input($comment)));
		if($this->error===true)
		{
		return false;
		}
		
	 	if($mode=='CUSTOMER')
	 	{
	 		$dateVersionUpdate=$this->searchLastVersion(__DBC_SCHEMATA_MANAGEMENT_TERMS_VERSION_CUSTOMER__);
	 		$szDescription = 'Customer T&C';
	 		$dtDateVersion = DATE('d. F Y',strtotime($dateVersionUpdate['dtVersionUpdated']));
	 		$this->version = '';
	 		$iFlag='1';
	 	}
		if($mode=='FORWARDER')
	 	{
	 		$dateVersionUpdate=$this->searchLastVersion(__DBC_SCHEMATA_MANAGEMENT_TERMS_VERSION__);
	 		$dtDateVersion = DATE('d. F Y',strtotime($dateVersionUpdate['dtVersionUpdated']));
	 		$szDescription = 'Forwarder T&C version '.$dtDateVersion;
	 		require_once (__APP_PATH__ ."/management/createOldVersionsPdfs.php");
			$this->version = saveOldVersion();
			$iFlag='2';
	 	}
		$query="
				INSERT INTO
				 	".__DBC_SCHEMATA_VERSION__." 
				 SET
				 	szComment 		= '".mysql_escape_custom($this->szComment)."',
				 	szDescription 	= '".mysql_escape_custom($szDescription)."',
				 	szFirstName 	= '".mysql_escape_custom($this->szFirstName)."',
				 	szLastName 		= '".mysql_escape_custom($this->szLastName)."',
				 	szVersionURL	= '".mysql_escape_custom($this->version)."',
				 	dtVersion 		= '".mysql_escape_custom($dateVersionUpdate['dtVersionUpdated'])."',
				 	dtCreatedOn 	= NOW(),
				 	iFlag ='".(int)$iFlag."'
			";
			//echo $query;
			if($result = $this->exeSQL( $query ))
			{	
				if($mode=='FORWARDER')
	 			{	$kForwarder = new cForwarder();
	 				$email_template = __SEND_FORWARDER_TERMS_CONDITION_MAIL__;
	 				$replace_ary['szDate']			=	DATE('d. F Y',strtotime('+30 days'));
	 				$replace_ary['szReasonText']	=	$this->szComment;
	 				$subject 						= 	'Transporteca Terms & Conditions updated ';
	 				$kForwarderContact = new cForwarderContact;
	 				$idForwarderArr=$kForwarderContact->versionConfirmedForwarder();
	 				$versionName = DATE('d F Y')." - Terms & Conditions";
	 				if($idForwarderArr!=array())
	 				{	ini_set (max_execution_time,360000);
	 					foreach($idForwarderArr as $idForwarder)
	 					{	
	 						$tncMode  = '2';
	 						//$forwarderContact=$kForwarderContact->getMainAdmin($idForwarder['id']);
	 						$forwarderContacter=$kForwarderContact->getAllForwarderAdmins($idForwarder['id']);
	 						if($forwarderContacter != array() || $forwarderContacter!='')
	 						{	
		 						foreach($forwarderContacter as $forwarderContacts)
		 						{
			 						if($forwarderContacts['szEmail']!='')
			 						{
				 						$replace_ary['szFirstName']=($forwarderContacts['szFirstName']?$forwarderContacts['szFirstName']:'User');
				 						$replace_ary['szLastName']=$forwarderContacts['szLastName'];
				 						$replace_ary['szLink']="<a href='http://".$idForwarder['szControlPanelUrl']."/T&C/'>here</a>";
				 						$to =$forwarderContacts['szEmail'];
				 						//$to = 'morten.laerkholm@transporteca.com';
				 						createEmail($email_template, $replace_ary, $to, $subject, $reply_to,$forwarderContacts['id'],__STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__);
			 						}
			 						
		 						}
	 						}
	 						$kForwarder->updateForwarderAgreement($idForwarder['id'],$tncMode,$versionName,1,1);
	 					}
	 				}
	 				return true;
	 			}
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function ForwarderGuideVersion()
	{	
		$query="
				INSERT INTO
				 	".__DBC_SCHEMATA_VERSION__." 
				 SET
				 	szDescription 	= '".mysql_escape_custom('Forwarder\'s Guide to Transporteca')."',
				 	szFirstName 	= '".mysql_escape_custom($this->szFirstName)."',
				 	szLastName 		= '".mysql_escape_custom($this->szLastName)."',
				 	dtCreatedOn 	= NOW()
			";
			//echo $query;
			if($result = $this->exeSQL( $query ))
			{	
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function findForwarderGuideVersion()
	{	
		$query="
				SELECT 
				 	szFirstName, 	
				 	szLastName,	
				 	dtCreatedOn 
				 FROM
				 	".__DBC_SCHEMATA_VERSION__." 
				 WHERE	
				 	szDescription = 'Forwarder\'s Guide to Transporteca'
				 ORDER BY 
				 	dtCreatedOn DESC
				 LIMIT 0,1		
			";
		
			//echo $query;
			if($result = $this->exeSQL( $query ))
			{	
				if ($this->getRowCnt() > 0)
				{
					return $row=$this->getAssoc($result);
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function searchLastVersion($table)
	{
		if($table!='')
		{
			$query="
				SELECT
					dtVersionUpdated
				FROM
					".mysql_escape_custom($table)."
				WHERE 
				id=1	
			";
			//echo $query;
			if($result = $this->exeSQL( $query ))
			{
				if ($this->getRowCnt() > 0)
				{
					return $row=$this->getAssoc($result);
				}
				else
				{
					return NULL;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}	
	}
	function historyViewTNC()
	{
		$query="
			SELECT
				id,
				szComment,
				szDescription,
				szVersionURL,
				szFirstName,
				szLastName,
				dtCreatedOn
			FROM
				".__DBC_SCHEMATA_VERSION__."
			ORDER BY
				dtCreatedOn DESC	
		";
		//echo $query;
		if($result = $this->exeSQL( $query ))
		{
			if ($this->getRowCnt() > 0)
			{	
				$historyLog = array();
				while($row=$this->getAssoc($result))
				$historyLog[] = $row;
				return $historyLog;
			}
			else
			{
				return NULL;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	
	}
	function historyDetails($id)
	{
		$id 	= (int)sanitize_all_html_input($id); 
		$query 	="
			SELECT
				szVersionURL,
				szComment
			FROM
				".__DBC_SCHEMATA_VERSION__."
			WHERE 
				id=".mysql_escape_custom($id)."	
		";
		//echo $query;
		if($result = $this->exeSQL( $query ))
		{
			if ($this->getRowCnt() > 0)
			{	
				$row=$this->getAssoc($result);
				return $row;
			}
			else
			{
				return NULL;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	
	}
	
	
	function sendEmail($data,$idUser)
	{
		$this->set_szSubject($this->sanitizeData($data['szSubject']));
		$this->set_szMessage($this->sanitizeData($data['szMessage']));
		
		
		if ($this->error === true)
		{
			return false;
		}
		
		$replace_ary['szSubject']=$this->szSubject;
		$replace_ary['szbody']=nl2br($this->szMessage);
		$replace_ary['szEmail']=$data['szEmail'];
		
		createEmail(__SEND_EMAIL_USER__, $replace_ary,$data['szEmail'],'' , __STORE_SUPPORT_EMAIL__,$idUser, __STORE_SUPPORT_EMAIL__,__FLAG_FOR_CUSTOMER__);
	}
	
	function getForwarderAccessHistory($sql,$forwarderContactDetailAry=array())
	{
		if($sql!='')
		{
                    /*
                     * We have discontinued using RSS feeds from 17th March 2017 - @Ajay
			$query="
				SELECT
					r.szTitle,
					r.szDescription,
					r.dtCreatedOn,
					f.szDisplayName
				FROM
					".__DBC_SCHEMATA_RSS_FEED__." AS r
				INNER JOIN
					".__DBC_SCHEMATA_FROWARDER__." As f
				ON
					r.idForwarder=f.id
				WHERE
					r.isDeleted='0'
				AND
					".$sql."
				ORDER BY 
					r.dtCreatedOn DESC	
			";
			//echo $query;
			if($result = $this->exeSQL( $query ))
			{
				if ($this->getRowCnt() > 0)
				{
					$ctr=0;
					while($row=$this->getAssoc($result))
					{
						$forwarderAccessHisArr[$ctr]=$row;
						$forwarderAccessHisArr[$ctr]['dtCreatedTime'] = strtotime($row['dtCreatedOn']);
						$ctr++;
					}
					
					if(!empty($forwarderContactDetailAry))
					{
						$szForwarderEmail = $forwarderContactDetailAry['szEmail'];
						$iEmailMode = __FLAG_FOR_FORWARDER__ ;
						//fetching email logs
						$forwarderEmailLogs = $this->getEmailLogByForwarderEmail($szForwarderEmail,$iEmailMode);
						
						if(!empty($forwarderEmailLogs))
						{
							$final_ary = array_merge($forwarderAccessHisArr,$forwarderEmailLogs);
						}
						
						//sort array 
						
						if(!empty($final_ary))
						{
							$final_ary = sortArray($final_ary,'dtCreatedTime','DESC');
						}
						return $final_ary ;
					}
					return $forwarderAccessHisArr;
				}
				else
				{
					return array();
				}
			}
			else
			{
				return array();
			}
                     * 
                     */
		}
	}
	function getEmailLogByForwarderEmail($szEmail,$iMode)
	{
		if(!empty($szEmail))
		{
			$query="
				SELECT
					szEmailSubject as szTitle,
					 dtSent as dtCreatedOn
				FROM
					".__DBC_SCHEMATA_EMAIL_LOG__."
				WHERE
					szToAddress = '".mysql_escape_custom(trim($szEmail))."'
				AND
					iMode = '".(int)$iMode."'
			";
			//echo "<br /> ".$query."<br /> ";
			if($result = $this->exeSQL( $query ))
			{
				if ($this->getRowCnt() > 0)
				{
					$ctr = 0;
					$forwarderEmailLogs = array();
					while($row=$this->getAssoc($result))
					{
						$forwarderEmailLogs[$ctr]=$row;
						$forwarderEmailLogs[$ctr]['dtCreatedTime'] = strtotime($row['dtCreatedOn']);
						$ctr++;
					}
					return $forwarderEmailLogs;
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function getAllApiLogs()
        {  
            $query="
                SELECT
                    id,
                    szBaseUrl,
                    szLastStatus,
                    szLastResponse,
                    szDeveloperNotes,
                    iErrorCount,
                    szFrequency,
                    szPriority,
                    dtCreatedOn,
                    dtLastRun
                FROM
                    ".__DBC_SCHEMATA_TRANSPORTECA_API_LOGS__."
                WHERE
                    iActive ='1'
            ";
            //echo $query;
            if($result = $this->exeSQL( $query ))
            {
                if ($this->getRowCnt() > 0)
                {
                    $retAry = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {
                        $retAry[$ctr] = $row;
                        $ctr++;
                    }
                    return $retAry;
                }
                else
                { 
                    return array();
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
	function selectCrownJobs($id)
	{
		
		$query="
			SELECT
				szDescription
			FROM
				".__DBC_SCHEMATA_CROWN_JOB_LOG__."
			WHERE
				id ='".mysql_escape_custom($id)."'
		";
		//echo $query;
		if($result = $this->exeSQL( $query ))
		{
			if ($this->getRowCnt() > 0)
			{
				return $row=$this->getAssoc($result);
			}
			else
			{
				$this->insertCrownJobs();
				return array();
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function updateCrownJobs($crownJobs,$id)
	{	
		$crownJobs	=	trim(sanitize_all_html_input($crownJobs));
		$query="
			UPDATE
				".__DBC_SCHEMATA_CROWN_JOB_LOG__."
			SET
				szDescription 	= '".mysql_escape_custom($crownJobs)."',
				dtUpdatedOn		= NOW()
			WHERE
				id ='".mysql_escape_custom($id)."'
		";
		//echo $query;
		if($result = $this->exeSQL( $query ))
		{
			return true;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function insertCrownJobs()
	{	
		
		$query="
			INSERT
				INTO
				".__DBC_SCHEMATA_CROWN_JOB_LOG__."
			SET
				szDescription 	= '',
				dtCreatedOn		= NOW(),
				id =1
		";
		//echo $query;
		if($result = $this->exeSQL( $query ))
		{
			return true;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	function getAllActiveCustomer($idCountries,$iNewsUpdateStr,$iActiveFlagStr,$flag='',$data=array())
	{
			if($flag=='sendMail')
			{
				$this->set_szSubject($this->sanitizeData($data['szSubject']));
				$this->set_szMessage(sanitize_specific_tinymce_html_input($data['szMessage']));
				
				if($idCountries=='')
				{
					$this->addError( "idCountry" , "Atleast one country should be selected." );
				}				
				/*if ($this->error === true)
				{
					return array();
				}*/
			}
			$sql="";
			//echo $idCountries."hello";
			if($idCountries!='')
			{
				if($idCountries!='All')
				{
					$sql .="
					AND
						idCountry IN (".$idCountries.")
					";
				}
			
				if($iNewsUpdateStr!='')
				{
					$sql .="
					AND
						iAcceptNewsUpdate IN (".$iNewsUpdateStr.")
					";
				}
					
				if($iActiveFlagStr!='')
				{
					$sql .="
					AND
						iActive IN (".$iActiveFlagStr.")
					";
				}
				
				
				$query="
					SELECT
						id,
						count(id) AS totalCount
					FROM
						".__DBC_SCHEMATA_USERS__."
					WHERE
						iConfirmed='1'
					".$sql."
					GROUP BY
						szEmail						
				";
				//echo $query;
				if($result = $this->exeSQL( $query ))
				{
					if ($this->getRowCnt() > 0)
					{
						while($row=$this->getAssoc($result))
						{
							$userIdArr[]=$row['id']."_".$row['totalCount'];
						}
						return $userIdArr;
					}
					else
					{
						return array();
					}
				}
				else
				{
					return array();
				}
			}
			else
			{
				return array();
			}
	}
	
	
	function getAllForwarderUserProfile($idForwarderArr,$idForwarderProfileArr,$iForwarderStatusFlag,$iForwarderUserStatusFlag,$flag='',$data=array())
	{
			
			if($flag=='sendMail')
			{
				$this->set_szSubject($this->sanitizeData($data['szSubject']));
				$this->set_szMessage(sanitize_specific_tinymce_html_input($data['szMessage']));
				
				if($idForwarderArr=='')
				{
					$this->addError( "idForwarder" , "Atleast one forwarder should be selected." );
				}				
				/*if ($this->error === true)
				{
					return array();
				}*/
			}
			$sql="";
			if($idForwarderArr!='')
			{
				if($idForwarderArr!='All')
				{
					$sql .="
						AND
							fc.idForwarder IN (".$idForwarderArr.")
					";
				}
				if($idForwarderProfileArr!='All' && $idForwarderProfileArr!='')
				{
					$sql .="
					AND
						fc.idForwarderContactRole IN (".$idForwarderProfileArr.")
					";
				}
				
				
				
				
				if($iForwarderUserStatusFlag!='')
				{
					
					$sql .="
					AND
						fc.iActive IN (".$iForwarderUserStatusFlag.")
					";
				}
								
				if($iForwarderStatusFlag!='')
				{
					$sql .="
					AND
						f.iActive IN (".$iForwarderStatusFlag.")
					";
				}
				
				
				
				$query="
					SELECT
						fc.id,
						count(fc.id) as totalCount
					FROM
						".__DBC_SCHEMATA_FORWARDERS_CONTACT__." AS fc
					INNER JOIN
						".__DBC_SCHEMATA_FROWARDER__." As f
					ON
						f.id=fc.idForwarder
					WHERE
						fc.iConfirmed='1'	
					".$sql."
					GROUP BY
						szEmail						
				";
				//echo $query;
				if($result = $this->exeSQL( $query ))
				{
					if ($this->getRowCnt() > 0)
					{
						while($row=$this->getAssoc($result))
						{
							$userIdArr[]=$row['id']."_".$row['totalCount'];
						}
						return $userIdArr;
					}
					else
					{
						return array();
					}
				}
				else
				{
					return array();
				}
			}
	}
	
	function getAllMessageSend($idMessage=0,$from=1)
	{
		$pageLimit = (int)__CONTENT_PER_PAGE_MESSAGES_REVIEWS__; 
		$sql='';
		if((int)$idMessage>0)
		{
			$sql="
				WHERE
					id='".(int)$idMessage."'
			";
		}
		$query="
			SELECT
				id,
				idUser,
    			szBody,
    			szSubject,
    			szEmail,
    			dtSend,
    			iUserType
			FROM
				".__DBC_SCHEMATA_MESSAGES_LOG__."
			$sql		
			ORDER BY
				dtSend DESC
			LIMIT ".(int)($from-1)*$pageLimit.",".(int)$pageLimit."	
		";
		//echo $query;
		if($result = $this->exeSQL( $query ))
		{
			if ($this->getRowCnt() > 0)
			{
				while($row=$this->getAssoc($result))
				{
					$sendMessageArr[]=$row;
				}
				
				return $sendMessageArr;
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function resendMessages($data)
	{
		$this->set_szNewEmail($this->sanitizeData($data['szEmail']));
		
		if ($this->error === true)
		{
			return array();
		}
		
		$kWHSSearch = new cWHSSearch();
		$fromEmail=$kWHSSearch->getManageMentVariableByDescription('__SEND_UPDATES_CUSTOMER_EMAIL__');
		
		sendBulkEmail($this->szNewEmail,$fromEmail,$data['szSubject'],$data['szBody'],$fromEmail,$data['idUser'],$data['iUserType']);
	}
	function templateEmail($id='',$iLanguage=false,$showFlag=0)
	{
                $queryWhere='';
                
                if($showFlag>0)
                {
                    if($showFlag==1)
                    {
                        $queryWhere ="WHERE iShowInForwarderOnly='0'";
                    }else if($showFlag==2)
                    {
                        $queryWhere ="WHERE iShowInForwarderOnly='1'";
                    }
                }
		$query ="
			SELECT 
				cm.id,
				";
		if($id=='')
		{
			$query .="
				cm.section_title,
				cm.szFriendlyName
				";
		} 
		if($id!='')
		{
			$query .="
                                cmm.subject,
                                cmm.section_description
                        ";
			$innerJoinQuery="
                            INNER JOIN
                                ".__DBC_SCHEMATA_CMS_MAPPING__." AS cmm
                            ON        
                                cmm.idEmailTemplate=cm.id    
                            ";
		}
		$query .="
			FROM 
                            ".__DBC_SCHEMATA_CMS_EMAIL__." AS cm
                        $innerJoinQuery        
                        ";
		if($id!='')
		{
                    $query .="
                        WHERE
                            cm.id=".mysql_escape_custom($id)."
                        ";
		} 
                if((int)$iLanguage>0)
                {
                    $query .="
                        AND
                            cmm.idLanguage=".mysql_escape_custom($iLanguage)."
                        ";
                }
                $query .=
                        "
                        $queryWhere
                        ORDER BY 
                            cm.iSequence 
                        ASC
                        ";
		
		if($result = $this->exeSQL( $query ))
		{
			if ($this->getRowCnt() > 0)
			{	
				$textEditor=array();
				while($row=$this->getAssoc($result))
				{	
						$textEditor[]=$row;
				}
				return $textEditor;
			}
			else
			{
				return array();
			}	
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}	
	}
	function saveTemplateEmail($EmailTemplateArr)
	{ 
            if($EmailTemplateArr==array())
            {
                return false;
            }
            $EmailTemplateArr['szDescription'] = htmlspecialchars_decode(urldecode(base64_decode($EmailTemplateArr['szDescription'])));

            $this->set_szTemplateSubject($EmailTemplateArr['szSubject']);
            $this->szTemplateSubject=preg_replace_callback('@<script[^>]*?>.*?</script>@si','',$this->szTemplateSubject);
            $this->set_szMessage(trim($EmailTemplateArr['szDescription']));
            $this->szMessage=preg_replace_callback('@<script[^>]*?>.*?</script>@si','',$this->szMessage); 
            $this->set_iManageid(sanitize_all_html_input($EmailTemplateArr['id']));
            $this->set_iLanguage(sanitize_all_html_input($EmailTemplateArr['iLanguage']));

            if($this->error===true)
            {
                return false;
            } 
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_CMS_MAPPING__."
                SET
                    subject='".mysql_escape_custom(utf8_encode($this->szTemplateSubject))."',
                    section_description='".mysql_escape_custom(utf8_encode($this->szMessage))."', 
                    updated = now()
                WHERE
                    idEmailTemplate= ".mysql_escape_custom($this->iId)."
                AND
                    idLanguage='".(int)$this->iLanguage."'
            "; 
//            echo $query;
//            die;
            if($result = $this->exeSQL( $query ))
            {
                return true;		
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
	}
	function findForwarderAdminsByBookingId($idForwarder)
	{
		$idForwarder = (int)sanitize_all_html_input($idForwarder);
		if($idForwarder>0)
		{
			$query="
				SELECT 
					f.id,
					f.szDisplayName,
					f.szControlPanelUrl, 
					fwd.szFirstName,
					fwd.szEmail
				FROM
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__." AS fwd
				INNER JOIN 
					".__DBC_SCHEMATA_FORWARDERS__."	 AS f
				ON(f.id=fwd.idForwarder)
				WHERE 
					fwd.idForwarder = ".$idForwarder."	
				AND 
					fwd.idForwarderContactRole = ".__ADMINISTRATOR_PROFILE_ID__."
				AND 
					fwd.iActive = 1						
			";
			//echo $query;die;
			if($result = $this->exeSQL( $query ))
			{
				if ($this->getRowCnt() > 0)
				{	
					$textEditor=array();
					while($row=$this->getAssoc($result))
					{	
							$textEditor[]=$row;
					}
					return $textEditor;
				}
				else
				{
					return array();
				}		
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}	
			}
	}
	function findForwarderId($id)
	{
		if($id > 0)
		{
		$query="
			SELECT 
				idForwarder
			FROM	
				".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
			WHERE
				id='".(int)$id."'
			";
			//echo $query;die();
			if($result = $this->exeSQL( $query ))
			{
				$row=$this->getAssoc($result);
				return $row['idForwarder'];
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function findCustomerEmail($id)
	{
		if((int)$id>0)
		{
			$query="
				SELECT
					szEmail
				FROM
					".__DBC_SCHEMATA_USERS__."
				WHERE
					id = '".(int)$id."'
			";
			//echo $query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{
	        	// Check if the user exists
	            if( $this->getRowCnt() > 0 )
	            {
	            		$row=$this->getAssoc($result);
	            		return $row['szEmail'];
	            }
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
            		
	}
	function addRssFeed($arr)
	{
            /*
             * We have discontinued using RSS feeds from 17th March 2017 - @Ajay
		if($arr!=array())
		{	
			$this->set_id(sanitize_all_html_input($arr['id']));
			$this->set_szHeading(sanitize_all_html_input($arr['szHeading']));
			$this->set_szDescription(sanitize_specific_tinymce_html_input($arr['szContent']));
			$this->set_szUrl(sanitize_all_html_input($arr['szUrl']));
			if($this->error === true)
			{
				return false;
			}
			$query = "
				INSERT 
					INTO 
					".__DBC_SCHEMATA_RSS_FEED__."
					(
						`id`,
						`idForwarder`, 
						`idForwarderContact`, 
						`idAdmin`, 
						`szTitle`, 
						`szDescription`, 
						`szDescriptionHTML`, 
						`szLink`, 
						`dtCreatedOn`, 
						`szFeedType`, 
						`szGUID`
					) 
					VALUES 
					(
						NULL,
						 '', 
						 '', 
						 '".$this->id."', 
						 '".mysql_escape_custom(trim($this->szHeading))."', 
						 '".mysql_escape_custom(trim($this->szDescription))."', 
						 '".mysql_escape_custom(trim($this->szDescription))."', 
						 '".mysql_escape_custom(trim($this->szUrl))."', 
						 NOW(), 
						 '__ADMIN_UPLOADED_MANUALLY__', 
						 '".MD5(time())."'
					)
				";
			//echo $query."<br>";
			//die;
			if( ( $result = $this->exeSQL( $query ) ) )
			{
	         	return true; 
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
			
		}
		else
		{
			return false;
		}
             * 
             */
	
	}
	
	function showForwarderPendingPaymentServiceUpload($idForwarder=0,$idForwarderCurrency=0)
	{
		$sql='';
		if((int)$idForwarder>0 && (int)$idForwarderCurrency>0)
		{
                    $sql="
                        AND
                            f.id='".(int)$idForwarder."'
                        AND
                            ft.idForwarderCurrency='".(int)$idForwarderCurrency."' 
                    ";
		}		
		$query="
			SELECT 
				f.id,
				ft.szForwarderCurrency,
				ft.idForwarderCurrency,
				c.szCurrency,
				SUM(ft.fTotalPriceForwarderCurrency) AS fForwarderTotalPrice
			FROM 
                            ". __DBC_SCHEMATA_FORWARDERS__ ." AS f	
			LEFT JOIN 	
                            ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS ft
			ON
                            (ft.idForwarder=f.id and iBatchNo='0' and (ft.iStatus='1' OR ft.iStatus='0') AND ft.iTransferConfirmed=0 and ft.iDebitCredit='5')
			LEFT JOIN 
                            ".__DBC_SCHEMATA_CURRENCY__." AS c
			ON 
                            (f.szCurrency=c.id)
			WHERE
                            f.szDisplayName!='' 
                        AND 
                            ft.iForwarderGPType != '".__FORWARDER_PROFIT_TYPE_MARK_UP__."'
				".$sql."	
			GROUP BY 
				f.id,ft.szForwarderCurrency	
			ORDER BY 
				f.szDisplayName
			";
			//ECHO $query;
			if($result = $this->exeSQL( $query ))
			{
				if ($this->getRowCnt() > 0)
				{	
					$manageVar=array();
					while($row=$this->getAssoc($result))
					{	
						$manageVar[$row['id']][$row['szForwarderCurrency']]=$row['fForwarderTotalPrice'];
					}
					return $manageVar;
				}
				else
				{
					return array();
				}	
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	
	function updateForarderUplaodServicePayment($data)
	{
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                SET
                    iStatus='2',
                    iBatchNo='".mysql_escape_custom($data['invoiceNumber'])."',
                    fExchangeRate='".mysql_escape_custom($data['fExchangeRate'])."'   
                WHERE
                    iDebitCredit='5'
                AND
                    idForwarder='".(int)$data['idForwarder']."'
                AND
                    (iStatus='1' OR (iTransferConfirmed = '0' AND iStatus='0'))
                AND
                    idForwarderCurrency='".(int)$data['idForwarderCurrency']."' 			
            ";
            $result = $this->exeSQL( $query );
	}
	function updateTblGraphDetails()
	{
		$query="
			INSERT INTO 
				".__DBC_SCHEMATA_GRAPH_USER__."
				(
                                    iTotalUsers,
                                    iFlag,
                                    dtCreatedOn
				)
                                SELECT 
                                    COUNT(id),
                                    1,
                                    DATE_FORMAT(LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)),'%Y-%m-%d 00:00:00')
                                FROM 
                                    ".__DBC_SCHEMATA_USERS__."
                                WHERE
                                    date_format(`dtCreateOn`, '%Y-%m')=date_format(now() - INTERVAL 1 MONTH, '%Y-%m')
                    ";
		//echo $query;die;
		if($result=$this->exeSQL($query))
		{				
			$query="			
				INSERT INTO 
					".__DBC_SCHEMATA_GRAPH_USER__."
				(
					iTotalUsers,
					iFlag,
					dtCreatedOn
				)
					SELECT 
						COUNT(id),
						2,
						DATE_FORMAT(LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)),'%Y-%m-%d 00:00:00')
					FROM 
						".__DBC_SCHEMATA_USERS__."
					WHERE
						date_format(`dtCreateOn`, '%Y-%m')<=date_format(now() - INTERVAL 1 MONTH, '%Y-%m')
					AND
						iActive = '1'
					AND
						iConfirmed = '1'
					";
			if($result=$this->exeSQL($query))
			{
				$query="		
					INSERT INTO 
					".__DBC_SCHEMATA_GRAPH_USER__."
					(
						iTotalUsers,
						iFlag,
						dtCreatedOn
					)					
						SELECT 
							COUNT(id),
							3,
							DATE_FORMAT(LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)),'%Y-%m-%d 00:00:00')
						FROM 
							".__DBC_SCHEMATA_FORWARDERS__."
						WHERE
							date_format(`dtCreateOn`, '%Y-%m')<=date_format(now() - INTERVAL 1 MONTH, '%Y-%m')
						AND
							iActive = '1'
						AND
							isOnline = '1'
						";
				if($result=$this->exeSQL($query))
				{			
					$query="				
						INSERT INTO 
						".__DBC_SCHEMATA_GRAPH_USER__."
						(
							iTotalUsers,
							iFlag,
							dtCreatedOn
						)
							SELECT 
								COUNT(fc.id),
								4,
								DATE_FORMAT(LAST_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)),'%Y-%m-%d 00:00:00')
							FROM 
								".__DBC_SCHEMATA_FORWARDERS_CONTACT__." AS fc
							INNER JOIN 
								".__DBC_SCHEMATA_FORWARDERS__."	as f
							ON
								(f.id = fc.idForwarder)			
							WHERE
								date_format(fc.`dtCreateOn`, '%Y-%m')<=date_format(now() - INTERVAL 1 MONTH, '%Y-%m')
							AND
								fc.iActive = '1'
							AND
								fc.iConfirmed = '1'	
							AND
								f.iActive = '1'
							AND
								f.isOnline = '1'							
							";
					if($result=$this->exeSQL($query))
					{
						return true;
					}
					else
					{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					}
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function updateTblGraphDetailsManually()
	{
		$query="
			INSERT INTO 
				".__DBC_SCHEMATA_GRAPH_USER__."
				(
					iTotalUsers,
					iFlag,
					dtCreatedOn
				)
					SELECT 
						COUNT(id),
						1,
						dtCreateOn
					FROM 
						".__DBC_SCHEMATA_USERS__."
					WHERE
						date_format(`dtCreateOn`, '%Y-%m') < date_format(now(), '%Y-%m')
					GROUP BY	
						MONTH(dtCreateOn)	
				";
		if($result=$this->exeSQL($query))
		{				
			$query="			
				INSERT INTO 
				".__DBC_SCHEMATA_GRAPH_USER__."
				(
					iTotalUsers,
					iFlag,
					dtCreatedOn
				)
					SELECT 
						COUNT(id),
						2,
						dtCreateOn
					FROM 
						".__DBC_SCHEMATA_USERS__."
					WHERE
						date_format(`dtCreateOn`, '%Y-%m') < date_format(now(), '%Y-%m')
					AND
						iActive = '1'
					AND
						iConfirmed = '1'
					GROUP BY	
						MONTH(dtCreateOn)	
					";
			if($result=$this->exeSQL($query))
			{
				$query="		
					INSERT INTO 
					".__DBC_SCHEMATA_GRAPH_USER__."
					(
						iTotalUsers,
						iFlag,
						dtCreatedOn
					)					
						SELECT 
							COUNT(id),
							3,
							dtCreateOn
						FROM 
							".__DBC_SCHEMATA_FORWARDERS__."
						WHERE
							date_format(`dtCreateOn`, '%Y-%m') < date_format(now(), '%Y-%m')
						AND
							iActive = '1'
						AND
							isOnline = '1'
						GROUP BY	
							MONTH(dtCreateOn)	
						";
				
				if($result=$this->exeSQL($query))
				{			
					$query="				
						INSERT INTO 
						".__DBC_SCHEMATA_GRAPH_USER__."
						(
							iTotalUsers,
							iFlag,
							dtCreatedOn
						)
							SELECT 
								COUNT(fc.id),
								4,
								fc.dtCreateOn
							FROM 
								".__DBC_SCHEMATA_FORWARDERS_CONTACT__." as fc
							INNER JOIN 
								".__DBC_SCHEMATA_FORWARDERS__."	as f
							ON
								(f.id = fc.idForwarder)		
							WHERE
								date_format(fc.`dtCreateOn`, '%Y-%m') < date_format(now(), '%Y-%m')
							AND
								fc.iActive = '1'
							AND
								fc.iConfirmed = '1'	
							AND
								f.iActive = '1'
							AND
								f.isOnline = '1'	
							GROUP BY	
								MONTH(fc.dtCreateOn)						
							";
					if($result=$this->exeSQL($query))
					{
						return true;
					}
					else
					{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					}
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function checkLastFiveMonthsInUserGraph()
	{	
		 $monthsArr = array();
		 for( $i = 1; $i <= 5 ; $i++ )
		 {
		 	//$months[] = date("Y-m%", strtotime( date( 'Y-m-01' )." -$i months"));
		 	 $monthsArr[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
		 }
		 	return $monthsArr;
	}
	function selectTblGraphDetails($flag = null)
	{
		if($flag!=null)
		{
			$monthsArr = $this->checkLastFiveMonthsInUserGraph();
			//print_r($monthsArr);
			//die;
			$data = implode("','",$monthsArr);
			
			$query="
				SELECT
					IFNULL(iTotalUsers,0) AS iTotalUsers, 
					dtCreatedOn
				FROM	
					".__DBC_SCHEMATA_GRAPH_USER__."
				WHERE
					iFlag = ".$flag."
				AND
					DATE_FORMAT(dtCreatedOn,'%Y-%m') IN ('".$data."')	
				ORDER BY
					dtCreatedOn	 DESC
					";
			//echo $query."<br/><br/>";
			if($result=$this->exeSQL($query))
			{
				$graphDetails = array();
				foreach($monthsArr as $monthArs)
				{
					$key1 = strtoupper(date('M',strtotime($monthArs)));
					$graphDetails[$key1] = 0;
				}
				//print_R($graphDetails);die;
				while($row=$this->getAssoc($result))
				{
					$key = strtoupper(date('M',strtotime($row['dtCreatedOn'])));
					$graphDetails[$key] = $row['iTotalUsers'];
				}
				
				$graphDetails = array_reverse($graphDetails);
				if($flag == 1)
				{
					$graph = $this->countCustomerCurrentMonth();
					$key = strtoupper(date('M',strtotime(date('Y-m-d'))));
					$graphDetails[$key] = $graph['total'];
				}
				if($flag == 2)
				{
					$graph = $this->countCustomerRegisteredCurrentMonth();
					$key = strtoupper(date('M',strtotime(date('Y-m-d'))));
					$graphDetails[$key] = $graph['total'];
				}
				if($flag == 3)
				{
					$graph = $this->countForwardersCurrentMonth();
					$key = strtoupper(date('M',strtotime(date('Y-m-d'))));
					$graphDetails[$key] = $graph['total'];
				}
				if($flag == 4)
				{
					$graph = $this->countForwarderProfileCurrentMonth();
					$key = strtoupper(date('M',strtotime(date('Y-m-d'))));
					$graphDetails[$key] = $graph['total'];
				}
				return $this->createImageUser($graphDetails,$flag);
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return 	array();
		}
	}
	function countCustomerCurrentMonth()
	{
		$query = "
			SELECT 
				COUNT(id) as total,
				dtCreateOn
			FROM 
				".__DBC_SCHEMATA_USERS__."
			WHERE
				date_format(`dtCreateOn`, '%Y-%m')=date_format(now(), '%Y-%m')
		";	
		if($result=$this->exeSQL($query))
		{
			$row=$this->getAssoc($result);
			return $row;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}	
	}
	function countCustomerRegisteredCurrentMonth()
	{
		$query = "
			SELECT 
				COUNT(id) as total,
				now() as dtCreateOn
			FROM 
				".__DBC_SCHEMATA_USERS__."
			WHERE
				date_format(`dtCreateOn`, '%Y-%m')<=date_format(now(), '%Y-%m')
			AND
				iActive = '1'
			AND
				iConfirmed = '1'
		";	
		if($result=$this->exeSQL($query))
		{
			$row=$this->getAssoc($result);
			return $row;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}	
	}
	function countForwardersCurrentMonth()
	{
		$query = "
			SELECT 
				COUNT(id) as total,
				now() as dtCreateOn
			FROM 
				".__DBC_SCHEMATA_FORWARDERS__."
			WHERE
				date_format(`dtCreateOn`, '%Y-%m')<=date_format(now(), '%Y-%m')
			AND
				iActive = '1'
			AND
				isOnline = '1'
		";	
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			$row=$this->getAssoc($result);
			return $row;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}	
	}
	function countForwarderProfileCurrentMonth()
	{
		$query = "
			SELECT 
				COUNT(fc.id) as total,
				now() as dtCreateOn
			FROM	
				".__DBC_SCHEMATA_FORWARDERS_CONTACT__." as fc
			INNER JOIN 
				".__DBC_SCHEMATA_FORWARDERS__."	as f
			ON
				(f.id = fc.idForwarder)	
			WHERE
				date_format(fc.`dtCreateOn`, '%Y-%m')<=date_format(now(), '%Y-%m')
			AND
				fc.iActive = '1'
			AND
				fc.iConfirmed = '1'
			AND
				f.iActive = '1'
			AND
				f.isOnline = '1'
                        AND
                                fc.idForwarderContactRole NOT IN ('7')
		";
		if($result=$this->exeSQL($query))
		{
			$row=$this->getAssoc($result);
			return $row;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}	
	}
	function createImageUser($values,$id)
	{
		# ------- The graph values in the form of associative array
		
		$img_width=460;
		$img_height=260; 
		$margins=30;
		
		
		$imageName="images".$id.".png";
		$imageAppPath=__APP_PATH_ROOT__."/management/graph/".$imageName;
		//echo $imageAppPath."test";
		if (file_exists($imageAppPath)) {
			unlink($imageAppPath);
		}
	 
		# ---- Find the size of graph by substracting the size of borders
		//$graph_width=$img_width - $margins * 2;
		$graph_width = $img_width ;
		$graph_height=$img_height - $margins * 2; 
		$img=imagecreate($img_width,$img_height);
	
	 	
		$bar_width=45;
		$total_bars=count($values);
		$gap= ($graph_width- $total_bars * $bar_width ) / ($total_bars +1);
		$gap = 20;
		//echo $gap ;
	 
		# -------  Define Colors ----------------
		$bar_color=imagecolorallocatealpha($img,179,179,152,5);
		$border_color=imagecolorallocate($img,255,255,255);
		$base_line= imagecolorallocatealpha($img,125,116,104,5);
		$base_line_string = imagecolorallocatealpha($img,0,0,0,5);
		# ------ Create the border around the graph ------
		imagesetthickness($img, 2);
		imagefilledrectangle($img,0,0,$img_width,$img_height,$border_color);
	
	 
		# ------- Max value is required to adjust the scale	-------
		$max_value=max($values) + 10;
		$ratio= $graph_height/$max_value;
		imageline ( $img ,20,15, 20 , 230, $base_line);
		imageline ( $img ,20,15, 25 , 25, $base_line);
		imageline ( $img ,20,15, 15 , 25, $base_line);
		imageline ( $img ,440,230, 20 , 230, $base_line);
		
		# ----------- Draw the bars here ------
		//$font = 'arial.ttf';
		$margins2= 20 ;
		$font_file_name = __APP_PATH__.'/fonts/font.ttf';
		$font_file_name2 = __APP_PATH__.'/fonts/Calibri.ttf';
		for($i=0;$i< $total_bars; $i++)
		{ 
			# ------ Extract key and value pair from the current pointer position
			list($key,$value)=each($values);
			
			$x1= $margins2 + $gap + $i * ($gap+$bar_width) ;
			$x2= $x1 + $bar_width; 
			$y1=$margins +$graph_height- intval($value * $ratio) ;
			$y2=$img_height-$margins;
			
			imagettftext($img, 10, 0, $x1+15, $y1-5, $base_line_string, $font_file_name, number_format($value));
			//imagestring($img,3,$x1+15,$y1-15,number_format($value),$bar_color);
			//imagestring($img,4,$x1+4,$img_height-20,$key,$base_line_string);		
			imagettftext($img, 12, 0, $x1+10, $img_height-10, $base_line_string, $font_file_name2, $key);
			imagefilledrectangle($img,$x1,$y1,$x2,$y2,$base_line);
		}
		
		imagepng($img,"graph/images".$id.".png");
		imagedestroy($img);
		
		return "graph/images".$id.".png";
	}
	function getAllForwarderNameList($id = null)
	{
		if((int)$id>0)
		{
                    $query_and= " AND id = '".(int)$id."' ";
		}
		$query="
			SELECT
                            id,
                            szDisplayName,
                            szForwarderAlias,
                            idCountry
			FROM
                            ".__DBC_SCHEMATA_FROWARDER__."
			WHERE
                            iActive = '1'
				$query_and
			AND 
                            (
                                szDisplayName!='' 
                            OR
                                szDisplayName != NULL
                            )
			ORDER BY
                            szDisplayName ASC
		";
//		echo $query ;
		if($result = $this->exeSQL( $query ))
		{
			if ($this->getRowCnt() > 0)
			{
				$szForwarderNameList = array();
				while($row = $this->getAssoc( $result))
				{
						$szForwarderNameList[] =$row;
				}
				return $szForwarderNameList;
			}
			else
			{
				return array();
			}	
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	/*function getAllWarehouseName($id)
	{
		$query="
			SELECT
				szCity,
				idCountry
			FROM
				".__DBC_SCHEMATA_WAREHOUSES__."
			WHERE
			 	id = '".(int)$id."'
		";
		if( ( $result = $this->exeSQL( $query ) ) )
		{
			$row = $this->getAssoc($result);
			$countryISO = $this->countriesView(1,$row['idCountry']);
			return returnLimitData($row['szCity'],6) .' ('.$countryISO['szCountryISO'].')';
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	*/
	function findForwarderNameUsingWarehouse($idWarehouse)
	{
		$query="
			SELECT
                            szDisplayName
			FROM
				".__DBC_SCHEMATA_FORWARDERS__." AS f
			INNER JOIN 	
				".__DBC_SCHEMATA_WAREHOUSES__." AS w
			ON
			  (w.idForwarder = f.id)
			 WHERE
                            w.id = ".(int)$idWarehouse." 
			";
		if( ( $result = $this->exeSQL( $query ) ) )
		{
			$row = $this->getAssoc($result);
			return $row['szDisplayName'];
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function findSOurceDestinationCountryForwarder($idForwarder,$iWarehouseType=false)
	{
            $add_query = '';
            if((int)$idForwarder>0)
            {
                $add_query = "
                    AND
                        w1.idForwarder  = ".(int)$idForwarder."	
                    AND
                        w2.idForwarder  = ".(int)$idForwarder."	
                "; 
            }
            if($iWarehouseType>0)
            {
                $add_query .= "   
                    AND w1.iWarehouseType = '".(int)$iWarehouseType."'
                AND
                    w2.iWarehouseType = '".(int)$iWarehouseType."'
                " ;
            }
            $query="
                SELECT
                    w1.idCountry as idOrignCountry,
                    w2.idCountry as idDestinationCountry
                FROM
                    ".__DBC_SCHEMATA_WAREHOUSES_PRICING__." AS wtw
                INNER JOIN 	
                    ".__DBC_SCHEMATA_WAREHOUSES__." AS w1
                ON
                  (w1.id = wtw.idWarehouseFrom)
                INNER JOIN 	
                    ".__DBC_SCHEMATA_WAREHOUSES__." AS w2
                ON
                    (w2.id = wtw.idWarehouseTo)
                WHERE 
                    w1.iActive = '1'
                AND
                    w2.iActive = '1'
                $add_query
            ";
            //echo  $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {	
                $idOriginDestination = array();
                while($row = $this->getAssoc($result))
                {
                    $idOriginDestination[]  = $row;
                }
                return $idOriginDestination;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
        
	function findForwarderDestinationCountryId($idSourceCountry,$iWarehouseType=false)
	{
		$add_query = '';
		if((int)$idSourceCountry>0)
		{
                    $add_query = " AND w1.idCountry = '".(int)$idSourceCountry."' ";
		}
                if((int)$iWarehouseType>0)
		{
                    $add_query .= "
                        AND 
                            w1.iWarehouseType = '".(int)$iWarehouseType."'
                        AND
                            w2.iWarehouseType = '".(int)$iWarehouseType."'
                    ";
		}
                
		$query="
                    SELECT
                        w2.idForwarder as idForwarder,
                        w2.idCountry as idDestinationCountry
                    FROM
                        ".__DBC_SCHEMATA_WAREHOUSES_PRICING__." AS wtw
                    INNER JOIN 	
                        ".__DBC_SCHEMATA_WAREHOUSES__." AS w1
                    ON
                        (w1.id = wtw.idWarehouseFrom)
                     INNER JOIN 	
                        ".__DBC_SCHEMATA_WAREHOUSES__." AS w2
                    ON
                      (w2.id = wtw.idWarehouseTo)
                    INNER JOIN
                        ".__DBC_SCHEMATA_COUNTRY__." AS C
                    ON
                        C.id=w2.idCountry
                    WHERE
                        w1.iActive = '1'
                    AND
                        w2.iActive = '1'
                        ".$add_query."
                    ORDER BY
                        C.szCountryName
                ";
		//echo  $query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{	
                    $idOriginDestination = array();
                    while($row = $this->getAssoc($result))
                    {
                        $idOriginDestination[]  = $row;
                    }
                    return $idOriginDestination;
		}
		else
		{
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
		}
	}
	function findForwarderOriginCountryId($idDestinationCountry,$iWarehouseType=false)
	{
            $add_query = '';
            if((int)$idDestinationCountry>0)
            {
                $add_query  = " AND w2.idCountry = ".(int)$idDestinationCountry." ";
            }
            if((int)$iWarehouseType>0)
            {
                $add_query .= "
                    AND 
                        w1.iWarehouseType = '".(int)$iWarehouseType."'
                    AND
                        w2.iWarehouseType = '".(int)$iWarehouseType."'
                ";
            }
            $query="
                SELECT
                    w2.idForwarder as idForwarder,
                    w1.idCountry as idOriginCountry
                FROM
                    ".__DBC_SCHEMATA_WAREHOUSES_PRICING__." AS wtw
                INNER JOIN 	
                    ".__DBC_SCHEMATA_WAREHOUSES__." AS w1
                ON
                  (w1.id = wtw.idWarehouseFrom)
                 INNER JOIN 	
                    ".__DBC_SCHEMATA_WAREHOUSES__." AS w2
                ON
                  (w2.id = wtw.idWarehouseTo)
                WHERE
                    w1.iActive = '1'
                AND
                    w2.iActive = '1'
                    ".$add_query."
            ";
            //echo  $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {	
                $idOriginDestination = array();
                while($row = $this->getAssoc($result))
                {
                    $idOriginDestination[]  = $row;
                }
                return $idOriginDestination;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
	function oboCCModelAdminForwarderList($flag,$idForwarder=0)
	{	
			if($flag == 0)
			{
				$addQuery = "INNER JOIN 
								".__DBC_SCHEMATA_PRICING_CC__." AS c
							ON
								(c.idWarehouseFrom = w.id ||  c.idWarehouseTo = c.id )
							";				
			}
			else
			{
				$addQuery = '';
			}
		if($idForwarder > 0)
			{
				$addSubQuery = "
				AND 
					w.idForwarder = '".$idForwarder."'
							";				
			}
			else
			{
				$addSubQuery = '';
			}
			
			$query="
				SELECT 
                                    w.idForwarder,
                                    w.id
				FROM 
                                    ".__DBC_SCHEMATA_WAREHOUSES__." AS w
				INNER JOIN 
                                    ".__DBC_SCHEMATA_FORWARDERS__."	AS fwd
				ON
                                    (fwd.id = w.idForwarder)	
                                    ".$addQuery."
				WHERE
                                    w.iActive = 1 
				AND
                                    fwd.iActive = 1	
					".$addSubQuery."
				GROUP BY 
					w.id	
				ORDER BY
					w.idForwarder,
					w.szWareHouseName
						
				";
			//echo $query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{	$idForwarder = array();
				while($row = $this->getAssoc($result))
				{
					$idForwarder[]  = $row;
				}
				return $idForwarder;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function oboCCModelAdminCount($oboCCModelAdmin=array(),$sortOrder = '',$order = '',$from = 0)
	{
		if((int)$oboCCModelAdmin!= array())
		{	
			$i = 0;
			
			$strAdd = '';
			if($oboCCModelAdmin != array())
			{
				$this->set_idOriginCountry((int)$oboCCModelAdmin['idOriginCountry']);
				$this->set_idDestinationCountry((int)$oboCCModelAdmin['idDestinationCountry']);
				$this->set_idForwarder((int)$oboCCModelAdmin['idForwarder']);
			
				
				if($this->idOriginCountry)
				{
					$strAdd .= "
							AND
								c.id = '".(int)$this->idOriginCountry."'	
						";
				}
				if($this->idDestinationCountry)
				{
					$strAdd .= "
							AND
								c2.id = '".(int)$this->idDestinationCountry."'	
						";
				}
				if($this->idForwarder)
				{
					$strAdd .= "
							AND
								fwd.id = '".(int)$this->idForwarder."'	
						";
				}
			}
			$orderBy = "
				ORDER BY
					fwd.szDisplayName 
				ASC
			";
			if((int)$sortOrder>0)
			{
				if((int)$sortOrder==1)
				{
					$orderBy = "
						ORDER BY
							c.szCountryISO ".mysql_escape_custom($order).",
							w.szCity ".mysql_escape_custom($order)."
								
					";
				}
				if((int)$sortOrder==2)
				{
					$orderBy = "
						ORDER BY
							c2.szCountryISO ".mysql_escape_custom($order).",
							w2.szCity ".mysql_escape_custom($order)."
								
					";
				}
			}
			$query="
				SELECT
					count(cc.id) as id
				FROM
					".__DBC_SCHEMATA_PRICING_CC__." as cc
				INNER JOIN
					".__DBC_SCHEMATA_PRICING_CC__." as cc2
				ON
					(cc.idWarehouseFrom = cc2.idWarehouseFrom &&  cc.idWarehouseTo = cc2.idWarehouseTo && cc2.szOriginDestination =2)	
				INNER JOIN
					".__DBC_SCHEMATA_WAREHOUSES__." AS w
				ON
					(w.id = cc2.idWarehouseFrom && w.iActive = 1)
				INNER JOIN
					".__DBC_SCHEMATA_WAREHOUSES__." AS w2
				ON
					(w2.id = cc2.idWarehouseTo && w2.iActive = 1)	
				INNER JOIN
					".__DBC_SCHEMATA_COUNTRY__." AS c
				ON
					(w.idCountry = c.id && c.iActive = 1)
				INNER JOIN
					".__DBC_SCHEMATA_COUNTRY__." AS c2
				ON
					(w2.idCountry = c2.id && c2.iActive = 1)	
				INNER JOIN
					".__DBC_SCHEMATA_FORWARDERS__."	AS fwd
				ON
					(fwd.id = w.idForwarder && fwd.iActive = 1)		
				WHERE
                                            cc.szOriginDestination = 1
					AND
                                            cc.id<>cc2.id  
					AND
					(	
						cc2.iActive = 1
						OR 
						cc.iActive = 1
					)	
					".$strAdd."	
				GROUP BY
					cc.id,cc2.id
					".$orderBy."
				";
				//echo $query."<br/>";
			if( ( $result1 = $this->exeSQL( $query ) ) )
			{
				return $this->iNumRows;
			}
		}	
	}
		
	function oboCCModelAdmin($oboCCModelAdmin=array(),$sortOrder = '',$order = '',$from = 1)
	{	
		$pageLimit = (int)__CONTENT_PER_PAGE_CUSTOM_CLEARENCE__; 
		if((int)$oboCCModelAdmin!= array())
		{	
			$i = 0;
			//$kConfig = new cConfig;
			//$Export_Import = new cExport_Import;
			$strAdd = '';
			if($oboCCModelAdmin != array())
			{
				$this->set_idOriginCountry((int)$oboCCModelAdmin['idOriginCountry']);
				$this->set_idDestinationCountry((int)$oboCCModelAdmin['idDestinationCountry']);
				$this->set_idForwarder((int)$oboCCModelAdmin['idForwarder']);
			
				
				if($this->idOriginCountry)
				{
					$strAdd .= "
							AND
								c.id = '".(int)$this->idOriginCountry."'	
						";
				}
				if($this->idDestinationCountry)
				{
					$strAdd .= "
							AND
								c2.id = '".(int)$this->idDestinationCountry."'	
						";
				}
				if($this->idForwarder)
				{
					$strAdd .= "
							AND
								fwd.id = '".(int)$this->idForwarder."'	
						";
				}
			}
			$orderBy = "
				ORDER BY
					fwd.szDisplayName 
				ASC
			";
			if((int)$sortOrder>0)
			{
				if((int)$sortOrder==1)
				{
					$orderBy = "
						ORDER BY
							c.szCountryISO ".mysql_escape_custom($order).",
							w.szCity ".mysql_escape_custom($order)."
								
					";
				}
				if((int)$sortOrder==2)
				{
					$orderBy = "
						ORDER BY
							c2.szCountryISO ".mysql_escape_custom($order).",
							w2.szCity ".mysql_escape_custom($order)."
								
					";
				}
			}
			$query="
				SELECT
					fwd.szDisplayName,
					cc.idWarehouseFrom as from_id,
					cc2.idWarehouseTo as to_id,
					w.szWareHouseName as wareHouseFrom,
					w.szCity as cityFrom,
					c.szCountryISO as countryFrom,
					w2.szWareHouseName as wareHouseTo,
					w2.szCity as cityTo,
					c2.szCountryISO as countryTo,
					cc.szCurrency AS export_currency,
					cc.fPrice AS export_value,
					cc.iActive as iActiveFrom, 
					cc2.szCurrency AS import_currency,
					cc2.fPrice AS import_value,
					cc2.iActive as iActiveTo
				FROM
					".__DBC_SCHEMATA_PRICING_CC__." as cc
				INNER JOIN
					".__DBC_SCHEMATA_PRICING_CC__." as cc2
				ON
					(cc.idWarehouseFrom = cc2.idWarehouseFrom &&  cc.idWarehouseTo = cc2.idWarehouseTo && cc2.szOriginDestination =2)	
				INNER JOIN
					".__DBC_SCHEMATA_WAREHOUSES__." AS w
				ON
					(w.id = cc2.idWarehouseFrom && w.iActive = 1)
				INNER JOIN
					".__DBC_SCHEMATA_WAREHOUSES__." AS w2
				ON
					(w2.id = cc2.idWarehouseTo && w2.iActive = 1)	
				INNER JOIN
					".__DBC_SCHEMATA_COUNTRY__." AS c
				ON
					(w.idCountry = c.id && c.iActive = 1)
				INNER JOIN
					".__DBC_SCHEMATA_COUNTRY__." AS c2
				ON
					(w2.idCountry = c2.id && c2.iActive = 1)	
				INNER JOIN
					".__DBC_SCHEMATA_FORWARDERS__."	AS fwd
				ON
					(fwd.id = w.idForwarder && fwd.iActive = 1)		
				WHERE
                                            cc.szOriginDestination = 1
					AND
                                            cc.id<>cc2.id 	 
					AND
					(	
						cc2.iActive = 1
						OR 
						cc.iActive = 1
					)	
					".$strAdd."	
				GROUP BY
					cc.id,cc2.id
					".$orderBy."
				LIMIT ".(int)($from-1)*$pageLimit.",".(int)$pageLimit."			
				";
				//echo $query."<br/>";
			if( ( $result1 = $this->exeSQL( $query ) ) )
			{
				$customClearArr = array();
				while($row1 = $this->getAssoc($result1))
				{	
					$customClearArr[]=$row1;
				}
					return $customClearArr;								
			}
		}		
	}
	function backUpoboCCModelAdmin($oboCCModelAdmin)
	{	
		if((int)$oboCCModelAdmin!= array())
		{	
			$i = 0;
			$kConfig = new cConfig;
			$Export_Import = new cExport_Import;
			
			$this->set_idOriginCountry((int)$oboCCModelAdmin['idOriginCountry']);
			$this->set_idDestinationCountry((int)$oboCCModelAdmin['idDestinationCountry']);
			$this->set_idForwarder((int)$oboCCModelAdmin['idForwarder']);
			
			if($this->idForwarder)
			{
				$idForwarder = $this->oboCCModelAdminForwarderList(0,$this->idForwarder);
			}
			else
			{
				$idForwarder = $this->oboCCModelAdminForwarderList(0);
			}
			$idWh = array();
			if($idForwarder != array() || $idForwarder !='')
			{	
				$idForwarderArrs = array();
				$count = 0;
				foreach($idForwarder as $idWare)
				{
					$orginarr[] = $idWare['id'];
					$desarr[]   = $idWare['id'];
				}
				
					if(!empty($orginarr))
					{
						$i=0;
						foreach($orginarr as $orginarrs)
						{	
							if(!empty($desarr))
							{	
								foreach($desarr as $desarrs)
								{	
									$customClearArr = array();
									if($desarrs != $orginarrs)
									{
									$addQuery = '';
									if($this->idOriginCountry)
									{
										$addQuery .="
											INNER JOIN 
												".__DBC_SCHEMATA_WAREHOUSES__." as w1
											ON
												(w1.id ='".(int)$orginarrs."' && w1.idCountry = '".$this->idOriginCountry."')
													
										";
									}
									if($this->idDestinationCountry)
									{
										$addQuery .="
											INNER JOIN 
												".__DBC_SCHEMATA_WAREHOUSES__." as w2
											ON	
											(w2.id ='".(int)$desarrs."' && w2.idCountry = '".$this->idDestinationCountry."')
													
										";
									}
									
										$query="
											SELECT
												cc.szOriginDestination,
												cc.szCurrency,
												cc.iActive,
												cc.fPrice,
												cc.szCurrency,
												cc.szOriginDestination
											FROM
												".__DBC_SCHEMATA_PRICING_CC__." as cc
												".$addQuery."
											WHERE
												cc.idWarehouseFrom='".(int)$orginarrs."'
											AND
												cc.idWarehouseTo='".(int)$desarrs."'
											AND
												cc.iActive='1'	
											";
											//echo $query."<br/>";
											if( ( $result1 = $this->exeSQL( $query ) ) )
											{
												if ($flag == 0)
												{
													if($this->iNumRows > 0)
													{
														$array_orign[$i]['from_id']=$orginarrs;
														$array_orign[$i]['to_id']=$desarrs;
													}		
												}
												else if ($flag == 1)
												{	
													$array_orign[$i]['from_id']=$orginarrs;
													$array_orign[$i]['to_id']=$desarrs;
												}
													
													///
												while($row1 = $this->getAssoc($result1))
												{	
													$customClearArr[]=$row1;
												}
												unset($row1);
												
												if(!empty($customClearArr))
												{	
													foreach($customClearArr as $customClearArrs)
													{	
														if($customClearArrs['szOriginDestination']=='1')
														{
															if($customClearArrs['fPrice']>0 && $customClearArrs['szCurrency']!='' && $customClearArrs['iActive'] == 1)
															{	
																$array_orign[$i]['export_value']=$customClearArrs['fPrice'];
																$array_orign[$i]['export_currency']=$Export_Import->getFreightCurrency($customClearArrs['szCurrency']);
															}
															else
															{	
																$array_orign[$i]['export_value']='';
																$array_orign[$i]['export_currency']='';
															}
														}
														else if($customClearArrs['szOriginDestination']=='2')
														{
															if($customClearArrs['fPrice']>0 && $customClearArrs['szCurrency']!=''  && $customClearArrs['iActive'] == 1)
															{
																$array_orign[$i]['import_value']=$customClearArrs['fPrice'];
																$array_orign[$i]['import_currency']=$Export_Import->getFreightCurrency($customClearArrs['szCurrency']);
															}
															else
															{	
																$array_orign[$i]['import_value']='';
																$array_orign[$i]['import_currency']='';
															}
														}
													}
												}
												++$i;
											}		
										}	
										unset($query,$result1);	
									}
								}
							}
						}
					}
				}
				return $array_orign;
	}
	function findEachCC($idWarehouse,$idForwarder,$originDestination,$flag = 1)
	{
		/**
		 *@param $flag  = 0 
		 * default means show all the relative content
		 * 
		 * $flag  = 1
		 * means show only those who have declared pricing 
		*/	
		if($flag == 0)
		{
			$join = "LEFT JOIN";
		
		}	
		else if($flag == 1)
		{
			$join = "INNER JOIN";
		}

		$query="
			SELECT 
				wf.id,
				wf.szWareHouseName,
				wf.szCity,
				wf.idCountry,
				cc.szCurrency,
				cc.fPrice
			FROM 
				".__DBC_SCHEMATA_WAREHOUSES__." as wf
			".$join."
				".__DBC_SCHEMATA_PRICING_CC__." as cc
			ON
				( cc.idWarehouseFrom = ".(int)$idWarehouse."  && cc.szOriginDestination = ".(int)$originDestination.")
			WHERE
				wf.id = ".(int)$idWarehouse."
			AND
				wf.idForwarder = ".(int)$idForwarder."	 
			";
		
		if( ( $result = $this->exeSQL( $query ) ) )
		{	
			$row = $this->getAssoc($result);
			{
				if($row != array())
				{
					$idForwarderArrs[]  = $row;
					//echo $query."<br />";
				}
			}
			return $idForwarderArrs;
		}
	}
	function showPostcodePriority($idAdmin,$sortBy='',$sortOrder='')
	{
		if((int)$idAdmin>0)
		{
			if(isset($sortBy) && $sortBy!='')
			{
				$sortBy = trim(sanitize_all_html_input($sortBy));
				$sortOrder = trim(sanitize_all_html_input($sortOrder));
				$sortByOrder = "
							ORDER BY
						".$sortBy."
						".$sortOrder."
					";
			}
			else
			{
				$sortByOrder = "
						ORDER BY
							c.szCountryName,
							p.szCity,
							p.szCity1,
							p.szCity2,
							p.szCity3,
							p.szCity4,
							p.szCity5,
							p.szCity6,
							p.szCity7,
							p.szCity8,
							p.szCity9,
							p.szCity10,
							p.szRegion1,
							p.szRegion2,
							p.szRegion3,
							p.szRegion4
					";	
			}
			$query="
					SELECT 
						p.id,
						p.szCity,
						p.szCity1,
						p.szCity2,
						p.szCity3,
						p.szCity4,
						p.szCity5,
						p.szCity6,
						p.szCity7,
						p.szCity8,
						p.szCity9,
						p.szCity10,
						p.szRegion1,
						p.szRegion2,
						p.szRegion3,
						p.szRegion4,
						c.szCountryName,
						p.iPriority						
					FROM 
						".__DBC_SCHEMATA_POSTCODE__." AS p
					INNER JOIN
						".__DBC_SCHEMATA_COUNTRY__." AS c	
					ON	(c.id = p.idCountry)	
					WHERE
						p.iActive = 1	
					AND
					(
							szCity != ''
						OR
							szCity1 != ''
						OR
							szCity2 != ''
						OR
							szCity3 != ''
						OR
							szCity4 != ''
						OR
							szCity5 != ''
						OR
							szCity6 != ''
						OR
							szCity7 != ''
						OR
							szCity8 != ''
						OR
							szCity9 != ''
						OR
							szCity10 != ''
						OR
							szRegion1 != ''	
						OR
							szRegion2 != ''
						OR
							szRegion3 != ''	
						OR
							szRegion4 != ''	
						)
					AND	
						iPriority >0	
						".$sortByOrder."
					";
				//echo $query;
				if( ( $result = $this->exeSQL( $query ) ) )
				{	$priorityArr = array();
					while($row = $this->getAssoc($result))
					{
						$priorityArr[]  = $row;
					}
					return $priorityArr;
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
		}
	}
	function showCCWarehouse($originDestination = 0)
	{
		if($originDestination>0)
		{
			$query="
					SELECT
						cc.id,
						cc.idWarehouseFrom,
						cc.idWarehouseTo,
						cc.szOriginDestination,
						cc.fPrice,
						cc.szCurrency
					FROM
						".__DBC_SCHEMATA_PRICING_CC__." cc
					WHERE
						szOriginDestination = ".$originDestination."
					AND
						iActive = 1	
					";
			//echo $query;	
			if($result=$this->exeSQL($query))
			{
				$ccDataAry = array();
				if($this->iNumRows>0)
				{
					while($row=$this->getAssoc($result))
					{
						$ccDataAry[] = $row;
					}
				}
					return $ccDataAry ;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	
	}
	function showCCWarehouseByID($importId,$exportId,$originDestination)
	{
		if($originDestination>0)
		{
			$query="
					SELECT
						cc.id,
						idWarehouseFrom,
						idWarehouseTo,
						cc.szOriginDestination,
						cc.fPrice,
						cc.szCurrency,
						cc.iActive
					FROM
						".__DBC_SCHEMATA_PRICING_CC__." cc
					WHERE
						idWarehouseFrom = ".(int)$importId."
					AND	
						idWarehouseTo = ".(int)$exportId."
					AND	
						szOriginDestination = ".(int)$originDestination."
					";
			//echo $query;	
			if($result=$this->exeSQL($query))
			{
				$ccDataAry = array();
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					return $row ;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	
	}
	function updatePriority($idAdmin,$id)
	{
		if((int)$idAdmin>0 && (int)$id>0)
		{
			$query="
				UPDATE
					".__DBC_SCHEMATA_POSTCODE__."
				SET
					iPriority  =0
				WHERE
					id = ".(int)$id."	
					";
			if($result=$this->exeSQL($query))
			{
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		
	}
	function currencyFinder($id)
	{
		$query="
			SELECT
				szCurrency
			FROM
				".__DBC_SCHEMATA_CURRENCY__."
			WHERE
				id ='".(int)$id."'
			";
		if( ( $result = $this->exeSQL( $query ) ) )
		{	
			$row=$this->getAssoc($result);
			return $row;	
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
			
	}
	function expiringWTWAdmin($sortBy = '',$order = '')
	{
		if($sortBy!='' && $order!='')
		{
			$orderByQuery = "
					ORDER BY
						".$sortBy." ".$order."
			";
		}
		else
		{
			$orderByQuery = "
					ORDER BY
						pw.dtExpiry DESC
			";
		}
                if($iWarehouseType>0)
                {
                    $query_and = " AND 
                        w.iWarehouseType = '".$iWarehouseType."'
                    AND
                        w2.iWarehouseType = '".$iWarehouseType."'
                    ";
                }
			$query="
					SELECT
						DISTINCT(pw.id) AS id,
						fwd.szDisplayName,
						w.szWareHouseName AS szWarehouseFrom,
						w.szCity AS szCityFrom,
						c1.szCountryISO AS  szCountryISOFrom,
						pw.idWarehouseFrom,
						w2.szWareHouseName AS szWarehouseTo,
						w2.szCity AS szCityTo,
						c2.szCountryISO AS  szCountryISOTo,
						pw.idWarehouseTo,
						pw.dtExpiry
					FROM
						".__DBC_SCHEMATA_WAREHOUSES_PRICING__." AS pw
					INNER JOIN
						".__DBC_SCHEMATA_WAREHOUSES__." AS w
					ON 	
						( w.id = pw.idWarehouseFrom && w.iActive = '1')
					INNER JOIN 
						".__DBC_SCHEMATA_COUNTRY__." AS c1
					ON	
						(c1.id = w.idCountry && c1.iActive = '1')		
					INNER JOIN
						".__DBC_SCHEMATA_WAREHOUSES__." AS w2
					ON 	
						( w2.id = pw.idWarehouseTo && w2.iActive = '1')
					INNER JOIN 
                                            ".__DBC_SCHEMATA_COUNTRY__." AS c2
					ON	
                                            (c2.id = w2.idCountry && c2.iActive = '1')	
					INNER JOIN
                                            ".__DBC_SCHEMATA_FORWARDERS__." AS fwd
					ON
                                            ( fwd.id = w.idForwarder && fwd.iActive  = '1')				
					WHERE
                                            pw.dtExpiry >=  DATE_SUB(CURDATE(), INTERVAL 31 DAY)
					AND
                                            pw.dtExpiry <=  DATE_ADD(CURDATE(), INTERVAL 31 DAY)
					AND
                                            pw.iActive='1' 
                                            $query_and
					".$orderByQuery."
					";
			//echo $query;	
			if($result=$this->exeSQL($query))
			{
				$ccDataAry = array();
				if($this->iNumRows>0)
				{
					while($row=$this->getAssoc($result))
					{
						$ccDataAry[] = $row;
					}
				}
					return $ccDataAry ;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	
	}
	function countHaulagePricing($id)
	{
		if($id>0)
		{
			$query="
					SELECT
						count(h.id) as id
					FROM
						".__DBC_SCHEMATA_HAULAGE_PRICING__." AS h
					INNER JOIN
						".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." AS hm
					ON(hm.id = h.idHaulagePricingModel)
					WHERE
						hm.idWarehouse = ".(int)$id."
					AND
						hm.iActive = 1	
					AND
						h.iActive = 1		
					";
			//echo $query;	
			if($result=$this->exeSQL($query))
			{
				$row=$this->getAssoc($result);
				return $row['id'];
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function countCCPricing($idFrom,$idTo,$originDestination)
	{
		$query="
				SELECT
					count(id) as id
				FROM
					".__DBC_SCHEMATA_PRICING_CC__."
				WHERE
					idWarehouseFrom = ".(int)$idFrom."
				AND
					idWarehouseTo = ".(int)$idTo."	
				AND
					szOriginDestination = ".(int)$originDestination."
				AND
					iActive = 1	
				";
		//echo $query;	
		if($result=$this->exeSQL($query))
		{
			$row=$this->getAssoc($result);
			return $row['id'];
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function selectDtValidExpiry($id)
	{
		$query="
				SELECT
					dtValidFrom,
					dtExpiry
				FROM
					".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
				WHERE
					id = ".(int)$id."
				";
		//echo $query;	
		if($result=$this->exeSQL($query))
		{
			$row=$this->getAssoc($result);
			return $row;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function setvaildateExpiry($id,$dtValidFrom,$dtValidTo)
	{
			$this->set_dtValidFrom(sanitize_all_html_input(trim($dtValidFrom)));
			$this->set_dtExpiry(sanitize_all_html_input(trim($dtValidTo)));
			$this->set_id((int)$id);
			if(!empty($this->dtValidFrom))
			{
				 $dateAry = explode("/",$this->dtValidFrom); 
				 $dtTiming_millisecond = strtotime($dateAry[2]."-".$dateAry[1]."-".$dateAry[0]);  // converting date 
				 $dtDate = date("d/m/Y",$dtTiming_millisecond);
				 $vaildity_timestam = $dtTiming_millisecond ;	 
				 if(!checkdate($dateAry[1],$dateAry[0],$dateAry[2]))
	             {
	                $this->addError("dtValidFrom",t($this->t_base.'fields/validity')." ".t($this->t_base.'fields/from')." ".t('Error/valid_date_landing_page'));
	                return false;
	             }
			 	 else if(($vaildity_timestam < strtotime(date('Y-m-d'))))
	             {
	             	$this->addError("dtValidFrom",t($this->t_base.'fields/validity')." ".t($this->t_base.'fields/from')." ".t('Error/must_be_greater_than_today'));
	             	return false;
	             }
	             else
	             {
	             	$this->dtValidFrom = $dateAry[2]."-".$dateAry[1]."-".$dateAry[0]." 00:00:00";  // YYYY/MM/DD H:I:S
	             }
			}	
			if(!empty($this->dtExpiry))
			{
				 $dateAry = explode("/",$this->dtExpiry); 
				 $dtTiming_millisecond = strtotime($dateAry[2]."-".$dateAry[1]."-".$dateAry[0]);  // converting date 
				 $dtDate = date("d/m/Y",$dtTiming_millisecond);
				 	 
				 if(!checkdate($dateAry[1],$dateAry[0],$dateAry[2]))
	             {
	                $this->addError("dtExpiry",t($this->t_base.'fields/validity')." ".t($this->t_base.'fields/to')." ".t('Error/valid_date_landing_page'));
	                return false;
	             }
				 else if(($dtTiming_millisecond <= strtotime(date('Y-m-d'))))
	             {
	             	$this->addError("dtExpiry",t($this->t_base.'fields/validity')." ".t($this->t_base.'fields/to')." ".t('Error/must_be_greater_than_today'));
	             	return false;
	             }
	             else if($vaildity_timestam > $dtTiming_millisecond)
	             {
	             	$this->addError("dtExpiry",t($this->t_base.'messages/validity_from_date_should_be_less_than_expiry_date'));
	             	return false;
	             }
	             else
	             {
	             	$this->dtExpiry = $dateAry[2]."-".$dateAry[1]."-".$dateAry[0]." 00:00:00";  // YYYY/MM/DD H:I:S
	             }
			}
				if($this->error==true)
				{
					return false;
				}
				$query="
					UPDATE
						".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
					SET
						dtValidFrom =  '".mysql_escape_custom(trim($this->dtValidFrom))."',
						dtExpiry	=  '".mysql_escape_custom(trim($this->dtExpiry))."',
						dtUpdatedOn = NOW()
				     WHERE
				      id = '".(int)$this->id."'
			";
			//echo "<br>".$query."<br>";
			//die;
			if($reusult=$this->exeSQL($query))
			{
				return true;
			}
			else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
	}
	 
	function fetchTotalPricingHaulage($idWarehouse,$iDirection,$idHaulageModel)
	{
		$query = "
			SELECT 
				count(id) as total
			FROM
				
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." 
			WHERE
				idWarehouse = '".(int)$idWarehouse."'
			AND	
				iDirection = '".(int)$iDirection."'
			AND	
				idHaulageModel = '".(int)$idHaulageModel."'
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->getRowCnt() > 0)
			{
				$row=$this->getAssoc($result);
				return $row['total'];
			}
			else
			{
				return 0;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	
	}
	function fetchActivePricingHaulage($idWarehouse,$iDirection,$idHaulageModel)
	{
		$query = "
			SELECT 
				count(DISTINCT(pm.id)) as active
			FROM
				
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." AS pm
			INNER JOIN
				".__DBC_SCHEMATA_HAULAGE_PRICING__." AS p
			ON
				(p.idHaulagePricingModel = pm.id)	
			WHERE
				p.iActive  = '1'
			AND
				pm.idWarehouse = '".(int)$idWarehouse."'
			AND	
				pm.iDirection = '".(int)$iDirection."'	
			AND	
				idHaulageModel = '".(int)$idHaulageModel."'		
		";
		//echo $query."<BR/>";
		if($result=$this->exeSQL($query))
		{
			if($this->getRowCnt() > 0)
			{
				$row=$this->getAssoc($result);
				return $row['active'];
			}
			else
			{
				return 0;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	
	}
	function checkExchangeRatesLCLServicePricing($idCurrency)
	{
			$query="
				SELECT 
					count(wtw.id) as numberLCLService,
					w.idForwarder
				FROM
					".__DBC_SCHEMATA_WAREHOUSES_PRICING__." AS wtw
				INNER JOIN
					".__DBC_SCHEMATA_WAREHOUSES__." AS w
				ON
					w.id=wtw.idWarehouseFrom
				INNER JOIN
					".__DBC_SCHEMATA_FORWARDERS__." AS f
				ON
					f.id=w.idForwarder
				WHERE
				(
                                    wtw.szOriginChargeCurrency = ".$idCurrency."
				OR
                                    wtw.szFreightCurrency= ".$idCurrency."
				OR
                                    wtw.szDestinationChargeCurrency = ".$idCurrency."
				 )
				AND
                                    wtw.iActive = 1
				AND
                                    date(wtw.dtExpiry)>='".mysql_escape_custom(date('Y-m-d'))."'
				AND
                                    f.iActive='1'
				AND
                                    f.isOnline='1' 
				HAVING 
					numberLCLService>0					
				";
			//echo $query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				$row=$this->getAssoc($result);
				
				return $row['numberLCLService'];
				
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	
	function checkExchangeRatesHaulageServicePricing($idCurrency)
	{
			$query="
				SELECT 
					count(h.id) as numberHaulagePricing,
					w.idForwarder
				FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING__." AS h
				INNER JOIN
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." AS hpm
				ON
					hpm.id=h.idHaulagePricingModel
				INNER JOIN
					".__DBC_SCHEMATA_WAREHOUSES__." AS w
				ON
					w.id=hpm.idWarehouse
				INNER JOIN
					".__DBC_SCHEMATA_FORWARDERS__." AS f
				ON
					f.id=w.idForwarder
				WHERE
                                    h.idCurrency = ".$idCurrency."				
                                AND
                                    h.iActive = '1'
				AND
                                    hpm.iActive='1'
				AND
                                    f.iActive='1'
				AND
                                    f.isOnline='1'	 
				HAVING 
                                    numberHaulagePricing>0			
                                ";
			//echo $query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				$row=$this->getAssoc($result);
				
				return $row['numberHaulagePricing'];
				
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	
	function checkExchangeRatesCustmClearanceoServicePricing($idCurrency)
	{
            $query="
                SELECT 
                    count(cc.id) as numberCustomClearance,
                    w.idForwarder 	
                FROM
                    ".__DBC_SCHEMATA_PRICING_CC__." AS cc
                INNER JOIN
                    ".__DBC_SCHEMATA_WAREHOUSES__." AS w
                ON
                    w.id=cc.idWarehouseFrom
                INNER JOIN
                    ".__DBC_SCHEMATA_FORWARDERS__." AS f
                ON
                    f.id=w.idForwarder
                WHERE
                    cc.szCurrency = ".$idCurrency."	
                 AND
                    cc.iActive = 1
                 AND
                    f.iActive='1'
                 AND
                    f.isOnline='1'	 
                HAVING numberCustomClearance>0				
            ";
            //echo $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {	
                $row=$this->getAssoc($result); 
                return $row['numberCustomClearance'];

            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
	
	function getForwarderIdForUpdatingCurrencyStatus($idCurrency)
	{
			$res_arr=array();
			$query="
				SELECT 
					count(cc.id) as numberCustomClearance,
					w.idForwarder 	
				FROM
					".__DBC_SCHEMATA_PRICING_CC__." AS cc
				INNER JOIN
					".__DBC_SCHEMATA_WAREHOUSES__." AS w
				ON
					w.id=cc.idWarehouseFrom
				INNER JOIN
					".__DBC_SCHEMATA_FORWARDERS__." AS f
				ON
					f.id=w.idForwarder
				WHERE
				 	cc.szCurrency = ".$idCurrency."	
				 AND
					cc.iActive = 1
				 AND
					f.iActive='1'
				 AND
					f.isOnline='1' 
				GROUP BY
					w.idForwarder					
				HAVING numberCustomClearance>0				
				";
			//echo "<br/><br/>".$query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{					
				while($row=$this->getAssoc($result))
				{
					$res_arr[$row['idForwarder']]['customClearance']=$row['numberCustomClearance'];
				}	
			}
			
			$query="
				SELECT 
					count(wtw.id) as numberLCLService,
					w.idForwarder
				FROM
					".__DBC_SCHEMATA_WAREHOUSES_PRICING__." AS wtw
				INNER JOIN
					".__DBC_SCHEMATA_WAREHOUSES__." AS w
				ON
					w.id=wtw.idWarehouseFrom
				INNER JOIN
					".__DBC_SCHEMATA_FORWARDERS__." AS f
				ON
					f.id=w.idForwarder
				WHERE
				(
                                        wtw.szOriginChargeCurrency = ".$idCurrency."
                                    OR
                                        wtw.szFreightCurrency= ".$idCurrency."
                                    OR
                                        wtw.szDestinationChargeCurrency = ".$idCurrency."
				 )
				AND
                                    wtw.iActive = 1
				AND
                                    date(wtw.dtExpiry)>='".mysql_escape_custom(date('Y-m-d'))."'
				AND
                                    f.iActive='1'
				 AND
                                    f.isOnline='1' 
				GROUP BY
                                    w.idForwarder					
				HAVING 
					numberLCLService>0					
				";
			//echo $query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				while($row=$this->getAssoc($result))
				{
					$res_arr[$row['idForwarder']]['lclService']=$row['numberLCLService'];
				}
			}
			
			$query="
				SELECT 
					count(h.id) as numberHaulagePricing,
					w.idForwarder
				FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING__." AS h
				INNER JOIN
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." AS hpm
				ON
                                    hpm.id=h.idHaulagePricingModel
				INNER JOIN
                                    ".__DBC_SCHEMATA_WAREHOUSES__." AS w
				ON
                                    w.id=hpm.idWarehouse
				INNER JOIN
                                    ".__DBC_SCHEMATA_FORWARDERS__." AS f
				ON
                                    w.idForwarder=f.id
				WHERE
                                    h.idCurrency = ".$idCurrency."				
				 AND
                                    h.iActive = '1'
				AND
                                    hpm.iActive='1'
				AND
                                    f.iActive='1'
				AND
                                    f.isOnline='1' 
				GROUP BY
                                    w.idForwarder					
				HAVING 
                                    numberHaulagePricing>0			
                            ";
			//echo $query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				while($row=$this->getAssoc($result))
				{
					$res_arr[$row['idForwarder']]['haulagePricing']=$row['numberHaulagePricing'];
				}
				
				return $res_arr;
			}
			
			return $res_arr;
			
	}
	
	function showForwarderPendingPaymentCancelBooking($idForwarder=0,$idForwarderCurrency=0)
	{
            $sql='';
            if((int)$idForwarder>0 && (int)$idForwarderCurrency>0)
            {
                $sql="
                    AND
                        f.id='".(int)$idForwarder."'
                    AND
                        ft.idForwarderCurrency='".(int)$idForwarderCurrency."' 
                ";
            }		
            $query="
                SELECT 
                    f.id,
                    ft.szForwarderCurrency,
                    ft.idForwarderCurrency,
                    c.szCurrency,
                    SUM(ft.fTotalPriceForwarderCurrency) AS fForwarderTotalPrice
                FROM 
                    ". __DBC_SCHEMATA_FORWARDERS__ ." AS f	
                LEFT JOIN 	
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS ft
                ON
                    (ft.idForwarder=f.id and iBatchNo='0' and (ft.iStatus='1' OR ft.iStatus='0') AND ft.iTransferConfirmed=0 and (ft.iDebitCredit='6' OR ft.iDebitCredit='7') )
                LEFT JOIN 
                    ".__DBC_SCHEMATA_CURRENCY__." AS c
                ON 
                    (f.szCurrency=c.id)
                WHERE
                    f.szDisplayName!=''
                AND 
                    ft.iForwarderGPType != '".__FORWARDER_PROFIT_TYPE_MARK_UP__."'
                    ".$sql."	
                GROUP BY 
                        f.id,ft.szForwarderCurrency	
                ORDER BY 
                        f.szDisplayName
                ";
                //ECHO $query;
                if($result = $this->exeSQL( $query ))
                {
                    if ($this->getRowCnt() > 0)
                    {	
                        $manageVar=array();
                        while($row=$this->getAssoc($result))
                        {	
                            $manageVar[$row['id']][$row['szForwarderCurrency']]=$row['fForwarderTotalPrice'];
                        }
                        return $manageVar;
                    }
                    else
                    {
                        return array();
                    }	
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
	}
	
	function checkForwarderCurrency($id,$flag=false)
	{
			
		$sql .="
			AND
				isOnline='1'
			AND
				iActive='1'	
		";
		
		
		$query="
			SELECT
				f.id,
				f.szDisplayName
			FROM 
				". __DBC_SCHEMATA_FORWARDERS__ ." AS f
			WHERE
				 szCurrency='".(int)$id."'
			".$sql."		
		";
		if($result = $this->exeSQL( $query ))
		{
			if ($this->getRowCnt() > 0)
			{
				if($flag)
				{
					$total=0;
					while($row=$this->getAssoc($result))
					{
						$res_arr[]=$row['id'];
					}
						$total=count($res_arr);
					return 	$total;
					
				}
				else
				{
					while($row=$this->getAssoc($result))
					{
						$res_arr[]=$row;
					}
					
					return $res_arr;
				}
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
		
	}
	
	
	function saveRatesForwarderPricing($id)
	{	
		if($id>0)
		{				
			$query="
					UPDATE
						".__DBC_SCHEMATA_CURRENCY__." AS c
					SET
						c.iPricing   ='0'
					WHERE	
						c.id='".$id."'	
				";
			
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				$query="
					UPDATE
						".__DBC_SCHEMATA_HAULAGE_PRICING__."
					SET
						iActive='0'
					WHERE
						idCurrency = ".$id."
				";
				$result = $this->exeSQL( $query );
				
				
				$query="
					UPDATE 
						".__DBC_SCHEMATA_WAREHOUSES_PRICING__." AS wtw
					SET
						wtw.iActive='0'
					WHERE
					(
							wtw.szOriginChargeCurrency = ".$id."
						OR
							wtw.szFreightCurrency= ".$id."
						OR
						 	wtw.szDestinationChargeCurrency = ".$id."
					 )
					";
					$result = $this->exeSQL( $query );
					
					$query="
						UPDATE 
							".__DBC_SCHEMATA_PRICING_CC__." AS cc
						SET
							cc.iActive='0'
						WHERE
						 	cc.szCurrency = ".$id."
					";
					$result = $this->exeSQL( $query );
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	
	function saveRatesForwarderSettling($id)
	{	
		if($id>0)
		{				
			$query="
					UPDATE
						".__DBC_SCHEMATA_CURRENCY__." AS c
					SET
						c.iSettling  ='0'
					WHERE	
						c.id='".(int)$id."'	
				";
			
			if( ( $result = $this->exeSQL( $query ) ) )
			{	
				$query="
					UPDATE
						". __DBC_SCHEMATA_FORWARDERS__ ."
					SET
					 	szCurrency='0',
					 	isOnline='0'
					 WHERE
					 	szCurrency='".(int)$id."'
				";
				
				$result = $this->exeSQL( $query );
				
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	
	function saveRatesChangeCurrency($id,$rates,$ratesId)
	{ 
            if($id>0 && (!empty($rates)) && $ratesId>0)
            {	
                $this->currencyId=$ratesId;
                $this->vildate_currency_details($rates);  

                if($this->error=== true)
                {
                    return false;
                } 
                
                $this->validateCurrencyName($ratesId);
                if($this->error=== true)
                {
                    return false;
                } 
			
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_CURRENCY__." AS c
                    SET
                        c.szCurrency ='".mysql_escape_custom($this->szCurrency)."',
                        c.iBooking ='".mysql_escape_custom($this->iBooking)."',
                        c.szFeed  ='".mysql_escape_custom($this->szFeed)."',
                        c.szBankName ='".mysql_escape_custom($this->szBankName)."',
                        c.szSortCode ='".mysql_escape_custom($this->szSortCode)."',
                        c.szAccountNumber ='".mysql_escape_custom($this->szAccountNumber)."',
                        c.szIBANNumber = '".mysql_escape_custom($this->szIBANNumber)."',
                        c.szSwiftNumber ='".mysql_escape_custom($this->szSwiftNumber)."',
                        c.szNameOnAccount ='".mysql_escape_custom($this->szNameOnAccount)."' 
                    WHERE	
                        c.id='".$ratesId."'	
                ";
                //echo $query."<br>";
                //die;
                if(($result = $this->exeSQL($query)))
                { 
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
            else
            {
                return false;
            }
	}
	
	function sendEmailForUpdateForwarderCurrency($forwarderUsingCurrencyArr,$idCurrency)
	{
		if(!empty($forwarderUsingCurrencyArr))
		{
			$kForwarder = new cForwarder();
			$kConfig = new cConfig();
			$szCurrencyArr=$kConfig->getBookingCurrency($idCurrency);
			foreach($forwarderUsingCurrencyArr as $forwarderUsingCurrencyArrs)
			{
				$kForwarder->load($forwarderUsingCurrencyArrs['id']);
				
				$query = "
					SELECT
						id,
						szFirstName,
						szLastName,
						szEmail
					FROM
						".__DBC_SCHEMATA_FORWARDERS_CONTACT__."		
					WHERE
							idForwarder = ".$forwarderUsingCurrencyArrs['id']."
					AND
						idForwarderContactRole   IN (".__ADMINISTRATOR_PROFILE_ID__.",".__PRICING_PROFILE_ID__.")
					AND
						iActive='1'
					AND
						iConfirmed ='1'			
				";
				if($result=$this->exeSQL($query))
				{
					$profileArr = array();
					while($row=$this->getAssoc($result))
					{
						$profileArr[] = $row;
					}
					if(!empty($profileArr))
					{
						foreach($profileArr as $profileArrs)
						{
							$replace_ary['szDisplayName'] = $kForwarder->szDisplayName;
							$replace_ary['szName'] = $profileArrs['szFirstName']." ".$profileArrs['szLastName'];
							$replace_ary['szEmail'] = $profileArrs['szEmail'];
							$replace_ary['szCurrency']=$szCurrencyArr[0]['szCurrency'];
							
							createEmail(__UPDATING_SETTLING_CURRENCY_BY_ADMIN_EMAIL__, $replace_ary,$profileArrs['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$profileArrs['id'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__,false);
						}
					}
				}
			}
			return true;
		}else
		{
			return true;
		}
	}
	
	function totalAmountConfirmUploadService($batchNumber)
	{
		$query="
			SELECT 
				fTotalPriceForwarderCurrency AS totalCurrencyValue,
				szForwarderCurrency,
				idForwarder,
				szInvoice
			FROM 
				".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
			WHERE
				iBatchNo='".mysql_escape_custom($batchNumber)."'
			AND
				iStatus='2'
			AND
			 	iDebitCredit='5'
			 	";
			//echo $query;
			if($result = $this->exeSQL( $query ))
			{
				if ($this->getRowCnt() > 0)
				{
					$transferPaymentVar=array();
					while($row=$this->getAssoc($result))
					{	
						$transferPaymentVar[]=$row;
					}
					return $transferPaymentVar;
				}
				else
				{
					return array();
				}
			}
			else
			{
				return array();
			}
	}
	
	function sendServiceInactivationEmailToForwarder($forwarderServiceStr,$szText,$idCurrency)
	{
		if(!empty($forwarderServiceStr))
		{
			$kConfig = new cConfig();
			$szCurrencyArr=$kConfig->getBookingCurrency($idCurrency);
			$forwarderServiceArr=explode(";",$forwarderServiceStr);
			if(!empty($forwarderServiceArr))
			{
				foreach($forwarderServiceArr as $forwarderServiceArrs)
				{
					$forwarderArr=array();
					$forwarderArr=explode("_",$forwarderServiceArrs);
					if(!empty($forwarderArr))
					{
						$idForwarder=$forwarderArr[0];
						$kForwarder = new cForwarder();
						$kForwarder->load($idForwarder);
						$szHaulageUrl='';
						$szCCUrl='';
						$szLCLServiceUrl='';
						$szControlUrl="http://".$kForwarder->szControlPanelUrl;
						if($forwarderArr[1]!='')
						{
							if($forwarderArr[1]=='h')
							{
								$szHaulageUrl=$szControlUrl."/HaulageBulk/InactiveService/".$idCurrency."/";
							}
							else if($forwarderArr[1]=='c')
							{
								$szCCUrl=$szControlUrl."/CustomsClearanceBulk/InactiveService/".$idCurrency."/";
							}
							else if($forwarderArr[1]=='l')
							{
								$szLCLServiceUrl=$szControlUrl."/LCLServicesBulk/InactiveService/".$idCurrency."/";
							}
						}
						
						if($forwarderArr[2]!='')
						{
							if($forwarderArr[2]=='h')
							{
								$szHaulageUrl=$szControlUrl."/HaulageBulk/InactiveService/".$idCurrency."/";
							}
							else if($forwarderArr[2]=='c')
							{
								$szCCUrl=$szControlUrl."/CustomsClearanceBulk/InactiveService/".$idCurrency."/";
							}
							else if($forwarderArr[2]=='l')
							{
								$szLCLServiceUrl=$szControlUrl."/LCLServicesBulk/InactiveService/".$idCurrency."/";
							}
						}
						
						if($forwarderArr[3]!='')
						{
							if($forwarderArr[3]=='h')
							{
								$szHaulageUrl=$szControlUrl."/HaulageBulk/InactiveService/".$idCurrency."/";
							}
							else if($forwarderArr[3]=='c')
							{
								$szCCUrl=$szControlUrl."/CustomsClearanceBulk/InactiveService/".$idCurrency."/";
							}
							else if($forwarderArr[3]=='l')
							{
								$szLCLServiceUrl=$szControlUrl."/LCLServicesBulk/InactiveService/".$idCurrency."/";
							}
						}
					}
					
					$query = "
						SELECT
							id,
							szFirstName,
							szLastName,
							szEmail
						FROM
							".__DBC_SCHEMATA_FORWARDERS_CONTACT__."		
						WHERE
								idForwarder = ".$idForwarder."
						AND
							idForwarderContactRole   IN (".__ADMINISTRATOR_PROFILE_ID__.",".__PRICING_PROFILE_ID__.")
						AND
							iActive='1'
						AND
							iConfirmed ='1'			
					";
					if($result=$this->exeSQL($query))
					{
						$profileArr = array();
						while($row=$this->getAssoc($result))
						{
							$profileArr[] = $row;
						}
						if(!empty($profileArr))
						{
							foreach($profileArr as $profileArrs)
							{	
								
								$replace_ary['szName'] = $profileArrs['szFirstName']." ".$profileArrs['szLastName'];
								$replace_ary['szEmail'] = $profileArrs['szEmail'];
								$replace_ary['szCurrency']=$szCurrencyArr[0]['szCurrency'];
								$replace_ary['szText'] = $szText;
								
								if($szHaulageUrl!='')
									$replace_ary['Haulage']="<a href='".$szHaulageUrl."'>Haulage Data</a><br/><br/>";
								else
									$replace_ary['Haulage']=$szHaulageUrl;
									
								if($szCCUrl!='')	
									$replace_ary['CCUrl']="<a href='".$szCCUrl."'>Custom Clearance Data</a><br/><br/>";
								else
									$replace_ary['CCUrl']='';

								if($szLCLServiceUrl!='')
									$replace_ary['LCLServiceUrl']="<a href='".$szLCLServiceUrl."'>LCL Service Data</a><br/><br/>";
								else
									$replace_ary['LCLServiceUrl']="";
									
								//print_r($replace_ary);
								createEmail(__SEND_EMAIL_TO_FORWARDER_OF_CANCEL_CURRENCY__, $replace_ary,$profileArrs['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$profileArrs['id'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__,false);
							}
						}
					}
				}
			}
		}
	}
	
	
	function getReferralFeeForwarderAmountCancelBooking($transferArr)
	{
		if($transferArr!='')
		{
			$query="
			SELECT 
				SUM(fTotalPriceForwarderCurrency) AS totalCurrencyValue,
				SUM(fReferalAmount) AS referralfee,
				(SUM(ROUND(fTotalPriceForwarderCurrency,2))-SUM(ROUND(fReferalAmount,2))) AS fPaidToForwarder,
				szForwarderCurrency,
				idForwarder,
				idForwarderCurrency				
			FROM 
				".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
			WHERE 
				id IN (".mysql_escape_custom(trim($transferArr)).")
			AND
				iStatus='1'
			AND
				iDebitCredit IN ('6','7') 
			";
			//echo $query;
			if($result = $this->exeSQL( $query ))
			{
				if ($this->getRowCnt() > 0)
				{	
					$amountValueArr=array();
					while($row=$this->getAssoc($result))
					{
						$amountValueArr[]=$row;
					}
					return $amountValueArr;
				}
				else
				{
					return array();
				}
			}
		}
		else
		{
			return array();
		}
	}
	
	function totalAmountConfirmCancelBooking($idBatchNumber)
	{
            $query="
                SELECT 
                    SUM(fTotalPriceForwarderCurrency) AS totalCurrencyValue,
                    SUM(fReferalAmount) AS referralfee,
                    (SUM(fTotalPriceForwarderCurrency)-SUM(fReferalAmount)) AS fPaidToForwarder,
                    szForwarderCurrency,
                    idForwarder
                FROM 
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                WHERE
                    iBatchNo='".mysql_escape_custom($idBatchNumber)."'
                AND
                    iStatus='1'
                AND
                    iDebitCredit='6'
            ";
            //echo $query;
            if($result = $this->exeSQL( $query ))
            {
                if ($this->getRowCnt() > 0)
                {
                    $transferPaymentVar=array();
                    while($row=$this->getAssoc($result))
                    {	
                        $transferPaymentVar[]=$row;
                    }
                    return $transferPaymentVar;
                }
            }
            else
            {
                return array();
            }
	}
        
        function totalAmountCourierLabelFee($idBatchNumber,$iDebitCredit=8)
	{
            $query="
                SELECT 
                    SUM(fTotalPriceForwarderCurrency) AS totalCurrencyValue,
                    SUM(fReferalAmount) AS referralfee,
                    (SUM(fTotalPriceForwarderCurrency)-SUM(fReferalAmount)) AS fPaidToForwarder,
                    szForwarderCurrency,
                    szInvoice,
                    idForwarder
                FROM 
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                WHERE
                    iBatchNo='".mysql_escape_custom($idBatchNumber)."'
                AND
                    iStatus = '2'
                AND
                    iDebitCredit='".$iDebitCredit."'
            ";
            //echo "<br><br>".$query;
            if($result = $this->exeSQL( $query ))
            {
                if ($this->getRowCnt() > 0)
                {
                    $transferPaymentVar=array();
                    $ctr=0;
                    while($row=$this->getAssoc($result))
                    {	
                        $transferPaymentVar[$ctr]=$row;
                        $ctr++;
                    }
                    return $transferPaymentVar;
                }
            }
            else
            {
                return array();
            }
	}
	
	function checkBookAmountEqualAmountPaid($id)
	{
		$query="
			SELECT
                            fCustomerPaidAmount,
                            fTotalPriceCustomerCurrency,
                            fTotalVat,
                            fTotalInsuranceCostForBookingCustomerCurrency,
                            iInsuranceIncluded,
                            szBookingRef
			FROM
				".__DBC_SCHEMATA_BOOKING__."
			WHERE
				id='".(int)$id."'
		";
		//echo $query."<br/>";
		if($result = $this->exeSQL( $query ))
		{
			if ($this->getRowCnt() > 0)
			{
				$row=$this->getAssoc($result);
                                
                                if($row['iInsuranceIncluded']==1)
                                { 
                                    $fTotalAmountTobePaid = $row['fTotalPriceCustomerCurrency'] + $row['fTotalInsuranceCostForBookingCustomerCurrency'] + $row['fTotalVat'];
                                }
                                else
                                { 
                                    $fTotalAmountTobePaid = $row['fTotalPriceCustomerCurrency'] + $row['fTotalVat'];
                                }
                                //echo "<br> Booking# ".$row['szBookingRef']." To be paid ".$fTotalAmountTobePaid."   to: ".$row['fCustomerPaidAmount']." cur ".$row['fTotalPriceCustomerCurrency']." vat ".$row['fTotalVat'];
				if(round((float)$row['fCustomerPaidAmount'],2)!=round((float)$fTotalAmountTobePaid,2))
				{
                                    return false;
				}
				else
				{
                                    return true;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function updateCustomerPaidAmount($bookingId,$iUpdateBankTransfer=false,$iMarkupPaymentReceived=false,$bQuickQuote=false)
	{
            if($iUpdateBankTransfer)
            {
                $query_update = ",dtBookingConfirmed = now() ";
            }
            
            if($iMarkupPaymentReceived)
            {
                //In case of mark-up we can't use iPaymentReceived because this different purpose so that we are creating a new one. 
                $query_update .= ", iMarkupPaymentReceived = '1' ";
            } 
            if($bQuickQuote)
            {
                $query_update .= " ,iQuickQuote = '".__QUICK_QUOTE_CONFIRMED_BOOKING__."' ";
            }

            $kBooking = new cBooking();
            $postSearchAry = $kBooking->getBookingDetails($bookingId);

            if($postSearchAry['iInsuranceIncluded']==1)
            { 
                $fCustomerPaidAmount = $postSearchAry['fTotalPriceCustomerCurrency'] + $postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency'] + $postSearchAry['fTotalVat'];
            }
            else
            { 
                $fCustomerPaidAmount = $postSearchAry['fTotalPriceCustomerCurrency'] + $postSearchAry['fTotalVat'];
            }  
            
            if(trim(cPartner::$szApiRequestMode) == __API_REQUEST_MODE_TEST__)
            {
                $table = __DBC_SCHEMATA_CLONE_BOOKING__;
            }
            else
            {
                $table = __DBC_SCHEMATA_BOOKING__;
            }
            
            $query="
                UPDATE
                    ".$table."
                SET
                    fCustomerPaidAmount= '".(float)$fCustomerPaidAmount."',
                    iTransferConfirmed = '0',
                    iBookingStep = 14,
                    iPaymentReceived = '1'
                    $query_update
                WHERE
                    id='".(int)$bookingId."'
            ";
            
            if($result = $this->exeSQL($query))
            {
                $fTotalVatUSD = $postSearchAry['fTotalVat'] * $postSearchAry['fExchangeRate']; 
                
                $fInsuranceBuyRateUSD = 0;
                if($postSearchAry['iInsuranceIncluded']==1)
                {
                    $fInsuranceBuyRateUSD = $postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency_buyRate'] * $postSearchAry['fExchangeRate'] ;
                }                
                
                $addWorkingCapitalAry = array();
                $addWorkingCapitalAry['fPriceUSD'] = $postSearchAry['fTotalPriceUSD'] + $fTotalVatUSD - $postSearchAry['fReferalAmountUSD'] + $fInsuranceBuyRateUSD;
                $addWorkingCapitalAry['szAmountType'] = '(Booking Price + VAT + Insurance Buyrate) - Referal Fee' ;
                $addWorkingCapitalAry['iDebitCredit'] = 1 ;
                $addWorkingCapitalAry['szReference'] = $postSearchAry['szBookingRef'] ;
                
                $kDashBoardAdmin = new cDashboardAdmin();
                $kDashBoardAdmin->addGraphWorkingCapitalCurrent($addWorkingCapitalAry);
                
                return true;
            }
            else
            {
                return false;
            }
	}
	
	function getReferralFeeUSDValue($invoiceNumber)
	{
            $query="
                SELECT	
                        IFNULL(SUM(B.fTotalPriceNoMarkupForwarderCurrency*B.fReferalPercentage*B.fForwarderExchangeRate)/100,0) as referralFeeTotal				
                FROM
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS FT
                INNER JOIN
                    ".__DBC_SCHEMATA_BOOKING__." AS B
                ON
                    B.id=FT.idBooking
                WHERE 
                    FT.iDebitCredit = 1 
                AND
                    FT.iStatus IN ('1','2')
                AND
                    FT.iBatchNo='".mysql_escape_custom($invoiceNumber)."'
            ";
            //echo $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                $dashBoardArr = array();
                $row=$this->getAssoc($result);

                $referralFeeTotal=$row['referralFeeTotal'];
            }
		
		
		$query="
			SELECT	
				IFNULL(SUM(B.fTotalPriceNoMarkupForwarderCurrency*B.fReferalPercentage*B.fForwarderExchangeRate)/100,0) as referralFeeTotal				
			FROM
				".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS FT
			INNER JOIN
				".__DBC_SCHEMATA_BOOKING__." AS B
			ON
				B.id=FT.idBooking
			WHERE 
				FT.iDebitCredit = 6 
			AND
				FT.iStatus IN ('1','2')
			AND
				FT.iBatchNo='".mysql_escape_custom($invoiceNumber)."'
			AND
				FT.fReferalAmount>0
		";
     	//echo $query;
    	if( ( $result = $this->exeSQL( $query ) ) )
		{
			$dashBoardArr = array();
			$row=$this->getAssoc($result);
			
			$referralFeeCancelTotal=$row['referralFeeTotal'];
		}
		
		if((float)$referralFeeTotal>0 && $referralFeeTotal>$referralFeeCancelTotal)
		{
			$referralFeeTotal=$referralFeeTotal-$referralFeeCancelTotal;
		}
		else
		{
			$referralFeeTotal=0;
		}
		
		return $referralFeeTotal;
	}
	
	
	function getActiveCustomerDetails($szEmail)
	{
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_USERS__."
			WHERE
				szEmail='".mysql_escape_custom($szEmail)."'
			AND
				iActive='1'
			AND
				iConfirmed='1'
		";
		//echo $query."<br/>";
		if( ( $result = $this->exeSQL( $query ) ) )
		{
			$row=$this->getAssoc($result);
			
			return $row['id'];
		}
		else
		{
			return 0;
		}
	}
	
	function updateEmailSendFlag($id)
	{
		$query="
			UPDATE
				".__DBC_SCHEMATA_MANAGEMENT_MESSAGES__."
			SET
				iMailSend='1'
			WHERE
				id='".(int)$id."'
		";
		$result = $this->exeSQL( $query );
	}
	
	function saveAdminMessages($szEmail,$idUser,$szMessages,$szSubject,$iType,$idBatch)
	{
		$query="
			INSERT INTO
				".__DBC_SCHEMATA_MANAGEMENT_MESSAGES__."
			(
				szSubject,
				szMessage,
				szEmail,
				iType,
				idUser,
				dtCreated,
				idBatch
			)
				VALUES
			(
				'".mysql_escape_custom($szSubject)."',
				'".mysql_escape_custom($szMessages)."',
				'".mysql_escape_custom($szEmail)."',
				'".(int)$iType."',
				'".(int)$idUser."',
				NOW(),
				'".(int)$idBatch."'
			)		
		";
		$result = $this->exeSQL( $query );
	}
	
	function getAllDataAdminEmailSend()
	{
	
		$query="
			DELETE FROM
				".__DBC_SCHEMATA_MANAGEMENT_MESSAGES__."
			WHERE
                            iMailSend='1'	
		";
		$result = $this->exeSQL( $query );
	
		$query="
			SELECT
				id,
				szSubject,
				szMessage,
				szEmail,
				iType,
				idUser
			FROM
				".__DBC_SCHEMATA_MANAGEMENT_MESSAGES__."
			WHERE
		    	iMailSend='0'	
		";
		//echo $query
		if( ( $result = $this->exeSQL( $query ) ) )
		{
			while($row=$this->getAssoc($result))
			{
				$res_arr[]=$row;
			}
			
			return $res_arr;
		}
		else
		{
			return array();
		}
	}
	
	function getAllForwarderHaveVerifiedUser()
	{
		$query="
			SELECT
				f.id,
				f.szDisplayName,
				f.iActive,
				f.szControlPanelUrl
			FROM
				".__DBC_SCHEMATA_FORWARDERS__."	AS f
			INNER JOIN
				".__DBC_SCHEMATA_FORWARDERS_CONTACT__." AS fc
			ON
				fc.idForwarder=f.id
			WHERE
				fc.iConfirmed='1'
			AND
				f.iActive='1'
			GROUP BY
				fc.idForwarder
			ORDER BY
				f.szDisplayName ASC,
				f.szControlPanelUrl ASC	
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$ret_ary = array();
				
				while($row=$this->getAssoc($result))
				{
					$ret_ary[] = $row;
				}
				return $ret_ary;
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function getActiveForwarderDetails($szEmail)
	{
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
			WHERE
				szEmail='".mysql_escape_custom($szEmail)."'
			AND
				iActive='1'
			AND
				iConfirmed='1'
		";
		//echo $query."<br/>";
		if( ( $result = $this->exeSQL( $query ) ) )
		{
			$row=$this->getAssoc($result);
			
			return $row['id'];
		}
		else
		{
			return 0;
		}
	}
	
	function getAllMessageSendCount($idMessage=0,$from=1)
	{
		
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_MESSAGES_LOG__."
		";
		if($result = $this->exeSQL( $query ))
		{
			if ($this->getRowCnt() > 0)
			{
				while($row=$this->getAssoc($result))
				{
					$sendMessageArr[]=$row;
				}
				
				return $sendMessageArr;
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function getMaxBatchId()
	{
		$query="
			SELECT
				MAX(idBatch) as BatchId
			FROM
				".__DBC_SCHEMATA_MANAGEMENT_MESSAGES__."
		";
		if($result = $this->exeSQL( $query ))
		{
			if ($this->getRowCnt() > 0)
			{
				$row=$this->getAssoc($result);
				$idBatch=$row['BatchId'];
				if((int)$idBatch>0)
				{
					
					$idBatch=$idBatch+1;
					return $idBatch;
				}
				else
				{
					return 1;
				}
			}
		}
	}
	
	function getAllQueueMessageSend()
	{
		$query="
			SELECT
				szSubject,
				count(id) AS total,
				idBatch,
				dtCreated
			FROM
				".__DBC_SCHEMATA_MANAGEMENT_MESSAGES__."
			WHERE
				iMailSend='0'
			GROUP BY
				idBatch
			ORDER BY
				dtCreated DESC			
		";
		if($result = $this->exeSQL( $query ))
		{
			if ($this->getRowCnt() > 0)
			{
				while($row=$this->getAssoc($result))
				{
					$res_arr[]=$row;
				}
				return $res_arr;
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function getQueueMessage($idMessage)
	{
		$query="
			SELECT
				szSubject,
				szMessage
			FROM
				".__DBC_SCHEMATA_MANAGEMENT_MESSAGES__."
			WHERE
				idBatch='".(int)$idMessage."'
		";
		if($result = $this->exeSQL( $query ))
		{
			if ($this->getRowCnt() > 0)
			{
				$row=$this->getAssoc($result);
				
				$res_arr[]=$row;
				return $res_arr;
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function deletQueuemessage($idMessage)
	{
		$query="
			DELETE FROM
				".__DBC_SCHEMATA_MANAGEMENT_MESSAGES__."
			WHERE
				idBatch='".(int)$idMessage."'		
		";
		//echo $quey;
		$result = $this->exeSQL( $query );
	}
	
	function emailLogtotalCount()
	{
		$query="
			SELECT 
				count(id) as total
			FROM 
				".__DBC_SCHEMATA_EMAIL_LOG__."	
				";
		
		if($result = $this->exeSQL( $query ))
		{
			if ($this->getRowCnt() > 0)
			{	
				$textEditor=array();
				$row=$this->getAssoc($result);
				return $row['total'];
			}
			else
			{
				return 0;
			}	
		}	
	}
	
	function emailLogDetails($min=1)
	{
            $query="
                SELECT 
                    id,
                    szEmailSubject,
                    szToAddress,
                    szEmailKey,
                    szEmailStatus,
                    dtEmailStatusUpdated,
                    dtSent as dtSents
                FROM 
                    ".__DBC_SCHEMATA_EMAIL_LOG__."	
                WHERE
                    iIncomingEmail = '0'
                ORDER BY 
                    dtSent DESC
                "; 

                $query.="
                    LIMIT
                        ".(($min -1 ) * __PER_PAGE__ ).",". __PER_PAGE__ ."
            ";
            //echo $query; 
            if($result = $this->exeSQL( $query ))
            {
                if ($this->getRowCnt() > 0)
                {	
                    $textEditor=array();
                    while($row=$this->getAssoc($result))
                    {	
                        $textEditor[]=$row;
                    }
                    return $textEditor;
                }
                else
                {
                    return array();
                }	
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }	
	} 
        
        
        function getReferralFeeForwarderAmountCancelBookingLabel($transferArr)
	{
		if($transferArr!='')
		{
			$query="
			SELECT 
				cb.fBookingLabelFeeRate,
                                cb.idBookingLabelFeeCurrency,
                                cb.szBookingLabelFeeCurrency,
                                cb.fBookingLabelFeeRate,
                                cb.fBookingLabelFeeROE,
				f.szForwarderCurrency,
				f.idForwarder,
				f.idForwarderCurrency,
                                f.fForwarderExchangeRate
			FROM 
				".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS f
                        INNER JOIN
                            ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__." AS cb
                        ON        
                            f.idBooking=cb.idBooking
			WHERE 
				f.id IN (".mysql_escape_custom(trim($transferArr)).")
			AND
				f.iStatus='1'
			AND
				f.iDebitCredit IN ('6','7') 
                        AND
                            cb.iCourierAgreementIncluded='0'
			";
			//echo $query;
			if($result = $this->exeSQL( $query ))
			{
				if ($this->getRowCnt() > 0)
				{	
					$amountValueArr=array();
					while($row=$this->getAssoc($result))
					{
                                            $bookingLabelFee=0;    
                                            $fBookingLabelFeeRate=$row['fBookingLabelFeeRate'];
                                            $fBookingLabelFeeRateROE=$row['fBookingLabelFeeROE'];
                                            $fBookingLabelFeeCurrencyId=$row['idBookingLabelFeeCurrency'];
                                            if($row['szForwarderCurrency']==$row['szBookingLabelFeeCurrency'])
                                            {
                                                $bookingLabelFee=$fBookingLabelFeeRate;
                                            }
                                            else
                                            {
                                                 if($row['szBookingLabelFeeCurrency']=='USD')
                                                 {
                                                     $forwarderExchangeRate=$row['fForwarderExchangeRate'];

                                                     $bookingLabelFee=$fBookingLabelFeeRate/$forwarderExchangeRate;
                                                 }
                                                 else
                                                 {
                                                     $fBookingLabelFeeRateUSD=$fBookingLabelFeeRate*$fBookingLabelFeeRateROE;

                                                      $forwarderExchangeRate=$row['fForwarderExchangeRate'];

                                                     $bookingLabelFee=$fBookingLabelFeeRateUSD/$forwarderExchangeRate;
                                                 }

                                            }
                                            
                                            $totalBookingLabel=$bookingLabelFee+$totalBookingLabel;
					}
                                        //echo $totalBookingLabel;
					return $totalBookingLabel;
				}
				else
				{
					return 0;
				}
			}
		}
		else
		{
			return 0;
		}
	}
        
        
        function insertCourierLabelFee($data)
	{ 
            if(!empty($data))
            {
                if($data['iDebitCredit']<=0)
                {
                    $data['iDebitCredit'] = 8;
                }
                $query="
                    INSERT INTO
                        ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                    (
                        idForwarder,
                        fTotalPriceForwarderCurrency,
                        szCurrency,
                        szForwarderCurrency,
                        szInvoice,
                        szBooking,
                        dtCreatedOn,
                        iDebitCredit,
                        iStatus,
                        iBatchNo,
                        dtInvoiceOn,
                        dtPaymentConfirmed,
                        fLabelFeeUSD,
                        fTotalHandlingFeeUSD,
                        fExchangeRate
                    )
                    VALUES
                    (
                        '".(int)mysql_escape_custom($data['idForwarder'])."',
                        '".mysql_escape_custom($data['fLabelFee'])."',
                        '".mysql_escape_custom($data['currencyName'])."',
                        '".mysql_escape_custom($data['currencyName'])."',
                        '".mysql_escape_custom($data['invoiceNumber'])."',
                        '".mysql_escape_custom($data['szDescription'])."',
                        NOW(),
                        '".(int)$data['iDebitCredit']."',
                        '2',
                        '".mysql_escape_custom($data['invoiceNumberBatchLabel'])."',
                        NOW(),
                        NOW(),
                        '".mysql_escape_custom($data['fLabelFeeUSD'])."',
                        '".mysql_escape_custom($data['fTotalHandlingFeeUSD'])."',
                        '".mysql_escape_custom($data['fExchangeRate'])."' 
                    )	
                ";
                //echo $query;
                if($result=$this->exeSQL($query))
                {
                        return true;
                }
                else
                {
                        return false;
                }
            } 
	}
        
        
        function updateCourierLabelFeeForwarderPaymentStatus($transArr,$invoiceNumber)
	{
            if($transArr!='')
            { 
                $query="
                    SELECT 
                        idBooking
                    FROM 
                        ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                    WHERE         
                        id IN (".mysql_escape_custom(trim($transArr)).")            
                ";
		//echo $query;
		if($result = $this->exeSQL( $query ))
		{
                    if ($this->getRowCnt() > 0)
                    {	
                        $amountValueArr=array();
                        while($row=$this->getAssoc($result))
                        {
                            $query1="
                                UPDATE
                                    ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__."
                                SET
                                    szCourierInvoiceNumber='".mysql_escape_custom($invoiceNumber)."'
                                WHERE
                                    idBooking = (".mysql_escape_custom(trim($row['idBooking'])).")
                                AND
                                    iCourierAgreementIncluded='0'
                            ";  
                           //echo $query1;
                            $result1 = $this->exeSQL( $query1 );
                        }
                    }
                }
                
            }
	}
        
        function updateHandlingFeePaymentStatus($transArr,$invoiceNumber)
	{
            if($transArr!='')
            { 
                $query="
                    SELECT 
                        idBooking
                    FROM 
                        ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                    WHERE         
                        id IN (".mysql_escape_custom(trim($transArr)).")            
                ";
		//echo $query;
		if($result = $this->exeSQL( $query ))
		{
                    if ($this->getRowCnt() > 0)
                    {	
                        $amountValueArr=array();
                        while($row=$this->getAssoc($result))
                        {
                            $query1="
                                UPDATE
                                    ".__DBC_SCHEMATA_BOOKING_HANDLING_FEE__."
                                SET
                                    szHandlingInvoice = '".mysql_escape_custom($invoiceNumber)."'
                                WHERE
                                    idBooking = (".mysql_escape_custom(trim($row['idBooking'])).") 
                            ";  
                           //echo $query1;
                            $result1 = $this->exeSQL( $query1 );
                        }
                    }
                }
                
            }
	}
        
        function updateCountryISO3Code()
        {
            $CountryListJson = '{"BD": "BGD", "BE": "BEL", "BF": "BFA", "BG": "BGR", "BA": "BIH", "BB": "BRB", "WF": "WLF", "BL": "BLM", "BM": "BMU", "BN": "BRN", "BO": "BOL", "BH": "BHR", "BI": "BDI", "BJ": "BEN", "BT": "BTN", "JM": "JAM", "BV": "BVT", "BW": "BWA", "WS": "WSM", "BQ": "BES", "BR": "BRA", "BS": "BHS", "JE": "JEY", "BY": "BLR", "BZ": "BLZ", "RU": "RUS", "RW": "RWA", "RS": "SRB", "TL": "TLS", "RE": "REU", "TM": "TKM", "TJ": "TJK", "RO": "ROU", "TK": "TKL", "GW": "GNB", "GU": "GUM", "GT": "GTM", "GS": "SGS", "GR": "GRC", "GQ": "GNQ", "GP": "GLP", "JP": "JPN", "GY": "GUY", "GG": "GGY", "GF": "GUF", "GE": "GEO", "GD": "GRD", "GB": "GBR", "GA": "GAB", "SV": "SLV", "GN": "GIN", "GM": "GMB", "GL": "GRL", "GI": "GIB", "GH": "GHA", "OM": "OMN", "TN": "TUN", "JO": "JOR", "HR": "HRV", "HT": "HTI", "HU": "HUN", "HK": "HKG", "HN": "HND", "HM": "HMD", "VE": "VEN", "PR": "PRI", "PS": "PSE", "PW": "PLW", "PT": "PRT", "SJ": "SJM", "PY": "PRY", "IQ": "IRQ", "PA": "PAN", "PF": "PYF", "PG": "PNG", "PE": "PER", "PK": "PAK", "PH": "PHL", "PN": "PCN", "PL": "POL", "PM": "SPM", "ZM": "ZMB", "EH": "ESH", "EE": "EST", "EG": "EGY", "ZA": "ZAF", "EC": "ECU", "IT": "ITA", "VN": "VNM", "SB": "SLB", "ET": "ETH", "SO": "SOM", "ZW": "ZWE", "SA": "SAU", "ES": "ESP", "ER": "ERI", "ME": "MNE", "MD": "MDA", "MG": "MDG", "MF": "MAF", "MA": "MAR", "MC": "MCO", "UZ": "UZB", "MM": "MMR", "ML": "MLI", "MO": "MAC", "MN": "MNG", "MH": "MHL", "MK": "MKD", "MU": "MUS", "MT": "MLT", "MW": "MWI", "MV": "MDV", "MQ": "MTQ", "MP": "MNP", "MS": "MSR", "MR": "MRT", "IM": "IMN", "UG": "UGA", "TZ": "TZA", "MY": "MYS", "MX": "MEX", "IL": "ISR", "FR": "FRA", "IO": "IOT", "SH": "SHN", "FI": "FIN", "FJ": "FJI", "FK": "FLK", "FM": "FSM", "FO": "FRO", "NI": "NIC", "NL": "NLD", "NO": "NOR", "NA": "NAM", "VU": "VUT", "NC": "NCL", "NE": "NER", "NF": "NFK", "NG": "NGA", "NZ": "NZL", "NP": "NPL", "NR": "NRU", "NU": "NIU", "CK": "COK", "XK": "XKX", "CI": "CIV", "CH": "CHE", "CO": "COL", "CN": "CHN", "CM": "CMR", "CL": "CHL", "CC": "CCK", "CA": "CAN", "CG": "COG", "CF": "CAF", "CD": "COD", "CZ": "CZE", "CY": "CYP", "CX": "CXR", "CR": "CRI", "CW": "CUW", "CV": "CPV", "CU": "CUB", "SZ": "SWZ", "SY": "SYR", "SX": "SXM", "KG": "KGZ", "KE": "KEN", "SS": "SSD", "SR": "SUR", "KI": "KIR", "KH": "KHM", "KN": "KNA", "KM": "COM", "ST": "STP", "SK": "SVK", "KR": "KOR", "SI": "SVN", "KP": "PRK", "KW": "KWT", "SN": "SEN", "SM": "SMR", "SL": "SLE", "SC": "SYC", "KZ": "KAZ", "KY": "CYM", "SG": "SGP", "SE": "SWE", "SD": "SDN", "DO": "DOM", "DM": "DMA", "DJ": "DJI", "DK": "DNK", "VG": "VGB", "DE": "DEU", "YE": "YEM", "DZ": "DZA", "US": "USA", "UY": "URY", "YT": "MYT", "UM": "UMI", "LB": "LBN", "LC": "LCA", "LA": "LAO", "TV": "TUV", "TW": "TWN", "TT": "TTO", "TR": "TUR", "LK": "LKA", "LI": "LIE", "LV": "LVA", "TO": "TON", "LT": "LTU", "LU": "LUX", "LR": "LBR", "LS": "LSO", "TH": "THA", "TF": "ATF", "TG": "TGO", "TD": "TCD", "TC": "TCA", "LY": "LBY", "VA": "VAT", "VC": "VCT", "AE": "ARE", "AD": "AND", "AG": "ATG", "AF": "AFG", "AI": "AIA", "VI": "VIR", "IS": "ISL", "IR": "IRN", "AM": "ARM", "AL": "ALB", "AO": "AGO", "AQ": "ATA", "AS": "ASM", "AR": "ARG", "AU": "AUS", "AT": "AUT", "AW": "ABW", "IN": "IND", "AX": "ALA", "AZ": "AZE", "IE": "IRL", "ID": "IDN", "UA": "UKR", "QA": "QAT", "MZ": "MOZ"}';
            $CountryListJsonAry = json_decode($CountryListJson);
              
           // print_R($CountryListJsonAry);
            if(!empty($CountryListJsonAry))
            {
                foreach($CountryListJsonAry as $szCountryISO=>$szCountryISO3)
                {
                    $query1="
                        UPDATE
                            ".__DBC_SCHEMATA_COUNTRY__."
                        SET
                            szCountryISO3Code = '".mysql_escape_custom($szCountryISO3)."'
                        WHERE
                            szCountryISO = '".mysql_escape_custom(trim($szCountryISO))."'
                    ";  
                    echo $query1."<br><br>"; 
                    //die;
                    if($result1 = $this->exeSQL($query1))
                    {
                        echo "<br>SUCCESS<br>";
                    }
                    else
                    {
                        echo "<br><br>ERROR: <br><br>".$szCountryISO." - ".$szCountryISO3;
                    }
                }
            }
        }
        
        function snoozeBooking($idBooking,$dtSnoozeDate)
        {
            if(!empty($dtSnoozeDate) && $idBooking>0)
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_BOOKING__."
                    SET
                        dtSnooze='".mysql_escape_custom($dtSnoozeDate)."'
                    WHERE
                        id='".mysql_escape_custom($idBooking)."'                    
                "; 
                if($result = $this->exeSQL( $query ))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        function insertSnoozeData($idBooking,$dtSnooze,$dtSnoozeTime,$szFlag='')
        {  
            if(!empty($dtSnooze) && $idBooking>0)
            {
                $dtSnoozeArr=  explode("/", $dtSnooze);
                $snoozeDate=$dtSnoozeArr[2]."-".$dtSnoozeArr[1]."-".$dtSnoozeArr[0]." ".$dtSnoozeTime;                
                
                $dtReminderDate = $snoozeDate; 
                if(empty($_SESSION['szCustomerTimeZoneGlobal']))
                {
                    $_SESSION['szCustomerTimeZoneGlobal'] = 'Europe/Copenhagen' ;
                }
                $szServerTimeZone = date_default_timezone_get(); 
                if(empty($szServerTimeZone))
                {
                    $szServerTimeZone = 'Europe/London';
                }
                $date = new DateTime($dtReminderDate,new DateTimeZone($_SESSION['szCustomerTimeZoneGlobal']));   
                $dtReminderDate = $date->format('Y-m-d H:i:s');

                $date->setTimezone(new DateTimeZone($szServerTimeZone));  
                $dtReminderDate = $date->format('Y-m-d H:i:s');   

                $dtCurrentDate = date('Y-m-d H:i:s');  
                $date_1 = new DateTime($dtCurrentDate,new DateTimeZone($_SESSION['szCustomerTimeZoneGlobal']));   
                $date_1->setTimezone(new DateTimeZone($szServerTimeZone));
                $dtCurrentDateTime = $date_1->format('Y-m-d H:i:s');

                $dtRemindDateTime = strtotime($dtReminderDate) ;
                 if(date('I')==1)
                {  
                    //Our server's My sql time is 1 hours behind so that's why added 2 hours to sent date 
                    //$dtReminderDate = date('Y-m-d H:i:s',strtotime("-1 HOUR",strtotime($dtReminderDate)));   
                }
                 
                $this->snoozeBooking($idBooking,$dtReminderDate);
                
                if($szFlag=='LABELNOTSENT')
                {
                    $kBooking = new cBooking();
                    $kBooking->load($idBooking);


                    $kBooking_new = new cBooking();
                    $kBooking_new->loadFile($kBooking->idFile);  

                    $fileLogsAry = array(); 
                    $fileLogsAry['szTransportecaTask'] = "";   
                    if(!empty($kBooking_new->szTaskStatus))
                    {
                        $fileLogsAry['szLastTaskStatus'] = $kBooking_new->szTaskStatus;
                    } 
                    $fileLogsAry['iSnoozeAdded'] = 1;
                    $fileLogsAry['dtSnoozeDate'] = $dtReminderDate;

                    $file_log_query = '';
                    if(!empty($fileLogsAry))
                    {
                        foreach($fileLogsAry as $key=>$value)
                        {
                            $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                        }
                    } 
                    $file_log_query = rtrim($file_log_query,",");   
                    //echo $file_log_query."file_log_query";
                    $kBooking->updateBookingFileDetails($file_log_query,$kBooking->idFile);
                    
                    if($kBooking_new->szTaskStatus!='')
                    {
                        $fileQueueArr=array();                        
                        $fileQueueArr['szTaskStatus']=$kBooking_new->szTaskStatus;
                        $fileQueueArr['idBookingFile']=$kBooking->idFile;
                        $fileQueueArr['szDeveloperNotes']="Snoozing the File From insertSnoozeData function";
                        $kPendingTray = new cPendingTray();
                        $kPendingTray->addFileTaskQueue($fileQueueArr);
                    }
                }
            } 
        }
        
        function selectSnoozeData()
        {
            $query="
                SELECT
                    szSnoozeFlag,
                    dtSnooze
                FROM
                    ".__DBC_SCHEMATA_SNOOZE_DATA__."
            ";
            if($result = $this->exeSQL( $query ))
            {
                if ($this->getRowCnt() > 0)
                {
                    while($row=$this->getAssoc($result))
                    {
                        $resArr[$row['szSnoozeFlag']]=$row['dtSnooze'];
                    }
                    
                    return $resArr;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                return array();
            }
        }
        
        function getMenuPagePermmissionList($iPageType=0)
        {
            $queryWhere='';
            if((int)$iPageType>0)
            {
                $queryWhere='WHERE
                    iType = "'.(int)$iPageType.'"
                        ';
            }
            $query="
                SELECT
                    *
                FROM
                    ".__DBC_SCHEMATA_PAGE_PERMISSION__."
                $queryWhere        
                ORDER BY
                    iType ASC
            "
            ;
            //echo $query;
            if($result = $this->exeSQL( $query ))
            {
                if ($this->getRowCnt() > 0)
                {
                    while($row=$this->getAssoc($result))
                    {
                        if((int)$iPageType>0)
                        {
                            $resArr[$row['szPageName']]=$row;
                        }
                        else
                        {
                            if($row['iType']==1)
                            {
                                $resArr[$row['id']][0]=$row;                        
                            }
                            else if($row['iType']==2)
                            {
                                $ctr=count($resArr[$row['idMenu']]);
                                $resArr[$row['idMenu']][$ctr]=$row;
                            }
                        }
                    }
                   // print_r($resArr);
                    return $resArr;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                return array();
            }
        }
        
        
        function getAllPermissionByRoles($szRoleType,$pagePermission='',$flag=false)
        { 
            $queryWhere='';
            if($pagePermission!='')
            {
                $queryWhere="
                    AND
                       pp.szPageName='".  mysql_escape_custom($pagePermission)."'
                ";
            }
            $query="
		SELECT 
                    id
		FROM
                    ".__DBC_SCHEMATA_ADMIN_ROLES__."
                WHERE
                    szRoleCode='".  mysql_escape_custom($szRoleType)."'
            "; 
            if($result = $this->exeSQL( $query ))
            { 
                $adminRolesAry = array();
                $row=$this->getAssoc($result);
                $idRole=$row['id'];
                if((int)$idRole>0)
                {
                    $query="
                        SELECT 
                            pp.szPageName,
                            pp.iType,
                            arpm.id,
                            pp.id AS idPermission
                        FROM
                            ".__DBC_SCHEMATA_PAGE_PERMISSION__." AS pp
                        INNER JOIN
                            ".__DBC_SCHEMATA_ADMIN_ROLES_PERMISSION_MAPPING__." AS arpm
                        ON        
                            arpm.idPage = pp.id       
                        WHERE
                            arpm.idRole='".(int)$idRole."'
                        AND
                            pp.iType
                        $queryWhere        
                    ";
                    if($result = $this->exeSQL( $query ))
                    {
                        if ($this->getRowCnt() > 0)
                        {
                            while($row=$this->getAssoc($result))
                            {
                                if($flag)
                                {
                                    $resArr[]=$row['idPermission'];
                                }
                                else
                                {
                                    $resArr[$row['iType']][$row['szPageName']]=$row['id'];
                                }
                            }

                            return $resArr;
                        }
                        else
                        {
                            return array();
                        }
                    }
                    else
                    {
                        return array();
                    }
                }
                else
                {
                    return array();
                }
                 
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
        }
        
        function insertUpdatedRolePermission($idRole,$szPermissionArr)
        {
            $queryDel="
                DELETE FROM
                    ".__DBC_SCHEMATA_ADMIN_ROLES_PERMISSION_MAPPING__."
                WHERE
                    idRole='".(int)$idRole."'
            ";
            $result = $this->exeSQL($queryDel);
            
            if(!empty($szPermissionArr))
            {
                foreach($szPermissionArr as $szPermissionArrs)
                {
                    $queryInsert="
                        INSERT INTO
                            ".__DBC_SCHEMATA_ADMIN_ROLES_PERMISSION_MAPPING__."
                        (
                            idRole,
                            idPage,
                            dtCreatedOn,
                            dtUpdatedOn
                        )
                            VALUES
                        (
                            '".(int)$idRole."',
                            '".(int)$szPermissionArrs."',
                            NOW(),
                            NOW()                                 
                        )
                    ";
                    $result = $this->exeSQL($queryInsert);
                }
            }
        }
        
        
        function addNewAdminRole($data)
        {
            $this->set_szRoleType(sanitize_all_html_input(trim($data['szRoleType'])));
            
            if($this->error==true)
            {
                return false;
            }            
            
            $query="
                SELECT
                    id
                FROM
                   ". __DBC_SCHEMATA_ADMIN_ROLES__."
                WHERE
                    szFriendlyName='".mysql_escape_custom($this->szRoleType)."'
               ";
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $this->addError('szRoleType','Role type already exists.');
                    return false;
                }
            }
            
            $szRoleType=  str_replace(" ","_", $this->szRoleType);
            $query="
                INSERT INTO
                    ". __DBC_SCHEMATA_ADMIN_ROLES__."
                (
                    szRoleCode,
                    szFriendlyName
                )
                    VALUES
                (
                    '".mysql_escape_custom($szRoleType)."',
                    '".mysql_escape_custom($this->szRoleType)."'    
                )
            ";
            if($result=$this->exeSQL($query))
            {
                return true;
            }
        }
        
        function checkRoleTypeAssign($idRole)
        {
            $query="
                SELECT 
                    m.id
                FROM 
                    ".__DBC_SCHEMATA_MANAGEMENT__." AS m
                INNER JOIN
                    ". __DBC_SCHEMATA_ADMIN_ROLES__." AS rt
                ON
                    rt.szRoleCode=m.szProfileType
                WHERE 
                      rt.id= '".(int)$idRole."'	
                ";
           
            if($result = $this->exeSQL( $query ))
            {
                if ($this->getRowCnt() > 0)
                {
                    return true;
                }
            }
            
            return false;
        }
        
        function deleteRoleType($idRole)
        {
            $query="
                DELETE FROM
                    ". __DBC_SCHEMATA_ADMIN_ROLES__."
                WHERE
                    id= '".(int)$idRole."'
            ";
            if($result = $this->exeSQL( $query ))
            {
                return true;
            }
        }
        
        function getOtherLanguageName($idCountry,$flag=false)
        {
            $query="
                SELECT
                    cnm.szName,
                    l.szLanguageCode,
                    l.id
                FROM    
                    ".__DBC_SCHEMATA_COUNTRY_NAME_MAPPING__." AS cnm
                INNER JOIN
                    ".__DBC_SCHEMATA_LANGUAGE__." AS l
                ON
                    l.id=cnm.idLanguage
                WHERE
                    cnm.idCountry='".(int)$idCountry."'
                AND
                    l.id<>'".(int)__LANGUAGE_ID_ENGLISH__."'
                ";
            //echo $query;
                if($result = $this->exeSQL( $query ))
                {
                    if ($this->getRowCnt() > 0)
                    {
                        if($flag)
                        {
                           while($row=$this->getAssoc($result))
                           {
                               $resArr['szCountryDanishName_'.$row['id']]=$row['szName'];
                           } 
                           
                           return $resArr;
                        }
                        else
                        {
                            while($row=$this->getAssoc($result))
                            {
                                $resArr[]=$row['szName']."(".  strtoupper($row['szLanguageCode']).")";
                            } 

                            if(!empty($resArr))
                            {
                                $otherCountryStr=implode("<br />",$resArr);
                            }
                            return $otherCountryStr;
                        }
                    }
                    else
                    {
                        return '';
                    }
                }
                else
                {
                    return '';
                }
        }
        
        
        function getListPaypalCoutries($idCountry=0)
        {
            $queryWhere='';
            $innerJOinCol='';
            $innerJOin='';
            $orderBY='';
            if((int)$idCountry>0)
            {
                $queryWhere .="WHERE pc.idCountry='".(int)$idCountry."'";
            }
            else
            {
                $innerJOinCol=',c.szCountryName';
                $innerJOin="INNER JOIN ".__DBC_SCHEMATA_COUNTRY__." AS c ON pc.idCountry=c.id";
                $orderBY="ORDER BY c.szCountryName ASC";
            }
            $query="
                SELECT
                    pc.id,
                    pc.idCountry
                    $innerJOinCol
                FROM
                    ".__DBC_SCHEMATA_PAYPAL_COUNTRIES__." AS pc
                $innerJOin         
                $queryWhere
                $orderBY   
            ";
           //echo $query;
            if($result = $this->exeSQL( $query ))
            {
                if ($this->getRowCnt() > 0)
                {
                    if((int)$idCountry>0)
                    {
                        return false;
                    }
                    else
                    {
                        while($row=$this->getAssoc($result))
                        {
                            $resArr[]=$row;
                        }
                        return $resArr;
                    }
                }
                else
                {
                    if((int)$idCountry>0)
                    {
                        return true;
                    }
                    else
                    {
                        return array();
                    }
                }
            }
            else
            {
                if((int)$idCountry>0)
                {
                    return true;
                }
                else
                {
                    return array();
                }
            }
                
        }
        
        function addToPaypalCountryList($idCountry)
        {
            $query="
                INSERT INTO
                    ".__DBC_SCHEMATA_PAYPAL_COUNTRIES__."
                (
                    idCountry
                )
                    VALUES
                (
                    '".(int)$idCountry."'
                )
            ";
            $result = $this->exeSQL( $query );
        }
        
        function removeFromPaypalCountryList($idCountry)
        {
            $query="
                DELETE FROM
                    ".__DBC_SCHEMATA_PAYPAL_COUNTRIES__."
                WHERE
                    idCountry='".(int)$idCountry."'
            ";
            $result = $this->exeSQL( $query );
        }
        
        
        function showBookingConfirmedInvoiceList($data)
	{
            if(!empty($data))
            {
                $kBooking = new cBooking();
                if(!empty($data['dtFromDate']))
                {
                        $from_date = $kBooking->convert_date($data['dtFromDate']);
                        $from_dateStr=  str_replace("/","-", $from_date);
                        
                        $query_and .= " AND DATE(b.dtPaymentConfirmed) >= '".mysql_escape_custom(trim($from_dateStr))."'" ;
                }
                if(!empty($data['dtToDate']))
                {
                        $to_date = $kBooking->convert_date($data['dtToDate']);
                        $to_dateStr=  str_replace("/","-", $to_date);
                        $query_and .= " AND DATE(b.dtPaymentConfirmed) <= '".mysql_escape_custom(trim($to_dateStr))."'" ;
                }
                
                
                
                if(isset($from_date) && isset($to_date) )
                {
                    if(strtotime($from_date) > strtotime($to_date))
                    {
                            $this->addError( 'fromTo' , t($this->t_base.'Error/from_grt_than_to_date') );	
                            return false;
                    }
                }
            }
            
            $query="
                SELECT 
                    bk.id AS idBooking
                FROM
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS b
                INNER JOIN
                    ".__DBC_SCHEMATA_FROWARDER__." AS f	
                ON
                    (f.id=b.idForwarder)
                INNER JOIN
                    ".__DBC_SCHEMATA_BOOKING__." AS bk	
                ON
                    (bk.id=b.idBooking)	
                WHERE 
                    iStatus<>'0' 
                AND
                    iDebitCredit IN ('1')
                AND
                    LOWER(f.szDisplayName) LIKE '%transporteca%' 
                $query_and    
                ORDER BY bk.dtBookingConfirmed DESC	
            ";
            //echo $query;
            if($result = $this->exeSQL( $query ))
            {
                if ($this->getRowCnt() > 0)
                {	
                    $manageSettledVar=array();
                    while($row=$this->getAssoc($result))
                    {	
                        $manageSettledVar[]=$row;
                    }
                    return $manageSettledVar;
                }
                else
                {
                    return array();
                }	
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
        
        
        function paymentOfForwardersTransactionDownload($idAdmin,$transporteca_invoice=false,$data)
	{
		if(!empty($idAdmin))
		{ 
                    
                    if(!empty($data))
                    {
                        $kBooking = new cBooking();
                        if(!empty($data['dtFromDate']))
                        {
                                $from_date = $kBooking->convert_date($data['dtFromDate']);
                                $from_dateStr=  str_replace("/","-", $from_date);

                                $query_and .= " DATE(t.dtPaymentConfirmed) >= '".mysql_escape_custom(trim($from_dateStr))."'" ;
                        }
                        if(!empty($data['dtToDate']))
                        {
                                $to_date = $kBooking->convert_date($data['dtToDate']);
                                $to_dateStr=  str_replace("/","-", $to_date);
                                $query_and .= " AND DATE(t.dtPaymentConfirmed) <= '".mysql_escape_custom(trim($to_dateStr))."'" ;
                        }



                        if(isset($from_date) && isset($to_date) )
                        {
                            if(strtotime($from_date) > strtotime($to_date))
                            {
                                    $this->addError( 'fromTo' , t($this->t_base.'Error/from_grt_than_to_date') );	
                                    return false;
                            }
                        }
                    }
			$query="
				SELECT
					t.id,
					t.idForwarder,
					t.szInvoice,
					t.szBooking,
					t.dtPaymentConfirmed,
					t.fTotalPriceForwarderCurrency,
					t.szCurrency,
					f.szDisplayName,
					t.iBatchNo,
					t.iStatus,
					t.iDebitCredit,
					t.szForwarderCurrency
				FROM
					".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS t
				JOIN 
					".__DBC_SCHEMATA_FROWARDER__." AS f
				ON
					(t.idForwarder=f.id)
				WHERE 
                                    $query_and
				ORDER BY dtPaymentConfirmed
					DESC, t.szInvoice DESC
			";
                	//echo "<br>".$query."<br>";
//                        die;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{	
                                    $ctr=0;
                                    while($row=$this->getAssoc($result))
                                    {
                                            $payment[$ctr]=$row;
                                            $ctr++;
                                    } 
                                }
                                if($transporteca_invoice)
                                {
                                    $kBooking = new cBooking();
                                    $bookingInsuranceAry = $kBooking->getAllNewBookingWithInsurance(false,false,true,false,false,$data);

                                    if(!empty($bookingInsuranceAry))
                                    {
                                        foreach($bookingInsuranceAry as $bookingInsuranceArys)
                                        {
                                            if($bookingInsuranceArys['iInsuranceStatus']==__BOOKING_INSURANCE_STATUS_CANCELLED__)
                                            {
                                                $payment[$ctr]['dtPaymentConfirmed'] = $bookingInsuranceArys['dtInsuranceStatusUpdated'];
                                                $payment[$ctr]['szBooking'] = "Insurance credit note";
                                                $payment[$ctr]['idBooking'] = $bookingInsuranceArys['id'];
                                                $payment[$ctr]['szInvoice'] = $bookingInsuranceArys['szInsuranceCreditNoteInvoice'] ;
                                                $payment[$ctr]['fTotalPriceForwarderCurrency'] = $bookingInsuranceArys['fTotalInsuranceCostForBookingCustomerCurrency'];
                                                $payment[$ctr]['szCurrency'] = $bookingInsuranceArys['szCurrency'];
                                                $payment[$ctr]['szDisplayName'] = 'N/A';
                                                $payment[$ctr]['iDebitCredit'] = 7;
                                                $payment[$ctr]['iInsuranceStatus'] = $bookingInsuranceArys['iInsuranceStatus'] ;

                                                $ctr++;
                                                $payment[$ctr]['dtPaymentConfirmed'] = $bookingInsuranceArys['dtBookingConfirmed'];
                                                $payment[$ctr]['szBooking'] = "Insurance invoice";
                                                $payment[$ctr]['idBooking'] = $bookingInsuranceArys['id'];
                                                $payment[$ctr]['szInvoice'] = $bookingInsuranceArys['szInsuranceInvoice'] ;
                                                $payment[$ctr]['fTotalPriceForwarderCurrency'] = $bookingInsuranceArys['fTotalInsuranceCostForBookingCustomerCurrency'];
                                                $payment[$ctr]['szCurrency'] = $bookingInsuranceArys['szCurrency'];
                                                $payment[$ctr]['szDisplayName'] = 'N/A';
                                                $payment[$ctr]['iDebitCredit'] = 7;
                                                $payment[$ctr]['iInsuranceStatus'] = 1 ;  
                                            }
                                            else
                                            {
                                                $payment[$ctr]['dtPaymentConfirmed'] = $bookingInsuranceArys['dtBookingConfirmed'];
                                                $payment[$ctr]['szBooking'] = "Insurance invoice";
                                                $payment[$ctr]['idBooking'] = $bookingInsuranceArys['id'];
                                                $payment[$ctr]['szInvoice'] = $bookingInsuranceArys['szInsuranceInvoice'] ;
                                                $payment[$ctr]['fTotalPriceForwarderCurrency'] = $bookingInsuranceArys['fTotalInsuranceCostForBookingCustomerCurrency'];
                                                $payment[$ctr]['szCurrency'] = $bookingInsuranceArys['szCurrency'];
                                                $payment[$ctr]['szDisplayName'] = 'N/A';
                                                $payment[$ctr]['iDebitCredit'] = 7;
                                                $payment[$ctr]['iInsuranceStatus'] = $bookingInsuranceArys['iInsuranceStatus'] ;
                                            } 
                                            $ctr++;
                                        }
                                    }
                                } 
                                if(!empty($payment))
                                {
                                    $payment = sortArray($payment,'dtPaymentConfirmed','DESC','szInvoice','DESC',false,false,true);
                                    return $payment;
                                }
                                else
                                {
                                    return array();
                                }    
                                    
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
        
        
        function updateInvoiceNumber()
        {
            $query="
                SELECT 
                    bk.id,
                    bk.szInvoice,
                    f.szForwarderAlias
                FROM
                   ".__DBC_SCHEMATA_FROWARDER__." AS f	
                INNER JOIN
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS bk	
                ON
                    (f.id=bk.idForwarder)	
                WHERE 
                    bk.iDebitCredit='6'	
            ";
            echo $query."<br />";
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {	
                    $ctr=0;
                    while($row=$this->getAssoc($result))
                    {
                        $szForwarderAlias=$row['szForwarderAlias'];
                        $szInvoice = $row['szInvoice'];
                        $idFT = $row['id'];
                        $szInvoiceStr="T".$row['szForwarderAlias'];
                        $szInvoiceNew=substr_replace($szInvoice,$szInvoiceStr,0,3);
                        
                        echo $szInvoice."-------".$szInvoiceNew."<br /><br />";
                        
                       
                        $query2="
                            UPDATE
                                ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                            SET
                               szInvoice='".  mysql_escape_custom($szInvoiceNew)."',
                               szOldInvoice='".  mysql_escape_custom($szInvoice)."'
                        WHERE
                            id='".(int)$idFT."'
                        AND
                            iDebitCredit='6'
                        ";
                        echo $query2."<br />";
                       $result2=$this->exeSQL($query2);
                    }
                }
            }
        }
        
        
        function updateReferralFeeForForwarder()
	{
                       
            $query="
                SELECT 
                    ft.id AS idForwarderTrans,
                    b.id AS idBooking,
                    b.iHandlingFeeApplicable,
                    b.idQuotePricingDetails,
                    qp.fTotalHandlingFeeUSD,
                    qp.idCustomerCurrency,
                    qp.fTotalPriceCustomerCurrency,
                    qp.iManualFeeApplicable,
                    qp.idForwarderAccountCurrency,
                    qp.fTotalPriceForwarderCurrency,
                    qp.fForwarderCurrencyExchangeRate,
                    qp.idForwarderCurrency,
                    qp.fForwarderAccountCurrencyExchangeRate,
                    qp.fTotalVat,
                    qp.fReferalPercentage,
                    qp.fTotalVatCustomerCurrency,
                   (SELECT cb.iCourierAgreementIncluded FROM ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__. " cb WHERE cb.idBooking = b.id ) as iCourierAgreementIncluded 
                FROM 
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS ft
                INNER JOIN
                    ".__DBC_SCHEMATA_BOOKING__." AS b
                ON
                    b.id=ft.idBooking
                INNER JOIN
                    ".__DBC_SCHEMATA_QUOTE_PRICING_DETAILS__." AS qp
                ON
                    qp.id=b.idQuotePricingDetails    
                WHERE 
                    ft.iStatus='0'
                AND
                    iBatchNo = 0
                AND
                    iDebitCredit!='5'
                AND
                    b.iHandlingFeeApplicable='1' 
                ORDER BY
                    ft.dtCreditOn DESC
            ";
            //echo "<br/>".$query."<br />";
            if($result = $this->exeSQL( $query ))
            {
                if ($this->getRowCnt() > 0)
                {	
                    $manageVar=array();
                    while($row=$this->getAssoc($result))
                    {	
                        $manageVar[]=$row;
                    }
                    return $manageVar;
                }
                else
                {
                    return array();
                }	
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
        
        
        function updateBookingReferalFee($fReferralFeeUSDAmount,$fReferralFeeAmount,$idForwarderTrans,$idBooking,$vatpercentage,$fTotalPriceForReferrealFee)
        {
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_BOOKING__."
                SET
                    fReferalAmount='".(float)$fReferralFeeAmount."',
                    fReferalAmountUSD='".(float)$fReferralFeeUSDAmount."',
                    fVATPercentage='".(float)$vatpercentage."',
                    fTotalForwarderPriceWithoutHandlingFee ='".(float)$fTotalPriceForReferrealFee."'   
                WHERE
                    id='".(int)$idBooking."'
            ";
            echo $query."<br />";
            $result = $this->exeSQL( $query );
            
            
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                SET
                    fReferalAmount='".(float)$fReferralFeeAmount."'
                WHERE
                    id='".(int)$idForwarderTrans."'
                AND
                    idBooking = '".(int)$idBooking."'
            ";
            echo $query."<br />";
            $result = $this->exeSQL( $query );
        }
        
        
         function updateCMSSecton()
        {
            $query="
                SELECT
                    section_description,
                    id
                FROM
                    ".__DBC_SCHEMATA_CMS_MAPPING__."
                ";
             if($result = $this->exeSQL( $query ))
            {
                if ($this->getRowCnt() > 0)
                {	
                    while($row=$this->getAssoc($result))
                    {
                        
                        $section_description= $row['section_description'];
                        $brcount= substr_count($section_description, '<br>');
                        echo "Check Break Count ---".$brcount."<br />";
                        if((int)$brcount<=0)
                        {                       
                            echo "Replacing New Line Replace With Br ---<br />";
                            $replaceArr=array("\r\n");
                            $section_description = str_replace(PHP_EOL, "<br>", $section_description);
                        }
                        else
                        {
                            echo "Replacing New Line Replace With Blank ---<br />";
                            $replaceArr=array("\r\n");
                            $section_description = str_replace(PHP_EOL, "", $section_description);
                        }
                        $section_description = "<div align='left'>".$section_description."</div>";
                        
                            $queryUpdate="
                               UPDATE
                                   ".__DBC_SCHEMATA_CMS_MAPPING__."
                               SET
                                   section_description='".mysql_escape_custom($section_description)."'
                               WHERE
                                   id='".(int)$row['id']."'
                           ";
                           echo $query."<br />";
                           $result1 = $this->exeSQL( $queryUpdate );
                    }
                        //die();
                }
            }
            
        }
        
        
        function updateCMSSectonRemind()
        {
            $query="
                SELECT
                    section_description,
                    id
                FROM
                    ".__DBC_SCHEMATA_CMS_REMIND_MAPPING__."
                ";
             if($result = $this->exeSQL( $query ))
            {
                if ($this->getRowCnt() > 0)
                {	
                    while($row=$this->getAssoc($result))
                    {
                        
                        $section_description= $row['section_description'];
                        $brcount= substr_count($section_description, '<br>');
                        echo "Check Break Count ---".$brcount."<br />";
                        if((int)$brcount<=0)
                        {                       
                            echo "Replacing New Line Replace With Br ---<br />";
                            $replaceArr=array("\r\n");
                            $section_description = str_replace(PHP_EOL, "<br>", $section_description);
                        }
                        else
                        {
                            echo "Replacing New Line Replace With Blank ---<br />";
                            $replaceArr=array("\r\n");
                            $section_description = str_replace(PHP_EOL, "", $section_description);
                        }
                        $section_description = "<div align='left'>".$section_description."</div>";
                        
                            $query1="
                               UPDATE
                                   ".__DBC_SCHEMATA_CMS_REMIND_MAPPING__."
                               SET
                                   section_description='".mysql_escape_custom($section_description)."'
                               WHERE
                                   id='".(int)$row['id']."'
                           ";
                           //echo $query."<br />";
                           $result1 = $this->exeSQL( $query1 );
                        }
                        //die();
                    }
                }
            
        }
        
        function addUpdateContactProfileData($data)
        {
            $this->set_szEmail(sanitize_all_html_input(($data['szEmail'])));
            $this->set_szFirstName(sanitize_all_html_input(($data['szFirstName'])),false);
            $this->set_szLastName(sanitize_all_html_input(($data['szLastName'])),false);
            $this->set_idForwarder(sanitize_all_html_input(($data['idForwarder'])));
            $this->set_szPhoneNumber(sanitize_all_html_input(urldecode(base64_decode($data['szPhoneUpdate']))),false);
            $this->set_szMobile(sanitize_all_html_input(urldecode(base64_decode($data['szMobileUpdate']))),false);            
            $this->set_szResponsibility(sanitize_all_html_input(($data['szResponsibility'])),false);
            $this->set_idCountry(sanitize_all_html_input(($data['idCountry'])),false);
            $this->idForwarderContact = (int)$data['idForwarderContact'];
            
            //exit if error exist.
            if ($this->error === true)
            {
                return false;
            }
            
            if((int)$this->idForwarderContact>0)
            {
                $query="
                    UPDATE 
                        ".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
                    SET
                        idForwarder = '".(int)$this->idForwarder."',
                        szFirstName = '".mysql_escape_custom(trim($this->szFirstName))."',
                        szLastName = '".mysql_escape_custom(trim($this->szLastName))."',
                        idCountry = '".(int)$this->idCountry."',
                        szPhone = '".mysql_escape_custom(trim($this->szPhoneNumber))."',
                        szMobile = '".mysql_escape_custom(trim($this->szMobile))."',
                        szEmail = '".mysql_escape_custom(trim($this->szEmail))."',
                        szResponsibility = '".mysql_escape_custom(trim($this->szResponsibility))."',    
                        idForwarderContactRole = '7',
                        iActive = '1'
                    WHERE
                        id = '".(int)$this->idForwarderContact."'	
                ";
            }
            else
            {
                $query="
                    INSERT INTO 
                            ".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
                    (
                        idForwarder,
                        szFirstName,
                        szLastName,
                        idCountry,
                        szPhone,
                        szMobile,
                        szEmail,
                        szResponsibility,    
                        idForwarderContactRole,
                        iActive,
                        dtCreateOn
                    )
                    VALUES
                    (
                        '".(int)$this->idForwarder."',
                        '".mysql_escape_custom(trim($this->szFirstName))."',
                        '".mysql_escape_custom(trim($this->szLastName))."',
                        '".(int)$this->idCountry."',
                        '".mysql_escape_custom(trim($this->szPhoneNumber))."',
                        '".mysql_escape_custom(trim($this->szMobile))."',
                        '".mysql_escape_custom(trim($this->szEmail))."',
                        '".mysql_escape_custom(trim($this->szResponsibility))."',    
                        '7',
                        '1',
                        NOW()
                   )	
                ";
            }
            if($result=$this->exeSQL($query))
            {
                if((int)$this->idForwarderContact==0)
                {
                    $this->idForwarderContact=$this->iLastInsertID;
                }
                    return true;
            }
            else
            {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
            }           
            
        }
        
        
        function importForwarderContactData($data)
        {
            $query="
                INSERT INTO 
                        ".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
                (
                    idForwarder,
                    szFirstName,
                    szLastName,
                    idCountry,
                    szPhone,
                    szMobile,
                    szEmail,
                    szResponsibility,    
                    idForwarderContactRole,
                    iActive,
                    dtCreateOn
                )
                VALUES
                (
                    '".(int)$data['idForwarder']."',
                    '".mysql_escape_custom(trim($data['szFirstName']))."',
                    '".mysql_escape_custom(trim($data['szLastName']))."',
                    '".(int)$data['idCountry']."',
                    '".mysql_escape_custom(trim($data['szPhone']))."',
                    '".mysql_escape_custom(trim($data['szMobile']))."',
                    '".mysql_escape_custom(trim($data['szEmail']))."',
                    '".mysql_escape_custom(trim($data['szResponsibility']))."',    
                    '".(int)$data['idForwarderContactRole']."',
                    '".(int)$data['iActive']."',
                    NOW()
               )	
            ";
            echo $query."<br /><br />";
            $result = $this->exeSQL($query);
        }
        
        function updateCustomerServiceAgentOfManagement($idAdmin)
        {
            $query="
                SELECT
                    iCustomerServiceAgent
                FROM
                ".__DBC_SCHEMATA_MANAGEMENT__."
                WHERE
                        id='".(int)$idAdmin."'
                ";
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                if( $this->getRowCnt() > 0 )
                {	
                    $row=$this->getAssoc($result);        
                    $iCustomerServiceAgent=$row['iCustomerServiceAgent'];
                    
                    if($iCustomerServiceAgent==1)
                    {
                        $iCustomerServiceAgentNew=0;
                        $iCustomerServiceAgentNewText='No';
                    }
                    else
                    {
                        $iCustomerServiceAgentNew=1;
                        $iCustomerServiceAgentNewText='Yes';
                    }
                    $query="
                        UPDATE
                            ".__DBC_SCHEMATA_MANAGEMENT__."
                        SET
                            iCustomerServiceAgent='".(int)$iCustomerServiceAgentNew."'
                        WHERE
                            id='".(int)$idAdmin."'
                        ";

                        if( $result = $this->exeSQL( $query ))
                        {
                            return $iCustomerServiceAgentNewText;
                        }
                        else
                        {
                            $this->error = true;
                            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                            return false;
                        }      
                }
            }
        }

        function set_szMobile( $value,$flag=true )
	{   
            $this->szMobile = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szMobile", "Mobile", false, false, $flag );
	}
        function set_idCountry( $value,$flag=true )
	{   
            $this->idCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idCountry", "Country Id", false, false, $flag );
	}
        
        function set_szResponsibility( $value,$flag=true )
	{   
            $this->szResponsibility = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szResponsibility", "Responsibility", false, false, $flag );
	}
        
        function set_szRoleType( $value )
	{   
            $this->szRoleType = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szRoleType", "Role Type", false, false, true );
	}
		
	function set_iManageid($value)
	{
            $this->iId = $this->validateInput($value,__VLD_CASE_NUMERIC__,'iId',t( $this->t_base."fields/id" ),false, 255 ,true );
	}
	function set_szManageHeading($value)
	{
            $this->szHeading = $this->validateInput($value,__VLD_CASE_NAME__,'szHeading',t( $this->t_base."fields/heading" ),false, 255 ,true );
	}
	function set_iManageValue($value)
	{
            $this->iValue = $this->validateInput($value,__VLD_CASE_ANYTHING__,'iValue',t( $this->t_base."fields/value" ),false, false ,true );
	}
	function set_szEmail ( $value )
	{
            $this -> szEmail = $this->validateInput($value , __VLD_CASE_EMAIL__ , "szEmail" , t( $this->t_base."fields/email" ),false, 255 ,true );
	}
	function set_szPassword ( $value )
	{
            $this -> szPassword = $this->validateInput($value , __VLD_CASE_ANYTHING__ , "szPassword" , t( $this->t_base."fields/password" ),false, 255 ,true );
	}
	function set_id( $value )
	{   
            $this->id = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "id", t($this->t_base.'fields/customer_id'), false, false, true );
	}
	function set_szFirstName( $value ,$flag=true)
	{ 
            $this->szFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szFirstName", t($this->t_base.'fields/f_name'), false, 255, $flag );
	}
	function set_szLastName( $value ,$flag=true)
	{
            $this->szLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLastName", t($this->t_base.'fields/l_name'), false, 255, $flag );
	}
        function set_szTitle( $value )
	{
            $this->szTitle = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTitle", t($this->t_base.'fields/title'), false, 255, true );
	}
        
	function set_szOldEmail( $value )
	{
            $this->szOldEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szOldEmail", t($this->t_base.'fields/email'), false, 255, true );
	}
	function set_szOldPassword( $value )
	{
            $this->szOldPassword = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szOldPassword", t($this->t_base.'fields/current_password'), false, 255, true );
	}
	function set_szRetypePassword( $value )
	{
            $this->szRetypePassword = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szReTypePassword", t($this->t_base.'fields/re_password'), false, 255, true );
	}
	function set_szCurrency( $value )
	{
            $this->szCurrency = $this->validateInput( $value, __VLD_CASE_NAME__, "szCurrency", t($this->t_base.'fields/currency'), false, 255, true );
	}
	function set_iPricing( $value )
	{
            $this->iPricing = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iPricing", t($this->t_base.'fields/pricing'), false, 255, true );
	}
	function set_iBooking( $value )
	{
            $this->iBooking = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iBooking", t($this->t_base.'fields/booking'), false, 255, true );
	}
	function set_iSettling( $value )
	{
            $this->iSettling = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iSettling", t($this->t_base.'fields/settling'), false, 255, true );
	}
	function set_szFeed( $value )
	{
            $this->szFeed = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szFeed", t($this->t_base.'fields/feed'), false, 255, true );
	}
	function set_szBankName( $value,$fag=false )
	{ 
            $this->szBankName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szBankName", t($this->t_base.'fields/bank'), false, 255, $fag );
	}
	
	function set_szSortCode( $value,$fag=false )
	{
            $this->szSortCode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSortCode", t($this->t_base.'fields/sort_code'), false, 255, $fag );
	}
	function set_szAccountNumber( $value,$fag=false )
	{
            $this->szAccountNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAccountNumber", t($this->t_base.'fields/account_number'), false, 255, $fag );
	}
        function set_szIBANNumber( $value,$fag=false )
	{
            $this->szIBANNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szIBANNumber", t($this->t_base.'fields/iban_number'), false, 255, $fag );
	}
        
	function set_szSwiftNumber( $value,$fag=false )
	{
		$this->szSwiftNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSwiftNumber", t($this->t_base.'fields/swift'), false, 255, $fag );
	}
	function set_szNameOnAccount( $value,$fag=false )
	{
		$this->szNameOnAccount = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szNameOnAccount", t($this->t_base.'fields/account_name'), false, 255, $fag );
	}

	function set_szCountryName( $value )
	{
		$this->szCountryName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCountryName", t($this->t_base.'fields/CountryName'), false, 255, true );
	}
	
	function set_szCountryDanishName( $value,$language ,$szCountryDanishName)
	{
            $message=t($this->t_base.'fields/CountryName')."(".$language.")";
		$this->$szCountryDanishName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, $szCountryDanishName, $message, false, 255, true );
	}
	
	function set_szCountryISO( $value )
	{
		$this->szCountryISO = $this->validateInput( $value, __VLD_CASE_NAME__, "szCountryISO", t($this->t_base.'fields/iso'), false, 255, true );
	}
	function set_fStandardTruckRate( $value )
	{
		$this->fStandardTruckRate = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fStandardTruckRate", t($this->t_base.'fields/truckRates'), false, 255, true );
	}
	function set_iActiveFromDropdown( $value )
	{
            $this->iActiveFromDropdown = $this->validateInput( $value, __VLD_CASE_BOOL__, "iActiveFromDropdown", t($this->t_base.'fields/fromDropDown'), false, 255, true );
	}
	function set_iActiveToDropdown( $value )
	{
            $this->iActiveToDropdown = $this->validateInput( $value, __VLD_CASE_BOOL__, "iActiveToDropdown", t($this->t_base.'fields/toDropDown'), false, 255, true );
	}
        
	function set_szHeading( $value )
	{
		$this->szHeading = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szHeading", t($this->t_base.'fields/heading'), false, 255, true );
	}
	function set_szDescription( $value )
	{
		$this->szDescription = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDescription", t($this->t_base.'fields/description'), false, false, true );
	}
	function set_szNewFirstName($value)
	{
		$this->szNewFirstName = $this->validateInput($value,__VLD_CASE_NAME__,'szNewFirstName',t( $this->t_base."fields/f_name" ),false, 255 ,true );
	}
        
        function set_szCapsule($value)
	{
            $this->szCapsule = $this->validateInput($value,__VLD_CASE_NAME__,'szCapsule',t( $this->t_base."fields/capsule" ),false, 255 ,false );
	}
        
	function set_szNewLastName($value)
	{
		$this->szNewLastName = $this->validateInput($value,__VLD_CASE_NAME__,'szNewLastName',t( $this->t_base."fields/l_name" ),false, 255 ,true );
	}
	function set_szNewEmail($value)
	{
		$this->szNewEmail = $this->validateInput($value,__VLD_CASE_EMAIL__,'szNewEmail',t( $this->t_base."fields/email" ),false, 255 ,true );
	}
	function set_szNewPassword($value)
	{
		$this->szNewPassword = $this->validateInput($value,__VLD_CASE_ANYTHING__,'szNewPassword',t( $this->t_base."fields/password" ),false, 255 ,true );
	}
	function set_szComment($value)
	{
		$this->szComment = $this->validateInput($value,__VLD_CASE_ANYTHING__,'szComment',t( $this->t_base."fields/comment" ),false, false ,true );
	}
	function set_iActive( $value )
	{
            $this->iActive = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iActive", t($this->t_base.'fields/iActive'), false, false, true );
	}
	
	function set_szSubject( $value )
	{
		$this->szSubject = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSubject", t($this->t_base.'fields/subject'), false, 255, true );
	}
	function set_szTemplateSubject( $value )
	{
		$this->szTemplateSubject = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSubject", t($this->t_base.'fields/subject'), false, 255, false );
	}
	function set_szMessage( $value )
	{
		$this->szMessage = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szMessage", t($this->t_base.'fields/message'), false, false, true );
	}
	function set_iAllAccess( $value )
	{
		$this->iAllAccess = $this->validateInput( $value, __VLD_CASE_BOOL__, "iAllAccess", t($this->t_base.'fields/iAllAccess'), false, 1, true );
	}
	function set_iManageidArr($value)
	{
		$this->iIdArr[] = $this->validateInput($value,__VLD_CASE_NUMERIC__,'iId',t( $this->t_base."fields/all" ),false, false ,true );
	}
	function set_szManageHeadingArr($value)
	{
		$this->szHeadingArr[] = $this->validateInput($value,__VLD_CASE_NAME__,'all',t( $this->t_base."fields/all" ),false, 255 ,true );
	}
	function set_iManageValueArr($value)
	{	
		$this->iValueArr[] = $this->validateInput($value,__VLD_CASE_ANYTHING__,'all',t( $this->t_base."fields/all" ),false, false ,true );
	}
	function set_iAuto($value)
	{	
		$this->iAuto = $this->validateInput($value,__VLD_CASE_ANYTHING__,'iAuto',t( $this->t_base."fields/iAuto" ),false, false ,true );
	}
	
	function set_iSmallCountry($value)
	{	
		$this->iSmallCountry = $this->validateInput($value,__VLD_CASE_ANYTHING__,'iSmallCountry',t( $this->t_base."fields/small_country" ),false, false ,false );
	}
	
	function set_szUrl($value)
	{	
		$this->szUrl = $this->validateInput($value,__VLD_CASE_URL__,'szUrl',t( $this->t_base."fields/szUrl" ),false, false ,true );
	}
	function set_dtValidFrom( $value )
	{   
		$this->dtValidFrom = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "dtValidFrom", t($this->t_base.'fields/validity')." ".t($this->t_base.'fields/from'), false, false, true );
	}
	function set_dtExpiry( $value )
	{   
		$this->dtExpiry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "dtExpiry", t($this->t_base.'fields/validity')." ".t($this->t_base.'fields/to'), false, false, true );
	}	
	function set_searchField( $value )
	{   
		$this->searchField = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "searchField", t($this->t_base.'fields/searchField')." ".t($this->t_base.'fields/to'), false, false, false );
	}
	function set_idOriginCountry($value)
	{
		$this->idOriginCountry = $this->validateInput($value,__VLD_CASE_NUMERIC__,'idOriginCountry',t( $this->t_base."fields/idOriginCountry" ),false, 255 ,false );
	}
	function set_idDestinationCountry($value)
	{
		$this->idDestinationCountry = $this->validateInput($value,__VLD_CASE_NUMERIC__,'idDestinationCountry',t( $this->t_base."fields/idDestinationCountry" ),false, 255 ,false );
	}
	function set_idForwarder($value)
	{
		$this->idForwarder = $this->validateInput($value,__VLD_CASE_NUMERIC__,'idForwarder',t( $this->t_base."fields/idForwarder" ),false, 255 ,false );
	}	
	function set_szLatitude($value)
	{
		$this->szLatitude = $this->validateInput($value,__VLD_CASE_ANYTHING__,'szLatitude',t( $this->t_base."fields/szLatitude" ),false, 255 ,true );
	}
	function set_szLongitude($value)
	{
		$this->szLongitude = $this->validateInput($value,__VLD_CASE_ANYTHING__,'szLongitude',t( $this->t_base."fields/szLongitude" ),false, 255 ,true );
	}
        function set_fMaxFobDistance($value)
	{
            $this->fMaxFobDistance = $this->validateInput($value,__VLD_CASE_NUMERIC__,'fMaxFobDistance',t( $this->t_base."fields/fMaxFobDistance" ),false, false ,false );
	}
        
	function set_iLanguage( $value )
	{
		$this->iLanguage = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iLanguage", t($this->t_base.'fields/language'), false, false, true );
	}
        function set_idInternationalDialCode( $value )
	{
            $this->idInternationalDialCode = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idInternationalDialCode", t($this->t_base.'fields/dial_code'), false, false, true );
	}
        function set_szPhoneNumber( $value , $flag=true)
	{
            $this->szPhoneNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPhoneNumber", t($this->t_base.'fields/phone'), false, false, $flag );
	}
        function set_szRegionName( $value )
	{
            $this->szRegionName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szRegionName", t($this->t_base.'fields/region_name'), false, false, true );
	}
        function set_szRegionShortName( $value )
	{
            $this->szRegionShortName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szRegionShortName", t($this->t_base.'fields/region_short_name'), false, false, false );
	}
	
 	function set_fVATRate($value)
	{
            $this->fVATRate = $this->validateInput($value,__VLD_CASE_NUMERIC__,'fVATRate',t( $this->t_base."fields/fVATRate" ),false, false ,false );
	}
        function set_szVATRegistration( $value )
	{
            $this->szVATRegistration = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szVATRegistration", t($this->t_base.'fields/szVATRegistration'), false, false, false );
	}
        
        function set_iDefaultCurrency($value)
	{
            $this->iDefaultCurrency = $this->validateInput($value,__VLD_CASE_NUMERIC__,'iDefaultCurrency',t( $this->t_base."fields/default_currency" ),false, false ,false );
	}
        function set_iDefaultLanguage($value)
	{
            $this->iDefaultLanguage = $this->validateInput($value,__VLD_CASE_NUMERIC__,'iDefaultLanguage',t( $this->t_base."fields/default_currency" ),false, false ,false );
	} 
        function set_szProfileType($value)
	{
            $this->szProfileType = $this->validateInput($value,__VLD_CASE_ANYTHING__,'szProfileType',t( $this->t_base."fields/profile_type" ),false, false ,true);
	}
        
        function set_szDefaultCourierPostcode($value)
	{
            $this->szDefaultCourierPostcode = $this->validateInput($value,__VLD_CASE_ANYTHING__,'szDefaultCourierPostcode',t( $this->t_base."fields/szDefaultCourierPostcode" ),false, false ,false);
	}
        
        function set_szDefaultCourierCity($value)
	{
            $this->szDefaultCourierCity = $this->validateInput($value,__VLD_CASE_ANYTHING__,'szDefaultCourierCity',t( $this->t_base."fields/szDefaultCourierCity" ),false, false ,false);
	}
        
        
        
}?>