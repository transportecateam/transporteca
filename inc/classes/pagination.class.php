<?php
/**
 * This file contains the pagination functionality.
 * 
 * pagination.php
 * @copyright Copyright (C) 2009 Cardrunners, LLC
 * @author Abhi
 * @package HM Store Development
 */

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
class Pagination {
	var $php_self;
	var $rows_per_page = 10; //Number of records to display per page
	var $total_rows = 0; //Total number of rows returned by the query
	var $links_per_page = 5; //Number of links to display per page
	var $append = ""; //Paremeters to append to pagination links
	var $arr = "";
	var $debug = false;
	var $conn = false;
	var $page = 1;
	var $max_pages = 0;
	var $offset = 0;
	var $ajax_function;
 	var $iIncludeDots;
        var $idBookingFile = 0;
	/**
	 * Constructor
	 *
	 * @param integer $rows_per_page Number of records to display per page. Defaults to 10
	 * @param integer $links_per_page Number of links to display per page. Defaults to 5
	 * @param string $append Parameters to be appended to pagination links 
	 */
	
	function Pagination ( $arr, $rows_per_page = 10, $links_per_page = 5, $append = "")
	 {
		$this->arr = $arr;
		$this->rows_per_page = (int)$rows_per_page;
		if (intval($links_per_page ) > 0)
		{
                    $this->links_per_page = (int)$links_per_page;
		} 
		else 
		{
                    $this->links_per_page = 5;
		}
		$this->append = $append;
		$this->php_self = htmlspecialchars($_SERVER['PHP_SELF'] );
		if (isset($_REQUEST['page'])) 
		{
                    $this->page = intval($_REQUEST['page'] );
		}
	}
	
	/**
	 *
	 * @access public
	 * @return resource
	 */
	function paginate($func = 'showPage') {
		
		$this->func = sanitize_all_html_input($func);
		if(is_numeric($this->arr))
		{
			//Find total number of rows
			$this->total_rows = $this->arr ;
		}
		else
		{
			$this->total_rows = count( $this->arr );
		}

		//Return FALSE if no rows found
		if ($this->total_rows == 0) 
		{
			return array();
		}
		//Max number of pages
		$this->max_pages = ceil($this->total_rows / $this->rows_per_page );
		if ($this->links_per_page > $this->max_pages) {
			$this->links_per_page = $this->max_pages;
		}
		
		//Check the page value just in case someone is trying to input an aribitrary value
		if ($this->page > $this->max_pages || $this->page <= 0) {
			$this->page = 1;
		}
		
		//Calculate Offset
		$this->offset = $this->rows_per_page * ($this->page - 1);
		
		//Fetch the required result set
		for( $i = $this->offset; $i < ($this->offset + $this->rows_per_page) && $i < $this->total_rows; $i++ )
		{
			$rs[$i+1] = $this->arr[$i];
		}

		if (! $rs)
		return array();
		else
		return $rs;
	}
	
	/**
	 * Display the link to the first page
	 *
	 * @access public
	 * @param string $tag Text string to be displayed as the link. Defaults to 'First'
	 * @return string
	 */
	function renderFirst($tag = 'First') {
		if ($this->total_rows == 0)
			return FALSE;
		
		if ($this->page == 1) {
			//return '<span class="page_link">' . $tag . '</span>';
                        return '<ul class="tsc_pagination tsc_paginationC tsc_paginationC01">
			    <li class="first link" id="first">
			        <a  href="javascript:void(0)" class="In-active disabled">'.$tag.'</a>
			    </li>';
		} else {
			//return '<span class="page_link"><a onclick="'.$this->func.'(1)" style="cursor:pointer;">' . $tag . '</a></span>';
                    return '<ul class="tsc_pagination tsc_paginationC tsc_paginationC01">
			    <li class="first link" id="first">
			        <a href="javascript:void(0)" onclick="'.$this->func.'(1,'.$this->idBookingFile.')" style="cursor:pointer;">'.$tag.'</a>
			    </li>';
		}
	}
	
	/**
	 * Display the link to the last page
	 *
	 * @access public
	 * @param string $tag Text string to be displayed as the link. Defaults to 'Last'
	 * @return string
	 */
	function renderLast($tag = 'Last') {
		if ($this->total_rows == 0)
			return FALSE;
		
		if ($this->page == $this->max_pages) {
			//return '<span class="page_link">' . $tag . '</span>';
                        return '<li class="last link" id="last">
				         <a href="javascript:void(0)" class="In-active disabled">'.$tag.'</a>
				    </li>
				    <li class="flash"></li>
				</ul>
			';
		} else {
                    //return '<span class="page_link"> <a onclick="'.$this->func.'('.$this->max_pages.')" style="cursor:pointer;">' . $tag . '</a> </span>';
                    return '<li class="last link" id="last">
                                <a href="javascript:void(0)" onclick="'.$this->func.'('.$this->max_pages.','.$this->idBookingFile.')">'.$tag.'</a>
                           </li>
                           <li class="flash"></li>
                       </ul>
                    ';
		}
	}
	
	/**
	 * Display the next link
	 *
	 * @access public
	 * @param string $tag Text string to be displayed as the link. Defaults to '>>'
	 * @return string
	 */
	function renderNext($tag = '&gt;&gt;') {
		if ($this->total_rows == 0)
			return FALSE;
		$next = (int)$this->page+1;
		if ($this->page < $this->max_pages) {
                    //return '<span class="page_link"><a onclick="'.$this->func.'('. $next .')" style="cursor:pointer;">' . $tag . '</a></span>';
                    return '<li class="link" id="next">
                                <a href="javascript:void(0)" onclick="'.$this->func.'('. $next .','.$this->idBookingFile.')" style="cursor:pointer;">'.$tag.'</a>
                           </li>
                    ';	
		} else {
                    //return '<span class="page_link">' . $tag . '</span>';
                    return '<li class="link" id="next">
                                <a href="javascript:void(0)" class="In-active disabled">'.$tag.'</a>
                           </li>
                    ';	
		}
	}
	
	function renderDots($tag = '...') {
		if ($this->total_rows == 0)
			return FALSE;
		if ($this->page < $this->max_pages) 
		{
			$batch = ceil($this->page / $this->links_per_page );
			$end = $batch * $this->links_per_page;
			
			if ($end > $this->max_pages) {
				$end = $this->max_pages;
			}
			if($this->page == $end)
			{
				return '<span class="page_link">' . $tag . '</span>';
			}
			else
			{
				return '<span class="page_link"><a onclick="'.$this->func.'('. $end .','.$this->idBookingFile.')" style="cursor:pointer;">' . $tag . '</a></span>';
			}
		} 
		else 
		{
			return '<span class="page_link">...</span>';
		}
	}
	
	/**
	 * Display the previous link
	 *
	 * @access public
	 * @param string $tag Text string to be displayed as the link. Defaults to '<<'
	 * @return string
	 */
	function renderPrev($tag = '<<') {
		if ($this->total_rows == 0)
			return FALSE;
		if ($this->page > 1) {
			$prev =(int)$this->page-1;
			//return '<span class="page_link"> <a class="page_link" style="cursor:pointer;">' . $tag . '</a></span>';
                        return '<li class="link" id="previous">
                                    <a href="javascript:void(0)" onclick="'.$this->func.'('. $prev .','.$this->idBookingFile.')" >'.$tag.'</a>
                               </li>
                        ';	
		} else {
			//return '<span class="page_link">' . $tag . '</span>';
                        return '<li class="link" id="previous">
                                    <a href="javascript:void(0)" class="In-active disabled">'.$tag.'</a>
                               </li>
                        ';
		}
	}
	
	/**
	 * Display the page links
	 *
	 * @access public
	 * @return string
	 */
	function renderNav($prefix = '<span class="page_link">', $suffix = '</span>') {
		if ($this->total_rows == 0)
			return FALSE;
		
		$batch = ceil($this->page / $this->links_per_page );
		$end = $batch * $this->links_per_page;
		if ($end == $this->page) {
			//$end = $end + $this->links_per_page - 1;
		//$end = $end + ceil($this->links_per_page/2);
		}
		if ($end > $this->max_pages) {
			$end = $this->max_pages;
		}
		$start = $end - $this->links_per_page + 1;
		$links = '';
		
		for($i = $start; $i <= $end; $i ++) {
                    $pageValue=$i-1;
			if ($i == $this->page) 
			{
                            //$links .= $prefix . " $i " . $suffix;
                            
                            $links .= '<li id="'.$pageValue.'_no" class="link">
				          <a  href="javascript:void(0)" class="In-active current">
				              '.($i).'
				          </a>
				    </li>';
			}
			else
			{
                            //$links .= ' ' . $prefix . '<a onclick="'.$this->func.'('. $i .')" style="cursor:pointer;">' . $i . '</a>' . $suffix . ' ';
                            $links .= '<li id="'.$pageValue.'_no" class="link">
                                        <a  href="javascript:void(0)" onclick="'.$this->func.'('. $i .','.$this->idBookingFile.')" style="cursor:pointer;">
                                            '.($i).'
                                        </a>
                                  </li>
                            ';
			}
		}
		return $links;
	}
	
	/**
	 * Display full pagination navigation
	 *
	 * @access public
	 * @return string
	 */
	function renderFullNav() 
	{	
            // 3 dots(...) will appear if total num of pages are more then 5
            if($this->iIncludeDots==1 && $this->max_pages>=5)
            {
                return $this->renderFirst() . '&nbsp;' . $this->renderPrev() . '&nbsp;' . $this->renderNav() . '&nbsp;'. $this->renderDots() . '&nbsp;' . $this->renderNext() . '&nbsp;' . $this->renderLast();
            }
            else
            {
                return $this->renderFirst() . '&nbsp;' . $this->renderPrev() . '&nbsp;' . $this->renderNav() . '&nbsp;' . $this->renderNext() . '&nbsp;' . $this->renderLast();
            }
	}


        
	/**
	 * Ajax pagination for specific pages
	 * Author : abhi
	 */
	function renderNavAjax($prefix = '<span class="page_link">', $suffix = '</span>') {
		if ($this->total_rows == 0)
			return FALSE;

		$batch = ceil($this->page / $this->links_per_page );
		$end = $batch * $this->links_per_page;
		if ($end == $this->page) {
			//$end = $end + $this->links_per_page - 1;
		//$end = $end + ceil($this->links_per_page/2);
		}
		if ($end > $this->max_pages) {
			$end = $this->max_pages;
		}
		$start = $end - $this->links_per_page + 1;
		$links = '';

		for($i = $start; $i <= $end; $i ++) {
			if ($i == $this->page) {
				$links .= $prefix . "<a class='page_number_selected' >". $i."</a> " . $suffix;
			} else {
				$links .= ' ' . $prefix . '<a href="javascript:void(0)" onclick="'.$this->ajax_function.'('.$i.','.$this->append.');" class="page_number">' . $i . '</a>' . $suffix . ' ';
			}
		}

		return $links;
	}

	function renderPrevAjax($tag = '&lt; Previous') 
	{
		if ($this->total_rows == 0)
			return FALSE;

		if ($this->page > 1) {
			$page=$this->page - 1;
			//return '<a href="javascript:void(0)" onclick="'.$this->ajax_function.'('.$page.','.$this->append.');">' . $tag . '</a>';
                        return '<span class="page_button"><a href="javascript:void(0);" onclick="'.$this->ajax_function.'('.$page.','.$this->append.');"><img alt="Next Button" src="'.CDN_BASE.'/images/previously_submitted_team_back_button.png" onmouseover="this.src=\''.CDN_BASE.'/images/previously_submitted_team_back_button_over.png\';" onmouseout="this.src=\''.CDN_BASE.'/images/previously_submitted_team_back_button.png\';" align="absmiddle" /></a></span>';
		}
	}
	function renderNextAjax($tag = 'NEXT &gt;') 
	{
		if ($this->total_rows == 0)
			return FALSE;

		if ($this->page < $this->max_pages) {
			$page=$this->page + 1;
			$pagearry=explode(",",$this->append);
			//return '<a href="javascript:void(0)" onclick="'.$this->ajax_function.'('.$page.','.$this->append.');">' . $tag . '</a>';
                        return '<span class="page_button"><a href="javascript:void(0);" onclick="'.$this->ajax_function.'('.$page.','.$this->append.');"><img alt="Next Button" src="'.CDN_BASE.'/images/previously_submitted_team_next_button.png" onmouseover="this.src=\''.CDN_BASE.'/images/previously_submitted_team_next_button_over.png\';" onmouseout="this.src=\''.CDN_BASE.'/images/previously_submitted_team_next_button.png\';" align="absmiddle" /></a></span>';
		}
	}
	function renderFullNavAjax( $ajax_func = false ) {
                //$this->ajax_function = $ajax_func;
                
		return $this->renderPrevAjax() . '&nbsp;' . $this->renderNavAjax() . '&nbsp;' . $this->renderNextAjax();
	}


	/**
	 * Set debug mode
	 *
	 * @access public
	 * @param bool $debug Set to TRUE to enable debug messages
	 * @return void
	 */
	function setDebug($debug) {
		$this->debug = $debug;
	}
}
?>
