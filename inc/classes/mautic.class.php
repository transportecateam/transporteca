<?php
/**
 * This file is the container for all Mautic api related functionality. 
 *
 * mautic.class.php
 *
 * @copyright Copyright (C) 2016 Transporteca
 * @author Ajay
 * @package Transporteca Development
 */ 
if(!isset($_SESSION))
{
    session_start();
}

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_INC__ . "/functions.php" ); 
require_once( __APP_PATH_CLASSES__ . "/database.class.php"); 
require_once(__APP_PATH_INC__."/lib/Mautic/mautic.config.php");
   
class cMautic extends cDatabase
{    
    var $iPostedSuccessfully=false;
    function __construct()
    {   
        parent::__construct();
        return true;
    } 
    
    public function CreateMauticLead($mauticApiRequestAry,$idCustomer=false,$iCalledFrom=1)
    { 
        /*
         * We are no longer using mautic API services with transporteca @Ajay - 2016-11-04
         */
        return false;
        if(!empty($mauticApiRequestAry))
        {
            // @todo Sanitize this URL. Make sure it starts with http/https and doesn't end with '/'
            $mauticBaseUrl = __MAUTIC_API_BASE_URL__;
            $kRegisterShipCon = new cRegisterShipCon();

            $mauticApiCridentialsAry = array();
            $mauticApiCridentialsAry = $this->getMauticAuthCridentials();  
            if(!empty($mauticApiCridentialsAry))
            {
                $szAccessToken = trim($mauticApiCridentialsAry['szAccessToken']);
                $szAccessTokenSecret = trim($mauticApiCridentialsAry['szAccessTokenSecret']);

                $szClientKey = trim($mauticApiCridentialsAry['szClientKey']);
                $szClientSecretKey = trim($mauticApiCridentialsAry['szClientSecretKey']);
                $szOuthVersion = trim($mauticApiCridentialsAry['szOuthVersion']);

                /*
                *  Creating Mautic Authentication variables
                */
                $expires_in = strtotime("+10 YEAR");
                $settings = array();
                $settings['baseUrl'] = $mauticBaseUrl; 
                $settings['clientKey'] = $szClientKey;
                $settings['clientSecret'] = $szClientSecretKey;
                $settings['callback'] = 'https://www.transporteca.com/ipTest.php';
                $settings['version'] = $szOuthVersion;  
                $settings['accessToken'] = $szAccessToken;
                $settings['accessTokenSecret'] = $szAccessTokenSecret;  
                $settings['accessTokenExpires'] = time() + $expires_in;
                $settings['refreshToken'] = $szAccessTokenSecret; 
                
                try
                {
                    /*
                    * Authenticatig API Credentials
                    */
                    $auth = Mautic\Auth\ApiAuth::initiate($settings); 
                    if($iCalledFrom==3)
                    {
                        $f = fopen(__APP_PATH_LOGS__."/mauticApi_demo_".date('Ymd').".log", "a");
                    }
                    else
                    {
                        $f = fopen(__APP_PATH_LOGS__."/mauticApi_".date('Ymd').".log", "a");
                    }
                    
                    fwrite($f, "\n Mautic Setting: ".print_R($settings,true)." \n");
                    fwrite($f, "\n Data To be posted: ".print_R($mauticApiRequestAry,true)." \n");
                    
                    $mauticApiBaseUrl = __MAUTIC_API_BASE_URL__ . '/api/';
                    /*
                     * Creating Mautic Context Object
                     */
                    $leadApi = Mautic\MauticApi::getContext('leads',$auth,$mauticApiBaseUrl);
                    fwrite($f, "\n Lead API Object: ".print_R($leadApi,true)." \n");
                    
                    if(!empty($mauticApiRequestAry['szMauticAccountID']))
                    {
                        // Create new a lead of ID 1 is not found?
                        $createIfNotFound = true; 
                        $szMauticAccountID = $mauticApiRequestAry['szMauticAccountID'];
                        unset($mauticApiRequestAry['szMauticAccountID']);
                        //$lead = $leadApi->edit($szMauticAccountID, $data, $createIfNotFound);
                        $lead = $leadApi->create($mauticApiRequestAry); 
                    }
                    else
                    { 
                        unset($mauticApiRequestAry['szMauticAccountID']); 
                        $lead = $leadApi->create($mauticApiRequestAry); 
                    }  
                    $message = isset($lead['error']) ? $lead['error']['message'] : '';
                    fwrite($f, "\n Mautic Leads: ".print_R($lead,true)." \n");
                    
                    if(!empty($lead['lead']) && $idCustomer>0)
                    { 
                        $this->iPostedSuccessfully=1;
                        $update_user_query = " szMauticAccountID = '".trim($lead['lead']['id'])."' ";   
                        $kRegisterShipCon->updateUserDetailsByQuotes($update_user_query,$idCustomer);  
                    } 
                    else
                    {
                        $this->iPostedSuccessfully = 0;
                    }
                    if(!empty($message))
                    {
                       // echo "<br> API Error: ".$message;
                    }
                     
                    $ApiResponseAry = $lead['lead'];
                    $mauticApiLogsAry = array();
                    $mauticApiLogsAry['idCustomer'] = $idCustomer;
                    $mauticApiLogsAry['isPublished'] = $ApiResponseAry['isPublished'];
                    $mauticApiLogsAry['szMauticAccountID'] = $ApiResponseAry['id'];
                    $mauticApiLogsAry['idCreatedBy'] = $ApiResponseAry['createdBy'];
                    $mauticApiLogsAry['szCreatedByUser'] = $ApiResponseAry['createdByUser'];
                    $mauticApiLogsAry['dtDateAdded'] = $ApiResponseAry['dateAdded'];
                    $mauticApiLogsAry['idModifiedBy'] = $ApiResponseAry['modifiedBy'];
                    $mauticApiLogsAry['szModifiedByUser'] = $ApiResponseAry['modifiedByUser']; 
                    $mauticApiLogsAry['dtDateModified'] = $ApiResponseAry['dateModified'];
                    $mauticApiLogsAry['szResponseData'] = print_R($lead,true);
                    $mauticApiLogsAry['szRequestData'] = print_R($mauticApiRequestAry,true); 
                    $mauticApiLogsAry['iCalledFrom'] = $iCalledFrom;
                    $this->addMauticApiLogs($mauticApiLogsAry);
                    return $ApiResponseAry;
                }
                catch (Exception $ex)
                { 
                    fwrite($f, "\n Mautic Exception: ".print_R($ex,true)." \n"); 
                }  
            }   
        } 
    }
    
    function getMauticApiList()
    {
        //
        // @todo Sanitize this URL. Make sure it starts with http/https and doesn't end with '/'
        $mauticBaseUrl = __MAUTIC_API_BASE_URL__;
        $kRegisterShipCon = new cRegisterShipCon();

        $mauticApiCridentialsAry = array();
        $mauticApiCridentialsAry = $this->getMauticAuthCridentials();  
        if(!empty($mauticApiCridentialsAry))
        {
            $szAccessToken = trim($mauticApiCridentialsAry['szAccessToken']);
            $szAccessTokenSecret = trim($mauticApiCridentialsAry['szAccessTokenSecret']);

            $szClientKey = trim($mauticApiCridentialsAry['szClientKey']);
            $szClientSecretKey = trim($mauticApiCridentialsAry['szClientSecretKey']);
            $szOuthVersion = trim($mauticApiCridentialsAry['szOuthVersion']);

            /*
            *  Creating Mautic Authentication variables
            */
            $settings = array();
            $settings['baseUrl'] = $mauticBaseUrl; 
            $settings['clientKey'] = $szClientKey;
            $settings['clientSecret'] = $szClientSecretKey;
            $settings['callback'] = 'https://www.transporteca.com/mauticTest.php';
            $settings['version'] = $szOuthVersion;  
            $settings['accessToken'] = $szAccessToken;
            $settings['accessTokenSecret'] = $szAccessTokenSecret; 

            try
            {
                /*
                * Authenticatig API Credentials
                */
                $auth = Mautic\Auth\ApiAuth::initiate($settings);  

                $mauticApiBaseUrl = __MAUTIC_API_BASE_URL__ . '/api/';
                /*
                 * Creating Mautic Context Object
                 */
                $leadApi = Mautic\MauticApi::getContext('leads',$auth,$mauticApiBaseUrl);
                
                $filter['email'] = 'ajay@whiz-solutions.com';
                $mauticApiAry = $leadApi->getList($filter,0,1000);  
                
                print_R($mauticApiAry);
                $f = fopen(__APP_PATH_LOGS__."/mauticApi_".date('Ymd').".log", "a");
                fwrite($f, "\n Mautic Setting: ".print_R($mauticApiAry,true)." \n");
                $mauticLeadsAry = $mauticApiAry['leads'];
                echo "Total: ".$mauticLeadsAry['total'];
                
                $mauticResponseAry = array();
                if(!empty($mauticLeadsAry))
                {
                    $ctr = 0;
                    foreach($mauticLeadsAry as $mauticLeadsArys)
                    {
                        if(is_array($mauticLeadsArys['ipAddresses']) && !empty($mauticLeadsArys['ipAddresses']))
                        {
                            foreach($mauticLeadsArys['ipAddresses'] as $ipAddress=>$values)
                            {
                                $mauticResponseAry[$ctr]['szIpAddress'] = $ipAddress;
                                break;
                            }
                            $mauticResponseAry[$ctr]['szType'] = 'Tracking';
                        }
                        else
                        {
                            $mauticResponseAry[$ctr]['szIpAddress'] = $mauticLeadsArys['ipAddresses'];
                            $mauticResponseAry[$ctr]['szType'] = 'API';
                        } 
                        $mauticResponseAry[$ctr]['isPublished'] = $mauticLeadsArys['isPublished'];
                        $mauticResponseAry[$ctr]['dtDateAdded'] = $mauticLeadsArys['dateAdded'];
                        $mauticResponseAry[$ctr]['dtDateModified'] = $mauticLeadsArys['dateModified'];
                        
                        $coreFieldsAry = $mauticLeadsArys['fields']['core'];
                        $mauticResponseAry[$ctr]['szCustomerName'] = $coreFieldsAry['firstname']['value']." ".$coreFieldsAry['lastname']['value'];
                        $mauticResponseAry[$ctr]['szCountryName'] = $coreFieldsAry['country']['value'];
                        $ctr++;
                    }
                }
                return $mauticResponseAry; 
            }
            catch (Exception $ex)
            { 
                fwrite($f, "\n Mautic Exception: ".print_R($ex,true)." \n"); 
            }  
        }    
    }
    
    function updateDataToMauticApi($idCustomer,$iPostType=2)
    { 
        if($idCustomer>0)
        {
            /*
             * From 29-04-2016 We are no longer adding mautic data on the fly. 
             * 
             * @Ajay
             * 
            $f = fopen(__APP_PATH_LOGS__."/mauticApi_".date('Ymd').".log", "a");
            fwrite($f, "\n\n################### Started to Post data to Mautic on: ".date("d-m-Y h:i:s")." for customer ID: ".$idCustomer."#######################\n");
            $mauticApiRequestAry = array();
            $mauticApiRequestAry = $this->buildApiRequestForMautic($idCustomer);
             
            fwrite($f, "\n------------------Mautic Data: -----------------------\n\n ".print_R($mauticApiRequestAry,true)."\n\n");

            if(!empty($mauticApiRequestAry))
            { 
                //Adding User details to Mautic API
                $mauicResponseAry = array();
                $mauicResponseAry = $this->CreateMauticLead($mauticApiRequestAry,$idCustomer,$iPostType);     
                fwrite($f, "\n------------------Mautic Response: -----------------------\n\n ".print_R($mauicResponseAry,true)."\n\n");
            }
            * 
            */
        } 
    }
    
    function validateMauticApiAuthentication()
    { 
        // @todo Sanitize this URL. Make sure it starts with http/https and doesn't end with '/'
        $mauticBaseUrl = "https://transporteca.mautic.net";

        $settings = array(
            'baseUrl'           => $mauticBaseUrl,
            'clientKey'         => "3le5i8gou8e80kw44sg0cokoco4okg0oko0c84cowc0o8ggs8s",
            'clientSecret'      => "3sc0h2e8b5gkwgck0044sg88kkokwggggsosk400400o444s08",
            'callback'          => 'http://dev.transporteca.com/mauticTest.php', // @todo Change this to your app callback. It should be the same as you entered when you were creating your Mautic API credentials.
            'version'           => 'OAuth1a'
        );
 
        if (!empty($accessTokenData['accessToken']) && !empty($accessTokenData['accessTokenSecret'])) {
            //$settings['accessToken']        = $accessTokenData['accessToken'] ;
            //$settings['accessTokenSecret']  = $accessTokenData['accessTokenSecret'];
            //$settings['accessTokenExpires'] = $accessTokenData['accessTokenExpires'];
        } 
        
        $auth = Mautic\Auth\ApiAuth::initiate($settings);
        echo "Auth <br><br>";
        print_R($auth);
        if ($auth->validateAccessToken()) {
            echo "In first FI";
            if ($auth->accessTokenUpdated()) {
                echo "Second IF <br><br> ";
                $accessTokenData = $auth->getAccessTokenData();
                $_SESSION['access_token'] = $accessTokenData;
                print_R($accessTokenData);
                // @todo Save $accessTokenData
                // @todo Display success authorization message
            } else {
                // @todo Display info message that this app is already authorized.
            }
        } else {
            // @todo Display info message that the token is not valid.
        }
        return $auth;
    }
    
    function validateMauticApiAuthenticationOuth2()
    { 
        // @todo Sanitize this URL. Make sure it starts with http/https and doesn't end with '/'
        $mauticBaseUrl = "https://transporteca.mautic.com";
 
        $settings = array(
            'baseUrl'      => 'https://transporteca.mautic.com',
            'version'      => 'OAuth2',
            'clientKey'    => '1_59dfgbsuavgo88k8c4woockw48g8kw0cc0s084g4ckk8c4soo4',
            'clientSecret' => '4mttvae6pgu884gss404gsk0g80ks8g8ss4gkk88sss8oc8gkw', 
            'callback'     => 'http://www.transporteca.com/ipTest.php'
        );
 
        if (!empty($accessTokenData['accessToken']) && !empty($accessTokenData['accessTokenSecret'])) {
            //$settings['accessToken']        = $accessTokenData['accessToken'] ;
            //$settings['accessTokenSecret']  = $accessTokenData['accessTokenSecret'];
            //$settings['accessTokenExpires'] = $accessTokenData['accessTokenExpires'];
        } 
        $auth = Mautic\Auth\ApiAuth::initiate($settings);
        echo "Auth <br><br>";
        print_R($auth);
        if ($auth->validateAccessToken()) {
            echo "In first FI";
            if ($auth->accessTokenUpdated()) {
                echo "Second IF <br><br> ";
                $accessTokenData = $auth->getAccessTokenData();
                $_SESSION['access_token'] = $accessTokenData;
                print_R($accessTokenData);
                // @todo Save $accessTokenData
                // @todo Display success authorization message
            } else {
                // @todo Display info message that this app is already authorized.
            }
        } else {
            // @todo Display info message that the token is not valid.
        }
        return $auth;
    }
    
    function getMauticApiLogsData()
    {
        /*
        * From now we are not using mautic in our system @Ajay - 2016-11-07
        */
        return false;
        $query="
            SELECT
                m.id as idMauticLogs,
                m.idCustomer,
                m.isPublished,
                m.szMauticAccountID,
                m.idCreatedBy,
                m.szCreatedByUser,
                m.dtDateAdded,
                m.idModifiedBy,
                m.szModifiedByUser,
                m.dtDateModified, 
                m.szResponseData,
                m.szRequestData,
                m.iCalledFrom,
                m.dtCreatedOn,
                m.iActive,
                u.szFirstName,
                u.szLastName,
                u.szEmail
            FROM
                ".__DBC_SCHEMATA_MAUTIC_API_LOGS__." m
            LEFT JOIN
                ".__DBC_SCHEMATA_USERS__." u
            ON
                m.idCustomer = u.id
            ORDER BY
                dtCreatedOn DESC
            LIMIT
                0,50
        "; 
        //echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        { 
            if($this->iNumRows>0)
            { 
                $ret_ary = array();
                while($row = $this->getAssoc($result))
                {
                    $ret_ary[$ctr] = $row;
                    $ctr++;
                }
                return $ret_ary ;
            }
            else
            {
                return array();
            }
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
    
    function addMauticApiLogs($data)
    {
        /*
        * From now we are not using mautic in our system @Ajay - 2016-11-07
        */
        return false;
        if(!empty($data))
        {
            $query = " 
                INSERT INTO
                  ".__DBC_SCHEMATA_MAUTIC_API_LOGS__."
                (
                    idCustomer,
                    isPublished,
                    szMauticAccountID,
                    idCreatedBy,
                    szCreatedByUser,
                    dtDateAdded,
                    idModifiedBy,
                    szModifiedByUser,
                    dtDateModified, 
                    szResponseData,
                    szRequestData,
                    iCalledFrom,
                    dtCreatedOn,
                    iActive 
                )
                VALUES
                ( 
                    '".mysql_escape_custom(trim($data['idCustomer']))."',
                    '".mysql_escape_custom(trim($data['isPublished']))."',
                    '".mysql_escape_custom(trim($data['szMauticAccountID']))."',
                    '".mysql_escape_custom(trim($data['idCreatedBy']))."',
                    '".mysql_escape_custom(trim($data['szCreatedByUser']))."',
                    '".mysql_escape_custom(trim($data['dtDateAdded']))."',
                    '".mysql_escape_custom(trim($data['idModifiedBy']))."',
                    '".mysql_escape_custom(trim($data['szModifiedByUser']))."',
                    '".mysql_escape_custom(trim($data['dtDateModified']))."',
                    '".mysql_escape_custom(trim($data['szResponseData']))."',
                    '".mysql_escape_custom(trim($data['szRequestData']))."',
                    '".mysql_escape_custom(trim($data['iCalledFrom']))."',
                    now(),
                    '1'
                )
            " ;  
            //echo "<br><br>".$query ;
            //die; 
            if($result=$this->exeSQL($query))
            {
                return true;
            }
            else
            { 
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    
    public function buildApiRequestForMautic($idCustomer)
    {
        if($idCustomer>0)
        {
            $kUser = new cUser();
            $kUser->getUserDetails($idCustomer);
            
            $kConfig_new = new cConfig();
            $kConfig_new->loadCountry($kUser->szCountry);
            $szCountryName = $kConfig_new->szCountryName; 

            $kConfig_new->loadCountry($kUser->idInternationalDialCode);
            $iInternationDialCode = $kConfig_new->iInternationDialCode;
            
            if(empty($kUser->szCompanyName))
            {
                $kUser->szCompanyName = 'Private';
            }
            $dtLastLogin = $kUser->dtLastLogin;
            
            $kConfig = new cConfig();
//            $langArr=$kConfig->getLanguageDetails('',$kUser->iLanguage);
//            
////            if(!empty($langArr)) //Danish
////            {
////                $szLanguage = ucwords(strtolower($langArr[0]['szName']));
////            }
////            else //English
////            {
////                $szLanguage = "English";
////            }
            
            $kConfig->loadCountry($kUser->szCountry);  
            if($kConfig->iDefaultLanguage>0){
            $langArr=$kConfig->getLanguageDetails('',$kConfig->iDefaultLanguage);
            }
            if(!empty($langArr)) //Denmark
            {
                $szLanguage = ucwords(strtolower($langArr[0]['szName']));
            }
            else
            {
                $szLanguage = "English";
            }
            
            if($kUser->szCurrency>0)
            {
                $kUser_new = new cUser();
                $kUser_new->loadCurrency($kUser->szCurrency);
                $szCurrency = $kUser_new->szCurrencyName;
            }
            else
            {
                $szCurrency = 'DKK';
            } 
            
            $apiRequestAry = array();
            $apiRequestAry['firstname'] = $kUser->szFirstName;
            $apiRequestAry['lastname'] = $kUser->szLastName;
            $apiRequestAry['company'] = $kUser->szCompanyName;
            $apiRequestAry['email'] = $kUser->szEmail;
            $apiRequestAry['phone'] = "+".$iInternationDialCode." ".$kUser->szPhoneNumber;
            $apiRequestAry['address1'] = $kUser->szAddress1;
            $apiRequestAry['address2'] = $kUser->szAddress2;
            $apiRequestAry['city'] = $kUser->szCity;
            $apiRequestAry['state'] = $kUser->szState;
            $apiRequestAry['zipcode'] = $kUser->szPostcode;
            $apiRequestAry['country'] = $szCountryName; 
            $apiRequestAry['language'] = $szLanguage;
            $apiRequestAry['lastlogin'] = $dtLastLogin;
            $apiRequestAry['currency'] = $szCurrency;
            $apiRequestAry['szMauticAccountID'] = $kUser->szMauticAccountID;  
            $apiRequestAry['ipaddress'] = getRealIP(); 
            
            if(!empty($kUser->szIpAddress))
            {
                //$apiRequestAry['ipaddress'] = $kUser->szIpAddress; 
            }
            return $apiRequestAry;
        }
    } 
    function getMauticAuthCridentials()
    {
        /*
        * From now we are not using mautic in our system @Ajay - 2016-11-07
        */
        return false;
        $query = " 
            SELECT 
                id,
                szOuthVerifier,
                szOuthSecretKey,
                szAccessToken,
                szAccessTokenSecret,
                szClientKey,
                szClientSecretKey,
                szOuthVersion,
                dtCreatedOn 
            FROM
                ".__DBC_MAUTIC_AUTH_CRIDENTIALS__."
            WHERE 
                iActive = '1'
        ";
        //echo $query ;
        if($result=$this->exeSQL($query))
        {
            $retAry = array(); 
            $row = $this->getAssoc($result); 
            return $row ;
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }	
    }
    
    
    public function CreateMauticLeadDemo($mauticApiRequestAry,$iCalledFrom=1)
    {
        if(!empty($mauticApiRequestAry))
        {
            $this->set_szEmail(trim(sanitize_all_html_input(strtolower($mauticApiRequestAry['email']))));
            if ($this->error === true)
            {
                return array();
            }
            print_R($mauticApiRequestAry);
            // @todo Sanitize this URL. Make sure it starts with http/https and doesn't end with '/'
            $mauticBaseUrl = __MAUTIC_API_BASE_URL__;
            $kRegisterShipCon = new cRegisterShipCon();

            $mauticApiCridentialsAry = array();
            $mauticApiCridentialsAry = $this->getMauticAuthCridentials();  
            if(!empty($mauticApiCridentialsAry))
            {
                $szAccessToken = trim($mauticApiCridentialsAry['szAccessToken']);
                $szAccessTokenSecret = trim($mauticApiCridentialsAry['szAccessTokenSecret']);

                $szClientKey = trim($mauticApiCridentialsAry['szClientKey']);
                $szClientSecretKey = trim($mauticApiCridentialsAry['szClientSecretKey']);
                $szOuthVersion = trim($mauticApiCridentialsAry['szOuthVersion']);

                /*
                *  Creating Mautic Authentication variables
                */
                $settings = array();
                $settings['baseUrl'] = $mauticBaseUrl; 
                $settings['clientKey'] = $szClientKey;
                $settings['clientSecret'] = $szClientSecretKey;
                $settings['callback'] = 'https://www.transporteca.com/mauticTest.php';
                $settings['version'] = $szOuthVersion;  
                $settings['accessToken'] = $szAccessToken;
                $settings['accessTokenSecret'] = $szAccessTokenSecret; 

                try
                {
                    /*
                    * Authenticatig API Credentials
                    */
                    $auth = Mautic\Auth\ApiAuth::initiate($settings); 
                    $f = fopen(__APP_PATH_LOGS__."/mauticApi_Demo_".date('Ymd').".log", "a");
                    fwrite($f, "\n Mautic Setting: ".print_R($settings,true)." \n");
                    
                    $mauticApiBaseUrl = __MAUTIC_API_BASE_URL__ . '/api/';
                    /*
                     * Creating Mautic Context Object
                     */
                    $leadApi = Mautic\MauticApi::getContext('leads',$auth,$mauticApiBaseUrl);
  
                    $lead = $leadApi->create($mauticApiRequestAry);  
                    $message = isset($lead['error']) ? $lead['error']['message'] : '';
                    fwrite($f, "\n Mautic Leads: ".print_R($lead,true)." \n");
                       
                    if(!empty($message))
                    {
                       return $lead;
                    }
                     
                    $ApiResponseAry = $lead['lead']; 
                    $mauticApiLogsAry = array();
                    $mauticApiLogsAry['idCustomer'] = $idCustomer;
                    $mauticApiLogsAry['isPublished'] = $ApiResponseAry['isPublished'];
                    $mauticApiLogsAry['szMauticAccountID'] = $ApiResponseAry['id'];
                    $mauticApiLogsAry['idCreatedBy'] = $ApiResponseAry['createdBy'];
                    $mauticApiLogsAry['szCreatedByUser'] = $ApiResponseAry['createdByUser'];
                    $mauticApiLogsAry['dtDateAdded'] = $ApiResponseAry['dateAdded'];
                    $mauticApiLogsAry['idModifiedBy'] = $ApiResponseAry['modifiedBy'];
                    $mauticApiLogsAry['szModifiedByUser'] = $ApiResponseAry['modifiedByUser']; 
                    $mauticApiLogsAry['dtDateModified'] = $ApiResponseAry['dateModified'];
                    $mauticApiLogsAry['szResponseData'] = print_R($lead,true);
                    $mauticApiLogsAry['szRequestData'] = print_R($mauticApiRequestAry,true); 
                    $mauticApiLogsAry['iCalledFrom'] = $iCalledFrom;
                    $this->addMauticApiLogs($mauticApiLogsAry);
                    return $ApiResponseAry;
                }
                catch (Exception $ex)
                { 
                    fwrite($f, "\n Mautic Exception: ".print_R($ex,true)." \n"); 
                }  
            }   
        } 
    } 
    
    
    function set_szEmail( $value,$required_flag=true )
    {
        $this->szEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szEmail", "Email Address", false, 255, $required_flag );
    }
}
?>