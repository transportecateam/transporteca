<?php
/**
 * This file is the container for all forwarders related functionality.
 * All functionality related to forwarders details should be contained in this class.
 *
 * forwarder.class.php
 *
 * @copyright Copyright (C) 2012 Transporteca, LLC
 * @author Anil
 * @package Transporteca Development
 */
if (!isset($_SESSION)) 
{
  session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_INC__ . "/functions.php" );
require_once( __APP_PATH__ . "/forwarders/listExcelData.php" );
class cForwarder extends cDatabase
{
    /**
     * These are the variables for each user.
    */

    var $id;
    var $szFirstName;
    var $szLastName;
    var $szEmail;
    var $szNewEmail;
    var $szPhoneNumber;
    var $szCountry;
    var $szCountryName;
    var $szCountryId;
    var $szCompanyName;
    var $szWebsite;
    var $szDisplayName;
    var $szLink;
    var $szPhone;
	var $szMobile;
	var $szPassword;
    var $szNonFrocePassword;
	var $t_base="Forwarders/";
	var $t_base_comp_info = "ForwardersCompany/company_information/";
	var $mode;
	var $sznonAcceptance;
	var $szControlPanelUrl;
	var $szCode;
	var $iAutomaticPaymentInvoicing;
	var $fRefFee;
	var $szGeneralEmailAddress;
	var $iPopupDetails;
	/**
	 * class constructor for php5
	 *
	 * @return bool
	 */
	function __construct()
	{
		parent::__construct();

		// establish a Database connection.
		//$this->connect( __DBC_USER__, __DBC_PASSWD__, __DBC_SCHEMATA__, __DBC_HOST__);
		return true;
	}
	
	function forwarderSignUp($data,$subject)
	{
		$this->set_szFirstName(trim(sanitize_all_html_input($data['szFirstName'])));
		$this->set_szLastName(trim(sanitize_all_html_input($data['szLastName'])));
		$this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szEmail']))));
		$this->set_szPhoneNumber(trim(sanitize_all_html_input(urldecode(base64_decode($data['szPhoneNumberUpdate'])))));
		$this->set_szCompanyName(trim(sanitize_all_html_input($data['szCompanyName'])));
		$this->set_szCountry(trim(sanitize_all_html_input($data['szCountry'])));
		$this->set_szWebSite(trim(sanitize_all_html_input($data['szWebsite'])));
		
		//exit if error exist.
		if ($this->error === true)
		{
			return false;
		}
		
		if(!empty($this->szPhoneNumber))
		{
			$szPhoneNumber=$this->szPhoneNumber;
		}
		$replace_ary=array();
		$kWHSSearch = new cWHSSearch();
		$toEmail=$kWHSSearch->getManageMentVariableByDescription('__FORWARDER_SIGNUP_EMAIL__');
		//$toEmail="anil@whiz-solutions.com,ashish@whiz-solutions.com";
		$subject=$subject." ".date("d/m/Y");
		$replace_ary['szCompanyName']=$this->szCompanyName;
		$replace_ary['szFirstName']=$this->szFirstName;
		$replace_ary['szLastName']=$this->szLastName;
		$replace_ary['szEmail']=$this->szEmail;
		$replace_ary['szCountry']=$this->szCountry;
		$replace_ary['szPhoneNumber']=$szPhoneNumber;
		$replace_ary['szWebsite']=$this->szWebSite;
		$replace_ary['szDate']=date('d/m/Y');
		createEmail(__FORWARDER_SIGNUP_EMAIL__, $replace_ary,$toEmail,$subject, __STORE_SUPPORT_EMAIL__,$_SESSION['user_id'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__);
		return true;
	}
	
	function save($data)
	{
		if(!empty($data))
		{
			$this->set_szCompanyName(trim(sanitize_all_html_input($data['szCompanyName'])));
			$this->set_szAddress1(trim(sanitize_all_html_input($data['szAddress'])));
			$this->set_szAddress2(trim(sanitize_all_html_input($data['szAddress2'])));
			$this->set_szAddress3(trim(sanitize_all_html_input($data['szAddress3'])));
			$this->set_szPostcode(trim(sanitize_all_html_input($data['szPostCode'])));	
			$this->set_szCity(trim(sanitize_all_html_input($data['szCity'])));
			$this->set_szState(trim(sanitize_all_html_input($data['szState'])));	
			$this->set_szCountry(trim(sanitize_all_html_input($data['idCountry'])));	
					
			$this->set_szPhoneNumber(trim(sanitize_all_html_input(urldecode(base64_decode($data['szPhoneUpdate'])))));
			$this->set_szCompanyRegistrationNum(trim(sanitize_all_html_input($data['szCompanyRegistrationNum'])));
			$this->set_szVatRegistrationNum(trim(sanitize_all_html_input($data['szVatRegistrationNum'])));
			
			$this->set_id(trim(sanitize_all_html_input($data['idForwarder'])));
			
			if($this->error===true)
			{
				return false;
			}
			if(!empty($this->szPhoneNumber))
			{
                            $phoneNumber = $this->szPhoneNumber ;
			}
			
			$query="
				UPDATE
					".__DBC_SCHEMATA_FROWARDER__."
				SET
                                    szCompanyName = '".mysql_escape_custom(trim($this->szCompanyName))."',
                                    szAddress = '".mysql_escape_custom(trim($this->szAddress1))."',
                                    szAddress2 = '".mysql_escape_custom(trim($this->szAddress2))."',
                                    szAddress3 = '".mysql_escape_custom(trim($this->szAddress3))."',
                                    szPostcode = '".mysql_escape_custom(trim($this->szPostcode))."',
                                    szCity = '".mysql_escape_custom(trim($this->szCity))."',
                                    szState = '".mysql_escape_custom(trim($this->szState))."',
                                    idCountry = '".(int)$this->szCountry."',
                                    szPhone = '".mysql_escape_custom(trim($phoneNumber))."',
                                    szCompanyRegistrationNum = '".mysql_escape_custom(trim($this->szCompanyRegistrationNum))."',
                                    szVatRegistrationNum = '".mysql_escape_custom(trim($this->szVatRegistrationNum))."'
				WHERE
					id = '".(int)$this->id."'
			";
			//echo "<br>".$query ;
			if($result = $this->exeSQL($query))
			{
                            return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function load($idForwarder=false,$szForwarderAlias=false)
	{
            if($idForwarder>0 || !empty($szForwarderAlias))
            {
                if(!empty($szForwarderAlias))
                {
                    $query_and = " f.szForwarderAlias = '".mysql_escape_custom($szForwarderAlias)."' ";
                }
                else if($idForwarder>0)
                {
                    $query_and = " f.id = '".(int)$idForwarder."' ";
                }
                else
                {
                    return false;
                }
                
                $query="
                    SELECT
                        f.id,
                        f.szDisplayName,
                        f.szGeneralEmailAddress,
                        f.szLogo,
                        f.szCompanyName,
                        f.szAddress,
                        f.szAddress2,
                        f.szAddress3,
                        f.szPostCode,
                        f.szCity,
                        f.szState,
                        f.idCountry,
                        f.szCompanyRegistrationNum,
                        f.szLink,
                        f.szForwarderAlias,
                        f.szPhone,
                        f.szVatRegistrationNum,
                        f.szBankName,
                        f.szNameOnAccount,
                        f.iAccountNumber,
                        f.iSortCode,
                        f.szIBANAccountNumber,
                        f.idBankCountry,
                        f.szCurrency,
                        f.szControlPanelUrl,
                        f.iAgreeTNC,
                        f.dtAgreement,
                        f.iVersionUpdate,
                        f.dtVersionUpdate,
                        f.szSwiftCode, 
                        f.isOnline, 
                        f.iPopupDetails,
                        f.iCurrencyFlag,
                        ( SELECT cont.szCountryName FROM ".__DBC_SCHEMATA_COUNTRY__." cont WHERE cont.id = f.idCountry ) as szCountryName,
                        ( SELECT cont1.szCountryName FROM ".__DBC_SCHEMATA_COUNTRY__." cont1 WHERE cont1.id = f.idBankCountry ) as szBankCountryName,
                        f.szConfirmationKeyForExpiryMail,
                        f.dtExpiryMail,
                        f.iExpiryMailType,
                        f.szServiceUpdatedBy,
                        f.szPdfLogo,
                        f.iProfitType, 
                        f.iOffLineQuotes,
                        f.iDisplayQuickQuote,
                        f.idCountry,
                        f.iCreditDays
                    FROM
                        ".__DBC_SCHEMATA_FROWARDER__." f
                    WHERE
                        $query_and	
                ";
                //echo "<br>".$query ;
                if($result = $this->exeSQL($query))
                { 
                    $row=$this->getAssoc($result);
                    $this->szDisplayName=sanitize_all_html_input(trim($row['szDisplayName']));
                    $this->szGeneralEmailAddress=sanitize_all_html_input(trim($row['szGeneralEmailAddress']));
                    $this->szLogo=sanitize_all_html_input(trim($row['szLogo']));
                    $this->id = sanitize_all_html_input(trim($row['id']));
                    $this->szCompanyName=sanitize_all_html_input(trim($row['szCompanyName']));
                    $this->szAddress=sanitize_all_html_input(trim($row['szAddress']));
                    $this->szAddress2=sanitize_all_html_input(trim($row['szAddress2']));
                    $this->szAddress3=sanitize_all_html_input(trim($row['szAddress3']));
                    $this->szPostCode=sanitize_all_html_input(trim($row['szPostCode']));
                    $this->szCity=sanitize_all_html_input(trim($row['szCity']));

                    $this->szState=sanitize_all_html_input(trim($row['szState']));
                    $this->idCountry=sanitize_all_html_input(trim($row['idCountry']));
                    $this->szCompanyRegistrationNum=sanitize_all_html_input(trim($row['szCompanyRegistrationNum']));
                    $this->szLink=sanitize_all_html_input(trim($row['szLink']));
                    $this->szForwarderAlies=sanitize_all_html_input(trim($row['szForwarderAlias']));
                    $this->szPhone=sanitize_all_html_input(trim($row['szPhone']));
                    $this->szVatRegistrationNum = sanitize_all_html_input(trim($row['szVatRegistrationNum']));

                    $this->szBankName=sanitize_all_html_input(trim($row['szBankName']));
                    $this->szNameOnAccount=sanitize_all_html_input(trim($row['szNameOnAccount']));
                    $this->iAccountNumber=sanitize_all_html_input(trim($row['iAccountNumber']));
                    $this->iSortCode=sanitize_all_html_input(trim($row['iSortCode']));
                    $this->szSwiftCode=sanitize_all_html_input(trim($row['szSwiftCode']));					
                    $this->szIBANAccountNumber=sanitize_all_html_input(trim($row['szIBANAccountNumber']));
                    $this->idBankCountry=sanitize_all_html_input(trim($row['idBankCountry']));
                    $this->szCurrency = sanitize_all_html_input(trim($row['szCurrency']));
                    $this->szCountryName = sanitize_all_html_input(trim($row['szCountryName']));
                    $this->szControlPanelUrl = sanitize_all_html_input(trim($row['szControlPanelUrl']));
                    $this->dtAgreement=sanitize_all_html_input(trim($row['dtAgreement']));
                    $this->iAgreeTNC=sanitize_all_html_input(trim($row['iAgreeTNC']));
                    $this->iVersionUpdate=sanitize_all_html_input(trim($row['iVersionUpdate']));
                    $this->dtVersionUpdate=sanitize_all_html_input(trim($row['dtVersionUpdate'])); 
                    $this->isOnline=sanitize_all_html_input(trim($row['isOnline']));
                    $this->szBankCountryName=sanitize_all_html_input(trim($row['szBankCountryName'])); 								
                    $this->iPopupDetails=sanitize_all_html_input(trim($row['iPopupDetails']));
                    $this->iCurrencyFlag=sanitize_all_html_input(trim($row['iCurrencyFlag']));
                    $this->szConfirmationKeyForExpiryMail=sanitize_all_html_input(trim($row['szConfirmationKeyForExpiryMail']));	
                    $this->dtExpiryMail=sanitize_all_html_input(trim($row['dtExpiryMail']));	
                    $this->iExpiryMailType=sanitize_all_html_input(trim($row['iExpiryMailType']));
                    $this->szServiceUpdatedBy=sanitize_all_html_input(trim($row['szServiceUpdatedBy']));
                    $this->idCountry=(int)$row['idCountry'];
                    $this->iCreditDays = (int)$row['iCreditDays'];
                    
                    $this->szPdfLogo=sanitize_all_html_input(trim($row['szPdfLogo']));
                    $this->iProfitType=sanitize_all_html_input(trim($row['iProfitType'])); 
                    $this->fReferalFee=sanitize_all_html_input(trim($row['fReferalFee']));
                    $this->iOffLineQuotes = sanitize_all_html_input(trim($row['iOffLineQuotes'])); 
                    $this->iDisplayQuickQuote = sanitize_all_html_input(trim($row['iDisplayQuickQuote']));
                    $this->iCreditDays = sanitize_all_html_input(trim($row['iCreditDays']));
                    return true ; 
                }
                else
                {
                    return false ;
                }
            }
	}
	
	function getForwarderBankDetails($idForwarder)
	{
	
		if($idForwarder>0)
		{
			$query="
				SELECT
					fbd.id,
					fbd.szBankName,
					fbd.szNameOnAccount,
					fbd.iAccountNumber,
					fbd.iSortCode,
					fbd.szIBANAccountNumber,
					fbd.idBankCountry,
					fbd.szSwiftCode,
					con.szCountryName
				FROM
					".__DBC_SCHEMATA_FROWARDER__." fbd
				INNER JOIN
					".__DBC_SCHEMATA_COUNTRY__." con
				ON
					con.id = fbd.idBankCountry	
				WHERE
					fbd.id = '".(int)$idForwarder."'	
			";
			//echo "<br>".$query ;
			if($result = $this->exeSQL($query))
			{
				//if($this->iNumrows>0)
			//	{
					$row=$this->getAssoc($result);
					
					$this->szBankName=sanitize_all_html_input(trim($row['szBankName']));
					$this->szNameOnAccount=sanitize_all_html_input(trim($row['szNameOnAccount']));
					$this->iAccountNumber=sanitize_all_html_input(trim($row['iAccountNumber']));
					$this->iSortCode=sanitize_all_html_input(trim($row['iSortCode']));
					$this->szSwiftCode=sanitize_all_html_input(trim($row['szSwiftCode']));					
					$this->szIBANAccountNumber=sanitize_all_html_input(trim($row['szIBANAccountNumber']));
					$this->idBankCountry=sanitize_all_html_input(trim($row['idBankCountry']));
					$this->szCurrency = sanitize_all_html_input(trim($row['szCurrency']));
					$this->idCurrency=sanitize_all_html_input(trim($row['idCurrency']));
					$this->szCountryName = sanitize_all_html_input(trim($row['szCountryName']));
					
					return true ;
				//}
				//else
				//{
				///	return false ;
				//}
			}
			else
			{
				return false ;
			}
		}
	}
	
	function getForwarderAllEmail($idForwarder)
	{
		if((int)$idForwarder>0)
		{
			$query="
				SELECT
					szEmail
				FROM
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
				WHERE
					idForwarder='".(int)$idForwarder."'
			";
			if($result = $this->exeSQL($query))
			{
		
				if($this->iNumRows > 0)
				{
					while($row=$this->getAssoc($result))
					{
						$szEmailArr[]=$row['szEmail'];	
					}
					
					return $szEmailArr;
				}
				else
				{
					return array();
				}
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function getForwardRating($idForwarder,$month='',$bPartnerApi=false)
	{
            if(!empty($idForwarder))
            {
                $whereQuery="";
                if(!empty($month) && $month!="")
                {
                    $today=date('Y-m-d');
                    $col .=",sum(iRating) as trating,iRating";

                    $prevdate = date("Y-m-d",strtotime(date("Y-m-d", strtotime($today)) . '-'.$month.' month'));

                    $whereQuery .="
                        AND
                            date(dtCompleted)>='".mysql_escape_custom($prevdate)."'
                    ";				
                }
                $query="
                    SELECT
                        iRating,
                        szReview,
                        dtCompleted
                    FROM
                        ".__DBC_SCHEMATA_RATING_REVIEW__."	
                    WHERE
                        idForwarder = '".(int)$idForwarder."'
                    AND
                        iRating>0
                    ".$whereQuery."	
                    ORDER BY
                        iRating DESC,dtCompleted DESC
                ";
                //echo "<br> ".$query ;
                if($result=$this->exeSQL($query))
                {
                    $ctr=0;
                    while($row=$this->getAssoc($result))
                    {
                        if($bPartnerApi)
                        {
                            $rate[$row['iRating']][$ctr]['szReview'] = $row['szReview'];
                            $rate[$row['iRating']][$ctr]['dtCompleted'] = date('Y-m-d',strtotime($row['dtCompleted']));
                            $ctr++;
                        }
                        else
                        {
                            $str='';
                            if(!empty($row['szReview']))
                            {
                                $str='"'.$row['szReview'].'" '.date('d F Y',strtotime($row['dtCompleted']));
                            }
                            $rate[$row['iRating']][]=$str;
                        }  
                    }
                    return $rate; 
                }
            }
	}
	
	function getForwarderCustomerServiceEmail($idForwarder,$idRole=0)
	{
            if($idForwarder>0)
            { 
                if((int)$idRole>0)
                {
                    $sql .=" idForwarderContactRole = '".(int)$idRole."'";
                }
                else
                {
                    $sql .=" idForwarderContactRole = '".(int)__CUSTOMER_PROFILE_ID__."'";
                }
                $query="
                    SELECT
                        szEmail
                    FROM
                        ".__DBC_SCHEMATA_FORWARDER_PREFERENCES__."	
                    WHERE
                        idForwarder = '".(int)$idForwarder."'
                    AND
                        ".$sql."
                    AND
                        szEmail != ''
                    AND
                        iActive='1'
                ";
                //echo "<br><br> ".$query."<br>";
                if($result=$this->exeSQL($query))
                {
                    $emailAry = array();
                    $ctr=0;
                    while($row=$this->getAssoc($result))
                    {
                        $emailAry[$ctr] = $row['szEmail'] ;
                        $ctr++ ;
                    }
                    return $emailAry ;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
	
	function getMaximumUsedNumberByForwarder($idForwarder,$counter=false,$szReferenceType='BOOKING')
	{
            if($idForwarder>0)
            {
                if($szReferenceType=='FILE')
                {
                    $query_and = " AND szReferenceType = 'FILE' ";
                }
                else
                {
                    $query_and = " AND szReferenceType = 'BOOKING' AND idForwarder = '".(int)$idForwarder."'";
                }
                
                $query = "
                    SELECT
                        MAX(iMaxNumReference) as iMaxReference
                    FROM
                        ".__DBC_SCHEMATA_BOOKING_REFERENCE_LOGS__."
                    WHERE 
                        MONTH(dtCreatedOn) = MONTH(now())
                    AND
                        YEAR(dtCreatedOn) = YEAR(now())
                    $query_and
                ";
                //echo "<br>".$query."<br>";
                if($result=$this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        if($counter)
                        {
                            return true ;
                        }
                        else
                        {
                            $row=$this->getAssoc($result);
                            return $row['iMaxReference'];
                        }	
                    }
                    else
                    {
                        return false ;	
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
	
	function updateMaxNumBookingRef($data)
	{
		if(!empty($data))
		{
			/*
			$iRecordExist = $this->getMaximumUsedNumberByForwarder($data['idForwarder'],true);
			if($iRecordExist)
			{
				$query="
					UPDATE
						".__DBC_SCHEMATA_BOOKING_REFERENCE_LOGS__."
					SET
					   iMaxNumReference='".(int)substr($data['szBookingRef'],-3)."',
					   szBookingReference = '".mysql_escape_custom(trim($data['szBookingRef']))."'	
					WHERE
						 idForwarder = '".(int)$data['idForwarder']."'
					AND
						MONTH(dtCreatedOn) = MONTH(now())
					AND
						YEAR(dtCreatedOn) = YEAR(now())	
					";
			}
			else
			{
*/
                        if(empty($data['szReferenceType']))
                        {
                            $data['szReferenceType'] = 'BOOKING';
                        }
                        
                        if($data['szReferenceType']=='FILE')
                        {
                            $iMaxNumReference = substr($data['szBookingRef'],-4) ;
                        }
                        else
                        {
                            $iMaxNumReference = substr($data['szBookingRef'],-3) ;
                        }
			$query = "
				INSERT INTO
                                    ".__DBC_SCHEMATA_BOOKING_REFERENCE_LOGS__."
				(
                                    iMaxNumReference,
                                    szBookingReference,
                                    idForwarder,
                                    szReferenceType,
                                    dtCreatedOn
				)
				VALUES
				(
                                    '".mysql_escape_custom($iMaxNumReference)."',
                                    '".mysql_escape_custom($data['szBookingRef'])."',
                                    '".(int)$data['idForwarder']."',
                                    '".mysql_escape_custom($data['szReferenceType'])."', 
                                    now()
				)	
			";
			//}
			//echo "<br>".$query."<br>" ;
			if($result=$this->exeSQL($query))
			{
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	/*function deactivateForwarderAccount($idForwarder)
	{
		if((int)$idForwarder>0)
		{
			$query="
				UPDATE
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
				SET
					iActive='0'
				WHERE
					id='".(int)$idForwarder."'
			";
			$result = $this->exeSQL( $query );
			
			return true;
		}
		else
		{
			return false;
		}
	}*/
	
	function updateForwarder($data,$FILES)
	{
		if(!empty($data))
		{
			$this->set_szDisplayName(trim(sanitize_all_html_input($data['szDisplayName'])));
			$this->set_szLink(trim(sanitize_all_html_input($data['szLink'])));
			$this->set_id(trim(sanitize_all_html_input($data['idForwarder'])));
			$this->set_szGeneralEmailAddress(trim(sanitize_all_html_input($data['szGeneralEmailAddress'])));
			$this->set_szLogo(trim(sanitize_all_html_input($data['szForwarderLogo'])));
			
			$pdfFlag=false;
			if(!empty($this->szLogo))
			{ 
                            $pdfFlag=true;
                            $szLogoArr=explode("|||||",$this->szLogo);
                            if(count($szLogoArr)==3)
                            {
                                $this->szLogo=$szLogoArr[1];
                            }
                            $temp_file_path = __APP_PATH_FORWARDER_IMAGES_TEMP__.'/'.$this->szLogo ;
                            $forwarder_file_path = __APP_PATH_FORWARDER_IMAGES__.'/'.$this->szLogo ;
                            if(file_exists($temp_file_path))
                            {
                                $file_path = __APP_PATH_FORWARDER_IMAGES__.'/'.$this->szLogo ;
                                if(copy($temp_file_path,$file_path))
                                {
                                    if(file_exists($temp_file_path))
                                    {
                                        @unlink($temp_file_path);
                                    }
                                    $this->szLogoFileName = $this->szLogo ;
		        	}
		        	
                                $filetype=explode('.',$this->szLogo);
                                $img_formats_create_jpg=array('image/jpeg','image/jpg','image/pjpeg');

                                if(!in_array($filetype[1],$img_formats_create_jpg))
                                {
                                    $filetype=explode('.',$this->szLogo);
                                    $fileNamePdfLogo=$filetype[0]."_pdflogo.jpg";

                                    $old_name = __APP_PATH_FORWARDER_IMAGES__."/".$this->szLogo ;
                                    $new_name = __APP_PATH_FORWARDER_IMAGES__."/".$fileNamePdfLogo;
                                    $this->createForwarderLogoFor($old_name, $new_name,$this->szLogo); 
                                    //createAvtarThumbs(__APP_PATH_FORWARDER_IMAGES__."/",$this->szLogo,__APP_PATH_FORWARDER_IMAGES__."/",'80','30',$fileNamePdfLogo);
                                }
                                else
                                {
                                    $filetype=explode('.',$this->szLogo);
                                    $fileNamePdfLogo=$filetype[0].'_pdflogo.'.$filetype[1];
                                    $file_path=__APP_PATH_FORWARDER_IMAGES__.$this->szLogo;
                                    $temp_file_path=__APP_PATH_FORWARDER_IMAGES__.$fileNamePdfLogo;
                                    copy($file_path,$temp_file_path);
                                }
                            }
                            else if(file_exists($forwarder_file_path))
                            {
                                $this->szLogoFileName = $this->szLogo ;
                            }
				
			}
			else
			{
                            $this->szLogoFileName = $data['szOldLogo'] ;
			}
			
			if($this->szLogoFileName =='')
			{
				$this->addError( "szLogoFileName" , 'Forwarder Logo is required' );
				return false;
			}
                        
                        


			if($this->error===true)
			{
				return false;
			}
			
			if($data['iDeleteLogo']==1)
			{
				$this->deleteForwarderLogo($this->id);
			}
			
			$query="
				UPDATE
					".__DBC_SCHEMATA_FROWARDER__."
				SET
					szDisplayName = '".mysql_escape_custom(trim($this->szDisplayName))."',	
					szLink = '".mysql_escape_custom(trim($this->szLink))."',
					szLogo = '".mysql_escape_custom(trim($this->szLogoFileName))."',
					szGeneralEmailAddress = '".mysql_escape_custom(trim($this->szGeneralEmailAddress))."'
				WHERE
					id = '".$this->id."'	
			";
			 
			if($result = $this->exeSQL( $query ))
			{ 
				if($pdfFlag)
				{
					$query="
						UPDATE
							".__DBC_SCHEMATA_FROWARDER__."
						SET
							szPdfLogo= '".mysql_escape_custom(trim($fileNamePdfLogo))."'
						WHERE
							id = '".$this->id."'	
					";
					$result = $this->exeSQL( $query );
				}
				$this->isForwarderOnline($this->id,'UPDATE',true);
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function createForwarderLogoFor($originalFile, $outputFile,$file_name) 
	{  
            if(copy($originalFile, $outputFile))
            { 
                // READ WIDTH & HEIGHT OF ORIGINAL IMAGE
                list($current_width, $current_height) = getimagesize($originalFile);

                // CENTER OF GIVEN IMAGE, WHERE WE WILL START THE CROPPING
                $left = 0;
                $top = 0;
                $padding = 0;

                // BUILD AN IMAGE WITH CROPPED PART
                $new_canvas = @imagecreatetruecolor($current_width, $current_height);

                $filetype=explode(".",$file_name);
                $iFileCount = count($filetype) - 1 ;

                $ext=$filetype[$iFileCount];
                $ext = strtolower($ext);

                if($ext=='jpeg' || $ext=='jpg')
                {			     
                   $source = imagecreatefromjpeg($originalFile);  
                   $white = imagecolorallocate($new_canvas, 255, 255, 255);

                   imagefill($new_canvas, 0, 0, $white);  
                   imagecopy($new_canvas, $source, 0, 0, 0, 0, $current_width, $current_height); 
                }
                else if($ext=='gif')
                {
                   $source = imagecreatefromgif( $originalFile ); 
                   $white = imagecolorallocate($new_canvas, 255, 255, 255);

                   imagefill($new_canvas, 0, 0, $white);  
                   imagecopy($new_canvas, $source, 0, 0, 0, 0, $current_width, $current_height);  
                }
                else if($ext=='png')
                {			
                   $source = imagecreatefrompng( $originalFile ); 
                   $white = imagecolorallocate($new_canvas, 255, 255, 255);
                   imagefill($new_canvas, 0, 0, $white); 
                   imagecopy($new_canvas, $source, 0, 0, 0, 0, $current_width, $current_height);	  
                }  
                @imagejpeg($new_canvas, $outputFile, 100); 
                @imagedestroy($new_canvas);
                return true;
            }  
	}
	
	function png2jpg($originalFile, $outputFile, $quality) 
	{	
	    $source = imagecreatefrompng($originalFile);
	    $image = imagecreatetruecolor(imagesx($source), imagesy($source));
	
	    $white = imagecolorallocate($image, 255, 255, 255);
	    imagefill($image, 0, 0, $white);
	
	    imagecopy($image, $source, 0, 0, 0, 0, imagesx($image), imagesy($image));
	
	    imagejpeg($image, $outputFile, $quality);
	    imagedestroy($image);
	    imagedestroy($source);
	}
	function updateForwarderBankDetails($data)
	{
		if(!empty($data))
		{
			$this->load($data['idForwarder']);
			$idOldCurrency = $this->szCurrency;
			$this->set_szBankName(trim(sanitize_all_html_input($data['szBankName'])));
			$this->set_idBankCountry(trim(sanitize_all_html_input($data['idBankCountry'])));
			$this->set_szNameOnAccount(trim(sanitize_all_html_input($data['szNameOnAccount'])));			
			$this->set_iSortCode(trim(sanitize_all_html_input($data['iSortCode'])));
			$this->set_szSwiftCode(trim(sanitize_all_html_input($data['szSwiftCode'])));			
			$this->set_iAccountNumber(trim(sanitize_all_html_input($data['iAccountNumber'])));
			$this->set_szIBANAccountNumber(trim(sanitize_all_html_input($data['szIBANAccountNumber'])));			
			//$this->set_szCurrency(trim(sanitize_all_html_input($data['szCurrency'])));			
			$this->set_id(trim(sanitize_all_html_input($data['idForwarder']))); 
			if($this->error===true)
			{
                            return false;
			}
			if($this->checkForwarderComleteBankInfomation($this->id,1))
			{
                            $sendTransferDueEmail = true;
			}			
			$query="
                            UPDATE
                                ".__DBC_SCHEMATA_FROWARDER__."
                            SET
                                szBankName = '".mysql_escape_custom(trim($this->szBankName))."',	
                                idBankCountry = '".mysql_escape_custom(trim($this->idBankCountry))."',
                                szNameOnAccount = '".mysql_escape_custom(trim($this->szNameOnAccount))."',
                                iSortCode = '".mysql_escape_custom(trim($this->iSortCode))."',	
                                iAccountNumber = '".mysql_escape_custom(trim($this->iAccountNumber))."',
                                szIBANAccountNumber = '".mysql_escape_custom(trim($this->szIBANAccountNumber))."',
                                szSwiftCode = '".mysql_escape_custom(trim($this->szSwiftCode))."'
                            WHERE
                                id = '".$this->id."'	
			";
			//echo $query ;
			if($result = $this->exeSQL( $query ))
			{
                            if((boolean)$sendTransferDueEmail)
                            { 
                                /*
                                $replace_ary['szSubject']="Transfer due to ".$this->szDisplayName;
                                $replace_ary['szDisplayName']=$this->szDisplayName; 
                                $replace_ary['szBankNamae']=$this->szBankName;
                                $replace_ary['szSwift']=$this->szSwiftCode;
                                $replace_ary['szCountry']=$this->szCountryName;
                                $replace_ary['szAccountName']=$this->szNameOnAccount;
                                $replace_ary['szSortCode']=$this->iSortCode;
                                $replace_ary['szAccountNumber']=$this->iAccountNumber;
                                $replace_ary['szIBAN']=$this->szIBANAccountNumber;
                                $url=__BASE_MANAGEMENT_URL__."/forwarderTransfer/";
                                $replace_ary['szTransferDueUrl']="<a href=".$url.">Transfers Due</a>";
                                
                                $replace_ary['fAmount'] = $forwarderPaymentDueAry['szForwarderCurrency']." ".number_format((float)$forwarderPaymentDue['fTotalPriceForwarderCurrency'],2);
                                
                                $replace_ary['szEmail']=$toEmail;
                                 * createEmail(__FORWARDER_REFERRAL_FEE_PAYMENT__, $replace_ary,$toEmail,$replace_ary['szSubject'], __FINANCE_CONTACT_EMAIL__,(int)$_SESSION['forwarder_user_id'], __FINANCE_CONTACT_EMAIL__,__FLAG_FOR_MANAGEMENT__);
                                 * 
                                 */
                                $idForwarder = $this->id;
                                $forwarderPaymentDueAry = array();
                                $forwarderPaymentDueAry = $this->getForwarderTransferDueDetails($idForwarder);
                                $invoiceNumber = $forwarderPaymentDueAry['iBatchNo'];
                                
                                $this->load($idForwarder);
                                $dtPaymentScheduledDate = $forwarderPaymentDueAry['dtPaymentScheduled'];
                                
                                
                                
                                $replace_ary = array();
                                $replace_ary['szBank'] = $this->szBankName;
                                $replace_ary['szAccount'] = $this->iAccountNumber;
                                $replace_ary['sztotal'] = $forwarderPaymentDueAry['szForwarderCurrency']." ".number_format((float)$forwarderPaymentDueAry['fTotalPriceForwarderCurrency'],2);
                                $replace_ary['szTotal'] = $forwarderPaymentDueAry['szForwarderCurrency']." ".number_format((float)$forwarderPaymentDueAry['fTotalPriceForwarderCurrency'],2);
                                $replace_ary['szReferralfee'] = "";
                                $replace_ary['szTransferamount'] = $forwarderPaymentDueAry['szForwarderCurrency']." ".number_format((float)$forwarderPaymentDueAry['fTotalPriceForwarderCurrency'],2);
                                $replace_ary['szinvoce'] = $invoiceNumber;
                                $replace_ary['fUploadServiceAmountStr'] = $uploadServiceStr;
                                $replace_ary['szDate'] = date('j. F Y',strtotime('-1 second',strtotime(date('m').'/01/'.date('Y'))));
                                $replace_ary['szMonth'] = date('F', strtotime(date('Y-m',strtotime($dtPaymentScheduledDate))." -1 month"));
                                
                                $idRoleAry=array('3');
                                $kForwarder= new cForwarder();
                                $getEmailArr = $kForwarder->getForwarderCustomerServiceEmail($idForwarder,__PAYMENT_AND_BILLING_PROFILE_ID__);

                                $attacfilename = getForwarderTransferPDF($idForwarder,$invoiceNumber,'PDF',true);  
            
                                $toEmail= __FINANCE_CONTACT_EMAIL__;
                                $szAttachmentFriendlyFileName='TRANSFER_INVOICE';
                                createEmail(__TRANSFER_CONFIRMATION_EMAIL__, $replace_ary,$getEmailArr,'', __FINANCE_CONTACT_EMAIL__,$replace_ary['idUser'], __FINANCE_CONTACT_EMAIL__,__FLAG_FOR_FORWARDER__,true,0,$attacfilename,false,false,false,false,$szAttachmentFriendlyFileName);  
                            }
                            /*
                            * adding a record into forwarder transaction table as Starting Balance.
                            */
                            if(!($this->isTransactionExistByForwarderId($this->id)))
                            {
                                $this->addForwardertransaction($this->id);
                            }

                            /*
                            * Logging record if forwarder changes hist price. 
                            */

                            if(($idOldCurrency != $this->szCurrency) && (!empty($idOldCurrency)))
                            {
                                $this->addCurrencyChangeLogs($idOldCurrency,$this->szCurrency,$this->id);
                            }
                            $this->isForwarderOnline($this->id,'UPDATE',true);
                            return true;
			}
			else
			{
                            $this->error = true;
                            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                            return false;
			}
		}
		else
		{
			return false;
		}
	}
	function toggleForwarderStatus($idForwarder,$iStatus)
	{
		if($idForwarder>0)
		{
			$query="
				UPDATE 
					".__DBC_SCHEMATA_FROWARDER__."
				SET
					iActive = '".(int)$iStatus."'
				WHERE
					id = '".(int)$idForwarder."'
			";
			//echo "<br /> ".$query."<br />";
			if($result = $this->exeSQL( $query ))
			{
				$kWHSSearch = new cWHSSearch();
				
				if($iStatus==1)
				{
					$this->toggleForwarderContactByForwarderId($idForwarder,$iStatus);
				}
				else
				{
					$kWHSSearch->inactiveWarehouseByForwarderId($idForwarder);
					$kWHSSearch->inactiveUploadServicesByForwarderId($idForwarder);
					$this->toggleForwarderContactByForwarderId($idForwarder,$iStatus);
				}
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function deleteForwarder($idForwarder)
	{
		if($idForwarder>0)
		{
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_FROWARDER__."
				WHERE
					id = '".(int)$idForwarder."'
			";
			//echo "<br /> ".$query."<br />";
			if($result = $this->exeSQL( $query ))
			{
				$kWHSSearch = new cWHSSearch();
				$this->deleteAllServicesByForwarder($idForwarder);
				$kWHSSearch->deleteWarehouseByForwarderId($idForwarder);
				$kWHSSearch->deleteUploadServicesByForwarderId($idForwarder);	
				$this->deleteForwarderContactByForwarderId($idForwarder);
				
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function deleteAllServicesByForwarder($idForwarder)
	{
            if($idForwarder>0)
            {
                $kWHSSearch = new cWHSSearch();
                $warehouseAry = $kWHSSearch->getAllWareHousesByForwarder($idForwarder);

                if(!empty($warehouseAry))
                {
                        $warehouse_str = implode(",",$warehouseAry);
                }
                else
                {
                        // there is no CFS added by this forwarder so we assume there is no service offered as well.  
                        return false;
                }

                $query="
                        DELETE FROM
                                ".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
                        WHERE
                        (
                                idWarehouseFrom IN (".$warehouse_str.")
                        OR
                                idWarehouseTo IN (".$warehouse_str.")
                        )
                ";

                //echo "<br /> ".$query."<br />";
                if($result=$this->exeSQL($query))
                {
                        //successfully deleted
                }
                else
                { 
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                }

                $query="
                    DELETE FROM
                        ".__DBC_SCHEMATA_PRICING_CC__."
                    WHERE
                    (
                        idWarehouseFrom IN (".$warehouse_str.")
                    OR
                        idWarehouseTo IN (".$warehouse_str.")
                    )
                ";

                //echo "<br /> ".$query."<br />";

                if($result=$this->exeSQL($query))
                {
                    //successfully deleted
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    //return false;
                }			
                $this->deletePricingHaulageByWarehouse($warehouseAry);
            }
	}
	
	function deletePricingHaulageByWarehouse($warehouseAry)
	{
            if(!empty($warehouseAry))
            {
                $haulagePricingModelAry = array();
                $haulagePricingModelAry = $this->getAllHaulagePricingModels($warehouseAry);

                if(!empty($haulagePricingModelAry))
                {
                    $haulagepricing_str = implode(",",$haulagePricingModelAry);
                }
                else
                {
                    // there is no CFS added by this forwarder so we assume there is no service offered as well.
                    return false;
                }
			
                /*
                * Please dont try to delete data from all table by using join in a single query bcz it may produce some kind of data out of sync problem
                *  as currently i am facing thats why i have used seprate query for all deletion
                */
                $query="
                    DELETE FROM
                        ".__DBC_SCHEMATA_HAULAGE_ZONE__."
                    WHERE
                        idHaulagePricingModel IN (".$haulagepricing_str.")
                ";
                //echo "<br /> ".$query."<br />";
		
                if($result=$this->exeSQL($query))
                {
                    //successfully deleted
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                }
			
                $query="
                    DELETE FROM
                        ".__DBC_SCHEMATA_HAULAGE_PRICING__."
                    WHERE
                        idHaulagePricingModel IN (".$haulagepricing_str.")
                ";
                //echo "<br /> ".$query."<br />";
			
                if($result=$this->exeSQL($query))
                {
                    //successfully deleted
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                } 
			
                $query="
                    DELETE FROM
                        ".__DBC_SCHEMATA_HAULAGE_POSTCODE__."
                    WHERE
                        idHaulagePricingModel IN (".$haulagepricing_str.")
                ";
                //echo "<br /> ".$query."<br />"; 
                if($result=$this->exeSQL($query))
                {
                    //successfully deleted
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                }
			
                $query="
                    DELETE FROM
                        ".__DBC_SCHEMATA_HAULAGE_DISTANCE__."
                    WHERE
                        idHaulagePricingModel IN (".$haulagepricing_str.")
                ";
                //echo "<br /> ".$query."<br />";
			
                if($result=$this->exeSQL($query))
                {
                    //successfully deleted
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                }
						
                $query="
                    DELETE FROM
                        ".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_APPROVAL_DATA__."
                    WHERE
                       idHaulagePricingModel IN (".$haulagepricing_str.")
                ";
                //echo "<br /> ".$query."<br />";
			
                if($result=$this->exeSQL($query))
                {
                    //successfully deleted
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                }
			
                if(!empty($warehouseAry))
                {
                    $warehouse_str = implode(",",$warehouseAry);
                }
                else
                {
                    // there is no CFS added by this forwarder so we assume there is no service offered as well.
                    //return false;
                }
			
                $query="
                    DELETE FROM
                        ".__DBC_SCHEMATA_CFS_MODEL_MAPPING__."
                    WHERE
                       idWarehouse IN (".$warehouse_str.")
                ";
                //echo "<br /> ".$query."<br />";
		
                if($result=$this->exeSQL($query))
                {
                    //successfully deleted
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
               }
			
                $query="
                    DELETE FROM
                        ".__DBC_SCHEMATA_WAREHOUSE_COUNTRIES__."
                    WHERE
                       warehouseID IN (".$warehouse_str.")
                ";
                //echo "<br /> ".$query."<br />";
                if($result=$this->exeSQL($query))
                {
                    //successfully deleted
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                }
                $query="
                    DELETE FROM
                        ".__DBC_SCHEMATA_HAULAGE_COUNTRY__."
                    WHERE
                       idWarehouse IN (".$warehouse_str.")
                ";
                //echo "<br /> ".$query."<br />";
                if($result=$this->exeSQL($query))
                {
                    //successfully deleted
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                }

                $query="
                    DELETE FROM
                        ".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
                    WHERE
                       idWarehouse IN (".$warehouse_str.")
                ";
                //echo "<br /> ".$query."<br />";
                if($result=$this->exeSQL($query))
                {
                    //successfully deleted
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                }
			
                $query="
                    DELETE FROM
                        ".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_MODEl_APPROVAL_DATA__."
                    WHERE
                       idWarehouse IN (".$warehouse_str.")
                ";
                //echo "<br /> ".$query."<br />"; 
                if($result=$this->exeSQL($query))
                {
                    //successfully deleted
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                }
            }
	}
	
	function getAllHaulagePricingModels($haulagePricingModelAry)
	{
		if(!empty($haulagePricingModelAry))
		{
			$haulagepricing_str = implode(",",$haulagePricingModelAry);
		}
		else
		{
			return false;
		}
		
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
			WHERE
				idWarehouse IN (".$haulagepricing_str.")
		";
		//echo "<br><br>".$query."<br><br>";
		if($result = $this->exeSQL($query))
		{
			$wareHouseAry = array();
			$ctr = 0;
			while($row=$this->getAssoc($result))
			{
				$wareHouseAry[$ctr] = $row['id'];
				$ctr++;
			}
			return $wareHouseAry;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	function toggleForwarderContactByForwarderId($idForwarder,$iStatus)
	{
		if((int)$idForwarder>0)
		{
			if($iStatus ==1)
			{
				$query_and = " AND idForwarderContactRole= '".__ADMINISTRATOR_PROFILE_ID__."'";
			}
			
			$query="
				UPDATE
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
				SET
					iActive = '".(int)$iStatus."'
				WHERE
					idForwarder='".(int)$idForwarder."'
				$query_and
			";
			if($result=$this->exeSQL($query))
			{
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	function deleteForwarderContactByForwarderId($idForwarder)
	{
		if((int)$idForwarder>0)
		{
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
				WHERE
					idForwarder='".(int)$idForwarder."'
			";
			if($result=$this->exeSQL($query))
			{
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function getForwarderTransferDueDetails($idForwarder)
	{
            $query="
                SELECT 
                    id,
                    idForwarder,
                    fTotalPriceForwarderCurrency,
                    szCurrency,
                    szForwarderCurrency,
                    szInvoice,
                    szBooking,
                    dtCreatedOn,
                    iDebitCredit,
                    iStatus,
                    iBatchNo,
                    dtInvoiceOn,
                    fExchangeRate,
                    dtPaymentScheduled,
                    iFinancialVersion
                FROM
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                WHERE 
                    idForwarder	=".(int)$idForwarder."
                AND
                    iStatus=1
                AND
                    iDebitCredit = 2
                AND 
                    (iBatchNo !='' || iBatchNo !=0)			
            ";
            if($result=$this->exeSQL($query))
            {
                $row=$this->getAssoc($result);
                return $row;
            }
            else
            {
                return 0;
            }
	}
	function addCurrencyChangeLogs($idOldCurrency,$idCurrency,$idForwarder)
	{
		if($idOldCurrency>0 && $idCurrency>0 && $idForwarder>0)
		{
			$kWHSSearch = new cWHSSearch();
			
			if($idCurrency==1)
			{
				$szNewCurrency = 'USD';
				$fNewExchangeRate = 1.00 ;
			}
			else
			{
				$forwarderNewCurrencyAry = $kWHSSearch->getCurrencyDetails($idCurrency);
							
				if(!empty($forwarderNewCurrencyAry))
				{					
					$szNewCurrency = $forwarderNewCurrencyAry['szCurrency'];
					$fNewExchangeRate = $forwarderNewCurrencyAry['fUsdValue'];
				}
			}
			
			if($idOldCurrency==1)
			{
				$szOldCurrency = 'USD';
				$szOldExchangeRate = 1.00 ;
			}
			else
			{
				$forwarderOldCurrencyAry = $kWHSSearch->getCurrencyDetails($idOldCurrency);			
				if(!empty($forwarderOldCurrencyAry))
				{					
					$szOldCurrency = $forwarderOldCurrencyAry['szCurrency'];
					$szOldExchangeRate = $forwarderOldCurrencyAry['fUsdValue'];
				}
			}
			
			if($this->isCurrencyLogExist($idForwarder))
			{
				$query="
					UPDATE
						".__DBC_SCHEMATA_FORWARDER_CURRENCY_LOGS__."
					SET
						idOldCurrency = '".(int)$idOldCurrency."',
						szOldCurrency = '".mysql_escape_custom(trim($szOldCurrency))."',
						fOldCurrencyExhchangeRate = '".(float)$szOldExchangeRate."',
						idCurrency = '".(int)$idCurrency."',
						szCurrency = '".mysql_escape_custom(trim($szNewCurrency))."',
						fExchangeRate = '".(float)$fNewExchangeRate."'
					WHERE
						idForwarder = '".(int)$idForwarder."'
					AND
						iActive = '1'
				";
			}
			else
			{
				$query="
					INSERT INTO
						".__DBC_SCHEMATA_FORWARDER_CURRENCY_LOGS__."
					(
						idForwarder,
						idOldCurrency,
						szOldCurrency,	
						fOldCurrencyExhchangeRate,					
						idCurrency,
						szCurrency,
						fExchangeRate,					
						iActive
					)
					VALUES
					(
						'".(int)$idForwarder."',
						'".(int)$idOldCurrency."',
						'".mysql_escape_custom(trim($szOldCurrency))."',
						'".(float)$szOldExchangeRate."',
						'".(int)$idCurrency."',
						'".mysql_escape_custom(trim($szNewCurrency))."',
						'".(float)$fNewExchangeRate."',
						'1'
					)
				";
			}
			//echo "<br>".$query."<br>";
			if($result=$this->exeSQL($query))
			{
				true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function isCurrencyLogExist($idForwarder)
	{
		if($idForwarder>0)
		{
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_FORWARDER_CURRENCY_LOGS__."
				WHERE
					idForwarder = '".(int)$idForwarder."'
				AND
					iActive='1'	
			";
			//echo "<br>".$query;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function getAllForwarderWithCurrencyChanged()
	{	
		$query="
			SELECT
				id,
				idForwarder,
				idOldCurrency,
				szOldCurrency,	
				fOldCurrencyExhchangeRate,					
				idCurrency,
				szCurrency,
				fExchangeRate
			FROM
				".__DBC_SCHEMATA_FORWARDER_CURRENCY_LOGS__."
			WHERE
				iActive = '1'	
			AND
				idForwarder > 0
		";
		//echo "<br>".$query;
		if($result=$this->exeSQL($query))
		{
			$ret_ary = array();
			$ctr=0;
			while($row=$this->getAssoc($result))
			{
				$ret_ary[$ctr] = $row ;
				$ctr++;
			}
			return $ret_ary;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}	
	function isTransactionExistByForwarderId($idForwarder)
	{
		if($idForwarder>0)
		{
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
				WHERE
					idForwarder = '".(int)$idForwarder."'
			";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function deleteForwarderCurrencyLogs($idForwarderAry)
	{
		if(!empty($idForwarderAry))
		{
			$query="
				UPDATE
					".__DBC_SCHEMATA_FORWARDER_CURRENCY_LOGS__."
				SET
					iActive='0'
				WHERE
					idForwarder IN (".implode(',',$idForwarderAry).")	
			";
			//echo "<br>".$query."<br>";
			if($result=$this->exeSQL($query))
			{
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function addForwardertransaction($idForwarder)
	{
		if($idForwarder>0)
		{
			$this->load($idForwarder);
			$idCurrency = $this->szCurrency ;
			
			if($idCurrency==1)
			{
				$currencyExchangeRateAry['idCurrency']=1;
				$currencyExchangeRateAry['szCurrency'] = 'USD';
				$currencyExchangeRateAry['fExchangeRate'] = 1;
			}
			else
			{
				$kWHSSearch = new cWHSSearch();
				$resultAry = $kWHSSearch->getCurrencyDetails($idCurrency);								
				$currencyExchangeRateAry['idCurrency']=$resultAry['idCurrency'];
				$currencyExchangeRateAry['szCurrency'] = $resultAry['szCurrency'];
				$currencyExchangeRateAry['fExchangeRate'] = $resultAry['fUsdValue'];
			}
			
			$szDescription ='Starting Balance';
			
			$query="
				INSERT INTO
					".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
				(
					idForwarder, 
					fTotalPriceForwarderCurrency, 
					idForwarderCurrency, 
					szForwarderCurrency, 
					fForwarderExchangeRate, 
					iDebitCredit, 
					szBooking,				
					dtCreatedOn,
					dtPaymentConfirmed,
					iStatus
				)
				VALUES
				(
					'".(int)$idForwarder."',
				    '0.00',
					'".(int)$currencyExchangeRateAry['idCurrency']."',
					'".mysql_escape_custom(trim($currencyExchangeRateAry['szCurrency']))."',
					'".(float)$currencyExchangeRateAry['fExchangeRate']."',
					'4',
					'".mysql_escape_custom(trim($szDescription))."',
					NOW(),
					NOW(),
					'2'
			 	)
			 ";
			//echo $query;
			if($result = $this->exeSQL($query))
			{
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function uploadForwarderLogo($idForwarder,$FILES)
	{
            if(!empty($FILES))
            {
                $img_formats=array('image/jpeg','image/jpg','image/pjpeg','image/png','image/gif');
                $img_formats_create_jpg=array('image/jpeg','image/jpg','image/pjpeg');
                $pathToImages=__APP_PATH_FORWARDER_IMAGES_TEMP__.'/' ;
                $pathToThumbs=__APP_PATH_FORWARDER_IMAGES_TEMP__.'/' ;

                $this->set_szForwarderLogo($FILES['updateRegistComapnyAry']['name']['szForwarderLogo']);
                $filetype=explode('.',$FILES['updateRegistComapnyAry']['name']['szForwarderLogo']);

                $file_type=$FILES['updateRegistComapnyAry']["type"]['szForwarderLogo'];

                $filesize=($FILES['updateRegistComapnyAry']['size']['szForwarderLogo']/1000);

                if((!empty($FILES['updateRegistComapnyAry']['name']['szForwarderLogo'])) && (!in_array($file_type,$img_formats)))
                {
                        $this->addError('szForwarderLogo',".".$filetype[1].' is not an allowed file type');
                        return false;
                }
                /*
                if((!empty($FILES['updateRegistComapnyAry']['name']['szForwarderLogo']))&& (preg_match('/[^a-zA-Z0-9_. ]+/',$filetype[0]))   )
                {
                        $this->addError('szForwarderLogo',"File name with special characters are not allowed");
                        return false;
                }
*/
                if((!empty($FILES['updateRegistComapnyAry']['name']['szForwarderLogo']))&&($FILES['updateRegistComapnyAry']['size']['szForwarderLogo']>1048576))
                {
                    $this->addError('szForwarderLogo','Maximum file size allowed is 1MB');
                    return false;
                }
                if($this->error === true)
                {
                    return false;
                }
                $this->load($idForwarder);
                if(!empty($this->szLogo))
                {
                    if(file_exists($pathToImages.$this->szLogo))
                    {
                        unlink($pathToImages.$this->szLogo);
                    }
                }
                $filename=$FILES['updateRegistComapnyAry']['name']['szForwarderLogo'];
                $filetype=explode('.',$filename);
                $clean_file_name = preg_replace_callback('/[^a-zA-Z0-9_ -%][().][\/]/s', '_', $filetype[0]);
                $fileName=$clean_file_name.'_'.time().".".$filetype[1];

                if(move_uploaded_file($FILES['updateRegistComapnyAry']['tmp_name']['szForwarderLogo'],$pathToImages.$fileName))
                { 
                    /* 
                    if(file_exists($pathToImages.$filename))
                    {
                            unlink($pathToImages.$filename);
                    }
                    */
                    $this->szLogoFileName = $fileName ;
                    //$this->fileNamePdfLogo = $fileNamePdfLogo ;
                    return true;
        	}
        	/*
        	$query="
        		UPDATE
        			".__DBC_SCHEMATA_FROWARDER__."
        		SET
        		  szLogo = '".mysql_escape_custom(trim($szForwarderLogo))."'
        		WHERE
        			id = '".(int)$idForwarder."'  	
        	";
        	//echo "<br>".$query."<br>";
        	if($result=$this->exeSQL($query))
        	{
        		return true;
        	}
        	else
        	{
        		$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
        	}
			*/
		}
	}
	
	function deleteForwarderLogo($idForwarder)
	{
		if($idForwarder)
		{
			//$this->load($idForwarder);
			
			$szLogo='';
			$query="
				SELECT
					szLogo
				FROM
					".__DBC_SCHEMATA_FROWARDER__."
				WHERE
					id='".(int)$idForwarder."'
			";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					
					$szLogo=$row['szLogo'];
				}
			}
			
			if(!empty($szLogo))
			{
				$pathToImages=__APP_PATH_IMAGES__.'/forwarders/' ;
				$filename = $szLogo ;
				if(file_exists($pathToImages.$filename))
				{
					unlink($pathToImages.$filename);
				}
				
				$query="
	        		UPDATE
	        			".__DBC_SCHEMATA_FROWARDER__."
	        		SET
	        		  szLogo = ''
	        		WHERE
	        			id = '".(int)$idForwarder."'  	
	        	";
	        	//echo "<br>".$query."<br>";
	        	if($result=$this->exeSQL($query))
	        	{
	        		return true;
	        	}
	        	else
	        	{
	        		$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
	        	}
			}
			else
			{
				$this->addError('delete_logo','File does not exists');
				return false ;
			}
		}
	}
	
	function checkForwarderComleteCompanyInfo($idForwarder)
	{
		if($idForwarder>0)
		{
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_FROWARDER__."	
				WHERE
					id = '".(int)$idForwarder."'	
				AND
				(
					szDisplayName =''
				OR
					szCompanyName =''	
				OR
					szAddress =''	
				OR
					szCity =''	
				OR
					idCountry =''	 
				OR
					szPhone =''
				OR
					szCompanyRegistrationNum =''	
				OR
					szLink =''	
				OR
					szBankName =''
				OR
					szNameOnAccount =''	
				OR
					iAccountNumber =''		
				OR
					idBankCountry =''	
				OR
					szCurrency =''	
				OR
					szSwiftCode = ''
				OR
					szControlPanelUrl = '' 
				)	
			";
			//echo "<br>".$query."<br>" ;
			//die;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					/*if(!$this->isForwarderPreferencesExists($idForwarder,__BOOKING_PROFILE_ID__))
					{
						return true;
					}
					else if(!$this->isForwarderPreferencesExists($idForwarder,__PAYMENT_AND_BILLING_PROFILE_ID__))
					{
						return true;
					}
					else if(!$this->isForwarderPreferencesExists($idForwarder,__PAYMENT_AND_BILLING_PROFILE_ID__))
					{
						return true;
					}
					else
					{*/
						return false;
					//}
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function isForwarderPreferencesExists($idForwarder,$idForwarderRole)
	{
		if($idForwarderRole>0 && $idForwarder>0)
		{
			$query="
				SELECT 
					fp.id 
				FROM 
					".__DBC_SCHEMATA_FORWARDER_PREFERENCES__." fp 
				WHERE 
					fp.idForwarder='".(int)$idForwarder."' 
				AND 
					fp.idForwarderContactRole='".(int)$idForwarderRole."'
				AND
					szEmail !=''
			";
			//echo "<br>".$query."<br>";
		   if($result=$this->exeSQL($query))
		   {
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	//for forwarder feedback
	
	function forwarderFeedback($days,$idForwarder)
	{ 
		if($idForwarder>0)
		{ $days=(int)$days;
		$query="
			SELECT 
			COALESCE(SUM(IF(iTimely>0,1,0)),0)
			AS
				totalTimely,
			COALESCE(ROUND((SUM(IF(iTimely=1,1,0))/COALESCE(SUM(IF(iTimely>0,1,0)),0))*100),0) 
			AS 
				timelyYesPct,
			COALESCE(ROUND((SUM(IF(iTimely=2,1,0))/COALESCE(SUM(IF(iTimely>0,1,0)),0))*100),0)
			AS 
				timelyNoPct,	
			COALESCE(SUM(IF(iCourtous>0,1,0)),0)
			AS
				totalCourtous,
			COALESCE(ROUND((SUM(IF(iCourtous=1,1,0))/COALESCE(SUM(IF(iCourtous>0,1,0)),0))*100),0)
			AS
				corteousYesPct,
			COALESCE(ROUND((SUM(IF(iCourtous=2,1,0))/COALESCE(SUM(IF(iCourtous>0,1,0)),0))*100),0)
			AS
				corteousNoPct,
			count(iRating)
			AS
				totalRating,
			COALESCE(CONVERT((SUM(IF(iRating='1' or iRating='2',1,0))/count(iRating))*100,SIGNED INTEGER),0) AS negative,
			COALESCE(CONVERT((SUM(IF(iRating='3',1,0))/count(iRating))*100,SIGNED INTEGER),0) AS neutral,
			COALESCE(CONVERT((SUM(IF(iRating='4' or iRating='5',1,0))/count(iRating))*100,SIGNED INTEGER),0) AS positive  
				
		    FROM
				".__DBC_SCHEMATA_RATING_REVIEW__." 
			WHERE ";
		
			if($days>0)
			{
				$query.= "
					dtCompleted>=DATE_SUB(CURDATE(),INTERVAL $days DAY)
				AND "; 
			}
			
			$query.="
				dtCompleted<=DATE_ADD(CURDATE(),INTERVAL 1 DAY)
			AND
				idForwarder=".(int)$idForwarder."
		";
		// echo $query."<br>";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{ 
					$szFeedbackArr=array();
					while($row=$this->getAssoc($result))
					{
						$szFeedbackArr=$row;	
					}
				
					return $szFeedbackArr;	
				}
				else
				{	
					return array();
				}
			}
			else
			{   
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function forwarderReviewSugg($field,$idForwarder,$limit='')
	{	//$rating=(int)$rating;
		if($limit=='')
		{
			/*$dtInterval="
				AND
					(dtCompleted>=DATE_SUB(CURDATE(),INTERVAL 365 DAY)
				AND
					dtCompleted<=DATE_ADD(CURDATE(),INTERVAL 1 DAY))
					";*/
                    $dtInterval="";
		}
		else
		{
			$dtInterval="";
		}
		if($field=='szReview')
		{
		
			$sFields="";
			$suggession="";
			$rating=','.__DBC_SCHEMATA_RATING_REVIEW__.'.iRating';
		
		}
		else if($field='szSuggestion')
		{
			$sFields=
				"  ,".__DBC_SCHEMATA_BOOKING__.".szFirstName,
					".__DBC_SCHEMATA_BOOKING__.".szLastName,
					".__DBC_SCHEMATA_BOOKING__.".szEmail
				";
			$suggession=	
				"	
				INNER JOIN
					".__DBC_SCHEMATA_BOOKING__."
				ON
					".__DBC_SCHEMATA_RATING_REVIEW__.".idBooking=".__DBC_SCHEMATA_BOOKING__.".id	
			";
			$rating='';
		}
		
		$fieldSz=__DBC_SCHEMATA_RATING_REVIEW__.".".$field;
		if((int)$idForwarder>0)
		{
			 $query="
			 	SELECT 
			 		DATE_FORMAT(dtCompleted,'%d/%m/%Y') as reviewDate,	
			 		".$fieldSz."
			 		".$rating."
			 		".$sFields."
			 	FROM 
			 		".__DBC_SCHEMATA_RATING_REVIEW__."
			 		".$suggession."
			 	WHERE
			 		".__DBC_SCHEMATA_RATING_REVIEW__.".idForwarder='".(int)$idForwarder."'
			 	AND	
			 	(
			 		".$fieldSz."
			 	IS NOT NULL 
			 	AND 
			 		".$fieldSz."!=''
			 	) 
			 	".$dtInterval."
			 	ORDER BY 
			 		".__DBC_SCHEMATA_RATING_REVIEW__.".dtCompleted 
			 	DESC
					".$limit."	
			 	";
			 //ECHO $query;
				if($result=$this->exeSQL($query))
				{
					if($this->iNumRows>0)
					{
						$szReviewSuggestion=array();
						while($row=$this->getAssoc($result))
						{
							$szReviewSuggestion[]=$row;
						}
						return $szReviewSuggestion;
					//print_r($szReviewSuggestion);
					}
					else
					{
						return array();
					}
				}
				else
				{   
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
		}
		else
		{
			return array();
		}
	}
	function forwarderOperationReviewSuggs($field,$idForwarder,$limit='')
	{	//$rating=(int)$rating;
		if($limit=='')
		{
			$dtInterval="
				AND
					(dtCompleted>=DATE_SUB(CURDATE(),INTERVAL 6 MONTH)
				AND
					dtCompleted<=DATE_ADD(CURDATE(),INTERVAL 1 DAY))
					";
		}
		else
		{
			$dtInterval="";
		}
		if($field=='szReview')
		{
		
			$sFields="";
			$suggession="";
			$rating=','.__DBC_SCHEMATA_RATING_REVIEW__.'.iRating';
		
		}
		else if($field='szSuggestion')
		{
			$sFields=
				"  ,".__DBC_SCHEMATA_BOOKING__.".szFirstName,
					".__DBC_SCHEMATA_BOOKING__.".szLastName,
					".__DBC_SCHEMATA_BOOKING__.".szEmail
				";
			$suggession=	
				"	
				INNER JOIN
					".__DBC_SCHEMATA_BOOKING__."
				ON
					".__DBC_SCHEMATA_RATING_REVIEW__.".idBooking=".__DBC_SCHEMATA_BOOKING__.".id	
			";
			$rating='';
		}
		
		$fieldSz=__DBC_SCHEMATA_RATING_REVIEW__.".".$field;
		if((int)$idForwarder>0)
		{
			 $query="
			 	SELECT 
			 		DATE_FORMAT(dtCompleted,'%d/%m/%Y') as reviewDate,	
			 		".$fieldSz."
			 		".$rating."
			 		".$sFields."
			 	FROM 
			 		".__DBC_SCHEMATA_RATING_REVIEW__."
			 		".$suggession."
			 	WHERE
			 		".__DBC_SCHEMATA_RATING_REVIEW__.".idForwarder='".(int)$idForwarder."'
			 	AND	
			 	(
			 		".$fieldSz."
			 	IS NOT NULL 
			 	AND 
			 		".$fieldSz."!=''
			 	) 
			 	".$dtInterval."
			 	ORDER BY 
			 		".__DBC_SCHEMATA_RATING_REVIEW__.".dtCompleted 
			 	DESC
					".$limit."	
			 	";
			 //ECHO $query;
				if($result=$this->exeSQL($query))
				{
					if($this->iNumRows>0)
					{
						$szReviewSuggestion=array();
						while($row=$this->getAssoc($result))
						{
							$szReviewSuggestion[]=$row;
						}
						return $szReviewSuggestion;
					//print_r($szReviewSuggestion);
					}
					else
					{
						return array();
					}
				}
				else
				{   
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
		}
		else
		{
			return array();
		}
	}
	function forwarderOperationReviewSugg($field,$idForwarder,$limit='')
	{	//$rating=(int)$rating;
		if($limit=='')
		{
			$dtInterval="
				AND
					(dtCompleted>=DATE_SUB(CURDATE(),INTERVAL 6 MONTH)
				AND
					dtCompleted<=DATE_ADD(CURDATE(),INTERVAL 1 DAY))
					";
		}
		else
		{
			$dtInterval="";
		}
		if($field=='szReview')
		{
		
			$sFields="";
			$suggession="";
			$rating=','.__DBC_SCHEMATA_RATING_REVIEW__.'.iRating';
		
		}
		else if($field='szSuggestion')
		{
			$sFields=
				"  ,".__DBC_SCHEMATA_BOOKING__.".szFirstName,
					".__DBC_SCHEMATA_BOOKING__.".szLastName,
					".__DBC_SCHEMATA_BOOKING__.".szEmail
				";
			$suggession=	
				"	
				INNER JOIN
					".__DBC_SCHEMATA_BOOKING__."
				ON
					".__DBC_SCHEMATA_RATING_REVIEW__.".idBooking=".__DBC_SCHEMATA_BOOKING__.".id	
			";
			$rating='';
		}
		
		$fieldSz=__DBC_SCHEMATA_RATING_REVIEW__.".".$field;
		if($idForwarder>0)
		{
			 $query="
			 	SELECT 
			 		DATE_FORMAT(dtCompleted,'%d/%m/%Y') as reviewDate,	
			 		".$fieldSz."
			 		".$rating."
			 		".$sFields."
			 	FROM 
			 		".__DBC_SCHEMATA_RATING_REVIEW__."
			 		".$suggession."
			 	WHERE
			 		".__DBC_SCHEMATA_RATING_REVIEW__.".idForwarder='".$idForwarder."'
			 	AND	
			 	(
			 		".$fieldSz."
			 	IS NOT NULL 
			 	AND 
			 		".$fieldSz."!=''
			 	) 
			 	".$dtInterval."
			 	ORDER BY 
			 		".__DBC_SCHEMATA_RATING_REVIEW__.".dtCompleted 
			 	DESC
					".$limit."	
			 	";
			 //ECHO $query;
				if($result=$this->exeSQL($query))
				{
					if($this->iNumRows>0)
					{
						$szReviewSuggestion=array();
						while($row=$this->getAssoc($result))
						{
							$szReviewSuggestion[]=$row;
						}
						return $szReviewSuggestion;
					//print_r($szReviewSuggestion);
					}
					else
					{
						return array();
					}
				}
				else
				{   
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
		}
		else
		{
			return array();
		}
	}
	function downloadFeedbackSuggestion($suggestionReview,$idForwarder)
	{ 	
	
	$suggestionReview=trim(sanitize_all_html_input($suggestionReview));
	$idForwarder=trim(sanitize_all_html_input($idForwarder));
	
	if($idForwarder>0)
	{
		if($suggestionReview=='szReview')
		{
			$title="Review";
			$file_name = "Transporteca_Review_";
		}
		else if($suggestionReview=='szSuggestion')
		{
			$title="Suggestions";
			$file_name = "Transporteca_Suggestions_";
		}
		
		$data=$this->forwarderReviewSugg($suggestionReview,$idForwarder);
		//PRINT_R($data);
		$sheetIndex=0;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->createSheet();
		$objPHPExcel->setActiveSheetIndex($sheetIndex);
		$styleArray = array(
				'font' => array(
					'bold' => false,
					'size' =>12,
				),
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				),
				'borders' => array(
					'top' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'bottom' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'right' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'left' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
				),
			);
			
			$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->getStartColor()->setARGB('FFddd9c3');
			
		
			
			$col=2;		
			$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
			
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);;
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
			if($suggestionReview=='szReview')
			{
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,1,'Review Date');
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1,1,'Review Text');
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2,1,'Rating');
                                if(!empty($data))
                                {
                                    foreach($data as $key=>$value)
                                    {   //echo $value['reviewDate'];
                                            $objPHPExcel->getActiveSheet()->setCellValue('A'.$col,$value['reviewDate']);
                                            $objPHPExcel->getActiveSheet()->getStyle('A'.$col)->applyFromArray($styleArray);
                                            $objPHPExcel->getActiveSheet()->setCellValue('B'.$col,$value['szReview']);
                                            $objPHPExcel->getActiveSheet()->getStyle('B'.$col)->applyFromArray($styleArray);
                                            $objPHPExcel->getActiveSheet()->setCellValue('C'.$col,$value['iRating']);
                                            $objPHPExcel->getActiveSheet()->getStyle('C'.$col)->applyFromArray($styleArray);				
                                            $col++;
                                    }
                                }
			}
			if($suggestionReview=='szSuggestion')
			{
				$objPHPExcel->getActiveSheet()->getStyle('D1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$objPHPExcel->getActiveSheet()->getStyle('D1')->getFill()->getStartColor()->setARGB('FFddd9c3');
				
				$objPHPExcel->getActiveSheet()->getStyle('E1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$objPHPExcel->getActiveSheet()->getStyle('E1')->getFill()->getStartColor()->setARGB('FFddd9c3');
				
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,1,'Date');
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1,1,'Suggestion for improvement');
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2,1,'Customer First Name');	
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,1,'Customer Last Name');
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,1,'Customer e-mail');
				if(!empty($data))
                                {
                                    foreach($data as $key=>$value)
                                    {   //echo $value['reviewDate'];
                                            $objPHPExcel->getActiveSheet()->setCellValue('A'.$col,$value['reviewDate']);
                                            $objPHPExcel->getActiveSheet()->getStyle('A'.$col)->applyFromArray($styleArray);
                                            $objPHPExcel->getActiveSheet()->setCellValue('B'.$col,$value['szSuggestion']);
                                            $objPHPExcel->getActiveSheet()->getStyle('B'.$col)->applyFromArray($styleArray);
                                            $objPHPExcel->getActiveSheet()->setCellValue('C'.$col,$value['szFirstName']);	
                                            $objPHPExcel->getActiveSheet()->getStyle('C'.$col)->applyFromArray($styleArray);
                                            $objPHPExcel->getActiveSheet()->setCellValue('D'.$col,$value['szLastName']);	
                                            $objPHPExcel->getActiveSheet()->getStyle('D'.$col)->applyFromArray($styleArray);			

                                            $objPHPExcel->getActiveSheet()->setCellValue('E'.$col,$value['szEmail']);	
                                            $objPHPExcel->getActiveSheet()->getStyle('E'.$col)->applyFromArray($styleArray);
                                            $col++;
                                    }
                                }
			}
			$objPHPExcel->getActiveSheet()->setTitle($title);
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->removeSheetByIndex(1);
			$file=$file_name.date('Ymd');
			$fileName=__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/forwarderDetails/$file.xlsx";
			$objWriter->save($fileName);
			return $file;
			
		}
	
	}
	function feedbackDataDownload($id)
	{
		if($id)
		{
			$query="
			SELECT 
				DATE_FORMAT(dtCompleted,'%d/%m/%Y') as reviewDate,
			  	iTimely,
			  	iCourtous,
			  	iRating
		    FROM
				".__DBC_SCHEMATA_RATING_REVIEW__."
			WHERE 
				idForwarder='".(int)$id."'
			AND 
				dtCompleted>=DATE_SUB(CURDATE(),INTERVAL 365 DAY)
			ORDER BY 
				dtCompleted 
			DESC
				";
		//echo $query;	
		if($result=$this->exeSQL($query))
				{
					if($this->iNumRows>0)
					{
						$szFeedback=array();
						while($row=$this->getAssoc($result))
						{
							$szFeedback[]=$row;
						}
						return $szFeedback;
					//print_r($szReviewSuggestion);
					}
					else
					{
						return array();
					}
				}
				else
				{   
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}			
		}
	
	}
	function downloadFeedbackHistory($idForwarder)
	{
		if($idForwarder>0)
		{
			$title="Feedback";
			//$value=$this->forwarderFeedback(365,$idForwarder);
			$value=$this->feedbackDataDownload($idForwarder);
			//print_r($value);
			$sheetIndex=0;
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->createSheet();
			$objPHPExcel->setActiveSheetIndex($sheetIndex);
			
			$styleArray = array(
				'font' => array(
					'bold' => false,
					'size' =>12,
				),
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				),
				'borders' => array(
					'top' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'bottom' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'right' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'left' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
				),
			);
			
			//$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			//$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFill()->getStartColor()->setARGB('FFddd9c3');
			
			
			if(!empty($value))
			{	$col=2;
				
				$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFill()->getStartColor()->setARGB('FFddd9c3');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray);
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
					
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,1,'Rating Date');
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1,1,'Was your shipment available on time?');
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2,1,'Was the customer service prompt and courteous?');
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,1,'Rating');
				
				$i=2;
				
				foreach($value as $key=>$value)
				{
					//print_r($value);
					$timely=($value['iTimely'])?"Yes":"No";
					$courtous=($value['iCourtous'])?"Yes":"No";
					$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$value['reviewDate']);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$timely);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$courtous);	
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$value['iRating']);
					$i++;
				}
				
				$objPHPExcel->getActiveSheet()->setTitle($title);
				
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				
				$objPHPExcel->setActiveSheetIndex(0);
				$objPHPExcel->removeSheetByIndex(1);
				$file='Transporteca_Feedback_History_'.date('Ymd');
				$fileName=__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/forwarderDetails/$file.xlsx";
				$objWriter->save($fileName);
				return $file;
			}	
		}
	}
	function mostSellingLcl($case,$idForwarder)
	{
		$query="
			SELECT 
                            `f`.`idWarehouseFrom` AS `idWarehouseFrom`,
                            `f`.`idWarehouseTo` AS `idWarehouseTo`,
                            `w1`.`szWareHouseName` AS `warehouseFromName`,
                            `w1`.`szCity` AS `warehouseFormCity`,
                            `g`.`szCountryName` AS `countryFrom`,
                            `w2`.`szWareHouseName` AS `warehouseToName`,
                            `w2`.`szCity` AS `warehouseToCity`,
				`h`.`szCountryName` AS `countryTo` ,
				 CONCAT_WS(',',idWarehouseFrom,idWarehouseTo) AS iWhsGroup,
				 count(f.id)
	         FROM 
				".__DBC_SCHEMATA_BOOKING__." AS `f` 
			INNER JOIN 
				".__DBC_SCHEMATA_WAREHOUSES__."	AS w1
			ON	
				(w1.id = f.idWarehouseFrom)	
			INNER JOIN 
				".__DBC_SCHEMATA_WAREHOUSES__."	AS w2
			ON	
				(w2.id = f.idWarehouseTo)	
			INNER JOIN 
				".__DBC_SCHEMATA_COUNTRY__." AS `g`
			ON
				(`w1`.`idCountry`=`g`.`id`)
			INNER JOIN
	        	".__DBC_SCHEMATA_COUNTRY__." AS `h`
			ON
				(`w2`.`idCountry`=`h`.`id`)	
		    WHERE
		    	`f`.`idForwarder`='".(int)$idForwarder."'
		    AND 
				`f`.`idBookingStatus` IN ('3','4')
			AND 
				`f`.`idWarehouseFrom` > '0' 
			AND 
				`f`.`idWarehouseTo` > '0' 
                        AND  
	            (`f`.`dtBookingConfirmed` <= NOW()
			AND
				 `f`.`dtBookingConfirmed`>=NOW()- INTERVAL 30 DAY
				)		 	
			GROUP BY 
				iWhsGroup	
			ORDER BY 
				count(f.id) DESC 
			LIMIT 0,3
			";	
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$szDetails=array();
				while($row=$this->getAssoc($result))
				{  //print_r($row);
					$szDetail[]=$row;
				}
				
				return $szDetail;
			//print_r($szReviewSuggestion);
			}
			else
			{
				return array();
			}
		}
		else
		{   
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
			
	}
	function dashboardDetails($case,$idForwarder)
	{
	
	/*$query="
		SELECT 
			`idWarehouseFrom` ,
			`idWarehouseTo` 
		FROM 
			`tblbookings` 
		WHERE 
			`idWarehouseFrom`!='0' 
		AND 
			`idWarehouseTo`!='0' 
		GROUP BY 
			`idWarehouseFrom` 
		ORDER BY 
			`idWarehouseFrom`,
			`dtBookingConfirmed` 
		DESC 
		LIMIT 0,3
	";
		*/
		$args = func_get_args();
		//print_r($args);
		//echo "<br/>";
		SWITCH($case){
		
		
		CASE 'BOOKING_STATUS':
		//query for booking status
		
		$query="
			SELECT 
				COUNT(id) AS `booking`,
				COALESCE(SUM(`fTotalSelfInvoiceAmount`)/1000,0) AS `price`,
				COALESCE(SUM(`fTotalSelfInvoiceAmount`)/count(id),0) AS `average`
			FROM 
				".__DBC_SCHEMATA_BOOKING__."
			AS
				`f`
			WHERE
		    	`f`.`idForwarder`='".(int)$idForwarder."'
		    AND 
				`f`.`idBookingStatus` IN (3,4)
			AND 
				`f`.`idWarehouseFrom`!='0' 
			AND 
				`f`.`idWarehouseTo`!='0' 
			AND 
	            (`f`.`dtBookingConfirmed` <= NOW()
			AND
				 `f`.`dtBookingConfirmed`>=NOW()- INTERVAL ".(int)$args[4]." DAY
				)
			AND 
				`f`.`idWarehouseFrom`='".(int)$args[2] ."'
			AND
				`f`.`idWarehouseTo`='".(int)$args[3] ."'
			AND
				`f`.`iTransferConfirmed`='0'
			HAVING
				booking>0			 
			";
		//echo "<br>".$query;
		break;
			
		CASE 'LCL_SERVICES':
		//query for total lcl services
		$query="
			SELECT 
				COUNT(DISTINCT CONCAT_WS(',',p.idWarehouseFrom,p.idWarehouseTo))  as lclServices
			FROM 
				".__DBC_SCHEMATA_WAREHOUSES_PRICING__." p,
				".__DBC_SCHEMATA_WAREHOUSES__." w
			WHERE 
				w.id IN (p.idWarehouseFrom,p.idWarehouseTo)  
			AND	
				w.idForwarder='".(int)$idForwarder."' 
			AND 
				w.iActive = '1'
			AND
				p.iActive = '1'
			AND 
				p.dtExpiry >= NOW()	
                        AND
                            w.iWarehouseType = '".__WAREHOUSE_TYPE_CFS__."' 
			";
		//echo $query."<br>";
		break;
		
		CASE 'RATING':
		//query for rating
		$query="
			SELECT 
			COALESCE(TRUNCATE(AVG(`iRating`),1),0) as avgRating
			FROM 
				".__DBC_SCHEMATA_RATING_REVIEW__."
			WHERE 
				`idForwarder`='".(int)$idForwarder."' 
			AND 
				(`dtCompleted` 
				BETWEEN 
				NOW()- 
				INTERVAL 30 DAY 
				AND 
				NOW())
			";
		//echo "<br><br>".$query."<br>";
		break;
		
		CASE 'BOOKING_PER_MONTH':
		//TOTAL BOOKING per month
		
		$query="
			SELECT 
				COUNT(id) as totalBooking
			FROM
				".__DBC_SCHEMATA_BOOKING__."
			WHERE
				`idForwarder`='".(int)$args[1]."'
			AND 
				(`idBookingStatus`='3'
			OR
				`idBookingStatus`='4')		
			AND	
				YEAR(`dtBookingConfirmed`)='".(int)$args[3]."'
			AND	
				MONTH(`dtBookingConfirmed`)='".(int)$args[2]."'
			AND
				`iTransferConfirmed`='0'
			";
		//echo "<br><br>".$query."<br>";
		break;
		
		CASE 'SALES_PER_MONTH':
		//TOTAL SALES per month
		
		$query="
			SELECT 
			COALESCE(TRUNCATE(SUM(`fTotalSelfInvoiceAmount`)/1000,1),0) AS totalPrice
			FROM
				".__DBC_SCHEMATA_BOOKING__."
			WHERE
				`idForwarder`='".(int)$args[1]."'
			AND
				(`idBookingStatus`='3'
			OR
				`idBookingStatus`='4')			
			AND	
				YEAR(`dtBookingConfirmed`)='".(int)$args[3]."'
			AND	
				MONTH(`dtBookingConfirmed`)='".(int)$args[2]."'
			AND
				`iTransferConfirmed`='0'
			";
		//echo "<br><br>".$query."<br>";
		//echo "<br/>";
		break;
		}
		
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$szDetails=array();
				while($row=$this->getAssoc($result))
				{  //print_r($row);
					$szDetail=$row;
				}
				
				return $szDetail;
			//print_r($szReviewSuggestion);
			}
			else
			{
				return array();
			}
		}
		else
		{   
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
		
	}
	
	function displaySalesDetails($mode,$idForwarder)
	{
	$mode=$this->mode=sanitize_all_html_input(trim($mode));
	$bookingDetails=array();
		for($i=0;$i<=5;$i++)
		{	$year=date('n/Y', mktime(0, 0, 0, date('n')-$i, 1, date('Y')));
			$time=explode('/',$year);
			$key=strtoupper(date('M', mktime(0, 0, 0, $time[0], 1, 1)));
			$value=$this->dashboardDetails($mode,$idForwarder,$time[0],$time[1]);
			
			$bookingDetails[$key]=$value['totalPrice'];
		}
		$bookingDetails=array_reverse($bookingDetails);
		//print_r($bookingDetails);	
		return $this->createImageSales($bookingDetails,$idForwarder);
	}
	
	function displayBookingDetails($mode,$idForwarder)
	{
		$mode=$this->mode=sanitize_all_html_input(trim($mode));
		
		$bookingDetails=array();
			for($i=0;$i<=5;$i++)
			{	
				$year=date('n/Y', mktime(0, 0, 0, date('n')-$i, 1, date('Y')));
				$time=explode('/',$year);
				$key=strtoupper(date('M', mktime(0, 0, 0, $time[0], 1, 1)));
				
				$value=$this->dashboardDetails($mode,$idForwarder,$time[0],$time[1]);
				
				$bookingDetails[$key]=$value['totalBooking'];
				
			}
			$bookingDetails=array_reverse($bookingDetails);
			return $this->createImageBooking($bookingDetails,$idForwarder);
	}
	
	//function for creating graphical image of a data 
	function createImageBooking($values,$idForwarder)
	{
		# ------- The graph values in the form of associative array
		
		$img_width=460;
		$img_height=260; 
		$margins=30;
		
		
		$imageName="images".$idForwarder.".png";
		$imageAppPath=__APP_PATH_ROOT__."/forwarders/graph/".$imageName;
		//echo $imageAppPath."test";
		if (file_exists($imageAppPath)) {
			unlink($imageAppPath);
		}
	
	 
		# ---- Find the size of graph by substracting the size of borders
		//$graph_width=$img_width - $margins * 2;
		$graph_width = $img_width ;
		$graph_height=$img_height - $margins * 2; 
		$img=imagecreate($img_width,$img_height);
	
	 	
		$bar_width=45;
		$total_bars=count($values);
		$gap= ($graph_width- $total_bars * $bar_width ) / ($total_bars +1);
		$gap = 20;
		//echo $gap ;
	 
		# -------  Define Colors ----------------
		$bar_color=imagecolorallocatealpha($img,179,179,152,5);
		$border_color=imagecolorallocate($img,255,255,255);
		$base_line= imagecolorallocatealpha($img,125,116,104,5);
		$base_line_string = imagecolorallocatealpha($img,0,0,0,5);
		# ------ Create the border around the graph ------
		imagesetthickness($img, 2);
		imagefilledrectangle($img,0,0,$img_width,$img_height,$border_color);
	
	 
		# ------- Max value is required to adjust the scale	-------
		$max_value=max($values) + 10;
		$ratio= $graph_height/$max_value;
		imageline ( $img ,20,15, 20 , 230, $base_line);
		imageline ( $img ,20,15, 25 , 25, $base_line);
		imageline ( $img ,20,15, 15 , 25, $base_line);
		imageline ( $img ,440,230, 20 , 230, $base_line);
		
		# ----------- Draw the bars here ------
		//$font = 'arial.ttf';
		$margins2= 20 ;
		$font_file_name = __APP_PATH__.'/fonts/font.ttf';
		$font_file_name2 = __APP_PATH__.'/fonts/Calibri.ttf';
		for($i=0;$i< $total_bars; $i++)
		{ 
			# ------ Extract key and value pair from the current pointer position
			list($key,$value)=each($values);
			
			$x1= $margins2 + $gap + $i * ($gap+$bar_width) ;
			$x2= $x1 + $bar_width; 
			$y1=$margins +$graph_height- intval($value * $ratio) ;
			$y2=$img_height-$margins;
			
			imagettftext($img, 10, 0, $x1+15, $y1-5, $base_line_string, $font_file_name, number_format($value));
			//imagestring($img,3,$x1+15,$y1-15,number_format($value),$bar_color);
			//imagestring($img,4,$x1+4,$img_height-20,$key,$base_line_string);		
			imagettftext($img, 11, 0, $x1+10, $img_height-10, $base_line_string, $font_file_name2, $key);
			imagefilledrectangle($img,$x1,$y1,$x2,$y2,$base_line);
		}
		
		imagepng($img,"graph/images".$idForwarder.".png");
		imagedestroy($img);
		
		return "graph/images".$idForwarder.".png";
	}
	
	function createImageSales($values,$idForwarder)
	{
		//print_r($values);
		# ------- The graph values in the form of associative array
		
		$img_width=460;
		$img_height=260; 
		$margins=30;
		
		$imageName="image".$idForwarder.".png";
		$imageAppPath=__APP_PATH_ROOT__."/forwarders/graph/".$imageName;
		//echo $imageAppPath."test";
		if (file_exists($imageAppPath)) {
			unlink($imageAppPath);
		}
	
	 
		# ---- Find the size of graph by substracting the size of borders
		//$graph_width=$img_width - $margins * 2;
		$graph_height=$img_height - $margins * 2; 
		$img1=imagecreate($img_width,$img_height);
	
	 	$graph_width = $img_width ;
		$bar_width=45;
		$total_bars=count($values);
		//$gap= ($graph_width- $total_bars * $bar_width ) / ($total_bars +1);
		$gap = 20;
	 
		# -------  Define Colors ----------------
		$bar_color=imagecolorallocatealpha($img1,179,179,152,5);		
		$border_color=imagecolorallocate($img1,255,255,255);
		$base_line= imagecolorallocatealpha($img1,125,116,104,5);
		$base_line_string = imagecolorallocatealpha($img1,0,0,0,5);
		# ------ Create the border around the graph ------
		imagesetthickness($img1, 2);
		imagefilledrectangle($img1,0,0,$img_width,$img_height,$border_color);
	
	 
		# ------- Max value is required to adjust the scale	-------
		$max_value=max($values) + 10;
		$ratio= $graph_height/$max_value;
		imageline ( $img1 ,20,15, 20 , 230, $base_line);
		imageline ( $img1 ,20,15, 25 , 25, $base_line);
		imageline ( $img1 ,20,15, 15 , 25, $base_line);
		imageline ( $img1 ,440,230, 20 , 230, $base_line);
		
		# ----------- Draw the bars here ------
		$font_file_name = __APP_PATH__.'/fonts/font.ttf';
		$font_file_name2 = __APP_PATH__.'/fonts/Calibri.ttf';
		for($i=0;$i< $total_bars; $i++)
		{ 
			# ------ Extract key and value pair from the current pointer position
			list($key,$value)=each($values); 
			$x1= $margins + $gap + $i * ($gap+$bar_width) ;
			$x2= $x1 + $bar_width; 
			$y1=$margins +$graph_height- intval($value * $ratio) ;
			$y2=$img_height-$margins;
			
			imagettftext($img1, 10, 0, $x1+10, $y1-5, $base_line_string, $font_file_name, number_format((float)$value,1));
			imagettftext($img1, 11, 0, $x1+10, $img_height-10, $base_line_string, $font_file_name2, $key);
			
			//imagestring($img1,3,$x1+5,$y1-15,$value,$bar_color);
			//imagestring($img1,4,$x1+4,$img_height-20,$key,$base_line_string);		
			imagefilledrectangle($img1,$x1,$y1,$x2,$y2,$base_line);
		}
		
		imagepng($img1,"graph/image".$idForwarder.".png");
		imagedestroy($img1);
		
		return "graph/image".$idForwarder.".png";
	}
	
	function checkNonAcceptanceOfForwarder($szGoodsName=false,$idForwarder,$szEmail=false,$type='GOODS')
	{
		if($idForwarder>0)
		{
			if($type=='GOODS')
			{
				$query_and = "
				AND
					szNonAcceptance = '".mysql_escape_custom(trim($szGoodsName))."'
				AND
					iNonAcceptance = '1'
				";
			}
			else if($type=='EMAIL')
			{
				$query_and = "
				AND
					szNonAcceptance = '".mysql_escape_custom(trim($szEmail))."'
				AND
					iNonAcceptance = '2'
				";
			}
			
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_FORWARDER_NON_ACCEPTANCE__."
				WHERE					
					idForwarder = '".(int)$idForwarder."'	
				AND
					iActive='1'	
					$query_and		
			";
			//echo "<br>".$query."<br>";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function getAllNonAcceptedGoodsForForwarder($idForwarder)
	{
		if($idForwarder>0)
		{
			$query="
				SELECT
					id,
					szNonAcceptance
				FROM
					".__DBC_SCHEMATA_FORWARDER_NON_ACCEPTANCE__."
				WHERE
					iNonAcceptance = '2'
				AND
					idForwarder = '".(int)$idForwarder."'	
				AND
					iActive = '1'			
			";
			if($result=$this->exeSQL($query))
			{
				$nonAcceptedAry = array();
				if($this->iNumRows>0)
				{
					$ctr=0;
					while($row=$this->getAssoc($result))
					{
						$nonAcceptedAry[$ctr] = $row;
						$ctr++;
					}
					return $nonAcceptedAry;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function getAllNonAcceptedForwarderByEmail($szEmail)
	{
		if(!empty($szEmail))
		{
			$query="
				SELECT
					idForwarder
				FROM
					".__DBC_SCHEMATA_FORWARDER_NON_ACCEPTANCE__."
				WHERE
					iNonAcceptance = '1'
				AND
					szNonAcceptance = '".mysql_escape_custom(trim($szEmail))."'
				AND
					iActive='1'				
			";
			if($result=$this->exeSQL($query))
			{
				$nonAcceptedAry = array();
				if($this->iNumRows>0)
				{
					$ctr=0;
					while($row=$this->getAssoc($result))
					{
						$nonAcceptedAry[$ctr] = $row['idForwarder'];
						$ctr++;
					}
					return $nonAcceptedAry;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
function nonAcceptance($id,$mode)
	{ 
	$idForwarder   = sanitize_all_html_input(trim($id));
	$nonAcceptance = sanitize_all_html_input(trim($mode));
	if($idForwarder>0)
	{
		$query="
			SELECT
				`id`, 
				`szNonAcceptance`
			FROM 
				".__DBC_SCHEMATA_FORWARDER_NON_ACCEPTANCE__."	
			WHERE
				`iNonAcceptance`='".mysql_escape_custom($nonAcceptance)."'
			AND 
				`idForwarder`='".(int)$idForwarder."'
			AND
				`iActive`='1'
			ORDER BY
					`szNonAcceptance` ASC			
		";
		//echo $query;
	if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$szDetails=array();
				while($row=$this->getAssoc($result))
				{  
					$szDetail[]=$row;
				}
				
				return $szDetail;
			
			}
			else
			{
				return array();
			}
		}
		else
		{   
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	}
	function remove_nonacc($id,$idForwarder)
	{ 	$id			 = sanitize_all_html_input($id);
		$idForwarder = sanitize_all_html_input($idForwarder);
		if($idForwarder>0)
		{
			$query = "
				UPDATE
					".__DBC_SCHEMATA_FORWARDER_NON_ACCEPTANCE__."
					SET	
					`iActive`='0'
				WHERE
					id='".$id."'
				AND	
					idForwarder='".$idForwarder."'
			";
			//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($result)
			{	
				return true;
			
			}
		}
		else
		{   
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}	
		}
	}
	
	function add_nonacc( $id , $idForwarder,$validate )
	{	//echo $id;
		$idForwarder=sanitize_all_html_input(trim($idForwarder));
		if($validate=='2')
		{
		$errFld="limitationAlreadyExists";
		$this->set_szNonAcceptance(sanitize_all_html_input(trim($id)));
		$id=$this->sznonAcceptance;
		}
		if($validate=='1')
		{
		$errFld="emailAlreadyExists";
		$this->set_szNewEmail(sanitize_all_html_input(trim($id)));
		$id=$this->szNewEmail;
		}
		$idForwarder = sanitize_all_html_input(trim($idForwarder));
		if ($this->error === true)
		{
			return false;
		}
		
		//echo $id;
		if($idForwarder>0 &&($validate=='1' || $validate=='2'))
		{	
		
			$query="
				SELECT 
					`id`
				FROM 
					".__DBC_SCHEMATA_FORWARDER_NON_ACCEPTANCE__."
				WHERE
					idForwarder='".(int)$idForwarder."'
				AND
					szNonAcceptance='".(int)$id."'
				AND
					iNonAcceptance='".(int)$validate."'
				AND iActive='1'
					";
			//echo $query;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows==0)
				{	$query='';
					$query = "
						INSERT 
						INTO
							".__DBC_SCHEMATA_FORWARDER_NON_ACCEPTANCE__."
						SET	
							`iNonAcceptance`='".mysql_escape_custom($validate)."',
							szNonAcceptance='".mysql_escape_custom($id)."',
							idForwarder='".(int)$idForwarder."',
							dtCreated=NOW(),
							dtUpdated=NOW(),
							iActive='1'
						";
					//echo $query;
					if($result=$this->exeSQL($query))
					{		
							
							return true;
					}
					else
					{   
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					}
				}
				else
				{	
					$this->addError( 'szNonAcceptance' , t($this->t_base.'Error/'.$errFld) );
					return false;
				}	
			}	
			else
			{
				return false;
			}	
		}
		else
		{
			return false;
		}
	}
	function isHostNameExist($szHostUrl)
	{
            if(!empty($szHostUrl))
            {
                $query="
                    SELECT
                        id,
                        iProfitType
                    FROM
                        ".__DBC_SCHEMATA_FROWARDER__."
                    WHERE
                        szControlPanelUrl = '".mysql_escape_custom(trim($szHostUrl))."'
                    AND
                            iActive = 1		
                "; 
//                echo $query;
//                die;
                if($result=$this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        $row = $this->getAssoc($result);
                        $this->id = $row['id'];
                        $this->iProfitType = $row['iProfitType']; 
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                    return false;
            }
	}
	
	function getAllForwarderDetails($bCount=false)
	{ 
            if($bCount)
            {
                $query_where = "
                    WHERE
                        iActive = '1' 
                    AND
                        iAgreeTNC = '1' 
                ";  
                $query_select = "count(id) as iNumForwarder ";
            }
            else
            {
                $query_select = "*";
            }
            $query="
                SELECT
                    $query_select
                FROM
                    ".__DBC_SCHEMATA_FROWARDER__." 
                $query_where
                
            "; 
            //echo $query;
            if($result=$this->exeSQL($query))
            {
                if($bCount)
                {
                    $row = $this->getAssoc($result);
                    return $row['iNumForwarder'];
                }
                else
                {
                    while($row=$this->getAssoc($result))
                    {
                        $ret_ary[$ctr]=$row ;
                        $ctr++;
                    }
                    return $ret_ary ;
                } 
            }
	}
	function updateforwarder_test()
	{
		$query="
			UPDATE
				".__DBC_SCHEMATA_FROWARDER__."
			SET
				szControlPanelUrl = 'forwarder.transporteca.com'
			WHERE
				id = '7'	
		";
		$result=$this->exeSQL($query);
	}
	
	function isProfileInfoComplete($idForwarderContact)
	{
		if($idForwarderContact>0)
		{
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."	
				WHERE
					id = '".(int)$idForwarderContact."'	
				AND
				(
					szFirstName =''
				OR
					szLastName =''	
				OR
					idForwarder ='0'	
				OR
					idForwarderContactRole =''
				OR
					szPhone =''	
				OR
					szEmail =''	 
				)	
			";
			//echo "<br>".$query."<br>" ;
			//die;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$this->firstTimeLogInUpdateBookingFrequency($idForwarderContact);
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function firstTimeLogInUpdateBookingFrequency($idForwarderContact)
	{
		$query = "
				UPDATE
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
				SET
					szBookingFrequency=1
				WHERE
					id='".(int)$idForwarderContact."'
					AND szBookingFrequency = 0
			";
		if($result=$this->exeSQL($query))
		{
			return true;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	
	}
	
	function updateForwarderAgreement($idForwarder,$iAgree,$szAgreement,$flag=0,$mode = 0)
	{
		//echo $idForwarder;
		$logFeed  = false;
		if($mode == 1)
		{
			if($this->checkAgreeTNC((int)$idForwarder))
			{
				$logFeed = True;
			}
		}
	if( (int)$idForwarder>0 )
	{	
		if( $logFeed == false )
		{
			if($flag == 0)
			{
				$kForwarderContact = new cForwarderContact();
				$kForwarderContact->load($_SESSION['forwarder_user_id']);
				$szName=$kForwarderContact->szFirstName." ".$kForwarderContact->szLastName;
				$this->load($idForwarder);
				if((int)$_SESSION['forwarder_admin_id']>0)
				{
					$idAdmin = (int)$_SESSION['forwarder_admin_id'];
					$idForwarderContact = 0 ;
				}
				else
				{
					$idAdmin = 0;
					$idForwarderContact = (int)$_SESSION['forwarder_user_id'];
				}
			}
			else
			{
				$idForwarder = $idForwarder;
				$idForwarderContact = 0;
				$idAdmin = 0;
				$szName = 'Transporteca';
				$this->szDisplayName = '';
			}
			
			$query="
				INSERT INTO
					".__DBC_SCHEMATA_FORWARDER_AGREEMENT_LOG__."
				(
					szName,
					idForwarder,
					idForwarderContact,
					idAdmin,
					iAgree,
					iAgreementVersion,
					dtAgreement,
					szCompanyName
					
				)
					VALUES
				(
					'".mysql_escape_custom($szName)."',
					'".(int)$idForwarder."',
					'".(int)$idForwarderContact."',
					'".(int)$idAdmin."',
					'".(int)$iAgree."',
					'".mysql_escape_custom($szAgreement)."',
					NOW(),
					'".mysql_escape_custom($this->szDisplayName)."'
				)			
				
			";
			//echo $query;
			if(!$this->exeSQL($query))
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
				
			}
		}
			
			if($iAgree == 0 || $iAgree == 1)
			{
				if($iAgree == 0)
				{
					$query_add = ",
						isOnline = 0";
				}
				else
				{
					$query_add = "";
				}
				$query = "
					UPDATE
						".__DBC_SCHEMATA_FROWARDER__."
					SET
						iAgreeTNC='".(int)$iAgree."',
						dtAgreement=NOW(),
						iVersionUpdate='0',
						dtVersionUpdate=''
						".$query_add."
					WHERE
						id='".(int)$idForwarder."'
				";
				//echo $query;
				if($result=$this->exeSQL($query))
				{
					$bForwarderOnline = false;
					if($this->isForwarderOnline($idForwarder,'UPDATE',true))
					{
						$bForwarderOnline = true;
					}
					/*
					if($iAgree==1)
					{
						if($kForwarder->iAgreeTNC==1 && $kForwarder->iUpdateRss==2)
						{
							/*
							$kConfig = new cConfig();
							$kWhsSearch = new cWHSSearch();
							
							$kWhsSearch->load($this->idOriginWarehouse);
							$szOriginCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
							
							$kWhsSearch->load($this->idDestinationWarehouse);
							$szDestinationCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
							
							$szForwarderNameList = $this->getAllActiveForwarderNameList($idForwarder);
							$rssDataAry=array();
							$rssDataAry['idForwarder'] = $kForwarder->id ;
							$rssDataAry['szFeedType'] = '__BULK_LCL_SERVICES__';
							$rssDataAry['szLink'] = __RSS_FEED_PAGE_URL__ ;
							$rssDataAry['szTitle'] = ' We are pleased to welcome '.$kForwarder->szDisplayName.' on Transporteca ';
							$rssDataAry['szDescription'] = $kForwarder->szDisplayName.' is now online for booking on Transporteca. We are pleased to welcome one more Freight Forwarder. See what they offer on <a href="'.__MAIN_SITE_HOME_PAGE_URL__.'">'.__MAIN_SITE_HOME_PAGE_URL__.'</a>.';
							$rssDataAry['szDescriptionHTML'] = " A warm welcome to ".$kForwarder->szDisplayName."! <br />".$kForwarder->szDisplayName." has now joined a group of freight forwarders standing out from the industry in their belief that transparency and simplicity is the road to a better customer experience. They offer their services here on Transporteca so you can compare and book your preferred solution in only a few clicks. Other associated freight forwarders include: <br />".$szForwarderNameList;
							$kRss = new cRSS();
							$kRss->addRssFeed($rssDataAry);
						}
					}
					*/
					return true;
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
			}
			elseif($iAgree == 2)
			{		
				$query = "
					UPDATE
						".__DBC_SCHEMATA_FROWARDER__."
					SET
						dtAgreement=NOW(),
						iVersionUpdate='1',
						dtVersionUpdate=NOW()
					WHERE
						id='".(int)$idForwarder."'
				";
				//echo $query;
				if($result=$this->exeSQL($query))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
	}
	function getAllActiveForwarderNameList($idForwarder=false,$flag=false)
	{
		if($idForwarder>0)
		{
			$query_and = " AND id!='".(int)$idForwarder."'" ;
		}
		$ord_query="";
		if($flag)
		{
			$ord_query="
				ORDER BY
					szDisplayName ASC
			";
		}
		$query="
			SELECT
				CONCAT_WS(',',szDisplayName) as szForwarderList
			FROM
				".__DBC_SCHEMATA_FROWARDER__."
			WHERE
				iActive = '1'
				$query_and
			AND
				iAgreeTNC = '1'
			AND
				isOnline = '1'
			$ord_query
		";
		//echo $query ;
		if($result = $this->exeSQL( $query ))
		{
			if ($this->getRowCnt() > 0)
			{
				$szForwarderNameList = '';
				while($row = $this->getAssoc( $result))
				{
						$szForwarderNameList .=$row['szForwarderList']."<br />";
				}
				return $szForwarderNameList;
			}
			else
			{
				return array();
			}	
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function selectVersion()
	{	
		$query="
			SELECT
				dtVersionUpdated
			FROM	
				".__DBC_SCHEMATA_MANAGEMENT_TERMS_VERSION__." 
			LIMIT 0,1	
			";
		if($result = $this->exeSQL( $query ))
		{
			if ($this->getRowCnt() > 0)
			{
				$row = $this->getAssoc( $result);	
				return $row;
			}
			else
			{
				return array();
			}	
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function getForwarderAgreement($idForwarder)
	{
		if((int)$idForwarder>0)
		{
			$query="
				SELECT 
					*					
				FROM
					".__DBC_SCHEMATA_FORWARDER_AGREEMENT_LOG__."
				WHERE
					idForwarder='".(int)$idForwarder."'			
			";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					while($row=$this->getAssoc($result))
					{  
						$szForwarderAgreementArr[]=$row;
					}
					
					return $szForwarderAgreementArr;
				}
				else
				{
					return array();
				}
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	function getMaxTncOrder()
	{	
				
		$query="
			SELECT 
				COUNT(*)  as orders  
			FROM 
				".__DBC_SCHEMATA_MANAGEMENT_TERMS__."
			";
		
		if($result=$this->exeSQL($query))
		{
			$row = $this->getAssoc( $result );
			if($row['orders']>0)
			{
				return $row['orders']+1;
			}
			else
			{
				return 1;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	function getTermsConditionAcceptedByUserDetails($idForwarder)
	{
		if($idForwarder>0)
		{
			$query="
				SELECT
					fc.szFirstName,
					fc.szLastName,
					log.iAgreementVersion,
					log.dtAgreement
				FROM
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__." fc
				INNER JOIN
					".__DBC_SCHEMATA_FORWARDER_AGREEMENT_LOG__." log
				ON
					log.idForwarderContact = fc.id
				WHERE
					log.idForwarder = '".(int)$idForwarder."'	
				AND
					log.iAgree = '1'
				ORDER BY
					log.id DESC
				LIMIT
					0,1							
			";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					return $row;
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() forwarder id is required : MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	function detailsForwarder($id)
	{
	if($id>0)
		{
			$query="
				SELECT
					fwd.szCompanyName,
					fwd.szAddress,
					fwd.szAddress2,
					fwd.szAddress3,
					fwd.szCity,
					fwd.szState,
					fwd.szControlPanelUrl,
					c.szCountryName,
                                        fwd.szVatRegistrationNum,
                                        fwd.iCreditDays
				FROM
					".__DBC_SCHEMATA_FROWARDER__." AS fwd
				INNER JOIN
					".__DBC_SCHEMATA_COUNTRY__."	AS c
				ON
					(c.id=fwd.idCountry)	
				WHERE
					fwd.id='".(int)$id."'				
			";
			//echo $query;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					return $row;
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() forwarder id is required   ";
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	
	}
	function detailsForwarderBillingEmail($id,$flag=false)
	{
		if($id>0)
		{
			$sql='';
			$limit='';
			if($flag)
			{
				$sql .="
					AND 
						c.idForwarderContactRole='1'
				";
				$limit="LIMIT 0,1";
			}
			else
			{
				$sql .="
					AND 
						c.idForwarderContactRole='5'
				";
			}
			
			$query="
				SELECT 
					c.szEmail 
				FROM
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."	AS c
				WHERE
					c.idForwarder='".(int)$id."'	
				$sql
				$limit
			";	
			//echo $query;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{	
					$email=array();
					while($row=$this->getAssoc($result))
					{
						$emails[]=$row;
					}
					return $emails;
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() forwarder id is required  ";
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	
	}
	
	function detailsTransferPdfForwarder($id)
	{
	if($id>0)
		{
			$query="
				SELECT
					fwd.szCompanyName,
					fwd.szBankName,
					fwd.szSwiftCode,
					fwd.iAccountNumber,
					fwd.szControlPanelUrl,
					c.szCurrency as settling,
                                        fwd.iCreditDays
				FROM
					".__DBC_SCHEMATA_FROWARDER__." AS fwd
				INNER JOIN
					".__DBC_SCHEMATA_CURRENCY__."	AS c
				ON
					(c.id=fwd.szCurrency)	
				WHERE
					fwd.id='".(int)$id."'				
			";
			//echo $query;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					return $row;
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() forwarder id is required   ";
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	
	}
	
	function getWarehouseExpiryRateForWTW($flag=true)
	{
		if($flag)
		{
			$datefrom =date('Y-m-d',strtotime('+'. __FROM_DATE__));
			$dateto =date('Y-m-d',strtotime('+'. __TO_DATE__));
			
			$sql .="
					date(dtExpiry)>='".mysql_escape_custom($datefrom)."'
				AND
					date(dtExpiry)<='".mysql_escape_custom($dateto)."'";
		}
		else
		{
			$today=date('Y-m-d');
			$sql .="
					date(dtExpiry)='".mysql_escape_custom($today)."'";
		}
		$kWHSSearch=new cWHSSearch();
		$kConfig = new cConfig();
		$query="
			SELECT
				idWarehouseFrom,
				idWarehouseTo,
				dtExpiry
			FROM
				".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
			WHERE
				".$sql."
			AND
				iActive='1'
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$ctr=0;
				$forwarderArr=array();
				while($row=$this->getAssoc($result))
				{
					$counter=0;
					
					$kWHSSearch->load($row['idWarehouseFrom']);
					$idForwarder=$kWHSSearch->idForwarder;
					if(in_array($idForwarder,$forwarderArr))
					{
						$ctr=count($forwarderWarehouseArr[$idForwarder]);
					}
					else
					{
						$ctr=0;
					}
					$warehouseArr=array();
					$warehouseArr[$ctr]['expiryDate']=$row['dtExpiry'];
					$warehouseArr[$ctr]['fromWarehouseCity']=$kWHSSearch->szCity;
					$warehouseArr[$ctr]['originWarehouseName']=$kWHSSearch->szWareHouseName;
					$warehouseArr[$ctr]['fromWarehouseCountry']=$kConfig->getCountryName($kWHSSearch->idCountry);
					$kWHSSearch->load($row['idWarehouseTo']);
					$warehouseArr[$ctr]['toWarehouseCity']=$kWHSSearch->szCity;
					$warehouseArr[$ctr]['desWarehouseName']=$kWHSSearch->szWareHouseName;
					$warehouseArr[$ctr]['toWarehouseCountry']=$kConfig->getCountryName($kWHSSearch->idCountry);
					if(in_array($idForwarder,$forwarderArr))
					{
						$counter=count($forwarderWarehouseArr[$idForwarder]);
						$forwarderWarehouseArr[$idForwarder][$counter]=$warehouseArr;
					}
					else
					{
						$forwarderArr[]=$idForwarder;
						$forwarderWarehouseArr[$idForwarder][$counter]=$warehouseArr;
					}
					//print_r($warehouseArr);
					//echo "<br /><br />";
					++$ctr;
				}
				return $forwarderWarehouseArr;
				//print_r(($forwarderWarehouseArr[3]));
			}
		}
		else
		{
			
		}
	}
	
	function getWarehouseExpiryRateForHaulage($flag=true)
	{
		if($flag)
		{
			$datefrom =date('Y-m-d',strtotime('+'. __FROM_DATE__));
			$dateto =date('Y-m-d',strtotime('+' .__TO_DATE__));
			
			$sql .="
					date(dtExpiry)>='".mysql_escape_custom($datefrom)."'
				AND
					date(dtExpiry)<='".mysql_escape_custom($dateto)."'";
		}
		else
		{
			$today=date('Y-m-d');
			$sql .="
					date(dtExpiry)='".mysql_escape_custom($today)."'";
		}
		$kWHSSearch=new cWHSSearch();
		$kConfig = new cConfig();
		$query="
			SELECT
				idWarehouse,
				idDirection,
				dtExpiry
			FROM
				".__DBC_SCHEMATA_PRICING_HAULAGE__."
			WHERE
				".$sql."
			AND
				iActive='1'
		";
		echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$ctr=0;
				$forwarderArr=array();
				while($row=$this->getAssoc($result))
				{
					$counter=0;
					
					$kWHSSearch->load($row['idWarehouse']);
					$idForwarder=$kWHSSearch->idForwarder;
					if(in_array($idForwarder,$forwarderArr))
					{
						$ctr=count($forwarderWarehouseArr[$idForwarder]);
					}
					else
					{
						$ctr=0;
					}
					$warehouseArr=array();
					$warehouseArr[$ctr]['expiryDate']=$row['dtExpiry'];
					if($row['idDirection']==2)
					{
						$idDirection='Import';
					}
					if($row['idDirection']==1)
					{
						$idDirection='Export';
					}
					$warehouseArr[$ctr]['idDirection']=$idDirection;
					$warehouseArr[$ctr]['fromWarehouseCity']=$kWHSSearch->szCity;
					$warehouseArr[$ctr]['originWarehouseName']=$kWHSSearch->szWareHouseName;
					$warehouseArr[$ctr]['fromWarehouseCountry']=$kConfig->getCountryName($kWHSSearch->idCountry);
					if(in_array($idForwarder,$forwarderArr))
					{
						$counter=count($forwarderWarehouseArr[$idForwarder]);
						$forwarderWarehouseArr[$idForwarder][$counter]=$warehouseArr;
					}
					else
					{
						$forwarderArr[]=$idForwarder;
						$forwarderWarehouseArr[$idForwarder][$counter]=$warehouseArr;
					}
					//print_r($warehouseArr);
					//echo "<br /><br />";
					++$ctr;
				}
				
				return $forwarderWarehouseArr;
				//print_r(($forwarderWarehouseArr[3]));
				
			}
		}
		else
		{
			
		}
	}
	
	function getAllForwardersContactEmail($idForwarder,$idRoleAry)
	{
		if($idForwarder>0)
		{
			if(is_array($idRoleAry) && !empty($idRoleAry))
			{
				$idRoleStr = implode(',',$idRoleAry);
				$query_and = "AND
					idForwarderContactRole IN (".$idRoleStr.") ";
			}
			else
			{
				$query_and = "AND
					idForwarderContactRole = '".$idRoleAry."' ";
			}
			
			$query="
				SELECT
					szEmail
				FROM
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
				WHERE
					idForwarder = '".(int)$idForwarder."'	
				AND
					iActive = '1'	
				$query_and	
			";
			//echo $query ;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$ret_ary = array();
					$ctr = 0;
					while($row=$this->getAssoc($result))
					{
						$ret_ary[] = $row['szEmail'];
						$ctr++ ;
					}
					return $ret_ary;
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function getForwarderArr($iAvailableForManualQuotes=false,$forwarderAry=array(),$bIDKey=false)
	{
            if($iAvailableForManualQuotes)
            {
                $query_and = " AND iAvailableForManualQuotes =1 ";
            } 
            if(!empty($forwarderAry))
            {
                $idstr_and_str = implode(",",$forwarderAry); 
                if(!empty($idstr_and_str))
                {
                    $query_and = " AND id IN (".$idstr_and_str.") ";
                }
            } 
            
            $query="
                SELECT
                    id,
                    szControlPanelUrl,
                    szDisplayName, 
                    iProfitType,
                    szCurrency,
                    szForwarderAlias,
                    szLogo,
                    idCountry,
                    iOffLineQuotes,
                    iDisplayQuickQuote
                FROM
                    ".__DBC_SCHEMATA_FROWARDER__."
                WHERE
                    iActive='1' 
                   $query_and
                ORDER BY
                    szDisplayName ASC, szForwarderAlias ASC 
            ";
            //echo $query;
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $forwarder_ary = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {
                        if($bIDKey)
                        {
                            $forwarder_ary[$row['id']] = $row; 
                        }
                        else
                        {
                            $forwarder_ary[$ctr] = $row;
                            $ctr++;
                        } 
                    }
                    return $forwarder_ary;
                }
                else
                {
                    return array();
                }
            }
	}
	function getForwarderPaymentFreqArr($flag)
	{
		$query="
			SELECT
				id,
				szControlPanelUrl,
				szDisplayName
			FROM
				".__DBC_SCHEMATA_FROWARDER__."
			WHERE
				iActive='1'	 
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$forwarder_ary = array();
				while($row=$this->getAssoc($result))
				{
					$forwarder_ary[] = $row;
				}
				return $forwarder_ary;
			}
			else
			{
				return array();
			}
		}
	}
	
	function getAllBookingForForwarder($idForwarder,$flag)
	{
		if($flag=='daily')
		{
                    $prevdate=date('Y-m-d',strtotime('- 1 DAY'));

                    $sql .="
                        AND
                            date(b.dtBookingConfirmed)='".mysql_escape_custom($prevdate)."'
                    ";
		}
		if($flag=='weekly')
		{
                    $prevdate=date('Y-m-d',strtotime('- 1 DAY'));
                    $sevenDayDate=date('Y-m-d',strtotime(' - 7 Day'));
                    $sql .="
                        AND
                            date(b.dtBookingConfirmed)>='".mysql_escape_custom($sevenDayDate)."'
                        AND
                            date(b.dtBookingConfirmed)<='".mysql_escape_custom($prevdate)."'
                    ";
		}
		
		if($flag=='monthly')
		{
                    $current_month=date('m');
                    $current_year=date('Y');
                    $lastmonth=$current_month-1;
                    if ($current_year < 0) $current_year = 0+date("Y");
                    $aMonth = mktime(0, 0, 0, $lastmonth, 1, $current_year);
                    $NumOfDay = 0+date("t", $aMonth);
                    $LastDayOfMonth = mktime(0, 0, 0, $lastmonth, $NumOfDay, $current_year);
                    $fisrtDayOfMonth = mktime(0, 0, 0, $lastmonth, 1, $current_year);

                    $LastDateOfMonth=date('Y-m-d',$LastDayOfMonth);
                    $firstDateOfMonth=date('Y-m-d',$fisrtDayOfMonth);

                    $sql .="
                        AND
                            date(b.dtBookingConfirmed)>='".mysql_escape_custom($firstDateOfMonth)."'
                        AND
                            date(b.dtBookingConfirmed)<='".mysql_escape_custom($LastDateOfMonth)."'
                    ";
		}
                
		$query="
                    SELECT
                        (SELECT c1.szCountryName FROM ".__DBC_SCHEMATA_COUNTRY__." c1 WHERE c1.id = b.idOriginCountry) as szOriginCountry,
                        (SELECT c2.szCountryName FROM ".__DBC_SCHEMATA_COUNTRY__." c2 WHERE c2.id = b.idDestinationCountry) as szDestinationCountry,
                        b.szBookingRef,
                        b.fCargoVolume,
                        b.fCargoWeight,
                        b.iQuotesStatus,
                        b.idTransportMode,
                        b.szTransportMode
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__." b
                    WHERE
                        b.idBookingStatus IN ('3','4')
                    AND
                        b.idForwarder='".(int)$idForwarder."'
                    AND
                        b.iTransferConfirmed = '0'
                    ".$sql."
                    ORDER BY
                            b.id ASC
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$forwarderbooking_ary = array();
				while($row=$this->getAssoc($result))
				{
					$forwarderbooking_ary[] = $row;
				}
				return $forwarderbooking_ary;
			}
			else
			{
				return array();
			}
		}
		
	}
	
	function getExpectedServieces($flag)
	{
		if($flag=='daily')
		{
			$prevdate=date('Y-m-d',strtotime('- 1 DAY'));
			$sql .="
				date(dtDateTime)='".mysql_escape_custom($prevdate)."'
			";
		}
		if($flag=='weekly')
		{
			$prevdate=date('Y-m-d',strtotime('- 1 DAY'));
			$sevenDayDate=date('Y-m-d',strtotime(' - 7 Day'));
			$sql .="
					date(dtDateTime)>='".mysql_escape_custom($sevenDayDate)."'
				AND
					date(dtDateTime)<='".mysql_escape_custom($prevdate)."'
			";
		}
		if($flag=='monthly')
		{
                    $current_month=date('m');
                    $current_year=date('Y');
                    $lastmonth=$current_month-1;
                    if ($current_year < 0) $current_year = 0+date("Y");
                    $aMonth  = mktime(0, 0, 0, $lastmonth, 1, $current_year);
                    $NumOfDay       = 0+date("t", $aMonth);
                    $LastDayOfMonth = mktime(0, 0, 0, $lastmonth, $NumOfDay, $current_year);
                    $fisrtDayOfMonth = mktime(0, 0, 0, $lastmonth, 1, $current_year);

                    $LastDateOfMonth=date('Y-m-d',$LastDayOfMonth);
                    $firstDateOfMonth=date('Y-m-d',$fisrtDayOfMonth);

                    $sql .="
                            date(dtDateTime)>='".mysql_escape_custom($firstDateOfMonth)."'
                        AND
                            date(dtDateTime)<='".mysql_escape_custom($LastDateOfMonth)."'
                    ";
		}
		//iServiceAvailable = '0'
		$query="
                    SELECT
                        count(id) as total,
                        szOriginCountry,
                        szDestinationCountry,
                        idOriginCountry,
                        idDestinationCountry,
                        szBrowserDetails,
                        CONCAT_WS('_',szOriginCountry,szDestinationCountry) szGroupingColumns
                    FROM
                        ".__DBC_SCHEMATA_COMPARE_BOOKING__." 
                    WHERE
                        ".$sql."
                    GROUP BY
                        szGroupingColumns
                    ORDER BY
                        szOriginCountry,szDestinationCountry
		";
		//echo $query;
		//die;
		if($result=$this->exeSQL($query))
		{
                    if($this->iNumRows>0)
                    {
                        $forwarderbooking_ary = array();
                        while($row=$this->getAssoc($result))
                        {
                            $forwarderbooking_ary[] = $row;
                        }
                        return $forwarderbooking_ary;
                    }
                    else
                    {
                        return array();
                    }
		}
		
	}
	function getTotalLCLCount($idForwarder)
	{
		if($idForwarder>0)
		{
			//getting all warehouses by forwarder id 
			$wareHouseAry = $this->getAllWareHousesByForwarderId($idForwarder);
                
			if(!empty($wareHouseAry))
			{
				$query="
					SELECT 
						COUNT(DISTINCT CONCAT_WS(',',p.idWarehouseFrom,p.idWarehouseTo))  as lclServices
					FROM 
						".__DBC_SCHEMATA_WAREHOUSES_PRICING__." p,
						".__DBC_SCHEMATA_WAREHOUSES__."	w
					WHERE 
						w.id IN (".implode(',',$wareHouseAry).")
					AND	
						w.idForwarder='".(int)$idForwarder."' 
					AND 
						w.iActive = '1'
					AND
						p.iActive = '1' 
				";
				//echo $query ;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					return $row['lclServices'];
				}
				else
				{
					return array();
				}
			}
			else
			{   
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
				
			}
		}
	}
	
	function getAllWareHousesByForwarderId($idForwarder)
	{		
		if($idForwarder>0)
		{
			$query="
				SELECT 
				   whs.id		   
				FROM 
					".__DBC_SCHEMATA_WAREHOUSES__." whs			
				WHERE
					whs.iActive = '1'	
				AND
				    whs.idForwarder = '".(int)$idForwarder."'	 
			";	
			//echo "<br>".$query."<br>";
			//die;
			if($result = $this->exeSQL($query))
			{
				$wareHouseAry = array();
				$ctr = 0;
				while($row=$this->getAssoc($result))
				{
					$wareHouseAry[$ctr] = $row['id'];
					$ctr++;
				}
				return $wareHouseAry;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}		
	}
	function getAllForwarderDetailsAdmin($iPage=false,$row_count=false)
	{
            if($row_count)
            {
                $query_select = " count(f.id) as iNumForwarder ";
            }
            else
            {
                $query_select = " 
                        id,
                        szDisplayName,
                        idCountry,
                        iActive,
                        isOnline,
                        iAgreeTNC,
                        szForwarderAlias
                ";
            }
            if($iPage>0)
            {
                $query_limit .= "
                    LIMIT
                        ".(($iPage -1 ) * __MAX_CUSTOMER_PER_PAGE__ ).",". __MAX_CUSTOMER_PER_PAGE__ ."
                ";
            }
            $query="
                SELECT
                    $query_select
                 FROM
                    ".__DBC_SCHEMATA_FROWARDER__." f
                 ORDER BY
                        if(szDisplayName = '' or szDisplayName is null,1,0),szDisplayName ASC
                 $query_limit
            ";
            //echo " <br> <br> ".$query;
            
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    if($row_count)
                    {
                        $row = $this->getAssoc($result);
                        return $row['iNumForwarder'];
                    }
                    else
                    {
                        $forwarderbooking_ary = array();
                        while($row=$this->getAssoc($result))
                        {
                            $forwarderbooking_ary[] = $row;
                        }
                        return $forwarderbooking_ary;
                    } 
                }
                else
                {
                    return array();
                }
            } 
	}
        
	function countForwardersContactRole($idForwarder)
	{
		$query="
			SELECT
				(
					SELECT 
						count(id)
					FROM	
						".__DBC_SCHEMATA_FORWARDERS_CONTACT__."	
					WHERE
						idForwarder = ".mysql_escape_custom($idForwarder)."
					AND 
						iActive = '1'
					AND
						iConfirmed = '1'
					AND 
						idForwarderContactRole IN(".__ADMINISTRATOR_PROFILE_ID__.",".__PRICING_PROFILE_ID__.",".__BILLING_PROFILE_ID__.")			
				) AS total,
				(
					SELECT 
						count(id)
					FROM	
						".__DBC_SCHEMATA_FORWARDERS_CONTACT__."	
					WHERE
						idForwarder = ".mysql_escape_custom($idForwarder)."
					AND	
						idForwarderContactRole = ".__ADMINISTRATOR_PROFILE_ID__."
					AND 
						iActive = '1'
					AND
						iConfirmed = '1'			
				) AS admin,
				(
					SELECT 
						count(id)
					FROM	
						".__DBC_SCHEMATA_FORWARDERS_CONTACT__."		
					WHERE
						idForwarder = ".mysql_escape_custom($idForwarder)."
					AND
						idForwarderContactRole = ".__PRICING_PROFILE_ID__."
					AND 
						iActive = '1'
					AND
						iConfirmed = '1'				
				) AS pricing,
				(
					SELECT 
						count(id)
					FROM	
						".__DBC_SCHEMATA_FORWARDERS_CONTACT__."		
					WHERE
						idForwarderContactRole = ".__BILLING_PROFILE_ID__."	
					AND
						idForwarder = ".mysql_escape_custom($idForwarder)."
					AND 
						iActive = '1'
					AND
						iConfirmed = '1'			
				) AS billing
		";
		//echo $query."<br>";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$contactArr = array();
				while($row=$this->getAssoc($result))
				{
					$contactArr = $row;
				}
				return $contactArr;
			}
			else
			{
				return array();
			}
		}
	
	}
	function createNewForwarder($createArr)
	{	
		if($createArr!=array())
		{ 
			$this->set_szEmail(trim(sanitize_all_html_input($createArr['szEmail'])));
			$this->set_szControlPanelUrl(sanitize_all_html_input($createArr['szControlPanelUrl']));
			$this->set_szcode(sanitize_all_html_input($createArr['code']));
			$this->set_iAutomaticPaymentInvoicing((int)(sanitize_all_html_input($createArr['iAutomaticPaymentInvoicing'])));
			//$this->set_fRefFee((sanitize_all_html_input($createArr['RefFee'])));
			$this->set_szComment(nl2br((string)trim(sanitize_specific_html_input($createArr['szComment']))));
			$this->set_dtValidTo((string)trim(sanitize_all_html_input($createArr['dtValidTo'])));
                        $this->set_iCreditDays((string)trim(sanitize_all_html_input($createArr['iCreditDays'])));
                
			$dns= $this->szControlPanelUrl;
                        $this->iOffLineQuotes = sanitize_all_html_input($createArr['iOffLineQuotes']);
                        $this->iDisplayQuickQuote = sanitize_all_html_input($createArr['iDisplayQuickQuote']); 
                        $this->iAvailableForManualQuotes = sanitize_all_html_input($createArr['iAvailableForManualQuotes']);
                
                        if(!empty($createArr['fReferralFee']))
                        {
                            foreach($createArr['fReferralFee'] as $idTransportMode=>$referralFeeAry)
                            { 
                                $this->set_fReferralFee(sanitize_all_html_input($referralFeeAry),$idTransportMode);
                            }
                        } 
                        else
                        {
                            $this->addError( 'fReferralFee_1' , t($this->t_base.'fields/referal_Fee_Req') );
                        } 
			if($this->error === true)
			{
                            return false;
                            exit;
			}
			if($this->dtValidTo == 'dd/mm/yyyy')
			{
                            $this->addError( $this->dtValidTo , t($this->t_base.'fields/dtValidTo') );
                            return false;
			}
			$time = explode('/',$this->dtValidTo);
			
			$time = mktime(0, 0, 0, $time[1], $time[0], $time[2]);
			$this->dtValidTo = date('Y-m-d h:i:s',$time);
			if($time < strtotime(date('Y-m-d h:i:s')) )
			{
                            $this->addError( $this->dtValidTo , t($this->t_base.'fields/dtValidTo_should') );
                            return false;
			}
			$this->isForwarderEmailExist($this->szEmail);
			$this->szControlPanelUrl = $this->findSubDomainName($this->szControlPanelUrl);
			$controlArr=$this->findAllforwarderSubDomain($id); 	
			if($controlArr!=array())
			{
                            foreach($controlArr as $key=>$value)
                            {
                                $checkValidateUrl=$this->findSubDomainName($value['szControlPanelUrl']);
                                if($checkValidateUrl == $this->szControlPanelUrl)
                                {
                                    $this->addError( $this->szControlPanelUrl ,$dns.".transporteca.com ".t($this->t_base.'Error/alreadyExists') );
                                    return false;	
                                }
                            }
			}
			$this->validateCode();
			if($this->error === true)
			{
				return false;
				exit;
			} 
			if(!empty($this->szControlPanelUrl))
			{
                            $query="
                                INSERT INTO 
                                    ".__DBC_SCHEMATA_FROWARDER__."
                                (
                                    szControlPanelUrl	,
                                    szForwarderAlias,
                                    iActive,
                                    dtCreateOn, 
                                    iAutomaticPaymentInvoicing,
                                    dtReferealFeeValid,
                                    szReferalFeeComment,
                                    iOffLineQuotes,
                                    iAvailableForManualQuotes,
                                    iDisplayQuickQuote,
                                    iCreditDays
                                )
                                VALUES
                                (
                                    '".mysql_escape_custom(strtolower($this->szControlPanelUrl)).".transporteca.com',
                                    '".mysql_escape_custom(strtoupper($this->szcode))."',
                                    '1',
                                    NOW(), 
                                    '".mysql_escape_custom($this->iAutomaticPaymentInvoicing)."',
                                    '".mysql_escape_custom($this->dtValidTo)."',
                                    '".mysql_escape_custom($this->szComment)."',
                                    '".mysql_escape_custom($this->iOffLineQuotes)."',
                                    '".mysql_escape_custom($this->iAvailableForManualQuotes)."',
                                    '".mysql_escape_custom($this->iDisplayQuickQuote)."',
                                    '".mysql_escape_custom($this->iCreditDays)."' 
                                )
                            ";
			  // echo $query;die;
				if($result=$this->exeSQL($query))
				{
					$this->id = mysql_insert_id();
					$this->szPassword = create_password();
                
                                        $kReferralPricing = new cReferralPricing();
                                        if(!empty($this->fReferralFee))
                                        {
                                            foreach($this->fReferralFee as $idTransportMode=>$fReferralFee)
                                            {
                                                $addReferalFeeAry = array();
                                                $addReferalFeeAry['idForwarder'] = $this->id;
                                                $addReferalFeeAry['fReferralFee'] = $fReferralFee;
                                                $addReferalFeeAry['idTransportMode'] = $idTransportMode;
                                                $kReferralPricing->addReferalFeeByTransportMode($addReferalFeeAry);
                                            }
                                        }
                                        
					$query="
                                            INSERT 
						INTO 
						".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
					SET
						idForwarder = '".mysql_escape_custom($this->id)."',
						szEmail = '".mysql_escape_custom($this->szEmail)."',
						idForwarderContactRole = ".__ADMINISTRATOR_PROFILE_ID__.",
						szPassword = '".md5($this->szPassword)."',
						iActive = 1,
						iConfirmed = 1,
						dtCreateOn = now()
					";
					//echo $query;
					if($result=$this->exeSQL($query))
					{
						$replace_ary['szURL']="<a href='http://".$this->szControlPanelUrl.".transporteca.com'>".$this->szControlPanelUrl.".transporteca.com</a>";
						$replace_ary['szEmail']=$this->szEmail;
						$replace_ary['szPassword']=$this->szPassword;
						if(createEmail(__CREATE_NEW_FORWARDER_PROFILE_BY_ADMINSTRATOR__, $replace_ary,$this->szEmail,__NEW_FORWARDER_EMAIL_SUBJECT__, __STORE_SUPPORT_EMAIL__,0, __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__))
						{
							return true;
						}
						else
						{
							return true;
						}
					}
					else
					{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					}
				}
				else
				{
					
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
			}
			else
			{
				return false;
			}
		}
	}
	function validateCode()
	{
	$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_FORWARDERS__."
			WHERE
				szForwarderAlias = '".mysql_escape_custom($this->szcode)."'
			AND
				iActive = '1'
		";
		//echo $query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
        	// Check if the user exists
            if( $this->getRowCnt() > 0 )
            {
                 $this->addError('szcode', 'Following codes are already in use: ' .$this->findCode());
				$this->Error = true;
				return false;
            }
            else
            {
                    return false;
            }
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function findCode()
	{
		$query="
			SELECT
				szForwarderAlias
			FROM
				".__DBC_SCHEMATA_FORWARDERS__."
			WHERE
				iActive = '1'
			ORDER BY 
				szForwarderAlias ASC	
		";
		//echo $query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
        	// Check if the user exists
            if( $this->getRowCnt() > 0 )
            {
            	$code = array();
                 while ($row=$this->getAssoc($result)) {
                 	$code[] = $row['szForwarderAlias'];
                 }
                 return implode(', ',$code);
            }
            else
            {
                    return array();
            }
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function isForwarderEmailExist( $email )
	{
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
			WHERE
				szEmail = '".mysql_escape_custom($this->szEmail)."'
			AND
				iActive = '1'
		";
		if( ( $result = $this->exeSQL( $query ) ) )
		{
        	// Check if the user exists
            if( $this->getRowCnt() > 0 )
            {
                 $this->addError('szEmail', 'A user with following E-mail already registered');
				$this->Error = true;
				return false;
            }
            else
            {
                    return false;
            }
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function findSubDomainName($domain)
	{
		if($domain!='')
		{	
			$domain	=	str_replace('http://','',$domain);
			$domain	=	str_replace('https://','',$domain);
			$szControlURL =explode(".",$domain);
			if(strtolower(trim($szControlURL['0']))=='www')
			{	
				$domain	=	$szControlURL['1'];
				
			}
			else
			{
				$domain	=	$szControlURL['0'];
			}
			return strtolower($domain);
		}
	}
	function findAllforwarderSubDomain($id=0)
	{	
		$query=
			"SELECT
				szControlPanelUrl
			 FROM	
				".__DBC_SCHEMATA_FROWARDER__."
			 WHERE
				iActive = '1'
			";
		if($id>0)
		{	
		$query .=
				"
				AND
					id<>'".(int)$id."'
				AND	
				";
		}
		$query .=
				" AND
					(
					szControlPanelUrl!=NULL
				OR
					szControlPanelUrl!=''
					)
				";
			//echo $query;	
			if($result = $this->exeSQL($query) )
			{
				if($this->iNumRows>0)
				{
					while($row=$this->getAssoc($result))
					$szControlUrlArr[]=$row;
					return $szControlUrlArr;
				}
				else
				{
					return array();
				}
			}	
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function findforwarderSubDomainName($id)
	{	
		$query=
			"SELECT
				szControlPanelUrl
			 FROM	
				".__DBC_SCHEMATA_FROWARDER__."
			 WHERE
				id = '".(int)$id."'
				AND	
				(
					szControlPanelUrl!=NULL
				OR
					szControlPanelUrl!=''
				)
				";
			//echo $query;	
			if($result = $this->exeSQL($query) )
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					return $this->findSubDomainName($row['szControlPanelUrl']);
				}
				else
				{
					return array();
				}
			}	
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function validateForwarderEmail($idForwarder)
	{
		if((int)$idForwarder>0)
		{
			$query="
				SELECT 
					id
				FROM 
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."	
				WHERE	
					idForwarder='".(int)$idForwarder."'
				AND
					szEmail = ".mysql_escape_custom($this->szEmail)."	
			";
		//	echo $query;die();
			if($result = $this->exeSQL($query))
			{
		
				if($this->iNumRows > 0)
				{	
					$this->addError('szEmail','E-mail already registered');
					$this->Error = true;
					return false;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return true;
		}
	}
	function saveForwarderComanyDetails($data)
	{	
		if(!empty($data))
		{	
			$this->set_szDisplayName(trim(sanitize_all_html_input($data['szDisplayName'])));
			$this->set_szLink(trim(sanitize_all_html_input($data['szLink'])));
			$this->set_szLogo(trim(sanitize_all_html_input($data['szForwarderLogo'])));
			$this->set_id(trim(sanitize_all_html_input($data['idForwarder'])));
			$this->set_szCompanyName(trim(sanitize_all_html_input($data['szCompanyName'])));
			$this->set_szAddress1(trim(sanitize_all_html_input($data['szAddress'])));
			$this->set_szAddress2(trim(sanitize_all_html_input($data['szAddress2'])));
			$this->set_szAddress3(trim(sanitize_all_html_input($data['szAddress3'])));
			$this->set_szPostcode(trim(sanitize_all_html_input($data['szPostCode'])));	
			$this->set_szCity(trim(sanitize_all_html_input($data['szCity'])));
			$this->set_szState(trim(sanitize_all_html_input($data['szState'])));	
			$this->set_szCountry(trim(sanitize_all_html_input($data['idCountry'])));	
			$this->set_szPhoneNumber(trim(sanitize_all_html_input(urlencode($data['szPhone']))));
			$this->set_szCompanyRegistrationNum(trim(sanitize_all_html_input($data['szCompanyRegistrationNum'])));
			$this->set_szVatRegistrationNum(trim(sanitize_all_html_input($data['szVatRegistrationNum'])));
			$this->set_id(trim(sanitize_all_html_input($data['idForwarder'])));
			
			if($this->error===true)
			{
				return false;
			}
			if(!empty($this->szLogo))
			{
				$temp_file_path = __APP_PATH_FORWARDER_IMAGES_TEMP__.'/'.$this->szLogo ;
				if(file_exists($temp_file_path))
				{
					$file_path = __APP_PATH_FORWARDER_IMAGES__.'/'.$this->szLogo ;
					if(copy($temp_file_path,$file_path))
					{
						if(file_exists($temp_file_path))
						{
							@unlink($temp_file_path);
						}
						$this->szLogoFileName = $this->szLogo ;
		        	}
				}	
			}
			else
			{
				$this->szLogoFileName = $data['szOldLogo'] ;
			}
			
			if($data['iDeleteLogo']==1)
			{
				$this->deleteForwarderLogo($this->id);
			}
			if(!empty($this->szPhoneNumber))
			{
				$str = substr($this->szPhoneNumber, 0, 1); 
				//echo $str;
				$substr=substr($this->szPhoneNumber,1);
				$phone=str_replace("+"," ",$substr);
				if(!empty($phone))
				{
					$szPhoneNumber=$str."".$phone;
				}
			}
			
			$query="
				UPDATE
					".__DBC_SCHEMATA_FROWARDER__."
				SET
					szDisplayName = '".mysql_escape_custom(trim($this->szDisplayName))."',	
					szLink = '".mysql_escape_custom(trim($this->szLink))."',
					szLogo = '".mysql_escape_custom(trim($this->szLogoFileName))."',
					szCompanyName = '".mysql_escape_custom(trim($this->szCompanyName))."',
					szAddress = '".mysql_escape_custom(trim($this->szAddress1))."',
					szAddress2 = '".mysql_escape_custom(trim($this->szAddress2))."',
					szAddress3 = '".mysql_escape_custom(trim($this->szAddress3))."',
					szPostcode = '".mysql_escape_custom(trim($this->szPostcode))."',
					szCity = '".mysql_escape_custom(trim($this->szCity))."',
					szState = '".mysql_escape_custom(trim($this->szState))."',
					idCountry = '".(int)$this->szCountry."',
					szPhone = '".mysql_escape_custom(trim($szPhoneNumber))."',
					szCompanyRegistrationNum = '".mysql_escape_custom(trim($this->szCompanyRegistrationNum))."',
					szVatRegistrationNum = '".mysql_escape_custom(trim($this->szVatRegistrationNum))."'
				WHERE
					id = '".(int)$this->id."'
			";
			//echo "<br>".$query ;
			if($result = $this->exeSQL($query))
			{
				if(!empty($this->szLogoFileName))
				{
					$this->isForwarderOnline($this->id,'UPDATE',true);
				}
				else
				{
					$this->isForwarderOnline($this->id,'UPDATE',false);
				}
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}

	function getForwarderAgreedTC($date)
	{
		$query="
			SELECT
				id
			 FROM
			 	".__DBC_SCHEMATA_FROWARDER__."
			 WHERE	
			 	iActive='1'
			 AND
			 	iAgreeTNC='1'
			 AND
			 	iVersionUpdate='1'
			 AND
			 	date(dtVersionUpdate)='".mysql_escape_custom($date)."'
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$contactArr = array();
				while($row=$this->getAssoc($result))
				{
					$forwarderArr[] = $row['id'];
				}
				return $forwarderArr;
			}
			else
			{
				return array();
			}
		}
	}
	
	
	function getForwarderAdminDetails($idForwarder)
	{
		$query="
			SELECT
				id, 
				szEmail,
				szFirstName,
				szLastName 
			FROM 
				".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
			WHERE
				idForwarder='".(int)$idForwarder."'
			AND
				iConfirmed='1'
			AND 
				idForwarderContactRole='1'	
			";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$contactArr = array();
				while($row=$this->getAssoc($result))
				{
					$forwarderContactArr[] = $row;
				}
				return $forwarderContactArr;
			}
			else
			{
				return array();
			}
		}
	}
	
	function updateForwarderTNCAgreedFlag($idForwarder)
	{
		$query = "
			UPDATE
				".__DBC_SCHEMATA_FROWARDER__."
			SET
				iAgreeTNC='0',
				iVersionUpdate='0',
				dtVersionUpdate=''
			WHERE
				id='".(int)$idForwarder."'
		";
		//echo $query."<br/>";
		if($result=$this->exeSQL($query))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	function selectIactiveStatus($idForwarder)
	{
		$idForwarder = 	(int)sanitize_all_html_input($idForwarder);
		$query="
				SELECT
					iActive
				FROM
					".__DBC_SCHEMATA_FORWARDERS__."
				WHERE
					id = '".(int)$idForwarder."'	
			";
			//echo $query ;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return $this->getAssoc($result);
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	
	}
	
	
	function getAllForwardersDetails($idForwarder,$idRoleAry,$flag)
	{
		if($idForwarder>0)
		{
			/*if(is_array($idRoleAry) && !empty($idRoleAry))
			{
				$idRoleStr = implode(',',$idRoleAry);
				$query_and = "AND
					idForwarderContactRole IN (".$idRoleStr.") ";
			}
			else
			{
				$query_and = "AND
					idForwarderContactRole = '".$idRoleAry."' ";
			}*/
			
			if($flag=='daily')
			{
				$sql .="
					AND
						szBookingFrequency='1'
				";
			}
			if($flag=='weekly')
			{
				$sql .="
					AND
						szBookingFrequency='2'
				";
			}
			
			if($flag=='monthly')
			{
				$sql .="
					AND
						szBookingFrequency='3'
				";
			}
			
			$query="
				SELECT
					id,
					szEmail,
					szFirstName
				FROM
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
				WHERE
					idForwarder = '".(int)$idForwarder."'	
				AND
					iActive = '1'
				AND
					iConfirmed='1'
				$sql	
			";
			//echo $query.'<BR/>' ;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$ret_ary = array();
					$ctr = 0;
					while($row=$this->getAssoc($result))
					{
						$ret_ary[] = $row;
						$ctr++ ;
					}
					return $ret_ary;
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function findForwarderCurrency($id)
	{
		$id	=	(int)sanitize_all_html_input($id);
		if($id<=0)
		{			
			return false;
		}
		$query="
				SELECT
					szCurrency
				FROM
					".__DBC_SCHEMATA_FROWARDER__."
				WHERE 
					id=".mysql_escape_custom($id)."		
			";
		
			if($result=$this->exeSQL($query))
			{
				$row=$this->getAssoc($result);
				if($row['szCurrency']=='')
				{
					return false;
				}
				else
				{
					return true;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	
	function updateUpdateServiceKey($ConfirmKey,$idForwarder)
	{
		if((int)$idForwarder>0 && $ConfirmKey!='')
		{
			$query="
				UPDATE
					".__DBC_SCHEMATA_FORWARDERS__."
				SET
					szServiceUpdateKey='".mysql_escape_custom($ConfirmKey)."'
				WHERE
					id='".(int)$idForwarder."'
			";
			$result=$this->exeSQL($query);
		}
	}
	
	function checkUpdateServiceKey($szUpdateKey,$idForwarder)
	{
		if((int)$idForwarder>0 && $szUpdateKey!='')
		{
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_FORWARDERS__."
				WHERE
					id='".(int)$idForwarder."'
				AND
					szServiceUpdateKey='".mysql_escape_custom($szUpdateKey)."'	
			";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function getAllForwarder($bReferalForwarder=false,$railTransportForwarder=false,$bActive=false)
	{
            if($bReferalForwarder)
            {
                $query_where = "
                    WHERE
                        iActive = '1'
                    AND
                        iAgreeTNC = '1'    
                    AND
                        iProfitType = '".__FORWARDER_PROFIT_TYPE_REFERRAL_FEE__."'
                ";
                $query_order_by = " ORDER BY szDisplayName ASC, szForwarderAlias ASC";
            } 
            
            if($railTransportForwarder)
            {
                $query_where = "
                    WHERE
                        iActive = '1'    
                    AND
                        iProfitType = '".__FORWARDER_PROFIT_TYPE_REFERRAL_FEE__."'
                ";
                $query_order_by = " ORDER BY szDisplayName ASC, szForwarderAlias ASC";
            } 
            if($bActive)
            {
                $query_where = "
                    WHERE
                        iActive = '1'    
                    AND
                        isOnline = '1'
                ";
            }
            
            $query="
                SELECT
                    id,
                    szDisplayName, 
                    idCountry,
                    iActive,
                    isOnline,
                    iAgreeTNC,
                    szForwarderAlias,
                    szCurrency 
                FROM
                    ".__DBC_SCHEMATA_FORWARDERS__."		
                $query_where
                $query_order_by
            ";
            //echo $query."<br><br>";
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $ret_ary = array();

                    while($row=$this->getAssoc($result))
                    {
                        $ret_ary[] = $row;
                    }
                    return $ret_ary;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                return array();
            }
	}
	
	function getAllForwarderProfiles()
	{
		$query="
			SELECT
				id,
				szRoleName
			FROM
				".__DBC_SCHEMATA_FORWARDERS_CONTACT_ROLE__."		
		";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$ret_ary = array();
				
				while($row=$this->getAssoc($result))
				{
					$ret_ary[] = $row;
				}
				return $ret_ary;
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	function findSzLinkForwarder($id)
	{
		$id	= (int)$id;
		if($id>0)
		{
			$query="
				SELECT
					szDisplayName,
					szLink
				FROM
					".__DBC_SCHEMATA_FORWARDERS__."	
				WHERE 
					id = ".mysql_escape_custom($id)."		
			";
			//echo $query;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{	
					$row=$this->getAssoc($result);
					return $row;
				}
				else
				{
					return '';
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	
	}
	function checkForwarderComleteCompanyInfomation($idForwarder)
	{
		if($idForwarder>0)
		{
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_FROWARDER__."	
				WHERE
					id = '".(int)$idForwarder."'	
				AND
				(
					szDisplayName =''
				OR
					szCompanyName =''	
				OR
					szAddress =''	
				OR
					szCity =''	
				OR
					idCountry =''	 
				OR
					szPhone =''
				OR
					szCompanyRegistrationNum =''	
				OR
					szLink =''	
				)	
			";
			//echo $query;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function checkForwarderComleteBankInfomation($idForwarder,$status)
	{
		if($idForwarder>0)
		{
			$status = (int)sanitize_all_html_input($status);
			$query="
				SELECT
					fwd.id
				FROM
					".__DBC_SCHEMATA_FROWARDER__."	AS fwd
				INNER JOIN 
					".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."	AS fwdtrans
				ON
				( 
					fwdtrans.idForwarder = fwd.id 
				AND 
					fwdtrans.iStatus = ".$status."
				AND
					fwdtrans.iDebitCredit!=4		
				)	
				WHERE
					fwd.id = '".(int)$idForwarder."'	
				AND
				(
					fwd.szBankName =''
				OR
					fwd.szNameOnAccount =''	
				OR
					fwd.iAccountNumber =''		
				OR
					fwd.idBankCountry =''	
				OR
					fwd.szSwiftCode = ''
				)	
			";
			//echo "<br>".$query."<br>" ;
			//die;
			/*
			OR
					
			*/
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function checkBillingBookingCustomer($idForwarder)
	{       
                //__BOOKING_PROFILE_ID__
		/*if(!$this->isForwarderPreferencesExists($idForwarder,__CATCH_UP_ALL_BOOKING_QUOTE_PROFILE_ID__))
		{
			return true;
		}
		else if(!$this->isForwarderPreferencesExists($idForwarder,__PAYMENT_AND_BILLING_PROFILE_ID__))
		{
			return true;
		}
		else if(!$this->isForwarderPreferencesExists($idForwarder,__CUSTOMER_PROFILE_ID__))
		{
			return true;
		}
		else
		{*/
			$query="
				SELECT
					id
				FROM
				 	".__DBC_SCHEMATA_FROWARDER__."
				WHERE
					id = '".(int)$idForwarder."'	
				AND  
					(szCurrency!=0	|| szCurrency!=NULL)	
				AND
					iActive = 1
			";
			//echo "<br>".$query."<br>" ;
			//die;
			/*
			OR
					
			*/
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return false;
				}
				else
				{
					return true;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}	
		//return false;
		//}
	}
	function checkAgreeTNC($idForwarder)
	{
		if($idForwarder>0)
		{
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_FROWARDER__."	
				WHERE
					id = '".(int)$idForwarder."'	
				AND
				iAgreeTNC = 0
			";
			//echo "<br>".$query."<br>" ;
			//die;
			/*
			OR
					
			*/
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function checkCFS($idForwarder)
	{
		if($idForwarder>0)
		{
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_WAREHOUSES__."	
				WHERE
                                    idForwarder = '".(int)$idForwarder."'	
				AND
                                    iActive = 1 
			";
			//echo "<br>".$query."<br>" ;
			//die;
			/*
			OR
					
			*/
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function checkWarehouses($idForwarder)
	{
		if($idForwarder>0)
		{
			$query="
				SELECT
					wp.id
				FROM
					".__DBC_SCHEMATA_WAREHOUSES__."	as w
				INNER JOIN 
					".__DBC_SCHEMATA_WAREHOUSES_PRICING__." as wp				
					ON ( (wp.idWarehouseFrom  = w.id OR wp.idWarehouseTo  = w.id) AND wp.iActive = 1 AND wp.dtExpiry>= NOW() )	
				WHERE
					w.idForwarder = '".(int)$idForwarder."'	
				AND
                                    w.iActive = 1 
				
			";
			//echo "<br>".$query."<br>" ;
			//die;
			/*
			OR
					
			*/
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function checkPricingHaulage($idForwarder)
	{
		if($idForwarder>0)
		{
			$query="
				SELECT
					ph.id
				FROM
					".__DBC_SCHEMATA_WAREHOUSES__."	as w
				INNER JOIN
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."	 AS hm
					ON ( hm.idWarehouse  = w.id   )		
				INNER JOIN  
					".__DBC_SCHEMATA_HAULAGE_PRICING__." as ph				
					ON ( hm.id  = ph.idHaulagePricingModel   )	
				WHERE
					w.idForwarder = '".(int)$idForwarder."'	
				AND
					w.iActive = 1
				AND 
					ph.iActive = 1 	
				AND
					hm.iActive = 1	 
			";
			//echo "<br>".$query."<br>" ;
			//die;
			/*
			OR
					
			*/
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function checkCC($idForwarder)
	{
		if($idForwarder>0)
		{
			$query="
				SELECT
					wp.id
				FROM
					".__DBC_SCHEMATA_WAREHOUSES__."	as w
				INNER JOIN 
                                    ".__DBC_SCHEMATA_PRICING_CC__." as wp				
                                    ON ( (wp.idWarehouseFrom  = w.id OR wp.idWarehouseTo  = w.id) AND wp.iActive = 1 )	
				WHERE
                                    w.idForwarder = '".(int)$idForwarder."'	
				AND
                                    w.iActive = 1 
				
			";
			//echo "<br>".$query."<br>" ;
			//die;
			/*
			OR
					
			*/
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function checkforwarderContacts($idForwarder)
	{
		if($idForwarder>0)
		{
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
				WHERE
					idForwarder = '".(int)$idForwarder."'	
				AND
					iActive = 1
			";
			//echo $query;
			if($result=$this->exeSQL($query))
			{	
				if($this->iNumRows>1)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function adminEachForwarder($idForwarderUser)
	{
		if($idForwarderUser>0)
		{
		$query="
			SELECT
				idForwarderContactRole
			FROM 
				".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
			WHERE
				id = ".mysql_escape_custom((int)$idForwarderUser)."
				";
		//echo $query;
		if($result = $this->exeSQL($query))
		{
		if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					return $row['idForwarderContactRole'];
				}
				else
				{
					return false ;
				}
		}
		else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function isForwarderCompanyDetailsOnLine($idForwarder)
	{
		if($idForwarder>0)
		{
			
			$idForwarder = (int)sanitize_all_html_input($idForwarder);
//			if(!$this->isForwarderPreferencesExists($idForwarder,__BOOKING_PROFILE_ID__))
//			{
//				return true;
//			}
//			else if(!$this->isForwarderPreferencesExists($idForwarder,__PAYMENT_AND_BILLING_PROFILE_ID__))
//			{
//				return true;
//			}
//			else if(!$this->isForwarderPreferencesExists($idForwarder,__CUSTOMER_PROFILE_ID__))
//			{
//				return true;
//			}
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_FROWARDER__."	
				WHERE
					id = '".(int)$idForwarder."'	
				AND
				(
					szDisplayName =''
				OR
					szCompanyName =''	
				OR
					szAddress =''	
				OR
					szCity =''	
				OR
					idCountry =''	 
				OR
					szPhone =''
				OR
					szCompanyRegistrationNum =''	
				OR
					szLink =''
				OR		
					iAgreeTNC = 0
				OR   
					(szCurrency=0	|| szCurrency=NULL)	
				OR
					szLogo = ''	
				)
					";
		
		//echo $query;
		if($result = $this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				return true;		
			}
			else
			{
				return false;
			}
		}
		else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
    function isForwarderOnline($idForwarder,$mode,$rss_flag=false)
    {
    	if((int)$idForwarder >0)
    	{
    		//$checkComapnyProfile = $this->checkForwarderComleteCompanyInfomation($idForwarder);
			//$checkComapnyEmails  = $this->checkBillingBookingCustomer($idForwarder);
			//$checkTnc			 = $this->checkAgreeTNC($idForwarder);
			//$checkCFS			 = $kForwarder->checkCFS($idForwarder);
			//$checkPricingWareh	 = $kForwarder->checkWarehouses($idForwarder);
			//|| (!$checkCFS) || (!$checkPricingWareh)
			
			//isForwarderCompanyDetailsOnLine returns TRUE when forwarder is not online
		
//			if(!$this->isForwarderCompanyDetailsOnLine($idForwarder) )
//			{
				if($mode=='UPDATE')
				{
					$query = "
						UPDATE
							".__DBC_SCHEMATA_FORWARDERS__."
						SET
							isOnline = '1'
						WHERE
							id='".(int)$idForwarder."'
					";
					//echo $query;
					if($result=$this->exeSQL($query))
					{
						
					}
					else
					{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					}
				}
				
				if($rss_flag)
				{
					$kForwarder = new cForwarder();
					$idForwarder = $_SESSION['forwarder_id'];
					$kForwarder->load($idForwarder);
																
					if($kForwarder->iAgreeTNC==1 && $kForwarder->iUpdateRss==2)
					{
						$kRss = new cRSS();
						if($kRss->isWelcomeFeedExists($kForwarder->id))
						{
							//If there is already welcome feed for forwarder then we dont add new one 
							return true;
						}
						
						$szForwarderNameList = $this->getAllActiveForwarderNameList($idForwarder,true);
						
						$replace_ary = array();
						$replace_ary['szForwarderDisplayName'] = $kForwarder->szDisplayName ;
						$replace_ary['szCountryName'] = $szOriginCountry ;
						$replace_ary['szSiteUrl'] = __MAIN_SITE_HOME_PAGE_SECURE_URL__ ;
						$replace_ary['szAssociateForwarderName'] = $szForwarderNameList ;
							
						$content_ary = array();
						$kExportImport = new cExport_Import();
						$content_ary = $kExportImport->replaceRssContent('__WELCOME_FORWARDER__',$replace_ary);
						
						$rssDataAry=array();
						$rssDataAry['idForwarder'] = $kForwarder->id ;
						$rssDataAry['szFeedType'] = '__WELCOME_FORWARDER__';
						$rssDataAry['szLink'] = __RSS_FEED_PAGE_URL__ ;
						$rssDataAry['szTitle'] = $content_ary['szTitle'];
						$rssDataAry['szDescription'] =  $content_ary['szDescription'];;
						$rssDataAry['szDescriptionHTML'] =  $content_ary['szDescriptionHTML'];
						$kRss->addRssFeed($rssDataAry);
					}
				}
				return true;	
//			}
//			else
//			{
//				return false;	
//			}
    	}
    	else
    	{
    		return false;
    	}
    }
	
	function getAllForwarderReferralFeeExpired($date)
	{
		$query="
			SELECT
				id,
				szDisplayName
			FROM
				".__DBC_SCHEMATA_FROWARDER__."
			WHERE
				date(dtReferealFeeValid)='".mysql_escape_custom($date)."'
			AND
				iActive='1'
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$ret_ary = array();
				
				while($row=$this->getAssoc($result))
				{
					$ret_ary[] = $row;
				}
				return $ret_ary;
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	function findLclServices($idForwarder)
	{
		$query = "
			SELECT 
                            DISTINCT
                            (
                                p.idWarehouseFrom),
                                p.idWarehouseTo
				
			FROM 
				".__DBC_SCHEMATA_WAREHOUSES_PRICING__." p,
				".__DBC_SCHEMATA_WAREHOUSES__." w
			WHERE 
				w.id IN (p.idWarehouseFrom,p.idWarehouseTo)  
			AND	
				w.idForwarder='".(int)$idForwarder."' 
			AND 
				w.iActive = '1'
			AND
				p.iActive = '1'
			AND 
				p.dtExpiry >= NOW()	 
			";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$ret_ary = array();
				
				while($row=$this->getAssoc($result))
				{
					$ret_ary[] = $row;
				}
				return $ret_ary;
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	
	}
	function findEachLclServices($idWarehouse)
	{
		$query = "
			SELECT 
                            w.szCity,
                            c.szCountryName
			FROM	
                            ".__DBC_SCHEMATA_WAREHOUSES__." AS w
			INNER JOIN
                            ".__DBC_SCHEMATA_COUNTRY__." AS c	
			ON(c.id = w.idCountry)	
			WHERE
                            w.id = ".(int)$idWarehouse." 
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				return $row=$this->getAssoc($result);
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	
	}
	function checkForwarderComleteBankDetails($idForwarder)
	{
		if($idForwarder>0)
		{
			$query="
				SELECT
					fwd.id
				FROM
					".__DBC_SCHEMATA_FROWARDER__."	AS fwd
				WHERE
					fwd.id = '".(int)$idForwarder."'	
				AND
				(
					fwd.szBankName =''
				OR
					fwd.szNameOnAccount =''	
				OR
					fwd.iAccountNumber =''		
				OR
					fwd.idBankCountry =''
				OR
					fwd.szSwiftCode = ''
				)	
			";
			//echo "<br>".$query."<br>" ;
			//die;
			/*
			OR
					
			*/
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function checkForwarderOnlineStatus($idForwarder)
	{
		$idForwarder 		=	(int)sanitize_all_html_input($idForwarder);
		
		$this->load($idForwarder);
		
		$logo =$this->szLogo;
		
		$checkComapnyProfile = $this->checkForwarderComleteCompanyInfomation($idForwarder);
		
		$checkComapnyEmails  = $this->checkBillingBookingCustomer($idForwarder);
		
		$checkTnc			 = $this->checkAgreeTNC($idForwarder);
		
		$checkCFS			 = $this->checkCFS($idForwarder);
		
		$checkPricingWareh	 = $this->checkWarehouses($idForwarder);
		
		if($checkComapnyProfile || $checkComapnyEmails  || $checkTnc || (!$checkCFS) || (!$checkPricingWareh) || ($logo=='')  )
		{	
		
			return false;
		
		}
		else
		{
		
			return true;	
		
		}
	}
	function detailsForwarderBillingGeneralEmail($idForwarder)
	{
		$query="
			SELECT
			 	szGeneralEmailAddress 
			FROM
				".__DBC_SCHEMATA_FROWARDER__."
			WHERE
				id='".(int)$idForwarder."'
		";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$row=$this->getAssoc($result);
				$emails[]=$row;
				return $emails;
			}
		}
		else
		{
			
		}
	}
	function updateForwarderCompanyInformation($data,$FILES)
	{
		if(!empty($data))
		{
			$this->set_szDisplayName(trim(sanitize_all_html_input($data['szDisplayName'])));
			$this->set_szLink(trim(sanitize_all_html_input($data['szLink'])));
			$this->set_id(trim(sanitize_all_html_input($data['idForwarder'])));
			$this->set_szGeneralEmailAddress(trim(sanitize_all_html_input($data['szGeneralEmailAddress'])));
			$this->set_szLogo(trim(sanitize_all_html_input($data['szForwarderLogo'])));

			$this->set_szCompanyName(trim(sanitize_all_html_input($data['szCompanyName'])));
			$this->set_szAddress1(trim(sanitize_all_html_input($data['szAddress'])));
			$this->set_szAddress2(trim(sanitize_all_html_input($data['szAddress2'])));
			$this->set_szAddress3(trim(sanitize_all_html_input($data['szAddress3'])));
			$this->set_szPostcode(trim(sanitize_all_html_input($data['szPostCode'])));	
			$this->set_szCity(trim(sanitize_all_html_input($data['szCity'])));
			$this->set_szState(trim(sanitize_all_html_input($data['szState'])));	
			$this->set_szCountry(trim(sanitize_all_html_input($data['idCountry'])));	
					
			$this->set_szPhoneNumber(trim(sanitize_all_html_input(urlencode($data['szPhone']))));
			$this->set_szCompanyRegistrationNum(trim(sanitize_all_html_input($data['szCompanyRegistrationNum'])));
			$this->set_szVatRegistrationNum(trim(sanitize_all_html_input($data['szVatRegistrationNum'])));
			$this->set_id(trim(sanitize_all_html_input($data['idForwarder'])));
			
			if($this->error===true)
			{
				return false;
			}
                        $bUpdatePdfLogo = false; 
                        if(!empty($this->szLogo))
			{ 
                            $bUpdatePdfLogo = true;
                            $szLogoArr=explode("|||||",$this->szLogo);                            
                            if(count($szLogoArr)==3)
                            {
                                $this->szLogo=$szLogoArr[1];
                            }
                            $temp_file_path = __APP_PATH_FORWARDER_IMAGES_TEMP__.'/'.$this->szLogo ;
                            $forwarder_file_path = __APP_PATH_FORWARDER_IMAGES__.'/'.$this->szLogo ;
                            if(file_exists($temp_file_path))
                            {
                                $file_path = __APP_PATH_FORWARDER_IMAGES__.'/'.$this->szLogo ;
                                if(copy($temp_file_path,$file_path))
                                {
                                    if(file_exists($temp_file_path))
                                    {
                                        @unlink($temp_file_path);
                                    }
                                    $this->szLogoFileName = $this->szLogo ;
		        	}
		        	
                                $filetype=explode('.',$this->szLogo);
                                $img_formats_create_jpg=array('image/jpeg','image/jpg','image/pjpeg');

                                if(!in_array($filetype[1],$img_formats_create_jpg))
                                {
                                    $filetype=explode('.',$this->szLogo);
                                    $fileNamePdfLogo=$filetype[0]."_pdflogo.jpg";

                                    $old_name = __APP_PATH_FORWARDER_IMAGES__."/".$this->szLogo ;
                                    $new_name = __APP_PATH_FORWARDER_IMAGES__."/".$fileNamePdfLogo;
                                    $this->createForwarderLogoFor($old_name, $new_name,$this->szLogo); 
                                    //createAvtarThumbs(__APP_PATH_FORWARDER_IMAGES__."/",$this->szLogo,__APP_PATH_FORWARDER_IMAGES__."/",'80','30',$fileNamePdfLogo);
                                }
                                else
                                {
                                    $filetype=explode('.',$this->szLogo);
                                    $fileNamePdfLogo=$filetype[0].'_pdflogo.'.$filetype[1];
                                    $file_path=__APP_PATH_FORWARDER_IMAGES__.$this->szLogo;
                                    $temp_file_path=__APP_PATH_FORWARDER_IMAGES__.$fileNamePdfLogo;
                                    copy($file_path,$temp_file_path);
                                }
                            }
                            else if(file_exists($forwarder_file_path))
                            {
                                $this->szLogoFileName = $this->szLogo ;
                            }
				
			}
			else
			{
                            $this->szLogoFileName = $data['szOldLogo'] ;
			}
                        
			if($this->szLogoFileName =='')
			{
				$this->addError( "szLogoFileName" , 'Forwarder Logo is required' );
				return false;
			}
			if($this->szLogo =='')
			{
				$this->addError( "szLogoFileName" , 'Forwarder Logo is required' );
				return false;
			}
			if(!empty($this->szPhoneNumber))
			{
				$str = substr($this->szPhoneNumber, 0, 1); 
				//echo $str;
				$substr=substr($this->szPhoneNumber,1);
				$phone=str_replace("+"," ",$substr);
				//echo $phone;
				if(!empty($phone))
				{
					$phoneNumber=$str."".urldecode($phone);
				}
				else
				{
					$phoneNumber = urldecode($this->szPhoneNumber) ;
				}
			}
			if($this->error===true)
			{
				return false;
			}
			if($data['iDeleteLogo']==1)
			{
				$this->deleteForwarderLogo($this->id);
			}
			$query="
				UPDATE
					".__DBC_SCHEMATA_FROWARDER__."
				SET
					szDisplayName = '".mysql_escape_custom(trim($this->szDisplayName))."',	
					szLink = '".mysql_escape_custom(trim($this->szLink))."',
					szLogo = '".mysql_escape_custom(trim($this->szLogoFileName))."',
					szGeneralEmailAddress = '".mysql_escape_custom(trim($this->szGeneralEmailAddress))."',
					szCompanyName = '".mysql_escape_custom(trim($this->szCompanyName))."',
					szAddress = '".mysql_escape_custom(trim($this->szAddress1))."',
					szAddress2 = '".mysql_escape_custom(trim($this->szAddress2))."',
					szAddress3 = '".mysql_escape_custom(trim($this->szAddress3))."',
					szPostcode = '".mysql_escape_custom(trim($this->szPostcode))."',
					szCity = '".mysql_escape_custom(trim($this->szCity))."',
					szState = '".mysql_escape_custom(trim($this->szState))."',
					idCountry = '".(int)$this->szCountry."',
					szPhone = '".mysql_escape_custom(trim($phoneNumber))."',
					szCompanyRegistrationNum = '".mysql_escape_custom(trim($this->szCompanyRegistrationNum))."',
					szVatRegistrationNum = '".mysql_escape_custom(trim($this->szVatRegistrationNum))."'
				WHERE
					id = '".(int)$this->id."'	
			";
                        $idForwarder = $this->id;
			//echo $query ;
			if($result = $this->exeSQL( $query ))
			{
                            if($bUpdatePdfLogo)
                            { 
                                $query="
                                    UPDATE
                                        ".__DBC_SCHEMATA_FROWARDER__."
                                    SET
                                        szPdfLogo= '".mysql_escape_custom(trim($fileNamePdfLogo))."'
                                    WHERE
                                        id = '".(int)$idForwarder."'	
                                ";
                                $result = $this->exeSQL( $query ); 
                            } 
                            /*
                            * update catch-up all booking e-mail
                            */ 
                            /*$kForwarderContact = new cForwarderContact();
                            $forwarderContactAry = $kForwarderContact->getAllForwarderContactsEmail($idForwarder);
                            if(empty($forwarderContactAry['szCatchUpBookingQuoteEmail']))
                            {
                                //If there is no catch-up email added for forwarder then then we adds general email as catch-up all booking
                                $catchAllEmailAry = array();
                                $catchAllEmailAry['szEmail'] = $this->szGeneralEmailAddress;
                                $catchAllEmailAry['idForwarder'] = $idForwarder;
                                $catchAllEmailAry['idForwarderContactRole'] = __CATCH_UP_ALL_BOOKING_QUOTE_PROFILE_ID__; 
                                $this->addForwarderCatchupAllBookings($catchAllEmailAry);
                            }*/
                            $this->isForwarderOnline($this->id,'UPDATE',true);
                            return true;
			}
			else
			{
                            $this->error = true;
                            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                            return false;
			}
		}
		else
		{
			return false;
		}
	}
        
        function addForwarderCatchupAllBookings($data)
        {
            if(!empty($data))
            {
                $query="
                    INSERT INTO
                       ".__DBC_SCHEMATA_FORWARDER_PREFERENCES__." 
                    (
                       szEmail,
                       idForwarder,
                       idForwarderContactRole,
                       dtCreated,
                       dtModified,
                       iActive
                    ) 
                    VALUES
                    (
                        '".mysql_escape_custom($data['szEmail'])."',  
                        '".mysql_escape_custom($data['idForwarder'])."',  
                        '".mysql_escape_custom($data['idForwarderContactRole'])."',  
                        NOW(), 
                        NOW(),
                        '1'
                    )	
               "; 
               if($this->exeSQL($query))
               {
                   return true;
               }
            }
        }
	function updatePopupdetails($idForwarder)
	{
		if((int)$idForwarder>0)
		{
			$query="
				UPDATE
					".__DBC_SCHEMATA_FROWARDER__."
				SET
					iPopupDetails = 1
				WHERE
					id = '".$idForwarder."'	
			";
			//echo $query ;
			if($result = $this->exeSQL( $query ))
			{
				$this->isForwarderOnline($this->id,'UPDATE',true);
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	
	}
	
	function checkForwarderComleteCompanyInfoUploadService($idForwarder)
	{
		if($idForwarder>0)
		{
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_FROWARDER__."	
				WHERE
					id = '".(int)$idForwarder."'	
				AND
				(
					szDisplayName =''
				OR
					szCompanyName =''	
				OR
					szAddress =''	
				OR
					szCity =''	
				OR
					idCountry =''	 
				OR
					szPhone =''
				OR
					szCompanyRegistrationNum =''	
				OR
					szLink =''	
				OR
					szCurrency =''	
				OR
					szControlPanelUrl = '' 
				)	
			";
			//echo "<br>".$query."<br>" ;
			//die;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					/*if(!$this->isForwarderPreferencesExists($idForwarder,__BOOKING_PROFILE_ID__))
					{
						return true;
					}
					else if(!$this->isForwarderPreferencesExists($idForwarder,__PAYMENT_AND_BILLING_PROFILE_ID__))
					{
						return true;
					}
					else if(!$this->isForwarderPreferencesExists($idForwarder,__PAYMENT_AND_BILLING_PROFILE_ID__))
					{
						return true;
					}
					else
					{*/
						return false;
					//}
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function getAllForwarderReferralFeeExpiryIn30Days($dtExpiry)
	{
		$query="
			SELECT
				id,
				szDisplayName,
				szControlPanelUrl,
                                szForwarderAlias
			FROM
				".__DBC_SCHEMATA_FROWARDER__."
			WHERE
				DATE(dtReferealFeeValid)='".mysql_escape_custom($dtExpiry)."'
		";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				while($row=$this->getAssoc($result))
				{
					$res_arr[]=$row;
				}
				return $res_arr;
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function getAllAdminEmail()
	{
		$query = "
			SELECT
				szEmail,
				id
			FROM
				".__DBC_SCHEMATA_MANAGEMENT__."		
			WHERE
					iActive = 1		
		";
		if($result=$this->exeSQL($query))
		{
			$profileArr = array();
			while($row=$this->getAssoc($result))
			{
				$profileArr[] = $row;
			}	
			
			return $profileArr;
		}
		else
		{
			return array();
		}
	}
	
	function updateExpiryServiceEmailDeail($idForwarder,$update_key,$type)
	{
		$query="
			UPDATE
				".__DBC_SCHEMATA_FROWARDER__."
			SET
				szConfirmationKeyForExpiryMail='".mysql_escape_custom($update_key)."',
				dtExpiryMail=NOW(),
				iExpiryMailType='".(int)$type."',
				szServiceUpdatedBy=''	
			WHERE
				id='".(int)$idForwarder."'
		";
		$result=$this->exeSQL($query);
	}
	
	
	function checkExpiryConfirmationKey($update_key)
	{
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_FROWARDER__."
			WHERE
				szConfirmationKeyForExpiryMail='".mysql_escape_custom($update_key)."'
			AND
				id='".(int)$_SESSION['forwarder_id']."'
		";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function updateForwarderLCLServices($flag=true,$date)
	{
		if($flag)
		{
			$date = strtotime($date);
			$dateFrom = strtotime("+".__FROM_DATE__, $date);
			$dateTo = strtotime("+".__TO_DATE__, $date);
			$datefrom =date('Y-m-d',($dateFrom));
			$dateto =date('Y-m-d',($dateTo));
			
			$sql .="
					date(dtExpiry)>='".mysql_escape_custom($datefrom)."'
				AND
					date(dtExpiry)<='".mysql_escape_custom($dateto)."'";
			
			$date = strtotime($dateto);
			$dateExpiry = strtotime("+ 1 MONTH", $date);
			$dtExpiry =date('Y-m-d',($dateExpiry));
		}
		else
		{
			$today=date('Y-m-d',strtotime($date));
			$sql .="
					date(dtExpiry)='".mysql_escape_custom($today)."'";
			
			$date = strtotime($date);
			$dateExpiry = strtotime("+ 1 MONTH", $date);
			$dtExpiry =date('Y-m-d',($dateExpiry));
		}
		
		
		$kWHSSearch=new cWHSSearch();
		$kConfig = new cConfig();
		$query="
			SELECT
				id,
				idWarehouseFrom,
				idWarehouseTo,
				dtExpiry
			FROM
				".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
			WHERE
				".$sql."
			AND
				iActive='1'
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$ctr=0;
				$forwarderArr=array();
				while($row=$this->getAssoc($result))
				{
					$counter=0;
					
					$kWHSSearch->load($row['idWarehouseFrom']);
					$idForwarder=$kWHSSearch->idForwarder;
					if($_SESSION['forwarder_id']==$idForwarder)
					{
					
						$query="
							UPDATE
								".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
							SET
								dtExpiry='".mysql_escape_custom($dtExpiry)."'
							WHERE
								id='".(int)$row['id']."'						
						";
						$result2=$this->exeSQL($query);
												
						++$ctr;
					}
				}
				if($ctr>0)
				{
					$kForwarderContact = new cForwarderContact();
					$kForwarderContact->load($_SESSION['forwarder_user_id']);
					$szName=$kForwarderContact->szFirstName." ".$kForwarderContact->szLastName;
					
					$query="
						UPDATE
							".__DBC_SCHEMATA_FROWARDER__."
						SET
							szServiceUpdatedBy='".mysql_escape_custom($szName)."'
						WHERE
							id='".(int)$_SESSION['forwarder_id']."'						
					";
					$result1=$this->exeSQL($query);
					
					return $ctr;
				}
				else
				{
					return 0;
				}
			}
			else
			{
				return 0;	
			}
		}
	}
	function isForwarderHasBooking($idForwarder)
	{
		if($idForwarder>0)
		{
			$query="
				SELECT
				    id
				FROM
					".__DBC_SCHEMATA_BOOKING__."
				WHERE
					idForwarder = '".(int)$idForwarder."'
				AND
					idBookingStatus IN ('".__BOOKING_STATUS_CONFIRMED__."','".__BOOKING_STATUS_ARCHIVED__."')
			";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return $this->iNumRows;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
	}
	
	
	function checkServiceAvailableForForwarder($idForwarder,$idOriginCountry,$idDestinationCountry)
	{
		
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_WAREHOUSES__."
			WHERE
				idForwarder='".(int)$idForwarder."'
			AND
				idCountry='".(int)$idOriginCountry."'
			AND
				iActive='1'
		";
		//echo $query."<br/>";
		if($result=$this->exeSQL($query))
		{
			$origin_country = array();
			while($row=$this->getAssoc($result))
			{
				$origin_country[] = $row['id'];
			}	
		}
		$origin_str='';
		if(!empty($origin_country))
		{
			$origin_str=implode("','",$origin_country);
		}
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_WAREHOUSES__."
			WHERE
				idForwarder='".(int)$idForwarder."'
			AND
				idCountry='".(int)$idDestinationCountry."'
			AND
				iActive='1'
		";
		if($result=$this->exeSQL($query))
		{
			$destination_country = array();
			while($row=$this->getAssoc($result))
			{
				$destination_country[] = $row['id'];
			}	
		}
		$destination_str='';
		if(!empty($destination_country))
		{
			$destination_str=implode("','",$destination_country);
		}
	
		if($destination_str!='' && $origin_str!='')
		{
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
				WHERE
					dtExpiry > NOW()
				AND
					idWarehouseFrom IN ('".$origin_str."')
				AND
					idWarehouseTo IN ('".$destination_str."')
				AND
					iActive='1'
			";
			//echo $query."<br/>";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{	
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function getForwardrVersionComment()
	{
		$query="
			SELECT
				 szComment
			FROM
				".__DBC_SCHEMATA_VERSION__."
			WHERE
				iFlag='2'
			ORDER BY
				id DESC
			LIMIT 0,1
		";
		if($result = $this->exeSQL( $query ))
		{
			if ($this->getRowCnt() > 0)
			{
				$row=$this->getAssoc($result);
				
				$szComment=$row['szComment'];
				
				return $szComment;
			}
			else
			{
				return '';
			}
		}
		else
		{
			return '';
		}
	}
        
        function updatePrivateCustomerPrices($data)
        {
            if(!empty($data))
            { 
                $possibleProductType = array();
                $possibleProductType = array('LCL','LTL','COURIER','AIR');
                
                /*
                * Validating Input fields
                */
                foreach($possibleProductType as $possibleProductTypes)
                {
                    $bRequired = false;
                    if($data['iPrivateCustomerAvailable'][$possibleProductTypes])
                    { 
                        $this->iPrivateCustomerAvailable[$possibleProductTypes] = 1;
                        $bRequired = true;
                    }
                    else
                    {
                        $this->iPrivateCustomerAvailable[$possibleProductTypes] = false;
                    }
                    $this->set_idCurrency(sanitize_all_html_input($data['idCurrency'][$possibleProductTypes]),$possibleProductTypes);
                    $this->set_fCustomerFee(sanitize_all_html_input($data['fCustomerFee'][$possibleProductTypes]),$possibleProductTypes);
                } 
                if($this->error==true)
                {
                    return false;
                } 
                
                $kForwarderContact = new cForwarderContact();
                $kForwarderContact->getUserDetails($_SESSION['forwarder_user_id']);
                $idForwarder = $kForwarderContact->idForwarder;
                
                foreach($possibleProductType as $possibleProductTypes)
                { 
                    $addPrivateFeeAry = array();
                    $addPrivateFeeAry['iPrivateCustomerAvailable'] = $this->iPrivateCustomerAvailable[$possibleProductTypes];
                    $addPrivateFeeAry['idCurrency'] = $this->idCurrency[$possibleProductTypes];
                    $addPrivateFeeAry['fCustomerFee'] = $this->fCustomerFee[$possibleProductTypes];
                    $addPrivateFeeAry['szProduct'] = $possibleProductTypes;
                    $addPrivateFeeAry['idForwarder'] = $idForwarder;
                    $this->addPrivateCustomerPricing($addPrivateFeeAry); 
                }
                return true;
            } 
        }
        
        function addPrivateCustomerPricing($data)
        {
            if(!empty($data))
            { 
                $idCurrency = $data['idCurrency']; 
                $idForwarder= $data['idForwarder'];
                
                if($idCurrency==1)
                {
                    $szCurrency = 'USD';
                    $fExchangeRate = 1.00 ;
                }
                else
                {
                    $kWHSSearch = new cWHSSearch();
                    $forwarderNewCurrencyAry = $kWHSSearch->getCurrencyDetails($idCurrency);
                
                    $szCurrency = $forwarderNewCurrencyAry['szCurrency'];
                    $fExchangeRate = $forwarderNewCurrencyAry['fUsdValue']; 
                }
                
                $iUpdatedByAdmin = 0;
                if((int)$_SESSION['forwarder_admin_id']>0)
                {
                    $iUpdatedByAdmin = 1;
                    $idForwarderUpdatedBy = $_SESSION['forwarder_admin_id'];
                }
                else
                {
                    $idForwarderUpdatedBy = $_SESSION['forwarder_user_id'];
                }
                
                if($this->isPrivateCustomerPricingExists($data['szProduct'],$idForwarder))
                {
                    $query="
                        UPDATE
                            ".__DBC_SCHEMATA_PRIVATE_CUSTOMER_SETTING__."
                        SET
                            idCurrency = '".mysql_escape_custom(trim($idCurrency))."', 
                            szCurrency = '".mysql_escape_custom(trim($szCurrency))."', 
                            fExchangeRate = '".mysql_escape_custom(trim($fExchangeRate))."', 
                            fCustomerFee = '".mysql_escape_custom(trim($data['fCustomerFee']))."', 
                            idForwarderUpdatedBy = '".mysql_escape_custom(trim($idForwarderUpdatedBy))."',
                            iUpdatedByAdmin = '".mysql_escape_custom(trim($iUpdatedByAdmin))."',
                            iPrivateCustomerAvailable = '".(int)$data['iPrivateCustomerAvailable']."',
                            dtUpdateOn = now()
                        WHERE
                            idForwarder = '".(int)$idForwarder."'
                        AND
                            szProduct = '".mysql_escape_custom(trim($data['szProduct']))."'    
                    ";
                }
                else
                {
                    $query = "
                        INSERT INTO
                            ".__DBC_SCHEMATA_PRIVATE_CUSTOMER_SETTING__."
                        (
                            iPrivateCustomerAvailable,
                            szProduct,
                            idForwarder,
                            idCurrency,
                            szCurrency,
                            fExchangeRate,
                            fCustomerFee,
                            idForwarderUpdatedBy, 
                            iUpdatedByAdmin,
                            iActive,
                            dtCreatedOn
                        )
                        VALUES
                        (
                            '".mysql_escape_custom($data['iPrivateCustomerAvailable'])."',
                            '".mysql_escape_custom($data['szProduct'])."',
                            '".mysql_escape_custom($idForwarder)."',
                            '".mysql_escape_custom($idCurrency)."',
                            '".mysql_escape_custom($szCurrency)."',
                            '".mysql_escape_custom($fExchangeRate)."',
                            '".mysql_escape_custom($data['fCustomerFee'])."', 
                            '".mysql_escape_custom($idForwarderUpdatedBy)."',
                            '".mysql_escape_custom($iUpdatedByAdmin)."',
                            '1',
                            now()
                        )	
                    ";
                }
//                echo $query;
//                die;
                if($result=$this->exeSQL($query))
                {
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
        }
        
        function isPrivateCustomerPricingExists($szProduct,$idForwarder)
        {
            if(!empty($szProduct) && $idForwarder>0)
            {
                $query="
                    SELECT
                        id
                    FROM
                        ".__DBC_SCHEMATA_PRIVATE_CUSTOMER_SETTING__."
                    WHERE
                        idForwarder = '".(int)$idForwarder."'
                    AND
                        szProduct = '".mysql_escape_custom(trim($szProduct))."'     
                ";
                //echo "<br>".$query."<br>" ;
                //die;
                if($result=$this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        return true;
                    }
                    else
                    { 
                        return false;
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            } 
        }
        
        function getAllPrivateCustomerPricing($idForwarder,$szProduct=false,$iPrivateCustomerAvailable=false)
        {
            if($idForwarder>0)
            {
                if(!empty($szProduct))
                {
                    $query_and = " AND szProduct = '".mysql_escape_custom($szProduct)."' ";
                }
                if($iPrivateCustomerAvailable>0)
                {
                    if($iPrivateCustomerAvailable==1)
                    {
                        $query_and .= " AND iPrivateCustomerAvailable = '1' ";
                    }
                    else
                    {
                        $query_and .= " AND iPrivateCustomerAvailable = '0' ";
                    }
                }
                $query="
                    SELECT
                        id,
                        iPrivateCustomerAvailable,
                        szProduct,
                        idForwarder,
                        idCurrency,
                        szCurrency,
                        fExchangeRate,
                        fCustomerFee,
                        idForwarderUpdatedBy, 
                        iUpdatedByAdmin,
                        dtCreatedOn,
                        iActive
                    FROM
                        ".__DBC_SCHEMATA_PRIVATE_CUSTOMER_SETTING__."
                    WHERE
                        idForwarder = '".(int)$idForwarder."'
                    AND
                        iActive = '1'   
                    $query_and
                ";
                //echo "<br>".$query."<br>" ;
                //die;
                if($result=$this->exeSQL($query))
                {
                    $ret_ary = array();
                    while($row=$this->getAssoc($result))
                    {
                        $ret_ary[$row['szProduct']] = $row;
                    }
                    return $ret_ary;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            } 
        }
        
        function deletePrivateCustomerPricing($szProduct,$idForwarder)
        {
            $query="
                DELETE FROM
                    ".__DBC_SCHEMATA_PRIVATE_CUSTOMER_SETTING__."
                WHERE
                    idForwarder = '".(int)$idForwarder."'
                AND
                    szProduct = '".mysql_escape_custom(trim($szProduct))."'
            ";
            //echo "<br /> ".$query."<br />";
            if($result = $this->exeSQL($query))
            {
                return true;
            }
            else
            {
                return false;
            }
        } 
        
        function dummyPrivateCustomerPricing()
        {
            $forwarderAry = array();
            $forwarderAry = $this->getAllForwarder(false,false,true);
            
            $possibleProductType = array();
            $possibleProductType = array('LCL','LTL','COURIER','AIR');
                    
            if(!empty($forwarderAry))
            {
                foreach($forwarderAry as $forwarderArys)
                { 
                    foreach($possibleProductType as $possibleProductTypes)
                    {
                        $addPrivateFeeAry = array();
                        $addPrivateFeeAry['idCurrency'] = $forwarderArys['szCurrency'];
                        $addPrivateFeeAry['fCustomerFee'] = 0;
                        $addPrivateFeeAry['szProduct'] = $possibleProductTypes;
                        $addPrivateFeeAry['idForwarder'] = $forwarderArys['id'];
                        $addPrivateFeeAry['iPrivateCustomerAvailable'] = 1; 
                        
                        $this->addPrivateCustomerPricing($addPrivateFeeAry);
                    } 
                }
            }
        }
        
        function listAllForwarderByProduct($szProduct)
        {
            if(!empty($szProduct))
            {
                $query="
                    SELECT  
                        DISTINCT idForwarder 
                    FROM
                        ".__DBC_SCHEMATA_PRIVATE_CUSTOMER_SETTING__."
                    WHERE
                        szProduct = '".  mysql_escape_custom($szProduct)."'
                    AND
                        iActive = '1'    
                    AND
                        iPrivateCustomerAvailable = '1'
                ";
                //echo "<br>".$query."<br>" ;
                //die;
                if($result=$this->exeSQL($query))
                {
                    $ret_ary = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {
                        $ret_ary[$ctr] = $row['idForwarder'];
                        $ctr++;
                    }
                    return $ret_ary;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
        }
        
        
        function getAllPaymentFrequency($idPaymentFrequency=false)
        {
            /*
             * We are no longer using table tblpaymentfrequency  
            if($idPaymentFrequency>0)
            {
                $query_and = " AND id = '".(int)$idPaymentFrequency."' ";
            } 
            $query="
                SELECT
                    id, 
                    iNumMonth,
                    iSortOrder
                FROM	
                    ".__DBC_SCHEMATA_PAYMENT_FREQUENCY__." 	
                WHERE
                    iActive = '1'
                    $query_and
                ORDER BY
                  iSortOrder ASC  
            ";		
            //echo $query ;
            if($result=$this->exeSQL($query))
            {
                $paymentFrequencyAry=array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    $paymentFrequencyAry[$row['id']] = $row; 
                }
                return $paymentFrequencyAry ;
            }
             * 
             */
        }
        
	function set_id( $value )
	{
            $this->id = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "id", t($this->t_base.'fields/customer_id'), false, false, true );
	}
	function set_szFirstName( $value )
	{
		$this->szFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szFirstName", t($this->t_base.'fields/f_name'), false, 255, true );
	}
	function set_szLastName( $value )
	{
		$this->szLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLastName", t($this->t_base.'fields/l_name'), false, 255, true );
	}
	function set_szEmail( $value )
	{ 
		$this->szEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szLoginEmail", t($this->t_base.'fields/email'), false, 255, true );
	
	}
	function set_szNewEmail( $value )
	{
		$this->szNewEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szNewEmail", t($this->t_base.'fields/email'), false, 255, true );
	}
	function set_szCountry( $value )
	{
		$this->szCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCountry", t($this->t_base.'fields/country'), false, 255, true );
	}
	function set_szPhoneNumber( $value )
	{
		$this->szPhoneNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPhoneNumber", t($this->t_base.'fields/p_number'), false, false, true );
	}
	function set_szCompanyName( $value )
	{
		$this->szCompanyName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCompanyName", t($this->t_base.'fields/c_name'), false, 255, true );
	}
	function set_szWebSite( $value )
	{
		$this->szWebSite = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szWebSite", t($this->t_base.'fields/website'), false, 255, true );
	}
	function set_szNonFrocePassword( $value )
	{
		$this->szNonFrocePassword = $this->validateInput( $value, __VLD_CASE_PASSWORD_NOFORCE_, "szPassword", t($this->t_base.'fields/password'), false, 255, true );
	}
	function set_szCompanyRegistrationNum( $value )
	{
		$this->szCompanyRegistrationNum = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCompanyRegistrationNum", t($this->t_base.'fields/company_reg_num'), false, 255, true );
	}
	function set_szVatRegistrationNum( $value )
	{
		$this->szVatRegistrationNum = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szVatRegistrationNum", t($this->t_base.'fields/vat_reg_num'), false, 255, false );
	}
	function set_szAddress1( $value )
	{
		$this->szAddress1 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAddress1", t($this->t_base.'fields/address1'), false, 255, true );
	}
	function set_szAddress2( $value )
	{
		$this->szAddress2 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAddress2", t($this->t_base.'fields/address2'), false, 255, false );
	}
	function set_szAddress3( $value )
	{
		$this->szAddress3 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAddress3", t($this->t_base.'fields/address3'), false, 255, false );
	}
    function set_szPostcode( $value )
	{
		$this->szPostcode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPostcode", t($this->t_base.'fields/postcode'), false, 255, false );
	}
	
	function set_szState( $value )
	{
		$this->szState = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szState", t($this->t_base.'fields/province'), false, 255, false );
	}
	function set_szCity( $value )
	{
		$this->szCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCity", t($this->t_base.'fields/city'), false, 255, true );
	}
	
	function set_szDisplayName( $value )
	{
		$this->szDisplayName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDisplayName", t($this->t_base_comp_info.'titles/display_name'), false, 255, true );
	}
	function set_szLink( $value )
	{
                if($value!='')
                {
                    if(!checkUrlHavingHttpOrHttps($value))
                    {
                        $this->addError( "szLink" , 'Hyperlink to Standard Terms must start with "http" or "https".' );
                    }
                    else
                    {
                        $this->szLink = $value;
                    }
                }
                else
                {
                    $this->szLink = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLink", t($this->t_base_comp_info.'titles/standard_terms'), false, 255, true );
                }
	}
	function set_szLogo( $value )
	{
		$this->szLogo = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLogo", t($this->t_base.'fields/display_logo'), false, 255, false );
	}
 	function set_szBankName( $value )
	{
		$this->szBankName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szBankName", t($this->t_base.'fields/bank_name'), false, 255, true );
	}
	function set_idBankCountry( $value )
	{
		$this->idBankCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idBankCountry", t($this->t_base.'fields/Country'), false, 255, true );
	}
	function set_szNameOnAccount( $value )
	{
		$this->szNameOnAccount = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szNameOnAccount", t($this->t_base.'fields/account_name'), false, 255, true );
	}
	function set_iSortCode( $value )
	{
		$this->iSortCode = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iSortCode", t($this->t_base.'fields/sort_code'), false, false, false );
	}
	function set_iAccountNumber( $value )
	{
		$this->iAccountNumber = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iAccountNumber", t($this->t_base.'fields/account_number'), false, false, true );
	}
	function set_szIBANAccountNumber( $value )
	{
		$this->szIBANAccountNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szIBANAccountNumber", t($this->t_base.'fields/IBAN_account_number'), false, 34, false );
	}	
	function set_szCurrency( $value )
	{
		$this->szCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "szCurrency", t($this->t_base.'fields/account_currency'), false, false, true );
	}
	function set_szForwarderLogo( $value )
	{
		$this->szForwarderLogo = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szForwarderLogo", t($this->t_base.'fields/display_logo'), false, 255, true );
	}
	function set_szNonAcceptance ( $value )
	{
		$this->sznonAcceptance = $this ->validateInput( $value, __VLD_CASE_ADDRESS__, "sznonAcceptance", t($this->t_base.'Error/enter_field'), false, 255, true );
	}
	function set_szSwiftCode( $value )
	{
		$this->szSwiftCode = $this ->validateInput( $value, __VLD_CASE_ADDRESS__, "szSwiftCode", t($this->t_base.'fields/swift_code'), 8, 11, true );
	}
	function set_szControlPanelUrl( $value )
	{
		$this->szControlPanelUrl = $this ->validateInput( $value, __VLD_CASE_ANYTHING__, "szControlPanelUrl", t($this->t_base.'fields/szControlPanelUrl'), false, 255, true );
	}
	function set_szPassword( $value )
	{
		$this->szPassword = $this ->validateInput( $value, __VLD_CASE_PASSWORD__, "szPassword", t($this->t_base.'fields/szPassword'), false, 255, true );
	}
	function set_szcode( $value )
	{
		$this->szcode = $this ->validateInput( $value, __VLD_CASE_ANYTHING__, "szcode", t($this->t_base.'fields/szCode'), false, 2, true );
	}
	function set_szComment( $value )
	{   
		$this->szComment = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szComment", t($this->t_base.'fields/szComment'), false, false, false );
	}
	function set_dtValidTo( $value )
	{   
		$this->dtValidTo = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "dtValidTo", t($this->t_base.'fields/dtValidTo'), false, 10, true );
	}
	function set_iAutomaticPaymentInvoicing( $value )
	{   
		$this->iAutomaticPaymentInvoicing = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iAutomaticPaymentInvoicing", t($this->t_base.'fields/auto'), false, 1, true );
	}
        function set_iCreditDays( $value )
	{   
            $this->iCreditDays = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iCreditDays", t($this->t_base.'fields/iCreditDays'), 0, false, true );
	} 
	function set_fRefFee( $value )
	{ 
		$this->fRefFee = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "fRefFee", t($this->t_base.'fields/fRefFee'), false, 255, true );
	}
	function set_szGeneralEmailAddress( $value )
	{
		$this->szGeneralEmailAddress = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szGeneralEmailAddress", t($this->t_base.'fields/szGeneralEmailAddress'), false, 255, true );
	}
        function set_idCurrency( $value, $szProductType,$flag=true )
	{
            $this->idCurrency[$szProductType] = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idCurrency_".$szProductType, t($this->t_base.'fields/Country'), false, false, $flag );
	}
        function set_fCustomerFee( $value, $szProductType,$flag=true )
	{
            $this->fCustomerFee[$szProductType] = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fCustomerFee_".$szProductType, t($this->t_base.'fields/Country'), false, false, $flag );
	}
        
        function set_fReferralFee( $value, $idTransportMode )
	{ 
            $this->fReferralFee[$idTransportMode] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "fReferralFee_".$idTransportMode, t($this->t_base.'fields/fRefFee'), 0, false, true );
	}
}	