<?php
/**
 * This file is the container for gmail api related functionality. 
 *
 * gmail.class.php
 *
 * @copyright Copyright (C) 2015 Transporteca
 * @author Ajay
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" ); 
require_once( __APP_PATH__ . "/inc/functions.php" ); 
require_once( __APP_PATH_CLASSES__ . "/customerLogs.class.php");

class cCRM extends cDatabase
{    
    function __construct()
    {
        parent::__construct();
        return true;
    }
    
    function addCrmEmailSnooze($data)
    {
        if(!empty($data))
        {
            $data['dtReminderDate'] = format_date_time($data['dtReminderDate']);
            $this->set_dtReminderDate(trim(sanitize_all_html_input($data['dtReminderDate'])));
            $this->set_szReminderTime(trim(sanitize_all_html_input($data['szReminderTime'])));
            $this->set_idCrmEmailLogs(trim(sanitize_all_html_input($data['idCrmEmailLogs'])));

            $szDateKey = "dtCrmReminderDate";
            $szTimeKey = "szCrmReminderTime";
            
            if($this->error==true)
            {
                return false;
            }
            $idCrmEmailLogs = $this->idCrmEmailLogs;
            $dtRemindDate = strtotime($data['dtReminderDate']) ; 
            $dtTodayDateTime = strtotime(date('Y-m-d')); 
            if($dtTodayDateTime>$dtRemindDate)
            {
                $this->addError($szDateKey,'Remind date must be greater then today.');
                return false;
            } 
            if(!empty($data['szReminderTime']))
            {
                $szReminderTimeAry = explode(":",$data['szReminderTime']);
                if($szReminderTimeAry[0]>23)
                {
                    $this->addError($szTimeKey,'Invalid hour format.');
                    return false;
                }
                else if($szReminderTimeAry[1]>=60)
                {
                    $this->addError($szTimeKey,'Invalid minute format.');
                    return false;
                }
            }  
            $dtReminderDate = $data['dtReminderDate']." ".$data['szReminderTime'].":00";  
            if(empty($_SESSION['szCustomerTimeZoneGlobal']))
            {
                $_SESSION['szCustomerTimeZoneGlobal'] = 'Europe/Copenhagen' ;
            }
            $szServerTimeZone = date_default_timezone_get(); 
            if(empty($szServerTimeZone))
            {
                $szServerTimeZone = 'Europe/London';
            }
            $date = new DateTime($dtReminderDate,new DateTimeZone($_SESSION['szCustomerTimeZoneGlobal']));   
            $dtReminderDate = $date->format('Y-m-d H:i:s');

            $date->setTimezone(new DateTimeZone($szServerTimeZone));  
            $dtReminderDate = $date->format('Y-m-d H:i:s');  

            $dtCurrentDate = date('Y-m-d H:i:s'); 
            $date_1 = new DateTime($dtCurrentDate,new DateTimeZone($_SESSION['szCustomerTimeZoneGlobal']));   
            $date_1->setTimezone(new DateTimeZone($szServerTimeZone));
            $dtCurrentDateTime = $date_1->format('Y-m-d H:i:s');

            $dtRemindDateTime = strtotime($dtReminderDate) ; 
            $dtTodayDateTime = strtotime($dtCurrentDateTime); 
            if($dtTodayDateTime>$dtRemindDateTime)
            {
                $this->addError($szTimeKey,'Remind date must be greater then today.');
                return false;
            } 
            if($this->error==true)
            {
                return false;
            }
            if(date('I')==1)
            {  
                //Our server's My sql time is 1 hours behind so that's why added 2 hours to sent date 
                //$dtReminderDate = date('Y-m-d H:i:s',strtotime("-1 HOUR",strtotime($dtReminderDate)));  
            }
            
            $fileLogsAry = array();  
            $fileLogsAry['iSnoozeEmail'] = 1;
            $fileLogsAry['dtSnoozeDate'] = $dtReminderDate;
              
            $file_log_query = '';
            if(!empty($fileLogsAry))
            {
                foreach($fileLogsAry as $key=>$value)
                {
                    $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            } 
            $file_log_query = rtrim($file_log_query,",");   
            
            if($this->updateCrmEmailDetails($file_log_query,$idCrmEmailLogs))
            {
                return true;
            }
            else
            {
                return false;
            }
        } 
    }
    
    function updateCrmEmailDetails($update_str,$idCrmEmailLogs,$update_from_customer=false)
    { 
        if(!empty($update_str) && $idCrmEmailLogs>0)
        { 
            $kBooking_new = new cBooking();   
            if(!$update_from_customer)
            {
                $query_update = " idAdminUpdatedBy = '".(int)$_SESSION['admin_id']."', ";
            }   
            $query="	
                UPDATE
                    ".__DBC_SCHEMATA_EMAIL_LOG__."
                SET
                    ".$update_str.", 
                    $query_update
                    dtUpdatedOn = now()
                WHERE
                    id = '".(int)$idCrmEmailLogs."';
            ";
//            echo $query ;
//            die;
            if($result=$this->exeSQL($query))
            {
                return true;
            }
            else
            { 
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    } 
    
    function updateCrmSnoozeFlag()
    {
        $query="	
            UPDATE
                ".__DBC_SCHEMATA_EMAIL_LOG__."
            SET
                dtSnoozeDate = '',
                iSnoozeEmail = '0',
                dtUpdatedOn = now()
            WHERE 
                dtSnoozeDate != '0000-00-00 00:00:00' 
            AND
                dtSnoozeDate < now()
            AND
                iSnoozeEmail = 1 
        ";
 //echo $query ;
//            die;
        if($result=$this->exeSQL($query))
        {
            return true;
        }
        else
        { 
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
    
    function closeCrmEmail($data)
    {
        if(!empty($data))
        {
            $this->set_idCrmEmailLogs(trim(sanitize_all_html_input($data['idCrmEmailLogs'])));
            
            if($this->error==true)
            {
                return false;
            }
            
            $idCrmEmailLogs = $this->idCrmEmailLogs;
            
            $fileLogsAry = array();  
            $fileLogsAry['iClosed'] = 1; 
             
            $file_log_query = '';
            if(!empty($fileLogsAry))
            {
                foreach($fileLogsAry as $key=>$value)
                {
                    $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            } 
            $file_log_query = rtrim($file_log_query,",");   
            
            if($this->updateCrmEmailDetails($file_log_query,$idCrmEmailLogs))
            {
                return true;
            }
            else
            {
                return false;
            }
        } 
    } 
    /*
     * Following function will move email from file id $idBookingFile to $idNewBookingFile
     */
    function reassignCrmEmailFile($data,$szType='REASSIGN_FILE')
    {
        if(!empty($data))
        { 
            if($szType=='REASSIGN_FILE')
            {
                $idBookingFile = (int)$data['idBookingFile'];
                $idNewBookingFile = (int)$data['idNewBookingFile'];
                $idCrmEmailLogs = (int)$data['idCrmEmailLogs'];

                $kBooking = new cBooking(); 
                $kBookingFile = new cBooking();
                $kBooking->loadFile($idBookingFile); 
                $idBooking = $kBooking->idBooking;
                $szPreviousTask = $kBooking->szPreviousTask;
                
                $kBookingNew = new cBooking();
                $kBookingNew->loadFile($idNewBookingFile);

                $szTransportecaTask = $kBookingNew->szTaskStatus;
                $iNumUnreadMessage = $kBookingNew->iNumUnreadMessage;
                  
                /*
                * When we re-assign a crm email to another file generally we do following 3 things
                * 1. Remove T210. Read Message Task from current
                * 2. Add T210. Read Message Task to new file in which we are assigning the email
                * 3. update idBooking and idUser in tblemaillog if applicable.
                */ 
                if($kBooking->id>0 && $kBookingNew->id>0 && $idCrmEmailLogs>0)
                {
                    $bUpdateCustomer = false;
                    if($kBooking->idUser!=$kBookingNew->idUser)
                    {
                        $bUpdateCustomer = true;
                    } 

                    $dtResponseTime = $kBookingFile->getRealNow();
                     
                    $kCrm = new cCRM();
                    $iNumUnseenEmails = $kCrm->getUnreadEmailsByBookingID($idBooking,true); 

                    if($iNumUnseenEmails>1)
                    { 
                        /*
                        * If a file has more than 1 unread message then we reassign next CRM email task to the file
                        */
                        $this->updateReadMesageTask($idBookingFile,true);
                    }
                    else
                    {
                        /*
                        * Removing T210. Read Message Task from old file
                        */
                        $fileLogsAry = array();  
                        $fileLogsAry['szTransportecaTask'] = $szPreviousTask;  
                        $fileLogsAry['iNumUnreadMessage'] = 0;
                        $file_log_query = '';
                        if(!empty($fileLogsAry))
                        {
                            foreach($fileLogsAry as $key=>$value)
                            {
                                $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                            } 
                            $file_log_query = rtrim($file_log_query,",");     
                            $kBookingFile->updateBookingFileDetails($file_log_query,$idBookingFile);  
                        }
                    } 
                    
                    /*
                    * Adding T210. Read Message Task from new file
                    */
                    $fileLogsAry = array();  
                    $fileLogsAry['szTransportecaTask'] = "T210";  
                    //$fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime;
                    $fileLogsAry['iClosed'] = 0; 
                    $fileLogsAry['idEmailLogs'] = $idCrmEmailLogs;
                    if($szTransportecaTask=='T210')
                    {
                        $fileLogsAry['iNumUnreadMessage'] = $iNumUnreadMessage + 1;
                    }
                    else
                    {
                        $fileLogsAry['iNumUnreadMessage'] = 1;
                    }
                    
                    $file_log_query = '';
                    if(!empty($fileLogsAry))
                    {
                        foreach($fileLogsAry as $key=>$value)
                        {
                            $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                        } 
                        $file_log_query = rtrim($file_log_query,",");     
                        $kBookingFile->updateBookingFileDetails($file_log_query,$idNewBookingFile);  
                    } 
                    
                    /*
                    * Updating idBooking in tblemaillogs
                    */
                    
                    $fileLogsAry = array();  
                    $fileLogsAry['idBooking'] = $kBookingNew->idBooking; 
                    $fileLogsAry['iSeen'] = 0;
                    if($bUpdateCustomer)
                    {
                        $idCustomer = $kBookingNew->idUser;
                        $kUser = new cUser();
                        $kUser->getUserDetails($idCustomer);
                        $szCustomerEmail = $kUser->szEmail;

                        $fileLogsAry['idUser'] = $kBookingNew->idUser; 
                        $fileLogsAry['iLinkedFlag'] = '1'; 
                        //$fileLogsAry['szToAddress'] = $kUser->szEmail;
                    } 
                    $file_log_query = '';
                    if(!empty($fileLogsAry))
                    {
                        foreach($fileLogsAry as $key=>$value)
                        {
                            $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                        }
                    } 
                    $file_log_query = rtrim($file_log_query,",");    
                    if($this->updateCrmEmailDetails($file_log_query,$idCrmEmailLogs))
                    { 
                        /*
                        * Updating seen flag in customer log snippet
                        */ 
                        $kCustomerLogs = new cCustomerLogs();
                        $kCustomerLogs->updateCustomerLogSnippet($fileLogsAry,$idCrmEmailLogs,__CUSTOMER_LOG_SNIPPET_TYPE_EMAIL__);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else if($szType=='REASSIGN_CUSTOMER')
            {
                $idCustomer = $data['idCustomer'];
                $idCrmEmailLogs = $data['idCrmEmailLogs'];
                $idBookingFile = (int)$data['idBookingFile'];
                
                if($idCustomer>0 && $idCrmEmailLogs>0 && $idBookingFile>0)
                {
                    $kUser = new cUser();
                    $kUser->getUserDetails($idCustomer);
                    $szCustomerEmail = $kUser->szEmail;

                    $kBooking = new cBooking(); 
                    $kBooking->loadFile($idBookingFile);
                
                    $bUpdateBooking = false;
                    if($kBooking->idUser!=$idCustomer)
                    {
                        $bUpdateBooking = true;
                    } 
                    $fileLogsAry = array();   
                    $fileLogsAry['idUser'] = $idCustomer; 
                    $fileLogsAry['iLinkedFlag'] = '1';
                    //$fileLogsAry['szToAddress'] = $kUser->szEmail; 
                    
                    if($bUpdateBooking)
                    {
                        $fileLogsAry['idBooking'] = 0;  
                    }
 
                    $file_log_query = '';
                    if(!empty($fileLogsAry))
                    {
                        foreach($fileLogsAry as $key=>$value)
                        {
                            $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                        }
                    } 
                    $file_log_query = rtrim($file_log_query,",");    
                    if($this->updateCrmEmailDetails($file_log_query,$idCrmEmailLogs))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                } 
            } 
        }
    }
    
    function getLinkedCustomerSearchResult($szNameString)
    { 
        if(!empty($szNameString))
        { 
            $szNameString = strtolower($szNameString);
            $query_and .= " AND ( (LOWER(szFirstName) LIKE '%".mysql_escape_custom(trim($szNameString))."%') OR (LOWER(szLastName) LIKE '%".mysql_escape_custom(trim($szNameString))."%') OR (LOWER(szCompanyName) LIKE '%".mysql_escape_custom(trim($szNameString))."%') OR (LOWER(szEmail) LIKE '%".mysql_escape_custom(trim($szNameString))."%') ) ";
            
            $query="
                SELECT 
                    id,
                    szFirstName,
                    szLastName,
                    szEmail,
                    szCompanyName,
                    iPrivate
                FROM
                    ".__DBC_SCHEMATA_USERS__." AS b 
                WHERE
                   b.iActive = '1' 
                $query_and 
            ";
            //echo $query;
            if($result = $this->exeSQL($query))
            {
                if($this->iNumRows>0)
                { 
                    $ctr=0;
                    $ret_ary = array();
                    while($row=$this->getAssoc($result))
                    {
                        $ret_ary[$ctr] = $row;
                        $ctr++;
                    } 
                    return $ret_ary;
                }
                else
                {
                    return false;
                }
            } 
        }  
    }
    
    function createUpdateCrmUserDetails($data)
    {
        if(!empty($data))
        { 
            $required_all_flag = false;
            $this->set_szFirstName(trim(sanitize_all_html_input($data['szFirstName'])),$required_all_flag);
            $this->set_szLastName(trim(sanitize_all_html_input($data['szLastName'])),$required_all_flag);
            $this->set_szEmail(trim(sanitize_all_html_input($data['szEmail'])),true);

            if($data['iPrivateShipping']==1)
            {
                $this->szCustomerCompanyName = $this->szFirstName." ".$this->szLastName;
                $this->szCustomerCompanyRegNo = trim(sanitize_all_html_input($data['szCustomerCompanyRegNo']));
            } 
            else
            { 
                $this->set_szCustomerCompanyName(trim(sanitize_all_html_input($data['szCustomerCompanyName'])),$required_all_flag); 
            }
            $this->set_idCustomerDialCode(trim(sanitize_all_html_input($data['idCustomerDialCode'])),$required_all_flag);
            $this->set_szCustomerPhoneNumber(trim(sanitize_all_html_input($data['szCustomerPhoneNumber'])),$required_all_flag); 
            $this->set_iBookingLanguage(trim(sanitize_all_html_input($data['iBookingLanguage'])),true); 
            $this->set_idCustomerOwner(trim(sanitize_all_html_input($data['idCustomerOwner'])),$required_all_flag);  
            $this->set_idCustomerCurrency(trim(sanitize_all_html_input($data['idCustomerCurrency'])),$required_all_flag);
            
            $this->set_szCustomerAddress1(trim(sanitize_all_html_input($data['szCustomerAddress1'])),$required_all_flag);
            $this->set_szCustomerPostCode(trim(sanitize_all_html_input($data['szCustomerPostCode'])),false);				
            $this->set_szCustomerCity(trim(sanitize_all_html_input($data['szCustomerCity'])),$required_all_flag);	
            $this->set_szCustomerCountry(trim(sanitize_all_html_input($data['szCustomerCountry'])),true);	
            $this->iAcceptNewsUpdate = $data['iAcceptNewsUpdate'];
            $this->iPrivateShipping = $data['iPrivateShipping'];
            $idBooking = $data['idBooking'];
            $idBookingFile = $data['idBookingFile'];
            $this->idUser = $data['idUser'];
            
            if($this->error==true)
            {
                return false;
            }
            $kUser = new cUser();
            if($kUser->isUserEmailExist($data['szEmail']))
            {  
                $idCustomer = $kUser->idIncompleteUser;
            }  
            $kRegisterShipCon = new cRegisterShipCon();
            $kBooking = new cBooking();
            
            if($idCustomer>0)
            { 
                /*
                if($kUser->isUserEmailExist($data['szEmail'],$idCustomer))
                {  
                    $this->addError('szEmail',"E-mail already exists");
                    return false;
                }
                 * 
                 */
                $updateUserDetailsAry = array(); 
                $updateUserDetailsAry['szFirstName'] = $this->szFirstName ;
                $updateUserDetailsAry['szLastName'] = $this->szLastName ; 
                $updateUserDetailsAry['szPostCode'] = $this->szCustomerPostCode ;
                $updateUserDetailsAry['szCity'] = $this->szCustomerCity ; 
                $updateUserDetailsAry['szState'] = $this->szCustomerState ;   
                $updateUserDetailsAry['szCompanyName'] = $this->szCustomerCompanyName ;  
                $updateUserDetailsAry['szAddress'] = $this->szCustomerAddress1 ;
                $updateUserDetailsAry['szCurrency'] = $this->idCustomerCurrency; 
                $updateUserDetailsAry['szPhone'] = $this->szCustomerPhoneNumber;
                $updateUserDetailsAry['idInternationalDialCode'] = $this->idCustomerDialCode ;
                $updateUserDetailsAry['idCountry'] = $this->szCustomerCountry ; 
                $updateUserDetailsAry['iPrivate'] = $this->iPrivateShipping ;
                $updateUserDetailsAry['iLanguage'] = $this->iBookingLanguage ; 
                $updateUserDetailsAry['iAcceptNewsUpdate'] = $this->iAcceptNewsUpdate;
                $updateUserDetailsAry['idCustomerOwner'] = $this->idCustomerOwner;
                $updateUserDetailsAry['iConfirmed'] = 1;
  
                if(!empty($updateUserDetailsAry))
                {
                    $update_user_query = '';
                    foreach($updateUserDetailsAry as $key=>$value)
                    {
                        $update_user_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                } 
                $update_user_query = rtrim($update_user_query,",");  
                if($kRegisterShipCon->updateUserDetailsByQuotes($update_user_query,$idCustomer))
                {

                }
            } 
            else
            {  
                $kRegisterShipCon = new cRegisterShipCon();  
                $kBooking = new cBooking();
                $szPassword = mt_rand(0,99)."".$kRegisterShipCon->generateUniqueToken(8);

                //User is not loogedin then we create a new profile
                $addUserAry = array();
                $addUserAry['szFirstName'] = $this->szFirstName ;
                $addUserAry['szLastName'] = $this->szLastName ;
                $addUserAry['szEmail'] = $this->szEmail ;
                $addUserAry['szPassword'] = $szPassword ;
                $addUserAry['szConfirmPassword'] = $szPassword ;
                $addUserAry['szPostCode'] = $this->szCustomerPostCode ;
                $addUserAry['szCity'] = $this->szCustomerCity ; 
                $addUserAry['szState'] = $this->szCustomerState ; 
                $addUserAry['szCountry'] = $this->szCustomerCountry ; 
                $addUserAry['szPhoneNumberUpdate'] = $this->szCustomerPhoneNumber ;
                $addUserAry['szCompanyName'] = $this->szCustomerCompanyName ;  
                $addUserAry['szAddress1'] = $this->szCustomerAddress1 ;
                $addUserAry['szCurrency'] = $this->idCustomerCurrency; 
                $addUserAry['iThisIsMe'] = $data['iThisIsMe'];
                $addUserAry['iConfirmed'] = 1; 
                $addUserAry['iFirstTimePassword'] = 1;  
                $addUserAry['iPrivate'] = $this->iPrivateShipping ;  
                $addUserAry['idInternationalDialCode'] = $this->idCustomerDialCode ;
                $addUserAry['iLanguage'] = $this->iBookingLanguage;

                if($kUser->createAccount($addUserAry,'QUICK_QUOTE'))
                { 
                    $idCustomer = $kUser->id ;   
                }  
            } 
            if($idCustomer>0)
            {
                $kUser->getUserDetails($idCustomer);
            } 
            
            /*
            * update customer ID to booking file.
            */ 
            $fileLogsAry = array();
            $fileLogsAry['idUser'] = $idCustomer ; 
            
            if(!empty($fileLogsAry))
            {
                $file_log_query = '';
                foreach($fileLogsAry as $key=>$value)
                {
                    $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                } 
                $file_log_query = rtrim($file_log_query,","); 
                if($kBooking->updateBookingFileDetails($file_log_query,$idBookingFile))
                {
                    //@TO DO
                }
            }
            
            /*
            * update customer details to booking.
            */ 
            $bookingAry = array();
            $bookingAry['idUser'] = $kUser->id ;				
            $bookingAry['szFirstName'] = $kUser->szFirstName ;
            $bookingAry['szLastName'] = $kUser->szLastName ;
            $bookingAry['szEmail'] = $kUser->szEmail ;
            $bookingAry['szCustomerAddress1'] = $kUser->szAddress1 ; 
            $bookingAry['szCustomerPostCode'] = $kUser->szPostCode;
            $bookingAry['szCustomerCity'] = $kUser->szCity;
            $bookingAry['szCustomerCountry'] = $kUser->szCountry;
            $bookingAry['szCustomerState'] = $kUser->szBillingState;
            $bookingAry['szCustomerPhoneNumber'] = $kUser->szPhoneNumber;
            $bookingAry['szCustomerCompanyName'] = $kUser->szCompanyName;	
            $bookingAry['szCustomerCompanyRegNo'] = $this->szCustomerCompanyRegNo; 
            $bookingAry['iPrivateShipping'] = $this->iPrivateShipping;
            $bookingAry['iPrivateCustomer'] = $this->iPrivateShipping;
            $bookingAry['idCustomerDialCode'] = $kUser->idInternationalDialCode; 
            $bookingAry['iBookingLanguage'] = $this->iBookingLanguage;
            //$bookingAry['idCustomerCurrency'] = $kUser->szCurrency;
            $bookingAry['idCustomerCurrency'] = $kUser->szCurrency;
            
            $update_query = ''; 
            if(!empty($bookingAry))
            {
                foreach($bookingAry as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            } 
            $update_query = rtrim($update_query,","); 
            $kBooking->updateDraftBooking($update_query,$idBooking); 
            
            return true; 
        }
    }
    
    function addTransportecaTask($data)
    {
        if(!empty($data))
        { 
            $data['dtTaskDate'] = format_date_time($data['dtTaskDate']);
                         
            $this->set_dtTaskDate(trim(sanitize_all_html_input($data['dtTaskDate'])));
            $this->set_dtTaskTime(trim(sanitize_all_html_input($data['dtTaskTime']))); 
            $this->set_szReminderNotes(trim(sanitize_all_html_input($data['szReminderNotes'])));
            
            if($this->error==true)
            {
                return false;
            }
            $szDateKey = 'dtTaskDate';
            $szTimeKey = 'dtTaskTime'; 
            
            $dtRemindDate = strtotime($data['dtTaskDate']) ; 
            $dtTodayDateTime = strtotime(date('Y-m-d')); 
            if($dtTodayDateTime>$dtRemindDate)
            {
                $this->addError($szDateKey,'Remind date must be greater then today.');
                return false;
            }

            if(!empty($data['dtTaskTime']))
            {
                $szReminderTimeAry = explode(":",$data['dtTaskTime']);
                if($szReminderTimeAry[0]>23)
                {
                    $this->addError($szTimeKey,'Invalid hour format.');
                    return false;
                }
                else if($szReminderTimeAry[1]>=60)
                {
                    $this->addError($szTimeKey,'Invalid minute format.');
                    return false;
                }
            }

            $dtReminderDate = $data['dtTaskDate']." ".$data['dtTaskTime'].":00"; 
            if(empty($_SESSION['szCustomerTimeZoneGlobal']))
            {
                $_SESSION['szCustomerTimeZoneGlobal'] = 'Europe/Copenhagen' ;
            }
            $szServerTimeZone = date_default_timezone_get(); 
            if(empty($szServerTimeZone))
            {
                $szServerTimeZone = 'Europe/London';
            }
            $date = new DateTime($dtReminderDate,new DateTimeZone($_SESSION['szCustomerTimeZoneGlobal']));   
            $dtReminderDate = $date->format('Y-m-d H:i:s');

            $date->setTimezone(new DateTimeZone($szServerTimeZone));  
            $dtReminderDate = $date->format('Y-m-d H:i:s');  

            $kBooking = new cBooking();
            $dtCurrentDate = $kBooking->getRealNow();
            //$dtCurrentDate = date('Y-m-d H:i:s'); 
            $date_1 = new DateTime($dtCurrentDate,new DateTimeZone($_SESSION['szCustomerTimeZoneGlobal']));   
            $date_1->setTimezone(new DateTimeZone($szServerTimeZone));
            $dtCurrentDateTime = $date_1->format('Y-m-d H:i:s');
            

            $dtRemindDateTime = strtotime($dtReminderDate) ; 
            $dtTodayDateTime = strtotime($dtCurrentDateTime); 
            if($dtTodayDateTime>$dtRemindDateTime)
            {
                $this->addError($szTimeKey,'Remind date must be greater then today.');
                return false;
            }
            if(date('I')==1)
            {  
                //Our server's My sql time is 1 hours behind so that's why added 2 hours to sent date 
                //$dtReminderDate = date('Y-m-d H:i:s',strtotime("-1 HOUR",strtotime($dtReminderDate)));  
            }  
            
            $addTransportecaTask = array();
            $addTransportecaTask = $data;
            $addTransportecaTask['dtRemindDate'] = $dtReminderDate;
            $addTransportecaTask['iOverviewTask'] = 1; 
            
            $kBooking = new cBooking();
            $kBooking->addAddReminderNotes($addTransportecaTask);
            $this->idBookingFileReminderNotes = $kBooking->idBookingFileReminderNotes;
            
            $idBookingFile = $data['idBookingFile'];
             
            $dtResponseTime = $kBooking->getRealNow();
            $dtSolveTaskCreated = getDateCustom($dtResponseTime);
            
            /*
            if(date('I')==1)
            {
                $dtSolveTaskCreated = date('Y-m-d H:i:s',strtotime("-2 HOUR",strtotime($dtReminderDate)));
            }
            else
            {
                $dtSolveTaskCreated = date('Y-m-d H:i:s',strtotime("-1 HOUR",strtotime($dtReminderDate)));
            }
             * 
             */
                
            $fileLogsAry = array();   
            $fileLogsAry['dtSolveTaskCreated'] = $dtSolveTaskCreated;
            $fileLogsAry['iAddSolveTask'] = 1;
            $fileLogsAry['idFileOwner'] = $data['idTaskOwner'];  
            $fileLogsAry['idReminderTask'] = $this->idBookingFileReminderNotes;
            
            if(!empty($fileLogsAry))
            {
                $file_log_query = "";
                foreach($fileLogsAry as $key=>$value)
                {
                    $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            } 
            $file_log_query = rtrim($file_log_query,",");   
            $kBooking_new = new cBooking();
            $kBooking_new->updateBookingFileDetails($file_log_query,$idBookingFile,true); 
             
            return true; 
        }
    }
    function updateReminderTask($data)
    {
        if(!empty($data))
        {
            $idAdminUpdateBy = $_SESSION['admin_id'] ;
            $idBookingFile = $data['idBookingFile'];
            $idReminderTask = $data['idReminderTask'];
            $szOperationType = $data['szOperationType'];
            
            $kBooking = new cBooking();
            $dtCurrentDateTime = $kBooking->getRealNow();
            $dtCurrentMysqlDateTime = getDateCustom($dtCurrentDateTime);
            /*
            if(date('I'))
            {
                $dtCurrentMysqlDateTime = date('Y-m-d H:i:s',strtotime("+2 HOUR",strtotime($dtCurrentDateTime)));
            }
            else
            {
                $dtCurrentMysqlDateTime = date('Y-m-d H:i:s',strtotime("+1 HOUR",strtotime($dtCurrentDateTime)));
            }
             * 
             */
                
            $kBooking->loadFile($idBookingFile);
            $szPreviousTask=$kBooking->szPreviousTask;
            $iLabelStatus=$kBooking->iLabelStatus;
            $szCurrentTask=$kBooking->szTaskStatus;
            $szFileStatus=$kBooking->szFileStatus;
            
            if($data['szOperationType']=='CLOSE')
            {
                $update_str =  " 
                    iClosed = '1', 
                    dtClosedOn = '".mysql_escape_custom($dtCurrentMysqlDateTime)."',
                    idAdminClosedBy = '".$idAdminUpdateBy."'
                ";
            }
            else
            {
                $update_str =  " 
                    iClosed = '0', 
                    dtClosedOn = '".mysql_escape_custom($dtCurrentMysqlDateTime)."',
                    idAdminClosedBy = '".$idAdminUpdateBy."'
                ";
            } 
            
            $query="	
                UPDATE
                    ".__DBC_SCHEMATA_BOOKING_FILE_EMAIL_LOGS__."
                SET
                    ".$update_str." 
                WHERE
                    id = '".(int)$idReminderTask."';
            ";
        //echo $query ;
//            die;
            if($result=$this->exeSQL($query))
            {
                $kBooking = new cBooking();
                $fileLogsAry = array();  
                //$dtResponseTime = $kBooking->getRealNow();
                $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtCurrentDateTime;  
                
                if($data['szOperationType']=='CLOSE')
                {
                    if($szFileStatus=='S160' && $szPreviousTask=='T140')
                    {
                        $szPreviousTask='';
                    }
                    if($szPreviousTask=='T140' && $iLabelStatus>1)
                    {
                        $szPreviousTask='';
                    }
                    $fileLogsAry['szTransportecaTask'] = $szPreviousTask;
                    $fileLogsAry['szPreviousTask'] = '';
                    $fileLogsAry['iAddSolveTask']=0;
                }
                else
                {
                    $fileLogsAry['szPreviousTask'] = $szCurrentTask;  
                    $fileLogsAry['szTransportecaTask'] = "T220";  
                    $fileLogsAry['iClosed'] = 0;
                    
                    $fileQueueArr=array();
                    $fileQueueArr['szTaskStatus']=$szCurrentTask;
                    $fileQueueArr['idBookingFile']=$idBookingFile;
                    $fileQueueArr['szDeveloperNotes']="Updating Solve Task From File Log/Customer Log Tab  in Pending Tray";
                    $kPendingTray = new cPendingTray();
                    $kPendingTray->addFileTaskQueue($fileQueueArr);
                }
                
                if(!empty($fileLogsAry))
                {
                    $file_log_query = '';
                    foreach($fileLogsAry as $key=>$value)
                    {
                        $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                    $file_log_query = rtrim($file_log_query,",");      
                    $kBooking->updateBookingFileDetails($file_log_query,$idBookingFile); 
                }  
                
                if($idReminderTask>0)
                {
                    $reminderLogAry = array();
                    if($szOperationType=='CLOSE')
                    {
                        $reminderLogAry['iClosed'] = 1;
                        $reminderLogAry['dtClosedOn'] = $dtCurrentMysqlDateTime;
                        $reminderLogAry['idAdminClosedBy'] = $idAdminUpdateBy; 
                    }
                    else
                    {
                        $reminderLogAry['iClosed'] = 0;
                        $reminderLogAry['dtClosedOn'] = $dtCurrentMysqlDateTime;
                        $reminderLogAry['idAdminClosedBy'] = $idAdminUpdateBy;   
                    }
                    
                    /*
                     * Updating seen flag in customer log snippet
                    */ 
                    $kCustomerLogs = new cCustomerLogs();
                    $kCustomerLogs->updateCustomerLogSnippet($reminderLogAry,$idReminderTask,__CUSTOMER_LOG_SNIPPET_TYPE_OVERVIEW_TASK__);
                }
                return true;
            }
            else
            { 
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    
    function markCrmMailSeen($idCrmEmailLogs,$idBookingFile)
    {
        if($idCrmEmailLogs>0 && $idBookingFile>0)
        {
            $kSendEmail = new cSendEmail();
            $logEmailsArys = array();
            $logEmailsArys = $kSendEmail->getEmailLogDetails($idCrmEmailLogs);
             
            if($logEmailsArys['iSeen']==1)
            {
                //If email is already marked as read then do nothing
                return false;
            }
            
            $query="
            	UPDATE
                    ".__DBC_SCHEMATA_EMAIL_LOG__."
            	SET
                    iSeen = '1'
                WHERE
                    id = '".(int)$idCrmEmailLogs."'
            "; 
//            echo $query;
//            die;
            if($result = $this->exeSQL($query))
            {
                /*
                * Updating seen flag in customer log snippet
                */
                $emailAry = array();
                $emailAry['iSeen'] = 1;
                $kCustomerLogs = new cCustomerLogs();
                $kCustomerLogs->updateCustomerLogSnippet($emailAry,$idCrmEmailLogs,__CUSTOMER_LOG_SNIPPET_TYPE_EMAIL__);
                
                $this->updateReadMesageTask($idBookingFile);
                return true;	
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    
    function updateReadMesageTask($idBookingFile,$update_crm_mail=false)
    {
        if($idBookingFile>0)
        {
            $kBooking = new cBooking();
            $kBookingNew = new cBooking();
            
            $kBooking->loadFile($idBookingFile);
            $iNumUnreadMessage = $kBooking->iNumUnreadMessage;
            $idBooking = $kBooking->idBooking;
            
            $fileLogsAry = array();
            if($update_crm_mail)
            {
                $idEmailLogs = $this->getUnreadEmailsByBookingID($idBooking); 
                $fileLogsAry['idEmailLogs'] = $idEmailLogs;
            } 
            else 
            {
                $fileLogsAry['iNumUnreadMessage'] = $iNumUnreadMessage - 1;   
            } 
            if(!empty($fileLogsAry))
            {
                $file_log_query = '';
                foreach($fileLogsAry as $key=>$value)
                {
                    $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                } 
                $file_log_query = rtrim($file_log_query,","); 
                if($kBookingNew->updateBookingFileDetails($file_log_query,$idBookingFile))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
    
    function getUnreadEmailsByBookingID($idBooking,$get_count=false)
    { 
        if($idBooking>0)
        {	  
            if($get_count)
            {
                $query_select = " count(id) iNumCount ";
            }
            else
            {
                $query_select = " id ";
                $query_limit = " LIMIT 0,1";
            }
            
            $query="
                SELECT
                    $query_select 
                FROM
                    ".__DBC_SCHEMATA_EMAIL_LOG__." b 
                WHERE  
                    idBooking = '".(int)$idBooking."'  
                AND
                   iSeen = '0' 
                AND
                    iIncomingEmail = '1'
                ORDER BY 
                    dtSent DESC 
                $query_limit
            "; 
            //echo $query;
            if($result = $this->exeSQL($query))
            {
                if($this->getRowCnt()>0)
                { 
                    $row = array();
                    $row = $this->getAssoc($result); 
                    if($get_count)
                    {
                        return $row['iNumCount']; 
                    }
                    else
                    {
                        return $row['id']; 
                    } 
                }
                else
                {
                    return array() ;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return array();
            }
        }
        else
        {
            return false;
        } 
    }
    
    function updateDraftEmailData($szReminderSubject,$szReminderEmailBody,$idCrm)
    {
        $iAlreadyUtf8Encoded=0; 
        if(mb_detect_encoding($szReminderEmailBody) == "UTF-8")
        {
           $iAlreadyUtf8Encoded=1;
           $szEmailBody = ($szReminderEmailBody);
        }          
        else
        {
            $szEmailBody = utf8_encode($szReminderEmailBody);
        }
            
        $query="
            UPDATE
                ".__DBC_SCHEMATA_EMAIL_LOG__."
            SET
                szDraftEmailBody='".  mysql_escape_custom($szEmailBody)."',
                szDraftEmailSubject='".  mysql_escape_custom(utf8_encode($szReminderSubject))."',
                iAlreadyUtf8Encoded='".(int)$iAlreadyUtf8Encoded."'     
            WHERE
                id='".(int)$idCrm."'
        ";
        //echo $query."<br />";
        $result = $this->exeSQL($query);
    }
    
    function updateDraftEmailRemindData($data,$idTemplate,$idBookingOverView)
    {
        $szCustomerEmail=  sanitize_all_html_input(trim($data['szCustomerEmail']));
        $szCustomerCCEmail=  sanitize_all_html_input(trim($data['szCustomerCCEmail']));
        $szCustomerBCCEmail=  sanitize_all_html_input(trim($data['szCustomerBCCEmail']));

        $szReminderSubject=  sanitize_all_html_input(trim($data['szReminderSubject']));
        $szReminderEmailBody=  htmlspecialchars_decode(urldecode(base64_decode(trim($data['szReminderEmailBody']))));
        
        
        $query="
            DELETE FROM
                ".__DBC_SCHEMATA_BOOKING_REMIND_EMAIL_DRAFT__."
            WHERE
                idBooking='".(int)$idBookingOverView."'
        ";
        $result = $this->exeSQL($query);
        
        $query="
            INSERT INTO
                ".__DBC_SCHEMATA_BOOKING_REMIND_EMAIL_DRAFT__."
            (
                idBooking,
                idRemindEmailTemplate,
                szRemindDraftSubject,
                szRemindDraftEmailBody,
                szCustomerEmail,
                szCustomerCCEmail,
                szCustomerBCCEmail
            )
                VALUES
            (
                '".(int)$idBookingOverView."',
                '".(int)$idTemplate."',                    
                '".  mysql_escape_custom(utf8_encode($szReminderSubject))."',
                '".  mysql_escape_custom(utf8_encode($szReminderEmailBody))."',
                '".  mysql_escape_custom($szCustomerEmail)."',
                '".  mysql_escape_custom($szCustomerCCEmail)."',
                '".  mysql_escape_custom($szCustomerBCCEmail)."'    
            )
            ";
            $result = $this->exeSQL($query);    

    }
    
    function getDraftEmailRemindData($idBooking)
    {
        $query="
            SELECT
                idBooking,
                idRemindEmailTemplate,
                szRemindDraftSubject,
                szRemindDraftEmailBody,
                szCustomerEmail,
                szCustomerCCEmail,
                szCustomerBCCEmail
            FROM
                ".__DBC_SCHEMATA_BOOKING_REMIND_EMAIL_DRAFT__."
            WHERE
                idBooking='".(int)$idBooking."'               
            ";
        //echo $query;
            if($result = $this->exeSQL($query))
            {
                if($this->getRowCnt()>0)
                {
                    $row = $this->getAssoc($result);
                    return $row;
                }
            }
    }
    
    function deleteDraftEmailForRemind($idBookingFile)
    {
        $query="
            SELECT 
                f.id,
                f.idBooking
            FROM
                ".__DBC_SCHEMATA_FILE_LOGS__." AS f 
            WHERE
               f.id = '".(int)$idBookingFile."'
        ";
        //echo $query;
        if($result = $this->exeSQL($query))
        {
            if($this->iNumRows>0)
            { 
                $row=$this->getAssoc($result); 
                $idBooking=$row['idBooking'];
            }
        }
        
        
        $query="
            DELETE FROM
                ".__DBC_SCHEMATA_BOOKING_REMIND_EMAIL_DRAFT__."
            WHERE
                idBooking='".(int)$idBooking."'
        ";
        //echo $query;
        $result = $this->exeSQL($query);
    }
    
    function updateReadMessageTaskToRead($idCrmEmailLogs)
    {
        $kBooking = new cBooking();
        $dtCurrentTime = $kBooking->getRealNow();
        
        $BookingFileName = __APP_PATH_ROOT__."/logs/readMessageTiming.log";
        $strdata=array();
        $strdata[0]=" \n\n *** Start Date  ".date("Y-m-d H:i:s")."\n\n"; 
        $strdata[1]="idCrmEmailLogs: ".$idCrmEmailLogs."\n" ;
        file_log(true, $strdata, $BookingFileName);
       
        $kAdmin= new cAdmin();
        $emailLogArr=$kAdmin->getEmailLogDetailById($idCrmEmailLogs);
        $strdata=array();
        $strdata[0]=" \n\n *** Email Data*****\n\n"; 
        $strdata[1]=print_r($emailLogArr,true)."\n" ;
        
        
        $dtSendTime=$emailLogArr[0]['dtSent'];
        $strdata[2]="dtSendTime: ".$dtSendTime."\n" ;
        $iDSTDifference=$emailLogArr[0]['iDSTDifference'];
        $strdata[3]="iDSTDifference: ".$iDSTDifference."\n" ;
        if((int)$iDSTDifference>0){
        $dtSend = date('Y-m-d H:i:s',strtotime("-".$iDSTDifference." HOUR",strtotime($dtSendTime)));
        }else
        {
            $dtSend =$dtSendTime;
        }
        $strdata[4]="dtSend: ".$dtSend."\n" ;
        $iTotalClearTime= get_working_hours_for_graph($dtSend,$dtCurrentTime);
        $strdata[5]="iTotalClearTime: ".$iTotalClearTime."\n" ;
        file_log(true, $strdata, $BookingFileName);
        if((float)$iTotalClearTime<=0)
        {
            $iTotalClearTime=0;
        }
        $query="
            UPDATE                
                ".__DBC_SCHEMATA_EMAIL_LOG__."
            SET
                dtReadMessageTaskToRead='".mysql_escape_custom($dtCurrentTime)."',
                idAdminAddedBy='".(int)$_SESSION['admin_id']."',
                iTotalClearTime='".(float)$iTotalClearTime."'    
            WHERE
                id='".(int)$idCrmEmailLogs."'
            AND
                iEmailNumber>0
            AND
                dtReadMessageTaskToRead='0000-00-00 00:00:00'
        ";
        //echo $query;
        $result = $this->exeSQL($query);
        
        $query="
            UPDATE                
                ".__DBC_SCHEMATA_CUSTOMER_LOG_SNIPPET__."
            SET
                dtReadMessageTaskToRead='".mysql_escape_custom($dtCurrentTime)."',
                idAdminAddedBy='".(int)$_SESSION['admin_id']."',
                iTotalClearTime='".(float)$iTotalClearTime."'    
            WHERE
                idPrimary='".(int)$idCrmEmailLogs."'
            AND
                idCustomerLogDataType=1
            AND
                dtReadMessageTaskToRead='0000-00-00 00:00:00'
        ";
        //echo $query;
        $result = $this->exeSQL($query);
                
    }
    
    function set_dtTaskDate($value,$flag=true)
    {
        $this->dtTaskDate = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "dtTaskDate", t($this->t_base.'fields/reminder_date'), false, false, $flag );
    }
    function set_dtTaskTime($value,$flag=true)
    {
        $this->dtTaskTime = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "dtTaskTime", t($this->t_base.'fields/reminder_true'), false, false, $flag );
    }
    function set_szReminderNotes($value,$flag=true)
    {
        $this->szReminderNotes = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szReminderNotes_New_Task_popup", t($this->t_base.'fields/reminder_true'), false, false, $flag );
    } 
    function set_idCrmEmailLogs($value,$flag=true)
    {
        $this->idCrmEmailLogs = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idCrmEmailLogs", t($this->t_base.'fields/reminder_date'), false, false, $flag );
    }
    function set_dtReminderDate($value,$flag=true)
    {
        $this->dtReminderDate = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "dtCrmReminderDate", t($this->t_base.'fields/reminder_date'), false, false, $flag );
    }
    function set_szReminderTime($value,$flag=true)
    {
        $this->szReminderTime = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCrmReminderTime", t($this->t_base.'fields/reminder_true'), false, false, $flag );
    } 
    
    function set_szFirstName( $value,$flag=true )
    {
        $this->szFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szFirstName", t($this->t_base.'fields/first_name'), false, false, $flag );
    }
    function set_szLastName( $value,$flag=true )
    {
        $this->szLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLastName", t($this->t_base.'fields/last_name'), false, false, $flag );
    } 
    function set_idCustomerDialCode( $value,$flag=true )
    {
        $this->idCustomerDialCode = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idCustomerDialCode", t($this->t_base.'fields/dial_code'), false, false, $flag );
    }
    function set_szCustomerPhoneNumber( $value,$flag=true )
    {
        $this->szCustomerPhoneNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCustomerPhoneNumber", t($this->t_base.'fields/phone'), false, false, $flag );
    }
    function set_szCustomerCompanyName( $value,$flag=true )
    {
        $this->szCustomerCompanyName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCustomerCompanyName", t($this->t_base.'fields/company_name'), false, false, $flag );
    } 
    function set_idCustomerOwner( $value,$flag=true )
    {
        $this->idCustomerOwner = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idCustomerOwner", t($this->t_base.'fields/file_owner'), false, false, false );
    } 
    function set_idCustomerCurrency( $value,$flag=true )
    {
        $this->idCustomerCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idCustomerCurrency", t($this->t_base.'fields/customer_currency'), false, false, $flag );
    }  
    function set_iBookingLanguage( $value,$flag=true )
    {
        $this->iBookingLanguage = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iBookingLanguage", t($this->t_base.'fields/booking_language'), false, false, $flag );
    }
    function set_szCustomerAddress1( $value,$flag=true )
    {
        $this->szCustomerAddress1 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCustomerAddress1", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/p_number'), false, false, false );
    } 
    function set_szCustomerPostCode( $value,$flag=true )
    {
        $this->szCustomerPostCode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCustomerPostCode", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/p_number'), false, false, $flag );
    }
    function set_szCustomerCity( $value,$flag=true )
    {
        $this->szCustomerCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCustomerCity", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/p_number'), false, false, $flag );
    }
    function set_szCustomerCountry( $value,$flag=true )
    {
        $this->szCustomerCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCustomerCountry", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/p_number'), false, false, $flag );
    } 
    function set_szEmail( $value,$flag=true )
    {
        $this->szEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szEmail", t($this->t_base.'fields/email'), false, false, $flag );
    } 
}
?>