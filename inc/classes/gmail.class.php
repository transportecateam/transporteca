<?php
/**
 * This file is the container for gmail api related functionality. 
 *
 * gmail.class.php
 *
 * @copyright Copyright (C) 2015 Transporteca
 * @author Ajay
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" ); 
require_once( __APP_PATH__ . "/inc/functions.php" ); 

class cGmail extends cDatabase
{   
    public static $szHostName = __GMAIL_HOST_NAME__; 
    public static $szUserName = __GMAIL_USER_NAME__; 
    public static $szPassword = __GMAIL_PASSWORD__; 
    public $iManuallyUpdating;
    
    function __construct()
    {
        parent::__construct();
        return true;
    } 
    
    function refreshGmailInboxData()
    {  
        $dtLastRunDate = $this->getLastEmailReceivedDate();   
        if(!empty($dtLastRunDate) && $dtLastRunDate!='0000-00-00 00:00:00')
        {
            $dtLastRunDate = date("Y-m-d H:i",strtotime("-1 hours",strtotime($dtLastRunDate))); 
        }  
        else
        {
            $dtLastRunDate = date("Y-m-d H:i",strtotime("-1 days")); 
        } 
        if(!empty($dtLastRunDate))
        {
            $dtLastRunDate = date('d F Y H:i',strtotime($dtLastRunDate));  
            $this->fetchInboxDataFromGmail($dtLastRunDate);
        } 
    }
    
    function getLastEmailReceivedDate()
    {
        $query="
            SELECT 
                dtReceivedOn 
            FROM
                ".__DBC_SCHEMATA_GMAIL_INBOX_LOGS__."	  
            ORDER BY 
                dtReceivedOn DESC
            LIMIT 0,1
        ";
        //echo "<br><br>".$query ;
        if($result = $this->exeSQL($query))
        { 
            $retAry = array(); 
            $row=$this->getAssoc($result); 
            return $row['dtReceivedOn'];
        }
    }
    
    function fetchInboxDataFromGmail($dtLastRunDate)
    {  
        if(!empty($dtLastRunDate))
        {
            try 
            {   
                $kImap = new cImap();
                /* grab emails */
                $emailDataAry = array();
                $emailDataAry = imap_search($kImap->mailbox,'SINCE "'.$dtLastRunDate.'"');
            
                $iTotalMessageCount = imap_num_msg($kImap->mailbox);
            
                $iMaxEmailNumber = $this->getMaxMinGmailId('DESC');
            
                $missingEmailNumberAry = array();
                $iEmailCounter = 1;
                $iLastUpdatedEmailNumber = false;
                
                /*
                * When we successfully connected to Gmail then we remove warning message from header
                */
                $this->deleteGmailSynByTags('SYNC_FAILED');
                
                /* if emails are returned, loop through each... */
                if(!empty($emailDataAry))
                {
                    sort($emailDataAry); 
                    $iMaxEmailNumberAtGmail = max($emailDataAry);  
                    
                    if($iMaxEmailNumber<=0)
                    {
                        $iMaxEmailNumber = min($emailDataAry);  
                    } 
                    $tempEmailIdAry = array();
                    $tempEmailIdAry = $emailDataAry;
                                        
                    $ctr = 0;
                    $emailNumberAry = array();
                    foreach($tempEmailIdAry as $tempEmailIdArys)
                    {
                        /*
                        * Removing all the email numbers which was preassumingly duplicate. 
                        * I mean suppose we have max(iEmailNumber) in tblgmailinboxlogs is 100 then we are assuming all the emails less then 100(1-99) is already processed correctly and should be there in our data base.
                        * So we have started processing loop from iEmailNumber: 101
                        */
                        if($tempEmailIdArys>$iMaxEmailNumber)
                        {
                            $emailNumberAry[$ctr] = $tempEmailIdArys;
                            $ctr++;
                        } 
                    }
                                        
                    $Counter = 0;
                    $loop_counter = $iMaxEmailNumberAtGmail - $iMaxEmailNumber; 
                    $emailDataAry = $emailNumberAry;
                    
                    /* @Ajay
                    * Suppose we recieve these email numbers $emailIdAry = (532,533,535,536,538)
                    * suppose $iMaxEmailNumber = 531;
                    * $loop_counter = max($emailIdAry) - $iMaxEmailNumber = 538 - 531 = 7
                    * 
                    * So we have lopped from 531 to 538 and processed (532,533,535,536,538) normally and processed 534,537 as missing email number
                    */
                    
                    /*
                     * If because of any reason in last run any email number has not been processed correctly then we store the email_number in this table and it will be processed manually time developer. 
                     */
                    $alreadyProcessedEmailWithErrorAry = array();
                    $alreadyProcessedEmailWithErrorAry = $this->getAllTrackedEmails();
                    
                    for($iMaxEmailNumber;$loop_counter>=$Counter;$Counter++)
                    {
                        $email_number = $emailDataAry[$Counter]; 
                        if(!empty($alreadyProcessedEmailWithErrorAry) && in_array($email_number,$alreadyProcessedEmailWithErrorAry))
                        {
                            continue;
                        } 
                        $this->addProcessTracker($email_number);
                        
                        $iExpactedNumber = $iMaxEmailNumber + $iEmailCounter; 
                        $iEmailCounter++;
                        if(!in_array($iExpactedNumber,$emailDataAry))
                        { 
                            if($iMaxEmailNumberAtGmail>=$iExpactedNumber)
                            { 
                                $missingEmailNumberAry[] = $iExpactedNumber;
                            }
                        }
                        
                        if($email_number>0)
                        {
                            if($this->isEmailAlreadyAdded($email_number))
                            {
                                /*
                                * If the email number is already added into the system we just continue to next record
                                */
                                //@TO DO;
                                $this->deleteTrackedEmail($email_number);
                            }  
                            else
                            {
                                $this->processEmails($kImap,$email_number);  
                                $this->deleteTrackedEmail($email_number);
                            } 
                            $iLastUpdatedEmailNumber = $email_number;
                        } 
                    }
    
                    /* for every email...  
                    foreach($emailDataAry as $email_number)
                    {   
                        $iExpactedNumber = $iMinEmailNumber + $iEmailCounter;
                        $iEmailCounter++;
                        if($iMinEmailNumber<$email_number && $email_number!=$iExpactedNumber)
                        {
                            $missingEmailNumberAry[] = $iExpactedNumber;
                        } 
                        if($this->isEmailAlreadyAdded($email_number))
                        { 
                        }  
                        else
                        {
                            $this->processEmails($kImap,$email_number);  
                        } 
                        $iLastUpdatedEmailNumber = $email_number;
                    }
                     * 
                     */
                } 
                /*
                * Recovering Missing Emails
                */
                if(!empty($missingEmailNumberAry))
                {
                    $this->recoverMissedEmails($missingEmailNumberAry);
                } 
                
                /*
                 * Fetching count of CRM emails at Transporteca
                 */
                $iTotalCrmEmailCount = $this->getMaxMinGmailId(false,false,true);
                
                /*
                * If total number of email at Gmail Inbox doesn't match with our emails logs then we add a warning line at admin header
                */
                if($iTotalMessageCount>$iTotalCrmEmailCount)
                {
                    $gmailSyncAry = array();
                    $gmailSyncAry['szErrorType'] = 'TOTAL_COUNT';
                    $gmailSyncAry['szErrorMessage'] = 'Total number of crm emails at Transporteca does not match with Gmail inbox';
                    $gmailSyncAry['szRemark'] = 'Total email at Transporteca: '.$iTotalCrmEmailCount." Total at Gmail: ".$iTotalMessageCount;
                    $this->addGmailSynchronizationLogs($gmailSyncAry);  
                }
                else
                {
                    $this->deleteGmailSynByTags('TOTAL_COUNT');
                }
            } 
            catch (Exception $ex) 
            {
                if(date('I')==1)
                {
                    $dtLastUpdatedDateTime = date('d F H:i',  strtotime("+1 hours"));
                }
                else
                {
                    $dtLastUpdatedDateTime = date('d F H:i');
                } 
                $gmailSyncAry = array();
                $gmailSyncAry['szErrorType'] = 'SYNC_FAILED';
                $gmailSyncAry['szErrorMessage'] = 'Gmail synchronization failed, last update '.$dtLastUpdatedDateTime;
                $gmailSyncAry['szRemark'] = 'Exception: '.$ex->getMessage();
                $this->addGmailSynchronizationLogs($gmailSyncAry);
            }
        }
    }
    
    function getAllTrackedEmails()
    {
        $query="
            SELECT 
                iEmailNumber 
            FROM
                ".__DBC_SCHEMATA_GMAIL_PROCESS_TRACKER__."	 
        ";
        //echo "<br><br>".$query ;
        if($result = $this->exeSQL($query))
        { 
            $retAry = array(); 
            while($row = $this->getAssoc($result))
            {
                $retAry[] = $row['iEmailNumber'];
            }
            return $retAry; 
        }
    } 
    function deleteTrackedEmail($iEmailNumber)
    {
        if($iEmailNumber>0)
        {
            $query="	
                DELETE FROM
                    ".__DBC_SCHEMATA_GMAIL_PROCESS_TRACKER__."
                WHERE
                    iEmailNumber = '".  mysql_escape_custom($iEmailNumber)."' 
            "; 
            //echo $query;
            if($result=$this->exeSQL($query))
            { 
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    
    function addProcessTracker($iEmailNumber)
    {
        if($iEmailNumber>0)
        {
            $query="	
                INSERT INTO
                    ".__DBC_SCHEMATA_GMAIL_PROCESS_TRACKER__."
                (
                    iEmailNumber, 
                    dtCreatedOn
                )
                VALUES
                ( 
                    '".mysql_escape_custom(trim($iEmailNumber))."', 
                    now()
                )  
            "; 
            if($result=$this->exeSQL($query))
            { 
                return true;
            }
            else
            {
                return false;
            }
        } 
    }
    function processEmails($kImap,$email_number,$szDeveloperRemarks,$bReturnAry=false)
    {
        $emailLogsAry = array();
        $emailLogsAry = $kImap->getMessage($email_number);
            
        /* Building data to for adding logs */
        if(!empty($emailLogsAry))
        {
            $kImap->bodyHTML = '';
            $kImap->bodyPlain = '';
            $kImap->iInlineAttachments = false;
            
            $attachmentAry = array();
            $attachmentAry = $kImap->getAttachments($email_number);
            
            $szEmailBody = $this->strip_html_tags($kImap->bodyHTML);
            
            //$szEmailBody = $this->custom_strip_tags($szEmailBody);
           
            if(!empty($szEmailBody))
            {
                if($kImap->iAddPlainTextBody && !empty($kImap->bodyPlain))
                {
                    $szEmailBody = nl2br($kImap->bodyPlain)."<br>".$szEmailBody;
                }
                $szEmailBodyType = 'HTML';
                $emailLogsAry['body'] = $szEmailBody;  
                $emailLogsAry['charSetTypeValue'] = $kImap->charSetTypeValue;
            }
            else
            {
                /*
                * If there is no HTML in E-mail body then we use Imap plain body 
                */
                $szEmailBodyType = 'TEXT';
                
                if(!empty($kImap->bodyPlain))
                {
                    $emailLogsAry['body'] = $kImap->bodyPlain;
                }   
                $emailLogsAry['body'] = nl2br($emailLogsAry['body']); 
            } 

            $szAttachmentStr = '';
            $iHasAttachment = 0;
            if(!empty($attachmentAry))
            {
               $szAttachmentStr = serialize($attachmentAry);
               $iHasAttachment  = 1;
            }
            $szFromEmail = '';
            if(!empty($emailLogsAry['szFromUserName']))
            {
                $szFromEmail = $emailLogsAry['szFromUserName']." (".$emailLogsAry['fromEmail'].") ";
            }
            else
            {
                $szFromEmail = $emailLogsAry['fromEmail'];
            }
            
            $szToEmail = '';
            if(!empty($emailLogsAry['szToUserName']))
            {
                $szToEmail = $emailLogsAry['szToUserName']." (".$emailLogsAry['szToEmailAddress'].") ";
            }
            else
            {
                $szToEmail = $emailLogsAry['szToEmailAddress'];
            }
            $szCCEmailsAddress = '';
            if(!empty($emailLogsAry['cc']))
            {
                $szCCEmailsAddress = implode(", ",$emailLogsAry['cc']);
            }
            $emailBody = $emailLogsAry['body'];

            if(date('I')==1)
            {
                /*
                * If this flag is set then DST(Daylight Saving Time) would be 2 hours, I mean difference between Gmail date and Transporteca Mysql date is 2 hour
                */
                $iDSTDifference = 1;
            }
            else
            {
                /*
                * If this flag is set then DST(Daylight Saving Time) would be 1 hours, I mean difference between Gmail date and Transporteca Mysql date is 1 hour
                */
                $iDSTDifference = 1;
            }
            
            $iInvalidHtmlTags = 0;
            $szEmailMessage = sanitize_specific_tinymce_html_input(($emailLogsAry['body'])); 
            /*
            * If HTML string has any unclosed tags then this function (closetags()) will add closing tag for all unclosed HTML tags
            */
            $this->iInvalidHtmlTags = false;
            $szValidatedEmailBody = $this->closetags($szEmailMessage);
            if($this->iInvalidHtmlTags)
            {
                $iInvalidHtmlTags = 1;
                $szEmailMessage = $szValidatedEmailBody;
            }
            else
            {
                $szEmailMessage = sanitize_specific_tinymce_html_input(($emailLogsAry['body']));
            }
                        
            /*$szEmailMessage  = preg_replace_callback("#(<br>){3,}#", "\\1", $szEmailMessage );           
            $szEmailMessage = str_replace("</br>", "<br>", $szEmailMessage);
            $szEmailMessage  = preg_replace_callback("/(<br>\s*|<br \/>\s*){3,}/i", "<br><br>", $szEmailMessage );*/
            
        
            $emailLogDataAry = array();
            $emailLogDataAry['szSubject'] = $emailLogsAry['subject'];
            $emailLogDataAry['szEmailMessage'] = $szEmailMessage;
            $emailLogDataAry['iEmailNumber'] = $emailLogsAry['uid'];
            $emailLogDataAry['szEmailID'] = $emailLogsAry['message_id']; 
            $emailLogDataAry['dtReceivedOn'] = date('Y-m-d H:i:s',strtotime($emailLogsAry['date_sent'])); 
            $emailLogDataAry['szRawHeader'] = $emailLogsAry['raw_header']; 
            $emailLogDataAry['szAttachments'] = $szAttachmentStr;  
            $emailLogDataAry['szToEmail'] = $emailLogsAry['szToEmailAddress'];
            $emailLogDataAry['szToUserName'] = $emailLogsAry['szToUserName']; 
            $emailLogDataAry['szFromEmail'] = $szFromEmail;
            $emailLogDataAry['szFromEmailAddress'] = $emailLogsAry['fromEmail'];
            $emailLogDataAry['szFromUserName'] = $emailLogsAry['szFromUserName'];
            $emailLogDataAry['iHasAttachment'] = $iHasAttachment;  
            $emailLogDataAry['szCCEmailAddress'] = $szCCEmailsAddress;
            $emailLogDataAry['szBCCEmailAddress'] = $emailLogsAry['bcc']; 
            $emailLogDataAry['szEmailBodyType'] = $szEmailBodyType;
            $emailLogDataAry['szDeveloperRemarks'] = $szDeveloperRemarks;
            $emailLogDataAry['szCharSetType'] = $emailLogsAry['charSetTypeValue'];
            $emailLogDataAry['iDSTDifference'] = $iDSTDifference;
            $emailLogDataAry['iInvalidHtmlTags'] = $iInvalidHtmlTags;
            
           // echo "<br> Subject: ".$emailLogDataAry['szSubject']."<br>"; 
            if($bReturnAry)
            {
                return $emailLogDataAry;
            }
            else
            {
                /* Adding data to gmail logs */
                $this->logGmailInboxData($emailLogDataAry); 
            }
        }
    }
    
    function recoverMissedEmails($missingEmailNumberAry)
    {
        if(!empty($missingEmailNumberAry))
        {
            $kImap = new cImap(true,'[Gmail]/All Mail');
            foreach($missingEmailNumberAry as $email_number)
            {
                if($this->isEmailAlreadyAdded($email_number))
                {
                    /*
                    * If the email number is already added into the system we just continue to next record
                    */
                    //@TO DO
                }
                else
                {
                    $this->processEmails($kImap,$email_number,'MISSING_EMAIL_RECOVERY'); 
                }
            }
        }
    }
    
    function getMaxMinGmailId($szType='DESC',$dtDateTime=false,$count=false)
    {
        if(!empty($dtDateTime))
        {
            $query_where = " WHERE dtReceivedOn >= '".mysql_escape_custom(trim($dtDateTime))."' ";
        }
        
        if($count)
        {
            $query_select = " count(iEmailNumber) as iTotalEmailCount";
        }
        else
        {
            $query_select = " iEmailNumber ";
            $query_order_by = " 
                ORDER BY 
                    iEmailNumber $szType
                LIMIT 0,1 
            ";
        }
            
        $query="
            SELECT 
                $query_select 
            FROM
                ".__DBC_SCHEMATA_GMAIL_INBOX_LOGS__."	  
                $query_where 
                $query_order_by
        ";
        //echo "<br><br>".$query ;
        if($result = $this->exeSQL($query))
        {
            $retAry = array(); 
            $row=$this->getAssoc($result); 
            if($count)
            {
                return $row['iTotalEmailCount'];
            }
            else
            {
                return $row['iEmailNumber'];
            } 
        }
    }
    
    function addGmailSynchronizationLogs($gmailSyncAry)
    {
        if(!empty($gmailSyncAry))
        {
            /*
            * Before adding any record we are deleting previously added record to make sure we never have any duplicate entry for a szErrorType
            */
            //$this->deleteGmailSynByTags($gmailSyncAry['szErrorType']);
            if($this->isSyncErrorAlreadyExists($gmailSyncAry['szErrorType']))
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_GMAIL_SYNCHRONISATION_LOGS__."
                    SET
                        szErrorMessage = '".mysql_escape_custom(trim($gmailSyncAry['szErrorMessage']))."',
                        szRemarks = '".mysql_escape_custom(trim($gmailSyncAry['szRemark']))."',
                        dtCreatedOn = now()
                    WHERE
                        szErrorType = '".mysql_escape_custom(trim($gmailSyncAry['szErrorType']))."'
                "; 
            }
            else
            {
                $query="	
                    INSERT INTO
                        ".__DBC_SCHEMATA_GMAIL_SYNCHRONISATION_LOGS__."
                    (
                        szErrorType, 
                        szErrorMessage,
                        szRemarks,
                        dtCreatedOn
                    )
                    VALUES
                    ( 
                        '".mysql_escape_custom(trim($gmailSyncAry['szErrorType']))."',
                        '".mysql_escape_custom(trim($gmailSyncAry['szErrorMessage']))."',
                        '".mysql_escape_custom(trim($gmailSyncAry['szRemark']))."',  
                        now()
                    )  
                "; 
            } 
            if($result=$this->exeSQL($query))
            { 
                return true;
            }
            else
            {
                return false;
            }
        } 
    }
    
    function isSyncErrorAlreadyExists($szErrorType)
    {
        if(!empty($szErrorType))
        {
            $query="
                SELECT 
                    id 
                FROM
                    ".__DBC_SCHEMATA_GMAIL_SYNCHRONISATION_LOGS__."	
                WHERE
                    szErrorType = '".  mysql_escape_custom($szErrorType)."' 
            ";
            //echo "<br><br>".$query ;
            if($result = $this->exeSQL($query))
            { 
                $retAry = array(); 
                $row = $this->getAssoc($result); 
                if($row['id']>0)
                {
                    return true;
                }
                else
                {
                    return false;
                } 
            }
        } 
    }
    
    function deleteGmailSynByTags($szErrorType)
    {
        if(!empty($szErrorType))
        {
            $query="	
                DELETE FROM
                    ".__DBC_SCHEMATA_GMAIL_SYNCHRONISATION_LOGS__."
                WHERE
                    szErrorType = '".  mysql_escape_custom($szErrorType)."' 
            "; 
            //echo $query;
            if($result=$this->exeSQL($query))
            { 
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    
    function getAllGmailSyncMessages()
    {
        $query="
            SELECT 
                id,
                szErrorType,
                szErrorMessage,
                szRemarks
            FROM
                ".__DBC_SCHEMATA_GMAIL_SYNCHRONISATION_LOGS__."	 
            ORDER BY
               dtCreatedOn ASC 
        ";
        //echo "<br><br>".$query ;
        if($result = $this->exeSQL($query))
        {
            $retAry = array(); 
            while($row=$this->getAssoc($result))
            {
                $retAry[$ctr] = $row;
                $ctr++;
            }
            return $retAry;
        }
    }
    
    function strip_tags_content($text) 
    { 
        return preg_replace_callback('@<(\w+)\b.*?>.*?</\1>@si', '', $text); 
    }
    
    function strip_html_tags( $text )
    {
        $text = preg_replace_callback(
            array(
              // Remove invisible content
                '@<head[^>]*?>.*?</head>@siu',
                '@<style[^>]*?>.*?</style>@siu',
                '@<script[^>]*?.*?</script>@siu',
                '@<object[^>]*?.*?</object>@siu',
                '@<embed[^>]*?.*?</embed>@siu',
                '@<applet[^>]*?.*?</applet>@siu',
                '@<noframes[^>]*?.*?</noframes>@siu',
                '@<noscript[^>]*?.*?</noscript>@siu',
                '@<noembed[^>]*?.*?</noembed>@siu',
              // Add line breaks before and after blocks
                '@</?((address)|(blockquote)|(center)|(del))@iu',
                '@</?((div)|(h[1-9])|(ins)|(isindex)|(p)|(pre))@iu',
                '@</?((dir)|(dl)|(dt)|(dd)|(li)|(menu)|(ol)|(ul))@iu',
                '@</?((table)|(th)|(td)|(caption))@iu',
                '@</?((form)|(button)|(fieldset)|(legend)|(input))@iu',
                '@</?((label)|(select)|(optgroup)|(option)|(textarea))@iu',
                '@</?((frameset)|(frame)|(iframe))@iu',
            ),
            array(
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',"$0", "$0", "$0", "$0", "$0", "$0","$0", "$0",), $text );

        $html_tags_allowed = '<a><i><br /><br><br ><strong><div><b><i><u><li><ul><ol><p><span><h1><h2><h3><h4><h5><h6><table><tr><th><td><hr><tbody><font>';
        // you can exclude some html tags here, in this case B and A tags        
        return strip_tags( $text , $html_tags_allowed );
    }
    
    function custom_strip_tags($emailBody) 
    { 
        if(!empty($emailBody))
        {  
            $filename = __APP_PATH_ROOT__."/logs/gmail_api_breakups_".date('Ymd').".log";
            $iDebugFlag = true;
            if($iDebugFlag)
            {
                $strdata=array();
                $strdata[0]="\n\n Original Body \n\n".$emailBody; 
                file_log(true, $strdata, $filename);
            }
            
            $szReminderEmailBody = $emailBody;
            if(empty($szReminderEmailBody))
            {
                return false;
            }
            $emailBodyAry = explode(PHP_EOL,$szReminderEmailBody);
            
            if(!empty($emailBodyAry))
            {
                $ctr = 0; 
                foreach($emailBodyAry as $emailBodyArys)
                {
                    $szDebugString = "";
                    $html_tags_allowed = '<br><br ><br/><br />';
                    $emailBodyArys = strip_tags($emailBodyArys,$html_tags_allowed);  
                    $emailBodyArys = trim($emailBodyArys);
                    
                    if(!empty($emailBodyArys) && $emailBodyArys!='&nbsp;' && $emailBodyArys!='&nbsp;&nbsp;')
                    {
                        $szNewEmailBody .= $emailBodyArys.PHP_EOL;
                        $szDebugString = "<br>IT: ".$emailBodyArys; 
                        $ctr = 0;
                    }
                    else if($ctr>0)
                    {  
                        $szDebugString = " CTR:".$ctr;   
                    }
                    else 
                    {
                        $szNewEmailBody .= PHP_EOL; 
                        $szDebugString = "NEW_LINE";  
                        $ctr++;
                    }
                    if($iDebugFlag && !empty($szDebugString))
                    {
                        $strdata=array();
                        $strdata[0]=" \n ".$szDebugString; 
                        file_log(true, $strdata, $filename);
                    }
                }
            }
            $breaksAry = array("<br/>","<br>","<br />"); 
            $szNewEmailBody = str_ireplace($breaksAry, "<br>", $szNewEmailBody); 
            
            $szNewEmailBody = htmlspecialchars_decode($szNewEmailBody);
            
            if($iDebugFlag)
            {
                $strdata=array();
                $strdata[0]="\n\n Final body \n\n".$szNewEmailBody; 
                file_log(true, $strdata, $filename);
            }
            return trim($szNewEmailBody);
        }
    }
    
    function findBookingRef($szEmailString)
    {
        if(!empty($szEmailString))
        {
            $szEmailString = sanitize_all_html_input(trim($szEmailString));
            $emailWordAry = array();
            $emailWordAry = preg_split('/[\s]+/', $szEmailString, -1, PREG_SPLIT_NO_EMPTY); 
            
            if(!empty($emailWordAry))
            {
                /*
                * Iterate email subject/body in reverse order.
                */
                $emailWordAry = array_reverse($emailWordAry);
            } 
            $idBooking = 0;
            $kBooking = new cBooking(); 
            if(!empty($emailWordAry))
            {
                foreach ($emailWordAry as $emailWordArys)
                {
                    $emailWordArys = trim($emailWordArys);
                    if(strlen($emailWordArys)>=9 && strlen($emailWordArys)<=12)
                    { 
                        $szWordsInitials = substr($emailWordArys,0,4);
                        $szTransportecaInitial = substr($emailWordArys,4,1); 
                        if(is_numeric($szWordsInitials) && strtoupper($szTransportecaInitial)=='T')
                        {
                            $idBooking = $kBooking->getBookingIdByBookingRef($emailWordArys);
                            if($idBooking>0)
                            {
                                break;
                            }
                            else
                            {
                                $fileLogsAry = array();
                                $fileLogsAry = $kBooking->getBookingFileIdByFileRef($emailWordArys); 
                                if($fileLogsAry['idBooking']>0)
                                {
                                    $idBooking = $fileLogsAry['idBooking'];
                                    break;
                                }
                            }
                        }
                    } 
                }
            }
            return $idBooking;
        }
    }
    
    function getAllEmails($idGmailLogs)
    {
        if($idGmailLogs>0)
        {
            $query_and = " AND id = '".(int)$idGmailLogs."' ";
        }
        $query="
            SELECT
                id,
                idBooking, 
                iEmailNumber,
                szEmailID,
                szFromEmail,
                szSubject,
                szEmailMessage,
                dtReceivedOn,
                szRawHeader,
                idAdminAddedBy,
                szIpAddress, 
                szHttpReferrar,
                szBrowser, 
                szAttachments,
                iSeen,
                dtCreatedOn   
            FROM
                ".__DBC_SCHEMATA_GMAIL_INBOX_LOGS__."	 
            WHERE
                isDeleted = 0
                $query_and
            ORDER BY 
                dtReceivedOn DESC
            LIMIT
                0,100
        ";
        //echo "<br><br>".$query ;
        if($result = $this->exeSQL($query))
        { 
            $retAry = array();
            $ctr = 0;
            while($row=$this->getAssoc($result))
            {
                $retAry[$ctr]=$row;
                $ctr++;
            }
            return $retAry;
        }
    }
    function markMailSeen($idGmailLogs)
    {
        if($idGmailLogs>0)
        {
            $query="
            	UPDATE
                    ".__DBC_SCHEMATA_GMAIL_INBOX_LOGS__."
            	SET
                    iSeen = '1'
                WHERE
                    id='".(int)$idGmailLogs."'
            "; 
            if( $result = $this->exeSQL( $query ) )
            {
                return true;	
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    function isEmailAlreadyAdded($iEmailNumber)
    {
        if($iEmailNumber)
        {
            $query="
                SELECT 
                    id 
                FROM
                    ".__DBC_SCHEMATA_GMAIL_INBOX_LOGS__."	
                WHERE
                    iEmailNumber = '".(int)$iEmailNumber."'
                ORDER BY 
                    dtReceivedOn DESC
                LIMIT 0,1
            ";
            //echo "<br><br>".$query ;
            if($result = $this->exeSQL($query))
            { 
                $retAry = array(); 
                $row = $this->getAssoc($result); 
                if($row['id']>0)
                {
                    return true;
                }
                else
                {
                    return false;
                } 
            }
        } 
    }
    
    function processCrmEmails($emailLogDataAry)
    {
        if(!empty($emailLogDataAry))
        {
            
        }
    }
    function logGmailInboxData($emailLogDataAry)
    { 
        if(!empty($emailLogDataAry))
        {   
            if(!$this->isEmailAlreadyAdded($emailLogDataAry['iEmailNumber']))
            {
                $idAdmin = $_SESSION['admin_id'];  
                /*
                *  checking for booking Reference number is Subject line
                */
                $bookingRefAry = array();
                $idBooking = $this->findBookingRef($emailLogDataAry['szSubject']);  
                if($idBooking<=0)
                {
                    /*
                    *  checking for booking Reference number is email body
                    */  
                    $idBooking = $this->findBookingRef($emailLogDataAry['szEmailMessage']); 
                } 
                $kUser = new cUser();
                $kAdmin = new cAdmin();
                $kBooking= new cBooking();

                $kForwarderContact = new cForwarderContact();

                $idCustomer = 0;
                $idForwarderContact = 0;
                $idTransportecaAdmin = 0; 
                $iNoReferenceFound = 0;
                $szFromEmailAddress = $emailLogDataAry['szFromEmailAddress'];  
                if($idBooking<=0)
                {
                    /*
                    *  if we don't find any booking ID or File ID for email then we check for customer email
                    */ 
                    $iNoReferenceFound = 2;
                    if(!empty($szFromEmailAddress))
                    {
                        if($kUser->isUserEmailExist($szFromEmailAddress))
                        {
                            $idCustomer = $kUser->idIncompleteUser;
                            $iNoReferenceFound = 1;
                        }
                        if($idCustomer<=0)
                        {
                            if($kForwarderContact->isForwarderEmailExist($szFromEmailAddress))
                            {
                                $idForwarderContact = $kForwarderContact->idForwarderContact;
                            } 
                        } 
                        if($idForwarderContact<=0 && !empty($emailLogDataAry['szToEmail']))
                        {
                            if($kAdmin->isAdminEmailExist($emailLogDataAry['szToEmail'],false,true))
                            {
                                $idTransportecaAdmin = $kAdmin->idTransportecaAdmin;
                            }  
                        }
                    }
                }   
                $kBookingNew = new cBooking();
                if($idBooking>0)
                {
                    $postSearchAry = $kBookingNew->getBookingDetails($idBooking); 
                    $idBookingFile = $postSearchAry['idFile']; 
                    $idCustomer = $postSearchAry['idUser']; 
                } 

                /*
                 *  The “Read message” task is allocated to (in this order – stop when allocation is done):
                    i.	if there is a file reference -> file owner, if there is one
                    ii.	if there is a management reference -> to that user
                    iii.	if there is a customer reference -> customer owner
                    iv.	Otherwise to “Not allocated” – so everyone can see it 
                */

                if($idBookingFile>0)
                { 
                    $kBookingNew->loadFile($idBookingFile);
                    $idFileOwner = $kBookingNew->idFileOwner;
                    $iNumUnreadMessage = $kBookingNew->iNumUnreadMessage;
                    $szTransportecaTask = $kBookingNew->szTaskStatus;
                    $szFileStatus = $kBookingNew->szFileStatus;
                    if($szTransportecaTask=='')
                    {
                        if((int)$kBookingNew->iSnoozeAdded==1)
                        {
                            if($kBookingNew->szLastTaskStatus=='T140')
                            {
                                $szTransportecaTask=$kBookingNew->szLastTaskStatus;
                            }else
                            {
                                if($kBookingNew->szPreviousTask=='T140')
                                {
                                    $szTransportecaTask=$kBookingNew->szPreviousTask;
                                }
                            }
                        }                        
                        if((int)$kBookingNew->iLabelStatus>1 && $szTransportecaTask=='T140')
                        {
                            $szTransportecaTask='';
                        }
                    }                    
                }
                else if($idTransportecaAdmin>0)
                {
                    $idFileOwner = $idTransportecaAdmin;
                }
                else if($idCustomer>0)
                {
                    $userDetailsAry = $kUser->getUserDetails($idCustomer);
                    $idFileOwner = $kUser->idCustomerOwner; 
                } 
                if($idFileOwner>0)
                {
                    $kAdmin = new cAdmin();
                    $kAdmin->getAdminDetails($idFileOwner);
                    if($kAdmin->iActive==1)
                    {
                        $idFileOwner = $kAdmin->id;
                    }
                    else
                    {
                        /*
                        *  If file owner is Inactive then we update file as No-Allocated 
                        */
                        $idFileOwner = false;
                    }
                }
                
                /*
                * Creating 'Read Message' task in pending Tray
                */
                if($idBookingFile>0)
                {
                    $dtResponseTime = $kBookingNew->getRealNow(); 

                    /*
                    * Re-opening file if file is closed
                    */
                    
                    if($szFileStatus=='S130') //If file status is 'Closed' then then we update status from previously saved status
                    {
                        //$kBookingNew->reOpenTask($idBookingFile);
                    }
                    
                    if($szFileStatus=='S160' && $szTransportecaTask=='T140') //If file status is 'Closed' then then we update status from previously saved status
                    {
                        $szTransportecaTask='';
                    }

                    $fileLogsAry = array();
                    $fileLogsAry['idUser'] = $idCustomer; 
                    $fileLogsAry['szTransportecaTask'] = 'T210'; 
                    $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime;  
                    $fileLogsAry['idFileOwner'] = $idFileOwner;
                    $fileLogsAry['idCustomerOwner'] = $idFileOwner; 
                    $fileLogsAry['iClosed'] = 0;
                    if($szTransportecaTask=='T210')
                    {
                        $fileLogsAry['iNumUnreadMessage'] = $iNumUnreadMessage + 1;
                    }
                    else
                    {
                        $fileLogsAry['iNumUnreadMessage'] = 1;
                        $fileLogsAry['szPreviousTask'] = $szTransportecaTask;
                        
                        $fileQueueArr=array();
                        $fileQueueArr['szTaskStatus']=$szTransportecaTask;
                        $fileQueueArr['idBookingFile']=$idBookingFile;
                        $fileQueueArr['szDeveloperNotes']="Updating Read Message Task When Importing Data From Gmail";
                        $kPendingTray = new cPendingTray();
                        $kPendingTray->addFileTaskQueue($fileQueueArr);
                    } 
                    $fileLogsAry['iNoReferenceFound'] = $iNoReferenceFound;
                    if(!empty($fileLogsAry))
                    {
                        $file_log_query = '';
                        foreach($fileLogsAry as $key=>$value)
                        {
                            $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                        } 
                        $file_log_query = rtrim($file_log_query,","); 
                        $kBookingNew->updateBookingFileDetails($file_log_query,$idBookingFile); 
                    } 
                }
                else
                {  
                    $chatDetailsAry = array();
                    $chatDetailsAry['idCustomer'] = $idCustomer;
                    $chatDetailsAry['idFileOwner'] = $idFileOwner;
                    $chatDetailsAry['iEmailLogs'] = 1;
                    $chatDetailsAry['iNumUnreadMessage'] = 1;
                    $chatDetailsAry['iNoReferenceFound'] = $iNoReferenceFound;

                    $kBooking->addNewBookingFile(true,$chatDetailsAry);
                    $idBookingFile = $kBooking->idBookingFile;  
                } 
                $kBooking->loadFile($idBookingFile);
                $idBooking = $kBooking->idBooking; 

                $query="	
                    INSERT INTO
                        ".__DBC_SCHEMATA_GMAIL_INBOX_LOGS__."
                    (
                        idBooking, 
                        iEmailNumber,
                        szEmailID,
                        szFromEmail,
                        szSubject,
                        szEmailMessage,
                        dtReceivedOn,
                        szRawHeader,
                        idAdminAddedBy,
                        szIpAddress, 
                        szHttpReferrar,
                        szBrowser, 
                        szAttachments,
                        szEmailBodyType,
                        dtCreatedOn,
                        szCharSetType
                    )
                    VALUES
                    (
                        '".mysql_escape_custom(trim($idBooking))."',
                        '".mysql_escape_custom(trim($emailLogDataAry['iEmailNumber']))."',
                        '".mysql_escape_custom(trim($emailLogDataAry['szEmailID']))."',
                        '".mysql_escape_custom(trim($emailLogDataAry['szFromEmail']))."',  
                        '".mysql_escape_custom(trim($emailLogDataAry['szSubject']))."', 
                        '".mysql_escape_custom(trim($emailLogDataAry['szEmailMessage']))."', 
                        '".mysql_escape_custom(trim($emailLogDataAry['dtReceivedOn']))."', 
                        '".mysql_escape_custom(trim($emailLogDataAry['szRawHeader']))."',  
                        '".$idAdmin."',
                        '".mysql_escape_custom(__REMOTE_ADDR__)."',  
                        '".mysql_escape_custom(__HTTP_REFERER__)."',  
                        '".mysql_escape_custom(__HTTP_USER_AGENT__)."',
                        '".mysql_escape_custom(trim($emailLogDataAry['szAttachments']))."',
                        '".mysql_escape_custom(trim($emailLogDataAry['szEmailBodyType']))."',
                        now(),
                        '".mysql_escape_custom(trim($emailLogDataAry['szCharSetType']))."'
                    )  
                "; 
               // echo "<br><br>".$query ;
                //die;

                if($result=$this->exeSQL($query))
                { 
                    $szEmailKey = md5($to."_".time()."_".mt_rand(0,9999));

                    /* 
                    *  adding Email logs
                    */
                    $emailLogsAry = array();
                    $emailLogsAry['szFromEmail'] = $emailLogDataAry['szToEmail'];
                    $emailLogsAry['szFromUserName'] = $emailLogDataAry['szToUserName'];
                    $emailLogsAry['szEmailBody'] = $emailLogDataAry['szEmailMessage'] ;
                    $emailLogsAry['szEmailSubject'] = $emailLogDataAry['szSubject'] ;
                    $emailLogsAry['szToAddress'] = $emailLogDataAry['szFromEmailAddress'];
                    $emailLogsAry['szToUserName'] = $emailLogDataAry['szFromUserName'];
                    $emailLogsAry['iHasAttachments'] = $emailLogDataAry['iHasAttachment']; 
                    $emailLogsAry['szCCEmailAddress'] = $emailLogDataAry['szCCEmailAddress'];
                    $emailLogsAry['szBCCEmailAddress'] = $emailLogDataAry['szBCCEmailAddress']; 
                    $emailLogsAry['iEmailNumber'] = $emailLogDataAry['iEmailNumber'];
                    $emailLogsAry['idUser'] = $idCustomer;
                    $emailLogsAry['idBooking'] = $idBooking;
                    $emailLogsAry['szEmailKey'] = $szEmailKey ;
                    $emailLogsAry['iQuoteEmail'] = 1;
                    $emailLogsAry['iMode'] = 3;
                    $emailLogsAry['iIgnoreTracking'] = '1';
                    $emailLogsAry['iIncomingEmail'] = 1;
                    $emailLogsAry['iCRMMail'] = 1;
                    $emailLogsAry['szAttachmentFiles'] = $emailLogDataAry['szAttachments'];
                    $emailLogsAry['dtSent'] = $emailLogDataAry['dtReceivedOn'];
                    $emailLogsAry['iGmailFlag'] = 1;
                    $emailLogsAry['iDSTDifference'] = $emailLogDataAry['iDSTDifference'];
                    $emailLogsAry['iInvalidHtmlTags'] = $emailLogDataAry['iInvalidHtmlTags']; 
                    $kSendEmail = new cSendEmail();

                    $idEmailLogs = $kSendEmail->logEmails($emailLogsAry); 

                    $fileLogsAry = array(); 
                    $fileLogsAry['idEmailLogs'] = $idEmailLogs;  
                    $fileLogsAry['iClosed'] = 0;

                    if(!empty($fileLogsAry))
                    {
                        $file_log_query = '';
                        foreach($fileLogsAry as $key=>$value)
                        {
                            $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                        } 
                        $file_log_query = rtrim($file_log_query,",");
                        $kBookingNew = new cBooking();
                        $kBookingNew->updateBookingFileDetails($file_log_query,$idBookingFile); 
                    }  

                    if($idCustomer>0)
                    {
                        $kRegisterShipCon = new cRegisterShipCon();
                        $updateUserDetailsAry = array();
                        $updateUserDetailsAry['iConfirmed'] = 1;

                        if(!empty($updateUserDetailsAry))
                        {
                            $update_user_query = '';
                            foreach($updateUserDetailsAry as $key=>$value)
                            {
                                $update_user_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                            }
                        } 
                        $update_user_query = rtrim($update_user_query,",");  
                        if($kRegisterShipCon->updateUserDetailsByQuotes($update_user_query,$idCustomer))
                        {

                        }
                    } 
                    //echo "<br><br>Customer ID: ".$idCustomer." E-mail: ".$emailLogDataAry['szFromEmail']." File ID: ".$idBookingFile; 
                    return true;
                }
                else
                { 
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                } 
            }
        }
    }
    
    /* 
     * @Ajay - Following filers can be used with imap_search() function 
        ALL - return all messages matching the rest of the criteria
        ANSWERED - match messages with the \\ANSWERED flag set
        BCC "string" - match messages with "string" in the Bcc: field
        BEFORE "date" - match messages with Date: before "date"
        BODY "string" - match messages with "string" in the body of the message
        CC "string" - match messages with "string" in the Cc: field
        DELETED - match deleted messages
        FLAGGED - match messages with the \\FLAGGED (sometimes referred to as Important or Urgent) flag set
        FROM "string" - match messages with "string" in the From: field
        KEYWORD "string" - match messages with "string" as a keyword
        NEW - match new messages
        OLD - match old messages
        ON "date" - match messages with Date: matching "date"
        RECENT - match messages with the \\RECENT flag set
        SEEN - match messages that have been read (the \\SEEN flag is set)
        SINCE "date" - match messages with Date: after "date"
        SUBJECT "string" - match messages with "string" in the Subject:
        TEXT "string" - match messages with text "string"
        TO "string" - match messages with "string" in the To:
        UNANSWERED - match messages that have not been answered
        UNDELETED - match messages that are not deleted
        UNFLAGGED - match messages that are not flagged
        UNKEYWORD "string" - match messages that do not have the keyword "string"
        UNSEEN - match messages which have not been read yet 
    */ 
    function fetchInboxDataFromGmail_backup($dtLastRunDate)
    {
        if(!empty($dtLastRunDate))
        {
            $szHostName = self::$szHostName;
            $szUserName = self::$szUserName;
            $szPassword = self::$szPassword;
            $filename = __APP_PATH_ROOT__."/logs/gmail_api_".date('Ymd').".log";
            
            try
            {
                /* try to connect */
                $inbox = imap_open($szHostName,$szUserName,$szPassword); 
                if($inbox)
                {
                    /* grab emails */
                    $emailDataAry = array();
                    $emailDataAry = imap_search($inbox,'SINCE "'.$dtLastRunDate.'"');
                     
                    /* if emails are returned, loop through each... */
                    if(!empty($emailDataAry)) 
                    { 
                        /* for every email... */
                        foreach($emailDataAry as $email_number) 
                        {  
                            /* get information specific to this email */
                            $overview = imap_fetch_overview($inbox,$email_number,0);   
                            $structure = imap_fetchstructure($inbox, $email_number);
                            $headerInfo = imap_headerinfo($inbox,$email_number);
                            if(isset($structure->parts) && is_array($structure->parts) && isset($structure->parts[1]))
                            {
                                $part = $structure->parts[1];
                                $message = imap_fetchbody($inbox,$email_number,2);  
                                $message = $this->decodeGmailData($part->encoding,$message); 
                            }  
                            
                            echo "<br><br>Header Ary <br><br>";
                            print_R($headerInfo);
                            echo "<br><br>Overview Ary <br><br>";
                            print_R($overview);
                            
                            /* Building data to for adding logs */
                            $emailLogDataAry = array();
                            $emailLogDataAry['szSubject'] = utf8_decode(imap_utf8($overview[0]->subject));
                            $emailLogDataAry['szEmailMessage'] = $message;
                            $emailLogDataAry['iEmailNumber'] = $email_number;
                            $emailLogDataAry['szEmailID'] = $szEmailID;
                            $emailLogDataAry['szFrom'] = utf8_decode(imap_utf8($overview[0]->from));
                            $emailLogDataAry['dtReceivedOn'] = $overview[0]->date; 
                            
                            echo "<br><br> Gmail Data <br><br>";
                            print_R($emailLogDataAry);
                            
                            /* Adding data to gmail logs */
                            $this->logGmailInboxData($emailLogDataAry);
                        }  
                    } 
                }
                else 
                {
                    $strdata=array();
                    $strdata[0]=" \n\n Gmail Connection Failure \n\n";
                    $strdata[1] = print_r(imap_errors(),true)."\n\n"; 
                    file_log(true, $strdata, $filename);
                    $this->addError("szSpecialError",'Cannot connect to Gmail');
                    return false;
                }
            } 
            catch (Exception $ex) 
            {

            } 
        }
    }
    
    function getLastUpdatedMessagesCount($data)
    {
        if(!empty($data))
        {
            $query="
                SELECT 
                    count(id),
                    id
                FROM
                    ".__DBC_SCHEMATA_GMAIL_INBOX_LOGS__."	  
                WHERE
                    dtReceivedOn>= '".  mysql_escape_custom($data['dtFromDate'])."'
                AND
                    dtReceivedOn <= '".  mysql_escape_custom($data['dtToDate'])."'
                ORDER BY 
                    dtReceivedOn DESC
                LIMIT 0,1
            ";
            //echo "<br><br>".$query ;
            if($result = $this->exeSQL($query))
            {
                $retAry = array(); 
                $row=$this->getAssoc($result); 
                return $row;
            }
        } 
    }
    
    /** * close all open xhtml tags at the end of the string

    * @param string $html 
    * @return string 
    * @author Milian <mail@mili.de> 
    */
    function closetags($html)
    { 
        #put all opened tags into an array 
        preg_match_all('#<([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result); 
        $openedtags = $result[1];   #put all closed tags into an array 
        preg_match_all('#</([a-z]+)>#iU', $html, $result); 
        $closedtags = $result[1]; 
        $len_opened = count($openedtags);
            
        # all tags are closed

        if (count($closedtags) == $len_opened)
        {  
            $this->iInvalidHtmlTags = false;
            return $html; 
        }
        else
        {
            $this->iInvalidHtmlTags = true;
        }
        $openedtags = array_reverse($openedtags);

        # close tags

        for ($i=0; $i < $len_opened; $i++) 
        { 
            if (!in_array($openedtags[$i], $closedtags))
            { 
                $html .= '</'.$openedtags[$i].'>'; 
            } 
            else 
            {
                unset($closedtags[array_search($openedtags[$i], $closedtags)]);     
            } 
        }   
        return $html; 
    }
    
    function decodeGmailData($encoding,$text)
    {
        switch ($encoding) 
        {
            # 7BIT
            case 0:
                return $text;
            # 8BIT
            case 1:
                return quoted_printable_decode(imap_8bit($text));
            # BINARY
            case 2:
                return imap_binary($text);
            # BASE64
            case 3:
                return imap_base64($text);
            # QUOTED-PRINTABLE
            case 4:
                return quoted_printable_decode($text);
            # OTHER
            case 5:
                return $text;
            # UNKNOWN
            default:
                return $text;
        }
    } 
}
?>