<?php
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/forwarders/listExcelData.php" );

class cInsurance extends cDatabase
{
	var $szTitle;
	var $szFriendlyName;
	var $szDescription;
	var $szDescriptionHTML ;
	var $t_base="management/Error/";
        var $t_base_my_booking='Booking/MyBooking/';
	
	function __construct()
	{
            parent::__construct();
            return true;
	} 
	
        public function addInsurance($data)
        {
            if(!empty($data))
            {  
                $this->validateInsuranceRate($data);
                if($this->error==true)
                {
                    return false;
                } 
                
                /*
                    if($data['idInsurance']<=0 && $this->iInsuranceType==2)
                    {
                        $this->addError('szSpecialError', "Please add a buy rate and product then only you can add sell rate. ");
                        return false;
                    }
                * 
                */ 
                $insuranceDataAry = array();
                $insuranceDataAry['idOriginCountry'] = $this->idOriginCountry;
                $insuranceDataAry['idDestinationCountry'] = $this->idDestinationCountry;
                $insuranceDataAry['idTransportMode'] = $this->idTransportMode ;
                $insuranceDataAry['szCargoType'] = $this->szCargoType ;
                $insuranceDataAry['iInsuranceType'] = $this->iInsuranceType; 
                
                if($this->iInsuranceType==__INSURANCE_BUY_RATE_TYPE__)
                {
                    if($this->isInsuranceProductExist($insuranceDataAry,$data['idInsurance']))
                    {
                        $this->addError('szSpecialError', "Rate for selected combination already exists.");
                        return false;
                    }  
                }
                else
                {
                    $iBuyrateAvailable = false;
                    $idTransportMode = $insuranceDataAry['idTransportMode'];  

                    $insuranceDataAry['fValueUptobeCovered'] = $this->fInsurancePriceUSD; 
                    $kInsurance = new cInsurance();
                    $buyRateListAry = array();
                    $buyRateListAry = $kInsurance->getBuyrateCombination($insuranceDataAry,__INSURANCE_BUY_RATE_TYPE__);
                     
                    if($buyRateListAry[$idTransportMode])
                    {
                        $iBuyrateAvailable = 1;
                    }
                }
                
                $add_insuranceFlag = false;
                if($data['idInsurance']>0)
                {
                    $idInsurance = $data['idInsurance']; 
                    
                    if($this->iInsuranceType==__INSURANCE_BUY_RATE_TYPE__)
                    {
                        $kInsuranceNew1 = new cInsurance();
                        $kInsuranceNew1->updateBuyrateAvailability($idInsurance);
                        
                        if(!empty($kInsuranceNew1->buyRateAvailableAry))
                        {
                            $buyRateAvailableAryStr =$kInsuranceNew1->buyRateAvailableAry;
                        }  
                        if( !empty($kInsuranceNew1->buyRateNotAvailableAry))
                        {
                            $buyRateNotAvailableAryStr =$kInsuranceNew1->buyRateNotAvailableAry;
                        }

                    }
                    
                    
                    $query="
                        UPDATE
                            ".__DBC_SCHEMATA_INSURANCE__."
                        SET
                            idOriginCountry = '".mysql_escape_custom(trim($this->idOriginCountry))."', 
                            idDestinationCountry = '".mysql_escape_custom(trim($this->idDestinationCountry))."', 
                            idTransportMode = '".mysql_escape_custom(trim($this->idTransportMode))."',
                            iPrivate = '".mysql_escape_custom(trim($this->iPrivate))."',
                            fInsuranceUptoPrice = '".mysql_escape_custom(trim($this->fInsuranceUptoPrice))."',
                            idInsuranceCurrency = '".mysql_escape_custom(trim($this->idInsuranceCurrency))."',
                            szInsuranceCurrency = '".mysql_escape_custom(trim($this->szInsuranceCurrency))."',
                            fInsuranceExchangeRate = '".mysql_escape_custom(trim($this->fInsuranceExchangeRate))."',
                            fInsuranceRate = '".mysql_escape_custom(trim($this->fInsuranceRate))."',
                            fInsuranceMinPrice = '".mysql_escape_custom(trim($this->fInsuranceMinPrice))."',
                            idInsuranceMinCurrency = '".mysql_escape_custom(trim($this->iInsuranceMinCurrency))."',
                            szInsuranceMinCurrency = '".mysql_escape_custom(trim($this->szInsuranceMinCurrency))."',
                            fInsuranceMinExchangeRate= '".mysql_escape_custom(trim($this->fInsuranceMinExchangeRate))."',
                            fInsurancePriceUSD = '".mysql_escape_custom(trim($this->fInsurancePriceUSD))."',
                            fInsuranceMinPriceUSD = '".mysql_escape_custom(trim($this->fInsuranceMinPriceUSD))."',
                            szOriginCountry = '".mysql_escape_custom(trim($this->szOriginCountry))."',
                            szDestinationCountry = '".mysql_escape_custom(trim($this->szDestinationCountry))."',
                            szTransportMode = '".mysql_escape_custom(trim($this->szTransportMode))."',
                            szCargoTypeCode = '".mysql_escape_custom(trim($this->szCargoType))."',
                            szCommodityName = '".mysql_escape_custom(trim($this->szCommodityName))."',
                            iBuyrateAvailable = '".(int)$iBuyrateAvailable."',
                            dtUpdatedOn = now()
                        WHERE
                            id='".(int)$idInsurance."'  
                    "; 
                }
                else
                {
                    $query="
                        INSERT INTO
                            ".__DBC_SCHEMATA_INSURANCE__."
                        (
                            iInsuranceType,
                            idOriginCountry,
                            idDestinationCountry,
                            idTransportMode,
                            iPrivate, 
                            fInsuranceUptoPrice,
                            idInsuranceCurrency,
                            szInsuranceCurrency,
                            fInsuranceExchangeRate,
                            fInsuranceRate,
                            fInsuranceMinPrice,
                            idInsuranceMinCurrency,
                            szInsuranceMinCurrency,
                            fInsuranceMinExchangeRate,
                            fInsurancePriceUSD,
                            fInsuranceMinPriceUSD,
                            szOriginCountry,
                            szDestinationCountry,
                            szTransportMode,
                            szCargoTypeCode,
                            szCommodityName,
                            iBuyrateAvailable,
                            dtCreatedOn,
                            iActive
                        )
                        VALUES
                        (
                            '".mysql_escape_custom(trim($this->iInsuranceType))."',
                            '".mysql_escape_custom(trim($this->idOriginCountry))."', 
                            '".mysql_escape_custom(trim($this->idDestinationCountry))."',
                            '".mysql_escape_custom(trim($this->idTransportMode))."',
                            '".mysql_escape_custom(trim($this->iPrivate))."', 
                            '".mysql_escape_custom(trim($this->fInsuranceUptoPrice))."',
                            '".mysql_escape_custom(trim($this->idInsuranceCurrency))."',
                            '".mysql_escape_custom(trim($this->szInsuranceCurrency))."',
                            '".mysql_escape_custom(trim($this->fInsuranceExchangeRate))."',
                            '".mysql_escape_custom(trim($this->fInsuranceRate))."',
                            '".mysql_escape_custom(trim($this->fInsuranceMinPrice))."', 
                            '".mysql_escape_custom(trim($this->iInsuranceMinCurrency))."',
                            '".mysql_escape_custom(trim($this->szInsuranceMinCurrency))."',
                            '".mysql_escape_custom(trim($this->fInsuranceMinExchangeRate))."',
                            '".mysql_escape_custom(trim($this->fInsurancePriceUSD))."',
                            '".mysql_escape_custom(trim($this->fInsuranceMinPriceUSD))."', 
                            '".mysql_escape_custom(trim($this->szOriginCountry))."', 
                            '".mysql_escape_custom(trim($this->szDestinationCountry))."', 
                            '".mysql_escape_custom(trim($this->szTransportMode))."',
                            '".mysql_escape_custom(trim($this->szCargoType))."',
                            '".mysql_escape_custom(trim($this->szCommodityName))."', 
                            '".mysql_escape_custom(trim($iBuyrateAvailable))."',  
                            NOW(),
                            '1'
                        )
                    "; 
                    $add_insuranceFlag = true;
                }
//                echo $query ;
//                die;
                
                if($result = $this->exeSQL($query))
                {		 
                    if($add_insuranceFlag)
                    {
                        $this->idLastAddedInsurance = $this->iLastInsertID ;
                        $idInsurance = $this->idLastAddedInsurance;
                    } 
                    if((int)$data['idInsurance']==0  && $this->iInsuranceType==__INSURANCE_BUY_RATE_TYPE__)
                    {
                        $this->updateBuyrateAvailability($idInsurance);
                    }
                    else if((int)$data['idInsurance']>0  && $this->iInsuranceType==__INSURANCE_BUY_RATE_TYPE__)
                    {
                        $kInsuranceNew = new cInsurance();
                        $kInsuranceNew->updateBuyrateAvailability($idInsurance);
                        if(!empty($kInsuranceNew->buyRateAvailableAry))
                        {
                            $buyRateAvailableAryNewStr =$kInsuranceNew->buyRateAvailableAry;
                            $this->buyRateAvailableAry=  $kInsuranceNew->buyRateAvailableAry;
                        }  
                        if( !empty($kInsuranceNew->buyRateNotAvailableAry))
                        {
                            $buyRateNotAvailableAryNewStr =  implode(";",$kInsuranceNew->buyRateNotAvailableAry);
                            $this->buyRateNotAvailableAry=  $kInsuranceNew->buyRateNotAvailableAry;
                            
                        }
                        
                        if(!empty($buyRateAvailableAryStr))
                        {
                            foreach($buyRateAvailableAryStr as $buyRateAvailableAryStrs)
                            {
                                if(!empty($buyRateAvailableAryNewStr))
                                {
                                    if(!in_array($buyRateAvailableAryStrs, $buyRateAvailableAryNewStr))
                                    {
                                        $newBuyRateArr[]=$buyRateAvailableAryStrs;
                                    }
                                }
                                else
                                {
                                    $newBuyRateArr[]=$buyRateAvailableAryStrs;
                                }
                            }
                            
                            if(!empty($newBuyRateArr))
                            {
                               $newBuyRateStr=implode(";",$newBuyRateArr);
                               
                               $buyRateAvailableAryStrId=str_replace(";",",",$newBuyRateStr);
                                $query="
                                       UPDATE
                                           ".__DBC_SCHEMATA_INSURANCE__."
                                       SET
                                           iBuyrateAvailable = '0' 
                                       WHERE
                                          id IN (".$buyRateAvailableAryStrId.")
                                   "; 
                                //echo $query;
                                $result = $this->exeSQL($query);
                             
                               if($buyRateNotAvailableAryNewStr!='')
                               {
                                   $buyRateNotAvailableAryStrNew=$buyRateNotAvailableAryNewStr.";".$newBuyRateStr;
                               }
                               else
                               {
                                   $buyRateNotAvailableAryStrNew=$newBuyRateStr;
                               }
                               $this->buyRateNotAvailableAry=  explode(";", $buyRateNotAvailableAryStrNew);
                            }
                        }
                        
                                             
                        
                        
                    }
                        
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;

                }
            }
        }
        
        function updateBuyrateAvailability($idInsuranceBuyrate)
        {
            if($idInsuranceBuyrate>0)
            {
                $kInsurance = new cInsurance();
                $kInsurance->getInsuranceDetails($idInsuranceBuyrate,true,__INSURANCE_BUY_RATE_TYPE__); 
                if($kInsurance->idInsurance>0)
                { 
                    $fInsurancePriceUSD_buyrate = $kInsurance->fInsurancePriceUSD;
                    
                    $insuranceDataAry = array(); 
                    $insuranceDataAry['idOriginCountry'] = $kInsurance->idOriginCountry;
                    $insuranceDataAry['idDestinationCountry'] = $kInsurance->idDestinationCountry;
                    $insuranceDataAry['idTransportMode'] = $kInsurance->idTransportMode ;
                    $insuranceDataAry['szCargoType'] = $kInsurance->szCargoType ;
                    
                    $idTransportMode = $insuranceDataAry['idTransportMode'];
                
                    $insuranceSellRateAry = array();
                    $insuranceSellRateAry = $kInsurance->getBuyrateCombination($insuranceDataAry,__INSURANCE_SELL_RATE_TYPE__,true); 
                    
                    $insuranceSellRateAry = $insuranceSellRateAry[$idTransportMode] ;
                    if(!empty($insuranceSellRateAry))
                    {
                        $buyRateAvailableAry = array();
                        $buyRateNotAvailableAry = array();
                        $ctr = 0;
                        $ctr1 = 0;
                        foreach($insuranceSellRateAry as $insuranceSellRateArys)
                        { 
                            $fInsurancePriceUSD = $insuranceSellRateArys['fInsurancePriceUSD'];
                            if($fInsurancePriceUSD_buyrate>=$fInsurancePriceUSD)
                            {
                                $buyRateAvailableAry[$ctr] = $insuranceSellRateArys['id'];
                                $ctr++;
                            } 
                            else
                            {
                                $buyRateNotAvailableAry[$ctr1] = $insuranceSellRateArys['id'];
                                $ctr1++;
                            }
                        }
                        $this->buyRateAvailableAry = $buyRateAvailableAry;
                        $this->buyRateNotAvailableAry = $buyRateNotAvailableAry;
                        
                        if(!empty($buyRateAvailableAry))
                        {
                            $this->updateInsuranceSellRate($buyRateAvailableAry,1);
                        } 
                        if(!empty($buyRateNotAvailableAry))
                        {
                            $this->updateInsuranceSellRate($buyRateNotAvailableAry,0);
                        } 
                    }
                } 
            }
        }
        
        function updateInsuranceSellRate($insuranceSellRateAry,$iBuyrateAvailable=false)
        {
            if(!empty($insuranceSellRateAry))
            {
                $szInsuranceIdStr = implode(",",$insuranceSellRateAry); 
                
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_INSURANCE__."
                    SET 
                        iBuyrateAvailable = '".mysql_escape_custom($iBuyrateAvailable)."',
                        dtUpdatedOn = now()
                    WHERE
                        id IN (".$szInsuranceIdStr.")
                ";  
                //echo $query; 
                if($result = $this->exeSQL($query))
                {		  
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false; 
                }
            } 
        }
        
        function isBuyrateAvailableForSellRate()
        { 
            $query="
                SELECT
                    count(id) as iNumSellRates
                FROM
                    ".__DBC_SCHEMATA_INSURANCE__."
                WHERE 
                    iInsuranceType = '".__INSURANCE_SELL_RATE_TYPE__."'
                AND
                    iBuyrateAvailable = '0'
                AND
                    isDeleted = '0'
            ";  
            //echo $query;
            //die;
            if($result = $this->exeSQL($query))
            { 
                $row = $this->getAssoc($result); 
                return $row['iNumSellRates']; 
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
        
        public function updateInsurance($data)
        {
            if(!empty($data))
            {  
                $this->validateInsuranceRate($data);
                $this->set_idInsurance(sanitize_all_html_input(trim($data['idInsurance']))); 
                
                if($this->error==true)
                {
                    return false;
                }  
                if($this->idInsurance>0 && $this->iInsuranceType==2)
                {
                    $this->addError('szSpecialError', "You can not update product from sell rate section.");
                    return false;
                } 
                
                $insuranceDataAry = array();
                $insuranceDataAry['idOriginCountry'] = $this->idOriginCountry;
                $insuranceDataAry['idDestinationCountry'] = $this->idDestinationCountry;
                $insuranceDataAry['idTransportMode'] = $this->idTransportMode ;
                $insuranceDataAry['szCargoType'] = $this->szCargoType ;
                $insuranceDataAry['iInsuranceType'] = $this->iInsuranceType;
                
                if($this->isInsuranceProductExist($insuranceDataAry,$data['idInsurance']))
                {
                    $this->addError('szSpecialError', "Rate for selected combination already exists.");
                    return false;
                }
                 
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_INSURANCE__."
                    SET
                        idOriginCountry = '".mysql_escape_custom(trim($this->idOriginCountry))."', 
                        idDestinationCountry = '".mysql_escape_custom(trim($this->idDestinationCountry))."', 
                        idTransportMode = '".mysql_escape_custom(trim($this->idTransportMode))."',
                        iPrivate = '".mysql_escape_custom(trim($this->iPrivate))."',
                        fInsuranceUptoPrice = '".mysql_escape_custom(trim($this->fInsuranceUptoPrice))."',
                        idInsuranceCurrency = '".mysql_escape_custom(trim($this->idInsuranceCurrency))."',
                        szInsuranceCurrency = '".mysql_escape_custom(trim($this->szInsuranceCurrency))."',
                        fInsuranceExchangeRate = '".mysql_escape_custom(trim($this->fInsuranceExchangeRate))."',
                        fInsuranceRate = '".mysql_escape_custom(trim($this->fInsuranceRate))."',
                        fInsuranceMinPrice = '".mysql_escape_custom(trim($this->fInsuranceMinPrice))."',
                        idInsuranceMinCurrency = '".mysql_escape_custom(trim($this->iInsuranceMinCurrency))."',
                        szInsuranceMinCurrency = '".mysql_escape_custom(trim($this->szInsuranceMinCurrency))."',
                        fInsuranceMinExchangeRate = '".mysql_escape_custom(trim($this->fInsuranceMinExchangeRate))."',
                        fInsurancePriceUSD = '".mysql_escape_custom(trim($this->fInsurancePriceUSD))."',
                        fInsuranceMinPriceUSD = '".mysql_escape_custom(trim($this->fInsuranceMinPriceUSD))."',
                        dtUpdatedOn = now()
                    WHERE
                        id='".(int)$data['idInsurance']."'  
                ";  
//                echo $query ;
//                die;
                
                if($result = $this->exeSQL($query))
                { 
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false; 
                }
            }
        }
        function getAllInsuranceRates($iInsuranceType,$data=array())
        { 
            if($data['id']>0)
            {
                $query_and = " AND id = '".(int)$data['id']."' ";
            } 
            if(!empty($data['szSortField']))
            {
                if($data['szSortOrder']=='DESC')
                {
                    $szSortOrder = "DESC";
                }
                else
                {
                    $szSortOrder = "ASC";
                }
                $query_order_by = " ORDER BY ".mysql_escape_custom($data['szSortField'])." ".$szSortOrder;
            }
            else
            {
                $query_order_by = " ORDER BY szOriginCountry ASC, szDestinationCountry ASC, szTransportMode ASC, szCommodityName ASC, fInsuranceUptoPrice ASC, fInsuranceMinPrice ASC ";
            }
            $query="
                SELECT
                    id,
                    iInsuranceType,
                    idOriginCountry,
                    idDestinationCountry,
                    idTransportMode,
                    iPrivate, 
                    fInsuranceUptoPrice,
                    idInsuranceCurrency,
                    szInsuranceCurrency,
                    fInsuranceExchangeRate,
                    fInsuranceRate,
                    fInsuranceMinPrice,
                    idInsuranceMinCurrency,
                    szInsuranceMinCurrency,
                    fInsuranceMinExchangeRate,
                    fInsurancePriceUSD,
                    fInsuranceMinPriceUSD,
                    szOriginCountry,
                    szDestinationCountry,
                    szTransportMode,
                    szCargoTypeCode,
                    szCommodityName,
                    iBuyrateAvailable,
                    dtCreatedOn,
                    iActive
                FROM
                    ".__DBC_SCHEMATA_INSURANCE__."
                WHERE
                    iInsuranceType = '".mysql_escape_custom($iInsuranceType)."'
                AND
                    isDeleted = '0'
                    $query_and
                    $query_order_by
            ";  
            //echo $query;
            //die;
            if($result = $this->exeSQL($query))
            {
                $retAry = array();
                $ctr = 0;
                while($row = $this->getAssoc($result))
                {
                    $retAry[$ctr] = $row;
                    $ctr++;
                }
                return $retAry;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
        
        function isInsuranceProductExist($data,$id=false)
	{
            if(!empty($data))
            {
                $query="
                    SELECT
                        id
                    FROM
                        ".__DBC_SCHEMATA_INSURANCE__."
                    WHERE
                        idOriginCountry = '".mysql_escape_custom($data['idOriginCountry'])."'	
                    AND
                        idDestinationCountry = '".mysql_escape_custom($data['idDestinationCountry'])."'
                    AND
                        idTransportMode = '".mysql_escape_custom($data['idTransportMode'])."'
                    AND
                        szCargoTypeCode = '".mysql_escape_custom($data['szCargoType'])."'
                    AND
                        iInsuranceType = '".mysql_escape_custom($data['iInsuranceType'])."'
                    AND
                        isDeleted = '0'
                "; 
                if($id>0)
                {
                    $query .="
                        AND
                            id<>'".(int)$id."'		
                    ";
                }
                //echo $query;
                //die;
                if($result = $this->exeSQL($query))
                {
                    // Check if the user exists
                    if($this->getRowCnt()>0)
                    {
                        $row = $this->getAssoc($result);
                        $this->idInsuranceBuyRate  = $row['id'];
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
        
        function getAllInsuranceProductForQuotes($data,$iQuote=false)
	{
            if(!empty($data))
            {  
                if($data['idTransportMode']>0)
                {
                    $query_and = " AND idTransportMode = '".mysql_escape_custom($data['idTransportMode'])."' ";
                }
                if($data['idOriginCountry']>0)
                {
                    $query_and .= " AND idOriginCountry = '".mysql_escape_custom($data['idOriginCountry'])."' ";
                }
                if($data['idDestinationCountry']>0)
                {
                    $query_and .= " AND idDestinationCountry = '".mysql_escape_custom($data['idDestinationCountry'])."' ";
                }
                if(isset($data['szCargoType']))
                {
                    $query_and .= " AND szCargoTypeCode = '".mysql_escape_custom($data['szCargoType'])."' ";
                }
                if(isset($data['iInsuranceType']))
                {
                    $query_and .= " AND iInsuranceType = '".mysql_escape_custom($data['iInsuranceType'])."' ";
                } 
                if($data['iTryOuteFlag']==0){
                    if($data['fValueUptobeCovered']>0)
                    {
                        $query_and .= " AND fInsurancePriceUSD >= '".mysql_escape_custom($data['fValueUptobeCovered'])."' ";
                    }    
                }
                if(!empty($data['arrTransportMode']))
                {
                    $data['arrTransportMode'] = $this->removeNullFromArray($data['arrTransportMode']);
                    if(!empty($data['arrTransportMode']))
                    {
                        $arrTransportModeStr = implode(",",$data['arrTransportMode']);
                        $query_and .= " AND idTransportMode IN (".$arrTransportModeStr.") ";
                    }
                }
                if(!empty($data['arrOriginCountry']))
                {
                    $data['arrOriginCountry'] = $this->removeNullFromArray($data['arrOriginCountry']);
                    if(!empty($data['arrOriginCountry']))
                    {
                        $arrOriginCountryStr = implode(",",$data['arrOriginCountry']);
                        $query_and .= " AND idOriginCountry IN (".$arrOriginCountryStr.") ";
                    }
                }
                if(!empty($data['arrDestinationCountry']))
                {
                    $data['arrDestinationCountry'] = $this->removeNullFromArray($data['arrDestinationCountry']);
                    if(!empty($data['arrDestinationCountry']))
                    {
                        $arrDestinatationCountryStr = implode(",",$data['arrDestinationCountry']);
                        $query_and .= " AND idDestinationCountry IN (".$arrDestinatationCountryStr.") ";
                    }
                }
                
                if($data['iInsuranceType'] == __INSURANCE_SELL_RATE_TYPE__)
                { 
                    if($data['iLargestSellRate']==1)
                    {
                        /*
                        * getting largest sell rate for the trade
                        */
                        $query_order_by = " ORDER BY fInsurancePriceUSD DESC LIMIT 0,1";
                    }
                    else if($data['fTotalBookingAmountToBeInsurancedUsd']>0)
                    {
                        $query_select = ", (SELECT rate.fUsdValue FROM ".__DBC_SCHEMATA_EXCHANGE_RATE__." rate WHERE rate.idCurrency = ir.idInsuranceCurrency ORDER BY rate.id DESC LIMIT 0,1) as fLatestInsuranceExchangeRate ";
                        $fTotalInsuredAmount = $data['fTotalBookingAmountToBeInsurancedUsd'];
                        $query_order_by = " ORDER BY fInsurancePriceUSD ASC ";
                    } 
                    else
                    {
                        $query_order_by = " ORDER BY fInsurancePriceUSD ASC "; 
                    }
                }
                else
                {
                    $query_order_by = " ORDER BY fInsurancePriceUSD ASC ";
                }
                
                $query="
                    SELECT
                        id,
                        iInsuranceType,
                        idOriginCountry,
                        idDestinationCountry,
                        iPrivate,
                        idTransportMode,
                        iPrivate, 
                        fInsuranceUptoPrice,
                        idInsuranceCurrency,
                        szInsuranceCurrency,
                        fInsuranceExchangeRate,
                        fInsuranceRate,
                        fInsuranceMinPrice,
                        idInsuranceMinCurrency,
                        szInsuranceMinCurrency,
                        fInsuranceMinExchangeRate,
                        fInsurancePriceUSD,
                        fInsuranceMinPriceUSD,
                        iActive,
                        (SELECT rate.fUsdValue FROM ".__DBC_SCHEMATA_EXCHANGE_RATE__." rate WHERE rate.idCurrency = ir.idInsuranceCurrency ORDER BY rate.id DESC LIMIT 0,1) as fLatestInsuranceExchangeRate
                        $query_select
                    FROM
                        ".__DBC_SCHEMATA_INSURANCE__." ir
                    WHERE 
                        isDeleted = '0'
                        $query_and  
                        $query_order_by
                "; 
                if($data['iTryOuteFlag']==1)
                {
                   // echo $query;
                }
    //              echo $query;
//                die;
                if($result = $this->exeSQL($query))
                {
                    // Check if the user exists
                    if($this->getRowCnt()>0)
                    {
                        $ret_ary = array();
                        $ctr=0;
                        while($row = $this->getAssoc($result))
                        {
                            if($data['iTryOuteFlag']==1)
                            {
                                $fInsurancePriceUSD=$row['fInsurancePriceUSD']*$row['fLatestInsuranceExchangeRate'];
                                if((float)$fInsurancePriceUSD>0.00)
                                {
                                    $row['fInsurancePriceUSD']=$fInsurancePriceUSD;
                                }
                            }
                            if($iQuote)
                            {
                                if($data['iInsuranceType'] == __INSURANCE_SELL_RATE_TYPE__)
                                { 
                                    if($fTotalInsuredAmount>0)
                                    {
                                        /*
                                        * Converting Insurance Upto Price to USD
                                        */
                                        $fMaxInsurancePriceUSD = $row['fInsuranceUptoPrice'] * $row['fLatestInsuranceExchangeRate'];
                                        //echo "<br> Max: ".$fMaxInsurancePriceUSD." Insura: ".$fTotalInsuredAmount; 
                                        if($fMaxInsurancePriceUSD>=$fTotalInsuredAmount)
                                        { 
                                            $ret_ary[$row['idTransportMode']][] = $row ;
                                            /*
                                            if(!empty($ret_ary[$row['idTransportMode']]) && !array_key_exists($row['idTransportMode'],$ret_ary))
                                            {
                                                $ret_ary[$row['idTransportMode']][] = $row ;
                                            }
                                            else if(empty($ret_ary[$row['idTransportMode']]))
                                            {
                                                $ret_ary[$row['idTransportMode']][] = $row ;
                                            } 
                                             * 
                                             */
                                        } 
                                    } 
                                    else
                                    {
                                        $ret_ary[$row['idTransportMode']][] = $row ;
                                    }
                                } 
                                else
                                {
                                    $ret_ary[$row['idTransportMode']][] = $row ;
                                }
                            }
                            else
                            {
                                $ret_ary[$ctr] = $row ;
                                $ctr++;
                            } 
                        }
                        return $ret_ary;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
        
        function removeNullFromArray($array)
        {
            if(!empty($array))
            {
                $ret_ary = array();
                $ctr = 0;
                foreach($array as $array_item)
                {
                    if((int)$array_item==0)
                    {
                        unset($array_item);
                    } 
                    else
                    {
                        $ret_ary[$ctr] = $array_item;
                        $ctr++;
                    } 
                }
                return $ret_ary;
            } 
        }
        
        function getBuyrateCombination($data,$iInsuranceType=__INSURANCE_BUY_RATE_TYPE__,$return_all_sell_rates=false,$getFinalSellRate=false)
        { 
            if(!empty($data))
            {   
                $idOriginCountry = $data['idOriginCountry'];
                $idDestinationCountry = $data['idDestinationCountry'];
                $idTransportMode = $data['idTransportMode'];
                 
                $kConfig = new cConfig();
                $kConfig->loadCountry($idOriginCountry);
                $idOrgRegion = $kConfig->idRegion ;
                
                $kAdmin = new cAdmin();
                if($idOrgRegion>0)
                {
                    $orgSearchAry = array();
                    $orgRegionAry = array();
                    
                    $orgSearchAry['idRegion'] = $idOrgRegion ;
                    $orgRegionAry = $kAdmin->getAllRegions($orgSearchAry);
                    
                    $idOriginRegion = $orgRegionAry[0]['idCountry'];
                }
                 
                $kConfig->loadCountry($idDestinationCountry);
                $idDesRegion = $kConfig->idRegion ;
                
                if($idDesRegion>0)
                {
                    $desSearchAry = array();
                    $desRegionAry = array();
                    
                    $desSearchAry['idRegion'] = $idDesRegion ;
                    $desRegionAry = $kAdmin->getAllRegions($desSearchAry);
                    
                    $idDestinationRegion = $desRegionAry[0]['idCountry'];
                } 
                $searchAry = array();
                $searchAry['szCargoType']  = $data['szCargoType'] ;  
                $searchAry['iInsuranceType'] = $iInsuranceType;
                $searchAry['fTotalBookingAmountToBeInsurancedUsd'] = $data['fTotalBookingAmountToBeInsurancedUsd'];
                $searchAry['iLargestSellRate'] = $data['iLargestSellRate'];
                $searchAry['fValueUptobeCovered'] = $data['fValueUptobeCovered'];
                $searchAry['iTryOuteFlag'] = $data['iTryOuteFlag'];
                
                if($idTransportMode>0)
                {
                    $searchAry['arrTransportMode'][0] = $idTransportMode ;
                    $searchAry['arrTransportMode'][1] = __ALL_WORLD_TRANSPORT_MODE_ID__ ;
                    $searchAry['arrTransportMode'][2] = __REST_TRANSPORT_MODE_ID__ ;
                } 
                if($data['iLargestSellRate']==1)
                {
                    $searchAry['arrOriginCountry'][0] = $idOriginCountry ;
                    if($idOriginRegion>0)
                    {
                        $searchAry['arrOriginCountry'][1] = $idOriginRegion ;
                    } 
                    $searchAry['arrOriginCountry'][2] = __WHOLE_WORLD_PREFERENCES_ID__ ;
                    $searchAry['arrOriginCountry'][3] = __REST_OF_WORLD_PREFERENCES_ID__ ; 
                    
                    $searchAry['arrDestinationCountry'][0] = $idDestinationCountry ;
                    if($idDestinationRegion>0)
                    {
                        $searchAry['arrDestinationCountry'][1] = $idDestinationRegion ;
                    }
                    
                    $searchAry['arrDestinationCountry'][2] = __WHOLE_WORLD_PREFERENCES_ID__ ;
                    $searchAry['arrDestinationCountry'][3] = __REST_OF_WORLD_PREFERENCES_ID__ ;
                }
                
                $insuranceBuyRateAry = array();
                $insuranceBuyRateAry = $this->getAllInsuranceProductForQuotes($searchAry,true);
                
                if(!empty($insuranceBuyRateAry))
                { 
                    foreach($insuranceBuyRateAry as $key=>$forwarderContactListAryss)
                    { 
                        if(!empty($forwarderContactListAryss))
                        {
                            $ctr=0;
                            foreach($forwarderContactListAryss as $forwarderContactListArys)
                            {
                                
                                if(($forwarderContactListArys['idOriginCountry']==$idOriginCountry) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationCountry))
                                {
                                    $insuranceBuyRateNewAry[$ctr] = $forwarderContactListArys ;
                                    $insuranceBuyRateNewAry[$ctr]['iPriority'] = 1 ;
                                }
                                else if(($forwarderContactListArys['idOriginCountry']==$idOriginRegion) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationCountry))
                                {
                                    $insuranceBuyRateNewAry[$ctr] = $forwarderContactListArys ;
                                    $insuranceBuyRateNewAry[$ctr]['iPriority'] = 2;
                                }
                                else if(($forwarderContactListArys['idOriginCountry']==$idOriginCountry) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationRegion))
                                {
                                    $insuranceBuyRateNewAry[$key][$ctr] = $forwarderContactListArys ;
                                    $insuranceBuyRateNewAry[$key][$ctr]['iPriority'] = 3;
                                }
                                else if(($forwarderContactListArys['idOriginCountry']==$idOriginRegion) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationRegion))
                                {
                                    $insuranceBuyRateNewAry[$ctr] = $forwarderContactListArys ;
                                    $insuranceBuyRateNewAry[$ctr]['iPriority'] = 6 ;
                                }
                                else if(($forwarderContactListArys['idOriginCountry']==__WHOLE_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationCountry))
                                {
                                    $insuranceBuyRateNewAry[$ctr] = $forwarderContactListArys ;
                                    $insuranceBuyRateNewAry[$ctr]['iPriority'] = 4;
                                }
                                else if(($forwarderContactListArys['idOriginCountry']==$idOriginCountry) && ($forwarderContactListArys['idDestinationCountry']==__WHOLE_WORLD_PREFERENCES_ID__))
                                {
                                    $insuranceBuyRateNewAry[$ctr] = $forwarderContactListArys ;
                                    $insuranceBuyRateNewAry[$ctr]['iPriority'] = 5;
                                } 
                                else if(($forwarderContactListArys['idOriginCountry']==$idOriginRegion) && ($forwarderContactListArys['idDestinationCountry']==__WHOLE_WORLD_PREFERENCES_ID__))
                                {
                                    $insuranceBuyRateNewAry[$ctr] = $forwarderContactListArys ;
                                    $insuranceBuyRateNewAry[$ctr]['iPriority'] = 8;
                                }
                                else if(($forwarderContactListArys['idOriginCountry']==__WHOLE_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationRegion))
                                {
                                    $insuranceBuyRateNewAry[$ctr] = $forwarderContactListArys ;
                                    $insuranceBuyRateNewAry[$ctr]['iPriority'] = 7;
                                }
                                else if(($forwarderContactListArys['idOriginCountry']==__WHOLE_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==__WHOLE_WORLD_PREFERENCES_ID__))
                                {
                                    $insuranceBuyRateNewAry[$ctr] = $forwarderContactListArys ;
                                    $insuranceBuyRateNewAry[$ctr]['iPriority'] = 9;
                                } 
                                ++$ctr;
                            }
                            $insuranceBuyRateSortAry=array();
                            if(!empty($insuranceBuyRateNewAry))
                            {
                                $insuranceBuyRateSortAry=  sortArray($insuranceBuyRateNewAry,'iPriority');
                                $insuranceBuyRateArr[$key]=$insuranceBuyRateSortAry;
                            }
                        }
                    }
                }
                $foundBuyRateAry = array();
                if(!empty($insuranceBuyRateArr))
                { 
                    foreach($insuranceBuyRateArr as $key=>$forwarderContactListAryss)
                    { 
                        if(!empty($forwarderContactListAryss))
                        {
                            foreach($forwarderContactListAryss as $forwarderContactListArys)
                            {
                                //echo "<br> key: ".$key." Org Cnt db: ".$forwarderContactListArys['idOriginCountry']." Dest cnt db: ".$forwarderContactListArys['idDestinationCountry']." org reg: ".$idOriginRegion." org cont: ".$idOriginCountry." des reg: ".$idDestinationRegion." des cnt ".$idDestinationCountry; 
                                //print_r($forwarderContactListArys);
                                if(($forwarderContactListArys['idOriginCountry']==$idOriginCountry) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationCountry))
                                {
                                    $foundBuyRateAry[$key] = $forwarderContactListArys ;
                                    $foundSellRateAry[$key][] = $forwarderContactListArys ;
                                }
                                else if(($forwarderContactListArys['idOriginCountry']==$idOriginRegion) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationCountry))
                                {
                                    $foundBuyRateAry[$key] = $forwarderContactListArys;
                                    $foundSellRateAry[$key][] = $forwarderContactListArys ;
                                }
                                else if(($forwarderContactListArys['idOriginCountry']==$idOriginCountry) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationRegion))
                                {
                                    $foundBuyRateAry[$key] = $forwarderContactListArys;
                                    $foundSellRateAry[$key][] = $forwarderContactListArys ;
                                }
                                else if(($forwarderContactListArys['idOriginCountry']==$idOriginRegion) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationRegion))
                                {
                                    $foundBuyRateAry[$key] = $forwarderContactListArys ;
                                    $foundSellRateAry[$key][] = $forwarderContactListArys ;
                                }
                                else if(($forwarderContactListArys['idOriginCountry']==__WHOLE_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationCountry))
                                {
                                    $foundBuyRateAry[$key] = $forwarderContactListArys;
                                    $foundSellRateAry[$key][] = $forwarderContactListArys ;
                                }
                                else if(($forwarderContactListArys['idOriginCountry']==$idOriginCountry) && ($forwarderContactListArys['idDestinationCountry']==__WHOLE_WORLD_PREFERENCES_ID__))
                                {
                                    $foundBuyRateAry[$key] = $forwarderContactListArys;
                                    $foundSellRateAry[$key][] = $forwarderContactListArys ;
                                } 
                                else if(($forwarderContactListArys['idOriginCountry']==$idOriginRegion) && ($forwarderContactListArys['idDestinationCountry']==__WHOLE_WORLD_PREFERENCES_ID__))
                                {
                                    $foundBuyRateAry[$key] = $forwarderContactListArys;
                                    $foundSellRateAry[$key][] = $forwarderContactListArys ;
                                }
                                else if(($forwarderContactListArys['idOriginCountry']==__WHOLE_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationRegion))
                                {
                                    $foundBuyRateAry[$key] = $forwarderContactListArys;
                                    $foundSellRateAry[$key][] = $forwarderContactListArys ;
                                }
                                else if(($forwarderContactListArys['idOriginCountry']==__WHOLE_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==__WHOLE_WORLD_PREFERENCES_ID__))
                                {
                                    $foundBuyRateAry[$key] = $forwarderContactListArys;
                                    $foundSellRateAry[$key][] = $forwarderContactListArys ;
                                } 
                                
                                if((int)$data['iGetAllRecord']==0)//To Get the All Mode Insurance Buy Rate 10-Jan-2017
                                {
                                    if($iInsuranceType==__INSURANCE_BUY_RATE_TYPE__ && !empty($foundBuyRateAry[$key]))
                                    {
                                        /*
                                        * If we are looking to get Insurance buy rate and got 1 matching record then no point in checking for furthure row.
                                        */
                                        break;
                                    }
                                }
                            }
                            
                        }
                        
                        if((int)$data['iGetAllRecord']==0)//To Get the All Mode Insurance Buy Rate 10-Jan-2017
                        {
                            if($iInsuranceType==__INSURANCE_BUY_RATE_TYPE__ && !empty($foundBuyRateAry[$key]))
                            {
                                /*
                                 * If we are looking to get Insurance buy rate and got 1 matching record then no point in checking for furthure row.
                                 */
                                break;
                            }
                        }
                    }  
                    
                  
                    //Finding data for REST of world
                    foreach($insuranceBuyRateNewAry as $key=>$forwarderContactListAryss)
                    { 
                        if(!empty($forwarderContactListAryss))
                        {
                            foreach($forwarderContactListAryss as $forwarderContactListArys)
                            {
                                if(empty($foundBuyRateAry[$key]))
                                {
                                     //If we do not got any matching on based of country, region and whole then search for Rest of world option 
                                    if(($forwarderContactListArys['idOriginCountry']==__REST_OF_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationCountry))
                                    {
                                        $foundBuyRateAry[$key] = $forwarderContactListArys;
                                        $foundSellRateAry[$key][] = $forwarderContactListArys ;
                                    }
                                    else if(($forwarderContactListArys['idOriginCountry']==$idOriginCountry) && ($forwarderContactListArys['idDestinationCountry']==__REST_OF_WORLD_PREFERENCES_ID__))
                                    {
                                        $foundBuyRateAry[$key] = $forwarderContactListArys;
                                        $foundSellRateAry[$key][] = $forwarderContactListArys ;
                                    }
                                    else if(($forwarderContactListArys['idOriginCountry']==__REST_OF_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationRegion))
                                    {
                                        $foundBuyRateAry[$key] = $forwarderContactListArys;
                                        $foundSellRateAry[$key][] = $forwarderContactListArys ;
                                    }
                                    else if(($forwarderContactListArys['idOriginCountry']==$idOriginRegion) && ($forwarderContactListArys['idDestinationCountry']==__REST_OF_WORLD_PREFERENCES_ID__))
                                    {
                                        $foundBuyRateAry[$key] = $forwarderContactListArys;
                                        $foundSellRateAry[$key][] = $forwarderContactListArys ;
                                    }
                                    else if(($forwarderContactListArys['idOriginCountry']==__WHOLE_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==__REST_OF_WORLD_PREFERENCES_ID__))
                                    {
                                        $foundBuyRateAry[$key] = $forwarderContactListArys;
                                        $foundSellRateAry[$key][] = $forwarderContactListArys ;
                                    }
                                    else if(($forwarderContactListArys['idOriginCountry']==__REST_OF_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==__WHOLE_WORLD_PREFERENCES_ID__))
                                    {
                                        $foundBuyRateAry[$key] = $forwarderContactListArys;
                                        $foundSellRateAry[$key][] = $forwarderContactListArys ;
                                    }
                                    else if(($forwarderContactListArys['idOriginCountry']==__REST_OF_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==__REST_OF_WORLD_PREFERENCES_ID__))
                                    {
                                        $foundBuyRateAry[$key] = $forwarderContactListArys;
                                        $foundSellRateAry[$key][] = $forwarderContactListArys ;
                                    }  
                                }
                            }
                        }
                    }
                } 
                if($return_all_sell_rates)
                { 
                    $sellRateListAry = $foundSellRateAry ;
                    if($idTransportMode>0)
                    {
                        if(!empty($sellRateListAry[$idTransportMode]))
                        {
                            $sellRateListAry[$idTransportMode] = $sellRateListAry[$idTransportMode] ;
                        }
                        else if(!empty($insuranceBuyRateAry[__ALL_WORLD_TRANSPORT_MODE_ID__]))
                        {
                            $sellRateListAry[$idTransportMode] = $sellRateListAry[__ALL_WORLD_TRANSPORT_MODE_ID__] ;
                        }
                        else if(!empty($insuranceBuyRateAry[__REST_TRANSPORT_MODE_ID__]))
                        {
                            $sellRateListAry[$idTransportMode] = $sellRateListAry[__REST_TRANSPORT_MODE_ID__] ;
                        }
                        
                        if($getFinalSellRate)
                        { 
                            $sortedInsuranceSellRateAry = array();
                            $sortedInsuranceSellRateAry = sortArray($sellRateListAry[$idTransportMode], 'fInsurancePriceUSD'); 
                             
                            $sellRateListAry = array();
                            $sellRateListAry[$idTransportMode] = $sortedInsuranceSellRateAry[0];
                        }  
                    }  
                    return $sellRateListAry ; 
                }
                else
                {
                    $buyRateListAry = $foundBuyRateAry ;
                    if($idTransportMode>0)
                    {
                        if(!empty($buyRateListAry[$idTransportMode]))
                        {
                            $buyRateListAry[$idTransportMode] = $buyRateListAry[$idTransportMode] ;
                        }
                        else if(!empty($insuranceBuyRateAry[__ALL_WORLD_TRANSPORT_MODE_ID__]))
                        {
                            $buyRateListAry[$idTransportMode] = $buyRateListAry[__ALL_WORLD_TRANSPORT_MODE_ID__] ;
                        }
                        else if(!empty($insuranceBuyRateAry[__REST_TRANSPORT_MODE_ID__]))
                        {
                            $buyRateListAry[$idTransportMode] = $buyRateListAry[__REST_TRANSPORT_MODE_ID__] ;
                        }
                    }  
                    return $buyRateListAry ;
                } 
            } 
        }
        
	public function addInsuranceRates($data)
	{
            if(!empty($data))
            {   
                $this->validateInsuranceRate($data); 
                if($this->error==true)
                {
                    return false;
                }
                
                $query="
                    INSERT INTO
                        ".__DBC_SCHEMATA_INSURANCE_RATES__."
                    (
                        iInsuranceType,
                        idInsurance,
                        fInsuranceUptoPrice,
                        idInsuranceCurrency,
                        szInsuranceCurrency,
                        fInsuranceExchangeRate,
                        fInsuranceRate,
                        fInsuranceMinPrice,
                        idInsuranceMinCurrency,
                        szInsuranceMinCurrency,
                        fInsuranceMinExchangeRate,
                        fInsurancePriceUSD,
                        fInsuranceMinPriceUSD,
                        dtCreated					
                    )
                    VALUES
                    (
                        '".mysql_escape_custom(trim($this->iInsuranceType))."',
                        '".(int)$data['idInsurance']."',
                        '".mysql_escape_custom(trim($this->fInsuranceUptoPrice))."',
                        '".mysql_escape_custom(trim($this->idInsuranceCurrency))."',
                        '".mysql_escape_custom(trim($this->szInsuranceCurrency))."',
                        '".mysql_escape_custom(trim($this->fInsuranceExchangeRate))."',
                        '".mysql_escape_custom(trim($this->fInsuranceRate))."',
                        '".mysql_escape_custom(trim($this->fInsuranceMinPrice))."', 
                        '".mysql_escape_custom(trim($this->iInsuranceMinCurrency))."',
                        '".mysql_escape_custom(trim($this->szInsuranceMinCurrency))."',
                        '".mysql_escape_custom(trim($this->fInsuranceMinExchangeRate))."',
                        '".mysql_escape_custom(trim($this->fInsurancePriceUSD))."',
                        '".mysql_escape_custom(trim($this->fInsuranceMinPriceUSD))."', 
                        NOW()
                    )
                ";
                //echo "<br>".$query."<br>" ;
                //die; 
                if($result = $this->exeSQL($query))
                {			
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;

                }
            }
	} 
	function validateInsuranceRate($data)
	{ 
            if(!empty($data))
            { 
                $this->set_idOriginCountry(sanitize_all_html_input(trim($data['idOriginCountry'])));
                $this->set_idDestinationCountry(sanitize_all_html_input(trim($data['idDestinationCountry'])));
                $this->set_idTransportMode(sanitize_all_html_input(trim($data['idTransportMode'])));
                $this->set_szCargoType(sanitize_all_html_input(trim($data['szCargoType'])));   
                $this->set_iInsuranceType(sanitize_all_html_input(trim($data['iInsuranceType'])));
                $this->set_fInsuranceUptoPrice(sanitize_all_html_input(trim($data['fInsuranceUptoPrice'])));
                $this->set_idInsuranceCurrency(sanitize_all_html_input(trim($data['idInsuranceCurrency']))); 
                $this->set_fInsuranceRate(sanitize_all_html_input(trim($data['fInsuranceRate'])));
                $this->set_fInsuranceMinPrice(sanitize_all_html_input(trim($data['fInsuranceMinPrice'])));
                
                //$this->set_iInsuranceMinCurrency(sanitize_all_html_input(trim($data['iInsuranceMinCurrency'])));

                if($this->error==true)
                {
                    return false;
                }
                else
                {
                    $kWarehouseSearch = new cWHSSearch(); 
                    $idInsuranceCurrency = $this->idInsuranceCurrency ;
                    if($idInsuranceCurrency==1)
                    {
                        $this->idInsuranceCurrency = 1;
                        $this->szInsuranceCurrency = 'USD';
                        $this->fInsuranceExchangeRate = 1;

                        $this->fInsurancePriceUSD = $this->fInsuranceUptoPrice ; 
                        
                        /*
                         * From now Value upto currency and Min currency will always be same
                         */
                        $this->iInsuranceMinCurrency = 1;
                        $this->szInsuranceMinCurrency = 'USD';
                        $this->fInsuranceMinExchangeRate = 1;

                        $this->fInsuranceMinPriceUSD = $this->fInsuranceMinPrice ;
                    }
                    else
                    {
                        $resultAry = $kWarehouseSearch->getCurrencyDetails($idInsuranceCurrency);		

                        $this->idInsuranceCurrency = $resultAry['idCurrency'];
                        $this->szInsuranceCurrency = $resultAry['szCurrency'];
                        $this->fInsuranceExchangeRate = $resultAry['fUsdValue'];

                        $this->fInsurancePriceUSD = round(($this->fInsuranceUptoPrice * $this->fInsuranceExchangeRate),4);
                        
                        $this->iInsuranceMinCurrency = $resultAry['idCurrency'];
                        $this->szInsuranceMinCurrency = $resultAry['szCurrency'];
                        $this->fInsuranceMinExchangeRate = $resultAry['fUsdValue'];

                        $this->fInsuranceMinPriceUSD = $this->fInsuranceMinPrice * $this->fInsuranceMinExchangeRate  ;
                    } 
                    $kConfig = new cConfig();
                    $countryRegionAry = array();
                    $countryRegionAry = $kConfig->getAllCountriesForPreferencesKeyValuePair();  
                    $configLangArr=$kConfig->getConfigurationLanguageData('__TABLE_TRANSPORT_MODE__');
                    
                    $this->szTransportMode = $configLangArr[$this->idTransportMode]['szShortName'];
                    $this->szOriginCountry = $countryRegionAry[$this->idOriginCountry]['szCountryName'];
                    $this->szDestinationCountry = $countryRegionAry[$this->idDestinationCountry]['szCountryName'];
                    
                    $kVatApplication = new cVatApplication();
                    $kVatApplication->loadCargoTypes($this->szCargoType);
                    $this->szCommodityName = $kVatApplication->szCargoTypeName;
/*
                    $iInsuranceMinCurrency = $this->idInsuranceCurrency ;
                    if($idInsuranceCurrency==1)
                    {
                        $this->iInsuranceMinCurrency = 1;
                        $this->szInsuranceMinCurrency = 'USD';
                        $this->fInsuranceMinExchangeRate = 1;

                        $this->fInsuranceMinPriceUSD = $this->fInsuranceMinPrice ;
                    }
                    else
                    {
                        $resultAry = $kWarehouseSearch->getCurrencyDetails($iInsuranceMinCurrency);		

                        $this->iInsuranceMinCurrency = $resultAry['idCurrency'];
                        $this->szInsuranceMinCurrency = $resultAry['szCurrency'];
                        $this->fInsuranceMinExchangeRate = $resultAry['fUsdValue'];

                        $this->fInsuranceMinPriceUSD = $this->fInsuranceMinPrice * $this->fInsuranceMinExchangeRate  ;
                    } 
 * 
 */
                    return true;
                }
            }
	}
        
	function loadInsuranceRates($iType,$idInsuranceRate)
	{
            if($iType>0 && $idInsuranceRate>0)
            {
                $query="
                    SELECT
                        id,
                        idInsurance,
                        iInsuranceType,
                        fInsuranceUptoPrice,
                        idInsuranceCurrency,
                        szInsuranceCurrency,
                        fInsuranceExchangeRate,
                        fInsuranceRate,
                        fInsuranceMinPrice,
                        idInsuranceMinCurrency,
                        szInsuranceMinCurrency,
                        fInsuranceMinExchangeRate,
                        fInsurancePriceUSD,
                        fInsuranceMinPriceUSD,
                        dtCreated	
                    FROM
                        ".__DBC_SCHEMATA_INSURANCE_RATES__."
                    WHERE
                        iInsuranceType = '".$iType."'
                    AND
                        id=".(int)$idInsuranceRate."
                    AND
                        isDeleted = '0'
                ";
                
                if($result=$this->exeSQL($query))
                {
                    $ret_ary = array();
                    $ctr = 0;
                    $row=$this->getAssoc($result); 

                    $this->id = sanitize_all_html_input(trim($row['id']));
                    $this->idInsurance = sanitize_all_html_input(trim($row['idInsurance']));
                    $this->iInsuranceType = sanitize_all_html_input(trim($row['iInsuranceType'])); 
                    $this->fInsuranceUptoPrice = sanitize_all_html_input(trim($row['fInsuranceUptoPrice']));
                    $this->idInsuranceCurrency = sanitize_all_html_input(trim($row['idInsuranceCurrency']));
                    $this->szInsuranceCurrency = sanitize_all_html_input(trim($row['szInsuranceCurrency'])); 
                    $this->fInsuranceExchangeRate = sanitize_all_html_input(trim($row['fInsuranceExchangeRate']));
                    $this->fInsuranceRate = sanitize_all_html_input(trim($row['fInsuranceRate']));
                    $this->fInsuranceMinPrice = sanitize_all_html_input(trim($row['fInsuranceMinPrice']));
                    $this->idInsuranceMinCurrency = sanitize_all_html_input(trim($row['idInsuranceMinCurrency'])); 
                    $this->szInsuranceMinCurrency = sanitize_all_html_input(trim($row['szInsuranceMinCurrency']));
                    $this->fInsuranceMinExchangeRate = sanitize_all_html_input(trim($row['fInsuranceMinExchangeRate']));
                    $this->fInsurancePriceUSD = sanitize_all_html_input(trim($row['fInsurancePriceUSD']));
                    $this->fInsuranceMinPriceUSD = sanitize_all_html_input(trim($row['fInsuranceMinPriceUSD']));
                    $this->dtCreated = sanitize_all_html_input(trim($row['dtCreated']));
                    return $row ;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
        function getInsuranceRates($iType,$idInsurance=false,$isWorkingFlag=false)
	{
            if($iType>0)
            {
                if($idInsurance>0)
                {
                    $query_and = " AND ir.idInsurance = '".(int)$idInsurance."' ";
                }
                if($isWorkingFlag)
                {
                    $query_and .= " AND ir.isWorking = '0' ";
                }
                
                $query="
                    SELECT
                        ir.id,
                        ir.idInsurance,
                        ir.iInsuranceType,
                        ir.fInsuranceUptoPrice,
                        ir.idInsuranceCurrency,
                        ir.szInsuranceCurrency,
                        ir.fInsuranceExchangeRate,
                        ir.fInsuranceRate,
                        ir.fInsuranceMinPrice,
                        ir.idInsuranceMinCurrency,
                        ir.szInsuranceMinCurrency,
                        ir.fInsuranceMinExchangeRate,
                        ir.fInsurancePriceUSD,
                        ir.fInsuranceMinPriceUSD,
                        ir.dtCreated,
                        (SELECT rate.fUsdValue FROM ".__DBC_SCHEMATA_EXCHANGE_RATE__." rate WHERE rate.idCurrency = ir.idInsuranceCurrency ORDER BY rate.id DESC LIMIT 0,1) as fLatestInsuranceExchangeRate,
                        (SELECT rate2.fUsdValue FROM ".__DBC_SCHEMATA_EXCHANGE_RATE__." rate2 WHERE rate2.idCurrency = ir.idInsuranceMinCurrency ORDER BY rate2.id DESC LIMIT 0,1) as fLatestInsuranceMinExchangeRate
                    FROM
                        ".__DBC_SCHEMATA_INSURANCE_RATES__." ir 
                    WHERE
                        ir.iInsuranceType = '".$iType."'
                    AND
                        ir.isDeleted = '0'
                        $query_and
                    ORDER BY
                        ir.fInsurancePriceUSD ASC, ir.fInsuranceMinPriceUSD ASC
                ";
                //echo $query ;
                if($result=$this->exeSQL($query))
                {
                    $ret_ary = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    { 
                        $ret_ary[$ctr] = $row ;
                        $ctr++;
                    } 
                    return $ret_ary ;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
        
        function getAllInsuranceBuyRates()
	{ 
            
            $kConfig = new cConfig();
            $configLangArr=$kConfig->getConfigurationLanguageData('__TABLE_TRANSPORT_MODE__');
            $query="
                SELECT
                    i.id,  
                    i.fInsuranceUptoPrice,
                    i.idInsuranceCurrency,
                    i.szInsuranceCurrency,
                    i.fInsuranceExchangeRate,
                    i.fInsuranceRate,
                    i.fInsuranceMinPrice,
                    i.idInsuranceMinCurrency,
                    i.szInsuranceMinCurrency,
                    i.fInsuranceMinExchangeRate,
                    i.fInsurancePriceUSD,
                    i.fInsuranceMinPriceUSD,
                    i.dtCreatedOn,
                    i.idOriginCountry,
                    i.idDestinationCountry,
                    ( SELECT cont.szCountryName FROM ".__DBC_SCHEMATA_COUNTRY__." cont WHERE cont.id = i.idOriginCountry ) as szOriginCountry,
                    ( SELECT cont2.szCountryName FROM ".__DBC_SCHEMATA_COUNTRY__." cont2 WHERE cont2.id = i.idDestinationCountry ) as szDestinationCountry,
                    i.iPrivate,
                    i.idTransportMode
                FROM 
                    ".__DBC_SCHEMATA_INSURANCE__." i 
                WHERE 
                    i.isDeleted = '0'
                AND
                    i.iActive = '1' 
            ";
            //echo $query; 
            if($result=$this->exeSQL($query))
            {
                $ret_ary = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                { 
                    if(!empty($configLangArr[$row['idTransportMode']]))
                    {
                        $configLangArr[$row['idTransportMode']]['szTransportMode']=$configLangArr[$row['idTransportMode']]['szShortName'];
                        $ret_ary[$ctr] = array_merge($configLangArr[$row['idTransportMode']],$row);
                    }else
                    {
                        $ret_ary[$ctr] = $row;
                    }
                    $ctr++;
                } 
                $sortArr=array();
                $sortArr[]='szOriginCountry_';
                $sortArr[]='szDestinationCountry_';
                $sortArr[]='szTransportMode_';
                $sortArr[]='iPrivate_1';
               $ret_ary=sortArrayNew($ret_ary,$sortArr);
                //print_r($ret_ary);
                return $ret_ary ;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
	}
	function getInsuranceDetails($idInsurance=false,$load_object=false,$iInsuranceType=false)
	{
            if($idInsurance>0)
            {
                $query_where = " AND i.id = '".(int)$idInsurance."'  ";
            } 
            if($iInsuranceType>0)
            {
                $query_where .= " AND iInsuranceType = '".(int)$iInsuranceType."'  ";
            }  
                    
            
            $query="
                SELECT 
                    id,
                    iInsuranceType,
                    idOriginCountry,
                    idDestinationCountry,
                    idTransportMode, 
                    fInsuranceUptoPrice,
                    idInsuranceCurrency,
                    szInsuranceCurrency,
                    fInsuranceExchangeRate,
                    fInsuranceRate,
                    fInsuranceMinPrice,
                    idInsuranceMinCurrency,
                    szInsuranceMinCurrency,
                    fInsuranceMinExchangeRate,
                    fInsurancePriceUSD,
                    fInsuranceMinPriceUSD,
                    szOriginCountry,
                    szDestinationCountry,
                    szTransportMode,
                    szCargoTypeCode,
                    szCommodityName,
                    dtCreatedOn,
                    iActive
                FROM 
                    ".__DBC_SCHEMATA_INSURANCE__." as i
                WHERE
                    iActive = '1' 
                 AND
                    isDeleted = '0'
                  $query_where
            "; 
            //echo $query;
            if($result=$this->exeSQL($query))
            { 
                if($this->iNumRows>0)
                {
                    if($idInsurance>0)
                    {
                        $row=$this->getAssoc($result) ;
                        if($load_object)
                        {
                            $this->id = sanitize_all_html_input(trim($row['id'])); 
                            $this->idInsurance = sanitize_all_html_input(trim($row['id']));
                            $this->iInsuranceType = sanitize_all_html_input(trim($row['iInsuranceType'])); 
                            $this->idOriginCountry = sanitize_all_html_input(trim($row['idOriginCountry']));
                            $this->idDestinationCountry = sanitize_all_html_input(trim($row['idDestinationCountry']));
                            $this->idTransportMode = sanitize_all_html_input(trim($row['idTransportMode']));
                            $this->iPrivate = sanitize_all_html_input(trim($row['iPrivate'])); 
                            $this->szOriginCountry = sanitize_all_html_input(trim($row['szOriginCountry']));
                            $this->szDestinationCountry = sanitize_all_html_input(trim($row['szDestinationCountry'])); 
                            $this->szTransportMode = sanitize_all_html_input(trim($row['szTransportMode']));  
                            $this->szCargoTypeCode = sanitize_all_html_input(trim($row['szCargoTypeCode'])); 
                            $this->szCommodityName = sanitize_all_html_input(trim($row['szCommodityName'])); 
                            $this->fInsuranceUptoPrice = sanitize_all_html_input(trim($row['fInsuranceUptoPrice']));
                            $this->idInsuranceCurrency = sanitize_all_html_input(trim($row['idInsuranceCurrency']));
                            $this->szInsuranceCurrency = sanitize_all_html_input(trim($row['szInsuranceCurrency'])); 
                            $this->fInsuranceExchangeRate = sanitize_all_html_input(trim($row['fInsuranceExchangeRate']));
                            $this->fInsuranceRate = sanitize_all_html_input(trim($row['fInsuranceRate']));
                            $this->fInsuranceMinPrice = sanitize_all_html_input(trim($row['fInsuranceMinPrice']));
                            $this->idInsuranceMinCurrency = sanitize_all_html_input(trim($row['idInsuranceMinCurrency'])); 
                            $this->szInsuranceMinCurrency = sanitize_all_html_input(trim($row['szInsuranceMinCurrency']));
                            $this->fInsuranceMinExchangeRate = sanitize_all_html_input(trim($row['fInsuranceMinExchangeRate']));
                            $this->fInsurancePriceUSD = sanitize_all_html_input(trim($row['fInsurancePriceUSD']));
                            $this->fInsuranceMinPriceUSD = sanitize_all_html_input(trim($row['fInsuranceMinPriceUSD']));
                            $this->dtCreated = sanitize_all_html_input(trim($row['dtCreated']));
                            $this->szCargoType = sanitize_all_html_input(trim($row['szCargoTypeCode'])); 
                        }  
                        return $row ;
                    }
                    else
                    {
                        $ret_ary = array();
                        $ctr = 0;
                        while($row=$this->getAssoc($result))
                        { 
                            $ret_ary[$ctr] = $row ;
                            $ctr++;
                        } 
                        return $ret_ary ;
                    } 
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
	}
	
        function deleteInsurance($iType,$idInsurance)
	{
            if($iType>0 && !empty($idInsurance))
            { 
                
                if(is_array($idInsurance))
                {
                    $insuranceIdStr = implode(",",$idInsurance);
                    $query_update = " id IN (".$insuranceIdStr.")";
                    $buyRateAvailableAryStr='';
                    $buyRateNotAvailableAryStr='';
                    if($iType==__INSURANCE_BUY_RATE_TYPE__)
                    {
                        if(!empty($idInsurance))
                        {
                            foreach($idInsurance as $idInsurances)
                            {
                                $resBuyRateArr=array();
                                $this->updateBuyrateAvailability($idInsurances);
                                if(!empty($this->buyRateAvailableAry))
                                {
                                    $buyRateAvailableAryString=implode(";",$this->buyRateAvailableAry);
                                    if($buyRateAvailableAryStr!='')
                                    {
                                        $buyRateAvailableAryStr =$buyRateAvailableAryStr.";".$buyRateAvailableAryString;
                                    }
                                    else
                                    {
                                        $buyRateAvailableAryStr =$buyRateAvailableAryString;
                                    }
                                    
                                }  
                                if( !empty($this->buyRateNotAvailableAry))
                                {
                                    $buyRateNotAvailableAryString=implode(";",$this->buyRateNotAvailableAry);
                                    if($buyRateNotAvailableAryStr!='')
                                    {
                                        $buyRateNotAvailableAryStr =$buyRateNotAvailableAryStr.";".$buyRateNotAvailableAryString;
                                    }
                                    else
                                    {
                                        $buyRateNotAvailableAryStr =$buyRateNotAvailableAryString;
                                    }
                                }
                                    
                            }
                        }
                        
                    }
                }
                else if($idInsurance>0)
                {
                    $query_update = " id = '".(int)$idInsurance."' ";
                    
                    $resBuyRateArr=array();
                    $this->updateBuyrateAvailability($idInsurance);
                    if(!empty($this->buyRateAvailableAry))
                    {
                        $buyRateAvailableAryStr =implode(";",$this->buyRateAvailableAry);
                    }  
                    if( !empty($this->buyRateNotAvailableAry))
                    {
                        $buyRateNotAvailableAryStr =implode(";",$this->buyRateNotAvailableAry);
                    }
                }
                else
                {
                    return false;
                } 
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_INSURANCE__."
                    SET
                        isDeleted = '1', 
                        dtUpdatedOn = now() 
                    WHERE
                       $query_update 
                "; 
//                echo $query ;
//                die;
                if($result = $this->exeSQL($query))
                {
                    /* 
                    * If we delete buy rate then we have to also deleting corresponding sell rate.
                    */
                    if($iType==1)
                    {  
                        $this->buyRateAvailableAryStr=$buyRateAvailableAryStr;
                        if($this->buyRateAvailableAryStr!='')
                        {
                            $buyRateAvailableAryStrId=str_replace(";",",",$this->buyRateAvailableAryStr);
                             $query="
                                    UPDATE
                                        ".__DBC_SCHEMATA_INSURANCE__."
                                    SET
                                        iBuyrateAvailable = '0' 
                                    WHERE
                                       id IN (".$buyRateAvailableAryStrId.")
                                "; 
                             //echo $query;
                             $result = $this->exeSQL($query);
                        }
                        $this->buyRateNotAvailableAryStr=$buyRateNotAvailableAryStr;
                        //$this->deleteInsuranceRate($iType,false,$idInsurance);
                    } 
                    return true;
                }
                else
                {
                    return false;
                } 
            }
	}
        
	function deleteInsuranceRate($iType,$idInsurateRate=false,$idInsurance=false)
	{
            if($iType>0)
            {
                /* 
                * If we delete buy rate then we have to also deleting corresponding sell rate.
                */
                if($iType==1 && $idInsurance>0)
                {  
                    $query_and = " idInsurance = '".(int)$idInsurance."' ";
                }
                else if($idInsurateRate>0)
                { 
                    $query_and ="   
                        id='".(int)$idInsurateRate."' 
                    AND
                        iInsuranceType = '".(int)$iType."'  
                    ";
                }
                else
                {
                    return false;
                }
                
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_INSURANCE_RATES__."
                    SET
                        isDeleted = '1', 
                        dtUpdated=now() 
                    WHERE
                        $query_and
                "; 
                //echo $query ;
                //die;
                if($result = $this->exeSQL($query))
                {
                    return true;
                }
                else
                {
                    return false;
                } 
            }
	}
	
	function getLargestInsuranceBuyRates($iType,$ret_details=false,$idInsurance=false)
	{
            if($iType>0)
            {
                if($idInsurance>0)
                {
                    $query_and = " AND idInsurance = '".(int)$idInsurance."' ";
                }
                
                $query="
                    SELECT
                        max(fInsurancePriceUSD) as fMaxInsurancePriceUSD,
                        fInsuranceMinPriceUSD as fMaxInsuranceMinPriceUSD,
                        id
                    FROM
                        ".__DBC_SCHEMATA_INSURANCE_RATES__."
                    WHERE
                        iInsuranceType = '".$iType."'
                    AND
                        isDeleted = '0' 
                    $query_and
                ";
               // echo $query ;
                if($result=$this->exeSQL($query))
                { 
                    $row = $this->getAssoc($result);  
                    if($ret_details)
                    {
                        return $row ;  
                    }
                    else
                    {
                        return $row['fMaxInsurancePriceUSD'] ;  	
                    }

                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
        function getLargestInsuranceValue($ret_details=false)
	{ 
            $query="
                SELECT
                    max(fInsurancePriceUSD) as fMaxInsurancePriceUSD,
                    fInsuranceMinPriceUSD as fMaxInsuranceMinPriceUSD,
                    id
                FROM
                    ".__DBC_SCHEMATA_INSURANCE__."
                WHERE 
                    isDeleted = '0' 
            ";

            if($result=$this->exeSQL($query))
            { 
                $row = $this->getAssoc($result);  
                if($ret_details)
                {
                    return $row ;  
                }
                else
                {
                    return $row['fMaxInsurancePriceUSD'] ;  	
                }

            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
	}
	
	function getNearestInsuranceSellRate($iType,$fTotalInsuredAmount,$idInsurance=false)
	{
            if($iType>0)
            {
                if($idInsurance>0)
                {
                    $query_and = " AND ir.idInsurance = '".(int)$idInsurance."' ";
                }
                
                $query="
                    SELECT
                        ir.fInsurancePriceUSD as fMaxInsurancePriceUSD,
                        ir.id,
                        ir.fInsuranceUptoPrice,
                        ir.idInsuranceCurrency,
                        (SELECT rate.fUsdValue FROM ".__DBC_SCHEMATA_EXCHANGE_RATE__." rate WHERE rate.idCurrency = ir.idInsuranceCurrency ORDER BY rate.id DESC LIMIT 0,1) as fLatestInsuranceExchangeRate
                    FROM
                        ".__DBC_SCHEMATA_INSURANCE_RATES__." ir 
                    WHERE
                        ir.iInsuranceType = '".$iType."' 
                    AND
                        ir.isDeleted = '0' 
                        $query_and 
                    ORDER BY
                        ir.fInsuranceUptoPrice ASC 
                ";
                /*
                * HAVING (fMaxInsurancePriceUSD >= '".(int)$fTotalInsuredAmount."')
                */
                //echo $query ; 
                if($result=$this->exeSQL($query))
                { 
                    while($row = $this->getAssoc($result))
                    {
                        /*
                        * Converting Insurance Upto Price to USD
                        */
                        $fMaxInsurancePriceUSD = $row['fInsuranceUptoPrice'] * $row['fLatestInsuranceExchangeRate'];
                        if($fMaxInsurancePriceUSD>=$fTotalInsuredAmount)
                        { 
                            return $row;
                        }
                    } 
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
        function getNearestInsuranceSellRate_backup($iType,$fTotalInsuredAmount,$idInsurance=false)
	{
            if($iType>0)
            {
                if($idInsurance>0)
                {
                    $query_and = " AND idInsurance = '".(int)$idInsurance."' ";
                }
                
                $query="
                    SELECT
                        fInsurancePriceUSD as fMaxInsurancePriceUSD,
                        id
                    FROM
                        ".__DBC_SCHEMATA_INSURANCE_RATES__."
                    WHERE
                        iInsuranceType = '".$iType."' 
                    AND
                        isDeleted = '0' 
                        $query_and
                    HAVING
                        (fMaxInsurancePriceUSD >= '".(int)$fTotalInsuredAmount."')
                    ORDER BY
                        fInsurancePriceUSD ASC
                    LIMIT
                        0,1
                ";
                //echo $query ; 
                if($result=$this->exeSQL($query))
                { 
                    $row = $this->getAssoc($result);   
                    return $row ;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
	
	function updateInsuranceRates($data,$idInsuranceRates)
	{ 
            if(!empty($data))
            { 
                $this->validateInsuranceRate($data);
                $this->set_id(sanitize_all_html_input(trim($idInsuranceRates)));
                
                if($this->error==true)
                {
                    return false;
                } 
                 
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_INSURANCE_RATES__."
                    SET
                        fInsuranceUptoPrice = '".mysql_escape_custom(trim($this->fInsuranceUptoPrice))."',
                        idInsuranceCurrency = '".mysql_escape_custom(trim($this->idInsuranceCurrency))."',
                        szInsuranceCurrency = '".mysql_escape_custom(trim($this->szInsuranceCurrency))."',
                        fInsuranceExchangeRate = '".mysql_escape_custom(trim($this->fInsuranceExchangeRate))."',
                        fInsuranceRate = '".mysql_escape_custom(trim($this->fInsuranceRate))."',
                        fInsuranceMinPrice = '".mysql_escape_custom(trim($this->fInsuranceMinPrice))."',
                        idInsuranceMinCurrency = '".mysql_escape_custom(trim($this->iInsuranceMinCurrency))."',
                        szInsuranceMinCurrency = '".mysql_escape_custom(trim($this->szInsuranceMinCurrency))."',
                        fInsuranceMinExchangeRate = '".mysql_escape_custom(trim($this->fInsuranceMinExchangeRate))."',
                        fInsurancePriceUSD = '".mysql_escape_custom(trim($this->fInsurancePriceUSD))."',
                        fInsuranceMinPriceUSD = '".mysql_escape_custom(trim($this->fInsuranceMinPriceUSD))."',
                        isWorking = '0',    
                        dtUpdated = now()
                    WHERE
                        id='".(int)$this->id."' 
                ";
         	//echo "<br>".$query ;
                //die;
                if($result = $this->exeSQL($query))
                { 
                    return true;
                }
                else
                {
                        return false;
                } 
            }
	}
	
	function sendInsuranceEmailToVendor($data,$mode=false)
	{  
            if(!empty($data))
            { 
                $this->szEmail = $data['szInsuranceVendorEmail'];
                $pipelineAry = array();
                $pipelineAry = explode("XXXX",$data['idBookingPipeLine']); 

                $kBooking = new cBooking();

                $newInsuredBookingAry = array();
                $newInsuredBookingAry = $kBooking->getAllNewBookingWithInsurance(0,$pipelineAry,true,true);  
                $cancelledInsuranceAry = array();
                
                if(!empty($newInsuredBookingAry))
                {				
                    $szBookingReferenceList = "<ul>" ;
                    foreach($newInsuredBookingAry as $newInsuredBookingArys)
                    {
                        $szBookingReferenceList .= "<li>".$newInsuredBookingArys['szBookingRef']."</li>"; 
                        
                        if($newInsuredBookingArys['iIncludeOnNewInsuranceScreen']==1)
                        {
                            $cancelledInsuranceAry[] = $newInsuredBookingArys['id'];
                        }
                        else
                        {
                            $newInsuranceBookingAry[] = $newInsuredBookingArys['id'];
                        }
                    }  
                    $szBookingReferenceList .= "</ul>" ; 
                    
                    $szBookingIds = trim($data['idBookingPipeLine']);
                    $szFileName = $this->downloadInsuredBooking($newInsuredBookingAry);   
                    $fTotalInsuranceBuyRate = $this->fTotalInsuranceBuyRate;
                    
                    $szTotalInsuranceBuy = "";
                    if($fTotalInsuranceBuyRate>0)
                    {
                        $szTotalInsuranceBuy = number_format_custom((float)$fTotalInsuranceBuyRate,2);
                    } 
                    //echo $szTotalInsuranceBuy; die;
                    if($mode=='PREVIEW')
                    {
                        return $szFileName;
                    }
                    $idAdmin = $_SESSION['admin_id'];

                    $query ="
                        INSERT INTO
                            ".__DBC_SCHEMATA_INSURANCE_VENDOR_EMAIL_LOGS__."
                        (
                            idAdmin,
                            szEmail,
                            szFileName,
                            szBookingIds,
                            dtCreatedOn,
                            iActive
                        )
                        VALUES
                        (
                            '".(int)$idAdmin."',
                            '".mysql_escape_custom(trim($this->szEmail))."',
                            '".mysql_escape_custom(trim($szFileName))."',
                            '".mysql_escape_custom(trim($szBookingIds))."',
                            now(),
                            1
                        )
                    ";			
                    //echo "<br>".$query."<br> ";
                    //die;

                    if($result = $this->exeSQL($query))
                    { 
                        if(!empty($cancelledInsuranceAry))
                        {
                            $this->updateCancelledInsuranceFlag($cancelledInsuranceAry,2);
                        }
                        $this->updateInsuranceStatus($newInsuranceBookingAry,__BOOKING_INSURANCE_STATUS_SENT__); 
                        
                        if(__ENVIRONMENT__ == "LIVE")
                        {
                            setcookie("__INSURANCE_VENDOR_EMAIL__", $this->szEmail, time()+(3600*24*90),'/',$_SERVER['HTTP_HOST'],true);
                        }
                        else
                        {
                            setcookie("__INSURANCE_VENDOR_EMAIL__", $this->szEmail, time()+(3600*24*90),'/');
                        }

                        $ret_ary= array(); 
                        //$ret_ary['szBookingReferenceList'] = $szBookingReferenceList ; 
                        $ret_ary['szEmail'] = $this->szEmail ; 
                        $ret_ary['szTotalInsuranceBuy'] = $szTotalInsuranceBuy;
                        $szAttachmentFileName = $szFileName ;

                        $kWhsSearch = new cWHSSearch();
                        $szFinanceEmail = $kWhsSearch->getManageMentVariableByDescription('__FINANCE_CONTACT_EMAIL__');

                        $recipientAry = array();
                        $recipientAry[0] = $this->szEmail ;
                        $recipientAry[1] = $szFinanceEmail ;
                        createEmail(__SEND_INSURANCE_VENDOR_EMAIL__, $ret_ary,$recipientAry, '', __STORE_SUPPORT_EMAIL__,$kUser->id, $szFinanceEmail,__FLAG_FOR_CUSTOMER__,true,0,$szAttachmentFileName,false,false,false,false,'INSURANCE_VENDOR');
                        return true;
                    }
                    else
                    {
                            return false;
                    } 
                }
            }
	}
        function updateCancelledInsuranceFlag($idBookingAry,$iInsuranceStatus,$iConfirmedFlag=false)
	{
            if(!empty($idBookingAry))
            {
                $idBookingStr = implode(",",$idBookingAry); 
                $updateQuery='';
                if($iConfirmedFlag){
                $updateQuery =",iInsuranceCancelledAfterSent='0'";
                }
                $query="
                    UPDATE	
                        ".__DBC_SCHEMATA_BOOKING__."
                    SET
                        iIncludeOnNewInsuranceScreen = '".(int)$iInsuranceStatus."'
                        $updateQuery
                    WHERE
                        id IN (".$idBookingStr.")
                ";	
                //echo "<br>".$query."<br>";
                //die;
                if($result = $this->exeSQL($query))
                {
                    return true;
                } 
                else
                {
                    return false;
                }
            }
        }
	
	function updateInsuranceStatus($idBookingAry,$iInsuranceStatus,$iCheckValueFlagAry=array())
	{
            if(!empty($idBookingAry))
            { 
                $kBooking = new cBooking();
                $query_update = '';
                $iConfirmedFlag=false;
                if($iInsuranceStatus==__BOOKING_INSURANCE_STATUS_CONFIRMED__)
                { 
                    $t=0;
                    if(!empty($idBookingAry))
                    {
                        foreach($idBookingAry as $idBookingArys)
                        {
                            $counter=count($iCheckValueFlagNewArys[$idBookingArys]);
                            $iCheckValueFlagNewArys[$idBookingArys][$counter]=$iCheckValueFlagAry[$t];
                            ++$t;
                        }
                    }
                    $iConfirmedFlag=true;
                    $newInsuredBookingAry = array();
                    $newInsuredBookingAry = $kBooking->getAllNewBookingWithInsurance(0,$idBookingAry,true,false);  
                    $cancelledInsuranceAry = array();
                    
                    
                    if(!empty($newInsuredBookingAry))
                    {				
                        $szBookingReferenceList = "<ul>" ;
                       
                        
                        foreach($newInsuredBookingAry as $newInsuredBookingArys)
                        { 
                             if($newInsuredBookingArys['iIncludeOnNewInsuranceScreen']==2 || $newInsuredBookingArys['iInsuranceCancelledAfterSent']=='1')
                            {
                                $cancelledInsuranceAry[] = $newInsuredBookingArys['id'];
                            }
                            else
                            {
                                $newInsuranceBookingAry[] = $newInsuredBookingArys['id'];
                            }
                            
                        } 
                    }
                }
                else
                {
                    $newInsuranceBookingAry = $idBookingAry ;
                }
                
              
                if(!empty($newInsuranceBookingAry))
                {
                    $idBookingStr = implode(",",$newInsuranceBookingAry);  
                    
                    $query="
                        UPDATE	
                            ".__DBC_SCHEMATA_BOOKING__."
                        SET
                            iInsuranceStatus = '".(int)$iInsuranceStatus."', 
                            dtInsuranceStatusUpdated = now() 
                        WHERE
                            id IN (".$idBookingStr.")
                    ";	
                    
                    if($result = $this->exeSQL($query))
                    {
                        if($iInsuranceStatus==__BOOKING_INSURANCE_STATUS_CONFIRMED__)
                        {
                            $ret_ary = array();
                            $ret_ary = $this->getTotalInsuranceBuyPrice($idBookingAry);

                            $fTotalInsuranceBuyPrice = $ret_ary['fTotalInsuranceBuyPrice'];
                            $szBookingRefStr = $ret_ary['szBookingRefStr'];

                            $addWorkingCapitalAry = array();
                            $addWorkingCapitalAry['fPriceUSD'] = $fTotalInsuranceBuyPrice;
                            $addWorkingCapitalAry['szAmountType'] = 'Insurance Amount Transferred' ;
                            $addWorkingCapitalAry['iDebitCredit'] = 2;
                            $addWorkingCapitalAry['szReference'] = $szBookingRefStr ;

                            $kDashBoardAdmin = new cDashboardAdmin();
                            $kDashBoardAdmin->addGraphWorkingCapitalCurrent($addWorkingCapitalAry);  
                        } 
                    } 
                }   
                
                
                if(!empty($cancelledInsuranceAry))
                {
                    foreach($cancelledInsuranceAry as $cancelledInsuranceArys)
                    {
                        $cancelArr[0]=$cancelledInsuranceArys;
                        $counter=count($iCheckValueFlagNewArys[$cancelledInsuranceArys]);
                        if((int)$counter==2)
                        {
                            $this->updateCancelledInsuranceFlag($cancelArr,0,true);
                        }else
                        {
                           
                            if($iCheckValueFlagNewArys[$cancelledInsuranceArys][0]==3)
                            {
                                $this->updateCancelledInsuranceFlag($cancelArr,0,true);
                            }
                            else if($iCheckValueFlagNewArys[$cancelledInsuranceArys][0]==5)
                            {
                                $this->updateCancelledInsuranceFlag($cancelArr,2,true);
                            }
                            else if($iCheckValueFlagNewArys[$cancelledInsuranceArys][0]==2)
                            {
                                $this->updateCancelledInsuranceFlag($cancelArr,0,true);
                            }
                            else if($iCheckValueFlagNewArys[$cancelledInsuranceArys][0]==4)
                            {
                                $this->updateCancelledInsuranceFlag($cancelArr,0,false);
                            }
                        }
                        
                        
                    }
                }
                
                if($iInsuranceStatus==__BOOKING_INSURANCE_STATUS_CONFIRMED__)
                {
                    $bookingCancelledAry = array();  
                    $bookingCancelledAry = $this->getAllBookingInsuranceCancalledAfterConfirm();

                    if(!empty($bookingCancelledAry))
                    { 
                        $fTotalInsuranceBuyPrice = $bookingCancelledAry['fTotalInsuranceBuyPrice'];
                        $szBookingRefStr = $bookingCancelledAry['szBookingRefStr'];

                        $addWorkingCapitalAry = array();
                        $addWorkingCapitalAry['fPriceUSD'] = $fTotalInsuranceBuyPrice;
                        $addWorkingCapitalAry['szAmountType'] = 'Insurance Amount Refunded' ;
                        $addWorkingCapitalAry['iDebitCredit'] = 1;
                        $addWorkingCapitalAry['szReference'] = $szBookingRefStr ;

                        $kDashBoardAdmin = new cDashboardAdmin();
                        $kDashBoardAdmin->addGraphWorkingCapitalCurrent($addWorkingCapitalAry); 
                        $this->updateInsuranceCancelledFlag(0); 
                    } 
                }
                return true ; 
            } 
	}
        
        
	function updateInsuranceCancelledFlag($iValue)
	{ 
            $query="	
                UPDATE
                    ".__DBC_SCHEMATA_BOOKING__."
                SET
                    iInsuranceCancelledAfterConfirm = '".(int)$iValue."',
                    dtUpdatedOn = now()
                WHERE
                    iInsuranceCancelledAfterConfirm = '1';
            ";
            //echo $query ;
            if($result=$this->exeSQL($query))
            {
                return true;
            }
            else
            {
                //echo " not updated ";
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
	}
        function getAllBookingInsuranceCancalledAfterConfirm()
        {  
            $query="
                SELECT 
                    fTotalInsuranceCostForBookingCustomerCurrency_buyRate,
                    fCustomerExchangeRate,
                    szBookingRef
                FROM 
                    ".__DBC_SCHEMATA_BOOKING__."
                WHERE
                    iInsuranceCancelledAfterConfirm = '1' 
                AND
                    iInsuranceIncluded = '1' 
            ";
            //echo $query;
            if($result = $this->exeSQL( $query ))
            {  
                if($this->iNumRows>0)
                { 
                    $fTotalInsuranceBuyPrice = 0;
                    $szBookingRefStr = '';
                    while($row=$this->getAssoc($result))
                    {	
                        $fTotalInsuranceBuyPrice += $row['fTotalInsuranceCostForBookingCustomerCurrency_buyRate'] * $row['fCustomerExchangeRate'] ;
                        $szBookingRefStr .= $row['szBookingRef'].", ";
                    }

                    $ret_ary = array();
                    $ret_ary['fTotalInsuranceBuyPrice'] = $fTotalInsuranceBuyPrice ;
                    $ret_ary['szBookingRefStr'] = $szBookingRefStr ; 
                    return $ret_ary; 
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return array();
            } 
        }
        
        function getTotalInsuranceBuyPrice($idBookingAry)
        {
            if(!empty($idBookingAry))
            {
                $idBookingStr = implode(",",$idBookingAry);
                
                $query="
                    SELECT 
                        fTotalInsuranceCostForBookingCustomerCurrency_buyRate,
                        fCustomerExchangeRate,
                        szBookingRef
                    FROM 
                        ".__DBC_SCHEMATA_BOOKING__."
                    WHERE
                        id IN (".$idBookingStr.")
                    AND
                        iInsuranceIncluded = '1' 
                ";
               // echo $query;
                if($result = $this->exeSQL( $query ))
                {  
                    $ret_ary = array();
                    if($this->iNumRows>0)
                    { 
                        $fTotalInsuranceBuyPrice = 0;
                        $szBookingRefStr = '';
                        while($row=$this->getAssoc($result))
                        {	
                            $fTotalInsuranceBuyPrice += $row['fTotalInsuranceCostForBookingCustomerCurrency_buyRate'] * $row['fCustomerExchangeRate'] ;
                            $szBookingRefStr .= $row['szBookingRef'].", ";
                        }

                        $ret_ary = array();
                        $ret_ary['fTotalInsuranceBuyPrice'] = $fTotalInsuranceBuyPrice ;
                        $ret_ary['szBookingRefStr'] = $szBookingRefStr ;
                    } 
                    return $ret_ary; 
                }
                else
                {
                    return array();
                }
            }
        }
	function downloadInsuredBooking($newInsuredBookingAry,$test_page=false)
	{   
            $kBooking = new cBooking(); 
            require_once( __APP_PATH_CLASSES__ . "/PHPExcel.php" );

            $objPHPExcel=new PHPExcel();


            $sheetIndex=0;
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->createSheet();
            $objPHPExcel->setActiveSheetIndex($sheetIndex);
            $sheet = $objPHPExcel->getActiveSheet(); 

            $styleArray = array(
                    'font' => array(
                            'bold' => false,
                            'size' =>12,
                    ),
                    'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    ),
                    'borders' => array(
                            'top' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'bottom' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'right' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'left' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                    ),
            );

            $styleArray1 = array(
                    'font' => array(
                            'bold' => false,
                            'size' =>12,
                    ),
                    'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    ),
                    'borders' => array(
                            'top' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'bottom' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'right' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'left' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                    ),
            ); 

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15); 
            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15); 
            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(15); 
            $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(15); 
            $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(15); 

            $objPHPExcel->getActiveSheet()->getStyle('A1:R1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A1:R1')->getFill()->getStartColor()->setARGB('FFddd9c3');
            //$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);

            $objPHPExcel->getActiveSheet()->setCellValue('A1','Booking Date')->getStyle('A1');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray); 

            $objPHPExcel->getActiveSheet()->setCellValue('B1','Reference')->getStyle('B1');
            $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('C1','Insured party')->getStyle('C1');
            $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('D1','Type of goods')->getStyle('P1');
            $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setCellValue('E1','Value (DKK)')->getStyle('D1');
            $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('F1','Volume (cbm)')->getStyle('E1');
            $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('G1','Weight (kg)')->getStyle('F1');
            $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('H1','Mode')->getStyle('G1');
            $objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('I1','Incoterm')->getStyle('H1');
            $objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('J1','From Country')->getStyle('I1');
            $objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('K1','From City')->getStyle('J1');
            $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('L1','ETD')->getStyle('K1');
            $objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('M1','To Country')->getStyle('L1');
            $objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('N1','To City')->getStyle('M1');
            $objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('O1','Delivery')->getStyle('N1');
            $objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('P1','Forwarder')->getStyle('O1');
            $objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray($styleArray);
               
            $objPHPExcel->getActiveSheet()->setCellValue('Q1','Rate')->getStyle('P1');
            $objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray($styleArray);
             
            $objPHPExcel->getActiveSheet()->setCellValue('R1','Price (DKK)')->getStyle('Q1');
            $objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray($styleArray);
            
            $kWarehouseSearch = new cWHSSearch();
            $resultAry = $kWarehouseSearch->getCurrencyDetails(20);		

            $kVatApplication = new cVatApplication();
            $cargoTypeAry = array();
            $cargoTypeAry =  $kVatApplication->getManualFeeCaroTypeList(true);
            
            $fDkkExchangeRate = 0;
            if($resultAry['fUsdValue']>0)
            {
                $fDkkExchangeRate = $resultAry['fUsdValue'] ;						 
            }    
            $pipelineAry = array();
 
            
            $iTotalActiveBooking = 0;
            $fSumofColumnEActiveBooking = 0;
            $fSumofColumnFActiveBooking = 0;
            $fSumofColumnGActiveBooking = 0;
            $fSumofColumnRActiveBooking = 0;
            
            $iTotalCancelledBooking = 0;
            $fSumofColumnECancelledBooking = 0;
            $fSumofColumnFCancelledBooking = 0;
            $fSumofColumnGCancelledBooking = 0;
            $fSumofColumnRCancelledBooking = 0;
            
            $col = 2;
            $kConfig = new cConfig(); 
            foreach($newInsuredBookingAry as $newInsuredBookingArys)
            {   
                if($newInsuredBookingArys['iIncludeOnNewInsuranceScreen']==1)
                {
                    $pipelineAry[] = $newInsuredBookingArys['id'];
                }                    
                else
                { 
                    $szServiceType='';
                    if($newInsuredBookingArys['iQuotesStatus']==__BOOKING_QUOTES_STATUS_WON__)
                    {
                        $serviceTypeAry = array();
                        //$serviceTypeAry = $kConfig->getConfigurationData(__DBC_SCHEMATA_SERVICE_TYPE__,$newInsuredBookingArys['idServiceType']); 
                        $kConfig = new cConfig();
                        $configLangArr=$kConfig->getConfigurationLanguageData('__TABLE_SERVICE_TYPE__');
                        $szServiceType = $configLangArr[$newInsuredBookingArys['idServiceType']]['szShortName'];  
                    }
                    else
                    {
                        if($newInsuredBookingArys['idServiceType']==__SERVICE_TYPE_DTD__) //DTD
                        {
                            $szServiceType = "EXW" ;
                        }
                        if($newInsuredBookingArys['idServiceType']==__SERVICE_TYPE_PTD__) //DTW
                        {
                            $szServiceType = "FOB" ;
                        } 
                    } 
                     
                    $fBookingDkkExchangeRate = 0;
                    if($newInsuredBookingArys['fDkkExchangeRate']>0)
                    {
                        $szGoodsValue = round((float)($newInsuredBookingArys['fTotalAmountInsured']/$newInsuredBookingArys['fDkkExchangeRate']));  
                        $fBookingDkkExchangeRate = $newInsuredBookingArys['fDkkExchangeRate'];
                    } 
                    else if($fDkkExchangeRate>0)
                    {
                        $szGoodsValue = round((float)($newInsuredBookingArys['fTotalAmountInsured']/$fDkkExchangeRate));  
                        $fBookingDkkExchangeRate = $fDkkExchangeRate;
                    }
                    else
                    {
                        $szGoodsValue = 0;  
                    }
                    
                    if($newInsuredBookingArys['idCurrency']==20) // If customer currency is also DKK then no need to convert price
                    {
                        $fInsuranceBuyrate = round((float)$newInsuredBookingArys['fTotalInsuranceCostForBookingCustomerCurrency_buyRate']);
                    }
                    else
                    {
                       if($fBookingDkkExchangeRate>0)
                       {
                            $fInsuranceBuyrate = round((float)($newInsuredBookingArys['fTotalInsuranceCostForBookingUSD_buyRate']/$fBookingDkkExchangeRate));  
                       } 
                    }
                    
                    /*
                     *  if($newInsuredBookingArys['iMinrateApplied']==1)
                        {
                            $szInsuranceRate = 'Min';
                        }
                        else
                     */
                    if((int)$newInsuredBookingArys['fInsuranceMinPrice_buyRate']==(int)$newInsuredBookingArys['fTotalInsuranceCostForBooking_buyRate'])
                    {
                        $szInsuranceRate = 'Min';
                    }
                    else
                    {
                        $szInsuranceRate = round((float)$newInsuredBookingArys['fInsuranceRate_buyRate'],2)."%";
                    } 
                    if(!empty($newInsuredBookingArys['szTransportMode']))
                    {
                        $szTransportMode = $newInsuredBookingArys['szTransportMode'];
                    }
                    else
                    {
                        $szTransportMode = __TRANSPORTECA_DEFAULT_TRANSPORT_MODE__;
                    }
                    
                    if($newInsuredBookingArys['isMoving']==1)
                    {
                        $szMovingText = 'Yes';
                    }
                    else
                    {
                        $szMovingText = 'No';
                    } 
                    if(!empty($newInsuredBookingArys['szCargoType']))
                    {
                        $szCargoType = $cargoTypeAry[$newInsuredBookingArys['szCargoType']]['szName'];
                    }
                    else
                    {
                        $szCargoType = $cargoTypeAry["__GENERAL_CODE__"]['szName'];
                    } 
                    $szCustomerCompanyName = trim($newInsuredBookingArys['szCustomerCompanyName']);
                    
                    if(empty($szCustomerCompanyName))
                    {
                        $szCustomerCompanyName = $newInsuredBookingArys['szFirstName']." ".$newInsuredBookingArys['szLastName'];
                    }
                    $objPHPExcel->getActiveSheet()->setCellValue("A".$col,date('Y-m-d',strtotime($newInsuredBookingArys['dtBookingConfirmed'])));
                    $objPHPExcel->getActiveSheet()->setCellValue("B".$col,$newInsuredBookingArys['szBookingRef']);
                    $objPHPExcel->getActiveSheet()->setCellValue("C".$col,$szCustomerCompanyName);
                    $objPHPExcel->getActiveSheet()->setCellValue("D".$col,$szCargoType);
                    $objPHPExcel->getActiveSheet()->setCellValue("E".$col,$szGoodsValue);

                    $volume=strval(round((float)$newInsuredBookingArys['fCargoVolume'],3)); 
                    $fCargoWeight = $newInsuredBookingArys['fCargoWeight'];
                    
                    $objPHPExcel->getActiveSheet()->setCellValue("F".$col,$volume);
                    $objPHPExcel->getActiveSheet()->setCellValue("G".$col,$fCargoWeight);  

                    if($test_page)
                    {
                        $newInsuredBookingArys['szShipperCity'] = $newInsuredBookingArys['szOriginCity'];
                    }
                    else
                    {
                        $newInsuredBookingArys['szShipperCity'] = $newInsuredBookingArys['szShipperCity'];
                    }

                    $objPHPExcel->getActiveSheet()->setCellValue("H".$col,$szTransportMode);
                    $objPHPExcel->getActiveSheet()->setCellValue("I".$col,$szServiceType); 
                    $objPHPExcel->getActiveSheet()->setCellValue("J".$col,$kBooking->findCountryName($newInsuredBookingArys['idOriginCountry']));
                    $objPHPExcel->getActiveSheet()->setCellValue("K".$col,$newInsuredBookingArys['szShipperCity']);

                    if(!empty($newInsuredBookingArys['dtCutOff']) && $newInsuredBookingArys['dtCutOff']!='0000-00-00 00:00:00')
                    {
                        $dtCutOff = date('Y-m-d',strtotime($newInsuredBookingArys['dtCutOff'])) ;
                    }
                    else if(!empty($newInsuredBookingArys['dtTimingDate']) && $newInsuredBookingArys['dtTimingDate']!='0000-00-00 00:00:00')
                    {
                        /*
                        * If dtCutOff is null then we are displaying Date of shipment as ETD
                        */
                        $dtCutOff = date('Y-m-d',strtotime($newInsuredBookingArys['dtTimingDate'])) ;
                    } 
                    else
                    {
                        $dtCutOff = "";
                    }
                    
                    if(!empty($newInsuredBookingArys['dtAvailable']) && $newInsuredBookingArys['dtAvailable']!='0000-00-00 00:00:00')
                    {
                        $dtAvailable = date('Y-m-d',strtotime($newInsuredBookingArys['dtAvailable']));
                    }
                    else
                    {
                        $dtAvailable = " ";
                    }  
                    if($test_page)
                    {
                        $newInsuredBookingArys['szConsigneeCity'] = $newInsuredBookingArys['szDestinationCity'];
                    }
                    else
                    {
                        $newInsuredBookingArys['szConsigneeCity'] = $newInsuredBookingArys['szConsigneeCity'];
                    } 
                    
                    $objPHPExcel->getActiveSheet()->setCellValue("L".$col,$dtCutOff);			
                    $objPHPExcel->getActiveSheet()->setCellValue("M".$col,$kBooking->findCountryName($newInsuredBookingArys['idDestinationCountry']));
                    $objPHPExcel->getActiveSheet()->setCellValue("N".$col,$newInsuredBookingArys['szConsigneeCity']);
                    $objPHPExcel->getActiveSheet()->setCellValue("O".$col,$dtAvailable); 
                    $objPHPExcel->getActiveSheet()->setCellValue("P".$col,$newInsuredBookingArys['szForwarderDispName']);
                    
                    $objPHPExcel->getActiveSheet()->setCellValue("Q".$col,$szInsuranceRate);
                    $objPHPExcel->getActiveSheet()->setCellValue("R".$col,$fInsuranceBuyrate);
                    $col++;
                    
                    $iTotalActiveBooking++;
                    $fSumofColumnEActiveBooking += $szGoodsValue;
                    $fSumofColumnFActiveBooking += $volume;
                    $fSumofColumnGActiveBooking += $fCargoWeight;
                    $fSumofColumnRActiveBooking += $fInsuranceBuyrate;
                }
            } 
            
            if(!empty($pipelineAry))
            { 
                $newInsuredBookingAry = array();
                $newInsuredBookingAry = $kBooking->getAllNewBookingWithInsurance(0,$pipelineAry,true,true);  

                $szCancelledPartHeading = "Cancelled bookings already invoiced - please deduct these from next invoice";
                $col++;
                $objPHPExcel->getActiveSheet()->setCellValue("A".$col,$szCancelledPartHeading);
                $col++;
                
                if(!empty($newInsuredBookingAry))
                {
                    foreach($newInsuredBookingAry as $newInsuredBookingArys)
                    {   
                        $szServiceType='';
                        if($newInsuredBookingArys['iQuotesStatus']==__BOOKING_QUOTES_STATUS_WON__)
                        {
                            $serviceTypeAry = array();
                            //$serviceTypeAry = $kConfig->getConfigurationData(__DBC_SCHEMATA_SERVICE_TYPE__,$newInsuredBookingArys['idServiceType']); 
                            $kConfig = new cConfig();
                            $configLangArr=$kConfig->getConfigurationLanguageData('__TABLE_SERVICE_TYPE__');
                            $szServiceType = $configLangArr[$newInsuredBookingArys['idServiceType']]['szShortName'];   
                        }
                        else
                        {
                            if($newInsuredBookingArys['idServiceType']==__SERVICE_TYPE_DTD__) //DTD
                            {
                                $szServiceType = "EXW" ;
                            }
                            if($newInsuredBookingArys['idServiceType']==__SERVICE_TYPE_PTD__) //DTW
                            {
                                $szServiceType = "FOB" ;
                            } 
                        } 
                        if($newInsuredBookingArys['fDkkExchangeRate']>0)
                        {
                            $szGoodsValue = round((float)($newInsuredBookingArys['fTotalAmountInsured']/$newInsuredBookingArys['fDkkExchangeRate'])); 
                        } 
                        else if($fDkkExchangeRate>0)
                        {
                            $szGoodsValue = round((float)($newInsuredBookingArys['fTotalAmountInsured']/$fDkkExchangeRate)); 
                        }
                        else
                        {
                            $szGoodsValue = 0; 
                        } 
                        if(!empty($newInsuredBookingArys['szTransportMode']))
                        {
                            $szTransportMode = $newInsuredBookingArys['szTransportMode'];
                        }
                        else
                        {
                            $szTransportMode = __TRANSPORTECA_DEFAULT_TRANSPORT_MODE__;
                        }
                        
                        if(!empty($newInsuredBookingArys['szCargoType']))
                        {
                            $szCargoType = $cargoTypeAry[$newInsuredBookingArys['szCargoType']]['szName'];
                        }
                        else
                        {
                            $szCargoType = $cargoTypeAry["__GENERAL_CODE__"]['szName'];
                        }
                        
                        $fBookingDkkExchangeRate = 0;
                        if($newInsuredBookingArys['fDkkExchangeRate']>0)
                        {
                            $fBookingDkkExchangeRate = $newInsuredBookingArys['fDkkExchangeRate'];
                        } 
                        else if($fDkkExchangeRate>0)
                        { 
                            $fBookingDkkExchangeRate = $fDkkExchangeRate;
                        }  
                        if($newInsuredBookingArys['idCurrency']==20) // If customer currency is also DKK then no need to convert price
                        {
                            $fInsuranceBuyrate = round((float)$newInsuredBookingArys['fTotalInsuranceCostForBookingCustomerCurrency_buyRate']);
                        }
                        else
                        {
                            if($fBookingDkkExchangeRate>0)
                            {
                                 $fInsuranceBuyrate = round((float)($newInsuredBookingArys['fTotalInsuranceCostForBookingUSD_buyRate']/$fBookingDkkExchangeRate));  
                            } 
                        }
                     
                        if((int)$newInsuredBookingArys['fInsuranceMinPrice_buyRate']==(int)$newInsuredBookingArys['fTotalInsuranceCostForBooking_buyRate'])
                        {
                            $szInsuranceRate = 'Min';
                        }
                        else
                        {
                            $szInsuranceRate = round((float)$newInsuredBookingArys['fInsuranceRate_buyRate'],2)."%";
                        } 
                    
                        $objPHPExcel->getActiveSheet()->setCellValue("A".$col,date('Y-m-d',strtotime($newInsuredBookingArys['dtBookingConfirmed'])));
                        $objPHPExcel->getActiveSheet()->setCellValue("B".$col,$newInsuredBookingArys['szBookingRef']);
                        $objPHPExcel->getActiveSheet()->setCellValue("C".$col,$newInsuredBookingArys['szCustomerCompanyName']);
                        $objPHPExcel->getActiveSheet()->setCellValue("D".$col,$szCargoType);
                        $objPHPExcel->getActiveSheet()->setCellValue("E".$col,$szGoodsValue);
                        

                        $volume=strval(round((float)$newInsuredBookingArys['fCargoVolume'],3));
                        $fCargoWeight = $newInsuredBookingArys['fCargoWeight']; 
                        
                        $objPHPExcel->getActiveSheet()->setCellValue("F".$col,$volume);
                        $objPHPExcel->getActiveSheet()->setCellValue("G".$col,$newInsuredBookingArys['fCargoWeight']);  


                        $objPHPExcel->getActiveSheet()->setCellValue("H".$col,$szTransportMode);
                        $objPHPExcel->getActiveSheet()->setCellValue("I".$col,$szServiceType); 
                        $objPHPExcel->getActiveSheet()->setCellValue("J".$col,$kBooking->findCountryName($newInsuredBookingArys['idOriginCountry']));
                        $objPHPExcel->getActiveSheet()->setCellValue("K".$col,$newInsuredBookingArys['szShipperCity']);

                        if(!empty($newInsuredBookingArys['dtCutOff']) && $newInsuredBookingArys['dtCutOff']!='0000-00-00 00:00:00')
                        {
                            $dtCutOff = date('Y-m-d',strtotime($newInsuredBookingArys['dtCutOff'])) ;
                        }
                        else
                        {
                            $dtCutOff = "";
                        } 
                        if(!empty($newInsuredBookingArys['dtAvailable']) && $newInsuredBookingArys['dtAvailable']!='0000-00-00 00:00:00')
                        {
                            $dtAvailable = date('Y-m-d',strtotime($newInsuredBookingArys['dtAvailable']));
                        }
                        else
                        {
                            $dtAvailable = " ";
                        }

                        $objPHPExcel->getActiveSheet()->setCellValue("L".$col,$dtCutOff);			
                        $objPHPExcel->getActiveSheet()->setCellValue("M".$col,$kBooking->findCountryName($newInsuredBookingArys['idDestinationCountry']));
                        $objPHPExcel->getActiveSheet()->setCellValue("N".$col,$newInsuredBookingArys['szConsigneeCity']);
                        $objPHPExcel->getActiveSheet()->setCellValue("O".$col,$dtAvailable); 
                        $objPHPExcel->getActiveSheet()->setCellValue("P".$col,$newInsuredBookingArys['szForwarderDispName']); 
                        
                        $objPHPExcel->getActiveSheet()->setCellValue("Q".$col,$szInsuranceRate);
                        $objPHPExcel->getActiveSheet()->setCellValue("R".$col,$fInsuranceBuyrate);
                        $col++;
                        
                        $iTotalCancelledBooking++;
                        $fSumofColumnECancelledBooking += $szGoodsValue;
                        $fSumofColumnFCancelledBooking += $volume;
                        $fSumofColumnGCancelledBooking += $fCargoWeight;
                        $fSumofColumnRCancelledBooking += $fInsuranceBuyrate; 
                    }
                }
            }  
            $totalRowAry = array(); 
            $totalRowAry['fSumofColumnE'] = $fSumofColumnEActiveBooking - $fSumofColumnECancelledBooking;
            $totalRowAry['fSumofColumnF'] = $fSumofColumnFActiveBooking - $fSumofColumnFCancelledBooking;
            $totalRowAry['fSumofColumnG'] = $fSumofColumnGActiveBooking - $fSumofColumnGCancelledBooking;
            $totalRowAry['fSumofColumnR'] = $fSumofColumnRActiveBooking - $fSumofColumnRCancelledBooking;
            
            $this->fTotalInsuranceBuyRate = $totalRowAry['fSumofColumnR'];
            if(!empty($totalRowAry))
            {
                $col++; 
                $szTotalStr = $iTotalActiveBooking." new bookings";
                if($iTotalCancelledBooking>0)
                {
                    $szTotalStr .= ", ".$iTotalCancelledBooking." cancelled bookings";
                }
                $objPHPExcel->getActiveSheet()->setCellValue("A".$col,"Total");
                $objPHPExcel->getActiveSheet()->setCellValue("B".$col,$szTotalStr);
                $objPHPExcel->getActiveSheet()->setCellValue("E".$col,$totalRowAry['fSumofColumnE']);
                $objPHPExcel->getActiveSheet()->setCellValue("F".$col,$totalRowAry['fSumofColumnF']);
                $objPHPExcel->getActiveSheet()->setCellValue("G".$col,$totalRowAry['fSumofColumnG']);
                $objPHPExcel->getActiveSheet()->setCellValue("R".$col,$totalRowAry['fSumofColumnR']);
            }
            $title = "_Transporteca_Insurance";
            $objPHPExcel->getActiveSheet()->setTitle('Transporteca Insurance');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->removeSheetByIndex(1);
            $file = date('Ymdhs').$title.".xlsx";
            $this->szInsuranceSheetFileName = $file; 
            $fileName=__APP_PATH_ROOT__."/insuranceSheets/".$file; 
            $objWriter->save($fileName);  
            return $fileName; 
    }
	
    function reuseInsuranceProduct($data)
    {
        if(!empty($data))
        { 
            $this->set_idOriginCountry(sanitize_all_html_input(trim($data['idOriginCountry'])));
            $this->set_idDestinationCountry(sanitize_all_html_input(trim($data['idDestinationCountry'])));
            $this->set_idTransportMode(sanitize_all_html_input(trim($data['idTransportMode'])));
            $this->set_iPrivate(sanitize_all_html_input(trim($data['iPrivate'])));
            $this->set_idInsurance(sanitize_all_html_input(trim($data['idInsurance'])));

            if($this->error==true)
            {
                return false;
            }
            
            $kInsurance_new = new cInsurance();
            $kInsurance_new->getInsuranceDetails($data['idInsurance'],true); 
           
            $query="
               INSERT INTO
                   ".__DBC_SCHEMATA_INSURANCE__."
                (
                    idOriginCountry,
                    idDestinationCountry,
                    idTransportMode,
                    iPrivate,   
                    fInsuranceUptoPrice,
                    idInsuranceCurrency,
                    szInsuranceCurrency,
                    fInsuranceExchangeRate,
                    fInsuranceRate,
                    fInsuranceMinPrice,
                    idInsuranceMinCurrency,
                    szInsuranceMinCurrency,
                    fInsuranceMinExchangeRate,
                    fInsurancePriceUSD,
                    fInsuranceMinPriceUSD, 
                    dtCreatedOn,
                    iActive
                )
                VALUES
                (
                   '".mysql_escape_custom(trim($this->idOriginCountry))."', 
                   '".mysql_escape_custom(trim($this->idDestinationCountry))."',
                   '".mysql_escape_custom(trim($this->idTransportMode))."',
                   '".mysql_escape_custom(trim($this->iPrivate))."', 
                    '".mysql_escape_custom(trim($kInsurance_new->fInsuranceUptoPrice))."',
                    '".mysql_escape_custom(trim($kInsurance_new->idInsuranceCurrency))."',
                    '".mysql_escape_custom(trim($kInsurance_new->szInsuranceCurrency))."',
                    '".mysql_escape_custom(trim($kInsurance_new->fInsuranceExchangeRate))."',
                    '".mysql_escape_custom(trim($kInsurance_new->fInsuranceRate))."',
                    '".mysql_escape_custom(trim($kInsurance_new->fInsuranceMinPrice))."', 
                    '".mysql_escape_custom(trim($kInsurance_new->iInsuranceMinCurrency))."',
                    '".mysql_escape_custom(trim($kInsurance_new->szInsuranceMinCurrency))."',
                    '".mysql_escape_custom(trim($kInsurance_new->fInsuranceMinExchangeRate))."',
                    '".mysql_escape_custom(trim($kInsurance_new->fInsurancePriceUSD))."',
                    '".mysql_escape_custom(trim($kInsurance_new->fInsuranceMinPriceUSD))."',
                    NOW(),
                    '1'
                )
           ";  
           //echo "<br>".$query."<br>" ;  
           //die; 
           if($result = $this->exeSQL($query))
           {		 
                $idNewInsurance = $this->iLastInsertID ;   
                $this->idLastAddedInsurance = $idNewInsurance ;
                
                /* Copying Sell Rate data */ 
                $insuranceSellRateAry = array();
                $insuranceSellRateAry = $this->getInsuranceRates(__INSURANCE_SELL_RATE_TYPE__,$data['idInsurance']);
                if(!empty($insuranceSellRateAry))
                {
                    foreach($insuranceSellRateAry as $insuranceSellRateArys)
                    {
                        $kInsurance = new cInsurance();
                        $kInsurance->loadInsuranceRates(__INSURANCE_SELL_RATE_TYPE__,$insuranceSellRateArys['id']);
                        
                        if((int)$kInsurance->fInsuranceRate==0)
                        {
                            $isWorking = 1;
                        }
                        else
                        {
                            $isWorking = 0;
                        }
                        
                        $query="
                            INSERT INTO
                                ".__DBC_SCHEMATA_INSURANCE_RATES__."
                            (
                                iInsuranceType,
                                idInsurance,
                                fInsuranceUptoPrice,
                                idInsuranceCurrency,
                                szInsuranceCurrency,
                                fInsuranceExchangeRate,
                                fInsuranceRate,
                                fInsuranceMinPrice,
                                idInsuranceMinCurrency,
                                szInsuranceMinCurrency,
                                fInsuranceMinExchangeRate,
                                fInsurancePriceUSD,
                                fInsuranceMinPriceUSD,
                                isWorking,
                                dtCreated					
                            )
                            VALUES
                            (
                                '".mysql_escape_custom(trim($kInsurance->iInsuranceType))."',
                                '".(int)$idNewInsurance."',
                                '".mysql_escape_custom(trim($kInsurance->fInsuranceUptoPrice))."',
                                '".mysql_escape_custom(trim($kInsurance->idInsuranceCurrency))."',
                                '".mysql_escape_custom(trim($kInsurance->szInsuranceCurrency))."',
                                '".mysql_escape_custom(trim($kInsurance->fInsuranceExchangeRate))."',
                                '".mysql_escape_custom(trim($kInsurance->fInsuranceRate))."',
                                '".mysql_escape_custom(trim($kInsurance->fInsuranceMinPrice))."', 
                                '".mysql_escape_custom(trim($kInsurance->iInsuranceMinCurrency))."',
                                '".mysql_escape_custom(trim($kInsurance->szInsuranceMinCurrency))."',
                                '".mysql_escape_custom(trim($kInsurance->fInsuranceMinExchangeRate))."',
                                '".mysql_escape_custom(trim($kInsurance->fInsurancePriceUSD))."',
                                '".mysql_escape_custom(trim($kInsurance->fInsuranceMinPriceUSD))."', 
                                '".(int)$isWorking."',
                                NOW()
                            )
                        ";
                        //echo "<br>".$query."<br>" ;
                        //die; 
                        if($result = $this->exeSQL($query))
                        {			
                           //return true;
                        }
                        else
                        {
                            $this->error = true;
                            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                            return false;

                        }
                    }
                }
                return true;
           }
           else
           {
               $this->error = true;
               $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
               $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
               return false;

           } 
       }
    } 
    function getInsuranceDetailsFroBooking($data,$send_quick_quote=false)
    {
        if(!empty($data))
        {  
            if($send_quick_quote)
            {
                $postSearchAry = $data;
            }
            else
            {
                $postSearchAry = array();
                $postSearchAry['idOriginCountry'] = $data['idOriginCountry'];
                $postSearchAry['idDestinationCountry'] = $data['idDestinationCountry'];
                $postSearchAry['idTransportMode'] = $data['idTransportMode'] ;
                $postSearchAry['szCargoType'] = $data['szCargoType']; 
                $postSearchAry['fTotalPriceCustomerCurrency'] = 1;
                $postSearchAry['fExchangeRate'] = 1;
                $postSearchAry['idCurrency'] = 1; //USD
                $postSearchAry['szCurrency'] = 'USD';
            } 
            
            $insuranceDataAry = array();
            $insuranceDataAry['iInsurance'] = 1;
            if($data['fValueOfGoods']>0)
            {
                $insuranceDataAry['fValueOfGoods'] = $data['fValueOfGoods'];
                $insuranceDataAry['idInsuranceCurrency'] = $data['idInsuranceCurrency'];
            }
            else
            {
                $insuranceDataAry['fValueOfGoods'] = 1;
                $insuranceDataAry['idInsuranceCurrency'] = 1; //USD
            }   
            $this->calculateBookingInsurance($insuranceDataAry,$postSearchAry,"INSURANCE_AVAILABILITY_CHECK"); 
            
            if($this->iInsuranceValidationFailed==1)
            {
                $this->addError('szSpecialError','No insurance rates found');
                return false;
            }
            else if($this->iInsuranceOtherErrors==1)
            {
                $this->addError('szSpecialError',$this->arErrorMessages['fValueOfGoods_1']);
                return false;
            } 
            else
            {
                return true;
            }
        }
    }
    
    function getInsuranceDataForNewBooking($iInsuranceStatus=false)
    {
        if($iInsuranceStatus==__BOOKING_INSURANCE_STATUS_SENT__)
        {
            $kBooking = new cBooking();
            $newInsuredBookingAry = array();
            $newInsuredBookingAry = $kBooking->getAllNewBookingWithInsurance(__BOOKING_INSURANCE_STATUS_SENT__);

            /* If the insurance was in the Sent or Confirmed screen when it was cancelled (regardless of whether it was cancelled from SERVICES - Insurance screens or from BOOINGS  screen, we should make it also show with a line in the screen SERVICES - Insurance - New bookings, but in the Sell, Buy, and GP column type the numbers negative, and instead of the name in the customer column, simply type "Cancelled"  
            * fetching all cancelled record for new Insurance Booking
            */

            $newCancelledInsuredBookingAry = array();
            $newCancelledInsuredBookingAry = $kBooking->getAllNewBookingWithInsurance(false,false,true,false,2); 
            if(!empty($newCancelledInsuredBookingAry))
            {
                $newInsuredBookingAry = array_merge($newInsuredBookingAry,$newCancelledInsuredBookingAry);
            }  
            $final_ret_ary = array(); 
            $final_ret_ary = sortArray($newInsuredBookingAry,'dtBookingConfirmed','DESC');	
            return $final_ret_ary ; 
        }
        else
        {
            $kBooking = new cBooking();
            $newInsuredBookingAry = array();
            $newInsuredBookingAry = $kBooking->getAllNewBookingWithInsurance();

            /* If the insurance was in the Sent or Confirmed screen when it was cancelled (regardless of whether it was cancelled from SERVICES - Insurance screens or from BOOINGS  screen, we should make it also show with a line in the screen SERVICES - Insurance - New bookings, but in the Sell, Buy, and GP column type the numbers negative, and instead of the name in the customer column, simply type "Cancelled"  
            * fetching all cancelled record for new Insurance Booking
            */

            $newCancelledInsuredBookingAry = array();
            $newCancelledInsuredBookingAry = $kBooking->getAllNewBookingWithInsurance(false,false,true,false,1);
  
            if(!empty($newCancelledInsuredBookingAry))
            {
                $newInsuredBookingAry = array_merge($newInsuredBookingAry,$newCancelledInsuredBookingAry);
            } 
            $final_ret_ary = array(); 
            $final_ret_ary = sortArray($newInsuredBookingAry,'dtBookingConfirmed','DESC');	
            return $final_ret_ary ;
        } 
    }
    
    function getAllInsuredBookingRef($szBookingRefStr)
    {
        if(!empty($szBookingRefStr))
        {  
            /*
             * DATE(dtBookingConfirmed) >= '2015-09-01' 
                AND
                    idBookingStatus IN (3,4)
                AND
                    iQuotesStatus > 0
                AND
                    iForwarderGPType = 1
                AND
                    idCustomerCurrency != idForwarderCurrency
             * 
             * szBookingRef IN $szBookingRefStr 
             */
            $query="
                SELECT
                     *
                FROM
                    ".__DBC_SCHEMATA_BOOKING__."
                WHERE
                   MONTH(dtBookingConfirmed) = '11' 
                AND
                    YEAR(dtBookingConfirmed) = '2015' 
                AND
                    idBookingStatus IN (3,4) 
                AND
                    iForwarderGPType = 1 
                AND
                    idForwarder = 42
                AND
                    fTotalVat>0
                AND
                    iBookingType != '".__BOOKING_TYPE_RFQ__."'
            ";
            //echo $query;
            if($result = $this->exeSQL($query))
            {
                $bookingAry=array();
                if($this->iNumRows>0)
                {
                    $ctr=0;
                    while($row=$this->getAssoc($result))
                    {
                        $bookingAry[$ctr]=$row;
                        $ctr++;
                    }
                    return $bookingAry ;
                }
                else
                {
                        return array();
                }
            }
            else
            {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
            }
        }
    }
    function getAllTransactionBookingRef($szBookingRefStr)
    {
        if(!empty($szBookingRefStr))
        {  
            /*
             * DATE(dtBookingConfirmed) >= '2015-09-01' 
                AND
                    idBookingStatus IN (3,4)
                AND
                    iQuotesStatus > 0
                AND
                    iForwarderGPType = 1
                AND
                    idCustomerCurrency != idForwarderCurrency
             * 
             * szBookingRef IN $szBookingRefStr 
             */
            $query="
                SELECT
                     *
                FROM
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                WHERE
                  idBooking IN $szBookingRefStr 
            ";
            //echo $query;
            if($result = $this->exeSQL($query))
            {
                $bookingAry=array();
                if($this->iNumRows>0)
                {
                    $ctr=0;
                    while($row=$this->getAssoc($result))
                    {
                        $bookingAry[$ctr]=$row;
                        $ctr++;
                    }
                    return $bookingAry ;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    
    function processInsuranceTryitout($data)
    {
        if(!empty($data))
        {
            $this->validateTryitoutDetails($data);
            if($this->error==true)
            {
                return false;
            }
            $kVatApplication = new cVatApplication();
            $fApplicableVatRate = 0; 
            if((int)$data['iPrivate']==1)
            {
                $vatInputAry = array();
                $vatInputAry['idCustomerCountry'] = $this->idCustomerCountry;
                $vatInputAry['idOriginCountry'] = $this->idOriginCountry;
                $vatInputAry['idDestinationCountry'] = $this->idDestinationCountry;
                
                $fApplicableVatRate = $kVatApplication->getTransportecaVat($vatInputAry); 
                
                $szVatString .= "<br> VAT Applicable: Yes ";
                $szVatString .= "<br> VAT Percentage: ".$fApplicableVatRate;
            }
            else
            {
                $szVatString .= "<br> Customer Type : Business ";
                $szVatString .= "<br> VAT Applicable : No ";
            }
            
            $fTotalVat = round((float)$this->fTotalPriceCustomerCurrency) * $fApplicableVatRate * .01;
            $szVatString .= "<br> Total VAT: ".$data['szCurrency']." ".$fTotalVat;
            
            $kWarehouseSearch = new cWHSSearch();
            if($this->idCustomerCurrency==1)
            {
                $fCustomerExchangeRate = 1;
                $szCustomerCurrency = "USD";
                $fTotalPriceUSD = $this->fTotalPriceCustomerCurrency;
                $fTotalVatUSD = $fTotalVat;
            }
            else
            {
                $resultAry = $kWarehouseSearch->getCurrencyDetails($this->idCustomerCurrency);		
  
                $szCustomerCurrency = $resultAry['szCurrency'];
                $fCustomerExchangeRate = $resultAry['fUsdValue']; 
                $fTotalPriceUSD = $this->fTotalPriceCustomerCurrency * $fCustomerExchangeRate ;  
                $fTotalVatUSD = $fTotalVat * $fCustomerExchangeRate;
            }
            
            if($this->idBookingCurrency==1)
            {
                $fBookingExchangeRate = 1;
                $szBookingCurrency = "USD"; 
            }
            else
            {
                $resultAry = $kWarehouseSearch->getCurrencyDetails($this->idBookingCurrency);		 
                $szBookingCurrency = $resultAry['szCurrency'];
                $fBookingExchangeRate = $resultAry['fUsdValue'];  
            }
            
            $postSearchAry = array();
            $postSearchAry['idOriginCountry'] = $this->idOriginCountry;
            $postSearchAry['idDestinationCountry'] = $this->idDestinationCountry;
            $postSearchAry['idTransportMode'] = $this->idTransportMode;
            $postSearchAry['szCargoType'] = $this->szCargoType;
            $postSearchAry['idCustomerCurrency'] = $this->idCustomerCurrency;            
            $postSearchAry['szCustomerCurrency'] = $szCustomerCurrency;
            $postSearchAry['fCustomerExchangeRate'] = $fCustomerExchangeRate;
            $postSearchAry['idCurrency'] = $this->idCustomerCurrency;
            $postSearchAry['fExchangeRate'] = $fCustomerExchangeRate;
            $postSearchAry['szCurrency'] = $szCustomerCurrency;
            $postSearchAry['fTotalPriceCustomerCurrency'] = $this->fTotalPriceCustomerCurrency;
            $postSearchAry['fTotalPriceUSD'] = $fTotalPriceUSD;
            $postSearchAry['fVatPercentage'] = $fApplicableVatRate;
            $postSearchAry['fTotalVat'] = $fTotalVat;
            $postSearchAry['fTotalVatUSD'] = $fTotalVatUSD; 
            $postSearchAry['iPrivateShipping'] = $this->iPrivateShipping;
            
            $postSearchAry['idImaginaryProfitCurrency'] = $this->idBookingCurrency;
            $postSearchAry['szImaginaryProfitCurrency'] = $szBookingCurrency;
            $postSearchAry['fImaginaryProfitExchangeRate'] = $fBookingExchangeRate;
            
            $postSearchAry['idBookingCurrency'] = 1;
            $postSearchAry['szBookingCurrency'] = "USD";
            $postSearchAry['fBookingExchangeRate'] = 1;
             
            $insuranceDataAry = array();
            $insuranceDataAry['iInsurance'] = 1;
            $insuranceDataAry['idInsuranceCurrency'] = $this->idGoodsInsuranceCurrency;
            $insuranceDataAry['fValueOfGoods'] = $this->fValueOfGoods;  
            
            
            $this->iNoSellRateDefined = false;
            $this->iNoBuyRateDefined = false;
            
            $insuranceDetailsAry = array();
            $insuranceDetailsAry = $this->calculateBookingInsurance($insuranceDataAry,$postSearchAry,"ADD_INSURANCE",false,false,true); 
             
            if($this->iInsuranceValidationFailed==1)
            {
                $this->addError('szSpecialError','No insurance rates found');
                return false;
            }
            else if($this->iInsuranceOtherErrors>0)
            {
                if($this->iNoSellRateDefined==1)
                { 
                    $this->addError('szSpecialError',t($this->t_base.'fields/no_sell_rate_defined')); 
                }
                else if($this->iNoBuyRateDefined==1)
                { 
                    $this->addError('szSpecialError',t($this->t_base.'fields/no_buy_rate_defined')); 
                }
                else
                {
                    $this->addError('szSpecialError',$this->arErrorMessages['fValueOfGoods_1']);
                } 
                return $this->tryitoutInsuranceAry;
            } 
            return $insuranceDetailsAry;
        }
    }
    
    function validateTryitoutDetails($data)
    {
        if(!empty($data))
        { 
            $this->set_szOriginCountryStr(sanitize_all_html_input(trim($data['szOriginCountryStr'])));
            $this->set_szDestinationCountryStr(sanitize_all_html_input(trim($data['szDestinationCountryStr'])));
            $this->set_idTransportMode(sanitize_all_html_input(trim($data['idTransportMode'])));
            $this->set_szCargoType(sanitize_all_html_input(trim($data['szCargoType']))); 
            $this->set_idGoodsInsuranceCurrency(sanitize_all_html_input(trim($data['idGoodsInsuranceCurrency'])));
            $this->set_fValueOfGoods(sanitize_all_html_input(trim($data['fValueOfGoods'])));
            
            $this->set_idCustomerCurrency(sanitize_all_html_input(trim($data['idCustomerCurrency'])));
            $this->set_fTotalPriceCustomerCurrency(sanitize_all_html_input(trim($data['fTotalPriceCustomerCurrency'])));
            $this->set_idCustomerCountry(sanitize_all_html_input(trim($data['idCustomerCountry'])));
            $this->set_idBookingCurrency(sanitize_all_html_input(trim($data['idBookingCurrency']))); 
            $this->iPrivateShipping = sanitize_all_html_input(trim($data['iPrivate']));
            
            if($this->error==true)
            {
                return false;
            }
            else
            {
                //Process with Google Map API
                $originCountryArr = reverse_geocode($this->szOriginCountryStr,true); 
                $kConfig = new cConfig(); 
                if(!empty($originCountryArr))
                {  
                    $idOriginCountry=$kConfig->getCountryIdByCountryName($originCountryArr['szCountryName'],$iLanguage); 
                    if($idOriginCountry<=0 && !empty($originCountryArr['szCountryCode']))
                    {
                        $kConfig->loadCountry(false,$originCountryArr['szCountryCode']);
                        $idOriginCountry = $kConfig->idCountry ; 
                    }
                }
                
                if($idOriginCountry>0)
                {
                    $this->idOriginCountry = $idOriginCountry;
                }
                else
                {
                    $this->addError('szOriginCountryStr','Origin country not found');
                }
                
                $destinationCountryArr = reverse_geocode($this->szDestinationCountryStr,true); 
                if(!empty($destinationCountryArr))
                {  
                    $idDestinationCountry=$kConfig->getCountryIdByCountryName($destinationCountryArr['szCountryName'],$iLanguage);

                    if($idDestinationCountry<=0 && !empty($destinationCountryArr['szCountryCode']))
                    {
                        $kConfig->loadCountry(false,$destinationCountryArr['szCountryCode']);
                        $idDestinationCountry = $kConfig->idCountry ; 
                    } 
                }
                if($idDestinationCountry>0)
                {
                    $this->idDestinationCountry = $idDestinationCountry;
                }
                else
                {
                    $this->addError('szOriginCountryStr','Origin country not found');
                } 
            } 
        }
    }
    
    function calculateBookingInsurance($data,$postSearchAry,$flag=false,$bParnerApi=false,$bQuickQuote=false,$bTryitout=false,$bInsuranceApi=false)
    { 
        if(!empty($data) && !empty($postSearchAry))
        {  
            if($flag=='REMOVE' || $flag=='REMOVE_INSURANCE_ONLY')
            {
                $this->iInsurance = 0;
            }
            else
            {
                $this->set_iInsurance(sanitize_all_html_input(trim($data['iInsurance'])));
                $this->set_idInsuranceCurrency(sanitize_all_html_input(trim($data['idInsuranceCurrency']))); 
                if(!is_numeric($data['fValueOfGoods']))
                {
                    $iUpdateInsuranceWithBlankValue = 1;
                    $data['fValueOfGoods'] = 0;
                }
                else
                {
                    $iUpdateInsuranceWithBlankValue = 0;
                } 
                $this->set_fValueOfGoods(sanitize_all_html_input(trim($data['fValueOfGoods']))); 
            } 
            if($this->error==true && !$bParnerApi && !$bInsuranceApi)
            { 
                $this->iInsuranceValidationFailed = 1;
                return false;
            } 

            $kInsurance = new cInsurance();
            $kWarehouseSearch = new cWHSSearch();
            $idBooking = $postSearchAry['id'] ;
 
            if($this->iInsurance==1)
            {
                if($postSearchAry['idTransportMode']<=0)
                {
                    $postSearchAry['idTransportMode'] = 2; //LCL
                } 
                if(empty($postSearchAry['szCargoType']))
                {
                    $postSearchAry['szCargoType'] = '__GENERAL_CODE__';
                }
                
                 /*
                *  Processing Transportation cost for the booking
                */
                $fTotalPriceCustomerCurrency = $postSearchAry['fTotalPriceCustomerCurrency']; 
                $fTotalVat = 0;
                if($postSearchAry['iPrivateShipping']==1)
                {
                    $fTotalPriceCustomerCurrency += $postSearchAry['fTotalVat'];
                    $fTotalVat = $postSearchAry['fTotalVat'];
                    $fVatPercentage = $postSearchAry['fVatPercentage'];
                } 
                else
                {
                    $postSearchAry['fTotalVatUSD']=0;
                }
                
                /*
                * While calculating insurance prices we always uses the current exchange of customer currency
                */
                if($postSearchAry['idCurrency']==1)
                {
                   $postSearchAry['idCurrency'] = 1; 
                   $postSearchAry['szCurrency'] = 'USD'; 
                   $postSearchAry['fExchangeRate'] = 1; 
                }
                else
                {
                    $currencyExAry = array();
                    $currencyExAry = $kWarehouseSearch->getCurrencyDetails($postSearchAry['idCurrency']);	
                    
                    if(!empty($currencyExAry))
                    {
                        $postSearchAry['idCurrency'] = $currencyExAry['idCurrency'];
                        $postSearchAry['szCurrency'] = $currencyExAry['szCurrency'];
                        $postSearchAry['fExchangeRate'] = $currencyExAry['fUsdValue'];
                    } 
                }
                
                $fTransportationCostForInsurance = $fTotalPriceCustomerCurrency;
                $fTotalBookingPriceUsd = ($fTotalPriceCustomerCurrency * $postSearchAry['fExchangeRate']);
                
                $idInsuranceCurrency = $this->idInsuranceCurrency ; 
                if($idInsuranceCurrency==1)
                {
                    $this->idGoodsInsuranceCurrency = 1;
                    $this->szGoodsInsuranceCurrency = 'USD';
                    $this->fGoodsInsuranceExchangeRate = 1; 
                    $this->fGoodsInsurancePriceUSD = $this->fValueOfGoods ; 
                    
                    $fMaxBookingAmountToBeInsuranced = $fMaxBookingAmountToBeInsurancedUSD ;
                }
                else
                {
                    $resultAry = $kWarehouseSearch->getCurrencyDetails($idInsuranceCurrency);		

                    $this->idGoodsInsuranceCurrency = $resultAry['idCurrency'];
                    $this->szGoodsInsuranceCurrency = $resultAry['szCurrency'];
                    $this->fGoodsInsuranceExchangeRate = $resultAry['fUsdValue'];

                    $this->fGoodsInsurancePriceUSD = $this->fValueOfGoods * $this->fGoodsInsuranceExchangeRate  ;  
                    
                    if($this->fGoodsInsuranceExchangeRate>0)
                    {
                        $fMaxBookingAmountToBeInsuranced = ($fMaxBookingAmountToBeInsurancedUSD/$this->fGoodsInsuranceExchangeRate);
                    } 
                    else
                    {
                        $fMaxBookingAmountToBeInsuranced = 0;
                    }
                }  
                //echo "<br><br> booking: ".$fTotalBookingPriceUsd." Goods: ".  $this->fGoodsInsurancePriceUSD." exc ".$this->fGoodsInsuranceExchangeRate." val goods ".$this->fValueOfGoods ;
                $fTotalBookingAmountToBeInsurancedUsd = $fTotalBookingPriceUsd + $this->fGoodsInsurancePriceUSD; 
                
                $iImaginaryProfitIncluded = 0;
                $fImaginaryProfitPercentage = 0;
                $fImaginaryProfitAmount = 0;
                
                if($postSearchAry['iPrivateShipping']!=1)
                {
                    /*
                    * For non-private users we are adding imaginary profit on total amount to be insured
                    */
                    $iImaginaryProfitPecentage = __INSURANCE_IMAGINARY_PROFIT__;
                    $iImaginaryProfitUsd = round((float)($fTotalBookingAmountToBeInsurancedUsd * $iImaginaryProfitPecentage * 0.01),2);
                    $fTotalBookingAmountToBeInsurancedUsd = $fTotalBookingAmountToBeInsurancedUsd + $iImaginaryProfitUsd;
                    
                    if($postSearchAry['fImaginaryProfitExchangeRate']>0)
                    {
                        $iImaginaryProfit = round((float)($iImaginaryProfitUsd / $postSearchAry['fImaginaryProfitExchangeRate']),2); 
                    }  
                    if($postSearchAry['fExchangeRate']>0)
                    {
                        $iImaginaryProfitCustomerCurrency = round((float)($iImaginaryProfitUsd / $postSearchAry['fExchangeRate']),2); 
                    } 
                    $iImaginaryProfitIncluded = 1;
                    $fImaginaryProfitPercentage = $iImaginaryProfitPecentage;
                    $fImaginaryProfitAmount = $iImaginaryProfitCustomerCurrency; 
                }
                
                $this->tryitoutInsuranceAry = array();
                if($bTryitout || $bInsuranceApi)
                {  
                    if($this->idGoodsInsuranceCurrency==$postSearchAry['idBookingCurrency'])
                    {
                        $fValueOfGoodsBookingCurrency = $this->fValueOfGoods;
                        $fGodsBookingExchangeRate = $this->fGoodsInsuranceExchangeRate;
                    }
                    else if($postSearchAry['fBookingExchangeRate']>0)
                    {
                        $fValueOfGoodsBookingCurrency = $this->fGoodsInsurancePriceUSD / $postSearchAry['fBookingExchangeRate'];
                        $fGodsBookingExchangeRate = round((float)($this->fGoodsInsuranceExchangeRate/$postSearchAry['fBookingExchangeRate']),4);
                    } 
                    
                    //echo "<br> Currency: ".$postSearchAry['idCustomerCurrency']." Bk curr: ".$postSearchAry['idBookingCurrency'];
                    if($postSearchAry['idCustomerCurrency']==$postSearchAry['idBookingCurrency'])
                    {
                        $fTransportationCostBookingCurrency = $postSearchAry['fTotalPriceCustomerCurrency'];
                        $fTotalVatBookingCurrency = $fTotalVat; 

                        $fCustomerBookingCurrency = $postSearchAry['fCustomerExchangeRate'];
                    } 
                    else if($postSearchAry['fBookingExchangeRate']>0)
                    {
                        $fTransportationCostBookingCurrency = round((float)($postSearchAry['fTotalPriceUSD'] / $postSearchAry['fBookingExchangeRate']),2);
                        $fTotalVatBookingCurrency = round((float)($postSearchAry['fTotalVatUSD'] / $postSearchAry['fBookingExchangeRate']),2); 
                        $fCustomerBookingCurrency = round((float)($postSearchAry['fCustomerExchangeRate']/$postSearchAry['fBookingExchangeRate']),4);
                    } 
                    if($postSearchAry['idImaginaryProfitCurrency']==$postSearchAry['idBookingCurrency'])
                    {
                        $iImaginaryProfitBookingCurrency = $iImaginaryProfit;
                        $fImaginaryProfitExchangeRate = $postSearchAry['fImaginaryProfitExchangeRate'];
                    }
                    else if($postSearchAry['fBookingExchangeRate']>0)
                    {
                        $iImaginaryProfitBookingCurrency = round((float)($iImaginaryProfitUsd/$postSearchAry['fBookingExchangeRate']),2);
                        $fImaginaryProfitExchangeRate = round((float)($postSearchAry['fImaginaryProfitExchangeRate']/$postSearchAry['fBookingExchangeRate']),4);
                    } 
                    $tryitoutInsuranceAry = array();
                    $tryitoutInsuranceAry['szBookingCurrency'] = $postSearchAry['szBookingCurrency'];
                    $tryitoutInsuranceAry['szCustomerCurrency'] = $postSearchAry['szCustomerCurrency'];
                    $tryitoutInsuranceAry['fValueOfGoodsBookingCurrency'] = $fValueOfGoodsBookingCurrency;
                    $tryitoutInsuranceAry['iImaginaryProfitPecentage'] = $iImaginaryProfitPecentage;
                    $tryitoutInsuranceAry['fImaginaryProfit'] = $iImaginaryProfit;                        
                    $tryitoutInsuranceAry['iImaginaryProfitBookingCurrency'] = $iImaginaryProfitBookingCurrency;

                    $tryitoutInsuranceAry['fTotalPriceCustomerCurrency'] = $postSearchAry['fTotalPriceCustomerCurrency'];
                    $tryitoutInsuranceAry['fTotalVat'] = $fTotalVat;
                    $tryitoutInsuranceAry['fVatPercentage'] = $fVatPercentage;

                    $tryitoutInsuranceAry['fTransportationCostBookingCurrency'] = $fTransportationCostBookingCurrency;
                    $tryitoutInsuranceAry['fTotalVatBookingCurrency'] = $fTotalVatBookingCurrency;

                    $tryitoutInsuranceAry['fGodsBookingExchangeRate'] = $fGodsBookingExchangeRate;
                    $tryitoutInsuranceAry['fCustomerBookingCurrency'] = $fCustomerBookingCurrency;
                    $tryitoutInsuranceAry['fImaginaryProfitExchangeRate'] = $fImaginaryProfitExchangeRate; 
                    $tryitoutInsuranceAry['szImaginaryProfitCurrency'] = $postSearchAry['szImaginaryProfitCurrency'];

                    $fTotalInsuranceValueLocal = $postSearchAry['fTotalPriceUSD'] + $postSearchAry['fTotalVatUSD'] + $iImaginaryProfitUsd + $this->fGoodsInsurancePriceUSD;
                    $fTotalInsuranceValueCustomer = $fTransportationCostBookingCurrency + $fTotalVatBookingCurrency + $iImaginaryProfitBookingCurrency + $fValueOfGoodsBookingCurrency;

                    $tryitoutInsuranceAry['fTotalInsuranceValueLocalCurrency'] = "USD";
                    $tryitoutInsuranceAry['fTotalInsuranceValueLocal'] = $fTotalInsuranceValueLocal; 
                    $tryitoutInsuranceAry['fTotalInsuranceValueCustomer'] = $fTotalInsuranceValueCustomer; 
                     
                    $tryitoutInsuranceAry['fTotalInsuranceExcahngeRate'] = round((float)(1/$postSearchAry['fBookingExchangeRate']),4); 
                    $this->tryitoutInsuranceAry = $tryitoutInsuranceAry;
                }
                 
                $insuranceDataAry = array();
                $insuranceDataAry['idOriginCountry'] = $postSearchAry['idOriginCountry'];
                $insuranceDataAry['idDestinationCountry'] = $postSearchAry['idDestinationCountry']; 
                $insuranceDataAry['idTransportMode'] = $postSearchAry['idTransportMode']; 
                $insuranceDataAry['szCargoType'] = $postSearchAry['szCargoType'];  
                $insuranceDataAry['fValueUptobeCovered'] = $fTotalBookingAmountToBeInsurancedUsd;
                $idTransportMode = $postSearchAry['idTransportMode'];
                 
                $kInsurance = new cInsurance();
                $buyRateListAry = array();
                $buyRateListAry = $kInsurance->getBuyrateCombination($insuranceDataAry,__INSURANCE_BUY_RATE_TYPE__);
                //print_r($buyRateListAry);
                $insuranceSellRateAry = array();
                if(!empty($buyRateListAry))
                {
                    $buyRateListArys= $buyRateListAry[$idTransportMode];
                    $idInsuranceBuyRate = $buyRateListArys['id'] ;  
                }  
                else
                {
                    if(!$bTryitout)
                    {
                        $this->iInsuranceOtherErrors = 1;
                        $this->iNoBuyRateDefined = 1; //This variable will only be used Inusrance try-it-out
                        $this->addError('fValueOfGoods_1',"No buy rate defined for the selected trade, mode and cargo type."); 
                        return false;
                    }
                } 
                 
                /*
                * Fetching max sell rate type insurance
                */
                $largetInsuranceDataAry = array();
                $largetInsuranceDataAry = $insuranceDataAry;
                $largetInsuranceDataAry['iLargestSellRate'] = 1;
                
                //When we find out largest insurance sell rate then there is no logic to send fTotalBookingAmountToBeInsurancedUsd to function. @Ajay
                unset($largetInsuranceDataAry['fValueUptobeCovered']);
                  
                $insuranceLargestRateAry = array();
                $insuranceLargestRateAry = $kInsurance->getBuyrateCombination($largetInsuranceDataAry,__INSURANCE_SELL_RATE_TYPE__); 
                $insuranceLargestRateArys = $insuranceLargestRateAry[$idTransportMode];
                  
                if(empty($insuranceLargestRateArys))
                {
                    if(!$bTryitout)
                    {
                        $this->iNoSellRateDefined = 1; //This variable will only be used Inusrance try-it-out
                        $this->iInsuranceOtherErrors = 1;
                        return false;
                    }
                }
                $fLargetSellPrice = $insuranceLargestRateArys['fInsurancePriceUSD']; 
    
                /*
                * Max amount to be insuranced = Max sell price - total booking price
                */
                $fMaxBookingAmountToBeInsurancedUSD = $fLargetSellPrice - $fTotalBookingPriceUsd ; 
                 
                /*
                * If max amount to be insured is greater then max sell rate then we gives an error
                */ 
                //echo " usd val ".$fTotalBookingAmountToBeInsurancedUsd." lrg price ".$fLargetSellPrice ;
                if($fTotalBookingAmountToBeInsurancedUsd > $fLargetSellPrice)
                {   
                    /*
                    * Rounding down to nearest 1000
                    */
                    if($fMaxBookingAmountToBeInsuranced>0)
                    {
                        $fMaxBookingAmountToBeInsuranced = floor((float)$fMaxBookingAmountToBeInsuranced/1000) * 1000 ;
                        $fMaxBookingAmountToBeInsuranced = number_format((float)$fMaxBookingAmountToBeInsuranced);
                    }
                    else
                    {
                        $fMaxBookingAmountToBeInsuranced = 0 ;
                    }	
                    if(!$bTryitout)
                    {
                        $this->iInsuranceOtherErrors = 2;
                        $this->addError('fValueOfGoods_1',t($this->t_base.'fields/maximum_insurance_value')." ".$this->szGoodsInsuranceCurrency." ".$fMaxBookingAmountToBeInsuranced); 
                        return false;
                    }
                }
    
                /*
                * Fetching nearest sell rate.
                */  
                $insuranceDataAry['iTryOuteFlag']=1;
                $insuranceDataAry['fTotalBookingAmountToBeInsurancedUsd'] = $fTotalBookingAmountToBeInsurancedUsd;
                $insuranceSellRateAry = array();
                $insuranceSellRateAry = $kInsurance->getBuyrateCombination($insuranceDataAry,__INSURANCE_SELL_RATE_TYPE__,true,true);
                
                if(empty($insuranceSellRateAry[$idTransportMode]) && !$bTryitout)
                {
                    if(!$bTryitout)
                    { 
                        $this->iInsuranceOtherErrors = 1; 
                        $this->iNoSellRateDefined = 1; //This variable will only be used Inusrance try-it-out
                        $this->addError('fValueOfGoods_1',"No sell rate defined for the selected trade, mode and cargo type."); 
                        return false;
                    }
                }    
                else
                {
                    $insuranceDetailsAry = $insuranceSellRateAry[$idTransportMode];  
                    if($bTryitout  || $bInsuranceApi)
                    {
                        $postSearchAry['idCurrency'] = $postSearchAry['idBookingCurrency'];
                        $postSearchAry['szCurrency'] = $postSearchAry['szBookingCurrency'];
                        $postSearchAry['fExchangeRate'] = $postSearchAry['fBookingExchangeRate'];
                    }
                    $idInsuranceRate = $insuranceDetailsAry['id']; 
                    $kInsurance->getInsuranceDetails($idInsuranceRate,true,__INSURANCE_SELL_RATE_TYPE__);
                    if($kInsurance->idInsuranceCurrency=='1')
                    {
                        $kInsurance->fInsuranceExchangeRate='1.000';
                    }
                    else
                    {
                        $kWarehouseSearch = new cWHSSearch();                                          
                        $resultAry = $kWarehouseSearch->getCurrencyDetails($kInsurance->idInsuranceCurrency); 
                        $fInsuranceExchangeRate = $resultAry['fUsdValue']; 
                        $kInsurance->fInsuranceExchangeRate=$fInsuranceExchangeRate;
                    } 
                    $idCalculationInsuranceCurrency = $kInsurance->idInsuranceCurrency; 
                    if( $kInsurance->idInsuranceMinCurrency=='1')
                    {
                        $kInsurance->fInsuranceMinExchangeRate='1.000';
                    }
                    else
                    {
                        $kWarehouseSearch = new cWHSSearch();                                          
                        $resultAry = $kWarehouseSearch->getCurrencyDetails($kInsurance->idInsuranceMinCurrency); 
                        $fInsuranceMinExchangeRate = $resultAry['fUsdValue']; 
                        $kInsurance->fInsuranceMinExchangeRate=$fInsuranceMinExchangeRate;
                    }

                    $fTotalInsuranceCostUsd = ($fTotalBookingAmountToBeInsurancedUsd * $kInsurance->fInsuranceRate)/100;

                    if($kInsurance->fInsuranceExchangeRate>0)
                    {
                        $fTotalInsuranceCost =  ( $fTotalInsuranceCostUsd/$kInsurance->fInsuranceExchangeRate );
                    }
                    else
                    {
                        $fTotalInsuranceCost = 0 ;
                    }  
                    $fTotalInsuranceMinCost = $kInsurance->fInsuranceMinPrice ;
                    $fTotalInsuranceMinCostUsd = $kInsurance->fInsuranceMinPrice * $kInsurance->fInsuranceMinExchangeRate ;
                    $iMinrateApplied = 0;
                    if($fTotalInsuranceMinCostUsd>$fTotalInsuranceCostUsd)
                    {
                        $fTotalInsuranceCost = $fTotalInsuranceMinCost ;
                        $fTotalInsuranceCostUsd = $fTotalInsuranceMinCostUsd ;
                        $idCalculationInsuranceCurrency = $kInsurance->idInsuranceMinCurrency ;
                        $iMinrateApplied = 1;
                    }   
                    
                    if($postSearchAry['bUpdatePriceByAPI']==1)
                    {
                        $fTotalInsuranceCost = $postSearchAry['fInsuranceSellPriceAPI'] ;
                        $fTotalInsuranceCostUsd = $postSearchAry['fInsuranceSellPriceAPI'] * $postSearchAry['fExchangeRate'];
                        $idCalculationInsuranceCurrency = $postSearchAry['idCurrency'] ;
                        $iMinrateApplied = 0;
                    }
                    
                    $fInsuranceBookingExchangeRate = 0;
                    if($idCalculationInsuranceCurrency==$postSearchAry['idCurrency'])
                    {
                        $fTotalInsuranceCostForBookingCustomerCurrency = $fTotalInsuranceCost ;
                        $fInsuranceBookingExchangeRate = 1;
                    }
                    else if($postSearchAry['fExchangeRate']!=0)
                    {
                        $fTotalInsuranceCostForBookingCustomerCurrency = round((float)($fTotalInsuranceCostUsd / $postSearchAry['fExchangeRate']),2);
                        $fInsuranceBookingExchangeRate = round((float)($kInsurance->fInsuranceExchangeRate/$postSearchAry['fExchangeRate']),4);
                    }
                    else
                    {
                        $fTotalInsuranceCostForBookingCustomerCurrency = 0 ;
                    } 
                    if($bTryitout  || $bInsuranceApi)
                    {
                        $fTotalInsuranceCostForBookingCustomerCurrency = round((float)($fTotalInsuranceCostUsd / $postSearchAry['fExchangeRate']),2);
                    }
                    else
                    {
                        $fTotalInsuranceCostForBookingCustomerCurrency = ceil((float)($fTotalInsuranceCostUsd / $postSearchAry['fExchangeRate']));
                    }
                    
                    if($flag=='INSURANCE_AVAILABILITY_CHECK')
                    {
                        $this->fTotalInsuranceCostForBookingCustomerCurrency = $fTotalInsuranceCostForBookingCustomerCurrency ;
                        $this->fValueUptoPrice = $kInsurance->fInsuranceUptoPrice ; 
                        return true ;
                    } 
                    $this->fTotalInsuranceCostForBookingCustomerCurrencyApi = $fTotalInsuranceCostForBookingCustomerCurrency;
                    $updateBookingAry = array(); 
                    if($bTryitout  || $bInsuranceApi)
                    {
                        $updateBookingAry = $tryitoutInsuranceAry;
                    }
                    $updateBookingAry['iImaginaryProfitIncluded'] = $iImaginaryProfitIncluded ;
                    $updateBookingAry['fImaginaryProfitPercentage'] = $fImaginaryProfitPercentage ;
                    $updateBookingAry['fImaginaryProfitAmount'] = $fImaginaryProfitAmount ;
                    $updateBookingAry['fTransportationCostForInsurance'] = $fTransportationCostForInsurance ;
                    
                    $updateBookingAry['iInsuranceIncluded'] = 1 ;
                    $updateBookingAry['fValueOfGoods'] = $this->fValueOfGoods ;
                    $updateBookingAry['fValueOfGoodsUSD'] = $this->fGoodsInsurancePriceUSD ;
                    $updateBookingAry['idGoodsInsuranceCurrency'] = $this->idGoodsInsuranceCurrency ; 
                    $updateBookingAry['szGoodsInsuranceCurrency'] = $this->szGoodsInsuranceCurrency ;
                    $updateBookingAry['fGoodsInsuranceExchangeRate'] = $this->fGoodsInsuranceExchangeRate ;
                    $updateBookingAry['fTotalInsuranceCostForBooking'] = $fTotalInsuranceCost ;
                    $updateBookingAry['fTotalInsuranceCostForBookingCustomerCurrency'] = $fTotalInsuranceCostForBookingCustomerCurrency ;
                    $updateBookingAry['fTotalInsuranceCostForBookingUSD'] = $fTotalInsuranceCostUsd ;
                    $updateBookingAry['fTotalAmountInsured'] = $fTotalBookingAmountToBeInsurancedUsd ; 
                    $updateBookingAry['iMinrateApplied'] = $iMinrateApplied;

                    $updateBookingAry['idInsuranceRate'] = $kInsurance->id;
                    $updateBookingAry['fInsuranceUptoPrice'] = $kInsurance->fInsuranceUptoPrice; 
                    $updateBookingAry['idInsuranceUptoCurrency'] = $kInsurance->idInsuranceCurrency; 
                    $updateBookingAry['szInsuranceUptoCurrency'] = $kInsurance->szInsuranceCurrency;
                    $updateBookingAry['fInsuranceUptoExchangeRate'] = $kInsurance->fInsuranceExchangeRate;
                    $updateBookingAry['fInsuranceRate'] = $kInsurance->fInsuranceRate;
                    $updateBookingAry['fInsuranceMinPrice'] = $kInsurance->fInsuranceMinPrice;
                    $updateBookingAry['idInsuranceMinCurrency'] = $kInsurance->idInsuranceMinCurrency;

                    $updateBookingAry['szInsuranceMinCurrency'] = $kInsurance->szInsuranceMinCurrency;
                    $updateBookingAry['fInsuranceMinExchangeRate'] = $kInsurance->fInsuranceMinExchangeRate;
                    $updateBookingAry['fInsuranceUptoPriceUSD'] = $kInsurance->fInsurancePriceUSD;
                    $updateBookingAry['fInsuranceMinPriceUSD'] = $kInsurance->fInsuranceMinPriceUSD;
                    $updateBookingAry['iInsuranceUpdatedFlag'] = 1; 
                    $updateBookingAry['iUpdateInsuranceWithBlankValue'] = $iUpdateInsuranceWithBlankValue; 
                    $updateBookingAry['iUpdateInsurancePriceByAPI'] = $postSearchAry['bUpdatePriceByAPI'];  
                    
                    /*
                    * Calculating buy rate for booking  
                    */ 
                    $insuranceDetailsAry = array();
                    $insuranceDetailsAry = $buyRateListArys;

                    if(!empty($insuranceDetailsAry))
                    {
                        $idInsuranceRate = $insuranceDetailsAry['id'];
                        $kInsurance->getInsuranceDetails($idInsuranceRate,true,__INSURANCE_BUY_RATE_TYPE__);

                        $fTotalInsuranceCostUsd = ($fTotalBookingAmountToBeInsurancedUsd * $kInsurance->fInsuranceRate)/100;    
                        $idCalculationInsuranceCurrency_buyrate = $kInsurance->idInsuranceCurrency; 

                        if($kInsurance->idInsuranceCurrency=='1')
                        {
                            $kInsurance->fInsuranceExchangeRate='1.000';
                        }
                        else
                        {
                            $kWarehouseSearch = new cWHSSearch();                                          
                            $resultAry = $kWarehouseSearch->getCurrencyDetails($kInsurance->idInsuranceCurrency); 
                            $fInsuranceExchangeRate = $resultAry['fUsdValue']; 
                            $kInsurance->fInsuranceExchangeRate=$fInsuranceExchangeRate;
                        }
                        if($kInsurance->fInsuranceExchangeRate>0)
                        {
                            $fTotalInsuranceCost = ($fTotalInsuranceCostUsd/$kInsurance->fInsuranceExchangeRate);
                        }
                        else
                        {
                            $fTotalInsuranceCost = 0;
                        } 

                        $fTotalInsuranceMinCost = $kInsurance->fInsuranceMinPrice ;
                        $fTotalInsuranceMinCostUsd = $kInsurance->fInsuranceMinPrice * $kInsurance->fInsuranceMinExchangeRate ;

                        if($fTotalInsuranceMinCostUsd>$fTotalInsuranceCostUsd)
                        {
                            $fTotalInsuranceCost = $fTotalInsuranceMinCost ;
                            $fTotalInsuranceCostUsd = $fTotalInsuranceMinCostUsd ;
                            $idCalculationInsuranceCurrency_buyrate = $kInsurance->idInsuranceMinCurrency; 
                        }  

                        $fInsuranceBookingExchangeRate_buyRate = 0;
                        if($idCalculationInsuranceCurrency_buyrate == $postSearchAry['idCurrency'])
                        {
                            $fTotalInsuranceCostForBookingCustomerCurrency = $fTotalInsuranceCost ;
                            $fInsuranceBookingExchangeRate_buyRate = 1.0000;
                        }
                        else if($postSearchAry['fExchangeRate']!=0)
                        {
                            if($bTryitout  || $bInsuranceApi)
                            {
                                $fTotalInsuranceCostForBookingCustomerCurrency = round((float)($fTotalInsuranceCostUsd / $postSearchAry['fExchangeRate']),2);
                            }
                            else
                            {
                                $fTotalInsuranceCostForBookingCustomerCurrency = ceil((float)($fTotalInsuranceCostUsd / $postSearchAry['fExchangeRate']));
                            }
                            $fInsuranceBookingExchangeRate_buyRate = round((float)($kInsurance->fInsuranceExchangeRate/$postSearchAry['fExchangeRate']),4);
                        }
                        else
                        {
                            $fTotalInsuranceCostForBookingCustomerCurrency = 0 ;
                        } 

                        if($data['iIncludeInsuranceWithZeroCargo']==1 && $this->fValueOfGoods==0)
                        {
                            /*
                            * For manual booking, If insurance: Yes and cargo value is updated as 0 then we keep all other fields but only remove sell rate and buy rate
                            */
//                                    $fTotalInsuranceCostForBookingCustomerCurrency = 0 ;
//                                    $fTotalInsuranceCost = 0;
//                                    $fTotalInsuranceCostUsd = 0;
                        }

                        $updateBookingAry['idInsuranceRate_buyRate'] = $kInsurance->id;
                        $updateBookingAry['fInsuranceUptoPrice_buyRate'] = $kInsurance->fInsuranceUptoPrice; 
                        $updateBookingAry['idInsuranceUptoCurrency_buyRate'] = $kInsurance->idInsuranceCurrency; 
                        $updateBookingAry['szInsuranceUptoCurrency_buyRate'] = $kInsurance->szInsuranceCurrency;
                        $updateBookingAry['fInsuranceUptoExchangeRate_buyRate'] = $kInsurance->fInsuranceExchangeRate;
                        $updateBookingAry['fInsuranceRate_buyRate'] = $kInsurance->fInsuranceRate;
                        $updateBookingAry['fInsuranceMinPrice_buyRate'] = $kInsurance->fInsuranceMinPrice;
                        $updateBookingAry['idInsuranceMinCurrency_buyRate'] = $kInsurance->idInsuranceMinCurrency; 
                        $updateBookingAry['szInsuranceMinCurrency_buyRate'] = $kInsurance->szInsuranceMinCurrency;
                        $updateBookingAry['fInsuranceMinExchangeRate_buyRate'] = $kInsurance->fInsuranceMinExchangeRate;
                        $updateBookingAry['fInsuranceUptoPriceUSD_buyRate'] = $kInsurance->fInsurancePriceUSD;
                        $updateBookingAry['fInsuranceMinPriceUSD_buyRate'] = $kInsurance->fInsuranceMinPriceUSD;

                        $updateBookingAry['fTotalInsuranceCostForBooking_buyRate'] = $fTotalInsuranceCost ;
                        $updateBookingAry['fTotalInsuranceCostForBookingCustomerCurrency_buyRate'] = $fTotalInsuranceCostForBookingCustomerCurrency ;
                        $updateBookingAry['fTotalInsuranceCostForBookingUSD_buyRate'] = $fTotalInsuranceCostUsd ; 
                    }
                    
                    $fDkkExchangeRate = 0; 
                    $dkkExchangeAry = array();
                    $kWHSSearch = new cWHSSearch();
                    $dkkExchangeAry = $kWHSSearch->getCurrencyDetails(20);	 
                    if($dkkExchangeAry['fUsdValue']>0)
                    {
                        $fDkkExchangeRate = $dkkExchangeAry['fUsdValue'] ;						 
                    } 
                    $updateBookingAry['fDkkExchangeRate'] = $fDkkExchangeRate;
                    
                    if($bTryitout || $bInsuranceApi)
                    {   
                        $updateBookingAry['fInsuranceBookingExchangeRate'] = $fInsuranceBookingExchangeRate; 
                        $updateBookingAry['fInsuranceBookingExchangeRate_buyRate'] = $fInsuranceBookingExchangeRate_buyRate; 
                    }  
                    $this->arrInsuranceUpdateAry = $updateBookingAry; 
                    if($bParnerApi || $bQuickQuote || $bTryitout || $bInsuranceApi)
                    {
                        return $updateBookingAry;
                    }
                    else
                    { 
                        /*
                        * updating booking from hold to draft booking 
                        */  
                        foreach($updateBookingAry as $key=>$value)
                        {
                            $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                        }
                        
                        $kBooking = new cBooking();
                        $update_query = rtrim($update_query,",");   
                        if($kBooking->updateDraftBooking($update_query,$idBooking))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        } 
                    } 
                } 
            }
            else
            {
                //Removing insurance from system 
                $updateBookingAry = array();
                if($flag=='REMOVE_INSURANCE_ONLY')
                {
                    $updateBookingAry['iInsuranceIncluded'] = 0; 
                }
                else
                {
                    $updateBookingAry['iInsuranceIncluded'] = 0; 
                    $updateBookingAry['iInsuranceUpdatedFlag'] = 1; 
                    if($data['iUpdateInsuranceChoice']==1)
                    {
                        $updateBookingAry['iInsuranceChoice'] = 2; 
                        $res_ary['iInsuranceMandatory'] = 0; 
                    } 
                }
                 
                /*
                * updating booking from hold to draft booking 
                */  

                foreach($updateBookingAry as $key=>$value)
                {
                        $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                } 
                $update_query = rtrim($update_query,",");
                $kBooking = new cBooking();
                if($kBooking->updateDraftBooking($update_query,$idBooking))
                {
                    return true;
                }
                else
                {
                    return false;
                } 
           }
        }
    }
    
    function set_id( $value )
    {
        $this->id = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "id", t($this->t_base.'fields/id'), false, false, true );
    }
    function set_iInsuranceType( $value )
    {
        $this->iInsuranceType = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iInsuranceType", t($this->t_base.'fields/insurance_type'), false, false, true );
    }
    function set_fInsuranceUptoPrice( $value )
    {
        $this->fInsuranceUptoPrice = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fInsuranceUptoPrice", t($this->t_base.'fields/value_upto'), 0, false, true );
    }
    function set_idInsuranceCurrency( $value )
    {
        $this->idInsuranceCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idInsuranceCurrency", t($this->t_base.'fields/value_upto')." ".t($this->t_base.'fields/currency'), false, false, true );
    }
    function set_fInsuranceRate( $value )
    {
        $this->fInsuranceRate = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fInsuranceRate", t($this->t_base.'fields/rates'), 0, 100, true );
    }
    function set_fInsuranceMinPrice($value)
    {
        $this->fInsuranceMinPrice = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fInsuranceMinPrice", t($this->t_base.'fields/minimum')." ".t($this->t_base.'fields/value') , 0, false, true );
    }
    function set_iInsuranceMinCurrency( $value )
    {
        $this->iInsuranceMinCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iInsuranceMinCurrency", t($this->t_base.'fields/minimum')." ".t($this->t_base.'fields/currency') , false, false, true );
    }
    function set_szTitle($value)
    {
        $this->szTitle = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTitle", t($this->t_base.'fields/rss_title'), false, 255, true );
    }
    function set_szFriendlyName($value)
    {
        $this->szFriendlyName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szFriendlyName", t($this->t_base.'fields/friendly_name'), false, 255, true );
    }
    function set_szDescription($value)
    {
        $this->szDescription = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDescription", t($this->t_base.'fields/description_text'), false, false, true );
    }
    function set_szDescriptionHTML($value)
    {
        $this->szDescriptionHTML = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDescriptionHTML", t($this->t_base.'fields/description_html'), false, false, true );
    }

    function set_idOriginCountry( $value )
    {
        $this->idOriginCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idOriginCountry", t($this->t_base.'fields/origin_country'), false, false, true );
    }
    function set_idDestinationCountry( $value )
    {
        $this->idDestinationCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idDestinationCountry", t($this->t_base.'fields/destination_country'), false, false, true );
    }
    function set_idTransportMode( $value )
    {
        $this->idTransportMode = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idTransportMode", t($this->t_base.'fields/transport_mode'), false, false, true );
    }
    function set_iPrivate( $value )
    {
        $this->iPrivate = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iPrivate", t($this->t_base.'fields/moving'), false, false, false );
    }
    function set_szCargoType( $value )
    {
        $this->szCargoType = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCargoType", t($this->t_base.'fields/cargo_type'), false, false, true );
    } 
    function set_idInsurance( $value )
    {
        $this->idInsurance = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idInsurance", t($this->t_base.'fields/idInsurance'), false, false, true );
    } 
    
    function set_szOriginCountryStr( $value )
    {
        $this->szOriginCountryStr = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szOriginCountryStr", t($this->t_base.'fields/origin_country'), false, false, true );
    }
    function set_szDestinationCountryStr( $value )
    {
        $this->szDestinationCountryStr = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDestinationCountryStr", t($this->t_base.'fields/origin_country'), false, false, true );
    }
    function set_idGoodsInsuranceCurrency( $value )
    {
        $this->idGoodsInsuranceCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idGoodsInsuranceCurrency", t($this->t_base.'fields/origin_country'), false, false, true );
    } 
    function set_fValueOfGoods( $value )
    {
        $this->fValueOfGoods = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fValueOfGoods", t($this->t_base.'fields/origin_country'), 0, false, true );
    }
    function set_idCustomerCurrency( $value )
    {
        $this->idCustomerCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idCustomerCurrency", t($this->t_base.'fields/origin_country'), false, false, true );
    }
    function set_fTotalPriceCustomerCurrency( $value )
    {
        $this->fTotalPriceCustomerCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fTotalPriceCustomerCurrency", t($this->t_base.'fields/origin_country'), false, false, true );
    }
    function set_idForwarderCountry( $value )
    {
        $this->idForwarderCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idForwarderCountry", t($this->t_base.'fields/origin_country'), false, false, true );
    }
    function set_idBookingCurrency( $value )
    {
        $this->idBookingCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idBookingCurrency", t($this->t_base.'fields/origin_country'), false, false, true );
    }
    function set_iInsurance( $value )
    {
        $this->iInsurance = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iInsurance", t($this->t_base.'fields/insurance'), false, false, true );
    }
    
    function set_idCustomerCountry( $value )
    {
        $this->idCustomerCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idCustomerCountry", t($this->t_base.'fields/origin_country'), false, false, true );
    }
}

?>