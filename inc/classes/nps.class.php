<?php
/**
* This file is the container for all customer feedback related functionality.
* All functionality related to customer feedback details should be contained in this class.
* 
* nps.class.php
* 
* @copyright Copyright (C) 2012 Transporteca, LLC
* @author Anil
* @package Transporteca Development
* 
*/

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );

/*
 * NPS stands for 'Net Promoter Score'
 * 
*/

class cNPS extends cDatabase
{
	var  $t_base="management/NPS/";
	function __construct()
	{
            parent::__construct();
            return true;
	} 
        
	function getCustomerFeedBackDay($idUser)
	{	
            $kWHSSearch = new cWHSSearch();
            $miniNPSDays=$kWHSSearch->getManageMentVariableByDescription('__MIMI_DAYS_FOR_NPS_FROM_CUSTOM__');

            $query="
                SELECT
                    id
                FROM
                    ".__DBC_SCHEMATA_CUSTOER_FEEDBACK__."
                WHERE
                    idUser='".(int)$idUser."'
                AND
                    DATE_FORMAT(dtComment,'%Y-%m-%d') >= DATE_FORMAT(NOW() - INTERVAL ".$miniNPSDays." DAY,'%Y-%m-%d')
            ";
            //echo $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                if( $this->getRowCnt() > 0 )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
	}
	
	
	function addCustomerFeedBack($data,$idUser)
	{
            $this->set_iScore(sanitize_all_html_input($data['iScore']));
            $this->set_szComment(sanitize_all_html_input($data['szComment']));

            if($this->error === true)
            {
                return false;
            }

            $kUser = new cUser();
            $kUser->getUserDetails($idUser);


            $query="
                INSERT INTO
                    ".__DBC_SCHEMATA_CUSTOER_FEEDBACK__."
                (
                    idUser,
                    szUserFirstName,
                    szUserLastName,
                    iScore,
                    szComment,
                    dtComment
                )
                VALUES
                (
                    '".(int)$idUser."',
                    '".mysql_escape_custom($kUser->szFirstName)."',
                    '".mysql_escape_custom($kUser->szLastName)."',
                    '".(int)$this->iScore."',
                    '".mysql_escape_custom($this->szComment)."',
                    NOW()
                )
            ";
            if( ( $result = $this->exeSQL( $query ) ) )
            { 
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_USERS__."
                    SET
                        iLastestScore='".(int)$this->iScore."'
                    WHERE
                        id='".(int)$idUser."'
                ";
                $result = $this->exeSQL( $query );
                return true;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
	
	function getLastThreeMonthComment($dataArr=array())
	{
	
		$sql="";
		$colvalue="";
		if(empty($dataArr))
		{
		
		  $colvalue="	
				id,
				idUser,
				szUserFirstName,
				szUserLastName,
				iScore,
				szComment,
				dtComment";
			$dateFromDate=date('Y-m-d',strtotime('- 3 MONTH'));
			//echo $dateFromDate;
			$sql="
				WHERE
					DATE(dtComment)>='".mysql_escape_custom($dateFromDate)."'
				AND
					szComment<>''	
					";
		}
		else
		{
			$colvalue="	
				idUser,
				iScore,
				szComment,
				dtComment";
			$fromDate=$dataArr['dtFromDownload'];
			$toDate=$dataArr['dtToDownload'];
			$newFromDate='';
			if($fromDate!='')
			{
				 $dateFromAry = explode("/",$fromDate); 
				 $dtTiming_Frommillisecond = strtotime($dateFromAry[2]."-".$dateFromAry[1]."-".$dateFromAry[0]);  // converting date 
				 //$dtDate = date("d/m/Y",$dtTiming_millisecond);
				 	 
				 if(!checkdate($dateFromAry[1],$dateFromAry[0],$dateFromAry[2]))
	             {
	                $this->addError("dtFromDownload",t($this->t_base.'fields/from_date')." ".t($this->t_base.'messages/from_date_should_be_valid'));
	                //return false;
	             }
	             else
	             {
	             	$newFromDate=$dateFromAry[2]."-".$dateFromAry[1]."-".$dateFromAry[0];
	             }
			}
			//echo $toDate;
			if($toDate!='')
			{
				 $dateToAry = explode("/",$toDate); 
				 $dtTiming_Tomillisecond = strtotime($dateToAry[2]."-".$dateToAry[1]."-".$dateToAry[0]);  // converting date 
				 //$dtDate = date("d/m/Y",$dtTiming_millisecond);
				 	 
				 if(!checkdate($dateToAry[1],$dateToAry[0],$dateToAry[2]))
	             {
	                $this->addError("dtToDownload",t($this->t_base.'fields/to_date')." ".t($this->t_base.'messages/from_date_should_be_valid'));
	                //return false;
	             }
	             else if(!empty($newFromDate) && $dtTiming_Tomillisecond<$dtTiming_Frommillisecond)
	             {
	             	 $this->addError("dtToDownload",t($this->t_base.'messages/to_date_greater_than_from_date'));
	               // return false;
	             }
	             else
	             {
	             	$newToDate=$dateToAry[2]."-".$dateToAry[1]."-".$dateToAry[0];
	             }
			}
			
			if($this->error===true)
			{
				return false;
			}
			
			$sql="
				WHERE
			";
			if($newFromDate!='')
			{
				$sql .="
					DATE(dtComment)>='".mysql_escape_custom($newFromDate)."'
				";
			}
			
			if($newFromDate!='' && $newToDate!='')
			{
				$sql .="
					AND
						DATE(dtComment)<='".mysql_escape_custom($newToDate)."'
				";
			}
			else if($newFromDate=='' && $newToDate!='')
			{
				$sql .="
					DATE(dtComment)<='".mysql_escape_custom($newToDate)."'
				";
			}
		}
		
		$query="
			SELECT
				$colvalue
			FROM
				".__DBC_SCHEMATA_CUSTOER_FEEDBACK__."
			$sql
			ORDER BY
				dtComment DESC
		";
		if($result = $this->exeSQL($query))
		{
			$bookingAry=array();
			if($this->iNumRows>0)
			{
				$ctr=0;
				while($row=$this->getAssoc($result))
				{
					$res_array[]=$row;
				}
				return $res_array;
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function totalCountOfBookingForUser($idUser,$flag=false)
	{
		$dateFromDate=date('Y-m-d',strtotime('- 12 MONTH'));
		$toDate=date('Y-m-d');
		$sql='';
		if($flag)
		{
			$sql ="
				AND
					idBookingStatus='".(int)__BOOKING_STATUS_CONFIRMED__."'
				AND
					dtBookingConfirmed>='".mysql_escape_custom($dateFromDate)."'
				AND
					dtBookingConfirmed<='".mysql_escape_custom($toDate)."'
			";
		}
		else
		{
			$sql ="
				AND
				   dtCreatedOn>='".mysql_escape_custom($dateFromDate)."'
				AND
					dtCreatedOn<='".mysql_escape_custom($toDate)."'
			";
		}
		
		$query="
			SELECT
				count(id) as Total
			FROM
				".__DBC_SCHEMATA_BOOKING__."
			WHERE
				idUser='".(int)$idUser."'
			$sql
		";
		if($result = $this->exeSQL($query))
		{
			$bookingAry=array();
			if($this->iNumRows>0)
			{
				$row=$this->getAssoc($result);
				return $row['Total'];
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}
	
	function storePrevMonthCustomerData($dtDate)
	{
		$redTotalCount=$this->storePrevMonthCustomerFeedBackData($dtDate,'',__CUSTOMER_FEEDBACK_RED_SCORE__);
		
		$yellowTotalCount=$this->storePrevMonthCustomerFeedBackData($dtDate,__CUSTOMER_FEEDBACK_RED_SCORE__,__CUSTOMER_FEEDBACK_YELLOW_SCORE__);
		
		$greenTotalCount=$this->storePrevMonthCustomerFeedBackData($dtDate,__CUSTOMER_FEEDBACK_YELLOW_SCORE__,__CUSTOMER_FEEDBACK_GREEN_SCORE__);
		
		$dtCreated=date('Y-m-d',strtotime('-1 MONTH'));
		
		$query="
			INSERT INTO
				".__DBC_SCHEMATA_CUSTOER_FEEDBACK_GRAPH__."
			(
				redCount,
				yellowCount,
				greenCount,
				dtCreated
			)
				VALUES
			(
				'".(int)$redTotalCount."',
				'".(int)$yellowTotalCount."',
				'".(int)$greenTotalCount."',
				'".mysql_escape_custom($dtCreated)."'
			)
		";
		$result = $this->exeSQL($query);
	}
	
	function storePrevMonthCustomerFeedBackData($dtDate,$formLimit=0,$toLimit)
	{
		$sql="";
		if((int)$formLimit>0)
		{
			$sql="
				AND
					iScore>'".(int)$formLimit."'
			
			";
		}
		
		if((int)$toLimit>0)
		{
			$sql .="
				AND
					iScore<='".(int)$toLimit."'
			
			";
		}
		$query="
			SELECT
				count(id) as Total
			FROM
				".__DBC_SCHEMATA_CUSTOER_FEEDBACK__."
			WHERE
				date_format(dtComment,'%Y-%m') ='".mysql_escape_custom($dtDate)."'
			 $sql		
			";
			//echo $query."<br /><br />";
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					return $row['Total'];
				}
				else
				{
					return 0;
				}
			}
			else
			{
				return 0;
			}
	}
	
	function storeCurrMonthCustomerData()
	{
		$dtDate=date('Y-m');
		$redTotalCount=$this->storePrevMonthCustomerFeedBackData($dtDate,'',__CUSTOMER_FEEDBACK_RED_SCORE__); 
		$yellowTotalCount=$this->storePrevMonthCustomerFeedBackData($dtDate,__CUSTOMER_FEEDBACK_RED_SCORE__,__CUSTOMER_FEEDBACK_YELLOW_SCORE__); 
		$greenTotalCount=$this->storePrevMonthCustomerFeedBackData($dtDate,__CUSTOMER_FEEDBACK_YELLOW_SCORE__,__CUSTOMER_FEEDBACK_GREEN_SCORE__); 
		
		$grandTotal=$redTotalCount+$yellowTotalCount+$greenTotalCount; 
		if($grandTotal>0)
		{
                    $redCountPer=($redTotalCount*100)/$grandTotal;
		}
		else 
                {
                    $redCountPer =0;
		}
		$redCountPer=round($redCountPer,2);
		
		if($grandTotal>0)
		{
                    $yellowCountPer=($yellowTotalCount*100)/$grandTotal;
		}
		else 
                {
                    $yellowCountPer =0;
		}
		
		$yellowCountPer=round($yellowCountPer,2);
		
		if($grandTotal>0)
		{
                    $greenCountPer=($greenTotalCount*100)/$grandTotal;
		}
		else 
                {
                    $greenCountPer =0;
		}
		
		$greenCountPer=round($greenCountPer,2);
		
		$data=$this->prev11MonthData();
		$data[date('M')]['redCount']=$redCountPer;
		$data[date('M')]['yellowCount']=$yellowCountPer;
		$data[date('M')]['greenCount']=$greenCountPer;
		$data[date('M')]['totalCount']=$grandTotal;
		
		
		return $data;
		//$this->createImageCustomerFeedbackGraph($data,'1');
	
	}
	
	function prev11MonthData()
	{
		$monthArr=$this->prev11MonthArr();
		//print_r($monthArr);
		$data=array();
		if(!empty($monthArr))
		{
			foreach($monthArr as $monthArrs)
			{
                            $grandTotal=0;
                            $redTotalCount = 0;
                            $yellowTotalCount = 0;
                            $greenTotalCount = 0; 
                            
                            $query="
                                SELECT
                                    redCount,
                                    yellowCount,
                                    greenCount
                                FROM
                                    ".__DBC_SCHEMATA_CUSTOER_FEEDBACK_GRAPH__."
                                WHERE
                                    date_format(dtCreated,'%Y-%m') ='".mysql_escape_custom($monthArrs)."'
                            ";
                           // echo $query ;
				if($result = $this->exeSQL($query))
				{
                                    if($this->iNumRows>0)
                                    {
                                        $row=$this->getAssoc($result);

                                        $redTotalCount=$row['redCount'];
                                        $yellowTotalCount=$row['yellowCount'];
                                        $greenTotalCount=$row['greenCount'];
                                    }
				}
				
				$grandTotal=$redTotalCount+$yellowTotalCount+$greenTotalCount;
                                
                                $redCountPer = 0;
                                $yellowCountPer = 0; 
                                $greenCountPer = 0; 
                                
				if($grandTotal>0)
				{
                                    $redCountPer=($redTotalCount*100)/$grandTotal;
                                    $redCountPer=round($redCountPer,2);


                                    $yellowCountPer=($yellowTotalCount*100)/$grandTotal;
                                    $yellowCountPer=round($yellowCountPer,2);


                                    $greenCountPer=($greenTotalCount*100)/$grandTotal;
                                    $greenCountPer=round($greenCountPer,2);
				}
				
				$key1 = strtoupper(date('M',strtotime($monthArrs)));
				
				$data[$key1]['redCount']=$redCountPer;
				$data[$key1]['yellowCount']=$yellowCountPer;
				$data[$key1]['greenCount']=$greenCountPer;
				$data[$key1]['totalCount']=$grandTotal;
				
				//print_r($data);
			}
			
		}
		
		return $data;
	}
	
	function prev11MonthArr()
	{
		$monthsArr = array();
		 for( $i = 11; $i >= 1 ; $i-- )
		 {
		 	 $monthsArr[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
		 }
		 	return $monthsArr;
	}
        
        function get_warehouse_distance_search($data,$warehouse,$idWareHouse=false,$idForwarder=false,$idCountry=false)
	{ 
            if(!empty($data))
            {				
                if(trim($data['szOriginCity'])==t($this->t_base_homepage.'fields/type_name'))
                {
                        $data['szOriginCity'] = '';
                }
                if(trim($data['szOriginPostCode'])==t($this->t_base_homepage.'fields/optional') || trim($data['szOriginPostCode'])==t($this->t_base_homepage.'fields/type_code'))
                {
                        $data['szOriginPostCode'] = '';
                }			

                if(trim($data['szDestinationCity'])==t($this->t_base_homepage.'fields/type_name'))
                {
                        $data['szDestinationCity'] = '';
                }
                if(trim($data['szDestinationPostCode'])==t($this->t_base_homepage.'fields/optional') || trim($data['szDestinationPostCode'])==t($this->t_base_homepage.'fields/type_code'))
                {
                        $data['szDestinationPostCode'] = '';
                }

                $distanceMeasureAry=array();	
                if($warehouse == 'TO')
                {
                    if(!empty($data['szDestinationPostCode']))
                    {
                        // getting laltitude and longitute by postcode 
                        $distanceMeasureAry = $this->getDistanceMeasure(false,$data['szDestinationPostCode'],$data['idDestinationCountry']);
                    }
                    else if(!empty($data['szDestinationCity']))
                    {
                        //getting laltitude and longitute by city
                        $distanceMeasureAry = $this->getDistanceMeasure($data['szDestinationCity'],false,$data['idDestinationCountry']); 
                    }
                    if((int)$idCountry <=0)
                    {
                            $distanceMeasureAry['idCountry'] = $data['idDestinationCountry'] ;
                    }
                    $distanceMeasureAry['iDirection'] = 2;
                }			
                if($warehouse=='FROM')
                {
                    $idCountry = $data['idOriginCountry'] ;
                    if(!empty($data['szOriginPostCode']))
                    {
                            // getting laltitude and longitute by postcode 
                            $distanceMeasureAry = $this->getDistanceMeasure(false,$data['szOriginPostCode'],$data['idOriginCountry']);
                    }
                    else if(!empty($data['szOriginCity']))
                    {
                            //getting laltitude and longitute by city
                            $distanceMeasureAry = $this->getDistanceMeasure($data['szOriginCity'],false,$data['idOriginCountry']);					
                    }
                    if((int)$idCountry <=0)
                    {
                            $distanceMeasureAry['idCountry'] = $data['idOriginCountry'] ;
                    }

                    $distanceMeasureAry['iDirection'] = 1;
                }

                if((int)$idWareHouse>0)
                {
                    $distanceMeasureAry['idWareHouse'] = $idWareHouse ;
                }
                if((int)$idForwarder>0)
                {
                    $distanceMeasureAry['idForwarder'] = $idForwarder ;
                }
                if((int)$idCountry>0)
                {
                    $distanceMeasureAry['idCountry'] = $idCountry ;
                }
                $distanceMeasureAry['idServiceType'] = $data['idServiceType'] ;
                /*
                echo "<br> distance ary <br> country id ".$idCountry;
                print_r($distanceMeasureAry);
                echo "<br><br>";

                $filename = __APP_PATH_ROOT__."/logs/debug.log";

                $strdata=array();
                $strdata[0]=" \n\n *** sending data to fetch warehaouses \n\n";
                $strdata[1] = print_R($distanceMeasureAry,true)."\n\n"; 
                file_log(true, $strdata, $filename);
                */
                if($data['iFromRequirementPage']==2 || $data['iFromRequirementPage']==3)
                {
                    if($warehouse == 'FROM')
                    {
                        $distanceMeasureAry['fTotalLatitude']=$data['fOriginLatitude'];
                        $distanceMeasureAry['fTotalLongitude']=$data['fOriginLongitude'];
                    }
                    else if($warehouse=='TO')
                    {
                        $distanceMeasureAry['fTotalLatitude']=$data['fDestinationLatitude'];
                        $distanceMeasureAry['fTotalLongitude']=$data['fDestinationLongitude'];
                    }
                } 
                
                $whsDistanceAry =array();
                $whsDistanceAry= $this->getDistanceFromAllWareHouses($distanceMeasureAry); 
                return $whsDistanceAry ;
            }		
	} 
	function set_iScore($value)
	{
		$this->iScore = $this->validateInput($value,__VLD_CASE_NUMERIC__,'iScore',t( $this->t_base."fields/iScore" ),false, false ,true );
	}
	
	function set_szComment($value)
	{
		$this->szComment = $this->validateInput($value,__VLD_CASE_ANYTHING__,'szComment',t( $this->t_base."fields/comment" ),false, false ,false );
	}
}

?>