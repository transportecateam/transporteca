<?php

/**
 * database.class.php
 *
 * @copyright Copyright (C) 2012 Transporteca, LLC
 * @author Anil
 * @package Transporteca Development
 */
if( !defined( "__APP_PATH__" ) )
    define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
ini_set('mysqlnd_qc.enable_qc', '1');
ini_set('mysqlnd_qc.cache_by_default', '1'); 
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_CLASSES__ . "/error.class.php" );

if (!function_exists('__autoload'))
{
    function __autoload($class_name)
    {
        require_once( __APP_PATH_CLASSES__ . "/error.class.php" );
    }
} 
/**
 * cDatabase is interfaced used to execute sql statements
 *
 * @package CR 3.0 Migration
 */
class cDatabase extends CError
{
    /** @var string */ 
    var $mysqli;
    /** @var string */
    var $szSQL;
    /** @var int */
    var $iLastInsertID;
    /** @var int */
    var $iNumRows;
    /** @var string */
    var $szTableName;
    /** @var string */
    var $szDatabase;
    /** @var int */
    var $iMySQLError;
    /** @var string */
    var $szMySQLError;
    public static $szDbConnection;

    //protected $_dbh = null;

    /**
     * class constructor for php5
     *
     * @return bool
     */
    function __construct()
    {
        parent::__construct();
        return true;
    }

    /**
     * class constructor for php4
     *
     * @return cDatabase
     */
    function cDatabase()
    {
        $argcv = func_get_args();
        call_user_func_array( array( &$this, '__construct' ), $argcv );
    }

    function connect($username, $password, $select_db, $host = 'localhost', $type="normal", $return_type="bool")
    {
        return true;	
    }
    /**
     * This function will initilize a db connect to the database.
     *
     * @author Anil
     * @param string $username
     * @param string $password
     * @param string $select_db
     * @param string $host
     * @param string $type
     * @param mixed $return type
     * @return resource
     */
    function connect_backup($username, $password, $select_db, $host = 'localhost', $type="normal", $return_type="bool",$utf8_encode=false)
    {
        $this->szMysqlUserName = $username ;
        if ($type == "persistant")
        {
            $this->_dbh = $dbh = mysqli_pconnect($host, $username, $password);
        }
        else
        {
            // $dbh = mysqli_connect($host, $username, $password);
            $this->_dbh = $dbh = mysqli_connect($host, $username, $password, $select_db);
            mysqli_set_charset('utf8',$dbh); 
            if($utf8_encode)
            {
                    //mysqli_set_charset('utf8',$dbh);
            }
        }

        if (!$dbh)
        {
            $this->szMySQLError = mysqli_error($this->_dbh);
            $this->logError( "HM_store", $this->szMySQLError, "MySQL", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }

        /*
        // select DB.
        $db_selected = mysqli_select_db($select_db, $dbh);
        if (!$db_selected)
        {
            $this->szMySQLError = mysqli_error();
            $this->logError( "HM_store", $this->szMySQLError, "MySQL", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
         * 
         */

        if ($return_type == "bool")
        {
                return true;
        }
        else
        {
                return $dbh;
        }
    }

    /**
     * Initiales the schema.table settings to use.
     *
     * @access private
     * @param string $schema
     * @param string $table
     */
    function initDatabase( $schema, $table )
    {
        $this->szDatabase = $schema;
        $this->szTableName = $table;
    }

    /**
     * Sets the SQL member
     *
     * @author Anil
     * @access public
     * @param string $sql
     */
    function setSQL( $sql )
    {
        $this->szSQL = trim((string)$sql);
    }

    /**
     * Executes the sql.
     *
     * @author Anil
     * @access public
     * @param string $sql
     * @return mysqli_query
     */
    function exeSQL( $sql = false, $resource=false )
    {             
        if(is_a(self::$szDbConnection, 'mysqli'))
        { 
            $dbh_sess = self::$szDbConnection; 
        }
        
        if(!is_a($dbh_sess, 'mysqli'))
        {   
            $dbh_sess = new mysqli(__DBC_HOST__, __DBC_USER__,__DBC_PASSWD__,__DBC_SCHEMATA__,__DBC_PORT__);
            
            /* check connection */
            if ($dbh_sess->connect_errno) 
            {
                $handle = fopen(__APP_PATH_LOGS__."/dbConnection.log", "a+");
                fwrite($handle, "Mysqli Connection error: ".$dbh_sess->connect_error." on " . date("d/m/Y H:i:s") . "\r\n");
                fclose($handle); 
                return false;
            }
            self::$szDbConnection = $dbh_sess; 
            if(is_a($dbh_sess, 'mysqli'))
            {  
                $dbh_sess->set_charset("utf8"); 
            }
        } 

        if ($sql !== false)
        {
            $this->setSQL( trim( $sql ) );
        }
        else
        {
            $sql = $this->szSQL;
        }   
        $result = $dbh_sess->query($sql);

        $this->iMySQLError = $dbh_sess->errno;
        $this->szMySQLError = $dbh_sess->error;	

        if( $this->iMySQLError != 0 )
        { 
            $this->logError( "database", "MySQL Error: " . $this->szMySQLError."\n\n".$sql, "MySQL", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
        else
        {
            if( preg_match( '/^INSERT INTO/i', $this->szSQL ) )
            {
                $this->iLastInsertID = $dbh_sess->insert_id;					
                return true;
            }
            elseif( preg_match( '/^SELECT/i', $this->szSQL ) )
            {
                $this->iNumRows = $result->num_rows;
            }
            return $result;
        }
    }

    /**
     * Returns a mysql associative array.
     *
     * @author Anil
     * @access public
     * @param mysqli_query $qry
     * @return mysqli_fetch_assoc
     */
    function getAssoc( $result )
    {
        return $result->fetch_assoc();
    }

    /**
     * Returns number of affected rows in the given MySQL operation.
     *
     * @author Anil
     * @access public
     * @param mysqli_query $qry
     * @return mysqli_affected_rows
     */
    function getRowCnt()
    {
        global $dbh_sess;
        $dbh_sess = self::$szDbConnection;  
        return $dbh_sess->affected_rows;
    }  
}
?>
