<?php
if(!isset($_SESSION))
{
	session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_INC__ . "/functions.php" );
require_once( __APP_PATH_CLASSES__ . "/database.class.php");

class cImportData extends cDatabase
{
    function __construct()
    {
            parent::__construct();
            return true;
    }

    function importTransportModeData()
    {
        $query="
            SELECT
                *
            FROM    
                ".__DBC_SCHEMATA_TRANSPORT_MODE__."
        ";
        
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
                while($row=$this->getAssoc($result))
                {
                    $resArr=array();
                    $idMapped=$row['id'];
                    $resArr[1]['szShortName']=$row['szShortName'];
                    $resArr[1]['szLongName']=$row['szLongName'];
                    $resArr[1]['szFrequency']=$row['szFrequency'];
                    
                    $resArr[2]['szShortName']=$row['szShortNameDanish'];
                    $resArr[2]['szLongName']=$row['szLongNameDanish'];
                    $resArr[2]['szFrequency']=$row['szFrequencyDanish'];
                    
                    $resArr[3]['szShortName']=$row['szShortNameDanish'];
                    $resArr[3]['szLongName']=$row['szLongNameDanish'];
                    $resArr[3]['szFrequency']=$row['szFrequencyDanish'];
                    
                    if(!empty($resArr))
                    {
                        foreach($resArr as $key=>$resArrs)
                        {
                            foreach($resArrs as $keys=>$value)
                            {
                                
                                $queryInsert="
                                    INSERT INTO
                                        ".__DBC_SCHEMATA_LANGUAGE_CONFIGURATION__."
                                    (
                                        idMapped,
                                        szFieldName,
                                        szValue,
                                        szTableKey,
                                        idLanguage
                                    )
                                    VALUES
                                    (
                                       '".(int)$idMapped."',
                                       '".mysql_escape_custom($keys)."',
                                       '".mysql_escape_custom($value)."',
                                       '__TABLE_TRANSPORT_MODE__',    
                                       '".(int)$key."'
                                    )
                                ";
                                $resultInsert = $this->exeSQL( $queryInsert );
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    function importWeightMeasureData()
    {
        $query="
            SELECT
                *
            FROM    
                ".__DBC_SCHEMATA_WEIGHT_MEASURE__."
        ";
        
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
                while($row=$this->getAssoc($result))
                {
                    $resArr=array();
                    $idMapped=$row['id'];
                    $resArr[1]['szDescription']=$row['szDescription'];
                    $resArr[1]['fKgFactor']=$row['fKgFactor'];
                    
                    $resArr[2]['szDescription']=$row['szDescriptionDanish'];
                    $resArr[2]['fKgFactor']=$row['fKgFactor'];
                    
                    $resArr[3]['szDescription']=$row['szDescriptionDanish'];
                    $resArr[3]['fKgFactor']=$row['fKgFactor'];
                    
                    if(!empty($resArr))
                    {
                        foreach($resArr as $key=>$resArrs)
                        {
                            foreach($resArrs as $keys=>$value)
                            {
                                
                                $queryInsert="
                                    INSERT INTO
                                        ".__DBC_SCHEMATA_LANGUAGE_CONFIGURATION__."
                                    (
                                        idMapped,
                                        szFieldName,
                                        szValue,
                                        szTableKey,
                                        idLanguage
                                    )
                                    VALUES
                                    (
                                       '".(int)$idMapped."',
                                       '".mysql_escape_custom($keys)."',
                                       '".mysql_escape_custom($value)."',
                                       '__TABLE_WEIGHT_MEASURE__',    
                                       '".(int)$key."'
                                    )
                                ";
                                //echo $queryInsert."<br /><br />";
                               $resultInsert = $this->exeSQL( $queryInsert );
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    function importCargoMeasureData()
    {
        $query="
            SELECT
                *
            FROM    
                ".__DBC_SCHEMATA_CARGO_MEASURE__."
        ";
        
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
                while($row=$this->getAssoc($result))
                {
                    $resArr=array();
                    $idMapped=$row['id'];
                    $resArr[1]['szDescription']=$row['szDescription'];
                    $resArr[1]['fCmFactor']=$row['fCmFactor'];
                    
                    $resArr[2]['szDescription']=$row['szDescriptionDanish'];
                    $resArr[2]['fCmFactor']=$row['fCmFactor'];
                    
                    $resArr[3]['szDescription']=$row['szDescriptionDanish'];
                    $resArr[3]['fCmFactor']=$row['fCmFactor'];
                    
                    if(!empty($resArr))
                    {
                        foreach($resArr as $key=>$resArrs)
                        {
                            foreach($resArrs as $keys=>$value)
                            {
                                
                                $queryInsert="
                                    INSERT INTO
                                        ".__DBC_SCHEMATA_LANGUAGE_CONFIGURATION__."
                                    (
                                        idMapped,
                                        szFieldName,
                                        szValue,
                                        szTableKey,
                                        idLanguage
                                    )
                                    VALUES
                                    (
                                       '".(int)$idMapped."',
                                       '".mysql_escape_custom($keys)."',
                                       '".mysql_escape_custom($value)."',
                                       '__TABLE_CARGO_MEASURE__',    
                                       '".(int)$key."'
                                    )
                                ";
                                //echo $queryInsert."<br /><br />";
                                $resultInsert = $this->exeSQL( $queryInsert );
                            }
                        }
                    }
                }
            }
        }
    }
    
    function importPalletTypeData()
    {
        $query="
            SELECT
                *
            FROM    
                ".__DBC_SCHEMATA_PALLET_TYPES__."
        ";
        
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
                while($row=$this->getAssoc($result))
                {
                    $resArr=array();
                    $idMapped=$row['id'];
                    $resArr[1]['szShortName']=$row['szShortName'];
                    $resArr[1]['szLongName']=$row['szLongName'];
                     $resArr[1]['fValue']=$row['fValue'];
                    
                    $resArr[2]['szShortName']=$row['szShortNameDanish'];
                    $resArr[2]['szLongName']=$row['szLongNameDanish'];
                    $resArr[2]['fValue']=$row['fValue'];
                    
                    $resArr[3]['szShortName']=$row['szShortNameDanish'];
                    $resArr[3]['szLongName']=$row['szLongNameDanish'];
                    $resArr[3]['fValue']=$row['fValue'];
                     
                    if(!empty($resArr))
                    {
                        foreach($resArr as $key=>$resArrs)
                        {
                            foreach($resArrs as $keys=>$value)
                            {
                                
                                $queryInsert="
                                    INSERT INTO
                                        ".__DBC_SCHEMATA_LANGUAGE_CONFIGURATION__."
                                    (
                                        idMapped,
                                        szFieldName,
                                        szValue,
                                        szTableKey,
                                        idLanguage
                                    )
                                    VALUES
                                    (
                                       '".(int)$idMapped."',
                                       '".mysql_escape_custom($keys)."',
                                       '".mysql_escape_custom($value)."',
                                       '__TABLE_PALLET_TYPE__',    
                                       '".(int)$key."'
                                    )
                                ";
                                //echo $queryInsert."<br /><br />";
                                $resultInsert = $this->exeSQL( $queryInsert );
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    function importCourierProviderProductPackingData()
    {
        $query="
            SELECT
                *
            FROM    
                ".__DBC_SCHEMATA_COUIER_PROVIDER_PRODUCTS_PACKING__."
        ";
        
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
                while($row=$this->getAssoc($result))
                {
                    $resArr=array();
                    $idMapped=$row['id'];
                    $resArr[1]['szPacking']=$row['szPacking'];
                    $resArr[1]['szPackingSingle']=$row['szPackingSingle'];
                    
                    $resArr[2]['szPacking']=$row['szPackingDanish'];
                    $resArr[2]['szPackingSingle']=$row['szPackingSingleDanish'];
                    
                    $resArr[3]['szPacking']=$row['szPackingDanish'];
                    $resArr[3]['szPackingSingle']=$row['szPackingSingleDanish'];
                     
                    if(!empty($resArr))
                    {
                        foreach($resArr as $key=>$resArrs)
                        {
                            foreach($resArrs as $keys=>$value)
                            {
                                
                                $queryInsert="
                                    INSERT INTO
                                        ".__DBC_SCHEMATA_LANGUAGE_CONFIGURATION__."
                                    (
                                        idMapped,
                                        szFieldName,
                                        szValue,
                                        szTableKey,
                                        idLanguage
                                    )
                                    VALUES
                                    (
                                       '".(int)$idMapped."',
                                       '".mysql_escape_custom($keys)."',
                                       '".mysql_escape_custom($value)."',
                                       '__TABLE_COURIER_PROVIDER_PRODUCT_PACKING__',    
                                       '".(int)$key."'
                                    )
                                ";
                                //echo $queryInsert."<br /><br />";
                                $resultInsert = $this->exeSQL( $queryInsert );
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    function importStandardCargoData()
    {
        $query="
            SELECT
                *
            FROM    
                ".__DBC_SCHEMATA_STANDARD_CARGO__."
        ";
        
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
                while($row=$this->getAssoc($result))
                {
                    $resArr=array();
                    $idMapped=$row['Id'];
                    $resArr[1]['szShortName']=$row['szShortName'];
                    $resArr[1]['szLongName']=$row['szLongName'];
                    
                    $resArr[2]['szShortName']=$row['szShortNameDanish'];
                    $resArr[2]['szLongName']=$row['szLongNameDanish'];
                    
                    $resArr[3]['szShortName']=$row['szShortNameSwedish'];
                    $resArr[3]['szLongName']=$row['szLongNameSwedish'];
                    
                    
                    $resArr[4]['szShortName']=$row['szShortNameNorwegian'];
                    $resArr[4]['szLongName']=$row['szLongNameNorwegian'];
                     
                    if(!empty($resArr))
                    {
                        foreach($resArr as $key=>$resArrs)
                        {
                            foreach($resArrs as $keys=>$value)
                            {
                                
                                $queryInsert="
                                    INSERT INTO
                                        ".__DBC_SCHEMATA_LANGUAGE_CONFIGURATION__."
                                    (
                                        idMapped,
                                        szFieldName,
                                        szValue,
                                        szTableKey,
                                        idLanguage
                                    )
                                    VALUES
                                    (
                                       '".(int)$idMapped."',
                                       '".mysql_escape_custom($keys)."',
                                       '".mysql_escape_custom($value)."',
                                       '__TABLE_STANDARD_CARGO__',    
                                       '".(int)$key."'
                                    )
                                ";
                                //echo $queryInsert."<br /><br />";
                                $resultInsert = $this->exeSQL( $queryInsert );
                            }
                        }
                    }
                }
            }
        }
    }
    
    function importStandardCargoCategoryData()
    {
        $query="
            SELECT
                *
            FROM    
                ".__DBC_SCHEMATA_STANDARD_CARGO_CATEGORY__."
        ";
        
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
                while($row=$this->getAssoc($result))
                {
                    $resArr=array();
                    $idMapped=$row['id'];
                    $resArr[1]['szCategoryName']=$row['szCategoryName'];
                    
                    $resArr[2]['szCategoryName']=$row['szCategoryNameDanish'];
                    
                    $resArr[3]['szCategoryName']=$row['szCategoryNameSwedish'];
                    
                    $resArr[4]['szCategoryName']=$row['szCategoryNameNorwegian'];
                     
                    if(!empty($resArr))
                    {
                        foreach($resArr as $key=>$resArrs)
                        {
                            foreach($resArrs as $keys=>$value)
                            {
                                
                                $queryInsert="
                                    INSERT INTO
                                        ".__DBC_SCHEMATA_LANGUAGE_CONFIGURATION__."
                                    (
                                        idMapped,
                                        szFieldName,
                                        szValue,
                                        szTableKey,
                                        idLanguage
                                    )
                                    VALUES
                                    (
                                       '".(int)$idMapped."',
                                       '".mysql_escape_custom($keys)."',
                                       '".mysql_escape_custom($value)."',
                                       '__TABLE_STANDARD_CARGO_CATEGORY__',    
                                       '".(int)$key."'
                                    )
                                ";
                               // echo $queryInsert."<br /><br />";
                                $resultInsert = $this->exeSQL( $queryInsert );
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    function importServiceTypeData()
    {
        $query="
            SELECT
                *
            FROM    
                ".__DBC_SCHEMATA_SERVICE_TYPE__."
        ";
        
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
                while($row=$this->getAssoc($result))
                {
                    $resArr=array();
                    $idMapped=$row['id'];
                    $resArr[1]['szDescription']=$row['szDescription'];                    
                    $resArr[1]['szShortName']=$row['szShortName'];
                    
                    $resArr[2]['szDescription']=$row['szDescriptionDanish'];                    
                    $resArr[2]['szShortName']=$row['szShortNameDanish'];
                    
                    $resArr[3]['szDescription']=$row['szDescriptionDanish'];                    
                    $resArr[3]['szShortName']=$row['szShortNameDanish'];
                     
                    if(!empty($resArr))
                    {
                        foreach($resArr as $key=>$resArrs)
                        {
                            foreach($resArrs as $keys=>$value)
                            {
                                
                                $queryInsert="
                                    INSERT INTO
                                        ".__DBC_SCHEMATA_LANGUAGE_CONFIGURATION__."
                                    (
                                        idMapped,
                                        szFieldName,
                                        szValue,
                                        szTableKey,
                                        idLanguage
                                    )
                                    VALUES
                                    (
                                       '".(int)$idMapped."',
                                       '".mysql_escape_custom($keys)."',
                                       '".mysql_escape_custom($value)."',
                                       '__TABLE_SERVICE_TYPE__',    
                                       '".(int)$key."'
                                    )
                                ";
                               // echo $queryInsert."<br /><br />";
                                $resultInsert = $this->exeSQL( $queryInsert );
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    function importShipConGroupData()
    {
        $query="
            SELECT
                *
            FROM    
                ".__DBC_SCHEMATA_SHIPPER_CONSIGNEES_GROUP__."
        ";
        
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
                while($row=$this->getAssoc($result))
                {
                    $resArr=array();
                    $idMapped=$row['id'];
                    $resArr[1]['szDescription']=$row['szDescription'];
                    
                    $resArr[2]['szDescription']=$row['szDescriptionDanish'];
                    
                    $resArr[3]['szDescription']=$row['szDescriptionDanish']; 
                     
                    if(!empty($resArr))
                    {
                        foreach($resArr as $key=>$resArrs)
                        {
                            foreach($resArrs as $keys=>$value)
                            {
                                
                                $queryInsert="
                                    INSERT INTO
                                        ".__DBC_SCHEMATA_LANGUAGE_CONFIGURATION__."
                                    (
                                        idMapped,
                                        szFieldName,
                                        szValue,
                                        szTableKey,
                                        idLanguage
                                    )
                                    VALUES
                                    (
                                       '".(int)$idMapped."',
                                       '".mysql_escape_custom($keys)."',
                                       '".mysql_escape_custom($value)."',
                                       '__TABLE_SHIP_CON_GROUP__',    
                                       '".(int)$key."'
                                    )
                                ";
                               //echo $queryInsert."<br /><br />";
                                $resultInsert = $this->exeSQL( $queryInsert );
                            }
                        }
                    }
                }
            }
        }
    }
    
    function importOriginDestinationData()
    {
        $query="
            SELECT
                *
            FROM    
                ".__DBC_SCHEMATA_CUSTOM_CLEARANCE__."
        ";
        
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
                while($row=$this->getAssoc($result))
                {
                    $resArr=array();
                    $idMapped=$row['id'];
                    $resArr[1]['szDescription']=$row['szDescription'];
                    
                    $resArr[2]['szDescription']=$row['szDescriptionDanish'];
                    
                    $resArr[3]['szDescription']=$row['szDescriptionDanish']; 
                     
                    if(!empty($resArr))
                    {
                        foreach($resArr as $key=>$resArrs)
                        {
                            foreach($resArrs as $keys=>$value)
                            {
                                
                                $queryInsert="
                                    INSERT INTO
                                        ".__DBC_SCHEMATA_LANGUAGE_CONFIGURATION__."
                                    (
                                        idMapped,
                                        szFieldName,
                                        szValue,
                                        szTableKey,
                                        idLanguage
                                    )
                                    VALUES
                                    (
                                       '".(int)$idMapped."',
                                       '".mysql_escape_custom($keys)."',
                                       '".mysql_escape_custom($value)."',
                                       '__TABLE_ORIGIN_DESTINATION__',    
                                       '".(int)$key."'
                                    )
                                ";
                                //echo $queryInsert."<br /><br />";
                                $resultInsert = $this->exeSQL( $queryInsert );
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    function importTimingTypeData()
    {
        $query="
            SELECT
                *
            FROM    
                ".__DBC_SCHEMATA_TIMING_TYPE__."
        ";
        
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
                while($row=$this->getAssoc($result))
                {
                    $resArr=array();
                    $idMapped=$row['id'];
                    $resArr[1]['szDescription']=$row['szDescription'];
                    
                    $resArr[2]['szDescription']=$row['szDescriptionDanish'];
                    
                    $resArr[3]['szDescription']=$row['szDescriptionDanish']; 
                     
                    if(!empty($resArr))
                    {
                        foreach($resArr as $key=>$resArrs)
                        {
                            foreach($resArrs as $keys=>$value)
                            {
                                
                                $queryInsert="
                                    INSERT INTO
                                        ".__DBC_SCHEMATA_LANGUAGE_CONFIGURATION__."
                                    (
                                        idMapped,
                                        szFieldName,
                                        szValue,
                                        szTableKey,
                                        idLanguage
                                    )
                                    VALUES
                                    (
                                       '".(int)$idMapped."',
                                       '".mysql_escape_custom($keys)."',
                                       '".mysql_escape_custom($value)."',
                                       '__TABLE_TIMING_TYPE__',    
                                       '".(int)$key."'
                                    )
                                ";
                                //echo $queryInsert."<br /><br />";
                                $resultInsert = $this->exeSQL( $queryInsert );
                            }
                        }
                    }
                }
            }
        }
    }
    
    function landingPage16Data()
    {
        $query="
            SELECT
                bf.szFileRef AS `File Reference`,
                bf.dtQuoteSend AS `Date/time we sent quote to customer`,
                cls.dtSent AS `Date/time we sent e-mail`,
                b.dtBookingConfirmed AS `Booking Date`
            FROM
                ".__DBC_SCHEMATA_BOOKING__." AS b
            INNER JOIN
                ".__DBC_SCHEMATA_FILE_LOGS__." AS bf
            ON
            	bf.idBooking=b.id
            INNER JOIN
                ".__DBC_SCHEMATA_CUSTOMER_LOG_SNIPPET__." AS cls
            ON
                cls.idBooking=b.id
            WHERE
                DATE(b.dtCreatedOn)>'2017-04-24'
            AND
                b.idLandingPage='16'
            AND
                b.iStandardPricing='1' 
            AND
                cls.`szEmailSubject`='Har du modtaget vores tilbud pÃ¥ transport af dine mÃ¸bler fra Voga?'
            AND
                cls.iEmailType='1'
            AND
                cls.idCustomerLogDataType='1'
            ";
        echo $query;
    }
}
?>