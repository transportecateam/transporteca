<?php

session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

class cServices extends cDatabase
{
    var $szLCLCalculationLogs = '';
	function __construct()
	{
            parent::__construct();
            return true;
	}
	
	function get_search_result($data)
	{
		if(!empty($data))
		{
			$warehouseFromAry = $this->getAllWareHousesByCountryPostCode($data,'FROM');
			$warehouseToAry = $this->getAllWareHousesByCountryPostCode($data,'TO');
		    echo "<br> from array<br> ";;
		    print_r($warehouseFromAry);
		    echo "<br> to array <br> ";
		    print_r($warehouseToAry);
		    echo "<br> final ary <br>";
		    
			if(!empty($warehouseFromAry) && !empty($warehouseToAry))
			{				
				if(!empty($data['dtTiming']))
				{
					$dateAry = explode("/",$data['dtTiming']);
					$date_str = $dateAry[2]."-".$dateAry[1]."-".$dateAry[0]." 23:59:59";
				}
				$date1 =date('Y-m-d H:i:s',strtotime($date_str));
				$date2= date('Y-m-d H:i:s');
				
				$seconds = strtotime($date1) - strtotime($date2);
				$hours = number_format((float)($seconds /3600),2);
				$from_ware_house_str = implode(",",$warehouseFromAry['id']);
				$to_ware_house_str = implode(",",$warehouseToAry['id']);
				if($data['idTimingType']==1)
				{
					$query_and = " AND ";
				}				
				$query="
					SELECT
						wtw.id idWTW,
						wtw.idWareHouseFrom,
						wtw.idWareHouseTo,
						wtw.iCutOffHours,
						wtw.idCutOffDay,
						wtw.szCutOffLocalTime,
						wtw.idAvailableDay,
						wtw.szAvailableLocalTime,
						wtw.iTransitDay,
						fwd.szDisplayName,
						fwd.szLogo,
						whs.idForwarder
					FROM				
						".__DBC_SCHEMATA_WAREHOUSES_PRICING__." wtw,
						".__DBC_SCHEMATA_WAREHOUSES__." whs
					INNER JOIN
						".__DBC_SCHEMATA_FROWARDER__." fwd	
					ON
						fwd.id = whs.idForwarder	
					WHERE
						(
							wtw.idWareHouseTo = whs.id
						OR
							wtw.idWareHouseFrom = whs.id	
						)
					AND	
						wtw.dtValidFrom < now()
					AND
						wtw.dtExpiry > now()		
					AND
						idWareHouseTo IN (".mysql_escape_custom($to_ware_house_str).")	
					AND
						idWareHouseFrom IN (".mysql_escape_custom($from_ware_house_str).")	
				   GROUP BY
				   	 	idWTW
				";
				echo $query ;
				if($result=$this->exeSQL($query))
				{
					$resultAry=array();
					$ctr=0;
					while($row=$this->getAssoc($result))
					{
						$resultAry[$ctr]=$row;
					}
					//echo "<br> result array <br><br>";
					//print_r($resultAry);
					return $resultAry ;
				}
			    else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
			}	
		}
	}
	
	function getAllWareHousesByCountryPostCode($data,$warehouse=false)
	{
		if(!empty($data))
		{
			if($warehouse=='FROM')
			{
				$idCountry = $data['szOriginCountry'] ;
				if(!empty($data['szOriginPostCode']))
				{
					$fromCityPostCodeAry =array();
					$fromCityPostCodeAry = explode("-",$data['szOriginPostCode']);
					$query_and = " AND 
							   (
							        szCity = '".mysql_escape_custom(trim($fromCityPostCodeAry[0]))."'
						";
					if(!empty($fromCityPostCodeAry[1]))
					{
						$query_and .= " OR szPostCode = '".mysql_escape_custom(trim($fromCityPostCodeAry[1]))."'";
					}
					$query_and .=" ) " ;
				}
			}
						
			if($warehouse == 'TO')
			{
				$idCountry = $data['szDestinationCountry'] ;
				if(!empty($data['szDestinationPostCode']))
				{
					$toCityPostCodeAry=array();
					$toCityPostCodeAry = explode("-",$data['szDestinationPostCode']);
					$query_and = " AND 
							   (
							   		szCity = '".mysql_escape_custom(trim($toCityPostCodeAry[0]))."' ";
					if(!empty($toCityPostCodeAry[1]))
					{
						$query_and .=" OR szPostCode = '".mysql_escape_custom(trim($toCityPostCodeAry[1]))."'";
					}
					$query_and .=" ) " ;
				}
			}
			
			$query="
				SELECT
					id,
					szLatitude,
					szLongitude
				FROM
					".__DBC_SCHEMATA_WAREHOUSES__."   
				WHERE
				  	idCountry = '".(int)$idCountry."'	
					".$query_and."
			";
			echo $query."<br>" ;
			if($result = $this->exeSQL($query))
			{
				$wareHouseAry = array();
				$from_ctr=0;
				$ctr = 0;
				while($row=$this->getAssoc($result))
				{
					$wareHouseAry['id'][$ctr] = $row['id'];
					$wareHouseAry['szLatitude'][$row['id']] = $row['szLatitude'];
					$wareHouseAry['szLongitude'][$row['id']] = $row['szLongitude'];
					$ctr++;
				}
				return $wareHouseAry;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}

	function getAllActiveForwarder()
	{
		$query="
			SELECT
				id,
				szDisplayName
			FROM
				".__DBC_SCHEMATA_FROWARDER__."
			WHERE
				iActive='1'		
		";
		if($result=$this->exeSQL($query))
		{
			$forwarderAry=array();
			$ctr=0;
			while($row=$this->getAssoc($result))
			{
				$forwarderAry[$ctr] = $row;
				$ctr++;
			}
			return $forwarderAry ;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
        
    function getAllDummyServices($szSortField=false,$szSortOrder=false,$idDummyService=0,$searchAry=array())
    {
        $iBookingLanguage = __LANGUAGE_ID_ENGLISH__;
        $kConfig = new cConfig();
        $transportModeListAry = array();
        $transportModeListAry = $kConfig->getAllTransportMode(false,$iBookingLanguage,true,true);
            
        if($szSortField!="" && $szSortOrder!="")
        {
            $orderBY="ORDER BY ".$szSortField." ".$szSortOrder;
        }
        else
        {
            $orderBY="ORDER BY cnm.szName ASC, dp.idTrade DESC, dp.idProduct DESC, dp.fPriceIncrease ASC, dp.iTransitTimeIncrease ASC";
        }
        $query_and = '';
        if((int)$idDummyService>0)
        {
            $query_and = "AND dp.id = '".(int)$idDummyService."'";
        }  
        if(!empty($searchAry))
        {
            if($searchAry['idCountry']>0)
            {
                $query_and .= "AND dp.idCountry = '".(int)$searchAry['idCountry']."'";
            }
            if(!empty($searchAry['countryAry']))
            {
                $countryStr = implode($searchAry['countryAry'],",");
                $query_and .= "AND dp.idCountry IN (".$countryStr.")";
            }
            if($searchAry['idTrade']>0)
            {
                $query_and .= "AND dp.idTrade = '".(int)$searchAry['idTrade']."'";
            }
            if($searchAry['idProduct']>0)
            {
                $query_and .= "AND dp.idProduct = '".(int)$searchAry['idProduct']."'";
            }
        }
        
        $query="
            SELECT
                dp.id,
                dp.idCountry,
                dp.idTrade,
                dp.idProduct,
                dp.fPriceIncrease,
                dp.iTransitTimeIncrease, 
                dp.dtCreated,
                cnm.szName AS szCountryName
            FROM
                ".__DBC_SCHEMATA_DUMMY_PRICING__." AS dp
            INNER JOIN
                ".__DBC_SCHEMATA_COUNTRY_NAME_MAPPING__." AS cnm
            ON
                cnm.idCountry=dp.idCountry        
            WHERE
                dp.isDeleted = '0'
            AND
                cnm.idLanguage='".(int)$iBookingLanguage."'
            $query_and        
            $orderBY    
        ";
        $BookingFileName = __APP_PATH_ROOT__."/logs/servicelag.log";
            $strdata=array();
            $strdata[0]="query\n\n"; 
            $strdata[1]="query: ".$query."\n" ;
            file_log(true, $strdata, $BookingFileName);
        if($result=$this->exeSQL($query))
        {
            $forwarderAry=array();
            $ctr=0;
            while($row=$this->getAssoc($result))
            {
                $forwarderAry[$ctr] = $row;
                $forwarderAry[$ctr]['szProductName'] = $transportModeListAry[$row['idProduct']]['szShortName'];
                
                $ctr++;
            } 
            return $forwarderAry ;
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
    function addUpdateDummyServices($data)
    { 
        if(!empty($data))
        {   
            $this->set_idCountry(sanitize_specific_html_input(trim($data['idCountry'])));
            $this->set_idTrade(sanitize_specific_html_input(trim($data['idTrade'])));
            $this->set_idProduct(sanitize_specific_html_input(trim($data['idProduct'])));
            $this->set_fPriceIncrease(sanitize_specific_html_input(trim($data['fPriceIncrease'])));
            $this->set_iTransitTimeIncrease(sanitize_specific_html_input(trim($data['iTransitTimeIncrease'])));
            $this->set_idDummyService(sanitize_specific_html_input(trim($data['idDummyService'])));

            //exit if error exist.
            if ($this->error === true)
            {
                return false;
            } 

            if($this->idDummyService>0)
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_DUMMY_PRICING__."
                    SET
                        idCountry = '".(int)$this->idCountry."',
                        idTrade = '".mysql_escape_custom($this->idTrade)."',
                        idProduct = '".mysql_escape_custom($this->idProduct)."',
                        fPriceIncrease = '".mysql_escape_custom($this->fPriceIncrease)."',
                        iTransitTimeIncrease = '".mysql_escape_custom($this->iTransitTimeIncrease)."', 
                        dtUpdated = now() 
                    WHERE
                        id='".(int)$this->idDummyService."'   					
                ";
            }
            else
            {
                $query="
                    INSERT INTO
                        ".__DBC_SCHEMATA_DUMMY_PRICING__."
                    (
                        idCountry,
                        idTrade,
                        idProduct,
                        fPriceIncrease,
                        iTransitTimeIncrease, 
                        dtCreated 
                    )
                    VALUES
                    (
                        '".(int)$this->idCountry."',
                        '".mysql_escape_custom(trim($this->idTrade))."',
                        '".mysql_escape_custom(trim($this->idProduct))."',
                        '".mysql_escape_custom(trim($this->fPriceIncrease))."', 
                        '".mysql_escape_custom(trim($this->iTransitTimeIncrease))."', 
                        now() 
                    )
                ";
            }
//            echo "<br>".$query;
//            die; 
            if(($result = $this->exeSQL($query)))
            { 
                return true;  
            } 
            else
            {
                return false;
            }
        }
    } 
    
    
    function deleteDummyService($idDummyServiceStr)
    {
        if(!empty($idDummyServiceStr))
        {
            $idDummyServiceNewStr=  str_replace(";", "','", $idDummyServiceStr);            
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_DUMMY_PRICING__."
                SET
                    isDeleted = '1' 
                WHERE
                    id IN ('".$idDummyServiceNewStr."')   					
            ";
            if($result = $this->exeSQL($query))
            {
                return true;
            }
            else
            {
                return false;
            }
        } 
    } 
    
    function addUpdateDummyServicesException($data)
    { 
        if(!empty($data))
        {    
            $this->set_idOriginCountry(sanitize_specific_html_input(trim($data['idOriginCountry'])));
            $this->set_idDestinationCountry(sanitize_specific_html_input(trim($data['idDestinationCountry'])));
            $this->set_idProduct(sanitize_specific_html_input(trim($data['idProduct']))); 
            $this->set_idDummyServiceException(sanitize_specific_html_input(trim($data['idDummyServiceException'])));

            //exit if error exist.
            if ($this->error === true)
            { 
                return false;
            }
            if($this->isDummyExceptionExists($data))
            { 
                $this->addError('szSpecialError','Selected combination already exists.');
                return false;
            }
            if($this->idDummyServiceException>0)
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_DUMMY_PRICING_EXCEPTION__."
                    SET
                        idOriginCountry = '".(int)$this->idOriginCountry."',
                        idDestinationCountry = '".mysql_escape_custom($this->idTrade)."',
                        idProduct = '".mysql_escape_custom($this->idProduct)."', 
                        dtUpdatedOn = now() 
                    WHERE
                        id='".(int)$this->idDummyServiceException."'   					
                ";
            }
            else
            {
                $query="
                    INSERT INTO
                        ".__DBC_SCHEMATA_DUMMY_PRICING_EXCEPTION__."
                    (
                        idOriginCountry,
                        idDestinationCountry,
                        idProduct, 
                        dtCreatedOn 
                    )
                    VALUES
                    (
                        '".(int)$this->idOriginCountry."',
                        '".mysql_escape_custom(trim($this->idDestinationCountry))."',
                        '".mysql_escape_custom(trim($this->idProduct))."', 
                        now() 
                    )
                ";
            } 
//            echo $query;
//            die;
            if(($result = $this->exeSQL($query)))
            { 
                return true;  
            } 
            else
            {
                return false;
            }
        }
    } 
     
    function isDummyExceptionExists($data)
    { 
        if(!empty($data))
        {  
            $query="
                SELECT
                    id 
                FROM
                    ".__DBC_SCHEMATA_DUMMY_PRICING_EXCEPTION__."
                WHERE
                    idOriginCountry = '".mysql_escape_custom($data['idOriginCountry'])."'
                AND
                    idDestinationCountry = '".mysql_escape_custom($data['idDestinationCountry'])."'
                AND
                    idProduct = '".mysql_escape_custom($data['idProduct'])."'
                AND
                    isDeleted ='0'
            "; 
            if((int)$data['idDummyServiceException']>0)
            {
                $query .="
                    AND
                        id<>'".(int)$data['idDummyServiceException']."'		
                ";
            }
            //echo "<br>".$query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                // Check if the user exists
                if( $this->getRowCnt() > 0 )
                { 
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        } 
    }
    function getAllDummyServicesException($szSortField='szOriginCoutntryName',$szSortOrder='ASC',$idDummyServiceException=0)
    {
        $iBookingLanguage = __LANGUAGE_ID_ENGLISH__;
        $kConfig = new cConfig();
        $transportModeListAry = array();
        $transportModeListAry = $kConfig->getAllTransportMode(false,$iBookingLanguage,true,true);
        
        $countryListAry = array();
        $countryListAry = $kConfig->getAllCountryInKeyValuePair(false,false,false,$iBookingLanguage);
             
        $queryWhere='';
        if((int)$idDummyServiceException>0)
        {
            $queryWhere= "AND dp.id = '".(int)$idDummyServiceException."'";
        }
        $query="
            SELECT
                dp.id,
                dp.idOriginCountry,
                dp.idDestinationCountry,
                dp.idProduct, 
                dp.dtCreatedOn 
            FROM
                ".__DBC_SCHEMATA_DUMMY_PRICING_EXCEPTION__." AS dp 
            WHERE
                dp.isDeleted = '0' 
            $queryWhere         
        ";
        //echo $query;
        if($result=$this->exeSQL($query))
        {
            $dummyExceptionAry=array();
            $ctr=0;
            while($row=$this->getAssoc($result))
            {
                $dummyExceptionAry[$ctr] = $row;
                $dummyExceptionAry[$ctr]['szProductName'] = $transportModeListAry[$row['idProduct']]['szShortName'];
                $dummyExceptionAry[$ctr]['szOriginCoutntryName'] = $countryListAry[$row['idOriginCountry']]['szCountryName'];
                $dummyExceptionAry[$ctr]['szDestinationCoutntryName'] = $countryListAry[$row['idDestinationCountry']]['szCountryName'];
                $ctr++;
            }  
            if(!empty($dummyExceptionAry) && !empty($szSortField))
            {
                if($szSortOrder == "DESC")
                {
                    $dummyExceptionAry = sortArray($dummyExceptionAry, $szSortField , "DESC");
                }
                else
                {
                    $dummyExceptionAry = sortArray($dummyExceptionAry, $szSortField);
                }
            }
            return $dummyExceptionAry ;
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
    
    function deleteDummyServiceException($idDummyServiceStr)
    {
        if(!empty($idDummyServiceStr))
        {
            $idDummyServiceNewStr=  str_replace(";", "','", $idDummyServiceStr);            
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_DUMMY_PRICING_EXCEPTION__."
                SET
                    isDeleted = '1' 
                WHERE
                    id IN ('".$idDummyServiceNewStr."')   					
            "; 
            if($result = $this->exeSQL($query))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    
    function getDummyServicesList($postSearchAry)
    {
        if(!empty($postSearchAry))
        {
            /*
            * Checking to see whether we nee to display dummy services or not?
            */
            $bEligibleDummyLclService = true;
            $bEligibleDummyCourierService = true;
            
            $this->iDisplayLCLDummyService =  0;
            $this->iDisplayCouirierDummyService =  0;
 
            $dummyExceptionAry = array();
            $dummyExceptionAry['idOriginCountry'] = $postSearchAry['idOriginCountry'];
            $dummyExceptionAry['idDestinationCountry'] = $postSearchAry['idDestinationCountry'];
            $dummyExceptionAry['idProduct'] = __BOOKING_TRANSPORT_MODE_SEA__;
            /*
            *  checking to see if searched trade with Product: LCL is added in exception 
            */
            if($this->isDummyExceptionExists($dummyExceptionAry))
            {
                /*
                *  If the trade is added to exception list then we don't show dummy services for the Trade
                */ 
                $bEligibleDummyLclService = false;
            } 
            $dummyExceptionAry['idProduct'] = __BOOKING_TRANSPORT_MODE_COURIER__;
            
            /*
            *  checking to see if searched trade with Product: Courier is added in exception 
            */
            if($this->isDummyExceptionExists($dummyExceptionAry))
            {
                /*
                *  If the trade is added to exception list then we don't show dummy services for the Trade
                */ 
                $bEligibleDummyCourierService = false;
            }  
            
            $lclExportDummyService = array();
            $lclImportDummyService = array();
            $courierExportDummyService = array();
            $courierImportDummyService = array();
            
            if($bEligibleDummyLclService)
            {
                /*
                * fetching export dummy services
                */
                $dummySearchAry = array();
                $lclDummyServiceExportAry=array();
                $dummySearchAry['idCountry'] = $postSearchAry['idOriginCountry']; 
                $dummySearchAry['idTrade'] = 2; 
                $dummySearchAry['idProduct'] = __BOOKING_TRANSPORT_MODE_SEA__;
                $lclDummyServiceExportAry = $this->getAllDummyServices(false,false,false,$dummySearchAry);
                
                $lclDummyServiceImportAry=array();
                $dummySearchAry = array();
                $dummySearchAry['idTrade'] = 1; 
                $dummySearchAry['idCountry'] = $postSearchAry['idDestinationCountry']; 
                $dummySearchAry['idProduct'] = __BOOKING_TRANSPORT_MODE_SEA__;
                $lclDummyServiceImportAry = $this->getAllDummyServices(false,false,false,$dummySearchAry);
                $lclDummyServiceAry = array_merge($lclDummyServiceExportAry,$lclDummyServiceImportAry);
                if(!empty($lclDummyServiceAry))
                {
                    $this->iDisplayLCLDummyService = 1;
                } 
                /*
                * fetching import dummy services
                
                $dummySearchAry = array();
                $dummySearchAry['idCountry'] = $postSearchAry['idDestinationCountry'];
                $dummySearchAry['idTrade'] = __TRADE_IMPORT__;
                $dummySearchAry['idProduct'] = __BOOKING_TRANSPORT_MODE_SEA__;
                $lclImportDummyServiceAry = $this->getAllDummyServices(false,false,false,$dummySearchAry);
                 * */ 
            } 
            if($bEligibleDummyCourierService)
            {
                /*
                * fetching export dummy services
                */
                $dummySearchAry = array();
                $courierDummyServiceExportAry=array();
                $dummySearchAry['idCountry'] = $postSearchAry['idOriginCountry']; 
                $dummySearchAry['idTrade'] = 2;
                $dummySearchAry['idProduct'] = __BOOKING_TRANSPORT_MODE_COURIER__;
                $courierDummyServiceExportAry = $this->getAllDummyServices(false,false,false,$dummySearchAry);
                
                
                $dummySearchAry = array();
                $courierDummyServiceImportAry = array();
                $dummySearchAry['idTrade'] = 1;
                $dummySearchAry['idCountry'] = $postSearchAry['idDestinationCountry']; 
                $dummySearchAry['idProduct'] = __BOOKING_TRANSPORT_MODE_COURIER__;
                $courierDummyServiceImportAry = $this->getAllDummyServices(false,false,false,$dummySearchAry);
                $courierDummyServiceAry = array_merge($courierDummyServiceExportAry,$courierDummyServiceImportAry);
                if(!empty($courierDummyServiceAry))
                {
                    $this->iDisplayCouirierDummyService = 1;
                }
                
                /*
                * fetching import dummy services
                
                $dummySearchAry = array();
                $dummySearchAry['idCountry'] = $postSearchAry['idDestinationCountry'];
                $dummySearchAry['idTrade'] = __TRADE_IMPORT__;
                $dummySearchAry['idProduct'] = __BOOKING_TRANSPORT_MODE_COURIER__;
                $courierImportDummyServiceAry = $this->getAllDummyServices(false,false,false,$dummySearchAry);
                 * 
                 */
            }
            
            $finalDummyServiceAry = array();
            $finalDummyServiceAry['lcl'] = $lclDummyServiceAry;
            $finalDummyServiceAry['couier'] = $courierDummyServiceAry;
            return $finalDummyServiceAry;
        }
    }
    
    function addEditRailTransport($data)
    {
        if(!empty($data))
        {
            $this->set_idForwarder(sanitize_specific_html_input(trim($data['idForwarder'])));
            $this->set_idFromWarehouse(sanitize_specific_html_input(trim($data['idFromWarehouse'])));
            $this->set_idToWarehouse(sanitize_specific_html_input(trim($data['idToWarehouse'])));
            $this->set_idRailTransport(sanitize_specific_html_input(trim($data['idRailTransport'])));
            $this->iDTD = sanitize_specific_html_input(trim($data['iDTD']));
            //exit if error exist.
            if ($this->error === true)
            { 
                return false;
            }
            if($this->isRailTransportExists($data))
            { 
                $this->addError('szSpecialError','Selected combination already exists.');
                return false;
            }
            
            $kWhsSearchFrom = new cWHSSearch();
            $kWhsSearchFrom->load($this->idFromWarehouse); 
            $szFromWarehouseCity = $kWhsSearchFrom->szCity;
            $idFromWarehouseCountry = $kWhsSearchFrom->idCountry;
            $szFromWarehouseName = $kWhsSearchFrom->szWareHouseName;
 
            $kConfigFrom = new cConfig();
            $kConfigFrom->loadCountry($idFromWarehouseCountry); 
            $szFromWarehouseCountry = $kConfigFrom->szCountryName;
            
            $kWhsSearchTo = new cWHSSearch();
            $kWhsSearchTo->load($this->idToWarehouse); 
            $szToWarehouseCity = $kWhsSearchTo->szCity;
            $idToWarehouseCountry = $kWhsSearchTo->idCountry;
            $szToWarehouseName = $kWhsSearchTo->szWareHouseName;
             
            $kConfigTo = new cConfig();
            $kConfigTo->loadCountry($idToWarehouseCountry); 
            $szToWarehouseCountry = $kConfigTo->szCountryName;
            
            if($this->idRailTransport>0)
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_RAIL_TRANSPORT__."
                    SET
                        idForwarder = '".(int)$this->idForwarder."',
                        iDTD = '".(int)$this->iDTD."',
                        idFromWarehouse = '".mysql_escape_custom($this->idFromWarehouse)."',
                        idToWarehouse = '".mysql_escape_custom($this->idToWarehouse)."', 
                        szFromWarehouseName = '".mysql_escape_custom($szFromWarehouseName)."',
                        szToWarehouseName = '".mysql_escape_custom($szToWarehouseName)."', 
                        szFromWarehouseCity = '".mysql_escape_custom($szFromWarehouseCity)."',
                        szFromWarehouseCountry = '".mysql_escape_custom($szFromWarehouseCountry)."',
                        idFromWarehouseCountry = '".mysql_escape_custom($idFromWarehouseCountry)."',
                        szToWarehouseCity = '".mysql_escape_custom($szToWarehouseCity)."',
                        szToWarehouseCountry = '".mysql_escape_custom($szToWarehouseCountry)."',
                        idToWarehouseCountry = '".mysql_escape_custom($idToWarehouseCountry)."',
                        dtUpdatedOn = now() 
                    WHERE
                        id='".(int)$this->idRailTransport."'   					
                ";
            }
            else
            {
                $query="
                    INSERT INTO
                        ".__DBC_SCHEMATA_RAIL_TRANSPORT__."
                    (
                        idForwarder,
                        iDTD,
                        idFromWarehouse,
                        szFromWarehouseName,
                        idToWarehouse, 
                        szToWarehouseName,
                        szFromWarehouseCity,
                        szFromWarehouseCountry,
                        idFromWarehouseCountry,
                        szToWarehouseCity,
                        szToWarehouseCountry,
                        idToWarehouseCountry,
                        iActive,
                        dtCreatedOn 
                    )
                    VALUES
                    (
                        '".(int)$this->idForwarder."',
                        '".(int)$this->iDTD."',
                        '".mysql_escape_custom(trim($this->idFromWarehouse))."',
                        '".mysql_escape_custom($szFromWarehouseName)."',
                        '".mysql_escape_custom(trim($this->idToWarehouse))."', 
                        '".mysql_escape_custom($szToWarehouseName)."',
                        '".mysql_escape_custom($szFromWarehouseCity)."',
                        '".mysql_escape_custom($szFromWarehouseCountry)."',
                        '".mysql_escape_custom($idFromWarehouseCountry)."',
                        '".mysql_escape_custom($szToWarehouseCity)."',
                        '".mysql_escape_custom($szToWarehouseCountry)."',
                        '".mysql_escape_custom($idToWarehouseCountry)."',
                        1,
                        now()
                    )
                ";
            } 
//            echo $query;
//            die;
            if(($result = $this->exeSQL($query)))
            { 
                return true;  
            } 
            else
            {
                return false;
            }
        }
    } 
    
    function deleteRailTransport($railTransportIdsStr)
    {
        if(!empty($railTransportIdsStr))
        {
            $railTransportIdsNewStr=  str_replace(";", "','", $railTransportIdsStr);            
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_RAIL_TRANSPORT__."
                SET
                    isDeleted = '1' 
                WHERE
                    id IN ('".$railTransportIdsNewStr."')   					
            "; 
            if($result = $this->exeSQL($query))
            {
                return true;
            }
            else
            {
                return false;
            }
        } 
    }
    function deleteRailTransportByWarehouseId($idWarehouse)
    {
        if($idWarehouse>0)
        {        
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_RAIL_TRANSPORT__."
                SET
                    isDeleted = '1' 
                WHERE
                    idFromWarehouse = '".(int)$idWarehouse."' 	
                OR
                    idToWarehouse = '".(int)$idWarehouse."'
            "; 
            //echo $query; 
            if($result = $this->exeSQL($query))
            {
                return true;
            }
            else
            {
                return false;
            }
        } 
    }
    function isRailTransportExists($data)
    { 
        if(!empty($data))
        {  
            $this->iRailDTDAvailable = false;
            
            $query="
                SELECT
                    id,
                    iDTD
                FROM
                    ".__DBC_SCHEMATA_RAIL_TRANSPORT__."
                WHERE
                    idForwarder = '".mysql_escape_custom($data['idForwarder'])."'
                AND
                    idFromWarehouse = '".mysql_escape_custom($data['idFromWarehouse'])."'
                AND
                    idToWarehouse = '".mysql_escape_custom($data['idToWarehouse'])."'
                AND
                    isDeleted ='0'
            "; 
            if((int)$data['idRailTransport']>0)
            {
                $query .="
                    AND
                        id<>'".(int)$data['idRailTransport']."'		
                ";
            }
           // echo "<br>".$query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                // Check if the user exists
                if( $this->getRowCnt() > 0 )
                { 
                    while($row=$this->getAssoc($result))
                    {
                        if($row['iDTD']==1)
                        {
                            $this->iRailDTDAvailable = 1;
                        }  
                    } 
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        } 
    }
    
    function getAllRailTransport($idRailTransport=false,$szSortField=false,$szSortOrder=false)
    {  
        $queryWhere='';
        if((int)$idRailTransport>0)
        {
            $queryWhere= "AND rt.id = '".(int)$idRailTransport."'";
        } 
        $query_order_by = "ORDER BY ";
        if(!empty($szSortField))
        {
            if($szSortField=='szFromWarehouse')
            {
                $query_order_by .= " rt.szFromWarehouseCountry ".$szSortOrder.", rt.szFromWarehouseCity ".$szSortOrder.", rt.szFromWarehouseName ".$szSortOrder;
            }
            else if($szSortField=='szToWarehouse')
            {
                $query_order_by .= " rt.szToWarehouseCountry ".$szSortOrder.", rt.szToWarehouseCity ".$szSortOrder.", rt.szToWarehouseName ".$szSortOrder;
            }
            else
            {
                $query_order_by .= " f.szDisplayName ".$szSortOrder.", f.szForwarderAlias ".$szSortOrder;
            }
        }
        else
        {
            $query_order_by .= " f.szDisplayName ASC, f.szForwarderAlias ASC ";
        }
        
        $query="
            SELECT
                rt.id as idRailTransport,
                rt.idForwarder,
                rt.idFromWarehouse,
                rt.szFromWarehouseName,
                rt.idToWarehouse, 
                rt.szToWarehouseName,
                rt.szFromWarehouseCity,
                rt.szFromWarehouseCountry,
                rt.idFromWarehouseCountry,
                rt.szToWarehouseCity,
                rt.szToWarehouseCountry,
                rt.idToWarehouseCountry,
                rt.iDTD,
                rt.iActive,
                rt.dtCreatedOn,
                f.szDisplayName,
                f.szForwarderAlias
            FROM
                ".__DBC_SCHEMATA_RAIL_TRANSPORT__." AS rt
            INNER JOIN
                ".__DBC_SCHEMATA_FORWARDERS__." as f
            ON
                f.id = rt.idForwarder
            WHERE
                rt.isDeleted = '0' 
            $queryWhere      
                $query_order_by
        ";
        //echo "<br><br>".$query."<br><br>";
        if($result=$this->exeSQL($query))
        {
            $dummyExceptionAry=array();
            $ctr=0;
            while($row=$this->getAssoc($result))
            {
                $dummyExceptionAry[$ctr] = $row; 
                $ctr++;
            }   
            return $dummyExceptionAry ;
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
    
    function getAllunpaidBookings()
    { 
        /*
         * SELECT b.id AS idBooking, b.iHandlingFeeApplicable, b.idQuotePricingDetails, b.szBookingRef
FROM transporteca.tblbookings AS b
WHERE b.idBookingStatus
IN ( 3, 4 )
AND DATE( b.dtBookingConfirmed ) >= '2016-08-01'
AND idCustomerCurrency != idForwarderCurrency
ORDER BY `b`.`idQuotePricingDetails` ASC
         */
        $query="
            SELECT  
                b.id AS idBooking,
                b.iHandlingFeeApplicable,
                b.idQuotePricingDetails,
                b.szBookingRef
            FROM
                ".__DBC_SCHEMATA_BOOKING__." AS b 
            INNER JOIN
                ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." as ft
            ON
                ft.idBooking = b.id
            WHERE  
                ft.iStatus='1'
            AND
                iBatchNo = 0
            AND
                iDebitCredit! = '5'
            AND
                b.idBookingStatus IN (3,4)
            AND
                DATE(b.dtBookingConfirmed) >= '2016-08-01'
            AND 
                b.idCustomerCurrency != b.idForwarderCurrency
            ORDER BY
                b.dtBookingConfirmed DESC
        ";
        //echo "<br/>".$query."<br />";
        if($result = $this->exeSQL( $query ))
        {
            if ($this->getRowCnt() > 0)
            {	
                $manageVar=array();
                while($row=$this->getAssoc($result))
                {	
                    $manageVar[]=$row;
                }
                return $manageVar;
            }
            else
            {
                return array();
            }	
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
    
    function updateCustomerDetailInBooking($idUser,$postSearchAry,$iStep=false)
    {
        if($idUser>0 && !empty($postSearchAry))
        {
            $kUser = new cUser();
            $kUser->getUserDetails($idUser);
            
            $res_ary=array();
            $res_ary['idUser'] = $kUser->id ;				
            $res_ary['szFirstName'] = $kUser->szFirstName ;
            $res_ary['szLastName'] = $kUser->szLastName ;
            $res_ary['szEmail'] = $kUser->szEmail ;	
            $res_ary['szCustomerAddress1'] = $kUser->szAddress1;
            $res_ary['szCustomerAddress2'] = $kUser->szAddress2;
            $res_ary['szCustomerAddress3'] = $kUser->szAddress3;
            $res_ary['szCustomerPostCode'] = $kUser->szPostcode;
            $res_ary['szCustomerCity'] = $kUser->szCity;
            $res_ary['szCustomerCountry'] = $kUser->szCountry;
            $res_ary['szCustomerState'] = $kUser->szState;
            $res_ary['szCustomerPhoneNumber'] = $kUser->szPhoneNumber;
            $res_ary['szCustomerCompanyName'] = $kUser->szCompanyName;	
            $res_ary['szCustomerCompanyRegNo'] = $kUser->szCompanyRegNo;	
            if((int)$postSearchAry['iBookingStep']<$iStep)
            {
                $res_ary['iBookingStep'] = $iStep ;
            }

            if(!empty($res_ary))
            {
                foreach($res_ary as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            }		
            $update_query = rtrim($update_query,",");
            $kBooking->updateDraftBooking($update_query,$idBooking);
        } 
    }
     
    function getImportExportWarehousesForSearch($data,$mode,$szSearchType='LCL')
    {
        $kWHSSearch =new cWHSSearch();
        $wareHouseFromAry = array();
        $wareHouseToAry = array(); 
        
        $idOriginCountry = $data['idOriginCountry'];
        $idDestinationCountry = $data['idDestinationCountry'];
         
        if($szSearchType=='RAIL')
        {
            $data['iRailTransport'] = 1;
        }
        else
        {
            $data['iRailTransport'] = 0;
        }
        
        /*
         * If search type is AIR then we only searches for iWarehouseType = 2 
         * ELSE we only search for warehous iWarehouseType = 1
         
        if($szSearchType=='AIR')
        {
            $data['iWarehouseType'] = __WAREHOUSE_TYPE_AIR__;
        }
        else
        {
            $data['iWarehouseType'] = __WAREHOUSE_TYPE_CFS__;
        }
         * 
         */
        if($mode=='RECALCULATE_PRICING')
        { 
            $wareHouseFromAry = $kWHSSearch->get_warehouse_distance_search($data,'FROM',$data['idWarehouseFrom']);
            $wareHouseToAry = $kWHSSearch->get_warehouse_distance_search($data,'TO',$data['idWarehouseTo']); 
        }
        else if($mode=='MANAGEMENT_TRY_IT_OUT')
        { 		
            $wareHouseFromAry = $kWHSSearch->get_warehouse_distance_search($data,'FROM',false,false);
            $wareHouseToAry = $kWHSSearch->get_warehouse_distance_search($data,'TO',false,false);
        }
        else if($mode=='FORWARDER_TRY_IT_OUT')
        {  			
            $wareHouseFromAry = $kWHSSearch->get_warehouse_distance_search($data,'FROM',false,$data['forwarder_id']);
            $wareHouseToAry = $kWHSSearch->get_warehouse_distance_search($data,'TO',false,$data['forwarder_id']);
        }
        else
        { 
            $wareHouseFromAry = $kWHSSearch->get_warehouse_distance_search($data,'FROM');
            $wareHouseToAry = $kWHSSearch->get_warehouse_distance_search($data,'TO');
        } 
        $szExportImportWhsCalculationLogs = ''; 
        $arrDescriptions = array("'__TOTAL_NUM_WAREHOUSE__'","'__TOTAL_NUM_WAREHOUSE_D_SERVICE__'","'__MAX_WAREHOUSE_DISTANCE_TO_BE_CONSIDER__'","'__MAX_DTD_DISTANCE_TO_BE_CONSIDER__'"); 
        $bulkManagemenrVarAry = array();
        $bulkManagemenrVarAry = $kWHSSearch->getBulkManageMentVariableByDescription($arrDescriptions);      

        $bDTDTradesFlag = false;
        $kCourierServices = new cCourierServices(); 
        if($kCourierServices->checkFromCountryToCountryExists($data['idSearchedOriginCountry'],$data['idSearchedDestinationCountry'],true))
        { 
            $bDTDTradesFlag = true;
        }
        $idServiceType = $data['idServiceType'];
        
        $bExportHaulageFlag = false;
        $bImportHaulageFlag = false;
        
        if($bDTDTradesFlag || $idServiceType==__SERVICE_TYPE_DTD__ || $data['iRailTransport']==1)
        {
            //If selected trade is added on Management /dtdTrades/ screen then we use this distance 
            $maxWarehouseDistance_export = $bulkManagemenrVarAry['__MAX_DTD_DISTANCE_TO_BE_CONSIDER__'];  
            $iWareHouseLimit_export = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE_D_SERVICE__'];
            
            $maxWarehouseDistance_import = $bulkManagemenrVarAry['__MAX_DTD_DISTANCE_TO_BE_CONSIDER__'];  
            $iWareHouseLimit_import = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE_D_SERVICE__'];
            
            $bExportHaulageFlag = true;
            $bImportHaulageFlag = true;
        }
        else
        {
            if($idServiceType==__SERVICE_TYPE_DTP__ || $idServiceType==__SERVICE_TYPE_DTW__)
            {
                $maxWarehouseDistance_export = $bulkManagemenrVarAry['__MAX_DTD_DISTANCE_TO_BE_CONSIDER__'];  
                $iWareHouseLimit_export = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE_D_SERVICE__'];
                
                $maxWarehouseDistance_import = $bulkManagemenrVarAry['__MAX_WAREHOUSE_DISTANCE_TO_BE_CONSIDER__'];  
                $iWareHouseLimit_import = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE__'];
                $bExportHaulageFlag = true;
            }
            else if($idServiceType==__SERVICE_TYPE_PTD__ || $idServiceType==__SERVICE_TYPE_WTD__)
            {
                $maxWarehouseDistance_export = $bulkManagemenrVarAry['__MAX_WAREHOUSE_DISTANCE_TO_BE_CONSIDER__'];  
                $iWareHouseLimit_export = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE__'];
                
                $maxWarehouseDistance_import = $bulkManagemenrVarAry['__MAX_DTD_DISTANCE_TO_BE_CONSIDER__'];  
                $iWareHouseLimit_import = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE_D_SERVICE__'];
                $bImportHaulageFlag = true;
            }
            else
            {
                $maxWarehouseDistance_export = $bulkManagemenrVarAry['__MAX_WAREHOUSE_DISTANCE_TO_BE_CONSIDER__']; 
                $iWareHouseLimit_export = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE__'];
                
                $maxWarehouseDistance_import = $bulkManagemenrVarAry['__MAX_WAREHOUSE_DISTANCE_TO_BE_CONSIDER__']; 
                $iWareHouseLimit_import = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE__'];
            } 
        } 
        
        $exportWarehouseAry = array();
        $importWarehouseAry = array();

        $loopingAry = array();
        $iNumExportwarehouse = count($wareHouseFromAry);
        $iNumImportwarehouse = count($wareHouseToAry);

        if($iNumExportwarehouse>$iNumImportwarehouse)
        {
            $loopingAry = $wareHouseToAry;
        }
        else
        {
            $loopingAry = $wareHouseFromAry;
        } 

        $eligibleForwarderAry  = array();
        if(!empty($loopingAry))
        { 
            unset($loopingAry['idForwarderAry']);
            foreach($loopingAry as $idWarehouse=>$loopingArys)
            { 
                $idForwarder = $loopingArys['idForwarder'];
                $bExportServiceAvailable = false;
                $bImportServiceAvailable = false;

                if(!empty($wareHouseFromAry['idForwarderAry']) && in_array($idForwarder,$wareHouseFromAry['idForwarderAry']))
                {
                    $bExportServiceAvailable = true;
                }
                if(!empty($wareHouseToAry['idForwarderAry']) && in_array($idForwarder,$wareHouseToAry['idForwarderAry']))
                {
                    $bImportServiceAvailable = true;
                } 
                if($bImportServiceAvailable && $bExportServiceAvailable)
                {
                    $eligibleForwarderAry[] = $idForwarder;
                }
            }
        } 
        $forwarderLogsAry = array();
        $forwarderLogsAry = $this->getForwarderNameForrLogs($eligibleForwarderAry); 
        //$szExportImportWhsCalculationLogs .= "<br><i> Forwarders IDs to be considered for search </i><br>".print_r($eligibleForwarderAry,true);
        if(!empty($forwarderLogsAry))
        {
            $szExportImportWhsCalculationLogs .= "<br><strong>Final eligible Forwarder's List</strong><br> ".implode("<br>",$forwarderLogsAry);
        }
        
        /*
        *  Building export warehouses List
        */ 
        $szExportWarehouseIncludedRowsLogs = '';
        $exportWarehouseIdsAry = array();
        if(!empty($wareHouseFromAry))
        {
            $countr = 1;
            unset($wareHouseFromAry['idForwarderAry']);
            foreach($wareHouseFromAry as $idwarehouse=>$wareHouseFromArys)
            {
                $idForwarder = $wareHouseFromArys['idForwarder'];
                if(!empty($eligibleForwarderAry) && in_array($idForwarder,$eligibleForwarderAry))
                { 
                    $exportWarehouseAry[$idwarehouse] = $wareHouseFromArys;
                    $exportWarehouseIdsAry[] = $idwarehouse;
                    $szExportWarehouseIncludedRowsLogs .= "<br><strong>".$countr.". Forwarder: ".$exportWarehouseAry[$idwarehouse]['szDisplayName'].", CFS: ".$exportWarehouseAry[$idwarehouse]['szWareHouseName']." - Included </strong>"; 
                    $countr++;
                }
            }
        }

        $szExportImportWhsCalculationLogs .= "<br><br><u>Fetching final warehouses for Export:</u>";
        if($bExportHaulageFlag)
        {
            $iTotalNumWarehouseFound = count($exportWarehouseIdsAry);
            
            $szExportImportWhsCalculationLogs .= "<br><strong>Step 5. Count(idWarehouse): ".$iTotalNumWarehouseFound.", Y: ".$iWareHouseLimit_export."</strong>"; 

            $szExportImportWhsCalculationLogs .= "<br><br><strong>Final warehouse List</strong>";
            $finalExportWarehouseAry = array();
            if($iTotalNumWarehouseFound>$iWareHouseLimit_export)
            {
                $distanceMeasureAry = array();
                $distanceMeasureAry['fTotalLatitude'] = $data['fOriginLatitude'];
                $distanceMeasureAry['fTotalLongitude'] = $data['fOriginLongitude'];
                $distanceMeasureAry['iDirection'] = 1;
                 
                $eligibleWarehouseAry = array();
                $eligibleWarehouseAry = $kWHSSearch->getEligibleNearestWarehouses($exportWarehouseIdsAry,$distanceMeasureAry,$iWareHouseLimit_export);
                $countr = 1;
                if(!empty($eligibleWarehouseAry))
                {
                    foreach($eligibleWarehouseAry as $idWarehouse=>$eligibleWarehouseArys)
                    {
                        if(!empty($exportWarehouseAry[$idWarehouse]))
                        { 
                            $finalExportWarehouseAry[$idWarehouse] = $exportWarehouseAry[$idWarehouse];
                            $finalExportWarehouseAry[$idWarehouse]['iDistance'] = $eligibleWarehouseArys['distance'];
                            $szExportImportWhsCalculationLogs .= "<br><strong>".$countr.". Forwarder: ".$finalExportWarehouseAry[$idWarehouse]['szDisplayName'].", CFS: ".$finalExportWarehouseAry[$idWarehouse]['szWareHouseName']." - Distance: ".number_format((float)$finalExportWarehouseAry[$idWarehouse]['iDistance'])." - Included </strong>";  
                            $countr++;
                        }
                    }
                }
            }
            else
            {
                $finalExportWarehouseAry = $exportWarehouseAry; 
                $szExportImportWhsCalculationLogs .= $szExportWarehouseIncludedRowsLogs;
            } 
        } 
        else
        {
            $kConfig = new cConfig();
            $kConfig->loadCountry($idOriginCountry); 
            $fFobDistance = $kConfig->fMaxFobDistance; 
            if($fFobDistance>$maxWarehouseDistance_export)
            {
                $maxWarehouseDistance_export = $fFobDistance;
            }

            $szExportImportWhsCalculationLogs .= "<br><strong>Step 5. Calculate distance from Warehouse to Shipper for all warehouses</strong>";  
            $szExportImportWhsCalculationLogs .= "<br> W/P Distance defined: ".$maxWarehouseDistance_export;
            $szExportImportWhsCalculationLogs .= "<br> FOB distance for country: ".$kConfig->szCountryName." is ".$fFobDistance;
  
            $szExportImportWhsCalculationLogs .= '<br><strong>Step 6. Find Max(tblcountry.fMaxFobDistance ; "Maximum theoretical distance for warehouse shipments (km)") i.e Z= '.$maxWarehouseDistance_export."</strong>";
            $szExportImportWhsCalculationLogs .= '<br><strong>Step 7. Remove all WarehouseID from the list, where distance > Z </strong>';
            $szExportImportWhsCalculationLogs .= '<br><strong>Step 8. Remove all idWarehouse from the list, which is not part of the X('.$iWareHouseLimit_export.') warehouses closest to consignee</strong>';
            
            $szExportImportWhsCalculationLogs .= "<br><strong>To optimize speed, we have combined stepstep 5, 6, 7, 8 into a single query</strong>";
            $szExportImportWhsCalculationLogs .= "<br><br><strong>Final warehouse List</strong>";
            
            $distanceMeasureAry = array();
            $distanceMeasureAry['fTotalLatitude'] = $data['fOriginLatitude'];
            $distanceMeasureAry['fTotalLongitude'] = $data['fOriginLongitude'];
            $distanceMeasureAry['iDirection'] = 1; 
            $distanceMeasureAry['maxWarehouseDistance'] = $maxWarehouseDistance_export; 
            $distanceMeasureAry['iWarehouseServiceSearch'] = 1; 

            $eligibleWarehouseAry = array();
            $eligibleWarehouseAry = $kWHSSearch->getEligibleNearestWarehouses($exportWarehouseIdsAry,$distanceMeasureAry,$iWareHouseLimit_export);
             
            $countr = 1;
            if(!empty($eligibleWarehouseAry))
            {
                foreach($eligibleWarehouseAry as $idWarehouse=>$eligibleWarehouseArys)
                {
                    if(!empty($exportWarehouseAry[$idWarehouse]))
                    { 
                        $finalExportWarehouseAry[$idWarehouse] = $exportWarehouseAry[$idWarehouse];
                        $finalExportWarehouseAry[$idWarehouse]['iDistance'] = $eligibleWarehouseArys['distance'];
                        $szExportImportWhsCalculationLogs .= "<br><strong>".$countr.". Forwarder: ".$finalExportWarehouseAry[$idWarehouse]['szDisplayName'].", CFS: ".$finalExportWarehouseAry[$idWarehouse]['szWareHouseName']." - Distance: ".number_format((float)$finalExportWarehouseAry[$idWarehouse]['iDistance'])." - Included </strong>";  
                        $countr++;
                    }
                }
            }   
        }
        
        /*
        *  Building Import warehouses List
        */ 
        $importWarehouseIdsAry = array();
        if(!empty($wareHouseToAry))
        {
            unset($wareHouseToAry['idForwarderAry']);
            $countr = 1;
            foreach($wareHouseToAry as $idwarehouse=>$wareHouseToArys)
            { 
                $idForwarder = $wareHouseToArys['idForwarder'];
                if(!empty($eligibleForwarderAry) && in_array($idForwarder,$eligibleForwarderAry))
                { 
                    $importWarehouseAry[$idwarehouse] = $wareHouseToArys;
                    $importWarehouseIdsAry[] = $idwarehouse;
                    $szImportWarehouseIncludedRowsLogs .= "<br><strong>".$countr.". Forwarder: ".$importWarehouseAry[$idwarehouse]['szDisplayName'].", CFS: ".$importWarehouseAry[$idwarehouse]['szWareHouseName']." - Included </strong>"; 
                    $countr++;
                }
            }
        }  
        $szExportImportWhsCalculationLogs .= "<br><br><u>Fetching final warehouses for Import:</u>";
        if($bImportHaulageFlag)
        {
            $iTotalNumImportWarehouseFound = count($importWarehouseIdsAry); 
            $szExportImportWhsCalculationLogs .= "<br><strong>Step 5. Count(idWarehouse): ".$iTotalNumImportWarehouseFound.", Y: ".$iWareHouseLimit_import."</strong>"; 

            $szExportImportWhsCalculationLogs .= "<br><br><strong>Final warehouse List</strong>";
            $finalImportWarehouseAry = array();
            if($iTotalNumImportWarehouseFound>$iWareHouseLimit_import)
            {
                $distanceMeasureAry = array();
                $distanceMeasureAry['fTotalLatitude'] = $data['fDestinationLatitude'];
                $distanceMeasureAry['fTotalLongitude'] = $data['fDestinationLongitude'];
                $distanceMeasureAry['iDirection'] = 2;  
                
                $eligibleWarehouseAry = array();
                $eligibleWarehouseAry = $kWHSSearch->getEligibleNearestWarehouses($importWarehouseIdsAry,$distanceMeasureAry,$iWareHouseLimit_import);
                if(!empty($eligibleWarehouseAry))
                {
                    $countr = 1;
                    foreach($eligibleWarehouseAry as $idWarehouse=>$eligibleWarehouseArys)
                    {
                        if(!empty($importWarehouseAry[$idWarehouse]))
                        { 
                            $finalImportWarehouseAry[$idWarehouse] = $importWarehouseAry[$idWarehouse];
                            $finalImportWarehouseAry[$idWarehouse]['iDistance'] = $eligibleWarehouseArys['distance'];
                            $szExportImportWhsCalculationLogs .= "<br><strong>".$countr.". Forwarder: ".$finalImportWarehouseAry[$idWarehouse]['szDisplayName'].", CFS: ".$finalImportWarehouseAry[$idWarehouse]['szWareHouseName']." - Distance: ".number_format((float)$finalImportWarehouseAry[$idWarehouse]['iDistance'])." - Included </strong>";  
                            $countr++;
                        }
                    }
                }
            }
            else
            {
                $finalImportWarehouseAry = $importWarehouseAry; 
                $szExportImportWhsCalculationLogs .= $szImportWarehouseIncludedRowsLogs;
            }
        } 
        else
        {
            $kConfig = new cConfig();
            $kConfig->loadCountry($idDestinationCountry); 
            $fFobDistance = $kConfig->fMaxFobDistance; 
            if($fFobDistance>$maxWarehouseDistance_import)
            {
                $maxWarehouseDistance_import = $fFobDistance;
            }
            $szExportImportWhsCalculationLogs .= "<br><strong>Step 5. Calculate distance from Warehouse to Consignee for all warehouses</strong>"; 
            $szExportImportWhsCalculationLogs .= "<br> W/P Distance defined: ".$maxWarehouseDistance_import;
            $szExportImportWhsCalculationLogs .= "<br> FOB distance for country: ".$kConfig->szCountryName." is ".$fFobDistance; 
            $szExportImportWhsCalculationLogs .= '<br><strong>Step 6. Find Max(tblcountry.fMaxFobDistance ; "Maximum theoretical distance for warehouse shipments (km)") i.e Z= '.$maxWarehouseDistance_import."</strong>";
            $szExportImportWhsCalculationLogs .= '<br><strong>Step 7. Remove all WarehouseID from the list, where distance > Z </strong>';
            $szExportImportWhsCalculationLogs .= '<br><strong>Step 8. Remove all idWarehouse from the list, which is not part of the X('.$iWareHouseLimit_import.') warehouses closest to consignee</strong>';
            
            $szExportImportWhsCalculationLogs .= "<br><strong>To optimize speed, we have combined stepstep 5, 6, 7, 8 into a single query</strong>";
            $szExportImportWhsCalculationLogs .= "<br><br><strong>Final warehouse List</strong>";
                    
            $distanceMeasureAry = array();
            $distanceMeasureAry['fTotalLatitude'] = $data['fDestinationLatitude'];
            $distanceMeasureAry['fTotalLongitude'] = $data['fDestinationLongitude'];
            $distanceMeasureAry['iDirection'] = 2;
            $distanceMeasureAry['maxWarehouseDistance'] = $maxWarehouseDistance_import; 
            $distanceMeasureAry['iWarehouseServiceSearch'] = 1; 

            $eligibleWarehouseAry = array();
            $eligibleWarehouseAry = $kWHSSearch->getEligibleNearestWarehouses($importWarehouseIdsAry,$distanceMeasureAry,$iWareHouseLimit_import);
            //$szExportImportWhsCalculationLogs .= $kWHSSearch->szWarehouseQueryLogs;
            
            if(!empty($eligibleWarehouseAry))
            {
                $countr = 1;
                foreach($eligibleWarehouseAry as $idWarehouse=>$eligibleWarehouseArys)
                {
                    if(!empty($importWarehouseAry[$idWarehouse]))
                    { 
                        $finalImportWarehouseAry[$idWarehouse] = $importWarehouseAry[$idWarehouse];
                        $finalImportWarehouseAry[$idWarehouse]['iDistance'] = $eligibleWarehouseArys['distance'];
                        $szExportImportWhsCalculationLogs .= "<br><strong>".$countr.". Forwarder: ".$finalImportWarehouseAry[$idWarehouse]['szDisplayName'].", CFS: ".$finalImportWarehouseAry[$idWarehouse]['szWareHouseName']." - Distance: ".number_format((float)$finalImportWarehouseAry[$idWarehouse]['iDistance'])." - Included </strong>";  
                        $countr++;
                    }
                }
            }
        }
        $this->szExportImportWhsCalculationLogs = $szExportImportWhsCalculationLogs;
        cWHSSearch::$szTestCalculationLogs .= $szExportImportWhsCalculationLogs;
        
        $importExportWarehousesAry = array();
        $importExportWarehousesAry['FROM'] = $finalExportWarehouseAry;
        $importExportWarehousesAry['TO'] = $finalImportWarehouseAry;  
        return $importExportWarehousesAry;
    }
    
    function getForwarderNameForrLogs($forwarderAry)
    {
        if(!empty($forwarderAry))
        {
            $iCount = 1;
            $iWhsCount = count($forwarderAry);
            if($iWhsCount>0)
            {
                $query_or = " AND ( ";
                foreach($forwarderAry as $forwarderArys)
                {
                    if($forwarderArys>0)
                    {
                        $query_or .= " id = '".$forwarderArys."'";
                        if($iWhsCount!=$iCount)
                        { 
                            $query_or .= " OR "; 
                        }
                    } 
                    $iCount++;
                }
                $query_or .= " )";
            }
            
            $query="
                SELECT
                    id,
                    szForwarderAlias,
                    szDisplayName
                FROM
                    ".__DBC_SCHEMATA_FORWARDERS__."
                WHERE
                    iActive = '1'
                    $query_or
                ORDER BY 
                    szDisplayName ASC, szForwarderAlias ASC	
            ";
            //echo $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                // Check if the user exists
                if( $this->getRowCnt() > 0 )
                {
                    $logAry = array();
                    $ctr = 1;
                    while ($row=$this->getAssoc($result))
                    {
                        $logAry[] = $ctr.". ".$row['szDisplayName']." (".$row['szForwarderAlias'].") ";
                        $ctr++;
                    } 
                    return $logAry;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        } 
    }
    
    function findWarehousesForHaulageServices($distanceMeasureAry,$szWarehouseSearchType=false)
    {  
        if(!empty($distanceMeasureAry))
        {
            $kWhsSearch = new cWHSSearch();
            $arrDescriptions = array("'__TOTAL_NUM_WAREHOUSE__'","'__TOTAL_NUM_WAREHOUSE_D_SERVICE__'","'__MAX_WAREHOUSE_DISTANCE_TO_BE_CONSIDER__'","'__MAX_DTD_DISTANCE_TO_BE_CONSIDER__'"); 
            $bulkManagemenrVarAry = array();
            $bulkManagemenrVarAry = $kWhsSearch->getBulkManageMentVariableByDescription($arrDescriptions);      

            $bDTDTradesFlag = false;
            $kCourierServices = new cCourierServices(); 
            if($kCourierServices->checkFromCountryToCountryExists($distanceMeasureAry['idSearchedOriginCountry'],$distanceMeasureAry['idSearchedDestinationCountry'],true))
            { 
                $bDTDTradesFlag = true;
            }
            $idServiceType = $distanceMeasureAry['idServiceType'];

            if($distanceMeasureAry['iRailTransport']==1)
            {
                $maxWarehouseDistance = $bulkManagemenrVarAry['__MAX_DTD_DISTANCE_TO_BE_CONSIDER__'];  
                $iWareHouseLimit = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE_D_SERVICE__'];
            }
            else if($szWarehouseSearchType=='FROM')
            {
                if($idServiceType==__SERVICE_TYPE_DTP__ || $idServiceType==__SERVICE_TYPE_DTW__ || $idServiceType==__SERVICE_TYPE_DTD__)
                {
                    $maxWarehouseDistance = $bulkManagemenrVarAry['__MAX_DTD_DISTANCE_TO_BE_CONSIDER__'];  
                    $iWareHouseLimit = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE_D_SERVICE__'];
                }
                else
                {
                    $maxWarehouseDistance = $bulkManagemenrVarAry['__MAX_WAREHOUSE_DISTANCE_TO_BE_CONSIDER__']; 
                    $iWareHouseLimit = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE__'];
                }
            }
            else if($szWarehouseSearchType=='TO')
            {
                if($idServiceType==__SERVICE_TYPE_PTD__ || $idServiceType==__SERVICE_TYPE_WTD__ || $idServiceType==__SERVICE_TYPE_DTD__)
                {
                    $maxWarehouseDistance = $bulkManagemenrVarAry['__MAX_DTD_DISTANCE_TO_BE_CONSIDER__'];  
                    $iWareHouseLimit = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE_D_SERVICE__'];
                }
                else
                {
                    $maxWarehouseDistance = $bulkManagemenrVarAry['__MAX_WAREHOUSE_DISTANCE_TO_BE_CONSIDER__']; 
                    $iWareHouseLimit = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE__'];
                }
            }
            else if($bDTDTradesFlag)
            {
                //If selected trade is added on Management /dtdTrades/ screen then we use this distance 
                $maxWarehouseDistance = $bulkManagemenrVarAry['__MAX_DTD_DISTANCE_TO_BE_CONSIDER__'];  
                $iWareHouseLimit = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE_D_SERVICE__'];
            }
            else
            {
                $maxWarehouseDistance = $bulkManagemenrVarAry['__MAX_WAREHOUSE_DISTANCE_TO_BE_CONSIDER__']; 
                $iWareHouseLimit = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE__'];
            }

            $query_and = '';
            if((int)$distanceMeasureAry['idWareHouse'])
            {
                /*
                * This code will called when we recalculate the LCL prices
                */
                $query_and = " AND whs.id = '".(int)$distanceMeasureAry['idWareHouse']."'" ;
            }
            if((int)$distanceMeasureAry['idForwarder']>0)
            {
                /*
                * This code will be from Forwarder Try-it-out
                */
                $query_and .= " AND fbd.iAgreeTNC IN (0,1)" ;
            }
            else
            {
               // $query_and .= " AND fbd.isOnline = '1' AND fbd.iAgreeTNC = '1' " ;
            }
            $iDirection = $distanceMeasureAry['iDirection'];
 
            if($_SESSION['user_id']>0)
            {
                $kUser = new cUser();
                $kForwarder = new cForwarder();
                $kUser->getUserDetails($_SESSION['user_id']);
                $idForwarderAry = $kForwarder->getAllNonAcceptedForwarderByEmail($kUser->szEmail);
                if(!empty($idForwarderAry))
                {
                    $query_and .=" AND whs.idForwarder NOT IN (".implode(',',$idForwarderAry).")";
                }
            }
            if($iDirection==1)
            {
                $szImportExport = "Export";
            }
            else
            {
                $szImportExport = "Import";
            }

            $this->szTestLCLCalculationLogs .= "<br><br><u> Fetching warehouses for ".$szImportExport.": </u>";

            if((int)$distanceMeasureAry['idCountry'])
            {
                //$query_and = " AND whs.idCountry = '".(int)$distanceMeasureAry['idCountry']."'" ;
            }

            /*
            GetDistance('".$distanceMeasureAry['fTotalLatitude'].",".$distanceMeasureAry['fTotalLongitude']."',CONCAT_WS(',',whs.szLatitude,whs.szLongitude))
            */ 
            $globalCountryServicedAry = array();
            $idServicedCountry = $distanceMeasureAry['idCountry'];   
            $idServiceType = $distanceMeasureAry['idServiceType'];
            $iDirection = $distanceMeasureAry['iDirection'];
            $bHaulageRequired = false;

            if($iDirection==1 && ($idServiceType==__SERVICE_TYPE_DTP__ || $idServiceType==__SERVICE_TYPE_DTW__ || $idServiceType==__SERVICE_TYPE_DTD__ || $distanceMeasureAry['iRailTransport']==1))
            {	
                $bHaulageRequired = true;
                $query_select = ", (SELECT count(hd.idCountry) FROM ".__DBC_SCHEMATA_HAULAGE_COUNTRY__." hd WHERE hd.idWarehouse = whs.id AND hd.idCountry = '".$idServicedCountry."' AND hd.idDirection = '1' AND hd.iActive='1') as iHaulageAvailabale ";
                $szHaulageStep = " In tblhaulagecountry, fetch all idWarehouse for rows where iActive =1 AND idDirection = 1 AND idCountry = ".$idServicedCountry;
            }
            else if($iDirection==2 && ($idServiceType==__SERVICE_TYPE_PTD__ || $idServiceType==__SERVICE_TYPE_WTD__ || $idServiceType==__SERVICE_TYPE_DTD__ || $distanceMeasureAry['iRailTransport']==1))
            {	 
                $query_select = ", (SELECT count(hd.idCountry) FROM ".__DBC_SCHEMATA_HAULAGE_COUNTRY__." hd WHERE hd.idWarehouse = whs.id AND hd.idCountry = '".$idServicedCountry."' AND hd.idDirection = '2' AND hd.iActive='1') as iHaulageAvailabale";
                $bHaulageRequired = true;
                $szHaulageStep = " In tblhaulagecountry, fetch all idWarehouse for rows where iActive =1 AND idDirection = 2 AND idCountry = ".$idServicedCountry;
            }
            $query_whs_select = ", (SELECT count(wc.id) FROM ".__DBC_SCHEMATA_WAREHOUSE_COUNTRIES__." wc WHERE wc.warehouseID = whs.id AND wc.countryID = '".(int)$idServicedCountry."') as iWTWServiceAvailabale ";

            /*
            * If User has searched for D service then at least 1 record for the country shoud be in tblhaulagecountry
            * Else If user has searched for P or W service then a record for the country should be in tblwarehousecountries
            */
            if($bHaulageRequired)
            {
                $query_having = " iHaulageAvailabale > 0 ";
            }
            else
            {
                $kConfig = new cConfig();
                $kConfig->loadCountry($idServicedCountry); 
                $fFobDistance = $kConfig->fMaxFobDistance; 

                $this->szTestLCLCalculationLogs .= "<br> W/P Distance defined: ".$maxWarehouseDistance;
                $this->szTestLCLCalculationLogs .= "<br> FOB distance for country: ".$idServicedCountry." is ".$fFobDistance;

                if($fFobDistance>$maxWarehouseDistance)
                {
                    $maxWarehouseDistance = $fFobDistance;
                } 
                $query_having = " iWTWServiceAvailabale > 0 ";
                
                $szHaulageStep = " In tblwarehousecountries, fetch all idWarehouse for rows where countryID = ".$idServicedCountry;
            }

            $szWarehouseSearchMode = "LCL";
            if($distanceMeasureAry['iRailTransport']==1)
            {
                /*
                * i. For all idFromWarehouse in tblrailtransport, where iActive = 1 AND isDeleted = 0, check in tblhaulagecountry, and take only those which has idDirection = 1 AND idCountry = “Shipper country”
                * ii. For those rows in tblrailtransport, check idToWarehouse  in tblhaulagecountry, and take only those  which have idDirection = 2 AND idCountry = “Consignee country” 
                */
                if($iDirection==1)
                {
                    $query_rail_and = " AND rt.idFromWarehouse = whs.id ";
                }
                else
                {
                    $query_rail_and = " AND rt.idToWarehouse = whs.id ";
                }
                $query_select .= ", (SELECT count(rt.id) as iRailWhsCount FROM ".__DBC_SCHEMATA_RAIL_TRANSPORT__." rt WHERE rt.isDeleted = 0 AND iDTD='1' ".$query_rail_and.") as iRailWhsCounter ";
                $query_having .= " AND iRailWhsCounter >0 ";
                $szWarehouseSearchMode = "Rail";
            } 
            else if($idServiceType !=__SERVICE_TYPE_DTD__)
            {
                if($iDirection==1)
                {
                    $query_rail_and = " AND rt.idFromWarehouse = whs.id ";
                }
                else
                {
                    $query_rail_and = " AND rt.idToWarehouse = whs.id ";
                }
                $query_select .= ", (SELECT count(rt.id) as iRailWhsCount FROM ".__DBC_SCHEMATA_RAIL_TRANSPORT__." rt WHERE rt.isDeleted = 0 AND iDTD='1' ".$query_rail_and.") as iRailWhsCounter ";
                $query_having .= " AND iRailWhsCounter = 0 ";
            }

            $query_excluded_forwarder_select='';
            if($_SESSION['user_id']>0)
            {
                $kUser = new cUser();
                $kForwarder = new cForwarder();
                $kUser->getUserDetails($_SESSION['user_id']);
                $excludedTradeSearch['idOriginCountry'] = $distanceMeasureAry['idSearchedOriginCountry'];
                $excludedTradeSearch['idDestinationCountry'] = $distanceMeasureAry['idSearchedDestinationCountry'];
                $iPrivateShipping = $kUser->iPrivate;
                if($kUser->iPrivate==1)
                {
                    $excludedTradeSearch['iCustomerType'] = __COURIER_EXCLUDED_TRADE_CUSTOMER_TYPE_PRIVATE__;
                }
                else
                {
                    $excludedTradeSearch['iCustomerType'] = __COURIER_EXCLUDED_TRADE_CUSTOMER_TYPE_BUSSINESS__;
                } 
                $kCourierServices = new cCourierServices(); 
                $forwarderExcludedArr = $kCourierServices->isTradeAddedToExcludedLCLList($excludedTradeSearch,'',true); 
                if(!empty($forwarderExcludedArr))
                {
                    $forwarderExcludedStr=implode("','",$forwarderExcludedArr);
                    $query_excluded_forwarder_select =" AND fbd.id NOT IN ('".$forwarderExcludedStr."') ";
                    $forwarderExcludedFullArr=$kCourierServices->isTradeAddedToExcludedLCLList($excludedTradeSearch,'',true,true); 
                    if(!empty($forwarderExcludedFullArr))
                    {
                        $forwarderExcludedFullStr=implode(", ",$forwarderExcludedFullArr);
                        $this->szTestLCLCalculationLogs .= " <br> Forwarder: ".$forwarderExcludedFullStr." <br> Trade: NOT OK  ";
                        $this->szTestLCLCalculationLogs .= " <br> Description: This trade is added in excluded trade service list. ";
                        //$this->szTestLCLCalculationLogs .= "<br> CFS: ".$row['szWareHouseName']." - Distance: ".ceil($row['distance'])." - Not Included"; 
                    }
                }   
            } 
            else
            {
                $iPrivateShipping = $distanceMeasureAry['iPrivateShipping'];
            }

            if($iPrivateShipping==1)
            {
                /*
                * If search is being performed for private customer then we only picks the forwarder who are actually proving services for private customer as on forwarder section /privateCustomers/
                */
                $kForwarder = new cForwarder();
                if($bDTDTradesFlag)
                {
                    $szProduct = 'LTL';
                }
                else
                {
                    $szProduct = 'LCL';
                } 
                $forwarderListAry = $kForwarder->listAllForwarderByProduct($szProduct); 
                if(!empty($forwarderListAry))
                {
                    $query_and .=" AND (whs.idForwarder IN (".implode(',',$forwarderListAry).") AND whs.iWarehouseType = '".__WAREHOUSE_TYPE_CFS__."') ";
                }

                /*
                * Fetching list of forwarders, who is offering AIR services.
                */ 
                $airFreightForwarderListAry = array();
                $airFreightForwarderListAry = $kForwarder->listAllForwarderByProduct("AIR");
                if(!empty($airFreightForwarderListAry))
                {
                    $query_and .=" AND (whs.idForwarder IN (".implode(',',$airFreightForwarderListAry).") AND whs.iWarehouseType = '".__WAREHOUSE_TYPE_AIR__."') ";
                }
            }
            if($distanceMeasureAry['iQuickQuote']!=1)
            {
                //$szSpecialQuery = " AND distance < ".(int)$maxWarehouseDistance." ";
            }
            
            if(!empty($query_having))
            {
                $query_having = " HAVING ".$query_having;
            }

            $query="
                SELECT
                    sup.*
                FROM 
                    (
                        SELECT 
                           whs.id, 
                           whs.id as idWarehouse,
                           whs.szWareHouseName,
                           whs.idForwarder,
                           whs.szUTCOffset ,
                           whs.idCountry,
                           whs.szCity,
                           whs.szPostCode,
                           fbd.szLogo,
                           fbd.szDisplayName,
                           fbd.szForwarderAlias,
                           fbd.szCurrency,
                           cont.fStandardTruckRate,
                           cont.szCountryName,		
                           cont.szCountryISO,
                           cont.fMaxFobDistance,
                           whs.szLatitude,
                           whs.szLongitude,
                           whs.szContactPerson,
                           whs.szEmail, 
                           fbd.dtReferealFeeValid, 
                           whs.iActive,
                           fbd.iActive as iForwarderActive,
                           fbd.iAgreeTNC,
                           fbd.isOnline,
                           fbd.idCountry as idForwarderCountry 
                           $query_select
                           $query_whs_select
                        FROM 
                            ".__DBC_SCHEMATA_WAREHOUSES__." whs
                        INNER JOIN
                            ".__DBC_SCHEMATA_COUNTRY__." cont
                        ON
                            cont.id = whs.idCountry				
                        INNER JOIN
                            ".__DBC_SCHEMATA_FORWARDERS__."	fbd
                        ON
                            fbd.id = whs.idForwarder 
                        WHERE
                            whs.isDeleted = '0' 
                            ".$query_excluded_forwarder_select."
                            ".$query_and."	 
                            $query_having  
                    ) as sup  
            ";	
//            echo "Query: ". $query;
//            die; 
            if($bHaulageRequired)
            {
                $this->szTestLCLCalculationLogs .= "<br>WHS LIMIT: ".$iWareHouseLimit." <strong> Mode: ".$szWarehouseSearchMode."</strong>";
            }
            else
            {
                $this->szTestLCLCalculationLogs .= "<br> Distance Range: ".$maxWarehouseDistance." WHS LIMIT: ".$iWareHouseLimit." <strong> Mode: ".$szWarehouseSearchMode."</strong>";
            }
            //$this->szTestLCLCalculationLogs .= "<br>".$query."<br>";

            $szWarehouseLogStep1 = '';
            $szWarehouseLogStep2 = '';
            $szWarehouseLogStep3 = '';
            
            if($result = $this->exeSQL($query))
            {
                $wareHouseAry = array();
                $warehouseIdsAry = array();
                $ctr = 0;
                $whs_counter = 0;
                $whs_counter2 = 0;
                $whs_counter3 = 0;
                $whs_counter4 = 0;
                $whs_counter5 = 0;
                while($row=$this->getAssoc($result))
                {   
                    if($bHaulageRequired)
                    {
                        if($row['iHaulageAvailabale']>0)
                        {
                            $bEligibleWarehouse = true;
                        }
                        else
                        {
                            $bEligibleWarehouse = false;
                            //$this->szTestLCLCalculationLogs .= "<br>Forwarder: ".$row['szDisplayName'].", CFS: ".$row['szWareHouseName']." - Distance: ".ceil($row['distance'])." - Not Included"; 
                            //$this->szTestLCLCalculationLogs .= "<br> Description: No haulage defined ";
                        }
                    }
                    else 
                    {
                        if($row['iWTWServiceAvailabale']>0)
                        {
                            $bEligibleWarehouse = true;
                        }
                        else
                        {
                            $bEligibleWarehouse = false;
                        }
                    }
                    
                    if($bEligibleWarehouse)
                    { 
                        $whs_counter++;
                        $szWarehouseLogStep1 .= $whs_counter.". ".$row['szDisplayName']." - ".$row['szWareHouseName']." <br>";
                        if($row['iActive']==1)
                        {
                            $whs_counter2++;
                            $szWarehouseLogStep2 .= $whs_counter2.". ".$row['szDisplayName']." - ".$row['szWareHouseName']."<br>";
                            if($row['iForwarderActive']==1)
                            {
                                $whs_counter3++;
                                $szWarehouseLogStep3 .= $whs_counter3.". ".$row['szDisplayName']." (".$row['szForwarderAlias'].") - ".$row['szWareHouseName']."<br>";
                                if($row['iAgreeTNC']==1 && $row['isOnline']==1)
                                {
                                    $whs_counter4++;
                                    $szWarehouseLogStep4 .= $whs_counter4.". ".$row['szDisplayName']." (".$row['szForwarderAlias'].") - ".$row['szWareHouseName']."<br>";
                                    $warehouseIdsAry[] = $row['id'];
                                    //if($iWareHouseLimit>$whs_counter5)
                                    //{
                                        $whs_counter5++;
                                        $szWarehouseLogStep5 .= "<br><strong>".$whs_counter5.". Forwarder: ".$row['szDisplayName'].", CFS: ".$row['szWareHouseName']." - Included </strong>"; 
                                        // echo "<br> WHS ID:  ".$row['id']." Direc ".$iDirection ." Haul: ".$row['iHaulageAvailabale'] ;
                                        if($distanceMeasureAry['iRailTransport']==1)
                                        {
                                            $wareHouseAry[$row['id']]['iRailTransport'] = 1;
                                        }
                                        $wareHouseAry[$row['id']]['iDistance'] = ceil($row['distance']);
                                        $wareHouseAry[$row['id']]['szWareHouseName'] = $row['szWareHouseName'];
                                        $wareHouseAry[$row['id']]['idForwarder'] = $row['idForwarder'];
                                        $wareHouseAry[$row['id']]['fStandardTruckRate'] = $row['fStandardTruckRate'];	
                                        $wareHouseAry[$row['id']]['szLogo'] = $row['szLogo'];	
                                        $wareHouseAry[$row['id']]['szCountry'] = $row['szCountryName'];		
                                        $wareHouseAry[$row['id']]['szCity'] = $row['szCity'];	
                                        $wareHouseAry[$row['id']]['idForwarder'] = $row['idForwarder'];	
                                        $wareHouseAry[$row['id']]['szDisplayName'] = $row['szDisplayName'];
                                        $wareHouseAry[$row['id']]['szForwarderAlias'] = $row['szForwarderAlias']; 
                                        $wareHouseAry[$row['id']]['szUTCOffset'] = $row['szUTCOffset'];	
                                        $wareHouseAry[$row['id']]['idCountry'] = $row['idCountry'];	
                                        $wareHouseAry[$row['id']]['szPostCode'] = $row['szPostCode'];	
                                        $wareHouseAry[$row['id']]['szCountryISO'] = $row['szCountryISO'];	
                                        $wareHouseAry[$row['id']]['szEmail'] = $row['szEmail'];	
                                        $wareHouseAry[$row['id']]['szContactPerson'] = $row['szContactPerson']; 
                                        $wareHouseAry[$row['id']]['id'] = $row['id'];
                                        $wareHouseAry[$row['id']]['szLatitude'] = $row['szLatitude'];	
                                        $wareHouseAry[$row['id']]['szLongitude'] = $row['szLongitude'];
                                        $wareHouseAry[$row['id']]['szForwarderCurrency'] = $row['szCurrency'];	
                                        $wareHouseAry[$row['id']]['fReferalFee'] = $row['fReferalFee'];
                                        $wareHouseAry[$row['id']]['dtReferealFeeValid'] = $row['dtReferealFeeValid'];
                                        $wareHouseAry[$row['id']]['idForwarderCountry'] = $row['idForwarderCountry']; 

                                        $forwarderCurrencyAry = array();
                                        if($row['szCurrency']==1)
                                        {
                                            $wareHouseAry[$row['id']]['szForwarderCurrencyName'] = 'USD';
                                            $wareHouseAry[$row['id']]['fForwarderCurrencyExchangeRate'] = 1.00;
                                        }
                                        else
                                        {
                                            if(!empty($alreadyUsedCurrency[$row['szCurrency']]))
                                            {
                                                $forwarderCurrencyAry = $alreadyUsedCurrency[$row['szCurrency']];
                                            }
                                            else
                                            {
                                                $forwarderCurrencyAry = $kWhsSearch->getCurrencyDetails($row['szCurrency']);		
                                                $alreadyUsedCurrency[$row['szCurrency']] = $forwarderCurrencyAry ;
                                            } 
                                            if(!empty($forwarderCurrencyAry))
                                            {					
                                                $wareHouseAry[$row['id']]['szForwarderCurrencyName'] = $forwarderCurrencyAry['szCurrency'];
                                                $wareHouseAry[$row['id']]['fForwarderCurrencyExchangeRate'] = $forwarderCurrencyAry['fUsdValue'];
                                            }
                                        } 
                                    //} 
                                } 
                            } 
                        }
                        else
                        {
                           //$szWarehouseLogStep2 .= $row['szDisplayName']." - ".$row['szWareHouseName']."<br>";
                        }  
                    }	
                    $ctr++; 
                }  
                $szFinalResponseLogs = "<br><strong>Step 1. ".$szHaulageStep."</strong><br>".$szWarehouseLogStep1;
                $szFinalResponseLogs .= "<strong>Step 2. Remove all idWarehouse from the list, where tblwarehouses.iActive = 0 </strong><br>".$szWarehouseLogStep2;
                $szFinalResponseLogs .= "<strong>Step 3. Remove all idWarehouse from the list, where idForwarder.iActive = 0 </strong><br>".$szWarehouseLogStep3;
                $szFinalResponseLogs .= "<strong>Step 4. Remove all idWarehouse from the list, where idForwarder.iAgreeTNC = 0 </strong><br>".$szWarehouseLogStep4; 
                $this->szTestLCLCalculationLogs .= $szFinalResponseLogs; 
                return $ret_ary;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
        } 
    }
    
    
	function getDistanceFromAllWareHouses_backup($distanceMeasureAry,$szWarehouseSearchType=false)
	{ 
            if(!empty($distanceMeasureAry))
            {
                $arrDescriptions = array("'__TOTAL_NUM_WAREHOUSE__'","'__TOTAL_NUM_WAREHOUSE_D_SERVICE__'","'__MAX_WAREHOUSE_DISTANCE_TO_BE_CONSIDER__'","'__MAX_DTD_DISTANCE_TO_BE_CONSIDER__'"); 
                $bulkManagemenrVarAry = array();
                $bulkManagemenrVarAry = $this->getBulkManageMentVariableByDescription($arrDescriptions);      
                        
                $bDTDTradesFlag = false;
                $kCourierServices = new cCourierServices(); 
                if($kCourierServices->checkFromCountryToCountryExists($distanceMeasureAry['idSearchedOriginCountry'],$distanceMeasureAry['idSearchedDestinationCountry'],true))
                { 
                    $bDTDTradesFlag = true;
                }
                $idServiceType = $distanceMeasureAry['idServiceType'];
                
                if($distanceMeasureAry['iRailTransport']==1)
                {
                    $maxWarehouseDistance = $bulkManagemenrVarAry['__MAX_DTD_DISTANCE_TO_BE_CONSIDER__'];  
                    $iWareHouseLimit = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE_D_SERVICE__'];
                }
                else if($szWarehouseSearchType=='FROM')
                {
                    if($idServiceType==__SERVICE_TYPE_DTP__ || $idServiceType==__SERVICE_TYPE_DTW__ || $idServiceType==__SERVICE_TYPE_DTD__)
                    {
                        $maxWarehouseDistance = $bulkManagemenrVarAry['__MAX_DTD_DISTANCE_TO_BE_CONSIDER__'];  
                        $iWareHouseLimit = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE_D_SERVICE__'];
                    }
                    else
                    {
                        $maxWarehouseDistance = $bulkManagemenrVarAry['__MAX_WAREHOUSE_DISTANCE_TO_BE_CONSIDER__']; 
                        $iWareHouseLimit = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE__'];
                    }
                }
                else if($szWarehouseSearchType=='TO')
                {
                    if($idServiceType==__SERVICE_TYPE_PTD__ || $idServiceType==__SERVICE_TYPE_WTD__ || $idServiceType==__SERVICE_TYPE_DTD__)
                    {
                        $maxWarehouseDistance = $bulkManagemenrVarAry['__MAX_DTD_DISTANCE_TO_BE_CONSIDER__'];  
                        $iWareHouseLimit = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE_D_SERVICE__'];
                    }
                    else
                    {
                        $maxWarehouseDistance = $bulkManagemenrVarAry['__MAX_WAREHOUSE_DISTANCE_TO_BE_CONSIDER__']; 
                        $iWareHouseLimit = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE__'];
                    }
                }
                else if($bDTDTradesFlag)
                {
                    //If selected trade is added on Management /dtdTrades/ screen then we use this distance 
                    $maxWarehouseDistance = $bulkManagemenrVarAry['__MAX_DTD_DISTANCE_TO_BE_CONSIDER__'];  
                    $iWareHouseLimit = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE_D_SERVICE__'];
                }
                else
                {
                    $maxWarehouseDistance = $bulkManagemenrVarAry['__MAX_WAREHOUSE_DISTANCE_TO_BE_CONSIDER__']; 
                    $iWareHouseLimit = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE__'];
                }
                
                $query_and = '';
		if((int)$distanceMeasureAry['idWareHouse'])
		{
                    /*
                    * This code will called when we recalculate the LCL prices
                    */
                    $query_and = " AND whs.id = '".(int)$distanceMeasureAry['idWareHouse']."'" ;
		}
		if((int)$distanceMeasureAry['idForwarder']>0)
		{
                    /*
                    * This code will be from Forwarder Try-it-out
                    */
                    $query_and .= " AND fbd.iAgreeTNC IN (0,1)" ;
		}
		else
		{
                    $query_and .= " AND fbd.isOnline = '1' AND fbd.iAgreeTNC = '1' " ;
		}
		$iDirection = $distanceMeasureAry['iDirection'];
                
                if($iDirection==1) //Origin CFS
                {
                    //$query_and .= " AND fbd.isOnline = '1' " ;
                }
                
		if($_SESSION['user_id']>0)
		{
                    $kUser = new cUser();
                    $kForwarder = new cForwarder();
                    $kUser->getUserDetails($_SESSION['user_id']);
                    $idForwarderAry = $kForwarder->getAllNonAcceptedForwarderByEmail($kUser->szEmail);
                    if(!empty($idForwarderAry))
                    {
                        $query_and .=" AND whs.idForwarder NOT IN (".implode(',',$idForwarderAry).")";
                    }
		}
                if($iDirection==1)
                {
                    $szImportExport = "Export";
                }
                else
                {
                    $szImportExport = "Import";
                }
                
                $this->szLCLCalculationLogs .= "<br><br><u> Fetching warehouses for ".$szImportExport.": </u>";
		
		if((int)$distanceMeasureAry['idCountry'])
		{
                    //$query_and = " AND whs.idCountry = '".(int)$distanceMeasureAry['idCountry']."'" ;
		}
                
		/*
		GetDistance('".$distanceMeasureAry['fTotalLatitude'].",".$distanceMeasureAry['fTotalLongitude']."',CONCAT_WS(',',whs.szLatitude,whs.szLongitude))
		*/ 
                $globalCountryServicedAry = array();
		$idServicedCountry = $distanceMeasureAry['idCountry'];   
                $idServiceType = $distanceMeasureAry['idServiceType'];
                $iDirection = $distanceMeasureAry['iDirection'];
                $bHaulageRequired = false;
                        
                if($iDirection==1 && ($idServiceType==__SERVICE_TYPE_DTP__ || $idServiceType==__SERVICE_TYPE_DTW__ || $idServiceType==__SERVICE_TYPE_DTD__ || $distanceMeasureAry['iRailTransport']==1))
                {	
                    $bHaulageRequired = true;
                    $query_select = ", (SELECT count(hd.idCountry) FROM ".__DBC_SCHEMATA_HAULAGE_COUNTRY__." hd WHERE hd.idWarehouse = whs.id AND hd.idCountry = '".$idServicedCountry."' AND hd.idDirection = '1' AND hd.iActive='1') as iHaulageAvailabale ";
                }
                else if($iDirection==2 && ($idServiceType==__SERVICE_TYPE_PTD__ || $idServiceType==__SERVICE_TYPE_WTD__ || $idServiceType==__SERVICE_TYPE_DTD__ || $distanceMeasureAry['iRailTransport']==1))
                {	 
                    $query_select = ", (SELECT count(hd.idCountry) FROM ".__DBC_SCHEMATA_HAULAGE_COUNTRY__." hd WHERE hd.idWarehouse = whs.id AND hd.idCountry = '".$idServicedCountry."' AND hd.idDirection = '2' AND hd.iActive='1') as iHaulageAvailabale";
                    $bHaulageRequired = true;
                }
                $query_whs_select = ", (SELECT count(wc.id) FROM ".__DBC_SCHEMATA_WAREHOUSE_COUNTRIES__." wc WHERE wc.warehouseID = whs.id AND wc.countryID = '".(int)$idServicedCountry."') as iWTWServiceAvailabale ";
                        
                /*
                * If User has searched for D service then at least 1 record for the country shoud be in tblhaulagecountry
                * Else If user has searched for P or W service then a record for the country should be in tblwarehousecountries
                */
                if($bHaulageRequired)
                {
                    $query_having = " iHaulageAvailabale > 0 ";
                }
                else
                {
                    $kConfig = new cConfig();
                    $kConfig->loadCountry($idServicedCountry); 
                    $fFobDistance = $kConfig->fMaxFobDistance; 
                    
                    $this->szLCLCalculationLogs .= "<br> W/P Distance defined: ".$maxWarehouseDistance;
                    $this->szLCLCalculationLogs .= "<br> FOB distance for country: ".$idServicedCountry." is ".$fFobDistance;
                    
                    if($fFobDistance>$maxWarehouseDistance)
                    {
                        $maxWarehouseDistance = $fFobDistance;
                    } 
                    $query_having = " distance < ".(int)$maxWarehouseDistance." AND iWTWServiceAvailabale > 0 ";
                }
                
                $szWarehouseSearchMode = "LCL";
                if($distanceMeasureAry['iRailTransport']==1)
                {
                    /*
                    * i. For all idFromWarehouse in tblrailtransport, where iActive = 1 AND isDeleted = 0, check in tblhaulagecountry, and take only those which has idDirection = 1 AND idCountry = “Shipper country”
                    * ii. For those rows in tblrailtransport, check idToWarehouse  in tblhaulagecountry, and take only those  which have idDirection = 2 AND idCountry = “Consignee country” 
                    */
                    if($iDirection==1)
                    {
                        $query_rail_and = " AND rt.idFromWarehouse = whs.id ";
                    }
                    else
                    {
                        $query_rail_and = " AND rt.idToWarehouse = whs.id ";
                    }
                    $query_select .= ", (SELECT count(rt.id) as iRailWhsCount FROM ".__DBC_SCHEMATA_RAIL_TRANSPORT__." rt WHERE rt.isDeleted = 0 AND iDTD='1' ".$query_rail_and.") as iRailWhsCounter ";
                    $query_having .= " AND iRailWhsCounter >0 ";
                    $szWarehouseSearchMode = "Rail";
                } 
                else if($idServiceType !=__SERVICE_TYPE_DTD__)
                {
                    if($iDirection==1)
                    {
                        $query_rail_and = " AND rt.idFromWarehouse = whs.id ";
                    }
                    else
                    {
                        $query_rail_and = " AND rt.idToWarehouse = whs.id ";
                    }
                    $query_select .= ", (SELECT count(rt.id) as iRailWhsCount FROM ".__DBC_SCHEMATA_RAIL_TRANSPORT__." rt WHERE rt.isDeleted = 0 AND iDTD='1' ".$query_rail_and.") as iRailWhsCounter ";
                    $query_having .= " AND iRailWhsCounter = 0 ";
                }
                
                $query_excluded_forwarder_select='';
                if($_SESSION['user_id']>0)
                {
                    $kUser = new cUser();
                    $kForwarder = new cForwarder();
                    $kUser->getUserDetails($_SESSION['user_id']);
                    $excludedTradeSearch['idOriginCountry'] = $distanceMeasureAry['idSearchedOriginCountry'];
                    $excludedTradeSearch['idDestinationCountry'] = $distanceMeasureAry['idSearchedDestinationCountry'];
                    $iPrivateShipping = $kUser->iPrivate;
                    if($kUser->iPrivate==1)
                    {
                        $excludedTradeSearch['iCustomerType'] = __COURIER_EXCLUDED_TRADE_CUSTOMER_TYPE_PRIVATE__;
                    }
                    else
                    {
                        $excludedTradeSearch['iCustomerType'] = __COURIER_EXCLUDED_TRADE_CUSTOMER_TYPE_BUSSINESS__;
                    } 
                    $kCourierServices = new cCourierServices(); 
                    $forwarderExcludedArr = $kCourierServices->isTradeAddedToExcludedLCLList($excludedTradeSearch,'',true); 
                    if(!empty($forwarderExcludedArr))
                    {
                        $forwarderExcludedStr=implode("','",$forwarderExcludedArr);
                        $query_excluded_forwarder_select ="AND fbd.id NOT IN ('".$forwarderExcludedStr."')";
                        $forwarderExcludedFullArr=$kCourierServices->isTradeAddedToExcludedLCLList($excludedTradeSearch,'',true,true); 
                        if(!empty($forwarderExcludedFullArr))
                        {
                            $forwarderExcludedFullStr=implode(", ",$forwarderExcludedFullArr);
                            $this->szLCLCalculationLogs .= " <br> Forwarder: ".$forwarderExcludedFullStr." <br> Trade: NOT OK  ";
                            $this->szLCLCalculationLogs .= " <br> Description: This trade is added in excluded trade service list. ";
                            //$this->szLCLCalculationLogs .= "<br> CFS: ".$row['szWareHouseName']." - Distance: ".ceil($row['distance'])." - Not Included"; 
                        }
                    }   
                } 
                else
                {
                    $iPrivateShipping = $distanceMeasureAry['iPrivateShipping'];
                }
                        
                if($iPrivateShipping==1)
                {
                    /*
                    * If search is being performed for private customer then we only picks the forwarder who are actually proving services for private customer as on forwarder section /privateCustomers/
                    */
                    $kForwarder = new cForwarder();
                    if($bDTDTradesFlag)
                    {
                        $szProduct = 'LTL';
                    }
                    else
                    {
                        $szProduct = 'LCL';
                    } 
                    $forwarderListAry = $kForwarder->listAllForwarderByProduct($szProduct); 
                    if(!empty($forwarderListAry))
                    {
                        $query_and .=" AND (whs.idForwarder IN (".implode(',',$forwarderListAry).") AND whs.iWarehouseType = '".__WAREHOUSE_TYPE_CFS__."') ";
                    }
                        
                    /*
                    * Fetching list of forwarders, who is offering AIR services.
                    */
                    $productAry = array();
                    $productAry[0] = "AIR"; 
                    $airFreightForwarderListAry = array();
                    $airFreightForwarderListAry = $kForwarder->listAllForwarderByProduct("AIR");
                    if(!empty($airFreightForwarderListAry))
                    {
                        $query_and .=" AND (whs.idForwarder IN (".implode(',',$airFreightForwarderListAry).") AND whs.iWarehouseType = '".__WAREHOUSE_TYPE_AIR__."') ";
                    }
                }
                if($distanceMeasureAry['iQuickQuote']!=1)
                {
                    //$szSpecialQuery = " AND distance < ".(int)$maxWarehouseDistance." ";
                }
                
		$query="
                    SELECT
                        sup.*
                    FROM 
                        (
                            SELECT 
                               whs.id, 
                               whs.id as idWarehouse,
                               whs.szWareHouseName,
                               whs.idForwarder,
                               whs.szUTCOffset ,
                               whs.idCountry,
                               whs.szCity,
                               whs.szPostCode,
                               fbd.szLogo,
                               fbd.szDisplayName,
                               fbd.szForwarderAlias,
                               fbd.szCurrency,
                               cont.fStandardTruckRate,
                               cont.szCountryName,		
                               cont.szCountryISO,
                               cont.fMaxFobDistance,
                               whs.szLatitude,
                               whs.szLongitude,
                               whs.szContactPerson,
                               whs.szEmail, 
                               fbd.dtReferealFeeValid, 
                               fbd.idCountry as idForwarderCountry,
                               (( 3959 * acos( cos( radians(".(float)$distanceMeasureAry['fTotalLatitude'].") ) * cos( radians( whs.szLatitude ) ) 
                               * cos( radians(whs.szLongitude) - radians(".(float)$distanceMeasureAry['fTotalLongitude'].")) + sin(radians(".(float)$distanceMeasureAry['fTotalLatitude'].")) 
                               * sin( radians(whs.szLatitude)))) * 1.609344) AS distance 
                               $query_select
                               $query_whs_select
                            FROM 
                                ".__DBC_SCHEMATA_WAREHOUSES__." whs
                            INNER JOIN
                                ".__DBC_SCHEMATA_COUNTRY__." cont
                            ON
                                cont.id = whs.idCountry				
                            INNER JOIN
                                ".__DBC_SCHEMATA_FORWARDERS__."	fbd
                            ON
                                fbd.id = whs.idForwarder 
                            WHERE
                                whs.iActive = '1'
                            AND
                                fbd.iActive = '1'
                                ".$query_excluded_forwarder_select."
                                ".$query_and."		
                            HAVING   
                                $query_having 
                            ORDER BY 
                                distance ASC
                            LIMIT
                                0, ".$iWareHouseLimit."
                        ) as sup 
                    ORDER BY 
                        sup.distance ASC
                    LIMIT
                        0, ".$iWareHouseLimit."
		";	
//                echo $query;
//                die; 
                if($bHaulageRequired)
                {
                    $this->szLCLCalculationLogs .= "<br>WHS LIMIT: ".$iWareHouseLimit." <strong> Mode: ".$szWarehouseSearchMode."</strong>";
                }
                else
                {
                    $this->szLCLCalculationLogs .= "<br> Distance Range: ".$maxWarehouseDistance." WHS LIMIT: ".$iWareHouseLimit." <strong> Mode: ".$szWarehouseSearchMode."</strong>";
                }
		//$this->szLCLCalculationLogs .= "<br>".$query."<br>";
                        
		if($result = $this->exeSQL($query))
		{
                    $wareHouseAry = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    { 
                        if((int)$row['fMaxFobDistance']>0 && $iDirection==1 && $row['fMaxFobDistance']<$row['distance']) //Origin CFS
                        {
                            //if fMaxFobDistance is defined for origin country and cfs distance is greater the defined FOB distance then we do not show that option. 
                        }
                        else
                        { 
                            $bEligibleWarehouse = true;
                            if($bHaulageRequired)
                            {
                                if($row['iHaulageAvailabale']>0)
                                {
                                    $bEligibleWarehouse = true;
                                }
                                else
                                {
                                    $bEligibleWarehouse = false;
                                    $this->szLCLCalculationLogs .= "<br>Forwarder: ".$row['szDisplayName'].", CFS: ".$row['szWareHouseName']." - Distance: ".ceil($row['distance'])." - Not Included"; 
                                    $this->szLCLCalculationLogs .= "<br> Description: No haulage defined ";
                                }
                            } 
                            if($bEligibleWarehouse)
                            { 
                               // echo "<br> WHS ID:  ".$row['id']." Direc ".$iDirection ." Haul: ".$row['iHaulageAvailabale'] ;
                                if($distanceMeasureAry['iRailTransport']==1)
                                {
                                    $wareHouseAry[$row['id']]['iRailTransport'] = 1;
                                }
                                $wareHouseAry[$row['id']]['iDistance'] = ceil($row['distance']);
                                $wareHouseAry[$row['id']]['szWareHouseName'] = $row['szWareHouseName'];
                                $wareHouseAry[$row['id']]['idForwarder'] = $row['idForwarder'];
                                $wareHouseAry[$row['id']]['fStandardTruckRate'] = $row['fStandardTruckRate'];	
                                $wareHouseAry[$row['id']]['szLogo'] = $row['szLogo'];	
                                $wareHouseAry[$row['id']]['szCountry'] = $row['szCountryName'];		
                                $wareHouseAry[$row['id']]['szCity'] = $row['szCity'];	
                                $wareHouseAry[$row['id']]['idForwarder'] = $row['idForwarder'];	
                                $wareHouseAry[$row['id']]['szDisplayName'] = $row['szDisplayName'];
                                $wareHouseAry[$row['id']]['szForwarderAlias'] = $row['szForwarderAlias']; 
                                $wareHouseAry[$row['id']]['szUTCOffset'] = $row['szUTCOffset'];	
                                $wareHouseAry[$row['id']]['idCountry'] = $row['idCountry'];	
                                $wareHouseAry[$row['id']]['szPostCode'] = $row['szPostCode'];	
                                $wareHouseAry[$row['id']]['szCountryISO'] = $row['szCountryISO'];	
                                $wareHouseAry[$row['id']]['szEmail'] = $row['szEmail'];	
                                $wareHouseAry[$row['id']]['szContactPerson'] = $row['szContactPerson']; 
                                $wareHouseAry[$row['id']]['id'] = $row['id'];
                                $wareHouseAry[$row['id']]['szLatitude'] = $row['szLatitude'];	
                                $wareHouseAry[$row['id']]['szLongitude'] = $row['szLongitude'];
                                $wareHouseAry[$row['id']]['szForwarderCurrency'] = $row['szCurrency'];	
                                $wareHouseAry[$row['id']]['fReferalFee'] = $row['fReferalFee'];
                                $wareHouseAry[$row['id']]['dtReferealFeeValid'] = $row['dtReferealFeeValid'];
                                $wareHouseAry[$row['id']]['idForwarderCountry'] = $row['idForwarderCountry']; 

                                $forwarderCurrencyAry = array();
                                if($row['szCurrency']==1)
                                {
                                    $wareHouseAry[$row['id']]['szForwarderCurrencyName'] = 'USD';
                                    $wareHouseAry[$row['id']]['fForwarderCurrencyExchangeRate'] = 1.00;
                                }
                                else
                                {
                                    if(!empty($alreadyUsedCurrency[$row['szCurrency']]))
                                    {
                                        $forwarderCurrencyAry = $alreadyUsedCurrency[$row['szCurrency']];
                                    }
                                    else
                                    {
                                        $forwarderCurrencyAry = $this->getCurrencyDetails($row['szCurrency']);		
                                        $alreadyUsedCurrency[$row['szCurrency']] = $forwarderCurrencyAry ;
                                    } 
                                    if(!empty($forwarderCurrencyAry))
                                    {					
                                        $wareHouseAry[$row['id']]['szForwarderCurrencyName'] = $forwarderCurrencyAry['szCurrency'];
                                        $wareHouseAry[$row['id']]['fForwarderCurrencyExchangeRate'] = $forwarderCurrencyAry['fUsdValue'];
                                    }
                                } 
                                $this->szLCLCalculationLogs .= "<br><strong>Forwarder: ".$row['szDisplayName'].", CFS: ".$wareHouseAry[$row['id']]['szWareHouseName']." - Distance: ".$wareHouseAry[$row['id']]['iDistance']." - Included </strong>"; 
                            }	
                            $ctr++;
                        } 
                    }  
                    
                    $actualCtr = 0;
                    if(!empty($wareHouseAry))
                    {
                        $idCountry = $distanceMeasureAry['idCountry']; 
                        
                        $ret_ary = array();
                        $globalCountryServicedAry = array();
                        foreach($wareHouseAry as $key=>$wareHouseArys)
                        {
                            $countryServicedAry = array();
                            $idWhsCountry = $wareHouseAry[$key]['idCountry'];

                            /*
                             * by default we are taking warehouse country as a serviced by WHS
                            if($idCountry == $idWhsCountry)
                            {
                                    $ret_ary[$key] = $wareHouseArys ;
                            }
                            else
                            {
                            */
                                if(!empty($globalCountryServicedAry) && array_key_exists($key,$globalCountryServicedAry))
                                {
                                    $countryServicedAry = $globalCountryServicedAry[$key];
                                    if(!empty($countryServicedAry) && in_array($idCountry,$countryServicedAry))
                                    {
                                        $ret_ary[$key] = $wareHouseArys ;
                                        $actualCtr++;
                                    }
                                }
                                else
                                {
                                    $ret_ary[$key] = $wareHouseArys ;
                                    
                                    /*
                                    $countryServicedAry = $this->getAllCountryServiceByWHS($key); 
                                    $globalCountryServicedAry[$key] = $countryServicedAry;
                                    
                                    if(!empty($countryServicedAry) && in_array($idCountry,$countryServicedAry))
                                    {
                                        $ret_ary[$key] = $wareHouseArys ;
                                        $actualCtr++;
                                    }
                                     * 
                                     */
                                }
                            //}
                            if($actualCtr>=$iWareHouseLimit)
                            {
                                break;
                            }
                        }
                    }
                    //$this->szLCLCalculationLogs .= print_R($ret_ary,true);
                    return $ret_ary;
		}
		else
		{
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
		} 
            } 
	}
        
    function getAllWarehouseType($iWarehouseType=false)
    { 
        if($iWarehouseType>0)
        {
            $query_and= " WHERE id = '".(int)$iWarehouseType."' ";
        }  

        $query="
            SELECT 
                id,
                szTypeCode,
                szWarehouseType as szFriendlyName
            FROM
                ".__DBC_SCHEMATA_WAREHOUSE_TYPE__."
            $query_and 
        ";
        //echo $query;
        if($result=$this->exeSQL($query))
        {
            $ret_ary = array();
            $ctr=0;
            while($row=$this->getAssoc($result))
            {
                $ret_ary[$row['id']] = $row; 
            }
            return $ret_ary;
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
    
    function getBookingDetailsByPriceToken($szReferenceToken)
    { 
        if(!empty($szReferenceToken))
        {
            $query="
                SELECT 
                    id,
                    idBookingStatus,
                    idFile,
                    idShipperConsignee,
                    fValueOfGoods,
                    szGoodsInsuranceCurrency,
                    idCustomerCurrency
                FROM
                    ".__DBC_SCHEMATA_BOOKING__." 
                WHERE
                    szReferenceToken = '".  mysql_escape_custom($szReferenceToken)."'
            ";
            //echo $query;
            if($result=$this->exeSQL($query))
            { 
                $row = $this->getAssoc($result); 
                return $row;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        } 
    }
    
    
    function updateServicePricingByInsuranceAPI($szReferenceToken,$szServiceID=false,$szQuoteID=false)
    {
        if(!empty($szReferenceToken) && (!empty($szServiceID) || !empty($szQuoteID)))
        {  
            $bAutomaticBooking = false;
            $bManualBooking = false;
            
            if(!empty($szServiceID))
            {
                $bAutomaticBooking = true;
            }
            else if(!empty($szQuoteID))
            {
                $bManualBooking = true;
            } 
            else
            {
                return false;
            }           
            if($bManualBooking)
            {
                $kQuote = new cQuote();
                $bookingQuoteAry = $kQuote->getQuotePricingIDbyKey($szQuoteID);
                $idBookingQuotePricing = $bookingQuoteAry['id']; 
                $idBooking = $bookingQuoteAry['idBooking'];
                if($idBooking<=0)
                {
                    self::$szDeveloperNotes .= ">. No booking is associated with szQuoteID ".PHP_EOL;
                    $errorFlag = true;
                    $this->buildErrorDetailAry('szServiceTokenAlreadyUsed','NOSERVICE');  
                }
                $kBooking = new cBooking();
                $bookingDataAry = array();
                $bookingDataAry = $kBooking->getBookingDetails($idBooking); 
                $idBookingFile = $bookingDataAry['idFile'];
                
                
                if($bookingDataAry['idBookingStatus']=='3' || $bookingDataAry['idBookingStatus']=='4' || $bookingDataAry['idBookingStatus']=='7')
                {
                    self::$szDeveloperNotes .= ">. Booking is already placed by using same token ".PHP_EOL;
                    $errorFlag = true;
                    $this->buildErrorDetailAry('szServiceTokenAlreadyUsed','NOSERVICE');  
                }
                if($bookingDataAry['iInsuranceChoice']==1)
                {
                    $iInsuranceChoice='Yes';
                }
                else if($bookingDataAry['iInsuranceChoice']==3)
                {
                    $iInsuranceChoice='Optional';
                }
                else
                {
                    $iInsuranceChoice='No';
                }
                $responseArr['bInsuranceAvailable'] = $iInsuranceChoice;
                $responseArr['fCargoValue'] = round((float)$bookingDataAry['fValueOfGoods'],2);
                $responseArr['szCargoCurrency'] = $bookingDataAry['szGoodsInsuranceCurrency'];
                return $responseArr;
            }
            else if($bAutomaticBooking)
            {
                cPartner::$szGlobalToken = $szReferenceToken;
            
                $kPartner = new cPartner();
                $kBooking = new cBooking();
                $idPartner = cPartner::$idGlobalPartner;  

                /*
                * First thing first, we are fetching booking ID from tblbookings on the basis of szReferenceToken 
                * If we finds matching record in tblbookings then we go ahead and update pricing and all for the booking 
                * If we don't find matching record in tblbooking then we just return False and this process should be stopped at this point.
                */ 

                $kPartner = new cPartner();
                $bookingDataAry = array();
                $bookingDataAry = $this->getBookingDetailsByPriceToken($szReferenceToken);

                $idBooking = 0;
                if(!empty($bookingDataAry))
                {
                    if($bookingDataAry['idBookingStatus']==3 || $bookingDataAry['idBookingStatus']==4 || $bookingDataAry['idBookingStatus']==7)
                    {
                        /*
                        * 3. Booking confirmed
                        * 4. Booking Archived
                        * 7. Booking Cancelled
                        */
                        cPartner::$szDeveloperNotes .= ">. Booking is already placed using the same token because this booking already has status: ".$bookingDataAry['idBookingStatus'].". Stopped! ".PHP_EOL;
                        $errorFlag = true;
                        $bServiceTokenAlreadyUsed = true;
                    }
                    else
                    {
                        $idBooking = $bookingDataAry['id'];
                        $idBookingFile = $bookingDataAry['idFile']; 
                    }
                }

                if($idBooking<=0)
                {
                    cPartner::$szDeveloperNotes .= ">. We did not find any matching row in tblbookings for the Token: ".$szReferenceToken.". Stopped! ".PHP_EOL;
                    return false;
                }

                $serviceResponseAry = array();
                $serviceResponseAry = $kPartner->getServiceResponseDetails($idPartner,$szReferenceToken,$szServiceID); 
                if(empty($serviceResponseAry))
                {
                    cPartner::$szDeveloperNotes .= ">. There is no service response matching with requested paramenters ".PHP_EOL; 
                    return false;
                } 
                cPartner::$szDeveloperNotes .= ">. We found service response matching with requested paramenters. Going forward ".PHP_EOL;

                $idShipmentType = $serviceResponseAry['iShipmentType'];
                $iBookingType = $serviceResponseAry['iBookingType'];
                $idPackingType = $serviceResponseAry['idPackingType'];
                $idPartnerApiResponse = $serviceResponseAry['id'];
                $fTotalInsuranceCostForBookingCustomerCurrency = $serviceResponseAry['fTotalInsuranceCostForBookingCustomerCurrency'];  

                $priceRequestParamAry = array();
                $priceRequestParamAry = $kPartner->getQuickQuoteRequestData('price',false,$szReferenceToken);

                /*
                * Checking if partner has made validatePrice request call for this szServiceID or not
                */
                $validateRequestParamAry = array();
                $validateRequestParamAry = $kPartner->getQuickQuoteRequestData('validatePrice',$szServiceID,$szReferenceToken);

                $tempPostSearchAry = array();
                if(!empty($validateRequestParamAry))
                {
                    /*
                    * As partner has already had made 'validatePrie' request for ths service ID so this is why we are comparing parameters with the request parameters what is passed in 'validatePrice' API call
                    */
                    $szRequestType = "validatePrice";
                    $tempPostSearchAry = $validateRequestParamAry;
                }
                else
                {
                    $tempPostSearchAry = $priceRequestParamAry;
                    $szRequestType = "Price";
                }  

                if(empty($tempPostSearchAry))
                { 
                    cPartner::$szDeveloperNotes .= ">. We did not found requested paramenters for either 'Price' call or 'Validate Price' API for the corresponding token. Stopped! ".PHP_EOL;
                    return false;
                } 

                //Calculate idServiceTerms by using szServiceTerm

                $szServiceTerm = $tempPostSearchAry['szServiceTerm'];
                if(empty($szServiceTerm) || $szServiceTerm=='ALL')
                {
                    $szServiceTerm =__SERVICE_SHORT_TERMS_EXW__;
                }

                $kConfig = new cConfig();
                $serviceTermsAry = array();
                $serviceTermsAry = $kConfig->getAllServiceTerms(false,$szServiceTerm);

                if(!empty($serviceTermsAry))
                {
                    $idServiceTerms = $serviceTermsAry[0]['id'];
                }

                $kConfig = new cConfig();
                $kConfig->loadCountry($tempPostSearchAry['szShipperCountry']);
                $szOriginCountryName = $kConfig->szCountryName;

                $kConfig = new cConfig();
                $kConfig->loadCountry($tempPostSearchAry['szConsigneeCountry']);
                $szDestinationCountryName = $kConfig->szCountryName;

                if($tempPostSearchAry['iDonotKnowShipperPostcode'])
                {
                    $tempPostSearchAry['szShipperPostCode'] = "";
                }
                if($tempPostSearchAry['iDonotKnowConsigneePostcode'])
                {
                    $tempPostSearchAry['szConsigneePostCode'] = "";
                }

                $szOriginCountryStr = trim($tempPostSearchAry['szShipperPostCode']." ".$tempPostSearchAry['szShipperCity']." ".$szOriginCountryName); 
                $szDestinationCountryStr = trim($tempPostSearchAry['szConsigneePostCode']." ".$tempPostSearchAry['szConsigneeCity']." ".$szDestinationCountryName); 

                $postSearchAry = $tempPostSearchAry;
                $postSearchAry['dtTimingDate'] = $tempPostSearchAry['dtShipping'];
                $postSearchAry['szOriginPostCode'] = $tempPostSearchAry['szShipperPostCode'];
                $postSearchAry['szOriginCity'] = $tempPostSearchAry['szShipperCity'];
                $postSearchAry['szOriginCountry'] = $szOriginCountryStr;
                $postSearchAry['idOriginCountry'] = $tempPostSearchAry['szShipperCountry'];
                $postSearchAry['fOriginLatitude'] = $tempPostSearchAry['szShipperLattitude'];
                $postSearchAry['fOriginLongitude'] = $tempPostSearchAry['szShipperLongitude'];

                $postSearchAry['idDestinationCountry'] = $tempPostSearchAry['szConsigneeCountry'];
                $postSearchAry['szDestinationCountry'] = $szDestinationCountryStr;
                $postSearchAry['szDestinationPostCode'] = $tempPostSearchAry['szConsigneePostCode'];
                $postSearchAry['szDestinationCity'] = $tempPostSearchAry['szConsigneeCity'];
                $postSearchAry['fDestinationLongitude'] = $tempPostSearchAry['szConsigneeLattitude'];
                $postSearchAry['fDestinationLatitude'] = $tempPostSearchAry['szConsigneeLongitude']; 
                $postSearchAry['idServiceTerms'] = $idServiceTerms;
                $postSearchAry['iBookingQuoteShipmentType'] = $idShipmentType;

                cPartner::$GlobalpostSearchAry = $postSearchAry;
                cPartner::$szRequestType = $szRequestType; 

                $this->addAutomaticBookingDetailsByCustomerID($szReferenceToken,$postSearchAry);

                $bookingDataAry = array();
                $bookingDataAry = $this->getBookingDetailsByPriceToken($szReferenceToken);

                if(!empty($bookingDataAry))
                { 
                    if($bookingDataAry['idBookingStatus']==3 || $bookingDataAry['idBookingStatus']==4 || $bookingDataAry['idBookingStatus']==7)
                    {
                        /*
                        * 3. Booking confirmed
                        * 4. Booking Archived
                        * 7. Booking Cancelled
                        */
                        cPartner::$szDeveloperNotes .= ">. Booking is already placed using the same token because this booking already has status: ".$bookingDataAry['idBookingStatus'].". Stopped! ".PHP_EOL;
                        $errorFlag = true;
                        $bServiceTokenAlreadyUsed = true;
                    }
                    else
                    {
                        $idBooking = $bookingDataAry['id'];
                        $idBookingFile = $bookingDataAry['idFile']; 
                    } 
                }
                else
                {
                    $errorFlag = true;
                }
                //Resetting $idPartnerCustomer to NULL as in Insurance Price API we never gets szCustomerToken for booking.
                cPartner::$idPartnerCustomer = '';
                if($errorFlag)
                {
                    return false;
                }
                else
                {
                    if($iBookingType==2)
                    {
                        /*
                        * Update data for courier bookings
                        */
                        $bookingUpdatedFlag = $kBooking->updateCourierServiceForwarderDetails($idBooking,$szServiceID,true);
                    }
                    else
                    {
                        /*
                        * Update data for LCL bookings
                        */ 
                        $bookingUpdatedFlag = $kBooking->updateForwarderDetails($idBooking,$szServiceID,true); 
                    }
                    $responseArr['fCargoValue']=round((float)$bookingDataAry['fValueOfGoods'],2);
                    $responseArr['szCargoCurrency']=$bookingDataAry['szGoodsInsuranceCurrency'];
                    return $responseArr;
                }
            } 
        } 
    }
    
    
    function createBookingByToken($szReferenceToken,$postSearchAry,$idCustomer=false,$bCreateBookingQuote=false)
    {
        if(!empty($szReferenceToken) && !empty($postSearchAry))
        { 
            $kUser = new cUser();
            if($idCustomer>0)
            {
                $kUser->getUserDetails($idCustomer); 
                $szEmail=$kUser->szEmail;
            } 
            
            $kPartner = new cPartner();
            $bookingDataAry = array();
            $bookingDataAry = $this->getBookingDetailsByPriceToken($szReferenceToken);
            $iAddedNewBooking = false;
            if(!empty($bookingDataAry))
            {
                if($bookingDataAry['idBookingStatus']==3 || $bookingDataAry['idBookingStatus']==4 || $bookingDataAry['idBookingStatus']==7)
                {
                    /*
                    * 3. Booking confirmed
                    * 4. Booking Archived
                    * 7. Booking Cancelled
                    */
                    cPartner::$szDeveloperNotes .= ">. Booking is already placed using the same token because this booking already has status: ".$bookingDataAry['idBookingStatus'].". Stopped! ".PHP_EOL;
                    $errorFlag = true;
                    $bServiceTokenAlreadyUsed = true;
                }
                else
                {
                    $idBooking = $bookingDataAry['id'];
                    $idBookingFile = $bookingDataAry['idFile'];
                    $idShipperConsignee = $bookingDataAry['idShipperConsignee'];
                }
            }
            else
            {
                /*
                * Creating actual booking into the system
                */
                $idBooking = $kPartner->createEmptyBooking();
                if($idBooking<=0)
                {
                    cPartner::$szDeveloperNotes .= ">. Create booking function returning with error message because we could not able to create Booking ID in tblbookings ".PHP_EOL; 
                    $errorFlag = true; 
                    $bCommunication = true;
                } 
                else
                {
                    cPartner::$szDeveloperNotes .= ">. Empty booking created successfully. Booking id is:".$idBooking.", Going forward ".PHP_EOL;
                }  
                
                $filename = __APP_PATH_ROOT__."/logs/file_create_for_transporteca_user_".date('Y-m-d').".log";
                
                
                $strdata=array();
                $strdata[0]=" \n\n ***ENVIRONMENT ".__ENVIRONMENT__." *** \n";
                $strdata[1]="szEmail--".$szEmail;
                $strdata[2]="idCustomer--".$idCustomer;
                $strdata[3]=" \n\n ***ENVIRONMENT ".__ENVIRONMENT__." *** \n";
                file_log_session(true, $strdata, $filename);
        
                $iAddBookingFile=false;
                if(__ENVIRONMENT__=='LIVE' || __ENVIRONMENT__ == "DEV_AWS")
                {
                    if(doNotCreateFileForThisDomainEmail($szEmail))
                    {
                        $iAddBookingFile = true;
                    }
                    else
                    {
                        $this->deleteSearchDoneByTransportecaUser($szReferenceToken);
                    }
                }
                else
                {
                    $iAddBookingFile = true;
                }
                
                $strdata=array();
                $strdata[0]=" \n\n ***ENVIRONMENT ".__ENVIRONMENT__." *** \n";
                $strdata[1]="szEmail--".$szEmail;
                $strdata[2]="idCustomer--".$idCustomer;
                $strdata[2]="iAddBookingFile--".$iAddBookingFile;
                $strdata[4]=" \n\n ***ENVIRONMENT ".__ENVIRONMENT__." *** \n";
                file_log_session(true, $strdata, $filename);
                
                /*
                * Creating booking file into the system
                */
                if($iAddBookingFile)
                {
                    $idBookingFile = $kPartner->createBookingFile($postSearchAry,$kUser,$idBooking,$bCreateBookingQuote);
                    if($idBookingFile<=0)
                    {
                        $errorFlag = true;
                        cPartner::$szDeveloperNotes .= ">. Create booking function returning with error message because we could not able to create Booking file in tblbookingfiles ".PHP_EOL; 
                        $bCommunication = true;
                    }   
                    else
                    {
                        cPartner::$szDeveloperNotes .= ">. Booking file created successfully. File ID: ".$idBookingFile.", Going forward ".PHP_EOL;
                    }
                }
                $iAddedNewBooking = 1;
            }
            
            if($errorFlag)
            {
                $szStatus = "ERROR";
            }
            else
            {
                $szStatus = "SUCCESS";
            }
            
            $ret_ary = array();
            $ret_ary['szStatus'] = $szStatus;
            $ret_ary['bCommunication'] = $bCommunication;
            $ret_ary['bServiceTokenAlreadyUsed'] = $bServiceTokenAlreadyUsed;
            $ret_ary['idBooking'] = $idBooking;
            $ret_ary['idBookingFile'] = $idBookingFile; 
            $ret_ary['iAddedNewBooking'] = $iAddedNewBooking; 
            return $ret_ary; 
        } 
    } 
    
    function addAutomaticBookingDetailsByCustomerID($szReferenceToken,$postSearchAry,$idCustomer=false,$searchResultAry=array())
    {
        if(!empty($szReferenceToken) && !empty($postSearchAry))
        {
            //print_r($postSearchAry);
            $kPartner = new cPartner();
            $kBooking = new cBooking();
             
            $iCreateQuoteFlag=false;
            if((int)$postSearchAry['iSearchStandardCargo']==1)
            {
                $iCreateQuoteFlag=true;
            }
            $bookingDataAry = array();
            $bookingDataAry = $this->createBookingByToken($szReferenceToken,$postSearchAry,$idCustomer,$iCreateQuoteFlag);
           
            $iAddedNewBooking = $bookingDataAry['iAddedNewBooking']; 
            if($bookingDataAry['szStatus']=='ERROR')
            {
                return false;
            }
            else if($bookingDataAry['idBooking']>0)
            {
                $idBooking = $bookingDataAry['idBooking'];
                $idBookingFile = $bookingDataAry['idBookingFile']; 
            }
            else
            {
                return false;
            }

            /*
            * Creating shipper consignee details into the system
            */ 
            $idShipperConsignee = $kPartner->createShipperConsignee($postSearchAry,$idBooking); 
            if($idShipperConsignee<=0)
            {
                $errorFlag = true;
                cPartner::$szDeveloperNotes .= ">. Create booking function returning with error message because we could not able to create shipper consignee details ".PHP_EOL; 
            }  
            else
            {
                cPartner::$szDeveloperNotes .= ">. Shipper consignee row created successfully. ID: ".$idShipperConsignee.", Going forward ".PHP_EOL;
            }
  
            /*
            *  getting total of shipment value for the booking
            */ 
            if($postSearchAry['iBookingQuoteShipmentType']>0)
            {
                $idShipmentType = $postSearchAry['iBookingQuoteShipmentType'];
            }
            else
            {
                $idShipmentType = cPartner::$idGlobalShipmentType;
            }  
            
            $cargoDetailsAry = array();
            $cargoDetailsAry = $kPartner->updateBookingCargDetails($idShipmentType,false,false,false);
            
            
            
            $dtCurrentDateTime = $kBooking->getRealNow();

            $kWarehouseSearch = new cWHSSearch();
            $resultAry = $kWarehouseSearch->getCurrencyDetails(20); //DKK	 
            $fDkkExchangeRate = 0;
            if($resultAry['fUsdValue']>0)
            {
                $fDkkExchangeRate = $resultAry['fUsdValue'] ;						 
            } 
            /*
            * Updating payment details to booking.
            * Note: Booking created by Partner API is always a Bank Trasfer booking
            */

            if($kUser->szCountry==$postSearchAry['idOriginCountry'])
            {
                $iShipperConsignee = 1;
            }
            else if($kUser->szCountry==$postSearchAry['idDestinationCountry'])
            {
                $iShipperConsignee = 2;
            }
            else
            {
                $iShipperConsignee = 3;
            }
            
            if($postSearchAry['iDonotKnowShipperPostcode'])
            {
                $postSearchAry['szOriginPostCode'] = "";
            }
            if($postSearchAry['iDonotKnowConsigneePostcode'])
            {
                $postSearchAry['szDestinationPostCode'] = "";
            }
            $szCalledFromAPIType = cPartner::$szCalledFromAPIType;
            
            $bookingAry = array();
            $bookingAry['idBookingStatus'] = __BOOKING_STATUS_DRAFT__; 
            $bookingAry['idServiceType'] = $postSearchAry['idServiceType']; 
            $bookingAry['iOriginCC'] = $postSearchAry['iOriginCC'];
            $bookingAry['iDestinationCC'] = $postSearchAry['iDestinationCC'];
            $bookingAry['idTimingType'] = $postSearchAry['idTimingType'];
            $bookingAry['dtTimingDate'] = $postSearchAry['dtTimingDate']; 
            $bookingAry['idOriginCountry'] = $postSearchAry['idOriginCountry'];
            $bookingAry['szOriginCountry'] = $postSearchAry['szOriginCountry'];
            $bookingAry['szOriginPostCode'] = $postSearchAry['szOriginPostCode'];
            $bookingAry['szOriginCity'] = $postSearchAry['szOriginCity'];
            $bookingAry['fOriginLatitude'] = $postSearchAry['fOriginLatitude'];
            $bookingAry['fOriginLongitude'] = $postSearchAry['fOriginLongitude'];  
            $bookingAry['idDestinationCountry'] = $postSearchAry['idDestinationCountry'];
            $bookingAry['szDestinationCountry'] = $postSearchAry['szDestinationCountry'];
            $bookingAry['szDestinationPostCode'] = $postSearchAry['szDestinationPostCode'];
            $bookingAry['szDestinationCity'] = $postSearchAry['szDestinationCity']; 
            $bookingAry['fDestinationLongitude'] = $postSearchAry['fDestinationLongitude'];
            $bookingAry['fDestinationLatitude'] = $postSearchAry['fDestinationLatitude']; 
            $bookingAry['szPartnerReference'] = $postSearchAry['szPartnerReference'];
            $bookingAry['idServiceTerms'] = $postSearchAry['idServiceTerms']; 
            $bookingAry['fCargoVolume'] = $cargoDetailsAry['fTotalVolume'];
            $bookingAry['fCargoWeight'] = $cargoDetailsAry['fTotalWeight']; 
            $bookingAry['iBookingLanguage'] = $postSearchAry['iBookingLanguage'];
            $bookingAry['iBookingStep'] = 8;
            $bookingAry['dtBookingInitiated'] = $dtCurrentDateTime;
            $bookingAry['iFromRequirementPage'] = 2;
            $bookingAry['idCustomerCurrency'] = $postSearchAry['idCurrency'];
            $bookingAry['szCustomerCurrency'] = $postSearchAry['szCurrency'];
            $bookingAry['fCustomerExchangeRate'] = $postSearchAry['fExchangeRate'];
            $bookingAry['iShipperConsignee'] = $iShipperConsignee; 
            $bookingAry['iTotalQuantity'] = $cargoDetailsAry['iTotalQuantity']; 
            $bookingAry['idFile'] = $idBookingFile;
            $bookingAry['iNumColli'] = $cargoDetailsAry['iNumColli'];    
            $bookingAry['fDkkExchangeRate'] = $fDkkExchangeRate; 
            $bookingAry['szInvoiceComment'] = $postSearchAry['szParnerReference'];
            $bookingAry['iRemovePriceGuarantee'] = $postSearchAry['iRemovePriceGuarantee'];
            
            if(strtolower($postSearchAry['iShipperConsigneeType'])=='consignee')
            {
                $bookingAry['iShipperConsignee']=2;
            }
            else if(strtolower($postSearchAry['iShipperConsigneeType'])=='shipper')
            {
                $bookingAry['iShipperConsignee']=1;
            }
            else
            {
                $bookingAry['iShipperConsignee']=3;
            }
            
            
            if((int)$postSearchAry['iSearchStandardCargo']==1)
            {
                $bookingAry['iVogaSearchAutomaticService'] = 1;
                $bookingAry['szGoodsInsuranceCurrency']=$postSearchAry['szGoodsInsuranceCurrency'];
                $bookingAry['idGoodsInsuranceCurrency']=$postSearchAry['idGoodsInsuranceCurrency'];
                $bookingAry['fValueOfGoods']=$postSearchAry['fCargoValue'];   
                $bookingAry['idLandingPage'] = $postSearchAry['idDefaultLandingPage'];
                $bookingAry['iSearchMiniVersion'] = $postSearchAry['iSearchMiniVesion'];
                $bookingAry['szCargoDescription'] = $postSearchAry['szCargoDescription'];
                $bookingAry['szCargoType'] = $postSearchAry['szCargoType'];
                
                   
                $bookingAry['iShipperDetailsLocked'] = $postSearchAry['iShipperDetailsLocked'];
                $bookingAry['iConsigneeDetailsLocked'] = $postSearchAry['iConsigneeDetailsLocked'];
                $bookingAry['iCargoDescriptionLocked'] = $postSearchAry['iCargoDescriptionLocked'];
                
                
                $bookingAry['szAutomatedRfqResponseCode'] = $postSearchAry['szAutomatedRfqResponseCode'];
                if($postSearchAry['iInsuranceIncluded']=='1')
                {
                    $bookingAry['iInsuranceChoice']=1;
                }
                else if($postSearchAry['iInsuranceIncluded']=='3')
                {
                    $bookingAry['iInsuranceChoice']=3;
                }
                else
                {
                    $bookingAry['iInsuranceChoice']=2;
                }              
                if($postSearchAry['szAutomatedRfqResponseCode']!='')
                {
                    $bookingAry['iCreateAutomaticQuote'] = 1;
                }                 
                $bookingAry['iCheckHandlingFeeForAutomaticQuote'] = $postSearchAry['iCreateAutomaticQuote'];  
                $bookingAry['idDefaultLandingPage'] = $postSearchAry['idDefaultLandingPage'];   
            }
            
            $bookingAry['idShipperConsignee'] = $idShipperConsignee;
            //$bookingAry['idTransportMode'] = __BOOKING_TRANSPORT_MODE_SEA__;
            $bookingAry['szReferenceToken'] = $szReferenceToken; 
            
            if($szCalledFromAPIType=='CUSTOMER_EVENT')
            {
                $bookingAry['szCargoType'] = $postSearchAry['szCargoType'];
            } 
            
            $bNoserviceFound = cPartner::$bNoserviceFound;
            if($bNoserviceFound)
            {
                $kWHSSearch=new cWHSSearch();
                $szUserDidNotGetAnyOnlineService = $kWHSSearch->getManageMentVariableByDescription('__USER_DID_NOT_GET_ANY_ONLINE_SERVICES__');
                $bookingAry['szInternalComment'] = $szUserDidNotGetAnyOnlineService; 
            }
            
            if($idCustomer>0)
            {
                $kUser = new cUser();
                $kUser->getUserDetails($idCustomer);
                
                $bookingAry['idUser'] = $kUser->id ;				
                $bookingAry['szFirstName'] = $kUser->szFirstName ;
                $bookingAry['szLastName'] = $kUser->szLastName ;
                $bookingAry['szEmail'] = $kUser->szEmail ;
                $bookingAry['szCustomerAddress1'] = $kUser->szAddress1;
                $bookingAry['szCustomerAddress2'] = $kUser->szAddress2;
                $bookingAry['szCustomerAddress3'] = $kUser->szAddress3;
                $bookingAry['szCustomerPostCode'] = $kUser->szPostcode;
                $bookingAry['szCustomerCity'] = $kUser->szCity;
                $bookingAry['szCustomerCountry'] = $kUser->szCountry;
                $bookingAry['szCustomerState'] = $kUser->szState;
                $bookingAry['szCustomerPhoneNumber'] = $kUser->szPhoneNumber;
                $bookingAry['szCustomerCompanyName'] = $kUser->szCompanyName;	
                $bookingAry['szCustomerCompanyRegNo'] = $kUser->szCompanyRegNo;
                $bookingAry['idCustomerDialCode'] = $kUser->idInternationalDialCode;
                $bookingAry['iPrivateShipping'] = $kUser->iPrivate; 
            } 
            if((int)$postSearchAry['iSearchStandardCargo']==1)
            {                
                if($postSearchAry['szAutomatedRfqResponseCode']=='')
                {
                    $postSearchAry['szInternalComment'] = $postSearchAry['szInternalComments'];
                    $szInternalComment = $postSearchAry['szInternalComment'];
                    $bookingAry['szInternalComment'] = $szInternalComment;
                    $bookingAry['iBookingType'] = __BOOKING_TYPE_RFQ__;
                    $bookingAry['iBookingQuotes'] = 1;
                    $bookingAry['iQuotesStatus'] = __BOOKING_QUOTES_STATUS_NEW_BOOKING__;
                    
                    if($postSearchAry['idServiceType']==__SERVICE_TERMS_EXW__)
                    {
                        $bookingAry['szHandoverCity'] = $postSearchAry['szOriginCity'];
                    }
                    else if($postSearchAry['idServiceType']==__SERVICE_TERMS_DAP__ || $postSearchAry['idServiceType']==__SERVICE_TERMS_DAP_NO_CC__)
                    {
                        $bookingAry['szHandoverCity'] = $postSearchAry['szDestinationCity'];
                    } 
                }
                else
                {
                    $szInternalComment = $postSearchAry['szInternalComments'];
                    $bookingAry['szInternalComment'] = $szInternalComment;
                    $bookingAry['iBookingType'] = __BOOKING_TYPE_RFQ__;
                }
            }
            else
            {
                $bookingAry['iBookingType'] = __BOOKING_TYPE_AUTOMATIC__;
            }
            
            $update_query = ''; 
            if(!empty($bookingAry))
            {
                foreach($bookingAry as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            }
            $update_query = rtrim($update_query,","); 
            if($kBooking->updateDraftBooking($update_query,$idBooking))
            {   
                
                $postSearchAry['id']=$idBooking;
                $postSearchAry['idFile']=$idBookingFile;
                $kWHSSearch = new cWHSSearch();                        
                if((int)$postSearchAry['iSearchStandardCargo']==1)
                {
                    if($postSearchAry['szAutomatedRfqResponseCode']=='')
                    {
                        $kBooking = new cBooking();
                        $dtResponseTime = $kBooking->getRealNow();

                        $fileLogsAry = array(); 
                        $fileLogsAry['szTransportecaStatus'] = 'S1';
                        $fileLogsAry['szTransportecaTask'] = 'T1';
                        $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
                        $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime;
                        $fileLogsAry['iSearchType'] = 2;
                        if(!empty($fileLogsAry))
                        {
                            $file_log_query = "";
                            foreach($fileLogsAry as $key=>$value)
                            {
                                $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                            }
                        } 
                        $kBooking_new = new cBooking();
                        $file_log_query = rtrim($file_log_query,",");   
                        //echo $file_log_query;
                        $kBooking_new->updateBookingFileDetails($file_log_query,$idBookingFile,true);
                    }
                    else
                    {
                        $kBooking = new cBooking();
                        $dtResponseTime = $kBooking->getRealNow();

                        $fileLogsAry = array(); 
                        $fileLogsAry['szTransportecaTask'] = '';
                        $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
                        $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime;
                        $fileLogsAry['iSearchType'] = 2;
                        if(!empty($fileLogsAry))
                        {
                            $file_log_query = "";
                            foreach($fileLogsAry as $key=>$value)
                            {
                                $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                            }
                        } 
                        $kBooking_new = new cBooking();
                        $file_log_query = rtrim($file_log_query,",");   
                        //echo $file_log_query;
                        $kBooking_new->updateBookingFileDetails($file_log_query,$idBookingFile,true);

                        $kConfig= new cConfig();
                        //$idDefaultLandingPage=$postSearchAry['idDefaultLandingPage'];
                        //$kConfig->createQuoteForVogaBooking($postSearchAry,$searchResultAry,$idDefaultLandingPage,false,1);
                    }
                }
                if(($szCalledFromAPIType=='PRICE' || $szCalledFromAPIType=='CUSTOMER_EVENT') && $iAddedNewBooking==1)
                {
                    /*
                    * We only updated following values once this is being called from 'Price' API call
                    */
                    $bookingDataAry = array();
                    $bookingDataAry = $kBooking->getBookingDetails($idBooking);
                    $bookingDataAry['szAdminFirstName'] = "API";
                    $kBooking->insertBookingSearchLogs($bookingDataAry);
                    
                    /*
                    * Updating cargo details for courier bookings
                    */
                    cPartner::$szDeveloperNotes .= ">. Updating cargo lines. Going forward ".PHP_EOL;
                    $kPartner->updateBookingCargDetails($idShipmentType,$idBooking,true); 

                    /*
                    * Updating cargo description
                    */
                    $kRegisterShipCon = new cRegisterShipCon(); 
                    $cargoAry = array();
                    $cargoAry['szCargoCommodity'] = $postSearchAry['szCargoDescription']; 
                    $cargoAry['idBooking'] = $idBooking ;
                    $kRegisterShipCon->addUpdateCargoCommodity($cargoAry); 
                } 
            }
            return true;
        } 
    }
    
    
    function updateQuoteDetailsToBooking($idQuotePricing,$iBookingLanguage)
    {
        if($idQuotePricing>0)
        { 
            $kBooking = new cBooking();
            $kWHSSearch = new cWHSSearch();
            $kVatApplication = new cVatApplication();

            $data = array();
            $data['idBookingQuotePricing'] = $idQuotePricing ;
            $quotePricingDetailsAry = array();
            $quotePricingDetailsAry = $kBooking->getAllBookingQuotesPricingDetails($data);
            
            $BookingFileName = __APP_PATH_ROOT__."/logs/quote_quick.log";
            $strdata=array();
            $strdata[0]=" \n\n **********************************\n\n"; 
            $strdata[1]=print_r($quotePricingDetailsAry,true) ;
            file_log(true, $strdata, $BookingFileName);

            if(!empty($quotePricingDetailsAry))
            {
                foreach($quotePricingDetailsAry as $quotePricingDetailsArys)
                {  
                    $fTotalPriceForwarderCurrencyExcludingHandlingFee = $quotePricingDetailsArys['fTotalPriceForwarderCurrency'];
                    $fTotalSelfInvoiceAmountQuoteCurrency = $quotePricingDetailsArys['fTotalPriceForwarderCurrency'];
                    $idBooking = $quotePricingDetailsArys['idBooking'];
                    $idBookingFile = $quotePricingDetailsArys['idFile'];
                    $idBookingQuote = $quotePricingDetailsArys['idQuote'];
                    $idQuotePricingDetails = $quotePricingDetailsArys['id'];
                    $iHandlingFeeApplicable = $quotePricingDetailsArys['iHandlingFeeApplicable'];
                    $iManualFeeApplicable = $quotePricingDetailsArys['iManualFeeApplicable'];
                    $szServiceID = $quotePricingDetailsArys['szServiceID'];
                    $iAddHandlingFee = $quotePricingDetailsArys['iAddHandlingFee']; 

                    $postSearchAry = array();
                    $postSearchAry = $kBooking->getBookingDetails($idBooking);

                    $szReferenceToken = $postSearchAry['szReferenceToken'];
                    $iQuickQuote = $postSearchAry['iQuickQuote'];
                    $idCustomer = $postSearchAry['idUser'];

                    if($postSearchAry['idBookingStatus']=='3' || $postSearchAry['idBookingStatus']=='4' || $postSearchAry['idBookingStatus']=='7')
                    {
                        //if booking is already paid then we do not update anything into booking just redirect user to next screen
                        $szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
                        $kConfig = new cConfig();
                        $kConfig->loadCountry($postSearchAry['idOriginCountry']);
                        $szCountryStrUrl = $kConfig->szCountryISO ;
                        $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
                        $szCountryStrUrl .= $kConfig->szCountryISO ;

                        $this->szRedirectUrl = __BOOKING_QUOTE_SERVICE_PAGE_URL__."/".$szBookingRandomNum."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
                        $this->iBookingAlreadyPaid = 1;
                        return false;
                    } 
                    else
                    { 
                        $bookingQuoteAry = array();
                        $bookingQuoteAry = $kBooking->getAllBookingQuotesByFile(false,false,$idBookingQuote); 
                        $bookingQuoteArys = $bookingQuoteAry['1']; 
                        $ret_ary = array();
                        /*
                        *  For quick quote bookings we need to update data for both Automatic Booking and RFQ because Quick quote bookings are hybrid mixture of Automatic and Manual bookings
                        */
                        if($iQuickQuote==__QUICK_QUOTE_SEND_QUOTE__)
                        {  
                            cPartner::$szGlobalToken = $szReferenceToken;  
                            cPartner::$idPartnerCustomer = $idCustomer;
                            cPartner::$idGlobalPartner = __QUICK_QUOTE_DEFAULT_PARTNER_ID__;
                            $idTransportMode = $bookingQuoteArys['idTransportMode'];

                            if($idTransportMode==__BOOKING_TRANSPORT_MODE_COURIER__)
                            {
                                $kBooking->updateCourierServiceForwarderDetails($idBooking,$szServiceID,false,true,$quotePricingDetailsArys); 
                            }
                            else
                            {
                                $kBooking->updateForwarderDetails($idBooking,$szServiceID,false,true,$quotePricingDetailsArys);
                            }

                            $postSearchAry = $kBooking->getBookingDetails($idBooking);

                            if(($postSearchAry['iInsuranceIncluded']==1 || $postSearchAry['iInsuranceChoice']==3) && $postSearchAry['fValueOfGoods']>0)
                            { 
                                /*
                                * Recalculation Insurance for Quick quotes
                                */

                                $insuranceInputAry = array();
                                $insurancePriceAry = array();
                                $insuranceInputAry['iInsurance'] = 1;
                                $insuranceInputAry['idInsuranceCurrency'] = $postSearchAry['idGoodsInsuranceCurrency'];
                                $insuranceInputAry['fValueOfGoods'] = $postSearchAry['fValueOfGoods'];

                                /*
                                * Recalculating Insurance Prices
                                */
                                $kBooking->updateInsuranceDetails($insuranceInputAry,$postSearchAry);  
                            }
                            $szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
                            $_SESSION['load-booking-details-page-for-booking-id'] = $szBookingRandomNum ;

                            /*  
                             *  $ret_ary['iBookingType'] = __BOOKING_TYPE_RFQ__ ; //Automatic Booking
                                $ret_ary['iQuotesStatus '] = __BOOKING_QUOTES_STATUS_SENT__;
                                $ret_ary['iBookingQuotes '] = 1;
                             *  
                             */

                            $kConfig = new cConfig();
                            $kConfig->loadCountry($postSearchAry['idOriginCountry']);
                            $szCountryStrUrl = $kConfig->szCountryISO ;
                            $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
                            $szCountryStrUrl .= $kConfig->szCountryISO ;
 
                            $this->szRedirectUrl = __NEW_SERVICES_PAGE_URL__."/".$szBookingRandomNum."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
                            $this->iSuccess = 1;
                            return true; 
                        } 
                        else
                        {
                            if($iManualFeeApplicable==1)
                            {
                                $ret_ary['iQuickQuote'] = 1;
                            }
                            else
                            {
                                $ret_ary['iQuickQuote'] = 0;
                            } 
                            $bRecalculateInsuranceFlag = false;

                            /*
                            * From Management > Pending Tray > Validate Pane
                             * 1. If admin has added Insurance: Yes and Cargo value: null that means $postSearchAry['iIncludeInsuranceWithZeroCargoValue']=1 and $postSearchAry['iInsuranceIncluded']=1. And for this case we don't recalculate Insurance Pricing again.
                             * 2. If admin has added Insurance: Optional and Cargo value: null that means $postSearchAry['iOptionalInsuranceWithZeroCargoValue']=1 and $postSearchAry['iInsuranceChoice']=1. And for this case we recalculate Insurance Pricing again.
                            */
                            if($postSearchAry['fValueOfGoods']<=0)
                            {
                                $bRecalculateInsuranceFlag = false;
                            }
                            else if($postSearchAry['iInsuranceIncluded']==1 || $postSearchAry['iInsuranceChoice']==3)
                            {
                                $bRecalculateInsuranceFlag = true;
                            }
                            else if(($postSearchAry['iIncludeInsuranceWithZeroCargoValue']==1 && $postSearchAry['iInsuranceIncluded']==1) || ($postSearchAry['iOptionalInsuranceWithZeroCargoValue']==1 && $postSearchAry['iInsuranceChoice']==3))
                            {
                                $bRecalculateInsuranceFlag = true;
                            }
                            else
                            {
                                /*
                                * For quick quote type: 'Send Quote' we don't update insurance values because we already have these with the booking.
                                */
                                $ret_ary['fTotalInsuranceCostForBooking'] = $quotePricingDetailsArys['fTotalInsuranceCostForBooking']; ;
                                $ret_ary['fTotalInsuranceCostForBookingCustomerCurrency'] = $quotePricingDetailsArys['fTotalInsuranceCostForBookingCustomerCurrency']; ;
                                $ret_ary['fTotalInsuranceCostForBookingUSD'] = $quotePricingDetailsArys['fTotalInsuranceCostForBookingUSD']; ;
                                $ret_ary['fTotalAmountInsured'] = $quotePricingDetailsArys['fTotalAmountInsured'];
                                $res_ary['iMinrateApplied'] = $quotePricingDetailsArys['iMinrateApplied'];
                                $ret_ary['idInsuranceRate'] = $quotePricingDetailsArys['idInsuranceSellCurrency'];
                                $ret_ary['fInsuranceUptoPrice'] = $quotePricingDetailsArys['fInsuranceUptoPrice'];
                                $ret_ary['idInsuranceUptoCurrency'] = $quotePricingDetailsArys['idInsuranceUptoCurrency'];
                                $ret_ary['szInsuranceUptoCurrency'] = $quotePricingDetailsArys['szInsuranceUptoCurrency'];
                                $ret_ary['fInsuranceUptoExchangeRate'] = $quotePricingDetailsArys['fInsuranceUptoExchangeRate'];
                                $ret_ary['fInsuranceRate'] = $quotePricingDetailsArys['fInsuranceRate'];
                                $ret_ary['fInsuranceMinPrice'] = $quotePricingDetailsArys['fInsuranceMinPrice'];
                                $ret_ary['idInsuranceMinCurrency'] = $quotePricingDetailsArys['idInsuranceMinCurrency']; 
                                $ret_ary['szInsuranceMinCurrency'] = $quotePricingDetailsArys['szInsuranceMinCurrency'];
                                $ret_ary['fInsuranceMinExchangeRate'] = $quotePricingDetailsArys['fInsuranceMinExchangeRate'];
                                $ret_ary['fInsuranceUptoPriceUSD'] = $quotePricingDetailsArys['fInsuranceUptoPriceUSD'];
                                $ret_ary['fInsuranceMinPriceUSD'] = $quotePricingDetailsArys['fInsuranceMinPriceUSD'];

                                $ret_ary['idInsuranceRate_buyRate'] = $quotePricingDetailsArys['idInsuranceRate_buyRate'];
                                $ret_ary['fInsuranceUptoPrice_buyRate'] = $quotePricingDetailsArys['fInsuranceUptoPrice_buyRate'];
                                $ret_ary['idInsuranceUptoCurrency_buyRate'] = $quotePricingDetailsArys['idInsuranceUptoCurrency_buyRate'];
                                $ret_ary['szInsuranceUptoCurrency_buyRate'] = $quotePricingDetailsArys['szInsuranceUptoCurrency_buyRate'];
                                $ret_ary['fInsuranceUptoExchangeRate_buyRate'] = $quotePricingDetailsArys['fInsuranceUptoExchangeRate_buyRate'];
                                $ret_ary['fInsuranceRate_buyRate'] = $quotePricingDetailsArys['fInsuranceRate_buyRate'];
                                $ret_ary['fInsuranceMinPrice_buyRate'] = $quotePricingDetailsArys['fInsuranceMinPrice_buyRate'];
                                $ret_ary['idInsuranceMinCurrency_buyRate'] = $quotePricingDetailsArys['idInsuranceMinCurrency_buyRate'];
                                $ret_ary['szInsuranceMinCurrency_buyRate'] = $quotePricingDetailsArys['szInsuranceMinCurrency_buyRate'];
                                $ret_ary['fInsuranceMinExchangeRate_buyRate'] = $quotePricingDetailsArys['fInsuranceMinExchangeRate_buyRate'];
                                $ret_ary['fInsuranceUptoPriceUSD_buyRate'] = $quotePricingDetailsArys['fInsuranceUptoPriceUSD_buyRate'];
                                $ret_ary['fInsuranceMinPriceUSD_buyRate'] = $quotePricingDetailsArys['fInsuranceMinPriceUSD_buyRate'];

                                $ret_ary['fTotalInsuranceCostForBooking_buyRate'] = $quotePricingDetailsArys['fTotalInsuranceCostForBooking_buyRate'];
                                $ret_ary['fTotalInsuranceCostForBookingCustomerCurrency_buyRate'] = $quotePricingDetailsArys['fTotalInsuranceCostForBookingCustomerCurrency_buyRate'];
                                $ret_ary['fTotalInsuranceCostForBookingUSD_buyRate'] = $quotePricingDetailsArys['fTotalInsuranceCostForBookingUSD_buyRate'];
                            } 
                            if($iManualFeeApplicable==1)
                            { 
                                cPartner::$szGlobalToken = $szReferenceToken; 
                                $kWHSSearch = new cWHSSearch();
                                $tempResultAry = array();
                                $searchResultAry = array();
                                $tempResultAry = $kWHSSearch->getSearchedDataFromTempData(false,false,false,true); 

                                $szSerializeData =  $tempResultAry['szSerializeData']; 
                                $searchResultAry = unserialize($szSerializeData); 

                                if(!empty($searchResultAry))
                                {
                                    foreach($searchResultAry as $searchResultArys)
                                    {
                                        if($searchResultArys['unique_id'] == $szServiceID )
                                        {
                                            $updateBookingAry = $searchResultArys ; 
                                            break;
                                        }
                                    }
                                }  
                                $ret_ary['idUniqueWTW'] = $szServiceID ;
                                if(!empty($updateBookingAry))
                                {
                                    $ret_ary['iBookingCutOffHours'] = $updateBookingAry['iBookingCutOffHours'];
                                    $ret_ary['dtCutOff']= $updateBookingAry['dtCutOffDate'];
                                    $ret_ary['dtAvailable'] = $updateBookingAry['dtAvailableDate'];
                                    $ret_ary['dtBookedAvailableDate'] = $updateBookingAry['dtAvailableDate'];
                                    $ret_ary['dtWhsCutOff'] = $updateBookingAry['dtWhsCutOff'];
                                    $ret_ary['dtWhsAvailabe'] = $updateBookingAry['dtWhsAvailable']; 
                                } 
                            }
                        }   

                        $kForwarder = new cForwarder();
                        $kForwarder->load($bookingQuoteArys['idForwarder']);

                        $ret_ary['szForwarderDispName'] = $kForwarder->szDisplayName;
                        $ret_ary['szForwarderRegistName'] = $kForwarder->szCompanyName;
                        $ret_ary['szForwarderAddress']=$kForwarder->szAddress;
                        $ret_ary['szForwarderAddress2']=$kForwarder->szAddress2;
                        $ret_ary['szForwarderAddress3']=$kForwarder->szAddress3;
                        $ret_ary['szForwarderPostCode']=$kForwarder->szPostCode;
                        $ret_ary['szForwarderCity']=$kForwarder->szCity;
                        $ret_ary['szForwarderState']=$kForwarder->szState;
                        $ret_ary['idForwarderCountry']=$kForwarder->idCountry;
                        $ret_ary['szForwarderLogo']=$kForwarder->szLogo; 
                        $ret_ary['idForwarderCurrency'] = $kForwarder->szCurrency ;
                        $ret_ary['iForwarderGPType'] = $kForwarder->iProfitType ;   
                        
                        $ret_ary['iBookingType'] = __BOOKING_TYPE_RFQ__ ; //Manual Request for quote (RFQ)
                        $ret_ary['iBookingQuotes '] = 1;

                        if(empty($szReferenceToken))
                        {
                            $szReferenceToken = cPartner::$szGlobalToken; 
                        }
                        if(!empty($szReferenceToken))
                        {
                            $ret_ary['szReferenceToken'] = $szReferenceToken ;   
                        }
                        $fApplicableVatRate = 0; 
                         

                        if((int)$postSearchAry['iBookingStep']<9)
                        {
                            $ret_ary['iBookingStep'] = 9;
                        }  
                        if($kForwarder->szCurrency==$quotePricingDetailsArys['idForwarderAccountCurrency'])
                        {
                            $ret_ary['szForwarderCurrency'] = $quotePricingDetailsArys['szForwarderAccountCurrency'];
                            $ret_ary['fForwarderExchangeRate'] = $quotePricingDetailsArys['fForwarderAccountCurrencyExchangeRate'];
                        }
                        else
                        {
                            if($kForwarder->szCurrency==1)
                            { 
                                $ret_ary['szForwarderCurrency'] = 'USD';
                                $ret_ary['fForwarderExchangeRate'] = 1;
                            }
                            else
                            {
                                $resultAry = $kWHSSearch->getCurrencyDetails($kForwarder->szCurrency);		 
                                $ret_ary['szForwarderCurrency'] = $resultAry['szCurrency'];
                                $ret_ary['fForwarderExchangeRate'] = $resultAry['fUsdValue'];
                            }
                        } 
                        if($iManualFeeApplicable==1)
                        { 
                            if($quotePricingDetailsArys['idHandlingCurrency']==$quotePricingDetailsArys['idForwarderCurrency'])
                            {
                                $fManualFeeForwarderCurrency = $quotePricingDetailsArys['fTotalHandlingFee'];
                            }
                            else if($quotePricingDetailsArys['idForwarderCurrency']==1)
                            {
                                $fManualFeeForwarderCurrency = $quotePricingDetailsArys['fTotalHandlingFeeUSD'];
                            }
                            else if($quotePricingDetailsArys['fForwarderCurrencyExchangeRate']>0)
                            {
                                $fManualFeeForwarderCurrency = round((float)($quotePricingDetailsArys['fTotalHandlingFeeUSD'] / $quotePricingDetailsArys['fForwarderCurrencyExchangeRate']),2);
                            }      
                            $quotePricingDetailsArys['fTotalPriceForwarderCurrency'] = $quotePricingDetailsArys['fTotalPriceForwarderCurrency'] + $fManualFeeForwarderCurrency ; 

                            if($kForwarder->szCurrency==$quotePricingDetailsArys['idForwarderAccountCurrency'])
                            {
                                $fManualFeeForwarderAccountCurrency = $fManualFeeForwarderCurrency;
                            }
                            else if($kForwarder->szCurrency==$quotePricingDetailsArys['idHandlingCurrency'])
                            {
                                $fManualFeeForwarderAccountCurrency = $quotePricingDetailsArys['fTotalHandlingFee'];
                            }
                            else if($ret_ary['fForwarderExchangeRate']>0)
                            {
                                $fManualFeeForwarderAccountCurrency = round((float)($quotePricingDetailsArys['fTotalHandlingFeeUSD'] / $ret_ary['fForwarderCurrencyExchangeRate']),2);
                            } 
                            $quotePricingDetailsArys['fTotalPriceForwarderAccountCurrency'] = $quotePricingDetailsArys['fTotalPriceForwarderAccountCurrency'] + $fManualFeeForwarderAccountCurrency;
                            $quotePricingDetailsArys['fTotalVatForwarderAccountCurrency'] = $quotePricingDetailsArys['fTotalPriceForwarderAccountCurrency'] * $fApplicableVatRate * 0.01; 
                        } 
                        else if($iAddHandlingFee==1)
                        { 
                            $fHandlingFeeForwarderCurrency = 0;
                            if($searchResults['idForwarderCurrency']==1) //USD
                            {
                                $fHandlingFeeForwarderCurrency = $quotePricingDetailsArys['fTotalHandlingFeeUSD'];
                            }
                            else if($quotePricingDetailsArys['fForwarderCurrencyExchangeRate']>0)
                            {
                                $fHandlingFeeForwarderCurrency = round((float)($quotePricingDetailsArys['fTotalHandlingFeeUSD']/$quotePricingDetailsArys['fForwarderCurrencyExchangeRate']),4);
                            }    
                            $quotePricingDetailsArys['fTotalPriceForwarderCurrency'] = $quotePricingDetailsArys['fTotalPriceForwarderCurrency'] + $fHandlingFeeForwarderCurrency ; 

                            if($searchResults['idForwarderCurrency']==1) //USD
                            {
                                $fHandlingFeeForwarderAccountCurrency = $quotePricingDetailsArys['fTotalHandlingFeeUSD'];
                            }
                            else if($ret_ary['fForwarderExchangeRate']>0)
                            {
                                $fHandlingFeeForwarderAccountCurrency = round((float)($quotePricingDetailsArys['fTotalHandlingFeeUSD']/$ret_ary['fForwarderExchangeRate']),4);
                            }  
                            $quotePricingDetailsArys['fTotalPriceForwarderAccountCurrency'] = $quotePricingDetailsArys['fTotalPriceForwarderAccountCurrency'] + $fHandlingFeeForwarderAccountCurrency;
                            $quotePricingDetailsArys['fTotalVatForwarderAccountCurrency'] = $quotePricingDetailsArys['fTotalPriceForwarderAccountCurrency'] * $fApplicableVatRate * 0.01 ;  
                        } 
                        /*
                        *  If forwarder Quote currency and Customer currency are same then we have rounded off both the values to make sure we always have matching values under Transaction History
                        */
                        if($quotePricingDetailsArys['idForwarderCurrency']==$postSearchAry['idCustomerCurrency'])
                        {
                            $quotePricingDetailsArys['fTotalPriceForwarderCurrency'] = round((float)$quotePricingDetailsArys['fTotalPriceForwarderCurrency']); 
                        } 
                        else
                        {
                            /*
                             * We always have customer currency value rounded to 0 decimal places
                             */
                            $quotePricingDetailsArys['fTotalPriceCustomerCurrency'] = round((float)$quotePricingDetailsArys['fTotalPriceCustomerCurrency']); 
                        }

                        $ret_ary['idQuotePricingDetails'] = $idQuotePricingDetails ;
                        $ret_ary['idFile'] = $idBookingFile ;
                        $ret_ary['idTransportMode'] = $bookingQuoteArys['idTransportMode'];
                        $ret_ary['szTransportMode'] = $bookingQuoteArys['szTransportMode'];
                        $ret_ary['idForwarder'] = $bookingQuoteArys['idForwarder'];  

                        if($ret_ary['idTransportMode']==__BOOKING_TRANSPORT_MODE_COURIER__) //Courier
                        { 
                            if($quotePricingDetailsArys['idCourierProviderProduct']>0)
                            {
                                $kCourierService = new cCourierServices();
                                $courierAgreementInputAry = array();
                                $courierAgreementInputAry['idForwarder'] = $bookingQuoteArys['idForwarder'];
                                $courierAgreementAry = $kCourierService->getCourierProductProviderPricingData($courierAgreementInputAry,false,$quotePricingDetailsArys['idCourierProviderProduct']);
                                $idCourierAgreement = $courierAgreementAry[0]['idCourierAgreement'];
                            }

                            $ret_ary['isManualCourierBooking'] = 1;
                            $ret_ary['dtCutOff'] = $postSearchAry['dtTimingDate']; 
                            $ret_ary['idServiceProvider'] = $idCourierAgreement;
                            $ret_ary['idServiceProviderProduct'] = $quotePricingDetailsArys['idCourierProviderProduct'];
                        }
                        else
                        {
                            $ret_ary['isManualCourierBooking'] = 0;
                        } 

                        $fTotalPriceIncludingVat = $quotePricingDetailsArys['fTotalPriceCustomerCurrency'] + $quotePricingDetailsArys['fTotalVatCustomerCurrency'];
                        $fTotalPriceIncludingVatUsd = ($fTotalPriceIncludingVat * $postSearchAry['fExchangeRate']) ;

                        /*
                        * If forwarder account currency and customer currency then there is no currency markup will applied
                        * @Ajay
                        */
                        if($kForwarder->szCurrency==$postSearchAry['idCurrency'])
                        {
                            $fCurrencyMarkupPrice = 0;
                            $fMarkupPercentage = 0;

                            $fTotalPriceNoMarkup = $quotePricingDetailsArys['fTotalPriceCustomerCurrency'];
                        }
                        else
                        {
                            $fPriceMarkUp = $kWHSSearch->getManageMentVariableByDescription('__MARK_UP_PRICING_PERCENTAGE__'); 

                            /*
                             * We are calculating no currency markup price from price with mark-up for e.g
                             * fTotalPriceCustomerCurrency = 400
                             * fMarkupRate = 2.5
                             * fPriceNoMarkup = (400 * 100 )/(100+2.5) = (40000/102.5) = 390.2439
                             * @Ajay
                             */
                            $fTotalPriceNoMarkup = ($quotePricingDetailsArys['fTotalPriceCustomerCurrency'] * 100 ) / (100+$fPriceMarkUp);
                            $fTotalVatNoMarkup = ($quotePricingDetailsArys['fTotalVatCustomerCurrency'] * 100 ) / (100+$fPriceMarkUp);

                            $fMarkupPercentage = $fPriceMarkUp ;
                            $fCurrencyMarkupPrice = ($quotePricingDetailsArys['fTotalPriceCustomerCurrency'] - $fTotalPriceNoMarkup) + ($quotePricingDetailsArys['fTotalVatCustomerCurrency'] - $fTotalVatNoMarkup) ;
                            $fCurrencyMarkupPriceUSD = ($fCurrencyMarkupPrice * $postSearchAry['fExchangeRate']) ;
                        } 

                        /*
                        * IF the forwarder account currency remains the same while quoting and booking then we are picking values from quote otherwise we will calculate forwarder prices in forwarder's account currency
                        * e.g if fwd account currency was USD when quote was created and remains same USD when this page got called then we calculate nothing and pick the values what we have in quote tables
                        * AND IF it changes to some other currency e.g DKK then we calculate these prices in DKK
                        * @Ajay
                        */
                        if($kForwarder->szCurrency==$quotePricingDetailsArys['idForwarderAccountCurrency'])
                        { 
                            $fTotalPriceForwarderAccountCurrency=$quotePricingDetailsArys['fTotalPriceForwarderAccountCurrency']+$quotePricingDetailsArys['fTotalVatForwarderAccountCurrency'];
                            $ret_ary['fTotalPriceForwarderCurrency'] = round((float)$fTotalPriceForwarderAccountCurrency,2);
                            $ret_ary['fTotalPriceNoMarkupForwarderCurrency'] = round((float)$fTotalPriceForwarderAccountCurrency,2);
                            $ret_ary['fTotalPriceNoMarkupForwarderCurrency_new'] = round((float)$fTotalPriceForwarderAccountCurrency,2);
                        }
                        else
                        {
                            if($kForwarder->szCurrency==$postSearchAry['idCurrency'])
                            {
                                $ret_ary['fTotalPriceForwarderCurrency'] = round((float)$fTotalPriceIncludingVat,2);
                                $ret_ary['fTotalPriceNoMarkupForwarderCurrency'] = round((float)$fTotalPriceIncludingVat,2);
                                $ret_ary['fTotalPriceNoMarkupForwarderCurrency_new'] = round((float)$fTotalPriceIncludingVat,2);
                            }
                            else
                            {  
                                if($ret_ary['fForwarderExchangeRate']>0)
                                {
                                    $ret_ary['fTotalPriceForwarderCurrency'] = ($fTotalPriceIncludingVatUsd/$ret_ary['fForwarderExchangeRate']);
                                    $ret_ary['fTotalPriceNoMarkupForwarderCurrency'] = ( ($fTotalPriceIncludingVatUsd-$fCurrencyMarkupPriceUSD)/$ret_ary['fForwarderExchangeRate']);
                                    $ret_ary['fTotalPriceNoMarkupForwarderCurrency_new'] = (($fTotalPriceIncludingVatUsd-$fCurrencyMarkupPriceUSD)/$ret_ary['fForwarderExchangeRate']);
                                }  
                            }
                        } 
                        $fDkkExchangeRate = 0;
                        if($postSearchAry['idCurrency']==20)
                        {
                            /*
                            * If customer currency is DKK(20) then we consider customer's currency exchange rate as fDkkExchangeRate
                            */
                            $fDkkExchangeRate = $postSearchAry['fExchangeRate'];
                        }
                        else
                        {
                            $dkkExchangeAry = array();
                            $dkkExchangeAry = $kWHSSearch->getCurrencyDetails(20);	 
                            if($dkkExchangeAry['fUsdValue']>0)
                            {
                                $fDkkExchangeRate = $dkkExchangeAry['fUsdValue'] ;						 
                            }
                        }

                        $fTotalPriceUSD = $postSearchAry['fExchangeRate'] * $quotePricingDetailsArys['fTotalPriceCustomerCurrency']; 
                        $fTotalPriceNoMarkupUSD = $fTotalPriceNoMarkup * $postSearchAry['fExchangeRate'] ;

                        $ret_ary['iIncludeComments'] = $quotePricingDetailsArys['iInsuranceComments'];
                        $ret_ary['szForwarderComment'] = $quotePricingDetailsArys['szForwarderComment']; 

                        if($quotePricingDetailsArys['iInsuranceComments']==1)
                        {
                            $ret_ary['szOtherComments'] = $quotePricingDetailsArys['szForwarderComment']; 
                        }  

                        $kConfig = new cConfig();
                        $transportModeListAry = array(); 
                        $transportModeListAry=$kConfig->getConfigurationLanguageData('__TABLE_TRANSPORT_MODE__',$iBookingLanguage);
                        
                        if($quotePricingDetailsArys['szHandoverCity']!='')
                        {
                            $ret_ary['szHandoverCity']=$quotePricingDetailsArys['szHandoverCity'];
                        }
                        $ret_ary['szFrequency'] =$transportModeListAry[$bookingQuoteArys['idTransportMode']]['szFrequency']; 

                        $ret_ary['idForwarderQuoteCurrency'] = $quotePricingDetailsArys['idForwarderCurrency'];
                        $ret_ary['szForwarderQuoteCurrency'] = $quotePricingDetailsArys['szForwarderCurrency'];
                        $ret_ary['fForwarderQuoteCurrencyExchangeRate'] = $quotePricingDetailsArys['fForwarderCurrencyExchangeRate']; 
                        $ret_ary['fForwarderTotalQuotePrice'] = $quotePricingDetailsArys['fTotalPriceForwarderCurrency']; 

                        $ret_ary['fPriceMarkupCustomerCurrency'] = $fCurrencyMarkupPrice;
                        $ret_ary['fMarkupPercentage'] = $fMarkupPercentage;
                        $ret_ary['fTotalPriceUSD'] = $fTotalPriceUSD ;
                        $ret_ary['fTotalPriceNoMarkupUSD'] = $fTotalPriceNoMarkupUSD ; 
                        $ret_ary['iTransitHours'] = $quotePricingDetailsArys['iTransitDays']; 
                        $ret_ary['fReferalPercentage'] = $quotePricingDetailsArys['fReferalPercentage'];
                        $ret_ary['iBookingLanguage'] = $iBookingLanguage; 
                       
                        //$ret_ary['fReferalAmount'] = round((float)(($ret_ary['fTotalPriceForwarderCurrency'] * $quotePricingDetailsArys['fReferalPercentage'])/100),2); 

                        /*
                        * Rounding the value when forwarder and customer currency are same. 
                        */
                        if($quotePricingDetailsArys['idForwarderCurrency']==$postSearchAry['idCustomerCurrency'])
                        {
                            $fTotalPriceForwarderCurrencyExcludingHandlingFee = round((float)$fTotalPriceForwarderCurrencyExcludingHandlingFee); 
                            $fTotalSelfInvoiceAmountQuoteCurrency = round((float)$fTotalSelfInvoiceAmountQuoteCurrency);
                        } 
                        if($ret_ary['iForwarderGPType']==__FORWARDER_PROFIT_TYPE_MARK_UP__)
                        {
                            if($postSearchAry['fExchangeRate']>0)
                            {
                                $ret_ary['fReferalAmountUSD'] = ($quotePricingDetailsArys['fReferalAmount'] * $postSearchAry['fExchangeRate']) ; 
                            }
                            if($ret_ary['fForwarderExchangeRate']>0)
                            {
                                $ret_ary['fReferalAmount'] = ($ret_ary['fReferalAmountUSD'] / $ret_ary['fForwarderExchangeRate']) ; 
                            }
                            //$ret_ary['fTotalVatForwarderCurrency'] = $quotePricingDetailsArys['fTotalVat'];
                            /*
                            * From Version 2 we always have Forwarder VAT as 0
                            */
                            $ret_ary['fTotalVatForwarderCurrency'] = 0;
                            $ret_ary['fTotalForwarderPriceWithoutHandlingFee'] = $fTotalSelfInvoiceAmountQuoteCurrency; //This field always have fTotalSelfInvoiceAmount + fReferalAmount
                            //Deducting Referal fee from Self invoiced amount
                            $fTotalSelfInvoiceAmountQuoteCurrency = ($fTotalSelfInvoiceAmountQuoteCurrency - $quotePricingDetailsArys['fReferalAmount']);

                            if($ret_ary['idForwarderCurrency']==$ret_ary['idForwarderQuoteCurrency'])
                            {
                                $ret_ary['fTotalSelfInvoiceAmount'] = $fTotalSelfInvoiceAmountQuoteCurrency;
                            }
                            else
                            {
                                $fTotalSelfInvoiceAmountUSD = $fTotalSelfInvoiceAmountQuoteCurrency * $ret_ary['fForwarderQuoteCurrencyExchangeRate'];  
                                if($ret_ary['fForwarderExchangeRate']>0)
                                { 
                                    $fTotalSelfInvoiceAmount = $fTotalSelfInvoiceAmountUSD / $ret_ary['fForwarderExchangeRate'];
                                    $ret_ary['fTotalForwarderPriceWithoutHandlingFee'] = $fTotalSelfInvoiceAmount; //This field always have fTotalSelfInvoiceAmount + fReferalAmount
                                    //deducting Referral Fee from self invoice amount
                                    $fTotalSelfInvoiceAmount = $fTotalSelfInvoiceAmount - $ret_ary['fReferalAmount'];
                                    $ret_ary['fTotalSelfInvoiceAmount'] = $fTotalSelfInvoiceAmount;
                                } 
                            }
                        }
                        else
                        {
                            /*
                            *  If forwarder Quote currency and Customer currency are same then then VAT should also be same in case of Referal Bookings
                            */
                            if($quotePricingDetailsArys['idForwarderCurrency']==$postSearchAry['idCustomerCurrency'])
                            {
                                $fTotalVatForwarderCurrency = $quotePricingDetailsArys['fTotalVatCustomerCurrency'];
                            }
                            else
                            {
                                $fTotalCustomerVatUsd = $quotePricingDetailsArys['fTotalVatCustomerCurrency']* $postSearchAry['fExchangeRate']; 
                                if($ret_ary['fForwarderQuoteCurrencyExchangeRate']>0)
                                {
                                    $fTotalVatForwarderCurrency = round((float)($fTotalCustomerVatUsd/$ret_ary['fForwarderQuoteCurrencyExchangeRate']),2);
                                    $fTotalVatForwarderCurrency = $fTotalVatForwarderCurrency/1.025; // removing currency mark-up
                                }
                            } 
                            /*
                            * From Version 2 we always have Forwarder VAT as 0
                            */
                            $fTotalVatForwarderCurrency = 0;
                            $ret_ary['fTotalVatForwarderCurrency'] = $fTotalVatForwarderCurrency;

                            $fManualFeeForwarderAccountCurrency = 0;
                            if($ret_ary['fForwarderCurrencyExchangeRate']>0)
                            {
                                $fManualFeeForwarderAccountCurrency = round((float)($quotePricingDetailsArys['fTotalHandlingFeeUSD'] / $ret_ary['fForwarderCurrencyExchangeRate']),2);
                            }    
                            /*
                            * Referal fee is calculated based on (Total Price + VAT - Handling Fee (if applicable))
                            */   
                            $fTotalVatForwarderCurrencyExcludingHandlingFee = round((float)($fTotalPriceForwarderCurrencyExcludingHandlingFee * $fApplicableVatRate * 0.01),2);  

                            $fTotalForwarderPriceWithoutHandlingFee = ($fTotalPriceForwarderCurrencyExcludingHandlingFee + $fTotalVatForwarderCurrencyExcludingHandlingFee);

                            $fReferalAmount = round((float)($fTotalSelfInvoiceAmountQuoteCurrency * $ret_ary['fReferalPercentage'] * 0.01),2); 
                            if($ret_ary['idForwarderCurrency']==$ret_ary['idForwarderQuoteCurrency'])
                            {
                                $ret_ary['fReferalAmount'] = $fReferalAmount;
                                $ret_ary['fReferalAmountUSD'] = ($fReferalAmount * $ret_ary['fForwarderQuoteCurrencyExchangeRate']) ;

                                //Deducting Referal fee from Self invoiced amount
                                $fTotalSelfInvoiceAmountQuoteCurrency = ($fTotalSelfInvoiceAmountQuoteCurrency - $fReferalAmount);
                                $ret_ary['fTotalSelfInvoiceAmount'] = $fTotalSelfInvoiceAmountQuoteCurrency;
                            }
                            else 
                            {
                                $ret_ary['fReferalAmountUSD'] = ($fReferalAmount * $ret_ary['fForwarderQuoteCurrencyExchangeRate']) ;

                                $fTotalForwarderPriceWithoutHandlingFeeUSD = $fTotalForwarderPriceWithoutHandlingFee * $ret_ary['fForwarderQuoteCurrencyExchangeRate'];
                                $fTotalSelfInvoiceAmountUSD = $fTotalSelfInvoiceAmountQuoteCurrency * $ret_ary['fForwarderQuoteCurrencyExchangeRate'];

                                if($ret_ary['fForwarderExchangeRate']>0)
                                {
                                    $ret_ary['fReferalAmount'] = ($ret_ary['fReferalAmountUSD'] / $ret_ary['fForwarderExchangeRate']) ;  
                                    $fTotalForwarderPriceWithoutHandlingFee = $fTotalForwarderPriceWithoutHandlingFeeUSD / $ret_ary['fForwarderExchangeRate'];
                                    $fTotalSelfInvoiceAmount = $fTotalSelfInvoiceAmountUSD / $ret_ary['fForwarderExchangeRate'];

                                    //deducting Referral Fee from self invoice amount
                                    $fTotalSelfInvoiceAmount = $fTotalSelfInvoiceAmount - $ret_ary['fReferalAmount'];
                                    $ret_ary['fTotalSelfInvoiceAmount'] = $fTotalSelfInvoiceAmount;
                                }
                            } 
                            $ret_ary['fTotalForwarderPriceWithoutHandlingFee'] = $fTotalForwarderPriceWithoutHandlingFee; 
                        } 
                        if((float)$quotePricingDetailsArys['fTotalVatCustomerCurrency']>0)
                        {
                            $kConfig_new1 = new cConfig();
                            $kConfig_new1->loadCountry($postSearchAry['szCustomerCountry']);
                            $szVATRegistration = $kConfig_new1->szVATRegistration;                
                            $ret_ary['szVATRegistration'] = $szVATRegistration;
                        }

                        $ret_ary['fTotalPriceCustomerCurrency'] = $quotePricingDetailsArys['fTotalPriceCustomerCurrency'];
                        $ret_ary['fTotalVat'] = $quotePricingDetailsArys['fTotalVatCustomerCurrency']; 
                        $ret_ary['dtQuoteValidTo'] = $quotePricingDetailsArys['dtQuoteValidTo'] ; 
                        $ret_ary['iHandlingFeeApplicable'] = $iHandlingFeeApplicable;
                        $ret_ary['fTotalHandlingFeeUSD'] = $quotePricingDetailsArys['fTotalHandlingFeeUSD']; 
                        $ret_ary['iFinancialVersion'] = __TRANSPORTECA_FINANCIAL_VERSION__;

                        if($quotePricingDetailsArys['fHandlingCurrencyExchangeRate']>0)
                        {
                            $ret_ary['fTotalHandlingFeeLocalCurrency'] = $quotePricingDetailsArys['fTotalHandlingFeeUSD']/$quotePricingDetailsArys['fHandlingCurrencyExchangeRate']; 
                            $ret_ary['idHandlingFeeLocalCurrency'] = $quotePricingDetailsArys['idHandlingCurrency']; 
                            $ret_ary['szHandlingFeeLocalCurrency'] = $quotePricingDetailsArys['szHandlingCurrency']; 
                            $ret_ary['szHandlingFeeLocalCurrencyExchangeRate']=$quotePricingDetailsArys['fHandlingCurrencyExchangeRate'];
                        }
                        if($postSearchAry['fExchangeRate']>0)
                        {
                            $ret_ary['fTotalHandlingFeeCustomerCurrency'] = round((float)($quotePricingDetailsArys['fTotalHandlingFeeUSD'] / $postSearchAry['fExchangeRate']),2);
                        }
                        if($ret_ary['fForwarderQuoteCurrencyExchangeRate']>0)
                        {
                            $ret_ary['fTotalHandlingFeeQuoteCurrency'] = round((float)($quotePricingDetailsArys['fTotalHandlingFeeUSD'] / $ret_ary['fForwarderQuoteCurrencyExchangeRate']),2);
                        }
                        if($ret_ary['fForwarderExchangeRate']>0)
                        {
                            $ret_ary['fTotalHandlingFeeForwarderCurrency'] = round((float)($quotePricingDetailsArys['fTotalHandlingFeeUSD'] / $ret_ary['fForwarderExchangeRate']),2);
                        }     
                        $ret_ary['fVATPercentage'] = $quotePricingDetailsArys['fVATPercentage']; 
                        $ret_ary['fDkkExchangeRate'] = $fDkkExchangeRate;
                        
                        if($quotePricingDetailsArys['iRemovePriceGuarantee']==1)
                        {
                            $ret_ary['iRemovePriceGuarantee'] = 0;
                        }
                        else
                        {
                            $ret_ary['iRemovePriceGuarantee'] = 1;
                        } 
                        if(!empty($ret_ary))
                        {
                            $update_query = '';
                            foreach($ret_ary as $key=>$value)
                            {
                                $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                            }
                        }   
                        $update_query = rtrim($update_query,","); 
                        if($kBooking->updateDraftBooking($update_query,$idBooking))
                        {   
                            $postSearchAry = array();
                            $postSearchAry = $kBooking->getBookingDetails($idBooking);
                            $isManualCourierBooking = $postSearchAry['isManualCourierBooking'];
                            $idCustomer = $postSearchAry['idUser'];

                            if($bRecalculateInsuranceFlag)
                            {
                                if($postSearchAry['iInsuranceIncluded']==1 || $postSearchAry['iInsuranceChoice']==3) //1. Yes 3. Optional
                                {  
                                    $insuranceInputAry = array();
                                    $insurancePriceAry = array();
                                    $insuranceInputAry['iInsurance'] = 1;
                                    $insuranceInputAry['idInsuranceCurrency'] = $postSearchAry['idGoodsInsuranceCurrency'];
                                    $insuranceInputAry['fValueOfGoods'] = $postSearchAry['fValueOfGoods'];

                                    /*
                                    * Adding Insurance prices to the booking
                                    */
                                    $kBooking->updateInsuranceDetails($insuranceInputAry,$postSearchAry);

                                    if($postSearchAry['iInsuranceChoice']==3)
                                    {
                                        $insuranceInputAry = array();
                                        $insurancePriceAry = array();
                                        $insuranceInputAry['iInsurance'] = 0; 

                                        /*
                                         * This looks bit funny but as per process we are calling same function again to achive following point
                                         * If insurance choice is added as 'Optional' then we by deault mark Insurance option on /pay/ screen as 'No' so do this i am removing Insurance flag
                                        */
                                        $kBooking->updateInsuranceDetails($insuranceInputAry,$postSearchAry,'REMOVE_INSURANCE_ONLY'); 
                                    }
                                }
                            }

                            $szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
                            $kConfig = new cConfig();
                            $kConfig->loadCountry($postSearchAry['idOriginCountry']);
                            $szCountryStrUrl = $kConfig->szCountryISO ;
                            $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
                            $szCountryStrUrl .= $kConfig->szCountryISO ;

                            if($iHandlingFeeApplicable==1)
                            {
                                $addHandlingFeeDetailsAry = array();
                                $addHandlingFeeDetailsAry['idBooking'] = $idBooking;
                                $addHandlingFeeDetailsAry['idForwarder'] = $bookingQuoteArys['idForwarder'];
                                if($iManualFeeApplicable==1)
                                {
                                    $addHandlingFeeDetailsAry['iProductType'] = __AUTOMATIC_MANUAL_FEE__; 
                                }
                                else
                                {
                                    $addHandlingFeeDetailsAry['iProductType'] = $quotePricingDetailsArys['iProductType']; 
                                } 
                                $addHandlingFeeDetailsAry['idCourierProvider'] = $quotePricingDetailsArys['idCourierProvider'];
                                $addHandlingFeeDetailsAry['idCourierProviderProduct'] = $quotePricingDetailsArys['idCourierProviderProduct'];
                                $addHandlingFeeDetailsAry['idHandlingCurrency'] = $quotePricingDetailsArys['idHandlingCurrency'];
                                $addHandlingFeeDetailsAry['szHandlingCurrency'] = $quotePricingDetailsArys['szHandlingCurrency'];
                                $addHandlingFeeDetailsAry['fHandlingCurrencyExchangeRate'] = $quotePricingDetailsArys['fHandlingCurrencyExchangeRate'];
                                $addHandlingFeeDetailsAry['fHandlingFeePerBooking'] = $quotePricingDetailsArys['fHandlingFeePerBooking'];
                                $addHandlingFeeDetailsAry['fHandlingFeePerBookingLocalCurrency'] = $quotePricingDetailsArys['fHandlingFeePerBooking']/$quotePricingDetailsArys['fHandlingCurrencyExchangeRate'];
                                $addHandlingFeeDetailsAry['fHandlingMarkupPercentage'] = $quotePricingDetailsArys['fHandlingMarkupPercentage']; 
                                $addHandlingFeeDetailsAry['idHandlingMinMarkupCurrency'] = $quotePricingDetailsArys['idHandlingMinMarkupCurrency'];
                                $addHandlingFeeDetailsAry['szHandlingMinMarkupCurrency'] = $quotePricingDetailsArys['szHandlingMinMarkupCurrency'];
                                $addHandlingFeeDetailsAry['fHandlingMinMarkupCurrencyExchangeRate'] = $quotePricingDetailsArys['fHandlingMinMarkupCurrencyExchangeRate'];
                                $addHandlingFeeDetailsAry['fHandlingMinMarkupPrice'] = $quotePricingDetailsArys['fHandlingMinMarkupPrice']; 
                                $addHandlingFeeDetailsAry['fHandlingMinMarkupPriceLocalCurrency'] = $quotePricingDetailsArys['fHandlingMinMarkupPrice']/$quotePricingDetailsArys['fHandlingMinMarkupCurrencyExchangeRate'];

                                $kBooking->updateBookingsHandlingFeeDetails($addHandlingFeeDetailsAry,$idBooking);
                            }

                            /*
                            * IF $iQuickQuote==__QUICK_QUOTE_SEND_QUOTE__ then we already have added courier service data data like as per automatic courier booking
                             * ELSE for $isManualCourierBooking==1 we add a dummy line in table tblcourierbooking
                            */
                            if($isManualCourierBooking==1 && $iQuickQuote!=__QUICK_QUOTE_SEND_QUOTE__)
                            {
                                $kCourierService = new cCourierServices();
                                $iLabelCreatedByForwarder = $kCourierService->checkAllCourierProductPricing($postSearchAry); 
                                if($iLabelCreatedByForwarder)
                                {
                                    $iBookingIncluded = 1;
                                }
                                else
                                {
                                    $iBookingIncluded = 0;
                                }
                                $updateBookingAry = array();
                                $updateBookingAry['szCurrencyCode'] = $postSearchAry['szCurrency'];
                                $updateBookingAry['iBookingIncluded'] = $iBookingIncluded;
                                $kBooking->updateCourierServiceDetails($updateBookingAry,$idBooking); 	
                            }  
                            
                            if($iBookingLanguage>0 && $idCustomer>0)
                            {
                                /*
                                * Update customer language to be same as booking language
                                */
                                 
                                $updateUserDetailsAry = array();
                                $updateUserDetailsAry['iLanguage'] = $iBookingLanguage ; 

                                $update_user_query = '';
                                foreach($updateUserDetailsAry as $key=>$value)
                                {
                                    $update_user_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                                } 
                                $kRegisterShipCon = new cRegisterShipCon();
                                $update_user_query = rtrim($update_user_query,",");  
                                $kRegisterShipCon->updateUserDetailsByQuotes($update_user_query,$idCustomer);
                            }
                            
                            if($iQuickQuote==__QUICK_QUOTE_SEND_QUOTE__)
                            {  
                                $this->iSuccess = 1;
                                $this->szRedirectUrl = __NEW_SERVICES_PAGE_URL__."/".$szBookingRandomNum."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;;
                                return true;
                            }
                            else
                            { 
                                $this->iSuccess = 1;
                                $this->szRedirectUrl = __BOOKING_QUOTE_SERVICE_PAGE_URL__."/".$szBookingRandomNum."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
                                return true;
                            } 
                        } 
                    }
                    break;
                }
            } 
        }
    }
    
    
    function updatePartialBookingDetails($bookingAry,$idBooking)
    {
        if(!empty($bookingAry) && $idBooking>0)
        {
            $update_query = ''; 
            if(!empty($bookingAry))
            {
                foreach($bookingAry as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            }
            $update_query = rtrim($update_query,","); 
            if($kBooking->updateDraftBooking($update_query,$idBooking))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    
    function deleteSearchDoneByTransportecaUser($szReferenceToken)
    {
        $query="
            DELETE FROM
                ".__DBC_SCHEMATA_COMPARE_BOOKING__."
            WHERE
                szReferenceToken='".  mysql_escape_custom($szReferenceToken)."'
           "
        ;        
        $result = $this->exeSQL( $query );
    }
    function set_idCountry( $value ,$flag=true)
    {
        $this->idCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idCountry", t($this->t_base_courier.'fields/currency'), false, false, $flag );
    }
    function set_idTrade( $value ,$flag=true) 
    {
        $this->idTrade = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idTrade", t($this->t_base_courier.'fields/currency'), false, false, $flag );
    }
    function set_idProduct( $value ,$flag=true)
    {
        $this->idProduct = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idTrade", t($this->t_base_courier.'fields/currency'), false, false, $flag );
    }
    function set_fPriceIncrease( $value ,$flag=true)
    {
        $this->fPriceIncrease = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fPriceIncrease", t($this->t_base_courier.'fields/currency'), false, false, $flag );
    }
    function set_iTransitTimeIncrease( $value ,$flag=true)
    {
        $this->iTransitTimeIncrease = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iTransitTimeIncrease", t($this->t_base_courier.'fields/currency'), false, false, $flag );
    }
    function set_idDummyService( $value ,$flag=false)
    {
        $this->idDummyService = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idDummyService", t($this->t_base_courier.'fields/currency'), false, false, $flag );
    }  
    function set_idOriginCountry( $value ,$flag=true)
    {
        $this->idOriginCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idOriginCountry", t($this->t_base_courier.'fields/currency'), false, false, $flag );
    }
    function set_idDestinationCountry( $value ,$flag=true)
    {
        $this->idDestinationCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idDestinationCountry", t($this->t_base_courier.'fields/currency'), false, false, $flag );
    } 
    function set_idDummyServiceException( $value ,$flag=false)
    {
        $this->idDummyServiceException = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idDummyServiceException", t($this->t_base_courier.'fields/currency'), false, false, $flag );
    } 
    function set_idForwarder( $value ,$flag=true)
    {
        $this->idForwarder = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idForwarder", t($this->t_base_courier.'fields/forwarder'), false, false, $flag );
    }
    function set_idFromWarehouse( $value ,$flag=true)
    {
        $this->idFromWarehouse = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idFromWarehouse", t($this->t_base_courier.'fields/from_warehouse'), false, false, $flag );
    }
    function set_idToWarehouse( $value ,$flag=true)
    {
        $this->idToWarehouse = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idToWarehouse", t($this->t_base_courier.'fields/to_warehouse'), false, false, $flag );
    } 
    function set_idRailTransport( $value ,$flag=false)
    {
        $this->idRailTransport = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idRailTransport", t($this->t_base_courier.'fields/rail_transport'), false, false, $flag );
    } 
} 

?>