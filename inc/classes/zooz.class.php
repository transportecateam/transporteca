<?php
/**
 * This file is the container for all customer's zooz payment related functionality.
 * All functionality related to user payment should be contained in this class.
 *
 * zooz.class.php
 *
 * @copyright Copyright (C) 2012 Transport-ece
 * @author Ashish
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

class cZooz extends cDatabase
{
	var $isSandbox = true;
	function __construct()
	{
            parent::__construct();
            return true;
	}
	
	
	public function add($paypalPaymentLog)
	{
            if(!empty($paypalPaymentLog) && !empty($paypalPaymentLog['szBookingRef']))
            {  
                if($paypalPaymentLog['iPaymentMode']<=0)
                {
                    $paypalPaymentLog['iPaymentMode'] = 1; //default zooz gateway
                }
                $query="
                    INSERT INTO
                        ".__DBC_SCHEMATA_PAYMENT_ZOOZ_LOG__."
                    (
                        idBooking, 
                        szToken,
                        szSessionToken,
                        szBookingRef,
                        szStatus,
                        szDeveloperNotes,
                        iPaymentMode, 
                        szSource,
                        dtPaymentInitiated
                    )
                    VALUES
                    (
                        '".(int)$paypalPaymentLog['idBooking']."', 
                        '".mysql_escape_custom($paypalPaymentLog['szToken'])."',
                        '".mysql_escape_custom($paypalPaymentLog['szSessionToken'])."',
                        '".mysql_escape_custom($paypalPaymentLog['szBookingRef'])."',
                        '".mysql_escape_custom($paypalPaymentLog['szStatus'])."',
                        '".mysql_escape_custom($paypalPaymentLog['szDeveloperNotes'])."', 
                        '".mysql_escape_custom($paypalPaymentLog['iPaymentMode'])."', 
                        '".mysql_escape_custom($paypalPaymentLog['szSource'])."', 
                        NOW()
                    )
                ";
                $result = $this->exeSQL($query);
                return true;
            }
	}
        
        public function updateTransactionDetails($paypalPaymentLog)
	{
            if(!empty($paypalPaymentLog))
            { 
                if($paypalPaymentLog['szStatus']=='PAYMENT_COMPLETED')
                {
                    $iVerifyStatus = 1;
                }
                else
                {
                    $iVerifyStatus = 1;
                }
                
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_PAYMENT_ZOOZ_LOG__."
                    SET
                        idTransaction = '".mysql_escape_custom($paypalPaymentLog['idTransaction'])."',
                        idTransactionDisplay = '".mysql_escape_custom($paypalPaymentLog['idTransactionDisplay'])."',
                        szStatus = '".mysql_escape_custom($paypalPaymentLog['szStatus'])."',
                        szDeveloperNotes = '".mysql_escape_custom($paypalPaymentLog['szDeveloperNotes'])."',
                        iVerifyStatus = '".(int)$iVerifyStatus."',
                        dtPayment = now()
                    WHERE
                        szSessionToken = '".mysql_escape_custom($paypalPaymentLog['szSessionToken'])."'
                    OR
                        szToken = '".mysql_escape_custom($paypalPaymentLog['szSessionToken'])."'
                ";
                //echo $query;
                if($result = $this->exeSQL($query))
                {
                    return true;
                }
                else
                {
                    return false;
                } 
            } 
	}
	
	function updateBookingZoozPaymentDetail($idTransaction,$status,$bStripeFlag=false,$currentPaidAmount=false)
	{
            if($status=='Completed')
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_PAYMENT_ZOOZ_LOG__."
                    SET
                        iVerifyStatus='1'
                    WHERE
                        idTransaction='".mysql_escape_custom($idTransaction)."'
                ";
                $result = $this->exeSQL($query);
            }
            
            if($bStripeFlag)
            {
                $currentPaidAmount = $currentPaidAmount;
            }
            else
            {
                $currentPaidAmount=$this->checkZoozTranscationDetail($idTransaction);
            }

            if((int)$_SESSION['booking_id']>0)
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_BOOKING__."
                    SET
                        idTransaction='".mysql_escape_custom($idTransaction)."',
                        szPaymentStatus='".mysql_escape_custom($status)."',
                        fCustomerPaidAmount='".mysql_escape_custom((float)$currentPaidAmount)."'
                     WHERE
                        id='".(int)$_SESSION['booking_id']."'
                ";
                $result = $this->exeSQL($query);
            }
            return true;
	}
	
	
	function checkZoozTranscationDetail($idTransaction)
	{
            //echo $this->isSandbox;
            if ($this->isSandbox == __URL_FLAG__) {
                    $url = 'https://sandbox.zooz.co/mobile/ExtendedServerAPI';
            } else {
                    $url = 'https://app.zooz.com/mobile/ExtendedServerAPI';
            }

            $parmas="cmd=getTransactionDetails&ver=1.0.0&transactionID=".$idTransaction;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('ZooZDeveloperId:'.__API_DEVELOPER_ID__,'ZooZServerAPIKey:'.__API_DEVELOPER_KEY__));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            // Timeout in seconds
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt ($ch, CURLOPT_POSTFIELDS, $parmas);

            ob_start();

            curl_exec($ch);

            //$result = ob_get_contents();
            $response=json_decode(ob_get_contents());

            ob_clean();
            return $response->ResponseObject->paidAmount;
	}
        
        function getPaymentDetailsByTransactionId($idTransaction)
        {
            $query="
               SELECT
                  idBooking 
               FROM
                   ".__DBC_SCHEMATA_PAYMENT_ZOOZ_LOG__." 
               WHERE
                   idTransaction = '".mysql_escape_custom($idTransaction)."'	 
                AND
                   iPaymentmode = '2'
                ";
            if($result = $this->exeSQL($query))
            {
                $retAry = array();
                $ctr = 0;
                while($row = $this->getAssoc($result))
                {
                    $retAry[$ctr] = $row;
                    $ctr++;
                }
                return $retAry;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
        
        function getPaymentDetailsByBookingId($Booking_Id)
	{
		$query="
			SELECT 
                            b.id,
                            b.fTotalAmount,
                            b.szCurrency,
                            f.iInsuranceIncluded,
                            f.fTotalInsuranceCostForBookingCustomerCurrency,
                            f.szBookingRef,
                            f.idBookingStatus,
                            f.szBookingRandomNum
			FROM
                            ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS b
			INNER JOIN
                            ".__DBC_SCHEMATA_BOOKING__." AS f	
			ON
                            (f.id=b.idBooking)	
			WHERE 
                            b.iStatus = '0'
			AND
                            f.idBookingStatus IN ('3','4') 
                        AND
                            b.idBooking='".(int)($Booking_Id)."'
		";
		//echo $query;
		if($result = $this->exeSQL( $query ))
		{
                    if ($this->getRowCnt() > 0)
                    {	
                        $bookingdetails=array();
                        while($row=$this->getAssoc($result))
                        {	
                            $bookingdetails[]=$row;
                        }
                        return $bookingdetails;
                    }
                    else
                    {
                        return array();
                    }	
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
        
        
        public function addPartnerApiStripLog($paypalPaymentLog)
	{
            if(!empty($paypalPaymentLog) && !empty($paypalPaymentLog['szBookingRef']))
            {  
                if($paypalPaymentLog['iPaymentMode']<=0)
                {
                    $paypalPaymentLog['iPaymentMode'] = 1; //default zooz gateway
                }
                $query="
                    INSERT INTO
                        ".__DBC_SCHEMATA_PAYMENT_ZOOZ_LOG__."
                    (
                        idBooking, 
                        szToken,
                        szSessionToken,
                        szBookingRef,
                        szStatus,
                        szDeveloperNotes,
                        iPaymentMode, 
                        szSource,
                        dtPaymentInitiated,
                        idTransaction,
                        iVerifyStatus,
                        dtPayment
                    )
                    VALUES
                    (
                        '".(int)$paypalPaymentLog['idBooking']."', 
                        '".mysql_escape_custom($paypalPaymentLog['szToken'])."',
                        '".mysql_escape_custom($paypalPaymentLog['szSessionToken'])."',
                        '".mysql_escape_custom($paypalPaymentLog['szBookingRef'])."',
                        '".mysql_escape_custom($paypalPaymentLog['szStatus'])."',
                        '".mysql_escape_custom($paypalPaymentLog['szDeveloperNotes'])."', 
                        '".mysql_escape_custom($paypalPaymentLog['iPaymentMode'])."', 
                        '".mysql_escape_custom($paypalPaymentLog['szSource'])."', 
                        NOW(),
                        '".mysql_escape_custom($paypalPaymentLog['szPaymentToken'])."',
                        '1',
                        NOW()
                    )
                ";
                $result = $this->exeSQL($query);
                return true;
            }
	}
        
}
?>