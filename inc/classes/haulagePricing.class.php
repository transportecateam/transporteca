<?php
/**
 * This file is the container for all user related functionality.
 * All functionality related to user details should be contained in this class.
 *
 * booking.class.php
 *
 * @copyright Copyright (C) 2012 Transport-ece
 * @author Ajay
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

class cHaulagePricing extends cDatabase
{
	var $t_base="HaulaugePricing/";
	var $iPricing; 
	var $idForwarder;
	function __construct()
	{
		parent::__construct();
		return true;
	}
		
	function isLocationExistsInZone($data,$zoneVertexAry=false,$zoneLatLongAry=false)
	{
            if(!empty($data))
            {
                $szLatitude = $data['szLatitude'];
                $szLongitude = $data['szLongitude'];
                $idHaulagePricingModel = $data['idHaulagePricingModel'];

                $szCalculationLogs = '';
                $szCalculationLogs .= "\n\n Asked lat ".$szLatitude ;
                $szCalculationLogs .= "\n Asked long ".$szLongitude ;
                $szCalculationLogs .= "\n model id ".$idHaulagePricingModel ;
                $iDebugFlag = 1;  
                if(empty($zoneVertexAry))
                {
                    $zoneVertexAry = $this->getAllVerticesOfZone($idHaulagePricingModel);
                }

                if(!empty($zoneLatLongAry))
                {
                    $this->szMaxLatitude = $zoneLatLongAry['szMaxLatitude'];
                    $this->szMinLatitude = $zoneLatLongAry['szMinLatitude'];
                    $this->szMaxLongitude = $zoneLatLongAry['szMaxLongitude'];
                    $this->szMinLongitude = $zoneLatLongAry['szMinLongitude'] ;
                }

                if(!empty($zoneVertexAry))
                {
                    //do calculation
                    $szCalculationLogs .= "\n\mHaulage Vertices \n\n";
                    $szCalculationLogs .=  print_r($zoneVertexAry,true);

                    $szCalculationLogs .= "\n Max latitude ".$this->szMaxLatitude ;				
                    $szCalculationLogs .= "\n min latitude ".$this->szMinLatitude ;

                    $szCalculationLogs .= "\n max longitude  ".$this->szMaxLongitude ;
                    $szCalculationLogs .= "\n min longitude ".$this->szMinLongitude ;
                    if(($this->szMaxLatitude < $szLatitude) || ($this->szMinLatitude > $szLatitude ))
                    {
                        $szCalculationLogs .= "\n Test1 failed. Not exists in latitude range";
                        $this->szLocationFindingLogs = $szCalculationLogs;
                        if($iDebugFlag==1)
                        {
                            $filename = __APP_PATH_ROOT__."/logs/debug_haulage_".date('Ymd').".log";
                            $strdata=array();
                            $strdata[0]=" \n\n *** Function :: isLocationExistsInZone() \n\n";
                            $strdata[1] = $szCalculationLogs."\n\n"; 
                            file_log(true, $strdata, $filename);
                        }
                        return false;
                    }
                    else if(($this->szMaxLongitude < $szLongitude) || ($this->szMinLongitude > $szLongitude ))
                    {
                        $szCalculationLogs .= "<br> Test1 succeed <br />";
                        $szCalculationLogs .= "<br>Test2 failed. Not exists in longitude range";
                        $this->szLocationFindingLogs = $szCalculationLogs;
                        if($iDebugFlag==1)
                        {
                            $filename = __APP_PATH_ROOT__."/logs/debug_haulage_".date('Ymd').".log";
                            $strdata=array();
                            $strdata[0]=" \n\n *** Function :: isLocationExistsInZone() \n\n";
                            $strdata[1] = $szCalculationLogs."\n\n"; 
                            file_log(true, $strdata, $filename);
                        }
                        return false;
                    }
                    else
                    {
                        $szCalculationLogs .= "\n Test1 succeed <br />";
                        $szCalculationLogs .= "\n Test2 succeed <br />";
                        $szCalculationLogs .= "\n Performing next level testing <br><br> ";

                        $verticesDifferenceAry[0]['Alpha'] = 0;
                        $verticesDifferenceAry[0]['Beta'] = $szLongitude;
                        $verticesDifferenceAry[0]['CrossX'] = '';
                        $verticesDifferenceAry[0]['MinX'] = $szLatitude;
                        $verticesDifferenceAry[0]['MaxX'] = $this->szMaxLatitude;
                        $j=1;
                        for($i=0;$i<(count($zoneVertexAry)-1);$i++)
                        {
                            if($zoneVertexAry[$j]['szLatitude']!=0 && $zoneVertexAry[$i]['szLatitude']!=0 && $zoneVertexAry[$j]['szLatitude']!=$zoneVertexAry[$i]['szLatitude'])
                            {
                                $verticesDifferenceAry[$j]['Alpha'] = (($zoneVertexAry[$j]['szLongitude'] - $zoneVertexAry[$i]['szLongitude']) / ($zoneVertexAry[$j]['szLatitude'] - $zoneVertexAry[$i]['szLatitude']));
                            }
                            else
                            {
                                $verticesDifferenceAry[$j]['Alpha'] = 0;
                            }
                            $verticesDifferenceAry[$j]['Beta'] = $zoneVertexAry[$i]['szLongitude'] - ($verticesDifferenceAry[$j]['Alpha'] * $zoneVertexAry[$i]['szLatitude']) ;

                            if($verticesDifferenceAry[$j]['Alpha']!=0)
                            {
                                $verticesDifferenceAry[$j]['CrossX'] = ($szLongitude - $verticesDifferenceAry[$j]['Beta']) / $verticesDifferenceAry[$j]['Alpha'] ;
                            }
                            else
                            {
                                $verticesDifferenceAry[$j]['CrossX'] = 0;
                            }

                            if($zoneVertexAry[$i]['szLatitude'] < $zoneVertexAry[$j]['szLatitude'])
                            {
                                $verticesDifferenceAry[$j]['MinX'] = $zoneVertexAry[$i]['szLatitude'] ;
                            }
                            else
                            {
                                $verticesDifferenceAry[$j]['MinX'] = $zoneVertexAry[$j]['szLatitude'];
                            }	

                            if($zoneVertexAry[$i]['szLatitude'] > $zoneVertexAry[$j]['szLatitude'])
                            {
                                $verticesDifferenceAry[$j]['MaxX'] = $zoneVertexAry[$i]['szLatitude'] ;
                            }
                            else
                            {
                                $verticesDifferenceAry[$j]['MaxX'] = $zoneVertexAry[$j]['szLatitude'];
                            }	

                            if($verticesDifferenceAry[$j]['MinX'] > $szLatitude)
                            {
                                $maximumMinX = $verticesDifferenceAry[$j]['MinX'] ;
                            }
                            else
                            {
                                $maximumMinX = $szLatitude ;
                            }

                            if($verticesDifferenceAry[$j]['MaxX'] > $this->szMaxLatitude)
                            {
                                $minimumManX = $this->szMaxLatitude ;
                            }
                            else
                            {
                                $minimumManX = $verticesDifferenceAry[$j]['MaxX'];
                            }

                            if(($verticesDifferenceAry[$j]['CrossX'] > $maximumMinX) && ($verticesDifferenceAry[$j]['CrossX'] < $minimumManX ))
                            {
                                $succesfully_found_counter ++;
                            }
                            $j++;
                        }
                        
                        $szCalculationLogs .= "\n\n Vertices Difference Ary \n\n ";
                        $szCalculationLogs .= print_r($verticesDifferenceAry,true);
                        $szCalculationLogs .= "\n\n # of successful intercept ".(int)$succesfully_found_counter;
                        if($succesfully_found_counter%2 == 1)
                        {
                            $szCalculationLogs .= "\n\n Location exists in this zone";
                            $bReturnFlag = true;
                        }
                        else
                        {	
                            $szCalculationLogs .= "\n\n Location does not exists in this zone";
                            $bReturnFlag = false; 
                        }					
                    }  
                    $this->szLocationFindingLogs = $szCalculationLogs;
                    if($iDebugFlag==1)
                    {
                        $filename = __APP_PATH_ROOT__."/logs/debug_haulage_".date('Ymd').".log";
                        $strdata=array();
                        $strdata[0]=" \n\n *** Function :: isLocationExistsInZone() \n\n";
                        $strdata[1] = $szCalculationLogs."\n\n"; 
                        file_log(true, $strdata, $filename);
                    }
                    return $bReturnFlag;
                }
                else
                {
                    return false;
                }
            }
	}
	
	function getAllVerticesOfZone($idHaulagePricingModel,$edit_polygon=false)
	{
		if($idHaulagePricingModel>0)
		{
			$query="
				SELECT
					szLatitude,
					szLongitude
				FROM
					".__DBC_SCHEMATA_HAULAGE_ZONE__."
				WHERE
					idHaulagePricingModel='".(int)$idHaulagePricingModel."'
				ORDER BY
					iVertexSequence ASC
			";
			//echo "<br>".$query."<br>";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$zoneVertexAry = array();
					$ctr=0;
					$szMaxLatitude = 0.0;
					$szMinLatitude = 0.0;
					
					$szMaxLongitude = 0.0;
					$szMinLongitude = 0.0;
					
					while($row=$this->getAssoc($result))
					{
						if($ctr==0)
						{
							$tempVertexAry[]=$row;				

							/*
								Initializing max and min latitude variables with first row data.
							*/
							$szMaxLatitude = $row['szLatitude'];
							$szMinLatitude = $row['szLatitude'];	
								
							/*
								Initializing max and min longitude variables with first row data.
							*/
							
							$szMaxLongitude =  $row['szLongitude'];
							$szMinLongitude =  $row['szLongitude'];
						}
						
						/*
							fetching maximum Latitude 
						*/
						if($szMaxLatitude < $row['szLatitude'])
						{
							$this->szMaxLatitude = $row['szLatitude'];
							$szMaxLatitude = $row['szLatitude'];
						}
						else
						{
							$this->szMaxLatitude = $szMaxLatitude;
						}
						
						/*
							fetching minimum Latitude 
						*/
						if($szMinLatitude > $row['szLatitude'])
						{
							$this->szMinLatitude = $row['szLatitude'];
							$szMinLatitude = $row['szLatitude'];
						}
						else
						{
							$this->szMinLatitude = $szMinLatitude;
						}
						
						/*
							fetching maximum Longitude 
						*/
						if($szMaxLongitude < $row['szLongitude'])
						{
							$this->szMaxLongitude = $row['szLongitude'];
							$szMaxLongitude = $row['szLongitude'];
						}
						else
						{
							$this->szMaxLongitude = $szMaxLongitude;
						}
						
						/*
							fetching minimum Longitude 
						*/
						if($szMinLongitude > $row['szLongitude'])
						{
							$this->szMinLongitude = $row['szLongitude'];
							$szMinLongitude = $row['szLongitude'];
						}
						else
						{
							$this->szMinLongitude = $szMinLongitude;
						}
						
						$zoneVertexAry[$ctr]=$row;
						$ctr++;
					}
					
					if($edit_polygon)
					{
						return $zoneVertexAry;
					}
					else
					{
						/**
						* adding first row data to the last of array to build a complete loop circle.
						*/
						if(!empty($zoneVertexAry) && !empty($tempVertexAry))
						{
							$zoneVertexAry = array_merge($zoneVertexAry,$tempVertexAry);
						}
						return $zoneVertexAry;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function getAllActiveHaulageModelsByCFS($idWarehouse,$iDirection)
	{
		if($idWarehouse>0)
		{
			$query="
				SELECT
					models.id,
					models.szName,
					models.idCountry,
					models.iRadiousKM,
					models.iDistanceUpToKm,
					models.idHaulageTransitTime as iHaulageTransitTime,
					models.fWmFactor,
					models.fFuelPercentage,
					map.idHaulageModel,
					tt.iHours as iTransitHours,
					models.iPriority as iModelPriority,
					map.iPriority
				FROM
					".__DBC_SCHEMATA_HAULAGE_MODELS_CFS_MAPPING__." map
				LEFT JOIN
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." models
				ON
					map.idHaulageModel = models.idHaulageModel
				INNER JOIN
					".__DBC_SCHEMATA_HAULAGE_TRANSIT_TIME__." tt
				ON
					tt.id = models.idHaulageTransitTime
				WHERE
					map.idWarehouse = '".(int)$idWarehouse."'	
				AND
					models.idWarehouse = '".(int)$idWarehouse."'
				AND
					models.iDirection = '".(int)$iDirection."'
				AND
					map.iDirection = '".(int)$iDirection."'
				AND
					map.iActive = '1'	
				AND
					models.iActive = '1'	
				ORDER BY
					map.iPriority ASC,models.iPriority ASC			
			";
			//echo "<br><br>".$query."<br><br>";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$haulageModelsAry = array();
					$ctr=0;					
					while($row=$this->getAssoc($result))
					{
						$haulageModelsAry[$ctr] = $row;
						$ctr++;
					}
					$res_ary = array();
					if(!empty($haulageModelsAry))
					{
						//$res_ary = sortArray($haulageModelsAry,'iPriority');
						$res_ary = $haulageModelsAry ;
					}
					return $res_ary ;
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}		
		}
	}
	
	function getAllActiveHaulageModelsByCFS_review($idWarehouse,$iDirection,$idHaulageModel)
	{
		if($idWarehouse>0)
		{
			$query="
				SELECT
					models.id,
					haulage_mod.szName,
					models.idCountry,
					haulage_mod.iRadiousKM,
					haulage_mod.iDistanceUpToKm,
					haulage_mod.idHaulageTransitTime as iHaulageTransitTime,
					haulage_mod.fWmFactor,
					haulage_mod.fFuelPercentage,
					tt.iHours as iTransitHours,
					haulage_mod.iPriority as iModelPriority,
					models.idHaulgePricingModel,
					models.idHaulageModel
				FROM
					".__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_MODEl_APPROVAL_DATA__." models
				INNER JOIN
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." haulage_mod
				ON
					haulage_mod.id = models.idHaulgePricingModel
				INNER JOIN
					".__DBC_SCHEMATA_HAULAGE_TRANSIT_TIME__." tt
				ON
					tt.id = haulage_mod.idHaulageTransitTime
				WHERE
					models.idWarehouse = '".(int)$idWarehouse."'
				AND
					models.iDirection = '".(int)$iDirection."'
				AND
					models.idHaulageModel = '".(int)$idHaulageModel."'
				AND
					models.iActive = '1'	
				ORDER BY
					haulage_mod.iPriority ASC			
			";
			//echo "<br><br>".$query."<br><br>";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$haulageModelsAry = array();
					$ctr=0;					
					while($row=$this->getAssoc($result))
					{
						$haulageModelsAry[$ctr] = $row;
						$ctr++;
					}
					$res_ary = array();
					if(!empty($haulageModelsAry))
					{
						//$res_ary = sortArray($haulageModelsAry,'iPriority');
						$res_ary = $haulageModelsAry ;
					}
					return $res_ary ;
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}		
		}
	}
	
	function calculateHaulageZonePricing($data,$mode=false)
	{
            if(!empty($data))
            {
                //converting cbm to kg
                if((float)$data['fWmFactor']>0)
                {
                    $fTotalWeightCBM = ($data['fWmFactor'] * $data['fVolume']);
                }

                if($fTotalWeightCBM > $data['fTotalWeight'])
                {
                    $fTotalWeight = $fTotalWeightCBM ;
                }
                else
                {
                    $fTotalWeight = $data['fTotalWeight'] ;
                } 
                
                //rounding weight to nearest 100 
                if($fTotalWeight>0)
                {
                    $fTotalWeight_temp = ceil((float)($fTotalWeight/100));
                    $fTotalCalculationWeight = round((float)(($fTotalWeight_temp))) * 100 ;		
                }

                if($mode=='REVIEW')
                {
                    $table_name_1 = __DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_APPROVAL_DATA__ ;
                    $table_name_2 = __DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_MODEl_APPROVAL_DATA__ ;
                }
                else
                {
                    $table_name_1 = __DBC_SCHEMATA_HAULAGE_PRICING__ ;
                    $table_name_2 = __DBC_SCHEMATA_HAULAGE_PRICING_MODELS__ ;
                }
			
                $query="
                    SELECT
                        hp.id,
                        hp.iUpToKg,
                        hp.fPricePer100Kg,
                        hp.fMinimumPrice,
                        hp.fPricePerBooking,
                        hp.idCurrency,
                        hp.iActive,
                        cur.szCurrency
                    FROM
                        ".$table_name_1." hp
                    INNER JOIN
                            ".$table_name_2." model
                    INNER JOIN
                            ".__DBC_SCHEMATA_CURRENCY__." cur
                    ON
                            cur.id = hp.idCurrency
                    WHERE
                            hp.iUpToKg >= '".(int)$fTotalWeight."'
                    AND
                            hp.idHaulagePricingModel = '".(int)$data['idHaulagePricingModel']."'
                    AND
                            hp.iActive='1'
                    AND
                            model.iActive = '1'
                    ORDER BY
                            hp.iUpToKg ASC
                    LIMIT
                            0,1
                ";
                //echo "<br><br>".$query."<br><br>";
                $filename = __APP_PATH_ROOT__."/logs/debug_priing_".date('Y-m-d').".log";

                $strdata=array();
                $strdata[0]=" \n\n Postcode Query: \n".$query."\n".print_R($data,true);
                file_log(true, $strdata, $filename);

                if($result=$this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        $haulagePricingAry = array();
                        $ctr=0;		
                        $kWHSSearch = new cWHSSearch();			
                        while($row=$this->getAssoc($result))
                        {
                            if($row['idCurrency']==1)// USD
                            {
                                $totalPrice = ($fTotalCalculationWeight * $row['fPricePer100Kg'])/100 ;							
                                if($totalPrice < $row['fMinimumPrice'])
                                {
                                    $totalPrice = $row['fMinimumPrice'] ;
                                }							
                                $totalPrice	= $totalPrice + $row['fPricePerBooking'];
                                $fUSDValue = 1.0000;
                            }
                            else
                            {
                                /**
                                * getting USD factor
                                */  
                                $fUSDValue = $kWHSSearch->getCurrencyFactor($row['idCurrency']);

                                /**
                                * converting price to USD
                                */

                                $totalPrice = ($fTotalCalculationWeight * $row['fPricePer100Kg'])/100 ;
                                if($totalPrice < $row['fMinimumPrice'])
                                {
                                        $totalPrice = $row['fMinimumPrice'] ;
                                }	
                                if($fUSDValue>0)
                                {
                                    $totalPrice	= ($totalPrice + $row['fPricePerBooking'] ) * $fUSDValue ;
                                }
                                else
                                {
                                    $totalPrice = 0.00 ;
                                }
                            }

                            $haulagePricingAry = $row;
                            $szCurrency = $row['szCurrency'] ;
                            $idCurrency = $row['idCurrency'];
                            $idHaulagePricing = $row['id'];
                            $ctr++;
                        }					
                        $totalPrice = round((float)$totalPrice,6);
                        $fuel_percentage = 0;
                        if($data['fFuelPercentage']>0)
                        {
                                $fuel_percentage = round((float)(($totalPrice * $data['fFuelPercentage'])/100),6);
                        }

                        //echo "<br /> Chargable weight : ".$fTotalWeight;
                        //echo "<br /> Fuel surcharge: ".$szCurrency." ".$fuel_percentage;
                        //echo "<br /> Haulage exchange rate : ".round((float)(1/$fUSDValue),6);	

                        $haulagePricingAry['fTotalHaulgePrice'] = $totalPrice + $fuel_percentage;
                        $haulagePricingAry['iHaulageTransitTime'] = $data['iHaulageTransitTime'] ;
                        $haulagePricingAry['fExchangeRate'] = round((float)(1/$fUSDValue),6);		
                        $haulagePricingAry['szHaulageCurrency'] = $szCurrency ;	
                        $haulagePricingAry['idCurrency'] = $idCurrency ;
                        $haulagePricingAry['fFuelSurcharge'] = $fuel_percentage ;	
                        $haulagePricingAry['idHaulagePricing'] = $idHaulagePricing ;
                        $haulagePricingAry['fChargableWeight'] = $fTotalCalculationWeight ;
                        $haulagePricingAry['fFuelPercentage'] = $data['fFuelPercentage'] ;
                                
                        return $haulagePricingAry ;
                    }
                    else
                    {
                        return array();
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }	
            }
	}
	
	function isLocationExistsByPostcode($data)
	{
		if(!empty($data))
		{
			$query="
                            SELECT
                                szPostCode
                            FROM
                                ".__DBC_SCHEMATA_HAULAGE_POSTCODE__."
                            WHERE
                                idHaulagePricingModel = '".(int)$data['idHaulagePricingModel']."'
			";
			//echo "<br><br>".$query."<br><br>";
			if($result=$this->exeSQL($query))
			{
                            if($this->iNumRows>0)
                            {
                                $postcodesAry = array();
                                $ctr=0;
                                while($row=$this->getAssoc($result))
                                {
                                    $postcodesAry[$ctr] = $row;
                                    $ctr++;
                                } 
					if(!empty($postcodesAry))
					{
						foreach($postcodesAry as $postcodesArys)
						{
							if(!empty($postcodesArys['szPostCode']))
							{
                                                            $length = strlen($postcodesArys['szPostCode']);
                                                            $postcodeFoundIndexs_astr = $this->count_occurences($postcodesArys['szPostCode'],'*');
                                                            $postcodeFoundIndexs_qmark = $this->count_occurences($postcodesArys['szPostCode'],'?');
                                                            $postcodeFoundIndexs_dash = $this->count_occurences($postcodesArys['szPostCode'],'-');

                                                            $DebugFlag = false;
                                                            if($postcodesArys['szPostCode']=='740?')
                                                            {
                                                               // $DebugFlag = true;
                                                            }
                                                            
                                                            if($DebugFlag)
                                                            {
                                                                echo "<br /> postcode Pattern".$postcodesArys['szPostCode'] ;
                                                                echo "<br /> Searched postcode ".$data['szPostcode'];
                                                                echo "<br /> position of * is: ".$postcodeFoundIndexs_astr;
                                                                echo "<br /> position of ? is: ".$postcodeFoundIndexs_qmark;								
                                                                echo "<br /> string length: ".$length ;
                                                            } 

                                                            if($data['szPostcode'] == $postcodesArys['szPostCode'])
                                                            {
                                                                if($DebugFlag)
                                                                {
                                                                    echo " Postcode Founds. Break!";
                                                                }
                                                                $success_flag = true;
                                                                break;
                                                            }
                                                            else if(!empty($postcodeFoundIndexs_qmark) && !empty($postcodeFoundIndexs_astr))
                                                            {
                                                                    // if first character is * then
                                                                    $iposition_astr = $postcodeFoundIndexs_astr ;
                                                                    $success_astr_flag = false ; 
                                                                    if($iposition_astr == ($length))
                                                                    {
                                                                        //echo "B4: ".$postcodesArys['szPostCode'];
                                                                        $szPostCode = substr($postcodesArys['szPostCode'],0,($length-1)); 
                                                                       // echo "Code: ".$szPostCode;
                                                                        $index_ary = explode(',',$postcodeFoundIndexs_qmark);
                                                                        $szPostCodeAry = str_split($szPostCode);
                                                                        $szHaulagePostCodeAry = str_split($data['szPostcode']);

                                                                        $string_len = count($szPostCodeAry);
                                                                        for($i=0;$i<$string_len;$i++)
                                                                        {
                                                                            $j = $i+1;
                                                                            if(!in_array($j,$index_ary))
                                                                            {
                                                                                $post_code_str .= $szPostCodeAry[$i];
                                                                                $haulage_post_code_str .= $szHaulagePostCodeAry[$i];
                                                                            } 
                                                                        } 								
                                                                        if($post_code_str == $haulage_post_code_str)
                                                                        {
                                                                           // echo " <br /> Record found with pattern ".$postcodesArys['szPostCode'];
                                                                            $success_flag = true;
                                                                            break;
                                                                        }
                                                                    }
                                                                    else if($iposition_astr==1)
                                                                    {
                                                                        $szPostCode = substr($postcodesArys['szPostCode'],1,$length);
                                                                        $szHaulagePostCode = substr($data['szPostcode'],-($length-1));

                                                                        if($szPostCode == $szHaulagePostCode)
                                                                        {
                                                                            $success_flag = true;
                                                                            //echo " <br /> Record found with pattern ".$postcodesArys['szPostCode'];
                                                                            break;
                                                                        }
                                                                    }
								}
								else if(!empty($postcodeFoundIndexs_dash) && !empty($postcodeFoundIndexs_astr))
								{
									$postcode_ary = explode("-",$postcodesArys['szPostCode']);	

									// if first character is * then
									$iposition_astr_1 = $this->count_occurences($postcode_ary[0],'*');
									$iposition_astr_2 = $this->count_occurences($postcode_ary[1],'*');
									
									$length_1 = strlen($postcode_ary[0]);
									$length_2 = strlen($postcode_ary[1]);
									
									if(($iposition_astr_1 == $length_1) && ($iposition_astr_2 == $length_2))
									{										
                                                                            $lower_limit = substr($postcode_ary[0],0,($length_1-1));
                                                                            $upper_limit = substr($postcode_ary[1],0,($length_2-1));

                                                                            if($lower_limit > $upper_limit)
                                                                            {
                                                                                $starting_point = $upper_limit ;
                                                                                $end_point = $lower_limit ;
                                                                            }
                                                                            else
                                                                            {
                                                                                $starting_point = $lower_limit ;
                                                                                $end_point = $upper_limit ;
                                                                            }
										
										for($i=$starting_point;$i<=$end_point;$i++)
										{
											$strlenght = strlen($i);
											$haulage_post_code_str = substr($data['szPostcode'],0,$strlenght);
																						
											if($haulage_post_code_str==$i)
											{
												//echo " <br /> Record found with pattern ".$postcodesArys['szPostCode'];
												$success_flag = true;
												break;
											}										
										}
									}
									else if(($iposition_astr_1 == 1) && ($iposition_astr_2 == 1))
									{
										$lower_limit = substr($postcode_ary[0],1,($length_1-1));
										$upper_limit = substr($postcode_ary[1],1,($length_2-1));
																				
										if($lower_limit > $upper_limit)
										{
											$starting_point = $upper_limit ;
											$end_point = $lower_limit ;
										}
										else
										{
											$starting_point = $lower_limit ;
											$end_point = $upper_limit ;
										}
										
										for($i=$starting_point;$i<=$end_point;$i++)
										{
											$strlenght = strlen($i);
											$haulage_post_code_str = substr($data['szPostcode'],-($strlenght));
																			
											if($haulage_post_code_str==$i)
											{
												//echo " <br /> Record found with pattern ".$postcodesArys['szPostCode'];
												$success_flag = true;
												break;
											}										
										}
									}
								}
								else if(!empty($postcodeFoundIndexs_qmark))
								{
                                                                    // if first character is * then
                                                                    $index_ary = explode(',',$postcodeFoundIndexs_qmark);
                                                                    $szPostCodeAry = str_split($postcodesArys['szPostCode']);
                                                                    $szHaulagePostCodeAry = str_split($data['szPostcode']);

                                                                    $string_len = count($szPostCodeAry);
                                                                    $haulage_string_len = count($szHaulagePostCodeAry);

                                                                    if($DebugFlag)
                                                                    {
                                                                        echo "<br> Length: ".$string_len;
                                                                        echo "<br> Text:".$postcodeFoundIndexs_qmark;
                                                                        echo "<br>";
                                                                        print_R($index_ary);
                                                                        echo "<br>Post Ary <br>";
                                                                        print_R($szPostCodeAry);
                                                                    }
                                                                    for($i=0;$i<$string_len;$i++)
                                                                    {
                                                                        $j = $i+1;
                                                                        if(!in_array($j,$index_ary))
                                                                        {
                                                                            $post_code_str .= $szPostCodeAry[$i];
                                                                            $haulage_post_code_str .= $szHaulagePostCodeAry[$i];
                                                                        } 
                                                                    }			
                                                                    if($DebugFlag)
                                                                    {
                                                                        echo "<br> Hp: ".$haulage_post_code_str." PC: ".$post_code_str." hp Str: ".$haulage_string_len." sl: ".$string_len;
                                                                    }
                                                                    if(!empty($index_ary) && in_array($string_len,$index_ary))
                                                                    {	
                                                                        if(($post_code_str == $haulage_post_code_str) && ($string_len==$haulage_string_len))
                                                                        {
                                                                            if($DebugFlag)
                                                                            {
                                                                                echo " <br /> 1 Record found with pattern ".$postcodesArys['szPostCode'];
                                                                            } 
                                                                            $success_flag = true;
                                                                            break;
                                                                        }				
                                                                    }
                                                                    else
                                                                    {
                                                                        if($post_code_str == $haulage_post_code_str)
                                                                        {
                                                                            if($DebugFlag)
                                                                            {
                                                                                echo " <br /> 2 Record found with pattern ".$postcodesArys['szPostCode'];
                                                                            } 
                                                                            //echo " <br /> Record found with pattern ".$postcodesArys['szPostCode'];
                                                                            $success_flag = true;
                                                                            break;
                                                                        }
                                                                    }
								}
								else if(!empty($postcodeFoundIndexs_astr))
								{ 
                                                                    // if first character is * then
                                                                    $iposition_astr = $postcodeFoundIndexs_astr ;
                                                                    $success_astr_flag = false ;
                                                                    $szLogString = "Searched Postcode: ".$data['szPostcode']." Position *: ".$iposition_astr." Length: ".$length." Pattern: ".$postcodesArys['szPostCode'];
                                                                    
                                                                    $filename = __APP_PATH_ROOT__."/logs/debug.log";
					
                                                                    $strdata=array();
                                                                    $strdata[0]=" \n\n Postcode Pattern Search: \n".$szLogString."\n";
                                                                    file_log(true, $strdata, $filename);
                                                                    
                                                                    if($iposition_astr == ($length))
                                                                    {
                                                                        $szPostCode = substr($postcodesArys['szPostCode'],0,($length-1));
                                                                        $szHaulagePostCode = substr($data['szPostcode'],0,($length-1));

                                                                        if($szPostCode == $szHaulagePostCode)
                                                                        {
                                                                            //echo " <br /> Record found with pattern ".$postcodesArys['szPostCode'];
                                                                            $success_flag = true;
                                                                            break;
                                                                        }
                                                                    }
                                                                    else if($iposition_astr==1)
                                                                    {
                                                                        $szPostCode = substr($postcodesArys['szPostCode'],1,$length);
                                                                        $szHaulagePostCode = substr($data['szPostcode'],-($length-1));

                                                                        if($szPostCode == $szHaulagePostCode)
                                                                        {
                                                                            //echo " <br /> Record found with pattern ".$postcodesArys['szPostCode'];
                                                                            $success_flag = true;
                                                                            break;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        $postcode_ary = explode("*",$postcodesArys['szPostCode']);

                                                                        $length_1 = strlen($postcode_ary[0]);
                                                                        $length_2 = strlen($postcode_ary[1]);

                                                                        // pattern of poscode
                                                                        $pattern_pc = $postcode_ary[0]."".$postcode_ary[1] ;

                                                                        $haulage_postcode_ary[0] = substr($data['szPostcode'],0,($length_1)); 
                                                                        $haulage_postcode_ary[1] = substr($data['szPostcode'],-($length_1));

                                                                        // patter haulage postcode
                                                                        $pattern_hpc = $haulage_postcode_ary[0]."".$haulage_postcode_ary[1] ;

                                                                        if($pattern_pc == $pattern_hpc)
                                                                        {
                                                                            //echo " <br /> Record found with pattern ".$postcodesArys['szPostCode'];
                                                                            $success_flag = true;
                                                                            break;
                                                                        }
                                                                    }
								}
								else if(!empty($postcodeFoundIndexs_dash))
								{
                                                                    $postcode_ary = explode("-",$postcodesArys['szPostCode']);

                                                                    if($postcode_ary[0]>$postcode_ary[1])
                                                                    {
                                                                            $lower_limit = $postcode_ary[1];
                                                                            $upper_limit = $postcode_ary[0];
                                                                    }
                                                                    else
                                                                    {
                                                                            $lower_limit = $postcode_ary[0];
                                                                            $upper_limit = $postcode_ary[1];
                                                                    }									
                                                                    for($i=$lower_limit;$i<=$upper_limit;$i++)
                                                                    {
                                                                            $strlenght = strlen($i);
                                                                            $haulage_post_code_str = substr($data['szPostcode'],0,$strlenght);

                                                                            if($haulage_post_code_str==$i)
                                                                            {
                                                                                    //echo " <br /> Record found with pattern ".$postcodesArys['szPostCode'];
                                                                                    $success_flag = true;
                                                                                    break;
                                                                            }										
                                                                    }
								}
							}
						} 
                                                if($DebugFlag)
                                                {
                                                    echo "<br> Final Flag: ".$success_flag;
                                                }
						return $success_flag;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}	
		}
	}
	
	function count_occurences($char_string, $haystack)
	{	
	    $characters = str_split($char_string);
	    $character_count = 1;
	   
	    foreach($characters as $character)
	    {
	        //$character_count = $character_count + substr_count($haystack, $character);
	        if($character == $haystack)
	        {
	        	if(!empty($index_str))
	        	{
	        		$index_str .= ','.$character_count ;
	        	}
	        	else
	        	{
	        		$index_str = $character_count ;
	        	}
	        }
	        $character_count++;
	    }
	    return $index_str;
	}
	
	function isLocationExistsByDistance($data)
	{
		if(!empty($data))
		{
			$query="
				SELECT
					model.id,
					model.iDistanceUpToKm,
					model.fWmFactor,
					model.fFuelPercentage
				FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." model
				INNER JOIN
					".__DBC_SCHEMATA_HAULAGE_COUNTRY__." hd
				ON
					hd.idHaulagePricingModel = model.id
				WHERE
					model.iDistanceUpToKm >= '".(int)$data['iDistance']."'
				AND
					model.id = '".(int)$data['idHaulagePricingModel']."'
				AND
					hd.idCountry = '".(int)$data['idCountry']."'
				ORDER BY
					iDistanceUpToKm ASC
				LIMIT
					0,1
			";
			//echo "<br><br>".$query."<br><br>";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row = $this->getAssoc($result);
					//echo "<br /> Record found with distance up to: ".$row['iDistanceUpToKm']." km, fWmFactor :".$row['fWmFactor']." and fuel percentage ".$row['fFuelPercentage']." id: ".$row['id'];
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}	
		}
	}
	
	function getDistanceFromWarehouse($distanceMeasureAry)
	{
		if(!empty($distanceMeasureAry))
		{
			$query = "
				SELECT 
					(( 3959 * acos( cos( radians(".(float)$distanceMeasureAry['szLatitude'].") ) * cos( radians( model.szLatitude ) ) 
					   * cos( radians(model.szLongitude) - radians(".(float)$distanceMeasureAry['szLongitude'].")) + sin(radians(".(float)$distanceMeasureAry['szLatitude'].")) 
					   * sin( radians(model.szLatitude)))) * 1.609344) as iDistance
				FROM
					".__DBC_SCHEMATA_WAREHOUSES__." model
				WHERE
					id = '".(int)$distanceMeasureAry['idWarehouse']."'
			" ;
			
			//echo "<br />".$query." <br />";
			if($result = $this->exeSQL($query))
			{
				$row = $this->getAssoc($result);
				return round((float)$row['iDistance'],2);
			}
		}
	}
	function getDistance($distanceMeasureAry,$mode=false)
	{
		$query = "
			SELECT 
				(( 3959 * acos( cos( radians(".(float)$distanceMeasureAry['szLatitude'].") ) * cos( radians( model.szLatitude ) ) 
				   * cos( radians(model.szLongitude) - radians(".(float)$distanceMeasureAry['szLongitude'].")) + sin(radians(".(float)$distanceMeasureAry['szLatitude'].")) 
				   * sin( radians(model.szLatitude)))) * 1.609344) as iDistance
			FROM
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." model
			WHERE
				id = '".(int)$distanceMeasureAry['idHaulagePricingModel']."'
		" ;
		//echo "<br />".$query." <br />";
		if($result = $this->exeSQL($query))
		{
			$row = $this->getAssoc($result);
			return round((float)$row['iDistance'],2);
		}
	}
		
	function isLocationExistsByCity($data,$mode=false)
	{
		if(!empty($data))
		{
			if($mode=='REVIEW')
			{
				$table_name = __DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_MODEl_APPROVAL_DATA__ ;
			}
			else
			{
				$table_name = __DBC_SCHEMATA_HAULAGE_PRICING_MODELS__ ;
			}
			$iDistance = $this->getDistance($data);
			//echo "<br /> Distance between door to city is: ".$iDistance." km<br />";
			$query="
				SELECT
                                    id
				FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." model
				WHERE
                                    model.iRadiousKM >= '".(float)$iDistance."'
				AND
                                    id = '".(int)$data['idHaulagePricingModel']."'
                                AND
                                    idCountry='".(int)$data['idCountry']."'
			";
			//echo "<br><br>".$query."<br><br>";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}	
		}
	} 
	function getHaulageDetails($haulageDAry)
	{
            $idWarehouse = $haulageDAry['idWarehouse'];
            $iDebugFlag = 1;
            //$idWarehouse = 1;
            $haulageModelsAry = array();
            $kHaulagePricing = new cHaulagePricing();
            $haulageModelsAry = $kHaulagePricing->getAllActiveHaulageModelsByCFS($idWarehouse,$haulageDAry['iDirection']);

            $iDirection = $haulageDAry['iDirection'];
            $idCountry = $haulageDAry['idCountry'];
            
            $filename = __APP_PATH_ROOT__."/logs/debug_haulage_er_1".date('Ymd').".log";
                                
            if(!empty($haulageModelsAry))
            {
                foreach($haulageModelsAry as $haulageModelsArys)
                {
                    //zone pricing
                    if($haulageModelsArys['idHaulageModel']==1)
                    { 
                        $data=array();
                        $data['szLatitude'] = $haulageDAry['szLatitude'] ;
                        $data['szLongitude'] = $haulageDAry['szLongitude'] ;
                        $data['idHaulagePricingModel'] = $haulageModelsArys['id'];
                        if($iDebugFlag==1)
                        {
                            $strdata=array();
                            $strdata[0]=" \n\n *** Zone sdds Search \n\n";
                            $strdata[1] = print_R($data,true)."\n\n"; 
                            file_log(true, $strdata, $filename);
                        }
                        
                        if($kHaulagePricing->isLocationExistsInZone($data))
                        {
                            $haulageDataAry=array();
                            $haulageDataAry['idHaulagePricingModel'] = $haulageModelsArys['id'];
                            $haulageDataAry['fTotalWeight'] = $haulageDAry['fTotalWeight'] ;
                            $haulageDataAry['fVolume'] = $haulageDAry['fVolume'];
                            $haulageDataAry['iHaulageTransitTime'] =  $haulageModelsArys['iTransitHours'];
                            $haulageDataAry['fWmFactor'] =  $haulageModelsArys['fWmFactor'];
                            $haulageDataAry['fFuelPercentage'] =  $haulageModelsArys['fFuelPercentage'];

                            $haulagePricingAry = $kHaulagePricing->calculateHaulageZonePricing($haulageDataAry);

                            if($iDebugFlag==1)
                            {
                                $strdata=array();
                                $strdata[0]=" \n\n *** Location exists by Zone \n\n";
                                $strdata[1] = print_R($haulageDataAry,true)."\n\n"; 
                                file_log(true, $strdata, $filename);
                            }
                            
                            if(!empty($haulagePricingAry))
                            {
                                $haulagePricingAry['idHaulageModel'] = $haulageModelsArys['idHaulageModel'];
                                $haulagePricingAry['idHaulagePricingModel'] = $haulageModelsArys['id'];
                                $haulagePricingAry['fWmFactor'] =  $haulageModelsArys['fWmFactor'];
                                $haulagePricingAry['szModelName'] =  $haulageModelsArys['szName'];
                                $haulagePricingAry['iDistanceUptoKm'] =  $haulageModelsArys['iDistanceUpToKm'];
                                $haulagePricingAry['fFuelPercentage'] =  $haulageModelsArys['fFuelPercentage'];
                                
                                if($iDebugFlag==1)
                                {
                                    $strdata=array();
                                    $strdata[0]=" \n\n *** Haulage Result Found \n\n";
                                    $strdata[1] = print_R($haulagePricingAry,true)."\n\n"; 
                                    file_log(true, $strdata, $filename);
                                } 
                                return $haulagePricingAry ;
                            }					
                        }
                    }
                    //city search
                    if($haulageModelsArys['idHaulageModel']==2)
                    { 
                        $data=array();
                        $data['iDistance'] = $haulageDAry['iDistance'];
                        $data['szCity'] = $haulageDAry['szCity'];
                        $data['szLatitude'] = $haulageDAry['szLatitude'];
                        $data['szLongitude'] = $haulageDAry['szLongitude'];
                        $data['idCountry'] = $haulageDAry['idCountry'];

                        $data['szCityLatitude'] = $haulageDAry['szCityLatitude'];
                        $data['szCityLongitude'] = $haulageDAry['szCityLongitude'];

                        $data['idHaulagePricingModel'] = $haulageModelsArys['id'];
                        $data['iRadiousKM'] = $haulageModelsArys['iRadiousKM'];
                        
                        if($iDebugFlag==1)
                        {
                            $strdata=array();
                            $strdata[0]=" \n\n *** City Search \n\n";
                            $strdata[1] = print_R($data,true)."\n\n"; 
                            file_log(true, $strdata, $filename);
                        }

                        if($kHaulagePricing->isLocationExistsByCity($data))
                        {						
                            $haulageDataAry=array();						
                            $haulageDataAry['idHaulagePricingModel'] = $haulageModelsArys['id'];
                            $haulageDataAry['fTotalWeight'] = $haulageDAry['fTotalWeight'] ;
                            $haulageDataAry['fVolume'] = $haulageDAry['fVolume'];
                            $haulageDataAry['iHaulageTransitTime'] =  $haulageModelsArys['iTransitHours'];
                            $haulageDataAry['fWmFactor'] =  $haulageModelsArys['fWmFactor'];
                            $haulageDataAry['fFuelPercentage'] =  $haulageModelsArys['fFuelPercentage'];
                            $haulageDataAry['idHaulageModel'] = $haulageModelsArys['idHaulageModel'];

                            $haulagePricingAry = $kHaulagePricing->calculateHaulageZonePricing($haulageDataAry);
                            
                            if($iDebugFlag==1)
                            {
                                $strdata=array();
                                $strdata[0]=" \n\n *** Location exists by City \n\n";
                                $strdata[1] = print_R($haulageDataAry,true)."\n\n"; 
                                file_log(true, $strdata, $filename);
                            }

                            if(!empty($haulagePricingAry))
                            {
                                $haulagePricingAry['idHaulageModel'] = $haulageModelsArys['idHaulageModel'];
                                $haulagePricingAry['idHaulagePricingModel'] = $haulageModelsArys['id'];
                                $haulagePricingAry['fWmFactor'] =  $haulageModelsArys['fWmFactor'];
                                $haulagePricingAry['szModelName'] =  $haulageModelsArys['szName'];
                                $haulagePricingAry['iDistanceUptoKm'] =  $haulageModelsArys['iDistanceUpToKm'];
                                $haulagePricingAry['fFuelPercentage'] =  $haulageModelsArys['fFuelPercentage'];
                                
                                if($iDebugFlag==1)
                                {
                                    $strdata=array();
                                    $strdata[0]=" \n\n *** Haulage Result Found \n\n";
                                    $strdata[1] = print_R($haulagePricingAry,true)."\n\n"; 
                                    file_log(true, $strdata, $filename);
                                }
                                
                                return $haulagePricingAry ;
                            }
                        }
                    }
                    //Postcode search
                    if($haulageModelsArys['idHaulageModel']==3)
                    {
                        $filename = __APP_PATH_ROOT__."/logs/debug_haulage_er_2".date('Ymd').".log";
                        $iDebugFlag = 1;
                        if($iDebugFlag==1)
                        {
                            $strdata=array();
                            $strdata[0]=" \n\n *** Postcode Search \n\n";
                            $strdata[1] = print_R($data,true)."\n\n"; 
                            file_log(true, $strdata, $filename);
                        }
                        if($this->isHaulageServicedInCountrybyWhs($idWarehouse,$idCountry,$iDirection))
                        {
                            if($iDebugFlag==1)
                            {
                                $strdata=array();
                                $strdata[0]=" \n\n *** Postcode is not being served by this warehouse \n\n"; 
                                file_log(true, $strdata, $filename);
                            }
                            continue;
                        }
                        if($iDebugFlag==1)
                        {
                            $strdata=array();
                            $strdata[0]=" \n\n *** Searched Country: ".$idCountry.", Haulage country: ".$haulageModelsArys['idCountry']." \n\n"; 
                            file_log(true, $strdata, $filename);
                        }
                        if($idCountry!=$haulageModelsArys['idCountry'])
                        {
                            if($iDebugFlag==1)
                            {
                                $strdata=array();
                                $strdata[0]=" \n\n *** In continue Searched Country: ".$idCountry.", Haulage country: ".$haulageModelsArys['idCountry']." \n\n"; 
                                file_log(true, $strdata, $filename);
                            }
                            continue;
                        } 
                        $data=array();
                        $data['szPostcode'] = $haulageDAry['szPostcode'] ;
                        $data['idHaulagePricingModel'] = $haulageModelsArys['id'];

                        if($iDebugFlag==1)
                        {
                            $strdata=array();
                            $strdata[0]=" \n\n *** Postcode Search \n\n";
                            $strdata[1] = print_R($data,true)."\n\n"; 
                            file_log(true, $strdata, $filename);
                        } 
                        if($kHaulagePricing->isLocationExistsByPostcode($data))
                        { 
                            $haulageDataAry=array();
                            $haulageDataAry['idHaulagePricingModel'] = $haulageModelsArys['id'];
                            $haulageDataAry['fTotalWeight'] = $haulageDAry['fTotalWeight'] ;
                            $haulageDataAry['fVolume'] = $haulageDAry['fVolume'];
                            $haulageDataAry['iHaulageTransitTime'] =  $haulageModelsArys['iTransitHours'];
                            $haulageDataAry['fWmFactor'] =  $haulageModelsArys['fWmFactor'];
                            $haulageDataAry['fFuelPercentage'] =  $haulageModelsArys['fFuelPercentage'];
                            
                            if($iDebugFlag==1)
                            {
                                $strdata=array();
                                $strdata[0]=" \n\n *** Location exists by Postcode. Checking calculation with following data \n\n";
                                $strdata[1] = print_R($haulageDataAry,true)."\n\n"; 
                                file_log(true, $strdata, $filename);
                            }
                        
                            $haulagePricingAry = $kHaulagePricing->calculateHaulageZonePricing($haulageDataAry);
                                
                            if(!empty($haulagePricingAry))
                            {
                                $haulagePricingAry['idHaulageModel'] = $haulageModelsArys['idHaulageModel'];
                                $haulagePricingAry['idHaulagePricingModel'] = $haulageModelsArys['id'];
                                $haulagePricingAry['fWmFactor'] =  $haulageModelsArys['fWmFactor'];
                                $haulagePricingAry['szModelName'] =  $haulageModelsArys['szName'];
                                $haulagePricingAry['iDistanceUptoKm'] =  $haulageModelsArys['iDistanceUpToKm'];
                                $haulagePricingAry['fFuelPercentage'] =  $haulageModelsArys['fFuelPercentage'];
                                
                                if($iDebugFlag==1)
                                {
                                    $strdata=array();
                                    $strdata[0]=" \n\n *** Haulage Result \n\n";
                                    $strdata[1] = print_R($haulagePricingAry,true)."\n\n"; 
                                    file_log(true, $strdata, $filename);
                                } 
                                return $haulagePricingAry ;
                            }
                        } 
                    }
                    if($haulageModelsArys['idHaulageModel']==4)
                    {
                        $data=array();
                        $data['iDistance'] = $haulageDAry['iDistance'];
                        $data['idCountry'] = $haulageDAry['idCountry'];
                        $data['idHaulagePricingModel'] = $haulageModelsArys['id'];
                        
                        if($iDebugFlag==1)
                        {
                            $strdata=array();
                            $strdata[0]=" \n\n *** Distance Search \n\n";
                            $strdata[1] = print_R($data,true)."\n\n"; 
                            file_log(true, $strdata, $filename);
                        }

                        if($data['iDistance']<=0)
                        {
                            $distanceInputAry = array();
                            $distanceInputAry['idHaulagePricingModel'] = $haulageModelsArys['id'] ;
                            $distanceInputAry['szLatitude'] =  $haulageDAry['szLatitude'];
                            $distanceInputAry['szLongitude'] = $haulageDAry['szLongitude'];
                            $distanceInputAry['idWarehouse'] = $haulageDAry['idWarehouse'] ;

                            $data['iDistance'] = $this->getDistanceFromWarehouse($distanceInputAry);
                            
                            if($iDebugFlag==1)
                            {
                                $strdata=array();
                                $strdata[0]=" \n\n *** New Distance Search \n\n";
                                $strdata[1] = print_R($data,true)."\n\n"; 
                                file_log(true, $strdata, $filename);
                            }
                        } 
                        
                        if($kHaulagePricing->isLocationExistsByDistance($data))
                        {						
                            $haulageDataAry=array();
                            $haulageDataAry['idHaulagePricingModel'] = $haulageModelsArys['id'];
                            $haulageDataAry['fTotalWeight'] = $haulageDAry['fTotalWeight'] ;
                            $haulageDataAry['fVolume'] = $haulageDAry['fVolume'];
                            $haulageDataAry['iHaulageTransitTime'] =  $haulageModelsArys['iTransitHours'];
                            $haulageDataAry['fWmFactor'] =  $haulageModelsArys['fWmFactor'];
                            $haulageDataAry['fFuelPercentage'] =  $haulageModelsArys['fFuelPercentage'];
                            $haulagePricingAry = $kHaulagePricing->calculateHaulageZonePricing($haulageDataAry);
                            
                            if($iDebugFlag==1)
                            {   
                                $strdata=array();
                                $strdata[0]=" \n\n *** Location exists by Postcode \n\n";
                                $strdata[1] = print_R($haulageDataAry,true)."\n\n"; 
                                file_log(true, $strdata, $filename);
                            }
                            
                            if(!empty($haulagePricingAry))
                            {
                                $haulagePricingAry['idHaulageModel'] = $haulageModelsArys['idHaulageModel'];
                                $haulagePricingAry['idHaulagePricingModel'] = $haulageModelsArys['id'];
                                $haulagePricingAry['fWmFactor'] =  $haulageModelsArys['fWmFactor'];
                                $haulagePricingAry['szModelName'] =  $haulageModelsArys['szName'];
                                $haulagePricingAry['iDistanceUptoKm'] =  $haulageModelsArys['iDistanceUpToKm'];
                                $haulagePricingAry['fFuelPercentage'] =  $haulageModelsArys['fFuelPercentage'];
                                
                                if($iDebugFlag==1)
                                {
                                    $strdata=array();
                                    $strdata[0]=" \n\n *** Haulage Result \n\n";
                                    $strdata[1] = print_R($haulagePricingAry,true)."\n\n"; 
                                    file_log(true, $strdata, $filename);
                                } 
                                return $haulagePricingAry ;
                            }
                        }
                    }
                }  
            }
            else
            {
                return array();
            }
	}
	
        function isHaulageServicedInCountrybyWhs($idWarehouse,$idCountry,$iDirection)
        {       
            if($idWarehouse>0 && $idCountry>0 && $iDirection>0)
            {
                $query = "  
                    SELECT 
                       id 
                    FROM 
                        ".__DBC_SCHEMATA_HAULAGE_COUNTRY__." hd 
                   WHERE 
                        hd.idWarehouse = '".(int)$idWarehouse."' 
                    AND 
                        hd.idCountry = '".(int)$idCountry."' 
                    AND 
                        hd.idDirection = '".(int)$iDirection."' 
                ";   
                if($result=$this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    return true;
                }
            }
        }

	function getHaulageDetails_review($haulageDAry)
	{
		$idWarehouse = $haulageDAry['idWarehouse'];
		$idHaulageModel = $haulageDAry['idHaulageModel'];
		//$idWarehouse = 1;
		$haulageModelsAry = array();
		$kHaulagePricing = new cHaulagePricing();
		$haulageModelsAry = $kHaulagePricing->getAllActiveHaulageModelsByCFS_review($idWarehouse,$haulageDAry['iDirection'],$idHaulageModel);
	
		if(!empty($haulageModelsAry))
		{
			foreach($haulageModelsAry as $haulageModelsArys)
			{
				//zone pricing
				if($haulageModelsArys['idHaulageModel']==1)
				{
					$data=array();
					$data['szLatitude'] = $haulageDAry['szLatitude'] ;
					$data['szLongitude'] = $haulageDAry['szLongitude'] ;
					$data['idHaulagePricingModel'] = $haulageModelsArys['idHaulgePricingModel'];
					if($kHaulagePricing->isLocationExistsInZone($data))
					{
						$haulageDataAry=array();
						$haulageDataAry['idHaulagePricingModel'] = $haulageModelsArys['idHaulgePricingModel'];
						$haulageDataAry['fTotalWeight'] = $haulageDAry['fTotalWeight'] ;
						$haulageDataAry['fVolume'] = $haulageDAry['fVolume'];
						$haulageDataAry['iHaulageTransitTime'] =  $haulageModelsArys['iTransitHours'];
						$haulageDataAry['fWmFactor'] =  $haulageModelsArys['fWmFactor'];
						$haulageDataAry['fFuelPercentage'] =  $haulageModelsArys['fFuelPercentage'];
					
						$haulagePricingAry = $kHaulagePricing->calculateHaulageZonePricing($haulageDataAry,'REVIEW');
					
						if(!empty($haulagePricingAry))
						{
							$haulagePricingAry['idHaulageModel'] = $haulageModelsArys['idHaulageModel'];
							$haulagePricingAry['idHaulagePricingModel'] = $haulageModelsArys['idHaulgePricingModel'];
							$haulagePricingAry['fWmFactor'] =  $haulageModelsArys['fWmFactor'];
							$haulagePricingAry['szModelName'] =  $haulageModelsArys['szName'];
							$haulagePricingAry['iDistanceUptoKm'] =  $haulageModelsArys['iDistanceUpToKm'];
							$haulagePricingAry['fFuelPercentage'] =  $haulageModelsArys['fFuelPercentage'];
							return $haulagePricingAry ;
						}					
					}
				}
				if($haulageModelsArys['idHaulageModel']==2)
				{
					$data=array();
					$data['iDistance'] = $haulageDAry['iDistance'];
					$data['szCity'] = $haulageDAry['szCity'];
					$data['szLatitude'] = $haulageDAry['szLatitude'];
					$data['szLongitude'] = $haulageDAry['szLongitude'];
					$data['idCountry'] = $haulageDAry['idCountry'];
					$data['szCityLatitude'] = $haulageDAry['szCityLatitude'];
					$data['szCityLongitude'] = $haulageDAry['szCityLongitude'];
					
					$data['idHaulagePricingModel'] = $haulageModelsArys['idHaulgePricingModel'];
					$data['iRadiousKM'] = $haulageModelsArys['iRadiousKM'];
					
					if($kHaulagePricing->isLocationExistsByCity($data,'REVIEW'))
					{	
						$haulageDataAry=array();						
						$haulageDataAry['idHaulagePricingModel'] = $haulageModelsArys['idHaulgePricingModel'];
						$haulageDataAry['fTotalWeight'] = $haulageDAry['fTotalWeight'] ;
						$haulageDataAry['fVolume'] = $haulageDAry['fVolume'];
						$haulageDataAry['iHaulageTransitTime'] =  $haulageModelsArys['iTransitHours'];
						$haulageDataAry['fWmFactor'] =  $haulageModelsArys['fWmFactor'];
						$haulageDataAry['fFuelPercentage'] =  $haulageModelsArys['fFuelPercentage'];
						$haulageDataAry['idHaulageModel'] = $haulageModelsArys['idHaulageModel'];
						
						$haulagePricingAry = $kHaulagePricing->calculateHaulageZonePricing($haulageDataAry,'REVIEW');
					
						if(!empty($haulagePricingAry))
						{
							$haulagePricingAry['idHaulageModel'] = $haulageModelsArys['idHaulageModel'];
							$haulagePricingAry['idHaulagePricingModel'] = $haulageModelsArys['idHaulgePricingModel'];
							$haulagePricingAry['fWmFactor'] =  $haulageModelsArys['fWmFactor'];
							$haulagePricingAry['szModelName'] =  $haulageModelsArys['szName'];
							$haulagePricingAry['iDistanceUptoKm'] =  $haulageModelsArys['iDistanceUpToKm'];
							$haulagePricingAry['fFuelPercentage'] =  $haulageModelsArys['fFuelPercentage'];
							return $haulagePricingAry ;
						}
					}
				}
				if($haulageModelsArys['idHaulageModel']==3)
				{
					$data=array();
					$data['szPostcode'] = $haulageDAry['szPostcode'] ;
					$data['idHaulagePricingModel'] = $haulageModelsArys['idHaulgePricingModel'];
					
					if($kHaulagePricing->isLocationExistsByPostcode($data))
					{
						$haulageDataAry=array();
						$haulageDataAry['idHaulagePricingModel'] = $haulageModelsArys['idHaulgePricingModel'];
						$haulageDataAry['fTotalWeight'] = $haulageDAry['fTotalWeight'] ;
						$haulageDataAry['fVolume'] = $haulageDAry['fVolume'];
						$haulageDataAry['iHaulageTransitTime'] =  $haulageModelsArys['iTransitHours'];
						$haulageDataAry['fWmFactor'] =  $haulageModelsArys['fWmFactor'];
						$haulageDataAry['fFuelPercentage'] =  $haulageModelsArys['fFuelPercentage'];
						$haulagePricingAry = $kHaulagePricing->calculateHaulageZonePricing($haulageDataAry,'REVIEW');
						if(!empty($haulagePricingAry))
						{
							$haulagePricingAry['idHaulageModel'] = $haulageModelsArys['idHaulageModel'];
							$haulagePricingAry['idHaulagePricingModel'] = $haulageModelsArys['idHaulgePricingModel'];
							$haulagePricingAry['fWmFactor'] =  $haulageModelsArys['fWmFactor'];
							$haulagePricingAry['szModelName'] =  $haulageModelsArys['szName'];
							$haulagePricingAry['iDistanceUptoKm'] =  $haulageModelsArys['iDistanceUpToKm'];
							$haulagePricingAry['fFuelPercentage'] =  $haulageModelsArys['fFuelPercentage'];
							return $haulagePricingAry ;
						}
					}
				}
				if($haulageModelsArys['idHaulageModel']==4)
				{
					$data=array();
					$data['iDistance'] = $haulageDAry['iDistance'];
					$data['idCountry'] = $haulageDAry['idCountry'];
					$data['idHaulagePricingModel'] = $haulageModelsArys['idHaulgePricingModel'];
					if($kHaulagePricing->isLocationExistsByDistance($data))
					{						
						$haulageDataAry=array();
						$haulageDataAry['idHaulagePricingModel'] = $haulageModelsArys['idHaulgePricingModel'];
						$haulageDataAry['fTotalWeight'] = $haulageDAry['fTotalWeight'] ;
						$haulageDataAry['fVolume'] = $haulageDAry['fVolume'];
						$haulageDataAry['iHaulageTransitTime'] =  $haulageModelsArys['iTransitHours'];
						$haulageDataAry['fWmFactor'] =  $haulageModelsArys['fWmFactor'];
						$haulageDataAry['fFuelPercentage'] =  $haulageModelsArys['fFuelPercentage'];
						$haulagePricingAry = $kHaulagePricing->calculateHaulageZonePricing($haulageDataAry,'REVIEW');
						if(!empty($haulagePricingAry))
						{
							$haulagePricingAry['idHaulageModel'] = $haulageModelsArys['idHaulageModel'];
							$haulagePricingAry['idHaulagePricingModel'] = $haulageModelsArys['idHaulgePricingModel'];
							$haulagePricingAry['fWmFactor'] =  $haulageModelsArys['fWmFactor'];
							$haulagePricingAry['szModelName'] =  $haulageModelsArys['szName'];
							$haulagePricingAry['iDistanceUptoKm'] =  $haulageModelsArys['iDistanceUpToKm'];
							$haulagePricingAry['fFuelPercentage'] =  $haulageModelsArys['fFuelPercentage'];
							return $haulagePricingAry ;
						}
					}
				}
			}
		}
		else
		{
			return array();
		}
	}
	
/**
	 * Get All Warehouse Haulage Model List
	*/
	function getWarehouseHaulageModelList($data)
	{
		$query="
                    SELECT
                        cm.id as idMapping,
                        cm.idHaulageModel,
                        cm.iPriority,
                        hp.szModelLongName,
                        cm.iActive,
                        cm.idWarehouse
                    FROM
                        ".__DBC_SCHEMATA_HAULAGE_MODELS_CFS_MAPPING__." AS cm
                    INNER JOIN
                        ".__DBC_SCHEMATA_HAULAGE_MODELS__." AS hp
                    ON
                        hp.id=cm.idHaulageModel
                    WHERE
                        cm.idWarehouse='".(int)$data['idWarehouse']."'
                    AND
                        cm.iDirection='".(int)$data['idDirection']."'	
                    ORDER BY
                        cm.iActive DESC,cm.iPriority ASC
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$ctr=0;
				while($row=$this->getAssoc($result))
				{
					$res_array[$ctr]['idHaulageModel']=$row['idHaulageModel'];
					$res_array[$ctr]['iPriority']=$row['iPriority'];
					$res_array[$ctr]['szModelLongName']=$row['szModelLongName'];
					$res_array[$ctr]['iActive'] = $row['iActive'];
					$res_array[$ctr]['idMapping'] = $row['idMapping'];
                                        $res_array[$ctr]['idWarehouse'] = $row['idWarehouse'];
                                        
					
					$query="
						SELECT
							count(id) as total
						FROM
							".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
						WHERE
							idWarehouse='".(int)$data['idWarehouse']."'
						AND
							iDirection='".(int)$data['idDirection']."'
						AND
							idHaulageModel='".(int)$row['idHaulageModel']."'
						AND
							iActive='1'
					";
					if($result1=$this->exeSQL($query))
					{
						if($this->iNumRows>0)
						{
							$row1=$this->getAssoc($result1);
							$res_array[$ctr]['iCount']=$row1['total'];
						}
						else
						{
							$res_array[$ctr]['iCount']=0;
						}
					}
					else
					{
						$res_array[$ctr]['iCount']=0;
					}
					++$ctr;
				}
				return $res_array;
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	/**
	 * Changing the status of CFS Haulage Model
	*/
	function updateStatusCFSHaulageModel($idMapping,$status,$idWarehouse,$idDirection)
	{	
		if((int)$idMapping>0)
		{
			if($status=='I')
			{
				$value='0';
				$priority='4';
			}
			else
			{
				$value='1';
				$query="
					SELECT
						MAX(iPriority) as priority
					FROM
						".__DBC_SCHEMATA_HAULAGE_MODELS_CFS_MAPPING__."
					WHERE
						idWarehouse='".(int)$idWarehouse."'
					AND
						iDirection='".(int)$idDirection."'
					AND
						iActive='1'
				";
				$result=$this->exeSQL($query);
				$row=$this->getAssoc($result);
				$priority=$row['priority']+1;
			}
			$query="
				SELECT
					iPriority
				FROM
					".__DBC_SCHEMATA_HAULAGE_MODELS_CFS_MAPPING__."
				WHERE
					id='".(int)$idMapping."'
			";
			//echo $query."<br/>";
			$result=$this->exeSQL($query);
			$row=$this->getAssoc($result);
			$iPriority=$row['iPriority'];
			
			$query="
				UPDATE
					".__DBC_SCHEMATA_HAULAGE_MODELS_CFS_MAPPING__."
				SET
					iPriority='".(int)$priority."',
					iActive='".(int)$value."'
				WHERE
					id='".(int)$idMapping."'	
			";
			//echo $query."<br/>";
			$result=$this->exeSQL($query);
			
			if($status=='I')
			{
				$query="
					UPDATE
						".__DBC_SCHEMATA_HAULAGE_MODELS_CFS_MAPPING__."
					SET
						iPriority=iPriority-1
					WHERE
						iPriority>'".(int)$iPriority."'
					AND	
						id<>'".(int)$idMapping."'
					AND
						idWarehouse='".(int)$idWarehouse."'
					AND
						iDirection='".(int)$idDirection."'
				";
				//echo $query."<br/>";
				$result=$this->exeSQL($query);
			}
			else
			{
				$query="
					UPDATE
						".__DBC_SCHEMATA_HAULAGE_MODELS_CFS_MAPPING__."
					SET
						iPriority=iPriority+1
					WHERE
						iPriority<='".(int)$iPriority."'
					AND
						id<>'".(int)$idMapping."'
					AND
						iActive='0'
					AND
						idWarehouse='".(int)$idWarehouse."'
					AND
						iDirection='".(int)$idDirection."'	
				";
				//echo $query."<br/>";
				$result=$this->exeSQL($query);
			}
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Changing  the priority
	*/
	function updatePriorityCFSHaulageModel($idMapping,$moveflag,$idWarehouse,$idDirection)
	{	
		if((int)$idMapping>0)
		{
			$query="
				SELECT
					iPriority
				FROM
					".__DBC_SCHEMATA_HAULAGE_MODELS_CFS_MAPPING__."
				WHERE
					id='".(int)$idMapping."'
			";
			//echo $query."<br/>";
			$result=$this->exeSQL($query);
			$row=$this->getAssoc($result);
			$iPriority=$row['iPriority'];
			
			if($moveflag=='up')
			{
				$query="
					UPDATE
						".__DBC_SCHEMATA_HAULAGE_MODELS_CFS_MAPPING__."
					SET
						iPriority=iPriority-1
					WHERE
						id='".(int)$idMapping."'
				";
				//echo $query."<br/>";
				$result=$this->exeSQL($query);
				
				$query="
					UPDATE
						".__DBC_SCHEMATA_HAULAGE_MODELS_CFS_MAPPING__."
					SET
						iPriority=iPriority+1
					WHERE
						id!='".(int)$idMapping."'
					AND
						iPriority='".(int)($iPriority-1)."'
					AND
						idWarehouse='".(int)$idWarehouse."'
					AND
						iDirection='".(int)$idDirection."'	
					";
	
				$result = $this->exeSQL( $query );
			}
			else if($moveflag=='down')
			{
				$query="
					UPDATE
						".__DBC_SCHEMATA_HAULAGE_MODELS_CFS_MAPPING__."
					SET
						iPriority=iPriority+1
					WHERE
						id='".(int)$idMapping."'
				";
				//echo $query."<br/>";
				$result=$this->exeSQL($query);
				
				$query="
					UPDATE
						".__DBC_SCHEMATA_HAULAGE_MODELS_CFS_MAPPING__."
					SET
						iPriority=iPriority-1
					WHERE
						id!='".(int)$idMapping."'
					AND
						iPriority='".(int)($iPriority+1)."'
					AND
						idWarehouse='".(int)$idWarehouse."'
					AND
						iDirection='".(int)$idDirection."'	
					";
	
				$result = $this->exeSQL( $query );
			}
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function viewPricingHaulageZoneModel($idWarehouse,$idHaulageModel,$idDirection,$flag='')
	{
		$inner_query='';
		$sql='';
		if($flag=='distance')
		{
			$sql="
				ORDER BY
					hp.iDistanceUpToKm ASC
				";
		}
		else if($flag=='szCity')
		{
			$sql="
				ORDER BY
					hp.szName ASC
				";
			/*$inner_query="
				INNER JOIN
					".__DBC_SCHEMATA_COUNTRY__." AS c
				ON
					c.id=hp.idCountry
			";*/
		}
		else if($flag=='zone')
		{
			$sql="
				ORDER BY
					hp.szName ASC
				";
		}
		/*else if($flag=='postcode')
		{
			$sql="
				ORDER BY
					c.szCountryName ASC,
					hp.szName ASC
				";
			$inner_query="
				INNER JOIN
					".__DBC_SCHEMATA_COUNTRY__." AS c
				ON
					c.id=hp.idCountry
			";
		}*/
		else
		{
			$sql="
				ORDER BY
					hp.iPriority ASC
				";
		}
		$query="
			SELECT
				hp.id,
				hp.szName,
				hp.iPriority,
				htt.iHours,
				hp.fWmFactor,
				hp.fFuelPercentage,
				hp.idWarehouse,
				hp.iDirection,
				hp.idHaulageModel,
				hp.idCountry,
				hp.iRadiousKM,
				hp.iDistanceUpToKm 	
			FROM
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." AS hp
			INNER JOIN
				".__DBC_SCHEMATA_HAULAGE_TRANSIT_TIME__." AS htt
			ON
				hp.idHaulageTransitTime=htt.id
				".$inner_query."
			WHERE
				hp.idWarehouse='".(int)$idWarehouse."'
			AND
				hp.iDirection='".(int)$idDirection."'
			AND
				hp.idHaulageModel='".(int)$idHaulageModel."'
			AND
				hp.iActive='1'
			$sql
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				while($row=$this->getAssoc($result))
				{
					$zone_arr[]=$row;
				}
				return $zone_arr;
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	/**
	 * Changing  the priority of pricing model
	*/
	function updatePriorityCFSHaulagePricingModel($idMapping,$moveflag,$idWarehouse,$idDirection,$idHaulageModel)
	{
	
		if((int)$idMapping>0)
		{
			$query="
				SELECT
					iPriority
				FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
				WHERE
					id='".(int)$idMapping."'
			";
			//echo $query."<br/>";
			$result=$this->exeSQL($query);
			$row=$this->getAssoc($result);
			$iPriority=$row['iPriority'];
			//echo $moveflag."hello";
			if($moveflag=='up')
			{
				$query="
					UPDATE
						".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
					SET
						iPriority=iPriority-1
					WHERE
						id='".(int)$idMapping."'
				";
				//echo $query."<br/>";
				$result=$this->exeSQL($query);
				
				$query="
					UPDATE
						".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
					SET
						iPriority=iPriority+1
					WHERE
						id!='".(int)$idMapping."'
					AND
						iPriority='".(int)($iPriority-1)."'
					AND
						idWarehouse='".(int)$idWarehouse."'
					AND
						iDirection='".(int)$idDirection."'
					AND
						idHaulageModel='".(int)$idHaulageModel."'	
					";
			//	echo $query;
				$result = $this->exeSQL( $query );
			}
			else if($moveflag=='down')
			{
				$query="
					UPDATE
						".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
					SET
						iPriority=iPriority+1
					WHERE
						id='".(int)$idMapping."'
				";
			//	echo $query."<br/>";
				$result=$this->exeSQL($query);
				
				$query="
					UPDATE
						".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
					SET
						iPriority=iPriority-1
					WHERE
						id!='".(int)$idMapping."'
					AND
						iPriority='".(int)($iPriority+1)."'
					AND
						idWarehouse='".(int)$idWarehouse."'
					AND
						iDirection='".(int)$idDirection."'
					AND
						idHaulageModel='".(int)$idHaulageModel."'	
					";
				//echo $query;
				$result = $this->exeSQL( $query );
				
				
			}
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function getHaulagePricingZoneDetailData($id)
	{
		$query="
			SELECT
				hp.id,
				hp.iUpToKg,
				hp.fPricePer100Kg,
				hp.fMinimumPrice,
				hp.fPricePerBooking,
				c.szCurrency,
				hpm.fWmFactor,
				hpm.fFuelPercentage,
				hp.idHaulagePricingModel,
				hp.idCurrency
			FROM
				".__DBC_SCHEMATA_HAULAGE_PRICING__." AS hp
			INNER JOIN
				".__DBC_SCHEMATA_CURRENCY__." AS c
			ON
				hp.idCurrency=c.id
			INNER JOIN
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." AS hpm
			ON
				hp.idHaulagePricingModel=hpm.id
			WHERE
				hp.idHaulagePricingModel='".(int)$id."'
			AND
				hp.iActive='1'
			ORDER BY
				hp.iUpToKg ASC
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				while($row=$this->getAssoc($result))
				{
					$zone_detail_arr[]=$row;
				}
				return $zone_detail_arr;
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function getHaulageModelName($id)
	{
		if((int)$id>0)
		{
			$query="
				SELECT
					szName
				FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
				WHERE
					id='".(int)$id."'
			";
			//echo $query."<br/>";
			$result=$this->exeSQL($query);
			$row=$this->getAssoc($result);
			$szName=$row['szName'];
			return $szName;
		}
		else
		{
			return '';
		}
	}
	
	function deleteHaulagePricingZoneData($id,$idWarehouse,$idHaulageModel,$idDirection)
	{
		if((int)$id>0)
		{
			$query="
				SELECT
					iPriority
				FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
				WHERE
					id='".(int)$id."'
			";
			//echo $query."<br/>";
			$result=$this->exeSQL($query);
			$row=$this->getAssoc($result);
			$iPriority=$row['iPriority'];
			
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
				WHERE
					id='".(int)$id."'				
			";
			$result=$this->exeSQL($query);
			
			
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING__."
				WHERE
					idHaulagePricingModel='".(int)$id."'				
			";
			$result=$this->exeSQL($query);
			
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_HAULAGE_ZONE__."
				WHERE
					idHaulagePricingModel='".(int)$id."'				
			";
			$result=$this->exeSQL($query);
			
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_HAULAGE_COUNTRY__."
				WHERE
					idHaulagePricingModel='".(int)$id."'				
			";
			$result=$this->exeSQL($query);
			
			
			$query="
				UPDATE
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
				SET
					iPriority=iPriority-1
				WHERE
					id!='".(int)$id."'
				AND
					iPriority>='".(int)($iPriority+1)."'
				AND
					idWarehouse='".(int)$idWarehouse."'
				AND
					iDirection='".(int)$idDirection."'
				AND
					idHaulageModel='".(int)$idHaulageModel."'	
				";
			//echo $query;
			$result = $this->exeSQL( $query );
		}
		else
		{
			return false;
		}
	}
	
	function deleteHaulageZonePricingData($id)
	{
		if((int)$id>0)
		{
			$query="
				UPDATE
					".__DBC_SCHEMATA_HAULAGE_PRICING__."
				SET
					iActive='0'
				WHERE
					id='".(int)$id."'				
			";
			$result=$this->exeSQL($query);
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function zoneLoadPricing($id)
	{
		if((int)$id>0)
		{
			$query="
				SELECT
					id,
					iUpToKg,
					fPricePer100Kg,
					fMinimumPrice,
					fPricePerBooking,
					idCurrency
				FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING__."
				WHERE
					id='".(int)$id."'
			";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					while($row=$this->getAssoc($result))
					{
						$zone_detail_arr[]=$row;
					}
					return $zone_detail_arr;
				}
				else
				{
					return array();
				}
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}		
	}
	
	function updateZonePricingLine($data)
	{
		$this->set_id(trim(sanitize_all_html_input($data['idEdit'])));
		$this->set_fPricePer100Kg(trim(sanitize_all_html_input($data['fPricePer100Kg'])));
		$this->set_iUpToKg(trim(sanitize_all_html_input($data['iUpToKg'])));
		$this->set_fMinimumPrice(trim(sanitize_all_html_input(urlencode($data['fMinimumPrice']))));
		$this->set_fPricePerBooking(trim(sanitize_all_html_input($data['fPricePerBooking'])));
		$this->set_idCurrency(trim(sanitize_all_html_input($data['idCurrency'])));
		
		
	   if(!empty($this->iUpToKg) && $this->isUpToKgWeightExist($this->id,$this->iUpToKg,$data['idHaulagePricingModel']))
	   {
			$this->addError( "iUpToKg" , t($this->t_base.'messages/weight_already_exist'));
	   }
		
		//exit if error exist.
		if ($this->error === true)
		{
			return false;
		}
		
		$query="
			UPDATE
				".__DBC_SCHEMATA_HAULAGE_PRICING__."
			SET
				iUpToKg='".(int)$this->iUpToKg."',
				fPricePer100Kg='".(float)$this->fPricePer100Kg."',
				fMinimumPrice='".(float)$this->fMinimumPrice."',
				fPricePerBooking='".(float)$this->fPricePerBooking."',
				idCurrency='".(int)$this->idCurrency."'
			WHERE
				id='".(int)$this->id."'
		";
		if($result=$this->exeSQL($query))
		{
			$kForwarder = new cForwarder();
			$idForwarder = $_SESSION['forwarder_id'];
			$kForwarder->load($idForwarder);
			if(($kForwarder->iAgreeTNC==1) && ($kForwarder->iUpdateRss==2))
			{
				$kConfig = new cConfig();
				$kWhsSearch = new cWHSSearch();
				$haulagePricingModelAry = array();
				$haulagePricingModelAry = $this->loadZone($data['idHaulagePricingModel']);
				$idOriginWhs = $haulagePricingModelAry['idWarehouse'];
				$Direction = $haulagePricingModelAry['iDirection'];
					
				$kWhsSearch->load($idOriginWhs);
				$szOriginCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
									
				if($Direction=='1')
				{
					$szDirection = 'Export';
				}
				else if($Direction=='2')
				{
					$szDirection = 'Import';
				}
					
				$replace_ary = array();
				$replace_ary['szForwarderDisplayName'] = $kForwarder->szDisplayName ;
				$replace_ary['szCountryName'] = $szOriginCountry ;
				$replace_ary['szImportExportText'] = $szDirection ;
					
				$content_ary = array();
				$kExportImport = new cExport_Import();
				$content_ary = $kExportImport->replaceRssContent('__OBO_HAULAGE__',$replace_ary);
			
				$rssDataAry=array();
				$rssDataAry['idForwarder'] = $kForwarder->id ;
				$rssDataAry['szFeedType'] = '__OBO_HAULAGE__';
				$rssDataAry['szLink'] = __RSS_FEED_PAGE_URL__ ;
				$rssDataAry['szTitle'] = $content_ary['szTitle'];
				$rssDataAry['szDescription'] = $content_ary['szDescription'];
				$rssDataAry['szDescriptionHTML'] = $content_ary['szDescriptionHTML'];
					
				$kRss = new cRSS();
				$kRss->addRssFeed($rssDataAry);
			}
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function isUpToKgWeightExist($id=0,$iUpToKg,$idHaulagePricingModel)
	{
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_HAULAGE_PRICING__."
			WHERE
				iUpToKg = '".mysql_escape_custom($iUpToKg)."'
			AND
				idHaulagePricingModel='".(int)$idHaulagePricingModel."'
			AND
				iActive ='1'
		";
		if((int)$id>0)
		{
			$query .="
				AND
				    id<>'".(int)$id."'		
			";
		}
		//echo "<br>".$query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
            if( $this->getRowCnt() > 0 )
            {
                return true;
            }
            else
            {
             	return false;
            }
		}
	}
	
	function addZonePricingLine($data)
	{
		$this->set_fPricePer100Kg(trim(sanitize_all_html_input($data['fPricePer100Kg'])));
		$this->set_iUpToKg(trim(sanitize_all_html_input($data['iUpToKg'])));
		$this->set_fMinimumPrice(trim(sanitize_all_html_input(urlencode($data['fMinimumPrice']))));
		$this->set_fPricePerBooking(trim(sanitize_all_html_input($data['fPricePerBooking'])));
		$this->set_idCurrency(trim(sanitize_all_html_input($data['idCurrency'])));
		
		
	   if(!empty($this->iUpToKg) && $this->isUpToKgWeightExist(0,$this->iUpToKg,$data['idHaulagePricingModel']))
	   {
			$this->addError( "iUpToKg" , t($this->t_base.'messages/weight_already_exist'));
	   }
		
		//exit if error exist.
		if ($this->error === true)
		{
			return false;
		}
		
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_HAULAGE_PRICING__."
			WHERE
				iUpToKg = '".(int)$this->iUpToKg."'
			AND
				idHaulagePricingModel='".(int)$data['idHaulagePricingModel']."'
		";
		//echo "<br>".$query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
            if( $this->getRowCnt() > 0 )
            {
            	$query="
					UPDATE
						".__DBC_SCHEMATA_HAULAGE_PRICING__."
					SET
						fPricePer100Kg='".(float)$this->fPricePer100Kg."',
						fMinimumPrice='".(float)$this->fMinimumPrice."',
						fPricePerBooking='".(float)$this->fPricePerBooking."',
						idCurrency='".(int)$this->idCurrency."',
						iActive='1'
					WHERE
						idHaulagePricingModel='".(int)$data['idHaulagePricingModel']."'
					AND
						iUpToKg = '".(int)$this->iUpToKg."'
					";
            	$result = $this->exeSQL( $query );
            	$kForwarder = new cForwarder();
				$idForwarder = $_SESSION['forwarder_id'];
				$kForwarder->load($idForwarder);
				if(($kForwarder->iAgreeTNC==1) && ($kForwarder->iUpdateRss==2))
				{				
					$kConfig = new cConfig();
					$kWhsSearch = new cWHSSearch();
					$haulagePricingModelAry = array();
					$haulagePricingModelAry = $this->loadZone($data['idHaulagePricingModel']);
					$idOriginWhs = $haulagePricingModelAry['idWarehouse'];
					$Direction = $haulagePricingModelAry['iDirection'];
					
					$kWhsSearch->load($idOriginWhs);
					$szOriginCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
					/*
					$kWhsSearch->load($this->idDestinationWarehouse);
					$szDestinationCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
					*/
					
					if($Direction=='1')
					{
						$szDirection = 'Export';
					}
					else if($Direction=='2')
					{
						$szDirection = 'Import';
					}
					
					$replace_ary = array();
					$replace_ary['szForwarderDisplayName'] = $kForwarder->szDisplayName ;
					$replace_ary['szCountryName'] = $szOriginCountry ;
					$replace_ary['szImportExportText'] = $szDirection ;
					
					$content_ary = array();
					$kExportImport = new cExport_Import();
					$content_ary = $kExportImport->replaceRssContent('__OBO_HAULAGE__',$replace_ary);
				
					$rssDataAry=array();
					$rssDataAry['idForwarder'] = $kForwarder->id ;
					$rssDataAry['szFeedType'] = '__OBO_HAULAGE__';
					$rssDataAry['szLink'] = __RSS_FEED_PAGE_URL__ ;
					$rssDataAry['szTitle'] = $content_ary['szTitle'];
					$rssDataAry['szDescription'] = $content_ary['szDescription'];
					$rssDataAry['szDescriptionHTML'] = $content_ary['szDescriptionHTML'];
					
					$kRss = new cRSS();
					$kRss->addRssFeed($rssDataAry);
				}
            	return true;
            }
            else
            {
		
				$query="
					INSERT INTO
						".__DBC_SCHEMATA_HAULAGE_PRICING__."
					(	
						idHaulagePricingModel,
						iUpToKg,
						fPricePer100Kg,
						fMinimumPrice,
						fPricePerBooking,
						idCurrency,
						dtCreatedOn
					)
						VALUES
					(
						'".(int)$data['idHaulagePricingModel']."',
						'".(int)$this->iUpToKg."',
						'".(float)$this->fPricePer100Kg."',
						'".(float)$this->fMinimumPrice."',
						'".(float)$this->fPricePerBooking."',
						'".(int)$this->idCurrency."',
						NOW()
					)
				";
				//echo $query;
				if($result=$this->exeSQL($query))
				{
					$kForwarder = new cForwarder();
					$idForwarder = $_SESSION['forwarder_id'];
					$kForwarder->load($idForwarder);
					
					if(($kForwarder->iAgreeTNC==1) && ($kForwarder->iUpdateRss==2))
					{				
						$kConfig = new cConfig();
						$kWhsSearch = new cWHSSearch();
						$haulagePricingModelAry = array();
						$haulagePricingModelAry = $this->loadZone($data['idHaulagePricingModel']);
						$idOriginWhs = $haulagePricingModelAry['idWarehouse'];
						$Direction = $haulagePricingModelAry['iDirection'];
						
						$kWhsSearch->load($idOriginWhs);
						$szOriginCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
						/*
						$kWhsSearch->load($this->idDestinationWarehouse);
						$szDestinationCountry = $kConfig->getCountryName($kWhsSearch->idCountry);
						*/
						
						if($Direction=='1')
						{
							$szDirection = 'Export';
						}
						else if($Direction=='2')
						{
							$szDirection = 'Import';
						}
						
						$replace_ary = array();
						$replace_ary['szForwarderDisplayName'] = $kForwarder->szDisplayName ;
						$replace_ary['szCountryName'] = $szOriginCountry ;
						$replace_ary['szImportExportText'] = $szDirection ;
						
						$content_ary = array();
						$kExportImport = new cExport_Import();
						$content_ary = $kExportImport->replaceRssContent('__OBO_HAULAGE__',$replace_ary);
						
						$rssDataAry=array();
						$rssDataAry['idForwarder'] = $kForwarder->id ;
						$rssDataAry['szFeedType'] = '__OBO_HAULAGE__';
						$rssDataAry['szLink'] = __RSS_FEED_PAGE_URL__ ;
						$rssDataAry['szTitle'] = $content_ary['szTitle'];
						$rssDataAry['szDescription'] = $content_ary['szDescription'];
						$rssDataAry['szDescriptionHTML'] = $content_ary['szDescriptionHTML'];
						
						$kRss = new cRSS();
						$kRss->addRssFeed($rssDataAry);
					}
					return true;
				}
				else
				{
					return false;
				}
            }
		}
	}
	
	function loadZone($id)
	{
		if((int)$id>0)
		{
			$query="
				SELECT
					id,
					szName,
					fWmFactor,
					fFuelPercentage,
					idHaulageTransitTime,
					idWarehouse,
					iDirection
				FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
				WHERE
					id='".(int)$id."'
			";
			//echo $query."<br/>";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					return $row;
				}
				else
				{
					return array();
				}
				
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	
	function isZoneNameExist($id=0,$szName,$idWarehouse,$iDirection,$idHaulageModel)
	{
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
			WHERE
				szName = '".mysql_escape_custom($szName)."'
			AND
				idWarehouse='".(int)$idWarehouse."'
			AND
				iDirection ='".(int)$iDirection."'
			AND
				idHaulageModel='".(int)$idHaulageModel."'
		";
		if((int)$id>0)
		{
			$query .="
				AND
				    id<>'".(int)$id."'		
			";
		}
		//echo "<br>".$query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
            if( $this->getRowCnt() > 0 )
            {
                return true;
            }
            else
            {
             	return false;
            }
		}
	}
	
	function updateZone($data)
	{
		$kWHSSearch = new cWHSSearch();
		$kWHSSearch->load($data['idWarehouse']);
		
		$maxWMFactor=$kWHSSearch->getManageMentVariableByDescription('__MAX_W/M_FACTOR__');
		$miniWMFactor=$kWHSSearch->getManageMentVariableByDescription('__MINI_W/M_FACTOR__');
		$maxFuel=$kWHSSearch->getManageMentVariableByDescription('__MAX_FUEL__');
		$miniFuel=$kWHSSearch->getManageMentVariableByDescription('__MINI_FUEL__');
		$maxPoints=$kWHSSearch->getManageMentVariableByDescription('__MAX_NUMBER_POINT_FOR_ZONE__');
		$maxDistance=$kWHSSearch->getManageMentVariableByDescription('__MAX_DISTANCE_FOR_ZONE__');
		
		$this->set_szName(trim(sanitize_all_html_input($data['szName'])),__MAX_LENGTH_ZONE__);
		$this->set_fFuelPercentage(trim(sanitize_all_html_input(urlencode($data['fFuelPercentage']))),$maxFuel,$miniFuel);
		$this->set_idTransitTime(trim(sanitize_all_html_input($data['idTransitTime'])));
		$this->set_fWmFactor(trim(sanitize_all_html_input($data['fWmFactor'])),$maxWMFactor,$miniWMFactor);
		$latlongArr=explode("||",$data['szLatLongPipeline1']);
		$latlongCount=count($latlongArr);
				
		if($latlongCount<=2)
		{
			$this->addError( "szLatLongPipeline1" , t($this->t_base.'messages/atleast_three_coordinate_are_required'));
		}
		
		
		if($this->isZoneNameExist($data['id'],$this->szName,$data['idWarehouse'],$data['idDirection'],$data['idHaulageModel']))
		{
			$this->addError( "szName" , t($this->t_base.'messages/zone_already_exist_for_warehouse')." ".$kWHSSearch->szWareHouseName.", ".t($this->t_base.'messages/zone_already_exist_for_warehouse_1'));
		}
		
		$replace_ary=array('(',')');
		$latlongReplaceArr=array();
		if(!empty($latlongArr))
		{
			foreach($latlongArr as $latlongArrs)
			{
				$latlong=str_replace($replace_ary,"",$latlongArrs);
				if(!in_array($latlong,$latlongReplaceArr) && $latlong!='')
				{
					$latlongReplaceArr[]=$latlong;
					$latlongReplaceNewArr[]=$latlong;
				}
			}
		}
		
		if(count($latlongReplaceArr)>$maxPoints)
		{
			$this->addError( "szLatLongPipeline1" , t($this->t_base.'messages/you_can_add_max')." ".$maxPoints." ".t($this->t_base.'messages/points'));
			return false;
		}
		
		if(!empty($latlongReplaceArr))
		{
			foreach($latlongReplaceArr as $latlongReplaceArrs)
			{
				$latlongNewArr=explode(",",$latlongReplaceArrs);
				$szLatitude=$latlongNewArr[0];
				$szLongitude=$latlongNewArr[1];
				foreach($latlongReplaceNewArr as $latlongReplaceNewArrs)
				{
					$latlongArr=explode(",",$latlongReplaceNewArrs);
					if($szLatitude!=$latlongArr[0] && $szLongitude!=$latlongArr[1])
					{
						$fMaxLength=0;
						$longitude_diff = $latlongArr[1] - $szLongitude ;
						$latitude_diff = $latlongArr[0] - $szLatitude;
						$length = sqrt(($latitude_diff*$latitude_diff)+($longitude_diff*$longitude_diff));
						$fMaxLength = $length ;
						$maxLength = $fMaxLength * 111 ;
						//echo $maxLength."<br/>";
						if($maxLength>$maxDistance)
						{
							$this->addError( "szLatLongPipeline1" , t($this->t_base.'messages/distance_could_not_be_greater_than')." ".number_format($maxDistance)." ".t($this->t_base.'fields/km') );
							return false;
						}
					}
				}
				
			}
		}
				
		//exit if error exist.
		if ($this->error === true)
		{
			return false;
		}
		/*$replace_ary=array('(',')');
		$latlongReplaceArr=array();
		if(!empty($latlongArr))
		{
			foreach($latlongArr as $latlongArrs)
			{
				$latlong=str_replace($replace_ary,"",$latlongArrs);
				if(!in_array($latlong,$latlongReplaceArr))
				{
					$latlongReplaceArr[]=$latlong;
				}
			}
		}*/
		
		$query="
			UPDATE
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
			SET	
				szName='".mysql_escape_custom($this->szName)."',
				fWmFactor='".(int)$this->fWmFactor."',
				fFuelPercentage='".(float)$this->fFuelPercentage."',
				idHaulageTransitTime='".(int)$this->idTransitTime."'
			WHERE
				id='".(int)$data['id']."'
		";
		if( ( $result = $this->exeSQL( $query ) ) )
		{
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_HAULAGE_ZONE__."
				WHERE
					idHaulagePricingModel='".(int)$data['id']."'
			";
			$result = $this->exeSQL( $query );
			
			/*
			*
			* 1. If there are two vertexes which follow each other and has exact same latitude - we add 0.000001 to the second latitude number before recording it
			* 2. If there are two vertexes which follow each other and has exact same longitude - we add 0.000001 to the second longitude number before recording it
			* 
			*/
			$latlongReplaceArr = formatLatoitudeLongitude($latlongReplaceArr);
			if(!empty($latlongReplaceArr))
			{
				$ctr=1;
				for($i=0;$i<count($latlongReplaceArr['szLatitude']);$i++)
				{
					//$latlongNewArr=explode(",",$latlongReplaceArrs);
					
					$query="
						INSERT INTO
							".__DBC_SCHEMATA_HAULAGE_ZONE__."
						(
							szLatitude,
							szLongitude,
							idHaulagePricingModel,
							iVertexSequence,
							iActive,
							iLatestUpdated 	
						)
							VALUES
						(
							'".mysql_escape_custom($latlongReplaceArr['szLatitude'][$i])."',
							'".mysql_escape_custom($latlongReplaceArr['szLongitude'][$i])."',
							'".(int)$data['id']."',
							'".(int)$ctr."',
							'1',
							'1'
						)
					";
					$result = $this->exeSQL( $query );
					++$ctr;
				}
			}
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	function addZone($data)
	{
	
		$kWHSSearch = new cWHSSearch();
		$kWHSSearch->load($data['idWarehouse']);
		
		$maxWMFactor=$kWHSSearch->getManageMentVariableByDescription('__MAX_W/M_FACTOR__');
		$miniWMFactor=$kWHSSearch->getManageMentVariableByDescription('__MINI_W/M_FACTOR__');
		$maxFuel=$kWHSSearch->getManageMentVariableByDescription('__MAX_FUEL__');
		$miniFuel=$kWHSSearch->getManageMentVariableByDescription('__MINI_FUEL__');
		$maxPoints=$kWHSSearch->getManageMentVariableByDescription('__MAX_NUMBER_POINT_FOR_ZONE__');
		$maxDistance=$kWHSSearch->getManageMentVariableByDescription('__MAX_DISTANCE_FOR_ZONE__');
		
		$this->set_szName(trim(sanitize_all_html_input($data['szName'])),__MAX_LENGTH_ZONE__);
		$this->set_fFuelPercentage(trim(sanitize_all_html_input(urlencode($data['fFuelPercentage']))),$maxFuel,$miniFuel);
		$this->set_idTransitTime(trim(sanitize_all_html_input($data['idTransitTime'])));
		$this->set_fWmFactor(trim(sanitize_all_html_input($data['fWmFactor'])),$maxWMFactor,$miniWMFactor);
		$latlongArr=explode("||",$data['szLatLongPipeline1']);
		$latlongCount=count($latlongArr);
		//print_r($data);
		//echo $latlongCount;
		if($latlongCount<=2)
		{
			$this->addError( "szLatLongPipeline1" , t($this->t_base.'messages/atleast_three_coordinate_are_required'));
		}
		else if($latlongCount>$maxPoints)
		{
			$this->addError( "szLatLongPipeline1" , t($this->t_base.'messages/you_can_add_max')." ".$maxPoints." ".t($this->t_base.'messages/points'));
		}
		
		if($this->isZoneNameExist(0,$this->szName,$data['idWarehouse'],$data['idDirection'],$data['idHaulageModel']))
		{
			$this->addError( "szName" , t($this->t_base.'messages/zone_already_exist_for_warehouse')." ".$kWHSSearch->szWareHouseName.", ".t($this->t_base.'messages/zone_already_exist_for_warehouse_1'));
		}
		
		$replace_ary=array('(',')');
		$latlongReplaceArr=array();
		if(!empty($latlongArr))
		{
			foreach($latlongArr as $latlongArrs)
			{
				$latlong=str_replace($replace_ary,"",$latlongArrs);
				if(!in_array($latlong,$latlongReplaceArr) && $latlong!='')
				{
					$latlongReplaceArr[]=$latlong;
					$latlongReplaceNewArr[]=$latlong;
				}
			}
		}
		//print_r($latlongReplaceArr);
		if(!empty($latlongReplaceArr))
		{
			foreach($latlongReplaceArr as $latlongReplaceArrs)
			{
				$latlongNewArr=explode(",",$latlongReplaceArrs);
				$szLatitude=$latlongNewArr[0];
				$szLongitude=$latlongNewArr[1];
				foreach($latlongReplaceNewArr as $latlongReplaceNewArrs)
				{
					$latlongArr=explode(",",$latlongReplaceNewArrs);
					if($szLatitude!=$latlongArr[0] && $szLongitude!=$latlongArr[1])
					{
						$fMaxLength=0;
						$longitude_diff = $latlongArr[1] - $szLongitude ;
						$latitude_diff = $latlongArr[0] - $szLatitude;
						$length = sqrt(($latitude_diff*$latitude_diff)+($longitude_diff*$longitude_diff));
						$fMaxLength = $length ;
						$maxLength = $fMaxLength * 111 ;
						//echo $maxLength."<br/>";
						if($maxLength>$maxDistance)
						{
							$this->addError( "szLatLongPipeline1" , t($this->t_base.'messages/distance_could_not_be_greater_than')." ".number_format($maxDistance)." ".t($this->t_base.'fields/km') );
							return false;
						}
					}
				}
				
			}
		}
		
		//exit if error exist.
		if ($this->error === true)
		{
			return false;
		}
		
		
		$query="
			SELECT
				MAX(iPriority) as iPriority
			FROM
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
			WHERE
				idWarehouse='".(int)$data['idWarehouse']."'
			AND
				iDirection ='".(int)$data['idDirection']."'
			AND
				idHaulageModel='".(int)$data['idHaulageModel']."'
		";
		$result=$this->exeSQL($query);
			$row=$this->getAssoc($result);
			$iPriority=$row['iPriority']+1;
		
		$query="
			INSERT INTO
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
			(
				idWarehouse,
				iDirection,
				idHaulageModel,
				iPriority,
				szName,
				idHaulageTransitTime,
				fWmFactor,
				fFuelPercentage,
				iActive
			)
				VALUES
			(
				'".(int)$data['idWarehouse']."',
				'".(int)$data['idDirection']."',
				'".(int)$data['idHaulageModel']."',
				'".(int)$iPriority."',
				'".mysql_escape_custom($this->szName)."',
				'".(int)$this->idTransitTime."',
				'".(int)$this->fWmFactor."',
				'".(float)$this->fFuelPercentage."',
				'1'
			)
		";
		if($result=$this->exeSQL($query))
		{
			$id= $this->iLastInsertID ;
			
			$query="
				SELECT
					id,
					iPriority
				FROM
					".__DBC_SCHEMATA_CFS_MODEL_MAPPING__."
				WHERE
					idWarehouse='".(int)$data['idWarehouse']."'
				AND
					idHaulageModel='".(int)$data['idHaulageModel']."'
				AND
					iDirection ='".(int)$data['idDirection']."'
				AND
					iActive='0'
			";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
				
					$row=$this->getAssoc($result);
					$oldPriority=$row['iPriority'];
					
					$query="
						SELECT
							MAX(iPriority) AS maxPriority
						FROM
							".__DBC_SCHEMATA_CFS_MODEL_MAPPING__."
						WHERE
							idWarehouse='".(int)$data['idWarehouse']."'
						AND
							iDirection ='".(int)$data['idDirection']."'
						AND
							iActive='1'
						GROUP BY
							idWarehouse
					";
					$result=$this->exeSQL($query);
					
					$row=$this->getAssoc($result);
					$maxPriority=$row['maxPriority'];
					
					$newPrority=$maxPriority+1;
					
					$query="
						UPDATE
							".__DBC_SCHEMATA_CFS_MODEL_MAPPING__."
						SET
							iPriority=iPriority+1
						WHERE
							idWarehouse='".(int)$data['idWarehouse']."'
						AND
							idHaulageModel<>'".(int)$data['idHaulageModel']."'
						AND
							iDirection ='".(int)$data['idDirection']."'
						AND
							iPriority>='".(int)$newPrority."'
						AND
							iPriority<'".(int)$oldPriority."'
					";
					$result=$this->exeSQL($query);
					
					$query="
						UPDATE
							".__DBC_SCHEMATA_CFS_MODEL_MAPPING__."
						SET
							iActive='1',
							iPriority='".(int)$newPrority."'
						WHERE
							idWarehouse='".(int)$data['idWarehouse']."'
						AND
							idHaulageModel='".(int)$data['idHaulageModel']."'
						AND
							iDirection ='".(int)$data['idDirection']."'
					";
					$result=$this->exeSQL($query);				
				}
			}			
			/*
			*
			* 1. If there are two vertexes which follow each other and has exact same latitude - we add 0.000001 to the second latitude number before recording it
			* 2. If there are two vertexes which follow each other and has exact same longitude - we add 0.000001 to the second longitude number before recording it
			* 
			*/
			$latlongReplaceArr = formatLatoitudeLongitude($latlongReplaceArr);
						
			if(!empty($latlongReplaceArr))
			{
				$ctr=1;
				for($i=0;$i<count($latlongReplaceArr['szLatitude']);$i++)
				{
					//$latlongNewArr=explode(",",$latlongReplaceArrs);
					
					$query="
						INSERT INTO
							".__DBC_SCHEMATA_HAULAGE_ZONE__."
						(
							szLatitude,
							szLongitude,
							idHaulagePricingModel,
							iVertexSequence,
							iActive,
							iLatestUpdated 	
						)
							VALUES
						(
							'".mysql_escape_custom($latlongReplaceArr['szLatitude'][$i])."',
							'".mysql_escape_custom($latlongReplaceArr['szLongitude'][$i])."',
							'".(int)$id."',
							'".(int)$ctr."',
							'1',
							'1'
						)
					";
					//echo "<br /> ".$query ;
					$result = $this->exeSQL( $query );
					++$ctr;
				}
			}
			return $id;
		}
		else
		{
			return 0;
		}
		
	}
	
	function getActiveWarehouseZone($idForwarder,$idWarehouse,$idHaulageModel,$idCountry=0,$szCity='',$iWarehouseType=false)
	{
		$sql='';
		$groupBY='';
		if((int)$idCountry>0)
		{
                    $sql .="
                            AND
                                w.idCountry='".(int)$idCountry."'		
                    ";		
                    $groupBY="w.szCity";	
		}
		if($szCity!='')
		{
                    $sql .="
                            AND
                                w.szCity='".mysql_escape_custom($szCity)."'		
                    ";
                    $groupBY="w.id";
		}
                if($iWarehouseType>0)
                {
                    $sql .=" AND w.iWarehouseType = '".mysql_escape_custom($iWarehouseType)."' ";
                }
		if($groupBY=='')
		{
                    $groupBY="w.idCountry";
		}
		if($idForwarder!='')
		{
                    $forwarderString = "AND w.idForwarder='".(int)$idForwarder."' ";
		}
		else
		{
                    $forwarderString = '';
		}
                
		$query="
                    SELECT
                        w.id AS warehouseId,
                        w.szWareHouseName,
                        w.szCity,
                        c.szCountryName,
                        c.id
                    FROM
                        ".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." AS hpm
                    INNER JOIN
                        ".__DBC_SCHEMATA_WAREHOUSES__." AS w
                    ON
                        hpm.idWarehouse=w.id
                    INNER JOIN
                        ".__DBC_SCHEMATA_COUNTRY__." AS c
                    ON
                        c.id=w.idCountry
                    WHERE
                        hpm.iActive='1'
                        ".$forwarderString."
                    AND
                            hpm.idHaulageModel='".(int)$idHaulageModel."'
                    AND
                            w.iActive='1'
                            ".$sql." 		
                    GROUP BY
                            $groupBY
                    ORDER BY
                            c.szCountryName ASC,
                            w.szCity ASC,
                            w.szWareHouseName ASC
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
                    if($this->iNumRows>0)
                    {
                        while($row=$this->getAssoc($result))
                        {
                            $ret_ary[]=$row;
                        }
                        return $ret_ary;
                    }
                    else
                    {
                            return array();
                    }
		}
		else
		{
			return array();
		}
	}
	
	function copyZoneData($data)
	{
		//print_r($data);
		if(count($data['szCopyZone'])<=0)
		{
			$this->addError( "szCopyZone" , t($this->t_base.'messages/atleast_one_zone_line_is_selected.'));
		}
		
		if(count($data['szCopyZonePricing'])>count($data['szCopyZone']))
		{
			$this->addError( "szCopyZone" , t($this->t_base.'pricing_data_count_greater_than_zone_data_count'));
		}
		$existZoneName=array();
		if(!empty($data['szCopyZone']))
		{
			$checkflag=false;
			foreach($data['szCopyZone'] as $szCopyZoneArr)
			{
				$zoneName=$this->getHaulageModelName($szCopyZoneArr);
				if($this->isZoneNameExist(0,$zoneName,$data['idZoneWarehouse'],$data['idZoneDirection'],$data['idZoneHaulageModel']))
				{
					$existZoneName[]=$zoneName;
				}
				
			}
		}
				
		if(!empty($existZoneName))
		{
			$existZoneNameStr=implode(",",$existZoneName);
			$this->addError( "szCopyZone" , t($this->t_base.'messages/already_defined_zone_msg'));
		}
		//exit if error exist.
		if ($this->error === true)
		{
			return false;
		}
		
		if(!empty($data['szCopyZone']))
		{		
			foreach($data['szCopyZone'] as $szCopyZoneArr)
			{
				$zoneArr[0]=$this->loadZone($szCopyZoneArr);
				
				$query="
					SELECT
						MAX(iPriority) as iPriority
					FROM
						".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
					WHERE
						idWarehouse='".(int)$data['idZoneWarehouse']."'
					AND
						iDirection ='".(int)$data['idZoneDirection']."'
					AND
						idHaulageModel='".(int)$data['idZoneHaulageModel']."'
				";
				$result=$this->exeSQL($query);
				$row=$this->getAssoc($result);
				$iPriority=$row['iPriority']+1;
				
				$query="
					INSERT INTO
						".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
					(
						idWarehouse,
						iDirection,
						idHaulageModel,
						iPriority,
						szName,
						idHaulageTransitTime,
						fWmFactor,
						fFuelPercentage,
						iActive
					)
						VALUES
					(
						'".(int)$data['idZoneWarehouse']."',
						'".(int)$data['idZoneDirection']."',
						'".(int)$data['idZoneHaulageModel']."',
						'".(int)$iPriority."',
						'".mysql_escape_custom($zoneArr[0]['szName'])."',
						'".(int)$zoneArr[0]['idHaulageTransitTime']."',
						'".(float)$zoneArr[0]['fWmFactor']."',
						'".(float)$zoneArr[0]['fFuelPercentage']."',
						'1'
					)
				";
				if($result=$this->exeSQL($query))
				{
					$id= $this->iLastInsertID ;
					
					if(!empty($data['szCopyZonePricing']))
					{
						if(!empty($data['szCopyZonePricing']) && in_array($szCopyZoneArr,$data['szCopyZonePricing']))
						{
							$pricingListArr=array();
							$pricingListArr=$this->getHaulagePricingZoneDetailData($szCopyZoneArr);
							//print_r($pricingListArr);
							if(!empty($pricingListArr))
							{
								foreach($pricingListArr  as $pricingListArrs)
								{
									$query="
										INSERT INTO
											".__DBC_SCHEMATA_HAULAGE_PRICING__."
										(	
											idHaulagePricingModel,
											iUpToKg,
											fPricePer100Kg,
											fMinimumPrice,
											fPricePerBooking,
											idCurrency,
											dtCreatedOn,
                                                                                        iActive
										)
											VALUES
										(
											'".(int)$id."',
											'".(int)$pricingListArrs['iUpToKg']."',
											'".(float)$pricingListArrs['fPricePer100Kg']."',
											'".(float)$pricingListArrs['fMinimumPrice']."',
											'".(float)$pricingListArrs['fPricePerBooking']."',
											'".(int)$pricingListArrs['idCurrency']."',
											NOW(),
                                                                                        1
										)
									";
									//echo $query."<br/><br/>";
									$result = $this->exeSQL( $query );
								}
							}
						}
					}
					
					$latlongReplaceArr = $this->getVerticesOfZone($szCopyZoneArr);
					if(!empty($latlongReplaceArr))
					{
						$ctr=1;
						foreach($latlongReplaceArr as $latlongReplaceArrs)
						{
														
							$query="
								INSERT INTO
									".__DBC_SCHEMATA_HAULAGE_ZONE__."
								(
									szLatitude,
									szLongitude,
									idHaulagePricingModel,
									iVertexSequence,
									iActive 	
								)
									VALUES
								(
									'".mysql_escape_custom($latlongReplaceArrs['szLatitude'])."',
									'".mysql_escape_custom($latlongReplaceArrs['szLongitude'])."',
									'".(int)$id."',
									'".(int)$ctr."',
									'1'
								)";
							$result = $this->exeSQL( $query );
							++$ctr;
						}
					}
				}
			}
			return true;
		}
	}
	
	function getVerticesOfZone($idHaulagePricingModel)
	{
		$ret_ary=array();
		if($idHaulagePricingModel>0)
		{
			$query="
				SELECT
					szLatitude,
					szLongitude
				FROM
					".__DBC_SCHEMATA_HAULAGE_ZONE__."
				WHERE
					idHaulagePricingModel='".(int)$idHaulagePricingModel."'
				ORDER BY
					iVertexSequence ASC
			";
			//echo "<br>".$query."<br>";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					while($row=$this->getAssoc($result))
					{
						$ret_ary[]=$row;
					}
					return $ret_ary;
				}
				else
				{
					return array();
				}
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function getSetOfPostCode($idHaulageModel,$flag=false)
	{
		$ret_ary=array();
		if($idHaulageModel>0)
		{
			$query="
				SELECT
					szPostCode
				FROM
					".__DBC_SCHEMATA_HAULAGE_POSTCODE__."
				WHERE
					idHaulagePricingModel='".(int)$idHaulageModel."'
				ORDER BY
					szPostCode ASC
			";
			//echo "<br>".$query."<br>";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					while($row=$this->getAssoc($result))
					{
						$ret_ary[]=$row['szPostCode'];
					}
					if($flag)
					{
						return $ret_ary;
					}
					else
					{
						$total=count($ret_ary);
						$str='';
						if(!empty($ret_ary))
						{
							$str.='';
							$ctr=0;
							foreach($ret_ary as $ret_arys)
							{
									if($ctr==0)
									{
										$str .= $ret_arys;
									}
									else if($ctr>0 && $ctr==($total-1))
									{
										$str .= ' and '.htmlentities($ret_arys,ENT_COMPAT, "UTF-8");
									}
									else
									{
										$str .= ', '.htmlentities($ret_arys,ENT_COMPAT, "UTF-8");
									}
									$ctr++;	
							}
						}
						
						return $str;
					}
				}
				else
				{
					return '';
				}
			}
			else
			{
				return '';
			}
		}
		else
		{
			return '';
		}
	}
	
	function deleteHaulagePricingPostCodeData($id,$idWarehouse,$idHaulageModel,$idDirection)
	{
		if((int)$id>0)
		{
			$query="
				SELECT
					iPriority
				FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
				WHERE
					id='".(int)$id."'
			";
			//echo $query."<br/>";
			$result=$this->exeSQL($query);
			$row=$this->getAssoc($result);
			$iPriority=$row['iPriority'];
			
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
				WHERE
					id='".(int)$id."'				
			";
			$result=$this->exeSQL($query);
			
			
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING__."
				WHERE
					idHaulagePricingModel='".(int)$id."'				
			";
			$result=$this->exeSQL($query);
			
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_HAULAGE_POSTCODE__."
				WHERE
					idHaulagePricingModel='".(int)$id."'				
			";
			$result=$this->exeSQL($query);
			
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_HAULAGE_COUNTRY__."
				WHERE
					idHaulagePricingModel='".(int)$id."'				
			";
			$result=$this->exeSQL($query);
			
			
			$query="
				UPDATE
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
				SET
					iPriority=iPriority-1
				WHERE
					id!='".(int)$id."'
				AND
					iPriority>='".(int)($iPriority+1)."'
				AND
					idWarehouse='".(int)$idWarehouse."'
				AND
					iDirection='".(int)$idDirection."'
				AND
					idHaulageModel='".(int)$idHaulageModel."'	
				";
			//echo $query;
			$result = $this->exeSQL( $query );
		}
		else
		{
			return false;
		}
	}
	
	function addPostCode($data)
	{
		
		$kWHSSearch = new cWHSSearch();
		$kWHSSearch->load($data['idWarehouse']);
	
		$maxWMFactor=$kWHSSearch->getManageMentVariableByDescription('__MAX_W/M_FACTOR__');
		$miniWMFactor=$kWHSSearch->getManageMentVariableByDescription('__MINI_W/M_FACTOR__');
		$maxFuel=$kWHSSearch->getManageMentVariableByDescription('__MAX_FUEL__');
		$miniFuel=$kWHSSearch->getManageMentVariableByDescription('__MINI_FUEL__');
		
		$this->set_szName(trim(sanitize_all_html_input($data['szName'])));
		$this->set_fFuelPercentage(trim(sanitize_all_html_input(urlencode($data['fFuelPercentage']))),$maxFuel,$miniFuel);
		$this->set_idTransitTime(trim(sanitize_all_html_input($data['idTransitTime'])));
		$this->set_fWmFactor(trim(sanitize_all_html_input($data['fWmFactor'])),$maxWMFactor,$miniWMFactor);
		$this->set_idCountry(trim(sanitize_all_html_input($data['szCountry'])));
		$this->set_szPostCodeStr(trim(sanitize_all_html_input($data['szPostCodeStr1'])));
		
		$postCodeNewArr=array();
		
		if($this->isZoneNameExist(0,$this->szName,$data['idWarehouse'],$data['idDirection'],$data['idHaulageModel']))
		{
			$this->addError( "szName" , t($this->t_base.'messages/postcode_already_exist_for_warehouse')." ".$kWHSSearch->szWareHouseName.", ".t($this->t_base.'messages/postcode_already_exist_for_warehouse_1'));
		}
		
		
		if(!empty($this->szPostCodeStr))
		{
			$postcodeArr=explode(",",trim($this->szPostCodeStr));
			foreach($postcodeArr as $postcodeArrs)
			{
			    if (preg_match('/[\'^�$%&()}{@#~><>,|=_+�]/', $postcodeArrs))
			    {
   					 $this->addError( "szPostCodeStr" ,"These special characters are not allowed in  ".$postcodeArrs.". Special characters allowed (*?-).");
				}
				else
				{
					$result_ary = explode("*",$postcodeArrs);
					if(count($result_ary)>2)
					{
						$this->addError( "szPostCodeStr" ,"Wrong format in ".$postcodeArrs.".");
					}
					$result_ary_1 = explode("-",$postcodeArrs);
					//echo count($result_ary_1[0]);
					if(count($result_ary_1)>2)
					{
						if($result_ary_1[0]=='' || $result_ary_1[1]=='')
						{
							$this->addError( "szPostCodeStr" ,"Wrong format in ".$postcodeArrs.".");
						}
					}
				}
				
				if(in_array($postcodeArrs,$postCodeNewArr))
				{
					$this->addError( "szPostCodeStr" ,$postcodeArrs." already taken.");
				}
				else
				{
					$postCodeNewArr[]=$postcodeArrs;
				}
				
			}
		}
		
		//exit if error exist.
		if ($this->error === true)
		{
			return false;
		}
		if(!empty($this->szPostCodeStr))
		{
			$postcodeArr=explode(",",$this->szPostCodeStr);
		}
		
		$query="
			SELECT
				MAX(iPriority) as iPriority
			FROM
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
			WHERE
				idWarehouse='".(int)$data['idWarehouse']."'
			AND
				iDirection ='".(int)$data['idDirection']."'
			AND
				idHaulageModel='".(int)$data['idHaulageModel']."'
		";
		$result=$this->exeSQL($query);
			$row=$this->getAssoc($result);
			$iPriority=$row['iPriority']+1;
		
		$query="
			INSERT INTO
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
			(
				idWarehouse,
				iDirection,
				idHaulageModel,
				iPriority,
				szName,
				idHaulageTransitTime,
				fWmFactor,
				fFuelPercentage,
				iActive,
				idCountry
			)
				VALUES
			(
				'".(int)$data['idWarehouse']."',
				'".(int)$data['idDirection']."',
				'".(int)$data['idHaulageModel']."',
				'".(int)$iPriority."',
				'".mysql_escape_custom($this->szName)."',
				'".(int)$this->idTransitTime."',
				'".(int)$this->fWmFactor."',
				'".(float)$this->fFuelPercentage."',
				'1',
				'".(int)$this->idCountry."'
			)
		";
		if($result=$this->exeSQL($query))
		{
			$id= $this->iLastInsertID ;
			
			$query="
				SELECT
					id,
					iPriority
				FROM
					".__DBC_SCHEMATA_CFS_MODEL_MAPPING__."
				WHERE
					idWarehouse='".(int)$data['idWarehouse']."'
				AND
					idHaulageModel='".(int)$data['idHaulageModel']."'
				AND
					iDirection ='".(int)$data['idDirection']."'
				AND
					iActive='0'
			";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
				
					$row=$this->getAssoc($result);
					$oldPriority=$row['iPriority'];
					
					$query="
						SELECT
							MAX(iPriority) AS maxPriority
						FROM
							".__DBC_SCHEMATA_CFS_MODEL_MAPPING__."
						WHERE
							idWarehouse='".(int)$data['idWarehouse']."'
						AND
							iDirection ='".(int)$data['idDirection']."'
						AND
							iActive='1'
						GROUP BY
							idWarehouse
					";
					$result=$this->exeSQL($query);
					
					$row=$this->getAssoc($result);
					$maxPriority=$row['maxPriority'];
					
					$newPrority=$maxPriority+1;
					
					$query="
						UPDATE
							".__DBC_SCHEMATA_CFS_MODEL_MAPPING__."
						SET
							iPriority=iPriority+1
						WHERE
							idWarehouse='".(int)$data['idWarehouse']."'
						AND
							idHaulageModel<>'".(int)$data['idHaulageModel']."'
						AND
							iDirection ='".(int)$data['idDirection']."'
						AND
							iPriority>='".(int)$newPrority."'
						AND
							iPriority<'".(int)$oldPriority."'
					";
					//echo $query;
					$result=$this->exeSQL($query);
					
					$query="
						UPDATE
							".__DBC_SCHEMATA_CFS_MODEL_MAPPING__."
						SET
							iActive='1',
							iPriority='".(int)$newPrority."'
						WHERE
							idWarehouse='".(int)$data['idWarehouse']."'
						AND
							idHaulageModel='".(int)$data['idHaulageModel']."'
						AND
							iDirection ='".(int)$data['idDirection']."'
					";
					//echo $query;
					$result=$this->exeSQL($query);				
				}
			}
			
			$query="
				INSERT INTO
					".__DBC_SCHEMATA_HAULAGE_COUNTRY__."
				(
					idCountry,
					idHaulagePricingModel,
					idForwarder,
					idWarehouse,
					idDirection
				)
					VALUES
				(
					'".(int)$this->idCountry."',
					'".(int)$id."',
					'".(int)$kWHSSearch->idForwarder."',
					'".(int)$data['idWarehouse']."',
					'".(int)$data['idDirection']."'
					
				)";
				$result = $this->exeSQL( $query );
				$countryData[]=$this->idCountry;
				//$this->addWarehouseCountries($data['idWarehouse'],$countryData);
			if(!empty($postcodeArr))
			{
				$ctr=1;
				foreach($postcodeArr as $postcodeArrs)
				{
					
					
					$query="
						INSERT INTO
							".__DBC_SCHEMATA_HAULAGE_POSTCODE__."
						(
							szPostCode,
							idHaulagePricingModel
						)
							VALUES
						(
							'".mysql_escape_custom($postcodeArrs)."',
							'".(int)$id."'
						)";
					$result = $this->exeSQL( $query );
					++$ctr;
				}
			}
			return $id;
		}
		else
		{
			return 0;
		}
	}
	
	
	function loadPostCode($id)
	{
		if((int)$id>0)
		{
			$query="
				SELECT
					id,
					szName,
					fWmFactor,
					fFuelPercentage,
					idHaulageTransitTime,
					idCountry
				FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
				WHERE
					id='".(int)$id."'
			";
			//echo $query."<br/>";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					return $row;
				}
				else
				{
					return array();
				}
				
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function updatePostCode($data)
	{
		$kWHSSearch = new cWHSSearch();
		$kWHSSearch->load($data['idWarehouse']);
	
		$maxWMFactor=$kWHSSearch->getManageMentVariableByDescription('__MAX_W/M_FACTOR__');
		$miniWMFactor=$kWHSSearch->getManageMentVariableByDescription('__MINI_W/M_FACTOR__');
		$maxFuel=$kWHSSearch->getManageMentVariableByDescription('__MAX_FUEL__');
		$miniFuel=$kWHSSearch->getManageMentVariableByDescription('__MINI_FUEL__');
		
		$this->set_szName(trim(sanitize_all_html_input($data['szName'])));
		$this->set_fFuelPercentage(trim(sanitize_all_html_input(urlencode($data['fFuelPercentage']))),$maxFuel,$miniFuel);
		$this->set_idTransitTime(trim(sanitize_all_html_input($data['idTransitTime'])));
		$this->set_fWmFactor(trim(sanitize_all_html_input($data['fWmFactor'])),$maxWMFactor,$miniWMFactor);
		$this->set_idCountry(trim(sanitize_all_html_input($data['szCountry'])));
		$this->set_szPostCodeStr(trim(sanitize_all_html_input($data['szPostCodeStr1'])));
		
		$postCodeNewArr=array();
	
		if($this->isZoneNameExist($data['id'],$this->szName,$data['idWarehouse'],$data['idDirection'],$data['idHaulageModel']))
		{
			$this->addError( "szName" , t($this->t_base.'messages/postcode_already_exist_for_warehouse')." ".$kWHSSearch->szWareHouseName.", ".t($this->t_base.'messages/postcode_already_exist_for_warehouse_1'));
		}
		
		if(!empty($this->szPostCodeStr))
		{
			$postcodeArr=explode(",",trim($this->szPostCodeStr));
			foreach($postcodeArr as $postcodeArrs)
			{
			    if (preg_match('/[\'^�$%&()}{@#~><>,|=_+�]/', $postcodeArrs))
			    {
   					 $this->addError( "szPostCodeStr" ,"These special characters are not allowed in  ".$postcodeArrs.". Special characters allowed (*?-).");
				}
				else
				{
					$result_ary = explode("*",$postcodeArrs);
					if(count($result_ary)>2)
					{
						$this->addError( "szPostCodeStr" ,"Wrong format in ".$postcodeArrs.".");
					}
					$result_ary_1 = explode("-",$postcodeArrs);
					//echo count($result_ary_1[0]);
					if(count($result_ary_1)>2)
					{
						if($result_ary_1[0]=='' || $result_ary_1[1]=='')
						{
							$this->addError( "szPostCodeStr" ,"Wrong format in ".$postcodeArrs.".");
						}
					}
				}
				
				if(in_array($postcodeArrs,$postCodeNewArr))
				{
					$this->addError( "szPostCodeStr" ,$postcodeArrs." already taken.");
				}
				else
				{
					$postCodeNewArr[]=$postcodeArrs;
				}
				
			}
		}
		
		
		//exit if error exist.
		if ($this->error === true)
		{
			return false;
		}
		if(!empty($this->szPostCodeStr))
		{
			$postcodeArr=explode(",",$this->szPostCodeStr);
		}
		
		
		$query="
			UPDATE
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
			SET	
				szName='".mysql_escape_custom($this->szName)."',
				fWmFactor='".(int)$this->fWmFactor."',
				fFuelPercentage='".(float)$this->fFuelPercentage."',
				idHaulageTransitTime='".(int)$this->idTransitTime."',
				idCountry='".(int)$this->idCountry."'
			WHERE
				id='".(int)$data['id']."'
		";
		if( ( $result = $this->exeSQL( $query ) ) )
		{
		
			$query="
				UPDATE
					".__DBC_SCHEMATA_HAULAGE_COUNTRY__."
				SET
					idCountry='".(int)$this->idCountry."'
				WHERE
					idHaulagePricingModel='".(int)$data['id']."'
			";
			$result = $this->exeSQL( $query );
			
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_HAULAGE_POSTCODE__."
				WHERE
					idHaulagePricingModel='".(int)$data['id']."'
			";
			$result = $this->exeSQL( $query );
			if(!empty($postcodeArr))
			{
				$ctr=1;
				foreach($postcodeArr as $postcodeArrs)
				{
					
					
					$query="
						INSERT INTO
							".__DBC_SCHEMATA_HAULAGE_POSTCODE__."
						(
							szPostCode,
							idHaulagePricingModel
						)
							VALUES
						(
							'".mysql_escape_custom($postcodeArrs)."',
							'".(int)$data['id']."'
						)";
					$result = $this->exeSQL( $query );
					++$ctr;
				}
			}
			$countryData[]=$this->idCountry;
			//$this->addWarehouseCountries($data['idWarehouse'],$countryData);
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
	function deleteHaulagePricingCityData($id,$idWarehouse,$idHaulageModel,$idDirection)
	{
		if((int)$id>0)
		{
			$query="
				SELECT
					iPriority
				FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
				WHERE
					id='".(int)$id."'
			";
			//echo $query."<br/>";
			$result=$this->exeSQL($query);
			$row=$this->getAssoc($result);
			$iPriority=$row['iPriority'];
			
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
				WHERE
					id='".(int)$id."'				
			";
			$result=$this->exeSQL($query);
			
			
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING__."
				WHERE
					idHaulagePricingModel='".(int)$id."'				
			";
			$result=$this->exeSQL($query);
			
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_HAULAGE_COUNTRY__."
				WHERE
					idHaulagePricingModel='".(int)$id."'				
			";
			$result=$this->exeSQL($query);
					
			$query="
				UPDATE
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
				SET
					iPriority=iPriority-1
				WHERE
					id!='".(int)$id."'
				AND
					iPriority>='".(int)($iPriority+1)."'
				AND
					idWarehouse='".(int)$idWarehouse."'
				AND
					iDirection='".(int)$idDirection."'
				AND
					idHaulageModel='".(int)$idHaulageModel."'	
				";
			//echo $query;
			$result = $this->exeSQL( $query );
		}
		else
		{
			return false;
		}
	}
	
	function getAllVerticesOfZone_cronjob($idHaulagePricingModel)
	{
		if($idHaulagePricingModel>0)
		{
			$query="
				SELECT
					szLatitude,
					szLongitude
				FROM
					".__DBC_SCHEMATA_HAULAGE_ZONE__."
				WHERE
					idHaulagePricingModel='".(int)$idHaulagePricingModel."'
				ORDER BY
					iVertexSequence ASC
			";
			//echo "<br>".$query."<br>";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$zoneVertexAry = array();
					$ctr=0;
					$szMaxLatitude = 0.0;
					$szMinLatitude = 0.0;
					
					$szMaxLongitude = 0.0;
					$szMinLongitude = 0.0;
					
					while($row=$this->getAssoc($result))
					{
						if($ctr==0)
						{
							$tempVertexAry[]=$row;				

							/*
								Initializing max and min latitude variables with first row data.
							*/
							$szMaxLatitude = $row['szLatitude'];
							$szMinLatitude = $row['szLatitude'];	
								
							/*
								Initializing max and min longitude variables with first row data.
							*/
							
							$szMaxLongitude =  $row['szLongitude'];
							$szMinLongitude =  $row['szLongitude'];
						}
						
						/*
							fetching maximum Latitude 
						*/
						if($szMaxLatitude < $row['szLatitude'])
						{
							$this->szMaxLatitude = $row['szLatitude'];
							$szMaxLatitude = $row['szLatitude'];
						}
						else
						{
							$this->szMaxLatitude = $szMaxLatitude;
						}
						
						/*
							fetching minimum Latitude 
						*/
						if($szMinLatitude > $row['szLatitude'])
						{
							$this->szMinLatitude = $row['szLatitude'];
							$szMinLatitude = $row['szLatitude'];
						}
						else
						{
							$this->szMinLatitude = $szMinLatitude;
						}
						
						/*
							fetching maximum Longitude 
						*/
						if($szMaxLongitude < $row['szLongitude'])
						{
							$this->szMaxLongitude = $row['szLongitude'];
							$szMaxLongitude = $row['szLongitude'];
						}
						else
						{
							$this->szMaxLongitude = $szMaxLongitude;
						}
						
						/*
							fetching minimum Longitude 
						*/
						if($szMinLongitude > $row['szLongitude'])
						{
							$this->szMinLongitude = $row['szLongitude'];
							$szMinLongitude = $row['szLongitude'];
						}
						else
						{
							$this->szMinLongitude = $szMinLongitude;
						}
						
						$zoneVertexAry[$ctr]=$row;
						$ctr++;
					}
					
					/**
					* adding first row data to the last of array to build a complete loop circle.
					*/
					if(!empty($zoneVertexAry) && !empty($tempVertexAry))
					{
						$zoneVertexAry = array_merge($zoneVertexAry,$tempVertexAry);
					}
					if(!empty($zoneVertexAry))
					{
						$fMaxLength = 0;
						for($i=0;$i<(count($zoneVertexAry)-1);$i++)
						{
							$j = $i+1 ;
							$longitude_diff = $zoneVertexAry[$j]['szLongitude'] - $zoneVertexAry[$i]['szLongitude'] ;
							$latitude_diff = $zoneVertexAry[$j]['szLatitude'] - $zoneVertexAry[$i]['szLatitude'];
							$length = sqrt(($latitude_diff*$latitude_diff)+($longitude_diff*$longitude_diff));
							$fMaxLength = $fMaxLength + $length ;
							$zoneVertexAry[$i]['iLength'] = $length ;
						}						
						$this->fVerticesMaxLength_degree = $fMaxLength ;
						$this->fVerticesMaxLength_km = $fMaxLength * 111 ;
					}
					return $zoneVertexAry;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function isLocationExistsInZone_cronjob($data)
	{
            if(!empty($data))
            { 
                $idHaulagePricingModel = $data['idHaulagePricingModel'];
                $idCountry = $data['idCountry'];

                $zoneVertexAry = $this->getAllVerticesOfZone_cronjob($idHaulagePricingModel);

                $this->szGlobalMaxLatitute = $this->szMaxLatitude;
                $this->szGlobalMinLatitute = $this->szMinLatitude; 
                $this->szGlobalMaxLongitute = $this->szMaxLongitude;
                $this->szGlobalMinLongitute = $this->szMinLongitude;

                $this->zoneVertexGlobalAry = $zoneVertexAry ;
                $kWhsSearch = new cWHSSearch();	
                $iDegreeKmFactor = $kWhsSearch->getManageMentVariableByDescription('__NUM_POINT_TO_TEST__');		
                $this->iNumPointsToTest = $iDegreeKmFactor ;
                if(!empty($zoneVertexAry))
                {
                    $szLatitude = $zoneVertexAry[0]['szLatitude'];
                    $szLongitude = $zoneVertexAry[0]['szLongitude'];

                    if($this->fVerticesMaxLength_km > 100000)
                    {
                        //echo "<br /> Maxlength must be less then or equal to 100000 Km";
                        //return false;
                    } 
                    if($iDegreeKmFactor>0)
                    {
                        $Y_axis = $this->fVerticesMaxLength_degree/$iDegreeKmFactor;
                    }
                    else
                    {
                        $Y_axis = 0;
                    }
                    $this->Y_axis = $Y_axis;
				
                    if(($this->szMaxLatitude < $szLatitude) || ($this->szMinLatitude > $szLatitude ))
                    {
                        //echo "<br> Test1 failed for country id = ".$idCountry." and idHaulagePricingModel = ".$idHaulagePricingModel.". Not exists in latitude range";
                        //return false;
                    }
                    if(($this->szMaxLongitude < $szLongitude) || ($this->szMinLongitude > $szLongitude ))
                    {
                        //echo "<br> Test1 succeed <br />";
                        //echo "<br>Test2 failed for country id = ".$idCountry." and idHaulagePricingModel = ".$idHaulagePricingModel.". Not exists in longitude range";
                        //return false;
                    }
                                
                    $verticesDifferenceAry[0]['Alpha'] = 0;
                    $verticesDifferenceAry[0]['Beta'] = $szLongitude;
                    $verticesDifferenceAry[0]['CrossX'] = '';
                    $verticesDifferenceAry[0]['MinX'] = $szLatitude;
                    $verticesDifferenceAry[0]['MaxX'] = $this->szMaxLatitude;
                    $j=1;
                    for($i=0;$i<(count($zoneVertexAry)-1);$i++)
                    {
                        $latlongDifference = ($zoneVertexAry[$j]['szLatitude'] - $zoneVertexAry[$i]['szLatitude']);
                        if($latlongDifference!=0)
                        {
                            $verticesDifferenceAry[$j]['Alpha'] = (($zoneVertexAry[$j]['szLongitude'] - $zoneVertexAry[$i]['szLongitude']) / $latlongDifference);
                        }
                        else
                        {
                            $verticesDifferenceAry[$j]['Alpha'] = 0;
                        }
                        $verticesDifferenceAry[$j]['Beta'] = $zoneVertexAry[$i]['szLongitude'] - ($verticesDifferenceAry[$j]['Alpha'] * $zoneVertexAry[$i]['szLatitude']) ;

                        if($verticesDifferenceAry[$j]['Alpha']!=0)
                        {
                            $verticesDifferenceAry[$j]['CrossX'] = ($szLongitude - $verticesDifferenceAry[$j]['Beta']) / $verticesDifferenceAry[$j]['Alpha'] ;
                        }
                        else
                        {
                            $verticesDifferenceAry[$j]['CrossX'] = 0;
                        }

                        if($zoneVertexAry[$i]['szLatitude'] < $zoneVertexAry[$j]['szLatitude'])
                        {
                            $verticesDifferenceAry[$j]['MinX'] = $zoneVertexAry[$i]['szLatitude'] ;
                        }
                        else
                        {
                            $verticesDifferenceAry[$j]['MinX'] = $zoneVertexAry[$j]['szLatitude'];
                        }	

                        if($zoneVertexAry[$i]['szLatitude'] > $zoneVertexAry[$j]['szLatitude'])
                        {
                            $verticesDifferenceAry[$j]['MaxX'] = $zoneVertexAry[$i]['szLatitude'] ;
                        }
                        else
                        {
                            $verticesDifferenceAry[$j]['MaxX'] = $zoneVertexAry[$j]['szLatitude'];
                        }	

                        if($verticesDifferenceAry[$j]['MinX'] > $szLatitude)
                        {
                            $maximumMinX = $verticesDifferenceAry[$j]['MinX'] ;
                        }
                        else
                        {
                            $maximumMinX = $szLatitude ;
                        }

                        if($verticesDifferenceAry[$j]['MaxX'] > $this->szMaxLatitude)
                        {
                            $minimumManX = $this->szMaxLatitude ;
                        }
                        else
                        {
                            $minimumManX = $verticesDifferenceAry[$j]['MaxX'];
                        }

                        if(($verticesDifferenceAry[$j]['CrossX'] > $maximumMinX) && ($verticesDifferenceAry[$j]['CrossX'] < $minimumManX ))
                        {
                            $succesfully_found_counter ++;
                        }

                        if($zoneVertexAry[$i]['szLatitude'] < $zoneVertexAry[$j]['szLatitude'])
                        {
                            $verticesDifferenceAry[$j]['X_Axis'] = sqrt(($Y_axis*$Y_axis)/(1+($verticesDifferenceAry[$j]['Alpha']*$verticesDifferenceAry[$j]['Alpha'])));
                        }
                        else
                        {
                            $verticesDifferenceAry[$j]['X_Axis'] = -(sqrt(($Y_axis*$Y_axis)/(1+($verticesDifferenceAry[$j]['Alpha']*$verticesDifferenceAry[$j]['Alpha']))));
                        }	
                        $j++;
                    }
                    /*					
                    if($zoneVertexAry[$j]['szLatitude'] < $zoneVertexAry[0]['szLatitude'])
                    {
                            $verticesDifferenceAry[$j]['X_Axis'] = sqrt(($Y_axis*$Y_axis)/(1+($verticesDifferenceAry[0]['Alpha']*$verticesDifferenceAry[0]['Alpha'])));
                    }
                    else
                    {
                            $verticesDifferenceAry[$j]['X_Axis'] = -(sqrt(($Y_axis*$Y_axis)/(1+($verticesDifferenceAry[0]['Alpha']*$verticesDifferenceAry[0]['Alpha']))));
                    }			
                    */
                    if($succesfully_found_counter%2 == 1)
                    {
                            //echo "<br> country id = ".$idCountry." exists in idHaulagePricingModel = ".$idHaulagePricingModel;

                    }
                    else
                    {
                            //echo "<br> country id = ".$idCountry." does not exists in idHaulagePricingModel = ".$idHaulagePricingModel;
                    }
                    $this->verticesDifferenceAry = $verticesDifferenceAry ;
                    if(!empty($verticesDifferenceAry))
                    {
                        $k=0;
                        $pointsAry = array();
                        $iNumrequest = 0; 

                        foreach($verticesDifferenceAry as $verticesDifferenceArys)
                        {
                            if($Y_axis>0)
                            {
                                $loop_counter = ceil($zoneVertexAry[$k]['iLength']/$Y_axis);

                                if((int)$loop_counter > 0)
                                {
                                    $pointsAry[0]['szLatitude'][0] = $zoneVertexAry[0]['szLatitude'];
                                    $pointsAry[0]['szLongitude'][0] = $zoneVertexAry[0]['szLongitude'];

                                    for($i=1;$i<$loop_counter;$i++)
                                    {
                                        $pointsAry[$k]['szLatitude'][$i] = $pointsAry[$k]['szLatitude'][$i-1] + $verticesDifferenceAry[$k+1]['X_Axis'] ; 
                                        $pointsAry[$k]['szLongitude'][$i] = $pointsAry[$k]['szLongitude'][$i-1] + ($verticesDifferenceAry[$k+1]['Alpha'] * $verticesDifferenceAry[$k+1]['X_Axis']);

                                        if($i>0 && empty($pointsAry[$i]['szLatitude'][0]))
                                        {
                                            $pointsAry[$i]['szLatitude'][0] = $zoneVertexAry[$i]['szLatitude'];
                                            $pointsAry[$i]['szLongitude'][0] = $zoneVertexAry[$i]['szLongitude'];
                                        }
                                        $iNumrequest++ ; 
                                    }
                                    $k++;
                                }
                            }				
                        }
                        $this->iTotalNumRequest = $iNumrequest ;
                    }
                    return $pointsAry ; 
                }
                else
                {
                        return false;
                }
            }
	}
	
	function getAllUpdatableZones()
	{
		$query="
			SELECT
				DISTINCT idHaulagePricingModel
			FROM
				".__DBC_SCHEMATA_HAULAGE_ZONE__."
			WHERE
				iLatestUpdated = '1'
		";
		if($result= $this->exeSQL($query))
		{
			$ctr = 0;
			$haulageZoneIdAry = array();
			while($row=$this->getAssoc($result))
			{
				$haulageZoneIdAry[$ctr]['idHaulagePricingModel'] = $row['idHaulagePricingModel'];
				$ctr++;
			}
			return $haulageZoneIdAry;
		}
	}
	
	function getAllActiveCountry()
	{
		$query="
			SELECT
				id,
				szLatitude,
				szLongitude,
				szCountryISO,
				szCountryName	
			FROM
				".__DBC_SCHEMATA_COUNTRY__."
			WHERE
				iActive = '1'
		";
		if($result= $this->exeSQL($query))
		{
			$ctr = 0;
			$haulageCountryAry = array();
			while($row=$this->getAssoc($result))
			{
				$haulageCountryAry[$ctr] = $row;
				$ctr++;
			}
			return $haulageCountryAry;
		}
	}
	
	function addHaulageZonesCountries($data)
	{
            if(!empty($data))
            {  
                $haulageModelDetails = array();
                $haulageModelDetails = $this->getHaulageModelDetails($data['idHaulagePricingModel']);
                                
                if(!empty($haulageModelDetails))
                {
                    $haulageInputDataAry = array();
                    $haulageInputDataAry['idHaulagePricingModel'] = $data['idHaulagePricingModel'];
                    $haulageInputDataAry['idCountry'] = $data['idCountry'];
                    $haulageInputDataAry['idWarehouse'] = $haulageModelDetails['idWarehouse'];
                    $haulageInputDataAry['idDirection'] = $haulageModelDetails['iDirection'];

                    if($this->isHaulageCountryExists($haulageInputDataAry))
                    { 
                        return false;
                    }
                    
                    $query="
                        INSERT INTO
                            ".__DBC_SCHEMATA_HAULAGE_COUNTRY__."
                        (
                            idHaulagePricingModel,
                            idCountry,
                            idForwarder,
                            idWarehouse,
                            idDirection,
                            iActive					
                        )
                        VALUES
                        (
                            '".(int)$data['idHaulagePricingModel']."',
                            '".(int)$data['idCountry']."',
                            '".mysql_escape_custom(trim($haulageModelDetails['idForwarder']))."',
                            '".(int)$haulageModelDetails['idWarehouse']."',
                            '".(int)$haulageModelDetails['iDirection']."',
                            '1'
                        )
                    ";	
                    //echo "<br/>".$query."<br> ";
                    if($result=$this->exeSQL($query))
                    {
                        $this->idWarehouse = (int)$haulageModelDetails['idWarehouse'] ; 
                    }
                    else
                    {
                        return false;
                    }
                }
            }
	}
	
	function addHaulageWarehouseCountries($idCountry,$idWarehouse)
	{
		if($idCountry>0 && $idWarehouse>0)
		{
			if($this->isWarehouseCountryExists($idWarehouse,$idCountry))
			{
				return false;
			}
			
			$query="
				INSERT INTO
					".__DBC_SCHEMATA_WAREHOUSE_COUNTRIES__."
				(
					warehouseID,
					countryID			
				)
				VALUES
				(
					'".(int)$idWarehouse."',
					'".(int)$idCountry."'
				)
			";	
			//echo "<br/>".$query."<br> ";
			if($result=$this->exeSQL($query))
			{
				//return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	function deleteOldHaulageCountry($idHaulagePricingModel)
	{
		if(!empty($idHaulagePricingModel))
		{
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_HAULAGE_COUNTRY__."
				WHERE
					idHaulagePricingModel = '".(int)$idHaulagePricingModel."'
			";
			//echo "<br /> ".$query."<br />";
			if($result=$this->exeSQL($query))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	function updateHaulageZones($idHaulagePricingModel)
	{
		if(!empty($idHaulagePricingModel))
		{
			$query="
				UPDATE
					".__DBC_SCHEMATA_HAULAGE_ZONE__."
				SET
					iLatestUpdated = '0'
				WHERE
					idHaulagePricingModel = '".(int)$idHaulagePricingModel."'
			";
			if($result=$this->exeSQL($query))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	function getHaulageModelDetails($idHaulagePricingModel)
	{
		if($idHaulagePricingModel>0)
		{
			$query="
				SELECT
					hpm.id,
					hpm.idWarehouse,
					hpm.iDirection,
					w.idForwarder
				FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." hpm
				INNER JOIN
					".__DBC_SCHEMATA_WAREHOUSES__." w
				ON
					w.id = hpm.idWarehouse
				WHERE
					hpm.id = '".(int)$idHaulagePricingModel."'
				AND
					w.iActive = '1'
			";	
			//echo "<br /> ".$query."<br /> ";
			if($result=$this->exeSQL($query))
			{
				$row = $this->getAssoc($result);
				return $row ;
			}
			else
			{
				return false;
			}
		}
	}
	
	function isHaulageCountryExists($data)
	{
            if(!empty($data))
            { 
                $query="
                    SELECT
                        id
                    FROM
                        ".__DBC_SCHEMATA_HAULAGE_COUNTRY__."
                    WHERE
                        idHaulagePricingModel = '".(int)$data['idHaulagePricingModel']."'
                    AND
                        idCountry = '".(int)$data['idCountry']."'
                    AND
                        idWarehouse = '".(int)$data['idWarehouse']."'
                    AND
                        idDirection = '".(int)$data['idDirection']."'
                ";	
                if($result=$this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
	}
	
	function isWarehouseCountryExists($idWarehouse,$idCountry)
	{
		if($idWarehouse>0 && $idCountry>0)
		{
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_WAREHOUSE_COUNTRIES__."
				WHERE
					warehouseID = '".(int)$idWarehouse."'
				AND
					countryID = '".(int)$idCountry."'
			";	
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
	}
	
	function addCity($data)
	{
	
		$kWHSSearch = new cWHSSearch();
		$kWHSSearch->load($data['idWarehouse']);
		$maxRadius=$kWHSSearch->getManageMentVariableByDescription('__MAX_RADIUS_CIRCLE__');
		//echo $maxRadius;
		
		$maxWMFactor=$kWHSSearch->getManageMentVariableByDescription('__MAX_W/M_FACTOR__');
		$miniWMFactor=$kWHSSearch->getManageMentVariableByDescription('__MINI_W/M_FACTOR__');
		$maxFuel=$kWHSSearch->getManageMentVariableByDescription('__MAX_FUEL__');
		$miniFuel=$kWHSSearch->getManageMentVariableByDescription('__MINI_FUEL__');
		
		$this->set_szCity(trim(sanitize_all_html_input($data['szName'])));
		$this->set_fFuelPercentage(trim(sanitize_all_html_input(urlencode($data['fFuelPercentage']))),$maxFuel,$miniFuel);
		$this->set_idTransitTime(trim(sanitize_all_html_input($data['idTransitTime'])));
		$this->set_fWmFactor(trim(sanitize_all_html_input($data['fWmFactor'])),$maxWMFactor,$miniWMFactor);
		$this->set_iRadius(trim(sanitize_all_html_input($data['iDistance'])),$maxRadius);
		
		$postCodeNewArr=array();
		
		if($this->isZoneNameExist(0,$this->szCity,$data['idWarehouse'],$data['idDirection'],$data['idHaulageModel']))
		{
			$this->addError( "szName" , t($this->t_base.'messages/city_already_exist_for_warehouse')." ".$kWHSSearch->szWareHouseName.", ".t($this->t_base.'messages/city_already_exist_for_warehouse_1'));
		}
		
		if(!empty($data['szLatLong']))
		{
			$latlongArr=explode(",",$data['szLatLong']);
		}
			$location='';
			$responseFlag=true;
			if($latlongArr[0]!='' && $latlongArr[1]!='' && $latlongArr[1]!=0 && $latlongArr[0]!=0)
			{
	  			$countryApiDetailAry = array();
	  			$countryApiDetailAry = getAddressDetails($latlongArr[0],$latlongArr[1]);
	  			$countryISO = $countryApiDetailAry['szCountryCode'] ;
	  			
	  			if(empty($countryApiDetailAry))
	  			{
	  				$this->addError( "szName" , t($this->t_base.'messages/yahoo_geocode_api_is_not_responsed_please_try_after_some_time'));
	  				$responseFlag=false;
	  			}
	  			$location = $countryISO;
			}
			
			if($location=='' && $responseFlag)
			{
				$this->addError( "szName" , t($this->t_base.'messages/the_location_not_defined_on_map'));
			}
		
		//exit if error exist.
		if ($this->error === true)
		{
			return false;
		}
		
		$idCountryBYISOCode=$this->getIdCountryFromISOCode($location);
				
		$query = "
			SELECT 
				(( 3959 * acos( cos( radians(".$latlongArr[0].") ) * cos( radians( ".$kWHSSearch->szLatitude." ) ) 
				   * cos( radians(".$kWHSSearch->szLongitude.") - radians(".$latlongArr[1].")) + sin(radians(".$latlongArr[0].")) 
				   * sin( radians(".$kWHSSearch->szLatitude.")))) * 1.609344) as iDistance
		" ;
		//echo "<br />".$query." <br />";
		if($result = $this->exeSQL($query))
		{
			$row = $this->getAssoc($result);
			$distance= round((float)$row['iDistance'],2);
		}
		
		$query="
			SELECT
				MAX(iPriority) as iPriority
			FROM
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
			WHERE
				idWarehouse='".(int)$data['idWarehouse']."'
			AND
				iDirection ='".(int)$data['idDirection']."'
			AND
				idHaulageModel='".(int)$data['idHaulageModel']."'
		";
			$result=$this->exeSQL($query);
			$row=$this->getAssoc($result);
			$iPriority=$row['iPriority']+1;
		
		$query="
			INSERT INTO
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
			(
				idWarehouse,
				iDirection,
				idHaulageModel,
				iPriority,
				szName,
				idHaulageTransitTime,
				fWmFactor,
				fFuelPercentage,
				iActive,
				iRadiousKM,
				iDistanceUpToKm,
				szLatitude,
				szLongitude,
				idCountry
			)
				VALUES
			(
				'".(int)$data['idWarehouse']."',
				'".(int)$data['idDirection']."',
				'".(int)$data['idHaulageModel']."',
				'".(int)$iPriority."',
				'".mysql_escape_custom($this->szCity)."',
				'".(int)$this->idTransitTime."',
				'".(int)$this->fWmFactor."',
				'".(float)$this->fFuelPercentage."',
				'1',
				'".(int)$this->iRadius."',
				'".(int)$distance."',
				'".mysql_escape_custom($latlongArr[0])."',
				'".mysql_escape_custom($latlongArr[1])."',
				'".(int)$idCountryBYISOCode."'
			)
		";
		if($result=$this->exeSQL($query))
		{
			$id= $this->iLastInsertID ;
			
			
		$query="
				SELECT
					id,
					iPriority
				FROM
					".__DBC_SCHEMATA_CFS_MODEL_MAPPING__."
				WHERE
					idWarehouse='".(int)$data['idWarehouse']."'
				AND
					idHaulageModel='".(int)$data['idHaulageModel']."'
				AND
					iDirection ='".(int)$data['idDirection']."'
				AND
					iActive='0'
			";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
				
					$row=$this->getAssoc($result);
					$oldPriority=$row['iPriority'];
					
					$query="
						SELECT
							MAX(iPriority) AS maxPriority
						FROM
							".__DBC_SCHEMATA_CFS_MODEL_MAPPING__."
						WHERE
							idWarehouse='".(int)$data['idWarehouse']."'
						AND
							iDirection ='".(int)$data['idDirection']."'
						AND
							iActive='1'
						GROUP BY
							idWarehouse
					";
					$result=$this->exeSQL($query);
					
					$row=$this->getAssoc($result);
					$maxPriority=$row['maxPriority'];
					
					$newPrority=$maxPriority+1;
					
					$query="
						UPDATE
							".__DBC_SCHEMATA_CFS_MODEL_MAPPING__."
						SET
							iPriority=iPriority+1
						WHERE
							idWarehouse='".(int)$data['idWarehouse']."'
						AND
							idHaulageModel<>'".(int)$data['idHaulageModel']."'
						AND
							iDirection ='".(int)$data['idDirection']."'
						AND
							iPriority>='".(int)$newPrority."'
						AND
							iPriority<'".(int)$oldPriority."'
					";
					$result=$this->exeSQL($query);
					
					$query="
						UPDATE
							".__DBC_SCHEMATA_CFS_MODEL_MAPPING__."
						SET
							iActive='1',
							iPriority='".(int)$newPrority."'
						WHERE
							idWarehouse='".(int)$data['idWarehouse']."'
						AND
							idHaulageModel='".(int)$data['idHaulageModel']."'
						AND
							iDirection ='".(int)$data['idDirection']."'
					";
					$result=$this->exeSQL($query);				
				}
			}
			
			$query="
				INSERT INTO
					".__DBC_SCHEMATA_HAULAGE_COUNTRY__."
				(
					idCountry,
					idHaulagePricingModel,
					idForwarder,
					idWarehouse,
					idDirection
				)
					VALUES
				(
					'".(int)$idCountryBYISOCode."',
					'".(int)$id."',
					'".(int)$kWHSSearch->idForwarder."',
					'".(int)$data['idWarehouse']."',
					'".(int)$data['idDirection']."'
					
				)";
				$result = $this->exeSQL( $query );
				$countryData[]=$idCountryBYISOCode;
				
				/*
				* We are no longer putting haulage data in tblwarehousecountry because client asked to display service for the CFS who has only D service 
				* 
				*/
				
				//$this->addWarehouseCountries($data['idWarehouse'],$countryData);
			return $id;
		}
		else
		{
			return 0;
		}
	}
	
	
	function loadCity($id)
	{
		if((int)$id>0)
		{
			$query="
				SELECT
					id,
					szName,
					idHaulageTransitTime,
					fWmFactor,
					fFuelPercentage,
					iActive,
					iRadiousKM,
					iDistanceUpToKm,
					szLatitude,
					szLongitude,
					idCountry
				FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
				WHERE
					id='".(int)$id."'
			";
			//echo $query."<br/>";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					return $row;
				}
				else
				{
					return array();
				}
				
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	
	function updateCity($data)
	{
		//print_r($data);
		$kWHSSearch = new cWHSSearch();
		$kWHSSearch->load($data['idWarehouse']);
		$maxRadius=$kWHSSearch->getManageMentVariableByDescription('__MAX_RADIUS_CIRCLE__');
	
		$maxWMFactor=$kWHSSearch->getManageMentVariableByDescription('__MAX_W/M_FACTOR__');
		$miniWMFactor=$kWHSSearch->getManageMentVariableByDescription('__MINI_W/M_FACTOR__');
		$maxFuel=$kWHSSearch->getManageMentVariableByDescription('__MAX_FUEL__');
		$miniFuel=$kWHSSearch->getManageMentVariableByDescription('__MINI_FUEL__');
		
		$this->set_szCity(trim(sanitize_all_html_input($data['szName'])));
		$this->set_fFuelPercentage(trim(sanitize_all_html_input(urlencode($data['fFuelPercentage']))),$maxFuel,$miniFuel);
		$this->set_idTransitTime(trim(sanitize_all_html_input($data['idTransitTime'])));
		$this->set_fWmFactor(trim(sanitize_all_html_input($data['fWmFactor'])),$maxWMFactor,$miniWMFactor);
		$this->set_iRadius(trim(sanitize_all_html_input($data['iDistance'])),$maxRadius);
		
		$postCodeNewArr=array();
		
		if($this->isZoneNameExist($data['id'],$this->szCity,$data['idWarehouse'],$data['idDirection'],$data['idHaulageModel']))
		{
			$this->addError( "szName" , t($this->t_base.'messages/city_already_exist_for_warehouse')." ".$kWHSSearch->szWareHouseName.", ".t($this->t_base.'messages/city_already_exist_for_warehouse_1'));
		}
		
			if(!empty($data['szLatLong']))
			{
				$latlongArr=explode(",",$data['szLatLong']);
			}
		
			$location='';
			$responseFlag=true;
			if($latlongArr[0]!='' && $latlongArr[1]!='' && $latlongArr[1]!=0 && $latlongArr[0]!=0)
			{	
	  			$countryApiDetailAry = array();
	  			$countryApiDetailAry = getAddressDetails($latlongArr[0],$latlongArr[1]);
	  			$countryISO = $countryApiDetailAry['szCountryCode'] ;
	  			
				if(empty($countryApiDetailAry))
	  			{
	  				$this->addError( "szName" , t($this->t_base.'messages/yahoo_geocode_api_is_not_responsed_please_try_after_some_time'));
	  				$responseFlag=false;
	  			}
	  			$location = $countryISO;
			}
			
			if($location=='' && $responseFlag)
			{
				$this->addError( "szName" , t($this->t_base.'messages/the_location_not_defined_on_map'));
			}
		
		//exit if error exist.
		if ($this->error === true)
		{
			return false;
		}
		
		
		
		$query = "
			SELECT 
				(( 3959 * acos( cos( radians(".$latlongArr[0].") ) * cos( radians( ".$kWHSSearch->szLatitude." ) ) 
				   * cos( radians(".$kWHSSearch->szLongitude.") - radians(".$latlongArr[1].")) + sin(radians(".$latlongArr[0].")) 
				   * sin( radians(".$kWHSSearch->szLatitude.")))) * 1.609344) as iDistance
		" ;
		//echo "<br />".$query." <br />";
		if($result = $this->exeSQL($query))
		{
			$row = $this->getAssoc($result);
			$distance= round((float)$row['iDistance'],2);
		}
		
		$idCountryBYISOCode=$this->getIdCountryFromISOCode($location);
		
		$query="
			UPDATE
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
			SET
			
				szName='".mysql_escape_custom($this->szCity)."',
				idHaulageTransitTime='".(int)$this->idTransitTime."',
				fWmFactor='".(int)$this->fWmFactor."',
				fFuelPercentage='".(float)$this->fFuelPercentage."',
				iRadiousKM='".(int)$this->iRadius."',
				iDistanceUpToKm='".(int)$distance."',
				szLatitude='".mysql_escape_custom($latlongArr[0])."',
				szLongitude='".mysql_escape_custom($latlongArr[1])."',
				idCountry='".(int)$idCountryBYISOCode."'
			WHERE
				id='".(int)$data['id']."'
		";
		if($result=$this->exeSQL($query))
		{
		
			$query="
				UPDATE
					".__DBC_SCHEMATA_HAULAGE_COUNTRY__."
				SET
					idCountry='".(int)$idCountryBYISOCode."'
				WHERE
					idHaulagePricingModel='".(int)$data['id']."'
			";
			$result = $this->exeSQL( $query );
				$countryData[]=$idCountryBYISOCode;
			//$this->addWarehouseCountries($data['idWarehouse'],$countryData);
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
	function getAllCityHaulageData($idWarehouse,$idDirection,$idHaulageModel)
	{
		$query="
			SELECT
				iRadiousKM,
				szLatitude,
				szLongitude
			FROM
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
			WHERE
				idWarehouse='".(int)$idWarehouse."'
			AND
				iDirection ='".(int)$idDirection."'
			AND
				idHaulageModel='".(int)$idHaulageModel."'
		";
		//echo $query;
		if($result= $this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				while($row=$this->getAssoc($result))
				{
					$ret_arr[]=$row;
				}
				return $ret_arr;
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function loadDistance($id)
	{
		if((int)$id>0)
		{
			$query="
				SELECT
					id,
					szName,
					idHaulageTransitTime,
					fWmFactor,
					fFuelPercentage,
					iActive,
					iDistanceUpToKm
				FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
				WHERE
					id='".(int)$id."'
			";
			//echo $query."<br/>";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					return $row;
				}
				else
				{
					return array();
				}
				
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function deleteHaulagePricingDistanceData($id,$idWarehouse,$idHaulageModel,$idDirection)
	{
		if((int)$id>0)
		{
			$query="
				SELECT
					iPriority
				FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
				WHERE
					id='".(int)$id."'
			";
			//echo $query."<br/>";
			$result=$this->exeSQL($query);
			$row=$this->getAssoc($result);
			$iPriority=$row['iPriority'];
			
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
				WHERE
					id='".(int)$id."'				
			";
			$result=$this->exeSQL($query);
			
			
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_HAULAGE_PRICING__."
				WHERE
					idHaulagePricingModel='".(int)$id."'				
			";
			$result=$this->exeSQL($query);
			
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_HAULAGE_COUNTRY__."
				WHERE
					idHaulagePricingModel='".(int)$id."'				
			";
			$result=$this->exeSQL($query);
			
			
			$query="
				UPDATE
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
				SET
					iPriority=iPriority-1
				WHERE
					id!='".(int)$id."'
				AND
					iPriority>='".(int)($iPriority+1)."'
				AND
					idWarehouse='".(int)$idWarehouse."'
				AND
					iDirection='".(int)$idDirection."'
				AND
					idHaulageModel='".(int)$idHaulageModel."'	
				";
			//echo $query;
			$result = $this->exeSQL( $query );
		}
		else
		{
			return false;
		}
	}
	
	function getAllCountriesForDistanceHaulageModel($id)
	{
		$query="
			SELECT
				hd.idCountry,
				c.szCountryName
			FROM
				".__DBC_SCHEMATA_HAULAGE_COUNTRY__." AS hd
			INNER JOIN
				".__DBC_SCHEMATA_COUNTRY__." AS c
			ON
				c.id=hd.idCountry
			WHERE
				hd.idHaulagePricingModel='".(int)$id."'
			ORDER BY
				c.szCountryName ASC					
		";
		//echo $query;
		if($result= $this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				while($row=$this->getAssoc($result))
				{
					$ret_arr[]=$row;
				}
				return $ret_arr;
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function addDistance($data)
	{  
		$kWHSSearch = new cWHSSearch();
		$kWHSSearch->load($data['idWarehouse']);
		
		$maxWMFactor=$kWHSSearch->getManageMentVariableByDescription('__MAX_W/M_FACTOR__');
		$miniWMFactor=$kWHSSearch->getManageMentVariableByDescription('__MINI_W/M_FACTOR__');
		$maxFuel=$kWHSSearch->getManageMentVariableByDescription('__MAX_FUEL__');
		$miniFuel=$kWHSSearch->getManageMentVariableByDescription('__MINI_FUEL__');
		$maxDistance=$kWHSSearch->getManageMentVariableByDescription('__MAX_DISTANCE__');
		
		$this->set_iDistanceKm(trim(sanitize_all_html_input($data['iDistanceKm'])),$maxDistance);
		$this->set_fFuelPercentage(trim(sanitize_all_html_input(urlencode($data['fFuelPercentage']))),$maxFuel,$miniFuel);
		$this->set_idTransitTime(trim(sanitize_all_html_input($data['idTransitTime'])));
		$this->set_fWmFactor(trim(sanitize_all_html_input($data['fWmFactor'])),$maxWMFactor,$miniWMFactor);
		$this->set_szCountriesStr(trim(sanitize_all_html_input($data['szCountriesStr'])));
		
		
		if(!empty($this->iDistanceKm) && $this->isDistanceExist(0,$this->iDistanceKm,$data['idWarehouse'],$data['idDirection'],$data['idHaulageModel']))
		{
			$this->addError( "iDistanceKm" , "This distance already exists for this warehouse ".$kWHSSearch->szWareHouseName);
		}
		
		//exit if error exist.
		if ($this->error === true)
		{
			return false;
		}
		
		$query="
			SELECT
                            MAX(iPriority) as iPriority
			FROM
                            ".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
			WHERE
                            idWarehouse='".(int)$data['idWarehouse']."'
			AND
                            iDirection ='".(int)$data['idDirection']."'
			AND
                            idHaulageModel='".(int)$data['idHaulageModel']."'
		";
                $result=$this->exeSQL($query);
                $row=$this->getAssoc($result);
                $iPriority=$row['iPriority']+1;
		
		$szCountriesArr=explode(",",trim($this->szCountriesStr));
		$query="
			INSERT INTO
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
			(
				idWarehouse,
				iDirection,
				idHaulageModel,
				iPriority,
				idHaulageTransitTime,
				fWmFactor,
				fFuelPercentage,
				iActive,
				iDistanceUpToKm
			)
				VALUES
			(
				'".(int)$data['idWarehouse']."',
				'".(int)$data['idDirection']."',
				'".(int)$data['idHaulageModel']."',
				'".(int)$iPriority."',
				'".(int)$this->idTransitTime."',
				'".(int)$this->fWmFactor."',
				'".(float)$this->fFuelPercentage."',
				'1',
				'".(int)$this->iDistanceKm."'
			)
		";
		if($result=$this->exeSQL($query))
		{
			$id= $this->iLastInsertID ;
			
			$query="
				SELECT
                                    id,
                                    iPriority
				FROM
                                    ".__DBC_SCHEMATA_CFS_MODEL_MAPPING__."
				WHERE
                                    idWarehouse='".(int)$data['idWarehouse']."'
				AND
                                    idHaulageModel='".(int)$data['idHaulageModel']."'
				AND
                                    iDirection ='".(int)$data['idDirection']."'
				AND
                                    iActive='0'
			";
			if($result=$this->exeSQL($query))
			{
                            if($this->iNumRows>0)
                            { 
                                $row=$this->getAssoc($result);
                                $oldPriority=$row['iPriority'];

                                $query="
                                    SELECT
                                        MAX(iPriority) AS maxPriority
                                    FROM
                                        ".__DBC_SCHEMATA_CFS_MODEL_MAPPING__."
                                    WHERE
                                        idWarehouse='".(int)$data['idWarehouse']."'
                                    AND
                                        iDirection ='".(int)$data['idDirection']."'
                                    AND
                                        iActive='1'
                                    GROUP BY
                                        idWarehouse
                                ";
                                $result=$this->exeSQL($query);

                                $row=$this->getAssoc($result);
                                $maxPriority=$row['maxPriority'];

                                $newPrority=$maxPriority+1;

                                $query="
                                    UPDATE
                                        ".__DBC_SCHEMATA_CFS_MODEL_MAPPING__."
                                    SET
                                        iPriority=iPriority+1
                                    WHERE
                                        idWarehouse='".(int)$data['idWarehouse']."'
                                    AND
                                        idHaulageModel<>'".(int)$data['idHaulageModel']."'
                                    AND
                                        iDirection ='".(int)$data['idDirection']."'
                                    AND
                                        iPriority>='".(int)$newPrority."'
                                    AND
                                        iPriority<'".(int)$oldPriority."'
                                ";
                                $result=$this->exeSQL($query);

                                $query="
                                    UPDATE
                                        ".__DBC_SCHEMATA_CFS_MODEL_MAPPING__."
                                    SET
                                        iActive='1',
                                        iPriority='".(int)$newPrority."'
                                    WHERE
                                        idWarehouse='".(int)$data['idWarehouse']."'
                                    AND
                                        idHaulageModel='".(int)$data['idHaulageModel']."'
                                    AND
                                        iDirection ='".(int)$data['idDirection']."'
                                ";
                                $result=$this->exeSQL($query);				
                            }
			}
			
			if(!empty($szCountriesArr))
			{
                            $ctr=1;
                            foreach($szCountriesArr as $szCountriesArrs)
                            {					
                                $query="
                                    INSERT INTO
                                        ".__DBC_SCHEMATA_HAULAGE_COUNTRY__."
                                    (
                                        idCountry,
                                        idHaulagePricingModel,
                                        idForwarder,
                                        idWarehouse,
                                        idDirection
                                    )
                                    VALUES
                                    (
                                        '".(int)$szCountriesArrs."',
                                        '".(int)$id."',
                                        '".(int)$kWHSSearch->idForwarder."',
                                        '".(int)$data['idWarehouse']."',
                                        '".(int)$data['idDirection']."'

                                    )
                                ";
                                //echo "<br> HC Query: ".$query;
                                $result = $this->exeSQL( $query );
                            }
			}
			//$this->addWarehouseCountries($data['idWarehouse'],$szCountriesArr);
			return $id;
		}
		else
		{
			return 0;
		}
	}
	
	
	function updateDistance($data)
	{
	
		$kWHSSearch = new cWHSSearch();
		$kWHSSearch->load($data['idWarehouse']);
		
		$maxWMFactor=$kWHSSearch->getManageMentVariableByDescription('__MAX_W/M_FACTOR__');
		$miniWMFactor=$kWHSSearch->getManageMentVariableByDescription('__MINI_W/M_FACTOR__');
		$maxFuel=$kWHSSearch->getManageMentVariableByDescription('__MAX_FUEL__');
		$miniFuel=$kWHSSearch->getManageMentVariableByDescription('__MINI_FUEL__');
		$maxDistance=$kWHSSearch->getManageMentVariableByDescription('__MAX_DISTANCE__');
		
		$this->set_iDistanceKm(trim(sanitize_all_html_input($data['iDistanceKm'])),$maxDistance);
		$this->set_fFuelPercentage(trim(sanitize_all_html_input(urlencode($data['fFuelPercentage']))),$maxFuel,$miniFuel);
		$this->set_idTransitTime(trim(sanitize_all_html_input($data['idTransitTime'])));
		$this->set_fWmFactor(trim(sanitize_all_html_input($data['fWmFactor'])),$maxWMFactor,$miniWMFactor);
		$this->set_szCountriesStr(trim(sanitize_all_html_input($data['szCountriesStr'])));
		
		
		if(!empty($this->iDistanceKm) && $this->isDistanceExist($data['id'],$this->iDistanceKm,$data['idWarehouse'],$data['idDirection'],$data['idHaulageModel']))
		{
			$this->addError( "iDistanceKm" , "This distance already exists for this warehouse ".$kWHSSearch->szWareHouseName);
		}
		
		//exit if error exist.
		if ($this->error === true)
		{
			return false;
		}
		
		
		$szCountriesArr=explode(",",trim($this->szCountriesStr));
		$query="
			UPDATE
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
			SET
				idHaulageTransitTime='".(int)$this->idTransitTime."',
				fWmFactor='".(int)$this->fWmFactor."',
				fFuelPercentage='".(float)$this->fFuelPercentage."',
				iDistanceUpToKm='".(int)$this->iDistanceKm."'
			WHERE
				id='".(int)$data['id']."'
		";
		if($result=$this->exeSQL($query))
		{
			
			
			if(!empty($szCountriesArr))
			{
				$query="
				DELETE FROM
					".__DBC_SCHEMATA_HAULAGE_COUNTRY__."
				WHERE
					idHaulagePricingModel='".(int)$data['id']."'				
				";
				$result=$this->exeSQL($query);
				foreach($szCountriesArr as $szCountriesArrs)
				{					
					$query="
						INSERT INTO
							".__DBC_SCHEMATA_HAULAGE_COUNTRY__."
						(
							idCountry,
							idHaulagePricingModel,
							idForwarder,
							idWarehouse,
							idDirection
						)
							VALUES
						(
							'".(int)$szCountriesArrs."',
							'".(int)$data['id']."',
							'".(int)$kWHSSearch->idForwarder."',
							'".(int)$data['idWarehouse']."',
							'".(int)$data['idDirection']."'
							
						)";
						$result = $this->exeSQL( $query );
				}
			}
			//$this->addWarehouseCountries($data['idWarehouse'],$szCountriesArr);
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function isDistanceExist($id=0,$iDistanceKm,$idWarehouse,$iDirection,$idHaulageModel)
	{
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
			WHERE
				iDistanceUpToKm = '".mysql_escape_custom($iDistanceKm)."'
			AND
				idWarehouse='".(int)$idWarehouse."'
			AND
				iDirection ='".(int)$iDirection."'
			AND
				idHaulageModel='".(int)$idHaulageModel."'
		";
		if((int)$id>0)
		{
			$query .="
				AND
				    id<>'".(int)$id."'		
			";
		}
		//echo "<br>".$query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
            if( $this->getRowCnt() > 0 )
            {
                return true;
            }
            else
            {
             	return false;
            }
		}
	}
	
	
	function getAllDistanceHaulageModel($idWarehouse,$iDirection,$idHaulageModel,$id=0)
	{
		$query="
			SELECT
				hpm.id,
				hpm.iDistanceUpToKm
			FROM
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." AS hpm
			INNER JOIN
				".__DBC_SCHEMATA_HAULAGE_COUNTRY__." AS hd
			ON
				hd.idHaulagePricingModel=hpm.id
			WHERE
				hpm.idWarehouse='".(int)$idWarehouse."'
			AND
				hpm.iDirection ='".(int)$iDirection."'
			AND
				hpm.idHaulageModel='".(int)$idHaulageModel."'
			AND
				hpm.iActive='1'
			
		";
		
		if((int)$id>0)
		{
			$query .="
				AND
				    hpm.id<>'".(int)$id."'		
			";
		}
		
		$query .="
			GROUP BY
				hpm.iDistanceUpToKm
		";
		//echo $query."<br/>";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				while($row=$this->getAssoc($result))
				{
					$res_arr[]=$row;
				}
				return $res_arr;
			}
			else
			{
				return array();
			}
			
		}
	}
	
	function copyDistanceData($data)
	{
		//print_r($data);
		if(count($data['szCopyZone'])<=0)
		{
			$this->addError( "szCopyZone" , t($this->t_base.'messages/atleast_one_distance_line_is_selected'));
		}
		
		if(count($data['szCopyZonePricing'])>count($data['szCopyZone']))
		{
			$this->addError( "szCopyZone" , t($this->t_base.'messages/pricing_data_count_greater_than_distance_data_count'));
		}
		$existZoneName=array();
		if(!empty($data['szCopyZone']))
		{
			$checkflag=false;
			foreach($data['szCopyZone'] as $szCopyZoneArr)
			{
				$arrDistance[0]=$this->loadDistance($szCopyZoneArr);
				if($this->isDistanceExist(0,$arrDistance[0]['iDistanceUpToKm'],$data['idZoneWarehouse'],$data['idZoneDirection'],$data['idZoneHaulageModel']))
				{
					$existZoneName[]=$arrDistance[0]['iDistanceUpToKm']." km";
				}
				
			}
		}
				
		if(!empty($existZoneName))
		{
			$existZoneNameStr=implode(",",$existZoneName);
			$this->addError( "szCopyZone" , t($this->t_base.'messages/already_defined_distance_msg'));
		}
		//exit if error exist.
		if ($this->error === true)
		{
			return false;
		}
		if(!empty($data['szCopyZone']))
		{
			
			$kWHSSearch = new cWHSSearch();
			$kWHSSearch->load($data['idZoneWarehouse']);
		
			foreach($data['szCopyZone'] as $szCopyZoneArr)
			{
				$distanceArr[0]=$this->loadDistance($szCopyZoneArr);
								
				$query="
					SELECT
						MAX(iPriority) as iPriority
					FROM
						".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
					WHERE
						idWarehouse='".(int)$data['idZoneWarehouse']."'
					AND
						iDirection ='".(int)$data['idZoneDirection']."'
					AND
						idHaulageModel='".(int)$data['idZoneHaulageModel']."'
				";
				$result=$this->exeSQL($query);
				$row=$this->getAssoc($result);
				$iPriority=$row['iPriority']+1;
				
				$query="
					INSERT INTO
						".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
					(
						idWarehouse,
						iDirection,
						idHaulageModel,
						iPriority,
						iDistanceUpToKm,
						idHaulageTransitTime,
						fWmFactor,
						fFuelPercentage,
						iActive
					)
						VALUES
					(
						'".(int)$data['idZoneWarehouse']."',
						'".(int)$data['idZoneDirection']."',
						'".(int)$data['idZoneHaulageModel']."',
						'".(int)$iPriority."',
						'".mysql_escape_custom($distanceArr[0]['iDistanceUpToKm'])."',
						'".(int)$distanceArr[0]['idHaulageTransitTime']."',
						'".(float)$distanceArr[0]['fWmFactor']."',
						'".(float)$distanceArr[0]['fFuelPercentage']."',
						'1'
					)
				";
				//echo $query;
				if($result=$this->exeSQL($query))
				{
					$id= $this->iLastInsertID ;
					
						if(!empty($data['szCopyZonePricing']) && in_array($szCopyZoneArr,$data['szCopyZonePricing']))
						{
							$pricingListArr=array();
							$pricingListArr=$this->getHaulagePricingZoneDetailData($szCopyZoneArr);
							//print_r($pricingListArr);
							if(!empty($pricingListArr))
							{
								foreach($pricingListArr  as $pricingListArrs)
								{
									$query="
										INSERT INTO
											".__DBC_SCHEMATA_HAULAGE_PRICING__."
										(	
											idHaulagePricingModel,
											iUpToKg,
											fPricePer100Kg,
											fMinimumPrice,
											fPricePerBooking,
											idCurrency,
											dtCreatedOn
										)
											VALUES
										(
											'".(int)$id."',
											'".(int)$pricingListArrs['iUpToKg']."',
											'".(float)$pricingListArrs['fPricePer100Kg']."',
											'".(float)$pricingListArrs['fMinimumPrice']."',
											'".(float)$pricingListArrs['fPricePerBooking']."',
											'".(int)$pricingListArrs['idCurrency']."',
											NOW()
										)
									";
									//echo $query."<br/><br/>";
									$result = $this->exeSQL( $query );
								}
							}
						}
					
					$szDistanceArr=$this->getAllCountriesForDistanceHaulageModel($szCopyZoneArr);
					if(!empty($szDistanceArr))
					{
						$ctr=1;
						foreach($szDistanceArr as $szDistanceArrs)
						{
						
							$query="
								INSERT INTO
									".__DBC_SCHEMATA_HAULAGE_COUNTRY__."
								(
									idCountry,
									idHaulagePricingModel,
									idForwarder,
									idWarehouse,
									idDirection
								)
									VALUES
								(
									'".(int)$szDistanceArrs['idCountry']."',
									'".(int)(int)$id."',
									'".(int)$kWHSSearch->idForwarder."',
									'".(int)$data['idZoneWarehouse']."',
									'".(int)$data['idZoneDirection']."'
									
								)";
								$result = $this->exeSQL( $query );
						}
					}
				}
			}
			return true;
		}
	}
	
	function getTotalNumRequestSent($dtDate,$check_flag=false)
	{
		$query="
			SELECT
				iNumRequestSent
			FROM
				".__DBC_SCHEMATA_YAHOO_API_LOGS__."
			WHERE
				DATE(dtDateTime) = '".mysql_escape_custom(trim($dtDate))."'
			ORDER BY
				iNumRequestSent DESC
		";
		//echo "<br />".$query."<br />";
		if($result = $this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				if($check_flag)
				{
					return true;
				}
				else
				{
					$row = $this->getAssoc($result);
					return $row['iNumRequestSent'];
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	function addYahooApiLogs($iNumRequestSent)
	{
		$dtDate = date('Y-m-d');
		if($this->getTotalNumRequestSent($dtDate,true))
		{
			$query="
				UPDATE
					".__DBC_SCHEMATA_YAHOO_API_LOGS__."
				SET
					iNumRequestSent = iNumRequestSent + '".(int)$iNumRequestSent."'
				WHERE
					DATE(dtDateTime) = '".mysql_escape_custom(trim($dtDate))."'
			";	
		}
		else
		{
			$query="
				INSERT INTO
					".__DBC_SCHEMATA_YAHOO_API_LOGS__."
				(
					iNumRequestSent,
					dtDateTime
				)
				VALUES
				(
					'".(int)$iNumRequestSent."',
					now()
				)
			";
		}
		if($result = $this->exeSQL($query))
		{
			return false;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	
	function mapAllCFSTOHaulageModel()
	{
	
		$iDirectionArr=array('1','2');
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_HAULAGE_MODELS__."
		";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				while($row=$this->getAssoc($result))
				{
					$res_arr[]=$row['id'];
				}
			}
		}
		
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_WAREHOUSES__."
		";
		
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				while($row=$this->getAssoc($result))
				{
					if(!empty($res_arr))
					{
					
						foreach($iDirectionArr as $iDirectionArrs)
						{
							$i=1;
							foreach($res_arr as $res_arrs)
							{
								$query_insert="
									INSERT INTO
										".__DBC_SCHEMATA_HAULAGE_MODELS_CFS_MAPPING__."
									(
										idWarehouse,
										idHaulageModel,
										iPriority,
										dtCreatedOn,
										iActive,
										iDirection
									)
										VALUES
									(
										'".(int)$row['id']."',
										'".(int)$res_arrs."',
										'".$i."',
										NOW(),
										'1',
										'".(int)$iDirectionArrs."'
									)
								";
								
								$result_insert = $this->exeSQL($query_insert);
								++$i;
							}
						}
					}
				}
			}
		}
	}
	

	function getDistanceFromCFS($distanceMeasureAry)
	{
		$query = "
			SELECT 
				(( 3959 * acos( cos( radians(".(float)$distanceMeasureAry['szLatitude'].") ) * cos( radians( model.szLatitude ) ) 
				   * cos( radians(model.szLongitude) - radians(".(float)$distanceMeasureAry['szLongitude'].")) + sin(radians(".(float)$distanceMeasureAry['szLatitude'].")) 
				   * sin( radians(model.szLatitude)))) * 1.609344) as iDistance
			FROM
				".__DBC_SCHEMATA_WAREHOUSES__." model
			WHERE
				id = '".(int)$distanceMeasureAry['idWarehouse']."'
		" ;
		//echo "<br />".$query." <br />";
		if($result = $this->exeSQL($query))
		{
			$row = $this->getAssoc($result);
			return round((float)$row['iDistance'],2);
		}
	}
	
	function getHaulagePricingDetais_review_tryitout($data)
	{
		if(!empty($data))
		{	
			$this->set_idCountry(trim(sanitize_all_html_input($data['szCountry'])));
			$this->set_szPostCode(trim(sanitize_all_html_input($data['szPostCodeCity'])));
			$this->set_fTotalWeight(trim(sanitize_all_html_input($data['fTotalWeight'])));
			$this->set_fVolume(trim(sanitize_all_html_input($data['fVolume'])));
			
			$this->set_idWarehouse(trim(sanitize_all_html_input($data['idWarehouse'])));
			$this->set_iDirection(trim(sanitize_all_html_input($data['idDirection'])));
			$this->set_idHaulageModel(trim(sanitize_all_html_input($data['idHaulageModel'])));
			
			if ($this->error === true)
			{
				return false;
			}			
			
			if($this->szPostCode=='From Map')
			{
				$this->set_szLatitude(trim(sanitize_all_html_input($data['szLatitude'])));
				$this->set_szLongitude(trim(sanitize_all_html_input($data['szLongitude'])));
				$szCityPostCode = 'From map';
			}
			else
			{
				$kConfig=new cConfig();
				$postCodeAry = $kConfig->getPostCodeDetails($this->idCountry,$this->szPostCode);
				
				if(!empty($postCodeAry))
				{
					$this->szLatitude = $postCodeAry['szLatitude'];			
					$this->szLongitude = $postCodeAry['szLongitude'];	
					if(!empty($postCodeAry['szCity']))
					{
						$szCityPostCode = $this->szPostCode." - ".$postCodeAry['szCity'];
					}
					else
					{
						$szCityPostCode = $this->szPostCode ;
					}
				}
				else
				{
					$error_messg = t($this->t_base.'messages/UNRECOGNISE_LOCATION_FROM_TRYIT_OUT');
					$this->addError('szLatitude',$error_messg);
					return false;
				}
			}
			if ($this->error === true)
			{
				return false;
			}
			
			$houlageData=array();
			$fFromWareHouseHaulageAry = array();
			$cfsDistanceAry = array();
			
			$cfsDistanceAry['szLatitude'] = $this->szLatitude ;
			$cfsDistanceAry['szLongitude'] = $this->szLongitude ;
			$cfsDistanceAry['idWarehouse'] = $this->idWarehouse ;
			
			$iDistance = $this->getDistanceFromCFS($cfsDistanceAry);
			
			$houlageData['iDistance'] = $iDistance ;
			$houlageData['idWarehouse'] = $this->idWarehouse ;
			$houlageData['iDirection'] = $this->iDirection ; // origin haulage		
				
			$houlageData['szLatitude'] = $this->szLatitude ;
			$houlageData['szLongitude'] = $this->szLongitude ;
			
			$houlageData['fTotalWeight'] = $this->fTotalWeight ;
			$houlageData['fVolume'] = $this->fVolume ;
			$houlageData['szPostcode'] = $this->szPostCode ;
			$houlageData['idCountry'] = $this->idCountry ;
			$houlageData['idHaulageModel'] = $this->idHaulageModel ;
			
			$WareHouseHaulageAry = $this->getHaulageDetails_review($houlageData);
			if(!empty($WareHouseHaulageAry))
			{
				$kConfig = new cConfig();
				$countryNameAry = array();
				$countryNameAry = $kConfig->getAllCountryInKeyValuePair(false,$this->idCountry);
				
				$WareHouseHaulageAry['fCargoWeight'] = $this->fTotalWeight ;
				$WareHouseHaulageAry['fCargoVolume'] = $this->fVolume ;
				$WareHouseHaulageAry['iDirection'] = $this->iDirection ;
				$WareHouseHaulageAry['idWarehouse'] = $this->idWarehouse ;
				$WareHouseHaulageAry['iDistance'] = $iDistance ;
				$WareHouseHaulageAry['szLatitude'] = $houlageData['szLatitude'];
				$WareHouseHaulageAry['szLongitude'] = $houlageData['szLongitude'];
				$WareHouseHaulageAry['szCityPostCode'] = $szCityPostCode ;
				$WareHouseHaulageAry['szCountryName'] = $countryNameAry[$this->idCountry]['szCountryName'];
				
				$kWHSSearch = new cWHSSearch();
				$kWHSSearch->load($this->idWarehouse);
				
				
				$countryNameAry = array();
				$countryNameAry = $kConfig->getAllCountryInKeyValuePair(false,$kWHSSearch->idCountry);
				
				$WareHouseHaulageAry['szWhsCity'] = $kWHSSearch->szCity;
				$WareHouseHaulageAry['szWhsPostcode'] = $kWHSSearch->szPostCode;
				$WareHouseHaulageAry['szWhsCountry'] = $countryNameAry[$kWHSSearch->idCountry]['szCountryName'];
				$WareHouseHaulageAry['szWhsLatitude'] = $kWHSSearch->szLatitude;
				$WareHouseHaulageAry['szWhsLongitude'] = $kWHSSearch->szLongitude;
			}
			return $WareHouseHaulageAry ;
		}
	}

	function getHaulagePricingDetais_tryitout($data)
	{ 
            if(!empty($data))
            {	
                if($data['iForwarderTryitoutFlag']==1)
                { 
                    $this->set_szLocation(trim(sanitize_all_html_input($data['szLocation'])));
                }
                else
                {
                    $this->set_idCountry(trim(sanitize_all_html_input($data['szCountry'])));
                    $this->set_szPostCode(trim(sanitize_all_html_input($data['szPostCodeCity'])));
                } 
                $this->set_fTotalWeight(trim(sanitize_all_html_input($data['fTotalWeight'])));
                $this->set_fVolume(trim(sanitize_all_html_input($data['fVolume'])));

                $this->set_idWarehouse(trim(sanitize_all_html_input($data['idWarehouse'])));
                $this->set_iDirection(trim(sanitize_all_html_input($data['idDirection'])));

                if ($this->error === true)
                {
                    return false;
                }			

                if($this->szPostCode=='From Map' || $this->szLocation=='Selected from map')
                {
                    $this->set_szLatitude(trim(sanitize_all_html_input($data['szLatitude'])));
                    $this->set_szLongitude(trim(sanitize_all_html_input($data['szLongitude'])));
                    
                    if($this->szLocation=='Selected from map')
                    {
                        $szCityPostCode = 'Selected from map';  
                        $szHaulageCountryCode = $this->getCountryCodeByHaulageZone($this->szLongitude,$this->szLatitude); 
                        if(!empty($szHaulageCountryCode))
                        {
                            $kConfig = new cConfig();
                            $kConfig->loadCountry(false, $szHaulageCountryCode);
                            $this->idCountry = $kConfig->idCountry;
                        }
                        if($this->idCountry<=0)
                        {
                            $this->addError('szLocation', "No country found as per your selection");
                        }
                    }
                    else
                    {
                        $szCityPostCode = 'From map';
                    } 
                }
                else if(!empty($this->szLocation))
                {
                    $kConfig = new cConfig();
                    $locationGeocodeAry = array();
                    $locationGeocodeAry = reverse_geocode($this->szLocation);
                                
                    if(!empty($locationGeocodeAry))
                    {
                        $this->szLatitude = $locationGeocodeAry['szLat'];
                        $this->szLongitude = $locationGeocodeAry['szLng'];
                        $this->szPostCode = $locationGeocodeAry['szPostCode'];
                        $this->szCityName = $locationGeocodeAry['szCityName']; 
                        $szHaulageCountryCode = $locationGeocodeAry['szCountryCode'];
                        
                        if(!empty($szHaulageCountryCode))
                        { 
                            $kConfig->loadCountry(false, $szHaulageCountryCode);
                            $this->idCountry = $kConfig->idCountry;
                        }
                        
                        if(empty($this->szPostCode) && $this->idCountry>0)
                        {
                            $postcodeCheckAry=array();
                            $postcodeResultAry = array();

                            $postcodeCheckAry['idCountry'] = $this->idCountry ;
                            $postcodeCheckAry['szLatitute'] = $this->szLatitude ;
                            $postcodeCheckAry['szLongitute'] = $this->szLongitude ;

                            $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry); 
                            if(!empty($postcodeResultAry))
                            {
                                $this->szPostCode = $postcodeResultAry['szPostCode'];  
                            }
                        } 
                        if(!empty($this->szCityName))
                        {
                            $szCityPostCode = $this->szPostCode." - ".$this->szCityName;
                        }
                        else
                        {
                            $szCityPostCode = $this->szPostCode ;
                        }
                    } 
                    if($this->idCountry<=0)
                    {
                        $this->addError('szLocation', "No country found as per your selection");
                    }
                    if(empty($this->szPostCode))
                    {
                        //$this->addError('szLocation', "Please enter a valid postcode");
                    }
                }
                else
                {
                    $kConfig=new cConfig();
                    $postCodeAry = $kConfig->getPostCodeDetails($this->idCountry,$this->szPostCode);

                    if(!empty($postCodeAry))
                    {
                        $this->szLatitude = $postCodeAry['szLatitude'];			
                        $this->szLongitude = $postCodeAry['szLongitude'];	
                        if(!empty($postCodeAry['szCity']))
                        {
                            $szCityPostCode = $this->szPostCode." - ".$postCodeAry['szCity'];
                        }
                        else
                        {
                            $szCityPostCode = $this->szPostCode ;
                        }
                    }
                    else
                    {
                        $error_messg = t($this->t_base.'messages/UNRECOGNISE_LOCATION_FROM_TRYIT_OUT');
                        $this->addError('szLatitude',$error_messg);
                        return false;
                    }
                }
                if ($this->error === true)
                {
                    return false;
                }

                $houlageData=array();
                $fFromWareHouseHaulageAry = array();
                $cfsDistanceAry = array();

                $cfsDistanceAry['szLatitude'] = $this->szLatitude ;
                $cfsDistanceAry['szLongitude'] = $this->szLongitude ;
                $cfsDistanceAry['idWarehouse'] = $this->idWarehouse ;

                $iDistance = $this->getDistanceFromCFS($cfsDistanceAry);

                $houlageData['iDistance'] = $iDistance ;
                $houlageData['idWarehouse'] = $this->idWarehouse ;
                $houlageData['iDirection'] = $this->iDirection ; // origin haulage		

                $houlageData['szLatitude'] = $this->szLatitude ;
                $houlageData['szLongitude'] = $this->szLongitude ;

                $houlageData['fTotalWeight'] = $this->fTotalWeight ;
                $houlageData['fVolume'] = $this->fVolume ;
                $houlageData['szPostcode'] = $this->szPostCode ;
                $houlageData['idCountry'] = $this->idCountry ;
                                
                $WareHouseHaulageAry = $this->getHaulageDetails($houlageData);
                if(!empty($WareHouseHaulageAry))
                {
                    $kConfig = new cConfig();
                    $countryNameAry = array();
                    $countryNameAry = $kConfig->getAllCountryInKeyValuePair(false,$this->idCountry);

                    $WareHouseHaulageAry['fCargoWeight'] = $this->fTotalWeight ;
                    $WareHouseHaulageAry['fCargoVolume'] = $this->fVolume ;
                    $WareHouseHaulageAry['iDirection'] = $this->iDirection ;
                    $WareHouseHaulageAry['idWarehouse'] = $this->idWarehouse ;
                    $WareHouseHaulageAry['iDistance'] = $iDistance ;
                    $WareHouseHaulageAry['szLatitude'] = $houlageData['szLatitude'];
                    $WareHouseHaulageAry['szLongitude'] = $houlageData['szLongitude'];
                    $WareHouseHaulageAry['szCityPostCode'] = $szCityPostCode ;
                    $WareHouseHaulageAry['szCountryName'] = $countryNameAry[$this->idCountry]['szCountryName'];

                    $kWHSSearch = new cWHSSearch();
                    $kWHSSearch->load($this->idWarehouse);


                    $countryNameAry = array();
                    $countryNameAry = $kConfig->getAllCountryInKeyValuePair(false,$kWHSSearch->idCountry);

                    $WareHouseHaulageAry['szWhsCity'] = $kWHSSearch->szCity;
                    $WareHouseHaulageAry['szWhsPostcode'] = $kWHSSearch->szPostCode;
                    $WareHouseHaulageAry['szWhsCountry'] = $countryNameAry[$kWHSSearch->idCountry]['szCountryName'];
                    $WareHouseHaulageAry['szWhsLatitude'] = $kWHSSearch->szLatitude;
                    $WareHouseHaulageAry['szWhsLongitude'] = $kWHSSearch->szLongitude;
                }
                return $WareHouseHaulageAry ;
            }
	}
	
        function getAddressDetailsByLatLong($szLatitude,$szLongitude)
        {
            if(!empty($szLatitude) && !empty($szLongitude))
            {    
                $kWhsSearch = new cWHSSearch();   
                $arrDescriptions = array("'__GOOGLE_MAP_V3_API_KEY__'"); 
                $bulkManagemenrVarAry = array();
                $bulkManagemenrVarAry = $kWhsSearch->getBulkManageMentVariableByDescription($arrDescriptions);      

                $szGoogleMapV3Key = $bulkManagemenrVarAry['__GOOGLE_MAP_V3_API_KEY__'];

                $url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=".$szLatitude.", ".$szLongitude."&key=".$szGoogleMapV3Key;
                
                echo "Url: ".$url;
                $c = curl_init();
                curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($c, CURLOPT_URL, $url);
                curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
                $result = curl_exec($c);
                $err  = curl_getinfo($c,CURLINFO_HTTP_CODE); 
                curl_close($c);
                print_R($result);
                
                $json = json_decode($result,true); 
                $GeoResultAry = array();                                      
                $address_ctr = 0;
                echo "<br><br> JSON respo <br><br>";
                print_R($json);
                if(!empty($json->results))
                { 
                    foreach ($json->results as $result)
                    { 
                        foreach($result->address_components as $addressPart) 
                        {  
                            if(((in_array('locality', $addressPart->types)) && (in_array('political', $addressPart->types))) || (in_array('sublocality', $addressPart->types)) && (in_array('political', $addressPart->types)))
                            {
                                $city = $addressPart->long_name;
                            }
                            else if((in_array('administrative_area_level_1', $addressPart->types)) && (in_array('political', $addressPart->types)))
                            {
                                $state = $addressPart->long_name;
                                $stateCode = $addressPart->short_name;
                            }
                            else if((in_array('country', $addressPart->types)) && (in_array('political', $addressPart->types)))
                            {
                                $country = $addressPart->long_name;
                                $countryISOCode = $addressPart->short_name;
                            }
                            else if((in_array('postal_code', $addressPart->types)))
                            {
                                $postalCode = $addressPart->long_name;
                            }
                            else if((in_array('postal_town', $addressPart->types)))
                            {
                                $city = $addressPart->long_name;
                            }
                            else if((in_array('route', $addressPart->types)))
                                $route = $addressPart->long_name;
                            else if((in_array('street_number', $addressPart->types)))
                                $street_number = $addressPart->long_name;					
                        }

                        if(trim($country)=='Kingdom of Norway')
                        {
                            $country = 'Norway' ;
                        }
                        if(empty($city))
                        {
                            $city = $state ;
                        }  
                        if($iLandingPageFlag)
                        {
                            $szLatititude = $result->geometry->location->lat;
                            $szLongitude = $result->geometry->location->lng; 

                            if($iLandingPageFlag && !empty($postalCode))
                            { 
                                $postalCode_lw = strtolower($postalCode);
                                $szCountryStr_lw = strtolower($szCountryStr); 

                                $pos = strpos($szCountryStr_lw, $postalCode_lw);    
                                if(trim($pos)!='')
                                { 
                                    $GeoResultAry[$address_ctr]['szPostCode'] = $postalCode ; 
                                } 
                                else
                                { 
                                    $GeoResultAry[$address_ctr]['szPostCode'] = "" ; 
                                }  
                            }
                            else if((strtolower($city)=='madrid') && empty($postalCode))
                            {
                                $postalCode = "28013" ; 
                                $GeoResultAry[$address_ctr]['szPostCode'] = "28013" ;
                            } 
                            else
                            {
                                $GeoResultAry[$address_ctr]['szPostCode'] = $postalCode ;
                            }
                            $GeoResultAry[$address_ctr]['szPostcodeTemp'] = $postalCode;
                            $GeoResultAry[$address_ctr]['szStreetNumber'] = $street_number ; 
                            $GeoResultAry[$address_ctr]['szRoute'] = $route ;
                            $GeoResultAry[$address_ctr]['szCityName'] = $city ;
                            $GeoResultAry[$address_ctr]['szState'] = $state ; 
                            $GeoResultAry[$address_ctr]['szStateCode'] = $stateCode ;  
                            $GeoResultAry[$address_ctr]['szCountryName'] = $country ;
                            $GeoResultAry[$address_ctr]['szCountryCode'] = $countryISOCode ; 
                            $GeoResultAry[$address_ctr]['szLat'] = $szLatititude;
                            $GeoResultAry[$address_ctr]['szLng'] = $szLongitude;
                            $address_ctr++; 
                        }
                    }
                }
                print_R($GeoResultAry);
                die;
            }
        }
	function getIdCountryFromISOCode($location)
	{
		if($location!='')
		{
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_COUNTRY__."
				WHERE
					szCountryISO='".mysql_escape_custom($location)."'
			";
			if($result = $this->exeSQL($query))
			{
				$row = $this->getAssoc($result);
				return $row['id'];
			}
		}
	}
	
	function getAllCountriesInDefinedDistance($maxDistance,$szLatitude,$szLongitude)
	{
		$query="
			SELECT 
				c.id,
				c.szCountryName 			
			, (
			( 3959 * acos( cos( radians( c.szLatitude ) ) * cos( radians( ".$szLatitude." ) ) * cos( radians( ".$szLongitude." ) - radians( c.szLongitude ) ) 
			+ sin( radians( c.szLatitude ) ) * sin( radians( ".$szLatitude." ) ) ) ) * 1.609344
			) AS iDistance
			FROM ".__DBC_SCHEMATA_COUNTRY__." AS c
			WHERE
				c.iActive='1'
			HAVING 
				iDistance < '".(int)$maxDistance."'
			ORDER BY
				szCountryName ASC
		";
		//echo "<br> ".$query;
		if($result=$this->exeSQL($query))
		{
			$countryAry=array();
			
			while($row=$this->getAssoc($result))
			{
				$countryAry[]=$row;
			}
			return $countryAry ;
		}
	}
	
	function getPositonDistanceAdded($iDistance,$idWarehouse,$idDirection,$idHaulageModel)
	{
		$query="
			SELECT
				(SELECT COUNT(*) FROM ".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." WHERE iDistanceUpToKm <= '".(float)$iDistance."' AND idHaulageModel='".(int)$idHaulageModel."' AND idWarehouse='".(int)$idWarehouse."' AND iDirection='".(int)$idDirection."') AS position
			FROM ".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
			WHERE iDistanceUpToKm = '".(float)$iDistance."'	
		";
		if($result=$this->exeSQL($query))
		{	
			if($this->iNumRows>0)
			{
				$row=$this->getAssoc($result);
				return $row['position'];
			}
			else
			{
				return 0;
			}
		
		}
		else
		{
			return 0;
		}
	}
	
	function getAllModelPricing()
	{
		$query="
			SELECT
				id,
				szModelShortName,
				szModelLongName
			FROM	
				".__DBC_SCHEMATA_HAULAGE_MODELS__."
		";
		if($result=$this->exeSQL($query))
		{	
			if($this->iNumRows>0)
			{
				while($row=$this->getAssoc($result))
				{
					$arrPricingModel[]=$row;
				}
				
				return $arrPricingModel;
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function getPriorityCFSHaulageModel($idWarehouse,$idHaulageModel,$idDirection)
	{
		$query="
			SELECT
				iPriority
			FROM
				".__DBC_SCHEMATA_CFS_MODEL_MAPPING__."
			WHERE
				idWarehouse='".(int)$idWarehouse."'
			AND
				idHaulageModel='".(int)$idHaulageModel."'
			AND
				iDirection='".(int)$idDirection."'
		";
		//echo $query;
		$result=$this->exeSQL($query);
		
		$row=$this->getAssoc($result);
		
		return $row['iPriority'];
			
	}
	
	
	function copyPostcodeData($data)
	{
		//print_r($data);
		if(count($data['szCopyZone'])<=0)
		{
			$this->addError( "szCopyZone" , t($this->t_base.'messages/atleast_one_postcode_set_is_selected'));
		}
		
		if(count($data['szCopyZonePricing'])>count($data['szCopyZone']))
		{
			$this->addError( "szCopyZone" , t($this->t_base.'messages/pricing_data_count_greater_than_postcode_data_count'));
		}
		$existZoneName=array();
		if(!empty($data['szCopyZone']))
		{
			$checkflag=false;
			foreach($data['szCopyZone'] as $szCopyZoneArr)
			{
				$arrDistance[0]=$this->loadPostCode($szCopyZoneArr);
				if($this->isZoneNameExist(0,$arrDistance[0]['szName'],$data['idZoneWarehouse'],$data['idZoneDirection'],$data['idZoneHaulageModel']))
				{
					$existZoneName[]=$arrDistance[0]['szName'];
				}
				
			}
		}
				
		if(!empty($existZoneName))
		{
			$existZoneNameStr=implode(",",$existZoneName);
			$this->addError( "szCopyZone" , t($this->t_base.'messages/already_defined_postcode_sets_msg'));
		}
		//exit if error exist.
		if ($this->error === true)
		{
			return false;
		}
		if(!empty($data['szCopyZone']))
		{
			
			$kWHSSearch = new cWHSSearch();
			$kWHSSearch->load($data['idZoneWarehouse']);
		
			foreach($data['szCopyZone'] as $szCopyZoneArr)
			{
				$postcodeArr[0]=$this->loadPostCode($szCopyZoneArr);
								
				$query="
					SELECT
						MAX(iPriority) as iPriority
					FROM
						".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
					WHERE
						idWarehouse='".(int)$data['idZoneWarehouse']."'
					AND
						iDirection ='".(int)$data['idZoneDirection']."'
					AND
						idHaulageModel='".(int)$data['idZoneHaulageModel']."'
				";
				$result=$this->exeSQL($query);
				$row=$this->getAssoc($result);
				$iPriority=$row['iPriority']+1;
				
				$query="
					INSERT INTO
						".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
					(
						idWarehouse,
						iDirection,
						idHaulageModel,
						iPriority,
						szName,
						idHaulageTransitTime,
						fWmFactor,
						fFuelPercentage,
						iActive,
						idCountry
						
					)
						VALUES
					(
						'".(int)$data['idZoneWarehouse']."',
						'".(int)$data['idZoneDirection']."',
						'".(int)$data['idZoneHaulageModel']."',
						'".(int)$iPriority."',
						'".mysql_escape_custom($postcodeArr[0]['szName'])."',
						'".(int)$postcodeArr[0]['idHaulageTransitTime']."',
						'".(float)$postcodeArr[0]['fWmFactor']."',
						'".(float)$postcodeArr[0]['fFuelPercentage']."',
						'1',
						'".(int)$postcodeArr[0]['idCountry']."'
					)
				";
				//echo $query;
				if($result=$this->exeSQL($query))
				{
					$id= $this->iLastInsertID ;
					
						if(!empty($data['szCopyZonePricing']) && in_array($szCopyZoneArr,$data['szCopyZonePricing']))
						{
							$pricingListArr=array();
							$pricingListArr=$this->getHaulagePricingZoneDetailData($szCopyZoneArr);
							//print_r($pricingListArr);
							if(!empty($pricingListArr))
							{
								foreach($pricingListArr  as $pricingListArrs)
								{
									$query="
										INSERT INTO
											".__DBC_SCHEMATA_HAULAGE_PRICING__."
										(	
											idHaulagePricingModel,
											iUpToKg,
											fPricePer100Kg,
											fMinimumPrice,
											fPricePerBooking,
											idCurrency,
											dtCreatedOn
										)
											VALUES
										(
											'".(int)$id."',
											'".(int)$pricingListArrs['iUpToKg']."',
											'".(float)$pricingListArrs['fPricePer100Kg']."',
											'".(float)$pricingListArrs['fMinimumPrice']."',
											'".(float)$pricingListArrs['fPricePerBooking']."',
											'".(int)$pricingListArrs['idCurrency']."',
											NOW()
										)
									";
									//echo $query."<br/><br/>";
									$result = $this->exeSQL( $query );
								}
							}
						}
					
						$szPostCodeArr=$this->getSetOfPostCode($szCopyZoneArr,true);	
						if(!empty($szPostCodeArr))
						{
							foreach($szPostCodeArr as $szPostCodeArrs)
							{
								$query="
									INSERT INTO
										".__DBC_SCHEMATA_HAULAGE_POSTCODE__."
									(
										szPostCode,
										idHaulagePricingModel
									)
										VALUES
									(
										'".mysql_escape_custom($szPostCodeArrs)."',
										'".(int)$id."'
									)";
								$result = $this->exeSQL( $query );
							}
						}
						
						$query="
							INSERT INTO
								".__DBC_SCHEMATA_HAULAGE_COUNTRY__."
							(
								idCountry,
								idHaulagePricingModel,
								idForwarder,
								idWarehouse,
								idDirection
							)
								VALUES
							(
								'".(int)$postcodeArr[0]['idCountry']."',
								'".(int)(int)$id."',
								'".(int)$kWHSSearch->idForwarder."',
								'".(int)$data['idZoneWarehouse']."',
								'".(int)$data['idZoneDirection']."'
								
							)";
							$result = $this->exeSQL( $query );
				
				}
			}
			return true;
		}
	}
	
	function addWarehouseCountries($idWarehouse,$data)
	{	
		if(!empty($data))
		{
			foreach($data as $datas)
			{
				$query="
					SELECT
						id
					FROM
						".__DBC_SCHEMATA_WAREHOUSE_COUNTRIES__."
					WHERE
						warehouseID='".(int)$idWarehouse."'
					AND
						countryID='".(int)$datas."'
				";
				if($result=$this->exeSQL($query))
				{	
					if($this->iNumRows<=0)
					{
					   $query="
					   		INSERT INTO
					   			".__DBC_SCHEMATA_WAREHOUSE_COUNTRIES__."
					   		(
					   			warehouseID,
					   			countryID
					   		)
					   			VALUES
					   		(
					   			'".(int)$idWarehouse."',
					   			'".(int)$datas."'
					   		)
					   ";
					   $result=$this->exeSQL($query);
					}
				}
			}
		}
	}
	/**
	*@param id IS ID OF MODELMAPPING TABLE
	*  THIS FUNCTION USES ANOTHER FUNCTION 
	*  WHICH CHANGE ITS STATUS OF FROM ACTIVE 
	*  AND VICEVERSA.
	*/
	function changeHaulageStatus($id)
	{
		$id = (int)$id;
		$row = $this->listHaulageDetails($id);
		if($row != array())
		{	
			$idMapping 	= $row['id'];
			$status 	= ($row['iActive']?'I':'A');
			$idWarehouse = $row['idWarehouse'];
			$idDirection = $row['iDirection'];
			$this->updateStatusCFSHaulageModel($idMapping,$status,$idWarehouse,$idDirection);
		}
	}
	function listHaulageDetails($id)
	{
		if($id>0)
		{
			$query = "
					SELECT 
						id,
						idWarehouse,
						idHaulageModel,
						iActive,
						iDirection
					FROM
						".__DBC_SCHEMATA_CFS_MODEL_MAPPING__."	
					WHERE
						id = ".(int)$id."	
				";
			//echo $query;
			if($result=$this->exeSQL($query))
			{
				$row=$this->getAssoc($result);
				return $row;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}	
	function changeHaulageLevel($id,$moveFlag)
	{
		if((int)$id>0)
		{	
			$row = $this->listHaulageDetails($id);
			if($row != array())
			{	
				$idMapping 	= $row['id'];
				$idWarehouse = $row['idWarehouse'];
				$idDirection = $row['iDirection'];				
				$flag = ($moveFlag?'up':'down');
				
				$this->updatePriorityCFSHaulageModel($idMapping,$flag,$idWarehouse,$idDirection);
			}
			
		}
	}
	function changeHaulageDetails($id)
	{
		$row = $this->listHaulageDetails($id);
		if($row != array())
		{	
			$idMapping 	= $row['id'];
			$idWarehouse = $row['idWarehouse'];
			$idDirection = $row['iDirection'];
			$idHaulageModel = $row['idHaulageModel'];
			
			SWITCH ((int)$idHaulageModel)
			{
				CASE 1:
				{
					$flag = '';
					BREAK;
				}
				CASE 2:
				{
					$flag = 'city';
					BREAK;
				}
				CASE 3:
				{
					$flag = 'postcode';
					BREAK;
				}
				CASE 4:
				{
					$flag = 'distance';
					BREAK;
				}
				DEFAULT :
				{
					$flag = '';
					BREAK;
				}
			
			}
			$haulageDetails = $this->viewPricingHaulageZoneModel($idWarehouse,$idHaulageModel,$idDirection,$flag);
			return $haulageDetails;
		}
		
		//
	}
	
	
	function copyCityData($data)
	{
		//print_r($data);
		if(count($data['szCopyZone'])<=0)
		{
			$this->addError( "szCopyZone" , t($this->t_base.'messages/atleast_one_city_line_is_selected'));
		}
		
		if(count($data['szCopyZonePricing'])>count($data['szCopyZone']))
		{
			$this->addError( "szCopyZone" , t($this->t_base.'messages/pricing_data_count_greater_than_city_data_count'));
		}
		$existZoneName=array();
		if(!empty($data['szCopyZone']))
		{
			$checkflag=false;
			foreach($data['szCopyZone'] as $szCopyZoneArr)
			{
				$zoneName=$this->getHaulageModelName($szCopyZoneArr);
				if($this->isZoneNameExist(0,$zoneName,$data['idZoneWarehouse'],$data['idZoneDirection'],$data['idZoneHaulageModel']))
				{
					$existZoneName[]=$zoneName;
				}
				
			}
		}
				
		if(!empty($existZoneName))
		{
			$existZoneNameStr=implode(",",$existZoneName);
			$this->addError( "szCopyZone" , t($this->t_base.'messages/already_defined_city_msg'));
		}
		//exit if error exist.
		if ($this->error === true)
		{
			return false;
		}
		if(!empty($data['szCopyZone']))
		{
			
			$kWHSSearch = new cWHSSearch();
			$kWHSSearch->load($data['idZoneWarehouse']);
		
			foreach($data['szCopyZone'] as $szCopyZoneArr)
			{
				$cityArr[0]=$this->loadCity($szCopyZoneArr);
								
				$query="
					SELECT
						MAX(iPriority) as iPriority
					FROM
						".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
					WHERE
						idWarehouse='".(int)$data['idZoneWarehouse']."'
					AND
						iDirection ='".(int)$data['idZoneDirection']."'
					AND
						idHaulageModel='".(int)$data['idZoneHaulageModel']."'
				";
				$result=$this->exeSQL($query);
				$row=$this->getAssoc($result);
				$iPriority=$row['iPriority']+1;
				
				$query="
					INSERT INTO
						".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
					(
						idWarehouse,
						iDirection,
						idHaulageModel,
						iPriority,
						szName,
						idHaulageTransitTime,
						fWmFactor,
						fFuelPercentage,
						iActive,
						iRadiousKM,
						iDistanceUpToKm,
						szLatitude,
						szLongitude,
						idCountry
					)
						VALUES
					(
						'".(int)$data['idZoneWarehouse']."',
						'".(int)$data['idZoneDirection']."',
						'".(int)$data['idZoneHaulageModel']."',
						'".(int)$iPriority."',
						'".mysql_escape_custom($cityArr[0]['szName'])."',
						'".(int)$cityArr[0]['idHaulageTransitTime']."',
						'".(float)$cityArr[0]['fWmFactor']."',
						'".(float)$cityArr[0]['fFuelPercentage']."',
						'1',
						'".(float)$cityArr[0]['iRadiousKM']."',
						'".(float)$cityArr[0]['iDistanceUpToKm']."',
						'".mysql_escape_custom($cityArr[0]['szLatitude'])."',
						'".mysql_escape_custom($cityArr[0]['szLongitude'])."',
						'".(int)$cityArr[0]['idCountry']."'
					)
				";
				//echo $query;
				if($result=$this->exeSQL($query))
				{
					$id= $this->iLastInsertID ;
					
						if(!empty($data['szCopyZonePricing']) && in_array($szCopyZoneArr,$data['szCopyZonePricing']))
						{
							$pricingListArr=array();
							$pricingListArr=$this->getHaulagePricingZoneDetailData($szCopyZoneArr);
							//print_r($pricingListArr);
							if(!empty($pricingListArr))
							{
								foreach($pricingListArr  as $pricingListArrs)
								{
									$query="
										INSERT INTO
											".__DBC_SCHEMATA_HAULAGE_PRICING__."
										(	
											idHaulagePricingModel,
											iUpToKg,
											fPricePer100Kg,
											fMinimumPrice,
											fPricePerBooking,
											idCurrency,
											dtCreatedOn
										)
											VALUES
										(
											'".(int)$id."',
											'".(int)$pricingListArrs['iUpToKg']."',
											'".(float)$pricingListArrs['fPricePer100Kg']."',
											'".(float)$pricingListArrs['fMinimumPrice']."',
											'".(float)$pricingListArrs['fPricePerBooking']."',
											'".(int)$pricingListArrs['idCurrency']."',
											NOW()
										)
									";
									//echo $query."<br/><br/>";
									$result = $this->exeSQL( $query );
								}
							}
						}
					
						$query="
							INSERT INTO
								".__DBC_SCHEMATA_HAULAGE_COUNTRY__."
							(
								idCountry,
								idHaulagePricingModel,
								idForwarder,
								idWarehouse,
								idDirection
							)
								VALUES
							(
								'".(int)$cityArr[0]['idCountry']."',
								'".(int)(int)$id."',
								'".(int)$kWHSSearch->idForwarder."',
								'".(int)$data['idZoneWarehouse']."',
								'".(int)$data['idZoneDirection']."'
								
							)";
							$result = $this->exeSQL( $query );
				}
			}
			return true;
		}
	}
  /**
		* THIS FUNCTION RETURNS ALL HAULAGES DETAILS
		*/
	function currentHaulages($contentArr = array())
	{
		if($contentArr != array())
		{
			$this->set_iPricing((int)trim($contentArr['iPricing']));
			$this->set_idCountry((int)trim($contentArr['idCountry']));
			$this->set_iDirection((int)trim($contentArr['idDirection']));
			$this->set_idForwarder((int)trim($contentArr['idForwarder']));
		
		}
		$status = '';
		//print_R($this->idCountry);
		if( ($this->iPricing || $this->iDirection || $this->idCountry || $this->idForwarder) )
		{	
			$status .= "				
				WHERE
				";
		}
		if($this->iPricing == 1)
		{
			$status .= "				
					hp.iActive = 1
				";
		
		}
		else if($this->iPricing == 2 )
		{
			$status .= "				
					hmm.iActive = 1
				";
		
		}
		//echo $this->idCountry;
		if($this->iPricing  && $this->idCountry)
		{
			$status .= "				
				AND
				";
		}
		if($this->idCountry)
		{
			$status .= "				
					c.id = '".$this->idCountry."'
				";
		
		}
		if( ($this->iDirection && $this->idCountry) || ($this->iPricing && $this->iDirection) )
		{
			$status .= "				
				AND
				";
		}
		if($this->iDirection)
		{
			$status .= "				
					hmm.iDirection = '".$this->iDirection."'
				";
		
		}
		if($this->iDirection && $this->idForwarder || ($this->iPricing && $this->idForwarder) )
		{
			$status .= "				
				AND
				";
		}
		if($this->idForwarder)
		{
			$status .= "				
				fwd.id = '".$this->idForwarder."'
				";
		
		}
		
		$query = "
			SELECT 
				hm.id as idHaulage,
				hmm.iActive,
				hmm.id,
				hmm.idWarehouse,
				fwd.szDisplayName,
				wh.szCity,
				wh.idCountry,
				wh.szWareHouseName,
				hmm.iDirection,
				hmm.iPriority,
				c.szCountryName,
				hm.szModelShortName
			FROM
				".__DBC_SCHEMATA_CFS_MODEL_MAPPING__." AS hmm
			INNER JOIN 
				".__DBC_SCHEMATA_WAREHOUSES__."	as wh
			ON( hmm.idWarehouse = wh.id and wh.iActive = 1 )	
			INNER JOIN
				".__DBC_SCHEMATA_FROWARDER__."	as fwd
			ON( wh.idForwarder = fwd.id  and fwd.iActive = 1 )	
			INNER JOIN
				".__DBC_SCHEMATA_HAULAGE_MODELS__."	AS hm
			ON(hm.id = hmm.idHaulageModel)	
			LEFT JOIN 
				".__DBC_SCHEMATA_COUNTRY__." AS c
			ON ( wh.idCountry = c.id and c.iActive = 1)	
			LEFT JOIN	
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." AS hp
			ON (hmm.idWarehouse = hp.idWarehouse && hmm.iDirection = hp.iDirection)	
			".$status."					
			GROUP BY
				wh.id,
				hmm.iDirection,
				hmm.iPriority	
			ORDER BY
				fwd.szDisplayName,
				c.szCountryName,
				wh.szCity,
				wh.szWareHouseName,
				hmm.iDirection,
				hmm.iPriority
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			$listHaulage = array();
			while($row=$this->getAssoc($result))
			{
				$listHaulage[] = $row;
			}
			return $listHaulage;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function findWarehouseName($id)
	{
		if($id>0)
		{
			$query = "
					SELECT 
						szWareHouseName		
					FROM
						".__DBC_SCHEMATA_WAREHOUSES__."	
					WHERE
						id = ".(int)$id."	
				";
			//echo $query;
			if($result=$this->exeSQL($query))
			{
				$row=$this->getAssoc($result);
				return $row['szWareHouseName'];
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function findHaulageModelName($id)
	{
		if($id>0)
		{
			$query = "
					SELECT 
						szModelShortName		
					FROM
						".__DBC_SCHEMATA_HAULAGE_MODELS__."	
					WHERE
						id = ".(int)$id."	
				";
			//echo $query;
			if($result=$this->exeSQL($query))
			{
				$row=$this->getAssoc($result);
				return $row['szModelShortName'];
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
        function getAllLemonHaulageList()
	{
            $query = "
                SELECT m.id as idHaulageModel , w.szWareHouseName
                FROM `tblhaulagepricingmodels` m
                INNER JOIN tblwarehouses w ON w.id = m.idWarehouse
                WHERE w.szWareHouseName LIKE 'LEMAN%' AND w.szWareHouseName!='LEMAN Taulov'	
            ";
            //echo $query;
            if($result=$this->exeSQL($query))
            {
                $ret_ary = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    $ret_ary[$ctr] = $row['idHaulageModel'];
                    $ctr++;
                }
                return $ret_ary;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
	}
        
	function updateHaulageVideoFlag($idForwarderContact,$updateFlag)
	{
		if((int)$idForwarderContact>0)
		{
			$query="
				UPDATE
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
				SET
					iHaulageVideo='".(int)$updateFlag."'
				WHERE
					id='".(int)$idForwarderContact."'
			";
			//echo $query;
			$result=$this->exeSQL($query);
		}
	}
	
	function getDistanceForShowCityOnMap($latlongArr,$szLongitude,$szLatitude)
	{
		$query = "
			SELECT 
				(( 3959 * acos( cos( radians(".$latlongArr[0]['szLatitude'].") ) * cos( radians( ".$szLatitude." ) ) 
				   * cos( radians(".$szLongitude.") - radians(".$latlongArr[0]['szLongitude'].")) + sin(radians(".$latlongArr[0]['szLatitude'].")) 
				   * sin( radians(".$szLatitude.")))) * 1.609344) as iDistance
		" ;
		//echo "<br />".$query." <br />";
		if($result = $this->exeSQL($query))
		{
			$row = $this->getAssoc($result);
			$distance= round((float)$row['iDistance'],2);
		}
		
		return $distance;
	}
        
        function importHaluageData($idWarehouse,$idWarehouseMap,$idForwarder)
        {
            $query="
                SELECT
                    id,
                    idWarehouse,
                    iDirection,
                    idHaulageModel,
                    iPriority,
                    szName,
                    idHaulageTransitTime,
                    fWmFactor,
                    fFuelPercentage,
                    iActive,
                    iRadiousKM,
                    iDistanceUpToKm,
                    szLatitude,
                    szLongitude,
                    idCountry
                FROM
                    ".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." 
                WHERE
                    idWarehouse='".(int)$idWarehouse."'
                AND
                    iActive='1'
        ";
        //echo $query."<br /><br />";
        if($result=$this->exeSQL($query))
        {
                if($this->iNumRows>0)
                {
                        while($row=$this->getAssoc($result))
                        {
                                //$zone_arr[]=$row;
                            
                            $queryInsert="
                                INSERT INTO
                                    ".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."
                                (
                                        idWarehouse,
                                        iDirection,
                                        idHaulageModel,
                                        iPriority,
                                        szName,
                                        idHaulageTransitTime,
                                        fWmFactor,
                                        fFuelPercentage,
                                        iActive,
                                        iRadiousKM,
                                        iDistanceUpToKm,
                                        szLatitude,
                                        szLongitude,
                                        idCountry
                                )
                                    VALUES
                                (
                                    '".(int)$idWarehouseMap."',
                                    '".(int)$row['iDirection']."',
                                    '".(int)$row['idHaulageModel']."',
                                    '".(int)$row['iPriority']."',
                                    '".mysql_escape_custom($row['szName'])."',
                                    '".(int)$row['idHaulageTransitTime']."',
                                    '".(float)$row['fWmFactor']."',
                                    '".(float)$row['fFuelPercentage']."',
                                    '1',
                                    '".(float)$row['iRadiousKM']."',
                                    '".(float)$row['iDistanceUpToKm']."',
                                    '".mysql_escape_custom($row['szLatitude'])."',
                                    '".mysql_escape_custom($row['szLongitude'])."',
                                    '".(int)$row['idCountry']."'
                                )
                        ";
                            //echo $queryInsert;
                            $resultNew=$this->exeSQL($queryInsert);
                            $iLastInserted=$this->iLastInsertID;
                            if($row['idHaulageModel']==4)
                            {
                                $queryCountry="
                                    SELECT
                                        hd.idCountry
                                    FROM
                                        ".__DBC_SCHEMATA_HAULAGE_COUNTRY__." AS hd
                                    WHERE
                                        hd.idHaulagePricingModel='".(int)$row['id']."'	
                                ";
                                if($resultCountry=$this->exeSQL($queryCountry))
                                {
                                    if($this->iNumRows>0)
                                    {
                                        while($rowCountry=$this->getAssoc($resultCountry))
                                        {
                                            $queryHaluageCountry="
                                                INSERT INTO
                                                        ".__DBC_SCHEMATA_HAULAGE_COUNTRY__."
                                                (
                                                        idCountry,
                                                        idHaulagePricingModel,
                                                        idForwarder,
                                                        idWarehouse,
                                                        idDirection
                                                )
                                                        VALUES
                                                (
                                                        '".(int)$rowCountry['idCountry']."',
                                                        '".(int)$iLastInserted."',
                                                        '".(int)$idForwarder."',
                                                        '".(int)$idWarehouseMap."',
                                                        '".(int)$row['idDirection']."'

                                                )";
                                            $resultHaluageCountry=$this->exeSQL($queryHaluageCountry);
                                        }
                                    }
                                }
                                
                            }
                            else if($row['idHaulageModel']==3)
                            {
                                $queryPostcode="
                                    SELECT
                                        szPostCode
                                    FROM
                                        ".__DBC_SCHEMATA_HAULAGE_POSTCODE__."
                                    WHERE
                                        idHaulagePricingModel = '".(int)$row['id']."'
                                ";
                                if($resultPostcode=$this->exeSQL($queryPostcode))
                                {
                                    if($this->iNumRows>0)
                                    {
                                        while($rowPostcode=$this->getAssoc($resultPostcode))
                                        {
                                            $InsertHaulagePostcode="
                                                INSERT INTO
                                                    ".__DBC_SCHEMATA_HAULAGE_POSTCODE__."
                                                (
                                                    idHaulagePricingModel,
                                                    szPostCode
                                                )
                                                    VALUES
                                                (
                                                    '".(int)$iLastInserted."',
                                                    '".mysql_escape_custom($rowPostcode['szPostCode'])."'    
                                                )
                                                ";
                                                $resultHaulagePostcode=$this->exeSQL($InsertHaulagePostcode);
                                        }
                                    }
                                }
                            }
                            else if($row['idHaulageModel']==1)
                            {
                                $queryZone="
				SELECT
                                    szLatitude,
                                    szLongitude,
                                    iVertexSequence
                                FROM
                                        ".__DBC_SCHEMATA_HAULAGE_ZONE__."
                                WHERE
                                        idHaulagePricingModel='".(int)$row['id']."'
                                ORDER BY
                                        iVertexSequence ASC
                                ";
                                if($resultZone=$this->exeSQL($queryZone))
                                {
                                    if($this->iNumRows>0)
                                    {
                                        while($rowZone=$this->getAssoc($resultZone))
                                        {
                                            $InsertHaulageZone="
                                                INSERT INTO
                                                    ".__DBC_SCHEMATA_HAULAGE_ZONE__."
                                                (
                                                    idHaulagePricingModel,
                                                    szLatitude,
                                                    szLongitude,
                                                    iVertexSequence
                                                )
                                                    VALUES
                                                (
                                                    '".(int)$iLastInserted."',
                                                    '".mysql_escape_custom($rowZone['szLatitude'])."',
                                                    '".mysql_escape_custom($rowZone['szLongitude'])."',
                                                    '".mysql_escape_custom($rowZone['iVertexSequence'])."'   
                                                )";
                                                $resultHaulageZone=$this->exeSQL($InsertHaulageZone);
                                        }
                                    }
                                }
                                
                                
                                $queryZoneC="
                                    SELECT
                                        idCountry
                                    FROM
                                        ".__DBC_SCHEMATA_HAULAGE_ZONES_COUNTRY_DETAILS__."
                                    WHERE
                                        idHaulagePricingModel = '".(int)$row['id']."'
                                ";
                                if($resultZoneC=$this->exeSQL($queryZoneC))
                                {
                                    if($this->iNumRows>0)
                                    {
                                        while($rowZoneC=$this->getAssoc($resultZoneC))
                                        {
                                            $InsertHaulageCountry="
                                                INSERT INTO
                                                    ".__DBC_SCHEMATA_HAULAGE_ZONES_COUNTRY_DETAILS__."
                                                (
                                                    idHaulagePricingModel,
                                                    idCountry
                                                )
                                                    VALUES
                                                (
                                                    '".(int)$iLastInserted."',
                                                    '".(int)$rowZoneC['idCountry']."'    
                                                )
                                                ";
                                                $resultHaulageCountry=$this->exeSQL($InsertHaulageCountry);
                                        }
                                    }
                                }
                            }
                        }
                        //return $zone_arr;
                }
                else
                {
                        return array();
                }
        }
        else
        {
                return array();
        }
        }
        
        function updateHaulageZoneCountries()
        { 
            $kHaulagePricing = new cHaulagePricing();
            $countryAry = array();
            $idHaulagePricingModelAry = array();
            
            $szCalculationLogs = "<br /> Script started on: ".date('d/m/Y H:i:s');
            $countryAry = $kHaulagePricing->getAllActiveCountry();
            $idHaulagePricingModelAry = $kHaulagePricing->getAllUpdatableZones();

            $today = date('Y-m-d');
            $iTotalNumRequestSent = $kHaulagePricing->getTotalNumRequestSent($today);
            $iNumRequestSent = $iTotalNumRequestSent;
            if(!empty($countryAry) && !empty($idHaulagePricingModelAry))
            {
		$counter=0;
		foreach($idHaulagePricingModelAry as $idHaulagePricingModelArys)
		{
                    $szCalculationLogs .= "<br /> Started working for Pricing Id: ".$idHaulagePricingModelArys['idHaulagePricingModel']."<br /><br />";
                    $countryIsoAry = array();
                    $szCalculationLogs .= "<br /> checking in country for Zone ID:".$idHaulagePricingModelArys['idHaulagePricingModel']." on ".date('d/m/Y H:i:s');

                    $zoneVertexAry = $kHaulagePricing->getAllVerticesOfZone($idHaulagePricingModel);
                    $zoneLatLongAry = array();

                    $zoneLatLongAry['szMaxLatitude'] = $kHaulagePricing->szMaxLatitude ;
                    $zoneLatLongAry['szMinLatitude'] = $kHaulagePricing->szMinLatitude ;
                    $zoneLatLongAry['szMaxLongitude'] = $kHaulagePricing->szMaxLongitude ;
                    $zoneLatLongAry['szMinLongitude'] = $kHaulagePricing->szMinLongitude ;

                    foreach($countryAry as $countryArys)
                    {			
                        $data = array();
                        $latLongPointsAry = array();
                        $data['szLatitude'] = $countryArys['szLatitude'] ;
                        $data['idCountry'] = $countryArys['id'] ;
                        $data['szLongitude'] = $countryArys['szLongitude'] ;
                        $data['idHaulagePricingModel'] = $idHaulagePricingModelArys['idHaulagePricingModel'];	

                        if($kHaulagePricing->isLocationExistsInZone($data,$zoneVertexAry,$zoneLatLongAry))
                        {
                            $countryIsoAry['szCountryCode'][$counter] = $countryArys['szCountryISO'];
                            $countryIsoAry['idCountry'][$counter] = $countryArys['id'] ;
                            $countryIsoAry['idHaulagePricingModel'][$counter] = $idHaulagePricingModelArys['idHaulagePricingModel'] ;
                            $countryIsoAry['szGoogleCode'][$counter] = $url ;
                            $counter++;			    	 
                            $szCalculationLogs .= "<br/> ".$countryArys['szCountryName']." exists in Zone ID ".$idHaulagePricingModelArys['idHaulagePricingModel']." with Latitude: ".$countryArys['szLatitude']." and Longitude: ".$countryArys['szLongitude']."<br/>";
                        }
                    }			
                    $data = array();
                    $latLongPointsAry = array();
                    $zoneVertexAry = array();
                    $data['idHaulagePricingModel'] = $idHaulagePricingModelArys['idHaulagePricingModel'];
                    $latLongPointsAry = $kHaulagePricing->isLocationExistsInZone_cronjob($data);
                    $zoneVertexAry = $kHaulagePricing->zoneVertexGlobalAry;

                    if(!empty($zoneVertexAry))
                    {
                        $szCalculationLogs .= ' 	
                            <table cellspacing="0" cellpadding="0" border="0" class="format-3" width="60%" >
                                <tr>
                                    <th align="left">PricingID</th>
                                    <th align="left">Latitude</th>
                                    <th align="left">Longitude</th>
                                    <th align="left">Length</th>
                                    <th align="center">Points</th>
                                </tr> 
                        ';
                        $vertex_ctr=0;
                        foreach($zoneVertexAry as $zoneVertexArys)
                        {
                            if($kHaulagePricing->Y_axis>0)
                            {
                                $loop_counter = ceil($zoneVertexAry[$vertex_ctr-1]['iLength']/$kHaulagePricing->Y_axis);
                            } 
                            $iVerticesLength = ($vertex_ctr>0?$zoneVertexAry[$vertex_ctr-1]['iLength']:'');
                            $szCalculationLogs .= ' 
                                <tr>
                                    <td>'.$data['idHaulagePricingModel'].'</td>
                                    <td>'.$zoneVertexArys['szLatitude'].'</td>
                                    <td>'.$zoneVertexArys['szLongitude'].'</td>
                                    <td>'.$iVerticesLength.'</td>
                                    <td>'.$loop_counter.'</td>
                                </tr>
                            ';
                            $vertex_ctr++; 
                        }  
                        $szCalculationLogs .= '</table>'; 
                    } 
                    
                    $szCalculationLogs .= "<br /> Number of points to test: ".$kHaulagePricing->iNumPointsToTest;
                    $szCalculationLogs .= "<br /> Y = ".$kHaulagePricing->Y_axis."<br />" ;

                    $verticesDifferenceAry = $kHaulagePricing->verticesDifferenceAry ;

                    if(!empty($verticesDifferenceAry))
                    { 
                        $szCalculationLogs .= ' 
                            <table cellspacing="0" cellpadding="0" border="0" class="format-3" width="20%" >
                            <tr>
                                <th align="left">Vertices</th>
                                <th align="center">X</th>
                            </tr>
                        '; 
                        $vertex_ctr=0;
                        foreach($verticesDifferenceAry as $verticesDifferenceArys)
                        {
                            if($vertex_ctr>0)
                            {						
                                $szCalculationLogs .=' 
                                    <tr>
                                        <td>'.$vertex_ctr.' to '.(($vertex_ctr+1)==count($verticesDifferenceAry)?1:$vertex_ctr+1).'</td>
                                        <td>'.$verticesDifferenceAry[$vertex_ctr]['X_Axis'].'</td>
                                    </tr>
                                '; 
                            }			
                            $vertex_ctr++; 
                        } 
			$szCalculationLogs .= '</table>'; 
                    }  
                    if($iTotalNumRequestSent>0)
                    {
                        $expactedNumRequest = $iNumRequestSent + ($kHaulagePricing->iTotalNumRequest);
                        $kWHSSearch = new cWHSSearch();
                        $szGoogleApiMaxRequestNumber = $kWHSSearch->getManageMentVariableByDescription('__GOGGLE_GEOCODE_API_MAX_NUM_REQUEST__'); 
                    }
                    else
                    {
                        $iTotalNumRequestSent = $iNumRequestSent ;
                    }
                    
                    if(!empty($latLongPointsAry))
		    {
                        $kWHSSearch = new cWHSSearch();
                        $ctr=0;

                        $szCalculationLogs .= "<br /> started calling API for Zone ID:".$idHaulagePricingModelArys['idHaulagePricingModel']." on ".date('d/m/Y H:i:s');
                                
                        $szMaximumLatitude = $latLongPointsArys['szLatitude'][0]; 
                        $szMaximumLongitude = $latLongPointsArys['szLongitude'][0]; 
                        
                        $counter = 0;
                        foreach($latLongPointsAry as $latLongPointsArys)
                        {
                            $loop_coun = count($latLongPointsArys['szLatitude']);			  		
                            for($i=0;$i<$loop_coun;$i++)
                            {
                                $lok2 = $latLongPointsArys['szLatitude'][$i];
                                $lok1 = $latLongPointsArys['szLongitude'][$i]; 

                                /*
                                  fetching maximum Latitude 
                                */
                                if($szMaximumLatitude < $lok2)
                                {
                                    $szMaximumLatitude = $lok2; 
                                } 

                                /*
                                  fetching minimum Latitude 
                                */  
                                if(empty($szMinimumLatitude))
                                {
                                    $szMinimumLatitude = $lok2;
                                }
                                else if($szMinimumLatitude > $lok2 && !empty($lok2))
                                {  
                                    $szMinimumLatitude = $lok2;
                                }

                                /*
                                    fetching maximum Longitude 
                                */
                                if($szMaximumLongitude < $lok1)
                                {
                                    $szMaximumLongitude = $lok1; 
                                } 

                                /*
                                fetching minimum Longitude 
                                */ 
                                if(empty($szMinimumLongitude))
                                {
                                    $szMinimumLongitude = $lok1;
                                }
                                else if($szMinimumLongitude > $lok1 && !empty($lok1))
                                {
                                    $szMinimumLongitude = $lok1; 
                                } 
                                
                                $szCountryCode = $this->getCountryCodeByHaulageZone($lok1,$lok2); 
                                $url = $this->szGoogleNameUrl;
                                
                                if($szCountryCode!='')
                                {
                                    $iErrorCode=0;
                                }   
                                if($iErrorCode==0)
                                {  
                                    if(!empty($szCountryCode))
                                    {
                                        $kConfig = new cConfig();
                                        $latLongCountry = $kConfig->getCountryName(false,$szCountryCode);
					        	
                                        if(!empty($countryIsoAry) && !in_array($latLongCountry,$countryIsoAry['idCountry']))
                                        {
                                            $countryIsoAry['szCountryCode'][$counter] = $szCountryCode ;
                                            $countryIsoAry['idCountry'][$counter] = $latLongCountry ;
                                            $countryIsoAry['idHaulagePricingModel'][$counter] = $idHaulagePricingModelArys['idHaulagePricingModel'] ;
                                            $countryIsoAry['szGoogleCode'][$counter] = $url ;
                                            $counter++;
                                        }
                                        else if(empty($countryIsoAry))
                                        {
                                            $countryIsoAry['szCountryCode'][$counter] = $szCountryCode ;
                                            $countryIsoAry['idHaulagePricingModel'][$counter] = $idHaulagePricingModelArys['idHaulagePricingModel'] ;
                                            $countryIsoAry['idCountry'][$counter] = $latLongCountry ;
                                            $countryIsoAry['szGoogleCode'][$counter] = $url ;
                                            $counter++;
                                        }
                                     }
                                }
                                else if($iErrorCode>0)
                                {					  
                                    $szCountry = $szErrorMessage ;
                                    $szCountryCode = $iErrorCode ;
                                }
                                $iNumRequestSent++;
                                $apiResponseAry[$ctr]['szApiResponse'][] = $szCountry."(".$szCountryCode.")";
                                
                                $globallatLongPointsAry[$idHaulagePricingModelArys['idHaulagePricingModel']]['szLatitude'][] = $latLongPointsArys['szLatitude'][$i] ;
                                $globallatLongPointsAry[$idHaulagePricingModelArys['idHaulagePricingModel']]['szLongitude'][] = $latLongPointsArys['szLongitude'][$i] ;
                                $globallatLongPointsAry[$idHaulagePricingModelArys['idHaulagePricingModel']]['szApiResponse'][] = $szCountry."(".$szCountryCode.")";
                            }
                            $ctr++;
                        } 
                        
                        if(!empty($countryIsoAry))
                        {
                            $kConfig = new cConfig();
                            $szCalculationLogs .= "<br /> List of countries covered in zone ID :".$idHaulagePricingModelArys['idHaulagePricingModel']." <br />";
                            $kHaulagePricing->deleteOldHaulageCountry($idHaulagePricingModelArys['idHaulagePricingModel']);

                            for($x=0;$x<count($countryIsoAry['szCountryCode']);$x++)
                            {
                                $data=array();
                                $data['idCountry'] = $countryIsoAry['idCountry'][$x];
                                $data['szGoogleCode'] = $countryIsoAry['szGoogleCode'][$x];
                                $data['szCountryCode'] = $countryIsoAry['szCountryCode'][$x];
                                $data['idHaulagePricingModel'] = $countryIsoAry['idHaulagePricingModel'][$x];
                                $countryAry = $kConfig->getAllCountryInKeyValuePair($data['idCountry']);
                                $szCalculationLogs .= $countryAry[$data['idCountry']]['szCountryName']."<br/>";
                                $kHaulagePricing->addHaulageZonesCountries($data);

                                //echo "<br /> Putting data for warehouse id ".$kHaulagePricing->idWarehouse ;
                                //echo " and country id ".$data['idCountry']."<br />";
                                //$kHaulagePricing->addHaulageWarehouseCountries($data['idCountry'],$kHaulagePricing->idWarehouse);
                            } 
                        }
		    	else
                        {
                            $szCalculationLogs .= "<br/><br/> Zone ID: ".$idHaulagePricingModelArys['idHaulagePricingModel']." does not belongs to any country."; 
                        }			    
                    }
                    
                    if(!empty($globallatLongPointsAry))
                    { 
                        $szCalculationLogs .= '<br /><br />
                            <table cellspacing="0" cellpadding="0" border="0" class="format-3" width="40%" >
                            <tr>
                                <th align="left">Points</th>
                                <th align="left">Latitude</th>
                                <th align="left">Longitude</th>	
                                <th align="left">Api Response</th>				
                            </tr>	
                        '; 
                        $vertex_ctr=1;
                        foreach($globallatLongPointsAry as $key=>$latLongPointsArys)
                        {
                            $loop_coun = count($latLongPointsArys['szLatitude']) ;
                            $szCalculationLogs .= ' 
                                <tr>
                                    <td colspan="4"><h3>Points for Haulage pricing ID: '.$key.'</h3></td>
                                </tr>
                            '; 
                            for($i=0;$i<($loop_coun);$i++)
                            {	
                                if(!empty($latLongPointsArys['szLatitude'][$i]))
                                {			 
                                    $szCalculationLogs .= '		
                                        <tr>
                                            <td>Point '.($i+1).'</td>
                                            <td>'.round($latLongPointsArys['szLatitude'][$i],6).'</td>
                                            <td>'.round($latLongPointsArys['szLongitude'][$i],6).'</td>
                                            <td>'.$latLongPointsArys['szApiResponse'][$i].'</td>								
                                        </tr> 
                                    '; 
                                }	
                            }	
                        }		
                        $vertex_ctr++; 
                    } 
                    $szCalculationLogs .= '</table>'; 
                    
                    $szCalculationLogs .= " <br> Checking additional points to consider vertices in Island ";
                            
                    $szCalculationLogs .= "<br> Max Latitude: ".$szMaximumLatitude;
                    $szCalculationLogs .= "<br> Min Latitude: ".$szMinimumLatitude;
                    $szCalculationLogs .= "<br> Max Longitude: ".$szMaximumLongitude;
                    $szCalculationLogs .= "<br> Min Longitude: ".$szMinimumLongitude;

                    /*
                    * Searching for centeral points 
                    */
                    $szCentralLatitude = (($szMaximumLatitude - $szMinimumLatitude )/2) + $szMinimumLatitude ;
                    $szCentralLongitude = (($szMaximumLongitude - $szMinimumLongitude )/2) + $szMinimumLongitude ;

                    /*
                     * CASE I: For the zone (all vertexes), take (max(szLatitude) - min(szLatitude))/2 + min(szLatitude), this is the Latitude we want to test
                        For the zone (all vertexes), take (max(szLongitude) - min(szLongitude))/2 + min(szLongitude), this is the Longitude we want to test

                        Test that this new point is inside the zone (like we do when we check whether a customer location is inside a zone).

                        If Yes, then test that point for country and if a country is returned, then stop.

                        CASE 2:  If No or if no country is returned, then for these 4 points:
                        1. (max(szLatitude) - min(szLatitude))/4 + min(szLatitude)
                           (max(szLongitude) - min(szLongitude))/4 + min(szLongitude)

                        2. (max(szLatitude) - min(szLatitude))/4 + min(szLatitude)
                           (max(szLongitude) - min(szLongitude))*3/4 + min(szLongitude)

                        3. (max(szLatitude) - min(szLatitude))*3/4 + min(szLatitude)
                           (max(szLongitude) - min(szLongitude))/4 + min(szLongitude)

                        4. (max(szLatitude) - min(szLatitude))*3/4 + min(szLatitude)
                           (max(szLongitude) - min(szLongitude))*3/4 + min(szLongitude) 
                        Test that the new points are inside the zone, and for those which are inside the zone, test for country.

                     */
                    $szCalculationLogs .= "<br><br> Central Lat: ".$szCentralLatitude; 
                    $szCalculationLogs .= "<br> Central Long: ".$szCentralLongitude; 


                    $szCentralLatitude_2 = (($szMaximumLatitude - $szMinimumLatitude )/4) + $szMinimumLatitude ;
                    $szCentralLongitude_2 = (($szMaximumLongitude - $szMinimumLongitude )/4) + $szMinimumLongitude ;

                    $szCentralLatitude_3 = (($szMaximumLatitude - $szMinimumLatitude )/4) + $szMinimumLatitude ;
                    $szCentralLongitude_3 = (($szMaximumLongitude - $szMinimumLongitude)*3/4) + $szMinimumLongitude ;

                    $szCentralLatitude_4 = (($szMaximumLatitude - $szMinimumLatitude )*3/4) + $szMinimumLatitude ;
                    $szCentralLongitude_4 = (($szMaximumLongitude - $szMinimumLongitude)/4) + $szMinimumLongitude ;

                    $szCentralLatitude_5 = (($szMaximumLatitude - $szMinimumLatitude )*3/4) + $szMinimumLatitude ;
                    $szCentralLongitude_5 = (($szMaximumLongitude - $szMinimumLongitude)*3/4) + $szMinimumLongitude ;

                    $islandCordinatesAry = array();
                    $islandCordinatesAry[0]['szLatitude'] = $szCentralLatitude;
                    $islandCordinatesAry[0]['szLongitude'] = $szCentralLongitude;
                    $islandCordinatesAry[1]['szLatitude'] = $szCentralLatitude_2;
                    $islandCordinatesAry[1]['szLongitude'] = $szCentralLongitude_2; 
                    $islandCordinatesAry[2]['szLatitude'] = $szCentralLatitude_3;
                    $islandCordinatesAry[2]['szLongitude'] = $szCentralLongitude_3; 
                    $islandCordinatesAry[3]['szLatitude'] = $szCentralLatitude_4;
                    $islandCordinatesAry[3]['szLongitude'] = $szCentralLongitude_4; 
                    $islandCordinatesAry[4]['szLatitude'] = $szCentralLatitude_5;
                    $islandCordinatesAry[4]['szLongitude'] = $szCentralLongitude_5;  

                    if(!empty($islandCordinatesAry))
                    {
                        foreach($islandCordinatesAry as $islandCordinatesArys)
                        { 
                            $islandInputAry = array(); 
                            $islandInputAry['szLatitude'] = $islandCordinatesArys['szLatitude'] ; 
                            $islandInputAry['szLongitude'] = $islandCordinatesArys['szLongitude'] ; 
                            $islandInputAry['idHaulagePricingModel'] = $idHaulagePricingModelArys['idHaulagePricingModel'];	

                            if($this->checkingHaulageZoneIslands($islandInputAry,$zoneVertexAry,$zoneLatLongAry))
                            {
                                $szCalculationLogs .= $this->szCalculationLogsInslands;
                                break;
                            }
                            else
                            {
                                $szCalculationLogs .= $this->szCalculationLogsInslands;
                            }
                        }
                    }
                    $this->updateHaulageZones($idHaulagePricingModelArys['idHaulagePricingModel']);
                    $iHaulageZoneProcess++ ;
                    $iTotalNumRequestSent += $iNumRequestSent ;

                    $szCalculationLogs .= "<br /> Execution completed for Zone ID:".$idHaulagePricingModelArys['idHaulagePricingModel']." on ".date('d/m/Y H:i:s');
		} 
		$kHaulagePricing->addYahooApiLogs($iNumRequestSent);
		
		$szCalculationLogs .= "<br><br> Script stoped on ".date('d/m/Y H:i:s');
                $this->szLastHaulageZoneUpdatingLogs = $szCalculationLogs;
                return true;
            }
        }
        
        function checkingHaulageZoneIslands($data,$zoneVertexAry,$zoneLatLongAry)
        {
            if(!empty($data))
            {
                $szCalculationLogs = "<br> Checking Location for Lat: ".$data['szLatitude']." Long: ".$data['szLongitude'];
                $kHaulagePricing = new cHaulagePricing();
                $bCalculateCase2 = false;
                if($kHaulagePricing->isLocationExistsInZone($data))
                {
                    $szCalculationLogs .= nl2br($kHaulagePricing->szLocationFindingLogs);
                    $szCalculationLogs .= "<br> Location exists in zone: Yes";

                    $szCentralLatitude = $data['szLatitude']; 
                    $szCentralLongitude = $data['szLongitude'];
                            
                    $szCountryCodeByCentralPoint = $this->getCountryCodeByHaulageZone($szCentralLongitude,$szCentralLatitude);
                    $url = $this->szGoogleNameUrl;
                    if(!empty($szCountryCodeByCentralPoint))
                    {
                        $kConfig = new cConfig();
                        $latLongCountry = $kConfig->getCountryName(false,$szCountryCodeByCentralPoint);

                        $szCalculationLogs .= "<br> ".$szCountryCodeByCentralPoint."<br>";
                        
                        $addZoneAry = array();
                        $addZoneAry['idCountry'] = $latLongCountry;
                        $addZoneAry['szGoogleCode'] = $url;
                        $addZoneAry['szCountryCode'] = $szCountryCodeByCentralPoint;
                        $addZoneAry['idHaulagePricingModel'] = $data['idHaulagePricingModel'];
                        $kHaulagePricing->addHaulageZonesCountries($addZoneAry);  
                    }
                    else
                    {
                        $szCalculationLogs .= "<br> No country found for central points";
                        $bCalculateCase2 = true;
                    }
                }
                else
                { 
                    $szCalculationLogs .= nl2br($kHaulagePricing->szLocationFindingLogs);
                    $szCalculationLogs .= "<br> Location exists in zone: No";
                    $bCalculateCase2 = true;
                }

                $this->szCalculationLogsInslands = $szCalculationLogs;
                if($bCalculateCase2)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        function getCountryCodeByHaulageZone($lok1,$lok2)
        {
            if(!empty($lok1) && !empty($lok2))
            {
                $kWHSSearch = new cWHSSearch();
                $response=array();
                $geocode='';
                $url = "http://ws.geonames.org/countryCode?type=JSON&lat=".round($lok2,6)."&lng=".round($lok1,6)."&username=ajaywhiz";
                $this->szGoogleNameUrl = $url;
                //echo "<br/><br/>".$url."<br/><br/>";
                $geocode = $kWHSSearch->curl_get_file_contents($url);

                $response = json_decode($geocode,true);  
                if($response['countryCode']!='')
                {
                    $iErrorCode=0;
                    return $response['countryCode'];
                }  
                else
                {
                    return false;
                }
                /*
                    The Web service returns the following error codes in the response:
                    0: No error
                    1: Feature not supported
                    100: No input parameters
                    102: Address data not recognized as valid UTF-8
                    103: Insufficient address data
                    104: Unknown language
                    105: No country detected
                    106: Country not supported
                    10NN: Internal problem detected				  			
                */
                //var_dump($response); 
            }
        }
        
	function set_id( $value )
	{
		$this->id = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "id", t($this->t_base.'fields/zone_pricing_line_id'), false, false, true );
	}
	function set_fPricePer100Kg( $value )
	{
		$this->fPricePer100Kg = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fPricePer100Kg", t($this->t_base.'fields/price_per_100_kg'), 0, false, true );
	}
	function set_iUpToKg( $value )
	{
		$this->iUpToKg = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iUpToKg", t($this->t_base.'fields/weight_up_to'), 1, false, true );
	}
	function set_fMinimumPrice( $value )
	{
		$this->fMinimumPrice = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fMinimumPrice", t($this->t_base.'fields/minimum_price'), 0, false, true );
	}
	function set_fPricePerBooking( $value )
	{
		$this->fPricePerBooking = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fPricePerBooking",  t($this->t_base.'fields/price_per_booking'), 0, false, true );
	}
	function set_idCurrency( $value )
	{
		$this->idCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idCurrency",  t($this->t_base.'fields/currency'), false, false, true );
	}
	
	function set_szName( $value ,$maxvalue=false)
	{
		$this->szName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szName",  t($this->t_base.'fields/name'), false, $maxvalue, true );
	}
	
	function set_fFuelPercentage( $value,$maxFuel,$miniFuel )
	{
		$this->fFuelPercentage = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fFuelPercentage",  t($this->t_base.'fields/fuel_percentage'),$miniFuel,$maxFuel, false );
	}
	
	function set_fWmFactor( $value,$maxWMFactor,$miniWMFactor )
	{
		$this->fWmFactor = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fWmFactor",  t($this->t_base.'fields/fWmFactor'),$miniWMFactor,$maxWMFactor, true );
	}
	
	function set_idTransitTime( $value )
	{
		$this->idTransitTime = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idTransitTime",  t($this->t_base.'fields/transit_time'), false, false, true );
	}
	
	function set_idCountry( $value )
	{
		$this->idCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "szCountry",  t($this->t_base.'fields/country'), false, false, true );
	}
	
	function set_szPostCodeStr( $value )
	{
		$this->szPostCodeStr = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPostCodeStr",  t($this->t_base.'fields/postcode'), false, false, true );
	}
        function set_szLocation( $value )
	{
            $this->szLocation = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLocation",  t($this->t_base.'fields/postcode'), false, false, true );
	}
	
	function set_iRadius( $value,$maxRadius )
	{
		$this->iRadius = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iDistance",  t($this->t_base.'fields/radius'), 1, $maxRadius, true );
	}
	function set_szCity( $value )
	{
		$this->szCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szName",  t($this->t_base.'fields/city'), false, false, true );
	}
	
	function set_szCountriesStr( $value )
	{
		$this->szCountriesStr = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCountriesStr",  t($this->t_base.'fields/country'), false, false, true );
	}
	
	function set_iDistanceKm( $value,$maxDistance )
	{
		$this->iDistanceKm = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iDistanceKm",  t($this->t_base.'fields/distance'), false, $maxDistance, true );
	}

	function set_szPostCode( $value )
	{
		$this->szPostCode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPostCode",  t($this->t_base.'fields/postcode'), false, false, true );
	}
	function set_fTotalWeight( $value)
	{
		$this->fTotalWeight = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fTotalWeight",  t($this->t_base.'fields/cargo_weight'), false, false, true );
	}
	function set_fVolume( $value)
	{
		$this->fVolume = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fVolume",  t($this->t_base.'fields/cargo_volume'), false, false, true );
	}
	function set_idWarehouse( $value)
	{
		$this->idWarehouse = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idWarehouse",  t($this->t_base.'fields/warehouse_id'), false, false, true );
	}
	function set_iDirection( $value)
	{
		$this->iDirection = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iDirection",  t($this->t_base.'fields/direction'), false, false, true );
	}
	
	function set_idHaulageModel( $value)
	{
		$this->idHaulageModel = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idHaulageModel",  t($this->t_base.'fields/invalid_haulage_pricing_id'), false, false, true );
	}
	function set_szLatitude( $value)
	{
		$this->szLatitude = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "szLatitude",  t($this->t_base.'fields/latitude'), false, false, true );
	}
	function set_szLongitude( $value)
	{
		$this->szLongitude = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "szLongitude",  t($this->t_base.'fields/longitude'), false, false, true );
	}	
	function set_iPricing( $value )
	{
		$this->iPricing = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iPricing", t($this->t_base.'fields/pricing'), false, 255, false );
	}
	function set_idForwarder( $value )
	{
		$this->idForwarder = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idForwarder", t($this->t_base.'fields/idForwarder'), false, false, false );
	}
}	

?>