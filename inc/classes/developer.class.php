<?php
/**
 * This file is the container for partner api related functionality. 
 *
 * api.class.php
 *
 * @copyright Copyright (C) 2015 Transporteca
 * @author Ajay
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" ); 

class cDeveloper extends cDatabase
{    
    function __construct()
    {
        parent::__construct();
        return true;
    }
    
    function addTask($data)
    {
        if(!empty($data))
        {
            $this->set_szTaskName(trim(sanitize_all_html_input($data['szTaskName'])));
            $this->set_szDeveloperName(trim(sanitize_all_html_input($data['szDeveloperName']))); 
            $this->set_szDeveloperRemark(trim(sanitize_all_html_input($data['szDeveloperRemark']))); 
            
            if($this->error==true)
            {
                return false;
            }
            $query="
                INSERT INTO
                    ".__DBC_SCHEMATA_DEVELOPER_TASKS__."
                (
                    szTaskName,  
                    szDeveloperName,
                    szDeveloperRemark,
                    dtStartedDate
                )
                VALUES
                (  
                    '".mysql_escape_custom($this->szTaskName)."', 
                    '".mysql_escape_custom($this->szDeveloperName)."', 
                    '".mysql_escape_custom($this->szDeveloperRemark)."',
                    now()
                )
            ";
            if($result = $this->exeSQL($query))
            {
                return true;
            }
            else
            {
                return false;
            } 
        }
    } 
    function getAllTaskLists()
    {
        $query="
            SELECT 
                id,
                szTaskName,  
                szDeveloperName,
                szDeveloperRemark,
                dtStartedDate
            FROM
                ".__DBC_SCHEMATA_DEVELOPER_TASKS__."  
            ORDER BY
                dtStartedDate ASC
        ";
        //echo $query;
        if($result = $this->exeSQL($query))
        {
            if($this->iNumRows>0)
            { 
                $ctr=0;
                $ret_ary = array();
                while($row=$this->getAssoc($result))
                {
                    $ret_ary[$ctr] = $row;
                    $ctr++;
                } 
                return $ret_ary;
            }
            else
            {
                return false;
            }
        } 
    }
    
    function updateCompletedTask($idTaskManager)
    {
        if($idTaskManager>0)
        {
            $query="	
                UPDATE
                    ".__DBC_SCHEMATA_BOOKING_FILE_EMAIL_LOGS__."
                SET
                    dtCompletedOn = now(),
                    iCompleted = '1'
                WHERE
                    id = '".(int)$idTaskManager."';
            "; 
            if($result=$this->exeSQL($query))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    
    function set_szTaskName($value,$flag=true)
    {
        $this->szTaskName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTaskName", t($this->t_base.'fields/reminder_true'), false, false, $flag );
    } 
    function set_szDeveloperName($value,$flag=true)
    {
        $this->szDeveloperName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDeveloperName", t($this->t_base.'fields/reminder_true'), false, false, $flag );
    }
    function set_szDeveloperRemark($value,$flag=false)
    {
        $this->szDeveloperRemark = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDeveloperRemark", t($this->t_base.'fields/reminder_true'), false, false, $flag );
    }
}

?>