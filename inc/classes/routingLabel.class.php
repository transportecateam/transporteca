<?php
/**
 * This file is the container for partner api related functionality. 
 *
 * transportecaApi.class.php
 *
 * @copyright Copyright (C) 2016 Transporteca
 * @author Ajay
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_CLASSES__ . "/responseErrors.class.php");

class cRoutingLabels extends cDatabase
{   
    function __construct()
    {
        parent::__construct();
        return true;
    }
    
    function createRoutingLabel($data)
    {
        if($data)
        {
            try
            {
                /*
                * Intializing credentials for Routing Label API.
                */
                $idBooking = $data['idBooking'];
                $szUsername = $postSearchAry['szUsername'];
                $szPassword = $postSearchAry['szPassword']; 
                $szCustomerXml = $this->getLabelRequestXml($data);     
                $szAccessApiEndPoint = __TNT_ROUTING_LABEL_API_END_POINT__;
                 
                $kLabel = new cLabels();  
                try
                {
                    $szLabelLogStr = "<br><p>Called Routing Label API</p><br>";
                    /*
                    * Creating a xml file with API request data. 
                    */
                    $kLabel->createXmlFile($szCustomerXml,$idBooking,true,"ROUTING_LABEL");  
                    
                    /*
                    * Sending Reques data to Routing Label API.
                    */
                    $AuthResponseXml = $this->httpPost($szAccessApiEndPoint, $szCustomerXml,$szUsername,$szPassword);   
                    
                    if(!empty($AuthResponseXml))
                    {
                        /*
                        * Successfully received response from Routing Label API
                        */
                        $szLabelLogStr = "<p>Got response from Routing label API</p><br>";
                        
                        /*
                        * Updating data content to a xml file.
                        */
                        $szXmlFilePath = $kLabel->createXmlFile($AuthResponseXml,$idBooking,false,"ROUTING_LABEL"); 
                        $szLabelLogStr = "<p>Stored Routing label API Response @ path : ".$szXmlFilePath."</p><br>";
                         
                        $szPdfFilePath = "";
                        $this->processRoutingLabelApiError($AuthResponseXml);
                        
                        if($this->iRoutingLabelApiError==1)
                        {
                            /*
                            * Received Error from Routing Label API.
                            */
                            $this->szLabelLogStr = $szLabelLogStr;
                            $this->szLabelLogStr .= $kLabel->szLabelLogStr; 
                            return false;
                        }
                        else
                        {
                            $szPdfFilePath = $kLabel->convertXml2Pdf($szXmlFilePath,$idBooking,false,'ROUTING_LABEL',$data);
                            
                            $this->szLabelLogStr = $szLabelLogStr;
                            $this->szLabelLogStr .= $kLabel->szLabelLogStr; 
                        
                            $this->szLabelPdfFileName = $szPdfFilePath;
                            $this->szLabelHtmlFileName = $this->szHtmlFilePath;
                            return $szPdfFilePath;
                        }  
                    }
                }
                catch (Exception $ex)
                {
                    $this->szLabelLogStr .= "<p>Got exception in obtaining access code </p><br>"; 
                    $this->szLabelLogStr .= "<p> Exception: ".$ex->getMessage()."</p>"; 
                }   
            }
            catch (Exception $ex)
            {
                print_R($ex);
            }
        } 
    }
    
    function getLabelRequestXml($postSearchAry)
    {
        if(!empty($postSearchAry))
        {
            $kLabel = new cLabels();
            if(strtolower($postSearchAry['iCollection'])=='scheduled')
            {
                $dtShipmentDate = format_date($postSearchAry['dtCollection']);  
                $dtCollectionStartTime = $kLabel->getFormatedLocalTime($postSearchAry['dtCollectionStartTime']); 
                
                $dtCollectionDateTime = trim($dtShipmentDate)."T".trim($dtCollectionStartTime).":00"; 
            }
            else
            { 
                $date_str = $postSearchAry['dtTimingDate'] ;  
                $curr_date_time = time();
                $date_str_time = strtotime($date_str); 
                if($curr_date_time > $date_str_time)
                {
                    $date_str = date('Y-m-d H:i:s');
                    $postSearchAry['dtTimingDate'] = $date_str;
                }   

                $dtFormatedDate = date('Y-m-d',strtotime($postSearchAry['dtTimingDate'])); 
                if(__isWeekend($dtFormatedDate))
                {
                    $this->szLabelApiError .= "<li> Searched day is weekend so we are picking next monday as colletion date.</li>";

                    $dtNextMondayDate = __nextMonday($dtFormatedDate); 
                    $dtShipmentDate = date("Y-m-d", strtotime($dtNextMondayDate));
                } 
                else
                {
                    $dtShipmentDate = date("Y-m-d",  strtotime($postSearchAry['dtTimingDate']));
                } 
                $dtCollectionDateTime = $dtShipmentDate."T09:00:00"; 
            }
   
            $szDeliveryInstruction = "";
            if(!empty($postSearchAry['szDeliveryInstructions']))
            {
                $szDeliveryInstruction = $postSearchAry['szDeliveryInstructions'];
            } 
            $szPackageDetailsXml = $this->buildRoutingPackageXml($postSearchAry);
            $iTotalNumColliForAPI = $this->iTotalNumColliForAPI;
             
            $fTotalCargoWeight = $postSearchAry['fCargoWeight'];
            $fTotalCargoVolume = $postSearchAry['fCargoVolume'];
            
            $kWhsSearch = new cWHSSearch();
            $iTNTApiReductionPercentage = $kWhsSearch->getManageMentVariableByDescription('__TNT_BOOKING_API_VOLUME_REDUCTION__'); 
            if($iTNTApiReductionPercentage>0)
            {
                $fTotalCargoVolume = $fTotalCargoVolume - ($fTotalCargoVolume * $iTNTApiReductionPercentage * 0.01);
            }
            
            $szCourierProductCode = $postSearchAry['szTNTApiCode']; 
            
            if($iTotalNumColliForAPI>0)
            {
                $iNumColli = $iTotalNumColliForAPI;
            }
            else
            {
                $iNumColli = $postSearchAry['iNumColli'];
            }
 
            if($postSearchAry['szShipperCountryCode']=="IE")
            {
                $postSearchAry['szShipperPostCode'] = "";
            }  

            if(empty($postSearchAry['szConsigneeCompanyName']))
            {
                $postSearchAry['szConsigneeCompanyName'] = $postSearchAry['szConsigneeFirstName']." ".$postSearchAry['szConsigneeLastName'];
            } 
            if(empty($postSearchAry['szShipperCompanyName']))
            {
                $postSearchAry['szShipperCompanyName'] = $postSearchAry['szShipperFirstName']." ".$postSearchAry['szShipperLastName'];
            }
            $szCargoDescription = $postSearchAry['szCargoDescription'];
  
            $szConsignmentNumber = $postSearchAry['szConsignmentNumber'];
            $szCustomerReference = $postSearchAry['szBookingRef'];
            
            /*
            * Checking if custom clearance is applicable on the Trade or not
            */
            if($postSearchAry['iIncludeCustomClearance']==1)
            {
                $szCustomControlled = "Y";
            }
            else
            {
                $szCustomControlled = "N";
            }
            $szTermsOfPayment = "";
            if($postSearchAry['szForwarderTNTAccountCountry']==$postSearchAry['szShipperCountryCode'])
            {
                /*
                * If forwarder country is same as shipper country then we use term Send will pay
                */
                $szTermsOfPayment = "S";
            }
            else if($postSearchAry['szForwarderTNTAccountCountry']==$postSearchAry['szConsigneeCountryCode'])
            {
                /*
                * If forwarder country is same as consignee country then we use term Send will pay
                */
                $szTermsOfPayment = "R";
            }
            
            $szSenderDetailsXml = $this->buildSenderAddressXml($postSearchAry,$szTermsOfPayment);
            $szDeliveryDetailsXml = $this->buildDeliveryAddressXml($postSearchAry,$szTermsOfPayment);
            $szTNTProductXml = $this->buildTNTProductXml($postSearchAry);
            
            $szRequestXml = '<?xml version="1.0" encoding="UTF-8"?>
                <labelRequest>  
                    <consignment KEY="CON1">
                        <consignmentIdentity>
                            <consignmentNumber><![CDATA['.$szConsignmentNumber.']]></consignmentNumber>
                            <customerReference><![CDATA['.$szCustomerReference.']]></customerReference>
                        </consignmentIdentity>
                        <collectionDateTime><![CDATA['.$dtCollectionDateTime.']]></collectionDateTime>
                           '.$szSenderDetailsXml.'
                           '.$szDeliveryDetailsXml.' 
                           '.$szTNTProductXml.'
                        <account>
                            <accountNumber><![CDATA['.$postSearchAry['szAccountNumber'].']]></accountNumber>
                            <accountCountry><![CDATA['.$postSearchAry['szForwarderTNTAccountCountry'].']]></accountCountry>
                        </account> 
                        <bulkShipment><![CDATA[N]]></bulkShipment> 
                        <specialInstructions><![CDATA['.$szDeliveryInstruction.']]></specialInstructions> 
                        <customControlled><![CDATA['.$szCustomControlled.']]></customControlled> 
                        <termsOfPayment><![CDATA['.$szTermsOfPayment.']]></termsOfPayment>
                        <totalNumberOfPieces><![CDATA['.$iNumColli.']]></totalNumberOfPieces> 
                        '.$szPackageDetailsXml.'
                    </consignment> 
                </labelRequest>  
            '; 
        }  
        return $szRequestXml;
    }
    
    function buildSenderAddressXml($postSearchAry,$szTermsOfPayment)
    {
        if(!empty($postSearchAry))
        {  
            $kLabels = new cLabels();
            $shipperAddressAry = array();
            $shipperAddressAry = $kLabels->formatStreetAddress($postSearchAry['szShipperAddress']);
            $szShipperAddress1 = $shipperAddressAry['szAddress1'];
            $szShipperAddress2 = $shipperAddressAry['szAddress2'];
            $szShipperAddress3 = $shipperAddressAry['szAddress3'];
            
            if(!empty($postSearchAry['szMessageToSender']))
            {
                $szCollectionInstruction = $postSearchAry['szMessageToSender'];
            }
            
            /*
            * IF $szTermsOfPayment=='S' i.e. if Shipment is export service then we pass Sender address as Forwarder Account details and collection address as Shipper's address details
            * ELSE we pass Shipper's details as both Sender and Collection
            */ 
            //$postSearchAry['szShipperCity'] = "bhghtgtgf";
            $szShipperPhone = substr($postSearchAry['szShipperPhone'], 0, 8);
            if($szTermsOfPayment=='S')
            {
                $szShipperDetailsXml = '<sender> 
                        <name><![CDATA['.$postSearchAry['szShipperCompanyName'].']]></name>
                        <addressLine1><![CDATA['.$szShipperAddress1.']]></addressLine1> 
                        <addressLine2><![CDATA['.$szShipperAddress2.']]></addressLine2>
                        <addressLine3><![CDATA['.$szShipperAddress3.']]></addressLine3>
                        <town><![CDATA['.$postSearchAry['szShipperCity'].']]></town> 
                        <exactMatch><![CDATA[Y]]></exactMatch>    
                        <postcode><![CDATA['.$postSearchAry['szShipperPostCode'].']]></postcode>
                        <country><![CDATA['.$postSearchAry['szShipperCountryCode'].']]></country>   
                    </sender>	
                ';
            }
            else
            {
                $szShipperDetailsXml = '<sender>
                        <name><![CDATA['.$postSearchAry['szShipperCompanyName'].']]></name>
                        <addressLine1><![CDATA['.$szShipperAddress1.']]></addressLine1> 
                        <addressLine2><![CDATA['.$szShipperAddress2.']]></addressLine2>
                        <addressLine3><![CDATA['.$szShipperAddress3.']]></addressLine3>
                        <town><![CDATA['.$postSearchAry['szShipperCity'].']]></town> 
                        <exactMatch><![CDATA[Y]]></exactMatch>
                        <postcode><![CDATA['.$postSearchAry['szShipperPostCode'].']]></postcode>
                        <country><![CDATA['.$postSearchAry['szShipperCountryCode'].']]></country>   
                    </sender>	
                ';
            }
            return $szShipperDetailsXml;
        }
    }
    
    function buildDeliveryAddressXml($postSearchAry,$szTermsOfPayment)
    { 
        if(!empty($postSearchAry))
        {
            $kLabels = new cLabels();
            $ConsigneeAddressAry = array();
            $ConsigneeAddressAry = $kLabels->formatStreetAddress($postSearchAry['szConsigneeAddress']);
            $szConsigneeAddress1 = $ConsigneeAddressAry['szAddress1'];
            $szConsigneeAddress2 = $ConsigneeAddressAry['szAddress2'];
            $szConsigneeAddress3 = $ConsigneeAddressAry['szAddress3'];
            
            $szConsigneePhone = substr($postSearchAry['szConsigneePhone'], 0, 8);
            
            /*
            * IF $szTermsOfPayment=='R' i.e. if Shipment is a Import service then we pass Receiver address as Forwarder Account details and Delivery address as Consignee's address details
            * ELSE we pass Consignee's details as both Sender and Collection
            */
            if($szTermsOfPayment=='R')
            {
                $szConsigneeAddressXml = ' 
                    <delivery>
                        <name><![CDATA['.$postSearchAry['szConsigneeCompanyName'].']]></name>
                        <addressLine1><![CDATA['.$szConsigneeAddress1.']]></addressLine1>
                        <addressLine2><![CDATA['.$szConsigneeAddress2.']]></addressLine2>
                        <addressLine3><![CDATA['.$szConsigneeAddress3.']]></addressLine3>
                        <town><![CDATA['.$postSearchAry['szConsigneeCity'].']]></town>
                        <exactMatch><![CDATA[Y]]></exactMatch>
                        <province/> 
                        <postcode><![CDATA['.$postSearchAry['szConsigneePostCode'].']]></postcode>
                        <country><![CDATA['.$postSearchAry['szConsigneeCountryCode'].']]></country>  
                    </delivery>
                ';
            }
            else
            {
                $szConsigneeAddressXml = ' 
                    <delivery>
                        <name><![CDATA['.$postSearchAry['szConsigneeCompanyName'].']]></name>
                        <addressLine1><![CDATA['.$szConsigneeAddress1.']]></addressLine1>
                        <addressLine2><![CDATA['.$szConsigneeAddress2.']]></addressLine2>
                        <addressLine3><![CDATA['.$szConsigneeAddress3.']]></addressLine3>
                        <town><![CDATA['.$postSearchAry['szConsigneeCity'].']]></town>
                        <exactMatch><![CDATA[Y]]></exactMatch>
                        <province/> 
                        <postcode><![CDATA['.$postSearchAry['szConsigneePostCode'].']]></postcode>
                        <country><![CDATA['.$postSearchAry['szConsigneeCountryCode'].']]></country>  
                    </delivery>
                ';
            } 
            return $szConsigneeAddressXml;
        }
    }
    
    
    function buildRoutingPackageXml($postSearchAry)
    { 
        if(!empty($postSearchAry))
        {
            $kLabels = new cLabels();
            $cargoDetailsAry = array();
            $cargoDetailsAry = $kLabels->getCargoDimensions($postSearchAry); 
            $this->iTotalNumColliForAPI = $kLabels->iTotalNumColliForAPI;
            
            $szPackageDetailsXml = "";
            
            $iCounter = 1;
            if(!empty($cargoDetailsAry))
            {
                foreach($cargoDetailsAry as $cargoDetailsArys)
                { 
                    /*
                    * Converting cargo dimen from CM to Meter
                    */
                    $fLength = round((float)($cargoDetailsArys['length']/100),2);
                    $fHeight = round((float)($cargoDetailsArys['height']/100),2);
                    $fWidth = round((float)($cargoDetailsArys['width']/100),2);
                    $fWeight = round((float)($cargoDetailsArys['weight']),2);
                    
                    /*
                     * As a standard the following are limited to:
                        Max weight per package: 70kg
                        Max length per package: 2.4m
                        Max height per package: 1.5m
                        Max width per package: 1.2m
                     */
                    if($fLength<=0 || $fWidth<=0 || $fHeight<=0 || $fWeight<=0)
                    { 
                        $this->szLabelApiError .= "<li>One or more of the dimensions is registered as 0. Please update the carton size, or make labels manually.</li>";
                        $bErrorFlag = true;
                    }
                    else
                    {
                        if($fLength>2.4)
                        {
                            $this->szLabelApiError .= "<li>Length must be no more than 2.4 meter.</li>";
                            $bErrorFlag = true;
                        } 
                        if($fHeight>1.5)
                        {
                            $this->szLabelApiError .= "<li>Height must be no more than 1.5 meter.</li>";
                            $bErrorFlag = true;
                        }
                        if($fWidth>1.2)
                        {
                            $this->szLabelApiError .= "<li>Width must be no more than 1.2 meter.</li>";
                            $bErrorFlag = true;
                        }
                        if($fWeight>70)
                        {
                            $this->szLabelApiError .= "<li>Weight must be no more than 70kg.</li>";
                            $bErrorFlag = true;
                        }
                    }
                    if(strlen($postSearchAry['description'])>=60)
                    {
                        $this->szLabelApiError .= "<li>Cargo line description must be no more than 60 characters.</li>";
                        $bErrorFlag = true;
                    }  
                    if($bErrorFlag)
                    {
                        $this->iCargoDimensionError = 1;
                        break;
                    } 
                    $szPackageDetailsXml .= '
                        <pieceLine>
                            <identifier><![CDATA['.$iCounter.']]></identifier>
                            <goodsDescription><![CDATA['.$cargoDetailsArys['description'].']]></goodsDescription> 
                            <pieceMeasurements>
                                <length><![CDATA['.$fLength.']]></length>
                                <height><![CDATA['.$fHeight.']]></height>
                                <width><![CDATA['.$fWidth.']]></width>
                                <weight><![CDATA['.$fWeight.']]></weight> 
                            </pieceMeasurements> 
                            <pieces> 
                                <sequenceNumbers><![CDATA['.$iCounter.']]></sequenceNumbers>
                                <pieceReference><![CDATA['.$cargoDetailsArys['szPieceDescription'].']]></pieceReference>
                            </pieces>
                        </pieceLine>
                    '; 
                    $iCounter++;
                }
            }
            return $szPackageDetailsXml;
        }
    }
    
    function buildTNTProductXml($postSearchAry)
    {
        if(!empty($postSearchAry))
        {
            $szTNTProductCode = $postSearchAry['szTNTApiCode']; 
            $routingProductDescAry = array();
            $routingProductDescAry = $this->getRoutingLabelProductDetails($szTNTProductCode);
            
            if(!empty($routingProductDescAry))
            {
                $szTNTProductXml = '
                    <product>
                        <lineOfBusiness><![CDATA['.(int)$routingProductDescAry['iLineOfBusiness'].']]></lineOfBusiness>
                        <groupId><![CDATA['.(int)$routingProductDescAry['iGroupID'].']]></groupId>
                        <subGroupId><![CDATA['.(int)$routingProductDescAry['iSubGroupID'].']]></subGroupId>
                        <id><![CDATA['.$routingProductDescAry['szProductID'].']]></id>
                        <type><![CDATA['.$routingProductDescAry['szType'].']]></type> 
                    </product>
                ';
                return $szTNTProductXml;
            }
            else
            {
                $this->iProductNotExistsError = 1;
                $this->szLabelErrorStr = "<p>We must have a row in table 'tbltntserviceconversions' for corresponding Product code ".$szTNTProductCode."</p><br>";
                return false;
            }
        }
    }
    
    function getRoutingLabelProductDetails($szApiCode)
    {
        if(!empty($szApiCode))
        { 
            $query="
                SELECT
                    id,
                    szProductCode,
                    szDivision,
                    szProductDescription,
                    szProductID,
                    iLineOfBusiness,
                    iGroupID,
                    iSubGroupID,
                    szType,
                    szRemarks,
                    dtCreatedOn
                FROM
                    ".__DBC_SCHEMATA_TNT_SERVICE_CONVERTIONS__."
                WHERE
                    szProductCode = '".mysql_escape_custom($szApiCode)."'
                AND
                    iActive = '1'
            ";
            //echo $query ;
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $row = $this->getAssoc($result);
                    return $row;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
      
    function httpPost($url, $strRequest,$szUsername,$szPassword)
    {
        $userPass = $szUsername.":".$szPassword;
        $headers = array(
            'Content-Type:application/xml;',
            'Authorization: Basic '. base64_encode($userPass)
        );
        
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_HEADER, $headers); 
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  
        curl_setopt($ch, CURLOPT_POST, 1);  
        curl_setopt($ch, CURLOPT_POSTFIELDS, $strRequest); 
        
        $result = curl_exec($ch);
        $curlInfoAry = curl_getinfo($ch); 
        
        $httpCode = $curlInfoAry['http_code'];
        if ( $httpCode == 200 )
        { 
            $iPos = strpos($result,"<?xml");
            $szXMLString = substr($result,$iPos);
            
            return $szXMLString;
        } 
        else 
        {
            $szExceptionMessage = "Return code is {$httpCode} \n" .curl_error($ch);
            throw new Exception($szExceptionMessage);
        }
    } 
      
    function processRoutingLabelApiError($szRoutingLabelResponseXml)
    {
        if(!empty($szRoutingLabelResponseXml))
        {
            $kWebServices = new cWebServices();
            if($kWebServices->_isValidXML($szRoutingLabelResponseXml))
            {
                $labelResponseAry = array();  
                $labelResponseAry = cXmlParser::createArray($szRoutingLabelResponseXml);
                
                if(!empty($labelResponseAry['labelResponse']['brokenRules']))
                {
                    $this->iRoutingLabelApiError = 1;
                    $szRoutingLabelErrorMessage = "";
                    $brokenRulesAry = array();
                    $brokenRulesAry = $labelResponseAry['labelResponse']['brokenRules']; 
                    
                    if(!empty($brokenRulesAry[0]))
                    { 
                        foreach($brokenRulesAry as $brokenRulesArys)
                        {
                            $szRoutingLabelErrorMessage .= "<li>".$brokenRulesArys['errorDescription']."</li>";
                        }
                    }
                    else if(!empty($brokenRulesAry))
                    {
                        $szRoutingLabelErrorMessage .= "<li>".$brokenRulesAry['errorDescription']."</li>";
                    }
                    $this->szRoutingLabelApiErrorMessage = "<ul>".$szRoutingLabelErrorMessage."</ul>";
                }
            }
            else
            {
                $this->iRoutingLabelApiError = 1;
                $this->szRoutingLabelApiErrorMessage = "";
            }
        }
    }
}