<?php
/**
 * This file is the container for all billing related functionality.
 * All functionality related to billing details should be contained in this class.
 *
 * billing.class.php
 *
 * @copyright Copyright (C) 2012 Transport-ece
 * @author Ajay
 */
if(!isset($_SESSION))
{
    session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

class cBilling extends cDatabase
{
	var $forwarderAry=array();
	var $szOriginCityAry=array();
	var $szDestinationCity=array();
	var $maxBookingDate;
	var $minBookingDate;
	var $maxCutOffDate;
	var $minCutOffDate;
	var $maxAvialDate;
	var $minAvialDate;
	var $t_base='Booking/MyBooking/';
	var $idForwarder;
	var $iTimely;
	var $idBooking;
	var $szSuggestion;
	var $szReview;
	var $iCourtous;
	var $iRating;
	var $id;
	var $dtCompleted;
	var $szInvoiceFwdRef;
	var $idUser;

	function __construct()
	{
            parent::__construct();
            return true;
	}
	
	
	function getCurrenctBalancePerForwarder($idForwarder)
	{
		$result_arr=array();
		$result_arr_uploadservice=array();
		$query="
			SELECT 
                            round(SUM(fTotalPriceForwarderCurrency),2) AS iCreditBalance,
                            szForwarderCurrency		
			FROM 
                            ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS ft		
			WHERE	
                            iDebitCredit IN ('1','13')
			AND
                            iStatus IN ('1','2')		
			AND 
                            idForwarder='".(int)$idForwarder."'
			AND
                            ft.iTransferConfirmed = '0'
                        AND
                            dtPaymentConfirmed != '0000-00-00 00:00:00'
                        AND 
                            iForwarderGPType != '".__FORWARDER_PROFIT_TYPE_MARK_UP__."'
			GROUP BY
                            szForwarderCurrency	
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
                    while($row=$this->getAssoc($result))
                    {
                        $result_arr[$row['szForwarderCurrency']]['booking']=round($row['iCreditBalance'],2);
                    }
		} 
		$query="
                    SELECT 
                        round(SUM(fTotalPriceForwarderCurrency),2) AS iCreditBalance,
                        szForwarderCurrency		
                    FROM 
                        ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS ft		
                    WHERE	
                        iDebitCredit IN ('3','5','6')
                    AND
                        iStatus IN ('1','2')		
                    AND 
                        idForwarder='".(int)$idForwarder."'
                    AND
                        iTransferConfirmed = '0'
                    AND
                        dtPaymentConfirmed != '0000-00-00 00:00:00'
                    AND 
                        iForwarderGPType != '".__FORWARDER_PROFIT_TYPE_MARK_UP__."'
                    GROUP BY
                            szForwarderCurrency	
		";
		//echo "<br><br>".$query;
		if($result=$this->exeSQL($query))
		{
                    while($row=$this->getAssoc($result))
                    {
                        $result_arr[$row['szForwarderCurrency']]['paidAmount']=round($row['iCreditBalance'],2);
                    }
		}
		
		
		$query="
                    SELECT 
                        round(SUM(fTotalPriceForwarderCurrency),2) AS iCreditBalance,
                        szForwarderCurrency		
                    FROM 
                            ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS ft		
                    WHERE	
                        iDebitCredit IN ('2','12')
                    AND
                        iStatus IN ('2')		
                    AND 
                        idForwarder='".(int)$idForwarder."'
                    AND
                        ft.iTransferConfirmed = '0'
                    AND
                        dtPaymentConfirmed != '0000-00-00 00:00:00'
                    AND 
                        iForwarderGPType != '".__FORWARDER_PROFIT_TYPE_MARK_UP__."'
                    GROUP BY
                        szForwarderCurrency	
		";
		//echo "<br><br>".$query;
		if($result=$this->exeSQL($query))
		{
                    while($row=$this->getAssoc($result))
                    {
                        $result_arr[$row['szForwarderCurrency']]['autoTransfer']=round($row['iCreditBalance'],2);
                    }
		}   
		if(!empty($result_arr))
		{
                    foreach($result_arr as $key=>$values)
                    {
                        $debitAmount=$values['paidAmount']+$values['autoTransfer'];
                        $totalAmountArr[$key]=round($values['booking'],2)-round($debitAmount,2);
                    }
		} 
		return $totalAmountArr;
		
	}
        
	function getAllForwarderForAutomaticPayment()
	{  
            $query="
                SELECT
                    id,
                    szDisplayName, 
                    idCountry,
                    iActive,
                    isOnline,
                    iAgreeTNC,
                    szForwarderAlias,
                    szCurrency
                FROM
                    ".__DBC_SCHEMATA_FORWARDERS__."		
                WHERE
                    iActive = '1'    
                AND
                    iProfitType = '".__FORWARDER_PROFIT_TYPE_REFERRAL_FEE__."'
                AND
                    iAutomaticPaymentInvoicing = '1'
            "; 
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $ret_ary = array();

                    while($row=$this->getAssoc($result))
                    {
                        $ret_ary[] = $row;
                    }
                    return $ret_ary;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                return array();
            }
	}
	
	function getPendingBalancePerForwarder($idForwarder)
	{
		$result_arr=array(); 
		$query="
			SELECT 
                            round(SUM(b.fTotalPriceForwarderCurrency),2) AS iCreditBalance,
                            b.szForwarderCurrency		
			FROM 
                            ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS ft
                        INNER JOIN
                            ".__DBC_SCHEMATA_BOOKING__." b
                        ON        
                            b.id=ft.idBooking            
			WHERE	
                            ft.iDebitCredit='1'
			AND
                            iStatus='0'		
			AND 
                            ft.idForwarder='".(int)$idForwarder."'
			AND
                            ft.iTransferConfirmed = '0'
                        AND 
                            ft.iForwarderGPType != '".__FORWARDER_PROFIT_TYPE_MARK_UP__."'
			GROUP BY
			 	b.szForwarderCurrency	
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			while($row=$this->getAssoc($result))
			{
				$result_arr[$row['szForwarderCurrency']]=$row['iCreditBalance'];
			}
			
			return $result_arr;
		}
		else
		{
			return $result_arr;
		}
	}
	
	function getInvoiceBillPending($idForwarder,$dateArr)
	{
		$result_arr=array();
		$query = "
			SELECT 
                            `id`, 
			    `idBooking`,
			    `idForwarder`,
			    `fTotalAmount`, 
			    `fTotalPriceForwarderCurrency`,
			    `idCurrency`,
			    `szCurrency`,
			    `fExchangeRate`,
                            `idForwarderCurrency`, 
			    `szForwarderCurrency`, 
			    `fForwarderExchangeRate`, 
			    `iDebitCredit`, 
			    `szBookingNotes`, 
			    `szInvoice`, 
			    `szBooking`,
			    `iStatus`, 
			    `dtCreatedOn`, 
			    `dtUpdatedOn`, 
			    `dtCreditOn`, 
			    `dtDebitOn`, 
			    `dtInvoiceOn`,
			    `iBatchNo`,
			    fReferalPercentage,
			    fReferalAmount,
			    dtPaymentConfirmed
		 	FROM	
                            ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                        WHERE
                            idForwarder='".(int)$idForwarder."'
		 	AND
                            iDebitCredit = '1'
		 	AND
                            iTransferConfirmed = '0'
		 	AND
                            iStatus='0'
                        AND
                            iForwarderGPType != '".__FORWARDER_PROFIT_TYPE_MARK_UP__."'
		";
		/*if(!empty($dateArr))
		{
			$query.= " AND dtCreatedOn BETWEEN '". mysql_escape_custom($dateArr[0]) . "' AND '".mysql_escape_custom($dateArr[1])."'";
		}*/
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			while($row=$this->getAssoc($result))
			{
				$result_arr[]=$row;
			}
			
			return $result_arr;
		}
		else
		{
			return $result_arr;
		}
	}
	
	function getInvoiceBillConfirmed($id,$date=array())
	{ 
            $query = "
                SELECT 
                    `id`, 
                    `idBooking`,
                    `idForwarder`,
                    `fTotalAmount`, 
                    `fTotalPriceForwarderCurrency`,
                    `idCurrency`,
                    `szCurrency`,
                    fExchangeRate,
                   `idForwarderCurrency`, 
                   `szForwarderCurrency`, 
                   `fForwarderExchangeRate`, 
                   `iDebitCredit`, 
                   `szBookingNotes`, 
                   `szInvoice`, 
                   `szBooking`,
                   `iStatus`, 
                   `dtCreatedOn`, 
                   `dtUpdatedOn`, 
                   `dtCreditOn`, 
                   `dtDebitOn`, 
                   `dtInvoiceOn`,
                   `iBatchNo`,
                   fReferalPercentage,
                   fReferalAmount,
                   dtPaymentConfirmed,
                   fTotalSelfInvoiceAmount,
                   iFinancialVersion,
                   szSelfInvoice,
                   dtPaymentScheduled
                FROM	
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
	    	WHERE
                    idForwarder='".(int)$id."' 
                AND 
                    iForwarderGPType != '".__FORWARDER_PROFIT_TYPE_MARK_UP__."'
                AND
                    iTransferConfirmed = '0'  
                ";
                if(!empty($date))
                {  
                    if($date[2]=='DATE_TIME')
                    {
                        $dtPaymentConfirmed = " dtPaymentConfirmed";
                        $dtInvoiceOn = " dtInvoiceOn ";
                        $dtPaymentScheduled = " dtInvoiceOn ";
                    }
                    else
                    {
                        $dtPaymentConfirmed = " DATE(dtPaymentConfirmed) ";	
                        $dtInvoiceOn = " DATE(dtInvoiceOn) ";
                        $dtPaymentScheduled = " DATE(dtInvoiceOn) ";
                    }
                    /*
                     * In following query we have 3 cases 
                     * Case - 1: This block will fetch all the 'Automatic Transfer' records which not have been confirmed yet. I mean all the record which on Management /forwarderTransfer/ screen
                     * Case - 2: This section will fetch all the record which has been paid. In system we consider those record as paid where iStatus = 2 and dtPaymentConfirmed <> NULL
                     * Case - 3: This section will fetch all the bookings where payment has been been received to Transporteca. I mean all the booking which is on Management > /outstandingPayment/ screen. 
                     */
                    $query .= "	 
                        AND 
                        (
                            (
                                    iDebitCredit = 2
                                AND
                                    iStatus = '1'
                                AND
                                    iBatchNo<>'0'
                                AND
                                    ".$dtPaymentScheduled." >= '". mysql_escape_custom($date[0]) . "'  
                            )
                            OR
                            (
                                    iStatus > 0
                                AND
                                    ".$dtPaymentConfirmed."
                                BETWEEN 
                                    '". mysql_escape_custom($date[0]) . "' 
                                AND 
                                    '".mysql_escape_custom($date[1])."' 
                            )
                            OR
                            (
                                    iStatus = 0
                                AND
                                    ".$dtInvoiceOn."
                                BETWEEN 
                                    '". mysql_escape_custom($date[0]) . "' 
                                AND 
                                    '".mysql_escape_custom($date[1])."' 
                            )
                        )
                    ";
                }
                $query .= " ORDER BY
                    dtPaymentConfirmed ASC 
            ";
            //echo "<br><br>".$query;
            if($result=$this->exeSQL($query))
            {
                $bookingAry = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    $bookingAry[$ctr]=$row;
                    if($row['iStatus']==0)
                    {
                        $bookingAry[$ctr]['dtPaymentConfirmed'] = $row['dtInvoiceOn'];
                    }
                    else if($row['iDebitCredit']==2 && $row['iStatus']==1)
                    {
                        /*
                         * If Transfer is scheduled but transfer not confirmed bookings I mean booking which is on http://devmanagement.transporteca.com/forwarderTransfer/ 
                         */
                        $bookingAry[$ctr]['dtPaymentConfirmed'] = $row['dtInvoiceOn'];
                    }
                    $ctr++;
                }
                if(!empty($bookingAry))
                { 
                    $bookingAry = sortArray($bookingAry,'dtPaymentConfirmed',false,'fTotalPriceForwarderCurrency',false,'id',false,true);   
                }
                return $bookingAry;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	} 
        
	function getNetBalancePerForwarder($idForwarder,$dataArr)
	{   
            $result_arr=array();
            $result_arr_uploadservice=array();

            $query="
                SELECT 
                    round(SUM(fTotalPriceForwarderCurrency),2) AS iCreditBalance,
                    szForwarderCurrency		
                FROM 
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS ft		
                WHERE	
                    iDebitCredit IN ('1','13') 
                AND 
                    idForwarder='".(int)$idForwarder."'
                AND
                    iTransferConfirmed = '0' 
                AND 
                (
                    (
                            iStatus > 0
                        AND
                           DATE(dtPaymentConfirmed)< '".  mysql_escape_custom($dataArr['dtFromDate'])."'  
                    )
                    OR
                    (
                            iStatus = 0
                        AND 
                           DATE(dtInvoiceOn)< '".  mysql_escape_custom($dataArr['dtFromDate'])."'  
                    )
                )
                AND 
                    iForwarderGPType != '".__FORWARDER_PROFIT_TYPE_MARK_UP__."'
                GROUP BY
                    szForwarderCurrency	
            ";
            //echo $query."<br/><br/>";
            if($result=$this->exeSQL($query))
            {
                while($row=$this->getAssoc($result))
                {
                    $result_arr[$row['szForwarderCurrency']]['booking']=$row['iCreditBalance'];
                }
            } 
		
            $query="
                SELECT 
                    round(SUM(fTotalPriceForwarderCurrency),2) AS iCreditBalance,
                    szForwarderCurrency		
                FROM 
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS ft		
                WHERE	
                    iDebitCredit IN ('2','3','12')
                AND
                    iStatus IN ('1','2')		
                AND 
                    idForwarder='".(int)$idForwarder."' 
                AND 
                (
                    ( 
                            iDebitCredit = '2'
                        AND
                            iStatus = '1'
                        AND
                            iBatchNo<>'0'
                        AND
                            dtInvoiceOn < '". mysql_escape_custom($dataArr['dtFromDate']) . "' 
                        AND
                            dtInvoiceOn != '0000-00-00 00:00:00'
                    )
                    OR
                    (
                            iStatus = '2' 
                        AND
                            DATE(dtPaymentConfirmed)< '".  mysql_escape_custom($dataArr['dtFromDate'])."'  
                        AND
                            dtPaymentConfirmed != '0000-00-00 00:00:00'
                    )
                ) 
                AND
                    iTransferConfirmed = '0'
                AND 
                    iForwarderGPType != '".__FORWARDER_PROFIT_TYPE_MARK_UP__."'
                GROUP BY
                        szForwarderCurrency	
            ";
            //echo $query."<br/>";
            if($result=$this->exeSQL($query))
            {
                while($row=$this->getAssoc($result))
                {
                    $result_arr[$row['szForwarderCurrency']]['paidAmount']=$row['iCreditBalance'];
                }
            }


            $query="
                SELECT 
                    round(SUM(fTotalPriceForwarderCurrency),2) AS iCreditBalance,
                    szForwarderCurrency		
                FROM 
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS ft		
                WHERE	
                    iDebitCredit IN ('6','7','5') 	
                AND 
                    idForwarder='".(int)$idForwarder."'
                AND 
                (
                    ( 
                            iStatus = '1' 
                        AND
                            dtInvoiceOn < '". mysql_escape_custom($dataArr['dtFromDate']) . "' 
                        AND
                            dtInvoiceOn != '0000-00-00 00:00:00'
                    )
                    OR
                    (
                            iStatus = '2' 
                        AND
                            DATE(dtPaymentConfirmed) < '".  mysql_escape_custom($dataArr['dtFromDate'])."'  
                        AND
                            dtPaymentConfirmed != '0000-00-00 00:00:00'
                    )
                )
                AND
                    iTransferConfirmed = '0'
                AND 
                    iForwarderGPType != '".__FORWARDER_PROFIT_TYPE_MARK_UP__."'
                GROUP BY
                    szForwarderCurrency	
            ";
            //echo $query."<br/>";

            if($result=$this->exeSQL($query))
            {
                while($row=$this->getAssoc($result))
                {
                    $result_arr[$row['szForwarderCurrency']]['autoTransfer']=$row['iCreditBalance'];
                }
            }    
            if(!empty($result_arr))
            {
                foreach($result_arr as $key=>$values)
                {
                    $totalAmountArr[$key]['fTotalBalance'] = round((float)($values['booking']-($values['paidAmount']+$values['autoTransfer'])),4);
                }
            }     
            return $totalAmountArr; 
	}
	
	function getInvoiceRoeValueCancelBooking($idBooking)
	{
            $query="
                SELECT
                    b.szCurrency as szCurrencyName,
                    b.fTotalAmount as fTAmount1,
                    b.fExchangeRate as fERate
                FROM
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."  AS b
                WHERE
                    idBooking='".(int)$idBooking."'
                AND
                    iDebitCredit='1'
                AND
                    iStatus IN ('1','2')
            ";
            //echo $query;
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $row=$this->getAssoc($result);
                    $res_arr['invoiceValue']=$row['szCurrencyName']." ".number_format($row['fTAmount1'],2);
                    $res_arr['roe']=$row['fERate']; 
                    return $res_arr;
                }
            }
	}
        
    function payForwarderInvoice($data)
    {
        if(!empty($data))
        { 
            $data['dtMarkupPaid'] = format_date_time($data['dtMarkupPaid']);

            $this->set_idForwarderQuoteCurrency(sanitize_all_html_input($data['idForwarderQuoteCurrency']));
            $this->set_fForwarderTotalQuotePrice(sanitize_all_html_input($data['fForwarderTotalQuotePrice']));
            $this->set_fTotalVatForwarderCurrency(sanitize_all_html_input($data['fTotalVatForwarderCurrency']));
            $this->set_dtMarkupPaid(sanitize_all_html_input($data['dtMarkupPaid'])); 
            $this->szMarkupPaymentComments = $data['szMarkupPaymentComments'];  
            
            if($this->error==true)
            {
                return false;
            }

            $idBooking = $data['idBooking']; 
            $kBooking = new cBooking();
            $kWHSSearch = new cWHSSearch();
            
            $postSearchAry = $kBooking->getBookingDetails($idBooking);
            
            $szBookingRef = $postSearchAry['szBookingRef'];
            $idBookingFile = $postSearchAry['idFile'];
            $fTotalPriceUSD = $postSearchAry['fTotalPriceUSD'];
            $fTotalVatCustomerCurrency = $postSearchAry['fTotalVat']; 
            $fCustomerCurrencyExchangeRate = $postSearchAry['fExchangeRate'];
            $idAdminAddedBy = $_SESSION['admin_id'];
            $idQuotePricingDetails = $postSearchAry['idQuotePricingDetails'];
            //echo "PricingID: ".$idQuotePricingDetails ;
            /**
             *  Calculating Budget Gross profit
             */
            $quotePricingAry = array();
            if($idQuotePricingDetails>0)
            { 
                $kBooking_new = new cBooking(); 
                $priceSearchAry = array();
                $priceSearchAry['idBookingQuotePricing'] = $idQuotePricingDetails;
                $quotePricingAry = $kBooking_new->getAllBookingQuotesPricingDetails($priceSearchAry);
                if(!empty($quotePricingAry[1]))
                {
                    $fTotalPriceForwarderCurrency = $quotePricingAry[1]['fTotalPriceForwarderCurrency'];
                    $fForwarderCurrencyExchangeRate = $quotePricingAry[1]['fForwarderCurrencyExchangeRate'];

                    $fActualQuoteByForwarderUSD = ($fTotalPriceForwarderCurrency * $fForwarderCurrencyExchangeRate);
                    $fTotalBudgetReferalAmount = $fTotalPriceUSD - $fActualQuoteByForwarderUSD ;
                    $fTotalBudgetReferalAmount = number_format((float)$fTotalBudgetReferalAmount);
                    
                    if($fActualQuoteByForwarderUSD>0)
                    {
                        $fTotalBudgetReferalPerentage = ($fTotalBudgetReferalAmount * 100 ) / $fActualQuoteByForwarderUSD ;
                        $fTotalBudgetReferalPerentage = round((float)$fTotalBudgetReferalPerentage);
                    }
                } 
            }
            
            /*
            * Calculating Actual Gross profit
            */
            if($this->idForwarderQuoteCurrency==1)
            {
                $szForwarderQuoteCurrency = 'USD';
                $fForwarderQuoteCurrencyExchangeRate = '1';
            }
            else
            {
                $currencyAry = array();
                $currencyAry = $kWHSSearch->getCurrencyDetails($this->idForwarderQuoteCurrency);
                
                $szForwarderQuoteCurrency = $currencyAry['szCurrency'];
                $fForwarderQuoteCurrencyExchangeRate = $currencyAry['fUsdValue'];
            }  
            //echo "FWD: ".$this->fForwarderTotalQuotePrice." Exchange: ".$fForwarderQuoteCurrencyExchangeRate ;
            $fForwarderTotalQuotePriceUSD = $this->fForwarderTotalQuotePrice * $fForwarderQuoteCurrencyExchangeRate;
            $fTotalVatForwarderCurrencyUSD = $this->fTotalVatForwarderCurrency * $fForwarderQuoteCurrencyExchangeRate; 
            $fReferralAmountUSD = $fTotalPriceUSD - $fForwarderTotalQuotePriceUSD ;
            
            $fReferalPercentage = 0;
            if($fForwarderTotalQuotePriceUSD>0)
            {
                $fReferalPercentage = round((float)(($fReferralAmountUSD * 100 ) / $fForwarderTotalQuotePriceUSD),4) ;
            }
            if($fForwarderQuoteCurrencyExchangeRate>0)
            {
                $fReferralAmount = round((float)($fReferralAmountUSD/$fForwarderQuoteCurrencyExchangeRate),4) ;
            }
            /*
             * Budget GP(%) - Actual GP (%)
             */
            $iPecentageDifference = abs($fTotalBudgetReferalPerentage - $fReferalPercentage);
            //echo "FWD FEE: ".$fForwarderTotalQuotePriceUSD." VAT: ".$fTotalVatForwarderCurrencyUSD." USD: ".$fTotalPriceUSD ; 
            
           // echo "<br> Budgt: ".$fTotalBudgetReferalPerentage." actl: ".$fReferalPercentage."Diffe ".$iPecentageDifference;
            //die;
            $validateData = true;
            if($data['iConfirmError']==1)
            {
                $validateData = false;
            }
            
            if($validateData)
            {
                if($fReferralAmountUSD<0)
                {
                    $this->iConfirmError = 1; 
                    $this->szConfirmationSpecialError = 'The profit is negative, click again to confirm';
                    return false;
                }
                else if($iPecentageDifference>10)
                {
                    $this->iConfirmError = 1;
                    $this->szConfirmationSpecialError = 'The actual profit is more than 10% different from budget, click again to confirm';
                    return false;
                } 
            }
            $oldValuesAry = array();
            $oldValuesAry['idForwarderQuoteCurrency'] = $postSearchAry['idForwarderQuoteCurrency'];
            $oldValuesAry['szForwarderQuoteCurrency'] = $postSearchAry['szForwarderQuoteCurrency'];
            $oldValuesAry['fForwarderQuoteCurrencyExchangeRate'] = $postSearchAry['fForwarderQuoteCurrencyExchangeRate'];
            $oldValuesAry['fForwarderTotalQuotePrice'] = $postSearchAry['fForwarderTotalQuotePrice'];
            $oldValuesAry['iForwarderGPType'] = $postSearchAry['iForwarderGPType'];
            $oldValuesAry['fTotalVatForwarderCurrency'] = $postSearchAry['fTotalVatForwarderCurrency'];
            $oldValuesAry['iMarkupPaid'] = $postSearchAry['iMarkupPaid'];
            $oldValuesAry['dtMarkupPaid'] = $postSearchAry['dtMarkupPaid'];
            $oldValuesAry['fReferalPercentage'] = $postSearchAry['fReferalPercentage'];
            $oldValuesAry['fReferalAmount'] = $postSearchAry['fReferalAmount'];
            $oldValuesAry['fReferalAmountUSD'] = $postSearchAry['fReferalAmountUSD'];
                
            //$fTotalInvoiceValueUSD = $fForwarderTotalQuotePriceUSD + $fTotalVatForwarderCurrencyUSD ;
            $fTotalInvoiceValueUSD = $fForwarderTotalQuotePriceUSD ;
                    
            $res_ary = array(); 
            $res_ary['idForwarderQuoteCurrency'] = $this->idForwarderQuoteCurrency;
            $res_ary['szForwarderQuoteCurrency'] = $szForwarderQuoteCurrency;
            $res_ary['fForwarderQuoteCurrencyExchangeRate'] = $fForwarderQuoteCurrencyExchangeRate;
            $res_ary['fForwarderTotalQuotePrice'] = $this->fForwarderTotalQuotePrice;
            $res_ary['fTotalVatForwarderCurrency'] = $this->fTotalVatForwarderCurrency;
            $res_ary['iMarkupPaid'] = 1;
            $res_ary['dtMarkupPaid'] = $this->dtMarkupPaid; 
            $res_ary['fReferalPercentage'] = $fReferalPercentage;
            $res_ary['fReferalAmount'] = $fReferralAmount;
            $res_ary['fReferalAmountUSD'] = $fReferralAmountUSD;
            $res_ary['szMarkupPaymentComments'] = $this->szMarkupPaymentComments;
              
            if(!empty($res_ary))
            {
                $update_query = '';
                foreach($res_ary as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                } 
                $update_query = rtrim($update_query,","); 
                $kBooking->updateDraftBooking($update_query,$idBooking); 
            }
             
            //we always add value in USD to display on Management=>Dashboard=> Actual Revenue Graph
            $referaalAmountArr = array();
            $referaalAmountArr['idBooking'] = $idBooking;
            $referaalAmountArr['idForwarder'] = $postSearchAry['idForwarder']; 
            $referaalAmountArr['fTotalPriceForwarderCurrency'] = $fTotalInvoiceValueUSD;
            $referaalAmountArr['idForwarderCurrency'] = 1;
            $referaalAmountArr['fForwarderExchangeRate'] = 1;
            $referaalAmountArr['szForwarderCurrency'] = 'USD'; 
            $referaalAmountArr['szBooking'] = 'Transporteca mark-up fee: '.$postSearchAry['szBookingRef'];  
            $referaalAmountArr['fUSDReferralFee'] = $fReferralAmountUSD;
            $referaalAmountArr['szBookingNotes'] = "We always keep this value in USD to display on Actual Revenue graph. ";
            $referaalAmountArr['dtPaymentConfirmed'] = $this->dtMarkupPaid; 
             
            $this->insertTransportecaMarkupFee($referaalAmountArr,true);
        
            $addFileActivityLogs=array();
            $addFileActivityLogs['idBooking'] = $idBooking;
            $addFileActivityLogs['idBookingFile'] = $idBookingFile;
            $addFileActivityLogs['szDataBeforeUpdate'] = print_R($oldValuesAry,true);
            $addFileActivityLogs['szDateTobeUpdated'] = print_R($res_ary,true);
            $addFileActivityLogs['idAdminAddedBy'] = $idAdminAddedBy;
            $addFileActivityLogs['szUpdateType'] = 'BOOKING';

            $kBooking->addBookingFileActivityLogs($addFileActivityLogs);

            $addWorkingCapitalAry = array();
            $addWorkingCapitalAry['fPriceUSD'] = $fForwarderTotalQuotePriceUSD + $fTotalVatForwarderCurrencyUSD;
            $addWorkingCapitalAry['szAmountType'] = 'Booking Buy + VAT';
            $addWorkingCapitalAry['iDebitCredit'] = 2;
            $addWorkingCapitalAry['szReference'] = $postSearchAry['szBookingRef'];
            $addWorkingCapitalAry['dtDateTime'] = $this->dtMarkupPaid;
            $addWorkingCapitalAry['iMarkupBooking'] = 1;
            $addWorkingCapitalAry['idBooking'] = $idBooking;
              
            $kDashBoardAdmin = new cDashboardAdmin();
            
            if($kDashBoardAdmin->isWorkingCapitalTransactionAlreadyAdded($idBooking))
            {
                $kDashBoardAdmin->updateGraphWorkingCapitalCurrent($addWorkingCapitalAry); 
            }
            else
            {
                $kDashBoardAdmin->addGraphWorkingCapitalCurrent($addWorkingCapitalAry); 
            } 
            return true;
        }
    }
    
    function insertTransportecaMarkupFee($data,$bPaid=false)
    { 
        if(!empty($data))
        { 
            if($bPaid)
            {
                $iDebitCredit = __TRANSACTION_DEBIT_CREDIT_FLAG_10__;
                $iStatus = 2;
            }
            else
            {
                $iDebitCredit = __TRANSACTION_DEBIT_CREDIT_FLAG_9__ ;
                $iStatus = 1;
            }
            
            if($this->isTransactionAlreadyAdded($data['idBooking'],$iDebitCredit))
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                    SET 
                        fTotalAmount = '".(float)mysql_escape_custom($data['fTotalAmount'])."', 
                        fTotalPriceForwarderCurrency = '".(float)mysql_escape_custom($data['fTotalPriceForwarderCurrency'])."',
                        idCurrency = '".mysql_escape_custom($data['idCurrency'])."',
                        szCurrency = '".mysql_escape_custom($data['szCurrency'])."',
                        fExchangeRate = '".mysql_escape_custom($data['fExchangeRate'])."',
                        idForwarderCurrency = '".mysql_escape_custom($data['idForwarderCurrency'])."',
                        szForwarderCurrency = '".mysql_escape_custom($data['szForwarderCurrency'])."',
                        fForwarderExchangeRate = '".mysql_escape_custom($data['fForwarderExchangeRate'])."',
                        szBooking = '".mysql_escape_custom($data['szBooking'])."',
                        szBookingNotes = '".mysql_escape_custom($data['szBookingNotes'])."',
                        fReferalAmount = '".mysql_escape_custom($data['fReferalAmount'])."',
                        fUSDReferralFee = '".mysql_escape_custom($data['fUSDReferralFee'])."',
                        dtPaymentConfirmed = '".mysql_escape_custom($data['dtPaymentConfirmed'])."',
                        dtUpdatedOn = now()
                    WHERE
                        idBooking = '".(int)$data['idBooking']."'
                    AND
                        iDebitCredit = '".(int)$iDebitCredit."' 
                ";
            }
            else
            {
                $kAdmin = new cAdmin();
                $invoiceNumber = $kAdmin->generateInvoiceForPayment();
            
                $query="
                    INSERT INTO
                        ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                    (
                        idBooking,
                        idForwarder,
                        fTotalAmount, 
                        fTotalPriceForwarderCurrency,
                        idCurrency,
                        szCurrency,
                        fExchangeRate,
                        idForwarderCurrency, 
                        szForwarderCurrency,
                        fForwarderExchangeRate,
                        szInvoice,
                        szBooking,
                        szBookingNotes,  
                        fReferalAmount,
                        fUSDReferralFee,
                        iDebitCredit,
                        iStatus,
                        iBatchNo,
                        dtInvoiceOn,
                        dtPaymentConfirmed, 
                        dtCreatedOn
                    )
                    VALUES
                    (
                        '".(int)mysql_escape_custom($data['idBooking'])."',
                        '".(int)mysql_escape_custom($data['idForwarder'])."',
                        '".(float)mysql_escape_custom($data['fTotalAmount'])."',
                        '".mysql_escape_custom($data['fTotalPriceForwarderCurrency'])."',
                        '".mysql_escape_custom($data['idCurrency'])."',
                        '".mysql_escape_custom($data['szCurrency'])."',
                        '".mysql_escape_custom($data['fExchangeRate'])."', 
                        '".mysql_escape_custom($data['idForwarderCurrency'])."', 
                        '".mysql_escape_custom($data['szForwarderCurrency'])."', 
                        '".mysql_escape_custom($data['fForwarderExchangeRate'])."',  
                        '".mysql_escape_custom($invoiceNumber)."', 
                        '".mysql_escape_custom($data['szBooking'])."', 
                        '".mysql_escape_custom($data['szBookingNotes'])."', 
                        '".mysql_escape_custom($data['fReferalAmount'])."',
                        '".mysql_escape_custom($data['fUSDReferralFee'])."',
                        '".(int)$iDebitCredit."',
                        '".(int)$iStatus."',
                        '',  
                        NOW(),
                        '".mysql_escape_custom($data['dtPaymentConfirmed'])."',
                        NOW()
                    )	
                ";
            }  
            if($result=$this->exeSQL($query))
            {
                return true;
            }
            else
            {
                return false;
            }
        } 
    }
    
    function isTransactionAlreadyAdded($idBooking,$iDebitCredit)
    {
        if($idBooking>0)
        { 
            $query="
                SELECT
                    id
                FROM
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                WHERE
                    idBooking = '".mysql_escape_custom($idBooking)."'	
                AND
                    iDebitCredit = ".(int)$iDebitCredit."
            "; 
            //echo $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
        	// Check if the user exists
                if( $this->getRowCnt() > 0 )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    
    function updateBillingBookingNotes($dataStr,$idBilling)
    {
       $query="
            UPDATE
                ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
            SET
               $dataStr,
               dtUpdatedOn = now()    
            WHERE
                id='".(int)$idBilling."'
          ";
       //echo $query;
        if($result=$this->exeSQL($query))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function load($idBilling)
    {
        $query="
            SELECT
                id,
                szBookingNotes
            FROM
                ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
            WHERE
                id='".(int)$idBilling."'
         ";
          if($result = $this->exeSQL($query))
        {
                $bookingAry=array();
                if($this->iNumRows>0)
                {
                        $ctr=0;
                        $row=$this->getAssoc($result);
                        $this->id = $row['id'] ;
                        $this->szBookingNotes = $row['szBookingNotes'] ;

                }
                else
                {
                        return array();
                }
        }
        else
        {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
        }   
    }
    
    function getForwarderTurnoverList($data,$bGetTotal=false)
    {
        if(!empty($data))
        {
            if(!empty($data['dtFromDate']))
            {
                $data['dtFromDate'] = format_date_time($data['dtFromDate']);
                $query_and .= " AND DATE(dtBookingConfirmed) >= '".mysql_escape_custom(trim($data['dtFromDate']))."' " ;
            }            
            if(!empty($data['dtToDate']))
            {
                $data['dtToDate'] = format_date_time($data['dtToDate']);
                $query_and .= " AND DATE(dtBookingConfirmed) <= '".mysql_escape_custom(trim($data['dtToDate']))."' " ;
            } 
            if($bGetTotal)
            {
                $query_select = " 
                    SUM(fTotalPriceCustomerCurrency*fCustomerExchangeRate) as fTotalSalesUSD,
                    SUM(fTotalVat*fCustomerExchangeRate) as fTotalVatUSD,
                    SUM(b.fReferalAmountUSD) as fTotalReferalAmountUSD,
                    SUM(b.fPriceMarkupCustomerCurrency*fCustomerExchangeRate) as fTotalPriceMarkupUSD,
                ";
                $query_group_by = " GROUP BY b.szForwarderDispName, b.idForwarder"; 
                $forwarderLabelAry = array();
                $forwarderLabelAry = $this->getBookingLabelFeeByForwarder($data); 
                
                $forwarderHandlingAry = array();
                $forwarderHandlingAry = $this->getBookingHandlingFeeByForwarder($data);
                
                 //print_r($forwarderHandlingAry);
            }
            else
            {
                $query_select = " 
                    (b.fTotalPriceCustomerCurrency*b.fCustomerExchangeRate) as fTotalPriceUSD,
                    (b.fTotalVat*b.fCustomerExchangeRate) as fTotalVatUSD, 
                    b.fReferalAmountUSD as fTotalReferalAmountUSD,
                    (b.fPriceMarkupCustomerCurrency*b.fCustomerExchangeRate) as fTotalPriceMarkupUSD,
                ";
                $query_group_by = "";
                //$query_sub = ", ( SELECT (fBookingLabelFeeRate*fBookingLabelFeeROE) as fLabelFee FROM ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__." cb WHERE idBooking = b.id AND cb.iCourierAgreementIncluded = '0' AND ) as fBookingLabelFee "; 
                $bookingLabelAry = array();
                $bookingLabelAry = $this->getBookingLabelFeeByForwarder($data,true); 
                
                
                $bookingHandlingAry = array();
                $bookingHandlingAry = $this->getBookingHandlingFeeByForwarder($data,true);
               
            }
            
            $query="
                SELECT
                    $query_select
                    b.id as idBooking,
                    b.szForwarderDispName,
                    b.idCustomerCurrency,
                    b.szCustomerCurrency, 
                    b.idForwarder,
                    b.szBookingRef,
                    b.dtBookingConfirmed, 
                    b.iBookingType,
                    b.fReferalPercentage,
                    b.fReferalAmount,
                    b.fReferalAmountUSD,
                    b.fTotalPriceForwarderCurrency,
                    b.fPriceMarkupCustomerCurrency,
                    f.szForwarderAlias,
                    b.idForwarder
                    $query_sub
                FROM
                    ".__DBC_SCHEMATA_BOOKING__." b
                INNER JOIN
                    ".__DBC_SCHEMATA_FROWARDER__." f
                ON
                    b.idForwarder = f.id
                WHERE
                    b.idBookingStatus IN (3,4)  
                    $query_and 
                    $query_group_by 
                ORDER BY
                    b.szForwarderDispName ASC, f.szForwarderAlias ASC
            ";
            //echo "<br> ".$query;
            if($result = $this->exeSQL($query))
            {
                $bookingAry=array();
                if($this->iNumRows>0)
                {
                    $ctr=0;
                    $retAry = array();
                    while($row=$this->getAssoc($result))
                    {
                        $retAry[$ctr] = $row;
                        $fTotalFeeUsd = 0;
                        $fTotalHandlingFeeUSD =0;
                        if($bGetTotal)
                        {
                            $fTotalFeeUsd = $forwarderLabelAry[$row['szForwarderDispName']][$row['idForwarder']]['fLabelFeeUSD'];
                            $fTotalHandlingFeeUSD = $forwarderHandlingAry[$row['szForwarderDispName']][$row['idForwarder']]['fTotalHandlingFee'];
                            
                           //echo $row['szForwarderDispName']."---".$fTotalFeeUsd."fTotalHandlingFeeUSD-------".$row['idForwarder'];
                        }
                        else
                        {
                            $fTotalFeeUsd = $bookingLabelAry[$row['idBooking']]['fLabelFeeUSD'];
                            $retAry[$ctr]['fBookingLabelFee'] = $fTotalFeeUsd; 
                            $fTotalHandlingFeeUSD = $bookingHandlingAry[$row['idBooking']]['fTotalHandlingFee'];
                            $retAry[$ctr]['fTotalHandlingFeeUSD'] = $fTotalHandlingFeeUSD;
                        }
                        $fTotalGrossProfit = $row['fTotalReferalAmountUSD'] + $row['fTotalPriceMarkupUSD'] + $fTotalFeeUsd+$fTotalHandlingFeeUSD;
                        $retAry[$ctr]['fTotalGrossProfitUSD'] = $fTotalGrossProfit;
                        $ctr++;
                    } 
                    return $retAry;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }   
        } 
    }
    
    function getBookingLabelFeeByForwarder($data,$bBookingFlag=false)
    {
        if(!empty($data))
        {
            if(!empty($data['dtFromDate']))
            { 
                $query_and .= " AND DATE(b.dtBookingConfirmed) >= '".mysql_escape_custom(trim($data['dtFromDate']))."' " ;
            }            
            if(!empty($data['dtToDate']))
            { 
                $query_and .= " AND DATE(b.dtBookingConfirmed) <= '".mysql_escape_custom(trim($data['dtToDate']))."' " ;
            }
            
            if($bBookingFlag)
            {
                $query_group_by = "";
                $query_select = " (cb.fBookingLabelFeeRate * cb.fBookingLabelFeeROE) as fLabelFee, ";
            }
            else
            {
                $query_group_by = " GROUP BY szForwarderDispName, idForwarder ";
                $query_select = " SUM(cb.fBookingLabelFeeRate * cb.fBookingLabelFeeROE) as fLabelFee, ";
            }
            
            $query="
                SELECT
                    $query_select
                    b.idForwarder,
                    b.id as idBooking,
                    b.szForwarderDispName
                FROM
                    ".__DBC_SCHEMATA_BOOKING__." b 
                INNER JOIN
                    ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__." cb
                ON
                    cb.idBooking = b.id
                WHERE
                    b.idBookingStatus IN (3,4)
                AND
                    cb.iCourierAgreementIncluded = '0'
                AND
                    b.idTransportMode = '".__BOOKING_TRANSPORT_MODE_COURIER__."'
                AND
                    (b.iBookingType = 2 OR b.iBookingType = 3)
                    $query_and  
                    $query_group_by
            ";
            //echo "<br> ".$query;
            if($result = $this->exeSQL($query))
            { 
                if($this->iNumRows>0)
                {
                    $ctr=0;
                    $retAry = array();
                    while($row=$this->getAssoc($result))
                    {
                        if($bBookingFlag)
                        {
                            $retAry[$row['idBooking']]['fLabelFeeUSD'] = $row['fLabelFee'];   
                        }
                        else
                        {
                            $retAry[$row['szForwarderDispName']][$row['idForwarder']]['fLabelFeeUSD'] = $row['fLabelFee'];   
                        } 
                    } 
                    return $retAry;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }   
        }
    }
    function downloadForwarderTurnoverSheet($forwarderTurnoverAry)
    {   
        $kBooking = new cBooking(); 
        require_once( __APP_PATH_CLASSES__ . "/PHPExcel.php" );

        $objPHPExcel=new PHPExcel(); 

        $sheetIndex=0;
        $objPHPExcel = new PHPExcel();
        
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex($sheetIndex);
        $sheet = $objPHPExcel->getActiveSheet(); 

        $styleArray = array(
                'font' => array(
                        'bold' => false,
                        'size' =>12,
                ),
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                ),
                'borders' => array(
                        'top' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'bottom' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'right' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'left' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                ),
        );

        $styleArray1 = array(
                'font' => array(
                        'bold' => false,
                        'size' =>12,
                ),
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                ),
                'borders' => array(
                        'top' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'bottom' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'right' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'left' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                ),
        ); 

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);

        $objPHPExcel->getActiveSheet()->getStyle('A1:K1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1:K1')->getFill()->getStartColor()->setARGB('FFddd9c3');
         
        $objPHPExcel->getActiveSheet()->setCellValue('A1','Forwarder Name')->getStyle('A1');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray); 

        $objPHPExcel->getActiveSheet()->setCellValue('B1','Forwarder Alias')->getStyle('B1');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->setCellValue('C1','Booking date')->getStyle('C1');
        $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
  
        $objPHPExcel->getActiveSheet()->setCellValue('D1','Booking reference')->getStyle('D1');
        $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray); 
        
        $objPHPExcel->getActiveSheet()->setCellValue('E1','Invoice value')->getStyle('E1');
        $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('F1','VAT')->getStyle('F1');
        $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);  
        
        $objPHPExcel->getActiveSheet()->setCellValue('G1','Referral fee')->getStyle('G1');
        $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($styleArray);  
        
        $objPHPExcel->getActiveSheet()->setCellValue('H1','Currency markup')->getStyle('H1');
        $objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($styleArray);  
        
        $objPHPExcel->getActiveSheet()->setCellValue('I1','Label fee')->getStyle('I1');
        $objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($styleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('J1','Handling Fee')->getStyle('J1');
        $objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($styleArray);
        
        
        $objPHPExcel->getActiveSheet()->setCellValue('K1','Gross profit')->getStyle('K1');
        $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($styleArray);
        
        $col = 2;
        /* 
        * Fetching data for last 2 months 
        */ 
   
        if(!empty($forwarderTurnoverAry))
        {
            foreach($forwarderTurnoverAry as $forwarderTurnoverArys)
            {     
                $objPHPExcel->getActiveSheet()->setCellValue("A".$col,$forwarderTurnoverArys['szForwarderDispName']);
                $objPHPExcel->getActiveSheet()->setCellValue("B".$col,$forwarderTurnoverArys['szForwarderAlias']);
                $objPHPExcel->getActiveSheet()->setCellValue("C".$col,date('Y-m-d',strtotime($forwarderTurnoverArys['dtBookingConfirmed'])));
                $objPHPExcel->getActiveSheet()->setCellValue("D".$col,$forwarderTurnoverArys['szBookingRef']);  
                $objPHPExcel->getActiveSheet()->setCellValue("E".$col,"USD ".number_format((float)$forwarderTurnoverArys['fTotalPriceUSD'],2)); 
                $objPHPExcel->getActiveSheet()->setCellValue("F".$col,"USD ".number_format((float)$forwarderTurnoverArys['fTotalVatUSD'],2)); 
                $objPHPExcel->getActiveSheet()->setCellValue("G".$col,"USD ".number_format((float)$forwarderTurnoverArys['fReferalAmountUSD'],2)); 
                $objPHPExcel->getActiveSheet()->setCellValue("H".$col,"USD ".number_format((float)$forwarderTurnoverArys['fTotalPriceMarkupUSD'],2)); 
                $objPHPExcel->getActiveSheet()->setCellValue("I".$col,"USD ".number_format((float)$forwarderTurnoverArys['fBookingLabelFee'],2)); 
                $objPHPExcel->getActiveSheet()->setCellValue("J".$col,"USD ".number_format((float)$forwarderTurnoverArys['fTotalHandlingFeeUSD'],2)); 
                $objPHPExcel->getActiveSheet()->setCellValue("K".$col,"USD ".number_format((float)$forwarderTurnoverArys['fTotalGrossProfitUSD'],2)); 
                $col++;
                $iTotalResponseTime += $lastMonthDataArys['iSearchResponseTime'];
            }
        }         
        
        $title = "_Transporteca_Forwarder_Turnover";
        $objPHPExcel->getActiveSheet()->setTitle('Forwarder Turnover');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->removeSheetByIndex(1);
        $szFileName = date('Ymdhs').$title.".xlsx";
        $fileName=__APP_PATH_ROOT__."/insuranceSheets/".$szFileName; 
        $objWriter->save($fileName); 
        return $szFileName;  
    }
    
    
    function getBookingHandlingFeeByForwarder($data,$bBookingFlag=false)
    {
        if(!empty($data))
        {
            if(!empty($data['dtFromDate']))
            { 
                $query_and .= " AND DATE(dtBookingConfirmed) >= '".mysql_escape_custom(trim($data['dtFromDate']))."' " ;
            }            
            if(!empty($data['dtToDate']))
            { 
                $query_and .= " AND DATE(dtBookingConfirmed) <= '".mysql_escape_custom(trim($data['dtToDate']))."' " ;
            }
            
            if($bBookingFlag)
            {
                $query_group_by = "";
                $query_select = " (fTotalHandlingFeeUSD) as fTotalHandlingFee";
            }
            else
            {
                $query_group_by = " GROUP BY szForwarderDispName, idForwarder ";
                $query_select = " SUM(fTotalHandlingFeeUSD) as fTotalHandlingFee";
            }
            
            $query="
                SELECT
                    id as idBooking,
                    idForwarder,
                    szForwarderDispName,
                    $query_select 	
                FROM
                    ".__DBC_SCHEMATA_BOOKING__."                 
                WHERE
                    idBookingStatus IN (3,4)
                AND
                    iHandlingFeeApplicable='1'
                     $query_and  
                    $query_group_by
            ";
            //echo "<br> ".$query;
            if($result = $this->exeSQL($query))
            { 
                if($this->iNumRows>0)
                {
                    $ctr=0;
                    $retAry = array();
                    while($row=$this->getAssoc($result))
                    {
                        if($bBookingFlag)
                        {
                            $retAry[$row['idBooking']]['fTotalHandlingFee'] = $row['fTotalHandlingFee'];   
                        }
                        else
                        {
                            $retAry[$row['szForwarderDispName']][$row['idForwarder']]['fTotalHandlingFee'] = $row['fTotalHandlingFee'];   
                        } 
                    } 
                    return $retAry;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }   
        }
    }
    
    
    function getTransportecaTurnoverList($data,$GTotalFlag=false)
    {
        if(!empty($data['dtFromDate']))
        {
            $data['dtFromDate'] = format_date_time($data['dtFromDate']);
            $query_and .= " AND DATE(dtBookingConfirmed) >= '".mysql_escape_custom(trim($data['dtFromDate']))."' " ;
        }            
        if(!empty($data['dtToDate']))
        {
            $data['dtToDate'] = format_date_time($data['dtToDate']);
            $query_and .= " AND DATE(dtBookingConfirmed) <= '".mysql_escape_custom(trim($data['dtToDate']))."' " ;
        } 
        $innerJoinQuery='';
        if($GTotalFlag)
        {
            $referalFeeMarkup="
                sum(bk.fReferalAmountUSD) AS fTotalReferalAmountUSD,
                sum(bk.fPriceMarkupCustomerCurrency * bk.fCustomerExchangeRate) AS fPriceMarkupUSD";
            
            
            $labelFee="sum(cbd.fBookingLabelFeeRate * cbd.fBookingLabelFeeROE ) AS fTotalLabelFeeAmountUSD";
            
            $HandlingFee="sum(fTotalHandlingFeeUSD) AS fTotalBookingHandlingFeeUSD";
            
            $InsuranceSellPrice="sum(fTotalInsuranceCostForBookingUSD) AS fTotalInsuranceSellPriceUSD";
            $resArr=array("Referral fee"=>"","Handling fee"=>"","Label fee"=>"","Insurance"=>"","Currency markup"=>"");
        }
        else
        {
            $referalFeeMarkup="bk.id,
                bk.fTotalPriceUSD AS fTurnover,
                bk.fReferalAmountUSD AS fTotalReferalAmountUSD,
                (bk.fPriceMarkupCustomerCurrency * bk.fCustomerExchangeRate) AS fPriceMarkupUSD,
                bk.dtBookingConfirmed,
                bk.szForwarderDispName,
                bk.szBookingRef,
                f.szForwarderAlias,
                bk.szCargoDescription";
            $innerJoinQuery="INNER JOIN
                    ".__DBC_SCHEMATA_FROWARDER__." f
                ON
                    f.id=bk.idForwarder
            ";
            $labelFee="(cbd.fBookingLabelFeeRate * cbd.fBookingLabelFeeROE ) AS fTotalLabelFeeAmountUSD,
                bt.id";
            
            $HandlingFee="fTotalHandlingFeeUSD AS fTotalBookingHandlingFeeUSD,
                id";
            
            $InsuranceSellPrice="fTotalInsuranceCostForBookingUSD AS fTotalInsuranceSellPriceUSD,
                id";
        }

        $query="
            SELECT	
                $referalFeeMarkup
            FROM
                ".__DBC_SCHEMATA_BOOKING__." as bk
                $innerJoinQuery
            WHERE
                ( 
                    bk.`dtBookingConfirmed` != '0000-00-00 00:00:00'
                    $query_and
                AND 
                    bk.idBookingStatus IN(3,4) 
                AND
                    bk.iTransferConfirmed = '0'
                )
        ";
        //echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                if($GTotalFlag)
                {
                    $resArr["Referral fee"]=$row['fTotalReferalAmountUSD'];
                    $fPriceMarkupUSD=$row['fPriceMarkupUSD'];
                }
                else
                {
                    $resArr[]=$row;
                }
                
            }
        }
        
        $ctr=0;
        $query="
            SELECT	
                $HandlingFee
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE
                 `dtBookingConfirmed` != '0000-00-00 00:00:00'
            $query_and  
            AND 
                idBookingStatus IN(3,4)
            AND
                iHandlingFeeApplicable='1'
            AND
                iTransferConfirmed = '0'
        ";
        //echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                if($GTotalFlag)
                {
                    $resArr["Handling fee"]=$row['fTotalBookingHandlingFeeUSD'];
                }
//                else
//                {
//                    if(!empty($resArr[$row['id']]))
//                    {
//                        $resArr[$row['id']]=  array_merge($resArr[$row['id']],$row);
//                    }
//                    else
//                    {
//                        $resArr[$row['id']]=$row;
//                    }  
//                }
                ++$ctr;
            }
        }
        
        //print_r($resArr);
        $ctr=0;
        $query="
            SELECT	
                $labelFee
            FROM
                ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__." AS cbd
            INNER JOIN
                ".__DBC_SCHEMATA_BOOKING__." as bt
            ON
                bt.id=cbd.idBooking
            WHERE
                `dtBookingConfirmed` != '0000-00-00 00:00:00'
            $query_and 
            AND 
                bt.idBookingStatus IN(3,4) 
            AND
                bt.iTransferConfirmed = '0'
            AND
                cbd.iCourierAgreementIncluded = '0'    
            AND
            (
                bt.iCourierBooking='1'
            OR
                bt.isManualCourierBooking='1'
            )        
        ";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                if($GTotalFlag)
                {
                    $resArr["Label fee"]=$row['fTotalLabelFeeAmountUSD'];
                }
//                else
//                {
//                    if(!empty($resArr[$row['id']]))
//                    {
//                        $resArr[$row['id']]=  array_merge($resArr[$row['id']],$row);
//                    }
//                    else
//                    {
//
//                        $resArr[$row['id']]=$row;
//                    } 
//                }
                ++$ctr;            
                
            }
        }
        
        $ctr=0;
        $query="
            SELECT	
                $InsuranceSellPrice
            FROM
                    ".__DBC_SCHEMATA_BOOKING__." as b2 
            WHERE
            ( 
                `dtBookingConfirmed` != '0000-00-00 00:00:00'
            $query_and
            AND 
                idBookingStatus IN(3,4) 
            AND
                iInsuranceIncluded = '1'
            AND
                iInsuranceStatus !='3'
             AND
                iTransferConfirmed = '0'
            )
        ";
        //echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                if($GTotalFlag)
                {
                    $resArr["Insurance"]=$row['fTotalInsuranceSellPriceUSD'];
                }
//                else
//                {
//                    if(!empty($resArr[$row['id']]))
//                    {
//                        $resArr[$row['id']]=  array_merge($resArr[$row['id']],$row);
//                    }
//                    else
//                    {
//                        $resArr[$row['id']]=$row;
//                    } 
//                }
                ++$ctr;
            }
        }
       
        if($GTotalFlag)
        {
            $resArr["Currency markup"]=$fPriceMarkupUSD;
        }
        
        return $resArr;
    }
    
    
    function downloadTransportecaTurnoverSheet($forwarderTurnoverAry)
    {   
        $kBooking = new cBooking(); 
        require_once( __APP_PATH_CLASSES__ . "/PHPExcel.php" );

        $objPHPExcel=new PHPExcel(); 

        $sheetIndex=0;
        $objPHPExcel = new PHPExcel();
        
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex($sheetIndex);
        $sheet = $objPHPExcel->getActiveSheet(); 

        $styleArray = array(
                'font' => array(
                        'bold' => false,
                        'size' =>12,
                ),
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                ),
                'borders' => array(
                        'top' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'bottom' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'right' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'left' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                ),
        );

        $styleArray1 = array(
                'font' => array(
                        'bold' => false,
                        'size' =>12,
                ),
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                ),
                'borders' => array(
                        'top' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'bottom' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'right' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'left' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                ),
        ); 
        $styleArray2 = array(
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                ),
                
        );

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15); 

        $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getFill()->getStartColor()->setARGB('FFddd9c3');
         
        $objPHPExcel->getActiveSheet()->setCellValue('A1','Invoice Date')->getStyle('A1');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray); 

        $objPHPExcel->getActiveSheet()->setCellValue('B1','Reference')->getStyle('B1');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->setCellValue('C1','Booking')->getStyle('C1');
        $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
  
        $objPHPExcel->getActiveSheet()->setCellValue('D1','Forwarder')->getStyle('D1');
        $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray); 
        
        $objPHPExcel->getActiveSheet()->setCellValue('E1','Description')->getStyle('E1');
        $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('F1','Amount')->getStyle('F1');
        $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('G1','Amount(USD)')->getStyle('G1');
        $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($styleArray1);
        
        $col = 2;
        /* 
        * Fetching data for last 2 months 
        */ 
   
        if(!empty($forwarderTurnoverAry))
        {
            foreach($forwarderTurnoverAry as $forwarderTurnoverArys)
            {     
                $szBookingRef="N/A";
                if($forwarderTurnoverArys['szBookingRef']!='')
                {
                    $szBookingRef=$forwarderTurnoverArys['szBookingRef'];
                }
                
                if($forwarderTurnoverArys['fExchangeRate']!='')
                {
                    $fTotalPriceForwarderCurrencyUSD=$forwarderTurnoverArys['fTotalPriceForwarderCurrency']*$forwarderTurnoverArys['fExchangeRate'];
                    
                }
                
                //$objPHPExcel->getActiveSheet()->getStyle('F'.$col)->getNumberFormat()->setFormatCode('#,##0.00');
                //$objPHPExcel->getActiveSheet()->getStyle('G'.$col)->getNumberFormat()->setFormatCode('#,##0.00');
                
                $objPHPExcel->getActiveSheet()->getStyle('B'.$col)->getNumberFormat()->setFormatCode('General');
                $objPHPExcel->getActiveSheet()->setCellValue("A".$col,date('Y-m-d',strtotime($forwarderTurnoverArys['dtPaymentConfirmed'])));
                $objPHPExcel->getActiveSheet()->setCellValue("B".$col,' '.$forwarderTurnoverArys['szInvoice'].'');
                $objPHPExcel->getActiveSheet()->setCellValue("C".$col,$szBookingRef);
                $objPHPExcel->getActiveSheet()->setCellValue("D".$col,$forwarderTurnoverArys['szDisplayName']);  
                
                if(($forwarderTurnoverArys['iDebitCredit']=='3' && $forwarderTurnoverArys['iDebitCredit']!='1'))
                {
                    $objPHPExcel->getActiveSheet()->setCellValue("E".$col,$forwarderTurnoverArys['szBooking']); 
                    $objPHPExcel->getActiveSheet()->setCellValue("F".$col,$forwarderTurnoverArys['szCurrency']." ".number_format((float)$forwarderTurnoverArys['fTotalPriceForwarderCurrency'],2)); 
                    $objPHPExcel->getActiveSheet()->setCellValue("G".$col,round((float)$fTotalPriceForwarderCurrencyUSD,2));
                }
                else if($forwarderTurnoverArys['iDebitCredit']=='8')
                {
                    if($forwarderTurnoverArys['szBooking']=='Courier Booking and Labels Invoice' && $forwarderTurnoverArys['fTotalPriceForwarderCurrency']<0)
                    {
                        $forwarderTurnoverArys['szBooking'] = 'Courier Booking and Labels Credit Note';
                        $szAmountStr = " (".$forwarderTurnoverArys['szCurrency']." ".number_format((float)abs($forwarderTurnoverArys['fTotalPriceForwarderCurrency']),2).")";
                        $fTotalPriceForwarderCurrencyUSDStrNegative="(".round((float)$fTotalPriceForwarderCurrencyUSD,2).")";
                        $objPHPExcel->getActiveSheet()->setCellValue("G".$col,$fTotalPriceForwarderCurrencyUSDStrNegative);
                    }
                    else
                    {
                        $szAmountStr = $forwarderTurnoverArys['szCurrency']." ".number_format($forwarderTurnoverArys['fTotalPriceForwarderCurrency'],2);
                        $objPHPExcel->getActiveSheet()->setCellValue("G".$col,round((float)$fTotalPriceForwarderCurrencyUSD,2));
                    }
                    
                    $objPHPExcel->getActiveSheet()->setCellValue("E".$col,$forwarderTurnoverArys['szBooking']); 
                    $objPHPExcel->getActiveSheet()->setCellValue("F".$col,$szAmountStr); 
                }
                else if(($forwarderTurnoverArys['iDebitCredit']=='12'))
                {
                    if($forwarderTurnoverArys['szBooking']==__HANDLING_FEE_INVOICE_TEXT__ && $forwarderTurnoverArys['fTotalPriceForwarderCurrency']<0)
                    {
                        $forwarderTurnoverArys['szBooking'] = __HANDLING_FEE_CREDIT_NOTE_TEXT__;
                        $szAmountStr = " (".$forwarderTurnoverArys['szCurrency']." ".number_format((float)abs($forwarderTurnoverArys['fTotalPriceForwarderCurrency']),2).")";
                        $fTotalPriceForwarderCurrencyUSDStrNegative="(".round((float)$fTotalPriceForwarderCurrencyUSD,2).")";
                        $objPHPExcel->getActiveSheet()->setCellValue("G".$col,$fTotalPriceForwarderCurrencyUSDStrNegative);
                    }
                    else
                    {
                        $szAmountStr = $forwarderTurnoverArys['szCurrency']." ".number_format($forwarderTurnoverArys['fTotalPriceForwarderCurrency'],2);
                        $objPHPExcel->getActiveSheet()->setCellValue("G".$col,round((float)$fTotalPriceForwarderCurrencyUSD,2));
                    }
                    
                    $objPHPExcel->getActiveSheet()->setCellValue("E".$col,$forwarderTurnoverArys['szBooking']); 
                    $objPHPExcel->getActiveSheet()->setCellValue("F".$col,$szAmountStr); 
                    
                }
                else if($forwarderTurnoverArys['iDebitCredit']=='5')
                {
                    $objPHPExcel->getActiveSheet()->setCellValue("E".$col,$forwarderTurnoverArys['szBooking']); 
                    $objPHPExcel->getActiveSheet()->setCellValue("F".$col,$forwarderTurnoverArys['szForwarderCurrency']." ".number_format((float)$forwarderTurnoverArys['fTotalPriceForwarderCurrency'],2)); 
                    $objPHPExcel->getActiveSheet()->setCellValue("G".$col,round((float)$fTotalPriceForwarderCurrencyUSD,2));
                }
                else if($forwarderTurnoverArys['iDebitCredit']=='7')
                {
                    $szFromPage='';
                    if($forwarderTurnoverArys['iInsuranceStatus']==__BOOKING_INSURANCE_STATUS_CANCELLED__)
                    {
                        $szFromPage = 'CANCELLED_INSURED_BOOKING';
                        $szAmountStr = "(".$forwarderTurnoverArys['szCurrency']." ".number_format($forwarderTurnoverArys['fTotalPriceForwarderCurrency'],2).")";
                        $fTotalPriceForwarderCurrencyUSDStrNegative="(".round((float)$fTotalPriceForwarderCurrencyUSD,2).")";
                        $objPHPExcel->getActiveSheet()->setCellValue("G".$col,$fTotalPriceForwarderCurrencyUSDStrNegative);
                    }
                    else
                    {
                        $szAmountStr = $forwarderTurnoverArys['szCurrency']." ".number_format($forwarderTurnoverArys['fTotalPriceForwarderCurrency'],2) ;
                        $objPHPExcel->getActiveSheet()->setCellValue("G".$col,round((float)$fTotalPriceForwarderCurrencyUSD,2));
                    }
                    
                    $objPHPExcel->getActiveSheet()->setCellValue("E".$col,$forwarderTurnoverArys['szBooking']); 
                    $objPHPExcel->getActiveSheet()->setCellValue("F".$col,$szAmountStr); 
                }
                $objPHPExcel->getActiveSheet()->getStyle('G'.$col)->applyFromArray($styleArray2);
                
                 
                $col++;
                $iTotalResponseTime += $lastMonthDataArys['iSearchResponseTime'];
            }
        }         
        
        $title = "_Transporteca_Turnover";
        $objPHPExcel->getActiveSheet()->setTitle('Transporteca Turnover');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->removeSheetByIndex(1);
        $szFileName = date('Ymdhsi').$title.".xlsx";
        $fileName=__APP_PATH_ROOT__."/insuranceSheets/".$szFileName; 
        $objWriter->save($fileName); 
        return $szFileName;  
    }
    
    function updateExchangeRate()
    {
        
        $kConfig = new cConfig();
        $query="
            SELECT
                t.id,
                t.idForwarder,
                t.szInvoice,
                t.szBooking,
                t.dtPaymentConfirmed,
                t.fTotalPriceForwarderCurrency,
                t.szCurrency,
                f.szDisplayName,
                t.iBatchNo,
                t.iStatus,
                t.iDebitCredit,
                t.szForwarderCurrency	
            FROM
                ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS t
            JOIN 
                ".__DBC_SCHEMATA_FROWARDER__." AS f
            ON
                (t.idForwarder=f.id)
            WHERE 
            (
                t.iDebitCredit='3'
            OR
                t.iDebitCredit='5'
            OR
                t.iDebitCredit='8'
            OR
                t.iDebitCredit='12'
            )	
            $query_and   
            ORDER BY dtPaymentConfirmed
            DESC, t.szInvoice DESC
         ";
        echo "<br>".$query."<br>";
        //die;
        if($result=$this->exeSQL($query))
        {
            if($this->iNumRows>0)
            {	
                $ctr=0;
                while($row=$this->getAssoc($result))
                {
                    //$payment[$ctr]=$row;
                    //$ctr++;
                    if($row['szCurrency']!=''){
                    
                    $fExchangeRate = $this->getExchangeRateBillingPrice($row['szCurrency'],$row['dtPaymentConfirmed']);
                        if($fExchangeRate>0)
                        {
                            $upquery="
                                UPDATE
                                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                                SET	
                                    fExchangeRate = '".(float)$fExchangeRate."' 
                                WHERE
                                    id = '".(int)$row['id']."'			
                            ";
                            echo $upquery."<br/><br/>";
                            $upresult=$this->exeSQL($upquery);
                        }
                        else
                        {
                            $fExchangeNewRate = $this->getExchangeRateForBookingTable180Days($row['iBatchNo'],$row['dtPaymentConfirmed']);
                            $upquery="
                                UPDATE
                                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                                SET	
                                    fExchangeRate = '".(float)$fExchangeNewRate."' 
                                WHERE
                                    id = '".(int)$row['id']."'			
                            ";
                            echo $upquery."<br/><br/>";
                            $upresult=$this->exeSQL($upquery);
                        }
                    }
                    else
                    {
                        
                        
                       $fExchangeRate = $this->getExchangeRateBillingPrice($row['szForwarderCurrency'],$row['dtPaymentConfirmed']);
                        if($fExchangeRate>0)
                        {
                            $upquery="
                                UPDATE
                                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                                SET	
                                    fExchangeRate = '".(float)$fExchangeRate."' 
                                WHERE
                                    id = '".(int)$row['id']."'			
                            ";
                            echo $upquery."<br/><br/>";
                            $upresult=$this->exeSQL($upquery);
                        }
                        else
                        {
                            $fExchangeNewRate = $this->getExchangeRateForBookingTable180Days($row['iBatchNo'],$row['dtPaymentConfirmed']);
                            $upquery="
                                UPDATE
                                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                                SET	
                                    fExchangeRate = '".(float)$fExchangeNewRate."' 
                                WHERE
                                    id = '".(int)$row['id']."'			
                            ";
                            echo $upquery."<br/><br/>";
                            $upresult=$this->exeSQL($upquery);
                        }
                    }    
                    
                } 
            }
        }
    }
    
    function getExchangeRateBillingPrice($szCurrencyCode,$dtDate='')
    {
        if($dtDate!='' && $dtDate!='0000-00-00 00:00:00' && $dtDate!='0000-00-00')
        {
            $queryWhere="AND DATE(dtDate)='".  mysql_escape_custom(date('Y-m-d',strtotime($dtDate)))."'";
        }
        
        if(strtolower($szCurrencyCode)=='usd')
        {
            $this->iCurrencyCodeExists = 1;
            return '1.0000'; 
        }
        
    	$query="
            SELECT
                ec.id,
            	ec.fUsdValue
            FROM
            	".__DBC_SCHEMATA_CURRENCY_EXCHANGE_RATE__." AS ec
            INNER JOIN
            	 ".__DBC_SCHEMATA_CURRENCY__." AS c
            ON
            	ec.idCurrency=c.id
            WHERE
            	LOWER(c.szCurrency) = '".mysql_escape_custom(strtolower($szCurrencyCode))."'
            $queryWhere        
            ORDER BY
                dtDate DESC
            LIMIT
                0,1
        ";   
        //echo $query."<br />";
    	if( ( $result = $this->exeSQL( $query ) ) )
        {
            if($this->getRowCnt()>0)
            {
            	$ret_ary = array();
                $ctr = 0;
                $row = $this->getAssoc($result); 
                if($row['id']>0)
                {
                    $this->iCurrencyCodeExists = 1;
                    return $row['fUsdValue']; 
                }
                else
                {
                    $this->iCurrencyCodeExists = false;
                    return false; 
                } 
            }
        } 
    }
    
    
    function getExchangeRateForBookingTable180Days($iBatchNo,$dtDate='')
    {
        if($dtDate!='' && $dtDate!='0000-00-00 00:00:00' && $dtDate!='0000-00-00')
        {
            $queryWhere="AND DATE(dtBookingConfirmed) <='".  mysql_escape_custom(date('Y-m-d',strtotime($dtDate)))."'";
        }
        
       
        
    	$query="
            SELECT
                b.id,
                b.fForwarderExchangeRate AS fUsdValue
            FROM
            	".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS ft
            INNER JOIN
            	 ".__DBC_SCHEMATA_BOOKING__." AS b
            ON
            	ft.idBooking=b.id
            WHERE
            	LOWER(ft.iBatchNo) = '".mysql_escape_custom(strtolower($iBatchNo))."'
            AND        
                b.idBookingStatus IN(3,4,6,7) 
            AND
                b.iTransferConfirmed = '0'        
                    
            ORDER BY
                b.dtBookingConfirmed DESC
            LIMIT
                0,1
        ";   
        echo $query."<br /><br />";
    	if( ( $result = $this->exeSQL( $query ) ) )
        {
            if($this->getRowCnt()>0)
            {
            	$ret_ary = array();
                $ctr = 0;
                $row = $this->getAssoc($result); 
                if($row['id']>0)
                {
                    $this->iCurrencyCodeExists = 1;
                    return $row['fUsdValue']; 
                }
                else
                {
                    $this->iCurrencyCodeExists = false;
                    return false; 
                } 
            }
        } 
    }
    
    
    function getBookingCurrencyMarkupByBatchNumber($iBatchNo,$data=array())
    {
        if(!empty($data['dtFromDate']))
        {
            $data['dtFromDate'] = format_date_time($data['dtFromDate']);
            $query_and .= " AND DATE(dtPaymentConfirmed) >= '".mysql_escape_custom(trim($data['dtFromDate']))."' " ;
        }            
        if(!empty($data['dtToDate']))
        {
            $data['dtToDate'] = format_date_time($data['dtToDate']);
            $query_and .= " AND DATE(dtPaymentConfirmed) <= '".mysql_escape_custom(trim($data['dtToDate']))."' " ;
        }
        
       
        
    	$query="
            SELECT
                sum(bk.fPriceMarkupCustomerCurrency * bk.fCustomerExchangeRate) AS fPriceMarkupUSD
            FROM
            	".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS ft
            INNER JOIN
            	 ".__DBC_SCHEMATA_BOOKING__." AS bk
            ON
            	ft.idBooking=bk.id
            WHERE
            	LOWER(ft.iBatchNo) = '".mysql_escape_custom(strtolower($iBatchNo))."'
            AND        
                bk.idBookingStatus IN(3,4) 
            AND
                bk.iTransferConfirmed = '0'        
            $query_and        
            ORDER BY
                bk.dtBookingConfirmed DESC
            LIMIT
                0,1
        ";   
       // echo $query."<br /><br />";
    	if( ( $result = $this->exeSQL( $query ) ) )
        {
            if($this->getRowCnt()>0)
            {
            	$ret_ary = array();
                $ctr = 0;
                $row = $this->getAssoc($result); 
                  
                return $row['fPriceMarkupUSD']; 
                
            }
        } 
    }
    
    function userBookingDataList($idUser,$flag)
    {
        $kBooking = new cBooking();
        $bookingArr=$kBooking->getAllMyBooking($idUser,$flag,array(),true);
        if(!empty($bookingArr))
        {
            $ctr=0;
           foreach($bookingArr as $myBookingArrs)
           {
                $bookingApiArr[$ctr]['szBookingRandomNum']=$myBookingArrs['szBookingRandomNum'];
                $originCity='';
                $desCity='';
                if($myBookingArrs['idServiceType']==__SERVICE_TYPE_DTD__)
                {
                        $servicename="DTD,";
                        if(strlen($myBookingArrs['szShipperCity_pickup'])>12)
                        {
                                $originCity=mb_substr_replace($myBookingArrs['szShipperCity_pickup'],'...',12,strlen($myBookingArrs['szShipperCity_pickup']),$format);
                        }
                        else
                        {
                                $originCity=$myBookingArrs['szShipperCity_pickup'];
                        }
                        if(strlen($myBookingArrs['szConsigneeCity_pickup'])>12)
                        {
                                $desCity=mb_substr_replace($myBookingArrs['szConsigneeCity_pickup'],'...',12,strlen($myBookingArrs['szConsigneeCity_pickup']),$format);
                        }
                        else
                        {
                                $desCity=$myBookingArrs['szConsigneeCity_pickup'];
                        }
                        
                        if(!empty($myBookingArrs['dtCutOff']) && $myBookingArrs['dtCutOff']!='0000-00-00 00:00:00')
                        {
                            $bookingApiArr[$ctr]['dtExpectedPickup']= date('d-m-Y',strtotime($myBookingArrs['dtCutOff']));
                        }

                        if(!empty($myBookingArrs['dtAvailable']) && $myBookingArrs['dtAvailable']!='0000-00-00 00:00:00')
                        {
                            $bookingApiArr[$ctr]['dtExpectedDelivery']= date('d-m-Y',strtotime($myBookingArrs['dtCutOff']));
                        }
                        
                        
                }
                else if($myBookingArrs['idServiceType']==__SERVICE_TYPE_DTW__ || $myBookingArrs['idServiceType']==__SERVICE_TYPE_DTP__)
                {
                        if($myBookingArrs['idServiceType']==__SERVICE_TYPE_DTW__)
                        {
                                $servicename="DTP,";
                        }
                        else if($myBookingArrs['idServiceType']==__SERVICE_TYPE_DTP__)
                        {
                                $servicename="DTP,";
                        }

                        if(strlen($myBookingArrs['szShipperCity_pickup'])>12)
                        {
                                $originCity=mb_substr_replace($myBookingArrs['szShipperCity_pickup'],'...',12,strlen($myBookingArrs['szShipperCity_pickup']),$format);
                        }
                        else
                        {
                                $originCity=$myBookingArrs['szShipperCity_pickup'];
                        }
                        if(strlen($myBookingArrs['szWarehouseToCity'])>12)
                        {
                                $desCity=mb_substr_replace($myBookingArrs['szWarehouseToCity'],'...',12,strlen($myBookingArrs['szWarehouseToCity']),$format);
                        }
                        else
                        {
                                $desCity=$myBookingArrs['szWarehouseToCity'];
                        }
                        
                        if(!empty($myBookingArrs['dtCutOff']) && $myBookingArrs['dtCutOff']!='0000-00-00 00:00:00')
                        {
                            $bookingApiArr[$ctr]['dtExpectedPickup']= date('d-m-Y',strtotime($myBookingArrs['dtCutOff']));
                        }

                        if(!empty($myBookingArrs['dtAvailable']) && $myBookingArrs['dtAvailable']!='0000-00-00 00:00:00')
                        {
                            $bookingApiArr[$ctr]['dtAvailable']= date('d-m-Y',strtotime($myBookingArrs['dtCutOff']));
                        }

                }
                else if($myBookingArrs['idServiceType']==__SERVICE_TYPE_WTD__ || $myBookingArrs['idServiceType']==__SERVICE_TYPE_PTD__)
                {
                        if($myBookingArrs['idServiceType']==__SERVICE_TYPE_WTD__)
                        {
                            $servicename="WTD,";
                        }
                        else if($myBookingArrs['idServiceType']==__SERVICE_TYPE_PTD__)
                        {
                            $servicename="PTD,";
                        }

                        if(strlen($myBookingArrs['szWarehouseFromCity'])>12)
                        {
                            $originCity=mb_substr_replace($myBookingArrs['szWarehouseFromCity'],'...',12,strlen($myBookingArrs['szWarehouseFromCity']),$format);
                        }
                        else
                        {
                            $originCity=$myBookingArrs['szWarehouseFromCity'];
                        }

                        if(strlen($myBookingArrs['szConsigneeCity_pickup'])>12)
                        {
                            $desCity=mb_substr_replace($myBookingArrs['szConsigneeCity_pickup'],'...',12,strlen($myBookingArrs['szConsigneeCity_pickup']),$format);
                        }
                        else
                        {
                            $desCity=$myBookingArrs['szConsigneeCity_pickup'];
                        }
                        
                        if(!empty($myBookingArrs['dtCutOff']) && $myBookingArrs['dtCutOff']!='0000-00-00 00:00:00')
                        {
                            $bookingApiArr[$ctr]['dtCutOff']= date('d-m-Y',strtotime($myBookingArrs['dtCutOff']));
                        }

                        if(!empty($myBookingArrs['dtAvailable']) && $myBookingArrs['dtAvailable']!='0000-00-00 00:00:00')
                        {
                            $bookingApiArr[$ctr]['dtExpectedDelivery']= date('d-m-Y',strtotime($myBookingArrs['dtCutOff']));
                        }

                }
                else if($myBookingArrs['idServiceType']==__SERVICE_TYPE_WTW__ || $myBookingArrs['idServiceType']==__SERVICE_TYPE_WTP__ || $myBookingArrs['idServiceType']==__SERVICE_TYPE_PTW__ || $myBookingArrs['idServiceType']==__SERVICE_TYPE_PTP__)
                {
                        if($myBookingArrs['idServiceType']==__SERVICE_TYPE_WTW__)
                        {
                                $servicename="WTW,";
                        }
                        else if($myBookingArrs['idServiceType']==__SERVICE_TYPE_PTW__)
                        {
                                $servicename="PTW,";
                        }
                        else if($myBookingArrs['idServiceType']==__SERVICE_TYPE_WTP__)
                        {
                                $servicename="WTP,";
                        }
                        else if($myBookingArrs['idServiceType']==__SERVICE_TYPE_PTP__)
                        {
                                $servicename="PTP,";
                        }

                        if(strlen($myBookingArrs['szWarehouseFromCity'])>12)
                        {
                            $originCity=mb_substr_replace($myBookingArrs['szWarehouseFromCity'],'...',12,strlen($myBookingArrs['szWarehouseFromCity']),$format);
                        }
                        else
                        {
                            $originCity=$myBookingArrs['szWarehouseFromCity'];
                        }
                        if(strlen($myBookingArrs['szWarehouseToCity'])>12)
                        {
                                $desCity=mb_substr_replace($myBookingArrs['szWarehouseToCity'],'...',12,strlen($myBookingArrs['szWarehouseToCity']),$format);
                        }
                        else
                        {
                                $desCity=$myBookingArrs['szWarehouseToCity'];
                        }
                        
                        if(!empty($myBookingArrs['dtCutOff']) && $myBookingArrs['dtCutOff']!='0000-00-00 00:00:00')
                        {
                            $bookingApiArr[$ctr]['dtCutOff']= date('d-m-Y',strtotime($myBookingArrs['dtCutOff']));
                        }

                        if(!empty($myBookingArrs['dtAvailable']) && $myBookingArrs['dtAvailable']!='0000-00-00 00:00:00')
                        {
                            $bookingApiArr[$ctr]['dtAvailable']= date('d-m-Y',strtotime($myBookingArrs['dtCutOff']));
                        }
                }

                if($originCity=="" || empty($originCity))
                {
                    if(strlen($myBookingArrs['szShipperCity'])>12)
                    {
                        $originCity=mb_substr_replace($myBookingArrs['szShipperCity'],'...',12,strlen($myBookingArrs['szOriginCity']),$format);
                    }
                    else
                    {
                        $originCity=$myBookingArrs['szShipperCity'];
                    } 
                }

                if($desCity=="" || empty($desCity))
                {
                        if(strlen($myBookingArrs['szDestinationCity'])>12)
                        {
                                $desCity= mb_substr_replace($myBookingArrs['szDestinationCity'],'...',12,strlen($myBookingArrs['szDestinationCity']),$format);
                        }
                        else
                        {
                                $desCity=$myBookingArrs['szDestinationCity'];
                        }
                        //$desCity=$myBookingArrs['szDestinationCity'];
                }
                $bookingApiArr[$ctr]['szOriginCity'] = cApi::encodeString($originCity);
                $bookingApiArr[$ctr]['szDestinationCity'] = cApi::encodeString($desCity);
                if($myBookingArrs['iCourierBooking']==1)
                {
                    $bookingApiArr[$ctr]['szOriginCity'] = cApi::encodeString($myBookingArrs['szOriginCity']);
                    $bookingApiArr[$ctr]['szDestinationCity'] = cApi::encodeString($myBookingArrs['szDestinationCity']);
                    
                    if(!empty($myBookingArrs['dtCutOff']) && $myBookingArrs['dtCutOff']!='0000-00-00 00:00:00')
                    {
                        $bookingApiArr[$ctr]['dtExpectedPickup']= date('d-m-Y',strtotime($myBookingArrs['dtCutOff']));
                    }
                    
                    if(!empty($myBookingArrs['dtAvailable']) && $myBookingArrs['dtAvailable']!='0000-00-00 00:00:00')
                    {
                        $bookingApiArr[$ctr]['dtExpectedDelivery']= date('d-m-Y',strtotime($myBookingArrs['dtCutOff']));
                    }
                }
                else
                {
                    if($myBookingArrs['iQuotesStatus']==__BOOKING_QUOTES_STATUS_WON__){
                        $bookingApiArr[$ctr]['szOriginCity'] = cApi::encodeString($myBookingArrs['szShipperCity']);
                        $bookingApiArr[$ctr]['szDestinationCity'] = cApi::encodeString($myBookingArrs['szConsigneeCity']);
                    }
                }

                if($myBookingArrs['iQuotesStatus']==__BOOKING_QUOTES_STATUS_WON__)
                {
                    $szServiceName = cApi::encodeString($myBookingArrs['szServiceDescription']);
                    $bookingApiArr[$ctr]['dtExpectedDelivery']='';
                    $bookingApiArr[$ctr]['dtExpectedPickup']='';
                    $bookingApiArr[$ctr]['dtCutOff']='';
                    $bookingApiArr[$ctr]['dtAvailable']='';
                }
                else
                {
                    $bookingDataArr=array();
                    $bookingDataArr = $kBooking->getExtendedBookingDetails($myBookingArrs['id']); 
                    $szServiceDescription = getServiceDescription($bookingDataArr);  
                }
                $bookingApiArr[$ctr]['szServiceDescription']= cApi::encodeString($szServiceDescription);
                $bookingApiArr[$ctr]['szServiceName']= cApi::encodeString($szServiceName);
                if(($myBookingArrs['idBookingStatus']=='3' || $myBookingArrs['idBookingStatus']=='4') && $myBookingArrs['dtBookingConfirmed']!='' && $myBookingArrs['dtBookingConfirmed']!='0000-00-00 00:00:00'){
                    $bookingApiArr[$ctr]['dtBookingConfirmed']=date('d-m-Y',strtotime($myBookingArrs['dtBookingConfirmed']));
                }
                else if($myBookingArrs['idBookingStatus']=='1' || $myBookingArrs['idBookingStatus']=='2'){
                    if(!empty($myBookingArrs['dtUpdatedOn']) && $myBookingArrs['dtUpdatedOn']!='0000-00-00 00:00:00')
                    {
                        $bookingApiArr[$ctr]['dtUpdatedOn']= date('d-m-Y',strtotime($myBookingArrs['dtUpdatedOn']));
                    }
                }
                $totalAmountPaid=0;   
                if($myBookingArrs['idBookingStatus']=='3' || $myBookingArrs['idBookingStatus']=='4'){
                    $bookingApiArr[$ctr]['szBookingRef']=$myBookingArrs['szBookingRef'];
                     $totalAmountPaid=0;   
                                                   
                    $totalAmountPaid= $myBookingArrs['fTotalPriceCustomerCurrency']+$myBookingArrs['fTotalInsuranceCostForBookingCustomerCurrency'];
                    $bookingApiArr[$ctr]['fCustomerAmountPaid']= $totalAmountPaid;
                    $bookingApiArr[$ctr]['szBooingCurrency']= $myBookingArrs['szCurrency'];
                                                    
                }
                else
                {
                    $bookingApiArr[$ctr]['fCustomerAmountPaid']= "N/A";
                    $bookingApiArr[$ctr]['szBooingCurrency']= "N/A";
                }
                
                if((int)$myBookingArrs['idForwarder']>0)
                {
                    $kForwarder = new cForwarder();
                    $kForwarder->load($myBookingArrs['idForwarder']);
                    $bookingApiArr[$ctr]['szLogoFileName'] = $kForwarder->szLogo ;
                    $bookingApiArr[$ctr]['szDisplayName'] = $kForwarder->szDisplayName;
                }
                
                $szCargoCommodity = $myBookingArrs['szCargoDescription'];
								
            $cargo_volume = format_volume($myBookingArrs['fCargoVolume'],$iLanguage);
            $cargo_weight=number_format_custom($myBookingArrs['fCargoWeight'],$iLanguage);
            
            $kConfig = new cConfig();
            $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$myBookingArrs['iBookingLanguage']);
            
            
            if($myBookingArrs['idBookingStatus']=='3')
            {
                $bookingApiArr[$ctr]['szBookingStatus']=$configLangArr[1]['szAticve_txt'];
            }
            else if($myBookingArrs['idBookingStatus']=='4')
            {
                $bookingApiArr[$ctr]['szBookingStatus']=$configLangArr[1]['szArchive_txt'];
            }
            else if($myBookingArrs['idBookingStatus']=='2')
            {
                $bookingApiArr[$ctr]['szBookingStatus']=$configLangArr[1]['szHold_txt'];
            }
            else if($myBookingArrs['idBookingStatus']=='1')
            {
                $bookingApiArr[$ctr]['szBookingStatus']=$configLangArr[1]['szDraft_txt'];
            }
            if($myBookingArrs['iCourierBooking']==1)
            {    
                $textPackage=t($t_base.'fields/package');
                if($myBookingArrs['idCourierPackingType']>0)
                {   
                    
                    $kCourierServices = new cCourierServices();
                    $packingTypeArr=$kCourierServices->selectProviderPackingList($myBookingArrs['idCourierPackingType'],1);
                                        
                    if($myBookingArrs['iTotalQuantity']>0)
                    {
                        $textPackage=$packingTypeArr[0]['szPackingSingle'];
                        if((int)$myBookingArrs['iTotalQuantity']>1)
                            $textPackage=$packingTypeArr[0]['szPacking'];
                    }
                }               
                $bookingApiArr[$ctr]['szCargoText']= number_format((int)$myBookingArrs['iTotalQuantity'])." ".$textPackage.", ".$cargo_volume." cbm, ".$cargo_weight." kg, ".cApi::encodeString($szCargoCommodity);
            }
            else
            {
                $bookingApiArr[$ctr]['szCargoText']= $cargo_volume." cbm, ".$cargo_weight." kg, ".cApi::encodeString($szCargoCommodity);
            }
            if($myBookingArrs['idBookingStatus']=='3' || $myBookingArrs['idBookingStatus']=='4'){ 
                if($myBookingArrs['idTransportMode']==4){
                    if($myBookingArrs['iSendTrackingUpdates'] == 1)
                    {
                        $bookingApiArr[$ctr]['szSendTrackingUpdates']='Yes';
                    }
                    else
                    {
                        $bookingApiArr[$ctr]['szSendTrackingUpdates']='No';
                    }
                }
            
            }
            
               ++$ctr;
           }
           return $bookingApiArr;
        }
        else
        {
            $bookingApiArr['szErrorMsg']=true;
           if($flag=='active') {
                $bookingApiArr['szKey']='iActiveBooking';
                $bookingApiArr['szValue']='NOT_FOUND';			
         } elseif($flag=='hold') { 
            $bookingApiArr['szKey']='iHoldBooking';
            $bookingApiArr['szValue']='NOT_FOUND';
         }elseif($flag=='draft')  {
            $bookingApiArr['szKey']='iDraftBooking';
            $bookingApiArr['szValue']='NOT_FOUND';
        } elseif($flag=='archive') { 
            $bookingApiArr['szKey']='iArchivedBooking';
            $bookingApiArr['szValue']='NOT_FOUND';
        }
        else
        {
            $bookingApiArr['szKey']='iAllBooking';
            $bookingApiArr['szValue']='NOT_FOUND';
        }
            return $bookingApiArr;
        }
    }
    
    function markTransportecaBookingReceived($bookingArr,$comment=false)
    {  
        if(!empty($bookingArr))
        {
            $kBooking = new cBooking();
            $kAdmin = new cAdmin();
            $isManualCourierBooking = $bookingArr['isManualCourierBooking'];
            $idBooking = $bookingArr['id']; 
            
            $dtCurrentTime = $kBooking->getRealNow();  
            
            $bookingTransactionAry = array();
            $bookingTransactionAry = $kAdmin->showBookingConfirmedTransaction($idBooking);
             
            if(!empty($bookingTransactionAry))
            {
                $idForwarderTransaction = $bookingTransactionAry[0]['id']; 
                
                if($idForwarderTransaction>0)
                {
                    $iMarkupPaymentReceived = false;
                    if($bookingArr['iForwarderGPType']==__FORWARDER_PROFIT_TYPE_MARK_UP__)
                    { 
                        $iMarkupPaymentReceived = true; 
                        $fTotalPriceUSD = $bookingArr['fTotalPriceCustomerCurrency'] * $bookingArr['fExchangeRate'];
                        $fTotalVATUSD = $bookingArr['fTotalVat'] * $bookingArr['fExchangeRate'];
                        $fReferralAmountUSD = $bookingArr['fReferalAmountUSD'];

                        //$fTotalInvoiceValueUSD = $fTotalPriceUSD + $fTotalVATUSD ;
                        $fTotalInvoiceValueUSD = $fTotalPriceUSD ;

                        //we always add value in USD to display on Management=>Dashboard=> Actual Revenue Graph
                        $referaalAmountArr = array();
                        $referaalAmountArr['idBooking'] = $bookingArr['idBooking'];
                        $referaalAmountArr['idForwarder'] = $bookingArr['idForwarder']; 
                        $referaalAmountArr['fTotalAmount'] = $fTotalInvoiceValueUSD;
                        $referaalAmountArr['idCurrency'] = 1;
                        $referaalAmountArr['fExchangeRate'] = 1;
                        $referaalAmountArr['szCurrency'] = 'USD'; 
                        $referaalAmountArr['szBooking'] = 'Transporteca mark-up: '.$bookingArr['szBookingRef'];  
                        $referaalAmountArr['fUSDReferralFee'] = $fReferralAmountUSD;
                        $referaalAmountArr['szBookingNotes'] = "We always keep this value in USD to display on Actual Revenue graph. ";

                        $szLogingLogStr .= PHP_EOL." Registered mark-up price details in tblforwardertransactions ".PHP_EOL;

                        $kBilling = new cBilling();
                        $kBilling->insertTransportecaMarkupFee($referaalAmountArr);
                    }

                    $status = $kAdmin->changePaymentRecievedStatus($idForwarderTransaction,1,$dtCurrentTime,false,$iMarkupPaymentReceived); 
                    if($status)
                    {	
                        $bQuickQuote = false;
                        if($bookingArr['iQuickQuote']==__QUICK_QUOTE_MAKE_BOOKING__)
                        {
                            $bQuickQuote = true;
                            
                            if($bookingArr['isManualCourierBooking']==1)
                            {
                                $kCourierServices=new cCourierServices();
                                $courierBookingArr=$kCourierServices->getCourierBookingData($bookingArr['id']);
                                if($courierBookingArr[0]['iCourierAgreementIncluded']==1)
                                {
                                    $idQuotePriceDetails = $bookingArr['idQuotePricingDetails'];

                                    $data = array();
                                    $data['idBookingQuotePricing'] = $idQuotePriceDetails ;
                                    $quotePricingAry = array();
                                    $quotePricingAry = $kBooking->getAllBookingQuotesPricingDetails($data);

                                    if(!empty($quotePricingAry))
                                    {
                                        $idBookingQuote = $quotePricingAry[1]['idQuote'];
                                        $dtStatusUpdatedOnTime=$kBooking->getRealNow();
                                        $res_ary = array();
                                        $res_ary['szForwarderQuoteStatus'] = 'FQS2' ; 
                                        $res_ary['szForwarderQuoteTask'] = '' ; 
                                        $res_ary['dtStatusUpdatedOn'] = $dtStatusUpdatedOnTime;
                                        $res_ary['iWonQuote'] = 1;
                                        $res_ary['iClosed'] = 1;
                                        $res_ary['iClosedForForwarder'] = 0;

                                        if(!empty($res_ary))
                                        {
                                            $update_query='';
                                            foreach($res_ary as $key=>$value)
                                            {           
                                                $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                                            }
                                        } 
                                        $update_query = rtrim($update_query,",");    
                                        $kBooking->updateBookingQuotesDetails($update_query,$idBookingQuote); 

                                    }
                                }
                            }
                        } 

                        $kForwarder = new cForwarder(); 
                        $idForwarder = $kAdmin->findForwarderId($idForwarderTransaction);
                        $kAdmin->updateCustomerPaidAmount($idBooking,false,$iMarkupPaymentReceived,$bQuickQuote);  
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        } 
    }  
    
    function getTotalBokingValueSnapshotByForwarder($idForwarder)
    {
        if($idForwarder>0)
        {
            $invoiceBillConfirmed = array();
            $invoiceBillConfirmed = $this->getInvoiceBillConfirmed($idForwarder);  
            
            $currentAvailableAmountAry = array();
            $currentAvailableAmountAry = $this->getBookingTotalValue($idForwarder,$invoiceBillConfirmed);
            return $currentAvailableAmountAry; 
        }
    }
    
    function getBookingTotalValue($idForwarder,$invoiceBillConfirmed)
    {
        if($idForwarder)
        { 
            $kAdmin = new cAdmin();
            /*
            * Amount for this should match the total value at Forwarder /billings/ screen 'Total Value'
            */
            if(!empty($invoiceBillConfirmed))
            {
                foreach($invoiceBillConfirmed as $searchResults)
                {
                    if($searchResults["dtPaymentConfirmed"]!='' && $searchResults["dtPaymentConfirmed"]!='0000-00-00 00:00:00')
                    {	
                        /*
                        * iDebitCredit = 1 means, New booking received ot Transporteca
                        * iDebitCredit = 2 means, Automatic Transfer
                        * iDebitCredit = 3 means, Transporteca referral fee
                        * iDebitCredit = 4 means, Starting Balance i.e balance when a Forwarder joins to Transporteca and value for this is always szForwarderCurrency 0.00
                        * iDebitCredit = 5 means, Transporteca upload service
                        * iDebitCredit = 6 means, Cancelled Bookings
                        * iDebitCredit = 7 means,  
                        * iDebitCredit = 8 means, Transporteca Courier Label Fee
                        * iDebitCredit = 9 means, 
                        * iDebitCredit = 10 means, Transporteca mark-up fee
                        * iDebitCredit = 11 means, 
                        * iDebitCredit = 12 means,Handling fee Invoice
                        * iDebitCredit = 13 means, Balance Adjustment i.e any amount that was added or substracted manually to make Financial screen's amount correct
                        */
                        if($searchResults['iDebitCredit']==1 || $searchResults['iDebitCredit']==13)
                        {
                            $idCurency = $searchResults['szForwarderCurrency'] ;  
                            $fTotalPriceForwarderCurrency = $searchResults['fTotalPriceForwarderCurrency']; 
                            $currentAvailableAmountAry[$idCurency]['fTotalBalance'] += round((float)$fTotalPriceForwarderCurrency,2);
                        }
                        else if(($searchResults['iDebitCredit']==3 || $searchResults['iDebitCredit']==8 || $searchResults['iDebitCredit']==12) && $searchResults['iStatus']==2)
                        {
                            $fTotalCourierLabelPaid = 0; 
                            $idCurency = $searchResults['szCurrency'];
                            $fTotalPriceForwarderCurrency = $searchResults['fTotalPriceForwarderCurrency'];
                            //$currentAvailableAmountAry[$idCurency]['fTotalBalance'] = $currentAvailableAmountAry[$idCurency]['fTotalBalance'] - round($fTotalPriceForwarderCurrency,2);  
                        }
                        else if(($searchResults['iDebitCredit']==6 || $searchResults['iDebitCredit']==7)) // Cancelled booking
                        {
                            $idCurency = $searchResults['szForwarderCurrency'];
                            $fTotalPriceForwarderCurrency = $searchResults['fTotalPriceForwarderCurrency']; 
                            $currentAvailableAmountAry[$idCurency]['fTotalBalance'] = $currentAvailableAmountAry[$idCurency]['fTotalBalance'] - $fTotalPriceForwarderCurrency;  
                        }
                        else if($searchResults['iDebitCredit']=='5')
                        {
                            $idCurency = $searchResults['szForwarderCurrency'] ;	
                            $fTotalPriceForwarderCurrency = $searchResults['fTotalPriceForwarderCurrency'];
                            //$currentAvailableAmountAry[$idCurency]['fTotalBalance'] = $currentAvailableAmountAry[$idCurency]['fTotalBalance'] - $fTotalPriceForwarderCurrency; 
                        }  
                        //echo "<br> Currency: ".$idCurency." Invoice Amt: ".$fTotalPriceForwarderCurrency." Sum: ".$currentAvailableAmountAry[$idCurency]['fTotalBalance']." DC: ".$searchResults['iDebitCredit']." ID: ".$searchResults['id']; 
                    }
                } 
            } 
            return $currentAvailableAmountAry;
        }
    } 
    
    function getBillingScreenHeadingValues($idForwarder)
    {
        if($idForwarder>0)
        {
            /*
            * Calculation Total Historal Booking Value for a Frwarder
            */
            $fTotalForwarderValue = $this->getForwarderBookingValues($idForwarder);
            
            /*
            * Calculating Transfer Date for Forwarder Billing screen.
            */
            $dtNextTransferDate = $this->getNextTransferDate($idForwarder);
            
            /*
            * Calculating Next Amount to be transferrred to Forwarder.
            */
            $fTotalAmountNextTransferred = $this->getNextTranserDetailsByForwarder($idForwarder);
            
            /*
            * Calculating Last transferred Amount to Forwarder.
            */ 
            $szTotalAmountLastTransferred = $this->getLastTranserDetailsByForwarder($idForwarder,true);
             
            if(empty($szTotalAmountLastTransferred))
            {
               $szTotalAmountLastTransferred = "N/A";
            }
            $ret_ary = array();
            $ret_ary['fTotalAmountLastTransferred'] = $szTotalAmountLastTransferred;
            $ret_ary['fTotalAmountNextTransferred'] = $fTotalAmountNextTransferred;
            $ret_ary['dtNextTransferDate'] = $dtNextTransferDate;
            $ret_ary['fTotalForwarderValue'] = $fTotalForwarderValue;
            return $ret_ary;
        }
    }
    
    function getForwarderBookingValues($idForwarder)
    {
        if($idForwarder>0)
        {
            /*
            * Get Historical sum of Total Booking Value.
            */
            $historicalSnapShotAry = array();
            $historicalSnapShotAry = $this->getAllForwardSnapshot($idForwarder);
                
            /*
            * Geting Total value since Snashot was last updated
            */
            $currentSnapShotAry = array();
            $currentSnapShotAry = $this->getTotalValueSinceLastSnapShot($idForwarder);
             
            $forwarderTotalValueAry = array();
            if(!empty($historicalSnapShotAry) && !empty($currentSnapShotAry))
            {
                $tempAry = array();
                $tempAry = $historicalSnapShotAry + $currentSnapShotAry;
                if(!empty($tempAry)) 
                {
                    foreach($tempAry as $szCurrencyName=>$tempArys)
                    {
                        $fTotalBalance = $tempArys['fTotalBalance'];
                        $forwarderTotalValueAry[$szCurrencyName]['fTotalBalance'] = $historicalSnapShotAry[$szCurrencyName]['fTotalBalance'] + $currentSnapShotAry[$szCurrencyName]['fTotalBalance'];
                    }
                }
            }
            else if(!empty($historicalSnapShotAry))
            {
                $forwarderTotalValueAry = $historicalSnapShotAry;
            } 
            else
            {
                $forwarderTotalValueAry = $currentSnapShotAry;
            } 
            $fTotalForwarderValue = $this->getFormatedTotalValue($forwarderTotalValueAry);
            return $fTotalForwarderValue;
        }
    }
    
    function getNextTransferDate($idForwarder)
    {
        if((int)$idForwarder>0)
        {
            $kAdmin = new cAdmin();
            $billingTransferDetails = array(); 
            $billingTransferDetails = $this->getNextTransferScheduledByForwarder($idForwarder);
            if(!empty($billingTransferDetails))
            {
                $dtFinalNextTransferDateTime = strtotime($billingTransferDetails['dtPaymentScheduled']);
                $dtFinalNextTransfer = (int)date('d',$dtFinalNextTransferDateTime).". ".date('F',$dtFinalNextTransferDateTime);
                return $dtFinalNextTransfer; 
            }
            else
            {
                /*
                * The "Transfer date" is minimum(end of last months date + iCreditDays ; end of current months date + iCreditDays). So it will simply show the date we next time will make a bank transfer to the forwarder. If, for example we have 15 days credit with a forwarder, so iCreditDays is 15, that means that the end of a month, we will add a line to the balance table which says  “Transfer – scheduled 15. February” , where “15. February” is the transaction date + iCreditDays, and where the amount is the total balance. We will add this with a cron job automatically at midnight of the last day of every month. On the 15th February, we will then make a bank transfer to the forwarder. For this “Transfer date”, we want to show the next date that we make a transfer. In the above example, if the forwarder opens the billing page on e.g. 5th February, the date will therefor be shown as 15th February. If the forwarder opens on on e.g. 20th February, the date will therefore be shown as 15th March.
                */
                $kForwarder = new cForwarder();
                $kForwarder->load($idForwarder); 
                $iCreditDays = $kForwarder->iCreditDays;
 
                $dtLastDateOfCurrentMonth = date("Y-m-t"); 
                if($iCreditDays>0)
                {  
                    $dtLastDateOfCurrentMonthCreditDaysTime = strtotime("+".$iCreditDays." days", strtotime($dtLastDateOfCurrentMonth));
                    $dtLastDateOfCurrentMonthCreditDays = date('Y-m-d',$dtLastDateOfCurrentMonthCreditDaysTime);
                }  
                $dtFinalNextTransfer = (int)date('d',$dtLastDateOfCurrentMonthCreditDaysTime).". ".date('F',$dtLastDateOfCurrentMonthCreditDaysTime);
                return $dtFinalNextTransfer; 
            } 
        }
    }
    
    function getLastTranserDetailsByForwarder($idForwarder,$bLastTransferAmount=false)
    {
        if($idForwarder>0)
        {  
            $kAdmin = new cAdmin();
            $query="
                SELECT 
                    fTotalPriceForwarderCurrency AS totalCurrencyValue,
                    szForwarderCurrency,
                    idForwarder,
                    dtInvoiceOn,
                    szInvoice,
                    szCurrency
                FROM
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                WHERE
                    idForwarder = '".mysql_escape_custom($idForwarder)."' 
                AND
                   iDebitCredit='2'
                AND
                    DATE(dtPaymentScheduled) < '".date('Y-m-d')."'
                ORDER BY
                    dtPaymentScheduled DESC
                LIMIT
                    0,1
            ";
            //echo "<br><br>".$query;
            if($result = $this->exeSQL( $query ))
            {
                if($this->getRowCnt() > 0)
                {
                    $transferPaymentVar=array();
                    if($bLastTransferAmount)
                    { 
                        $row = $this->getAssoc($result); 
                        
                        $CreateLabelBatch = $row['szInvoice'] + 1;
                        $szInvoice_formatted = $kAdmin->getFormattedString($CreateLabelBatch,6);
                        $customer_code = $szInvoice_formatted ; 
                        $totalCourierLabelArr=$kAdmin->totalAmountCourierLabelFee($customer_code); 
                        $fTotalCourierLabelPaid = 0;
                        if(!empty($totalCourierLabelArr))
                        {
                            $fTotalCourierLabelPaid = $totalCourierLabelArr[0]['totalCurrencyValue'];  
                        }
            
                        return $row['szCurrency']." ".number_format((float)($row['totalCurrencyValue']-$fTotalCourierLabelPaid));
                    }
                    else
                    { 
                        $ctr = 0;
                        while($row=$this->getAssoc($result))
                        {	
                            $transferPaymentVar[$ctr] = $row;
                        }
                        return $transferPaymentVar;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return array();
            }
        }
    }
    
    function getNextTransferScheduledByForwarder($idForwarder)
    {
        if($idForwarder>0)
        {  
            $kAdmin = new cAdmin();
            $query="
                SELECT 
                    fTotalPriceForwarderCurrency,
                    szForwarderCurrency,
                    idForwarder,
                    dtInvoiceOn,
                    szInvoice,
                    szCurrency,
                    dtPaymentScheduled
                FROM
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                WHERE
                    idForwarder = '".mysql_escape_custom($idForwarder)."' 
                AND
                   iDebitCredit='2'
                AND
                    DATE(dtPaymentScheduled) >= '".date('Y-m-d')."'
                ORDER BY
                    dtPaymentScheduled ASC
                LIMIT
                    0,1
            ";
            //echo "<br><br>".$query;
            if($result = $this->exeSQL( $query ))
            {
                if($this->getRowCnt() > 0)
                { 
                    $row = $this->getAssoc($result);  
                    return $row; 
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return array();
            }
        }
    } 
    function getNextTranserDetailsByForwarder($idForwarder)
    {
        if($idForwarder>0)
        {   
            $kAdmin = new cAdmin(); 
            $billingTransferDetails = array(); 
            $billingTransferDetails = $this->getNextTransferScheduledByForwarder($idForwarder); 
            if(!empty($billingTransferDetails))
            { 
                $this->iDisplayAstrix= false;
                return $billingTransferDetails['szCurrency']." ".number_format($billingTransferDetails['fTotalPriceForwarderCurrency']); 
            }
            else
            {
                $billingDetailsAry = array();
                $billingDetailsAry = $kAdmin->showForwarderPendingPayment(true,$idForwarder,true); 

                if(!empty($billingDetailsAry))
                {
                    $szNextTransferAmount = $this->getFormatedTotalValue($billingDetailsAry);
                }
                else
                {
                    $kForwarder = new cForwarder();
                    $kWHSSearch = new cWHSSearch();
                    $kForwarder->load($idForwarder);
                    $idCurrency = $kForwarder->szCurrency;
                    if($idCurrency==1)
                    {
                        $szNextTransferAmount = "USD 0";
                    }
                    else
                    {
                        $currencyAry = array();
                        $currencyAry = $kWHSSearch->getCurrencyDetails($idCurrency); 
                        $szForwarderCurrency = $currencyAry['szCurrency']; 
                        $szNextTransferAmount = $szForwarderCurrency." 0";
                    } 
                }
                $this->iDisplayAstrix= true;
                return $szNextTransferAmount;
            } 
        }
    }
    
    function getFormatedTotalValue($currenctBalaceArr)
    {
        if(!empty($currenctBalaceArr))
        {
            $currentBalArr = array();
            foreach($currenctBalaceArr as $key=>$currenctBalaceArrs)
            { 
                $values = $currenctBalaceArrs['fTotalBalance'];
                if($values>=0)
                {
                    $currentBalArr[]=$key." ".number_format($values);
                }
                else
                {
                    $values=str_replace("-","",$values);
                    $currentBalArr[]="(".$key." ".number_format($values).")";
                } 
            }
            if(!empty($currentBalArr))
            {
                return implode('<br>',$currentBalArr);
            }
        }
    }
    function getTotalValueSinceLastSnapShot($idForwarder)
    {
        if($idForwarder>0)
        {
            $kBooking = new cBooking();  
            
            $dtSnapshotLastUpdated = $this->getSnapshotLastUpdatedDate($idForwarder);
            
            $dateAry = array();
            if(!empty($dtSnapshotLastUpdated))
            {
                $dtCurrentDateTime = $kBooking->getRealNow();
                $dateAry[0] = $dtSnapshotLastUpdated;
                $dateAry[1] = $dtCurrentDateTime;
                $dateAry[2] = 'DATE_TIME';
            }  
            $invoiceBillConfirmed = array();
            $invoiceBillConfirmed = $this->getInvoiceBillConfirmed($idForwarder,$dateAry);  
            
            $currentAvailableAmountAry = array();
            $currentAvailableAmountAry = $this->getBookingTotalValue($idForwarder,$invoiceBillConfirmed);
             
            return $currentAvailableAmountAry; 
        }
    }
    function addForwarderBookingSnapshot($data)
    {
        if(!empty($data))
        {
            $idForwarderSnapshot = $this->isBookingSnapshotExists($data['idForwarder'],$data['szCurrencyName']);
            if($idForwarderSnapshot>0)
            {
                $query="	
                    UPDATE
                        ".__DBC_SCHEMATA_FORWARDER_TOTAL_BOOKING_SNAPSHOT__."
                    SET 
                        fForwarderTotalVolume = '".(float)$data['fForwarderTotalVolume']."',
                        dtLastUpdated = now()
                    WHERE
                        id = '".(int)$idForwarderSnapshot."'
                "; 
            }
            else
            {
                $query="
                    INSERT INTO
                        ".__DBC_SCHEMATA_FORWARDER_TOTAL_BOOKING_SNAPSHOT__."
                    (
                        idForwarder,
                        szCurrencyName, 
                        fForwarderTotalVolume, 
                        dtLastUpdated
                    )
                    VALUES
                    ( 
                        '".(int)mysql_escape_custom($data['idForwarder'])."',
                        '".mysql_escape_custom($data['szCurrencyName'])."',
                        '".(float)mysql_escape_custom($data['fForwarderTotalVolume'])."',   
                        NOW()
                    )	
                ";
            }
            //echo $query ;
            if($result=$this->exeSQL($query))
            { 
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    
    function isBookingSnapshotExists($idForwarder,$szCurrencyName)
    {
        if($idForwarder>0 && !empty($szCurrencyName))
        {
            $query="
                SELECT
                    id 
                FROM
                    ".__DBC_SCHEMATA_FORWARDER_TOTAL_BOOKING_SNAPSHOT__."
                WHERE 
                    szCurrencyName = '".mysql_escape_custom($szCurrencyName)."'
                AND
                    idForwarder = '".(int)$idForwarder."'     
            ";   
            //echo $query."<br /><br />";
            if(($result = $this->exeSQL($query)))
            {
                if($this->getRowCnt()>0)
                {  
                    $row = $this->getAssoc($result); 
                    return $row['id']; 
                }
                else
                {
                    return false;
                }
            } 
            else
            {
                return false;
            }
        }
    }
    
    function getAllForwardSnapshot($idForwarder)
    {
        if($idForwarder>0)
        {
            $query="
                SELECT
                    id,
                    idForwarder,
                    szCurrencyName, 
                    fForwarderTotalVolume, 
                    dtLastUpdated
                FROM
                    ".__DBC_SCHEMATA_FORWARDER_TOTAL_BOOKING_SNAPSHOT__."
                WHERE  
                    idForwarder = '".(int)$idForwarder."'     
            ";   
            //echo $query."<br /><br />";
            if(($result = $this->exeSQL($query)))
            {
                if($this->getRowCnt()>0)
                {
                    $ret_ary = array(); 
                    while($row = $this->getAssoc($result))
                    {
                        $ret_ary[$row['szCurrencyName']]['fTotalBalance'] = $row['fForwarderTotalVolume'];
                    }
                    return $ret_ary; 
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
    
    function getSnapshotLastUpdatedDate($idForwarder)
    {
        if($idForwarder>0)
        {
            $query="
                SELECT 
                    dtLastUpdated
                FROM
                    ".__DBC_SCHEMATA_FORWARDER_TOTAL_BOOKING_SNAPSHOT__."
                WHERE  
                    idForwarder = '".(int)$idForwarder."'  
                ORDER BY
                  dtLastUpdated DESC
                LIMIT
                    0,1
            ";   
           // echo $query."<br /><br />";
            if(($result = $this->exeSQL($query)))
            {
                if($this->getRowCnt()>0)
                { 
                    $row = $this->getAssoc($result); 
                    return $row['dtLastUpdated']; 
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
    
    function getCustomerDetailsByBooking($idBooking)
    {
        if($idBooking>0)
        { 
            $query="
                SELECT
                    id,
                    iPrivateShipping,
                    szFirstName,
                    szLastName,
                    szCustomerCompanyName,
                    szBookingRef,
                    iInsuranceIncluded,
                    fTotalPriceCustomerCurrency,
                    fTotalVat,
                    szCustomerCurrency,
                    fTotalInsuranceCostForBookingCustomerCurrency,
                    dtBookingConfirmed,
                    iInsuranceCancelledIndividually,
                    dtInsuranceStatusUpdated,
                    szInsuranceCreditNoteInvoice,
                    szBookingRef,
                    iInsuranceStatus,
                    fCustomerExchangeRate
                FROM
                    ".__DBC_SCHEMATA_BOOKING__."
                WHERE
                    id = '".(int)$idBooking."'	 
            ";
            //echo $query;
            if($result = $this->exeSQL($query))
            {
                $bookingAry=array();
                if($this->iNumRows>0)
                {
                    $bookingAry = $this->getAssoc($result); 
                    return $bookingAry;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    
    
    function getBookingPackageNotCollcetedAsScheduled()
    {
        $dtReminderDateTime=date('Y-m-d H:i:s');
        $szCustomerTimeZone = 'Europe/Copenhagen' ; 
        $date = new DateTime($dtReminderDateTime); 
        $date->setTimezone(new DateTimeZone($szCustomerTimeZone));    

        $dtEmailReminderDate = $date->format('Y-m-d');
        $dtEmailReminderDateFormat = $date->format('d/m/Y');
        $szEmailReminderTime = $date->format('H:i');
        $szEmailReminderHour = $date->format('H');
        $dtPreviousDateTime=date('Y-m-d',  strtotime("-1 DAY") );
        
        $kCrm = new cCRM();
        //if($szEmailReminderHour=='8')
        //{
            $query="
                SELECT
                    b.id AS idBooking,
                    cb.dtCollection,
                    cb.dtCollectionEndTime,
                    b.szTrackingNumber,
                    b.idFile,
                    f.idFileOwner,
                    b.szBookingRef
                FROM
                    ".__DBC_SCHEMATA_FILE_LOGS__." AS f
                INNER JOIN
                    ".__DBC_SCHEMATA_BOOKING__." AS b
                ON
                    b.id = f.idBooking
                INNER JOIN
                    ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__." AS cb
                ON
                    b.id = cb.idBooking
                WHERE
                   b.idBookingStatus IN (3,4) 
                AND
                    f.iLabelStatus>='2'
                AND
                    cb.dtCollection<>'0000-00-00'
                AND
                    DATE(cb.dtCollection) ='".  mysql_escape_custom($dtPreviousDateTime)."'
                AND
                    b.iTrackingEmailSend='0'
                AND
                    b.iSendTrackingUpdates='1'    

            ";
            //echo $query."<br/>";
          
            if(($result = $this->exeSQL($query)))
            {
                if($this->getRowCnt()>0)
                { 
                    $ctr=0;
                    while($row = $this->getAssoc($result))
                    {
                        echo $row['szTrackingNumber']."<br />";
                        if(!$this->checkEamilAlreadySendForTrackingNumber($row['szTrackingNumber'],$row['idBooking'],$row['dtCollection']))
                        {
                            $kBooking = new cBooking();
                            $dtCurrentDateTime = $kBooking->getRealNow(); 
                            $dtReminderDate = getDateCustom($dtCurrentDateTime);
                            
                            /*
                                if(date('I')==1)
                                {
                                    $dtReminderDate = date('Y-m-d H:i:s',strtotime("+2 HOUR",strtotime($dtCurrentDateTime)));  
                                }
                                else
                                {
                                    $dtReminderDate = date('Y-m-d H:i:s',strtotime("+1 HOUR",strtotime($dtCurrentDateTime)));  
                                }
                            * 
                            */
                            
                            /*$reminderNotesAry = array();
                            $reminderNotesAry = format_reminder_date_time();
                            $dtTaskDate = $reminderNotesAry['dtReminderDate'];
                            $dtTaskTime = $reminderNotesAry['szReminderTime'];
                            $dtTaskDateArr=explode("/", $dtTaskDate);
                            $dtTaskDate=$dtTaskDateArr[2]."-".$dtTaskDateArr[1]."-".$dtTaskDateArr[0];
                            
                            $dtReminderDate = $dtTaskDate." ".$dtTaskTime.":00"; 
                            
                            if(empty($_SESSION['szCustomerTimeZoneGlobal']))
                            {
                                $_SESSION['szCustomerTimeZoneGlobal'] = 'Europe/Copenhagen' ;
                            }
                            
                            $szServerTimeZone = 'Europe/London';
                            
                            $date = new DateTime($dtReminderDate,new DateTimeZone($_SESSION['szCustomerTimeZoneGlobal']));   
                            $dtReminderDate = $date->format('Y-m-d H:i:s');

                            $date->setTimezone(new DateTimeZone($szServerTimeZone));  
                            $dtReminderDate = $date->format('Y-m-d H:i:s');  
                           
                            
                            if(date('I')==1)
                            {  
                                //Our server's My sql time is 1 hours behind so that's why added 2 hours to sent date 
                                $dtReminderDate = date('Y-m-d H:i:s',strtotime("-1 HOUR",strtotime($dtReminderDate)));  
                            }*/
                            
                            
                            $addTransportecaTask = array();
                            $addTransportecaTask['szReminderNotes']=__REMINDER_TEXT_SLOVE_TASK__;
                            $addTransportecaTask['idTaskOwner']=$row['idFileOwner'];
                            $addTransportecaTask['idBookingFile']=$row['idFile'];
                            $addTransportecaTask['dtTaskDate']=$dtReminderDate;
                            $addTransportecaTask['dtTaskTime']=date('H:i:s',  strtotime($dtReminderDate)); 
                            $addTransportecaTask['dtRemindDate'] = $dtReminderDate;
                            $addTransportecaTask['iOverviewTask'] = 1; 

                            
                            $kBooking->addAddReminderNotes($addTransportecaTask);
                            $this->idBookingFileReminderNotes = $kBooking->idBookingFileReminderNotes;

                            $idBookingFile = $row['idFile'];                            

                            $fileLogsAry = array();   
                            $fileLogsAry['dtSolveTaskCreated'] = $dtCurrentDateTime;
                            $fileLogsAry['iAddSolveTask'] = 1;
                            $fileLogsAry['idFileOwner'] = 0;  
                            $fileLogsAry['idReminderTask'] = $this->idBookingFileReminderNotes;                            
                            $fileLogsAry['iClosed'] = 0; 
                            $fileLogsAry['szTransportecaTask'] = "T220";  
                            $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtCurrentDateTime; 

                            if(!empty($fileLogsAry))
                            {
                                $file_log_query = "";
                                foreach($fileLogsAry as $key=>$value)
                                {
                                    $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                                }
                            } 
                            $file_log_query = rtrim($file_log_query,",");   
                            $kBooking_new = new cBooking();
                            $kBooking_new->updateBookingFileDetails($file_log_query,$idBookingFile,true);
                            
                            $bookingDataArr=$kBooking->getExtendedBookingDetails($row['idBooking']);
                            
                            $kUser = new cUser();
                            $kUser->getUserDetails($bookingDataArr['idUser']);
                            $szCustomerEmail = $kUser->szEmail;
                            $kTracking = new cTracking();
                            $courierProviderName = $kTracking->getCourierProviderNameById($bookingDataArr['idServiceProvider']);
                            
                            $replace_ary=array();
                            $replace_ary['szPickupDate']=$szPickupDate;
                            $replace_ary['szBookingRef']=$bookingDataArr['szBookingRef'];
                            $replace_ary['szShipperCompany']=$bookingDataArr['szShipperCompanyName'];
                            $replace_ary['szFirstName']=$kUser->szFirstName;
                            $replace_ary['szLastName']=$kUser->szLastName;
                            $replace_ary['iBookingLanguage']=$bookingDataArr['iBookingLanguage'];
                            $replace_ary['idBooking']=$bookingDataArr['id'];
                            $replace_ary['iAdminFlag']=1;
                            $replace_ary['szCourierCompany']= $courierProviderName;
                            
                            createEmail(__COURIER_TRACKING_MOT_COLLECTED_AS_SCHEDULED_ALL_BOOKING__, $replace_ary,$szCustomerEmail, '', __STORE_SUPPORT_EMAIL__,'', __STORE_SUPPORT_EMAIL__,__FLAG_FOR_DEFAULT__,'',$data['idBooking']);
                            
                            $this->insertEamilAlreadySendForTrackingNumber($row['szTrackingNumber'],$row['idBooking'],$row['dtCollection']);
                        }
                    }
                }
            }
                  
        //}
    }
    
    function checkEamilAlreadySendForTrackingNumber($szTrackingNumber,$idBooking,$dtCollection)
    {
        $query="
            SELECT
                id
            FROM
                ".__DBC_SCHEMATA_COURIER_TRACKING_NOT_COLLECTED__."
            WHERE
                idBooking='".(int)$idBooking."'
            AND
                szTrackingNumber='".mysql_escape_custom($szTrackingNumber)."'
            AND
                dtCollection='".mysql_escape_custom($dtCollection)."'
            ";
            if(($result = $this->exeSQL($query)))
            {
                if($this->getRowCnt()>0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
    }
    
    function insertEamilAlreadySendForTrackingNumber($szTrackingNumber,$idBooking,$dtCollection)
    {
        $query="
            INSERT INTO
                ".__DBC_SCHEMATA_COURIER_TRACKING_NOT_COLLECTED__."
            (
                idBooking,
                szTrackingNumber,
                dtCollection
            )
                VALUES
            (
                '".(int)$idBooking."',
                '".mysql_escape_custom($szTrackingNumber)."',
                '".mysql_escape_custom($dtCollection)."'    
            )
            ";
            if(($result = $this->exeSQL($query)))
            {
                if($this->getRowCnt()>0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
    }
    
    function updateHandovercityForVogaBookings()
    {
        $query="
            SELECT 
                id, 
                idServiceTerms,
                szOriginCity,
                szDestinationCity,
                szBookingRandomNum
            FROM 
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE 
                `iStandardPricing` =1
            ";
            if(($result = $this->exeSQL($query)))
            {
                if($this->getRowCnt()>0)
                { 
                    $ctr=0;
                    while($row = $this->getAssoc($result))
                    {
                        $idBooking=$row['id'];
                        $idServiceTerms=$row['idServiceTerms'];
                        $szOriginCity=$row['szOriginCity'];
                        $szDestinationCity=$row['szDestinationCity'];
                        if($idServiceTerms==3 &&  $idServiceTerms==6)
                        {
                            $szHandoverCity=$szDestinationCity;
                        }
                        else
                        {
                            $szHandoverCity=$szOriginCity;
                        }
                        
                        $queryUpdate="
                            UPDATE
                                ".__DBC_SCHEMATA_BOOKING__."
                            SET
                                szHandoverCity='".  mysql_escape_custom($szHandoverCity)."'
                            WHERE
                                id ='".(int)$idBooking."'
                        ";
                        echo $row['szBookingRandomNum']."szBookingRandomNum<br />";
                        echo $queryUpdate."<br />";
                        $resultUpdate = $this->exeSQL($queryUpdate);
                        
                    }   
                }
            }
        
    }
    
    
    function updateSelfInvoiceAmountForVerison2()
    {
        $query="
            SELECT 
                id, 
                fReferalAmount,
                fTotalSelfInvoiceAmount,
                dtBookingConfirmed
            FROM 
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE 
                `iFinancialVersion` =2
            ";
             if(($result = $this->exeSQL($query)))
            {
                if($this->getRowCnt()>0)
                { 
                    $ctr=0;
                    while($row = $this->getAssoc($result))
                    {
                        $idBooking=$row['id'];
                        echo $idBooking."idBooking<br/>";
                        $dtBookingConfirmed=$row['dtBookingConfirmed'];
                        $fTotalSelfInvoiceAmount=$row['fTotalSelfInvoiceAmount'];
                        echo $fTotalSelfInvoiceAmount."fTotalSelfInvoiceAmount<br/>";
                        $fReferalAmount=round((float)$row['fReferalAmount'],2);
                        echo $fReferalAmount."fReferalAmount<br/>";
                        $fTotalSelfInvoiceAmountRemaining=0;
                        
                        $fTotalSelfInvoiceAmountRemaining=$fTotalSelfInvoiceAmount;
                        
                        /*$queryUpdate="
                            UPDATE
                                ".__DBC_SCHEMATA_BOOKING__."
                            SET
                                fTotalSelfInvoiceAmount='".(float)$fTotalSelfInvoiceAmountRemaining."'
                            WHERE
                                id ='".(int)$idBooking."'
                        ";
                        echo $queryUpdate."queryUpdateBok<br/>";
                        $resultUpdate = $this->exeSQL($queryUpdate*/
                        
                        if($dtBookingConfirmed!='' && $dtBookingConfirmed!='0000-00-00 00:0000')
                        {
                            $queryUpdate="
                                UPDATE
                                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                                SET
                                    fTotalSelfInvoiceAmount='".(float)$fTotalSelfInvoiceAmountRemaining."',
                                    iFinancialVersion='2',
                                    fTotalPriceForwarderCurrency='".(float)$fTotalSelfInvoiceAmountRemaining."'
                                WHERE
                                    idBooking ='".(int)$idBooking."'
                                AND
                                    iDebitCredit='6'
                            ";
                            echo $queryUpdate."queryUpdateFT<br/>";
                            $resultUpdate = $this->exeSQL($queryUpdate);
                        }
                       
                    }
                }
            }
    }
    
    function confirmBookingAfterPayment($postSearchAry,$szPaymentType)
    {
        if(!empty($postSearchAry))
        { 
            $idBooking = $postSearchAry['id'];
            
            $kForwarder= new cForwarder();
            $kConfig = new cConfig();
            $kForwarder->load($postSearchAry['idForwarder']);
            $iCreditDays= $kForwarder->iCreditDays;
            $szForwarderVatRegistrationNum= $kForwarder->szVatRegistrationNum;
            cPartner::$szDeveloperNotes .= ">. Updating booking flags after successful payment completion.".PHP_EOL;
            if($szPaymentType==__TRANSPORTECA_PAYMENT_TYPE_1__)
            {
                $kBooking = new cBooking();
                $dtResponseTime = $kBooking->getRealNow();
                
                $res_ary = array();
                $res_ary['iPaymentType'] = __TRANSPORTECA_PAYMENT_TYPE_1__;
                $res_ary['dtBookingConfirmed'] = $dtResponseTime;
                $res_ary['iTransferConfirmed'] = 0;
                $res_ary['iBookingStep'] = 14 ; 
                $res_ary['iCreditDays'] = $iCreditDays;
                $res_ary['szForwarderVatRegistrationNum']=$szForwarderVatRegistrationNum;
                $res_ary['fCustomerPaidAmount'] = $postSearchAry['fCustomerPaidAmount'];
                
                
                $update_query = "";
                if(!empty($res_ary))
                {
                    foreach($res_ary as $key=>$value)
                    {
                        $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                }
                $update_query = rtrim($update_query,",");
                if($kBooking->updateDraftBooking($update_query,$idBooking))
                {
                    $bookingDataArr = array();
                    $bookingDataArr = $kBooking->getExtendedBookingDetails($idBooking);
                    
                    cPartner::$szDeveloperNotes .= ">. Successfully updated Stripe data into database.".PHP_EOL;
                    //updating iTransferConfirm into tblforwardertransaction
                    $kAdmin = new cAdmin();
                    $bookingTransactionAry = array();
                    $bookingTransactionAry = $kAdmin->showBookingConfirmedTransaction($idBooking);

                    if(!empty($bookingTransactionAry))
                    {
                        $idForwarderTransaction = $bookingTransactionAry[0]['id']; 
                        cPartner::$szDeveloperNotes .= ">. Updating iTransferConfirm flag into forwarder transaction.".PHP_EOL;
                        $this->changeTransferStatus($idForwarderTransaction,$bookingDataArr);
                    }

                    
                    $iLanguage = $bookingDataArr['iBookingLanguage'];
                    $isManualCourierBooking = $bookingDataArr['isManualCourierBooking'];
                    
                    $szOriginCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idOriginCountry'],false,$iLanguage);
                    $szOriginCountry = $szOriginCountryAry[$bookingDataArr['idOriginCountry']]['szCountryName'] ;
                    
                    $szDestinationCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idDestinationCountry'],false,$iLanguage);
                    $szDestinationCountry = $szDestinationCountryAry[$bookingDataArr['idOriginCountry']]['szCountryName'] ;
                    
                    $replace_ary = array();
                    $idForwarder = $bookingDataArr['idForwarder'] ;
                    $customerServiceEmailAry = $kForwarder->getForwarderCustomerServiceEmail($idForwarder);
                    $replace_ary['idUser'] = $bookingDataArr['idUser'];
                    $replace_ary['szFirstName'] = $bookingDataArr['szFirstName'];
                    $replace_ary['szLastName'] = $bookingDataArr['szLastName'];
                    $replace_ary['szEmail'] = $bookingDataArr['szEmail'];
                    $replace_ary['szForwarderDispName'] = $bookingDataArr['szForwarderDispName'];
                    $replace_ary['szForwarderCustServiceEmail'] = $customerServiceEmailAry[0];
                    $replace_ary['szBookingRef'] = $bookingDataArr['szBookingRef']; 
                    $replace_ary['supportEmail'] = __STORE_SUPPORT_EMAIL__;        
                    $replace_ary['szOriginCFSContact'] = $bookingDataArr['szWarehouseFromContactPerson'];;
                    $replace_ary['szOriginCFSEmail'] = $bookingDataArr['szWarehouseFromEmail'];        
                    $replace_ary['szShipperCompany'] = $bookingDataArr['szShipperCompanyName']; 
                    $replace_ary['szOriginCountry'] = $szOriginCountry;
                    $replace_ary['szDestinationCountry'] = $szDestinationCountry;
                    $replace_ary['iAdminFlag']=1; 
                    $replace_ary['idBooking'] = $idBooking;
                    $replace_ary['iBookingLanguage']=$bookingDataArr['iBookingLanguage'];

                    if($bookingDataArr['iCourierBooking']==1)
                    {
                        $replace_ary['szName']=$bookingDataArr['szFirstName']." ".$bookingDataArr['szLastName'];
                        $replace_ary['szSenderEmail']=$bookingDataArr['szShipperEmail'];
                        createEmail(__COURIER_BOOKING_CONFIRMATION_AND_INVOICE_EMAIL__, $replace_ary,$replace_ary['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__,true,$idBooking);
                    }
                    else
                    {
                        if($bookingDataArr['iBookingType']==__BOOKING_TYPE_RFQ__)
                        {
                            createEmail(__MANUAL_BOOKING_CONFIRMATION_EMAIL__, $replace_ary,$replace_ary['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__,true,$idBooking);
                        }
                        else
                        {
                            createEmail(__BOOKING_CONFIRMATION_EMAIL__, $replace_ary,$replace_ary['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__,true,$idBooking);
                        } 
                    }
                    
                    $idForwarder = $bookingDataArr['idForwarder'];
                    $kForwarder = new cForwarder(); 
                    $acceptedByUserAry = $kForwarder->getTermsConditionAcceptedByUserDetails($idForwarder);

                    $kForwarder->load($idForwarder);
                    $iProfitType = $kForwarder->iProfitType;
                    $replace_ary = array();
                    $replace_ary['szForwarderDispName'] = $kForwarder->szDisplayName;

                     $szControlPanelUrl = "http://".$kForwarder->szControlPanelUrl."/booking/".$idBooking."__".md5(time())."/";
 
                    if(($bookingDataArr['iBookingType']==__BOOKING_TYPE_COURIER__) || ($bookingDataArr['isManualCourierBooking']==1 && $bookingDataArr['iBookingType']==__BOOKING_TYPE_RFQ__))
                    {
                        cPartner::$szDeveloperNotes .= ">. Updating following data for courier API.".PHP_EOL;
                        /*
                        * If booking is either Automatic courier booking or manual courier booking then update following Status and other details
                        */
                        $kCourierServices = new cCourierServices();
                        $courierBookingArr = array();
                        $courierBookingArr = $kCourierServices->getCourierBookingData($idBooking);
                        
                        $iAddBookingFile=false;
                        if(__ENVIRONMENT__=='LIVE')
                        {
                            if(doNotCreateFileForThisDomainEmail($bookingDataArr['szEmail']))
                            {
                                $iAddBookingFile = true;
                            }
                        }
                        else
                        {
                            $iAddBookingFile = true;
                        }
                        if($iAddBookingFile)
                        {
                            if((int)$courierBookingArr[0]['iCourierAgreementIncluded']==1)
                            {
                                $fileLogsAry = array();
                                $fileLogsAry['szTransportecaStatus'] = "S7";
                                $fileLogsAry['iLabelStatus'] = __LABEL_NOT_SEND_FLAG__;
                                $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
                                $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime; 
                                $fileLogsAry['szForwarderBookingStatus'] = '';    
                                $fileLogsAry['szTransportecaTask'] = ''; 
                                $fileLogsAry['szForwarderBookingTask'] = 'T140'; 
                                $fileLogsAry['idBooking'] = (int)$idBooking;
                                $fileLogsAry['idUser'] = (int)$bookingDataArr['idUser'];  

                                cPartner::$szDeveloperNotes .= ">. ".print_R($fileLogsAry,true).PHP_EOL;
                               $kBooking->insertCourierBookingFileDetails($fileLogsAry); 
                            }
                            else
                            {
                                $fileLogsAry = array();
                                $fileLogsAry['szTransportecaStatus'] = "S7";
                                $fileLogsAry['iLabelStatus'] = __LABEL_NOT_SEND_FLAG__;
                                $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
                                $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime;                          
                                $fileLogsAry['szTransportecaTask'] = 'T140';  
                                $fileLogsAry['szForwarderBookingTask'] = ''; 
                                $fileLogsAry['idBooking'] = (int)$idBooking;        
                                $fileLogsAry['idUser'] = (int)$bookingDataArr['idUser']; 
                                cPartner::$szDeveloperNotes .= ">. ".print_R($fileLogsAry,true).PHP_EOL;
                                $kBooking->insertCourierBookingFileDetails($fileLogsAry);
                            }
                        }
                                                
                        $kForwarder = new cForwarder();
                        $kForwarder->load($bookingDataArr['idForwarder']);
                        $iProfitType = $kForwarder->iProfitType;
                        
                        $kCourierServices= new cCourierServices();
                        $newCourierBookingAry=$kCourierServices->getAllNewBookingWithCourier($idBooking);

                        $shipperDetailsArr=$kCourierServices->getShipperNameById($bookingArr['idShipperConsignee'],true);

                        if((int)$newCourierBookingAry[0]['iCourierAgreementIncluded']==1)
                        {
                            $szSender=$newCourierBookingAry[0]['szDisplayName'];
                        }
                        else
                        {    
                            if($bookingArr['iBookingLanguage']==2)
                              $szSender="Vi";
                            else
                                $szSender="we have";
                        }
                        
                        $searchDataAry = array();
                        $searchDataAry['idForwarder'] = $bookingDataArr['idForwarder'];
                        $searchDataAry['idTransportMode'] = $bookingDataArr['idTransportMode'];
                        $searchDataAry['idOriginCountry'] = $bookingDataArr['idOriginCountry'];
                        $searchDataAry['idDestinationCountry'] = $bookingDataArr['idDestinationCountry'];
                        $searchDataAry['iBookingSentFlag'] = '1';

                        $forwarderContactAry = array(); 
                        $kConfig = new cConfig();
                        $forwarderContactAry = $kConfig->getForwarderContact($searchDataAry);
                        
                        $replace_ary=array();
                        $replace_ary['szBookingRef']=$bookingDataArr['szBookingRef'];
                        $replace_ary['iBookingLanguage']=$bookingDataArr['iBookingLanguage'];
                        $replace_ary['szOriginCountry']=$bookingDataArr['szOriginCountry'];
                        $replace_ary['szDestinationCountry']=$bookingDataArr['szDestinationCountry'];
                        $replace_ary['szSender']=$szSender;
                        $replace_ary['szShipperCompany']=$shipperDetailsArr['szShipperCompanyName'];
                        $replace_ary['szSuuportEmail']=__STORE_SUPPORT_EMAIL__;
                        $replace_ary['szSupportPhone']=__STORE_SUPPORT_PHONE_NUMBER__;
                        $replace_ary['szForwarderDispName']=$bookingDataArr['szForwarderDispName'];
                        $replace_ary['szForwarderCustServiceEmail']=$customerServiceEmailStr;
                        $replace_ary['szName']=$bookingDataArr['szFirstName']." ".$bookingDataArr['szLastName'];
                        $replace_ary['szEmail']=$bookingDataArr['szEmail'];     
                        $replace_ary['szBooking'] = $bookingDataArr['szBookingRef'];
                        $replace_ary['iAdminFlag']=1; 
                        $replace_ary['idBooking'] = $idBooking; 
                        
                        $replace_ary=array(); 
                        $szControlPanelUrl = "http://".$kForwarder->szControlPanelUrl."/booking/".$idBooking."__".md5(time())."/"; 

                        $replace_ary['szLink'] = '<a href="'.$szControlPanelUrl.'"> CLICK HERE</a>';
                        $replace_ary['szUrl'] = $szControlPanelUrl;


                        $quotesAry =$kCourierServices->getAllBookingQuotesByFileByBookingId($idBooking);

                        $szControlPanelLabelUrl='http://'.$kForwarder->szControlPanelUrl."/pendingQuotes/createLabel/".$idBooking."__".md5(time())."/";


                        $replace_ary['szLabelLink'] = '<a href="'.$szControlPanelLabelUrl.'" target="_blank"> UPLOAD LABLES AND TRACKING NUMBER HERE</a>';
                        $replace_ary['szLabelUrl'] = $szControlPanelLabelUrl;

                        $replace_ary['szBookingRef']=$bookingDataArr['szBookingRef'];
                        $replace_ary['iBookingLanguage']=$bookingDataArr['iBookingLanguage'];
                        $replace_ary['szOriginCountry']=$bookingDataArr['szOriginCountry'];
                        $replace_ary['szDestinationCountry']=$bookingDataArr['szDestinationCountry'];
                        $replace_ary['szForwarderDispName']=$bookingDataArr['szForwarderDispName'];

                        $replace_ary['szFirstName'] = html_entities_flag($acceptedByUserAry['szFirstName'],false);
                        $replace_ary['szLastName'] = html_entities_flag($acceptedByUserAry['szLastName'],false);
                        $replace_ary['dtAgreement'] = date('d/m/Y',strtotime($acceptedByUserAry['dtAgreement']));
                        $replace_ary['szEmail']=$forwarderContactArys;   

                        if($bookingArr['iCourierBookingType']==1)
                        {
                            $szBookingType="Courier";
                        }
                        else
                        {
                            $szBookingType="Courier";
                        }        
                        $replace_ary['szBookingType']=$szBookingType;

                        $replace_ary['szTermsCoditionUrl'] = '<a href="http://'.$kForwarder->szControlPanelUrl.'/T&C/">Terms & Conditions</a>'; 
                        $replace_ary['iAdminFlag']=1; 
                        $replace_ary['idBooking'] = $idBooking;
                        
                        if($iProfitType==1)
                        {
                            $kConfig = new cConfig();
                            $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',1);
                            $szForwarderTerms_text1 = $configLangArr[1]['szForwarderTermsText1'];
                            $szForwarderTerms_text2 = $configLangArr[1]['szForwarderTermsText2'];
                            $szForwarderTerms_text3 = $configLangArr[1]['szForwarderTermsText3'];
                            $iTermsConditionForReferralForwarder =$szForwarderTerms_text1." ".$replace_ary['szTermsCoditionUrl']."".$szForwarderTerms_text2." ".$replace_ary['szFirstName']." ".$replace_ary['szLastName']." ".$szForwarderTerms_text3." ".$replace_ary['dtAgreement'];
                            $replace_ary['szTermsConditionForReferralForwarderText']="<br>".$iTermsConditionForReferralForwarder.".<br>";
                        }
                        else
                        {
                            $replace_ary['szTermsConditionForReferralForwarderText']='';
                        }

                        $kWhsSearch = new cWHSSearch();  
                        $forwarderBookingEmail = $kWhsSearch->getManageMentVariableByDescription('__FORWARDER_BOOKING_CONFIRMATION_EMAIL__'); 
                        

                        if((int)$courierBookingArr[0]['iCourierAgreementIncluded']==1)
                        {
                            createEmail(__COURIER_BOOKING_RECEIVED_CREATE_LABEL_FORWARDER_EMAIL__, $replace_ary, $forwarderContactAry,'' , $forwarderBookingEmail,$bookingArr['idForwarder'] , $forwarderBookingEmail,__FLAG_FOR_FORWARDER__);
                        }
                        else 
                        {
                            $forwarderContactNewAry=array();
                            if(!empty($forwarderContactAry))
                            {

                                $ctr=0;
                                foreach($forwarderContactAry as $forwarderContactArys)
                                {
                                    if(doNotSendMailToThisDomainEmail($forwarderContactArys))
                                    {
                                        $forwarderContactNewAry[$ctr]=$forwarderContactArys;
                                        ++$ctr;
                                    }
                                }
                            }
                            if(!empty($forwarderContactNewAry)){
                                createEmail(__FORWADER_COURIER_BOOKING_RECEIVED__, $replace_ary, $forwarderContactNewAry,'' , $forwarderBookingEmail,$bookingArr['idForwarder'] , $forwarderBookingEmail,__FLAG_FOR_FORWARDER__); 
                            }
                        }
                        
                        if($bookingDataArr['isManualCourierBooking']==1)
                        {
                            
                            
                            $kCourierServices=new cCourierServices();
                            $courierBookingArr=$kCourierServices->getCourierBookingData($bookingDataArr['id']);
                                                        
                            if($courierBookingArr[0]['iCourierAgreementIncluded']==1)
                            {
                                $idQuotePriceDetails = $bookingDataArr['idQuotePricingDetails'];

                                $data = array();
                                $data['idBookingQuotePricing'] = $idQuotePriceDetails ;
                                $quotePricingAry = array();
                                $quotePricingAry = $kBooking->getAllBookingQuotesPricingDetails($data);
                                
                                if(!empty($quotePricingAry))
                                {
                                    $idBookingQuote = $quotePricingAry[1]['idQuote'];
                                    $dtStatusUpdatedOnTime=$kBooking->getRealNow();
                                    $res_ary = array();
                                    $res_ary['szForwarderQuoteStatus'] = 'FQS2' ; 
                                    $res_ary['szForwarderQuoteTask'] = '' ; 
                                    $res_ary['dtStatusUpdatedOn'] = $dtStatusUpdatedOnTime;
                                    $res_ary['iWonQuote'] = 1;
                                    $res_ary['iClosed'] = 1;
                                    $res_ary['iClosedForForwarder'] = 0;
                                    
                                    
                                    if(!empty($res_ary))
                                    {
                                        $update_query='';
                                        foreach($res_ary as $key=>$value)
                                        {           
                                            $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                                        }
                                    } 
                                    $update_query = rtrim($update_query,",");    
                                    $kBooking->updateBookingQuotesDetails($update_query,$idBookingQuote); 

                                }
                            }
                        }
                        
                        
                    }
                    else
                    {
                        
                        cPartner::$szDeveloperNotes .= ">. Updating Task status for LCL/LTL.".PHP_EOL;
                        /*
                        * If booking is either Automatic LCL booking or manual RFQs booking then we call following function to update status in tblbookingfile
                        */
                        $kBooking->updateBookingFileAfterPaymentConfirm($bookingDataArr);
                        
                        $replace_ary['szLink'] = '<a href="'.$szControlPanelUrl.'"> CLICK HERE</a>';
                        $replace_ary['szUrl'] = $szControlPanelUrl;
                        $replace_ary['szTermsCoditionUrl'] = '<a href="http://'.$kForwarder->szControlPanelUrl.'/T&C/">Terms & Conditions</a>';	

                        $replace_ary['szFirstName'] = utf8_encode($acceptedByUserAry['szFirstName']);
                        $replace_ary['szLastName'] = utf8_encode($acceptedByUserAry['szLastName']);
                        $replace_ary['dtAgreement'] = date('d/m/Y',strtotime($acceptedByUserAry['dtAgreement']));

                        $kConfig = new cConfig();
                        $szOriginCountryAry = array();
                        $szDestinationCountryAry = array();

                        if($iProfitType==1)
                        {
                            $kConfig = new cConfig();
                            $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',1);
                            $szForwarderTerms_text1 = $configLangArr[1]['szForwarderTermsText1'];
                            $szForwarderTerms_text2 = $configLangArr[1]['szForwarderTermsText2'];
                            $szForwarderTerms_text3 = $configLangArr[1]['szForwarderTermsText3'];
                            $iTermsConditionForReferralForwarder =$szForwarderTerms_text1." ".$replace_ary['szTermsCoditionUrl']."".$szForwarderTerms_text2." ".$replace_ary['szFirstName']." ".$replace_ary['szLastName']." ".$szForwarderTerms_text3." ".$replace_ary['dtAgreement'];
                            $replace_ary['szTermsConditionForReferralForwarderText']="<br>".$iTermsConditionForReferralForwarder.".<br>";
                        }
                        else
                        {
                            $replace_ary['szTermsConditionForReferralForwarderText']='';
                        }

                        if($bookingDataArr['iBookingType']==__BOOKING_TYPE_RFQ__)
                        {
                            $szOriginCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idOriginCountry']);
                            $szDestinationCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idDestinationCountry']) ;

                            $szOriginCountry = $szOriginCountryAry[$bookingDataArr['idOriginCountry']]['szCountryName'] ;
                            $szDestinationCountry = $szDestinationCountryAry[$bookingDataArr['idDestinationCountry']]['szCountryName'] ;	

                            $szOriginCountryStr = '';
                            if(!empty($bookingDataArr['szShipperCity']))
                            {
                                $szOriginCountryStr = $bookingDataArr['szShipperCity'].", ";
                            }
                            $szOriginCountryStr .= $szOriginCountry ;

                            $szDestinationCountryStr = '';
                            if(!empty($bookingDataArr['szConsigneeCity']))
                            {
                                $szDestinationCountryStr = $bookingDataArr['szConsigneeCity'].", ";
                            }
                            $szDestinationCountryStr .= $szDestinationCountry ;
                        }
                        else
                        {
                            $szOriginCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idWarehouseFromCountry']);
                            $szDestinationCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idWarehouseToCountry']) ;

                            $szOriginCountry = $szOriginCountryAry[$bookingDataArr['idWarehouseFromCountry']]['szCountryName'] ;
                            $szDestinationCountry = $szDestinationCountryAry[$bookingDataArr['idWarehouseToCountry']]['szCountryName'] ;	

                            $szOriginCountryStr = '';
                            if(!empty($bookingDataArr['szWarehouseFromCity']))
                            {
                                $szOriginCountryStr = $bookingDataArr['szWarehouseFromCity'].", ";
                            }
                            $szOriginCountryStr .= $szOriginCountry ;


                            $szDestinationCountryStr = '';
                            if(!empty($bookingDataArr['szWarehouseToCity']))
                            {
                                $szDestinationCountryStr = $bookingDataArr['szWarehouseToCity'].", ";
                            }
                            $szDestinationCountryStr .= $szDestinationCountry ;
                        } 

                        $replace_ary['szBookingRef'] = $bookingDataArr['szBookingRef'];
                        $replace_ary['supportEmail'] = __STORE_SUPPORT_EMAIL__;
                        $replace_ary['szOriginCountry'] = $szOriginCountryStr;
                        $replace_ary['szDestinationCountry'] = $szDestinationCountryStr;

                        $email_address_forwarders_Arrs=$kForwarder->getForwarderCustomerServiceEmail($idForwarder,1);
                        //$email_address_forwardersAry = format_fowarder_emails($email_address_forwarders_Arrs);
                        //$forwarder_booking_email = $email_address_forwardersAry[0];

                        if($bookingDataArr['idTransportMode']>0)
                        {
                            $kConfig = new cConfig();
                            $transportModeListAry = array();
                            $transportModeListAry = $kConfig->getAllTransportMode(false,false,true); 

                            $replace_ary['szBookingType'] = $transportModeListAry[$bookingDataArr['idTransportMode']]['szLongName'] ;
                        }
                        else
                        {
                            $replace_ary['szBookingType'] = 'Seafreight';
                        }
                        $replace_ary['iAdminFlag']=1; 
                        $replace_ary['idBooking'] = $idBooking;
                        $searchDataAry = array();
                        $searchDataAry['idForwarder'] = $bookingDataArr['idForwarder'];
                        $searchDataAry['idTransportMode'] = $bookingDataArr['idTransportMode'];
                        $searchDataAry['idOriginCountry'] = $bookingDataArr['idOriginCountry'];
                        $searchDataAry['idDestinationCountry'] = $bookingDataArr['idDestinationCountry'];
                        $searchDataAry['iBookingSentFlag'] = '1';

                        $forwarderContactAry = array(); 
                        $kConfig = new cConfig();
                        $forwarderContactAry = $kConfig->getForwarderContact($searchDataAry);
                        createEmail('__FORWARDER_BOOKING_CONFIRMATION_EMAIL__', $replace_ary,$forwarderContactAry ,' ', $forwarderBookingEmail,$replace_ary['idUser'], $forwarderBookingEmail,__FLAG_FOR_FORWARDER__,false,$idBooking);
                        
                        /* 
                        * In Case of RFQ we do not send email confirmation to shipper we send this only in case of Automatic booking 
                        */
                       if($bookingDataArr['iBookingType']==__BOOKING_TYPE_AUTOMATIC__)
                       {
                           $szWareHouseMapStr = '';

                           if(!empty($bookingDataArr['szWarehouseFromName']))
                           { 
                               $szWareHouseMapStr.=$bookingDataArr['szWarehouseFromName']." <br>";
                           }
                           if(!empty($bookingDataArr['szWarehouseFromAddress']))
                           { 
                               $szWareHouseMapStr .= $bookingDataArr['szWarehouseFromAddress'];
                           }
                           if(!empty($bookingDataArr['szWarehouseFromAddress2']))
                           { 
                               $szWareHouseMapStr .= ", ".$bookingDataArr['szWarehouseFromAddress2'];
                           }
                           if(!empty($bookingDataArr['szWarehouseFromAddress3']))
                           { 
                               $szWareHouseMapStr .= ", ".$bookingDataArr['szWarehouseFromAddress3'];
                           }

                           if(!empty($bookingDataArr['szWarehouseFromPostCode']) || !empty($bookingDataArr['szWarehouseFromCity']))
                           {
                               if(!empty($bookingDataArr['szWarehouseFromPostCode']))
                               {
                                   $szWareHouseFromStr.="<br> ".$bookingDataArr['szWarehouseFromPostCode'];
                                   $szWareHouseMapStr .="<br> ".$bookingDataArr['szWarehouseFromPostCode'];
                               }

                               if(!empty($bookingDataArr['szWarehouseFromPostCode']) && !empty($bookingDataArr['szWarehouseFromCity']))
                               {
                                   $szWareHouseFromStr.=" ".$bookingDataArr['szWarehouseFromCity']."<br> ";
                                   $szWareHouseMapStr .=" ".$bookingDataArr['szWarehouseFromCity']."<br> ";
                               }
                               else if(!empty($bookingDataArr['szWarehouseFromCity']))
                               {
                                   $szWareHouseFromStr.= "<br> ".$bookingDataArr['szWarehouseFromCity']."<br> ";
                                   $szWareHouseMapStr .= "<br> ".$bookingDataArr['szWarehouseFromCity']."<br> ";
                               }
                               else if(!empty($bookingDataArr['szWarehouseFromPostCode']))
                               {
                                   $szWareHouseMapStr .= "<br /> ";
                                   $szWareHouseFromStr .= "<br /> ";
                               }
                           }						
                           if(!empty($bookingDataArr['szWarehouseFromCountry']) || !empty($bookingDataArr['szWarehouseFromState']))
                           {
                               if(!empty($bookingDataArr['szWarehouseFromState']))
                               {
                                   $szWareHouseFromStr.=$bookingDataArr['szWarehouseFromState'];
                                   $szWareHouseMapStr .= $bookingDataArr['szWarehouseFromState'];
                               }

                               if(!empty($bookingDataArr['szWarehouseFromState']) && !empty($bookingDataArr['szWarehouseFromCountry']))
                               {
                                   $szWareHouseFromStr.=" ".$bookingDataArr['szWarehouseFromCountry']."<br> ";
                                   $szWareHouseMapStr .=" ".$bookingDataArr['szWarehouseFromCountry']."<br> ";
                               }
                               else if(!empty($bookingDataArr['szWarehouseToCountry']))
                               {
                                   $szWareHouseFromStr.= $bookingDataArr['szWarehouseFromCountry']."<br> ";
                                   $szWareHouseMapStr .= $bookingDataArr['szWarehouseFromCountry']."<br> ";
                               }
                           }

                           $serviceTypeEnglishAry = $kConfig->getConfigurationData(__DBC_SCHEMATA_SERVICE_TYPE__,$bookingDataArr['idServiceType'],false,true);
                           $configLangArr=$kConfig->getConfigurationLanguageData('__TABLE_SERVICE_TYPE__');
                           $serviceTypeEnglishAry = $configLangArr[$bookingDataArr['idServiceType']];            
                           $services_string = $serviceTypeEnglishAry['szDescription'];

                           $cc_string = '';
                           if((int)$bookingDataArr['iOriginCC']==1)
                           {
                               $cc_string = t($t_base.'fields/and')." ".t($t_base.'fields/customs_clearance_at')." ".t($t_base.'fields/origin');
                               $cc_string_english = "and customs clearance at origin";
                           }								
                           if((int)$bookingDataArr['iDestinationCC']==2)
                           {
                               if(empty($cc_string))
                               {
                                   $cc_string = t($t_base.'fields/and')." ".t($t_base.'fields/customs_clearance_at')." ".t($t_base.'fields/destination');
                                   $cc_string_english = "and customs clearance at destination";
                               }
                               else
                               {
                                   $cc_string.= " ".t($t_base.'fields/and')." ".t($t_base.'fields/destination');
                                   $cc_string_english .= " and destination";
                               }
                           }

                           $customerServiceEmailAry = array();
                           $idForwarder = $bookingDataArr['idForwarder'] ;
                           $customerServiceEmailAry = $kForwarder->getForwarderCustomerServiceEmail($idForwarder);

                           $customerServiceEmailLinkAry = format_fowarder_emails($customerServiceEmailAry) ;
                           $customerServiceEmailLink = $customerServiceEmailLinkAry[0];
                           $customerServiceEmailStr = $customerServiceEmailLinkAry[1];

                           $cargoDetailsAry = $kBooking->getCargoComodityDeailsByBookingId($bookingDataArr['id']);
                           $szCargoCommodity = $cargoDetailsAry['1']['szCommodity'];

                           $cargo_volume = format_volume($bookingDataArr['fCargoVolume']);
                           $cargo_weight = round((float)$bookingDataArr['fCargoWeight']);							
                           $szCargoDetails_str = $cargo_volume." cbm, ".$cargo_weight." kg, ".utf8_decode($szCargoCommodity);

                           /*if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__)
                           {
                               $services_string = "EXW ".$bookingDataArr['szShipperCity'].", with delivery in ".$bookingDataArr['szConsigneeCity'].", including customs clearance. ";
                           }
                           else
                           {
                               $services_string = "FOB ".$bookingDataArr['szWarehouseFromCity'].", with delivery in ".$bookingDataArr['szConsigneeCity'].", including import customs clearance. ";
                           }*/	 

                           $services_string =display_service_type_description($bookingDataArr,__LANGUAGE_ID_ENGLISH__,true);

                           $replace_ary = array();
                           $replace_ary['szBookingRef'] = $bookingDataArr['szBookingRef'];
                           $replace_ary['szFirstName'] = $bookingDataArr['szFirstName'] ;
                           $replace_ary['szLastName'] = $bookingDataArr['szLastName'] ;

                           $replace_ary['szShipperFirstName'] = $bookingDataArr['szShipperFirstName'] ;
                           $replace_ary['szShipperLastName'] = $bookingDataArr['szShipperLastName'] ;

                           $replace_ary['szForwarderDispName'] = $bookingDataArr['szForwarderDispName']; 
                           $replace_ary['szServiceType'] = $services_string  ;
                           $replace_ary['szShipperCompany'] = $bookingDataArr['szShipperCompanyName'] ;
                           $replace_ary['szConsigneeCompany'] = $bookingDataArr['szConsigneeCompanyName'] ;
                           $replace_ary['szCargoDetails'] = $szCargoDetails_str ;
                           $replace_ary['szForwarderEmail'] = $customerServiceEmailStr; 

                           $replace_ary['szOriginCFSContact'] = $bookingDataArr['szWarehouseFromContactPerson'];;
                           $replace_ary['szOriginCFSEmail'] = $bookingDataArr['szWarehouseFromEmail'];

                           if($bookingDataArr['idServiceType']==__SERVICE_TYPE_WTD__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_WTW__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_PTW__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_WTP__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_PTP__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_PTD__ )  // WTW=4 , WTD = 3
                           {					
                               $szCutoffAvailableString = "Please ensure that the cargo is delivered to: <br> ".nl2br($szWareHouseMapStr)."<br>Latest by cut-off: ".date('d/m/Y H:i',strtotime($bookingDataArr['dtCutOff'])) ;
                               $replace_ary['szCutoffAvailableString'] = $szCutoffAvailableString ; 
                           }
                           else
                           {
                               $szCutoffAvailableString = $bookingDataArr['szForwarderDispName']." will contact you directly to arrange exact details for pick-up. You should expect to have the shipment ready for pick-up by ".date('d/m/Y',strtotime($bookingDataArr['dtCutOff'])).".";
                               $replace_ary['szCutoffAvailableString'] = $szCutoffAvailableString ;
                           } 

                           $replace_ary['iAdminFlag']=1; 
                           $replace_ary['idBooking'] = $idBooking; 

                           $shipper_mail = $bookingDataArr['szShipperEmail'] ;
                           $ccEmail = $bookingDataArr['szWarehouseFromEmail']; 
                           $shipperEmailAry = array();
                           $shipperEmailAry[0] = $shipper_mail ;
                           $shipperEmailAry[1] = $ccEmail ;
                           $replace_ary['szToEmail'] = $shipper_mail;
                           $replace_ary['szCCEmail'] = $ccEmail;
                           $ccEmail='';

                           createEmail('__SHIPPER_CONFIRMATION_EMAIL_TEXT__', $replace_ary,$shipperEmailAry ,' ', __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__,false,$idBooking,false,$ccEmail);
                       }
                    }
                    return true;
                } 
                else
                {
                    cPartner::$szDeveloperNotes .= ">. Could not able to update Stripe data into database.".PHP_EOL;
                }
            }
            else
            {
                return false; 
            }   
        }
    } 
    
    function changeTransferStatus($idForwarderTransaction,$bookingDataArr)
    {
        if((int)$idForwarderTransaction>0)
        { 
            $szSelfInvoiceCol='';
            if($bookingDataArr['iPaymentType']!=__TRANSPORTECA_PAYMENT_TYPE_3__)
            {
                if($bookingDataArr['iForwarderGPType']==__FORWARDER_PROFIT_TYPE_REFERRAL_FEE__)
                {
                    $kBooking = new cBooking();
                    $szSelfInvoice = $kBooking->generateInvoice($bookingDataArr['idForwarder'],true);
                    $szSelfInvoiceCol = ",szSelfInvoice='".  mysql_escape_custom($szSelfInvoice)."'";
                }
            }
            
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                SET 
                    iTransferConfirmed = '0'
                    $szSelfInvoiceCol
                WHERE
                    id = '".$idForwarderTransaction."'
            ";
            //echo $query;
            if($result = $this->exeSQL( $query ))
            {
                return true;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    
    function updateiTrackingEmailSend()
    {
        $dtReminderDateTime=date('Y-m-d H:i:s');
        $szCustomerTimeZone = 'Europe/Copenhagen' ; 
        $date = new DateTime($dtReminderDateTime); 
        $date->setTimezone(new DateTimeZone($szCustomerTimeZone));    

        $dtEmailReminderDate = $date->format('Y-m-d');
        $dtEmailReminderDateFormat = $date->format('d/m/Y');
        $szEmailReminderTime = $date->format('H:i');
        $szEmailReminderHour = $date->format('H');
        $dtPreviousDateTime=date('Y-m-d',  strtotime("-1 DAY") );
        
        $query="
            SELECT
                b.id AS idBooking,
                cb.dtCollection,
                cb.dtCollectionEndTime,
                b.szTrackingNumber,
                b.idFile,
                f.idFileOwner,
                b.szBookingRef,
                b.szTrackingTag
            FROM
                ".__DBC_SCHEMATA_FILE_LOGS__." AS f
            INNER JOIN
                ".__DBC_SCHEMATA_BOOKING__." AS b
            ON
                b.id = f.idBooking
            INNER JOIN
                ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__." AS cb
            ON
                b.id = cb.idBooking
            WHERE
                f.iLabelStatus>='2'
            AND
                cb.dtCollection<>'0000-00-00'
            AND
                b.iTrackingEmailSend='0'
            AND
                b.iSendTrackingUpdates='1'
        ";
        echo $query;
        if(($result = $this->exeSQL($query)))
        {
            if($this->getRowCnt()>0)
            { 
                $ctr=0;
                while($row = $this->getAssoc($result))
                {
                    $szTrackingTag=$row['szTrackingTag'];
                    $idBooking=$row['idBooking'];
                    if($szTrackingTag!='' && $szTrackingTag!='Pending')
                    {
                        $queryUpdate="
                            UPDATE
                                ".__DBC_SCHEMATA_BOOKING__."
                            SET 
                                iInTransitEmailSend = '1' 
                            WHERE
                                id = '".(int)$idBooking."'
                        ";
                        echo $queryUpdate."<br /><br />";
                       // $resultUpdate = $this->exeSQL( $queryUpdate );
                        
                    }
                }
            }
        }
    }
    
    function isBillingDetailsEmpty($data)
    {
        if(!empty($data))
        {
            if(!empty($data['szBillingCompanyName']))
            {
                return true;
            }
            else if(!empty($data['szBillingFirstName']))
            {
                return true;
            }
            else if(!empty($data['szBillingLastName']))
            {
                return true;
            }
            else if(!empty($data['szBillingEmail']))
            {
                return true;
            }
            else if(!empty($data['iBillingCountryDialCode']))
            {
                return true;
            }
            else if(!empty($data['iBillingPhoneNumber']))
            {
                return true;
            }
            else if(!empty($data['szBillingAddress']))
            {
                return true;
            }
            else if(!empty($data['szBillingCity']))
            {
                return true;
            }
            else if(!empty($data['szBillingPostcode']))
            {
                return true;
            }
            else if(!empty($data['szBillingCountry']))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    
    function generateBatchNumberForPayment()
    {	
        $kAdmin = new cAdmin();
        $szInvoice = $this->getMaxBatchNumberForPayment();
        $szInvoice_formatted = $kAdmin->getFormattedString($szInvoice+1,6);
        $customer_code = "1".$szInvoice_formatted ;
 
        $bookingInvoiceAry = array();
        $bookingInvoiceAry['iMaxNumInvoice'] = $szInvoice+1;
        $bookingInvoiceAry['szInvoice'] = $customer_code; 
        $this->addPaymentBatchNumberLogs($bookingInvoiceAry); 
        return $customer_code;
    }
    
    function getMaxBatchNumberForPayment()
    {
        $query="
            SELECT
                MAX(iMaxNumInvoice) iMaxInvoice
            FROM
                ".__DBC_SCHEMATA_PAYMENT_BATCH_NUMBER_LOGS__."
        ";
        if($result=$this->exeSQL($query))
        {
            $row=$this->getAssoc($result);
            return $row['iMaxInvoice'];
        }
        else
        {
            return 0;
        }
    }  
    
    function addPaymentBatchNumberLogs($data)
    {
        if(!empty($data))
        {
            $query = "
                INSERT INTO
                    ".__DBC_SCHEMATA_PAYMENT_BATCH_NUMBER_LOGS__."
                (
                    iMaxNumInvoice,
                    szInvoice,
                    dtCreatedOn
                )
                VALUES
                (
                    '".mysql_escape_custom($data['iMaxNumInvoice'])."',
                    '".mysql_escape_custom($data['szInvoice'])."',
                    now()
                )	
            ";
            //echo  $query;
            if($result=$this->exeSQL($query))
            {
                return true;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    
    function getAccountBalanceByForwarder($idForwarder,$dateArr=array())
    {
            $result_arr=array();
            $result_arr_uploadservice=array(); 
            if(!empty($dateArr))
            {   
                $dtPaymentConfirmed = " DATE(dtPaymentConfirmed) ";	
                $dtInvoiceOn = " DATE(dtInvoiceOn) ";
                $dtPaymentScheduled = " DATE(dtInvoiceOn) "; 
                /*
                 * In following query we have 3 cases 
                 * Case - 1: This block will fetch all the 'Automatic Transfer' records which not have been confirmed yet. I mean all the record which on Management /forwarderTransfer/ screen
                 * Case - 2: This section will fetch all the record which has been paid. In system we consider those record as paid where iStatus = 2 and dtPaymentConfirmed <> NULL
                 * Case - 3: This section will fetch all the bookings where payment has been been received to Transporteca. I mean all the booking which is on Management > /outstandingPayment/ screen. 
                 */
                $query_date = "	 
                    AND 
                    (
                        (
                                iDebitCredit = 2
                            AND
                                iStatus = '1'
                            AND
                                iBatchNo<>'0'
                            AND
                                ".$dtPaymentScheduled." >= '". mysql_escape_custom($dateArr[0]) . "'  
                        )
                        OR
                        (
                                iStatus > 0
                            AND
                                ".$dtPaymentConfirmed."
                            BETWEEN 
                                '". mysql_escape_custom($dateArr[0]) . "' 
                            AND 
                                '".mysql_escape_custom($dateArr[1])."' 
                        )
                        OR
                        (
                                iStatus = 0
                            AND
                                ".$dtInvoiceOn."
                            BETWEEN 
                                '". mysql_escape_custom($dateArr[0]) . "' 
                            AND 
                                '".mysql_escape_custom($dateArr[1])."' 
                        )
                    )
                ";
            }
            
            $query="
                SELECT 
                    round(SUM(fTotalPriceForwarderCurrency),2) AS iCreditBalance,
                    szForwarderCurrency		
                FROM 
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS ft		
                WHERE	
                    iDebitCredit IN ('1','13') 	
                AND 
                    idForwarder='".(int)$idForwarder."'
                AND
                    ft.iTransferConfirmed = '0'
                AND 
                    iForwarderGPType != '".__FORWARDER_PROFIT_TYPE_MARK_UP__."'
                    $query_date
                GROUP BY
                    szForwarderCurrency	
            ";
           // echo $query;
            if($result=$this->exeSQL($query))
            {
                while($row=$this->getAssoc($result))
                {
                    $result_arr[$row['szForwarderCurrency']]['booking']=round($row['iCreditBalance'],2);
                }
            } 
            $query="
                SELECT 
                    round(SUM(fTotalPriceForwarderCurrency),2) AS iCreditBalance,
                    szForwarderCurrency		
                FROM 
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS ft		
                WHERE	
                    iDebitCredit IN ('3','5','6') 
                AND 
                    idForwarder='".(int)$idForwarder."'
                AND
                    iTransferConfirmed = '0' 
                AND 
                    iForwarderGPType != '".__FORWARDER_PROFIT_TYPE_MARK_UP__."'
                    $query_date
                GROUP BY
                        szForwarderCurrency	
            ";
            //echo "<br><br>".$query;
            if($result=$this->exeSQL($query))
            {
                while($row=$this->getAssoc($result))
                {
                    $result_arr[$row['szForwarderCurrency']]['paidAmount']=round($row['iCreditBalance'],2);
                }
            }


            $query="
                SELECT 
                    round(SUM(fTotalPriceForwarderCurrency),2) AS iCreditBalance,
                    szForwarderCurrency		
                FROM 
                        ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS ft		
                WHERE	
                    iDebitCredit IN ('2','12') 
                AND 
                    idForwarder='".(int)$idForwarder."'
                AND
                    ft.iTransferConfirmed = '0'
                AND 
                    iForwarderGPType != '".__FORWARDER_PROFIT_TYPE_MARK_UP__."'
                    $query_date
                GROUP BY
                    szForwarderCurrency	
            ";
            //echo "<br><br>".$query;
            if($result=$this->exeSQL($query))
            {
                while($row=$this->getAssoc($result))
                {
                    $result_arr[$row['szForwarderCurrency']]['autoTransfer']=round($row['iCreditBalance'],2);
                }
            } 
 
            if(!empty($result_arr))
            {
                foreach($result_arr as $key=>$values)
                {
                    $debitAmount=$values['paidAmount']+$values['autoTransfer'];
                    $totalAmountArr[$key]=round($values['booking'],2)-round($debitAmount,2);
                }
            }  
            return $totalAmountArr;

    }
    
    function confirmBookingForBankTransfer($postSearchAry,$szPaymentType)
    {
        if(!empty($postSearchAry))
        { 
            $idBooking = $postSearchAry['id'];
            
            $kForwarder= new cForwarder();
            $kConfig = new cConfig();
            $kForwarder->load($postSearchAry['idForwarder']);
            $iCreditDays= $kForwarder->iCreditDays;
            $szForwarderVatRegistrationNum= $kForwarder->szVatRegistrationNum;
            cPartner::$szDeveloperNotes .= ">. Updating booking flags after customer select the bank transfer payment method.".PHP_EOL;
            if($szPaymentType==__TRANSPORTECA_PAYMENT_TYPE_3__)
            {
                $kForwarder = new cForwarder();
                $bookingRefLogAry = array();

                $kBooking = new cBooking();
                $kConfig = new cConfig();
                $kForwarderContact= new cForwarderContact();
                $kUser = new cUser();
                $kWHSSearch = new cWHSSearch();
                
                $bookingDataArr = array();
                $bookingDataArr = $kBooking->getExtendedBookingDetails($idBooking);
                $iLanguage = $bookingDataArr['iBookingLanguage'];
                
                
                if((int)$bookingDataArr['iPaymentTypePartner']==0)
                {
                    $szCustomerCareNumer = $kWHSSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__',$iLanguage);
                
                    $kForwarder->load($bookingDataArr['idForwarder']);
                    $iCreditDays= $kForwarder->iCreditDays;
                    $szForwarderVatRegistrationNum= $kForwarder->szVatRegistrationNum;
                    if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__)
                    {
                        $szBankTransferDueDateTime = strtotime($bookingDataArr['dtCutOff']) - ($bookingDataArr['iBookingCutOffHours']*60*60);
                        //$szBankTransferDueDateTime = date('j. F',$szBankTransferDueDateTime);

                        $iMonth = (int)date("m",$szBankTransferDueDateTime);
                        $szMonthName = getMonthName($iLanguage,$iMonth,true);
                        $szBankTransferDueDateTime = date('j.',$szBankTransferDueDateTime)." ".$szMonthName;
                    }
                    else
                    {
                        $szBankTransferDueDateTime = strtotime($bookingDataArr['dtWhsCutOff']) - ($bookingDataArr['iBookingCutOffHours']*60*60);

                        $iMonth = (int)date("m",$szBankTransferDueDateTime);
                        $szMonthName = getMonthName($iLanguage,$iMonth,true);
                        $szBankTransferDueDateTime = date('j.',$szBankTransferDueDateTime)." ".$szMonthName;
                    }

                    $kAdmin  =new cAdmin();
                    $invoiceNumber = '';
                    if($bookingDataArr['iInsuranceIncluded']==1)
                    { 
                        //$invoiceNumber=$kAdmin->generateInvoiceForPayment();
                        $invoiceNumber = $bookingDataArr['szInvoice'];
                    }

                    $kWhsSearch = new cWHSSearch();  
                    $iNumDaysPayBeforePickup = $kWhsSearch->getManageMentVariableByDescription('__NUMBER_OF_WEEKDAYS_PAYMENT_REQUIRED_BEFORE_REQUESTED_PICKUP__'); 

                    $dtBookingConfirmed = $kBooking->getRealNow();

                    $iBookingType = $bookingDataArr['iBookingType'];
                    /*
                    * Re-validating iBookingType Flag to make system robust
                    */
                    if($bookingDataArr['idServiceProvider']>0 && $bookingDataArr['iCourierBooking']==1)
                    {
                        $iBookingType = __BOOKING_TYPE_COURIER__ ;
                    }
                    else if($postSearchAry['iQuotesStatus']>0)
                    {
                        $iBookingType = __BOOKING_TYPE_RFQ__ ;
                    }
                    else
                    {
                        $iBookingType = __BOOKING_TYPE_AUTOMATIC__ ;
                    }

                    if($bookingDataArr['iBookingType']==__BOOKING_TYPE_COURIER__)
                    {
                        $szBankTransferDueDateTime=date('Y-m-d',strtotime($bookingDataArr['dtCutOff']));
                        $szBankTransferDueDateTime=getBusinessDaysForCourier($szBankTransferDueDateTime,$iNumDaysPayBeforePickup.' DAY');

                        $iMonth = (int)date("m",strtotime($szBankTransferDueDateTime));
                        $szMonthName = getMonthName($iLanguage,$iMonth,true);
                        $szBankTransferDueDateTime = date('d.',strtotime($szBankTransferDueDateTime))." ".$szMonthName;		 
                    } 


                    $kBooking = new cBooking();
                    $dtResponseTime = $kBooking->getRealNow();

                    $idBookingFile = $bookingDataArr['idFile'] ;  
                    $fileLogsAry = array();
                    $fileLogsAry['szTransportecaStatus'] = "S6";
                    $fileLogsAry['szTransportecaTask'] = ""; //T4
                    $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
                    $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime; 
                    $fileLogsAry['iStatus'] = 6; 
                    $fileLogsAry['iClosed'] = 0; 

                    if(!empty($fileLogsAry))
                    {
                        foreach($fileLogsAry as $key=>$value)
                        {
                            $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                        }
                    }  
                    $file_log_query = rtrim($file_log_query,",");   
                    $kBooking->updateBookingFileDetails($file_log_query,$idBookingFile,true); 


                    $replace_ary = array();

                    $idForwarder = $bookingDataArr['idForwarder'] ;
                    $customerServiceEmailAry = $kForwarder->getForwarderCustomerServiceEmail($idForwarder);
                    $iBookingLanguage = $bookingDataArr['iBookingLanguage'] ;
                    //Send Bank Transfer recieved success email to user				
                    $szOriginCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idOriginCountry'],false,$iBookingLanguage);
                    $szDestinationCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idDestinationCountry'],false,$iBookingLanguage) ;

                    $szOriginCountry = $szOriginCountryAry[$bookingDataArr['idOriginCountry']]['szCountryName'] ;
                    $szDestinationCountry = $szDestinationCountryAry[$bookingDataArr['idDestinationCountry']]['szCountryName'] ;
                    if($bookingDataArr['iInsuranceIncluded']==1)
                    {
                        $fTotalBookingCost = ($bookingDataArr['fTotalPriceCustomerCurrency'] + $bookingDataArr['fTotalInsuranceCostForBookingCustomerCurrency'] + $bookingDataArr['fTotalVat']);
                    }
                    else
                    {
                        $fTotalBookingCost = ($bookingDataArr['fTotalPriceCustomerCurrency'] + $bookingDataArr['fTotalVat']);
                    }

                    $idCurrency = $bookingDataArr['idCurrency'];
                    $currencyDetailsAry = array();
                    $currencyDetailsAry = $kBooking->loadCurrencyDetails($idCurrency);

                    $fTotalBookingCost = number_format_custom((float)$fTotalBookingCost,$iLanguage);

                    $replace_ary['szTotalAmount'] = $bookingDataArr['szCurrency']." ".$fTotalBookingCost;
                    $replace_ary['idUser'] = $bookingDataArr['idUser'];
                    $replace_ary['szFirstName'] = $bookingDataArr['szFirstName'];
                    $replace_ary['szLastName'] = $bookingDataArr['szLastName'];
                    $replace_ary['szEmail'] = $bookingDataArr['szEmail'];
                    $replace_ary['szForwarderDispName'] = $bookingDataArr['szForwarderDispName'];
                    $replace_ary['szForwarderCustServiceEmail'] = $customerServiceEmailAry[0];
                    $replace_ary['szBookingRef'] = $bookingDataArr['szBookingRef'];
                    $replace_ary['szLatestPaymentDate'] =  $szBankTransferDueDateTime ;

                    $replace_ary['szBank'] = $currencyDetailsAry['szBankName'] ;
                    $replace_ary['szSortCode'] = $currencyDetailsAry['szSortCode'] ;
                    $replace_ary['szAccountNumber'] = $currencyDetailsAry['szAccountNumber'] ;
                    $replace_ary['szSwift'] = $currencyDetailsAry['szSwiftNumber'] ;
                    $replace_ary['szNameOnAccount'] = $currencyDetailsAry['szNameOnAccount'] ;
                    $replace_ary['szSuuportEmail'] = __STORE_SUPPORT_EMAIL__;
                    $replace_ary['szSupportPhone'] = $szCustomerCareNumer;  
                    $replace_ary['szOriginCFSContact'] = $bookingDataArr['szWarehouseFromContactPerson'];;
                    $replace_ary['szOriginCFSEmail'] = $bookingDataArr['szWarehouseFromEmail'];
                    $replace_ary['szShipperCompany'] = $bookingDataArr['szShipperCompanyName']; 
                    $replace_ary['szOriginCountry'] = $szOriginCountry;
                    $replace_ary['szDestinationCountry'] = $szDestinationCountry;
                    $replace_ary['iAdminFlag']=1; 
                    $replace_ary['idBooking'] = $idBooking;
                    $replace_ary['iBookingLanguage'] = $bookingDataArr['iBookingLanguage'] ;

                    if($bookingDataArr['iBookingType']==__BOOKING_TYPE_COURIER__)
                    {
                        //$replace_ary['szName']=$postSearchAry['szFirstName']." ".$postSearchAry['szLastName'];
                        //$replace_ary['szSenderEmail']=$postSearchAry['szShipperEmail'];
                        //createEmail(__COURIER_BOOKING_CONFIRMATION_AND_INVOICE_EMAIL__, $replace_ary,$replace_ary['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__,true,$postSearchAry['id']);
                        createEmail(__BOOKING_BY_BANK_TRANSFER_CONFIRMATION_EMAIL__, $replace_ary,$replace_ary['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__,true);
                    }
                    else
                    {

                        if($bookingDataArr['iBookingType']==__BOOKING_TYPE_RFQ__)
                        {  
                            createEmail(__MANUAL_BOOKING_BY_BANK_TRANSFER_CONFIRMATION_EMAIL__, $replace_ary,$replace_ary['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__,true);
                        }
                        else
                        { 
                            createEmail(__BOOKING_BY_BANK_TRANSFER_CONFIRMATION_EMAIL__, $replace_ary,$replace_ary['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__,true);
                        } 
                    }
                }
                else
                {
                    $dtResponseTime = $kBooking->getRealNow();
                    $idBookingFile = $bookingDataArr['idFile'];  
                    $fileLogsAry = array();
                    $fileLogsAry['szTransportecaStatus'] = "S6";
                    $fileLogsAry['szTransportecaTask'] = "T4";
                    $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
                    $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime; 
                    $fileLogsAry['iStatus'] = 6; 
                    $fileLogsAry['iClosed'] = 0; 

                    if(!empty($fileLogsAry))
                    {
                        foreach($fileLogsAry as $key=>$value)
                        {
                            $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                        }
                    } 
                    $file_log_query = rtrim($file_log_query,",");   
                    $kBooking->updateBookingFileDetails($file_log_query,$idBookingFile,true);
                    cPartner::$szDeveloperNotes .= ">. Booking File has been successfully updated as confirmed: ".print_R($fileLogsAry,true).", Going forward ".PHP_EOL;
                }
            }
        }
        else
        {
            return false;
        }
    }
    
    function getTransactionDetails($idForwarderTransaction)
    {
        if($idForwarderTransaction>0)
        {
            $query="
                SELECT
                    id,
                    idBooking,
                    idForwarder,
                    dtInvoiceOn,
                    iTransferConfirmed
                FROM
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."  AS b
                WHERE
                    id = '".(int)$idForwarderTransaction."' 
            ";
            //echo $query;
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $row = $this->getAssoc($result); 
                    return $row;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        } 
    }
    
    function getAllTranserEmailTobeSentTransactions()
    { 
        /*
        * iDebitCredit: 2 means 'Automatic Transfer' for Forwarder
        * iStatus: 1 means, 'Confirm Scheduled' button has not been clicked on Management > /forwarderTransfer/ screen
        */
        $query="
            SELECT
                *
            FROM
                ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."  AS b
            WHERE
                iTransferEmailTobeSent = '2' 
            AND
                iDebitCredit = '2'
            AND
                iStatus = '1'
        ";
        //echo $query;
        if($result=$this->exeSQL($query))
        {
            if($this->iNumRows>0)
            {
                $row = $this->getAssoc($result); 
                return $row;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        } 
    }
    
    function updateForwarderPaymentStatus($idForwarderTransaction)
    {
        if($idForwarderTransaction>0)
        {
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                SET
                    iTransferEmailTobeSent = '0'
                WHERE
                    id = ".(int)$idForwarderTransaction."
            ";  
            if($result=$this->exeSQL($query))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    
    function updateBookingDetails($res_ary,$idBooking)
    { 
        if(!empty($res_ary))
        {
            $update_query = ""; 
            foreach($res_ary as $key=>$value)
            {
                $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
            } 
            $update_query = rtrim($update_query,",");
            $kBooking = new cBooking();
            if($kBooking->updateDraftBooking($update_query,$idBooking))
            {
                return true;
            }
            else
            {
                return false;
            }
        } 
    }
    function set_idForwarderQuoteCurrency( $value )
    {
        $this->idForwarderQuoteCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idForwarderQuoteCurrency", t($this->t_base_cfs.'fields/forwarder_quote_currency'), 1, false, true );
    }
    function set_fForwarderTotalQuotePrice( $value )
    {
        $this->fForwarderTotalQuotePrice = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fForwarderTotalQuotePrice", t($this->t_base_cfs.'fields/fForwarderTotalQuotePrice'), false, false, true );
    }
    function set_fTotalVatForwarderCurrency( $value )
    {
        $this->fTotalVatForwarderCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fTotalVatForwarderCurrency", t($this->t_base_cfs.'fields/fTotalVatForwarderCurrency'), false, false, true );
    }
    function set_dtMarkupPaid( $value )
    {
        $this->dtMarkupPaid = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "dtMarkupPaid", t($this->t_base_cfs.'fields/dtMarkupPaid'), false, false, true );
    }
}