<?php
/**
 * This file is the container for import data related functionality.
 *
 * importdata.class.php
 *
 * @copyright Copyright (C) 2009 Cardrunners, LLC
 * @author Anil
 * @package Transportcea Development
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

class cDBImport extends cDatabase
{
	function __construct()
	{
		parent::__construct();
		// establish a Database connection.
		//$this->connect( __DBC_USER__, __DBC_PASSWD__, __DBC_SCHEMATA__, __DBC_HOST__);
		return true;
	}

	/**
	 * Importing Countries Data
	 *
	 * @param unknown_type $countriesArr
	 */
	function importCountriesData($countriesArr)
	{
		if(!empty($countriesArr))
		{
			$query = "TRUNCATE TABLE ".__DBC_SCHEMATA_COUNTRY__;
			
			$result = $this->exeSQL( $query );
			
			foreach($countriesArr as $countriesArrs)
			{   
				$query="
					INSERT INTO
						".__DBC_SCHEMATA_COUNTRY__."
					(
						szCountryName,
						szCountryISO, 
						fStandardTruckRate, 
						iInternationDialCode,
						szLatitude,
						szLongitude,
						iActive
					)
						VALUES
					(

						'".mysql_escape_custom($countriesArrs['szCountryName'])."',
						'".mysql_escape_custom($countriesArrs['szCountryISO'])."', 
						'".(float)$countriesArrs['fStandardTruckRate']."', 
						'".(int)$countriesArrs['iInternationDialCode']."',
						'".mysql_escape_custom($countriesArrs['szLatitude'])."',
						'".mysql_escape_custom($countriesArrs['szLongitude'])."',
						'".mysql_escape_custom($countriesArrs['iActive'])."'
					)
				";
				//echo "<br/>".$query."<br/>";
				$result = $this->exeSQL( $query );

			}
		}
	}
	
	/**
	 * Importing PostCode Data
	 *
	 * @param array $postcodeArr
	 */
	
	function importPostCodeData($file)
	{

		/*if(!empty($postcodeArr))
		{
			//$query = "TRUNCATE TABLE ".__DBC_SCHEMATA_POSTCODE__;
			
			//$result = $this->exeSQL( $query );
			
			
			
			foreach($postcodeArr as $postcodeArrs)
			{
				if(strtolower($postcodeArrs['szRegion1'])=='hong kong')
				{
					$query="
					SELECT
						id
					FROM	
						".__DBC_SCHEMATA_COUNTRY__."
					WHERE
						szCountryName='".mysql_escape_custom($postcodeArrs['szRegion1'])."'	
					";
				}
				else
				{
					$query="
					SELECT
						id
					FROM	
						".__DBC_SCHEMATA_COUNTRY__."
					WHERE
						szCountryISO='".mysql_escape_custom($postcodeArrs['idCountry'])."'	
					";
				}
				//echo $query."<br/>";
				if($result=$this->exeSQL($query))
				{
					$row=$this->getAssoc($result);
					$idCountry=$row['id'];
					if((int)$idCountry>0)
					{				
						$query="
							INSERT INTO
								".__DBC_SCHEMATA_POSTCODE__."
							(
								idCountry,
								szPostCode,
								szCity,
								szArea1,
								szArea2,
								szLat,
								szLng,
								szRegion1,
								szRegion2,
								szRegion3,
								szRegion4
								
							)
								VALUES
							(
								'".(int)$idCountry."',
								'".mysql_escape_custom($postcodeArrs['szPostCode'])."',
								'".mysql_escape_custom($postcodeArrs['szCity'])."',
								'".mysql_escape_custom($postcodeArrs['szArea1'])."',
								'".mysql_escape_custom($postcodeArrs['szArea2'])."',
								'".mysql_escape_custom($postcodeArrs['Lat'])."',
								'".mysql_escape_custom($postcodeArrs['Lng'])."',
								'".mysql_escape_custom($postcodeArrs['szRegion1'])."',
								'".mysql_escape_custom($postcodeArrs['szRegion2'])."',
								'".mysql_escape_custom($postcodeArrs['szRegion3'])."',
								'".mysql_escape_custom($postcodeArrs['szRegion4'])."'
								
							)
						";
						//echo $query."<br/>";
						$result = $this->exeSQL( $query );
					}
				}

			}
		}*/
		
		$query="
			LOAD DATA LOCAL INFILE '".$file."'
	        INTO TABLE ".__DBC_SCHEMATA_POSTCODE__."
	        CHARACTER SET UTF8
	        FIELDS
	            TERMINATED BY ';'
	            ENCLOSED BY '\"'
	        LINES
	            TERMINATED BY '\r\n'
	         IGNORE 1 LINES
	        (szCountry,szLanguage,PID,ISO2,szRegion1,szRegion2,szRegion3,szRegion4,szPostCode,szCity,szArea1,szArea2,szLat,szLng,szTimeZone,UTC,DST)
		";
		//echo $query;
		if($result = $this->exeSQL( $query ))
		{
			$query="
				UPDATE 
					".__DBC_SCHEMATA_POSTCODE__." AS pc
			    SET 
			    	pc.idCountry=(SELECT c.id FROM ".__DBC_SCHEMATA_COUNTRY__." AS c WHERE c.szCountryISO=pc.szCountry )
			    WHERE
			    	pc.idCountry='0'
			";
			//echo $query;
			$result = $this->exeSQL( $query );
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Importing Forwarders Data
	 *
	 * @param array $forwardersArr
	 */
	
	function importForwardersData($forwardersArr)
	{
		if(!empty($forwardersArr))
		{
			
			$query = "TRUNCATE TABLE ".__DBC_SCHEMATA_FORWARDERS__;
			
			$result = $this->exeSQL( $query );
			foreach($forwardersArr as $forwardersArrs)
			{
				
				$query="
					SELECT
						id
					FROM	
						".__DBC_SCHEMATA_COUNTRY__."
					WHERE
						szCountryName='".mysql_escape_custom($forwardersArrs['BankCountry'])."'	
					";
					if($result=$this->exeSQL($query))
					{
						$row=$this->getAssoc($result);
						$idBankCountry=$row['id'];
					}
					$query="
						INSERT INTO
							".__DBC_SCHEMATA_FORWARDERS__."
					    (
					    	szDisplayName,
					    	szLogo,
					    	szCompanyName,
					    	szAddress,
					    	szAddress2,
					    	szAddress3,
					    	szPostCode,
					    	szCity,
					    	szState,
					    	idCountry,
					    	szPhone,
					    	iExt,
					    	szVatRegistrationNum,
					    	szCompanyRegistrationNum,
					    	szLink,
					    	szBankName,
					    	szNameOnAccount,
					    	iAccountNumber,
					    	iSortCode,
					    	szIBANAccountNumber,
					    	szCurrency,
					    	iActive,
					    	idBankCountry,
					    	dtCreateOn,
					    	dtUpdateOn
					    	
					    )
					    	VALUES
					    (
					    	'".mysql_escape_custom($forwardersArrs['displayName'])."',
					    	'".mysql_escape_custom($forwardersArrs['Logo'])."',
					    	'".mysql_escape_custom($forwardersArrs['companyName'])."',
					    	'".mysql_escape_custom($forwardersArrs['AddressLine1'])."',
					    	'".mysql_escape_custom($forwardersArrs['AddressLine2'])."',
					    	'".mysql_escape_custom($forwardersArrs['AddressLine3'])."',
					    	'".mysql_escape_custom($forwardersArrs['Postcode'])."',
					    	'".mysql_escape_custom($forwardersArrs['City'])."',
					    	'".mysql_escape_custom($forwardersArrs['State'])."',
					    	'".(int)$forwardersArrs['idCountry']."',
					    	'".mysql_escape_custom($forwardersArrs['PhoneNumber'])."',
					    	'".mysql_escape_custom($forwardersArrs['Extension'])."',
					    	'".mysql_escape_custom($forwardersArrs['VATNumber'])."',
					    	'".mysql_escape_custom($forwardersArrs['CompanyNumber'])."',
					    	'".mysql_escape_custom($forwardersArrs['StandardTerms'])."',
					    	'".mysql_escape_custom($forwardersArrs['BankName'])."',
					    	'".mysql_escape_custom($forwardersArrs['NameOnAccount'])."',
					    	'".mysql_escape_custom($forwardersArrs['AccountNumber'])."',
					    	'".mysql_escape_custom($forwardersArrs['SortCode'])."',
					    	'".mysql_escape_custom($forwardersArrs['IBANAccountNumber'])."',
					    	'".mysql_escape_custom($forwardersArrs['CurrencyID'])."',
					    	'".mysql_escape_custom($forwardersArrs['Active'])."',
					    	'".mysql_escape_custom($idBankCountry)."',
					    	NOW(),
					    	NOW()
					    )
					";
					//echo $query."<br/>";
					$result=$this->exeSQL($query);
			}
		}
	}
	
	/**
	 * Importing Forwarders Contact Details
	 *
	 * @param $forwardersContactArr
	 */
	function importForwardersContactData($forwardersContactArr)
	{
		if(!empty($forwardersContactArr))
		{
			$query = "TRUNCATE TABLE ".__DBC_SCHEMATA_FORWARDERS_CONTACT__;
			
			$result = $this->exeSQL( $query );
			
			foreach($forwardersContactArr as $forwardersContactArrs)
			{
				$query="
					INSERT INTO
						".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
					(
						idForwarder,
						szFirtName,
						szLastName,
						idCountry,
						szPhone,
						szMobile,
						szEmail,
						idForwarderContactRole,
						szPassword						
					)
						VALUES
					(
						'".(int)$forwardersContactArrs['ForwarderId']."',
						'".mysql_escape_custom($forwardersContactArrs['Fname'])."',
						'".mysql_escape_custom($forwardersContactArrs['Lname'])."',
						'".(int)$forwardersContactArrs['CountryID']."',
						'".mysql_escape_custom($forwardersContactArrs['LPN'])."',
						'".mysql_escape_custom($forwardersContactArrs['MPN'])."',
						'".mysql_escape_custom($forwardersContactArrs['Email'])."',
						'".(int)$forwardersContactArrs['RoleID']."',
						'".mysql_escape_custom($forwardersContactArrs['Password'])."'
					)
				
				";
				//echo $query."<br/>";
				$result=$this->exeSQL($query);
			}
		}
	}
	
	/**
	 * Import WareHouse Data
	 *
	 * @param array $wareHouseArr
	 */
	
	function importWareHouseData($wareHouseArr)
	{
		if(!empty($wareHouseArr))
		{
			$query = "TRUNCATE TABLE ".__DBC_SCHEMATA_WAREHOUSES__;
			
			$result = $this->exeSQL( $query ); 
			
			foreach($wareHouseArr as $wareHouseArrs)
			{
				$query="
                                    INSERT INTO
                                        ".__DBC_SCHEMATA_WAREHOUSES__."
                                    (
                                        idForwarder,
                                        szWareHouseName,
                                        szAddress,
                                        szAddress2,
                                        szAddress3,
                                        szPostCode,
                                        szCity,
                                        szState,
                                        idCountry,
                                        szPhone,
                                        szLatitude,
                                        szLongitude,
                                        szUTCOffset,
                                        iActive,
                                        iWarehouseType,
                                        dtCreatedOn,
                                        dtUpdatedOn
                                    )
                                    VALUES
                                    (
                                        '".(int)$wareHouseArrs['ForwarderID']."',
                                        '".mysql_escape_custom($wareHouseArrs['WarehouseName'])."',
                                        '".mysql_escape_custom($wareHouseArrs['AddressLine1'])."',
                                        '".mysql_escape_custom($wareHouseArrs['AddressLine2'])."',
                                        '".mysql_escape_custom($wareHouseArrs['AddressLine3'])."',
                                        '".mysql_escape_custom($wareHouseArrs['Postcode'])."',
                                        '".mysql_escape_custom($wareHouseArrs['City'])."',
                                        '".mysql_escape_custom($wareHouseArrs['State'])."',
                                        '".(int)$wareHouseArrs['CountryID']."',
                                        '".mysql_escape_custom($wareHouseArrs['PhoneNumber'])."',
                                        '".mysql_escape_custom($wareHouseArrs['Latitude'])."',
                                        '".mysql_escape_custom($wareHouseArrs['Longitude'])."',
                                        '".mysql_escape_custom($wareHouseArrs['UTCOffsetMs'])."',
                                        '".mysql_escape_custom($wareHouseArrs['Active'])."',
                                        '".__WAREHOUSE_TYPE_CFS__."',
                                        NOW(),
                                        NOW()
                                    )			
				";
				//echo $query."<br/>";
				$result=$this->exeSQL($query);
			}
		}
	}
	
	/**
	 * Importing Warehouse Pricing Data
	 *
	 * @param array $wareHousePricingArr
	 */
	
	function importWareHousePricingData($wareHousePricingArr)
	{
		if(!empty($wareHousePricingArr))
		{
			$query = "TRUNCATE TABLE ".__DBC_SCHEMATA_WAREHOUSES_PRICING__;
			
			$result = $this->exeSQL( $query ); 
			
			foreach($wareHousePricingArr as $wareHousePricingArrs)
			{
				$expiryDate='';
				$validFromDate='';
				$expiryDate=date('Y-m-d',strtotime($wareHousePricingArrs['ExpiryDate']));
				$validFromDate=date('Y-m-d H:i',strtotime($wareHousePricingArrs['ValidFromDate']));
				
				$query="
					INSERT INTO
						".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
					(
						idWarehouseFrom,
						idWarehouseTo,
						iCutOffHours,
						idCutOffDay,
						szCutOffLocalTime,
						idAvailableDay,
						szAvailableLocalTime,
						iTransitDay,
						idFrequency,
						fFreightRateWM,
						fMinRateWM,
						fRate,
						szFreightCurrency,
						dtValidFrom,
						dtExpiry
					)
						VALUES
					(
						'".(int)$wareHousePricingArrs['FromWarehouseID']."',
						'".(int)$wareHousePricingArrs['ToWarehouseID']."',
						'".mysql_escape_custom($wareHousePricingArrs['BookingCutoffHours'])."',
						'".mysql_escape_custom($wareHousePricingArrs['CutOffDayID'])."',
						'".mysql_escape_custom($wareHousePricingArrs['CutOffLocalTime'])."',
						'".mysql_escape_custom($wareHousePricingArrs['AvailableDayID'])."',
						'".mysql_escape_custom($wareHousePricingArrs['AvailableLocalTime'])."',
						'".mysql_escape_custom($wareHousePricingArrs['TransitDays'])."',
						'".mysql_escape_custom($wareHousePricingArrs['FrequencyID'])."',
						'".mysql_escape_custom($wareHousePricingArrs['RateWM'])."',
						'".mysql_escape_custom($wareHousePricingArrs['MinimumRateWM'])."',
						'".mysql_escape_custom($wareHousePricingArrs['RatePerBooking'])."',
						'".mysql_escape_custom($wareHousePricingArrs['FreightCurrency'])."',
						'".mysql_escape_custom($validFromDate)."',
						'".mysql_escape_custom($expiryDate)."'						
					)
				";
				//echo $query."<br/>";
				$result=$this->exeSQL($query);
			}
		}
	}
	
	/**
	 * Importing Pricing Data
	 *
	 * @param array $pricingccdatasArr
	 */
	function importPricingCCData($pricingccdatasArr)
	{
		if(!empty($pricingccdatasArr))
		{
			
			$query = "TRUNCATE TABLE ".__DBC_SCHEMATA_PRICING_CC__;
			
			$result = $this->exeSQL( $query ); 
			foreach($pricingccdatasArr as $pricingccdatasArrs)
			{
				$updatedDate=date('Y-m-d H:i',strtotime($pricingccdatasArrs['Updated']));
				$query="
					INSERT INTO
						".__DBC_SCHEMATA_PRICING_CC__."
					(
						idWarehouseFrom,
						idWarehouseTo,
						szOriginDestination,
						fPrice,
						szCurrency, 
						dtUpdateOn,
						iActive
					)
						VALUES
					(
						'".(int)$pricingccdatasArrs['FromWarehouseID']."',
						'".(int)$pricingccdatasArrs['ToWarehouseID']."',
						'".mysql_escape_custom($pricingccdatasArrs['OriginDestinationID'])."',
						'".(float)$pricingccdatasArrs['Price']."',
						'".mysql_escape_custom($pricingccdatasArrs['Currency'])."',
						'".mysql_escape_custom($updatedDate)."',
						'".(int)$pricingccdatasArrs['Active']."'
						
					)
					";
				//echo $query."<br/>";
				$result=$this->exeSQL($query);
			}
		}
	}
	
	function importPricingHaulageData($pricinghaulageArr)
	{
		if(!empty($pricinghaulageArr))
		{
			$query = "TRUNCATE TABLE ".__DBC_SCHEMATA_PRICING_HAULAGE__;
			
			$result = $this->exeSQL( $query ); 
			foreach($pricinghaulageArr as $pricinghaulageArrs)
			{
				$expiryDate='';
				$validFromDate='';
				$expiryDate=date('Y-m-d',strtotime($pricinghaulageArrs['ExpiryDate']));
				$updatedDate=date('Y-m-d H:i',strtotime($pricinghaulageArrs['Updated']));
				$query="
					INSERT INTO
						".__DBC_SCHEMATA_PRICING_HAULAGE__."
					(
						idWarehouse,
						idDirection,
						iUpToKM,
						idHaulageTransitTime, 
						fRateWM_Km,
						fMinRateWM_Km, 
						fRate, 
						szHaulageCurrency,
						iCrossBorder,
						dtUpdatedOn,
						dtExpiry
					)
						VALUES
					(
						'".(int)$pricinghaulageArrs['WarehouseID']."',
						'".(int)$pricinghaulageArrs['DirectionID']."',
						'".mysql_escape_custom($pricinghaulageArrs['UpToKm'])."',
						'".mysql_escape_custom($pricinghaulageArrs['HaulageTransitTimeID'])."',
						'".mysql_escape_custom($pricinghaulageArrs['RateWM_Km'])."',
						'".mysql_escape_custom($pricinghaulageArrs['MinimumRateWM_Km'])."',
						'".mysql_escape_custom($pricinghaulageArrs['RatePerBooking'])."',
						'".mysql_escape_custom($pricinghaulageArrs['HaulageCurrency'])."',
						'".mysql_escape_custom($pricinghaulageArrs['CrossBorder'])."',
						'".mysql_escape_custom($updatedDate)."',
						'".mysql_escape_custom($expiryDate)."'
						
					)
					";
				//echo $query."<br/>";
				$result=$this->exeSQL($query);
			}
		}
	}
	
	function importCurrencyData($data)
	{
		if(!empty($data))
		{
			$query="
				TRUNCATE TABLE ".__DBC_SCHEMATA_CURRENCY__;
			if($result = $this->exeSQL($query))
			{
			
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
			
			foreach($data as $currencys)
			{
				$query="
					INSERT INTO
						".__DBC_SCHEMATA_CURRENCY__."
					(
						szCurrency,
						iPricing,
						iBooking,
						szFeed
					)
					VALUES
					(
						'".mysql_escape_custom(trim($currencys['szCurrency']))."',
						'".mysql_escape_custom(trim($currencys['iPricing']))."',
						'".mysql_escape_custom(trim($currencys['iBooking']))."',
						'".mysql_escape_custom(trim($currencys['szFeed']))."'
					)
				";
				//echo "<br>".$query."<br>";
				
				if($result = $this->exeSQL($query))
				{
				
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
			}
		}
	}
        
        
        /**
	 * Importing Standar Cargo Details
	 *
	 * @param $standardCargoArr
	 */
	function importStandardCargoData($standardCargoArr)
	{
		if(!empty($standardCargoArr))
		{
			//$query = "TRUNCATE TABLE ".__DBC_SCHEMATA_STANDARD_CARGO__;
			
			//$result = $this->exeSQL( $query );
			
			foreach($standardCargoArr as $standardCargoArrs)
			{
				$query="
					INSERT INTO
						".__DBC_SCHEMATA_STANDARD_CARGO__."
					(
						idSearchMini,
						idCategory,
						szSellerReference,
						szShortName,
						szLongName,
						szShortNameDanish,
						szLongNameDanish,
						fVolume,
						fWeight,					
						fLength,					
						fWidth,					
						fHeight,					
						iColli,						
						dtCreatedOn,
                                                iActive,
                                                szShortNameSwedish,
                                                szLongNameSwedish,
                                                szShortNameNorwegian,
                                                szLongNameNorwegian
					)
						VALUES
					(
						'".(int)$standardCargoArrs['idSearchMini']."',
						'".(int)$standardCargoArrs['idCategory']."',
						'".mysql_escape_custom($standardCargoArrs['szSellerReference'])."',
						'".mysql_escape_custom($standardCargoArrs['szShortName'])."',
						'".mysql_escape_custom($standardCargoArrs['szLongName'])."',
						'".mysql_escape_custom($standardCargoArrs['szShortNameDanish'])."',
						'".mysql_escape_custom($standardCargoArrs['szLongNameDanish'])."',
						'".(float)$standardCargoArrs['fVolume']."',
						'".(float)$standardCargoArrs['fWeight']."',
						'".(float)$standardCargoArrs['fLength']."',
						'".(float)$standardCargoArrs['fWidth']."',
						'".(float)$standardCargoArrs['fHeight']."',
						'".(int)$standardCargoArrs['iColli']."',
                                                NOW(),
						'".(int)$standardCargoArrs['iActive']."',
						'".mysql_escape_custom($standardCargoArrs['szShortNameSwedish'])."',
						'".mysql_escape_custom($standardCargoArrs['szLongNameSwedish'])."',
						'".mysql_escape_custom($standardCargoArrs['szShortNameNorwegian'])."',
						'".mysql_escape_custom($standardCargoArrs['szLongNameNorwegian'])."'
					)
				
				";
				//echo $query."<br/>";
				$result=$this->exeSQL($query);
			}
		}
	}
        /**
	 * Importing Standar Cargo Category Details
	 *
	 * @param $standardCargoArr
	 */
	function importStandardCargoCategoryData($standardCargoCategoryArr)
	{
		if(!empty($standardCargoCategoryArr))
		{
			//$query = "TRUNCATE TABLE ".__DBC_SCHEMATA_STANDARD_CARGO_CATEGORY__;
			
			//$result = $this->exeSQL( $query );
			
			foreach($standardCargoCategoryArr as $standardCargoCategoryArrs)
			{
				$query="
					INSERT INTO
						".__DBC_SCHEMATA_STANDARD_CARGO_CATEGORY__."
					(
						szCategoryName,
						szCategoryNameDanish,
						iActive,
						dtCreatedOn,
						isDeleted
					)
						VALUES
					(
						'".mysql_escape_custom($standardCargoCategoryArrs['szCategoryName'])."',
						'".mysql_escape_custom($standardCargoCategoryArrs['szCategoryNameDanish'])."',
						'".(int)$standardCargoCategoryArrs['iActive']."',
                                                NOW(),
						'".(int)$standardCargoArrs['isDeleted']."'
					)
				
				";
				//echo $query."<br/>";
				$result=$this->exeSQL($query);
			}
		}
	}
	
    function deleteidSearchMiniCargoData()
    {
        $query="
            SELECT
                id
            FROM
                ".__DBC_SCHEMATA_STANDARD_CARGO__."
            WHERE
                idSearchMini='3'
            ";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            if($this->getRowCnt()>0)
            {
                while($row = $this->getAssoc($result))
                {
                    $queryDelete="DELETE FROM
                        ".__DBC_SCHEMATA_LANGUAGE_CONFIGURATION__."
                    WHERE
                        idMapped='".(int)$row['id']."'
                    AND
                        szTableKey='__TABLE_STANDARD_CARGO__'
                    ";
                    echo $queryDelete."<br />";
                    $resultDelete = $this->exeSQL( $queryDelete );
                    
                    
                    $queryDeleteCargo="DELETE FROM
                        ".__DBC_SCHEMATA_STANDARD_CARGO__."
                    WHERE
                        id='".(int)$row['id']."'
                    AND
                        idSearchMini='3'
                    ";
                    echo $queryDelete."<br />";
                    $resultDeleteCargo = $this->exeSQL( $queryDeleteCargo );
                }
            }
        }
    }    
    
    function deactivateDuplicateLines()
    {
        $query="
            SELECT 
                * , count( * ) AS total
            FROM 
                ".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
             WHERE
                iActive='1'        
            GROUP BY 
                `idWarehouseFrom` , `idWarehouseTo`           
            HAVING 
                total >1
            ORDER BY
                `total` ASC ";
        echo $query."<br />";
        if($result = $this->exeSQL($query))
        {
            if($this->iNumRows>0)
            { 
                while($row=$this->getAssoc($result))
                {
                    $querySel="
                        SELECT 
                            id
                        FROM 
                            ".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
                        
                        WHERE
                            iActive='1'
                        AND
                            idWarehouseFrom='".$row['idWarehouseFrom']."'
                        AND
                            idWarehouseTo='".$row['idWarehouseTo']."'    
                        ORDER BY
                            id DESC
                         ";
                    echo $querySel."<br />";
                         if($resultSel = $this->exeSQL($querySel))
                        {
                            if($this->iNumRows>0)
                            { 
                                $i=0;
                                $ctr=0;
                                while($rowSel=$this->getAssoc($resultSel))
                                {
                                    if($i>0)
                                    {
                                        $ressArr[$ctr]=$rowSel['id'];
                                        ++$ctr;
                                    }
                                    ++$i;
                                }
                                if(!empty($ressArr))
                                {
                                    $ressArrStr=implode(",",$ressArr);
                                    
                                    $queryUpdate="
                                        UPDATE
                                            ".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
                                         SET
                                            iActive='0'
                                         WHERE
                                            id IN (".$ressArrStr.")
                                        ";
                                    echo $queryUpdate."<br />";
                                        $resultUpdate = $this->exeSQL($queryUpdate);
                                }
                            }
                        }
                }
            }
        }

    }
    
    
    function importForwarderContactData($data)
    {
        $query="
            INSERT INTO 
                    ".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
            (
                idForwarder,
                szFirstName,
                szLastName,
                idCountry,
                szPhone,
                szMobile,
                szEmail,
                szResponsibility,    
                idForwarderContactRole,
                iActive,
                dtCreateOn
            )
            VALUES
            (
                '".(int)$data['idForwarder']."',
                '".mysql_escape_custom(trim(utf8_encode($data['szFirstName'])))."',
                '".mysql_escape_custom(trim(utf8_encode($data['szLastName'])))."',
                '".(int)$data['idCountry']."',
                '".mysql_escape_custom(trim($data['szPhone']))."',
                '".mysql_escape_custom(trim($data['szMobile']))."',
                '".mysql_escape_custom(trim(utf8_encode($data['szEmail'])))."',
                '".mysql_escape_custom(trim(utf8_encode($data['szResponsibility'])))."',    
                '".(int)$data['idForwarderContactRole']."',
                '".(int)$data['iActive']."',
                NOW()
           )	
        ";
        echo $query."<br /><br />";
        $result = $this->exeSQL($query);
    }
    
    function updateVogoBookingFlagInTrackerTable()
    {
        $query="
            SELECT
                b.id
            FROM
                ".__DBC_SCHEMATA_BOOKING__." AS b
            INNER JOIN
                ".__DBC_SCHEMATA_BOOKING_QUOTES__." AS bq
            ON
                bq.idBooking=b.id
            WHERE
                b.iStandardPricing='1'
            AND
                bq.iAutoQuote='1'
        ";
        echo $query;
        if($result = $this->exeSQL($query))
        {
            if($this->getRowCnt()>0)
            {
                $ret_ary = array();
                $ctr = 1;
                while($row = $this->getAssoc($result))
                { 
                    $idBooking=$row['id'];
                    $updateQuery="
                        UPDATE
                            ".__DBC_SCHEMATA_RESPONSE_TIME_TRACKER__."
                        SET
                            iSearchType='3'
                        WHERE    
                            idBooking='".(int)$idBooking."'
                        ";
                    echo $updateQuery."<br /><br />";
                    $result_update = $this->exeSQL($updateQuery);
                }
            }
        }
    }
    
    
    function importCourierDeliveryTime($data)
    {

        $query="
            INSERT INTO
                ".__DBC_SCHEMATA_COURIER_DELIVERY_TIME__."
            (
                idCourierProvider,
                idCountry,
                szPostcode,
                iFromHour,
                iFromMinute,
                iToHour,
                iToMinute
            )
                VALUES
            (
                '".(int)$data['iCourierID']."',
                '".(int)$data['iCountry']."',
                '".mysql_escape_custom($data['szPostcode'])."',
                '".(int)$data['iFromHour']."',
                '".(int)$data['iFromMinute']."',
                '".(int)$data['iToHour']."',
                '".(int)$data['iToMinute']."'    
            )
        ";
        echo $query."<br /><br />";
        if($result = $this->exeSQL( $query ))
        {
                return true;
        }
        else
        {
                return false;
        }
    }

}
?>