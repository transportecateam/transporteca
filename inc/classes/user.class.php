<?php
/**
 * This file is the container for all user related functionality.
 * All functionality related to user details should be contained in this class.
 *
 * user.class.php
 *
 * @copyright Copyright (C) 2012 Transporteca, LLC
 * @author Anil
 * @package Transporteca Development
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_INC__ . "/functions.php" );

class cUser extends cDatabase
{
    /**
     * These are the variables for each user.
     */

    var $id;
    var $szFirstName;
    var $szLastName;
    var $szEmail;
    var $szPassword;
    var $szRetypePassword;
    var $szPhoneNumber;
    var $szCountry;
    var $szCurrency;
    var $szAddress1;
    var $szAddress2;
    var $szAddress3;
    var $szCompanyName;
    var $szCompanyRegNo;
    var $szState;
    var $szCity;
    var $szPostcode;
    var $idGroup;
    var $iAcceptNewsUpdate;
    var $dtCreateOn;
    var $iActive;
    var $szConfirmKey;
    var $iConfirmed;
    var $szCountryName;
    var $szCurrencyName;
    var $szConNewPassword;
    var $szOldPassword;
    var $iInvited;
    var $t_base='Users/AccountPage/';
    var $t_base_access='Users/MultiUserAccess/';
    var $t_base_delete_account='Users/DeleteAccount/';
    var $iAlreadyMapperUserError;
    var $iPasswordUpdated;
    var $szEmailNotExists;
    var $idUserForSignUpApi=0;
  
	/**
	 * class constructor for php5
	 *
	 * @return bool
	 */
	function __construct()
	{
		parent::__construct();

		// establish a Database connection.
		//$this->connect( __DBC_USER__, __DBC_PASSWD__, __DBC_SCHEMATA__, __DBC_HOST__);
		return true;
	}
	function createAccount($data,$added_from=false,$bApiLogin=false)
	{
            if(is_array($data))
            {		
                $rquired_flag = true;
                if($added_from=='BOOKING_PROCESS' || $added_from=='BOOKING_QUOTE_PROCESS' || $added_from=='LABEL_DOWNLOAD_PROCESS' || $added_from=='PARTNER_API')
                {
                    $rquired_flag=false;
                } 
                if($added_from=='RFQ_VERIFY_BUTTON' || $bApiLogin || $added_from=='QUICK_QUOTE')
                {
                    $required_all_flag = false;
                    $rquired_flag=false;
                }  
                
                $this->set_szFirstName(trim(sanitize_all_html_input($data['szFirstName'])),$required_all_flag);
                $this->set_szLastName(trim(sanitize_all_html_input($data['szLastName'])),$required_all_flag);
                $this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szEmail']))),$required_all_flag);
                $this->set_szPassword(trim(sanitize_all_html_input($data['szPassword'])),$required_all_flag);
                $this->set_szRetypePassword(trim(sanitize_all_html_input($data['szConfirmPassword'])),$required_all_flag);
                $this->set_szCity(trim(sanitize_all_html_input($data['szCity'])),$rquired_flag);
                $this->set_szState(trim(sanitize_all_html_input($data['szState'])),$rquired_flag);
                $this->set_szCountry(trim(sanitize_all_html_input($data['szCountry'])),$rquired_flag);
                $this->set_szCompanyRegNo(trim(sanitize_all_html_input($data['szCompanyRegNo'])),$rquired_flag);
                
                $this->set_szCompanyName(trim(sanitize_all_html_input($data['szCompanyName'])),$rquired_flag);

                if($added_from=='BOOKING_PROCESS' || $added_from=='BOOKING_QUOTE_PROCESS' || $added_from=='RFQ_VERIFY_BUTTON' || $added_from=='PARTNER_API' || $added_from=='QUICK_QUOTE')
                {
                    $this->set_szPhoneNumber(trim(sanitize_all_html_input($data['szPhoneNumberUpdate'])),$required_all_flag);
                }
                else
                {
                    
                    $this->set_szPhoneNumber(trim(sanitize_all_html_input(urldecode(base64_decode($data['szPhoneNumberUpdate'])))),$required_all_flag);
                } 
                $this->set_szAddress1(trim(sanitize_all_html_input($data['szAddress1'])),$rquired_flag);
                $this->set_szAddress2(trim(sanitize_all_html_input($data['szAddress2'])),$rquired_flag);
                $this->set_szAddress3(trim(sanitize_all_html_input($data['szAddress3'])),$rquired_flag);
                $this->set_szPostcode(trim(sanitize_all_html_input($data['szPostCode'])),$rquired_flag);
                $iEmailAlreadyChecked=false;
                if($data['iUpdateExistUserNotCreatedFromSignApi']==1 && $data['idUserForSignUpApi']>0)
                {
                    $iEmailAlreadyChecked=true;
                }
                if(!$iEmailAlreadyChecked)
                {
                    if($data['iIncompleteProfile']==1 && (int)$_SESSION['user_id']>0)
                    {
                        //check if the an user with the following email ID already exists
                        if(!empty($this->szEmail) && $this->isUserEmailExist($this->szEmail,$_SESSION['user_id']))
                        {
                            /*if($this->iIncompleteProfileExists)
                            {
                                $this->deactivateUserAccount($this->idIncompleteUser);
                            }
                            else {
                                $this->addError( "szEmail" , t($this->t_base.'messages/email_exists') );
                            }*/
                            //New Line Added 15/03/2015

                            $this->addError( "szEmail" , t($this->t_base.'messages/email_exists') );
                        }
                    }
                    else
                    {
                        //check if the an user with the following email ID already exists
                        if(!empty($this->szEmail) && $this->isUserEmailExist($this->szEmail) )
                        {
                            /*if($this->iIncompleteProfileExists)
                            {
                                $this->deactivateUserAccount($this->idIncompleteUser);
                            }
                            else 
                            {
                                $this->addError( "szEmail" , t($this->t_base.'messages/email_exists') );
                            }*/
                            //New Line Added 15/03/2015
                            $this->addError( "szEmail" , t($this->t_base.'messages/email_exists') );
                        }
                    }
                }
                if(!empty($this->szPassword) && $this->szPassword!=$this->szRetypePassword)
                {
                    $this->addError( "szConfirmPassword" , t($this->t_base.'messages/match_password') );
                }
                
                //exit if error exist.
                if ($this->error === true)
                {
                    return false;
                }
                $ConfirmKey=md5(date("Y-m-d h:i:s").random_number_ConfirmKey());

                if(!empty($this->szPhoneNumber))
                {
                    $phoneNumber = $this->szPhoneNumber;
                }
                $addUserFlag = false;
                if(($data['iIncompleteProfile']==1 && (int)$_SESSION['user_id']>0) || ($data['iUpdateExistUserNotCreatedFromSignApi']==1 && $data['idUserForSignUpApi']>0))
                {
                    if($data['idUserForSignUpApi']>0)
                    {
                        $idCustomer=$data['idUserForSignUpApi'];
                    }
                    else
                    {
                        $idCustomer=$_SESSION['user_id'];
                    }
                    $query = "
                        UPDATE
                            ".__DBC_SCHEMATA_USERS__."
                        SET
                            szCompanyName = '".mysql_escape_custom($this->szCompanyName)."',
                            szCompanyRegNo = '".mysql_escape_custom($this->szCompanyRegNo)."',
                            szFirstName = '".mysql_escape_custom(ucwords($this->szFirstName))."',
                            szLastName = '".mysql_escape_custom(ucwords($this->szLastName))."',
                            szAddress = '".mysql_escape_custom($this->szAddress1)."',
                            szAddress2 = '".mysql_escape_custom($this->szAddress2)."',
                            szAddress3 = '".mysql_escape_custom($this->szAddress3)."',
                            szCity = '".mysql_escape_custom($this->szCity)."',
                            szState = '".mysql_escape_custom($this->szState)."',
                            idCountry = '".mysql_escape_custom($this->szCountry)."',
                            szPhone = '".mysql_escape_custom($phoneNumber)."',
                            szPostCode = '".mysql_escape_custom($this->szPostcode)."',
                            szEmail = '".mysql_escape_custom($this->szEmail)."',
                            szCurrency = '".mysql_escape_custom($data['szCurrency'])."',
                            iAcceptNewsUpdate = '".(int)$data['iSendUpdate']."',
                            szPassword = '".mysql_escape_custom(md5($this->szPassword))."',
                            szConfirmKey = '".mysql_escape_custom($ConfirmKey)."',
                            idInternationalDialCode = '".mysql_escape_custom($data['idInternationalDialCode'])."',
                            dtCreateOn = NOW(),
                            iActive = '1',
                            dtConfirmationCodeSent = NOW(),
                            iIncompleteProfile = '0',
                            iConfirmed = '".(int)$data['iConfirmed']."',
                            iFirstTimePassword = '".(int)$data['iFirstTimePassword']."',
                            iPrivate = '".(int)$data['iPrivate']."',
                            idCustomerOwner = '".(int)$data['idCustomerOwner']."',
                            iLanguage = '".(int)$data['iLanguage']."',
                            iSignUp = '".(int)$data['iSignUp']."'      
                        WHERE
                            id = '".(int)$idCustomer."'
                        ";
                    }
                    else
                    {
                        if($added_from!='PARTNER_API')
                        {
                            if((int)$data['iSendUpdate']==0)
                            {
                                $data['iSendUpdate'] =1;
                            }
                        }
                        $addUserFlag = true;
                        $query="
                            INSERT INTO
                                ".__DBC_SCHEMATA_USERS__."
                            (
                                szCompanyName,
                                szCompanyRegNo,
                                szFirstName,
                                szLastName,
                                szAddress,
                                szAddress2,
                                szAddress3,
                                szCity,
                                szState,
                                idCountry,
                                szPhone,
                                szPostCode,
                                szEmail,
                                szCurrency,
                                idInternationalDialCode,
                                iAcceptNewsUpdate,
                                szPassword,
                                szConfirmKey,
                                dtCreateOn,
                                iActive,
                                dtConfirmationCodeSent,
                                iIncompleteProfile,
                                iConfirmed,
                                iFirstTimePassword,
                                iPrivate,
                                idCustomerOwner,
                                iLanguage,
                                iSignUp
                            )
                            VALUES
                            (
                                '".mysql_escape_custom($this->szCompanyName)."',
                                '".mysql_escape_custom($this->szCompanyRegNo)."',
                                '".mysql_escape_custom(ucwords($this->szFirstName))."',
                                '".mysql_escape_custom(ucwords($this->szLastName))."',
                                '".mysql_escape_custom($this->szAddress1)."',
                                '".mysql_escape_custom($this->szAddress2)."',
                                '".mysql_escape_custom($this->szAddress3)."',
                                '".mysql_escape_custom($this->szCity)."',
                                '".mysql_escape_custom($this->szState)."',
                                '".mysql_escape_custom($this->szCountry)."',
                                '".mysql_escape_custom($phoneNumber)."',
                                '".mysql_escape_custom($this->szPostcode)."',
                                '".mysql_escape_custom($this->szEmail)."',
                                '".mysql_escape_custom($data['szCurrency'])."',
                                '".mysql_escape_custom($data['idInternationalDialCode'])."',
                                '".(int)$data['iSendUpdate']."',
                                '".mysql_escape_custom(md5($this->szPassword))."',
                                '".mysql_escape_custom($ConfirmKey)."',
                                NOW(),
                                '1',
                                now(),
                                0,
                                '".(int)$data['iConfirmed']."',
                                '".(int)$data['iFirstTimePassword']."',
                                '".(int)$data['iPrivate']."',
                                '".(int)$data['idCustomerOwner']."',
                                '".(int)$data['iLanguage']."',
                                '".(int)$data['iSignUp']."'     
                            )
                        ";
                    }
                //echo "<br>".$query."<br> ";
                if( ( $result = $this->exeSQL( $query ) ) )
                {
                    if(($data['iIncompleteProfile']==1 && (int)$_SESSION['user_id']>0) || ($data['iUpdateExistUserNotCreatedFromSignApi']==1 && $data['idUserForSignUpApi']>0))
                    {
                        $this->id = (int)$idCustomer ;
                    }
                    else
                    {
                        $this->id = $this->iLastInsertID;
                        
                    }  
                    $idCustomer = $this->id; 
                    if($added_from=='PARTNER_API' || $added_from=='QUICK_QUOTE')
                    {
                        $this->idApiCustomer = $this->id;
                        return true;
                    }
                    else if($added_from=='BOOKING_PROCESS' || $added_from=='BOOKING_QUOTE_PROCESS' || $added_from=='RFQ_VERIFY_BUTTON' || $added_from=='LABEL_DOWNLOAD_PROCESS')
                    {
                        //In case when a user is created from booking process then we do not send E-mail for verification. 
                        $_SESSION['idUser']=$this->id;
                        $_SESSION['user_id']=$this->id; 
                        return true;
                    }
                    else
                    {		
                        if(__ENVIRONMENT__ == "LIVE")
                        {
                            setcookie("__USER_EMAIL_COOKIE__", $this->szEmail, time()+(3600*24*90),'/',__BASE_URL_SECURE__,true);
                        }
                        else
                        {
                            setcookie("__USER_EMAIL_COOKIE__", $this->szEmail, time()+(3600*24*90),'/');
                        } 
                        if($addUserFlag)
                        {
                            $kMautic = new cMautic(); 
                            //$kMautic->updateDataToMauticApi($idCustomer);
                        } 
                    
                        $_SESSION['idUser']=$this->id;
                        $_SESSION['user_id']=$this->id; 

                        $replace_ary['szFirstName']=$this->szFirstName;
                        $replace_ary['szEmail']=$this->szEmail;
                        $szBookingRandomNumber = trim($data['szBookingRandomNumber']);
                        $iLanguage = getLanguageId();
                        $LangUrl = '';
                        
                        $kConfig =new cConfig();
        		$langArr=$kConfig->getLanguageDetails('',$iLanguage,'','','','','',false);
                        if(!empty($langArr))
                        {
                            $LangUrl = "&lang=".  strtolower($langArr[0]['szName']);
                            //$szLinkText = 'KLIK FOR AT BEKRÆFTE DIN E-MAIL';
                        }
                        else
                        {
                            //$szLinkText = 'CLICK TO VERIFY E-MAIL ADDRESS';
                        }
                        $szLinkText=t($this->t_base.'fields/click_verify_link');
                        if(!empty($szBookingRandomNumber))
                        {
                            $confirmationLink=__MAIN_SITE_HOME_PAGE_SECURE_URL__."/confirmation.php?confirmationKey=".$ConfirmKey."&booking_random_key=".$szBookingRandomNumber.$LangUrl;
                            $confirmationLinkHttps=__MAIN_SITE_HOME_PAGE_SECURE_URL__."/confirmation.php?confirmationKey=".$ConfirmKey."&booking_random_key=".$szBookingRandomNumber.$LangUrl;
                        }
                        else
                        {
                            $confirmationLink=__MAIN_SITE_HOME_PAGE_SECURE_URL__."/confirmation.php?confirmationKey=".$ConfirmKey.$LangUrl;
                            $confirmationLinkHttps=__MAIN_SITE_HOME_PAGE_SECURE_URL__."/confirmation.php?confirmationKey=".$ConfirmKey.$LangUrl;
                        }					
                        $replace_ary['szLink']="<a href='".$confirmationLink."'>".$szLinkText."</a>";
                        $replace_ary['szHttpsLink']=$confirmationLinkHttps;
                        $replace_ary['supportEmail'] = __STORE_SUPPORT_EMAIL__; 
                        createEmail(__CREATE_USER_ACCOUNT__, $replace_ary,$this->szEmail, __CREATE_USER_ACCOUNT_SUBJECT__, __STORE_SUPPORT_EMAIL__,$this->id, __STORE_SUPPORT_EMAIL__,__FLAG_FOR_CUSTOMER__); 
                        //$this->checkIncompleteUserProfile($this->id); 
                        
                        $kRegisteredShipCon = new cRegisterShipCon();
                        $kRegisteredShipCon->updateCapsuleCrmDetails($this->id);
                    }
                    return true; 
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() requires an array.";
                $this->logError( "input", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
	
	function isUserEmailExist( $email,$idUser=0 )
	{
            $this->set_szEmail($email);

            $query="
                SELECT
                    id,
                    iIncompleteProfile,
                    iSignUp
                FROM
                    ".__DBC_SCHEMATA_USERS__."
                WHERE
                    szEmail = '".mysql_escape_custom($this->szEmail)."'
                AND
                    iActive ='1'
            ";
           // echo $query;
            //die;
            if((int)$idUser>0)
            {
                $query .="
                    AND
                        id<>'".(int)$idUser."'		
                ";
            }
            //echo "<br>".$query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
        	// Check if the user exists
                if( $this->getRowCnt() > 0 )
                {
                    while($row=$this->getAssoc($result))
                    {
                        $this->iIncompleteProfileExists = $row['iIncompleteProfile'];
                        $this->idIncompleteUser = $row['id']; 
                        if($idUser==0 && (int)$row['iSignUp']==0)
                        {
                            $this->idUserSignupApi = $row['id']; 
                        }
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
	
	function checkConfirmationKey($szConfirmationKey)
	{ 
            $query="
                SELECT
                    id,
                    szFirstName,
                    szLastName
                FROM
                    ".__DBC_SCHEMATA_USERS__."
                WHERE
                    szConfirmKey  = '".mysql_escape_custom($szConfirmationKey)."';
            ";
            //echo $query;;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
        	// Check if the user exists
                if( $this->getRowCnt() > 0 )
                {
                    $row=$this->getAssoc($result); 
                    
                    $_SESSION['idUser']=$row['id'];
                    if(($_SESSION['idUser']==$_SESSION['user_id']) || (int)$_SESSION['user_id']==0)
                    {
            		if((int)$_SESSION['user_id']>0)
            		{
                            $query="
                                UPDATE
                                    ".__DBC_SCHEMATA_USERS__."
                                SET
                                    iConfirmed='1',
                                    szConfirmKey='',
                                    dtConfirmationCodeSent = '',
                                    dtVerified=NOW()
                                WHERE
                                    szConfirmKey='".mysql_escape_custom($szConfirmationKey)."'
                            ";
                            $result = $this->exeSQL( $query );
                            $this->szFirstName=$row['szFirstName'];
                            $this->szLastName=$row['szLastName'];
                            
                            $kRegisterShipCon = new cRegisterShipCon();
                            $szCustomerHistoryNotes = "E-mail verified on ".date('d/m/Y H:i:s');
                            $kRegisterShipCon->updateCapsuleHistoryNotes($szCustomerHistoryNotes,$_SESSION['idUser']); 
            		}
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
	}
	
	function resendActivationCode($idUser,$szBookinRandomNum=false)
	{
		$query="
			SELECT
				id,
				szEmail,
				szFirstName
			FROM
			".__DBC_SCHEMATA_USERS__."
			WHERE
				id='".(int)$idUser."'
			
			";
		if( ( $result = $this->exeSQL( $query ) ) )
		{
        	
            if( $this->getRowCnt() > 0 )
            {
            	$row=$this->getAssoc($result);
            	$szEmail=$row['szEmail'];
            	$szFirstName=$row['szFirstName'];
            	$ConfirmKey=md5(date("Y-m-d h:i:s").random_number_ConfirmKey());
            	
            	$query="
            		UPDATE
            			".__DBC_SCHEMATA_USERS__."
            		SET
            			szConfirmKey='".mysql_escape_custom($ConfirmKey)."',
            			dtConfirmationCodeSent = now()
            		WHERE
            			id='".(int)$idUser."'
            	";
            	$result = $this->exeSQL( $query );
            	
            		$replace_ary['szFirstName']=$szFirstName;
					$replace_ary['szEmail']=$szEmail;
					
            		$iLanguage = getLanguageId();
					$LangUrl = '';
					 
            		$kConfig =new cConfig();
        		$langArr=$kConfig->getLanguageDetails('',$iLanguage,'','','','','',false);
                        if(!empty($langArr))
                        {
                            $LangUrl = "&lang=".  strtolower($langArr[0]['szName']);
                            //$szLinkText = 'KLIK FOR AT BEKRÆFTE DIN E-MAIL';
                        }
                        else
                        {
                            //$szLinkText = 'CLICK TO VERIFY E-MAIL ADDRESS';
                        }
                        $szLinkText=t($this->t_base.'fields/click_verify_link');
           			if(!empty($szBookinRandomNum))
					{
						$confirmationLink=__MAIN_SITE_HOME_PAGE_SECURE_URL__."/confirmation.php?confirmationKey=".$ConfirmKey."&booking_random_key=".$szBookinRandomNum.$LangUrl;
						$confirmationLinkHttps=__MAIN_SITE_HOME_PAGE_SECURE_URL__."/confirmation.php?confirmationKey=".$ConfirmKey."&booking_random_key=".$szBookinRandomNum.$LangUrl;
					}
					else
					{
						$confirmationLink=__MAIN_SITE_HOME_PAGE_SECURE_URL__."/confirmation.php?confirmationKey=".$ConfirmKey.$LangUrl;
						$confirmationLinkHttps=__MAIN_SITE_HOME_PAGE_SECURE_URL__."/confirmation.php?confirmationKey=".$ConfirmKey.$LangUrl;
					}
										
					$replace_ary['szLink']="<a href='".$confirmationLink."'>".$szLinkText."</a>";
					$replace_ary['szHttpsLink']=$confirmationLinkHttps;
                    $replace_ary['supportEmail'] = __STORE_SUPPORT_EMAIL__;
                    
					createEmail(__CREATE_USER_ACCOUNT__, $replace_ary,$szEmail, __CREATE_USER_ACCOUNT_SUBJECT__, __STORE_SUPPORT_EMAIL__,$idUser, __STORE_SUPPORT_EMAIL__,__FLAG_FOR_CUSTOMER__);
					//$this->checkIncompleteUserProfile($idUser);
                    return true;
                      
            }
            else
            {
            	return false;
            }
		}
		else
		{
			return false;
		}
	}
	
	function updateEmailAddress($data,$idUser)
	{
		$szPhoneNumber=urlencode(base64_decode($data['szPhoneNumber']));
		if(is_array($data) && (int)$idUser>0)
		{
			$this->set_szFirstName(trim(sanitize_all_html_input($data['szFirstName'])));
			$this->set_szLastName(trim(sanitize_all_html_input($data['szLastName'])));
			$this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szEmail']))));
			
			$this->set_szPassword(trim(sanitize_all_html_input($data['szPassword'])));
			$this->set_szRetypePassword(trim(sanitize_all_html_input($data['szConfirmPassword'])));
			
			$this->set_szCity(trim(sanitize_all_html_input($data['szCity'])));
			$this->set_szState(trim(sanitize_all_html_input($data['szState'])));
			$this->set_szCountry(trim(sanitize_all_html_input($data['szCountry'])));
			$this->set_szPhoneNumber(trim(sanitize_all_html_input(urldecode(base64_decode($data['szPhoneNumberUpdate'])))));
			$this->set_szCompanyName(trim(sanitize_all_html_input($data['szCompanyName'])));
			$this->set_szCompanyRegNo(trim(sanitize_all_html_input($data['szCompanyRegNo'])));
			$this->set_szAddress1(trim(sanitize_all_html_input($data['szAddress1'])));
			$this->set_szAddress2(trim(sanitize_all_html_input($data['szAddress2'])));
			$this->set_szAddress3(trim(sanitize_all_html_input($data['szAddress3'])));
			$this->set_szPostcode(trim(sanitize_all_html_input($data['szPostCode'])));
			$this->set_id($idUser);
			//check if the an user with the following email ID already exists
			if(!empty($this->szEmail) && $this->isUserEmailExist($this->szEmail,$this->id) )
			{
				$this->addError( "szEmail" , t($this->t_base.'messages/email_exists') );
			}
			
			if(!empty($this->szPassword) && $this->szPassword!=$this->szRetypePassword)
			{
				$this->addError( "szConfirmPassword" , t($this->t_base.'messages/match_password') );
			}
			
			//exit if error exist.
			if ($this->error === true)
			{
				return false;
			}
			
				
			/*if(!empty($this->szPhoneNumber)){
				$szPhoneNumber=ltrim($this->szPhoneNumber,"0");
				
				$countrydailcode=$this->getInternationalDailCode($this->szCountry);
				$codeLength=strlen($countrydailcode);
				$str = substr($szPhoneNumber, 0, $codeLength); 
				if($str==$countrydailcode)
				{
					$substr=substr($szPhoneNumber,$codeLength);
					$pnumber=ltrim($substr,"0");
					$szPhoneNumber=$countrydailcode."".$pnumber;
				}
				else
				{
					$szPhoneNumber=$countrydailcode."".$szPhoneNumber;
				}
			}*/
			//echo $phoneNumber;
			$ConfirmKey=md5(date("Y-m-d h:i:s").random_number_ConfirmKey());
            	
            	$query="
                    UPDATE
                        ".__DBC_SCHEMATA_USERS__."
                    SET
                        szConfirmKey='".mysql_escape_custom($ConfirmKey)."',
                        szEmail='".mysql_escape_custom($this->szEmail)."',
                        szCompanyName='".mysql_escape_custom($this->szCompanyName)."',
                        szCompanyRegNo='".mysql_escape_custom($this->szCompanyRegNo)."',
                        szFirstName='".mysql_escape_custom(ucwords($this->szFirstName))."',
                        szLastName='".mysql_escape_custom(ucwords($this->szLastName))."',
                        szAddress='".mysql_escape_custom($this->szAddress1)."',
                        szAddress2='".mysql_escape_custom($this->szAddress2)."',
                        szAddress3='".mysql_escape_custom($this->szAddress3)."',
                        szCity='".mysql_escape_custom($this->szCity)."',
                        szState='".mysql_escape_custom($this->szState)."',
                        idCountry='".mysql_escape_custom($this->szCountry)."',
                        szPhone='".mysql_escape_custom($this->szPhoneNumber)."',
                        szPostCode='".mysql_escape_custom($this->szPostcode)."',
                        szPassword='".mysql_escape_custom(md5($this->szPassword))."',
                        szCurrency='".mysql_escape_custom($data['szCurrency'])."',
                        iAcceptNewsUpdate='".(int)$data['iSendUpdate']."'
                    WHERE
                        id='".(int)$this->id."'
            	";
            	//echo $query;
            	$result = $this->exeSQL( $query );
            	
                $iLanguage = getLanguageId();
                $LangUrl = '';
                $kConfig =new cConfig();
                $langArr=$kConfig->getLanguageDetails('',$iLanguage,'','','','','',false);
                if(!empty($langArr))
                {
                    $LangUrl = "&lang=".  strtolower($langArr[0]['szName']);
                    //$szLinkText = 'KLIK FOR AT BEKRÆFTE DIN E-MAIL';
                }
                else
                {
                    //$szLinkText = 'CLICK TO VERIFY E-MAIL ADDRESS';
                }
                $szLinkText=t($this->t_base.'fields/click_verify_link');

                $replace_ary['szEmail']=$this->szEmail;
                $confirmationLink=__MAIN_SITE_HOME_PAGE_SECURE_URL__."/confirmation.php?confirmationKey=".$ConfirmKey.$LangUrl;
                $confirmationLinkHttps=__MAIN_SITE_HOME_PAGE_SECURE_URL__."/confirmation.php?confirmationKey=".$ConfirmKey.$LangUrl;
                $replace_ary['szLink']="<a href='".$confirmationLink."'>".$szLinkText."</a>";
                $replace_ary['szHttpsLink']=$confirmationLinkHttps;
                $replace_ary['supportEmail'] = __STORE_SUPPORT_EMAIL__; 
                createEmail(__CREATE_USER_ACCOUNT__, $replace_ary,$this->szEmail, __CREATE_USER_ACCOUNT_SUBJECT__, __STORE_SUPPORT_EMAIL__,$this->id, __STORE_SUPPORT_EMAIL__,__FLAG_FOR_CUSTOMER__);

                //$this->checkIncompleteUserProfile($this->id);
                $kRegisteredShipCon = new cRegisterShipCon();
                $kRegisteredShipCon->updateCapsuleCrmDetails($this->id);
                
                return true;
			
		}
		else
		{
			return false;
		}
	}
	
	function getInternationalDailCode($idCountry)
	{
		if((int)$idCountry>0)
		{
			$query="
				SELECT
					iInternationDialCode
				FROM
					".__DBC_SCHEMATA_COUNTRY__."
				WHERE
					id='".(int)$idCountry."'
			";
			if( ( $result = $this->exeSQL( $query ) ) )
			{
	        	// Check if the user exists
	            if( $this->getRowCnt() > 0 )
	            {
	            	$row=$this->getAssoc($result);
	            	return $row['iInternationDialCode'];
	            }
	            else
	            {
	            	return 0;
	            }
			}
		}
		else
		{
			return 0;
		}
	}
	
    function getUserDetails($idUser=false,$szCapsulePrtyID=false)
    {
        if((int)$idUser>0 || !empty($szCapsulePrtyID))
        {
            if($idUser>0)
            {
                $query_and = " WHERE u.id= '".(int)$idUser."' ";
            }
            /*else if(!empty($szCapsulePrtyID))
            {
                $query_and = " WHERE u.szCapsulePrtyID = '".mysql_escape_custom($szCapsulePrtyID)."' ";
            }*/
            else
            {
                return false;
            }
                    
            $query="
                SELECT
                    u.id,
                    u.idGroup,
                    u.szCompanyName,
                    u.szCompanyRegNo,
                    u.szFirstName,
                    u.szLastName,
                    u.szAddress,
                    u.szAddress2,
                    u.szAddress3,
                    u.szCity,
                    u.szState,
                    u.idCountry,
                    u.szPhone,
                    u.szPostCode, 
                    u.szEmail,
                    u.szCurrency,
                    u.iAcceptNewsUpdate,
                    u.szPassword,
                    u.iConfirmed,
                    u.szConfirmKey,
                    u.iActive,
                    u.dtCreateOn,
                    u.iInvited, 
                    u.dtConfirmationCodeSent,
                    u.iIncompleteProfile,
                    u.iPasswordUpdated,
                    u.szPassword,
                    u.iFirstTimePassword,
                    u.szUserPasswordKey,
                    u.idInternationalDialCode,
                    u.szCapsulePrtyID,
                    u.iPrivate,
                    u.idCustomerOwner,
                    u.iLanguage,
                    u.dtLastLogin,
                    u.szMauticAccountID,
                    u.szIpAddress,
                    u.szHttpReferrar,
                    u.szBrowser,
                    u.iSignUp,
                    (SELECT szCountryName FROM ".__DBC_SCHEMATA_COUNTRY__." c WHERE c.id=u.idCountry ) as szCountryName,
                    (SELECT c2.szCountryISO FROM ".__DBC_SCHEMATA_COUNTRY__." c2 WHERE c2.id=u.idCountry ) as szCountryISOName,
                    (SELECT iInternationDialCode FROM ".__DBC_SCHEMATA_COUNTRY__." c WHERE c.id=u.idInternationalDialCode ) as iInternationDialCode,
                    (SELECT cu.szCurrency FROM ".__DBC_SCHEMATA_CURRENCY__." cu WHERE cu.id=u.szCurrency ) as szCurrencyName,
                    (SELECT l.szName FROM ".__DBC_SCHEMATA_LANGUAGE__." l WHERE l.id=u.iLanguage ) as szCustomerLanguage    
                FROM
                    ".__DBC_SCHEMATA_USERS__." AS u  
                    $query_and
            ";
                    //echo $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                    // Check if the user exists
                if( $this->getRowCnt() > 0 )
                {
                    $row=$this->getAssoc($result);

                    $this->szEmail= trim(sanitize_all_html_input($row['szEmail']));
                    $this->id=$row['id'];
                    $this->idGroup=$row['idGroup'];
                    $this->szFirstName= trim(sanitize_all_html_input($row['szFirstName']));
                    $this->szLastName= trim(sanitize_all_html_input($row['szLastName'])) ;
                    $this->szCompanyName= trim(sanitize_all_html_input($row['szCompanyName']));
                    $this->szCompanyRegNo= trim(sanitize_all_html_input($row['szCompanyRegNo']));
                    $this->szAddress1= trim(sanitize_all_html_input($row['szAddress']));
                    $this->szAddress2=trim(sanitize_all_html_input($row['szAddress2']));
                    $this->szAddress3=trim(sanitize_all_html_input($row['szAddress3']));
                    $this->szCity= trim(sanitize_all_html_input($row['szCity']));
                    $this->szPostCode= trim(sanitize_all_html_input($row['szPostCode']));
                    $this->szState= trim(sanitize_all_html_input($row['szState']));
                    $this->szCountry= trim(sanitize_all_html_input($row['idCountry']));
                    $this->szPhoneNumber= trim(sanitize_all_html_input($row['szPhone']));
                    $this->szPostcode= trim(sanitize_all_html_input($row['szPostCode']));
                    $this->szCurrency= trim(sanitize_all_html_input($row['szCurrency']));
                    $this->iAcceptNewsUpdate=$row['iAcceptNewsUpdate'];
                    $this->iConfirmed=$row['iConfirmed'];
                    $this->szConfirmKey= trim(sanitize_all_html_input($row['szConfirmKey']));
                    $this->iActive=$row['iActive'];
                    $this->dtCreateOn=$row['dtCreateOn'];
                    $this->szCountryName= trim(sanitize_all_html_input($row['szCountryName']));
                    $this->szCurrencyName= trim(sanitize_all_html_input($row['szCurrencyName']));
                    $this->iInvited=$row['iInvited'];
                    $this->dtConfirmationCodeSent = $row['dtConfirmationCodeSent'];
                    $this->iIncompleteProfile = $row['iIncompleteProfile'];
                    $this->iPasswordUpdated = $row['iPasswordUpdated'];
                    $this->szPassword = $row['szPassword'];
                    $this->iFirstTimePassword = $row['iFirstTimePassword']; 
                    $this->szUserPasswordKey = trim(sanitize_all_html_input($row['szUserPasswordKey'])); 
                    $this->idInternationalDialCode = $row['idInternationalDialCode'];  
                    //$this->szCapsulePrtyID = trim(sanitize_all_html_input($row['szCapsulePrtyID']));   
                    $this->iPrivate = $row['iPrivate'];    
                    $this->idCustomerOwner = $row['idCustomerOwner'];    
                    $this->iLanguage = $row['iLanguage']; 
                    $this->dtLastLogin = $row['dtLastLogin'];  
                    $this->szMauticAccountID = sanitize_all_html_input($row['szMauticAccountID']); 
                    $this->szIpAddress = sanitize_all_html_input($row['szIpAddress']);
                    $this->szHttpReferrar = sanitize_all_html_input($row['szHttpReferrar']);
                    $this->szBrowser = sanitize_all_html_input($row['szBrowser']);
                    $this->szCountryISOName = sanitize_all_html_input($row['szCountryISOName']); 
                    $this->iSignUp = sanitize_all_html_input($row['iSignUp']);
                    $this->szCustomerLanguage = sanitize_all_html_input($row['szCustomerLanguage']);
                    return $row;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                    return array();
            }
        }
        else
        {
                return array();
        }
    }
    
    function getUserDetailsApi($idUser)
    {
        if($idUser>0)
        {
            $userDetailsAry = array();
            $userDetailsAry = $this->getUserDetails($idUser);
            if($userDetailsAry['iPrivate']==1)
            {
                $szPrivate = "Yes";
            }
            else
            {
                $szPrivate = "No";
            }
            /*if($userDetailsAry['iAcceptNewsUpdate']==1)
            {
                $iAcceptNewsUpdate = "true";
            }
            else
            {
                $iAcceptNewsUpdate = "false";
            }*/
            $kCustomerEvent = new cCustomerEevent();
            $szCustomerNotes = $kCustomerEvent->getLastBookingsDetailsByCustomer($idUser);
            $szLanguageName = getLanguageName($userDetailsAry['iLanguage']);
            
            $ret_ary = array();
            $ret_ary['szFirstName'] = cApi::encodeString($userDetailsAry['szFirstName']);
            $ret_ary['szLastName'] = cApi::encodeString($userDetailsAry['szLastName']); 
            $ret_ary['szEmail'] = cApi::encodeString($userDetailsAry['szEmail']); 
            $ret_ary['szPhone'] = cApi::encodeString($userDetailsAry['szPhone']);
            $ret_ary['iInternationDialCode'] = cApi::encodeString($userDetailsAry['iInternationDialCode']);
            $ret_ary['szCompanyName'] = cApi::encodeString($userDetailsAry['szCompanyName']);
            $ret_ary['szCompanyRegNo'] = cApi::encodeString($userDetailsAry['szCompanyRegNo']);
            $ret_ary['isPrivate'] = $szPrivate;
            $ret_ary['szAddress'] = cApi::encodeString($userDetailsAry['szAddress']);
            $ret_ary['szPostCode'] = cApi::encodeString($userDetailsAry['szPostCode']);
            $ret_ary['szCity'] = cApi::encodeString($userDetailsAry['szCity']);
            $ret_ary['szCountry'] = cApi::encodeString($userDetailsAry['szCountryISOName']);
            $ret_ary['szCurrency'] = cApi::encodeString($userDetailsAry['szCurrencyName']);
            $ret_ary['szLanguage'] = cApi::encodeString($szLanguageName); 
            $ret_ary['iAcceptNewsUpdate'] = $userDetailsAry['iAcceptNewsUpdate'];
            $ret_ary['szCustomerNotes'] = cApi::encodeString($szCustomerNotes); 
            return $ret_ary;
        } 
    }
	
	function loadCurrency($idCurrency)
	{
		if((int)$idCurrency>0)
		{
			$query="
				SELECT
					cu.id,
					cu.szCurrency,
					cu.iActive,
					cu.iSettling,
					cu.iBooking,
					cu.iPricing
				FROM
					".__DBC_SCHEMATA_CURRENCY__." AS cu
				WHERE
					cu.id='".(int)$idCurrency."'		
			";
			//echo $query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{
	        	// Check if the user exists
	            if( $this->getRowCnt() > 0 )
	            {
	            	$row=$this->getAssoc($result);
	            	
	            	$this->szCurrencyName = $row['szCurrency'];
	            	$this->iActiveCurrency =$row['iActive'];
	            	$this->iSettling=$row['iSettling'];
	            	$this->iBooking=$row['iBooking'];
	            	$this->iPricing=$row['iPricing'];
	            	return true;
	            }
	            else
	            {
	            	return array();
	            }
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function updateUserWithCurrency($idCurrency)
	{
		if(($idCurrency>0))
		{
			$currencyExchangeRateAry = array();
			if($idCurrency==1)
			{
				$currencyExchangeRateAry['idCurrency']=1;
				$currencyExchangeRateAry['szCurrency'] = 'USD';
				$currencyExchangeRateAry['fExchangeRate'] = 1;
			}
			else
			{
				$kWhsSearch = new cWHSSearch();
				$resultAry = $kWhsSearch->getCurrencyDetails($idCurrency);								
				$currencyExchangeRateAry['idCurrency']=$resultAry['idCurrency'];
				$currencyExchangeRateAry['szCurrency'] = $resultAry['szCurrency'];
				$currencyExchangeRateAry['fExchangeRate'] = $resultAry['fUsdValue'];
			}
			if($_SESSION['user_id']>0)
			{
				$idUser = $_SESSION['user_id'];	
			}
			else
			{
				$idUser = $_SESSION['user_temp_id']	;	
			}
			$query="
				UPDATE
					".__DBC_SCHEMATA_USERS__."
				SET
					szCurrency = '".(int)mysql_escape_custom($currencyExchangeRateAry['idCurrency'])."'
				WHERE
					id = '".(int)$idUser."'	
			" ;
			if($result = $this->exeSQL($query))
			{
				$_SESSION['user_id'] = $idUser ;
				return true ;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function updateUserInfo($data,$idUser,$admin_flag=false)
	{
            if(is_array($data) && (int)$idUser>0)
            {
                if($admin_flag)
                {
                    $required_flag=false;
                }
                else
                {
                    $required_flag=true;
                }
                $this->set_szFirstName(trim(sanitize_all_html_input($data['szFirstName'])),$required_flag);
                $this->set_szLastName(trim(sanitize_all_html_input($data['szLastName'])),$required_flag);
                $this->set_szCity(trim(sanitize_all_html_input($data['szCity'])),$required_flag);
                $this->set_szState(trim(sanitize_all_html_input($data['szState'])),$required_flag);
                
                if($data['iPrivate']==1)
                {
                    $this->szCompanyName = '';
                    $this->szCompanyRegNo = '';
                }
                else
                {
                    $this->set_szCompanyName(trim(sanitize_all_html_input($data['szCompanyName'])),$required_flag);
                    $this->set_szCompanyRegNo(trim(sanitize_all_html_input($data['szCompanyRegNo'])),$required_flag);
                }
                
                $this->set_szCountry(trim(sanitize_all_html_input($data['szCountry'])),$required_flag); 
                $this->set_szAddress1(trim(sanitize_all_html_input($data['szAddress1'])),$required_flag);
                $this->set_szAddress2(trim(sanitize_all_html_input($data['szAddress2'])));
                $this->set_szAddress3(trim(sanitize_all_html_input($data['szAddress3'])));
                $this->set_szPostcode(trim(sanitize_all_html_input($data['szPostCode'])),$required_flag);
                $this->set_id($idUser);
                
                if($admin_flag)
                {
                    $this->set_iLanguage(trim(sanitize_all_html_input($data['iLanguage'])),$required_flag);
                }
                //exit if error exist.
                if ($this->error === true)
                {
                        return false;
                }
                /*if(!empty($data['szPhoneNo'])){
                        if($this->szCountry!=$data['szOldCountry'])
                        {
                                $oldDailCode=$this->getInternationalDailCode($data['szOldCountry']);
                                $newDailCode=$this->getInternationalDailCode($this->szCountry);

                                $codeLength=strlen($oldDailCode);
                                $pnumber = substr($data['szPhoneNo'],$codeLength); 
                                $szPhoneNumber=$newDailCode."".$pnumber;


                        }
                }*/
                
                
                $kConfig_new = new cConfig();
                $kConfig_new->loadCountry($this->szCountry);
			
		$iInternationDialCode = $kConfig_new->iInternationDialCode; 
                $column='';
                if($admin_flag)
                {
                    $column=",iLanguage = '".mysql_escape_custom($this->iLanguage)."'";
                }
            	$query="
            		UPDATE
                            ".__DBC_SCHEMATA_USERS__."
            		SET
                            szCompanyName='".mysql_escape_custom($this->szCompanyName)."',
                            szCompanyRegNo='".mysql_escape_custom($this->szCompanyRegNo)."',
                            szFirstName='".mysql_escape_custom(ucwords($this->szFirstName))."',
                            szLastName='".mysql_escape_custom(ucwords($this->szLastName))."',
                            szAddress='".mysql_escape_custom($this->szAddress1)."',
                            szAddress2='".mysql_escape_custom($this->szAddress2)."',
                            szAddress3='".mysql_escape_custom($this->szAddress3)."',
                            szCity='".mysql_escape_custom($this->szCity)."',
                            szState='".mysql_escape_custom($this->szState)."',
                            idCountry='".mysql_escape_custom($this->szCountry)."',
                            szPostCode='".mysql_escape_custom($this->szPostcode)."',
                            szCurrency='".mysql_escape_custom($data['szCurrency'])."',
                            iPrivate = '".mysql_escape_custom($data['iPrivate'])."',
                            idInternationalDialCode = '".mysql_escape_custom($iInternationDialCode)."'
                            $column    
                            ";
            	/*if($this->szCountry!=$data['szOldCountry'])
				{
					$query .="
							,szPhone='".mysql_escape_custom($szPhoneNumber)."'
					";
				}*/
				$query .="
            		WHERE
            			id='".(int)$this->id."'
            	";
            	//echo $query;
//                die;
            	$result = $this->exeSQL( $query );
            	//$this->checkIncompleteUserProfile($this->id);
                $kRegisterShipCon = new cRegisterShipCon();
                $kRegisterShipCon->updateCapsuleCrmDetails($this->id,false,false,$admin_flag);
            	return true;
			
		}
		else
		{
			return false;
		}
	}
	
	function sendVerificationEmail($data,$idUser)
	{ 
		if(is_array($data))
		{ 
			$this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szEmail'])))); 
			$this->set_id($idUser);
			
			//check if the an user with the following email ID already exists
			if(!empty($this->szEmail) && $this->isUserEmailExist($this->szEmail,$this->id))
			{
				$this->addError( "szEmail" , t($this->t_base.'messages/email_exists') );
			}
			
			//exit if error exist.
			if ($this->error === true)
			{
				return false;
			}
			if((int)$_SESSION['user_id']>0)
			{
				$idUser=$_SESSION['user_id'];
			}
						
			$kUser = new cUser();
			$kUser->getUserDetails($idUser); 
			$ConfirmKey=md5(date("Y-m-d h:i:s").random_number_ConfirmKey());	 
			
			$query="
            	UPDATE
            		".__DBC_SCHEMATA_USERS__."
            	SET
            		szEmail='".mysql_escape_custom($this->szEmail)."', 
            		dtConfirmationCodeSent = now(),
					iConfirmed='0',
            		szConfirmKey='".mysql_escape_custom($ConfirmKey)."' 
				WHERE
					id='".(int)$this->id."'
			";
			if($result = $this->exeSQL($query))
			{
				$iLanguage = getLanguageId();
				$LangUrl = '';
				 
				$kConfig =new cConfig();
                                $langArr=$kConfig->getLanguageDetails('',$iLanguage,'','','','','',false);
                                if(!empty($langArr))
                                {
                                    $LangUrl = "&lang=".  strtolower($langArr[0]['szName']);
                                    //$szLinkText = 'KLIK FOR AT BEKRÆFTE DIN E-MAIL';
                                }
                                else
                                {
                                    //$szLinkText = 'CLICK TO VERIFY E-MAIL ADDRESS';
                                }
                                $szLinkText=t($this->t_base.'fields/click_verify_link');
				$replace_ary['szEmail']=$this->szEmail;
				$confirmationLink=__MAIN_SITE_HOME_PAGE_SECURE_URL__."/confirmation.php?confirmationKey=".$ConfirmKey.$LangUrl;
				$confirmationLinkHttps=__MAIN_SITE_HOME_PAGE_SECURE_URL__."/confirmation.php?confirmationKey=".$ConfirmKey.$LangUrl;
				$replace_ary['szLink']="<a href='".$confirmationLink."'>".$szLinkText."</a>";
				$replace_ary['szHttpsLink']=$confirmationLinkHttps;
                $replace_ary['supportEmail'] = __STORE_SUPPORT_EMAIL__; 
				createEmail(__UPDATE_CONTACT_INFORMATION__, $replace_ary,$this->szEmail, __UPDATE_CONTACT_INFORMATION_SUBJECT__, __STORE_SUPPORT_EMAIL__,$this->id, __STORE_SUPPORT_EMAIL__,__FLAG_FOR_CUSTOMER__);
				return true ;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			} 
		}
		else
		{
			return false;
		}
	}
	function updateContactInfo($data,$idUser,$admin_flag=false)
	{
            if(is_array($data))
            {
                $this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szEmail']))));
                $this->set_idInternationalDialCode(trim(sanitize_all_html_input(strtolower($data['idInternationalDialCode']))));
                $this->set_szPhoneNumber(trim(sanitize_all_html_input(urldecode(base64_decode($data['szPhoneNumberUpdate'])))));
                $this->set_id($idUser);

                //check if the an user with the following email ID already exists
                if(!empty($this->szEmail) && $this->isUserEmailExist($this->szEmail,$this->id) )
                {
                        $this->addError( "szEmail" , t($this->t_base.'messages/email_exists') );
                }

                //exit if error exist.
                if ($this->error === true)
                {
                        return false;
                }

                if((int)$_SESSION['user_id']>0)
                {
                        $idUser=$_SESSION['user_id'];
                }

                $kUser = new cUser();
                $kUser->getUserDetails($idUser);
                if(!empty($this->szPhoneNumber))
                {
                        $phoneNumber = $this->szPhoneNumber;

                }
                /*
                 if(!empty($this->szPhoneNumber)){
                        $szPhoneNumber=ltrim($this->szPhoneNumber,"0");

                        $countrydailcode=$this->getInternationalDailCode($data['szCountry']);
                        $codeLength=strlen($countrydailcode);
                        $str = substr($szPhoneNumber, 0, $codeLength); 
                        if($str==$countrydailcode)
                        {
                            $substr=substr($szPhoneNumber,$codeLength);
                            $pnumber=ltrim($substr,"0");
                            $szPhoneNumber=$countrydailcode."".$pnumber;
                        }
                        else
                        {
                            $szPhoneNumber=$countrydailcode."".$szPhoneNumber;
                        }
                }*/

                    if(($this->szEmail!=$data['szOldEmail']) || ($kUser->iConfirmed<=0))
                    {
                        $ConfirmKey=md5(date("Y-m-d h:i:s").random_number_ConfirmKey());	
                    }

                    $query="
                        UPDATE
                            ".__DBC_SCHEMATA_USERS__."
                        SET
                            szEmail='".mysql_escape_custom($this->szEmail)."',
                            szPhone='".mysql_escape_custom($phoneNumber)."',
                            idInternationalDialCode = '".mysql_escape_custom($this->idInternationalDialCode)."',
                            iAcceptNewsUpdate='".(int)$data['iSendUpdate']."'
                    ";
                    if(($this->szEmail!=$data['szOldEmail']) || ($kUser->iConfirmed<=0))
                    {
                            $query .="
                                    ,dtConfirmationCodeSent = now(),
                                    iConfirmed='0',
                    szConfirmKey='".mysql_escape_custom($ConfirmKey)."'
                            ";
                    }
                    $query .="
                            WHERE
                                    id='".(int)$this->id."'
                    ";

                    $result = $this->exeSQL( $query );
                    if(!$admin_flag)
                    {
                        if(($this->szEmail!=$data['szOldEmail']) || ($kUser->iConfirmed<=0))
                        {
                            $iLanguage = getLanguageId();
                            $LangUrl = '';
                            $kConfig =new cConfig();
                            $langArr=$kConfig->getLanguageDetails('',$iLanguage,'','','','','',false);
                            if(!empty($langArr))
                            {
                                $LangUrl = "&lang=".  strtolower($langArr[0]['szName']);
                                //$szLinkText = 'KLIK FOR AT BEKRÆFTE DIN E-MAIL';
                            }
                            else
                            {
                                //$szLinkText = 'CLICK TO VERIFY E-MAIL ADDRESS';
                            }
                            $szLinkText=t($this->t_base.'fields/click_verify_link');
                            $replace_ary['szEmail']=$this->szEmail;
                            $confirmationLink=__MAIN_SITE_HOME_PAGE_SECURE_URL__."/confirmation.php?confirmationKey=".$ConfirmKey.$LangUrl;
                            $confirmationLinkHttps=__MAIN_SITE_HOME_PAGE_SECURE_URL__."/confirmation.php?confirmationKey=".$ConfirmKey.$LangUrl;
                            $replace_ary['szLink']="<a href='".$confirmationLink."'>".$szLinkText."</a>";
                            $replace_ary['szHttpsLink']=$confirmationLinkHttps;
                            $replace_ary['supportEmail'] = __STORE_SUPPORT_EMAIL__;
                            createEmail(__UPDATE_CONTACT_INFORMATION__, $replace_ary,$this->szEmail, __UPDATE_CONTACT_INFORMATION_SUBJECT__, __STORE_SUPPORT_EMAIL__,$this->id, __STORE_SUPPORT_EMAIL__,__FLAG_FOR_CUSTOMER__);
                        }
                    }
                   // $this->checkIncompleteUserProfile($this->id);
                    
                    $kRegisterShipCon = new cRegisterShipCon(); 
                    $kRegisterShipCon->updateCapsuleCrmDetails($this->id,false,false,$admin_flag);
	            return true;
		}
		else
		{
			return false;
		}
	}
	
	function changePasswordIncompleteProfile($data,$id)
	{
		if(is_array($data) && (int)$id>0)
		{
			$this->set_szPassword(trim(sanitize_all_html_input($data['szNewPassword'])));
			$this->set_szConNewPassword(trim(sanitize_all_html_input($data['szConPassword'])));
			$this->set_id(trim(sanitize_all_html_input($id)));
				
			//exit if error exist.
			if ($this->error === true)
			{
				return false;
			}
			
			if(!empty($this->szPassword) && $this->szPassword!=$this->szConNewPassword)
			{
				$this->addError( "szConfirmPassword" , t($this->t_base.'messages/match_password') );
				return false;
			}
				
			$query="
            	UPDATE
            		".__DBC_SCHEMATA_USERS__."
            	SET
            		szPassword='".mysql_escape_custom(md5($this->szPassword))."'
				WHERE
					id='".(int)$this->id."'
			";
			//echo $query;
			$result = $this->exeSQL( $query );
			//$this->checkIncompleteUserProfile($this->id); 
			return true;
				
		}
	}
	function changePassword($data,$id)
	{
		if(is_array($data) && (int)$id>0)
		{
			$this->set_szOldPassword(trim(sanitize_all_html_input($data['szOldPassword'])));
			$this->set_szPassword(trim(sanitize_all_html_input($data['szNewPassword'])));
			$this->set_szConNewPassword(trim(sanitize_all_html_input($data['szConPassword'])));
			$this->set_id(trim(sanitize_all_html_input($id)));
			
			//exit if error exist.
			if ($this->error === true)
			{
				return false;
			}
			
			//echo $this->szConNewPassword;
			//echo $this->szPassword;
			$query="
				SELECT
					szPassword
				FROM
					".__DBC_SCHEMATA_USERS__."
				WHERE
					id='".(int)$this->id."'
				";
			//echo $query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{
	        	// Check if the user exists
	            if( $this->getRowCnt() > 0 )
	            {
	            	$row=$this->getAssoc($result);
	            	$password=$row['szPassword'];
	            }
			}
			if($password!=md5($this->szOldPassword))
			{
				$this->addError( "szOldPassword" , t($this->t_base.'messages/wrong_old_password') );
				return false;
			}
			if(!empty($this->szPassword) && $this->szPassword!=$this->szConNewPassword)
			{
				$this->addError( "szConfirmPassword" , t($this->t_base.'messages/match_password') );
				return false;
			} 
			$query="
            	UPDATE
            		".__DBC_SCHEMATA_USERS__."
            	SET
            		szPassword='".mysql_escape_custom(md5($this->szPassword))."'
				WHERE
					id='".(int)$this->id."'
			";
			//echo $query;
			$result = $this->exeSQL( $query );
			//$this->checkIncompleteUserProfile($this->id);
			return true;
			
		}
	}
	
	function checkUserAccountAuthentication($data,$id)
	{
		$this->set_szPassword(trim(sanitize_all_html_input($data['szPassword'])));
		$this->set_id(trim(sanitize_all_html_input($id)));
		
		if ($this->error === true)
		{
			return false;
		}
		
		$query="
			SELECT
				szPassword
			FROM
				".__DBC_SCHEMATA_USERS__."
			WHERE
				id='".(int)$this->id."'
			";
		
		//echo $query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
        	// Check if the user exists
            if( $this->getRowCnt() > 0 )
            {
            	$row=$this->getAssoc($result);
            	$password=$row['szPassword'];
            }
            
			if($password!=md5($this->szPassword))
			{
				$this->addError( "szPassword" , t($this->t_base.'messages/wrong_old_password') );
				return false;
			}
			else
			{
				$this->addCookieForPassword($id);
				return true;
			}
		}
		else
		{
			return false;
		}
	}
	function firstTimeChangePassword($data,$id)
	{
		if(is_array($data) && (int)$id>0)
		{ 
			if(empty($data['szPassword']))
			{
				$this->addError( "szPassword_1" , t($this->t_base.'fields/password')." ".t($this->t_base.'fields/is_required') );
			}
			else
			{
				$this->set_szPassword(trim(sanitize_all_html_input($data['szPassword'])));
			}
			
			$this->set_szConNewPassword(trim(sanitize_all_html_input($data['szConPassword'])));
			$this->set_id(trim(sanitize_all_html_input($id)));
			
			//exit if error exist.
			if ($this->error === true)
			{
				return false;
			}
			
			if(!empty($this->szPassword) && $this->szPassword!=$this->szConNewPassword)
			{
				$this->addError( "szConPassword_1" , t($this->t_base.'messages/match_password') );
				return false;
			} 
			
			$query="
            	UPDATE
            		".__DBC_SCHEMATA_USERS__."
            	SET
            		szPassword= '".mysql_escape_custom(md5($this->szPassword))."', 
            		iFirstTimePassword = '0'
				WHERE
					id='".(int)$this->id."'
			";
			//echo $query;
			//die;
			if(($result = $this->exeSQL($query)))
			{ 
				$this->addCookieForPassword($id);
				return true;
			}
			else
			{
				return false;
			} 
		}
	}
	
	function addCookieForPassword($idUser)
	{
		if($idUser>0)
		{
			$kUser = new cUser();
			$kUser->getUserDetails($idUser);
			$szUserPasswordKey = md5($kUser->szEmail."_".$kUser->id."_".mt_rand(0,999)); 
			
			$query="
            	UPDATE
            		".__DBC_SCHEMATA_USERS__."
            	SET 
            		szUserPasswordKey = '".mysql_escape_custom($szUserPasswordKey)."' 
				WHERE
					id='".(int)$idUser."'
			";
			if(($result = $this->exeSQL($query)))
			{ 
				if(__ENVIRONMENT__ == "LIVE")
				{
					setcookie("__USER_PASSWORD_COOKIE__", $szUserPasswordKey, time()+(3600*24*90),'/',$_SERVER['HTTP_HOST'],true);
				}
				else
				{
					setcookie("__USER_PASSWORD_COOKIE__", $szUserPasswordKey, time()+(3600*24*90),'/');
				}
				return true;
			}
			else
			{
				return false;
			} 
		}
	}
	
	function userLogin($data,$bPartnerApi=false)
	{
            if(is_array($data))
            {
                //new user
                if($data['iUserType']==2)
                {
                    $this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szNewEmail']))));
                    $this->set_idCurrency(trim(sanitize_all_html_input(strtolower($data['idCurrency']))));

                    if($this->error === true)
                    {
                            return false;
                    }

                    //check if the an user with the following email ID already exists
                    if(!empty($this->szEmail) && $this->isUserEmailExist($this->szEmail) )
                    {
                        /*if($this->iIncompleteProfileExists)
                        {
                            $this->deactivateUserAccount($this->idIncompleteUser);
                        }
                        else 
                        {
                            $this->addError( "szEmail" , t($this->t_base.'messages/email_exists_signin_below') );
                            return false;
                        }*/ 
                        
                        $this->addError( "szEmail" , t($this->t_base.'messages/email_exists_signin_below') );
                        return false;
                    }				
                    //creating an incomplete profile for user.				
                    if($this->shortUserSignup($data))
                    {
                        $idUser = $this->id ; 
                        $this->addCookieForPassword($idUser);
                        return true;
                    }
                }
                else
                {
                    //signin for existing user 
                    $this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szEmail']))));
                    $this->set_szNonFrocePassword(trim(sanitize_all_html_input($data['szPassword'])));
                    //exit if error exist.
                    if ($this->error === true)
                    {
                        return false;
                    }

                    $query="
                        SELECT
                            id,
                            szPassword,
                            iConfirmed
                        FROM
                            ".__DBC_SCHEMATA_USERS__."
                        WHERE
                            szEmail='".mysql_escape_custom($this->szEmail)."'
                        AND
                            iActive='1'
                    ";
                    //echo $query;
                    if( ( $result = $this->exeSQL( $query ) ) )
                    {
                        if( $this->getRowCnt() > 0 )
                        {
                            $row=$this->getAssoc($result);
                            $password=$row['szPassword'];
                            $idUser=$row['id'];
                            $iConfirmed=$row['iConfirmed'];

                            $kRegisterShipCon = new cRegisterShipCon();
                            $kRegisterShipCon->updateCapsuleCrmDetails($idUser); 
                            if($bPartnerApi)
                            {
                                #@TO DO
                            }
                            else
                            {
                                //if user gets logged in we just remove temporary user id.
                                $_SESSION['temp_user_id'] = '';
                                unset($_SESSION['temp_user_id']); 
                            } 
                        }
                        else
                        {
                            if($bPartnerApi)
                            {
                                $this->iWrongUserNamePassword = 1;
                                return false;
                            } 
                            else
                            {
                                $this->addError( "szEmail" , t($this->t_base.'messages/wrong_username_password') );
                                return false;
                            } 
                        }
                    }

                    if($password!=md5($data['szPassword']))
                    { 
                        if($bPartnerApi)
                        {
                            $this->iWrongUserNamePassword = 1;
                            return false;
                        } 
                        else
                        {
                            $this->addError( "szEmail" , t($this->t_base.'messages/wrong_username_password') );
                            return false;
                        }
                    }

                    $loginLogAry = array();
                    $loginLogAry['idUser'] = $idUser;
                    $loginLogAry['szEmail'] = $this->szEmail;
                    $loginLogAry['iSuccess'] = 1;
                     
                    if($bPartnerApi)
                    {
                        $loginLogAry['iPartnerApiLogin'] = 1;
                        $this->userLoginLog($loginLogAry);
                        $this->iUserSuccessfullyLogin = 1;
                        $this->idCustomer = $idUser; 
                        return true;
                    }
                    else
                    {
                        $this->userLoginLog($loginLogAry);
                        if(__ENVIRONMENT__ == "LIVE")
                        {
                            setcookie("__USER_EMAIL_COOKIE__", $this->szEmail, time()+(3600*24*90),'/',$_SERVER['HTTP_HOST'],true);
                        }
                        else
                        {
                            setcookie("__USER_EMAIL_COOKIE__", $this->szEmail, time()+(3600*24*90),'/');
                        } 
                        $kMautic = new cMautic(); 
                        //$kMautic->updateDataToMauticApi($idUser);

                        $this->updateLastLoginUser($idUser);
                        $this->addCookieForPassword($idUser);

                        if((int)$idUser>0)
                        {
                            $token = sha1(uniqid(rand(), true));
                            $this->updateUserLoginCookie($idUser,$token);
                            if(__ENVIRONMENT__ == "LIVE")
                            {
                                setcookie("__USER_LOGIN_COOKIE__", $token, time()+(3600*24*90),'/',$_SERVER['HTTP_HOST'],true);
                            }
                            else
                            {
                                setcookie("__USER_LOGIN_COOKIE__", $token, time()+(3600*24*90),'/');
                            }
                            $_SESSION['user_id']=$idUser;
                            return true;
                        }
                    } 
                }
            }
            else
            {
                return false;
            }
	}
	
        /**
	 * writes the user log history to the database
	 *
	 * @access public
	 * @return bool
	 * @author Ajay
	 */
	 
	function userLoginLog($data,$cookieFlag=false,$iFacebookLogin=0,$iGoogleLogin=0)
	{
            $cookieValue=0;
            if($cookieFlag===true)
            {
               $cookieValue=1; 
            } 
            
            $query = "
                INSERT INTO
                    ".__DBC_SCHEMATA_LOGIN_LOG__."
                (
                    idUser, 
                    szUserName,
                    iSuccess,
                    dtLogin, 
                    szIP,
                    szReferer,
                    szUserAgent,
                    isFromCookie,
                    iFaceBookLogin,
                    iGoogleLogin,
                    iPartnerApiLogin
                )
                VALUES
                ( 
                    '".(int)$data['idUser']."',
                    '".mysql_escape_custom($data['szEmail'])."',
                    '".(int)$data['iSuccess']."',
                    now(), 
                    '".mysql_escape_custom(__REMOTE_ADDR__)."',
                    '".mysql_escape_custom(__HTTP_REFERER__)."',
                    '".mysql_escape_custom(__HTTP_USER_AGENT__)."',
                    '".(int)$cookieValue."',
                    '".(int)$iFacebookLogin."',
                    '".(int)$iGoogleLogin."',
                    '".(int)$data['iPartnerApiLogin']."'
                )
            ";
            //echo "<br />".$query."<br/>"; 
            if( ( $result = $this->exeSQL( $query ) ) )
            {  
                return true;
            }
            else
            {
                return false;
            } 
	}
	function manualLogin($idUser,$iFacebookLogin=0,$iGoogleLogin=0)
        { 
            if($idUser>0)
            {
                $kUser = new cUser();
                $kUser->getUserDetails($idUser);
                $iConfirmed=$row['iConfirmed'];
                if(__ENVIRONMENT__ == "LIVE")
                {
                    setcookie("__USER_EMAIL_COOKIE__", $kUser->szEmail, time()+(3600*24*90),'/',$_SERVER['HTTP_HOST'],true);
                }
                else
                {
                    setcookie("__USER_EMAIL_COOKIE__", $kUser->szEmail, time()+(3600*24*90),'/');
                }
                
                $loginLogAry = array();
                $loginLogAry['idUser'] = $kUser->id;
                $loginLogAry['szEmail'] = $kUser->szEmail;
                $loginLogAry['iSuccess'] = 1;
                $this->userLoginLog($loginLogAry,false,$iFacebookLogin,$iGoogleLogin);
                
                $this->updateLastLoginUser($idUser);
                $this->addCookieForPassword($idUser);

                if($iConfirmed!=1)
                {
                    $_SESSION['idUser']=$idUser;
                    $_SESSION['user_id']=$idUser; 
                    $token = sha1(uniqid(rand(), true));
                    $this->updateUserLoginCookie($idUser,$token);
                    if(__ENVIRONMENT__ == "LIVE")
                    {
                        setcookie("__USER_LOGIN_COOKIE__", $token, time()+(3600*24*90),'/',$_SERVER['HTTP_HOST'],true);
                    }
                    else
                    {
                        setcookie("__USER_LOGIN_COOKIE__", $token, time()+(3600*24*90),'/');
                    } 
                    return true; 
                }
                else if((int)$idUser>0)
                {
                    $token = sha1(uniqid(rand(), true));
                    $this->updateUserLoginCookie($idUser,$token);
                    if(__ENVIRONMENT__ == "LIVE")
                    {
                        setcookie("__USER_LOGIN_COOKIE__", $token, time()+(3600*24*90),'/',$_SERVER['HTTP_HOST'],true);
                    }
                    else
                    {
                        setcookie("__USER_LOGIN_COOKIE__", $token, time()+(3600*24*90),'/');
                    }
                    $_SESSION['user_id']=$idUser;
                    return true;
                }
            }
        }
        
        function getAllUsersCapsuleCrmID($szCapsulePrtyID=false,$get_all_records=false,$get_not_added_users=false,$bMautic=false)
        { 
            /*if(!empty($szCapsulePrtyID))
            {
                $query_and = " AND szCapsulePrtyID = '".mysql_escape_custom($szCapsulePrtyID)."' ";
            }*/
            
            if($bMautic)
            {
                if($get_not_added_users)
                {
                    $query_where = " szMauticAccountID = '' ";
                }
                else
                {
                    $query_where = " szMauticAccountID != '' ";
                }
            }
            else
            {
                /*if($get_not_added_users)
                {
                    $query_where = " szCapsulePrtyID = '' ";
                }
                else
                {
                    $query_where = " szCapsulePrtyID != '' ";
                }*/
            } 
            
            $query = " 
                SELECT 
                    id,
                    szCapsulePrtyID,
                    iAcceptNewsUpdate,
                    iLanguage,
                    szMauticAccountID
                FROM
                    ".__DBC_SCHEMATA_USERS__."
                WHERE
                    $query_where
                    $query_and
            ";
            //echo $query ;
            if($result=$this->exeSQL($query))
            {
                /*if(!empty($szCapsulePrtyID))
                {
                    $row = $this->getAssoc($result);
                    return $row['id'];
                }
                else
                {*/
                    $retAry=array(); 
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    { 
                        if($get_all_records)
                        {
                            $retAry[$ctr] = $row;   
                            $ctr++;
                        }
                        else
                        {
                            if($bMautic)
                            {
                                $retAry[$row['id']] = $row['szMauticAccountID'];   
                            }
                            /*else
                            {
                                $retAry[$row['id']] = $row['szCapsulePrtyID'];   
                            }*/ 
                        } 
                    }
                    return $retAry ;
                //} 
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }	
        }
        
	function cookieUserLogin($cookieKey)
	{
            $query="
                SELECT
                    id,
                    szPassword,
                    iConfirmed,
                    szEmail
                FROM
                    ".__DBC_SCHEMATA_USERS__."
                WHERE
                    szLoginCookie ='".mysql_escape_custom(trim($cookieKey))."'
                AND
                    iActive='1'
            ";
            if($result = $this->exeSQL($query))
            {
                if($this->getRowCnt() > 0 )
                {
                    $row=$this->getAssoc($result);
                    $password=$row['szPassword'];
                    $idUser=$row['id'];
                    $iConfirmed=$row['iConfirmed'];
                    $szEmail=$row['szEmail'];

                    if(__ENVIRONMENT__ == "LIVE")
                    {
                        setcookie("__USER_EMAIL_COOKIE__", $szEmail, time()+(3600*24*90),'/',$_SERVER['HTTP_HOST'],true);
                    }
                    else
                    {
                        setcookie("__USER_EMAIL_COOKIE__", $szEmail, time()+(3600*24*90),'/');
                    }

                    $loginLogAry = array();
                    $loginLogAry['idUser'] = $idUser;
                    $loginLogAry['szEmail'] = $szEmail;
                    $loginLogAry['iSuccess'] = 1;
                    $this->userLoginLog($loginLogAry,true);
                    
                    $kMautic = new cMautic(); 
                    //$kMautic->updateDataToMauticApi($idUser);
                
                    $this->updateLastLoginUser($idUser);
                    $_SESSION['user_id']=$idUser;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                    return false;
            }
	}
	
	function updateLastLoginUser($id)
	{
            if($id>0)
            { 
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_USERS__."
                    SET
                        dtLastLogin=NOW(),
                        szIpAddress = '".mysql_escape_custom(__REMOTE_ADDR__)."',
                        szHttpReferrar = '".mysql_escape_custom(__HTTP_REFERER__)."',
                        szBrowser = '".mysql_escape_custom(__HTTP_USER_AGENT__)."'
                    WHERE
                        id = ".(int)$id."
                ";
                if( ( $result = $this->exeSQL( $query ) ) )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
	} 
	
	function updateUserLoginCookie($id,$token)
	{
            if(!empty($id))
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_USERS__."
                    SET
                        szLoginCookie='".mysql_escape_custom($token)."'
                    WHERE
                            id = ".(int)$id."
                    ";
                if( ( $result = $this->exeSQL( $query ) ) )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
	}
			
			
	function checkdeleteUserAccount($data)
	{
		//print_r($data);
		if(is_array($data))
		{
			$this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szEmail']))));
			$this->set_szNonFrocePassword(trim(sanitize_all_html_input($data['szPassword'])));
			
			if(!empty($this->szEmail) && ($this->szEmail!=$data['szLoginEmail']))
			{
				$this->addError( "szEmail" , t($this->t_base_delete_account.'messages/email_does_not_match') );
			}			
			//exit if error exist.
			if ($this->error === true)
			{
				return false;
			}
			
				$query="
					SELECT
						szPassword
					FROM
						".__DBC_SCHEMATA_USERS__."
					WHERE
						szEmail='".mysql_escape_custom($this->szEmail)."'
					AND
						iActive = '1'
				";
				//echo $query;
				if( ( $result = $this->exeSQL( $query ) ) )
				{
		            if( $this->getRowCnt() > 0 )
		            {
		            	$row=$this->getAssoc($result);
		            	$password=$row['szPassword'];
		            	$idUser=$row['id'];
		            	$iConfirmed=$row['iConfirmed'];
		            }
				}				
				if($password!=md5($data['szPassword']))
				{
					$this->addError( "szOldPassword" , t($this->t_base_delete_account.'messages/wrong_username_password') );
					return false;
				}
				else
				{
					return true;
				}
		}
		else
		{
			return false;
		}
	}
	
	function getMultiUserData($idGroup,$flag=false)
	{
		if((int)$idGroup>0)
		{
			$whereQuery='';
			if($flag)
			{
				$whereQuery="
					AND
						iInvited='1'
				";
			}
			$query="
				SELECT
					id,
					szFirstName,
					szLastName,
					iInvited,
					szEmail,
					szCompanyName
				FROM
					".__DBC_SCHEMATA_USERS__."
				WHERE
					idGroup='".(int)$idGroup."'
				AND
					iActive = '1'	
					".$whereQuery."
				ORDER BY
					szFirstName ASC	
			";
			//echo $query ;
			if( ( $result = $this->exeSQL( $query ) ) )
			{
	        	
	            if( $this->getRowCnt() > 0 )
	            {
	                  while($row=$this->getAssoc($result))
	                  {
	                  	$multiUserArr[]=$row;
	                  }
	                  return $multiUserArr;
	            }
	            else
	            {
	                    return array();
	            }
			}
		}
		else
		{
			return array();
		}
	}
	
	function removeUser($data)
	{
		if(!empty($data))
		{
			$query="
				UPDATE
					".__DBC_SCHEMATA_USERS__."
				SET
					idGroup='0'
				WHERE
					id IN (".mysql_escape_custom($data).")
				AND
				    iActive = '1'	
			";
			if( ( $result = $this->exeSQL( $query ) ) )
			{
				//return true;
			}
			else
			{
				return false;
			}
			$this->getUserDetails($_SESSION['user_id']);
			$idGroup=$this->idGroup;
			$idUser = $_SESSION['user_id'];
			$query="
				SELECT
					count(id) as totalMappedUser
				FROM
					".__DBC_SCHEMATA_USERS__."
				WHERE
					idGroup='".(int)$idGroup."'
				AND
					id='".(int)$idUser."'
			";
			//echo $query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{
	        	
	            if( $this->getRowCnt() <= 1 )
	            {
	            	$query="
						UPDATE
							".__DBC_SCHEMATA_USERS__."
						SET
							idGroup='0'
						WHERE
							id ='".(int)$idUser."'
						AND
						    iActive = '1'	
					";
	            	//echo $query;
	            	$result = $this->exeSQL( $query ) ;
	            }
			}
			
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_USER_INVITE_HISTORY__."
				WHERE
					IdInvitedUser IN (".mysql_escape_custom($data).")
				AND
				    idUser = '".(int)$idUser."'	
			";
			if( ( $result = $this->exeSQL( $query ) ) )
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function checkEmailAddress($data)
	{
		$this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szEmail']))));	
		$idUser = $_SESSION['user_id'];
		if(!empty($this->szEmail) && !$this->userEmailExits($this->szEmail))
		{
			//$this->addError( "szEmail" , t($this->t_base_access.'messages/email_not_exists') );
			$this->szEmailNotExists=true;
			return false;
		}
		else if(($this->idMappedUserId>0) && ($this->idMappedUserId == $idUser))
		{
			$this->addError( "szEmail" , t($this->t_base_access.'messages/map_with_self') );
			$this->idMappedUserId = 0;
		}
		
		if(!empty($this->szEmail) && $this->isUserEmailAlreadyMapped($this->szEmail))
		{
			$this->iAlreadyMapperUserError = true;
			//$this->addError( "szEmail" , t($this->t_base_access.'messages/already_mapped') );
			return false;
		}
		//exit if error exist.
		if ($this->error === true)
		{
			return array();
		}
		$idUser = $_SESSION['user_id'];
		$query="
			SELECT
				id,
				szFirstName,
				szLastName
			FROM
				".__DBC_SCHEMATA_USERS__."
			WHERE
				szEmail = '".mysql_escape_custom($this->szEmail)."'
			AND
				id !='".(int)$idUser."'	
			AND
				iActive = '1'	
		";
		if( ( $result = $this->exeSQL( $query ) ) )
		{
        	// Check if the user exists
            if( $this->getRowCnt() > 0 )
            {
            	while($row=$this->getAssoc($result))
            	{
            		$userDetails[]=$row;
            	}
            	return $userDetails;
            }
		}
		else
		{
			return array();
		}
	}
	
	function isUserEmailAlreadyMapped( $email )
	{
		$this->set_szEmail($email);
		
		if($this->error==true)
		{
			return false;
		}
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_USERS__."
			WHERE
				szEmail = '".mysql_escape_custom($this->szEmail)."'
			AND
				idGroup>'0'
			AND
				iActive = '1'
		";
		//echo $query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
        	// Check if the user exists
            if( $this->getRowCnt() > 0 )
            {
                   return true;
            }
            else
            {
                    return false;
            }
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	function userEmailExits( $email )
	{
            $this->set_szEmail($email); 
            $query="
                SELECT
                    id
                FROM
                    ".__DBC_SCHEMATA_USERS__."
                WHERE
                    szEmail = '".mysql_escape_custom($this->szEmail)."'
                AND
                    iActive = '1'
            ";
            //echo $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
        	// Check if the user exists
                if( $this->getRowCnt() > 0 )
                {
                    $row=$this->getAssoc($result);
                    $this->idMappedUserId = $row['id'];
                    return true;
                }
                else
                {
                        return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
	
	function mappedUser($data)
	{
		if(!empty($data))
		{
			$ConfirmKey=md5(date("Y-m-d h:i:s").random_number_ConfirmKey());
			
			$userArr=$this->getUserDetails($data['idUser']);
			$mappedArr=$this->getUserDetails($data['mappedId']);
			
			//$idGroup=$this->getUserGroup($data['idUser']);
			
			/*
			$query="
					UPDATE
						".__DBC_SCHEMATA_USERS__."
					SET
						idGroup='".(int)$idGroup."',
						iInvited='1'
					WHERE
						id='".(int)$data['idUser']."'
				";
			
				$result = $this->exeSQL( $query );
				
			$query="
					UPDATE
						".__DBC_SCHEMATA_USERS__."
					SET
						idGroup='".(int)$idGroup."',
						szInvitedKey='".mysql_escape_custom($ConfirmKey)."'
					WHERE
						id='".(int)$data['mappedId']."'
				";
				*/
				$query="
					INSERT INTO
						".__DBC_SCHEMATA_USER_INVITE_HISTORY__."
					(
						idUser,
						IdInvitedUser,
						szConfirmationKey,
						dtCreatedOn
					)
					VALUES
					(
						'".(int)$data['idUser']."',
						'".(int)$data['mappedId']."',
						'".mysql_escape_custom(trim($ConfirmKey))."',
						now()
					)
				";
			//echo $query;
			
				if( ( $result = $this->exeSQL( $query ) ) )
				{
					$iLanguage = getLanguageId();
					$LangUrl = '';
					$kConfig =new cConfig();
                                        $langArr=$kConfig->getLanguageDetails('',$iLanguage,'','','','','',false);
                                        if(!empty($langArr))
                                        {
                                            $LangUrl = "&lang=".  strtolower($langArr[0]['szName']);
                                            //$szLinkText = 'KLIK FOR AT BEKRÆFTE DIN E-MAIL';
                                        }
                                        else
                                        {
                                            //$szLinkText = 'CLICK TO VERIFY E-MAIL ADDRESS';
                                        }
                                        $szLinkText=t($this->t_base.'fields/click_to_confirm_link');
					
					$replace_ary['mappedName']=ucwords($mappedArr['szFirstName'])." ".ucwords($mappedArr['szLastName']);
					$replace_ary['szName']=ucwords($userArr['szFirstName'])." ".ucwords($userArr['szLastName']);
					$replace_ary['szEmail'] = $mappedArr['szEmail'];
					$confirmationLink = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/inviteConfirmation.php?confirmationKey=".$ConfirmKey.$LangUrl;
					$confirmationLinkHttps = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/inviteConfirmation.php?confirmationKey=".$ConfirmKey.$LangUrl;
					$replace_ary['szLink'] = "<a href='".$confirmationLink."'>".$szLinkText."</a>";
					$replace_ary['szHttpsLink']=$confirmationLinkHttps;
	                $replace_ary['supportEmail'] = __STORE_SUPPORT_EMAIL__; 
					createEmail(__USER_INVITATION__, $replace_ary,$mappedArr['szEmail'], __USER_INVITATION_SUBJECT__, __STORE_SUPPORT_EMAIL__,$mappedArr['id'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_CUSTOMER__);
					return true;
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
		}
		else
		{
			return false;
		}
	}
	
	function getPendingInvitedUser($idUser,$idGroup=false)
	{
		if($idUser>0)
		{
			$query="
				SELECT
					u.id,
					u.szFirstName,
					u.szLastName,
					u.szEmail,
					u.szCompanyName,
					inv.IdInvitedUser,
					inv.id as invtid
				FROM
					".__DBC_SCHEMATA_USERS__." u
				INNER JOIN
					".__DBC_SCHEMATA_USER_INVITE_HISTORY__." inv
				ON
					inv.IdInvitedUser = u.id	
				WHERE
					inv.IdUser = '".(int)$idUser."'	
				GROUP BY
					inv.IdInvitedUser		
			";
			//echo "<br>".$query."<br>";
			if($result=$this->exeSQL($query))
			{
				$invitedUserAry = array();
				$ctr = 0;
				while($row=$this->getAssoc($result))
				{
					$invitedUserAry[$ctr] = $row ;
					$ctr++;
				}
				
				return $invitedUserAry ;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return array();
		}
	}
	
	function checkInviteConfirmationKey($szConfirmationKey)
	{
		$query="
			SELECT
				id,
				idUser,
				IdInvitedUser
			FROM
				".__DBC_SCHEMATA_USER_INVITE_HISTORY__."
			WHERE
				szConfirmationKey  = '".mysql_escape_custom(trim($szConfirmationKey))."';
		";
		//echo $query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
        	// Check if the user exists
            if( $this->getRowCnt() > 0 )
            {
            	$row=$this->getAssoc($result);
            	
            	//$_SEEION['idUser']=$row['id'];
            	$userArr[]=$row;
            	
                return $userArr;
            }
            else
            {
                    return array();
            }
		}
	}
	
	function getUserGroup($idUser)
	{
		if((int)$idUser>0)
		{
			$query="
				SELECT
					MAX(idGroup) as group_id
				FROM
					".__DBC_SCHEMATA_USERS__."
				WHERE
					iActive = '1'
			";
			//echo "<br>".$query."<br>";
			if( ( $result = $this->exeSQL( $query ) ) )
			{
	        	// Check if the user exists
	            if( $this->getRowCnt() > 0 )
	            {
	            	$row=$this->getAssoc($result);
	            	$maxGroupId=$row['group_id'];
	            	if($maxGroupId>0)
	            	{
	            		$query="
							SELECT
								idGroup
							FROM
								".__DBC_SCHEMATA_USERS__."
							WHERE
								id='".(int)$idUser."'
							AND
								iActive = '1'
						";
	            		//echo "<br>".$query."<br>";
			            if( ( $result = $this->exeSQL( $query ) ) )
						{
			        		// Check if the user exists
			            	if( $this->getRowCnt() > 0 )
			            	{
	            				$row=$this->getAssoc($result);
	            				if($row['idGroup']>0)
	            				{
	            					return $row['idGroup'];
	            				}
	            				else
	            				{
	            					$maxGroupId=$maxGroupId+1;
	            					return $maxGroupId;
	            				}
			            	}
						}
	            	}
	            	else
	            	{
	            		return 1;
	            	}
	            	//return $row['idGroup'];
	            }
			}
		}
	}
	
	function deactivateUserAccount($idUser,$iPartnerApi=false)
	{
		if((int)$idUser>0)
		{
			$query="
				UPDATE
					".__DBC_SCHEMATA_USERS__."
				SET
					iActive='0'
				WHERE
					id='".(int)$idUser."'
				";
			if($result = $this->exeSQL( $query ))
			{
                            if($iPartnerApi)
                            {
                                $queryPartnerApi="
                                    UPDATE
                                            ".__DBC_SCHEMATA_CUSTOMER_API_LOGIN__."
                                    SET
                                            iActive='0'
                                    WHERE
                                            idCustomer='".(int)$idUser."'
                                    ";
                                $resultPartnerApi = $this->exeSQL( $queryPartnerApi );
                            }
                            
				if($this->deleteUserInviteHistory((int)$idUser))
				{
					return true;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return true;;
		}
	}
	function deleteUserInviteHistory($idUser)
	{
			$query="
					DELETE FROM
						".__DBC_SCHEMATA_USER_INVITE_HISTORY__."
					WHERE
						IdInvitedUser = '".(int)$idUser."'
					OR
						idUser = '".(int)$idUser."'	
					";
				if($result = $this->exeSQL( $query ))
				{
					return true;
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
	}
	
	function forgotPassword($szEmail)
	{
		$this->set_szEmail(sanitize_all_html_input(strtolower($szEmail)));
		//exit if error exist.
		if ($this->error === true)
		{
			return false;
		}
		
		$query="
			SELECT
				id,
				szFirstName,
				szLastName
			FROM
				".__DBC_SCHEMATA_USERS__."
			WHERE
				szEmail = '".mysql_escape_custom($this->szEmail)."'
			AND
				iActive = '1'
		";
		//echo $query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
        	// Check if the user exists
            if( $this->getRowCnt() > 0 )
            {
            		$row=$this->getAssoc($result);
            		$this->id = $row['id'];
            		$password=create_password();
            		$query="
                            UPDATE
                                ".__DBC_SCHEMATA_USERS__."
                            SET
                                szPassword='".mysql_escape_custom(md5($password))."',
                                iPasswordUpdated = 1
                            WHERE
                                szEmail = '".mysql_escape_custom($this->szEmail)."'
                            AND
                                id = ".$this->id."	
                            AND
                                iActive = '1'	
            		";
            		$result = $this->exeSQL( $query );
            		$replace_ary['szFirstName']=$row['szFirstName'];
            		$replace_ary['szLastName']=$row['szLastName'];
            		$replace_ary['szPassword']=$password;
            		$replace_ary['szEmail']=$this->szEmail;
            		createEmail(__FORGOT_PASSWORD__, $replace_ary,$this->szEmail, __FORGOT_PASSWORD_SUBJECT__, __STORE_SUPPORT_EMAIL__,$this->id, __STORE_SUPPORT_EMAIL__,__FLAG_FOR_CUSTOMER__);
                   
            		//$this->checkIncompleteUserProfile($this->id);
            		return true;
            }
            else
            {
            	$this->addError( "szEmail" , t($this->t_base_access.'messages/email_not_exists') );
                return false;
            }
		}
	}
        
        function sendForgotPasswordEmail($idCustomer,$idLanguage=0)
        {
            if($idCustomer>0)
            { 
                $userDataAry = array();
                $userDataAry = $this->getUserDetails($idCustomer);
                
                $password = create_password();
                
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_USERS__."
                    SET
                        szPassword='".mysql_escape_custom(md5($password))."',
                        iPasswordUpdated = 1
                    WHERE
                        szEmail = '".mysql_escape_custom($this->szEmail)."'
                    AND
                        id = ".$idCustomer."	
                    AND
                        iActive = '1'	
                ";
                if($result = $this->exeSQL( $query ))
                {
                    $replace_ary = array();
                    $replace_ary['szFirstName'] = $userDataAry['szFirstName'];
                    $replace_ary['szLastName'] = $userDataAry['szLastName'];
                    $replace_ary['szPassword'] = $password;
                    $replace_ary['szEmail'] = $userDataAry['szEmail'];
                    $replace_ary['iLanguage']=$idLanguage;
                    createEmail(__FORGOT_PASSWORD__, $replace_ary,$this->szEmail, __FORGOT_PASSWORD_SUBJECT__, __STORE_SUPPORT_EMAIL__,$this->id, __STORE_SUPPORT_EMAIL__,__FLAG_FOR_CUSTOMER__);
                    return true;
                }
                else
                {
                    return false;
                } 
            } 
        }
	
	function updateInvitationStatus($data)
	{
		$idGroup=$this->getUserGroup($data['idUser']);
		
		$query="
			UPDATE
				".__DBC_SCHEMATA_USERS__."
			SET
				idGroup='".(int)$idGroup."',
				iInvited='1'
			WHERE
				id='".(int)$data['idUser']."'
			AND
				iActive = '1'
		";
		//echo "<br>".$query."<br>";
		$result = $this->exeSQL( $query );
				
		$query="
			UPDATE
				".__DBC_SCHEMATA_USERS__."
			SET
				iInvited='1',
				idGroup = '".(int)$idGroup."'
			WHERE
				id ='".(int)$data['IdInvitedUser']."'
			AND
				iActive = '1'
		";
		//echo "<br>".$query."<br>";
		if($result = $this->exeSQL( $query ))
		{
			$this->deleteInvitedHistory($data['IdInvitedUser'],$data['idUser']);
			return true;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function deleteInvitedHistory($idInvitedUser,$user)
	{
		$query="
			DELETE 
			FROM
				".__DBC_SCHEMATA_USER_INVITE_HISTORY__."
			WHERE
				(
					IdInvitedUser = '".(int)$idInvitedUser."'	
				AND
					idUser = '".(int)$user."'
				)
			OR	
				(
					IdInvitedUser = '".(int)$user."'	
				AND
					idUser = '".(int)$idInvitedUser."'
				)
		";
		if($result = $this->exeSQL( $query ))
		{
			return true;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function contactEmail($data,$subject)
	{
		if(!empty($data))
		{
			$this->set_szMessage(sanitize_specific_html_input($data['szMessage']));
			if($data['contactEditEmail']=='1' || !empty($data['szEmail']))
			{
				$this->set_szContactEmail(sanitize_specific_html_input($data['szEmail']));
			}
			
			//exit if error exist.
			if ($this->error === true)
			{
				return false;
			}
			
			$kWHSSearch = new cWHSSearch();
			$toEmail=$kWHSSearch->getManageMentVariableByDescription('__CONTACT_EMAIL__');
			//$toEmail="ashish@whiz-solutions.com";
			$subject=$subject." ".date("d/m/Y");
			$message="";
			if(!empty($this->szContactEmail))
			{
				$message .=$this->szContactEmail."<br/>";
			}
			$message .=$this->szMessage;
                        
                        /*
                        * building block for send Transporteca E-mail
                        */
                        $mailBasicDetailsAry = array();
                        $mailBasicDetailsAry['szEmailTo'] = $toEmail;
                        $mailBasicDetailsAry['szEmailFrom'] = __STORE_SUPPORT_EMAIL__; 
                        $mailBasicDetailsAry['szEmailSubject'] = $subject;
                        $mailBasicDetailsAry['szEmailMessage'] = $message;
                        $mailBasicDetailsAry['szReplyTo'] = __STORE_SUPPORT_EMAIL__; 
                        //print_r($mailBasicDetailsAry);
                        $kSendEmail = new cSendEmail();
                        $kSendEmail->sendEmail($mailBasicDetailsAry); 
			//sendEmail($toEmail,__STORE_SUPPORT_EMAIL__,$subject,$message,__STORE_SUPPORT_EMAIL__,0);
			return true;
		}	
		else
		{
			return false;
		}
	}

	function getAllMappedUser($idGroup)
	{
		if((int)$idGroup>0)
		{
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_USERS__."
				WHERE
					idGroup='".(int)$idGroup."'
				AND
				    iActive = '1'
			";
			if( ( $result = $this->exeSQL( $query ) ) )
			{
	            if( $this->getRowCnt() > 0 )
	            {
	            	$ctr=0;
	                while($row=$this->getAssoc($result))
	                {
	                    $multiUserArr[$ctr]=$row['id'];
	                  	$ctr++;
	                }
	                return $multiUserArr;
	            }
	            else
	            {
	                    return array();
	            }
			}
		}
		else
		{
			return array();
		}
	}
	
	function updateIConfirmedStatus($idUser)
	{
		if((int)$idUser>0)
		{
			$query="
				UPDATE
					".__DBC_SCHEMATA_USERS__."
				SET
					iConfirmed='1',
					szConfirmKey='',
					dtConfirmationCodeSent = ''
				WHERE
					id='".(int)$idUser."'
			";
			$result = $this->exeSQL( $query );
			//$this->checkIncompleteUserProfile($idUser);
                        
                        $kRegisterShipCon = new cRegisterShipCon();
                        $szCustomerHistoryNotes = "E-mail verified on ".date('d/m/Y H:i:s');
                        $kRegisterShipCon->updateCapsuleHistoryNotes($szCustomerHistoryNotes,$idUser);
                
			$this->szFirstName=$row['szFirstName'];
			$this->szLastName=$row['szLastName'];
		}
	}
	
	
	function updateUserInfoByAdmin($data,$idUser)
	{
		if(is_array($data) && (int)$idUser>0)
		{
			$this->set_szFirstName(trim(sanitize_all_html_input($data['szFirstName'])));
			$this->set_szLastName(trim(sanitize_all_html_input($data['szLastName'])));
			$this->set_szCity(trim(sanitize_all_html_input($data['szCity'])));
			$this->set_szState(trim(sanitize_all_html_input($data['szState'])));
			$this->set_szCountry(trim(sanitize_all_html_input($data['szCountry'])));
			$this->set_szCompanyName(trim(sanitize_all_html_input($data['szCompanyName'])));
			$this->set_szCompanyRegNo(trim(sanitize_all_html_input($data['szCompanyRegNo'])));
			$this->set_szAddress1(trim(sanitize_all_html_input($data['szAddress1'])));
			$this->set_szAddress2(trim(sanitize_all_html_input($data['szAddress2'])));
			$this->set_szAddress3(trim(sanitize_all_html_input($data['szAddress3'])));
			$this->set_szPostcode(trim(sanitize_all_html_input($data['szPostCode'])));
			$this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szEmail']))));
			$this->set_szPhoneNumber(trim(sanitize_all_html_input(urlencode($data['szPhoneNumber']))));
			$this->set_id($idUser);
			if(!empty($this->szEmail) && $this->isUserEmailExist($this->szEmail,$this->id) )
			{
				$this->addError( "szEmail" , t($this->t_base.'messages/email_exists') );
			}
					
			//exit if error exist.
			if ($this->error === true)
			{
				return false;
			}
			
			if(!empty($this->szPhoneNumber))
			{
				$str = substr($this->szPhoneNumber, 0, 1); 
				//echo $str;
				$substr=substr($this->szPhoneNumber,1);
				$phone=str_replace("+"," ",$substr);
				//echo $phone;
				if(!empty($phone))
				{
					$phoneNumber=$str."".urldecode($phone);
				}
				else
				{
					$phoneNumber = urldecode($this->szPhoneNumber) ;
				}
			}
			/*if(!empty($data['szPhoneNo'])){
				if($this->szCountry!=$data['szOldCountry'])
				{
					$oldDailCode=$this->getInternationalDailCode($data['szOldCountry']);
					$newDailCode=$this->getInternationalDailCode($this->szCountry);
					
					$codeLength=strlen($oldDailCode);
					$pnumber = substr($data['szPhoneNo'],$codeLength); 
					$szPhoneNumber=$newDailCode."".$pnumber;
					
					
				}
			}*/
			
			
            	$query="
            		UPDATE
            			".__DBC_SCHEMATA_USERS__."
            		SET
            			szCompanyName='".mysql_escape_custom($this->szCompanyName)."',
            			szCompanyRegNo='".mysql_escape_custom($this->szCompanyRegNo)."',
						szFirstName='".mysql_escape_custom(ucwords($this->szFirstName))."',
						szLastName='".mysql_escape_custom(ucwords($this->szLastName))."',
						szAddress='".mysql_escape_custom($this->szAddress1)."',
						szAddress2='".mysql_escape_custom($this->szAddress2)."',
						szAddress3='".mysql_escape_custom($this->szAddress3)."',
						szCity='".mysql_escape_custom($this->szCity)."',
						szState='".mysql_escape_custom($this->szState)."',
						idCountry='".mysql_escape_custom($this->szCountry)."',
						szPostCode='".mysql_escape_custom($this->szPostcode)."',
						szCurrency='".mysql_escape_custom($data['szCurrency'])."',
						szEmail='".mysql_escape_custom($this->szEmail)."',
	            		szPhone='".mysql_escape_custom($phoneNumber)."',
	            		iAcceptNewsUpdate='".(int)$data['iSendUpdate']."'
						";
            	/*if($this->szCountry!=$data['szOldCountry'])
				{
					$query .="
							,szPhone='".mysql_escape_custom($szPhoneNumber)."'
					";
				}*/
				$query .="
            		WHERE
            			id='".(int)$this->id."'
            	";
            	//echo $query;
            	$result = $this->exeSQL( $query );
            	
            	//$this->checkIncompleteUserProfile($this->id);
            	return true;
			
		}
		else
		{
			return false;
		}
	}
	function checkIncompleteUserProfile($idUser)
	{
            if($idUser>0)
            {
                if(!$this->isUserProfileCompleted($idUser))
                {
                    //profile is completed
                    $quey_update = " iIncompleteProfile = '0' ";
                }
                else 
                {
                    //incomplete profile 
                    $quey_update = " iIncompleteProfile = '1' ";
                }

                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_USERS__."
                    SET
                        $quey_update
                    WHERE
                        id = '".(int)$idUser."'
                ";

                if($result = $this->exeSQL( $query ))
                {
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
	
	function removeUserByAdmin($id,$flag)
	{
		if((int)$id>0)
		{
			if($flag=='confirm')
			{
				$query="
					UPDATE
						".__DBC_SCHEMATA_USERS__."
					SET
						idGroup='0'
					WHERE
						id='".(int)$id."'
					AND
					    iActive = '1'	
				";
			
			}else if($flag=='pending')
			{
				$query="
					DELETE FROM
						".__DBC_SCHEMATA_USER_INVITE_HISTORY__."
					WHERE
						id='".(int)$id."'							
				";
			}
			if( ( $result = $this->exeSQL( $query ) ) )
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	function checkEmailAddressByAdmin($data)
	{
		$this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szEmail']))));	
		$idUser = trim(sanitize_all_html_input(strtolower($data['idUser'])));
		if(!empty($this->szEmail) && !$this->userEmailExits($this->szEmail))
		{
			$this->addError( "szEmail" , t($this->t_base_access.'messages/email_not_exists') );
		}
		else if(($this->idMappedUserId>0) && ($this->idMappedUserId == $idUser))
		{
			$this->addError( "szEmail" , t($this->t_base_access.'messages/map_with_self') );
			$this->idMappedUserId = 0;
		}
		
		if(!empty($this->szEmail) && $this->isUserEmailAlreadyMapped($this->szEmail))
		{
			$this->iAlreadyMapperUserError = true;
			//$this->addError( "szEmail" , t($this->t_base_access.'messages/already_mapped') );
			return false;
		}
		//exit if error exist.
		if ($this->error === true)
		{
			return array();
		}
		$idUser = $_SESSION['user_id'];
		$query="
			SELECT
				id,
				szFirstName,
				szLastName
			FROM
				".__DBC_SCHEMATA_USERS__."
			WHERE
				szEmail = '".mysql_escape_custom($this->szEmail)."'
			AND
				id !='".(int)$idUser."'	
			AND
				iActive = '1'	
		";
		if( ( $result = $this->exeSQL( $query ) ) )
		{
        	// Check if the user exists
            if( $this->getRowCnt() > 0 )
            {
            	while($row=$this->getAssoc($result))
            	{
            		$userDetails[]=$row;
            	}
            	return $userDetails;
            }
		}
		else
		{
			return array();
		}
	}
	
	function deleteUserByAdmin($idUser,$flag)
	{
		if($idUser>0)
		{
			if($flag=='delete')
			{
				$value='0';
			}
			else if($flag=='reactive')
			{
				$value='1';
			}
			
			$query="
				UPDATE
					".__DBC_SCHEMATA_USERS__."
				SET
					iActive='".(int)$value."'
				WHERE
					id='".(int)$idUser."'
			";
			if($result = $this->exeSQL( $query ))
			{
				if($flag=='delete' && $this->deleteUserInviteHistory((int)$idUser))
				{
					return true;
				}
				
				return true;
			}
			else
			{
				return false;	
			}
		}
	}
	function sendShareSiteEmail($data)
	{
		if(!empty($data))
		{
			$this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szEmail']))));
			$this->set_szEmailSubject(trim(sanitize_all_html_input($data['szSubject'])));
			$this->set_szEmailMessage(trim($data['szMessage']));
			
			//exit if error exist.
			if ($this->error === true)
			{
				return array();
			}
			ob_start();  
		    require_once(__APP_PATH_ROOT__.'/layout/email_header.php');
		    $message = ob_get_clean();
		    
			$message .= nl2br($this->szEmailMessage);
			
			ob_start();
		    require_once(__APP_PATH_ROOT__.'/layout/email_footer.php');
		    $message .= ob_get_clean();
    
			$kWhsSearch = new cWHSSearch();
			$to = $this->szEmail ;
			$from = $kWhsSearch->getManageMentVariableByDescription('__CONTACT_EMAIL__');
			$subject = $this->szEmailSubject ;
		
			$reply_to = $from ;
			$idUser = $_SESSION['user_id'];
                        /*
                        * building block for send Transporteca E-mail
                        */
                        $mailBasicDetailsAry = array();
                        $mailBasicDetailsAry['szEmailTo'] = $to;
                        $mailBasicDetailsAry['szEmailFrom'] = $from; 
                        $mailBasicDetailsAry['szEmailSubject'] = $subject;
                        $mailBasicDetailsAry['szEmailMessage'] = $message;
                        $mailBasicDetailsAry['szReplyTo'] = $reply_to;
                        $mailBasicDetailsAry['idUser'] = $idUser;   
                        $kSendEmail = new cSendEmail();
                        $kSendEmail->sendEmail($mailBasicDetailsAry);
                        
			//sendEmail($to,$from,$subject,$message,$reply_to, $idUser);
			return true;
		}
	}

	function shortUserSignup($data)
	{
		if(is_array($data))
		{	
			$this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szNewEmail']))));
				
				$query="
					INSERT INTO
						".__DBC_SCHEMATA_USERS__."
					(
						szEmail,
						szCurrency,
						iIncompleteProfile,
						dtCreateOn,
						iActive
					)
						VALUES
					(				
						'".mysql_escape_custom($this->szEmail)."',
						'".mysql_escape_custom($data['idCurrency'])."',
						'1',
						NOW(),
						'1'
					)
				";
				
				if( ( $result = $this->exeSQL( $query ) ) )
				{
					$this->id = $this->iLastInsertID;
					
					$_SESSION['user_id']=$this->id;
					
					if(__ENVIRONMENT__ == "LIVE")
					{
						setcookie("__USER_EMAIL_COOKIE__", $this->szEmail, time()+(3600*24*90),'/',__BASE_URL_SECURE__,true);
					}
					else
					{
						setcookie("__USER_EMAIL_COOKIE__", $this->szEmail, time()+(3600*24*90),'/');
					}     
					return true;
				}
				else
				{
					return false;
				}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() requires an array.";
			$this->logError( "input", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	function isUserProfileCompleted($idUser)
	{
            if($idUser>0)
            {
                $query="
                    SELECT
                        id
                    FROM
                        ".__DBC_SCHEMATA_USERS__."
                    WHERE
                        id='".(int)$idUser."'
                    AND
                    (
                       szFirstName = ''
                    OR
                        szAddress =''
                    OR
                        szCity = ''
                    OR
                        idCountry =''
                     OR
                        szCompanyName = ''
                     OR
                        szCompanyRegNo = ''
                     OR
                        szLastName = ''
                     OR
                         szEmail = ''
                     OR
                         szPassword = ''
                     OR
                         szPostCode = ''
                     OR
                         szCurrency = ''
                    )
                ";

                //echo $query;
                if($result = $this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
	
	function sendEmailNonRegisterUser($szEmail)
	{
            if(!empty($szEmail))
            {
                $this->getUserDetails($_SESSION['user_id']);

                $replace_ary['szName']=$this->szFirstName." ".$this->szLastName;
                $replace_ary['szFirstName']=$this->szFirstName;
                $szLink=__CREATE_ACCOUNT_PAGE_URL__."?szEmail=".$szEmail;
                $replace_ary['szLink']='<a href="'.$szLink.'">CLICK HERE TO CREATE A PROFILE</a>';
                $replace_ary['szCopyLink']=$szLink; 
                createEmail(__INVITATION_TO_CONNECT_ON_TRANSPORTECA__, $replace_ary,$szEmail, '', __STORE_SUPPORT_EMAIL__,'', __STORE_SUPPORT_EMAIL__,'');
                return true;
            }
            else
            {
                return false;
            }
	}
        
        function addIPlocationBlack($data)
        { 
            if(!empty($data))
            {
                $iRowCounter = 0;
                foreach($data as $datas)
                { 
                    if(!empty($datas))
                    {
                        foreach($datas as $datass)
                        {
                            $query=" INSERT INTO ".__DBC_SCHEMATA_IP_LOCATION__." (idLocationDetails,iStartIpLocation,iEndIpLocation,dtCreatedOn,iActive) VALUES ( '".$datass['idLocationDetails']."','".$datass['iStartIpLocation']."','".$datass['iEndIpLocation']."',now(),1 ) ";
                            if($result = $this->exeSQL( $query ))
                            {
                                $iRowCounter++;
                            }
                        }
                    }
                }
                $this->iRowCounter = $iRowCounter ;
                return true ;
            }
        }
        function addIPlocationDetails($data)
        { 
            if(!empty($data))
            {
                $iRowCounter = 0;
                foreach($data as $datas)
                { 
                    if(!empty($datas))
                    {
                        foreach($datas as $datass)
                        {
                            $query=" 
                                INSERT INTO ".__DBC_SCHEMATA_IP_LOCATION_DETAILS__." 
                                (
                                    idLocationDetails,
                                    szCountryCode,
                                    szCountryName,
                                    idCountry,
                                    szRegion,
                                    szCity,                                     
                                    szPostcode,
                                    szLatitute,
                                    szLongitute,
                                    dtCreatedOn,
                                    iActive
                                )
                                VALUES 
                                ( 
                                    '".mysql_escape_custom(trim($datass['idLocationDetails']))."',
                                    '".mysql_escape_custom(trim($datass['szCountryCode']))."',
                                    '".mysql_escape_custom(trim($datass['szCountryName']))."',
                                    '".mysql_escape_custom(trim($datass['idCountry']))."',
                                    '".mysql_escape_custom(trim(utf8_encode($datass['szRegion'])))."',
                                    '".mysql_escape_custom(trim(utf8_encode($datass['szCity'])))."', 
                                    '".mysql_escape_custom(trim(utf8_encode($datass['szPostcode'])))."',
                                    '".mysql_escape_custom(trim($datass['szLatitute']))."',
                                    '".mysql_escape_custom(trim($datass['szLongitute']))."',
                                    now(),
                                    1 
                                ) 
                            "; 
                            //echo " Query:  ".$query ;
                            if($result = $this->exeSQL( $query ))
                            {
                                $iRowCounter++; 
                            } 
                        }
                    }
                }
                $this->iRowCounter = $iRowCounter ;
                return true ;
            }
        }
        
        function getDetailsByIP($szIpAddress)
        {
            if(!empty($szIpAddress))
            {
                $query=" 
                    SELECT 
                        idLocationDetails
                    FROM 
                        ".__DBC_SCHEMATA_IP_LOCATION__."
                    WHERE 
                        INET_ATON('".mysql_escape_custom(trim($szIpAddress))."')
                    BETWEEN 
                        `iStartIpLocation`
                    AND 
                        `iEndIpLocation` 
                 ";
//                echo $query ;
//                die;
                if($result = $this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        $row = $this->getAssoc($result); 
                        
                        $idLocationDetails = $row['idLocationDetails'] ; 
                        $ret_ary = array();
                        if(!empty($idLocationDetails))
                        {
                            $ret_ary = $this->getCityDetailsByLocationID($idLocationDetails);
                            return $ret_ary ;
                        }
                        else
                        {
                            return $ret_ary ;
                        }
                    }
                    else
                    {
                        return array();
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
                   
            }
        }
        function addVisitorsRedirectLogs($data)
        {
            if(!empty($data))
            {
                $query="
                    INSERT INTO
                        ".__DBC_SCHEMATA_VISITORS_REDIRECT_LOGS__."
                    (
                        idUser,
                        szIPAddress,
                        szReferer,
                        szUserAgent,
                        isRedirected,
                        iFoudBYAPI,
                        szApiResponseData,
                        szCountryCode,
                        dtCreatedOn
                    )
                    VALUES
                    ( 
                        '".mysql_escape_custom(trim($data['idUser']))."',
                        '".mysql_escape_custom(trim($data['szIPAddress']))."',
                        '".mysql_escape_custom(trim($data['szReferer']))."',
                        '".mysql_escape_custom(trim(__HTTP_USER_AGENT__))."',
                        '".mysql_escape_custom(trim($data['isRedirected']))."',
                        '".mysql_escape_custom(trim($data['iFoudBYAPI']))."',
                        '".mysql_escape_custom(trim($data['szApiResponseData']))."',
                        '".mysql_escape_custom(trim($data['szCountryCode']))."',
                        now()
                    )
                ";
                //echo $query; 
                if( ( $result = $this->exeSQL( $query ) ) )
                {
                    return true;
                }
                else 
                {
                    return false;
                }
            }
        }
        function getCityDetailsByLocationID($idLocationDetails)
        {
            if(!empty($idLocationDetails))
            {
                $query=" 
                    SELECT 
                        id,
                        idLocationDetails,
                        szCountryCode,
                        szCountryName,
                        idCountry,
                        szRegion,
                        szLongitute, 
                        szPostcode,
                        szLatitute,
                        szCity
                    FROM 
                        ".__DBC_SCHEMATA_IP_LOCATION_DETAILS__."
                    WHERE 
                        idLocationDetails = '".mysql_escape_custom(trim($idLocationDetails))."' 
                 ";
                if($result = $this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        $row = $this->getAssoc($result);  
                        return $row ; 
                    }
                    else
                    {
                        return array();
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
                   
            }
        }
        
        function getAllCrmIdsLogByUserid($idCustomer=false,$szCapsulePrtyID=false,$iGetAll=false)
        { 
            if($idCustomer>0 || $iGetAll)
            { 
                /*if(!empty($szCapsulePrtyID))
                {
                    $query_and = " AND szCapsulePrimaryID = '".mysql_escape_custom($szCapsulePrtyID)."' ";
                }*/
                if($idCustomer>0)
                {
                    $query_and = " AND idUser = '".mysql_escape_custom($idCustomer)."' ";
                }
                
                $query="
                    SELECT
                        id,
                        idUser,
                        szCapsulePrimaryID,
                        szCapsuleAddressID,
                        szCapsuleEmailID,
                        szCapsulePhoneID,
                        dtCreatedOn,
                        iActive
                    FROM
                        ".__DBC_SCHEMATA_CAPSULE_CRM_ID_LOGS__."
                    WHERE 
                        iActive ='1'
                     $query_and
                "; 
                //echo "<br>".$query;
                if(($result = $this->exeSQL($query)))
                { 
                    if($iGetAll)
                    {
                        $ret_ary = array();
                        while($row=$this->getAssoc($result))
                        {
                            $ret_ary[] = $row['idUser'] ;
                        }
                        return $ret_ary ;
                    }
                    else
                    {
                        $row=$this->getAssoc($result); 
                        return $row; 
                    } 
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
        }
        
        function createAndVerifyUserAccount($data)
        {
            if(!empty($data['szEmail']))
            {
                $kRegisterShipCon = new cRegisterShipCon();
                if($this->isUserEmailExist($data['szEmail']))
                {
                    /*if($this->iIncompleteProfileExists)
                    {
                        //If user email already exists with the existing email then we just deactivate that account and create a new one.
                        $this->deactivateUserAccount($this->idIncompleteUser);
                        $iAddNewUser = true; 
                    }
                    else
                    {*/
                        //give an error that email is already exists
                        $idCustomer = $this->idIncompleteUser ; 
                        $this->idNewAddedUser = $idCustomer ;
                        
                        $updateUserDetailsAry = array();  
                        $updateUserDetailsAry['iConfirmed'] = 1 ;  
                        
                        if(!empty($updateUserDetailsAry))
                        {
                            $update_user_query = '';
                            foreach($updateUserDetailsAry as $key=>$value)
                            {
                                $update_user_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                            }
                        }  
                        $update_user_query = rtrim($update_user_query,",");  
                        $kRegisterShipCon->updateUserDetailsByQuotes($update_user_query,$idCustomer);
                        return true;
                   // }
                }
                else
                {
                    $iAddNewUser = true;
                }
                
                if($iAddNewUser)
                { 
                    $szPassword = mt_rand(0,99)."".$kRegisterShipCon->generateUniqueToken(8);

                    //User is not loogedin then we create a new profile
                    $addUserAry = array();
                    $addUserAry['szFirstName'] = $data['szFirstName'] ;
                    $addUserAry['szLastName'] = $data['szLastName'] ;
                    $addUserAry['szEmail'] = $data['szEmail'] ;
                    $addUserAry['szPassword'] = $szPassword ;
                    $addUserAry['szConfirmPassword'] = $szPassword ;   
                    $addUserAry['iConfirmed'] = 1; 
                    $addUserAry['iFirstTimePassword'] = 1;   
                    $addUserAry['szCompanyRegNo'] = $data['szCustomerCompanyRegNo'];
                    $addUserAry['szCompanyName'] = $data['szCustomerCompanyName'];
                    
                    if($this->createAccount($addUserAry,'RFQ_VERIFY_BUTTON'))
                    { 
                        $idCustomer = $this->id ;     
                        $kRegisterShipCon->updateCapsuleCrmDetails($idCustomer); 
                        $this->idNewAddedUser = $idCustomer ;
                        return true;
                    } 
                    else
                    {
                        return false;
                    }
                }
            }
        }
        
        function validatePassword($idCustomer,$szCurrentPassword)
        {
            if($idCustomer>0 && !empty($szCurrentPassword))
            {
                $this->iCustomerNotFound = false;
                $this->iCustomerPasswordNotMatched = false;
                
                $query="
                    SELECT
                        szPassword
                    FROM
                        ".__DBC_SCHEMATA_USERS__."
                    WHERE
                        id='".(int)$idCustomer."'
                ";
                //echo $query;
                if( ( $result = $this->exeSQL( $query ) ) )
                {
                    // Check if the user exists
                    if( $this->getRowCnt() > 0 )
                    {
                        $row=$this->getAssoc($result);
                        $szPassword=$row['szPassword'];
                    }
                }
                else
                {
                    $this->iCustomerNotFound = 1;
                    return false;
                }
                
                if($szPassword==md5($szCurrentPassword))
                { 
                    return true;
                }
                else
                {
                    $this->iCustomerPasswordNotMatched = 1;
                    return false;
                }
            }
        }
        
        function updateUserPassword($idCustomer,$szPassword)
        {
            if($idCustomer>0 && !empty($szPassword))
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_USERS__."
                    SET
            		szPassword='".mysql_escape_custom(md5($szPassword))."'
                    WHERE
                        id='".(int)$idCustomer."'
                ";
                //echo $query;
                if($result = $this->exeSQL( $query ))
                {
                    return true;
                } 
                else
                {
                    return false;
                } 
            }
        }
        
        /**
	 * Fetchig the data of last booking made by customer
	 *
	 * @access public
	 * @param int $idUser
	 * @return array
	 * @author Ajay
	 */
	 
	public function getLastBookingByUserID($idUser)
	{
            if($idUser>0)
            {
                $query="
                    SELECT
                        id,  
                        szBookingRandomNum,
                        idOriginCountry, 
                        szOriginCountry,
                        szOriginPostCode,
                        szOriginCity, 
                        szDestinationAddress,
                        szOriginAddress, 					
                        idDestinationCountry,	 
                        szDestinationCountry,
                        szDestinationPostCode,
                        szDestinationCity,  
                        idCustomerCurrency as idCurrency,
                        szCustomerCurrency as szCurrency,
                        fCustomerExchangeRate as fExchangeRate, 
                        fCargoVolume,
                        fCargoWeight,
                        idBookingStatus,
                        fTotalPriceCustomerCurrency,     
                        szBookingRef
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__."
                    WHERE
                        idUser = '".(int)$idUser."'		
                    AND
                        idBookingStatus IN (3,4)
                    ORDER BY
                        dtBookingConfirmed DESC
                    LIMIT
                        0,1
		";
                //echo $query;
                if($result = $this->exeSQL($query))
                {
                    $bookingAry=array();
                    if($this->iNumRows>0)
                    {
                        $row=$this->getAssoc($result); 
                        $bookingAry = $row;
                        return $bookingAry ;
                    }
                    else
                    {
                        return array();
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }		
	}
        
        
        function userSignupFormHoldingPage($data)
        {
            $this->set_szEmail(sanitize_all_html_input(trim($data['szEmail'])));
            $this->set_idFromCountry(sanitize_all_html_input(trim($data['idFromCountry'])));
            $this->set_idToCountry(sanitize_all_html_input(trim($data['idToCountry'])));
            
            
            //exit if error exist.
            if ($this->error === true)
            {
                    return false;
            }
            
            $kConfig = new cConfig();
            $allCountriesArr = array();
            $allCountriesArr=$kConfig->getAllCountries(true);
            
            if(!empty($allCountriesArr))
            {
                foreach($allCountriesArr as $allCountriesArrs)
                {
                    if(strtolower(trim($data['idIpCountry']))== strtolower(trim($allCountriesArrs['szCountryISO'])))
                    {
                        $idIpCountry=$allCountriesArrs['id'];
                    }                    
                    if(strtolower(trim($this->idFromCountry))== strtolower(trim($allCountriesArrs['szCountryISO'])))
                    {
                        $idFromCountry=$allCountriesArrs['id'];
                    }
                    
                    if(strtolower(trim($this->idToCountry))== strtolower(trim($allCountriesArrs['szCountryISO'])))
                    {
                        $idToCountry=$allCountriesArrs['id'];
                    }
                }
            }
           
            
            $query="
                INSERT INTO
                    ".__DBC_SCHEMATA_SIGNUPS_DATA__."
                (
                    szCustomerEmail, 
                    idIpCountry,
                    idLanguage,
                    idOriginCountry,
                    idDestinationCountry,
                    dtSignup,
                    idLandingPage
                )
                    VALUES
                (
                    '".  mysql_escape_custom($this->szEmail)."',
                    '".  mysql_escape_custom($idIpCountry)."',
                    '".  mysql_escape_custom($data['idLanguage'])."',
                    '".  mysql_escape_custom($idFromCountry)."',
                    '".  mysql_escape_custom($idToCountry)."',
                    NOW(),
                    '".  mysql_escape_custom($data['idLandingPage'])."'
                )                
            ";
            //echo $query;
            if($result = $this->exeSQL($query))
            {
                return true;
            }
            
        }
        
        function updateCustomerExpiryTime($data)
        {
            if($data['szToken']!='' && (int)$data['idCustomer']>0)
            {
                $inserFlag=true;
                $query=" 
                    SELECT 
                        id
                    FROM 
                        ".__DBC_SCHEMATA_CUSTOMER_API_LOGIN__."
                    WHERE 
                        szToken='".  mysql_escape_custom($data['szToken'])."'
                    AND
                        idCustomer='".(int)$data['idCustomer']."'
                 ";
                if($result = $this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        $inserFlag=false;
                    }
                }
                
                $kBooking = new cBooking();
                $dtExpiredTime = $kBooking->getRealNow();                
                $dtExpired = date('Y-m-d H:i:s',strtotime("+".__TIME_FOR_EXPIRY_TOKEN__,strtotime($dtExpiredTime)));
                if($inserFlag)
                {
                    $query="
                        INSERT INTO
                            ".__DBC_SCHEMATA_CUSTOMER_API_LOGIN__."
                        (
                            idCustomer,
                            szToken,
                            dtExpired,
                            iLoginType,
                            dtCreated
                        )
                            VALUES
                        (
                            '".(int)$data['idCustomer']."',
                            '".  mysql_escape_custom($data['szToken'])."',
                            '".  mysql_escape_custom($dtExpired)."',
                            '".(int)$data['iLoginType']."',
                            NOW()    
                        )
                    ";
                }
                else
                {
                    
                    $query=" 
                        UPDATE
                            ".__DBC_SCHEMATA_CUSTOMER_API_LOGIN__."
                        SET
                            dtExpired= '".  mysql_escape_custom($dtExpired)."'
                        WHERE 
                            szToken='".  mysql_escape_custom($data['szToken'])."'
                        AND
                            idCustomer='".(int)$data['idCustomer']."'
                    ";
                }
                
                $result = $this->exeSQL($query);
            }
        }
        
	function set_id( $value )
	{
            $this->id = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "id", t($this->t_base.'fields/customer_id'), false, false, true );
	}
	function set_idCurrency( $value )
	{
            $this->idCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idCurrency", t($this->t_base.'messages/booking_currency'), false, false, true );
	}
	
	function set_szFirstName( $value,$required_flag=true )
	{
            $this->szFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szFirstName", t($this->t_base.'fields/f_name'), false, 255, $required_flag );
	}
	function set_szLastName( $value,$required_flag=true )
	{
            $this->szLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLastName", t($this->t_base.'fields/l_name'), false, 255, $required_flag );
	}
	
	function set_szEmailSubject( $value )
	{
            $this->szEmailSubject = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szEmailSubject", t($this->t_base.'fields/email_subject'), false, 255, true );
	}
	function set_szEmailMessage( $value )
	{
            $this->szEmailMessage = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szMessage", t($this->t_base.'fields/email_message'), false, 255, true );
	}
	
	function set_szEmail( $value,$required_flag=true )
	{
            $this->szEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szEmail", t($this->t_base.'fields/email'), false, 255, $required_flag );
	}
	function set_szPassword( $value )
	{
            $this->szPassword = $this->validateInput( $value, __VLD_CASE_PASSWORD_NOFORCE_, "szPassword", t($this->t_base.'fields/password'), false, 255, true );
	}
	function set_szRetypePassword( $value )
	{
            $this->szRetypePassword = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szRetypePassword", t($this->t_base.'fields/re_password'), false, 255, true );
	}
	function set_szCity( $value,$required_flag=true )
	{
            $this->szCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCity", t($this->t_base.'fields/city'), false, 255, $required_flag );
	}
	function set_szState( $value ,$required_flag=false)
	{
            $this->szState = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szState", t($this->t_base.'fields/p_r_s'), false, 255, false );
	}
	function set_szCountry( $value,$required_flag=true )
	{
            $this->szCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCountry", t($this->t_base.'fields/country'), false, 255, $required_flag );
	}
	function set_szPhoneNumber( $value,$required_flag=false )
	{
            $this->szPhoneNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPhoneNumber", t($this->t_base.'fields/p_number'), false, false, $required_flag );
	}
        function set_idInternationalDialCode( $value,$required_flag=false )
	{
            $this->idInternationalDialCode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idInternationalDialCode", t($this->t_base.'fields/p_number'), false, false, $required_flag );
	}
        
	function set_szCompanyName( $value,$required_flag=true )
	{
            $this->szCompanyName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCompanyName", t($this->t_base.'fields/c_name'), false, 255, $required_flag );
	}
	function set_szCompanyRegNo($value,$required_flag=true)
	{
            $this->szCompanyRegNo = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCompanyRegNo", t($this->t_base.'fields/c_reg_n'), false, 255, $required_flag );
	}
	function set_szAddress1( $value,$required_flag=true )
	{
            $this->szAddress1 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAddress1", t($this->t_base.'fields/address_line1'), false, 255, $required_flag );
	}
	function set_szAddress2( $value )
	{
            $this->szAddress2 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAddress2", t($this->t_base.'fields/address_line2'), false, 255, false );
	}
	function set_szAddress3( $value )
	{
            $this->szAddress3 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAddress3", t($this->t_base.'fields/address_line3'), false, 255, false );
	}
        function set_szPostcode( $value,$required_flag=true )
	{
            $this->szPostcode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPostcode", t($this->t_base.'fields/postcode'), false, 255, $required_flag);
	}
	
	function set_szOldPassword( $value )
	{
            $this->szOldPassword = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szOldPassword", t($this->t_base.'fields/current_password'), false, 255, true );
	}
	
	function set_szConNewPassword( $value )
	{
            $this->szConNewPassword = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConPassword", t($this->t_base.'fields/con_new_password'), false, 255, true );
	}
	
	function set_szNonFrocePassword( $value )
	{
            $this->szNonFrocePassword = $this->validateInput( $value, __VLD_CASE_PASSWORD_NOFORCE_, "szPassword", t($this->t_base.'fields/password'), false, 255, true );
	}
	
	function set_szMessage( $value )
	{
            if(empty($value))
            {
                    $this->addError('szMessage',t($this->t_base.'fields/message'));
                    return false;
            }
            $this->szMessage = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szMessage", t($this->t_base.'fields/message'), false, 255, true );
	}
	
	function set_szContactEmail( $value )
	{
		$this->szContactEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szEmail", t($this->t_base.'fields/email'), false, 255, true );
	}
        
        
        function set_idFromCountry( $value,$required_flag=true )
	{
            $this->idFromCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idFromCountry", t($this->t_base.'fields/country'), false, 255, $required_flag );
	}
        
        function set_idToCountry( $value,$required_flag=true )
	{
            $this->idToCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idToCountry", t($this->t_base.'fields/country'), false, 255, $required_flag );
	}
        
        
        function set_iLanguage( $value )
	{
            $this->iLanguage = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iLanguage", t($this->t_base.'messages/language'), false, false, true );
	}
        
}
?>
