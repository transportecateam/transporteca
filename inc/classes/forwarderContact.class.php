<?php
/**
 * This file is the container for all forwarders related functionality.
 * All functionality related to forwarders details should be contained in this class.
 *
 * forwarder.class.php
 *
 * @copyright Copyright (C) 2012 Transporteca, LLC
 * @author Anil
 * @package Transporteca Development
 */
if (!isset($_SESSION)) 
{
  session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" ); 
require_once( __APP_PATH_CLASSES__ . "/database.class.php");

class cForwarderContact extends cDatabase
{
    /**
     * These are the variables for each user.
     */

    var $id; 
    var $szFirstName;
    var $szLastName;
    var $szEmail;
    var $szOldEmail;
    var $szPassword;
    var $szReTypePassword;
    var	$szOldPassword;
    var $szPhoneNumber;
    var $szCountry;
    var $szCompanyName;
    var $szWebsite;
    var $szDisplayName;
    var $szLink;
    var $t_base="Forwarders/UserAccess/";
    var $szControlUrl;
    var $controlBooking;
	var	$controlPayment;
	var	$controlRss;
	var $szNonFrocePassword; 
	var $firstActiveAdmin;
	var $iConfirmed;
	var $szBookingEmail=array();
	var $szPaymentEmail=array();
	var $szCustomerEmail=array();
	var $mainAdminEmail;
	var $idForwarderRole;
	var $szMessage;
	var $szContact;
	var $idForwarderContactRole;
	var $idCountry;
	var $szControlBookingReference;
	var $fRefFee;
	var $szComment;
	var $dtValidTo;
	var $iAutomaticPaymentInvoicing;
	var $szCurrency;
	var $iOldCurrency;
	var $szCurrencyName;
	var $iPasswordUpdated;
	private $iCurrencyFlag;
	/**
	 * class constructor for php5
	 *
	 * @return bool
	 */
	function __construct()
	{
		parent::__construct();
		
		// establish a Database connection.
		//$this->connect( __DBC_USER__, __DBC_PASSWD__, __DBC_SCHEMATA__, __DBC_HOST__);
		return true;
	}
	
	function addForwarderContact($data)
	{
		if(!empty($data))
		{
			$this->set_szFirstName(trim(sanitize_all_html_input($data['szFirstName'])));
			$this->set_szLastName(trim(sanitize_all_html_input($data['szLastName'])));
			$this->set_idCountry(trim(sanitize_all_html_input($data['idCountry'])));
			$this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szEmail']))));
			$this->set_szPhoneNumber(trim(sanitize_all_html_input(urlencode($data['szPhone']))));
			$this->set_szMobileNumber(trim(sanitize_all_html_input(urlencode($data['szMobile']))));			
			$this->set_idForwarderRole(trim(sanitize_all_html_input($data['idForwarderRole'])));
			//exit if error exist.
			if ($this->error === true)
			{
				return false;
			}
			
			if(!empty($this->szPhoneNumber))
			{
				$str = substr($this->szPhoneNumber, 0, 1); 
				$substr=substr($this->szPhoneNumber,1);
				$phone=str_replace("+"," ",$substr);
				if(!empty($phone))
				{
					$szPhoneNumber=$str."".$phone;
				}
			}
			
			if(!empty($this->szMobileNumber))
			{
				$str = substr($this->szMobileNumber, 0, 1); 
				$substr=substr($this->szMobileNumber,1);
				$phone=str_replace("+"," ",$substr);				//echo $phone;
				if(!empty($phone))
				{
					$szMobileNumber=$str."".$phone;
				}
			}
			
			$query="
				INSERT INTO
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
				(
					idForwarder,
					szFirstName,
					szLastName,
					idCountry,
					szPhone,
					szMobile,
					szEmail,
					idForwarderContactRole,
					szPassword,
					dtCreatedOn,
					iActive
				)	
				VALUES
				(
					'".(int)$this->idForwarder."',
					'".mysql_escape_custom(trim($this->szFirstName))."',
					'".mysql_escape_custom(trim($this->szLastName))."',
					'".(int)$this->idCountry."',
					'".mysql_escape_custom(trim($szPhoneNumber))."',
					'".mysql_escape_custom(trim($szMobileNumber))."',
					'".mysql_escape_custom(trim($this->szEmail))."',
					'".(int)$this->idForwarderRole."',
					'',
					NOW(),
					'1'
				)
			";
			if($result=$this->exeSQL($query))
			{
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function updateForwarderContact($data)
	{
		if(!empty($data))
		{
			$this->set_szFirstName(ucfirst(trim(sanitize_all_html_input($data['szFirstName']))));
			$this->set_szLastName(ucfirst(trim(sanitize_all_html_input($data['szLastName']))));
			$this->set_idCountry(trim(sanitize_all_html_input($data['idCountry'])));
			$this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szEmail']))));
			$this->set_szPhoneNumber(trim(sanitize_all_html_input(urldecode(base64_decode($data['szPhoneUpdate'])))));
			$this->set_szMobileNumber(trim(sanitize_all_html_input(urldecode(base64_decode($data['szMobileUpdate'])))));			
			$this->set_idForwarderRole(trim(sanitize_all_html_input($data['idForwarderRole'])));
			$this->set_idForwarder(trim(sanitize_all_html_input($data['idForwarder'])));
			$this->set_id(trim(sanitize_all_html_input($data['id'])));
			
			//exit if error exist.
			if ($this->error === true)
			{
				return false;
			}
			if($this->isForwarderEmailExist($this->szEmail,$this->id))
			{
				$this->addError( "szEmail" , t($this->t_base.'messages/email_exists') );
				return false;
			}
			//echo "<br> phone number ".$this->szPhoneNumber ;
			if(!empty($this->szPhoneNumber))
			{
				$szPhoneNumber = $this->szPhoneNumber;
			}
			
			if(!empty($this->szMobileNumber))
			{
				$szMobileNumber=$this->szMobileNumber;
			}
			
			$query="
				UPDATE 
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
				SET
					idForwarder = '".(int)$this->idForwarder."',
					szFirstName = '".mysql_escape_custom(trim($this->szFirstName))."',
					szLastName = '".mysql_escape_custom(trim($this->szLastName))."',
					idCountry = '".(int)$this->idCountry."',
					szPhone = '".mysql_escape_custom(trim($szPhoneNumber))."',
					szMobile = '".mysql_escape_custom(trim($szMobileNumber))."',
					szEmail = '".mysql_escape_custom(trim($this->szEmail))."',
					idForwarderContactRole = '".(int)$this->idForwarderRole."',
					iActive = '1'
				WHERE
					id = '".(int)$this->id."'	
			";
			if($result=$this->exeSQL($query))
			{
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function loadContactRole($idForwarderContactrole)
	{
		if($idForwarderContactrole>0)
		{
			$query="
				SELECT
					role.id,
					role.szRoleName
				FROM
					".__DBC_SCHEMATA_FORWARDERS_CONTACT_ROLE__." role
				WHERE
					role.id = '".(int)$idForwarderContactrole."'	
			";
			//echo "<br>".$query ;
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					$this->idForwarderContactRole=sanitize_all_html_input(trim($row['id']));
					$this->szRoleName=sanitize_all_html_input(trim($row['szRoleName']));					
					return true ;
				}
				else
				{
					return false ;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function load($idForwarderContact)
	{
		if($idForwarderContact>0)
		{
			$query="
				SELECT
					c.idForwarder,
					c.szFirstName,
					c.szLastName,
					c.idCountry,
					c.szPhone,
					c.szMobile,
					c.szEmail,
					c.idForwarderContactRole,
					c.szPassword,
					c.iActive,
					c.iConfirmed,
					role.szRoleName,
					c.iHaulageVideo,
					c.iLCLServiceVideo
				FROM
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."	c
				INNER JOIN
					".__DBC_SCHEMATA_FORWARDERS_CONTACT_ROLE__." role
				ON
					role.id = c.idForwarderContactRole 		
				WHERE
					c.id = '".(int)$idForwarderContact."'	
			";
			//echo "<br>".$query ;
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					$this->idForwarder=sanitize_all_html_input(trim($row['idForwarder']));
					$this->szFirstName=sanitize_all_html_input(trim($row['szFirstName']));					
					$this->szLastName=sanitize_all_html_input(trim($row['szLastName']));
					$this->idCountry=sanitize_all_html_input(trim($row['idCountry']));
					$this->szPhone=sanitize_all_html_input(trim($row['szPhone']));
					$this->szMobile=sanitize_all_html_input(trim($row['szMobile']));
					$this->szEmail=sanitize_all_html_input(trim($row['szEmail']));
					$this->idForwarderContactRole=sanitize_all_html_input(trim($row['idForwarderContactRole']));
					$this->szPassword=sanitize_all_html_input(trim($row['szPassword']));
					$this->iConfirmed=sanitize_all_html_input(trim($row['iConfirmed']));
					$this->szRoleName=sanitize_all_html_input(trim($row['szRoleName']));
					$this->iHaulageVideo=sanitize_all_html_input(trim($row['iHaulageVideo']));
					$this->iLCLServiceVideo=sanitize_all_html_input(trim($row['iLCLServiceVideo']));			
					return true ;
				}
				else
				{
					return false ;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	//$firstActiveAdmin
	function getMainAdmin($idForwarder)
	{
		if($idForwarder>0)
		{
		$query="
			SELECT
				id, 
				szEmail,
				szFirstName,
				szLastName 
			FROM 
				".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
			WHERE
				idForwarder='".(int)$idForwarder."'
			AND
				iConfirmed='1'
			AND 
				idForwarderContactRole='1'	
			ORDER BY
				id 
			ASC	 
			LIMIT
				0,1	
				";
		//echo $query;
		if($result = $this->exeSQL($query))
		{
		if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					$this->mainAdminEmail=$row['szEmail'];
					$this->szLastName=$row['szLastName'];
					$this->szFirstName=$row['szFirstName'];
					$this->idAdmin=$row['id'];
				}
				else
				{
					return false ;
				}
		}
		else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
        
        //$firstActiveAdmin
	function searchForwarderContacts($szSearchTerms,$idForwarderContact=0)
	{
            if(!empty($szSearchTerms) || (int)$idForwarderContact>0)
            { 
                if(!empty($szSearchTerms))
                {
                    $searchTermsAry = array();
                    $searchTermsAry = explode(" ",$szSearchTerms);
                    $query_and = ' AND ';
                    $ctr = 0;
                    if(!empty($searchTermsAry))
                    {
                        foreach($searchTermsAry as $searchTermsArys)
                        {
                            $query_and .= "
                                (
                                        fc.szFirstName LIKE '%".mysql_escape_custom(trim($searchTermsArys))."%'
                                    OR
                                        fc.szLastName LIKE '%".mysql_escape_custom(trim($searchTermsArys))."%'
                                    OR
                                        fc.szEmail LIKE '%".mysql_escape_custom(trim($searchTermsArys))."%'
                                    OR
                                        fc.szResponsibility LIKE '%".mysql_escape_custom(trim($searchTermsArys))."%'
                                    OR
                                        f.szDisplayName LIKE '%".mysql_escape_custom(trim($searchTermsArys))."%'
                                    OR
                                        f.szCompanyName LIKE '%".mysql_escape_custom(trim($searchTermsArys))."%'
                                ) 
                            "; 
                            $ctr++;
                            if(count($searchTermsAry)!=$ctr)
                            {
                                $query_and .= " AND ";
                            } 
                        }
                    }
                }
                
                if((int)$idForwarderContact>0)
                {
                    $ctr = 0;
                    $query_and = "
                        AND
                            fc.id='".(int)$idForwarderContact."'
                          ";
                }
                
		$query="
                    SELECT
                        fc.id, 
                        fc.szEmail,
                        fc.szFirstName,
                        fc.szLastName,
                        f.szCompanyName,
                        f.szDisplayName,
                        fc.szResponsibility,
                        fc.szPhone,
                        fc.szMobile,
                        fc.idForwarderContactRole,
                        f.szForwarderAlias
                    FROM 
                        ".__DBC_SCHEMATA_FORWARDERS_CONTACT__." fc
                    INNER JOIN
                        ".__DBC_SCHEMATA_FORWARDERS__."	as f
                    ON
                        (f.id = fc.idForwarder)		
                    WHERE 
                        f.iActive = '1'
                    AND
                        fc.iActive = '1' 
                        $query_and
                    ORDER BY
                        szFirstName ASC	  
                ";
		//echo $query;
//                die;
		if($result = $this->exeSQL($query))
		{
                    if($this->iNumRows>0)
                    {
                        while($row=$this->getAssoc($result))
                        {
                            $ret_ary[$ctr] = $row;
                            $ret_ary[$ctr]['szCustomerCompanyName'] = $row['szDisplayName'];
                            $ctr++;
                        } 
                        return $ret_ary;
                    }
                    else
                    {
                        return false ;
                    }
		}
		else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
	function getAllForwarderAdmins($idForwarder)
	{
            if($idForwarder>0)
            {
		$query="
                    SELECT
                        fc.id, 
                        fc.szEmail,
                        fc.szFirstName,
                        fc.szLastName 
                    FROM 
                        ".__DBC_SCHEMATA_FORWARDERS_CONTACT__." as fc
                    INNER JOIN
                        ".__DBC_SCHEMATA_FORWARDERS__."	as f
                    ON
                        (f.id = fc.idForwarder)			
                    WHERE
                        fc.idForwarder='".(int)$idForwarder."'
                    AND
                        fc.iConfirmed='1'
                    AND 
                        fc.iActive = '1'	
                    AND 
                        fc.idForwarderContactRole='1'	
                    AND
                        f.iActive = '1'
                    AND
                        f.isOnline = '1'	
                    ORDER BY
                        id  ASC	 
                ";
		if($result = $this->exeSQL($query))
		{
                    $data = array();
                    if($this->iNumRows>0)
                    {	
                        while($row=$this->getAssoc($result))
                        {
                            $data[] = $row;
                        }
                        return $data;
                    }
                    else
                    {
                        return false ;
                    }
		}
		else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
	function getForwarderContactDetails($idForwarderContact)
	{
		if($idForwarderContact>0)
		{
			$query="
				SELECT
					id,
					idForwarder,
					szFirstName,
					szLastName,
					idCountry,
					szPhone,
					szMobile,
					szEmail,
					idForwarderContactRole,
					szPassword,
					iActive,
					iConfirmed
				FROM
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."	
				WHERE
					id = '".(int)$idForwarderContact."'	
			";
			//echo "<br>".$query ;
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					return $row ;	
				}
				else
				{
					return false ;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function getForwarderContactFnLn($idForwarderContact)
	{
		if($idForwarderContact>0)
		{
			$query="
				SELECT
					id,
					szFirstName,
					szLastName,
					szEmail
				FROM
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."	
				WHERE
					id = '".(int)$idForwarderContact."'	
			";
			//echo "<br>".$query ;
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					return $row ;	
				}
				else
				{
					return false ;
				}
			}
			else
			{
                            $this->error = true;
                            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                            return false;
			}
		}
	}
	function getAllForwardersContact($idForwarder=false,$idRoleAry)
	{
            if($idForwarder>0)
            { 
                $query_and = " AND idForwarder = '".(int)$idForwarder."' ";
            }
            if(is_array($idRoleAry) && !empty($idRoleAry))
            {
                $idRoleStr = implode(',',$idRoleAry);
                $query_and .= "AND idForwarderContactRole IN (".mysql_escape_custom($idRoleStr).") ";
            }
            else if(!empty($idRoleAry))
            {
                $query_and .= "AND  idForwarderContactRole = '".mysql_escape_custom($idRoleAry)."' ";
            }
			
            $query="
                SELECT
                    id,
                    szFirstName,
                    szLastName,
                    idCountry,
                    szEmail,
                    idForwarder,
                    idForwarderContactRole,
                    dtLastLogin,
                    iConfirmed
                FROM
                    ".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
                WHERE 
                    iActive = '1'	
                $query_and	
                ORDER BY 
                    szFirstName ASC, szLastName ASC
            ";
            //echo $query ;
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $ret_ary = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {
                        $ret_ary[$ctr] = $row;
                        $ctr++ ;
                    }
                    return $ret_ary;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
	}
	
	function getAllForwarderContactsEmail($idForwarder,$idRoleAry=false)
	{
            if($idForwarder>0)
            {
                if(is_array($idRoleAry) && !empty($idRoleAry))
                {
                    $idRoleStr = implode(',',$idRoleAry);    
                    $query_and = "AND
                            idForwarderContactRole IN (".mysql_escape_custom($idRoleStr).") ";
                }
                else if(!empty($idRoleAry))
                {
                    $query_and = "AND
                            idForwarderContactRole = '".mysql_escape_custom($idRoleAry)."' ";
                }
                else 
                {
                    $query_and = '';
                }
                $query="
                    SELECT
                        id,
                        szEmail,
                        idForwarderContactRole
                    FROM
                        ".__DBC_SCHEMATA_FORWARDER_PREFERENCES__."
                    WHERE
                        idForwarder = '".(int)$idForwarder."'	
                    AND
                        iActive = '1'
                    $query_and	
                "; 	
               // echo $query ;
                //die;
                if($result=$this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        $ret_ary = array();
                        $ctr1 = 0;
                        $ctr2 = 0;
                        $ctr3 = 0;
                        $ctr4 = 0;
                        $ctr5 = 0;
                        $ctr6 = 0;
                        $ctr7 = 0;
                        while($row=$this->getAssoc($result))
                        {
                            if($row['idForwarderContactRole']==__BOOKING_PROFILE_ID__) //Sea Bookings
                            {
                                $ret_ary['szBookingEmail'][$ctr1] = $row;
                                //$ret_ary['szBookingEmail'][$ctr1] = $row['id'];
                                $ctr1++;
                            }	
                            else if($row['idForwarderContactRole']==__PAYMENT_AND_BILLING_PROFILE_ID__) //Payment Advice & Financial Statements E-mail
                            {
                                $ret_ary['szPaymentEmail'][$ctr2] = $row;
                                //$ret_ary['szPaymentEmail'][$ctr2] = $row['id'];
                                $ctr2++;
                            }	
                            else if($row['idForwarderContactRole']==__CUSTOMER_PROFILE_ID__) //Customer Service E-mail
                            {
                                $ret_ary['szCustomerServiceEmail'][$ctr3]= $row;
                                //$ret_ary['szCustomerServiceEmail'][$ctr3]['id'] = $row['id'];
                                $ctr3++;
                            } 
                            else if($row['idForwarderContactRole']==__AIR_FREIGHT_BOOKING_PROFILE_ID__) //Air Freight E-mail
                            {
                                $ret_ary['szAirBookingEmail'][$ctr4] = $row; 
                                $ctr4++;
                            }
                            else if($row['idForwarderContactRole']==__ROAD_FREIGHT_BOOKING_PROFILE_ID__) //Road Freight Service E-mail
                            {
                                $ret_ary['szRoadBookingEmail'][$ctr5]= $row; 
                                $ctr5++;
                            }
                            else if($row['idForwarderContactRole']==__COURIER_FREIGHT_BOOKING_PROFILE_ID__) //Courier Freight Service E-mail
                            {
                                $ret_ary['szCourierBookingEmail'][$ctr6]= $row;
                                //$ret_ary['szCustomerServiceEmail'][$ctr3]['id'] = $row['id'];
                                $ctr6++;
                            } 
                            else if($row['idForwarderContactRole']==__CATCH_UP_ALL_BOOKING_QUOTE_PROFILE_ID__) //Courier Freight Service E-mail
                            {
                                $ret_ary['szCatchUpBookingQuoteEmail'][$ctr7]= $row; 
                                $ctr7++;
                            }
                        } 
                        return $ret_ary;
                    }
                    else
                    {
                            return array();
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
        }
    }
	
	function deleteForwarderContact($idForwarderContact)
	{
		if($idForwarderContact>0)
		{	
			/*	
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
				WHERE
					id = '".(int)$idForwarderContact."'	
			";
			*/
			$query = "
				UPDATE
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
				SET
					iActive = '0'
				WHERE
					id = '".(int)$idForwarderContact."'
			";
			//echo "<br>".$query."<br>";
			if($result=$this->exeSQL($query))
			{
				return true ;
			}
			else 
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function resendActivationCode($idForwarderContact)
	{
		$query="
			SELECT
				id,
				szEmail,
				szFirstName
			FROM
			".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
			WHERE
				id='".(int)$idForwarderContact."'
			AND
				iConfirmed='0'
			";
		//echo $query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
        	
            if( $this->getRowCnt() > 0 )
            {	
	
			    $row=$this->getAssoc($result);        
            	$szEmail=$row['szEmail'];
            	$szFirstName=$row['szFirstName'];
            	$id=$row['id'];
            	$ConfirmKey=md5(date("Y-m-d h:i:s").random_number_ConfirmKey());
            	
            	
            	$query="
            		UPDATE
            			".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
            		SET
            			szActivationKey='".mysql_escape_custom($ConfirmKey)."',
            			dtActivationCodeSent = now()
            		WHERE
            			id='".(int)$id."'
            	";
            	//echo $query;
            	$result = $this->exeSQL( $query );
            	
            		$replace_ary['szFirstName']=$this->szFirstName;
					$replace_ary['szEmail']=$this->szEmail;
					$confirmationLink=__FORWARDER_HOME_PAGE_URL__."/confirmation.php?confirmationKey=".$ConfirmKey;
					$confirmationLinkHttps=__FORWARDER_HOME_PAGE_URL_SECURE__."/confirmation.php?confirmationKey=".$ConfirmKey;
					$replace_ary['szLink']="<a href='".$confirmationLink."'>CLICK TO VERIFY E-MAIL ADDRESS</a>";
					$replace_ary['szHttpsLink']=$confirmationLinkHttps;
                    $replace_ary['supportEmail'] = __STORE_SUPPORT_EMAIL__;
                    createEmail(__CREATE_FORWARDER_ACCOUNT__, $replace_ary,$this->szEmail, __CREATE_USER_ACCOUNT_SUBJECT__, __STORE_SUPPORT_EMAIL__,$this->id, __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__);
                    
                    return true;
                      
            }
            else
            {
            	return false;
            }
		}
		else
		{
			return false;
		}
	}
	
	function checkConfirmationKey($szConfirmationKey)
	{
		$query="
			SELECT
				id,
				szFirstName,
				szLastName
			FROM
				".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
			WHERE
				szActivationKey  = '".mysql_escape_custom($szConfirmationKey)."'
		";
		//echo $query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
        	// Check if the user exists
            if( $this->getRowCnt() > 0 )
            {
            	$row=$this->getAssoc($result);
            	
            	$_SESSION['idForwarder']=$row['id'];
            	if(($_SESSION['idForwarder']==$_SESSION['forwarder_user_id']) || (int)$_SESSION['forwarder_user_id']==0)
            	{
	            	$query="
						UPDATE
							".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
						SET
							iConfirmed='1',
							szActivationKey='',
							dtActivationCodeSent = ''
						WHERE
							szActivationKey='".mysql_escape_custom($szConfirmationKey)."'
					";
	            	//echo $query;
	            	
					if($result = $this->exeSQL( $query ))
					{	
//						$this->set_iConfirmed(trim(sanitize_all_html_input(1)));
						$this->iConfirmed = 1;
	                 return true;
            	
					}
            	}	
            	else
            	{
            		 return false;
            	}
            }
            else
            {
                    return false;
            }
		}
	}
	
	function getUserDetails($idUser,$adminFlag=false)
	{
            if((int)$idUser>0)
            {
                if((isset($_SESSION['forwarder_admin_id']) && (int)$_SESSION['forwarder_admin_id']>0) || ($adminFlag))
                {
                    $query="
                        SELECT 
                            f.id,
                            f.szFirstName,
                            f.szLastName,
                            f.szEmail
                        FROM 
                            ".__DBC_SCHEMATA_MANAGEMENT__." as f 
                        WHERE 
                            f.id='". (int)$idUser."'
                        ";
                        //echo "<br>".$query."<br>";	
                        if($result=$this->exeSQL($query))
                        { 
                            if($this->getRowCnt()>0)
                            {
                                $row=$this->getAssoc($result);
                                $this->id=$row['id'];
                                $this->szFirstName=$row['szFirstName'];
                                $this->szLastName=$row['szLastName'];						
                                $this->szEmail=$row['szEmail'];
                                $this->idForwarder = $_SESSION['forwarder_id'] ;
                                $this->idForwarderContactRole= __ADMINISTRATOR_PROFILE_ID__; 
                                return true;
                            }
                            else
                            {
                                return array();
                            }
                        }
                        else 
                        {
                            return array();
                        }
                    }
                    else
                    {
                        $query="
                            SELECT 
                                f.id,
                                f.idForwarder,
                                f.szFirstName,
                                f.szLastName,
                                f.idCountry,
                                f.szEmail,
                                f.szPhone,
                                f.szMobile,
                                f.szPassword,
                                f.iActive,
                                f.iConfirmed,
                                f.idForwarderContactRole,
                                f.szActivationKey,
                                f.dtActivationCodeSent,
                                f.szBookingFrequency,
                                f.iPasswordUpdated,
                                (SELECT c.szCountryName FROM ".__DBC_SCHEMATA_COUNTRY__." AS c WHERE c.id = f.idCountry ) szCountryName
                            FROM 
                                    ".__DBC_SCHEMATA_FORWARDERS_CONTACT__." as f 
                            WHERE 
                                    f.id='". (int)$idUser."'
                        ";
                        //echo "<br>".$query."<br>";	
                        if($result=$this->exeSQL($query))
                        { 
                            if($this->getRowCnt()>0)
                            {
                                $row=$this->getAssoc($result);
                                $this->id=$row['id'];
                                $this->szFirstName=$row['szFirstName'];
                                $this->szLastName=$row['szLastName'];
                                $this->szCountryName=$row['szCountryName'];
                                $this->szCountryId=$row['idCountry'];
                                $this->szEmail=$row['szEmail'];
                                $this->szPhone=$row['szPhone'];
                                $this->szMobile=$row['szMobile'];
                                $this->szPassword=$row['szPassword'];
                                $this->iConfirmed=$row['iConfirmed']; 
                                $this->idForwarder=$row['idForwarder']; 
                                $this->idForwarderContactRole=$row['idForwarderContactRole']; 
                                $this->szActivationKey=$row['szActivationKey']; 
                                $this->dtActivationCodeSent=$row['dtActivationCodeSent']; 
                                $this->szBookingFrequency=$row['szBookingFrequency']; 
                                $this->iPasswordUpdated = $row['iPasswordUpdated'];
                                return true;
                            }
                            else
                            {
                                return array();
                            }
                        }
                        else 
                        {
                                return array();
                        }
                    }
		}
		else 
		{
			return array();
		}
	}
	
	function checkdeleteForwarderAccount($data)
	{
		//print_r($data);
		if(is_array($data))
		{
			$this->set_szEmail(trim(sanitize_all_html_input($data['szEmail'])));
			$this->set_szNonFrocePassword(trim(sanitize_all_html_input($data['szPassword'])));
			
			if(!empty($this->szEmail) && ($this->szEmail!=$data['szLoginEmail']))
			{
				$this->addError( "szEmail" , t($this->t_base.'messages/email_does_not_match') );
			}			
			//exit if error exist.
			if ($this->error === true)
			{
				return false;
			}
			
				$query="
					SELECT
						szPassword
					FROM
						".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
					WHERE
						szEmail='".mysql_escape_custom($this->szEmail)."'
				";
				//echo $query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{
	            if( $this->getRowCnt() > 0 )
	            {
	            	$row=$this->getAssoc($result);
	            	$password=$row['szPassword'];
	            	//$idUser=$row['id'];
	            	//$iConfirmed=$row['iConfirmed'];
	            }
			}				
			if($password!=md5($data['szPassword']))
			{	
				$this->addError( "szOldPassword" , t($this->t_base.'messages/wrong_username_password') );
				return false;
			}
				else
				{
					return true;
				}
		}
			else
			{
				return false;
			}
	}
	
		function deactivateForwarderAccount($idForwarder)
			{
				if((int)$idForwarder>0)
				{
					$query="
						UPDATE
							".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
						SET
							iActive='0'
						WHERE
							id='".(int)$idForwarder."'
					";
					$result = $this->exeSQL( $query );
			
					return true;
				}
				else
					{
						return false;
				}
		}
	
		function updateForwarderinfo($data,$forwarderId)
		{
				//print_r($data);
				if(is_array($data) && (int)$forwarderId>0)
				{
					$this->set_szFirstName(trim(sanitize_all_html_input($data['szFirstName'])));
					$this->set_szLastName(trim(sanitize_all_html_input($data['szLastName'])));
					$this->set_szCountry(trim(sanitize_all_html_input($data['szCountry'])));
					$this->set_id($forwarderId);
					if($this->error==true)
					{
						return false;
					}
				  	$query="UPDATE 
				  				".__DBC_SCHEMATA_FORWARDERS_CONTACT__." 
				  			SET 
								`szFirstName`='".mysql_escape_custom(ucwords(strtolower($this->szFirstName)))."',
								`szLastName`='".mysql_escape_custom(ucwords(strtolower($this->szLastName)))."',
								`idCountry`='".mysql_escape_custom($this->szCountry)."'
							WHERE 
								`id`='".(int)$this->id."'";		
				  	 
				  	if($result=$this->exeSQL($query))
				  	{
				  		return true;  	 									
				  	}
				  		else{
				  			return false;
				  		}		
				}
					else
					{
					return false;
					}	
		}
		
	function updateForwarderContact_info($data,$forwarderId)
	{
		
		if(is_array($data) && (int)$forwarderId>0)
		{	$this->set_szOldEmail(trim(sanitize_all_html_input(strtolower($data['szOldEmail']))));
			$this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szEmail']))));
			$this->set_szMobileNumber(trim(sanitize_all_html_input(urldecode(base64_decode($data['szMobileUpdate'])))));
			$this->set_szPhoneNumber(trim(sanitize_all_html_input(urldecode(base64_decode($data['szPhoneUpdate'])))));
			$this->set_controlBooking(trim(sanitize_all_html_input($data['szBooking'])));
			$this->set_id($forwarderId);
			
			if(!empty($this->szEmail) && $this->isForwarderEmailExist($this->szEmail,$this->id) )
			{
				$this->addError( "szEmail" , t($this->t_base.'messages/already_exists') );
			}
			if($this->error==true)
			{
				return false;
			}
			
			if($this->szEmail != $this->szOldEmail)
			{   
			
				$query="
            		UPDATE
            			".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
            		SET
            			iConfirmed='0'
            		WHERE
            			id='".(int)$this->id."'
            	";
				//echo $query;
            	if($result = $this->exeSQL( $query ))
            	{
					$this->resendActivationCode($this->id);
            	}
            	else
            	{
		            $this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
            	}
			}					
			if(!empty($this->szPhoneNumber))
			{
				$this->szPhoneNumber=$this->szPhoneNumber;
			}					
		  	if(!empty($this->szMobileNumber))
		  	{
		  		$this->szMobileNumber=$this->szMobileNumber;
		  	}					
		  	
			$query="
				UPDATE 
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__." 
				SET 
					`szEmail`='".mysql_escape_custom($this->szEmail)."',
					`szPhone`='".mysql_escape_custom($this->szPhoneNumber)."',
					`szMobile`='".mysql_escape_custom($this->szMobileNumber)."',
					szBookingFrequency='".(int)$this->controlBooking."' 
				WHERE 
					`id`='".(int)$this->id."'";		
		  	// echo $query;
			if($result=$this->exeSQL($query))
			{
				return true;  	 									
	  		}
  			else
  			{
  				return false;
  			}		
		}
		else
		{
			return false;
		}	
	}
	
	function contactNumber($contact)
	{
		$str = substr($this->$contact, 0, 1); 
		//echo $str;
		$substr=substr($this->$contact,1);
		$phone=str_replace("+"," ",$substr);
		//echo $phone;
		if(!empty($phone))
		{
			$phoneNumber=$str."".urldecode($phone);
		}
		else
		{
			$phoneNumber = urldecode($this->$contact) ;
		}
		$this->$contact = $phoneNumber ;
	}													
													
	function isForwarderEmailExist( $email,$id=false)
	{
		if((int)$id>0)
		{
			$query_and = " AND id!='".(int)$id."' ";
		}
		
		$query="
			SELECT
                            id
			FROM
                            ".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
			WHERE
                            szEmail = '".mysql_escape_custom($this->szEmail)."'	
			AND
                            iActive = '1'
                        AND
                            idForwarderContactRole!='7'
		";
		//AND iActive ='1'
		if($id>0)
		{
			$query .="
				AND
				    id<>'".(int)$id."'		
			";
		}
		//echo "<br><br>".$query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
        	// Check if the user exists
            if( $this->getRowCnt() > 0 )
            {
                while($row=$this->getAssoc($result))
                { 
                    $this->idForwarderContact = $row['id']; 
                }
                return true;
            }
            else
            {
                    return false;
            }
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	function updateForwarderPassword_info($data,$forwarderId)
	{
		$this->set_szOldPassword(trim(sanitize_all_html_input($data['szOldPassword'])));
		$this->set_szPassword(trim(sanitize_all_html_input($data['szNewPassword'])));
		$this->set_szReTypePassword(trim(sanitize_all_html_input($data['szConPassword'])));
		$this->set_id(trim(sanitize_all_html_input($forwarderId)));
			
		if ($this->error == true)
		{
			return false;
		}
		$query="
			SELECT
				szPassword
			FROM
				".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
			WHERE
				id='".(int)$this->id."'
			";
			//echo $query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
        	// Check if the user exists
            if( $this->getRowCnt() > 0 )
            {
            	$row=$this->getAssoc($result);
            	
            	$password=$row['szPassword'];
            	
            }
		}
	if($password == md5($this->szOldPassword))
		{
		$query="
            UPDATE
            	".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
            SET
            	szPassword='".mysql_escape_custom(md5($this->szPassword))."'
			WHERE
				id='".(int)$this->id."'
			";
			//echo $query;
		}
		else if($password != md5($this->szOldPassword))
		{	
			$this->addError( "szOldPassword" , t($this->t_base.'messages/wrong_old_password') );
			return false;
		}
		if(!empty($this->szPassword) && $this->szPassword!=$this->szRetypePassword)
		{
			$this->addError( "szRetypePassword" , t($this->t_base.'messages/match_password') );
			return false;
		}
		if($password == md5($this->szOldPassword))
		{
		$query="
            UPDATE
            	".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
            SET
            	szPassword='".mysql_escape_custom(md5($this->szPassword))."'
			WHERE
				id='".(int)$this->id."'
			";
			//echo $query;
		}
		if($result = $this->exeSQL( $query ))
		{
			return true;
		}	
			else
			{
				return false;
			}
		}
                
	function isContactEmailExist( $email,$id=0,$idForwarder=0,$role )
	{
            $query="
                SELECT
                    id
                FROM
                    ".__DBC_SCHEMATA_FORWARDER_PREFERENCES__."
                WHERE
                    szEmail='".mysql_escape_custom($email)."'
                AND 
                    idForwarderContactRole='".(int)$role."'
                AND 
                    idForwarder = '".mysql_escape_custom($idForwarder)."'
                AND 
                    iActive ='1'	
            ";
            if($id>0)
            {
                $query .=" AND id<> '".(int)$id."' ";
            }
            //echo $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            { 
        	// Check if the user exists
                if( $this->getRowCnt() > 0 )
                {	$this->addError( $email , t($this->t_base.'messages/already_exists') );
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
	
	function validateUpdateMailingAddress($contact,$contactId=0,$contactRole,$idForwarder,$value=0,$key=0)
	{	
            if(isset($contactId[$key]))
            {
                $id=$contactId[$key];
            }
            else
            {
                $id=0;
            }
            
            //$existsMail=$this->isContactEmailExist($value,$id,$idForwarder,$contactRole);
            if( (int)$id!=0)
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_FORWARDER_PREFERENCES__."
                    SET 
                        szEmail='".mysql_escape_custom($value)."',
                        dtModified= NOW()		
                    WHERE
                        idForwarder = '".mysql_escape_custom($idForwarder)."'
                    and 
                        id='".(int)$id."' 
                "; 
                if($this->exeSQL($query))
                { 
                    return true;
                }
            }
            else if((int)$id==0)
            {
                $query="
                   INSERT INTO
                       ".__DBC_SCHEMATA_FORWARDER_PREFERENCES__." 
                   (
                       szEmail,
                       idForwarder,
                       idForwarderContactRole,
                       dtCreated,
                       dtModified,
                       iActive
                   ) 
                   VALUES
                   (
                       '".$value."', '".mysql_escape_custom($idForwarder)."','".$contactRole."',NOW(),NOW(),'1'
                   )	
               "; 
               if($this->exeSQL($query))
               {
                   return true;
               }
            }
            else if($existsMail)
            { 
                //we are having one condition if the email id already exists so to remove the predefined error we use $existsMail in this else statement
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;	
            }
	}

	function updateMailingAddress($data,$idForwarder)
	{	 
//            if(!empty($data['szEmailCatchUpAll']))
//            {	 
//                if(count($data['szEmailCatchUpAll'])>1)
//                {	
//                    $count=1;
//                    foreach($data['szEmailCatchUpAll'] as $key=>$value)
//                    {	
//                        if(!empty($data['catchAllEmailId'][$count]) && empty($value))
//                        {	
//                            $removeArr[]=$data['catchAllEmailId'][$count];
//                            unset($data['catchAllEmailId'][$count],$data['szEmailCatchUpAll'][$count]);	
//                        }
//                        if(empty($data['catchAllEmailId'][$count]) && empty($value) )
//                        {	 
//                            unset($data['catchAllEmailId'][$count],$data['szEmailCatchUpAll'][$count]);	
//                        }
//                        else
//                        {
//                            $this->set_szMultipleEmail($value,'szEmailCatchUpAll');
//                        }
//                        $count++;
//                    }
//                    if(empty($data['szEmailCatchUpAll']))
//                    {
//                        $this->addError( ' ',t($this->t_base.'messages/catch_up_all_mail_id_empty') );
//                        return false;
//                    }
//                    unset($count);
//                } 
//                else
//                {
//                    $szQuoteCatchUpEmail = trim(sanitize_all_html_input($data['szEmailCatchUpAll'][1]));
//                    $this->set_szMultipleEmail($szQuoteCatchUpEmail,'szEmailCatchUpAll'); 
//                    if($this->error === true)
//                    {
//                        return false;
//                    } 
//                    $quoteEmailIdAry = $data['quoteEmailId'];
//                    $existsMail=$this->isContactEmailExist($contactB,$quoteEmailIdAry[1],$idForwarder,__CATCH_UP_ALL_BOOKING_QUOTE_PROFILE_ID__);
//                    if ($this->error === true)
//                    {
//                        return false;
//                    } 
//                } 
//            }
//            else
//            {
//                $this->addError('Catch', "-all for quotes and bookings email is required.");
//            }
            if((count($data['szEmailPayment'])==1) && (count($data['szEmailCustomer'])==1) && (count($data['szEmailCatchUpAll'])==1))
            {
                if(empty($data['szEmailPayment'][1]) && empty($data['szEmailCustomer'][1]) && empty($data['szEmailCatchUpAll'][1]))
                {
                    $this->addError( ' ',t($this->t_base.'messages/mail_id_empty') );
                    return false;
                }    
                if(!empty($data['szEmailCatchUpAll'][1]))
                {	
                    $contactA=trim(sanitize_all_html_input($data['szEmailCatchUpAll'][1]));
                    $this->set_szMultipleEmail($contactA,'szEmailCatchUpAll');
                    if ($this->error === true)
                    {
                            return false;
                    }

                    $quoteEmailIdAry = $data['quoteEmailId'];
                    $existsMail=$this->isContactEmailExist($contactA,$quoteEmailIdAry[1],$idForwarder,__CATCH_UP_ALL_BOOKING_QUOTE_PROFILE_ID__);
                    if ($this->error === true)
                    {
                            return false;
                    } 
                }
                elseif(empty($data['szEmailCatchUpAll'][1]))
                {
                    $this->addError('Catch', "-all for bookings email is required.");
                    return false;
                }
                
                if(!empty($data['szEmailPayment'][1]))
                {	
                    $contactB=trim(sanitize_all_html_input($data['szEmailPayment'][1]));
                    $this->set_szMultipleEmail($contactB,'szPaymentEmail');
                    if ($this->error === true)
                    {
                            return false;
                    }

                    $contactIdB=$data['paymentId'];
                    $existsMail=$this->isContactEmailExist($contactB,$contactIdB[1],$idForwarder,__PAYMENT_AND_BILLING_PROFILE_ID__);
                    if ($this->error === true)
                    {
                            return false;
                    } 
                }
                elseif(empty($data['szEmailPayment'][1]))
                {
                    $this->addError( ' ',t($this->t_base.'messages/payment_mail_id_empty') );
                    return false;
                }

                if(!empty($data['szEmailCustomer'][1]))
                {	
                    $contactC=trim(sanitize_all_html_input($data['szEmailCustomer'][1]));
                    $this->set_szMultipleEmail($contactC,'szEmailCustomer');
                    if ($this->error === true)
                    {
                            return false;
                    }

                    $contactIdC=$data['customerId'];
                    $existsMail=$this->isContactEmailExist($contactC,$contactIdC[1],$idForwarder,__CUSTOMER_PROFILE_ID__);
                    if ($this->error === true)
                    {
                            return false;
                    }
                }
                if(empty($data['szEmailCustomer'][1]))
                {
                    $this->addError( ' ',t($this->t_base.'messages/customer_mail_id_empty') );
                    return false;
                }

                if ($this->error === true)
                {
                    return false;
                }
                else
                { 
                    if(!empty($contactA))
                    {
                        $data1=$this->validateUpdateMailingAddress($contactA,$quoteEmailIdAry,__CATCH_UP_ALL_BOOKING_QUOTE_PROFILE_ID__,$idForwarder,$contactA,1);
                    }
                    else
                    {
                        $data1=1;
                    }
                    
                    if(!empty($contactB))
                    {
                        $data2=$this->validateUpdateMailingAddress($contactB,$contactIdB,__PAYMENT_AND_BILLING_PROFILE_ID__,$idForwarder,$contactB,1);
                    }
                    else
                    {
                        $data2=1;
                    } 
                    
                    if(!empty($contactC))
                    {
                        $data3=$this->validateUpdateMailingAddress($contactC,$contactIdC,__CUSTOMER_PROFILE_ID__,$idForwarder,$contactC,1);
                    }
                    else
                    {
                        $data3=1;
                    } 
                }
            }
            else
            {	
                $removeArr=array(); 
                if($data['szEmailCatchUpAll'])
                {	
                    $count=1;
                    foreach($data['szEmailCatchUpAll'] as $key=>$value)
                    {	
                        if(!empty($data['quoteEmailId'][$count]) && empty($value) )
                        {	
                            $removeArr[]=$data['quoteEmailId'][$count];
                            unset($data['quoteEmailId'][$count],$data['szEmailCatchUpAll'][$count]);	
                        }
                        if(empty($data['quoteEmailId'][$count]) && empty($value) )
                        {	
                            //$removeArr[]=$data['paymentId'][$count];
                            unset($data['quoteEmailId'][$count],$data['szEmailCatchUpAll'][$count]);	
                        }
                        else
                        {
                            $this->set_szMultipleEmail($value,'szEmailCatchUpAll');
                        }
                        $count++;
                    }
                    if(empty($data['szEmailCatchUpAll']))
                    {
                        $this->addError('Catch', "-all for bookings email is required.");
                        return false;
                    }
                    unset($count);
                }
                
                if($data['szEmailPayment'])
                {	
                    $count=1;
                    foreach($data['szEmailPayment'] as $key=>$value)
                    {	
                        if(!empty($data['paymentId'][$count]) && empty($value) )
                        {	
                            $removeArr[]=$data['paymentId'][$count];
                            unset($data['paymentId'][$count],$data['szEmailPayment'][$count]);	
                        }
                        if(empty($data['paymentId'][$count]) && empty($value) )
                        {	
                            //$removeArr[]=$data['paymentId'][$count];
                            unset($data['paymentId'][$count],$data['szEmailPayment'][$count]);	
                        }
                        else
                        {
                            $this->set_szMultipleEmail($value,'szPaymentEmail');
                        }
                        $count++;
                    }
                    if(empty($data['szEmailPayment']))
                    {
                        $this->addError( ' ',t($this->t_base.'messages/payment_mail_id_empty') );
                        return false;
                    }
                    unset($count);
                }

                if($data['szEmailCustomer'])
                {	
                    $count=1;
                    foreach($data['szEmailCustomer'] as $key=>$value)
                    {	
                        if(!empty($data['customerId'][$count]) && empty($value) )
                        {
                                $removeArr[]=$data['customerId'][$count];
                                unset($data['customerId'][$count],$data['szEmailCustomer'][$count]);	
                        }
                        if(empty($data['customerId'][$count]) && empty($value) )
                        {
                                $removeArr[]=$data['customerId'][$count];
                                unset($data['customerId'][$count],$data['szEmailCustomer'][$count]);	
                        }
                        else
                        {
                                $this->set_szMultipleEmail($value,'szCustomerEmail');
                        }
                        $count++;
                    }
                    if(empty($data['szEmailCustomer']))
                    {
                        $this->addError( ' ',t($this->t_base.'messages/customer_mail_id_empty') );
                        return false;
                    }
                    unset($count);
                }

                if ($this->error === true)
                {
                    return false;
                }

                //FOR CHECKING EMAILS EXISTS OR NOT   
                if( $data['szEmailCatchUpAll'] )
                {
                    $i=0;
                    foreach($data['szEmailCatchUpAll'] as $key=>$value)
                    {	
                        $contact=trim(sanitize_all_html_input($value));
                        $count=array_keys($data['szEmailCatchUpAll'],$contact);
                        if(count($count)>1)
                        {
                            $this->addError( ' ',t($this->t_base.'messages/insert_same_email_id') );
                            return false;
                        }
                        $contactId=$data['quoteEmailId'];
                        $contactRole=__CATCH_UP_ALL_BOOKING_QUOTE_PROFILE_ID__;
                        $existsMail=$this->isContactEmailExist($contact,$contactId[$key],$idForwarder,$contactRole);
                        $i++;
                    }
                    unset($count,$i);	
                }
                
                if( $data['szEmailPayment'] )
                {
                    $i=0;
                    foreach($data['szEmailPayment'] as $key=>$value)
                    {	
                        $contact=trim(sanitize_all_html_input($value));
                        $count=array_keys($data['szEmailPayment'],$contact);
                        if(count($count)>1)
                        {
                            $this->addError( ' ',t($this->t_base.'messages/insert_same_email_id') );
                            return false;
                        }
                        $contactId=$data['paymentId'];
                        $contactRole=__PAYMENT_AND_BILLING_PROFILE_ID__;
                        $existsMail=$this->isContactEmailExist($contact,$contactId[$key],$idForwarder,$contactRole);
                        $i++;
                    }
                    unset($count,$i);	
                } 
                if( $data['szEmailCustomer'] )
                {
                    $i=0;
                    foreach($data['szEmailCustomer'] as $key=>$value)
                    {	
                        $contact=trim(sanitize_all_html_input($value));
                        $count=array_keys($data['szEmailCustomer'],$contact);
                        if(count($count)>1)
                        {
                            $this->addError( ' ',t($this->t_base.'messages/insert_same_email_id') );
                            return false;
                        }
                        $contactId=$data['customerId'];
                        $contactRole=__CUSTOMER_PROFILE_ID__;
                        $existsMail=$this->isContactEmailExist($contact,$contactId[$key],$idForwarder,$contactRole);
                    }
                    unset($count,$i);		
                } 
                
                if($data['szEmailCatchUpAll'])
                {
                    foreach($data['szEmailCatchUpAll'] as $key=>$value)
                    {	
                        $contact=trim(sanitize_all_html_input($value));
                        $contactId=$data['quoteEmailId'];
                        $contactRole=__CATCH_UP_ALL_BOOKING_QUOTE_PROFILE_ID__;
                        $data1=$this->validateUpdateMailingAddress($contact,$contactId,$contactRole,$idForwarder,$value,$key);
                    }
                }
                if($data['szEmailPayment'])
                {
                    foreach($data['szEmailPayment'] as $key=>$value)
                    {	
                        $contact=trim(sanitize_all_html_input($value));
                        $contactId=$data['paymentId'];
                        $contactRole=__PAYMENT_AND_BILLING_PROFILE_ID__;
                        $data2=$this->validateUpdateMailingAddress($contact,$contactId,$contactRole,$idForwarder,$value,$key);
                    }
                }	
                if($data['szEmailCustomer'])
                {
                    foreach($data['szEmailCustomer'] as $key=>$value)
                    {	
                        $contact=trim(sanitize_all_html_input($value));
                        $contactId=$data['customerId'];
                        $contactRole=__CUSTOMER_PROFILE_ID__;
                        $data3=$this->validateUpdateMailingAddress($contact,$contactId,$contactRole,$idForwarder,$value,$key);
                    }
                }
            } 
            
            if($this->error === true)
            {
                return false;
            } 
            
            if($data1 || $data2 || $data3)
            {	
                if(isset($removeArr))
                {
                    foreach($removeArr as $key=>$value)
                    {	
                        if(!empty($value))
                        {
                            $checkRemove=$this->removeMailingAddresses($value);
                        }
                    }
                    if($checkRemove)
                    {
                        return true;
                    }
                }
                $kForwarder = new cForwarder();
                $kForwarder->isForwarderOnline($idForwarder,'UPDATE',false);	
                return true;
            }
	}
		
	function removeMailingAddresses($id)
	{	
		$idContact = (int)$id;
		if($idContact>0)
		{
			$query="
				UPDATE 
						".__DBC_SCHEMATA_FORWARDER_PREFERENCES__." 
				SET 
						iActive='0'		
				WHERE
						id='".(int)$idContact."'
			";
			//echo $query;
			if( $this->exeSQL($query) )
			{
				return true;
			}	
				else{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
				}
		}
	}
	function getSystemPanelDetails($idForwarder)
	{	$id=(int)$idForwarder;
		if($id>0)
		{
			$query=
				"SELECT  
					fwd.szCurrency as idCurrency,
					cur.szCurrency
				FROM
					".__DBC_SCHEMATA_FROWARDER__." as fwd
				LEFT JOIN
					".__DBC_SCHEMATA_CURRENCY__." cur
				ON
					cur.id = fwd.szCurrency	
				
				WHERE
					fwd.id='".(int)$id."'	
					";
				
				if( $this->exeSQL($query) )
				{
					if($result=$this->exeSQL($query))
					{
						if($this->iNumRows>0)
						{
							$ret_ary = array();
							
							while($row=$this->getAssoc($result))
							{
								$ret_ary = $row;
							}
							return $ret_ary;
						}
						else
						{
							return array();
						}
					}
					else
					{
						return array();
					}
				}	
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
		}
				
	}
	function updatePreferencesCotrol($data,$idForwarder)
	{	
		$id=(int)$idForwarder;
		if($id>0)
		{	
			$this->findForwarderCurrency((int)$id);
			//$this->controlBooking=trim(sanitize_all_html_input($data['szBooking'])); 
			$this->set_szCurrency(trim(sanitize_all_html_input($data['szCurrency'])));
			if($this->error==true)
			{
                            return false;
			}
                        
			$query= "
                            UPDATE
                                ".__DBC_SCHEMATA_FORWARDERS__."
                            SET 
                                szCurrency='".mysql_escape_custom($this->szCurrency)."'
                            WHERE
                                id='".(int)$id."'
                        ";
			//echo $query;die;	
			if( $this->exeSQL($query) )
			{ 
                            $this->updatePrivateCustomerFeePrices($idForwarder);
                            
                            if( ($this->iOldCurrency == NULL || $this->iOldCurrency =='' || $this->iOldCurrency ==0) && $this->iCurrencyFlag==0 )
                            {
                                $query= "
                                    UPDATE
                                        ".__DBC_SCHEMATA_FORWARDERS__."
                                    SET
                                        iCurrencyFlag='1'
                                    WHERE
                                        id='".(int)$id."'
                                ";		
                                $result = $this->exeSQL( $query ) ;	
                                $kForwarder = new cForwarder();
                                $kForwarder->isForwarderOnline($id,'UPDATE',true);			
                                $this->setForwarderTransactionBaseline($idForwarder);
                            }
                            return true;
			}	
				else{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
				}
		}
		
	}
        function updatePrivateCustomerFeePrices($idForwarder)
        {
            if($idForwarder>0)
            {
                $kForwarder = new cForwarder();
                $privateCustomerFeeAry = array();
                $privateCustomerFeeAry = $kForwarder->getAllPrivateCustomerPricing($idForwarder);
                
                $kWHSSearch = new cWHSSearch();
                
                if(!empty($privateCustomerFeeAry))
                {
                    $kForwarder->load($idForwarder);
                    $idForwarderAccountCurrency = $kForwarder->szCurrency;
                    
                    if($idForwarderAccountCurrency==1) //USD
                    {
                        $fForwarderAccountCurrencyExchangeRate = 1;
                    }
                    else
                    { 
                        $forwarderNewCurrencyAry = $kWHSSearch->getCurrencyDetails($idForwarderAccountCurrency); 
                        $fForwarderAccountCurrencyExchangeRate = $forwarderNewCurrencyAry['fUsdValue'];  
                    }
                    
                    foreach($privateCustomerFeeAry as $szProduct=>$privateCustomerFeeArys)
                    {
                        if($idForwarderAccountCurrency!=$privateCustomerFeeArys['idCurrency'])
                        {
                            $fOldCustomerFee = $privateCustomerFeeArys['fCustomerFee'];
                            $idOldCurrency = $privateCustomerFeeArys['idCurrency'];
                            
                            if($idOldCurrency==1) //USD
                            {
                                $fCustomerFeeUSD = $fOldCustomerFee;
                            }
                            else
                            { 
                                $forwarderNewCurrencyAry = $kWHSSearch->getCurrencyDetails($idOldCurrency);

                                $szCurrency = $forwarderNewCurrencyAry['szCurrency'];
                                $fExchangeRate = $forwarderNewCurrencyAry['fUsdValue']; 
                                $fCustomerFeeUSD = $fOldCustomerFee * $fExchangeRate;
                            }
                            
                            $fCustomerFee = 0;
                            if($fForwarderAccountCurrencyExchangeRate>0)
                            {
                                $fCustomerFee = round((float)($fCustomerFeeUSD/$fForwarderAccountCurrencyExchangeRate),2);
                            }
                            
                            $addPrivateFeeAry = array();
                            $addPrivateFeeAry['iPrivateCustomerAvailable'] = $privateCustomerFeeArys['iPrivateCustomerAvailable'];
                            $addPrivateFeeAry['idCurrency'] = $idForwarderAccountCurrency;
                            $addPrivateFeeAry['fCustomerFee'] = $fCustomerFee;
                            $addPrivateFeeAry['szProduct'] = $szProduct;
                            $addPrivateFeeAry['idForwarder'] = $idForwarder;
                             
                            $kForwarder->addPrivateCustomerPricing($addPrivateFeeAry);
                        }
                    }
                } 
            } 
        }
	function setForwarderTransactionBaseline($idForwarder)
	{
		$this->findCurrencyName();
		$query=
				"INSERT INTO 
					".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." 
					(
						`id`, 
						`idBooking`, 
						`idForwarder`, 
						`fTotalAmount`, 
						`fTotalPriceForwarderCurrency`, 
						`idCurrency`, 
						`szCurrency`, 
						`fExchangeRate`, 
						`idForwarderCurrency`, 
						`szForwarderCurrency`, 
						`fForwarderExchangeRate`, 
						`iDebitCredit`, 
						`szBookingNotes`, 
						`szInvoice`, 
						`szBooking`, 
						`iStatus`, 
						`dtInvoiceOn`, 
						`dtCreatedOn`, 
						`dtUpdatedOn`, 
						`dtCreditOn`, 
						`dtDebitOn`, 
						`iBatchNo`, 
						`fReferalPercentage`, 
						`fReferalAmount`, 
						`szComment`, 
						`dtPaymentConfirmed`
						) 
						VALUES 
						(
						NULL, 
						'', 
						'".(int)$idForwarder."', 
						'0', 
						'0.00', 
						'0', 
						'', 
						'0', 
						'".(int)$this->szCurrency."', 
						'".$this->szCurrencyName."', 
						'', 
						'4', 
						'', 
						'', 
						'Starting Balance', 
						'2', 
						'', 
						NOW(), 
						NOW(), 
						NOW(), 
						NOW(), 
						'', 
						'', 
						'', 
						'', 
						NOW()
						)
				";
				//echo $query;
			if( $result = $this->exeSQL( $query ) )
			{
				return true;
			}	
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function findForwarderCurrency($idForwarder)
	{
		$query=
				"SELECT
					szCurrency,
					iCurrencyFlag
				FROM	
					".__DBC_SCHEMATA_FROWARDER__."
				WHERE
					id='".(int)$idForwarder."'
				";
			if( $result = $this->exeSQL( $query ) )
			{
				$row=$this->getAssoc($result);
				$this->iOldCurrency  = $row['szCurrency'];
				$this->iCurrencyFlag = $row['iCurrencyFlag'];
				return true;
			}	
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function findCurrencyName()
	{
		$query=
				"SELECT
					szCurrency as iCurrency
				FROM	
					".__DBC_SCHEMATA_CURRENCY__."
				WHERE
					id='".(int)$this->szCurrency."'
				";
			if( $result = $this->exeSQL( $query ) )
			{
				$row=$this->getAssoc($result);
				$this->szCurrencyName  = $row['iCurrency'];
				return true;
			}	
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	
	function forwarderLogin($data)
	{
            if(is_array($data))
            {
                $this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szEmail']))));
                $this->set_szNonFrocePassword(trim(sanitize_all_html_input($data['szPassword'])));
                $this->set_szControlPanelUrl(trim(sanitize_all_html_input($data['szControlPanelUrl'])));

                //exit if error exist.
                if($this->error === true)
                {
                        return false;
                }

                $query="
                    SELECT
                        fc.id,
                        fc.iConfirmed,
                        fc.idForwarder
                    FROM
                        ".__DBC_SCHEMATA_FORWARDERS_CONTACT__." fc
                    INNER JOIN
                        ".__DBC_SCHEMATA_FROWARDER__." f
                    ON
                        f.id=fc.idForwarder		
                    WHERE
                        fc.szEmail='".mysql_escape_custom($this->szEmail)."'
                    AND
                        fc.szPassword = '".mysql_escape_custom(trim(md5($this->szNonFrocePassword)))."'	
                    AND
                        f.szControlPanelUrl = '".mysql_escape_custom(trim($this->szControlPanelUrl))."'	
                    AND
                        fc.iActive='1'
                ";
                //echo $query;
                if( ( $result = $this->exeSQL( $query ) ) )
                {
	            if( $this->getRowCnt() > 0 )
	            {
	            	$row=$this->getAssoc($result);
	            	$idForwarderContact = $row['id'];
	            	$_SESSION['forwarder_user_id']=$row['id'];
                        $_SESSION['forwarder_id']=$row['idForwarder'];
					
                        if(__ENVIRONMENT__ == "LIVE")
                        {
                            setcookie("__FORWARDR_EMAIL_COOKIE__", $this->szEmail, time()+(3600*24*90),'/',__BASE_URL_SECURE__,true);
                        }
                        else
                        {
                            setcookie("__FORWARDR_EMAIL_COOKIE__", $this->szEmail, time()+(3600*24*90),'/');
                        } 	
                        /*
                        if($row['iConfirmed']!=1)
                        {
                                $_SESSION['forwarder_iConfirmed'] = $row['iConfirmed'];
                        }
                        */	  
                        $this->updateForwarderLastLogin($idForwarderContact);
                        return true ;
	            }
	            else
	            {
	            	$query="
                            SELECT
                                id
                            FROM
                                ".__DBC_SCHEMATA_MANAGEMENT__."
                            WHERE
                                szEmail='".mysql_escape_custom($this->szEmail)."'
                            AND
                                szPassword = '".mysql_escape_custom(trim(md5($this->szNonFrocePassword)))."'
                            AND
                                iAllAccess = '1'	
	            	";
	            	//echo $query."<br />" ;
	            	if( ( $result = $this->exeSQL( $query ) ) )
                        {
                            if( $this->getRowCnt() > 0 )
                            {
                                $kForwarder = new cForwarder();	
                                $kForwarder->isHostNameExist($this->szControlPanelUrl);	
                                //echo " forwarder id ".$kForwarder->id ;
                                if($kForwarder->id>0)
                                {	            	
                                    $row=$this->getAssoc($result);
                                    $_SESSION['forwarder_user_id']=$row['id'];
                                    $_SESSION['forwarder_admin_id']=$row['id'];
                                    $_SESSION['forwarder_id']=$kForwarder->id;
                                    //echo $_SESSION['forwarder_user_id'] ;
                                    return true ;
                                }
                                else
                                {
                                        $this->addError( "szEmail" , t($this->t_base.'messages/forwarder_doenot_exists') );
                                        return false;
                                }
                            }
                            else
                            {
                                $this->addError( "szEmail" , t($this->t_base.'messages/wrong_username_password') );
                                return false;
                            }
                        }
                        else
                        {
                            $this->addError( "szEmail" , t($this->t_base.'messages/wrong_username_password') );
                            return false;
                        }
	            }
                }
                else
                {
                        $this->error = true;
                        $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                        $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                        return false;
                }
            }
            else
            {
                    return false;
            }
	}
	
        function updateForwarderLastLogin($idForwarderContact)
        {
            if($idForwarderContact>0)
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
                    SET
                        dtLastLogin=now()
                    WHERE
                        id = '".(int)$idForwarderContact."'		
                ";
                if($resul=$this->exeSQL($query))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            } 
        }
	function findForwarderIdFrControlPanel($controlPanel)
	{
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_FORWARDERS__."	
			WHERE 
				szControlPanelUrl='".sanitize_all_html_input($controlPanel)."'
		";
		
		if($result=$this->exeSQL($query))
		{	
			if($this->iNumRows>0)
			{
			$row=$this->getAssoc($result);
			return $row['id'];
			}
			else
			{
				return NULL;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}

	function forgotPassword($szEmail,$idForwarder)
	{
		if((int)$idForwarder>0)
		{
			$this->set_szEmail(sanitize_all_html_input(strtolower($szEmail)));
			//exit if error exist.
			if ($this->error === true)
			{
				return false;
			}
			
			$query="
				SELECT
					id,
					szFirstName,
					szLastName,
					iConfirmed
				FROM
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
				WHERE
					szEmail = '".mysql_escape_custom($this->szEmail)."'
				AND 
					idForwarder = ".(int)$idForwarder."	
				AND
					iActive = 1	
			";
			//echo $query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{
	        	// Check if the user exists
	            if( $this->getRowCnt() > 0 )
	            {
	            		$row=$this->getAssoc($result);
	            		$this->id = $row['id'];
	            		$this->iConfirmed = $row['iConfirmed'];
	            		if($this->iConfirmed)
	            		{
	            			$add_query = "
	            						,
	            						iPasswordUpdated  = 1
	            						";
	            		}
	            		else
	            		{
	            			$add_query = "";
	            		}
	            		$password=create_password();
	            		$query="
	            			UPDATE
	            				".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
	            			SET
	            				szPassword='".mysql_escape_custom(md5($password))."'
	            				".$add_query."
	            			WHERE
								szEmail = '".mysql_escape_custom($this->szEmail)."'	
							AND 
								idForwarder = ".(int)$idForwarder."
							AND
								id = $this->id	
	            		";
	            		//echo $query;
	            		$result = $this->exeSQL( $query );
	            		$replace_ary['szFirstName']=$row['szFirstName'];
	            		$replace_ary['szLastName']=$row['szLastName'];
	            		$replace_ary['szPassword']=$password;
	            		$replace_ary['szEmail']=$this->szEmail;
	            		createEmail(__FORGOT_PASSWORD__, $replace_ary,$this->szEmail, __FORGOT_PASSWORD_SUBJECT__, __STORE_SUPPORT_EMAIL__,$this->id, __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__);
	                   return true;
	            }
	            else
	            {
	            	$this->addError( "szEmail" , t($this->t_base.'messages/email_not_exists') );
	                return false;
	            }
			}
		}
	}
	
	function addForwarderEmail($data)
	{
            if(!empty($data))
            {
                $this->set_szEmail(sanitize_all_html_input(strtolower($data['szEmail'])));

                //exit if error exist.
                if ($this->error === true)
                {
                    return false;
                }
                if($this->isForwarderEmailExist($this->szEmail))
                {
                    $this->addError( "szEmail" , t($this->t_base.'messages/email_exists') );
                    return false;
                }

                $newPass= uniqid(rand(1,9), false);
                $newPass = substr($newPass, -7);
 
                $query="
                    INSERT INTO
                        ".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
                    (
                        szEmail,
                        idForwarder,
                        idForwarderContactRole,
                        iActive,
                        szPassword
                    )	
                    VALUES
                    (
                        '".mysql_escape_custom(trim($this->szEmail))."',
                        '".mysql_escape_custom($_SESSION['forwarder_id'])."',
                        '".mysql_escape_custom(trim($data['idForwarderRole']))."',
                        '1',
                        '".mysql_escape_custom(trim(md5($newPass)))."'
                    )
                "; 
                if($result=$this->exeSQL($query))
                {
                    $szNewProfileEmail = $this->szEmail ;
                    if($_SESSION['forwarder_admin_id']>0)
                    {
                        $kAdmin = new cAdmin();
                        $kAdmin->getAdminDetails($_SESSION['forwarder_admin_id']);
                        
                        $this->szFirstName = $kAdmin->szFirstName ;
                        $this->szLastName = $kAdmin->szLastName ;
                        $this->szEmail = $kAdmin->szEmail ; 
                    }
                    else
                    {
                        $this->load($_SESSION['forwarder_user_id']); 
                    } 
                    
                    $kForwarder = new cForwarder();
                    $kForwarder->load($_SESSION['forwarder_id']);

                    $replace_ary['szFirstName'] = $this->szFirstName;
                    $replace_ary['szLastName'] = $this->szLastName;
                    $replace_ary['szEmail'] = $this->szEmail;
                    $replace_ary['szNewUserEmail'] = $szNewProfileEmail;
                    $replace_ary['szPassword'] = $newPass;
                    $siteUrl = 'http://'.$kForwarder->szControlPanelUrl ;
                    $replace_ary['szControlPanelUrl'] = "<a href='".$siteUrl."'>".$siteUrl."</a>";

                    $this->loadContactRole($data['idForwarderRole']);

                    $replace_ary['szForwarderRole']=$this->szRoleName ;

                    //$confirmationLink=__BASE_URL__."/forwarder_confirmation.php?confirmationKey=".$ConfirmKey;
                    //$confirmationLinkHttps=__BASE_URL_SECURE__."/forwarder_confirmation.php?confirmationKey=".$ConfirmKey;
                   // $replace_ary['szLink']="<a href='".$confirmationLink."'>CLICK TO VERIFY E-MAIL ADDRESS</a>";
                   //$replace_ary['szHttpsLink']=$confirmationLinkHttps;

                    $replace_ary['szForwarderCompany'] = $kForwarder->szDisplayName;
                    createEmail('__ADD_NEW_FORWARDER_PROFILE__', $replace_ary,$szNewProfileEmail, __ADD_NEW_FORWARDER_PROFILE_SUBJECT__, __STORE_SUPPORT_EMAIL__,$_SESSION['forwarder_user_id'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__);
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
	function addForwarderEmailByAdmin($data)
	{
		if(!empty($data))
		{
			$this->set_szEmail(sanitize_all_html_input(strtolower($data['szEmail'])));
			$this->set_idForwarder(sanitize_all_html_input(strtolower($data['idForwarder'])));
			$this->set_idForwarderRole(sanitize_all_html_input(strtolower($data['idForwarderRole'])));
			//exit if error exist.
			if ($this->error === true)
			{
				return false;
			}
			if($this->isForwarderEmailExist($this->szEmail))
			{
				$this->addError( "szEmail" , t($this->t_base.'messages/email_exists') );
				return false;
			}
			
			$newPass= uniqid(rand(1,9), false);
            $newPass = substr($newPass, -7);
            
			$query="
				INSERT INTO
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
				(
					szEmail,
					idForwarder,
					idForwarderContactRole,
					iActive,
					szPassword
				)	
				VALUES
				(
					'".mysql_escape_custom(trim($this->szEmail))."',
					'".(int)$this->idForwarder."',
					'".(int)trim($this->idForwarderRole)."',
					'1',
					'".mysql_escape_custom(trim(md5($newPass)))."'
				)
			";
			if($result=$this->exeSQL($query))
			{	
				$this->id = $this->iLastInsertID;
				//$szNewProfileEmail = $this->szEmail ;
				$this->load($this->id);
				//print_r($this);
				$kForwarder = new cForwarder();
				$kForwarder->load($this->idForwarder);
	            $replace_ary['szEmail'] = $this->szEmail;
	            //$replace_ary['szNewUserEmail'] = $szNewProfileEmail;
	            $replace_ary['szPassword'] = $newPass;
	            $siteUrl = 'http://'.$kForwarder->szControlPanelUrl ;
	            $replace_ary['szControlPanelUrl'] = "<a href='".$siteUrl."'>".$siteUrl."</a>";
	            
	            $this->loadContactRole($data['idForwarderRole']);
	            
	            $replace_ary['szForwarderRole']=$this->szRoleName ;
	            
	            //$confirmationLink=__BASE_URL__."/forwarder_confirmation.php?confirmationKey=".$ConfirmKey;
			    //$confirmationLinkHttps=__BASE_URL_SECURE__."/forwarder_confirmation.php?confirmationKey=".$ConfirmKey;
			   // $replace_ary['szLink']="<a href='".$confirmationLink."'>CLICK TO VERIFY E-MAIL ADDRESS</a>";
			   //$replace_ary['szHttpsLink']=$confirmationLinkHttps;
	            
	            $replace_ary['szForwarderCompany'] = $kForwarder->szDisplayName;
	            createEmail('__ADD_NEW_FORWARDER_PROFILE_BY_ADMIN__', $replace_ary,$this->szEmail, '', __STORE_SUPPORT_EMAIL__,$this->id, __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__);
	            return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function getAllForwarderRole()
	{
		$query="
			SELECT
				id,
				szRoleName
			FROM
				".__DBC_SCHEMATA_FORWARDERS_CONTACT_ROLE__."	
		";
		if($result=$this->exeSQL($query))
		{
			$forwarderRoleAry=array();
			$ctr=0;
			while($row=$this->getAssoc($result))
			{
				$forwarderRoleAry[$ctr]=$row;
				$ctr++;
			}
			return $forwarderRoleAry ;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
function contactEmailForwarder($data,$subject,$idForwarder)
	{
		if(!empty($data))
			{
				$this->set_szMessage(sanitize_specific_html_input($data['szMessage']));
				$this->set_szContact(sanitize_specific_html_input($data['szEmail']));
				//exit if error exist.
				if ($this->error === true)
				{
					return false;
				}
				/*
				* @toEmail is mailing address of main forwarder
				* uncomment function sendEmail for hosting server 
				*
				*/

				$this->getMainAdmin($idForwarder);
				$toEmail = $this->mainAdminEmail;
				$subject=$subject." ".date("d/m/Y");
				$message="";
				if(!empty($this->szContact))
				{
					$message .=$this->szContact."<br/>";
				}
				$message .=$this->szMessage;
				
				sendEmail(__STORE_SUPPORT_EMAIL__,__STORE_SUPPORT_EMAIL__,$subject,$message,__STORE_SUPPORT_EMAIL__,0);
				return true;
			}	
		return true;
	}
	
	function updateForwarderContactFirstLogin($data)
	{
		if(!empty($data))
		{
			$this->set_szFirstName(ucfirst(trim(sanitize_all_html_input($data['szFirstName']))));
			$this->set_szLastName(ucfirst(trim(sanitize_all_html_input($data['szLastName']))));
			$this->set_idCountry(trim(sanitize_all_html_input($data['idCountry'])));			
			$this->set_szPhoneNumber(trim(sanitize_all_html_input(urldecode(base64_decode($data['szPhoneUpdate'])))));
			$this->set_szMobileNumber(trim(sanitize_all_html_input(urldecode(base64_decode($data['szMobileUpdate'])))));	
			$this->set_idForwarder(trim(sanitize_all_html_input($data['idForwarder'])));
			$this->set_id(trim(sanitize_all_html_input($data['id'])));
			
			$this->set_szPassword(trim(sanitize_all_html_input(strtolower($data['szPassword']))));
			$this->set_szConfirmPassword(trim(sanitize_all_html_input(strtolower($data['szConfirmPassword']))));
			//exit if error exist.
			if ($this->error === true)
			{
				return false;
			}
			if($this->szPassword != $this->szConfirmPassword)
			{
				$this->addError( "szEmail" , t($this->t_base.'messages/password_confirm_password_not_match') );
				return false;
			}
			
			if(!empty($this->szPhoneNumber))
			{
				$szPhoneNumber = $this->szPhoneNumber;
			}
			
			if(!empty($this->szMobileNumber))
			{
				$szMobileNumber =$this->szMobileNumber;
			}
			
			$query="
				UPDATE 
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
				SET
					szFirstName = '".mysql_escape_custom(trim($this->szFirstName))."',
					szLastName = '".mysql_escape_custom(trim($this->szLastName))."',
					idCountry = '".(int)$this->idCountry."',
					szPhone = '".mysql_escape_custom(trim($szPhoneNumber))."',
					szPassword = '".mysql_escape_custom(md5(trim($this->szPassword)))."',
					szMobile = '".mysql_escape_custom(trim($szMobileNumber))."',
					iConfirmed = '1',
					iActive = '1',
					iPasswordUpdated = '0'
				WHERE
					id = '".(int)$this->id."'	
			";
			if($result=$this->exeSQL($query))
			{
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function getForwarderFirstAndLastName($id)
	{
		$query="
			SELECT
				szFirstName,
				szLastName
			FROM
				".__DBC_SCHEMATA_FORWARDERS_CONTACT__."	
			WHERE 
				id='".(int)$id."'	
		";
		
		if($result=$this->exeSQL($query))
		{
			$row=$this->getAssoc($result);
			$this->szFirstName=$row['szFirstName'];
			$this->szLastName=$row['szLastName'];
			
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	function updateIConfirmedStatus($idUser)
	{
		if((int)$idUser>0)
		{
			$query="
				UPDATE
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
				SET
					iConfirmed='1',
					szConfirmKey='',
					dtConfirmationCodeSent = ''
				WHERE
					id='".(int)$idUser."'
			";
			$result = $this->exeSQL( $query );
			$this->szFirstName=$row['szFirstName'];
			$this->szLastName=$row['szLastName'];
		}
	}
	
	function updateForwarderFromAdmin($contactArr)
	{	
		//print_r($contactArr);
		$this->set_controlBooking((int)trim(sanitize_all_html_input($contactArr['szBooking'])));
		$this->set_id((int)sanitize_all_html_input($contactArr['idForwarder']));
		$this->set_szFirstName(sanitize_all_html_input($contactArr['szFirstName']));
		$this->set_szLastName(sanitize_all_html_input($contactArr['szLastName']));
		$this->set_idCountry(sanitize_all_html_input($contactArr['szCountry']));
		$this->set_szEmail(sanitize_all_html_input($contactArr['szEmail']));
		$this->set_szOldEmail(sanitize_all_html_input($contactArr['szOldEmail']));
		$this->set_szPhoneNumber(sanitize_all_html_input(urldecode(base64_decode($contactArr['szPhoneUpdate']))));
		$this->set_szMobileNumber(sanitize_all_html_input(urldecode(base64_decode($contactArr['szMobileUpdate']))));
		$this->set_idForwarderContactRole(trim(sanitize_all_html_input($contactArr['idForwarderContactRole'])));
                $this->set_szResponsibility(sanitize_all_html_input(($contactArr['szResponsibility'])),false);
		if($this->error === true)
		{
			return false;
		}
		if($this->isForwarderEmailExist($this->szEmail,$this->id))
		{
			$this->addError( "szEmail" , t($this->t_base.'messages/email_exists') );
			return false;
		}
			if(!empty($this->szPhoneNumber))
			{
				$szPhoneNumber = $this->szPhoneNumber;
			}
			
			if(!empty($this->szMobileNumber))
			{
				$szMobileNumber=$this->szMobileNumber;
			}
			
			$query="
				UPDATE 
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
				SET
					szFirstName = '".mysql_escape_custom(trim($this->szFirstName))."',
					szLastName = '".mysql_escape_custom(trim($this->szLastName))."',
					idCountry = '".(int)mysql_escape_custom($this->idCountry)."',
					szPhone = '".mysql_escape_custom(trim($szPhoneNumber))."',
					szMobile = '".mysql_escape_custom(trim($szMobileNumber))."',
					szEmail = '".mysql_escape_custom(trim($this->szEmail))."',
					idForwarderContactRole = '".(int)mysql_escape_custom($this->idForwarderContactRole)."',
					szBookingFrequency = '".(int)mysql_escape_custom($this->controlBooking)."',
					iActive = '1',
                                        szResponsibility = '".mysql_escape_custom(trim($this->szResponsibility))."'
				WHERE
					id = '".(int)$this->id."'	
			";
			//echo $query;
			if($result=$this->exeSQL($query))
			{
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function changeszPasswordForwarder($idForwarder)
	{	
		$idForwarder = (int)sanitize_all_html_input($idForwarder);
		$password = create_password();
		
		if($idForwarder>0)
		{
			$query="
				UPDATE 
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
				SET
					szPassword = '".mysql_escape_custom(md5($password))."'
				WHERE 
					id = '".(int)$idForwarder."'
					";	
			if($result=$this->exeSQL($query))
			{
				return array(true,$password);
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function setForwarderProfileDetails($contactArr)
	{
		$this->set_id((int)sanitize_all_html_input($contactArr['idForwarder']));
		//$this->set_szFirstName(sanitize_all_html_input($contactArr['szFirstName']));
		//$this->set_szLastName(sanitize_all_html_input($contactArr['szLastName']));
		//$this->set_szCountry((int)sanitize_all_html_input($contactArr['szCountry']));
		//$this->set_szEmail(sanitize_all_html_input($contactArr['szEmail']));
		//$this->set_szOldEmail(sanitize_all_html_input($contactArr['szOldEmail']));
		//$this->set_szPhoneNumber(sanitize_all_html_input($contactArr['szPhone']));
		//$this->set_szMobileNumber(sanitize_all_html_input($contactArr['szMobile']));
		//$this->set_idForwarderContactRole(trim(sanitize_all_html_input($contactArr['idForwarderContactRole'])));
		return true;
	}
	
function preload($idForwarderContact)
	{
		if($idForwarderContact>0)
		{
			$query="
				SELECT
					c.id,
					c.idForwarder,
					c.szFirstName,
					c.szLastName,
					c.idCountry,
					c.szPhone,
					c.szMobile,
					c.szEmail,
					c.idForwarderContactRole,
					c.szBookingFrequency,
                                        c.szResponsibility
				FROM
					".__DBC_SCHEMATA_FORWARDERS_CONTACT__." AS	c
				WHERE	
					c.id = '".(int)$idForwarderContact."'	
			";
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					$this->id=sanitize_all_html_input(trim($row['id']));
					$this->idForwarder=sanitize_all_html_input(trim($row['idForwarder']));
					$this->szFirstName=sanitize_all_html_input(trim($row['szFirstName']));					
					$this->szLastName=sanitize_all_html_input(trim($row['szLastName']));
					$this->idCountry=sanitize_all_html_input(trim($row['idCountry']));
					$this->szPhone=sanitize_all_html_input(trim($row['szPhone']));
					$this->szMobile=sanitize_all_html_input(trim($row['szMobile']));
					$this->szEmail=sanitize_all_html_input(trim($row['szEmail']));
					$this->idForwarderContactRole=sanitize_all_html_input(trim($row['idForwarderContactRole']));				
					$this->controlBooking=(int)trim(sanitize_all_html_input($row['szBookingFrequency']));
                                        $this->szResponsibility=trim(sanitize_all_html_input($row['szResponsibility']));
					return $row ;
				}
				else
				{
					return false ;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function newForwarderProfile($createProfileArr)
	{
		$this->set_idForwarder((int)sanitize_all_html_input($createProfileArr['forwarderId']));
		$this->set_szEmail(sanitize_all_html_input($createProfileArr['eMail']));
		$this->set_idForwarderContactRole(trim(sanitize_all_html_input($createProfileArr['idForwarderContactRole'])));
		
		if($this->error === true)
		{
			return false;
		}
		if($this->isForwarderEmailExist($this->szEmail,$this->id))
		{
			$this->addError( "szEmail" , t($this->t_base.'messages/email_exists') );
			return false;
		}
		$query="
                    INSERT INTO 
                        ".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
                    SET
                        idForwarder = '".mysql_escape_custom(trim($this->idForwarder))."',
                        szEmail = '".mysql_escape_custom(trim($this->szEmail))."',
                        idForwarderContactRole = '".(int)mysql_escape_custom($this->idForwarderContactRole)."',
                        iActive = '1',
                        dtCreateOn=NOW()
                ";
                if($result=$this->exeSQL($query))
                {
                        return true;
                }
                else
                {
                        $this->error = true;
                        $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                        $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                        return false;
                }
	}
	function getForwarderName($id)
	{
		$query="
			SELECT
				szDisplayName
			FROM
				".__DBC_SCHEMATA_FORWARDERS__."	
				";
		if($id>0)
		{
			$query .= "
				WHERE 
					id='".mysql_escape_custom($id)."'	
				";
		}
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			$row=$this->getAssoc($result);
			return $row['szDisplayName'];
			
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function versionConfirmedForwarder()
	{
		$query="
			SELECT
				id,
				szControlPanelUrl
			FROM
				".__DBC_SCHEMATA_FORWARDERS__."		
		";
		//WHERE 
		//		iAgreeTNC=1	
		if($result=$this->exeSQL($query))
		{	
			if($this->iNumRows>0)
			{
			while($row=$this->getAssoc($result))
			$idForwarder[]=$row;
			return $idForwarder;
			}
			else
			{
				return array();
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function showPreferencesForwarder($id)
	{
		$id	=	(int)sanitize_all_html_input($id);
		if($id>0)
		{
			$query="
				SELECT 
					id,
					szEmail,
					idForwarderContactRole
				FROM 
					".__DBC_SCHEMATA_FORWARDER_PREFERENCES__."
				WHERE 
					idForwarder = ".mysql_escape_custom($id)."
				AND 
					iActive = 1
				AND
					idForwarderContactRole IN (".__BOOKING_PROFILE_ID__.",".__PAYMENT_AND_BILLING_PROFILE_ID__.",".__CUSTOMER_PROFILE_ID__.")							
				ORDER BY
					idForwarderContactRole = ".__CUSTOMER_PROFILE_ID__." ,
					idForwarderContactRole = ".__PAYMENT_AND_BILLING_PROFILE_ID__." ,
					idForwarderContactRole = ".__BOOKING_PROFILE_ID__." 
				";
			
			if($result=$this->exeSQL($query))
			{	
				if($this->iNumRows>0)
				{
					while($row=$this->getAssoc($result))
					$preferences[]=$row;
					return $preferences;
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function showPreferencesEachForwarder($idPrefer)
	{
		$id	=	(int)sanitize_all_html_input($idPrefer);
		if($id>0)
		{
			$query="
				SELECT 
					id,
					szEmail,
					idForwarder,
					idForwarderContactRole
				FROM 
					".__DBC_SCHEMATA_FORWARDER_PREFERENCES__."
				WHERE 
					id = ".mysql_escape_custom($id)."
				AND 
					iActive = 1
				AND
					idForwarderContactRole IN (".__BOOKING_PROFILE_ID__.",".__PAYMENT_AND_BILLING_PROFILE_ID__.",".__CUSTOMER_PROFILE_ID__.")							
				";
			//echo $query;
			if($result=$this->exeSQL($query))
			{	
				if($this->iNumRows>0)
				{
					while($row=$this->getAssoc($result))
					return $row;
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function updatePreferencesEachForwarder($prefArr)
	{	
		$this->set_id((int)sanitize_all_html_input($prefArr['forwarderContactId']));
		$this->set_idForwarder((int)sanitize_all_html_input($prefArr['idForwarder']));
		$this->set_idForwarderRole((int)sanitize_all_html_input($prefArr['szContactRole']));
		$this->set_szEmail(sanitize_all_html_input($prefArr['szEmail']));
		if($this->error === true)
		{
			return false;
		}
		//print_r($this->arErrorMessages);
		if(!($this->isContactEmailExist($this->szEmail,$this->id,$this->idForwarder,$this->idForwarderRole)))
		{
			return false;
		}
		
		
		$query="
			UPDATE
				".__DBC_SCHEMATA_FORWARDER_PREFERENCES__."
			SET
				szEmail					=	'".mysql_escape_custom($this->szEmail)."',
				idForwarderContactRole	=	".(int)$this->idForwarderRole."
			WHERE
				id						=	".(int)$this->id."	
			AND
				idForwarder				=	".(int)$this->idForwarder."	
			";
		//echo $query;
		if($result=$this->exeSQL($query))
		{	
			return true;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
		
	}
	function addPreferencesEachForwarder($prefArr)
	{	
		$this->set_idForwarder((int)sanitize_all_html_input($prefArr['idForwarder']));
		$this->set_idForwarderRole((int)sanitize_all_html_input($prefArr['szContactRole']));
		$this->set_szEmail(sanitize_all_html_input($prefArr['szEmail']));
		if($this->error === true)
		{
			return false;
		}
		//print_r($this->arErrorMessages);
		if(!($this->isContactEmailExist($this->szEmail,'',$this->idForwarder,$this->idForwarderRole)))
		{
			return false;
		}
		
		
		$query="
			INSERT INTO
				".__DBC_SCHEMATA_FORWARDER_PREFERENCES__."
				(
					szEmail,
					idForwarderContactRole,
					idForwarder,
					iActive,
					dtCreated,
					dtModified
				)							
				VALUES
				(
					'".mysql_escape_custom($this->szEmail)."',
					".(int)$this->idForwarderRole.",
					".(int)$this->idForwarder.",
					1,
					NOW(),
					NOW()
				)	
			";
		//Echo $query;
		if($result=$this->exeSQL($query))
		{	
			$kForwarder = new cForwarder();
			$kForwarder->isForwarderOnline($this->id,'UPDATE',false);
			return true;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
		
	}
	function deactiveForwarderPreferences($prefId)
	{	
		$this->set_id((int)sanitize_all_html_input($prefId));
		if($this->error === true)
		{
			return false;
		}
		$query="
			UPDATE
				".__DBC_SCHEMATA_FORWARDER_PREFERENCES__."
			SET
				iActive =0
			WHERE
				id	=	".mysql_escape_custom($this->id)."
			";
		//echo $query;
		if($result=$this->exeSQL($query))
		{	
			return true;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
		
	}
	function findForwarderPreferencesId($prefId)
	{	
		$id =	(int)sanitize_all_html_input($prefId);
		$query="
			SELECT
				idForwarder 
			FROM	
				".__DBC_SCHEMATA_FORWARDER_PREFERENCES__."
			WHERE	
				id	=	".mysql_escape_custom($id)."
			";
		if($result=$this->exeSQL($query))
		{	
			if($this->iNumRows>0)
			{
				return $row=$this->getAssoc($result);	
			}
			else
			{
				return array();
			}	
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
		
	}
	function updatePreferencesCotrolPanel($data,$idForwarder)
	{	$id=(int)$idForwarder;
		if($id>0)
		{	
			$this->set_szComment(nl2br((string)trim(sanitize_specific_html_input($data['szComment']))));
			$this->set_dtValidTo((string)trim(sanitize_all_html_input($data['dtValidTo']))); 
			$this->set_szControlBookingReference(trim(sanitize_all_html_input($data['BookingRef'])));
			$this->set_szControlPanelUrl(trim(sanitize_all_html_input($data['dnsURL'])));
			$this->set_iAutomaticPaymentInvoicing(trim(sanitize_all_html_input($data['iAutomaticPaymentInvoicing'])));
			$dns= $this->szControlPanelUrl;
			$this->set_fRefFee((float)trim(sanitize_all_html_input($data['RefFee'])));
			if($this->dtValidTo == 'dd/mm/yyyy')
			{
				$this->addError( $this->dtValidTo , t($this->t_base.'fields/dtValidTo') );
                return false;
			}
			$time = explode('/',$this->dtValidTo);
			
			$time = mktime(0, 0, 0, $time[1], $time[0], $time[2]);
			$this->dtValidTo = date('Y-m-d h:i:s',$time);
			if($time < strtotime(date('Y-m-d h:i:s')) )
			{
				$this->addError( $this->dtValidTo , t($this->t_base.'fields/dtValidTo_should') );
                return false;
			}
			if($this->fRefFee==0)
			{
				$this->addError( $this->fRefFee , t($this->t_base.'fields/referal_Fee_Req') );
                return false;	
			}
			
			$this->szControlPanelUrl = $this->findSubDomainName($this->szControlPanelUrl);
			$controlArr=$this->findAllforwarderSubDomain($id); 	
			if($controlArr!=array())
			{
				foreach($controlArr as $key=>$value)
				{
					$checkValidateUrl=$this->findSubDomainName($value['szControlPanelUrl']);
					if($checkValidateUrl == $this->szControlPanelUrl)
					{
						$this->addError( $this->szControlPanelUrl ,$dns.".transporteca.com ".t($this->t_base.'fields/alreadyExists') );
               			return false;	
					}
				}
			}
			if($this->error==true)
			{
                            return false;
			}
			
			$query= "
                            UPDATE
                                ".__DBC_SCHEMATA_FROWARDER__."
                            SET 
                                szControlPanelUrl='".mysql_escape_custom($this->szControlPanelUrl).".transporteca.com',
                                szForwarderAlias='".mysql_escape_custom(strtoupper($this->szControlBookingReference))."', 
                                dtReferealFeeValid='".mysql_escape_custom($this->dtValidTo)."',
                                szReferalFeeComment='".mysql_escape_custom($this->szComment)."',
                                iAutomaticPaymentInvoicing = '".mysql_escape_custom($this->iAutomaticPaymentInvoicing)."'
                            WHERE
                                id='".(int)$id."'
                        ";
			//echo $query;die;	
			if( $this->exeSQL($query) )
			{
                            return true;
			}	
			else
			{
                            $this->error = true;
                            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                            return false;
			}
		}
		
	}
	function validateCode($idForwarder)
	{
	$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_FORWARDERS__."
			WHERE
				szForwarderAlias = '".mysql_escape_custom($this->szControlBookingReference)."'
			AND 
				id<>".(int)$idForwarder."	
		";
		if( ( $result = $this->exeSQL( $query ) ) )
		{
        	// Check if the user exists
            if( $this->getRowCnt() > 0 )
            {
                 $this->addError('szcode', 'Following codes are already in use: ' .$this->findCode($idForwarder));
				$this->Error = true;
				//return false;
            }
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function findCode($idForwarder)
	{
	$query="
			SELECT
				szForwarderAlias
			FROM
				".__DBC_SCHEMATA_FORWARDERS__."
			WHERE
				id<>".(int)$idForwarder."
			ORDER BY 
				szForwarderAlias ASC
					
		";
		//echo $query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
        	// Check if the user exists
            if( $this->getRowCnt() > 0 )
            {
            	$code = array();
                 while ($row=$this->getAssoc($result)) {
                 	$code[] = $row['szForwarderAlias'];
                 }
                 return implode(', ',$code);
            }
            else
            {
                    return array();
            }
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function updateSettingForwarder($data,$idForwarder)
	{	$id=(int)$idForwarder;
		if($id>0)
		{	
			$this->set_szControlBookingReference(trim(sanitize_all_html_input($data['BookingRef'])));
			$this->set_iAvailableForManualQuotes(trim(sanitize_all_html_input($data['iAvailableForManualQuotes'])));
                        $this->set_iOffLineQuotes(trim(sanitize_all_html_input($data['iOffLineQuotes'])));
                        $this->set_iDisplayQuickQuote(trim(sanitize_all_html_input($data['iDisplayQuickQuote'])));
                        
			
			$this->validateCode($idForwarder);
			$this->set_szControlPanelUrl(trim(sanitize_all_html_input($data['dnsURL'])));
			$dns= $this->szControlPanelUrl;
			$this->szControlPanelUrl = $this->findSubDomainName($this->szControlPanelUrl);
			$controlArr=$this->findAllforwarderSubDomain($id); 	
			if($controlArr!=array())
			{
				foreach($controlArr as $key=>$value)
				{
					$checkValidateUrl=$this->findSubDomainName($value['szControlPanelUrl']);
					if($checkValidateUrl == $this->szControlPanelUrl)
					{
						$this->addError( $this->szControlPanelUrl ,$dns.".transporteca.com ".t($this->t_base.'fields/alreadyExists') );
               			return false;	
					}
				}
			}
			if($this->error==true)
			{
                            return false;
			}
			
			$query=" 
                            UPDATE
                                ".__DBC_SCHEMATA_FROWARDER__."
                            SET
                                szControlPanelUrl='".mysql_escape_custom($this->szControlPanelUrl).".transporteca.com',
                                szForwarderAlias='".mysql_escape_custom(strtoupper($this->szControlBookingReference))."',
                                iAvailableForManualQuotes='".mysql_escape_custom($this->iAvailableForManualQuotes)."',
                                iOffLineQuotes = '".mysql_escape_custom($this->iOffLineQuotes)."',
                                iDisplayQuickQuote = '".mysql_escape_custom($this->iDisplayQuickQuote)."'
                            WHERE
                                id='".(int)$id."'
                        ";
			//echo $query;die;	
			if( $this->exeSQL($query) )
			{
				return true;
			}	
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		
	}
	function updatePricingForwarder($data,$idForwarder)
	{	
            $id=(int)$idForwarder;
            if($id>0)
            {	 
                //$this->set_fRefFee((float)trim(sanitize_all_html_input($data['RefFee'])));
                $this->set_dtValidTo((string)trim(sanitize_all_html_input($data['dtValidTo'])));
                $this->set_iAutomaticPaymentInvoicing(trim(sanitize_all_html_input($data['iAutomaticPaymentInvoicing'])));
                $this->set_iProfitType(trim(sanitize_all_html_input($data['iProfitType'])));
                $this->set_iCreditDays(trim(sanitize_all_html_input($data['iCreditDays'])));
  
                if(!empty($data['fReferralFee']))
                {
                    foreach($data['fReferralFee'] as $idTransportMode=>$referralFeeAry)
                    { 
                        $this->set_fReferralFee(sanitize_all_html_input($referralFeeAry),$idTransportMode);
                    }
                } 
                else
                {
                    $this->addError( 'fReferralFee_1' , t($this->t_base.'fields/referal_Fee_Req') );
                } 
                if($this->dtValidTo == 'dd/mm/yyyy')
                {
                    $this->addError( 'dtValidTo' , t($this->t_base.'fields/dtValidTo') );  
                    return false;              
                }
                $time = explode('/',$this->dtValidTo);

                $time = mktime(0, 0, 0, $time[1], $time[0], $time[2]);
                $this->dtValidTo = date('Y-m-d h:i:s',$time);
                if($time < strtotime(date('Y-m-d h:i:s')) )
                {
                    $this->addError( 'dtValidTo' , t($this->t_base.'fields/dtValidTo_should') );
                }
                if($this->error==true)
                {
                    return false;
                }
                 
                $query= "
                    UPDATE
                        ".__DBC_SCHEMATA_FROWARDER__."
                    SET  
                        dtReferealFeeValid= '".mysql_escape_custom($this->dtValidTo)."',
                        iAutomaticPaymentInvoicing = '".mysql_escape_custom($this->iAutomaticPaymentInvoicing)."',
                        iProfitType = '".mysql_escape_custom($this->iProfitType)."',
                        iCreditDays = '".mysql_escape_custom($this->iCreditDays)."'
                    WHERE
                        id='".(int)$id."'
                "; 
//                echo $query;
//                die;
                if( $this->exeSQL($query) )
                { 
                    $kReferralPricing = new cReferralPricing();
                    if(!empty($this->fReferralFee))
                    {
                        foreach($this->fReferralFee as $idTransportMode=>$fReferralFee)
                        {
                            $addReferalFeeAry = array();
                            $addReferalFeeAry['idForwarder'] = $id;
                            $addReferalFeeAry['fReferralFee'] = $fReferralFee;
                            $addReferalFeeAry['idTransportMode'] = $idTransportMode;
                            $kReferralPricing->addReferalFeeByTransportMode($addReferalFeeAry);
                        }
                    } 
                    return true;
                }	
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
		
	}
	function updateCommentsForwarder($data,$idForwarder)
	{	$id=(int)$idForwarder;
		if($id>0)
		{	
			$this->set_szComment(nl2br((string)trim(sanitize_specific_html_input($data['szComment']))));
			if($this->error==true)
			{
			return false;
			}
			
			$query=
				"UPDATE
					".__DBC_SCHEMATA_FROWARDER__."
				SET
					szReferalFeeComment='".mysql_escape_custom($this->szComment)."'
				WHERE
					id='".(int)$id."'
				";
			//echo $query;die;	
			if( $this->exeSQL($query) )
			{
				return true;
			}	
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		
	}
	function findSubDomainName($domain)
	{
		if($domain!='')
		{	
			$domain	=	str_replace('http://','',$domain);
			$domain	=	str_replace('https://','',$domain);
			$szControlURL =explode(".",$domain);
			if(strtolower(trim($szControlURL['0']))=='www')
			{	
				$domain	=	$szControlURL['1'];
				
			}
			else
			{
				$domain	=	$szControlURL['0'];
			}
			return strtolower($domain);
		}
	}
	function findAllforwarderSubDomain($id=0)
	{	
		$query=
			"SELECT
				szControlPanelUrl
			 FROM	
				".__DBC_SCHEMATA_FROWARDER__."
			 WHERE
			";
		if($id>0)
		{	
		$query .=
				"
				
					id<>'".(int)$id."'
				AND	
				";
		}
		$query .=
				"
					(
					szControlPanelUrl!=NULL
				OR
					szControlPanelUrl!=''
					)
				";
			//echo $query;	
			if($result = $this->exeSQL($query) )
			{
				if($this->iNumRows>0)
				{
					while($row=$this->getAssoc($result))
					$szControlUrlArr[]=$row;
					return $szControlUrlArr;
				}
				else
				{
					return array();
				}
			}	
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function getSystemPanelforwarderAdmin($idForwarder)
	{	$id=(int)$idForwarder;
		if($id>0)
		{
			$query= "
                            SELECT 
                                szForwarderAlias,
                                szControlPanelUrl, 
                                szReferalFeeComment,
                                dtReferealFeeValid,
                                iAutomaticPaymentInvoicing,
                                iAvailableForManualQuotes,
                                iProfitType,
                                iOffLineQuotes,
                                iDisplayQuickQuote,
                                iCreditDays
                            FROM
                                ".__DBC_SCHEMATA_FROWARDER__."
                            WHERE
                                id='".(int)$id."'	
                        ";
				if( $this->exeSQL($query) )
				{
					if($result=$this->exeSQL($query))
					{
						if($this->iNumRows>0)
						{
							$ret_ary = array();
							while($row=$this->getAssoc($result))
							{
								$ret_ary = $row;
							}
							return $ret_ary;
						}
						else
						{
							return array();
						}
					}
					else
					{
						return array();
					}
				}	
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
		}
				
	}
	function getSystemPanelforwarderSettingDet($idForwarder)
	{	$id=(int)$idForwarder;
		if($id>0)
		{
			$query=
				"SELECT 
                                    szForwarderAlias,
                                    szControlPanelUrl,
                                    iAvailableForManualQuotes,
                                    iOffLineQuotes,
                                    iDisplayQuickQuote
				FROM
					".__DBC_SCHEMATA_FROWARDER__."
				WHERE
					id='".(int)$id."'	
					";
				if( $this->exeSQL($query) )
				{
					if($result=$this->exeSQL($query))
					{
						if($this->iNumRows>0)
						{
							$ret_ary = array();
							while($row=$this->getAssoc($result))
							{
								$ret_ary = $row;
							}
							return $ret_ary;
						}
						else
						{
							return array();
						}
					}
					else
					{
						return array();
					}
				}	
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
		}
				
	}
	function getSystemPanelforwarderPricingAdmin($idForwarder)
	{	$id=(int)$idForwarder;
		if($id>0)
		{
			$query=
				"SELECT  
                                    dtReferealFeeValid,
                                    iAutomaticPaymentInvoicing,
                                    iProfitType,
                                    iCreditDays
				FROM
					".__DBC_SCHEMATA_FROWARDER__."
				WHERE
					id='".(int)$id."'	
					";
				if( $this->exeSQL($query) )
				{
					if($result=$this->exeSQL($query))
					{
						if($this->iNumRows>0)
						{
							$ret_ary = array();
							while($row=$this->getAssoc($result))
							{
								$ret_ary = $row;
							}
							return $ret_ary;
						}
						else
						{
							return array();
						}
					}
					else
					{
						return array();
					}
				}	
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
		}
				
	}
function getSystemPanelforwarderCommentsAdmin($idForwarder)
	{	$id=(int)$idForwarder;
		if($id>0)
		{
			$query=
				"SELECT 
					szReferalFeeComment
				FROM
					".__DBC_SCHEMATA_FROWARDER__."
				WHERE
					id='".(int)$id."'	
					";
				if( $this->exeSQL($query) )
				{
					if($result=$this->exeSQL($query))
					{
						if($this->iNumRows>0)
						{
							$ret_ary = array();
							while($row=$this->getAssoc($result))
							{
								$ret_ary = $row;
							}
							return $ret_ary;
						}
						else
						{
							return array();
						}
					}
					else
					{
						return array();
					}
				}	
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
		}
				
	}
	function getCompanyPricingDtails($idForwarder)
	{	$id=(int)$idForwarder;
		if($id>0)
		{
			$query=
				"SELECT  
					szReferalFeeComment,
					dtReferealFeeValid,
                                        iCreditDays
				FROM
					".__DBC_SCHEMATA_FROWARDER__."
				WHERE
					id='".(int)$id."'	
					";
				if( $this->exeSQL($query) )
				{
					if($result=$this->exeSQL($query))
					{
						if($this->iNumRows>0)
						{
							$ret_ary = array();
							while($row=$this->getAssoc($result))
							{
								$ret_ary = $row;
							}
							return $ret_ary;
						}
						else
						{
							return array();
						}
					}
					else
					{
						return array();
					}
				}	
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
		}
				
	}
	function deleteForwarderProfile($id)
	{
		$query=
				"UPDATE
					".__DBC_SCHEMATA_FROWARDER__."
				SET
					iActive=(!iActive),
					dtVersionUpdate = 0,
					iAgreeTNC = 0,
					isOnline = 0	
				WHERE
					id='".(int)$id."'
				";
			
			if( $this->exeSQL($query) )
			{
				return true;
			}	
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
	function getSystemPanelsDetails($idForwarder)
	{	$id=(int)$idForwarder;
		if($id>0)
		{
			$query=
				"SELECT 
					szControlPanelUrl as PanelUrl 
				FROM
					".__DBC_SCHEMATA_FROWARDER__."
				WHERE
					id='".(int)$id."'	
					";
				
				if( $this->exeSQL($query) )
				{
					if($result=$this->exeSQL($query))
					{
						if($this->iNumRows>0)
						{
							$ret_ary = array();
							
							while($row=$this->getAssoc($result))
							{
								$ret_ary = $row;
								
							}
							return $ret_ary;
						}
						else
						{
							return array();
						}
					}
					else
					{
						return array();
					}
				}	
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				}
		}
				
	}
	function updateForwarderPreferences($data,$idForwarder)
	{	
                //PRINT_R($data);
                //$this->findForwarderCurrency((int)$id);
                //$this->controlBooking=trim(sanitize_all_html_input($data['szBooking'])); 
                $this->set_szCurrency(trim(sanitize_all_html_input($data['szCurrency'])));
                if($this->error==true)
                {
                    return false;
                }
	
		if((count($data['szEmailBooking'])==1) AND (count($data['szEmailPayment'])==1) AND (count($data['szEmailCustomer'])==1))
		{
                    if( empty($data['szEmailBooking'][1]) AND empty($data['szEmailPayment'][1]) AND empty($data['szEmailCustomer'][1]) )
                    {
                        $this->addError( ' ',t($this->t_base.'messages/mail_id_empty') );
                        return false;
                    }
                    if(!empty($data['szEmailBooking'][1]))
                    {	$contactA=trim(sanitize_all_html_input($data['szEmailBooking'][1]));
                        $this->set_szMultipleEmail($contactA,'szBookingEmail');
                        if ($this->error === true)
                        {
                            return false;
                        }
                        $contactIdA=$data['bookingId'];
                        $existsMail=$this->isContactEmailExist($contactA,$contactIdA[1],$idForwarder,__BOOKING_PROFILE_ID__);
                        if ($this->error === true)
                        {
                            return false;
                        } 
                    }
                    elseif(empty($data['szEmailBooking'][1]))
                    {
                        $this->addError( ' ',t($this->t_base.'messages/booking_mail_id_empty') );
                        return false;
                    }
			
			if(!empty($data['szEmailPayment'][1]))
			{	$contactB=trim(sanitize_all_html_input($data['szEmailPayment'][1]));
				$this->set_szMultipleEmail($contactB,'szPaymentEmail');
				if ($this->error === true)
				{
					return false;
				}
				
				$contactIdB=$data['paymentId'];
				$existsMail=$this->isContactEmailExist($contactB,$contactIdB[1],$idForwarder,__PAYMENT_AND_BILLING_PROFILE_ID__);
				if ($this->error === true)
				{
					return false;
				}
				
			}
			elseif(empty($data['szEmailPayment'][1]))
			{
				$this->addError( ' ',t($this->t_base.'messages/payment_mail_id_empty') );
                return false;
			}
			
			if(!empty($data['szEmailCustomer'][1]))
			{	$contactC=trim(sanitize_all_html_input($data['szEmailCustomer'][1]));
				$this->set_szMultipleEmail($contactC,'szEmailCustomer');
				if ($this->error === true)
				{
					return false;
				}
				
				$contactIdC=$data['customerId'];
				$existsMail=$this->isContactEmailExist($contactC,$contactIdC[1],$idForwarder,__CUSTOMER_PROFILE_ID__);
				if ($this->error === true)
				{
					return false;
				}
			}
			if(empty($data['szEmailCustomer'][1]))
			{
				$this->addError( ' ',t($this->t_base.'messages/customer_mail_id_empty') );
                return false;
			}
			
			if ($this->error === true)
			{
				return false;
			}
			else
			{	
				if(!empty($contactA))
				{	
					$data1=$this->validateUpdateMailingAddress($contactA,$contactIdA,__BOOKING_PROFILE_ID__,$idForwarder,$contactA,1);
				
                                        //$kForwarderContact = new cForwarderContact();
                                        $forwarderContactAry = $this->getAllForwarderContactsEmail($idForwarder);
                                        if(empty($forwarderContactAry['szCatchUpBookingQuoteEmail']))
                                        {
                                            $kForwarder= new cForwarder();
                                            //If there is no catch-up email added for forwarder then then we adds general email as catch-up all booking
                                            $catchAllEmailAry = array();
                                            $catchAllEmailAry['szEmail'] = $contactA;
                                            $catchAllEmailAry['idForwarder'] = $idForwarder;
                                            $catchAllEmailAry['idForwarderContactRole'] = __CATCH_UP_ALL_BOOKING_QUOTE_PROFILE_ID__; 
                                            $kForwarder->addForwarderCatchupAllBookings($catchAllEmailAry);
                                        }
                                }
					else
					{
						$data1=1;
					}
				if(!empty($contactB))
				{
					$data2=$this->validateUpdateMailingAddress($contactB,$contactIdB,__PAYMENT_AND_BILLING_PROFILE_ID__,$idForwarder,$contactB,1);
				}
					else
					{
						$data2=1;
					}
				if(!empty($contactC))
				{
					$data3=$this->validateUpdateMailingAddress($contactC,$contactIdC,__CUSTOMER_PROFILE_ID__,$idForwarder,$contactC,1);
				}
					else
					{
						$data3=1;
					}
			}
		}
		else
		{	
			$removeArr=array();
			if($data['szEmailBooking'])
			{	
				$count=1;
				foreach($data['szEmailBooking'] as $key=>$value)
				{	
					if(!empty($data['bookingId'][$count]) && empty($value) )
					{	
						$removeArr[]=$data['bookingId'][$count];
						unset($data['bookingId'][$count],$data['szEmailBooking'][$count]);	
					}
					if(empty($data['bookingId'][$count]) && empty($value) )
					{	
						//$removeArr[]=$data['bookingId'][$count];
						unset($data['bookingId'][$count],$data['szEmailBooking'][$count]);	
					}
					else
					{
						$this->set_szMultipleEmail($value,'szBookingEmail');
					}
					
					$count++;
				}
				if(empty($data['szEmailBooking']))
				{
					$this->addError( ' ',t($this->t_base.'messages/booking_mail_id_empty') );
                	return false;
				}
				unset($count);
			}
			if($data['szEmailPayment'])
			{	
				$count=1;
				foreach($data['szEmailPayment'] as $key=>$value)
				{	
					if(!empty($data['paymentId'][$count]) && empty($value) )
					{	
						$removeArr[]=$data['paymentId'][$count];
						unset($data['paymentId'][$count],$data['szEmailPayment'][$count]);	
					}
					if(empty($data['paymentId'][$count]) && empty($value) )
					{	
						//$removeArr[]=$data['paymentId'][$count];
						unset($data['paymentId'][$count],$data['szEmailPayment'][$count]);	
					}
					else
					{
						$this->set_szMultipleEmail($value,'szPaymentEmail');
					}
					$count++;
				}
				if(empty($data['szEmailPayment']))
				{
					$this->addError( ' ',t($this->t_base.'messages/payment_mail_id_empty') );
                	return false;
				}
				unset($count);
			}
			
			if($data['szEmailCustomer'])
			{	
				$count=1;
				foreach($data['szEmailCustomer'] as $key=>$value)
				{	
					if(!empty($data['customerId'][$count]) && empty($value) )
					{
						$removeArr[]=$data['customerId'][$count];
						unset($data['customerId'][$count],$data['szEmailCustomer'][$count]);	
					}
					if(empty($data['customerId'][$count]) && empty($value) )
					{
						$removeArr[]=$data['customerId'][$count];
						unset($data['customerId'][$count],$data['szEmailCustomer'][$count]);	
					}
					else
					{
						$this->set_szMultipleEmail($value,'szCustomerEmail');
					}
					$count++;
				}
				if(empty($data['szEmailCustomer']))
				{
					$this->addError( ' ',t($this->t_base.'messages/customer_mail_id_empty') );
                	return false;
				}
				unset($count);
			}
			
			if ($this->error === true)
			{
				return false;
			}
			
			//FOR CHECKING EMAILS EXISTS OR NOT
			
			if($data['szEmailBooking'])
			{	
				$i=0;
				foreach($data['szEmailBooking'] as $key=>$value)
				{	
					$contact=trim(sanitize_all_html_input($value));
					$count=array_keys($data['szEmailBooking'],$contact);
					if(count($count)>1)
					{
						$this->addError( ' ',t($this->t_base.'messages/insert_same_email_id') );
	                	return false;
					}
					$contactId=$data['bookingId'];
					$contactRole=__BOOKING_PROFILE_ID__;
					$existsMail=$this->isContactEmailExist($contact,$contactId[$key],$idForwarder,$contactRole);
					$i++;
				}
				unset($count,$i);
			}
			
			
			if( $data['szEmailPayment'] )
			{
				$i=0;
				foreach($data['szEmailPayment'] as $key=>$value)
				{	
					$contact=trim(sanitize_all_html_input($value));
					$count=array_keys($data['szEmailPayment'],$contact);
					if(count($count)>1)
					{
						$this->addError( ' ',t($this->t_base.'messages/insert_same_email_id') );
	                	return false;
					}
					$contactId=$data['paymentId'];
					$contactRole=__PAYMENT_AND_BILLING_PROFILE_ID__;
					$existsMail=$this->isContactEmailExist($contact,$contactId[$key],$idForwarder,$contactRole);
					$i++;
				}
				unset($count,$i);	
			}
			
			
			if( $data['szEmailCustomer'] )
			{
				$i=0;
				foreach($data['szEmailCustomer'] as $key=>$value)
				{	
					$contact=trim(sanitize_all_html_input($value));
					$count=array_keys($data['szEmailCustomer'],$contact);
					if(count($count)>1)
					{
						$this->addError( ' ',t($this->t_base.'messages/insert_same_email_id') );
	                	return false;
					}
					$contactId=$data['customerId'];
					$contactRole=__CUSTOMER_PROFILE_ID__;
					$existsMail=$this->isContactEmailExist($contact,$contactId[$key],$idForwarder,$contactRole);
				}
				unset($count,$i);		
			}
			
			// FOR INSERTING DATA
			if($data['szEmailBooking'])
			{
                            $forwarderContactAry = $this->getAllForwarderContactsEmail($idForwarder);
                            foreach($data['szEmailBooking'] as $key=>$value)
                            {	

                                $contact=trim(sanitize_all_html_input($value));
                                $contactId=$data['bookingId'];
                                $contactRole=__BOOKING_PROFILE_ID__;
                                $data1=$this->validateUpdateMailingAddress($contact,$contactId,$contactRole,$idForwarder,$value,$key);
                                
                                
                                if(empty($forwarderContactAry['szCatchUpBookingQuoteEmail']))
                                {
                                    $kForwarder= new cForwarder();
                                    //If there is no catch-up email added for forwarder then then we adds general email as catch-up all booking
                                    $catchAllEmailAry = array();
                                    $catchAllEmailAry['szEmail'] = $value;
                                    $catchAllEmailAry['idForwarder'] = $idForwarder;
                                    $catchAllEmailAry['idForwarderContactRole'] = __CATCH_UP_ALL_BOOKING_QUOTE_PROFILE_ID__; 
                                    $kForwarder->addForwarderCatchupAllBookings($catchAllEmailAry);
                                }
                            }
			}
			if($data['szEmailPayment'])
			{
				foreach($data['szEmailPayment'] as $key=>$value)
				{	
					$contact=trim(sanitize_all_html_input($value));
					$contactId=$data['paymentId'];
					$contactRole=__PAYMENT_AND_BILLING_PROFILE_ID__;
					$data2=$this->validateUpdateMailingAddress($contact,$contactId,$contactRole,$idForwarder,$value,$key);
				}
			}	
			if($data['szEmailCustomer'])
			{
				foreach($data['szEmailCustomer'] as $key=>$value)
				{	
					$contact=trim(sanitize_all_html_input($value));
					$contactId=$data['customerId'];
					$contactRole=__CUSTOMER_PROFILE_ID__;
					$data3=$this->validateUpdateMailingAddress($contact,$contactId,$contactRole,$idForwarder,$value,$key);
				}
			}
		}
		if($data1 && $data2 && $data3)
		{	
                    if(isset($removeArr))
                    {
                        foreach($removeArr as $key=>$value)
                        {	
                            if(!empty($value))
                            {
                                $checkRemove=$this->removeMailingAddresses($value);
                            }
                        }
                        if($checkRemove)
                        {
                            $check =  true;
                        }
                    }
                    //$kForwarder = new cForwarder();
                    //$kForwarder->isForwarderOnline($idForwarder,'UPDATE',false);	
                    $query= "
                        UPDATE
                            ".__DBC_SCHEMATA_FORWARDERS__."
                        SET 
                            szCurrency='".mysql_escape_custom($this->szCurrency)."'
                        WHERE
                            id='".(int)$idForwarder."'
                    ";
                    //echo $query;	
                    if( $this->exeSQL($query) )
                    { 
                        $kForwarder=new cForwarder();
                        $kForwarder->load($idForwarder);
                        //echo $kForwarder->iCurrencyFlag;
                        if($kForwarder->iCurrencyFlag==0)
                        {
                            $query= "
                                UPDATE
                                    ".__DBC_SCHEMATA_FORWARDERS__."
                                SET
                                    iCurrencyFlag='1'
                                WHERE
                                    id='".(int)$idForwarder."'
                            ";	
                            //echo $query;		
                            $result = $this->exeSQL( $query ) ;	
                            $kForwarder = new cForwarder();
                            $kForwarder->isForwarderOnline($id,'UPDATE',true);			
                            $this->setForwarderTransactionBaseline($idForwarder);
                        }
                    }
                    return true;
		}
	}
        
        function updateForwarderPreferencesByMode($data)
        {
            if(!empty($data))
            {
                $this->set_idTransportMode(sanitize_all_html_input($data['idTransportMode']));
                $this->set_idOriginCountry(sanitize_all_html_input($data['idOriginCountry']));
                $this->set_idDestinationCountry(sanitize_all_html_input($data['idDestinationCountry']));
                $this->set_szEmail(sanitize_all_html_input($data['szEmail']));
                $this->set_szBookingEmail(sanitize_all_html_input($data['szBookingEmail']));
                
                if($this->error==true)
                {
                    return false;
                }  
                
                $this->idForwarder = $_SESSION['forwarder_id'];
                if($data['idPreferences']>0)
                {
                    $query = "  
                        UPDATE
                            ".__DBC_SCHEMATA_FORWARDER_PREFERENCES__."
                        SET
                           idTransportMode = '".  mysql_escape_custom(trim($this->idTransportMode))."',
                           idOriginCountry = '".  mysql_escape_custom(trim($this->idOriginCountry))."',
                           idDestinationCountry = '".  mysql_escape_custom(trim($this->idDestinationCountry))."',
                           idForwarder = '".  mysql_escape_custom(trim($this->idForwarder))."',
                           szEmail = '".  mysql_escape_custom(trim($this->szEmail))."',
                           idForwarderContactRole = '".__QUOTES_AND_BOOKING_PROFILE_ID__."',
                           dtModified = now(),
                           szBookingEmail='".  mysql_escape_custom($this->szBookingEmail)."'
                        WHERE
                            id = '".(int)$data['idPreferences']."'
                    ";
                }
                else
                { 
                    $query="
			INSERT INTO
                            ".__DBC_SCHEMATA_FORWARDER_PREFERENCES__."
                        (
                            szEmail,
                            idTransportMode,
                            idForwarder,
                            idForwarderContactRole,
                            idOriginCountry,
                            idDestinationCountry,  
                            iActive,
                            dtCreated,
                            dtModified,
                            szBookingEmail
                        )							
                        VALUES
                        (
                            '".mysql_escape_custom($this->szEmail)."',
                            '".(int)$this->idTransportMode."',
                            '".(int)$this->idForwarder."',
                            '".__QUOTES_AND_BOOKING_PROFILE_ID__."',
                            '".(int)$this->idOriginCountry."',
                            '".(int)$this->idDestinationCountry."', 
                            '1',
                            NOW(),
                            NOW(),
                           '".  mysql_escape_custom($this->szBookingEmail)."' 
                        )	
                    ";
                } 
		//echo $query;
                //die; 
		if($result=$this->exeSQL($query))
		{	 
                    return true;
		}
		else
		{
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
		} 
            }
        }
        
        function getAllQuotesAndBookingEmails($idForwarder=false,$idForwarderPreferences=false,$searchAry=array())
        { 
            if($idForwarder>0)
            {    
                $query_and .= " AND idForwarder = '".(int)$idForwarder."' " ;
            }
            if($idForwarderPreferences>0)
            {
                $query_and .= " AND id = '".(int)$idForwarderPreferences."' " ;
            } 
            if(!empty($searchAry))
            {
                if(!empty($searchAry['idForwarder']))
                {
                    $query_and .= " AND idForwarder = '".(int)$searchAry['idForwarder']."' " ;
                }
                if(!empty($searchAry['idTransportMode']))
                {
                    $query_and .= " AND idTransportMode = '".(int)$searchAry['idTransportMode']."' " ;
                }
                if(!empty($searchAry['arrTransportMode']))
                {
                    $szTransportModeStr = implode(",",$searchAry['arrTransportMode']);
                    if(!empty($szTransportModeStr))
                    {
                        $query_and .= " AND idTransportMode IN (".$szTransportModeStr.") " ;
                    }
                }
                if(!empty($searchAry['idOriginCountry']))
                {
                    $query_and .= " AND idOriginCountry = '".(int)$searchAry['idOriginCountry']."' " ;
                }
                if(!empty($searchAry['idDestinationCountry']))
                {
                    $query_and .= " AND idDestinationCountry = '".(int)$searchAry['idDestinationCountry']."' " ;
                }
            }
            
            $kConfig = new cConfig();
            $configLangArr=$kConfig->getConfigurationLanguageData('__TABLE_TRANSPORT_MODE__');
           
            $query="
                SELECT
                    id,
                    szEmail,
                    idForwarderContactRole,
                    idForwarder,
                    idTransportMode,
                    idOriginCountry,
                    idDestinationCountry,
                    iActive,
                    szBookingEmail
                FROM
                    ".__DBC_SCHEMATA_FORWARDER_PREFERENCES__."
                WHERE 
                    idForwarderContactRole = '".__QUOTES_AND_BOOKING_PROFILE_ID__."'
                AND
                    iActive = '1' 
                $query_and
                ORDER BY
                    idTransportMode ASC
            "; 	 
            //echo $query ;
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $ret_ary = array();
                    $ctr = 0; 
                    while($row=$this->getAssoc($result))
                    {   
                        if(!empty($configLangArr[$row['idTransportMode']]))
                        {
                            $configLangArr[$row['idTransportMode']]['szTransportMode']=$configLangArr[$row['idTransportMode']]['szShortName'];
                            $ret_ary[$ctr] = array_merge($configLangArr[$row['idTransportMode']],$row);
                        }else
                        {
                            $ret_ary[$ctr] = $row;
                        } 
                        $ctr++; 
                    } 
                    return $ret_ary;
                }
                else
                {
                        return array();
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
        }
         
        function deletePreferences($idForwarderPreferences)
        { 
            if($idForwarderPreferences>0)
            {  
                $query = "  
                    DELETE FROM
                        ".__DBC_SCHEMATA_FORWARDER_PREFERENCES__." 
                    WHERE
                        id = '".(int)$idForwarderPreferences."'
                "; 
		//echo $query;
                //die; 
		if($result=$this->exeSQL($query))
		{	 
                    return true;
		}
		else
		{
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
		} 
            }
        }
	function set_id( $value )
	{   
		$this->id = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "id", t($this->t_base.'fields/customer_id'), false, false, true );
	}
	function set_szFirstName( $value )
	{ 
		$this->szFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szFirstName", t($this->t_base.'fields/f_name'), false, 255, true );
	}
	function set_szLastName( $value )
	{
		$this->szLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLastName", t($this->t_base.'fields/l_name'), false, 255, true );
	}
	function set_szBookingEmail( $value )
	{
		$this->szBookingEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szBookingEmail", t($this->t_base.'fields/booking_email'), false, 255, true );
	}
        
        function set_szEmail( $value )
	{
		$this->szEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szEmail", t($this->t_base.'fields/email'), false, 255, true );
	}
        
	function set_szOldEmail( $value )
	{
		$this->szOldEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szOldEmail", t($this->t_base.'fields/email'), false, 255, true );
	}
	function set_szCountry( $value )
	{
		$this->szCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCountry", t($this->t_base.'fields/country'), false, 255, true );
	}
	function set_szPhoneNumber( $value )
	{
		$this->szPhoneNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPhoneNumber", t($this->t_base.'fields/office_phone_number'), false, false, true );
	}
	function set_szMobileNumber( $value )
	{
		$this->szMobileNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szMobileNumber", t($this->t_base.'fields/mobile_phone_number'), false, false, false );
	}
	function set_idForwarderRole( $value )
	{
		$this->idForwarderRole = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idForwarderRole", t($this->t_base.'fields/forwarder_role'), false, false, true );
	}
	function set_idCountry( $value )
	{
            $this->idCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idCountry", t($this->t_base.'fields/country'), false, false, true );
	}
        
        function set_idOriginCountry( $value )
	{
            $this->idOriginCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idOriginCountry", t($this->t_base.'fields/country'), false, false, true );
	}
        function set_idDestinationCountry( $value )
	{
            $this->idDestinationCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idDestinationCountry", t($this->t_base.'fields/country'), false, false, true );
	}
        function set_idTransportMode( $value )
	{
            $this->idTransportMode = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idTransportMode", t($this->t_base.'fields/transport_mode'), false, false, true );
	}
	function set_idForwarder( $value )
	{
		$this->idForwarder = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idForwarder", t($this->t_base.'fields/forwarder'), false, false, true );
	}
	function set_szPassword( $value )
	{
		$this->szPassword = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPassword", t($this->t_base.'fields/password'), false, 255, true );
	}
	function set_szRetypePassword( $value )
	{
		$this->szRetypePassword = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szReTypePassword", t($this->t_base.'fields/re_password'), false, 255, true );
	}
	function set_szOldPassword( $value )
	{
		$this->szOldPassword = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szOldPassword", t($this->t_base.'fields/current_password'), false, 255, true );
	}
	function set_controlPanelUrl( $value )
	{
		$this->szControlUrl = $this->validateInput( $value, __VLD_CASE_ALPHANUMERIC__,t($this->t_base.'fields/contrlURL'), '', 2, 255, true);
	}
	function set_szNonFrocePassword( $value )
	{
		$this->szNonFrocePassword = $this->validateInput( $value, __VLD_CASE_PASSWORD_NOFORCE_, "szPassword", t($this->t_base.'fields/password'), false, 255, true );
	}
	function set_szMultipleEmail( $value,$valdate )
	{
		$this->$valdate=$this->validateInput( $value, __VLD_CASE_EMAIL__, $value, t($this->t_base.'fields/email'), false, 255, true );
	}
	function set_iConfirmed( $value )
	{
		$this->iConfirmed = $this->validateInput( $value, __VLD_CASE_NUMERIC__,$value, '', 2, 255, true);
	}
	function set_szMessage( $value )
	{
		$this->szMessage = $this->validateInput( $value , __VLD_CASE_ANYTHING__ , "szMessage" , t($this->t_base.'fields/message'), false, 255, true );
	}
	function set_szContact( $value )
	{
		$this->szContact = $this->validateInput( $value , __VLD_CASE_EMAIL__ , "szContact" , t($this->t_base.'fields/email'), false, 255, true );
	} 	
	function set_szControlPanelUrl( $value )
	{
		$this->szControlPanelUrl = $this->validateInput( $value , __VLD_CASE_ANYTHING__ , "szControlPanelUrl" , t($this->t_base.'fields/conrol_panel'), false, 255, true );
	}
	function set_szConfirmPassword( $value )
	{
		$this->szConfirmPassword = $this->validateInput( $value , __VLD_CASE_ANYTHING__ , "szConfirmPassword" , t($this->t_base.'fields/confirm_new_assword'), false, 255, true );
	}
	function set_idForwarderContactRole( $value )
	{
		$this->idForwarderContactRole = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idForwarderContactRole", t($this->t_base.'fields/forwarder_role'), false, false, true );
	}
	function set_szControlBookingReference ( $value )
	{ 
		$this->szControlBookingReference = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szControlBookingReference", t($this->t_base.'fields/szControlBookingReference'), false, 2, true );
	}
	function set_iAvailableForManualQuotes ( $value )
	{ 
		$this->iAvailableForManualQuotes = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iAvailableForManualQuotes", t($this->t_base.'fields/szControlBookingReference'), false, false, false );
	}
        function set_iOffLineQuotes ( $value )
	{ 
            $this->iOffLineQuotes = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iOffLineQuotes", t($this->t_base.'fields/iOffLineQuotes'), false, false, false );
	} 
        function set_iDisplayQuickQuote ( $value )
	{ 
            $this->iDisplayQuickQuote = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iDisplayQuickQuote", t($this->t_base.'fields/quick_quote_link'), false, false, false );
	}  
	function set_fRefFee( $value )
	{ 
            $this->fRefFee = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "fRefFee", t($this->t_base.'fields/fRefFee'), false, 255, true );
	}
        function set_fReferralFee( $value, $idTransportMode )
	{ 
            $this->fReferralFee[$idTransportMode] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "fReferralFee_".$idTransportMode, t($this->t_base.'fields/fRefFee'), 0, false, true );
	}
	function set_controlBooking( $value )
	{   
		$this->controlBooking = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "controlBooking", t($this->t_base.'fields/controlBooking'), false, 5, true );
	}
	function set_controlPayment( $value )
	{   
		$this->controlPayment = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "controlPayment", t($this->t_base.'fields/controlPayment'), false, 5, true );
	}
	function set_controlRss( $value )
	{   
		$this->controlRss = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "controlRss", t($this->t_base.'fields/controlRss'), false, 5, true );
	}
	function set_szComment( $value )
	{   
		$this->szComment = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szComment", t($this->t_base.'fields/szComment'), false, false, false );
	}
	function set_dtValidTo( $value )
	{   
		$this->dtValidTo = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "dtValidTo", t($this->t_base.'fields/dtValidTo'), false, 10, true );
	}
	function set_iAutomaticPaymentInvoicing( $value )
	{   
            $this->iAutomaticPaymentInvoicing = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iAutomaticPaymentInvoicing", t($this->t_base.'fields/auto'), false, 1, true );
	}
        function set_iProfitType( $value )
	{   
            $this->iProfitType = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iProfitType", t($this->t_base.'fields/profit_typ'), false, 2, true );
	}
        
	function set_szCurrency( $value )
	{
            $this->szCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "szCurrency", t($this->t_base.'fields/szCurrency'), false,false, true );	
	}
        
        function set_szResponsibility( $value,$flag=true )
	{   
            $this->szResponsibility = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szResponsibility", t($this->t_base.'fields/responsibility'), false, false, $flag );
	}
        function set_iCreditDays( $value )
	{   
            $this->iCreditDays = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iCreditDays", t($this->t_base.'fields/iCreditDays'), 0, false, true );
	}
}
?>