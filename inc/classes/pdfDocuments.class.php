<?php
/**
 * This file is the container for partner api related functionality. 
 *
 * transportecaApi.class.php
 *
 * @copyright Copyright (C) 2016 Transporteca
 * @author Ajay
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_CLASSES__ . "/responseErrors.class.php");

class cPDFDocuments extends cDatabase
{   
    function __construct()
    {
        parent::__construct();
        return true;
    } 
    
    
    function getForwarderDetails($bookingDataArr)
    {
        if(!empty($bookingDataArr))
        {
            $forwarderDetails .= html_entities_flag($bookingDataArr['szForwarderRegistName'],$utf8Flag).'<br />';
            if($bookingDataArr['szForwarderAddress']!='')
                $forwarderDetails .= html_entities_flag($bookingDataArr['szForwarderAddress'],$utf8Flag).'<br />';
            if($bookingDataArr['szForwarderAddress2']!='')	
                $forwarderDetails .= html_entities_flag($bookingDataArr['szForwarderAddress2'],$utf8Flag).'<br />';
            if($bookingDataArr['szForwarderPostCode']!='')
                $forwarderDetails .= html_entities_flag($bookingDataArr['szForwarderPostCode'],$utf8Flag).'<br />';
            if($bookingDataArr['szForwarderCity']!='')	
                $forwarderDetails .= html_entities_flag($bookingDataArr['szForwarderCity'],$utf8Flag).'<br />';
            return $forwarderDetails;
        } 
    }
    
    function getForwarderEmails($idForwarder,$idRole=__PAYMENT_AND_BILLING_PROFILE_ID__)
    {
        if($idForwarder>0)
        {
            $bookingServiceEmailArr = $kForwarder->getForwarderCustomerServiceEmail($idForwarder,$idRole);
            $iTotalCounter=count($bookingServiceEmailArr);
            if(!empty($bookingServiceEmailArr))
            {
                foreach($bookingServiceEmailArr as $customerServiceEmailArys)
                {
                    if(empty($customerServiceEmailStr))
                    {
                        $customerServiceEmailStr = $customerServiceEmailArys ;
                        $customerServiceEmailLink = $customerServiceEmailArys;
                    }
                    else
                    {
                        //$customerServiceEmailLink .= ', '.$customerServiceEmailArys;

                        if($ctr>0 && $ctr==($iTotalCounter-1))
                        {
                            $customerServiceEmailStr .= ' and '.$customerServiceEmailArys;
                        }
                        else
                        {
                            $customerServiceEmailStr .= ', '.$customerServiceEmailArys;
                        }
                    }							
                    $ctr++;
                }
            }
            return $customerServiceEmailStr;
        } 
    }
    
    function getBookingServiceDes($bookingDataArr)
    {
        if(!empty($bookingDataArr))
        {
            $serviceString="Freight from"; 
            if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__)
            {
                $serviceString .=" door at origin to door at destination (DTD)";
                $pickAddressText="Pick-up address";
                $deliveryAddressText="Delivery";
                if($bookingDataArr['iShipperConsignee']==1){
                    $picktimetext="Expected pick-up";
                }else{
                $picktimetext="Expected pick-up time";
                }
                $deliverytimetext="Expected delivery";

            }
            else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTW__)
            {
                $serviceString .=" door at origin to warehouse at destination (DTW)";
                $pickAddressText="Pick-up address";
                $deliveryAddressText="Destination Warehouse";	
                $picktimetext="Expected pick-up time";
                $deliverytimetext="Expected cargo availability";	
            }
            else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_WTD__)
            {
                $serviceString .=" warehouse at origin to door at destination (WTD)";
                $pickAddressText="Origin Warehouse";
                $deliveryAddressText="Delivery";		
                $picktimetext="Cargo cut off";
                $deliverytimetext="Expected delivery";
            }
            else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_WTW__)
            {
                $serviceString .=" warehouse at origin to warehouse at destination (WTW)";
                $pickAddressText="Origin Warehouse";
                $deliveryAddressText="Destination Warehouse";
                $picktimetext="Cargo cut off";
                $deliverytimetext="Expected cargo availability";
            }
            else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTP__)
            {
                $serviceString .=" door at origin to port at destination (DTP)";
                $pickAddressText="Pick-up address";
                $deliveryAddressText="Destination Warehouse";	
                if($bookingDataArr['iShipperConsignee']==1){
                    $picktimetext="Expected pick-up";
                }else{
                $picktimetext="Expected pick-up time";
                } 
                $deliverytimetext="Expected cargo availability";	
            }
            else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_PTD__)
            {
                $serviceString .=" port at origin to door at destination (PTD)";
                $pickAddressText="Origin Warehouse";
                $deliveryAddressText="Delivery";		
                $picktimetext="Cargo cut off";
                $deliverytimetext="Expected delivery";
            }
            else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_PTP__)
            {
                $serviceString .=" port at origin to port at destination (PTP)";
                $pickAddressText="Origin Warehouse";
                $deliveryAddressText="Destination Warehouse";
                $picktimetext="Cargo cut off";
                $deliverytimetext="Expected cargo availability";
            }
            else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_PTW__)
            {
                $serviceString .=" port at origin to warehouse at destination (PTW)";
                $pickAddressText="Origin Warehouse";
                $deliveryAddressText="Destination Warehouse";
                $picktimetext="Cargo cut off";
                $deliverytimetext="Expected cargo availability";
            }
            else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_WTP__)
            {
                $serviceString .=" warehouse at origin to port at destination (WTP)";
                $pickAddressText="Origin Warehouse";
                $deliveryAddressText="Destination Warehouse";
                $picktimetext="Cargo cut off";
                $deliverytimetext="Expected cargo availability";
            }

            if($bookingDataArr['idTransportMode']==__BOOKING_TRANSPORT_MODE_ROAD__) //LTL
            {
                //$serviceString .=" door at origin to port at destination (DTP)";
                $picktimetext="Expected pick-up";
                //$deliveryAddressText="Destination Warehouse";	
                //$picktimetext="Expected pick-up";
                $deliverytimetext="Expected delivery";
            }

            if((int)$bookingDataArr['idServiceProvider']>0 && $bookingDataArr['iBookingType']==__BOOKING_TYPE_COURIER__)
            {
                $picktimetext="Expected pick-up";
                $deliverytimetext="Expected delivery"; 
                $deliveryAddressText="Delivery address";
            } 
            else
            {
                $fCargoWeight=$bookingDataArr['fCargoWeight']/1000;	 
            }

            if($bookingDataArr['iOriginCC']==1 && $bookingDataArr['iDestinationCC']==0)
            {
                $serviceString .=" and customs clearance at origin";
            }

            if($bookingDataArr['iDestinationCC']==2 && $bookingDataArr['iOriginCC']==0)
            {
                    $serviceString .=" and customs clearance at destination";
            }

            if($bookingDataArr['iDestinationCC']==2 && $bookingDataArr['iOriginCC']==1)
            {
                    $serviceString .=" and customs clearance at origin and destination";
            }
            if($bookingDataArr['iDestinationCC']==0 && $bookingDataArr['iOriginCC']==0)
            {
                    $serviceString .=". Customs clearance not included";
            }
            $serviceDescAry = array();
            $serviceDescAry['serviceString'] = $serviceString;  
            $serviceDescAry['pickAddressText'] = $pickAddressText;  
            $serviceDescAry['deliveryAddressText'] = $deliveryAddressText; 
            $serviceDescAry['picktimetext'] = $picktimetext; 
            $serviceDescAry['deliverytimetext'] = $deliverytimetext;  
            return $serviceDescAry;
        }
    }
    
    function getCustomerBillingAddress($bookingDataArr)
    {
        if(!empty($bookingDataArr))
        {
            $kUser = new cUser();
            $kConfig = new cConfig();
            $kUser->getUserDetails($bookingDataArr['idUser']); 

            $billcountry=$kConfig->getCountryName($bookingDataArr['szCustomerCountry']);
            $billingAddress .=html_entities_flag($bookingDataArr['szCustomerCompanyName'],$utf8Flag)."<br />";         	
            $billingAddress .=html_entities_flag($bookingDataArr['szCustomerAddress1'],$utf8Flag);

            $kConfig_new = new cConfig();
            $kConfig_new->loadCountry($bookingDataArr['idCustomerDialCode']);
            $iInternationDialCode = $kConfig_new->iInternationDialCode;

            if(!empty($bookingDataArr['szCustomerAddress2']))
                $billingAddress .=", ".html_entities_flag($bookingDataArr['szCustomerAddress2'],$utf8Flag);
            if(!empty($bookingDataArr['szCustomerAddress3']))
                $billingAddress .=", ".html_entities_flag($bookingDataArr['szCustomerAddress3'],$utf8Flag);			
            if(!empty($bookingDataArr['szCustomerPostCode']))
                $billingAddress .="<br />".html_entities_flag($bookingDataArr['szCustomerPostCode'],$utf8Flag);
            if(!empty($bookingDataArr['szCustomerCity']))
            {
                if(!empty($bookingDataArr['szCustomerPostCode']))
                    $billingAddress .=" ".html_entities_flag($bookingDataArr['szCustomerCity'],$utf8Flag);
                else
                    $billingAddress .=html_entities_flag($bookingDataArr['szCustomerCity'],$utf8Flag);
            }
            if(!empty($bookingDataArr['szCustomerState']))
                $billingAddress .="<br />".html_entities_flag($bookingDataArr['szCustomerState'],$utf8Flag);
            if(!empty($billcountry))
                $billingAddress .=", ".html_entities_flag($billcountry,$utf8Flag);
            else
                $billingAddress .="<br />"; 
            
            return $billingAddress;
        } 
    }
    
    function getShipperAddress($bookingDataArr)
    {
        if($bookingDataArr)
        {
            $shipperAddress = '';
            if(!empty($bookingDataArr['szShipperCompanyName']))
            {
                $shipperAddress.= html_entities_flag($bookingDataArr['szShipperCompanyName']);
            }  
            if(!empty($bookingDataArr['szShipperAddress_pickup']))
            {
                $shipperAddress.= ", ".html_entities_flag($bookingDataArr['szShipperAddress_pickup']);
            }
            if(!empty($bookingDataArr['szShipperAddress2']))
            {
                $shipperAddress .=", ".html_entities_flag($bookingDataArr['szShipperAddress2']) ;
            }
            if(!empty($bookingDataArr['szShipperAddress3']))
            {
                $shipperAddress .=", ".html_entities_flag($bookingDataArr['szShipperAddress3']);
            }
            if(!empty($bookingDataArr['szShipperPostCode']) || !empty($bookingDataArr['szShipperCity']))
            {
                if(!empty($bookingDataArr['szShipperPostCode']) && !empty($bookingDataArr['szShipperCity']))
                {
                    $shipperAddress.=", ".html_entities_flag($bookingDataArr['szShipperPostCode'])." ".html_entities_flag($bookingDataArr['szShipperCity'])."";
                }
                else if(!empty($bookingDataArr['szShipperPostCode']))
                {
                    $shipperAddress.=", ".html_entities_flag($bookingDataArr['szShipperPostCode']);
                }
                else if(!empty($bookingDataArr['szShipperCity']))
                {
                    $shipperAddress.=", ".html_entities_flag($bookingDataArr['szShipperCity']);
                }
            }
            return $shipperAddress;
        }
    }
    
    function getConsigneeAddress($bookingDataArr)
    {
        if(!empty($bookingDataArr))
        {
            $ConsigneeAddress = '';
            if(!empty($bookingDataArr['szConsigneeCompanyName']))
            {
                $ConsigneeAddress.= html_entities_flag($bookingDataArr['szConsigneeCompanyName']).", ";
            }  
            if(!empty($bookingDataArr['szConsigneeAddress']))
            {
                $ConsigneeAddress.= html_entities_flag($bookingDataArr['szConsigneeAddress']);
            }
            if(!empty($bookingDataArr['szConsigneeAddress2']))
            {
                $ConsigneeAddress .=", ".html_entities_flag($bookingDataArr['szConsigneeAddress2']);
            }
            if(!empty($bookingDataArr['szConsigneeAddress3']))
            {
                $ConsigneeAddress .=", ".html_entities_flag($bookingDataArr['szConsigneeAddress3']);
            }
            if(!empty($bookingDataArr['szConsigneePostCode']) || !empty($bookingDataArr['szConsigneeCity']))
            {
                $ConsigneeAddress.=", ".html_entities_flag($bookingDataArr['szConsigneePostCode'])." ".html_entities_flag($bookingDataArr['szConsigneeCity'])."";
            } 
            return $ConsigneeAddress;
        }
    }
    
    function getPriceBreakTopHeading($bookingDataArr,$priceText='&nbsp;',$td_style='')
    {
       
        $szPricingDetailsString .= '  
                <tr>
                    <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;"><strong>'.$priceText.'</strong></td>
                    <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;"><strong>Rate</strong></td>
                    <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;"><strong>ROE</strong></td>
                    <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;"><strong>Value</strong></td>
                </tr>
        ' ;                
        
        return $szPricingDetailsString;
    }
}

?>