<?php
/**
 * This file is the container for partner api related functionality. 
 *
 * transportecaApi.class.php
 *
 * @copyright Copyright (C) 2016 Transporteca
 * @author Ajay
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_CLASSES__ . "/responseErrors.class.php");

class cTransportecaApi extends cDatabase
{   
    public static $idPartnerApiEventApi=false;
    function __construct()
    {
        parent::__construct(); 
        return true;
    }
    
    public function callTransportecaAPI($clientApiHeaderAry,$requestInputAry,$szMethod)
    {      
        $kApi = new cApi(); 
        $requestInputAry = $kApi->formatApiRequestParams($requestInputAry);
      
        $kPartner = new cPartner();
        $idPartnerApiEventApi = $kPartner->addPartnerApiEventLogs($requestInputAry,$clientApiHeaderAry);
        $szReferenceToken = $requestInputAry['token']; 
        cApi::$idPartnerApiEventApi = $idPartnerApiEventApi;
          
        if($kPartner->validatePartnerKey($clientApiHeaderAry,$idPartnerApiEventApi,$szMethod,$szReferenceToken))
        {        
            if($szMethod=='USER_LOGIN')
            { 
                /*
                * callig API to validate input and log user in to transporteca
                */ 
                $kPartner->userLogin($requestInputAry);    
            } 
            else if($szMethod=='USER_SIGNUP')
            { 
                /*
                * callig API to validate input and log user in to transporteca
                */ 
                $kPartner->userSignup($requestInputAry);    
            } 
            else if($szMethod=='USER_PROFILE_DETAILS')
            {
                /*
                * callig API to fetch customer details from transporteca
                */ 
                $kPartner->getUserProfileDetails($requestInputAry);    
            }
            else if($szMethod=='CUSTOMER_CHANGE_PASSWORD')
            {
                /*
                * callig API to change customer's account password @transporteca
                */ 
                $kPartner->changePassword($requestInputAry);    
            }
            else if($szMethod=='CUSTOMER_FORGOT_PASSWORD')
            {
                /*
                * callig API to change customer's account password @transporteca
                */ 
                $kPartner->forgotPassword($requestInputAry);    
            }
            else if($szMethod=='USER_BOOKING_HISTORY')
            { 
                /*
                * callig API to validate input and all bookings user in to transporteca
                */ 
                $kPartner->userBookingHistory($requestInputAry);    
            }
            else if($szMethod=='USER_ACTIVE_BOOKING')
            {
                /*
                * callig API to validate input and all active bookings user in to transporteca
                */ 
                $kPartner->userBookingHistory($requestInputAry,'active'); 
            }
            else if($szMethod=='USER_ARCHIVED_BOOKING')
            {
                /*
                * callig API to validate input and all archived bookings of user on transporteca
                */ 
                $kPartner->userBookingHistory($requestInputAry,'archive'); 
            }
            else if($szMethod=='USER_DRAFT_BOOKING')
            {
                /*
                * callig API to validate input and all draft bookings of user on transporteca
                */ 
                $kPartner->userBookingHistory($requestInputAry,'draft'); 
            }
            else if($szMethod=='USER_HOLD_BOOKING')
            {
                /*
                * callig API to validate input and all hold bookings of user on transporteca
                */ 
                $kPartner->userBookingHistory($requestInputAry,'hold'); 
            }
            else if($szMethod=='DELETE_CUSTOMER_ACCOUNT')
            {
                /*
                * callig API to validate input and delete user account of transporteca
                */ 
                $kPartner->deleteCustomerAccount($requestInputAry); 
            }
            else if($szMethod=='UPDATE_USER_PROFILE')
            {
                /*
                * callig API to validate input and update user account of transporteca
                */ 
                $kPartner->updateCustomerAccount($requestInputAry); 
            }
            else if($szMethod=='CUSTOMER_REGISTERED_SHIPPER')
            {
                /*
                * callig API to validate input and get user registered shipper on transporteca
                */ 
                $kPartner->getAllRegisterdShipperConsigneeList($requestInputAry,'ship'); 
            }
            else if($szMethod=='CUSTOMER_REGISTERED_CONSIGNEES')
            {
                /*
                * callig API to validate input and get user registered consignees on transporteca
                */ 
                $kPartner->getAllRegisterdShipperConsigneeList($requestInputAry,'con'); 
            }
            else if($szMethod=='CUSTOMER_REGISTERED_SHIPPER_DETAIL')
            {
                /*
                * callig API to validate input and get user registered shipper details on transporteca
                */ 
                $kPartner->getRegisterdShipperConsigneeDetails($requestInputAry,'ship'); 
            }
            else if($szMethod=='CUSTOMER_REGISTERED_CONSIGNEE_DETAIL')
            {
                /*
                * callig API to validate input and get user registered consignees details on transporteca
                */ 
                $kPartner->getRegisterdShipperConsigneeDetails($requestInputAry,'con'); 
            }
            else if($szMethod=='CUSTOMER_UPDATE_CONSIGNEE_DETAIL')
            {
                /*
                * callig API to validate input and get user update consignees details on transporteca
                */ 
                $kPartner->updateShipperConsigneeDetails($requestInputAry,'con','UPDATE'); 
            }
            else if($szMethod=='CUSTOMER_UPDATE_SHIPPER_DETAIL')
            {
                /*
                * callig API to validate input and get user update shipper details on transporteca
                */ 
                $kPartner->updateShipperConsigneeDetails($requestInputAry,'ship','UPDATE'); 
            }
            else if($szMethod=='CUSTOMER_ADD_CONSIGNEE_DETAIL')
            {
                /*
                * callig API to validate input and get user update consignees details on transporteca
                */ 
                $kPartner->updateShipperConsigneeDetails($requestInputAry,'con','UPDATE'); 
            }
            else if($szMethod=='FORWARDER_RATING')
            {
                /*
                * callig API to validate input and get user update consignees details on transporteca
                */ 
                $kPartner->getForwarderRating($requestInputAry); 
            } 
            else if($szMethod=='GET_GOOD_INSURANCE_VALUE')
            { 
                /*
                * callig API to validate input and get insurance value
                */ 
                $kPartner->getInsuranceValue($requestInputAry); 
            } 
            else if($szMethod=='CUSTOMER_EVENTS')
            {
                /*
                * callig API to process customer events
                */ 
                $kPartner->customerEvents($requestInputAry); 
            }
            else if($szMethod=='GET_STANDARD_CARGO_LIST')
            { 
                /*
                * callig API to validate input and get Standard Cargo List value
                */ 
                $kPartner->getStandardCargo($requestInputAry); 
            }  
            else if($szMethod=='GET_QUOTE_DETAILS')
            { 
                /*
                * Callig API to fetch details of a quotes
                */ 
                $kPartner->getQuoteDetails($requestInputAry); 
            } 
            else if($szMethod=='GET_LABEL_DETAILS')
            { 
                /*
                * Callig API to fetch details of a quotes
                */ 
                $kPartner->getLabelDetails($requestInputAry); 
            } 
            else if($szMethod=='CONFIRM_LABEL_DOWNLOAD')
            { 
                /*
                * Callig API to fetch details of a quotes
                */ 
                $kPartner->getConfirmLabelDownload($requestInputAry); 
            }
            else if($szMethod=='SUBMIT_NPS')
            { 
                /*
                * Callig API to fetch details of a quotes
                */ 
                $kPartner->submitNPS($requestInputAry); 
            } 
            else if($szMethod=='NEWS_LETTER_SIGNUP')
            {
                /*
                * Callig API For News Letter Signup
                */ 
                $kPartner->newsLetterSignup($requestInputAry); 
            }
            
            
            if(cPartner::$szGlobalErrorType=='VALIDATION')
            { 
                $szResponseSerialized = cResponseError::getSerializedMessages();
                $kPartner->updateApiEventLogResponse($szResponseSerialized,404,$idPartnerApiEventApi);
                $szResponseJson = cResponseError::getMessages();
                ob_end_clean();
                header( 'HTTP/1.1 200 OK' );
                echo $szResponseJson; 
                die;
            }
            else if(cPartner::$szGlobalErrorType=='NOTIFICATION')
            {
                $szResponseSerialized = cResponseError::getSerializedMessages();
                $kPartner->updateApiEventLogResponse($szResponseSerialized,200,$idPartnerApiEventApi);
                $szResponseJson = cResponseError::getMessages(); 
                ob_end_clean();
                header( 'HTTP/1.1 200 OK' );
                echo $szResponseJson;
                die;
            }
            else if(cPartner::$szGlobalErrorType=='SUCCESS')
            { 
                $szResponseSerialized = cResponseError::getSerializedMessages();
                $kPartner->updateApiEventLogResponse($szResponseSerialized,200,$idPartnerApiEventApi);
                $szResponseJson = cResponseError::getMessages(); 
                ob_end_clean();
                header( 'HTTP/1.1 200 OK' );
                echo $szResponseJson; 
                die;
            }
        }
        else
        {  
            $szResponseSerialized = cResponseError::getSerializedMessages();
            $kPartner->updateApiEventLogResponse($szResponseSerialized,404,$idPartnerApiEventApi);
            $szResponseJson = cResponseError::getMessages();
            ob_end_clean();
            header( 'HTTP/1.1 200 OK' );
            echo $szResponseJson;
            die;
        } 
    }  
    
    public function isTokenValid($szToken)
    {
        if(!empty($szToken))
        {
            $query="
                SELECT
                    id,
                    idCustomer
                FROM
                    ".__DBC_SCHEMATA_CUSTOMER_API_LOGIN__."
                WHERE
                    szToken = '".mysql_escape_custom($szToken)."'
                AND
                    dtExpired >= NOW()
                AND
                    iActive='1'
            "; 
           // echo "<br>".$query; 
            if( ( $result = $this->exeSQL( $query ) ) )
            {
        	// Check if the user exists
                if( $this->getRowCnt() > 0 )
                { 
                    $row = $this->getAssoc($result); 
                    $this->idApiCustomer = $row['idCustomer'];
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
}
?>