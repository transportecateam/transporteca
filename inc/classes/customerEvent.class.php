<?php
/**
 * This file is the container for partner api related functionality. 
 *
 * api.class.php
 *
 * @copyright Copyright (C) 2017 Transporteca
 * @author Ajay
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_CLASSES__ . "/responseErrors.class.php");

class cCustomerEevent extends cDatabase
{     
    /**
    * class constructor for php5
    *
    * @return bool
    */
    var $iErrorFlag = false;
    var $error_detail_ary = array();
    function __construct()
    {
        parent::__construct(); 
        return true;
    }
    
    public function priceRequestContact($data)
    { 
        if(!empty($data))
        { 
            /*
            * First thing first, we are fetching booking ID from tblbookings on the basis of szReferenceToken 
            * If we finds matching record in tblbookings then we go ahead and update pricing and all for the booking 
            * If we don't find matching record in tblbooking then we just return False and this process should be stopped at this point.
            */  
            $szReferenceToken = $data['szPriceToken'];
            $szCustomerToken = $data['szCustomerToken']; 
             
            cPartner::$szGlobalToken = $szReferenceToken;
            
            $kPartner = new cPartner();
            $kServices = new cServices();
            $kRegisterShipCon = new cRegisterShipCon();
            
            /*
            * Building post search data from 'Price' API calls
            */
            
            $priceRequestParamAry = array();
            $priceRequestParamAry = $kPartner->getQuickQuoteRequestData('price',false,$szReferenceToken);
               
            $szRequestType = "Price"; 
            
            if(empty($priceRequestParamAry))
            { 
                $errorFlag = true;
                cPartner::$szDeveloperNotes .= ">. We did not find any matching row in tblbookings for the Token: ".$szReferenceToken.". Stopped! ".PHP_EOL;
                $this->buildErrorDetailAry('szPriceToken','INVALID');  
            }  
            //Calculate idServiceTerms by using szServiceTerm
            
            $szServiceTerm = $priceRequestParamAry['szServiceTerm'];
            if(empty($szServiceTerm) || $szServiceTerm=='ALL')
            {
                $szServiceTerm =__SERVICE_SHORT_TERMS_EXW__;
            }
            
            $kConfig = new cConfig();
            $serviceTermsAry = array();
            $serviceTermsAry = $kConfig->getAllServiceTerms(false,$szServiceTerm);

            if(!empty($serviceTermsAry))
            {
                $idServiceTerms = $serviceTermsAry[0]['id'];
            }
            
            $kConfig = new cConfig();
            $kConfig->loadCountry($priceRequestParamAry['szShipperCountry']);
            $szOriginCountryName = $kConfig->szCountryName;
            
            $kConfig = new cConfig();
            $kConfig->loadCountry($priceRequestParamAry['szConsigneeCountry']);
            $szDestinationCountryName = $kConfig->szCountryName;
            
            if($priceRequestParamAry['iDonotKnowShipperPostcode'])
            {
                $priceRequestParamAry['szShipperPostCode'] = "";
            }
            if($priceRequestParamAry['iDonotKnowConsigneePostcode'])
            {
                $priceRequestParamAry['szConsigneePostCode'] = "";
            }
            
            $szOriginCountryStr = trim($priceRequestParamAry['szShipperPostCode']." ".$priceRequestParamAry['szShipperCity']." ".$szOriginCountryName); 
            $szDestinationCountryStr = trim($priceRequestParamAry['szConsigneePostCode']." ".$priceRequestParamAry['szConsigneeCity']." ".$szDestinationCountryName); 
            
            $postSearchAry = $priceRequestParamAry;
            $postSearchAry['dtTimingDate'] = $priceRequestParamAry['dtShipping'];
            $postSearchAry['szOriginPostCode'] = $priceRequestParamAry['szShipperPostCode'];
            $postSearchAry['szOriginCity'] = $priceRequestParamAry['szShipperCity'];
            $postSearchAry['szOriginCountry'] = $szOriginCountryStr;
            $postSearchAry['idOriginCountry'] = $priceRequestParamAry['szShipperCountry'];
            $postSearchAry['fOriginLatitude'] = $priceRequestParamAry['szShipperLattitude'];
            $postSearchAry['fOriginLongitude'] = $priceRequestParamAry['szShipperLongitude'];

            $postSearchAry['idDestinationCountry'] = $priceRequestParamAry['szConsigneeCountry'];
            $postSearchAry['szDestinationCountry'] = $szDestinationCountryStr;
            $postSearchAry['szDestinationPostCode'] = $priceRequestParamAry['szConsigneePostCode'];
            $postSearchAry['szDestinationCity'] = $priceRequestParamAry['szConsigneeCity'];
            $postSearchAry['fDestinationLongitude'] = $priceRequestParamAry['szConsigneeLattitude'];
            $postSearchAry['fDestinationLatitude'] = $priceRequestParamAry['szConsigneeLongitude']; 
            $postSearchAry['iBookingQuoteShipmentType'] = $priceRequestParamAry['iShipmentType'];  
            $postSearchAry['idServiceTerms'] = $idServiceTerms; 
            if(!empty($data['szCargoType']))
            {
                $postSearchAry['szCargoType'] = $data['szCargoType'];
            } 
            cPartner::$GlobalpostSearchAry = $postSearchAry;
            cPartner::$szRequestType = $szRequestType;  
             
            $idCustomer = false;
            $szCustomerCompnay = "";
            $iPrivateShipping = "";  
            $iBookingLanguage = $postSearchAry['iBookingLanguage'];
             
            if(!empty($szCustomerToken))
            {
                /*
                * If we receive szCustomerToken in API request then we check if customer token is valid then using that customer for the booking  
                */
                $kBooking = new cBooking();
                $kTransportecaApi = new cTransportecaApi();
                $kTransportecaApi->isTokenValid($szCustomerToken); 
                $idCustomer = $kTransportecaApi->idApiCustomer;

                if($idCustomer>0)
                { 
                    cPartner::$idPartnerCustomer = $idCustomer;    
                     
                    $updateUserExpiryArr=array();
                    $updateUserExpiryArr['szToken'] = $szCustomerToken ;
                    $updateUserExpiryArr['idCustomer'] = $idCustomer;
                    $kUser = new cUser();
                    $kUser->updateCustomerExpiryTime($updateUserExpiryArr);  
                } 
            }  
            
            if($idCustomer<=0) 
            {
                /*
                * If we don't find  customer by token then we checks if customer is exists by szBillingEmail
                */
                $iAddNewUser = false;
                $kUser = new cUser();
                if($kUser->isUserEmailExist($data['szBillingEmail']))
                {  
                    $idCustomer = $kUser->idIncompleteUser ;
                } 
                else
                {
                    /*
                     * If we don't find customer by either szCustomerToken and szBillingEmail then creates a new customer in the system
                     */
                    $iAddNewUser = true;
                }
            }
            
            if($idCustomer>0)
            {
                $kUserNew = new cUser();
                if($kUserNew->isUserEmailExist($data['szBillingEmail'],$idCustomer))
                {  
                    /*
                    *  checking other user exists with the same email.
                    */
                    if((int)$kUserNew->idUserSignupApi==0)
                    {
                        $errorFlag = true;
                        $this->buildErrorDetailAry('szEmail','ALREADY_EXISTS');
                    }
                } 
            }
            
            if($errorFlag)
            {
                $this->iErrorFlag = true;
                cPartner::$arrErrorMessageAry = $this->error_detail_ary; 
                return false;
            } 
            
            $bookingQuotesAry['szCargoType'] = $data['szCargoType'];   
            
            $szShipperCompanyName = $data['szBillingCompanyName'];
            $szShipperFirstName = $data['szBillingFirstName'];
            $szShipperLastName = $data['szBillingLastName'];  
            $szShipperEmail = $data['szBillingEmail'];
            $szShipperPhone = $data['iBillingPhoneNumber'];
            $idShipperDialCode = $data['iBillingCountryDialCode']; 
            $iPrivateShipping = $data['iPrivate'];   
            
            if($idShipperDialCode==$postSearchAry['idOriginCountry'])
            {
                $szBillingPostcode = $postSearchAry['szOriginPostCode'];
                $szBillingCity = $postSearchAry['szOriginCity'];
                $szBillingAddress = $postSearchAry['szOriginAddress'];
                $iShipperConsignee = 1;
            }
            else if($idShipperDialCode==$postSearchAry['idDestinationCountry'])
            {
                $szBillingPostcode = $postSearchAry['szDestinationPostCode'];
                $szBillingCity = $postSearchAry['szDestinationCity'];
                $szBillingAddress = $postSearchAry['szDestinationAddress'];
                $iShipperConsignee = 2;
            } 
            else
            {
                $iShipperConsignee = 3;
            }
             
            if($idCustomer>0)
            {
                $kUser = new cUser();
                $kUser->getUserDetails($idCustomer);  
                if(empty($szShipperCompanyName))
                {
                    $szShipperCompanyName = $kUser->szCompanyName;
                }
                $updateUserDetailsAry = array();
                $updateUserDetailsAry['szCompanyName'] = $szShipperCompanyName;
                $updateUserDetailsAry['szFirstName'] = $szShipperFirstName; 
                $updateUserDetailsAry['szLastName'] = $szShipperLastName;  
                $updateUserDetailsAry['szPhone'] = $szShipperPhone;
                $updateUserDetailsAry['idInternationalDialCode'] = $idShipperDialCode ;  
                $updateUserDetailsAry['iLanguage'] = $iBookingLanguage;
                $updateUserDetailsAry['iPrivate'] = $iPrivateShipping;

                if(empty($kUser->szCountry))
                {
                    $updateUserDetailsAry['idCountry'] = $idShipperDialCode ;  
                }
                if(empty($kUser->szAddress1))
                {
                    $updateUserDetailsAry['szAddress'] = $szBillingAddress ;
                }
                if(empty($kUser->szPostCode))
                {
                    $updateUserDetailsAry['szPostCode'] = $szBillingPostcode;
                }
                if(empty($kUser->szCity))
                {
                    $updateUserDetailsAry['szCity'] = $szBillingCity ; 
                }  
                if($kUser->iConfirmed!=1)
                {
                    $updateUserDetailsAry['szEmail'] = $szShipperEmail;
                }  
                if(!empty($updateUserDetailsAry))
                {
                    $update_user_query = '';
                    foreach($updateUserDetailsAry as $key=>$value)
                    {
                        $update_user_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                } 
                $update_user_query = rtrim($update_user_query,",");   
                if($kRegisterShipCon->updateUserDetailsByQuotes($update_user_query,$idCustomer))
                { 
                    //TO DO
                } 
                else
                {
                    $errorFlag = true;
                    cPartner::$szDeveloperNotes .= ">. Could not update customer details on booking. Stopped! ".PHP_EOL;
                    $this->buildErrorDetailAry('szCommunication','INVALID');
                }
            }
            else
            {
                $szPassword = mt_rand(0,99)."".$kRegisterShipCon->generateUniqueToken(8); 

                //User is not loogedin then we create a new profile
                $addUserAry = array();
                $addUserAry['szFirstName'] = $szShipperFirstName ;
                $addUserAry['szLastName'] = $szShipperLastName ;
                $addUserAry['szEmail'] = $szShipperEmail ;
                $addUserAry['szPassword'] = $szPassword ;
                $addUserAry['szConfirmPassword'] = $szPassword ; 
                $addUserAry['szPhoneNumberUpdate'] = $szShipperPhone;
                $addUserAry['szCompanyName'] = $szShipperCompanyName ; 
                $addUserAry['szCurrency'] = $bookingDataAry['idCurrency']; 
                $addUserAry['iConfirmed'] = 0; 
                $addUserAry['iFirstTimePassword'] = 1;   
                $addUserAry['szCountry'] = $idShipperDialCode ;
                $addUserAry['idInternationalDialCode'] = $idShipperDialCode ;  
                $addUserAry['szAddress1'] = $szBillingAddress ;
                $addUserAry['szPostCode'] = $szBillingPostcode;
                $addUserAry['szCity'] = $szBillingCity ;  
                $addUserAry['iPrivate'] = $iPrivateShipping;                       
                $addUserAry['iLanguage'] = $iBookingLanguage;

                if($kUser->createAccount($addUserAry,'BOOKING_QUOTE_PROCESS',$bApiLogin))
                { 
                    $idCustomer = $kUser->id ;    
                }  
                else
                {
                    $errorFlag = true;
                    cPartner::$szDeveloperNotes .= ">. Could not update customer details on booking. Stopped! ".PHP_EOL;
                    $this->buildErrorDetailAry('szCommunication','INVALID');
                }
            } 
            if($errorFlag)
            {
                $this->iErrorFlag = true;
                cPartner::$arrErrorMessageAry = $this->error_detail_ary; 
                return false;
            }
            cPartner::$szCalledFromAPIType = "CUSTOMER_EVENT";
            
            $idPartner = cPartner::$idGlobalPartner; 
            $szApiRequestMode = cPartner::$szApiRequestMode;
            
            $serviceListAry = array();
            $serviceListAry = $kPartner->getServiceResponseDetails($idPartner,$szReferenceToken,false,true);
            $iNumServiceFound = count($serviceListAry); 
            
            if($iNumServiceFound>0)
            {
                cPartner::$bNoserviceFound = false;
            }
            else
            {
                cPartner::$bNoserviceFound = 1;
            } 
            $this->idCustomerEvent = $idCustomer;
            $kServices = new cServices();
            $kServices->addAutomaticBookingDetailsByCustomerID($szReferenceToken,$postSearchAry,$idCustomer);
            return true; 
        }
    }
    
    public function updateCustomerDetailsToBooking($data,$idBooking=false)
    {
        if(!empty($data))
        {
            $iAddNewUser = false;
            $kUser = new cUser();
            $kBooking = new cBooking();
            $kRegisterShipCon = new cRegisterShipCon();
            
            if($kUser->isUserEmailExist($data['szBillingEmail']))
            {  
                $idCustomer = $kUser->idIncompleteUser ;
            } 
            if($idBooking>0)
            {
                $bookingDataAry = array();
                $bookingDataAry = $kBooking->getExtendedBookingDetails($idBooking); 
                $iBookingLanguage = $bookingDataAry['iBookingLanguage'];
                $iPrivateShipping = $bookingDataAry['iPrivateShipping'];
                
                $idBillingCountry = $data['idBillingCountry']; 
                if($idBillingCountry==$bookingDataAry['idOriginCountry'])
                {
                    $szBillingAddress = $bookingDataAry['szShipperAddress'];
                    $szBillingPostcode = $bookingDataAry['szShipperPostCode'];
                    $szBillingCity = $bookingDataAry['szShipperCity']; 
                    $iShipperConsignee = 1;
                }
                else if($idBillingCountry==$bookingDataAry['idDestinationCountry'])
                {
                    $szBillingAddress = $bookingDataAry['szConsigneeAddress'];
                    $szBillingPostcode = $bookingDataAry['szConsigneePostCode'];
                    $szBillingCity = $bookingDataAry['szConsigneeCity']; 
                    $iShipperConsignee = 2;
                } 
                else
                {
                    if(!empty($data['szBillingAddress']) ||  !empty($data['szBillingPostcode']) || !empty($data['szBillingCity']))
                    {
                        //So if we gets anyof the 3 value as parameter then we updates billing details from there other wise we use same details what we have in booking 
                        $szBillingAddress = $data['szBillingAddress'];
                        $szBillingPostcode = $data['szBillingPostcode'];
                        $szBillingCity = $data['szBillingCity'];
                    }
                    else
                    {
                        $szBillingAddress = $bookingDataAry['szBillingAddress'];
                        $szBillingPostcode = $bookingDataAry['szBillingPostcode'];
                        $szBillingCity = $bookingDataAry['szBillingCity'];
                    } 
                    $iShipperConsignee = 3;
                }
            } 
            else
            {
                $iBookingLanguage = $data['iBookingLanguage'];
                $iPrivateShipping = $data['iPrivate'];
                
                $szBillingAddress = $data['szBillingAddress']; 
                $szBillingPostcode = $data['szBillingPostcode']; 
                $szBillingCity = $data['szBillingCity']; 
                
                if(strtolower($data['iShipperConsigneeType'])=='consignee')
                {
                    $iShipperConsignee=2;
                }
                else if(strtolower($data['iShipperConsigneeType'])=='shipper')
                {
                    $iShipperConsignee=1;
                }
                else
                {
                    $iShipperConsignee=3;
                }
            }
            
            $szShipperFirstName = $data['szBillingFirstName'];
            $szShipperLastName = $data['szBillingLastName']; 
            $szShipperCompanyName = $data['szBillingCompanyName'];
            $szShipperEmail = $data['szBillingEmail'];
            $szShipperPhone = $data['iBillingPhoneNumber'];
            $idShipperDialCode = $data['iBillingCountryDialCode']; 
            $idBillingCountry = $data['idBillingCountry']; 
             
            if($idCustomer>0)
            {
                $kUser->getUserDetails($idCustomer);   
                  
                $updateUserDetailsAry = array();
                $updateUserDetailsAry['szCompanyName'] = $szShipperCompanyName;
                $updateUserDetailsAry['szFirstName'] = $szShipperFirstName; 
                $updateUserDetailsAry['szLastName'] = $szShipperLastName;  
                $updateUserDetailsAry['szPhone'] = $szShipperPhone;
                $updateUserDetailsAry['idInternationalDialCode'] = $idShipperDialCode ; 
                $updateUserDetailsAry['iPrivate'] = $iPrivateShipping;
                $updateUserDetailsAry['iLanguage'] = $iBookingLanguage;

                if($idBillingCountry>0)
                {
                    $updateUserDetailsAry['idCountry'] = $idBillingCountry ;  
                }
                if(!empty($szBillingAddress))
                {
                    $updateUserDetailsAry['szAddress'] = $szBillingAddress ;
                }
                if(!empty($szBillingPostcode))
                {
                    $updateUserDetailsAry['szPostCode'] = $szBillingPostcode;
                }
                if(!empty($szBillingCity))
                {
                    $updateUserDetailsAry['szCity'] = $szBillingCity ; 
                }  
                if($kUser->iConfirmed!=1)
                {
                    $updateUserDetailsAry['szEmail'] = $szShipperEmail;
                } 
                if(!empty($updateUserDetailsAry))
                {
                    $update_user_query = '';
                    foreach($updateUserDetailsAry as $key=>$value)
                    {
                        $update_user_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                } 
                $update_user_query = rtrim($update_user_query,",");   
                if($kRegisterShipCon->updateUserDetailsByQuotes($update_user_query,$idCustomer))
                { 
                    //TO DO
                } 
            }
            else
            {
                $szPassword = mt_rand(0,99)."".$kRegisterShipCon->generateUniqueToken(8); 

                //User is not loogedin then we create a new profile
                $addUserAry = array();
                $addUserAry['szFirstName'] = $szShipperFirstName ;
                $addUserAry['szLastName'] = $szShipperLastName ;
                $addUserAry['szEmail'] = $szShipperEmail ;
                $addUserAry['szPassword'] = $szPassword ;
                $addUserAry['szConfirmPassword'] = $szPassword ; 
                $addUserAry['szPhoneNumberUpdate'] = $szShipperPhone;
                $addUserAry['szCompanyName'] = $szShipperCompanyName ; 
                $addUserAry['szCurrency'] = $bookingDataAry['idCurrency']; 
                $addUserAry['iConfirmed'] = 0; 
                $addUserAry['iFirstTimePassword'] = 1;   
                $addUserAry['szCountry'] = $idShipperDialCode ;
                $addUserAry['idInternationalDialCode'] = $idShipperDialCode ;  
                $addUserAry['szAddress1'] = $szBillingAddress ;
                $addUserAry['szPostCode'] = $szBillingPostcode;
                $addUserAry['szCity'] = $szBillingCity ;  
                $addUserAry['iPrivate'] = $iPrivateShipping;                       
                $addUserAry['iLanguage'] = $iBookingLanguage;
               
                if($kUser->createAccount($addUserAry,'BOOKING_QUOTE_PROCESS',$bApiLogin))
                { 
                    $idCustomer = $kUser->id ;    
                }  
            }
            
            $this->idCustomer = $idCustomer;
            
            if($idCustomer>0 && $idBooking>0)
            {
                /*
                 * We only updates booking if we already have 
                 */
                $kUser = new cUser();
                $kUser->getUserDetails($idCustomer);
                
                $bookingAry = array();
                $bookingAry['idUser'] = $kUser->id ;				
                $bookingAry['szFirstName'] = $kUser->szFirstName ;
                $bookingAry['szLastName'] = $kUser->szLastName ;
                $bookingAry['szEmail'] = $kUser->szEmail ;
                $bookingAry['szCustomerAddress1'] = $kUser->szAddress1;
                $bookingAry['szCustomerAddress2'] = $kUser->szAddress2;
                $bookingAry['szCustomerAddress3'] = $kUser->szAddress3;
                $bookingAry['szCustomerPostCode'] = $kUser->szPostcode;
                $bookingAry['szCustomerCity'] = $kUser->szCity;
                $bookingAry['szCustomerCountry'] = $kUser->szCountry;
                $bookingAry['szCustomerState'] = $kUser->szState;
                $bookingAry['szCustomerPhoneNumber'] = $kUser->szPhoneNumber;
                $bookingAry['szCustomerCompanyName'] = $kUser->szCompanyName;	
                $bookingAry['szCustomerCompanyRegNo'] = $kUser->szCompanyRegNo;
                $bookingAry['idCustomerDialCode'] = $kUser->idInternationalDialCode;
                $bookingAry['iPrivateShipping'] = $kUser->iPrivate; 
                $bookingAry['iShipperConsignee'] = $iShipperConsignee; 
                
                $update_query = ''; 
                if(!empty($bookingAry))
                {
                    foreach($bookingAry as $key=>$value)
                    {
                        $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                }
                $update_query = rtrim($update_query,","); 
                if($kBooking->updateDraftBooking($update_query,$idBooking))
                { 
                    return true;
                }
                else
                {
                    return false;
                }
            } 
            else
            {
                return false;
            } 
        }
    }
    
    function buildGetQuoteApiResponse($idBooking,$idBookingQuotePricing)
    {
        if($idBooking>0 && $idBookingQuotePricing>0)
        {
            $kBooking = new cBooking();
            $bookingDataAry = array();
            $bookingDataAry = $kBooking->getExtendedBookingDetails($idBooking); 
            $iBookingLanguage = $bookingDataAry['iBookingLanguage']; 
            
            $dtTransportStartDay = "";
            $dtTransportStartMonth = "";
            $dtTransportStartYear = "";
            
            $dtTransportFinishDay = "";
            $dtTransportFinishMonth = "";
            $dtTransportFinishYear = "";
            $szTransportStartCity = "";
            $iScheduled = 0;
            $szTransportFinishDate='';
            $szTransportStartDate='';
            if(!empty($bookingDataAry['dtCutOff']) && $bookingDataAry['dtCutOff']!='0000-00-00 00:00:00')
            {
                $dtCutOffDate = $bookingDataAry['dtCutOff']; 
                $dtTransportStartDay = date('d',strtotime($dtCutOffDate));
                $dtTransportStartMonth = date('m',strtotime($dtCutOffDate));
                $dtTransportStartYear = date('Y',strtotime($dtCutOffDate));
                $bPickupScheduled = true;
                
                $szStartDateDay = date('j.',strtotime($dtCutOffDate)); 
                $szMonthName = getMonthName($iBookingLanguage,$dtTransportStartMonth,true);
                $szTransportStartDate =$szStartDateDay." ".$szMonthName ;
            } 
            if(!empty($bookingDataAry['dtAvailable']) && $bookingDataAry['dtAvailable']!='0000-00-00 00:00:00')
            {
                
                $dtAvailableDate = $bookingDataAry['dtAvailable']; 
                $dtTransportFinishDay = date('d',strtotime($dtAvailableDate));
                $dtTransportFinishMonth = date('m',strtotime($dtAvailableDate));
                $dtTransportFinishYear = date('Y',strtotime($dtAvailableDate));
                $bDeliveryScheduled = true;
                
                $szFinishDateDay = date('j.',strtotime($dtAvailableDate)); 
                $szMonthName = getMonthName($iBookingLanguage,$dtTransportFinishMonth,true);
                $szTransportFinishDate =$szFinishDateDay." ".$szMonthName ;
            }
            if($bPickupScheduled && $bDeliveryScheduled)
            {
                $iScheduled = 1;
            }
            if($iScheduled==1)
            {
                $iTransitDays = '';
                $szFrequency = "";
                
                $kConfig = new cConfig(); 
                $languageTextDataAry=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$iBookingLanguage);
                $szDay_txt=$languageTextDataAry[1]['szDay_txt'];
                if(!empty($bookingDataAry['dtCutOff']) && $bookingDataAry['dtCutOff']!='0000-00-00 00:00:00' && !empty($bookingDataAry['dtAvailable']) && $bookingDataAry['dtAvailable']!='0000-00-00 00:00:00')
                {    
                    $totalDays=ceil((strtotime($bookingDataAry['dtAvailable'])-strtotime($bookingDataAry['dtCutOff']))/86400);
                    $iTransitDays = $totalDays." ".$szDay_txt; 
                }
                
            }
            else
            {
                $kConfig = new cConfig();
                $transportModeListAry = array(); 
                $transportModeListAry=$kConfig->getConfigurationLanguageData('__TABLE_TRANSPORT_MODE__',$iBookingLanguage);
                
                $kConfig = new cConfig(); 
                $languageTextDataAry=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$iBookingLanguage);
                $szDay_txt=$languageTextDataAry[1]['szDay_txt'];
                
                $szFrequency = $transportModeListAry[$bookingDataAry['idTransportMode']]['szFrequency'];
                $iTransitDays = $bookingDataAry['iTransitHours']." ".$szDay_txt; 
            }
            
            if($bookingDataAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_COURIER__)
            {
                $szTransportStartCity = $bookingDataAry['szOriginCity'];
                $szTransportFinishCity = $bookingDataAry['szDestinationCity'];
                
                $szTransportStartCountry = $bookingDataAry['szShipperCountryCode'];
                $szTransportFinishCountry = $bookingDataAry['szConsigneeCountryCode']; 
            }
            else
            {
                if($bookingDataAry['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataAry['idServiceType']==__SERVICE_TYPE_DTW__ || $bookingDataAry['idServiceType']==__SERVICE_TYPE_DTP__)
                {
                    $szTransportStartCity = $bookingDataAry['szOriginCity'];
                    $szTransportStartCountry = $bookingDataAry['szShipperCountryCode'];
                }
                else
                {
                    if(!empty($bookingDataAry['szWarehouseFromCity']))
                    {
                        $szTransportStartCity = $bookingDataAry['szWarehouseFromCity'];
                        $szTransportStartCountry = $bookingDataAry['szWarehouseFromCountryCode'];
                    }
                    else
                    {
                        $szTransportStartCity = $bookingDataAry['szOriginCity'];
                        $szTransportStartCountry = $bookingDataAry['szShipperCountryCode'];
                    } 
                }
                
                if($bookingDataAry['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataAry['idServiceType']==__SERVICE_TYPE_WTD__ || $bookingDataAry['idServiceType']==__SERVICE_TYPE_PTD__)
                {
                    $szTransportFinishCity = $bookingDataAry['szDestinationCity'];
                    $szTransportFinishCountry = $bookingDataAry['szConsigneeCountryCode']; 
                }
                else
                {
                    if(!empty($bookingDataAry['szWarehouseToCity']))
                    {
                        $szTransportFinishCity = $bookingDataAry['szWarehouseToCity'];
                        $szTransportFinishCountry = $bookingDataAry['szWarehouseToCountryCode']; 
                    }
                    else
                    {
                        $szTransportFinishCity = $bookingDataAry['szDestinationCity'];
                        $szTransportFinishCountry = $bookingDataAry['szConsigneeCountryCode']; 
                    } 
                }
            } 
            $kConfig = new cConfig();
            $transportModeListAry = array();
            $transportModeListAry = $kConfig->getAllTransportMode(false,$iBookingLanguage,true);
            
            /*
            * Following array always contains values in english
            */
            $englishTransportModeListAry = array();
            $englishTransportModeListAry = $kConfig->getAllTransportMode(false,__LANGUAGE_ID_ENGLISH__,true);
            $szTransportMode = $transportModeListAry[$bookingDataAry['idTransportMode']]['szLongName'];
            $szTransportModeCode = $englishTransportModeListAry[$bookingDataAry['idTransportMode']]['szShortName']; 
            
            if($bookingDataAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_COURIER__ || $bookingDataAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_AIR__)
            {
                $szTransportIcon=__AIR_ICON__;
                if($bookingDataAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_COURIER__)
                {
                    $kCourierServices = new cCourierServices();
                    if($kCourierServices->checkFromCountryToCountryExists($bookingDataAry['idOriginCountry'],$bookingDataAry['idDestinationCountry'],false))
                    { 
                        $szTransportIcon=__TRUCK_ICON__;
                    }
                }
            }
            else if($bookingDataAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_FCL__ || $bookingDataAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_SEA__)
            {
                $szTransportIcon=__SEA_ICON__;
            }
            else if($bookingDataAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_ROAD__ || $bookingDataAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_FTL__ || $bookingDataAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_MOVING__)
            {
                $szTransportIcon=__TRUCK_ICON__;
            }
            else if($bookingDataAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_RAIL__)
            {
                $szTransportIcon=__RAIL_ICON__;
            }
            
            $idServiceTerms = $bookingDataAry['idServiceTerms'];
            $serviceTermsAry = array();
            $serviceTermsAry = $kConfig->getAllServiceTerms($idServiceTerms);
            $szServiceTerm = $serviceTermsAry[0]['szShortTerms'];
            
            $szServiceDescription = display_service_type_description($bookingDataAry,$iBookingLanguage);
            
            $szCollection = 'No';
            $szDelivery = 'No';
            $szShipperPostcodeRequired="No";
            $szConsigneePostcodeRequired="No";
            $szBankTransferDueDay = "";
            $szBankTransferDueMonth = "";
            $szBankTransferDueYear = "";
            
            if($bookingDataAry['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataAry['idServiceType']==__SERVICE_TYPE_DTW__ || $bookingDataAry['idServiceType']==__SERVICE_TYPE_DTP__) // 1.DTD 2.DTD
            {
                $szCollection = 'Yes';
            } 
            if($bookingDataAry['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataAry['idServiceType']==__SERVICE_TYPE_WTD__ || $bookingDataAry['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD 2.DTD
            {
                $szDelivery = 'Yes';
            }  
            
            $kConfig = new cConfig();
            $idShipperCountry = $bookingDataAry['idShipperCountry'];
            $kConfig->loadCountry($idShipperCountry); 
            
            if($szCollection=='Yes' && $kConfig->iUsePostcodes==1)
            {
                $szShipperPostcodeRequired = "Yes";
            } 
            
            $kConfig = new cConfig();
            $idConsigneeCountry = $bookingDataAry['idConsigneeCountry'];
            $kConfig->loadCountry($idConsigneeCountry);
             
            if($szDelivery=='Yes' && $kConfig->iUsePostcodes==1)
            {
                $szConsigneePostcodeRequired="Yes";
            }
            
            $szCommodity = false;
            $kVatApplication = new cVatApplication();
            if(!empty($bookingDataAry['szCargoType']))
            {
                $kVatApplication->loadCargoTypes($bookingDataAry['szCargoType']);
                $szCommodity = $kVatApplication->idCargoType; 
            }  
            
            $szDtdTrade=0;
            $kCourierServices = new cCourierServices();
            if($kCourierServices->checkFromCountryToCountryExists($bookingDataAry['idOriginCountry'],$bookingDataAry['idDestinationCountry'],true))
            { 
                $szDtdTrade=1;
            } 
            
            $cargo_volume = format_volume($bookingDataAry['fCargoVolume']);
            if($iBookingLanguage==__LANGUAGE_ID_DANISH__)
            {
                $cargoVolumeText = $cargo_volume." m3";
            }
            else
            {
                $cargoVolumeText=$cargo_volume." cbm";
            }  
            $cargo_weight = number_format_custom((float)ceil($bookingDataAry['fCargoWeight']),$iBookingLanguage) ;
            
            $szCargo = $cargoVolumeText.", ".$cargo_weight." kg"; 
            
            $getQuoteResponseAry = array();  
            $getQuoteResponseAry['dtTransportStartDay'] = $dtTransportStartDay;
            $getQuoteResponseAry['dtTransportStartMonth'] = $dtTransportStartMonth;
            $getQuoteResponseAry['dtTransportStartYear'] = $dtTransportStartYear;
            $getQuoteResponseAry['szTransportStartDate'] = $szTransportStartDate;
            $getQuoteResponseAry['dtTransportFinishDay'] = $dtTransportFinishDay;
            $getQuoteResponseAry['dtTransportFinishMonth'] = $dtTransportFinishMonth;
            $getQuoteResponseAry['szTransportFinishDate'] = $szTransportFinishDate;
            $getQuoteResponseAry['szTransportStartDate'] = $szTransportStartDate;
            $getQuoteResponseAry['szTransportStartCity'] = $szTransportStartCity;
            $getQuoteResponseAry['szTransportStartCountry'] = $szTransportStartCountry; 
            $getQuoteResponseAry['szTransportFinishCity'] = $szTransportFinishCity;  
            $getQuoteResponseAry['szTransportFinishCountry'] = $szTransportFinishCountry;  
            
            $getQuoteResponseAry['szCustomerCurrency'] = $bookingDataAry['szCustomerCurrency'];
            $getQuoteResponseAry['fBookingPrice'] = round((float)$bookingDataAry['fTotalPriceCustomerCurrency']);
            $getQuoteResponseAry['fVatAmount'] = round((float)$bookingDataAry['fTotalVat'],2);
            $getQuoteResponseAry['szCargo'] = $szCargo;
             
            if($bookingDataAry['iPrivateShipping']==1)
            {
                $getQuoteResponseAry['szCustomerType'] = "PRIVATE";
            }
            else
            {
                $getQuoteResponseAry['szCustomerType'] = "BUSINESS"; 
            }  
            $getQuoteResponseAry['szTransportMode'] = cApi::encodeString($szTransportMode);
            $getQuoteResponseAry['szTransportModeCode'] = cApi::encodeString($szTransportModeCode); 
            $getQuoteResponseAry['szTransportIcon'] = $szTransportIcon; 
            $getQuoteResponseAry['szServiceDescription'] = cApi::encodeString($szServiceDescription);
            $getQuoteResponseAry['szServiceTerm'] = $szServiceTerm;
            $getQuoteResponseAry['szCollection'] = $szCollection;
            $getQuoteResponseAry['szDelivery'] = $szDelivery;
            $getQuoteResponseAry['szShipperPostcodeRequired'] = $szShipperPostcodeRequired;
            $getQuoteResponseAry['szConsigneePostcodeRequired'] = $szConsigneePostcodeRequired;
 
            $idForwarder = $bookingDataAry['idForwarder'];
            $kForwarder = new cForwarder();
            $kForwarder->load($idForwarder);

            $kConfig = new cConfig();
            $kConfig->loadCountry($kForwarder->idCountry);
            $szCarrierCountry = $kConfig->szCountryISO;
  
            $getQuoteResponseAry['szCarrierName'] = cApi::encodeString($kForwarder->szDisplayName);
            $getQuoteResponseAry['szForwarderAlias'] = cApi::encodeString($kForwarder->szForwarderAlies);
            $getQuoteResponseAry['szCarrierAddressLine1'] = cApi::encodeString($kForwarder->szAddress);
            $getQuoteResponseAry['szCarrierAddressLine2'] = cApi::encodeString($kForwarder->szAddress2);
            $getQuoteResponseAry['szCarrierAddressLine3'] = cApi::encodeString($kForwarder->szAddress3);
            $getQuoteResponseAry['szCarrierPostcode'] = $kForwarder->szPostCode;
            $getQuoteResponseAry['szCarrierCity'] = cApi::encodeString($kForwarder->szCity);
            $getQuoteResponseAry['szCarrierRegion'] = $kForwarder->szState;
            $getQuoteResponseAry['szCarrierCountry'] = $szCarrierCountry;
            $getQuoteResponseAry['szCarrierCompanyRegistration'] = cApi::encodeString($kForwarder->szCompanyRegistrationNum);
            $getQuoteResponseAry['szCarrierTerms'] = cApi::encodeString($kForwarder->szLink);
               
            //Adding Shipper details 
            $getQuoteResponseAry['szShipperCompanyName'] = cApi::encodeString($bookingDataAry['szShipperCompanyName']);
            $getQuoteResponseAry['szShipperFirstName'] = cApi::encodeString($bookingDataAry['szShipperFirstName']);
            $getQuoteResponseAry['szShipperLastName'] = cApi::encodeString($bookingDataAry['szShipperLastName']);
            $getQuoteResponseAry['szShipperEmail'] = $bookingDataAry['szShipperEmail'];
            $getQuoteResponseAry['szShipperAddress'] = cApi::encodeString($bookingDataAry['szShipperAddress']);
            $getQuoteResponseAry['szShipperAddress2'] = cApi::encodeString($bookingDataAry['szShipperAddress2']);
            $getQuoteResponseAry['szShipperAddress3'] = cApi::encodeString($bookingDataAry['szShipperAddress3']);
            $getQuoteResponseAry['szShipperPostcode'] = cApi::encodeString($bookingDataAry['szShipperPostCode']);
            $getQuoteResponseAry['szShipperCity'] = cApi::encodeString($bookingDataAry['szShipperCity']);
            $getQuoteResponseAry['szShipperCountry'] = $bookingDataAry['szShipperCountryCode'];
            $getQuoteResponseAry['iShipperCountryDialCode'] = $bookingDataAry['iShipperDialCode'];
            $getQuoteResponseAry['iShipperPhoneNumber'] = $bookingDataAry['szShipperPhone'];
             
            //Adding Consignee details
            $getQuoteResponseAry['szConsigneeCompanyName'] = cApi::encodeString($bookingDataAry['szConsigneeCompanyName']);
            $getQuoteResponseAry['szConsigneeFirstName'] = cApi::encodeString($bookingDataAry['szConsigneeFirstName']);
            $getQuoteResponseAry['szConsigneeLastName'] = cApi::encodeString($bookingDataAry['szConsigneeLastName']); 
            $getQuoteResponseAry['szConsigneeEmail'] = $bookingDataAry['szConsigneeEmail'];
            $getQuoteResponseAry['szConsigneeAddress'] = cApi::encodeString($bookingDataAry['szConsigneeAddress']);            
            $getQuoteResponseAry['szConsigneeAddress2'] = cApi::encodeString($bookingDataAry['szConsigneeAddress2']);
            $getQuoteResponseAry['szConsigneeAddress3'] = cApi::encodeString($bookingDataAry['szConsigneeAddress3']);
            $getQuoteResponseAry['szConsigneePostcode'] = cApi::encodeString($bookingDataAry['szConsigneePostCode']);
            $getQuoteResponseAry['szConsigneeCity'] = cApi::encodeString($bookingDataAry['szConsigneeCity']);
            $getQuoteResponseAry['szConsigneeCountry'] = cApi::encodeString($bookingDataAry['szConsigneeCountryCode']);
            $getQuoteResponseAry['iConsigneeCountryDialCode'] = $bookingDataAry['iConsigneeDialCode'];
            $getQuoteResponseAry['iConsigneePhoneNumber'] = $bookingDataAry['szConsigneePhone']; 
             
            //Adding Billing Details
            $getQuoteResponseAry['szBillingCompanyName'] = cApi::encodeString($bookingDataAry['szCustomerCompanyName']);
            $getQuoteResponseAry['szBillingFirstName'] = cApi::encodeString($bookingDataAry['szFirstName']);
            $getQuoteResponseAry['szBillingLastName'] = cApi::encodeString($bookingDataAry['szLastName']); 
            $getQuoteResponseAry['szBillingEmail'] = $bookingDataAry['szEmail'];
            $getQuoteResponseAry['szBillingAddress'] = cApi::encodeString($bookingDataAry['szCustomerAddress1']); 
            $getQuoteResponseAry['szBillingAddress2'] = cApi::encodeString($bookingDataAry['szCustomerAddress2']);
            $getQuoteResponseAry['szBillingAddress3'] = cApi::encodeString($bookingDataAry['szCustomerAddress3']); 
            $getQuoteResponseAry['szBillingPostcode'] = cApi::encodeString($bookingDataAry['szCustomerPostCode']);
            $getQuoteResponseAry['szBillingCity'] = cApi::encodeString($bookingDataAry['szCustomerCity']);
            $getQuoteResponseAry['szBillingCountry'] = cApi::encodeString($bookingDataAry['szCustomerCountryCode']); 
            $getQuoteResponseAry['iBillingCountryDialCode'] = $bookingDataAry['iBillingDialCode'];
            $getQuoteResponseAry['iBillingPhoneNumber'] = $bookingDataAry['szCustomerPhoneNumber'];
            $getQuoteResponseAry['iShipperConsigneeType'] = $bookingDataAry['iShipperConsignee'];
            if(is_string($bookingDataAry['szCargoDescription']) && (mb_detect_encoding($bookingDataAry['szCargoDescription'], "UTF-8", true) == "UTF-8"))
            {
                $bookingDataAry['szCargoDescription']=(utf8_decode($bookingDataAry['szCargoDescription']));
            }
            $getQuoteResponseAry['szCargoDescription'] = cApi::encodeString($bookingDataAry['szCargoDescription']); 
            $getQuoteResponseAry['iScheduled'] = $iScheduled;
            $getQuoteResponseAry['szFrequency'] = cApi::encodeString($szFrequency);
            $getQuoteResponseAry['szDays'] = $iTransitDays;
            //$getQuoteResponseAry['szDtdTrade'] = $szDtdTrade;
            $getQuoteResponseAry['szQuoteComments'] = cApi::encodeString($bookingDataAry['szForwarderComment']);
             
            cPartner::$szDtdTradeValue =$szDtdTrade;  
            if($bookingDataAry['iInsuranceIncluded']==1)
            {
                $getQuoteResponseAry['bInsuranceAvailable'] = 'Yes';
                $getQuoteResponseAry['fInsurancePrice'] = round((float)$bookingDataAry['fTotalInsuranceCostForBookingCustomerCurrency'],2); 
            }
            else
            {
                $getQuoteResponseAry['bInsuranceAvailable'] = 'No';
                $getQuoteResponseAry['fInsurancePrice'] = "0.00"; 
            }  
            $getQuoteResponseAry['iShipperDetailsLocked'] = $bookingDataAry['iShipperDetailsLocked'];
            $getQuoteResponseAry['iConsigneeDetailsLocked'] = $bookingDataAry['iConsigneeDetailsLocked'];
            $getQuoteResponseAry['iCargoDescriptionLocked'] = $bookingDataAry['iCargoDescriptionLocked'];
            $getQuoteResponseAry['szCommodity'] = $szCommodity;
            $getQuoteResponseAry['iRemovePriceGuarantee'] = $bookingDataAry['iRemovePriceGuarantee'];
            $getQuoteResponseAry['szPriceToken'] = $bookingDataAry['szReferenceToken'];
            
            
            $kWHSSearch = new cWHSSearch();
            /*
            * Fetching Average rating of forwarder
            */
            $forwarderRatingAry = $kWHSSearch->getForwarderAvgRating($idForwarder);
            /*
            * Fetching all the reviews of forwarder
            */
            $forwarderReviewsAry = $kWHSSearch->getForwarderAvgReviews($idForwarder);

            /*
            * Fetching all the historical reviews of forwarder
            */ 
            $reviewDetailsAry = $kForwarder->getForwardRating($idForwarder,'24',true);

            $fAverageRating = $forwarderRatingAry['fAvgRating'];
            $iTotalNumRating = $forwarderRatingAry['iNumRating'];
            $iTotalNumReviews = $forwarderReviewsAry['iNumReview'];
 
            $getQuoteResponseAry['fAverageRating'] = round((float)$fAverageRating,2);
            $getQuoteResponseAry['iTotalNumRating'] = $iTotalNumRating;  

            $reviewTempAry = array();
            $ctr=0;
            if(!empty($reviewDetailsAry))
            {
                foreach($reviewDetailsAry as $iNumRating=>$reviewDetailsArys)
                { 
                    $reviewTempAry[$ctr]['iStarRating'] = $iNumRating; 
                    if(!empty($reviewDetailsArys))
                    { 
                        $ctr_1=0;
                        foreach($reviewDetailsArys as $reviewDetailsAryss)
                        { 
                            $szReview = trim($reviewDetailsAryss['szReview']);
                            if(!empty($szReview))
                            {
                                $reviewTempAry[$ctr]['description'][$ctr_1]['szReview'] = cApi::encodeString($szReview);
                                $reviewTempAry[$ctr]['description'][$ctr_1]['dtDateTime'] = $reviewDetailsAryss['dtCompleted'];
                                $ctr_1++;  
                            } 
                        }
                    } 
                    $ctr++;
                }
            } 
            $getQuoteResponseAry['reviews'] = $reviewTempAry; 
            return $getQuoteResponseAry;
        }
    }
    
    function createValidateBookingile($idBookingFile)
    {
        if($idBookingFile>0)
        {
            $kBooking = new cBooking();
            $dtResponseTime = $kBooking->getRealNow();

            $fileLogsAry = array(); 
            $fileLogsAry['szTransportecaStatus'] = 'S1';
            $fileLogsAry['szTransportecaTask'] = 'T1';
            $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
            $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime;
            $fileLogsAry['iSearchType'] = 2;
            if(!empty($fileLogsAry))
            {
                $file_log_query = "";
                foreach($fileLogsAry as $key=>$value)
                {
                    $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            } 
            $kBooking_new = new cBooking();
            $file_log_query = rtrim($file_log_query,",");   
            $kBooking_new->updateBookingFileDetails($file_log_query,$idBookingFile,true); 
        } 
    }
    
    
    function createStandardCargoResponse($postSearchAry)
    {
        $bookingResponseAry['szShipperCompanyName'] = cApi::encodeString($postSearchAry['szShipperCompanyName']);
        $bookingResponseAry['szShipperFirstName'] = cApi::encodeString($postSearchAry['szShipperFirstName']);
        $bookingResponseAry['szShipperLastName'] = cApi::encodeString($postSearchAry['szShipperLastName']);
        $bookingResponseAry['szShipperEmail'] = cApi::encodeString($postSearchAry['szShipperEmail']);
        $bookingResponseAry['iShipperCountryDialCode'] = cApi::encodeString($postSearchAry['iShipperCounrtyDialCode']);
        $bookingResponseAry['iShipperPhoneNumber'] = cApi::encodeString($postSearchAry['szShipperPhoneNumber']);
        $bookingResponseAry['szShipperAddress'] = cApi::encodeString($postSearchAry['szShipperAddress']);
        $bookingResponseAry['szShipperPostcode'] = cApi::encodeString($postSearchAry['szOriginPostCode']);
        $bookingResponseAry['szShipperCity'] = cApi::encodeString($postSearchAry['szOriginCity']);
        if((int)$postSearchAry['idOriginCountry']>0)
        {
            $kConfig = new cConfig();
            $kConfig->loadCountry($postSearchAry['idOriginCountry'],'',$postSearchAry['iBookingLanguage']);
            $szShipperCountryName=$kConfig->szCountryName;
            $szShipperCountry=$kConfig->szCountryISO;
        }
        $bookingResponseAry['szShipperCountry'] = cApi::encodeString($szShipperCountry);
        $bookingResponseAry['szShipperCountryName'] = cApi::encodeString($szShipperCountryName);
        
        
        $bookingResponseAry['szConsigneeCompanyName'] = cApi::encodeString($postSearchAry['szConsigneeCompanyName']);
        $bookingResponseAry['szConsigneeFirstName'] = cApi::encodeString($postSearchAry['szConsigneeFirstName']);
        $bookingResponseAry['szConsigneeLastName'] = cApi::encodeString($postSearchAry['szConsigneeLastName']);
        $bookingResponseAry['szConsigneeEmail'] = cApi::encodeString($postSearchAry['szConsigneeEmail']);
        $bookingResponseAry['iConsigneeCountryDialCode'] = cApi::encodeString($postSearchAry['iConsigneeCounrtyDialCode']);
        $bookingResponseAry['iConsigneePhoneNumber'] = cApi::encodeString($postSearchAry['szConsigneePhoneNumber']);
        $bookingResponseAry['szConsigneeAddress'] = cApi::encodeString($postSearchAry['szConsigneeAddress']);
        $bookingResponseAry['szConsigneePostcode'] = cApi::encodeString($postSearchAry['szDestinationPostCode']);
        $bookingResponseAry['szConsigneeCity'] = cApi::encodeString($postSearchAry['szDestinationCity']);
        if((int)$postSearchAry['idDestinationCountry']>0)
        {
            $kConfig = new cConfig();
            $kConfig->loadCountry($postSearchAry['idDestinationCountry'],'',$postSearchAry['iBookingLanguage']);
            $szConsigneeCountryName=$kConfig->szCountryName;
            $szConsigneeCountry=$kConfig->szCountryISO;
        }
        $bookingResponseAry['szConsigneeCountry'] = cApi::encodeString($szConsigneeCountry);
        $bookingResponseAry['szConsigneeCountryName'] = cApi::encodeString($szConsigneeCountryName);
        
        if($postSearchAry['iUsePartnersBillingDetails']==1)
        {
            $kPartner = new cPartner();
            $idPartner=$postSearchAry['idPartner'];
            $parnerDetailsAry = $kPartner->getParnerAccountDetails($idPartner); 
            $kUserPartner = new cUser();
            $kUserPartner->getUserDetails($parnerDetailsAry['idCustomer']);
            
            
            $bookingResponseAry['szBillingCompanyName'] = cApi::encodeString($kUserPartner->szCompanyName);
            $bookingResponseAry['szBillingFirstName'] = cApi::encodeString($kUserPartner->szFirstName);
            $bookingResponseAry['szBillingLastName'] = cApi::encodeString($kUserPartner->szLastNamestName);
            $bookingResponseAry['szBillingEmail'] = cApi::encodeString($kUserPartner->szEmail);
            $bookingResponseAry['iBillingCountryDialCode'] = cApi::encodeString($postSearchAry['iBillingCountryDialCode']);
            $bookingResponseAry['iBillingPhoneNumber'] = cApi::encodeString($kUserPartner->szPhoneNumber);
            $bookingResponseAry['szBillingAddress'] = cApi::encodeString($kUserPartner->szAddress1);
            $bookingResponseAry['szBillingPostcode'] = cApi::encodeString($kUserPartner->szPostCode);
            $bookingResponseAry['szBillingCity'] = cApi::encodeString($kUserPartner->szCity);
            if((int)$kUserPartner->szCountry>0)
            {
                $kConfig = new cConfig();
                $kConfig->loadCountry($kUserPartner->szCountry,'',$postSearchAry['iBookingLanguage']);
                $szBillingCountryName=$kConfig->szCountryName;
                $szBillingCountry=$kConfig->szCountryISO;
            }
            $bookingResponseAry['szBillingCountry'] = cApi::encodeString($szBillingCountry);
            $bookingResponseAry['szBillingCountryName'] = cApi::encodeString($szBillingCountryName);
        }
        else
        {
            $bookingResponseAry['szBillingCompanyName'] = cApi::encodeString($postSearchAry['szBillingCompanyName']);
            $bookingResponseAry['szBillingFirstName'] = cApi::encodeString($postSearchAry['szBillingFirstName']);
            $bookingResponseAry['szBillingLastName'] = cApi::encodeString($postSearchAry['szBillingLastName']);
            $bookingResponseAry['szBillingEmail'] = cApi::encodeString($postSearchAry['szBillingEmail']);
            $bookingResponseAry['iBillingCountryDialCode'] = cApi::encodeString($postSearchAry['iBillingCountryDialCode']);
            $bookingResponseAry['iBillingPhoneNumber'] = cApi::encodeString($postSearchAry['iBillingPhoneNumber']);
            $bookingResponseAry['szBillingAddress'] = cApi::encodeString($postSearchAry['szBillingAddress']);
            $bookingResponseAry['szBillingPostcode'] = cApi::encodeString($postSearchAry['szBillingPostcode']);
            $bookingResponseAry['szBillingCity'] = cApi::encodeString($postSearchAry['szBillingCity']);
            if((int)$postSearchAry['idBillingCountry']>0)
            {
                $kConfig = new cConfig();
                $kConfig->loadCountry($postSearchAry['idBillingCountry'],'',$postSearchAry['iBookingLanguage']);
                $szBillingCountryName=$kConfig->szCountryName;
                $szBillingCountry=$kConfig->szCountryISO;
            }
            $bookingResponseAry['szBillingCountry'] = cApi::encodeString($szBillingCountry);
            $bookingResponseAry['szBillingCountryName'] = cApi::encodeString($szBillingCountryName);
        }
        
        return $bookingResponseAry;
    }
    
    function validatePricesForQuickQuoteBookings($postSearchAry)
    { 
        if(!empty($postSearchAry))
        {
            $kWHSSearch = new cWHSSearch();
            cPartner::$idGlobalPartner = __QUICK_QUOTE_DEFAULT_PARTNER_ID__; 
            cPartner::$szGlobalToken = $postSearchAry['szReferenceToken']; 
            cPartner::$idBooking = $postSearchAry['id']; 
            $tempResultAry = $kWHSSearch->getSearchedDataFromTempData(false,false,false,true); 
            $searchResultAry = unserialize($tempResultAry['szSerializeData']);  
            $szCourierCalculationLogString = $tempResultAry['szCourierCalculationLogString'];
            $iSearchResultLimit = count($searchResultAry);
  
            $bDisplayPriceChangedPopup = false;
            /*
            * Recalculate transporteca pricing 
            */
            $bDisplayPriceChangedPopup = false; 
            $postSearchAry['iQuickQuote'] = 1;
            $kQuote = new cQuote();
            $szRecalculationTag = $kQuote->recalculateQuickQuotePricing($postSearchAry,true,true,true);
            
            $szPartnerAPICalculationLogs = $kQuote->szCourierCalculationLogString;
            cPartner::$priceCalculationLogs = $szPartnerAPICalculationLogs;  

            if($szRecalculationTag=='SUCCESS' || $szRecalculationTag=='WARNING') 
            {
                $quickQuotePriceAry = array();
                $quickQuotePriceAry['szCustomerCurrencyVaildate'] = $kQuote->szCustomerCurrencyVaildate;
                $quickQuotePriceAry['fTotalPriceCustomerCurrencyVaildate'] = $kQuote->fTotalPriceCustomerCurrencyVaildate;
                $quickQuotePriceAry['fTotalVatCustomerCurrencyVaildate'] = $kQuote->fTotalVatCustomerCurrencyVaildate;
                $quickQuotePriceAry['szBankTransferDueText'] = $kQuote->szBankTransferDueText;
                return $quickQuotePriceAry;
            } 
            else
            {
                $this->iErrorFlag = true;
                return false;
            }
        } 
    }
    
    /*
    * This function is used to check whether 
    * booking post and stored tblpartnerapicall data are same or not
    * @return true or false
    * @author Whiz Solutions
    */
    function isParamsChangedOnValidatePriceCall($data,$idPartner,$szApiRequestMode,$szReferenceToken)
    {
        if(!empty($data))
        {   
            $query="
                SELECT
                    id
                FROM
                    ".__DBC_SCHEMATA_PARTNERS_API_CALL__."
                WHERE
                    szRequestType = 'PRICE'
                AND
                    idPartner = '".(int)$idPartner."'
                AND
                    szRequestMode = '".mysql_escape_custom(trim($szApiRequestMode))."'
                AND
                    szReferenceToken = '".mysql_escape_custom(trim($szReferenceToken))."'                        
                AND
                    idCurrency = '".mysql_escape_custom(trim($data['idCurrency']))."'
            ";
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                { 
                    return 'SUCCESS';
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
        }
        else
        {
            return array();
        }
    }
    
    function updateBookingOriginDestination($postSearchAry,$idBooking)
    {
        if(!empty($postSearchAry) && $idBooking>0)
        {
            $bookingAry = array();
            $bookingAry['idOriginCountry'] = $postSearchAry['idOriginCountry'];
            $bookingAry['szOriginCountry'] = $postSearchAry['szOriginCountry'];
            $bookingAry['szOriginPostCode'] = $postSearchAry['szOriginPostCode'];
            $bookingAry['szOriginCity'] = $postSearchAry['szOriginCity'];
            $bookingAry['fOriginLatitude'] = $postSearchAry['fOriginLatitude'];
            $bookingAry['fOriginLongitude'] = $postSearchAry['fOriginLongitude'];  
            $bookingAry['idDestinationCountry'] = $postSearchAry['idDestinationCountry'];
            $bookingAry['szDestinationCountry'] = $postSearchAry['szDestinationCountry'];
            $bookingAry['szDestinationPostCode'] = $postSearchAry['szDestinationPostCode'];
            $bookingAry['szDestinationCity'] = $postSearchAry['szDestinationCity']; 
            $bookingAry['fDestinationLongitude'] = $postSearchAry['fDestinationLongitude'];
            $bookingAry['fDestinationLatitude'] = $postSearchAry['fDestinationLatitude']; 
             
            $update_query = ''; 
            if(!empty($bookingAry))
            {
                foreach($bookingAry as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            }
            $update_query = rtrim($update_query,","); 
            $kBooking = new cBooking();
            if($kBooking->updateDraftBooking($update_query,$idBooking))
            { 
                return true;
            }
            else
            {
                return false;
            }
        } 
    }
    
    
    function checkShipperConsigneeDetailsChanged($data,$idBooking)
    {
        $kBooking =  new cBooking();
        $bookingArr=$kBooking->getExtendedBookingDetails($idBooking);
        
        $szShipperPostCode=$bookingArr['szShipperPostCode'];
        $szShipperCity=$bookingArr['szShipperCity'];
        
        
        $szConsigneePostCode=$bookingArr['szConsigneePostCode'];
        $szConsigneeCity=$bookingArr['szConsigneeCity'];
        $this->iCheckShipperPostcodeCityDetails=0;
        if($data->szShipperPostcode==$szShipperPostCode && $data->szShipperCity==$szShipperCity)
        {
            $this->iCheckShipperPostcodeCityDetails=1;
        }
        $this->iCheckConsigneePostcodeCityDetails=0;
        if($data->szConsigneePostcode==$szConsigneePostCode && $data->szConsigneeCity==$szConsigneeCity)
        {
            $this->iCheckConsigneePostcodeCityDetails=1;
        }
        return true;
        
    }
    
    function getCustomerShipperConsigneeBillingType($data)
    {
        if($data['szBillingCountry']==$data['szConsigneeCountry'])
        {
            if($data['szBillingFirstName']=='' && $data['szBillingLastName']=='' && $data['iBillingCountryDialCode']=='' && $data['iBillingPhoneNumber']=='' && $data['szBillingEmail']=='' && $data['szBillingAddress']=='')
            {
                return 'CONSIGNEE';
            }
            else if($data['szBillingFirstName']==$data['szConsigneeFirstName'] && $data['szBillingLastName']==$data['szConsigneeLastName'] && $data['iBillingCountryDialCode']==$data['iConsigneeCountryDialCode'] && $data['iBillingPhoneNumber']==$data['iConsigneePhoneNumber'] && $data['szBillingEmail']==$data['szConsigneeEmail'] && $data['szBillingAddress']==$data['szConsigneeAddress'])
            {
                return 'CONSIGNEE';
            }
            else
            {
                return 'BILLING';
            }
        }
        else if($data['szBillingCountry']==$data['szShipperCountry'])
        {
            if($data['szBillingFirstName']=='' && $data['szBillingLastName']=='' && $data['iBillingCountryDialCode']=='' && $data['iBillingPhoneNumber']=='' && $data['szBillingEmail']=='' && $data['szBillingAddress']=='')
            {
                return 'SHIPPER';
            }
            else if($data['szBillingFirstName']==$data['szShipperFirstName'] && $data['szBillingLastName']==$data['szShipperLastName'] && $data['iBillingCountryDialCode']==$data['iShipperCountryDialCode'] && $data['iBillingPhoneNumber']==$data['iShipperPhoneNumber'] && $data['szBillingEmail']==$data['szShipperEmail'] && $data['szBillingAddress']==$data['szShipperAddress'])
            {
                return 'SHIPPER';
            }
            else
            {
                return 'BILLING';
            }
        }
        else
        {
            return 'BILLING';
        }
    }
    
    function getLastBookingsDetailsByCustomer($idCustomer)
    {
        if($idCustomer>0)
        {
            $kUser_new = new cUser();
            $lastBookingAry = array();
            $lastBookingAry = $kUser_new->getLastBookingByUserID($idCustomer);
            
            if(!empty($lastBookingAry))
            { 
                $szPendingTrayUrl = __BASE_MANAGEMENT_URL__."/pendingTray/".$lastBookingAry['szBookingRandomNum']."/";
                $szVolume =  format_volume($lastBookingAry['fCargoVolume']);
                $szWeight =  get_formated_cargo_measure($lastBookingAry['fCargoWeight']);
 
                $szCustomerNotes = "Last booking: ".$lastBookingAry['szBookingRef'].", ".$szVolume."cbm/".$szWeight."kg from ".$lastBookingAry['szOriginCity']." to ".$lastBookingAry['szDestinationCity']." (".$szPendingTrayUrl.") ";
            }
            else
            {
                $szCustomerNotes = 'No booking yet';
            }
            return $szCustomerNotes;
        }
    }
}

?>