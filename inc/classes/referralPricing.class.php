<?php
/**
 * This file is the container for partner api related functionality. 
 *
 * api.class.php
 *
 * @copyright Copyright (C) 2015 Transporteca
 * @author Ajay
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" ); 

class cReferralPricing extends cDatabase
{     
    function __construct()
    {
        parent::__construct();
        return true;
    }
    
    function addReferalFeeByTransportMode($data)
    {
        if(!empty($data))
        { 
            $idTransportMode = $data['idTransportMode']; 
            $idForwarder = $data['idForwarder'];
            $fReferralFee = $data['fReferralFee'];
   
            $iUpdatedByAdmin = 0;
            if((int)$_SESSION['admin_id']>0)
            {
                $iUpdatedByAdmin = 1;
                $idForwarderUpdatedBy = $_SESSION['admin_id'];
            }
            else if((int)$_SESSION['forwarder_admin_id']>0)
            {
                $iUpdatedByAdmin = 1;
                $idForwarderUpdatedBy = $_SESSION['forwarder_admin_id'];
            }
            else
            {
                $idForwarderUpdatedBy = $_SESSION['forwarder_user_id'];
            }

            if($this->isReferralFeeExists($idTransportMode,$idForwarder))
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_FORWARDER_PRICING__."
                    SET
                        fReferralFee = '".mysql_escape_custom(trim($fReferralFee))."',  
                        idForwarderUpdatedBy = '".mysql_escape_custom(trim($idForwarderUpdatedBy))."',
                        iUpdatedByAdmin = '".mysql_escape_custom(trim($iUpdatedByAdmin))."', 
                        dtUpdateOn = now()
                    WHERE
                        idForwarder = '".(int)$idForwarder."'
                    AND
                        idTransportMode = '".(int)$idTransportMode."'    
                ";
            }
            else
            {
                $query = "
                    INSERT INTO
                        ".__DBC_SCHEMATA_FORWARDER_PRICING__."
                    ( 
                        idTransportMode,
                        idForwarder, 
                        fReferralFee,
                        idForwarderUpdatedBy, 
                        iUpdatedByAdmin,
                        iActive,
                        dtCreatedOn
                    )
                    VALUES
                    ( 
                        '".mysql_escape_custom($idTransportMode)."',
                        '".mysql_escape_custom($idForwarder)."', 
                        '".mysql_escape_custom($fReferralFee)."', 
                        '".mysql_escape_custom($idForwarderUpdatedBy)."',
                        '".mysql_escape_custom($iUpdatedByAdmin)."',
                        '1',
                        now()
                    )	
                ";
            }
//            echo $query;
//            die;
            if($result=$this->exeSQL($query))
            {
                return true;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    
    function isReferralFeeExists($idTransportMode,$idForwarder)
    {
        if($idTransportMode>0 && $idForwarder>0)
        {
            $query="
                SELECT
                    id
                FROM
                    ".__DBC_SCHEMATA_FORWARDER_PRICING__."
                WHERE
                    idForwarder = '".(int)$idForwarder."'
                AND
                    idTransportMode = '".(int)$idTransportMode."'     
            ";
            //echo "<br>".$query."<br>" ;
            //die;
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    return true;
                }
                else
                { 
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        } 
    }
    
    function getAllReferralFeeByForwarder($idForwarder,$idTransportMode=false)
    {
        if($idForwarder>0)
        {
            if($idTransportMode>0)
            {
                $query_and = " AND idTransportMode = '".(int)$idTransportMode."' ";
            }
            $query="
                SELECT
                    id,
                    idTransportMode,
                    idForwarder, 
                    fReferralFee
                FROM
                    ".__DBC_SCHEMATA_FORWARDER_PRICING__."
                WHERE
                    idForwarder = '".(int)$idForwarder."' 
                    $query_and
            "; 
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $ret_ary = array(); 
                    while($row=$this->getAssoc($result))
                    {
                        $ret_ary[$row['idTransportMode']] = $row;
                    }
                    return $ret_ary;
                }
                else
                { 
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        } 
    }
    
    function getForwarderReferralFee($data)
    {
        if(!empty($data))
        {
            //$iOldVersion = 1;
            $idTransportMode = $data['idTransportMode'];
            $idForwarder = $data['idForwarder'];
            if($iOldVersion==1)
            {
                $kForwarder = new cForwarder();
                $kForwarder->load($idForwarder);
                return $kForwarder->fReferalFee;
            }
            else
            { 
                if($idForwarder>0 && $idTransportMode>0)
                {
                    $referralFeeAry = array();
                    $referralFeeAry = $this->getAllReferralFeeByForwarder($idForwarder,$idTransportMode);

                    if(!empty($referralFeeAry[$idTransportMode]))
                    {
                        return (float)$referralFeeAry[$idTransportMode]['fReferralFee'];
                    }
                    else
                    {
                        return 0.00;
                    }
                }
                else
                {
                    return 0.00;
                }
            } 
        }
    }
}
?>