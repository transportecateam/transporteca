<?php
/**
 * This file is the container for all admin related functionality.
 * All functionality related to admin details should be contained in this class.
 *
 * admin.class.php
 *
 * @copyright Copyright (C) 2012 Transporteca, LLC
 * @author Anil
 * @package Transporteca Development
 */
if(!isset($_SESSION))
{
    session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_INC__ . "/functions.php" );
require_once( __APP_PATH_INC__ . "/courier_functions.php" );
require_once( __APP_PATH_CLASSES__ . "/database.class.php");
require_once( __APP_PATH__ . "/inc/classes/parser.class.php" );
require_once(__APP_PATH_INC__."/lib/easypost.php");
require_once( __APP_PATH__ . "/inc/courier_functions.php" );

class cWebServices extends cDatabase
{  
    public $t_base = "SERVICEOFFERING/"; 
    function __construct()
    {
        $seriverTypeArr = array(); 
        
        //Valid domestic values
        $seriverTypeArr["01"] = 'Next Day Air';
        $seriverTypeArr["02"] = '2nd Day Air'; 
        $seriverTypeArr["03"] = 'Ground';
        $seriverTypeArr["12"] = '3 Day Select';
        $seriverTypeArr["13"] = 'Next Day Air Saver';
        $seriverTypeArr["14"] = 'Next Day Air Early AM';
        $seriverTypeArr["59"] = '2nd Day Air AM';

        //Valid international values:
        $seriverTypeArr["07"] = 'Worldwide Express';
        $seriverTypeArr["08"] = 'Worldwide Expedited';
        $seriverTypeArr["11"] = 'Standard';
        $seriverTypeArr["54"] = 'Worldwide Express Plus';
        $seriverTypeArr["65"] = 'UPS Saver. Required for Rating and Ignored for Shopping';

        //Valid Poland to Poland Same Day values:
        $seriverTypeArr["82"] = 'UPS Today Standard';
        $seriverTypeArr["83"] = "UPS Today Dedicated Courier";
        $seriverTypeArr["84"] = "UPS Today Intercity";
        $seriverTypeArr["85"] = "UPS Today Express";
        $seriverTypeArr["86"] = "UPS Today Express Saver";
        $seriverTypeArr["96"] = "UPS Worldwide Express Freight";
        
        $this->seriverTypeArr = $seriverTypeArr; 
        parent::__construct();
        return true;
    } 
    
    var $t_base_courier='management/providerProduct/'; 
    var $t_base_services = "SERVICEOFFERING/"; 
    var $t_base_courier_label ="COURIERLABEL/";
      
    function addFedexApiLogs($data)
    {
        if(!empty($data))
        {
            $query="
                INSERT INTO
                    ".__DBC_SCHEMATA_FEDEX_API_LOGS__."
                (
                    idUser,
                    iWebServiceType,
                    szRequestData,
                    szResponseData,
                    szDeveloperNotes,
                    szReferenceToken,
                    szResponseSerializedData,
                    idBooking,
                    dtCreatedOn
                )
                VALUES
                (
                    '".mysql_escape_custom(trim($data['idUser']))."',
                    '".mysql_escape_custom(trim($data['iWebServiceType']))."',
                    '".mysql_escape_custom(trim($data['szRequestData']))."',
                    '".mysql_escape_custom(trim($data['szResponseData']))."',
                    '".mysql_escape_custom(trim($data['szDeveloperNotes']))."',
                    '".mysql_escape_custom(trim($data['szReferenceToken']))."',
                    '".mysql_escape_custom(trim($data['szResponseSerializedData']))."',
                    '".mysql_escape_custom(trim($data['idBooking']))."', 
                    now()
                )
            ";		

            if(($result = $this->exeSQL($query)))
            {
                return true;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }		
        }
    }
    
    function getCourierApiReponseFromTemp($szReferenceToken,$iWebServiceType=false)
    {
        if(!empty($szReferenceToken))
        {
            if($iWebServiceType>0)
            {
                $query_and = " AND iWebServiceType = '".(int)$iWebServiceType."' ";
            } 
            $query="
                SELECT
                    id,
                    idUser,
                    iWebServiceType,
                    szRequestData,
                    szResponseData,
                    szDeveloperNotes,
                    szReferenceToken,
                    szResponseSerializedData,
                    idBooking,
                    dtCreatedOn
                FROM
                    ".__DBC_SCHEMATA_FEDEX_API_LOGS__."
                WHERE 
                    szReferenceToken = '".  mysql_escape_custom($szReferenceToken)."' 
                    $query_and
                ORDER BY
                    dtCreatedOn DESC
                LIMIT
                    0,1
            "; 
            //echo $query ;
            if($result = $this->exeSQL($query))
            {
                $cargoAry=array();
                if($this->iNumRows>0)
                {
                    $ctr = 0;
                    $row = $this->getAssoc($result); 
                    return $row;
                }
                else
                {
                    return false;
                }
            }
        } 
    }
    function createTntRequestXml($data,$check_auth=false,$test_page=false)
    {
        if(!empty($data))
        {  
            if(!empty($data['dtTimingDate']))
            { 
                $dtShippingDate = date('c',strtotime($data['dtTimingDate']));
            }
            else
            {  
                $dtShippingDate = date('c');
            }   
            $data['szAccountNumber'] = '';
            if($check_auth)
            {
                /* We check authenticaion when Managment user tries to add/eit current aggrement from this page http://management.transporteca.com/currentAgreement/ 
                 * In this case we only need to check user's API login details. 
                 */
                
                $szTntRequestXml = '
                    <?xml version="1.0" encoding="UTF-8"?><priceRequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                       <appId>'.$data['szAccountNumber'].'</appId>
                       <appVersion>3.0</appVersion>
                       <priceCheck>
                         <rateId>rate2</rateId>
                         <sender>
                           <country>DK</country>
                           <town>Copenhagen</town>
                           <postcode>2300</postcode>
                         </sender>
                         <delivery>
                           <country>DE</country>
                           <town>Berlin</town>
                           <postcode>10117</postcode>
                         </delivery>
                         <collectionDateTime>'.$dtShippingDate.'</collectionDateTime>
                         <product>
                           <type>N</type>
                         </product>
                         <account>
                            <accountNumber>2013579</accountNumber>
                            <accountCountry>DK</accountCountry>
                        </account> 
                         <currency>DKK</currency>
                         <priceBreakDown>true</priceBreakDown> 
                         <pieceLine>
                           <numberOfPieces>1</numberOfPieces>
                           <pieceMeasurements>
                             <length>2</length>
                             <width>5</width>
                             <height>2</height>
                             <weight>5</weight>
                           </pieceMeasurements>
                           <pallet>1</pallet>
                         </pieceLine>
                       </priceCheck>
                     </priceRequest>
               ';  
            }
            else
            {
                if($test_page)
                {
                    $chargeAbleFlag=false;
                    $fVolume = ($data['fCargoLength'] * $data['fCargoWidth'] * $data['fCargoHeight'] * .000001);
                    $fWeight = $data['fCargoWeight'] ;
                    
                    $fVolume = round_up($fVolume,2);
                    $data['fCargoVolume'] = $fVolume ;
                    $data['fCargoWeight'] = $fWeight ;
                     
                    if(($data['fCargoVolume']*200)>$data['fCargoWeight'])
                    {
                        $fDefaultChargeableWeight = $data['fCargoVolume']*200 ; 
                    }
                    else
                    {
                        $fDefaultChargeableWeight = $data['fCargoWeight'];
                    }
                    $fDefaultChargeableWeight = $data['fCargoWeight'];
                    $data['szCurrency'] = 'DKK';
                    $iTotalQuantity = 1;
                    
                   // $fCargoLength = round((float)($data['fCargoLength']/100),2);
                   // $fCargoWidth = round((float)($data['fCargoWidth']/100),2);
                   // $fCargoHeight = round((float)($data['fCargoHeight']/100),2);
                    
                    $szCargodetailsXml = ' 
                        <priceBreakDown>true</priceBreakDown>
                        <pieceLine>
                            <numberOfPieces>1</numberOfPieces>
                            <pieceMeasurements>
                              <length>'.(float)$fCargoLength.'</length>
                              <width>'.(float)$fCargoWidth.'</width>
                              <height>'.(float)$fCargoHeight.'</height>
                              <weight>'.(float)$data['fCargoWeight'].'</weight>
                            </pieceMeasurements> 
                            <pallet>false</pallet>
                        </pieceLine>
                    ';
                }
                else
                { 
                    if(empty($data['szCurrency']))
                    {
                        $data['szCurrency'] = 'USD';
                    }  
                    $chargeAbleFlag=false;
                    if(($data['fCargoVolume']*200)>$data['fCargoWeight'])
                    {
                        $fDefaultChargeableWeight = $data['fCargoVolume']*200 ;
                        $chargeAbleFlag=true;
                    }
                    else
                    {
                        $fDefaultChargeableWeight = $data['fCargoWeight'];
                    }
                    $fDefaultChargeableWeight = $data['fCargoWeight'];
                    $iTotalQuantity = $data['iTotalQuantity'];
                    
                    if($iTotalQuantity<=0)
                    {
                        $iTotalQuantity = 2;
                    }
                    $bDefaultPackageFlag = true; 
                    $idBooking = $data['idBooking'];
                    $bPartnerPacking = false;
                    if(cPartner::$bApiValidation && !empty(cPartner::$searchablePacketTypes) && (in_array(1, cPartner::$searchablePacketTypes) || in_array(2, cPartner::$searchablePacketTypes)))
                    {
                        $bPartnerPacking = true;
                    }
                    if($idBooking>0 || $bPartnerPacking)
                    {
                        $kBooking =new cBooking();
                        
                        if($bPartnerPacking)
                        {
                            $kPartner = new cPartner();
                            $bookingCargoAry = $kPartner->getCargoDeailsByPartnerApi(); 
                        }
                        else
                        {
                            $bookingCargoAry = $kBooking->getCargoDeailsByBookingId($idBooking,true);
                            $postSearchAry = $kBooking->getBookingDetails($idBooking);
                        } 
                        $kWHSSearch = new cWHSSearch();
                        if(!empty($bookingCargoAry) && ($bPartnerPacking || $postSearchAry['iCourierBooking']==1 || $postSearchAry['iSearchMiniVersion']==3 || $postSearchAry['iSearchMiniVersion']==4))
                        {  
                            $ctr=1;
                            $iQuantityCounter = 0;
                            $bDefaultPackageFlag = false;
                            foreach($bookingCargoAry as $bookingCargoArys)
                            { 
                                if($bookingCargoArys['idWeightMeasure']==1)
                                {
                                    $fKgFactor = 1;
                                }
                                else
                                { 
                                    $fKgFactor = $kWHSSearch->getWeightFactor($bookingCargoArys['idWeightMeasure']); 
                                }

                                if($bookingCargoArys['idCargoMeasure']==1)
                                {
                                    $fCmFactor = 1;
                                }
                                else
                                {
                                    $fCmFactor = $kWHSSearch->getCargoFactor($bookingCargoArys['idCargoMeasure']);  
                                }

                                $iLenght = 0;
                                $iWidth = 0;
                                $iHeight = 0;
                                $fWeight = 0;

                                if($fCmFactor>0)
                                {
                                    //Converting values into Cms
                                    $iLenght = round((float)($bookingCargoArys['fLength'] / $fCmFactor),4);
                                    $iWidth = round((float)($bookingCargoArys['fWidth'] / $fCmFactor),4);
                                    $iHeight = round((float)($bookingCargoArys['fHeight'] / $fCmFactor),4);
                                } 
                                if($fKgFactor>0)
                                {
                                    $fWeight = round((float)($bookingCargoArys['fWeight']/$fKgFactor),2) ;
                                } 
                                
                                for($i=0;$i<$bookingCargoArys['iQuantity'];$i++)
                                {
                                    //Now convert these values in to Meters as TNT API accepts measurement in Meters
                                    $fCargoLength = round((float)($iLenght/100),2);
                                    $fCargoWidth = round((float)($iWidth/100),2);
                                    $fCargoHeight = round((float)($iHeight/100),2);

                                    $szCargodetailsXml .= '  
                                        <pieceLine>
                                            <numberOfPieces>1</numberOfPieces>
                                            <pieceMeasurements>
                                              <length>'.(float)$fCargoLength.'</length>
                                              <width>'.(float)$fCargoWidth.'</width>
                                              <height>'.(float)$fCargoHeight.'</height>
                                              <weight>'.(float)$fWeight.'</weight>
                                            </pieceMeasurements> 
                                            <pallet>false</pallet>
                                        </pieceLine>
                                    ';
                                    $iQuantityCounter++; 
                                } 
                                $szPackageDetailsLogs .= "<br> Package: ".$bookingCargoArys['fLength']." X ".$bookingCargoArys['fWidth']." X ".$bookingCargoArys['fHeight']." ".$szCargoMeasure. ", ".$bookingCargoArys['fWeight']." ".$szWeightMeasure.", Count: ".$bookingCargoArys['iQuantity'] ;
                            }  
                            $iTotalQuantity = $iQuantityCounter;
                        }
                        $this->szPackageDetailsLogs .= $szPackageDetailsLogs ;
                    } 
                    if($bDefaultPackageFlag)
                    {
                        $szCargodetailsXml = '
                            <consignmentDetails>
                                <totalWeight>'.$fDefaultChargeableWeight.'</totalWeight>
                                <totalVolume>'.$data['fCargoVolume'].'</totalVolume>
                                <totalNumberOfPieces>'.$iTotalQuantity.'</totalNumberOfPieces>
                            </consignmentDetails>
                        ';
                    } 
                }
                
                $szPayerType = 'S';
                if($data['szSCountry']=='DK')
                {
                    //If shipment is imported to Denmark then receiver pays the bill otherwise always sender pays the bill
                    $szPayerType = 'R';
                } 
                $szRandomRateID = 'Rate1';               
                $szTntRequestXml = '
                    <?xml version="1.0" encoding="UTF-8"?>
                        <priceRequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                            <appId>PC</appId>
                            <appVersion>3.0</appVersion>
                            <priceCheck>
                              <rateId>'.$szRandomRateID.'</rateId>
                              <sender>
                                <country>'.$data['szCountry'].'</country>
                                <town>'.$data['szCity'].'</town>
                                <postcode>'.$data['szPostCode'].'</postcode>
                              </sender> 
                              <delivery>
                                <country>'.$data['szSCountry'].'</country>
                                <town>'.$data['szSCity'].'</town>
                                <postcode>'.$data['szSPostCode'].'</postcode> 
                              </delivery>
                              <collectionDateTime>'.$dtShippingDate.'</collectionDateTime>
                              <product>
                                <type>N</type>
                              </product>  
                              <account>
                                  <accountNumber>2013579</accountNumber>
                                  <accountCountry>DK</accountCountry>
                              </account> 
                              <termsOfPayment>'.$szPayerType.'</termsOfPayment>
                              <currency>'.$data['szCurrency'].'</currency>
                             '.$szCargodetailsXml.'</priceCheck>
                        </priceRequest>
                ';   
            }   
            return $szTntRequestXml;
        } 
    }
    
    function calculateRatesFromUpsNewApi($data,$flag=false,$test_page=false)
    {  
        $this->validateInputNewDetails($data);  
        if($this->error==true)
        {
            return array();
        }   
        if($_SESSION['user_id']>0)
        { 
            $kUser->getUserDetails($_SESSION['user_id']); 
            $iPrivateShipping = $kUser->iPrivate; 
            $data['iPrivateShipping'] = $iPrivateShipping;
            $data['idCustomerCountry'] = $kUser->szCountry;
        } 
        else
        {
            $iPrivateShipping = $data['iPrivateShipping'];
            $data['idCustomerCountry'] = $data['idDestinationCountry'];
        }
        if((int)$data['iPrivateShipping']==0 && !$test_page)
        {
            $kVatApplication = new cVatApplication();
            
            $fVatRate=$kVatApplication->getTransportecaVat($data); 
            
        }
        
        $seriverTypeArr=array("01"=>"Next Day Air","02"=>"2nd Day Air","03"=>"Ground","12"=>"3 Day Select","13"=>"Next Day Air Saver","14"=>"Next Day Air Early AM","59"=>"2nd Day Air AM",
            "07"=>"Worldwide Express","08"=>"Worldwide Expedited","11"=>"Standard","54"=>"Worldwide Express Plus","65"=>"UPS Saver. Required for Rating and Ignored for Shopping",
            "82"=>"UPS Today Standard","83"=>"UPS Today Dedicated Courier","84"=>"UPS Today Intercity","85"=>"UPS Today Express","86"=>"UPS Today Express Saver","96"=>"UPS Worldwide Express Freight");
        
        $validServiceTypeArr=array();
        $resArr=$this->getCourierServices($data,$flag); 
        if($data['szAPICode']!='')
        {
            $validServiceTypeArr=explode(";",$data['szAPICode']);
        }  
        //print_r($validServiceTypeArr);
        $szApiResponseLog = $data['szForwarderCompanyName']." - UPS";
        
        $szApiResponseLog .= "<br><u>API response:</u>";
        
        $kUser = new cUser();
        $szAccountNumber=decrypt($data['szAccountNumber'],ENCRYPT_KEY);
        $szUsername=decrypt($data['szUsername'],ENCRYPT_KEY);
       
        $szPassword=decrypt($data['szPassword'],ENCRYPT_KEY);
        
        //ConfigurationUPS Express Saver 
        $access = $szAccountNumber;
        $userid = $szUsername;
        $passwd = $szPassword; 
        $wsdl = __APP_PATH__."/wsdl/RateWS.wsdl";
        $operation = "ProcessRate";
        $endpointurl = 'https://onlinetools.ups.com/webservices/Rate';
        $outputFileName = __APP_PATH__."/wsdl/XOLTResult.xml";
  
        if(!empty($data['dtTimingDate']))
        { 
            $dtShippingDate = $data['dtTimingDate'];
        }
        else
        { 
            $dtShippingDate = date('c');
        } 
        try
        { 
            $mode = array
            (
                'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
                'trace' => 1
            ); 
            // initialize soap client
            $client = new SoapClient($wsdl , $mode);
            //set endpoint url
            $client->__setLocation($endpointurl);
 
            //create soap header
            $usernameToken['Username'] = $userid;
            $usernameToken['Password'] = $passwd;
            $serviceAccessLicense['AccessLicenseNumber'] = $access;
            $upss['UsernameToken'] = $usernameToken;
            $upss['ServiceAccessToken'] = $serviceAccessLicense;
             
            try
            {
                $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0','UPSSecurity',$upss);
                $client->__setSoapHeaders($header);

                $request = $this->prepareUpsApiRequest($data,$flag,$test_page);   
                 
                $resp = $client->__soapCall($operation ,array($request));  
            } 
            catch (Exception $ex) 
            { 
                $outputFileName = __APP_PATH__."/wsdl/ups_api_exception.log";
                 //save soap request and response to file
                $fw = fopen($outputFileName , 'w'); 
                fwrite($fw , "Exception: \n" . print_R($ex,true) . "\n");
                fclose($fw); 
            } 
            
            //get status
            $szResponseStatus = $resp->Response->ResponseStatus->Description;
            $szAlertCode = $resp->Response->ResponseStatus->Alert->Code;
            $szDescription = $resp->Response->ResponseStatus->Alert->Description;
            
            $this->szPackageDetailsLogs .= "<br> <strong> API Response:</strong> <br><br> Status: ".  strtoupper($szResponseStatus)."";
            $this->szPackageDetailsLogs .= " <br> Status code: ".$szAlertCode."";
            $this->szPackageDetailsLogs .= " <br> Description: ".$szDescription."<br><br>";
             
            $fedexApiLogsAry = array(); 
            $fedexApiLogsAry['iWebServiceType'] = 2 ;
            $fedexApiLogsAry['szRequestData'] = print_R($request,true);
            $fedexApiLogsAry['szResponseData'] = print_R($resp,true);
            $this->addFedexApiLogs($fedexApiLogsAry);
        
            // Logging UPS API request and response in xml file 
            $requestFileName = __APP_PATH__."/wsdl/ups_api_request.xml";
            $responseFileName = __APP_PATH__."/wsdl/ups_api_response.xml";

            //save soap request and response to file
            $fw = fopen($requestFileName ,'w');
            fwrite($fw ,$client->__getLastRequest() . "\n");
            fclose($fw);

            $fw = fopen($responseFileName ,'w');
            fwrite($fw , $client->__getLastResponse() . "\n");
            fclose($fw);
                   
            if(strtolower($szResponseStatus)=='success')
            {
                $RatedShipmentAry = $resp->RatedShipment ;   
                $kWarehouseSearch = new cWHSSearch();
                $resultAry = $kWarehouseSearch->getCurrencyDetails(26);		
                
                $kCourierService = new cCourierServices();
                $upsDeliveryDateAry = array();
                $upsDeliveryDateAry = $kCourierService->calculateShipmentTransitTimeUPS($data);
                 
                $fInrExchangeRate = 0;
                if($resultAry['fUsdValue']>0)
                {
                    $fInrExchangeRate = $resultAry['fUsdValue'] ;						 
                }
                $iNumDaysAddedToDelivery = $kWarehouseSearch->getManageMentVariableByDescription('__NUMBER_OF_DAYS_ADDED_TO_DELIVERY_DATE__');
                
                $courierProductMapping = array();
                $courierProductMapping = $this->getAllProductByApiCodeMapping();
                 
                $szReturnStr =  '<h4>Your Rates: </h4><table class="format-2" style="width:90%">';
                $szReturnStr .= '<tr><td><strong>Service Type</strong></td><td><strong>Amount</strong></td><td><strong>Shipping Date</strong></td><td><strong>Delivery Date</strong></td></tr>';
                $ctr=0;
                 
                if(!empty($RatedShipmentAry))
                {
                    foreach($RatedShipmentAry as $RatedShipmentArys)
                    {  
                        $code = $RatedShipmentArys->Service->Code;
                        if($test_page)
                        {
                            $iTransitDays = $RatedShipmentArys->GuaranteedDelivery->BusinessDaysInTransit ;
                            $TotalNetCharge = $RatedShipmentArys->TotalCharges->MonetaryValue;
                            $dtShipmentTime = $RatedShipmentArys->GuaranteedDelivery->DeliveryByTime;
                            $szCurrencyCode = $RatedShipmentArys->TotalCharges->CurrencyCode ;
                            $idServiceCode = $RatedShipmentArys->Service->Code;

                            $szNegotiableCurrencyCode = $RatedShipmentArys->NegotiatedRateCharges->TotalCharge->CurrencyCode ;
                            $fNegotiableTotalCharge = $RatedShipmentArys->NegotiatedRateCharges->TotalCharge->MonetaryValue ;

                            if($fNegotiableTotalCharge>0)
                            {
                                $TotalNetCharge = $fNegotiableTotalCharge;
                                $szCurrencyCode = $szNegotiableCurrencyCode;
                            }

                            if($TotalNetCharge>0 && $code!=54)
                            {
                                if($iTransitDays>0)
                                {
                                    if(empty($dtShipmentTime))
                                    {
                                        $dtShipmentTime = "By EOD";
                                    } 
                                    $szTransitDays = date('Y-m-d',strtotime("+".($iTransitDays+1)." DAYS"))." ".$dtShipmentTime ;
                                }
                                else
                                { 
                                    $szTransitDays = 'N/A';
                                }  
                                $fShipmentCost = number_format($TotalNetCharge,2,".",",") ;
                                $serviceType = '<tr><td>'.$seriverTypeArr[$idServiceCode].'</td>';
                                $amount = '<td>'.$szCurrencyCode." " . $fShipmentCost . '</td>';
                                $shippingDate = '<td>'.date('Y-m-d H:i:s') . '</td>';
                                $deliveryDate =  '<td>'.$szTransitDays. '</td>';

                                $szReturnStr .= $serviceType . $amount.$shippingDate. $deliveryDate;
                                $szReturnStr .= '</tr>'; 
                            }
                        }
                        else
                        { 
                            if(!empty($validServiceTypeArr) && in_array($code,$validServiceTypeArr))
                            {    
                                $idCourierProviderProductid = false;
                                $pricingArr=$this->getCourierProductProviderPricingData($data,$code,$idCourierProviderProductid);
                                $iTransitDays = 0; 
                                $iServiceCode = (int)$code;

                                /*
                                 * 
                                 */ 
                                $iTransitDays = $RatedShipmentArys->GuaranteedDelivery->BusinessDaysInTransit ;

                                if($iTransitDays<=0 && !empty($upsDeliveryDateAry[$iServiceCode]))
                                {
                                    $iTransitDays = $upsDeliveryDateAry[$code]['iBusinessDaysInTransit'] ; 
                                } 
                                if($iTransitDays<=0)
                                {
                                    $iTransitDays = $courierProductMapping[$code]['iDaysStd'];  
                                } 

                                $TotalNetCharge = $RatedShipmentArys->TotalCharges->MonetaryValue;
                                $dtShipmentTime = $RatedShipmentArys->GuaranteedDelivery->DeliveryByTime;
                                $szCurrencyCode = $RatedShipmentArys->TotalCharges->CurrencyCode ;

                                $szNegotiableCurrencyCode = $RatedShipmentArys->NegotiatedRateCharges->TotalCharge->CurrencyCode ;
                                $fNegotiableTotalCharge = $RatedShipmentArys->NegotiatedRateCharges->TotalCharge->MonetaryValue ;

                                if($fNegotiableTotalCharge>0)
                                {
                                    $TotalNetCharge = $fNegotiableTotalCharge;
                                    $szCurrencyCode = $szNegotiableCurrencyCode;
                                }

                                if($szCurrencyCode=='RMB')
                                {
                                    $szCurrencyCode='CNY';
                                }

                                $szApiResponseLog .=" <br> Shipment Price: ".$szCurrencyCode." ".number_format((float)$TotalNetCharge,2);

                                if(!empty($upsDeliveryDateAry[$code]))
                                {
                                    $szApiResponseLog .=" <br> Pickup date: ".$upsDeliveryDateAry[$code]['dtPickupDate'] ; 
                                    $szApiResponseLog .=" <br> Business Days In Transit: ".$upsDeliveryDateAry[$code]['iBusinessDaysInTransit'] ;
                                    $szApiResponseLog .=" <br> Total Days In Transit: ".$upsDeliveryDateAry[$code]['iTotalTransitDays'] ;
                                    $szApiResponseLog .=" <br> Arrival Date: ".$upsDeliveryDateAry[$code]['dtArrivalDate'] ;
                                    $szApiResponseLog .=" <br> Days of Week: ".$upsDeliveryDateAry[$code]['szDayOfWeek'] ;
                                    $szApiResponseLog .=" <br> Shipping Type: ".$seriverTypeArr[$code];
                                }
                                else
                                {
                                    $szApiResponseLog .=" <br> Shipping date: ".$dtShippingDate ;
                                    $szApiResponseLog .=" <br> Shipping Type: ".$seriverTypeArr[$code];
                                }

                                if(strtolower($szCurrencyCode)=='usd')
                                {    
                                    $fExchangeRate='1.000';
                                }
                                else
                                {
                                     $fExchangeRate=$this->getExchangeRateCourierPrice($szCurrencyCode);
                                }    
                                if((float)$fExchangeRate>0)
                                {
                                    if($TotalNetCharge>0)
                                    {
                                        if($iTransitDays>0)
                                        {
                                            if(empty($dtShipmentTime))
                                            {
                                               // $dtShipmentTime = "By EOD";
                                            }  
                                            $iTransitDays=$iTransitDays+1;
                                            $szTransitDays = getBusinessDaysForCourierDeliveryDate($dtShippingDate, $iTransitDays)." ".$dtShipmentTime ;
                                        }
                                        else
                                        {  
                                            $szTransitDays = $dtShippingDate;
                                        }
                                        $szApiResponseLog .=" <br> Delivery date: ".$szTransitDays ; 
                                        $szApiResponseLog .=" <br> <br> Rate calculation: " ;

                                        $fTotalShipmentCost = 0;
                                        if($fInrExchangeRate>0)
                                        {
                                            $fTotalShipmentCost = round((float)($TotalNetCharge * $fInrExchangeRate)); 
                                        }
                                        $fShipmentCost = $TotalNetCharge;

                                        if($szCurrencyCode==__TO_CURRENCY_CONVERSION__)
                                        {
                                            $fShipmentAPIUSDCost =$fShipmentCost;
                                        }
                                        else
                                        { 
                                            $fExchangeRate=$this->getExchangeRateCourierPrice($szCurrencyCode); 
                                            $fExchangeRate = 1/$fExchangeRate; 
                                            $fShipmentAPIUSDCost =$fShipmentCost/$fExchangeRate; 
                                        }

                                        $szApiResponseLog .= "<br>Buy rate - ".$szCurrencyCode." ".number_format((float)$TotalNetCharge,2);
                                        $szApiResponseLog .= "<br>ROE - ".round($fExchangeRate,4) ;
                                        $szApiResponseLog .= "<br>Base currency - USD ".$fShipmentAPIUSDCost ; 

                                        $idForwarderCurrency = $data['idForwarderCurrency'];
                                        $miniMarkUpRate = $pricingArr[0]['fMinimumMarkup'];
                                        $fMarkupperShipment = $pricingArr[0]['fMarkupperShipment']; 

                                        $markUpByRate = $fShipmentAPIUSDCost * .01 * $pricingArr[0]['fMarkupPercent']; 

                                        if($idForwarderCurrency==1)
                                        { 
                                            $fMarkupperShipmentUSD = $fMarkupperShipment ;
                                            $miniMarkUpRateUSD = $miniMarkUpRate ;
                                            $szForwarderCurrency = 'USD';
                                        }
                                        else
                                        {
                                            $resultAry = $kWarehouseSearch->getCurrencyDetails($idForwarderCurrency);	  
                                            $szForwarderCurrency = $resultAry['szCurrency'];
                                            $fForwarderExchangeRate = $resultAry['fUsdValue'];

                                            $fMarkupperShipmentUSD = $fMarkupperShipment * $fForwarderExchangeRate ;
                                            $miniMarkUpRateUSD = $miniMarkUpRate * $fForwarderExchangeRate ; 
                                        }

                                        if($miniMarkUpRateUSD>$markUpByRate)
                                        {
                                            $fTotalMarkup = $miniMarkUpRateUSD ;
                                        }
                                        else
                                        {
                                           $fTotalMarkup =  $markUpByRate ;
                                        }
                                        $fPriceAfterMarkUp = $fShipmentAPIUSDCost + $fTotalMarkup ;

                                        $szApiResponseLog .= "<br>Markup (".$pricingArr[0]['fMarkupPercent']."%, minimum USD ".$miniMarkUpRateUSD.") – USD ".$fTotalMarkup;
                                        $szApiResponseLog .= "<br> Additional fixed markup – USD ".$fMarkupperShipmentUSD ;


                                        $fBookingprice=0;
                                        if((int)$pricingArr[0]['iBookingIncluded']!=1)
                                        { 
                                            if($data['iBookingPriceCurrency']==1)
                                            {  
                                                $iBookingPriceCurrency = 1;  
                                                $iBookingPriceCurrencyROE = 1;
                                                $szBookingPriceCurrency = 'USD';
                                                $fBookingprice = $data['fPrice']; 
                                            }
                                            else
                                            {
                                                $resultAry = $kWarehouseSearch->getCurrencyDetails($data['iBookingPriceCurrency']); 
                                                $iBookingPriceCurrencyROE = $resultAry['fUsdValue']; 
                                                $szBookingPriceCurrency = $resultAry['szCurrency']; 
                                                $iBookingPriceCurrency = $resultAry['idCurrency']; 
                                                if($iBookingPriceCurrencyROE>0)
                                                {
                                                    $fBookingprice = $data['fPrice']*$iBookingPriceCurrencyROE;
                                                }
                                                else
                                                {
                                                    $fBookingprice = 0;
                                                }
                                            }  
                                            $szApiResponseLog .= " <br> Booking and label fee for Transporteca: ".$szBookingPriceCurrency." ".$data['fPrice']."" ;
                                            $szApiResponseLog .= " <br> Booking and label fee USD: ".$fBookingprice ;
                                            $szApiResponseLog .= " <br> Booking and label fee exchange rate: ".$iBookingPriceCurrencyROE ;
                                        }  
                                        else
                                        {
                                            $szApiResponseLog .= " <br> Booking and label fee for Transporteca: No ";
                                        }
                                        $fShipmentUSDCost = $fPriceAfterMarkUp + $fMarkupperShipmentUSD + $fBookingprice;  
                                        $fMarkUpRefferalFee=$fShipmentUSDCost*.01*$data['fReferalFee'];

                                        if($data['iProfitType']==2)
                                        { 
                                            $subTotalForRefferalFee=$fShipmentUSDCost-$fBookingprice;
                                            $fMarkUpRefferalFee = ((0.01 * $data['fReferalFee'])/( 1 - (0.01*$data['fReferalFee'])) ) * $subTotalForRefferalFee;
                                            $fShipmentUSDCost = $fShipmentUSDCost + $fMarkUpRefferalFee ;
                                            $szApiResponseLog .= " <br> GP Type: Mark-up ";
                                            $szApiResponseLog .= " <br> GP Amount: USD ".($fMarkUpRefferalFee+$fBookingprice);
                                        }
                                        else
                                        {
                                            $subTotalForRefferalFee=$fShipmentUSDCost-$fBookingprice;
                                            $fMarkUpRefferalFee = $subTotalForRefferalFee*.01*$data['fReferalFee'];
                                            $szApiResponseLog .= " <br> GP Type: Referral ";
                                            $szApiResponseLog .= " <br> GP Amount: USD ".($fMarkUpRefferalFee+$fBookingprice);
                                        } 

                                        $szApiResponseLog .= "<br> Total sales price without VAT: USD ".number_format((float)$fShipmentUSDCost,2);
                                        $serviceType =$szServiceType;
                                        if($iNumDaysAddedToDelivery>0)
                                        {
                                            $deliveryDate = date('Y-m-d H:i:s',strtotime($szTransitDays . "+".$iNumDaysAddedToDelivery." days"));

                                            $timeStamp=date('H:i:s',strtotime($deliveryDate));
                                            $dayName=date('D',strtotime($deliveryDate));

                                            if($dayName=='Sun' || $dayName=='Sat')
                                            {
                                               $deliveryDate = getBusinessDaysForCourierDeliveryDate($deliveryDate, 1)." ".$timeStamp;
                                            }
                                        }
                                        else
                                        {
                                            $deliveryDate = date('Y-m-d H:i:s',strtotime($szTransitDays)); 
                                        }  

                                        $szApiResponseLog .= "<br>Delivery date ".$deliveryDate;

                                        if((int)$_SESSION['user_id']>0)
                                        {
                                            $kUser = new cUser();
                                            $kUser->getUserDetails($_SESSION['user_id']);
                                            //print_r($kUser);
                                            if($kUser->szCurrency==1)
                                            {
                                                $fVatRateAmount = round((float)$fShipmentUSDCost)*0.01*$fVatRate;   
                                                $fShipmentVatUSDCost = $fShipmentUSDCost+$fVatRateAmount;

                                                $szApiResponseLog .= "<br>VAT Percentage:".$fVatRate."%"; 
                                                $szApiResponseLog .= "<br>VAT Amount: USD ".round($fVatRateAmount,2);

                                            }
                                            else
                                            {
                                                    $szApiResponseLog .= "<br>VAT Percentage:".$fVatRate."%";
                                                    $resultAry = $kWarehouseSearch->getCurrencyDetails($kUser->szCurrency); 
                                                    $iVatPriceCurrencyROE = $resultAry['fUsdValue']; 
                                                    $szVatPriceCurrency = $resultAry['szCurrency']; 
                                                    $iVatPriceCurrency = $resultAry['idCurrency']; 
                                                    if($iVatPriceCurrencyROE>0)
                                                    {
                                                        $fVatCalAmuount = $fShipmentUSDCost/$iVatPriceCurrencyROE;
                                                    }
                                                    else
                                                    {
                                                        $fVatCalAmuount = 0;
                                                    }

                                                    $fVatRateAmount=$fVatCalAmuount*0.01*$fVatRate;

                                                    $fVatRateAmountUSD=$fVatRateAmount*$iVatPriceCurrencyROE;
                                                    $fShipmentVatUSDCost=$fShipmentUSDCost+$fVatRateAmountUSD;
                                                   // echo $fShipmentAPIUSDCost;
                                                    $szApiResponseLog .= "<br>VAT Amount: USD ".round($fVatRateAmountUSD,2);

                                                }

                                                $szApiResponseLog .= "<br> Total sales price including VAT: USD ".number_format((float)$fShipmentVatUSDCost,2);
                                        }

                                        $ret_ary[$ctr]['dtAvailableDate'] = $deliveryDate ;
                                        $ret_ary[$ctr]['szCurrencyCode'] = $szCurrencyCode ;
                                        $ret_ary[$ctr]['fShippingAmount'] = $fShipmentCost ;
                                        $ret_ary[$ctr]['szServiceType'] =  $code ; 
                                        $ret_ary[$ctr]['fShipmentAPIUSDCost'] =  round($fShipmentAPIUSDCost,2) ; 
                                        $ret_ary[$ctr]['fShipmentUSDCost'] =  round($fShipmentUSDCost,2) ; 
                                        $ret_ary[$ctr]['fDisplayPrice'] =  round($fShipmentCustomerCost,2) ; 
                                        $ret_ary[$ctr]['szCurrency'] =  $szCurrency ;
                                        $ret_ary[$ctr]['idCurrency'] =  $idCurrency ;  
                                        $ret_ary[$ctr]['markUpByRate'] =  $fTotalMarkup ; 
                                        $ret_ary[$ctr]['miniMarkUpRate'] = $miniMarkUpRate;
                                        $ret_ary[$ctr]['fPriceAfterMarkUp']=$fPriceAfterMarkUp;   
                                        $ret_ary[$ctr]['fMarkupperShipment']=$fMarkupperShipment; 
                                        $ret_ary[$ctr]['fMarkUpRefferalFee']=$fMarkUpRefferalFee;
                                        $ret_ary[$ctr]['fBookingprice']=$fBookingprice;
                                        $ret_ary[$ctr]['fReferalPercentage'] = $data['fReferalFee'] ; 
                                        $ret_ary[$ctr]['idForwarderCurrency'] = $idForwarderCurrency;
                                        $ret_ary[$ctr]['iBookingIncluded'] = $pricingArr[0]['iBookingIncluded'];
                                        ++$ctr;
                                    }
                                }
                                else
                                {
                                    $kConfig = new cConfig();
                                    $kConfig->loadCountry(false,$data['szCountry']);
                                    $countryFrom=$kConfig->szCountryName;

                                    $kConfig->loadCountry(false,$data['szSCountry']);
                                    $countryTo=$kConfig->szCountryName;

                                    $replace_ary['zsAPIName']="UPS";
                                    $replace_ary['szCurrencyCode']=$szCurrencyCode;
                                    $replace_ary['fromCountry']=$countryFrom;
                                    $replace_ary['toCountry']=$countryTo;
                                    $replace_ary['supportEmail'] = __STORE_SUPPORT_EMAIL__;

                                    createEmail(__CURRENCY_DO_NOT_MATCH__, $replace_ary,__STORE_SUPPORT_EMAIL__, '', __STORE_SUPPORT_EMAIL__,'', __STORE_SUPPORT_EMAIL__,__FLAG_FOR_DEFAULT__);

                                }
                            } 
                        }
                    } 
                    if($test_page)
                    {
                        $szReturnStr .= '</table>'; 
                        $this->szApiResponseText = $szReturnStr ;
                        $this->iSuccessMessage = 1 ;
                        $this->szApiResponseLog = $szApiResponseLog ; 
                        return true;
                    }
                    else
                    {
                        $this->szApiResponseText = $szReturnStr ;
                        $this->iSuccessMessage = 1 ;
                        $this->szApiResponseLog = $szApiResponseLog ; 
                        return $ret_ary;
                    } 
                }
            }
            else
            {
                $this->iFedexApiError = 1 ;
                $this->szApiResponseText = " <strong> UPS API Error: </strong> <br> No service available" ;
                $this->iSuccessMessage = 3 ; 
                $szApiResponseLog .= $this->szApiResponseText ; 
                $this->szApiResponseLog = $szApiResponseLog ;
                return array();
            } 
        }
        catch(Exception $ex)
        {  
            $this->iFedexApiError = 1 ;
            $this->szApiResponseText = " <strong> UPS API Error: </strong> <br> ".$ex->getMessage() ;
            $this->iSuccessMessage = 3 ;  
            $szApiResponseLog .= $this->szApiResponseText ;  
            $this->szApiResponseLog = $szApiResponseLog ;
            return array();
        }
    }
    
    function calculateRatesFromTNTApi($data,$check_auth=false,$test_page=false)
    {   
        $kCourierServices = new cCourierServices(); 
        if($check_auth)
        {
            $szCustomerXml = $this->createTntRequestXml($data,true);
        }
        else
        {
            $kCourierServices->validateInputDetails($data); 
            if($this->error==true)
            {
                return false;
            }   
            $szCustomerXml = $this->createTntRequestXml($data,false,$test_page);
        }   
        $seriverTypeArr = $this->seriverTypeArr; 
          
        if($data['szAPICode']!='')
        {
            $validServiceTypeArr=explode(";",$data['szAPICode']);
        } 
        
        $validApiCodeAry = array();
        $ctr_1=0;
        if(!empty($validServiceTypeArr))
        {
            foreach($validServiceTypeArr as $validServiceTypeArrs)
            {
                if(!empty($validServiceTypeArrs))
                {
                    $validServiceTypeAry = explode("||||",$validServiceTypeArrs);
                    $validApiCodeAry[$ctr_1] = $validServiceTypeAry[0]; 
                    
                    $validProviderIDAry[$validServiceTypeAry[0]][$ctr_1] = $validServiceTypeAry[1]; 
                    $ctr_1++;
                }
            }
        } 
        
        $apiCodeValuesAry = array();
        $apiCodeValuesAry = array_count_values($validApiCodeAry);
           
        $validServiceTypeArr = $validApiCodeAry ;
          
        $szApiResponseLog = $data['szForwarderCompanyName']." - TNT";
        
        $szTNTHostUrl = 'https://express.tnt.com/expressconnect/pricing/getprice';
        
        try
        {
            $ch = curl_init($szTNTHostUrl); 
            $szAuthString = $data['szUsername'].":".$data['szPassword'] ;
            $headers = array(
                'Content-Type:application/xml',
                'Authorization: Basic '. base64_encode($szAuthString)
            );

            $options = array(  
                CURLOPT_HTTPHEADER => $headers,  
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => trim($szCustomerXml),
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_TIMEOUT => '20L'
            ); 

            curl_setopt_array($ch, $options);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 

            $cUrlResponseAry = array();
            $cUrlResponseXml = curl_exec($ch);  
            $culrInfoAry = curl_getinfo($ch);  
             
            $fedexApiLogsAry = array(); 
            $fedexApiLogsAry['iWebServiceType'] = 3 ;
            $fedexApiLogsAry['szRequestData'] = $szCustomerXml;
            $fedexApiLogsAry['szResponseData'] = "Http Code: ".$culrInfoAry['http_code']."\n\n".$cUrlResponseXml;
            $this->addFedexApiLogs($fedexApiLogsAry);
         
            if($culrInfoAry['http_code']==100 && empty($cUrlResponseXml)) //continue
            { 
                curl_setopt_array($ch, $options);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 

                $cUrlResponseAry = array();
                $cUrlResponseXml = curl_exec($ch);  
                $culrInfoAry = curl_getinfo($ch);   
                
                $fedexApiLogsAry = array(); 
                $fedexApiLogsAry['iWebServiceType'] = 3 ;
                $fedexApiLogsAry['szRequestData'] = $szCustomerXml;
                $fedexApiLogsAry['szResponseData'] = "Http Code: ".$culrInfoAry['http_code']."\n\n".$cUrlResponseXml;
                $this->addFedexApiLogs($fedexApiLogsAry); 
                
                $bAuthenticationFlag = true;
            }
            else if($culrInfoAry['http_code']==200)
            {
                $bAuthenticationFlag = true;
            }
            else 
            { 
                $bAuthenticationFlag = false;
            }
            if($check_auth)
            { 
                /*
                 * Checking Authentication with TNT API 
                 */
                if($bAuthenticationFlag)
                {
                    return "SUCCESS";
                }
                else 
                {
                    return "ERROR";
                } 
            }
        } 
        catch (Exception $ex) 
        {
            $this->szPackageDetailsLogs .= "<br> <strong> API Response:</strong> <br><br> Status: Exception";
            $this->szPackageDetailsLogs .= " <br> Description: Please check the logs/tnt_exception.log for more details"; 
            
            $outputFileName = __APP_PATH__."/logs/tnt_exception.log";
            //save soap request and response to file
            $fw = fopen($outputFileName , 'a'); 
            fwrite($fw , "Exception: \n" . print_R($ex,true) . "\n");
            fclose($fw); 
        } 
        
        $szReturnStr =  '<h4>Your Rates: </h4><table class="format-2" style="width:90%">';
        $szReturnStr .= '<tr><td><strong>Service Type</strong></td><td><strong>Amount</strong></td><td><strong>Vat</strong></td><td><strong>Total Price</strong></td><td><strong>Shipping Date</strong></td><td><strong>Delivery Date</strong></td></tr>';
          
        
        if(!empty($cUrlResponseXml))
        {  
            $this->szPackageDetailsLogs .= "<br> <strong> API Response:</strong> <br><br> Status: SUCCESS";
            $this->szPackageDetailsLogs .= " <br> Status code: ".$culrInfoAry['http_code'].""; 

            if($this->_isValidXML($cUrlResponseXml))
            {
                $responseAry = array();  
                $responseAry = cXmlParser::createArray($cUrlResponseXml);
            } 

            if(!empty($responseAry))
            {
                $tntErrorsAry = array();
                $tntErrorsAry = $responseAry['document']['errors'];
                $bReturnFlag = false;
                $this->iSuccessMessage = 0;
                if(!empty($tntErrorsAry))
                {
                    /*
                    * There are three error types which can be returned within the errors element
                    * 1. runtimeError
                    * 2. parseError
                    * 3. brokenRule 
                    */
                    $brokenRulesAry = array();
                    $brokenRulesAry = $tntErrorsAry['brokenRule'];

                    $runtimeErrorAry = array();
                    $runtimeErrorAry = $tntErrorsAry['runtimeError'];

                    $parseErrorAry = array();
                    $parseErrorAry = $tntErrorsAry['parseError'];

                    if(!empty($runtimeErrorAry))
                    {
                        $this->szApiResponseInformationText = " <strong> ERROR: </strong> <br> ".$runtimeErrorAry['errorReason'] ;
                        $this->iSuccessMessage = 2 ;
                        $bReturnFlag = true;
                    }
                    else if(!empty($parseErrorAry))
                    {
                        $this->szApiResponseInformationText = " <strong> ERROR: </strong> <br> ".$parseErrorAry['errorReason'] ;
                        $this->iSuccessMessage = 2 ;
                        $bReturnFlag = true;
                    }
                    else if(!empty($brokenRulesAry))
                    {
                        /*
                        * 1. if messageType=E then its error and we didn't got any response so return from error block
                        * 2. if messageType=E then its Warning and we may have the response so please process furthure
                        * 3. if messageType=I then its Information and we may have the response.
                        */
                       if($brokenRulesAry['messageType']=='E')
                       {
                           $this->szApiResponseInformationText = " <strong> ERROR: </strong> <br> ".$brokenRulesAry['description'] ;
                           $this->iSuccessMessage = 2 ;
                           $bReturnFlag = true;
                       }
                       else if($brokenRulesAry['messageType']=='W')
                       {
                           $this->szApiResponseInformationText = " <strong> WARNING: </strong> <br> ".$brokenRulesAry['description'] ;
                       }
                       else if($brokenRulesAry['messageType']=='I')
                       {
                           $this->szApiResponseInformationText = "<strong> INFORMATION: </strong> <br> ".$brokenRulesAry['description'] ;
                       }
                    } 
                    if($bReturnFlag)
                    {
                        /*
                         * Returning with error messages
                         */
                        return $bReturnFlag;
                    }
                } 
                
                $priceResponseAry = array();
                $priceResponseAry = $responseAry['document']['priceResponse']['ratedServices']['ratedService']; 
                $szCurrency = $responseAry['document']['priceResponse']['ratedServices']['currency'];

                $kWarehouseSearch = new cWHSSearch();
                $kCourierService = new cCourierServices(); 
                $iNumDaysAddedToDelivery = $kWarehouseSearch->getManageMentVariableByDescription('__NUMBER_OF_DAYS_ADDED_TO_DELIVERY_DATE__'); 
                
                $courierProductMapping = array();
                $courierProductMapping = $kCourierService->getAllProductByApiCodeMapping();
                //$szApiResponseLog = '';
                $dtShippingDate = $data['dtTimingDate'];
                $ctr = 0;    

                $TntPriceResponseAry = array(); 
                if(!empty($priceResponseAry))
                {
                    $tnt_ctr=0;
                    foreach($priceResponseAry as $priceResponseArys)
                    {
                        $szSericeCode = $priceResponseArys['product']['id'];  
                        $szSericeDescription = $priceResponseArys['product']['productDesc'];  
                        if($test_page)
                        {
                            $szReturnStr .= '<tr>';
                            $szReturnStr .= '<td>'.$szSericeDescription.' ('.$szSericeCode.') </td>';
                            $szReturnStr .= '<td>'.$szCurrency.' '.$priceResponseArys['totalPriceExclVat'].' </td>';
                            $szReturnStr .= '<td>'.$szCurrency.' '.$priceResponseArys['vatAmount'].' </td>';
                            $szReturnStr .= '<td>'.$szCurrency.' '.$priceResponseArys['totalPrice'].' </td>';
                            $szReturnStr .= '<td>'.date('Y-m-d').' </td>';
                            $szReturnStr .= '<td>N/A</td>';
                            $szReturnStr .= '</tr>';
                        }
                        else
                        {
                            $szSericeCode = $priceResponseArys['product']['id'];  
                            $szSericeDescription = $priceResponseArys['product']['productDesc'];   
                            $TotalNetCharge = $priceResponseArys['totalPrice'];

                            $code = $szSericeCode; 
                            if(!empty($validServiceTypeArr) && in_array($szSericeCode,$validServiceTypeArr))
                            {
                                $iLoopCounter = $apiCodeValuesAry[$code]; 
                                if(!empty($validProviderIDAry[$code]))
                                {
                                    foreach($validProviderIDAry[$code] as $idProviderProduct)
                                    {
                                        if($idProviderProduct>0)
                                        {
                                            $TntPriceResponseAry[$tnt_ctr]['szServiceCode'] = $szSericeCode;
                                            $TntPriceResponseAry[$tnt_ctr]['szServiceDescription'] = $szSericeDescription;
                                            $TntPriceResponseAry[$tnt_ctr]['fTotalPrice'] = $TotalNetCharge ;
                                            $TntPriceResponseAry[$tnt_ctr]['idProviderProduct'] = $idProviderProduct ;
                                            $tnt_ctr++;
                                        }
                                    }
                                } 
                            }
                        }
                    }
                }   
                if($TntPriceResponseAry)
                {
                    $tnt_ctr=0;
                    foreach($TntPriceResponseAry as $priceResponseArys)
                    {
                        $szSericeCode = $priceResponseArys['szServiceCode'];  
                        $szSericeDescription = $priceResponseArys['szServiceDescription'];   
                        $idCourierProviderProductid = $priceResponseArys['idProviderProduct']; 
                        $code = $szSericeCode; 

                        if(!empty($validServiceTypeArr) && in_array($szSericeCode,$validServiceTypeArr))
                        { 
                            $pricingArr = $kCourierService->getCourierProductProviderPricingData($data,$szSericeCode,$idCourierProviderProductid);

                            $iTransitDays = 0;   
                            $TotalNetCharge = $priceResponseArys['fTotalPrice'];

                            $iTransitDays = $courierProductMapping[$code]['iDaysStd'];  
                            $iTransitDays=$iTransitDays;
                            $szTransitDays = getBusinessDaysForCourierDeliveryDate($dtShippingDate, $iTransitDays)." ".$dtShipmentTime ;

                            $szApiResponseLog .=" <br><br> Working On Sertive Type: ".$code; 
                            $szApiResponseLog .=" <br> Shipment Price: ".$szCurrency." ".number_format((float)$TotalNetCharge,2);
                            $szApiResponseLog .=" <br> Delivery date: ".$szTransitDays; 
                            $szApiResponseLog .=" <br> Transit Days: ".$iTransitDays;  
                            $szApiResponseLog .=" <br> <br><u> Rate calculation: </u><br><br> " ;

                            $fShipmentCost = $TotalNetCharge ;
                            $szCurrencyCode = $szCurrency ;

                            if($szCurrency==__TO_CURRENCY_CONVERSION__)
                            {
                                $fShipmentAPIUSDCost = $fShipmentCost;
                                $fExchangeRate = 1;
                            }
                            else
                            { 
                                $fExchangeRate = $kCourierService->getExchangeRateCourierPrice($szCurrency); 
                                $fExchangeRate = 1/$fExchangeRate; 

                                if($fExchangeRate>0)
                                {
                                    $fShipmentAPIUSDCost = $TotalNetCharge/$fExchangeRate; 
                                }
                            } 

                            $szApiResponseLog .= "<br>Buy rate - ".$szCurrency." ".number_format((float)$TotalNetCharge,2);
                            $szApiResponseLog .= "<br>ROE - ".round($fExchangeRate,4) ;
                            $szApiResponseLog .= "<br>Base currency - USD ".$TotalNetChargeUsd ;  

                            $idForwarderCurrency = $data['idForwarderCurrency'];
                            $miniMarkUpRate = $pricingArr[0]['fMinimumMarkup'];
                            $fMarkupperShipment = $pricingArr[0]['fMarkupperShipment']; 

                            $markUpByRate = $fShipmentAPIUSDCost * .01 * $pricingArr[0]['fMarkupPercent']; 

                            if($idForwarderCurrency==1)
                            { 
                                $fMarkupperShipmentUSD = $fMarkupperShipment ;
                                $miniMarkUpRateUSD = $miniMarkUpRate ;
                                $szForwarderCurrency = 'USD';
                            }
                            else
                            {
                                $resultAry = $kWarehouseSearch->getCurrencyDetails($idForwarderCurrency);	  
                                $szForwarderCurrency = $resultAry['szCurrency'];
                                $fForwarderExchangeRate = $resultAry['fUsdValue'];

                                $fMarkupperShipmentUSD = $fMarkupperShipment * $fForwarderExchangeRate ;
                                $miniMarkUpRateUSD = $miniMarkUpRate * $fForwarderExchangeRate ; 
                            }

                            if($miniMarkUpRateUSD>$markUpByRate)
                            {
                                $fTotalMarkup = $miniMarkUpRateUSD ;
                            }
                            else
                            {
                               $fTotalMarkup =  $markUpByRate ;
                            }
                            $fPriceAfterMarkUp = $fShipmentAPIUSDCost + $fTotalMarkup ; 
                            $szApiResponseLog .= "<br>Markup (".$pricingArr[0]['fMarkupPercent']."%, minimum USD ".$miniMarkUpRateUSD.") – USD ".$fTotalMarkup;
                            $szApiResponseLog .= "<br> Additional fixed markup – USD ".$fMarkupperShipmentUSD ;

                            $fBookingprice=0;
                            if((int)$pricingArr[0]['iBookingIncluded']!=1)
                            { 
                                if($data['iBookingPriceCurrency']==1)
                                {  
                                    $iBookingPriceCurrency = 1;  
                                    $iBookingPriceCurrencyROE = 1;
                                    $szBookingPriceCurrency = 'USD';
                                    $fBookingprice = $data['fPrice']; 
                                }
                                else
                                {
                                    $resultAry = $kWarehouseSearch->getCurrencyDetails($data['iBookingPriceCurrency']); 
                                    $iBookingPriceCurrencyROE = $resultAry['fUsdValue']; 
                                    $szBookingPriceCurrency = $resultAry['szCurrency']; 
                                    $iBookingPriceCurrency = $resultAry['idCurrency']; 
                                    if($iBookingPriceCurrencyROE>0)
                                    {
                                        $fBookingprice = $data['fPrice']*$iBookingPriceCurrencyROE;
                                    }
                                    else
                                    {
                                        $fBookingprice = 0;
                                    }
                                }  
                                $szApiResponseLog .= " <br> Booking and label fee for Transporteca: ".$szBookingPriceCurrency." ".$data['fPrice']."" ;
                                $szApiResponseLog .= " <br> Booking and label fee USD: ".$fBookingprice ;
                                $szApiResponseLog .= " <br> Booking and label fee exchange rate: ".$iBookingPriceCurrencyROE ;
                            }  
                            else
                            {
                                $szApiResponseLog .= " <br> Booking and label fee for Transporteca: No ";
                            }
                            $fShipmentUSDCost = $fPriceAfterMarkUp + $fMarkupperShipmentUSD + $fBookingprice;  
                            $fMarkUpRefferalFee=$fShipmentUSDCost*.01*$data['fReferalFee'];

                            if($data['iProfitType']==2)
                            { 
                                $subTotalForRefferalFee=$fShipmentUSDCost-$fBookingprice;
                                $fMarkUpRefferalFee = ((0.01 * $data['fReferalFee'])/( 1 - (0.01*$data['fReferalFee'])) ) * $subTotalForRefferalFee;
                                $fShipmentUSDCost = $fShipmentUSDCost + $fMarkUpRefferalFee ;
                                $szApiResponseLog .= " <br> GP Type: Mark-up ";
                                $szApiResponseLog .= " <br> GP Amount: USD ".($fMarkUpRefferalFee+$fBookingprice);
                            }
                            else
                            {
                                $subTotalForRefferalFee=$fShipmentUSDCost-$fBookingprice;
                                $fMarkUpRefferalFee = $subTotalForRefferalFee*.01*$data['fReferalFee'];
                                $szApiResponseLog .= " <br> GP Type: Referral ";
                                $szApiResponseLog .= " <br> GP Amount: USD ".($fMarkUpRefferalFee+$fBookingprice);
                            } 

                            $szApiResponseLog .= "<br> Total sales price without VAT: USD ".number_format((float)$fShipmentUSDCost,2)."<br><br>";

                            $serviceType =$szServiceType;
                            if($iNumDaysAddedToDelivery>0)
                            {
                                $deliveryDate = date('Y-m-d H:i:s',strtotime($szTransitDays . "+".$iNumDaysAddedToDelivery." days"));

                                $timeStamp=date('H:i:s',strtotime($deliveryDate));
                                $dayName=date('D',strtotime($deliveryDate));

                                if($dayName=='Sun' || $dayName=='Sat')
                                {
                                   $deliveryDate = getBusinessDaysForCourierDeliveryDate($deliveryDate, 1)." ".$timeStamp;
                                }
                            }
                            else
                            {
                                $deliveryDate = date('Y-m-d H:i:s',strtotime($szTransitDays)); 
                            }  

                            $szApiResponseLog .= "<br>Delivery date ".$deliveryDate;

                            if((int)$_SESSION['user_id']>0)
                            {
                                $kUser = new cUser();
                                $kUser->getUserDetails($_SESSION['user_id']);
                                //print_r($kUser);
                                if($kUser->szCurrency==1)
                                {
                                    $fVatRateAmount = round((float)$fShipmentUSDCost)*0.01*$fVatRate;   
                                    $fShipmentVatUSDCost = $fShipmentUSDCost+$fVatRateAmount;

                                    $szApiResponseLog .= "<br>VAT Percentage:".$fVatRate."%"; 
                                    $szApiResponseLog .= "<br>VAT Amount: USD ".round($fVatRateAmount,2);

                                }
                                else
                                {
                                    $szApiResponseLog .= "<br>VAT Percentage:".$fVatRate."%";
                                    $resultAry = $kWarehouseSearch->getCurrencyDetails($kUser->szCurrency); 
                                    $iVatPriceCurrencyROE = $resultAry['fUsdValue']; 
                                    $szVatPriceCurrency = $resultAry['szCurrency']; 
                                    $iVatPriceCurrency = $resultAry['idCurrency']; 
                                    if($iVatPriceCurrencyROE>0)
                                    {
                                        $fVatCalAmuount = $fShipmentUSDCost/$iVatPriceCurrencyROE;
                                    }
                                    else
                                    {
                                        $fVatCalAmuount = 0;
                                    }

                                    $fVatRateAmount=$fVatCalAmuount*0.01*$fVatRate;

                                    $fVatRateAmountUSD=$fVatRateAmount*$iVatPriceCurrencyROE;
                                    $fShipmentVatUSDCost=$fShipmentUSDCost+$fVatRateAmountUSD;
                                   // echo $fShipmentAPIUSDCost;
                                    $szApiResponseLog .= "<br>VAT Amount: USD ".round($fVatRateAmountUSD,2); 
                                } 
                                $szApiResponseLog .= "<br> Total sales price including VAT: USD ".number_format((float)$fShipmentVatUSDCost,2);
                            }

                            $ret_ary[$ctr]['idCourierProviderProduct'] = $idCourierProviderProductid;
                            $ret_ary[$ctr]['dtAvailableDate'] = $deliveryDate ;
                            $ret_ary[$ctr]['szCurrencyCode'] = $szCurrencyCode ;
                            $ret_ary[$ctr]['fShippingAmount'] = $fShipmentCost ;
                            $ret_ary[$ctr]['szServiceType'] =  $code ; 
                            $ret_ary[$ctr]['fShipmentAPIUSDCost'] =  round($fShipmentAPIUSDCost,2) ; 
                            $ret_ary[$ctr]['fShipmentUSDCost'] =  round($fShipmentUSDCost,2) ; 
                            $ret_ary[$ctr]['fDisplayPrice'] =  round($fShipmentCustomerCost,2) ; 
                            $ret_ary[$ctr]['szCurrency'] =  $szCurrency ;
                            $ret_ary[$ctr]['idCurrency'] =  $idCurrency ;  
                            $ret_ary[$ctr]['markUpByRate'] =  $fTotalMarkup ; 
                            $ret_ary[$ctr]['miniMarkUpRate'] = $miniMarkUpRate;
                            $ret_ary[$ctr]['fPriceAfterMarkUp']=$fPriceAfterMarkUp;   
                            $ret_ary[$ctr]['fMarkupperShipment']=$fMarkupperShipment; 
                            $ret_ary[$ctr]['fMarkUpRefferalFee']=$fMarkUpRefferalFee;
                            $ret_ary[$ctr]['fBookingprice']=$fBookingprice;
                            $ret_ary[$ctr]['fReferalPercentage'] = $data['fReferalFee'] ; 
                            $ret_ary[$ctr]['idForwarderCurrency'] = $idForwarderCurrency;
                            $ret_ary[$ctr]['idProviderProduct'] = $idCourierProviderProductid;
                            $ret_ary[$ctr]['iBookingIncluded'] = $pricingArr[0]['iBookingIncluded']; 
                            ++$ctr;
                        } 
                    }
                }   
                if($test_page)
                { 
                    $szReturnStr .= "</table>"; 
                    $this->szApiResponseText = $szReturnStr ;
                    $this->iSuccessMessage = 1 ;
                    return true;
                }
                else
                {
                    $this->iSuccessMessage = 1 ;
                    $this->szApiResponseLog = $szApiResponseLog;   
                    return $ret_ary; 
                }  
            }  
        }
    } 
    
    //check if xml is valid document
    public function _isValidXML($xml) {
        $doc = @simplexml_load_string($xml);
        if ($doc) {
            return true; //this is valid
        } else {
            return false; //this is not valid
        }
    }
    
    function getdetailsFromCourierAPI($data)
    {
        if(!empty($data))
        {       
            $kCourierServices = new cCourierServices();  
            $kEasyPost = new cEasyPost();
            $resArr = $kEasyPost->getAllCarrierResponseFromEasypostAPI($data); 
            
//            $kPostmen = new cPostmen();
//            $resArr = $kPostmen->getAllCarrierResponseFromPostmenAPI($data); 

            $this->szPackageDetailsLogs = $kPostmen->szApiResponseInformationText."<br><br>". $kPostmen->szPackageDetailsLogs;
            $this->szApiResponseLog = $kPostmen->szApiResponseLog; 
            
            if(!empty($resArr))
            {
                $resArr = sortArray($resArr, 'fShipmentUSDCost');
            }  
            return $resArr ; 
            
            /*
             *
             * 
            if($data['idCourierProvider']==__FEDEX_API__)
            {   
                $strdata=array();
                $strdata[0]=" \n\n *** Started Calling Fedex ".date('Y-m-d H:i:s')." \n\n"; 
                $resArr = $kCourierServices->calculateShippingFedexDetails($data);
                $strdata[1] = " Got response of FedEx: ".date('Y-m-d H:i:s')." \n\n"; 
                file_log(true, $strdata, $filename);

                $this->szPackageDetailsLogs = $kCourierServices->szPackageDetailsLogs;
                $this->szApiResponseLog = $kCourierServices->szApiResponseLog;
            }
            else if($data['idCourierProvider']==__UPS_API__)
            { 
                $strdata=array();
                $strdata[0]=" \n\n *** Started Calling UPS ".date('Y-m-d H:i:s')." \n\n"; 
                $resArr = $kCourierServices->calculateRatesFromUpsNewApi($data); 
                $strdata[1] = " Got response of UPS: ".date('Y-m-d H:i:s')." \n\n"; 
                file_log(true, $strdata, $filename);

                $this->szPackageDetailsLogs = $kCourierServices->szPackageDetailsLogs;
                $this->szApiResponseLog = $kCourierServices->szApiResponseLog;
            }
            else
            */
        }
    }
    
    function processTrack($data)
    {
        //create soap request
        $req['RequestOption'] = '15';
        $tref['CustomerContext'] = 'UPS Booking';
        $req['TransactionReference'] = $tref;
        $request['Request'] = $req;
        $request['InquiryNumber'] = $data['szTrackingNumber'];
        $request['TrackingOption'] = '02'; 
        return $request; 
    }
  
    function getTrackingDetailsFromUPS($data)
    {
        if(!empty($data))
        {
            try
            {  
                //Configuration
                $szAccountNumber = $data['szAccountNumber'];
                $szUsername = $data['szUsername'];
                $szPassword = $data['szPassword']; 
        
                $wsdl = __APP_PATH__."/wsdl/Track.wsdl";
                $operation = "ProcessTrack";
                $endpointurl = 'https://wwwcie.ups.com/webservices/Track';
                $outputFileName = "XOLTResult.xml"; 
    
                $mode = array
                (
                  'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
                  'trace' => 1
                );

                // initialize soap client
                $client = new SoapClient($wsdl , $mode);

                //set endpoint url
                $client->__setLocation($endpointurl);
 
                //create soap header
                $usernameToken['Username'] = $szUsername;
                $usernameToken['Password'] = $szPassword;
                $serviceAccessLicense['AccessLicenseNumber'] = $szAccountNumber;
                $upss['UsernameToken'] = $usernameToken;
                $upss['ServiceAccessToken'] = $serviceAccessLicense;

                $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0','UPSSecurity',$upss);
                $client->__setSoapHeaders($header);

                //get response
                $resp = $client->__soapCall($operation ,array($this->processTracking($data['szTrackingNumber'])));

                //get status
                //echo "Response Status: " . $resp->Response->ResponseStatus->Description ."\n";
                $resArr=toArray($resp);   
                print_r($resArr);
                
                $trackingRetAry = array();
                if($resArr ['Shipment']['Package']['Activity']['Status']['Description']!='')
                {
                    $trackingStatusArr=$resArr ['Shipment']['Package']['Activity']['Status'];
                    
                    $trackingRetAry['Description'] = $trackingStatusArr['Description'];
                    $trackingRetAry['Code'] = $trackingStatusArr['Code']; 
                }
                else if($resArr ['Shipment']['Package']['Status']['Description']!='')
                {
                    $trackingStatusArr=$resArr ['Shipment']['Package']['Status'];
                    
                    $trackingRetAry['Description'] = $trackingStatusArr['Description'];
                    $trackingRetAry['Code'] = $trackingStatusArr['Code'];  
                } 
                //save soap request and response to file
                $fw = fopen($outputFileName , 'w');
                fwrite($fw , "Request: \n" . $client->__getLastRequest() . "\n");
                fwrite($fw , "Response: \n" . $client->__getLastResponse() . "\n");
                fclose($fw);  
                
                return $trackingRetAry;
            }
            catch(Exception $ex)
            {
                print_r ($ex);
            }
        }
    }
    function processTracking($szTrackingNumber)
    {
        //create soap request
        $req = array();
        $req['RequestOption'] = '15';
        $tref['CustomerContext'] = 'UPS Booking';
        $req['TransactionReference'] = $tref;
        $request['Request'] = $req;
        $request['InquiryNumber'] = $szTrackingNumber;
        $request['TrackingOption'] = '02';  
        return $request;
    }
    
    function getTrackingDetailsFromTNT($data)
    {
        if(!empty($data))
        {
            $szRequestXml = '
                <?xml version="1.0" encoding="UTF-8" standalone="no"?>
                <TrackRequest> 
                    <SearchCriteria marketType="INTERNATIONAL" originCountry="'.$data['szOriginCountry'].'">
                        <ConsignmentNumber>'.$data['szTrackingNumber'].'</ConsignmentNumber> 
                        <Account>
                            <Number>'.$data['szAccountNumber'].'</Number>
                            <CountryCode>DK</CountryCode>
                        </Account>   
                    </SearchCriteria>
                    <LevelOfDetail>
                        <Complete/>
                    </LevelOfDetail>   
                </TrackRequest>
            ';  
            $szTNTHostUrl = 'https://express.tnt.com/expressconnect/track.do';

            $ch = curl_init($szTNTHostUrl); 
            $szAuthString = $data['szUsername'].":".$data['szPassword'] ;
            $headers = array(
                'Content-Type:application/xml',
                'Authorization: Basic '. base64_encode($szAuthString)
            );

            $szRequestXml = urlencode($szRequestXml);
            $options = array(  
                CURLOPT_HTTPHEADER => $headers,  
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => trim($szRequestXml),
                CURLOPT_RETURNTRANSFER => 1
            ); 

            curl_setopt_array($ch, $options);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 

            $cUrlResponseAry = array();
            $cUrlResponseXml = curl_exec($ch);   
            
            $culrInfoAry = curl_getinfo($ch);  
            
            if(!empty($cUrlResponseXml))
            { 
                if($this->_isValidXML($cUrlResponseXml))
                {
                    $responseAry = array();  
                    $responseAry = cXmlParser::createArray($cUrlResponseXml); 
                }
                
                $trackingResponseAry = array();
                if(!empty($responseAry['TrackResponse']))
                {
                    if($responseAry['TrackResponse']['SummaryCode']=='INT')
                    {
                        $trackingResponseAry['Description'] = 'In Transit';
                        $trackingResponseAry['Code'] = 'INT';
                        return $trackingResponseAry;
                    }
                    else if($responseAry['TrackResponse']['SummaryCode']=='DEL')
                    {
                        $trackingResponseAry['Description'] = 'Delivered';
                        $trackingResponseAry['Code'] = 'INT';
                        return $trackingResponseAry; 
                    }
                    else
                    {
                        $trackingResponseAry['Description'] = 'N/A';
                        $trackingResponseAry['Code'] = $responseAry['TrackResponse']['SummaryCode'];
                        return $trackingResponseAry; 
                    }
                }
                else
                {
                    return false;
                }
            }  
        } 
    }
    
    public function send($xml)
    {     
        $data = array ('xmlRequest' => $xml,  'username' => $this->username, 'password' => $this->password);

        $data = http_build_query($data);
        $data = str_replace('&amp;','&',$data);
        $data = str_replace('+','%20',$data) ;
        $data = str_replace('%2F','/',$data) ;
        echo '<!--'.$xml.'-->';
        $TNTURL = $this->requestURL;
        $ch = curl_init($TNTURL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        $output = curl_exec($ch);

        return $output;
        curl_close($ch);    
    } 
    
    function getShipmentDetailsFromCourierTrackingAPI($data)
    {
        if(!empty($data))
        { 
            $kCourierServices = new cCourierServices(); 
            if($data['idCourierProvider']==__FEDEX_API__)
            {   
                $resArr = $kCourierServices->calculateShippingFedexDetails($data);
                 
                $this->szPackageDetailsLogs = $kCourierServices->szPackageDetailsLogs;
                $this->szApiResponseLog = $kCourierServices->szApiResponseLog;
            }
            else if($data['idCourierProvider']==__UPS_API__)
            { 
                $resArr = $kCourierServices->calculateRatesFromUpsNewApi($data); 
                
                $this->szPackageDetailsLogs = $kCourierServices->szPackageDetailsLogs;
                $this->szApiResponseLog = $kCourierServices->szApiResponseLog;
            } 
            else if($data['idCourierProvider']==__TNT_API__)
            { 
                $resArr = $this->calculateRatesFromTNTApi($data); 
            }   
            return $resArr ;
        }
    } 
    function getCourierTrackingNotification($data,$iType)
    {
        if(!empty($data))
        { 
            $kCourierServices = new cCourierServices(); 
            if($iType==__FEDEX_API__)
            {   
                //$resArr = $kCourierServices->calculateShippingFedexDetails($data);
                 
                $this->szPackageDetailsLogs = $kCourierServices->szPackageDetailsLogs;
                $this->szApiResponseLog = $kCourierServices->szApiResponseLog;
            }
            else if($iType==__UPS_API__)
            { 
                $resArr = $kCourierServices->calculateRatesFromUpsNewApi($data); 
                
                $this->szPackageDetailsLogs = $kCourierServices->szPackageDetailsLogs;
                $this->szApiResponseLog = $kCourierServices->szApiResponseLog;
            } 
            else if($data['idCourierProvider']==__TNT_API__)
            { 
                $resArr = $this->calculateRatesFromTNTApi($data); 
            }  
            return $resArr ;
        }
    }
    
    function calculateRatesFromCourierApibyThread($data)
    {
        $kCourierServices = new cCourierServices();  
        $kCourierServices->validateInputDetails($data); 
        if($this->error==true)
        {
            return false;
        }     
        $kFedExAPI = new cCourierAPI(1,$data);
        
        $data['szUsername'] = 'hXuU27wq';
        $data['szAccountNumber'] = '7CEF991B40A35795';
        $data['szPassword'] = '405y713L';
        $kUPSAPI = new cCourierAPI(2,$data);
        
        $data['szUsername'] = 'TransportT';
        $data['szAccountNumber'] = '2013579';
        $data['szPassword'] = 'tnt1234t'; 
        $kTNTAPI = new cCourierAPI(3,$data); 
         
//        if ($kFedExAPI->start()) 
//        { 
//            /* do some work */ 
//            /* ensure we have data */
//            $kFedExAPI->join(); 
//            /* we can now manipulate the response */
//            var_dump($kFedExAPI->szApiResponseText);
//        }
        try
        {
            $kFedExAPI->start();
            $kUPSAPI->start();
            $kTNTAPI->start();
            
            while($kFedExAPI->isAlive() && $kUPSAPI->isAlive() && $kTNTAPI->isAlive())
            { 
                sleep(1);
            }
        } 
        catch (Exception $ex) 
        {

        } 
    }   
    
    
    function createOriginAddressObject($kCourierServices,$iCreateLabel=false)
    {
        if($iCreateLabel)
        {
            // create addresses
            $from_address_params = array(  
                "name"    => "Morten Laerkholm",
                "street1" => "388 Townsend St",
                "street2" => "Apt 20",
                "city"    => $kCourierServices->szCity, 
                "zip"     => $kCourierServices->szPostcode,
                "country" => $kCourierServices->szCountry,
                "phone"   => "323-855-0394"
            ); 
        }
        else
        {
            // create addresses
            $from_address_params = array(   
                "zip"     => $kCourierServices->szPostcode,
                "country" => $kCourierServices->szCountry 
            ); 
        } 
        if($kCourierServices->szCountry=='US')
        {
            $kEasyPost = new cEasyPost();
            $stateSearchAry = $from_address_params;
            $stateSearchAry['city'] = $kCourierServices->szCity;
            $szState = $kEasyPost->getUsaStates($stateSearchAry);
            $from_address_params['state'] = $szState; 
        } 
        //return \EasyPost\Address::create($from_address_params);
        return $from_address_params;
    }
    
    function createDestinationAddressObject($kCourierServices,$iCreateLabel=false)
    { 
        if($iCreateLabel)
        {
            $to_address_params =   array(
                "name"    => "Ajay Jha",
                "street1" => "618 FF-4",
                "street2" => "NK-2",
                "city"    => $kCourierServices->szSCity, 
                "zip"     => $kCourierServices->szSPostcode,
                "country" => $kCourierServices->szSCountry,
                "phone"   => "+91-9911466405"
            );
        }
        else
        {
            $to_address_params =   array( 
                "zip"     => $kCourierServices->szSPostcode,
                "country" => $kCourierServices->szSCountry 
            );
        } 
        if($kCourierServices->szSCountry=='US')
        {
            $kEasyPost = new cEasyPost();
            $stateSearchAry = $to_address_params;
            $stateSearchAry['city'] = $kCourierServices->szSCity;
            $szState = $kEasyPost->getUsaStates($stateSearchAry);
            $to_address_params['state'] = $szState;
        } 
        return $to_address_params; 
    }
    
    function createParcelObject($kCourierServices)
    {
        /*
        * Coverting dimensions to INCHES
        */
        $fCargoLength = 0.393701 * $kCourierServices->fCargoLength ;
        $fCargoWidth = 0.393701 * $kCourierServices->fCargoWidth ;
        $fCargoHeight = 0.393701 * $kCourierServices->fCargoHeight ;
        
        $fCargoWeight = 35.274 * $kCourierServices->fCargoWeight ; //Converting this OUNCES
        
        // create parcel
        $parcel_params = array("length"             => round_up($fCargoLength,1),
                               "width"              => round_up($fCargoWidth,1),
                               "height"             => round_up($fCargoHeight,1), 
                               "weight"             => round_up($fCargoWeight,1) 
        ); 
        return $parcel_params;
    }
    
    function getCourierApiDetailsFromEasypost($data,$iCreateLabel=false)
    {  
        $kCourierServices = new cCourierServices();  
        $kCourierServices->validateInputDetails($data);
        if($this->error==true)
        {
            return false;
        } 
        $kWhsSearch = new cWHSSearch();
        $szEasypostApi = $kWhsSearch->getManageMentVariableByDescription('__EASYPOST_API_KEY__');
        \EasyPost\EasyPost::setApiKey($szEasypostApi); 
        //\EasyPost\EasyPost::setApiKey('DtJtSi7Vvh2TSLigmzNqtw');
         
        $from_address = $this->createOriginAddressObject($kCourierServices,$iCreateLabel);
        $to_address = $this->createDestinationAddressObject($kCourierServices,$iCreateLabel);  
        $parcel = $this->createParcelObject($kCourierServices,$iCreateLabel);  
            
        if($iCreateLabel==1)
        {
            try
            { 
               $fCargoWeight = 35.274 * $kCourierServices->fCargoWeight; 
                // customs info form
                $customs_item_params = array(
                    "description"      => "Many, many EasyPost stickers.",
                    "hs_tariff_number" => 123456,
                    "origin_country"   => $kCourierServices->szCountry,
                    "quantity"         => 1,
                    "value"            => 879.47,
                    "weight"           => $fCargoWeight
                );
                //$customs_item = \EasyPost\CustomsItem::create($customs_item_params);

                $customs_info_params = array(
                    "customs_certify"      => true,
                    "customs_signer"       => "Borat Sagdiyev",
                    "contents_type"        => "gift",
                    "contents_explanation" => "", // only necessary for contents_type=other
                    "eel_pfc"              => "NOEEI 30.37(a)",
                    "non_delivery_option"  => "abandon",
                    "restriction_type"     => "none",
                    "restriction_comments" => "",
                    "scan_form" => "2015-11-22T05:39:56Z", 
                    "customs_items"        => array($customs_item_params)
                );
                //$customs_info = \EasyPost\CustomsInfo::create($customs_info_params);
                
                // create shipment
                $shipment_params = array(
                    "from_address" => $from_address,
                    "to_address"   => $to_address,
                    "parcel"       => $parcel,
                    "customs_info" => $customs_info_params 
                );   
                
                $shipment = \EasyPost\Shipment::create($shipment_params); 
                $shipment->buy(array('rate' => $shipment->lowest_rate()));  
                
                $fedexApiLogsAry = array(); 
                $fedexApiLogsAry['iWebServiceType'] = 5 ;
                $fedexApiLogsAry['szRequestData'] = print_R($shipment_params,true);
                $fedexApiLogsAry['szResponseData'] = print_R($shipment,true);
                $this->addFedexApiLogs($fedexApiLogsAry);
                
                $szShippingNumber = $shipment->tracker->shipment_id;
                $szTrackingNumber = $shipment->tracker->tracking_code;
               // echo "<br> Shipping Number: ".$szShippingNumber." <br> Tracking Number: ".$szTrackingNumber;
                
                if(!empty($shipment->postage_label->label_url))
                {
                    $this->iSuccessMessage = 3 ;
                    $this->szShippingLabelUrl = $shipment->postage_label->label_url;
                }  
                $shipment = \EasyPost\Shipment::retrieve($szShippingNumber);
                $shipment->label(array('file_format' => 'pdf')); 
                  
                if(!empty($shipment->postage_label->label_pdf_url))
                {
                    $this->iSuccessMessage = 3 ;
                    $this->szShippingLabelUrl = $shipment->postage_label->label_pdf_url; 
                }
                return true;
                
            } catch (Exception $ex) { 
               $this->szApiResponseInformationText = print_R($ex,true);
               $this->iSuccessMessage = 2 ;
           }
           return true;
        }
        else
        {
            // create shipment
            $shipment_params = array(
                "from_address" => $from_address,
                "to_address"   => $to_address,
                "parcel"       => $parcel,
                "min_datetime" => '2015-11-22T05:39:56Z'
            );
            //constinue to work on rates
            $shipment = \EasyPost\Shipment::create($shipment_params);
            
            // get shipment rates - optional, rates are added to the obj when it's created if addresses and parcel are present
            if (count($shipment->rates) === 0) {
                $shipment->get_rates(); 
            } 
            
            $fedexApiLogsAry = array(); 
            $fedexApiLogsAry['iWebServiceType'] = 6 ;
            $fedexApiLogsAry['szRequestData'] = print_R($shipment_params,true);
            $fedexApiLogsAry['szResponseData'] = print_R($shipment,true);
            $this->addFedexApiLogs($fedexApiLogsAry);
        }
        
        $kObject = new \EasyPost\Object();  
        $kObject->_values = $shipment; 
        $responseAry = array();
        $responseAry = $kObject->__toArray(true);
        
        if(!empty($responseAry['_values']['messages']))
        { 
            $responseMessageAry = array();
            $responseMessageAry = $responseAry['_values']['messages'] ;
            if(!empty($responseMessageAry))
            {
                $szErrorReturnStr =  '<h4>API Notification</h4><table class="format-2" style="width:90%">';
                $szErrorReturnStr .= '<tr><td style="width:15%;"><strong>Type</strong></td><td style="width:15%;"><strong>Carrier</strong></td><td style="width:70%;"><strong>Message</strong></td></tr>';
                
                foreach($responseMessageAry as $responseMessageArys)
                {
                    $szErrorReturnStr .= '<tr>';
                    $szErrorReturnStr .= '<td>'.$responseMessageArys['type'].' </td>';
                    $szErrorReturnStr .= '<td>'.$responseMessageArys['carrier'].'</td>';
                    $szErrorReturnStr .= '<td>'.$responseMessageArys['message'].'</td>'; 
                    $szErrorReturnStr .= '</tr>';
                }
            } 
            $szErrorReturnStr .= "</table>"; 
            $this->szApiResponseInformationText = $szErrorReturnStr;
            $this->iSuccessMessage = 2 ;
        }    
        //echo $szErrorReturnStr;  
        //print_R($shipment);
        if(count($shipment->rates) > 0)
        {
            $kObject->_values = $shipment->rates ;
            $apiResponseAry = array();
            $responseAry = array();
            $responseAry = $kObject->__toArray(true);

            $apiResponseAry = $responseAry['_values']; 
            
            if(!empty($apiResponseAry))
            {
                $szReturnStr =  '<h4>Your Rates: </h4><table class="format-2" style="width:90%">';
                $szReturnStr .= '<tr><td><strong>Account ID</strong></td><td><strong>Carrier</strong></td><td><strong>Service Type</strong></td><td><strong>Total Price</strong></td><td><strong>Shipping Date</strong></td><td><strong>Delivery Date</strong></td></tr>';
                foreach($apiResponseAry as $apiResponseArys)
                {
                    $szReturnStr .= '<tr>';
                    $szReturnStr .= '<td>'.$apiResponseArys['carrier_account_id'].' </td>';
                    $szReturnStr .= '<td>'.$apiResponseArys['carrier'].'</td>';
                    $szReturnStr .= '<td>'.$apiResponseArys['service'].'</td>';
                    $szReturnStr .= '<td>'.$apiResponseArys['currency'].' '.$apiResponseArys['rate'].' </td>';  
                    $szReturnStr .= '<td>'.date('Y-m-d').' </td>';

                    if(!empty($apiResponseArys['delivery_date']))
                    {
                        $szReturnStr .= '<td>'.date('Y-m-d',strtotime($apiResponseArys['delivery_date'])).' </td>';
                    }
                    else if($apiResponseArys['delivery_days']>0)
                    {
                        $szReturnStr .= '<td>'.date('Y-m-d',strtotime("+".(int)$apiResponseArys['delivery_days']." DAYS")).'</td>';
                    }
                    else
                    {
                        $szReturnStr .= '<td>N/A</td>';
                    }  
                    $szReturnStr .= '</tr>';
                }
                
//                $szReturnStr .= '<tr>';
//                $szReturnStr .= '<td colspan="6" style="float:right"><input type="button" onclick="submit_courier_form(1);" value="Create Label" name="create_label"></td></tr>';
                

                $szReturnStr .= "</table>"; 
                $this->szApiResponseText = $szReturnStr ;
                $this->iSuccessMessage = 1 ;
                return true;
            }
            else
            {
                $this->szApiResponseInformationText .= " <strong> ERROR: </strong> <br> No Response From API ";
                $this->iSuccessMessage = 2 ;
                return true;
            }
        } 
        else
        {
            $this->szApiResponseInformationText .= " <strong> ERROR: </strong> <br> No Response From API ";
            $this->iSuccessMessage = 2 ;
            return true;
        }
    } 
    
    function checkCourierApiCredentials($carrierAgreementAry,$bForwarderFlag=false)
    {  
        $bCourierValidationFlag = false; 
        if($carrierAgreementAry['idCourierProvider']==__FEDEX_API__)
        {
            $responseTag = checkFedexLoginDetails($carrierAgreementAry);  
        }
        else if($carrierAgreementAry['idCourierProvider']==__UPS_API__)
        {
            $responseTag = checkUPSLoginDetails($carrierAgreementAry); 
        }
        else if($carrierAgreementAry['idCourierProvider']==__DHL_API__)
        {
            $responseTag = checkDHLLoginDetails($carrierAgreementAry); 
        }
        else
        { 
            $responseTag = $this->calculateRatesFromTNTApi($carrierAgreementAry,true);    
        } 
         
        if($responseTag=='SUCCESS')
        {
            /*
            * API credential are valid for TNT API
            */
            $bCourierValidationFlag = true;
        }
        else
        {
            /*
            * Invalid API credential for TNT API
            */
            $bCourierValidationFlag = false;
            $this->iApiCredentialsNotValid = 1;
            $this->szSpecialErrorMesage = t($this->t_base.'errormsg/api_login_error');
        }

        if($bForwarderFlag)
        {
            /*
            * i. When forwarder do ‘check login’ then we call TNT api to check the credential and if credentials are valid then we allowed Forwarder to add/edit details.
            */
            return $bCourierValidationFlag;
        }
        else
        { 
            if($bCourierValidationFlag)
            {
                /*
                 * If TNT API credentials are valid then we validate Carrier Account ID with Easypost
                 */
                $szCarrierAccountID = $carrierAgreementAry['szCarrierAccountID'];
                if(!empty($szCarrierAccountID))
                {
                    /*
                    * Fetching all available Carrier Account ID from Easypost API
                    */
                    $carrierAccountAry = array();
                    $kEasypost = new cEasyPost();
                    $carrierAccountAry = $kEasypost->getCourierDataEasypostApi($carrierAgreementAry,true);
                    
//                    $kPostmen = new cPostmen();
//                    $carrierAccountAry = $kPostmen->getCourierDataPostmenApi($carrierAgreementAry,true);
 
                    /*
                    * Checking Carrier Account ID is exists in available CAI list of Easypost API
                    */ 
                    
                    $iCarrierExists = false;
                    if(!empty($carrierAccountAry) && in_array($szCarrierAccountID, $carrierAccountAry))
                    {
                        $iCarrierExists = true;
                    }

                    if($iCarrierExists)
                    { 
                        return true;
                    }
                    else
                    {
                        $this->iCarrierAccountIDNotValid = 1;
                        $this->szSpecialErrorMesage = t($this->t_base.'errormsg/invalid_carrier_account');
                        return false;
                    }
                } 
                else
                {
                    /*
                    * As Carrier Account ID is optional field, so if CAI is empty then we return true/false on the basis of API credentials
                    */
                    return $bCourierValidationFlag;
                }
            }
            else
            {
                return $bCourierValidationFlag;
            } 
        }  

        if($response['Notifications']['Code']=='1000' || $response['Notifications']['Code']=='806' || $response['Notifications']['Code']=='803' || $response=='10001' || $response=='401')
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
?>