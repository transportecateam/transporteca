<?php
/**
 * This file is the container for partner api related functionality. 
 *
 * transportecaApi.class.php
 *
 * @copyright Copyright (C) 2016 Transporteca
 * @author Ajay
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_CLASSES__ . "/responseErrors.class.php");

class cLabels extends cDatabase
{   
    function __construct()
    {
        parent::__construct();
        return true;
    } 
    
    function createLabelRequestXml($postSearchAry)
    { 
        if(!empty($postSearchAry))
        {    
            $szDeliveryInstruction = "";
            if(!empty($postSearchAry['szDeliveryInstructions']))
            {
                $szDeliveryInstruction = $postSearchAry['szDeliveryInstructions'];
            } 
            $szPackageDetailsXml = $this->buildPackageXml($postSearchAry);
            $iTotalNumColliForAPI = $this->iTotalNumColliForAPI;
            
            $kWhsSearch = new cWHSSearch();
            $iTNTApiReductionPercentage = $kWhsSearch->getManageMentVariableByDescription('__TNT_BOOKING_API_VOLUME_REDUCTION__'); 
            
            $fTotalCargoWeight = $postSearchAry['fCargoWeight'];
            $fTotalCargoVolume = $postSearchAry['fCargoVolume'];
            
            if($iTNTApiReductionPercentage>0)
            {
                $fTotalCargoVolume = $fTotalCargoVolume - ($fTotalCargoVolume * $iTNTApiReductionPercentage * 0.01);
            }
            
            $szCourierProductCode = $postSearchAry['szTNTApiCode']; 
            if($iTotalNumColliForAPI>0)
            {
                $iNumColli = $iTotalNumColliForAPI;
            }
            else
            {
                $iNumColli = $postSearchAry['iNumColli'];
            } 
            if($postSearchAry['szShipperCountryCode']=="IE")
            {
                $postSearchAry['szShipperPostCode'] = "";
            }  
            
            if(empty($postSearchAry['szConsigneeCompanyName']))
            {
                $postSearchAry['szConsigneeCompanyName'] = $postSearchAry['szConsigneeFirstName']." ".$postSearchAry['szConsigneeLastName'];
            } 
            if(empty($postSearchAry['szShipperCompanyName']))
            {
                $postSearchAry['szShipperCompanyName'] = $postSearchAry['szShipperFirstName']." ".$postSearchAry['szShipperLastName'];
            }
            $szCargoDescription = $postSearchAry['szCargoDescription']; 
            /*
             * When we want to Test system in Dev environment the please enable following code.
             
            $postSearchAry['szUsername'] = "TransportT";
                $postSearchAry['szPassword'] = "tnt1234t";
                $postSearchAry['szShipperEmail'] = "ajay@whiz-solutions.com";
                $postSearchAry['szConsigneeEmail'] = "ajay@whiz-solutions.com";
             */ 
            $szConnectionReference = $postSearchAry['szBookingRef']."T".mt_rand(0, 999);
             
            $szTermsOfPayment = "";
            if($postSearchAry['szForwarderTNTAccountCountry']==$postSearchAry['szShipperCountryCode'])
            {
                /*
                * If forwarder country is same as shipper country then we use term Send will pay
                */
                $szTermsOfPayment = "S";
            }
            else if($postSearchAry['szForwarderTNTAccountCountry']==$postSearchAry['szConsigneeCountryCode'])
            {
                /*
                * If forwarder country is same as consignee country then we use term Send will pay
                */
                $szTermsOfPayment = "R";
            }
            else
            { 
                $this->iTermsOfPaymentError = 1;
                $this->szLabelApiError .= "<li>Shipment must be either exported to ".$postSearchAry['szForwarderTNTAccountCountry']." or Imported from ".$postSearchAry['szForwarderTNTAccountCountry']."</li>";
                return false;
            } 
            $shipperAddressXml = $this->buildShipperAddressXml($postSearchAry,$szTermsOfPayment);
            $consigneeAddressXml = $this->buildConsigneeAddressXml($postSearchAry,$szTermsOfPayment);
            
            $xmlStr = '<?xml version="1.0" encoding="UTF-8"?>
                <ESHIPPER>
                    <LOGIN>
                        <COMPANY>'.$postSearchAry['szUsername'].'</COMPANY>
                        <PASSWORD>'.$postSearchAry['szPassword'].'</PASSWORD>
                        <APPID>EC</APPID>
                        <APPVERSION>2.2</APPVERSION>
                    </LOGIN>
                    <CONSIGNMENTBATCH> 
                        '.$shipperAddressXml.'
                        <CONSIGNMENT>
                            <CONREF>'.$szConnectionReference.'</CONREF>
                            <DETAILS>
                                '.$consigneeAddressXml.'
                                <CUSTOMERREF>'.$postSearchAry['szBookingRef'].'</CUSTOMERREF> 
                                <CONTYPE>N</CONTYPE>
                                <PAYMENTIND>'.$szTermsOfPayment.'</PAYMENTIND>
                                <ITEMS>'.$iNumColli.'</ITEMS>
                                <TOTALWEIGHT>'.round((float)$fTotalCargoWeight,2).'</TOTALWEIGHT>
                                <TOTALVOLUME>'.round((float)$fTotalCargoVolume,3).'</TOTALVOLUME>
                                <CURRENCY>'.$postSearchAry['szCustomerCurrency'].'</CURRENCY> 
                                <SERVICE>'.$szCourierProductCode.'</SERVICE>  
                                <DESCRIPTION>'.$szCargoDescription.'</DESCRIPTION>
                                <DELIVERYINST>'.$szDeliveryInstruction.'</DELIVERYINST> 
                                '.$szPackageDetailsXml.'
                            </DETAILS>
                        </CONSIGNMENT>
                    </CONSIGNMENTBATCH>
                    <ACTIVITY>
                        <CREATE>
                            <CONREF>'.$szConnectionReference.'</CONREF>
                        </CREATE>
                        <BOOK ShowBookingRef="Y">
                            <CONREF>'.$szConnectionReference.'</CONREF>
                        </BOOK>
                        <SHIP> 
                            <CONREF>'.$szConnectionReference.'</CONREF> 
                        </SHIP>  
                        <PRINT>
                            <CONNOTE>
				<CONREF>'.$szConnectionReference.'</CONREF>
                            </CONNOTE>
                            <LABEL>
                                <CONREF>'.$szConnectionReference.'</CONREF>
                            </LABEL>   
                            <MANIFEST>
                                <CONREF>'.$szConnectionReference.'</CONREF>
                            </MANIFEST>
                            <INVOICE>
                                <CONREF>'.$szConnectionReference.'</CONREF>
                            </INVOICE>
                        </PRINT>
                    </ACTIVITY>
            </ESHIPPER>';
            
            /*
            *  Till we don't adds support for special characters we are replacing specials with following chars
            *   å = aa
                ø = oe
                æ = ae
                Å = Aa
                Ø = Oe
                Æ = Ae
               
                $xmlStr = str_replace("å", "aa", $xmlStr);
                $xmlStr = str_replace("ø", "oe", $xmlStr);
                $xmlStr = str_replace("æ", "ae", $xmlStr);
             * 
             */ 
            $xmlStr = str_replace("Å", "å", $xmlStr);
            $xmlStr = str_replace("Ø", "ø", $xmlStr);
            $xmlStr = str_replace("Æ", "æ", $xmlStr); 
        }   
//        echo $xmlStr;
//        die;
        return $xmlStr; 
    }
    
    function getWordWrapText($szDataString,$iLength)
    {
        if(!empty($szDataString))
        {
            $pos = strpos($szDataString, ' ', $iLength);
            if ($pos !== false) {
                return substr($szDataString, 0, $pos);
            }
            else
            {
                return $szDataString;
            }
        } 
    }
    
    
    function buildShipperAddressXml($postSearchAry,$szTermsOfPayment)
    {
        if(!empty($postSearchAry))
        {
            if(strtolower($postSearchAry['iCollection'])=='scheduled')
            { 
                $dtShipmentDate = $postSearchAry['dtCollection'];
                
                $dtCollectionStartTime = $this->getFormatedLocalTime($postSearchAry['dtCollectionStartTime']);
                $dtCollectionEndTime = $this->getFormatedLocalTime($postSearchAry['dtCollectionEndTime']);
                
                $dtAltCollectionStartTime = $this->getFormatedLocalTime($postSearchAry['dtCollectionEndTime']+4); 
                $dtAltCollectionEndTime = $this->getFormatedLocalTime($postSearchAry['dtCollectionEndTime']+6); 
            }
            else
            { 
                $date_str = $postSearchAry['dtTimingDate'] ;  
                $curr_date_time = time();
                $date_str_time = strtotime($date_str); 
                if($curr_date_time > $date_str_time)
                {
                    $date_str = date('Y-m-d H:i:s');
                    $postSearchAry['dtTimingDate'] = $date_str;
                }   
                
                $dtFormatedDate = date('Y-m-d',strtotime($postSearchAry['dtTimingDate'])); 
                if(__isWeekend($dtFormatedDate))
                {
                    $this->szLabelApiError .= "<li> Searched day is weekend so we are picking next monday as colletion date.</li>";
                    
                    $dtNextMondayDate = __nextMonday($dtFormatedDate); 
                    $dtShipmentDate = date("d/m/Y", strtotime($dtNextMondayDate));
                } 
                else
                {
                    $dtShipmentDate = date("d/m/Y",  strtotime($postSearchAry['dtTimingDate']));
                } 
                $dtCollectionStartTime = "09:00";
                $dtCollectionEndTime = "10:00";
                
                $dtAltCollectionStartTime = "11:00";
                $dtAltCollectionEndTime = "12:00";
            }
            
            $shipperAddressAry = array();
            $shipperAddressAry = $this->formatStreetAddress($postSearchAry['szShipperAddress']);
            $szShipperAddress1 = $shipperAddressAry['szAddress1'];
            $szShipperAddress2 = $shipperAddressAry['szAddress2'];
            $szShipperAddress3 = $shipperAddressAry['szAddress3'];
            
            if(!empty($postSearchAry['szMessageToSender']))
            {
                $szCollectionInstruction = $postSearchAry['szMessageToSender'];
            }
            
            /*
            * IF $szTermsOfPayment=='S' i.e. if Shipment is export service then we pass Sender address as Forwarder Account details and collection address as Shipper's address details
            * ELSE we pass Shipper's details as both Sender and Collection
            */ 
            
            $szShipperPhone = substr($postSearchAry['szShipperPhone'], 0, 8);
            if($szTermsOfPayment=='S')
            {
                $szShipperDetailsXml = '
                    <SENDER>
                        <COMPANYNAME>'.$postSearchAry['szForwarderCompanyName'].'</COMPANYNAME>
                        <STREETADDRESS1>'.$postSearchAry['szForwarderTNTAccountAddress'].'</STREETADDRESS1> 
                        <CITY>'.$postSearchAry['szForwarderTNTAccountCity'].'</CITY>
                        <PROVINCE/>
                        <POSTCODE>'.$postSearchAry['szForwarderTNTAccountPostcode'].'</POSTCODE>
                        <COUNTRY>'.$postSearchAry['szForwarderTNTAccountCountry'].'</COUNTRY>
                        <ACCOUNT>'.$postSearchAry['szAccountNumber'].'</ACCOUNT>
                        <VAT/>
                        <CONTACTNAME>'.$postSearchAry['szManagementUserName'].'</CONTACTNAME>
                        <CONTACTDIALCODE>'.$postSearchAry['iManagementUserDialCode'].'</CONTACTDIALCODE>
                        <CONTACTTELEPHONE>'.$postSearchAry['szManagementUserPhone'].'</CONTACTTELEPHONE>
                        <CONTACTEMAIL>'.$postSearchAry['szManagementUserEmail'].'</CONTACTEMAIL>
                        <COLLECTION>
                            <COLLECTIONADDRESS>
                                <COMPANYNAME>'.$postSearchAry['szShipperCompanyName'].'</COMPANYNAME>
                                <STREETADDRESS1>'.$szShipperAddress1.'</STREETADDRESS1> 
                                <STREETADDRESS2>'.$szShipperAddress2.'</STREETADDRESS2>
                                <STREETADDRESS3>'.$szShipperAddress3.'</STREETADDRESS3>
                                <CITY>'.$postSearchAry['szShipperCity'].'</CITY>
                                <PROVINCE/>
                                <POSTCODE>'.$postSearchAry['szShipperPostCode'].'</POSTCODE>
                                <COUNTRY>'.$postSearchAry['szShipperCountryCode'].'</COUNTRY>
                                <VAT/>
                                <CONTACTNAME>'.$postSearchAry['szShipperFullName'].'</CONTACTNAME>
                                <CONTACTDIALCODE>'.$postSearchAry['iShipperDialCode'].'</CONTACTDIALCODE>
                                <CONTACTTELEPHONE>'.$szShipperPhone.'</CONTACTTELEPHONE>
                                <CONTACTEMAIL>'.$postSearchAry['szShipperEmail'].'</CONTACTEMAIL>
                            </COLLECTIONADDRESS>
                            <SHIPDATE>'.$dtShipmentDate.'</SHIPDATE>
                            <PREFCOLLECTTIME>
                                <FROM>'.$dtCollectionStartTime.'</FROM>
                                <TO>'.$dtCollectionEndTime.'</TO>
                            </PREFCOLLECTTIME> 
                            <COLLINSTRUCTIONS>'.$szCollectionInstruction.'</COLLINSTRUCTIONS>
                        </COLLECTION>
                    </SENDER>	
                ';
            }
            else
            {
                $szShipperDetailsXml = '
                    <SENDER>
                        <COMPANYNAME>'.$postSearchAry['szShipperCompanyName'].'</COMPANYNAME>
                        <STREETADDRESS1>'.$szShipperAddress1.'</STREETADDRESS1> 
                        <STREETADDRESS2>'.$szShipperAddress2.'</STREETADDRESS2>
                        <STREETADDRESS3>'.$szShipperAddress3.'</STREETADDRESS3>
                        <CITY>'.$postSearchAry['szShipperCity'].'</CITY>
                        <PROVINCE/>
                        <POSTCODE>'.$postSearchAry['szShipperPostCode'].'</POSTCODE>
                        <COUNTRY>'.$postSearchAry['szShipperCountryCode'].'</COUNTRY>  
                        <ACCOUNT/>
                        <VAT/>
                        <CONTACTNAME>'.$postSearchAry['szShipperFullName'].'</CONTACTNAME>
                        <CONTACTDIALCODE>'.$postSearchAry['iShipperDialCode'].'</CONTACTDIALCODE>
                        <CONTACTTELEPHONE>'.$szShipperPhone.'</CONTACTTELEPHONE>
                        <CONTACTEMAIL>'.$postSearchAry['szShipperEmail'].'</CONTACTEMAIL> 
                        <COLLECTION>
                            <COLLECTIONADDRESS>
                                <COMPANYNAME>'.$postSearchAry['szShipperCompanyName'].'</COMPANYNAME>
                                <STREETADDRESS1>'.$szShipperAddress1.'</STREETADDRESS1> 
                                <STREETADDRESS2>'.$szShipperAddress2.'</STREETADDRESS2>
                                <STREETADDRESS3>'.$szShipperAddress3.'</STREETADDRESS3>
                                <CITY>'.$postSearchAry['szShipperCity'].'</CITY>
                                <PROVINCE/>
                                <POSTCODE>'.$postSearchAry['szShipperPostCode'].'</POSTCODE>
                                <COUNTRY>'.$postSearchAry['szShipperCountryCode'].'</COUNTRY>
                                <VAT/>
                                <CONTACTNAME>'.$postSearchAry['szShipperFullName'].'</CONTACTNAME>
                                <CONTACTDIALCODE>'.$postSearchAry['iShipperDialCode'].'</CONTACTDIALCODE>
                                <CONTACTTELEPHONE>'.$szShipperPhone.'</CONTACTTELEPHONE>
                                <CONTACTEMAIL>'.$postSearchAry['szShipperEmail'].'</CONTACTEMAIL>
                            </COLLECTIONADDRESS>
                            <SHIPDATE>'.$dtShipmentDate.'</SHIPDATE>
                            <PREFCOLLECTTIME>
                                <FROM>'.$dtCollectionStartTime.'</FROM>
                                <TO>'.$dtCollectionEndTime.'</TO>
                            </PREFCOLLECTTIME> 
                            <COLLINSTRUCTIONS>'.$szCollectionInstruction.'</COLLINSTRUCTIONS>
                        </COLLECTION>
                    </SENDER>	
                ';
            } 
            return $szShipperDetailsXml;
        }
    }
    
    function buildConsigneeAddressXml($postSearchAry,$szTermsOfPayment)
    {
        if(!empty($postSearchAry))
        {
            $ConsigneeAddressAry = array();
            $ConsigneeAddressAry = $this->formatStreetAddress($postSearchAry['szConsigneeAddress']);
            $szConsigneeAddress1 = $ConsigneeAddressAry['szAddress1'];
            $szConsigneeAddress2 = $ConsigneeAddressAry['szAddress2'];
            $szConsigneeAddress3 = $ConsigneeAddressAry['szAddress3'];
            
            $szConsigneePhone = substr($postSearchAry['szConsigneePhone'], 0, 8);
            
            /*
            * IF $szTermsOfPayment=='R' i.e. if Shipment is a Import service then we pass Receiver address as Forwarder Account details and Delivery address as Consignee's address details
            * ELSE we pass Consignee's details as both Sender and Collection
            */
            if($szTermsOfPayment=='R')
            {
                $szConsigneeAddressXml = '
                    <RECEIVER>
                        <COMPANYNAME>'.$postSearchAry['szForwarderCompanyName'].'</COMPANYNAME>
                        <STREETADDRESS1>'.$postSearchAry['szForwarderTNTAccountAddress'].'</STREETADDRESS1> 
                        <CITY>'.$postSearchAry['szForwarderTNTAccountCity'].'</CITY>
                        <PROVINCE/>
                        <POSTCODE>'.$postSearchAry['szForwarderTNTAccountPostcode'].'</POSTCODE>
                        <COUNTRY>'.$postSearchAry['szForwarderTNTAccountCountry'].'</COUNTRY> 
                        <VAT/>
                        <CONTACTNAME>'.$postSearchAry['szManagementUserName'].'</CONTACTNAME>
                        <CONTACTDIALCODE>'.$postSearchAry['iManagementUserDialCode'].'</CONTACTDIALCODE>
                        <CONTACTTELEPHONE>'.$postSearchAry['szManagementUserPhone'].'</CONTACTTELEPHONE>
                        <CONTACTEMAIL>'.$postSearchAry['szManagementUserEmail'].'</CONTACTEMAIL> 
                        <ACCOUNT>'.$postSearchAry['szAccountNumber'].'</ACCOUNT>
                        <ACCOUNTCOUNTRY>'.$postSearchAry['szForwarderTNTAccountCountry'].'</ACCOUNTCOUNTRY>
                    </RECEIVER>
                    <DELIVERY>
                        <COMPANYNAME>'.$postSearchAry['szConsigneeCompanyName'].'</COMPANYNAME>
                        <STREETADDRESS1>'.$szConsigneeAddress1.'</STREETADDRESS1>
                        <STREETADDRESS2>'.$szConsigneeAddress2.'</STREETADDRESS2>
                        <STREETADDRESS3>'.$szConsigneeAddress3.'</STREETADDRESS3>
                        <CITY>'.$postSearchAry['szConsigneeCity'].'</CITY>
                        <PROVINCE/>
                        <POSTCODE>'.$postSearchAry['szConsigneePostCode'].'</POSTCODE>
                        <COUNTRY>'.$postSearchAry['szConsigneeCountryCode'].'</COUNTRY>
                        <VAT/>
                        <CONTACTNAME>'.$postSearchAry['szConsigneeFullName'].'</CONTACTNAME>
                        <CONTACTDIALCODE>'.$postSearchAry['iConsigneeDialCode'].'</CONTACTDIALCODE>
                        <CONTACTTELEPHONE>'.$szConsigneePhone.'</CONTACTTELEPHONE>
                        <CONTACTEMAIL>'.$postSearchAry['szConsigneeEmail'].'</CONTACTEMAIL>
                    </DELIVERY>
                ';
            }
            else
            {
                $szConsigneeAddressXml = '
                    <RECEIVER>
                        <COMPANYNAME>'.$postSearchAry['szConsigneeCompanyName'].'</COMPANYNAME>
                        <STREETADDRESS1>'.$szConsigneeAddress1.'</STREETADDRESS1>
                        <STREETADDRESS2>'.$szConsigneeAddress2.'</STREETADDRESS2>
                        <STREETADDRESS3>'.$szConsigneeAddress3.'</STREETADDRESS3>
                        <CITY>'.$postSearchAry['szConsigneeCity'].'</CITY>
                        <PROVINCE/>
                        <POSTCODE>'.$postSearchAry['szConsigneePostCode'].'</POSTCODE>
                        <COUNTRY>'.$postSearchAry['szConsigneeCountryCode'].'</COUNTRY>
                        <VAT/>
                        <CONTACTNAME>'.$postSearchAry['szConsigneeFullName'].'</CONTACTNAME>
                        <CONTACTDIALCODE>'.$postSearchAry['iConsigneeDialCode'].'</CONTACTDIALCODE>
                        <CONTACTTELEPHONE>'.$szConsigneePhone.'</CONTACTTELEPHONE>
                        <CONTACTEMAIL>'.$postSearchAry['szConsigneeEmail'].'</CONTACTEMAIL>
                    </RECEIVER>
                    <DELIVERY>
                        <COMPANYNAME>'.$postSearchAry['szConsigneeCompanyName'].'</COMPANYNAME>
                        <STREETADDRESS1>'.$szConsigneeAddress1.'</STREETADDRESS1>
                        <STREETADDRESS2>'.$szConsigneeAddress2.'</STREETADDRESS2>
                        <STREETADDRESS3>'.$szConsigneeAddress3.'</STREETADDRESS3>
                        <CITY>'.$postSearchAry['szConsigneeCity'].'</CITY>
                        <PROVINCE/>
                        <POSTCODE>'.$postSearchAry['szConsigneePostCode'].'</POSTCODE>
                        <COUNTRY>'.$postSearchAry['szConsigneeCountryCode'].'</COUNTRY>
                        <VAT/>
                        <CONTACTNAME>'.$postSearchAry['szConsigneeFullName'].'</CONTACTNAME>
                        <CONTACTDIALCODE>'.$postSearchAry['iConsigneeDialCode'].'</CONTACTDIALCODE>
                        <CONTACTTELEPHONE>'.$szConsigneePhone.'</CONTACTTELEPHONE>
                        <CONTACTEMAIL>'.$postSearchAry['szConsigneeEmail'].'</CONTACTEMAIL>
                    </DELIVERY>
                ';
            } 
            return $szConsigneeAddressXml;
        }
    }
    
    function formatStreetAddress($szStreetAddress)
    {
        if(strlen($szStreetAddress)>60)
        { 
            $szShipperAddressString = $szStreetAddress;
            $szShipperAddress1 = substrwords($szShipperAddressString, 30,false); 
            
            $szAddress2 = substr($szShipperAddressString, strlen($szShipperAddress1));
            $szShipperAddress2 = substrwords($szAddress2, 30,false); 
            
            $totalLenUsed = strlen($szShipperAddress1) + strlen($szShipperAddress2);
            
            $szShipperAddress3 = substr($szShipperAddressString, $totalLenUsed); 
            
            if(strlen($szShipperAddress3)>30)
            {
                $szShipperAddress3 = substrwords($szShipperAddress3, 30,false);  
            }
        }
        else if(strlen($szStreetAddress)>30)
        {
            /*
            * If Address String >30 chars, then we take first 30 chars as szAddress1 and rest in szAddress2
             * $szShipperAddressString = $szStreetAddress;
            $szShipperAddress1 = $this->getWordWrapText($szShipperAddressString,30);

            $szShipperAddressString_2 = substr($szStreetAddress,strlen($szShipperAddress1)+1);
            $szShipperAddress2 = $this->getWordWrapText($szShipperAddressString_2,30);
            */ 
            
            $szShipperAddressString = $szStreetAddress;
            $szShipperAddress1 = substrwords($szShipperAddressString, 30,false); 
            
            $szShipperAddress2 = substr($szShipperAddressString, strlen($szShipperAddress1)); 
            if(strlen($szShipperAddress2)>30)
            {
                $szAddress2 = substr($szShipperAddressString, strlen($szShipperAddress1));
                $szShipperAddress2 = substrwords($szAddress2, 30,false);  
                $totalLenUsed = strlen($szShipperAddress1) + strlen($szShipperAddress2); 
                $szShipperAddress3 = substr($szShipperAddressString, $totalLenUsed); 
                
                if(strlen($szShipperAddress3)>30)
                {
                    $szShipperAddress3 = substrwords($szShipperAddress3, 30,false);  
                }
            }
        }
        else
        {
            $szShipperAddress1 = $szStreetAddress;
        } 
        $retAry = array();
        $retAry['szAddress1'] = trim($szShipperAddress1);
        $retAry['szAddress2'] = trim($szShipperAddress2);
        $retAry['szAddress3'] = trim($szShipperAddress3); 
        return $retAry;
    }
    
    function buildPackageXml($postSearchAry)
    {
        if(!empty($postSearchAry))
        {
            $cargoDetailsAry = array();
            $cargoDetailsAry = $this->getCargoDimensions($postSearchAry);
                  
            $szPackageDetailsXml = "";
            
            if(!empty($cargoDetailsAry))
            {
                foreach($cargoDetailsAry as $cargoDetailsArys)
                { 
                    /*
                     * Converting cargo dimen from CM to Meter
                     */
                    $fLength = round((float)($cargoDetailsArys['length']/100),2);
                    $fHeight = round((float)($cargoDetailsArys['height']/100),2);
                    $fWidth = round((float)($cargoDetailsArys['width']/100),2);
                    $fWeight = round((float)($cargoDetailsArys['weight']),2);
                    
                    /*
                     * As a standard the following are limited to:
                        Max weight per package: 70kg
                        Max length per package: 2.4m
                        Max height per package: 1.5m
                        Max width per package: 1.2m
                     */
                    if($fLength<=0 || $fWidth<=0 || $fHeight<=0 || $fWeight<=0)
                    { 
                        $this->szLabelApiError .= "<li>One or more of the dimensions is registered as 0. Please update the carton size, or make labels manually.</li>";
                        $bErrorFlag = true;
                    }
                    else
                    {
                        if($fLength>2.4)
                        {
                            $this->szLabelApiError .= "<li>Length must be no more than 2.4 meter.</li>";
                            $bErrorFlag = true;
                        } 
                        if($fHeight>1.5)
                        {
                            $this->szLabelApiError .= "<li>Height must be no more than 1.5 meter.</li>";
                            $bErrorFlag = true;
                        }
                        if($fWidth>1.2)
                        {
                            $this->szLabelApiError .= "<li>Width must be no more than 1.2 meter.</li>";
                            $bErrorFlag = true;
                        }
                        if($fWeight>70)
                        {
                            $this->szLabelApiError .= "<li>Weight must be no more than 70kg.</li>";
                            $bErrorFlag = true;
                        }
                    }
                    if(strlen($cargoDetailsArys['szPieceDescription'])>24)
                    {
                        $this->szLabelApiError .= "<li>Piece line description must be no more than 24 characters.</li>";
                        $bErrorFlag = true;
                    } 
                    
                    if(strlen($postSearchAry['description'])>30)
                    {
                        $this->szLabelApiError .= "<li>Cargo line description must be no more than 30 characters.</li>";
                        $bErrorFlag = true;
                    }  
                    if($bErrorFlag)
                    {
                        $this->iCargoDimensionError = 1;
                        break;
                    } 
                    $szPackageDetailsXml .= '
                        <PACKAGE>
                            <ITEMS>'.$cargoDetailsArys['count'].'</ITEMS>
                            <DESCRIPTION>'.$cargoDetailsArys['description'].'</DESCRIPTION>
                            <LENGTH>'.$fLength.'</LENGTH>
                            <HEIGHT>'.$fHeight.'</HEIGHT>
                            <WIDTH>'.$fWidth.'</WIDTH>
                            <WEIGHT>'.$fWeight.'</WEIGHT> 
                        </PACKAGE>
                    '; 
                }
            } 
            return $szPackageDetailsXml;
        }
    } 
    /*
     * $fedexApiLogsAry = array(); 
            $fedexApiLogsAry['iWebServiceType'] = 12 ;
            $fedexApiLogsAry['szRequestData'] = $szCustomerXml;
            $fedexApiLogsAry['szResponseData'] = "Http Code: ".$culrInfoAry['http_code']."\n\n".$cUrlResponseXml;
            //$this->addFedexApiLogs($fedexApiLogsAry);
     */
    function logDebugger($szFilePath,$szDataString)
    {
        if(!empty($szFilePath) && !empty($szDataString))
        {
            $fw = fopen($szFilePath , 'a'); 
            fwrite($fw , $szDataString);
            fclose($fw); 
        }
    }
    
    function downloadSplitedPdfLabelFrom($szPdfFileName,$szTrackingNumber)
    {
        if(!empty($szPdfFileName))
        {
            $pdfDocumentsAry = array();
            $pdfDocumentsAry = unserialize($szPdfFileName);
            
            $sliptedPdfDocumenrAry = array();
            if(!empty($pdfDocumentsAry))
            {
                foreach($pdfDocumentsAry as $pdfDocumentsArys)
                {
                    if(!empty($pdfDocumentsArys))
                    {
                        $szPdfFilePath = __UPLOAD_COURIER_LABEL_PDF_MANAGEMENT__."/".$pdfDocumentsArys;     
                        if(file_exists($szPdfFilePath))
                        {     
                            $szOutputDir = __UPLOAD_COURIER_LABEL_PDF_MANAGEMENT__."/";
                            $kCourierService = new cCourierServices();
                            $pdfFilesAry = array();
                            $pdfFilesAry = $kCourierService->splitUploadedLabels($pdfDocumentsArys,$szOutputDir);  
                           
                            if(!empty($sliptedPdfDocumenrAry) && !empty($pdfFilesAry))
                            {
                                $sliptedPdfDocumenrAry = array_merge($sliptedPdfDocumenrAry,$pdfFilesAry);
                            }
                            else if(!empty($pdfFilesAry))
                            {
                                $sliptedPdfDocumenrAry = $pdfFilesAry;
                            } 
                        }
                    }
                } 
                if(!empty($sliptedPdfDocumenrAry))
                {
                    $szPdfFileNameJsonStr = json_encode($sliptedPdfDocumenrAry,true);
                }
                
                $retAry = array();
                $retAry['szTrackingNumber'] = $szTrackingNumber;
                $retAry['szPdfFileNameJson'] = $szPdfFileNameJsonStr; 
                return $retAry; 
            } 
        }
    }
    
    function downloadLabelFromTNTApi($idBooking,$additionalOptionsAry=array())
    {
        if($idBooking>0)
        {
            $kBooking = new cBooking();
            $postSearchAry = array();
            $postSearchAry = $kBooking->getExtendedBookingDetails($idBooking);
            $postSearchAry['szShipperFullName'] = $postSearchAry['szShipperFirstName']." ".$postSearchAry['szShipperLastName'];
            $postSearchAry['szConsigneeFullName'] = $postSearchAry['szConsigneeFirstName']." ".$postSearchAry['szConsigneeLastName'];
            if(!empty($additionalOptionsAry))
            {
                $postSearchAry['dtCollection'] = $additionalOptionsAry['dtCollection'];
                $postSearchAry['dtCollectionStartTime'] = $additionalOptionsAry['dtCollectionStartTime'];
                $postSearchAry['dtCollectionEndTime'] = $additionalOptionsAry['dtCollectionEndTime'];
                $postSearchAry['iCollection'] = $additionalOptionsAry['iCollection']; 
                 
                
                if(!empty($additionalOptionsAry['szCargoDescription']) && $additionalOptionsAry['iDisplayCargoDescField']==1)
                {
                    $postSearchAry['szCargoDescription'] = $additionalOptionsAry['szCargoDescription'];
                }
                else
                {
                    $postSearchAry['szCargoDescription'] = utf8_decode($postSearchAry['szCargoDescription']);
                } 
                if($additionalOptionsAry['iDisplayIndividualLineCargoDescField']==1)
                {
                    $postSearchAry['cargoLineDescriptionAry'] = $additionalOptionsAry['cargoLineDescriptionAry'];
                    $postSearchAry['iDisplayIndividualLineCargoDescField'] = 1;
                }
                else
                {
                    $postSearchAry['iDisplayIndividualLineCargoDescField'] = false;
                }
                   
                if($additionalOptionsAry['iDisplayIndividualLinePieceDescField']==1)
                {
                    $postSearchAry['pieceDescriptionAry'] = $additionalOptionsAry['pieceDescriptionAry'];
                    $postSearchAry['iDisplayIndividualLinePieceDescField'] = 1;
                }
                else
                {
                    $postSearchAry['iDisplayIndividualLinePieceDescField'] = false;
                } 
                
                $labelErrorFieldsAry = array();
                $labelErrorFieldsAry = $additionalOptionsAry['labelErrorFieldsAry'];
                
                $labelShipConFieldsAry = array();
                $labelShipConFieldsAry = $additionalOptionsAry['labelShipConFieldsAry'];
                     
                /*
                * Processing Shipper City Details
                */
                if($additionalOptionsAry['iCheckShipperCityFlag']==1)
                {
                    $postSearchAry['szShipperCity'] = $additionalOptionsAry['szShipperCity'];
                } 
                else if($labelErrorFieldsAry['iDisplayShipperCityField']==1)
                {
                    $postSearchAry['szShipperCity'] = $labelShipConFieldsAry['szShipperCity'];
                }
                
                /*
                * Processing Consignee City Details
                */
                if($additionalOptionsAry['iCheckConsigneeCityFlag']==1)
                {
                    $postSearchAry['szConsigneeCity'] = $additionalOptionsAry['szConsigneeCity'];
                } 
                else if($labelErrorFieldsAry['iDisplayShipperCityField']==1)
                {
                    $postSearchAry['szConsigneeCity'] = $labelShipConFieldsAry['szConsigneeCity'];
                }  
                if(!empty($labelErrorFieldsAry))
                {  
                    /*
                    * Adding Shipper details from 'Shipping Instruction' popup if there are strings length is greater than as defined for TNT Label API
                    */
                    if($labelErrorFieldsAry['iDisplayShipperNameField']==1)
                    {
                        $postSearchAry['szShipperFullName'] = $labelShipConFieldsAry['szShipperName'];
                    } 
                    if($labelErrorFieldsAry['iDisplayShipperCompanyNameField']==1)
                    {
                        $postSearchAry['szShipperCompanyName'] = $labelShipConFieldsAry['szShipperCompanyName'];
                    }
                    if($labelErrorFieldsAry['iDisplayShipperEmailField']==1)
                    {
                        $postSearchAry['szShipperEmail'] = $labelShipConFieldsAry['szShipperEmail'];
                    }
                    if($labelErrorFieldsAry['iDisplayShipperAddressField']==1)
                    {
                        $postSearchAry['szShipperAddress'] = $labelShipConFieldsAry['szShipperAddress'];
                    }
                    if($labelErrorFieldsAry['iDisplayShipperPostcodeField']==1)
                    {
                        $postSearchAry['szShipperPostCode'] = $labelShipConFieldsAry['szShipperPostCode'];
                    } 

                    /*
                    * Adding Consignee details from 'Shipping Instruction' popup if there are strings length is greater than as defined for TNT Label API
                    */

                    if($labelErrorFieldsAry['iDisplayConsigneeNameField']==1)
                    {
                        $postSearchAry['szConsigneeFullName'] = $labelShipConFieldsAry['szConsigneeName'];
                    } 
                    if($labelErrorFieldsAry['iDisplayConsigneeCompanyNameField']==1)
                    {
                        $postSearchAry['szConsigneeCompanyName'] = $labelShipConFieldsAry['szConsigneeCompanyName'];
                    }

                    if($labelErrorFieldsAry['iDisplayConsigneeEmailField']==1)
                    {
                        $postSearchAry['szConsigneeEmail'] = $labelShipConFieldsAry['szConsigneeEmail'];
                    }
                    if($labelErrorFieldsAry['iDisplayConsigneeAddressField']==1)
                    {
                        $postSearchAry['szConsigneeAddress'] = $labelShipConFieldsAry['szConsigneeAddress'];
                    }
                    if($labelErrorFieldsAry['iDisplayConsigneePostcodeField']==1)
                    {
                        $postSearchAry['szConsigneePostCode'] = $labelShipConFieldsAry['szConsigneePostCode'];
                    } 
                }
                if((int)$postSearchAry['isManualCourierBooking']=='1')//Manual RFQ Booking
                {
                    $idCourierProduct = $additionalOptionsAry['idCourierProduct'];
                    if(!empty($idCourierProduct))
                    {
                        $idCourierProductArr=explode("-",$idCourierProduct); 
                        if((int)count($idCourierProductArr)>0)
                        { 
                            $idCourierProvider = $idCourierProductArr[0];  
                            $idServiceAgreement = $idCourierProductArr[1];
                        }
                    } 
                } 
                else
                {
                    $idCourierProvider = $postSearchAry['idServiceProviderProduct'];
                    $idServiceAgreement = $postSearchAry['idServiceProvider'];
                }  
                $kCourierService = new cCourierServices(); 
                $groupApiCodeArr = $kCourierService->getGroupIdCourierProviderProduct($idCourierProvider);
                $idGroup = $groupApiCodeArr[0]['idGroup'];
                 
                $providerForwarderArrs = array();
                $providerForwarderArrs['idForwarder'] = $postSearchAry['idForwarder'];
                $providerForwarderArrs['idCourierAgreement'] = $idServiceAgreement;
                $providerForwarderArrs['idCourierProviderProductid'] = $idCourierProvider;
                   
                $kCourierServices = new cCourierServices();
                $courierAgreementAry = array();
                $courierAgreementAry = $kCourierServices->getCourierServices($providerForwarderArrs);  
                $courierAgreementAry = $courierAgreementAry[0];  
                
                if(!empty($courierAgreementAry))
                {
                    $szTNTApiCode = $this->getTNTApiServiceCode($courierAgreementAry['szCourierAPICode']); 
                    if(empty($szTNTApiCode))
                    { 
                        $szTNTApiCode = "48N";
                    }
                    $courierAgreementAry['szTNTApiCode'] = $szTNTApiCode;
                } 
            }   
             
            if($courierAgreementAry['idCourierProvider']!=__TNT_API__)
            {
                $this->szLabelApiError = "<li>Create label option is only available for TNT services. Please try again later.</li>"; 
                return false;
            }
            else
            {  
                $kForwarder = new cForwarder();
                $kForwarder->load($postSearchAry['idForwarder']);

                $idForwarderCountry = $kForwarder->idCountry;
                
                if($idForwarderCountry>0)
                {
                    $kConfig = new cConfig();
                    $kConfig->loadCountry($idForwarderCountry);
                    $szForwarderCountryISO = $kConfig->szCountryISO; 
                }
                else
                {
                    $szForwarderCountryISO = "DK";
                }  
                $szForwarderCompanyName = $kForwarder->szCompanyName; 
                $szForwarderTNTAccountAddress = $kForwarder->szAddress;
                $szForwarderTNTAccountPostcode = $kForwarder->szPostCode;
                $szForwarderTNTAccountCity = $kForwarder->szCity;  
                
                $idAdmin = $_SESSION['admin_id'] ;
                $kAdmin = new cAdmin();
                $kAdmin->getAdminDetails($idAdmin);
                $szManagementUserName = $kAdmin->szFirstName." ".$kAdmin->szLastName ;
                $szManagementUserEmail = $kAdmin->szEmail;
                
                $kConfig = new cConfig();
                $kConfig->loadCountry($kAdmin->idInternationalDialCode);
                
                $iManagementUserDialCode = $kConfig->iInternationDialCode; 
                $szManagementUserPhone = substr($kAdmin->szPhone, 0,8);
            
                $postSearchAry['szTNTApiCode'] = $courierAgreementAry['szTNTApiCode'];
                $postSearchAry['szUsername'] = $courierAgreementAry['szUsername'];
                $postSearchAry['szPassword'] = $courierAgreementAry['szPassword'];
                $postSearchAry['szAccountNumber'] = $courierAgreementAry['szAccountNumber'];
                $postSearchAry['idCourierProvider'] = $courierAgreementAry['idCourierProvider']; 
                $postSearchAry['szCourierProviderName'] = $courierAgreementAry['szName'];
                $postSearchAry['szMeterNumber'] = $courierAgreementAry['szMeterNumber'];
                $postSearchAry['idGroup'] = $courierAgreementAry['idGroup'];  
                
                $postSearchAry['szForwarderTNTAccountCountry'] = $szForwarderCountryISO; 
                $postSearchAry['szForwarderTNTAccountPostcode'] = $szForwarderTNTAccountPostcode; 
                $postSearchAry['szForwarderTNTAccountCity'] = $szForwarderTNTAccountCity;
                $postSearchAry['szForwarderTNTAccountAddress'] = $szForwarderTNTAccountAddress; 
                $postSearchAry['szForwarderCompanyName'] = $szForwarderCompanyName;   
                
                $postSearchAry['szManagementUserName'] = $szManagementUserName;
                $postSearchAry['szManagementUserEmail'] = $szManagementUserEmail;
                $postSearchAry['iManagementUserDialCode'] = $iManagementUserDialCode;
                $postSearchAry['szManagementUserPhone'] = $szManagementUserPhone; 
            } 
            /*
            $kCourierService = new cCourierServices();
            if($kCourierService->checkFromCountryToCountryExistsForCC($postSearchAry['idShipperCountry'],$postSearchAry['idConsigneeCountry']))
            {
                $postSearchAry['iIncludeCustomClearance'] = 0;
            }
            else
            {
                $postSearchAry['iIncludeCustomClearance'] = 1;
            }
             * 
             */
            $postSearchAry['iIncludeCustomClearance'] = 0;
            /*
             * For testing purpose we 
             
            $postSearchAry['szUsername'] = "TransportT";
            $postSearchAry['szPassword'] = "tnt1234t";
            $postSearchAry['szAccountNumber'] = "2013579";
            $postSearchAry['szShipperEmail'] = "ajay@whiz-solutions.com";
            $postSearchAry['szConsigneeEmail'] = "ajay@whiz-solutions.com";
             */
            /*
            * Checking if the details corresponds to Routing label product is exists in table 'tbltntserviceconversions'
            */
            $kRoutingLabel = new cRoutingLabels();
            $kRoutingLabel->buildTNTProductXml($postSearchAry);
            if($kRoutingLabel->iProductNotExistsError==1)
            {
                $this->szLabelApiError .= "<li>We must have a row in table 'tbltntserviceconversions' for corresponding Product code ".$postSearchAry['szTNTApiCode']."</li>";
                return false;
            }
            
            /*
            * Validating Input fields
            */
            if($this->validateLabelInputFields($postSearchAry))
            {
                return false;
            } 
            else
            {
                $this->szTrackingNumber = "";
                $this->szLabelPdfFileName = "";
 
                $tntLabelDocumentAry = array();
                $tntLabelDocumentAry = $this->createLabelFromTNTApi($postSearchAry);
                $szTrackingNumber = $this->szTrackingNumber;
                   
                $pdfFileNameAry = array();
                if(!empty($tntLabelDocumentAry))
                {
                    foreach($tntLabelDocumentAry as $szDocumentKey=>$tntLabelDocumentArys)
                    {
                        if(!empty($tntLabelDocumentArys))
                        {
                            if($szDocumentKey=='szLabelPdfFilePath')
                            { 
                                $this->szLabelLogStr .= "<p>Stored Routing Label PDF at: ".__BASE_URL_TNT_HTML__."/labels/".$tntLabelDocumentArys."</p><br>"; 
                                $szInitials = "TL";
                            }
                            else if($szDocumentKey=='szManiFestFilePath')
                            {
                                $this->szLabelLogStr .= "<p>Stored Manifest PDF at: ".__BASE_URL_TNT_HTML__."/labels/".$tntLabelDocumentArys."</p><br>"; 
                                $szInitials = "TMF";
                            }
                            else if($szDocumentKey=='szConsignmentNoteFilePath')
                            {
                                $this->szLabelLogStr .= "<p>Stored Consignment Notes PDF at: ".__BASE_URL_TNT_HTML__."/labels/".$tntLabelDocumentArys."</p><br>"; 
                                $szInitials = "TCN";
                            }
                            else if($szDocumentKey=='szInvoiceFilePath')
                            {
                                $this->szLabelLogStr .= "<p>Stored TNT Invoice PDF at: ".__BASE_URL_TNT_HTML__."/labels/".$tntLabelDocumentArys."</p><br>"; 
                                $szInitials = "TI";
                            }
                            else
                            {
                                continue;
                            }
                            $szPdfFilePath = __APP_PATH_TNT_LABEL__."/".$tntLabelDocumentArys;
                            $szLabelPdfFileName = $szInitials."_".$idBooking."_".$szTrackingNumber."_".date('YmdHis')."_".mt_rand(1, 99999).".pdf";
                            $szLabelPdfFilePath = __UPLOAD_COURIER_LABEL_PDF_MANAGEMENT__."/".$szLabelPdfFileName;
                            if(file_exists($szLabelPdfFilePath))
                            {
                                @unlink($szLabelPdfFilePath);
                            }
                            $this->szLabelLogStr .= "<p>Copying file from ".$szPdfFilePath." to ".$szLabelPdfFilePath."</p>"; 
                            @copy($szPdfFilePath, $szLabelPdfFilePath);

                            $this->szLabelLogStr .= "<p>PDF file successfully copied. Process complete</p>"; 
                            $pdfFileNameAry[] = $szLabelPdfFileName;
                        } 
                    } 
                    $retAry = array();
                    $retAry['szTrackingNumber'] = $szTrackingNumber;
                    $retAry['documents'] = $pdfFileNameAry;
                    return $retAry;
                }
                else
                {
                    return false;
                }
            } 
        }
    }
    
    function validateLabelInputFields($postSearchAry,$bPopupFlag=false)
    { 
        $this->labelValidationErrorAry = array();
        $postSearchAry['szCargoDescription'] = trim($postSearchAry['szCargoDescription']); 
        if(!empty($postSearchAry))
        { 
            $bErrorFlag = false;
            $labelValidationErrorAry = array();
            if(strtolower($postSearchAry['iCollection'])=='scheduled')
            {
                if($postSearchAry['dtCollectionStartTime']>=$postSearchAry['dtCollectionEndTime'])
                {
                    $this->szLabelApiError .= "<li>Collection from time must be less then to time.</li>";
                    $bErrorFlag = true;
                    $labelValidationErrorAry['iCollectionTimeError'] = 1;
                }
                else
                {
                    if($postSearchAry['dtCollectionStartTime']<18 || $postSearchAry['dtCollectionStartTime']>32)
                    {
                        $this->szLabelApiError .= "<li>For TNT, collection must between 09:00 and 16:00</li>";
                        $bErrorFlag = true;
                        $labelValidationErrorAry['iCollectionTimeError'] = 1;
                    }
                    else if($postSearchAry['dtCollectionEndTime']<18 || $postSearchAry['dtCollectionEndTime']>32)
                    {
                        $this->szLabelApiError .= "<li>For TNT, collection must between 09:00 and 16:00</li>";
                        $bErrorFlag = true;
                        $labelValidationErrorAry['iCollectionTimeError'] = 1;
                    } 
                }
            }
            
            if($bPopupFlag && $labelValidationErrorAry['iCollectionTimeError']==1)
            {
                $this->labelValidationErrorAry = $labelValidationErrorAry;
                return false;
            }
            
            $postSearchAry['szCargoDescription'] = trim($postSearchAry['szCargoDescription']); 
            
            if(strlen($postSearchAry['szCargoDescription'])>30)
            {
                $this->szLabelApiError .= "<li>Cargo description must be no more than 30 characters.</li>"; 
                $bErrorFlag = true;
            }
            if(strlen($postSearchAry['szDeliveryInstructions'])>60)
            {
                $this->szLabelApiError .= "<li>Delivery instruction must be no more than 60 characters.</li>";
                $bErrorFlag = true;
            }
             
            if(strlen($postSearchAry['szShipperFullName'])>25)
            {
                $this->szLabelApiError .= "<li>Shipper name must be no more than 25 characters.</li>";
                $bErrorFlag = true;
                $labelValidationErrorAry['iDisplayShipperNameField'] = 1;
            }
            if(strlen($postSearchAry['szShipperEmail'])>30)
            {
                $this->szLabelApiError .= "<li>Shipper email must be no more than 30 characters.</li>";
                $bErrorFlag = true;
                $labelValidationErrorAry['iDisplayShipperEmailField'] = 1;
            }
            if(strlen($postSearchAry['szShipperCompanyName'])>50)
            {
                $this->szLabelApiError .= "<li>Shipper company name must be no more than 50 characters.</li>";
                $bErrorFlag = true;
                $labelValidationErrorAry['iDisplayShipperCompanyNameField'] = 1;
            } 
            if(strlen($postSearchAry['szShipperAddress'])>90)
            {
                $this->szLabelApiError .= "<li>Shipper address must be no more than 80 characters.</li>";
                $bErrorFlag = true;
                $labelValidationErrorAry['iDisplayShipperAddressField'] = 1;
            }
            if(strlen($postSearchAry['szShipperPostCode'])>9)
            {
                $this->szLabelApiError .= "<li>Shipper postcode must be no more than 9 characters.</li>";
                $bErrorFlag = true;
                $labelValidationErrorAry['iDisplayShipperPostcodeField'] = 1;
            }
            if(strlen($postSearchAry['szShipperCity'])>30)
            {
                $this->szLabelApiError .= "<li>Shipper city must be no more than 30 characters.</li>";
                $bErrorFlag = true;
                $labelValidationErrorAry['iDisplayShipperCityField'] = 1;
            }
            
            if(strlen($postSearchAry['szConsigneeFullName'])>25)
            {
                $this->szLabelApiError .= "<li>Consignee name must be no more than 25 characters.</li>";
                $bErrorFlag = true;
                $labelValidationErrorAry['iDisplayConsigneeNameField'] = 1;
            }  
            if(strlen($postSearchAry['szConsigneeEmail'])>30)
            {
                $this->szLabelApiError .= "<li>Consignee email must be no more than 30 characters.</li>";
                $bErrorFlag = true;
                $labelValidationErrorAry['iDisplayConsigneeEmailField'] = 1;
            }
            if(strlen($postSearchAry['szConsigneeCompanyName'])>50)
            {
                $this->szLabelApiError .= "<li>Consignee company name must be no more than 50 characters.</li>";
                $bErrorFlag = true;
                $labelValidationErrorAry['iDisplayConsigneeCompanyNameField'] = 1;
            }  
            if(strlen($postSearchAry['szConsigneeAddress'])>90)
            {
                $this->szLabelApiError .= "<li>Consignee address must be no more than 80 characters.</li>";
                $bErrorFlag = true;
                $labelValidationErrorAry['iDisplayConsigneeAddressField'] = 1;
            } 
            if(strlen($postSearchAry['szConsigneePostCode'])>9)
            {
                $this->szLabelApiError .= "<li>Consignee postcode must be no more than 9 characters.</li>";
                $bErrorFlag = true;
                $labelValidationErrorAry['iDisplayConsigneePostcodeField'] = 1;
            }
            if(strlen($postSearchAry['szConsigneeCity'])>30)
            {
                $this->szLabelApiError .= "<li>Consignee city must be no more than 30 characters.</li>";
                $bErrorFlag = true;
                $labelValidationErrorAry['iDisplayConsigneeCityField'] = 1;
            }  
            if(strlen($postSearchAry['szShipperCountryCode'])>3)
            {
                $this->szLabelApiError .= "<li>Shipper country must be no more than 3 characters.</li>";
                $bErrorFlag = true;
            }
            if(strlen($postSearchAry['szConsigneeCountryCode'])>3)
            {
                $this->szLabelApiError .= "<li>Consignee country must be no more than 3 characters.</li>";
                $bErrorFlag = true;
            } 
            if(strlen($postSearchAry['iShipperDialCode'])>=6)
            {
                $this->szLabelApiError .= "<li>Shipper dial code must be no more than 6 characters.</li>";
                $bErrorFlag = true;
            }
            if(strlen($postSearchAry['iConsigneeDialCode'])>=6)
            {
                $this->szLabelApiError .= "<li>Consignee dial code must be no more than 6 characters.</li>";
                $bErrorFlag = true;
            }   
            $this->labelValidationErrorAry = $labelValidationErrorAry;
            return $bErrorFlag;
        }
    }
    
    function createLabelFromTNTApi($data,$test_page=false)
    {    
        $this->szLabelLogStr = "";
        $this->szAccessCode = "";
        $this->iCargoDimensionError = false;
        
        $iDebug = 1;
        $idBooking = $data['idBooking'];
        $outputFileName = __APP_PATH__."/logs/tnt_label_respo_".date('Ymd').".log"; 
        $szCustomerXml = $this->createLabelRequestXml($data,$test_page);    
     
        if($this->iCargoDimensionError==1 || $this->iTermsOfPaymentError==1)
        {
            return false;
        }
        
        try
        {
            $this->createXmlFile($szCustomerXml,$idBooking,true); 
     
            $this->szLabelLogStr = "<p>Calling iConnect API to obtain Access code from TNT for booking ID: ".$idBooking." and Booking Ref: ".$data['szBookingRef']."</p><br>";
            $szAccessApiEndPoint = "http://iconnection.tnt.com/ShipperGate2.asp";
            $szCustomerXml = urlencode($szCustomerXml);
            $AuthResponseXml = $this->httpPost($szAccessApiEndPoint, $szCustomerXml);  
                
            //$AuthResponseXml = "COMPLETE:38443128";  
            if($iDebug==1)
            {
                $szDebugMessage = "Access Code: \n" . print_R($AuthResponseXml,true) . "\n";
                $this->logDebugger($outputFileName,$szDebugMessage);
            }
             
            if(!empty($AuthResponseXml))
            {
                $this->szLabelLogStr .= "<p>Got response for Access Code API</p><br>";
                list($key, $szAccessCode) = explode(":", $AuthResponseXml); 
                if(!empty($szAccessCode))
                {
                    try
                    {
                        $tntLabelDocumentAry = array();
                        $this->szAccessCode = $szAccessCode;
                        $this->szLabelLogStr .= "<p>Access Code: ".$szAccessCode." </p><br>";
                        
                        $this->szLabelLogStr .= "<p>Fetching Tracking code from Shipping API</p>";
                         
                        /*
                        * Fetching Result from TNT API
                        */ 
                        $this->fetchShippingAPIResultFromTNT($data,$szAccessCode);
   
                        if($this->iShippingApiError==1)
                        {  
                            $this->szLabelApiError .= $this->szShippingApiErrorMessage;
                            
                            $szDebugMessage = "<Strong>Shipping API Error</strong>".PHP_EOL."".PHP_EOL.$this->szShippingApiErrorMessage;
                            $this->logTNTApiErrors($iDebug,$szDebugMessage);
                            return false;
                        }
                        else if(!empty($this->szTrackingNumber))
                        {
                            $this->szLabelLogStr .= "<p><br>Calling Manifet API to generate Manifest Pdf</p><br>";  
                            /*
                            * Fetching Manifest PDF from TNT API
                            */ 
                            $szManiFestFilePath = $this->fetchManifestFromTNTAPI($data,$szAccessCode);
                            
                            /*
                            * Fetching Label PDF from TNT API
                            */
                            $szLabelPdfFilePath = $this->fetchLabelFromTNTAPI($data,$szAccessCode);
                             
                            /*
                             * Following 2 pdf document is only required if custom clearance is applicable for the Trade.
                             * 
                             * 1. Consignment Notes PDF
                             * 2. Invoice PDF
                             */
                            if($data['iIncludeCustomClearance']==1)
                            {
                                $this->szLabelLogStr .= "<p><br>Calling Consignment Note API to generate Consignment Notes Pdf</p><br>"; 
                                /*
                                * Fetching Consignment Notes PDF from TNT API
                                */
                                $szConsignmentNoteFilePath = $this->fetchConsignmentNotesFromTNTAPI($data,$szAccessCode);

                                $this->szLabelLogStr .= "<p><br>Calling Invoice API to generate Invoice Pdf</p><br>"; 
                                /*
                                * Fetching Invoice PDF from TNT API
                                */ 
                                $szInvoiceFilePath = $this->fetchInvoiceFromTNTAPI($data,$szAccessCode);
                            }
                            
                            if($this->iRoutingLabelApiError==1)
                            {
                                $this->szLabelApiErrorHeading = "<p>We have successfully received response from Shipping API with Tracking Number: ".$this->szTrackingNumber." but because of following errors we could not able to produce Routing Label automaticall. Please find links for other pdf documents for the booking below.</p><br><ol>";
                                
                                if(!empty($szManiFestFilePath))
                                {
                                    $szManiFestFileUrl = __BASE_URL_TNT__."/labels/".$szManiFestFilePath;
                                    $szManiFestFileAnchorTag = '<a href="'.$szManiFestFileUrl.'" target="_blank">Manifest File URL</a>';
                                    $this->szLabelApiErrorHeading .= '<li>'.$szManiFestFileAnchorTag."</li>";
                                }
                                if(!empty($szConsignmentNoteFilePath))
                                {
                                    $szConsignmentNoteFileURL = __BASE_URL_TNT__."/labels/".$szConsignmentNoteFilePath;
                                    $this->szLabelApiErrorHeading .= "<li>Consignment Notes URL: ".$szConsignmentNoteFileURL."</li>";
                                }
                                if(!empty($szInvoiceFilePath))
                                {
                                    $szInvoiceFileUrl = __BASE_URL_TNT__."/labels/".$szInvoiceFilePath;
                                    $this->szLabelApiErrorHeading .= "<li>Invoice File URL: ".$szInvoiceFileUrl."</li>";
                                } 
                                $this->szLabelApiErrorHeading .= "</ol><br>";
                                
                                $this->szLabelApiError .= $this->szRoutingLabelApiErrorMessage;
                            
                                $szDebugMessage = "<Strong>Routing Label API Error</strong>".PHP_EOL."".PHP_EOL.$this->szRoutingLabelApiErrorMessage;
                                $this->logTNTApiErrors($iDebug,$szDebugMessage);
                                return false;
                            }
                            else
                            {
                                $tntLabelDocumentAry['szLabelPdfFilePath'] = $szLabelPdfFilePath;
                                $tntLabelDocumentAry['szManiFestFilePath'] = $szManiFestFilePath;
                                $tntLabelDocumentAry['szConsignmentNoteFilePath'] = $szConsignmentNoteFilePath;
                                $tntLabelDocumentAry['szInvoiceFilePath'] = $szInvoiceFilePath; 

                                $szDebugMessage = " \n\n Final Response: \n  " . print_R($tntLabelDocumentAry,true) . "\n";
                            }
                            
                            /*
                            * Logging calculation logs 
                            */ 
                            $this->logTNTApiErrors($iDebug,$szDebugMessage);

                            return $tntLabelDocumentAry; 
                        }
                        else
                        {
                            $this->szLabelApiError = "<p>Could not able to fetch Tracking code from Shipping API Response</p>";
                            return false;
                        } 
                    } 
                    catch (Exception $ex)
                    {
                        $this->szLabelLogStr .= "<p>Got exception in obtaining label xml </p><br>"; 
                        $this->szLabelLogStr .= "<p> Exception: ".$ex->getMessage()."</p>"; 
                        return false;
                    } 
                }
                else
                {
                    $this->szLabelLogStr .= "<p>Access Api Response: Got access code - null from API.</p><br>"; 
                    return false;
                }
            }  
            else
            {
                $this->szLabelLogStr .= "<p>Access Api Response: Got empty response from Api</p><br>"; 
                return false;
            }
        } 
        catch (Exception $ex) 
        {
            $this->szLabelLogStr .= "<p>Got exception in obtaining access code </p><br>"; 
            $this->szLabelLogStr .= "<p> Exception: ".$ex->getMessage()."</p>"; 
            return false;
        } 
    } 
    
    function fetchLabelFromTNTAPI($data,$szAccessCode)
    {
        if(!empty($data) && !empty($szAccessCode))
        {
            $iDebug = 0;
            $idBooking = $data['idBooking'];
             
            $szTrackingNumber = $this->szTrackingNumber;
            if(!empty($szTrackingNumber))
            {  
                $this->szLabelLogStr .= "<p><br>Calling Routing Label API to generate Routing Label Pdf</p><br>";  

                $data['szConsignmentNumber'] = $szTrackingNumber;
                $kRoutingLabel = new cRoutingLabels();
                $szPdfFilePath = $kRoutingLabel->createRoutingLabel($data);

                $this->szLabelLogStr .= $kRoutingLabel->szLabelLogStr;
                
                if($kRoutingLabel->iRoutingLabelApiError==1)
                {
                    $this->iRoutingLabelApiError = 1;
                    $this->szRoutingLabelApiErrorMessage = $kRoutingLabel->szRoutingLabelApiErrorMessage;
                    return false;
                } 
                else
                {
                    $this->szLabelPdfFileName = $szPdfFilePath;
                    $this->szLabelHtmlFileName = $this->szHtmlFilePath;
                    return $szPdfFilePath;
                }
            }
            else
            {
                $this->szLabelLogStr .= "<p>Received Invalid Response XML from Shipping API.</p><br>"; 
            } 
        }
    }
    
    function fetchLabelFromTNTAPI_backup($data,$szAccessCode)
    {
        if(!empty($data) && !empty($szAccessCode))
        {
            $iDebug = 0;
            $idBooking = $data['idBooking'];
            /*
            * Calling Curl to fetch Label from TNT API
            */
            $szLabelRequestXml = "GET_LABEL:".$szAccessCode;
            $szLabelApiEndPoint = __TNT_SHIPPING_API_END_POINT__;
            $labelResponseXml = $this->httpPost($szLabelApiEndPoint, $szLabelRequestXml,true); 
             
            if($iDebug==1)
            {
                $szDebugMessage = "Label Response: \n" . print_R($labelResponseXml,true) . "\n";
                $this->logDebugger($outputFileName,$szDebugMessage);
            }
 
            $kWebServices = new cWebServices();
            if(!empty($labelResponseXml) && $kWebServices->_isValidXML($labelResponseXml))
            { 
                /*
                * Updating data content to a xml file.
                */
                $szXmlFilePath = $this->createXmlFile($labelResponseXml,$idBooking,false,'ADDRESS_LABEL'); 
            
                $responseAry = array();  
                $responseAry = cXmlParser::createArray($labelResponseXml); 

                $szRawTrackingNumber = $responseAry['CONSIGNMENTBATCH']['PACKAGE']['CONSIGNMENT']['CONNUMBER'];
                if(empty($szRawTrackingNumber))
                {
                    $szRawTrackingNumber = $responseAry['CONSIGNMENTBATCH']['PACKAGE'][0]['CONSIGNMENT']['CONNUMBER'];
                }
                $this->getFormatedTrackingNumber($szRawTrackingNumber); 
                $szTrackingNumber = $this->szTrackingNumber; 
                $this->szRawTrackingNumber = $szRawTrackingNumber;
                
                if(!empty($szTrackingNumber))
                { 
                    $this->szLabelLogStr .= "<p>Successfully received Response from Shipping API.</p><br>"; 
                    $this->szLabelLogStr .= "<p>Shipment successfully created with Tracking Number: ".$szTrackingNumber."</p><br>";  
                    
                    $this->szLabelLogStr .= "<p>Downloading Barcode Image for Shipping API with CONNUMBER ".$szRawTrackingNumber."</p><br>";  

                    $this->downloadBarcodeImage($szRawTrackingNumber);
                    $this->szLabelLogStr .= "<p><br>Calling Routing Label API to generate Routing Label Pdf</p><br>";  

                    $data['szConsignmentNumber'] = $szTrackingNumber;
                    $kRoutingLabel = new cRoutingLabels();
                    $szPdfFilePath = $kRoutingLabel->createRoutingLabel($data);
                    
                    $this->szLabelLogStr .= $kRoutingLabel->szLabelLogStr;
                    $this->szLabelPdfFileName = $szPdfFilePath;
                    $this->szLabelHtmlFileName = $this->szHtmlFilePath;
                    return $szPdfFilePath;
                }
                else
                {
                    $this->szLabelLogStr .= "<p>Received Invalid Response XML from Shipping API.</p><br>"; 
                }
            } 
            else
            {
                $this->szLabelLogStr .= "<p>No response from Shipping API. Please try again later: ".$labelResponseXml."</p><br>";
            }
        }
    }
    
    function fetchConsignmentNotesFromTNTAPI($data,$szAccessCode)
    { 
        if(!empty($data) && !empty($szAccessCode))
        {
            $iDebug = 0;
            $idBooking = $data['idBooking'];
            /*
            * Calling Curl to fetch Label from TNT API
            */
            $szLabelRequestXml = "GET_CONNOTE:".$szAccessCode;
            $szLabelApiEndPoint = __TNT_SHIPPING_API_END_POINT__;
            $labelResponseXml = $this->httpPost($szLabelApiEndPoint, $szLabelRequestXml,true); 
             
            if($iDebug==1)
            {
                $szDebugMessage = "Manifest Response: \n" . print_R($labelResponseXml,true) . "\n";
                $this->logDebugger($outputFileName,$szDebugMessage);
            }

            /*
            * Updating data content to a xml file.
            */
            $szXmlFilePath = $this->createXmlFile($labelResponseXml,$idBooking,false,'CONNOTE'); 
 
            if(!empty($szXmlFilePath))
            {  
                /*
                * Converting Manifest xml file to PDF Document
                */
                $szPdfFilePath = $this->convertXml2Pdf($szXmlFilePath,$idBooking,false,'CONSIGNMENT_NOTE');
                $this->szLabelPdfFileName = $szPdfFilePath;
                $this->szLabelHtmlFileName = $this->szHtmlFilePath;
                return $szPdfFilePath;
            } 
            else
            {
                $this->szLabelLogStr .= "<p>No response from Shipping API for Manifest. Please try again later: ".$labelResponseXml."</p><br>";
            }
        }
    }
    
    function fetchInvoiceFromTNTAPI($data,$szAccessCode)
    { 
        if(!empty($data) && !empty($szAccessCode))
        {
            $iDebug = 0;
            $idBooking = $data['idBooking'];
            /*
            * Calling Curl to fetch Label from TNT API
            */
            $szLabelRequestXml = "GET_INVOICE:".$szAccessCode;
            $szLabelApiEndPoint = __TNT_SHIPPING_API_END_POINT__;
            $labelResponseXml = $this->httpPost($szLabelApiEndPoint, $szLabelRequestXml,true); 
              
            if($iDebug==1)
            {
                $szDebugMessage = "Manifest Response: \n" . print_R($labelResponseXml,true) . "\n";
                $this->logDebugger($outputFileName,$szDebugMessage);
            }

            /*
            * Updating data content to a xml file.
            */
            $szXmlFilePath = $this->createXmlFile($labelResponseXml,$idBooking,false,'INVOICE'); 
 
            if(!empty($szXmlFilePath))
            {  
                /*
                * Converting Manifest xml file to PDF Document
                */
                $szPdfFilePath = $this->convertXml2Pdf($szXmlFilePath,$idBooking,false,'TNT_INVOICE');
                $this->szLabelPdfFileName = $szPdfFilePath;
                $this->szLabelHtmlFileName = $this->szHtmlFilePath;
                return $szPdfFilePath;
            } 
            else
            {
                $this->szLabelLogStr .= "<p>No response from Shipping API for Manifest. Please try again later: ".$labelResponseXml."</p><br>";
            }
        }
    }
    
    function fetchShippingAPIResultFromTNT($data,$szAccessCode)
    { 
        if(!empty($data) && !empty($szAccessCode))
        {
            $outputFileName = __APP_PATH__."/logs/tnt_label_respo_".date('Ymd').".log"; 
            $iDebug = 0;
            $idBooking = $data['idBooking'];
            /*
            * Calling Curl to fetch Label from TNT API
            */
            $szLabelRequestXml = "GET_RESULT:".$szAccessCode;
            
            $szLabelApiEndPoint = __TNT_SHIPPING_API_END_POINT__;
            $labelResponseXml = $this->httpPost($szLabelApiEndPoint, $szLabelRequestXml,true); 
             
            if($iDebug==1)
            {
                $szDebugMessage = "GET RESULT Response: \n" . print_R($labelResponseXml,true) . "\n";
                $this->logDebugger($outputFileName,$szDebugMessage);
            }   
            if(!empty($labelResponseXml))
            {
                /*
                * Processing Response XML returned by TNT Shipping API.
                */
                $this->processShippingApiResponse($labelResponseXml,$idBooking);   
                return true;
            } 
            else
            {
                $this->iShippingApiError = 1;
                $this->szShippingApiErrorMessage = "<li>Invalid response from SHipping API. Please try again later</li>";
                $this->szLabelLogStr .= "<p>No response from Shipping API for Manifest. Please try again later: ".$labelResponseXml."</p><br>";
                return false;
            }
        }
    }
    
    function fetchManifestFromTNTAPI($data,$szAccessCode)
    {
        if(!empty($data) && !empty($szAccessCode))
        {
            $iDebug = 0;
            $idBooking = $data['idBooking'];
            /*
            * Calling Curl to fetch Label from TNT API
            */
            $szLabelRequestXml = "GET_MANIFEST:".$szAccessCode;
            $szLabelApiEndPoint = __TNT_SHIPPING_API_END_POINT__;
            $labelResponseXml = $this->httpPost($szLabelApiEndPoint, $szLabelRequestXml,true); 
             
            if($iDebug==1)
            {
                $szDebugMessage = "Manifest Response: \n" . print_R($labelResponseXml,true) . "\n";
                $this->logDebugger($outputFileName,$szDebugMessage);
            }

            /*
            * Updating data content to a xml file.
            */
            $szXmlFilePath = $this->createXmlFile($labelResponseXml,$idBooking,false,'MANIFEST'); 
 
            if(!empty($szXmlFilePath))
            {  
                /*
                * Converting Manifest xml file to PDF Document
                */
                $szPdfFilePath = $this->convertXml2Pdf($szXmlFilePath,$idBooking,false,'MANIFEST');
                $this->szLabelPdfFileName = $szPdfFilePath;
                $this->szLabelHtmlFileName = $this->szHtmlFilePath;
                return $szPdfFilePath;
            } 
            else
            {
                $this->szLabelLogStr .= "<p>No response from Shipping API for Manifest. Please try again later: ".$labelResponseXml."</p><br>";
            }
        }
    }
    
    function createXmlFile($szDataString,$idBooking,$bRequestXml=false,$szDocumentType=false)
    { 
        if(!empty($szDataString))
        {   
            if($bRequestXml)
            {
                if(!empty($szDocumentType))
                {
                    $szRequestXmlFileName = $szDocumentType."_API_REQUEST_".$idBooking."_".date('YmdHi').".xml";
                }
                else
                {
                    $szRequestXmlFileName = "label_".$idBooking."_".date('YmdHi').".xml";
                } 
                $szFilePath = __APP_PATH_TNT_XMLS__."/Request/".$szRequestXmlFileName;
                
                $this->szRequestXmlFileName = $szRequestXmlFileName;
                $this->szRequestXmlFilePath = $szFilePath;
            }
            else
            {
                if(!empty($szDocumentType))
                { 
                    $szFilePath = __APP_PATH_TNT_XMLS__."/".$szDocumentType."_API_RESPONSE_".$idBooking."_".date('YmdHi').".xml";
                }
                else
                {
                    $szFilePath = __APP_PATH_TNT_XMLS__."/label_".$idBooking."_".date('YmdHi').".xml";
                } 
            }
            if($bRoutingLabel)
            {
                $this->szLabelLogStr .= "<p>Creating XML file for Routing Label API @ path: ".$szFilePath." </p><br>"; 
            }
            else
            {
                $this->szLabelLogStr .= "<p>Creating XML file with path: ".$szFilePath." </p><br>"; 
            } 
            
            if(file_exists($szFilePath))
            {
                @unlink($szFilePath);
            }
            file_put_contents($szFilePath,$szDataString);
            if(file_exists($szFilePath))
            { 
                //Write permission 128, 16, 2 are for writable for owner, group and other respectively.
                chmod($szFilePath, fileperms($szFilePath) | 128 + 16 + 2);				
            }
            return $szFilePath;
        } 
    }
    
    function convertXml2Pdf($szXmlFilePath,$idBooking,$bTest=false,$szDocumentType='ADDRESS_LABEL',$postSearchAry=array())
    {
        if(!empty($szXmlFilePath) && $idBooking>0)
        {  
            $this->iPdfCreatingError = false;
            $this->szPdfCreatingErrorMessage = false;
            if(file_exists($szXmlFilePath))
            {
                try
                {   
                    $szHtmlString = $this->convertXml2Html($szXmlFilePath,$idBooking,$szDocumentType,$bTest);  
                    if(empty($szHtmlString))
                    {
                        $this->szLabelLogStr .= "<p>Failed to load HTML file from System</p><br>"; 
                        return false;
                    }   
                    if($bTest)
                    {
                        echo $szHtmlString;
                        die;
                    }
                    
                    /*
                    *  Downloading Barcode Image from TNT Server
                    */
                    $szTrackingCode = $this->szTrackingNumber; 
                    $szRawTrackingNumber = $this->szRawTrackingNumber;
                    if(empty($this->szBarcodeImageName) && !empty($szRawTrackingNumber))
                    {
                        $this->downloadBarcodeImage($szTrackingCode);
                    }
                        
                    /*
                     * Generally we are converting 4 types of xml into pdf file
                     * 1. ROUTING_LABEL
                     * 2. MANIFEST
                     * 3. CONSIGNMENT_NOTE
                     * 4. TNT_INVOICE
                     */
                    if($szDocumentType=='ROUTING_LABEL')
                    {
                        $szHtmlString = str_replace(TNT_LABEL_IMAGE_PATH, __BASE_URL_TNT_IMAGES__, $szHtmlString);  
                        $szTNTStyleSheetStr = "";
                        $szHtmlString = str_replace(INCLUDE_TNT_API_STYLE_SHEET, $szTNTStyleSheetStr, $szHtmlString);  
                        
                        $szCustomerOrderReference = trim($postSearchAry['szDeliveryInstructions']); 
                        $szHtmlString = str_replace("TRANSPORTECA_CUSTOMER_ORDER_REFERENCE", $szCustomerOrderReference, $szHtmlString);   
                        $szPrintablePdfStr = $szHtmlString;
                    }
                    else if($szDocumentType=='MANIFEST')
                    {
                        //@TO DO
                        /*
                        * Replacing TNT Logo image for PDF &amp;
                        */
                        $dtInvoiceDate = date("d/m/Y");
                        $szHtmlString = str_replace("TNT_INVOICE_DATE", $dtInvoiceDate, $szHtmlString);  
                        $szHtmlString = str_replace("TRANPORTECA_SPACE_BAR", " ", $szHtmlString);  
                        $szHtmlString = str_replace("TRANPORTECA_AMPERSAND", "&", $szHtmlString);  
                        
                        $szTNTLogoImagePath = __BASE_URL_TNT_IMAGES__."/logo.png";
                        $szTNTLogoImageTag = '<img src="'.$szTNTLogoImagePath.'"/>';
                        $szHtmlString = str_replace("TNT_LOGO_IMAGE_TAG", $szTNTLogoImageTag, $szHtmlString);  
                        $szHtmlString = html_entity_decode($szHtmlString);
                        //$this->szBarcodeImageName = "barcode_GE334903073DK.jpg";
                        
                        if(!empty($this->szBarcodeImageName))
                        {
                            $szBarcodeImagePath = __BASE_URL_TNT_BARCODE_DOWNLOADED_IMAGES__."/".$this->szBarcodeImageName;
                            $szBarcodeImageTag = '<img src="'.$szBarcodeImagePath.'"/>';
                            $szHtmlString = str_replace("TNT_BARCODE_IMAGE_TAG", $szBarcodeImageTag, $szHtmlString);  
                        } 
                        $szPrintablePdfStr = $szHtmlString;
                        $szPrintablePdfStr = $szHtmlString;
                        
                    }
                    else if($szDocumentType=='CONSIGNMENT_NOTE')
                    {
                        //@TO DO
                        /*
                        * Replacing TNT Logo image for PDF
                        */
                         
                        $szTNTLogoImagePath = __BASE_URL_TNT_IMAGES__."/logo.png";
                        $szTNTLogoImageTag = '<img src="'.$szTNTLogoImagePath.'"/>';
                        $szHtmlString = str_replace("TNT_LOGO_IMAGE_TAG", $szTNTLogoImageTag, $szHtmlString);  
                        
                        //$this->szBarcodeImageName = "barcode_GE334903073DK.jpg";
                        if(!empty($this->szBarcodeImageName))
                        {
                            $szBarcodeImagePath = __BASE_URL_TNT_BARCODE_DOWNLOADED_IMAGES__."/".$this->szBarcodeImageName;
                            $szBarcodeImageTag = '<img src="'.$szBarcodeImagePath.'"/>';
                            $szHtmlString = str_replace("TNT_BARCODE_IMAGE_TAG", $szBarcodeImageTag, $szHtmlString);  
                        } 
                        $szPrintablePdfStr = $szHtmlString;
                    }
                    else if($szDocumentType=='TNT_INVOICE')
                    {
                        //@TO DO 
                        $dtInvoiceDate = date("d/m/Y");
                        $szHtmlString = str_replace("TRANPORTECA_SPACE_BAR", "&nbsp;", $szHtmlString);  
                        $szHtmlString = str_replace("TNT_INVOICE_DATE", $dtInvoiceDate, $szHtmlString);  
                        //$szHtmlString = preg_replace_callback('#<script(.*?)>(.*?)</script>#is', '', $szHtmlString);
                        
                        $szPrintablePdfStr = $szHtmlString;
                    }
                    else
                    {
                        $szPdfString = $szHtmlString;
                        $imgOpeningTag = strpos ($szPdfString,IMG_OPENING_TAG);
                        $imgClosingTag = strpos ($szPdfString,IMG_CLOSING_TAG);
                        $iLength = ($imgClosingTag - $imgOpeningTag - 15);

                        $szTrackingNumber = substr($szPdfString, $imgOpeningTag+15, $iLength); 
                        $barcodeImagesStr = $this->getBarcodeImage($szTrackingNumber);

                        $this->szLabelLogStr .= "<p>Tracking Number: ".$szTrackingNumber."</p><br>"; 

                        $szIMGDefinedStr = "IMG_OPENING_TAG".$szTrackingNumber."IMG_CLOSING_TAG";
                        $szPrintablePdfStr = str_replace($szIMGDefinedStr, $barcodeImagesStr, $szPdfString); 
                        $this->getFormatedTrackingNumber($szTrackingNumber);
                        $this->szHtmlString = $szPrintablePdfStr;
                    }
                    $this->szPdfString = $szPrintablePdfStr;
                    $this->szHtmlString = $szPrintablePdfStr;
                   // echo $szPrintablePdfStr;
                    //die;
                    
                    if($bTest)
                    { 
                        return false;
                    }
                    else
                    { 
                        /*
                        * Creating a pdf file using Label out put
                        */
                        $szPdfFileName = $this->createLabelPdfFile($szPrintablePdfStr,$idBooking,$szDocumentType); 
                        return $szPdfFileName;
                    } 
                } 
                catch (Exception $ex)
                {
                    $this->iPdfCreatingError = 1;
                    $this->szPdfCreatingErrorMessage = "Exception: ".$ex->getMessage();
                } 
            } 
            else
            {
                $this->iPdfCreatingError = 1;
                $this->szPdfCreatingErrorMessage = "Error: Xml file not exists.";
            }
        }
    }
    
    function convertXml2Html($szXmlFilePath,$idBooking,$szDocumentType="ADDRESS_LABEL",$bOriginal=false)
    {
        try
        { 
            $szHtmlFileName = $szDocumentType."_".$idBooking."_".date('YmdHis')."_".mt_rand(0, 99999).".html";
            $szHtmlPath = __APP_PATH_TNT_HTML__."/".$szHtmlFileName;
            $szHtmlUrl = __BASE_URL_TNT_HTML__."/".$szHtmlFileName;

            /*
             * To create a HTML we need a xml stylesheet called(.xsl) file and we are choosing xsl depending on document type
             */
            
            /*
            *  $bOriginal flag is only used for testing purpose. I mean to compare original document and Document created by Transporteca System.
            */
            if($szDocumentType=='ROUTING_LABEL')
            { 
                if($bOriginal)
                {
                    $szXslPath = __APP_PATH__."/TNT/xsl/HTMLRoutingLabelRenderer_org.xsl";
                }
                else
                {
                    $szXslPath = __APP_PATH_TNT_XSL__."/HTMLRoutingLabelRenderer.xsl";
                } 
            }
            else if($szDocumentType=='MANIFEST')
            {
                if($bOriginal)
                {
                    $szXslPath = __APP_PATH_TNT_XSL__."/Manifest_org.xsl";
                }
                else
                {
                    $szXslPath = __APP_PATH_TNT_XSL__."/Manifest.xsl";
                } 
            } 
            else if($szDocumentType=='CONSIGNMENT_NOTE')
            {
                if($bOriginal)
                {
                    $szXslPath = __APP_PATH_TNT_XSL__."/Connotes_org.xsl";
                }
                else
                {
                    $szXslPath = __APP_PATH_TNT_XSL__."/Connotes.xsl";
                } 
            } 
            else if($szDocumentType=='TNT_INVOICE')
            {
                if($bOriginal)
                {
                    $szXslPath = __APP_PATH_TNT_XSL__."/Invoice_org.xsl";
                }
                else
                {
                    $szXslPath = __APP_PATH_TNT_XSL__."/Invoice.xsl";
                } 
            }
            else
            {
                $szXslPath = __APP_PATH__."/TNT/tnt-label.xsl";
            } 
            $this->szLabelLogStr .= "<p>Creating HTML file with path: ".$szHtmlPath." </p><br>"; 

            if(file_exists($szHtmlPath))
            {
                @unlink($szHtmlPath);
            } 
            
            // Load the XML source
            $xml = new DOMDocument;
            $xml->load($szXmlFilePath);

            $xsl = new DOMDocument;
            $xsl->load($szXslPath);
            libxml_use_internal_errors(true);

            // Configure the transformer
            $proc = new XSLTProcessor;
            $result = $proc->importStyleSheet($xsl); // attach the xsl rules
            if(!$result)
            { 
                foreach (libxml_get_errors() as $error) {
                    $this->szLabelLogStr .= "<br>Libxml error: {$error->message}\n";
                } 
            }
            else
            {
                $proc->transformToURI($xml, $szHtmlPath);
            }
            libxml_use_internal_errors(false); 
            $this->szHtmlFilePath = $szHtmlPath;
            
            if(file_exists($szHtmlPath))
            { 
                $this->szLabelLogStr .= "<p>HTML file successfully created at: ".$szHtmlUrl." </p><br>"; 
                //Write permission 128, 16, 2 are for writable for owner, group and other respectively.
                chmod($szHtmlPath, fileperms($szHtmlPath) | 128 + 16 + 2);
                
                $kWhsSearch = new cWHSSearch();
                $szHtmlString = $this->getPage($szHtmlUrl);
            }     
            else
            {
                echo "<br>ERROR while creating HTML page. ".$szHtmlPath;
            } 
            return $szHtmlString;
        }
        catch (Exception $ex)
        { 
            $this->szLabelLogStr .= "<p>Error in converting XML file to HTML in function cLabel::convertXml2Html() </p><br>";
            $this->iPdfCreatingError = 1;
            return false;
        }
    }
    
    function getFormatedTrackingNumber($szTrackingNumber)
    {
        if(!empty($szTrackingNumber))
        { 
            /*
            * We are trimming 2 char from start and 2 char from end so if we get Tracking number like: 'GE123456789IE' then we trim 'GE' from start and 'IE' from last. So the final tracking number will be 123456789.
            */
            $szTrackingNumber = substr($szTrackingNumber,2); 
            $szTrackingNumber = substr($szTrackingNumber,0,strlen($szTrackingNumber)-2); 
            $this->szTrackingNumber = $szTrackingNumber;
        }
    }
    function getBarcodeImage($szTrackingNumber)
    {
        if(!empty($szTrackingNumber))
        { 
            $barcodeAry = array();
            $barcodeAry = str_split($szTrackingNumber);
            $szImgStr = "";
            if(!empty($barcodeAry))
            {
                $szImgTags = '<img src="'.__BASE_URL_TNT_BARCODE_PNG_IMAGES__.'/tbc_star.png" style="height:30px;width:14px;text-align:center;top:10px;" alt="*"/>';
                foreach($barcodeAry as $barcodeArys)
                { 
                    if(!empty($barcodeArys))
                    {
                        $szImgPath = __BASE_URL_TNT_BARCODE_PNG_IMAGES__."/tbc_".$barcodeArys.".png";
                        $szImgTags .= '<img src="'.$szImgPath.'" style="height:30px;width:14px;text-align:center;top:10px;" alt="'.$barcodeArys.'"/>';
                    } 
                }
                $szImgTags .= '<img src="'.__BASE_URL_TNT_BARCODE_PNG_IMAGES__.'/tbc_star.png" style="height:30px;width:14px;text-align:center;top:10px;" alt="*"/>'; 
            }  
            return $szImgTags;
            //$szImgTags = '<img src="http://dev.transporteca.com/TNT/barcode_generator/images/GE329534693DK20161226121201.png" width="140px" height="30px;"/>';
        }
    }
    function getPage($str_url)
    { 
        if(!empty($str_url))
        {
            $curl = curl_init();
            //curl_setopt ($curl, CURLOPT_REFERER, strFOARD);
            curl_setopt ($curl, CURLOPT_URL, $str_url);
            curl_setopt ($curl, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)");
            curl_setopt ($curl, CURLOPT_HEADER, 0);
            curl_setopt ($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt ($curl, CURLOPT_BINARYTRANSFER, true); 
            curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, 0);
            $html = curl_exec ($curl);
            curl_close ($curl);
            return $html;
        } 
    }
    function createLabelPdfFile($szPdfString,$idBooking=false,$szDocumentType=false)
    {
        if(!empty($szPdfString))
        {  
            $this->szLabelLogStr .= "<p>Creating booking label PDF file.</p><br>";   
            $iTcpdfFlag = 1; 
            if($iTcpdfFlag==1)
            {        
                //$szPdfString = utf8_encode($szPdfString);
                
                require_once(__APP_PATH__.'/TNT/tcpdf/tcpdf.php'); 
                // create new PDF document
                $pdf = new cLabelPdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false); 
                if($szDocumentType=='ROUTING_LABEL')
                {
                    //set page margins left top right
                    $pdf->SetMargins(4, 4, 4);
                } 

                // set auto page breaks and bottom margin
                $pdf->SetAutoPageBreak(true, 10);
                // set page font
                //$pdf->SetFont('freeserif', 'NN', 12); 

                $pdf->AddPage();
                // Form validation functions
                 
                // Print text using writeHTMLCell()
                $pdf->writeHTML($szPdfString, true, false, true, false,'');
                // reset pointer to the last page
                $pdf->lastPage();
                if($szDocumentType=='CONSIGNMENT_NOTE' || $szDocumentType=='ROUTING_LABEL' || $szDocumentType=='MANIFEST')
                {
                    $lastPage = $pdf->getPage();
                    $pdf->deletePage($lastPage);
                }
            }
            else
            {
                require_once(__APP_PATH__.'/forwarders/html2pdf/html2pdfBooking.php'); 
                $pdf=new HTML2FPDFBOOKING();
                $pdf->AddPage('',true); 
                $pdf->WriteHTML($szPdfString,true);  
            } 
            try
            {  
                if(!empty($szDocumentType))
                {
                    $szPdfFileName = "TP_".$szDocumentType."_".$idBooking."_".date('Ymd')."_".mt_rand(1,99999).".pdf";
                }
                else
                {
                    $szPdfFileName = "label_".$idBooking."_".date('Ymd')."_".mt_rand(1,99999).".pdf";
                } 
                $szPdfFilePath = __APP_PATH_TNT_LABEL__."/".$szPdfFileName;

                $this->szLabelLogStr .= "<p>Trying to create PDF file at path: ".$szPdfFilePath." </p><br>"; 
                $this->szPdfFileNameOriginal = $szPdfFileName;
                
                if(file_exists($szPdfFilePath))
                {
                    @unlink($szPdfFilePath);
                }  
                $pdf->Output($szPdfFilePath,'F'); 
                
                if(file_exists($szPdfFilePath))
                { 
                    $this->szLabelLogStr .= "<p>PDF file successfully created at path: ".$szPdfFilePath." </p><br>"; 
                    //Write permission 128, 16, 2 are for writable for owner, group and other respectively.
                    chmod($szPdfFilePath, fileperms($szPdfFilePath) | 128 + 16 + 2);				
                }  
                return $szPdfFileName;
            }
            catch (Exception $ex) 
            {
                $this->iPdfCreatingError = 1;
                $this->szPdfCreatingErrorMessage = "Exception: Got exception while write content to pdf file.\n Message: ".$ex->getMessage();
                $this->szLabelLogStr .= $this->szPdfCreatingErrorMessage; 
                return false;
            } 
            
        } 
    }
    
    function httpPost($url, $strRequest,$bLabelFlag=false)  
    {
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url);
         
        if($bLabelFlag)
        {
            //echo "Request: ".$strRequest;
        }
        else
        {
            curl_setopt ($ch, CURLOPT_PORT , 81); 
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
            curl_setopt($ch, CURLOPT_TIMEOUT, 180);
        }
        curl_setopt($ch, CURLOPT_POST, true); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, "xml_in=" . $strRequest);
        curl_setopt($ch, CURLOPT_HEADER, 'Content-type: application/x-www-form-urlencoded');
        curl_setopt($ch, CURLOPT_HEADER, 'User-Agent: ShipperGate_socket/1.0');
        curl_setopt($ch, CURLOPT_HEADER, "Content-length: " . strlen("xml_in=" . $strRequest));
        curl_setopt($ch, CURLOPT_HEADER, 'Accept: */*');
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
 
        if($bLabelFlag)
        { 
            $result = mb_convert_encoding($result,'ISO-8859-1','utf-8'); 
        }
        if ( $httpCode == 200 )
        {
            return $result;
        } 
        else 
        {
            $szExceptionMessage = "Return code is {$httpCode} \n" .curl_error($ch);
            throw new Exception($szExceptionMessage);
        }
    }
    
    function getCargoDimensions($postSearchAry)
    {
        if(!empty($postSearchAry))
        { 
            $kBooking = new cBooking();
            $kWHSSearch = new cWHSSearch();
            $this->iTotalNumColliForAPI = 0;
            $idBooking =  $postSearchAry['idBooking'];
            $bookingCargoAry = $kBooking->getCargoDeailsByBookingId($idBooking,true); 
             
            $cargoLineDescriptionAry = array();
            $cargoLineDescriptionAry = $postSearchAry['cargoLineDescriptionAry'];
            $iDisplayIndividualLineCargoDescField = $postSearchAry['iDisplayIndividualLineCargoDescField'];
                     
            $bDefaultPackageFlag = true;
            $iTotalNumColliForAPI = 0; 
            if(!empty($bookingCargoAry))
            {
                $ctr=1;
                $iQuantity = 0;
                $bDefaultPackageFlag = false;
                $parcel_params = array();
                foreach($bookingCargoAry as $bookingCargoArys)
                {
                    if($postSearchAry['iPalletType']>0 && (float)$bookingCargoArys['fHeight']<=0)
                    {
                        /*
                        * If shipment type is pallet and height is empty then we use standard height as per pallet type
                        */
                        if($postSearchAry['iPalletType']==1) //Euro Pallet
                        {
                            $bookingCargoArys['fHeight'] = __STANDARD_HEIGHT_EURO_PALLET__;
                        }
                        else if($postSearchAry['iPalletType']==2) //Half Pallet
                        {
                            $bookingCargoArys['fHeight'] = __STANDARD_HEIGHT_HALF_PALLET__;
                        }
                    }
                    if($bookingCargoArys['idWeightMeasure']==1)
                    {
                        $fKgFactor = 1;
                    }
                    else
                    { 
                        $fKgFactor = $kWHSSearch->getWeightFactor($bookingCargoArys['idWeightMeasure']); 
                    }
                    if($bookingCargoArys['idCargoMeasure']==1)
                    {
                        $fCmFactor = 1;
                    }
                    else
                    {
                        $fCmFactor = $kWHSSearch->getCargoFactor($bookingCargoArys['idCargoMeasure']);  
                    }
                    
                    $iLenght = 0;
                    $iWidth = 0;
                    $iHeight = 0;
                    $fWeight = 0;
                    $iQuantity = $bookingCargoArys['iQuantity'];
                    $iNumColli = $bookingCargoArys['iColli'];
                    $idCargo = $bookingCargoArys['id'];
                    
                    if($postSearchAry['iSearchMiniVersion']==7)
                    {
                        /*
                        * For Voga booking we always consider iColli instead of iQuantity
                        */
                        //$iQuantity = $iNumColli;
                    }  
                    
                    if($fCmFactor>0)
                    {
                        //Converting to Cm
                        $iLength = round($bookingCargoArys['fLength'] / $fCmFactor);
                        $iWidth = round($bookingCargoArys['fWidth'] / $fCmFactor);
                        $iHeight = round($bookingCargoArys['fHeight'] / $fCmFactor); 
                    } 
                    if($fKgFactor>0)
                    {
                        $fWeight = ceil(($bookingCargoArys['fWeight']/$fKgFactor)) ; 
                    }
                    
                    if($iDisplayIndividualLineCargoDescField==1 && !empty($cargoLineDescriptionAry[$bookingCargoArys['id']]))
                    {
                        /*
                        * If value of cargo Line is updated in 'Shipping Instruction' popup then we use that value
                        */
                        $szCommodity = $cargoLineDescriptionAry[$bookingCargoArys['id']];
                    }
                    else if(!empty($bookingCargoArys['szCommodity']))
                    {
                        $szCommodity = $bookingCargoArys['szCommodity'];
                    } 
                    else
                    {
                        $szCommodity = $postSearchAry['szCargoDescription'];
                    } 
                    
                    if(strlen($szCommodity)>60)
                    {
                        /*
                        * Normally this case never happens but to be sure had added this additional check.
                        */
                        $szCommodity = substr($szCommodity,0,58);
                    } 
                    
                    if($postSearchAry['iDisplayIndividualLinePieceDescField']==1 && !empty($postSearchAry['pieceDescriptionAry']))
                    {
                        $szSellerReference = trim($postSearchAry['pieceDescriptionAry'][$idCargo]); 
                    }
                    else
                    {
                        $szSellerReference = trim($bookingCargoArys['szSellerReference']);
                        if(empty($szSellerReference))
                        {
                            $szSellerReference = substr($szCommodity,0,18);
                        }  
                    }
                     
                    $iColliCounter = 0;
                    for($i=0;$i<$iQuantity;$i++)
                    {   
                        if($postSearchAry['iSearchMiniVersion']==7) //Voga booking
                        {
                            $iColiCounter = (int)($iNumColli/$iQuantity);
                             
                            for($j=0;$j<$iColiCounter;$j++)
                            {
                                $szCargoDescription = $szCommodity; 
                                if($iColiCounter>1)
                                {
                                    //$szCargoDescription = $szCommodity." (".($j+1)." of ".$iColiCounter.")";   
                                    $szPieceDescription = $szSellerReference." ".($iColliCounter+1)."/".$iNumColli;
                                }
                                else
                                { 
                                    if($iQuantity>1)
                                    {
                                        $szPieceDescription = $szSellerReference." ".($i+1)."/".$iQuantity;
                                    }
                                    else
                                    {
                                        $szPieceDescription = $szSellerReference;
                                    } 
                                }
                                /*
                                * For Voga bookings, any colli after the first one is updated with length, height and width 10cm and weight 1kg
                                */
                                if($j==0)
                                {
                                    // create parcel
                                    $parcel_params[] = array(
                                        "length" => $iLength,
                                        "width"  => $iWidth,
                                        "height" => $iHeight, 
                                        "weight" => $fWeight,
                                        "count"  => 1,
                                        "description" => $szCargoDescription,
                                        "szPieceDescription" => $szPieceDescription,
                                        "idCargo" => $idCargo
                                    ); 
                                }
                                else
                                {
                                    // create parcel
                                    $parcel_params[] = array(
                                        "length" => 10,
                                        "width"  => 10,
                                        "height" => 10, 
                                        "weight" => 1,
                                        "count"  => 1,
                                        "description" => $szCargoDescription,
                                        "szPieceDescription" => $szPieceDescription,
                                        "idCargo" => $idCargo
                                    ); 
                                }
                                $iTotalNumColliForAPI++;
                                $iColliCounter++;
                            } 
                        }
                        else
                        {  
                            if($iQuantity>1)
                            {
                                $szPieceDescription = $szSellerReference." ".($i+1)."/".$iQuantity;
                            }
                            else
                            {
                                $szPieceDescription = $szSellerReference;
                            }
                            
                            // create parcel
                            $parcel_params[] = array(
                                "length" => $iLength,
                                "width"  => $iWidth,
                                "height" => $iHeight, 
                                "weight" => $fWeight,
                                "count"  => 1,
                                "description" => $szCommodity,
                                "szPieceDescription" => $szPieceDescription,
                                "idCargo" => $idCargo
                            ); 
                            $iTotalNumColliForAPI++;
                            $iColliCounter++;
                        }  
                    }  
                }
            } 
            if($bDefaultPackageFlag)
            {
                $szCommodity = $postSearchAry['szCargoDescription'];
                $kCourierServices = new cCourierServices();
                $productProviderArr = $kCourierServices->getProductProviderLimitations($postSearchAry['idGroup'],true);  
                $bWeightLimitationDefined = false; 
                if(!empty($productProviderArr))
                {
                    foreach($productProviderArr as $productProviderArrs)
                    {
                        if($productProviderArrs['idCargoLimitationType']==__WEIGHT_PER_COLI__)
                        {
                            if($productProviderArrs['szRestriction']=='Maximum')
                            {
                                $maxWeightPerColli = $productProviderArrs['szLimit']; 
                                $bWeightLimitationDefined = true;
                            }  
                        } 
                        if($productProviderArrs['idCargoLimitationType']==__CHARGEABLE_WEIGHT_PER_COLI__)
                        {
                            if($productProviderArrs['szRestriction']=='Maximum')
                            {
                                $maxChargeableWeight = $productProviderArrs['szLimit']; 
                                $bWeightLimitationDefined = true;
                            } 
                        }
                    }
                }

                $changeFlag=false; 
                $fCargoWeight = $postSearchAry['fCargoWeight'];
                $fCargoVolume = $postSearchAry['fCargoVolume']; 
                $cargeAbleFlag=false;

                $fRmaingChargeableWeight = 0;
                $fRmaingWeight = 0;

                $fCargoActualWeight = $postSearchAry['fCargoWeight'];

                if(($fCargoVolume*200)>$fCargoWeight)
                {
                    $cargeAbleFlag=true;
                    $fCargoWeight = $postSearchAry['fCargoVolume']*200 ; 
                }   
                $fCargoChargeableWeight = $fCargoWeight; 
                if($bWeightLimitationDefined)
                { 
                    $changeFlag=true;
                    $iTotalNumActulalColli=1;
                    if($maxWeightPerColli>0.00)
                    {
                        $fRmaingWeight = (float)($fCargoActualWeight % $maxWeightPerColli);
                        $iTotalNumActulalColli = (int)($fCargoActualWeight / $maxWeightPerColli);
                        $t=0;
                        if((int)$fRmaingWeight>0)
                        {
                            $iTotalNumActulalColli = $iTotalNumActulalColli + 1;
                        } 
                    }
                    $iTotalChargeNumColli=1;
                    if($maxChargeableWeight>0.00)
                    {                    
                        $fRmaingChargeableWeight = (float)($fCargoChargeableWeight % $maxChargeableWeight);
                        $iTotalChargeNumColli = (int)($fCargoChargeableWeight / $maxChargeableWeight);
                        $t=0;
                        if((int)$fRmaingChargeableWeight>0)
                        {
                            $iTotalChargeNumColli = $iTotalChargeNumColli + 1;
                        } 
                    }  
                    $fRmaingWeight = 0;  
                    if($iTotalChargeNumColli>=$iTotalNumActulalColli)
                    { 
                        $cargoWeight=$fCargoChargeableWeight/$iTotalChargeNumColli;
                        $iTotalNumColli=$iTotalChargeNumColli;
                        $fRmaingWeight = $fCargoChargeableWeight - ($cargoWeight*$iTotalNumColli);
                    }
                    else if($iTotalChargeNumColli<$iTotalNumActulalColli)
                    {
                        $cargoWeight=$fCargoActualWeight/$iTotalNumActulalColli;
                        $iTotalNumColli=$iTotalNumActulalColli;
                        $fRmaingWeight = $fCargoActualWeight - ($cargoWeight*$iTotalNumColli);
                    }
                    else
                    {
                        $changeFlag=true;
                        $iTotalNumColli = 1;
                        $cargoWeight = $fCargoWeight;
                        $fRmaingWeight = 0;
                    } 
                }
                else
                {
                    $changeFlag=true;
                    $iTotalNumColli = 1;
                    $cargoWeight = $fCargoWeight;
                    $fRmaingWeight = 0;
                }
 
                $totalPackage = $iTotalNumColli;  
                if($changeFlag)
                { 
                    if((float)$fRmaingWeight>0.00)
                    {
                        $totalMaxPackage = $iTotalNumColli-1;
                    }
                    else
                    {
                        $totalMaxPackage = $iTotalNumColli;
                    } 

                    if($iTotalNumColli>0)
                    {
                        $fCargoVolume = $fCargoVolume/$iTotalNumColli ;
                    }

                    /*
                    * Coverting dimensions to Centimeters(CM)
                    */
                    $fCalculativeValue = pow($fCargoVolume, 0.333333);
                    $iLengthCm = round((100*$fCalculativeValue),2);
                    $iWidthCm = $iLengthCm;
                    $iHeightCm = $iLengthCm;
                    $iLenghtCm = $iLengthCm ; 
                    
                    if(strlen($szCommodity)>20)
                    {
                        $szCommodity = substr($szCommodity,0,20);
                    }
                    
                    $szSellerReference = $szCommodity;
                    if($totalMaxPackage>1 && strlen($szSellerReference)>19)
                    {
                        $szSellerReference = substr($szSellerReference,0,19);
                    }
                    else if(strlen($szSellerReference)>23)
                    {
                        $szSellerReference = substr($szSellerReference,0,23);
                    }
                    
                    $t=0; 
                    for($i=0;$i<$totalMaxPackage;++$i)
                    { 
                        if($totalMaxPackage>1)
                        {
                            $szPieceDescription = $szSellerReference." ".($i+1)."/".$totalMaxPackage;
                            $szCargoDescription = $szCommodity." (".($i+1)." of ".$totalMaxPackage.")";   
                        }
                        else
                        {
                            $szPieceDescription = $szSellerReference;
                            $szCargoDescription = $szCommodity;
                        }
                        $fWeightKg = $cargoWeight;   
                        // create parcel
                        $parcel_params[] = array(
                            "length" => $iLenghtCm,
                            "width"  => $iWidthCm,
                            "height" => $iHeightCm, 
                            "weight" => $fWeightKg,
                            "count"  => '1',
                            "description" => $szCargoDescription,
                            "szPieceDescription" => $szPieceDescription 
                        );  
                        ++$t;
                        $iTotalNumColliForAPI++;
                    } 

                    if((float)$fRmaingWeight>0.00)
                    {
                        $fWeightKg = $fRmaingWeight;  
                        // create parcel
                        $parcel_params[] = array(
                            "length" => $iLength,
                            "width"  => $iWidth,
                            "height" => $iHeight, 
                            "weight" => $fWeight,
                            "count" => '1',
                            "description" => $szCommodity
                        );   
                        $iTotalNumColliForAPI++;
                    }
                }   
            }  
            $this->iTotalNumColliForAPI = $iTotalNumColliForAPI;  
            return $parcel_params;
        } 
    }
    
    function convertGif2Png($szGifImagePath,$szPngImageName)
    {
        if(!empty($szGifImagePath) && !empty($szPngImageName) && file_exists($szGifImagePath))
        {
            $image = imagecreatefromgif($szGifImagePath);
            
            $szPngImagePath = __APP_PATH_TNT_IMAGES__."/".$szPngImageName;
            if(file_exists($szPngImagePath))
            {
                @unlink($szPngImagePath);
            }
            imagepng($image,$szPngImagePath);
            return true;
        }
    }
    
    function getTNTApiServiceCode($szApiCode)
    {
        if(!empty($szApiCode))
        {
            if(__ENVIRONMENT__ == "LIVE")
            {
                $query_where = " szEasyPostApiCode = '".mysql_escape_custom($szApiCode)."' ";
            }
            else
            {
                $query_where = " szPostmenApiCode = '".mysql_escape_custom($szApiCode)."' ";
            }
            $query="
                SELECT
                    szTNTApiCode
                FROM
                    ".__DBC_SCHEMATA_COURIER_PRODUCT_TNT_MAPPING_SERVICE_CODE__."
                WHERE
                    $query_where
            ";
            //echo $query ;
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $row = $this->getAssoc($result);
                    return $row['szTNTApiCode'];
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    
    function getFormatedLocalTime($iKey)
    { 
	$fColumnData = array('00:00','00:30','01:00','01:30','02:00','02:30','03:00','03:30',
	'04:00','04:30','05:00','05:30','06:00','06:30','07:00','07:30','08:00','08:30','09:00',
	'09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30',
	'15:00','15:30','16:00','16:30','17:00','17:30','18:00','18:30','19:00','19:30','20:00',
	'20:30','21:00','21:30','22:00','22:30','23:00','23:30');
	
	return $fColumnData[$iKey]; 
    }
    
    function getErrorCode()
    {
        return $this->errorCode;
    }
    function getErrorMessage()
    {
        return $this->errorMessage;
    }
    function getSocketResponse()
    {
        return $this->socketResponse;
    }
    
    
    function getAllNewBookingWithCourier($idBooking=0,$flag=__LABEL_NOT_SENT__,$count_rows=false,$header_notification=false)
    { 
        if($flag==__LABEL_SENT__)
        {
            $query_and = " b.idBookingStatus IN ('3','4') AND  (b.idServiceProvider>0 OR b.isManualCourierBooking=1)  AND fl.iLabelStatus=".__LABEL_SEND_FLAG__."";

            if($header_notification)
            {
                $query_and .= " AND DATE(b.dtLabelSent) < '".date('Y-m-d',strtotime('-1 DAYS'))."' AND b.dtSnooze < now() ";
            }
        }
        else if($flag==__LABEL_DOWNLOAD__)
        {
            $query_and = " b.idBookingStatus IN ('3','4') AND
                (b.idServiceProvider>0 OR b.isManualCourierBooking=1) AND fl.iLabelStatus=".__LABEL_DOWNLOAD_FLAG__.""; 
        }
        else
        {
            $query_and = " b.idBookingStatus IN ('3','4') AND
                (b.idServiceProvider>0 OR b.isManualCourierBooking=1)";

            if($header_notification)
            {
                $query_and .= " AND b.dtBookingConfirmed < '".date('Y-m-d H:i:s',strtotime('-24 HOURS'))."' AND b.dtSnooze < now() AND fl.dtSnoozeDate < now() AND fl.iSnoozeAdded='0'";
            }
            
            $query_and .= " AND (fl.iLabelStatus=".__LABEL_NOT_SEND_FLAG__." || (fl.szForwarderBookingTask = 'T140' && fl.iLabelStatus!=".__LABEL_SEND_FLAG__." && fl.iLabelStatus!=".__LABEL_DOWNLOAD_FLAG__."))";
        } 
        if($header_notification)
        {
            $query = "
                SELECT
                    count(b.id) iNumBookings
                FROM
                    ".__DBC_SCHEMATA_BOOKING__." as b
                LEFT JOIN
                    ".__DBC_SCHEMATA_FILE_LOGS__." AS fl
                ON        
                    fl.id=b.idFile
                    ".$inner_query."		
                WHERE
                    $query_and
                    $queryWhere 
                ORDER BY
                    b.dtBookingConfirmed DESC	 
            ";
        }
        else
        {        
            if($count_rows)
            {
                $query_select = " count(b.id) iNumBookings ";
            }
            else
            {
                $query_select = " 
                    b.id,
                    b.idServiceProvider,
                    b.idServiceProviderProduct,
                    b.dtBookingConfirmed,
                    b.szBookingRef,
                    b.idServiceType,
                    b.szWarehouseFromCity,
                    b.szWarehouseToCity,
                    b.fCargoVolume,
                    b.fCargoWeight,
                    b.idForwarder,
                    b.iSendTrackingUpdates,
                    b.szForwarderDispName as szDisplayName,
                    b.idWarehouseFromCountry,
                    b.idWarehouseToCountry,
                    b.szForwarderCurrency,
                    b.fTotalPriceForwarderCurrency,
                    b.iOriginCC,
                    b.iDestinationCC,
                    b.dtBookingCancelled,
                    b.fTotalPriceUSD as fBookingPriceUSD,
                    b.szCustomerCurrency,
                    b.fTotalPriceCustomerCurrency,
                    b.dtCutOff,
                    b.dtWhsCutOff,
                    b.iBookingCutOffHours,
                    b.iBookingLanguage,
                    b.iQuotesStatus,
                    b.idOriginCountry,
                    b.idDestinationCountry,
                    b.szTransportMode,
                    b.idBookingStatus,
                    (SELECT sp1.szShipperCity FROM ".__DBC_SCHEMATA_SHIPPER_CONSIGNEE__. " sp1 WHERE sp1.idBooking = b.id ) as szShipperCity,
                    (SELECT sp2.szConsigneeCity FROM ".__DBC_SCHEMATA_SHIPPER_CONSIGNEE__. " sp2 WHERE sp2.idBooking = b.id ) as szConsigneeCity, 
                    b.szTrackingNumber,
                    b.dtLabelSent,
                    b.dtLabelDownloaded,
                    b.idLabelDownloadedBy,
                    b.szBookingRandomNum,
                    b.dtSnooze,
                    (SELECT cbd.iCourierAgreementIncluded FROM ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__. " cbd WHERE cbd.idBooking = b.id ) as iCourierAgreementIncluded,
                    fl.dtSnoozeDate,
                    fl.iSnoozeAdded
                ";
            }
            $queryWhere='';
            if((int)$idBooking>0)
            {
                $query_and = "(b.idServiceProvider>0 OR isManualCourierBooking=1) ";
                $queryWhere="
                AND
                    b.id='".(int)$idBooking."'	

                ";
            }
            
            $courierProviderProductAry = array();
            $kCourierService = new cCourierServices();
            $courierProviderProductAry = $kCourierService->getAllProductByApiCodeMapping(true);

            $courierAgreementDataAry = array();
            $courierAgreementDataAry = $this->getCourierAgreemetProviderProductData();
            
            $query = "
                SELECT
                    $query_select
                FROM
                    ".__DBC_SCHEMATA_BOOKING__." as b  
                INNER JOIN
                    ".__DBC_SCHEMATA_FILE_LOGS__." AS fl
                ON        
                    fl.id=b.idFile
                    ".$inner_query."		
                WHERE
                    $query_and
                    $queryWhere 
                ORDER BY
                    b.dtBookingConfirmed DESC	 
            ";
        }
     // echo nl2br($query);
//         die;
        if($result=$this->exeSQL($query))
        {
            if($this->iNumRows>0)
            {
                if($count_rows)
                {
                    $row = $this->getAssoc($result);
                    return $row['iNumBookings'];
                }
                else
                {
                    $bookingAry = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {
                        $bookingAry[$ctr]=$row;
                        $bookingAry[$ctr]['szProviderName'] = $courierAgreementDataAry[$row['idServiceProvider']]['szProviderName'];
                        $bookingAry[$ctr]['idProvider'] = $courierAgreementDataAry[$row['idServiceProvider']]['idCourierProvider'];
                        $bookingAry[$ctr]['szProviderProductName'] = $courierProviderProductAry[$row['idServiceProviderProduct']]['szCourierProductName']; 
                        $bookingAry[$ctr]['iBookingIncluded'] = $row['iCourierAgreementIncluded']; 
                        $ctr++;
                    }
                    return $bookingAry;
                }
            } 
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
    
    function getCourierAgreemetProviderProductData()
    {    
    	$query="
            SELECT 
                cp.szName as szProviderName,  
                ced.id as idServiceProvider,
                cp.id as idCourierProvider
            FROM
                ".__DBC_SCHEMATA_COUIER_AGREEMENT_DATA__." AS ced
            INNER JOIN
                ".__DBC_SCHEMATA_COUIER_PROVIDER__." AS cp
            ON	
                cp.id = ced.idCourierProvider 
            WHERE 
                cp.isDeleted='0'
            AND
                ced.isDeleted='0' 
    	"; 
        
    	//echo $query;
    	if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
                $ret_ary = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    $ret_ary[$row['idServiceProvider']] = $row;  
                }
                return $ret_ary;
            }
            else
            {
            	return array();
            }
        }
        else
        {
            return array();
        }  
    }
    
    function getAvailableCourierProductByForwarder($idForwarder)
    {
        if($idForwarder>0)
        {
            /*
            * If booking was placed using Forwarder : $idForwarder and if this forwarder offers courier services then all the TNT products of the forwarder is eligible to create automatic label
            * If Forwarder : $idForwarder not offers any courier service then we have listed out all the available courier products at Transporteca and in that case one can not be allowed to create automatic label for the booking.
            */
            $courierProductsAry = array();
            $courierProductsAry = $this->getAllAvailableCourierProducts($idForwarder);
            $this->iProductsAvailableForCreateLabel = false;
            if(!empty($courierProductsAry))
            {
                /*
                * This means Forwarder : $idForwarder, is offering courier services so all the TNT product is allowed to create label in this case
                */
                $this->iProductsAvailableForCreateLabel = 1; 
                return $courierProductsAry;
            }
            else
            { 
                /*
                * This means Forwarder : $idForwarder, is offering not courier services so we do not allowed to create automatic courier label for these products
                */
                $courierProductsAry = array();
                $courierProductsAry = $this->getAllAvailableCourierProducts();
                return $courierProductsAry;
            }
        }
    }
    
    function getAllAvailableCourierProducts($idForwarder=false)
    {
        $kCourierServices = new cCourierServices();
        if($idForwarder>0)
        {
            $fromCountryProviderArr = array(); 
            $fromCountryProviderArr=$kCourierServices->selectCourierAgreementByCountry(false,false,$idForwarder);
            $fromCount=count($fromCountryProviderArr);

            $ToCountryProviderArr=$kCourierServices->selectCourierAgreementByCountry(false,false,$idForwarder);
            $toCount=count($ToCountryProviderArr);

            $providerForwarderArr=array_merge($fromCountryProviderArr,$ToCountryProviderArr); 
        }
        else
        {
            $fromCountryProviderArr = array(); 
            $fromCountryProviderArr=$kCourierServices->selectCourierAgreementByCountry();
            $fromCount=count($fromCountryProviderArr);

            $ToCountryProviderArr=$kCourierServices->selectCourierAgreementByCountry();
            $toCount=count($ToCountryProviderArr);

            $providerForwarderArr=array_merge($fromCountryProviderArr,$ToCountryProviderArr); 
        }  
        $courierProductArr=array();
        if(!empty($providerForwarderArr))
        {
            $ctr=0;
            $courierProviderComanyAry = array();
            $courierProviderComanyAry = $kCourierServices->getCourierProviderDetails(); 
            $alreadyAddedProductsAry = array();
            foreach($providerForwarderArr as $providerForwarderArrs)
            {  
                $resArr = array();
                $resArr = $kCourierServices->getCourierServices($providerForwarderArrs,false,false);  
                $szProviderName = $courierProviderComanyAry[$providerForwarderArrs['idCourierProvider']]['szName'];

                if(count($resArr)>0)
                { 
                    foreach($resArr as $key=>$values)
                    {  
                        if(!empty($values))
                        {
                            foreach($values as $value)
                            {
                                if(!empty($alreadyAddedProductsAry) && in_array($value['idCourierProviderProductid'],$alreadyAddedProductsAry))
                                {
                                    continue;
                                }
                                else
                                {
                                    $alreadyAddedProductsAry[] = $value['idCourierProviderProductid'];
                                    
                                    $szName=$szProviderName." - ".$value['szCourierProductName']; 
                                    $courierProductArr[$ctr]['idCourierProviderProductid'] = $value['idCourierProviderProductid'];
                                    $courierProductArr[$ctr]['idCourierAgreement']=$value['idCourierAgreement'];
                                    $courierProductArr[$ctr]['idCourierProvider'] = $providerForwarderArrs['idCourierProvider'];
                                    $courierProductArr[$ctr]['szName']=$szName;
                                } 
                                ++$ctr;
                            }
                        }
                    }
                } 
            }
        }
        return $courierProductArr;
    }
    
    function downloadBarcodeImage($szTrackingCode)
    {
        if(!empty($szTrackingCode))
        {
            $szBarcodeUrl = "https://my.tnt.com//barcode/barbecue?data=$szTrackingCode&type=Code128&height=70"; 
            $kWHSSearch = new cWHSSearch();
            $szBarcodeImageRawData = $kWHSSearch->curl_get_file_contents($szBarcodeUrl);
            if(!empty($szBarcodeImageRawData))
            {
                $szFileName = "barcode_".$szTrackingCode.".jpg";
                $szFilePath = __APP_PATH_TNT_BARCODE_DOWNLOAD_IMAGES__.'/'.$szFileName; 
                if(file_exists($szFilePath))
                {
                    @unlink($szFilePath);
                }
                file_put_contents($szFilePath,$szBarcodeImageRawData);
                if(file_exists($szFilePath))
                { 
                    //Write permission 128, 16, 2 are for writable for owner, group and other respectively.
                    chmod($szFilePath, fileperms($szFilePath) | 128 + 16 + 2);				
                }
            }
            $this->szBarcodeImageName = $szFileName;
        } 
    }
    
    function processShippingApiResponse($szApiResponseXml,$idBooking)
    {
        if(!empty($szApiResponseXml))
        { 
            /*
            * Updating data content to a xml file.
            */
            $szXmlFilePath = $this->createXmlFile($szApiResponseXml,$idBooking,false,'API_RESULT'); 
            
            $kWebServices = new cWebServices();
            if($kWebServices->_isValidXML($szApiResponseXml))
            { 
                echo "Valid XML,...";
                $labelResponseAry = array();  
                $labelResponseAry = cXmlParser::createArray($szApiResponseXml); 
                
                if(!empty($labelResponseAry['document']))
                {
                    $labelResponseAry = $labelResponseAry['document'];
                }  
                else if(!empty($labelResponseAry['parse_error']))
                {
                    $labelResponseAry = $labelResponseAry;
                }
                if(!empty($labelResponseAry))
                {
                    if(!empty($labelResponseAry['BOOK']['CONSIGNMENT']))
                    {
                        /*
                        * Received successful response from Shipping API.
                        */
                        $bookedConsignmentAry = array();
                        $bookedConsignmentAry = $labelResponseAry['BOOK']['CONSIGNMENT'];
                        
                        $szRawTrackingNumber = $bookedConsignmentAry['CONNUMBER'];
                        $szTNTBookingReference = $bookedConsignmentAry['BOOKINGREF'];
                        
                        $this->szRawTrackingNumber = $szRawTrackingNumber;
                        $this->szTNTBookingReference = $szTNTBookingReference;
                        
                        if(!empty($szRawTrackingNumber))
                        {
                            /*
                            * Removing first 2 and last character from Raw Tracking. TNT tracking numer will always be 9 digit number.
                            */
                            $this->getFormatedTrackingNumber($szRawTrackingNumber);  

                            $szTrackingNumber = $this->szTrackingNumber;
                            
                            $this->szLabelLogStr .= "<p>Successfully received Response from Shipping API.</p><br>"; 
                            $this->szLabelLogStr .= "<p>Shipment successfully created with Tracking Number: ".$szTrackingNumber."</p><br>";  

                            $this->szLabelLogStr .= "<p>Downloading Barcode Image for Shipping API with CONNUMBER ".$szRawTrackingNumber."</p><br>";  

                            $this->downloadBarcodeImage($szRawTrackingNumber);
                        } 
                        else
                        {
                            $this->szLabelLogStr .= "<p>Could not able to retrive Tracking code from Response.</p><br>"; 
                        }
                    }
                    else if(!empty($labelResponseAry['ERROR']))
                    {
                        /*
                        * Received Error response from Shipping API.
                        */
                        $this->iShippingApiError = 1;
                        $szShippingApiErrorMessage = "";
                        $apiErrorAry = array();
                        $apiErrorAry = $labelResponseAry['ERROR']; 

                        if(!empty($apiErrorAry[0]))
                        { 
                            foreach($apiErrorAry as $apiErrorArys)
                            {
                                $szShippingApiErrorMessage .= "<li>".$apiErrorArys['DESCRIPTION']."</li>";
                            }
                        }
                        else if(!empty($apiErrorAry))
                        {
                            $szShippingApiErrorMessage .= "<li>".$apiErrorAry['DESCRIPTION']."</li>";
                        }
                        $this->szShippingApiErrorMessage = $szShippingApiErrorMessage;
                    }
                    else if(!empty($labelResponseAry['parse_error']))
                    {
                        /*
                        * Received Error response from Shipping API.
                        */
                        $this->iShippingApiError = 1;
                        $szShippingApiErrorMessage = "";
                        $apiErrorAry = array();
                        $apiErrorAry = $labelResponseAry['parse_error']; 
 
                        if(!empty($apiErrorAry[0]))
                        { 
                            foreach($apiErrorAry as $apiErrorArys)
                            {
                                $szShippingApiErrorMessage .= "<li>".$apiErrorArys['error_reason']."</li>";
                            }
                        } 
                        else if(!empty($apiErrorAry))
                        {
                            $szShippingApiErrorMessage .= "<li>Location:".htmlentities($apiErrorAry['error_srcText']['@cdata'])."<br>Description: ".$apiErrorAry['error_reason']."</li>";
                            print_R($apiErrorAry['error_srcText']);
                        }
                        $this->szShippingApiErrorMessage = $szShippingApiErrorMessage;
                    } 
                    else
                    {
                        $this->iShippingApiError = 1;
                        $this->szShippingApiErrorMessage = "<li>Invalid response from Shipping API. Please try again later</li>";
                    }
                }  
                else
                {
                    $this->iShippingApiError = 1;
                    $this->szShippingApiErrorMessage = "<li>Invalid response from Shipping API. Please try again later</li>";
                }
            }
            else
            {
                $this->iShippingApiError = 1;
                $this->szShippingApiErrorMessage = "<li>Invalid response from Shipping API. Please try again later</li>";
            }
        }
    }
    
    function logTNTApiErrors($iDebug=false,$szDebugMessage=false)
    {
        if($iDebug==1)
        {
            $outputFileName = __APP_PATH__."/logs/tnt_label_respo_".date('Ymd').".log"; 
            $szLabelLogStr = $this->szLabelLogStr;

            $breaksAry = array("<br />","<br>","<br/>");                              
            $szLabelLogStr = str_replace($breaksAry, "\r\n", $szLabelLogStr);  

            $szDebugMessage = $szLabelLogStr.PHP_EOL."".PHP_EOL.$szDebugMessage;
            $this->logDebugger($outputFileName,$szDebugMessage);
        }
    } 
    
    
    function checkCityNameWithTNT($szCountryCode,$szCity,$szPostCode)
    {
        $query="
            SELECT
                szTownName
            FROM    
                ".__DBC_SCHEMATA_TNT_POSTCODE__."
            WHERE
                LOWER(szCountryCode)='".  mysql_escape_custom(strtolower($szCountryCode))."'
            AND
                szPostcodeFrom='".  mysql_escape_custom($szPostCode)."'
            ";
            //echo $query;
            
            if( ( $result = $this->exeSQL( $query ) ) )
            { 
                if( $this->getRowCnt() > 0 )
                {
                    $ret_ary = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {
                        $resArr[$ctr]=$row['szTownName'];
                        $resCheckArr[$ctr]=strtolower($row['szTownName']);
                        ++$ctr;
                    } 
                    if(!empty($resCheckArr))
                    {
                        if(in_array(strtolower($szCity), $resCheckArr))
                        {
                            return array();
                        }
                        else
                        {
                            return $resArr;
                        }
                    }
                }
            }
    }
}

?>