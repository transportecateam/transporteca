<?php
/**
 * This file is the container for all user related functionality.
 * All functionality related to user details should be contained in this class.
 *
 * WHSSearch.class.php
 *
 * @copyright Copyright (C) 2012 Transport-eca 
 * @author Ajay
 */
 if(!isset($_SESSION))
 {
    session_start();
 }
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once(__APP_PATH_CLASSES__."/easyPost.class.php");
require_once(__APP_PATH_CLASSES__."/vatApplication.class.php");
require_once(__APP_PATH_CLASSES__."/webServices.class.php");
                        
if(!class_exists('cDatabase'))
{
    require_once(__APP_PATH_CLASSES__."/error.class.php");
    require_once(__APP_PATH_CLASSES__."/database.class.php");
    require_once(__APP_PATH_CLASSES__."/haulagePricing.class.php");
    require_once(__APP_PATH_CLASSES__."/config.class.php");
    require_once(__APP_PATH_CLASSES__."/user.class.php");
    require_once(__APP_PATH_CLASSES__."/booking.class.php");
    require_once(__APP_PATH_CLASSES__."/forwarder.class.php");
} 

class cWHSSearch extends cDatabase
{
    var $forwarderAry=array();
    var $t_base = "SelectService/";
    var $t_base_homepage = "home/homepage/";
    var $t_base_cfs = "CFSLoaction/";
    var $t_base_error="management/Error/";
    var $CountryName;
    var $countryISO;
    var $tempDataId;
    var $idBatch;
    private $countWarehouse;
    private $szSelectionAvailables;
    private $iSelections;
    public $szLCLCalculationLogs;
    public static $cForwarderTryitoutCargoDetails = array();
    public static $szServiceCalculationMode;
    public static $szTestCalculationLogs;
    function __construct()
    {
        parent::__construct();
        return true;
    }
	
        /**
        * This function is used to search through database and courier provider( such as Fedex, UPS ...) API's and populate service list
        * 
        * @NOTE: To see back-up of this function please check nps.class.pp and search for get_search_result(). reason to putting backup there is nps class is less in use and small in size.
         * 
        * @access public
        * @param array,int,int
        * @return array 
        * @author Ajay 
        */
	function get_search_result($data,$cargoAry,$mode=false,$idBooking=false,$bSearchOnlyLCL=false)
	{
            if(!empty($data))
            {
                $kConfig  = new cConfig();
                $iDebugFlag = 0;
                $filename = __APP_PATH_ROOT__."/logs/debug_search_".date('Ymd').".log";
                
                if((int)$_SESSION['user_id']>0)
                {
                    $kQuote = new cQuote();
                    $fCustomerExchangeRate=$kQuote->updateCustomerCurrencyExchangeRate($idBooking,$data['idCurrency']);
                    if((int)$data['idCurrency']>0 && (float)$fCustomerExchangeRate>0.00)
                    {
                        $data['fExchangeRate']=$fCustomerExchangeRate;
                        //echo $data['fExchangeRate']."fExchangeRate";
                    }
                }
                        
                if(empty($data['szOriginPostCode']))
                {
                    $postcodeCheckAry=array();
                    $postcodeResultAry = array();

                    $postcodeCheckAry['idCountry'] = $data['idOriginCountry'] ;
                    $postcodeCheckAry['szLatitute'] = $data['fOriginLatitude'] ;
                    $postcodeCheckAry['szLongitute'] = $data['fOriginLongitude'] ;

                    $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry);

                    if(!empty($postcodeResultAry))
                    {
                        $data['szOriginPostCode'] = $postcodeResultAry['szPostCode']; 
                    }
                }
                if(empty($data['szDestinationPostCode']))
                {
                    $postcodeCheckAry=array();
                    $postcodeResultAry = array();

                    $postcodeCheckAry['idCountry'] = $data['idDestinationCountry'] ;
                    $postcodeCheckAry['szLatitute'] = $data['fDestinationLatitude'] ;
                    $postcodeCheckAry['szLongitute'] = $data['fDestinationLongitude'] ;

                    $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry);

                    if(!empty($postcodeResultAry))
                    {
                        $data['szDestinationPostCode'] = $postcodeResultAry['szPostCode']; 
                    }
                }
                
                if(!empty($data['dtTimingDate']))
                {
                    /*
                    * If user has selected any date less then today then we consider date as today for calculation
                    */
                    $date_str = $data['dtTimingDate'] ;  
                    $curr_date_time = time();
                    $date_str_time = strtotime($date_str); 
                    if($curr_date_time > $date_str_time)
                    {
                        $date_str = date('Y-m-d H:i:s');
                        $data['dtTimingDate'] = $date_str;
                    } 
                }   
                
                $szServiceTerm = trim($data['szServiceTerm']);
                if(!empty($szServiceTerm))
                {
                    $kConfig = new cConfig();
                    $serviceTermsAry = array();
                    $serviceTermsAry = $kConfig->getAllServiceTerms(false,$szServiceTerm);

                    if(!empty($serviceTermsAry))
                    {
                        $idServiceTerms = $serviceTermsAry[0]['id'];
                    }
                } 
                $this->szLCLCalculationLogs .= "<br> <h4> LCL Calculation - ".$mode." </h4>";
                $this->szLCLCalculationLogs .= "<br><u>Origin:</u>";
                $this->szLCLCalculationLogs .= "<br>Postcode: ".$data['szOriginPostCode'];
                $this->szLCLCalculationLogs .= "<br>City: ".$data['szOriginCity'];
                $this->szLCLCalculationLogs .= "<br>Country: ".$data['idOriginCountry'];
                
                $this->szLCLCalculationLogs .= "<br><br> <u>Destination:</u> ";
                $this->szLCLCalculationLogs .= "<br>Postcode: ".$data['szDestinationPostCode'];
                $this->szLCLCalculationLogs .= "<br>City: ".$data['szDestinationCity'];
                $this->szLCLCalculationLogs .= "<br>Country: ".$data['idDestinationCountry'];
                
                $this->szLCLCalculationLogs .= "<br><br> Total Volume: ".$data['fCargoVolume']." cbm";
                $this->szLCLCalculationLogs .= "<br><br> Total Weight: ".$data['fCargoWeight']." kg ";
                
                $this->szLCLCalculationLogs .= "<br><br>Service Type: ".getServiceName($data['idServiceType'])." <br>";
                if($data['iOriginCC']==1)
                {
                    $this->szLCLCalculationLogs .= "<br>Origin custom clearance applied: Yes ";
                }
                else
                {
                    $this->szLCLCalculationLogs .= "<br>Origin custom clearance applied: No ";
                }

                if($data['iDestinationCC']==2)
                {
                    $this->szLCLCalculationLogs .= "<br>Destination custom clearance applied: Yes ";
                }
                else
                {
                    $this->szLCLCalculationLogs .= "<br>Destination custom clearance applied: No ";
                } 
                
                $bDTDTradesFlag = false;
                $kCourierServices = new cCourierServices(); 
                if($kCourierServices->checkFromCountryToCountryExists($data['idOriginCountry'],$data['idDestinationCountry'],true))
                { 
                    $bDTDTradesFlag = true; 
                } 
                $this->szLCLCalculationLogs .= "<br><br><u></u>";
                if($bDTDTradesFlag)
                {
                    $this->szLCLCalculationLogs .= "<br>Door to door Trade: Yes ";
                    /*
                    *   If We search for LTL services then following scenarios may applied
                    *   CASE: 1. Search date = current date
                        Search LTL services with collection on search date + 1 day if time of shipper is am, and +2 days if time of shippers is pm
                        E.g. search 3. august and todays date is 3 august, if search is made AM, we display services with collection 4. august and later, if search is PM, then we display services with collection 5. august or later. If collection date is weekend, then following monday

                        CASE: 2. Search date = current date + 1 day
                        Search LTL services with collection on search date if time of shipper is am, and +1 day if time of shippers is pm
                        E.g. search 4. august and todays date is 3 august, if search is made AM, we display services with collection 4. august and later, if search is PM, then we display services with collection 5. august or later. If collection date is weekend, then following monday 
                     */

                    $dtNewTimingDate = $data['dtTimingDate']; 

                    $dtTomorrowDate = date("Y-m-d",strtotime("+1 DAY"));
                    $dtSearchedDate = date('Y-m-d',strtotime($data['dtTimingDate']));

                    $this->szLCLCalculationLogs .= "<br>Searched date: ".$dtSearchedDate;
                        
                    $dtTomorrowDateTime = strtotime($dtTomorrowDate);
                    $dtSearchedDateTime = strtotime($dtSearchedDate);
                    if($dtSearchedDateTime>$dtTomorrowDateTime)
                    {
                        if(__isWeekend($dtNewTimingDate))
                        {
                            $this->szLCLCalculationLogs .= "<br> Searched day is weekend so we are picking next monday as colletion date.";
                            $dtNewTimingDate = __nextMonday($dtNewTimingDate);
                        } 
                        else
                        {
                            //Do nothing if the searched date is greater then tomorrow date
                            $this->szLCLCalculationLogs .= "<br>Search date > current date + 1 day so no calculation will take place and just use the date what user has enetered.";
                        } 
                        $date_str = date('Y-m-d H:i:s',strtotime($dtNewTimingDate));
                    }
                    else
                    {
                        $szServerTimeZone = date_default_timezone_get(); 
                        $kConfigNew = new cConfig();
                        $kConfigNew->loadCountry($data['idOriginCountry']);
                        $szCountryCode=$kConfigNew->szCountryISO;
                        $szTimeZone = getTimeZoneByCountryCode($szCountryCode);  
                        //$szCustomerTimeZone = $szTimeZone;
                        $this->szLCLCalculationLogs .= "<br>Shipper country: ".$kConfigNew->szCountryName; 
                        
                        if(!empty($szTimeZone))
                        { 
                            $date = new DateTime($dtReminderDate,new DateTimeZone($szTimeZone));   
                            $dtShipperDateTime = $date->format('Y-m-d H:i:s');    
                            $dtShipperDate = date('Y-m-d',strtotime($dtShipperDateTime));                            
                            $dtShipperTime = date('H',strtotime($dtShipperDateTime));

                            $dtTomorrowDate = date("Y-m-d",strtotime("+1 DAY"));
                            $dtTomorrowDateTime = strtotime($dtTomorrowDate);
                            $dtTodaysDateTime = strtotime($dtShipperDate);  

                            $this->szLCLCalculationLogs .= "<br>Time zone: ".$szTimeZone;
                            $this->szLCLCalculationLogs .= "<br>Current time in ".$kConfigNew->szCountryName.": ".$dtShipperDateTime;

                            if($dtSearchedDateTime==$dtTodaysDateTime)
                            { 
                                $this->szLCLCalculationLogs .= "<br> Case applied: (Search date = current date) ";
                                if($dtShipperTime>=12)
                                {
                                    $this->szLCLCalculationLogs .= "<br> Shipper time >= 12 so adding 2 days in searched date.";
                                    $dtNewTimingDate = date('Y-m-d',strtotime("+2 DAY",strtotime($dtShipperDate)));
                                    if(__isWeekend($dtNewTimingDate))
                                    {
                                        $dtNewTimingDate = __nextMonday($dtNewTimingDate);
                                    }
                                }
                                else
                                {
                                    $this->szLCLCalculationLogs .= "<br> Shipper time < 12 so adding 1 days in searched date.";
                                    $dtNewTimingDate = date('Y-m-d',strtotime("+1 DAY",strtotime($dtShipperDate)));
                                    if(__isWeekend($dtNewTimingDate))
                                    {
                                        $dtNewTimingDate = __nextMonday($dtNewTimingDate);
                                    }
                                } 
                            }
                            else if($dtSearchedDateTime==$dtTomorrowDateTime)
                            {  
                                $this->szLCLCalculationLogs .= "<br> Case applied: (Search date = current date + 1 day) ";

                                if($dtShipperTime>=12)
                                {
                                    $this->szLCLCalculationLogs .= "<br> Shipper time >= 12 so adding 1 days in searched date.";
                                    $dtNewTimingDate = date('Y-m-d',strtotime("+2 DAY",strtotime($dtShipperDate)));
                                    if(__isWeekend($dtNewTimingDate))
                                    {
                                        $dtNewTimingDate = __nextMonday($dtNewTimingDate);
                                    } 
                                }  
                            }
                            $date_str = date('Y-m-d H:i:s',strtotime($dtNewTimingDate));
                            $date->setTimezone(new DateTimeZone($szServerTimeZone));  
                        }
                    } 
                }
                else
                {
                    $this->szLCLCalculationLogs .= "<br>Door to door Trade: No "; 
                }
                $this->szLCLCalculationLogs .= "<br><strong> Final date applied on the booking: ".date('Y-m-d',strtotime($date_str)).'</strong>'; 
                        
                $szBookingRandomNum = $data['szBookingRandomNum']; 
                if(($mode=='RECALCULATE_PRICING' || $mode=='FORWARDER_TRY_IT_OUT' || $mode=='MANAGEMENT_TRY_IT_OUT') && ($data['iPartnerApiPriceRecalculate']!=1))
                {
                    $kConfig = new cConfig(); 
                    if(empty($data['fOriginLatitude']) || empty($data['fOriginLongitude']))
                    {
                        $originPotcodeAry = array();
                        $originPotcodeAry = $kConfig->getPostCodeDetails_requirement($data['idOriginCountry'],$data['szOriginCity'],$data['szOriginPostCode']);
                        if(!empty($originPotcodeAry))
                        {
                            $data['fOriginLatitude'] = $originPotcodeAry['szLatitude'];
                            $data['fOriginLongitude'] = $originPotcodeAry['szLongitude'];
                        }
                    } 
                    if(empty($data['fDestinationLatitude']) || empty($data['fDestinationLongitude']))
                    {
                        $destinationPotcodeAry= array();
                        $destinationPotcodeAry = $kConfig->getPostCodeDetails_requirement($data['idDestinationCountry'],$data['szDestinationCity'],$data['szDestinationPostCode']);
                        if(!empty($destinationPotcodeAry))
                        {
                            $data['fDestinationLatitude'] = $destinationPotcodeAry['szLatitude'];
                            $data['fDestinationLongitude'] = $destinationPotcodeAry['szLongitude'];
                        }
                    } 
                } 
                
                $kUser = new cUser();
                $kForwarder = new cForwarder();
                
                if($_SESSION['user_id']>0)
                { 
                    $kUser->getUserDetails($_SESSION['user_id']); 
                    $iPrivateShipping = $kUser->iPrivate; 
                    $data['iPrivateShipping'] = $iPrivateShipping;
                    $idCustomerAccountCountry = $kUser->szCountry;
                } 
                else
                {
                    $iPrivateShipping = $data['iPrivateShipping'];
                    $idCustomerAccountCountry = $data['idBillingCountry'];
                }
                if($iPrivateShipping==1)
                {
                    $this->szLCLCalculationLogs .= "<br><strong>Customer Type: Private</strong>";
                } 
                else
                {
                    $this->szLCLCalculationLogs .= "<br><strong>Customer Type: Business</strong>";
                }
                
                $wareHouseFromAry = array();
                $wareHouseToAry = array(); 
                
                cWHSSearch::$szTestCalculationLogs = '';
                $kService = new cServices();
                $importExportWarehousesAry = array();
                $importExportWarehousesAry = $kService->getImportExportWarehousesForSearch($data,$mode);
                
                if(!empty($importExportWarehousesAry))
                {
                    $wareHouseFromAry = $importExportWarehousesAry['FROM'];
                    $wareHouseToAry = $importExportWarehousesAry['TO'];
                }   
                $this->szLCLCalculationLogs .= $kService->szExportImportWhsCalculationLogs; 
                
                /*
                * Fetching eligible warehouses for Rail Transport
                */
                if($bDTDTradesFlag || $data['idServiceType']==__SERVICE_TYPE_DTD__)
                {
                    /*
                    *  If user has selected service type: DTD or searched is defined on /dtdTrades/ page then we nothing
                    */ 
                }
                else
                {
                    /*
                    * Fetching warehouse for Rail transport
                    */
                    $kService->szExportImportWhsCalculationLogs = "";
                    $railImportExportWarehousesAry = array();
                    $railImportExportWarehousesAry = $kService->getImportExportWarehousesForSearch($data,$mode,'RAIL');
                        
                    if(!empty($railImportExportWarehousesAry))
                    {
                        if(!empty($railImportExportWarehousesAry['FROM']) && !empty($wareHouseFromAry))
                        {
                            $wareHouseFromAry = $wareHouseFromAry + $railImportExportWarehousesAry['FROM'];
                        } 
                        else if(!empty($railImportExportWarehousesAry['FROM']))
                        {
                            $wareHouseFromAry = $railImportExportWarehousesAry['FROM'];
                        }
                        
                        if(!empty($railImportExportWarehousesAry['TO']) && !empty($wareHouseToAry))
                        {
                            $wareHouseToAry = $wareHouseToAry + $railImportExportWarehousesAry['TO'];
                        }
                        else if(!empty($railImportExportWarehousesAry['TO']))
                        {
                            $wareHouseToAry = $railImportExportWarehousesAry['TO'];
                        }
                    }
                    $this->szLCLCalculationLogs .= $kService->szExportImportWhsCalculationLogs; 
                } 
                    
                $this->szTestCalculationLogs = cWHSSearch::$szTestCalculationLogs;
                
                if($mode=='RECALCULATE_PRICING')
                {  
                    $query_where = " AND wtw.id = '".(int)$data['idPricingWtw']."'";
                } 
                if($mode=='PARTNER_API_CALL')
                {
                    if(empty($wareHouseFromAry))
                    {
                        $this->buildErrorDetailAry('szOriginCfsNotFound','NOT_FOUND');
                    }
                    if(empty($wareHouseToAry))
                    {
                        $this->buildErrorDetailAry('szDestinationCfsNotFound','NOT_FOUND');
                    }
                } 
                /*
                $userIpDetailsAry = array();
                $userIpDetailsAry = getCountryCodeByIPAddress();  
                $szUserCountryName  = $userIpDetailsAry['szCountryCode']; 

                if(!empty($szUserCountryName))
                {
                    $kConfigCountry = new cConfig();
                    $kConfigCountry->loadCountry(false,$szUserCountryName);

                    $idCustomerCountry = $kConfigCountry->idCountry;
                }
                 * 
                 */ 
                $kForwarder_new = new cForwarder();  
                
                $idCustomerCountry = $data['idOriginCountry'];
                
                $usedCurrencyAry = array();
                if(!empty($wareHouseToAry) && !empty($wareHouseFromAry))
                {  
                    $resultAry=array();
                    $ctr=0; 
                    if($data['iOriginCC']==1 || $data['iDestinationCC']==2)
                    {
                        $quryCC_having = " HAVING idWTW>0 " ;
                        $queryCC = '';
                        if($data['iOriginCC']==1)  // OriginCustomClearance
                        {
                            $queryCC="
                                ,(
                                        SELECT
                                                count(cc.id)
                                        FROM
                                                ".__DBC_SCHEMATA_PRICING_CC__."	cc
                                        WHERE
                                                cc.idWarehouseFrom = wtw.idWarehouseFrom
                                        AND
                                                cc.idWarehouseTo = wtw.idWarehouseTo 
                                        AND
                                                cc.szOriginDestination = '1'
                                        AND
                                           cc.iActive = '1'
                                ) countOriginCC				
                            ";
                            $quryCC_having .= " AND countOriginCC > 0 " ;
                        }
                        if($data['iDestinationCC']==2)  // DestinationCustomClearance
                        {
                            $queryCC .="
                                ,(
                                    SELECT
                                        count(cc.id)
                                    FROM
                                        ".__DBC_SCHEMATA_PRICING_CC__."	cc
                                    WHERE
                                        cc.idWarehouseFrom = wtw.idWarehouseFrom
                                    AND
                                        cc.idWarehouseTo = wtw.idWarehouseTo 
                                    AND
                                        cc.szOriginDestination = '2'
                                   AND
                                        cc.iActive = '1'
                                ) countDestinationCC				
                            ";
                            $quryCC_having .= " AND countDestinationCC > 0 " ;
                        }
                    }
                    $counter_cargo = count($cargoAry);
                    
                    /*
                    * Calculating rate per W/M for LCL services
                    */
                    $totalVolume=0; 
                    $fTotalVolume = $data['fCargoVolume'];
                    $fTotalWeight = $data['fCargoWeight']/1000 ;  // converting weight from kg to meteric ton (mt)

                    // we are calculating price on MAX('total weight','total volume')
                    if($fTotalWeight > $fTotalVolume)
                    {
                        $fPriceFactorLclService = ceil($fTotalWeight) ;
                    }
                    else
                    {
                        $fPriceFactorLclService = ceil($fTotalVolume) ;
                    }               
                    
                    /*
                    * Calculating rate per W/M for Airfreight services
                    * Formula: we should take max (volume * 167 , weight ) and round up to nearest full number
                    */
                    $fTotalVolumeAirService = $data['fCargoVolume'] * 167 ;
                    
                    // we are calculating price on MAX('total weight','total volume')
                    if($data['fCargoWeight'] > $fTotalVolumeAirService)
                    {
                        $fPriceFactorAirService = ceil($data['fCargoWeight']) ;
                    }
                    else
                    {
                        $fPriceFactorAirService = ceil($fTotalVolumeAirService) ;
                    } 
                    
                    $weekDaysAry = array();
                    $weekDaysAry = $this->getAllWeekDays();
                        
                    $frequencyAry = array();
                    $frequencyAry = $this->getAllFrequencies(); 
                    
                    $arrDescriptions = array("'__CUTOFF_N_DAYS__'","'__PICK_UP_AFTER_M_DAYS__'","'__MARK_UP_PRICING_PERCENTAGE__'","'__TOTAL_NUM_SEARCH_RESULT__'","'__TOTAL_NUM_AIR_FREIGHT_SEARCH_RESULT__'"); 
                    $bulkManagemenrVarAry = array();
                    $bulkManagemenrVarAry = $this->getBulkManageMentVariableByDescription($arrDescriptions);       

                    $iGlobalCutOffNDays = $bulkManagemenrVarAry['__CUTOFF_N_DAYS__'];
                    $iGlobalAvailableNDays = $bulkManagemenrVarAry['__PICK_UP_AFTER_M_DAYS__'];
                    $fGlobalPriceMarkUp = $bulkManagemenrVarAry['__MARK_UP_PRICING_PERCENTAGE__'];  
                    $iGlobalSearchResultLimit = $bulkManagemenrVarAry['__TOTAL_NUM_SEARCH_RESULT__'];  
                    $iGlobalSearchAirResultLimit = $bulkManagemenrVarAry['__TOTAL_NUM_AIR_FREIGHT_SEARCH_RESULT__'];  
                    
                        
                    $fApplicableVatRate = 0;
                    $kVatApplication = new cVatApplication();
                        
                    $kService = new cServices(); 
                    $idServiceSelectedByUser = $data['idServiceType'];
                    $ctr_gl = 0;
                    $this->szLCLCalculationLogs .= "<br> <br><strong> Warehouse Service Look-up started </strong> ";
                    $this->szLCLCalculationLogs .=  "<br> Service selected by user: ".$idServiceSelectedByUser; 
                    
                    foreach($wareHouseFromAry as $idWHSFrom=>$iDistanceWHSFrom)
                    {
                        foreach($wareHouseToAry as $idWHSTo=>$iDistanceWHSTo)
                        {
                            if($idWHSTo!=$idWHSFrom)
                            {  
                                $ctr_gl++;
                                $query="
                                    SELECT
                                        wtw.id idWTW,
                                        wtw.idWarehouseFrom,
                                        wtw.idWarehouseTo,
                                        wtw.iBookingCutOffHours,
                                        wtw.idCutOffDay,
                                        wtw.szCutOffLocalTime,
                                        wtw.idAvailableDay,
                                        wtw.szAvailableLocalTime,
                                        wtw.iTransitHours,
                                        wtw.idFrequency,
                                        wtw.fOriginChargeRateWM as fOriginRateWM,
                                        wtw.fOriginChargeMinRateWM as fOriginMinRateWM,
                                        wtw.fOriginChargeBookingRate as fOriginRate,
                                        wtw.szOriginChargeCurrency as szOriginRateCurrency,					
                                        wtw.fDestinationChargeRateWM as fDestinationRateWM,
                                        wtw.fDestinationChargeMinRateWM as fDestinationMinRateWM,
                                        wtw.fDestinationChargeBookingRate as fDestinationRate,
                                        wtw.szDestinationChargeCurrency as szDestinationRateCurrency,					
                                        wtw.fFreightRateWM as fRateWM,
                                        wtw.fFreightMinRateWM as fMinRateWM,
                                        wtw.fFreightBookingRate as fRate,
                                        wtw.dtValidFrom,
                                        wtw.dtExpiry,
                                        wtw.szFreightCurrency,
                                        wtw.iOriginChargesApplicable,
                                        wtw.iDestinationChargesApplicable,	
                                        wtw.iTransitDays
                                        $queryCC
                                    FROM				
                                        ".__DBC_SCHEMATA_WAREHOUSES_PRICING__." wtw 
                                    WHERE
                                        wtw.idWarehouseTo ='".mysql_escape_custom($idWHSTo)."'	
                                    AND
                                        wtw.idWarehouseFrom ='".mysql_escape_custom($idWHSFrom)."'
                                    AND
                                        wtw.iActive = '1'	
                                    AND
                                        wtw.dtExpiry > now()
                                    ".$query_where."
                                    ".$quryCC_having."
                                "; 
//                                echo "<br> ".$query;
//                                die;
                                if($iDebugFlag==1)
                                { 
                                    $strdata=array();
                                    $strdata[0]=" \n\n ***Warehouse Query \n\n";
                                    $strdata[1] = " ".$query." \n\n";  
                                    file_log(true, $strdata, $filename);
                                } 
                                if($result=$this->exeSQL($query))
                                {
                                    if($this->iNumRows>0)
                                    {  
                                        $iWarehouseType = $iDistanceWHSFrom['iWarehouseType'];
                                        if($iDistanceWHSFrom['iWarehouseType']==__WAREHOUSE_TYPE_AIR__)
                                        {
                                            $this->szLCLCalculationLogs .= "<strong>Warehouse Type: Airport warehouse</strong>"; 
                                        }
                                        else
                                        {
                                            $this->szLCLCalculationLogs .= "<strong>Warehouse Type: LCL/Rail warehouse</strong>";
                                        }
                                        $this->szLCLCalculationLogs .= " <br><span style='color: green'>".$ctr_gl.". Found rates from CFS: ".$iDistanceWHSFrom['szWareHouseName']." to CFS: ".$iDistanceWHSTo['szWareHouseName']."</span>";  
                        
                                        $idLandingPage = $data['idLandingPage'];
                                        $iSearchMiniVersion = $data['iSearchMiniVersion'];
                                        $iCheckHandlingFeeForAutomaticQuote = (int)$data['iCheckHandlingFeeForAutomaticQuote'];

                                        $vogaHandlingFeeAry = array();
                                        if(($iSearchMiniVersion==7 || $iSearchMiniVersion==8 || $iSearchMiniVersion==9 || $iCheckHandlingFeeForAutomaticQuote==1) && $idLandingPage>0)
                                        {
                                            $kExplain = new cExplain();
                                            $iApiCall=false;
                                            if($iCheckHandlingFeeForAutomaticQuote==1)
                                            {
                                                $iApiCall=true;
                                            }
                                            $vogaHandlingFeeAry = $kExplain->getStandardQuotePriceByPageID($idLandingPage,false,false,true,$iApiCall);
                                        } 
                                        
                                        while($row=$this->getAssoc($result))
                                        { 
                                            /*
                                            * By default we uses idTransportMode as Sea while calculating LCL booking
                                            */
                                            $idTransportModeForCalculation = __BOOKING_TRANSPORT_MODE_SEA__; 
                                            if($bDTDTradesFlag)
                                            {
                                                /*
                                                * If trade is listed on /dtdTrades/ screen then we Sea while calculating LCL booking
                                                */
                                                $idTransportModeForCalculation = __BOOKING_TRANSPORT_MODE_ROAD__;
                                            }
                                            if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
                                            {
                                                $fPriceFactor = $fPriceFactorAirService;
                                                /*
                                                * If warehouse type is Airport warehouse then we will use mode as Air
                                                */
                                                $idTransportModeForCalculation = __BOOKING_TRANSPORT_MODE_AIR__;
                                            }
                                            else
                                            {
                                                $fPriceFactor = $fPriceFactorLclService;
                                            }  
                                            
                                            $iRailDTDAvailable = false;
                                            $iRailTransport = false;
                                            
                                            $railTransportAry = array();
                                            $railTransportAry['idForwarder'] = $iDistanceWHSTo['idForwarder'];
                                            $railTransportAry['idFromWarehouse'] = $idWHSFrom;
                                            $railTransportAry['idToWarehouse'] = $idWHSTo; 
                                            if($kService->isRailTransportExists($railTransportAry))
                                            {
                                                if($kService->iRailDTDAvailable==1)
                                                {
                                                    $iRailDTDAvailable = true;
                                                    $idTransportModeForCalculation = __BOOKING_TRANSPORT_MODE_RAIL__;
                                                }
                                                $iRailTransport = 1;
                                            }
                                            
                                            $referralFeeAry = array();
                                            $referralFeeAry['idForwarder'] = $iDistanceWHSTo['idForwarder'];
                                            $referralFeeAry['idTransportMode'] = $idTransportModeForCalculation; 
                                            $kReferralPricing = new cReferralPricing();
                                            $fReferralFee = $kReferralPricing->getForwarderReferralFee($referralFeeAry);  
                                            if(($iDistanceWHSFrom['iRailTransport']==1 || $iDistanceWHSTo['iRailTransport']==1) && $idServiceSelectedByUser!=__SERVICE_TYPE_DTD__)
                                            {
                                                /*
                                                * If From and To warehouse are added as Rail Transport on http://management.transporteca.com/railTransport/ and Service type selected by user is other then DTD
                                                * Then we calculate services for the trade as DTD
                                                */  
                                                if($iRailDTDAvailable)
                                                {
                                                    $this->szLCLCalculationLogs .= "<br><br>DTD Rail Transport: Yes "; 
                                                    $data['idServiceType'] = __SERVICE_TYPE_DTD__; 
                                                }
                                                else
                                                {
                                                    $data['idServiceType'] = $idServiceSelectedByUser;
                                                } 
;                                           }
                                            else
                                            {
                                                $data['idServiceType'] = $idServiceSelectedByUser;
                                                $this->szLCLCalculationLogs .= "<br><br>Rail Transport: No "; 
                                            }
                                            $szForwarderPriceLogs = '';
                                            $szCustomerPriceLogs = '';
                                            $row['szWeekDay'] = $weekDaysAry[$row['idCutOffDay']];
                                            $row['szAvailableDay'] = $weekDaysAry[$row['idAvailableDay']];
                                            $row['iFrequency'] = $frequencyAry[$row['idFrequency']];  
                                            
                                            $fCustomAmount = 0;	
                                            /**
                                            * Converting transit days into hours
                                            **/
                                            $fTransitHours = 0;
                                            $fTransitHours = $row['iTransitHours'];
                                            $fTransiDays_hour = ($row['iTransitDays']*24); //This is the actual Transit Days
                                            $fTruckingRateTo=0;
                                            $fTruckingRateFrom = 0;
                                            $fTotalStandardTruckingRate = 0;
                                            $fTotalHaulagePrice = 0;	
                                            $fTotalTransitHours = 0;
                                            $dtTransportTime_ms = 0;
                                            $dtTotalTransportDays = 0;
                                            $iBookingCutOffHours = 0;
                                            /**
                                             * calculating Origin port freight price
                                            */	
                                            $szOriginChargesStr = '';
                                            if($row['szOriginRateCurrency']==1)  // USD
                                            {
                                                $fOriginPrice = $row['fOriginRateWM'] * $fPriceFactor ;
                                                $fOriginBookingRate = $row['fOriginRate'];
                                                $fOriginMinRateWM = $row['fOriginMinRateWM'];
                                                $fOriginFreightExchangeRate = 1.00 ;
                                                $szOriginPortCurrency = 'USD';
                                                if($fOriginPrice < $fOriginMinRateWM)
                                                {
                                                    $fOriginPrice = $row['fOriginMinRateWM'];								
                                                }
                                                $fTotalOriginPortPrice = $fOriginPrice + $fOriginBookingRate ;	
                                                $fOriginPriceLocalCurrency = $fTotalOriginPortPrice;
                                            }
                                            else
                                            {
                                                /**
                                                * getting USD factor
                                                */  
                                                if($usedCurrencyAry[$row['szOriginRateCurrency']])
                                                {
                                                    $currencyAry = $usedCurrencyAry[$row['szOriginRateCurrency']] ;
                                                }
                                                else
                                                {
                                                    $currencyAry = array();
                                                    $currencyAry = $this->getCurrencyDetails($row['szOriginRateCurrency']);
                                                    $usedCurrencyAry[$row['szOriginRateCurrency']] = $currencyAry ;
                                                } 
                                                $fUSDValue = $currencyAry['fUsdValue'];
                                                $szOriginPortCurrency = $currencyAry['szCurrency'];
                                                /**
                                                * converting price to USD
                                                */	

                                                $fOriginPrice = $row['fOriginRateWM'] * $fPriceFactor ;
                                                $fOriginBookingRate = $row['fOriginRate'];
                                                $fOriginMinRateWM = $row['fOriginMinRateWM'];
                        
                                                if($fOriginPrice < $fOriginMinRateWM)
                                                {
                                                    $fOriginPrice = $row['fOriginMinRateWM'];								
                                                }		
                                                $fOriginPriceLocalCurrency = $fOriginPrice;

                                                $fOriginPrice = $fOriginPrice * $fUSDValue ;
                                                $fOriginBookingRate = $row['fOriginRate'] * $fUSDValue;
                                                $fOriginMinRateWM = $row['fOriginMinRateWM'] * $fUSDValue ;
                                                $fOriginFreightExchangeRate = $fUSDValue;

                                                $fTotalOriginPortPrice = $fOriginPrice + $fOriginBookingRate ;
                                                $fOriginPriceLocalCurrency = $fOriginPriceLocalCurrency + $row['fOriginRate'];
                                            } 
                                            $szOriginChargesStr = "<strong>A. Origin Port Price </strong>";
                                            //$szOriginChargesStr .= "<br> MAX((fOriginRateWM * fChargeableWeight);fOriginMinRateWM) + fOriginRate ";
                                            $szOriginChargesStr .= "<br> MAX((".$szOriginPortCurrency." ".$row['fOriginRateWM']." * ".$fPriceFactor.");".$szOriginPortCurrency." ".$row['fOriginMinRateWM'].") + ".$szOriginPortCurrency." ".$row['fOriginRate']." = ".$szOriginPortCurrency." ".$fOriginPriceLocalCurrency;
                                            $szOriginChargesStr .= "<br> ROE: ".$fOriginFreightExchangeRate;
                                            $szOriginChargesStr .= "<br> Origin Port Price (USD): ".$fTotalOriginPortPrice;
                                            
                                            $this->szLCLCalculationLogs .= "<br><br>Service Type: ".$data['idServiceType']; 
                                            
                                            /**
                                             * calculating Destination port freight price
                                            */
                                            if($row['szDestinationRateCurrency']==1)  // USD
                                            {
                                                $fDestinationPrice = $row['fDestinationRateWM'] * $fPriceFactor ;
                                                $fDestinationBookingRate = $row['fDestinationRate'];
                                                $fDestinationMinRateWM = $row['fDestinationMinRateWM'];
                                                $fDestinationFreightExchangeRate = 1 ; 	
                                                $szDestinationPortCurrency = 'USD';
                                                if($fDestinationPrice < $fDestinationMinRateWM)
                                                {
                                                    $fDestinationPrice = $row['fDestinationMinRateWM'];								
                                                }
                                                $fTotalDestinationPortPrice = $fDestinationPrice + $fDestinationBookingRate ;
                                                $fDestinationPriceLocalCurrency = $fTotalDestinationPortPrice;
                                            }
                                            else
                                            {
                                                /**
                                                * getting USD factor
                                                */  
                                                if($usedCurrencyAry[$row['szDestinationRateCurrency']])
                                                {
                                                    $currencyAry = $usedCurrencyAry[$row['szDestinationRateCurrency']] ;
                                                }
                                                else
                                                {
                                                    $currencyAry = array();
                                                    $currencyAry = $this->getCurrencyDetails($row['szDestinationRateCurrency']);
                                                    $usedCurrencyAry[$row['szDestinationRateCurrency']] = $currencyAry ;
                                                } 

                                                $fUSDValue = $currencyAry['fUsdValue'];
                                                $szDestinationPortCurrency = $currencyAry['szCurrency'];
                                                /**
                                                * converting price to USD
                                                */	

                                                $fDestinationPrice = $row['fDestinationRateWM'] * $fPriceFactor ;
                                                $fDestinationBookingRate = $row['fDestinationRate'];
                                                $fDestinationMinRateWM = $row['fDestinationMinRateWM'];

                                                if($fDestinationPrice < $fDestinationMinRateWM)
                                                {
                                                    $fDestinationPrice = $row['fDestinationMinRateWM'];								
                                                }		

                                                $fDestinationPriceLocalCurrency = $fDestinationPrice;
                                                $fDestinationPrice = $fDestinationPrice * $fUSDValue ;
                                                $fDestinationBookingRate = $row['fDestinationRate'] * $fUSDValue;
                                                $fDestinationMinRateWM = $row['fDestinationMinRateWM'] * $fUSDValue ;
                                                $fDestinationFreightExchangeRate = $fUSDValue ; 	

                                                $fTotalDestinationPortPrice = $fDestinationPrice + $fDestinationBookingRate ;	 
                                                $fDestinationPriceLocalCurrency += $row['fDestinationRate'];
                                            } 
                                            
                                            $szDestinationChargesStr = "<br><strong>C. Destination Port Price </strong>";
                                            //$szDestinationChargesStr .= "<br> MAX((fDestinationRateWM * fChargeableWeight);fDestinationMinRateWM) + fDestinationRate ";
                                            $szDestinationChargesStr .= "<br> MAX((".$szDestinationPortCurrency." ".$row['fDestinationRateWM']." * ".$fPriceFactor.");".$szDestinationPortCurrency." ".$row['fDestinationMinRateWM'].") + ".$szDestinationPortCurrency." ".$row['fDestinationRate']." = ".$szDestinationPortCurrency." ".$fDestinationPriceLocalCurrency;
                                            $szDestinationChargesStr .= "<br> ROE: ".$fDestinationFreightExchangeRate;
                                            $szDestinationChargesStr .= "<br> Destination Port Price (USD): ".$fTotalDestinationPortPrice; 
                                            
                                            $fTotalPortPrice = 0 ;
                                            if($data['idServiceType']==__SERVICE_TYPE_DTD__) //DTD
                                            {
                                                if($iDebugFlag==1)
                                                { 
                                                    $strdata=array();
                                                    $strdata[0]=" \n\n ***Warehouse combination \n\n";
                                                    $strdata[1] = " idWarehouseTo: ".$idWHSTo." idWarehouseFrom: ".$idWHSFrom." \n\n";  
                                                    file_log(true, $strdata, $filename);
                                                }
                                                if($row['iDestinationChargesApplicable']!=1 || $row['iOriginChargesApplicable']!=1)
                                                {
                                                    if($row['iDestinationChargesApplicable']==1)
                                                    {
                                                        $this->szLCLCalculationLogs .= "<br><br> <span style='color:red'> There is no W service available at Origin warehouse. Calcultion stopped </span>";
                                                    }
                                                    else
                                                    {
                                                        $this->szLCLCalculationLogs .= "<br><br> <span style='color:red'> There is no W service available at Destination warehouse. Calcultion stopped </span>";
                                                    }
                                                    continue;
                                                }
                                                /**
                                                * For DTD Haulage price is chargable for both Origin and Destination
                                                */  

                                                /**
                                                * calculating the Haulage price at Origin in USD
                                                **/

                                                $fTotalPortPrice = $fTotalOriginPortPrice + $fTotalDestinationPortPrice;									
                                                
                                                $houlageData=array();
                                                $fFromWareHouseHaulageAry = array();
                                                $houlageData['idWarehouse'] = $idWHSFrom ;
                                                $houlageData['iDistance'] = $iDistanceWHSFrom['iDistance'];
                                                $houlageData['iDirection'] = 1; // origin haulage		

                                                $houlageData['szLatitude'] = $data['fOriginLatitude'] ;
                                                $houlageData['szLongitude'] = $data['fOriginLongitude'] ;

                                                if(!empty($data['szOriginPostCode']))
                                                {
                                                    $szHaulagePostCode = $data['szOriginPostCode'] ;
                                                }
                                                else
                                                {
                                                    $szHaulagePostCode = $data['szOriginCity'] ;
                                                }

                                                $houlageData['fTotalWeight'] = $data['fCargoWeight'];
                                                $houlageData['fVolume'] = $data['fCargoVolume'];
                                                $houlageData['szPostcode'] = $szHaulagePostCode;
                                                $houlageData['idCountry'] = $data['idOriginCountry'];	
                                                
                                                $this->szLCLCalculationLogs .= "<br><br><u>Checking Haulage services origin:</u>";
                                                $this->szLCLCalculationLogs .= "<br> Weight: ".$houlageData['fTotalWeight'];
                                                $this->szLCLCalculationLogs .= "<br> Volume: ".$houlageData['fVolume'];
                                                $this->szLCLCalculationLogs .= "<br> Postcode: ".$houlageData['szPostcode'];
                        
                                                $kHaulagePricing = new cHaulagePricing();
                                                $fFromWareHouseHaulageAry = $kHaulagePricing->getHaulageDetails($houlageData);
                                                if(!empty($fFromWareHouseHaulageAry))
                                                {
                                                    $this->szLCLCalculationLogs .= "<br><b> Haulage Service Found </b>";
                                                    $this->szLCLCalculationLogs .= "<br>Haulage Price: ".$fFromWareHouseHaulageAry['fTotalHaulgePrice'];
                                                    $this->szLCLCalculationLogs .= "<br>Model: ".$fFromWareHouseHaulageAry['szModelName'];
                                                    $this->szLCLCalculationLogs .= "<br>Haulage Transit: ".$fFromWareHouseHaulageAry['iHaulageTransitTime'];
                                                }
                                                else
                                                {
                                                    $this->szLCLCalculationLogs .= "<br> <span style='color:red'> No Haulage service found at Origin.</span><br>";
                                                }

                                                if($iDebugFlag==1)
                                                { 
                                                    $strdata=array();
                                                    $strdata[0]=" \n\n ***Origin Haulage Input Ary \n\n";
                                                    $strdata[1] = print_R($houlageData,true)."\n\n"; 
                                                    $strdata[2]=" \n\n ***Origin Haulage Output Ary \n\n";
                                                    $strdata[3] = print_R($fFromWareHouseHaulageAry,true)."\n\n"; 
                                                    file_log(true, $strdata, $filename);
                                                }
                                                
                                                $houlageData=array();
                                                $fToWareHouseHaulageAry = array();
                                                $houlageData['idWarehouse'] = $idWHSTo ;
                                                $houlageData['iDistance'] = $iDistanceWHSTo['iDistance'];
                                                $houlageData['iDirection'] = 2; // origin haulage		

                                                $houlageData['szLatitude'] = $data['fDestinationLatitude'] ;
                                                $houlageData['szLongitude'] = $data['fDestinationLongitude'];

                                                if(!empty($data['szDestinationPostCode']))
                                                {
                                                    $szHaulagePostCode = $data['szDestinationPostCode'] ;
                                                }
                                                else
                                                {
                                                    $szHaulagePostCode = $data['szDestinationCity'] ;
                                                }

                                                $houlageData['fTotalWeight'] = $data['fCargoWeight'];
                                                $houlageData['fVolume'] = $data['fCargoVolume'];
                                                $houlageData['szPostcode'] = $szHaulagePostCode;
                                                $houlageData['idCountry'] = $data['idDestinationCountry'];
                                                
                                                $this->szLCLCalculationLogs .= "<br><br> Checking Haulage services at Destination";
                                                $this->szLCLCalculationLogs .= "<br> Weight: ".$houlageData['fTotalWeight'];
                                                $this->szLCLCalculationLogs .= "<br> Volume: ".$houlageData['fVolume'];
                                                $this->szLCLCalculationLogs .= "<br> Postcode: ".$houlageData['szPostcode'];

                                                $kHaulagePricing = new cHaulagePricing();
                                                $fToWareHouseHaulageAry = $kHaulagePricing->getHaulageDetails($houlageData);
                                                
                                                if(!empty($fToWareHouseHaulageAry))
                                                {
                                                    $this->szLCLCalculationLogs .= "<br><b> Haulage Service Found </b>";
                                                    $this->szLCLCalculationLogs .= "<br>Haulage Price: ".$fToWareHouseHaulageAry['fTotalHaulgePrice'];
                                                    $this->szLCLCalculationLogs .= "<br>Model: ".$fToWareHouseHaulageAry['szModelName'];
                                                    $this->szLCLCalculationLogs .= "<br>Haulage Transit: ".$fToWareHouseHaulageAry['iHaulageTransitTime'];
                                                    
                                                }
                                                else
                                                {
                                                    $this->szLCLCalculationLogs .= "<br><span style='color:red'> No Haulage service found at Destination.</span><br>";
                                                }

                                                if($iDebugFlag==1)
                                                { 
                                                    $strdata=array();
                                                    $strdata[0]=" \n\n ***Destination Haulage Input Ary \n\n";
                                                    $strdata[1] = print_R($houlageData,true)."\n\n"; 
                                                    $strdata[2]=" \n\n ***Destination Haulage Output Ary \n\n";
                                                    $strdata[3] = print_R($fFromWareHouseHaulageAry,true)."\n\n"; 
                                                    file_log(true, $strdata, $filename);
                                                } 
                        
                                                $fTotalHaulagePrice = $fFromWareHouseHaulageAry['fTotalHaulgePrice'] + $fToWareHouseHaulageAry['fTotalHaulgePrice'];

                                                /**
                                                * Adding haulage transit time from Origing and Destination with wtw transit time 
                                                **/ 
                                                if($data['idTimingType']==1) //Ready at origin  DTD
                                                {
                                                    $fTotalTransitHours = $fTransitHours + $fToWareHouseHaulageAry['iHaulageTransitTime'] + $fFromWareHouseHaulageAry['iHaulageTransitTime'];
                                                    $fTotalTransitDays_Hours = $fTransiDays_hour + $fToWareHouseHaulageAry['iHaulageTransitTime'] + $fFromWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                    $iBookingCutOffHours = (($row['iBookingCutOffHours'] + $fFromWareHouseHaulageAry['iHaulageTransitTime'])*60*60);											
                                                }
                                                else
                                                {
                                                    $fTotalTransitHours = $fTransitHours + $fToWareHouseHaulageAry['iHaulageTransitTime'] + $fFromWareHouseHaulageAry['iHaulageTransitTime'];
                                                    $fTotalTransitDays_Hours = $fTransiDays_hour + $fToWareHouseHaulageAry['iHaulageTransitTime'] + $fFromWareHouseHaulageAry['iHaulageTransitTime'];
                                                    $iBookingCutOffHours = (($row['iBookingCutOffHours'] + $fToWareHouseHaulageAry['iHaulageTransitTime'])*60*60);
                                                }
                                            }																	
                                            else if($data['idServiceType']==(int)__SERVICE_TYPE_DTW__) //DTW
                                            {
                                                if($row['iDestinationChargesApplicable']!=1 || $row['iOriginChargesApplicable']!=1)
                                                {
                                                    continue;
                                                } 
                                                /**
                                                * for DTW Standard Trucking rate is applicable at Destination and Haulage Price is applicable at Origin
                                                */ 
                                                $fTotalPortPrice = $fTotalOriginPortPrice + $fTotalDestinationPortPrice;

                                                /**
                                                * calculating the Haulage price at Origin in USD
                                                **/  
                                                $houlageData=array();
                                                $fFromWareHouseHaulageAry = array();
                                                $houlageData['idWarehouse'] = $idWHSFrom ;
                                                $houlageData['iDistance'] = $iDistanceWHSFrom['iDistance'];
                                                $houlageData['iDirection'] = 1; // origin haulage		

                                                $houlageData['szLatitude'] = $data['fOriginLatitude'] ;
                                                $houlageData['szLongitude'] = $data['fOriginLongitude'] ;

                                                if(!empty($data['szOriginPostCode']))
                                                {
                                                    $szHaulagePostCode = $data['szOriginPostCode'] ;
                                                }
                                                else
                                                {
                                                    $szHaulagePostCode = $data['szOriginCity'] ;
                                                }

                                                $houlageData['fTotalWeight'] = $data['fCargoWeight'];
                                                $houlageData['fVolume'] = $data['fCargoVolume'];
                                                $houlageData['szPostcode'] = $szHaulagePostCode;
                                                $houlageData['idCountry'] = $data['idOriginCountry'];		

                                                $kHaulagePricing = new cHaulagePricing();
                                                $fFromWareHouseHaulageAry = $kHaulagePricing->getHaulageDetails($houlageData);

                                                $fTotalHaulagePrice = $fFromWareHouseHaulageAry['fTotalHaulgePrice'] ;	
                                                $fTotalTransitHours = $fTransitHours + $fFromWareHouseHaulageAry['iHaulageTransitTime'];
                                                $fTotalTransitDays_Hours = $fTransiDays_hour + $fFromWareHouseHaulageAry['iHaulageTransitTime'];
                                                /**
                                                 * calculating standard trucking rate at Destination in USD
                                                */	
                                                $fTruckingRateTo = $iDistanceWHSTo['iDistance'] * $iDistanceWHSTo['fStandardTruckRate'];
                                                $fTotalStandardTruckingRate = $fTruckingRateTo ;

                                                $iBookingCutOffHours = (($row['iBookingCutOffHours']+$fFromWareHouseHaulageAry['iHaulageTransitTime'])*60*60);

                                                $iBookingCutOffHours_available = (($row['iBookingCutOffHours']+$fFromWareHouseHaulageAry['iHaulageTransitTime'])*60*60);


                                                /**
                                                * Adding haulage transit time from Origing and Destination with wtw transit time 
                                                **/ 
                                                if($data['idTimingType']==1) //Ready at origin  DTW
                                                {
                                                    $fTotalTransitHours = $fTransitHours + $fFromWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                    $fTotalTransitDays_Hours = $fTransiDays_hour + $fFromWareHouseHaulageAry['iHaulageTransitTime'] ;											
                                                    $iBookingCutOffHours = (($row['iBookingCutOffHours']+$fFromWareHouseHaulageAry['iHaulageTransitTime'])*60*60);										
                                                }
                                                else
                                                {
                                                    $fTotalTransitHours = $fTransitHours + $fFromWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                    $fTotalTransitDays_Hours = $fTransiDays_hour + $fFromWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                    $iBookingCutOffHours = (($row['iBookingCutOffHours']*60*60));
                                                }
                                            }								
                                            elseif($data['idServiceType']==(int)__SERVICE_TYPE_WTD__) //WTD
                                            {
                                                if($row['iDestinationChargesApplicable']!=1 || $row['iOriginChargesApplicable']!=1)
                                                {
                                                    continue;
                                                }

                                                /**
                                                * for WTD Standard Trucking rate is applicable at Origin and Haulage Price is applicable at Destination
                                                */  
                                                $fTotalPortPrice = $fTotalOriginPortPrice + $fTotalDestinationPortPrice;

                                                /**
                                                 * calculating standard trucking rate at Origin in USD
                                                */										

                                                $fTruckingRateFrom = $iDistanceWHSFrom['iDistance'] * $iDistanceWHSFrom['fStandardTruckRate'];
                                                $fTotalStandardTruckingRate = $fTruckingRateFrom ;	

                                                /**
                                                * calculating To Haulage price at Destination in USD
                                                */ 
                                                $houlageData=array();
                                                $fFromWareHouseHaulageAry = array();
                                                $houlageData['idWarehouse'] = $idWHSTo ;
                                                $houlageData['iDistance'] = $iDistanceWHSTo['iDistance'];
                                                $houlageData['iDirection'] = 2; // origin haulage		

                                                $houlageData['szLatitude'] = $data['fDestinationLatitude'] ;
                                                $houlageData['szLongitude'] = $data['fDestinationLongitude'] ;

                                                if(!empty($data['szDestinationPostCode']))
                                                {
                                                    $szHaulagePostCode = $data['szDestinationPostCode'] ;
                                                }
                                                else
                                                {
                                                    $szHaulagePostCode = $data['szDestinationCity'] ;
                                                }

                                                $houlageData['fTotalWeight'] = $data['fCargoWeight'];
                                                $houlageData['fVolume'] = $data['fCargoVolume'];
                                                $houlageData['szPostcode'] = $szHaulagePostCode;
                                                $houlageData['idCountry'] = $data['idDestinationCountry'];

                                                $kHaulagePricing = new cHaulagePricing();
                                                $fToWareHouseHaulageAry = $kHaulagePricing->getHaulageDetails($houlageData);

                                                //$fToWareHouseHaulageAry = $this->getHaulageDetails($houlageData);

                                                $fTotalHaulagePrice = $fToWareHouseHaulageAry['fTotalHaulgePrice'];

                                                //$fTotalTransitHours = $fTransitHours + $fToWareHouseHaulageAry['iHaulageTransitTime'];

                                                /**
                                                * Adding haulage transit time from Origing and Destination with wtw transit time 
                                                **/ 
                                                if($data['idTimingType']==1) //Ready at origin  WTD
                                                {
                                                    $fTotalTransitHours = $fTransitHours + $fToWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                    $fTotalTransitDays_Hours = $fTransiDays_hour + $fToWareHouseHaulageAry['iHaulageTransitTime'] ;											
                                                    $iBookingCutOffHours = ($row['iBookingCutOffHours']*60*60);											
                                                }
                                                else
                                                {
                                                    $fTotalTransitHours = $fTransitHours + $fToWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                    $fTotalTransitDays_Hours = $fTransiDays_hour + $fToWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                    $iBookingCutOffHours = (($row['iBookingCutOffHours'] + $fToWareHouseHaulageAry['iHaulageTransitTime'])*60*60);
                                                }
                                            }								
                                            else if($data['idServiceType']==(int)__SERVICE_TYPE_WTW__) // WTW
                                            {
                                                if($row['iDestinationChargesApplicable']!=1 || $row['iOriginChargesApplicable']!=1)
                                                {
                                                    continue;
                                                }
                                                /**
                                                * Haulage Price and Haulage transit time is not applicable for WTW 
                                                */ 
                                                $fTotalPortPrice = $fTotalOriginPortPrice + $fTotalDestinationPortPrice;

                                                $fTotalHaulagePrice = 0;

                                                /**
                                                 * calculating standard trucking rate
                                                */
                                                $fTruckingRateTo = $iDistanceWHSTo['iDistance'] * $iDistanceWHSTo['fStandardTruckRate'];

                                                $fTruckingRateFrom = $iDistanceWHSFrom['iDistance'] * $iDistanceWHSFrom['fStandardTruckRate'];

                                                $fTotalStandardTruckingRate = $fTruckingRateFrom + $fTruckingRateTo ;

                                                $fTotalTransitHours = $fTransitHours ;
                                                $fTotalTransitDays_Hours = $fTransiDays_hour ;
                                                $iBookingCutOffHours = (($row['iBookingCutOffHours'])*60*60);
                                            }
                                            else if($data['idServiceType']==(int)__SERVICE_TYPE_DTP__) //DTP
                                            {		 
                                                if($row['iOriginChargesApplicable']!=1)
                                                {
                                                    $this->szLCLCalculationLogs .= "<br> No Origin charges added".$row['idWTW'];
                                                    continue;
                                                }							
                                                /**
                                                * for DTP Haulage Price and origin port price is applicable at Origin 
                                                */ 

                                                $fTotalDestinationPortPrice = 0;
                                                $fDestinationFreightExchangeRate = 0;

                                                $fTotalPortPrice = $fTotalOriginPortPrice ;

                                                /**
                                                * calculating the Haulage price at Origin in USD
                                                **/ 

                                                $houlageData=array();
                                                $fFromWareHouseHaulageAry = array();
                                                $houlageData['idWarehouse'] = $idWHSFrom ;
                                                $houlageData['iDistance'] = $iDistanceWHSFrom['iDistance'];
                                                $houlageData['iDirection'] = 1; // origin haulage		

                                                $houlageData['szLatitude'] = $data['fOriginLatitude'] ;
                                                $houlageData['szLongitude'] = $data['fOriginLongitude'] ;

                                                if(!empty($data['szOriginPostCode']))
                                                {
                                                        $szHaulagePostCode = $data['szOriginPostCode'] ;
                                                }
                                                else
                                                {
                                                        $szHaulagePostCode = $data['szOriginCity'] ;
                                                }

                                                $houlageData['fTotalWeight'] = $data['fCargoWeight'];
                                                $houlageData['fVolume'] = $data['fCargoVolume'];
                                                $houlageData['szPostcode'] = $szHaulagePostCode;
                                                $houlageData['idCountry'] = $data['idOriginCountry'];		

                                                $kHaulagePricing = new cHaulagePricing();
                                                $fFromWareHouseHaulageAry = $kHaulagePricing->getHaulageDetails($houlageData);

                                                /**
                                                 * calculating standard trucking rate at Destination in USD
                                                */	
                                                $fTruckingRateTo = $iDistanceWHSTo['iDistance'] * $iDistanceWHSTo['fStandardTruckRate'];
                                                $fTotalStandardTruckingRate = $fTruckingRateTo ;

                                                $fTotalHaulagePrice = $fFromWareHouseHaulageAry['fTotalHaulgePrice'] ;	
                                                $fTotalTransitHours = $fTransitHours + $fFromWareHouseHaulageAry['iHaulageTransitTime'];
                                                $fTotalTransitDays_Hours = $fTransiDays_hour + $fFromWareHouseHaulageAry['iHaulageTransitTime'];

                                                $iBookingCutOffHours = (($row['iBookingCutOffHours']+$fFromWareHouseHaulageAry['iHaulageTransitTime'])*60*60);

                                                $iBookingCutOffHours_available = (($row['iBookingCutOffHours']+$fFromWareHouseHaulageAry['iHaulageTransitTime'])*60*60);


                                                /**
                                                * Adding haulage transit time from Origing and Destination with wtw transit time 
                                                **/ 
                                                if($data['idTimingType']==1) //Ready at origin  DTW
                                                {
                                                    $fTotalTransitHours = $fTransitHours + $fFromWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                    $fTotalTransitDays_Hours = $fTransiDays_hour + $fFromWareHouseHaulageAry['iHaulageTransitTime'] ;											
                                                    $iBookingCutOffHours = (($row['iBookingCutOffHours']+$fFromWareHouseHaulageAry['iHaulageTransitTime'])*60*60);										
                                                }
                                                else
                                                {
                                                    $fTotalTransitHours = $fTransitHours + $fFromWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                    $fTotalTransitDays_Hours = $fTransiDays_hour + $fFromWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                    $iBookingCutOffHours = (($row['iBookingCutOffHours']*60*60));
                                                }
                                            }
                                            else if($data['idServiceType']==(int)__SERVICE_TYPE_PTD__) //PTD
                                            {
                                                if($row['iDestinationChargesApplicable']!=1)
                                                {
                                                    continue;
                                                }
                                                /**
                                                * for WTD Standard Trucking rate is applicable at Origin and Haulage Price is applicable at Destination
                                                */  

                                                /**
                                                 * calculating standard trucking rate at Origin in USD
                                                 **/										

                                                $fTruckingRateFrom = $iDistanceWHSFrom['iDistance'] * $iDistanceWHSFrom['fStandardTruckRate'];
                                                $fTotalStandardTruckingRate = $fTruckingRateFrom ;

                                                $fTotalPortPrice = $fTotalDestinationPortPrice;
                                                $resultAry[$ctr]['fTotalDestinationPortPrice'] = $fTotalDestinationPortPrice;	
                                                $resultAry[$ctr]['fDestinationFreightExchangeRate'] = $fDestinationFreightExchangeRate;	

                                                /**
                                                * calculating To Haulage price at Destination in USD
                                                */

                                                $houlageData=array();
                                                $fFromWareHouseHaulageAry = array();
                                                $houlageData['idWarehouse'] = $idWHSTo ;
                                                $houlageData['iDistance'] = $iDistanceWHSTo['iDistance'];
                                                $houlageData['iDirection'] = 2; // origin haulage		

                                                $houlageData['szLatitude'] = $data['fDestinationLatitude'] ;
                                                $houlageData['szLongitude'] = $data['fDestinationLongitude'] ;

                                                if(!empty($data['szDestinationPostCode']))
                                                {
                                                        $szHaulagePostCode = $data['szDestinationPostCode'] ;
                                                }
                                                else
                                                {
                                                        $szHaulagePostCode = $data['szDestinationCity'] ;
                                                }

                                                $houlageData['fTotalWeight'] = $data['fCargoWeight'];
                                                $houlageData['fVolume'] = $data['fCargoVolume'];
                                                $houlageData['szPostcode'] = $szHaulagePostCode;
                                                $houlageData['idCountry'] = $data['idDestinationCountry'];
                                                
                                                $this->szLCLCalculationLogs .= "<br> Checking Haulage services at Destination";
                                                $this->szLCLCalculationLogs .= "<br> Weight: ".$houlageData['fTotalWeight'];
                                                $this->szLCLCalculationLogs .= "<br> Volume: ".$houlageData['fVolume'];
                                                $this->szLCLCalculationLogs .= "<br> Postcode: ".$houlageData['szPostcode'];

                                                $kHaulagePricing = new cHaulagePricing();
                                                $fToWareHouseHaulageAry = $kHaulagePricing->getHaulageDetails($houlageData);
                                                
                                                if(!empty($fToWareHouseHaulageAry))
                                                {
                                                    $this->szLCLCalculationLogs .= "<br><b> Haulage Service Found </b>";
                                                    $this->szLCLCalculationLogs .= "<br>Haulage Price: ".$fToWareHouseHaulageAry['fTotalHaulgePrice'];
                                                    $this->szLCLCalculationLogs .= "<br>Model: ".$fToWareHouseHaulageAry['szModelName'];
                                                    $this->szLCLCalculationLogs .= "<br>Haulage Transit: ".$fToWareHouseHaulageAry['iHaulageTransitTime'];
                                                    
                                                }
                                                else
                                                {
                                                    $this->szLCLCalculationLogs .= "<br> <span style='color:red'> No Haulage service found at Destination.</span>";
                                                }

                                                $fTotalHaulagePrice = $fToWareHouseHaulageAry['fTotalHaulgePrice'];
                        
                                                //$fTotalTransitHours = $fTransitHours + $fToWareHouseHaulageAry['iHaulageTransitTime'];

                                                /**
                                                * Adding haulage transit time from Origing and Destination with wtw transit time 
                                                **/ 
                                                if($data['idTimingType']==1) //Ready at origin  WTD
                                                {
                                                    $fTotalTransitHours = $fTransitHours + $fToWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                    $fTotalTransitDays_Hours = $fTransiDays_hour + $fToWareHouseHaulageAry['iHaulageTransitTime'] ;											
                                                    $iBookingCutOffHours = ($row['iBookingCutOffHours']*60*60);											
                                                }
                                                else
                                                {
                                                    $fTotalTransitHours = $fTransitHours + $fToWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                    $fTotalTransitDays_Hours = $fTransiDays_hour + $fToWareHouseHaulageAry['iHaulageTransitTime'] ;
                                                    $iBookingCutOffHours = (($row['iBookingCutOffHours'] + $fToWareHouseHaulageAry['iHaulageTransitTime'])*60*60);
                                                }
                                            }
                                            else if($data['idServiceType']==(int)__SERVICE_TYPE_WTP__) //WTP
                                            {
                                                if($row['iOriginChargesApplicable']!=1)
                                                {
                                                    continue;
                                                }

                                                /**
                                                * for WTD Standard Trucking rate is applicable at Origin and Haulage Price is applicable at Destination
                                                */  
                                                $fTotalDestinationPortPrice = 0;
                                                $fDestinationFreightExchangeRate = 0;

                                                $fTotalPortPrice = $fTotalOriginPortPrice ;

                                                /**
                                                 * calculating standard trucking rate at Origin in USD
                                                */										

                                                $fTruckingRateFrom = $iDistanceWHSFrom['iDistance'] * $iDistanceWHSFrom['fStandardTruckRate'];

                                                /**
                                                 * calculating standard trucking rate at Destination in USD
                                                */	
                                                $fTruckingRateTo = $iDistanceWHSTo['iDistance'] * $iDistanceWHSTo['fStandardTruckRate'];

                                                $fTotalStandardTruckingRate = $fTruckingRateFrom + $fTruckingRateTo ;


                                                //$fTotalTransitHours = $fTransitHours + $fToWareHouseHaulageAry['iHaulageTransitTime'];

                                                /**
                                                * Adding haulage transit time from Origing and Destination with wtw transit time 
                                                **/ 
                                                if($data['idTimingType']==1) //Ready at origin  WTD
                                                {
                                                    $fTotalTransitHours = $fTransitHours ;
                                                    $fTotalTransitDays_Hours = $fTransiDays_hour ;											
                                                    $iBookingCutOffHours = ($row['iBookingCutOffHours']*60*60);											
                                                }
                                                else
                                                {
                                                    $fTotalTransitHours = $fTransitHours ;
                                                    $fTotalTransitDays_Hours = $fTransiDays_hour ;
                                                    $iBookingCutOffHours = (($row['iBookingCutOffHours'])*60*60);
                                                }		
                                            }
                                            else if($data['idServiceType']==(int)__SERVICE_TYPE_PTW__) //PTW
                                            {
                                                if($row['iDestinationChargesApplicable']!=1)
                                                {
                                                    continue;
                                                }
                                                /**
                                                * for DTW Standard Trucking rate is applicable at Destination and Haulage Price is applicable at Origin
                                                */ 
                                                $fTotalOriginPortPrice = 0;
                                                $fOriginFreightExchangeRate = 0;

                                                $fTotalPortPrice = $fTotalDestinationPortPrice;

                                                $fTotalTransitHours = $fTransitHours ;
                                                $fTotalTransitDays_Hours = $fTransiDays_hour ;

                                                /**
                                                 * calculating standard trucking rate at Origin in USD
                                                 **/										

                                                $fTruckingRateFrom = $iDistanceWHSFrom['iDistance'] * $iDistanceWHSFrom['fStandardTruckRate'];

                                                /**
                                                 * calculating standard trucking rate at Destination in USD
                                                */	
                                                $fTruckingRateTo = $iDistanceWHSTo['iDistance'] * $iDistanceWHSTo['fStandardTruckRate'];

                                                $fTotalStandardTruckingRate = $fTruckingRateFrom + $fTruckingRateTo ;

                                                $iBookingCutOffHours = (($row['iBookingCutOffHours'])*60*60);

                                                $iBookingCutOffHours_available = (($row['iBookingCutOffHours'])*60*60);


                                                /**
                                                * Adding haulage transit time from Origing and Destination with wtw transit time 
                                                **/ 
                                                if($data['idTimingType']==1) //Ready at origin  DTW
                                                {
                                                    $fTotalTransitHours = $fTransitHours ;
                                                    $fTotalTransitDays_Hours = $fTransiDays_hour ;											
                                                    $iBookingCutOffHours = (($row['iBookingCutOffHours'] )*60*60);										
                                                }
                                                else
                                                {
                                                    $fTotalTransitHours = $fTransitHours ;
                                                    $fTotalTransitDays_Hours = $fTransiDays_hour;
                                                    $iBookingCutOffHours = (($row['iBookingCutOffHours']*60*60));
                                                }
                                            }	
                                            else if($data['idServiceType']==(int)__SERVICE_TYPE_PTP__) // PTP
                                            {
                                                /**
                                                 * calculating standard trucking rate
                                                */
                                                $fTruckingRateTo = $iDistanceWHSTo['iDistance'] * $iDistanceWHSTo['fStandardTruckRate'];

                                                $fTruckingRateFrom = $iDistanceWHSFrom['iDistance'] * $iDistanceWHSFrom['fStandardTruckRate'];

                                                $fTotalStandardTruckingRate = $fTruckingRateFrom + $fTruckingRateTo ;

                                                $fTotalTransitHours = $fTransitHours ;
                                                $fTotalTransitDays_Hours = $fTransiDays_hour ;
                                                $iBookingCutOffHours = (($row['iBookingCutOffHours'])*60*60);
                                            }
                                            if((($data['idServiceType']==__SERVICE_TYPE_DTD__) && !empty($fToWareHouseHaulageAry) && !empty($fFromWareHouseHaulageAry)) || (($data['idServiceType']==__SERVICE_TYPE_DTW__ || $data['idServiceType']==__SERVICE_TYPE_DTP__ ) && !empty($fFromWareHouseHaulageAry)) || (($data['idServiceType']==__SERVICE_TYPE_WTD__ || $data['idServiceType']==__SERVICE_TYPE_PTD__ ) && !empty($fToWareHouseHaulageAry)) || ($data['idServiceType']==__SERVICE_TYPE_WTW__ || $data['idServiceType']==__SERVICE_TYPE_WTP__ || $data['idServiceType']==__SERVICE_TYPE_PTP__|| $data['idServiceType']==__SERVICE_TYPE_PTW__ ))
                                            { 
                                                $szOriginHaulageStr = '';
                                                $resultAry[$ctr]=$row;
                                                if(($data['idServiceType']==__SERVICE_TYPE_DTD__) || ($data['idServiceType']==__SERVICE_TYPE_DTW__) || ($data['idServiceType']==__SERVICE_TYPE_DTP__))
                                                {
                                                    $resultAry[$ctr]['fOriginHaulageDistance'] = $iDistanceWHSFrom['iDistance'] ;
                                                    $resultAry[$ctr]['fOriginHaulageTransitTime'] = $fFromWareHouseHaulageAry['iHaulageTransitTime'];
                                                    $resultAry[$ctr]['fOriginHaulageRateWM_KM'] = $fFromWareHouseHaulageAry['fPricePer100Kg'];
                                                    $resultAry[$ctr]['fOriginHaulageMinRateWM_KM'] = $fFromWareHouseHaulageAry['fMinimumPrice'];
                                                    $resultAry[$ctr]['fOriginHaulageBookingRateWM_KM'] = $fFromWareHouseHaulageAry['fPricePerBooking'];
                                                    $resultAry[$ctr]['fOriginHaulageExchangeRate'] = $fFromWareHouseHaulageAry['fExchangeRate'];
                                                    $resultAry[$ctr]['fOriginHaulagePrice'] = $fFromWareHouseHaulageAry['fTotalHaulgePrice'];
                                                    $resultAry[$ctr]['szOriginHaulageCurrency'] = $fFromWareHouseHaulageAry['szHaulageCurrency'];
                                                    $resultAry[$ctr]['idOriginHaulageCurrency'] = $fFromWareHouseHaulageAry['idCurrency'];

                                                    $resultAry[$ctr]['fOriginHaulageFuelSurcharge'] = $fFromWareHouseHaulageAry['fFuelSurcharge'];
                                                    $resultAry[$ctr]['iOriginHaulageDistanceUptoKm'] = $fFromWareHouseHaulageAry['iDistanceUptoKm'];
                                                    $resultAry[$ctr]['szOriginHaulageModelName'] = $fFromWareHouseHaulageAry['szModelName'];

                                                    $resultAry[$ctr]['fOriginHaulageWmFactor'] = $fFromWareHouseHaulageAry['fWmFactor'];
                                                    $resultAry[$ctr]['fOriginHaulageChargableWeight'] = $fFromWareHouseHaulageAry['fChargableWeight'];
                                                    $resultAry[$ctr]['fOriginHaulageFuelPercentage'] = $fFromWareHouseHaulageAry['fFuelPercentage'];
                                                    
                                                    $szOriginHaulageStr = "<br><strong>F. Export Haulage</strong>"; 
                                                    $szOriginHaulageStr .= '<br>
                                                            Formula: (max(ROUNDUP(max('.number_format((float)$data['fCargoWeight']).'kg;'.format_volume((float)$data['fCargoVolume']).'cbm x '.number_format((float)$resultAry[$ctr]['fOriginHaulageWmFactor']).'kg/cbm)/100kg) x '.$resultAry[$ctr]['szOriginHaulageCurrency'].' '.number_format((float)$resultAry[$ctr]['fOriginHaulageRateWM_KM'],2).') ; '.$resultAry[$ctr]['szOriginHaulageCurrency'].' '.number_format((float)$resultAry[$ctr]['fOriginHaulageMinRateWM_KM'],2).') + '.$resultAry[$ctr]['szOriginHaulageCurrency'].' '.number_format((float)$resultAry[$ctr]['fOriginHaulageBookingRateWM_KM'],2).') x ( 1 + ('.number_format((float)$resultAry[$ctr]['fOriginHaulageFuelPercentage'],1).'%)) = '.$resultAry[$ctr]['szOriginHaulageCurrency']." ".number_format((float)($fFromWareHouseHaulageAry['fTotalHaulgePrice']/$data['fExchangeRate']),6);;
                                                    $szOriginHaulageStr .= "<br> ROE: ".($resultAry[$ctr]['fOriginHaulageExchangeRate']>0?round((float)(1/$resultAry[$ctr]['fOriginHaulageExchangeRate']),4):'0');
                                                    $szOriginHaulageStr .= "<br>Export Haulage price (USD): ".$resultAry[$ctr]['fOriginHaulagePrice']; 
                                                } 
                                                $szDestinationHaulageStr = '';
                                                if(($data['idServiceType']==__SERVICE_TYPE_DTD__) || ($data['idServiceType']==__SERVICE_TYPE_WTD__) || ($data['idServiceType']==__SERVICE_TYPE_PTD__))
                                                {
                                                    $resultAry[$ctr]['fDestinationHaulageDistance'] = $iDistanceWHSTo['iDistance'] ;
                                                    $resultAry[$ctr]['fDestinationHaulageTransitTime'] = $fToWareHouseHaulageAry['iHaulageTransitTime'];
                                                    $resultAry[$ctr]['fDestinationHaulageRateWM_KM'] = $fToWareHouseHaulageAry['fPricePer100Kg'];
                                                    $resultAry[$ctr]['fDestinationHaulageMinRateWM_KM'] = $fToWareHouseHaulageAry['fMinimumPrice'];
                                                    $resultAry[$ctr]['fDestinationHaulageBookingRateWM_KM'] = $fToWareHouseHaulageAry['fPricePerBooking'];
                                                    $resultAry[$ctr]['fDestinationHaulageExchangeRate'] = $fToWareHouseHaulageAry['fExchangeRate'];
                                                    $resultAry[$ctr]['fDestinationHaulagePrice'] = $fToWareHouseHaulageAry['fTotalHaulgePrice'];
                                                    $resultAry[$ctr]['szDestinationHaulageCurrency'] = $fToWareHouseHaulageAry['szHaulageCurrency'];
                                                    $resultAry[$ctr]['idDestinationHaulageCurrency'] = $fToWareHouseHaulageAry['idCurrency'];
                                                    $resultAry[$ctr]['fDestinationHaulageFuelSurcharge'] = $fToWareHouseHaulageAry['fFuelSurcharge'];
                                                    $resultAry[$ctr]['iDestinationHaulageDistanceUptoKm'] = $fToWareHouseHaulageAry['iDistanceUptoKm'];
                                                    $resultAry[$ctr]['szDestinationHaulageModelName'] = $fToWareHouseHaulageAry['szModelName'];

                                                    $resultAry[$ctr]['fDestinationHaulageWmFactor'] = $fToWareHouseHaulageAry['fWmFactor'];
                                                    $resultAry[$ctr]['fDestinationHaulageChargableWeight'] = $fToWareHouseHaulageAry['fChargableWeight'];
                                                    $resultAry[$ctr]['fDestinationHaulageFuelPercentage'] = $fToWareHouseHaulageAry['fFuelPercentage'];	
                                                    
                                                    $szDestinationHaulageStr = "<br><strong>G. Import Haulage</strong>";
                                                    $szDestinationHaulageStr .= ' <br>
                                                            (max(ROUNDUP(max('.number_format((float)$data['fCargoWeight']).'kg;'.format_volume((float)$data['fCargoVolume']).'cbm x '.number_format((float)$resultAry[$ctr]['fDestinationHaulageWmFactor']).'kg/cbm)/100kg) x '.$resultAry[$ctr]['szDestinationHaulageCurrency'].' '.number_format((float)$resultAry[$ctr]['fDestinationHaulageRateWM_KM'],2).') ; '.$resultAry[$ctr]['szDestinationHaulageCurrency'].' '.number_format((float)$resultAry[$ctr]['fDestinationHaulageMinRateWM_KM'],2).') + '.$resultAry[$ctr]['szDestinationHaulageCurrency'].' '.number_format((float)$resultAry[$ctr]['fDestinationHaulageBookingRateWM_KM'],2).') x ( 1 + ('.number_format((float)$resultAry[$ctr]['fDestinationHaulageFuelPercentage'],1).'%)) = '.$resultAry[$ctr]['szDestinationHaulageCurrency']." ".number_format((float)($fToWareHouseHaulageAry['fTotalHaulgePrice']/$data['fExchangeRate']),6);
                                                    $szDestinationHaulageStr .= "<br> ROE: ".($resultAry[$ctr]['fDestinationHaulageExchangeRate']>0?round((float)(1/$resultAry[$ctr]['fDestinationHaulageExchangeRate']),4):'0');
                                                    $szDestinationHaulageStr .= "<br><i> Import Haulage price (USD): ".$resultAry[$ctr]['fDestinationHaulagePrice']."</i>";
                                                }

                                                if(($data['idServiceType']==__SERVICE_TYPE_WTW__) || ($data['idServiceType']==__SERVICE_TYPE_DTW__) || ($data['idServiceType']==__SERVICE_TYPE_PTW__) || ($data['idServiceType']==__SERVICE_TYPE_PTP__) || ($data['idServiceType']==__SERVICE_TYPE_DTP__) || ($data['idServiceType']==__SERVICE_TYPE_WTP__))
                                                {
                                                    $resultAry[$ctr]['fDestinationTruckingRate'] = $fTruckingRateTo ;
                                                }
                                                if(($data['idServiceType']==__SERVICE_TYPE_WTW__) || ($data['idServiceType']==__SERVICE_TYPE_WTD__) || ($data['idServiceType']==__SERVICE_TYPE_WTP__) || ($data['idServiceType']==__SERVICE_TYPE_PTP__) || ($data['idServiceType']==__SERVICE_TYPE_PTW__) || ($data['idServiceType']==__SERVICE_TYPE_PTD__))
                                                {
                                                    $resultAry[$ctr]['fOriginTruckingRate'] = $fTruckingRateFrom ;
                                                }

                                                $resultAry[$ctr]['fTotalDestinationPortPrice'] = $fTotalDestinationPortPrice;
                                                $resultAry[$ctr]['szDestinationPortCurrency'] = $szDestinationPortCurrency;

                                                if($fDestinationFreightExchangeRate>0)
                                                {
                                                    $resultAry[$ctr]['fDestinationFreightExchangeRate'] = round((float)(1/$fDestinationFreightExchangeRate),6);
                                                }
                                                else
                                                {
                                                    $resultAry[$ctr]['fDestinationFreightExchangeRate'] = 0 ;	
                                                }

                                                $resultAry[$ctr]['fTotalOriginPortPrice'] = $fTotalOriginPortPrice;
                                                $resultAry[$ctr]['szOriginPortCurrency'] = $szOriginPortCurrency;

                                                if($fOriginFreightExchangeRate>0)
                                                {
                                                    $resultAry[$ctr]['fOriginFreightExchangeRate'] = round((float)(1/$fOriginFreightExchangeRate),6);
                                                }
                                                else
                                                {
                                                    $resultAry[$ctr]['fOriginFreightExchangeRate'] = 0 ;
                                                }

                                                $fPortToPortPriceLocalCurrency = 0;
                                                if($row['szFreightCurrency']==1)  // USD
                                                {
                                                    $fPrice = $row['fRateWM'] * $fPriceFactor ;
                                                    $fBookingRate = $row['fRate'];
                                                    $fMinRateWM = $row['fMinRateWM'];
                                                    $resultAry[$ctr]['fFreightExchangeRate'] = 1 ; 	

                                                    if($fPrice < $fMinRateWM)
                                                    {
                                                        $fPrice = $row['fMinRateWM'];								
                                                    }		
                                                    $row['szFreightCurrencyName'] = 'USD';
                                                    $resultAry[$ctr]['szFreightCurrencyName'] = "USD";
                                                    $fPortToPortPriceLocalCurrency = $fPrice;
                                                }
                                                else
                                                {
                                                    /**
                                                    * getting USD factor
                                                    */  
                                                    if($usedCurrencyAry[$row['szFreightCurrency']])
                                                    {
                                                        $currencyAry = $usedCurrencyAry[$row['szFreightCurrency']] ;
                                                    }
                                                    else
                                                    {
                                                        $currencyAry = array();
                                                        $currencyAry = $this->getCurrencyDetails($row['szFreightCurrency']);
                                                        $usedCurrencyAry[$row['szFreightCurrency']] = $currencyAry ;
                                                    }
                                                    $fUSDValue = $currencyAry['fUsdValue'];  
                                                    $row['szFreightCurrencyName'] = $currencyAry['szCurrency'];
                                                    $resultAry[$ctr]['szFreightCurrencyName'] = $currencyAry['szCurrency'];
                                                    /**
                                                    * converting price to USD
                                                    */	

                                                    $fPrice = $row['fRateWM'] * $fPriceFactor ;
                                                    $fBookingRate = $row['fRate'];
                                                    $fMinRateWM = $row['fMinRateWM'];

                                                    if($fPrice < $fMinRateWM)
                                                    {
                                                        $fPrice = $row['fMinRateWM'];								
                                                    }		
                                                    $fPortToPortPriceLocalCurrency = $fPrice;
                                                    
                                                    $fPrice = $fPrice * $fUSDValue ;
                                                    $fBookingRate = $row['fRate'] * $fUSDValue;
                                                    $fMinRateWM = $row['fMinRateWM'] * $fUSDValue ;
                                                    $resultAry[$ctr]['fFreightExchangeRate'] = $fUSDValue;
                                                }
                                                
                                                $szPorttoPortStr = '<br><strong>B. Port to Port Price</strong>';
                                                $szPorttoPortStr .= '<br>MAX('.$resultAry[$ctr]['szFreightCurrency'].' '.$resultAry[$ctr]['fRateWM'].' x max ('.ceil($data['fCargoVolume']).'cbm;'.ceil($data['fCargoWeight']/1000).'mt);'.$resultAry[$ctr]['szFreightCurrency'].' '.$resultAry[$ctr]['fMinRateWM'].') + '.$resultAry[$ctr]['szFreightCurrency'].' '.$resultAry[$ctr]['fBookingRate'].' = '.$resultAry[$ctr]['szFreightCurrency']." ".$fPortToPortPriceLocalCurrency; 
                                                $szPorttoPortStr .= "<br>P2P Currency: ".$resultAry[$ctr]['szFreightCurrencyName'];
                                                $szPorttoPortStr .= "<br>P2P ROE: ".$resultAry[$ctr]['fFreightExchangeRate'];
                                                $szPorttoPortStr .= "<br>P2P Price(USD): ".$fPrice; 
                                                $szPorttoPortStr .= "<br>P2P Booking Price(USD): ".$fBookingRate; 
                        
                                                $resultAry[$ctr]['iRailTransport'] = $iRailTransport;
                                                /**
                                                * calculating custom clearance amount in USD
                                                **/
                                                $szExportCustomStr = '';
                                                $szImportCustomStr = "";
                                                if($data['iOriginCC']==1)  // OriginCustomClearance
                                                {
                                                    $originCCAry=array();
                                                    $originCCAry = $this->getCustomClearanceDetails($idWHSTo,$idWHSFrom,$data['iOriginCC']);
                                                    $resultAry[$ctr]['fOriginCCPrice'] = $originCCAry['fPrice'];
                                                    $resultAry[$ctr]['fOriginCCCurrency'] = $originCCAry['idCurrency'];
                                                    $resultAry[$ctr]['szOriginCCCurrency'] = $originCCAry['szCurrency'];
                                                    $resultAry[$ctr]['fOriginCCExchangeRate'] = $originCCAry['fExchangeRate'];
                        
                                                    $szExportCustomStr = '<br><strong>D. Export Custom Clearance</strong>';
                                                    $szExportCustomStr .= "<br> Currency: ".$originCCAry['szCurrency'];
                                                    $szExportCustomStr .= "<br> ROE: ".$originCCAry['fExchangeRate'];
                                                    $szExportCustomStr .= "<br> Export CC Price (USD): ".$originCCAry['fPrice'];
                                                }
                                                if($data['iDestinationCC']==2)  // DestinationCustomClearance
                                                {
                                                    $DestinationCCAry=array();
                                                    $DestinationCCAry = $this->getCustomClearanceDetails($idWHSTo,$idWHSFrom,$data['iDestinationCC']);

                                                    $resultAry[$ctr]['fDestinationCCPrice'] = $DestinationCCAry['fPrice'];
                                                    $resultAry[$ctr]['fDestinationCCCurrency'] = $DestinationCCAry['idCurrency'];
                                                    $resultAry[$ctr]['szDestinationCCCurrency'] = $DestinationCCAry['szCurrency'];
                                                    $resultAry[$ctr]['fDestinationCCExchangeRate'] = $DestinationCCAry['fExchangeRate'];
                        
                                                    $szImportCustomStr = '<br><strong>E. Import Custom Clearance</strong>';
                                                    $szImportCustomStr .= "<br> Currency: ".$DestinationCCAry['szCurrency'];
                                                    $szImportCustomStr .= "<br> ROE: ".$DestinationCCAry['fExchangeRate'];
                                                    $szImportCustomStr .= "<br> Import CC Price (USD): ".$DestinationCCAry['fPrice'];
                                                }
                                                $fCustomAmount = $originCCAry['fPrice'] + $DestinationCCAry['fPrice'] ;  

                                                /**
                                                 * calculating total price for shipment.
                                                 * 
                                                 * Total price = WTW Price + custom clearance amount + rate per booking  
                                                 *               + standard trucking rate ( if applicable ) + Haulage price ( if applicable ) 
                                                */

                                                $fPrice = round((float)($fPrice + $fTotalPortPrice),6);
                                                $fCustomAmount = round((float)$fCustomAmount,6);
                                                $fBookingRate = round((float)$fBookingRate,6);
                                                $fTotalStandardTruckingRate = round((float)($fTotalStandardTruckingRate * $fPriceFactor),6);
                                                $fTotalHaulagePrice = round((float)$fTotalHaulagePrice,6);

                                                $fTotalPrice = $fPrice + $fCustomAmount + $fBookingRate + ($fTotalStandardTruckingRate) + $fTotalHaulagePrice ;
                                                $fPriceUSD = $fTotalPrice - $fTotalStandardTruckingRate ;
                                                $szCalculatedPriceStr .= "<br> Rate per booking USD: ".number_format((float)$fBookingRate,2);
                                                $resultAry[$ctr]['fPTPPrice'] = $fPrice + $fBookingRate -$fTotalPortPrice ;
                                                $resultAry[$ctr]['fWTWPrice'] = $fPrice + $fBookingRate;
                                                
                                                $fTotalSelfInvoiceAmountUSD = $fPriceUSD;
                        
                                                // this the price which are display for the user 
                                                $szCalculatedPriceStr = "<br><br><strong>Customer Prices1 </strong>";
                                                $szCalculatedPriceStr .= "<br>Customer currency: ".$data['szCurrency'];
                                                $szCalculatedPriceStr .= "<br>Customer ROE: ".$data['fExchangeRate'];
                                                if($data['fExchangeRate']>0)
                                                {
                                                    $fPrice = round((float)$fPrice/$data['fExchangeRate'],6);
                                                    $fCustomAmount = round((float)$fCustomAmount/$data['fExchangeRate'],6);
                                                    $fBookingRate = round((float)$fBookingRate/$data['fExchangeRate'],6);
                                                    $fTotalStandardTruckingRate = round((float)(($fTotalStandardTruckingRate)/$data['fExchangeRate']),6);
                                                    $fTotalHaulagePrice = round((float)$fTotalHaulagePrice/$data['fExchangeRate'],6); 
                                                    $resultAry[$ctr]['fDisplayPrice'] =number_format((double)( ($fPrice + $fCustomAmount + $fBookingRate + $fTotalHaulagePrice)),6,'.','') ;
                        
                                                }
                                                else
                                                {  
                                                    $resultAry[$ctr]['fDisplayPrice'] =number_format((double)(($fPrice + $fCustomAmount + $fBookingRate + $fTotalHaulagePrice)),6,'.','') ;
                                                } 
                                                
                                                $szCalculatedPriceStr .= "<br><br> Freight Charges: ".number_format((float)$fPrice,2);
                                                $szCalculatedPriceStr .= "<br> Rate per booking: ".number_format((float)$fBookingRate,2);
                                                $szCalculatedPriceStr .= "<br> Custom Clearance price: ".number_format((float)$fCustomAmount,2);
                                                $szCalculatedPriceStr .= "<br> Haulage price: ".number_format((float)$fTotalHaulagePrice,2);
                                                
                                                $szCalculatedPriceStr .= "<br><br>Transportation Price: ". $fPrice ." + ". $fCustomAmount ." + ". $fBookingRate ." + ". $fTotalHaulagePrice;
                                                $szCalculatedPriceStr .= "<br> Transportation Price: ".$data['szCurrency']." ".$resultAry[$ctr]['fDisplayPrice'];
                        
                                                $bPrivateCustomerFeeApplied = false; 
                                                $szPrivateCustomerFeeStr = "<br><strong>H. Private Customer Fee </strong>";
                                                
                                                if($iPrivateShipping==1)
                                                {
                                                    $resultAry[$ctr]['szCustomerType'] = "Private"; 
                                                }
                                                else
                                                {
                                                    $resultAry[$ctr]['szCustomerType'] = "Business"; 
                                                }
                                                if($iPrivateShipping==1)
                                                { 
                                                    if($bDTDTradesFlag)
                                                    {
                                                        $szForwarderProduct = 'LTL';
                                                    }
                                                    else
                                                    {
                                                        $szForwarderProduct = 'LCL';
                                                    }
                                                    $privateCustomerFeeAry = array();
                                                    $privateCustomerFeeAry = $kForwarder_new->getAllPrivateCustomerPricing($iDistanceWHSTo['idForwarder'],$szForwarderProduct,1);
                        
                                                    $fShipmentUSDCost = $fPriceUSD; 
                                                    if(!empty($privateCustomerFeeAry[$szForwarderProduct]))
                                                    {
                                                        $privateCustomerFeeArys = $privateCustomerFeeAry[$szForwarderProduct];
                                                        if($privateCustomerFeeArys['idCurrency']==1)
                                                        {
                                                            $idPrivateCustomerFeeCurrency = 1; //USD
                                                            $szPrivateCustomerFeeCurrency = "USD";
                                                            $fPrivateCustomerFeeExchangeRate = 1;

                                                            $fPrivateCustomerFee = $privateCustomerFeeArys['fCustomerFee'];
                                                            $fPrivateCustomerFeeUSD = $privateCustomerFeeArys['fCustomerFee'];
                                                        }
                                                        else
                                                        {
                                                            $privateCurrAry = array();
                                                            $privateCurrAry = $this->getCurrencyDetails($privateCustomerFeeArys['idCurrency']); 
                                                            $fPrivateCustomerFeeExchangeRate = $privateCurrAry['fUsdValue']; 
                                                            $szPrivateCustomerFeeCurrency = $privateCurrAry['szCurrency']; 
                                                            $idPrivateCustomerFeeCurrency = $privateCurrAry['idCurrency'];  
                                                            
                                                            $fPrivateCustomerFee = $privateCustomerFeeArys['fCustomerFee'];

                                                            $fPrivateCustomerFeeUSD = $privateCustomerFeeArys['fCustomerFee'] * $fPrivateCustomerFeeExchangeRate;
                                                        } 
                                                        $bPrivateCustomerFeeApplied = true; 
                                                        $fShipmentUSDCost = $fShipmentUSDCost + $fPrivateCustomerFeeUSD;   
                                                        $fTotalSelfInvoiceAmountUSD = $fTotalSelfInvoiceAmountUSD + $fPrivateCustomerFeeUSD;

                                                        $szPrivateCustomerFeeStr .= "<br> Local Currency: ".$szPrivateCustomerFeeCurrency;
                                                        $szPrivateCustomerFeeStr .= "<br> Local ROE: ".$fPrivateCustomerFeeExchangeRate;
                                                        $szPrivateCustomerFeeStr .= " <br> Private customer fee for booking: USD ".$fPrivateCustomerFeeUSD;     

                                                        $fPriceUSD = $fShipmentUSDCost; 
                                                        if($data['fExchangeRate']>0)
                                                        {
                                                            $fTotalPrice = round((float)$fPriceUSD/$data['fExchangeRate'],6);
                                                            $resultAry[$ctr]['fDisplayPrice'] = round((float)$fPriceUSD/$data['fExchangeRate'],6);
                                                            $szCalculatedPriceStr .= "<br> Price Including Private Customer Fee Fee: ".$data['szCurrency']." ".$resultAry[$ctr]['fDisplayPrice'];
                                                        } 
                                                    }  
                                                }
                                                else
                                                {
                                                    $szPrivateCustomerFeeStr .= " <br> Private customer fee applied: No ";
                                                } 
                                                if($bPrivateCustomerFeeApplied)
                                                {
                                                    $resultAry[$ctr]['iPrivateCustomerFeeApplicable'] = 1; 
                                                    $resultAry[$ctr]['idPrivateCustomerFeeCurrency'] = $idPrivateCustomerFeeCurrency;
                                                    $resultAry[$ctr]['szPrivateCustomerFeeCurrency'] = $szPrivateCustomerFeeCurrency;
                                                    $resultAry[$ctr]['fPrivateCustomerFeeExchangeRate'] = $fPrivateCustomerFeeExchangeRate; 
                                                    $resultAry[$ctr]['fPrivateCustomerFee'] = $fPrivateCustomerFee;
                                                    $resultAry[$ctr]['fPrivateCustomerFeeUSD'] = $fPrivateCustomerFeeUSD;  
                                                }
                                                else
                                                {
                                                    $resultAry[$ctr]['iPrivateCustomerFeeApplicable'] = 0;
                                                }
                        
                                                $webKey = $iDistanceWHSTo['idForwarder'];
                                                $bHandlingFeeApplied = false; 
                                                $szHandlingFeeStr = "<br><strong>I. Hanling Fee </strong>";
                                                if(!empty($vogaHandlingFeeAry['lcl'][$webKey]))
                                                { 
                                                    $handlingFeeAry = $vogaHandlingFeeAry['lcl'][$webKey];
                                                    $fShipmentUSDCost = $fPriceUSD; 

                                                    $bHandlingFeeApplied = true;
                                                    if($handlingFeeAry['idHandlingCurrency']==1)
                                                    {
                                                        $idHandlingCurrency = $handlingFeeAry['idHandlingCurrency'];
                                                        $szHandlingCurrencyName = $handlingFeeAry['szHandlingCurrencyName'];
                                                        $fHandlingCurrencyExchangeRate = $handlingFeeAry['fHandlingCurrencyExchangeRate'];

                                                        $fHandlingFeePerBooking = $handlingFeeAry['fHandlingFeePerBooking'];
                                                    }
                                                    else
                                                    {
                                                        $resultAry = $this->getCurrencyDetails($handlingFeeAry['idHandlingCurrency']); 
                                                        $fHandlingCurrencyExchangeRate = $resultAry['fUsdValue']; 
                                                        $szHandlingCurrencyName = $resultAry['szCurrency']; 
                                                        $idHandlingCurrency = $resultAry['idCurrency'];  

                                                        $fHandlingFeePerBooking = $handlingFeeAry['fHandlingFeePerBooking'] * $fHandlingCurrencyExchangeRate;
                                                    } 
                                                    if($handlingFeeAry['idHandlingMinMarkupCurrency']==1)
                                                    {
                                                        $idHandlingMinMarkupCurrency = $handlingFeeAry['idHandlingMinMarkupCurrency'];
                                                        $szHandlingMinMarkupCurrencyName = $handlingFeeAry['szHandlingMinMarkupCurrencyName'];
                                                        $fHandlingMinMarkupCurrencyExchangeRate = $handlingFeeAry['fHandlingMinMarkupCurrencyExchangeRate']; 
                                                        $fHandlingMinMarkupPrice = $handlingFeeAry['fHandlingMinMarkupPrice'];
                                                    }
                                                    else
                                                    {
                                                        $resultAry = $this->getCurrencyDetails($handlingFeeAry['idHandlingMinMarkupCurrency']); 
                                                        $fHandlingMinMarkupCurrencyExchangeRate = $resultAry['fUsdValue']; 
                                                        $szHandlingMinMarkupCurrencyName = $resultAry['szCurrency']; 
                                                        $idHandlingMinMarkupCurrency = $resultAry['idCurrency'];  

                                                        $fHandlingMinMarkupPrice = $handlingFeeAry['fHandlingMinMarkupPrice'] * $fHandlingMinMarkupCurrencyExchangeRate;
                                                    }  
                                                    $fHandlingMarkupPercentage = $handlingFeeAry['fHandlingMarkupPercentage'];
                                                    $fHandlingMarkupPercentageValue = $fShipmentUSDCost * $handlingFeeAry['fHandlingMarkupPercentage'] * 0.01;

                                                    if($fHandlingMarkupPercentageValue>$fHandlingMinMarkupPrice)
                                                    {
                                                        $fHandlingMarkupFeeUsd = $fHandlingMarkupPercentageValue;
                                                    }
                                                    else
                                                    {
                                                        $fHandlingMarkupFeeUsd = $fHandlingMinMarkupPrice;
                                                    } 
                                                    $fTotalHandlingFeeUSD = $fHandlingFeePerBooking + $fHandlingMarkupFeeUsd;
                                                    $fShipmentUSDCost = $fShipmentUSDCost + $fTotalHandlingFeeUSD;   
                        
                                                    $szHandlingFeeStr .= "<br> Local Currency: ".$szHandlingMinMarkupCurrencyName;
                                                    $szHandlingFeeStr .= "<br> Local ROE: ".$fHandlingMinMarkupCurrencyExchangeRate;
                                                    $szHandlingFeeStr .= " <br> Handling fee pre booking: USD ".$fHandlingFeePerBooking;                    
                                                    $szHandlingFeeStr .= "<br>Handling Markup (".$handlingFeeAry['fHandlingMarkupPercentage']."%, minimum USD ".$fHandlingMinMarkupPrice.") – USD ".$fHandlingMarkupFeeUsd;
                                                    $szHandlingFeeStr .= "<br> Handling Fee (USD) ".$fTotalHandlingFeeUSD;
                                                    
                                                    $fPriceUSD = $fShipmentUSDCost; 
                                                    if($data['fExchangeRate']>0)
                                                    {
                                                        $fTotalPrice = round((float)$fPriceUSD/$data['fExchangeRate'],6);
                                                        $resultAry[$ctr]['fDisplayPrice'] = round((float)$fPriceUSD/$data['fExchangeRate'],6);
                                                        $szCalculatedPriceStr .= "<br> Price Including Handling Fee: ".$data['szCurrency']." ".$resultAry[$ctr]['fDisplayPrice'];
                                                    } 
                                                }
                                                else
                                                {
                                                    $szHandlingFeeStr .= " <br> Handling Price Applied: No ";
                                                } 
                        
                                                if($bHandlingFeeApplied)
                                                {
                                                    $resultAry[$ctr]['iHandlingFeeApplicable'] = 1;
                                                    $resultAry[$ctr]['iProductType'] = 1; //LCL
                                                    $resultAry[$ctr]['idHandlingCurrency'] = $idHandlingCurrency;
                                                    $resultAry[$ctr]['szHandlingCurrencyName'] = $szHandlingCurrencyName;
                                                    $resultAry[$ctr]['fHandlingCurrencyExchangeRate'] = $fHandlingCurrencyExchangeRate;

                                                    $resultAry[$ctr]['fHandlingMarkupPercentage'] = $fHandlingMarkupPercentage; 

                                                    $resultAry[$ctr]['idHandlingMinMarkupCurrency'] = $idHandlingMinMarkupCurrency;
                                                    $resultAry[$ctr]['szHandlingMinMarkupCurrencyName'] = $szHandlingMinMarkupCurrencyName;
                                                    $resultAry[$ctr]['fHandlingMinMarkupCurrencyExchangeRate'] = $fHandlingMinMarkupCurrencyExchangeRate;
                                                    $resultAry[$ctr]['fHandlingMinMarkupPrice'] = $fHandlingMinMarkupPrice; 
                                                    $resultAry[$ctr]['fHandlingFeePerBooking'] = $fHandlingFeePerBooking; 
                                                    $resultAry[$ctr]['fTotalHandlingFeeUSD'] = $fTotalHandlingFeeUSD;  
                                                }
                                                else
                                                {
                                                    $resultAry[$ctr]['iHandlingFeeApplicable'] = 0;
                                                }
                                                
                                                $bManualFeeApplicable = false;
                                                $bRailTransport = false;
                                                if($data['iQuickQuote']==1 && $data['iQuickQuoteTryitout']!=1)
                                                {
                                                    $fShipmentUSDCost = $fPriceUSD;
                                                    $szHandlingFeeStr .= " <br> Manual Fee Applied: Yes ";
//                                                    $szHandlingFeeStr .= " <br> Sales price excluding Manual fee: USD ".number_format((float)$fShipmentUSDCost);
//                                                    $szHandlingFeeStr .= " <br> Sales price excluding Manual fee: ".number_format((float)$resultAry[$ctr]['fDisplayPrice']);
                                                    
                                                    $manualFeeSearchAry = array();
                                                    $manualFeeSearchAry['idForwarder'] = $iDistanceWHSTo['idForwarder'];
                                                    
                                                    $railTransportAry = array();
                                                    $railTransportAry['idForwarder'] = $iDistanceWHSTo['idForwarder'];
                                                    $railTransportAry['idFromWarehouse'] = $idWHSFrom;
                                                    $railTransportAry['idToWarehouse'] = $idWHSTo;  
                        
                                                    if($kService->isRailTransportExists($railTransportAry))
                                                    {
                                                        $manualFeeSearchAry['szProduct'] = __MANUAL_FEE_RAIL__;
                                                        $bRailTransport = true;
                                                    }
                                                    else if($bDTDTradesFlag)
                                                    {
                                                        $manualFeeSearchAry['szProduct'] = __MANUAL_FEE_LTL__;
                                                    }
                                                    else
                                                    {
                                                        $manualFeeSearchAry['szProduct'] = __MANUAL_FEE_LCL__;
                                                    } 
                                                    $manualFeeSearchAry['szCargo'] = $data['szCargoType'];
                                                    $kVatApplication = new cVatApplication();
                                                    $manualFeeAry = array();
                                                    $manualFeeAry = $kVatApplication->getManualFeeListing(false, false, $manualFeeSearchAry);
                                                    $manualFeeAry = $manualFeeAry[0];
                                                    
                                                    if(empty($manualFeeAry))
                                                    {
                                                        /*
                                                        * If There is no Manual fee defined for the Forwarder then we calculate based on All other forwarder manual fee rates
                                                        */
                                                        $manualFeeSearchAry = array();
                                                        $manualFeeSearchAry['iAllForwarderFlag'] = '1';
                                                        if($bRailTransport)
                                                        {
                                                            $manualFeeSearchAry['szProduct'] = __MANUAL_FEE_RAIL__;
                                                        }
                                                        else if($bDTDTradesFlag)
                                                        {
                                                            $manualFeeSearchAry['szProduct'] = __MANUAL_FEE_LTL__;
                                                        }
                                                        else
                                                        {
                                                            $manualFeeSearchAry['szProduct'] = __MANUAL_FEE_LCL__;
                                                        }  
                                                        $manualFeeSearchAry['szCargo'] = $data['szCargoType'];
                                                        $manualFeeAry = $kVatApplication->getManualFeeListing(false, false, $manualFeeSearchAry);
                                                        $manualFeeAry = $manualFeeAry[0];
                                                    }
                                                    
                                                    if(empty($manualFeeAry))
                                                    {
                                                        /*
                                                        * If There is no Manual fee defined for the Trade then we calculate based on default manual fee rates
                                                        */
                                                        $manualFeeAry = $kVatApplication->getDefaultManualFee();
                                                    } 
                                                    if(!empty($manualFeeAry))
                                                    {
                                                        $bManualFeeApplicable = true;
                                                        $idManualFeeCurrency = $manualFeeAry['idCurrency'];
                                                        $fManualFee = $manualFeeAry['fManualFee']; 
                                                        if($idManualFeeCurrency==1)
                                                        {
                                                            $fTotalManualFeeUSD = $fManualFee;
                                                            $fManualFeeExchangeRates = 1;
                                                            $szManualFeeCurrency = 'USD';
                                                        }
                                                        else
                                                        {
                                                            $manualCurrencyAry = array();
                                                            $manualCurrencyAry = $this->getCurrencyDetails($idManualFeeCurrency); 
                                                            $fManualFeeExchangeRates = $manualCurrencyAry['fUsdValue']; 
                                                            $szManualFeeCurrency = $manualCurrencyAry['szCurrency'];  

                                                            $fTotalManualFeeUSD = $fManualFee * $fManualFeeExchangeRates;
                                                        } 
                        
                                                        $szHandlingFeeStr .= "<br> Local Currency: ".$szManualFeeCurrency;
                                                        $szHandlingFeeStr .= "<br> Local ROE: ".$fManualFeeExchangeRates;
                                                        $szHandlingFeeStr .= " <br> Manual fee: ".$manualFeeAry['szCurrency']." ".number_format((float)$fManualFee,2);
                                                        $szHandlingFeeStr .= "<br> Manual Fee (USD) ".$fTotalManualFeeUSD;
                                                    }
                                                    else
                                                    {
                                                        $this->$szHandlingFeeStr .= " <br> There is no Manual fee defined for the trade.";
                                                    }
                                                }
                                                else
                                                {
                                                    $szHandlingFeeStr .= " <br> Manual Fee Applied: No ";
                                                }  
                                                
                                                if($bManualFeeApplicable)
                                                {
                                                    $resultAry[$ctr]['iManualFeeApplicable'] = 1; 
                                                    $resultAry[$ctr]['idManualFeeCurrency'] = $idManualFeeCurrency;
                                                    $resultAry[$ctr]['szManualFeeCurrency'] = $szManualFeeCurrency;
                                                    $resultAry[$ctr]['fManualFeeExchangeRates'] = $fManualFeeExchangeRates; 
                                                    $resultAry[$ctr]['fTotalManualFeeUSD'] = $fTotalManualFeeUSD;
                                                    $resultAry[$ctr]['fTotalManualFee'] = $fManualFee;
                                                }
                                                
                                                $fTotalMarkupPrice = 0.00;
                                                $fMarkupPriceUSD = 0.00 ;
                        
                                                $szCurrencyMarkupStr = "<br><strong>J. Currency Markup </strong>";
                                                if($iDistanceWHSFrom['szForwarderCurrency']==$data['idCurrency'])
                                                {
                                                    $resultAry[$ctr]['fTotalMarkupPrice'] = 0.00 ;
                                                    $szCurrencyMarkupStr .= "<br> Currency Mark-up : No ";
                                                }
                                                else
                                                { 
                                                    $fTotalDisplayPrice = $resultAry[$ctr]['fDisplayPrice'] ;
                                                    if($fGlobalPriceMarkUp>0)
                                                    {
                                                        $fPriceMarkUp = $fGlobalPriceMarkUp;
                                                    }
                                                    else
                                                    {
                                                        $fPriceMarkUp = $this->getManageMentVariableByDescription('__MARK_UP_PRICING_PERCENTAGE__');
                                                        $fGlobalPriceMarkUp = $fPriceMarkUp;
                                                    } 
                                                    $fMarkupPriceUSD = (($fPriceUSD* $fPriceMarkUp)/100); 
                                                    $fTotalMarkupPrice = ($fTotalDisplayPrice * $fPriceMarkUp * 0.01);
                                                    $fTotalDisplayPrice = $fTotalDisplayPrice + $fTotalMarkupPrice ;
                                                    
                                                    $resultAry[$ctr]['fTotalMarkupPrice'] = round((float)$fTotalMarkupPrice,6) ;
                                                    $resultAry[$ctr]['fMarkupPercentage'] = $fPriceMarkUp ;
                                                    $resultAry[$ctr]['fDisplayPrice'] = round((float)$fTotalDisplayPrice,6);
                        
                                                    $szCurrencyMarkupStr .= "<br> Currency Mark-up : Yes ";
                                                    $szCurrencyMarkupStr .= "<br> Mark-up percentage : ".$fPriceMarkUp."% ";
                                                    $szCurrencyMarkupStr .= "<br> Mark-up price : ".$data['szCurrency']." ".$resultAry[$ctr]['fTotalMarkupPrice']." ";
                                                }  
                                                $szVatString = "<br><strong>I. VAT </strong>";
                                                $fApplicableVatRate = 0; 
                                                $szVATRegistration='';
                                                if($iPrivateShipping==1)
                                                {
                                                    $szVatString = "<br> Customer Type : Private ";
                                                    $vatInputAry = array();
                                                    $vatInputAry['idCustomerCountry'] = $idCustomerAccountCountry;
                                                    $vatInputAry['idOriginCountry'] = $data['idOriginCountry'];
                                                    $vatInputAry['idDestinationCountry'] = $data['idDestinationCountry'];
                        
                                                    $kConfig_new1 = new cConfig();
                                                    $kConfig_new1->loadCountry($idCustomerAccountCountry);
                                                    $szVATRegistration = $kConfig_new1->szVATRegistration;

                                                    

                                                    $fApplicableVatRate = $kVatApplication->getTransportecaVat($vatInputAry);  
                                                    if($fApplicableVatRate>0)
                                                    {
                                                        $szVatString .= "<br> VAT Applicable: Yes ";
                                                        $szVatString .= "<br> VAT Percentage: ".$fApplicableVatRate;
                                                    }
                                                    else
                                                    {
                                                        $szVatString .= "<br> VAT Applicable: No ";
                                                    } 
                                                }
                                                else
                                                {
                                                    $szVatString = "<br> Customer Type : Business ";
                                                    $szVatString = "<br> VAT Applicable : No ";
                                                } 
                                                $fTotalVat = round((float)$resultAry[$ctr]['fDisplayPrice']) * $fApplicableVatRate * .01;
                                                $szVatString .= "<br> Total VAT: ".$data['szCurrency']." ".$fTotalVat;
                        
                                                $fTotalBookingPriceUsd = $fMarkupPriceUSD + $fPriceUSD ;
                                                $fTotalBookingPriceIncludingTruckingUsd = $fTotalBookingPriceUsd + $fTotalStandardTruckingRate;
                        
                                                $szTruckingRateLogString = "<br><strong>Trucking Rate </strong>";
                                                $szTruckingRateLogString .= "<br> Trucking from prices(USD): ".$fTruckingRateFrom;
                                                $szTruckingRateLogString .= "<br> Trucking to prices(USD): ".$fTruckingRateTo;
                                                $szTruckingRateLogString .= "<br> Total Trucking price(USD): ".$fTotalStandardTruckingRate;
                                                $szTruckingRateLogString .= "<br> Total Booking price including Trucking price(USD): ".$fTotalBookingPriceIncludingTruckingUsd;
                                                
                                                $fTotalBookingPriceIncludingTrucking = 0;
                                                if($data['fExchangeRate']>0)
                                                {
                                                    $fTotalBookingPriceIncludingTrucking = round((float)($fTotalBookingPriceIncludingTruckingUsd/$data['fExchangeRate']),2);
                                                } 
                                                $szTruckingRateLogString .= "<br> Total Booking price including Trucking price(".$data['szCurrency']."): ".$fTotalBookingPriceIncludingTrucking;
                                                $resultAry[$ctr]['fVATPercentage'] = $fApplicableVatRate;
                                                $resultAry[$ctr]['fTotalVat'] = $fTotalVat ; 
                                                $resultAry[$ctr]['fBookingPriceUSD'] = $fTotalBookingPriceUsd;
                                                $resultAry[$ctr]['fTotalHaulagePrice'] = $fTotalHaulagePrice ;
                                                $resultAry[$ctr]['fTotalHaulagePrice'] = $fTotalHaulagePrice ;
                                                $resultAry[$ctr]['szVATRegistration'] = $szVATRegistration;
                                                $resultAry[$ctr]['fTotalBookingPriceIncludingTrucking'] = $fTotalBookingPriceIncludingTrucking ;

                                                
                                                $resultAry[$ctr]['idForwarderCurrency'] = $iDistanceWHSFrom['szForwarderCurrency'] ;
                                                $resultAry[$ctr]['szForwarderCurrency'] = $iDistanceWHSFrom['szForwarderCurrencyName'];
                                                $resultAry[$ctr]['fForwarderCurrencyExchangeRate'] = $iDistanceWHSFrom['fForwarderCurrencyExchangeRate'] ;
                        
                                                if($iDistanceWHSFrom['szForwarderCurrency']==$data['idCurrency'])
                                                {
                                                    //This is customer currency price without XE mark-up
                                                    $fCustomerCurrencyPrice_XE =  $resultAry[$ctr]['fDisplayPrice'];
                                                    $fCustomerCurrencyPrice_XEForForwader =  $resultAry[$ctr]['fDisplayPrice'];
                                                }
                                                else
                                                {
                                                    //This is customer currency price without XE mark-up
                                                    $fCustomerCurrencyPrice_XE =  $resultAry[$ctr]['fDisplayPrice'];
                                                    $fCustomerCurrencyPrice_XEForForwader =  $fCustomerCurrencyPrice_XE/1.025;
                                                } 
                                                
                                                $szCalculatedPriceStr .= "<br>Customer Price XE: ".$fCustomerCurrencyPrice_XE; 
                                                $fCustomerCurrencyPrice_XE = round((float)$fCustomerCurrencyPrice_XE);
                                                $szCalculatedPriceStr .= "<br>Customer Invoice Rounded: ".$fCustomerCurrencyPrice_XE; 
                                                $szCalculatedPriceStr .= "<br>".$szVatString."<br>".$szCurrencyMarkupStr."<br>".$szTruckingRateLogString;
                                                
                                                $szCalculatedPriceStr .= "<br><br><strong> Forwarder Prices</strong>";
                                                $szCalculatedPriceStr .= "<br>Forwarder Currency: ".$resultAry[$ctr]['szForwarderCurrency'];
                                                $szCalculatedPriceStr .= "<br>Forwarder ROE: ".$resultAry[$ctr]['fForwarderCurrencyExchangeRate'];
                                                $fAdjustmentPriceForwarderCurrency =round((float)($fCustomerCurrencyPrice_XEForForwader-$fCustomerCurrencyPrice_XE),4);
                                                
                                                /*
                                                * converting price from customer currency to forwarder currency
                                                */
                                                if($iDistanceWHSFrom['szForwarderCurrency'] == $data['idCurrency'])
                                                {
                                                    /*
                                                    * If customer currency and forwarder currency is same then no need of any conversion
                                                    */
                                                    $resultAry[$ctr]['fForwarderCurrencyPrice'] = round((float)$resultAry[$ctr]['fDisplayPrice']);
                                                    $resultAry[$ctr]['fForwarderCurrencyPrice_XE'] = round((float)$fCustomerCurrencyPrice_XE);
                        
                                                    $fTotalVatForwarderCurrency = $fTotalVat;
                                                    
                                                    $szCalculatedPriceStr .= "<br> Forwarder Price: ".$resultAry[$ctr]['szForwarderCurrency']." ".$resultAry[$ctr]['fForwarderCurrencyPrice_XE'];
                                                    $szCalculatedPriceStr .= "<br> Forwarder Price including currency mark-up: ".$resultAry[$ctr]['szForwarderCurrency']." ".$resultAry[$ctr]['fForwarderCurrencyPrice'];
                                                }
                                                else
                                                {
                                                    /*
                                                    * converting price from customer currency to forwarder currency.
                                                    * to findout price i first convert customer price in USD then convert it into forwarder currency 
                                                    */
                                                    $tempCustomerUSDPrice = 0;
                                                    if($data['idCurrency']==1)
                                                    {
                                                        /*
                                                        * if customer price is in USD then no need of converstion
                                                        */
                                                        $tempCustomerUSDPrice = round((float)$resultAry[$ctr]['fDisplayPrice'],2);
                                                        $tempCustomerUSDPrice_XE = $fCustomerCurrencyPrice_XE ;
                                                        $fTotalVatUSD = $fTotalVat;
                                                    }
                                                    else
                                                    {
                                                        /*
                                                        * now converting customer price into USD
                                                        */												
                                                        $tempCustomerUSDPrice = $fCustomerCurrencyPrice_XEForForwader * $data['fExchangeRate'] ;
                                                        $tempCustomerUSDPrice_XE = $fCustomerCurrencyPrice_XEForForwader * $data['fExchangeRate'] ;
                                                        $fTotalVatUSD = $fTotalVat * $data['fExchangeRate']; 
                                                    }  
                                                    $szCalculatedPriceStr .= "<br> Forwarder Price (USD): ".$tempCustomerUSDPrice;
                                                    if($resultAry[$ctr]['idForwarderCurrency']==1)
                                                    {
                                                        /*
                                                        * if forwarder price is in USD then no need of converstion
                                                        */
                                                        $resultAry[$ctr]['fForwarderCurrencyPrice'] =round((float)$tempCustomerUSDPrice,4);
                                                        $resultAry[$ctr]['fForwarderCurrencyPrice_XE'] = round((float)$tempCustomerUSDPrice_XE,4); 
                        
                                                        $fTotalVatForwarderCurrency = $fTotalVatUSD;
                                                    }
                                                    else if($resultAry[$ctr]['fForwarderCurrencyExchangeRate']>0)
                                                    {
                                                        $tempCustomerUSDPrice = round((float)($tempCustomerUSDPrice/$resultAry[$ctr]['fForwarderCurrencyExchangeRate']),6);
                                                        $tempCustomerUSDPrice_XE = round((float)($tempCustomerUSDPrice_XE/$resultAry[$ctr]['fForwarderCurrencyExchangeRate']),6);
                        
                                                        $resultAry[$ctr]['fForwarderCurrencyPrice'] = round((float)$tempCustomerUSDPrice,4);
                                                        $resultAry[$ctr]['fForwarderCurrencyPrice_XE'] = round((float)$tempCustomerUSDPrice_XE,4);
                                                        
                                                        $fTotalVatForwarderCurrency = round((float)($fTotalVatUSD/$resultAry[$ctr]['fForwarderCurrencyExchangeRate']),4); 
                                                    }  
                                                    $szCalculatedPriceStr .= "<br> Forwarder Price: ".$resultAry[$ctr]['szForwarderCurrency']." ".$resultAry[$ctr]['fForwarderCurrencyPrice_XE'];
                                                    $szCalculatedPriceStr .= "<br> Forwarder Price including currency mark-up: ".$resultAry[$ctr]['szForwarderCurrency']." ".$resultAry[$ctr]['fForwarderCurrencyPrice'];
                                                } 
                                                
                                                /*
                                                * From Financial Version 2 bookings we are no longer adding VAT on forwarder prices
                                                */
                                                $fTotalVatForwarderCurrency = 0;
                                                if($resultAry[$ctr]['idForwarderCurrency']==1)
                                                { 
                                                    $fTotalSelfInvoiceAmount = round((float)$fTotalSelfInvoiceAmountUSD,4);
                                                }
                                                else if($resultAry[$ctr]['fForwarderCurrencyExchangeRate']>0)
                                                {
                                                    $fTotalSelfInvoiceAmount = round((float)($fTotalSelfInvoiceAmountUSD/$resultAry[$ctr]['fForwarderCurrencyExchangeRate']),4); 
                                                } 
                                                $szCalculatedPriceStr .= "<br> Self Invoiced amount (USD): ".$fTotalSelfInvoiceAmountUSD;
                                                $szCalculatedPriceStr .= "<br> Self Invoiced amount(Including Referral Fee) ".$resultAry[$ctr]['szForwarderCurrency']." ".$fTotalSelfInvoiceAmount;
                                                
                                                $resultAry[$ctr]['fAdjustmentPriceForwarderCurrency'] = round((float)$fAdjustmentPriceForwarderCurrency,4);
                                                //$fReferralPercentage = $iDistanceWHSFrom['fReferalFee'];                                                
                                                $fReferralPercentage = $fReferralFee;                                                
                                                if($fReferralPercentage>0)
                                                {
                                                    //$fReferralAmount = (($resultAry[$ctr]['fForwarderCurrencyPrice_XE'] + $fTotalVatForwarderCurrency) * $fReferralPercentage)/100 ;
                                                    $fReferralAmount = (($fTotalSelfInvoiceAmount * $fReferralPercentage)/100) ;
                        
                                                    $resultAry[$ctr]['fReferalAmount'] = $fReferralAmount ;
                                                    $resultAry[$ctr]['fReferalPercentage'] = $fReferralPercentage ;

                                                    $fForwarderExchangeRate = $resultAry[$ctr]['fForwarderCurrencyExchangeRate'] ;
                                                    
                                                    //Deducting Referral Fee amount from Self Invoice Amount
                                                    $fTotalSelfInvoiceAmount = $fTotalSelfInvoiceAmount - $fReferralAmount;

                                                    if($fForwarderExchangeRate>0)
                                                    {
                                                        $resultAry[$ctr]['fReferalAmountUSD'] = ($fReferralAmount * $fForwarderExchangeRate) ;
                                                    }
                                                    else
                                                    {
                                                        $resultAry[$ctr]['fReferalAmountUSD'] = 0 ;
                                                    }
                                                } 
                                                
                                                $szCalculatedPriceStr .="<br><br><strong>K. Referal Fee</strong>";
                                                $szCalculatedPriceStr .= "<br> Referal Percentage: ".$resultAry[$ctr]['fReferalPercentage'];
                                                $szCalculatedPriceStr .= "<br> Referal Amount: ".$resultAry[$ctr]['szForwarderCurrency']." ".$resultAry[$ctr]['fReferalAmount'];
                                                $szCalculatedPriceStr .= "<br> Referal Amount: USD ".$resultAry[$ctr]['fReferalAmountUSD'];
                                                $szCalculatedPriceStr .= "<br> Self Invoiced amounts ".$resultAry[$ctr]['szForwarderCurrency']." ".$fTotalSelfInvoiceAmount;
                                                
                                                $resultAry[$ctr]['fTotalSelfInvoiceAmount'] = $fTotalSelfInvoiceAmount;

                                                /**
                                                * Converting price from USD to Customer currency 
                                                */  									
                                                $resultAry[$ctr]['idCurrency'] =$data['idCurrency'];
                                                $resultAry[$ctr]['szCurrency'] =$data['szCurrency'];
                                                $resultAry[$ctr]['fUsdValue'] = $data['fExchangeRate'];
                                                
                                                if((float)$data['fExchangeRate']>0)
                                                {							
                                                   $resultAry[$ctr]['fTotalPriceCustomerCurrency'] =number_format((double)(($fTotalPrice / $data['fExchangeRate'])+$fTotalMarkupPrice),6,'.','') ;
                                                }
                                                else
                                                {
                                                    $resultAry[$ctr]['fTotalPriceCustomerCurrency'] =number_format((double)($fTotalPrice+$fTotalMarkupPrice),6,'.','');
                                                } 
                                                $szCustomerPriceLogs = "<strong><i>Calculation break-up</i></strong><br>".$szOriginChargesStr." <br>".$szPorttoPortStr. " <br>".$szDestinationChargesStr." <br>".$szExportCustomStr."<br>".$szImportCustomStr." <br>".$szOriginHaulageStr." <br> ".$szDestinationHaulageStr."<br>".$szPrivateCustomerFeeStr."<br>".$szHandlingFeeStr;
                                                
                                                $szCustomerPriceLogs .= "
                                                    <br><br> A. Origin Port Price <br>
                                                    B. Port to Port Price <br>
                                                    C. Destination Port Price <br>
                                                    D. Export Custom Clearance <br>
                                                    E. Import Custom Clearance <br>
                                                    F. Export Haulage Price <br> 
                                                    G. Import Haulage Price <br> 
                                                    H. Handling Fee <br>
                                                    I. Vat <br>
                                                    J. Currency Mark-up <br>
                                                    K. Referal Fee <br>
                                                ";
                                                $szCustomerPriceLogs .= $szCalculatedPriceStr;
                                                $szCustomerPriceLogs .= "<br><strong> Total price customer currency (excluding VAT): ".$data['szCurrency']." ".$resultAry[$ctr]['fTotalPriceCustomerCurrency']."</strong>";
                                                $resultAry[$ctr]['szCustomerPriceLogs'] = $szCustomerPriceLogs;
                                                /**
                                                * calculating total transportation time
                                                **/ 
                                                //to get transportation time in millisecond we need to convert transit day to millisecond and then add szUTCOffset
                                                $dtTransportTime_ms = ($fTotalTransitHours*60*60*1000) ;
                                                $iTransitDays_time_ms = ($fTotalTransitDays_Hours*60*60*1000) ; // converting actual day value to millisecond
                        
                                                $szTransitTimeLogsStr = "<br><br><strong>Calculating Cuttoff date</strong><br>";
                                                if(empty($szCustomerTimeZone))
                                                {
                                                    $kConfigNew = new cConfig();
                                                    $kConfigNew->loadCountry($idCustomerCountry);
                                                    $szCountryCode=$kConfigNew->szCountryISO;
                                                    $szCustomerTimeZone = getTimeZoneByCountryCode($szCountryCode);    
                                                }     
                                                if(!empty($szCustomerTimeZone))
                                                {
                                                    $date = new DateTime();  
                                                    $date->setTimezone(new DateTimeZone($szCustomerTimeZone));
                                                    $dtCurrentTimeAtOrigin = $date->format('Y-m-d H:i:s');  
                                                }  
                                                $szCountryCode = $iDistanceWHSFrom['szCountryISO'];
                                                $szWarehouseTimeZone = getTimeZoneByCountryCode($szCountryCode); 
                        
                                                $szTransitTimeLogsStr .= "<br>Warehouse Country: ".$szCountryCode;
                                                $szTransitTimeLogsStr .= "<br>Time Zone: ".$szWarehouseTimeZone;
                                                $szTransitTimeLogsStr .= "<br>Current time at origin country(".$szCountryCode."): ".$dtCurrentTimeAtOrigin;
                                                
                                                if($data['idTimingType']==1)
                                                {						
                                                    if($data['idServiceType']==__SERVICE_TYPE_DTD__ || $data['idServiceType']== __SERVICE_TYPE_DTW__ || $data['idServiceType']== __SERVICE_TYPE_DTP__) // 1.DTD 2.DTW
                                                    {
                                                        // in case of DTD and DTW substracting origin haulage from cutoff day for calculating expacted pick-up date
                                                        $WhsOffHours = $iBookingCutOffHours - ($fFromWareHouseHaulageAry['iHaulageTransitTime']*60*60);
                                                        $dtExpactedCuttoff = time() + ($WhsOffHours); 
                                                        $dtExpactedCuttoffAtOrigin = strtotime($dtCurrentTimeAtOrigin) + ($WhsOffHours);  
                                                    } 
                                                    else
                                                    {
                                                        $dtExpactedCuttoff = time() + ($iBookingCutOffHours);
                                                        $dtExpactedCuttoffAtOrigin = strtotime($dtCurrentTimeAtOrigin) + ($iBookingCutOffHours); 
                                                    } 
                                                    $szTransitTimeLogsStr .= "<br> Cut-off Hour: ".$row['iBookingCutOffHours'];
                                                    $szTransitTimeLogsStr .= "<br> Haulage Transit: ".$fFromWareHouseHaulageAry['iHaulageTransitTime'];
                                                    
                                                    $szTransitTimeLogsStr .= "<br>Current time + Cut-off: ".date('Y-m-d H:i',$dtExpactedCuttoffAtOrigin);
                                                    
                                                    $dtReadyAtOrigin = strtotime($date_str);

                                                    if($dtReadyAtOrigin>$dtExpactedCuttoff)
                                                    {
                                                        $total_date_str = $dtReadyAtOrigin;
                                                    }
                                                    else
                                                    {
                                                        $total_date_str = $dtExpactedCuttoff ;
                                                    }  
                                                    
                                                    $total_date_str = $total_date_str + ($fFromWareHouseHaulageAry['iHaulageTransitTime']*60*60); 
                        
                                                    if(date('l',$total_date_str)==$row['szWeekDay'])
                                                    {
                                                        $dtCutOffDateTime = date('Y-m-d',$total_date_str)." ".$row['szCutOffLocalTime'] ;										 		
                                                    }
                                                    else
                                                    {
                                                        $dtCutOffDateTime = date('Y-m-d',strtotime("next ".strtolower($row['szWeekDay']),$total_date_str.'+1DAY'))." ".$row['szCutOffLocalTime'] ;										 		
                                                    }   
                                                    $szTransitTimeLogsStr .= "<br>Calculated Cutoff Date: ".$dtCutOffDateTime; 
                        
                                                    if(!empty($szWarehouseTimeZone))
                                                    {  
                                                        $date = new DateTime($dtCutOffDateTime, new DateTimeZone($szWarehouseTimeZone));  
                                                        $dtWarehouseLocalTime = $date->format('Y-m-d H:i');
                                                        //$szTransitTimeLogsStr .= "<br>Warehouse Local Time: ".$dtWarehouseLocalTime; 
                        
                                                        //$dtCurrentTimeAtOriginTime = strtotime($dtExpactedCuttoffAtOrigin);
                                                        if($dtExpactedCuttoffAtOrigin>strtotime($dtCutOffDateTime))
                                                        {
                                                            /*
                                                            *  If current local time at origin country is greater then the calculated cut-off then we consider next week and cut-off
                                                            */
                                                            $dtCutOffDateTime = date('Y-m-d',strtotime("next ".strtolower($row['szWeekDay']),$dtExpactedCuttoffAtOrigin))." ".$row['szCutOffLocalTime'];
                                                            $szTransitTimeLogsStr .= "<br>current time at Origin country is greater then cut-off time so considering next weeks cut-off day ".$dtCutOffDateTime; 
                                                        }
                                                    } 
                                                    /*
                                                        // we are taking transit day -1 day 
                                                        if($row['iTransitHours']>=24)
                                                        {
                                                            $iTransitHour_1 = $row['iTransitHours']-24;
                                                        }
                                                        else
                                                        {
                                                            $iTransitHour_1 = 0;
                                                        } 
                                                    */
                                                    
                                                    $iTransitHour_1 = $row['iTransitHours'];

                                                   // calculating actual warehouse cutoff date there is no haulage included in it
                                                   $fTotalWTWTransitHour = strtotime($dtCutOffDateTime) + ($iTransitHour_1*60*60); 
                                                   $resultAry[$ctr]['dtWhsCutOff'] = date('d/m/Y H:i',strtotime($dtCutOffDateTime)); 
                                                   $resultAry[$ctr]['dtWhsCutOff_time'] = strtotime($dtCutOffDateTime);

                                                   $szTransitTimeLogsStr .= "<br>Warehouse cutoff Date(WHS Zone): ".$resultAry[$ctr]['dtWhsCutOff'];
                                                   // now calculating cutoff date which is shown on services page. 
                                                    if($data['idServiceType']==__SERVICE_TYPE_DTD__ || $data['idServiceType']== __SERVICE_TYPE_DTW__ || $data['idServiceType']== __SERVICE_TYPE_DTP__) // 1.DTD 2.DTW 3.DTP
                                                    {
                                                         // in case of DTD and DTW substracting origin haulage from cutoff day for calculating pick-up date
                                                         $iCutOffHours = strtotime($dtCutOffDateTime) - ($fFromWareHouseHaulageAry['iHaulageTransitTime']*60*60); 
                                                    }
                                                    else
                                                    {
                                                         $iCutOffHours = strtotime($dtCutOffDateTime); 
                                                    }

                                                    $szCollectionDay = date('l',$iCutOffHours);
                                                    if((strtolower($szCollectionDay)=='saturday' || strtolower($szCollectionDay)=='sunday') && $bDTDTradesFlag)
                                                    {
                                                        $dtCutOffDateTime = date('Y-m-d',strtotime("last friday",$total_date_str))." ".$row['szCutOffLocalTime'] ;
                                                        $iCutOffHours = strtotime($dtCutOffDateTime); 
                                                        $dtReadyAtOrigin = strtotime($date_str);
                                                        if($dtReadyAtOrigin>$iCutOffHours)
                                                        {
                                                            $dtCutOffDateTime = date('Y-m-d',strtotime("next friday",$total_date_str))." ".$row['szCutOffLocalTime'] ;
                                                            $iCutOffHours = strtotime($dtCutOffDateTime); 
                                                        }
                                                    } 
                                                    
                                                    $resultAry[$ctr]['dtCutOffDate'] = date('d/m/Y H:i',$iCutOffHours);
                                                    $resultAry[$ctr]['dtCuttOffDate_formated'] = date('Y-m-d',$iCutOffHours);

                                                    // calculating actual warehouse available date there is no haulage included in it
                                                    $dtAvailableDateTime = strtotime($dtCutOffDateTime) + ($iTransitHour_1*60*60); 
                                                    if(date('l',$dtAvailableDateTime)==$row['szAvailableDay'])
                                                    {
                                                         $resultAry[$ctr]['dtWhsAvailable'] = date('d/m/Y',$dtAvailableDateTime)." ".$row['szAvailableLocalTime'];
                                                         $resultAry[$ctr]['dtWhsAvailable_formated'] = date('Y-m-d',$dtAvailableDateTime)." ".$row['szAvailableLocalTime'];
                                                    }
                                                    else
                                                    {
                                                        //$dtCutOffDateTime = date('Y-m-d',strtotime("next ".strtolower($row['szWeekDay']),$total_date_str.'+1DAY'))." ".$row['szCutOffLocalTime'] ;
                                                        $nextAvailableDay = date('Y-m-d',strtotime("next ".strtolower($row['szAvailableDay']),$dtAvailableDateTime.'+1DAY'))." ".$row['szAvailableLocalTime'] ;
                                                        $dtAvailableDateTime = strtotime($nextAvailableDay);
                                                        $resultAry[$ctr]['dtWhsAvailable'] = date('d/m/Y',$dtAvailableDateTime)." ".$row['szAvailableLocalTime'];
                                                        $resultAry[$ctr]['dtWhsAvailable_formated'] = date('Y-m-d',$dtAvailableDateTime)." ".$row['szAvailableLocalTime'];
                                                    }
                                                   // now calculating available which is shown on services page. 
                                                   if($data['idServiceType']==__SERVICE_TYPE_DTD__ || $data['idServiceType']==__SERVICE_TYPE_WTD__ || $data['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD 3.WTD
                                                   {
                                                       // in case of DTD and WTD adding destination haulage into available day for calculating delivery date.
                                                       $dtAvailableTimes = strtotime($resultAry[$ctr]['dtWhsAvailable_formated']) + ($fToWareHouseHaulageAry['iHaulageTransitTime']*60*60); 
                                                   }
                                                   else
                                                   {
                                                       $dtAvailableTimes = strtotime($resultAry[$ctr]['dtWhsAvailable_formated']); 
                                                   }

                                                   /*
                                                    * when displaying services which end with D (e.g. PTD or DTD), if the delivery date is either Saturday or Sunday, then we show the following Monday instead. 
                                                    */
                                                    if($data['idServiceType']==__SERVICE_TYPE_DTD__ || $data['idServiceType']==__SERVICE_TYPE_WTD__ || $data['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD 3.WTD
                                                    {
                                                       $szDayName = strtolower(date('l',$dtAvailableTimes)) ; 
                                                       $weekendAry = array('saturday','sunday');

                                                       if(!empty($weekendAry) && in_array($szDayName, $weekendAry))
                                                       { 
                                                            $szNextMondayDate = date('Y-m-d H:i:s', strtotime('next Monday'." ".date('Y-m-d',$dtAvailableTimes))); 
                                                            $dtAvailableTimes = strtotime($szNextMondayDate);
                                                       }
                                                    }

                                                       //This is the available date time which is shown on services page.
                                                   $resultAry[$ctr]['dtAvailableDate']= date('d/m/Y H:i',$dtAvailableTimes) ;
                                                   $resultAry[$ctr]['dtAvailabeleDate_formated'] = date('Y-m-d',$dtAvailableTimes);

                                                    // we are taking cutoff date and Available date in time and php date format (Y-m-d H:i) so that we do not need any furthure calculation for searching
                                                   $resultAry[$ctr]['iCuttOffDate_time'] = strtotime($resultAry[$ctr]['dtCuttOffDate_formated']);
                                                   $resultAry[$ctr]['iAvailabeleDate_time'] = strtotime($resultAry[$ctr]['dtAvailabeleDate_formated']);
                                                   $resultAry[$ctr]['dtCuttOffDate_formated'] = convert_time_to_UTC_date($resultAry[$ctr]['iCuttOffDate_time']);
                                                   $resultAry[$ctr]['iAvailabeleDate_formated'] = convert_time_to_UTC_date($resultAry[$ctr]['iAvailabeleDate_time']); 
                                                }
                                                else if($data['idTimingType']==2)
                                                {
                                                   $total_date_str = strtotime($date_str) ;

                                                   if(date('l',$total_date_str)==$row['szAvailableDay'])
                                                   {
                                                       $dtAvailableDate = date('Y-m-d',$total_date_str)." ".$row['szAvailableLocalTime'] ;										 		
                                                   }
                                                   else
                                                   {
                                                       $dtAvailableDate = date('Y-m-d',strtotime("last ".$row['szAvailableDay'],$total_date_str.'+1DAY'))." ".$row['szAvailableLocalTime'] ;										 		
                                                   }

                                                   // calculating actual warehouse cutoff and available date there is no haulage included in it
                                                   //$fTotalWTWTransitHour = strtotime($dtAvailableDateTime) - ($row['iTransitHours']*60*60);
                                                   $resultAry[$ctr]['dtWhsAvailable'] = date('d/m/Y H:i',strtotime($dtAvailableDate));									 	
                                                   $resultAry[$ctr]['dtWhsAvailable_time'] = strtotime($dtAvailableDate); 
                                                   $resultAry[$ctr]['dtWhsAvailable_formated'] = date('Y-m-d H:i',strtotime($dtAvailableDate));

                                                 // now calculating available which is shown on services page. 
                                                    if($data['idServiceType']==__SERVICE_TYPE_DTD__ || $data['idServiceType']==__SERVICE_TYPE_WTD__ || $data['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD 3.WTD
                                                    {
                                                        // in case of DTD and WTD adding destination haulage into available day for calculating delivery date.
                                                        $dtAvailableDateTime = strtotime($dtAvailableDate) + ($fToWareHouseHaulageAry['iHaulageTransitTime']*60*60); 
                                                    }
                                                    else
                                                    {
                                                        $dtAvailableDateTime = strtotime($dtAvailableDate); 
                                                    }

                                                    /*
                                                    * when displaying services which end with D (e.g. PTD or DTD), if the delivery date is either Saturday or Sunday, then we show the following Monday instead. 
                                                    */
                                                    if($data['idServiceType']==__SERVICE_TYPE_DTD__ || $data['idServiceType']==__SERVICE_TYPE_WTD__ || $data['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD 3.WTD
                                                    {
                                                       $szDayName = strtolower(date('l',$dtAvailableDateTime)) ; 
                                                       $weekendAry = array('saturday','sunday');

                                                       if(!empty($weekendAry) && in_array($szDayName, $weekendAry))
                                                       { 
                                                            $szNextMondayDate = date('Y-m-d H:i:s', strtotime('next Monday'." ".date('Y-m-d',$dtAvailableDateTime))); 
                                                            $dtAvailableDateTime = strtotime($szNextMondayDate);
                                                       }
                                                    }

                                                    $resultAry[$ctr]['dtAvailableDate'] = date('d/m/Y H:i',$dtAvailableDateTime);
                                                    $resultAry[$ctr]['dtAvailabeleDate_formated'] = date('Y-m-d',$dtAvailableDateTime);

                                                    // we are taking transit day -1 day 
                                                    if($row['iTransitHours']>=24)
                                                    {
                                                        $iTransitHour_1 = $row['iTransitHours'] - 24 ;
                                                    }
                                                    else
                                                    {
                                                        $iTransitHour_1 = 0;
                                                    }

                                                    $dtCutOffDateTime = $resultAry[$ctr]['dtWhsAvailable_time'] - ($iTransitHour_1*60*60);

                                                    if(date('l',$dtCutOffDateTime) == $row['szWeekDay'])
                                                    {
                                                        $resultAry[$ctr]['dtWhsCutOff'] = date('d/m/Y',$dtCutOffDateTime)." ".$row['szCutOffLocalTime'] ;
                                                        $resultAry[$ctr]['dtWhsCutOff_format'] = date('Y-m-d',$dtCutOffDateTime)." ".$row['szCutOffLocalTime'] ;										 		
                                                    }
                                                    else
                                                    {
                                                        $resultAry[$ctr]['dtWhsCutOff'] = date('d/m/Y',strtotime("last ".strtolower($row['szWeekDay']),$dtCutOffDateTime.'+1DAY'))." ".$row['szCutOffLocalTime'] ;
                                                        $resultAry[$ctr]['dtWhsCutOff_format'] = date('Y-m-d',strtotime("last ".strtolower($row['szWeekDay']),$dtCutOffDateTime.'+1DAY'))." ".$row['szCutOffLocalTime'] ;									 		
                                                    } 
                                                     // now calculating cutoff date which is shown on services page. 
                                                    if($data['idServiceType']==__SERVICE_TYPE_DTD__ || $data['idServiceType']==__SERVICE_TYPE_DTW__ || $data['idServiceType']==__SERVICE_TYPE_DTP__) // 1.DTD 2.DTW
                                                    {
                                                        // in case of DTD and DTW substracting origin haulage from cutoff day for calculating pick-up date
                                                        $dtCutOffDateTime = strtotime($resultAry[$ctr]['dtWhsCutOff_format']) - ($fFromWareHouseHaulageAry['iHaulageTransitTime']*60*60); 
                                                    }
                                                    else
                                                    {
                                                        $dtCutOffDateTime = strtotime($resultAry[$ctr]['dtWhsCutOff_format']); 
                                                    }

                                                    $resultAry[$ctr]['dtCutOffDate'] = date('d/m/Y H:i',$dtCutOffDateTime);
                                                    $resultAry[$ctr]['dtCuttOffDate_formated'] = date('Y-m-d H:i',$dtCutOffDateTime);


                                                    // we are taking cutoff date and Available date in time and php date format (Y-m-d H:i) so that we do not need any furthure calculation for searching
                                                    $resultAry[$ctr]['iCuttOffDate_time'] = strtotime($resultAry[$ctr]['dtCuttOffDate_formated']);
                                                    $resultAry[$ctr]['iAvailabeleDate_time'] = strtotime($resultAry[$ctr]['dtAvailabeleDate_formated']);	

                                                    $resultAry[$ctr]['iAvailabeleDate_formated'] = convert_time_to_UTC_date($resultAry[$ctr]['iAvailabeleDate_time']); 
                                                }								

                                                // now convert $dtTransportTime_ms to day to get actual transportation time. 
                                                $dtTotalTransportDays =floor((double)($dtTransportTime_ms/(1000*60*60*24)));

                                                // at the following line we are finding reminder hours 
                                                $dtTotalTransportHours =($dtTransportTime_ms -($dtTotalTransportDays * (1000*60*60*24)))/(1000*60*60);

                                                $szTotalTransportationTime = ""; 
                                                $szTotalTransportationTime = (int)$dtTotalTransportDays." ".t($this->t_base.'fields/days');
                                                $szTotalTransportationTime .= "<br>".(int)$dtTotalTransportHours." ".t($this->t_base.'fields/hours');

                                                $szTransitTimeLogsStr .= "<br><br><strong>Transit Details</strong>";
                                                $szTransitTimeLogsStr .= "<br> Transit Time: ".$szTotalTransportationTime; 
                                                $szTransitTimeLogsStr .= "<br> Warehouse cut-off: ".$resultAry[$ctr]['dtWhsCutOff_format'];
                                                $szTransitTimeLogsStr .= "<br> Warehouse Available: ".$resultAry[$ctr]['dtWhsAvailable_formated']; 
                                                $szTransitTimeLogsStr .= "<br> Pick-up: ".$resultAry[$ctr]['dtCuttOffDate_formated'];
                                                $szTransitTimeLogsStr .= "<br> Delivery: ".$resultAry[$ctr]['dtAvailabeleDate_formated'];
                                                
                                                $this->szLCLCalculationLogs .= "<br>".$szCustomerPriceLogs."<br><br>".$szTransitTimeLogsStr;
                                                
                                                
                                                $resultAry[$ctr]['idTimingType'] = $data['idTimingType'] ;
                                                // taking total transportation time in hours for left slider at select services page .
                                                $resultAry[$ctr]['iTransitHour'] = ($dtTotalTransportDays*24)+$dtTotalTransportHours ;

                                                $resultAry[$ctr]['szTotalTransportationTime'] = $szTotalTransportationTime ;
                                                //$resultAry[$ctr]['iDistanceFromWHS'] = number_format((float)$iDistanceWHSFrom['iDistance']);  // distance from warehouse
                                                //$resultAry[$ctr]['iDistanceToWHS'] = number_format((float)$iDistanceWHSTo['iDistance']);      // distance to warehouse

                                                $resultAry[$ctr]['iDistanceFromWHS'] = round((float)$iDistanceWHSFrom['iDistance']);  // distance from warehouse
                                                $resultAry[$ctr]['iDistanceToWHS'] = round((float)$iDistanceWHSTo['iDistance']); 

                                                $resultAry[$ctr]['szFromWHSName'] = $iDistanceWHSFrom['szWareHouseName'];     //To WareHouse Name;
                                                $resultAry[$ctr]['szToWHSName'] = $iDistanceWHSTo['szWareHouseName'];        // To WareHouse Name

                                                $resultAry[$ctr]['szFromWHSContactPerson'] = $iDistanceWHSFrom['szContactPerson'];     //To WareHouse Name;
                                                $resultAry[$ctr]['szToWHSContactPerson'] = $iDistanceWHSTo['szContactPerson'];        // To WareHouse Name
                                                $resultAry[$ctr]['szFromWHSEmail'] = $iDistanceWHSFrom['szEmail'];     //To WareHouse Name;
                                                $resultAry[$ctr]['szToWHSEmail'] = $iDistanceWHSTo['szEmail'];        // To WareHouse Name

                                                $resultAry[$ctr]['szFromWHSPostCode'] = $iDistanceWHSFrom['szPostCode'];     //To WareHouse Name;
                                                $resultAry[$ctr]['szToWHSPostCode'] = $iDistanceWHSTo['szPostCode'];        // To WareHouse Name

                                                $resultAry[$ctr]['szFromCountry'] = $iDistanceWHSFrom['szCountry'];     //To WareHouse Name;
                                                $resultAry[$ctr]['szToCountry'] = $iDistanceWHSTo['szCountry']; 

                                                $resultAry[$ctr]['idForwardTo'] = $iDistanceWHSTo['idForwarder'];
                                                $resultAry[$ctr]['idForwardFrom'] = $iDistanceWHSFrom['idForwarder'];
                                                $resultAry[$ctr]['fPrice'] = round((float)$fTotalPrice,2);
                                                $resultAry[$ctr]['idForwarder'] = $iDistanceWHSFrom['idForwarder'] ;
                                                $resultAry[$ctr]['szDisplayName'] = $iDistanceWHSFrom['szDisplayName'] ;
                                                $resultAry[$ctr]['szForwarderAlias'] = $iDistanceWHSFrom['szForwarderAlias'] ;
                                                

                                                $resultAry[$ctr]['szFromWHSCity'] = $iDistanceWHSFrom['szCity'];     //To WareHouse Name;
                                                $resultAry[$ctr]['szToWHSCity'] = $iDistanceWHSTo['szCity'];        // To WareHouse Name
                                                $resultAry[$ctr]['szFromWHSCountryISO'] = $iDistanceWHSFrom['szCountryISO'];     //To WareHouse Name;
                                                $resultAry[$ctr]['szToWHSCountryISO'] = $iDistanceWHSTo['szCountryISO'];        // To WareHouse Name

                                                $resultAry[$ctr]['szFromWHSCountry'] = $iDistanceWHSFrom['szCountry'];     //To WareHouse Name;
                                                $resultAry[$ctr]['szToWHSCountry'] = $iDistanceWHSTo['szCountry'];        // To WareHouse Name

                                                $resultAry[$ctr]['idFromWHS'] = $iDistanceWHSFrom['id'];     //To WareHouse Name;
                                                $resultAry[$ctr]['idToWHS'] = $iDistanceWHSTo['id'];   
                                                $resultAry[$ctr]['iWarehouseType'] = $iWarehouseType;   
                                                

                                                $resultAry[$ctr]['szFromWHSLat'] = $iDistanceWHSFrom['szLatitude'];     //To WareHouse Name;
                                                $resultAry[$ctr]['szToWHSLat'] = $iDistanceWHSTo['szLatitude'];        // To WareHouse Name
                                                $resultAry[$ctr]['szFromWHSLong'] = $iDistanceWHSFrom['szLongitude'];     //To WareHouse Name;
                                                $resultAry[$ctr]['szToWHSLong'] = $iDistanceWHSTo['szLongitude'];
                                                $resultAry[$ctr]['szLogo'] = $iDistanceWHSFrom['szLogo'];
                                                $resultAry[$ctr]['dtValidFrom'] = $row['dtValidFrom'];     //To WareHouse Name;
                                                $resultAry[$ctr]['dtExpiry'] = $row['dtExpiry'];
                                                $resultAry[$ctr]['idServiceType'] = $data['idServiceType'];
                                                $resultAry[$ctr]['fCargoVolume'] = $data['fCargoVolume'];
                                                $resultAry[$ctr]['fCargoWeight'] = $data['fCargoWeight']; 
                                                $resultAry[$ctr]['szServiceTerm'] = $szServiceTerm;
                                                $resultAry[$ctr]['idServiceTerms'] = $idServiceTerms; 

                                                $resultAry[$ctr]['dtTransportTime_ms'] = $dtTransportTime_ms;
                                                $resultAry[$ctr]['iTransitDays_time_ms'] = $iTransitDays_time_ms;

                                                if($mode=='PARTNER_API_CALL')
                                                { 
                                                    $szPartnerAlies = cPartner::$szGlobalPartnerAlies;
                                                    $idShipmentType = cPartner::$idGlobalShipmentType;
                                                    $resultAry[$ctr]['idShipmentType'] = $idShipmentType;
                                                    $szUniqueKey = $data['szServiceTerm'];
                                                    if($iPrivateShipping==1)
                                                    {
                                                        $szUniqueKey .= "P"; //P. represents Private Customer
                                                    }
                                                    else
                                                    {
                                                        $szUniqueKey .= "B"; //B. represents Private Customer
                                                    }
                                                    $resultAry[$ctr]['unique_id'] = date("d")."".date("m")."T".$szPartnerAlies."".$szUniqueKey."".$row['idWTW'].mt_rand(10,99);
                                                } 
                                                else
                                                {
                                                    $resultAry[$ctr]['unique_id'] = $resultAry[$ctr]['iCuttOffDate_time'].'_'.$row['idWTW'].'_'.$resultAry[$ctr]['iAvailabeleDate_time'];
                                                }
                                                $ctr++; 
                                                $this->szLCLCalculationLogsDisplayedRows .= "Loop - ".$resultAry[$ctr]['szFromWHSName']." - ".$resultAry[$ctr]['szToWHSName']." - ".$resultAry[$ctr]['dtCuttOffDate_formated']." - ".$resultAry[$ctr]['dtAvailabeleDate_formated']." - ".$resultAry[$ctr]['fTotalPriceCustomerCurrency'];
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //$this->szLCLCalculationLogs .= " <br><span style='color: red; '>".$ctr_gl.". Not Found rates from CFS: ".$iDistanceWHSFrom['szWareHouseName']." to CFS: ".$iDistanceWHSTo['szWareHouseName']."</span>";  
                                    }
                                }
                                else
                                {
                                    $this->error = true;
                                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                                    return false;
                                }
                                $innerCtr++;
                            }
                        }
                        $outerCtr++;
                    }  
                    if(!empty($resultAry))
                    {
                        $final_ary=array();
                        $res_ary = array();

                        $iCutOffNDays =(int) $iGlobalCutOffNDays;
                        $iAvailableNDays =(int) $iGlobalAvailableNDays; 

                        $landing_page_time = strtotime(date('y-m-d',strtotime($date_str)));
                        $minus_14_days = strtotime ( '-'.$iAvailableNDays.' day' , $landing_page_time ) ;
                        $plus_14_days = strtotime ('+'.$iCutOffNDays.' day' , $landing_page_time ) ;

                        $finalResultAry = array();
                        foreach($resultAry as $resultArys)
                        {		
                            $dtValidFromDate = 	strtotime($resultArys['dtValidFrom']);
                            $dtExpiryFromDate = strtotime($resultArys['dtExpiry']);
                            if($data['idTimingType']==1) //Ready at origin
                            {
                                $landing_page_time = strtotime(date('y-m-d',strtotime($date_str)));
                                $landing_page_time =$landing_page_time;
                        
                                if(($resultArys['iCuttOffDate_time'] >= $landing_page_time) && ($resultArys['iCuttOffDate_time']>=$dtValidFromDate) && ($resultArys['iCuttOffDate_time']  < $dtExpiryFromDate))
                                {
                                    $tempResultAry = array();
                                    $days = ($plus_14_days - $resultArys['iCuttOffDate_time']) / (60 * 60 * 24);
                                    if(($resultArys['iFrequency'] < $days))
                                    {
                                        $finalResultAry[$countrr]=$resultArys ;
                                        $countrr++;
                        
                                        $dtCutOffDateTime = date('Y-m-d',strtotime("next ".strtolower($resultArys['szWeekDay']),$resultArys['dtWhsCutOff_time'].'+1DAY'))." ".$resultArys['szCutOffLocalTime'] ; 
                        
                                        $tempResultAry['dtWhsCutOff'] = date('d/m/Y H:i',strtotime($dtCutOffDateTime)); 
                                        $tempResultAry['dtWhsCutOff_time'] = strtotime($dtCutOffDateTime);

                                        // now calculating available which is shown on services page. 
                                        if($resultArys['idServiceType']==__SERVICE_TYPE_DTD__ || $resultArys['idServiceType']==__SERVICE_TYPE_DTW__ || $data['idServiceType']==__SERVICE_TYPE_DTP__) // 1.DTD 2.DTD
                                        {
                                            // in case of DTD and DTD adding origin haulage into cutoff day for calculating pick-up date.
                                            $iCutOffHours = strtotime($dtCutOffDateTime) - ($resultArys['fOriginHaulageTransitTime']*60*60); 
                                        }
                                        else
                                        {
                                            $iCutOffHours = strtotime($dtCutOffDateTime);
                                        }	
                                        
                                        $szCollectionDay = strtolower(date('l',$iCutOffHours));
                                        if(($szCollectionDay=='saturday' || $szCollectionDay=='sunday') && $bDTDTradesFlag)
                                        {
                                            $dtCutOffDateTime = date('Y-m-d',strtotime("last friday",$iCutOffHours))." ".$resultArys['szCutOffLocalTime'] ;								 		
                                            $iCutOffHours = strtotime($dtCutOffDateTime);
                                        }
                                        $tempResultAry['dtCutOffDate'] = date('d/m/Y H:i',$iCutOffHours);
                                        $tempResultAry['dtCuttOffDate_formated'] = date('Y-m-d',$iCutOffHours);

                                        // finding Available at time 
                                        //$dtAvailableDateTime = $iCutOffHours + ($resultArys['iTransitDays_time_ms']/1000);

                                        // calculating actual warehouse available date there is no haulage included in it
                                        // we are taking transit day -1 day 
                                        if($resultArys['iTransitHours']>=24)
                                        {
                                            $iTransitHour_1 = $resultArys['iTransitHours'] - 24 ;
                                        }
                                        else
                                        {
                                            $iTransitHour_1 = 0;
                                        }
                                        $dtAvailableDateTime = strtotime($dtCutOffDateTime) + ($iTransitHour_1*60*60);

                                        if(date('l',$dtAvailableDateTime) == $resultArys['szAvailableDay'])
                                        {
                                            $tempResultAry['dtWhsAvailable'] = date('d/m/Y',$dtAvailableDateTime)." ".$resultArys['szAvailableLocalTime'];
                                            $tempResultAry['dtWhsAvailable_formated'] = date('Y-m-d',$dtAvailableDateTime)." ".$resultArys['szAvailableLocalTime'];
                                        }
                                        else
                                        {
                                            $nextAvailableDay = date('Y-m-d',strtotime("next ".strtolower($resultArys['szAvailableDay']),$dtAvailableDateTime))." ".$resultArys['szAvailableLocalTime'] ;
                                            $dtAvailableDateTime = strtotime($nextAvailableDay);

                                            $tempResultAry['dtWhsAvailable'] = date('d/m/Y',$dtAvailableDateTime)." ".$resultArys['szAvailableLocalTime'];
                                            $tempResultAry['dtWhsAvailable_formated'] = date('Y-m-d',$dtAvailableDateTime)." ".$resultArys['szAvailableLocalTime'];
                                        }
                                        // now calculating available which is shown on services page. 
                                        if($resultArys['idServiceType']==__SERVICE_TYPE_DTD__ || $resultArys['idServiceType']==__SERVICE_TYPE_WTD__ || $resultArys['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD 3.WTD
                                        {
                                            // in case of DTD and WTD adding destination haulage into available day for calculating delivery date.
                                            $dtAvailableTimes = strtotime($tempResultAry['dtWhsAvailable_formated']) + ($resultArys['fDestinationHaulageTransitTime']*60*60); 
                                        }
                                        else
                                        {
                                            $dtAvailableTimes = strtotime($tempResultAry['dtWhsAvailable_formated']); 
                                        } 	 	
                                        /*
                                        * when displaying services which end with D (e.g. PTD or DTD), if the delivery date is either Saturday or Sunday, then we show the following Monday instead. 
                                        */
                                        if($resultArys['idServiceType']==__SERVICE_TYPE_DTD__ || $resultArys['idServiceType']==__SERVICE_TYPE_WTD__ || $resultArys['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD 3.WTD
                                        {
                                           $szDayName = strtolower(date('l',$dtAvailableTimes)) ; 
                                           $weekendAry = array('saturday','sunday');

                                           if(!empty($weekendAry) && in_array($szDayName, $weekendAry))
                                           { 
                                                $szNextMondayDate = date('Y-m-d H:i:s', strtotime('next Monday'." ".date('Y-m-d',$dtAvailableTimes))); 
                                                $dtAvailableTimes = strtotime($szNextMondayDate);
                                           }
                                        }

                                        $tempResultAry['dtAvailableDate'] = date('d/m/Y H:i',$dtAvailableTimes);
                                        $tempResultAry['dtAvailabeleDate_formated'] = date('Y-m-d',$dtAvailableTimes);


                                        // we are taking cutoff date and Available date in time and php date format (Y-m-d H:i) so that we do not need any furthure calculation for searching
                                        $tempResultAry['iCuttOffDate_time'] = strtotime($tempResultAry['dtCuttOffDate_formated']);
                                        $tempResultAry['iAvailabeleDate_time'] = strtotime($tempResultAry['dtAvailabeleDate_formated']);
                                        $tempResultAry['dtCuttOffDate_formated'] = convert_time_to_UTC_date($tempResultAry['iCuttOffDate_time']);
                                        $tempResultAry['iAvailabeleDate_formated'] = convert_time_to_UTC_date($tempResultAry['iAvailabeleDate_time']);


                                        if(($tempResultAry['iCuttOffDate_time'] >= $landing_page_time) && ($tempResultAry['iCuttOffDate_time']>=$dtValidFromDate) && ($tempResultAry['iCuttOffDate_time']  < $dtExpiryFromDate))
                                        {	
                                            $finalResultAry[$countrr]=$resultArys ;

                                            $finalResultAry[$countrr]['dtWhsCutOff'] = $tempResultAry['dtWhsCutOff'];
                                            $finalResultAry[$countrr]['dtWhsCutOff_time'] = $tempResultAry['dtWhsCutOff_time'];
                                            $finalResultAry[$countrr]['dtWhsAvailable'] = $tempResultAry['dtWhsAvailable'];
                                            $finalResultAry[$countrr]['dtWhsAvailable_formated'] = $tempResultAry['dtAvailabeleDate_formated'];


                                            $finalResultAry[$countrr]['dtAvailableDate'] = $tempResultAry['dtAvailableDate'];
                                            $finalResultAry[$countrr]['dtAvailabeleDate_formated'] = $tempResultAry['dtAvailabeleDate_formated'];
                                            $finalResultAry[$countrr]['dtCutOffDate'] = $tempResultAry['dtCutOffDate'];
                                            $finalResultAry[$countrr]['dtCuttOffDate_formated'] = $tempResultAry['dtCuttOffDate_formated'];
                                            $finalResultAry[$countrr]['iCuttOffDate_time'] = $tempResultAry['iCuttOffDate_time'];
                                            $finalResultAry[$countrr]['iAvailabeleDate_time'] = $tempResultAry['iAvailabeleDate_time'];
                                            $finalResultAry[$countrr]['iAvailabeleDate_formated'] = $tempResultAry['iAvailabeleDate_formated'];	
                                            
                                            if($mode=='PARTNER_API_CALL')
                                            { 
                                                $szPartnerAlies = cPartner::$szGlobalPartnerAlies; 
                                                $idShipmentType = cPartner::$idGlobalShipmentType;
                                                $finalResultAry[$countrr]['idShipmentType'] = $idShipmentType;
                                                $szUniqueKey = $data['szServiceTerm'];
                                                if($iPrivateShipping==1)
                                                {
                                                    $szUniqueKey .= "P"; //P. represents Private Customer
                                                }
                                                else
                                                {
                                                    $szUniqueKey .= "B"; //B. represents Private Customer
                                                }
                                                $finalResultAry[$countrr]['unique_id'] = date("d")."".date("m")."T".$szPartnerAlies."".$szUniqueKey."".$resultArys['idWTW'].mt_rand(10,99);
                                            } 
                                            else
                                            {
                                                $finalResultAry[$countrr]['unique_id'] = $tempResultAry['iCuttOffDate_time'].'_'.$resultArys['idWTW'].'_'.$tempResultAry['iAvailabeleDate_time'];
                                            }
                                            $countrr++;
                                            $this->szLCLCalculationLogsDisplayedRows .= "Ready at Origin - ".$finalResultAry[$countrr]['szFromWHSName']." - ".$finalResultAry[$countrr]['szToWHSName']." - ".$finalResultAry[$countrr]['dtCuttOffDate_formated']." - ".$finalResultAry[$countrr]['dtAvailabeleDate_formated']." - ".$finalResultAry[$countrr]['fTotalPriceCustomerCurrency'];
                                        }
                                    }
                                    else
                                    {
                                        $finalResultAry[$countrr]=$resultArys ;
                                        $countrr++;
                                    }
                                }
                            }
                            else
                            {
                                // Available date should in between the date selected at landing page and and date before its 14 days 
                                if(($resultArys['iAvailabeleDate_time'] <= $landing_page_time) && ($resultArys['iAvailabeleDate_time'] >= $minus_14_days) && ($resultArys['iCuttOffDate_time']>=time()) && ($resultArys['iCuttOffDate_time']>=$dtValidFromDate) && ($resultArys['iCuttOffDate_time']  <= $dtExpiryFromDate))
                                {		
                                    $days = ($resultArys['iAvailabeleDate_time'] - $minus_14_days) / (60 * 60 * 24); 
                                    if(($resultArys['iFrequency'] < $days))
                                    {				
                                        $finalResultAry[$countrr]=$resultArys ;
                                        $countrr++;

                                        $tempResultAry = array();

                                        $dtAvailableDate = date('Y-m-d',strtotime("last ".$resultArys['szAvailableDay'],$resultArys['iAvailabeleDate_time'].'+1DAY'))." ".$resultArys['szAvailableLocalTime'] ;

                                        // calculating actual warehouse cutoff and available date there is no haulage included in it
                                        //$fTotalWTWTransitHour = strtotime($dtAvailableDateTime) - ($row['iTransitHours']*60*60);
                                        $tempResultAry['dtWhsAvailable'] = date('d/m/Y H:i',strtotime($dtAvailableDate));										 	
                                        $tempResultAry['dtWhsAvailable_time'] = strtotime($dtAvailableDate);

                                        // now calculating available which is shown on services page. 
                                        if($resultArys['idServiceType']==__SERVICE_TYPE_DTD__ || $resultArys['idServiceType']==__SERVICE_TYPE_WTD__ || $resultArys['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD 3.WTD
                                        {
                                            // in case of DTD and WTD adding destination haulage into available day for calculating delivery date.
                                            $dtAvailableDateTime = strtotime($dtAvailableDate) + ($resultArys['fDestinationHaulageTransitTime']*60*60); 
                                        }
                                        else
                                        {
                                            $dtAvailableDateTime = strtotime($dtAvailableDate); 
                                        }

                                        /*
                                        * when displaying services which end with D (e.g. PTD or DTD), if the delivery date is either Saturday or Sunday, then we show the following Monday instead. 
                                        */
                                        if($resultArys['idServiceType']==__SERVICE_TYPE_DTD__ || $resultArys['idServiceType']==__SERVICE_TYPE_WTD__ || $resultArys['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD 3.WTD
                                        {
                                           $szDayName = strtolower(date('l',$dtAvailableDateTime)) ; 
                                           $weekendAry = array('saturday','sunday');

                                           if(!empty($weekendAry) && in_array($szDayName, $weekendAry))
                                           { 
                                                $szNextMondayDate = date('Y-m-d H:i:s', strtotime('next Monday'." ".date('Y-m-d',$dtAvailableDateTime))); 
                                                $dtAvailableDateTime = strtotime($szNextMondayDate);
                                           }
                                        }

                                        $tempResultAry['dtAvailableDate'] = date('d/m/Y H:i',$dtAvailableDateTime);
                                        $tempResultAry['dtAvailabeleDate_formated'] = date('Y-m-d',$dtAvailableDateTime);

                                        if($resultArys['iTransitHours']>=24)
                                        {
                                            $iTransitHour_1 = $resultArys['iTransitHours'] - 24 ;
                                        }
                                        else
                                        {
                                            $iTransitHour_1 = 0;
                                        }

                                        $dtCutOffDateTime = $tempResultAry['dtWhsAvailable_time'] - ($iTransitHour_1*60*60);
                                        if(date('l',$dtCutOffDateTime) == $resultArys['szWeekDay'])
                                        {
                                            $tempResultAry['dtWhsCutOff'] = date('d/m/Y',$dtCutOffDateTime)." ".$resultArys['szCutOffLocalTime'] ;
                                            $tempResultAry['dtWhsCutOff_format'] = date('Y-m-d',$dtCutOffDateTime)." ".$resultArys['szCutOffLocalTime'] ;										 		
                                        }
                                        else
                                        {
                                            $tempResultAry['dtWhsCutOff'] = date('d/m/Y',strtotime("last ".strtolower($resultArys['szWeekDay']),$dtCutOffDateTime.'+1DAY'))." ".$resultArys['szCutOffLocalTime'] ;
                                            $tempResultAry['dtWhsCutOff_format'] = date('Y-m-d',strtotime("last ".strtolower($resultArys['szWeekDay']),$dtCutOffDateTime.'+1DAY'))." ".$resultArys['szCutOffLocalTime'] ;									 		
                                        }

                                        // now calculating cutoff date which is shown on services page. 
                                        if($resultArys['idServiceType']==__SERVICE_TYPE_DTD__ || $resultArys['idServiceType']==__SERVICE_TYPE_DTW__) // 1.DTD 2.DTW
                                        {
                                            // in case of DTD and DTW substracting origin haulage from cutoff day for calculating pick-up date
                                            $dtCutOffDateTime = strtotime($tempResultAry['dtWhsCutOff_format']) - ($resultArys['fOriginHaulageTransitTime']*60*60); 
                                        }
                                        else
                                        {
                                            $dtCutOffDateTime = strtotime($tempResultAry['dtWhsCutOff_format']); 
                                        }

                                        $tempResultAry['dtCutOffDate'] = date('d/m/Y H:i',$dtCutOffDateTime);
                                        $tempResultAry['dtCuttOffDate_formated'] = date('Y-m-d H:i',$dtCutOffDateTime);


                                        // we are taking cutoff date and Available date in time and php date format (Y-m-d H:i) so that we do not need any furthure calculation for searching
                                        $tempResultAry['iCuttOffDate_time'] = strtotime($tempResultAry['dtCuttOffDate_formated']);
                                        $tempResultAry['iAvailabeleDate_time'] = strtotime($tempResultAry['dtAvailabeleDate_formated']);	

                                        $tempResultAry['iAvailabeleDate_formated'] = convert_time_to_UTC_date($tempResultAry['iAvailabeleDate_time']);

                                        if(($tempResultAry['iAvailabeleDate_time'] <= $landing_page_time) && ($tempResultAry['iAvailabeleDate_time'] >= $minus_14_days) && ($tempResultAry['iCuttOffDate_time']>time()) && ($tempResultAry['iCuttOffDate_time']>=$dtValidFromDate) && ($tempResultAry['iCuttOffDate_time']  < $dtExpiryFromDate))
                                        {	
                                            $finalResultAry[$countrr]=$resultArys ;

                                            $finalResultAry[$countrr]['dtWhsCutOff'] = $tempResultAry['dtWhsCutOff'];
                                            $finalResultAry[$countrr]['dtWhsCutOff_time'] = $tempResultAry['dtWhsCutOff_format'];
                                            $finalResultAry[$countrr]['dtWhsAvailable'] = $tempResultAry['dtWhsAvailable'];
                                            $finalResultAry[$countrr]['dtWhsAvailable_formated'] = $tempResultAry['dtWhsAvailable_time'];

                                            $finalResultAry[$countrr]['dtAvailableDate'] = $tempResultAry['dtAvailableDate'];
                                            $finalResultAry[$countrr]['dtAvailabeleDate_formated'] = $tempResultAry['dtAvailabeleDate_formated'];
                                            $finalResultAry[$countrr]['dtCutOffDate'] = $tempResultAry['dtCutOffDate'];
                                            $finalResultAry[$countrr]['dtCuttOffDate_formated'] = $tempResultAry['dtCuttOffDate_formated'];
                                            $finalResultAry[$countrr]['iCuttOffDate_time'] = $tempResultAry['iCuttOffDate_time'];
                                            $finalResultAry[$countrr]['iAvailabeleDate_time'] = $tempResultAry['iAvailabeleDate_time'];
                                            $finalResultAry[$countrr]['iAvailabeleDate_formated'] = $tempResultAry['iAvailabeleDate_formated'];
                                            if($mode=='PARTNER_API_CALL')
                                            { 
                                                $szPartnerAlies = cPartner::$szGlobalPartnerAlies;
                                                $idShipmentType = cPartner::$idGlobalShipmentType;
                                                $finalResultAry[$countrr]['idShipmentType'] = $idShipmentType;
                                                $szUniqueKey = $data['szServiceTerm'];
                                                if($iPrivateShipping==1)
                                                {
                                                    $szUniqueKey .= "P"; //P. represents Private Customer
                                                }
                                                else
                                                {
                                                    $szUniqueKey .= "B"; //B. represents Private Customer
                                                }
                                                $finalResultAry[$countrr]['unique_id'] = date("d")."".date("m")."T".$szPartnerAlies."".$szUniqueKey."".$resultArys['idWTW'].mt_rand(10,99);
                                            } 
                                            else
                                            {
                                                $finalResultAry[$countrr]['unique_id'] = $tempResultAry['iCuttOffDate_time'].'_'.$resultArys['idWTW'].'_'.$tempResultAry['iAvailabeleDate_time'];
                                            }
                                            $countrr++;
                                            //print_r($finalResultAry);
                                            $this->szLCLCalculationLogsDisplayedRows .= "Available at Destination ".$finalResultAry[$countrr]['szFromWHSName']." - ".$finalResultAry[$countrr]['szToWHSName']." - ".$finalResultAry[$countrr]['dtCuttOffDate_formated']." - ".$finalResultAry[$countrr]['dtAvailabeleDate_formated']." - ".$finalResultAry[$countrr]['fTotalPriceCustomerCurrency'];
                                        }
                                    }
                                    else
                                    {
                                        $finalResultAry[$countrr]=$resultArys ;
                                        $countrr++;
                                    }
                                }	
                            } //END of else
                        } // END of foreach($resultAry as $resultArys)

                        $final_ary = sortArray($finalResultAry,'fTotalPriceCustomerCurrency');  // sorting array ASC to fTotalPriceCustomerCurrency   
                        $iSearchResultLimit =(int)$iGlobalSearchResultLimit;   
                        /*
                         * we are showing max 5 record of a forwarder.
                         * so suppose forwarder X have 15 relevant result but we only show 5 of them. 
                         * 
                         *   $final_sliced_ary = $this->sliceArrayByForwarder($final_ary);
                         *   $res_ary = $final_sliced_ary; 
                         *   $final_ary_to_return = $res_ary ; 
                         */
                           
                        $res_ary = $final_ary; 
                        $final_ary_to_return = $res_ary ; 

                        if(!empty($res_ary))
                        {
                            $counter=0;
                            $forwarderRatingAry = array();
                            $ratingReviewAry=array();
                            $forwarderReviewsAry=array();
                            foreach($final_ary_to_return as $res_arys)
                            {							
                                if(!empty($this->forwarderAry) && array_key_exists($res_arys['idForwarder'],$this->forwarderAry))
                                {
                                    $final_ary_to_return[$counter]['fAvgRating'] = $forwarderRatingAry[$res_arys['idForwarder']]['fAvgRating'];
                                    $final_ary_to_return[$counter]['iNumRating'] = $forwarderRatingAry[$res_arys['idForwarder']]['iNumRating'];
                                    $final_ary_to_return[$counter]['iNumReview'] = $forwarderReviewsAry[$res_arys['idForwarder']]['iNumReview']; 
                                    $ifCtr++;
                                }
                                else
                                {
                                    $forwarderRatingAry[$res_arys['idForwarder']] = $this->getForwarderAvgRating($res_arys['idForwarder']);
                                    $forwarderReviewsAry[$res_arys['idForwarder']] = $this->getForwarderAvgReviews($res_arys['idForwarder']);

                                    $final_ary_to_return[$counter]['fAvgRating'] = $forwarderRatingAry[$res_arys['idForwarder']]['fAvgRating'];
                                    $final_ary_to_return[$counter]['iNumRating'] = $forwarderRatingAry[$res_arys['idForwarder']]['iNumRating'];
                                    $final_ary_to_return[$counter]['iNumReview'] = $forwarderReviewsAry[$res_arys['idForwarder']]['iNumReview']; 
                                    $elseCtr++;
                                }
                                $this->forwarderAry[$res_arys['idForwarder']] = $res_arys['szDisplayName'] ;								
                                $counter++;							
                            }
                        } 
                        
                        $final_resulting_arr=array();
                        $tempAry = array(); 
                        if(!empty($final_ary_to_return))
                        {
                            $this->szLCLCalculationLogs .= "<br><br><strong><u>Final Response: </u> </strong>";
                            foreach ($final_ary_to_return as $final_ary_to_returns) 
                            { 
                                $start=date("Y-m-d",strtotime(str_replace("/","-",trim($final_ary_to_returns['dtCutOffDate']))));
                                $end=date("Y-m-d",strtotime(str_replace("/","-",trim($final_ary_to_returns['dtAvailableDate']))));
                                $days_between = ceil(abs(strtotime($end) - strtotime($start)) / 86400);

                                $final_ary_to_returns['iDaysBetween'] = $days_between ;

                                if($final_ary_to_returns['iRailTransport']==1)
                                {
                                    $szTransportMode = 'Rail';
                                }
                                else
                                {
                                    $szTransportMode = 'LCL';
                                }
                                $unique_id = $final_ary_to_returns['unique_id'];						
                                if (!empty($tempAry) && !in_array($unique_id,$tempAry) && $final_ary_to_returns['fDisplayPrice']>0)
                                { 
                                    array_push($final_resulting_arr,$final_ary_to_returns);
                                    $this->szLCLCalculationLogs .= "<br> <strong> ".$szTransportMode." - ".$final_ary_to_returns['szDisplayName']." - ".$final_ary_to_returns['szFromWHSName']." - ".$final_ary_to_returns['szToWHSName']." - ".$days_between." days - ".$final_ary_to_returns['szCurrency']." ".number_format((float)$final_ary_to_returns['fDisplayPrice'])." - Cut-off: ".date('Y-m-d',strtotime($final_ary_to_returns['dtCuttOffDate_formated']))." - Available: ".$final_ary_to_returns['dtAvailabeleDate_formated']."</strong>";
                                    $tempAry[]=$unique_id ; 
                                }
                                else if(empty($tempAry) && $final_ary_to_returns['fDisplayPrice']>0)
                                {
                                    array_push($final_resulting_arr,$final_ary_to_returns); 
                                    $this->szLCLCalculationLogs .= "<br> <strong> ".$szTransportMode." - ".$final_ary_to_returns['szDisplayName']." - ".$final_ary_to_returns['szFromWHSName']." - ".$final_ary_to_returns['szToWHSName']." - ".$days_between." days - ".$final_ary_to_returns['szCurrency']." ".number_format((float)$final_ary_to_returns['fDisplayPrice'])." - Cut-off: ".date('Y-m-d',strtotime($final_ary_to_returns['dtCuttOffDate_formated']))." - Available: ".$final_ary_to_returns['dtAvailabeleDate_formated']."</strong>";
                                    $tempAry[]=$unique_id ; 
                                }
                            }
                        }        
                    } // END of if(!empty($resultAry))
                } // END of if(!empty($wareHouseToAry) && !empty($wareHouseFromAry))
                        
                if($mode!='FORWARDER_TRY_IT_OUT' && $mode!='MANAGEMENT_TRY_IT_OUT')
                {
                   
                    /*
                     * In case when calculation is performed rom management /try/ or forwarder /try/ then we don't call courier API
                     */
                    $postSearchAry = $data;
                    //Calculation of courier producst is started
                    $idCustomerCurrencye = $postSearchAry['idCustomerCurrency'] ;
                    $courierServiceNewArr = array();
                    
                    if($bSearchOnlyLCL)
                    {
                        /*
                         * If this flag is set to true then we only search for LCL service
                         */
                        $this->szCourierCalculationLogString = $this->szLCLCalculationLogs;
                    }
                    else
                    {
                        $bSearchCourierServices = true;
                        
                        /*
                        * if max(the total volume of the search * 250 ; weight) > __MAXIMUM_CHARGEABLE_WEIGHT_FOR_AIRFREIGHT__, then we do not request courier services for the search. 
                        * Example: If use makes a search of 500kg, 1cbm from USA to Denmark. This trade is not in /modeTransport/ screen, and 1cbm * 250 = 250. Maximum (500 ; 250) = 500. 500 > 200 => so we do not search for courier services.
                        */
                        $kCourierServices = new cCourierServices(); 
                        $modeTransport = false;
                        if($kCourierServices->checkFromCountryToCountryExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry']))
                        {
                            $modeTransport = true;
                            $this->szLCLCalculationLogs .= "<br><br> Road Transport: Yes";
                        }  
                        else
                        {
                            $this->szLCLCalculationLogs .= "<br><br>Road Transport: No";
                        }
                        if(!$modeTransport)
                        {
                            $fMaxChargeableWeightAirFreight = $this->getManageMentVariableByDescription('__MAXIMUM_CHARGEABLE_WEIGHT_FOR_AIRFREIGHT__');
                            $fShipmentVolume = $postSearchAry['fCargoVolume'] * 250;
                            $fShipmentWeight = $postSearchAry['fCargoWeight'];
                            
                            if($fShipmentVolume>$fShipmentWeight)
                            {
                                $fShipmentChargebleWeight = $fShipmentVolume;
                            }
                            else
                            {
                                $fShipmentChargebleWeight = $fShipmentWeight;
                            } 
                            $this->szLCLCalculationLogs .= "<br>Max chargeable weight air freight: ".$fMaxChargeableWeightAirFreight;
                            $this->szLCLCalculationLogs .= "<br>Chargeable weight for Shipment: MAX(".$postSearchAry['fCargoVolume']."*250, ".$postSearchAry['fCargoWeight'].") = ".$fShipmentChargebleWeight;
                            if($fShipmentChargebleWeight>$fMaxChargeableWeightAirFreight)
                            {
                                $bSearchCourierServices = false;
                                $this->szLCLCalculationLogs .= "<br>Search courier services: No";
                            }
                            else
                            {
                                $this->szLCLCalculationLogs .= "<br>Search courier services: Yes";
                            }
                        }
                       
                        if($postSearchAry['iShowCourierPriceFlag']==1)
                        {
                            $bSearchCourierServices =false;
                        }
                        //$bSearchCourierServices = false;
                        if($bSearchCourierServices)
                        { 
                            $postSearchAry['iPrivateShipping'] = $iPrivateShipping; 
                            $postSearchAry['idCustomerAccountCountry'] = $idCustomerAccountCountry;
                            $courierServiceNewArr = $this->getCourierProviderData($postSearchAry,$idCustomerCurrencye,false,$mode);
                            $this->szCalculationLogString = $this->szLCLCalculationLogs."<br><br>"."<br><br>".$this->szCalculationLogString;
                            $this->szCourierCalculationLogString = $this->szCalculationLogString; 
                        } 
                        else
                        {
                            $this->szCourierCalculationLogString = $this->szLCLCalculationLogs;
                            $this->szCalculationLogString = $this->szLCLCalculationLogs;
                        }
                    } 
                    if($iDebugFlag==1)
                    { 
                        $strdata=array();
                        $strdata[0]=" \n\n *** Courier API Logs \n\n";
                        $strdata[1] = $this->szCourierCalculationLogString."\n\n"; 
                        file_log(true, $strdata, $filename);
                    } 
                    if(!empty($final_resulting_arr) && !empty($courierServiceNewArr))
                    {
                        $iSearchResultType = 1; //Both LCL and Courier Options are available
                        $final_resulting_arr=array_merge($final_resulting_arr,$courierServiceNewArr);
                    }
                    else if(!empty($courierServiceNewArr) && empty($final_resulting_arr))
                    {
                        $iSearchResultType = 3; //Only Courier Options are available
                        $final_resulting_arr = $courierServiceNewArr;
                    }
                    else if(!empty($final_resulting_arr))
                    {
                        $iSearchResultType = 2; //Only LCL Options are available
                    }
                    $this->iSearchResultType = $iSearchResultType; 
                } 
                if($mode=='FORWARDER_TRY_IT_OUT')
                {	
                    // adding resulting data into temperary table 
                    $this->addTempSearchData_forwarder($final_resulting_arr);
                    $forwarder_try_ret_ary = array();
                    if(!empty($final_resulting_arr))
                    {
                        $ctrr=0;
                        foreach($final_resulting_arr as $final_resulting_arrs)
                        {
                            if($final_resulting_arrs['idForwarder'] == $data['forwarder_id'])
                            {
                                $forwarder_try_ret_ary[$ctrr] = $final_resulting_arrs ;
                                $ctrr++;
                            }
                        }
                    }
                    return $forwarder_try_ret_ary;
                }				 
                else if($mode=='MANAGEMENT_TRY_IT_OUT')
                {
                    // adding resulting data into temperary table 
                    $this->addTempSearchData_forwarder($final_resulting_arr,$mode);
                    return $final_resulting_arr;
                }
                else if($mode=='PARTNER_API_CALL')
                {   
                    $idPartner = cPartner::$idGlobalPartner;
                    $szToken = cPartner::$szGlobalToken;
                        
                    // adding resulting data into temperary table 
                   // $this->addTempSearchData($final_resulting_arr,false,$iSearchResultType,$idPartner,$szToken);
                }
                else if($mode!='RECALCULATE_PRICING')
                {   
                    //print_R($final_resulting_arr);
                    // adding resulting data into temperary table 
                    $this->addTempSearchData($final_resulting_arr,$idBooking,$iSearchResultType);
                } 
                return $final_resulting_arr ;
                
            } //END of if(!empty($data))
        }
        
        /**
        * This function is used to fetch courier service rate from different providers
        * @access public
        * @param array,int,int
        * @return array 
        * @author Ajay 
        */
        function getCourierProviderData($postSearchAry,$idCurrency=0,$idForwarder=false,$mode=false)
        {     
            $kConfig = new cConfig();
            $kCourierServices= new cCourierServices();
            if((int)$idCurrency>0)
            {
                $currencyArr=$kConfig->getBookingCurrency($idCurrency);
            } 
            
            $szServiceTerm = trim($postSearchAry['szServiceTerm']);
            
            $kConfig = new cConfig();
            $serviceTermsAry = array();
            $serviceTermsAry = $kConfig->getAllServiceTerms(false,$szServiceTerm);

            if(!empty($serviceTermsAry))
            {
                $idServiceTerms = $serviceTermsAry[0]['id'];
            }
            
            /*
             * @Ajay
            *  If Origin city is NULL then we google API to get City and Postcode.
            *  I believe we never get origin city name empty in tblbooking but still have added following code to make sure we always have the city name
            */
            $originArr = array();
            if(empty($postSearchAry['szOriginCity']))
            {
                $originArr = reverse_geocode($postSearchAry['szOriginCountry']); 
                
                $kConfig = new cConfig();
                $kConfig->loadCountry($postSearchAry['idOriginCountry']);
                $szOriginCountryCode = $kConfig->szCountryISO;
                $szCountryISO3Code = $kConfig->szCountryISO3Code;
                
                $originArr['szCountryCode'] = $szOriginCountryCode;
                $originArr['szCountryISO3Code'] = $szCountryISO3Code;
                
            }
            else
            {
                $kConfig = new cConfig();
                $kConfig->loadCountry($postSearchAry['idOriginCountry']);
                $szOriginCountryCode = $kConfig->szCountryISO;
                $szCountryISO3Code = $kConfig->szCountryISO3Code;
                $originArr['szCountryCode'] = $szOriginCountryCode;
                $originArr['szCountryISO3Code'] = $szCountryISO3Code;
                
                if(!empty($postSearchAry['szOriginPostCode']))
                {
                    $originArr['szPostCode'] = $postSearchAry['szOriginPostCode'];
                }
                else if(!empty($postSearchAry['szOriginPostcodeTemp']))
                {
                    $originArr['szPostCode'] = $postSearchAry['szOriginPostcodeTemp'];
                }
                $originArr['szLat'] = $postSearchAry['fOriginLatitude'];
                $originArr['szLng'] = $postSearchAry['fOriginLongitude'];
                $originArr['szCityName'] = $postSearchAry['szOriginCity']; 
            }
            
            $desArr = array();
            if(empty($postSearchAry['szDestinationCity']))
            {
                $desArr = reverse_geocode($postSearchAry['szDestinationCountry']);
                
                $kConfig = new cConfig();
                $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
                $szDestinationCountryCode = $kConfig->szCountryISO;
                $szCountryISO3Code = $kConfig->szCountryISO3Code;
                
                $desArr['szCountryCode'] = $szDestinationCountryCode;
                $desArr['szCountryISO3Code'] = $szCountryISO3Code;
            }
            else
            {
                $kConfig = new cConfig();
                $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
                $szDestinationCountryCode = $kConfig->szCountryISO;
                $szCountryISO3Code = $kConfig->szCountryISO3Code;
                
                $desArr['szCountryCode'] = $szDestinationCountryCode;
                $desArr['szCountryISO3Code'] = $szCountryISO3Code;
                
                if(!empty($postSearchAry['szDestinationPostCode']))
                {
                    $desArr['szPostCode'] = $postSearchAry['szDestinationPostCode'];
                }
                else if(!empty($postSearchAry['szDestinationPostcodeTemp']))
                {
                    $desArr['szPostCode'] = $postSearchAry['szDestinationPostcodeTemp'];
                }
                $desArr['szLat'] = $postSearchAry['fDestinationLatitude'];
                $desArr['szLng'] = $postSearchAry['fDestinationLongitude'];
                $desArr['szCityName'] = $postSearchAry['szDestinationCity']; 
            } 
            $szSearchMode = $postSearchAry['szSearchMode'];
                        
            $courierPackingType = array();
            if($postSearchAry['iPalletType']>0)
            {
                /*
                *  If searching for pallets, we should only display courier services on /services/ which is pakcing “Pallets” or “Pallets and cartons” 
                */
                $courierPackingType[] = 2;
                $courierPackingType[] = 3;
            }
            if($originArr['szPostCode']=='')
            {
                $postcodeCheckAry['idCountry'] = $postSearchAry['idOriginCountry'];
                $postcodeCheckAry['szLatitute'] = $originArr['szLat'] ;
                $postcodeCheckAry['szLongitute'] = $originArr['szLng'] ; 
                $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry); 
                if(!empty($postcodeResultAry))
                {
                    //If counry is China and we get postcode 200000 in second attempt of Google API then we simply use postcode 200001 instead of 200000
                    if($postcodeResultAry['szPostCode']=='200000' && $postSearchAry['idOriginCountry']==46)
                    {
                        $postcodeResultAry['szPostCode'] = '200001';
                    }
                    $originArr['szPostCode'] = $postcodeResultAry['szPostCode'];
                }
            } 
            if($desArr['szPostCode']=='')
            {
                $postcodeCheckAry['idCountry'] = $postSearchAry['idDestinationCountry'] ;
                $postcodeCheckAry['szLatitute'] = $desArr['szLat'] ;
                $postcodeCheckAry['szLongitute'] = $desArr['szLng'] ; 
                $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry); 
                if(!empty($postcodeResultAry))
                {
                    //If counry is China and we get postcode 200000 in second attempt of Google API then we simply use postcode 200001 instead of 200000
                    if($postcodeResultAry['szPostCode']=='200000' && $postSearchAry['idDestinationCountry']==46)
                    {
                        $postcodeResultAry['szPostCode'] = '200001';
                    }
                    $desArr['szPostCode'] = $postcodeResultAry['szPostCode'];
                }
            } 
            $addressArr=array("szCountry"=>$originArr['szCountryCode'],"szCountryISO3Code"=>$originArr['szCountryISO3Code'],"szSCountry"=>$desArr['szCountryCode'],"szSCountryISO3Code"=>$desArr['szCountryISO3Code'],"szCity"=>$originArr['szCityName'],"szSCity"=>$desArr['szCityName'],"szPostCode"=>$originArr['szPostCode'],"szSPostCode"=>$desArr['szPostCode'],"fCargoWeight"=>$postSearchAry['fCargoWeight']);
                        
            $toatlWeight=$postSearchAry['fCargoWeight'];
            $totalVolume=$postSearchAry['fCargoVolume'];
                        
            if(($fCargoVolume*200)>$fCargoWeight)
            {
                $toatlWeight = $data['fCargoVolume']*200 ;
            } 
                        
            $idOriginCountry = $postSearchAry['idOriginCountry'];
            $idDestinationCountry = $postSearchAry['idDestinationCountry'];
            
            $szCalculationLogString = '';
            if($postSearchAry['idOriginCountry']!=$postSearchAry['idDestinationCountry'])
            { 
                $szCalculationLogString = "<h2>International Trade Service</h2><br>";
                
                $fromCountryProviderArr=$kCourierServices->selectCourierAgreementByCountry($postSearchAry['idOriginCountry'],__TRADE_EXPORT__,$idForwarder);
                $fromCount=count($fromCountryProviderArr);
                        
                $ToCountryProviderArr=$kCourierServices->selectCourierAgreementByCountry($postSearchAry['idDestinationCountry'],__TRADE_IMPORT__,$idForwarder);
                $toCount=count($ToCountryProviderArr);
                
                $providerForwarderArr=array_merge($fromCountryProviderArr,$ToCountryProviderArr);
                
                if(!empty($ToCountryProviderArr))
                {
                    /*
                    foreach($ToCountryProviderArr as $keyTo=>$valueTo)
                    {
                        if(!empty($fromCountryProviderArr))
                        { 
                            foreach($fromCountryProviderArr as $keyFrom=>$valueFrom)
                            {
                                if($keyTo==$keyFrom)
                                { 
                                    $providerForwarderArr[]=$valueFrom;	 
                                }
                            }
                        }
                    }
                    */
                }  
            }
            else
            {
                $providerForwarderArr=$kCourierServices->selectCourierAgreementByCountry($postSearchAry['idOriginCountry'],__TRADE_DOMESTIC__); 
                $szCalculationLogString = "<h2>Domestic Trade Service</h2> <br> ";
            }
            
            /*
             * IF we receive service request for China to Denmark then
             * 
             * 1. if id of tblcourieragreement is equal to 20 Then we don't show service for that courier agreement
             * 2. 46: China and 61: Denmark
             */
            $bSpecialServiceRequest = false;
            if($postSearchAry['idOriginCountry']==46 && $postSearchAry['idDestinationCountry']==61)
            {
                $bSpecialServiceRequest= true;
            }
            
//            if(!empty($providerForwarderArr))
//            {
//                /*
//                * Building Carrier Account IDs for Easypost API
//                * @Ajay This will be a very small piece of array and there is no harm in iterating this twice
//                */
//                $carrierAccountIDAry = array();
//                foreach($providerForwarderArr as $providerForwarderArrs)
//                {
//                   // $carrierAccountIDAry[] = $providerForwarderArrs['szCarrierAccountID'];
//                }
//            }   
            $carrierAccountIDAry = array();
            $stepCounter=1;
            $courierProductForwarderArr=array(); 
            if(!empty($providerForwarderArr))
            {
                $szCalculationLogString_0 = "<br><br><h3>Step 1: Test weight and measure fit with courier product groups</h3>";    
                $kCourierServices = new cCourierServices();

                $courierProviderComanyAry = array();
                $courierProviderComanyAry = $kCourierServices->getCourierProviderDetails();
                
                foreach($providerForwarderArr as $providerForwarderArrs)
                {   
                    if($bSpecialServiceRequest && $providerForwarderArrs['idCourierAgreement']==20 && __ENVIRONMENT__=='LIVE')
                    { 
                        $szCalculationLogString_0 .= "<br> Courier agreement id: ".$providerForwarderArrs['idCourierAgreement']." is added in excluded service list manually ";
                        continue;
                    }
                    $resArr=$kCourierServices->getCourierServices($providerForwarderArrs,false,true,$courierPackingType,$mode);
                    $szProviderName = $courierProviderComanyAry[$providerForwarderArrs['idCourierProvider']]['szName'];
                    /*
                    if($providerForwarderArrs['idCourierProvider']==__FEDEX_API__)
                    {
                        $szProviderName = "Fedex";
                    }
                    else if($providerForwarderArrs['idCourierProvider']==__UPS_API__)
                    {
                        $szProviderName = "UPS";
                    }
                    else
                    {
                        $szProviderName = "TNT";
                    }
                     * 
                     */
                        
                    $checkFlag=false;
                    if(count($resArr)>0)
                    {
                        foreach($resArr as $key=>$values)
                        {   
                            $szCalculationLogString_0 .= "<br><br> <strong>".$stepCounter.".</strong> ".$szProviderName." - ".$values[0]['szForwarderAlias']." Group Name - ".$values[0]['szGroupName']; 
                            $checkLimitFlag=false;   
                            $productProviderArr=array();
                            $productProviderArr=$kCourierServices->getProductProviderLimitations($key,true);

                            if($productProviderArr)
                            {
                              $checkLimitFlag=true; 
                            }
                            if(count($productProviderArr)==0)
                            { 
                                $szCalculationLogString_0 .= "<br> Weight and measure Status: OK ";
                                $szCalculationLogString_0 .= "<br> Description: There is not weight of volume limitation defined";  
                                $includeProductProvider=true;
                            }
                            else
                            {
                                $maxWeight='';
                                $miniWeight='';
                                $maxVolume='';
                                $miniVolume='';
                                $maxNumberPerColli='';
                                $maxWeightPerColli='';
                                $maxChargeableWeight='';
                                $miniWeightPerColli='';
                                $maxChargeableWeight='';
                                $maxWeightStr='Not Defined';
                                $miniWeightStr='Not Defined';
                                $miniWeightPerColliStr="Not Defined";
                                $miniVolumeStr="Not Defined";
                                $maxVolumeStr="Not Defined";
                                $maxChargeableWeightStr="Not Defined";
                                $maxNumberPerColliStr="Not Defined";
                                $maxWeightPerColliStr="Not Defined";
                                $miniChargeableWeight='';
                               // $maxWeightPerColli='';
                                if(!empty($productProviderArr))
                                { 
                                    foreach($productProviderArr as $productProviderArrs)
                                    {
                                        if($productProviderArrs['idCargoLimitationType']==__TOTAL_WEIGHT_FOR_SHIPPMENT__)
                                        {
                                            if($productProviderArrs['szRestriction']=='Minimum')
                                            {
                                                $miniWeight=$productProviderArrs['szLimit']; 
                                                $miniWeightStr=$miniWeight." kg";
                                            }
                                            else if($productProviderArrs['szRestriction']=='Maximum')
                                            {
                                                    $maxWeight=$productProviderArrs['szLimit'];
                                                    $maxWeightStr=$maxWeight." kg";
                                            }
                                        }

                                        if($productProviderArrs['idCargoLimitationType']==__TOTAL_VALUME_FOR_SHIPPMENT__)
                                        {
                                            if($productProviderArrs['szRestriction']=='Minimum')
                                            {
                                                $miniVolume=$productProviderArrs['szLimit'];
                                                $miniVolumeStr=$miniVolume." cbm";
                                                

                                            }
                                            else if($productProviderArrs['szRestriction']=='Maximum')
                                            {
                                                $maxVolume=$productProviderArrs['szLimit'];
                                                $maxVolumeStr=$maxVolume." cbm";
                                            }
                                        }
                                        if($productProviderArrs['idCargoLimitationType']==__WEIGHT_PER_COLI__)
                                        {
                                            if($productProviderArrs['szRestriction']=='Minimum')
                                            {
                                                $miniWeightPerColli = $productProviderArrs['szLimit']; 
                                                $miniWeightPerColliStr=$miniWeightPerColli." kg";
                                            } 
                                            
                                            if($productProviderArrs['szRestriction']=='Maximum')
                                            {
                                                $maxWeightPerColli = $productProviderArrs['szLimit']; 
                                                $maxWeightPerColliStr=$maxWeightPerColli." kg";
                                            } 
                                        } 
                                        if($productProviderArrs['idCargoLimitationType']==__CHARGEABLE_WEIGHT_PER_COLI__)
                                        {
                                            if($productProviderArrs['szRestriction']=='Minimum')
                                            {
                                                $miniChargeableWeight = $productProviderArrs['szLimit']; 
                                            } 
                                            
                                            if($productProviderArrs['szRestriction']=='Maximum')
                                            {
                                                $maxChargeableWeight = $productProviderArrs['szLimit']; 
                                                $maxChargeableWeightStr=$maxChargeableWeight." kg";
                                            } 
                                        } 
                                        
                                        if($productProviderArrs['idCargoLimitationType']==__NUMBER_OF_COLLI__)
                                        {
                                            if($productProviderArrs['szRestriction']=='Maximum')
                                            {
                                                $maxNumberPerColli = $productProviderArrs['szLimit'];  
                                                $maxNumberPerColliStr=round($maxNumberPerColli);
                                            } 
                                        }
                                    }

                                    $iMinWeightCheck = true;
                                    $iMaxWeightCheck = true;
                                    $iMinVolumeCheck = true;
                                    $iMaxVolumeCheck = true; 
                                    $iMinWeightPerColli = true;
                                    $iMinChargeableWeight = true;
                                    $iMaxNumberColli=true;
                                    $iMaxNumberChargeColli=true;
                                    $stopFlag=true;

                                    $kBooking = new cBooking();
                                    $fChargeableWeight = $kBooking->getMinMax($totalVolume*200,$toatlWeight,'MAX');
                                    
                                    $includeProductProvider=true;
                                    $szCalculationLogString_0 .="<br>Total weight for search - ".round($toatlWeight,2)." kg"; 
                                    $szCalculationLogString_0 .="<br>Total weight for shipment(maximum) - ".$maxWeightStr;
                                    $szCalculationLogString_0 .="<br>Total weight for shipment(minimum) - ".$miniWeightStr;
                                    if((float)$miniWeight>0.00 && ((float)$toatlWeight<(float)$miniWeight))
                                    {
                                        $includeProductProvider=false;
                                        $iMinWeightCheck = false;
                                    }

                                    if($includeProductProvider && (float)$maxWeight>0.00)
                                    {
                                        if((float)$toatlWeight>(float)$maxWeight)
                                        {
                                            $includeProductProvider=false;
                                            $iMaxWeightCheck = false;
                                        }
                                    }  
                                    if($iMaxWeightCheck && $iMinWeightCheck)
                                    {
                                        $szCalculationLogString_0.="<br/> Total weight for shipment - Ok";
                                    }
                                    else
                                    {
                                        $stopFlag=false;
                                        $szCalculationLogString_0.="<br/> Total weight for shipment - Stop";
                                    }     
                                    
                                    if($stopFlag)
                                    {    
                                        $szCalculationLogString_0 .="<br/>Total weight for search -".round($toatlWeight,2)." kg";
                                        $szCalculationLogString_0 .="<br/>Actual weight per colli(minimum) - ".$miniWeightPerColliStr;
                        
                                        if($includeProductProvider && (float)$miniWeightPerColli>0.00)
                                        {
                                            if((float)$toatlWeight<(float)$miniWeightPerColli)
                                            {
                                                $includeProductProvider=false;
                                                $iMinWeightPerColli = false;
                                            } 
                                        }

                                        if($iMinWeightPerColli)
                                        {
                                            $szCalculationLogString_0 .="<br/>Actual weight per colli - Ok<br/>";
                                        }
                                        else
                                        {
                                            $stopFlag=false;
                                            $szCalculationLogString_0 .="<br/>Actual weight per colli - Stop<br/>";
                                        }  
                                    }
                                    
                                    if($stopFlag){
                                    $szCalculationLogString_0 .="<br>Total volume for search - ".$totalVolume." cbm"; 
                                    $szCalculationLogString_0 .="<br>Total volume of shipment(maximum) - ".$maxVolumeStr;
                                    $szCalculationLogString_0 .="<br>Total volume of shipment(minimum) - ".$miniVolumeStr;
                                    
                                        if($includeProductProvider && (float)$miniVolume>0.00)
                                        {
                                            if((float)$totalVolume<(float)$miniVolume)
                                            {
                                                $includeProductProvider=false;
                                                $iMinVolumeCheck = false;
                                            } 
                                        } 	
                                        if($includeProductProvider && (float)$maxVolume>0.00)
                                        {
                                            if((float)$totalVolume>(float)$maxVolume)
                                            {
                                                $includeProductProvider=false;
                                                $iMaxVolumeCheck = false;
                                            } 
                                        } 


                                        if($iMinVolumeCheck && $iMaxVolumeCheck)
                                        {
                                            $szCalculationLogString_0.="<br/> Total volume for shipment - Ok";
                                        }
                                        else
                                        {
                                            $stopFlag=false;
                                            $szCalculationLogString_0.="<br/> Total volume for shipment - Stop";
                                        }
                                    
                                    }

                                    if($includeProductProvider && (float)$fChargeableWeight>0.00)
                                    {
                                        if((float)$fChargeableWeight<(float)$miniChargeableWeight)
                                        {
                                            $includeProductProvider=false;
                                            $iMinChargeableWeight = false;
                                        } 
                                    }   
                                    
                                    if($stopFlag){
                                        $CalculatePackageFlag="No";
                                        $totalCargeableColli=1;
                                        $totalCargeableColliRound=1;
                                        if((float)$fChargeableWeight>(float)$maxChargeableWeight && (float)$maxChargeableWeight>0.00)
                                        {
                                            $CalculatePackageFlag="Yes";
                                            $rmaingWeight=$fChargeableWeight%$maxChargeableWeight;
                                            $totalMaxColliKgPack=($fChargeableWeight-$rmaingWeight)/$maxChargeableWeight;
                                            $t=0;
                                            if((int)$rmaingWeight>0)
                                            {
                                              $t=1;  
                                            }
                                            $totalMaxColliKgPack=$totalMaxColliKgPack+$t;
                                            $totalCargeableColli=$fChargeableWeight/$maxChargeableWeight;
                                            $totalCargeableColliRound=$totalMaxColliKgPack; 
                                        }    
                                        $szCalculationLogString_0 .="<br>Chargeable weight for search - ".$fChargeableWeight." kg"; 
                                        $szCalculationLogString_0 .="<br>Chargeable weight per colli(maximum) - ".$maxChargeableWeightStr;
                                        $szCalculationLogString_0 .="<br>Calculate number of packages - ".$CalculatePackageFlag;
                                        $szCalculationLogString_0 .="<br>Number of packages - ".$totalCargeableColli;
                                        $szCalculationLogString_0 .="<br>Round up - ".$totalCargeableColliRound;
                                        $szCalculationLogString_0 .="<br>Number of colli(maximum) - ".$maxNumberPerColliStr;
                        
                                        if($includeProductProvider && (float)$maxNumberPerColli>0)
                                        { 
                                            if((float)$toatlWeight>(float)$maxWeightPerColli && (float)$maxWeightPerColli>0.00 && (int)$maxNumberPerColli>0)
                                            { 
                                                $rmaingWeight=$toatlWeight%$maxWeightPerColli;
                                                $totalMaxColliKgPack=($toatlWeight-$rmaingWeight)/$maxWeightPerColli;
                                                $t=0;
                                                if((int)$rmaingWeight>0)
                                                {
                                                  $t=1;  
                                                }

                                                $totalCount=$totalMaxColliKgPack+$t;  
                                                if((int)$totalCount>(int)$maxNumberPerColli)
                                                {
                                                    $includeProductProvider=false;
                                                    $iMaxNumberColli = false;
                                                }
                                            } 

                                            if((float)$fChargeableWeight>(float)$maxChargeableWeight && (float)$maxChargeableWeight>0.00 && (int)$maxNumberPerColli>0)
                                            { 
                                                $rmaingWeight=$fChargeableWeight%$maxChargeableWeight;
                                                $totalMaxColliKgPack=($fChargeableWeight-$rmaingWeight)/$maxChargeableWeight;
                                                $t=0;
                                                if((int)$rmaingWeight>0)
                                                {
                                                  $t=1;  
                                                }

                                                $totalCount=$totalMaxColliKgPack+$t;  
                                                if((int)$totalCount>(int)$maxNumberPerColli)
                                                {
                                                    $includeProductProvider=false;
                                                    $iMaxNumberChargeColli = false;
                                                }
                                            }
                                        } 
                                        
                                        if($iMaxNumberChargeColli)
                                        {
                                            $szCalculationLogString_0 .="<br>Number of colli - Ok";
                                        }
                                        else
                                        {
                                            $stopFlag=false;
                                            $szCalculationLogString_0 .="<br>Number of colli - Stop";
                                        }
                                        
                                        $CalculateActualPackageFlag="No";
                                        $totalactualColli=1;
                                        $totalactualColliRound=1;
                                        if((float)$toatlWeight>(float)$maxWeightPerColli && (float)$maxWeightPerColli>0.00)
                                        {
                                            $CalculateActualPackageFlag="Yes";
                                            $rmaingWeight=$toatlWeight%$maxWeightPerColli;
                                            $totalMaxColliKgPack=($toatlWeight-$rmaingWeight)/$maxWeightPerColli;
                                            $t=0;
                                            if((int)$rmaingWeight>0)
                                            {
                                              $t=1;  
                                            }
                                            $totalMaxColliKgPack=$totalMaxColliKgPack+$t;
                                            $totalactualColli=$toatlWeight/$maxWeightPerColli;
                                            $totalactualColliRound=$totalMaxColliKgPack;
                                        } 
                                        $szCalculationLogString_0 .="<br>Actual weight for search - ".$toatlWeight." kg"; 
                                        $szCalculationLogString_0 .="<br>Actual weight per colli(maximum) - ".$maxWeightPerColliStr;
                                        $szCalculationLogString_0 .="<br>Calculate number of packages - ".$CalculateActualPackageFlag;
                                        $szCalculationLogString_0 .="<br>Number of packages - ".$totalactualColli;
                                        $szCalculationLogString_0 .="<br>Round up - ".$totalactualColliRound;
                                        $szCalculationLogString_0 .="<br>Number of colli(maximum) - ".$maxNumberPerColliStr;
                                        if($iMaxNumberColli)
                                        {
                                            $szCalculationLogString_0 .="<br>Number of colli - Ok";
                                        }
                                        else
                                        {
                                             $stopFlag=false;
                                            $szCalculationLogString_0 .="<br>Number of colli - Stop";
                                        }
                                    }     
                                    if($stopFlag)
                                    {
                                        $szCalculationLogString_0 .="<br>API call - Yes";
                                    }
                                    else
                                    {
                                        $szCalculationLogString_0 .="<br>API call - No";
                                    }    
                                    if($totalactualColliRound>$totalCargeableColliRound)
                                    {
                                        $szCalculationLogString_0 .="<br>API call packages - ".$totalactualColliRound;
                                        $packageWeight=$toatlWeight/$totalactualColliRound;
                                        $szCalculationLogString_0 .="<br>API call weight per package - ".$packageWeight;
                                    }
                                    else
                                    {
                                        $szCalculationLogString_0 .="<br>API call packages - ".$totalCargeableColliRound;
                                        $packageWeight=$fChargeableWeight/$totalCargeableColliRound;
                                        $szCalculationLogString_0 .="<br>API call weight per package - ".$packageWeight;
                                    }
                                    /*if((float)$toatlWeight<(float)$fChargeableWeight)
                                    {
                                        $szCalculationLogString_0 .="<br>API call packages - ".$totalCargeableColliRound;
                                        $packageWeight=$fChargeableWeight/$totalCargeableColliRound;
                                        $szCalculationLogString_0 .="<br>API call weight per package - ".$packageWeight;
                                    }   
                                    else if((float)$toatlWeight==(float)$fChargeableWeight)
                                    {
                                        if($totalactualColliRound>$totalCargeableColliRound)
                                        {
                                            $szCalculationLogString_0 .="<br>API call packages - ".$totalactualColliRound;
                                            $packageWeight=$toatlWeight/$totalactualColliRound;
                                            $szCalculationLogString_0 .="<br>API call weight per package - ".$packageWeight;
                                        }
                                        else
                                        {
                                            $szCalculationLogString_0 .="<br>API call packages - ".$totalCargeableColliRound;
                                            $packageWeight=$fChargeableWeight/$totalCargeableColliRound;
                                            $szCalculationLogString_0 .="<br>API call weight per package - ".$packageWeight;
                                        }
                                    }
                                    else
                                    {
                                        $szCalculationLogString_0 .="<br>API call packages - ".$totalactualColliRound;
                                        $packageWeight=$toatlWeight/$totalactualColliRound;
                                        $szCalculationLogString_0 .="<br>API call weight per package - ".$packageWeight;
                                    }*/
                                        
                                    if($includeProductProvider)
                                    {
                                        $szCalculationLogString_0 .= "<br> Weight and measure Status: OK "; 
                                    }
                                    else
                                    {
                                        if(!$iMinWeightCheck)
                                        {
                                            $szDescription = " Weight check failed ";
                                        }
                                        else if(!$iMaxWeightCheck)
                                        {
                                            $szDescription = " Weight check failed ";
                                        }
                                        else if(!$iMinVolumeCheck)
                                        {
                                            $szDescription = " Volume check failed ";
                                        }
                                        else if(!$iMaxVolumeCheck)
                                        {
                                            $szDescription = " Volume check failed ";
                                        }
                                        else if(!$iMinWeightPerColli)
                                        {
                                            $szDescription = " Minimum weight per colli failed ";
                                        }
                                        else if(!$iMinChargeableWeight)
                                        {
                                            $szDescription = " Minimum chargeable weight per colli failed ";
                                        }
                                        else if(!$iMaxNumberColli)
                                        {
                                            $szDescription = " Number of colli exceed for actual weight";
                                        }
                                        else if(!$iMaxNumberChargeColli)
                                        {
                                            $szDescription = " Number of colli exceed for chargeable weight";
                                        } 
                                        $szCalculationLogString_0 .= "<br> Weight and measure Status: Stop ";
                                        $szCalculationLogString_0 .= "<br> Description: ".$szDescription;
                                    } 
                                }
                            } 
                            $apiCodeArr=array();
                            $apiCodeArrLogs = array();
                            $apiCodeStr=''; 
                            if($includeProductProvider)
                            { 
                                if($providerForwarderArrs['idCourierProvider']==__FEDEX_API__ || $providerForwarderArrs['idCourierProvider']==__UPS_API__ || $providerForwarderArrs['idCourierProvider']==__DHL_API__)
                                { 
                                    if(!empty($carrierAccountIDAry['others']) && !in_array($providerForwarderArrs['szCarrierAccountID'],$carrierAccountIDAry['others']))
                                    {
                                        $carrierAccountIDAry['others'][] = "'".$providerForwarderArrs['szCarrierAccountID']."'";
                                        $carrierAccountIDAry['all'][] = "'".$providerForwarderArrs['szCarrierAccountID']."'";
                                    }
                                    else if(empty($carrierAccountIDAry['others']))
                                    {
                                        $carrierAccountIDAry['others'][] = "'".$providerForwarderArrs['szCarrierAccountID']."'";
                                        $carrierAccountIDAry['all'][] = "'".$providerForwarderArrs['szCarrierAccountID']."'";
                                    } 
                                }
                                else 
                                {
                                    if(!empty($carrierAccountIDAry['tnt']) && !in_array($providerForwarderArrs['szCarrierAccountID'],$carrierAccountIDAry['tnt']))
                                    {
                                        $carrierAccountIDAry['tnt'][] = "'".$providerForwarderArrs['szCarrierAccountID']."'";
                                        $carrierAccountIDAry['all'][] = "'".$providerForwarderArrs['szCarrierAccountID']."'";
                                    }
                                    else if(empty($carrierAccountIDAry['tnt']))
                                    {
                                        $carrierAccountIDAry['tnt'][] = "'".$providerForwarderArrs['szCarrierAccountID']."'";
                                        $carrierAccountIDAry['all'][] = "'".$providerForwarderArrs['szCarrierAccountID']."'";
                                    }  
                                }   
                                if(count($values)>0)
                                {
                                    if(count($values))
                                    { 
                                        foreach($values as $value)
                                        {
                                            $apiCodeArr[]=$value['szCourierAPICode']."||||".$value['idCourierProviderProductid']."||||".$value['idCourierAgreement'];
                                            $apiCodeArrLogs[]=$value['szCourierAPICode'].";".$value['idCourierProviderProductid'].";".$value['idCourierAgreement'];
                                            //$providerProductIDArr[]=$value['idCourierProviderProductid'];
                                        }
                                        if(count($apiCodeArr)>0)
                                            $apiCodeStr=implode(";",$apiCodeArr);
//                                        if(count($providerProductIDArr)>0)
//                                            $providerProductIDStr=implode(";",$providerProductIDArr);
                                    } 
                                    $values[0]['szAPICode']=$apiCodeStr; //This is used for actual calculation
                                    $values[0]['szAPICodeStr']=$apiCodeArrLogs; //This is one only used for calculation logs 
                                    $courierProductForwarderArr=array_merge($courierProductForwarderArr,$values);                                     
                                }
                            }
                            ++$stepCounter;
                        }
                    } 
                } 
            } 
            //print_R($carrierAccountIDAry);
            cEasyPost::setCarrierProviders($carrierAccountIDAry);
            
            $oldidCourierAgreement=0;
            $courierResultAry = array();
            $kWHSSearch = new cWHSSearch();
            $showResponseCtr=0; 
            if(!empty($courierProductForwarderArr))
            {
                $ctr=0;
                $szCalculationLogString_1 = "<h3>Step 2: Find forwarders offering courier products and check if trade covered</h3><br> ";
                //$szCalculationLogString_2 = "<br><br><h3>Step 2: Test weight and measure fit with courier product </h3>";
                $szCalculationLogString_3 = "<br><h3>Step 2: API request sent </h3>";
                $szCalculationLogString_4 .= "<h3>Step 3: Calculate sales rate </h3>"; 
                $szCalculationLogString_5 .= "<h3>Step 4: Select relevant options for gross list and calculate display price</h3>"; 
                $szCalculationLogString_6 .= "<h3>Step 5: Select what to show on screen 1 from gross list </h3>"; 
                $stepCounter = 0;
                $stepCounter1=1; 
                $courierGroupTempAry = array();
                        
                if($mode!='PARTNER_API_CALL')
                {
                    /*
                    * In case of partner API calls we initialize following variables in partner.class.php
                    */                    
                    cEasyPost::$iEasyPostApiCalled = false;
                    cEasyPost::$iEasyPostApiCalledTNT = false;
                    cEasyPost::$iEasyPostApiCalledStandardAgreementTNT = false;
                        
                    cEasyPost::$globalEasyPostApiResponse = array();
                    cEasyPost::$globalEasyPostApiResponseTNT = array();

                    cEasyPost::$globalEasyPostApiResponseStandardAgreementTNT = array();
                    cEasyPost::$globalEasyPostApiResponseStandardAgreement = array();

                    cPostmen::$globalPostMenApiResponse = array();
                    cPostmen::$globalEasyPostApiResponse = array();
                }  
                
                $kCourierServices = new cCourierServices();
                $kForwarderNew = new cForwarder();

                $courierProviderComanyAry = array();
                $courierProviderComanyAry = $kCourierServices->getCourierProviderDetails();
                
                $alreadyCheckedForwarderAry = array();
                $szForwarderProduct = "COURIER";
                foreach($courierProductForwarderArr as $courierProductForwarderArrs)
                {      
                    $stepCounter++;
                    $szProviderName = $courierProviderComanyAry[$courierProductForwarderArrs['idCourierProvider']]['szName'];
                        
                    if($postSearchAry['iPrivateShipping']==1)
                    {   
                        if(!empty($alreadyCheckedForwarderAry) && in_array($courierProductForwarderArrs['idForwarder'],$alreadyCheckedForwarderAry))
                        { 
                            continue;
                        }
                        else  
                        { 
                            $privateCustomerFeeAry = array();
                            $privateCustomerFeeAry = $kForwarderNew->getAllPrivateCustomerPricing($courierProductForwarderArrs['idForwarder'],$szForwarderProduct,1);

                            if(empty($privateCustomerFeeAry[$szForwarderProduct]))
                            {
                                $szCalculationLogString_1 .= "<br><br><strong> ".$courierProductForwarderArrs['szForwarderCompanyName']." doesn't accept Courier bookings from private customers</strong>"; 
                                $alreadyCheckedForwarderAry[] = $courierProductForwarderArrs['idForwarder']; 
                                continue; 
                            } 
                        } 
                    } 
                    $szCalculationLogString_1 .= "<br><br> <strong>".$stepCounter.".</strong> ".$szProviderName." - ".$courierProductForwarderArrs['szDisplayName'];
                        
                    $iSeachPricesForTrade = true;
                    
                    //Checking if trades are added to courier excluded trade service list or not
                    //If trade is added to excluded list then no need to check furthure
                    
                    $excludedTradeSearch = array();
                    $excludedTradeSearch['idCourierProvider'] = $courierProductForwarderArrs['idCourierProvider'];
                    $excludedTradeSearch['idForwarder'] = $courierProductForwarderArrs['idForwarder'];
                    $excludedTradeSearch['idOriginCountry'] = $idOriginCountry;
                    $excludedTradeSearch['idDestinationCountry'] = $idDestinationCountry;
                        
                    if($_SESSION['user_id']>0)
                    {
                        $kUser = new cUser();
                        $kForwarder = new cForwarder();
                        $kUser->getUserDetails($_SESSION['user_id']);
                        
                        if($kUser->iPrivate==1)
                        {
                            $excludedTradeSearch['iCustomerType'] = __COURIER_EXCLUDED_TRADE_CUSTOMER_TYPE_PRIVATE__;
                        }
                        else
                        {
                            $excludedTradeSearch['iCustomerType'] = __COURIER_EXCLUDED_TRADE_CUSTOMER_TYPE_BUSSINESS__;
                        } 
                    } 
                    if($kCourierServices->isTradeAddedToExcludedList($excludedTradeSearch))
                    {
                        $iSeachPricesForTrade = false;
                        $szCalculationLogString_1 .= " <br> Forwarder: ".$courierProductForwarderArrs['szForwarderCompanyName']." (".$courierProductForwarderArrs['szForwarderAlias'].") <br> Trade: NOT OK  ";
                        $szCalculationLogString_1 .= " <br> Description: This trade is added in excluded trade service list. ";
                    }
                    else
                    {
                        $szCalculationLogString_1 .= " <br> Forwarder: ".$courierProductForwarderArrs['szForwarderCompanyName']." (".$courierProductForwarderArrs['szForwarderAlias'].") <br> Trade: OK  ";
                    }
                    
                    if($iSeachPricesForTrade)
                    {
                        $productProviderArr=$kCourierServices->getProductProviderLimitations($courierProductForwarderArrs['idCourierProviderProductid'],true);
                        
                        $data=array();
                        $data=array_merge((array)$courierProductForwarderArrs,(array)$addressArr);
                        $data['fCargoVolume']=$totalVolume;  
                        
                        $idForwarder = $courierProductForwarderArrs['idForwarder'];
                        if(!empty($forwarderUsedAry) && in_array($idForwarder, $forwarderUsedAry))
                        {
                            $bCallAPI = false; 
                            //$szGroupKey = $idForwarder."-".$courierProductForwarderArrs['idGroup']."-".$courierProductForwarderArrs['idCourierProviderProductid']."-".$courierProductForwarderArrs['idCourierAgreement'];
                            $szGroupKey = $idForwarder."-".$courierProductForwarderArrs['idGroup'] ."-".$courierProductForwarderArrs['idCourierAgreement'];
                            if(!empty($courierGroupTempAry) && !in_array($szGroupKey, $courierGroupTempAry))
                            {
                                $bCallAPI = true; 
                            }
                            else if(empty($courierGroupTempAry))
                            {
                                $bCallAPI = true; 
                            }
                            /*
                            if(((int)$oldidGroup!=(int)$courierProductForwarderArrs['idGroup']))
                            {
                                $bCallAPI = true; 
                            }
                            else
                            {
                                $bCallAPI = false; 
                            }
                             * 
                             */
                        } 
                        else
                        {
                            $forwarderUsedAry[] = $idForwarder;
                            $bCallAPI = true; 
                        } 
                        if($bCallAPI)
                        {  
                            $showResponseCtr=1; 
                            $resArr = array();
                            $oldidGroup=$courierProductForwarderArrs['idGroup']; 
                            $courierGroupTempAry[] = $idForwarder."-".$courierProductForwarderArrs['idGroup'] ."-".$courierProductForwarderArrs['idCourierAgreement'];
                        
                            $curr_date_time = strtotime(date('Y-m-d'));
                            $date_str_time = strtotime(date('Y-m-d',strtotime($postSearchAry['dtTimingDate']))); 
                            
                            /*
                            * Add 1 day to the searched date before sending it to courier API if searched date = today. If searched date is greater, then send the searched date.
                            */
                            $data['dtTimingDate'] = $postSearchAry['dtTimingDate'];  
                            if($curr_date_time>=$date_str_time)
                            {
                                $dtTimingDate=date('Y-m-d',strtotime('+1 DAY'));
                                $szCalculationLogString_1 .= "<br> Condition applied: (Search date = current date)";
                                if(__isWeekend($dtTimingDate))
                                {
                                    $dtTimingDate = __nextMonday($dtTimingDate);
                                }
                                $data['dtTimingDate'] = $dtTimingDate;  
                            }
                            else if(__isWeekend($data['dtTimingDate']))
                            {
                                $szCalculationLogString_1 .= "<br> Searched day is weekend so we are picking next monday as colletion date.";
                                $data['dtTimingDate'] = __nextMonday($data['dtTimingDate']);
                            } 
                            $postSearchAry['dtTimingDate'] = $data['dtTimingDate'];
                            $data['idOriginCountry']=$postSearchAry['idOriginCountry'];
                            $data['idDestinationCountry']=$postSearchAry['idDestinationCountry'];
                            $data['idBooking'] = $postSearchAry['id'];  
                            $data['fCargoWeight']=$postSearchAry['fCargoWeight'];
                            $data['fCargoVolume']=$postSearchAry['fCargoVolume']; 
                            $data['szCarrierAccounID']=$postSearchAry['fCargoVolume'];  
                            $data['idLandingPage'] = $postSearchAry['idLandingPage'];  
                            $data['iSearchMiniVersion']=$postSearchAry['iSearchMiniVersion'];   
                            $data['szCargoType']=$postSearchAry['szCargoType'];  
                            $data['iQuickQuote']=$postSearchAry['iQuickQuote'];  
                            $data['szSearchMode'] = $szSearchMode;  
                            $data['iPrivateShipping'] = $postSearchAry['iPrivateShipping']; 
                            $data['iQuickQuoteTryitout'] = $postSearchAry['iQuickQuoteTryitout'];
                            $data['iCheckHandlingFeeForAutomaticQuote'] = $postSearchAry['iCheckHandlingFeeForAutomaticQuote'];
                            
                            $kWebServices = new cWebServices();
                            $resArr = $kWebServices->getdetailsFromCourierAPI($data);
                            $szCalculationLogString_3 .= "<br>".$kWebServices->szPackageDetailsLogs ; 
                            
                            $szCalculationLogString_3 .= "<br><br> <strong>".$stepCounter.".</strong> ".$szProviderName." - ".$courierProductForwarderArrs['szDisplayName']; 
                            $szCalculationLogString_3 .= "<br> ".$courierProductForwarderArrs['szForwarderAlias']." Username: ". $courierProductForwarderArrs['szUsername']." ";
                            $szCalculationLogString_3 .= "<br> ".$courierProductForwarderArrs['szForwarderAlias']." Password: ". $courierProductForwarderArrs['szPassword']." ";
                            $szCalculationLogString_3 .= "<br> Request services: ";
                            $szCalculationLogString_3 .= "<br> ".$courierProductForwarderArrs['szCourierProductName']." -  ". $courierProductForwarderArrs['szCourierAPICode']." ";
                        }
                        else
                        {
                           // ++$showResponseCtr;
                        }         
                        $idCourierProviderProductid = $courierProductForwarderArrs['idCourierProviderProductid'];
                        $idCourierAgreement = $courierProductForwarderArrs['idCourierAgreement']; 
                        $newresArr=array(); 
                        //echo "<br><br> Provider: ".$courierProductForwarderArrs['idCourierProvider']." Code: ".$courierProductForwarderArrs['szCourierAPICode']." Prod: ".$idCourierProviderProductid." Agr: ".$idCourierAgreement;
                        if(count($resArr)>0)
                        {  
                            foreach($resArr as $resArrs)
                            {     
                                //echo "<br> Result: ".$resArrs['szServiceType']." ID: ".$resArrs['idCourierProviderProduct']." Agr: ".$resArrs['idCourierAgreement']; 
                                if($resArrs['szServiceType']==$courierProductForwarderArrs['szCourierAPICode']  && $resArrs['idCourierProviderProduct']==$courierProductForwarderArrs['idCourierProviderProductid'])
                                {  
                                    $newresArr = $resArrs; 
                                }   
                            } 
                            if(!empty($newresArr))
                            { 
                                $szCalculationLogString_3 .= "<br><b>Status: Got response from API </b>for ".str_replace("_"," ",$courierProductForwarderArrs['szCourierAPICode'])." service type ";
                                $courierResultAry[$ctr]=array_merge((array)$courierProductForwarderArrs,(array)$addressArr,$newresArr); 
                        
                                if($showResponseCtr==1)
                                {
                                    ++$showResponseCtr;
                                    $szCalculationLogString_4 .= $kWebServices->szApiResponseLog ;  
                                } 
                                $ctr++;
                                ++$stepCounter1;
                            }
                            else
                            {
                              $szCalculationLogString_3 .= "<br><b>Status: No response from API for </b>".str_replace("_"," ",$courierProductForwarderArrs['szCourierAPICode'])." service type. Process Ends... <br> ";  
                            } 
                        }
                        else
                        {
                            $szCalculationLogString_3 .= "<br><b>Status: No response from API</b> Process Ends... <br> ";
                        }
                    } 
                }
            } 
                        
            $courierResultAry = sortArray($courierResultAry,'fShipmentUSDCost');
            $kCourierServices = new cCourierServices();            
            $courierResultGrossListAry = array();
                        
            if(!empty($courierResultAry))
            {	 
                $stepCounter = 1;
                $ctr = 0;
                $addedForwarderAry = array();
                $forwarderReviewsAry = array();
                $forwarderRatingAry = array(); 
                $kVatApplication = new cVatApplication();        
                foreach($courierResultAry as $courierResultArys)
                {
                    $szCalculationLogString_5 .= "<br><br> <strong>".$stepCounter.".</strong>  " ; 
                    $szCalculationLogString_5 .= " ".$courierResultArys['szName']." - ".$courierResultArys['szCourierProductName']." - ".$courierResultArys['szForwarderCompanyName']. " - USD ".$courierResultArys['fShipmentUSDCost'];
                    $szServiceType = $courierResultArys['szCourierAPICode'];
                        
                    //$szServiceTypeKey = $courierResultArys['szCourierAPICode']."_".$courierResultArys['idForwarder']."_".$courierResultArys['idCourierPacking'];
                    $szServiceTypeKey = $courierResultArys['szCourierAPICode']."_".$courierResultArys['idCourierPacking'];
                    if((!empty($serviceTypeAry) && !in_array($szServiceTypeKey, $serviceTypeAry)) || empty($serviceTypeAry))
                    {
                        $courierResultGrossListAry[$ctr] = $courierResultArys ;
                        $serviceTypeAry[] = $szServiceTypeKey ; 
                        $szCalculationLogString_5 .= " - Cheapest";
                        
                        $idForwarderCurrency = $courierResultArys['szForworderCurrency'] ;
                        if($idForwarderCurrency==1)
                        {
                            $courierResultGrossListAry[$ctr]['fDisplayForwarderPrice'] = $courierResultArys['fShipmentUSDCost'];
                            $courierResultGrossListAry[$ctr]['szForworderCurrency'] = 'USD';
                            $courierResultGrossListAry[$ctr]['idForworderCurrency'] = $idForwarderCurrency; 
                            $courierResultGrossListAry[$ctr]['fForwarderCurrencyExchangeRate'] = 1; 
                        }
                        else
                        { 
                            $currencyResultAry = $this->getCurrencyDetails($idForwarderCurrency);
                            $fExchangeRate = $currencyResultAry['fUsdValue']; 
                            
                            if($fExchangeRate>0)
                            {
                                $fCostForwarder = $courierResultArys['fShipmentUSDCost']/$fExchangeRate; 
                            }
                            else
                            {
                                $fCostForwarder = 0;
                            } 
                            $courierResultGrossListAry[$ctr]['fDisplayForwarderPrice'] = $fCostForwarder;
                            $courierResultGrossListAry[$ctr]['szForworderCurrency'] = $currencyResultAry['szCurrency'];
                            $courierResultGrossListAry[$ctr]['idForworderCurrency'] = $idForwarderCurrency; 
                            $courierResultGrossListAry[$ctr]['fForwarderCurrencyExchangeRate'] = $fExchangeRate ;
                        } 
                        
                        $courierResultGrossListAry[$ctr]['szForwarderCurrency'] = $courierResultGrossListAry[$ctr]['szForworderCurrency'];
                        $courierResultGrossListAry[$ctr]['idForwarderCurrency'] = $courierResultGrossListAry[$ctr]['idForworderCurrency']; 
                        
                        $courierResultGrossListAry[$ctr]['fReferalAmountUSD'] =  $courierResultGrossListAry[$ctr]['fMarkUpRefferalFee'];
                        
                        if($courierResultGrossListAry[$ctr]['fForwarderCurrencyExchangeRate']>0)
                        {
                            $courierResultGrossListAry[$ctr]['fReferalAmount'] = ($courierResultGrossListAry[$ctr]['fReferalAmountUSD'] / $courierResultGrossListAry[$ctr]['fForwarderCurrencyExchangeRate']) ;
                        }
                        else
                        {
                            $courierResultGrossListAry[$ctr]['fReferalAmount'] = 0;
                        }
                        
                        $iCurrencyMarkup = false;
                        $fPriceMarkUp = 0; 
                        
                        $fPriceNoMarkupUSD = $courierResultArys['fShipmentUSDCost'];
                        $fTotalSelfInvoiceAmountUSD = $courierResultArys['fTotalSelfInvoiceAmountUSD'];
                        
                        if($idForwarderCurrency!=$idCurrency)
                        {
                            $fPriceMarkUp = $this->getManageMentVariableByDescription('__MARK_UP_PRICING_PERCENTAGE__');
                        
                            $fMarkupPriceUSD = (($courierResultArys['fShipmentUSDCost'] * $fPriceMarkUp)/100);
                            $courierResultArys['fShipmentUSDCost'] = $courierResultArys['fShipmentUSDCost'] + $fMarkupPriceUSD;  
                            $iCurrencyMarkup = true;  
                        }  
                        $courierResultGrossListAry[$ctr]['fBookingPriceUSD'] = $courierResultArys['fShipmentUSDCost'] + $fPriceUSD ;  
                        if($idCurrency==1)
                        { 
                            $courierResultGrossListAry[$ctr]['fDisplayPrice'] = $courierResultArys['fShipmentUSDCost'];
                            $courierResultGrossListAry[$ctr]['szCurrency'] = 'USD';
                            $courierResultGrossListAry[$ctr]['idCurrency'] = $idCurrency;
                            $courierResultGrossListAry[$ctr]['fExchangeRate'] = 1;
                            $fExchangeRate = 1;
                            
                            $courierResultGrossListAry[$ctr]['fTotalMarkupPrice'] = round((float)$fMarkupPriceUSD,6) ;
                            $courierResultGrossListAry[$ctr]['fMarkupPercentage'] = $fPriceMarkUp ; 
                            
                            $fPriceNoMarkup = $fPriceNoMarkupUSD;
                        }
                        else
                        { 
                            $currencyResultAry = $this->getCurrencyDetails($idCurrency);
                            $fExchangeRate = $currencyResultAry['fUsdValue']; 
                            
                            if($fExchangeRate>0)
                            {
                                $fCost= $courierResultArys['fShipmentUSDCost']/$fExchangeRate;
                                $fMarkupPrice = $fMarkupPriceUSD / $fExchangeRate ;
                                $fPriceNoMarkup = $fPriceNoMarkupUSD/$fExchangeRate;
                            }
                            else
                            {
                                $fCost = 0;
                            } 
                            $courierResultGrossListAry[$ctr]['fDisplayPrice'] = $fCost;
                            $courierResultGrossListAry[$ctr]['szCurrency'] = $currencyResultAry['szCurrency'];
                            $courierResultGrossListAry[$ctr]['idCurrency'] = $idCurrency;
                            $courierResultGrossListAry[$ctr]['fExchangeRate'] = $fExchangeRate;
                            
                            $courierResultGrossListAry[$ctr]['fTotalMarkupPrice'] = round((float)$fMarkupPrice,6) ;
                            $courierResultGrossListAry[$ctr]['fMarkupPercentage'] = $fPriceMarkUp ;
                        } 
                        /*
                        * fTotalBookingPriceIncludingTrucking price will used only for sorting purpose in case of service type: P/W
                        */
                        $courierResultGrossListAry[$ctr]['fTotalBookingPriceIncludingTrucking'] = $courierResultGrossListAry[$ctr]['fDisplayPrice']; 
                        
                        /*if((int)$courierResultGrossListAry[$ctr]['idForwarderCountry']>0)
                        {
                            $kVatApplication = new cVatApplication();
                            if(!$kVatApplication->checkForwarderCountryExists($courierResultGrossListAry[$ctr]['idForwarderCountry'],true))
                            { */
                            if((int)$postSearchAry['iPrivateShipping']==1)
                            {
                                $vatInputAry = array();
                                $vatInputAry['idCustomerCountry'] = $postSearchAry['idCustomerAccountCountry'];
                                $vatInputAry['idOriginCountry'] = $postSearchAry['idOriginCountry'];
                                $vatInputAry['idDestinationCountry'] = $postSearchAry['idDestinationCountry']; 
                                $fVatRate = $kVatApplication->getTransportecaVat($vatInputAry);
                                
                                $kConfig_new1 = new cConfig();
                                $kConfig_new1->loadCountry($postSearchAry['idCustomerAccountCountry']);
                                $szVATRegistration = $kConfig_new1->szVATRegistration;
                                
                                $courierResultGrossListAry[$ctr]['szVATRegistration'] = $szVATRegistration;
                                $courierResultGrossListAry[$ctr]['iVatPriceCurrencyROE']=$courierResultGrossListAry[$ctr]['fExchangeRate'];
                                $courierResultGrossListAry[$ctr]['fVATPercentage'] = $fVatRate;
                                $courierResultGrossListAry[$ctr]['fTotalVat'] = round((float)$courierResultGrossListAry[$ctr]['fDisplayPrice'])*.01*$fVatRate;
                                
                                $fVatNoMarkup = round((float)($fPriceNoMarkup* $fVatRate * .01),2); 
                                
                                $szCalculationLogString_5 .= " <br>VAT(%): ".$fVatRate; 
                                $szCalculationLogString_5 .= " <br> VAT Formula : ".round((float)$courierResultGrossListAry[$ctr]['fDisplayPrice'])." * 0.01 * ".$fVatRate; 
                                $szCalculationLogString_5 .= " <br> VAT: ".$courierResultGrossListAry[$ctr]['fTotalVat']; 
                                
                                $szCalculationLogString_5 .= " <br> VAT Formula (without markup): ".$fPriceNoMarkup." * 0.01 * ".$fVatRate; 
                                $szCalculationLogString_5 .= " <br> VAT(without markup): ".$fVatNoMarkup; 
                            }    
                            else
                            {
                                $szCalculationLogString_5 .= "<br> Customer Type : Business ";
                                $szCalculationLogString_5 .= "<br> VAT Applicable : No ";
                            }
                        /*}
                        else
                        {
                            $szCalculationLogString_5 .= " <br> NO VAT APPLIED OUT FWD: country: ".$courierResultGrossListAry[$ctr]['idForwarderCountry']; 
                        }*/
                        
                        //$fCustomerCurrencyPrice_XE = $courierResultGrossListAry[$ctr]['fDisplayPrice'] - $courierResultGrossListAry[$ctr]['fTotalMarkupPrice'] ;
                        $fCustomerCurrencyPrice_XE = $fPriceNoMarkup; 
                        $courierResultGrossListAry[$ctr]['fVatNoMarkup'] = $fVatNoMarkup;
                        
                        if($idForwarderCurrency==$idCurrency)
                        { 
                            $courierResultGrossListAry[$ctr]['fForwarderCurrencyPrice'] = round((float)$courierResultGrossListAry[$ctr]['fDisplayPrice'],6);
                            $courierResultGrossListAry[$ctr]['fForwarderCurrencyPrice_XE'] = round((float)$fCustomerCurrencyPrice_XE,6);
                        }
                        else
                        { 
                            
                            /*
                            * converting price from customer currency to forwarder currency.
                            * to findout price i first convert customer price in USD then convert it into forwarder currency 
                            */
                            $tempCustomerUSDPrice = 0;
                            if($courierResultGrossListAry[$ctr]['idCurrency']==1)
                            {
                                /*
                                * if customer price is in USD then no need of converstion
                                */
                                $tempCustomerUSDPrice = $courierResultGrossListAry[$ctr]['fDisplayPrice'] ;
                                $tempCustomerUSDPrice_XE = $fCustomerCurrencyPrice_XE ;
                            }
                            else
                            {
                                /*
                                * now converting customer price into USD
                                */												
                                $tempCustomerUSDPrice = $courierResultGrossListAry[$ctr]['fDisplayPrice'] * $courierResultGrossListAry[$ctr]['fExchangeRate'] ;
                                $tempCustomerUSDPrice_XE = $fCustomerCurrencyPrice_XE * $courierResultGrossListAry[$ctr]['fExchangeRate'] ;
                            }
                            if($resultAry[$ctr]['idForwarderCurrency']==1)
                            {
                                /*
                                * if forwarder price is in USD then no need of converstion
                                */
                                $courierResultGrossListAry[$ctr]['fForwarderCurrencyPrice'] =round((float)$tempCustomerUSDPrice,4);
                                $courierResultGrossListAry[$ctr]['fForwarderCurrencyPrice_XE'] = round((float)$tempCustomerUSDPrice_XE,4);
                            }
                            else
                            {
                                $tempCustomerUSDPrice = round((float)($tempCustomerUSDPrice/$courierResultGrossListAry[$ctr]['fForwarderCurrencyExchangeRate']),6);
                                $tempCustomerUSDPrice_XE = round((float)($tempCustomerUSDPrice_XE/$courierResultGrossListAry[$ctr]['fForwarderCurrencyExchangeRate']),6);
                                
                                $courierResultGrossListAry[$ctr]['fForwarderCurrencyPrice'] = round((float)$tempCustomerUSDPrice,4);
                                $courierResultGrossListAry[$ctr]['fForwarderCurrencyPrice_XE'] = round((float)$tempCustomerUSDPrice_XE,4);
                            }
                        } 
                        
                        $szCalculationLogString_5 .= " <br> Forwarder Price With Markup: ".$courierResultGrossListAry[$ctr]['szForworderCurrency']." ".$courierResultGrossListAry[$ctr]['fForwarderCurrencyPrice'];
                        $szCalculationLogString_5 .= " <br> Forwarder Price Without Markup: ".$courierResultGrossListAry[$ctr]['szForworderCurrency']." ".$courierResultGrossListAry[$ctr]['fForwarderCurrencyPrice_XE'];
                        //$courierResultGrossListAry[$ctr]['fForwarderCurrencyPriceWithoutVat_XE'] = round((float)$tempCustomerUSDPrice_XE,4);
                        
                        if($resultAry[$ctr]['idForwarderCurrency']==1)
                        {
                            $fTotalSelfInvoiceAmount = round((float)$fTotalSelfInvoiceAmountUSD,4);
                        }
                        else if($courierResultGrossListAry[$ctr]['fForwarderCurrencyExchangeRate']>0)
                        {
                            $fTotalSelfInvoiceAmount = round((float)($fTotalSelfInvoiceAmountUSD/$courierResultGrossListAry[$ctr]['fForwarderCurrencyExchangeRate']),4); 
                        }
                        $szCalculationLogString_5 .= " <br> Self invoice amount (USD): ".$fTotalSelfInvoiceAmountUSD;
                        $szCalculationLogString_5 .= " <br> Self invoice amount(Including Referral Fee): ".$fTotalSelfInvoiceAmount;
                        
                        $fVatPriceUSD = $courierResultGrossListAry[$ctr]['fTotalVat'] * $courierResultGrossListAry[$ctr]['fExchangeRate'];
                        $fVatPriceForwarderCurrency = 0;
                        if($courierResultGrossListAry[$ctr]['fForwarderCurrencyExchangeRate']>0)
                        {
                            /*
                            * From Financial Version - 2 We are no longer adding VAT on forwarder amount.
                            */
                           // $fVatPriceForwarderCurrency = round((float)($fVatPriceUSD/$courierResultGrossListAry[$ctr]['fForwarderCurrencyExchangeRate']),6);
                        }
                        $fReferalPercentage = $courierResultGrossListAry[$ctr]['fReferalPercentage'];
                        //$courierResultGrossListAry[$ctr]['fReferalAmount'] = round((float)(($courierResultGrossListAry[$ctr]['fForwarderCurrencyPrice_XE'] + $fVatPriceForwarderCurrency) * .01 * $fReferalPercentage ),6);
                        $courierResultGrossListAry[$ctr]['fReferalAmount'] = round((float)(($courierResultGrossListAry[$ctr]['fTotalSelfInvoiceAmount']) * .01 * $fReferalPercentage ),6);
                        
                        //Deducting Referral Fee from fTotalSelfInvoiceAmount
                        //$fTotalSelfInvoiceAmount = $fTotalSelfInvoiceAmount - $courierResultGrossListAry[$ctr]['fReferalAmount'];
                        $courierResultGrossListAry[$ctr]['fTotalSelfInvoiceAmount'] = round((float)$fTotalSelfInvoiceAmount,4);
                        
                        $dtShippingDate = date('Y-m-d',strtotime($postSearchAry['dtTimingDate']));
                        if(__isWeekend($dtShippingDate))
                        {
                            $dtShippingDate = __nextMonday($dtShippingDate);
                        }
                        $courierResultGrossListAry[$ctr]['fTotalPriceCustomerCurrency'] = $courierResultGrossListAry[$ctr]['fDisplayPrice'];  
                        $courierResultGrossListAry[$ctr]['dtCutOffDate']= $dtShippingDate; 
                        
                        $courierResultGrossListAry[$ctr]['iCuttOffDate_time'] = strtotime($courierResultGrossListAry[$ctr]['dtCutOffDate']);
                        $courierResultGrossListAry[$ctr]['iAvailabeleDate_time'] = strtotime($courierResultGrossListAry[$ctr]['dtAvailableDate']);
                        //$days_between = ceil(abs($courierResultGrossListAry[$ctr]['iAvailabeleDate_time'] - $courierResultGrossListAry[$ctr]['iCuttOffDate_time']) / 86400); 
                        
                        $courierResultGrossListAry[$ctr]['idServiceTerms'] = $idServiceTerms;
                        $courierResultGrossListAry[$ctr]['szServiceTerm'] = $szServiceTerm;
                        $courierResultGrossListAry[$ctr]['idServiceType'] = __SERVICE_TYPE_DTD__;
                        
                        
                        $start = date("Y-m-d",strtotime($courierResultGrossListAry[$ctr]['dtCutOffDate']));
                        $end = date("Y-m-d",strtotime($courierResultGrossListAry[$ctr]['dtAvailableDate']));
                        $days_between = ceil(abs(strtotime($end) - strtotime($start)) / 86400); 
                        
                        $courierResultGrossListAry[$ctr]['iDaysBetween'] = $days_between;  
                        if($mode=='PARTNER_API_CALL')
                        { 
                            $szPartnerAlies = cPartner::$szGlobalPartnerAlies;
                            $idShipmentType = cPartner::$idGlobalShipmentType;
                            $courierResultGrossListAry[$ctr]['idShipmentType'] = $idShipmentType;
                            $szUniqueKey = $szServiceTerm."".$courierResultGrossListAry[$ctr]['idForwarder']."".$courierResultGrossListAry[$ctr]['idCourierProvider']."".$courierResultGrossListAry[$ctr]['idCourierProviderProductid'] ;
                            
                            if($iPrivateShipping==1)
                            {
                                $szUniqueKey .= "P"; //P. represents Private Customer
                            }
                            else
                            {
                                $szUniqueKey .= "B"; //B. represents Private Customer
                            }
                            $courierResultGrossListAry[$ctr]['unique_id'] = date("d")."".date("m")."T".$szPartnerAlies."".$szUniqueKey.mt_rand(10,99); 
                        } 
                        else
                        {
                            $szUniqueKey = $courierResultGrossListAry[$ctr]['idForwarder']."_".$courierResultGrossListAry[$ctr]['idCourierProvider']."_".$courierResultGrossListAry[$ctr]['idCourierProviderProductid'] ;
                            $courierResultGrossListAry[$ctr]['unique_id'] = $szUniqueKey;
                        }
                        
                        $szCalculationLogString_5 .= " <br> Forwarder currency: ".$courierResultGrossListAry[$ctr]['szForworderCurrency'];
                        $szCalculationLogString_5 .= " <br> Customer currency: ".$courierResultGrossListAry[$ctr]['szCurrency'];
                        $szCalculationLogString_5 .= "<br> Currency markup (".$fPriceMarkUp."%) – USD ".$fMarkupPriceUSD;
                        
                        $szCalculationLogString_5 .= "<br> Total sales price without VAT incl. currency markup – USD ".$courierResultArys['fShipmentUSDCost'] ;
                        $szCalculationLogString_5 .= "<br> Customer currency ROE - ".$fExchangeRate ; 
                        $szCalculationLogString_5 .= "<br> Total sales price without VAT customer currency – ".$courierResultGrossListAry[$ctr]['fDisplayPrice'] ;
                        
                        $idForwarder = $courierResultArys['idForwarder'];  
                        if(!empty($addedForwarderAry) && in_array($idForwarder,$addedForwarderAry))
                        {
                            $courierResultGrossListAry[$ctr]['fAvgRating'] = $forwarderRatingAry[$idForwarder]['fAvgRating'];
                            $courierResultGrossListAry[$ctr]['iNumRating'] = $forwarderRatingAry[$idForwarder]['iNumRating'];
                            $courierResultGrossListAry[$ctr]['iNumReview'] = $forwarderReviewsAry[$idForwarder]['iNumReview']; 
                        }
                        else
                        {
                            $forwarderRatingAry[$idForwarder] = $this->getForwarderAvgRating($idForwarder);
                            $forwarderReviewsAry[$idForwarder] = $this->getForwarderAvgReviews($idForwarder);
                            
                            $courierResultGrossListAry[$ctr]['fAvgRating'] = $forwarderRatingAry[$idForwarder]['fAvgRating'];
                            $courierResultGrossListAry[$ctr]['iNumRating'] = $forwarderRatingAry[$idForwarder]['iNumRating'];
                            $courierResultGrossListAry[$ctr]['iNumReview'] = $forwarderReviewsAry[$idForwarder]['iNumReview'];  
                            $addedForwarderAry[] = $idForwarder;
                        } 
                        $ctr++;  
                    } 
                    else
                    {
                        $szCalculationLogString_5 .= " - Not cheapest, disregard ";
                    }
                    $stepCounter++;
                }
            }           
            //echo count($courierServiceNewArr);
            $kCourierServices->saveSearchCourierProviderData($courierResultGrossListAry,$postSearchAry['szBookingRandomNum']);
            
            $szCalculationLogString = $szCalculationLogString_0." ".$szCalculationLogString_1." ".$szCalculationLogString_2." <br> ".$szCalculationLogString_3." <br><br> ".$szCalculationLogString_4."<br>".$szCalculationLogString_5."<br> ".$szCalculationLogString_6 ;
            $this->szCalculationLogString  = $szCalculationLogString ; 
            return $courierResultGrossListAry;  
        }
        
	function sliceArrayByForwarder($dataAry)
	{
            if(!empty($dataAry))
            { 
                $tempAry = array();
                $ctr = 0;
                $iMaxRecordPerForwarder =(int) $this->getManageMentVariableByDescription('__MAX_NUM_RECORD_PER_FORWARDER__');
                        
                foreach($dataAry as $dataArys)
                {
                    $counterAry[$dataArys['idForwardFrom']]  += 1 ;
                    if($counterAry[$dataArys['idForwardFrom']]>$iMaxRecordPerForwarder)
                    { 
                        continue;
                    } 
                    else if($dataArys['idForwardFrom']>0)
                    {
                        $ret_ary[$ctr] = $dataArys ;
                        $ctr++;
                    }	
                }
                return $ret_ary;
            }
            else
            {
                return array();
            }
	}
	
	function getHaulageDetails($data) 
	{
		if(!empty($data))
		{
		 /*
			if($data['idOriginCountry']!=$data['idWarehouseCountry'])
			{
				$query_and = "  ";
			}
			else
			{
				$query_and = " AND iCrossBorder='0' ";
			}
		 */
		if($data['iCrossBorder']==0)
		{
			$order_by = " ph.iUpToKM ASC,ph.iCrossBorder ASC";
		}
		else
		{
			$query_and = " AND ph.iCrossBorder = '1' " ;
			$order_by = " ph.iUpToKM ASC";
		}
			
			$query="
				SELECT
				  ph.id,
				  ph.fRate,
				  ph.fRateWM_Km,
				  ph.fMinRateWM_Km,
				  ph.idHaulageTransitTime,
				  ph.szHaulageCurrency,
				  htt.iHours iHaulageTransitTime
				FROM
					".__DBC_SCHEMATA_PRICING_HAULAGE__." ph
				INNER JOIN
					".__DBC_SCHEMATA_HAULAGE_TRANSIT_TIME__." htt
				ON
					htt.id = ph.idHaulageTransitTime	
				WHERE
					ph.idWarehouse = '".(int)$data['idWarehouse']."' 
				AND
				  ph.iUpToKM >= '".(float)$data['iDistance']."'
			    AND
			      ph.idDirection = '".(int)$data['iDirection']."' 
			    AND
			    	ph.iActive = '1'  
			      $query_and
			    ORDER BY
			    	".$order_by."
			    LIMIT
			    	0,1	
			";
			//echo "<br>".$query."<br>";
			/*
			if($data['idWarehouse']==26)
			{
				echo "<br>".$query."<br>";
			}
			*/
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$haulageDetailAry = array();
					while($row=$this->getAssoc($result))
					{
						if($row['szHaulageCurrency']==1) // USD
						{
							$houlageData['fTotalCargo'] = $fPriceFactor;
							$totalPrice = $data['iDistance'] * $row['fRateWM_Km'] * $data['fTotalCargoMeasure'];
							if($totalPrice < $row['fMinRateWM_Km'])
							{
								$totalPrice = $row['fMinRateWM_Km'];
							}
							$totalPrice = $totalPrice + $row['fRate'];
							//$fBookingRate = $row['fRate'] * $data['fTotalCargoMeasure'];
							$fUSDValue = 1.00 ;
						}
						else
						{
							/**
							* getting USD factor
							*/  
							$fUSDValue = $this->getCurrencyFactor($row['szHaulageCurrency']);
							
							/**
							* converting price to USD
							*/
							/*		
							if($data['idWarehouse']==26)
							{
								echo "<br> total distance ".$data['iDistance'] ;
								echo "<br> total ratewm_ ".$row['fRateWM_Km'] ;
								echo "<br>exchange currency  ".$fUSDValue;
								echo "<br> total fTotalCargoMeasure ".$data['fTotalCargoMeasure'] ;
							}	
							*/
							$totalPrice = $data['iDistance'] * $row['fRateWM_Km'] * $data['fTotalCargoMeasure'];
							if($totalPrice < $row['fMinRateWM_Km'])
							{
								$totalPrice = $row['fMinRateWM_Km'] ;
							}	
							$totalPrice	= ($totalPrice + $row['fRate'] ) * $fUSDValue ;	
						}
						$iHaulageTransitTime = $row['iHaulageTransitTime'];
						$res_ary = $row;
					}
										
					$res_ary['fTotalHaulgePrice'] = round((float)$totalPrice,6);
					$res_ary['iHaulageTransitTime'] = $iHaulageTransitTime ;
					$res_ary['fExchangeRate'] = round((float)$fUSDValue,6);
					return $res_ary ;
				}
				else
				{
				/*
					$query="
						SELECT
						  ph.id,
						  ph.fRate,
						  ph.fRateWM_Km,
						  ph.fMinRateWM_Km,
						  ph.idHaulageTransitTime,
						  ph.szHaulageCurrency,
						  htt.iHours iHaulageTransitTime
						FROM
							".__DBC_SCHEMATA_PRICING_HAULAGE__." ph
						INNER JOIN
							".__DBC_SCHEMATA_HAULAGE_TRANSIT_TIME__." htt
						ON
							htt.id = ph.idHaulageTransitTime	
						WHERE
							ph.idWarehouse = '".(int)$data['idWarehouse']."'
					    AND
					      ph.idDirection = '".(int)$data['iDirection']."' 
					      ".$query_and."
					    ORDER BY
					    	ph.iUpToKM  DESC
					    LIMIT
					    	0,1	
					";
					/*
				if($data['idWarehouse']==25)
				{
					echo "<br>".$query."<br>";
				}	

				if($result=$this->exeSQL($query))
			    {
					if($this->iNumRows>0)
					{
						$haulageDetailAry = array();
						while($row=$this->getAssoc($result))
						{
							if($row['szHaulageCurrency']==1)  // USD
							{
								$totalPrice = $data['iDistance'] * ($row['fRateWM_Km']+$row['fRate']) * $data['fTotalCargoMeasure'];
								if($totalPrice < $row['fMinRateWM_Km'])
								{
									$totalPrice = $row['fMinRateWM_Km'] ;
								}
								$fBookingRate = $row['fRate'] * $data['fTotalCargoMeasure'];
								$fUSDValue = 1.00 ;
							}
							else
							{
								/**
								* getting USD factor
								  
								$fUSDValue = $this->getCurrencyFactor($row['szHaulageCurrency']);
								
								/**
								* converting price to USD
										
								
								$totalPrice = $data['iDistance'] * (($row['fRateWM_Km']+$row['fRate'])*$fUSDValue) * $data['fTotalCargoMeasure'];
								if($totalPrice < ($row['fMinRateWM_Km']*$fUSDValue))
								{
									$totalPrice = $row['fMinRateWM_Km']*$fUSDValue ;
								}						
								$fBookingRate = $row['fRate']*$fUSDValue* $data['fTotalCargoMeasure'] ;
							}
							//$totalPrice += $fBookingRate;
							$iHaulageTransitTime = $row['iHaulageTransitTime'];
							$res_ary = $row;
						}						
						$res_ary['fTotalHaulgePrice'] = $totalPrice ;
						$res_ary['iHaulageTransitTime'] = $iHaulageTransitTime ;
						$res_ary['fExchangeRate'] = $fUSDValue ;
						return $res_ary ;
					}
					else
					{
						return array();
					}
			      }
			      else
			      {
			      	 return array();
			      }
			      */
				}			
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return array();
		}
	}
	
	function addTempSearchData($res_ary,$idBooking=false,$iSearchResultType=false,$idPartner=false,$szToken=false)
	{
            if(!empty($res_ary) || $idPartner>0)
            {
                $serialize_data = serialize($res_ary);
                $forwarder_data = serialize($this->forwarderAry); 
                $szCourierCalculationLogString = $this->szCourierCalculationLogString; 
                $idTemp = session_id(); 
                
                if($idPartner>0)
                {
                    $query_where = " idPartner = '".(int)$idPartner."' AND szReferenceToken = '".  mysql_escape_custom($szToken)."' ";
                }
                else
                {
                    $query_where = " idBooking = '".(int)$idBooking."'	";
                }
                
                $query_sel = "
                    SELECT
                        id
                    FROM
                        ".__DBC_SCHEMATA_TEMP_SEARCH_RESULT__."
                    WHERE
                       $query_where 	
                ";
                        
                if($result_sel = $this->exeSQL($query_sel))
                {
                    if(strlen($serialize_data)>4000000000)
                    {
                        $serialize_data = substr($serialize_data, 0,4000000000);
                    } 
                    if(strlen($szCourierCalculationLogString)>4000000000)
                    {
                        $szCourierCalculationLogString = substr($szCourierCalculationLogString, 0,4000000000);
                    }
                    if($this->iNumRows>0)
                    {
                        $query = "
                            UPDATE
                                ".__DBC_SCHEMATA_TEMP_SEARCH_RESULT__."
                            SET
                              szSerializeData = '".mysql_escape_custom(trim($serialize_data))."',
                              szForwarderData = '".mysql_escape_custom(trim($forwarder_data))."',
                              szCourierCalculationLogString = '".mysql_escape_custom(trim($szCourierCalculationLogString))."',
                              iSearchResultType = '".mysql_escape_custom(trim($iSearchResultType))."', 
                              dtCreatedOn = now()
                            WHERE
                                $query_where
                        ";
                    }
                    else
                    {			
                        $query="
                            INSERT INTO
                                ".__DBC_SCHEMATA_TEMP_SEARCH_RESULT__."
                            (
                                idUser,
                                idBooking,
                                idTempSearch,
                                szSerializeData,
                                szForwarderData,
                                iSearchResultType,
                                szCourierCalculationLogString,
                                idPartner,
                                szReferenceToken,
                                dtCreatedOn				
                            )
                            VALUES
                            (
                                '".(int)$_SESSION['user_id']."',
                                '".(int)$idBooking."',
                                '".mysql_escape_custom(trim($idTemp))."',
                                '".mysql_escape_custom(trim($serialize_data))."',
                                '".mysql_escape_custom(trim($forwarder_data))."',
                                '".mysql_escape_custom(trim($iSearchResultType))."',
                                '".mysql_escape_custom(trim($szCourierCalculationLogString))."',
                                '".(int)$idPartner."',
                                '".mysql_escape_custom(trim($szToken))."',
                                now()
                            )
                        ";
                    }
                    //echo $query ;
                    if($result=$this->exeSQL($query))
                    {
                        return true ;
                    }
                    else
                    {
                        $this->error = true;
                        $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                        $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                        return false;
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
	
	function addTempSearchData_forwarder($res_ary,$mode=false)
	{
            if(!empty($res_ary))
            {
                $serialize_data = serialize($res_ary);
                $forwarder_data = serialize($this->forwarderAry);
                if($mode=='MANAGEMENT_TRY_IT_OUT')
                {
                    $idTemp = session_id()."_".md5(time()."_".mt_rand(0,10));
                }
                else
                {
                    if(!empty($_SESSION['forwarder_temp_search_id']))
                    {
                        $idTemp = $_SESSION['forwarder_temp_search_id'] ;
                    }
                    else
                    {
                        $idTemp = session_id();	
                    }
                }

                $query_sel = "
                        SELECT
                            id
                        FROM
                            ".__DBC_SCHEMATA_TEMP_SEARCH_RESULT__."
                        WHERE
                            idTempSearch = '".(int)$idTemp."'		
                ";

                if($result_sel = $this->exeSQL($query_sel))
                {
                    if(strlen($serialize_data)>4000000000)
                    {
                        $serialize_data = substr($serialize_data, 0,4000000000);
                    }
                    if($this->iNumRows>0)
                    { 
                        $query = "
                            UPDATE
                                ".__DBC_SCHEMATA_TEMP_SEARCH_RESULT__."
                            SET
                              szSerializeData = '".mysql_escape_custom(trim($serialize_data))."',
                              szForwarderData = '".mysql_escape_custom(trim($forwarder_data))."',
                              dtCreatedOn=now()
                            WHERE
                                idTempSearch = '".mysql_escape_custom(trim($idTemp))."' 	
                        ";
                    }
                    else
                    {			
                        $query="
                            INSERT INTO
                                ".__DBC_SCHEMATA_TEMP_SEARCH_RESULT__."
                            (
                                idTempSearch,
                                szSerializeData,
                                szForwarderData,
                                dtCreatedOn				
                            )
                            VALUES
                            (
                                '".mysql_escape_custom(trim($idTemp))."',
                                '".mysql_escape_custom(trim($serialize_data))."',
                                '".mysql_escape_custom(trim($forwarder_data))."',
                                now()
                            )
                        ";
                    }
                    //echo $query ;
                    if($result=$this->exeSQL($query))
                    {
                        if($mode=='MANAGEMENT_TRY_IT_OUT')
                        {
                            $_SESSION['management_temp_search_id'] = $idTemp ;
                        }
                        else
                        {
                            $_SESSION['forwarder_temp_search_id'] = $idTemp ;
                        } 
                        return true ;
                    }
                    else
                    {
                            $this->error = true;
                            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                            return false;
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
        
	function getSearchedDataFromTempData($idBooking=false,$idTemp=false,$dtCreatedDate=false,$bPartnerApiFlag=false)
	{
            if($idBooking>0 || !empty($idTemp) || !empty($dtCreatedDate) || $bPartnerApiFlag)
            {
                if($idBooking>0)
                {
                    $query_and = "WHERE
                        idBooking = '".(int)$idBooking."' " ;
                }
                if(!empty($idTemp))
                {
                    $query_and = "WHERE
                        idTempSearch = '".mysql_escape_custom(trim($idTemp))."' " ;
                }
                if(!empty($dtCreatedDate))
                {
                    $query_and = "WHERE
                        dtCreatedOn < '".mysql_escape_custom(trim($dtCreatedDate))."' " ;
                }
                
                if($bPartnerApiFlag)
                {
                    $idPartner = cPartner::$idGlobalPartner;
                    $szReferenceToken = cPartner::$szGlobalToken;
                    
                    $query_and = "
                        WHERE
                            idPartner = '".mysql_escape_custom(trim($idPartner))."' 
                        AND
                            szReferenceToken = '".mysql_escape_custom(trim($szReferenceToken))."' 
                    ";
                }

                $query_sel = "
                    SELECT
                        id,
                        szSerializeData,
                        szForwarderData,
                        szCourierCalculationLogString,
                        iSearchResultType,
                        idPartner,
                        szReferenceToken
                    FROM
                        ".__DBC_SCHEMATA_TEMP_SEARCH_RESULT__."
                        $query_and
                ";
                //echo $query_sel."<br>" ;
                if($result = $this->exeSQL($query_sel))
                {
                    if($this->iNumRows>0)
                    {
                        $ctr=0;
                        while($row = $this->getAssoc($result))
                        {
                            $searchAry=$row;
                        }
                        return $searchAry ;
                    }
                    else
                    {
                        return array();
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }   
            }
            else
            {
                return array();
            }
	}
        
	function get_warehouse_distance_search($data,$warehouse,$idWareHouse=false,$idForwarder=false,$idCountry=false)
	{ 
            if(!empty($data))
            {			
                if($data['iFromRequirementPage']==2 || $data['iFromRequirementPage']==3)
                {
                    if($warehouse == 'FROM')
                    {
                        $distanceMeasureAry['fTotalLatitude']=$data['fOriginLatitude'];
                        $distanceMeasureAry['fTotalLongitude']=$data['fOriginLongitude'];
                        
                        if((int)$idCountry <=0)
                        {
                            $distanceMeasureAry['idCountry'] = $data['idOriginCountry'] ;
                        } 
                        $distanceMeasureAry['iDirection'] = 1;
                    }
                    else if($warehouse=='TO')
                    {
                        $distanceMeasureAry['fTotalLatitude']=$data['fDestinationLatitude'];
                        $distanceMeasureAry['fTotalLongitude']=$data['fDestinationLongitude'];
                        
                        if((int)$idCountry <=0)
                        {
                            $distanceMeasureAry['idCountry'] = $data['idDestinationCountry'] ;
                        }
                        $distanceMeasureAry['iDirection'] = 2;
                    }
                } 
                else
                {
                    if(trim($data['szOriginCity'])==t($this->t_base_homepage.'fields/type_name'))
                    {
                        $data['szOriginCity'] = '';
                    }
                    if(trim($data['szOriginPostCode'])==t($this->t_base_homepage.'fields/optional') || trim($data['szOriginPostCode'])==t($this->t_base_homepage.'fields/type_code'))
                    {
                        $data['szOriginPostCode'] = '';
                    }			

                    if(trim($data['szDestinationCity'])==t($this->t_base_homepage.'fields/type_name'))
                    {
                        $data['szDestinationCity'] = '';
                    }
                    if(trim($data['szDestinationPostCode'])==t($this->t_base_homepage.'fields/optional') || trim($data['szDestinationPostCode'])==t($this->t_base_homepage.'fields/type_code'))
                    {
                        $data['szDestinationPostCode'] = '';
                    }

                    $distanceMeasureAry=array();	
                    if($warehouse == 'TO')
                    {
                        if(!empty($data['szDestinationPostCode']))
                        {
                            // getting laltitude and longitute by postcode 
                            $distanceMeasureAry = $this->getDistanceMeasure(false,$data['szDestinationPostCode'],$data['idDestinationCountry']);
                        }
                        else if(!empty($data['szDestinationCity']))
                        {
                            //getting laltitude and longitute by city
                            $distanceMeasureAry = $this->getDistanceMeasure($data['szDestinationCity'],false,$data['idDestinationCountry']); 
                        }
                        if((int)$idCountry <=0)
                        {
                            $distanceMeasureAry['idCountry'] = $data['idDestinationCountry'] ;
                        }
                        $distanceMeasureAry['iDirection'] = 2;
                    }			
                    if($warehouse=='FROM')
                    {
                        $idCountry = $data['idOriginCountry'] ;
                        if(!empty($data['szOriginPostCode']))
                        {
                            // getting laltitude and longitute by postcode 
                            $distanceMeasureAry = $this->getDistanceMeasure(false,$data['szOriginPostCode'],$data['idOriginCountry']);
                        }
                        else if(!empty($data['szOriginCity']))
                        {
                            //getting laltitude and longitute by city
                            $distanceMeasureAry = $this->getDistanceMeasure($data['szOriginCity'],false,$data['idOriginCountry']);					
                        }
                        if((int)$idCountry <=0)
                        {
                                $distanceMeasureAry['idCountry'] = $data['idOriginCountry'] ;
                        }

                        $distanceMeasureAry['iDirection'] = 1;
                    } 
                }
                
                if((int)$idWareHouse>0)
                {
                    $distanceMeasureAry['idWareHouse'] = $idWareHouse ;
                }
                if((int)$idForwarder>0)
                {
                    $distanceMeasureAry['idForwarder'] = $idForwarder ;
                }
                if((int)$idCountry>0)
                {
                    $distanceMeasureAry['idCountry'] = $idCountry ;
                }
                $distanceMeasureAry['idServiceType'] = $data['idServiceType'] ; 
                
                $distanceMeasureAry['idSearchedOriginCountry'] = $data['idOriginCountry'] ;
                $distanceMeasureAry['idSearchedDestinationCountry'] = $data['idDestinationCountry'] ;
                $distanceMeasureAry['iQuickQuote'] = $data['iQuickQuote'] ;
                $distanceMeasureAry['iPrivateShipping'] = $data['iPrivateShipping'] ;
                //$distanceMeasureAry['iWarehouseType'] = $data['iWarehouseType'] ; 
                        
                if($data['iRailTransport']>0)
                {
                    $distanceMeasureAry['iRailTransport'] = 1;
                }
                $lclWarehousesAry =array();
                $lclWarehousesAry= $this->getDistanceFromAllWareHouses($distanceMeasureAry,$warehouse); 
                        
                if(self::$szServiceCalculationMode=='MANAGEMENT_TRY_IT_OUT')
                {
                    /*
                    * This function will be called from Management try it out page just to check steps followed in finding of warehouses
                    */
                    $idServiceType = $distanceMeasureAry['idServiceType'];
                    $iDirection = $distanceMeasureAry['iDirection'];

                    if($iDirection==1 && ($idServiceType==__SERVICE_TYPE_DTP__ || $idServiceType==__SERVICE_TYPE_DTW__ || $idServiceType==__SERVICE_TYPE_DTD__ || $distanceMeasureAry['iRailTransport']==1))
                    {	
                        $bHaulageRequired = true; 
                    }
                    else if($iDirection==2 && ($idServiceType==__SERVICE_TYPE_PTD__ || $idServiceType==__SERVICE_TYPE_WTD__ || $idServiceType==__SERVICE_TYPE_DTD__ || $distanceMeasureAry['iRailTransport']==1))
                    {	  
                        $bHaulageRequired = true;
                    }  
                    $kService = new cServices();
                    $testWarehousesAry =array();
                    $testWarehousesAry= $kService->findWarehousesForHaulageServices($distanceMeasureAry,$warehouse); 

                    self::$szTestCalculationLogs .= $kService->szTestLCLCalculationLogs;   
                }  
                return $lclWarehousesAry;
            }		
	}
	
	function getAllWareHouses($flag='',$idForwarder=false)
	{
            $sql='';
            if($flag=='updateUTCOffSet')
            {
                $sql="
                    WHERE
                        date(dtUTCOffsetUpdated)='".date('Y-m-d')."'
                ";				
            }
            if($flag=='forwarder_warehouse')
            {
                $sql="
                    WHERE
                        whs.idForwarder='".(int)$idForwarder."'
                    AND
                        whs.iActive = '1'	
                ";				
            }
            $query="
                SELECT
                    whs.id,
                    whs.szLatitude,
                    whs.szLongitude,
                    whs.szUTCOffset,
                    whs.szWareHouseName,
                    whs.szAddress,
                    whs.szAddress2,
                    whs.szAddress3,
                    whs.szPostCode,
                    whs.szCity,
                    whs.szState,
                    whs.idCountry,
                    con.szCountryName
                FROM
                    ".__DBC_SCHEMATA_WAREHOUSES__." whs
                INNER JOIN 
                    ".__DBC_SCHEMATA_COUNTRY__." con
                ON
                    con.id = whs.idCountry	
                ".$sql."	
                ORDER BY
                    con.szCountryName ASC, whs.szCity ASC, whs.szWareHouseName ASC
            ";
            //echo $query."<br><br>";
            if($result = $this->exeSQL($query))
            {
                $wareHouseAry = array();
                $from_ctr=0;
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    $wareHouseAry[$ctr] = $row;
                    $ctr++;
                }
                return $wareHouseAry;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
	
	function getDistanceMeasure($szCity=false,$szPostCode=false,$idCountry)
	{
		if(!empty($szCity) || !empty($szPostCode))
		{
			if(!empty($szCity))
			{
				//$query_and = " AND szCity ='".mysql_escape_custom(trim($szCity))."'"; 
				
				$query_and.= "
						AND 
					(
						szCity = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity1 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity2 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity3 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity4 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity5 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity6 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity7 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity8 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity9 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity10 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szRegion1 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion2 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion3 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion4 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szPostCode = '".mysql_escape_custom(trim($szCity))."'	
					)		
				";
			}
			if(!empty($szPostCode))
			{
				$query_and .= " AND szPostCode = '".mysql_escape_custom(trim($szPostCode))."'"; 
			}
			
			$query="
				SELECT
                                    count(id) iCount,
                                    sum(szLat) fTotalLatitude,
                                    sum(szLng) fTotalLongitude
				FROM
                                    ".__DBC_SCHEMATA_POSTCODE__."	
				WHERE
                                    idCountry = '".(int)$idCountry."'
                                    $query_and
			";
			//echo "<br>".$query."<br>";
			/*
			$filename = __APP_PATH_ROOT__."/logs/debug.log";
				
			$strdata=array();
			$strdata[0]=" \n\n *** fetching latitute longitute \n\n";
			$strdata[1] = $query."\n\n"; 
			file_log(true, $strdata, $filename);
			*/
			if($result=$this->exeSQL($query))
			{
				$ret_ary = array();
				while($row=$this->getAssoc($result))
				{
					if($row['iCount']>0)
					{
						$totalLat = $row['fTotalLatitude'] / $row['iCount'] ;
						$totalLong = $row['fTotalLongitude'] / $row['iCount'] ;
					}	
				}
				$ret_ary['fTotalLatitude'] =  $totalLat ;
				$ret_ary['fTotalLongitude'] =  $totalLong ;
				return $ret_ary ;
			}
		}
	}
        function getDistanceFromAllWareHouses($distanceMeasureAry,$szWarehouseSearchType=false)
	{ 
            if(!empty($distanceMeasureAry))
            {
                $arrDescriptions = array("'__TOTAL_NUM_WAREHOUSE__'","'__TOTAL_NUM_WAREHOUSE_D_SERVICE__'","'__MAX_WAREHOUSE_DISTANCE_TO_BE_CONSIDER__'","'__MAX_DTD_DISTANCE_TO_BE_CONSIDER__'"); 
                $bulkManagemenrVarAry = array();
                $bulkManagemenrVarAry = $this->getBulkManageMentVariableByDescription($arrDescriptions);      
                        
                $bDTDTradesFlag = false;
                $kCourierServices = new cCourierServices(); 
                if($kCourierServices->checkFromCountryToCountryExists($distanceMeasureAry['idSearchedOriginCountry'],$distanceMeasureAry['idSearchedDestinationCountry'],true))
                { 
                    $bDTDTradesFlag = true;
                }
                $idServiceType = $distanceMeasureAry['idServiceType'];
                
                if($distanceMeasureAry['iRailTransport']==1)
                {
                    $maxWarehouseDistance = $bulkManagemenrVarAry['__MAX_DTD_DISTANCE_TO_BE_CONSIDER__'];  
                    $iWareHouseLimit = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE_D_SERVICE__'];
                }
                else if($szWarehouseSearchType=='FROM')
                {
                    if($idServiceType==__SERVICE_TYPE_DTP__ || $idServiceType==__SERVICE_TYPE_DTW__ || $idServiceType==__SERVICE_TYPE_DTD__)
                    {
                        $maxWarehouseDistance = $bulkManagemenrVarAry['__MAX_DTD_DISTANCE_TO_BE_CONSIDER__'];  
                        $iWareHouseLimit = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE_D_SERVICE__'];
                    }
                    else
                    {
                        $maxWarehouseDistance = $bulkManagemenrVarAry['__MAX_WAREHOUSE_DISTANCE_TO_BE_CONSIDER__']; 
                        $iWareHouseLimit = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE__'];
                    }
                }
                else if($szWarehouseSearchType=='TO')
                {
                    if($idServiceType==__SERVICE_TYPE_PTD__ || $idServiceType==__SERVICE_TYPE_WTD__ || $idServiceType==__SERVICE_TYPE_DTD__)
                    {
                        $maxWarehouseDistance = $bulkManagemenrVarAry['__MAX_DTD_DISTANCE_TO_BE_CONSIDER__'];  
                        $iWareHouseLimit = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE_D_SERVICE__'];
                    }
                    else
                    {
                        $maxWarehouseDistance = $bulkManagemenrVarAry['__MAX_WAREHOUSE_DISTANCE_TO_BE_CONSIDER__']; 
                        $iWareHouseLimit = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE__'];
                    }
                }
                else if($bDTDTradesFlag)
                {
                    //If selected trade is added on Management /dtdTrades/ screen then we use this distance 
                    $maxWarehouseDistance = $bulkManagemenrVarAry['__MAX_DTD_DISTANCE_TO_BE_CONSIDER__'];  
                    $iWareHouseLimit = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE_D_SERVICE__'];
                }
                else
                {
                    $maxWarehouseDistance = $bulkManagemenrVarAry['__MAX_WAREHOUSE_DISTANCE_TO_BE_CONSIDER__']; 
                    $iWareHouseLimit = $bulkManagemenrVarAry['__TOTAL_NUM_WAREHOUSE__'];
                }
                
                $query_and = '';
		if((int)$distanceMeasureAry['idWareHouse'])
		{
                    /*
                    * This code will called when we recalculate the LCL prices
                    */
                    $query_and = " AND whs.id = '".(int)$distanceMeasureAry['idWareHouse']."'" ;
		}
                if((int)$distanceMeasureAry['iWarehouseType']>0)
		{ 
                    $query_and = " AND whs.iWarehouseType = '".(int)$distanceMeasureAry['iWarehouseType']."'" ;
		}
                
		if((int)$distanceMeasureAry['idForwarder']>0)
		{
                    /*
                    * This code will be from Forwarder Try-it-out
                    */
                    $query_and .= " AND fbd.iAgreeTNC IN (0,1)" ;
		}
		else
		{
                    $query_and .= " AND fbd.isOnline = '1' AND fbd.iAgreeTNC = '1' " ;
		}
		$iDirection = $distanceMeasureAry['iDirection'];
                        
		if($_SESSION['user_id']>0)
		{
                    $kUser = new cUser();
                    $kForwarder = new cForwarder();
                    $kUser->getUserDetails($_SESSION['user_id']);
                    $idForwarderAry = $kForwarder->getAllNonAcceptedForwarderByEmail($kUser->szEmail);
                    if(!empty($idForwarderAry))
                    {
                        $query_and .=" AND whs.idForwarder NOT IN (".implode(',',$idForwarderAry).")";
                    }
		}
                if($iDirection==1)
                {
                    $szImportExport = "Export";
                }
                else
                {
                    $szImportExport = "Import";
                }
                
                $this->szLCLCalculationLogs .= "<br><br><u> Fetching warehouses for ".$szImportExport.": </u>";
		
		if((int)$distanceMeasureAry['idCountry'])
		{
                    //$query_and = " AND whs.idCountry = '".(int)$distanceMeasureAry['idCountry']."'" ;
		}
                
		/*
		GetDistance('".$distanceMeasureAry['fTotalLatitude'].",".$distanceMeasureAry['fTotalLongitude']."',CONCAT_WS(',',whs.szLatitude,whs.szLongitude))
		*/ 
                $globalCountryServicedAry = array();
		$idServicedCountry = $distanceMeasureAry['idCountry'];   
                $idServiceType = $distanceMeasureAry['idServiceType'];
                $iDirection = $distanceMeasureAry['iDirection'];
                $bHaulageRequired = false;
                        
                if($iDirection==1 && ($idServiceType==__SERVICE_TYPE_DTP__ || $idServiceType==__SERVICE_TYPE_DTW__ || $idServiceType==__SERVICE_TYPE_DTD__ || $distanceMeasureAry['iRailTransport']==1))
                {	
                    $bHaulageRequired = true;
                    $query_select = ", (SELECT count(hd.idCountry) FROM ".__DBC_SCHEMATA_HAULAGE_COUNTRY__." hd WHERE hd.idWarehouse = whs.id AND hd.idCountry = '".$idServicedCountry."' AND hd.idDirection = '1' AND hd.iActive='1') as iHaulageAvailabale ";
                }
                else if($iDirection==2 && ($idServiceType==__SERVICE_TYPE_PTD__ || $idServiceType==__SERVICE_TYPE_WTD__ || $idServiceType==__SERVICE_TYPE_DTD__ || $distanceMeasureAry['iRailTransport']==1))
                {	 
                    $query_select = ", (SELECT count(hd.idCountry) FROM ".__DBC_SCHEMATA_HAULAGE_COUNTRY__." hd WHERE hd.idWarehouse = whs.id AND hd.idCountry = '".$idServicedCountry."' AND hd.idDirection = '2' AND hd.iActive='1') as iHaulageAvailabale";
                    $bHaulageRequired = true;
                }
                $query_whs_select = ", (SELECT count(wc.id) FROM ".__DBC_SCHEMATA_WAREHOUSE_COUNTRIES__." wc WHERE wc.warehouseID = whs.id AND wc.countryID = '".(int)$idServicedCountry."') as iWTWServiceAvailabale ";
                        
                /*
                * If User has searched for D service then at least 1 record for the country shoud be in tblhaulagecountry
                * Else If user has searched for P or W service then a record for the country should be in tblwarehousecountries
                */
                if($bHaulageRequired)
                {
                    $query_having = " iHaulageAvailabale > 0 ";
                }
                else
                {
                    $kConfig = new cConfig();
                    $kConfig->loadCountry($idServicedCountry); 
                    $fFobDistance = $kConfig->fMaxFobDistance; 
                    
                    $this->szLCLCalculationLogs .= "<br> W/P Distance defined: ".$maxWarehouseDistance;
                    $this->szLCLCalculationLogs .= "<br> FOB distance for country: ".$idServicedCountry." is ".$fFobDistance;
                    
                    if($fFobDistance>$maxWarehouseDistance)
                    {
                        $maxWarehouseDistance = $fFobDistance;
                    } 
                    $query_having = " iWTWServiceAvailabale > 0 ";
                }
                
                if($distanceMeasureAry['iWarehouseType']==__WAREHOUSE_TYPE_AIR__)
                {
                    $szWarehouseSearchMode = "AIR";
                }
                else
                {
                    $szWarehouseSearchMode = "LCL";
                } 
                if($distanceMeasureAry['iRailTransport']==1)
                {
                    /*
                    * i. For all idFromWarehouse in tblrailtransport, where iActive = 1 AND isDeleted = 0, check in tblhaulagecountry, and take only those which has idDirection = 1 AND idCountry = “Shipper country”
                    * ii. For those rows in tblrailtransport, check idToWarehouse  in tblhaulagecountry, and take only those  which have idDirection = 2 AND idCountry = “Consignee country” 
                    */
                    if($iDirection==1)
                    {
                        $query_rail_and = " AND rt.idFromWarehouse = whs.id ";
                    }
                    else
                    {
                        $query_rail_and = " AND rt.idToWarehouse = whs.id ";
                    }
                    $query_select .= ", (SELECT count(rt.id) as iRailWhsCount FROM ".__DBC_SCHEMATA_RAIL_TRANSPORT__." rt WHERE rt.isDeleted = 0 AND iDTD='1' ".$query_rail_and.") as iRailWhsCounter ";
                    $query_having .= " AND iRailWhsCounter >0 ";
                    $szWarehouseSearchMode = "Rail";
                } 
                else if($idServiceType !=__SERVICE_TYPE_DTD__)
                {
                    if($iDirection==1)
                    {
                        $query_rail_and = " AND rt.idFromWarehouse = whs.id ";
                    }
                    else
                    {
                        $query_rail_and = " AND rt.idToWarehouse = whs.id ";
                    }
                    $query_select .= ", (SELECT count(rt.id) as iRailWhsCount FROM ".__DBC_SCHEMATA_RAIL_TRANSPORT__." rt WHERE rt.isDeleted = 0 AND iDTD='1' ".$query_rail_and.") as iRailWhsCounter ";
                    $query_having .= " AND iRailWhsCounter = 0 ";
                }
                
                $query_excluded_forwarder_select='';
                if($_SESSION['user_id']>0)
                {
                    $kUser = new cUser();
                    $kForwarder = new cForwarder();
                    $kUser->getUserDetails($_SESSION['user_id']);
                    $excludedTradeSearch['idOriginCountry'] = $distanceMeasureAry['idSearchedOriginCountry'];
                    $excludedTradeSearch['idDestinationCountry'] = $distanceMeasureAry['idSearchedDestinationCountry'];
                    $iPrivateShipping = $kUser->iPrivate;
                    if($kUser->iPrivate==1)
                    {
                        $excludedTradeSearch['iCustomerType'] = __COURIER_EXCLUDED_TRADE_CUSTOMER_TYPE_PRIVATE__;
                    }
                    else
                    {
                        $excludedTradeSearch['iCustomerType'] = __COURIER_EXCLUDED_TRADE_CUSTOMER_TYPE_BUSSINESS__;
                    } 
                    $kCourierServices = new cCourierServices(); 
                    $forwarderExcludedArr = $kCourierServices->isTradeAddedToExcludedLCLList($excludedTradeSearch,'',true); 
                    if(!empty($forwarderExcludedArr))
                    {
                        $forwarderExcludedStr=implode("','",$forwarderExcludedArr);
                        $query_excluded_forwarder_select ="AND fbd.id NOT IN ('".$forwarderExcludedStr."')";
                        $forwarderExcludedFullArr=$kCourierServices->isTradeAddedToExcludedLCLList($excludedTradeSearch,'',true,true); 
                        if(!empty($forwarderExcludedFullArr))
                        {
                            $forwarderExcludedFullStr=implode(", ",$forwarderExcludedFullArr);
                            $this->szLCLCalculationLogs .= " <br> Forwarder: ".$forwarderExcludedFullStr." <br> Trade: NOT OK  ";
                            $this->szLCLCalculationLogs .= " <br> Description: This trade is added in excluded trade service list. ";
                            //$this->szLCLCalculationLogs .= "<br> CFS: ".$row['szWareHouseName']." - Distance: ".ceil($row['distance'])." - Not Included"; 
                        }
                    }
                } 
                else
                {
                    $iPrivateShipping = $distanceMeasureAry['iPrivateShipping'];
                }
                        
                if($iPrivateShipping==1)
                {
                    /*
                    * If search is being performed for private customer then we only picks the forwarder who are actually proving services for private customer as on forwarder section /privateCustomers/
                    */
                    $kForwarder = new cForwarder(); 
                    if($bDTDTradesFlag)
                    {
                        $szProduct = 'LTL';
                    }
                    else
                    {
                        $szProduct = 'LCL';
                    } 
                    $forwarderListAry = array();
                    $forwarderListAry = $kForwarder->listAllForwarderByProduct($szProduct); 
                    if(!empty($forwarderListAry))
                    {
                        
                    }
                        
                    /*
                    * Fetching list of forwarders, who is offering AIR services.
                    */
                    $productAry = array();
                    $productAry[0] = "AIR"; 
                    $airFreightForwarderListAry = array();
                    $airFreightForwarderListAry = $kForwarder->listAllForwarderByProduct("AIR");
                    if(!empty($airFreightForwarderListAry) && !empty($forwarderListAry))
                    {
                        $query_and .=" AND ((whs.idForwarder IN (".implode(',',$forwarderListAry).") AND whs.iWarehouseType = '".__WAREHOUSE_TYPE_CFS__."') ";
                        $query_and .=" OR (whs.idForwarder IN (".implode(',',$airFreightForwarderListAry).") AND whs.iWarehouseType = '".__WAREHOUSE_TYPE_AIR__."') )";
                    }
                    else if(!empty($forwarderListAry))
                    {
                        $query_and .=" AND (whs.idForwarder IN (".implode(',',$forwarderListAry).") AND whs.iWarehouseType = '".__WAREHOUSE_TYPE_CFS__."') ";
                    }
                    else if(!empty($airFreightForwarderListAry))
                    {
                        $query_and .=" AND (whs.idForwarder IN (".implode(',',$airFreightForwarderListAry).") AND whs.iWarehouseType = '".__WAREHOUSE_TYPE_AIR__."') ";
                    }
                }
                if($distanceMeasureAry['iQuickQuote']!=1)
                {
                    //$szSpecialQuery = " AND distance < ".(int)$maxWarehouseDistance." ";
                } 
		$query="
                    SELECT
                        sup.*
                    FROM 
                        (
                            SELECT 
                               whs.id, 
                               whs.id as idWarehouse,
                               whs.szWareHouseName,
                               whs.idForwarder,
                               whs.szUTCOffset ,
                               whs.idCountry,
                               whs.szCity,
                               whs.szPostCode,
                               fbd.szLogo,
                               fbd.szDisplayName,
                               fbd.szForwarderAlias,
                               fbd.szCurrency,
                               cont.fStandardTruckRate,
                               cont.szCountryName,		
                               cont.szCountryISO,
                               cont.fMaxFobDistance,
                               whs.szLatitude,
                               whs.szLongitude,
                               whs.szContactPerson,
                               whs.szEmail,
                               whs.iWarehouseType, 
                               fbd.dtReferealFeeValid, 
                               fbd.idCountry as idForwarderCountry 
                               $query_select
                               $query_whs_select
                            FROM 
                                ".__DBC_SCHEMATA_WAREHOUSES__." whs
                            INNER JOIN
                                ".__DBC_SCHEMATA_COUNTRY__." cont
                            ON
                                cont.id = whs.idCountry				
                            INNER JOIN
                                ".__DBC_SCHEMATA_FORWARDERS__."	fbd
                            ON
                                fbd.id = whs.idForwarder 
                            WHERE
                                whs.iActive = '1'
                            AND
                                fbd.iActive = '1'
                                ".$query_excluded_forwarder_select."
                                ".$query_and."		
                            HAVING   
                                $query_having 
                        ) as sup 
		";	
//                echo $query;
//                die; 
                if($bHaulageRequired)
                {
                    $this->szLCLCalculationLogs .= "<br>WHS LIMIT: ".$iWareHouseLimit." <strong> Mode: ".$szWarehouseSearchMode."</strong>";
                }
                else
                {
                    $this->szLCLCalculationLogs .= "<br> Distance Range: ".$maxWarehouseDistance." WHS LIMIT: ".$iWareHouseLimit." <strong> Mode: ".$szWarehouseSearchMode."</strong>";
                } 
                $this->szLCLCalculationLogs .= "<br>".$query."<br>";
                $szWarehouseIncludedRowsLogs = '';
		if($result = $this->exeSQL($query))
		{
                    $wareHouseAry = array();
                    $warehouseIdsAry = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {  
                        $bEligibleWarehouse = true;
                        if($bHaulageRequired)
                        {
                            if($row['iHaulageAvailabale']>0)
                            {
                                $bEligibleWarehouse = true;
                            }
                            else
                            {
                                $bEligibleWarehouse = false;
                                $this->szLCLCalculationLogs .= "<br>Forwarder: ".$row['szDisplayName'].", CFS: ".$row['szWareHouseName']." - Distance: ".ceil($row['distance'])." - Not Included"; 
                                $this->szLCLCalculationLogs .= "<br> Description: No haulage defined ";
                            }
                        } 
                        if($bEligibleWarehouse)
                        {
                            /*
                            * If eleigible then we checks if the forwarder is offering AIR services or not, If not then we don't Include that warehouse in the list.
                            */
                            $idForwarder = $row['idForwarder'];
                            if($iPrivateShipping==1)
                            {
                                if($row['iWarehouseType']==__WAREHOUSE_TYPE_AIR__)
                                {
                                    /*
                                    *  If warehouse type is Air, then we checks if the Forwarder is actually offers the AIR Services for Private customer.
                                    */
                                    if(!empty($airFreightForwarderListAry) && !in_array($idForwarder,$airFreightForwarderListAry))
                                    {
                                        $bEligibleWarehouse = false;
                                        $this->szLCLCalculationLogs .= "<br>Forwarder: ".$row['szDisplayName'].", Airport warehouse: ".$row['szWareHouseName']." - Distance: ".ceil($row['distance'])." - Not Included"; 
                                        $this->szLCLCalculationLogs .= "<br> Description: Forwarder does not offers AIR services for private customer ";
                                    }
                                    else if(empty($airFreightForwarderListAry))
                                    {
                                        /*
                                        * This means we don't have any forwarder who is offering AIR services.
                                        */
                                        $bEligibleWarehouse = false;
                                        $this->szLCLCalculationLogs .= "<br>Forwarder: ".$row['szDisplayName'].", Airport warehouse: ".$row['szWareHouseName']." - Distance: ".ceil($row['distance'])." - Not Included"; 
                                        $this->szLCLCalculationLogs .= "<br> Description: Forwarder does not offers AIR services for private customer ";
                                    }
                                } 
                                else if($row['iWarehouseType']==__WAREHOUSE_TYPE_CFS__)
                                {
                                    /*
                                    *  If warehouse type is Air, then we checks if the Forwarder is actually offers the AIR Services for Private customer.
                                    */
                                    if(!empty($forwarderListAry) && !in_array($idForwarder,$forwarderListAry))
                                    {
                                        $bEligibleWarehouse = false;
                                        $this->szLCLCalculationLogs .= "<br>Forwarder: ".$row['szDisplayName'].", CFS: ".$row['szWareHouseName']." - Distance: ".ceil($row['distance'])." - Not Included"; 
                                        $this->szLCLCalculationLogs .= "<br> Description: Forwarder does not offers LCL services for private customer ";
                                    }
                                    else if(empty($forwarderListAry))
                                    {
                                        /*
                                        * This means we don't have any forwarder who is offering AIR services.
                                        */
                                        $bEligibleWarehouse = false;
                                        $this->szLCLCalculationLogs .= "<br>Forwarder: ".$row['szDisplayName'].", CFS: ".$row['szWareHouseName']." - Distance: ".ceil($row['distance'])." - Not Included"; 
                                        $this->szLCLCalculationLogs .= "<br> Description: Forwarder does not offers LCL services for private customer ";
                                    }
                                }
                            }
                        }
                        if($bEligibleWarehouse)
                        { 
                           //echo "<br> WHS ID:  ".$row['id']." Direc ".$iDirection ." Haul: ".$row['iHaulageAvailabale'] ; 
                            $warehouseIdsAry[] = $row['id'];
                            $wareHouseAry['idForwarderAry'][] = $row['idForwarder'];
                            if($distanceMeasureAry['iRailTransport']==1)
                            {
                                $wareHouseAry[$row['id']]['iRailTransport'] = 1;
                            } 
                            $wareHouseAry[$row['id']]['iDistance'] = ceil($row['distance']);
                            $wareHouseAry[$row['id']]['szWareHouseName'] = $row['szWareHouseName'];
                            $wareHouseAry[$row['id']]['idForwarder'] = $row['idForwarder'];
                            $wareHouseAry[$row['id']]['fStandardTruckRate'] = $row['fStandardTruckRate'];	
                            $wareHouseAry[$row['id']]['szLogo'] = $row['szLogo'];	
                            $wareHouseAry[$row['id']]['szCountry'] = $row['szCountryName'];		
                            $wareHouseAry[$row['id']]['szCity'] = $row['szCity'];	
                            $wareHouseAry[$row['id']]['idForwarder'] = $row['idForwarder'];	
                            $wareHouseAry[$row['id']]['szDisplayName'] = $row['szDisplayName'];
                            $wareHouseAry[$row['id']]['szForwarderAlias'] = $row['szForwarderAlias']; 
                            $wareHouseAry[$row['id']]['szUTCOffset'] = $row['szUTCOffset'];	
                            $wareHouseAry[$row['id']]['idCountry'] = $row['idCountry'];	
                            $wareHouseAry[$row['id']]['szPostCode'] = $row['szPostCode'];	
                            $wareHouseAry[$row['id']]['szCountryISO'] = $row['szCountryISO'];	
                            $wareHouseAry[$row['id']]['szEmail'] = $row['szEmail'];	
                            $wareHouseAry[$row['id']]['szContactPerson'] = $row['szContactPerson']; 
                            $wareHouseAry[$row['id']]['id'] = $row['id'];
                            $wareHouseAry[$row['id']]['szLatitude'] = $row['szLatitude'];	
                            $wareHouseAry[$row['id']]['szLongitude'] = $row['szLongitude'];
                            $wareHouseAry[$row['id']]['szForwarderCurrency'] = $row['szCurrency'];	
                            $wareHouseAry[$row['id']]['fReferalFee'] = $row['fReferalFee'];
                            $wareHouseAry[$row['id']]['dtReferealFeeValid'] = $row['dtReferealFeeValid'];
                            $wareHouseAry[$row['id']]['idForwarderCountry'] = $row['idForwarderCountry']; 
                            $wareHouseAry[$row['id']]['iWarehouseType'] = $row['iWarehouseType'];  

                            $forwarderCurrencyAry = array();
                            if($row['szCurrency']==1)
                            {
                                $wareHouseAry[$row['id']]['szForwarderCurrencyName'] = 'USD';
                                $wareHouseAry[$row['id']]['fForwarderCurrencyExchangeRate'] = 1.00;
                            }
                            else
                            {
                                if(!empty($alreadyUsedCurrency[$row['szCurrency']]))
                                {
                                    $forwarderCurrencyAry = $alreadyUsedCurrency[$row['szCurrency']];
                                }
                                else
                                {
                                    $forwarderCurrencyAry = $this->getCurrencyDetails($row['szCurrency']);		
                                    $alreadyUsedCurrency[$row['szCurrency']] = $forwarderCurrencyAry ;
                                } 
                                if(!empty($forwarderCurrencyAry))
                                {					
                                    $wareHouseAry[$row['id']]['szForwarderCurrencyName'] = $forwarderCurrencyAry['szCurrency'];
                                    $wareHouseAry[$row['id']]['fForwarderCurrencyExchangeRate'] = $forwarderCurrencyAry['fUsdValue'];
                                }
                            }  
                            $szWarehouseIncludedRowsLogs .= "<br><strong>Forwarder: ".$row['szDisplayName'].", CFS: ".$wareHouseAry[$row['id']]['szWareHouseName']." - Included </strong>"; 
                        }	
                        $ctr++; 
                    }
                    $iTotalNumWarehouseFound = count($warehouseIdsAry);
                    $this->szLCLCalculationLogs .= "<br><strong> Count(idWarehouse): ".$iTotalNumWarehouseFound.", Y: ".$iWareHouseLimit."</strong>";  
                    return $wareHouseAry;
		}
		else
		{
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
		} 
            } 
	}
	
        function getEligibleNearestWarehouses($warehouseIdAry,$distanceMeasureAry,$iWareHouseLimit)
        {
            if(!empty($warehouseIdAry))
            { 
                $iCount = 1;
                $iWhsCount = count($warehouseIdAry);
                if($iWhsCount>0)
                {
                    $query_or = " AND ( ";
                    foreach($warehouseIdAry as $warehouseIdArys)
                    {
                        if($warehouseIdArys>0)
                        {
                            $query_or .= " id = '".$warehouseIdArys."'";
                            if($iWhsCount!=$iCount)
                            { 
                                $query_or .= " OR "; 
                            }
                        } 
                        $iCount++;
                    }
                    $query_or .= " )";
                }
                $query_and = '';
                if($distanceMeasureAry['iWarehouseServiceSearch']==1)
                {
                    $query_and = " HAVING distance < ".(int)$distanceMeasureAry['maxWarehouseDistance'];
                }
                
                $query="
                    SELECT  
                        whs.id as idWarehouse,
                        (( 3959 * acos( cos( radians(".(float)$distanceMeasureAry['fTotalLatitude'].") ) * cos( radians( whs.szLatitude ) ) 
                            * cos( radians(whs.szLongitude) - radians(".(float)$distanceMeasureAry['fTotalLongitude'].")) + sin(radians(".(float)$distanceMeasureAry['fTotalLatitude'].")) 
                            * sin( radians(whs.szLatitude)))) * 1.609344) AS distance
                    FROM 
                        ".__DBC_SCHEMATA_WAREHOUSES__." whs 
                    WHERE
                        whs.iActive = '1'  
                        $query_or
                        $query_and
                    ORDER BY 
                        distance ASC
                    LIMIT
                        0, ".$iWareHouseLimit."
		";	
                $this->szWarehouseQueryLogs = $query;
                
//                echo $query;
//                die;  
		if($result = $this->exeSQL($query))
		{
                    $wareHouseAry = array();
                    $warehouseIdsAry = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {  
                        $warehouseIdsAry[$row['idWarehouse']] = $row;
                    }
                    return $warehouseIdsAry;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
        } 
	function getAllCountryServiceByWHS($idWarehouse)
	{
            if(!empty($idWarehouse))
            {
                $query="
                    SELECT
                        countryID
                    FROM
                        ".__DBC_SCHEMATA_WAREHOUSE_COUNTRIES__."
                    WHERE
                        warehouseID = '".(int)$idWarehouse."'
                ";
                //echo "<br> ".$query."<br />";
                if($result = $this->exeSQL($query))
                {
                    $countryServicedAry = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {
                        $countryServicedAry[$ctr] = $row['countryID'];
                        $ctr++;
                    }
                    return $countryServicedAry;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
	
	function getDistance($lat1, $lon1, $lat2, $lon2, $unit) 
	{ 
		  $theta = $lon1 - $lon2; 
		  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta)); 
		  $dist = acos($dist); 
		  $dist = rad2deg($dist); 
		  $miles = $dist * 60 * 1.1515;
		  $unit = strtoupper($unit);
		
		  if ($unit == "K") 
		  {
		    return round((float)($miles * 1.609344),2); 
		  }
		  else if ($unit == "N") 
		  {
		      return round((float)($miles * 0.8684),2);
		  }
		  else
		  {
		     return $miles;
		  }
	}
	
	function getForwarderLogo($idForwarder)
	{
		if($idForwarder>0)
		{
			$query="
				SELECT
					szLogo,
					szDisplayName
				FROM
					".__DBC_SCHEMATA_FROWARDER__."	
				WHERE
					id = '".(int)$idForwarder."'	
			";
			if($result = $this->exeSQL($query))
			{
				$row=$this->getAssoc($result);
				return $row;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			return false ;
		}
	}
	
	function getCargoFactor($idCargoMeasure)
	{
		if($idCargoMeasure>0)
		{
                    $kConfig = new cConfig();
                    $configLangArr=$kConfig->getConfigurationLanguageData('__TABLE_CARGO_MEASURE__','',true);
                
			$query = "
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_CARGO_MEASURE__."
				WHERE
					id = '".(int)$idCargoMeasure."'	
			";
			if($result=$this->exeSQL($query))
			{
				$row=$this->getAssoc($result);
                                 return $configLangArr[$row['id']]['fCmFactor'];
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{	
			return false;
		}
	}
	
	function getWeightFactor($idWeightMeasure)
	{
            if($idWeightMeasure>0)
            {
                $query = "
                    SELECT
                        fKgFactor 
                    FROM
                        ".__DBC_SCHEMATA_WEIGHT_MEASURE__."
                    WHERE
                        id = '".(int)$idWeightMeasure."'	
                ";
                if($result=$this->exeSQL($query))
                {
                    $row=$this->getAssoc($result);
                    return $row['fKgFactor'];
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
            else
            {
                $this->error = true;
//                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
//                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	} 
	function getCurrencyFactor($idCurrency)
	{
		if($idCurrency>0)
		{
			$query = "
				SELECT
					fUsdValue 	 
				FROM
					".__DBC_SCHEMATA_EXCHANGE_RATE__."
				WHERE
					idCurrency = '".(int)$idCurrency."'	
				ORDER BY
					id DESC
				LIMIT
					0,1		
			";
			//echo "<br /> ".$query ;
			if($result=$this->exeSQL($query))
			{
				$row=$this->getAssoc($result);
				if($row['fUsdValue']>0)
				{
					return $row['fUsdValue'];
				}
				else
				{
					return 0;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	function getCustomClearanceCost($idWareHouseTo,$idWareHouseFrom,$customClearAry)
	{
		if(!empty($customClearAry))
		{
			$idCC_str = implode(",",$customClearAry);
			$query_and = " AND szOriginDestination IN (".$idCC_str.")" ;
		}
		
		$query="
			SELECT
				fPrice,
				szCurrency
			FROM
				".__DBC_SCHEMATA_PRICING_CC__."	
			WHERE
				idWareHouseFrom = '".(int)$idWareHouseFrom."'	
			AND
				idWareHouseTo = '".(int)$idWareHouseTo."'
			".$query_and."
		";
		//echo "<br>".$query."<br> ";
		if($result = $this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$fTotalClearanceAmt = 0;
				while($row=$this->getAssoc($result))
				{
					if($row['szCurrency']==1) // USD
					{
						$fTotalClearanceAmt +=$row['fPrice'] ;
					}
					else
					{
						$fUsdFactor = $this->getCurrencyFactor($idCurrency);
						
						// converting values in USD 
						$fTotalClearanceAmt +=$row['fPrice'] * $fUsdFactor ;
					}
				}
				return $fTotalClearanceAmt ;
			}
			else
			{
				return 0.00 ;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
    function getCustomClearanceDetails($idWareHouseTo,$idWareHouseFrom,$iOriginDestination,$check_flag=false)
	{
		if($iOriginDestination>0)
		{
			$query_and = " AND cc.szOriginDestination ='".(int)$iOriginDestination."'" ;
		}
		
		$query="
			SELECT
				cc.fPrice,
				cc.szCurrency idCurrency,
				c.szCurrency as szCurrencyName	
			FROM
				".__DBC_SCHEMATA_PRICING_CC__."	cc
			INNER JOIN
				".__DBC_SCHEMATA_CURRENCY__." c
			ON
				cc.szCurrency = c.id	
			WHERE
				cc.idWareHouseFrom = '".(int)$idWareHouseFrom."'	
			AND
				cc.idWareHouseTo = '".(int)$idWareHouseTo."'
			AND
			  cc.iActive = '1'	
			".$query_and."
		";
		//echo $query ;
		if($result = $this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				if($check_flag)
				{
					return true;
				}
				else
				{
					$fTotalClearanceAmt = 0;
					$CustomClearAry=array();
					while($row=$this->getAssoc($result))
					{
						if($row['idCurrency']==1) // USD
						{
							$fTotalClearanceAmt = $row['fPrice'] ;
							$fUsdFactor = 1.00 ;
						}
						else
						{
							$fUsdFactor = $this->getCurrencyFactor($row['idCurrency']);						
							// converting values in USD 
							$fTotalClearanceAmt = $row['fPrice'] * $fUsdFactor ;
						}					
						$szCurrency = $row['idCurrency'] ;
						$szCurrencyName = $row['szCurrencyName'] ;
					}
					$CustomClearAry['fPrice'] = round((float)$fTotalClearanceAmt,6);
					$CustomClearAry['fExchangeRate'] = round((float)$fUsdFactor,6);	
					$CustomClearAry['idCurrency'] = $szCurrency ;		
					$CustomClearAry['szCurrency'] = $szCurrencyName ;		
					return $CustomClearAry ;
				}
			}
			else
			{
				return array() ;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
        
        function getBulkManageMentVariableByDescription($arrDescription)
	{
            if(!empty($arrDescription))
            {
                $szDescriptionStr = implode(",",$arrDescription);
                $query="
                    SELECT
                        szValue,
                        szDescription
                    FROM
                        ".__DBC_SCHEMATA_MANAGEMENT_VARIABLE__."
                    WHERE
                        szDescription IN (".$szDescriptionStr.")		
                ";
                //echo $query;
                if($result=$this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        $ret_ary = array();
                        while($row = $this->getAssoc($result))
                        {
                            $ret_ary[$row['szDescription']] = $row['szValue'];
                        }
                        return $ret_ary;
                    }
                    else
                    {
                        return false ;
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
	function getManageMentVariableByDescription($szDescription,$idLanguage=0)
	{
            if(!empty($szDescription))
            {
                $query="
                    SELECT
                        szValue
                    FROM
                        ".__DBC_SCHEMATA_MANAGEMENT_VARIABLE__."
                    WHERE
                        szDescription = '".mysql_escape_custom(trim($szDescription))."'		
                ";
                if((int)$idLanguage>0)
                {
                    $query .="
                        AND
                            iLanguage='".(int)$idLanguage."'
                    ";
                }
                //echo $query;
                if($result=$this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        $row=$this->getAssoc($result);
                        return $row['szValue'];
                    }
                    else
                    {
                        return false ;
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
	
	function getDescription($query)
	{
		if($query)
		{
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{	
					$tnc=array();
					while($row=$this->getAssoc($result))
					{
					$tnc[]=$row;
					}
					return $tnc;
				}
				else
				{
					return false ;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function selectQuery($var,$iLanguage=false)
	{
		if($iLanguage>0)
		{
			$query_select = " AND iLanguage = '".(int)$iLanguage."' ";
		}
		
		switch($var){
			case 1:
				$query="
				SELECT
					szHeading,
					szDescription
				FROM
					".__DBC_SCHEMATA_MANAGEMENT_TERMS__."
				WHERE
					iActive = '1'
				AND
					iOrderBy!=0	
				ORDER BY 
					iOrderBy			
					";
			break;
			case 2:
				$query="
				SELECT
					szHeading,
					szDescription
				FROM
					".__DBC_SCHEMATA_MANAGEMENT_FAQ__."
				WHERE
					iActive = '1'
				AND
					iOrderBy!=0	
				ORDER BY 
					iOrderBy			
					";
			break;
			case 3:
				$query="
				SELECT
					szHeading,
					szDescription,
					iLanguage
				FROM
					".__DBC_SCHEMATA_MANAGEMENT_FAQ_CUSTOMER__."
				WHERE
					iActive = '1'
				AND
					iOrderBy!=0		
					$query_select
				ORDER BY 
					iOrderBy			
					";
			break;
			case 4:
				$query="
				SELECT
					szHeading,
					szDescription,
					iLanguage
				FROM
					".__DBC_SCHEMATA_MANAGEMENT_TERMS_CUSTOMER__."
				WHERE
					iActive = '1'
					$query_select
				AND
					iOrderBy!=0		
				ORDER BY 
					iOrderBy			
					";
			break;
		}
			$response=$this->getDescription($query);
			return $response;
			
	}
	function selectQueryPreview($var)
	{
		switch($var){
			case 1:
				$query="
				SELECT
					 szDraftHeading,
					 szDraftDescription
				FROM
					".__DBC_SCHEMATA_MANAGEMENT_TERMS__."
				WHERE
					iActive = '1'
				AND 
					(iOrderByDraft!='' OR iOrderByDraft!=0)	
				ORDER BY 
					iOrderByDraft			
					";
			break;
			case 2:
				$query="
				SELECT
					 szDraftHeading,
					 szDraftDescription
				FROM
					".__DBC_SCHEMATA_MANAGEMENT_FAQ__."
				WHERE
					iActive = '1'
				AND 
					(iOrderByDraft!='' OR iOrderByDraft!=0)	
				ORDER BY 
					iOrderByDraft			
					";
			break;
			case 3:
				$query="
				SELECT
					 szDraftHeading,
					 szDraftDescription
				FROM
					".__DBC_SCHEMATA_MANAGEMENT_FAQ_CUSTOMER__."
				WHERE
					iActive = '1'
				AND 
					(iOrderByDraft!='' OR iOrderByDraft!=0)		
				ORDER BY 
					iOrderByDraft			
					";
			break;
			case 4:
				$query="
				SELECT
					 szDraftHeading,
					 szDraftDescription
				FROM
					".__DBC_SCHEMATA_MANAGEMENT_TERMS_CUSTOMER__."
				WHERE
					iActive = '1'
				AND 
					(iOrderByDraft!='' OR iOrderByDraft!=0)		
				ORDER BY 
					iOrderByDraft			
					";
			break;
		}
			$response=$this->getDescription($query);
			return $response;
			
	}
	function getForwarderAvgRating($idForwarder,$month="")
	{
		if(!empty($idForwarder))
		{
			$col="";
			$whereQuery="";
			if(!empty($month) && $month!="")
			{
                            $today=date('Y-m-d');
                            $col .=",sum(iRating) as trating,iRating";

                            $prevdate = date("Y-m-d",strtotime(date("Y-m-d", strtotime($today)) . '-'.$month.' month'));


                            $whereQuery .="
                                    AND
                                            date(dtCompleted)>='".mysql_escape_custom($prevdate)."'
                            ";
			}
			else
			{ 
                            $month = $this->getManageMentVariableByDescription('__FORWARDER_RATING_REVIEW__');
                            $today=date('Y-m-d');
                            $prevdate = date("Y-m-d",strtotime(date("Y-m-d", strtotime($today)) . '-'.$month.' month'));

                            $whereQuery .="
                                    AND
                                            date(dtCompleted)>='".mysql_escape_custom($prevdate)."'
                            ";
			}
			$query="
				SELECT
                                    count(id) iNumRating,
                                    sum(iRating)/count(id) fAvgRating
                                    ".$col."
				FROM
                                    ".__DBC_SCHEMATA_RATING_REVIEW__."	
				WHERE
                                    idForwarder = '".(int)$idForwarder."'
				".$whereQuery."	
				HAVING
                                    iNumRating>0	
			";
			//echo $query ;
			if($idForwarder==5)
			{
				//echo "<br> ".$query ;
			}
			if($result=$this->exeSQL($query))
			{
				$row=$this->getAssoc($result) ;
				return $row;
			}
		}
	}
	
	function getCurrencyDetails($idCurrency=false,$idCurrencyAry=array(),$group_by=false,$date='',$iBooking=false)
	{
            if($idCurrency>0)
            {
                $query_and = " rate.idCurrency = '".(int)$idCurrency."' ";
                $query_limit = " LIMIT 0,1";
            }
            else if(!empty($idCurrencyAry))
            {
                $idCurrencyStr = implode(",",$idCurrencyAry);
                $query_and = " rate.idCurrency IN (".$idCurrencyStr.") ";
            }
            else if($iBooking)
            {
                $query_and = " cur.iBooking = '1' ";
            }
            else
            {
                return false; 
            }
            
            if(trim($date)!='')
            {
                $query_and .= "AND DATE(rate.dtDate)<='".mysql_escape_custom(date('Y-m-d',strtotime($date)))."'";
            }
            
            if($group_by)
            {
                $query_group_by = " GROUP BY cur.id ORDER BY rate.dtDate DESC ";
            }
            else
            {
                $query_group_by = " ORDER BY rate.id DESC ";
            }
            $query = "
                SELECT
                    rate.fUsdValue,
                    cur.szCurrency,
                    rate.idCurrency	 
                FROM
                    ".__DBC_SCHEMATA_EXCHANGE_RATE__." rate
                INNER JOIN
                    ".__DBC_SCHEMATA_CURRENCY__." cur
                ON
                    cur.id = rate.idCurrency	
                WHERE
                    $query_and 
                    $query_group_by 
            ";
//            echo "<br>".$query."<br>" ;
//            die;
            if($result=$this->exeSQL($query))
            {
                if(!empty($idCurrencyAry))
                {
                    $ret_ary = array();
                    if($idCurrency>0)
                    {
                        $row=$this->getAssoc($result);
                        return $row;
                    } 
                    else
                    {
                        $ctr=0;
                        while($row=$this->getAssoc($result))
                        {
                            $ret_ary[$ctr] = $row ;
                            $ctr++;
                        }
                        return $ret_ary; 
                    }
                }
                else
                {
                    if($iBooking)
                    {  
                        $ret_ary = array();
                        $ret_ary['USD'] = 1;
                        while($row=$this->getAssoc($result))
                        { 
                            $idCurrency = $row['idCurrency']; 
                            if($idCurrency>0)
                            {
                                //Fetching Latest exchange rate for the currency
                                $currencyDetailsAry = array(); 
                                $currencyDetailsAry = $this->getCurrencyDetails($idCurrency);	 
                                
                                $ret_ary[$row['szCurrency']] = $currencyDetailsAry['fUsdValue'];
                            } 
                        } 
                        return $ret_ary; 
                    }
                    else
                    {
                        $row=$this->getAssoc($result);
                        return $row;
                    } 
                } 
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
	}
	

	function load($idWarehouse)
	{
            if($idWarehouse>0)
            {
                $query="
                    SELECT
                        id,
                        idForwarder,
                        szWareHouseName,
                        szAddress,
                        szAddress2,
                        szAddress3,
                        szPostCode,
                        szCity,
                        szState,
                        idCountry,
                        szPhone,
                        szLatitude,
                        szLongitude,
                        szUTCOffset,
                        szEmail,
                        szContactPerson,
                        iWarehouseType
                    FROM
                        ".__DBC_SCHEMATA_WAREHOUSES__."	
                    WHERE
                        id = '".(int)$idWarehouse."'	
                ";
                //echo "<br><br>".$query ;
                if($result = $this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        $row=$this->getAssoc($result);
                        $this->id=sanitize_all_html_input(trim($row['id']));	
                        $this->idForwarder=sanitize_all_html_input(trim($row['idForwarder']));					
                        $this->szWareHouseName=sanitize_all_html_input(trim($row['szWareHouseName']));
                        $this->szAddress=sanitize_all_html_input(trim($row['szAddress']));
                        $this->szAddress2=sanitize_all_html_input(trim($row['szAddress2']));
                        $this->szAddress3=sanitize_all_html_input(trim($row['szAddress3']));
                        $this->szPostCode=sanitize_all_html_input(trim($row['szPostCode']));
                        $this->szCity=sanitize_all_html_input(trim($row['szCity'])); 
                        $this->szState=sanitize_all_html_input(trim($row['szState']));
                        $this->idCountry=sanitize_all_html_input(trim($row['idCountry']));
                        $this->szPhone=sanitize_all_html_input(trim($row['szPhone']));
                        $this->szEmail=sanitize_all_html_input(trim($row['szEmail']));
                        $this->szContactPerson=sanitize_all_html_input(trim($row['szContactPerson'])); 
                        $this->szLatitude=sanitize_all_html_input(trim($row['szLatitude']));
                        $this->szLongitude=sanitize_all_html_input(trim($row['szLongitude']));
                        $this->szUTCOffset=sanitize_all_html_input(trim($row['szUTCOffset']));
                        $this->iWarehouseType = sanitize_all_html_input(trim($row['iWarehouseType'])); 
                        return true ;
                    }
                    else
                    {
                        return false ;
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
	
        function updateBookingQuotesWithCurrency($idCurrency,$szBookingRandomNum)
        {
            if(($idCurrency>0) && !empty($szBookingRandomNum))
            {
                $currencyExchangeRateAry = array();
                if($idCurrency==1)
                {
                    $currencyExchangeRateAry['idCurrency']=1;
                    $currencyExchangeRateAry['szCurrency'] = 'USD';
                    $currencyExchangeRateAry['fExchangeRate'] = 1;
                }
                else
                {
                    $resultAry = $this->getCurrencyDetails($idCurrency);								
                    $currencyExchangeRateAry['idCurrency']=$resultAry['idCurrency'];
                    $currencyExchangeRateAry['szCurrency'] = $resultAry['szCurrency'];
                    $currencyExchangeRateAry['fExchangeRate'] = $resultAry['fUsdValue'];
                }

                $kBooking = new cBooking();
                $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
                
                $bookingIdAry = array();
                $bookingIdAry[0] = $idBooking ;
                $newInsuredBookingAry = array();
                $newBookingQuotesAry = $kBooking->getAllBookingQuotes(false,$bookingIdAry);
                        
                $postSearchAry = array();
                $postSearchAry = $newBookingQuotesAry[0];  
                        
                $fTotalPriceUSD = $postSearchAry['fCustomerExchangeRate'] * $postSearchAry['fTotalPriceCustomerCurrency'];
                $fVatValueUsd = $postSearchAry['fTotalVat'] * $postSearchAry['fCustomerExchangeRate'];
                $fTotalInsuranceCostForBookingCustomerCurrency = $postSearchAry['fInsuranceSellPrice'] * $postSearchAry['fCustomerExchangeRate'];
                $fTotalInsuranceCostForBookingCustomerCurrency_buyRate = $postSearchAry['fInsuranceBuyPrice'] * $postSearchAry['fCustomerExchangeRate'];
                
                if($currencyExchangeRateAry['fExchangeRate']>0)
                {
                    $fTotalPriceCustomerCurrency = $fTotalPriceUSD / $currencyExchangeRateAry['fExchangeRate'] ; 
                    $fTotalVat = $fVatValueUsd / $currencyExchangeRateAry['fExchangeRate'] ; 
                    $fTotalInsuranceCostForBookingCustomerCurrency = $fTotalInsuranceCostForBookingCustomerCurrency / $currencyExchangeRateAry['fExchangeRate'];
                    $fTotalInsuranceCostForBookingCustomerCurrency_buyRate = $fTotalInsuranceCostForBookingCustomerCurrency_buyRate / $currencyExchangeRateAry['fExchangeRate'] ;
                } 
                
                $res_ary = array();
                $res_ary['fTotalPriceUSD'] = $fTotalPriceUSD ;
                $res_ary['fTotalPriceCustomerCurrency'] = $fTotalPriceCustomerCurrency ;
                $res_ary['szCustomerCurrency'] = $currencyExchangeRateAry['szCurrency'] ;
                $res_ary['fCustomerExchangeRate'] = $currencyExchangeRateAry['fExchangeRate'] ;
                $res_ary['idCustomerCurrency'] = $currencyExchangeRateAry['idCurrency'] ;
                $res_ary['fTotalVat'] = $fTotalVat ; 
                $res_ary['fTotalInsuranceCostForBookingCustomerCurrency'] = $fTotalInsuranceCostForBookingCustomerCurrency ;
                $res_ary['fTotalInsuranceCostForBookingCustomerCurrency_buyRate'] = $fTotalInsuranceCostForBookingCustomerCurrency_buyRate ;
                        
                if(!empty($res_ary))
                {
                    $update_query = '';
                    foreach($res_ary as $key=>$value)
                    {
                        $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                } 
                
                $update_query = rtrim($update_query,","); 
                if($kBooking->updateDraftBooking($update_query,$idBooking))
                {
                    return true;
                } 
                else
                {
                    return false;
                } 
            }
        }
	function updateBookingWithCurrency($idCurrency,$szBookingRandomNum)
	{
		if(($idCurrency>0) && !empty($szBookingRandomNum))
		{
			$currencyExchangeRateAry = array();
			if($idCurrency==1)
			{
				$currencyExchangeRateAry['idCurrency']=1;
				$currencyExchangeRateAry['szCurrency'] = 'USD';
				$currencyExchangeRateAry['fExchangeRate'] = 1;
			}
			else
			{
				$resultAry = $this->getCurrencyDetails($idCurrency);								
				$currencyExchangeRateAry['idCurrency']=$resultAry['idCurrency'];
				$currencyExchangeRateAry['szCurrency'] = $resultAry['szCurrency'];
				$currencyExchangeRateAry['fExchangeRate'] = $resultAry['fUsdValue'];
			}
			
			$kBooking = new cBooking();
			$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			if((int)$postSearchAry['iBookingStep']<7)
			{
				$iBookingStep = 7 ;
			}
			else
			{
				$iBookingStep = (int)$postSearchAry['iBookingStep'] ;
			}
			$query="
				UPDATE
					".__DBC_SCHEMATA_BOOKING__."
				SET
					idCustomerCurrency = '".mysql_escape_custom($currencyExchangeRateAry['idCurrency'])."',	
					szCustomerCurrency = '".mysql_escape_custom($currencyExchangeRateAry['szCurrency'])."',
					fCustomerExchangeRate = '".mysql_escape_custom($currencyExchangeRateAry['fExchangeRate'])."',
					iBookingStep = '".(int)$iBookingStep."'
				WHERE
					id = '".(int)$idBooking."'	
			" ;
			if($result = $this->exeSQL($query))
			{
				return true ;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function updateUTCOffSetMs($idWareHouse,$utcoffset)
	{ 
            if((int)$idWareHouse>0)
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_WAREHOUSES__."
                    SET
                        szUTCOffset = '".mysql_escape_custom($utcoffset)."',
                        dtUTCOffsetUpdated=NOW()
                    WHERE
                        id='".(int)$idWareHouse."'	
                ";
                //echo $query;
                if($result = $this->exeSQL($query))
                {
                    return true ;
                }
                else
                {
                    return false ;
                }
            }
            else
            {
                return false ;
            }
	}
	
	
	function getWareHouseDataById($idForwarder,$order_by=false)
	{
		$sql_order_by='';
		if(!empty($order_by))
		{
			$sql_order_by = " ORDER BY ".$order_by." ASC ";
		}
		$query="
			SELECT
				idWarehouseFrom,
				idWarehouseTo,
				szCutOffLocalTime,
				szAvailableLocalTime
			FROM
				".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
			WHERE
				(
					idWarehouseFrom='".(int)$idForwarder."'
				OR
					idWarehouseTo='".(int)$idForwarder."'
				)
			".$sql_order_by."
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				while($row=$this->getAssoc($result))
				{
					$forwardweArr[]=$row;
				}
				return $forwardweArr;
			}
			else
			{
				return false ;
			}
		}
	}
	
	
	function updateTransitTime($idWareHouse1,$idWareHouse2,$transittime)
	{
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
                SET
                    iTransitHours=iTransitHours + '".$transittime."'
                WHERE
                    (
                        idWarehouseFrom='".(int)$idWareHouse2."'
                    AND
                        idWarehouseTo='".(int)$idWareHouse1."'
                    )
                OR
                    (
                        idWarehouseFrom='".(int)$idWareHouse1."'
                    AND
                        idWarehouseTo='".(int)$idWareHouse2."'
                    )	
            ";
            //echo $query;
            if($result = $this->exeSQL($query))
            {
                return true ;
            }
            else
            {
                return false ;
            }
	}
	
	function getWareHouseData($idForwarder,$order_by=false,$iWarehouseType=__WAREHOUSE_TYPE_CFS__)
	{
            if((int)$idForwarder>0)
            {
                if(!empty($order_by))
                {
                    $sql_order_by = " ORDER BY ".$order_by." ASC ";
                }
                else
                {
                    $sql_order_by = " ORDER BY c.szCountryName ASC,w.szCity ASC,w.szWareHouseName ASC  ";
                }
                $query="
                    SELECT 
                        w.`id` ,
                        w.`idForwarder` ,
                        w.`szWareHouseName` ,
                        w.`szAddress` , 
                        w.`szAddress2` , 
                        w.`szAddress3` , 
                        w.`szPostCode` , 
                        w.`szCity` , 
                        w.`szState` , 
                        w.`idCountry` , 
                        w.`szPhone` , 
                        w.`szLatitude` , 
                        w.`szLongitude` , 
                        w.`szUpdatedBy`,
                        w.`dtUpdatedOn`,
                        c.szCountryName
                    FROM
                        ".__DBC_SCHEMATA_WAREHOUSES__." AS w
                    INNER JOIN
                        ".__DBC_SCHEMATA_COUNTRY__." AS c
                    ON
                        c.id=w.idCountry
                    WHERE
                        w.idForwarder='".(int)$idForwarder."'
                    AND
                        w.iActive='1'
                    AND
                        w.iWarehouseType = '".$iWarehouseType."'
                    $sql_order_by
                ";
                //echo $query ;
                if($result=$this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        while($row=$this->getAssoc($result))
                        {
                            $warehouseDataArr[]=$row;
                        }
                        return $warehouseDataArr;
                    }
                    else
                    {
                        return array() ;
                    }
                }
            }
            else
            {
                return array() ;
            }
	}
	
	
	function deleteWarehouse($idWarehouse,$idForwarder)
	{
            if((int)$idWarehouse>0 && (int)$idForwarder>0)
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_WAREHOUSES__."
                    SET
                        iActive='0',
                        dtUpdatedOn=NOW()
                    WHERE
                        id='".(int)$idWarehouse."'
                    AND
                        idForwarder='".(int)$idForwarder."'
                ";
                if($result=$this->exeSQL($query))
                {
                    /*
                    * If this warehouse is added in any Rail transport then we also deletes that one
                    */
                    $kService = new cServices();
                    $kService->deleteRailTransportByWarehouseId($idWarehouse);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
	}
	
	function deleteWarehouseByForwarderId($idForwarder)
	{
		if((int)$idForwarder>0)
		{
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_WAREHOUSES__."
				WHERE
					idForwarder='".(int)$idForwarder."'
			";
			if($result=$this->exeSQL($query))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function inactiveWarehouseByForwarderId($idForwarder)
	{
		if((int)$idForwarder>0)
		{
			$query="
				UPDATE
					".__DBC_SCHEMATA_WAREHOUSES__."
				SET
					iActive = '0'
				WHERE
					idForwarder='".(int)$idForwarder."'
			";
			if($result=$this->exeSQL($query))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function inactiveUploadServicesByForwarderId($idForwarder)
	{
		if((int)$idForwarder>0)
		{
			$query="
				UPDATE
					".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
				SET
					iActive = '0'
				WHERE
					idForwarder='".(int)$idForwarder."'
			";
			if($result=$this->exeSQL($query))
			{
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function deleteUploadServicesByForwarderId($idForwarder)
	{
		if((int)$idForwarder>0)
		{
			$query = "
				DELETE FROM
					".__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__."
				WHERE
					idForwarder = '".(int)$idForwarder."';
			";
			if($result=$this->exeSQL($query))
			{
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function getAllWareHousesByForwarder($idForwarder)
	{
		if($idForwarder>0)
		{
			$query="
				SELECT
					whs.id
				FROM
					".__DBC_SCHEMATA_WAREHOUSES__." whs
				WHERE
					whs.idForwarder='".(int)$idForwarder."'
			";
			//echo $query."<br><br>";
			if($result = $this->exeSQL($query))
			{
				$wareHouseAry = array();
				$ctr = 0;
				while($row=$this->getAssoc($result))
				{
					$wareHouseAry[$ctr] = $row['id'];
					$ctr++;
				}
				return $wareHouseAry;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function save($data)
	{	
		$forwarderContactId = (int)$_SESSION['forwarder_user_id'];	
		if($data['szCity']== t($t_base.'fields/type_name'))
		{
			$data['szCity'] = '';
		}
		if($data['szPostCode']== t($t_base.'fields/optional'))
		{
			$data['szPostCode'] = '';
		}
		//print_r($data);
		$this->set_szWarehouseName(trim(sanitize_all_html_input($data['szCFSName'])));
		$this->set_szAddress1(trim(sanitize_all_html_input($data['szAddressLine1'])));
		$this->set_szAddress2(trim(sanitize_all_html_input($data['szAddressLine2'])));
		$this->set_szAddress3(trim(sanitize_all_html_input($data['szAddressLine3'])));
		
		$this->set_szCity(trim(sanitize_all_html_input($data['szCity'])));
		$this->set_szState(trim(sanitize_all_html_input($data['szState'])));
		$this->set_szCountry(trim(sanitize_all_html_input($data['szCountry'])));
		$this->set_szPhoneNumber(trim(sanitize_all_html_input(urlencode($data['szPhoneNumber']))));
		
		$this->set_szLongitude(trim(sanitize_all_html_input($data['szLongitude'])));
		$this->set_szLatitude(trim(sanitize_all_html_input($data['szLatitude'])));
		$this->set_id(trim(sanitize_all_html_input($data['idEditWarehouse'])));	
		
		$this->set_iForwarder(trim(sanitize_all_html_input($data['idForwarder'])));
		
		if((int)$data['szCountry']==__ID_COUNTRY_HONG_KONG__)
		{
			$this->szPostcode = $data['szPostCode'] ;
		}
		else
		{
			//$this->set_szPostcode();
			$this->szPostcode = trim(sanitize_all_html_input($data['szPostCode'])) ;
		}
		/*
		$kRegisterShipCon=new cRegisterShipCon();
		if($kRegisterShipCon->isCheckCityPostCode($this->szCountry,$this->szCity,$this->szPostcode))
		{
			$this->addError( "szPostcode" , t($this->t_base_cfs.'messages/added_postcode_does_not_match') );
		}
		*/
		
		//exit if error exist.
		if ($this->error === true)
		{
			return false;
		}
		
		if(!empty($this->szPhoneNumber))
		{
			$str = substr($this->szPhoneNumber, 0, 1); 
			//echo $str;
			$substr=substr($this->szPhoneNumber,1);
			$phone=str_replace("+"," ",$substr);
			//echo $phone;
			if(!empty($phone))
			{
				$phoneNumber=$str."".$phone;
			}
		}
		$transitHourFlag=false;
		$oldLatitudeLoongitudeArr=$this->getLatitudeLongitude($this->id);
		
		if($oldLatitudeLoongitudeArr['szLatitude']!=$this->szLatitude && $oldLatitudeLoongitudeArr['szLongitude']!=$this->szLongitude)
		{
			$timezone_url=__ASK_GEO_API_URL__."/".__ASK_GEO_ACCOUNT_ID__."/".__ASK_GEO_API_KEY__."/query.".__ASK_GEO_FORMAT__."?databases=".__ASK_GEO_DATABATE__."&points=";
			
			$posturl=$timezone_url."".$this->szLatitude.",".$this->szLongitude;
			 $response = $this->curl_get_file_contents($posturl);
			  $decoded_response = json_decode($response);
			  $utcoffsetupdated='';
			  $utcoffsetupdated=$decoded_response->data[0]->TimeZone->CurrentOffsetMs;
		}
		
		if($oldLatitudeLoongitudeArr['szUTCOffset']!=$utcoffsetupdated)
		{
			$transitHourFlag=true;
		}
		$forwarderContactName	=	$this->findForwarderFirstLastName($forwarderContactId); 
		
		if((int)$_SESSION['forwarder_admin_id']>0)
		{
			$idAdmin = (int)$_SESSION['forwarder_admin_id'];
			$idForwarderContact = 0 ;
		}
		else
		{
			$idAdmin = 0;
			$idForwarderContact = (int)$_SESSION['forwarder_user_id'];
		} 
		$query="
			UPDATE
				".__DBC_SCHEMATA_WAREHOUSES__."
			SET
				szWareHouseName='".mysql_escape_custom($this->szWarehouseName)."',
				szAddress='".mysql_escape_custom($this->szAddress1)."',
				szAddress2='".mysql_escape_custom($this->szAddress2)."',
				szAddress3='".mysql_escape_custom($this->szAddress3)."',
				szPostCode='".mysql_escape_custom($this->szPostcode)."',
				szCity='".mysql_escape_custom(ucwords(strtolower($this->szCity)))."',
				szState='".mysql_escape_custom($this->szState)."',
				idCountry='".(int)$this->szCountry."',
				szPhone='".mysql_escape_custom($phoneNumber)."',
				szLatitude='".mysql_escape_custom($this->szLatitude)."',
				szLongitude='".mysql_escape_custom($this->szLongitude)."',
				dtUpdatedOn=NOW(),
				idForwarderContact = '".(int)$idForwarderContact."',
				idAdmin = '".(int)$idAdmin."',
				szUpdatedBy	=	'".mysql_escape_custom($forwarderContactName)."',
				szUTCOffset='".mysql_escape_custom($utcoffsetupdated)."'
			WHERE
				id='".(int)$this->id."'
			AND
				idForwarder='".(int)$this->idForwarder."'	
		";
		//ECHO $query;die;
		if($result=$this->exeSQL($query))
		{
			if($transitHourFlag)
			{
				$idWareHouse=$this->getWareHouseDataById($this->id);
				$id=$this->id;
				if(!empty($idWareHouse))
				{
					foreach($idWareHouse as $idWareHouses)
					{
						$toUTCOFFSET='';
						//$fromflag=false;
						//$toflag=false;
						$szAvailableLocalTime='';
						$szCutOffLocalTime='';
						$cutoffTimeAry = explode(':',$idWareHouses['szCutOffLocalTime']);
						$szCutOffLocalTime = $cutoffTimeAry[0]+($cutoffTimeAry[1]/60) ;
						
						$availabelTimeAry = explode(':',$idWareHouses['szAvailableLocalTime']);
						$szAvailableLocalTime = $availabelTimeAry[0]+($availabelTimeAry[1]/60) ;
						
						$localtime=$szAvailableLocalTime-$szCutOffLocalTime;
						
						if($idWareHouses['idWarehouseFrom']==$id)
						{
							$idWareHouseTo=$idWareHouses['idWarehouseTo'];
							//$fromflag=true;
						}
						else if($idWareHouses['idWarehouseTo']==$id)
						{
							$idWareHouseTo=$idWareHouses['idWarehouseFrom'];
							//$toflag=true;
						}
												
						$this->load($idWareHouseTo);
						
						$toUTCOFFSET=$this->szUTCOffset;
						
						$diffUTVOffSet=$wareHouseUpdatedArrs['szUTCOffset']-$toUTCOFFSET;
						
						$transit_time=($diffUTVOffSet)/1000/60/60;
						$transittime=$transit_time+$localtime;
						$this->updateTransitTime($idWareHouseTo,$id,$transittime);
						
					}
				}
			}
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
	function add($data)
	{		
		if($data['szCity']== t($t_base.'fields/type_name'))
		{
			$data['szCity'] = '';
		}
		if($data['szPostCode']== t($t_base.'fields/optional'))
		{
			$data['szPostCode'] = '';
		}
		//print_r($data);
		$this->set_szWarehouseName(trim(sanitize_all_html_input($data['szCFSName'])));
		$this->set_szAddress1(trim(sanitize_all_html_input($data['szAddressLine1'])));
		$this->set_szAddress2(trim(sanitize_all_html_input($data['szAddressLine2'])));
		$this->set_szAddress3(trim(sanitize_all_html_input($data['szAddressLine3'])));
		$this->set_szCity(trim(sanitize_all_html_input($data['szCity'])));
		$this->set_szState(trim(sanitize_all_html_input($data['szState'])));
		$this->set_szCountry(trim(sanitize_all_html_input($data['szCountry'])));
		$this->set_szPhoneNumber(trim(sanitize_all_html_input(urlencode($data['szPhoneNumber']))));
		
		$this->set_szLongitude(trim(sanitize_all_html_input($data['szLongitude'])));
		$this->set_szLatitude(trim(sanitize_all_html_input($data['szLatitude'])));
		
		
		
		$this->set_iForwarder(trim(sanitize_all_html_input($data['idForwarder'])));
		
		if((int)$data['szCountry']==__ID_COUNTRY_HONG_KONG__)
		{
			$this->szPostcode = $data['szPostCode'] ;
		}
		else
		{
			$this->szPostcode = $data['szPostCode'] ;
			//$this->set_szPostcode(trim(sanitize_all_html_input($data['szPostCode'])));
		}
		/*
		$kRegisterShipCon=new cRegisterShipCon();
		if($kRegisterShipCon->isCheckCityPostCode($this->szCountry,$this->szCity,$this->szPostcode))
		{
			$this->addError( "szPostcode" , t($this->t_base_cfs.'messages/added_postcode_does_not_match') );
		}
		*/
		
		//exit if error exist.
		if ($this->error === true)
		{
			return false;
		}
		
		if(!empty($this->szPhoneNumber))
		{
			$str = substr($this->szPhoneNumber, 0, 1); 
			//echo $str;
			$substr=substr($this->szPhoneNumber,1);
			$phone=str_replace("+"," ",$substr);
			//echo $phone;
			if(!empty($phone))
			{
				$phoneNumber=$str."".$phone;
			}
		}
		
		$timezone_url=__ASK_GEO_API_URL__."/".__ASK_GEO_ACCOUNT_ID__."/".__ASK_GEO_API_KEY__."/query.".__ASK_GEO_FORMAT__."?databases=".__ASK_GEO_DATABATE__."&points=";
		
		$posturl=$timezone_url."".$this->szLatitude.",".$this->szLongitude;
		$response = $this->curl_get_file_contents($posturl);
		$decoded_response = json_decode($response);
		$utcoffsetupdated='';
		$utcoffsetupdated=$decoded_response->data[0]->TimeZone->CurrentOffsetMs;
		$forwarderContactId = (int)$_SESSION['forwarder_user_id'];	
	    $forwarderContactName	=	$this->findForwarderFirstLastName($forwarderContactId);  
	    
		if((int)$_SESSION['forwarder_admin_id']>0)
		{
			$idAdmin = (int)$_SESSION['forwarder_admin_id'];
			$idForwarderContact = 0 ;
		}
		else
		{
			$idAdmin = 0;
			$idForwarderContact = (int)$_SESSION['forwarder_user_id'];
		}
						
		$query="
			INSERT INTO
				".__DBC_SCHEMATA_WAREHOUSES__."
			(
				szWareHouseName,
				szAddress,
				szAddress2,
				szAddress3,
				szPostCode,
				szCity,
				szState,
				idCountry,
				szPhone,
				szLatitude,
				szLongitude,
				idForwarder,
				dtUpdatedOn,
				dtCreatedOn,
				iActive,
				idForwarderContact,
				idAdmin,
				szUpdatedBy,
				szUTCOffset
			)	
				VALUES
			(	
				'".mysql_escape_custom($this->szWarehouseName)."',
				'".mysql_escape_custom($this->szAddress1)."',
				'".mysql_escape_custom($this->szAddress2)."',
				'".mysql_escape_custom($this->szAddress3)."',
				'".mysql_escape_custom($this->szPostcode)."',
				'".mysql_escape_custom(ucwords(strtolower($this->szCity)))."',
				'".mysql_escape_custom($this->szState)."',
				'".(int)$this->szCountry."',
				'".mysql_escape_custom($phoneNumber)."',
				'".mysql_escape_custom($this->szLatitude)."',
				'".mysql_escape_custom($this->szLongitude)."',
				'".(int)$this->idForwarder."',
				NOW(),
				NOW(),
				'1',
				'".(int)$idForwarderContact."',
				'".(int)$idAdmin."',
				'".$forwarderContactName."',
				'".mysql_escape_custom($utcoffsetupdated)."'
			 )	
		";
		if($result=$this->exeSQL($query))
		{
                    return true;
		}
		else
		{
                    return false;
		}
	}
	
	function curl_get_file_contents($URL,$bGeoplugin=false) 
	{
            $c = curl_init();
            curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($c, CURLOPT_URL, $URL);
            curl_setopt($c, CURLOPT_SSL_VERIFYPEER, FALSE);
            if($bGeoplugin)
            { 
                curl_setopt($c, CURLOPT_TIMEOUT_MS, '5000');
            } 
            $contents = curl_exec($c); 
            $err  = curl_getinfo($c,CURLINFO_HTTP_CODE);
            curl_close($c);
            if ($contents) return $contents;
            else return FALSE;
         }
	 
	 function getLatitudeLongitude($idWarehouse)
	 {
	 	if($idWarehouse>0)
		{
			$query="
				SELECT
					szLatitude,
					szLongitude,
					szUTCOffset
				FROM
					".__DBC_SCHEMATA_WAREHOUSES__."	
				WHERE
					id = '".(int)$idWarehouse."'	
			";
			//echo "<br><br>".$query ;
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					return $row ;
				}
				else
				{
					return false ;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	 }
	 
	 function getExpectedServices()
	 {
	 	$query="
			SELECT
			 	idDestinationCountry,
			 	idOriginCountry
			FROM
				".__DBC_SCHEMATA_EXPACTED_SERVICES__."
			WHERE
				iSendNotification='1'
			AND
				iNotificationSent='0'
			GROUP BY idOriginCountry ,idDestinationCountry
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$forwarderbooking_ary = array();
				while($row=$this->getAssoc($result))
				{
					$forwarderbooking_ary[] = $row;
				}
				return $forwarderbooking_ary;
			}
			else
			{
				return array();
			}
		}
	 }
	 
	 function getWarehouseFromCountryId($idCounty)
	 {
	 	$query="
	 		SELECT
	 			id
	 		FROM
	 			".__DBC_SCHEMATA_FROWARDER__."
	 		WHERE
 			 	idCountry='".(int)$idCounty."'
	 	";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$warehouse_ary = array();
				while($row=$this->getAssoc($result))
				{
					$warehouse_ary[] = $row['id'];
				}
				return $warehouse_ary;
			}
			else
			{
				return array();
			}
		}
	 }
	 
	 function getLCLServiceWarehouses($fromWarehouse,$toWarehouse)
	 {
	 
	 	if((int)$fromWarehouse>0 && (int)$toWarehouse>0)
	 	{
	 		$query="
	 			SELECT
	 				id
	 			FROM
	 				".__DBC_SCHEMATA_WAREHOUSES_PRICING__."	
	 			WHERE
	 				idWarehouseFrom='".(int)$fromWarehouse."'
	 			AND
	 				idWarehouseTo='".(int)$toWarehouse."'
	 			AND
	 				iActive='1'
	 		";
	 		if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
	 	}
	 	else
	 	{
	 		return false;
	 	}
	 }
	
	 function getExpectedServicesDetails($idOriginCountry,$idDestinationCountry)
	 {
	 	$query="
			SELECT
			 	szFirstName,
			 	szLastName,
			 	szEmail,
			 	szOriginCountry,
			 	szDestinationCountry
			FROM
				".__DBC_SCHEMATA_EXPACTED_SERVICES__."
			WHERE
				iSendNotification='1'
			AND
				idDestinationCountry='".(int)$idDestinationCountry."'
			AND
				idOriginCountry='".(int)$idOriginCountry."'
			AND
				iNotificationSent='0'
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$forwarderbooking_ary = array();
				while($row=$this->getAssoc($result))
				{
					$forwarderbooking_ary[] = $row;
				}
				return $forwarderbooking_ary;
			}
			else
			{
				return array();
			}
		}
	 }
	 
	 function updateSendNotificationFlag($idOriginCountry,$idDestinationCountry)
	 {
	    if((int)$idOriginCountry>0 && (int)$idDestinationCountry>0)
	 	{
	 		$query="
	 			UPDATE
	 				".__DBC_SCHEMATA_EXPACTED_SERVICES__."
	 			SET
	 				iNotificationSent='1',
	 				dtNotificationSent=NOW()
	 			WHERE
					iSendNotification='1'
				AND
					idDestinationCountry='".(int)$idDestinationCountry."'
				AND
					idOriginCountry='".(int)$idOriginCountry."'
				AND
					iNotificationSent='0'	 		
	 		";
	 		if($result=$this->exeSQL($query))
			{
				return true;
			}
	 	}
	 }
	 
	 function getAlternateService($data)
	 {
	 	if(!empty($data))
	 	{
	 		$wareHouseFromAry = $this->get_warehouse_distance_search($data,'FROM',false,false,$data['idOriginCountry']);
			$wareHouseToAry = $this->get_warehouse_distance_search($data,'TO',false,false,$data['idDestinationCountry']);
		 	if(!empty($wareHouseFromAry) && !empty($wareHouseToAry))
			{
				foreach($wareHouseFromAry as $idWHSFrom=>$iDistanceWHSFrom)
				{
					foreach($wareHouseToAry as $idWHSTo=>$iDistanceWHSTo)
					{
						if($idWHSTo!=$idWHSFrom)
						{
							$query="
								SELECT
									wtw.id,
									wtw.id idWTW,
									wtw.idWarehouseFrom,
									wtw.idWarehouseTo,
									wtw.iBookingCutOffHours,
									wtw.idCutOffDay,
									wtw.szCutOffLocalTime,
									wtw.idAvailableDay,
									wtw.szAvailableLocalTime,
									wtw.iTransitHours,
									wtw.dtValidFrom,
									wtw.dtExpiry,
									wtw.szFreightCurrency,	
									wtw.iTransitDays,		
									( SELECT szWeekDay FROM ".__DBC_SCHEMATA_WEEK_DAYS__." wd WHERE wd.id = wtw.idAvailableDay ) szWeekDay,
									( SELECT szWeekDay FROM ".__DBC_SCHEMATA_WEEK_DAYS__." wd WHERE wd.id = wtw.idAvailableDay ) szAvailableDay
								FROM
									".__DBC_SCHEMATA_WAREHOUSES_PRICING__."	wtw
								WHERE
									wtw.idWarehouseTo ='".mysql_escape_custom($idWHSTo)."'	
								AND
									wtw.idWarehouseFrom ='".mysql_escape_custom($idWHSFrom)."'
								AND
									wtw.iActive = '1'
								AND
									wtw.dtValidFrom	< now()
								AND
									wtw.dtExpiry > now()	
							";					
							if($result=$this->exeSQL($query))
							{
								if($this->iNumRows>0)
								{
									$ret_ary['idWarehouseTo'] = $idWHSTo ;
									$ret_ary['idWarehouseFrom'] = $idWHSFrom ;
								}
							}
							else
							{
								$this->error = true;
								$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
								$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
								return false;
							}
						}
					}
				}
		 	}
	 	}
	 }
	 function returnCC($idForwarder)
	 {
	 	$query="
			SELECT 
				COUNT(DISTINCT(p.id))  as pricingCC
			FROM 
				".__DBC_SCHEMATA_PRICING_CC__." AS  p
			INNER JOIN 	
				".__DBC_SCHEMATA_WAREHOUSES__." AS w
			ON
				(w.id IN (p.idWarehouseFrom,p.idWarehouseTo) ) 
			AND	
				w.idForwarder='".(int)$idForwarder."' 
			AND 
				w.iActive = '1'
			AND
				p.iActive = '1'
		";
		//echo $query."<BR>";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$forwarderCC = array();
				while($row=$this->getAssoc($result))
				{
					$forwarderCC = $row;
				}
				return $forwarderCC;
			}
			else
			{
				return array();
			}
		}
	 
	 }
	function returnHaulage($idForwarder)
	 {
	 	$query="
			SELECT 
				COUNT(DISTINCT(p.id))  as pricingHaulage
			FROM 
				".__DBC_SCHEMATA_HAULAGE_PRICING__." p
			INNER JOIN	
				".__DBC_SCHEMATA_WAREHOUSES__." w
			ON 
				(w.id=p.idWarehouse)
			WHERE 
				w.idForwarder='".(int)$idForwarder."' 
			AND 
				w.iActive 	= '1'
			AND
				p.iActive 	= '1'
			AND
				p.dtExpiry 	>= NOW()
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$forwarderCC = array();
				while($row=$this->getAssoc($result))
				{
					$forwarderCC = $row;
				}
				return $forwarderCC;
			}
			else
			{
				return array();
			}
		}
	 }
	 
	function returnLcl($idForwarder)
	{
		$query = "	
			SELECT 
				COUNT(DISTINCT(p.id)) as lclServices
			FROM 
				".__DBC_SCHEMATA_WAREHOUSES_PRICING__." p
			INNER JOIN 	
				".__DBC_SCHEMATA_WAREHOUSES__." w	
			ON 
				( w.id = p.idWarehouseFrom AND w.iActive = '1' ) 
			INNER JOIN 	
				".__DBC_SCHEMATA_WAREHOUSES__." w2	
			ON 
				( w2.id = p.idWarehouseTo AND w2.iActive = '1' ) 
			AND	
				w.idForwarder='".(int)$idForwarder."' 
			AND
				p.iActive = '1'
			AND 
				p.dtExpiry >= NOW()	
			";
		
		//echo $query."<br>";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$contactArr = array();
				while($row=$this->getAssoc($result))
				{
					$contactArr = $row;
				}
				return $contactArr;
			}
			else
			{
				return array();
			}
		}
	}
	
	function returnSalesMonth($idForwarder,$days)
	{
		$query = "	
			SELECT
			COALESCE(TRUNCATE(SUM(`fTotalPriceUSD`),1),0) AS totalPrice
			FROM
				".__DBC_SCHEMATA_BOOKING__."
			WHERE
				`idForwarder`='".(int)$idForwarder."'
			AND
				(`idBookingStatus`='3'
			OR
				`idBookingStatus`='4')			
			AND	
			`dtBookingConfirmed`>=(DATE_SUB(CURDATE(), INTERVAL ".(int)$days." DAY))
			";
		
		//echo $query."<br>";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$row=$this->getAssoc($result);
			
				return $row['totalPrice'];
			}
			else
			{
				return array();
			}
		}
	}
	function selectPublishmentCustomer()
	{
	$query = "	
		SELECT 
			id
		FROM
			".__DBC_SCHEMATA_MANAGEMENT_TERMS_CUSTOMER__."
		WHERE
			(
				iOrderByDraft=0 
			OR
				iOrderBy=0
			OR 
				iUpdated =1	
			)	
			";
		
		//echo $query."<br>";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{	
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	function selectPublishmentForwarder()
	{
	$query = "	
		SELECT 
			id
		FROM
			".__DBC_SCHEMATA_MANAGEMENT_TERMS__."
		WHERE
			(
				iOrderByDraft=0 
			OR
				iOrderBy=0
			OR 
				iUpdated =1	
			)	
			";
		
		//echo $query."<br>";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{	
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	function selectPublishmentFAQCustomer()
	{
	$query = "	
		SELECT 
			id
		FROM
			".__DBC_SCHEMATA_MANAGEMENT_FAQ_CUSTOMER__."
		WHERE
			(
				iOrderByDraft=0 
			OR
				iOrderBy=0
			OR 
				iUpdated =1	
			)	
			";
		
		//echo $query."<br>";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{	
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	function selectPublishmentFAQForwarder()
	{
	$query = "	
		SELECT 
			id
		FROM
			".__DBC_SCHEMATA_MANAGEMENT_FAQ__."
		WHERE
			(
				iOrderByDraft=0 
			OR
				iOrderBy=0
			OR 
				iUpdated =1	
			)	
			";
		
		//echo $query."<br>";
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{	
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	function getCountryDetails($idCountry)
	{
		if($idCountry>0)
		{
			$query="
				SELECT
					id,
					szCountryName 
				FROM
					".__DBC_SCHEMATA_COUNTRY__."
				WHERE
				   id = ".(int)mysql_escape_custom($idCountry)."	
			";
			if( ( $result = $this->exeSQL( $query ) ) )
			{
				if($this->iNumRows>0)
				{
					$ret_ary = array();
					$ctr = 0;
					while($row=$this->getAssoc($result))
					{
						$ret_ary = $row;
					}
					return $ret_ary;
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
	}
}

function getCountryLocationDetails($idCountry,$iLanguage=false)
{
	if($idCountry>0)
	{ 
		$query="
                    SELECT
                        c.id,
                        cnm.szName AS szCountryName 
                    FROM
                        ".__DBC_SCHEMATA_COUNTRY__." c
                    INNER JOIN
                        ".__DBC_SCHEMATA_COUNTRY_NAME_MAPPING__." AS cnm
                    ON
                        cnm.idCountry=c.id            
                    WHERE
                        c.id = ".(int)mysql_escape_custom($idCountry)."
                    AND
                        cnm.idLanguage='".(int)$iLanguage."'
		";
		//echo $query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
			if($this->iNumRows>0)
			{
				$ret_ary = array();
				$ctr = 0;
				while($row=$this->getAssoc($result))
				{
					$ret_ary = $row;
				}
				return $ret_ary;
			}
			else
			{
				return array();
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
}
	function LocationDescription($idCountry=0,$iLanguage=false)
	{  
		$query="
                        SELECT
                            c.id,
                            cnm.szName AS szCountryName 
                            FROM
                                ".__DBC_SCHEMATA_COUNTRY__." AS c
                            INNER JOIN
                                ".__DBC_SCHEMATA_COUNTRY_NAME_MAPPING__." AS cnm
                            ON
                                cnm.idCountry=c.id
                            WHERE
                                cnm.idLanguage='".(int)$iLanguage."'
                            ";
			if($idCountry>0)
			{
				$idCountry = (int)sanitize_all_html_input($idCountry);
				$query .= "
                                    AND
                                        c.id = ".(int)mysql_escape_custom($idCountry)."	
				";
				//echo $query;
			}
			else
			{
				$query .="
                                        ORDER BY 
                                            szCountryName ASC
					"; 
				
			}
			//echo $query;
			if( ( $result = $this->exeSQL( $query ) ) )
			{
				if($this->iNumRows>0)
					{
						$ret_ary = array();
						$ctr = 0;
						while($row=$this->getAssoc($result))
						{
							$ret_ary[] = $row;
							$ctr++ ;
						}
						return $ret_ary;
					}
					else
					{
						return array();
					}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}		
	}
	function saveLocationDescription($idCountry,$exactLoc,$nonExactLoc,$iLanguage=false)
	{
            return false;
            
		$exactLoc		= 	(string)sanitize_all_html_input($_POST['exactLoc']);
		$nonExactLoc	= 	(string)sanitize_all_html_input($_POST['nonExactLoc']);
                        
		$query="
                    UPDATE
                        ".__DBC_SCHEMATA_COUNTRY_NAME_MAPPING__."
                    SET
                        $query_update
                    WHERE
                        idCountry = ".mysql_escape_custom($idCountry)."
                    AND
                        idLanguage='".(int)$iLanguage."'
                    ";
                    //echo $query;
                    if( ( $result = $this->exeSQL( $query ) ) )
                    {
                        return true;
                    }
                    else
                    {
                        $this->error = true;
                        $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                        $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                        return false;
                    }		
	}
	function cityNotFoundDetails()
	{
		$query="
			SELECT
				idCountry,
				szCountry,
				szCity,
				dtCreatedOn,
				COUNT(id) AS value
				FROM
					".__DBC_SCHEMATA_CITY_NOT_FOUND__."
				GROUP BY
					szCity
				HAVING
					value >=10
				ORDER BY 	
					value DESC
				";
		//echo $query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
			if($this->iNumRows>0)
			{
				$ret_ary = array();
				$ctr = 0;
				while($row=$this->getAssoc($result))
				{
					$ret_ary[] = $row;
					$ctr++ ;
				}
				return $ret_ary;
			}
			else
			{
				return array();
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function cityCountNotFoundDetails($id)
	{
		$query="
			SELECT
				count(id) as value
				FROM
					".__DBC_SCHEMATA_CITY_NOT_FOUND__."
				WHERE 
				idCountry = ".(int)$id."	
				";
		//echo $qurey;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
			if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					return $row['value'];
				}
				else
				{
					return array();
				}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function deleteCityNotFoundDetails()
	{
		$query="
			DELETE
				FROM
				".__DBC_SCHEMATA_CITY_NOT_FOUND__."
				";
		//echo $query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
			return true;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function getForwarderFeedbackAvgRating($id,$month,$year)
	{
		$query ="SELECT 
					AVG(iRating) as rating
				FROM 
					".__DBC_SCHEMATA_RATING_REVIEW__." 
				WHERE 
					(MONTH(dtCompleted) = ".mysql_escape_custom($month)." AND YEAR(dtCompleted) = ".mysql_escape_custom($year).")
				AND 
					iRating>0
				AND 
					idForwarder = ".mysql_escape_custom($id)."	
					";	
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			while($row=$this->getAssoc($result))
			{
				$bookingAry=$row;
			}
			return $bookingAry;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function getForwarderFeedback6MAvgRating($id,$interval)
	{
		$query ="SELECT 
					AVG(iRating) as rating
				FROM 
					".__DBC_SCHEMATA_RATING_REVIEW__." 
				WHERE 
					(dtCompleted > (DATE_SUB(CURDATE(), INTERVAL ".mysql_escape_custom($interval)." MONTH)))
				AND 
					iRating>0
				AND 
					idForwarder = ".mysql_escape_custom($id)."	
					";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			while($row=$this->getAssoc($result))
			{
				$bookingAry=$row;
			}
			return $bookingAry;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function getAvailableSelection($id=0,$iLanguage=false)
	{
            /*
            * As we have deleted tblcountrtyavailableselection on 15th March 2017 so we no longer using the following code but as of now we have kept the function to avoid any error in the system
            * @Ajay
            */
            return false;
            
            /*
		if((int)$id>0)
		{
			$id = (int)sanitize_all_html_input($id); 
			$query_and .= "WHERE
                                    id = ".mysql_escape_custom($id)."
				";
		}
		
		if($iLanguage>0)
		{
			$iLanguage  = (int)sanitize_all_html_input($iLanguage); 
			if(empty($query_and))
			{
				$query_and = " WHERE iLanguage = '".mysql_escape_custom($iLanguage)."' ";
			}
			else
			{
				$query_and .= " AND iLanguage = '".mysql_escape_custom($iLanguage)."' ";
			}
		}
		
		$query ="
			SELECT 
				id,
				szAvailableSelection,
				iLanguage
			FROM 
				".__DBC_SCHEMATA_AVAILABLE_SELECTION__."
			$query_and
		";
		//echo $query;
		
		if($result=$this->exeSQL($query))
		{
			while($row=$this->getAssoc($result))
			{
				$bookingAry[]=$row;
			}
			return $bookingAry;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
             * 
             */
	}
	function saveAvailableLocation($szSelectionAvailables,$iSelections,$iLanguage)
	{
            /*
            * As we have deleted tblcountrtyavailableselection on 15th March 2017 so we no longer using the following code but as of now we have kept the function to avoid any error in the system
            * @Ajay
            */
            return false;
            /*
		$this->set_szSelectionAvailables((string)sanitize_all_html_input($szSelectionAvailables));
		$this->set_iSelections((int)sanitize_all_html_input($iSelections));
		$this->set_iLanguage((int)sanitize_all_html_input($iLanguage));
		
		//echo "selection ".$szSelectionAvailables."  id ".$iSelections." lang ".$iLanguage ;
		
		if($this->error === true)
		{	
			return false;
		}
		if($this->iSelections==0)
		{
			$query="
				INSERT
					".__DBC_SCHEMATA_AVAILABLE_SELECTION__."
					SET
						szAvailableSelection = '".mysql_escape_custom($this->szSelectionAvailables)."',
						iLanguage = '".mysql_escape_custom($this->iLanguage)."',
						dtCreatedOn = NOW(),
						dtUpdatedOn = NOW()
					";
		}
		else if($this->iSelections>0)
		{
			$query="
				UPDATE
					".__DBC_SCHEMATA_AVAILABLE_SELECTION__."
				SET
					szAvailableSelection = '".mysql_escape_custom($this->szSelectionAvailables)."',
					iLanguage = '".mysql_escape_custom($this->iLanguage)."',
					dtUpdatedOn = NOW()
				WHERE
					id = 	".mysql_escape_custom($this->iSelections)."
					";
		
		}
		//echo "<br>".$query ;
			if( ( $result = $this->exeSQL( $query ) ) )
			{
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}		
             * 
             */
	}
	function getAvailableSelectionName($id)
	{
            /*
            * As we have deleted tblcountrtyavailableselection on 15th March 2017 so we no longer using the following code but as of now we have kept the function to avoid any error in the system
            * @Ajay
            */
            return false;
            /*
		$query ="SELECT 
					szAvailableSelection
				FROM 
					".__DBC_SCHEMATA_AVAILABLE_SELECTION__."
					";
		if((int)$id>0)
		{
			$id  	= (int)sanitize_all_html_input($id); 
			$query .= "WHERE
						id = ".mysql_escape_custom($id)."
				";
		}
		
		if($result=$this->exeSQL($query))
		{
			while($row=$this->getAssoc($result))
			{
				$bookingAry=$row;
			}
			return $bookingAry['szAvailableSelection'];
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
             * 
             */
	}
	function getWareHouseDetails($idWarehouse=false,$currentHaulage = array())
	{
            $query_and = '';
            if( $currentHaulage!= array())
            {
                $this->set_idCountry((int)($currentHaulage['idCountry']));
                $this->set_iForwarder((int)($currentHaulage['idForwarder']));
                if($this->idCountry)
                {
                    $query_and .= " AND w.idCountry = '".(int)$this->idCountry."'";
                }
                if($this->idForwarder)
                {
                    $query_and .= " AND w.idForwarder = '".(int)$this->idForwarder."'";
                }
                if($currentHaulage['iWarehouseType']>0)
                {
                    $query_and .= " AND w.iWarehouseType = '".(int)$currentHaulage['iWarehouseType']."'";
                }
            }

            if($idWarehouse>0)
            {
                $query_and = " AND w.id = '".(int)$idWarehouse."'";
            }

            $query="
                SELECT
                    DISTINCT(w.id),
                    w.idForwarder,
                    w.szWareHouseName,
                    w.szCity,
                    f.szDisplayName,
                    c.szCountryISO,
                    c.szCountryName,
                    w.idCountry,
                    w.szLatitude,
                    w.szLongitude,
                    w.szPostCode
                FROM
                    ".__DBC_SCHEMATA_WAREHOUSES__." AS w
                INNER JOIN
                    ".__DBC_SCHEMATA_COUNTRY__." AS c
                        ON( c.id = w.idCountry && c.iActive = '1')
                INNER JOIN
                    ".__DBC_SCHEMATA_FROWARDER__." AS f	
                        ON( f.id = w.idForwarder && f.iActive = '1' )
                WHERE
                    w.iActive = 1	
                    $query_and
                ORDER BY
                    c.szCountryName ASC,
                    w.szCity ASC,
                    f.szDisplayName ASC,
                    w.szWareHouseName ASC

            ";
            //echo "<br ><br>".$query."<br><br>";
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    while($row=$this->getAssoc($result))
                    {
                        $warehouseDataArr[]=$row;
                    }
                    return $warehouseDataArr;
                }
                else
                {
                    return array() ;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
	function getWareHouseCount($idWarehouse,$sourceDestination)
	{
		$query="
			SELECT
				count(*) as value
			FROM
				".__DBC_SCHEMATA_WAREHOUSES_PRICING__."
			WHERE
				".mysql_escape_custom($sourceDestination)." = ".mysql_escape_custom($idWarehouse)."
			AND 
				iActive = 1	
			AND
				dtExpiry >= NOW()		
		";
		//echo $query ;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$row=$this->getAssoc($result);
				return $row['value'];
			}
			else
			{
				return array() ;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function maximumPricingHaulage($idWarehouse,$szOriginDestination)
	{
		$query="
			SELECT
				count(ph.id) as total
			FROM
				".__DBC_SCHEMATA_HAULAGE_PRICING__." AS ph
			INNER JOIN
				".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__."	AS phm
			ON
				(phm.id = ph.idHaulagePricingModel)	
			WHERE
				phm.idWarehouse = ".mysql_escape_custom($idWarehouse)."
			AND 
				phm.iDirection  = ".mysql_escape_custom($szOriginDestination)."
			AND 
				phm.iActive = 1		
			AND
				ph.iActive = 1	
		";
		//echo $query ;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{	
				$row=$this->getAssoc($result);
				return $row['total'];
			}
			else
			{
				return false;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function saveCFS($data)
	{	
		//print_r($data);
		if($data['szCity'] == t($t_base.'fields/type_name'))
		{
			$data['szCity']	 = '';
		}
		if($data['szPostCode'] == t($t_base.'fields/optional'))
		{
			$data['szPostCode'] = '';
		}
		$this->set_iForwarder(trim(sanitize_all_html_input($data['idForwarder'])));
		$this->set_id(trim(sanitize_all_html_input($data['idEditWarehouse'])));
		$this->set_szWarehouseName(trim(sanitize_all_html_input($data['szCFSName'])));
		$this->set_szAddress1(trim(sanitize_all_html_input($data['szAddressLine1'])));
		$this->set_szAddress2(trim(sanitize_all_html_input($data['szAddressLine2'])));
		$this->set_szAddress3(trim(sanitize_all_html_input($data['szAddressLine3'])));
		$this->set_szCity(trim(sanitize_all_html_input($data['szCity'])));
		$this->set_szState(trim(sanitize_all_html_input($data['szState'])));
		$this->set_szCountry(trim(sanitize_all_html_input($data['szCountry'])));
		$this->set_szPostcode(trim(sanitize_all_html_input($data['szPostCode'])));
		$this->set_szPhoneNumber(trim(sanitize_all_html_input(urldecode(base64_decode($data['szPhoneNumberUpdate'])))));
		$this->set_szLatitude((float)sanitize_all_html_input(trim($data['szLatitude'])));
		$selectNearCFS = (sanitize_all_html_input(trim($data['selectNearCFS'])));	
                $this->iWarehouseType = $data['iWarehouseType'];
                if($this->szLatitude!=NULL)
                {
                    if($this->szLatitude< -90 || $this->szLatitude>90 || !is_numeric($this->szLatitude))
                    {
                        $this->addError( "szLatitude" , t($this->t_base_cfs.'error/latitude_must_be'));
                    } 
                }
                $this->set_szLongitude((float)sanitize_all_html_input(trim($data['szLongitude'])));
                if($this->szLongitude!=NULL)
                {
                    if($this->szLongitude< -180 || $this->szLongitude>180 || !is_numeric($this->szLongitude))
                    {
                        $this->addError( "szLongitude" , t($this->t_base_cfs.'error/longitude_must_be'));
                    } 
                }
                if ($this->error === true)
                {
                    return false;
                }
                if($this->szLatitude!=NULL && 	$this->szLongitude!=NULL && $this->szCountry!=NULL)
                {
                    $countryApiDetailAry = array();
                    $countryApiDetailAry = getAddressDetails($this->szLatitude,$this->szLongitude,$this->szCountry);
                    $countryISO = $countryApiDetailAry['szCountryCode'] ;

                    $countryDetails = $this->setCountryNameISO();	
                    if(strtoupper(trim($countryDetails['szCountryISO'])) != strtoupper(trim($countryISO)))
                    {
                        $this->addError( "idCountry" , t($this->t_base_cfs.'error/this_location_is_not_in')." ".$countryDetails['szCountryName'] .". ". t($this->t_base_cfs.'error/please_update_your_lat_lang') );
                    }
                } 
                if ($this->error === true)
                {
                    return false;
                }

                if(!empty($this->szPhoneNumber))
                {
                    $this->szPhoneNumber =$this->szPhoneNumber;
                }
                        
			if((int)$this->id > 0)
			{
				$oldLatitudeLoongitudeArr=$this->getLatitudeLongitude($this->id);				
				if($oldLatitudeLoongitudeArr['szLatitude']!=$this->szLatitude && $oldLatitudeLoongitudeArr['szLongitude']!=$this->szLongitude)
				{
				 	$timezone_url =	__ASK_GEO_API_URL__."/".__ASK_GEO_ACCOUNT_ID__."/".__ASK_GEO_API_KEY__."/query.".__ASK_GEO_FORMAT__."?databases=".__ASK_GEO_DATABATE__."&points=";
					$posturl =	$timezone_url."".$this->szLatitude.",".$this->szLongitude;
				 	$response  = 	$this->curl_get_file_contents($posturl);
				 	$decoded_response	= 	json_decode($response);
				  	$utcoffsetupdated	=	'';
				  	$utcoffsetupdated	=	$decoded_response->data[0]->TimeZone->CurrentOffsetMs;
				}
			
			}
			if($this->id > 0)
			{
				$query = " UPDATE ";
			}	
			else
			{
				$query = " INSERT INTO ";
			}		  
		
		$query .="
				".__DBC_SCHEMATA_WAREHOUSES__."
			SET
				szWareHouseName='".mysql_escape_custom($this->szWarehouseName)."',
				szAddress='".mysql_escape_custom($this->szAddress1)."',
				szAddress2='".mysql_escape_custom($this->szAddress2)."',
				szAddress3='".mysql_escape_custom($this->szAddress3)."',
				szPostCode='".mysql_escape_custom($this->szPostcode)."',
				szCity='".mysql_escape_custom(ucwords(strtolower($this->szCity)))."',
				szState='".mysql_escape_custom($this->szState)."',
				idCountry='".(int)$this->szCountry."',
				szPhone='".mysql_escape_custom($this->szPhoneNumber)."',
				szLatitude='".mysql_escape_custom($this->szLatitude)."',
				szLongitude='".mysql_escape_custom($this->szLongitude)."',
				dtUpdatedOn=NOW(),
				szUpdatedBy = 'Transporteca Support',
				idForwarderContact = 0,
				szUTCOffset='".mysql_escape_custom($utcoffsetupdated)."',
				idForwarder='".(int)$this->idForwarder."',
                                iWarehouseType='".(int)$this->iWarehouseType."',
				dtUTCOffsetUpdated = NOW()
			";
			if($this->id==0)
			{
				$query.=" ,
					iActive = 1,
					dtCreatedOn = NOW()
					";
			}
			if($this->id>0)
			{
				$query .= "
				WHERE
					id='".(int)$this->id."'
					";
			}
			
		//ECHO $query;die;
		if($result=$this->exeSQL($query))
		{	
                    if($selectNearCFS!='')
                    {
                        $this->removeWarehouseCountries($this->id);
                        $this->addWarehouseCountries($selectNearCFS,$this->id);
                    }
                    if($this->id <= 0)
                    {
                        $warehouseId = mysql_insert_id();
                        $this->cfsModelMapping($warehouseId);
                    }
                    return true;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function deleteCFS($id)
	{
            if($id>0)
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_WAREHOUSES__."
                    SET
                        iActive = 0	
                    WHERE
                        id=".(int)$id."	
                ";
                //echo $query ;
                if($result=$this->exeSQL($query))
                {
                    /*
                    * If this warehouse is added in any Rail transport then we also deletes that one
                    */
                    $kService = new cServices();
                    $kService->deleteRailTransportByWarehouseId($id);

                    if($this->iNumRows>0)
                    {	
                        $row=$this->getAssoc($result); 
                        return json_encode($row);
                    }
                    else
                    {
                        return array();
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            } 
	}
        
        
	function editSearchCFS($id)
	{
		$id	= 	(int)sanitize_all_html_input($id);
		if($id>0)
		{
			$query="
				SELECT
					id,
					szWareHouseName,
					szAddress,
					szAddress2,
					szAddress3,
					szPostCode,
					szCity,
					szState,
					idCountry,
					szPhone,
					szLatitude,
					szLongitude,
					idForwarder
				FROM
					".__DBC_SCHEMATA_WAREHOUSES__."
				WHERE
					id=".(int)$id."	
			";
			//echo $query ;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{	
					$row=$this->getAssoc($result);
					return json_encode($row);
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function cfsModelMapping($warehouseId)
	{
                $kHaulagePricing = new cHaulagePricing();
		$warehouseId	=	(int)sanitize_all_html_input($warehouseId);
		$iDirectArr=array('1','2');
		if($warehouseId>0)
		{
			foreach($iDirectArr as $iDirectArrs)
			{
                            $iDirect=$iDirectArrs;
                            if(!$this->checkCFSMappingWithHaulageModel($warehouseId,1,$iDirect))
                            {
                                $query="
					INSERT INTO
                                            ".__DBC_SCHEMATA_CFS_MODEL_MAPPING__."
					(
                                            idWarehouse,
                                            idHaulageModel,
                                            iPriority,
                                            dtCreatedOn,
                                            dtUpdatedOn,
                                            iActive,
                                            iDirection
					)
					VALUES
					(
                                            ".(int)$warehouseId.",
                                            1,
                                            1,
                                            NOW(),
                                            NOW(),
                                            0,
                                            '".(int)$iDirectArrs."'
					)		
				";
                                $result=$this->exeSQL($query);
                            }
                            if(!$this->checkCFSMappingWithHaulageModel($warehouseId,2,$iDirect))
                            {
                                $query="
					INSERT INTO
                                            ".__DBC_SCHEMATA_CFS_MODEL_MAPPING__."
					(
                                            idWarehouse,
                                            idHaulageModel,
                                            iPriority,
                                            dtCreatedOn,
                                            dtUpdatedOn,
                                            iActive,
                                            iDirection
					)
					VALUES
					(
                                            ".(int)$warehouseId.",
                                            2,
                                            2,
                                            NOW(),
                                            NOW(),
                                            0,
                                            '".(int)$iDirectArrs."'
					)		
				";
                                $result=$this->exeSQL($query);
                            }
                            if(!$this->checkCFSMappingWithHaulageModel($warehouseId,3,$iDirect))
                            {
                                $query="
					INSERT INTO
                                            ".__DBC_SCHEMATA_CFS_MODEL_MAPPING__."
					(
                                            idWarehouse,
                                            idHaulageModel,
                                            iPriority,
                                            dtCreatedOn,
                                            dtUpdatedOn,
                                            iActive,
                                            iDirection
					)
					VALUES
					(
                                            ".(int)$warehouseId.",
                                            3,
                                            3,
                                            NOW(),
                                            NOW(),
                                            0,
                                            '".(int)$iDirectArrs."'
					)		
				";
                                $result=$this->exeSQL($query);
                            }
                            if(!$this->checkCFSMappingWithHaulageModel($warehouseId,4,$iDirect))
                            {
                                $query="
					INSERT INTO
                                            ".__DBC_SCHEMATA_CFS_MODEL_MAPPING__."
					(
                                            idWarehouse,
                                            idHaulageModel,
                                            iPriority,
                                            dtCreatedOn,
                                            dtUpdatedOn,
                                            iActive,
                                            iDirection
					)
					VALUES
					(
                                            ".(int)$warehouseId.",
                                            4,
                                            4,
                                            NOW(),
                                            NOW(),
                                            0,
                                            '".(int)$iDirectArrs."'
					)		
				";
                                $result=$this->exeSQL($query);
                            }
				
			
				/*if($result=$this->exeSQL($query))
				{
                                    //return true;
				}
				else
				{
                                    $this->error = true;
                                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                                    return false;
				}*/
			}
			return true;
		}	
	}
	function findForwarderFirstLastName($forwarderContactId)
	{
		$forwarderContactId	= 	(int)sanitize_all_html_input($forwarderContactId);
		if($forwarderContactId>0)
		{
			if((int)$_SESSION['forwarder_admin_id']>0)
			{
				//$szTableName = __DBC_SCHEMATA_MANAGEMENT__;
				return __CFS_UPDATED_BY_ADMIN_TEXT__ ;
			}
			else
			{
				$szTableName = __DBC_SCHEMATA_FORWARDERS_CONTACT__;
			}
			
			$query="
				SELECT 
					szFirstName,
					szLastName
				FROM 
					".$szTableName."
				WHERE
					id = ".(int)$forwarderContactId."		
			";
			//echo $query ;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{	
					$row=$this->getAssoc($result);
					return ($row['szFirstName']." ".$row['szLastName']);
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}	
		}	
	}
	function getAllWareHousesData($data)
	{
            if(!empty($data))
            {
                if($data['idForwarder'])
                {
                    $query_and = " AND f.id = '".(int)$data['idForwarder']."'";
                } 
                if($data['idCountry'])
                {
                    $query_and = " AND c.id = '".(int)$data['idCountry']."'";
                }
                if($data['iWarehouseType']>0)
                {
                    $query_and = " AND w.iWarehouseType = '".(int)$data['iWarehouseType']."'";
                }
            }
            $query="
                SELECT 
                    w.`id`,	
                    w.`idForwarder` ,
                    w.`szWareHouseName` ,
                    w.`szAddress` , 
                    w.`szAddress2` , 
                    w.`szAddress3` , 
                    w.`szPostCode` , 
                    w.`szCity` , 
                    w.`szState` ,  
                    w.`szLatitude` , 
                    w.`szLongitude` , 
                    c.szCountryName,
                    f.szDisplayName
                FROM
                    ".__DBC_SCHEMATA_WAREHOUSES__." AS w
                INNER JOIN
                    ".__DBC_SCHEMATA_FROWARDER__." AS f
                ON
                    ( f.id = w.`idForwarder` && f.iActive = '1')
                INNER JOIN
                    ".__DBC_SCHEMATA_COUNTRY__." AS c
                ON
                    (c.id=w.idCountry && c.iActive = '1')
                WHERE
                    w.iActive='1'
                    $query_and
                ORDER BY 
                    w.idForwarder	
            ";
            //echo $query ;
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    while($row=$this->getAssoc($result))
                    {
                        $warehouseDataArr[$row['idForwarder']][]=$row;
                    }
                    return $warehouseDataArr;
                }
                else
                {
                    return array() ;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }	
	}
	function selectCfsTempData($idBatch,$iWarehouseType = __WAREHOUSE_TYPE_CFS__)
	{
            $idBatch = (int)sanitize_all_html_input($idBatch);
            if($idBatch>0)
            {
                $query="
                    SELECT
                        WT.`id`,
                        WT.`szWareHouseName` ,
                        WT.`szAddress1` ,
                        WT.`szAddress2` ,
                        WT.`szAddress3` ,
                        WT.`szPostCode` ,
                        WT.`szCity` ,
                        WT.`szState` ,
                        WT.`szPhone` ,
                        WT.szEmail,
                        WT.szContactPerson,
                        WT.`szLatitude` ,
                        WT.`szLongitude` ,
                        WT.`idBatch`,
                        AA.`idForwarder`,
                        AA.`idForwarderContact`,
                        c.szCountryName as szCountry
                    FROM
                        ".__DBC_SCHEMATA_WAREHOUSE_TEMP_DATA__."	AS WT
                    INNER JOIN 
                        ".__DBC_SCHEMATA_AWATING_APPROVAL__."	 AS AA
                    ON
                        (WT.idBatch = AA.id)
                    INNER JOIN
                        ".__DBC_SCHEMATA_COUNTRY__." AS c
                    ON
                        c.id=WT.idCountry	
                    WHERE
                        idBatch = ".(int)$idBatch."
                    AND
                        WT.iWarehouseType = '".$iWarehouseType."'  
                    ORDER BY 
                        id		
                    LIMIT 0,1

                ";
                //echo "<br><br>".$query ;
                if($result = $this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        $row=$this->getAssoc($result);
                        if($this->setWarehouseContent($row,$iWarehouseType))
                        {
                            return true ;
                        }
                        else
                        {
                            return false;
                        }	
                    }
                    else
                    {
                        return false ;
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            } 
	}
        
	function setWarehouseContent($row,$iWarehouseType=false)
	{
		
		if($row!= array())
		{
			$this->idBatch = $row['idBatch'];
			$this->tempDataId=sanitize_all_html_input(trim($row['id']));	
			$this->set_iForwarder(sanitize_all_html_input(trim($row['idForwarder'])));	
			//$this->tempDataId=sanitize_all_html_input(trim($row['idForwarderContct']));					
			$this->set_szWarehouseName(sanitize_all_html_input(trim($row['szWareHouseName'])));
			$this->set_szAddress1(sanitize_all_html_input(trim($row['szAddress1'])));
			$this->set_szAddress2(sanitize_all_html_input(trim($row['szAddress2'])));
			$this->set_szAddress3(sanitize_all_html_input(trim($row['szAddress3'])));
			$this->set_szPostCode(sanitize_all_html_input(trim($row['szPostCode'])));
			$this->set_szCity(sanitize_all_html_input(trim($row['szCity'])));
			$this->set_szState(sanitize_all_html_input(trim($row['szState'])));
			$this->CountryName = sanitize_all_html_input(trim($row['szCountry']));
			if($this->CountryName!='')
			{
				$this->setCountryId();
			}
			else
			{
				$this->addError( "idCountry" ,  t($this->t_base_cfs.'error/country_name_req'));
			}
			$this->set_szPhoneNumber(sanitize_all_html_input(trim($row['szPhone'])));
			$this->set_szEmail(sanitize_all_html_input(trim($row['szEmail'])));
			$this->set_szContactPerson(sanitize_all_html_input(trim($row['szContactPerson'])));
			$this->set_szLatitude(sanitize_all_html_input(trim($row['szLatitude'])));

			if($this->szLatitude!=NULL)
			{
				if($this->szLatitude< -90 || $this->szLatitude>90)
				{
					$this->addError( "szLatitude" , t($this->t_base_cfs.'error/latitude_must_be'));
				}
			
			}
			$this->set_szLongitude(sanitize_all_html_input(trim($row['szLongitude'])));
			if($this->szLongitude!=NULL)
			{
				if($this->szLongitude< -180 || $this->szLongitude>180)
				{
					$this->addError( "szLongitude" , t($this->t_base_cfs.'error/longitude_must_be'));
				}
			
			}
			if($this->idBatch && $this->idCountry!=null && $this->szCity!='')
			{
				if(isset($_SESSION['idBatch']))
				{
					if($_SESSION['show_once']!=1)
					{
						$this->countWareHouse($iWarehouseType);
						$_SESSION['show_once'] =1;
					}
				}
			}
			if($this->szLatitude!=NULL && 	$this->szLongitude!=NULL && $this->idCountry!=NULL)
			{
				$countryApiDetailAry = array();
				$countryApiDetailAry = getAddressDetails($this->szLatitude,$this->szLongitude,$this->szCountry);
				$countryISO = $countryApiDetailAry['szCountryCode'] ;
                        
				if(strtoupper(trim($this->countryISO)) != strtoupper(trim($countryISO)))
				{
					$this->addError( "idCountry" , t($this->t_base_cfs.'error/this_location_is_not_in')." ".$this->CountryName .". ". t($this->t_base_cfs.'error/please_update_your_lat_lang') );
				}
			}
			if ($this->error === true)
			{
				return false;
			}
		//	if(!$this->findCfsNameApproval())
		//	{
			//	return false;
		//	}
			
			
			return true;
		}
		else
		{
			return false;
		}
		
	}
	function addWarehouseContent($row)
	{
            if($row!= array())
            {			
                if(isset($row['idBatch']))
                {
                    $this->idBatch = sanitize_all_html_input((int)$row['idBatch']);
                    $this->tempDataId=sanitize_all_html_input(trim($row['tempDataId']));
                }
                else
                {
                    $this->set_id((int)$row['idEditWarehouse']);
                }
                $this->idForwarder=sanitize_all_html_input((int)$_SESSION['forwarder_id']);	 
                $this->set_szWarehouseName(sanitize_all_html_input(trim($row['szWareHouseName'])));
                $this->set_szAddress1(sanitize_all_html_input(trim($row['szAddress1'])));
                $this->set_szAddress2(sanitize_all_html_input(trim($row['szAddress2'])));
                $this->set_szAddress3(sanitize_all_html_input(trim($row['szAddress3'])));
                $this->set_szPostCode(sanitize_all_html_input(trim($row['szPostCode'])));
                $this->set_szCity(sanitize_all_html_input(trim($row['szCity'])));
                $this->set_szState(sanitize_all_html_input(trim($row['szState'])));
                $this->set_szCountry(sanitize_all_html_input(trim($row['szCountry'])));
                $this->set_szPhoneNumber(sanitize_all_html_input(trim(urldecode(base64_decode($row['szPhoneNumberUpdate'])))));
                $this->set_szLatitude(sanitize_all_html_input(trim($row['szLatitude'])));
                $this->iWarehouseType = (int)$row['iWarehouseType'];


                $this->set_szEmail(sanitize_all_html_input(trim($row['szEmail'])));
                $this->set_szContactPerson(sanitize_all_html_input(trim($row['szContactPerson'])));

                if($this->szLatitude!=NULL)
                { 
                    if($this->szLatitude< -90 || $this->szLatitude>90 || !is_numeric($this->szLatitude))
                    {
                        $this->addError( "szLatitude" , t($this->t_base_cfs.'error/latitude_must_be'));
                    } 
                }
                $this->set_szLongitude(sanitize_all_html_input(trim($row['szLongitude'])));
                if($this->szLongitude!=NULL)
                {
                    if($this->szLongitude< -180 || $this->szLongitude>180 || !is_numeric($this->szLongitude))
                    {
                        $this->addError( "szLongitude" , t($this->t_base_cfs.'error/longitude_must_be'));
                    }

                }
                if ($this->error === true)
                {
                    return false;
                }
                if($this->szLatitude!=NULL && 	$this->szLongitude!=NULL && $this->szCountry!=NULL)
                {
                    $countryApiDetailAry = array();
                    $countryApiDetailAry = getAddressDetails($this->szLatitude,$this->szLongitude,$this->szCountry);
                    $countryISO = $countryApiDetailAry['szCountryCode'] ;
                    $szCountryName = $countryApiDetailAry['szCountryName'];    
                    $countryDetails = $this->setCountryNameISO();	
                    if((strtoupper(trim($countryDetails['szCountryISO'])) != strtoupper(trim($countryISO))) && (strtoupper(trim($countryDetails['szCountryName'])) != strtoupper(trim($szCountryName))))
                    {
                        $this->addError( "idCountry" , t($this->t_base_cfs.'error/this_location_is_not_in')." ".$countryDetails['szCountryName'] .". ". t($this->t_base_cfs.'error/please_update_your_lat_lang') );
                    }
                }

                $query="
                    SELECT 
                        id
                    FROM
                        ".__DBC_SCHEMATA_WAREHOUSES__."	
                    WHERE
                        idForwarder = 	'".(int)$this->idForwarder."'	
                    AND
                        idCountry = '".(int)$this->szCountry."'	
                    AND	
                        LOWER(szCity) = '".mysql_escape_custom(strtolower($this->szCity))."'
                    AND
                        szWareHouseName='".mysql_escape_custom($this->szWarehouseName)."'
                    AND
                        iWarehouseType = '".mysql_escape_custom($this->iWarehouseType)."' 
                    AND
                        iActive='1'	
                ";
                //echo $query ;
                //die;
                if($this->id!=NULL)
                {
                    $query.="
                            AND id<>".$this->id."
                    ";
                }
                //echo $query;
                if($result=$this->exeSQL($query))
                { 
                    if($this->iNumRows>0)
                    {
                        if($this->iWarehouseType==__WAREHOUSE_TYPE_AIR__)
                        {
                            $this->addError( "szWareHouseName" ,t($this->t_base_cfs.'error/air_name_already_exist_in_this_city'));
                        }
                        else
                        {
                            $this->addError( "szWareHouseName" ,t($this->t_base_cfs.'error/cfs_name_already_exist_in_this_city'));
                        } 
                        return false;
                    }
                } 
                if ($this->error === true)
                {
                    return false;
                }

                if(isset($_SESSION['idBatch']) && $_SESSION['show_once']!=1)
                {
                    $this->countWareHouse($this->iWarehouseType);
                    $_SESSION['show_once'] =1;
                }	
                if ($this->error === true)
                {
                    return false;
                }
                return true;
            }
	}
	function countWareHouse($iWarehouseType=false)
	{
            $this->countWarehouse = $this->findTotalWarehouseCity($iWarehouseType);
            if($this->countWarehouse>0)
            {
                if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
                {
                    $this->addError( "countWarehouse" ,t($this->t_base_cfs.'error/note').': '. t($this->t_base_cfs.'error/you_already')." ".$this->countWarehouse." ".t($this->t_base_cfs.'error/air_other_cfs') );
                }
                else
                {
                    $this->addError( "countWarehouse" ,t($this->t_base_cfs.'error/note').': '. t($this->t_base_cfs.'error/you_already')." ".$this->countWarehouse." ".t($this->t_base_cfs.'error/other_cfs') );
                } 
                $this->error = true;
                return true;
            } 
	}
	function findTotalWarehouseCity($iWarehouseType=false)
        {
            $country = $this->idCountry?$this->idCountry:$this->szCountry;
            if($this->szCity!='')
            {
                if($iWarehouseType>0)
                {
                    $query_and = " AND iWarehouseType = '".(int)$iWarehouseType."'";
                }
                $query="
                    SELECT 
                        count(id) as value
                    FROM
                        ".__DBC_SCHEMATA_WAREHOUSES__."	
                    WHERE
                        idForwarder = 	".(int)$this->idForwarder."	
                    AND
                        idCountry = ".(int)$country."	
                    AND	
                        szCity = '".mysql_escape_custom($this->szCity)."'
                    AND
                        iActive='1'
                    AND
                        isDeleted = '0'
                    $query_and
                ";
                //echo $query ;
                if($this->id!=NULL)
                {
                        $query.="
                                AND id<>".$this->id."
                        ";
                }
                //echo $query ;
                if($result=$this->exeSQL($query))
                {
                    $row=$this->getAssoc($result);
                    return $row['value'];
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
	function findCfsNameApproval()
	{
		$country = $this->idCountry?$this->idCountry:$this->szCountry;
		if($this->szCity!='' && $this->szWarehouseName!='' )
		{
			$query="
				SELECT 
					count(id) as value
				FROM
					".__DBC_SCHEMATA_WAREHOUSES__."	
				WHERE
					idForwarder = 	".(int)$this->idForwarder."	
				AND	
					szCity = '".mysql_escape_custom($this->szCity)."'
				AND
					idCountry = ".(int)$country."	
				AND	
					szWareHouseName = '".mysql_escape_custom($this->szWarehouseName)."'
					";
			if($this->id!=NULL)
			{
				$query.="
					AND id<>".$this->id."
				";
			}
			//ECHO $query;
			if($result=$this->exeSQL($query))
			{
				$row=$this->getAssoc($result);
				if($row['value']>0)
				{
					$countryDetails = $this->setCountryNameISO();
					$this->addError( "szWareHouseName" ,t($this->t_base_cfs.'error/you_already_have').' '.($countryDetails['szCountryName']!=''?$countryDetails['szCountryName']:$this->CountryName) );
					return false;
				}
				else
				{
					return true;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function setCountryId()
	{
		if($this->CountryName!='')
		{
			$query="
				SELECT 
					id,
					szCountryISO
				FROM
					".__DBC_SCHEMATA_COUNTRY__."	
				WHERE
					szCountryName = '".mysql_escape_custom($this->CountryName)."'
					";
			if($result=$this->exeSQL($query))
			{
				if($row=$this->getAssoc($result))
				{
					$this->idCountry = $row['id'];
					$this->countryISO = $row['szCountryISO'];
				}
				else
				{
					$this->idCountry = NULL;
					$this->countryISO = '';
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}	
	}
	function setCountryNameISO($szCountryName=false)
	{
            if($this->szCountry!='' || !empty($szCountryName))
            {
                if(!empty($szCountryName))
                {
                    $query_where = " LOWER(szCountryISO) = '".mysql_escape_custom(strtolower($szCountryName))."' OR LOWER(szCountryName) = '".mysql_escape_custom(strtolower($szCountryName))."' ";
                }
                else
                {
                    $query_where = " id = '".(int)$this->szCountry."' ";
                }
                $query="
                    SELECT 
                        szCountryISO,
                        szCountryName
                    FROM
                        ".__DBC_SCHEMATA_COUNTRY__."	
                    WHERE
                        $query_where
                    ";
                //echo $query;
                if($result=$this->exeSQL($query))
                {
                    if($row=$this->getAssoc($result))
                    {
                        return $row;
                    }
                    else
                    {
                        return array();
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }	
	}
	function addWarehouse($data)
	{		 
		if(!$this->addWarehouseContent($data))
		{ 	
                    return false; 
		}
		$idForwarderContact		=	(int)$_SESSION['forwarder_user_id'];
		$forwarderContactName	=	$this->findForwarderFirstLastName($idForwarderContact); 
		if ($this->error === true)
		{
			return false;
		}
		if((int)$_SESSION['forwarder_admin_id']>0)
		{
                    $idAdmin = (int)$_SESSION['forwarder_admin_id'];
                    $idForwarderContact = 0 ;
		}
		else
		{
                    $idAdmin = 0;
                    $idForwarderContact = (int)$_SESSION['forwarder_user_id'];
		}
		
		$timezone_url=__ASK_GEO_API_URL__."/".__ASK_GEO_ACCOUNT_ID__."/".__ASK_GEO_API_KEY__."/query.".__ASK_GEO_FORMAT__."?databases=".__ASK_GEO_DATABATE__."&points=";
		
		$posturl=$timezone_url."".$this->szLatitude.",".$this->szLongitude;
		$response = $this->curl_get_file_contents($posturl);
		$decoded_response = json_decode($response);
		$utcoffsetupdated='';
		$utcoffsetupdated=$decoded_response->data[0]->TimeZone->CurrentOffsetMs;
		
		if(!empty($this->szPhoneNumber))
		{
			$this->szPhoneNumber = $this->szPhoneNumber;
		}
		 
		$query="
			INSERT INTO
				".__DBC_SCHEMATA_WAREHOUSES__."
			(
				szWareHouseName,
				szAddress,
				szAddress2,
				szAddress3,
				szPostCode,
				szCity,
				szState,
				idCountry,
				szPhone,
				szLatitude,
				szLongitude,
				szEmail,
				szContactPerson,
				idForwarder,
				dtUpdatedOn,
				dtCreatedOn,
				iActive,
				idForwarderContact,
				idAdmin,
				szUpdatedBy,
				szUTCOffset,
                                iWarehouseType
			)	
				VALUES
			(	
				'".mysql_escape_custom($this->szWarehouseName)."',
				'".mysql_escape_custom($this->szAddress1)."',
				'".mysql_escape_custom($this->szAddress2)."',
				'".mysql_escape_custom($this->szAddress3)."',
				'".mysql_escape_custom($this->szPostcode)."',
				'".mysql_escape_custom(ucwords(strtolower($this->szCity)))."',
				'".mysql_escape_custom($this->szState)."',
				'".(int)$this->szCountry."',
				'".mysql_escape_custom($this->szPhoneNumber)."',
				'".mysql_escape_custom($this->szLatitude)."',
				'".mysql_escape_custom($this->szLongitude)."',
				'".mysql_escape_custom($this->szEmail)."',
				'".mysql_escape_custom($this->szContactPerson)."', 
				'".(int)$this->idForwarder."',
				NOW(),
				NOW(),
				'1',
				'".(int)$idForwarderContact."',
				'".(int)$idAdmin."',
				'".mysql_escape_custom($forwarderContactName)."',
				'".mysql_escape_custom($utcoffsetupdated)."',
                                '".mysql_escape_custom($this->iWarehouseType)."'
			 )	
		";
		//ECHO $query;
		if($result=$this->exeSQL($query))
		{	
			$this->showMessageUnset();
			
			$warehouseID = (int)$this->iLastInsertID;
			if($warehouseID==NULL)
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
			$this->cfsModelMapping($warehouseID);
			$warehouseContry  = sanitize_all_html_input($data['selectNearCFS']);
			if($warehouseContry!='')
			{
                            $this->addWarehouseCountries($warehouseContry,$warehouseID);
			}
			if($this->tempDataId)
			{
                            $cfsUploaded = $this->dleteWarehouse($this->tempDataId,$this->idBatch,$this->iWarehouseType);
                            $this->showMessageUnset();
                            if((int)$cfsUploaded>0)
                            {
                                $_SESSION['completed'] +=1;
                            }
			}
		}
		else
		{
			return false;
		}
	}
	function showMessageUnset()
	{
		if(isset($_SESSION['show_once']))
		{
			unset($_SESSION['show_once']);
		}
	}
	function saveWarehouse($data)
	{		
		if(!$this->addWarehouseContent($data))
		{
                    return false; 
		}
		$idForwarderContact = (int)$_SESSION['forwarder_user_id'];
		$forwarderContactName = $this->findForwarderFirstLastName($idForwarderContact); 
		if ($this->error === true)
		{
                    return false;
		}
		
		if((int)$this->id > 0)
		{
			$oldLatitudeLoongitudeArr=$this->getLatitudeLongitude($this->id);
			
			if($oldLatitudeLoongitudeArr['szLatitude']!=$this->szLatitude && $oldLatitudeLoongitudeArr['szLongitude']!=$this->szLongitude)
			{
			 	$timezone_url		=	__ASK_GEO_API_URL__."/".__ASK_GEO_ACCOUNT_ID__."/".__ASK_GEO_API_KEY__."/query.".__ASK_GEO_FORMAT__."?databases=".__ASK_GEO_DATABATE__."&points=";
				$posturl			=	$timezone_url."".$this->szLatitude.",".$this->szLongitude;
			 	$response 			= 	$this->curl_get_file_contents($posturl);
			 	$decoded_response	= 	json_decode($response);
			  	$utcoffsetupdated	=	'';
			  	$utcoffsetupdated	=	$decoded_response->data[0]->TimeZone->CurrentOffsetMs;
			}
		
		}
		
		if((int)$_SESSION['forwarder_admin_id']>0)
		{
			$idAdmin = (int)$_SESSION['forwarder_admin_id'];
			$idForwarderContact = 0 ;
		}
		else
		{
			$idAdmin = 0;
			$idForwarderContact = (int)$_SESSION['forwarder_user_id'];
		}
		
		if(!empty($this->szPhoneNumber))
		{
			$this->szPhoneNumber = $this->szPhoneNumber;
		}
		
		$query="
			UPDATE
                            ".__DBC_SCHEMATA_WAREHOUSES__."
			SET 
                            szWareHouseName = '".mysql_escape_custom($this->szWarehouseName)."',
                            szAddress = '".mysql_escape_custom($this->szAddress1)."',
                            szAddress2 = '".mysql_escape_custom($this->szAddress2)."',
                            szAddress3 = '".mysql_escape_custom($this->szAddress3)."',
                            szPostCode = '".mysql_escape_custom($this->szPostcode)."',
                            szCity = '".mysql_escape_custom(ucwords(strtolower($this->szCity)))."',
                            szState = '".mysql_escape_custom($this->szState)."',
                            idCountry = '".(int)$this->szCountry."',
                            szPhone = '".mysql_escape_custom($this->szPhoneNumber)."',
                            szLatitude = '".mysql_escape_custom($this->szLatitude)."',
                            szLongitude = '".mysql_escape_custom($this->szLongitude)."',
                            szEmail = '".mysql_escape_custom($this->szEmail)."',
                            szContactPerson = '".mysql_escape_custom($this->szContactPerson)."',
                            idForwarder = '".(int)$this->idForwarder."',
                ";
		if($utcoffsetupdated!=0 || $utcoffsetupdated!=NULL || $utcoffsetupdated!='')
		{
			$query.="
				szUTCOffset = '".$utcoffsetupdated."',
				dtUTCOffsetUpdated = NOW(),";
		}
		$query.="
                        dtCreatedOn = NOW(),
                        dtUpdatedOn = NOW(),
                        idForwarderContact = '".(int)$idForwarderContact."',
                        idAdmin = '".(int)$idAdmin."',
                        szUpdatedBy = '".mysql_escape_custom($forwarderContactName)."',
                        iWarehouseType = '".$this->iWarehouseType."'
                    WHERE
                        id = ".(int)$this->id."	
		";
		//echo  $query;
		//die;
		if($result=$this->exeSQL($query))
		{
			
			$warehouseContry  = sanitize_all_html_input($data['selectNearCFS']);
			$this->removeWarehouseCountries($this->id);
			if($warehouseContry!='')
			{
                            //$this->removeWarehouseCountries($this->id);
                            $this->addWarehouseCountries($warehouseContry,$this->id);
			}
				
		}
		else
		{
			return false;
		}
	}
	function findTotalCFS($idBatch,$iWarehouseType)
	{
		$query = "
			SELECT 
                            count(wh.id) as numCount,
                            aa.iRecords
			FROM 
                            ".__DBC_SCHEMATA_WAREHOUSE_TEMP_DATA__." as wh
			INNER JOIN
                            ".__DBC_SCHEMATA_AWATING_APPROVAL__." AS aa
                            ON (wh.idBatch = aa.id)	
			WHERE
                            aa.id =".(int)$idBatch."
                        AND
                            wh.iWarehouseType = '".(int)$iWarehouseType."'
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($row=$this->getAssoc($result))
			{ 
				if(!isset($_SESSION['CFS']))
				{
                                    $_SESSION['CFS'] = $row['iRecords'];
				}
				return $row['numCount'];
			}
		}
	
	}
	function dleteWarehouse($id,$idBatch,$iWarehouseType=__WAREHOUSE_TYPE_CFS__)
	{
            $this->idBatch = (int)$idBatch;
            $this->tempDataId = (int)$id;
            if($this->tempDataId>0)
            {
                $query = "
                    DELETE FROM 
                        ".__DBC_SCHEMATA_WAREHOUSE_TEMP_DATA__."
                    WHERE
                        id =".(int)$this->tempDataId."
                ";
                if($result=$this->exeSQL($query))
                {
                    $query = "
                        SELECT 
                            count(wh.id) as numCount
                        FROM 
                            ".__DBC_SCHEMATA_WAREHOUSE_TEMP_DATA__." as wh
                        INNER JOIN
                            ".__DBC_SCHEMATA_AWATING_APPROVAL__." AS aa
                        ON (wh.idBatch = aa.id)	
                        WHERE
                            aa.id =".(int)$this->idBatch."
                        AND
                            wh.iWarehouseType = '".(int)$iWarehouseType."' 
                    ";
                    //echo $query;
                    if($result=$this->exeSQL($query))
                    {
                        if($row=$this->getAssoc($result))
                        {
                            if((int)$row['numCount']!=0)
                            {
                                $query = "
                                    UPDATE
                                        ".__DBC_SCHEMATA_AWATING_APPROVAL__." AS aa
                                    SET
                                        iRecords = iRecords -1						
                                    WHERE
                                        aa.id =".$this->idBatch."
                                ";
                                if($result=$this->exeSQL($query))
                                {	
                                    $_SESSION['eachCfs'] =$_SESSION['eachCfs']+1;
                                    return true;
                                }
                                else
                                {
                                    return false;
                                }
                            }
                            else
                            {
                                if((int)$row['numCount']==0)
                                {
                                    $query = "
                                        DELETE FROM 
                                            ".__DBC_SCHEMATA_AWATING_APPROVAL__."
                                        WHERE
                                            id =".$this->idBatch."
                                    ";
                                    if($result=$this->exeSQL($query))
                                    {
                                        unset($_SESSION['idBatch'],$_SESSION['CFS'],$_SESSION['eachCfs'],$_SESSION['mode']);
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
	}
	
	function batchDelete($idBatch,$iWarehouseType=__WAREHOUSE_TYPE_CFS__)
	{
            $this->idBatch = (int)$idBatch; 
            $query = "
                DELETE FROM 
                    ".__DBC_SCHEMATA_WAREHOUSE_TEMP_DATA__."
                WHERE
                    idBatch =".(int)$this->idBatch."
            ";
            if($result=$this->exeSQL($query))
            {
                $query = "
                    DELETE FROM 
                        ".__DBC_SCHEMATA_AWATING_APPROVAL__."
                    WHERE
                        id =".(int)$this->idBatch."
                    AND
                        iWarehouseType = '".(int)$iWarehouseType."' 
                ";
                if($result=$this->exeSQL($query))
                {	
                    $this->showMessageUnset();
                    unset($_SESSION['idBatch'],$_SESSION['CFS'],$_SESSION['eachCfs'],$_SESSION['mode']);
                    return true;
                }
                else
                {
                    return false;
                }
            }
	}
	function findWarehouseCountryList($countryId,$latitude,$longitude)
	{
		$countryId = (int)$countryId;
		if($countryId>0)
		{
			$latitude  = (float)$latitude;
			$longitude = (float)$longitude;
			if($latitude ==NULL && $longitude==NULL)
			{
				$getLatitudeLongitude = $this->getLatitudeLongitudeForCountry((int)$countryId);
				$latitude  = $getLatitudeLongitude['szLatitude'];
				$longitude = $getLatitudeLongitude['szLongitude'];
			}
			$maxWarehouseDistance = $this->getManageMentVariableByDescription('__MAX_WAREHOUSE_DISTANCE_TO_BE_CONSIDER__'); 
			$query="
				SELECT 
					id,
					szCountryName,
				   (( 3959 * acos( cos( radians(".(float)$latitude.") ) * cos( radians( szLatitude ) ) 
				   * cos( radians(szLongitude) - radians(".(float)$longitude.")) + sin(radians(".(float)$latitude.")) 
				   * sin( radians(szLatitude)))) * 1.609344) 
				   AS distance
				FROM 
					".__DBC_SCHEMATA_COUNTRY__."
				WHERE
					iActive = 1	
				HAVING 
					distance < ".(int)$maxWarehouseDistance."
				ORDER BY 
					distance
				";
				//echo $query;
			if($result=$this->exeSQL($query))
			{	
				//$countryDeatails = array();
				while($row=$this->getAssoc($result))
				{
					$countryDeatails[] = $row;	
				}
				return $countryDeatails;
			}
		}	
	}
	
 	function getLatitudeLongitudeForCountry($idCountry)
	 {
	 	if($idCountry>0)
		{
			$query="
				SELECT
					szLatitude,
					szLongitude					
				FROM
					".__DBC_SCHEMATA_COUNTRY__."	
				WHERE
					id = '".(int)$idCountry."'	
			";
			//echo "<br><br>".$query ;
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					return $row ;
				}
				else
				{
					return false ;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	 }
	function addWarehouseCountries($content,$idWarehouse)
	{
		if($content!='')
		{
			$content = explode(',',$content);
			//echo count($content);
			foreach($content as $key=>$value)
			{
				if($value!= NULL)
				{
					$data .= '('.(int)$idWarehouse.','.(int)$value.' ),';
				}
			}	
			$data = substr($data,0,-1);
			$query="
				INSERT INTO				
					".__DBC_SCHEMATA_WAREHOUSE_COUNTRIES__."	
				(
					warehouseID,
					countryID
				)
				VALUES
				".$data."
			";
			//echo "<br><br>".$query ;
			if($result = $this->exeSQL($query))
			{
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		} 
	}
	function removeWarehouseCountries($idWarehouse)
	{
		$query="
			DELETE FROM			
				".__DBC_SCHEMATA_WAREHOUSE_COUNTRIES__."	
			WHERE
				warehouseID=".(int)$idWarehouse."
			";
		//echo "<br><br>".$query ;
		if($result = $this->exeSQL($query))
		{
			return true;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function selectWarehouseCountries($idWarehouse)
	{
		$query="
			SELECT
				countryID
			FROM			
				".__DBC_SCHEMATA_WAREHOUSE_COUNTRIES__."	
			WHERE
				warehouseID=".(int)$idWarehouse."
			";
		//echo "<br><br>".$query ;
		if($result = $this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				while($row=$this->getAssoc($result))
				{
					$warehouseCountry[]  = $row['countryID'];
				}
				return $warehouseCountry;
			}
			else
			{
				return array() ;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	function getAllActiveCfsServices()
	{		
		$query="
			SELECT
				wtw.idWarehouseFrom,
				wtw.idWarehouseTo,
				wtw.iTransitHours,
				wtw.idFrequency,
				wtw.iOriginChargesApplicable,
				wtw.iDestinationChargesApplicable,
				wtw.dtValidFrom,
				wtw.dtExpiry,
				freq.szDescription,
				(SELECT isOnline FROM ".__DBC_SCHEMATA_FROWARDER__." f WHERE f.id=(SELECT idForwarder FROM ".__DBC_SCHEMATA_WAREHOUSES__." w WHERE w.id = wtw.idWarehouseFrom AND w.iActive=1)) iOriginForwarderOnline
			FROM			
				".__DBC_SCHEMATA_WAREHOUSES_PRICING__." wtw
			INNER JOIN
				 ".__DBC_SCHEMATA_FREQUENCY__." freq
			ON
				freq.id = wtw.idFrequency
			WHERE
				wtw.iActive = '1'
			AND
				DATE(dtExpiry) >= CURDATE()
			HAVING
				iOriginForwarderOnline >0
		";
		//echo "<br><br>".$query."<br><br>";
		//die;
		if($result = $this->exeSQL($query))
		{
			$cfsServiceAry = array();
			if($this->iNumRows>0)
			{
				$ctr = 0;
				while($row=$this->getAssoc($result))
				{
					$cfsServiceAry[$ctr]  = $row;
					$ctr++;
				}
				return $cfsServiceAry;
			}
			else
			{
				return array() ;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}			
	}
	
	function getWarehouseAllCountry($idWarehouse)
	{
		if($idWarehouse>0)
		{	
			$query="
				SELECT 
					wc.countryID,
					c.szCountryName,
					c.szLatitude,
					c.szLongitude 
				FROM 
					".__DBC_SCHEMATA_WAREHOUSE_COUNTRIES__." wc 
				INNER JOIN
					".__DBC_SCHEMATA_COUNTRY__." c
				ON
					c.id = wc.countryID 
				WHERE 
					wc.warehouseID ='".(int)$idWarehouse."'				
				";
			//echo "<br><br>".$query."<br><br>";
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$ctr=0;
					$warehouseCountry = array();
					while($row=$this->getAssoc($result))
					{
						$warehouseCountry[$ctr]  = $row;
						$ctr++;
					}
					return $warehouseCountry;
				}
				else
				{
					return array() ;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function getAllHaulageCountry($idWarehouse,$idDirection=false)
	{
		if($idWarehouse>0)
		{	
			$query="
				SELECT 
					hc.idCountry,
					hc.idCountry countryID,
					c.szCountryName,
					c.szLatitude,
					c.szLongitude 
				FROM 
					".__DBC_SCHEMATA_HAULAGE_COUNTRY__." hc 
				INNER JOIN
					".__DBC_SCHEMATA_COUNTRY__." c
				ON
					c.id = hc.idCountry 
				WHERE 
					hc.idWarehouse  = '".(int)$idWarehouse."'			
				AND
					hc.idDirection = '".(int)$idDirection."'
				";
			//echo "<br><br>".$query."<br><br>";
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$ctr=0;
					$warehouseCountry = array();
					while($row=$this->getAssoc($result))
					{
						$warehouseCountry[$ctr]  = $row;
						$ctr++;
					}
					return $warehouseCountry;
				}
				else
				{
					return array() ;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function addCFSCombinatin($data)
	{
		if(!empty($data))
		{
			$ctr=1;
			$ary_count = count($data);
			foreach($data as $datas)
			{
				if(((int)$datas['idForwarder']>0) && ((int)$datas['idOriginCountry']>0) && ((int)$datas['idDestinationCountry']>0))
				{
					$query="
						INSERT INTO
							".__DBC_SCHEMATA_SERVICE_AVAILABLE__."
						(	
							idForwarder,
							szForwarderName,
							idWarehouseFrom,
							idWarehouseTo,
							idOriginCountry,
							szOriginCountry,
							idDestinationCountry,
							szDestinationCountry,
							iPTP,
							iPTW,
							iPTD,
							iDTP,
							iWTW,
							iWTD,
							iDTW,
							iDTD,
							iWTP,
							szOriginCity,
							szDestinationCity,
							iTransitTime,
							idFrequency,
							szFrequency,
							dtValidFrom,
							dtValidTo,
							iStatus,
							iOriginCC,
							iDestinationCC,
							dtCreatedOn
						)
						VALUES					
					";
					$query .= "
						(
							'".(int)$datas['idForwarder']."',
							'".mysql_escape_custom($datas['szDisplayName'])."',
							'".(int)$datas['idWarehouseFrom']."',
							'".(int)$datas['idWarehouseTo']."',
							'".(int)$datas['idOriginCountry']."',
							'".mysql_escape_custom($datas['szOriginCountryName'])."',
							'".(int)$datas['idDestinationCountry']."',
							'".mysql_escape_custom($datas['szDestinationCountryName'])."',
							'".(int)$datas['iPTP']."',
							'".(int)$datas['iPTW']."',
							'".(int)$datas['iPTD']."',
							'".(int)$datas['iDTP']."',
							'".(int)$datas['iWTW']."',
							'".(int)$datas['iWTD']."',
							'".(int)$datas['iDTW']."',
							'".(int)$datas['iDTD']."',	
							'".(int)$datas['iWTP']."',								
							'".mysql_escape_custom($datas['szOriginCity'])."',
							'".mysql_escape_custom($datas['szDestinationCity'])."',
							'".(int)$datas['iTransitTime']."',		
							'".(int)$datas['idFrequency']."',				
							'".mysql_escape_custom($datas['szFrequency'])."',
							'".mysql_escape_custom($datas['dtValidFrom'])."',
							'".mysql_escape_custom($datas['dtValidTo'])."',
							'3',
							'".(int)$datas['iOriginCC']."',
							'".(int)$datas['iDestinationCC']."',
							now()
						)
					";
					//echo "<br><br/> ".$query."<br /><br> ";
					if($datas['idWarehouseTo']==220)
					{
						//echo "<br><br/> ".$query."<br /><br> ";
					}
					if($result = $this->exeSQL($query))
					{
						//return true;
					}
					else
					{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					}
					/*
					if($ctr>0 && $ctr!=$ary_count)
					{
						$query .=",";
					}
					*/
				}
				$ctr++;
			}
		}
	}
	
	function deleteOldServices()
	{
		$query="
			DELETE FROM
				".__DBC_SCHEMATA_SERVICE_AVAILABLE__."
			WHERE
				iStatus = '2'
		";
		//echo "<br /> ".$query."<br />";
		if($result = $this->exeSQL($query))
		{
			//return true;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	function updateAvailabelServices()
	{		
		/*
		* Update all old service with status=2 Where iStatus = '1'
		*/
		$query="
			UPDATE
				".__DBC_SCHEMATA_SERVICE_AVAILABLE__."
			SET
				iStatus = '2'
			WHERE
				iStatus = '1'
		";
		//echo "<br /> ".$query."<br />";
		if($result = $this->exeSQL($query))
		{
			//return true;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
		
		/*
		* Update all newly added service with status=1 Where iStatus = '3'
		*/
		
		$query="
			UPDATE
				".__DBC_SCHEMATA_SERVICE_AVAILABLE__."
			SET
				iStatus = '1'
			WHERE
				iStatus = '3'
		";
		//echo "<br /> ".$query."<br />";
		if($result = $this->exeSQL($query))
		{
			return true;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	
	function isHaulageServiceExist($idCountry,$idWarehouse,$idDirection)
	{
            $query="
                SELECT
                    hc.id
                FROM
                    ".__DBC_SCHEMATA_CFS_MODEL_MAPPING__." map
                INNER JOIN
                    ".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." models
                ON
                    map.idHaulageModel = models.idHaulageModel
                INNER JOIN
                    ".__DBC_SCHEMATA_HAULAGE_PRICING__." hp
                ON
                    hp.idHaulagePricingModel = models.id
                INNER JOIN			
                    ".__DBC_SCHEMATA_HAULAGE_COUNTRY__." hc
                ON
                    models.id = hc.idHaulagePricingModel
                WHERE
                    map.idWarehouse = '".(int)$idWarehouse."'
                AND
                    hc.idWarehouse = '".(int)$idWarehouse."'
                AND
                    hc.idCountry = '".(int)$idCountry."'
                AND
                    hc.idDirection ='".(int)$idDirection."'
                AND
                    map.iDirection ='".(int)$idDirection."'
                AND
                    models.iDirection = '".(int)$idDirection."'
                AND
                    map.iActive = '1'
                AND
                    hp.iActive = '1'
                AND
                    models.iActive = '1'
            ";
            //echo "<br/><br /> ".$query."<br>";
            if($idDirection==2)
            {
                    //echo "<br/>".$query."<br />";
            }
            if($result = $this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                        return 1;
                }
                else
                {
                        return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
	
	function getAllHaulageCountryByCFS($idWarehouse,$idDirection)
	{
		if($idWarehouse>0)
		{
                    if($idDirection>0)
                    {
                        $query_and = " AND idDirection = '".(int)$idDirection."' ";
                    }
                    $query="
                        SELECT
                            hd.idCountry
                        FROM					
                            ".__DBC_SCHEMATA_HAULAGE_COUNTRY__." hd
                        WHERE
                            hd.idWarehouse = '".(int)$idWarehouse."'
                            $query_and
                    ";
                    //echo "<br>".$query ;
                    if($result=$this->exeSQL($query))
                    {
                        if($this->iNumRows>0)
                        {
                            $ret_ary=array();
                            $ctr=0;
                            while($row = $this->getAssoc($result))
                            {
                                $ret_ary[$ctr] = $row['idCountry'];
                                $ctr++;
                            }
                            return $ret_ary ;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        $this->error = true;
                        $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                        $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                        return false;
                    }	
		}
	}
	/*
	function isHaulageServiceExist($idCountry,$idWarehouse,$idDirection)
	{
		if($idCountry>0)
		{
			$query="
				SELECT
					hc.id
				FROM
					".__DBC_SCHEMATA_HAULAGE_COUNTRY__." hc
				WHERE
					hc.idCountry = '".(int)$idCountry."'
				AND
					hc.idDirection ='".(int)$idDirection."'
			   AND
				   hc.idWarehouse = '".(int)$idWarehouse."'
				LIMIT
					0,1
			";
			echo "<br/> ".$query."<br>";
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$query="
						SELECT
							models.id
						FROM
							".__DBC_SCHEMATA_HAULAGE_PRICING__." hp
						INNER JOIN
						ON
							hp.idHaulagePricingModel = models.id
						INNER JOIN
							".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." models
						ON
							map.idHaulageModel = models.idHaulageModel
						INNER JOIN
							".__DBC_SCHEMATA_CFS_MODEL_MAPPING__." map
						WHERE
							map.idWarehouse = '".(int)$idWarehouse."'
						AND
							map.iDirection ='".(int)$idDirection."'
						AND
							models.iDirection = '".(int)$idDirection."'
						AND
							map.iActive = '1'
						AND
							hp.iActive = '1'
						AND
							models.iActive = '1'
						LIMIT
							0,1
					";
					echo "<br /> ".$query."<br /> ";
					if($result = $this->exeSQL($query))
					{
						if($this->iNumRows>0)
						{
							return 1;
						}
						else
						{
							return false;
						}
					}
					else
					{
						$this->error = true;
						$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
						$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	*/
	function getAllCountryServedByCFS($idWarehouse,$countryAry=array(),$idDirection)
	{
		if($idWarehouse>0)
		{
			if(!empty($countryAry))
			{
				$uniqueCountryAry = array_unique($countryAry);
				$country_str = implode(',',$uniqueCountryAry);
				$query_and = " AND
						hc.idCountry NOT IN (".$country_str.")";
			}
			$query="
				SELECT
					hc.id,
					hc.idCountry
				FROM
					".__DBC_SCHEMATA_CFS_MODEL_MAPPING__." map
				INNER JOIN
					".__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__." models
				ON
					map.idHaulageModel = models.idHaulageModel
				INNER JOIN
					".__DBC_SCHEMATA_HAULAGE_PRICING__." hp
				ON
					hp.idHaulagePricingModel = models.id
				INNER JOIN			
					".__DBC_SCHEMATA_HAULAGE_COUNTRY__." hc
				ON
					models.id = hc.idHaulagePricingModel
				WHERE
					map.idWarehouse = '".(int)$idWarehouse."'
				AND
					hc.idDirection ='".(int)$idDirection."'
				AND
					map.iDirection ='".(int)$idDirection."'
				AND
					models.iDirection = '".(int)$idDirection."'
				AND
					map.iActive = '1'
				AND
					hp.iActive = '1'
				AND
					models.iActive = '1'
				$query_and
			";
			
			//echo "<br /><br />".$query."<br /><br />";
			if($result = $this->exeSQL($query))
			{
				$ret_ary=array();
				$ctr=0;
				while($row = $this->getAssoc($result))
				{
					$ret_ary[$ctr] = $row;
					$ctr++;
				}
				return $ret_ary ;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function getAvailableService($idOriginCountry,$idDestinationCountry,$count_flag=false)
	{
		if((int)$idOriginCountry>0 && (int)$idDestinationCountry>0)
		{
			if($count_flag)
			{
                            $query_select = " 
                                    iPTP,
                                    iPTW,
                                    iPTD,
                                    iDTP,
                                    iWTW,
                                    iWTD,
                                    iDTW,
                                    iDTD,
                                    iWTP
                            ";
			}
			else
			{
                            $query_select = " s.id";
                            $query_join = " INNER JOIN ".__DBC_SCHEMATA_FROWARDER__." f ON f.id = s.idForwarder " ;
			}
			
			$query="
				SELECT
                                    ".$query_select."
				FROM
					".__DBC_SCHEMATA_SERVICE_AVAILABLE__." s
				WHERE
                                     s.idOriginCountry = '".(int)$idOriginCountry."'
				AND
                                    s.idDestinationCountry = '".(int)$idDestinationCountry."'
				AND
					s.dtValidTo > now()
				AND
					iStatus = '1'
			";
			//echo "<br />".$query."<br/>";
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					if($count_flag)
					{
						$service_counter = 0;
						while($row = $this->getAssoc($result))
						{
							if($row['iPTP']==1)
							{
								$service_counter++;
							}
							if($row['iPTW']==1)
							{
								$service_counter++;
							}
							if($row['iPTD']==1)
							{
								$service_counter++;
							}
							if($row['iDTP']==1)
							{
								$service_counter++;
							}
							if($row['iDTW']==1)
							{
								$service_counter++;
							}
							if($row['iDTD']==1)
							{
								$service_counter++;
							}							
							if($row['iWTP']==1)
							{
								$service_counter++;
							}
							if($row['iWTW']==1)
							{
								$service_counter++;
							}
							if($row['iWTD']==1)
							{
								$service_counter++;
							}
						}
						return $service_counter;
					}
					else 
					{
						$ret_ary=array();
						$ctr=0;
						while($row = $this->getAssoc($result))
						{
							$ret_ary[$ctr] = $row;
							$ctr++;
						}
						return $ret_ary ;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function getAllAvailableService($idOriginCountry,$idDestinationCountry,$szBookingRandomNum)
	{
		$query="
			SELECT
				s.id,
				s.szForwarderName,
				s.idOriginCountry,
				s.szOriginCountry,
				s.idDestinationCountry,
				s.szDestinationCountry,
				s.szFrequency,
				s.dtValidFrom,
				s.dtValidTo,
				s.szOriginCity,
				s.szDestinationCity,
				f.szLogo as szForwarderLogo,
				iPTP,
				iPTW,
				iPTD,
				iDTP,
				iWTW,
				iWTD,
				iDTW,
				iDTD,
				iWTP
			FROM
				".__DBC_SCHEMATA_SERVICE_AVAILABLE__." s
			INNER JOIN 
				".__DBC_SCHEMATA_FROWARDER__." f 
			ON 
				f.id = s.idForwarder
			WHERE
				s.idOriginCountry = '".(int)$idOriginCountry."'
			AND
				s.idDestinationCountry = '".(int)$idDestinationCountry."'
			AND
				s.dtValidTo > now()
			AND
				iStatus = '1'
			ORDER BY
				RAND()
		";
		//echo "<br />".$query."<br/>";
		if($result = $this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$ret_ary=array();
				$ctr=0;
				while($row = $this->getAssoc($result))
				{
					if($row['iPTP']==1)
					{
						$ret_ary[$ctr] = $row;
						$ret_ary[$ctr]['iServiceType'] = __SERVICE_TYPE_PTP__;
						$ctr++;
					}
					if($row['iPTW']==1)
					{
						$ret_ary[$ctr] = $row;
						$ret_ary[$ctr]['iServiceType'] = __SERVICE_TYPE_PTW__;
						$ctr++;
					}
					if($row['iPTD']==1)
					{
						$ret_ary[$ctr] = $row;
						$ret_ary[$ctr]['iServiceType'] = __SERVICE_TYPE_PTD__;
						$ctr++;
					}
					if($row['iDTP']==1)
					{
						$ret_ary[$ctr] = $row;
						$ret_ary[$ctr]['iServiceType'] = __SERVICE_TYPE_DTP__;
						$ctr++;
					}
					if($row['iDTW']==1)
					{
						$ret_ary[$ctr] = $row;
						$ret_ary[$ctr]['iServiceType'] = __SERVICE_TYPE_DTW__;
						$ctr++;
					}
					if($row['iDTD']==1)
					{
						$ret_ary[$ctr] = $row;
						$ret_ary[$ctr]['iServiceType'] = __SERVICE_TYPE_DTD__;
						$ctr++;
					}							
					if($row['iWTP']==1)
					{
						$ret_ary[$ctr] = $row;
						$ret_ary[$ctr]['iServiceType'] = __SERVICE_TYPE_WTP__;
						$ctr++;
					}
					if($row['iWTW']==1)
					{
						$ret_ary[$ctr] = $row;
						$ret_ary[$ctr]['iServiceType'] = __SERVICE_TYPE_WTW__;
						$ctr++;
					}
					if($row['iWTD']==1)
					{
						$ret_ary[$ctr] = $row;
						$ret_ary[$ctr]['iServiceType'] = __SERVICE_TYPE_WTD__;
						$ctr++;
					}
				}
				
				$randomSortedAry=array();
				if(!empty($ret_ary))
				{
				 	$randomSortedAry = random_sort_array($ret_ary);
				}
				//return $randomSortedAry ;
				
				if(!empty($randomSortedAry))
				{
					foreach($randomSortedAry as $randomSortedArys)
					{
						$query="
							INSERT INTO
								".__DBC_SCHEMATA_SERVICE_TEMP_DATA__."
							(
								szForwarderName,
								szForwarderLogo,
								szOriginCity,
								szDestinationCity,
								iServiceType,
								szFrequency,
								dtValidTo,
								szSearchNumber
							)
								VALUES
							(
								'".mysql_escape_custom($randomSortedArys['szForwarderName'])."',
								'".mysql_escape_custom($randomSortedArys['szForwarderLogo'])."',
								'".mysql_escape_custom($randomSortedArys['szOriginCity'])."',
								'".mysql_escape_custom($randomSortedArys['szDestinationCity'])."',
								'".mysql_escape_custom($randomSortedArys['iServiceType'])."',
								'".mysql_escape_custom($randomSortedArys['szFrequency'])."',
								'".mysql_escape_custom($randomSortedArys['dtValidTo'])."',
								'".mysql_escape_custom($szBookingRandomNum)."'
							)
						";
						$result = $this->exeSQL($query);
					}
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	function isServiceExists($idOriginCountry,$idDestinationCountry,$field_name=false)
	{
		if((int)$idOriginCountry>0 && (int)$idDestinationCountry>0)
		{
			if(!empty($field_name))
			{
				$query_select = " , ".mysql_escape_custom(trim($field_name));
				$query_and = " AND ".mysql_escape_custom(trim($field_name))." = '1'";
			}
						
			$query="
				SELECT
					id
					".$query_select."
				FROM
					".__DBC_SCHEMATA_SERVICE_AVAILABLE__." s
				WHERE
					s.idOriginCountry = '".(int)$idOriginCountry."'
				AND
					s.idDestinationCountry = '".(int)$idDestinationCountry."'
				AND
					iStatus = '1'
				$query_and
			";
			//echo "<br /><br />".$query."<br/><br />";
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function isHaulageExists($data,$idDirection)
	{
		if(!empty($data))
		{
			$warehoseAry=array();
			$idServiceType = $data['idServiceType'];
			$warehoseAry = $this->getAllAvailableWarehouseByCountry($data['idOriginCountry'],$data['idDestinationCountry'],$idServiceType);
		
			if(!empty($warehoseAry))
			{
				$haulage_pricing_exist_flag = false;
				if($idDirection==1)
				{
					$_SESSION[$data['id']."_orig"] = array();
					unset($_SESSION[$data['id']."_orig"]);
				}
				else
				{
					$_SESSION[$data['id']."_dest"]= array();
					unset($_SESSION[$data['id']."_dest"]);
				}
				$originWarehouseAry = array();
				$destinationWarehouseAry = array();
				$ctr=0;
				foreach($warehoseAry as $warehoseArys)
				{
					if($idDirection==1)
					{
						if(!empty($data['szOriginPostCode']))
						{
							$szPostCode = $data['szOriginPostCode'] ;
						}
						else
						{
							$szPostCode = $data['szOriginCity'] ;
						}
						
						$houlageData=array();
						$fFromWareHouseHaulageAry = array();
						$houlageData['idWarehouse'] = $warehoseArys['idWarehouseFrom'] ;
						$houlageData['iDirection'] = $idDirection; // origin haulage		
						
						$houlageData['szLatitude'] = $data['fOriginLatitude'] ;
						$houlageData['szLongitude'] = $data['fOriginLongitude'] ;
						$houlageData['szPostcode'] = $szPostCode;
						$houlageData['idCountry'] = $data['idOriginCountry'];	
						$houlageData['fTotalWeight'] = 1;
						$houlageData['fVolume'] = 0.1 ;
											
						$kHaulagePricing = new cHaulagePricing();
						$fFromWareHouseHaulageAry = $kHaulagePricing->getHaulageDetails($houlageData);
						
						if(!empty($fFromWareHouseHaulageAry))
						{
							$haulage_pricing_exist_flag = true;
							if(!empty($fFromWareHouseHaulageAry))
							{
								$haulage_pricing_exist_flag = true;
								if(!empty($_SESSION[$data['id']."_orig"]))
								{
									$_SESSION[$data['id']."_orig"][$ctr] .= "||||".$warehoseArys['idWarehouseFrom']."||".$warehoseArys['idWarehouseTo'];
								}
								else
								{
									$_SESSION[$data['id']."_orig"][$ctr] = $warehoseArys['idWarehouseFrom']."||".$warehoseArys['idWarehouseTo'];
								}
								$ctr++;
							}
							$ctr++;
						}
					}
					else
					{	
						if(!empty($data['szDestinationPostCode']))
						{
							$szPostCode = $data['szDestinationPostCode'] ;
						}
						else
						{
							$szPostCode = $data['szDestinationCity'] ;
						}
						
						$houlageData=array();
						$fFromWareHouseHaulageAry = array();
						$houlageData['idWarehouse'] = $warehoseArys['idWarehouseTo'] ;
						$houlageData['iDirection'] = $idDirection; // destination haulage		
						
						$houlageData['szLatitude'] = $data['fDestinationLatitude'] ;
						$houlageData['szLongitude'] = $data['fDestinationLongitude'] ;
						$houlageData['szPostcode'] = $szPostCode;
						$houlageData['idCountry'] = $data['idDestinationCountry'];		
						$houlageData['fTotalWeight'] = 1;
						$houlageData['fVolume'] = 0.1 ;
						
						$kHaulagePricing = new cHaulagePricing();
						$fFromWareHouseHaulageAry = $kHaulagePricing->getHaulageDetails($houlageData);
						
						if(!empty($fFromWareHouseHaulageAry))
						{
							if(!empty($fFromWareHouseHaulageAry))
							{
								if(!empty($_SESSION[$data['id']."_dest"]))
								{
									$_SESSION[$data['id']."_dest"][$ctr] .= "||||".$warehoseArys['idWarehouseFrom']."||".$warehoseArys['idWarehouseTo'];
								}
								else
								{
									$_SESSION[$data['id']."_dest"][$ctr] = $warehoseArys['idWarehouseFrom']."||".$warehoseArys['idWarehouseTo'];
								}
								$ctr++;
							}
							$ctr++;
						}
					}
				}
				
				if($idDirection==1)
				{
					if(empty($_SESSION[$data['id']."_orig"]))
					{
						return true;
					}
					else
					{
						return false;
					}
					
				}
				else
				{
					if(empty($_SESSION[$data['id']."_dest"]))
					{
						return true;
					}
					else
					{
						return false;
					}
				}
			}
			else
			{
				return true;
			}
		}
		else
		{
			return true;
		}
	}
	
	function isHaulageExistByWarehouseId($data,$idDirection)
	{
		if(!empty($data))
		{
			if(!empty($data['szPostcode']))
			{
				$szPostCode = $data['szPostcode'] ;
			}
			else
			{
				$szPostCode = $data['szCity'] ;
			}
						
			$houlageData=array();
			$fFromWareHouseHaulageAry = array();
			$houlageData['idWarehouse'] = $data['idWareHouse'] ;
			$houlageData['iDirection'] = $idDirection; // origin haulage

			$houlageData['szLatitude'] = $data['szLatitude'] ;
			$houlageData['szLongitude'] = $data['szLongitude'] ;
			$houlageData['szPostcode'] = $szPostCode;
			$houlageData['idCountry'] = $data['idCountry'];
			$houlageData['fTotalWeight'] = 1;
			$houlageData['fVolume'] = 0.1 ;
			//echo "<br /> Input ";
			//print_R($houlageData);
			$kHaulagePricing = new cHaulagePricing();
			$fFromWareHouseHaulageAry = $kHaulagePricing->getHaulageDetails($houlageData);
			//echo "<br /> output ";
			//print_R($fFromWareHouseHaulageAry);
			if(!empty($fFromWareHouseHaulageAry))
			{
				$haulage_pricing_exist_flag = true;
				return $haulage_pricing_exist_flag;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function refineWarehouses($data,$warehoseAry,$idDirection)
	{
		if($idDirection==1)
		{
			$_SESSION[$data['id']."_orig"]=array();
			unset($_SESSION[$data['id']."_orig"]);
		}
		else
		{
			$_SESSION[$data['id']."_dest"]=array();
			unset($_SESSION[$data['id']."_dest"]);
		}
		
		if(!empty($warehoseAry))
		{
			$ctr=0;
			foreach($warehoseAry as $warehoseArys)
			{
				if($idDirection==1)
				{
					if(!empty($data['szOriginPostCode']))
					{
						$szPostCode = $data['szOriginPostCode'] ;
					}
					else
					{
						$szPostCode = $data['szOriginCity'] ;
					}
					
					$houlageData=array();
					$fFromWareHouseHaulageAry = array();
					$houlageData['idWarehouse'] = $warehoseArys['idWarehouseFrom'] ;
					$houlageData['iDirection'] = $idDirection; // origin haulage
			
					$houlageData['szLatitude'] = $data['fOriginLatitude'] ;
					$houlageData['szLongitude'] = $data['fOriginLongitude'] ;
					$houlageData['szPostcode'] = $szPostCode;
					$houlageData['idCountry'] = $data['idOriginCountry'];
					$houlageData['fTotalWeight'] = 1;
					$houlageData['fVolume'] = 0.1 ;
					//echo "<br /> origin i/p<br /> ";
					//print_R($houlageData);
					
					$kHaulagePricing = new cHaulagePricing();
					$fFromWareHouseHaulageAry = $kHaulagePricing->getHaulageDetails($houlageData);
			
					//echo "<br /> origin output array ";
					//print_r($fFromWareHouseHaulageAry);
			
					if(!empty($fFromWareHouseHaulageAry))
					{
						$haulage_pricing_exist_flag = true;
						if(!empty($_SESSION[$data['id']."_orig"]))
						{
							$_SESSION[$data['id']."_orig"][$ctr] .= "||||".$warehoseArys['idWarehouseFrom']."||".$warehoseArys['idWarehouseTo'];
						}
						else
						{
							$_SESSION[$data['id']."_orig"][$ctr] = $warehoseArys['idWarehouseFrom']."||".$warehoseArys['idWarehouseTo'];
						}
						$ctr++;
					}
				}
				else
				{
					if(!empty($data['szDestinationPostCode']))
					{
						$szPostCode = $data['szDestinationPostCode'] ;
					}
					else
					{
						$szPostCode = $data['szDestinationCity'] ;
					}
					
					$houlageData=array();
					$fFromWareHouseHaulageAry = array();
					$houlageData['idWarehouse'] = $warehoseArys['idWarehouseTo'] ;
					$houlageData['iDirection'] = $idDirection; // destination haulage
			
					$houlageData['szLatitude'] = $data['fDestinationLatitude'] ;
					$houlageData['szLongitude'] = $data['fDestinationLongitude'] ;
					$houlageData['szPostcode'] = $szPostCode;
					$houlageData['idCountry'] = $data['idDestinationCountry'];
					$houlageData['fTotalWeight'] = 1;
					$houlageData['fVolume'] = 0.1 ;
					
					//echo "<br /> destination i/p array <br /> ";
					//print_r($houlageData);
					$kHaulagePricing = new cHaulagePricing();
					$fFromWareHouseHaulageAry = $kHaulagePricing->getHaulageDetails($houlageData);
					
					//echo "<br /> destination output array ";
					//print_r($fFromWareHouseHaulageAry);
					
					if(!empty($fFromWareHouseHaulageAry))
					{
						if(!empty($_SESSION[$data['id']."_dest"]))
						{
							$_SESSION[$data['id']."_dest"][$ctr] .= "||||".$warehoseArys['idWarehouseFrom']."||".$warehoseArys['idWarehouseTo'];
						}
						else
						{
							$_SESSION[$data['id']."_dest"][$ctr] = $warehoseArys['idWarehouseFrom']."||".$warehoseArys['idWarehouseTo'];
						}
						$ctr++;
					}
				}
			}
		}
	}
	
	function getAllAvailableWarehouseByCountry($idOriginCountry,$idDestinationCountry,$idServiceType)
	{
		if($idOriginCountry>0 && $idDestinationCountry>0)
		{
			if($idServiceType>0)
			{
				$field_name = get_service_field_name($idServiceType);
				if(!empty($field_name))
				{
					$query_and = " AND ".$field_name." = '1' ";
				}
			}
			$query="
				SELECT
					id,
					idWarehouseFrom,
					idWarehouseTo,
					iPTD,
					iWTD,
					iDTP,
					iDTW,
					iDTD
				FROM
					".__DBC_SCHEMATA_SERVICE_AVAILABLE__."
				WHERE
					idOriginCountry = '".(int)$idOriginCountry."'
				AND
					idDestinationCountry = '".(int)$idDestinationCountry."'
				AND
					dtValidTo > now()
				AND
					iStatus = '1'
				$query_and
			";
			//echo "<br />".$query."<br/>";
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$ret_ary=array();
					$ctr=0;
					while($row = $this->getAssoc($result))
					{
						$ret_ary[$ctr] = $row;
						$ctr++;
					}
					return $ret_ary ;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function getMaxExpiryDay($idOriginCountry,$idDestinationCountry,$idServiceType)
	{
		if($idOriginCountry>0 && $idDestinationCountry>0)
		{
			if((int)$idServiceType>0)
			{
				$field_name = get_service_field_name($idServiceType);
				$query_and = " AND ".$field_name." = '1' ";
			}
			
			$query="
				SELECT
					dtValidTo
				FROM
					".__DBC_SCHEMATA_SERVICE_AVAILABLE__."
				WHERE
					idOriginCountry = '".(int)$idOriginCountry."'
				AND
					idDestinationCountry = '".(int)$idDestinationCountry."'
				$query_and
				AND
					dtValidTo > now()
				AND
					iStatus = '1'
				ORDER BY
					dtValidTo DESC
				LIMIT
					0,1
			";
			//echo "<br />".$query."<br/>";
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row = $this->getAssoc($result);
					return $row['dtValidTo'];
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function getMinTransitDaysFromAvailableService($idOriginCountry,$idDestinationCountry,$idServiceType)
	{
		if($idOriginCountry>0 && $idDestinationCountry>0)
		{
			$query="
				SELECT
					 iTransitTime
				FROM
					".__DBC_SCHEMATA_SERVICE_AVAILABLE__."
				WHERE
					idOriginCountry = '".(int)$idOriginCountry."'
				AND
					idDestinationCountry = '".(int)$idDestinationCountry."'
				AND
					dtValidTo > now()
				AND
					iStatus = '1'
				ORDER BY
					 iTransitTime ASC
				LIMIT
					0,1
			";
			//echo "<br />".$query."<br/>";
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row = $this->getAssoc($result);
					return $row['iTransitTime'];
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	function isCustomClearanceExist($postSearchAry)
	{
		if(!empty($postSearchAry))
		{			
			$idServiceType = $postSearchAry['idServiceType'];
			if($idServiceType==__SERVICE_TYPE_DTD__)
			{
				if(!empty($_SESSION[$postSearchAry['id'].'_orig']) || !empty($_SESSION[$postSearchAry['id'].'_dest']))
				{
					if(!empty($_SESSION[$postSearchAry['id'].'_orig']))
					{
						$originWarehouseAry = getModifiedArray($_SESSION[$postSearchAry['id'].'_orig'],true);
						$this->refineWarehouses($postSearchAry,$originWarehouseAry,2);
					}
					
					if(!empty($_SESSION[$postSearchAry['id'].'_dest']))
					{
						$destinationWarehouseAry = getModifiedArray($_SESSION[$postSearchAry['id'].'_dest'],true);
						$this->refineWarehouses($postSearchAry,$destinationWarehouseAry,1);
					}
										
					$originWhsAry = getModifiedArray($_SESSION[$postSearchAry['id'].'_orig']);
					$destinationWhsAry = getModifiedArray($_SESSION[$postSearchAry['id'].'_dest']);
					$finalAry = array();
					$finalAry = $originWhsAry;
					if(!empty($destinationWhsAry) && !empty($finalAry))
					{
						$finalAry['idWarehouseFrom'] = array_merge($finalAry['idWarehouseFrom'],$destinationWhsAry['idWarehouseFrom']);
						if(!empty($finalAry['idWarehouseFrom']))
						{
							$finalAry['idWarehouseFrom'] = array_unique($finalAry['idWarehouseFrom']);
						}
						$finalAry['idWarehouseTo'] = array_merge($finalAry['idWarehouseTo'],$destinationWhsAry['idWarehouseTo']);
						if(!empty($finalAry['idWarehouseTo']))
						{
							$finalAry['idWarehouseTo'] = array_unique($finalAry['idWarehouseTo']);
						}
					}
				}
				/*
					if(!empty($_SESSION[$postSearchAry['id'].'_orig']))
					{
						$this->checkCustomeClearanceBySession('ORIGIN',$postSearchAry);
					}
					if(!empty($_SESSION[$postSearchAry['id'].'_dest']))
					{
						$this->checkCustomeClearanceBySession('DESTINATION',$postSearchAry);
					}
					return true;
				*/
			}
			else if($idServiceType==__SERVICE_TYPE_DTW__ || $idServiceType==__SERVICE_TYPE_DTP__)
			{
				if(!empty($_SESSION[$postSearchAry['id'].'_orig']))
				{
					$destinationWhsAry = array();
					$destinationWhsAry = getModifiedArray($_SESSION[$postSearchAry['id'].'_orig']);
					$finalAry = array();
					$finalAry = $destinationWhsAry;
					if(!empty($finalAry['idWarehouseFrom']))
					{
						$finalAry['idWarehouseFrom'] = array_unique($finalAry['idWarehouseFrom']);
					}
					if(!empty($finalAry['idWarehouseTo']))
					{
						$finalAry['idWarehouseTo'] = array_unique($finalAry['idWarehouseTo']);
					}
				}
				$_SESSION[$postSearchAry['id'].'_dest'] = array();
				unset($_SESSION[$postSearchAry['id'].'_dest']);
			}
			else if($idServiceType==__SERVICE_TYPE_WTD__ || $idServiceType==__SERVICE_TYPE_PTD__)
			{
				if(!empty($_SESSION[$postSearchAry['id'].'_dest']))
				{
					$originWhsAry = getModifiedArray($_SESSION[$postSearchAry['id'].'_dest']);
					$finalAry = array();
					$finalAry = $originWhsAry;
					if(!empty($finalAry['idWarehouseFrom']))
					{
						$finalAry['idWarehouseFrom'] = array_unique($finalAry['idWarehouseFrom']);
					}
					if(!empty($finalAry['idWarehouseTo']))
					{
						$finalAry['idWarehouseTo'] = array_unique($finalAry['idWarehouseTo']);
					}
				}
				$_SESSION[$postSearchAry['id'].'_orig'] = array();
				unset($_SESSION[$postSearchAry['id'].'_orig']);
			}
			else
			{
				$_SESSION[$postSearchAry['id'].'_orig'] = array();
				unset($_SESSION[$postSearchAry['id'].'_orig']);
				
				$_SESSION[$postSearchAry['id'].'_dest'] = array();
				unset($_SESSION[$postSearchAry['id'].'_dest']);
				
				$finalAry['idWarehouseFrom'] = array();
				$finalAry['idWarehouseTo'] = array();
			}
			
			$idOriginCountry = $postSearchAry['idOriginCountry'];
			$idDestinationCountry = $postSearchAry['idDestinationCountry'];
			$feild_name = get_service_field_name($idServiceType);
			
			if(!empty($feild_name))
			{
				$query_and = " AND ".mysql_escape_custom(trim($feild_name))." ='1' ";
			}
			
			if(!empty($finalAry['idWarehouseFrom']))
			{
				$from_whs_str = implode(',',$finalAry['idWarehouseFrom']);
				$query_and .=" AND idWarehouseFrom IN (".$from_whs_str.")" ;
			}			
			if(!empty($finalAry['idWarehouseTo']))
			{
				$to_whs_str = implode(',',$finalAry['idWarehouseTo']);
				$query_and .=" AND idWarehouseTo IN (".$to_whs_str.")" ;
			}
			
			if(!empty($postSearchAry['dtTimingDate']) && $postSearchAry['dtTimingDate']!='0000-00-00 00:00:00')
			{
				$query_and .= " AND dtValidTo >= '".mysql_escape_custom(trim($postSearchAry['dtTimingDate']))."'";
				$query_and .= " AND dtValidFrom <= '".mysql_escape_custom(trim($postSearchAry['dtTimingDate']))."'";
			}
				
			$query="
				SELECT
					 iOriginCC,
					 iDestinationCC
				FROM
					".__DBC_SCHEMATA_SERVICE_AVAILABLE__."
				WHERE
					idOriginCountry = '".(int)$idOriginCountry."'
				AND
					idDestinationCountry = '".(int)$idDestinationCountry."'
					$query_and
				AND
					dtValidTo >= now()
				AND
					iStatus = '1'
				AND
					( 
						iDestinationCC = 1 
					OR
						iOriginCC = 1
					)
			";
			//echo "<br />".$query."<br/>";
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$iOriginCC = 0;
					$iDestinationCC = 0;
					while($row = $this->getAssoc($result))
					{
						if($row['iOriginCC']==1)
						{
							$iOriginCC = 1;
						}
						
						if($row['iDestinationCC']==1)
						{
							$iDestinationCC = 1;
						}
						
						//If we found both custom clearance then no need for check furthur and exit from the loop.
						if($iOriginCC==1 && $iDestinationCC==1)
						{
							break;
						}
					}
					
					$this->iOriginCC = $iOriginCC;
					$this->iDestinationCC = $iDestinationCC;
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}

	function addWarehouseApprovedData($data)
	{
	
		if((int)$_SESSION['forwarder_admin_id']>0)
		{
			$idAdmin = (int)$_SESSION['forwarder_admin_id'];
			$idForwarderContact = 0 ;
		}
		else
		{
			$idAdmin = 0;
			$idForwarderContact = (int)$_SESSION['forwarder_user_id'];
		}
				
		$this->idForwarder=sanitize_all_html_input((int)$_SESSION['forwarder_id']);					
				
                $iWarehouseType = $data['iWarehouseType'];
                
		$this->set_szWarehouseName(sanitize_all_html_input(trim($data['szWareHouseName'])));
		$this->set_szAddress1(sanitize_all_html_input(trim($data['szAddress1'])));
		$this->set_szAddress2(sanitize_all_html_input(trim($data['szAddress2'])));
		$this->set_szAddress3(sanitize_all_html_input(trim($data['szAddress3'])));
		$this->set_szPostCode(sanitize_all_html_input(trim($data['szPostCode'])));
		$this->set_szCity(sanitize_all_html_input(trim($data['szCity'])));
		$this->set_szState(sanitize_all_html_input(trim($data['szState'])));
		$this->set_szCountry(sanitize_all_html_input(trim($data['szCountry'])));
		$this->set_szPhoneNumber(sanitize_all_html_input(urlencode(trim($data['szPhone']))));
		$this->set_szLatitude(sanitize_all_html_input(trim($data['szLatitude'])));
		
		if($this->szLatitude!=NULL)
		{
                    if($this->szLatitude< -90 || $this->szLatitude>90 || !is_numeric($this->szLatitude))
                    {
                        $this->addError( "szLatitude" , t($this->t_base_cfs.'error/latitude_must_be'));
                    } 
		}
		$this->set_szLongitude(sanitize_all_html_input(trim($data['szLongitude'])));
		if($this->szLongitude!=NULL)
		{
                    if($this->szLongitude< -180 || $this->szLongitude>180 || !is_numeric($this->szLongitude))
                    {
                        $this->addError( "szLongitude" , t($this->t_base_cfs.'error/longitude_must_be'));
                    }
		
		}
		if ($this->error === true)
		{
                    return false;
		}
		if($this->szLatitude!=NULL && 	$this->szLongitude!=NULL && $this->szCountry!=NULL)
		{
                    $countryApiDetailAry = array();
                    $countryApiDetailAry = getAddressDetails($this->szLatitude,$this->szLongitude,$this->szCountry);
                    $countryISO = $countryApiDetailAry['szCountryCode'] ;

                    $countryDetails = $this->setCountryNameISO();	
                    if(strtoupper(trim($countryDetails['szCountryISO'])) != strtoupper(trim($countryISO)))
                    {
                        $this->addError( "idCountry" , t($this->t_base_cfs.'error/this_location_is_not_in')." ".$countryDetails['szCountryName'] .". ". t($this->t_base_cfs.'error/please_update_your_lat_lang') );
                    }
		} 
                $query="
                    SELECT 
                        id
                    FROM
                        ".__DBC_SCHEMATA_WAREHOUSES__."	
                    WHERE
                        idForwarder = 	'".(int)$this->idForwarder."'	
                    AND
                        idCountry = '".(int)$this->szCountry."'	
                    AND	
                        LOWER(szCity) = '".mysql_escape_custom(strtolower($this->szCity))."'
                    AND
                        szWareHouseName='".mysql_escape_custom($this->szWarehouseName)."'
                    AND
                        iActive='1'	
                    AND
                        iWarehouseType = '".(int)$iWarehouseType."'
                ";
                //echo $query;
                if($result=$this->exeSQL($query))
                {
                    //echo $this->iNumRows."<br/>";	
                    if($this->iNumRows>0)
                    {
                        if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
                        {
                            $this->addError( "szWareHouseName" ,t($this->t_base_cfs.'error/air_name_already_exist_in_this_city'));
                        }
                        else
                        {
                            $this->addError( "szWareHouseName" ,t($this->t_base_cfs.'error/cfs_name_already_exist_in_this_city'));
                        } 
                        return false;
                    }
                }
		if ($this->error === true)
		{
                    return false;
		}
		
		if(isset($_SESSION['idBatch']) && $_SESSION['show_once']!=1)
		{
                    $this->countWareHouse($iWarehouseType);
                    $_SESSION['show_once'] =1;
		}
		
		if ($this->error === true)
		{
			return false;
		}
		$forwarderContactName	= $this->findForwarderFirstLastName($idForwarderContact); 
		
		$timezone_url=__ASK_GEO_API_URL__."/".__ASK_GEO_ACCOUNT_ID__."/".__ASK_GEO_API_KEY__."/query.".__ASK_GEO_FORMAT__."?databases=".__ASK_GEO_DATABATE__."&points=";
		
		$posturl=$timezone_url."".$this->szLatitude.",".$this->szLongitude;
		$response = $this->curl_get_file_contents($posturl);
		$decoded_response = json_decode($response);
		$utcoffsetupdated='';
		$utcoffsetupdated=$decoded_response->data[0]->TimeZone->CurrentOffsetMs;
		
		if(!empty($this->szPhoneNumber))
		{
                    $str = substr($this->szPhoneNumber, 0, 1); 
                    //echo $str;
                    $substr=substr($this->szPhoneNumber,1);
                    $phone=str_replace("+"," ",$substr);
                    //echo $phone;
                    if(!empty($phone))
                    {
                        $phoneNumber=$str."".urldecode($phone);
                        $this->szPhoneNumber = $phoneNumber;
                    }
                    else
                    {
                        $this->szPhoneNumber = urldecode($this->szPhoneNumber);
                    }
		}
		
		$query="
			INSERT INTO
                            ".__DBC_SCHEMATA_WAREHOUSES__."
			(
                            szWareHouseName,
                            szAddress,
                            szAddress2,
                            szAddress3,
                            szPostCode,
                            szCity,
                            szState,
                            idCountry,
                            szPhone,
                            szLatitude,
                            szLongitude,
                            idForwarder,
                            dtUpdatedOn,
                            dtCreatedOn,
                            iActive,
                            idForwarderContact,
                            idAdmin,
                            szUpdatedBy,
                            szUTCOffset,
                            iWarehouseType
			)	
                        VALUES
			(	
                            '".mysql_escape_custom($this->szWarehouseName)."',
                            '".mysql_escape_custom($this->szAddress1)."',
                            '".mysql_escape_custom($this->szAddress2)."',
                            '".mysql_escape_custom($this->szAddress3)."',
                            '".mysql_escape_custom($this->szPostcode)."',
                            '".mysql_escape_custom(ucwords(strtolower($this->szCity)))."',
                            '".mysql_escape_custom($this->szState)."',
                            '".(int)$this->szCountry."',
                            '".mysql_escape_custom($this->szPhoneNumber)."',
                            '".mysql_escape_custom($this->szLatitude)."',
                            '".mysql_escape_custom($this->szLongitude)."',
                            '".(int)$this->idForwarder."',
                            NOW(),
                            NOW(),
                            '1',
                            '".(int)$idForwarderContact."',
                            '".(int)$idAdmin."',
                            '".mysql_escape_custom($forwarderContactName)."',
                            '".mysql_escape_custom($utcoffsetupdated)."',
                            '".mysql_escape_custom($iWarehouseType)."'
			 )	
		";
		//ECHO $query;
		if($result=$this->exeSQL($query))
		{
                    $warehouseID = (int)$this->iLastInsertID;
                    $this->cfsModelMapping($warehouseID);

                    $warehouseContry  = sanitize_all_html_input($data['selectNearCFS']);
                    if($warehouseContry!='')
                    {
                        $this->addWarehouseCountries($warehouseContry,$warehouseID);
                    } 
                    $this->showMessageUnset();
                    return true;
		}
		else
		{
                    return false;
		}	
	}
	
	function checkCustomeClearanceBySession($mode,$data)
	{
		if($mode=='ORIGIN')
		{
			if(!empty($_SESSION[$data['id'].'_orig']))
			{
				$final_ary = array();
				$ctr=0;
				$warehAry = $_SESSION[$data['id'].'_orig'] ;
				
				foreach($warehAry as $warehArys)
				{
					$warehouse_pair_ary = explode("||||",$warehArys);
					if(!empty($warehouse_pair_ary))
					{
						foreach($warehouse_pair_ary as $warehouse_pair_arys)
						{
							if(!empty($warehouse_pair_arys))
							{
								$warehouseAry = explode("||",$warehouse_pair_arys);
								$final_ary[$ctr]['idWarehouseFrom'] =  $warehouseAry[0];
								$final_ary[$ctr]['idWarehouseTo'] =  $warehouseAry[1];
								$ctr++;
							}
						}
					}
				}
				if(!empty($final_ary))
				{
					foreach($final_ary as $final_arys)
					{
						$query="
							SELECT
								id
							FROM
								".__DBC_SCHEMATA_PRICING_CC__."
							WHERE
								idWarehouseFrom = '".(int)$final_arys['idWarehouseFrom']."'
							AND
								idWarehouseTo = '".(int)$final_arys['idWarehouseTo']."'
							AND
								 szOriginDestination = '1'
							AND
								iActive = '1'
						";
						//echo "<br /> ".$query ;
						if($result=$this->exeSQL($query))
						{
							if($this->iNumRows>0)
							{
								$this->iOriginCC = 1;
								return true;
							}
						}
						else
						{
							$this->error = true;
							$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
							$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
							return false;
						}
					}
				}
			}
		}
		else if($mode=='DESTINATION')
		{		
			if(!empty($_SESSION[$data['id'].'_dest']))
			{
				$final_ary = array();
				$ctr=0;
				$warehAry = $_SESSION[$data['id'].'_dest'] ;
				
				foreach($warehAry as $warehArys)
				{
					$warehouse_pair_ary = explode("||||",$warehArys);
					if(!empty($warehouse_pair_ary))
					{
						foreach($warehouse_pair_ary as $warehouse_pair_arys)
						{
							if(!empty($warehouse_pair_arys))
							{
								$warehouseAry = explode("||",$warehouse_pair_arys);
								$final_ary[$ctr]['idWarehouseFrom'] =  $warehouseAry[0];
								$final_ary[$ctr]['idWarehouseTo'] =  $warehouseAry[1];
								$ctr++;
							}
						}
					}
				}
				
				if(!empty($final_ary))
				{
					foreach($final_ary as $final_arys)
					{
						$query="
							SELECT
								id
							FROM
								".__DBC_SCHEMATA_PRICING_CC__."
							WHERE
								idWarehouseFrom = '".(int)$final_arys['idWarehouseFrom']."'
							AND
								idWarehouseTo = '".(int)$final_arys['idWarehouseTo']."'
							AND
								 szOriginDestination = '2'
							AND
								iActive = '1'
						";
						if($result=$this->exeSQL($query))
						{
							if($this->iNumRows>0)
							{
								$this->iDestinationCC = 2;
								return true;
							}
						}
						else
						{
							$this->error = true;
							$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
							$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
							return false;
						}
					}
				}
			}
		}
	}
	
	function transferUploadCFSLocationData($id)
	{
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_AWATING_APPROVAL__."
                SET
                    iTransfer='1'
                WHERE
                    id='".(int)$id."'
            ";
            //echo $query;
            $result=$this->exeSQL($query); 
            return true;
	}
	function selectWarehouseName($id)
	{
		$query="
			SELECT
				szWareHouseName
			FROM
				".__DBC_SCHEMATA_WAREHOUSES__."
			WHERE
				id = '".(int)$id."'	
		";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$row=$this->getAssoc($result);	
				return $row['szWareHouseName'];
			}
			else
			{
				return array();
			}
		}
	}
	
	function setUploadServiceCFSError($data,$iWarehouseType=false)
	{
		if((int)$_SESSION['forwarder_admin_id']>0)
		{
                    $idAdmin = (int)$_SESSION['forwarder_admin_id'];
                    $idForwarderContact = 0 ;
		}
		else
		{
                    $idAdmin = 0;
                    $idForwarderContact = (int)$_SESSION['forwarder_user_id'];
		}
				
		$this->idForwarder=sanitize_all_html_input((int)$_SESSION['forwarder_id']);					
							
		$this->set_szWarehouseName(sanitize_all_html_input(trim($data['szWareHouseName'])));
		$this->set_szAddress1(sanitize_all_html_input(trim($data['szAddress1'])));
		$this->set_szAddress2(sanitize_all_html_input(trim($data['szAddress2'])));
		$this->set_szAddress3(sanitize_all_html_input(trim($data['szAddress3'])));
		$this->set_szPostCode(sanitize_all_html_input(trim($data['szPostCode'])));
		$this->set_szCity(sanitize_all_html_input(trim($data['szCity'])));
		$this->set_szState(sanitize_all_html_input(trim($data['szState'])));
		$this->set_szCountry(sanitize_all_html_input(trim($data['idCountry'])));
		$this->set_szPhoneNumber(sanitize_all_html_input(trim($data['szPhone'])));
		$this->set_szLatitude(sanitize_all_html_input(trim($data['szLatitude'])));
		
		if($this->szLatitude!=NULL)
		{
			if($this->szLatitude< -90 || $this->szLatitude>90)
			{
				$this->addError( "szLatitude" , t($this->t_base_cfs.'error/latitude_must_be'));
			}
		
		}
		$this->set_szLongitude(sanitize_all_html_input(trim($data['szLongitude'])));
		if($this->szLongitude!=NULL)
		{
			if($this->szLongitude< -180 || $this->szLongitude>180)
			{
				$this->addError( "szLongitude" , t($this->t_base_cfs.'error/longitude_must_be'));
			}
		
		}
		if ($this->error === true)
		{
			return false;
		}
		if($this->szLatitude!=NULL && 	$this->szLongitude!=NULL && $this->szCountry!=NULL)
		{
			$countryApiDetailAry = array();
			$countryApiDetailAry = getAddressDetails($this->szLatitude,$this->szLongitude,$this->szCountry);
			$countryISO = $countryApiDetailAry['szCountryCode'] ;
			
			$countryDetails = $this->setCountryNameISO();	
			if(strtoupper(trim($countryDetails['szCountryISO'])) != strtoupper(trim($countryISO)))
			{
				$this->addError( "idCountry" , t($this->t_base_cfs.'error/this_location_is_not_in')." ".$countryDetails['szCountryName'] .". ". t($this->t_base_cfs.'error/please_update_your_lat_lang') );
			}
		}
		if($iWarehouseType>0)
                {
                    $query_and = " AND iWarehouseType = '".$iWarehouseType."' ";
                }
	
			$query="
				SELECT 
					id
				FROM
					".__DBC_SCHEMATA_WAREHOUSES__."	
				WHERE
					idForwarder = 	'".(int)$this->idForwarder."'	
				AND
					idCountry = '".(int)$this->szCountry."'	
				AND	
					LOWER(szCity) = '".mysql_escape_custom(strtolower($this->szCity))."'
				AND
					szWareHouseName='".mysql_escape_custom($this->szWarehouseName)."'
				AND
					iActive='1'
                                    $query_and
			";
			//echo $query;
			if($result=$this->exeSQL($query))
			{
                            //echo $this->iNumRows."<br/>";	
                            if($this->iNumRows>0)
                            {
                                if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
                                {
                                    $this->addError( "szWareHouseName" ,t($this->t_base_cfs.'error/air_name_already_exist_in_this_city'));
                                }
                                else
                                {
                                    $this->addError( "szWareHouseName" ,t($this->t_base_cfs.'error/cfs_name_already_exist_in_this_city'));
                                } 
                                return false;
                            }
			}
		if ($this->error === true)
		{
                    return false;
		}
		
		if(isset($_SESSION['idBatch']) && $_SESSION['show_once']!=1)
		{
                        $this->countWareHouse($iWarehouseType);
                        $_SESSION['show_once'] =1;
		}
		
		if ($this->error === true)
		{
			return false;
		}
		
		
		return true;
	}
	
	function getAllNotificationTobeSent()
	{
		$query="
			SELECT
				id,
				idCustomer,
				szCustomerName,
				szCustomerEmail,
				idNotificationType,
				idOriginCountry,
				szOriginCountry,
				idDestinationCountry,
				szDestinationCountry,
				dtTiming,
				dtCreatedOn
			FROM
				".__DBC_SCHEMATA_NOTIFICATION__."
			WHERE
				iNotificationSent = '0'
		";
		
		//echo "<br />".$query."<br/>";
		if($result = $this->exeSQL($query))
		{
			if($this->iNumRows>0)
			{
				$ret_ary=array();
				$ctr=0;
				while($row = $this->getAssoc($result))
				{
					$ret_ary[$ctr] = $row;
					$ctr++;
				}
				return $ret_ary ;
			}
			else
			{
				return false;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
	
	function sendNotificationEmail($data,$idNotificationType)
	{
		if(!empty($data))
		{
			$replace_ary = array();
			if($idNotificationType == __NOTIFICATION_TYPE_NO_SERVICE_COUNTRY_TO_COUNTRY___) //notification type 1
			{
				$szSectionTitle = __SERVICE_NOTIFICATION_EMAIL__ ;
				$replace_ary['szPickupDeliverText'] = "";
				$replace_ary['szNotificationTypeSubject'] = "Transportation from ".$data['szOriginCountry']." to ".$data['szDestinationCountry']." is now available on Transporteca ";
				$replace_ary['szNewText'] = '';
			}
			else if($idNotificationType == __NOTIFICATION_TYPE_NO_DELIVERY_TO_DESTINATION_COUNTRY___) //notification type 2
			{
				$replace_ary['szPickupDeliverText'] = "with door delivery in ".$data['szDestinationCountry']." ";
				$replace_ary['szNotificationTypeSubject'] = " Delivery in ".$data['szDestinationCountry']." is now available on Transporteca";
				$szSectionTitle = __SERVICE_NOTIFICATION_EMAIL__ ;
				$replace_ary['szNewText'] = '';
			}
			else if($idNotificationType == __NOTIFICATION_TYPE_NO_PICKUP_FROM_ORIGIN_COUNTRY___) //notification type 3
			{
				$replace_ary['szPickupDeliverText'] = "with door pick-up in ".$data['szOriginCountry']." ";
				$replace_ary['szNotificationTypeSubject'] = "Pick-up in ".$data['szOriginCountry']." is now available on Transporteca";
				$szSectionTitle = __SERVICE_NOTIFICATION_EMAIL__ ;
				$replace_ary['szNewText'] = '';
			}
			else if($idNotificationType == __NOTIFICATION_TYPE_NO_PICKUP_AND_NO_DELIVERY___) //notification type 4
			{
				$replace_ary['szPickupDeliverText'] = "including pick-up and delivery ";
				$replace_ary['szNotificationTypeSubject'] = "Door-to-Door service from ".$data['szOriginCountry']." to ".$data['szDestinationCountry']." is now available on Transporteca";
				$szSectionTitle = __SERVICE_NOTIFICATION_EMAIL__ ;
				$replace_ary['szNewText'] = '';
			}
			else if($idNotificationType == __NOTIFICATION_TYPE_RP1_RP2___) //notification type 5
			{
				$replace_ary['szPickupDeliverText'] = "";
				$replace_ary['szNotificationTypeSubject'] = "New services from ".$data['szOriginCountry']." to ".$data['szDestinationCountry']." are now available on Transporteca";
				$szSectionTitle = __SERVICE_NOTIFICATION_EMAIL__ ;
				$replace_ary['szNewText'] = ' new ';
			}			
			else if($idNotificationType == __NOTIFICATION_TYPE_S1_S2___) //notification type 6
			{
				$replace_ary['szPickupDeliverText'] = "with departure on ".date('d. F Y',strtotime($data['dtTiming']))." ";
				$replace_ary['szNotificationTypeSubject'] = "Transportation from ".$data['szOriginCountry']." to ".$data['szDestinationCountry']." is now available on Transporteca ";
				$szSectionTitle = __SERVICE_NOTIFICATION_EMAIL__ ;
				$replace_ary['szNewText'] = '';
			}
			else if($idNotificationType == __NOTIFICATION_TYPE_T1_T2___) //notification type 7
			{
				$replace_ary['szPickupDeliverText'] = "with arrival on ".date('d. F Y',strtotime($data['dtTiming']))." ";
				$replace_ary['szNotificationTypeSubject'] = "Transportation from ".$data['szOriginCountry']." to ".$data['szDestinationCountry']." is now available on Transporteca ";
				$szSectionTitle = __SERVICE_NOTIFICATION_EMAIL__ ;
				$replace_ary['szNewText'] = '';
			}
			else if($idNotificationType == __NOTIFICATION_TYPE_SORRY_EMAIL_S1_S2___)
			{
				if(date('Y-m-d',strtotime($data['dtTiming'])) == date('Y-m-d'))
				{
					$szTimingText = 'today';
				}
				else
				{
					$szTimingText = "on ".date('d. F Y',strtotime($data['dtTiming']));
				}
				
				$replace_ary['szPickupDeliverText'] = "with departure today. ";
				$replace_ary['szNotificationTypeSubject'] = "We are sorry we couldn't help";
				$szSectionTitle = __DATE_EXPIRED_SERVICE_NOTIFICATION_EMAIL__ ;
				$replace_ary['szNewText'] = '';
			}
			else if($idNotificationType == __NOTIFICATION_TYPE_SORRY_EMAIL_T1_T2___)
			{
				if(date('Y-m-d',strtotime($data['dtTiming'])) == date('Y-m-d'))
				{
					$szTimingText = 'today';
				}
				else
				{
					$szTimingText = "on ".date('d. F Y',strtotime($data['dtTiming']));
				}
				
				$replace_ary['szPickupDeliverText'] ="with arrival today. ";
				$replace_ary['szNotificationTypeSubject'] = "We are sorry we couldn't help";
				$szSectionTitle = __DATE_EXPIRED_SERVICE_NOTIFICATION_EMAIL__ ;
				$replace_ary['szNewText'] = '';
			}
			
			if(!empty($szSectionTitle))
			{				
				$replace_ary['szCustomerName']=$data['szCustomerName'];
				$replace_ary['szCustomerEmail']=$data['szCustomerEmail'];
				
				$replace_ary['szOriginCountry']=$data['szOriginCountry'];
				$replace_ary['szDestinationCountry']=$data['szDestinationCountry'];
				
				$szAvailableServiceUrl =__TRANSPORTECA_LANDING_PAGE_URL__.'/'.$data['idOriginCountry']."__".$data['idDestinationCountry'].'/';
				
				$replace_ary['szAvailableServiceLink']="<a href='".$szAvailableServiceUrl."'>SHOW OPTIONS FROM ".strtoupper($data['szOriginCountry'])." TO ".strtoupper($data['szDestinationCountry'])."</a>";
				$replace_ary['szAvailableServiceUrl'] = $szAvailableServiceUrl;
	            $replace_ary['supportEmail'] = __STORE_SUPPORT_EMAIL__;
	            $replace_ary['szWebsiteUrl'] =remove_http_from_url( __MAIN_SITE_HOME_PAGE_URL__);
	            $replace_ary['szDateTime'] = date('d F Y',strtotime($data['dtCreatedOn']));
	            
	            echo "<br /> Sending E-mail to ".$data['szCustomerEmail']." for the service request from ".$data['szOriginCountry']." to ".$data['szDestinationCountry']." with notification type ".$idNotificationType ;
	          	createEmail($szSectionTitle, $replace_ary,$data['szCustomerEmail'], '', __STORE_SUPPORT_EMAIL__,$data['idCustomer'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_CUSTOMER__);
			}       
		}
	}
	
	function updateNotificationStatus($idNotificationAry)
	{
		if(!empty($idNotificationAry))
		{
			$id_str = implode(",",$idNotificationAry);
			$query="
				UPDATE
					".__DBC_SCHEMATA_NOTIFICATION__."	
				SET
				   iNotificationSent = '1'
				WHERE
					id IN (".$id_str.")
			";
			if($result = $this->exeSQL($query))
			{
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function checkNewServiceAdded($data)
	{
		if($data)
		{
			$query="
				SELECT
					s.id,
					s.szForwarderName,
					s.idOriginCountry,
					s.szOriginCountry,
					s.idDestinationCountry,
					s.szDestinationCountry,
					s.szFrequency,
					s.dtValidFrom,
					s.dtValidTo,
					s.szOriginCity,
					s.szDestinationCity,
					iPTP,
					iPTW,
					iPTD,
					iDTP,
					iWTW,
					iWTD,
					iDTW,
					iDTD,
					iWTP,
					iStatus
				FROM
					".__DBC_SCHEMATA_SERVICE_AVAILABLE__." s
				WHERE
					s.idOriginCountry = '".(int)$data['idOriginCountry']."'
				AND
					s.idDestinationCountry = '".(int)$data['idDestinationCountry']."'
				AND
					s.dtValidTo > now()
				ORDER BY
					RAND()
			";
			//echo "<br />".$query."<br/>";
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$ret_ary=array();
					$ctr=0;
					$iActiveServiceCounter = 0;
					$iInactiveServiceCounter = 0;
					while($row = $this->getAssoc($result))
					{
						if($row['iStatus']==1)
						{
							if($row['iPTP']==1)
							{
								$iActiveServiceCounter++;
							}
							if($row['iPTW']==1)
							{
								$iActiveServiceCounter++;
							}
							if($row['iPTD']==1)
							{
								$iActiveServiceCounter++;
							}
							if($row['iWTP']==1)
							{
								$iActiveServiceCounter++;
							}
							if($row['iDTP']==1)
							{
								$iActiveServiceCounter++;
							}
							if($row['iWTW']==1)
							{
								$iActiveServiceCounter++;
							}
							if($row['iWTD']==1)
							{
								$iActiveServiceCounter++;
							}
							if($row['iDTW']==1)
							{
								$iActiveServiceCounter++;
							}
							if($row['iDTD']==1)
							{
								$iActiveServiceCounter++;
							}
						}
						else if($row['iStatus']==2)
						{
							if($row['iPTP']==1)
							{
								$iInactiveServiceCounter++;
							}
							if($row['iPTW']==1)
							{
								$iInactiveServiceCounter++;
							}
							if($row['iPTD']==1)
							{
								$iInactiveServiceCounter++;
							}
							if($row['iWTP']==1)
							{
								$iInactiveServiceCounter++;
							}
							if($row['iDTP']==1)
							{
								$iInactiveServiceCounter++;
							}
							if($row['iWTW']==1)
							{
								$iInactiveServiceCounter++;
							}
							if($row['iWTD']==1)
							{
								$iInactiveServiceCounter++;
							}
							if($row['iDTW']==1)
							{
								$iInactiveServiceCounter++;
							}
							if($row['iDTD']==1)
							{
								$iInactiveServiceCounter++;
							}
						}
					}
					
					//echo "<br /> num new service ".$iActiveServiceCounter;
					//echo "<br /> num of old services ".$iInactiveServiceCounter ;
					
					if($iActiveServiceCounter>$iInactiveServiceCounter)
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	function isValidServiceExists($idOriginCountry,$idDestinationCountry,$field_name,$dtTiming,$idTiming=false)
	{
		if((int)$idOriginCountry>0 && (int)$idDestinationCountry>0)
		{
			if(!empty($field_name))
			{
				$query_select = " , ".mysql_escape_custom(trim($field_name));
				$query_and = " AND ".mysql_escape_custom(trim($field_name))." = '1'";
			}
			
			if($idTiming==1)
			{
				$query_and .= "AND dtValidFrom < '".mysql_escape_custom(trim($dtTiming))."' AND '".mysql_escape_custom(trim($dtTiming))."' < dtValidTo";
			}
			else if($idTiming==2) {
				$query_and .= "AND DATE_ADD(dtValidFrom,INTERVAL iTransitTime/24 DAY) < '".mysql_escape_custom(trim($dtTiming))."' AND '".mysql_escape_custom(trim($dtTiming))."' < DATE_ADD(dtValidTo,INTERVAL iTransitTime/24 DAY)";
			}
	
			$query="
				SELECT
					id
					".$query_select."
				FROM
					".__DBC_SCHEMATA_SERVICE_AVAILABLE__." s
				WHERE
					s.idOriginCountry = '".(int)$idOriginCountry."'
				AND
					s.idDestinationCountry = '".(int)$idDestinationCountry."'
				AND
					iStatus = '1'
				$query_and
			";
			//echo "<br />".$query."<br/>";
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
	
	function getAllAvailableServiceTempTable($szBookingRandomNum,$from='',$to='')
	{
		$query_limit="";
		if($from!='' && $to!='')
		{
			$query_limit="LIMIT $from,$to";
		}
		$query="
			SELECT
				szForwarderName,
				szForwarderLogo,
				szOriginCity,
				szDestinationCity,
				iServiceType,
				szFrequency,
				dtValidTo
			FROM
				".__DBC_SCHEMATA_SERVICE_TEMP_DATA__."
			WHERE
				szSearchNumber='".mysql_escape_custom($szBookingRandomNum)."'
				$query_limit
			";
		//echo $query."<br/>";
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					while($row = $this->getAssoc($result))
					{
						$res_arr[]=$row;
					}
					return $res_arr;
				}
				else
				{
					return array();
				}
			}
			else
			{
				return array();
			}
	}
	
	function deleteServiceTempData($szBookingRandomNum)
	{
		$query="
			DELETE
				FROM
				".__DBC_SCHEMATA_SERVICE_TEMP_DATA__."
			WHERE
				szSearchNumber='".mysql_escape_custom($szBookingRandomNum)."'
		";
		$result = $this->exeSQL($query);
	}
	
	
	function getForwarderAvgReviews($idForwarder)
	{
	
		$col="";
		$whereQuery="";
		if(!empty($month) && $month!="")
		{
			$today=date('Y-m-d');
			$col .=",sum(iRating) as trating,iRating";
			
			$prevdate = date("Y-m-d",strtotime(date("Y-m-d", strtotime($today)) . '-'.$month.' month'));
			
			
			$whereQuery .="
				AND
					date(dtCompleted)>='".mysql_escape_custom($prevdate)."'
			";
		}
		else
		{
			//__FORWARDER_RATING__
			$month = $this->getManageMentVariableByDescription('__FORWARDER_RATING_REVIEW__');
			$today=date('Y-m-d');
			$prevdate = date("Y-m-d",strtotime(date("Y-m-d", strtotime($today)) . '-'.$month.' month'));
							
			$whereQuery .="
				AND
					date(dtCompleted)>='".mysql_escape_custom($prevdate)."'
			";
		}
		$query="
			SELECT
				count(id) AS iNumReview
			FROM
				".__DBC_SCHEMATA_RATING_REVIEW__."	
			WHERE
				idForwarder = '".(int)$idForwarder."'
			AND
				szReview<>''
			$whereQuery	
			HAVING
				iNumReview>0	
		";
		//echo $query ;
		if($idForwarder==5)
		{
			//echo "<br> ".$query ;
		}
		if($result=$this->exeSQL($query))
		{
			$row=$this->getAssoc($result) ;
			return $row;
		}
	}
	
        function getAllTestBooking()
        {
            $query=" 
                SELECT
                    b.id,
                    b.szEmail
                FROM 
                    tblbookings b 
                WHERE 
                    b.idUser = 0
                AND
                    b.szEmail != '' ;
            "; 
            //echo $query ;
            if($result=$this->exeSQL($query))
            {
                $ret_ary = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    $ret_ary[$ctr] = $row ;
                    $ctr++;
                }
                return $ret_ary;
            }
        } 
        function getAllWeekDays()
        {
            $query=" SELECT id,szWeekDay FROM ".__DBC_SCHEMATA_WEEK_DAYS__;  
            if($result=$this->exeSQL($query))
            {
                $ret_ary = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    $ret_ary[$row['id']] = $row['szWeekDay'] ;
                    $ctr++;
                }
                return $ret_ary;
            }
        }
        function getAllFrequencies()
        {
            $query=" SELECT id,iDays FROM ".__DBC_SCHEMATA_FREQUENCY__;  
            if($result=$this->exeSQL($query))
            {
                $ret_ary = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    $ret_ary[$row['id']] = $row['iDays'] ;
                    $ctr++;
                }
                return $ret_ary;
            }
        }  
        function updateAnonymousBookingFlag($idBooking,$bAnonymousFlag=false)
        {
            if($idBooking>0)
            {   
                $kUser = new cUser();
                $kBooking = new cBooking();
                $kUser->getUserDetails($_SESSION['user_id']);

                if($bAnonymousFlag)
                {
                    $res_ary['iAnonymusBooking'] = 0;
                }
                $res_ary['idUser'] = $kUser->id ;				
                $res_ary['szFirstName'] = $kUser->szFirstName ;
                $res_ary['szLastName'] = $kUser->szLastName ;
                $res_ary['szEmail'] = $kUser->szEmail ;
                $res_ary['szCustomerAddress1'] = $kUser->szAddress1;
                $res_ary['szCustomerAddress2'] = $kUser->szAddress2;
                $res_ary['szCustomerAddress3'] = $kUser->szAddress3;
                $res_ary['szCustomerPostCode'] = $kUser->szPostcode;
                $res_ary['szCustomerCity'] = $kUser->szCity;
                $res_ary['szCustomerCountry'] = $kUser->szCountry;
                $res_ary['szCustomerState'] = $kUser->szState;
                $res_ary['szCustomerPhoneNumber'] = $kUser->szPhoneNumber;
                $res_ary['szCustomerCompanyName'] = $kUser->szCompanyName;	
                $res_ary['szCustomerCompanyRegNo'] = $kUser->szCompanyRegNo; 
                
                if(!empty($res_ary))
                {
                    foreach($res_ary as $key=>$value)
                    {
                        $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                }  
                $update_query = rtrim($update_query,",");    
                $kBooking->updateDraftBooking($update_query,$idBooking) ; 
                return true;
            }
        } 
        
        function checkCFSMappingWithHaulageModel($idWarehouse,$idHaulageModel,$idDirection)
	{
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_CFS_MODEL_MAPPING__."
			WHERE
				idWarehouse='".(int)$idWarehouse."'
			AND
				idHaulageModel='".(int)$idHaulageModel."'
			AND
				iDirection='".(int)$idDirection."'
		";
		//echo $query;
		$result=$this->exeSQL($query);
		
		$row=$this->getAssoc($result);
		if((int)$row['id']>0)
                {
                    return true;
                }
		return false;
			
	}
        
	function set_id( $value )
	{
		$this->id = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "id", t($this->t_base_cfs.'fields/warehouse_id'), false, false, true );
	}
	function set_szState( $value )
	{
		$this->szState = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szState", t($this->t_base_cfs.'fields/p_r_s'), false, 255, false );
	}
	function set_szCountry( $value )
	{
		$this->szCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCountry", t($this->t_base_cfs.'fields/country'), false, 255, true );
	}
	function set_szPhoneNumber( $value )
	{
		$this->szPhoneNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPhoneNumber", t($this->t_base_cfs.'fields/p_number'), false, false, true );
	}
	function set_szWarehouseName( $value )
	{
            $this->szWarehouseName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCFSName", t($this->t_base_cfs.'fields/warehouse_name'), false, 255, true );
	}
	function set_szAddress1( $value )
	{
		$this->szAddress1 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAddressLine1", t($this->t_base_cfs.'fields/address'), false, 255, true );
	}
	function set_szAddress2( $value )
	{
		$this->szAddress2 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAddressLine2", t($this->t_base_cfs.'fields/address_line2'), false, 255, false );
	}
	function set_szAddress3( $value )
	{
		$this->szAddress3 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAddressLine3", t($this->t_base_cfs.'fields/address_line3'), false, 255, false );
	}
    function set_szPostcode( $value )
	{
		$this->szPostcode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPostcode", t($this->t_base_cfs.'fields/postcode'), false, 255, false );
	}
	function set_szCity( $value )
	{
		$this->szCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCity", t($this->t_base_cfs.'fields/city'), false, 255, true );
	}
	function set_szLatitude( $value )
	{
		$szLatitude = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLatitude", t($this->t_base_cfs.'fields/latitude'), false, 255, true );
		if(is_numeric($szLatitude))
		{
			$this->szLatitude=round($szLatitude,6);
		}
		else
		{
			$this->szLatitude=$szLatitude;
		}
	}
	function set_szLongitude( $value )
	{
		$szLongitude = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLongitude", t($this->t_base_cfs.'fields/longitude'), false, 255, true );
		//$this->szLongitude=round($szLongitude,6);
		if(is_numeric($szLongitude))
		{
			$this->szLongitude=round($szLongitude,6);
		}
		else
		{
			$this->szLongitude=$szLongitude;
		}
	}
	
	function set_iForwarder( $value )
	{
		$this->idForwarder = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idForwarder", t($this->t_base_cfs.'fields/forwarder_id'), false, 255, true );
	}

	function set_szSelectionAvailables( $value )
	{
		$this->szSelectionAvailables = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSelectionAvailables", t($this->t_base_error.'fields/szSelectionAvailables'), false, 255, true );
	}
	function set_iSelections( $value )
	{
		$this->iSelections = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iSelections", t($this->t_base_error.'fields/iSelections'), false, false, true );
	}
	function set_idCountry( $value )
	{
		$this->idCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idCountry", t($this->t_base_cfs.'fields/country'), false, 255, true );
	}
	function set_iLanguage( $value )
	{
		$this->iLanguage = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iLanguage", t($this->t_base.'fields/iLanguage'), false, false, true );
	}
	function set_szEmail( $value,$flag=true)
	{
		$this->szEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szEmail", t($this->t_base.'fields/email'), false, 255, $flag );
	}
	function set_szContactPerson( $value,$flag=true)
	{
		$this->szContactPerson = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szContactPerson", t($this->t_base.'fields/contact_person'), false, 255, $flag );
	}
}
?>