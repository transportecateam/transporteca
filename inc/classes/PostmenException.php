<?php
/**
 * This file is the container for Postmen api exception related functionality. 
 *
 * PostmenException.php
 *
 * @copyright Copyright (C) 2016 Transporteca
 * @author Ajay
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" ); 

class PostmenException extends Exception
{
	private $retryable;
	private $details;
	
	public function __construct($message, $code, $retryable, $details, Exception $previous = null) {
		$this->retryable = $retryable;
		$this->details = $details;
		parent::__construct($message, $code, $previous);
	}

	public function isRetryable() {
		return $this->retryable;
	}

	public function getDetails() {
		return $this->details;
	}

}
