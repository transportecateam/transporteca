<?php
/**
 * This file is the container for all affiliate related functionality.
 * All functionality related to affiliate details should be contained in this class.
 *
 * affiliate.class.php
 *
 * @copyright Copyright (C) 2012 Transporteca, LLC
 * @author Anil
 * @package Transporteca Development
 */
if (!isset($_SESSION)) 
{
  session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_INC__ . "/functions.php" );

class cAffiliate extends cDatabase
{
	/**
     * These are the variables for each user.
     */

    var $id;
    var $szFirstName;
    var $szLastName;
    var $szEmail;
    var $szNewEmail;
    var $szPhoneNumber;
    var $szCountry;
    var $szCountryName;
    var $szCountryId;
    var $szCompanyName;
    var $t_base="Affiliate/";
    
    
/**
	 * class constructor for php5
	 *
	 * @return bool
	 */
	function __construct()
	{
		parent::__construct();

		// establish a Database connection.
		//$this->connect( __DBC_USER__, __DBC_PASSWD__, __DBC_SCHEMATA__, __DBC_HOST__);
		return true;
	}
	
	function affiliateSignUp($data)
	{
		$this->set_szFirstName(trim(sanitize_all_html_input($data['szFirstName'])));
		$this->set_szLastName(trim(sanitize_all_html_input($data['szLastName'])));
		$this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szEmail']))));
		$this->set_szPhoneNumber(trim(sanitize_all_html_input(urldecode(base64_decode($data['szPhoneNumberUpdate'])))));
		$this->set_szCompanyName(trim(sanitize_all_html_input($data['szCompanyName'])));
		$this->set_szCountry(trim(sanitize_all_html_input($data['szCountry'])));
		
		
		//exit if error exist.
		if ($this->error === true)
		{
			return false;
		}
		
		if(!empty($this->szPhoneNumber))
		{
			$szPhoneNumber=$this->szPhoneNumber;
		}
		$replace_ary=array();
		$kWHSSearch = new cWHSSearch();
		$toEmail=$kWHSSearch->getManageMentVariableByDescription('__CONTACT_EMAIL__');
		//$toEmail="anil@whiz-solutions.com";
		//$subject="Interested in Affiliate Programme";
		$replace_ary['szCompanyName']=$this->szCompanyName;
		$replace_ary['szFirstName']=$this->szFirstName;
		$replace_ary['szLastName']=$this->szLastName;
		$replace_ary['szEmail']=$this->szEmail;
		$replace_ary['szCountry']=$this->szCountry;
		$replace_ary['szPhoneNumber']=$szPhoneNumber;
		$replace_ary['szDate']=date('d/m/Y');
		createEmail(__AFFILIATE_PROGRAMME_EMAIL__, $replace_ary,$toEmail,'', __STORE_SUPPORT_EMAIL__,$_SESSION['user_id'], __STORE_SUPPORT_EMAIL__);
		return true;
	}
        
        
        function getDutyProductList()
        {
            $query="
                SELECT
                    id,
                    szProductName,
                    fDutyRate
                FROM
                    ".__DBC_SCHEMATA_DUTY_PRODUCTS__."
                ORDER BY
                    szProductName ASC
                ";
                if($result=$this->exeSQL($query))
		{
                    if($this->iNumRows>0)
                    {
                        while($row=$this->getAssoc($result))
                        {
                            $resArr[]=$row;
                        }
                        return $resArr;
                    }
                    else
                    {
                        return array();
                    }
                   
                }
                else
                {
                    return array();
                }
            
        }
	
	
	function set_id( $value )
	{
		$this->id = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "id", t($this->t_base.'fields/customer_id'), false, false, true );
	}
	function set_szFirstName( $value )
	{
		$this->szFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szFirstName", t($this->t_base.'fields/f_name'), false, 255, true );
	}
	function set_szLastName( $value )
	{
		$this->szLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLastName", t($this->t_base.'fields/l_name'), false, 255, true );
	}
	function set_szEmail( $value )
	{ 
		$this->szEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szLoginEmail", t($this->t_base.'fields/email'), false, 255, true );
	
	}
	function set_szCountry( $value )
	{
		$this->szCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCountry", t($this->t_base.'fields/country'), false, 255, true );
	}
	function set_szPhoneNumber( $value )
	{
		$this->szPhoneNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPhoneNumber", t($this->t_base.'fields/p_number'), false, false, true );
	}
	function set_szCompanyName( $value )
	{
		$this->szCompanyName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCompanyName", t($this->t_base.'fields/c_name'), false, 255, false );
	}
    
    
}