<?php
/**
 * This file is the container for all admin related functionality.
 * All functionality related to admin details should be contained in this class.
 *
 * admin.class.php
 *
 * @copyright Copyright (C) 2012 Transporteca, LLC
 * @author Anil
 * @package Transporteca Development
 */
if(!isset($_SESSION))
{
    session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_INC__ . "/functions.php" );
require_once( __APP_PATH_INC__ . "/courier_functions.php" );
require_once( __APP_PATH_CLASSES__ . "/database.class.php");
require_once( __APP_PATH__ . "/inc/classes/parser.class.php" );
require_once( __APP_PATH_CLASSES__.'/tracking.class.php' );
require_once( __APP_PATH_CLASSES__.'/labels.class.php' );

class cCourierServices extends cDatabase
{  
    function __construct()
    {
        $seriverTypeArr = array(); 
        
        //Valid domestic values
        $seriverTypeArr["01"] = 'Next Day Air';
        $seriverTypeArr["02"] = '2nd Day Air'; 
        $seriverTypeArr["03"] = 'Ground';
        $seriverTypeArr["12"] = '3 Day Select';
        $seriverTypeArr["13"] = 'Next Day Air Saver';
        $seriverTypeArr["14"] = 'Next Day Air Early AM';
        $seriverTypeArr["59"] = '2nd Day Air AM';

        //Valid international values:
        $seriverTypeArr["07"] = 'Worldwide Express';
        $seriverTypeArr["08"] = 'Worldwide Expedited';
        $seriverTypeArr["11"] = 'Standard';
        $seriverTypeArr["54"] = 'Worldwide Express Plus';
        $seriverTypeArr["65"] = 'UPS Saver. Required for Rating and Ignored for Shopping';

        //Valid Poland to Poland Same Day values:
        $seriverTypeArr["82"] = 'UPS Today Standard';
        $seriverTypeArr["83"] = "UPS Today Dedicated Courier";
        $seriverTypeArr["84"] = "UPS Today Intercity";
        $seriverTypeArr["85"] = "UPS Today Express";
        $seriverTypeArr["86"] = "UPS Today Express Saver";
        $seriverTypeArr["96"] = "UPS Worldwide Express Freight";
        
        
        $this->seriverTypeArr = $seriverTypeArr; 
        parent::__construct();
        return true;
    } 
    var $t_base_courier='management/providerProduct/'; 
    var $t_base_services = "SERVICEOFFERING/"; 
    var $t_base_courier_label ="COURIERLABEL/";
    
    function calculateShippingDetails($data)
    {  
        $this->validateInputDetails($data);
        if($this->error==true)
        {
            return false;
        } 
        $newline = "<br />"; 
        
        //Please include and reference in $path_to_wsdl variable.
        $path_to_wsdl = __APP_PATH__."/wsdl/RateService_v14.wsdl";
         
        //The WSDL is not included with the sample code.
        $client = new SoapClient($path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information
         
        $request['WebAuthenticationDetail'] = array(
                'UserCredential' =>array(
                        'Key' => getProperty('key'), 
                        'Password' => getProperty('password')
                )
        ); 
        $request['ClientDetail'] = array(
                'AccountNumber' => getProperty('shipaccount'), 
                'MeterNumber' => getProperty('meter')
        );
        $request['TransactionDetail'] = array('CustomerTransactionId' => md5(time()));
        $request['Version'] = array(
                'ServiceId' => 'crs', 
                'Major' => '14', 
                'Intermediate' => '0', 
                'Minor' => '0'
        );
 
        $serviceType = 'INTERNATIONAL_PRIORITY'; 

        $request['ReturnTransitAndCommit'] = true;
        $request['RequestedShipment']['DropoffType'] = 'REGULAR_PICKUP'; // valid values REGULAR_PICKUP, REQUEST_COURIER, ...
        $request['RequestedShipment']['ShipTimestamp'] = date('c');
        
        //$data['szServiceType'] = '';
        if(!empty($data['szServiceType']))
        {
            $request['RequestedShipment']['ServiceType'] = $data['szServiceType']; // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
        }
        //$request['RequestedShipment']['PackagingType'] = $data['szPackageType']; // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...

        /*
        $request['RequestedShipment']['TotalInsuredValue']=array(
                'Ammount'=>100,
                'Currency'=>'USD'
        ); */ 
        $request['RequestedShipment']['Shipper'] = $this->addShipper($this);
        $request['RequestedShipment']['Recipient'] = $this->addRecipient($this);

        $request['RequestedShipment']['ShippingChargesPayment'] = $this->addShippingChargesPayment($this);		
        $request['RequestedShipment']['RateRequestTypes'] = 'ACCOUNT'; 
        $request['RequestedShipment']['RateRequestTypes'] = 'LIST'; 
        $request['RequestedShipment']['PurposeOfShipmentType'] = 'SAMPLE';
        $request['RequestedShipment']['PackageCount'] = '1';
        $request['RequestedShipment']['RequestedPackageLineItems'] = $this->addPackageLineItem1($this); 
  
        try 
        {
            if(setEndpoint('changeEndpoint'))
            {
                $newLocation = $client->__setLocation(setEndpoint('endpoint'));
            }

            $response = $client->getRates($request);

            $fedexApiLogsAry = array();
            $fedexApiLogsAry['idUser'] = $kObject->id ;
            $fedexApiLogsAry['iWebServiceType'] = 1 ;
            $fedexApiLogsAry['szRequestData'] = print_R($request,true);
            $fedexApiLogsAry['szResponseData'] = print_R($response,true);
            $this->addFedexApiLogs($fedexApiLogsAry);   

            if ($response -> HighestSeverity != 'FAILURE' && $response -> HighestSeverity != 'ERROR' && $response-> HighestSeverity != 'WARNING'){  	
                $rateReply = $response -> RateReplyDetails; 
                 
                $kWarehouseSearch = new cWHSSearch();
                $resultAry = $kWarehouseSearch->getCurrencyDetails(26);		

                $fInrExchangeRate = 0;
                if($resultAry['fUsdValue']>0)
                {
                    $fInrExchangeRate = $resultAry['fUsdValue'] ;						 
                }
                
                if(!empty($rateReply) && empty($data['szServiceType']))
                {
                    $szReturnStr =  '<h4>Your Rates: </h4><table class="format-2" style="width:90%">';
                    $szReturnStr .= '<tr><td><strong>Service Type</strong></td><td><strong>Amount</strong></td><td><strong>Shipping Date</strong></td><td><strong>Delivery Date</strong></td></tr>';
                     
                    foreach($rateReply as $rateReplys)
                    { 
                        $szServiceType = $rateReplys->ServiceType ;
                        $dtDeliveryTimestamp = $rateReplys->DeliveryTimestamp ;
                        
                        $TotalNetCharge = $rateReplys->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Amount ;
                        $szCurrencyCode = $rateReplys->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Currency ; 
                        $fTotalShipmentCost = 0;
                        if($fInrExchangeRate>0)
                        {
                            $fTotalShipmentCost = round((float)($TotalNetCharge * $fInrExchangeRate)); 
                        }
                        $fShipmentCost = number_format($TotalNetCharge,2,".",",") ;
                         
                        $serviceType = '<tr><td>'.$szServiceType . '</td>';
                        $amount = '<td>'.$szCurrencyCode." " . $fShipmentCost . '</td>';
                        $shippingDate = '<td>'.date('Y-m-d H:i:s') . '</td>';
                        if(array_key_exists('DeliveryTimestamp',$rateReplys)){
                                $deliveryDate= '<td>' . $rateReplys->DeliveryTimestamp . '</td>';
                        }else if(array_key_exists('TransitTime',$rateReplys)){
                                $deliveryDate= '<td>' . $rateReplys->TransitTime . '</td>';
                        }else {
                                $deliveryDate='<td>&nbsp;</td>';
                        }
                        $szReturnStr .= $serviceType . $amount.$shippingDate. $deliveryDate;
                        $szReturnStr .= '</tr>';
                    }
                    $szReturnStr .= '</table>'; 
                }
                else
                {
                    $szReturnStr =  '<h4>Your Rate: </h4><table class="format-2" style="width:90%">';
                    $szReturnStr .= '<tr><td><strong>Service Type</strong></td><td><strong>Amount</strong></td><td><strong>Shipping Date</strong></td><td><strong>Delivery Date</strong></td></tr><tr>';
                    $serviceType = '<td>'.$rateReply -> ServiceType . '</td>';
                    
                    $TotalNetCharge = $rateReply->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Amount ;
                    $fTotalShipmentCost = 0;
                    if($fInrExchangeRate>0)
                    {
                        $fTotalShipmentCost = round((float)($TotalNetCharge * $fInrExchangeRate)); 
                    } 
                    $amount = '<td>$' . number_format($fTotalShipmentCost,2,".",",") . '</td>';
                    $shippingDate = '<td>'.date('Y-m-d H:i:s') . '</td>';
                    if(array_key_exists('DeliveryTimestamp',$rateReply)){
                            $deliveryDate= '<td>' . $rateReply->DeliveryTimestamp . '</td>';
                    }else if(array_key_exists('TransitTime',$rateReply)){
                            $deliveryDate= '<td>' . $rateReply->TransitTime . '</td>';
                    }else {
                            $deliveryDate='<td>&nbsp;</td>';
                    }
                    $szReturnStr .= $serviceType . $amount.$shippingDate. $deliveryDate;
                    $szReturnStr .= '</tr>';
                    $szReturnStr .= '</table>'; 
                } 
                
                $ret_ary = array();
                $ret_ary['dtDeliveryDate'] = $deliveryDate ;
                $ret_ary['fShippingAmount'] = $amount ;
                $ret_ary['szServiceType'] =  $serviceType ;
                $ret_ary['fServiceTax'] =  round((float)(($amount*12.36)/100),2);	
                
                $this->szApiResponseText = $szReturnStr ;
                $this->iSuccessMessage = 1 ;
                
                $szLogFile = __APP_PATH__."/wsdl/response.xml";
                $strData=array();
                $strData[0] = print_R($response,true) ;
                filelog(true,$strData,$szLogFile);
                
                return true; 
            }else{  
                $szNotiFication = '';
                $notificationAry = $response->Notifications;
                if(!empty($notificationAry->LocalizedMessage))
                {
                    $szNotiFication .= $notificationAry->LocalizedMessage."<br>" ;
                }
                else if(!empty($notificationAry))
                {
                    foreach($notificationAry as $errors)
                    { 
                        $szNotiFication .= $errors->LocalizedMessage."<br>" ;
                    }
                } 
                $szLogFile = __APP_PATH__."/wsdl/response.xml";
                $strData=array();
                $strData[0] = print_R($response,true) ;
                filelog(true,$strData,$szLogFile);
                $this->iFedexApiError = 1 ;

                if(empty($szNotiFication))
                {
                    $szNotiFication = ' Internal Server Error. ';
                } 
                $this->szApiResponseText = " <strong> Fedex API Notification: </strong> <br> ".$szNotiFication ;
                $this->iSuccessMessage = 2 ;
                return true;
            } 
        } catch (SoapFault $exception) {
   
            $fedexApiLogsAry = array();
            $fedexApiLogsAry['idUser'] = $kObject->id ;
            $fedexApiLogsAry['szRequestData'] = print_R($request,true);
            $fedexApiLogsAry['szResponseData'] = print_R($response,true);
            $this->addFedexApiLogs($fedexApiLogsAry);   

            $szNotiFication =  $response->Notifications->LocalizedMessage ; 
            if(empty($szNotiFication))
            {
                $szNotiFication = ' Internal Server Error. ';
            }		    	 
            
            $this->iFedexApiError = 1 ;
            $this->szApiResponseText = " <strong> Fedex API Error: </strong> <br> ".$szNotiFication ;
            $this->iSuccessMessage = 3 ; 
            return true;       
        }
    }
    
    function addShipper($kObject)
    {
	$szAddress = $kObject->szAddress1 ;
	if(!empty($kObject->szAddress2))
	{
            $szAddress .= $kObject->szAddress2 ;
	}
	
	$shipper = array(
            'Contact' => array(
            'PersonName' => $kObject->szFirstName." ".$kObject->szLastName,
            'CompanyName' => $kObject->szCompany,
            'PhoneNumber' => $kObject->szPhone
            ),
            'Address' => array(
            'StreetLines' => array($szAddress),
            'City' => $kObject->szCity,
            'PostalCode' => $kObject->szPostcode,
            'CountryCode' => $kObject->szCountry
            )
	); 
	return $shipper;	  
    } 
	function addFedexApiLogs($data)
	{
            if(!empty($data))
            {
                $query="
                    INSERT INTO
                        ".__DBC_SCHEMATA_FEDEX_API_LOGS__."
                    (
                        idUser,
                        iWebServiceType,
                        szRequestData,
                        szResponseData,
                        dtCreatedOn
                    )
                    VALUES
                    (
                        '".mysql_escape_custom(trim($data['idUser']))."',
                        '".mysql_escape_custom(trim($data['iWebServiceType']))."',
                        '".mysql_escape_custom(trim($data['szRequestData']))."',
                        '".mysql_escape_custom(trim($data['szResponseData']))."',
                        now()
                    )
                ";		
			
                if(($result = $this->exeSQL($query)))
                {
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }		
            }
	}
    
    function addRecipient($kObject)
    { 
	$szAddress = $kObject->szAddress1 ;
	if(!empty($kObject->szAddress2))
	{
            $szAddress .= $kObject->szAddress2 ;
	}
	if($kObject->szCountry=='US')
	{		
            $recipient = array(
                    'Contact' => array(
                    'PersonName' => $kObject->szSFirstName." ".$kObject->szSLastName,
                    'CompanyName' => $kObject->szSCompany,
                    'PhoneNumber' => $kObject->szSPhone
                    ),
                    'Address' => array(
                    'StreetLines' => array($szAddress),
                    'City' => $kObject->szSCity,
                    'StateOrProvinceCode' => $kObject->szSState,
                    'PostalCode' => $kObject->szSPostcode,
                    'CountryCode' => $kObject->szSCountry,
                    'Residential' => false
                )
            );	
	}
	else
	{			
            $recipient = array(
                'Contact' => array(
                'PersonName' => $kObject->szSFirstName." ".$kObject->szSLastName,
                'CompanyName' => $kObject->szSCompany,
                'PhoneNumber' => $kObject->szSPhone
                ),
                'Address' => array(
                'StreetLines' => array($szAddress),
                'City' => $kObject->szSCity,
                'PostalCode' => $kObject->szSPostcode,
                'CountryCode' => $kObject->szSCountry,
                'Residential' => false
                )
            );	
	}
	return $recipient;	                                    
    }

    function addShippingChargesPayment()
    {
	$shippingChargesPayment = array(
            'PaymentType' => 'SENDER', // valid values RECIPIENT, SENDER and THIRD_PARTY
            'Payor' => array(
                'ResponsibleParty' => array(
                'AccountNumber' => getProperty('billaccount'),
                'CountryCode' => 'IN'
                )
            )
        );
        return $shippingChargesPayment;
    }
    function addLabelSpecification()
    {
        $labelSpecification = array(
                'LabelFormatType' => 'COMMON2D', // valid values COMMON2D, LABEL_DATA_ONLY
                'ImageType' => 'PDF',  // valid values DPL, EPL2, PDF, ZPLII and PNG
                'LabelStockType' => 'PAPER_7X4.75'
        );
        return $labelSpecification;
    }

    function addSpecialServices()
    {
	$specialServices = array(
            'SpecialServiceTypes' => array('COD'),
            'CodDetail' => array(
                'CodCollectionAmount' => array(
                'Currency' => 'USD', 
                'Amount' => 150
                ),
                'CollectionType' => 'ANY' // ANY, GUARANTEED_FUNDS
            )
	);
	return false; 
    }

    function addPackageLineItem1($kObject)
    {
        $packageLineItem = array(
            'SequenceNumber'=>1,
            'GroupPackageCount'=>1,
            'Weight' => array(
            'Value' => $kObject->fCargoWeight,
            'Units' => 'KG'
            ),
            'Dimensions' => array(
            'Length' => $kObject->fCargoLength,
            'Width' => $kObject->fCargoWidth,
            'Height' => $kObject->fCargoHeight,
            'Units' => 'CM'
            )
        );
        return $packageLineItem;
    }
    
    function validateInputDetails($data)
    {
        if(!empty($data))
        { 
            $this->set_szFirstName(trim(sanitize_all_html_input($data['szFirstName'])),false);
            $this->set_szLastName(trim(sanitize_all_html_input($data['szLastName'])),false);
            $this->set_szCity(trim(sanitize_all_html_input($data['szCity'])),false);
            $this->set_szState(trim(sanitize_all_html_input($data['szState'])));
            $this->set_szCountry(trim(sanitize_all_html_input($data['szCountry'])));
            $this->set_szAddress1(trim(sanitize_all_html_input($data['szAddress1'])),false);
            $this->set_szAddress2(trim(sanitize_all_html_input($data['szAddress2'])),false);
            $this->set_szAddress3(trim(sanitize_all_html_input($data['szAddress3'])));
            $this->set_szPostcode(trim(sanitize_all_html_input($data['szPostCode'])));

            $this->set_szSFirstName(trim(sanitize_all_html_input($data['szSFirstName'])),false);
            $this->set_szSLastName(trim(sanitize_all_html_input($data['szSLastName'])),false);
            $this->set_szSCity(trim(sanitize_all_html_input($data['szSCity'])),false);
            $this->set_szSState(trim(sanitize_all_html_input($data['szSState'])));
            $this->set_szSCountry(trim(sanitize_all_html_input($data['szSCountry'])));
            $this->set_szSAddress1(trim(sanitize_all_html_input($data['szSAddress1'])),false);
            $this->set_szSAddress2(trim(sanitize_all_html_input($data['szSAddress2'])));
            $this->set_szSAddress3(trim(sanitize_all_html_input($data['szSAddress3'])));
            $this->set_szSPostcode(trim(sanitize_all_html_input($data['szSPostCode'])));

            $this->set_fCargoLength(trim(sanitize_all_html_input($data['fCargoLength'])));
            $this->set_fCargoWidth(trim(sanitize_all_html_input($data['fCargoWidth'])));
            $this->set_fCargoHeight(trim(sanitize_all_html_input($data['fCargoHeight'])));
            $this->set_fCargoWeight(trim(sanitize_all_html_input($data['fCargoWeight'])));

            if($this->error==true)
            {
                return false;
            } 
            else
            {
                return true;
            }
        }
    }
     
    function calculateRatesFromUpsApi($data)
    {  
        $this->validateInputDetails($data); 
        if($this->error==true)
        {
            return false;
        }  
        
        //Configuration
        if(!empty($data['szAccountNumber']))
        {
            $access = $data['szAccountNumber'];
        }
        else
        {
            $access = __UPS_ACCESS_KEY__;
        }
        if(!empty($data['szUsername']))
        {
            $userid = $data['szUsername'];
        }
        else
        {
            $userid = __UPS_API_USER_ID__;
        }
        if(!empty($data['szPassword']))
        {
            $passwd = $data['szPassword'];
        }
        else
        {
            $passwd = __UPS_API_PASSWORD__; 
        } 
        
        $wsdl = __APP_PATH__."/wsdl/RateWS.wsdl";
        $operation = "ProcessRate";
        $endpointurl = 'http://wwwapps.ups.com/ctc/htmlTool';
        $outputFileName = __APP_PATH__."/wsdl/XOLTResult.xml";
  
        try
        { 
            $mode = array
            (
                 'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
                 'trace' => 1
            ); 
            // initialize soap client
            $client = new SoapClient($wsdl , $mode);
 
            //create soap header
            $usernameToken['Username'] = $userid;
            $usernameToken['Password'] = $passwd;
            $serviceAccessLicense['AccessLicenseNumber'] = $access;
            $upss['UsernameToken'] = $usernameToken;
            $upss['ServiceAccessToken'] = $serviceAccessLicense;

            $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0','UPSSecurity',$upss);
            print_R($header);
            echo "<br><br> Request <br><br> ";
            $request = $this->processRate($data);
            print_R($request);
            die;
            try{
                //get response
                $resp = $client->__soapCall($operation ,array($request)); 
            }
            catch(Exception $ex)
            {
               print_R($ex);
            } 
            $seriverTypeArr=array("01"=>"Next Day Air","02"=>"2nd Day Air","03"=>"Ground","12"=>"3 Day Select","13"=>"Next Day Air Saver","14"=>"Next Day Air Early AM","59"=>"2nd Day Air AM",
            "07"=>"Worldwide Express","08"=>"Worldwide Expedited","11"=>"Standard","54"=>"Worldwide Express Plus","65"=>"UPS Saver. Required for Rating and Ignored for Shopping",
            "82"=>"UPS Today Standard","83"=>"UPS Today Dedicated Courier","84"=>"UPS Today Intercity","85"=>"UPS Today Express","86"=>"UPS Today Express Saver","96"=>"UPS Worldwide Express Freight");
            
            $requestFileName = __APP_PATH__."/wsdl/fedex_api_request.xml";
            $responseFileName = __APP_PATH__."/wsdl/fedex_api_response.xml";

            //save soap request and response to file
            $fw = fopen($requestFileName , 'w');
            fwrite($fw , "Request: \n" . $client->__getLastRequest() . "\n");
            fclose($fw);

            $fw = fopen($responseFileName , 'w');
            fwrite($fw , "Response: \n" . $client->__getLastResponse() . "\n");
            fclose($fw);
            
            //get status
            $szResponseStatus = $resp->Response->ResponseStatus->Description;
            
            if(strtolower($szResponseStatus)=='success')
            {
                $RatedShipmentAry = $resp->RatedShipment ;  
                $fedexApiLogsAry = array(); 
                $fedexApiLogsAry['iWebServiceType'] = 2 ;
                $fedexApiLogsAry['szRequestData'] = print_R($request,true);
                $fedexApiLogsAry['szResponseData'] = print_R($resp,true);
                $this->addFedexApiLogs($fedexApiLogsAry);
            
                
                
                $kWarehouseSearch = new cWHSSearch();
                $resultAry = $kWarehouseSearch->getCurrencyDetails(26);		

                $fInrExchangeRate = 0;
                if($resultAry['fUsdValue']>0)
                {
                    $fInrExchangeRate = $resultAry['fUsdValue'] ;						 
                }
                $szReturnStr =  '<h4>Your Rates: </h4><table class="format-2" style="width:90%">';
                $szReturnStr .= '<tr><td><strong>Service Type</strong></td><td><strong>Amount</strong></td><td><strong>Shipping Date</strong></td><td><strong>Delivery Date</strong></td></tr>';
                if(!empty($RatedShipmentAry))
                {
                    foreach($RatedShipmentAry as $RatedShipmentArys)
                    {
                        $iTransitDays = $RatedShipmentArys->GuaranteedDelivery->BusinessDaysInTransit ;
                        $TotalNetCharge = $RatedShipmentArys->TotalCharges->MonetaryValue;
                        $dtShipmentTime = $RatedShipmentArys->GuaranteedDelivery->DeliveryByTime;
                        $szCurrencyCode = $RatedShipmentArys->TotalCharges->CurrencyCode ;
                        $idServiceCode = $RatedShipmentArys->Service->Code;
                         
                        if($TotalNetCharge>0)
                        {
                            if($iTransitDays>0)
                            {
                                if(empty($dtShipmentTime))
                                {
                                    $dtShipmentTime = "By EOD";
                                } 
                                $szTransitDays = date('Y-m-d',strtotime("+".($iTransitDays+1)." DAYS"))." ".$dtShipmentTime ;
                            }
                            else
                            { 
                                $szTransitDays = 'N/A';
                            }

                            $fTotalShipmentCost = 0;
                            if($fInrExchangeRate>0)
                            {
                                $fTotalShipmentCost = round((float)($TotalNetCharge * $fInrExchangeRate)); 
                            }
                            $fShipmentCost = number_format($TotalNetCharge,2,".",",") ;
                            $serviceType = '<tr><td>'.$seriverTypeArr[$idServiceCode].'</td>';
                            $amount = '<td>'.$szCurrencyCode." " . $fShipmentCost . '</td>';
                            $shippingDate = '<td>'.date('Y-m-d H:i:s') . '</td>';
                            $deliveryDate =  '<td>'.$szTransitDays. '</td>';

                            $szReturnStr .= $serviceType . $amount.$shippingDate. $deliveryDate;
                            $szReturnStr .= '</tr>'; 
                        }
                    }
                    $szReturnStr .= '</table>'; 
                    $this->szApiResponseText = $szReturnStr ;
                    $this->iSuccessMessage = 1 ;
                    return true;
                }
            }
            else
            {
                $this->iFedexApiError = 1 ;
                $this->szApiResponseText = " <strong> UPS API Error: </strong> <br> No service available" ;
                $this->iSuccessMessage = 3 ; 
            }
            
             //save soap request and response to file
            $fw = fopen($outputFileName , 'w'); 
            fwrite($fw , "Response: \n" . print_R($resp,true) . "\n");
            fclose($fw); 
        }
        catch(Exception $ex)
        {  
            $this->iFedexApiError = 1 ;
            $this->szApiResponseText = " <strong> UPS API Error: </strong> <br> ".$ex->getMessage() ;
            $this->iSuccessMessage = 3 ;  
            return true;
        }
    }  
    function processRate($kObject)
    { 
        //create soap request
        $request = array();
        $option = array();
         
        //$option['RequestAction'] = 'Rate';
        $option['RequestOption'] = 'Shop';
        $request['Request'] = $option; 
        
        $pickuptype['Code'] = '03';
        $pickuptype['Description'] = 'Customer counter';
        $request['PickupType'] = $pickuptype;
        
        //$request['AccountNumber'] = 'A12E28';  
        $customerclassification['Code'] = '00'; 
        $customerclassification['Description'] = 'Classfication';
        $request['CustomerClassification'] = $customerclassification;
           
        $ctr=1;
        $iQuantity = 0; 
         
        $packaging1['Code'] = '02';
        $packaging1['Description'] = 'Package/customer supplied';
        $package1['PackagingType'] = $packaging1;

        $szCargoMeasureCode = 'CM';
        $szCargoMeasure = 'Centimeters';

        $szWeightMeasureCode = 'KGS';
        $szWeightMeasure = 'Kgs';
        
        $dunit1['Code'] = $szCargoMeasureCode;
        $dunit1['Description'] = $szCargoMeasure; 
        $dimensions1['Length'] = $kObject['fCargoLength'];
        $dimensions1['Width'] = $kObject['fCargoWidth'];
        $dimensions1['Height'] = $kObject['fCargoHeight']; 
        $dimensions1['UnitOfMeasurement'] = $dunit1; 
        $package1['Dimensions'] = $dimensions1;
        
        $punit1['Code'] = $szWeightMeasureCode;
        $punit1['Description'] = $szWeightMeasure;
        $packageweight1['Weight'] = $kObject['fCargoWeight'];
        $packageweight1['UnitOfMeasurement'] = $punit1;
        $package1['PackageWeight'] = $packageweight1;  
        $packageArr[]=$package1;  
        
        $szPackageDetailsLogs .= "<br> Package: ".$kObject['fCargoLength']." X ".$kObject['fCargoWidth']." X ".$kObject['fCargoHeight']." ".$szCargoMeasure. ", ".$kObject['fCargoHeight']." ".$szWeightMeasure.", Count: 1 " ;
                  
        $this->szPackageDetailsLogs = $szPackageDetailsLogs;
        $this->szPackageDetailsLogs .= "<br><br> <u>Shipper </u><br> ";
        $this->szPackageDetailsLogs .= "Address: ".$kObject['szAddress1'];
        $this->szPackageDetailsLogs .= "<br>Postcode: ".$kObject['szPostCode'];
        $this->szPackageDetailsLogs .= "<br>City: ".$kObject['szCity'];
        $this->szPackageDetailsLogs .= "<br>Country: ".$kObject['szCountry'];
        
        $this->szPackageDetailsLogs .= "<br><br> <u>Consignee </u><br> ";
        $this->szPackageDetailsLogs .= "Address: ".$kObject['szSAddress1'];
        $this->szPackageDetailsLogs .= "<br>Postcode: ".$kObject['szSPostCode'];
        $this->szPackageDetailsLogs .= "<br>City: ".$kObject['szSCity'];
        $this->szPackageDetailsLogs .= "<br>Country: ".$kObject['szSCountry'];
        
        $szAddress = $kObject['szAddress1'] ;
        if(!empty($kObject['szAddress2']))
        {
            $szAddress .= $kObject['szAddress2'] ;
        } 
        
        $shipper['Name'] = $kObject['szFirstName']." ".$kObject['szLastName']; 
        $shipper['ShipperNumber'] = decrypt($kObject['szMeterNumber'],ENCRYPT_KEY);
           
        $address = array();
        $address['AddressLine'] = array( $szAddress ); 
        $address['StateProvinceCode'] = $kObject['szState']; 
        $address['PostalCode'] = "2100"; 
        $address['CountryCode'] = "DK";
         
        $shipper['Address'] = $address;
        $shipment['Shipper'] = $shipper;

        $szAddress = $kObject['szSAddress1'] ;
        if(!empty($kObject['szSAddress2']))
        {
            $szAddress .= $kObject['szSAddress2'] ;
        }

        $shipto['Name'] = $kObject['szSFirstName']." ".$kObject['szSLastName'];
        $addressTo = array();
        $addressTo['AddressLine'] = $szAddress; 
        $addressTo['PostalCode'] = $kObject['szSPostCode'];
        $addressTo['CountryCode'] = $kObject['szSCountry']; 
        $shipto['Address'] = $addressTo;
        $shipment['ShipTo'] = $shipto;

        $shipfrom['Name'] = $kObject['szFirstName']." ".$kObject['szLastName'];
        $addressFrom['AddressLine'] = array(); 
        $addressFrom['PostalCode'] = $kObject['szPostCode'];
        $addressFrom['CountryCode'] = $kObject['szCountry']; 
         
        $shipfrom['Address'] = $addressFrom;
        $shipment['ShipFrom'] = $shipfrom;   
        $shipment['Package'] = $packageArr; 
        $shipment['ShipmentRatingOptions']['NegotiatedRatesIndicator'] = 1;
          
        $shipment['Commodity'] = $CommodityValue;     
            
        $shipment['RatingMethodRequestedIndicator'] = 1;
        $shipment['TaxInformationIndicator'] = 1;
          
        if($kObject['szSCountry']=='DK')
        {
            $shipment['ShipmentServiceOptions']['ImportControl']['Code'] = '04';
            $shipment['ShipmentServiceOptions']['ImportControl']['Description'] = 'ImportControl Electronic Label'; 
            $FRSPaymentInformation = array();
            $FRSPaymentInformation['Type']['Code'] = '03';
            $FRSPaymentInformation['Type']['Description'] = 'BillThirdParty';
            $FRSPaymentInformation['AccountNumber'] = decrypt($kObject['szMeterNumber'],ENCRYPT_KEY);
            $FRSPaymentInformation['Address']['PostalCode'] = '2100';
            $FRSPaymentInformation['Address']['CountryCode'] = 'DK';
            $shipment['FRSPaymentInformation'] = $FRSPaymentInformation;
        }  
        $request['Shipment'] = $shipment;    
        return $request;
    }
    function processRate_backup($kObject,$flag=false,$packageFlag=false)
    {
        //create soap request
        $request = array();
        $option = array();
        
        $option['RequestOption'] = 'Shop';
        $request['Request'] = $option;
        
        $kObject->szFirstName = 'Ajay';
        $kObject->szLastName = 'Jha';
        
        $kObject->szSFirstName = 'Ajay';
        $kObject->szSLastName = 'Jha';

        $customerclassification['Code'] = '01';
        $customerclassification['Description'] = 'Classfication';
        $request['CustomerClassification'] = $customerclassification;
         
        $szAddress = $kObject->szAddress1 ;
        if(!empty($kObject->szAddress2))
        {
            $szAddress .= $kObject->szAddress2 ;
        }

        $shipper['Name'] = $kObject->szFirstName." ".$kObject->szLastName;
        $shipper['ShipperNumber'] = '222006';

        $address = array();
        $address['AddressLine'] = array( $szAddress );
        $address['City'] = $kObject->szCity;
        $address['StateProvinceCode'] = $kObject->szState;
        $address['PostalCode'] = $kObject->szPostcode;
        $address['CountryCode'] = $kObject->szCountry;

        $shipper['Address'] = $address;
        $shipment['Shipper'] = $shipper;

        $szAddress = $kObject->szSAddress1 ;
        if(!empty($kObject->szSAddress2))
        {
            $szAddress .= $kObject->szSAddress2 ;
        }

        $shipto['Name'] = $kObject->szSFirstName." ".$kObject->szSLastName;
        $addressTo = array();
        $addressTo['AddressLine'] = $szAddress;
        $addressTo['City'] = $kObject->szSCity;
        $addressTo['StateProvinceCode'] = $kObject->szSState;
        $addressTo['PostalCode'] = $kObject->szSPostcode;
        $addressTo['CountryCode'] = $kObject->szSCountry;
        //$addressTo['ResidentialAddressIndicator'] = '';
        $shipto['Address'] = $addressTo;
        $shipment['ShipTo'] = $shipto;

        $shipfrom['Name'] = $kObject->szFirstName." ".$kObject->szLastName;
        $addressFrom['AddressLine'] = array();
        $addressFrom['City'] = $kObject->szCity;
        $addressFrom['StateProvinceCode'] = $kObject->szState;
        $addressFrom['PostalCode'] = $kObject->szPostcode;
        $addressFrom['CountryCode'] = $kObject->szCountry;
        $shipfrom['Address'] = $addressFrom;
        $shipment['ShipFrom'] = $shipfrom;

        $service['Code'] = '03';
        $service['Description'] = 'Service Code';
        $shipment['Service'] = $service;

        $packaging1['Code'] = '00';
        $packaging1['Description'] = 'Your Packaging';
        $package1['PackagingType'] = $packaging1;

        $punit1['Code'] = 'KGS';
        $punit1['Description'] = 'Kgs';
        $packageweight1['Weight'] = $kObject->fCargoWeight;
        $packageweight1['UnitOfMeasurement'] = $punit1;
        $package1['PackageWeight'] = $packageweight1;

        $shipment['Package'] = array($package1);
        $shipment['ShipmentServiceOptions'] = '';
        $shipment['LargePackageIndicator'] = '';
        $shipment['Commodity'] = $CommodityValue;
        $request['Shipment'] = $shipment;  
        return $request;
    }
    
    
    function validateProviderProduct($data)
    {
    		$this->set_szName(sanitize_specific_html_input(trim($data['szName'])));
			$this->set_fPrice(sanitize_all_html_input(trim($data['fPrice'])));
			$this->set_iBookingPriceCurrency(sanitize_all_html_input(trim($data['iBookingPriceCurrency'])));
			
			//exit if error exist.
			if ($this->error === true)
			{
				return false;
			}
			
			if($this->szName!='' && $this->checkCourierProviderName((int)$data['id']))
			{
				$this->addError( "szName" , t($this->t_base_courier.'messages/company_name_already_exists') );
            	return false;
			}
			
			return true;
    }
    
    function checkCourierProviderName($id=0)
    {
    	$queryWhere='';
    	if((int)$id>0)
    	{
    		$queryWhere="
    			AND
    				id<>'".(int)$id."'
    		";
    		
    	}
    	$query="
    		SELECT
    			id
    		FROM
    			".__DBC_SCHEMATA_COUIER_PROVIDER__."
    		WHERE	
    			szName='".mysql_escape_custom($this->szName)."'
    		AND
    			isDeleted='0'	
    		$queryWhere			
    	";
    	//echo $query;
    	if( ( $result = $this->exeSQL( $query ) ) )
		{
            if( $this->getRowCnt() > 0 )
            {
            	return true;
            }
            else
            {
            	return false;
            }
		}
		else
		{
			return false;
		}
    	
    }
    
    function addEditCourierProvider($data)
    {
    	if($this->validateProviderProduct($data))
    	{
    	
    		if((int)$data['id']>0)
    		{
    			$query="
	    			UPDATE
	    				".__DBC_SCHEMATA_COUIER_PROVIDER__."
	    			SET
	    				szName='".mysql_escape_custom($this->szName)."',
	    				fPrice='".(float)$this->fPrice."',
	    				iBookingPriceCurrency='".(int)$this->iBookingPriceCurrency."'
					WHERE
						id='".(int)$data['id']."'
	    		";
	    		$result = $this->exeSQL( $query );
	    		//$id = $this->iLastInsertID ;
	    		$this->idProvider=$data['id'];	
	    		return true;
    		}
    		else
    		{
	    		$query="
	    			INSERT INTO
	    				".__DBC_SCHEMATA_COUIER_PROVIDER__."
	    			(
	    				szName,
	    				fPrice,
	    				dtCreated,
	    				iBookingPriceCurrency
	    			)
	    				VALUES
	    			(
						'".mysql_escape_custom($this->szName)."',
						'".(float)$this->fPrice."',
						NOW(),
						'".(int)$this->iBookingPriceCurrency."'
						
	    			)
	    		";
	    		$result = $this->exeSQL( $query );
	    		$id = $this->iLastInsertID ;
	    		$this->idProvider=$id;	
	    		return true;
    		}
    	}
    
    }
    
    
    function getCourierProviderList($id=0)
    {
    	$queryWhere='';
    	if((int)$id>0)
    	{
    		$queryWhere="
    			AND
    				p.id='".(int)$id."'
    		";
    		
    	}
    	$query="
    		SELECT
    			p.id,
    			p.szName,
    			p.fPrice,
    			p.dtCreated,
    			p.szProviderName,
    			p.iBookingPriceCurrency,
    			c.szCurrency
    		FROM
    			".__DBC_SCHEMATA_COUIER_PROVIDER__." AS p
    		INNER JOIN
    			".__DBC_SCHEMATA_CURRENCY__." AS c
    		ON
    			c.id=p.iBookingPriceCurrency
    		WHERE	
    			p.isDeleted='0'	
    		$queryWhere	
    		ORDER BY
    			p.szName ASC		
    	";
    	if( ( $result = $this->exeSQL( $query ) ) )
		{
            if( $this->getRowCnt() > 0 )
            {
            		$ret_ary = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {
                        $ret_ary[$ctr] = $row ;
                        $ctr++;
                    }
                    return $ret_ary;
            }
            else
            {
            	return array();
            }
		}
		else
		{
			return array();
		}
    	
    }
    
    function getCourierProviderDetails()
    { 
        $query="
            SELECT
                id,
                szProviderName,
                szName,
                fPrice,
                iBookingPriceCurrency,
                isDeleted,
                dtCreated
            FROM
                ".__DBC_SCHEMATA_COUIER_PROVIDER__."
            WHERE 
                isDeleted = '0' 
        "; 
        //echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        { 
            if($this->iNumRows>0)
            { 
                $ret_ary = array();
                while($row = $this->getAssoc($result))
                {
                    $ret_ary[$row['id']] = $row;
                }
                return $ret_ary ;
            }
            else
            {
                return array();
            }
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
    
    function deleteCourierProvider($idProvider)
    {
	    if((int)$idProvider>0)
		{
    		$query="
	    			UPDATE
	    				".__DBC_SCHEMATA_COUIER_PROVIDER__."
	    			SET
	    				isDeleted='1'
					WHERE
						id='".(int)$idProvider."'
	    		";
	    		$result = $this->exeSQL( $query );
    		return true;
		}
    }
    
    function selectProviderPackingList($id=0,$idLanguage=0)
    {
    	$queryWhere='';
    	if((int)$id>0)
    	{
            $queryWhere="
                    WHERE
                        id='".(int)$id."' 
            "; 
    	}
        
        $kConfig = new cConfig();
        $configLangArr=$kConfig->getConfigurationLanguageData('__TABLE_COURIER_PROVIDER_PRODUCT_PACKING__',$idLanguage);
        
    	$query="
            SELECT
                id
            FROM	
                ".__DBC_SCHEMATA_COUIER_PROVIDER_PRODUCTS_PACKING__."
            $queryWhere	
    	";
        
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
                $ret_ary = array();
                if((int)$id>0)
                {	
                    $row=$this->getAssoc($result);
                    $ctr=0;
                    if(!empty($configLangArr[$row['id']]))
                    {
                        $ret_ary[$ctr] = array_merge($configLangArr[$row['id']],$row);
                        $ret_ary[$ctr]['szSingle']=$configLangArr[$row['id']]['szPackingSingle'];
                    }else
                    {
                        $ret_ary[$ctr] = $row;
                    }
                }
                else
                {
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {
                        if(!empty($configLangArr[$row['id']]))
                        {
                            $ret_ary[$ctr] = array_merge($configLangArr[$row['id']],$row);
                            $ret_ary[$ctr]['szSingle']=$configLangArr[$row['id']]['szPackingSingle'];
                        }else
                        {
                            $ret_ary[$ctr] = $row;
                        }
                        $ctr++;
                    }
                }
                return $ret_ary;
            }
            else
            {
            	return array();
            }
        }
        else
        {
                return array();
        } 
    }
    
    function getCourierProviderProductList($idCourierProvider=false,$id=0,$idLanguage=__LANGUAGE_ID_ENGLISH__)
    {
        $queryWhere='';
    	if((int)$id>0)
    	{
            $queryWhere="
                AND  pp.id='".(int)$id."'
            "; 
        }
        
        if((int)$idCourierProvider>0)
    	{
            $queryWhere .= "
                AND  pp.idCourierProvider='".(int)$idCourierProvider."'	
            "; 
        }
        $kConfig = new cConfig();
        $configLangArr=$kConfig->getConfigurationLanguageData('__TABLE_COURIER_PROVIDER_PRODUCT_PACKING__',$idLanguage,false,true); 
                
    	$query="
            SELECT
                pp.id,
                pp.idCourierProvider,
                pp.szName,
                pp.szApiCode,
                pp.idPacking,
                pp.isDeleted,
                pp.dtCreated,
                pp.iDaysStd,
                ppp.id as idPackingType,
                pp.idGroup,
                cpg.szName AS szGroupName,
                pp.iDaysMin
            FROM	
                ".__DBC_SCHEMATA_COUIER_PROVIDER_PRODUCTS__." AS pp
            INNER JOIN
                ".__DBC_SCHEMATA_COUIER_PROVIDER_PRODUCTS_PACKING__." AS ppp
            ON
                ppp.id=pp.idPacking 
            INNER JOIN
                ".__DBC_SCHEMATA_COURIER_PRODUCT_GROUP__." AS cpg
            ON
                cpg.id=pp.idGroup         
            WHERE  
                pp.isDeleted='0'	
            $queryWhere	
            ORDER BY
                pp.szName ASC
    	";
    	//echo $query;
    	if(($result = $this->exeSQL($query)))
        {
            if($this->getRowCnt()>0)
            {
                $ret_ary = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    if(!empty($configLangArr[$row['idPackingType']]))
                    { 
                        $ret_ary[$ctr] = array_merge($row,$configLangArr[$row['idPackingType']]);
                    }
                    else
                    {
                        $ret_ary[$ctr] = $row;
                    }
                    $ctr++;
                }
                return $ret_ary;
            }
            else
            {
            	return array();
            }
        }
        else
        {
            return array();
        } 
    }
    
    
    function validateProviderProductData($data)
    {
        $this->set_szProductName(sanitize_specific_html_input(trim($data['szProductName'])));
        $this->set_szApiCode(sanitize_all_html_input(trim($data['szApiCode'])));
        $this->set_idPacking(sanitize_all_html_input(trim($data['idPacking'])));
        $this->set_idGroup(sanitize_all_html_input(trim($data['idGroup'])));
        $this->set_iDaysStd(sanitize_all_html_input(trim($data['iDaysStd'])));
        $this->set_iDaysMin(sanitize_all_html_input(trim($data['iDaysMin'])));
	
        //exit if error exist.
        if ($this->error === true)
        {
            return false;
        } 
        return true;
    }
    
    function addEditProviderProductData($data)
    {
    	if($this->validateProviderProductData($data))
    	{
    	
    		if((int)$data['id']>0)
    		{
    			$query="
	    			UPDATE
                                    ".__DBC_SCHEMATA_COUIER_PROVIDER_PRODUCTS__."
	    			SET
                                    szName='".mysql_escape_custom($this->szProductName)."',
                                    szApiCode='".mysql_escape_custom($this->szApiCode)."',
                                    idPacking='".mysql_escape_custom($this->idPacking)."',
                                    idGroup=  '".(int)$this->idGroup."', 
                                    iDaysStd=  '".(int)$this->iDaysStd."',
                                    iDaysMin=  '".(int)$this->iDaysMin."'    
                                WHERE
                                    id='".(int)$data['id']."'
	    		";
	    		$result = $this->exeSQL( $query );
	    		//$id = $this->iLastInsertID ;
	    		$this->idProvider=$data['id'];	
	    		return true;
    		}
    		else
    		{
	    		$query="
	    			INSERT INTO
	    				".__DBC_SCHEMATA_COUIER_PROVIDER_PRODUCTS__."
	    			(
                                    szName,
                                    szApiCode,
                                    idPacking,
                                    dtCreated,
                                    idCourierProvider,
                                    idGroup,
                                    iDaysStd,
                                    iDaysMin
	    			)
                                    VALUES
	    			(
                                    '".mysql_escape_custom($this->szProductName)."',
                                    '".mysql_escape_custom($this->szApiCode)."',
                                    '".mysql_escape_custom($this->idPacking)."',
                                    NOW(),
                                    '".(int)$data['idCourierProvider']."',
                                    '".(int)$this->idGroup."',
                                    '".(int)$this->iDaysStd."',
                                    '".(int)$this->iDaysMin."'    
                                                    
						
	    			)
	    		";
	    		$result = $this->exeSQL( $query );
	    		$id = $this->iLastInsertID ;
	    		//$this->idProvider=$id;	
	    		return true;
    		}
    	}
    }
    
	function deleteCourierProviderProduct($idProviderProduct)
    {
	    if((int)$idProviderProduct>0)
		{
    		$query="
	    			UPDATE
	    				".__DBC_SCHEMATA_COUIER_PROVIDER_PRODUCTS__."
	    			SET
	    				isDeleted='1'
					WHERE
						id='".(int)$idProviderProduct."'
	    		";
	    		$result = $this->exeSQL( $query );
    		return true;
		}
    }
    
    function getCourierCargoLimitionsType()
    {
    	$query="
    		SELECT
    			id,
    			szVariable,
    			iSorting,
    			szUnit
    		FROM	
    			".__DBC_SCHEMATA_COUIER_CARGO_LIMITATION_TYPE_PACKING__."
    		ORDER BY
    			iSorting ASC	
    	";
    	if( ( $result = $this->exeSQL( $query ) ) )
		{
            if( $this->getRowCnt() > 0 )
            {
            		$ret_ary = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {
                        $ret_ary[$ctr] = $row ;
                        $ctr++;
                    }
                    return $ret_ary;
            }
            else
            {
            	return array();
            }
		}
		else
		{
			return array();
		}
    
    }
    
    
    function validateCourierCargoLimitationData($data)
    {
        $this->set_szRestriction(sanitize_specific_html_input(trim($data['szRestriction'])));
        $this->set_idCargoLimitationType(sanitize_all_html_input(trim($data['idCargoLimitationType'])));
        $this->set_szLimit(sanitize_all_html_input(trim($data['szLimit'])));
        if((int)$data['idGroup']==0)
        {
            $this->addError("idGroup" , t($this->t_base_courier.'messages/invalid_group_id') );
                return false;
        }
			
        //exit if error exist.
        if ($this->error === true)
        {
            return false;
        }
        
        $queryWhere="";
        if((int)$data['id']>0)
        {
            $queryWhere=" AND id<>'".(int)$data['id']."' ";    			
        }
        
        $query="
            SELECT
                id
            FROM
                ".__DBC_SCHEMATA_COUIER_CARGO_LIMITATION_DATA__."
            WHERE
                idGroup='".(int)$data['idGroup']."'
            AND
                idCargoLimitationType='".mysql_escape_custom($this->idCargoLimitationType)."'		
            AND
                isDeleted='0'
            AND
                szRestriction='".mysql_escape_custom($this->szRestriction)."'
            $queryWhere	
        ";
        //echo $query;
        if(($result = $this->exeSQL($query)))
        {
            if($this->getRowCnt()>0)
            {
                $this->addError("idCargoLimitationType" , t($this->t_base_courier.'messages/value_already_defined') );
                return false;
            }
        }
        return true;
    } 
    function addEditCargoLimitationData($data)
    {
    	if($this->validateCourierCargoLimitationData($data))
    	{ 
            if((int)$data['id']>0)
            {
                $query="
                        UPDATE
                                ".__DBC_SCHEMATA_COUIER_CARGO_LIMITATION_DATA__."
                        SET
                                szRestriction='".mysql_escape_custom($this->szRestriction)."',
                                idCargoLimitationType='".mysql_escape_custom($this->idCargoLimitationType)."',
                                szLimit='".mysql_escape_custom($this->szLimit)."'
                                WHERE
                                        id='".(int)$data['id']."'
                ";
                $result = $this->exeSQL( $query );
                //$id = $this->iLastInsertID ;
                $this->idProvider=$data['id'];	
                return true;
            }
            else
            {
	    		$query="
	    			INSERT INTO
	    				".__DBC_SCHEMATA_COUIER_CARGO_LIMITATION_DATA__."
	    			(
	    				szRestriction,
	    				idCargoLimitationType,
	    				szLimit,
	    				idGroup
	    			)
	    				VALUES
	    			(
                                        '".mysql_escape_custom($this->szRestriction)."',
                                        '".(int)$this->idCargoLimitationType."',
                                        '".mysql_escape_custom($this->szLimit)."',
                                        '".(int)$data['idGroup']."'
						
	    			)
	    		";
	    		//echo $query;
	    		$result = $this->exeSQL( $query );
	    		$id = $this->iLastInsertID ;
	    		//$this->idProvider=$id;	
	    		return true;
    		}
    	}
    }
    
    
    function getCourierCargoLimitationData($idGroup,$id=0)
    {
    
     	$queryWhere='';
    	if((int)$id>0)
    	{
    		$queryWhere="
    			AND
    				ccld.id='".(int)$id."'
    		";
    		
    	}
    	
    	$query="
    		SELECT
    			ccld.id,
    			ccld.szRestriction,
    			ccld.idCargoLimitationType,
    			ccld.szLimit,
    			ccld.idGroup,
    			cclt.szVariable,
    			cclt.szUnit
    		FROM
    			".__DBC_SCHEMATA_COUIER_CARGO_LIMITATION_DATA__." AS ccld
    		INNER JOIN
    			".__DBC_SCHEMATA_COUIER_CARGO_LIMITATION_TYPE_PACKING__." AS cclt
    		ON
    			cclt.id=ccld.idCargoLimitationType
    		WHERE
    			ccld.idGroup='".(int)$idGroup."'	
    		AND
    			ccld.isDeleted='0'	
    		$queryWhere		
    	";
        //echo $query;
    	if( ( $result = $this->exeSQL( $query ) ) )
		{
            if( $this->getRowCnt() > 0 )
            {
            		$ret_ary = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {
                        $ret_ary[$ctr] = $row ;
                        $ctr++;
                    }
                    return $ret_ary;
            }
            else
            {
            	return array();
            }
		}
		else
		{
			return array();
		}
    
    }
    
    function deleteCourierCargoLimitation($idCargoLimit)
    {
    
   		if((int)$idCargoLimit>0)
		{
    		$query="
	    			UPDATE
	    				".__DBC_SCHEMATA_COUIER_CARGO_LIMITATION_DATA__."
	    			SET
	    				isDeleted='1'
					WHERE
						id='".(int)$idCargoLimit."'
	    		";
	    		$result = $this->exeSQL( $query );
    		return true;
		}
    }
    
    function validateTermsInstructionData($data)
    {
    	$this->set_szInstruction(sanitize_specific_html_input(trim($data['szInstruction'])));
        $this->set_szTerms(sanitize_all_html_input(trim($data['szTerms'])));
		 
        //exit if error exist.
        if ($this->error === true)
        {
            return false;
        } 
        return true;
    }
    
    
	function addEditTermsInstructionData($data)
    {
    	if($this->validateTermsInstructionData($data))
    	{
    	
    		if((int)$data['id']>0)
    		{
    			$query="
	    			UPDATE
	    				".__DBC_SCHEMATA_COUIER_PRODUCT_TERMS_INSTRUCTION_DATA__."
	    			SET
	    				szInstruction='".mysql_escape_custom($this->szInstruction)."',
	    				szTerms='".mysql_escape_custom($this->szTerms)."'
					WHERE
						id='".(int)$data['id']."'
					AND
						idLanguage='".(int)$data['idLanguage']."'	
	    		";
	    		$result = $this->exeSQL( $query );
	    		//$id = $this->iLastInsertID ;
	    		$this->idTermsInstruction=$data['id'];	
	    		return true;
    		}
    		else
    		{
	    		$query="
	    			INSERT INTO
	    				".__DBC_SCHEMATA_COUIER_PRODUCT_TERMS_INSTRUCTION_DATA__."
	    			(
	    				szInstruction,
	    				szTerms,
	    				idLanguage,
	    				idCourierProviderProduct
	    			)
	    				VALUES
	    			(
						'".mysql_escape_custom($this->szInstruction)."',
						'".mysql_escape_custom($this->szTerms)."',
						'".(int)$data['idLanguage']."',
						'".(int)$data['idCourierProviderProduct']."'
						
	    			)
	    		";
	    		//echo $query;
	    		$result = $this->exeSQL( $query );
	    		$id = $this->iLastInsertID ;
	    		$this->idTermsInstruction=$id;	
	    		return true;
    		}
    	}
    }
    
    
    function getProductTermsInstructionData($idProviderProduct,$id=0,$idLanguage=0)
    {
    	$queryWhere='';
    	if((int)$id>0)
    	{
    		$queryWhere .="
    			AND
    				id='".(int)$id."'
    		";
    		
    	}
    	
    	
    	if((int)$idLanguage>0)
    	{
    		$queryWhere .="
    			AND
    				idLanguage='".(int)$idLanguage."'
    		";
    		
    	}
    	$query="
    		SELECT
    			id,
    			szInstruction,
    			szTerms,
    			idLanguage,
    			idCourierProviderProduct
    		FROM
    			".__DBC_SCHEMATA_COUIER_PRODUCT_TERMS_INSTRUCTION_DATA__."
    		WHERE
    			idCourierProviderProduct='".(int)$idProviderProduct."'
    		$queryWhere
    		ORDER BY
    			idLanguage ASC
    	";
    	if( ( $result = $this->exeSQL( $query ) ) )
		{
            if( $this->getRowCnt() > 0 )
            {
            		$ret_ary = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {
                        $ret_ary[$ctr] = $row ;
                        $ctr++;
                    }
                    return $ret_ary;
            }
            else
            {
            	return array();
            }
		}
		else
		{
			return array();
		}
    }
    
    function getAllRegion()
    {
    	$query="
    		SELECT
    			id,
    			szRegionName,
    			szRegionShortName
    		FROM	 	
    			".__DBC_SCHEMATA_REGIONS__."
    		WHERE
    			iActive='1'
    		AND
    			isDeleted='0'	
                ORDER BY
                    szRegionName ASC
    	";
    	if( ( $result = $this->exeSQL( $query ) ) )
		{
            if( $this->getRowCnt() > 0 )
            {
            		$ret_ary = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {
                        $ret_ary[$ctr] = $row ;
                        $ctr++;
                    }
                    return $ret_ary;
            }
            else
            {
            	return array();
            }
		}
		else
		{
			return array();
		}
    }
    
    function validateModeTransportData($data)
    {
    	$this->set_idFromCountry(sanitize_specific_html_input(trim($data['idFromCountry'])));
        $this->set_idToCountry(sanitize_all_html_input(trim($data['idToCountry']))); 
        $this->set_iDTDTrades(sanitize_all_html_input(trim($data['iDTDTrades'])));
		 
        //exit if error exist.
        if ($this->error === true)
        {
            return false;
        } 

        if($this->idFromCountry!='' && $this->idToCountry!='')
        {
            $fromArr=explode("_",$this->idFromCountry);
            $fromFlag=false;
            $toFlag=false;
            if($fromArr[1]=='r')
            {
                $fromCountryArr=$this->getCountriesByIdRegion($fromArr[0]);
                $fromFlag=true;
            }
            else
            {
                $fromCountryArr[0]=$this->idFromCountry;
            }
			
            $toArr=explode("_",$this->idToCountry);
            if($toArr[1]=='r')
            {
                $toCountryArr=$this->getCountriesByIdRegion($toArr[0]);
                $toFlag=true;
            }
            else
            {
                $toCountryArr[0]=$this->idToCountry;
            } 
             
            if(!empty($toCountryArr) && !empty($fromCountryArr))
            {
                foreach($fromCountryArr as $fromCountryArrs)
                {
                    $fromCountry=$fromCountryArrs; 
                    foreach($toCountryArr as $toCountryArrs)
                    { 
                        if(!$this->checkFromCountryToCountryExists($fromCountry,$toCountryArrs,$this->iDTDTrades))
                        { 
                            return true; 
                        } 
                    } 
                }
            }		
        } 	
        return false;
    } 
    
    function getCountriesByIdRegion($idRegion)
    {
    
    	$query="
    		SELECT
    			id
    		FROM
    			".__DBC_SCHEMATA_COUNTRY__."
    		WHERE
    			idRegion='".(int)$idRegion."'
    		AND
    			iActive='1'	 	
    	";
   		 if( ( $result = $this->exeSQL( $query ) ) )
		{
            if( $this->getRowCnt() > 0 )
            {
            		$ret_ary = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {
                        $ret_ary[$ctr] = $row['id'] ;
                        $ctr++;
                    }
                    return $ret_ary;
            }
            else
            {
            	return array();
            }
		}
		else
		{
			return array();
		}
    }
    
    
    function checkFromCountryToCountryExists($idFromCountry,$idToCountry,$bDTDFlag=false)
    { 
        if($bDTDFlag)
        {
            $query_and = " AND iDTDTrades = 1 ";
        }
        else
        {
            $query_and = " AND iDTDTrades = 0 ";
        }
        
    	$query="
            SELECT
                id
            FROM
                ".__DBC_SCHEMATA_COUIER_TRUCKICON_DATA__."
            WHERE
                idFromCountry='".(int)$idFromCountry."'
            AND
                idToCountry='".(int)$idToCountry."'	 
            AND
                isDeleted='0'	
            $query_and
    	";
        //echo $query;
        //die;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
            	return true;
            }
            else
            {
            	return false;
            }
        } 
    }
    
    function addEditFromToCountryForModeTransport($data)
    { 
    	if($this->validateModeTransportData($data))
    	{  
            $fromArr=explode("_",$this->idFromCountry);
            $fromFlag=false;
            $toFlag=false;
            if($fromArr[1]=='r')
            {
                $fromCountryArr=$this->getCountriesByIdRegion($fromArr[0]);
                $fromFlag=true;
            }
            else
            {
                $fromCountryArr[0]=$this->idFromCountry;
            }

            $toArr=explode("_",$this->idToCountry);
            if($toArr[1]=='r')
            {
                $toCountryArr=$this->getCountriesByIdRegion($toArr[0]);
                $toFlag=true;
            }
            else
            {
                $toCountryArr[0]=$this->idToCountry;
            }
            $iDTDTrades = $this->iDTDTrades ; 
            if(!empty($toCountryArr) && !empty($fromCountryArr))
            {
                foreach($fromCountryArr as $fromCountryArrs)
                {
                    $fromCountry=$fromCountryArrs; 
                    foreach($toCountryArr as $toCountryArrs)
                    { 
                        if(!$this->checkFromCountryToCountryExists($fromCountry,$toCountryArrs,$iDTDTrades))
                        { 
                            $query="
                                INSERT INTO
                                    ".__DBC_SCHEMATA_COUIER_TRUCKICON_DATA__."
                                (
                                    idFromCountry,
                                    idToCountry,
                                    iDTDTrades 
                                )
                                VALUES
                                (
                                    '".(int)$fromCountry."',
                                    '".(int)$toCountryArrs."',
                                    '".(int)$iDTDTrades."'
                                )
                            ";
                            //echo $query."<br><br> ";
                            $result = $this->exeSQL( $query );
                        }
                    }
                }
            } 
            return true;
    	}
    }  
    
    function getAllCourierTruckiconData($idPrimarySortKey='',$szSortValue='',$idOtherSortKeyArr='',$bDTDFlag=false)
    {
    	if($idPrimarySortKey!='' && $szSortValue!='')
    	{
            $orderBY="
                ORDER BY
                    $idPrimarySortKey
                    $szSortValue
            ";
            $idOtherSortKeyArr;
            if(count($idOtherSortKeyArr)>0)
            {
                foreach($idOtherSortKeyArr as $idOtherSortKeyArrs)
                {
                    if(trim($idOtherSortKeyArrs)!='')
                    {	
                        $sortArr=explode("_",$idOtherSortKeyArrs);
                        $orderBY .=",
                            ".$sortArr[0]."
                            ".$sortArr[1]."
                        ";
                    }
                } 
            } 
    	}
        else
        {
            $orderBY="  ORDER BY szFromCountry ASC ";
        }
        
        if($bDTDFlag)
        {
            $query_and = " AND ctd.iDTDTrades = 1 ";
        }
        else
        {
            $query_and = " AND ctd.iDTDTrades = 0 ";
        }
        
    	$query="
            SELECT
                ctd.id,
                ctd.idFromCountry,
                ctd.idToCountry,
                cf.szCountryName AS szFromCountry,
                ct.szCountryName AS szToCountry,
                rf.szRegionName AS szFromRegionName,
                rt.szRegionName AS szToRegionName
            FROM
                ".__DBC_SCHEMATA_COUIER_TRUCKICON_DATA__." AS ctd
            INNER JOIN
                ".__DBC_SCHEMATA_COUNTRY__." AS cf
            ON
                cf.id=ctd.idFromCountry
            INNER JOIN
                ".__DBC_SCHEMATA_COUNTRY__." AS ct
            ON
                ct.id=ctd.idToCountry
            INNER JOIN
                ".__DBC_SCHEMATA_REGIONS__." AS rf
            ON
                rf.id=cf.idRegion
            INNER JOIN
                ".__DBC_SCHEMATA_REGIONS__." AS rt
            ON
                rt.id=ct.idRegion
            WHERE
                ctd.isDeleted='0'
                $query_and
            $orderBY		 
    	";
    	//echo $query;
     	if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
                $ret_ary = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    $ret_ary[$ctr] = $row;
                    $ctr++;
                }
                return $ret_ary;
            }
            else
            {
            	return array();
            }
        }
        else
        {
            return array();
        } 
    } 
    
    function deleteTruckIconData($deleteTruckArr)
    {
        if(!empty($deleteTruckArr))
        {
            foreach($deleteTruckArr as $deleteTruckArrs)
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_COUIER_TRUCKICON_DATA__."
                    SET
                        isDeleted='1'
                    WHERE
                        id='".(int)$deleteTruckArrs."'	
                ";
                $result = $this->exeSQL( $query ); 
            } 
    	} 
    } 
    
    function addCourierAgreementData($data,$bForwarderFlag=false)
    { 
    	$this->set_idCourierProvider(sanitize_all_html_input(trim($data['idCourierProvider'])));
        $this->set_szAccountNumber(sanitize_all_html_input(trim($data['szAccountNumber'])));
        $this->set_szUsername(sanitize_all_html_input(trim($data['szUsername'])));
        $this->set_szPassword(sanitize_all_html_input(trim($data['szPassword']))); 
        $this->set_szCarrierAccountID(sanitize_all_html_input(trim($data['szCarrierAccountID'])));  
        
        if($this->idCourierProvider==__TNT_API__ || $this->idCourierProvider==__DHL_API__)
        {
            $this->szMeterNumber = sanitize_all_html_input(trim($data['szMeterNumber'])); 
        }
        else
        {
            $this->set_szMeterNumber(sanitize_all_html_input(trim($data['szMeterNumber']))); 
        } 
        //$this->set_iServiceType(sanitize_all_html_input(trim($data['iServiceType']))); 
         
        if((int)$_SESSION['admin_id']>0)
        {
            $this->set_idForwarder(sanitize_all_html_input(trim($data['idForwarder']))); 
        } 
        
        //exit if error exist.
        if ($this->error === true)
        {
            return false;
        }

        $this->szAccountNumber=encrypt($this->szAccountNumber,ENCRYPT_KEY);
        $this->szUsername=encrypt($this->szUsername,ENCRYPT_KEY);
        $this->szMeterNumber=encrypt($this->szMeterNumber,ENCRYPT_KEY);
        $this->szPassword=encrypt($this->szPassword,ENCRYPT_KEY);

        if((int)$_SESSION['forwarder_id']>0)
        {
            $idForwarder= $_SESSION['forwarder_id'];
        }
        else
        {
            $idForwarder=$this->idForwarder; 
        }
        if($this->isAccountAlreadyExists($this->szAccountNumber,$this->idCourierProvider,$idForwarder,(int)$data['idEdit']))
        {
            $this->addError('szSpecialErrorMesage', "This account number has already been added");
            return false;
        }
        $query_update = '';
        if(!$bForwarderFlag)
        {
            $query_update = " ,szCarrierAccountID = '".mysql_escape_custom($this->szCarrierAccountID)."' ";
        } 
            
        if((int)$data['idEdit']>0)
        {
            $courierAgreementAry = array();
            if($bForwarderFlag)
            {
                $courierAgreementAry = $this->loadCourierAgreementDetails($data['idEdit']);
            }
            
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_COUIER_AGREEMENT_DATA__."
                SET
                    idCourierProvider='".(int)$this->idCourierProvider."',	
                    szAccountNumber='".mysql_escape_custom($this->szAccountNumber)."',
                    szUsername='".mysql_escape_custom($this->szUsername)."',
                    szPassword='".mysql_escape_custom($this->szPassword)."',
                    szMeterNumber='".mysql_escape_custom($this->szMeterNumber)."',
                    iServiceType = '".mysql_escape_custom($this->iServiceType)."' 
                    $query_update
                WHERE
                    id='".(int)$data['idEdit']."'	
            ";
        } else { 
            $query="
                INSERT INTO
                    ".__DBC_SCHEMATA_COUIER_AGREEMENT_DATA__."
                (
                    idForwarder,
                    idCourierProvider,
                    szAccountNumber,
                    szUsername,
                    szPassword,
                    dtCreated,
                    szMeterNumber,
                    iServiceType,
                    szCarrierAccountID
                )
                VALUES
                (
                    '".(int)$idForwarder."',
                    '".(int)$this->idCourierProvider."',
                    '".mysql_escape_custom($this->szAccountNumber)."',
                    '".mysql_escape_custom($this->szUsername)."',
                    '".mysql_escape_custom($this->szPassword)."',
                    NOW(),
                    '".mysql_escape_custom($this->szMeterNumber)."',
                    '".mysql_escape_custom($this->iServiceType)."',
                    '".mysql_escape_custom($this->szCarrierAccountID)."' 
                )
            ";
        }
//    echo $query;
//        die;
        if($result = $this->exeSQL( $query ))
        {
            $iSendAgreementEmail = false;
            if((int)$data['idEdit']>0)
            {
                $this->idCourierProviderAgree=(int)$data['idEdit'];  
                if($bForwarderFlag)
                {
                    $bValueChanged = false;
                    if($courierAgreementAry['szAccountNumber'] != $this->szAccountNumber)
                    {
                        $bValueChanged = true;
                    }
                    else if($courierAgreementAry['szUsername'] != $this->szUsername)
                    {
                        $bValueChanged = true;
                    }
                    else if($courierAgreementAry['szPassword'] != $this->szPassword)
                    {
                        $bValueChanged = true;
                    } 
                    if($bValueChanged)
                    {
                        /*
                        * If Courier agreement was updated by forwarder then we send a notification email to Management
                        */
                        $iSendAgreementEmail = true;
                    } 
                }
            }
            else
            {
                $id = $this->iLastInsertID ;
                $this->idCourierProviderAgree=$id; 
                if($bForwarderFlag)
                {
                    /*
                    * If Courier agreement was added from forwarder section then we send a notification email to Management
                    */
                    $iSendAgreementEmail = true;
                }
            }	
            
            $kWebservice = new cWebServices();
            if($kWebservice->checkCourierApiCredentials($data,$bForwarderFlag))
            {
                $data['id']=$this->idCourierProviderAgree;
                //Everything is alright  
                $this->isCourierAgreementDataComplete($data);
            }
            else
            {
                //Error From API
                $szAgrrementStatus = 'Authentication Failed';
                $this->updateAgreementStatus($szAgrrementStatus,$this->idCourierProviderAgree);
            }  
            if($iSendAgreementEmail)
            {
                $idForwarder = $_SESSION['forwarder_id'] ;
                $kForwarder = new cForwarder();
                $kForwarder->load($idForwarder);  
                $szForwarderDisplayName = $kForwarder->szDisplayName;
                $idForwarderCountry = $kForwarder->idCountry;
                
                $kConfig = new cConfig();
                $kConfig->loadCountry($idForwarderCountry);
                $szForwarderCountry = $kConfig->szCountryName;
                
                if(empty($this->szMeterNumber))
                {
                    $this->szMeterNumber = "N/A";
                }
                
                $return_ary = array(); 
                $replace_ary['szCourierCompany'] = $szCourierCompany; 
                $replace_ary['szCourierAgreementUrl'] = __MANAGEMENT_OPRERATION_COUIER_AGREEMENT__;
                $replace_ary['szEaspostUrl'] = "https://www.easypost.com/account/carriers";
                $replace_ary['szAccountNumber'] = $this->szAccountNumber; 
                $replace_ary['szUsername'] = $this->szUsername;
                $replace_ary['szPassword'] = $this->szPassword;
                $replace_ary['szMeterNumber'] = $this->szMeterNumber;
                $replace_ary['szForwarderDisplayName'] = $szForwarderDisplayName;
                $replace_ary['szForwarderCountry'] = $szForwarderCountry;  
                $replace_ary['supportEmail'] = __STORE_SUPPORT_EMAIL__; 
                createEmail(__CREATE_NEW_EASYPOST_AGREEMENT__, $replace_ary,__STORE_SUPPORT_EMAIL__, '', __STORE_SUPPORT_EMAIL__,'', __STORE_SUPPORT_EMAIL__,__FLAG_FOR_MANAGEMENT__); 
            }
            return true;		
        }
        else
        {
            return false;
        } 
    }
    
    											
    function isAccountAlreadyExists( $szAccountNumber,$idCourierProvider,$idForwarder,$id=0 )
    {
        $query="
            SELECT
                id
            FROM
                ".__DBC_SCHEMATA_COUIER_AGREEMENT_DATA__."
            WHERE
                szAccountNumber = '".mysql_escape_custom($szAccountNumber)."'	
            AND
                idCourierProvider = '".mysql_escape_custom($idCourierProvider)."'
            AND
                idForwarder = '".mysql_escape_custom($idForwarder)."' 
            AND
                isDeleted='0'
        ";
        if($id>0)
        {
            $query .="
                AND
                    id<>'".(int)$id."'		
            ";
        }
        //echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            // Check if the user exists
            /*
            * One can't add more than 200 courier agreement for the same account
            */
            if( $this->getRowCnt() > 200 )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
     
    function loadCourierServiceProvider($idCourierProider)
    {
        if($idCourierProider>0)
        { 
            $query="
                SELECT
                    id,
                    szProviderName,
                    szName,
                    fPrice,
                    iBookingPriceCurrency,
                    isDeleted,
                    dtCreated
                FROM
                    ".__DBC_SCHEMATA_COUIER_PROVIDER__."
                WHERE 
                    isDeleted = '0'
                AND
                    id = '".(int)$idCourierProider."'
            "; 
            //echo $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            { 
                if($this->iNumRows>0)
                { 
                    $row = $this->getAssoc($result);  
                    return $row ;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    function getAgreementPendingReviewCount()
    { 
         $query="
            SELECT
                count(ced.id) as iNumPendingReview
            FROM
                ".__DBC_SCHEMATA_COUIER_AGREEMENT_DATA__." AS ced 
            WHERE 
                ced.isDeleted='0'   
            AND
                szManagementStatus = '".  mysql_escape_custom(__AGREEMENT_PENDING_REVIEW__)."'
        "; 
        //echo $query;
        if(($result = $this->exeSQL($query)))
        {
            if( $this->getRowCnt() > 0 )
            {
                $ret_ary = array(); 
                $ret_ary = $this->getAssoc($result); 
                return $ret_ary['iNumPendingReview'];
            }
            else
            {
                return array();
            }
        }
        else
        {
            return array();
        } 
    }
    function loadCourierAgreementDetails($idCourierAgreement)
    {
        if($idCourierAgreement>0)
        { 
            $query="
                SELECT
                    ced.id,
                    ced.idForwarder,
                    ced.idCourierProvider,
                    ced.szAccountNumber,
                    ced.szUsername,
                    ced.szPassword,
                    ced.dtCreated,
                    ced.szStatus, 
                    ced.szMeterNumber,
                    ced.iBookingIncluded,
                    ced.fMarkupPercent,
                    ced.fMinimumMarkup,
                    ced.fMarkupperShipment,
                    ced.isDeleted
                FROM
                    ".__DBC_SCHEMATA_COUIER_AGREEMENT_DATA__." AS ced 
                WHERE 
                    ced.isDeleted='0'  
                AND
                    ced.id = '".$idCourierAgreement."'
            ";
            //echo $query;
            if(($result = $this->exeSQL($query)))
            {
                if( $this->getRowCnt() > 0 )
                {
                    $ret_ary = array(); 
                    $ret_ary = $this->getAssoc($result); 
                    return $ret_ary;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                return array();
            } 
        }
    }
    function getCourierAgreementData($id=0,$idCourierProvider=0,$idForwarder=0,$flag=false)
    {
    	$queryWhere='';
    	if((int)$id>0)
    	{
            $queryWhereId = "
                AND
                    ced.id='".(int)$id."' 
            ";
    		
    	}
        if((int)$idCourierProvider>0)
    	{
            $queryWhereId .= " AND ced.idCourierProvider='".(int)$idCourierProvider."' "; 
    	} 
    	if((int)$_SESSION['forwarder_id']>0)
    	{
    		$queryWhereId .= "
                    AND
    			ced.idForwarder='".(int)$_SESSION['forwarder_id']."'
    		";
    	}
    	
    	if((int)$_SESSION['admin_id']>0)
        {
            if((int)$idForwarder>0)
            {
                    $queryWhereId .="
                            AND
                                    ced.idForwarder='".(int)$idForwarder."'
                    ";
            }
    		
    	}  
        $queryFlag='';
        if(!$flag)
        {
            $queryFlag="
                cp.isDeleted='0'
            AND
                ced.isDeleted='0'";
        }
        else
        {
             $queryWhere="
                    ced.id='".(int)$id."'
            ";
        }
        
    	$query="
            SELECT
                ced.id,
                ced.idForwarder,
                ced.idCourierProvider,
                ced.szAccountNumber,
                ced.szUsername,
                ced.szPassword,
                ced.dtCreated,
                ced.szStatus,
                ced.szManagementStatus,
                ced.iServiceType,
                ced.iBookingIncluded,
                ced.fMarkupPercent, 
                ced.fMinimumMarkup,
                ced.fMarkupperShipment,
                cp.szName,
                f.szForwarderAlias,
                f.szDisplayName,
                ced.szMeterNumber,
                szCarrierAccountID,
                cp.id AS idCourierProvider
            FROM
                ".__DBC_SCHEMATA_COUIER_AGREEMENT_DATA__." AS ced
            INNER JOIN
                ".__DBC_SCHEMATA_COUIER_PROVIDER__." AS cp
            ON	
                cp.id=ced.idCourierProvider
            INNER JOIN
                ".__DBC_SCHEMATA_FROWARDER__." f
            ON
                f.id=ced.idForwarder		
            WHERE 
                $queryFlag 
                $queryWhere	
                $queryWhereId
            ORDER BY
                f.szForwarderAlias ASC,cp.szName ASC	
    	";
//        $filename = __APP_PATH_ROOT__."/logs/debug_courier.log";
//				
//        $strdata=array(); 
//        $strdata[0] = "\n\n Query: ".$query."\n\n"; 
//        file_log(true, $strdata, $filename);
        
    	//echo $query;
    	if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
                $ret_ary = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    $ret_ary[$ctr] = $row;
                    $totalBooking=0;
                    $totalBookingArr=$this->gettotalBookingForProivder($row['id']);
                    $ret_ary[$ctr]['totalBooking']=$totalBookingArr['iNumBookingReceived'];
                    $ret_ary[$ctr]['totalSales']=$totalBookingArr['fTotalPriceUSD'];
                    $ctr++;
                }
                return $ret_ary;
            }
            else
            {
            	return array();
            }
        }
        else
        {
            return array();
        } 
    }
    
    function deleteCourierProviderAgreementData($id)
    {
    	if((int)$id>0)
    	{
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_COUIER_AGREEMENT_DATA__."
                SET
                    isDeleted='1'
                WHERE
                    id='".(int)$id."'		
            ";
            if($result = $this->exeSQL( $query ))
            {
                $this->deleteServiceOffered($id);
                $this->deleteTradesOffered($id);
                return true;
            }
            else
            {
                return false;
            }
    	} 
    }
    function deleteServiceOffered($idCourierAgreement)
    {
    	if((int)$idCourierAgreement>0)
    	{
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_COUIER_SERVICES_OFFERED_DATA__."
                SET
                    isDeleted='1'
                WHERE
                    idCourierAgreement = '".(int)$idCourierAgreement."'		
            ";
            if($result = $this->exeSQL( $query ))
            { 
                return true;
            }
            else
            {
                return false;
            }
    	} 
    }
    
    function deleteTradesOffered($idCourierAgreement)
    {
    	if((int)$idCourierAgreement>0)
    	{
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_COUIER_TRADES_OFFERED_DATA__."
                SET
                    isDeleted = '1'
                WHERE
                    idCourierAgreement = '".(int)$idCourierAgreement."'		
            "; 
            if($result = $this->exeSQL( $query ))
            { 
                return true;
            }
            else
            {
                return false;
            }
    	} 
    }
    
    
    function getCourierActiveProviderProduct($idCourierProvider)
    { 
    	$query="
    		SELECT
    			cpp.id,
    			cpp.szName
			FROM
				".__DBC_SCHEMATA_COUIER_PROVIDER_PRODUCTS__." AS cpp
			INNER JOIN
				".__DBC_SCHEMATA_COUIER_PROVIDER__." AS cp
			ON	
				cp.id=cpp.idCourierProvider
			WHERE
				cp.isDeleted='0'
			AND
				cpp.isDeleted='0'
			AND
				cpp.idCourierProvider='".(int)$idCourierProvider."'		
    	";
    	if( ( $result = $this->exeSQL( $query ) ) )
		{
            if( $this->getRowCnt() > 0 )
            {
            		$ret_ary = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {
                        $ret_ary[$ctr] = $row;
                        $ctr++;
                    }
                    return $ret_ary;
            }
            else
            {
            	return array();
            }
		}
		else
		{
			return array();
		}
    
    }
    
    function addCourierAgreementServiceData($data)
    {
    	$this->set_idProviderProduct(sanitize_all_html_input(trim($data['idProviderProduct'])));
        $this->set_szDisplayName(sanitize_all_html_input(trim($data['szDisplayName'])));
		
    	//exit if error exist.
        if ($this->error === true)
        {
            return false;
        }
        if((int)$_SESSION['forwarder_id']>0)
        {
            $idForwarder=$_SESSION['forwarder_id'];
        }
        else
        {
            $courierAgreeDataArr=$this->getCourierAgreementData($data['idCourierAgreeProvider']);
            $idForwarder=$courierAgreeDataArr[0]['idForwarder'];
        } 
        if((int)$data['id']>0)
        {
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_COUIER_SERVICES_OFFERED_DATA__."
                SET
                    szDisplayName='".mysql_escape_custom($this->szDisplayName)."',
                    idCourierProviderProductid='".(int)$this->idProviderProduct."',
                    idForwarder='".(int)$idForwarder."'
                WHERE
                    id='".(int)$data['id']."'
            ";
        }
        else
        {
            $query="
                INSERT INTO
                    ".__DBC_SCHEMATA_COUIER_SERVICES_OFFERED_DATA__."
                (
                    idCourierProviderProductid,
                    idCourierAgreement,
                    szDisplayName,
                    dtCreated,
                    idForwarder
                )
                VALUES
                (
                    '".(int)$this->idProviderProduct."',
                    '".(int)$data['idCourierAgreeProvider']."',
                    '".mysql_escape_custom($this->szDisplayName)."',
                    NOW(),
                    '".(int)$idForwarder."' 
                )
            ";
        }
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            $idCourierAgreeProvider = $data['idCourierAgreeProvider']; 
            $courierAgreementArr=$this->getCourierAgreementData($idCourierAgreeProvider);  
            
            $courierAgreementArrs = array();
            $courierAgreementArrs = $courierAgreementArr[0];
            $this->isCourierAgreementDataComplete($courierAgreementArrs);
            
            return true;
        }
        else
        {
            return false;
        }
    } 
    function getCourierAgreeServiceData($idCourierAgreement,$id=0)
    {
    	$queryWhere='';
    	if((int)$id>0)
    	{
            $queryWhere="
                AND
                    csod.id='".(int)$id."'
                "; 
    	}
    
    	$query="
            SELECT
                csod.id,
                cpp.szName,
                csod.idCourierProviderProductid AS idProviderProduct,
                csod.idCourierAgreement,
                csod.szDisplayName,
                csod.dtCreated,
                csod.iBookingIncluded,
                csod.fMarkupPercent,
                csod.fMinimumMarkup,
                csod.fMarkupperShipment,
                csod.idForwarder
                FROM
                    ".__DBC_SCHEMATA_COUIER_PROVIDER_PRODUCTS__." AS cpp
                INNER JOIN
                    ".__DBC_SCHEMATA_COUIER_SERVICES_OFFERED_DATA__." AS csod
                ON	
                    cpp.id=csod.idCourierProviderProductid
                WHERE
                    cpp.isDeleted='0'
                AND
                    csod.isDeleted='0'
                AND
                    csod.idCourierAgreement='".(int)$idCourierAgreement."'
                    $queryWhere			
    	";
    	//echo $query;
    	if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
                $ret_ary = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    $ret_ary[$ctr] = $row;
                    $ctr++;
                }
                return $ret_ary;
            }
            else
            {
            	return array();
            }
        }
        else
        {
            return array();
        } 
    }
    
    
    function deleteServiceData($id)
    {
    	$query="
    		UPDATE
    			".__DBC_SCHEMATA_COUIER_SERVICES_OFFERED_DATA__."
    		SET
    			isDeleted='1'
    		WHERE
    			id='".(int)$id."'	
    	";
    	$result = $this->exeSQL( $query );
    
    }
    
    
    function addCourierAgreementTradeOffered($data)
    {
    	$this->set_idCountryFrom(sanitize_all_html_input(trim($data['idCountryFrom'])));
        $this->set_iTrade(sanitize_all_html_input(trim($data['iTrade'])));
        
        
        if ($this->error === true)
        {
            return false;
        }
        
		
		
    	//exit if error exist.
        
		
    	if((int)$_SESSION['forwarder_id']>0)
        {
                $idForwarder=$_SESSION['forwarder_id'];
        }
        else
        {
                $courierAgreeDataArr=$this->getCourierAgreementData($data['idCourierAgreeProvider']);
                $idForwarder=$courierAgreeDataArr[0]['idForwarder'];
        }

        if((int)$data['id']>0)
        {
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_COUIER_TRADES_OFFERED_DATA__."
                SET
                    idCountryFrom='".mysql_escape_custom($this->idCountryFrom)."',
                    iTrade='".(int)$this->iTrade."',
                    idForwarder='".(int)$idForwarder."'
                WHERE
                    id='".(int)$data['id']."'
            ";
        }
        else
        {
            $query="
                INSERT INTO
                    ".__DBC_SCHEMATA_COUIER_TRADES_OFFERED_DATA__."
                (
                    idCountryFrom,
                    idCourierAgreement,
                    iTrade,
                    idForwarder
                )
                VALUES
                (
                    '".(int)$this->idCountryFrom."',
                    '".(int)$data['idCourierAgreeProvider']."',
                    '".mysql_escape_custom($this->iTrade)."',
                    '".(int)$idForwarder."'
                )
            ";
        }
        //echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            $idCourierAgreeProvider = $data['idCourierAgreeProvider']; 
            $courierAgreementArr=$this->getCourierAgreementData($idCourierAgreeProvider);  
            
            $courierAgreementArrs = array();
            $courierAgreementArrs = $courierAgreementArr[0];
            $this->isCourierAgreementDataComplete($courierAgreementArrs);
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function getCourierAgreeTradeData($idCourierAgreement,$id=0)
    {
    	$queryWhere='';
    	if((int)$id>0)
    	{
    		$queryWhere="
    			AND
    				cto.id='".(int)$id."'
    		";
    		
    	}
    
    	$query="
    		SELECT
    			cto.id,
    			cto.idCountryFrom,
				cto.idCourierAgreement,
				cto.iTrade,
				c.szCountryName
			FROM
				".__DBC_SCHEMATA_COUIER_TRADES_OFFERED_DATA__." AS cto
			INNER JOIN
				".__DBC_SCHEMATA_COUNTRY__." AS c
			ON	
				c.id=cto.idCountryFrom
			WHERE
				cto.isDeleted='0'
			AND
				cto.idCourierAgreement='".(int)$idCourierAgreement."'
			$queryWhere	
		ORDER BY
			c.szCountryName ASC			
    	";
    	//echo $query;
    	if( ( $result = $this->exeSQL( $query ) ) )
		{
            if( $this->getRowCnt() > 0 )
            {
            		$ret_ary = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {
                        $ret_ary[$ctr] = $row;
                        if($ret_ary[$ctr]['iTrade']=='1')
                        	$iTrade="Import";
                        else if($ret_ary[$ctr]['iTrade']=='2')
                        	$iTrade="Export";
                        else if($ret_ary[$ctr]['iTrade']=='3')
                        	$iTrade="Domestic";
                        	
                         $ret_ary[$ctr]['iTradeValue'] = $iTrade;		
                        $ctr++;
                    }
                    return $ret_ary;
            }
            else
            {
            	return array();
            }
		}
		else
		{
			return array();
		}
    
    }
    
 	function deleteTradeOfferedData($id)
    {
    	$query="
    		UPDATE
    			".__DBC_SCHEMATA_COUIER_TRADES_OFFERED_DATA__."
    		SET
    			isDeleted='1'
    		WHERE
    			id='".(int)$id."'	
    	";
    	$result = $this->exeSQL( $query );
    
    }
    
    
    function updateCourierAgreementPricingData($data)
    {
    	$this->set_iBookingIncluded(sanitize_all_html_input(trim($data['iBookingIncluded'])));
        $this->set_fMarkupPercent(sanitize_all_html_input(trim($data['fMarkupPercent'])));
        $this->set_fMinimumMarkup(sanitize_all_html_input(trim($data['fMinimumMarkup'])));
        $this->set_fMarkupperShipment(sanitize_all_html_input(trim($data['fMarkupperShipment'])));
		 
        //exit if error exist.
        if ($this->error === true)
        {
            return false;
        }  
        $query="
            UPDATE
                ".__DBC_SCHEMATA_COUIER_SERVICES_OFFERED_DATA__."
            SET
                iBookingIncluded='".(int)$this->iBookingIncluded."',
                fMarkupPercent='".(float)$this->fMarkupPercent."',
                fMinimumMarkup='".(float)$this->fMinimumMarkup."',
                fMarkupperShipment='".(float)$this->fMarkupperShipment."',
                iServiceAvailable = '1'
            WHERE
                id='".(int)$data['idCourierServiceOffered']."'
        ";
//        echo $query."<br>";
//        die;
        if($result = $this->exeSQL( $query ))
        {
            return true;		
        }
        else
        {
            return false;
        }
    }
    
    function getAllCourierAgreement($data,$provider_Product_flag=false,$provider_forwarder_flag=false)
    {  
        if($provider_Product_flag)
        {
            //Load All Unique forwarder who provides courier services 
            $query_and = " AND ced.idCourierProvider = '".(int)$data['idCourierProvider']."' ";
            $query_group_by = " GROUP BY ced.idForwarder ";
            $order_by = " ORDER BY f.szDisplayName ASC, szForwarderAlias ASC ";
        }
        else if($provider_forwarder_flag)
        {
            $query_and = "AND szManagementStatus='Working'  AND ced.idForwarder = '".(int)$data['idForwarder']."' ";
            //$query_group_by = " GROUP BY ced.idForwarder ";
            $order_by = " ORDER BY f.szDisplayName ASC, szForwarderAlias ASC ";
        }
        else
        {
            $query_and = " AND ced.idForwarder = '".(int)$data['idForwarder']."' AND ced.id = '".(int)$data['idCourierAgreement']."' ";
        }
        
    	$query="
            SELECT
                ced.id,
                ced.id as idCourierAgreement,
                ced.idForwarder,
                ced.idCourierProvider,
                ced.szAccountNumber,
                ced.szUsername,
                ced.szPassword,
                ced.dtCreated, 
                ced.szMeterNumber,
                f.szCurrency As szForworderCurrency,
                f.iProfitType, 
                f.szForwarderAlias, 
                f.szCompanyName as szForwarderCompanyName,
                f.szDisplayName as szForwarderDisplayName,
                f.szCurrency as idForwarderCurrency,
                cp.szName AS szCourierProviderName
            FROM
                ".__DBC_SCHEMATA_COUIER_AGREEMENT_DATA__." AS ced 
            INNER JOIN
                ".__DBC_SCHEMATA_FROWARDER__." f
            ON
                f.id = ced.idForwarder
            INNER JOIN
                ".__DBC_SCHEMATA_COUIER_PROVIDER__." cp
            ON
                cp.id = ced.idCourierProvider
            WHERE 
                ced.isDeleted = '0'
            AND
                f.iActive = '1'
            AND
                f.isOnline = '1'    
            $query_and
            $query_group_by
    	";
    	//echo $query."<br/><br/>";
    	if( ( $result = $this->exeSQL( $query ) ) )
        {
            if($this->getRowCnt()>0)
            {
                $ret_ary = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    $ret_ary[$ctr] = $row;
                    $ctr++;
                }
                return $ret_ary;
            }
            else
            {
            	return array();
            }
        }
        else
        {
            return array();
        } 
    }
    
    function getCourierServices($data,$flag=true,$packingTypeFlag=false,$packingAry=array(),$mode=false)
    { 
        if($flag)
        {
            if($data['idCourierProviderProductid']>0)
            {
                $query_and = " AND csod.idCourierProviderProductid = '".(int)$data['idCourierProviderProductid']."' ";
            }
        }
        
        if($packingTypeFlag)
        {
            if(cPartner::$bApiValidation)
            {
                if(!empty(cPartner::$searchablePacketTypes))
                {
                    $packingAry = cPartner::$searchablePacketTypes;
                }
            } 
        } 
        
        if(!empty($packingAry))
        {
            $packingStr = implode(",",$packingAry);
            $query_and .= " AND products.idPacking IN (".$packingStr.") ";
        } 
        
        /*
        *  Exempted try-it-out modes
        */ 
        $excemptedModesAry = array('FORWARDER_COURIER_TRY_IT_OUT','FORWARDER_TRY_IT_OUT');
        if(!empty($mode) && in_array($mode,$excemptedModesAry))
        {
            //For exempted modes we display services even if forwarder is not online
        }
        else
        {
            $query_and .= " AND f.isOnline='1' ";
        }
        
    	$query="
            SELECT
                ced.id,
                ced.id as idCourierAgreement,
                ced.idForwarder,
                ced.idCourierProvider,
                ced.szAccountNumber,
                ced.szUsername,
                ced.szPassword,
                csod.iBookingIncluded,
                csod.dtCreated,
                csod.fMarkupPercent,
                csod.fMinimumMarkup,
                csod.fMarkupperShipment,
                cp.szName, 
                cp.fPrice,
                cp.iBookingPriceCurrency,
                csod.szDisplayName,
                csod.idCourierProviderProductid,
                ced.szMeterNumber,
                f.szCurrency As szForworderCurrency,
                f.iProfitType, 
                f.szForwarderAlias, 
                f.szCompanyName as szForwarderCompanyName,
                f.szCurrency as idForwarderCurrency,
                products.szName as szCourierProductName,
                products.szApiCode as szCourierAPICode,
                products.idPacking as idCourierPacking,
                products.iDaysStd,
                f.idCountry AS idForwarderCountry,
                products.idGroup,
                cpg.szName AS szGroupName,
                c.szCurrency AS szCurrencyName,
                szCarrierAccountID
            FROM
                ".__DBC_SCHEMATA_COUIER_AGREEMENT_DATA__." AS ced
            INNER JOIN
                ".__DBC_SCHEMATA_COUIER_PROVIDER__." AS cp
            ON	
                cp.id=ced.idCourierProvider
            INNER JOIN
                ".__DBC_SCHEMATA_FROWARDER__." f
            ON
                f.id=ced.idForwarder
            INNER JOIN
                ".__DBC_SCHEMATA_COUIER_SERVICES_OFFERED_DATA__." AS csod
            ON
                csod.idCourierAgreement=ced.id	
            INNER JOIN
                ".__DBC_SCHEMATA_COUIER_PROVIDER_PRODUCTS__." products
            ON
                products.id = csod.idCourierProviderProductid
            INNER JOIN
                ".__DBC_SCHEMATA_COURIER_PRODUCT_GROUP__." AS cpg
            ON
                cpg.id = products.idGroup
            INNER JOIN
                ".__DBC_SCHEMATA_CURRENCY__." AS c
            ON
                f.szCurrency=c.id
            WHERE
                cp.isDeleted='0'
            AND
                ced.isDeleted='0'
            AND
                f.iActive='1' 
            AND
                csod.isDeleted='0'
            AND
                csod.iServiceAvailable = '1'
            AND
                ced.idForwarder='".(int)$data['idForwarder']."'
            AND
                ced.id='".(int)$data['idCourierAgreement']."'
            AND
                products.isDeleted='0'
                $query_and
    	";
    	//echo "<br/><br/>".$query."<br/><br/>"; 
    	if( ( $result = $this->exeSQL( $query ) ) )
        {
            if($this->getRowCnt()>0)
            {
                $ret_ary = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    if($flag)
                    {
                        $ret_ary[$ctr] = $row;
                    }
                    else
                    {
                        $counter=count($ret_ary[$row['idGroup']]);
                        $ret_ary[$row['idGroup']][$counter] = $row;
                    }
                    $ctr++;
                }
                return $ret_ary;
            }
            else
            {
            	return array();
            }
        }
        else
        {
                return array();
        } 
    } 
    
    function getProductProviderLimitations($idGroup,$flag=false,$key_flag=false)
    {
    	$queryWhere='';
    	if($flag)
    	{
            $queryWhere="
                AND
                (		
                    idCargoLimitationType='".__TOTAL_WEIGHT_FOR_SHIPPMENT__."'
                OR
                    idCargoLimitationType='".__TOTAL_VALUME_FOR_SHIPPMENT__."'
                OR
                    idCargoLimitationType='".__CHARGEABLE_WEIGHT_PER_COLI__."'
                OR
                    idCargoLimitationType='".__WEIGHT_PER_COLI__."'
                 OR
                    idCargoLimitationType='".__NUMBER_OF_COLLI__."'       
                )	    		
            "; 
    	}
        $query="
           SELECT
               id,
               szRestriction,
               szLimit,
               idCargoLimitationType
           FROM
               ".__DBC_SCHEMATA_COUIER_CARGO_LIMITATION_DATA__."
           WHERE
               idGroup='".(int)$idGroup."'
           AND
               isDeleted='0'
           $queryWhere	
       ";
        //echo $query;
        if(($result = $this->exeSQL($query)))
        {
            if( $this->getRowCnt() > 0 )
            { 
                $ret_ary = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    if($key_flag)
                    {
                        $ret_ary[$row['idCargoLimitationType']][$row['szRestriction']] = $row;
                        $ctr++;
                    }
                    else
                    {
                        $ret_ary[$ctr] = $row;
                        $ctr++;
                    } 
                }
                return $ret_ary;
            }
            else
            {
                return array();
            }
        }
        else
        {
            return array();				
        } 
    }
    
    
    function selectCourierAgreementByCountry($idCountry=false,$flag=false,$idForwarder=false,$bVogaPages=false)
    {
        if($idForwarder>0)
        {
            $query_and = " AND ctod.idForwarder = '".(int)$idForwarder."' ";
        }
        
        if($bVogaPages)
        {
            /*
            * This section is being called from /starndardCargoPricing/
            */
        }
        else
        {
            if((int)$idCountry>0)
            {
                $query_and .= " 
                    AND 
                        ctod.idCountryFrom = '".(int)$idCountry."'
                    AND
                       ctod.iTrade='".(int)$flag."'	";
            }
        }
        
    	$query="
            SELECT 
                ctod.idCourierAgreement,
                ctod.idForwarder,
                ced.id as idCourierAgreement,
                ced.idForwarder,
                ced.idCourierProvider,
                ced.szAccountNumber,
                ced.szUsername,
                ced.szPassword,
                ced.dtCreated, 
                ced.szMeterNumber,
                ced.szCarrierAccountID,
                f.szCurrency As szForworderCurrency,
                f.iProfitType, 
                f.szForwarderAlias, 
                f.szCompanyName as szForwarderCompanyName,
                f.szCurrency as idForwarderCurrency
            FROM	
                ".__DBC_SCHEMATA_COUIER_TRADES_OFFERED_DATA__." AS ctod 
            INNER JOIN
                ".__DBC_SCHEMATA_COUIER_AGREEMENT_DATA__." AS ced 
            ON
                ced.id = ctod.idCourierAgreement
            INNER JOIN
                ".__DBC_SCHEMATA_COUIER_PROVIDER__." AS cp
            ON	
                cp.id = ced.idCourierProvider
            INNER JOIN
                ".__DBC_SCHEMATA_FROWARDER__." f
            ON
                f.id = ctod.idForwarder
            WHERE 
               ctod.isDeleted='0'  
            AND
                ced.szStatus = '".__AGREEMENT_WORKING__."' 
                $query_and
            GROUP BY
                ctod.idCourierAgreement
    	";
        //echo $query;
        if(($result = $this->exeSQL($query)))
        {
            if($this->getRowCnt()>0)
            { 
                $ret_ary = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    $ret_ary[$row['idCourierAgreement']."_".$row['idForwarder']] = $row; 
                }
                return $ret_ary;
            }
            else
            {
                    return array();
            }
        }
        else
        {
            return array();				
        }
    } 
    
    function addShippingChargesNewPayment($data)
    { 
    	$szAccountNumber=decrypt($data['szAccountNumber'],ENCRYPT_KEY);
		$shippingChargesPayment = array(
            'PaymentType' => 'SENDER', // valid values RECIPIENT, SENDER and THIRD_PARTY
            'Payor' => array(
                'ResponsibleParty' => array(
                'AccountNumber' => $szAccountNumber,
                'CountryCode' => $data['szSCountry']
                )
            )
        );
        return $shippingChargesPayment;
    }
    
    function prepareUpsApiRequest($kObject,$flag=false,$test_page=false)
    { 
        //create soap request
        $request = array();
        $option = array();
         
        //$option['RequestAction'] = 'Rate';
        $option['RequestOption'] = 'Shop';
        $request['Request'] = $option;
        $request['CurrencyCode']='USD'; 
        
        $pickuptype['Code'] = '03';
        $pickuptype['Description'] = 'Customer counter';
        $request['PickupType'] = $pickuptype;
         
        $customerclassification['Code'] = '00'; 
        $customerclassification['Description'] = 'Classfication';
        $request['CustomerClassification'] = $customerclassification; 
        if(!$test_page)
        { 
            $t = 0;
            $cargoWeight = $kObject['fCargoWeight'];
            
            $packaging1['Code'] = '02';
            $packaging1['Description'] = 'Package/customer supplied';
            $packageArr[$t]['PackagingType'] = $packaging1;
            $punit1['Code'] = 'KGS';
            $punit1['Description'] = 'Kgs';
            $packageweight1['Weight'] = $cargoWeight;
            $packageweight1['UnitOfMeasurement'] = $punit1;
            $packageArr[$t]['PackageWeight'] = $packageweight1;  
            ++$t;
        }
        else
        {
            $idBooking=$kObject['idBooking'];
            $bDefaultPackageFlag = true;
            $bPartnerPacking = false;
            if(cPartner::$bApiValidation && !empty(cPartner::$searchablePacketTypes) && (in_array(1, cPartner::$searchablePacketTypes) || in_array(2, cPartner::$searchablePacketTypes)))
            {
                $bPartnerPacking = true;
            } 
            $idBooking = 21937; 
            if($idBooking>0 || $bPartnerPacking)
            {
                $kBooking =new cBooking();
                if($bPartnerPacking)
                {
                    $kPartner = new cPartner();
                    $bookingCargoAry = $kPartner->getCargoDeailsByPartnerApi(); 
                }
                else
                {
                    $bookingCargoAry = $kBooking->getCargoDeailsByBookingId($idBooking,true); 
                    $postSearchAry = $kBooking->getBookingDetails($idBooking);
                } 
                $postSearchAry['iCourierBooking'] = 1;
               
                $kWHSSearch = new cWHSSearch();
                if(!empty($bookingCargoAry) && ($bPartnerPacking || $postSearchAry['iCourierBooking']==1 || $postSearchAry['iSearchMiniVersion']==3 || $postSearchAry['iSearchMiniVersion']==4))
                {  
                    $ctr=1;
                    $iQuantity = 0;
                    $bDefaultPackageFlag = false;
                    foreach($bookingCargoAry as $bookingCargoArys)
                    { 
                        if($postSearchAry['iPalletType']>0 && (float)$bookingCargoArys['fHeight']<=0)
                        {
                            /*
                            * If shipment type is pallet and height is empty then we use standard height as per pallet type
                            */
                            if($postSearchAry['iPalletType']==1) //Euro Pallet
                            {
                                $bookingCargoArys['fHeight'] = __STANDARD_HEIGHT_EURO_PALLET__;
                            }
                            else if($postSearchAry['iPalletType']==2) //Half Pallet
                            {
                                $bookingCargoArys['fHeight'] = __STANDARD_HEIGHT_HALF_PALLET__;
                            }
                        }
                        
                        if($bookingCargoArys['idWeightMeasure']==1)
                        {
                            $fKgFactor = 1;
                        }
                        else
                        { 
                            $fKgFactor = $kWHSSearch->getWeightFactor($bookingCargoArys['idWeightMeasure']); 
                        }

                        if($bookingCargoArys['idCargoMeasure']==1)
                        {
                            $fCmFactor = 1;
                        }
                        else
                        {
                            $fCmFactor = $kWHSSearch->getCargoFactor($bookingCargoArys['idCargoMeasure']);  
                        }

                        $iLenght = 0;
                        $iWidth = 0;
                        $iHeight = 0;
                        $fWeight = 0;

                        if($fCmFactor>0)
                        {
                            $iLenght = round($bookingCargoArys['fLength'] / $fCmFactor);
                            $iWidth = round($bookingCargoArys['fWidth'] / $fCmFactor);
                            $iHeight = round($bookingCargoArys['fHeight'] / $fCmFactor);
                        }

                        if($fKgFactor>0)
                        {
                            $fWeight = round($bookingCargoArys['fWeight']/$fKgFactor) ;
                        }

                        $szCargoMeasureCode = 'CM';
                        $szCargoMeasure = 'Centimeters';

                        $szWeightMeasureCode = 'KGS';
                        $szWeightMeasure = 'Kgs';

                        $packageQuantity = $bookingCargoArys['iQuantity'];
                        $iTotalQuantity += $bookingCargoArys['iQuantity'];

                        for($i=0;$i<$packageQuantity;++$i)
                        {
                            $packaging1['Code'] = '02';
                            $packaging1['Description'] = 'Package/customer supplied';
                            $package1['PackagingType'] = $packaging1;
                            $dunit1['Code'] = $szCargoMeasureCode;
                            $dunit1['Description'] = $szCargoMeasure;
                            $dimensions1['Length'] = $iLenght;
                            $dimensions1['Width'] = $iWidth;
                            $dimensions1['Height'] = $iHeight;
                            $dimensions1['UnitOfMeasurement'] = $dunit1;
                            $package1['Dimensions'] = $dimensions1;
                            $punit1['Code'] = $szWeightMeasureCode;
                            $punit1['Description'] = $szWeightMeasure;
                            $packageweight1['Weight'] = $fWeight;
                            $packageweight1['UnitOfMeasurement'] = $punit1;
                            $package1['PackageWeight'] = $packageweight1;  
                            $packageArr[]=$package1;
                        }
                        $szPackageDetailsLogs .= "<br> Package: ".$bookingCargoArys['fLength']." X ".$bookingCargoArys['fWidth']." X ".$bookingCargoArys['fHeight']." ".$szCargoMeasure. ", ".$bookingCargoArys['fWeight']." ".$szWeightMeasure.", Count: ".$bookingCargoArys['iQuantity'] ;
                    } 
                    $szPackageDetailsLogs .= "<br><br> Total package: ".$iTotalQuantity;
                }
            }
            
            if($bDefaultPackageFlag)
            { 
                $productProviderArr=$this->getProductProviderLimitations($kObject['idGroup'],true);  
                $bWeightLimitationDefined = false; 
                if(!empty($productProviderArr))
                {
                    foreach($productProviderArr as $productProviderArrs)
                    {
                        if($productProviderArrs['idCargoLimitationType']==__WEIGHT_PER_COLI__)
                        {
                            if($productProviderArrs['szRestriction']=='Maximum')
                            {
                                $maxWeightPerColli = $productProviderArrs['szLimit']; 
                                $bWeightLimitationDefined = true;
                            }  
                        } 
                        if($productProviderArrs['idCargoLimitationType']==__CHARGEABLE_WEIGHT_PER_COLI__)
                        {
                            if($productProviderArrs['szRestriction']=='Maximum')
                            {
                                $maxChargeableWeight = $productProviderArrs['szLimit']; 
                                $bWeightLimitationDefined = true;
                            } 
                        }
                    }
                } 
                $changeFlag=false; 
                $fCargoWeight = $kObject['fCargoWeight'];
                $fCargoVolume = $kObject['fCargoVolume']; 
                $cargeAbleFlag=false;

                $fRmaingChargeableWeight = 0;
                $fRmaingWeight = 0;

                $fCargoActualWeight = $kObject['fCargoWeight'];

                if(($fCargoVolume*200)>$fCargoWeight)
                {
                    $cargeAbleFlag=true;
                    $fCargoWeight = $kObject['fCargoVolume']*200 ; 
                }   
                $fCargoChargeableWeight = $fCargoWeight; 
                if($bWeightLimitationDefined)
                { 
                    $changeFlag=true;
                    $iTotalNumActulalColli=1;
                    if($maxWeightPerColli>0.00)
                    {
                        $fRmaingWeight = (float)($fCargoActualWeight % $maxWeightPerColli);
                        $iTotalNumActulalColli = (int)($fCargoActualWeight / $maxWeightPerColli);
                        $t=0;
                        if((int)$fRmaingWeight>0)
                        {
                          $iTotalNumActulalColli = $iTotalNumActulalColli + 1;
                        } 
                    }
                    $iTotalChargeNumColli=1;
                    if($maxChargeableWeight>0.00)
                    {                    
                        $fRmaingChargeableWeight = (float)($fCargoChargeableWeight % $maxChargeableWeight);
                        $iTotalChargeNumColli = (int)($fCargoChargeableWeight / $maxChargeableWeight);
                        $t=0;
                        if((int)$fRmaingChargeableWeight>0)
                        {
                          $iTotalChargeNumColli = $iTotalChargeNumColli + 1;
                        } 
                    }  
                    $fRmaingWeight = 0;  
                    if($iTotalChargeNumColli>=$iTotalNumActulalColli)
                    { 
                        $cargoWeight=$fCargoChargeableWeight/$iTotalChargeNumColli;
                        $iTotalNumColli=$iTotalChargeNumColli;
                        $fRmaingWeight = $fCargoChargeableWeight - ($cargoWeight*$iTotalNumColli);
                    }
                    else if($iTotalChargeNumColli<$iTotalNumActulalColli)
                    {
                        $cargoWeight=$fCargoActualWeight/$iTotalNumActulalColli;
                        $iTotalNumColli=$iTotalNumActulalColli;
                        $fRmaingWeight = $fCargoActualWeight - ($cargoWeight*$iTotalNumColli);
                    }
                    else
                    {
                        $changeFlag=true;
                        $iTotalNumColli = 1;
                        $cargoWeight = $fCargoWeight;
                        $fRmaingWeight = 0;
                    } 
                }
                else
                {
                    $changeFlag=true;
                    $iTotalNumColli = 1;
                    $cargoWeight = $fCargoWeight;
                    $fRmaingWeight = 0;
                }
                $totalPackage = $iTotalNumColli; 

                if($changeFlag)
                { 
                    if((float)$fRmaingWeight>0.00)
                    {
                        $totalMaxPackage = $iTotalNumColli-1;
                    }
                    else
                    {
                        $totalMaxPackage = $iTotalNumColli;
                    } 
                    $t=0;
                    $szPackageDetailsLogs .= "<br> Number of Package: ".$totalMaxPackage.", Weight per package: ".$cargoWeight;
                    if((float)$fRmaingWeight>0.00)
                    {
                        $szPackageDetailsLogs .= "<br> Number of Package: 1, Weight per package: ".$fRmaingWeight;
                        $packaging1['Code'] = '02';
                        $packaging1['Description'] = 'Package/customer supplied';
                        $packageArr[0]['PackagingType'] = $packaging1;
                        $punit1['Code'] = 'KGS';
                        $punit1['Description'] = 'Kgs';
                        $packageweight1['Weight'] = $fRmaingWeight;
                        $packageweight1['UnitOfMeasurement'] = $punit1;
                        $packageArr[0]['PackageWeight'] = $packageweight1;  
                        ++$t;
                    }  
                    for($i=0;$i<$totalMaxPackage;++$i)
                    {
                        $packaging1['Code'] = '02';
                        $packaging1['Description'] = 'Package/customer supplied';
                        $packageArr[$t]['PackagingType'] = $packaging1;
                        $punit1['Code'] = 'KGS';
                        $punit1['Description'] = 'Kgs';
                        $packageweight1['Weight'] = $cargoWeight;
                        $packageweight1['UnitOfMeasurement'] = $punit1;
                        $packageArr[$t]['PackageWeight'] = $packageweight1;  
                        ++$t;
                    } 
                } 
            }  
        }
        
        $this->szPackageDetailsLogs = $szPackageDetailsLogs;
        $this->szPackageDetailsLogs .= "<br><br> <u>Shipper </u><br> ";
        $this->szPackageDetailsLogs .= "Address: ".$kObject['szAddress1'];
        $this->szPackageDetailsLogs .= "<br>Postcode: ".$kObject['szPostCode'];
        $this->szPackageDetailsLogs .= "<br>City: ".$kObject['szCity'];
        $this->szPackageDetailsLogs .= "<br>Country: ".$kObject['szCountry'];
        
        $this->szPackageDetailsLogs .= "<br><br> <u>Consignee </u><br> ";
        $this->szPackageDetailsLogs .= "Address: ".$kObject['szSAddress1'];
        $this->szPackageDetailsLogs .= "<br>Postcode: ".$kObject['szSPostCode'];
        $this->szPackageDetailsLogs .= "<br>City: ".$kObject['szSCity'];
        $this->szPackageDetailsLogs .= "<br>Country: ".$kObject['szSCountry'];
        
        $szAddress = $kObject['szAddress1'] ;
        if(!empty($kObject['szAddress2']))
        {
            $szAddress .= $kObject['szAddress2'] ;
        } 
        
        $shipper['Name'] = $kObject['szFirstName']." ".$kObject['szLastName']; 
        $shipper['ShipperNumber'] = decrypt($kObject['szMeterNumber'],ENCRYPT_KEY);
           
        $address = array();
        $address['AddressLine'] = array( $szAddress ); 
        $address['StateProvinceCode'] = $kObject['szState'];
          
        $address['PostalCode'] = "2100";
        //$address['City'] = "Copenhagen";
        $address['CountryCode'] = "DK";
         
        $shipper['Address'] = $address;
        $shipment['Shipper'] = $shipper;

        $szAddress = $kObject['szSAddress1'] ;
        if(!empty($kObject['szSAddress2']))
        {
            $szAddress .= $kObject['szSAddress2'] ;
        }

        $shipto['Name'] = $kObject['szSFirstName']." ".$kObject['szSLastName'];
        $addressTo = array();
        $addressTo['AddressLine'] = $szAddress;
        //$addressTo['City'] = $kObject['szSCity'];
        //$addressTo['StateProvinceCode'] = $kObject['szSState'];
        $addressTo['PostalCode'] = $kObject['szSPostCode'];
        $addressTo['CountryCode'] = $kObject['szSCountry'];
        //$addressTo['ResidentialAddressIndicator'] = '';
        $shipto['Address'] = $addressTo;
        $shipment['ShipTo'] = $shipto;

        $shipfrom['Name'] = $kObject['szFirstName']." ".$kObject['szLastName'];
        $addressFrom['AddressLine'] = array();
        //$addressFrom['City'] = $kObject['szCity'];
        //$addressFrom['City'] = 'TRENTO' ;
        //$addressFrom['StateProvinceCode'] = $kObject['szState'];
        $addressFrom['PostalCode'] = $kObject['szPostCode'];
        $addressFrom['CountryCode'] = $kObject['szCountry']; 
         
        $shipfrom['Address'] = $addressFrom;
        $shipment['ShipFrom'] = $shipfrom;   
        $shipment['Package'] = $packageArr; 
        $shipment['ShipmentRatingOptions']['NegotiatedRatesIndicator'] = 1;
          
        $shipment['Commodity'] = $CommodityValue;     
            
        $shipment['RatingMethodRequestedIndicator'] = 1;
        $shipment['TaxInformationIndicator'] = 1;
          
        if($kObject['szSCountry']=='DK')
        { 
            $shipment['ShipmentServiceOptions']['ImportControlIndicator'] = 1;
            $shipment['ShipmentServiceOptions']['ImportControl']['Code'] = '04';
            $shipment['ShipmentServiceOptions']['ImportControl']['Description'] = 'ImportControl Electronic Label';
            //$shipment['ShipmentRatingOptions']['FRSShipmentIndicator'] = '1';
            $FRSPaymentInformation = array();
            $FRSPaymentInformation['Type']['Code'] = '03';
            $FRSPaymentInformation['Type']['Description'] = 'BillThirdParty';
            $FRSPaymentInformation['AccountNumber'] = decrypt($kObject['szMeterNumber'],ENCRYPT_KEY);
            $FRSPaymentInformation['Address']['PostalCode'] = '2100';
            $FRSPaymentInformation['Address']['CountryCode'] = 'DK';
            $shipment['FRSPaymentInformation'] = $FRSPaymentInformation;
        }  
        $shipment['ReturnService'] = 1;
        $shipment['ReturnService']['Code'] = '03';
        $shipment['ReturnService']['Description'] = 'Return Service description';
        $request['Shipment'] = $shipment;     
        return $request;
    }
    
    
    function prepareUpsApiRequestTNTWS($kObject)
    { 
        if(!empty($kObject))
        { 
            //create soap request
            $request = array();
            $option = array(); 
 
            if(!empty($kObject['dtTimingDate']))
            { 
                $dtShippingDate = date('Ymd',strtotime($kObject['dtTimingDate']));
            }
            else
            { 
                $dtShippingDate = date('Ymd');
            }

            //For Transit Time API we only required to send weight of packages no need to send actual dimention details.
            $bDefaultPackageFlag = true;
            if($bDefaultPackageFlag)
            { 
                $productProviderArr=$this->getProductProviderLimitations($kObject['idGroup'],true);  
                $bWeightLimitationDefined = false; 
                if(!empty($productProviderArr))
                {
                    foreach($productProviderArr as $productProviderArrs)
                    {
                        if($productProviderArrs['idCargoLimitationType']==__WEIGHT_PER_COLI__)
                        {
                            if($productProviderArrs['szRestriction']=='Maximum')
                            {
                                $maxWeightPerColli = $productProviderArrs['szLimit']; 
                                $bWeightLimitationDefined = true;
                            }  
                        } 
                        if($productProviderArrs['idCargoLimitationType']==__CHARGEABLE_WEIGHT_PER_COLI__)
                        {
                            if($productProviderArrs['szRestriction']=='Maximum')
                            {
                                $maxChargeableWeight = $productProviderArrs['szLimit']; 
                                $bWeightLimitationDefined = true;
                            } 
                        }
                    }
                } 
                $changeFlag=false; 
                $fCargoWeight = $kObject['fCargoWeight'];
                $fCargoVolume = $kObject['fCargoVolume']; 
                $cargeAbleFlag=false;

                $fRmaingChargeableWeight = 0;
                $fRmaingWeight = 0;

                $fCargoActualWeight = $kObject['fCargoWeight'];

                if(($fCargoVolume*200)>$fCargoWeight)
                {
                    $cargeAbleFlag=true;
                    $fCargoWeight = $kObject['fCargoVolume']*200 ; 
                }   
                $fCargoChargeableWeight = $fCargoWeight; 
                if($bWeightLimitationDefined)
                { 
                    $changeFlag=true;
                    $iTotalNumActulalColli=1;
                    if($maxWeightPerColli>0.00)
                    {
                        $fRmaingWeight = (float)($fCargoActualWeight % $maxWeightPerColli);
                        $iTotalNumActulalColli = (int)($fCargoActualWeight / $maxWeightPerColli);
                        $t=0;
                        if((int)$fRmaingWeight>0)
                        {
                          $iTotalNumActulalColli = $iTotalNumActulalColli + 1;
                        } 
                    }
                    $iTotalChargeNumColli=1;
                    if($maxChargeableWeight>0.00)
                    {                    
                        $fRmaingChargeableWeight = (float)($fCargoChargeableWeight % $maxChargeableWeight);
                        $iTotalChargeNumColli = (int)($fCargoChargeableWeight / $maxChargeableWeight);
                        $t=0;
                        if((int)$fRmaingChargeableWeight>0)
                        {
                          $iTotalChargeNumColli = $iTotalChargeNumColli + 1;
                        } 
                    }  
                    $fRmaingWeight = 0;  
                    if($iTotalChargeNumColli>=$iTotalNumActulalColli)
                    { 
                        $cargoWeight=$fCargoChargeableWeight/$iTotalChargeNumColli;
                        $iTotalNumColli=$iTotalChargeNumColli;
                        $fRmaingWeight = $fCargoChargeableWeight - ($cargoWeight*$iTotalNumColli);
                    }
                    else if($iTotalChargeNumColli<$iTotalNumActulalColli)
                    {
                        $cargoWeight=$fCargoActualWeight/$iTotalNumActulalColli;
                        $iTotalNumColli=$iTotalNumActulalColli;
                        $fRmaingWeight = $fCargoActualWeight - ($cargoWeight*$iTotalNumColli);
                    }
                    else
                    {
                        $changeFlag=true;
                        $iTotalNumColli = 1;
                        $cargoWeight = $fCargoWeight;
                        $fRmaingWeight = 0;
                    }
                    //echo $cargoWeight."###".$iTotalNumColli;
                    //echo $iTotalNumColli;
                }
                else
                {
                    $changeFlag=true;
                    $iTotalNumColli = 1;
                    $cargoWeight = $fCargoWeight;
                    $fRmaingWeight = 0;
                }   

                $totalPackage = $iTotalNumColli; 

                if($changeFlag)
                { 
                    if((float)$fRmaingWeight>0.00)
                    {
                        $totalMaxPackage = $iTotalNumColli-1;
                    }
                    else
                    {
                        $totalMaxPackage = $iTotalNumColli;
                    } 
                    $t=0;
                    $szPackageDetailsLogs .= "<br> Number of Package: ".$totalMaxPackage.", Weight per package: ".$cargoWeight;     
                } 
            } 
            
            $option['RequestOption'] = 'TNT';
            $request['Request'] = $option; 
            
            $pickup['Date'] = $dtShippingDate;
            $request['Pickup'] = $pickup; 

            $szAddress = $kObject['szAddress1'] ;
            if(!empty($kObject['szAddress2']))
            {
                $szAddress .= $kObject['szAddress2'] ;
            }  
 
            $shipfrom['Name'] = $kObject['szFirstName']." ".$kObject['szLastName'];
            //$addressFrom['AddressLine'] = array($szAddress);
            //$addressFrom['City'] = $kObject['szCity'];
            //$addressFrom['StateProvinceCode'] = $kObject['szState'];
            $addressFrom['PostalCode'] = $kObject['szPostCode'];
            $addressFrom['CountryCode'] = $kObject['szCountry'];  
            $shipfrom['Address'] = $addressFrom; 

            $request['ShipFrom'] = $shipfrom;

            $szAddress = $kObject['szSAddress1'] ;
            if(!empty($kObject['szSAddress2']))
            {
                $szAddress .= $kObject['szSAddress2'] ;
            } 
            
            $shipto['Name'] = $kObject['szSFirstName']." ".$kObject['szSLastName'];
            $addressTo = array();
            //$addressTo['AddressLine'] = $szAddress;
            //$addressTo['City'] = $kObject['szSCity'];
            //$addressTo['StateProvinceCode'] = $kObject['szSState'];
            $addressTo['PostalCode'] = $kObject['szSPostCode'];
            $addressTo['CountryCode'] = $kObject['szSCountry']; 
            $shipto['Address'] = $addressTo;
            $request['ShipTo'] = $shipto; 

            $unitOfMeasurement['Code'] = 'KGS';
            $unitOfMeasurement['Description'] ='Kilograms';
            $shipmentWeight['UnitOfMeasurement'] = $unitOfMeasurement;
            $shipmentWeight['Weight'] = $cargoWeight;
            $request['ShipmentWeight'] = $shipmentWeight;

            $request['TotalPackagesInShipment'] = $totalMaxPackage;
            
            $invoiceLineTotal['CurrencyCode'] = 'DKK';
            $invoiceLineTotal['MonetaryValue'] = '1';
            $request['InvoiceLineTotal'] = $invoiceLineTotal;  
            return $request;  
        }
    }
    
    function getValidServiceCodes($validServiceTypeArr)
    {
        if(!empty($validServiceTypeArr))
        {
            $validApiCodeAry = array();
            $ctr_1=0;
            if(!empty($validServiceTypeArr))
            {
                foreach($validServiceTypeArr as $validServiceTypeArrs)
                {
                    if(!empty($validServiceTypeArrs))
                    {
                        $validServiceTypeAry = explode("||||",$validServiceTypeArrs);
                        $validApiCodeAry[$ctr_1] = $validServiceTypeAry[0]; 

                        $validProviderIDAry[$validServiceTypeAry[0]] = $validServiceTypeAry[1]; 
                        $ctr_1++;
                    }
                }
            }  
            $this->apiCodeValuesAry = array_count_values($validApiCodeAry); 
            $this->validServiceTypeArr = $validApiCodeAry ;
            $this->validProviderIDAry = $validProviderIDAry ; 
        } 
    }
    
    function calculateShipmentTransitTimeUPS($data)
    {   
        $this->validateInputNewDetails($data);   
        if($this->error==true)
        {
            return array();
        }      
        $seriverTypeArr = $this->seriverTypeArr; 
        if($data['szAPICode']!='')
        {
            $validServiceTypeArr = explode(";",$data['szAPICode']);
            $this->getValidServiceCodes($validServiceTypeArr);
            $apiCodeValuesAry = array();
            $validServiceTypeArr = array();
            
            $apiCodeValuesAry = $this->apiCodeValuesAry;
            $validServiceTypeArr = $this->validServiceTypeArr ;
        }  
        
        $szApiResponseLog = $data['szForwarderCompanyName']." - UPS";
        
        $szApiResponseLog .= "<br><u>API response:</u>";
        
        $kUser = new cUser();
        $szAccountNumber=decrypt($data['szAccountNumber'],ENCRYPT_KEY);
        $szUsername=decrypt($data['szUsername'],ENCRYPT_KEY);
       
        $szPassword=decrypt($data['szPassword'],ENCRYPT_KEY);
        
        //Configuration
        $access = $szAccountNumber;
        $userid = $szUsername;
        $passwd = $szPassword; 
        $wsdl = __APP_PATH__."/wsdl/TNTWS.wsdl";
        $operation = "ProcessTimeInTransit";
        //$endpointurl = 'http://wwwapps.ups.com/ctc/htmlTool';
        $endpointurl = 'https://onlinetools.ups.com/webservices/TimeInTransit';
        $outputFileName = __APP_PATH__."/wsdl/XOLTResult.xml";
  
        if(!empty($data['dtTimingDate']))
        {  
            $dtShippingDate = date('Ymd',$data['dtTimingDate']);
        }
        else
        { 
            $dtShippingDate = date('Ymd');
        } 
        
        try
        { 
            $mode = array
            (
                'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
                'trace' => 1
            ); 
            // initialize soap client
            $client = new SoapClient($wsdl , $mode);
 
            //create soap header
            $usernameToken['Username'] = $userid;
            $usernameToken['Password'] = $passwd;
            $serviceAccessLicense['AccessLicenseNumber'] = $access;
            $upss['UsernameToken'] = $usernameToken;
            $upss['ServiceAccessToken'] = $serviceAccessLicense; 
             
            try
            {
                $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0','UPSSecurity',$upss);
                $client->__setSoapHeaders($header);

                $request = $this->prepareUpsApiRequestTNTWS($data);     
                $resp = $client->__soapCall($operation ,array($request));   
            } 
            catch (Exception $ex) 
            { 
                $outputFileName = __APP_PATH__."/wsdl/ups_api_exception.log"; 
                //save soap request and response to file
                $fw = fopen($outputFileName , 'w'); 
                fwrite($fw , "Exception: \n" . print_R($ex,true) . "\n");
                fclose($fw); 
            }  
            $fedexApiLogsAry = array(); 
            $fedexApiLogsAry['iWebServiceType'] = 4 ;
            $fedexApiLogsAry['szRequestData'] = print_R($request,true);
            $fedexApiLogsAry['szResponseData'] = print_R($resp,true);
            $this->addFedexApiLogs($fedexApiLogsAry);
        
            // Logging UPS API request and response in xml file 
            $requestFileName = __APP_PATH__."/wsdl/ups_api_request_tt.xml";
            $responseFileName = __APP_PATH__."/wsdl/ups_api_response_tt.xml";

            //save soap request and response to file
            $fw = fopen($requestFileName ,'w');
            fwrite($fw , "Request: \n" . $client->__getLastRequest() . "\n");
            fclose($fw);

            $fw = fopen($responseFileName ,'w');
            fwrite($fw , "Response: \n" . $client->__getLastResponse() . "\n");
            fclose($fw);
                   
            $szApiResponseTextTnT = '<table class="format-2" style="width:100%">';
            $szApiResponseTextTnT .= "<tr>";
            $szApiResponseTextTnT .= "<th>Service Code</th>";
            $szApiResponseTextTnT .= "<th>Service</th>"; 
            $szApiResponseTextTnT .= "<th>Pick-up</th>";
            $szApiResponseTextTnT .= "<th>Delivery</th>";
            $szApiResponseTextTnT .= "<th>Transit(Total)</th>";
            $szApiResponseTextTnT .= "<th>Transit(Busi)</th>";
            $szApiResponseTextTnT .= "<th>Guaranteed</th>";
            $szApiResponseTextTnT .= "</tr>";
            
            $szResponseStatus = $resp->Response->ResponseStatus->Description;
            if(strtolower($szResponseStatus)=='success')
            { 
                $TimeTransitResponseAry = $resp->TransitResponse->ServiceSummary;   
                $ctr=0;
                $ret_ary = array();
                if(!empty($TimeTransitResponseAry))
                {
                    foreach($TimeTransitResponseAry as $TimeTransitResponseArys)
                    { 
                        $code = (int)$TimeTransitResponseArys->Service->Code; 
                        $iActualCode = (int)$TimeTransitResponseArys->Service->Code; 
                        $szServiceDescription = $TimeTransitResponseArys->Service->Description; 
                        
                        /*
                        * For now i am hardcoding few service code because service codes for Rate API is different Then Transit and Time API
                        */
                        if($code==8 || $code==3 || $code==68) //UPS Standard
                        {
                            $code = 11 ; //Standard
                            $szServiceCode = "UPS Standard";
                        }
                        if($code==10 || $code==9) //UPS Express
                        {
                            $code = '07' ; //Worldwide Express
                            $szServiceCode = "Worldwide Express";
                        }
                        if($code==5 || $code==19) //UPS Worldwide Expedited
                        {
                            $code = '08' ; //UPS Worldwide Expedited
                            $szServiceCode = "UPS Worldwide Expedited";
                        }
                        if($code==29) //UPS Worldwide Express Freight
                        {
                            $code = '96' ; //UPS Worldwide Express Freight
                            $szServiceCode = "UPS Worldwide Express Freight";
                        } 
                        if($code==18 || $code==20 || $code==28) //UPS Saver. Required for Rating and Ignored for Shopping
                        {
                            $code = 65 ; //Worldwide Express
                            $szServiceCode = "UPS Saver";
                        }
                        
                        if(!empty($TimeTransitResponseArys->GuaranteedIndicator))
                        {
                            $EstimatedArrivalAry = $TimeTransitResponseArys->GuaranteedIndicator->EstimatedArrival;
                            $szGuaranteed = 'Yes';
                        }
                        else
                        {
                            $EstimatedArrivalAry = $TimeTransitResponseArys->EstimatedArrival;
                            $szGuaranteed = 'No';
                        } 
                        $ret_ary[$code]['dtArrivalDate'] = $EstimatedArrivalAry->Arrival->Date." ". $EstimatedArrivalAry->Arrival->Time;
                        $ret_ary[$code]['dtPickupDate'] = $EstimatedArrivalAry->Pickup->Date." ". $EstimatedArrivalAry->Pickup->Time;
                        $ret_ary[$code]['iBusinessDaysInTransit'] = $EstimatedArrivalAry->BusinessDaysInTransit ;
                        $ret_ary[$code]['iTotalTransitDays'] = $EstimatedArrivalAry->TotalTransitDays ; 
                        $ret_ary[$code]['szDayOfWeek'] = $EstimatedArrivalAry->DayOfWeek ;  
                        
                        $szApiResponseTextTnT .= "<tr>";
                        $szApiResponseTextTnT .= "<td>".$iActualCode."</td>"; 
                        $szApiResponseTextTnT .= "<td>".$szServiceDescription."</td>"; 
                        $szApiResponseTextTnT .= "<td>".date('d M, Y H:i:s',strtotime($ret_ary[$code]['dtPickupDate']))."</td>"; 
                        $szApiResponseTextTnT .= "<td>".date('d M, Y H:i:s',strtotime($ret_ary[$code]['dtArrivalDate']))."</td>"; 
                        $szApiResponseTextTnT .= "<td>".$ret_ary[$code]['iTotalTransitDays']."</td>"; 
                        $szApiResponseTextTnT .= "<td>".$ret_ary[$code]['iBusinessDaysInTransit']."</td>"; 
                        $szApiResponseTextTnT .= "<td>".$szGuaranteed."</td>";  
                        $szApiResponseTextTnT .= "</tr>";
                    }
                }
                
                $szApiResponseTextTnT .= "</table>";
                $this->iSuccessMessage =1;
                $this->szApiResponseText = $szApiResponseTextTnT ; 
                return $ret_ary; 
            }
            else
            { 
                return array();
            }  
        }
        catch(Exception $ex)
        {   
            $outputFileName = __APP_PATH__."/wsdl/ups_api_exception.log"; 
            print_R($ex);
            //save soap request and response to file
            $fw = fopen($outputFileName , 'w'); 
            fwrite($fw , "Exception: \n" . print_R($ex,true) . "\n");
            fclose($fw);
            return array();
        }
    } 
    
    function calculateShippingFedexDetails($data,$rates_by_packet=false,$validServiceTypeArr=array())
    {   
        $this->validateInputNewDetails($data);  
        if($this->error==true)
        {   
            return array();
        }   
        $validServiceTypeArr=array();
        if($rates_by_packet)
        { 
            $arrAPICode = array();
            $serviceCodeDetailsAry = array(); 
            if($data['szAPICode']!='')
            {
                $validServiceTypeArr = explode(";",$data['szAPICode']); 
            }
        }
        else
        {
            if($data['szAPICode']!='')
            {
                $validServiceTypeArr = explode(";",$data['szAPICode']);
                $this->getValidServiceCodes($validServiceTypeArr);
                $apiCodeValuesAry = array();
                $validServiceTypeArr = array();

                $apiCodeValuesAry = $this->apiCodeValuesAry;
                $validServiceTypeArr = $this->validServiceTypeArr;
                $validProviderIDAry = $this->validProviderIDAry;
            }
        }
        
        $szApiResponseLog = $data['szForwarderCompanyName']." - FedEx" ;
         
        $newline = "<br />";  
        $kUser = new cUser();
        $szAccountNumber=decrypt($data['szAccountNumber'],ENCRYPT_KEY);
        $szUsername=decrypt($data['szUsername'],ENCRYPT_KEY);
        $szMeterNumber=decrypt($data['szMeterNumber'],ENCRYPT_KEY);
        $szPassword=decrypt($data['szPassword'],ENCRYPT_KEY);
        $idCustomerCurrency = $data['idCurrency'];
        
        //Please include and reference in $path_to_wsdl variable.
        $path_to_wsdl = __APP_PATH__."/wsdl/RateService_v14.wsdl";
         
        //The WSDL is not included with the sample code.
        $client = new SoapClient($path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information
         
        $request['WebAuthenticationDetail'] = array(
                'UserCredential' =>array(
                        'Key' => $szUsername, 
                        'Password' => $szPassword
                )
        ); 
        $request['ClientDetail'] = array(
                'AccountNumber' => $szAccountNumber, 
                'MeterNumber' => $szMeterNumber
        );
        $request['TransactionDetail'] = array('CustomerTransactionId' => md5(time()));
        $request['Version'] = array(
                'ServiceId' => 'crs', 
                'Major' => '14', 
                'Intermediate' => '0', 
                'Minor' => '0'
        );
 
        //$serviceType = 'INTERNATIONAL_PRIORITY'; 

        $request['ReturnTransitAndCommit'] = true;
        $request['RequestedShipment']['DropoffType'] = 'REGULAR_PICKUP'; // valid values REGULAR_PICKUP, REQUEST_COURIER, ...
        
        if(!empty($data['dtTimingDate']))
        {
            $request['RequestedShipment']['ShipTimestamp'] = date('c',strtotime($data['dtTimingDate']));
            $dtShippingDate = $data['dtTimingDate'];
        }
        else
        {
            $request['RequestedShipment']['ShipTimestamp'] = date('c');
            $dtShippingDate = date('c');
        }
         
        if(!empty($data['szServiceType']))
        {
            //$request['RequestedShipment']['ServiceType'] = $data['szServiceType']; // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
        }
        //$request['RequestedShipment']['PackagingType'] = $data['szPackageType']; // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
 
        $request['RequestedShipment']['Shipper'] = $this->addShipper($this);
        $request['RequestedShipment']['Recipient'] = $this->addRecipient($this);

        $request['RequestedShipment']['ShippingChargesPayment'] = $this->addShippingChargesNewPayment($data);		
        $request['RequestedShipment']['RateRequestTypes'] = 'ACCOUNT'; 
        $request['RequestedShipment']['RateRequestTypes'] = 'PREFERRED'; 
        $request['RequestedShipment']['PurposeOfShipmentType'] = 'SAMPLE'; 
         
        $idBooking = $data['idBooking']; 
        $iSendDefaultCargo = true;
        
        $szPackageDetailsLogs = '<br><br><u>Package Details</u><br>';
        $bPartnerPacking = false;
        if(cPartner::$bApiValidation && !empty(cPartner::$searchablePacketTypes) && (in_array(1, cPartner::$searchablePacketTypes) || in_array(2, cPartner::$searchablePacketTypes)))
        {
            $bPartnerPacking = true;
        } 
        if($idBooking>0 || $bPartnerPacking)
        {
            $bookingCargoAry = array(); 
            $kBooking =new cBooking();
            if($bPartnerPacking)
            {
                $kPartner = new cPartner();
                $bookingCargoAry = $kPartner->getCargoDeailsByPartnerApi(); 
            }
            else
            {
                $bookingCargoAry = $kBooking->getCargoDeailsByBookingId($idBooking,true); 
                $postSearchAry = $kBooking->getBookingDetails($idBooking);
            }  
            
            $kWHSSearch = new cWHSSearch();
            if(!empty($bookingCargoAry) && ($bPartnerPacking || $postSearchAry['iCourierBooking']==1 || $postSearchAry['iSearchMiniVersion']==3 || $postSearchAry['iSearchMiniVersion']==4))
            { 
                $iSendDefaultCargo = false;
                $ctr=1;
                $iQuantity = 0;
                foreach($bookingCargoAry as $bookingCargoArys)
                {  
                    if($postSearchAry['iPalletType']>0 && (float)$bookingCargoArys['fHeight']<=0)
                    {
                        /*
                         * If shipment type is pallet and height is empty then we use standard height as per pallet type
                         */
                        if($postSearchAry['iPalletType']==1) //Euro Pallet
                        {
                            $bookingCargoArys['fHeight'] = __STANDARD_HEIGHT_EURO_PALLET__;
                        }
                        else if($postSearchAry['iPalletType']==2) //Half Pallet
                        {
                            $bookingCargoArys['fHeight'] = __STANDARD_HEIGHT_HALF_PALLET__;
                        }
                    }
                    
                    $iLenght = 0;
                    $iWidth = 0;
                    $iHeight = 0;
                    $fWeight = 0;
                    if($bookingCargoArys['idWeightMeasure']==1)
                    {
                        $fKgFactor = 1;
                    }
                    else
                    { 
                        $fKgFactor = $kWHSSearch->getWeightFactor($bookingCargoArys['idWeightMeasure']); 
                    }

                    if($bookingCargoArys['idCargoMeasure']==1)
                    {
                        $fCmFactor = 1;
                    }
                    else
                    {
                        $fCmFactor = $kWHSSearch->getCargoFactor($bookingCargoArys['idCargoMeasure']);  
                    }

                    if($fCmFactor>0)
                    {
                        $iLenght = round($bookingCargoArys['fLength'] / $fCmFactor);
                        $iWidth = round($bookingCargoArys['fWidth'] / $fCmFactor);
                        $iHeight = round($bookingCargoArys['fHeight'] / $fCmFactor);
                    }

                    if($fKgFactor>0)
                    {
                        $fWeight = round($bookingCargoArys['fWeight']/$fKgFactor) ;
                    }

                    $szCargoMeasureCode = 'CM';  
                    $szWeightMeasureCode = 'KG'; 
                    
                    $iQuantity += $bookingCargoArys['iQuantity'] ;
                    $RequestedPackageLineItemsAry[] = array(
                        'SequenceNumber'=>$ctr,
                        'GroupPackageCount'=> $bookingCargoArys['iQuantity'],
                        'Weight' => array(
                        'Value' => $fWeight,
                        'Units' => $szWeightMeasureCode
                        ),
                        'Dimensions' => array(
                        'Length' => $iLenght,
                        'Width' => $iWidth,
                        'Height' => $iHeight,
                        'Units' => $szCargoMeasureCode
                        ) 
                    );
                    $ctr++;
                    $szPackageDetailsLogs .= "<br> Package: ".$bookingCargoArys['fLength']." X ".$bookingCargoArys['fWidth']." X ".$bookingCargoArys['fHeight']." ".$szCargoMeasure. ", ".$bookingCargoArys['fWeight']." ".$szWeightMeasure.", Count: ".$bookingCargoArys['iQuantity'] ;
                }
                $szPackageDetailsLogs .= "<br><br> Total package: ".$iQuantity;
                $request['RequestedShipment']['PackageCount'] = $iQuantity;
                $request['RequestedShipment']['PreferredCurrency'] = $data['szCurrencyName'];
                $request['RequestedShipment']['RequestedPackageLineItems'] = $RequestedPackageLineItemsAry;
            } 
        } 
        
        $chargeAbleFlag=false;
        if(($data['fCargoVolume']*200)>$this->fCargoWeight)
        {
            $fDefaultChargeableWeight = $data['fCargoVolume']*200 ;
            $chargeAbleFlag=true;
        }
        else
        {
            $fDefaultChargeableWeight = $this->fCargoWeight;
        } 
        $fCargoChargeableWeight = $fDefaultChargeableWeight ;
        $fCargoActualWeight = $this->fCargoWeight;
        
        if($iSendDefaultCargo)
        { 
            $productProviderArr = $this->getProductProviderLimitations($data['idGroup'],true);
             
            $bWeightPercolliAdded = false;
            $bChargeableWeightAdded = false;
            if(!empty($productProviderArr))
            {
                foreach($productProviderArr as $productProviderArrs)
                {
                    if($productProviderArrs['idCargoLimitationType']==__WEIGHT_PER_COLI__)
                    {
                        if($productProviderArrs['szRestriction']=='Maximum')
                        {
                            $maxWeightPerColli = $productProviderArrs['szLimit']; 
                            $bWeightPercolliAdded = true;
                        }  
                    } 
                    if($productProviderArrs['idCargoLimitationType']==__CHARGEABLE_WEIGHT_PER_COLI__)
                    {
                        if($productProviderArrs['szRestriction']=='Maximum')
                        {
                            $maxChargeableWeight = $productProviderArrs['szLimit']; 
                            $bChargeableWeightAdded = true;
                            $bWeightPercolliAdded = true;
                        } 
                    }
                }
            } 
             
            $changeFlag=false;
            $fRmaingWeight = 0;
            $fRmaingChargeableWeight = 0;
            if($bWeightPercolliAdded)
            {
                $changeFlag=true;
                $iTotalNumActulalColli=1;
                if($maxWeightPerColli>0.00)
                {
                    $fRmaingWeight = (float)($fCargoActualWeight % $maxWeightPerColli);
                    $iTotalNumActulalColli = (int)($fCargoActualWeight / $maxWeightPerColli);
                    $t=0;
                    if((int)$fRmaingWeight>0)
                    {
                      $iTotalNumActulalColli = $iTotalNumActulalColli + 1;
                    } 
                }
                $iTotalChargeNumColli=1;
                if($maxChargeableWeight>0.00)
                {                    
                    $fRmaingChargeableWeight = (float)($fCargoChargeableWeight % $maxChargeableWeight);
                    $iTotalChargeNumColli = (int)($fCargoChargeableWeight / $maxChargeableWeight);
                    $t=0;
                    if((int)$fRmaingChargeableWeight>0)
                    {
                      $iTotalChargeNumColli = $iTotalChargeNumColli + 1;
                    } 
                }  
                if($iTotalChargeNumColli>=$iTotalNumActulalColli)
                {
                    $cargoWeight=$fCargoChargeableWeight/$iTotalChargeNumColli;
                    $totalActualCount=$iTotalChargeNumColli;
                    $fRmaingWeight = $fCargoChargeableWeight - ($cargoWeight*$totalActualCount);
                }
                else if($iTotalChargeNumColli<$iTotalNumActulalColli)
                {
                    $cargoWeight=$fCargoActualWeight/$iTotalNumActulalColli;
                    $totalActualCount=$iTotalNumActulalColli;
                    $fRmaingWeight = $fCargoActualWeight - ($cargoWeight*$totalActualCount);
                }
                else
                {                                                                                                                                                                                                                                                                                                                                                                                  
                    $changeFlag=true;
                    $totalActualCount = 1;
                    $cargoWeight = $fDefaultChargeableWeight;
                    $fRmaingWeight = 0;
                }
            }
            else
            {
                $changeFlag=true;
                $totalActualCount = 1;
                $cargoWeight = $fDefaultChargeableWeight;
                $fRmaingWeight = 0;
            } 
            
            if($totalChargeCount>$totalActualCount)
            {
                $totalPackage=$totalChargeCount; 
            }
            else
            {
                $totalPackage=$totalActualCount;
            }
            if($changeFlag)
            {
                $szPackageDetailsLogs .= "<br> Number of Package: ".$totalPackage.", Weight per package: ".$cargoWeight;
                $totalMaxColliKgPack=$totalPackage;
                
                if((int)$fRmaingWeight>0)
                {
                    $szPackageDetailsLogs .= "<br> Number of Package: 1, Weight per package: ".$fRmaingWeight;
                    $RequestedPackageLineItemsAry[] = array(
                         'SequenceNumber'=>1,
                         'GroupPackageCount'=>$totalMaxColliKgPack,
                         'Weight' => array(
                         'Value' => $cargoWeight,
                         'Units' => 'KG'
                        ) 
                    );   
                    $RequestedPackageLineItemsAry[] = array(
                         'SequenceNumber'=>2,
                         'GroupPackageCount'=>1,
                         'Weight' => array(
                         'Value' => $fRmaingWeight,
                         'Units' => 'KG'
                        ) 
                    ); 
                }
                else
                {
                    $RequestedPackageLineItemsAry[] = array(
                         'SequenceNumber'=>1,
                         'GroupPackageCount'=>$totalMaxColliKgPack,
                         'Weight' => array(
                         'Value' => $cargoWeight,
                         'Units' => 'KG'
                        ) 
                    );   
                } 
                //$request['RequestedShipment']['RateRequestType'] = 'PREFERRED';
                $request['RequestedShipment']['PreferredCurrency'] = $data['szCurrencyName'];
                $request['RequestedShipment']['PackageCount'] = $totalPackage;
                $request['RequestedShipment']['RequestedPackageLineItems'] = $RequestedPackageLineItemsAry; 
            }
            else 
            {
                //$request['RequestedShipment']['RateRequestType'] = 'PREFERRED';
                $request['RequestedShipment']['PreferredCurrency'] = $data['szCurrencyName'];
                $request['RequestedShipment']['PackageCount'] = 1;
                $request['RequestedShipment']['RequestedPackageLineItems'] = $this->addPackageLineItem1($this); 
            }
        }  
        
        $this->szPackageDetailsLogs = $szPackageDetailsLogs ;
        $this->szPackageDetailsLogs .= "<br><br> <u>Shipper </u><br> ";
        $this->szPackageDetailsLogs .= "Address: ".$data['szAddress1'];
        $this->szPackageDetailsLogs .= "<br>Postcode: ".$data['szPostCode'];
        $this->szPackageDetailsLogs .= "<br>City: ".$data['szCity'];
        $this->szPackageDetailsLogs .= "<br>Country: ".$data['szCountry'];
        
        $this->szPackageDetailsLogs .= "<br><br> <u>Consignee </u><br> ";
        $this->szPackageDetailsLogs .= "Address: ".$data['szSAddress1'];
        $this->szPackageDetailsLogs .= "<br>Postcode: ".$data['szSPostCode'];
        $this->szPackageDetailsLogs .= "<br>City: ".$data['szSCity'];
        $this->szPackageDetailsLogs .= "<br>Country: ".$data['szSCountry'];
        
        $szApiResponseLog .= "<br><br><u>API response:</u>"; 
        
        try 
        {
            if(setEndpoint('changeEndpoint'))
            {
                $newLocation = $client->__setLocation(setEndpoint('endpoint'));
            }   
            
            $_SESSION['fedexRates'] = array();
            if(!empty($_SESSION['fedexRates']))
            {
               $response = $_SESSION['fedexRates'];
            }
            else
            {
                $response = $client->getRates($request);   
            } 
            $requestFileName = __APP_PATH__."/wsdl/fedex_api_request.xml";
            $responseFileName = __APP_PATH__."/wsdl/fedex_api_response.xml";

            //save soap request and response to file
            $fw = fopen($requestFileName , 'w');
            fwrite($fw , "Request: \n" . $client->__getLastRequest() . "\n");
            fclose($fw);

            $fw = fopen($responseFileName , 'w');
            fwrite($fw , "Response: \n" . $client->__getLastResponse() . "\n");
            fclose($fw);
            
            $fedexApiLogsAry = array();
            $fedexApiLogsAry['idUser'] = $kObject->id ;
            $fedexApiLogsAry['iWebServiceType'] = 1 ;
            $fedexApiLogsAry['szRequestData'] = print_R($request,true);
            $fedexApiLogsAry['szResponseData'] = print_R($response,true);
            $this->addFedexApiLogs($fedexApiLogsAry);   
                    
            $this->szPackageDetailsLogs .= "<br> <strong> API Response:</strong> <br><br> Status: ".$response -> HighestSeverity."";
            $this->szPackageDetailsLogs .= " <br> Status code: ".$response -> Notifications->Code."";
            $this->szPackageDetailsLogs .= " <br> Description: ".$response->Notifications->LocalizedMessage."<br><br>";
              
            if($response ->HighestSeverity != 'FAILURE' && $response ->HighestSeverity != 'ERROR')
            {  	
                $rateReply = $response->RateReplyDetails;  
                $kWarehouseSearch = new cWHSSearch();
                $iNumDaysAddedToDelivery = $kWarehouseSearch->getManageMentVariableByDescription('__NUMBER_OF_DAYS_ADDED_TO_DELIVERY_DATE__');
                
                $szAmount=0;
                $ctr_1 = 0;
                $apiResponseAry = array();
               
                $courierProductMapping = array();
                $courierProductMapping = $this->getAllProductByApiCodeMapping();
                  
                if(!empty($rateReply))
                { 
                    foreach($rateReply as $rateReplys)
                    {  
                        if(!empty($validServiceTypeArr) && in_array($rateReplys->ServiceType,$validServiceTypeArr))
                        {
                            $szServiceType = $rateReplys->ServiceType ;
                            $dtDeliveryTimestamp = $rateReplys->DeliveryTimestamp ;  
                            if(empty($dtDeliveryTimestamp))
                            { 
                                $iTransitDays = $courierProductMapping[$rateReplys->ServiceType]['iDaysStd'];   
                                $dtDeliveryTimestamp = date('Y-m-d H:i:s',strtotime($dtShippingDate."+".$iTransitDays." DAYS"));   
                            } 
                            
                            $rateShippmentArr=array($rateShippmentArr);
                            $rateShippmentArr=$rateReplys->RatedShipmentDetails;  
                            if(!empty($rateShippmentArr))
                            { 
                                $checkDataFlag=false;
                                foreach($rateShippmentArr as $rateShippmentArrs)
                                { 
                                    if($rateShippmentArrs->ShipmentRateDetail->RateType=='PREFERRED_ACCOUNT_SHIPMENT')
                                    {
                                        $fTotalNetCharge=$rateShippmentArrs->ShipmentRateDetail->TotalNetCharge->Amount ; 
                                        if(!empty($dtDeliveryTimestamp) && $fTotalNetCharge>0)
                                        {
                                            $checkDataFlag=true;
                                            $apiResponseAry[$ctr_1]['szServiceType'] = $rateReplys->ServiceType ;
                                            $apiResponseAry[$ctr_1]['dtDeliveryTimeStamp'] = $dtDeliveryTimestamp ;
                                            $apiResponseAry[$ctr_1]['fTotalNetCharge'] = $fTotalNetCharge;
                                            $apiResponseAry[$ctr_1]['szShippingCurrency'] = $rateShippmentArrs->ShipmentRateDetail->TotalNetCharge->Currency ;  
                                            $ctr_1++;
                                        }
                                    } 
                                }
                                
                                if(!$checkDataFlag) 
                                {
                                    $fTotalNetCharge = $rateReplys->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Amount ; 
                                    if(!empty($dtDeliveryTimestamp) && $fTotalNetCharge>0)
                                    {
                                        $apiResponseAry[$ctr_1]['szServiceType'] = $rateReplys->ServiceType ;
                                        $apiResponseAry[$ctr_1]['dtDeliveryTimeStamp'] = $dtDeliveryTimestamp ;
                                        $apiResponseAry[$ctr_1]['fTotalNetCharge'] = $fTotalNetCharge;
                                        $apiResponseAry[$ctr_1]['szShippingCurrency'] = $rateReplys->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Currency ;  
                                        $ctr_1++;
                                    }
                                }
                            }
                        }
                    }  
                }  
                /*
                if($rates_by_packet && !empty($apiResponseAry))
                { 
                    /*
                     * 
                     * 1. This section will be called when user enetered cargo details on /service/ screen
                     * 2. Here we are taking response in to 2 steps
                     * 3. If get the response back from API for same Service code what user has selected then no need for extra calculation just return that service code's response.
                     * 4. If we do not get response for selected service code then we select cheapest and fastest service.  @Ajay
                     * 
                     
                    $apiResponseTempAry = $apiResponseAry;
                    $bFoundService = false;
                    if(!empty($apiResponseTempAry))
                    {
                        foreach($apiResponseTempAry as $apiResponseTempArys)
                        {
                            if($apiResponseTempArys['szServiceType']==$data['szAPICode'])
                            {
                                $bFoundService = false;
                                $apiResponseFoundAry = $apiResponseTempArys;
                            } 
                        }
                    }

                    if($bFoundService)
                    { 
                        $apiResponseAry = array();
                        $apiResponseAry[0] = $apiResponseFoundAry ;
                    }
                    else
                    { 
                        $apiResponseTempAry = sortArray($apiResponseTempAry,'fTotalNetCharge',true,'dtDeliveryTimeStamp',true,false,false,true); 
                        $apiResponseAry = array();
                        $apiResponseAry[0] = $apiResponseTempAry[0]; 
                    }  
                    
                    $szSpiServiceCode = $apiResponseAry[0]['szServiceType'];
                     
                    if(!empty($serviceCodeDetailsAry[$szSpiServiceCode]))
                    {  
                        $apiResponseAry[0]['idCourierProviderProductid'] = $serviceCodeDetailsAry[$szSpiServiceCode]['idCourierProviderProductid'];
                    } 
                } */
                if(!empty($apiResponseAry))
                {
                    $c=0;
                    $counter=0;
                    foreach($apiResponseAry as $apiResponseArys)
                    {  
                        $idCourierProviderProductid = 0;
                        if(!empty($validProviderIDAry[$code]))
                        {
                            $idCourierProviderProductid = $validProviderIDAry[$code];
                        }
                        $pricingArr=$this->getCourierProductProviderPricingData($data,$apiResponseArys['szServiceType'],$idCourierProviderProductid);
                        $szServiceType = $apiResponseArys['szServiceType'];
                        $dtDeliveryTimeStamp = $apiResponseArys['dtDeliveryTimeStamp'];
                        $fTotalShipmentCost = $apiResponseArys['fTotalNetCharge'];
                        $szCurrencyCode = $apiResponseArys['szShippingCurrency'];
                         
                        $szApiResponseLog .=" <br><b>".++$counter.". Service Type:</b> ".str_replace("_"," ",$szServiceType) ; 
                        $szApiResponseLog .=" <br> Shipment Price: ".$szCurrencyCode." ".number_format((float)$fTotalShipmentCost,2);
                        $szApiResponseLog .=" <br> Shipping date: ".$dtShippingDate ; 
                        $szApiResponseLog .=" <br> Delivery date: ".$dtDeliveryTimeStamp ; 
                        if($szCurrencyCode=='RMB')
                        {
                            $szCurrencyCode='CNY';
                        }
                        
                        //$szApiResponseLog .=" <br> <br> Rate calculation: " ;
                        
                        $fShipmentCost = $fTotalShipmentCost; 
                        if($szCurrencyCode==__TO_CURRENCY_CONVERSION__)
                        {
                            $fShipmentAPIUSDCost = $fShipmentCost;
                        }
                        else
                        { 
                            $fExchangeRate=$this->getExchangeRateCourierPrice($szCurrencyCode); 
                            $fExchangeRate = 1/$fExchangeRate; 
                            $fShipmentAPIUSDCost =$fShipmentCost/$fExchangeRate;
                        } 
                        
                        $szApiResponseLog .= "<br>Buy rate - ".$szCurrencyCode." ".number_format((float)$fTotalShipmentCost,2);
                        $szApiResponseLog .= "<br>ROE - ".round($fExchangeRate,4) ;
                        $szApiResponseLog .= "<br>Base currency - USD ".$fShipmentAPIUSDCost ; 
                    	$markUpByRate = $fShipmentAPIUSDCost * .01 * $pricingArr[0]['fMarkupPercent']; 
                       
                        $idForwarderCurrency = $data['idForwarderCurrency'];
                        $miniMarkUpRate = $pricingArr[0]['fMinimumMarkup'];
                        $fMarkupperShipment = $pricingArr[0]['fMarkupperShipment']; 
                        
                        if($idForwarderCurrency==1)
                        { 
                            $fMarkupperShipmentUSD = $fMarkupperShipment ;
                            $miniMarkUpRateUSD = $miniMarkUpRate ;
                            $szForwarderCurrency = 'USD';
                        }
                        else
                        {
                            $resultAry = $kWarehouseSearch->getCurrencyDetails($idForwarderCurrency);	  
                            $szForwarderCurrency = $resultAry['szCurrency'];
                            $fForwarderExchangeRate = $resultAry['fUsdValue'];
                            
                            $fMarkupperShipmentUSD = $fMarkupperShipment * $fForwarderExchangeRate ;
                            $miniMarkUpRateUSD = $miniMarkUpRate * $fForwarderExchangeRate ; 
                       } 
                        
                        if($miniMarkUpRateUSD>$markUpByRate)
                        {
                            $fTotalMarkup = $miniMarkUpRateUSD ;
                        }
                        else
                        {
                           $fTotalMarkup =  $markUpByRate ;
                        }
                       	$fPriceAfterMarkUp = $fShipmentAPIUSDCost + $fTotalMarkup ; 
                       	 
                        $szApiResponseLog .= "<br>Markup (".$pricingArr[0]['fMarkupPercent']."%, minimum USD ".$miniMarkUpRateUSD.") – USD ".$fTotalMarkup;
                        $szApiResponseLog .= "<br> Additional fixed markup – USD ".$fMarkupperShipmentUSD ;
                        
                        $fBookingLabelPrice=0;
                       	$fBookingprice=0;
                       	if((int)$pricingArr[0]['iBookingIncluded']!=1)
                       	{ 
                            if($data['iBookingPriceCurrency']==1)
                            {  
                                $iBookingPriceCurrency = 1;  
                                $iBookingPriceCurrencyROE = 1;
                                $szBookingPriceCurrency = 'USD';
                                $fBookingprice = $data['fPrice']; 
                            }
                            else
                            {
                                $resultAry = $kWarehouseSearch->getCurrencyDetails($data['iBookingPriceCurrency']); 
                                $iBookingPriceCurrencyROE = $resultAry['fUsdValue']; 
                                $szBookingPriceCurrency = $resultAry['szCurrency']; 
                                $iBookingPriceCurrency = $resultAry['idCurrency']; 
                                if($iBookingPriceCurrencyROE>0)
                                {
                                    $fBookingprice = $data['fPrice']*$iBookingPriceCurrencyROE;
                                }
                                else
                                {
                                    $fBookingprice = 0;
                                }
                            } 
                            $fBookingLabelPrice=$data['fPrice'];
                            $szApiResponseLog .= " <br> Booking and label fee for Transporteca: ".$szBookingPriceCurrency." ".$data['fPrice']."" ;
                            $szApiResponseLog .= " <br> Booking and label fee USD: ".$fBookingprice ;
                            $szApiResponseLog .= " <br> Booking and label fee exchange rate: ".$iBookingPriceCurrencyROE ;
                       	}  
                        else
                        {
                            $szApiResponseLog .= " <br> Booking and label fee for Transporteca: NO ";
                        } 
                       	$fShipmentUSDCost = $fPriceAfterMarkUp + $fMarkupperShipmentUSD + $fBookingprice; 
                       	  
                       	if($data['iProfitType']==2)
                       	{ 
                            $subTotalForRefferalFee=$fShipmentUSDCost;
                            $fMarkUpRefferalFee = ((0.01 * $data['fReferalFee'])/( 1 - (0.01*$data['fReferalFee'])) ) * $subTotalForRefferalFee;
                            $fShipmentUSDCost = $fShipmentUSDCost + $fMarkUpRefferalFee ;
                            $szApiResponseLog .= " <br> GP Type: Mark-up ";
                            $szApiResponseLog .= " <br> GP Amount: USD ".($fMarkUpRefferalFee+$fBookingprice);
                       	}
                        else
                        {
                            $subTotalForRefferalFee=$fShipmentUSDCost;
                            $fMarkUpRefferalFee = $subTotalForRefferalFee*.01*$data['fReferalFee'];
                            $szApiResponseLog .= " <br> GP Type: Referral ";
                            $szApiResponseLog .= " <br> GP Amount: USD ".($fMarkUpRefferalFee+$fBookingprice);
                        } 
                    	 
                        $szApiResponseLog .= "<br> Total sales price without VAT: USD ".number_format((float)$fShipmentUSDCost,2);
                        
                        $szApiResponseLog .= "<br><br> API Delivery ".date('Y-m-d H:i:s',strtotime($dtDeliveryTimeStamp));
                        $szApiResponseLog .= "<br>Number of days added to delivery ".$iNumDaysAddedToDelivery." days";
                        
                        $serviceType =$szServiceType; 
                        if($iNumDaysAddedToDelivery>0)
                        {
                            $deliveryDate = date('Y-m-d H:i:s',strtotime($dtDeliveryTimeStamp . "+".$iNumDaysAddedToDelivery." days"));
                            
                            $timeStamp=date('H:i:s',strtotime($deliveryDate));
                            $dayName=date('D',strtotime($deliveryDate)); 
                            if($dayName=='Sun' || $dayName=='Sat')
                            {
                               $deliveryDate = getBusinessDaysForCourierDeliveryDate($deliveryDate, 1)." ".$timeStamp;
                            } 
                        }
                        else
                        {
                            $deliveryDate = date('Y-m-d H:i:s',strtotime($dtDeliveryTimeStamp)); 
                        }
                        
                        $szApiResponseLog .= "<br>Delivery date: ".$deliveryDate;
                        
                       // echo $deliveryDate;
                        $count=count($ret_ary);
                        $updateFlag=true;
                        if($count>0)
                        {
                            $updateFlag=false;
                            if((float)$szAmount>(float)$fShipmentCost)
                            {														
                                $updateFlag=true;							
                            } 
                        }
                        else
                        {
                            $updateFlag=true;
                            $szAmount=$fShipmentCost;
                        }
                        
                        if($updateFlag)
                        {
                            /*if((float)$fVatRate>0.00)
                            {
                                if((int)$_SESSION['user_id']>0)
                                {
                                    $kUser = new cUser();
                                    $kUser->getUserDetails($_SESSION['user_id']);
                                    //print_r($kUser);
                                    $kWarehouseSearch = new cWHSSearch();
                                    $fPriceMarkUp = $kWhsSearch->getManageMentVariableByDescription('__MARK_UP_PRICING_PERCENTAGE__');
                                    if($kUser->szCurrency==1)
                                    {
                                        $fVatRateAmount=$fShipmentUSDCost*0.01*$fVatRate;   
                                        $fShipmentVatUSDCost=$fShipmentUSDCost+$fVatRateAmount;



                                        $szApiResponseLog .= "<br>VAT Percentage:".$fVatRate."%";

                                        $szApiResponseLog .= "<br>VAT Amount: USD ".round($fVatRateAmount,2);

                                    }
                                    else
                                    {
                                            $szApiResponseLog .= "<br>VAT Percentage:".$fVatRate."%";
                                            $resultAry = $kWarehouseSearch->getCurrencyDetails($kUser->szCurrency); 
                                            $iVatPriceCurrencyROE = $resultAry['fUsdValue']; 
                                            $szVatPriceCurrency = $resultAry['szCurrency']; 
                                            $iVatPriceCurrency = $resultAry['idCurrency']; 
                                            if($iVatPriceCurrencyROE>0)
                                            {
                                                $fVatCalAmuount = $fShipmentUSDCost/$iVatPriceCurrencyROE;
                                            }
                                            else
                                            {
                                                $fVatCalAmuount = 0;
                                            }
                                            //echo $fVatCalAmuount;    
                                            $fVatRateAmount=$fVatCalAmuount*0.01*$fVatRate;

                                            $fVatRateAmountUSD=$fVatRateAmount*$iVatPriceCurrencyROE;
                                            $fShipmentVatUSDCost=$fShipmentUSDCost+$fVatRateAmountUSD;
                                           // echo $fShipmentAPIUSDCost;
                                            $szApiResponseLog .= "<br>VAT Amount: USD ".round($fVatRateAmountUSD,2);

                                        }

                                        $szApiResponseLog .= "<br> Total sales price including VAT: USD ".number_format((float)$fShipmentVatUSDCost,2);
                                }
                            }*/
                            $ret_ary[$c]['dtAvailableDate'] = $deliveryDate ;
                            $ret_ary[$c]['szCurrencyCode'] = $szCurrencyCode ;
                            $ret_ary[$c]['fShippingAmount'] = $fShipmentCost ;
                            $ret_ary[$c]['szServiceType'] =  $serviceType ; 
                            $ret_ary[$c]['fShipmentAPIUSDCost'] =  round($fShipmentAPIUSDCost,2) ; 
                            $ret_ary[$c]['fShipmentUSDCost'] =  round($fShipmentUSDCost,2) ;  
                            $ret_ary[$c]['markUpByRate'] =  $fTotalMarkup ; 
                            $ret_ary[$c]['miniMarkUpRate'] = $miniMarkUpRate;
                            $ret_ary[$c]['fPriceAfterMarkUp']=$fPriceAfterMarkUp;   
                            $ret_ary[$c]['fMarkupperShipment']=$fMarkupperShipment; 
                            $ret_ary[$c]['fMarkUpRefferalFee']=$fMarkUpRefferalFee;
                            $ret_ary[$c]['fReferalPercentage'] = $data['fReferalFee'] ;
                            $ret_ary[$c]['fBookingprice']=$fBookingprice; 
                            $ret_ary[$c]['idForwarderCurrency'] = $idForwarderCurrency;
                            $ret_ary[$c]['iBookingIncluded'] = $pricingArr[0]['iBookingIncluded'];
                            
                            if($idCourierProviderProductid>0 && $rates_by_packet)
                            {
                                $ret_ary[$c]['idCourierProviderProductid'] = $idCourierProviderProductid;
                            }
                            //$ret_ary[$c]['fVATPercentage'] = $fVatRate;
                            //$ret_ary[$c]['fVATAmountForwarderCurrency'] = $fVatRateAmount;
                            //$ret_ary[$c]['iVatPriceCurrencyROE'] = $iVatPriceCurrencyROE;
                            //print_r($ret_ary);
                            ++$c;
                        }
                    }  
                } 
                $this->szApiResponseText = print_r($ret_ary,true);
                $this->iSuccessMessage = 1 ; 
                $this->szApiResponseLog = $szApiResponseLog ;
                
                
//                $szLogFile = __APP_PATH__."/wsdl/response.xml";
//                $strData=array();
//                $strData[0] = print_R($response,true) ;
//                filelog(true,$strData,$szLogFile);
                
                return $ret_ary; 
            }else{  
                $szNotiFication = '';
                $notificationAry = $response->Notifications; 
                if(!empty($notificationAry->Message))
                {
                    $szNotiFication .= $notificationAry->Message."<br>" ;
                }
                else if(!empty($notificationAry))
                {
                    foreach($notificationAry as $errors)
                    { 
                        $szNotiFication .= $errors->Message."<br>" ;
                    }
                } 
                
                $szLogFile = __APP_PATH__."/wsdl/response.xml";
                $strData=array();
                $strData[0] = print_R($response,true) ;
                filelog(true,$strData,$szLogFile);
                $this->iFedexApiError = 1 ;

                if(empty($szNotiFication))
                {
                    $szNotiFication = ' Internal Server Error. ';
                } 
                $this->szApiResponseText = "<br> <strong> Fedex API Notification: </strong> <br> ".$szNotiFication ;
                $szApiResponseLog .= $this->szApiResponseText ;
                $this->iSuccessMessage = 2 ;
                $this->szApiResponseLog = $szApiResponseLog ;
                return array();
            } 
        } catch (SoapFault $exception) {
   
            $fedexApiLogsAry = array();
            $fedexApiLogsAry['idUser'] = $kObject->id ;
            $fedexApiLogsAry['szRequestData'] = print_R($request,true);
            $fedexApiLogsAry['szResponseData'] = print_R($response,true);
            $this->addFedexApiLogs($fedexApiLogsAry);   

            $szNotiFication =  $response->Notifications->LocalizedMessage ; 
            if(empty($szNotiFication))
            {
                $szNotiFication = ' Internal Server Error. ';
            }		    	 
            
            $this->iFedexApiError = 1 ;
            $this->szApiResponseText = " <strong> Fedex API Error: </strong> <br> ".$szNotiFication ;
            $szApiResponseLog .= $this->szApiResponseText ;
            $this->iSuccessMessage = 3 ; 
            
            $this->szApiResponseLog = $szApiResponseLog ;
            return array();       
        }
    }  
    
    
    function validateInputNewDetails($data)
    {
        if(!empty($data))
        { 
            $this->set_szFirstName(trim(sanitize_all_html_input($data['szFirstName'])),false);
            $this->set_szLastName(trim(sanitize_all_html_input($data['szLastName'])),false);
            $this->set_szCity(trim(sanitize_all_html_input($data['szCity'])),false);
            $this->set_szState(trim(sanitize_all_html_input($data['szState'])),false);
            $this->set_szCountry(trim(sanitize_all_html_input($data['szCountry'])));
            $this->set_szAddress1(trim(sanitize_all_html_input($data['szAddress1'])),false);
            $this->set_szAddress2(trim(sanitize_all_html_input($data['szAddress2'])),false);
            $this->set_szAddress3(trim(sanitize_all_html_input($data['szAddress3'])));
            $this->set_szPostcode(trim(sanitize_all_html_input($data['szPostCode'])),false);

            $this->set_szSFirstName(trim(sanitize_all_html_input($data['szSFirstName'])),false);
            $this->set_szSLastName(trim(sanitize_all_html_input($data['szSLastName'])),false);
            $this->set_szSCity(trim(sanitize_all_html_input($data['szSCity'])),false);
            $this->set_szSState(trim(sanitize_all_html_input($data['szSState'])),false);
            $this->set_szSCountry(trim(sanitize_all_html_input($data['szSCountry'])));
            $this->set_szSAddress1(trim(sanitize_all_html_input($data['szSAddress1'])),false);
            $this->set_szSAddress2(trim(sanitize_all_html_input($data['szSAddress2'])),false);
            $this->set_szSAddress3(trim(sanitize_all_html_input($data['szSAddress3'])));
            $this->set_szSPostcode(trim(sanitize_all_html_input($data['szSPostCode'])),false);  
            $this->set_fCargoWeight(trim(sanitize_all_html_input($data['fCargoWeight'])));

            if($this->error==true)
            {
                return false;
            } 
            else
            {
                return true;
            }
        }
    }
    
    function calculateRatesFromUpsNewApi($data,$flag=false,$test_page=false)
    {  
        $this->validateInputNewDetails($data);  
        if($this->error==true)
        {
            return array();
        }    
        
        
        if($_SESSION['user_id']>0)
        { 
            $kUser->getUserDetails($_SESSION['user_id']); 
            $iPrivateShipping = $kUser->iPrivate; 
            $data['iPrivateShipping'] = $iPrivateShipping;
            $data['idCustomerCountry'] = $kUser->szCountry;
        } 
        else
        {
            $iPrivateShipping = $data['iPrivateShipping'];
            $data['idCustomerCountry'] = $data['idDestinationCountry'];
        }
        if((int)$data['iPrivateShipping']==0 && !$test_page)
        {
           

            $kVatApplication = new cVatApplication();            
            $fVatRate=$kVatApplication->getTransportecaVat($data); 
                
        }
        
        $seriverTypeArr=array("01"=>"Next Day Air","02"=>"2nd Day Air","03"=>"Ground","12"=>"3 Day Select","13"=>"Next Day Air Saver","14"=>"Next Day Air Early AM","59"=>"2nd Day Air AM",
            "07"=>"Worldwide Express","08"=>"Worldwide Expedited","11"=>"Standard","54"=>"Worldwide Express Plus","65"=>"UPS Saver. Required for Rating and Ignored for Shopping",
            "82"=>"UPS Today Standard","83"=>"UPS Today Dedicated Courier","84"=>"UPS Today Intercity","85"=>"UPS Today Express","86"=>"UPS Today Express Saver","96"=>"UPS Worldwide Express Freight");
        
        $validServiceTypeArr=array();
        $resArr=$this->getCourierServices($data,$flag); 
        if($data['szAPICode']!='')
        {
            $validServiceTypeArr=explode(";",$data['szAPICode']);
            
            $this->getValidServiceCodes($validServiceTypeArr);
            $apiCodeValuesAry = array();
            $validServiceTypeArr = array();

            $apiCodeValuesAry = $this->apiCodeValuesAry;
            $validServiceTypeArr = $this->validServiceTypeArr ;
            $validProviderIDAry = $this->validProviderIDAry;  
        }     
        
        $szApiResponseLog = $data['szForwarderCompanyName']." - UPS";
         
        $kUser = new cUser();
        $szAccountNumber=decrypt($data['szAccountNumber'],ENCRYPT_KEY);
        $szUsername=decrypt($data['szUsername'],ENCRYPT_KEY);
       
        $szPassword=decrypt($data['szPassword'],ENCRYPT_KEY);
        
        //ConfigurationUPS Express Saver 
        $access = $szAccountNumber;
        $userid = $szUsername;
        $passwd = $szPassword; 
        $wsdl = __APP_PATH__."/wsdl/RateWS.wsdl";
        $operation = "ProcessRate";
        $endpointurl = 'https://onlinetools.ups.com/webservices/Rate';
        $outputFileName = __APP_PATH__."/wsdl/XOLTResult.xml";
  
        if(!empty($data['dtTimingDate']))
        { 
            $dtShippingDate = $data['dtTimingDate'];
        }
        else
        { 
            $dtShippingDate = date('c');
        } 
        try
        { 
            $mode = array
            (
                'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
                'trace' => 1
            ); 
            // initialize soap client
            $client = new SoapClient($wsdl , $mode);
            //set endpoint url
            $client->__setLocation($endpointurl);
 
            //create soap header
            $usernameToken['Username'] = $userid;
            $usernameToken['Password'] = $passwd;
            $serviceAccessLicense['AccessLicenseNumber'] = $access;
            $upss['UsernameToken'] = $usernameToken;
            $upss['ServiceAccessToken'] = $serviceAccessLicense;
             
            try
            {
                $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0','UPSSecurity',$upss);
                $client->__setSoapHeaders($header);

                $request = $this->prepareUpsApiRequest($data,$flag,$test_page);   
                
                $resp = $client->__soapCall($operation ,array($request));    
            }
            catch (Exception $ex)
            {  
                $outputFileName = __APP_PATH__."/wsdl/ups_api_exception.log";
                 //save soap request and response to file
                $fw = fopen($outputFileName , 'w'); 
                fwrite($fw , "Exception: \n" . print_R($ex,true) . "\n");
                fclose($fw); 
            } 
            
            //get status
            $szResponseStatus = $resp->Response->ResponseStatus->Description;
            $szAlertCode = $resp->Response->ResponseStatus->Alert->Code;
            $szDescription = $resp->Response->ResponseStatus->Alert->Description;
            
            $this->szPackageDetailsLogs .= "<br> <strong> API Response:</strong> <br><br> Status: ".  strtoupper($szResponseStatus)."";
            $this->szPackageDetailsLogs .= " <br> Status code: ".$szAlertCode."";
            $this->szPackageDetailsLogs .= " <br> Description: ".$szDescription."<br><br>";
             
            $fedexApiLogsAry = array(); 
            $fedexApiLogsAry['iWebServiceType'] = 2 ;
            $fedexApiLogsAry['szRequestData'] = print_R($request,true);
            $fedexApiLogsAry['szResponseData'] = print_R($resp,true);
            $this->addFedexApiLogs($fedexApiLogsAry);
        
            // Logging UPS API request and response in xml file 
            $requestFileName = __APP_PATH__."/wsdl/ups_api_request_".date("Y-m-d").".xml";
            $responseFileName = __APP_PATH__."/wsdl/ups_api_response".date("Y-m-d").".xml";

            //save soap request and response to file
            $fw = fopen($requestFileName ,'w');
            fwrite($fw ,$client->__getLastRequest() . "\n");
            fclose($fw);

            $fw = fopen($responseFileName ,'w');
            fwrite($fw , $client->__getLastResponse() . "\n");
            fclose($fw);
                   
            if(strtolower($szResponseStatus)=='success')
            {
                $RatedShipmentAry = $resp->RatedShipment ;   
                $kWarehouseSearch = new cWHSSearch();
                $resultAry = $kWarehouseSearch->getCurrencyDetails(26);		
                
                $upsDeliveryDateAry = array();
                $upsDeliveryDateAry = $this->calculateShipmentTransitTimeUPS($data);
                $szApiResponseText = "<br><br><strong><u>UPS Transit API Response:</u></strong><br>".$this->szApiResponseText;
                 
                $fInrExchangeRate = 0;
                if($resultAry['fUsdValue']>0)
                {
                    $fInrExchangeRate = $resultAry['fUsdValue'] ;						 
                }
                $iNumDaysAddedToDelivery = $kWarehouseSearch->getManageMentVariableByDescription('__NUMBER_OF_DAYS_ADDED_TO_DELIVERY_DATE__');
                
                $courierProductMapping = array();
                $courierProductMapping = $this->getAllProductByApiCodeMapping();
                 
                $szReturnStr =  '<h4>Your Rates: </h4><table class="format-2" style="width:90%">';
                $szReturnStr .= '<tr><td><strong>Service Type</strong></td><td><strong>Amount</strong></td><td><strong>Shipping Date</strong></td><td><strong>Delivery Date</strong></td></tr>';
                $ctr=0;
                $szApiResponseLog .= $szApiResponseText;
                $szApiResponseLog .= "<br><strong><u>Rate API response:</u></strong>";
                if(!empty($RatedShipmentAry))
                {
                    foreach($RatedShipmentAry as $RatedShipmentArys)
                    {  
                        $code = $RatedShipmentArys->Service->Code;
                        if($test_page)
                        {
                            $iTransitDays = $RatedShipmentArys->GuaranteedDelivery->BusinessDaysInTransit ;
                            $TotalNetCharge = $RatedShipmentArys->TotalCharges->MonetaryValue;
                            $dtShipmentTime = $RatedShipmentArys->GuaranteedDelivery->DeliveryByTime;
                            $szCurrencyCode = $RatedShipmentArys->TotalCharges->CurrencyCode ;
                            $idServiceCode = $RatedShipmentArys->Service->Code;

                            $szNegotiableCurrencyCode = $RatedShipmentArys->NegotiatedRateCharges->TotalCharge->CurrencyCode ;
                            $fNegotiableTotalCharge = $RatedShipmentArys->NegotiatedRateCharges->TotalCharge->MonetaryValue ;

                            if($fNegotiableTotalCharge>0)
                            {
                                $TotalNetCharge = $fNegotiableTotalCharge;
                                $szCurrencyCode = $szNegotiableCurrencyCode;
                            }

                            if($TotalNetCharge>0 && $code!=54)
                            {
                                if($iTransitDays>0)
                                {
                                    if(empty($dtShipmentTime))
                                    {
                                        $dtShipmentTime = "By EOD";
                                    } 
                                    $szTransitDays = date('Y-m-d',strtotime("+".($iTransitDays)." DAYS"))." ".$dtShipmentTime ;
                                }
                                else
                                { 
                                    $szTransitDays = 'N/A';
                                }  
                                $fShipmentCost = number_format($TotalNetCharge,2,".",",") ;
                                $serviceType = '<tr><td>'.$seriverTypeArr[$idServiceCode].'</td>';
                                $amount = '<td>'.$szCurrencyCode." " . $fShipmentCost . '</td>';
                                $shippingDate = '<td>'.date('Y-m-d H:i:s') . '</td>';
                                $deliveryDate =  '<td>'.$szTransitDays. '</td>';

                                $szReturnStr .= $serviceType . $amount.$shippingDate. $deliveryDate;
                                $szReturnStr .= '</tr>'; 
                            }
                        }
                        else
                        { 
                            if(!empty($validServiceTypeArr) && in_array($code,$validServiceTypeArr))
                            {   
                                $idCourierProviderProductid = 0;
                                if(!empty($validProviderIDAry[$code]))
                                {
                                    $idCourierProviderProductid = $validProviderIDAry[$code];
                                }
                                $pricingArr=$this->getCourierProductProviderPricingData($data,$code,$idCourierProviderProductid);
                                
                                $iTransitDays = 0; 
                                $iServiceCode = $code;

                                /*
                                 * 
                                 */ 
                                $iTransitDays = $RatedShipmentArys->GuaranteedDelivery->BusinessDaysInTransit ;

                                if($iTransitDays<=0 && !empty($upsDeliveryDateAry[$iServiceCode]))
                                {
                                    $iTransitDays = $upsDeliveryDateAry[$iServiceCode]['iBusinessDaysInTransit'] ; 
                                } 
                                if($iTransitDays<=0)
                                {
                                    $iTransitDays = $courierProductMapping[$code]['iDaysStd'];  
                                } 

                                $TotalNetCharge = $RatedShipmentArys->TotalCharges->MonetaryValue;
                                $dtShipmentTime = $RatedShipmentArys->GuaranteedDelivery->DeliveryByTime;
                                $szCurrencyCode = $RatedShipmentArys->TotalCharges->CurrencyCode ;

                                $szNegotiableCurrencyCode = $RatedShipmentArys->NegotiatedRateCharges->TotalCharge->CurrencyCode ;
                                $fNegotiableTotalCharge = $RatedShipmentArys->NegotiatedRateCharges->TotalCharge->MonetaryValue ;

                                if($fNegotiableTotalCharge>0)
                                {
                                    $TotalNetCharge = $fNegotiableTotalCharge;
                                    $szCurrencyCode = $szNegotiableCurrencyCode;
                                }

                                if($szCurrencyCode=='RMB')
                                {
                                    $szCurrencyCode='CNY';
                                }

                                $szApiResponseLog .=" <br><br><strong> Shipping Type: ".$seriverTypeArr[$code]."</strong>";
                                $szApiResponseLog .=" <br> Shipment Price: ".$szCurrencyCode." ".number_format((float)$TotalNetCharge,2);

                                if(!empty($upsDeliveryDateAry[$code]))
                                {    
                                    $szApiResponseLog .=" <br> Pickup date: ".$upsDeliveryDateAry[$code]['dtPickupDate'] ; 
                                    $szApiResponseLog .=" <br> Business Days In Transit: ".$upsDeliveryDateAry[$code]['iBusinessDaysInTransit'] ;
                                    $szApiResponseLog .=" <br> Total Days In Transit: ".$upsDeliveryDateAry[$code]['iTotalTransitDays'] ;
                                    $szApiResponseLog .=" <br> Arrival Date: ".$upsDeliveryDateAry[$code]['dtArrivalDate'] ;
                                    $szApiResponseLog .=" <br> Days of Week: ".$upsDeliveryDateAry[$code]['szDayOfWeek'] ; 
                                }
                                else
                                {  
                                    $szApiResponseLog .=" <br> Shipping date: ".$dtShippingDate ;
                                }

                                if(strtolower($szCurrencyCode)=='usd')
                                {    
                                    $fExchangeRate='1.000';
                                }
                                else
                                {
                                     $fExchangeRate=$this->getExchangeRateCourierPrice($szCurrencyCode);
                                }    
                                if((float)$fExchangeRate>0)
                                {
                                    if($TotalNetCharge>0)
                                    {
                                        if($iTransitDays>0)
                                        {
                                            if(empty($dtShipmentTime))
                                            {
                                               // $dtShipmentTime = "By EOD";
                                            }  
                                            $iTransitDays=$iTransitDays;
                                            $szTransitDays = getBusinessDaysForCourierDeliveryDate($dtShippingDate, $iTransitDays)." ".$dtShipmentTime ;
                                        }
                                        else
                                        {  
                                            $szTransitDays = $dtShippingDate;
                                        }
                                        $szApiResponseLog .=" <br> Ship date: ".$dtShippingDate ;
                                        $szApiResponseLog .=" <br> Days: ".$iTransitDays ;
                                        $szApiResponseLog .=" <br> Delivery date: ".$szTransitDays ; 
                                        $szApiResponseLog .=" <br> <br> Rate calculation: " ;

                                        $fTotalShipmentCost = 0;
                                        if($fInrExchangeRate>0)
                                        {
                                            $fTotalShipmentCost = round((float)($TotalNetCharge * $fInrExchangeRate)); 
                                        }
                                        $fShipmentCost = $TotalNetCharge;

                                        if($szCurrencyCode==__TO_CURRENCY_CONVERSION__)
                                        {
                                            $fShipmentAPIUSDCost =$fShipmentCost;
                                        }
                                        else
                                        { 
                                            $fExchangeRate=$this->getExchangeRateCourierPrice($szCurrencyCode); 
                                            $fExchangeRate = 1/$fExchangeRate; 
                                            $fShipmentAPIUSDCost =$fShipmentCost/$fExchangeRate; 
                                        }

                                        $szApiResponseLog .= "<br>Buy rate - ".$szCurrencyCode." ".number_format((float)$TotalNetCharge,2);
                                        $szApiResponseLog .= "<br>ROE - ".round($fExchangeRate,4) ;
                                        $szApiResponseLog .= "<br>Base currency - USD ".$fShipmentAPIUSDCost ; 

                                        $idForwarderCurrency = $data['idForwarderCurrency'];
                                        $miniMarkUpRate = $pricingArr[0]['fMinimumMarkup'];
                                        $fMarkupperShipment = $pricingArr[0]['fMarkupperShipment']; 

                                        $markUpByRate = $fShipmentAPIUSDCost * .01 * $pricingArr[0]['fMarkupPercent']; 

                                        if($idForwarderCurrency==1)
                                        { 
                                            $fMarkupperShipmentUSD = $fMarkupperShipment ;
                                            $miniMarkUpRateUSD = $miniMarkUpRate ;
                                            $szForwarderCurrency = 'USD';
                                        }
                                        else
                                        {
                                            $resultAry = $kWarehouseSearch->getCurrencyDetails($idForwarderCurrency);	  
                                            $szForwarderCurrency = $resultAry['szCurrency'];
                                            $fForwarderExchangeRate = $resultAry['fUsdValue'];

                                            $fMarkupperShipmentUSD = $fMarkupperShipment * $fForwarderExchangeRate ;
                                            $miniMarkUpRateUSD = $miniMarkUpRate * $fForwarderExchangeRate ; 
                                        }

                                        if($miniMarkUpRateUSD>$markUpByRate)
                                        {
                                            $fTotalMarkup = $miniMarkUpRateUSD ;
                                        }
                                        else
                                        {
                                           $fTotalMarkup =  $markUpByRate ;
                                        }
                                        $fPriceAfterMarkUp = $fShipmentAPIUSDCost + $fTotalMarkup ;

                                        $szApiResponseLog .= "<br>Markup (".$pricingArr[0]['fMarkupPercent']."%, minimum USD ".$miniMarkUpRateUSD.") – USD ".$fTotalMarkup;
                                        $szApiResponseLog .= "<br> Additional fixed markup – USD ".$fMarkupperShipmentUSD ;


                                        $fBookingprice=0;
                                        if((int)$pricingArr[0]['iBookingIncluded']!=1)
                                        { 
                                            if($data['iBookingPriceCurrency']==1)
                                            {  
                                                $iBookingPriceCurrency = 1;  
                                                $iBookingPriceCurrencyROE = 1;
                                                $szBookingPriceCurrency = 'USD';
                                                $fBookingprice = $data['fPrice']; 
                                            }
                                            else
                                            {
                                                $resultAry = $kWarehouseSearch->getCurrencyDetails($data['iBookingPriceCurrency']); 
                                                $iBookingPriceCurrencyROE = $resultAry['fUsdValue']; 
                                                $szBookingPriceCurrency = $resultAry['szCurrency']; 
                                                $iBookingPriceCurrency = $resultAry['idCurrency']; 
                                                if($iBookingPriceCurrencyROE>0)
                                                {
                                                    $fBookingprice = $data['fPrice']*$iBookingPriceCurrencyROE;
                                                }
                                                else
                                                {
                                                    $fBookingprice = 0;
                                                }
                                            }  
                                            $szApiResponseLog .= " <br> Booking and label fee for Transporteca: ".$szBookingPriceCurrency." ".$data['fPrice']."" ;
                                            $szApiResponseLog .= " <br> Booking and label fee USD: ".$fBookingprice ;
                                            $szApiResponseLog .= " <br> Booking and label fee exchange rate: ".$iBookingPriceCurrencyROE ;
                                        }  
                                        else
                                        {
                                            $szApiResponseLog .= " <br> Booking and label fee for Transporteca: No ";
                                        }
                                        $fShipmentUSDCost = $fPriceAfterMarkUp + $fMarkupperShipmentUSD + $fBookingprice;  
                                        $fMarkUpRefferalFee=$fShipmentUSDCost*.01*$data['fReferalFee'];

                                        if($data['iProfitType']==2)
                                        { 
                                            $subTotalForRefferalFee=$fShipmentUSDCost-$fBookingprice;
                                            $fMarkUpRefferalFee = ((0.01 * $data['fReferalFee'])/( 1 - (0.01*$data['fReferalFee'])) ) * $subTotalForRefferalFee;
                                            $fShipmentUSDCost = $fShipmentUSDCost + $fMarkUpRefferalFee ;
                                            $szApiResponseLog .= " <br> GP Type: Mark-up ";
                                            $szApiResponseLog .= " <br> GP Amount: USD ".($fMarkUpRefferalFee+$fBookingprice);
                                        }
                                        else
                                        {
                                            $subTotalForRefferalFee=$fShipmentUSDCost-$fBookingprice;
                                            $fMarkUpRefferalFee = $subTotalForRefferalFee*.01*$data['fReferalFee'];
                                            $szApiResponseLog .= " <br> GP Type: Referral ";
                                            $szApiResponseLog .= " <br> GP Amount: USD ".($fMarkUpRefferalFee+$fBookingprice);
                                        } 

                                        $szApiResponseLog .= "<br> Total sales price without VAT: USD ".number_format((float)$fShipmentUSDCost,2);
                                        $szApiResponseLog .= "<br><br> API Delivery date ".$szTransitDays;
                                        $szApiResponseLog .= "<br> Days to be added in Delivery date ".$iNumDaysAddedToDelivery;
                                        
                                        $serviceType =$szServiceType;
                                        if($iNumDaysAddedToDelivery>0)
                                        {
                                            $deliveryDate = date('Y-m-d H:i:s',strtotime($szTransitDays . "+".$iNumDaysAddedToDelivery." days"));

                                            $timeStamp=date('H:i:s',strtotime($deliveryDate));
                                            $dayName=date('D',strtotime($deliveryDate));

                                            if($dayName=='Sun' || $dayName=='Sat')
                                            {
                                               $deliveryDate = getBusinessDaysForCourierDeliveryDate($deliveryDate, 1)." ".$timeStamp;
                                            }
                                        }
                                        else
                                        {
                                            $deliveryDate = date('Y-m-d H:i:s',strtotime($szTransitDays)); 
                                        }  

                                        $szApiResponseLog .= "<br>Delivery date ".$deliveryDate;

                                        if((int)$_SESSION['user_id']>0)
                                        {
                                            $kUser = new cUser();
                                            $kUser->getUserDetails($_SESSION['user_id']);
                                            //print_r($kUser);
                                            if($kUser->szCurrency==1)
                                            {
                                                $fVatRateAmount = round((float)$fShipmentUSDCost)*0.01*$fVatRate;   
                                                $fShipmentVatUSDCost = $fShipmentUSDCost+$fVatRateAmount;

                                                $szApiResponseLog .= "<br>VAT Percentage:".$fVatRate."%"; 
                                                $szApiResponseLog .= "<br>VAT Amount: USD ".round($fVatRateAmount,2);

                                            }
                                            else
                                            {
                                                    $szApiResponseLog .= "<br>VAT Percentage:".$fVatRate."%";
                                                    $resultAry = $kWarehouseSearch->getCurrencyDetails($kUser->szCurrency); 
                                                    $iVatPriceCurrencyROE = $resultAry['fUsdValue']; 
                                                    $szVatPriceCurrency = $resultAry['szCurrency']; 
                                                    $iVatPriceCurrency = $resultAry['idCurrency']; 
                                                    if($iVatPriceCurrencyROE>0)
                                                    {
                                                        $fVatCalAmuount = $fShipmentUSDCost/$iVatPriceCurrencyROE;
                                                    }
                                                    else
                                                    {
                                                        $fVatCalAmuount = 0;
                                                    }

                                                    $fVatRateAmount=$fVatCalAmuount*0.01*$fVatRate;

                                                    $fVatRateAmountUSD=$fVatRateAmount*$iVatPriceCurrencyROE;
                                                    $fShipmentVatUSDCost=$fShipmentUSDCost+$fVatRateAmountUSD;
                                                   // echo $fShipmentAPIUSDCost;
                                                    $szApiResponseLog .= "<br>VAT Amount: USD ".round($fVatRateAmountUSD,2);

                                                }

                                                $szApiResponseLog .= "<br> Total sales price including VAT: USD ".number_format((float)$fShipmentVatUSDCost,2);
                                        }

                                        $ret_ary[$ctr]['dtAvailableDate'] = $deliveryDate ;
                                        $ret_ary[$ctr]['szCurrencyCode'] = $szCurrencyCode ;
                                        $ret_ary[$ctr]['fShippingAmount'] = $fShipmentCost ;
                                        $ret_ary[$ctr]['szServiceType'] =  $code ; 
                                        $ret_ary[$ctr]['fShipmentAPIUSDCost'] =  round($fShipmentAPIUSDCost,2) ; 
                                        $ret_ary[$ctr]['fShipmentUSDCost'] =  round($fShipmentUSDCost,2) ; 
                                        $ret_ary[$ctr]['fDisplayPrice'] =  round($fShipmentCustomerCost,2) ; 
                                        $ret_ary[$ctr]['szCurrency'] =  $szCurrency ;
                                        $ret_ary[$ctr]['idCurrency'] =  $idCurrency ;  
                                        $ret_ary[$ctr]['markUpByRate'] =  $fTotalMarkup ; 
                                        $ret_ary[$ctr]['miniMarkUpRate'] = $miniMarkUpRate;
                                        $ret_ary[$ctr]['fPriceAfterMarkUp']=$fPriceAfterMarkUp;   
                                        $ret_ary[$ctr]['fMarkupperShipment']=$fMarkupperShipment; 
                                        $ret_ary[$ctr]['fMarkUpRefferalFee']=$fMarkUpRefferalFee;
                                        $ret_ary[$ctr]['fBookingprice']=$fBookingprice;
                                        $ret_ary[$ctr]['fReferalPercentage'] = $data['fReferalFee'] ; 
                                        $ret_ary[$ctr]['idForwarderCurrency'] = $idForwarderCurrency;
                                        $ret_ary[$ctr]['iBookingIncluded'] = $pricingArr[0]['iBookingIncluded'];
                                        ++$ctr;
                                    }
                                }
                                else
                                {
                                    $kConfig = new cConfig();
                                    $kConfig->loadCountry(false,$data['szCountry']);
                                    $countryFrom=$kConfig->szCountryName;

                                    $kConfig->loadCountry(false,$data['szSCountry']);
                                    $countryTo=$kConfig->szCountryName;

                                    $replace_ary['zsAPIName']="UPS";
                                    $replace_ary['szCurrencyCode']=$szCurrencyCode;
                                    $replace_ary['fromCountry']=$countryFrom;
                                    $replace_ary['toCountry']=$countryTo;
                                    $replace_ary['supportEmail'] = __STORE_SUPPORT_EMAIL__;

                                    createEmail(__CURRENCY_DO_NOT_MATCH__, $replace_ary,__STORE_SUPPORT_EMAIL__, '', __STORE_SUPPORT_EMAIL__,'', __STORE_SUPPORT_EMAIL__,__FLAG_FOR_DEFAULT__);

                                }
                            } 
                        }
                    } 
                    if($test_page)
                    {
                        $szReturnStr .= '</table>'; 
                        $this->szApiResponseText = $szReturnStr ;
                        $this->iSuccessMessage = 1 ;
                        $this->szApiResponseLog = $szApiResponseLog ; 
                        return true;
                    }
                    else
                    {
                        $this->szApiResponseText = $szReturnStr ;
                        $this->iSuccessMessage = 1 ;
                        $this->szApiResponseLog = $szApiResponseLog ; 
                        return $ret_ary;
                    } 
                }
            }
            else
            {
                $this->iFedexApiError = 1 ;
                $this->szApiResponseText = " <strong> UPS API Error: </strong> <br> No service available" ;
                $this->iSuccessMessage = 3 ; 
                $szApiResponseLog .= $this->szApiResponseText ; 
                $this->szApiResponseLog = $szApiResponseLog ;
                return array();
            }
            
             //save soap request and response to file
            $fw = fopen($outputFileName , 'w'); 
            fwrite($fw , "Response: \n" . print_R($resp,true) . "\n");
            fclose($fw); 
        }
        catch(Exception $ex)
        {  
            $this->iFedexApiError = 1 ;
            $this->szApiResponseText = " <strong> UPS API Error: </strong> <br> ".$ex->getMessage() ;
            $this->iSuccessMessage = 3 ;  
            $szApiResponseLog .= $this->szApiResponseText ;  
            $this->szApiResponseLog = $szApiResponseLog ;
            return array();
        }
    } 
    function getExchangeRateCourierPrice($szCurrencyCode)
    {
    	$query="
            SELECT
                ec.id,
            	ec.fUsdValue
            FROM
            	".__DBC_SCHEMATA_CURRENCY_EXCHANGE_RATE__." AS ec
            INNER JOIN
            	 ".__DBC_SCHEMATA_CURRENCY__." AS c
            ON
            	ec.idCurrency=c.id
            WHERE
            	LOWER(c.szCurrency) = '".mysql_escape_custom(strtolower($szCurrencyCode))."' 	
            ORDER BY
                dtDate DESC
            LIMIT
                0,1
        ";   
        //echo $query;
    	if( ( $result = $this->exeSQL( $query ) ) )
        {
            if($this->getRowCnt()>0)
            {
            	$ret_ary = array();
                $ctr = 0;
                $row = $this->getAssoc($result); 
                if($row['id']>0)
                {
                    $this->iCurrencyCodeExists = 1;
                    return $row['fUsdValue']; 
                }
                else
                {
                    $this->iCurrencyCodeExists = false;
                    return false; 
                } 
            }
        } 
    } 
    
    function saveSearchCourierProviderData($data,$idBookingNumber)
    { 
    	$query="
            DELETE FROM
                ".__DBC_SCHEMATA_COUIER_SEARCH_DATA__."
            WHERE	
                idBooking='".mysql_escape_custom($idBookingNumber)."'	
    	";
    	$result = $this->exeSQL( $query );
    	
    	$query="
    		INSERT INTO
                    ".__DBC_SCHEMATA_COUIER_SEARCH_DATA__."
    		(
                    idBooking,
                    szTextSearch,
                    dtSearch
    		)
                VALUES
    		(
                    '".mysql_escape_custom($idBookingNumber)."',
                    '".mysql_escape_custom(serialize($data))."',
                    NOW()
    		)
    	";
    	$result = $this->exeSQL( $query );
    }
    
    function gettotalBookingForProivder($idProvider)
    {
    	$iNumBookingReceived=0;
    	$query="
    		SELECT	
                    COUNT(id) iNumBookingReceived,
                    dtBookingConfirmed
                FROM
                    ".__DBC_SCHEMATA_BOOKING__." as b1
                WHERE 
                    date_format(dtBookingConfirmed,'%Y-%m') > '".date('Y-m',strtotime("-365 DAY"))."'
                AND 
                    idBookingStatus IN(3,4) 
                AND
                    iTransferConfirmed = '0'
                AND
                	idServiceProvider='".(int)$idProvider."'     
                ORDER BY
                    dtBookingConfirmed ASC
    	";
    	//echo $query."<br/><br/>";
    	if( ( $result = $this->exeSQL( $query ) ) )
		{
        	if( $this->getRowCnt() > 0 )
            {
            	
                $row=$this->getAssoc($result);
                
                $iNumBookingReceived= $row['iNumBookingReceived'];
                 
                
            }
		} 
		$fTotalPriceUSD=0;
		
		$query="
    		SELECT	
                    sum(fTotalPriceUSD) fTotalPriceUSD,
                    dtBookingConfirmed
                FROM
                    ".__DBC_SCHEMATA_BOOKING__." as b1
                WHERE 
                    date_format(dtBookingConfirmed,'%Y-%m') > '".date('Y-m',strtotime("-365 DAY"))."'
                AND 
                    idBookingStatus IN(3,4) 
                AND
                    iTransferConfirmed = '0'
                AND
                	idServiceProvider='".(int)$idProvider."'     
                ORDER BY
                    dtBookingConfirmed ASC
    	";
    	//echo $query."<br/><br/>";
    	if( ( $result = $this->exeSQL( $query ) ) )
		{
        	if( $this->getRowCnt() > 0 )
            {
            	
                $row=$this->getAssoc($result);
                
                $fTotalPriceUSD= $row['fTotalPriceUSD'];
                 
                
            }
		} 
		
		$res_arr['fTotalPriceUSD']=$fTotalPriceUSD;
		$res_arr['iNumBookingReceived']=$iNumBookingReceived;
		return $res_arr;
    	
    }
    /* 
    function getAllNewBookingWithCourier($idBooking=0,$flag=__LABEL_NOT_SENT__,$count_rows=false,$header_notification=false)
    {
        if($flag==__LABEL_SENT__)
        {
            $query_and = " b.idBookingStatus IN ('3','4') AND  b.idServiceProvider>0  AND fl.szTransportecaStatus='S8'";

            if($header_notification)
            {
                $query_and .= " AND DATE(b.dtLabelSent) < '".date('Y-m-d',strtotime('-2 DAYS'))."' ";
            }
        }
        else if($flag==__LABEL_DOWNLOAD__)
        {
            $query_and = " b.idBookingStatus IN ('3','4') AND
                b.idServiceProvider>0 AND fl.szTransportecaStatus='S9'"; 
        }
        else
        {
            $query_and = " b.idBookingStatus IN ('3','4') AND
                b.idServiceProvider>0 AND (fl.szTransportecaStatus='S7' || (fl.szForwarderBookingTask = 'T140' && fl.szTransportecaStatus!='S8' && fl.szTransportecaStatus!='S9'))";

            if($header_notification)
            {
                $query_and .= " AND DATE(b.dtBookingConfirmed) < '".date('Y-m-d',strtotime('-2 DAYS'))."' ";
            }
        } 
        if($count_rows)
        {
            $query_select = " count(b.id) iNumBookings ";
        }
        else
        {
            $query_select = " 
                b.id,
                b.dtBookingConfirmed,
                b.szBookingRef,
                b.idServiceType,
                b.szWarehouseFromCity,
                b.szWarehouseToCity,
                b.fCargoVolume,
                b.fCargoWeight,
                b.idForwarder,
                f.szDisplayName,
                b.idWarehouseFromCountry,
                b.idWarehouseToCountry,
                b.szForwarderCurrency,
                b.fTotalPriceForwarderCurrency,
                b.iOriginCC,
                b.iDestinationCC,
                b.dtBookingCancelled,
                b.fTotalPriceUSD as fBookingPriceUSD,
                b.szCustomerCurrency,
                b.fTotalPriceCustomerCurrency,
                b.dtCutOff,
                b.dtWhsCutOff,
                b.iBookingCutOffHours,
                b.iBookingLanguage,
                b.iQuotesStatus,
                b.idOriginCountry,
                b.idDestinationCountry,
                b.szTransportMode,
                b.idBookingStatus,
                (SELECT sp1.szShipperCity FROM ".__DBC_SCHEMATA_SHIPPER_CONSIGNEE__. " sp1 WHERE sp1.idBooking = b.id ) as szShipperCity,
                (SELECT sp2.szConsigneeCity FROM ".__DBC_SCHEMATA_SHIPPER_CONSIGNEE__. " sp2 WHERE sp2.idBooking = b.id ) as szConsigneeCity,
                cp.szName as szProviderName,
                cpp.szName as szProviderProductName,
                ca.iBookingIncluded,
                b.szTrackingNumber,
                b.dtLabelSent,
                b.dtLabelDownloaded,
                b.idLabelDownloadedBy,
                cbd.iCourierAgreementIncluded,
                cp.id AS idProvider
            ";
        }
        $queryWhere='';
        if((int)$idBooking>0)
        {
            $query_and = "b.idServiceProvider>0 ";
            $queryWhere="
            AND
                b.id='".(int)$idBooking."'	

            ";
        }
                
        $query = "
            SELECT
                $query_select
            FROM
                ".__DBC_SCHEMATA_BOOKING__." as b
            INNER JOIN
                ".__DBC_SCHEMATA_FROWARDER__." as f
            ON
                f.id = b.idForwarder
            INNER JOIN
                ".__DBC_SCHEMATA_COUIER_AGREEMENT_DATA__." as ca
            ON
                ca.id = b.idServiceProvider
            INNER JOIN
                ".__DBC_SCHEMATA_COUIER_PROVIDER__." as	cp
            ON
                cp.id = ca.idCourierProvider
            INNER JOIN
                ".__DBC_SCHEMATA_COUIER_PROVIDER_PRODUCTS__." as cpp
            ON
                cpp.id = b.idServiceProviderProduct
             INNER JOIN
                ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__." as cbd
            ON
                cbd.idBooking = b.id    
            INNER JOIN
                ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." as ft
            ON
                ft.idBooking = b.id
            LEFT JOIN
                ".__DBC_SCHEMATA_FILE_LOGS__." AS fl
            ON        
                fl.id=b.idFile
                ".$inner_query."		
            WHERE
                $query_and
                $queryWhere 
            ORDER BY
                b.dtBookingConfirmed DESC	 
        ";
         //echo $query;
         //die;
        if($result=$this->exeSQL($query))
        {
            if($this->iNumRows>0)
            {
                if($count_rows)
                {
                    $row = $this->getAssoc($result);
                    return $row['iNumBookings'];
                }
                else
                {
                    $bookingAry = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {
                        $bookingAry[$ctr]=$row;
                        $ctr++;
                    }
                    return $bookingAry;
                }
            } 
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
     * 
     */
    function getAllNewBookingWithCourier($idBooking=0,$flag=__LABEL_NOT_SENT__,$count_rows=false,$header_notification=false)
    {
        /*
         * Backup of this function is named as getAllNewBookingWithCourier_backupp() in same class cCourierService
         */
        $kLabels = new cLabels();
        $bookingAry = array();
        $bookingAry = $kLabels->getAllNewBookingWithCourier($idBooking,$flag,$count_rows,$header_notification);
        return $bookingAry;
    }
    
    function getAllNewBookingWithCourier_backupp($idBooking=0,$flag=__LABEL_NOT_SENT__,$count_rows=false,$header_notification=false)
    { 
        if($flag==__LABEL_SENT__)
        {
            $query_and = " b.idBookingStatus IN ('3','4') AND  (b.idServiceProvider>0 OR isManualCourierBooking=1)  AND fl.iLabelStatus=".__LABEL_SEND_FLAG__."";

            if($header_notification)
            {
                $query_and .= " AND DATE(b.dtLabelSent) < '".date('Y-m-d',strtotime('-1 DAYS'))."' AND b.dtSnooze < '".date('Y-m-d')."' ";
            }
        }
        else if($flag==__LABEL_DOWNLOAD__)
        {
            $query_and = " b.idBookingStatus IN ('3','4') AND
                (b.idServiceProvider>0 OR isManualCourierBooking=1) AND fl.iLabelStatus=".__LABEL_DOWNLOAD_FLAG__.""; 
        }
        else
        {
            $query_and = " b.idBookingStatus IN ('3','4') AND
                (b.idServiceProvider>0 OR isManualCourierBooking=1)";

            if($header_notification)
            {
                $query_and .= " AND b.dtBookingConfirmed < '".date('Y-m-d H:i:s',strtotime('-24 HOURS'))."' AND b.dtSnooze < '".date('Y-m-d')."'";
            }
            
            $query_and .= " AND (fl.iLabelStatus=".__LABEL_NOT_SEND_FLAG__." || (fl.szForwarderBookingTask = 'T140' && fl.iLabelStatus!=".__LABEL_SEND_FLAG__." && fl.iLabelStatus!=".__LABEL_DOWNLOAD_FLAG__."))";
        } 
        if($header_notification)
        {
            $query = "
                SELECT
                    count(b.id) iNumBookings
                FROM
                    ".__DBC_SCHEMATA_BOOKING__." as b
                LEFT JOIN
                    ".__DBC_SCHEMATA_FILE_LOGS__." AS fl
                ON        
                    fl.id=b.idFile
                    ".$inner_query."		
                WHERE
                    $query_and
                    $queryWhere 
                ORDER BY
                    b.dtBookingConfirmed DESC	 
            ";
        }
        else
        {        
            if($count_rows)
            {
                $query_select = " count(b.id) iNumBookings ";
            }
            else
            {
                $query_select = " 
                    b.id,
                    b.dtBookingConfirmed,
                    b.szBookingRef,
                    b.idServiceType,
                    b.szWarehouseFromCity,
                    b.szWarehouseToCity,
                    b.fCargoVolume,
                    b.fCargoWeight,
                    b.idForwarder,
                    b.iSendTrackingUpdates,
                    b.szForwarderDispName as szDisplayName,
                    b.idWarehouseFromCountry,
                    b.idWarehouseToCountry,
                    b.szForwarderCurrency,
                    b.fTotalPriceForwarderCurrency,
                    b.iOriginCC,
                    b.iDestinationCC,
                    b.dtBookingCancelled,
                    b.fTotalPriceUSD as fBookingPriceUSD,
                    b.szCustomerCurrency,
                    b.fTotalPriceCustomerCurrency,
                    b.dtCutOff,
                    b.dtWhsCutOff,
                    b.iBookingCutOffHours,
                    b.iBookingLanguage,
                    b.iQuotesStatus,
                    b.idOriginCountry,
                    b.idDestinationCountry,
                    b.szTransportMode,
                    b.idBookingStatus,
                    (SELECT sp1.szShipperCity FROM ".__DBC_SCHEMATA_SHIPPER_CONSIGNEE__. " sp1 WHERE sp1.idBooking = b.id ) as szShipperCity,
                    (SELECT sp2.szConsigneeCity FROM ".__DBC_SCHEMATA_SHIPPER_CONSIGNEE__. " sp2 WHERE sp2.idBooking = b.id ) as szConsigneeCity,
                    cp.szName as szProviderName,
                    cpp.szName as szProviderProductName,
                    ca.iBookingIncluded,
                    b.szTrackingNumber,
                    b.dtLabelSent,
                    b.dtLabelDownloaded,
                    b.idLabelDownloadedBy,
                    cbd.iCourierAgreementIncluded,
                    cp.id AS idProvider,
                    b.szBookingRandomNum,
                    b.dtSnooze
                ";
            }
            $queryWhere='';
            if((int)$idBooking>0)
            {
                $query_and = "(b.idServiceProvider>0 OR isManualCourierBooking=1) ";
                $queryWhere="
                AND
                    b.id='".(int)$idBooking."'	

                ";
            }

            $query = "
                SELECT
                    $query_select
                FROM
                    ".__DBC_SCHEMATA_BOOKING__." as b 
                LEFT JOIN
                    ".__DBC_SCHEMATA_COUIER_AGREEMENT_DATA__." as ca
                ON
                    ca.id = b.idServiceProvider
                LEFT JOIN
                    ".__DBC_SCHEMATA_COUIER_PROVIDER__." as	cp
                ON
                    cp.id = ca.idCourierProvider
                LEFT JOIN
                    ".__DBC_SCHEMATA_COUIER_PROVIDER_PRODUCTS__." as cpp
                ON
                    cpp.id = b.idServiceProviderProduct
                LEFT JOIN
                    ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__." as cbd
                ON
                    cbd.idBooking = b.id 
                LEFT JOIN
                    ".__DBC_SCHEMATA_FILE_LOGS__." AS fl
                ON        
                    fl.id=b.idFile
                    ".$inner_query."		
                WHERE
                    $query_and
                    $queryWhere 
                ORDER BY
                    b.dtBookingConfirmed DESC	 
            ";
        }
         //echo $query;
         //die;
        if($result=$this->exeSQL($query))
        {
            if($this->iNumRows>0)
            {
                if($count_rows)
                {
                    $row = $this->getAssoc($result);
                    return $row['iNumBookings'];
                }
                else
                {
                    $bookingAry = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {
                        $bookingAry[$ctr]=$row;
                        $ctr++;
                    }
                    return $bookingAry;
                }
            } 
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
	function getIDFileBuBookingId($idBooking)
	{
			if($idBooking>0)
            { 
                $query="
                    SELECT 
                        f.id
                    FROM
                        ".__DBC_SCHEMATA_FILE_LOGS__." AS f 
                    WHERE
                       f.idBooking = '".(int)$idBooking."'
                ";
                //echo $query;
                if($result = $this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    { 
                        $row=$this->getAssoc($result); 
                        
                        return $row['id'] ;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
	}
	
	function updateMasterTrackingNumber($szTrackingNumber,$idBooking,$iSendTrackingUpdates,$trackingUpdate=false)
	{
            $this->set_szTrackingNumber(trim(sanitize_all_html_input($szTrackingNumber))); 
            if($this->error==true)
            {
                return false;
            }  
            if($trackingUpdate == false)
            {
                $kTracking = new cTracking(); 
                $trackingArY = $kTracking->createAfterShipTracking($idBooking,$this->szTrackingNumber);
            }
              
            if($kTracking->tracking_error)
            {
                //$this->addError('szAfterShipTrackingNotification',"API ERROR: ".$kTracking->szAfterShipTrackingNotification);
                //return false;
            }
            
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_BOOKING__."
                SET
                    szTrackingNumber='".mysql_escape_custom($this->szTrackingNumber)."',
                    iSendTrackingUpdates = '".(int)$iSendTrackingUpdates."'
                WHERE
                    id='".(int)$idBooking."'		 
            "; 
            $result = $this->exeSQL($query);
//            $kTracking = new cTracking(); 
//            $trackingARY = $kTracking->createAfterShipTracking($idBooking,$this->szTrackingNumber);  
            return true;  		
	} 
        
        function courierLabelSend($szTrackingNumber,$idBooking,$iSendTrackingUpdates)
	{
            $this->set_szTrackingNumber(trim(sanitize_all_html_input($szTrackingNumber))); 
            if($this->error==true)
            {
                return false;
            }  
            $kTracking = new cTracking(); 
            $trackingArY = $kTracking->createAfterShipTracking($idBooking,$this->szTrackingNumber);  
            if($kTracking->tracking_error)
            {
                //$this->addError('szAfterShipTrackingNotification',"API ERROR: ".$kTracking->szAfterShipTrackingNotification);
                //return false;
            }
            
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_BOOKING__."
                SET
                    szTrackingNumber='".mysql_escape_custom($this->szTrackingNumber)."',
                    iSendTrackingUpdates = '".(int)$iSendTrackingUpdates."',
                    dtLabelSent=NOW()
                WHERE
                    id='".(int)$idBooking."' 
            "; 
            $result = $this->exeSQL($query); 
            
            $courierBookingArr = array();
            $courierBookingArr=$this->getCourierBookingData($idBooking);
            if((int)$courierBookingArr[0]['iCourierAgreementIncluded']==1)
            { 
                $query_update = ", szForwarderBookingStatus = '', szForwarderBookingTask='T150' "; 
            }
            else
            {
                $query_update = ", szTransportecaTask = '' "; 
            }  
            if((int)$courierBookingArr[0]['iCourierAgreementIncluded']==1)
            {
                $kBooking = new cBooking();
                $bookingDataArr=$kBooking->getExtendedBookingDetails($idBooking);               
                
                $data = array();
                $data['idBookingQuotePricing'] = $bookingDataArr['idQuotePricingDetails'] ;
                $quotePricingAry = array();
                $quotePricingAry = $kBooking->getAllBookingQuotesPricingDetails($data);
                if(!empty($quotePricingAry))
                {
                    $idBookingQuote = $quotePricingAry[1]['idQuote'];
                }
                
                if((int)$idBookingQuote>0)
                {         
                    $query="
                        UPDATE
                            ".__DBC_SCHEMATA_BOOKING_QUOTES__."
                        SET
                            iClosed='1',
                            iClosedForForwarder='1'
                        WHERE
                            idBooking='".(int)$idBooking."'
                        AND
                            id='".(int)$idBookingQuote."'
                    ";
                    $result = $this->exeSQL($query);
                }
            }
            
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_FILE_LOGS__."
                SET
                    szTransportecaStatus ='S8',
                    iLabelStatus ='".__LABEL_SEND_FLAG__."',
                    dtFileStatusUpdatedOn = NOW(),
                    dtTaskStatusUpdatedOn = NOW(),
                    iClosed='1'
                    $query_update
                WHERE
                    idBooking='".(int)$idBooking."'	 
            ";
            $result = $this->exeSQL($query);  
            return true; 
	}
	
	function updateNewLabelForBooking($idBooking,$idQuotePricingDetails=0,$isManualCourierBooking=0)
	{
            
            $courierBookingAry=$this->getAllNewBookingWithCourier($idBooking);
            
            $kBooking = new cBooking();
            $data = array();
            $data['idBookingQuotePricing'] = $idQuotePricingDetails ;
            $quotePricingAry = array();
            $quotePricingAry = $kBooking->getAllBookingQuotesPricingDetails($data);
            if(!empty($quotePricingAry))
            {
                $idBookingQuote = $quotePricingAry[1]['idQuote'];
            }
            if((int)$idBookingQuote>0 && (int)$isManualCourierBooking==1 && (int)$courierBookingAry[0]['iCourierAgreementIncluded']==1)
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_BOOKING_QUOTES__."
                    SET
                        iClosedForForwarder='0'
                    WHERE
                        idBooking='".(int)$idBooking."'
                    AND
                        id='".(int)$idBookingQuote."'
                ";
                $result = $this->exeSQL($query);
            }
        
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_BOOKING__."
                SET
                    szTrackingNumber='',
                    idLabelDownloadedBy='0',
                    iTrackingEmailSend='0'
                WHERE
                    id='".(int)$idBooking."'		
           ";
           $result = $this->exeSQL($query);
        
            $query="
               UPDATE
                   ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__."
               SET
                   iTotalPage='',
                   szCollection='',
                   dtCollection='0000-00-00',
                   dtCollectionStartTime='',
                   dtCollectionEndTime=''
               WHERE
                   idBooking='".(int)$idBooking."'
           ";
           $result = $this->exeSQL($query);
           
          $query="
               DELETE FROM
                   ".__DBC_SCHEMATA_COURIER_LABEL_UPLOADED_DATA__."
               WHERE
                   idBooking='".(int)$idBooking."'
           ";
           $result = $this->exeSQL($query); 
           
           $courierBookingArr=$this->getCourierBookingData($idBooking);
           
            $kBooking = new cBooking();
            $dtResponseTime = $kBooking->getRealNow();
            $bookingDataArr=$kBooking->getExtendedBookingDetails($idBooking);
            $fileLogsAry = array();
            $fileLogsAry['szTransportecaStatus'] = "S7";
            $fileLogsAry['iLabelStatus'] = __LABEL_NOT_SEND_FLAG__;
            $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
            $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime;
            $fileLogsAry['iStatus'] = '5';
            $fileLogsAry['iClosed'] = 0;
            if((int)$courierBookingArr[0]['iCourierAgreementIncluded']==1)
            {
                $fileLogsAry['szForwarderBookingStatus'] = '';                                    
                $fileLogsAry['szForwarderBookingTask'] = 'T140';  
            }
            else
            {
                $fileLogsAry['szTransportecaTask'] = 'T140';  
            }    
            if(!empty($fileLogsAry))
            {
                foreach($fileLogsAry as $key=>$value)
                {
                    $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            }  
            $file_log_query = rtrim($file_log_query,",");   
            $kBooking->updateBookingFileDetails($file_log_query,$bookingDataArr['idFile'],true);  
            return true;
	}
	
	function getShipperNameById($id,$flag=false)
	{
		$query="
			SELECT
				szShipperFirstName,
				szShipperLastName,
				szShipperCompanyName
			FROM
				".__DBC_SCHEMATA_SHIPPER_CONSIGNEE__."
			WHERE	
				id='".(int)$id."'		
		";
		if($result = $this->exeSQL($query))
                {
                        if($this->iNumRows>0)
                    { 
                        $row=$this->getAssoc($result); 
                         if($flag)
                         {
                             return $row;  
                         }
                         else
                         {
                                return $row['szShipperFirstName']." ".$row['szShipperLastName'] ;
                         }
                    }
                    else
                    {
                         return false;
                     }
                }
	}
	
	function getCourierBookingData($idBooking)
	{
		$query="
			SELECT
				`id`,
				`idBooking`,
				`idProviderCurrency`,
				`szProviderCurrency`,
				`fProviderRate`,
				`fProviderROE`,
				`idMarkUpCurrency`,
				`szMarkUpCurrency`,
				`fMarkUpRate`,
				`fMarkUpROE`,
				`idAdditionalMarkUpCurrency`,
				`szAdditionalMarkUpCurrency`,
				`fAdditionalMarkUpRate`,
				`fAdditionalMarkUpREO`,
				`idBookingLabelFeeCurrency`,
				`szBookingLabelFeeCurrency`,
				`fBookingLabelFeeRate`,
				`fBookingLabelFeeROE`,
				fMarkupPercentageDefault,
				fMarkupMinimumPrice,
				fVATPercentage,
				iCourierAgreementIncluded,
                                dtCollectionEndTime,
                                dtCollectionStartTime,
                                dtCollection,
                                szCollection,
                                iTotalPage,
                                szShipperEmail,
                                szCourierBookingStatusCode,
                                szCourierBookingStatusText,
                                fMinimumMarkup,
                                fMarkupPercent,
                                idCourierProvider,
                                szServiceType,
                                fMarkupperShipment,
                                szServiceProviderName,
                                fMiniMarkDefaultAmount,
                                fVATAmountCustomerCurrency
			FROM	
				".__DBC_SCHEMATA_COURIER_BOOKING_DATA__."
			WHERE
				idBooking='".(int)$idBooking."'
		";
               //echo $query;
			if($result=$this->exeSQL($query))
            {
                $bookingAry = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    $bookingAry[$ctr]=$row;
                    $ctr++;
                }
                return $bookingAry;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	
	
	}
	function getAllBookingQuotesByFileByBookingId($idBooking)
	{		
            if((int)$idBooking>0)
            {  
                $query="
                    SELECT
                        id,
                        idBooking, 
                        idFile,
                        idTransportMode,
                        szTransportMode,
                        idForwarder,
                        szForwarderContact, 
                        isSent,
                        iActive,
                        dtCreatedOn,
                        iQuotationId, 
                        dtTaskStatusUpdatedOn,
                        idStatusUpdatedBy,
                        iUpdatedByAdmin,
                        dtStatusUpdatedOn,
                        iClosed,
                        iClosedForForwarder
                    FROM
                        ".__DBC_SCHEMATA_BOOKING_QUOTES__."
                    WHERE 
                        iActive = '1'
                    AND
                        isDeleted = '0'
                     AND
                        idBooking='".(int)$idBooking."'    
                ";
                //echo $query ;
                if($result = $this->exeSQL($query))
                {
                    if($this->getRowCnt()>0)
                    {
                        $ret_ary = array();
                        $ctr = 1;
                        while($row = $this->getAssoc($result))
                        {
                            $ret_ary[$ctr] = $row;
                            $ctr++;
                        }
                        return $ret_ary;
                    }
                    else
                    {
                        return false ;
                    }
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
            else
            {
                return false;
            }
	}
        
        function bookingNotConfirmedDateMoreThanCourierTwoDays()
	{
                $date=getBusinessDaysForCourier(date('Y-m-d'),'2 DAY');
		$query="
                    SELECT
                        count(b.id) as id
                    FROM
                            ".__DBC_SCHEMATA_BOOKING__." AS b	
                    LEFT JOIN
                        ".__DBC_SCHEMATA_BOOKING_ACKNOWLEDMENT__." AS ack	
                    ON
                        (ack.idBooking = b.id)				
                    WHERE	
                        DATE(b.dtBookingConfirmed) <= '".$date."'	
                    AND 
                        b.idUser != 0
                    AND 
                        b.idBookingStatus IN ('3','4')	
                    AND
                        b.iTransferConfirmed = '0'
                    AND
                        ack.id IS NULL
                    AND
                        b.iCourierBooking='1'
                ";
		//echo $query;
		if($result=$this->exeSQL($query))
		{
			$row=$this->getAssoc($result);
			$idCount = $row['id'];
			if($idCount)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}	
	}
        
    function createBookingCourierLabelValidation($data,$saveFlag)
    {
        $this->set_szTrackingNumber(trim(sanitize_all_html_input($data['szMasterTrackingNumber'])),$saveFlag);
        if((int)$data['iManualCourierBooking']=='1')
        {
            $this->set_idCourierProduct(trim(sanitize_all_html_input($data['idCourierProduct'])),$saveFlag);
        }
        $this->set_iFileCount(trim(sanitize_all_html_input($data['filecount'])),$saveFlag);
        $this->set_iCollection(trim(sanitize_all_html_input($data['iCollection'])),$saveFlag);
        
        if($this->iCollection=='Scheduled')
        {    
            $this->set_dtCollection(trim(sanitize_all_html_input($data['dtCollection'])),$saveFlag);
            $this->set_dtCollectionEndTime(trim(sanitize_all_html_input($data['dtCollectionEndTime'])),$saveFlag);
            $this->set_dtCollectionStartTime(trim(sanitize_all_html_input($data['dtCollectionStartTime'])),$saveFlag);
        } 
        //print_r($this);
        if($this->error==true)
       	{
            return false;
        }   
        return true; 
    }
    function createBookingCourierLabel($data,$saveFlag=true,$bForwarderFlag=false)
    {   
        $filename = __APP_PATH_ROOT__."/logs/courier_debug.log";
 
        if($this->createBookingCourierLabelValidation($data,$saveFlag))
       	{ 
            if(!$bForwarderFlag)
            {
                $query_update = " iSendTrackingUpdates='".(int)$data['iSendTrackingUpdates']."', ";
            }
            $column='';
            if((int)$data['iManualCourierBooking']=='1')
            {
                $idCourierProductArr=explode("-",$this->idCourierProduct);
               
                if((int)count($idCourierProductArr)>0)
                {
                   $column=" ,
                      idServiceProviderProduct='".(int)$idCourierProductArr[0]."',  
                      idServiceProvider='".(int)$idCourierProductArr[1]."'";
                }
            }
            
            
            $kBooking = new cBooking();
            $bookingDataArr=$kBooking->getExtendedBookingDetails($data['idBooking']);               

            $dataArr = array();
            $dataArr['idBookingQuotePricing'] = $bookingDataArr['idQuotePricingDetails'] ;
            $quotePricingAry = array();
            $quotePricingAry = $kBooking->getAllBookingQuotesPricingDetails($dataArr);
            if(!empty($quotePricingAry))
            {
                $idBookingQuote = $quotePricingAry[1]['idQuote'];
            }
            
            if((int)$bookingDataArr['isManualCourierBooking']=='1' && (int)$idBookingQuote>0)
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_BOOKING_QUOTES__."
                    SET
                        iClosed='1',
                        iClosedForForwarder='1'
                    WHERE
                        idBooking='".(int)$data['idBooking']."'
                    AND
                        id='".(int)$idBookingQuote."'
                ";
                $result = $this->exeSQL($query);
            }
            
            $query="
                UPDATE
                  ".__DBC_SCHEMATA_BOOKING__."
                SET
                  szTrackingNumber='".mysql_escape_custom(strtoupper($this->szTrackingNumber))."',
                  $query_update
                  dtLabelSent= NOW(),
                  iUpdateToAftership = '1'
                  $column
                WHERE
                  id='".(int)$data['idBooking']."'		
            ";
//            echo $query;   
//            die;
            
           $result = $this->exeSQL($query);
            
           $dtCollectionArr=explode("/",$this->dtCollection);
           $this->dtCollection=$dtCollectionArr[2]."-".$dtCollectionArr[1]."-".$dtCollectionArr[0];
           $query="
                UPDATE
                    ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__."
                SET
                    iTotalPage='".(int)$this->iFileCount."',
                    szCollection='".mysql_escape_custom($this->iCollection)."',
                    dtCollection='".mysql_escape_custom($this->dtCollection)."',
                    dtCollectionStartTime='".mysql_escape_custom($this->dtCollectionStartTime)."',
                    dtCollectionEndTime='".mysql_escape_custom($this->dtCollectionEndTime)."'
                WHERE
                    idBooking='".(int)$data['idBooking']."'
           "; 
			
           //echo $query;
           $result = $this->exeSQL($query);  
           if($saveFlag)
           {
                $kBooking= new cBooking();
                $bookingDataArr=$kBooking->getExtendedBookingDetails($data['idBooking']);
                $idForwarder = $bookingDataArr['idForwarder'];
				
                if($data['idBookingFile']<=0)
                {
                    $data['idBookingFile'] = $bookingDataArr['idFile'];
                }
				
                $courierBookingArr=$this->getCourierBookingData($data['idBooking']); 
                
                $kBooking = new cBooking();
                $dtResponseTime = $kBooking->getRealNow();
            
                $fileLogsAry = array();
                $fileLogsAry['szTransportecaStatus'] = "S8";
                $fileLogsAry['iLabelStatus'] = __LABEL_SEND_FLAG__;
                $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
                $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime; 
                $fileLogsAry['iClosed']='1';
                if((int)$courierBookingArr[0]['iCourierAgreementIncluded']==1)
                {
                    $fileLogsAry['szForwarderBookingStatus'] = '';                                    
                    $fileLogsAry['szForwarderBookingTask'] = 'T150';  
                }
                else
                {
                   $fileLogsAry['szTransportecaTask'] = '';  
                }     	 

                if(!empty($fileLogsAry))
                {
                    foreach($fileLogsAry as $key=>$value)
                    {
                        $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                }  
                $file_log_query = rtrim($file_log_query,",");   
                $kBooking->updateBookingFileDetails($file_log_query,$data['idBookingFile'],true); 
                    
                //if($courierBookingArr[0]['iCourierAgreementIncluded']==1)//19-01-2017FW: E-mail change
                //{
                    $kBooking = new cBooking();
                    $downloadedByAry = $kBooking->getAcknowledgeDownloadedByUserDetails($data['idBooking'],$idForwarder);
                    if(empty($downloadedByAry))
                    {
                        $kBooking->sendAcknowlodgementEmail($data['idBooking'],$idForwarder,true);
                    }
                //}
                
                /*$query="
                    UPDATE
                        ".__DBC_SCHEMATA_BOOKING_QUOTES__."
                    SET
                        iClosed='1'
                     WHERE
                        idFile='".(int)$data['idBookingFile']."'
                ";
                $result = $this->exeSQL($query);*/
                 
                
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__."
                    SET
                       szShipperEmail='".mysql_escape_custom($bookingDataArr['szShipperEmail'])."'
                    WHERE
                        idBooking='".(int)$data['idBooking']."'
                 ";  
                $result = $this->exeSQL($query);
                  
                $replace_ary=array();  
                $kConfig =new cConfig();
                
                
                $kConfig->loadCountry($bookingDataArr['idShipperCountry']); 
                if($kConfig->iDefaultLanguage>0)
                {
                    $langShipperArr=$kConfig->getLanguageDetails('',$kConfig->iDefaultLanguage);
                }
                
                //echo $kConfig->iDefaultLanguage."iDefaultLanguage<br />";
                $iBookingLanguage = $bookingDataArr['iBookingLanguage'];
                $langArr=$kConfig->getLanguageDetails('',$bookingDataArr['iBookingLanguage']);
                
                //$kConfig = new cConfig();
                $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$bookingDataArr['iBookingLanguage']);
                $szClickDownloadLink=$configLangArr[1]['szClickDownloadLink'];
                        
                if(!empty($langShipperArr)) // 61 means Demark
                {
                    if($kConfig->iDefaultLanguage!=__LANGUAGE_ID_ENGLISH__)
                    {
                        $szDomain=createDownlabelLink(2,$kConfig->iDefaultLanguage,$bookingDataArr['szBookingRandomNum']);
                        $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$langShipperArr[0]['id']);
                        $szClickDownloadLink=$configLangArr[1]['szClickDownloadLink'];
                
                        $szLanguageCode=$langShipperArr[0]['szLanguageCode'];
                        $szLinkValue=$szDomain;
                        $szLabelLink="<a href='".$szLinkValue."'>".$szClickDownloadLink."</a>";  
                        $replace_ary['iBookingLanguage'] = $langShipperArr[0]['id'];
                    }
                    else
                    {
                        $szDomain=createDownlabelLink(2,__LANGUAGE_ID_ENGLISH__,$bookingDataArr['szBookingRandomNum']);
                        $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',__LANGUAGE_ID_ENGLISH__);
                        $szClickDownloadLink=$configLangArr[1]['szClickDownloadLink'];
                        
                        $szLinkValue=$szDomain;
                        $szLabelLink="<a href='".$szLinkValue."'>".$szClickDownloadLink."</a>";
                        $replace_ary['iBookingLanguage'] = __LANGUAGE_ID_ENGLISH__;
                    }
                }
                else if(!empty($langArr) && $bookingDataArr['iBookingLanguage']!=__LANGUAGE_ID_ENGLISH__)
                {
                    $szDomain=createDownlabelLink(2,$bookingDataArr['iBookingLanguage'],$bookingDataArr['szBookingRandomNum']);
                    $szLanguageCode=$langArr[0]['szLanguageCode'];
                    $szLinkValue=$szDomain;
                    $szLabelLink="<a href='".$szLinkValue."'>".$szClickDownloadLink."</a>";  
                    $replace_ary['iBookingLanguage'] = $bookingDataArr['iBookingLanguage'];
                }
                else
                {
                   $szDomain=createDownlabelLink(2,__LANGUAGE_ID_ENGLISH__,$bookingDataArr['szBookingRandomNum']);
                   $szLinkValue=$szDomain;
                   $szLabelLink="<a href='".$szLinkValue."'>".$szClickDownloadLink."</a>";
                   $replace_ary['iBookingLanguage'] = __LANGUAGE_ID_ENGLISH__;
                } 
                $iBookingLanguage = $replace_ary['iBookingLanguage'];
                
                $cutoffLocalTimeAry = fColumnData();
                $newCourierBookingAry=$this->getAllNewBookingWithCourier($data['idBooking']);
                
                $replace_ary['szShipperName']=$bookingDataArr['szShipperFirstName']." ".$bookingDataArr['szShipperLastName'];
                $replace_ary['szCourierProvider']=$newCourierBookingAry[0]['szProviderName'];
                $replace_ary['szCourierCompany']=$newCourierBookingAry[0]['szProviderName'];
                if(!empty($bookingDataArr['szConsigneeCompanyName']))
                {
                    $replace_ary['szConsigneeCompany'] = $bookingDataArr['szConsigneeCompanyName'];
                }
                else
                {
                    $replace_ary['szConsigneeCompany'] = $bookingDataArr['szConsigneeFirstName']." ".$bookingDataArr['szConsigneeLastName'];;
                }
                
                $replace_ary['szBookingReference']=$bookingDataArr['szBookingRef'];
                
                $kConfig = new cConfig();
                $kConfig->loadCountry($bookingDataArr['idConsigneeCountry'],false,$iBookingLanguage); 
                $replace_ary['szConsigneeCountry']= $kConfig->szCountryName;
                
                if($bookingDataArr['szConsigneeCompanyName']!='')
                {
                    $replace_ary['szConsigneeName']=", ".$bookingDataArr['szConsigneeFirstName']." ".$bookingDataArr['szConsigneeLastName'];
                }
                else
                {
                    $replace_ary['szConsigneeName']='';
                }
                if($this->iCollection=='Scheduled')
                {
                    $replace_ary['szPickupDate']=date('d/m/Y',strtotime($this->dtCollection));
                    $replace_ary['szPickupTimeFrom']=$cutoffLocalTimeAry[$this->dtCollectionStartTime];
                    $replace_ary['szPickupTimeTo']=$cutoffLocalTimeAry[$this->dtCollectionEndTime];
                    if((strtolower($bookingDataArr['szShipperPostCode'])=='w91 w207' || strtolower($bookingDataArr['szShipperPostCode'])=='w91 ve82') && $bookingDataArr['idShipperCountry']=='107'){                        
                        $month = date('m',strtotime($this->dtCollection));
                        $szMonth = getMonthName($bookingDataArr['iBookingLanguage'],(int)$month,true);
                        $iDay = date('j',  strtotime($this->dtCollection));
                        $szPickupDate = $iDay.". ".$szMonth;     
                        $replace_voga_ary=array();
                        $replace_voga_ary['szPickupDate']=$szPickupDate;
                        $replace_voga_ary['szBookingRef']=$bookingDataArr['szBookingRef'];
                        $replace_voga_ary['szShipperCompany']=$bookingDataArr['szShipperCompanyName'];
                        $replace_voga_ary['szFirstName']=$bookingDataArr['szConsigneeFirstName'];
                        $replace_voga_ary['szLastName']=$bookingDataArr['szConsigneeLastName'];
                        $replace_voga_ary['iBookingLanguage']=$bookingDataArr['iBookingLanguage'];
                        $replace_voga_ary['idBooking']=$bookingDataArr['id'];
                        $replace_voga_ary['iAdminFlag']=1;
                    }
                }
                $replace_ary['szLabelLink']=$szLabelLink;
                $replace_ary['szUrl']=$szLinkValue;
                $replace_ary['supportEmail'] = __STORE_SUPPORT_EMAIL__;
                $replace_ary['iIgnoreTracking']=1;
				 
                if($this->iCollection=='Scheduled')
                {
                    createEmail(__SHIPPER_COURIER_BOOKING_LABEL_READY_FOR_DOWNLOAD_PICK_SCHEDULED_EMAIL__, $replace_ary,$bookingDataArr['szShipperEmail'], '', __STORE_SUPPORT_EMAIL__,'', __STORE_SUPPORT_EMAIL__,__FLAG_FOR_DEFAULT__,'',$data['idBooking']);
                    if(strtolower(($bookingDataArr['szShipperPostCode'])=='w91 w207' || strtolower($bookingDataArr['szShipperPostCode'])=='w91 ve82') && $bookingDataArr['idShipperCountry']=='107'){                      
                        createEmail(__COURIER_BOOKING_SCHEDULED_FOR_COLLECTION_VOGA__, $replace_voga_ary,$bookingDataArr['szConsigneeEmail'], '', __STORE_SUPPORT_EMAIL__,'', __STORE_SUPPORT_EMAIL__,__FLAG_FOR_DEFAULT__,'',$data['idBooking']);
                    }
                }
                else
                {
                     createEmail(__SHIPPER_COURIER_BOOKING_LABEL_READY_FOR_DOWNLOAD_PICK_NOT_SCHEDULED_EMAIL__, $replace_ary,$bookingDataArr['szShipperEmail'], '', __STORE_SUPPORT_EMAIL__,'', __STORE_SUPPORT_EMAIL__,__FLAG_FOR_DEFAULT__,'',$data['idBooking']);
                } 
           }


           $query="
               DELETE FROM
                   ".__DBC_SCHEMATA_COURIER_LABEL_UPLOADED_DATA__."
               WHERE
                   idBooking='".(int)$data['idBooking']."'
           ";
           $result = $this->exeSQL($query);


           if($data['file_name']!='')
           {
               $fileArr=explode(";",$data['file_name']);
               
               if(count($fileArr)>0)
               {
                   for($i=0;$i<count($fileArr);++$i)
                   {
                       $fileNameArr=explode("#####",$fileArr[$i]);

                       $query="
                           INSERT INTO
                               ".__DBC_SCHEMATA_COURIER_LABEL_UPLOADED_DATA__."
                           (
                               idBooking,
                               idFile,
                               szFileName,
                               szUploadedFileName
                            )
                            VALUES
                           ( 
                               '".(int)$data['idBooking']."',
                               '".(int)$data['idBookingFile']."',
                               '".mysql_escape_custom($fileNameArr[0])."',
                               '".mysql_escape_custom($fileNameArr[1])."'    
                            )   
                       "; 
                       $result = $this->exeSQL($query);
                   } 
               }    
           } 
           return true;	
        }
    }
    
    function getCourierLabelUploadedPdf($idBooking,$flag=false,$fullInfo=false)
    {
        $query="
            SELECT
                id,
                szFileName,
                szUploadedFileName
            FROM
                ".__DBC_SCHEMATA_COURIER_LABEL_UPLOADED_DATA__."
            WHERE
                idBooking='".(int)$idBooking."'
        ";
        //echo $query; 
        if($result = $this->exeSQL($query))
        {
            if($this->getRowCnt()>0)
            {
                $ret_ary = array();
                $ctr = 1;
                if($flag)
                {
                    while($row = $this->getAssoc($result))
                    {
                        if($fullInfo)
                        {
                           $ret_ary[$ctr] = $row;
                            $ctr++; 
                        }
                        else 
                        {
                            $ret_ary[$ctr] = $row['szUploadedFileName'];
                            $ctr++;
                        }
                    } 
                    return $ret_ary;
                }
                else
                {    
                    while($row = $this->getAssoc($result))
                    {

                        $ret_ary[$ctr] = $row['szFileName']."#####".$row['szUploadedFileName']."#####".$row['id'];
                        $ctr++;
                    }
                    $retStr=implode(";",$ret_ary);
                     return $retStr;
                }
               
            }
            else
            {
                return false ;
            }
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
    
    function reOpenBookingQuote($idBookingQuote,$idBooking)
    {
       
         $query="
                UPDATE
                     ".__DBC_SCHEMATA_BOOKING__."
                SET
                     szTrackingNumber='".mysql_escape_custom(strtoupper($this->szTrackingNumber))."'
                WHERE
                        id='".(int)$idBooking."'		
           ";
           $result = $this->exeSQL($query);
        
        $query="
               UPDATE
                   ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__."
               SET
                   iTotalPage='',
                   szCollection='',
                   dtCollection='0000-00-00',
                   dtCollectionStartTime='',
                   dtCollectionEndTime=''
               WHERE
                   idBooking='".(int)$idBooking."'
           ";
           $result = $this->exeSQL($query);
           
          $query="
               DELETE FROM
                   ".__DBC_SCHEMATA_COURIER_LABEL_UPLOADED_DATA__."
               WHERE
                   idBooking='".(int)$idBooking."'
           ";
           $result = $this->exeSQL($query);  
    }
    
    function resendLabelEmail($idBooking,$szEmail)
    {
        
        $this->set_szEmail(trim(sanitize_all_html_input($szEmail)));
        
        if($this->error==true)
       	{
            return false;
        } 
        
        $kBooking= new cBooking();
        $bookingDataArr=$kBooking->getExtendedBookingDetails($idBooking);
        
        $courierBookingArr=$this->getCourierBookingData($idBooking);
            
            $query="
                  UPDATE
                       ".__DBC_SCHEMATA_BOOKING__."
                  SET
                      dtLabelSent= NOW() 
                  WHERE
                          id='".(int)$idBooking."'		
           ";
           $result = $this->exeSQL($query);
         
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__."
                SET
                   szShipperEmail='".mysql_escape_custom($this->szEmail)."'
                WHERE
                    idBooking='".(int)$idBooking."'
            ";
            $result = $this->exeSQL($query);

        $replace_ary=array();
        //print_r($bookingDataArr['idShipperCountry']);
        $replace_ary['iBookingLanguage']=__LANGUAGE_ID_ENGLISH__;
        $replace_ary['iBookingLanguage']=$bookingDataArr['iBookingLanguage'];
        
        
        $kConfig = new cConfig();
        $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$bookingDataArr['iBookingLanguage']);
        $szClickDownloadLink=$configLangArr[1]['szClickDownloadLink'];
        
        
        $kConfig =new cConfig();
        $langArr=$kConfig->getLanguageDetails('',$bookingDataArr['iBookingLanguage']);
        
        $kConfig->loadCountry($bookingDataArr['idShipperCountry']); 
        if($kConfig->iDefaultLanguage>0)
        {
            $langShipperArr=$kConfig->getLanguageDetails('',$kConfig->iDefaultLanguage);
        }       
        if(!empty($langShipperArr)) // 61 means Demark
        {
            if($kConfig->iDefaultLanguage!=__LANGUAGE_ID_ENGLISH__)
            {
                $szLanguageCode=$langShipperArr[0]['szLanguageCode'];
                $szDomain=createDownlabelLink(2,$kConfig->iDefaultLanguage,$bookingDataArr['szBookingRandomNum']);
                $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$langShipperArr[0]['id']);
                $szClickDownloadLink=$configLangArr[1]['szClickDownloadLink'];
        
                $szLinkValue=$szDomain;
                $szLabelLink="<a href='".$szLinkValue."'>".$szClickDownloadLink."</a>";  
                $replace_ary['iBookingLanguage'] = $langShipperArr[0]['id'];
            }
            else
            {
                $szDomain=createDownlabelLink(2,__LANGUAGE_ID_ENGLISH__,$bookingDataArr['szBookingRandomNum']);
                $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',__LANGUAGE_ID_ENGLISH__);
                $szClickDownloadLink=$configLangArr[1]['szClickDownloadLink'];
                
                $szLinkValue=$szDomain;
                $szLabelLink="<a href='".$szLinkValue."'>".$szClickDownloadLink."</a>";
                $replace_ary['iBookingLanguage'] = __LANGUAGE_ID_ENGLISH__;
            }
        }
        else if(!empty($langArr) && $bookingDataArr['iBookingLanguage']!=__LANGUAGE_ID_ENGLISH__)
        {
            $szDomain=createDownlabelLink(2,$bookingDataArr['iBookingLanguage'],$bookingDataArr['szBookingRandomNum']);
            $szLanguageCode=$langArr[0]['szLanguageCode'];
            $szLinkValue=$szDomain;
            $szLabelLink="<a href='".$szLinkValue."'>".$szClickDownloadLink."</a>";  
            $replace_ary['iBookingLanguage'] = $bookingDataArr['iBookingLanguage'];
        }
        else
        {
           $szDomain=createDownlabelLink(2,__LANGUAGE_ID_ENGLISH__,$bookingDataArr['szBookingRandomNum']);
           $szLinkValue=$szDomain;
           $szLabelLink="<a href='".$szLinkValue."'>".$szClickDownloadLink."</a>";
           $replace_ary['iBookingLanguage'] = __LANGUAGE_ID_ENGLISH__;
        }
        //echo $szLinkValue;
        $cutoffLocalTimeAry = fColumnData();
        $newCourierBookingAry=$this->getAllNewBookingWithCourier($data['idBooking']);

        $replace_ary['szShipperName']=$bookingDataArr['szShipperFirstName']." ".$bookingDataArr['szShipperLastName'];
        $replace_ary['szCourierProvider']=$newCourierBookingAry[0]['szProviderName'];
        $replace_ary['szCourierCompany']=$newCourierBookingAry[0]['szProviderName'];
        if($bookingDataArr['szConsigneeCompanyName']!=''){
        $replace_ary['szConsigneeCompany']=$bookingDataArr['szConsigneeCompanyName'];
        }else
        {
            $replace_ary['szConsigneeCompany']=$bookingDataArr['szConsigneeFirstName']." ".$bookingDataArr['szConsigneeLastName'];;
        }
        $replace_ary['szBookingReference']=$bookingDataArr['szBookingRef'];
        if($bookingDataArr['idShipperCountry']==61)
        {
            $kConfig = new cConfig();
            $kConfig->loadCountry($bookingDataArr['idConsigneeCountry'],false,__LANGUAGE_ID_DANISH__);
            $replace_ary['szConsigneeCountry']=$kConfig->szCountryName;
        }
        else
        {
            $replace_ary['szConsigneeCountry']=$bookingDataArr['szConsigneeCountry'];
        }
        if($bookingDataArr['szConsigneeCompanyName']!=''){
            $replace_ary['szConsigneeName']=", ".$bookingDataArr['szConsigneeFirstName']." ".$bookingDataArr['szConsigneeLastName'];
        }
        else
        {
            $replace_ary['szConsigneeName']='';
        }
        if($courierBookingArr[0]['szCollection']=='Scheduled')
        {
            $replace_ary['szPickupDate']=date('d/m/Y',strtotime($courierBookingArr[0]['dtCollection']));
            $replace_ary['szPickupTimeFrom']=$cutoffLocalTimeAry[$courierBookingArr[0]['dtCollectionStartTime']];
            $replace_ary['szPickupTimeTo']=$cutoffLocalTimeAry[$courierBookingArr[0]['dtCollectionEndTime']];
        }
        $replace_ary['szLabelLink']=$szLabelLink;
        $replace_ary['szUrl']=$szLinkValue;
        $replace_ary['supportEmail'] = __STORE_SUPPORT_EMAIL__;
        $replace_ary['iIgnoreTracking']=1;
        
        if($courierBookingArr[0]['iCollection']=='Scheduled')
        {
            createEmail(__SHIPPER_COURIER_BOOKING_LABEL_READY_FOR_DOWNLOAD_PICK_SCHEDULED_EMAIL__, $replace_ary,$this->szEmail, '', __STORE_SUPPORT_EMAIL__,'', __STORE_SUPPORT_EMAIL__,__FLAG_FOR_DEFAULT__,'',$idBooking);
        }
        else
        {
             createEmail(__SHIPPER_COURIER_BOOKING_LABEL_READY_FOR_DOWNLOAD_PICK_NOT_SCHEDULED_EMAIL__, $replace_ary,$this->szEmail, '', __STORE_SUPPORT_EMAIL__,'', __STORE_SUPPORT_EMAIL__,__FLAG_FOR_DEFAULT__,'',$idBooking);
        }
        return true;
    }
    
    function updateShipperDetails($data,$bPartnerApi=false)
    {
        $this->set_szShipperCompanyName(trim(sanitize_all_html_input($data['szShipperCompanyName'])),true);
        $this->set_szShipperFirstName(trim(sanitize_all_html_input($data['szShipperFirstName'])));
        $this->set_szShipperLastName(trim(sanitize_all_html_input($data['szShipperLastName'])));
        $this->set_szShipperEmail(trim(sanitize_all_html_input($data['szShipperEmail'])));
        $this->set_idShipperCountry(trim(sanitize_all_html_input($data['idShipperCountry'])));
        $this->set_dtShipperPickUp(trim(sanitize_all_html_input($data['dtShipperPickUp'])));
        $this->set_szShipperPhone(trim(sanitize_all_html_input($data['szShipperPhone'])));
         
        if($this->error==true)
       	{
            if($bPartnerApi)
            {
                /*
                * Usually in case of partner API we never end up in this block, 
                */
                $this->iValidationError = 1;
                return false;
            }
            else
            {
                $_SESSION['secondClickFlag']=0;
                return false;
            } 
        } 
        
        if($bPartnerApi)
        {
            $iLanguage = $data['iLanguage'];     
        }
        else
        {
            $iLanguage = getLanguageId();     
        }
        $allCourierBookingArr=$this->getAllNewBookingWithCourier($data['idBooking']);
        $courierBookingArr=$this->getCourierBookingData($data['idBooking']);
        
        $dateShipArr=explode("/",$this->dtShipperPickUp);
        $shipdate=$dateShipArr[2]."-".$dateShipArr[1]."-".$dateShipArr[0];
        $this->dtShipperPickUp=$shipdate;
        
        if($courierBookingArr[0]['szCollection']=='Scheduled')
        {   
            $dtCollection=date('Y-m-d',strtotime($courierBookingArr[0]['dtCollection'])); 
            $monthName=getMonthName($iLanguage,date('m',strtotime($courierBookingArr[0]['dtCollection']))); 
            if(strtotime($dtCollection)!=strtotime($shipdate))
            { 
                if($iLanguage==__LANGUAGE_ID_DANISH__)
                {
                    $monthName=strtolower($monthName).".";
                }
                else
                {
                    $monthName=$monthName.",";
                } 
                $error_text = t($this->t_base_courier_label.'messages/msg_1')." ".date('d',strtotime($courierBookingArr[0]['dtCollection'])).". ".$monthName." ".str_replace("szProvider",$allCourierBookingArr[0]['szProviderName'],t($this->t_base_courier_label.'messages/msg_2')) ;
                if($bPartnerApi)
                {
                    $this->szLabelDownloadText = $error_text;
                }
                else
                {
                    $this->addError( "dtShipperPickUp1" , $error_text ); 
                    if($_SESSION['secondClickFlag']!=1)
                    {    
                        return false;
                    }
                } 
            }
        }
        else
        { 
            $error_text = t($this->t_base_courier_label.'messages/msg_3') ;
            $error_text = str_replace("szProvider",$allCourierBookingArr[0]['szProviderName'],$error_text);
            
            if($bPartnerApi)
            {
                $this->szLabelDownloadText = $error_text;
            }
            else
            {
                $this->addError( "dtShipperPickUp1" , $error_text ); 
                if($_SESSION['secondClickFlag']!=1)
                {    
                    return false;
                } 
            } 
        } 
        
        if($bPartnerApi)
        {
            $idCustomer = cPartner::$idPartnerCustomer;
        }
        else
        {
            $userSessionData = array();
            $userSessionData = get_user_session_data();
            $idCustomer = $userSessionData['idUser']; 
        } 
        
        $sendEmailFlag=false;
        $kBooking= new cBooking(); 
        $kRegisterShipCon = new cRegisterShipCon();
        $bookingDataArr=$kBooking->getExtendedBookingDetails($data['idBooking']);
        $kUser=new cUser();
        if(((int)$idCustomer<=0))
        {
            $iAddNewUser = false;
            if($kUser->isUserEmailExist($this->szShipperEmail))
            {  
                //New Line Added 15/03/2015
                $idCustomer = $kUser->idIncompleteUser ;

                //This session is available only for booking process   
                $kUser->manualLogin($idCustomer); 
            }
            else
            {
                $iAddNewUser = true;
            }
           
            if($iAddNewUser)
            {  
                $szPassword = mt_rand(0,99)."".$kRegisterShipCon->generateUniqueToken(8);

                //User is not loogedin then we create a new profile
                $addUserAry = array();
                $addUserAry['szFirstName'] = $this->szShipperFirstName ;
                $addUserAry['szLastName'] = $this->szShipperLastName ;
                $addUserAry['szEmail'] = $this->szShipperEmail ;
                $addUserAry['szPassword'] = $szPassword ;
                $addUserAry['szConfirmPassword'] = $szPassword ;
                $addUserAry['szPostCode'] = $bookingDataArr['szShipperPostcode'] ;
                $addUserAry['szCity'] = $bookingDataArr['szShipperCity'] ; 
                $addUserAry['szState'] = $bookingDataArr['szShipperState']; 
                $addUserAry['szCountry'] = $this->idShipperCountry ; 
                $addUserAry['szPhoneNumberUpdate'] = $this->szShipperPhone ;
                $addUserAry['szCompanyName'] = $this->szShipperCompanyName ;  
                $addUserAry['szAddress1'] = $bookingDataArr['szShipperAddress'] ;
                $addUserAry['szCurrency'] = $bookingDataArr['idCustomerCurrency']; 
                if($this->szShipperEmail==$bookingDataArr['szShipperEmail'])
                {    
                    $addUserAry['iConfirmed'] = 1; 
                }
                else
                {
                    $sendEmailFlag=true;
                    $addUserAry['iConfirmed'] = 0;
                }
                $addUserAry['iFirstTimePassword'] = 1;  
                $addUserAry['idInternationalDialCode'] = $this->idShipperCountry ; 
                if($kUser->createAccount($addUserAry,'LABEL_DOWNLOAD_PROCESS'))
                { 
                    $idCustomer = $kUser->id ;  
                    $kUser->manualLogin($idCustomer);
                    if($bPartnerApi)
                    {
                        //@TO DO
                    }
                    else
                    {
                        $token = sha1(uniqid(rand(), true));
                        $kUser->updateUserLoginCookie($idCustomer,$token);
                        if(__ENVIRONMENT__ == "LIVE")
                        {
                            setcookie("__USER_LOGIN_COOKIE__", $token, time()+(3600*24*90),'/',$_SERVER['HTTP_HOST'],true);
                        }
                        else
                        {
                            setcookie("__USER_LOGIN_COOKIE__", $token, time()+(3600*24*90),'/');
                        } 
                    } 
                } 
            }
        } 
        if(!$iAddNewUser)
        { 
            $kUser->getUserDetails($idCustomer);

            $updateUserDetailsAry = array(); 
            $updateUserDetailsAry['szFirstName'] = $this->szShipperFirstName ;
            $updateUserDetailsAry['szLastName'] = $this->szShipperLastName ; 
            $updateUserDetailsAry['szPostCode'] = $bookingDataArr['szShipperPostcode']  ;
            $updateUserDetailsAry['szCity'] = $bookingDataArr['szShipperCity'] ; 
            $updateUserDetailsAry['szState'] = $bookingDataArr['szShipperState'] ;   
            $updateUserDetailsAry['szCompanyName'] = $this->szShipperCompanyName ;  
            $updateUserDetailsAry['szAddress'] = $this->szBillingAddress ;
            $updateUserDetailsAry['szCurrency'] = $bookingDataArr['idCustomerCurrency']; 
            $updateUserDetailsAry['szPhone'] = $this->szShipperPhone;
            $updateUserDetailsAry['idInternationalDialCode'] = $this->idShipperCountry ;
            $updateUserDetailsAry['idCountry'] = $this->idShipperCountry ; 
            if($this->szShipperEmail!=$bookingDataArr['szShipperEmail'])
            {    
                $sendEmailFlag=true;
                $updateUserDetailsAry['iConfirmed'] = '1' ; 
            }

            if($kUser->iConfirmed!=1)
            {
                $updateUserDetailsAry['szEmail'] = $this->szShipperEmail ; 
            } 
            
            if(!empty($updateUserDetailsAry))
            {
                $update_user_query = '';
                foreach($updateUserDetailsAry as $key=>$value)
                {
                    $update_user_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                } 
                
                if($bPartnerApi)
                {
                    //@TO DO
                }
                else
                {
                    $token = sha1(uniqid(rand(), true));
                    $kUser->updateUserLoginCookie($idCustomer,$token);
                    if(__ENVIRONMENT__ == "LIVE")
                    {
                        setcookie("__USER_LOGIN_COOKIE__", $token, time()+(3600*24*90),'/',$_SERVER['HTTP_HOST'],true);
                    }
                    else
                    {
                        setcookie("__USER_LOGIN_COOKIE__", $token, time()+(3600*24*90),'/');
                    }
                } 
            } 
            
            $update_user_query = rtrim($update_user_query,",");  
            if($kRegisterShipCon->updateUserDetailsByQuotes($update_user_query,$idCustomer))
            {
                
            }  
        } 
        
        if((int)$bookingDataArr['idLabelDownloadedBy']==0 || $bPartnerApi)
        {
            /*
            * In case of $bPartnerApi: true we always update the fllowing details
            */
            
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_BOOKING__."
                SET
                    idLabelDownloadedBy='".(int)$idCustomer."',
                    dtLabelDownloaded=NOW(),
                    iLabelDownloaded='1'
                WHERE
                    id='".(int)$data['idBooking']."'
            ";
            $result = $this->exeSQL($query);
                    
           $query="
                UPDATE
                    ".__DBC_SCHEMATA_FILE_LOGS__."
                SET
                    szTransportecaStatus='S9',
                    iLabelStatus=".__LABEL_DOWNLOAD_FLAG__.", 
                    iStatus='6'
                WHERE
                    idBooking='".(int)$data['idBooking']."'
            ";
            $result = $this->exeSQL($query);
            $sendEmailFlag=true;
        } 
        
        if(trim($kUser->szEmail) == trim($bookingDataArr['szShipperEmail']))
        {
            $sendEmailFlag = false;
        } 
        if((int)$bookingDataArr['iLabelDownloaded']==1)
        {
            $sendEmailFlag = false;
        } 
        
        if($sendEmailFlag)
        { 
            $szCountryISO3Code = "";
            $kConfigNew1 = new cConfig();
            $kTracking = new cTracking();
            $idCourierProvider = $kTracking->getIdCourierProviderByIdServiceProvider($bookingDataArr['idServiceProvider']);
            
            if($bookingDataArr['idDestinationCountry']>0 && $idCourierProvider==__TNT_API__)
            {
                $kConfigNew1->loadCountry($bookingDataArr['idDestinationCountry']);
                if(!empty($kConfigNew1->szCountryISO3Code))
                {
                    $szCountryISO3Code = ":".$kConfigNew1->szCountryISO3Code;
                } 
            } 
    
            $replace_ary=array(); 
            $replace_ary['iBookingLanguage']=__LANGUAGE_ID_ENGLISH__;
            $szLinkValue = __AFTER_SHIP_TRACKING_URL__."/".$bookingDataArr['szTrackingNumber']."".$szCountryISO3Code;
            
            $kConfig =new cConfig();
            $langArr=$kConfig->getLanguageDetails('',$bookingDataArr['iBookingLanguage']);
            
            if(!empty($langArr) && $bookingDataArr['iBookingLanguage']!=__LANGUAGE_ID_ENGLISH__)
            { 
                $szLanguageCode=$langArr[0]['szLanguageCode'];
                $szUnsubscribedValue = __BASE_STORE_URL__."/".$szLanguageCode."/myBooking/unsubscribe/".$bookingDataArr['szBookingRandomNum']."/";
                $szUnsubscribedLink="<a href='".$szUnsubscribedValue."'>".t($this->t_base_courier_label.'fields/click_here')."</a>"; 
                
                $szLabelLink="<a href='".$szLinkValue."'>".t($this->t_base_courier_label.'fields/track_shipping_ref')." ".$bookingDataArr['szBookingRef']."</a>";
                $replace_ary['iBookingLanguage']=$bookingDataArr['iBookingLanguage'];  
            }
            else
            {  
                $szUnsubscribedValue = __BASE_STORE_URL__."/myBooking/unsubscribe/".$bookingDataArr['szBookingRandomNum']."/";
                $szUnsubscribedLink="<a href='".$szUnsubscribedValue."'>".t($this->t_base_courier_label.'fields/click_here')."</a>"; 
                $szLabelLink="<a href='".$szLinkValue."'>".t($this->t_base_courier_label.'fields/track_shipping_ref')." ".$bookingDataArr['szBookingRef']."</a>";
            }
            
            $replace_ary['szUrl']=$szLinkValue;
            $replace_ary['szLink']=$szLabelLink;
            $replace_ary['idBooking']=$bookingDataArr['id'];
            $replace_ary['szBookingRef']=$bookingDataArr['szBookingRef'];
            $replace_ary['szName']=$bookingDataArr['szFirstName']." ".$bookingDataArr['szLastName'];
            $replace_ary['szShipperCompany']=$this->szShipperCompanyName;
            $replace_ary['szShipperName']=$this->szShipperFirstName." ".$this->szShipperLastName;
            $replace_ary['szUnsubscribedLink'] = $szUnsubscribedLink;
            $replace_ary['iAdminFlag']=1; 
            $monthName=date('j',strtotime($this->dtShipperPickUp)).". ".getMonthName($bookingDataArr['iBookingLanguage'],(int)date('m',strtotime($this->dtShipperPickUp)));
            $replace_ary['szShippingDate']=$monthName;            
            createEmail(__COURIER_BOOKING_LABELS_RECEIVED_BY_SHIPPER_EMAIL__, $replace_ary,$bookingDataArr['szEmail'], '', __STORE_SUPPORT_EMAIL__,'', __STORE_SUPPORT_EMAIL__,__FLAG_FOR_DEFAULT__);
        }  
        if($bPartnerApi)
        {
            $this->iSuccessFlag = 1;
            return true;
        }
        else
        {
            return true; 
        } 
    }
    
    
    function courierLabelDownloadedByAdmin($data)
    { 
        $this->set_szShipperFirstName(trim(sanitize_all_html_input($data['szShipperFirstName'])));
        $this->set_szShipperLastName(trim(sanitize_all_html_input($data['szShipperLastName'])));
        $this->set_szShipperEmail(trim(sanitize_all_html_input($data['szShipperEmail'])));
       
        if($this->error==true)
       	{
            return false;
        } 
        
        
        $sendEmailFlag=false;
        $kBooking= new cBooking(); 
        $kRegisterShipCon = new cRegisterShipCon();
        $bookingDataArr=$kBooking->getExtendedBookingDetails($data['idBooking']);
        $kUser=new cUser();
       
            $iAddNewUser = false;
            if($kUser->isUserEmailExist($this->szShipperEmail))
            { 
                /*if($kUser->iIncompleteProfileExists)
                {
                    //If user email already exists with the existing email then we just deactivate that account and create a new one.
                    $kUser->deactivateUserAccount($kUser->idIncompleteUser);
                    $iAddNewUser = true; 
                }
                else
                {
                    //give an error that email is already exists
                    $idCustomer = $kUser->idIncompleteUser ;

                    //This session is available only for booking process   
                    $kUser->manualLogin($idCustomer);
                    //$_SESSION['logged_in_by_booking_process'] = 1 ; 
                }*/
                //New Line Added 15/03/2015
                $idCustomer = $kUser->idIncompleteUser ;

                //This session is available only for booking process   
                $kUser->manualLogin($idCustomer);
               // $_SESSION['logged_in_by_booking_process'] = 1 ;
            }
            else
            {
                $iAddNewUser = true;
            }
           
            if($iAddNewUser)
            { 
                    
                $szPassword = mt_rand(0,99)."".$kRegisterShipCon->generateUniqueToken(8);

                //User is not loogedin then we create a new profile
                $addUserAry = array();
                $addUserAry['szFirstName'] = $this->szShipperFirstName ;
                $addUserAry['szLastName'] = $this->szShipperLastName ;
                $addUserAry['szEmail'] = $this->szShipperEmail ;
                $addUserAry['szPassword'] = $szPassword ;
                $addUserAry['szConfirmPassword'] = $szPassword ;
                $addUserAry['szPostCode'] = $bookingDataArr['szShipperPostcode'] ;
                $addUserAry['szCity'] = $bookingDataArr['szShipperCity'] ; 
                $addUserAry['szState'] = $bookingDataArr['szShipperState']; 
                $addUserAry['szCountry'] = $bookingDataArr['idShipperCountry'] ; 
                $addUserAry['szPhoneNumberUpdate'] = $bookingDataArr['szShipperPhone'] ;
                $addUserAry['szCompanyName'] = $bookingDataArr['szShipperCompanyName'] ;  
                $addUserAry['szAddress1'] = $bookingDataArr['szShipperAddress'] ;
                $addUserAry['szCurrency'] = $bookingDataArr['idCustomerCurrency'];
                //$addUserAry['iThisIsMe'] = $data['iThisIsMe'];
                if($this->szShipperEmail==$bookingDataArr['szShipperEmail'])
                {    
                    $addUserAry['iConfirmed'] = 1; 
                }
                else
                {
                      $sendEmailFlag=true;
                     $addUserAry['iConfirmed'] = 0;
                }
                $addUserAry['iFirstTimePassword'] = 1;  
                $addUserAry['idInternationalDialCode'] = $bookingDataArr['idShipperCountry'] ;
                 //print_r($addUserAry);   
                if($kUser->createAccount($addUserAry,'LABEL_DOWNLOAD_PROCESS'))
                { 
                    $idCustomer = $kUser->id ;  
                    $kUser->manualLogin($idCustomer);

                    //$this->updateCapsuleCrmDetails($idCustomer);

                    //$_SESSION['logged_in_by_booking_process'] = 1 ; 
                } 
            }
       
        
        if(!$iAddNewUser)
        { 
            $kUser->getUserDetails($idCustomer);

            $updateUserDetailsAry = array(); 
            $updateUserDetailsAry['szFirstName'] = $this->szShipperFirstName ;
            $updateUserDetailsAry['szLastName'] = $this->szShipperLastName ; 
            if($this->szShipperEmail!=$bookingDataArr['szShipperEmail'])
            {    
                 $sendEmailFlag=true;
            }

            if($kUser->iConfirmed!=1)
            {
                $updateUserDetailsAry['szEmail'] = $this->szShipperEmail ; 
            }  
            if(!empty($updateUserDetailsAry))
            {
                $update_user_query = '';
                foreach($updateUserDetailsAry as $key=>$value)
                {
                    $update_user_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            } 
            $update_user_query = rtrim($update_user_query,","); 
           // echo $update_user_query;
            if($kRegisterShipCon->updateUserDetailsByQuotes($update_user_query,$idCustomer))
            {
                //$this->updateCapsuleCrmDetails($idCustomer);
            }  
        }
        
        if((int)$bookingDataArr['idLabelDownloadedBy']==0)
        {
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_BOOKING__."
                SET
                    idLabelDownloadedBy='".(int)$idCustomer."',
                    dtLabelDownloaded=NOW()
                WHERE
                    id='".(int)$data['idBooking']."'
            ";
            $result = $this->exeSQL($query);
                    
           $query="
                UPDATE
                    ".__DBC_SCHEMATA_FILE_LOGS__."
                SET
                    szTransportecaStatus='S9',
                    iLabelStatus=".__LABEL_DOWNLOAD_FLAG__.",
                    iStatus='6'
                WHERE
                    idBooking='".(int)$data['idBooking']."'
            ";
            $result = $this->exeSQL($query);         
        }
        
        if($sendEmailFlag)
        { 
            $kConfig = new cConfig();
            $kTracking = new cTracking();
            $idCourierProvider = $kTracking->getIdCourierProviderByIdServiceProvider($bookingDataArr['idServiceProvider']);
            
            if($bookingDataArr['idDestinationCountry']>0 && $idCourierProvider==__TNT_API__)
            {
                $kConfig->loadCountry($bookingDataArr['idDestinationCountry']);
                if(!empty($kConfig->szCountryISO3Code))
                {
                    $szCountryISO3Code = ":".$kConfig->szCountryISO3Code;
                } 
            }
            
            $replace_ary=array();
            //print_r($bookingDataArr);
            $replace_ary['iBookingLanguage']=__LANGUAGE_ID_ENGLISH__;
            $szLinkValue = __AFTER_SHIP_TRACKING_URL__."/".$bookingDataArr['szTrackingNumber']."".$szCountryISO3Code;
            $kConfig =new cConfig();
            $langArr=$kConfig->getLanguageDetails('',$bookingDataArr['iBookingLanguage']);
            
            if(!empty($langArr) && $bookingDataArr['iBookingLanguage']!=__LANGUAGE_ID_ENGLISH__)
            { 
                $szLanguageCode=$langArr[0]['szLanguageCode'];
               
                $szLabelLink="<a href='".$szLinkValue."'>".t($this->t_base_courier_label.'fields/track_shipping_ref')."</a>";  
                $szUnsubscribedValue = __BASE_STORE_URL__."/".$szLanguageCode."/myBooking/unsubscribe/".$bookingDataArr['szBookingRandomNum']."/";
                $szUnsubscribedLink="<a href='".$szUnsubscribedValue."'>".t($this->t_base_courier_label.'fields/click_here')."</a>"; 
                $replace_ary['iBookingLanguage']=$bookingDataArr['iBookingLanguage']; 
            }
            else
            {  
                $szUnsubscribedValue = __BASE_STORE_URL__."/myBooking/unsubscribe/".$bookingDataArr['szBookingRandomNum']."/";
                $szUnsubscribedLink="<a href='".$szUnsubscribedValue."'>".t($this->t_base_courier_label.'fields/click_here')."</a>"; 
                $szLabelLink="<a href='".$szLinkValue."'>".t($this->t_base_courier_label.'fields/track_shipping_ref')."</a>";
            }
            
            $replace_ary['szUrl']=$szLinkValue;
            $replace_ary['szLink']=$szLabelLink;
            $replace_ary['szBookingRef']=$bookingDataArr['szBookingRef'];
            $replace_ary['szName']=$bookingDataArr['szFirstName']." ".$bookingDataArr['szLastName'];
            $replace_ary['szShipperCompany']=$this->szShipperCompanyName;
            $replace_ary['szShipperName']=$this->szShipperFirstName." ".$this->szShipperLastName;
            $replace_ary['szUnsubscribedLink'] = $szUnsubscribedLink;
            
            //$iLanguage = getLanguageId();
            $monthName=date('d',strtotime($this->dtShipperPickUp)).". ".getMonthName($bookingDataArr['iBookingLanguage'],date('m',strtotime($this->dtShipperPickUp)))." ".date('Y',strtotime($this->dtShipperPickUp));
            $replace_ary['szShippingDate']=$monthName;            
            createEmail(__COURIER_BOOKING_LABELS_RECEIVED_BY_SHIPPER_EMAIL__, $replace_ary,$bookingDataArr['szEmail'], '', __STORE_SUPPORT_EMAIL__,'', __STORE_SUPPORT_EMAIL__,__FLAG_FOR_DEFAULT__);
        } 
        return true;
          
    }
    
    function updateBookingCourierStatus($trackingStatusArr,$idBooking)
    {
         $query="
                UPDATE
                    ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__."
                SET
                    szCourierBookingStatusText='".mysql_escape_custom($trackingStatusArr['Description'])."',
                    szCourierBookingStatusCode='".mysql_escape_custom($trackingStatusArr['Code'])."'
                WHERE
                    idBooking='".(int)$idBooking."'
            ";
         //Courierecho $query;
            $result = $this->exeSQL($query);
    } 
    
    function deleteCourierLabelPdfFile($idFile,$idBooking,$idBookingFile)
    {
        $query="
               DELETE FROM
                   ".__DBC_SCHEMATA_COURIER_LABEL_UPLOADED_DATA__."
               WHERE
                   idBooking='".(int)$idBooking."'
               AND
                    id='".(int)$idFile."'
               AND
                    idFile='".(int)$idBookingFile."'
           ";
           $result = $this->exeSQL($query); 
           
           $query="
               UPDATE
                   ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__."
               SET
                   iTotalPage=iTotalPage-1
               WHERE
                   idBooking='".(int)$idBooking."'
           ";
           $result = $this->exeSQL($query);
    }
    
    function checkCourierProviderCredentials($bForwarderFlag=false)
    { 
        $courierAgreementArr = array();
        $courierAgreementArr=$this->getCourierAgreementData();  
         
        if(!empty($courierAgreementArr))
        {
            foreach($courierAgreementArr as $courierAgreementArrs)
            { 
                $idCourierAgreement = $courierAgreementArrs['id']; 
                
                $courierProviderAgreementArr = array();
                $courierProviderAgreementArr['szUsername'] = $courierAgreementArrs['szUsername'];
                $courierProviderAgreementArr['szAccountNumber'] = $courierAgreementArrs['szAccountNumber'];
                $courierProviderAgreementArr['szPassword'] = $courierAgreementArrs['szPassword'];
                $courierProviderAgreementArr['szMeterNumber'] = $courierAgreementArrs['szMeterNumber'];
                $courierProviderAgreementArr['szCarrierAccountID'] = $courierAgreementArrs['szCarrierAccountID'];
                $courierProviderAgreementArr['idCourierProvider'] = $courierAgreementArrs['idCourierProvider'];
                
                $kWebservices = new cWebServices();
                if($kWebservices->checkCourierApiCredentials($courierProviderAgreementArr,$bForwarderFlag))
                {
                    //Everything is alright  
                    $this->isCourierAgreementDataComplete($courierAgreementArrs);
                } 
                else
                { 
                    //Error From API
                    $szAgrrementStatus = 'Authentication Failed';
                    $this->updateAgreementStatus($szAgrrementStatus,$idCourierAgreement);
                }  
            }
        }
        return true;
    }
    
    function isCourierAgreementDataComplete($courierAgreementArrs)
    { 
        if(!empty($courierAgreementArrs))
        {
            /* 
            * checks are performed as follws
            * Step - 1: checks if service covered data not exists then just update status: 'No Service' and no need to check further
            * Step - 2: checks if trade offered data not exists then just update status: 'No Trade' and no need to check further
            * Step - 3: checks if pricing data not exists then update: 'No Pricing' 
            */

            $idCourierAgreement = $courierAgreementArrs['id']; 
            $szCarrierAccountID = $courierAgreementArrs['szCarrierAccountID'];
            
            $servicDataArr = array();
            $servicDataArr=$this->getCourierAgreeServiceData($idCourierAgreement); 
            
            if(!empty($servicDataArr))
            {
                $tradesDataArr = array();
                $tradesDataArr=$this->getCourierAgreeTradeData($idCourierAgreement);  
                
                if(!empty($tradesDataArr))
                {
                    $workingFlag=false; 
                    $pricingDataAry = array();
                    //$pricingDataAry = $this->loadCourierAgreementDetails($idCourierAgreement);
                    $pricingDataAry = $servicDataArr; 
                     
                    if(!empty($pricingDataAry))
                    {
                        foreach($pricingDataAry as $pricingDataArys)
                        {
                            if(!$workingFlag)
                            {
                                if($pricingDataArys['iBookingIncluded']!=0 || $pricingDataArys['fMarkupPercent']!=0 || $pricingDataArys['fMinimumMarkup']!=0 || $pricingDataArys['fMarkupperShipment']!=0)
                                {
                                    $workingFlag=true;
                                    break;
                                }
                            }
                        }
                    } 
                    if(!$workingFlag)
                    {
                        $szAgrrementStatus = __AGREEMENT_NO_PRICING__;
                    }
                    else
                    {
                        if(!empty($szCarrierAccountID))
                        {
                            $szAgrrementStatus = __AGREEMENT_WORKING__;
                        }
                        else
                        {
                            $szAgrrementStatus = __AGREEMENT_PENDING_REVIEW__;
                        } 
                    }
                }
                else
                {
                    $szAgrrementStatus = __AGREEMENT_NO_TRADES__;
                }
            }
            else
            {
                $szAgrrementStatus = __AGREEMENT_NO_SERVICE__;
            }
            
            if(!empty($szCarrierAccountID))
            {
                $szManagementStatus = $szAgrrementStatus;
            }
            else
            {
                /*
                * in Management - made the status “Pending review” immediately, also if user has not completed Services, Trades and Pricing – so it always has that status if Carrier Account ID is missing – but this is only in management
                */
                $szManagementStatus = __AGREEMENT_PENDING_REVIEW__;
            }
            $this->updateAgreementStatus($szAgrrementStatus,$idCourierAgreement,$szManagementStatus);
            return $szAgrrementStatus;
        }
    }
    
    function updateAgreementStatus($szAgrrementStatus,$idCourierAgreement,$szManagementStatus)
    {
        if($idCourierAgreement>0)
        {
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_COUIER_AGREEMENT_DATA__."
                SET
                    szStatus = '".mysql_escape_custom($szAgrrementStatus)."',
                    szManagementStatus = '".mysql_escape_custom($szManagementStatus)."'
                WHERE
                    id='".(int)$idCourierAgreement."'
            ";
            //echo $query;
            if($result = $this->exeSQL( $query ))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    
    function checkTradeAlreadyExists($data)
    {
        $this->set_idCountryFrom(sanitize_all_html_input(trim($data['idCountryFrom'])));
        $this->set_iTrade(sanitize_all_html_input(trim($data['iTrade'])));
        $queryWhere='';
        if((int)$data['id']>0)
        {
            $queryWhere="
                AND
                    id<>'".(int)$data['id']."'
            ";
        }
        $query="
            SELECT
                id
            FROM
                ".__DBC_SCHEMATA_COUIER_TRADES_OFFERED_DATA__."
            WHERE
                 idCountryFrom='".mysql_escape_custom($this->idCountryFrom)."'
            AND         
                iTrade='".(int)$this->iTrade."'
            AND
                idCourierAgreement='".(int)$data['idCourierAgreeProvider']."'
            AND
                isDeleted='0'
            $queryWhere        
        ";
       // echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
               return false;    
            }
            else
            {
                return true;
            }
        }
    }
    
    function getCourierProductGroupByidProvider($idCourierProvider,$id=0)
    {
        $query='';
        if((int)$id>0)
        {
            $query="
                AND
                    id='".(int)$id."'
                ";
        }
        $query="
            SELECT
                id,
                szName
            FROM
                ".__DBC_SCHEMATA_COURIER_PRODUCT_GROUP__."
            WHERE
                idCourierProvider='".(int)$idCourierProvider."'
            AND
                isDeleted='0'
            $query    
        ";
        //echo $query;
        if($result = $this->exeSQL($query))
        {
            if($this->getRowCnt()>0)
            {
                 while($row = $this->getAssoc($result))
                 {
                     $resArr[]=$row;
                 }
                 
                 return $resArr;
            }
            else
            {
                return array();
            }
        }
        else
        {
            return array();
        }
    }
    
    function addUpdateProductGroupData($data)
    {
        $this->set_szGroupName(sanitize_all_html_input(trim($data['szName'])));
       // print_r($this);
        if($this->error==true)
       	{
            return false;
        }  
        if((int)$data['id']>0)
        {
            $query="
                UPDATE
                     ".__DBC_SCHEMATA_COURIER_PRODUCT_GROUP__."
                SET
                    szName='".  mysql_escape_custom($this->szGroupName)."',
                    dtUpdated=NOW()
             WHERE
                id='".(int)$data['id']."'
            ";
        }
        else
        {
            $query="
                INSERT INTO
                     ".__DBC_SCHEMATA_COURIER_PRODUCT_GROUP__."
                (
                    szName,
                    dtCreated,
                    idCourierProvider
                )
                    VALUES
                (
                    '".  mysql_escape_custom($this->szGroupName)."',
                    NOW(),
                    '".(int)$data['idCourierProvider']."'
                )
             
            ";
            //echo $query;
        }
        if($result = $this->exeSQL($query))
        {
             return true;
        }
        else
        {
            return false;
        }
    }
    
    function deleteCourierProductGroup($id)
    {
        $query="
            UPDATE
                ".__DBC_SCHEMATA_COURIER_PRODUCT_GROUP__."
            SET        
                isDeleted='1'
            WHERE
                id='".(int)$id."'
        ";
        if($result = $this->exeSQL($query))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    
    function getGroupIdCourierProviderProduct($id=0)
    { 
    	$query="
            SELECT
               idGroup,
               szApiCode
            FROM	
                ".__DBC_SCHEMATA_COUIER_PROVIDER_PRODUCTS__."         
            WHERE
                id='".(int)$id."'	
            AND
                isDeleted='0'	
          	";
    	//echo $query;
    	if(($result = $this->exeSQL($query)))
        {
            if($this->getRowCnt()>0)
            {
                $ret_ary = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    $ret_ary[] = $row ;
                    $ctr++;
                }
                return $ret_ary;
            }
            else
            {
            	return array();
            }
        }
        else
        {
            return array();
        } 
    }
    
    function getCourierProductProviderPricingData($data,$szApiCode,$idProviderProduct=false,$idCourierAgreement=false)
    {
        if($idProviderProduct>0)
        {
            $query_and = " AND cpp.id = '".(int)$idProviderProduct."' ";
        } 
        
        if($idCourierAgreement>0)
        {
            $query_and .= " AND cpso.idCourierAgreement = '".(int)$idCourierAgreement."' ";
        } 
        else if($data['idCourierAgreement']>0)
        {
            $query_and .= " AND cpso.idCourierAgreement='".(int)$data['idCourierAgreement']."' ";
        } 
        if(!empty($szApiCode))
        {
            $query_and .= " AND cpp.szApiCode='".mysql_escape_custom($szApiCode)."' ";
        }
        else if($data['idCourierProvider']>0)
        {
            $query_and .= " AND cpp.idCourierProvider = '".mysql_escape_custom($data['idCourierProvider'])."' ";
        }
        
        
        $query="
            SELECT
                cpso.iBookingIncluded,
                cpso.fMarkupPercent,
                cpso.fMinimumMarkup,
                cpso.fMarkupperShipment,
                cpso.idCourierAgreement
            FROM    
                ".__DBC_SCHEMATA_COUIER_SERVICES_OFFERED_DATA__." AS cpso
            INNER JOIN
                ".__DBC_SCHEMATA_COUIER_PROVIDER_PRODUCTS__." AS cpp
            ON
                cpp.id=cpso.idCourierProviderProductid
            WHERE         
                cpso.idForwarder='".(int)$data['idForwarder']."' 
            AND
                cpp.isDeleted='0'
             AND
                cpso.isDeleted='0'
                $query_and
        ";
        //echo $query."<br/><br/>";
        if(($result = $this->exeSQL($query)))
        {
            if($this->getRowCnt()>0)
            {
                $ret_ary = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    $ret_ary[] = $row ;
                    $ctr++;
                }
                return $ret_ary;
            }
            else
            {
                return array();
            }
        }
        else
        {
            return array();
        } 
    }
    
    function getAllProductByApiCodeMapping($bGetNameByID=false)
    {
        $query="
            SELECT 
                products.id,
                products.szName as szCourierProductName,
                products.szApiCode as szCourierAPICode,
                products.idPacking as idCourierPacking,
                products.iDaysStd, 
                products.idGroup,
                products.iDaysMin
            FROM 
                ".__DBC_SCHEMATA_COUIER_PROVIDER_PRODUCTS__." products 
            WHERE 
                products.isDeleted='0' 
    	"; 
    	if(($result=$this->exeSQL($query)))
        {
            if($this->getRowCnt()>0)
            {
                $ret_ary = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {  
                    if($bGetNameByID)
                    {
                        $ret_ary[$row['id']] = $row; 
                        $ctr++;
                    }
                    else
                    {
                        $ret_ary[$row['szCourierAPICode']] = $row; 
                        $ctr++;
                    } 
                }
                return $ret_ary;
            }
            else
            {
            	return array();
            }
        }
        else
        {
                return array();
        } 
    }
    
    function validateAddressFromGoogle($data)
    {
        if(!empty($data))
        { 
            $szShipperAddress = $data['szShipperPostCode']." ".$data['szShipperCity']." ".$data['szShipperCountry'];
            $szConsigneeAddress = $data['szConsigneePostCode']." ".$data['szConsigneeCity']." ".$data['szConsigneeCountry'];
             
            $originArr=reverse_geocode($szShipperAddress); 
            $destinationArr=reverse_geocode($szConsigneeAddress);  
            
            $iReturnFlag = false;
            
            $originArr['szPostCode'] =trim($originArr['szPostCode']);
            $originArr['szCityName'] =trim($originArr['szCityName']);
            $destinationArr['szCityName'] =trim($destinationArr['szCityName']);
            $destinationArr['szPostCode'] =trim($destinationArr['szPostCode']); 
            $kConfig = new cConfig();
            if(empty($originArr['szPostCode']))
            {  
                if($kConfig->isCityExistsByPostCode($data['szShipperCity'],$data['idShipperCountry'],$data['szShipperPostCode']))
                {
                    //Postcode exists
                }
                else
                {
                    //We donot found postcode neigher from google nor from our database
                    $iReturnFlag = 1;
                }
            } 
            
            if(empty($destinationArr['szPostCode']))
            {
                if($kConfig->isCityExistsByPostCode($data['szConsigneeCity'],$data['idConsigneeCountry'],$data['szConsigneePostCode']))
                {
                    //Postcode exists
                }
                else
                {
                    //We donot found postcode neigher from google nor from our database
                    $iReturnFlag = 1;
                }
            }  
            return $iReturnFlag ;
        }
    }
    function validateUpsAddressFromAPI($data,$iCheckFromGoogle=false)
    { 
        if(!empty($data))
        {
            $iCheckFromGoogle = 1;
            if($iCheckFromGoogle==1)
            {
                //If we get any exception from UPS then we call Google API to check if shipper/consignee address is valid or not
                if($this->validateAddressFromGoogle($data))
                { 
                    return true;
                }
                else
                {
                    return false;
                }
            }
            
            //Configuration
            $access = __UPS_ACCESS_KEY__;
            $userid = __UPS_API_USER_ID__;
            $passwd = __UPS_API_PASSWORD__; 
            $wsdl = __APP_PATH__."/wsdl/XAV.wsdl";
            $operation = "ProcessXAV";
            $endpointurl = 'https://onlinetools.ups.com/webservices/XAV';
            $outputFileName = __APP_PATH__."/wsdl/XOLTResult.xml"; 
            
            try
            {   
                $mode = array
                (
                     'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
                     'trace' => 1
                );

                 // initialize soap client
                $client = new SoapClient($wsdl , $mode);

                //set endpoint url
                $client->__setLocation($endpointurl);
 
                //create soap header
                $usernameToken['Username'] = $userid;
                $usernameToken['Password'] = $passwd;
                $serviceAccessLicense['AccessLicenseNumber'] = $access;
                $upss['UsernameToken'] = $usernameToken;
                $upss['ServiceAccessToken'] = $serviceAccessLicense;

                $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0','UPSSecurity',$upss);
                $client->__setSoapHeaders($header);
                 
                $request = $this->processXAV($data);
                
                try
                {
                    $resp = $client->__soapCall($operation ,array($request));
                } 
                catch (Exception $ex) 
                {
                    $outputFileName = __APP_PATH__."/wsdl/ups_api_exception.log";
                    //save soap request and response to file
                    $fw = fopen($outputFileName , 'w'); 
                    fwrite($fw , "Exception: \n" . print_R($ex,true) . "\n");
                    fclose($fw); 
                } 

                print_R($resp);
                //get status
                echo "Response Status: " . $resp->Response->ResponseStatus->Description ."\n";
                
                $requestFileName = __APP_PATH__."/wsdl/ups_api_request.xml";
                $responseFileName = __APP_PATH__."/wsdl/ups_api_response.xml";
            
                //save soap request and response to file
                $fw = fopen($requestFileName , 'w');
                fwrite($fw , "Request: \n" . $client->__getLastRequest() . "\n");
                fclose($fw);
                
                $fw = fopen($responseFileName , 'w');
                fwrite($fw , "Response: \n" . $client->__getLastResponse() . "\n");
                fclose($fw);
                echo "dieing in try ";
                die;
                
            } catch (Exception $ex) {
                
                $requestFileName = __APP_PATH__."/wsdl/ups_api_request.xml";
                $responseFileName = __APP_PATH__."/wsdl/ups_api_response.xml";
            
                //save soap request and response to file
                $fw = fopen($requestFileName , 'w');
                fwrite($fw , "Request: \n" . $client->__getLastRequest() . "\n");
                fclose($fw);
                
                $fw = fopen($responseFileName , 'w');
                fwrite($fw , "Response: \n" . $client->__getLastResponse() . "\n");
                fclose($fw);
                
                $outputFileName = __APP_PATH__."/wsdl/ups_api_exception.log";
                 //save soap request and response to file
                $fw = fopen($outputFileName , 'w'); 
                fwrite($fw , "Exception: \n" . print_R($ex,true) . "\n");
                fclose($fw); 
                
                print_R($ex);
                die;
            }
        }
    }
    
    function processXAV($data)
    {
        //create soap request
        $option['RequestOption'] = '1';
        $request['Request'] = $option;
 
        $addrkeyfrmt['ConsigneeName'] = $data['szFirstName']." ".$data['szLastName'];
        $addrkeyfrmt['AddressLine'] = array ( $data['szAddress'] ); 
        $addrkeyfrmt['PoliticalDivision2'] = $data['szCity'];
        $addrkeyfrmt['PoliticalDivision1'] = $data['szState'];
        $addrkeyfrmt['PostcodePrimaryLow'] = $data['szPostCode'];
        $addrkeyfrmt['CountryCode'] = $data['szCountryCode'];
        $request['AddressKeyFormat'] = $addrkeyfrmt; 
        return $request;
    }
    function deleteExludedTrades($idExcludedTrades)
    {
        if(!empty($idExcludedTrades))
        { 
            $excludedIdAry = explode(";",$idExcludedTrades);   
            if(!empty($excludedIdAry))
            {
                $excludeIdList = implode(',',$excludedIdAry);
                $query_and = " id IN (".$excludeIdList.") ";
            }
            else
            {
                return false;
            }
            
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_COURIER_EXCLUDED_TRADES__."
                SET
                    isDeleted = '1', 
                    dtUpdatedOn = now()
                WHERE
                    $query_and
            ";
//            echo $query;
//            die;
            
            if($result = $this->exeSQL( $query ))
            {
                return true;
            } 
            else
            {
                return false;
            } 
        }
    }
    function addEditCourierExludedTrades($data)
    {
        if(!empty($data))
        { 
            $this->set_idCourierProvider(sanitize_all_html_input(trim($data['idCourierProvider']))); 
            $this->set_idForwarder(sanitize_all_html_input(trim($data['idForwarder']))); 
            $this->set_idOriginCountry(sanitize_all_html_input(trim($data['idOriginCountry']))); 
            $this->set_idDestinationCountry(sanitize_all_html_input(trim($data['idDestinationCountry']))); 
            $this->set_idExludedTrades(sanitize_all_html_input(trim($data['idExludedTrades']))); 
            $this->set_iCustomerType(sanitize_all_html_input(trim($data['iCustomerType']))); 
            
            if($this->error==true)
            {
                return false;
            } 
            
            $kConfig = new cConfig(); 
            $kForwarder = new cForwarder();
            $kForwarder->load($this->idForwarder);
            
            $this->szForwarderDisName = $kForwarder->szDisplayName;
            $this->szForwarderAlies = $kForwarder->szForwarderAlies;
             
            $courierProviderAry = array();
            $courierProviderAry = $this->loadCourierServiceProvider($this->idCourierProvider);
            
            $this->szCourierProviderName = $courierProviderAry['szProviderName'];
            
            
            $fromArr=explode("_",$this->idOriginCountry);
            
            $fromCountryFlag = true;
            $toCountryFlag = true;
            
            if($fromArr[1]=='r')
            {
                $fromCountryArr=$this->getCountriesByIdRegion($fromArr[0]);
                $fromCountryFlag=false;
            }
            else
            {
                $fromCountryArr[0]=$this->idOriginCountry;
                $kConfig->loadCountry($this->idOriginCountry);
                $this->szOriginCountry = $kConfig->szCountryName;
            }
            
            $toArr=explode("_",$this->idDestinationCountry);
            if($toArr[1]=='r')
            {
                $toCountryArr=$this->getCountriesByIdRegion($toArr[0]);
                $toCountryFlag = false;
            }
            else
            {
                $toCountryArr[0]=$this->idDestinationCountry;
                
                $kConfig->loadCountry($this->idDestinationCountry);
                $this->szDestinationCountry = $kConfig->szCountryName;
            }
            
            if($toCountryFlag && $fromCountryFlag)
            {
                $excludedTradeSearch = array();
                $excludedTradeSearch['idCourierProvider'] = $this->idCourierProvider;
                $excludedTradeSearch['idForwarder'] = $this->idForwarder;
                $excludedTradeSearch['idOriginCountry'] = $this->idOriginCountry;
                $excludedTradeSearch['idDestinationCountry'] = $this->idDestinationCountry;
                $excludedTradeSearch['iCustomerType'] = $this->iCustomerType;
                
                //If both from and is individual country then we do like we were doing before,We just perform normal add/update
                if($this->idExludedTrades>0)
                {
                    if($this->isTradeAddedToExcludedList($excludedTradeSearch,$this->idExludedTrades))
                    {
                        $this->addError('szSpecialError', t($this->t_base_courier.'fields/combination_already_exists') );
                        return false;
                    } 
                    $query="
                        UPDATE
                            ".__DBC_SCHEMATA_COURIER_EXCLUDED_TRADES__."
                        SET
                            idCourierProvider = '".mysql_escape_custom($this->idCourierProvider)."',
                            idOriginCountry = '".(int)$this->idOriginCountry."',
                            szOriginCountry = '".mysql_escape_custom($this->szOriginCountry)."',
                            idDestinationCountry = '".mysql_escape_custom($this->idDestinationCountry)."',
                            szDestinationCountry = '".mysql_escape_custom($this->szDestinationCountry)."',
                            idForwarder = '".mysql_escape_custom($this->idForwarder)."',
                            szForwarderDisName = '".mysql_escape_custom($this->szForwarderDisName)."',
                            szForwarderAlies = '".mysql_escape_custom($this->szForwarderAlies)."',
                            szCourierProviderName = '".mysql_escape_custom($this->szCourierProviderName)."',
                            iCustomerType = '".mysql_escape_custom($this->iCustomerType)."',
                            dtUpdatedOn = now(),
                            iActive = '1'
                        WHERE
                            id='".(int)$this->idExludedTrades."'
                    ";
                }
                else
                {
                    if($this->isTradeAddedToExcludedList($excludedTradeSearch))
                    {
                        $this->addError('szSpecialError', t($this->t_base_courier.'fields/combination_already_exists') );
                        return false;
                    }

                    $query="
                        INSERT INTO
                            ".__DBC_SCHEMATA_COURIER_EXCLUDED_TRADES__."
                        (
                            idCourierProvider,
                            idOriginCountry,
                            szOriginCountry,
                            idDestinationCountry,
                            szDestinationCountry,
                            idForwarder,
                            szForwarderDisName,
                            szForwarderAlies,
                            szCourierProviderName,
                            iCustomerType,
                            iActive,
                            dtCreatedOn
                        )
                        VALUES
                        (
                            '".mysql_escape_custom($this->idCourierProvider)."',
                            '".mysql_escape_custom($this->idOriginCountry)."',
                            '".mysql_escape_custom($this->szOriginCountry)."',
                            '".mysql_escape_custom($this->idDestinationCountry)."',
                            '".mysql_escape_custom($this->szDestinationCountry)."',
                            '".mysql_escape_custom($this->idForwarder)."', 
                            '".mysql_escape_custom($this->szForwarderDisName)."',
                            '".mysql_escape_custom($this->szForwarderAlies)."',
                            '".mysql_escape_custom($this->szCourierProviderName)."',
                            '".mysql_escape_custom($this->iCustomerType)."', 
                            1,
                            now()
                        )
                    ";		 
                }
                //echo $query;
                //die;
                if(($result = $this->exeSQL($query)))
                {
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
            else
            {     
                //If any of from or to is region then we only Insert trades that is not added yet.
                if(!empty($toCountryArr) && !empty($fromCountryArr))
                {
                    foreach($fromCountryArr as $fromCountryArrs)
                    {
                        $idOriginCountry = $fromCountryArrs; 
                        foreach($toCountryArr as $toCountryArrs)
                        { 
                            $idDestinationCountry = $toCountryArrs; 

                            $excludedTradeSearch = array();
                            $excludedTradeSearch['idCourierProvider'] = $this->idCourierProvider;
                            $excludedTradeSearch['idForwarder'] = $this->idForwarder;
                            $excludedTradeSearch['idOriginCountry'] = $idOriginCountry;
                            $excludedTradeSearch['idDestinationCountry'] = $idDestinationCountry;
                            $excludedTradeSearch['iCustomerType'] = $this->iCustomerType;
                            
                            if($this->isTradeAddedToExcludedList($excludedTradeSearch))
                            {
                                //$this->addError('szSpecialError', t($this->t_base_courier.'fields/combination_already_exists') );
                                //return false;
                                continue;
                            }
                            
                            $this->idOriginCountry = $idOriginCountry;
                            $this->idDestinationCountry = $idDestinationCountry;
                            
                            $kConfig = new cConfig();
                            $kConfig->loadCountry($this->idOriginCountry);
                            $this->szOriginCountry = $kConfig->szCountryName;

                            $kConfig = new cConfig();
                            $kConfig->loadCountry($this->idDestinationCountry);
                            $this->szDestinationCountry = $kConfig->szCountryName;

                            $query="
                                INSERT INTO
                                    ".__DBC_SCHEMATA_COURIER_EXCLUDED_TRADES__."
                                (
                                    idCourierProvider,
                                    idOriginCountry,
                                    szOriginCountry,
                                    idDestinationCountry,
                                    szDestinationCountry,
                                    idForwarder,
                                    szForwarderDisName,
                                    szForwarderAlies,
                                    szCourierProviderName,
                                    iCustomerType,
                                    iActive,
                                    dtCreatedOn
                                )
                                VALUES
                                (
                                    '".mysql_escape_custom($this->idCourierProvider)."',
                                    '".mysql_escape_custom($this->idOriginCountry)."',
                                    '".mysql_escape_custom($this->szOriginCountry)."',
                                    '".mysql_escape_custom($this->idDestinationCountry)."',
                                    '".mysql_escape_custom($this->szDestinationCountry)."',
                                    '".mysql_escape_custom($this->idForwarder)."', 
                                    '".mysql_escape_custom($this->szForwarderDisName)."',
                                    '".mysql_escape_custom($this->szForwarderAlies)."',
                                    '".mysql_escape_custom($this->szCourierProviderName)."',
                                    '".mysql_escape_custom($this->iCustomerType)."', 
                                    1,
                                    now()
                                )
                            ";	 
//                            echo $query;
//                            die;
                            $result = $this->exeSQL($query); 
                        }
                    }
                } 
                return true;
            }  
        }
    }
    
    function isTradeAddedToExcludedList($data,$idExcludedTrades=false,$ret_ary=false )
    {
        if(!empty($data))
        { 
            if($data['iCustomerType']>0)
            {
                $query_and = " AND (iCustomerType = '".__COURIER_EXCLUDED_TRADE_CUSTOMER_TYPE_PRIVATE_AND_BUSSINESS__."' OR iCustomerType = '".(int)$data['iCustomerType']."') ";
            }
            else
            {
                $query_and = " AND iCustomerType = '".__COURIER_EXCLUDED_TRADE_CUSTOMER_TYPE_PRIVATE_AND_BUSSINESS__."' ";
            }
            
            if(!$ret_ary)
            {
                $query_and .= " AND idCourierProvider = '".mysql_escape_custom($data['idCourierProvider'])."' AND idForwarder = '".mysql_escape_custom($data['idForwarder'])."' ";
            }
            
            $query="
                SELECT
                    id,
                    idCourierProvider,
                    idForwarder
                FROM
                    ".__DBC_SCHEMATA_COURIER_EXCLUDED_TRADES__."
                WHERE 
                    idOriginCountry = '".mysql_escape_custom($data['idOriginCountry'])."'
                AND
                    idDestinationCountry = '".mysql_escape_custom($data['idDestinationCountry'])."' 
                AND
                    isDeleted='0'
                 $query_and
            ";
            //echo $query;
            if($idExcludedTrades>0)
            {
                $query .="
                    AND
                        id<>'".(int)$idExcludedTrades."'		
                ";
            } 
            
            if(($result = $this->exeSQL($query)))
            {
                // Check if the user exists
                if( $this->getRowCnt() > 0 )
                {
                    if($ret_ary)
                    {
                        $returnAry = array();
                        $ctr = 0;
                        while($row=$this->getAssoc($result))
                        {  
                            $returnAry[$ctr] = $row['idForwarder']."_".$row['idCourierProvider']; 
                            $ctr++;
                        }
                        return $returnAry;
                    }
                    else
                    {
                        return true;
                    } 
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
     
    function getAllExcludedTrades($idExcludedTrade=false)
    { 
        if($idExcludedTrade>0)
        {
            $query_and = " AND id = '".(int)$idExcludedTrade."' ";
        }
        
        $query="
            SELECT
                id,
                idCourierProvider,
                idOriginCountry,
                szOriginCountry,
                idDestinationCountry,
                szDestinationCountry,
                idForwarder,
                szForwarderDisName,
                szForwarderAlies,
                szCourierProviderName,
                iCustomerType,
                iActive,
                dtCreatedOn
            FROM
                ".__DBC_SCHEMATA_COURIER_EXCLUDED_TRADES__."
            WHERE 
                isDeleted = '0' 
                $query_and
            ORDER BY
                szCourierProviderName ASC,
                szForwarderDisName ASC,
                szOriginCountry ASC,
                szDestinationCountry ASC
        "; 
        //echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        { 
            if($this->iNumRows>0)
            { 
                $ctr = 0;
                while($row = $this->getAssoc($result))
                {
                    $retAry[$ctr] = $row;
                    $ctr++;
                }
                return $retAry ;
            }
            else
            {
                return array();
            }
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        } 
    }
    
    function splitUploadedLabels($filename, $end_directory,$bForwarder=false,$bTestPage=false,$bLabelPdf=false)
    { 
        require_once(__APP_PATH__.'/forwarders/html2pdf/html2pdfBooking.php');
        require_once(__APP_PATH__.'/pdfLib/fpdi/fpdi.php');
        require_once(__APP_PATH__.'/pdfLib/fpdi/pdf_parser.php');
        require_once(__APP_PATH__.'/pdfLib/fpdi/fpdi_pdf_parser.php'); 
      
        /*
         * $end_directory: This is the location where we have puted pdf file after successful upload.
         */
        if (!is_dir($end_directory))
        { 
            // Will make directories under end directory that don't exist
            // Provided that end directory exists and has the right permissions            
           mkdir($end_directory, 0777, true);
           chmod($end_directory, 0777);
        } 
        $szSourceFilePath = $end_directory."".$filename;  
        
        try
        { 
            $pdf_parser = new fpdi_pdf_parser($szSourceFilePath);
            $pdf = new FPDI();
            $pagecount = $pdf->setSourceFile($szSourceFilePath); // How many pages?  
            /*
            if($pdf_parser->_xref['trailer'][1]['/Encrypt'])
            {
               return array();
            }
            else
            {
                $pdf = new FPDI();
                $pagecount = $pdf->setSourceFile($szSourceFilePath); // How many pages? 
            }
             * 
             */
        } 
        catch (Exception $e)
        {  
            $szLogFile = __APP_PATH__."/logs/pdf_split.log";
            $strData=array();
            $strData[0] = "There is problem with getting page count ";
            $strData[1] = print_R($e,true) ;
            filelog(true,$strData,$szLogFile);
            return false;
        } 
        $ret_ary = array();
        $ctr = 0;
         
        /*
        * $szPdfLabelDirectory: This is the final directory where all the pdf  label will be stored after splited to page
        * $szTempPdfLabelDirectory: This is the temporary directory where we are storing splited pdf files and later will move splited file to final directory
        */
        $szPdfLabelDirectory = __UPLOAD_COURIER_LABEL_PDF__."/";
        if($bTestPage)
        {
            $szTempPdfLabelDirectory = __APP_PATH__.'/java/pdfs/';
            $szPdfLabelDirectory = __APP_PATH__.'/java/pdfs/splited/'; 
        }
        else if($bForwarder)
        { 
            $szTempPdfLabelDirectory = __UPLOAD_COURIER_LABEL_PDF_FORWARDER__."/"; 
        } 
        else
        {
            $szTempPdfLabelDirectory = __UPLOAD_COURIER_LABEL_PDF_MANAGEMENT__."/";
        }   
         
        try 
        {  
            // Split each page into a new PDF
            for ($i = 1; $i <= $pagecount; $i++) 
            {         
                $new_pdf = new FPDI();
                $new_pdf->AddPage();
                /*
                * Loading pdf file from source directory
                */
                $new_pdf->setSourceFile($szSourceFilePath); 
                $tplIdx = $new_pdf->importPage($i);
                 
                $new_pdf->useTemplate($tplIdx);     
                $szNewFilename = str_replace('.pdf', '', $filename).'_'.$i.".pdf"; 
               
                /*
                 * Storing splited pdf file page by page into temporary folder
                 */
                $szSplitedFilePath = $szTempPdfLabelDirectory.''.$szNewFilename; 
                $new_pdf->Output($szSplitedFilePath, "F");
                 
                /*
                * Moving splited pdf file to final directory 
                */
                $szDestinationPdfFilePath = $szPdfLabelDirectory."".$szNewFilename ; 
                @rename($szSplitedFilePath, $szDestinationPdfFilePath);
                
                $ret_ary[$ctr]['name'] = $szNewFilename;
                $ctr++;  
                $new_pdf->close(); 
            }
        } 
        catch (Exception $e) 
        {  
            $szLogFile = __APP_PATH__."/logs/pdf_split.log";
            $strData=array();
            $strData[0] = "We got an exception while spliting file: ".$szSourceFilePath;
            $strData[1] = print_R($e,true) ;
            filelog(true,$strData,$szLogFile);
            return false;
        }
        return $ret_ary;
    }
    
    function splitUploadedLabels_backup($filename, $end_directory,$bForwarder=false,$bTestPage=false)
    { 
        require_once(__APP_PATH__.'/forwarders/html2pdf/html2pdfBooking.php');
        require_once(__APP_PATH__.'/pdfLib/fpdi/fpdi.php');
        require_once(__APP_PATH__.'/pdfLib/fpdi/pdf_parser.php');
  
        //$end_directory = $end_directory ? $end_directory : './';
        //$new_path = preg_replace_callback('/[\/]+/', '/', $end_directory.'/'.substr($filename, 0, strrpos($filename, '/'))); 

        $new_path = $end_directory ;   
        if (!is_dir($new_path))
        {
            $szLogFile = __APP_PATH__."/logs/pdf_split.log";
            $strData=array();
            $strData[0] = "\n\n\n Path Not Exists: ".$new_path; 
            filelog(true,$strData,$szLogFile);
            
            // Will make directories under end directory that don't exist
            // Provided that end directory exists and has the right permissions            
           mkdir($new_path, 0777, true);
           chmod($new_path, 0777);
        } 
        $szFilePath = $new_path."".$filename;  
        try
        {
            $szLogFile = __APP_PATH__."/logs/pdf_split.log";
            $strData=array();
            $strData[0] = "\n\n\n File Path: ".$szFilePath; 
            filelog(true,$strData,$szLogFile);
            
            $pdf_parser = new pdf_parser($szFilePath);
            if($pdf_parser->xref['trailer'][1]['/Encrypt'])
            {
               return array();
            }
            else
            {
                $pdf = new FPDI();
                $pagecount = $pdf->setSourceFile($szFilePath); // How many pages? 
            }
        } 
        catch (Exception $e)
        {  
            $szLogFile = __APP_PATH__."/logs/pdf_split.log";
            $strData=array();
            $strData[0] = "There is problem with getting page count ";
            $strData[1] = print_R($e,true) ;
            filelog(true,$strData,$szLogFile);
            return false;
        }
          
        $ret_ary = array();
        $ctr = 0;
        
        $output_dir = __UPLOAD_COURIER_LABEL_PDF__."/";
        if($bTestPage)
        {
            $old_file_dir = __APP_PATH__.'/java/pdfs/';
            $output_dir = __APP_PATH__.'/java/pdfs/splited/'; 
        }
        else if($bForwarder)
        {
            //$old_file_dir = __UPLOAD_COURIER_LABEL_PDF__."/temp/";
            $old_file_dir = __UPLOAD_COURIER_LABEL_PDF_FORWARDER__."/";
            
        }
        else
        {
            $old_file_dir = __UPLOAD_COURIER_LABEL_PDF_MANAGEMENT__."/";
        }  
        try 
        {  
            // Split each page into a new PDF
            for ($i = 1; $i <= $pagecount; $i++) 
            {        
                $new_pdf = new FPDI();
                $new_pdf->AddPage();
                $new_pdf->setSourceFile($filename); 
                $tplIdx = $new_pdf->importPage($i);
                $strData=array();
                $strData[0] = "***************************";
                $strData[1] = print_R($tplIdx,true) ;
                filelog(true,$strData,$szLogFile);
                $new_pdf->useTemplate($tplIdx);     
                $new_filename = str_replace('.pdf', '', $filename).'_'.$i.".pdf"; 
               
                $new_pdf->Output($new_filename, "F");
                
                $szOldPdfFilePath = $old_file_dir."".$new_filename ;
                $szNewPdfFilePath = $output_dir."".$new_filename ;
                   
                @rename($szOldPdfFilePath, $szNewPdfFilePath);
                
                $ret_ary[$ctr]['name'] = $new_filename;
                $ctr++;  
                $new_pdf->close(); 
            }
        } 
        catch (Exception $e) 
        {  
            $szLogFile = __APP_PATH__."/logs/pdf_split.log";
            $strData=array();
            $strData[0] = print_R($e,true) ;
            filelog(true,$strData,$szLogFile);
            return false;
        }
        return $ret_ary;
    }
    
    function getCourierPricingDataByForwarder($idForwarder)
    { 
        if($idForwarder>0)
        {
            $query="
                SELECT
                    cpso.iBookingIncluded,
                    cpso.fMarkupPercent,
                    cpso.fMinimumMarkup,
                    cpso.fMarkupperShipment
                FROM    
                    ".__DBC_SCHEMATA_COUIER_SERVICES_OFFERED_DATA__." AS cpso  
                WHERE         
                    cpso.idForwarder='".(int)$idForwarder."'  
                 AND
                    cpso.isDeleted='0'
                    $query_and
            ";
          //echo $query."<br/><br/>";
            if(($result = $this->exeSQL($query)))
            {
                if($this->getRowCnt()>0)
                {
                    $ret_ary = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {
                        $ret_ary[$ctr] = $row ;
                        $ctr++;
                    }
                    return $ret_ary;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                return array();
            } 
        } 
    }
    
    function checkAllCourierProductPricing($postSearchAry)
    {
        if(!empty($postSearchAry))
        {
            $idForwarder = $postSearchAry['idForwarder'];
            $fromCountryProviderArr = $this->selectCourierAgreementByCountry($postSearchAry['idOriginCountry'],__TRADE_EXPORT__,$idForwarder);
            $fromCount=count($fromCountryProviderArr);

            $ToCountryProviderArr = $this->selectCourierAgreementByCountry($postSearchAry['idDestinationCountry'],__TRADE_IMPORT__,$idForwarder);
            $toCount=count($ToCountryProviderArr);

            $providerForwarderArr = array_merge($fromCountryProviderArr,$ToCountryProviderArr);
             
            $iLabelCreatedBy = 0;
            if(!empty($providerForwarderArr))
            {
                foreach($providerForwarderArr as $providerForwarderArrs)
                { 
                    $arrProviderDetails = array();
                    $arrProviderDetails['idCourierAgreement'] = $providerForwarderArrs['idCourierAgreement'];
                    $arrProviderDetails['idForwarder'] = $idForwarder;
                    $arrProviderDetails['idCourierProvider'] = $providerForwarderArrs['idCourierProvider'];
                             
                    $courierPricingAry = array();
                    $courierPricingAry = $this->getCourierProductProviderPricingData($arrProviderDetails);
                     
                    if(!empty($courierPricingAry))
                    {
                        foreach($courierPricingAry as $courierPricingArys)
                        {
                            if($courierPricingArys['iBookingIncluded']==1)
                            {
                                $iLabelCreatedBy = 1;
                            }
                            else
                            {
                                $iLabelCreatedBy = 2;
                                break;
                            }
                        }
                    }
                    if($iLabelCreatedBy==2)
                    {
                        break;
                    }
                } 
            }
            /*
                case 1: If forwarder has no automatic courier services online, then management makes labels
                case 2: If forwarder has automatic courier services online, and all has label included in pricing, then forwarder makes labels
                case 3: Otherwise, management makes labels 
            */
            
            if($iLabelCreatedBy==1)
            {
                return true;
            }
            else
            {
                return false;
            }
        } 
    } 
    
    function getCourierActiveProviderName($idCourierProvider)
    { 
    	$query="
    		SELECT
    			cp.id,
    			cp.szName
			FROM
				".__DBC_SCHEMATA_COUIER_PROVIDER_PRODUCTS__." AS cpp
			INNER JOIN
				".__DBC_SCHEMATA_COUIER_PROVIDER__." AS cp
			ON	
				cp.id=cpp.idCourierProvider
			WHERE
				cp.isDeleted='0'
			AND
				cpp.isDeleted='0'
			AND
				cpp.id='".(int)$idCourierProvider."'		
    	";
    	if( ( $result = $this->exeSQL( $query ) ) )
		{
            if( $this->getRowCnt() > 0 )
            {
            		$ret_ary = array();
                    $ctr = 0;
                    while($row=$this->getAssoc($result))
                    {
                        $ret_ary[$ctr] = $row;
                        $ctr++;
                    }
                    return $ret_ary;
            }
            else
            {
            	return array();
            }
		}
		else
		{
			return array();
		}
    
    }
    
    function addEditLCLExludedTrades($data)
    {
        if(!empty($data))
        { 
           
            $this->set_idForwarder(sanitize_all_html_input(trim($data['idForwarder']))); 
            $this->set_idOriginCountry(sanitize_all_html_input(trim($data['idOriginCountry']))); 
            $this->set_idDestinationCountry(sanitize_all_html_input(trim($data['idDestinationCountry']))); 
            $this->set_idExludedTrades(sanitize_all_html_input(trim($data['idExludedTrades']))); 
            $this->set_iCustomerType(sanitize_all_html_input(trim($data['iCustomerType']))); 
            
            if($this->error==true)
            {
                return false;
            } 
            
            $kConfig = new cConfig(); 
            $kForwarder = new cForwarder();
            $kForwarder->load($this->idForwarder);
            
            $this->szForwarderDisName = $kForwarder->szDisplayName;
            $this->szForwarderAlies = $kForwarder->szForwarderAlies;
             
           
            $fromArr=explode("_",$this->idOriginCountry);
            
            $fromCountryFlag = true;
            $toCountryFlag = true;
            
            if($fromArr[1]=='r')
            {
                $fromCountryArr=$this->getCountriesByIdRegion($fromArr[0]);
                $fromCountryFlag=false;
            }
            else
            {
                $fromCountryArr[0]=$this->idOriginCountry;
                $kConfig->loadCountry($this->idOriginCountry);
                $this->szOriginCountry = $kConfig->szCountryName;
            }
            
            $toArr=explode("_",$this->idDestinationCountry);
            if($toArr[1]=='r')
            {
                $toCountryArr=$this->getCountriesByIdRegion($toArr[0]);
                $toCountryFlag = false;
            }
            else
            {
                $toCountryArr[0]=$this->idDestinationCountry;
                
                $kConfig->loadCountry($this->idDestinationCountry);
                $this->szDestinationCountry = $kConfig->szCountryName;
            }
            
            if($toCountryFlag && $fromCountryFlag)
            {
                $excludedTradeSearch = array();
                
                $excludedTradeSearch['idForwarder'] = $this->idForwarder;
                $excludedTradeSearch['idOriginCountry'] = $this->idOriginCountry;
                $excludedTradeSearch['idDestinationCountry'] = $this->idDestinationCountry;
                $excludedTradeSearch['iCustomerType'] = $this->iCustomerType;
                
                //If both from and is individual country then we do like we were doing before,We just perform normal add/update
                if($this->idExludedTrades>0)
                {
                    if($this->isTradeAddedToExcludedLCLList($excludedTradeSearch,$this->idExludedTrades))
                    {
                        $this->addError('szSpecialError', t($this->t_base_courier.'fields/combination_already_exists') );
                        return false;
                    } 
                    $query="
                        UPDATE
                            ".__DBC_SCHEMATA_LCL_EXCLUDED_TRADES__."
                        SET
                            idOriginCountry = '".(int)$this->idOriginCountry."',
                            szOriginCountry = '".mysql_escape_custom($this->szOriginCountry)."',
                            idDestinationCountry = '".mysql_escape_custom($this->idDestinationCountry)."',
                            szDestinationCountry = '".mysql_escape_custom($this->szDestinationCountry)."',
                            idForwarder = '".mysql_escape_custom($this->idForwarder)."',
                            szForwarderDisName = '".mysql_escape_custom($this->szForwarderDisName)."',
                            szForwarderAlies = '".mysql_escape_custom($this->szForwarderAlies)."',
                            iCustomerType = '".mysql_escape_custom($this->iCustomerType)."',
                            dtUpdatedOn = now(),
                            iActive = '1'
                        WHERE
                            id='".(int)$this->idExludedTrades."'
                    ";
                }
                else
                {
                    if($this->isTradeAddedToExcludedList($excludedTradeSearch))
                    {
                        $this->addError('szSpecialError', t($this->t_base_courier.'fields/combination_already_exists') );
                        return false;
                    }

                    $query="
                        INSERT INTO
                            ".__DBC_SCHEMATA_LCL_EXCLUDED_TRADES__."
                        (
                            idOriginCountry,
                            szOriginCountry,
                            idDestinationCountry,
                            szDestinationCountry,
                            idForwarder,
                            szForwarderDisName,
                            szForwarderAlies,
                            iCustomerType,
                            iActive,
                            dtCreatedOn
                        )
                        VALUES
                        (
                            '".mysql_escape_custom($this->idOriginCountry)."',
                            '".mysql_escape_custom($this->szOriginCountry)."',
                            '".mysql_escape_custom($this->idDestinationCountry)."',
                            '".mysql_escape_custom($this->szDestinationCountry)."',
                            '".mysql_escape_custom($this->idForwarder)."', 
                            '".mysql_escape_custom($this->szForwarderDisName)."',
                            '".mysql_escape_custom($this->szForwarderAlies)."',
                            '".mysql_escape_custom($this->iCustomerType)."', 
                            1,
                            now()
                        )
                    ";		 
                }
                //echo $query;
                //die;
                if(($result = $this->exeSQL($query)))
                {
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
            else
            {     
                //If any of from or to is region then we only Insert trades that is not added yet.
                //print_r($toCountryArr);
                if(!empty($toCountryArr) && !empty($fromCountryArr))
                {
                    foreach($fromCountryArr as $fromCountryArrs)
                    {
                        $idOriginCountry = $fromCountryArrs; 
                        foreach($toCountryArr as $toCountryArrs)
                        { 
                            $idDestinationCountry = $toCountryArrs; 

                            $excludedTradeSearch = array();
                            
                            $excludedTradeSearch['idForwarder'] = $this->idForwarder;
                            $excludedTradeSearch['idOriginCountry'] = $idOriginCountry;
                            $excludedTradeSearch['idDestinationCountry'] = $idDestinationCountry;
                            $excludedTradeSearch['iCustomerType'] = $this->iCustomerType;
                            
                            if($this->isTradeAddedToExcludedLCLList($excludedTradeSearch))
                            {
                                //$this->addError('szSpecialError', t($this->t_base_courier.'fields/combination_already_exists') );
                                //return false;
                                continue;
                            }
                            
                            $this->idOriginCountry = $idOriginCountry;
                            $this->idDestinationCountry = $idDestinationCountry;
                            
                            $kConfig = new cConfig();
                            $kConfig->loadCountry($this->idOriginCountry);
                            $this->szOriginCountry = $kConfig->szCountryName;

                            $kConfig = new cConfig();
                            $kConfig->loadCountry($this->idDestinationCountry);
                            $this->szDestinationCountry = $kConfig->szCountryName;

                            $query="
                                INSERT INTO
                                    ".__DBC_SCHEMATA_LCL_EXCLUDED_TRADES__."
                                (
                                    idOriginCountry,
                                    szOriginCountry,
                                    idDestinationCountry,
                                    szDestinationCountry,
                                    idForwarder,
                                    szForwarderDisName,
                                    szForwarderAlies,
                                    iCustomerType,
                                    iActive,
                                    dtCreatedOn
                                )
                                VALUES
                                (
                                    '".mysql_escape_custom($this->idOriginCountry)."',
                                    '".mysql_escape_custom($this->szOriginCountry)."',
                                    '".mysql_escape_custom($this->idDestinationCountry)."',
                                    '".mysql_escape_custom($this->szDestinationCountry)."',
                                    '".mysql_escape_custom($this->idForwarder)."', 
                                    '".mysql_escape_custom($this->szForwarderDisName)."',
                                    '".mysql_escape_custom($this->szForwarderAlies)."',
                                    '".mysql_escape_custom($this->iCustomerType)."', 
                                    1,
                                    now()
                                )
                            ";	 
                            //echo $query;
//                            die;
                            $result = $this->exeSQL($query); 
                        }
                    }
                } 
                return true;
            }  
        }
    }
    
    function isTradeAddedToExcludedLCLList($data,$idExcludedTrades=false,$ret_ary=false,$fullDataFlag=false )
    {
        if(!empty($data))
        { 
            if($data['iCustomerType']>0)
            {
                $query_and = " AND (iCustomerType = '".__COURIER_EXCLUDED_TRADE_CUSTOMER_TYPE_PRIVATE_AND_BUSSINESS__."' OR iCustomerType = '".(int)$data['iCustomerType']."') ";
            }
            else
            {
                $query_and = " AND iCustomerType = '".__COURIER_EXCLUDED_TRADE_CUSTOMER_TYPE_PRIVATE_AND_BUSSINESS__."' ";
            }
            
            if(!$ret_ary)
            {
                $query_and .= " AND idForwarder = '".mysql_escape_custom($data['idForwarder'])."' ";
            }
            
            $query="
                SELECT
                    id,
                    idForwarder,
                    szForwarderDisName,
                    szForwarderAlies
                FROM
                    ".__DBC_SCHEMATA_LCL_EXCLUDED_TRADES__."
                WHERE 
                    idOriginCountry = '".mysql_escape_custom($data['idOriginCountry'])."'
                AND
                    idDestinationCountry = '".mysql_escape_custom($data['idDestinationCountry'])."' 
                AND
                    isDeleted='0'
                 $query_and
            ";
            //echo $query;
            if($idExcludedTrades>0)
            {
                $query .="
                    AND
                        id<>'".(int)$idExcludedTrades."'		
                ";
            } 
            
            if(($result = $this->exeSQL($query)))
            {
                // Check if the user exists
                if( $this->getRowCnt() > 0 )
                {
                    if($ret_ary)
                    {
                        $returnAry = array();
                        $ctr = 0;
                        while($row=$this->getAssoc($result))
                        {  
                            if($fullDataFlag)
                            {
                                $returnAry[$ctr] = $row['szForwarderDisName']."(".$row['szForwarderAlies'].")";
                            }
                            else
                            {
                                $returnAry[$ctr] = $row['idForwarder'];
                            }
                                $ctr++;
                        }
                        return $returnAry;
                    }
                    else
                    {
                        return true;
                    } 
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    
    function getAllExcludedLCLTrades($idExcludedTrade=false)
    { 
        if($idExcludedTrade>0)
        {
            $query_and = " AND id = '".(int)$idExcludedTrade."' ";
        }
        
        $query="
            SELECT
                id,
                idOriginCountry,
                szOriginCountry,
                idDestinationCountry,
                szDestinationCountry,
                idForwarder,
                szForwarderDisName,
                szForwarderAlies,
                iCustomerType,
                iActive,
                dtCreatedOn
            FROM
                ".__DBC_SCHEMATA_LCL_EXCLUDED_TRADES__."
            WHERE 
                isDeleted = '0' 
                $query_and
            ORDER BY
                szForwarderDisName ASC,
                szOriginCountry ASC,
                szDestinationCountry ASC
        "; 
        //echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        { 
            if($this->iNumRows>0)
            { 
                $ctr = 0;
                while($row = $this->getAssoc($result))
                {
                    $retAry[$ctr] = $row;
                    $ctr++;
                }
                return $retAry ;
            }
            else
            {
                return array();
            }
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        } 
    }
    
    function deleteExludedLCLTrades($idExcludedTrades)
    {
        if(!empty($idExcludedTrades))
        { 
            $excludedIdAry = explode(";",$idExcludedTrades);   
            if(!empty($excludedIdAry))
            {
                $excludeIdList = implode(',',$excludedIdAry);
                $query_and = " id IN (".$excludeIdList.") ";
            }
            else
            {
                return false;
            }
            
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_LCL_EXCLUDED_TRADES__."
                SET
                    isDeleted = '1', 
                    dtUpdatedOn = now()
                WHERE
                    $query_and
            ";
//            echo $query;
//            die;
            
            if($result = $this->exeSQL( $query ))
            {
                return true;
            } 
            else
            {
                return false;
            } 
        }
    }
    
    
    function getCourierProductByIdForwarderIdCourierProvider($idCourierProvider,$idForwarder,$idProviderProduct=0)
    {
    	   
        if((int)$idProviderProduct>0)
        {
            $queryWhere="AND
                    csod.idCourierProviderProductid='".(int)$idProviderProduct."' ";
        }
    	$query="
            SELECT
                csod.id,
                cpp.szName,
                csod.idCourierProviderProductid AS idProviderProduct,
                csod.idCourierAgreement,
                csod.szDisplayName,
                csod.dtCreated,
                csod.iBookingIncluded,
                csod.fMarkupPercent,
                csod.fMinimumMarkup,
                csod.fMarkupperShipment,
                csod.idForwarder
                FROM
                    ".__DBC_SCHEMATA_COUIER_PROVIDER_PRODUCTS__." AS cpp
                INNER JOIN
                    ".__DBC_SCHEMATA_COUIER_SERVICES_OFFERED_DATA__." AS csod
                ON	
                    cpp.id=csod.idCourierProviderProductid
                INNER JOIN
                    ".__DBC_SCHEMATA_COUIER_PROVIDER__." AS cp
                ON	
                    cp.id=cpp.idCourierProvider
                INNER JOIN    
                ".__DBC_SCHEMATA_COUIER_AGREEMENT_DATA__." AS cad
                ON	
                    cad.id=csod.idCourierAgreement    
                WHERE
                    cpp.isDeleted='0'
                AND
                    csod.isDeleted='0'
                AND
                    cad.isDeleted='0'
                AND
                    csod.idForwarder='".(int)$idForwarder."'
                AND
                    cpp.idCourierProvider='".(int)$idCourierProvider."'        
                $queryWhere    			
    	";
    	//echo $query;
    	if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
                $ret_ary = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    $ret_ary[$ctr] = $row;
                    $ctr++;
                }
                return $ret_ary;
            }
            else
            {
            	return array();
            }
        }
        else
        {
            return array();
        } 
    }
    
    function getAllDTDTradesList($bDTDFlag=false)
    { 
        if($bDTDFlag)
        {
            $query_and = " AND iDTDTrades = 1 ";
        }
        else
        {
            $query_and = " AND iDTDTrades = 0 ";
        }
        
    	$query="
            SELECT
                idFromCountry,
                idToCountry
            FROM
                ".__DBC_SCHEMATA_COUIER_TRUCKICON_DATA__."
            WHERE
                isDeleted='0'	
            $query_and
    	";
        //echo $query;
        //die;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
                $ret_ary = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    $ret_ary[$ctr] = $row['idFromCountry']."-".$row['idToCountry'];
                    $ctr++;
                }
                $resStr='';
                if(!empty($ret_ary))
                {
                    $resStr=implode(";",$ret_ary);
                }
                return $resStr;
            }
            else
            {
            	return '';
            }
        }
        else
        {
            return '';
        }
    }
    
    
    function getAllCustomerClearanceData($idPrimarySortKey='',$szSortValue='',$idOtherSortKeyArr='',$bDTDFlag=false)
    {
    	if($idPrimarySortKey!='' && $szSortValue!='')
    	{
            $orderBY="
                ORDER BY
                    $idPrimarySortKey
                    $szSortValue
            ";
            $idOtherSortKeyArr;
            if(count($idOtherSortKeyArr)>0)
            {
                foreach($idOtherSortKeyArr as $idOtherSortKeyArrs)
                {
                    if(trim($idOtherSortKeyArrs)!='')
                    {	
                        $sortArr=explode("_",$idOtherSortKeyArrs);
                        $orderBY .=",
                            ".$sortArr[0]."
                            ".$sortArr[1]."
                        ";
                    }
                } 
            } 
    	}
        else
        {
            $orderBY="  ORDER BY szFromCountry ASC ";
        }
        
        
        
    	$query="
            SELECT
                cc.id,
                cc.idFromCountry,
                cc.idToCountry,
                cf.szCountryName  AS szFromCountry,
                ct.szCountryName AS szToCountry,
                rf.szRegionName AS szFromRegionName,
                rt.szRegionName AS szToRegionName
            FROM
                ".__DBC_SCHEMATA_NO_CUSTOM_CLEARANCE__." AS cc
            INNER JOIN
                ".__DBC_SCHEMATA_COUNTRY__." AS cf
            ON
                cf.id=cc.idFromCountry
            INNER JOIN
                ".__DBC_SCHEMATA_COUNTRY__." AS ct
            ON
                ct.id=cc.idToCountry
            INNER JOIN
                ".__DBC_SCHEMATA_REGIONS__." AS rf
            ON
                rf.id=cf.idRegion
            INNER JOIN
                ".__DBC_SCHEMATA_REGIONS__." AS rt
            ON
                rt.id=ct.idRegion
            WHERE
                cc.isDeleted='0'
                $query_and
            $orderBY		 
    	";
    	//echo $query;
     	if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
                $ret_ary = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    $ret_ary[$ctr] = $row;
                    $ctr++;
                }
                return $ret_ary;
            }
            else
            {
            	return array();
            }
        }
        else
        {
            return array();
        } 
    } 
    
    
    function deleteCustomerClearanceData($deleteTruckArr)
    {
        if(!empty($deleteTruckArr))
        {
            $deleteTruckStr=implode(",",$deleteTruckArr);
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_NO_CUSTOM_CLEARANCE__."
                SET
                    isDeleted='1'
                WHERE
                    id IN  (".$deleteTruckStr.")	
            ";
            $result = $this->exeSQL( $query ); 
           
    	} 
    }
    
    function validateCCData($data)
    {
    	$this->set_idFromCountry(sanitize_specific_html_input(trim($data['idFromCountry'])));
        $this->set_idToCountry(sanitize_all_html_input(trim($data['idToCountry'])));
		 
        //exit if error exist.
        if ($this->error === true)
        {
            return false;
        } 

        if($this->idFromCountry!='' && $this->idToCountry!='')
        {
            $fromArr=explode("_",$this->idFromCountry);
            $fromFlag=false;
            $toFlag=false;
            if($fromArr[1]=='r')
            {
                $fromCountryArr=$this->getCountriesByIdRegion($fromArr[0]);
                $fromFlag=true;
            }
            else
            {
                $fromCountryArr[0]=$this->idFromCountry;
            }
			
            $toArr=explode("_",$this->idToCountry);
            if($toArr[1]=='r')
            {
                $toCountryArr=$this->getCountriesByIdRegion($toArr[0]);
                $toFlag=true;
            }
            else
            {
                $toCountryArr[0]=$this->idToCountry;
            } 
             
            if(!empty($toCountryArr) && !empty($fromCountryArr))
            {
                foreach($fromCountryArr as $fromCountryArrs)
                {
                    $fromCountry=$fromCountryArrs; 
                    foreach($toCountryArr as $toCountryArrs)
                    { 
                        if(!$this->checkFromCountryToCountryExistsForCC($fromCountry,$toCountryArrs))
                        { 
                            return true; 
                        } 
                    } 
                }
            }		
        } 	
        return false;
    }
    
    function checkFromCountryToCountryExistsForCC($idFromCountry,$idToCountry)
    { 
               
    	$query="
            SELECT
                id
            FROM
                ".__DBC_SCHEMATA_NO_CUSTOM_CLEARANCE__."
            WHERE
                idFromCountry='".(int)$idFromCountry."'
            AND
                idToCountry='".(int)$idToCountry."'	 
            AND
                isDeleted='0'	
            $query_and
    	";
        //echo $query;
        //die;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
            	return true;
            }
            else
            {
            	return false;
            }
        } 
    }
    
    function addEditFromToCountryForCustomerClearance($data)
    { 
    	if($this->validateCCData($data))
    	{  
            $fromArr=explode("_",$this->idFromCountry);
            $fromFlag=false;
            $toFlag=false;
            if($fromArr[1]=='r')
            {
                $fromCountryArr=$this->getCountriesByIdRegion($fromArr[0]);
                $fromFlag=true;
            }
            else
            {
                $fromCountryArr[0]=$this->idFromCountry;
            }

            $toArr=explode("_",$this->idToCountry);
            if($toArr[1]=='r')
            {
                $toCountryArr=$this->getCountriesByIdRegion($toArr[0]);
                $toFlag=true;
            }
            else
            {
                $toCountryArr[0]=$this->idToCountry;
            }
            $iDTDTrades = $this->iDTDTrades ; 
            if(!empty($toCountryArr) && !empty($fromCountryArr))
            {
                foreach($fromCountryArr as $fromCountryArrs)
                {
                    $fromCountry=$fromCountryArrs; 
                    $fromCourntyRegionArr=$this->getCountryAndRegionName($fromCountry);
                    foreach($toCountryArr as $toCountryArrs)
                    { 
                        if(!$this->checkFromCountryToCountryExistsForCC($fromCountry,$toCountryArrs))
                        { 
                            $toCourntyRegionArr=$this->getCountryAndRegionName($toCountryArrs);
                            $query="
                                INSERT INTO
                                    ".__DBC_SCHEMATA_NO_CUSTOM_CLEARANCE__."
                                (
                                    idFromCountry,
                                    idToCountry
                                )
                                VALUES
                                (
                                    '".(int)$fromCountry."',
                                    '".(int)$toCountryArrs."'
                                )
                            ";
                            //echo $query."<br><br> ";
                            $result = $this->exeSQL( $query );
                        }
                    }
                }
            } 
            return true;
    	}
    }
    
    
    function getCountryAndRegionName($idCountry)
    {
        $query="
            SELECT
                c.szCountryName,
                r.szRegionName
            FROM
                ".__DBC_SCHEMATA_COUNTRY__." AS c
            INNER JOIN
                ".__DBC_SCHEMATA_REGIONS__." AS r
            ON
                r.id=c.idRegion
            WHERE
                c.id='".(int)$idCountry."'	 
    	";
        //echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            if( $this->getRowCnt() > 0 )
            {
                $ret_ary = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    $ret_ary = $row;
                }
                return $ret_ary;
            }
        }
    }
    
    function set_idOriginCountry( $value )
    {
        $this->idOriginCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idOriginCountry", t($this->t_base_courier_label.'fields/orig_country'), false, false, true );
    }
    function set_idDestinationCountry( $value )
    {
        $this->idDestinationCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idDestinationCountry", t($this->t_base_courier_label.'fields/dest_country'), false, false, true );
    }
    function set_idExludedTrades( $value )
    {
        $this->idExludedTrades = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idExludedTrades", t($this->t_base_courier_label.'fields/excluded_service'), false, false, false );
    } 
    function set_szGroupName( $value )
    {
        $this->szGroupName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szName", t($this->t_base_courier.'fields/groupName'), false, 255, true );
    }
    
    function set_szShipperEmail( $value )
    {
        $this->szShipperEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szShipperEmail", t($this->t_base_courier_label.'fields/email'), false, 255, true );
    }
    
    function set_szShipperFirstName( $value )
    {
        $this->szShipperFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperFirstName", t($this->t_base_courier_label.'fields/first_name'), false, 255, true );
    }
    
    function set_szShipperLastName( $value )
    {
        $this->szShipperLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperLastName", t($this->t_base_courier_label.'fields/last_name'), false, 255, true );
    }
    
    function set_szShipperCompanyName( $value,$bRequiredFlag=true )
    {
        $this->szShipperCompanyName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperCompanyName", t($this->t_base_courier_label.'fields/company'), false, 255, $bRequiredFlag );
    }
    function set_idShipperCountry( $value )
    {
        $this->idShipperCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idShipperCountry", t($this->t_base_courier_label.'fields/phone_number'), false, false, false );
    }
    
    function set_dtShipperPickUp( $value )
    {
        $this->dtShipperPickUp = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "dtShipperPickUp", t($this->t_base_courier_label.'fields/phone_number'), false, false, true );
    }
    
    function set_szShipperPhone( $value )
    {
        $this->szShipperPhone = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperPhone", t($this->t_base_courier_label.'fields/shipping_date'), false, 255, true );
    }
    
    function set_szEmail( $value )
    {
        $this->szEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szEmail", t($this->t_base_services.'fields/email'), false, 255, true );
    }
    
    function set_dtCollectionStartTime( $value,$saveFlag )
    {
        $this->dtCollectionStartTime = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "dtCollectionStartTime", t($this->t_base_services.'fields/from_time'), false, false, $saveFlag );
    }
    
    function set_iCollection( $value,$saveFlag )
    {
        $this->iCollection = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iCollection", t($this->t_base_services.'fields/collection'), false, false, $saveFlag );
    }
    
    function set_dtCollectionEndTime( $value,$saveFlag )
    {
        $this->dtCollectionEndTime = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "dtCollectionEndTime", t($this->t_base_services.'fields/to_time'), false, false, $saveFlag );
    }
    function set_dtCollection( $value,$saveFlag )
    {
        $this->dtCollection = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "dtCollection", t($this->t_base_services.'fields/date'), false, false, $saveFlag );
    }
    
    function set_iFileCount( $value,$saveFlag )
    {
        if($saveFlag &&  (int)$value==0)
        {
            $value='';
        }
        $this->iFileCount = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iFileCount", t($this->t_base_services.'fields/iFileCount'), false, false, $saveFlag );
    }
    function set_szTrackingNumber( $value )
    {
        $this->szTrackingNumber = $this->validateInput( $value, __VLD_CASE_ALPHANUMERIC__, "szTrackingNumber", t($this->t_base_services.'fields/tracking_number'), false, false, true );
    }
   
    function set_szMeterNumber( $value )
    {
        $this->szMeterNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szMeterNumber", t($this->t_base_services.'fields/meter_number'), false, false, true );
    }
    function set_iServiceType( $value )
    {
        $this->iServiceType = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iServiceType", t($this->t_base_services.'fields/booking_provider_sending_shipper'), false, false, true );
    }
    
    function set_iBookingIncluded( $value )
    {
        $this->iBookingIncluded = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iBookingIncluded", t($this->t_base_services.'fields/booking_provider_sending_shipper'), false, false, true );
    }
    
    function set_fMarkupPercent( $value)
    {
        $this->fMarkupPercent = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fMarkupPercent", t($this->t_base.'fields/markup_percent'), false, false, true);
    }
    
    function set_fMinimumMarkup( $value)
    {
        $this->fMinimumMarkup = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fMinimumMarkup", t($this->t_base.'fields/mini_markup'), false, false, true);
    }
    
    function set_fMarkupperShipment( $value)
    {
        $this->fMarkupperShipment = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fMarkupperShipment", t($this->t_base.'fields/mini_markup_shipment'), false, false, true);
    }
    
    function set_idCountryFrom( $value )
    {
        $this->idCountryFrom = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idCountryFrom", t($this->t_base_services.'fields/country'), false, false, true );
    }
    function set_iTrade( $value)
    {
        $this->iTrade = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iTrade", t($this->t_base_services.'fields/trade'), false, false, true );
    }
    
	function set_idProviderProduct( $value )
    {
        $this->idProviderProduct = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idProviderProduct", t($this->t_base_services.'fields/service'), false, false, true );
    }
    function set_szDisplayName( $value)
    {
        $this->szDisplayName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDisplayName", t($this->t_base_services.'fields/your_name'), false, false, true );
    }
    
	    
    function set_idCourierProvider( $value )
    {
        $this->idCourierProvider = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idCourierProvider", t($this->t_base_services.'fields/courier_provider'), false, false, true );
    }
    function set_szAccountNumber( $value)
    {
        $this->szAccountNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAccountNumber", t($this->t_base_services.'fields/account_number'), false, false, true );
    }
    
    function set_szUsername( $value )
    {
        $this->szUsername = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szUsername", t($this->t_base_services.'fields/user_name'), false, false, true );
    }
    function set_szPassword( $value)
    {
        $this->szPassword = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPassword", t($this->t_base_services.'fields/password'), false, false, true );
    }
    
    function set_szCarrierAccountID( $value)
    {
        $this->szCarrierAccountID = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCarrierAccountID", t($this->t_base_services.'fields/password'), false, false, false );
    } 
    function set_idFromCountry( $value )
    {
        $this->idFromCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idFromCountry", t($this->t_base_courier.'fields/instruction'), false, false, true );
    }
    function set_idToCountry( $value)
    {
        $this->idToCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idToCountry", t($this->t_base_courier.'fields/terms_contitions'), false, false, true );
    }
    function set_iDTDTrades( $value)
    {
        $this->iDTDTrades = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iDTDTrades", t($this->t_base_courier.'fields/terms_contitions'), false, false, false );
    } 
    function set_szInstruction( $value, $flag=true )
    {
        $this->szInstruction = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szInstruction", t($this->t_base_courier.'fields/instruction'), false, false, false );
    }
    function set_szTerms( $value)
    {
        $this->szTerms = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTerms", t($this->t_base_courier.'fields/terms_contitions'), false, false, false );
    }
    
    function set_szRestriction( $value )
    {
        $this->szRestriction = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szRestriction", t($this->t_base_courier.'fields/restriction'), false, 255, true );
    }
    function set_idCargoLimitationType( $value)
    {
        $this->idCargoLimitationType = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idCargoLimitationType", t($this->t_base_courier.'fields/variable'), false, 255, true );
    }
    function set_szLimit( $value)
    {
        $this->szLimit = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "szLimit", t($this->t_base_courier.'fields/limit'), false, false, true );
    }
    
    function set_szProductName( $value )
    {
        $this->szProductName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szProductName", t($this->t_base_courier.'fields/product_name'), false, 255, true );
    }
    function set_szApiCode( $value)
    {
        $this->szApiCode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szApiCode", t($this->t_base_courier.'fields/api_code'), false, 255, true );
    }
    
    function set_idGroup( $value)
    {
        $this->idGroup = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idPacking", t($this->t_base_courier.'fields/group'), false, 255, true );
    }
    
    function set_idPacking( $value)
    {
        $this->idPacking = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idPacking", t($this->t_base_courier.'fields/packing'), false, 255, true );
    }
    
    function set_szFirstName( $value,$required_flag=true )
    {
        $this->szFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szFirstName", t($this->t_base.'fields/f_name'), false, 255, $required_flag );
    }
    function set_szLastName( $value,$required_flag=true )
    {
        $this->szLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLastName", t($this->t_base.'fields/l_name'), false, 255, $required_flag );
    }
    function set_szCity( $value,$required_flag=true )
    {
        $this->szCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCity", t($this->t_base.'fields/city'), false, 255, $required_flag );
    }
    function set_szState( $value ,$required_flag=false)
    {
        $this->szState = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szState", t($this->t_base.'fields/p_r_s'), false, 255, false );
    }
    function set_szCountry( $value,$required_flag=true )
    {
        $this->szCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCountry", t($this->t_base.'fields/country'), false, 255, $required_flag );
    } 
    function set_szAddress1( $value,$required_flag=true )
    {
        $this->szAddress1 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAddress1", t($this->t_base.'fields/address_line1'), false, 255, $required_flag );
    }
    function set_szAddress2( $value )
    {
        $this->szAddress2 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAddress2", t($this->t_base.'fields/address_line2'), false, 255, false );
    }
    function set_szAddress3( $value )
    {
        $this->szAddress3 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAddress3", t($this->t_base.'fields/address_line3'), false, 255, false );
    }
    function set_szPostcode( $value,$flag=true)
    {
        $this->szPostcode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPostcode", t($this->t_base.'fields/postcode'), false, 255, $flag);
    } 
    function set_szSFirstName( $value,$required_flag=true)
    {
        $this->szSFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSFirstName", t($this->t_base.'fields/f_name'), false, 255, $required_flag );
    }
    function set_szSLastName( $value,$required_flag=true)
    {
        $this->szSLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSLastName", t($this->t_base.'fields/l_name'), false, 255, $required_flag );
    }
    function set_szSCity( $value,$required_flag=true)
    {
        $this->szSCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSCity", t($this->t_base.'fields/city'), false, 255, $required_flag );
    }
    function set_szSState( $value)
    {
        $this->szSState = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSState", t($this->t_base.'fields/p_r_s'), false, 255, false );
    }
    function set_szSCountry( $value)
    {
        $this->szSCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSCountry", t($this->t_base.'fields/country'), false, 255, true );
    } 
    function set_szSAddress1( $value,$required_flag=true)
    {
        $this->szSAddress1 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSAddress1", t($this->t_base.'fields/address_line1'), false, 255, $required_flag );
    }
    function set_szSAddress2( $value )
    {
        $this->szSAddress2 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSAddress2", t($this->t_base.'fields/address_line2'), false, 255, false );
    }
    function set_szSAddress3( $value )
    {
        $this->szSAddress3 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSAddress3", t($this->t_base.'fields/address_line3'), false, 255, false );
    }
    function set_szSPostcode( $value,$flag=true)
    {
        $this->szSPostcode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szSPostcode", t($this->t_base.'fields/postcode'), false, 255, $flag);
    }
    function set_fCargoLength( $value)
    {
        $this->fCargoLength = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fCargoLength", t($this->t_base.'fields/postcode'), false, false, true);
    }
    function set_fCargoWidth( $value)
    {
        $this->fCargoWidth = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fCargoWidth", t($this->t_base.'fields/postcode'), false, false, true);
    }
    function set_fCargoHeight( $value)
    {
        $this->fCargoHeight = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fCargoHeight", t($this->t_base.'fields/postcode'), false, false, true);
    }
    function set_fCargoWeight( $value)
    {
        $this->fCargoWeight = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fCargoWeight", t($this->t_base.'fields/postcode'), false, false, true);
    } 
    
    function set_szName( $value)
    {
        $this->szName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szName", t($this->t_base_courier.'fields/company_display_name'), false, false, true);
    }
    function set_fPrice( $value)
    {
        $this->fPrice = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fPrice", t($this->t_base_courier.'fields/booking_price'), false, false, true);
    } 
    
    function set_iBookingPriceCurrency( $value)
    {
        $this->iBookingPriceCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fPrice", t($this->t_base_courier.'fields/currency'), false, false, true);
    } 
    
    function set_idForwarder( $value)
    {
        $this->idForwarder = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idForwarder", t($this->t_base_courier.'fields/forwarder'), false, false, true);
    }  
    function set_iDaysMin( $value)
    {
        $this->iDaysMin = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iDaysMin", t($this->t_base_courier.'fields/days')."".t($this->t_base_courier.'fields/min'), false, 255, true );
    }
    
    
    function set_iDaysStd( $value)
    {
        $this->iDaysStd = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iDaysStd", t($this->t_base_courier.'fields/days'), false, 255, true );
    }
    
    function set_iCustomerType( $value)
    {
        $this->iCustomerType = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iCustomerType", t($this->t_base_courier.'fields/days'), false, 255, true );
    } 
    
    function set_idCourierProduct($value)
    {
        $this->idCourierProduct = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idCourierProduct", t($this->t_base_courier.'fields/courier_product'), false, 255, true );
    }
}
?>