<?php
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );

require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php");

//define('K_PATH_MAIN', __APP_PATH__);
require_once(__APP_PATH__.'/TNT/tcpdf/tcpdf.php');

// Extend the TCPDF class to create custom Header and Footer
class cLabelPdf extends TCPDF {

    //Page header
    public function Header() 
    {	
            //overwriting header function 
    }

    // Page footer
    function Footer()
    {
        $this->SetY(-10);
        // Set font
        $this->SetFont('helvetica', 'I', 10);
        $footerimage= __MAIN_SITE_HOME_PAGE_URL__.'/images/Powered_By.jpg';
        $footer_table = '
                    <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr >

                                            <td width="100%" align="right">
                                                <img src="'.$footerimage.'">
                                            </td>
                                    </tr>
                            </table>
                            <br pagebreak="true" />
             ';
        //overwriting footer function	
        $this->writeHTML($footer_table, true, false, true, false, ''); 
    }  
}

?>