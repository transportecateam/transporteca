<?php
/**
 * This file is the container for all partner api related functionality.
 * All functionality related to partner api should be contained in this class.
 *
 * partner.class.php
 *
 * @copyright Copyright (C) 2015 Transporteca
 * @author Ajay
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

if(!class_exists('cUser'))
{
    require_once( __APP_PATH_CLASSES__ . "/booking.class.php");
    require_once( __APP_PATH_CLASSES__ . "/registeredShippersConsignees.class.php");
    require_once( __APP_PATH_CLASSES__ . "/user.class.php");
    require_once(__APP_PATH_CLASSES__."/config.class.php");
    require_once(__APP_PATH_CLASSES__."/warehouseSearch.class.php");
    require_once(__APP_PATH_CLASSES__."/booking.class.php");
    require_once(__APP_PATH_CLASSES__.'/insurance.class.php'); 
    require_once(__APP_PATH_CLASSES__.'/courierServices.class.php');
}  
class cPartner extends cDatabase
{ 
    /*
    * Variable for containing shipping date
    */
    public static $arrErrorMessageAry;
    public static $szDeveloperNotes; 

    public $t_base = "Partner/";
    public $t_base_service = "SelectService/";
    public static $szBaseErrorURI = "Partner/error/";
    var $t_base_booking_confirmation ="BookingConfirmation/"; 
    public static $bApiValidation = true;
    public static $searchablePacketTypes = array();
    public $predefinedServiceIDForQuote = array(__API_SERVICE_ID_BILLING__,__API_SERVICE_ID_SHIPPER__,__API_SERVICE_ID_CONSIGNEE__,__API_SERVICE_ID_BILLING_CUSTOMER__);
    public static $idGlobalShipmentType;
    public static $dtShippingDay;
    public static $dtShippingMonth;
    public static $dtShippingYear; 
    public static $szPacketType;
    public static $szNotification;
    public static $szGlobalMode;
    public static $szRequestType;
    public static $szGlobalPartnerAPIKey;
    public static $bNoserviceFound = false;
    public static $iNumServiceFound = false;
    public static $szCargoDescription;
    public static $fCargoValue;
    public static $szCargoCurrency;
    public static $fTotalVolume;
    public static $fTotalWeight;
    public static $szServiceTerm;
    public static $idBooking;
    public static $szCalledFromAPIType;
    public static $searchValues;
    
    public static $szShipperCompanyName;
    public static $szShipperFirstName;
    public static $szShipperLastName;
    public static $szShipperEmail;
    public static $iShipperCounrtyDialCode;
    public static $iShipperPhoneNumber;
    public static $szShipperAddress;
    public static $szShipperCity;
    public static $szShipperPostcode;
    public static $szShipperCountry;
    
    public static $szConsigneeCompanyName;
    public static $szConsigneeFirstName;
    public static $szConsigneeLastName;
    public static $szConsigneeEmail;
    public static $iConsigneeCounrtyDialCode;
    public static $iConsigneePhoneNumber;
    public static $szConsigneeAddress;
    public static $szConsigneeCity;
    public static $szConsigneePostcode;
    public static $szConsigneeCountry;
    
    public static $szServiceType;
    public static $szPalletType;
    public static $fDimension;
    public static $szDimensionMeasure;
    public static $szWeightMeasure;
    public static $iQuantity=array();
    public static $iHeight=array();
    public static $iLength=array();
    public static $iWidth=array();   
    public static $iWeight=array();
    public static $GlobalpostSearchAry = array();
    public static $idPartnerCustomer;
    
    // Validation Required
    
    public static $dtShippingDayReqFlag = "Hello";
    public static $dtShippingMonthReqFlag = true;
    public static $dtShippingYearReqFlag = true;
    public static $szPacketTypeReqFlag = false;
    
    public static $szCargoDescriptionReqFlag = false;
    public static $fCargoValueReqFlag = true;
    public static $szCargoCurrencyReqFlag = true;
    public static $fTotalVolumeReqFlag = false;
    public static $fTotalWeightReqFlag = false;
    public static $szServiceTermReqFlag = false;
    
    public static $szShipperCompanyNameReqFlag = false;
    public static $szShipperFirstNameReqFlag = false;
    public static $szShipperLastNameReqFlag = false;
    public static $szShipperEmailReqFlag = false;
    public static $iShipperCounrtyDialCodeReqFlag = false;
    public static $iShipperPhoneNumberReqFlag = false;
    public static $szShipperAddressReqFlag = false;
    public static $szShipperCityReqFlag = true;
    public static $szShipperPostcodeReqFlag = false;
    public static $szShipperCountryReqFlag = true;
    
    public static $szConsigneeCompanyNameReqFlag = false;
    public static $szConsigneeFirstNameReqFlag = false;
    public static $szConsigneeLastNameReqFlag = false;
    public static $szConsigneeEmailReqFlag = false;
    public static $iConsigneeCounrtyDialCodeReqFlag = false;
    public static $iConsigneePhoneNumberReqFlag = false;
    public static $szConsigneeAddressReqFlag = false;
    public static $szConsigneeCityReqFlag = true;
    public static $szConsigneePostcodeReqFlag = false;
    public static $szConsigneeCountryReqFlag = true;
    
    public static $szServiceTypeReqFlag = false;
    public static $szPalletTypeReqFlag = false;
    public static $fDimensionReqFlag = false;
    public static $fUnitReqFlag = false;
    public static $iQuantityReqFlag = false;
    public static $iHeightReqFlag = false;
    public static $iLengthReqFlag = false;
    public static $iWidthReqFlag = false;
    public static $iWeightReqFlag = false;
    public static $szDimensionMeasureReqFlag = false;
    public static $szWeightMeasureReqFlag = false;
    public static $szServiceRefrenceReqFlag = false;
    public static $iInsuranceFlagReqFlag = false;
    public static $szGlobalPartnerAlies; 
    public static $idGlobalPartner;
    public static $szGlobalToken;
    public static $szGlobalErrorType;
    public static $szClientUserAgent;
    public static $szUserAgent;
    public static $arrServiceListAry;
    public static $priceCalculationLogs;
    public static $szApiRequestMode;
     
    //public static $error_detail_ary = array();
    public static $responsive_detail_ary = array();
      
    public static $szDtdTradeValue;
    public static $iOfferType;
    public static $iOfferTypeText;
    public static $arCustomerEventTypes;
    
    public static $iResponseDay='';
    public static $iOfferTypeFlag;
    public static $iCreateAutoQuoteForVogaBooking=0;
    public static $serviceFoundForVogaQuote;
    public static $arrRFQListAry;
    /*
    * This is constructor of partner class which is reponsible to initiate data base connection and create instance of self
    */
    function __construct()
    {
        parent::__construct(); 
        self::$arCustomerEventTypes = array('PriceRequestContact');
        return true;
    }
    /*
    * 
    */ 
    public function getPrice($data,$mode=false,$iQuickQuoteTryitout=false)
    {  
        if(!empty($data))
        {    
            $kWHSSearch=new cWHSSearch();   
            $kConfig = new cConfig(); 
            $result_Ary = array();   
            /*
            *  Validating request parameters
            */
          
            $this->validateParams($data,false,$mode); 
            if($this->error==true)
            {      
                if($mode=='QUICK_QUOTE')
                {
                    return false;
                }
                else
                {
                    return self::buildErrorMessage();
                } 
            } 
            /*
            *  Preparing request parameters for search
            */
            $postSearchAry = $this->prepareRequestParamsForSearch($data['cargo'],'price',$mode,$iQuickQuoteTryitout);   
            
            if($postSearchAry['iSearchStandardCargo']==1)
            {
                $szCustomerToken = $postSearchAry['szCustomerToken'];
                if(!empty($szCustomerToken))
                {
                    $kBooking = new cBooking();
                    $kTransportecaApi = new cTransportecaApi();
                    $kTransportecaApi->isTokenValid($szCustomerToken); 
                    $idCustomer = $kTransportecaApi->idApiCustomer;
                    
                }
                else if(!empty($postSearchAry['szBillingEmail']))
                {
                    $iAddNewUser = false;
                    $kUser = new cUser();
                    if($kUser->isUserEmailExist($postSearchAry['szBillingEmail']))
                    {  
                        $idCustomer = $kUser->idIncompleteUser ;
                    }
                }
               
                
                
                $this->iCreateAutoQuoteForVogaBooking=1;
                    
                $iWeekDay = date('w');
                if($iWeekDay==5 || $iWeekDay==6) //5: Friday, 6: Saturday
                {
                    $iResponseDay = 1 ;
                }
                else
                {
                    $iResponseDay = 0 ; 
                }
                
                $kCustomerEevent = new cCustomerEevent();
                $responseArr=$kCustomerEevent->createStandardCargoResponse($postSearchAry);
                
                if($idCustomer>0)
                {
                    $kUser = new cUser();
                    $kUser->getUserDetails($idCustomer);
                    if($kUser->iPrivate==1)
                    {
                        $postSearchAry['szCustomerCompanyName']=$kUser->szFirstName." ".$kUser->szLastName;
                    }
                    else
                    {
                        $postSearchAry['szCustomerCompanyName']=$kUser->szCompanyName;
                    }
                    
                }                
                $bookingResponseAry = array();   
                $bookingResponseAry=$responseArr;
                $bookingResponseAry['notification'] = "Transporteca will send the transportation quotation directly to ".$postSearchAry['szCustomerCompanyName']." within one working day";
                $bookingResponseAry['iResponseDay'] = $iResponseDay;
                self::$searchValues = array('weight'=>$postSearchAry['fCargoWeight'],'cbm'=>$postSearchAry['fCargoVolume']);
            }
            else
            {
                /*
                *  Finally processing request and making service search for Transporteca.
                */
                $result_Ary = $this->processServiceApiRequest($postSearchAry,$mode);  
            }  
            if(self::$szApiRequestMode==__API_REQUEST_MODE_LIVE__ && $mode!='QUICK_QUOTE')
            {
                $postSearchAry['iCreateRFQForStandardCargo']=$this->iCreateRFQForStandardCargo;
                $postSearchAry['iCreateAutoQuoteForVogaBooking']=$this->iCreateAutoQuoteForVogaBooking;
                $this->createAutomaticBooking($postSearchAry,$mode);
            }
            
            if($postSearchAry['iSearchStandardCargo']==1)
            {
                self::$arrServiceListAry = $bookingResponseAry;
                self::$szGlobalErrorType = 'SUCCESS'; 
                return self::buildSuccessMessage('Booking');
            }
            return $result_Ary;
        }
    }
    
    public function validatePrice($data,$mode=false)
    {  
        if(!empty($data))
        {   
            $kWHSSearch=new cWHSSearch();   
            $kConfig = new cConfig(); 
            $kBooking = new cBooking();
            $kRegisterShipCon = new cRegisterShipCon();
            $result_Ary = array();    
            self::$szDeveloperNotes .= ">. Initiated vaidate price request from API ".PHP_EOL; 
            /*
            *  Validating request parameters
            */
            $this->validateParams($data,'VALIDATE_PRICE',$mode);  
             
            if($this->error==true)
            {       
                self::$szDeveloperNotes .= ">. Validate Price validation failed because of this error:\n\n ".print_R($this->arErrorMessages,true)." ".PHP_EOL; 
                return self::buildErrorMessage(); 
            } 
            else
            {
                self::$szDeveloperNotes .= ">. Validate Price validation OK. Going forward ".PHP_EOL;
            } 
            self::$szRequestType = 'validatePrice';
            
            $iAutomaticBooking = false;
            $bManualRFQBooking = false;
            $szReferenceToken = self::$szGlobalToken;
                
            $idPartner = self::$idGlobalPartner;  
            $szApiRequestMode = self::$szApiRequestMode;
            $errorFlag = false; 
            $szServiceID = trim($data['szServiceID']);
            $szQuotePriceKey = trim($data['szQuoteID']);
            $dtCurrentDateTime = $kBooking->getRealNow(); 
            $dtMinus24HoursDateTime = date('Y-m-d H:i:s',strtotime("-24 HOUR",strtotime($dtCurrentDateTime)));
             
            /*
            * Delete previously searched data for validate price bapi all
            */
            $this->clearOldValidatePriceData();
            /*
            *  Preparing request parameters for search
            */
            $postSearchAry = $this->prepareRequestParamsForSearch($data['cargo'],'validatePrice',$mode,$iQuickQuoteTryitout); 
                
            $billingDataAry = array();
            $billingDataAry['szBillingCompanyName'] = $postSearchAry['szBillingCompanyName'];
            $billingDataAry['szBillingFirstName'] = $postSearchAry['szBillingFirstName'];
            $billingDataAry['szBillingLastName'] = $postSearchAry['szBillingLastName'];
            $billingDataAry['iBillingCountryDialCode'] = $postSearchAry['iBillingCountryDialCode'];
            $billingDataAry['iBillingPhoneNumber'] = $postSearchAry['iBillingPhoneNumber'];
            $billingDataAry['szBillingEmail'] = $postSearchAry['szBillingEmail'];
            $billingDataAry['szBillingAddress'] = $postSearchAry['szBillingAddress'];
            $billingDataAry['szBillingPostcode'] = $postSearchAry['szBillingPostcode'];
            $billingDataAry['szBillingCity'] = $postSearchAry['szBillingCity'];
            $billingDataAry['idBillingCountry'] = $postSearchAry['idBillingCountry']; 
            $szCargoDescription = $postSearchAry['szCargoDescription'];
            $idCurrency = $postSearchAry['idCurrency'];
            
            if(!empty($szQuotePriceKey))
            {
                /*
                * This code will be called for RFQ bookings
                */
                $kQuote = new cQuote();                
                $bManualRFQBooking = true;
                $bookingQuoteAry = array();
                $bookingQuoteAry = $kQuote->getQuotePricingIDbyKey($szQuotePriceKey);
                $idBookingQuotePricing = $bookingQuoteAry['id']; 
                $idBooking = $bookingQuoteAry['idBooking'];
                
                if($idBookingQuotePricing>0 && $idBooking>0)
                { 
                    $bookingDataAry = array();
                    $bookingDataAry = $kBooking->getBookingDetails($idBooking); 
                     
                    $idCustomerCurrency = $bookingDataAry['idCurrency']; 
                    if((int)$idCurrency!=(int)$idCustomerCurrency)
                    {
                        self::$szDeveloperNotes .= ">.  Validate Price API call is made with a different currency ".PHP_EOL;
                        $errorFlag = true;
                        $this->buildErrorDetailAry('szPriceCurrency','INVALID');
                        self::$arrErrorMessageAry = $this->error_detail_ary; 
                        self::$szGlobalErrorType = 'VALIDATION';  
                        return self::buildErrorMessage();
                    }
                    else
                    { 
                        $iBookingLanguage = $postSearchAry['iBookingLanguage']; 
                        $kServices = new cServices();
                        $kServices->updateQuoteDetailsToBooking($idBookingQuotePricing,$iBookingLanguage); 

                        if($kServices->iBookingAlreadyPaid==1)
                        {
                            self::$szDeveloperNotes .= ">. Booking is already placed by using same token ".PHP_EOL;
                            $errorFlag = true;
                            $this->buildErrorDetailAry('szServiceTokenAlreadyUsed','NOSERVICE');  
                        }
                        else if($kServices->iSuccess==1)
                        {
                            /*
                            * updating Shipper consignee data with data what we have received in validate price API call
                            */ 
                            $idShipperConsignee = $this->createShipperConsignee($postSearchAry,$idBooking); 
                            
                            /*
                            * Updating Origin and Destination city and country based on Shipper details
                            */
                            $kCustomerEvents = new cCustomerEevent(); 
                            $kCustomerEvents->updateBookingOriginDestination($postSearchAry,$idBooking);

                            /*
                             * Updating cargo description
                             */
                            $cargoAry = array();
                            $cargoAry['szCargoCommodity'] = $szCargoDescription; 
                            $cargoAry['idBooking'] = $idBooking ;
                            $kRegisterShipCon->addUpdateCargoCommodity($cargoAry); 

                            if(!empty($postSearchAry['szBillingEmail']))
                            {
                                $kCustomerEvents = new cCustomerEevent();
                                $kCustomerEvents->updateCustomerDetailsToBooking($billingDataAry,$idBooking);
                            } 
                            $postSearchAry = array();
                            $postSearchAry = $kBooking->getBookingDetails($idBooking); 

                            $iTodayMinusOne = strtotime("-1 DAY"); 
                            $iTodayMinusOneDateTime = strtotime(date('Y-m-d 23:59:59',$iTodayMinusOne)); 

                            $quickQuoteDetailsAry = array();
                            $bQuickQuoteBooking = false;
                            if($postSearchAry['iQuickQuote']==__QUICK_QUOTE_SEND_QUOTE__)
                            {
                                $bQuickQuoteBooking = true;

                                $kQuote = new cQuote();
                                if($postSearchAry['iBookingType']==1) //LCL
                                { 
                                    $dateAry = array();
                                    $dateAry['dtCutOffDate'] = date('d/m/Y',strtotime($postSearchAry['dtCutOff']));
                                    $dateAry['dtWhsCutOff'] = date('d/m/Y',strtotime($postSearchAry['dtWhsCutOff']));
                                    $dateAry['idServiceType'] = $postSearchAry['idServiceType'];
                                    $dateAry['iBookingCutOffHours'] = $postSearchAry['iBookingCutOffHours'];
                                    $dateAry['iBookingCutOffHours'] = $postSearchAry['iBookingCutOffHours'];

                                    $szTransferDueDateTime = $kQuote->getBankTransferDueDateForLcl($dateAry);
                                    $szBankTransferDueDateTime = t($this->t_base_booking_confirmation.'messages/bank_transfer_text')." ".$szTransferDueDateTime;
                                }
                                else
                                {
                                    $szBankTransferDueDateTime = t($this->t_base_booking_confirmation.'messages/bank_transfer_text_courier');
                                } 
                                /*
                                $kCustomerEvents = new cCustomerEevent(); 
                                $quickQuoteDetailsAry = $kCustomerEvents->validatePricesForQuickQuoteBookings($postSearchAry);
                                if(!empty($quickQuoteDetailsAry))
                                {  
                                    $szQuickQuoteBankTransferDueText = $quickQuoteDetailsAry['szBankTransferDueText'];  
                                }   
                                else
                                {
                                    $errorFlag = true;
                                    self::$szDeveloperNotes .= ">. Quick quote price recalculation failed. Stop! ".PHP_EOL;
                                    $this->buildErrorDetailAry('szQuoteExpired','INVALID'); 
                                } 
                                 * 
                                 */
                            } 
                            else if((strtotime($postSearchAry['dtQuoteValidTo']) < $iTodayMinusOneDateTime ) || ($postSearchAry['iQuotesStatus']==__BOOKING_QUOTES_STATUS_EXPIRED__))
                            {
                                $errorFlag = true;
                                self::$szDeveloperNotes .= ">. Quote has been expired. API called on: ".date('Y-m-d H:i:s')." dtQuoteValidTo: ".$postSearchAry['dtQuoteValidTo']." Status: ".$postSearchAry['iQuotesStatus'].PHP_EOL;
                                $this->buildErrorDetailAry('szQuoteExpired','INVALID');   
                            } 

                            if(!$errorFlag)
                            {
                                $kVatApplication = new cVatApplication();
                                $kVatApplication->updateVatonBooking($idBooking,true);

                                if($bQuickQuoteBooking)
                                {
                                    $szBankTransferDueText = $szQuickQuoteBankTransferDueText;
                                }
                                else
                                { 
                                    $szBankTransferDueText = t($this->t_base_booking_confirmation.'title/bank_transfer_confirmation_text'); 
                                } 
                                $postSearchAry = array();
                                $postSearchAry = $kBooking->getExtendedBookingDetails($idBooking);
                                
                                $idCustomer = $postSearchAry['idUser'];
                                
                                $res_ary_validate=array(); 
                                $res_ary_validate['iSuccessValidatePrice'] = 1;
                                if(!empty($res_ary_validate))
                                {
                                    $update_query = "";
                                    foreach($res_ary_validate as $key=>$value)
                                    {
                                        $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                                    } 
                                    $update_query = rtrim($update_query,",");

                                    $kBooking->updateDraftBooking($update_query,$idBooking);
                                }

                                $kCustomerEvent = new cCustomerEevent();
                                $szCustomerNotes = $kCustomerEvent->getLastBookingsDetailsByCustomer($postSearchAry['idUser']);
                    
                                $succesResponseAry = array();
                                $succesResponseAry['root']['szQuoteID'] = $szQuotePriceKey;
                                $succesResponseAry['root']['szCurrency'] = $postSearchAry['szCurrency'];
                                $succesResponseAry['root']['fBookingPrice'] = round((float)$postSearchAry['fTotalPriceCustomerCurrency']);
                                $succesResponseAry['root']['fVatAmount'] = round((float)$postSearchAry['fTotalVat'],2); 
                                $succesResponseAry['root']['szShipperCity'] = $postSearchAry['szShipperCity'];
                                $succesResponseAry['root']['szConsigneeCity'] = $postSearchAry['szConsigneeCity'];
                                $succesResponseAry['root']['szBankTransferDueText'] = $szBankTransferDueText; 
                                $succesResponseAry['root']['szCustomerNotes'] = $szCustomerNotes; 
                                self::$arrServiceListAry = $succesResponseAry;
                                self::$szGlobalErrorType = 'SUCCESS'; 
                                return self::buildSuccessMessage('root'); 
                            }
                        }
                        else
                        {
                            $errorFlag = true;
                            self::$szDeveloperNotes .= ">. Something went wrong for quote and could not been updated correctly. ".print_R($kServices,true).PHP_EOL;
                            $this->buildErrorDetailAry('szCommunication','INVALID');    
                        } 
                    }
                }
                else
                {
                    $errorFlag = true;
                    self::$szDeveloperNotes .= ">. Something went wrong while building response for the API ".PHP_EOL;
                    $this->buildErrorDetailAry('szCommunication','INVALID');  
                }
                if($errorFlag)
                {
                    self::$szDeveloperNotes .= ">. Returning with API error. Stopped ".PHP_EOL;
                    self::$arrErrorMessageAry = $this->error_detail_ary; 
                    self::$szGlobalErrorType = 'VALIDATION';  
                    return self::buildErrorMessage();
                }
            }
            else
            {  
                /*
                * This code block will be used for Automatic pricing booking
                */
                
                /*
                * Checking if the booking is already placed for the token. This check is only applicable in request mode: LIVE
                */
                if($this->isBookingAlreadyPlacedForToken())
                {
                    self::$szDeveloperNotes .= ">. Booking is already placed by using same token ".PHP_EOL;
                    $errorFlag = true;
                    $this->buildErrorDetailAry('szServiceTokenAlreadyUsed','NOSERVICE');  
                } 
            
                $serviceResponseAry = array();
                $serviceResponseAry = $this->getServiceResponseDetails($idPartner,$szReferenceToken,$szServiceID);  
                if(empty($serviceResponseAry))
                {
                    self::$szDeveloperNotes .= ">. There is no service response matching with requested paramenters ".PHP_EOL;
                    $errorFlag = true;
                    $this->buildErrorDetailAry('szServiceToken','INVALID');   
                }

                if(!$errorFlag)
                {
                    self::$szDeveloperNotes .= ">. We found service response matching with requested paramenters. Going forward ".PHP_EOL;
                    $idShipmentType = $serviceResponseAry['iShipmentType'];
                    $iBookingType = $serviceResponseAry['iBookingType'];
                    $idPackingType = $serviceResponseAry['idPackingType'];
                    $idPartnerApiResponse = $serviceResponseAry['id'];
                    $fTotalInsuranceCostForBookingCustomerCurrency = $serviceResponseAry['fTotalInsuranceCostForBookingCustomerCurrency'];

                    $dtResponseSentDateTime = strtotime($serviceResponseAry['dtCreatedOn']); 
                    $dtMinus24HoursTime = strtotime($dtMinus24HoursDateTime);
                    self::$szDeveloperNotes .= ">. Price response sent on: ".$serviceResponseAry['dtCreatedOn']." Validate Price Request received on: ".$dtMinus24HoursDateTime."".PHP_EOL; 
                    if($dtMinus24HoursTime>=$dtResponseSentDateTime)
                    {
                        /*
                         * If the call is more than 24 hours old then we have returned an error message
                         */ 
                        //$errorFlag = true;
                        //$this->buildErrorDetailAry('szPriceExpired','INVALID'); 
                        self::$szDeveloperNotes .= ">. Requested service is more then 24 hours old so we need to recalculate booking prices again ".PHP_EOL; 
                    }  
                }

                if(!$errorFlag)
                {
                    $kWHSSearch = new cWHSSearch();
                    $tempResultAry = array();
                    $tempResultAry = $kWHSSearch->getSearchedDataFromTempData(false,false,false,true);

                    $searchResultAry = array();
                    $searchResultAry = unserialize($tempResultAry['szSerializeData']);

                    $updateBookingAry = array();
                    if(!empty($searchResultAry))
                    {
                        $updateBookingAry = array();
                        $searchResultTempAry = array();
                        foreach($searchResultAry as $searchResultArys)
                        {
                            if($searchResultArys['unique_id'] == $szServiceID )
                            {
                                $updateBookingAry = $searchResultArys ; 
                                break;
                            }
                        }  
                    }

                    if(empty($updateBookingAry))
                    { 
                        self::$szDeveloperNotes .= ">. There is no service response matching with requested paramenters ".PHP_EOL;
                        $errorFlag = true;
                        $this->buildErrorDetailAry('szServiceToken','INVALID');   
                    }
                } 
                $szReferenceToken = self::$szGlobalToken; 

                /*
                * We are fetching booking ID from tblbookings on the basis of szReferenceToken 
                 * If we finds matching record in tblbookings then we go ahead and update pricing and all for the booking 
                 * If we don't find matching record in tblbooking then we just return False and this process should be stopped at this point.
                */ 
                $kServices = new cServices();
                $bookingDataAry = array();
                $bookingDataAry = $kServices->getBookingDetailsByPriceToken($szReferenceToken);

                $idBooking = 0;
                if(!empty($bookingDataAry))
                {
                    if($bookingDataAry['idBookingStatus']==3 || $bookingDataAry['idBookingStatus']==4 || $bookingDataAry['idBookingStatus']==7)
                    {
                        /*
                        * 3. Booking confirmed
                        * 4. Booking Archived
                        * 7. Booking Cancelled
                        */
                        self::$szDeveloperNotes .= ">. Booking is already placed using the same token because this booking already has status: ".$bookingDataAry['idBookingStatus'].". Stopped! ".PHP_EOL; 
                        $errorFlag = true;
                        $this->buildErrorDetailAry('szServiceTokenAlreadyUsed','NOSERVICE');  
                    }
                    else
                    {
                        $idBooking = $bookingDataAry['id'];
                        $idBookingFile = $bookingDataAry['idFile']; 
                        $idBookingCurrency = $bookingDataAry['idCustomerCurrency'];
                    }
                } 
                
                if(!$errorFlag && $idBooking<=0)
                {
                    $errorFlag = true;
                    cPartner::$szDeveloperNotes .= ">. We did not find any matching row in tblbookings for the Token: ".$szReferenceToken.". Stopped! ".PHP_EOL;
                    $this->buildErrorDetailAry('szPriceToken','INVALID');   
                }

                if($errorFlag)
                {
                    self::$szDeveloperNotes .= ">. Validate price function returning with error message. Stopped ".PHP_EOL;
                    self::$arrErrorMessageAry = $this->error_detail_ary; 
                    self::$szGlobalErrorType = 'VALIDATION';  
                    return self::buildErrorMessage();
                }  
                self::$idBooking = $idBooking;  
                if((int)$idCurrency!=(int)$idBookingCurrency)
                {
                    
                    self::$szDeveloperNotes .= ">.  Validate Price API call is made with a different currency ".PHP_EOL;
                    $errorFlag = true;
                    $this->buildErrorDetailAry('szPriceCurrency','INVALID');
                    self::$arrErrorMessageAry = $this->error_detail_ary; 
                    self::$szGlobalErrorType = 'VALIDATION';  
                    return self::buildErrorMessage();
                }
                else
                {
                    /*
                    * updating Shipper consignee data with data what we have received in validate price API call
                    */
                    $idShipperConsignee = $this->createShipperConsignee($postSearchAry,$idBooking); 

                    /*
                    * Updating Origin and Destination city and country based on Shipper details
                    */
                    $kCustomerEvents = new cCustomerEevent(); 
                    $kCustomerEvents->updateBookingOriginDestination($postSearchAry,$idBooking);
                    
                    $cargoAry = array();
                    $cargoAry['szCargoCommodity'] = $szCargoDescription; 
                    $cargoAry['idBooking'] = $idBooking ;
                    $kRegisterShipCon->addUpdateCargoCommodity($cargoAry); 

                    if(!empty($postSearchAry['szBillingEmail']))
                    {
                        /*
                        * Updating Customer Details to bookings
                        */
                        $kCustomerEvents = new cCustomerEevent();
                        $kCustomerEvents->updateCustomerDetailsToBooking($billingDataAry,$idBooking);
                    }
                    /*
                    * Building data for courier calculation
                    */
                    if($iBookingType==2)
                    {
                        $postSearchAry['iBookingType'] = __BOOKING_TYPE_COURIER__;
                        $fExchangeRate = $updateBookingAry['fExchangeRate'];
                    }
                    else
                    {
                        $postSearchAry['iBookingType'] = __BOOKING_TYPE_AUTOMATIC__;
                        $fExchangeRate = $updateBookingAry['fUsdValue'];
                    } 
                    $postSearchAry['szShipperPostCode'] = $postSearchAry['szOriginPostCode'];
                    $postSearchAry['idShipperCountry'] = $postSearchAry['idOriginCountry'];
                    $postSearchAry['idShipperCountry'] = $postSearchAry['idOriginCountry']; 
                    $postSearchAry['szConsigneePostCode'] = $postSearchAry['szDestinationPostCode'];
                    $postSearchAry['idConsigneeCountry'] = $postSearchAry['idDestinationCountry'];
                    $postSearchAry['idConsigneeCountry'] = $postSearchAry['idDestinationCountry']; 
                    $postSearchAry['idForwarder'] = $updateBookingAry['idForwarder'];
                    $postSearchAry['idServiceProvider'] = $updateBookingAry['idCourierAgreement'];
                    $postSearchAry['idServiceProviderProduct'] = $updateBookingAry['idCourierProviderProductid'];
                    $postSearchAry['fTotalPriceUSD'] = $updateBookingAry['fBookingPriceUSD'];
                    $postSearchAry['idForwarderCurrency'] = $updateBookingAry['idForwarderCurrency'];
                    $postSearchAry['idCurrency'] = $updateBookingAry['idCurrency'];
                    $postSearchAry['fExchangeRate'] = $fExchangeRate;
                    $postSearchAry['fTotalPriceCustomerCurrency'] = $updateBookingAry['fDisplayPrice'];
                    $postSearchAry['fDisplayPrice'] = $updateBookingAry['fDisplayPrice']; 
                    $postSearchAry['fTotalVat'] = $updateBookingAry['fTotalVat']; 
                    $postSearchAry['idPricingWtw'] = $updateBookingAry['idWTW'];

                    if($updateBookingAry['szCustomerType']=='Private')
                    {
                        $postSearchAry['iPrivateShipping'] = 1;
                    }
                    else
                    {
                        $postSearchAry['iPrivateShipping'] = 0;
                    }
 
                    if($idShipmentType==1)
                    {
                        self::$searchablePacketTypes = array();
                        self::$searchablePacketTypes[] = 1;
                        self::$searchablePacketTypes[] = 3;
                        self::$idGlobalShipmentType = 1;
                    }
                    else if($idShipmentType==2)
                    {
                        self::$searchablePacketTypes = array();
                        self::$searchablePacketTypes[] = 2;
                        self::$searchablePacketTypes[] = 3;
                        self::$idGlobalShipmentType = 2;
                    } 
                    $szReferenceToken = self::$szGlobalToken; 
                    if($iBookingType==2)
                    { 
                        /*
                        * For recalculating prices we are now storing last courier price response in table called tblfedexapi in column szResponseSerializedData
                        */
                        $kWebServices = new cWebServices();
                        $responseSerializedDataAry = array();
                        $responseSerializedDataTNTAry = array();
                        $courierSearchResult = array();
                        $courierSearchResult = $kWebServices->getCourierApiReponseFromTemp($szReferenceToken,101);
                        if(!empty($courierSearchResult['szResponseSerializedData']))
                        {
                            $apiResponseAry = array();
                            $apiResponseAry = unserialize($courierSearchResult['szResponseSerializedData']);
                            $responseSerializedDataAry = $this->processCourierResultData($apiResponseAry); 
                        } 
                        /*
                        * Finding out courier service result for TNT
                        */
                        $courierSearchResultTNT = array();
                        $courierSearchResultTNT = $kWebServices->getCourierApiReponseFromTemp($szReferenceToken,102);
                        if(!empty($courierSearchResultTNT['szResponseSerializedData']))
                        {
                            $apiResponseAry = array();
                            $apiResponseAry = unserialize($courierSearchResultTNT['szResponseSerializedData']);
                            $responseSerializedDataTNTAry = $this->processCourierResultData($apiResponseAry);
                        }

                        if(!empty($responseSerializedDataAry))
                        {
                            cEasyPost::$iEasyPostApiCalled = true;
                            cEasyPost::$globalEasyPostApiResponse = $responseSerializedDataAry;
                        }
                        if(!empty($responseSerializedDataTNTAry))
                        {
                            cEasyPost::$iEasyPostApiCalledTNT = true;
                            cEasyPost::$globalEasyPostApiResponseTNT = $responseSerializedDataTNTAry;
                        }
                    }
                    self::$GlobalpostSearchAry = $postSearchAry;
                    $kQuote = new cQuote();
                    $szRecalculationTag = $kQuote->recalculateQuickQuotePricing($postSearchAry,true,true);

                    $szPartnerAPICalculationLogs = $kQuote->szCourierCalculationLogString;
                    self::$priceCalculationLogs = $szPartnerAPICalculationLogs;  

                    if($szRecalculationTag=='SUCCESS' || $szRecalculationTag=='WARNING')
                    {
                        if($kQuote->fTotalPriceCustomerCurrencyVaildate>0)
                        {
                            $postSearchAry = array();
                            $postSearchAry = $kBooking->getExtendedBookingDetails($idBooking);
                            
                            $res_ary_validate=array(); 
                            $res_ary_validate['iSuccessValidatePrice'] = 1;
                            if(!empty($res_ary_validate))
                            {
                                $update_query = "";
                                foreach($res_ary_validate as $key=>$value)
                                {
                                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                                } 
                                $update_query = rtrim($update_query,",");

                                $kBooking->updateDraftBooking($update_query,$idBooking);
                            }
                            
                            $kCustomerEvent = new cCustomerEevent();
                            $szCustomerNotes = $kCustomerEvent->getLastBookingsDetailsByCustomer($postSearchAry['idUser']);

                            $succesResponseAry = array();
                            $succesResponseAry['root']['ServiceID']=$szServiceID;
                            $succesResponseAry['root']['szCurrency']=$kQuote->szCustomerCurrencyVaildate;
                            $succesResponseAry['root']['fBookingPrice']=$kQuote->fTotalPriceCustomerCurrencyVaildate;
                            $succesResponseAry['root']['szShipperCity'] = $postSearchAry['szShipperCity'];
                            $succesResponseAry['root']['szConsigneeCity'] = $postSearchAry['szConsigneeCity'];
                            $succesResponseAry['root']['fVatAmount']=$kQuote->fTotalVatCustomerCurrencyVaildate; 
                            $succesResponseAry['root']['szCustomerNotes'] = $szCustomerNotes;
                            if(!empty($kQuote->dtBankTransferDueDate))
                            {
                                //$succesResponseAry['root']['dtBankTransferDueDate'] = $kQuote->dtBankTransferDueDate;
                            }
                            if(!empty($kQuote->szBankTransferDueText))
                            {
                                $succesResponseAry['root']['szBankTransferDueText'] = cApi::encodeString($kQuote->szBankTransferDueText);
                            }
                            self::$arrServiceListAry = $succesResponseAry;
                            self::$szGlobalErrorType = 'SUCCESS'; 
                            return self::buildSuccessMessage('root');
                        }
                        else
                        {
                            self::$szDeveloperNotes .= ">. Price for service has been changed please make a new price request to obtain new prices. Stopped ".PHP_EOL;

                            $errorFlag = true;
                            $this->buildErrorDetailAry('szNoRecordFound','NOSERVICE');   
                            self::$arrErrorMessageAry = $this->error_detail_ary; 
                            self::$szGlobalErrorType = 'VALIDATION';  
                            return self::buildErrorMessage(); 
                        } 
                    } 
                    else
                    {
                        self::$szDeveloperNotes .= ">. Price for service has been changed please make a new price request to obtain new prices. Stopped ".PHP_EOL;
                        self::$szNotification = "E1171";
                        $this->buildErrorDetailAry('szPriceExpired','INVALID'); 
                        self::$szDeveloperNotes .= ">. Validate price function returning with error message. Stopped ".PHP_EOL;
                        self::$arrErrorMessageAry = $this->error_detail_ary; 
                        self::$szGlobalErrorType = 'VALIDATION';  
                        return self::buildErrorMessage();
                    } 
                }
            } 
        }
    } 
    
    function processCourierResultData($apiResponseAry)
    {
        if(!empty($apiResponseAry))
        { 
            $dtShippingDate = date('Y-m-d');
            $carrierResponseAry = array();
            foreach($apiResponseAry as $apiResponseArys)
            {  
                $courierApiRatesAry = array();
                $courierApiRatesAry = $apiResponseArys['rates'];
                if(!empty($courierApiRatesAry))
                {
                    foreach($courierApiRatesAry as $courierApiRatesArys)
                    {
                        $szCarrierAccountId = trim($courierApiRatesArys['carrier_account_id']);  
                        $carrierResponseAry[$szCarrierAccountId][$ctr] = $courierApiRatesArys;
                        $carrierResponseAry[$szCarrierAccountId][$ctr]['dtShippingDate'] = $dtShippingDate; 
                        $ctr++; 
                    }
                } 
            } 
            return $carrierResponseAry;
        }
    }
    
    function clearOldValidatePriceData()
    {
        /*
         * Check if there is any onld row with 'validatePrice' exists
         */
        $requestDataAry = array();
        $requestDataAry = $this->getQuickQuoteRequestData('validatePrice');
         
        if(!empty($requestDataAry))
        {
            /*
            * Deleting old rows with request type 'validatePrice'
            */
            $this->deleteOldValidatePriceRequest();
            
            /*
            * Deleting old cargo rows with request type 'validatePrice'
            */
            $this->deleteOldValidatePriceRequestCargoLines();
        } 
    }
    
    function deleteOldValidatePriceRequest()
    { 
        $idPartner = self::$idGlobalPartner;
        $szReferenceToken = self::$szGlobalToken; 
        $szRequestMode = self::$szApiRequestMode;
        
        $query="
            DELETE FROM  
                ".__DBC_SCHEMATA_PARTNERS_API_CALL__."
            WHERE
                szReferenceToken='".mysql_escape_custom(trim($szReferenceToken))."'
            AND
                idPartner = '".(int)$idPartner."' 
            AND
                szRequestType = 'validatePrice' 
            AND
                szRequestMode = '".mysql_escape_custom(trim($szRequestMode))."'
        "; 
       // echo "<br><br>".$query; 
        if($result=$this->exeSQL($query))
        { 
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function deleteOldValidatePriceRequestCargoLines()
    { 
        $idPartner = self::$idGlobalPartner;
        $szReferenceToken = self::$szGlobalToken;
         
        $query="
            DELETE FROM 
                ".__DBC_SCHEMATA_PARTNERS_API_CARGO_DETAIL__."
            WHERE
               idPartner = '".(int)$idPartner."'	 
             AND
                szReferenceToken = '".  mysql_escape_custom($szReferenceToken)."' 
             AND
                szRequestType = 'validatePrice' 
        "; 
        //echo "<br>".$query."<br>" ;
        if($result = $this->exeSQL($query))
        { 
            return true;
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
    
    private function processServiceApiRequest($postSearchAry,$mode=false)
    { 
        $kBooking = new cBooking();
        $kWHSSearch = new cWHSSearch();
        if(!empty($postSearchAry))
        {  
            $this->iParcelCalculationCounter = 1;
            $this->iPalletCalculationCounter = 1;
            $this->iBreakbulkCalculationCounter = 1;
            $iNumDaysPayBeforePickup = $kWHSSearch->getManageMentVariableByDescription('__NUMBER_OF_WEEKDAYS_PAYMENT_REQUIRED_BEFORE_REQUESTED_PICKUP__'); 
            if(strtoupper($postSearchAry['szServiceTerm'])=='ALL')
            {
                $searchResultAry = $this->searchTransportecaServicesForAllTerms($postSearchAry,$mode); 
            }
            else
            {
                $searchResultAry = $this->searchTransportecaServices($postSearchAry,$mode); 
            } 
            if($mode=='QUICK_QUOTE')
            {
                return $searchResultAry;
            }
            
            if($postSearchAry['iSearchStandardCargo']==1)
            {
                $this->serviceFoundForVogaQuote=$searchResultAry;
                $szCustomerToken = $postSearchAry['szCustomerToken'];
                if(!empty($szCustomerToken))
                {
                    $kBooking = new cBooking();
                    $kTransportecaApi = new cTransportecaApi();
                    $kTransportecaApi->isTokenValid($szCustomerToken); 
                    $idCustomer = $kTransportecaApi->idApiCustomer; 
                }
                else if(!empty($postSearchAry['szBillingEmail']))
                {
                    $iAddNewUser = false;
                    $kUser = new cUser();
                    if($kUser->isUserEmailExist($postSearchAry['szBillingEmail']))
                    {  
                        $idCustomer = $kUser->idIncompleteUser ;
                    }
                }
               
                if($idCustomer>0)
                {
                    $kUser = new cUser();
                    $kUser->getUserDetails($idCustomer);
                    if($kUser->iPrivate==1)
                    {
                        $postSearchAry['szCustomerCompanyName']=$kUser->szFirstName." ".$kUser->szLastName;
                    }
                    else
                    {
                        $postSearchAry['szCustomerCompanyName']=$kUser->szCompanyName;
                    }
                    
                }
            }
            self::$searchValues = array('weight'=>$postSearchAry['fCargoWeight'],'cbm'=>$postSearchAry['fCargoVolume']);
            if(!empty($searchResultAry))
            { 
                $serviceAry = array();
                
                if($postSearchAry['iSearchStandardCargo']==1)
                {
                    $this->iCreateAutoQuoteForVogaBooking=1;
                    
                    $iWeekDay = date('w');
                    if($iWeekDay==5 || $iWeekDay==6) //5: Friday, 6: Saturday
                    {
                        $iResponseDay = 1 ;
                    }
                    else
                    {
                        $iResponseDay = 0 ; 
                    }
                    $bookingResponseAry = array();                                        
                    $bookingResponseAry['notification'] = "Transporteca will send the transportation quotation directly to ".$postSearchAry['szCustomerCompanyName']." within one working day";
                    $bookingResponseAry['iResponseDay'] = $iResponseDay;
                    self::$arrServiceListAry = $bookingResponseAry;
                    self::$szGlobalErrorType = 'SUCCESS'; 
                    return self::buildSuccessMessage('Booking');
                }
                else
                {
                    $kCourierServices = new cCourierServices();
                    $kConfig = new cConfig();
                    $transportModeListAry = array();
                    $transportModeListAry = $kConfig->getAllTransportMode(false,$postSearchAry['iBookingLanguage'],true);
                    $kServices = new cServices();
                    /*
                    * Following array always contains values in english
                    */
                    $englishTransportModeListAry = array();
                    $englishTransportModeListAry = $kConfig->getAllTransportMode(false,__LANGUAGE_ID_ENGLISH__,true);

                    $modeTransport=false;
                    if($kCourierServices->checkFromCountryToCountryExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry']))
                    {
                        $modeTransport=true;
                    }

                    $ctr = 0;
                    $iLanguage = $postSearchAry['iBookingLanguage'];
                    $courier_service_ctr=0;
                    $rail_service_counter=0;
                    $lcl_service_counter=0;
                    $air_service_counter=0;
                    $air_service_business_counter=0;
                    $iOnlyCartonServiceDisplayedCtr=0;
                    $air_service_business_counter=0;
                    $lcl_service_business_counter=0;
                    $lcl_service_private_counter=0;
                    $courier_service_business_ctr=0;
                    $courier_service_private_ctr=0;
                    $rail_service_business_counter=0;
                    $rail_service_private_counter=0;
                    
                    $rail_service_counter_for_offer=0;
                    $rail_service_business_counter_for_offer=0;
                    $rail_service_private_counter_for_offer=0;
                    
                    $air_service_counter_for_offer=0;
                    $air_service_business_counter_for_offer=0;
                    $air_service_private_counter_for_offer=0;
                    
                    $sea_service_business_counter_for_offer=0;
                    $sea_service_business_counter_for_offer=0;
                    $sea_service_counter_for_offer=0;
                    
                                        
                    $showOfferFlag=true;
                    foreach($searchResultAry as $searchResultArys)
                    {

                        $searchResultArys['szWarehouseFromCity']=$searchResultArys['szFromWHSCity'];
                        $searchResultArys['szWarehouseToCity']=$searchResultArys['szToWHSCity'];
                        $searchResultArys['szShipperCity']=$postSearchAry['szOriginCity'];
                        $searchResultArys['szConsigneeCity']=$postSearchAry['szDestinationCity'];
                        $searchResultArys['idOriginCountry']=$postSearchAry['idOriginCountry'];
                        $searchResultArys['idDestinationCountry']=$postSearchAry['idDestinationCountry'];

                        if((int)$searchResultArys['countOriginCC']>0)
                        {
                            $searchResultArys['iOriginCC']=1;
                        }

                        if((int)$searchResultArys['countDestinationCC']>0)
                        {
                            $searchResultArys['iDestinationCC']=2;
                        } 
                        //$searchResultArys['idTransportMode']=$postSearchAry['idTransportMode'];
                        //$szServiceDescription = display_service_type_description($searchResultArys,$iLanguage,true,true);
                        //$szServiceDescription = display_service_type_description($searchResultArys,$iLanguage,false,true);

                        $dtCutOffDate = date("Y-m-d H:i:s",strtotime(str_replace("/","-",trim($searchResultArys['dtCutOffDate'])))); 
                        $dtAvailableDate = date("Y-m-d H:i:s",strtotime(str_replace("/","-",trim($searchResultArys['dtAvailableDate']))));

                        $serviceAry[$ctr]['szServiceID'] = $searchResultArys['unique_id'];     
                        $serviceAry[$ctr]['dtTransportStartDay'] = date('d',strtotime($dtCutOffDate));
                        $serviceAry[$ctr]['dtTransportStartMonth'] = date('m',strtotime($dtCutOffDate));
                        $serviceAry[$ctr]['dtTransportStartYear'] = date('Y',strtotime($dtCutOffDate));
                        if($searchResultArys['idCourierProvider']>0)
                        {
                           $serviceAry[$ctr]['szTransportStartCity'] = $postSearchAry['szOriginCity'];
                        }
                        else
                        {
                            if($searchResultArys['idServiceType']==__SERVICE_TYPE_DTD__ || $searchResultArys['idServiceType']==__SERVICE_TYPE_DTW__ || $searchResultArys['idServiceType']==__SERVICE_TYPE_DTP__)
                            {
                                $serviceAry[$ctr]['szTransportStartCity'] = $postSearchAry['szOriginCity'];
                            }
                            else
                            {
                                $serviceAry[$ctr]['szTransportStartCity'] = $searchResultArys['szFromWHSCity'];
                            } 
                        } 
                        $dtTransportStartMonth =date('m',strtotime($dtCutOffDate));
                        $szStartDateDay = date('j.',strtotime($dtCutOffDate)); 
                        $szMonthName = getMonthName($iLanguage,$dtTransportStartMonth,true);
                        $szTransportStartDate =$szStartDateDay." ".$szMonthName ;
                        $serviceAry[$ctr]['szTransportStartDate'] = cApi::encodeString($szTransportStartDate);

                        $serviceAry[$ctr]['dtTransportFinishDay'] = date('d',strtotime($dtAvailableDate));
                        $serviceAry[$ctr]['dtTransportFinishMonth'] = date('m',strtotime($dtAvailableDate));
                        $serviceAry[$ctr]['dtTransportFinishYear'] = date('Y',strtotime($dtAvailableDate));
                        
                        $dtTransportFinishMonth = date('m',strtotime($dtAvailableDate));
                        $szFinishDateDay = date('j.',strtotime($dtAvailableDate)); 
                        $szMonthName = getMonthName($iLanguage,$dtTransportFinishMonth,true);
                        $szTransportFinishDate =$szFinishDateDay." ".$szMonthName ;
                        $serviceAry[$ctr]['szTransportFinishDate'] = cApi::encodeString($szTransportFinishDate);
                        
                        $kConfig = new cConfig(); 
                        $languageTextDataAry=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$iLanguage);
                        $szDay_txt=$languageTextDataAry[1]['szDay_txt'];
                        $iTransitDays='';
                        if(!empty($dtCutOffDate) && $dtCutOffDate!='0000-00-00 00:00:00' && !empty($dtAvailableDate) && $dtAvailableDate!='0000-00-00 00:00:00')
                        {    
                            $totalDays=ceil((strtotime($dtAvailableDate)-strtotime($dtCutOffDate))/86400);
                            $iTransitDays = $totalDays." ".$szDay_txt; 
                        }
                        $serviceAry[$ctr]['szDays'] = cApi::encodeString($iTransitDays);
                        
                        if($searchResultArys['idCourierProvider']>0)
                        {
                           $serviceAry[$ctr]['szTransportFinishCity'] = $postSearchAry['szDestinationCity'];
                        }
                        else
                        {
                            if($searchResultArys['idServiceType']==__SERVICE_TYPE_DTD__ || $searchResultArys['idServiceType']==__SERVICE_TYPE_WTD__ || $searchResultArys['idServiceType']==__SERVICE_TYPE_PTD__)
                            {
                                $serviceAry[$ctr]['szTransportFinishCity'] = $postSearchAry['szDestinationCity'];
                            }
                            else
                            {
                                $serviceAry[$ctr]['szTransportFinishCity'] = $searchResultArys['szToWHSCity'];
                            }
                        }


                        $szDtdTrade=0;
                        $kCourierServices = new cCourierServices();
                        if($kCourierServices->checkFromCountryToCountryExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true))
                        { 
                            $szDtdTrade=1;
                        }                    
                        //$serviceAry[$ctr]['szDtdTrade']=$szDtdTrade;

                        $idShipmentType = $searchResultArys['idShipmentType'];
                        $cargoDetailsAry = array();
                        $cargoDetailsAry = $this->updateBookingCargDetails($idShipmentType);

                        $fShipmentTotalVolume = $cargoDetailsAry['fTotalVolume'];
                        $fShipmentfTotalWeight = $cargoDetailsAry['fTotalWeight'];
                        $iShipmentTotalQuantity = $cargoDetailsAry['iTotalQuantity']; 

                        $szBankTransferDueDay = '';
                        $szBankTransferDueMonth = '';
                        $szBankTransferDueYear = '';
                        $szBankTransferDueDateTime='';
                        $szCourierCargo = "";  
                        if($searchResultArys['idCourierProvider']>0)
                        {
                            $szLanguageCode=  strtolower($this->szLanguage);
                            $kConfig = new cConfig();
                            $languageArr=$kConfig->getLanguageDetails($szLanguageCode);
                            $idLanguage=__LANGUAGE_ID_ENGLISH__;
                            if(!empty($languageArr))
                            {
                                $idLanguage=$languageArr[0]['id'];
                            }                        
                            if($searchResultArys['szCustomerType']=='Private')
                            {
                                $air_service_private_counter++;
                            }
                            else
                            {
                                $air_service_business_counter++;
                            }
                            $air_service_counter++;

                            $packingArr = $kCourierServices->getCourierProviderProductList($searchResultArys['idCourierProvider'],$searchResultArys['idCourierProviderProductid'],$idLanguage); 
                            $packingText_only= t($this->t_base_service.'fields/only');

                            if($packingArr[0]['idPackingType'] == __COURIER_PACKAGING_TYPE_PALLETS_AND_CARTONS__)
                            {
                                $szPackingType = ucfirst($packingArr[0]['szPacking']);
                            } 
                            else
                            {
                                $szPackingType = $packingText_only." ".strtolower($packingArr[0]['szPacking']);
                            }  

                            if($modeTransport)
                            {
                                $showOfferFlag=false;
                            }

                            if($packingArr[0]['idPackingType']==1)
                            {
                                $iOnlyCartonServiceDisplayedCtr++;
                            }

                            $postSearchAry['idTransportMode'] = __BOOKING_TRANSPORT_MODE_COURIER__;
                            $iBookingType = 2;
                            $idPackingType = $packingArr[0]['idPackingType'];

                            if($iShipmentTotalQuantity>0)
                            {
                                $szCourierCargo = "".$iShipmentTotalQuantity." ".t($this->t_base_service.'fields/colli').", "; 
                            }
                            $szCollection = 'Yes';
                            $szDelivery = 'Yes';
                            
                            if($iNumDaysPayBeforePickup<=0)
                            {
                                $iNumDaysPayBeforePickup = 2;
                            }
                            $szBankTransferDueDateTime=date('Y-m-d',strtotime($dtCutOffDate));

                            $szBankTransferDueDateTime=getBusinessDaysForCourier($szBankTransferDueDateTime,$iNumDaysPayBeforePickup.' DAY');
                            if($szBankTransferDueDateTime!='')
                            {
                                $szBankTransferDueDay = date('d',strtotime($szBankTransferDueDateTime));
                                $szBankTransferDueMonth = date('m',strtotime($szBankTransferDueDateTime));
                                $szBankTransferDueYear = date('Y',strtotime($szBankTransferDueDateTime));
                            }
                        }
                        else
                        { 

                            if($searchResultArys['iWarehouseType']==__WAREHOUSE_TYPE_AIR__)
                            {
                                $postSearchAry['idTransportMode'] = __BOOKING_TRANSPORT_MODE_AIR__; 
                            }
                            else if($postSearchAry['iDTDTrades']==1)
                            {
                                $postSearchAry['idTransportMode'] = __BOOKING_TRANSPORT_MODE_ROAD__; 
                            }
                            else if($searchResultArys['iRailTransport']==1)
                            { 
                                $postSearchAry['idTransportMode'] = __BOOKING_TRANSPORT_MODE_RAIL__; 
                            }
                            else
                            {    
                                $postSearchAry['idTransportMode'] = __BOOKING_TRANSPORT_MODE_SEA__; 
                            } 
                            if($postSearchAry['iDTDTrades']==1)
                            {
                                if($postSearchAry['fCargoVolume']>0.6 || $postSearchAry['fCargoWeight']>270)
                                {
                                    $szPackingType = t($this->t_base_service.'fields/euro_pallets'); 
                                }
                                else
                                {
                                    $szPackingType = t($this->t_base_service.'fields/half_euro_pallets'); 
                                }
                            } 
                            else
                            {
                                $szPackingType = t($this->t_base_service.'fields/pallets_and_cartons'); 
                            } 
                            $iBookingType = 1; 


                            $railTransportAry = array();
                            $railTransportAry['idForwarder'] = $searchResultArys['idForwarder'];
                            $railTransportAry['idFromWarehouse'] = $searchResultArys['idWarehouseFrom'];
                            $railTransportAry['idToWarehouse'] = $searchResultArys['idWarehouseTo'];  

                            if($kServices->isRailTransportExists($railTransportAry))
                            {
                                if($searchResultArys['szCustomerType']=='Private')
                                {
                                    $rail_service_private_counter++;
                                }
                                else
                                {
                                    $rail_service_business_counter++;
                                }
                                $rail_service_counter++;
                            }
                            else 
                            {    
                                if($searchResultArys['iWarehouseType']==__WAREHOUSE_TYPE_AIR__)
                                {
                                    if($searchResultArys['szCustomerType']=='Private')
                                    {
                                        $air_service_private_counter++;
                                    }
                                    else
                                    {
                                        $air_service_business_counter++;
                                    }
                                    $air_service_counter++;
                                }
                                else
                                {
                                    if($searchResultArys['szCustomerType']=='Private')
                                    {
                                        $lcl_service_private_counter++;
                                    }
                                    else
                                    {
                                        $lcl_service_business_counter++; 
                                    }
                                    $lcl_service_counter++;
                                }
                                
                            }

                            if($searchResultArys['idServiceType']==__SERVICE_TYPE_DTD__ || $searchResultArys['idServiceType']==__SERVICE_TYPE_DTW__ || $searchResultArys['idServiceType']==__SERVICE_TYPE_DTP__) // 1.DTD 2.DTD
                            {
                                $szCollection = 'Yes';
                            }
                            else
                            {
                                $szCollection = 'No';
                            } 
                            if($searchResultArys['idServiceType']==__SERVICE_TYPE_DTD__ || $searchResultArys['idServiceType']==__SERVICE_TYPE_WTD__ || $searchResultArys['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD 2.DTD
                            {
                                $szDelivery = 'Yes';
                            }
                            else
                            {
                                $szDelivery = 'No';
                            }  
                            $iBookingCutOffHours=$searchResultArys['iBookingCutOffHours'];
                            if($searchResultArys['idServiceType']==__SERVICE_TYPE_DTD__)
                            {
                                $szBankTransferDueDateTime = strtotime($searchResultArys['dtCuttOffDate_formated']) - ($iBookingCutOffHours*60*60);
                            }
                            else
                            {
                                $szBankTransferDueDateTime = $searchResultArys['dtWhsCutOff_time'] - ($iBookingCutOffHours*60*60);
                            }
                            if($szBankTransferDueDateTime!='')
                            {
                                $szBankTransferDueDay = date('d',$szBankTransferDueDateTime);
                                $szBankTransferDueMonth = date('m',$szBankTransferDueDateTime);
                                $szBankTransferDueYear = date('Y',$szBankTransferDueDateTime);
                            }


                        } 
                        $kAdmin = new cAdmin();
                        $idShipperCountry=$this->idShipperCountry;

                        $shipperCountryArr=$kAdmin->countriesView($idShipperCountry,$idShipperCountry);

                        if($szCollection=='Yes' && $shipperCountryArr['iUsePostcodes']==1)
                        {
                            $szShipperPostcodeRequired="Yes";
                        }
                        else
                        {
                            $szShipperPostcodeRequired="No";
                        }

                        $kAdmin = new cAdmin();
                        $consigneeCountryArr=$kAdmin->countriesView($this->idConsigneeCountry,$this->idConsigneeCountry);
                        if($szDelivery=='Yes' && $consigneeCountryArr['iUsePostcodes']==1)
                        {
                            $szConsigneePostcodeRequired="Yes";
                        }
                        else
                        {
                            $szConsigneePostcodeRequired="No";
                        }

                        $szCargoCommodity = $postSearchAry['szCargoDescription'];
                        $cargo_volume = format_volume($fShipmentTotalVolume);
                        if($iLanguage==__LANGUAGE_ID_DANISH__)
                        {
                             $cargoVolumeText=$cargo_volume." m3";
                        }
                        else
                        {
                            $cargoVolumeText=$cargo_volume." ".t($this->t_base_service.'fields/cbm');
                        } 

                        $cargo_weight = number_format_custom((float)ceil($fShipmentfTotalWeight),$iLanguage) ;

                        if($postSearchAry['iSearchPallet']==1)
                        {
                            $szCargo = $postSearchAry['szServiceLineCargoDescription'].", ";
                        }
                        else
                        {
                            $szCargo = $cargoVolumeText.", ".$cargo_weight." ".t($this->t_base_service.'fields/kg').", ".$szCourierCargo.cApi::encodeString($szCargoCommodity); 
                        } 
                        $searchResultArys['idTransportMode']=$postSearchAry['idTransportMode'];



                        //echo $this->iUsePartnersBillingDetails."iUsePartnersBillingDetails<br />";
                        if($this->iUsePartnersBillingDetails)
                        {
                            $iShipperConsignee=3;
                        }
                        else
                        {
                            if($this->idShipperCountry==$this->idBillingCountry)
                            {
                                $iShipperConsignee=1;
                            }
                            else if($this->idConsigneeCountry==$this->idBillingCountry)
                            {
                                $iShipperConsignee=2;
                            }
                            else
                            {
                                $iShipperConsignee=3;
                            }
                        }
                        $searchResultArys['iShipperConsignee']=$iShipperConsignee;  

                        //$szServiceDescription = display_service_type_description($searchResultArys,$iLanguage,true,true);
                        $szServiceDescription = display_service_type_description($searchResultArys,$iLanguage,false,true);

                        $szTransportMode = $transportModeListAry[$postSearchAry['idTransportMode']]['szLongName'];
                        $szTransportModeCode = $englishTransportModeListAry[$postSearchAry['idTransportMode']]['szShortName']; 

                        if($postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_COURIER__ || $postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_AIR__)
                        {                            
                            $szTransportIcon=__AIR_ICON__;
                            if($postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_COURIER__)
                            {
                                $kCourierServices = new cCourierServices();
                                if($kCourierServices->checkFromCountryToCountryExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],false))
                                { 
                                    $szTransportIcon=__TRUCK_ICON__;
                                }
                            }
                        }
                        else if($postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_FCL__ || $postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_SEA__)
                        {
                            $szTransportIcon=__SEA_ICON__;
                        }
                        else if($postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_ROAD__ || $postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_FTL__ || $postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_MOVING__)
                        {
                            $szTransportIcon=__TRUCK_ICON__;                            
                        }
                        else if($postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_RAIL__)
                        {
                            $szTransportIcon=__RAIL_ICON__;
                            
                        }
                        $totalPrice=0;
                        $totalPrice=round((float)$searchResultArys['fDisplayPrice'])+round((float)$searchResultArys['fTotalVat'],2);
                        $serviceAry[$ctr]['szCommodity'] = 1; 
                        $serviceAry[$ctr]['szPackingType'] = cApi::encodeString($szPackingType);
                        $serviceAry[$ctr]['szCurrency'] = $searchResultArys['szCurrency']; 
                        $serviceAry[$ctr]['fBookingPrice'] = round((float)$searchResultArys['fDisplayPrice']);
                        $serviceAry[$ctr]['szBookingPrice'] = number_format_custom((float)$searchResultArys['fDisplayPrice'],$iLanguage);
                        $serviceAry[$ctr]['fVatAmount'] = round((float)$searchResultArys['fTotalVat'],2); 
                        $serviceAry[$ctr]['szVatAmount'] = number_format_custom((float)$searchResultArys['fTotalVat'],$iLanguage,2);
                        $serviceAry[$ctr]['szCustomerType'] = $searchResultArys['szCustomerType'];
                        $totalPrice = round((float)$totalPrice,2);
                        $totalPriceArr=explode(".",$totalPrice);
                        if((int)$totalPriceArr[1]==0)
                        {
                            $serviceAry[$ctr]['szTotalTransportPrice'] = number_format_custom((float)$totalPrice,$iLanguage);
                        }
                        else
                        {
                            $serviceAry[$ctr]['szTotalTransportPrice'] = number_format_custom((float)$totalPrice,$iLanguage,2);
                        }                        
                        $serviceAry[$ctr]['szTransportMode'] = cApi::encodeString($szTransportMode);
                        $serviceAry[$ctr]['szTransportModeCode'] = cApi::encodeString($szTransportModeCode);
                        $serviceAry[$ctr]['szTransportIcon'] = $szTransportIcon;
                        $serviceAry[$ctr]['szForwarderAlias'] = $searchResultArys['szForwarderAlias'];
                        $serviceAry[$ctr]['szServiceDescription'] = cApi::encodeString($szServiceDescription);
                        $serviceAry[$ctr]['szServiceTerm'] = $searchResultArys['szServiceTerm'];
                        $serviceAry[$ctr]['szCollection'] = $szCollection;
                        $serviceAry[$ctr]['szDelivery'] = $szDelivery;
                        $serviceAry[$ctr]['szShipperPostcodeRequired'] = $szShipperPostcodeRequired;
                        $serviceAry[$ctr]['szConsigneePostcodeRequired'] = $szConsigneePostcodeRequired;


                        $postSearchAry['fTotalPriceUSD'] = $searchResultArys['fDisplayPrice'] * $postSearchAry['fExchangeRate'];

                        $kForwarder = new cForwarder();
                        $kForwarder->load($searchResultArys['idForwarder']);

                        $kConfig = new cConfig();
                        $kConfig->loadCountry($kForwarder->idCountry);
                        $szCarrierCountry = $kConfig->szCountryISO;

                        $serviceAry[$ctr]['szCarrierName'] = cApi::encodeString($kForwarder->szDisplayName);
                        $serviceAry[$ctr]['szCarrierAddressLine1'] = cApi::encodeString($kForwarder->szAddress);
                        $serviceAry[$ctr]['szCarrierAddressLine2'] = cApi::encodeString($kForwarder->szAddress2);
                        $serviceAry[$ctr]['szCarrierAddressLine3'] = cApi::encodeString($kForwarder->szAddress3);
                        $serviceAry[$ctr]['szCarrierPostcode'] = $kForwarder->szPostCode;
                        $serviceAry[$ctr]['szCarrierCity'] = cApi::encodeString($kForwarder->szCity);
                        $serviceAry[$ctr]['szCarrierRegion'] = $kForwarder->szState;
                        $serviceAry[$ctr]['szCarrierCountry'] = $szCarrierCountry;
                        $serviceAry[$ctr]['szCarrierCompanyRegistration'] = cApi::encodeString($kForwarder->szCompanyRegistrationNum);
                        $serviceAry[$ctr]['szCarrierTerms'] = cApi::encodeString($kForwarder->szLink);
                        $serviceAry[$ctr]['szCargo'] = $szCargo;

                        if($postSearchAry['fCargoValue']>0)
                        {
                            $insuranceInputAry = array();
                            $insurancePriceAry = array();
                            $insuranceInputAry['iInsurance'] = 1;
                            $insuranceInputAry['idInsuranceCurrency'] = $postSearchAry['idGoodsInsuranceCurrency'];
                            $insuranceInputAry['fValueOfGoods'] = $postSearchAry['fCargoValue'];  

                            $insuranceBookingDataAry = array();
                            $insuranceBookingDataAry = $postSearchAry;
                            $insuranceBookingDataAry['fTotalPriceCustomerCurrency'] = $searchResultArys['fDisplayPrice'];
                            $insuranceBookingDataAry['fTotalVat'] = $searchResultArys['fTotalVat'];

                            $insurancePriceAry = $kBooking->updateInsuranceDetails($insuranceInputAry,$insuranceBookingDataAry,false,true); 

                            if(!empty($insurancePriceAry))
                            {
                                //t($this->t_base_service.'fields/pick_yes')
                                $serviceAry[$ctr]['bInsuranceAvailable'] = 'Yes';
                                $serviceAry[$ctr]['fInsurancePrice'] = round((float)$insurancePriceAry['fTotalInsuranceCostForBookingCustomerCurrency'],2); 
                            }
                            else
                            {
                                //t($this->t_base_service.'fields/pick_no')
                                $serviceAry[$ctr]['bInsuranceAvailable'] = 'No';
                            } 
                        }
                        else
                        {
                            //$serviceAry[$ctr]['bInsuranceAvailable'] = utf8_encode(t($this->t_base_service.'fields/cargo_value_required')); 
                            $serviceAry[$ctr]['bInsuranceAvailable'] = "No, Cargo value required"; 
                        } 
                        $serviceAry[$ctr]['dtLatestPaymentDay'] = $szBankTransferDueDay; 
                        $serviceAry[$ctr]['dtLatestPaymentMonth'] = $szBankTransferDueMonth; 
                        $serviceAry[$ctr]['dtLatestPaymentYear'] = $szBankTransferDueYear; 

                        $responseLogAry = array();
                        $responseLogAry['idPartner'] = self::$idGlobalPartner; 
                        $responseLogAry['szReferenceToken'] = self::$szGlobalToken; 
                        $responseLogAry['szServiceID'] = $serviceAry[$ctr]['szServiceID'];
                        $responseLogAry['dtExpectedDelivery'] = $dtAvailableDate;
                        $responseLogAry['szPackingType'] = cApi::encodeString($szPackingType);
                        $responseLogAry['fBookingPrice'] = $serviceAry[$ctr]['fBookingPrice'];
                        $responseLogAry['fVatAmount'] = $serviceAry[$ctr]['fVatAmount'];
                        $responseLogAry['bInsuranceAvailable'] = $serviceAry[$ctr]['bInsuranceAvailable'];
                        $responseLogAry['fInsurancePrice'] = $serviceAry[$ctr]['fInsurancePrice'];
                        $responseLogAry['idCurrency'] = $searchResultArys['idCurrency'];
                        $responseLogAry['szCurrency'] = $searchResultArys['szCurrency'];
                        $responseLogAry['fExchangeRate'] = $postSearchAry['fExchangeRate'];
                        $responseLogAry['iShipmentType'] = $searchResultArys['idShipmentType'];
                        $responseLogAry['iBookingType'] = $iBookingType;
                        $responseLogAry['idPackingType'] = $idPackingType; 
                        $responseLogAry['szCustomerType'] = $searchResultArys['szCustomerType'];
                        $responseLogAry['szServiceDescription'] = cApi::encodeString($szServiceDescription);
                        $responseLogAry['szCollection'] = $szCollection;
                        $responseLogAry['szDelivery'] = $szDelivery;
                        $responseLogAry['szShipperPostcodeRequired'] = $szShipperPostcodeRequired;
                        $responseLogAry['szConsigneePostcodeRequired'] = $szConsigneePostcodeRequired;

                        if(!empty($insurancePriceAry))
                        {
                            $responseLogAry['fValueOfGoods'] = $insurancePriceAry['fValueOfGoods'];
                            $responseLogAry['fValueOfGoodsUSD'] = $insurancePriceAry['fValueOfGoodsUSD'];
                            $responseLogAry['idGoodsInsuranceCurrency'] = $insurancePriceAry['idGoodsInsuranceCurrency'];
                            $responseLogAry['szGoodsInsuranceCurrency'] = $insurancePriceAry['szGoodsInsuranceCurrency'];
                            $responseLogAry['fGoodsInsuranceExchangeRate'] = $insurancePriceAry['fGoodsInsuranceExchangeRate'];
                            $responseLogAry['fTotalInsuranceCostForBooking'] = $insurancePriceAry['fTotalInsuranceCostForBooking']; 
                            $responseLogAry['fTotalInsuranceCostForBookingCustomerCurrency'] = $insurancePriceAry['fTotalInsuranceCostForBookingCustomerCurrency'];
                            $responseLogAry['fTotalInsuranceCostForBookingUSD'] = $insurancePriceAry['fTotalInsuranceCostForBookingUSD'];
                            $responseLogAry['fTotalAmountInsured'] = $insurancePriceAry['fTotalAmountInsured'];
                            $responseLogAry['iMinrateApplied'] = $insurancePriceAry['iMinrateApplied']; 
                            $responseLogAry['fTotalInsuranceCostForBookingCustomerCurrency_buyRate'] = $insurancePriceAry['fTotalInsuranceCostForBookingCustomerCurrency_buyRate'];
                            $responseLogAry['fTotalInsuranceCostForBookingUSD_buyRate'] = $insurancePriceAry['fTotalInsuranceCostForBookingUSD_buyRate'];
                        } 

                        $this->addServiceApiResponse($responseLogAry);
                        $ctr++;

                    }  
                    $iOfferTypeForCourier='';
                    $iOfferTypeForLCL='';
                    if($szDtdTrade==0)
                    {
                        if($lcl_service_counter==0 && $air_service_counter>0 && $modeTransport)
                        {
                            $iOfferTypeForCourier=4;
                            if($rail_service_counter>0)
                            {
                                $iOfferTypeForCourier=9;
                            }                            
                            if((int)$air_service_business_counter==0)
                            {
                                $szCustomerTypeAir="BUSINESS";
                            }
                            else if((int)$air_service_private_counter==0)
                            {
                                $szCustomerTypeAir="PRIVATE";
                            }
                            $szCustomerTypeLCL='["PRIVATE","BUSINESS"]';
                            
                        }
                        else
                        {
                            $iOfferTypeValidForLCL=3;
                            if((int)$air_service_counter>0 && $rail_service_counter>0)
                            {
                                $iOfferTypeValidForLCL=12;
                            }
                            if((int)$air_service_counter>0)
                            {
                                if($modeTransport)
                                {
                                    $iOfferTypeForCourier=1;
                                }
                                else
                                {
                                    $iOfferTypeForCourier=2;
                                }
                                if((int)$air_service_business_counter==0)
                                {
                                    $szCustomerTypeAir="BUSINESS";
                                }
                                else if((int)$air_service_private_counter==0)
                                {
                                    $szCustomerTypeAir="PRIVATE";
                                }
                            }
                            if($air_service_counter==0)
                            {
                                $kCourierService = new cCourierServices();  
                                if($kCourierService->checkFromCountryToCountryExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry']))
                                {
                                    if((int)$lcl_service_counter>0 && $rail_service_counter==0)
                                    {
                                        $iOfferTypeForCourier=1;
                                    }
                                    else  if((int)$lcl_service_counter>0 && $rail_service_counter>0)
                                    {
                                        $iOfferTypeForCourier=11;
                                    }
                                    else if((int)$lcl_service_counter==0 && $rail_service_counter>0)
                                    {
                                        $iOfferTypeForCourier=7;
                                    } 
                                } 
                                else
                                {

                                    if((int)$lcl_service_counter>0 && $rail_service_counter>0)
                                    {
                                        $iOfferTypeForCourier=10;
                                    }
                                    else if((int)$lcl_service_counter>0 && $rail_service_counter==0)
                                    {
                                        $iOfferTypeForCourier=2;
                                    }
                                    else if((int)$lcl_service_counter==0 && $rail_service_counter>0)
                                    {
                                        $iOfferTypeForCourier=8;
                                    }
                                } 
                                $iOfferTypeValidForLCL=6;
                                $tr_counter++;
                                
                                $szCustomerTypeAir='["PRIVATE","BUSINESS"]';
                            } 
                            if((int)$lcl_service_counter>0)
                            {
                                if((int)$lcl_service_business_counter==0)
                                {
                                    $szCustomerTypeLCL="BUSINESS";
                                }
                                else if((int)$lcl_service_private_counter==0)
                                {
                                    $szCustomerTypeLCL="PRIVATE";
                                }
                            }
                            if($lcl_service_counter==0 && $showOfferFlag)
                            {
                                $iOfferTypeForLCL=$iOfferTypeValidForLCL;
                                $szCustomerTypeLCL='["PRIVATE","BUSINESS"]';
                            }                          
                        }
                        
                        if($iOfferTypeForCourier==1 || $iOfferTypeForCourier==7 || $iOfferTypeForCourier==11 || $iOfferTypeForCourier==4 || $iOfferTypeForCourier==9) //Trucking Icon
                        {
                           $szTransportIconForCourier=__TRUCK_ICON__;
                           $idTransportModeForCourier = __BOOKING_TRANSPORT_MODE_ROAD__;
                           $kConfig=new cConfig();
                           $transportModeArr=$kConfig->getAllTransportMode($idTransportModeForCourier,$iLanguage);
                           $szTransportModeForCourier = $transportModeArr[0]['szLongName']; 
                           $szCustomerTypeAir = $szCustomerTypeAir; 
                        }
                        else if($iOfferTypeForCourier==2 || $iOfferTypeForCourier==8 || $iOfferTypeForCourier==10) //Trucking Icon
                        {
                            $szTransportIconForCourier=__AIR_ICON__;
                            $idTransportModeForCourier = __BOOKING_TRANSPORT_MODE_AIR__ ;
                            $kConfig=new cConfig();
                            $transportModeArr=$kConfig->getAllTransportMode($idTransportModeForCourier,$iLanguage);
                            $szTransportModeForCourier = $transportModeArr[0]['szLongName'] ;
                            $szCustomerTypeAir = $szCustomerTypeAir; 
                        }

                        if($iOfferTypeForLCL==12 || $iOfferTypeForLCL==6 || $iOfferTypeForLCL==3) 
                        {
                            $szTransportIconForLCL=__SEA_ICON__;
                            $idTransportModeForLCL = __BOOKING_TRANSPORT_MODE_SEA__ ;
                            $kConfig=new cConfig();
                            $transportModeArr=$kConfig->getAllTransportMode($idTransportModeForLCL,$iLanguage);
                            $szTransportModeForLCL = $transportModeArr[0]['szLongName'] ;
                            $szSeaCustomerType = $szCustomerTypeLCL;
                        }
                    }
                    $ctrRFQ=0;
                    $serviceRFQAry=array();
                    if($iOfferTypeForLCL!='' || $iOfferTypeForCourier!='')
                    {
                        if($iOfferTypeForLCL!='')
                        {
                            $lclOfferResponse=$this->getOfferTypeArray($iOfferTypeForLCL,$szTransportModeForLCL,$idTransportModeForLCL,$szTransportIconForLCL,$szSeaCustomerType);
                            $serviceRFQAry[$ctrRFQ]=$lclOfferResponse;
                            ++$ctrRFQ;
                        }

                        if($iOfferTypeForCourier!='')
                        {
                            $courierOfferResponse=$this->getOfferTypeArray($iOfferTypeForCourier,$szTransportModeForCourier,$idTransportModeForCourier,$szTransportIconForCourier,$szCustomerTypeAir);
                           $serviceRFQAry[$ctrRFQ]=$courierOfferResponse;

                            ++$ctrRFQ;
                        }

                    }
                    if($iOfferTypeForLCL!='' || $iOfferTypeForCourier!='')
                    {
                        self::$iOfferTypeFlag =1;
                        $iWeekDay = date('w');
                        if($iWeekDay==5 || $iWeekDay==6) //5: Friday, 6: Saturday
                        {
                            $iResponseDay = 1 ;
                        }
                        else
                        {
                            $iResponseDay = 0 ; 
                        }
                        self::$iResponseDay = $iResponseDay;
                    }
                    self::$iNumServiceFound = count($serviceAry);
                    self::$szDtdTradeValue =$szDtdTrade; 
                    self::$arrServiceListAry = $serviceAry;
                    self::$arrRFQListAry = $serviceRFQAry;
                    self::$szGlobalErrorType = 'SUCCESS'; 
                    return self::buildSuccessMessage();
                }
            } 
            if($this->error == true)
            {       
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage();  
            }
            else if(empty($searchResultAry))
            { 
                if($this->iSearchStandardCargo==1)
                {
                   $this->iCreateRFQForStandardCargo=1;
                    $iWeekDay = date('w');
                    if($iWeekDay==5 || $iWeekDay==6) //5: Friday, 6: Saturday
                    {
                        $iResponseDay = 1 ;
                    }
                    else
                    {
                        $iResponseDay = 0 ; 
                    }
                    $bookingResponseAry = array();
                    $bookingResponseAry['notification'] = "Transporteca will send the transportation quotation directly to ".$postSearchAry['szCustomerCompanyName']." within one working day";
                    $bookingResponseAry['iResponseDay'] = $iResponseDay;
                    self::$arrServiceListAry = $bookingResponseAry;
                    self::$szGlobalErrorType = 'SUCCESS'; 
                    return self::buildSuccessMessage('Booking');
                }
                else
                {
                    $iWeekDay = date('w');
                    if($iWeekDay==5 || $iWeekDay==6) //5: Friday, 6: Saturday
                    {
                        $iResponseDay = 1 ;
                    }
                    else
                    {
                        $iResponseDay = 2 ; 
                    }
                    self::$bNoserviceFound = 1;
                    self::$iResponseDay = $iResponseDay; 
                    $errorFlag = true;
                    $this->buildErrorDetailAry('szNoRecordFound','NOSERVICE');   
                    self::$arrErrorMessageAry = $this->error_detail_ary; 
                    self::$szGlobalErrorType = 'VALIDATION';  
                    return self::buildErrorMessage('price');  
                }
            }
            return $result_Ary;
        } 
    }
    
    private static function initializeCourierVars()
    {
        cEasyPost::$iEasyPostApiCalled = false;
        cEasyPost::$iEasyPostApiCalledTNT = false;
        cEasyPost::$iEasyPostApiCalledStandardAgreementTNT = false;
        
        cEasyPost::$globalEasyPostApiResponse = array();
        cEasyPost::$globalEasyPostApiResponseTNT = array();

        cEasyPost::$globalEasyPostApiResponseStandardAgreementTNT = array();
        cEasyPost::$globalEasyPostApiResponseStandardAgreement = array();

        cPostmen::$globalPostMenApiResponse = array();
        cPostmen::$globalEasyPostApiResponse = array();
    }
    
    function searchTransportecaServicesForAllTerms($postSearchAry,$mode=false)
    {
        if(!empty($postSearchAry))
        {
            $iCheckCustomerClearanceFlag=false;
            $kCourierServices = new cCourierServices();    
            if(!$kCourierServices->checkFromCountryToCountryExistsForCC($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry']))
            {
                $iCheckCustomerClearanceFlag=true;
            }
                
            /*
            *  when we receives a partner API call with szServiceTerm: ALL then we do following things
             * a. We only makes courier services call with szServiceTerm: EXW and have reused the same service line for other terms
            */
            $szServiceTerm = "EXW";
            $serviceTypeAry = array();
            $serviceTypeAry = $this->getServiceType($szServiceTerm);

            $serviceSearchInputAry = array();
            $serviceSearchInputAry = $postSearchAry;
            $serviceSearchInputAry['szServiceTerm'] = $szServiceTerm;
            $serviceSearchInputAry['idServiceType'] = $serviceTypeAry['idServiceType'];

            if($iCheckCustomerClearanceFlag)
            {
                $serviceSearchInputAry['iOriginCC'] = $serviceTypeAry['iOriginCC'];
                $serviceSearchInputAry['iDestinationCC'] = $serviceTypeAry['iDestinationCC']; 
            }
            else
            {
                $serviceSearchInputAry['iOriginCC'] = 0;
                $serviceSearchInputAry['iDestinationCC'] = 0;
            } 
            
            /*
            * Fetching all the courier services
            */ 
            $courierServiceResultAry = array();
            $courierServiceResultAry = $this->searchTransportecaServices($serviceSearchInputAry,$mode,1,'COURIER');  
              
            $kConfig = new cConfig();
            $serviceTermsAry = array();
            $serviceTermsAry = $kConfig->getAllServiceTerms(false,false,true);
             
            $finalSearchResultAry = array();
            $ctr = 0;
            if(!empty($serviceTermsAry))
            {
                foreach($serviceTermsAry as $serviceTermsArys)
                {
                    $szServiceTerm = $serviceTermsArys['szShortTerms'];
                     
                    $serviceTypeAry = array();
                    $serviceTypeAry = $this->getServiceType($szServiceTerm);
                     
                    $serviceSearchInputAry = array();
                    $serviceSearchInputAry = $postSearchAry;
                    $serviceSearchInputAry['szServiceTerm'] = $szServiceTerm;
                    $serviceSearchInputAry['idServiceType'] = $serviceTypeAry['idServiceType'];
                     
                    if($iCheckCustomerClearanceFlag)
                    {
                        $serviceSearchInputAry['iOriginCC'] = $serviceTypeAry['iOriginCC'];
                        $serviceSearchInputAry['iDestinationCC'] = $serviceTypeAry['iDestinationCC']; 
                    }
                    else
                    {
                        $serviceSearchInputAry['iOriginCC'] = 0;
                        $serviceSearchInputAry['iDestinationCC'] = 0;
                    } 
                    
                    if($postSearchAry['iDTDTrades']==1 && ($szServiceTerm=='CFR' || $szServiceTerm=='FOB'))
                    {
                        /*
                         * If trading terms is “CFR” or "FOB" (in this case, do not run search if trade is defined in http://management.transporteca.com/dtdTrades/)
                         */
                        $this->iStopLclSearchParcel = 1;
                        $szDeveloperNotes .= "<br><br>Status: PARCEL, stopped LCL search";
                        $szDeveloperNotes .= "<br>Description: Trade is defined under /dtdTrades/ and service term is: ".$data['szServiceTerm'];
                    }
                    
                    if(($szServiceTerm=='EXW' || $szServiceTerm=='DAP') && (!empty($searchTermResultAry['EXW']) || !empty($searchTermResultAry['DAP'])))
                    {
                        /*
                        * In the process o save processing time, we have clubed the response for EXW and DAP so 
                        * If we receives result for any of 1 then have used the same search result for another.
                        * e.g. if we receives the result for service term: EXW then we have used the same search result for service term DAP
                        */
                        if(!empty($searchTermResultAry['EXW']))
                        {
                            $searchResultAry = $searchTermResultAry['EXW'];
                        }
                        else
                        {
                            $searchResultAry = $searchTermResultAry['DAP'];
                        }
                    }
                    else
                    { 
                        $searchResultAry = array();
                        $searchResultAry = $this->searchTransportecaServices($serviceSearchInputAry,$mode,1,'LCL'); 
                        
                        if($szServiceTerm=='EXW' || $szServiceTerm=='DAP') 
                        {
                            $searchTermResultAry[$szServiceTerm] = $searchResultAry;
                        }
                    }
                    
                    if(!empty($searchResultAry))
                    {
                        foreach($searchResultAry as $searchResultArys)
                        {
                            $finalSearchResultAry[$ctr] = $searchResultArys;
                            $ctr++;
                        }
                    }
                    
                    if(!empty($courierServiceResultAry))
                    {
                        foreach($courierServiceResultAry as $courierServiceResultArys)
                        {
                            $finalSearchResultAry[$ctr] = $courierServiceResultArys;
                            $finalSearchResultAry[$ctr]['szServiceTerm'] = $szServiceTerm;
                            $ctr++;
                        }
                    }
                }
            } 
            return $finalSearchResultAry;
        } 
    } 
    
    function searchTransportecaServices($postSearchAry,$mode=false,$iCalculationCounter=1,$bSearchServiceType='ALL')
    {
        $kWHSSearch = new cWHSSearch();  
        $szPartnerAPICalculationLogs = '';
        $bulkSearchResultAry = array();
        $parcelSearchResultAry = array();
        $palletSearchResultAry = array(); 
         
        $bPartnerApi = true;
        if($mode=='QUICK_QUOTE')
        {
            $bPartnerApi = false;
        }  
        if($this->iUnknownShipmentType==1)
        {
            $szPartnerAPICalculationLogs = "Shipment type: Unknown";
        }
        else
        { 
            if($bSearchServiceType=='LCL')
            {
                $bOnlySearchLCLServices = true;
                $bOnlySearchCourierServices = false;
                $bSearchAllServices = false;
            }
            else if($bSearchServiceType=='COURIER')
            {
                $bOnlySearchLCLServices = false;
                $bOnlySearchCourierServices = true;
                $bSearchAllServices = false;
            }
            else
            {
                $bOnlySearchLCLServices = false;
                $bOnlySearchCourierServices = false;
                $bSearchAllServices = true;
            }
            $idCustomerCurrencye = $postSearchAry['idCustomerCurrency'];
            if($this->iSearchParcel==1)
            { 
                $kWHSSearch->szCourierCalculationLogString = '';
                $kWHSSearch->szLCLCalculationLogs = '';

                /*
                 * We only initialize courier API variable on first calculation attempt. This field only make sense when we are searching with szServiceTerms = ALL
                 * 
                 * The reason for doing this is, When we are searching szServiceTerm: ALL 
                 * Then we iterate all service terms listed in tblservicesterms one by one and we perform individual calculation on individual service term but calls Easypost API only once for a Shipment Type. 
                 */
                if($this->iParcelCalculationCounter==1)
                {
                    self::initializeCourierVars();
                    $this->iParcelCalculationCounter = 2;
                }
                
                /*
                * Calculating services for shipment type: PARCEL
                */
                if($this->iStopLclSearchParcel==1 && ($bSearchAllServices || $bOnlySearchCourierServices))
                {
                    /*
                    * We are not going to search LCL services as some condition were failed while validation, Please check szDeveloperNotes in 
                    */
                    /*
                    *  Calculating services for Parcel shipment
                    */ 
                    $postSearchAry['fCargoVolume'] = $this->fTotalCargoVolumeParcel;
                    $postSearchAry['fCargoWeight'] = $this->fTotalCargoWeightParcel;
                    self::$searchablePacketTypes = array();
                    self::$searchablePacketTypes[] = 1;
                    self::$searchablePacketTypes[] = 3;
                    self::$idGlobalShipmentType = 1;
                    if(!empty($postSearchAry['customerType']) && in_array(__PARTNER_API_CUSTOMER_TYPE_BUSINESS__,$postSearchAry['customerType']))
                    {
                        $parcelSearchResultAry = $kWHSSearch->getCourierProviderData($postSearchAry,$idCustomerCurrencye,false,'PARTNER_API_CALL');  
                        $szPartnerAPICalculationLogs .= "<br> <h3> Case: Searching for Parcel Shipment</h3> <br>";
                        $szPartnerAPICalculationLogs .= $kWHSSearch->szCalculationLogString; 
                    } 
                    
                    if(!empty($postSearchAry['customerType']) && in_array(__PARTNER_API_CUSTOMER_TYPE_PRIVATE__,$postSearchAry['customerType']))
                    {
                        /*
                        * Calculating services for private customer
                        */
                        $privateSearchAry = array();
                        $privateSearchAry = $postSearchAry;
                        $privateSearchAry['iPrivateShipping'] = 1;

                        $privateCustomerParcelSearchResultAry = $kWHSSearch->getCourierProviderData($privateSearchAry,$idCustomerCurrencye,false,'PARTNER_API_CALL'); 

                        if(!empty($privateCustomerParcelSearchResultAry) && !empty($parcelSearchResultAry))
                        {
                            $parcelSearchResultAry = array_merge($parcelSearchResultAry,$privateCustomerParcelSearchResultAry);
                        }
                        else if(!empty($privateCustomerParcelSearchResultAry))
                        {
                            $parcelSearchResultAry = $privateCustomerParcelSearchResultAry;
                        }
                        $szPartnerAPICalculationLogs .= "<br> <h3> Case: Searching for Parcel Shipment - Private Customer</h3> <br>";
                        $szPartnerAPICalculationLogs .= $kWHSSearch->szCalculationLogString; 
                    } 
                }
                else
                {
                    /*
                    *  Calculating services for Parcel shipment
                    */
                    $postSearchAry['fCargoVolume'] = $this->fTotalCargoVolumeParcel;
                    $postSearchAry['fCargoWeight'] = $this->fTotalCargoWeightParcel;
                    self::$searchablePacketTypes = array();
                    self::$searchablePacketTypes[] = 1;
                    self::$searchablePacketTypes[] = 3;
                    self::$idGlobalShipmentType = 1;
                    
                    if(!empty($postSearchAry['customerType']) && in_array(__PARTNER_API_CUSTOMER_TYPE_BUSINESS__,$postSearchAry['customerType']))
                    {
                        if($bOnlySearchLCLServices)
                        {
                            /*
                            * If $bOnlySearchLCLServices is set to true then we searches only for LCL services.
                            * Normally we arrives in this section only in case when user searches for szServiceTerm: ALL
                            */
                            $parcelSearchResultAry = $kWHSSearch->get_search_result($postSearchAry,false,'PARTNER_API_CALL',false,true);   
                        }
                        else if($bOnlySearchCourierServices)
                        {
                            /*
                            * If $bOnlySearchLCLServices is set to true then we searches only for LCL services.
                            * Normally we arrives in this section only in case when user searches for szServiceTerm: ALL
                            */
                            $parcelSearchResultAry = $kWHSSearch->getCourierProviderData($postSearchAry,$idCustomerCurrencye,false,'PARTNER_API_CALL');
                        }
                        else
                        {
                            $parcelSearchResultAry = $kWHSSearch->get_search_result($postSearchAry,false,'PARTNER_API_CALL');   
                        } 
                        $szPartnerAPICalculationLogs .= "<br> <h3> Case: Searching for Parcel Shipment</h3> <br>";
                        $szPartnerAPICalculationLogs .= $kWHSSearch->szCourierCalculationLogString;  
                    }
                    
                    if(!empty($postSearchAry['customerType']) && in_array(__PARTNER_API_CUSTOMER_TYPE_PRIVATE__,$postSearchAry['customerType']))
                    {
                        /*
                        * Calculating services for private customer
                        */ 
                        $kWHSSearch->szCourierCalculationLogString = "";
                        $privateSearchAry = array();
                        $privateSearchAry = $postSearchAry;
                        $privateSearchAry['iPrivateShipping'] = 1;
 
                        if($bOnlySearchLCLServices)
                        {
                            /*
                            * If $bOnlySearchLCLServices is set to true then we searches only for LCL services.
                            * Normally we arrives in this section only in case when user searches for szServiceTerm: ALL
                            */
                            $privateCustomerParcelSearchResultAry = $kWHSSearch->get_search_result($privateSearchAry,false,'PARTNER_API_CALL',false,true);   
                        }
                        else if($bOnlySearchCourierServices)
                        {
                            /*
                            * If $bOnlySearchLCLServices is set to true then we searches only for LCL services.
                            * Normally we arrives in this section only in case when user searches for szServiceTerm: ALL
                            */
                            $privateCustomerParcelSearchResultAry = $kWHSSearch->getCourierProviderData($privateSearchAry,$idCustomerCurrencye,false,'PARTNER_API_CALL');
                        }
                        else
                        {
                            $privateCustomerParcelSearchResultAry = $kWHSSearch->get_search_result($privateSearchAry,false,'PARTNER_API_CALL');   
                        } 
                        if(!empty($privateCustomerParcelSearchResultAry) && !empty($parcelSearchResultAry))
                        {
                            $parcelSearchResultAry = array_merge($parcelSearchResultAry,$privateCustomerParcelSearchResultAry);
                        }
                        else if(!empty($privateCustomerParcelSearchResultAry))
                        {  
                            $parcelSearchResultAry = $privateCustomerParcelSearchResultAry;
                        } 
                        $szPartnerAPICalculationLogs .= "<br> <h3> Case: Searching for Parcel Shipment - Private Customer</h3> <br>";
                        $szPartnerAPICalculationLogs .= $kWHSSearch->szCourierCalculationLogString; 
                    }
                }
            } 
            if($this->iSearchPallet==1)
            { 
                if($this->iPalletCalculationCounter==1)
                {
                    self::initializeCourierVars();
                    $this->iPalletCalculationCounter = 2;
                }
                $kWHSSearch->szCourierCalculationLogString = '';
                $kWHSSearch->szLCLCalculationLogs = '';
                /*
                * Calculating services for shipment type: PALLET
                */
                if($this->iStopAllSearchPallet==1)
                {
                    /*
                    * We are not going to search services as some condition were failed while validation, Please check szDeveloperNotes in 
                    */
                }
                else if($this->iStopLclSearchPallet==1 && ($bSearchAllServices || $bOnlySearchCourierServices))
                {
                    $idCustomerCurrencye = $postSearchAry['idCustomerCurrency'];
                    $postSearchAry['fCargoVolume'] = $this->fTotalCargoVolumePallet;
                    $postSearchAry['fCargoWeight'] = $this->fTotalCargoWeightPallet;

                    /*
                    *  Calculating only courier service for shipment type pallet
                    */
                    self::$searchablePacketTypes = array();
                    self::$searchablePacketTypes[] = 2;
                    self::$searchablePacketTypes[] = 3;
                    self::$idGlobalShipmentType = 2;
                     
                    if(!empty($postSearchAry['customerType']) && in_array(__PARTNER_API_CUSTOMER_TYPE_BUSINESS__,$postSearchAry['customerType']))
                    {
                        $palletSearchResultAry = $kWHSSearch->getCourierProviderData($postSearchAry,$idCustomerCurrencye,false,'PARTNER_API_CALL');

                        $szPartnerAPICalculationLogs .= "<br> <h3> Searching for Pallet Shipment</h3> <br>";
                        $szPartnerAPICalculationLogs .= $kWHSSearch->szCalculationLogString; 
                    } 
                    
                    if(!empty($postSearchAry['customerType']) && in_array(__PARTNER_API_CUSTOMER_TYPE_PRIVATE__,$postSearchAry['customerType']))
                    {
                        /*
                        * Calculating services for private customer
                        */
                        $privateSearchAry = array();
                        $privateSearchAry = $postSearchAry;
                        $privateSearchAry['iPrivateShipping'] = 1;

                        $privateCustomerParcelSearchResultAry = $kWHSSearch->getCourierProviderData($privateSearchAry,$idCustomerCurrencye,false,'PARTNER_API_CALL'); 

                        if(!empty($privateCustomerParcelSearchResultAry) && !empty($parcelSearchResultAry))
                        {
                            $parcelSearchResultAry = array_merge($parcelSearchResultAry,$privateCustomerParcelSearchResultAry);
                        }
                        else if(!empty($privateCustomerParcelSearchResultAry))
                        {
                            $parcelSearchResultAry = $privateCustomerParcelSearchResultAry;
                        }
                        $szPartnerAPICalculationLogs .= "<br> <h3> Case: Searching for Parcel Shipment - Private Customer</h3> <br>";
                        $szPartnerAPICalculationLogs .= $kWHSSearch->szCalculationLogString; 
                    } 
                }
                else
                {
                    /*
                    *  Calculating services for Pallet shipment
                    */
                    $postSearchAry['fCargoVolume'] = $this->fTotalCargoVolumePallet;
                    $postSearchAry['fCargoWeight'] = $this->fTotalCargoWeightPallet;
                    self::$searchablePacketTypes = array();
                    self::$searchablePacketTypes[] = 2;
                    self::$searchablePacketTypes[] = 3;
                    self::$idGlobalShipmentType = 2;
                    
                    if(!empty($postSearchAry['customerType']) && in_array(__PARTNER_API_CUSTOMER_TYPE_BUSINESS__,$postSearchAry['customerType']))
                    {
                        if($bOnlySearchCourierServices)
                        {
                            /*
                            * If $bOnlySearchLCLServices is set to true then we searches only for LCL services.
                            * Normally we arrives in this section only in case when user searches for szServiceTerm: ALL
                            */
                            $palletSearchResultAry = $kWHSSearch->get_search_result($postSearchAry,false,'PARTNER_API_CALL',false,true);   
                        } 
                        else if($bOnlySearchCourierServices)
                        {
                            /*
                            * If $bOnlySearchLCLServices is set to true then we searches only for LCL services.
                            * Normally we arrives in this section only in case when user searches for szServiceTerm: ALL
                            */
                            $palletSearchResultAry = $kWHSSearch->getCourierProviderData($postSearchAry,$idCustomerCurrencye,false,'PARTNER_API_CALL');
                        }
                        else
                        {
                            $palletSearchResultAry = $kWHSSearch->get_search_result($postSearchAry,false,'PARTNER_API_CALL');   
                        } 
                        $szPartnerAPICalculationLogs .= "<br> <h3>Case: Searching for Pallet Shipment</h3> <br>";
                        $szPartnerAPICalculationLogs .= $kWHSSearch->szCourierCalculationLogString; 
                    }
                    
                    if(!empty($postSearchAry['customerType']) && in_array(__PARTNER_API_CUSTOMER_TYPE_PRIVATE__,$postSearchAry['customerType']))
                    {
                        /*
                        * Calculating services for private customer
                        */
                        
                        $kWHSSearch->szCourierCalculationLogString = "";
                        $privateSearchAry = array();
                        $privateSearchAry = $postSearchAry;
                        $privateSearchAry['iPrivateShipping'] = 1;
  
                        $privateCustomerParcelSearchResultAry = array();
                        if($bOnlySearchCourierServices)
                        {
                            /*
                            * If $bOnlySearchLCLServices is set to true then we searches only for LCL services.
                            * Normally we arrives in this section only in case when user searches for szServiceTerm: ALL
                            */
                            $privateCustomerParcelSearchResultAry = $kWHSSearch->get_search_result($privateSearchAry,false,'PARTNER_API_CALL',false,true);   
                        } 
                        else if($bOnlySearchCourierServices)
                        {
                            /*
                            * If $bOnlySearchLCLServices is set to true then we searches only for LCL services.
                            * Normally we arrives in this section only in case when user searches for szServiceTerm: ALL
                            */
                            $privateCustomerParcelSearchResultAry = $kWHSSearch->getCourierProviderData($privateSearchAry,$idCustomerCurrencye,false,'PARTNER_API_CALL');
                        }
                        else
                        {
                            $privateCustomerParcelSearchResultAry = $kWHSSearch->get_search_result($privateSearchAry,false,'PARTNER_API_CALL');   
                        } 

                        if(!empty($privateCustomerParcelSearchResultAry) && !empty($palletSearchResultAry))
                        {
                            $palletSearchResultAry = array_merge($palletSearchResultAry,$privateCustomerParcelSearchResultAry);
                        }
                        else if(!empty($privateCustomerParcelSearchResultAry))
                        {
                            $palletSearchResultAry = $privateCustomerParcelSearchResultAry;
                        }
                        $szPartnerAPICalculationLogs .= "<br> <h3> Case: Searching for Parcel Shipment - Private Customer</h3> <br>";
                        $szPartnerAPICalculationLogs .= $kWHSSearch->szCourierCalculationLogString; 
                    }
                }
            }
            
            if($this->iSearchBreakBulk==1 && ($bSearchAllServices || $bOnlySearchLCLServices))
            { 
                if($this->iBreakbulkCalculationCounter==1)
                {
                    self::initializeCourierVars();
                    $this->iBreakbulkCalculationCounter = 2;
                }                
                $kWHSSearch->szCourierCalculationLogString = '';
                $kWHSSearch->szLCLCalculationLogs = '';
                /*
                * Calculating services for shipment type: BREAK_BULK
                */
                if($this->iStopAllSearchBulk==1)
                {
                    /*
                    * We are not going to search services as some condition were failed while validation, Please check szDeveloperNotes in 
                    */
                }
                else
                {
                    /*
                    *  Calculating services for Pallet shipment
                    */
                    $postSearchAry['fCargoVolume'] = $this->fTotalCargoVolumeBreakBulk;
                    $postSearchAry['fCargoWeight'] = $this->fTotalCargoWeightBreakBulk;

                    self::$idGlobalShipmentType = 3;
                    if(!empty($postSearchAry['customerType']) && in_array(__PARTNER_API_CUSTOMER_TYPE_BUSINESS__,$postSearchAry['customerType']))
                    {
                        $bulkSearchResultAry = $kWHSSearch->get_search_result($postSearchAry,false,'PARTNER_API_CALL',false,true);  

                        $szPartnerAPICalculationLogs .= "<br> <h3>Case: Searching for Break bulk Shipment</h3> <br>";
                        $szPartnerAPICalculationLogs .= $kWHSSearch->szCourierCalculationLogString; 
                    } 
                    
                    if(!empty($postSearchAry['customerType']) && in_array(__PARTNER_API_CUSTOMER_TYPE_PRIVATE__,$postSearchAry['customerType']))
                    {
                        /*
                        * Calculating services for private customer
                        */ 
                        $kWHSSearch->szCourierCalculationLogString = "";
                        $privateSearchAry = array();
                        $privateSearchAry = $postSearchAry;
                        $privateSearchAry['iPrivateShipping'] = 1;

                        $privateCustomerParcelSearchResultAry = array();
                        $privateCustomerParcelSearchResultAry = $kWHSSearch->get_search_result($privateSearchAry,false,'PARTNER_API_CALL',false,true); 
                        

                        if(!empty($privateCustomerParcelSearchResultAry) && !empty($bulkSearchResultAry))
                        {
                            $bulkSearchResultAry = array_merge($bulkSearchResultAry,$privateCustomerParcelSearchResultAry);
                        }
                        else if(!empty($privateCustomerParcelSearchResultAry))
                        {
                            $bulkSearchResultAry = $privateCustomerParcelSearchResultAry;
                        }
                        
                        
                        $szPartnerAPICalculationLogs .= "<br> <h3> Case: Searching for Parcel Shipment - Private Customer</h3> <br>";
                        $szPartnerAPICalculationLogs .= $kWHSSearch->szCourierCalculationLogString; 
                    }
                }
            }
            
            if($this->iSearchStandardCargo==1)
            { 
                
                /*
                * Calculating services for shipment type: STANDARD_CARGO
                */

                $postSearchAry['fCargoVolume'] = $this->fTotalVolumeStandardCargo;
                $postSearchAry['fCargoWeight'] = $this->fTotalWeightStandardCargo;
                $postSearchAry['iSearchMiniVersion'] = $this->iSearchMiniVesion;
                $postSearchAry['idLandingPage'] = $this->idDefaultLandingPage;
                if($this->iVogaSearchAutomatic)
                {
                   $standardSearchResultAry = $kWHSSearch->get_search_result($postSearchAry,false,'PARTNER_API_CALL',false,true);
                }
                
            }
           
            if($this->iSearchStandardCargo==1)
            {
                 $finalSearchResultList = $standardSearchResultAry;
            }
            else
          
            {
                $finalSearchResultList = $bulkSearchResultAry;
                if(!empty($finalSearchResultList) && !empty($parcelSearchResultAry))
                {
                    $finalSearchResultList = array_merge($finalSearchResultList,$parcelSearchResultAry);
                }
                else if(!empty($parcelSearchResultAry))
                {
                    $finalSearchResultList = $parcelSearchResultAry;
                }  

                if(!empty($finalSearchResultList) && !empty($palletSearchResultAry))
                {
                    $finalSearchResultList = array_merge($finalSearchResultList,$palletSearchResultAry);
                }
                else if(!empty($palletSearchResultAry))
                {
                    $finalSearchResultList = $palletSearchResultAry;
                } 
            }

            $searchResultAry = array();
            if(!empty($finalSearchResultList))
            {
                $ctr = 0;
                foreach($finalSearchResultList as $finalSearchResultLists)
                {
                    $dtCutOffDate=date("Y-m-d",strtotime(str_replace("/","-",trim($finalSearchResultLists['dtCutOffDate'])))); 
                    $dtAvailableDate=date("Y-m-d",strtotime(str_replace("/","-",trim($finalSearchResultLists['dtAvailableDate']))));

                    $days_between = ceil(abs(strtotime($dtAvailableDate) - strtotime($dtCutOffDate)) / 86400);

                    $iCuttOffDateTime = strtotime($dtCutOffDate);
                    $iAvailableDateTime = strtotime($dtAvailableDate);

                    $szFilterString = $finalSearchResultLists['idForwarder']."-".$finalSearchResultLists['iWarehouseType']."-".$finalSearchResultLists['szCustomerType']."-".$iCuttOffDateTime."-".$iAvailableDateTime."-".$days_between."-".round((float)$finalSearchResultLists['fDisplayPrice'],2); 
                    
                    //echo "<br><br> Filter: ".$szFilterString;
                    if(empty($filterServiceAry) || !in_array($szFilterString,$filterServiceAry))
                    {
                        $filterServiceAry[] = $szFilterString ;
                        $searchResultAry[$ctr] = $finalSearchResultLists;
                        $ctr++;
                    }
                }
            } 
            if(!empty($searchResultAry))
            {
                $idPartner = self::$idGlobalPartner;
                $szToken = self::$szGlobalToken;
                $iSearchResultType = 4;

                // adding resulting data into temperary table 
               $kWHSSearch->addTempSearchData($searchResultAry,false,$iSearchResultType,$idPartner,$szToken); 
            }  
            $postSearchAry['iPageNumber'] = 100;
            $searchResultAry = fetch_selected_services($searchResultAry,$kWHSSearch,$postSearchAry); 
            $szPartnerAPICalculationLogs .= $kWHSSearch->szLogString; 
            if(!empty($this->szDeveloperNotes))
            {
                $szPartnerAPICalculationLogs .= " <br><br><strong><u>Developer Notes:</u></strong> ".$this->szDeveloperNotes;
            }  
        } 
        $this->foundServicesArr=$searchResultAry;
        file_log_session(true, $strdata, $filename);
        self::$priceCalculationLogs = $szPartnerAPICalculationLogs;  
        return $searchResultAry;
    }
    
    /**
    * Fetching cargo details for a partner api call from the database.
    *
    * @access public
    * @param int $idBooking
    * @return array
    * @author Anil
    */

    public function getCargoDeailsByPartnerApi($idShippmentType=false,$szRequestType='price')
    {  
        if(!empty(self::$szRequestType))
        {
            $szRequestType = self::$szRequestType;
        }
        $idPartner = self::$idGlobalPartner;
        $szReferenceToken = self::$szGlobalToken;
        if(!empty($idShippmentType))
        {
            $iShipmentType = $idShippmentType;
        }
        else
        {
            $iShipmentType = self::$idGlobalShipmentType;
        }
        if($idPartner>0)
        {
            
            $kConfig = new cConfig();
            $configLangArr=$kConfig->getConfigurationLanguageData('__TABLE_WEIGHT_MEASURE__');

            $kConfig = new cConfig();
            $configLangCargoArr=$kConfig->getConfigurationLanguageData('__TABLE_CARGO_MEASURE__');
                
            $query="
               SELECT
                   c.id,
                   c.idPartner,
                   c.szReferenceToken,
                   c.fLength,
                   c.fWidth,
                   c.fHeight,
                   c.idCargoMeasure,
                   c.iQuantity,
                   c.fWeight,
                   c.idWeightMeasure,
                   c.fTotalWeight,
                   c.fTotalVolume,
                   c.iShipmentType,
                   c.szPalletType,
                   c.szCommodity,
                   c.szSellersReference
               FROM
                   ".__DBC_SCHEMATA_PARTNERS_API_CARGO_DETAIL__." AS c 
               WHERE
                   c.idPartner = '".(int)$idPartner."'	 
                AND
                    c.szReferenceToken = '".  mysql_escape_custom($szReferenceToken)."'
                AND
                    c.iShipmentType = '".(int)$iShipmentType."'
                AND
                    c.szRequestType = '".  mysql_escape_custom($szRequestType)."'
               ORDER BY
                   c.id ASC
           "; 
           //echo $query ;
           if($result = $this->exeSQL($query))
           {
                $cargoAry=array();
                if($this->iNumRows>0)
                {
                    $ctr=1;
                    while($row=$this->getAssoc($result))
                    {
                        if(!empty($configLangArr[$row['idWeightMeasure']]))
                        {
                            $configLangArr[$row['idWeightMeasure']]['wmdes']=$configLangArr[$row['idWeightMeasure']]['szDescription'];
                            $cargoAry[$ctr] = array_merge($configLangArr[$row['idWeightMeasure']],$row);
                        }else
                        {
                            $cargoAry[$ctr] = $row;
                        }
                        if(!empty($configLangCargoArr[$row['idCargoMeasure']]))
                        {
                            $configLangCargoArr[$row['idCargoMeasure']]['cmdes']=$configLangCargoArr[$row['idCargoMeasure']]['szDescription'];
                            $cargoAry[$ctr] = array_merge($configLangCargoArr[$row['idCargoMeasure']],$cargoAry[$ctr]);
                        }
                        $ctr++;
                    }
                    return $cargoAry ;
                }
                else
                {
                     return array();
                }
           }
           else
           {
               $this->error = true;
               $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
               $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
               return false;
           }
       }
   }
    
    /*
    * Preaparing data array for service calculation calculations
    */
    private function prepareRequestParamsForSearch($cargoDetailsAry,$szMethod='price',$mode=false,$iQuickQuoteTryitout=false)
    {
        /*
        * Function For 
        * Shipper Fileds 
        */ 
        $data = array();
        $data['szMethod'] = $this->szRequestType; 
        if(!empty($mode))
        {
            $data['szSearchType'] = $mode;
            $data['iQuickQuote'] = 1;
            $data['iQuickQuoteTryitout'] = $iQuickQuoteTryitout;  
        }
        else
        {
            $data['szSearchType'] = "API"; 
        } 
        
        $data['customerType'] = $this->customerType;
        $data['szCustomerToken'] = $this->szCustomerToken; 
        $data['szQuoteOriginCountryStr'] = $this->szQuoteOriginCountryStr;
        $data['szQuoteDestinationCountryStr'] = $this->szQuoteDestinationCountryStr;
        $data['szCargoType'] = $this->szCargoType;
        $data['iCustomerType'] = $this->iCustomerType; 
        $data['iPrivateShipping'] = $this->iPrivateShipping; 
        $data['szPaymentType'] = $this->szPaymentType; 
        $data['szStripeToken'] = $this->szStripeToken; 
        $data['idTransaction'] = $this->idTransaction; 
        $data['szAmount'] = $this->szAmount;  
        $data['szShipperCompanyName'] = $this->szShipperCompanyName;
        $data['szShipperFirstName'] = $this->szShipperFirstName;
        $data['szShipperLastName'] = $this->szShipperLastName;
        $data['szShipperEmail'] = $this->szShipperEmail;
        $data['iShipperCounrtyDialCode'] = $this->iShipperCounrtyDialCode; //This is dail code value like: 91 for India, 45 for Denmark
        $data['idShipperDialCode'] = $this->idShipperDialCode; //This is primaery id of tblcountry
        $data['szShipperPhoneNumber'] = $this->iShipperPhoneNumber;
        $data['szShipperAddress'] = $this->szShipperAddress;
        $data['szOriginPostCode'] = $this->szShipperPostcode;
        $data['szOriginCity'] = $this->szShipperCity;
        $data['szOriginCountry'] = $this->szOriginCountryStr;
        $data['idOriginCountry'] = $this->idShipperCountry; 
        $data['fOriginLatitude'] = $this->fOriginLatitude;
        $data['fOriginLongitude'] = $this->fOriginLongitude;
        $data['iInsuranceIncluded'] = $this->iInsuranceIncluded;
        $data['szInsurancePrice'] = $this->szInsurancePrice;
        
        $data['iBookingLanguage'] = $this->iBookingLanguage;
        $data['szCargoType'] = $this->szCargoType;  
        $data['szQuoteID'] = $this->szQuoteID;  
        
        
        /*
        * IF iDonotKnowShipperPostcode == 1 then this means we have not received Shipper postcode in API request, We have calculated Shipper postcode by using latitute and longitute
        * IF iDonotKnowConsigneePostcode == 1 then this means we have not received Consignee postcode in API request, We have calculated Consignee postcode by using latitute and longitute
        * @Ajay
        */
        $data['iDonotKnowShipperPostcode'] = $this->iDonotKnowShipperPostcode; 
        $data['iDonotKnowConsigneePostcode'] = $this->iDonotKnowConsigneePostcode;   
        
        /*
        * Function For 
        * Consignee Fileds 
        */ 
        $data['szConsigneeCompanyName'] = $this->szConsigneeCompanyName;
        $data['szConsigneeFirstName'] = $this->szConsigneeFirstName;
        $data['szConsigneeLastName'] = $this->szConsigneeLastName;
        $data['szConsigneeEmail'] = $this->szConsigneeEmail;
        $data['iConsigneeCounrtyDialCode'] = $this->iConsigneeCounrtyDialCode;
        $data['idConsigneeDialCode'] = $this->idConsigneeDialCode;
        $data['szConsigneePhoneNumber'] = $this->iConsigneePhoneNumber;
        $data['szConsigneeAddress'] = $this->szConsigneeAddress; 
        
        $data['szDestinationCity'] = $this->szConsigneeCity;
        $data['szDestinationPostCode'] = $this->szConsigneePostcode;
        $data['szDestinationCountry'] = $this->szDestinationCountryStr;
        $data['idDestinationCountry'] = $this->idConsigneeCountry;
        $data['fDestinationLatitude'] = $this->fDestinationLatitude;
        $data['fDestinationLongitude'] = $this->fDestinationLongitude;
         
        $data['szPacketType'] = $this->szPacketType;
        if(empty($this->szServiceTerm))
        {
            $data['szServiceTerm'] = __SERVICE_SHORT_TERMS_EXW__;
        }
        else
        { 
            $data['szServiceTerm'] = $this->szServiceTerm;
        }
        
        $data['idServiceTerms'] = $this->idServiceTerms; 
        $data['szPartnerReference'] = $this->szPartnerReference; 
        
        $data['idCurrency'] = $this->idCustomerCurrency; 
        $data['idCustomerCurrency'] = $this->idCustomerCurrency; 
        $data['szCurrency'] = $this->szCustomerCurrency; 
        $data['fExchangeRate'] = $this->fCustomerExchangeRate;  
        
        $data['idGoodsInsuranceCurrency'] = $this->idGoodsInsuranceCurrency;
        $data['szGoodsInsuranceCurrency'] = $this->szGoodsInsuranceCurrency;
        $data['fGoodsInsuranceExchangeRate'] = $this->fGoodsInsuranceExchangeRate;
        $data['fCargoValue'] = $this->fCargoValue; 
         
        $data['idTimingType'] = 1; 
        $data['idServiceType'] = $this->idServiceType; 
        $data['iOriginCC'] = $this->iOriginCC; 
        $data['iDestinationCC'] = $this->iDestinationCC; 
        
        $data['dtTiming'] = $this->dtShippingDay."/".$this->dtShippingMonth."/".$this->dtShippingYear; 
        $data['dtShipping'] = $this->dtShippingYear."-".$this->dtShippingMonth."-".$this->dtShippingDay; 
        $data['dtTimingDate'] = $this->dtShippingYear."-".$this->dtShippingMonth."-".$this->dtShippingDay." 00:00:00"; 
        $data['fCargoVolume'] = $this->fGrandTotalCargoVolume; 
        $data['fCargoWeight'] = $this->fGrandTotalCargoWeight;  
        $data['iFromRequirementPage'] = 2; 
        
        $data['iSearchBreakBulk'] = $this->iSearchBreakBulk;
        $data['iSearchParcel'] = $this->iSearchParcel;
        $data['iSearchPallet'] = $this->iSearchPallet;
        $data['iUnknownShipmentType'] = $this->iUnknownShipmentType; 
        $data['szServiceLineCargoDescription'] = $this->szServiceLineCargoDescription;   
        /*
        * If we don't receive billing country in partner API call then we have  used country of partner for which API Key is belongs to.
        */ 
        if($this->iUsePartnersBillingDetails)
        {
            $data['idBillingCountry'] = $this->idBillingCountry;
            $data['iUsePartnersBillingDetails'] = $this->iUsePartnersBillingDetails;
        }
        else
        {
            /*
            * Function For 
            * Billing Details Fileds 
            */ 
            if($this->iShipperConsigneeType=='BILLING')
            {
                $data['szBillingCompanyName'] = $this->szBillingCompanyName;
                $data['szBillingFirstName'] = $this->szBillingFirstName;
                $data['szBillingLastName'] = $this->szBillingLastName;
                $data['iBillingCountryDialCode'] = $this->idBillingCountryDialCode;
                $data['iBillingPhoneNumber'] = $this->iBillingPhoneNumber;
                $data['szBillingEmail'] = $this->szBillingEmail;
                $data['szBillingAddress'] = $this->szBillingAddress;
                $data['szBillingPostcode'] = $this->szBillingPostcode;
                $data['szBillingCity'] = $this->szBillingCity;
                $data['idBillingCountry'] = $this->idBillingCountry;
            }
            else if($this->iShipperConsigneeType=='SHIPPER')
            {
                $data['szBillingCompanyName'] = $this->szShipperCompanyName;
                $data['szBillingFirstName'] = $this->szShipperFirstName;
                $data['szBillingLastName'] = $this->szShipperLastName;
                $data['szBillingEmail'] = $this->szShipperEmail;
                $data['iBillingCountryDialCode'] = $this->idShipperDialCode;
                $data['iBillingPhoneNumber'] = $this->iShipperPhoneNumber;
                $data['szBillingAddress'] = $this->szShipperAddress; 
                $data['szBillingCity'] = $this->szShipperCity;
                $data['szBillingPostcode'] = $this->szShipperPostcode;
                $data['idBillingCountry'] = $this->idShipperCountry;
            }
            else if($this->iShipperConsigneeType=='CONSIGNEE')
            {
                $data['szBillingCompanyName'] = $this->szConsigneeCompanyName;
                $data['szBillingFirstName'] = $this->szConsigneeFirstName;
                $data['szBillingLastName'] = $this->szConsigneeLastName;
                $data['szBillingEmail'] = $this->szConsigneeEmail;
                $data['iBillingCountryDialCode'] = $this->idConsigneeDialCode;
                $data['iBillingPhoneNumber'] = $this->iConsigneePhoneNumber;
                $data['szBillingAddress'] = $this->szConsigneeAddress; 
                $data['szBillingCity'] = $this->szConsigneeCity;
                $data['szBillingPostcode'] = $this->szConsigneePostcode;
                $data['idBillingCountry'] = $this->idConsigneeCountry;
            }
            $data['iShipperConsigneeType'] = $this->iShipperConsigneeType;
        } 
        
        if($this->iSearchParcel==1)
        {
            $data['iBookingQuoteShipmentType'] = 1;
        }
        else if($this->iSearchPallet == 1)
        {
            $data['iBookingQuoteShipmentType'] = 2;
        }
        else if($this->iSearchBreakBulk==1)
        {
            $data['iBookingQuoteShipmentType'] = 3;
        }
        else if($this->iUnknownShipmentType==1)
        {
            $data['iBookingQuoteShipmentType'] = 4;
        }
        else if($this->iSearchStandardCargo==1)
        {
            $data['iBookingQuoteShipmentType'] = 5;
        }
        
        $data['szGoodsInsuranceCurrency'] = $this->szGoodsInsuranceCurrency; 
        $data['idGoodsInsuranceCurrency'] = $this->idGoodsInsuranceCurrency; 
        $data['fGoodsInsuranceExchangeRate'] = $this->fGoodsInsuranceExchangeRate; 
        $data['iDTDTrades'] = $this->iDTDTrades; 
        $data['idPartner'] = self::$idGlobalPartner; 
        $data['szReferenceToken'] = self::$szGlobalToken;   
        $data['szServiceRefrence'] = $this->szServiceRefrence;   
        
        $data['iSearchStandardCargo']=$this->iSearchStandardCargo;
        $data['iVogaSearchAutomatic']=$this->iVogaSearchAutomatic;
        $data['idDefaultLandingPage'] = $this->idDefaultLandingPage; 
        $data['iSearchMiniVesion']=$this->iSearchMiniVesion; 
        $data['szInternalComments']=$this->szInternalComments;
        
        if($this->iSearchStandardCargo==1)
        {
            if(!empty($this->szCargoDescriptionForStandardCargo))
            {
                $data['szCargoDescription'] = $this->szCargoDescriptionForStandardCargo;
            }
            else
            {
                $data['szCargoDescription'] = $this->szCargoDescription;
            } 
        }
        else
        {
            $data['szCargoDescription'] = $this->szCargoDescription;
        }
        $data['idLandingPage'] = $this->idDefaultLandingPage;
        $data['iShipperDetailsLocked'] = $this->iShipperDetailsLocked; 
        $data['iConsigneeDetailsLocked']=$this->iConsigneeDetailsLocked; 
        $data['iCargoDescriptionLocked']=$this->iCargoDescriptionLocked;
        $data['iCreateAutomaticQuote']=$this->iCreateAutomaticQuote;
        $data['szAutomatedRfqResponseCode']=$this->szAutomatedRfqResponseCode;
        $data['iRemovePriceGuarantee']=$this->iRemovePriceGuarantee;
        $data['iPaymentTypePartner'] = $this->iPaymentTypePartner;
        
        $this->savePartnerApiData($data,$cargoDetailsAry);  
        return $data;
    }
    
    public function createBooking($data,$szMethod=false,$mode=false)
    {
        $filename = __APP_PATH_ROOT__."/logs/booking_partner_api_".date('Y-m-d').".log";
        $strdata=array();
        $strdata[0]=" \n\n ***Start Booking Request Data ".date('d-M-Y H:i:s')." *** \n";
        $strdata[1]=print_r($data,true);
        $strdata[2]=" \n\n ***End Booking Request Data ".date('d-M-Y H:i:s')." *** \n";
        file_log_session(true, $strdata, $filename);

        if(!empty($data))
        {
            $kWHSSearch=new cWHSSearch();   
            $kConfig = new cConfig(); 
            $kBooking = new cBooking(); 
            $result_Ary = array();   
            if(empty($data['szCurrency']))
            {
                $data['szCurrency'] = 'USD';
            }
            self::$szDeveloperNotes .= ">. Initiated create booking task by API ".PHP_EOL; 
            $this->validateParams($data,$szMethod,$mode);

            $strdata=array();
            $strdata[0]=" \n\n ####After Validation Booking Request Data ".date('d-M-Y H:i:s')."####* \n";
            $strdata[1]=print_r($this,true);
            $strdata[2]=" \n\n #### End Validation Booking Request Data ".date('d-M-Y H:i:s')." #### \n";
            file_log_session(true, $strdata, $filename);
           
            if($this->error==true)
            {         
                self::$szDeveloperNotes .= ">. Create booking validation failed because of this error:\n\n ".print_R($this->arErrorMessages,true)." ".PHP_EOL; 
                return self::buildErrorMessage(); 
            }  
            else
            {
                self::$szDeveloperNotes .= ">. Create booking validation OK. Going forward ".PHP_EOL;
            } 
            $postSearchAry = $this->prepareRequestParamsForSearch($data['cargo'],'booking');  
            self::$GlobalpostSearchAry = $postSearchAry;
            $idPartner = self::$idGlobalPartner;  
            $iInsuranceIncluded = $postSearchAry['iInsuranceIncluded']; 
            $szInsurancePrice = $postSearchAry['szInsurancePrice'];  
            
            $szPaymentType = $postSearchAry['szPaymentType'];
            $szStripeToken = $postSearchAry['szStripeToken'];
            $idTransaction = $postSearchAry['idTransaction'];
            $szAmount = $postSearchAry['szAmount'];
            $iPaymentTypePartner = $postSearchAry['iPaymentTypePartner'];  
            $errorFlag = false;
            $bRecalculateFlag = false;
            $szServiceID = trim($data['szServiceID']); 
            $szQuotePriceKey = trim($data['szQuoteID']);
            $szRequestType = "price";
            $dtCurrentDateTime = $kBooking->getRealNow(); 
            $dtMinus24HoursDateTime = date('Y-m-d H:i:s',strtotime("-24 HOUR",strtotime($dtCurrentDateTime)));
             
            $predefinedServiceIdAry = $this->predefinedServiceIDForQuote;
            
            $bCreateBookingQuote = false;
            $bCreateConfirmedBooking = false;
            
            $bConfirmedAutomaticBooking = false;
            $bConfirmedManualBooking = false;
             
            if(in_array($szServiceID, $predefinedServiceIdAry))
            {
                $bCreateBookingQuote = true; 
            } 
            else
            {
                $bCreateConfirmedBooking = true;  
                if(!empty($szQuotePriceKey))
                {
                    $bConfirmedManualBooking = true;
                }
                else
                {
                    $bConfirmedAutomaticBooking = true;
                }
            } 
            
            if($bConfirmedManualBooking)
            {
                $kQuote = new cQuote();
                $bookingQuoteAry = array();
                $bookingQuoteAry = $kQuote->getQuotePricingIDbyKey($szQuotePriceKey);
                $idBookingQuotePricing = $bookingQuoteAry['id']; 
                $idBooking = $bookingQuoteAry['idBooking'];
                if($idBooking<=0)
                {
                    self::$szDeveloperNotes .= ">. No booking is associated with szQuoteID ".PHP_EOL;
                    $errorFlag = true;
                    $this->buildErrorDetailAry('szServiceTokenAlreadyUsed','NOSERVICE');  
                }
                
                $strdata=array();
                $strdata[0]=" \n\n Manual  Booking Quote Id ".$szQuotePriceKey." \n";
                file_log_session(true, $strdata, $filename);
                
                $bookingDataAry = array();
                $bookingDataAry = $kBooking->getBookingDetails($idBooking); 
                $idBookingFile = $bookingDataAry['idFile'];
                $iQuickQuote = $bookingDataAry['iQuickQuote'];
                
                if($bookingDataAry['idBookingStatus']=='3' || $bookingDataAry['idBookingStatus']=='4' || $bookingDataAry['idBookingStatus']=='7')
                {
                    self::$szDeveloperNotes .= ">. Booking is already placed by using same token ".PHP_EOL;
                    $errorFlag = true;
                    $this->buildErrorDetailAry('szServiceTokenAlreadyUsed','NOSERVICE');  
                }
            }
            else
            {
                /*
                * Checking if the booking is already placed for the token. This check is only applicable in request mode: LIVE
                */
                if($this->isBookingAlreadyPlacedForToken())
                {
                    self::$szDeveloperNotes .= ">. Booking is already placed by using same token ".PHP_EOL;
                    $errorFlag = true;
                    $this->buildErrorDetailAry('szServiceTokenAlreadyUsed','NOSERVICE');  
                } 
                
                $strdata=array();
                $strdata[0]=" \n\nBooking Automatic Booking \n";
                file_log_session(true, $strdata, $filename);
            }
            
            
            
            if($bCreateConfirmedBooking && $bConfirmedAutomaticBooking)
            {  
                /*
                * We only performs following actions for the booking in case when we are getting 'Booking' API call for 
                */
                
                $serviceResponseAry = array();
                $serviceResponseAry = $this->getServiceResponseDetails($idPartner,$szReferenceToken,$szServiceID); 
                if(empty($serviceResponseAry))
                {
                    self::$szDeveloperNotes .= ">. There is no service response matching with requested paramenters ".PHP_EOL;
                    $errorFlag = true;
                    $this->buildErrorDetailAry('szServiceToken','INVALID');   
                } 
                
                 $strdata=array();
                $strdata[0]=" \n\nBooking Automatic Booking After Service Found \n";
                file_log_session(true, $strdata, $filename);
                
                if(!$errorFlag)
                {
                    self::$szDeveloperNotes .= ">. We found service response matching with requested paramenters. Going forward ".PHP_EOL;
                    $idShipmentType = $serviceResponseAry['iShipmentType'];
                    $iBookingType = $serviceResponseAry['iBookingType'];
                    $idPackingType = $serviceResponseAry['idPackingType'];
                    $idPartnerApiResponse = $serviceResponseAry['id'];
                    $fTotalInsuranceCostForBookingCustomerCurrency = $serviceResponseAry['fTotalInsuranceCostForBookingCustomerCurrency'];

                    $dtResponseSentDateTime = strtotime($serviceResponseAry['dtCreatedOn']); 
                    $dtMinus24HoursTime = strtotime($dtMinus24HoursDateTime);
                    self::$szDeveloperNotes .= ">. Price response sent on: ".$serviceResponseAry['dtCreatedOn']." Booking Request received on: ".$dtMinus24HoursDateTime."".PHP_EOL; 
                    if($dtMinus24HoursTime>=$dtResponseSentDateTime)
                    {
                        /*
                         * If the call is more than 24 hours old then we recalculate pricing
                         */
                        $bRecalculateFlag = true;
                        self::$szDeveloperNotes .= ">. Requested service is more then 24 hours old so we need to recalculate booking prices again ".PHP_EOL; 
                    
                        $strdata=array();
                        $strdata[0]=" \n\Recalculating Automatic Booking \n";
                        file_log_session(true, $strdata, $filename);
                    }  
                } 
                
                
                $cargoDetailsAry = array();
                $cargoDetailsAry = $this->updateBookingCargDetails($idShipmentType);
            
                if(!$errorFlag)
                {
                    /*
                    * Checking if partner has made validatePrice request call for this szServiceID or not
                    */
                    $validateRequestParamAry = array();
                    $validateRequestParamAry = $this->getQuickQuoteRequestData('validatePrice',$szServiceID);
                     
                    if(!empty($validateRequestParamAry))
                    {
                        /*
                        * As partner has already had made 'validatePrie' request for ths service ID so this is why we are comparing parameters with the request parameters what is passed in 'validatePrice' API call
                        */
                        $szRequestType = "validatePrice";
                    }
                    $postSearchAry['iShipmentType'] = $idShipmentType;
                    $changedFlag = $this->isParamsChangedOnBookingCall($postSearchAry,$szRequestType);                
                    if($changedFlag!='SUCCESS')
                    {
                        $errorFlag = true;
                        self::$szDeveloperNotes .= ">. Request parameters has been changed in the API call compared with the ".$szRequestType." API call we have received earlier ".PHP_EOL;
                        $this->buildErrorDetailAry('szBookingDataChange','INVALID');   
                        
                        $strdata=array();
                        $strdata[0]=" \n\Booking Data Change For Automatic Booking \n";
                        file_log_session(true, $strdata, $filename);
                    }
                    else
                    {
                        self::$szDeveloperNotes .= ">. We found service response matching with requested paramenters. Going forward ".PHP_EOL;
                        //18-May-2017 Not Comparing Cargo Details in Booking Call
                        if($this->compareCargoDetail($szRequestType))
                        {
                            self::$szDeveloperNotes .= ">. Cargo dimension is same as we received on ".$szRequestType." request. Going forward ".PHP_EOL;
                        }
                        else
                        {
                            $errorFlag = true;
                            self::$szDeveloperNotes .= ">. Cargo dimension has been changed in the API call compared with the ".$szRequestType." API call we have received earlier ".PHP_EOL;
                            $this->buildErrorDetailAry('szBookingDataChange','INVALID');   
                        }
                    }
                }  
            }  
            else if($bCreateBookingQuote)
            {
                
                $strdata=array();
                $strdata[0]=" \n\Creating Manual Booking Booking".$szQuotePriceKey." \n";
                file_log_session(true, $strdata, $filename);
                        
                /*
                *  getting total of shipment value for the booking
                */
                $idShipmentType = $postSearchAry['iBookingQuoteShipmentType']; 
                $cargoDetailsAry = array();
                $cargoDetailsAry = $this->updateBookingCargDetails($idShipmentType);
                
                /*
                * If there is multiple shipment types added, then we display other shipment in Internal comment section of Validate pane.
                * Building Internal comments by cargo values like ollows
                   Pallet: 2 Europallets (80x120x180cm), total 500kg 
                */
                $szInterCommentsCargoStr = '';
                if($postSearchAry['iSearchParcel']==1 && $idShipmentType!=1)
                {
                    $iBookingQuoteShipmentTypeIC = 1; //Parcel Shipment 
                    $parcelDetailsAry = array();
                    $parcelDetailsAry = $this->updateBookingCargDetails($iBookingQuoteShipmentTypeIC);
                    if(!empty($parcelDetailsAry['szCargoTotalLine']))
                    {
                        $szInterCommentsCargoStr = $parcelDetailsAry['szCargoTotalLine'];
                    }
                } 
                
                if($postSearchAry['iSearchPallet']==1 && $idShipmentType!=2)
                {
                    $iBookingQuoteShipmentTypeIC = 2; //Pallet Shipment
                    $parcelDetailsAry = array();
                    $parcelDetailsAry = $this->updateBookingCargDetails($iBookingQuoteShipmentTypeIC);
                    if(!empty($parcelDetailsAry['szCargoTotalLine']))
                    {
                        $szInterCommentsCargoStr .= $parcelDetailsAry['szCargoTotalLine'];
                    }
                } 
                
                if($postSearchAry['iSearchBreakBulk']==1 && $idShipmentType!=3)
                {
                    $iBookingQuoteShipmentTypeIC = 3; //Pallet Shipment
                    $parcelDetailsAry = array();
                    $parcelDetailsAry = $this->updateBookingCargDetails($iBookingQuoteShipmentTypeIC);
                    if(!empty($parcelDetailsAry['szCargoTotalLine']))
                    {
                        $szInterCommentsCargoStr .= $parcelDetailsAry['szCargoTotalLine'];
                    }
                }
                if(!empty($szInterCommentsCargoStr))
                {
                    $szInterCommentsCargoStr = "Could also be shipped as: ".PHP_EOL."".$szInterCommentsCargoStr;
                }
            } 
            if($errorFlag)
            {
                $strdata=array();
                $strdata[0]=" \n\n ####After Validation Booking Request Data ".date('d-M-Y H:i:s')."####* \n";
                $strdata[1]=print_r($this,true);
                $strdata[2]=print_r($this->error_detail_ary,true);
                $strdata[3]=" \n\n #### End Validation Booking Request Data ".date('d-M-Y H:i:s')." #### \n";
                file_log_session(true, $strdata, $filename);
                self::$szDeveloperNotes .= ">. Create booking function returning with error message. Stopped ".PHP_EOL;
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage();
            }    
            if($bCreateConfirmedBooking)
            {
                if($this->iUsePartnersBillingDetails)
                {
                    /*
                    * IF Partner is creating booking then Customer Role should always be 'Billing'
                    */
                    $parnerDetailsAry = $this->getParnerAccountDetails($idPartner); 
                    $idCustomer = $parnerDetailsAry['idCustomer'];
                    $postSearchAry['iShipperConsignee'] = 3;
                }
                else
                {
                    $idCustomer = $this->createCustomerByAPI($postSearchAry,$postSearchAry['iShipperConsigneeType']);

                    if($postSearchAry['iShipperConsigneeType']=='BILLING')  
                    {
                        $postSearchAry['iShipperConsignee'] = 3;
                    }
                    else if($postSearchAry['iShipperConsigneeType']=='SHIPPER')  
                    {
                        $postSearchAry['iShipperConsignee'] = 1;                
                    }
                    else if($postSearchAry['iShipperConsigneeType']=='CONSIGNEE')  
                    {
                        $postSearchAry['iShipperConsignee'] = 2;
                    }
                } 
            }
            else
            {
                $strdata=array();
                $strdata[0]=" \n\Creating user for Manual Booking".$szQuotePriceKey." \n";
                file_log_session(true, $strdata, $filename);
                
                $parnerDetailsAry = $this->getParnerAccountDetails($idPartner); 
                $kUserPartner = new cUser();
                $kUserPartner->getUserDetails($parnerDetailsAry['idCustomer']); 
                $szPartnerCompanyName = $kUserPartner->szCompanyName; 
               
                if($szServiceID==__API_SERVICE_ID_SHIPPER__)
                {
                    
                    /*
                    * Creating Customer on the basis of Shipper Details
                    */
                    $idCustomer = $this->createCustomerByAPI($postSearchAry,'SHIPPER',true);
                    /*
                    * Customer Role should always be 'Shipper' in this case
                    */
                    $postSearchAry['iShipperConsignee'] = 1; 
                }
                else if($szServiceID==__API_SERVICE_ID_CONSIGNEE__)
                {
                    /*
                    * Creating Customer on the basis of Consignee Details
                    */
                    $idCustomer = $this->createCustomerByAPI($postSearchAry,'CONSIGNEE',true);
                    /*
                    * Customer Role should always be 'Consignee' in this case
                    */
                    $postSearchAry['iShipperConsignee'] = 2; 
                }
                else if($szServiceID==__API_SERVICE_ID_BILLING_CUSTOMER__)
                {
                    /*
                    * Creating Customer on the basis of Billing Details
                    */
                    if($this->iUsePartnersBillingDetails)
                    {
                        $idCustomer = $parnerDetailsAry['idCustomer'];
                    }else{
                    $idCustomer = $this->createCustomerByAPI($postSearchAry,'BILLING',true);
                    }
                    /*
                    * Customer Role should always be 'Billing' in this case
                    */
                    $postSearchAry['iShipperConsignee'] = 3; 
                }
                else
                { 
                    $idCustomer = $parnerDetailsAry['idCustomer'];
                    /*
                    * IF Partner is creating booking then Customer Role should always be 'Billing'
                    */
                    $postSearchAry['iShipperConsignee'] = 3; 
                }
                if($szServiceID!=__API_SERVICE_ID_BILLING_CUSTOMER__)
                {
                    $szInternalComment = "API RFQ from ".$szPartnerCompanyName; 
                    if(!empty($szInterCommentsCargoStr))
                    {
                        $szInternalComment = $szInternalComment."".PHP_EOL."".PHP_EOL.$szInterCommentsCargoStr;
                    }                
                }
                else
                {
                    if(!empty($szInterCommentsCargoStr))
                    {
                        $szInternalComment = $szInterCommentsCargoStr;
                    }
                }
                
                if(!empty($postSearchAry['szPartnerReference']))
                {
                    $szInternalComment = $szInternalComment ."".PHP_EOL."Reference: ". $postSearchAry['szPartnerReference'];
                } 
                
                $strdata=array();
                $strdata[0]=" \n\After creating user for Manual Booking".$szQuotePriceKey." \n";
                file_log_session(true, $strdata, $filename);
            } 
            
            $kUser = new cUser();
            $kUser->getUserDetails($idCustomer); 
            
            if($kUser->id<=0)
            {
                $errorFlag = true;
                self::$szDeveloperNotes .= ">. Create booking function returning with error message because there is no customer ID associated with tblpartner ".PHP_EOL;
                $this->buildErrorDetailAry('szCommunication','INVALID');    
            }
            self::$idPartnerCustomer = $idCustomer;
            $szReferenceToken = self::$szGlobalToken; 
            
            if(!$bConfirmedManualBooking)
            {
                $kServices = new cServices();
                $bookingDataAry = array();
                $bookingDataAry = $kServices->createBookingByToken($szReferenceToken,$postSearchAry,$idCustomer,$bCreateBookingQuote);

                $iAddedNewBooking = $bookingDataAry['iAddedNewBooking']; 
                if($bookingDataAry['szStatus']=='ERROR')
                {
                    if($bookingDataAry['bServiceTokenAlreadyUsed'])
                    {
                        $this->buildErrorDetailAry('szServiceTokenAlreadyUsed','NOSERVICE');  
                    }
                    else
                    {
                        $this->buildErrorDetailAry('szCommunication','INVALID');   
                    }
                    self::$szDeveloperNotes .= ">. Create booking function returning with error message because we could not able to create Booking file in tblbookingfiles ".PHP_EOL;
                }
                else if($bookingDataAry['idBooking']>0)
                {
                    $idBooking = $bookingDataAry['idBooking'];
                    $idBookingFile = $bookingDataAry['idBookingFile']; 
                }
                else
                {
                    $errorFlag = true;
                    self::$szDeveloperNotes .= ">. Create booking function returning with error message because we could not able to create Booking file in tblbookingfiles ".PHP_EOL;
                    $this->buildErrorDetailAry('szCommunication','INVALID');   
                }
            } 
            if($bCreateConfirmedBooking)
            {
                if($bConfirmedAutomaticBooking)
                {
                    /*
                    * We only updates service details in case of Automatic bookings
                    */
                    if($iBookingType==2)
                    {
                        /*
                        * Update data for courier bookings
                        */
                        $bookingUpdatedFlag = $kBooking->updateCourierServiceForwarderDetails($idBooking,$szServiceID,true);
                    }
                    else
                    {
                        /*
                        * Update data for LCL bookings
                        */ 
                        $bookingUpdatedFlag = $kBooking->updateForwarderDetails($idBooking,$szServiceID,true); 
                    }  
                    if($kBooking->iBookingDataUpdated!=1)
                    {
                        $errorFlag = true;
                        self::$szDeveloperNotes .= ">. Create booking function returning with error message because we could not able to update bookings data to tblbookings ".PHP_EOL;
                        $this->buildErrorDetailAry('szCommunication','INVALID');  
                    }
                    else
                    {
                        self::$szDeveloperNotes .= ">. Service data updated successfully. Going forward ".PHP_EOL;
                    }  
                    //$bRecalculateFlag = true;
                    if($bRecalculateFlag && !$errorFlag)
                    {
                        /*
                        * Recalculate transporteca pricing
                        */ 
                        $szResponseStr = $this->recalculateCargoBookingData($postSearchAry,$serviceResponseAry,$idBooking);
                        if($szResponseStr=='SUCCESS')
                        {
                            self::$szDeveloperNotes .= ">. Booking Price recalculated successfully. Going forward ".PHP_EOL;
                        }
                        else if($szResponseStr=='WARNING')
                        {
                            self::$szDeveloperNotes .= ">. Booking Price recalculated successfully, but transportation price expired because its been changed Going to show service list with a notification 'Transportation price expired'  ".PHP_EOL;
                            self::$szNotification = "E1099";
                            $result_Ary = $this->processServiceApiRequest($postSearchAry);
                            return $result_Ary;
                        }
                        else
                        {
                            $errorFlag = true;
                            self::$szDeveloperNotes .= ">. We have had some issues in price re-calculation ".PHP_EOL;
                            $this->buildErrorDetailAry('szCommunication','INVALID');  
                        }
                    } 
                } 
                
                $bookingPartialDataAry = $kBooking->getBookingDetails($idBooking);
               
                if($iInsuranceIncluded==1)
                {
                    /*
                    * Update Insurance data to the booking
                    */  
                    $postSearchAry['fTotalPriceUSD'] = $bookingPartialDataAry['fTotalPriceUSD'];
                    $postSearchAry['idTransportMode'] = $bookingPartialDataAry['idTransportMode'];
                    $postSearchAry['id'] = $idBooking;
                    $postSearchAry['isMoving'] = 0;
                    $postSearchAry['szCargoType'] = $this->szCargoType; 

                    $insuranceInputAry = array();
                    $insurancePriceAry = array();
                    $insuranceInputAry['iInsurance'] = 1;
                    $insuranceInputAry['idInsuranceCurrency'] = $postSearchAry['idGoodsInsuranceCurrency'];
                    $insuranceInputAry['fValueOfGoods'] = $postSearchAry['fCargoValue'];
 
                    $insuranceBookingDataAry = array();
                    $insuranceBookingDataAry = $postSearchAry;
                    $insuranceBookingDataAry['fTotalPriceCustomerCurrency'] = $bookingPartialDataAry['fTotalPriceCustomerCurrency'];
                    $insuranceBookingDataAry['fTotalVat'] = $bookingPartialDataAry['fTotalVat'];
                    $insuranceBookingDataAry['iPrivateShipping'] = $bookingPartialDataAry['iPrivateShipping']; 
                    
                    $insuranceBookingDataAry['bUpdatePriceByAPI'] = 1;
                    $insuranceBookingDataAry['fInsuranceSellPriceAPI'] = $szInsurancePrice;
                        
                    if($kBooking->updateInsuranceDetails($insuranceInputAry,$insuranceBookingDataAry))
                    {  
                        /*
                        if($this->comparePricingChanges($fTotalInsuranceCostForBookingCustomerCurrency,$kBooking->fTotalInsuranceCostForBookingCustomerCurrencyApi))
                        {
                            self::$szDeveloperNotes .= ">. Insurance data successfully updated. Going forward ".PHP_EOL; 
                        }
                        else
                        {
                            self::$szDeveloperNotes .= ">. Insurance Price recalculated successfully with following data: ".print_R($kBooking->arrInsuranceUpdateAry,true)." and old price was: ".$fTotalInsuranceCostForBookingCustomerCurrency." but Insurance price expired because its been changed Going to show service list with a notification 'Transportation price expired'  ".PHP_EOL;
                            self::$szNotification = "E1099";
                            $result_Ary = $this->processServiceApiRequest($postSearchAry);
                            return $result_Ary;
                        }
                        * 
                        */
                    }
                    else if($kBooking->iInsuranceValidationFailed==1)
                    {
                        $errorFlag = true;
                        self::$szDeveloperNotes .= ">. Inusrance data is not successfully updated into db ".PHP_EOL;
                        $this->buildErrorDetailAry('szCommunication','INVALID');    
                    }
                    else if($kBooking->iInsuranceOtherErrors==1)
                    {
                        $errorFlag = true;
                        self::$szDeveloperNotes .= ">. Inusrance data is not successfully updated into db because of this error:\n\n ".print_R($kBooking->arErrorMessages,true)." ".PHP_EOL;
                        $this->buildErrorDetailAry('szInsuranceTradeNotAvaialable','NOSERVICE');    
                    }
                    
                    
                }
                if($bookingPartialDataAry['idForwarder']>0)
                {
                    $idForwarder = $bookingPartialDataAry['idForwarder'];
                }
                else
                {
                    $idForwarder = $kBooking->idBookingForwarder;
                }
                
                
                //For API booking we default set 'Tracking Updates' to 'No'
                $iSendTrackingUpdates = 0;
            } 
            else
            {
                /*
                *  When we create from API, We only check for Insurance availability
                *  1. If Insurance availabel for the selcted trade, we update Insurance as 'Optional' on Validate Pane
                *  2. If Insurance availabel for the selcted trade, we update Insurance as 'Not Available' on Validate Pane
                */
                $idTransportMode = __BOOKING_TRANSPORT_MODE_SEA__;
                $insuranceInputAry = array();
                $insuranceInputAry['idOriginCountry'] = $postSearchAry['idOriginCountry'];
                $insuranceInputAry['idDestinationCountry'] = $postSearchAry['idDestinationCountry'];
                $insuranceInputAry['idTransportMode'] = $idTransportMode;
                $insuranceInputAry['isMoving'] = false;
                $insuranceInputAry['szCargoType'] = $postSearchAry['szCargoType']; 
                
                $iInsuranceChoice = 2; //Not Available
                $kInsurance = new cInsurance();
                if($kInsurance->getInsuranceDetailsFroBooking($insuranceInputAry))
                {
                    $iInsuranceChoice = 3; //Optional
                } 
                //For API Quotes we default set 'Tracking Updates' to 'Yes'
                $iSendTrackingUpdates = 1;
            } 
            
            /*
            * Creating shipper consignee details into the system
            */ 
            $idShipperConsignee = $this->createShipperConsignee($postSearchAry,$idBooking); 
            if($idShipperConsignee<=0)
            {
                $errorFlag = true;
                self::$szDeveloperNotes .= ">. Create booking function returning with error message because we could not able to create shipper consignee details ".PHP_EOL;
                $this->buildErrorDetailAry('szCommunication','INVALID');    
            }  
            else
            {
                self::$szDeveloperNotes .= ">. Shipper consignee row created successfully. ID: ".$idShipperConsignee.", Going forward ".PHP_EOL;
            }
            if($errorFlag)
            {
                $strdata=array();
                $strdata[0]=" \n\After creating user for Manual Booking".$idBooking." \n";
                $strdata[1]=print_r($this->error_detail_ary,true);
                file_log_session(true, $strdata, $filename);
                
                self::$szDeveloperNotes .= ">. Create booking function returning with error message ".PHP_EOL;
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage();
            }  
            
            if($bCreateConfirmedBooking)
            {
                $kForwarder=new cForwarder();
                $kForwarder->load($idForwarder);
                $szCompanyRegistrationNum = $kForwarder->szCompanyRegistrationNum;
 
                $szReferenceType = 'BOOKING';
                if(trim(self::$szApiRequestMode) == __API_REQUEST_MODE_TEST__)
                {
                    $szReferenceType = "TEST_API"; 
                    $szTransportecInvoice = "T00008".mt_rand(0,999); 
                    $invoiceNumber = "0001".mt_rand(9999,99999); 
                    $szBookingRef = "".date('y')."".date('m')."TTR".mt_rand(999,9999);
                }
                else
                { 
                    $bQuickQuoteBooking = false;
                    $iCheckQuickQuoteBooking=false;
                    if($bookingPartialDataAry['iQuickQuote']==__QUICK_QUOTE_SEND_QUOTE__)
                    {
                        $iCheckQuickQuoteBooking=true;
                    }
                    if($bookingPartialDataAry['iQuickQuote']==__QUICK_QUOTE_MAKE_BOOKING__)
                    {
                        /*
                        * For Quick quotes we already have szBookingRef ad Invoice number for the booking while creating quick quotes.
                        */
                        $szBookingRef = $bookingPartialDataAry['szBookingRef'];
                        $szTransportecInvoice = $bookingPartialDataAry['szInvoice'];
                        $szInsuranceInvoice = $bookingPartialDataAry['szInsuranceInvoice'];
                        $bQuickQuoteBooking  = true;
                        $iCheckQuickQuoteBooking=true;
                    }
                    else
                    {
                        $max_number_used_for_forwarder = $kForwarder->getMaximumUsedNumberByForwarder($idForwarder,false,$szReferenceType);
                        $szBookingRef = $kBooking->generateBookingRef($max_number_used_for_forwarder,$kForwarder->szForwarderAlies);

                        $bookingRefLogAry = array();
                        $bookingRefLogAry['idForwarder'] = $idForwarder;
                        $bookingRefLogAry['szBookingRef'] = $szBookingRef;	
                        $bookingRefLogAry['szReferenceType'] = $szReferenceType;	
                        $kForwarder->updateMaxNumBookingRef($bookingRefLogAry);

                        //$szTransportecInvoice = $kBooking->generateInvoice($idForwarder,true);  
                        $kAdmin = new cAdmin();
                        $szTransportecInvoice = $kAdmin->generateInvoiceForPayment(true);
                        $invoiceNumber = '';
                        if($iInsuranceIncluded==1)
                        {  
                            $invoiceNumber = $szTransportecInvoice;
                        } 
                    } 
                } 
                $iPalletType = 0;
                if($idShipmentType==2)
                {
                    $iPalletType = 1;
                }
            }
            
            $dtBookingConfirmed = $kBooking->getRealNow();  
            $iNumDaysPayBeforePickup = $kWHSSearch->getManageMentVariableByDescription('__NUMBER_OF_WEEKDAYS_PAYMENT_REQUIRED_BEFORE_REQUESTED_PICKUP__'); 
            
            $kWarehouseSearch = new cWHSSearch();
            $resultAry = $kWarehouseSearch->getCurrencyDetails(20);	 
            $fDkkExchangeRate = 0;
            if($resultAry['fUsdValue']>0)
            {
                $fDkkExchangeRate = $resultAry['fUsdValue'] ;						 
            }  
            
            /*
            * Updating payment details to booking.
            * Note: Booking created by Partner API is always a Bank Trasfer booking
            */ 
            $bookingAry = array(); 
            $bookingAry['idServiceType'] = $postSearchAry['idServiceType']; 
            $bookingAry['iOriginCC'] = $postSearchAry['iOriginCC'];
            $bookingAry['iDestinationCC'] = $postSearchAry['iDestinationCC'];
            $bookingAry['idTimingType'] = $postSearchAry['idTimingType'];
            $bookingAry['dtTimingDate'] = $postSearchAry['dtTimingDate']; 
            $bookingAry['idOriginCountry'] = $postSearchAry['idOriginCountry'];
            $bookingAry['szOriginCountry'] = $postSearchAry['szOriginCountry'];
            $bookingAry['szOriginPostCode'] = $postSearchAry['szOriginPostCode'];
            $bookingAry['szOriginCity'] = $postSearchAry['szOriginCity'];
            $bookingAry['fOriginLatitude'] = $postSearchAry['fOriginLatitude'];
            $bookingAry['fOriginLongitude'] = $postSearchAry['fOriginLongitude'];  
            $bookingAry['idDestinationCountry'] = $postSearchAry['idDestinationCountry'];
            $bookingAry['szDestinationCountry'] = $postSearchAry['szDestinationCountry'];
            $bookingAry['szDestinationPostCode'] = $postSearchAry['szDestinationPostCode'];
            $bookingAry['szDestinationCity'] = $postSearchAry['szDestinationCity']; 
            $bookingAry['fDestinationLongitude'] = $postSearchAry['fDestinationLongitude'];
            $bookingAry['fDestinationLatitude'] = $postSearchAry['fDestinationLatitude']; 
            $bookingAry['szPartnerReference'] = $postSearchAry['szPartnerReference'];
            $bookingAry['idServiceTerms'] = $postSearchAry['idServiceTerms'];  
            $bookingAry['idUser'] = $kUser->id ;				
            $bookingAry['szFirstName'] = $kUser->szFirstName ;
            $bookingAry['szLastName'] = $kUser->szLastName ;
            $bookingAry['szEmail'] = $kUser->szEmail ;
            $bookingAry['szCustomerAddress1'] = $kUser->szAddress1;
            $bookingAry['szCustomerAddress2'] = $kUser->szAddress2;
            $bookingAry['szCustomerAddress3'] = $kUser->szAddress3;
            $bookingAry['szCustomerPostCode'] = $kUser->szPostcode;
            $bookingAry['szCustomerCity'] = $kUser->szCity;
            $bookingAry['szCustomerCountry'] = $kUser->szCountry;
            $bookingAry['szCustomerState'] = $kUser->szState;
            $bookingAry['szCustomerPhoneNumber'] = $kUser->szPhoneNumber;
            $bookingAry['szCustomerCompanyName'] = $kUser->szCompanyName;	
            $bookingAry['szCustomerCompanyRegNo'] = $kUser->szCompanyRegNo;
            $bookingAry['idCustomerDialCode'] = $kUser->idInternationalDialCode;
            $bookingAry['idShipperConsignee'] = $idShipperConsignee; 
            $bookingAry['iBookingLanguage'] = $this->iBookingLanguage;
            $bookingAry['iBookingStep'] = 13;
            $bookingAry['dtBookingInitiated'] = $dtCurrentDateTime;
            $bookingAry['iFromRequirementPage'] = 2;
            $bookingAry['idCustomerCurrency'] = $postSearchAry['idCurrency'];
            $bookingAry['szCustomerCurrency'] = $postSearchAry['szCurrency'];
            $bookingAry['fCustomerExchangeRate'] = $postSearchAry['fExchangeRate'];
            $bookingAry['iShipperConsignee'] = $postSearchAry['iShipperConsignee'];  
            $bookingAry['idFile'] = $idBookingFile; 
            $bookingAry['iCourierBooking'] = $postSearchAry['iCourierBooking'];
            $bookingAry['iPalletType'] = $iPalletType;
            $bookingAry['szCargoDescription'] = $postSearchAry['szCargoDescription'];
            $bookingAry['fDkkExchangeRate'] = $fDkkExchangeRate; 
            $bookingAry['szInvoiceComment'] = $postSearchAry['szParnerReference']; 
                        
            if($bConfirmedAutomaticBooking)
            {
                /*
                * We update cargo volume and weight only in case of automatic bookings for Manual RFQ we assume that we already have updated cargo details to the bookings from Pending Tray
                */
                $bookingAry['fCargoVolume'] = $cargoDetailsAry['fTotalVolume'];
                $bookingAry['fCargoWeight'] = $cargoDetailsAry['fTotalWeight'];
                $bookingAry['iNumColli'] = $cargoDetailsAry['iNumColli'];     
                $bookingAry['iTotalQuantity'] = $cargoDetailsAry['iTotalQuantity']; 
            } 
            else if($bCreateBookingQuote)
            {
                $bookingAry['fCargoVolume'] = $cargoDetailsAry['fTotalVolume'];
                $bookingAry['fCargoWeight'] = $cargoDetailsAry['fTotalWeight'];
                $bookingAry['iNumColli'] = $cargoDetailsAry['iNumColli'];     
                $bookingAry['iTotalQuantity'] = $cargoDetailsAry['iTotalQuantity']; 
            }
            
            if($bCreateConfirmedBooking)
            {
                /*
                *  Booking confirmation fields will only be updated when we create actual booking into the system
                */
                $bookingAry['idBookingStatus'] = __BOOKING_STATUS_CONFIRMED__;
                $bookingAry['iAcceptedTerms'] = 1;
                $bookingAry['iRemindToRate'] = 0;
                $bookingAry['szBookingRef'] = $szBookingRef;
                $bookingAry['iPaymentType'] = __TRANSPORTECA_PAYMENT_TYPE_3__;
                $bookingAry['szInvoice'] = $szTransportecInvoice;
                $bookingAry['dtBookingConfirmed'] = $dtBookingConfirmed;
                $bookingAry['dtBooked'] = $dtBookingConfirmed;
                $bookingAry['szPaymentStatus'] = 'Completed';
                $bookingAry['idTransaction'] = $idTransaction; 
                $bookingAry['iTransferConfirmed'] = 1;
                $bookingAry['iNumDaysPayBeforePickup'] = $iNumDaysPayBeforePickup;
                $bookingAry['szInsuranceInvoice'] = $invoiceNumber; 
                $bookingAry['iPaymentTypePartner'] = $iPaymentTypePartner; 
                if($bQuickQuoteBooking || $iCheckQuickQuoteBooking)
                {
                    $bookingAry['iQuickQuote']=__QUICK_QUOTE_CONFIRMED_BOOKING__;
                }
                
                if(!$iCheckQuickQuoteBooking)
                {
                    if($bConfirmedManualBooking)
                    {
                        $bookingAry['iQuotesStatus'] = __BOOKING_QUOTES_STATUS_WON__ ;   
                        $bookingAry['iBookingType'] = __BOOKING_TYPE_RFQ__;
                    }
                    else
                    {
                        if($iBookingType==2)
                        {
                            $bookingAry['iCourierBooking'] = 1;
                            $bookingAry['iBookingType'] = __BOOKING_TYPE_COURIER__;
                        }
                        else
                        {
                            $bookingAry['iBookingType'] = __BOOKING_TYPE_AUTOMATIC__;
                        }
                    } 
                }
                $bookingAry['idPartnerApiResponse'] = $idPartnerApiResponse;
            }
            else
            {
                /*
                * Following fields are only updatedin caswhen we create Quote from API.
                */
                
                $bookingAry['iPrivateShipping'] = $kUser->iPrivate;
                
                
                if((float)$postSearchAry['fCargoValue']>0)
                {
                    $fValueOfGoodsUSD=round(($postSearchAry['fCargoValue']*$postSearchAry['fGoodsInsuranceExchangeRate']),4);
                }
                $bookingAry['idGoodsInsuranceCurrency'] = $postSearchAry['idGoodsInsuranceCurrency'];
                $bookingAry['szGoodsInsuranceCurrency'] = $postSearchAry['szGoodsInsuranceCurrency'];
                $bookingAry['fGoodsInsuranceExchangeRate'] = $postSearchAry['fGoodsInsuranceExchangeRate'];
                $bookingAry['fValueOfGoods'] = $postSearchAry['fCargoValue'];
                $bookingAry['fValueOfGoodsUSD'] = $fValueOfGoodsUSD;
             
                $bookingAry['idBookingStatus'] = __BOOKING_STATUS_DRAFT__;
                $bookingAry['iCourierBooking'] = 0;
                $bookingAry['iBookingType'] = __BOOKING_TYPE_RFQ__;
                $bookingAry['iBookingQuotes'] = 1;
                $bookingAry['iQuotesStatus'] = __BOOKING_QUOTES_STATUS_NEW_BOOKING__;
                $bookingAry['iInsuranceChoice'] = 3;
                $bookingAry['szInternalComment'] = $szInternalComment; 
                if($postSearchAry['idServiceType']==__SERVICE_TERMS_EXW__)
                {
                    $bookingAry['szHandoverCity'] = $postSearchAry['szOriginCity'];
                }
                else if($postSearchAry['idServiceType']==__SERVICE_TERMS_DAP__ || $postSearchAry['idServiceType']==__SERVICE_TERMS_DAP_NO_CC__)
                {
                    $bookingAry['szHandoverCity'] = $postSearchAry['szDestinationCity'];
                }
                
                if($data['iOfferType']==1 || $data['iOfferType']==7 || $data['iOfferType']==11 || $data['iOfferType']==4 || $data['iOfferType']==9)
                { 
                    $idTransportMode = __BOOKING_TRANSPORT_MODE_ROAD__;
                }
                else if($data['iOfferType']==2 || $data['iOfferType']==8 || $data['iOfferType']==10)
                { 
                    $idTransportMode = __BOOKING_TRANSPORT_MODE_AIR__ ;
                }
                else if($data['iOfferType']==3 || $data['iOfferType']==6 || $data['iOfferType']==12 || $data['iOfferType']=='')
                { 
                    $idTransportMode = __BOOKING_TRANSPORT_MODE_SEA__ ;
                }                
                $bookingAry['idTransportMode'] = $idTransportMode;
                
                if($data['iOfferType']==1)
                {
                    $bookingAry['szInternalComment'] = 'Had sea options displayed, clicked GET OFFER for road transport';
                }
                else if($data['iOfferType']==2)
                {
                    $bookingAry['szInternalComment'] = 'Had sea options displayed, clicked GET OFFER for airfreight';
                } 
                else if($data['iOfferType']==4)
                {
                    $bookingAry['szInternalComment'] = 'Had courier options with cartons displayed, clicked GET OFFER for shipping on pallets';
                } 
                else if($data['iOfferType']==5)
                {
                    $bookingAry['szInternalComment'] = "";
                }
                else if($data['iOfferType']==6)
                {
                    $bookingAry['szInternalComment'] = "Had rail options displayed, clicked GET OFFER for ocean freight";
                }
                else if($data['iOfferType']==7)
                {
                    $bookingAry['szInternalComment'] = 'Had rail options displayed, clicked GET OFFER for road transport';
                }
                else if($data['iOfferType']==8)
                {
                    $bookingAry['szInternalComment'] = 'Had rail options displayed, clicked GET OFFER for airfreight';
                }
                else if($data['iOfferType']==9)
                {
                    $bookingAry['szInternalComment'] = 'Had rail and courier options with cartons displayed, clicked GET OFFER for shipping on pallets';
                }
                else if($data['iOfferType']==10)
                {
                    $bookingAry['szInternalComment'] = 'Had sea and rail options displayed, clicked GET OFFER for airfreight';
                }
                else if($data['iOfferType']==11)
                {
                    $bookingAry['szInternalComment'] = 'Had sea and rail options displayed, clicked GET OFFER for airfreight';
                }
                else if($data['iOfferType']==12)
                {
                    $bookingAry['szInternalComment'] = 'Had rail and courier options displayed, clicked GET OFFER for airfreight';
                }
                else if($data['iOfferType']>0)
                {
                    $bookingAry['szInternalComment'] = 'Had air options displayed, clicked GET OFFER for ocean freight';
                } 
            }
            if(!empty($this->szCargoType))
            {
                $bookingAry['szCargoType'] = $this->szCargoType; 
            } 
            $filename = __APP_PATH_ROOT__."/logs/api_timing_new".date('Ymd').".log";
 
            $bookingAry['iSendTrackingUpdates'] = $iSendTrackingUpdates; 
            
            $update_query = ''; 
            if(!empty($bookingAry))
            {
                foreach($bookingAry as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            }
            
            $update_query = rtrim($update_query,","); 
            if($kBooking->updateDraftBooking($update_query,$idBooking))
            {
                self::$szDeveloperNotes .= ">. Finnally booking has been successfully created with data: ".print_R($bookingAry,true).", Going forward ".PHP_EOL;
                $bookingDataArr = $kBooking->getExtendedBookingDetails($idBooking); 
                
                $kBooking = new cBooking();
                $dtResponseTime = $kBooking->getRealNow();
                $idBookingFile =$bookingDataArr['idFile'];
                
                if(!$bConfirmedManualBooking)
                {
                    /*
                    * We are not calling this function in case we are confirming the Manual RFQ booking we already have creted this for the booking at time when we are adding this to pending tray
                    */
                    $kCustomerEvent = new cCustomerEevent(); 
                    $kCustomerEvent->createValidateBookingile($idBookingFile);
                } 
                
                $stripeApiResponseAry = array();
                $bReceivedPaymentSuccessFlag = false;
                if($bCreateConfirmedBooking && $szPaymentType==__TRANSPORTECA_PAYMENT_TYPE_1__)
                {
                    $bReceivedPaymentSuccessFlag = true; 
                    /*
                    *  Updating won flag for api call request type: price
                    */ 
                    $this->updateBookingWonFlagApiCallByToken($szRequestType);
                    
                    $stripeChargeAry = array();
                    $stripeChargeAry['szCargeID'] = $szStripeToken;
                    $stripeChargeAry['szBookingRef'] = $szBookingRef;
                    $stripeChargeAry['idBooking'] = $idBooking;
                    cZoozLib::updateStripeInvoiceDescription($stripeChargeAry); 
                } 

                if($bCreateConfirmedBooking)
                {
                    if($bConfirmedAutomaticBooking)
                    {
                        /*
                        *  Updating won flag for api response by service id
                        */  
                        $this->updateBookingWonFlagApiResponse($szServiceID);

                        if($iBookingType==2 && $iAddedNewBooking==1)
                        {
                            /*
                            * Updating cargo details for courier bookings
                            */
                            self::$szDeveloperNotes .= ">. Updating cargo lines. Going forward ".PHP_EOL;
                            $this->updateBookingCargDetails($idShipmentType,$idBooking,true);
                        } 
                        self::$szDeveloperNotes .= ">. Updating cargo Description. Going forward ".PHP_EOL;
                    } 

                    /*
                    * Updating cargo description
                    */
                    $kRegisterShipCon = new cRegisterShipCon(); 
                    $cargoAry = array();
                    $cargoAry['szCargoCommodity'] = $postSearchAry['szCargoDescription']; 
                    $cargoAry['idBooking'] = $idBooking ;
                    $kRegisterShipCon->addUpdateCargoCommodity($cargoAry); 

                    /*
                    * Updating booking file as confirmed booking
                    */
                    $idBookingFile = $bookingDataArr['idFile'];   
                    self::$szDeveloperNotes .= ">. Booking File has been successfully updated as confirmed: ".print_R($fileLogsAry,true).", Going forward ".PHP_EOL;

                    if(trim(self::$szApiRequestMode) == __API_REQUEST_MODE_TEST__)
                    {
                        /*
                        * In case of TEST api call we don't add data in forwarder transaction
                        */
                    }
                    else
                    {
                        if(!$bQuickQuoteBooking)
                        {
                            /*
                            * Updating forwarder transaction after booking confirmation
                            */
                            $bookingDataArr['iTransferConfirmed'] = 1;  
                            $bookingDataArr['iPaymentType']=$szPaymentType;
                            if($bookingDataArr['iCourierBooking']==1)
                            {
                                $kCourierServices=new cCourierServices();
                                $courierBookingArr=$kCourierServices->getCourierBookingData($bookingDataArr['id']);
                                if($courierBookingArr[0]['iCourierAgreementIncluded']==0)
                                {
                                    $courLabelFee=$courierBookingArr[0]['fBookingLabelFeeRate'];
                                    if($courierBookingArr[0]['idBookingLabelFeeCurrency']!=1)
                                    {
                                        $courLabelFee=$courierBookingArr[0]['fBookingLabelFeeRate']*$courierBookingArr[0]['fBookingLabelFeeROE'];
                                    }
                                    $bookingDataArr['fLabelFeeUSD']=$courLabelFee;
                                }
                            } 
                            $kBooking->addrForwarderTranscaction($bookingDataArr); 
                        }
                    } 
                    self::$szDeveloperNotes .= ">. Booking data successfully added to tblforwardertransaction. Going forward ".PHP_EOL;

                    /*
                    * Creating data to log api response
                    */
                    if(!empty($bookingDataArr))
                    { 
                        $dtAvailableDate = $bookingDataArr['dtAvailable'];
                        $dtCutOffDate = $bookingDataArr['dtCutOff'];
                        if($iBookingType==2)
                        {
                            $kCourierServices = new cCourierServices();
                            $packingArr = $kCourierServices->selectProviderPackingList($idPackingType,$this->iBookingLanguage); 
                            $packingText_only= t($this->t_base_service.'fields/only');
                            $szPackingType = ucfirst($packingArr[0]['szPackingDanish']);  
                            if($idPackingType == __COURIER_PACKAGING_TYPE_PALLETS_AND_CARTONS__)
                            {
                                $szPackingType = ucfirst($packingArr[0]['szPacking']);
                            } 
                            else
                            {
                                $szPackingType = $packingText_only." ".strtolower($packingArr[0]['szPacking']);
                            }  
                            $postSearchAry['idTransportMode'] = __BOOKING_TRANSPORT_MODE_COURIER__;
                            $iBookingType = 2;
                        }
                        else
                        {
                            if($postSearchAry['iDTDTrades']==1)
                            {
                                if($postSearchAry['fCargoVolume']>0.6 || $postSearchAry['fCargoWeight']>270)
                                {
                                    $szPackingType = t($this->t_base_service.'fields/euro_pallets'); 
                                }
                                else
                                {
                                    $szPackingType = t($this->t_base_service.'fields/half_euro_pallets'); 
                                }
                            } 
                            else
                            {
                                $szPackingType = t($this->t_base_service.'fields/pallets_and_cartons'); 
                            } 
                            $iBookingType = 1;
                        }
                        $szLatestPaymentDate='';  
                        $iBookingLanguage = $this->iBookingLanguage;
                        $transportModeListAry = array();
                        $transportModeListAry = $kConfig->getAllTransportMode($bookingDataArr['idTransportMode'],$iBookingLanguage,true);
                        $szTransportMode = $transportModeListAry[$bookingDataArr['idTransportMode']]['szLongName'];
 
                        $languageTextDataAry = $kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$iBookingLanguage);
                        $szDay_txt = $languageTextDataAry[1]['szDay_txt'];
                
                        $kWhsSearch = new cWHSSearch();  
                        $arrDescriptions = array("'__TRANSPORTECA_CUSTOMER_CARE__'","'__SEND_UPDATES_CUSTOMER_EMAIL__'"); 
                        $bulkManagemenrVarAry = array();
                        $bulkManagemenrVarAry = $kWhsSearch->getBulkManageMentVariableByDescription($arrDescriptions);      

                        $szCustomerCareNumer = $bulkManagemenrVarAry['__TRANSPORTECA_CUSTOMER_CARE__'];  
                        $szCustomerCareEmail = $bulkManagemenrVarAry['__SEND_UPDATES_CUSTOMER_EMAIL__']; 

                        $szServiceDescription = getServiceDescription($bookingDataArr);
                        
                        $szCollection = 'No';
                        $szDelivery = 'No';
                        if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_DTW__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_DTP__) // 1.DTD 2.DTD
                        {
                            $szCollection = 'Yes';
                        } 
                        if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_WTD__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD 2.DTD
                        {
                            $szDelivery = 'Yes';
                        }
                        
                        $szTransportStartDate = "";
                        $dtTransportStartDay = "";
                        $dtTransportStartMonth = "";
                        $dtTransportStartYear = "";
                        
                        $dtTransportFinishDay = "";
                        $dtTransportFinishMonth = "";
                        $dtTransportFinishYear = ""; 
                        
                        if(!empty($dtCutOffDate) && $dtCutOffDate!='0000-00-00 00:00:00')
                        { 
                            $dtTransportStartDay = date('d',strtotime($dtCutOffDate));
                            $dtTransportStartMonth = date('m',strtotime($dtCutOffDate));
                            $dtTransportStartYear = date('Y',strtotime($dtCutOffDate));
                            $bPickupScheduled = true;
                            
                            $dtStartDateDay = date("j",strtotime($dtCutOffDate));
                            $dtStartDateMonth = date("m",strtotime($dtCutOffDate));
                            $dtStartDateMonthName = getMonthName($iBookingLanguage,$dtStartDateMonth,true);
                            $szTransportStartDate = $dtStartDateDay.". ".$dtStartDateMonthName;
                        } 
                        if(!empty($dtAvailableDate) && $dtAvailableDate!='0000-00-00 00:00:00')
                        { 
                            $dtTransportFinishDay = date('d',strtotime($dtAvailableDate));
                            $dtTransportFinishMonth = date('m',strtotime($dtAvailableDate));
                            $dtTransportFinishYear = date('Y',strtotime($dtAvailableDate));
                            $bDeliveryScheduled = true;
                            
                            $dtFinishDateDay=date("j",strtotime($dtAvailableDate));
                            $dtFinishDateMonth=date("m",strtotime($dtAvailableDate));
                            $dtFinishDateMonthName=getMonthName($iBookingLanguage,$dtFinishDateMonth,true);
                            $szTransportFinishDate = $dtFinishDateDay.". ".$dtFinishDateMonthName;
                        }
                        $iScheduled = 0;
                        if($bPickupScheduled && $bDeliveryScheduled)
                        {
                            $iScheduled = 1;
                        }
                        
                        if($iScheduled==1)
                        {
                            $iTransitDays = "";
                        }
                        else
                        {
                            $iTransitDays = $bookingDataArr['iTransitHours']." ".$szDay_txt; 
                        }
                        $bookingResponseAry = array(); 
                        $bookingResponseAry['szCollection'] = $szCollection;
                        $bookingResponseAry['dtTransportStartDay'] = $dtTransportStartDay;
                        $bookingResponseAry['dtTransportStartMonth'] = $dtTransportStartMonth;
                        $bookingResponseAry['dtTransportStartYear'] = $dtTransportStartYear;
                        $bookingResponseAry['szTransportStartDate']= $szTransportStartDate;
                        
                        $bookingResponseAry['szDelivery'] = $szDelivery; 
                        $bookingResponseAry['dtTransportFinishDay'] = $dtTransportFinishDay;
                        $bookingResponseAry['dtTransportFinishMonth'] = $dtTransportFinishMonth;
                        $bookingResponseAry['dtTransportFinishYear'] = $dtTransportFinishYear;   
                        $bookingResponseAry['szTransportFinishDate'] = $szTransportFinishDate;
                        
                        $bookingResponseAry['iScheduled'] = $iScheduled;  
                        $bookingResponseAry['szTransitTime'] = $iTransitDays;  
                        
                        if($szPaymentType==__TRANSPORTECA_PAYMENT_TYPE_3__)
                        {
                            if($bookingDataArr['iCourierBooking']!=1)
                            {
                                if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__)
                                {
                                    $szBankTransferDueDateTime = strtotime($bookingDataArr['dtCutOff']) - ($bookingDataArr['iBookingCutOffHours']*60*60); 

                                    $iMonth = date('m',$szBankTransferDueDateTime);

                                    $szBankTransferDueDateTime = date('j.',$szBankTransferDueDateTime); 
                                    $szMonthName = getMonthName($this->iBookingLanguage,$iMonth);
                                    $szBankTransferDueDateTime .=" ".$szMonthName ;
                                }
                                else
                                {
                                    $szBankTransferDueDateTime = strtotime($bookingDataArr['dtWhsCutOff']) - ($bookingDataArr['iBookingCutOffHours']*60*60);

                                    $iMonth = date('m',$szBankTransferDueDateTime); 
                                    $szBankTransferDueDateTime = date('j.',$szBankTransferDueDateTime);  

                                    $szMonthName = getMonthName($iBookingLanguage,$iMonth);
                                    $szBankTransferDueDateTime .=" ".$szMonthName ;
                                }
                                $szLatestPaymentDate=$szBankTransferDueDateTime;
                            }
                            
                            if((int)$bookingDataArr['idServiceProvider']>0 && $bookingDataArr['iBookingType']==__BOOKING_TYPE_COURIER__)
                            {
                                $iNumDaysPayBeforePickup = $bookingDataArr['iNumDaysPayBeforePickup'];
                                if($iNumDaysPayBeforePickup<=0)
                                {
                                    $iNumDaysPayBeforePickup = 2;
                                }
                                $szBankTransferDueDateTime=date('Y-m-d',strtotime($bookingDataArr['dtCutOff']));
                                
                                $szBankTransferDueDateTime=getBusinessDaysForCourier($szBankTransferDueDateTime,$iNumDaysPayBeforePickup.' DAY');
                                
                                $iMonth = date('m',strtotime($szBankTransferDueDateTime));
                                $szBankTransferDueDateTime = date('j.',strtotime($szBankTransferDueDateTime));  
                                $szMonthName = getMonthName($iBookingLanguage,$iMonth);
                                $szBankTransferDueDateTime .=" ".$szMonthName ;
                                $szLatestPaymentDate=$szBankTransferDueDateTime;
                            }
                        }
                                     
                        $szTransportStartCountry = "";
                        $szTransportFinishCountry = "";
                        if($bookingDataArr['idServiceProvider']>0)
                        {
                           $bookingResponseAry['szTransportStartCity'] = $bookingDataArr['szOriginCity'];
                           $szTransportStartCountry = $bookingDataArr['szShipperCountryCode'];
                        }
                        else
                        {
                            if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_DTW__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_DTP__)
                            {
                                $bookingResponseAry['szTransportStartCity'] = $bookingDataArr['szOriginCity'];
                                $szTransportStartCountry = $bookingDataArr['szShipperCountryCode'];
                            }
                            else
                            {
                                $bookingResponseAry['szTransportStartCity'] = $bookingDataArr['szWarehouseFromCity'];
                                $szTransportStartCountry = $bookingDataArr['szWarehouseFromCountryCode'];
                            } 
                        } 
                         
                        if($bookingDataArr['idServiceProvider']>0)
                        {
                           $bookingResponseAry['szTransportFinishCity'] = $bookingDataArr['szDestinationCity'];
                           $szTransportFinishCountry = $bookingDataArr['szConsigneeCountryCode']; 
                        }
                        else
                        {
                            if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_WTD__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_PTD__)
                            {
                                $bookingResponseAry['szTransportFinishCity'] = $bookingDataArr['szDestinationCity'];
                                $szTransportFinishCountry = $bookingDataArr['szConsigneeCountryCode']; 
                            }
                            else
                            {
                                $bookingResponseAry['szTransportFinishCity'] = $bookingDataArr['szWarehouseToCity'];
                                $szTransportFinishCountry = $bookingDataArr['szWarehouseToCountryCode']; 
                            }
                        } 
                        $bookingResponseAry['szTransportStartCountry'] = $szTransportStartCountry;
                        $bookingResponseAry['szTransportFinishCountry'] = $szTransportFinishCountry; 
                        
                        if($bookingDataArr['idTransportMode']==__BOOKING_TRANSPORT_MODE_COURIER__ || $bookingDataArr['idTransportMode']==__BOOKING_TRANSPORT_MODE_AIR__)
                        {
                            $szTransportIcon=__AIR_ICON__;
                            if($postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_COURIER__)
                            {
                                $kCourierServices = new cCourierServices();  
                                if($kCourierServices->checkFromCountryToCountryExists($bookingDataArr['idOriginCountry'],$bookingDataArr['idDestinationCountry'],false))
                                { 
                                    $szTransportIcon=__TRUCK_ICON__;

                                }
                            }
                        }
                        else if($bookingDataArr['idTransportMode']==__BOOKING_TRANSPORT_MODE_FCL__ || $bookingDataArr['idTransportMode']==__BOOKING_TRANSPORT_MODE_SEA__)
                        {
                            $szTransportIcon=__SEA_ICON__;
                        }
                        else if($bookingDataArr['idTransportMode']==__BOOKING_TRANSPORT_MODE_ROAD__ || $bookingDataArr['idTransportMode']==__BOOKING_TRANSPORT_MODE_FTL__ || $bookingDataArr['idTransportMode']==__BOOKING_TRANSPORT_MODE_MOVING__)
                        {
                            $szTransportIcon=__TRUCK_ICON__;
                        }
                        else if($bookingDataArr['idTransportMode']==__BOOKING_TRANSPORT_MODE_RAIL__)
                        {
                            $szTransportIcon=__RAIL_ICON__;
                        }
                        
                        $fTotalPrice = round((float)($bookingDataArr['fTotalPriceCustomerCurrency'] + $bookingDataArr['fTotalVat']),2);
                        if($bookingDataArr['iInsuranceIncluded']==1)
                        {
                            $fTotalPrice = round((float)($fTotalPrice + $bookingDataArr['fTotalInsuranceCostForBookingCustomerCurrency']),2);
                        }
                        
                        $bookingResponseAry['szBillingEmail'] = $bookingDataArr['szEmail']; 
                        $bookingResponseAry['szLatestPaymentDate'] = $szLatestPaymentDate; 
                        $bookingResponseAry['szBookingStatus'] = "Confirmed";
                        $bookingResponseAry['szBookingRef'] = $bookingDataArr['szBookingRef'];   
                        $bookingResponseAry['szServiceDescription'] = cApi::encodeString($szServiceDescription);   
                        $bookingResponseAry['szTransportMode'] = cApi::encodeString($szTransportMode);
                        $bookingResponseAry['szTransportIcon'] = $szTransportIcon;
                        $bookingResponseAry['szPackingType'] = cApi::encodeString($szPackingType);
                        $bookingResponseAry['fBookingPrice'] = $bookingDataArr['fTotalPriceCustomerCurrency'];   
                        $bookingResponseAry['fVatAmount'] = $bookingDataArr['fTotalVat']; 
                        $bookingResponseAry['fTotalPrice'] = $fTotalPrice;  
                        $bookingResponseAry['szTotalPrice'] = number_format_custom((float)$fTotalPrice,$iBookingLanguage);
                        if($bookingDataArr['iInsuranceIncluded']==1)
                        {
                            $bookingResponseAry['bCargoInsurance'] = 'Yes'; 
                            $bookingResponseAry['fInsurancePrice'] = round((float)$bookingDataArr['fTotalInsuranceCostForBookingCustomerCurrency'],2);
                        }
                        else
                        {
                            $bookingResponseAry['bCargoInsurance'] = 'No'; 
                        }

                        $kConfig = new cConfig();
                        $kConfig->loadCountry($bookingDataArr['idForwarderCountry']);
                        $szCarrierCountry = $kConfig->szCountryISO;

                        $bookingResponseAry['szCurrency'] = cApi::encodeString($bookingDataArr['szCurrency']);  
                        $bookingResponseAry['szCarrierName'] = cApi::encodeString($bookingDataArr['szForwarderDispName']);
                        $bookingResponseAry['szCarrierAddressLine1'] = cApi::encodeString($bookingDataArr['szForwarderAddress']);
                        $bookingResponseAry['szCarrierAddressLine2'] = cApi::encodeString($bookingDataArr['szForwarderAddress2']);
                        $bookingResponseAry['szCarrierAddressLine3'] = cApi::encodeString($bookingDataArr['szForwarderAddress3']);
                        $bookingResponseAry['szCarrierPostcode'] = $bookingDataArr['szForwarderPostCode'];
                        $bookingResponseAry['szCarrierCity'] = cApi::encodeString($bookingDataArr['szForwarderCity']);
                        $bookingResponseAry['szCarrierRegion'] = $bookingDataArr['szForwarderState'];
                        $bookingResponseAry['szCarrierCountry'] = $szCarrierCountry;
                        $bookingResponseAry['szCarrierCompanyRegistration'] = $szCompanyRegistrationNum;
                        $bookingResponseAry['szBookingContactNumber'] = $szCustomerCareNumer;
                        $bookingResponseAry['szBookingContactEmail'] = $szCustomerCareEmail; 
                        $bookingResponseAry['szPaymentType'] = $szPaymentType; 
                        $bookingResponseAry['iPaymentTypePartner'] = $iPaymentTypePartner; 
                        
                        if($szPaymentType==__TRANSPORTECA_PAYMENT_TYPE_1__)
                        {
                            if($bReceivedPaymentSuccessFlag)
                            {
                                $bookingResponseAry['szPaymentStatus'] = "Completed";
                                $bookingResponseAry['szPaymentDescription'] = "Credit Card payment successfully completed.";
                                //$bookingResponseAry['szStripeTransactionID'] = $stripeApiResponseAry['szStripeTransactionID'];
                                //$bookingResponseAry['szStripeBalanceTransactionID'] = $stripeApiResponseAry['szStripeBalanceTransactionID']; 
                            }
                            else
                            {
                                $bookingResponseAry['szPaymentStatus'] = $stripeApiResponseAry['szPaymentStatus'];
                                $bookingResponseAry['szPaymentDescription'] = $stripeApiResponseAry['szPaymentDescription'];
                            }
                            
                        } 
                        else if($szPaymentType==__TRANSPORTECA_PAYMENT_TYPE_3__)
                        {
                            $idCurrency = $bookingDataArr['idCurrency'];
                            $currencyDetailsAry = array();
                            $currencyDetailsAry = $kBooking->loadCurrencyDetails($idCurrency);
                            
                            $bookingResponseAry['szBank'] = $currencyDetailsAry['szBankName'] ;
                            $bookingResponseAry['szSortCode'] = $currencyDetailsAry['szSortCode'] ;
                            $bookingResponseAry['szAccountNumber'] = $currencyDetailsAry['szAccountNumber'] ;
                            $bookingResponseAry['szSwift'] = $currencyDetailsAry['szSwiftNumber'] ;
                            $bookingResponseAry['szNameOnAccount'] = $currencyDetailsAry['szNameOnAccount'] ;
                        }
                        
                        $kReport= new cReport();
                        $fSaleRevenue = $kReport->getGrossProfitByBooking($idBooking);

                        $bookingResponseAry['grossProfit']['szCurrency'] = "USD";
                        $bookingResponseAry['grossProfit']['fAmount'] = round((float)$fSaleRevenue,4);
                        $responseLogAry = array();
                        $responseLogAry['idPartner'] = self::$idGlobalPartner; 
                        $responseLogAry['szReferenceToken'] = self::$szGlobalToken; 
                        $responseLogAry['szServiceID'] = $szServiceID;
                        $responseLogAry['dtExpectedDelivery'] = $dtAvailableDate;
                        $responseLogAry['szPackingType'] = $szPackingType;
                        $responseLogAry['idPackingType'] = $idPackingType;
                        $responseLogAry['fBookingPrice'] = $bookingDataArr['fTotalPriceCustomerCurrency'];
                        $responseLogAry['fVatAmount'] = $bookingDataArr['fTotalVat'];
                        $responseLogAry['bInsuranceAvailable'] = $bookingResponseAry['bCargoInsurance'];
                        $responseLogAry['fInsurancePrice'] = $serviceAry[$ctr]['fInsurancePrice'];
                        $responseLogAry['idCurrency'] = $bookingDataArr['idCurrency'];
                        $responseLogAry['szCurrency'] = $bookingDataArr['szCurrency'];
                        $responseLogAry['fExchangeRate'] = $bookingDataArr['fExchangeRate'];
                        $responseLogAry['iShipmentType'] = $idShipmentType;
                        $responseLogAry['iBookingType'] = $iBookingType; 
                        $responseLogAry['szBookingReference'] = $bookingResponseAry['szBookingRef'];
                        $responseLogAry['szBookingStatus'] = $bookingResponseAry['szBookingStatus'];
                        $responseLogAry['szBookingContactPhone'] = $bookingResponseAry['szBookingContactNumber'];
                        $responseLogAry['szBookingContactEmail'] = $bookingResponseAry['szBookingContactEmail'];
                        $responseLogAry['szCarrierName'] = $bookingResponseAry['szCarrierName'];
                        $responseLogAry['szCarrierAddress'] = $bookingResponseAry['szCarrierAddressLine1'];
                        $responseLogAry['szCarrierPostCode'] = $bookingResponseAry['szCarrierPostcode'];
                        $responseLogAry['szCarrierCity'] = $bookingResponseAry['szCarrierCity'];
                        $responseLogAry['szCarrierState'] = $bookingResponseAry['szCarrierRegion']; 
                        $responseLogAry['szCarrierCounrty'] = $bookingResponseAry['szCarrierCountry'];
                        $responseLogAry['szCarrierCompanyRegNo'] = $szCompanyRegistrationNum;

                        if($bookingDataArr['iInsuranceIncluded']==1)
                        {
                            $responseLogAry['fValueOfGoods'] = $bookingDataArr['fValueOfGoods'];
                            $responseLogAry['fValueOfGoodsUSD'] = $bookingDataArr['fValueOfGoodsUSD'];
                            $responseLogAry['idGoodsInsuranceCurrency'] = $bookingDataArr['idGoodsInsuranceCurrency'];
                            $responseLogAry['szGoodsInsuranceCurrency'] = $bookingDataArr['szGoodsInsuranceCurrency'];
                            $responseLogAry['fGoodsInsuranceExchangeRate'] = $bookingDataArr['fGoodsInsuranceExchangeRate'];
                            $responseLogAry['fTotalInsuranceCostForBooking'] = $bookingDataArr['fTotalInsuranceCostForBooking']; 
                            $responseLogAry['fTotalInsuranceCostForBookingCustomerCurrency'] = $bookingDataArr['fTotalInsuranceCostForBookingCustomerCurrency'];
                            $responseLogAry['fTotalInsuranceCostForBookingUSD'] = $bookingDataArr['fTotalInsuranceCostForBookingUSD'];
                            $responseLogAry['fTotalAmountInsured'] = $bookingDataArr['fTotalAmountInsured'];
                            $responseLogAry['iMinrateApplied'] = $bookingDataArr['iMinrateApplied']; 
                            $responseLogAry['fTotalInsuranceCostForBookingCustomerCurrency_buyRate'] = $bookingDataArr['fTotalInsuranceCostForBookingCustomerCurrency_buyRate'];
                            $responseLogAry['fTotalInsuranceCostForBookingUSD_buyRate'] = $bookingDataArr['fTotalInsuranceCostForBookingUSD_buyRate'];
                        }  
                        
                        $responseLogAry['szPaymentStatus'] = $bookingResponseAry['szPaymentStatus'];
                        $responseLogAry['szPaymentDescription'] = $bookingResponseAry['szPaymentDescription'];
                        $responseLogAry['szStripeTransactionID'] = $bookingResponseAry['szStripeTransactionID'];
                        $responseLogAry['szStripeBalanceTransactionID'] = $bookingResponseAry['szStripeBalanceTransactionID']; 
                        $this->addServiceApiResponse($responseLogAry);
                    } 

                    if($bookingDataArr['iInsuranceIncluded']==1)
                    { 
                        $fCustomerPaidAmount = $bookingDataArr['fTotalPriceCustomerCurrency'] + $bookingDataArr['fTotalInsuranceCostForBookingCustomerCurrency'];
                        $szPaidAmountStr = $bookingDataArr['szCurrency']." ".number_format((float)$fCustomerPaidAmount)." (".t($this->t_base.'fields/including_insurance')." ".$bookingDataArr['szCurrency']." ".number_format((float)$bookingDataArr['fTotalInsuranceCostForBookingCustomerCurrency'])." )"; 
                    }
                    else
                    { 
                        $fCustomerPaidAmount = $bookingDataArr['fTotalPriceCustomerCurrency'];
                        $szPaidAmountStr = $bookingDataArr['szCurrency']." ".number_format((float)$fCustomerPaidAmount);
                    }   

                    if(trim(self::$szApiRequestMode) == __API_REQUEST_MODE_TEST__)
                    {
                        /*
                        * In case of TEST api call we don't send any email to customer, forwarder or Customer
                        */
                    }
                    else
                    {
                        self::$szDeveloperNotes .= ">. Sending Booking confirmation E-mail to Management and Forwarder and also confirming booking as 'Received'. Going forward ".PHP_EOL;
                        //$this->confirmBookingPayment($idBooking);
                        
                        if($szPaymentType==__TRANSPORTECA_PAYMENT_TYPE_1__ && $bReceivedPaymentSuccessFlag)
                        { 
                            /*
                            * Updating booking file status and task status depending on 
                            */
                            if($szAmount>0)
                            {
                                $bookingDataArr['fCustomerPaidAmount'] = $szAmount;
                            }
                            else
                            {
                                $bookingDataArr['fCustomerPaidAmount'] = $fCustomerPaidAmount;
                            }
                            
                            
                            $addZoozLogsAry = array();
                            $addZoozLogsAry['idBooking'] = $bookingDataArr['id'];
                            $addZoozLogsAry['szBookingRef'] = $bookingDataArr['szBookingRef'];
                            $addZoozLogsAry['szToken'] = $szStripeToken;
                            $addZoozLogsAry['szSessionToken'] = $szStripeToken; 
                            $addZoozLogsAry['szStatus'] = 'PAYMENT_COMPLETED';  
                            $addZoozLogsAry['szDeveloperNotes'] = "Credit Card payment successfully completed.";
                            $addZoozLogsAry['iPaymentMode'] = 2; 
                            $addZoozLogsAry['szSource'] = 'API';
                            $addZoozLogsAry['szPaymentToken'] = $data['szPaymentToken'];
                            $kZooz = new cZooz(); 
                            $kZooz->addPartnerApiStripLog($addZoozLogsAry);
                            
                            $kBilling = new cBilling();
                            $kBilling->confirmBookingAfterPayment($bookingDataArr,$szPaymentType);
                        }
                        else if($szPaymentType==__TRANSPORTECA_PAYMENT_TYPE_3__)
                        {
                            if($szAmount>0)
                            {
                                $bookingDataArr['fCustomerPaidAmount'] = $szAmount;
                            }
                            else
                            {
                                $bookingDataArr['fCustomerPaidAmount'] = $fCustomerPaidAmount;
                            } 
                            $bookingUpdateAry = array();
                            $bookingUpdateAry['fCustomerPaidAmount'] = $bookingDataArr['fCustomerPaidAmount'];
                            
                            $kBilling = new cBilling();
                            $kBilling->updateBookingDetails($bookingUpdateAry,$idBooking);
                            
                            $kBilling->confirmBookingForBankTransfer($bookingDataArr,$szPaymentType);
                        }
                        self::$szDeveloperNotes .= ">. Booking has been confirmed by clicking 'Received' button. Going forward ".PHP_EOL;

                        /*
                        * Sending confirmation email to customer/partner
                        */
                        $kConfig = new cConfig();
                        $kConfig->loadCountry($bookingDataArr['idOriginCountry']);
                        $szOriginCountry = $kConfig->szCountryName;

                        $kConfig = new cConfig();
                        $kConfig->loadCountry($bookingDataArr['idDestinationCountry']);
                        $szDestinationCountry = $kConfig->szCountryName;

                        $replace_ary = array();
                        $replace_ary['szFirstName'] = $bookingDataArr['szFirstName'];
                        $replace_ary['szLastName'] = $bookingDataArr['szLastName'];
                        $replace_ary['szOriginCountry'] = $szOriginCountry;
                        $replace_ary['szDestinationCountry'] = $szDestinationCountry;
                        $replace_ary['szCustomerCompany'] = $bookingDataArr['szCustomerCompanyName'];
                        $replace_ary['szBookingReference'] = $bookingDataArr['szBookingRef']; 
                        $replace_ary['szBookingRef'] = $bookingDataArr['szBookingRef'];
                        $replace_ary['idBooking'] = $idBooking;
                        $replace_ary['iAdminFlag'] = 1;
                        $replace_ary['szBookingValue'] = $szPaidAmountStr;
                        $replace_ary['szVATAmount'] = $bookingDataArr['szCurrency']." ".$bookingDataArr['fTotalVat'];
                        $replace_ary['szYourReference'] = $bookingDataArr['szPartnerReference'];
                        $replace_ary['iBookingLanguage'] = $this->iBookingLanguage;

                       // $bookingDataArr['szEmail'] = 'ajay@whiz-solutions.com';

                        self::$szDeveloperNotes .= ">. Sending Booking confirmation to Customer/Partner. Going forward ".PHP_EOL; 
                        //createEmail(__API_BOOKING_CONFIRMATION__, $replace_ary,$bookingDataArr['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$bookingDataArr['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_CUSTOMER__,true,$idBooking);
                        self::$szDeveloperNotes .= ">. Booking confirmation successfully sent to Customer/Partner. Going forward ".PHP_EOL; 
                        $bookingDetailAry['szEmail'] = __STORE_SUPPORT_EMAIL__;
 
                        $szGlobalPartnerAPIKey = self::$szGlobalPartnerAPIKey;
                        if($szGlobalPartnerAPIKey!='3f31e6d2729d90a98024468a3f3b2bf5') //We don't send this email for partner 'TP'
                        {
                            createEmail(__NEW_BOOKING_WITH_PARTNER_API__, $replace_ary,$bookingDetailAry['szEmail'], '', __STORE_SUPPORT_EMAIL__,$bookingDataArr['idUser'], 'feedback@transporteca.com',__FLAG_FOR_MANAGEMENT__,false,$idBooking);
                            self::$szDeveloperNotes .= ">. Partner API E-mail sent successfully with data: ".print_R($replace_ary,true).". Going forward ".PHP_EOL;
                        } 
                    } 
                }
                else
                { 
                    /*
                    *  IF shipment type is 'Parcel' or 'Pallet' then we update cargo lines in tblcargo
                    */
                    /*if($idShipmentType==1 || $idShipmentType==2)
                    {
                        $cargoDetailsAry = array(); 
                        $this->updateBookingCargDetails($idShipmentType,$idBooking,true);
                    }*/ 
                    
                    //2-June-2017
                    if($bCreateBookingQuote)
                    {
                        if($idShipmentType==1 || $idShipmentType==2)
                        {
                            $cargoDetailsAry = array(); 
                            $this->updateBookingCargDetails($idShipmentType,$idBooking,true);
                        }
                    }
                    
                    /*
                    * Sending confirmation email to Transporteca Admin
                    */
                    $kConfig = new cConfig();
                    $kConfig->loadCountry($bookingDataArr['idOriginCountry']);
                    $szOriginCountry = $kConfig->szCountryName;

                    $kConfig = new cConfig();
                    $kConfig->loadCountry($bookingDataArr['idDestinationCountry']);
                    $szDestinationCountry = $kConfig->szCountryName;
                    
                    $kConfig = new cConfig();
                    $kConfig->loadCountry($bookingDataArr['idCustomerDialCode']);
                    $iInternationDialCode = $kConfig->iInternationDialCode;
                    
                    
                    $replace_ary = array(); 
                    $replace_ary['szOrigingCountryStr'] = $bookingDataArr['szOriginCountry'];
                    $replace_ary['szDestinationCountryStr'] = $bookingDataArr['szDestinationCountry'];
                    $replace_ary['szOriginCountry'] = $szOriginCountry;
                    $replace_ary['szDestinationCountry'] = $szDestinationCountry;
                    $replace_ary['dtTimingDate'] = date('d. M, Y',strtotime($bookingDataArr['dtTimingDate']));
                    $replace_ary['fTotalVolume'] = get_formated_cargo_measure($bookingDataArr['fCargoVolume']);
                    $replace_ary['fTotalWeight'] = number_format((float)$bookingDataArr['fCargoWeight']);

                    $replace_ary['szCustomerCompanyName'] = $bookingDataArr['szCustomerCompanyName']; 
                    $replace_ary['szCustomerFirstName'] = $bookingDataArr['szFirstName'];
                    $replace_ary['szCustomerLastName'] = $bookingDataArr['szLastName']; 
                    $replace_ary['szCustomerEmail'] = $bookingDataArr['szEmail'];
                    $replace_ary['szCustomerPhone'] = "+".$iInternationDialCode." ".$bookingDataArr['szCustomerPhoneNumber'];

                    if(empty($szTransportModeLongName))
                    {
                        $szTransportModeLongName = 'Not specified';
                    }
                    $replace_ary['szTransportMode'] = $szTransportModeLongName ;

                    if(trim(self::$szApiRequestMode) == __API_REQUEST_MODE_TEST__)
                    {
                        //$szToEmail = 'ajay@whiz-solutions.com';
                        $szToEmail = __STORE_SUPPORT_EMAIL__ ;
                        /*
                        * We no longer send this email from 18-04-2016
                        */
                       // createEmail(__TRANSPORTECA_NEW_RFQ__, $replace_ary,$szToEmail, '', __STORE_SUPPORT_EMAIL__,false,false,false,false,$idBooking);
                    }
                    else
                    {
                        //$szToEmail = 'ajay@whiz-solutions.com';
                        $szToEmail = __STORE_SUPPORT_EMAIL__ ;
                        /*
                        * We no longer send this email from 18-04-2016
                        */
                        //createEmail(__TRANSPORTECA_NEW_RFQ__, $replace_ary,$szToEmail, '', __STORE_SUPPORT_EMAIL__,false,false,false,false,$idBooking);
                    } 
                    $iWeekDay = date('w');
                    if($iWeekDay==5 || $iWeekDay==6) //5: Friday, 6: Saturday
                    {
                        $iResponseDay = 1 ;
                    }
                    else
                    {
                        $iResponseDay = 0 ; 
                    }
                    $bookingResponseAry = array();
                    $bookingResponseAry['notification'] = "Transporteca will send the transportation quotation directly to ".$bookingDataArr['szCustomerCompanyName']." within one working day";
                    $bookingResponseAry['iResponseDay'] = $iResponseDay; 
                }
                
                if((int)$bookingDataArr['idOriginCountry']>0)
                {
                    $kConfig = new cConfig();
                    $kConfig->loadCountry($bookingDataArr['idOriginCountry'],'',$bookingDataArr['iBookingLanguage']);
                    $szShipperCountryName=$kConfig->szCountryName;
                    $szShipperCountry=$kConfig->szCountryISO;
                }
                $bookingResponseAry['szShipperCountryName'] = cApi::encodeString($szShipperCountryName);

                if((int)$bookingDataArr['idDestinationCountry']>0)
                {
                    $kConfig = new cConfig();
                    $kConfig->loadCountry($bookingDataArr['idDestinationCountry'],'',$bookingDataArr['iBookingLanguage']);
                    $szConsigneeCountryName=$kConfig->szCountryName;
                    $szConsigneeCountry=$kConfig->szCountryISO;
                }
                $bookingResponseAry['szConsigneeCountryName'] = cApi::encodeString($szConsigneeCountryName);
                $bookingResponseAry['szBillingEmail'] = cApi::encodeString($bookingDataArr['szEmail']);
                if((int)$bookingDataArr['szCustomerCountry']>0)
                {
                    $kConfig = new cConfig();
                    $kConfig->loadCountry($bookingDataArr['szCustomerCountry'],'',$bookingDataArr['iBookingLanguage']);
                    $szBillingCountryName=$kConfig->szCountryName;
                    $szBillingCountry=$kConfig->szCountryISO;
                }
                $bookingResponseAry['szBillingCountryName'] = cApi::encodeString($szBillingCountryName);
                
                $iRequestNps = false;
                if($idCustomer>0)
                {
                    $kNPS = new cNPS();
                    if(!$kNPS->getCustomerFeedBackDay($idCustomer))
                    {
                        $iRequestNps = 1;
                    }
                } 
                $bookingResponseAry['iRequestNps'] = $iRequestNps; 
                 
                self::$szDeveloperNotes .= ">. ********* Process completed with Booking Reference: ".$bookingDataArr['szBookingRef']." ********** ".PHP_EOL;
                self::$arrServiceListAry = $bookingResponseAry;
                self::$szGlobalErrorType = 'SUCCESS'; 
                return self::buildSuccessMessage('Booking');
            } 
        }
    }
    
    private function updateBookingWonFlagApiCallByToken($szRequestType='price')
    {
        $idPartner = self::$idGlobalPartner;
        $szReferenceToken = self::$szGlobalToken;
        $szRequestMode = self::$szApiRequestMode;
            
        $query = "
           UPDATE
               ".__DBC_SCHEMATA_PARTNERS_API_CALL__."
           SET
               iWonBooking = '1' 
           WHERE
                szReferenceToken='".mysql_escape_custom(trim($szReferenceToken))."'
           AND
               idPartner = '".(int)$idPartner."'
           AND
               szRequestType = '".  mysql_escape_custom($szRequestType)."' 
           AND
               szRequestMode = '".mysql_escape_custom(trim($szRequestMode))."'
        ";
        //echo $query ;
        if($result=$this->exeSQL($query))
        {
            return true ;
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
    
    private function updateBookingWonFlagApiResponse($szServiceID)
    {
        if(!empty($szServiceID))
        {
            $idPartner = self::$idGlobalPartner;
            $szReferenceToken = self::$szGlobalToken; 
            $szRequestMode = self::$szApiRequestMode;

            $query = "
                UPDATE
                    ".__DBC_SCHEMATA_PARTNERS_API_RESPONSE__."
                SET
                    iWonBooking = '1' 
                WHERE
                    szServiceID='".mysql_escape_custom(trim($szServiceID))."'
                AND
                     szReferenceToken= '".mysql_escape_custom(trim($szReferenceToken))."'
                AND
                    idPartner = '".(int)$idPartner."' 
                AND
                    szRequestMode = '".mysql_escape_custom(trim($szRequestMode))."'
            ";
            //echo $query ;
            if($result=$this->exeSQL($query))
            {
                return true ;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        } 
    }
    
    private function isBookingAlreadyPlacedForToken()
    {
        $idPartner = self::$idGlobalPartner;
        $szReferenceToken = self::$szGlobalToken; 
        $szRequestMode = self::$szApiRequestMode;
        
        $query="
            SELECT
                id 
            FROM
                ".__DBC_SCHEMATA_PARTNERS_API_CALL__."
            WHERE
                szReferenceToken='".mysql_escape_custom(trim($szReferenceToken))."'
            AND
                idPartner = '".(int)$idPartner."'
            AND
                iWonBooking = '1' 
            AND
                (szRequestType = 'price' OR szRequestType = 'validatePrice')
            AND
                szRequestMode = '".mysql_escape_custom(trim($szRequestMode))."'
        "; 
        //echo "<br><br>".$query; 
        if($result=$this->exeSQL($query))
        {
            if($this->getRowCnt()>0)
            {
                $successFlag = true; 
            }
            else
            {
                $successFlag = false; 
            }
        }
        return $successFlag;
    }
    
    private function compareCargoDetail($szRequestType='price')
    { 
        if(!empty($this->szPacketType))
        {
            $counter =1; 
            $idPartner = self::$idGlobalPartner;
            $szReferenceToken = self::$szGlobalToken; 
            $successFlag = true;
            foreach($this->szPacketType as $key=>$cargoDetail)
            {    
                if(strtoupper($this->szPacketType[$key])==__PACKET_TYPE_PARCEL__) 
                {
                    $iShipmentType = 1;
                }
                else if(strtoupper($this->szPacketType[$key])==__PACKET_TYPE_PALLET__)
                {
                    $iShipmentType = 2;
                }
                else if(strtoupper($this->szPacketType[$key])==__PACKET_TYPE_UNKNOWN__)
                {
                    $iShipmentType = 4;
                }
                else
                {
                    $iShipmentType = 3;
                }
                
                $iLength = (float)$this->iLength[$key];
                $iHeight = (float)$this->iHeight[$key];
                $iWidth = (float)$this->iWidth[$key];
                $iWeight = (float)$this->iWeight[$key]; 
                $iQuantity = (int)$this->iQuantity[$key];  
                $szPalletType = $this->szPalletType[$key];
                
                $szDimensionMeasure = $this->szDimensionMeasure[$key];
                $szWeightMeasure = $this->szWeightMeasure[$key];
                $idWeightMeasure = $this->idWeightMeasure[$key];
                $idDimensionMeasure = $this->idDimensionMeasure[$key];
                 
                $fTotalWeight = (float)$this->fTotalWeight[$key]; 
                $fTotalVolume = (float)$this->fTotalVolume[$key]; 
                $counter++;
                
                if($iShipmentType==4)
                {
                    $query_and = " ";
                } 
                else if($iShipmentType==3)
                {
                    $query_and = " 
                        AND 
                            fTotalWeight = ".mysql_escape_custom($fTotalWeight)."
                        AND 
                            fTotalVolume = ".mysql_escape_custom($fTotalVolume)."
                        AND
                            idWeightMeasure='".mysql_escape_custom($idWeightMeasure)."'
                        ";
                }
                else
                {
                    $query_and = " 
                        AND 
                            fLength = ".mysql_escape_custom($iLength)." 
                        AND 
                            fWidth = ".mysql_escape_custom($iWidth)." 
                        AND 
                            fHeight = ".mysql_escape_custom($iHeight)."
                        AND 
                            iQuantity = '".mysql_escape_custom($iQuantity)."' 
                        AND 
                            fWeight = ".mysql_escape_custom($iWeight)."
                        AND
                            idWeightMeasure='".mysql_escape_custom($idWeightMeasure)."'
                        AND
                            idCargoMeasure='".mysql_escape_custom($idDimensionMeasure)."'
                    ";
                } 
                if($iShipmentType==2)
                {
                    $query_and .= " AND LOWER(szPalletType) = '".mysql_escape_custom(strtolower($szPalletType))."' ";
                } 
                $query="
                    SELECT
                        id,
                        idPartner,
                        szReferenceToken, 
                        iShipmentType,
                        idWeightMeasure,
                        idCargoMeasure,
                        szDimensionMeasure,
                        szWeightMeasure,
                        dtCreatedOn 
                    FROM
                        ".__DBC_SCHEMATA_PARTNERS_API_CARGO_DETAIL__."
                    WHERE
                        szReferenceToken='".mysql_escape_custom(trim($szReferenceToken))."'
                    AND
                        idPartner = '".(int)$idPartner."'
                    AND
                        iShipmentType = '".(int)$iShipmentType."' 
                    AND
                        szRequestType = '".  mysql_escape_custom($szRequestType)."'
                    $query_and
                "; 
                //echo "<br><br>".$query; 
                if($result=$this->exeSQL($query))
                {
                    if($this->getRowCnt()>0)
                    {
                         
                    }
                    else
                    {
                        $successFlag = false;
                        break;
                    }
                }
            }   
            return $successFlag;
        }
        else
        {
            return false;
        }
    } 
    
    function updateBookingCargDetails($idShipmentType,$idBooking=false,$bUpdateCargoData=false,$bFormatTotal=false)
    {    
        if($idShipmentType)
        {
            $cargoDetail = array(); 
            $cargoDetailAry = $this->getCargoDeailsByPartnerApi($idShipmentType); 
            $szCargoTotalLine = '';
            $kWHSSearch = new cWHSSearch();
            $szGlobalMode = self::$szGlobalMode;
            if(!empty($cargoDetailAry))
            {
                $i=1; 
                foreach($cargoDetailAry as $cargoDetailData)
                {
                    $idPalletType = 0;
                    $szCommodity = "";
                    $szSellersReference='';
                    if($idShipmentType==3) //Break bulk
                    {
                        $fTotalVolume += $cargoDetailData['fTotalVolume'];
                        $fTotalWeight += $cargoDetailData['fTotalWeight'];
                        $iNumColli = 0;
                        $iTotalQuantity = 0; 
                        if($szGlobalMode=='QUICK_QUOTE')
                        {
                            $szCargoTotalLine .= "Price based on Break Bulk: ".format_volume($cargoDetailData['fTotalVolume'])." cbm, ".number_format((float)$cargoDetailData['fTotalWeight'])." kg ".PHP_EOL; 
                        }
                        else
                        {
                            $szCargoTotalLine .= "Break Bulk: ".format_volume($cargoDetailData['fTotalVolume'])." cbm, ".number_format((float)$cargoDetailData['fTotalWeight'])." kg ".PHP_EOL; 
                        } 
                        $szShipmentType = __PACKET_TYPE_BREAK_BULK__;
                    }
                    else
                    {
                        $fLength = $cargoDetailData['fLength'];
                        $fWidth = $cargoDetailData['fWidth'];
                        $fHeight = $cargoDetailData['fHeight'];
                        $iQuantity = $cargoDetailData['iQuantity'];
                        $fWeight = $cargoDetailData['fWeight'];
                        $idWeightMeasure = $cargoDetailData['idWeightMeasure'];
                        $idCargoMeasure = $cargoDetailData['idCargoMeasure'];
                        $szCommodity = $cargoDetailData['szCommodity'];
                        $fTotalVolumePerColli = $cargoDetailData['fTotalVolume'];
                        $szSellersReference = $cargoDetailData['szSellersReference'];
                        $iTotalQuantity += $iQuantity ;
                        $iNumColli++;
                        
                        if((float)$fTotalVolumePerColli>0.00)
                        {
                            $fTotalVolume += $fTotalVolumePerColli * $iQuantity;
                            if($idCargoMeasure==1)  // cm
                            {
                                $szCargoMeasure = 'cm';
                            }
                            else{
                                $szCargoMeasure = 'inch';
                            }
                        }
                        else
                        {
                            if($idCargoMeasure==1)  // cm
                            {
                                $fTotalVolume += ($fLength/100) * ($fWidth/100) * ($fHeight/100) * $iQuantity;
                                $szCargoMeasure = 'cm';
                            }
                            else
                            {
                                $fCmFactor = $kWHSSearch->getCargoFactor($idCargoMeasure);

                                if($fCmFactor>0)
                                {
                                    $fTotalVolume += (($fLength/$fCmFactor )/100) * (($fWidth/$fCmFactor)/100) * (($fHeight/$fCmFactor)/100) * $iQuantity;
                                }        
                                $szCargoMeasure = 'inch';
                            }
                        }
                        if($idWeightMeasure==1)  // kg
                        {
                            $fTotalWeight += ($fWeight*$iQuantity);
                            $szWeightMeasure = "kg";
                        }
                        else
                        {
                            $fKgFactor = $kWHSSearch->getWeightFactor($idWeightMeasure); 
                            if($fKgFactor>0)
                            {
                                $fTotalWeight += ((($fWeight*$iQuantity)/$fKgFactor ));
                            }        
                            $szWeightMeasure = "Pounds";
                        }  
                        
                        if(trim(self::$szApiRequestMode) == __API_REQUEST_MODE_TEST__)
                        {
                            $table = __DBC_SCHEMATA_CLONE_CARGO__;
                        }
                        else
                        {
                            $table = __DBC_SCHEMATA_CARGO__;
                        } 
                        
                        if($idShipmentType==2)//Pallet
                        { 
                            $preText = "Pallet:";
                            $szShipmentType = __PACKET_TYPE_PALLET__;
                            
                            $kQuote = new cQuote();
                            $palletDetailAry = $kQuote->getPalletDetails(false,false,$cargoDetailData['szPalletType']);
                            if(!empty($palletDetailAry))
                            {
                                $idPalletType = $palletDetailAry['id'];
                                $szPalletType = $palletDetailAry['szShortName'];
                                $szPalletType = ucfirst(str_replace(' ', '', $szPalletType));
                                
                                $szCommodity = $palletDetailAry['szLongName'];
                                if($iQuantity>1)
                                {
                                    $szPalletType = $szPalletType."s";
                                }
                            } 
                            /*
                             * Previously we were using these values from constant but now we are picking this from 
                            if($cargoDetailData['szPalletType']==__PALLET_TYPE_EURO__)
                            {
                                if($iQuantity>1)
                                {
                                    $szPalletType = "Europallets";
                                }
                                else
                                {
                                    $szPalletType = "Europallet";
                                } 
                                $idPalletType = __PALLET_ID_EURO__;
                            } 
                            else if($cargoDetailData['szPalletType']==__PALLET_TYPE_HALF__)
                            {
                                if($iQuantity>1)
                                {
                                    $szPalletType = "Halfpallets";
                                }
                                else
                                {
                                    $szPalletType = "Halfpallet";
                                }  
                                $idPalletType = __PALLET_ID_HALF__;
                            }
                            else if($cargoDetailData['szPalletType']==__PALLET_TYPE_ENGLISH_PALLET__)
                            {
                                if($iQuantity>1)
                                {
                                    $szPalletType = "Englishpallets";
                                }
                                else
                                {
                                    $szPalletType = "Englishpallet";
                                }  
                                $idPalletType = __PALLET_ID_ENGLISH_PALLET__;
                            }
                            else
                            {
                                $szPalletType = "Others";
                                $idPalletType = __PALLET_ID_OTHER__;
                            }
                             * 
                             */
                            //$szCommodity = $szPalletType;
                        }
                        else if($idShipmentType==1) //Parcel
                        { 
                            $szPalletType = "colli";
                            $preText = "Parcel:";
                            $szShipmentType = __PACKET_TYPE_PARCEL__;
                        } 
                        if($szGlobalMode=='QUICK_QUOTE')
                        {
                            $preText = "Price based on ".$preText; 
                        }
                        $szCargoTotalLine .= $preText." ".$iQuantity." ".$szPalletType." (".round_up($fLength,1)."x".round_up($fWidth,1)."x".round_up($fHeight,1)."".$szCargoMeasure."), total ".round_up(($fWeight*$iQuantity),1).$szWeightMeasure." ".PHP_EOL; 
                        
                        if($bUpdateCargoData)
                        {
                            $query="
                                INSERT INTO
                                    ".$table."
                                (
                                    idBooking,
                                    idPalletType,
                                    szShipmentType,
                                    fLength,
                                    fWidth,
                                    fHeight,
                                    idCargoMeasure,
                                    iQuantity,
                                    szCommodity,
                                    iColli,
                                    fWeight,
                                    idWeightMeasure, 
                                    iActive,
                                    dtCreatedOn,
                                    fVolume,
                                    szSellerReference
                                )
                                VALUES
                                (
                                    '".(int)$idBooking."',
                                    '".(int)$idPalletType."', 
                                    '".mysql_escape_custom($szShipmentType)."', 
                                    '".round((float)$fLength,2)."',
                                    '".round((float)$fWidth,2)."',
                                    '".round((float)$fHeight,2)."',
                                    '".(int)$idCargoMeasure."',
                                    '".round((float)$iQuantity)."',
                                    '".mysql_escape_custom($szCommodity)."',
                                    '".round((float)$iQuantity)."',
                                    '".round((float)$fWeight,2)."',
                                    '".(int)$idWeightMeasure."', 
                                    '1',
                                    now(),
                                    '".(float)$fTotalVolumePerColli."',
                                    '".mysql_escape_custom($szSellersReference)."'    
                                )
                            ";
                            //echo "<br /> ".$query."<br /> ";
                            $result=$this->exeSQL($query);
                        } 
                    } 
                } 
                
                $cargoDetail['fTotalVolume'] = $fTotalVolume;
                $cargoDetail['fTotalWeight'] = $fTotalWeight;
                $cargoDetail['iTotalQuantity'] = $iTotalQuantity;
                $cargoDetail['iNumColli'] = $iTotalQuantity;
                $cargoDetail['szCargoTotalLine'] = $szCargoTotalLine;
                return $cargoDetail;
            }
        }
    }  
    
    public function getQuickQuoteRequestData($szRequestType='price',$szServiceRefrence=false,$szReferenceToken=false)
    {
        $idPartner = self::$idGlobalPartner;
        if(empty($szReferenceToken))
        {
            $szReferenceToken = self::$szGlobalToken; 
        }  
        $szRequestMode = self::$szApiRequestMode; 
        
        $query_and = "";
        if(!empty($szServiceRefrence))
        {
            $query_and = " AND szServiceRefrence = '".mysql_escape_custom($szServiceRefrence)."' ";
        }
        
        $query="
            SELECT
                id,
                idPartner,
                szRequestMode,
                szSearchType,
                idTimingType,
                idServiceType,
                szOriginCountryStr,
                szDestinationCountryStr,
                szCargoType,
                iCustomerType,
                iOriginCC,
                iDestinationCC,
                szReferenceToken,
                szRequestType,
                dtShipping,
                szPacketType,
                szServiceTerm,
                szShipperCompanyName,
                szShipperFirstName,
                szShipperLastName,
                szShipperEmail,
                iShipperCountryDialCode,
                szShipperPhoneNumber,
                szShipperAddress,
                szShipperCity,
                szShipperPostCode,
                szShipperCountry,
                szShipperLattitude,
                szShipperLongitude,
                szConsigneeCompanyName,
                szConsigneeFirstName,
                szConsigneeLastName,
                szConsigneeEmail,
                iConsigneeCountryDialCode,
                szConsigneePhoneNumber,
                szConsigneeAddress,
                szConsigneeCity,
                szConsigneePostCode,
                szConsigneeCountry,
                szConsigneeLattitude,
                szConsigneeLongitude,
                idCurrency,
                szCurrency,
                fExchangeRate,
                fTotalVolume,
                fTotalWeight,
                fCargoValue,
                szGoodsInsuranceCurrency,
                idGoodsInsuranceCurrency,
                fGoodsInsuranceExchangeRate,
                szCargoDescription, 
                szServiceRefrence,
                iInsuranceFlag,
                iSearchBreakBulk,
                iSearchParcel,
                iSearchPallet,
                szClientUserAgent,
                szUserAgent,  
                dtCreatedOn,
                szBillingCompanyName
                szBillingFirstName,
                szBillingLastName,
                iBillingCountryDialCode,
                iBillingPhoneNumber,
                szBillingEmail,
                szBillingAddress,
                szBillingPostcode,
                szBillingCity,
                szBillingCountry
                iShipperConsigneeType,
                iDonotKnowShipperPostcode,
                iDonotKnowConsigneePostcode,
                iBookingLanguage,
                iShipmentType
            FROM
                ".__DBC_SCHEMATA_PARTNERS_API_CALL__."
            WHERE
                szReferenceToken='".mysql_escape_custom(trim($szReferenceToken))."'
            AND
                idPartner = '".(int)$idPartner."' 
            AND
                szRequestType = '".mysql_escape_custom($szRequestType)."' 
            AND
                szRequestMode = '".mysql_escape_custom(trim($szRequestMode))."'
                $query_and
        "; 
       // echo "<br><br>".$query;  
        if($result=$this->exeSQL($query))
        {
            if($this->getRowCnt()>0)
            {
                $row = $this->getAssoc($result);
                return $row;
            }
            else
            {
                $successFlag = false; 
            }
        }
        return $successFlag;
    }

    public function createEmptyBooking()
    {
        if(trim(self::$szApiRequestMode) == __API_REQUEST_MODE_TEST__)
        {
            $table = __DBC_SCHEMATA_CLONE_BOOKING__;
        }
        else
        {
            $table = __DBC_SCHEMATA_BOOKING__;
        }
        
        $kBooking = new cBooking();
        $szBookingRandomNum = $kBooking->getUniqueBookingKey(10);
        
        $szBookingRandomNum = $kBooking->isBookingRandomNumExist($szBookingRandomNum); 
        
        $query="
            INSERT INTO
                ".$table."
            (
                idBookingStatus,
                szBookingRandomNum,
                iPartnerBooking,
                iFinancialVersion,
                dtCreatedOn
            )
            VALUES
            (
                '".__BOOKING_STATUS_DRAFT__."',
                '".  mysql_escape_custom($szBookingRandomNum)."',
                '1',
                '".__TRANSPORTECA_FINANCIAL_VERSION__."',
                now()
            )
        "; 
        //echo $query;
        if($result = $this->exeSQL($query))
        {
            $this->szBookingRandomNum=$szBookingRandomNum;
            return $this->iLastInsertID ;    
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
    
    public function createBookingFile($postSearchAry,$kUser,$idBooking,$bBookingQuote=false,$bQuickQuote=false)
    {
        if(!empty($postSearchAry) && $idBooking>0)
        {
            if($bBookingQuote)
            {
                $szFileStatus = 'S1';
                $szTaskStatus = 'T1';
                $iSearchType = 2; //RFQ
            }
            else
            {
                $szFileStatus = 'S120';
                $szTaskStatus = 'T160';
                $iSearchType = 1; //Automatic Bookings
            }
            
            $idPartner = self::$idGlobalPartner;
            
            $createBookingFileAry = array();
            $createBookingFileAry['idBooking'] = $idBooking ; 
            $createBookingFileAry['szFileStatus'] = $szFileStatus;  
            $createBookingFileAry['iSearchType'] = $iSearchType;
            
            if($kUser->id>0)
            {
                $createBookingFileAry['idUser'] = $kUser->id; 
                $createBookingFileAry['idCustomerOwner'] = $kUser->idCustomerOwner ;
                $createBookingFileAry['idFileOwner'] = $kUser->idCustomerOwner ;
            }
            
            if($bQuickQuote)
            {
                $createBookingFileAry['iFromApi'] = 0; 
                $createBookingFileAry['idApiUser'] = ''; 
                $createBookingFileAry['szTaskStatus'] = ""; 
            }
            else
            {
                $createBookingFileAry['iFromApi'] = 1; 
                $createBookingFileAry['idApiUser'] = $idPartner; 
                $createBookingFileAry['szTaskStatus'] = $szTaskStatus; 
            }  
            $kRegisterShipCon = new cRegisterShipCon(); 
            $kRegisterShipCon->addBookingFile($createBookingFileAry);
            $idBookingFile = $kRegisterShipCon->idBookingFile ;
            return $idBookingFile;
        }
    }
    
    public function createShipperConsignee($postSearchAry,$idBooking)
    {
        if(!empty($postSearchAry) && $idBooking>0)
        { 
            if($postSearchAry['iDonotKnowShipperPostcode'])
            {
                $postSearchAry['szOriginPostCode'] = "";
            }
            if($postSearchAry['iDonotKnowConsigneePostcode'])
            {
                $postSearchAry['szDestinationPostCode'] = "";
            } 
            
            $kRegisterShipCon = new cRegisterShipCon();   
            $kRegisterShipCon->szShipperCompanyName = $postSearchAry['szShipperCompanyName'];
            $kRegisterShipCon->szShipperFirstName = $postSearchAry['szShipperFirstName'];
            $kRegisterShipCon->szShipperLastName = $postSearchAry['szShipperLastName'];
            $kRegisterShipCon->szShipperEmail = $postSearchAry['szShipperEmail'];
            $kRegisterShipCon->szShipperPhone = $postSearchAry['szShipperPhoneNumber'];
            $kRegisterShipCon->szShipperAddress = $postSearchAry['szShipperAddress'];
            $kRegisterShipCon->szShipperPostcode = $postSearchAry['szOriginPostCode'];
            $kRegisterShipCon->szShipperCity = $postSearchAry['szOriginCity'];
            $kRegisterShipCon->szShipperState = $postSearchAry['szShipperState'];
            $kRegisterShipCon->idShipperCountry = $postSearchAry['idOriginCountry'];
            $kRegisterShipCon->idShipperDialCode = $postSearchAry['idShipperDialCode']; 
            $kRegisterShipCon->szShipperAddress_pickup = $postSearchAry['szShipperAddress'];
            $kRegisterShipCon->szShipperPostcode_pickup = $postSearchAry['szOriginPostCode'];
            $kRegisterShipCon->szShipperCity_pickup = $postSearchAry['szOriginCity'];
            $kRegisterShipCon->szShipperState_pickup = $postSearchAry['szShipperState'];
            $kRegisterShipCon->idShipperCountry_pickup = $postSearchAry['idOriginCountry'];
            $kRegisterShipCon->iShipperPostcodeRequired = $postSearchAry['iShipperPostcodeRequired'];
            $kRegisterShipCon->iCollectionFlag = $postSearchAry['iCollectionFlag'];
              
            $kRegisterShipCon->szConsigneeCompanyName = $postSearchAry['szConsigneeCompanyName'];
            $kRegisterShipCon->szConsigneeFirstName = $postSearchAry['szConsigneeFirstName'];
            $kRegisterShipCon->szConsigneeLastName = $postSearchAry['szConsigneeLastName'];
            $kRegisterShipCon->szConsigneeEmail = $postSearchAry['szConsigneeEmail'];
            $kRegisterShipCon->szConsigneePhone = $postSearchAry['szConsigneePhoneNumber'];
            $kRegisterShipCon->idConsigneeDialCode = $postSearchAry['idConsigneeDialCode'];
            $kRegisterShipCon->szConsigneeAddress = $postSearchAry['szConsigneeAddress'];
            $kRegisterShipCon->szConsigneePostcode = $postSearchAry['szDestinationPostCode'];
            $kRegisterShipCon->szConsigneeCity = $postSearchAry['szDestinationCity'];
            $kRegisterShipCon->szConsigneeState = "";
            $kRegisterShipCon->idConsigneeCountry = $postSearchAry['idDestinationCountry'];
            $kRegisterShipCon->szConsigneeAddress_pickup = $postSearchAry['szConsigneeAddress'];
            $kRegisterShipCon->szConsigneePostcode_pickup = $postSearchAry['szDestinationPostCode'];
            $kRegisterShipCon->szConsigneeCity_pickup = $postSearchAry['szDestinationCity'];
            $kRegisterShipCon->szConsigneeState_pickup = "";
            $kRegisterShipCon->idConsigneeCountry_pickup = $postSearchAry['idDestinationCountry'];
            $kRegisterShipCon->iShipperConsignee = $postSearchAry['iShipperConsignee'];
            
            $kRegisterShipCon->iDeliveryFlag = $postSearchAry['iDeliveryFlag'];
            $kRegisterShipCon->iConsigneePostcodeRequired = $postSearchAry['iConsigneePostcodeRequired'];
            
                
            $shipperConsigneeAry = array();
            $shipperConsigneeAry['iPrivate'] = $postSearchAry['iCustomerType'];
            $idShipperConsignee = $kRegisterShipCon->addShipperConsignee_new($shipperConsigneeAry,$idBooking,true);  
            return $idShipperConsignee;
        }
    }
    
    /*
    * 
    */
    public function getParnerAccountDetails($idPartner)
    {
        if($idPartner>0)
        {
            $query="
                SELECT 
                    id, 
                    idCustomer,
                    szApiKey,
                    szPartnerAlies,
                    dtCreatedOn,
                    dtUpdatedOn,
                    iActive,
                    isDeleted
                FROM	
                    ".__DBC_SCHEMATA_PARTNERS__."
                WHERE
                    id = '".mysql_escape_custom(trim($idPartner))."'
                AND
                    isDeleted = '0'
                AND
                    iActive = '1'
            ";
            //echo $query;
            if($result=$this->exeSQL($query))
            {
                if($this->getRowCnt()>0)
                {
                    $row = $this->getAssoc($result);
                    return $row;
                } 
            } 
        } 
    }
    
    /*
    * This function is used to check wheteher service exist in tblpartnerapiresponse or not
    * @return array()
    * @author Whiz Solutions
    */
    public function getServiceResponseDetails($idPartner,$szReferenceToken,$szServiceID,$bGetAllServices=false)
    {
        if(!empty($szServiceID) || $bGetAllServices)
        {
            if(empty($szReferenceToken))
            {
                $szReferenceToken = self::$szGlobalToken;
            } 
            $idPartner = self::$idGlobalPartner; 
            $szApiRequestMode = self::$szApiRequestMode;
            
            if(!empty($szServiceID))
            {
                $query_and = " AND szServiceID='".mysql_escape_custom(trim($szServiceID))."' ";
            }
            
            $query="
                SELECT
                    id,
                    szReferenceToken,
                    szServiceID,
                    dtExpectedDelivery,
                    szPackingType,
                    fBookingPrice,
                    fVatAmount,
                    bInsuranceAvailable,
                    fInsurancePrice,
                    idCurrency,
                    szCurrency,
                    fExchangeRate,
                    fValueOfGoods,
                    fValueOfGoodsUSD,
                    idGoodsInsuranceCurrency,
                    szGoodsInsuranceCurrency,
                    fGoodsInsuranceExchangeRate,
                    fTotalInsuranceCostForBookingCustomerCurrency,
                    fTotalInsuranceCostForBookingCustomerCurrency_buyRate,
                    iShipmentType,
                    iBookingType,
                    idPackingType,
                    dtCreatedOn,
                    szCollection,
                    szDelivery,
                    szShipperPostcodeRequired,
                    szConsigneePostcodeRequired 
                FROM
                    ".__DBC_SCHEMATA_PARTNERS_API_RESPONSE__."
                WHERE 
                    szReferenceToken='".mysql_escape_custom(trim($szReferenceToken))."'
                AND
                    idPartner='".(int)$idPartner."'
                AND
                    szRequestMode = '".mysql_escape_custom(trim($szApiRequestMode))."'
                $query_and
            ";
            //echo $query;
            if($result=$this->exeSQL($query))
            {
                if($this->getRowCnt()>0)
                {
                    if($bGetAllServices)
                    {
                        $ret_ary = array();
                        while($row = $this->getAssoc($result))
                        {
                            $ret_ary[$ctr] = $row;
                            $ctr++;
                        }
                        return $ret_ary;
                    }
                    else
                    {
                        $row = $this->getAssoc($result);
                        return $row;
                    } 
                }
                else
                {
                    return array();
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }     
        }
        else
        {
            return array();
        } 
    }
    /*
    * This function is used to check whether 
    * booking post and stored tblpartnerapicall data are same or not
    * @return true or false
    * @author Whiz Solutions
    */
    private function isParamsChangedOnBookingCall($data,$szRequestType='price')
    {
        if(!empty($data))
        {   
            $szReferenceToken = self::$szGlobalToken;
            $idPartner = self::$idGlobalPartner;  
            if(trim($data['iShipmentType']) == __PACKET_TYPE_PALLET__ || trim($data['iShipmentType']) == __PACKET_TYPE_PARCEL__)
            {
                $query_and = "
                    AND
                        szShipperPostCode = '".mysql_escape_custom(trim($data['szOriginPostCode']))."'
                    AND
                        szConsigneePostCode = '".mysql_escape_custom(trim($data['szDestinationPostCode']))."'
                ";
            }
            if($data['iInsuranceIncluded']==1)
            {
                /*
                $query_and .= "
                    AND
                        idGoodsInsuranceCurrency = '".mysql_escape_custom(trim($data['idGoodsInsuranceCurrency']))."'
                    AND
                        fCargoValue = '".mysql_escape_custom(trim($data['fCargoValue']))."'
                ";
                 * 
                 */
            }
            $szApiRequestMode = self::$szApiRequestMode;
            
            /*
             * We are no longer validating city names in booking API call
             * AND
                    szShipperCity = '".mysql_escape_custom(trim($data['szOriginCity']))."'
             * AND
                    szConsigneeCity = '".mysql_escape_custom(trim($data['szDestinationCity']))."'
             */
            $query="
                SELECT
                    id
                FROM
                    ".__DBC_SCHEMATA_PARTNERS_API_CALL__."
                WHERE
                    szRequestType = '".  mysql_escape_custom($szRequestType)."'
                AND
                    idPartner = '".(int)$idPartner."'
                AND
                    szRequestMode = '".mysql_escape_custom(trim($szApiRequestMode))."'
                AND
                    szReferenceToken = '".mysql_escape_custom(trim($szReferenceToken))."'
                AND
                    DATE(dtShipping) = '".mysql_escape_custom(trim($data['dtShipping']))."' 
                AND
                    szShipperCountry = '".mysql_escape_custom(trim($data['idOriginCountry']))."' 
                AND
                    szConsigneeCountry = '".mysql_escape_custom(trim($data['idDestinationCountry']))."'
                AND
                    szBillingCountry = '".mysql_escape_custom(trim($data['idBillingCountry']))."'
                AND
                    idCurrency = '".mysql_escape_custom(trim($data['idCurrency']))."'
                AND
                    idServiceType = '".mysql_escape_custom(trim($data['idServiceType']))."'
                AND
                    iOriginCC = '".mysql_escape_custom(trim($data['iOriginCC']))."'
                AND
                    iDestinationCC = '".mysql_escape_custom(trim($data['iDestinationCC']))."'
                $query_and
            ";
            //echo $query;
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                { 
                    return 'SUCCESS';
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
        }
        else
        {
            return array();
        }
    }
    /*
    * This function is check service is 
    * 24hrs older or not
    * @return true or false
    * @author Whiz Solutions
    */
    private function checkServiceExpire($idPartner,$szReferenceToken,$szServiceID)
    {
        $oneDayAgoTime = date('Y-m-d H:i:s', strtotime(' -1 day'));
        
        if(!empty($szServiceID) && !empty($szReferenceToken))
        {
            $query="
                    SELECT
                            id,
                            szReferenceToken,
                            szServiceID,
                            iShipmentType,
                            dtCreatedOn
                            
                    FROM
                            ".__DBC_SCHEMATA_PARTNERS_API_RESPONSE__."
                    WHERE
                            szServiceID='".mysql_escape_custom(trim($szServiceID))."'
                    AND
                            szReferenceToken='".mysql_escape_custom(trim($szReferenceToken))."'
                    AND
                            idPartner='".(int)$idPartner."'
                    AND
                            dtCreatedOn > '".$oneDayAgoTime."'
            ";
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }     
        }
        else
        {
            return false;
        }

    }
    /*
    * This function is check stored cargo Detail
    * if service ID is not older then one day
    * @return array
    * iShipmentType = 1 => Parcel
    * iShipmentType = 2 => Pallet
    * iShipmentType = 3 => Break Bulk
    * @author Whiz Solutions
    */
    private function validateCargoBookingPostData($data,$iShipmentType)
    {
        if(!empty($data))
        {
            $szReferenceToken = $data['token'];
            
            $idPartner = self::$idGlobalPartner;
            
            if((int)$iShipmentType == 3)
            {
                $cargoSelectFields="
                                    fTotalWeight,
                                    fTotalVolume,
                                ";
                
                $cargoWhereQuery="
                                    AND
                                            fTotalWeight='".(float)$data['cargo']['fTotalWeight']."'
                                    AND
                                            fTotalVolume='".(float)$data['cargo']['fTotalVolume']."'
                                ";
            }
            else
            {
                $cargoSelectFields="
                                    fLength,
                                    fWidth,
                                    fHeight,
                                    iQuantity,
                                    fWeight,
                                    szPalletType,
                                ";
                
                $cargoWhereQuery="
                                    AND
                                            fLength='".(float)$data['cargo']['iLength']."'
                                    AND
                                            fWidth='".(float)$data['cargo']['iWidth']."'
                                    AND
                                            fHeight='".(float)$data['cargo']['iHeight']."'
                                    AND
                                            fWeight='".(float)$data['cargo']['iWeight']."'
                                    AND
                                            iQuantity='".(int)$data['cargo']['iQuantity']."'
                                ";
                
                if((int)$iShipmentType == 2)
                {
                    $cargoWhereQuery.="
                                        AND 
                                            szPalletType='".mysql_escape_custom(trim($data['cargo']['szPalletType']))."'
                                    ";
                }
            }
            
            $query="
                    SELECT
                            id,
                            idPartner,
                            szReferenceToken,
                            $cargoSelectFields
                            iShipmentType,
                            idWeightMeasure,
                            idCargoMeasure,
                            szDimensionMeasure,
                            szWeightMeasure,
                            dtCreatedOn
                            
                    FROM
                            ".__DBC_SCHEMATA_PARTNERS_API_RESPONSE__."
                    WHERE
                            szReferenceToken='".mysql_escape_custom(trim($szReferenceToken))."'
                    AND
                            idPartner='".(int)$idPartner."'
                    AND
                            iShipmentType='".(int)$iShipmentType."'
                    AND
                            szDimensionMeasure='".mysql_escape_custom(trim(strtoupper($data['cargo']['szDimensionMeasure'])))."'
                    AND
                            szWeightMeasure='".mysql_escape_custom(trim(strtoupper($data['cargo']['szWeightMeasure'])))."'
                    $cargoWhereQuery
            ";
            if($result=$this->exeSQL($query))
            {
                if($this->getRowCnt()>0)
                {
                    $row = $this->getAssoc($result);
                    return $row;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
        else
        {
            return array();
        }
    }
    /*
    * This function is recalculate
    * cargo detail if cargo detail is
    * 24hrs old
    * @return array()
    * @author Whiz Solutions
    */
    private function recalculateCargoBookingData($postSearchAry,$serviceResponseAry,$idBooking)
    {
        if(!empty($postSearchAry))
        {
            $szServiceID = $postSearchAry['szServiceRefrence'];
            $idShipmentType = $serviceResponseAry['iShipmentType'];
            $iBookingType = $serviceResponseAry['iBookingType'];
            $idPackingType = $serviceResponseAry['idPackingType'];
            
            $kWHSSearch = new cWHSSearch(); 
            $kBooking = new cBooking(); 
            
            $tempResultAry = $kWHSSearch->getSearchedDataFromTempData(false,false,false,true);
            if(!empty($tempResultAry['szSerializeData']))
            {
                $searchResultAry = unserialize($tempResultAry['szSerializeData']);
                if(!empty($searchResultAry))
                {
                    $updateBookingAry = array();
                    $searchResultTempAry = array();
                    foreach($searchResultAry as $searchResultArys)
                    {
                        if($searchResultArys['unique_id'] == $szServiceID )
                        {
                            $updateBookingAry = $searchResultArys ;
                            $searchResultTempAry[0] = $searchResultArys ;
                            break;
                        }
                    }  
                    if(!empty($updateBookingAry))
                    { 
                        if($iBookingType==2)//Courier Bookings
                        {
                            /*
                            * Recalculate courier service prices
                            */ 
                            $postSearchAry['idServiceProvider'] = $updateBookingAry['idCourierAgreement'];
                            $postSearchAry['idServiceProviderProduct'] = $updateBookingAry['idCourierProviderProductid']; 
                            $postSearchAry['idForwarder'] = $updateBookingAry['idForwarder'];
                            $postSearchAry['fTotalPriceUSD'] = $updateBookingAry['fBookingPriceUSD'];
                            $postSearchAry['idForwarderCurrency'] = $updateBookingAry['idForwarderCurrency'];
                            $postSearchAry['fTotalPriceCustomerCurrency'] = $updateBookingAry['fDisplayPrice'];
                            $postSearchAry['fTotalVat'] = $updateBookingAry['fTotalVat'];  
                            $postSearchAry['idShipperCountry'] = $postSearchAry['idOriginCountry'];
                            $postSearchAry['szShipperCity'] = $postSearchAry['szOriginCity'];
                            $postSearchAry['szShipperPostCode'] = $postSearchAry['szOriginPostCode'];  
                            $postSearchAry['idConsigneeCountry'] = $postSearchAry['idDestinationCountry'];  
                            $postSearchAry['szConsigneeCity'] = $postSearchAry['szDestinationCity'];
                            $postSearchAry['szConsigneePostCode'] = $postSearchAry['szDestinationPostCode'];
                            
                            if($this->iSearchParcel==1)
                            {
                                $postSearchAry['fCargoVolume'] = $this->fTotalCargoVolumeParcel;
                                $postSearchAry['fCargoWeight'] = $this->fTotalCargoWeightParcel;
                            }      
                            else if($this->iSearchPallet==1)
                            {
                                $postSearchAry['fCargoVolume'] = $this->fTotalCargoVolumePallet;
                                $postSearchAry['fCargoWeight'] = $this->fTotalCargoWeightPallet;
                            }
                            else if($this->iSearchBreakBulk==1)
                            {
                                $postSearchAry['fCargoVolume'] = $this->fTotalCargoVolumeBreakBulk;
                                $postSearchAry['fCargoWeight'] = $this->fTotalCargoWeightBreakBulk;
                            } 
                            if($idShipmentType==1)
                            {
                                self::$searchablePacketTypes = array();
                                self::$searchablePacketTypes[] = 1;
                                self::$searchablePacketTypes[] = 3;
                                self::$idGlobalShipmentType = 1;
                            }
                            else if($idShipmentType==2)
                            {
                                self::$searchablePacketTypes = array();
                                self::$searchablePacketTypes[] = 2;
                                self::$searchablePacketTypes[] = 3;
                                self::$idGlobalShipmentType = 2;
                            }
                            self::$GlobalpostSearchAry = $postSearchAry;
                            $kBooking->recalculateCourierPricing($idBooking,false,false,true);
                            self::$szDeveloperNotes .= $kBooking->szCargoLogString;  
                            
                            if($kBooking->iTolerablePrices==1)
                            {
                                return 'SUCCESS';
                            }
                            else if($kBooking->iDonotCalculateAgain==1)
                            {
                                return 'WARNING';
                            }
                            else
                            {
                                self::$szDeveloperNotes .= "> No response received while re-calculating Courier prices. ";
                                return 'ERROR';
                            } 
                        }
                        else
                        {
                            /*
                            * Recalculate lcl service prices 
                            */ 
                            
                            $kWhsSearch = new cWHSSearch();
                            $iOverWeightTolerance = $kWhsSearch->getManageMentVariableByDescription('__OVER_WEIGHT_MEASURE_TOLERANCE_WITHOUT_PRICE_ADJUSTMENT__'); 

                            $fTotalPriceCustomerCurrency = $updateBookingAry['fDisplayPrice']; 
                            $minTolerancePriceLevel = $fTotalPriceCustomerCurrency - ($fTotalPriceCustomerCurrency * $iOverWeightTolerance * .01) ;
                            $maxTolerancePriceLevel = $fTotalPriceCustomerCurrency + ($fTotalPriceCustomerCurrency * $iOverWeightTolerance * .01) ;

                            $this->szCargoLogString = '';
                            $this->szCargoLogString .= "<br> Overweight tolerance: ".$iOverWeightTolerance." %<br> ";
                            $this->szCargoLogString .= "<br>Booking Price Customer Currency: ".number_format((float)$fTotalPriceCustomerCurrency) ;
                            $this->szCargoLogString .= "<br>Min tolerance price (CC): ".number_format((float)$minTolerancePriceLevel);
                            $this->szCargoLogString .= "<br>Max tolerance price (CC): ".number_format((float)$maxTolerancePriceLevel);
                            $this->szCargoLogString .= "<h3> Step: 4 </h3>";
                            $this->szCargoLogString .= $kWHSSearch->szCourierCalculationLogString; 
                                
                            
                            $postSearchAry['idPricingWtw'] = $updateBookingAry['idWTW'];
                            $postSearchAry['iPartnerApiPriceRecalculate'] = 1;
                            $resultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,'RECALCULATE_PRICING',false,true);
                            
                            if(!empty($resultAry))
                            {
                                $fShipmentUSDCost = $resultAry[0]['fDisplayPrice']; 
                                $this->szCargoLogString .= "<br>Shipment cost (USD): ".number_format((float)$fShipmentUSDCost); 
                                self::$szDeveloperNotes .= $this->szCargoLogString; 
                                
                                if($fShipmentUSDCost>=$minTolerancePriceLevel && $fShipmentUSDCost<=$maxTolerancePriceLevel)
                                {
                                    /*
                                    * This means booking price is between tolerable level i.e +, - 3% of booking amount
                                    */
                                    return 'SUCCESS';
                                }
                                else
                                { 
                                    $res_ary=array();
                                    $updateBookingAry = $resultAry[0]; 
                                    
                                    if(!empty($updateBookingAry))
                                    {
                                        $res_ary = updatePricingChanges($updateBookingAry);
                                    }  
                                    if(!empty($res_ary))
                                    {
                                        foreach($res_ary as $key=>$value)
                                        {
                                            $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                                        }
                                    } 
                                    $update_query = rtrim($update_query,",");
                                    $kBooking->updateDraftBooking($update_query,$idBooking); 
                                    return 'WARNING'; 
                                }
                            }   
                            else
                            {
                                self::$szDeveloperNotes .= "> No response received while re-calculating LCL prices. ";
                                return 'ERROR';
                            } 
                        }
                    }
                    else
                    {
                        self::$szDeveloperNotes .= "> There is no price response data associated with given service ID ";
                        return "ERROR";
                    }
                }
                else
                {
                    self::$szDeveloperNotes .= "> There is no price response data associated with given service ID - Outer ";
                    return "ERROR";
                }
            }
            else
            {
                self::$szDeveloperNotes .= "> There is no price response data associated with given service ID - Most Outer ";
                return "ERROR";
            }
        }
        else
        {
            return "ERROR";
        }
    } 
    
    /*
    * Saving partner API data to database just to have an log
    * 
    */
    private function savePartnerApiData($data,$cargoDetailsAry)
    { 
        if(!empty($data))
        {
            $szClientUserAgent = self::$szClientUserAgent;
            $szUserAgent = self::$szUserAgent;  
            $szApiRequestMode = self::$szApiRequestMode;
            
            $szCustomerTypeStr = "";
            if(!empty($data['customerType']))
            {
                $szCustomerTypeStr = serialize($data['customerType']);
            }
            
            $query="
                INSERT INTO
                    ".__DBC_SCHEMATA_PARTNERS_API_CALL__."
                (
                    idPartner,
                    szRequestMode,
                    szSearchType,
                    idTimingType,
                    idServiceType,
                    szOriginCountryStr,
                    szDestinationCountryStr,
                    szCargoType,
                    iCustomerType,
                    iOriginCC,
                    iDestinationCC,
                    szReferenceToken,
                    szRequestType,
                    dtShipping,
                    szPacketType,
                    szServiceTerm,
                    szShipperCompanyName,
                    szShipperFirstName,
                    szShipperLastName,
                    szShipperEmail,
                    iShipperCountryDialCode,
                    szShipperPhoneNumber,
                    szShipperAddress,
                    szShipperCity,
                    szShipperPostCode,
                    szShipperCountry,
                    szShipperLattitude,
                    szShipperLongitude,
                    szConsigneeCompanyName,
                    szConsigneeFirstName,
                    szConsigneeLastName,
                    szConsigneeEmail,
                    iConsigneeCountryDialCode,
                    szConsigneePhoneNumber,
                    szConsigneeAddress,
                    szConsigneeCity,
                    szConsigneePostCode,
                    szConsigneeCountry,
                    szConsigneeLattitude,
                    szConsigneeLongitude,
                    idCurrency,
                    szCurrency,
                    fExchangeRate,
                    fTotalVolume,
                    fTotalWeight,
                    fCargoValue,
                    szGoodsInsuranceCurrency,
                    idGoodsInsuranceCurrency,
                    fGoodsInsuranceExchangeRate,
                    szCargoDescription, 
                    szServiceRefrence,
                    iInsuranceFlag,
                    iSearchBreakBulk,
                    iSearchParcel,
                    iSearchPallet,
                    szClientUserAgent,
                    szUserAgent,  
                    szCustomerType,
                    szStripeToken,
                    szPaymentType,
                    dtCreatedOn,
                    szBillingCompanyName,
                    szBillingFirstName,
                    szBillingLastName,
                    iBillingCountryDialCode,
                    iBillingPhoneNumber,
                    szBillingEmail,
                    szBillingAddress,
                    szBillingPostcode,
                    szBillingCity,
                    szBillingCountry,
                    iShipperConsigneeType,
                    iDonotKnowShipperPostcode,
                    iDonotKnowConsigneePostcode,
                    iBookingLanguage,
                    iShipmentType,
                    iVogaSearchAutomatic,
                    iSearchStandardCargo,
                    idDefaultLandingPage,
                    iSearchMiniVesion,
                    szInternalComments,
                    iCargoDescriptionLocked,
                    iConsigneeDetailsLocked,
                    iShipperDetailsLocked,
                    iCreateAutomaticQuote,
                    szAutomatedRfqResponseCode,
                    iRemovePriceGuarantee,
                    iPaymentTypePartner
                )
                VALUE
                (
                    '".(int)$data['idPartner']."', 
                    '".mysql_escape_custom(trim($szApiRequestMode))."',
                    '".mysql_escape_custom(trim($data['szSearchType']))."', 
                    '".(int)$data['idTimingType']."', 
                    '".(int)$data['idServiceType']."', 
                    '".mysql_escape_custom(trim($data['szQuoteOriginCountryStr']))."', 
                    '".mysql_escape_custom(trim($data['szQuoteDestinationCountryStr']))."', 
                    '".mysql_escape_custom(trim($data['szCargoType']))."',
                    '".(int)$data['iCustomerType']."', 
                    '".(int)$data['iOriginCC']."', 
                    '".(int)$data['iDestinationCC']."',  
                    '".mysql_escape_custom(trim($data['szReferenceToken']))."',
                    '".mysql_escape_custom(trim($data['szMethod']))."',
                    '".mysql_escape_custom($data['dtShipping'])."',
                    '".mysql_escape_custom(trim($data['szPacketType']))."',
                    '".mysql_escape_custom(trim($data['szServiceTerm']))."',
                    '".mysql_escape_custom(trim($data['szShipperCompanyName']))."',
                    '".mysql_escape_custom(trim($data['szShipperFirstName']))."',
                    '".mysql_escape_custom(trim($data['szShipperLastName']))."',
                    '".mysql_escape_custom(trim($data['szShipperEmail']))."',
                    '".mysql_escape_custom(trim($data['iShipperCounrtyDialCode']))."',
                    '".mysql_escape_custom(trim($data['iShipperPhoneNumber']))."',
                    '".mysql_escape_custom(trim($data['szShipperAddress']))."',
                    '".mysql_escape_custom(trim($data['szOriginCity']))."',
                    '".mysql_escape_custom(trim($data['szOriginPostCode']))."',
                    '".mysql_escape_custom(trim($data['idOriginCountry']))."',
                    '".mysql_escape_custom(trim($data['fOriginLatitude']))."',
                    '".mysql_escape_custom(trim($data['fOriginLongitude']))."',
                    '".mysql_escape_custom(trim($data['szConsigneeCompanyName']))."',
                    '".mysql_escape_custom(trim($data['szConsigneeFirstName']))."',
                    '".mysql_escape_custom(trim($data['szConsigneeLastName']))."',
                    '".mysql_escape_custom(trim($data['szConsigneeEmail']))."',
                    '".mysql_escape_custom(trim($data['iConsigneeCounrtyDialCode']))."',
                    '".mysql_escape_custom(trim($data['iConsigneePhoneNumber']))."',
                    '".mysql_escape_custom(trim($data['szConsigneeAddress']))."',
                    '".mysql_escape_custom(trim($data['szDestinationCity']))."',
                    '".mysql_escape_custom(trim($data['szDestinationPostCode']))."',
                    '".mysql_escape_custom(trim($data['idDestinationCountry']))."',
                    '".mysql_escape_custom(trim($data['fDestinationLatitude']))."',
                    '".mysql_escape_custom(trim($data['fDestinationLongitude']))."',
                    '".(int)$data['idCurrency']."',
                    '".mysql_escape_custom(trim($data['szCurrency']))."',
                    '".(float)$data['fExchangeRate']."',
                    '".(float)$data['fCargoVolume']."',
                    '".(float)$data['fCargoWeight']."',
                    '".mysql_escape_custom($data['fCargoValue'])."',
                    '".mysql_escape_custom(trim($data['szGoodsInsuranceCurrency']))."',
                    '".(int)$data['idGoodsInsuranceCurrency']."',
                    '".(float)$data['fGoodsInsuranceExchangeRate']."',
                    '".mysql_escape_custom(trim($data['szCargoDescription']))."', 
                    '".mysql_escape_custom(trim($data['szServiceRefrence']))."',
                    '".mysql_escape_custom(trim($data['iInsuranceFlag']))."',
                    '".mysql_escape_custom(trim($data['iSearchBreakBulk']))."',
                    '".mysql_escape_custom(trim($data['iSearchParcel']))."',
                    '".mysql_escape_custom(trim($data['iSearchPallet']))."',
                    '".mysql_escape_custom(trim($szClientUserAgent))."',
                    '".mysql_escape_custom(trim($szUserAgent))."', 
                    '".mysql_escape_custom(trim($szCustomerTypeStr))."', 
                    '".mysql_escape_custom(trim($data['szStripeToken']))."',
                    '".mysql_escape_custom(trim($data['szPaymentType']))."',
                    NOW(),
                    '".mysql_escape_custom(trim($data['szBillingCompanyName']))."',
                    '".mysql_escape_custom(trim($data['szBillingFirstName']))."',
                    '".mysql_escape_custom(trim($data['szBillingLastName']))."',
                    '".mysql_escape_custom(trim($data['iBillingCountryDialCode']))."',
                    '".mysql_escape_custom(trim($data['iBillingPhoneNumber']))."',
                    '".mysql_escape_custom(trim($data['szBillingEmail']))."',
                    '".mysql_escape_custom(trim($data['szBillingAddress']))."',
                    '".mysql_escape_custom(trim($data['szBillingPostcode']))."',
                    '".mysql_escape_custom(trim($data['szBillingCity']))."',
                    '".mysql_escape_custom(trim($data['idBillingCountry']))."',
                    '".mysql_escape_custom(trim($data['iShipperConsigneeType']))."',
                    '".mysql_escape_custom(trim($data['iDonotKnowShipperPostcode']))."' ,
                    '".mysql_escape_custom(trim($data['iDonotKnowConsigneePostcode']))."',
                    '".mysql_escape_custom(trim($data['iBookingLanguage']))."',
                    '".mysql_escape_custom(trim($data['iBookingQuoteShipmentType']))."',
                    '".mysql_escape_custom(trim($data['iVogaSearchAutomatic']))."',
                    '".mysql_escape_custom(trim($data['iSearchStandardCargo']))."',
                    '".mysql_escape_custom(trim($data['idDefaultLandingPage']))."',
                    '".  mysql_escape_custom($data['iSearchMiniVesion'])."',
                    '".  mysql_escape_custom($data['szInternalComments'])."',
                    '".(int)$data['iCargoDescriptionLocked']."',
                    '".(int)$data['iConsigneeDetailsLocked']."',
                    '".(int)$data['iShipperDetailsLocked']."',
                    '".(int)$data['iCreateAutomaticQuote']."',
                    '".mysql_escape_custom($data['szAutomatedRfqResponseCode'])."',
                    '".(int)$data['iRemovePriceGuarantee']."',
                    '".(int)$data['iPaymentTypePartner']."'    
                )
            ";  
           //echo "<br />".$query."<br>" ;
            //die; 
            if($result = $this->exeSQL($query))
            {
                $idPartnerApi = $this->iLastInsertID ;  
                if($this->savePartnerApiCargoData($cargoDetailsAry,$idPartnerApi))
                {

                }
                return true;

            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
        }
        else
        {
            return false;
        }
    }
    /*
     * 
     * 
     */
    public function savePartnerApiCargoData($cargodetailAry,$idPartnerApi)
    {  
        if(!empty($this->szPacketType))
        {
            $counter =1; 
            $idPartner = self::$idGlobalPartner;
            $szReferenceToken = self::$szGlobalToken; 
            foreach($this->szPacketType as $key=>$cargoDetail)
            {    
                
                if(strtoupper($this->szPacketType[$key])==__PACKET_TYPE_PARCEL__) 
                {
                    $iShipmentType = 1;
                }
                else if(strtoupper($this->szPacketType[$key])==__PACKET_TYPE_PALLET__)
                {
                    $iShipmentType = 2;
                }
                else if(strtolower($this->szPacketType[$key]) == strtolower(__PACKET_TYPE_UNKNOWN__)) 
                {
                    $iShipmentType = 4;
                }
                else if(strtolower($this->szPacketType[$key]) == strtolower(__PACKET_TYPE_STANDARD_CARGO__)) 
                {
                    $iShipmentType=5;
                }
                else
                {
                    $iShipmentType = 3;
                }
                
                                
                $cargodetailAry['iLength'][$key] = $this->iLength[$key];
                $cargodetailAry['iHeight'][$key] = $this->iHeight[$key];
                $cargodetailAry['iWidth'][$key] = $this->iWidth[$key];
                $cargodetailAry['iWeight'][$key] = $this->iWeight[$key]; 
                $cargodetailAry['iQuantity'][$key] = $this->iQuantity[$key];  
                $cargodetailAry['szPalletType'][$key] = $this->szPalletType[$key]; 
                $cargodetailAry['szDimensionMeasure'][$key] = $this->szDimensionMeasure[$key]; 
                $cargodetailAry['szWeightMeasure'][$key] = $this->szWeightMeasure[$key];  
                $cargodetailAry['idWeightMeasure'][$key] = $this->idWeightMeasure[$key]; 
                $cargodetailAry['idDimensionMeasure'][$key] = $this->idDimensionMeasure[$key]; 
                if(strtolower($this->szPacketType[$key]) == strtolower(__PACKET_TYPE_STANDARD_CARGO__)) 
                {
                    $cargodetailAry['szCommodity'][$key] = $this->szCommodityStandCargo[$key];
                }
                else
                {
                    $cargodetailAry['szCommodity'][$key] = $this->szCommodity[$key];
                }
                $cargodetailAry['szSellersReference'][$key] = $this->szSellersReference[$key];
                 
                $cargodetailAry['fTotalWeight'][$key] = $this->fTotalWeight[$key]; 
                $cargodetailAry['fTotalVolume'][$key] = $this->fTotalVolume[$key]; 
                
                $counter++;

                $query = "
                    INSERT INTO
                        ".__DBC_SCHEMATA_PARTNERS_API_CARGO_DETAIL__."
                    (
                        idPartner,
                        szRequestType,
                        szReferenceToken, 
                        fLength,
                        fWidth,
                        fHeight,
                        iQuantity,
                        fWeight,
                        szPalletType,
                        fTotalWeight,
                        fTotalVolume,
                        iShipmentType,
                        idWeightMeasure,
                        idCargoMeasure,
                        szDimensionMeasure,
                        szWeightMeasure,
                        dtCreatedOn,
                        szCommodity,
                        szSellersReference
                    )
                    VALUE
                    (
                        '".(int)$idPartner."', 
                        '".mysql_escape_custom($this->szRequestType)."',
                        '".mysql_escape_custom($szReferenceToken)."',
                        '".(float)$cargodetailAry['iLength'][$key]."',
                        '".(float)$cargodetailAry['iWidth'][$key]."',
                        '".(float)$cargodetailAry['iHeight'][$key]."',
                        '".(int)$cargodetailAry['iQuantity'][$key]."',
                        '".(float)$cargodetailAry['iWeight'][$key]."',
                        '".mysql_escape_custom(trim($cargodetailAry['szPalletType'][$key]))."',
                        '".(float)$cargodetailAry['fTotalWeight'][$key]."',
                        '".(float)$cargodetailAry['fTotalVolume'][$key]."',
                        '".$iShipmentType."',
                        '".$cargodetailAry['idWeightMeasure'][$key]."',
                        '".$cargodetailAry['idDimensionMeasure'][$key]."', 
                        '".mysql_escape_custom(trim($cargodetailAry['szDimensionMeasure'][$key]))."',
                        '".mysql_escape_custom(trim($cargodetailAry['szWeightMeasure'][$key]))."',
                        NOW(),
                        '".mysql_escape_custom($cargodetailAry['szCommodity'][$key])."',
                        '".mysql_escape_custom($cargodetailAry['szSellersReference'][$key])."'    
                    )
                ";
                if($result = $this->exeSQL($query))
                {

                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
            return true;
        }
        else
        {
            return false;
        }
    }
    /*
    * 
    */
    private function checkAuthorizedPartner($szApiKey,$szApiMode)
    {
        /*
        if($szApiMode=='LIVE')
        {
            $query_and = " AND szLiveApiKey = '".mysql_escape_custom(trim($szApiKey))."' ";
        }
        else
        {
            $query_and = " AND szApiKey = '".mysql_escape_custom(trim($szApiKey))."' ";
        }
        */
        $query_and = " AND szApiKey = '".mysql_escape_custom(trim($szApiKey))."' ";
            
        $query="
            SELECT 
                id, 
                idCustomer,
                szApiKey,
                szPartnerAlies,
                dtCreatedOn,
                dtUpdatedOn,
                iActive,
                isDeleted
            FROM	
                ".__DBC_SCHEMATA_PARTNERS__."
            WHERE 
                isDeleted = '0'
            AND
                iActive = '1'
            $query_and
        ";
//        echo "<br>".$query."<br>"; 
//        die;
        if($result=$this->exeSQL($query))
        {
            if($this->getRowCnt()>0)
            {
                $row = $this->getAssoc($result);
                return $row;
            }
            else
            {
                $this->buildErrorDetailAry("Unauthorized");
                return false;
            }
        } 
    }
    
    function validatePriceCalculationDate($dd,$mnth,$yyyy,$bLabel=false) 
    {
        $errorDetail = array();
        $longMonth = array();
        
        $longMonth[0] = 1;
        $longMonth[1] = 3;
        $longMonth[2] = 5;
        $longMonth[3] = 7;
        $longMonth[4] = 8;
        $longMonth[5] = 10;
        $longMonth[6] = 12;

        if($bLabel)
        {
            $szFieldKey_Day = "dtPickUpDay"; 
        }
        else
        {
            $szFieldKey_Day = "dtShippingDay";
        }
        $iValidationError = false;
        if($mnth == 2 && $dd > 28) 
        {
            $isLeapYear = date('L', strtotime($yyyy."-".$mnth."-".$dd)); 
            if((int)$isLeapYear == 0)
            {
                $iValidationError = true;
                $this->buildErrorDetailAry($szFieldKey_Day,"INVALID");
            }
        } 
        if($dd == 31 && !in_array($mnth, $longMonth))
        {
            $iValidationError = true;
            $this->buildErrorDetailAry($szFieldKey_Day,"INVALID");
        }  
        return $iValidationError;
    }
    
    /*
    * 
    */
    private function validateParams($data,$szMethod=false,$mode=false)
    {
        if(!empty($data))
        {   
            $bMethodTypePrice = true;
            $bMethodTypeBooking = false; 
            $bMethodTypeQuoteOrBooking = false;
            $bCheckBillingCountry = true;
            $bCheckPriceForPostcodeFlag = false;
            $bCheckCargoLineFlag = true;
            if($szMethod == "BOOKING")
            {
                $bMethodTypePrice = false;
                if($mode=='QUICK_QUOTE')
                {
                    $bMethodTypeBooking = false; 
                    $bMethodTypeQuoteOrBooking = false;
                }
                else
                {
                    $bMethodTypeBooking = true;
                    $bMethodTypeQuoteOrBooking = true;
                    $bCheckPriceForPostcodeFlag = true; 
                } 
                $this->szRequestType = 'booking'; 
                if(empty($data['szPaymentType']))
                {
                    $data['szPaymentType'] = __TRANSPORTECA_PAYMENT_TYPE_3__;
                    $data['iPaymentTypePartner'] = 1;
                }
            }
            else if($szMethod == "VALIDATE_PRICE")
            {
                $this->szRequestType = 'validatePrice';
                $bCheckPriceForPostcodeFlag = true;
            }
            else
            {
                $this->szRequestType = 'price';
                $bCheckPriceForPostcodeFlag = true;
            }   
            if($mode=='QUICK_QUOTE')
            {
                if($data['iPrivateShipping']==1)
                {
                    $data['customerType'][0] = __PARTNER_API_CUSTOMER_TYPE_PRIVATE__;
                }
                else
                {
                    $data['customerType'][0] = __PARTNER_API_CUSTOMER_TYPE_BUSINESS__;
                }
            }
            else
            {
                if(empty($data['customerType']))
                {
                    $data['customerType'][0] = __PARTNER_API_CUSTOMER_TYPE_BUSINESS__;
                }
            } 
            if(empty($data['szCurrency']))
            {
                $data['szCurrency'] = 'USD';
            } 
            if(!empty($data['szLanguage']))
            {
                $langArr = array();
                $kConfig =new cConfig();
                $langArr = $kConfig->getLanguageDetails(strtolower($data['szLanguage']));
                if(!empty($langArr))
                {
                    $data['szLanguage'] = strtoupper($langArr[0]['szLanguageCode']); 
                }
                else
                {
                    $data['szLanguage'] = 'EN'; 
                }
            }
            else
            {
                $data['szLanguage'] = 'EN'; 
            }
            
            $iCheckShipperCityName=true;
            $iCheckConsigneeCityName=true;
            if($szMethod == "VALIDATE_PRICE")
            {
                if((int)$data['iShipperDetailsLocked']==1)
                {
                    $iCheckShipperCityName=false;
                }                
                if((int)$data['iConsigneeDetailsLocked']==1)
                {
                    $iCheckConsigneeCityName=false;
                }
            }
            
            

            $iShipperPostcodeRequiredFlag=true;
            $iConsigneePostcodeRequiredFlag=true;
            $iConsigneeAddressRequiredFlag=true;
            $iShipperAddressRequiredFlag=true;
            
            $bConfirmedManualBooking = false;
            $bConfirmedAutomaticBooking = false;
             
             
            $szServiceID = trim($data['szServiceID']);
            $szQuotePriceKey = trim($data['szQuoteID']);
                
            if(!empty($szQuotePriceKey))
            {
                $bConfirmedManualBooking = true;
                $szServiceRefrence = $szQuotePriceKey;
                $this->szQuoteID = $szQuotePriceKey; 
                $kQuote = new cQuote();
                $bookingQuoteAry = array();
                $bookingQuoteAry = $kQuote->getQuotePricingIDbyKey($szQuotePriceKey);
                $idBookingQuotePricing = $bookingQuoteAry['id']; 
                $idBooking = $bookingQuoteAry['idBooking'];                
                
            }
            else
            {
                $bConfirmedAutomaticBooking = true;
                $szServiceRefrence = $szServiceID; 
                if($szMethod == "BOOKING" && $mode!='QUICK_QUOTE')
                {
                    $szToken=trim($data['token']);
                    $bookingDataAutomaticBookingsAry = array();
                    $kService = new cServices();
                    $bookingDataAutomaticBookingsAry = $kService->getBookingDetailsByPriceToken($szToken);
                    $idBooking = $bookingDataAutomaticBookingsAry['id'];  
                }
            }
            
            

            $this->set_szServiceRefrence(trim(sanitize_all_html_input($szServiceRefrence)),$bMethodTypeBooking); 
            if($bMethodTypeBooking)
            {   
                $idPartner = self::$idGlobalPartner;    

                if($bConfirmedAutomaticBooking)
                {  
                    $serviceResponseAry = array();
                    $serviceResponseAry = $this->getServiceResponseDetails($idPartner,$szReferenceToken,$szServiceID); 

                    if(!empty($serviceResponseAry))
                    {
                        if(strtolower($serviceResponseAry['szShipperPostcodeRequired'])=='no')
                        {
                            $iShipperPostcodeRequiredFlag=false;
                        }

                        if(strtolower($serviceResponseAry['szConsigneePostcodeRequired'])=='no')
                        {
                            $iConsigneePostcodeRequiredFlag=false;
                        }

                        if(strtolower($serviceResponseAry['szCollection'])=='no')
                        {
                            $iShipperAddressRequiredFlag=false;
                        }

                        if(strtolower($serviceResponseAry['szDelivery'])=='no')
                        {
                            $iConsigneeAddressRequiredFlag=false;
                        }
                    }
                } 
            }
             
            $predefinedServiceIdAry = $this->predefinedServiceIDForQuote;
             
            if(in_array($szServiceID, $predefinedServiceIdAry))
            {
                $bMethodTypeQuoteOrBooking = false; 
                
            }
            /*
            * Function For
            * Shipping Validations
            */ 
            if(empty($data['dtDay']) || empty($data['dtMonth']) || empty($data['dtYear']))
            {
                /*
                 * If date is empty then 
                 */
                $this->dtShippingDay = date("d");
                $this->dtShippingMonth = date("m");
                $this->dtShippingYear = date("Y");
                $bDefaultShipmentDate = true;
            }
            else
            {
                $this->dtShippingDay = trim(sanitize_all_html_input($data['dtDay']));
                $this->dtShippingMonth = trim(sanitize_all_html_input($data['dtMonth']));
                $this->dtShippingYear = trim(sanitize_all_html_input($data['dtYear']));
                $bDefaultShipmentDate = false;
            }
            
            //$this->set_dtShippingDay(trim(sanitize_all_html_input($data['dtDay'])));
            //$this->set_dtShippingMonth(trim(sanitize_all_html_input($data['dtMonth'])));
            //$this->set_dtShippingYear(trim(sanitize_all_html_input($data['dtYear']))); 
            $this->set_szConsigneeCountry(trim(sanitize_all_html_input($data['szConsigneeCountry']))); 
            $this->set_szShipperCountry(trim(sanitize_all_html_input($data['szShipperCountry'])));
            $this->szLanguage = $data['szLanguage']; 
            $this->szQuoteOriginCountryStr = $data['szOriginCountryStr']; 
            $this->szQuoteDestinationCountryStr = $data['szDestinationCountryStr']; 
            $this->iCustomerType = $data['iCustomerType']; 
            $this->szCustomerToken = sanitize_all_html_input($data['szCustomerToken']); 
            $this->iRemovePriceGuarantee=(int)$data['iRemovePriceGuarantee'];
            if(!empty($data['customerType']))
            {
                foreach($data['customerType'] as $customerType)
                {
                    $customerType = strtoupper($customerType);
                    if($customerType==__PARTNER_API_CUSTOMER_TYPE_BUSINESS__ || $customerType==__PARTNER_API_CUSTOMER_TYPE_PRIVATE__)
                    {
                        $this->customerType[] = $customerType;  
                    } 
                    else
                    {
                        $this->buildErrorDetailAry('szCustomerType','INVALID',$counter);
                    }
                }
            } 
             
            if(!empty($langArr))
            {
                $this->iBookingLanguage = $langArr[0]['id'];
            }
            else
            {
                $this->iBookingLanguage = 1;
            } 
            if(!empty($data['szShipperCountry']) && !empty($data['szConsigneeCountry']))
            {
                $kConfig = new cConfig();
                $kConfig->loadCountry(false,$data['szShipperCountry']); 
                if($kConfig->idCountry>0)
                {
                    $this->idShipperCountry = $kConfig->idCountry;
                    $this->szShipperCountryName = $kConfig->szCountryName;
                } 
                else
                {   
                    $errorFlag = true;
                    $this->buildErrorDetailAry('szShipperCountry','INVALID');  
                } 
                $kConfig = new cConfig();
                $kConfig->loadCountry(false,$data['szConsigneeCountry']); 
                if($kConfig->idCountry>0)
                {
                    $this->idConsigneeCountry = $kConfig->idCountry;
                    $this->szConsigneeCountryName = $kConfig->szCountryName;
                } 
                else
                { 
                    $errorFlag = true;
                    $this->buildErrorDetailAry('szConsigneeCountry','INVALID');  
                } 
                $iCheckCustomerClearanceFlag=false;
                $kCourierServices = new cCourierServices();    
                if(!$kCourierServices->checkFromCountryToCountryExistsForCC($this->idShipperCountry,$this->idConsigneeCountry))
                {
                    $iCheckCustomerClearanceFlag=true;
                }
                $kCourierServices = new cCourierServices();    
                if($kCourierServices->checkFromCountryToCountryExists($this->idShipperCountry,$this->idConsigneeCountry,true))
                { 
                    $this->iDTDTrades = 1;
                }
                else
                {
                    $this->iDTDTrades = 0;
                }
            }  
            if(!$bConfirmedManualBooking)
            {
                $kConfig = new cConfig(); 
                $cargodetailAry = $data['cargo'];   
                if(!empty($cargodetailAry))
                { 
                    /*
                    * Function For
                    * Dimensions Validations
                    */
                    $kWHSSearch=new cWHSSearch();  
                    $arrDescriptions = array("'__MAX_CARGO_DIMENSION_LENGTH__'","'__MAX_CARGO_DIMENSION_WIDTH__'","'__MAX_CARGO_DIMENSION_HEIGHT__'","'__MAX_CARGO_WEIGHT__'","'__MAX_CARGO_QUANTITY__'","'__MAX_CARGO_VOLUME__'"); 
                    $bulkManagemenrVarAry = array();
                    $bulkManagemenrVarAry = $kWHSSearch->getBulkManageMentVariableByDescription($arrDescriptions);      

                    $maxCargoQty = $bulkManagemenrVarAry['__MAX_CARGO_QUANTITY__'];
                    $maxCargoHeight = $bulkManagemenrVarAry['__MAX_CARGO_DIMENSION_HEIGHT__'];
                    $maxCargoLength = $bulkManagemenrVarAry['__MAX_CARGO_DIMENSION_LENGTH__'];
                    $maxCargoWidth = $bulkManagemenrVarAry['__MAX_CARGO_DIMENSION_WIDTH__'];
                    $maxCargoWeight = $bulkManagemenrVarAry['__MAX_CARGO_WEIGHT__'];
                    $maxCargoDimension = $bulkManagemenrVarAry['__MAX_CARGO_DIMENSION__'];

                    $ctr=0;
                    $counter =1;   
                    $fTotalCargoVolume = 0;
                    $fTotalCargoWeight = 0; 
                    $iStopLclSearch = 0;
                    $iStopCourierSearch = 0;
                    $szDeveloperNotes = ''; 

                    $fTotalCargoVolumeBreakBulk = 0;
                    $fTotalCargoWeightBreakBulk = 0; 
                    $fTotalCargoVolumeParcel = 0;
                    $fTotalCargoWeightParcel = 0; 
                    $fTotalCargoVolumePallet = 0;
                    $fTotalCargoWeightPallet = 0;

                    $fTotalWeightStandardCargo=0;
                    $fTotalVolumeStandardCargo=0;

                    $iNumEuroPallet = 0;
                    $iNumHalfPallet = 0;
                    $iNumOthers = 0; 
                    foreach($cargodetailAry as $cargodetailArys)
                    { 
                        $this->set_szPacketType(sanitize_all_html_input($cargodetailArys['szShipmentType']),$counter,false,$key); 
                        $szCommodity='';
                        $bParcel = false;
                        $bPallet = false;
                        $bBreakBulk = false; 

                        if((strtolower($this->szPacketType[$counter]) == strtolower(__PACKET_TYPE_PARCEL__)) || (strtolower($this->szPacketType[$counter]) == strtolower(__PACKET_TYPE_PALLET__)))
                        {
                            if(strtolower($this->szPacketType[$counter])== strtolower(__PACKET_TYPE_PALLET__))
                            {
                                $bPallet = true; 
                                $this->iSearchPallet = 1;
                            }
                            else
                            { 
                                $bParcel = true; 
                                $this->iSearchParcel = 1;
                            }

                            $iLength = format_numeric_vals($cargodetailArys['iLength']);
                            $iWidth = format_numeric_vals($cargodetailArys['iWidth']);
                            $iHeight = format_numeric_vals($cargodetailArys['iHeight']); 
                            $iWeight = format_numeric_vals($cargodetailArys['iWeight']);  
                            $iQuantity = format_numeric_vals($cargodetailArys['iQuantity']);
                            $szCommodity =trim($cargodetailArys['szCommodity']);

                            $iQuantity = ceil($iQuantity);
                            $this->set_szPalletType(sanitize_all_html_input($cargodetailArys['szPalletType']),$counter,false,$parcel_key,$bPallet); 
                            if($bParcel)
                            {
                                if($this->iDTDTrades==1 && ($data['szServiceTerm']=='CFR' || $data['szServiceTerm']=='FOB'))
                                {
                                    /*
                                     * If trading terms is “CFR” or "FOB" (in this case, do not run search if trade is defined in http://management.transporteca.com/dtdTrades/)
                                     */
                                    $this->iStopLclSearchParcel = 1;
                                    $szDeveloperNotes .= "<br><br>Status: PARCEL, stopped LCL search";
                                    $szDeveloperNotes .= "<br>Description: Trade is defined under /dtdTrades/ and service term is: ".$data['szServiceTerm'];
                                }
                            }
                            else if($bPallet)
                            {
                                /*
                                *  a. Pallet length 
                                       If empty, use 120 cm if pallet type “Euro” 
                                       If empty, use 80 cm if pallet type “Half”
                                   b. Pallet width 
                                       If empty, use 80 cm if pallet type “Euro” 
                                       If empty, use 60 cm if pallet type “Half” 
                                */
                                $kQuote = new cQuote();
                                $palletDetailAry = $kQuote->getPalletDetails(false,false,$this->szPalletType[$counter]); 
                                if((float)$iLength<=0)
                                {
                                    $iLength = $palletDetailAry['fDefaultLength'];
                                }
                                if((float)$iWidth<=0)
                                {
                                    $iWidth = $palletDetailAry['fDefaultWidth'];
                                } 
                                $szCommodity = $palletDetailAry['szLongName']; 

                                /*
                                 * Previously we were picking these values from constants but now we are getting this from data base
                                if($this->szPalletType[$counter]==__PALLET_TYPE_EURO__)
                                { 
                                    if((float)$iLength<=0)
                                    {
                                        $iLength = 120;
                                    }
                                    if((float)$iWidth<=0)
                                    {
                                        $iWidth = 80;
                                    } 
                                }
                                else if($this->szPalletType[$counter]==__PALLET_TYPE_HALF__)
                                {
                                    if((float)$iLength<=0)
                                    {
                                        $iLength = 80;
                                    }
                                    if((float)$iWidth<=0)
                                    {
                                        $iWidth = 60;
                                    } 
                                } 
                                 * 
                                 */
                                if($this->iDTDTrades==1)
                                {
                                    if($this->szPalletType[$counter]==__PALLET_TYPE_OTHER__)
                                    {
                                        if($iLength<=80 && $iWidth<=60)
                                        {
                                            $this->szPalletType[$counter] = __PALLET_TYPE_HALF__;
                                        }
                                        else if($iLength<=60 && $iWidth<=80)
                                        {
                                            $this->szPalletType[$counter] = __PALLET_TYPE_HALF__;
                                        }
                                        else if($iLength<=120 && $iWidth<=80)
                                        {
                                            $this->szPalletType[$counter] = __PALLET_TYPE_EURO__;
                                        }
                                        else if($iLength<=80 && $iWidth<=120)
                                        {
                                            $this->szPalletType[$counter] = __PALLET_TYPE_EURO__;
                                        } 
                                    }  
                                    if($this->szPalletType[$counter]==__PALLET_TYPE_OTHER__)
                                    {
                                        /*
                                        *  If Pallet Type (after above changes) = “other”, then stop (we do not search LCL Services)
                                        */
                                        $this->iStopLclSearchPallet = 1;
                                        $szDeveloperNotes .= "<br><br>Status: PALLET, stopped searching LCL Services";
                                        $szDeveloperNotes .= "<br>Description: Trade is defined under /dtdTrades/ and after all changes Pallet type is 'Other' ";
                                    }
                                    else 
                                    {
                                        //echo "<br> Length: ".$iLength." Width: ".$iWidth." Type: ".$this->szPalletType[$counter];
                                        if($this->szPalletType[$counter]==__PALLET_TYPE_HALF__)
                                        {
                                            if($iLength>80 || $iWidth>80)
                                            {
                                                /*
                                                 * Pallet Length OR Width > 80, then change “Half” to “Euro” 
                                                 */
                                                $this->szPalletType[$counter] = __PALLET_TYPE_EURO__;
                                            }
                                            else
                                            {
                                                /*
                                                 * If Minimum(Pallet Length;Pallet Width) >= 60, then change “Half” to “Euro”
                                                 */
                                                $fMiniDimension = $iLength;
                                                if($iWidth<$fMiniDimension)
                                                {
                                                    $fMiniDimension = $iWidth;
                                                } 
                                                if($fMiniDimension>60)
                                                {
                                                    $this->szPalletType[$counter] = __PALLET_TYPE_EURO__;
                                                }
                                            }
                                        }

                                        if($this->szPalletType[$counter]==__PALLET_TYPE_EURO__)
                                        {
                                            if($iLength>120 || $iWidth>120)
                                            {
                                                $this->iStopLclSearchPallet = 1; 
                                                $szDeveloperNotes .= "<br><br>Status: PALLET, stopped searching LCL Services";
                                                $szDeveloperNotes .= "<br>Description: Trade is defined under /dtdTrades/ and Pallet type: Euro and pallet Length OR Width > 120 ";
                                                $szDeveloperNotes .= "<br>Description: ";
                                            }
                                            else
                                            {

                                                $fMiniDimension = $iLength;
                                                if($iWidth<$fMiniDimension)
                                                {
                                                    $fMiniDimension = $iWidth;
                                                }
                                                if($fMiniDimension>80)
                                                {
                                                    $this->iStopLclSearchPallet = 1; 
                                                    $this->szPalletType[$counter] = __PALLET_TYPE_OTHER__;
                                                    $szDeveloperNotes .= "<br><br>Status: PALLET, stopped searching LCL Services";
                                                    $szDeveloperNotes .= "<br>Description: Trade is defined under /dtdTrades/ and Pallet type: Euro and pallet Minimum(Length;Width) > 80 "; 
                                                }
                                            }
                                        }
                                    }    
                                }
                                else
                                { 
                                    $maxCargoHeight = 220;
                                    if($iHeight>$maxCargoHeight)
                                    {
                                        /*
                                        *  if pallet height is larger than 220 and trade is not door-to-door trade, we do not give any LCL results. 
                                        */
                                        $this->iStopLclSearchPallet = 1;
                                        $szDeveloperNotes .= "<br><br>Status: PALLET, stopped searching LCL Services";
                                        $szDeveloperNotes .= "<br>Description: Pallet height is larger than 220 and trade is not door-to-door trade, we do not search for LCL services ";
                                    }
                                } 
                            }

                            $this->set_iLength(sanitize_all_html_input($iLength),$counter,$maxCargoLength,$parcel_key);  
                            $this->set_iWidth(sanitize_all_html_input($iWidth),$counter,$maxCargoWidth,$parcel_key);  
                            $this->set_iHeight(sanitize_all_html_input($iHeight),$counter,false,$parcel_key);  
                            $this->set_iQuantity(sanitize_all_html_input($iQuantity),$counter,$maxCargoQty,$parcel_key);  
                            $this->set_iWeight(sanitize_all_html_input($iWeight),$counter,$maxCargoWeight,$parcel_key);    
                            $this->set_szDimensionMeasure(sanitize_all_html_input($cargodetailArys['szDimensionMeasure']),$counter,false,$parcel_key); 
                            $this->set_szWeightMeasure(sanitize_all_html_input($cargodetailArys['szWeightMeasure']),$counter,false,$parcel_key);  
                            $this->szCommodity[$counter]=$szCommodity;
                            if(!empty($this->szWeightMeasure[$counter]))
                            {
                                $weightMeasureAry = $kConfig->isWeightMeasureExists($this->szWeightMeasure[$counter]);
                                if($weightMeasureAry['id']>0)
                                {
                                    $this->idWeightMeasure[$counter] = $weightMeasureAry['id'];
                                    $this->fKgFactor[$counter] = $weightMeasureAry['fKgFactor'];
                                }
                                else
                                {
                                    $this->buildErrorMessage('szWeightMeasure','INVALID',$counter);
                                }
                            } 
                            if(!empty($this->szDimensionMeasure[$counter]))
                            {
                                $cargoMeasureAry = array();
                                $cargoMeasureAry = $kConfig->isCargoMeasureExists($this->szDimensionMeasure[$counter]);
                                if($cargoMeasureAry['id']>0)
                                {
                                    $this->idDimensionMeasure[$counter] = $cargoMeasureAry['id'];
                                    $this->fCmFactor[$counter] = $cargoMeasureAry['fCmFactor'];
                                }
                                else
                                {
                                    $this->buildErrorMessage('szDimensionMeasure','INVALID',$counter);
                                }
                            } 
                            $fLineTotalVolume = 0;
                            $fLineTotalWeight = 0;

                            $fKgFactor = $this->fKgFactor[$counter];
                            if($fKgFactor>0)
                            {
                                $fLineTotalWeight = ($this->iWeight[$counter]/$fKgFactor) * $this->iQuantity[$counter]; 
                            } 
                            $fTotalCargoWeight += $fLineTotalWeight;

                            if($bPallet)
                            {
                                if($this->szPalletType[$counter]==__PALLET_TYPE_EURO__)
                                { 
                                    $iNumEuroPallet += $this->iQuantity[$counter];
                                }
                                else if($this->szPalletType[$counter]==__PALLET_TYPE_HALF__)
                                { 
                                    $iNumHalfPallet += $this->iQuantity[$counter];
                                }
                                else
                                {
                                    $iNumOthers += $this->iQuantity[$counter];
                                }
                            }

                            if($bPallet && $this->iDTDTrades==1)
                            {
                                if($this->szPalletType[$counter]==__PALLET_TYPE_EURO__)
                                {
                                    $bEuroPallets = true;
                                    $iEuroPalletQuantity += $this->iQuantity[$counter];
                                }
                                else if($this->szPalletType[$counter]==__PALLET_TYPE_HALF__)
                                {
                                    $bHalfPallets = true;
                                    $iHalfPalletQuantity += $this->iQuantity[$counter];
                                }
                                $iTotalQuantity += $this->iQuantity[$counter];
                            }
                            else
                            {  
                                $fCmFactor = $this->fCmFactor[$counter]; 
                                if($fCmFactor>0)
                                {
                                    $iLenght = $this->iLength[$counter]/$fCmFactor ;
                                    $iWidth = $this->iWidth[$counter]/$fCmFactor ;
                                    $iHeight = $this->iHeight[$counter]/$fCmFactor ;
                                } 
                                $fLineTotalVolume = ($iLenght/100) * ($iWidth/100) * ($iHeight/100) * $this->iQuantity[$counter];   
                                $fTotalCargoVolume += $fLineTotalVolume; 
                            }
                            if($bPallet)
                            {
                                $fTotalCargoVolumePallet += $fLineTotalVolume;
                                $fTotalCargoWeightPallet += $fLineTotalWeight;
                            }
                            else
                            {
                                $fTotalCargoVolumeParcel += $fLineTotalVolume;
                                $fTotalCargoWeightParcel += $fLineTotalWeight;
                            }
                            $counter++;  
                        }
                        else if(strtolower($this->szPacketType[$counter]) == strtolower(__PACKET_TYPE_BREAK_BULK__))
                        {
                            if($this->iDTDTrades==1)
                            {
                                /*
                                 * If trade defined in http://management.transporteca.com/dtdTrades/, then stop (no search)
                                 */
                                $this->iStopAllSearchBulk = 1;

                                $szDeveloperNotes .= "<br><br>Status: BREAK_BULK, stopped search";
                                $szDeveloperNotes .= "<br>Description: Trade is defined under /dtdTrades/";
                            }
                            $this->set_fTotalVolume(sanitize_all_html_input($cargodetailArys['fTotalVolume']),$counter,false,$bulk_key);
                            $this->set_fTotalWeight(sanitize_all_html_input($cargodetailArys['fTotalWeight']),$counter,false,$bulk_key); 

                            $this->set_szDimensionMeasure(sanitize_all_html_input($cargodetailArys['szDimensionMeasure']),$counter,false,$parcel_key); 
                            $this->set_szWeightMeasure(sanitize_all_html_input($cargodetailArys['szWeightMeasure']),$counter,false,$parcel_key); 
                            $bBreakBulk = true;
                            $this->iSearchBreakBulk = 1;

                            if(!empty($this->szWeightMeasure[$counter]))
                            {
                                $weightMeasureAry = $kConfig->isWeightMeasureExists($this->szWeightMeasure[$counter]);
                                if($weightMeasureAry['id']>0)
                                {
                                    $this->idWeightMeasure[$counter] = $weightMeasureAry['id'];
                                    $this->fKgFactor[$counter] = $weightMeasureAry['fKgFactor'];
                                }
                                else
                                {
                                    $this->buildErrorMessage('szWeightMeasure','INVALID',$counter);
                                }
                            } 
                            if(!empty($this->szDimensionMeasure[$counter]))
                            {
                                if(strtolower($this->szDimensionMeasure[$counter]) != 'cbm')
                                {
                                    $this->buildErrorMessage('szDimensionMeasure','INVALID',$counter);
                                }
                                else
                                {
                                    $this->idDimensionMeasure[$counter] = 3;
                                }
                            }

                            $fTotalCargoVolumeBreakBulk += $this->fTotalVolume[$counter];
                            $fTotalCargoWeightBreakBulk += $this->fTotalWeight[$counter];

                            $fTotalCargoVolume += $this->fTotalVolume[$counter];
                            $fTotalCargoWeight += $this->fTotalWeight[$counter];
                            $counter++;  
                        }
                        else if(strtolower($this->szPacketType[$counter]) == strtolower(__PACKET_TYPE_STANDARD_CARGO__))
                        {

                            $this->iSearchStandardCargo = 1;                        


                            $iStandardCargoId = ($cargodetailArys['iStandardCargoId']);

                            $iQunatity = ($cargodetailArys['iQuantity']);

                            $standardCargoAry = array();
                            $standardCargoAry = $kConfig->getStandardCargoList(false,$this->iBookingLanguage,$iStandardCargoId); 

                            if(!empty($standardCargoAry))
                            {
                                foreach($standardCargoAry as $standardCargoArys)
                                {
                                    if($standardCargoArys['idSearchMini']==8 || $standardCargoArys['idSearchMini']==9)
                                    {
                                        if(($standardCargoArys['fVolume']>0 || ($standardCargoArys['fHeight']>0 && $standardCargoArys['fWidth']>0 && $standardCargoArys['fLength']>0))&& $standardCargoArys['fWeight']>0   && $iEmptyRowFound!=1)
                                        { 
                                            $bCargoavailableForAllLine = true;
                                        }
                                        else
                                        { 
                                            $bCargoavailableForAllLine = false;
                                            $iEmptyRowFound = 1;
                                        }
                                    }
                                    else
                                    {
                                        if($standardCargoArys['fVolume']>0 && $standardCargoArys['fWeight']>0 && $standardCargoArys['fHeight']>0 && $standardCargoArys['fWidth']>0 && $standardCargoArys['fLength']>0  && $iEmptyRowFound!=1)
                                        { 
                                            $bCargoavailableForAllLine = true;
                                        }
                                        else
                                        { 
                                            $bCargoavailableForAllLine = false;
                                            $iEmptyRowFound = 1;
                                        }
                                    }


                                    $this->iLength[$counter] = $standardCargoArys['fLength'];
                                    $this->iWidth[$counter] = $standardCargoArys['fWidth'];
                                    $this->iHeight[$counter] = $standardCargoArys['fHeight'];
                                    $this->iWeight[$counter] = $standardCargoArys['fWeight'];  
                                    $this->szCommodityStandCargo[$counter] = $standardCargoArys['szLongName'];   
                                    $this->szSellersReference[$counter] = $standardCargoArys['szSellerReference'];
                                    $this->iColli[$counter] = $standardCargoArys['iColli'] * $iQunatity; 
                                    $this->fTotalVolumePerCargo[$counter] = $standardCargoArys['fVolume']* $iQunatity; 
                                    $this->iVolumePerColli[$counter] = $standardCargoArys['fVolume']; 
                                    $this->fTotalVolume[$counter] = $standardCargoArys['fVolume']; 
                                    $this->fTotalWeight[$counter] = $standardCargoArys['fWeight']; 

                                   // echo $iPageSearchType."iPageSearchType";
                                    //if($iPageSearchType=='5')
                                    //{
                                        //echo $standardCargoArys['fWeight']."fWeight";
                                        //echo $standardCargoArys['fVolume']."fVolume";
                                        if($standardCargoArys['fWeight']==0 )
                                        {
                                            $iUpdateWeightToZeroFlag=true;
                                        }
                                        if( $standardCargoArys['fVolume']==0)
                                        {
                                            $iUpdateVolumeToZeroFlag=true;
                                        }
                                    //}

                                    $this->idDimensionMeasure[$counter] = 1; //cm
                                    $this->idWeightMeasure[$counter] = 1; //kg
                                    $this->szDimensionMeasure[$counter]='CM';
                                    $this->szWeightMeasure[$counter]='KG';
                                    
                                    $this->iQuantity[$counter] = $iQunatity;
                                    $this->idStandardCargoType[$counter] = $this->idStandardCargoType[$key];
                                    $this->idCargoCategory[$counter] = $this->idCargoCategory[$key];

                                    $lenWidthHeightStr='';
                                    if((float)$this->iLength[$counter]==0.00 && (float)$this->iWidth[$counter]==0.00 && (float)$this->iHeight[$counter]==0.00)
                                    {
                                        $iShowCourierPriceFlag=1;
                                    }
                                    if((float)$this->iLength[$counter]>0.00 || (float)$this->iWidth[$counter]>0.00 || (float)$this->iHeight[$counter]>0.00)
                                    {
                                       $lenWidthHeightStr = format_numeric_vals($this->iLength[$counter]).'x'.format_numeric_vals($this->iWidth[$counter]).'x'.format_numeric_vals($this->iHeight[$counter]).'cm, ';
                                    }   
                                    $weightStr='';
                                   // if((int)$this->iWeight[$ctr]>0)
                                    //{
                                       $weightStr = format_numeric_vals($this->iWeight[$counter]).'kg ';
                                    //}

                                    $szCDQuantityText='';   
                                    $szICQuantityText='';   
                                    if((int)$iQunatity>0)
                                    {
                                        $szCDQuantityText =$iQunatity.' stk ';
                                        $szICQuantityText = $iQunatity.' pcs: ';
                                    }
                                    $fTotalVolumeStandardCargo += $standardCargoArys['fVolume'] * $iQunatity;
                                    $fTotalWeightStandardCargo += $standardCargoArys['fWeight'] * $iQunatity;

                                    $fTotalCargoVolume += $standardCargoArys['fVolume'] * $iQunatity;
                                    $fTotalCargoWeight += $standardCargoArys['fWeight'] * $iQunatity;



                                    $szInternalComments .= $szICQuantityText.$this->szCommodityStandCargo[$counter].', '.$lenWidthHeightStr.''.$weightStr.''.PHP_EOL;
                                    $szCargoDescription .= $szCDQuantityText.$this->szCommodityStandCargo[$counter].', ';
                                }
                            }
                            $counter++; 
                        }
                        else if(strtolower($this->szPacketType[$counter]) == strtolower(__PACKET_TYPE_UNKNOWN__))
                        {
                            $this->iUnknownShipmentType = 1;
                        }
                    } 
                }  
                if($bHalfPallets)
                {
                    if($iHalfPalletQuantity==1)
                    {
                        $fTotalCargoVolume += 0.6;
                        $fTotalCargoVolumePallet += 0.6;
                    }
                    elseif($iHalfPalletQuantity==2)
                    {
                        $fTotalCargoVolume +=  1.8;
                        $fTotalCargoVolumePallet += 1.8;
                    }
                    else
                    {
                        $fTotalCargoVolume += ($iHalfPalletQuantity * 1.8);
                        $fTotalCargoVolumePallet += ($iHalfPalletQuantity * 1.8);
                    }
                }
                if($bEuroPallets)
                { 
                    $fTotalCargoVolume += ($iEuroPalletQuantity * 1.8);
                    $fTotalCargoVolumePallet += ($iEuroPalletQuantity * 1.8);
                }

                $szServiceLineCargoDescription = "";
                if($this->iSearchPallet==1)
                {
                    if($iNumEuroPallet>0)
                    {
                        $kQuote = new cQuote();
                        $palletDetailAry = array();
                        $palletDetailAry = $kQuote->getPalletDetails(false,$this->iBookingLanguage,__PALLET_TYPE_EURO__); 
                        $szServiceLineCargoDescription = $iNumEuroPallet." x ".$palletDetailAry['szLongName'];
                    }
                    if($iNumHalfPallet>0)
                    {
                        $kQuote = new cQuote();
                        $palletDetailAry = array();
                        $palletDetailAry = $kQuote->getPalletDetails(false,$this->iBookingLanguage,__PALLET_TYPE_HALF__); 

                        if(!empty($szServiceLineCargoDescription))
                        {
                            $szServiceLineCargoDescription .=", ". $iNumHalfPallet." x ".$palletDetailAry['szLongName'];
                        }
                        else
                        {
                            $szServiceLineCargoDescription = $iNumHalfPallet." x ".$palletDetailAry['szLongName'];
                        } 
                    } 
                    if($iNumOthers>0)
                    {
                        $kQuote = new cQuote();
                        $palletDetailAry = array();
                        $palletDetailAry = $kQuote->getPalletDetails(false,$this->iBookingLanguage,__PALLET_TYPE_OTHER__); 

                        if(!empty($szServiceLineCargoDescription))
                        {
                            $szServiceLineCargoDescription .=", ". $iNumOthers." x ".$palletDetailAry['szLongName'];
                        }
                        else
                        {
                            $szServiceLineCargoDescription = $iNumOthers." x ".$palletDetailAry['szLongName'];
                        }  
                    }
                } 
                $this->szServiceLineCargoDescription = $szServiceLineCargoDescription;
                if($this->iSearchStandardCargo == 1)
                {
                    $this->iShipperDetailsLocked = (int)$data['iShipperDetailsLocked'];
                    $this->iConsigneeDetailsLocked = (int)$data['iConsigneeDetailsLocked'];
                    $this->iCargoDescriptionLocked = (int)$data['iCargoDescriptionLocked'];

                    if($bCargoavailableForAllLine)
                    { 
                        /*if($data['szAutomatedRfqResponseCode']=='')
                        {
                            $errorFlag = true;
                            $this->buildErrorDetailAry('szAutomatedRfqResponseCode','REQUIRED');
                        }*/

                        if($data['szAutomatedRfqResponseCode']!='')
                        {
                            $szAutomatedRfqResponseCode=$data['szAutomatedRfqResponseCode'];
                            $kExplain = new cExplain();
                            $landingPageArr=$kExplain->getAutomatedRfqResponseList('',$szAutomatedRfqResponseCode);
                            if(!empty($landingPageArr))
                            {
                                $idAutomatedRfqResponseCode=$landingPageArr[0]['id'];  
                                $this->szAutomatedRfqResponseCode=$szAutomatedRfqResponseCode;
                            }
                            /*else
                            {
                                $errorFlag = true;
                                $this->buildErrorDetailAry('szAutomatedRfqResponseCode','INVALID');
                            }*/
                            
                        }
                        if((int)$idAutomatedRfqResponseCode>0)
                        {
                            $kExplain = new cExplain();
                            $standardQuotePricingAry = array();
                            $standardQuotePricingAry = $kExplain->getStandardQuotePriceByPageID($idAutomatedRfqResponseCode,true,0,false,true);  

                            $standardHandlerAry = array();
                            $standardHandlerAry = $kExplain->getQuoteHandlerByPageID($idAutomatedRfqResponseCode,true,true); 

                            if(!empty($standardHandlerAry) && !empty($standardQuotePricingAry))
                            {
                                $this->iVogaSearchAutomatic = true;
                            }
                            else
                            {
                                $this->iVogaSearchAutomatic = false;
                            }

                            $this->iCreateAutomaticQuote = 1;
                            $this->idDefaultLandingPage=$idAutomatedRfqResponseCode;
                        }
                        else
                        {
                            $this->iVogaSearchAutomatic = false;
                        }
                    }
                    if($iUpdateWeightToZeroFlag)
                    {
                        $fTotalWeight=0;
                        $this->iUpdateWeightToZeroFlag = $iUpdateWeightToZeroFlag;
                    }
                    if($iUpdateVolumeToZeroFlag)
                    {
                        $fTotalVolume=0;
                        $this->iUpdateVolumeToZeroFlag = $iUpdateVolumeToZeroFlag;
                    }
                    //echo $this->iUpdateVolumeWeightToZeroFlag."iUpdateVolumeWeightToZeroFlag";

                    $this->fTotalWeightStandardCargo = $fTotalWeightStandardCargo;
                    $this->fTotalVolumeStandardCargo = $fTotalVolumeStandardCargo;
                    $this->szInternalComments = $szInternalComments;
                    $szCargoDescription = trim($szCargoDescription);
                    $this->szCargoDescriptionForStandardCargo = rtrim($szCargoDescription,',');  
                    $this->iShowCourierPriceFlag=$iShowCourierPriceFlag;
                    
                    if((int)$data['iShipperDetailsLocked']==1)
                    {
                        $iCheckShipperCityName=false;
                    }                
                    if((int)$data['iConsigneeDetailsLocked']==1)
                    {
                        $iCheckConsigneeCityName=false;
                    }
                }
                else
                {
                    $this->fTotalCargoVolumeBreakBulk = $fTotalCargoVolumeBreakBulk;
                    $this->fTotalCargoWeightBreakBulk = $fTotalCargoWeightBreakBulk;
                    $this->fTotalCargoVolumeParcel = $fTotalCargoVolumeParcel;
                    $this->fTotalCargoWeightParcel = $fTotalCargoWeightParcel; 
                    $this->fTotalCargoVolumePallet = $fTotalCargoVolumePallet;
                    $this->fTotalCargoWeightPallet = $fTotalCargoWeightPallet;

                    $this->iStopLclSearch = $iStopLclSearch;
                    $this->iStopAllSearch = $iStopAllSearch;

                    $this->iAlreadyDefinedServiceType = 0;     
                    if($this->iSearchPallet==1 && $this->iDTDTrades==1)
                    {
                        $this->iAlreadyDefinedServiceType = 1;
                        $this->idServiceType = 1;
                        if($iCheckCustomerClearanceFlag){
                            $this->iOriginCC = 1;
                            $this->iDestinationCC = 2;
                        }
                    }
                }


                if($this->iSearchParcel!=1 && $this->iSearchPallet!=1 && $this->iSearchBreakBulk!=1 && $this->iUnknownShipmentType!=1 && $this->iSearchStandardCargo!='1')
                {
                    $errorFlag = true;
                    $this->buildErrorDetailAry('iCargoLine','INVALID');  
                } 
                $this->fGrandTotalCargoVolume = $fTotalCargoVolume;
                $this->fGrandTotalCargoWeight = $fTotalCargoWeight; 
                $this->szDeveloperNotes = $szDeveloperNotes;
            
            }
             
            /*
            * Function For
            * Cargo Validations
            */
            $bCargoValueRequired = false;
            if(!empty($data['fCargoValue']) && $data['fCargoValue']>0)
            {
                $bCargoValueRequired = true;
            } 
            else
            {
                $data['fCargoValue'] = ""; 
            }
            $this->set_szCargoDescription(trim(sanitize_all_html_input($data['szCargoDescription'])),$bMethodTypeQuoteOrBooking); 
            
            if($mode=='QUICK_QUOTE')
            {
                $this->szCargoType = $data['szCargoType']; 
                $this->iPrivateShipping = $data['iPrivateShipping'];   
            }
            else
            {
                $this->set_szCommodity(trim(sanitize_all_html_input($data['szCommodity'])),$bMethodTypeQuoteOrBooking);
                if($this->szCommodity>0)
                {
                    $kVatApplication = new cVatApplication();
                    $kVatApplication->loadCargoTypes(false,$this->szCommodity);
                    if(!empty($kVatApplication->szCargoTypeCode))
                    {
                        $this->szCargoType = $kVatApplication->szCargoTypeCode;
                    }
                    else
                    { 
                        $this->buildErrorDetailAry("szCommodity",'INVALID'); 
                    }
                }
                //For Partner API call we always assume customer is a non-private user
                $this->iPrivateShipping = 0;
            }  
            
            $this->set_fCargoValue(trim(sanitize_all_html_input($data['fCargoValue'])),$bCargoValueRequired);
            $this->set_szCargoCurrency(trim(sanitize_all_html_input($data['szCargoCurrency'])),$bCargoValueRequired);
            $this->set_szCurrency(trim(sanitize_all_html_input($data['szCurrency'])));
            
            if($this->iSearchBreakBulk==1)
            {
                $bServiceTermsRequired = true;
            }
            if($this->iSearchParcel==1)
            {
                $bServiceTermsRequired = false;
            }
            
            $bPostcodeRequired = false;
            if($this->iSearchParcel==1 || $this->iSearchPallet==1)
            {
                $bPostcodeRequired = true;
            } 
            //Only required in case of breakbulk
            $this->set_szServiceTerm(trim(sanitize_all_html_input($data['szServiceTerm'])),$bServiceTermsRequired);
            $this->set_szServiceType(trim(sanitize_all_html_input($data['szServiceType'])),self::$szServiceTypeReqFlag);
            
            if($bCheckPriceForPostcodeFlag)
            {
                if(empty($this->szServiceTerm))
                {
                    $szServiceTerm = __SERVICE_SHORT_TERMS_EXW__;
                }
                else
                {
                    $szServiceTerm = $this->szServiceTerm;
                }
                $kConfig = new cConfig();
                $serviceTermsAry = array();
                $serviceTermsAry = $kConfig->getAllServiceTerms(false,$szServiceTerm);

                if(!empty($serviceTermsAry))
                {
                    $this->idServiceTerms = $serviceTermsAry[0]['id'];
                }
                
                if($this->iAlreadyDefinedServiceType!=1)
                {
                    $serviceTypeAry = array();
                    $serviceTypeAry = $this->getServiceType($szServiceTerm);
                    $this->idServiceType = $serviceTypeAry['idServiceType'];
                }   
                if($this->idServiceType==__SERVICE_TYPE_DTD__ || $this->idServiceType==__SERVICE_TYPE_DTW__ || $this->idServiceType==__SERVICE_TYPE_DTP__) // 1.DTD 2.DTD
                {
                    $iShipperAddressRequiredFlag=true;
                }
                else
                {
                    $iShipperAddressRequiredFlag=false;
                } 
                if($this->idServiceType==__SERVICE_TYPE_DTD__ || $this->idServiceType==__SERVICE_TYPE_WTD__ || $this->idServiceType==__SERVICE_TYPE_PTD__) // 1.DTD 2.DTD
                {
                    $iConsigneeAddressRequiredFlag=true;
                }
                else
                {
                    $iConsigneeAddressRequiredFlag=false;
                }
                
                $kAdmin = new cAdmin();
                $idShipperCountry=$this->idShipperCountry;
                $shipperCountryArr=$kAdmin->countriesView($idShipperCountry,$idShipperCountry);
 
                if($iShipperAddressRequiredFlag && $shipperCountryArr['iUsePostcodes']==1)
                {
                    $iShipperPostcodeRequiredFlag=true;
                }
                else
                {
                    $iShipperPostcodeRequiredFlag=false;
                }

                $idConsigneeCountry=$this->idConsigneeCountry;
                $kAdmin = new cAdmin();
                $consigneeCountryArr=$kAdmin->countriesView($idConsigneeCountry,$idConsigneeCountry);
                if($iConsigneeAddressRequiredFlag && $consigneeCountryArr['iUsePostcodes']==1)
                {
                    $iConsigneePostcodeRequiredFlag=true;
                }
                else
                {
                    $iConsigneePostcodeRequiredFlag=false;
                }
            }
            
            /*
            * Function For
            * Shipper Validations
            */
            $this->set_szShipperCompanyName(trim(sanitize_all_html_input($data['szShipperCompanyName'])));
            $this->set_szShipperFirstName(trim(sanitize_all_html_input($data['szShipperFirstName'])),$bMethodTypeQuoteOrBooking);
            $this->set_szShipperLastName(trim(sanitize_all_html_input($data['szShipperLastName'])),$bMethodTypeQuoteOrBooking);
            $this->set_szShipperEmail(trim(sanitize_all_html_input($data['szShipperEmail'])),$bMethodTypeQuoteOrBooking);
            $this->set_iShipperCounrtyDialCode(trim(sanitize_all_html_input($data['iShipperCountryDialCode'])),$bMethodTypeQuoteOrBooking);
            $this->set_iShipperPhoneNumber(trim(sanitize_all_html_input($data['iShipperPhoneNumber'])),$bMethodTypeQuoteOrBooking);
            if(!$iShipperAddressRequiredFlag)
            {
                $this->set_szShipperAddress(trim(sanitize_all_html_input($data['szShipperAddress'])),false);
            }else{
            $this->set_szShipperAddress(trim(sanitize_all_html_input($data['szShipperAddress'])),$bMethodTypeQuoteOrBooking);  
            }
            $this->set_szShipperCity(trim(sanitize_all_html_input($data['szShipperCity'])));
            /*
            * Function For
            * Consignee Validations
            */
            $this->set_szConsigneeCompanyName(trim(sanitize_all_html_input($data['szConsigneeCompanyName'])));
            $this->set_szConsigneeFirstName(trim(sanitize_all_html_input($data['szConsigneeFirstName'])),$bMethodTypeQuoteOrBooking);
            $this->set_szConsigneeLastName(trim(sanitize_all_html_input($data['szConsigneeLastName'])),$bMethodTypeQuoteOrBooking);
            $this->set_szConsigneeEmail(trim(sanitize_all_html_input($data['szConsigneeEmail'])),$bMethodTypeQuoteOrBooking);
            $this->set_iConsigneeCounrtyDialCode(trim(sanitize_all_html_input($data['iConsigneeCountryDialCode'])),$bMethodTypeQuoteOrBooking);
            $this->set_iConsigneePhoneNumber(trim(sanitize_all_html_input($data['iConsigneePhoneNumber'])),$bMethodTypeQuoteOrBooking);
            if(!$iConsigneeAddressRequiredFlag){
                 $this->set_szConsigneeAddress(trim(sanitize_all_html_input($data['szConsigneeAddress'])),false); 
            }else{
                $this->set_szConsigneeAddress(trim(sanitize_all_html_input($data['szConsigneeAddress'])),$bMethodTypeQuoteOrBooking);  
            }
            $this->set_szConsigneeCity(trim(sanitize_all_html_input($data['szConsigneeCity'])));   
            $this->set_szPaymentType(trim(sanitize_all_html_input($data['szPaymentType'])),$bMethodTypeQuoteOrBooking);  
            $this->iPaymentTypePartner=(int)$data['iPaymentTypePartner'];  
            $billingFieldsFlag = false;
            $billingFieldsFlagForQuoteOrBooking=false;
            if($bMethodTypeBooking)
            {
                $kBilling = new cBilling();
                if($kBilling->isBillingDetailsEmpty($data))
                {
                    $billingFieldsFlag = true;                    
                    if($bMethodTypeQuoteOrBooking)
                    {
                        $billingFieldsFlagForQuoteOrBooking=true;
                    }
                } 
            }
            
            
            if(empty($data['szBillingCountry']))
            {
                /*
                * If billing country is empty then we consider partner's country as billing country
                */
                $idPartner = self::$idGlobalPartner;  
                $parnerDetailsAry = $this->getParnerAccountDetails($idPartner); 
                $kUserPartner = new cUser();
                $kUserPartner->getUserDetails($parnerDetailsAry['idCustomer']);  
                $data['szBillingCountry'] = $kUserPartner->szCountryISOName;   
                $this->iUsePartnersBillingDetails = true;
            } 
            else
            {
                $this->iUsePartnersBillingDetails = false;
            }
            
            /*
            * Function For
            * Billing Validations
            */ 
            $this->set_szBillingCompanyName(trim(sanitize_all_html_input($data['szBillingCompanyName'])));
            $this->set_szBillingFirstName(trim(sanitize_all_html_input($data['szBillingFirstName'])),$billingFieldsFlagForQuoteOrBooking);
            $this->set_szBillingLastName(trim(sanitize_all_html_input($data['szBillingLastName'])),$billingFieldsFlagForQuoteOrBooking);
            $this->set_szBillingEmail(trim(sanitize_all_html_input($data['szBillingEmail'])),$billingFieldsFlag);
            $this->set_iBillingCountryDialCode(trim(sanitize_all_html_input($data['iBillingCountryDialCode'])),$billingFieldsFlagForQuoteOrBooking);
            $this->set_iBillingPhoneNumber(trim(sanitize_all_html_input($data['iBillingPhoneNumber'])),$billingFieldsFlagForQuoteOrBooking);
            $this->set_szBillingAddress(trim(sanitize_all_html_input($data['szBillingAddress'])),$billingFieldsFlagForQuoteOrBooking);  
            $this->set_szBillingCity(trim(sanitize_all_html_input($data['szBillingCity'])),$billingFieldsFlagForQuoteOrBooking); 
            $this->set_szBillingPostcode(trim(sanitize_all_html_input($data['szBillingPostcode'])),$billingFieldsFlagForQuoteOrBooking); 
            $this->set_szBillingCountry(trim(sanitize_all_html_input($data['szBillingCountry'])),$billingFieldsFlag);
            
            if($this->szPaymentType==__TRANSPORTECA_PAYMENT_TYPE_1__ && $bMethodTypeBooking)
            {
                $this->szAmount = (float)(trim(sanitize_all_html_input($data['szAmount'])));  
                //$this->set_idTransaction(trim(sanitize_all_html_input($data['idTransaction']))); 
                $this->idTransaction = $data['idTransaction'];
            }  
            
            $this->set_szShipperPostcode(trim(sanitize_all_html_input($data['szShipperPostcode'])),false); 
            $this->set_szShipperLatitude(trim(sanitize_all_html_input($data['szShipperLatitude'])));
            $this->set_szShipperLongitude(trim(sanitize_all_html_input($data['szShipperLongitude'])));

            $this->set_szConsigneePostcode(trim(sanitize_all_html_input($data['szConsigneePostcode'])),false);
            $this->set_szConsigneeLatitude(trim(sanitize_all_html_input($data['szConsigneeLatitude'])));
            $this->set_szConsigneeLongitude(trim(sanitize_all_html_input($data['szConsigneeLongitude'])));  
            
            $this->set_iInsuranceFlag(trim(sanitize_all_html_input($data['bInsuranceRequired'])));
            $this->set_szPartnerReference(trim(sanitize_all_html_input($data['szReference']))); 
            
            $insuranceCheckFlag=true;
            if($this->iSearchStandardCargo == 1)
            {
                 $insuranceCheckFlag=false;
            }
            if(strtolower($this->iInsuranceFlag)== 'yes' || strtolower($this->iInsuranceFlag)== 'y' || (int)$this->iInsuranceFlag==1)
            {
                $this->iInsuranceIncluded  =1;
                $this->set_szInsurancePrice(trim(sanitize_all_html_input($data['szInsurancePrice'])),$insuranceCheckFlag); 
            }
            else if(strtolower($this->iInsuranceFlag)== 'optional')
            {
                $this->iInsuranceIncluded  =3;
                $this->set_szInsurancePrice(trim(sanitize_all_html_input($data['szInsurancePrice'])),$insuranceCheckFlag); 
            }
            else
            {
                $this->iInsuranceIncluded  = 0;
                $this->szInsurancePrice = sanitize_all_html_input($data['szInsurancePrice']); //In this case we don't apply any such validation
            }
             
            if($this->error==true)
            {  
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION'; 
                return true;
            } 
            else
            {  
                $kConfig = new cConfig();
                $kConfig->loadCountry(false,false,false,$data['iShipperCountryDialCode']); 
                if($kConfig->idCountry>0)
                {
                    $this->idShipperDialCode = $kConfig->idCountry;
                }
                
                $kConfig = new cConfig();
                $kConfig->loadCountry(false,false,false,$data['iConsigneeCountryDialCode']); 
                if($kConfig->idCountry>0)
                {
                    $this->idConsigneeDialCode = $kConfig->idCountry;
                }   
                
                if(!empty($data['iBillingCountryDialCode']))
                { 
                    $kConfig = new cConfig();
                    $kConfig->loadCountry(false,false,false,$data['iBillingCountryDialCode']); 
                    if($kConfig->idCountry>0)
                    {
                        $this->idBillingCountryDialCode = $kConfig->idCountry;
                    } 
                }
                if($this->szServiceTerm==__SERVICE_SHORT_TERMS_FOB__ || $this->szServiceTerm==__SERVICE_SHORT_TERMS_CFR__)
                {    
                    if((int)$this->iDTDTrades==1)
                    { 
                        if($this->szServiceTerm==__SERVICE_SHORT_TERMS_FOB__)
                        {
                            $this->szServiceTerm = __SERVICE_SHORT_TERMS_EXW__;
                        }
                        else if($this->szServiceTerm==__SERVICE_SHORT_TERMS_CFR__)
                        {
                            $this->szServiceTerm = __SERVICE_SHORT_TERMS_DAP__;
                        }                            
                    }
                }
                
                if(empty($this->szServiceTerm))
                {
                    $szServiceTerm = __SERVICE_SHORT_TERMS_EXW__;
                }
                else
                {
                    $szServiceTerm = $this->szServiceTerm;
                }
                $kConfig = new cConfig();
                $serviceTermsAry = array();
                $serviceTermsAry = $kConfig->getAllServiceTerms(false,$szServiceTerm);
                 
                if(!empty($serviceTermsAry))
                {
                    $this->idServiceTerms = $serviceTermsAry[0]['id'];
                }
                if($this->iAlreadyDefinedServiceType!=1)
                {
                    $serviceTypeAry = array();
                    $serviceTypeAry = $this->getServiceType($szServiceTerm);
                    $this->idServiceType = $serviceTypeAry['idServiceType'];
                    if($iCheckCustomerClearanceFlag){
                        $this->iOriginCC = $serviceTypeAry['iOriginCC'];
                        $this->iDestinationCC = $serviceTypeAry['iDestinationCC']; 
                    }
                }
                
                if(!$bDefaultShipmentDate)
                {
                    $errorFlag = false;
                    $dateErrorFlag = $this->validatePriceCalculationDate($data['dtShippingDay'],$data['dtShippingMonth'],$data['dtShippingYear']); 

                    if($dateErrorFlag)
                    {  
                        $errorFlag = true;
                    }
                    if(trim($data['dtShippingDay']) != '')
                    {
                        if(!checkdate($data['dtShippingMonth'], $data['dtShippingDay'], $data['dtShippingYear']))
                        {  
                            $errorFlag = true;
                        }
                    }
                }
                
                 
                $kConfig = new cConfig();
                $currencyAry = $kConfig->getBookingCurrency(false,true,false,false,$data['szCurrency'],true); 
                
                if(!empty($currencyAry))
                {
                    $this->idCustomerCurrency = $currencyAry[0]['id'];
                    $this->szCustomerCurrency = $currencyAry[0]['szCurrency'];
                    if($this->idCustomerCurrency ==1)
                    {
                        $this->fCustomerExchangeRate = 1; 
                    }
                    else
                    {
                        $this->fCustomerExchangeRate = $currencyAry[0]['fUsdValue']; 
                    } 
                }
                else
                {
                    $errorFlag = true;
                    $this->buildErrorDetailAry('szCurrency','INVALID');  
                } 
                if(!empty($data['szCargoCurrency']) || $this->iSearchStandardCargo == 1)
                {
                    if(strtolower($data['szCargoCurrency']) =='usd')
                    {
                        $this->idGoodsInsuranceCurrency = 1;
                        $this->szGoodsInsuranceCurrency = 'USD';
                        $this->fGoodsInsuranceExchangeRate = 1; 
                    }
                    else
                    {
                        $cargo_currencyAry = $kConfig->getBookingCurrency(false,true,false,false,$data['szCargoCurrency'],true); 
                       
                        if(!empty($cargo_currencyAry))
                        {
                            $this->idGoodsInsuranceCurrency = $cargo_currencyAry[0]['id'];
                            $this->szGoodsInsuranceCurrency = $cargo_currencyAry[0]['szCurrency'];
                            $this->fGoodsInsuranceExchangeRate = $cargo_currencyAry[0]['fUsdValue']; 
                        }
                        else
                        {
                            $errorFlag = true;
                            $this->buildErrorDetailAry('szCargoCurrency','INVALID');  
                        }
                    }
                } 
                 
                $kConfig_billing = new cConfig();
                $kConfig_billing->loadCountry(false,$data['szBillingCountry']); 
                if($kConfig_billing->idCountry>0)
                {
                    $this->idBillingCountry = $kConfig_billing->idCountry;
                    $this->szBillingCountryName = $kConfig_billing->szCountryName;
                } 
                else
                { 
                    $errorFlag = true;
                    $this->buildErrorDetailAry('szBillingCountry','INVALID');  
                }
                
                if($this->iUsePartnersBillingDetails)
                {
                    #@TO DO
                }
                else if(!empty($data['szBillingCountry']))
                { 
                    /*if($data['szBillingCountry']==$data['szShipperCountry'])
                    {
                        $this->iShipperConsigneeType = 'SHIPPER';
                    }
                    else if($data['szBillingCountry']==$data['szConsigneeCountry'])
                    {
                        $this->iShipperConsigneeType = 'CONSIGNEE';
                    }
                    else
                    {
                        $this->iShipperConsigneeType = "BILLING";
                    }*/
                    $kCustomerEvent=new cCustomerEevent();
                    $this->iShipperConsigneeType =$kCustomerEvent->getCustomerShipperConsigneeBillingType($data);
                } 
                
                $this->iDonotKnowShipperPostcode = false;
                $this->iDonotKnowConsigneePostcode = false;
                if(!empty($this->szShipperPostcode))
                {
                    //Nothing to do as we already have received postcode in API request parameters
                }
                else if(!empty($this->szShipperLatitude) && !empty($this->szShipperLongitude))
                {
                    //Find postcode from google geo API. If not found then search the same in Transporteca's database
                    
                    $postcodeCheckAry = array();
                    $postcodeCheckAry['idCountry'] =  $this->idShipperCountry ;
                    $postcodeCheckAry['szLatitute'] = $this->szShipperLatitude ;
                    $postcodeCheckAry['szLongitute'] = $this->szShipperLongitude;  
                    $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry); 
                  
                    $this->iDonotKnowShipperPostcode = 1;
                    if(!empty($postcodeResultAry['szPostCode']))
                    {
                        $this->szShipperPostcode = $postcodeResultAry['szPostCode'];
                    }
                    else
                    {
                        if($iShipperPostcodeRequiredFlag && $bMethodTypeQuoteOrBooking)
                        {
                            $errorFlag = true;
                            $this->buildErrorDetailAry('szShipperPostcode','REQUIRED');
                        } 
                    } 
                }
                else
                {
                    if($iShipperPostcodeRequiredFlag && $bMethodTypeQuoteOrBooking)
                    {
                        $errorFlag = true;
                        $this->buildErrorDetailAry('szShipperPostcode','REQUIRED');
                    }
                } 
                
                if(!empty($this->szConsigneePostcode))
                {
                    //Nothing to do as we already have received postcode in API request parameters
                }
                else if(!empty($this->szConsigneeLatitude) && !empty($this->szConsigneeLongitude))
                {
                    //Find postcode from google geo API. If not found then search the same in Transporteca's database
                    
                    $postcodeCheckAry = array();
                    $postcodeCheckAry['idCountry'] =  $this->idConsigneeCountry ;
                    $postcodeCheckAry['szLatitute'] = $this->szConsigneeLatitude ;
                    $postcodeCheckAry['szLongitute'] = $this->szConsigneeLongitude; 
                    $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry); 
                    $this->iDonotKnowConsigneePostcode = 1;
                    if(!empty($postcodeResultAry['szPostCode']))
                    {
                        $this->szConsigneePostcode = $postcodeResultAry['szPostCode'];
                    }
                    else
                    {
                        if($iConsigneePostcodeRequiredFlag && $bMethodTypeQuoteOrBooking)
                        {
                            $errorFlag = true;
                            $this->buildErrorDetailAry('szConsigneePostcode','REQUIRED');
                        } 
                    }
                }
                else
                {
                    if($iConsigneePostcodeRequiredFlag && $bMethodTypeQuoteOrBooking)
                    {
                        $errorFlag = true;
                        $this->buildErrorDetailAry('szConsigneePostcode','REQUIRED');
                    }
                } 
                if(!$errorFlag && $bMethodTypeBooking)
                {
                    $validPaymentTypeAry = array(__TRANSPORTECA_PAYMENT_TYPE_1__,__TRANSPORTECA_PAYMENT_TYPE_2__,__TRANSPORTECA_PAYMENT_TYPE_3__);
                    if(!in_array($this->szPaymentType,$validPaymentTypeAry))
                    {
                        $errorFlag = true;
                        $this->buildErrorDetailAry('szPaymentType','INVALID');
                    }
                }
                $bValidateShipperPostcodeWithGoogle = false;
                $bValidateConsigneePostcodeWithGoogle = false;
                
                /*
                * If we receive szShipperPostcode in API request then we can check this data against rows in tblpostcode
                */
                if($iShipperPostcodeRequiredFlag && !empty($this->szShipperPostcode) && !$this->iDonotKnowShipperPostcode)
                {
                    $bValidateShipperPostcodeWithGoogle = true;
                } 
                
                /*
                * If we receive szShipperPostcode in API request then we can check this data against rows in tblpostcode
                */
                if($iConsigneePostcodeRequiredFlag && !empty($this->szConsigneePostcode) && !$this->iDonotKnowConsigneePostcode)
                {
                    $bValidateConsigneePostcodeWithGoogle = true;
                }
                if(!$errorFlag)
                {
                    
                    if((int)$idBooking>0)
                    {
                        $kBooking = new cBooking();
                        $postSearchStandardCargoAry = array();
                        $postSearchStandardCargoAry = $kBooking->getBookingDetails($idBooking);
                        $iStandardPricing=$postSearchStandardCargoAry['iStandardPricing'];                    
                        if($iStandardPricing==1)
                        {
                            $this->iSearchStandardCargo = 1;
                        }                    
                        if($szMethod == "BOOKING" && $mode!='QUICK_QUOTE')
                        {
                            $iSuccessValidatePrice=$postSearchStandardCargoAry['iSuccessValidatePrice'];
                            if($iSuccessValidatePrice==1)
                            {
                                $kCustomerEevent= new cCustomerEevent();
                                $kCustomerEevent->checkShipperConsigneeDetailsChanged($this,$idBooking);                        
                            }
                        }
                    }
                    //If all errors are already validated then we check address validation from google 
                    if(!empty($this->szShipperPostcode) && !$this->iDonotKnowShipperPostcode)
                    {
                        $szOriginAddressStr = $this->szShipperPostcode.", "; 
                    }
                    
                    if($iShipperPostcodeRequiredFlag)
                    {
                        //We only pass postcode data to google if and only if we gets a valid postcode data for the country where we have $iShipperPostcodeRequiredFlag: true;
                        $szShipperAddressForGoogle = $this->szShipperPostcode.", ";  
                    }
                    else
                    {
                        /*
                        * In case when postcode is not required but if we receive postcode in API request then we pass postcode and Country name and if postcode is empty then we pass city, country name into google API
                        */
                        $this->szShipperPostcode = trim($this->szShipperPostcode);
                        if(!empty($this->szShipperPostcode))
                        {
                            $szShipperAddressForGoogle = $this->szShipperPostcode.", ";  
                        }
                        else
                        {
                            $szShipperAddressForGoogle = $this->szShipperCity.", ";  
                        } 
                    }
                    $szOriginAddressStr .= $this->szShipperCity.", ".$this->szShipperCountryName;
                    $szShipperAddressForGoogle .= $this->szShipperCountryName;
                      
                    /*
                    * If postcode if required for the country then we are passing {Postcode, Country} Name to Google API and 
                    * If Postcode is not required for the country then we send all the information we have like {Postcode, City, Country}
                    */
                    if($bValidateShipperPostcodeWithGoogle && (int)$this->iSearchStandardCargo == 0)
                    {
                        //@fetching lat long from google api
                        $originAddressDetailsAry = reverse_geocode($szShipperAddressForGoogle,true); 
                    }
                    else
                    {
                        //@fetching lat long from google api
                        $originAddressDetailsAry = reverse_geocode($szShipperAddressForGoogle,true); 
                    } 
                    
                    if($iCheckShipperCityName  && $kCustomerEevent->iCheckShipperPostcodeCityDetails=='0' && (int)$this->iSearchStandardCargo == 0 && !$this->iDonotKnowShipperPostcode)
                    {
                        if(!empty($originAddressDetailsAry['szCityName']))
                        {
                            /*
                            * If we gets city name from google then we always uses Shipper city name same as we receives rom Google
                            */
                            $this->szShipperCity = $originAddressDetailsAry['szCityName'];

                        }
                        else
                        {
                            /*
                            * If we don't finds any city name from google then we just returns an error message sayig 'City not recognised'
                            */
                            $errorFlag = true;
                            $this->buildErrorDetailAry('szShipperCity','INVALID');
                        }
                    }
                     
                    if($bValidateShipperPostcodeWithGoogle && (int)$this->iSearchStandardCargo == 0 && !$this->iDonotKnowShipperPostcode && $kCustomerEevent->iCheckShipperPostcodeCityDetails=='0')
                    {
                        if(!empty($originAddressDetailsAry['szPostCode']))
                        {
                            $szShipperPoscodeFromGoogle = $originAddressDetailsAry['szPostCode'];
                        }
                        else
                        {
                            $szShipperPoscodeFromGoogle = $originAddressDetailsAry['szPostcodeTemp'];
                        }
                        
                        /*
                        * Removing blank space from postcode
                        */
                        $szFormatedOriginPostcode = $this->formatPostcode($szShipperPoscodeFromGoogle);
                        if($this->szShipperPostcode==$szShipperPoscodeFromGoogle || $this->szShipperPostcode==$szFormatedOriginPostcode)
                        {
                            /*
                            * Here formated postcodes means, a postcode which doesn't have any special characters. 
                            * e.g. For Sweden, if we receives postcode like '113 51' or '11351' both are valid postcode.
                            */ 
                        }
                        else
                        {
                            $errorFlag = true;
                            $this->buildErrorDetailAry('szShipperPostcode','INVALID');
                        }
                    }
                    
                    /*
                    * For considering lat/long for calculation we are perorming following steps
                    * a. In case we get lat/long in the price call - then we uses that coordinate for the calculation.
                    * b. In case we do not get lat/long in price call, then we must look up the city in tblpostcodes, and take lat/long directly from there
                    * c. In case we do not get lat/long neither in price call nor from tblpostcode, then we uses lat/long what we receives from Google API.
                    */
                    if(empty($this->szShipperLatitude) || empty($this->szShipperLongitude))
                    {
                        $kConfig = new cConfig();
                        $originPostCodeAry = array();
                        $originPostCodeAry = $kConfig->getPostCodeDetails_requirement($this->idShipperCountry,$this->szShipperCity);
                        if(!empty($originPostCodeAry))
                        {
                            $this->szShipperLatitude = $originPostCodeAry['szLatitude']; 
                            $this->szShipperLongitude = $originPostCodeAry['szLongitude'];
                        }
                    }
                    if(!empty($this->szShipperLatitude) && !empty($this->szShipperLongitude))
                    {
                        $this->fOriginLatitude = $this->szShipperLatitude;
                        $this->fOriginLongitude = $this->szShipperLongitude;
                    }
                    else
                    {
                        $this->fOriginLatitude = $originAddressDetailsAry['szLat'];
                        $this->fOriginLongitude = $originAddressDetailsAry['szLng'];
                    }
                    
                    if(!empty($this->szConsigneePostcode) && !$this->iDonotKnowConsigneePostcode)
                    {
                        $szDestinationAddressStr = $this->szConsigneePostcode.", "; 
                    } 
                    
                    if($iConsigneePostcodeRequiredFlag)
                    {
                        //We only pass postcode data to google if and only if we gets a valid postcode data for the country where we have $iShipperPostcodeRequiredFlag: true;
                        $szConsigneeAddressForGoogle = $this->szConsigneePostcode.", ";  
                    }
                    else
                    {
                        /*
                        * In case when postcode is not required but if we receive postcode in API request then we pass postcode and Country name and if postcode is empty then we pass city, country name into google API
                        */
                        $this->szConsigneePostcode = trim($this->szConsigneePostcode);
                        if(!empty($this->szConsigneePostcode))
                        {
                            $szConsigneeAddressForGoogle = $this->szConsigneePostcode.", ";  
                        }
                        else
                        {
                            $szConsigneeAddressForGoogle = $this->szConsigneeCity.", ";  
                        }
                    }
                    $szDestinationAddressStr .= $this->szConsigneeCity.", ".$this->szConsigneeCountryName;
                    $szConsigneeAddressForGoogle .= $this->szConsigneeCountryName;
                        
//                     
                    /*
                    * If postcode if required for the country then we are passing {Postcode, Country} Name to Google API and 
                    * If Postcode is not required for the country then we send all the information we have like {Postcode, City, Country}
                    */
                    if($bValidateConsigneePostcodeWithGoogle && (int)$this->iSearchStandardCargo == 0)
                    { 
                       //@fetching lat long from google api
                        $destinationAddressDetailsAry = reverse_geocode($szConsigneeAddressForGoogle,true);  
                    }
                    else
                    { 
                        //@fetching lat long from google api
                        $destinationAddressDetailsAry = reverse_geocode($szConsigneeAddressForGoogle,true);  
                    }    
                     
                    if($iCheckConsigneeCityName  && $kCustomerEevent->iCheckConsigneePostcodeCityDetails=='0' && (int)$this->iSearchStandardCargo == 0 && !$this->iDonotKnowConsigneePostcode)
                    {
                        if(!empty($destinationAddressDetailsAry['szCityName']))
                        {
                            /*
                            * If we gets city name from google then we always uses Consignee city name same as we receives rom Google
                            */
                           $this->szConsigneeCity = $destinationAddressDetailsAry['szCityName'];
                        }
                        else
                        {
                            /*
                            * If we don't finds any city name from google then we just returns an error message sayig 'City not recognised'
                            */
                            $errorFlag = true;
                            $this->buildErrorDetailAry('szConsigneeCity','INVALID');
                        }
                    } 
                    if($bValidateConsigneePostcodeWithGoogle && (int)$this->iSearchStandardCargo == 0 && !$this->iDonotKnowConsigneePostcode && $kCustomerEevent->iCheckConsigneePostcodeCityDetails=='0')
                    {  
                        if(!empty($destinationAddressDetailsAry['szPostCode']))
                        {
                            $szConsigneePoscodeFromGoogle = $destinationAddressDetailsAry['szPostCode'];
                        }
                        else
                        {
                            $szConsigneePoscodeFromGoogle = $destinationAddressDetailsAry['szPostcodeTemp'];
                        }
                        /*
                        * Removing blank space from postcode
                        */
                        $szFormatedDestinationPostcode = $this->formatPostcode($szConsigneePoscodeFromGoogle); 
                        if($this->szConsigneePostcode==$szConsigneePoscodeFromGoogle || $this->szConsigneePostcode==$szFormatedDestinationPostcode)
                        {
                            /*
                            * Here formated postcodes means, a postcode which doesn't have any special characters. 
                            * e.g. For Sweden, if we receives postcode like '113 51' or '11351' both are valid postcode.
                            */
                        }
                        else
                        {
                            $errorFlag = true;
                            $this->buildErrorDetailAry('szConsigneePostcode','INVALID');
                        }
                    }
                    
                    /*
                    * For considering lat/long for calculation we are perorming following steps
                    * a. In case we get lat/long in the price call - then we uses that coordinate for the calculation.
                    * b. In case we do not get lat/long in price call, then we must look up the city in tblpostcodes, and take lat/long directly from there
                    * c. In case we do not get lat/long neither in price call nor from tblpostcode, then we uses lat/long what we receives from Google API.
                    */
                    if(empty($this->szConsigneeLatitude) || empty($this->szConsigneeLongitude))
                    {
                        $kConfig = new cConfig();
                        $destinationPostCodeAry = array();
                        $destinationPostCodeAry = $kConfig->getPostCodeDetails_requirement($this->idConsigneeCountry,$this->szConsigneeCity);
                        if(!empty($destinationPostCodeAry))
                        {
                            $this->szConsigneeLatitude = $destinationPostCodeAry['szLatitude']; 
                            $this->szConsigneeLongitude = $destinationPostCodeAry['szLongitude'];
                        }
                    }
                    
                    if(!empty($this->szConsigneeLatitude) && !empty($this->szConsigneeLongitude))
                    {
                        $this->fDestinationLatitude = $this->szConsigneeLatitude;
                        $this->fDestinationLongitude = $this->szConsigneeLongitude;
                    }
                    else
                    {
                        $this->fDestinationLatitude = $destinationAddressDetailsAry['szLat'];
                        $this->fDestinationLongitude = $destinationAddressDetailsAry['szLng'];
                    } 
                    $this->szOriginCountryStr =  $szOriginAddressStr;
                    $this->szDestinationCountryStr =  $szDestinationAddressStr;
                }  
                if($errorFlag == true)
                {
                    $this->error = true;
                    self::$arrErrorMessageAry = $this->error_detail_ary; 
                    return true;
                } 
            } 
        }
    } 
    
    function formatPostcode($szPostcode)
    { 
        $strlen = strlen( $szPostcode );
        $szFormatedCode = "";
        for( $i = 0; $i <= $strlen; $i++ ) 
        {
            $szChar = substr( $szPostcode, $i, 1 ); 
            $szChar = trim($szChar); 
            if(!empty($szChar))
            {
                $szFormatedCode .= $szChar;
            }
        }
        return $szFormatedCode;
    }
    
    function getBillingCountryByTerms($data)
    {
        if(!empty($data))
        {
            $szServiceTerm = $data['szServiceTerm'];
            if($szServiceTerm=='ALL')
            {
                return $data['szConsigneeCountry'];
            }
            else if(empty($szServiceTerm))
            {
                $szServiceTerm =__SERVICE_SHORT_TERMS_EXW__;
            }
            
            $kConfig = new cConfig();
            $serviceTermsAry = array();
            $serviceTermsAry = $kConfig->getAllServiceTerms(false,$szServiceTerm);

            if(!empty($serviceTermsAry))
            {
                $idServiceTerms = $serviceTermsAry[0]['id'];
            }
            if($idServiceTerms>0)
            {
                /*
                * 1. EXW-DTD
                * 2. FOB
                * 3. DAP - Cleared
                * 4. CFR
                * 5. EXW-WTD
                * 6. DAP - No CC
                */
                $iShipperConsignee = '';
                if($idServiceTerms==1 || $idServiceTerms==2 || $idServiceTerms==5)
                {
                    //consignee  
                    $iShipperConsignee = 2;
                }
                else
                { 
                    //Shipper
                    $iShipperConsignee = 1;
                } 
                if($iShipperConsignee==2)
                {
                    return $data['szConsigneeCountry'];
                }
                else
                {
                    return $data['szShipperCountry'];
                }
            } 
        }
    } 
    
    public function getServiceType($szServiceTerms)
    { 
        $parcelSericeTerms = array(__SERVICE_SHORT_TERMS_EXW__,__SERVICE_SHORT_TERMS_DAP__,__SERVICE_SHORT_TERMS_DTD__);
        if(empty($szServiceTerms) || in_array($szServiceTerms, $parcelSericeTerms))
        {
            $idServiceType = __SERVICE_TYPE_DTD__;
            $iOriginCC = 1;
            $iDestinationCC = 2;
        } 
        else if($szServiceTerms==__SERVICE_SHORT_TERMS_DAP_NO_CC__)
        {
            $idServiceType = __SERVICE_TYPE_DTD__;
            $iOriginCC = 1; 
            $iDestinationCC = 0;
        }
        else if($szServiceTerms==__SERVICE_SHORT_TERMS_EXW_WTD__)
        {
            $idServiceType = __SERVICE_TYPE_WTD__;
            $iOriginCC = 1;
            $iDestinationCC = 2; 
        }
        else if($szServiceTerms==__SERVICE_SHORT_TERMS_CFR__)
        {
            $idServiceType = __SERVICE_TYPE_DTP__;
            $iOriginCC = 1; 
            $iDestinationCC = 0; 
        }
        else if($szServiceTerms==__SERVICE_SHORT_TERMS_FOB__)
        {
            $idServiceType = __SERVICE_TYPE_PTD__;
            $iOriginCC = 0; 
            $iDestinationCC = 2; 
        }  
        $returnAry = array();
        $returnAry['idServiceType'] = $idServiceType;
        $returnAry['iOriginCC'] = $iOriginCC;
        $returnAry['iDestinationCC'] = $iDestinationCC;
        return $returnAry;  
    }
    private function comparePricingChanges($fTotalPriceCustomerCurrency,$fShipmentUSDCost)
    {
        $kWhsSearch = new cWHSSearch();
        $iOverWeightTolerance = $kWhsSearch->getManageMentVariableByDescription('__OVER_WEIGHT_MEASURE_TOLERANCE_WITHOUT_PRICE_ADJUSTMENT__'); 
 
        $minTolerancePriceLevel = $fTotalPriceCustomerCurrency - ($fTotalPriceCustomerCurrency * $iOverWeightTolerance * .01) ;
        $maxTolerancePriceLevel = $fTotalPriceCustomerCurrency + ($fTotalPriceCustomerCurrency * $iOverWeightTolerance * .01) ;
 
        $this->szCargoLogString .= "<br> Overweight tolerance: ".$iOverWeightTolerance." %<br> ";
        $this->szCargoLogString .= "<br>Booking Price Customer Currency: ".number_format((float)$fTotalPriceCustomerCurrency) ;
        $this->szCargoLogString .= "<br>Min tolerance price (CC): ".number_format((float)$minTolerancePriceLevel);
        $this->szCargoLogString .= "<br>Max tolerance price (CC): ".number_format((float)$maxTolerancePriceLevel);
        $this->szCargoLogString .= "<h3> Step: 4 </h3>"; 
  
        if($fShipmentUSDCost>=$minTolerancePriceLevel && $fShipmentUSDCost<=$maxTolerancePriceLevel)
        {
            /*
            * This means booking price is between tolerable level i.e +, - 3% of booking amount
            */
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /*
    * 
    */
    public static function buildErrorMessage($flag='price')
    {  
        $arrErrorMessageAry = self::$arrErrorMessageAry;  
        $szGlobalErrorType = 'VALIDATION';
        cPartner::$szGlobalErrorType = $szGlobalErrorType;  
        if(!empty($arrErrorMessageAry))
        { 
            $errorMessageAry =  array();
            $errorMessageAry['status'] = 'Error';
            $errorMessageAry['token'] = self::$szGlobalToken;   
            if(!empty(self::$szNotification))
            { 
                $errorMessageAry['notification'] = t(self::$szBaseErrorURI."".self::$szNotification);
            } 
            if($flag=='price' && self::$iResponseDay!='')
            { 
                $iResponseDay=self::$iResponseDay;
                if($iResponseDay==2)
                {
                    $iResponseDay=0;
                }
                $errorMessageAry['iResponseDay'] = $iResponseDay;
            } 
            if(!empty(self::$searchValues))
            {
                $errorMessageAry['searchValues'] = self::$searchValues;
            }
            foreach($arrErrorMessageAry as $arrErrorMessageArys)
            { 
                $errorAry = array(); 
                $errorAry['code'] = $arrErrorMessageArys; 
                $errorAry['description'] = cApi::encodeString(t(self::$szBaseErrorURI."".$arrErrorMessageArys));
                $errorMessageAry['errors'][] = $errorAry;
            } 
            cResponseError::addMessage($errorMessageAry); 
            return true;
        }  
    }
    
    public static function buildSuccessMessage($type='Price')
    { 
        $arrServiceListAry = self::$arrServiceListAry; 
        $priceCalculationLogs = self::$priceCalculationLogs;
        cPartner::$szGlobalErrorType = 'SUCCESS';
        if(!empty($arrServiceListAry))
        {  
            $errorMessageAry =  array();
            $errorMessageAry['status'] = 'OK';
            if($type=='root'){
                $errorMessageAry['PriceToken'] = self::$szGlobalToken;
            }else{
            $errorMessageAry['token'] = self::$szGlobalToken;  
            }
            if(!empty(self::$szNotification))
            { 
                $errorMessageAry['notification'] = t(self::$szBaseErrorURI."".self::$szNotification);
            }
            if(!empty(self::$searchValues))
            {
                $errorMessageAry['searchValues'] = self::$searchValues;
            }
            if($type=='Booking')
            {
                $errorMessageAry['bookings'] = $arrServiceListAry; 
            }
            else if($type=='customer')
            {
                $errorMessageAry['customer'] = $arrServiceListAry; 
            }
            else if($type=='label')
            {
                $errorMessageAry['label'] = $arrServiceListAry; 
            } 
            else if($type=='customer_booking_history')
            {
                $errorMessageAry['myBookingList'] = $arrServiceListAry; 
            } 
            else if($type=='customer_registered_shipper')
            {
                $errorMessageAry['registeredShipperList'] = $arrServiceListAry; 
            }
            else if($type=='customer_registered_shipper_detail')
            {
                $errorMessageAry['registeredShipperDetail'] = $arrServiceListAry; 
            }
            else if($type=='customer_registered_consignee_detail')
            {
                $errorMessageAry['registeredConsigneeDetail'] = $arrServiceListAry; 
            }
            else if($type=='customer_registered_consignee')
            {
                $errorMessageAry['registeredConsigneeList'] = $arrServiceListAry; 
            }
            else if($type=='forwarder_rating_reviews')
            {
                $errorMessageAry['forwarderRating'] = $arrServiceListAry; 
            }
            else if($type=='insurance_data')
            {
                $errorMessageAry['insurance'] = $arrServiceListAry; 
            }
            else if($type=='customerEvent')
            {
                $errorMessageAry['customerEvent'] = $arrServiceListAry; 
            } 
            else if($type=='newsLetterSignup')
            {
                $errorMessageAry['newsLetterSignup'] = $arrServiceListAry; 
            }
            else if($type=='bookingQuote')
            {
                $szDtdTradeValue = self::$szDtdTradeValue;
                $errorMessageAry['szDtdTrade'] = $szDtdTradeValue;
                $errorMessageAry['bookingQuote'] = $arrServiceListAry; 
            } 
            else if($type=='dummy')
            {
                //$errorMessageAry['customer'] = $arrServiceListAry; 
            }
            else if($type=='standard_cargo_data')
            {
               $iResponseDay = self::$iResponseDay; 
               $errorMessageAry['iResponseDay'] = $iResponseDay;  
               $errorMessageAry['standardCargo'] = $arrServiceListAry;  
            }
            else if($type=='root' && !empty($arrServiceListAry))
            {
                $errorMessageAry = $errorMessageAry + $arrServiceListAry['root'];
            }
            else
            {
                $szDtdTradeValue = self::$szDtdTradeValue;
                $iOfferTypeFlag = self::$iOfferTypeFlag;
                $iResponseDay = self::$iResponseDay; 
                
                $arrRFQListAry = self::$arrRFQListAry;
                 
                $errorMessageAry['szDtdTrade'] = $szDtdTradeValue;
                if($iOfferTypeFlag=='1')
                { 
                    $errorMessageAry['iResponseDay'] = $iResponseDay; 
                }
                $errorMessageAry['services'] = $arrServiceListAry; 
                if(!empty($arrRFQListAry))
                {
                    $errorMessageAry['rfq'] = $arrRFQListAry;
                }
            }
            cResponseError::addMessage($errorMessageAry); 
            return true;
        }  
    }
    /*
    * 
    */
    public function getErrorDetailArry($fieldKey,$szErrorType)
    {   
        if(!empty($fieldKey) && !empty($szErrorType))
        {
            $result_ary = array(); 
            $query="
                SELECT 
                    id, 
                    szFieldKey,
                    szErrorCode,
                    szErrorType,
                    iErrorTypeFlag
                FROM	
                    ".__DBC_SCHEMATA_ERROR_DETAIL__."
                WHERE
                    szFieldKey = '".mysql_escape_custom(trim($fieldKey))."'
                AND
                    szErrorType = '".mysql_escape_custom(trim($szErrorType))."'
            "; 
            if($result=$this->exeSQL($query))
            {
                if($this->getRowCnt()>0)
                {
                    $row = $this->getAssoc($result); 
                    return $row['szErrorCode']; 
                }
                return false;
            }
            else
            {
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
        else
        {
            return false;
        } 
    } 
    
    /*
    * This function is used to parse $_SERVER array return header sent from Web API
    * @return array()
    * @author Whiz Solutions
    */
    public static function request_headers() 
    {
        $requestHeaderAry = array();
        $rx_http = '/\AHTTP_/';
        foreach($_SERVER as $key => $val) {
                if( preg_match($rx_http, $key) ) {
                        $arh_key = preg_replace_callback($rx_http, '', $key);
                        $rx_matches = array();
                        // do string manipulations to restore the original letter case
                        $rx_matches = explode('_', $arh_key);
                        if( count($rx_matches) > 0 and strlen($arh_key) > 2 ) {
                                foreach($rx_matches as $ak_key => $ak_val) $rx_matches[$ak_key] = ucfirst($ak_val);
                                $arh_key = implode('-', $rx_matches);
                        }
                        $arh_key = strtoupper($arh_key); 
                        $requestHeaderAry[$arh_key] = $val;
                }
        }
        return $requestHeaderAry;
    } 
    public function validatePartnerKey($clientApiHeaderAry,$idPartnerApiEventApi,$szMethod,$szReferenceToken=false)
    {
        /*
        * In case of getPrice and createBooking calls we receive $clientApiHeaderAry['X-Api-Key'] instead of $clientApiHeaderAry['X-API-KEY'] and we get $clientApiHeaderAry['X-Api-Mode'] instead of $clientApiHeaderAry['X-API-MODE']
        */ 
        if(!empty($clientApiHeaderAry['X-Api-Key']))
        {
            $clientApiHeaderAry['X-API-KEY'] = $clientApiHeaderAry['X-Api-Key'];
        }
        else if($clientApiHeaderAry['HTTP-X-API-KEY'])
        {
            $clientApiHeaderAry['X-API-KEY'] = $clientApiHeaderAry['HTTP-X-API-KEY'];
        }
        
        if(!empty($clientApiHeaderAry['X-Api-Mode']))
        {
            $clientApiHeaderAry['X-API-MODE'] = $clientApiHeaderAry['X-Api-Mode'];
        } 
        else if(!empty($clientApiHeaderAry['HTTP-X-API-MODE']))
        {
            $clientApiHeaderAry['X-API-MODE'] = $clientApiHeaderAry['HTTP-X-API-MODE'];
        }
        
        if(!empty($clientApiHeaderAry['X-Transporteca-Client-User-Agent']))
        {
            $clientApiHeaderAry['X-TRANSPORTECA-CLIENT-USER-AGENT'] = $clientApiHeaderAry['X-Transporteca-Client-User-Agent'];
        }  
        else if(!empty(['HTTP-X-TRANSPORTECA-CLIENT-USER-AGENT']))
        {
            $clientApiHeaderAry['X-TRANSPORTECA-CLIENT-USER-AGENT'] = $clientApiHeaderAry['HTTP-X-TRANSPORTECA-CLIENT-USER-AGENT'];
        }
        
        if(!empty($clientApiHeaderAry['User-Agent']))
        {
            $clientApiHeaderAry['USER-AGENT'] = $clientApiHeaderAry['User-Agent'];
        } 
        else if(!empty($clientApiHeaderAry['HTTP-X-TRANSPORTECA-CLIENT-USER-AGENT']))
        {
            $clientApiHeaderAry['USER-AGENT'] = $clientApiHeaderAry['HTTP-X-TRANSPORTECA-CLIENT-USER-AGENT'];
        } 
         
        if(empty($clientApiHeaderAry['X-API-KEY']))
        {
            $this->buildErrorDetailAry("szAuthorization",'AUTHENTICATION'); 
            $errorFlag = true;  
        } 
        if(empty($clientApiHeaderAry['X-API-MODE']))
        {
            $this->buildErrorDetailAry("szAuthorization",'AUTHENTICATION'); 
            $errorFlag = true;  
        } 
        
        $operationMethodAry = array();
        $operationMethodAry[0] = 'BOOKING';
        $operationMethodAry[1] = 'USER_PROFILE_DETAILS'; 
        $operationMethodAry[2] = 'CUSTOMER_CHANGE_PASSWORD';        
        $operationMethodAry[3] = 'USER_BOOKING_HISTORY';
        $operationMethodAry[4] = 'USER_ACTIVE_BOOKING';
        $operationMethodAry[5] = 'USER_ARCHIVED_BOOKING';
        $operationMethodAry[6] = 'USER_DRAFT_BOOKING';
        $operationMethodAry[7] = 'USER_HOLD_BOOKING';
        $operationMethodAry[8] = 'DELETE_CUSTOMER_ACCOUNT';
        $operationMethodAry[9] = 'UPDATE_USER_PROFILE';
        $operationMethodAry[10] = 'CUSTOMER_REGISTERED_SHIPPER';
        $operationMethodAry[11] = 'CUSTOMER_REGISTERED_CONSIGNEES';
        $operationMethodAry[12] = 'CUSTOMER_REGISTERED_SHIPPER_DETAIL';
        $operationMethodAry[13] = 'CUSTOMER_REGISTERED_CONSIGNEE_DETAIL';
        $operationMethodAry[14] = 'CUSTOMER_UPDATE_SHIPPER_DETAIL';
        $operationMethodAry[15] = 'CUSTOMER_UPDATE_CONSIGNEE_DETAIL';
        $operationMethodAry[16] = 'CUSTOMER_ADD_SHIPPER_DETAIL';
        $operationMethodAry[17] = 'CUSTOMER_ADD_CONSIGNEE_DETAIL';
        $operationMethodAry[18] = 'VALIDATE_PRICE';
        $operationMethodAry[19] = 'CONFIRM_PAYMENT'; 
        
        if(in_array($szMethod,$operationMethodAry) && empty($szReferenceToken))
        {
            $this->buildErrorDetailAry("szReferenceToken",'REQUIRED'); 
            $errorFlag = true; 
        }  
        if($errorFlag)
        { 
            self::$arrErrorMessageAry = $this->error_detail_ary; 
            self::$szGlobalErrorType = 'VALIDATION';  
            self::buildErrorMessage(); 
            return false;
        }
        
        $szApiKey = trim($clientApiHeaderAry['X-API-KEY']);
        $szApiMode = trim($clientApiHeaderAry['X-API-MODE']);
        $szApiMode = strtoupper($szApiMode);
        self::$szApiRequestMode = $szApiMode;
         
        $partenerDetail = $this->checkAuthorizedPartner($szApiKey,$szApiMode);
     
        if(!empty($partenerDetail))
        {
            $szPartnerAlies = $partenerDetail['szPartnerAlies'];  
            if(empty($szReferenceToken))
            {
               /*
                * We create token for each request and in case of booking we use token what we created o the time of service request.
                */
                $szReferenceToken = $this->getPartnerReferenceToken($szPartnerAlies); 
                $szReferenceToken = $this->isReferenceTokenExist($szReferenceToken,$szPartnerAlies);   
            } 
         
            self::$szGlobalPartnerAlies = $partenerDetail['szPartnerAlies'];
            self::$idGlobalPartner = $partenerDetail['id'];
            self::$szGlobalPartnerAPIKey = $szApiKey;
            self::$szGlobalToken = $szReferenceToken;   
            self::$szClientUserAgent = $clientApiHeaderAry['X-TRANSPORTECA-CLIENT-USER-AGENT'];
            self::$szUserAgent = $clientApiHeaderAry['USER-AGENT'];
            $this->updateApiEventLogToken($idPartnerApiEventApi);
            return true;
        }
        else
        {   
            $this->buildErrorDetailAry("szAuthorization",'AUTHENTICATION'); 
            $errorFlag = true; 
            self::$arrErrorMessageAry = $this->error_detail_ary; 
            self::$szGlobalErrorType = 'VALIDATION';  
            self::buildErrorMessage();  
            return false;
        } 
    }  
    
    /*
    * 
    */
    function getPartnerReferenceToken($szPartnerAlies)
    {
        $kBooking = new cBooking(); 
        $number = 17; 
        $uniqueNumber = $kBooking->generateUniqueToken($number); 
        $szReferenceToken = "T".$szPartnerAlies.$uniqueNumber; 
        $szReferenceToken = strtoupper($szReferenceToken); 
        return $szReferenceToken;
    }
    
    function isReferenceTokenExist($szReferenceToken,$szPartnerAlies)
    {
        if(!empty($szReferenceToken))
        {
            $query="
                SELECT
                    id
                FROM
                    ".__DBC_SCHEMATA_PARTNERS_API_CALL__."
                WHERE
                    szReferenceToken='".mysql_escape_custom(trim($szReferenceToken))."'
            ";
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $szReferenceToken = $this->getPartnerReferenceToken($szPartnerAlies);
                    $this->isReferenceTokenExist($szReferenceToken,$szPartnerAlies);
                }
                else
                {
                    return $szReferenceToken;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    /*
     * 
     */
    private function sendBookingConfirmationEmail($idBooking)
    {
        $kBooking = new cBooking();
        $bookingDetailAry = $kBooking->getBookingDetails($idBooking);
        
        if(!empty($bookingDetailAry))
        {
            $replace_ary['szCustomerCompany']= $bookingDetailAry['szCustomerCompanyName'];
            $replace_ary['szBookingReference']= $bookingDetailAry['szBookingRef'];
            
            $replace_ary['szBookingValue']= $bookingDetailAry[''];
            $replace_ary['szVATAmount']= $bookingDetailAry['fTotalVat'];
            $replace_ary['szYourReference']= $bookingDetailAry[''];

            $bookingDetailAry['szEmail'] = 'ajay@whiz-solutions.com';
            createEmail(__NEW_BOOKING_WITH_PARTNER_API__, $replace_ary,$bookingDetailAry['szEmail']);
        }
    }
    
    public function addPartnerApiEventLogs($priceInputAry,$clientApiHeaderAry)
    {
        if(!empty($priceInputAry))
        {
            $szRequestParams = serialize($priceInputAry);
        }
        if(!empty($clientApiHeaderAry))
        {
            $szRequestHeaders = serialize($clientApiHeaderAry);
        }
        
        $query="
            INSERT INTO
                ".__DBC_SCHEMATA_PARTNERS_API_EVENT_LOG_CALL__."
            (
                szRequestHeaders,
                szRequestParams,
                iActive,
                dtCreated
            )
            VALUE
            (
                '".mysql_escape_custom($szRequestHeaders)."',
                '".mysql_escape_custom($szRequestParams)."',
                1,
                NOW()
            )
        ";
        //echo "<br />".$query."<br>" ; 
        if($result = $this->exeSQL($query))
        {
            return $this->iLastInsertID ;  
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
    
    public function updateApiEventLogToken($idPartnerApiEventApi)
    {
        $idPartner = self::$idGlobalPartner;
        $szPartnerToken = self::$szGlobalToken; 
        $query = "
            UPDATE
                ".__DBC_SCHEMATA_PARTNERS_API_EVENT_LOG_CALL__."
            SET
              idPartner = '".mysql_escape_custom(trim($idPartner))."',
              szToken = '".mysql_escape_custom(trim($szPartnerToken))."', 
              dtUpdatedOn = now()
            WHERE
                id = '".(int)$idPartnerApiEventApi."' 	
        ";
        //echo $query ;
        if($result=$this->exeSQL($query))
        {
            return true ;
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
    
    public function updateApiEventLogResponse($szResposeData,$szResponseCode,$idPartnerApiEventApi)
    {
        $szDeveloperNotes = self::$szDeveloperNotes;
        $szApiRequestMode = self::$szApiRequestMode;
        
        $query = "
            UPDATE
                ".__DBC_SCHEMATA_PARTNERS_API_EVENT_LOG_CALL__."
            SET
              szResponseCode = '".mysql_escape_custom(trim($szResponseCode))."',
              szResposeData = '".mysql_escape_custom(trim($szResposeData))."', 
              szDeveloperNotes = '".mysql_escape_custom(trim($szDeveloperNotes))."', 
              szRequestMode = '".mysql_escape_custom(trim($szApiRequestMode))."', 
              dtUpdatedOn = now()
            WHERE
                id = '".(int)$idPartnerApiEventApi."' 	
        ";
        //echo $query ;
        if($result=$this->exeSQL($query))
        {
            return true ;
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
    
    public function prepareRequestParams($data,$cargoAry)
    {
        if(!empty($data))
        {
            $apiRequestParams = $data;   
            if(!empty($cargoAry))
            {
                $ctr=0;
                foreach ($cargoAry as $key=>$cargoArys)
                {   
                    if(!empty($cargoArys['szShipmentType']))
                    { 
                        if($cargoArys['szShipmentType']=='PARCEL' || $cargoArys['szShipmentType']=='PALLET')
                        {
                            $tempCargoAry[$ctr]['szShipmentType'] = $cargoArys['szShipmentType'];
                            if($cargoArys['szShipmentType']=='PALLET')
                            {
                                $tempCargoAry[$ctr]['szPalletType'] = $cargoArys['szPalletType'];
                            }  
                            $tempCargoAry[$ctr]['iLength'] = $cargoArys['iLength'];
                            $tempCargoAry[$ctr]['iWidth'] = $cargoArys['iWidth'];
                            $tempCargoAry[$ctr]['iHeight'] = $cargoArys['iHeight'];
                            $tempCargoAry[$ctr]['iWeight'] = $cargoArys['iWeight'];
                            $tempCargoAry[$ctr]['iQuantity'] = $cargoArys['iQuantity']; 
                            
                            $tempCargoAry[$ctr]['szWeightMeasure'] = $cargoArys['szWeightMeasure'];
                            $tempCargoAry[$ctr]['szDimensionMeasure'] = $cargoArys['szDimensionMeasure'];
                            $ctr++;
                        }
                        else if($cargoArys['szShipmentType']=='BREAK_BULK')
                        {
                            $tempCargoAry[$ctr]['szShipmentType'] = $cargoArys['szShipmentType'];
                            $tempCargoAry[$ctr]['fTotalVolume'] = $cargoArys['fTotalVolume'];
                            $tempCargoAry[$ctr]['fTotalWeight'] = $cargoArys['fTotalWeight']; 
                            $tempCargoAry[$ctr]['szWeightMeasure'] = $cargoArys['szWeightMeasure'];
                            $tempCargoAry[$ctr]['szDimensionMeasure'] = $cargoArys['szDimensionMeasure'];
                            $ctr++;
                        } 
                        else if($cargoArys['szShipmentType']=='UNKNOWN')
                        {
                            $tempCargoAry[$ctr]['szShipmentType'] = $cargoArys['szShipmentType'];
                        }
                    }  
                }
            }  
            $apiRequestParams['cargo'] = $tempCargoAry;  
            return $apiRequestParams;
        }
    }
    
    function addServiceApiResponse($data)
    {
        if(!empty($data))
        {
            $szApiRequestMode = self::$szApiRequestMode;
            
            $query="
                INSERT INTO
                    ".__DBC_SCHEMATA_PARTNERS_API_RESPONSE__."
                (
                    idPartner,
                    szRequestMode,
                    szReferenceToken,
                    szServiceID, 
                    dtExpectedDelivery,
                    szPackingType,
                    fBookingPrice,
                    fVatAmount,
                    bInsuranceAvailable,
                    fInsurancePrice,
                    idCurrency,
                    szCurrency,
                    fExchangeRate, 
                    fValueOfGoods,
                    fValueOfGoodsUSD,
                    idGoodsInsuranceCurrency,
                    szGoodsInsuranceCurrency,
                    fGoodsInsuranceExchangeRate,
                    fTotalInsuranceCostForBooking,
                    fTotalInsuranceCostForBookingCustomerCurrency,
                    fTotalInsuranceCostForBookingUSD,
                    fTotalAmountInsured,
                    iMinrateApplied,
                    fTotalInsuranceCostForBookingCustomerCurrency_buyRate,
                    fTotalInsuranceCostForBookingUSD_buyRate,
                    iShipmentType,
                    iBookingType,
                    idPackingType, 
                    szBookingReference,
                    szBookingStatus,
                    szBookingContactPhone,
                    szBookingContactEmail,
                    szCarrierName,
                    szCarrierAddress,
                    szCarrierPostCode,
                    szCarrierCity,
                    szCarrierState,
                    szCarrierCounrty,
                    szCarrierCompanyRegNo,
                    dtCreatedOn,
                    szServiceDescription,
                    szCollection,
                    szDelivery,
                    szShipperPostcodeRequired,
                    szConsigneePostcodeRequired
                )
                VALUE
                (
                    '".(int)$data['idPartner']."',
                    '".mysql_escape_custom(trim($szApiRequestMode))."', 
                    '".mysql_escape_custom(trim($data['szReferenceToken']))."', 
                    '".mysql_escape_custom(trim($data['szServiceID']))."', 
                    '".mysql_escape_custom(trim($data['dtExpectedDelivery']))."',
                    '".mysql_escape_custom(trim($data['szPackingType']))."', 
                    '".mysql_escape_custom(trim($data['fBookingPrice']))."',
                    '".mysql_escape_custom(trim($data['fVatAmount']))."',
                    '".mysql_escape_custom(trim($data['bInsuranceAvailable']))."',
                    '".mysql_escape_custom(trim($data['fInsurancePrice']))."',
                    '".mysql_escape_custom(trim($data['idCurrency']))."',
                    '".mysql_escape_custom(trim($data['szCurrency']))."',
                    '".mysql_escape_custom(trim($data['fExchangeRate']))."', 
                    '".mysql_escape_custom(trim($data['fValueOfGoods']))."',
                    '".mysql_escape_custom(trim($data['fValueOfGoodsUSD']))."',
                    '".mysql_escape_custom(trim($data['idGoodsInsuranceCurrency']))."',
                    '".mysql_escape_custom(trim($data['szGoodsInsuranceCurrency']))."',
                    '".mysql_escape_custom(trim($data['fGoodsInsuranceExchangeRate']))."',
                    '".mysql_escape_custom(trim($data['fTotalInsuranceCostForBooking']))."', 
                    '".mysql_escape_custom(trim($data['fTotalInsuranceCostForBookingCustomerCurrency']))."',
                    '".mysql_escape_custom(trim($data['fTotalInsuranceCostForBookingUSD']))."',
                    '".mysql_escape_custom(trim($data['fTotalAmountInsured']))."',
                    '".mysql_escape_custom(trim($data['iMinrateApplied']))."',
                    '".mysql_escape_custom(trim($data['fTotalInsuranceCostForBookingCustomerCurrency_buyRate']))."',
                    '".mysql_escape_custom(trim($data['fTotalInsuranceCostForBookingUSD_buyRate']))."',
                    '".mysql_escape_custom(trim($data['iShipmentType']))."', 
                    '".mysql_escape_custom(trim($data['iBookingType']))."', 
                    '".mysql_escape_custom(trim($data['idPackingType']))."',   
                    '".mysql_escape_custom(trim($data['szBookingReference']))."',  
                    '".mysql_escape_custom(trim($data['szBookingStatus']))."',  
                    '".mysql_escape_custom(trim($data['szBookingContactPhone']))."',  
                    '".mysql_escape_custom(trim($data['szBookingContactEmail']))."',  
                    '".mysql_escape_custom(trim($data['szCarrierName']))."',  
                    '".mysql_escape_custom(trim($data['szCarrierAddress']))."',  
                    '".mysql_escape_custom(trim($data['szCarrierPostCode']))."',  
                    '".mysql_escape_custom(trim($data['szCarrierCity']))."',  
                    '".mysql_escape_custom(trim($data['szCarrierState']))."',  
                    '".mysql_escape_custom(trim($data['szCarrierCounrty']))."',
                    '".mysql_escape_custom(trim($data['szCarrierCompanyRegNo']))."',
                    NOW(),
                    '".mysql_escape_custom(trim($data['szServiceDescription']))."',
                    '".mysql_escape_custom(trim($data['szCollection']))."',
                    '".mysql_escape_custom(trim($data['szDelivery']))."',
                    '".mysql_escape_custom(trim($data['szShipperPostcodeRequired']))."',
                    '".mysql_escape_custom(trim($data['szConsigneePostcodeRequired']))."'    
                )
            ";  
        //    echo "<br />".$query."<br>" ;
//            die; 
            if($result = $this->exeSQL($query))
            { 
                return true; 
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
        }
    }
    
    private function confirmBookingPayment($idBooking)
    {
        if($idBooking>0)
        { 
            $kBooking = new cBooking(); 
            $kBilling = new cBilling();
            $bookingArr = array();
            $bookingArr=$kBooking->getExtendedBookingDetails($idBooking);
            
            $comment = 'Partner Booking - Auto received'; 
            //$kBilling->markTransportecaBookingReceived($bookingArr,$comment);
            return true;
        }
    }
    
    private function confirmBookingPayment_backup($idBooking)
    { 
        if($idBooking>0)
        {
            $kBooking = new cBooking();
            $kAdmin = new cAdmin();
            $kCourierServices = new cCourierServices();
            $bookingArr = array();
            $bookingArr=$kBooking->getExtendedBookingDetails($idBooking);
 
            /*
             * Booking created from partner api is always a Bank Tranfer booking
             */
            $iUpdateBankTransfer = true; 

            $iMarkupPaymentReceived = false;
            if($bookingArr['iForwarderGPType']==__FORWARDER_PROFIT_TYPE_MARK_UP__)
            {
                $iMarkupPaymentReceived = true;
            }
            
            $check = 1;
            $dtCurrentDate = $kBooking->getRealNow();
            $comment = 'Partner Booking - Auto received';
            $status=$kAdmin->changePaymentRecievedStatus($idBooking,$check,$dtCurrentDate,$comment,$iMarkupPaymentReceived);
 
            if($iMarkupPaymentReceived)
            { 
                $fTotalPriceUSD = $bookingArr['fTotalPriceCustomerCurrency'] * $bookingArr['fExchangeRate'];
                $fTotalVATUSD = $bookingArr['fTotalVat'] * $bookingArr['fExchangeRate'];
                $fReferralAmountUSD = $bookingArr['fReferalAmountUSD'];

                //$fTotalInvoiceValueUSD = $fTotalPriceUSD + $fTotalVATUSD ;
                $fTotalInvoiceValueUSD = $fTotalPriceUSD ;

                //we always add value in USD to display on Management=>Dashboard=> Actual Revenue Graph
                $referaalAmountArr = array();
                $referaalAmountArr['idBooking'] = $bookingArr['idBooking'];
                $referaalAmountArr['idForwarder'] = $bookingArr['idForwarder']; 
                $referaalAmountArr['fTotalAmount'] = $fTotalInvoiceValueUSD;
                $referaalAmountArr['idCurrency'] = 1;
                $referaalAmountArr['fExchangeRate'] = 1;
                $referaalAmountArr['szCurrency'] = 'USD'; 
                $referaalAmountArr['szBooking'] = 'Transporteca mark-up: '.$bookingArr['szBookingRef'];  
                $referaalAmountArr['fUSDReferralFee'] = $fReferralAmountUSD;
                $referaalAmountArr['szBookingNotes'] = "We always keep this value in USD to display on Actual Revenue graph. ";

                $kBilling = new cBilling();
                $kBilling->insertTransportecaMarkupFee($referaalAmountArr);
            }
            
            if($status==1)
            {	
                $kForwarder = new cForwarder(); 
                $idForwarder = $bookingArr['idForwarder'];
                $kAdmin->updateCustomerPaidAmount($idBooking,$iUpdateBankTransfer,$iMarkupPaymentReceived);
                $checkComapnyBankDetils = $kForwarder->checkForwarderComleteBankInfomation($idForwarder,1);
 
                if($bookingArr['iBookingType']==__BOOKING_TYPE_COURIER__)
                { 
                    if($iUpdateBankTransfer)
                    {   
                        $courierBookingArr=$kCourierServices->getCourierBookingData($idBooking);

                        $kConfig = new cConfig();
                        $data = array();
                        $data['idForwarder'] = $bookingArr['idForwarder'];
                        $data['idTransportMode'] = $bookingArr['idTransportMode'];
                        $data['idOriginCountry'] = $bookingArr['idOriginCountry'];
                        $data['idDestinationCountry'] = $bookingArr['idDestinationCountry'];
                        $data['iBookingSentFlag'] = '1';
                        $kForwarder = new cForwarder();
                        $kForwarder->load($bookingArr['idForwarder']);
                        $iProfitType = $kForwarder->iProfitType;
                        $acceptedByUserAry = $kForwarder->getTermsConditionAcceptedByUserDetails($bookingArr['idForwarder']);

                        $forwarderContactAry = array();
                        $forwarderContactAry = $kConfig->getForwarderContact($data);

                        $kBooking = new cBooking();
                        $dtResponseTime = $kBooking->getRealNow(); 
                        $iAddBookingFile=false;
                        if(__ENVIRONMENT__=='LIVE')
                        {
                            if(doNotCreateFileForThisDomainEmail($bookingArr['szEmail']))
                            {
                                $iAddBookingFile = true;
                            }
                        }
                        else
                        {
                            $iAddBookingFile = true;
                        } 
                        
                        if($iAddBookingFile)
                        {
                            if((int)$courierBookingArr[0]['iCourierAgreementIncluded']==1)
                            {
                               $fileLogsAry = array();
                               $fileLogsAry['szTransportecaStatus'] = "S7";
                               $fileLogsAry['iLabelStatus'] = __LABEL_NOT_SEND_FLAG__;
                               $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
                               $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime; 
                               $fileLogsAry['szForwarderBookingStatus'] = '';                                    
                               $fileLogsAry['szForwarderBookingTask'] = 'T140'; 
                               $fileLogsAry['idBooking'] = (int)$idBooking;
                               $fileLogsAry['idUser'] = (int)$bookingArr['idUser'];  

                               $kBooking->insertCourierBookingFileDetails($fileLogsAry); 
                            }
                            else
                            {
                               $fileLogsAry = array();
                               $fileLogsAry['szTransportecaStatus'] = "S7";
                               $fileLogsAry['iLabelStatus'] = __LABEL_NOT_SEND_FLAG__;
                               $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
                               $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime; 
                               //$fileLogsAry['szForwarderBookingStatus'] = '';                                    
                               $fileLogsAry['szTransportecaTask'] = 'T140';  
                               $fileLogsAry['idBooking'] = (int)$idBooking;        
                               $fileLogsAry['idUser'] = (int)$bookingArr['idUser'];

                               $kBooking->insertCourierBookingFileDetails($fileLogsAry);
                            }
                        }

                        if(!empty($forwarderContactAry))
                        {
                           foreach($forwarderContactAry as $forwarderContactArys)
                           {
                                $replace_ary=array(); 
                                $szControlPanelUrl = "http://".$kForwarder->szControlPanelUrl."/booking/".$idBooking."__".md5(time())."/"; 

                                $replace_ary['szLink'] = '<a href="'.$szControlPanelUrl.'"> CLICK HERE</a>';
                                $replace_ary['szUrl'] = $szControlPanelUrl; 

                                $quotesAry =$kCourierServices->getAllBookingQuotesByFileByBookingId($idBooking);

                                $szControlPanelLabelUrl='http://'.$kForwarder->szControlPanelUrl."/pendingQuotes/createLabel/".$idBooking."__".md5(time())."/";


                                $replace_ary['szLabelLink'] = '<a href="'.$szControlPanelLabelUrl.'" target="_blank"> UPLOAD LABLES AND TRACKING NUMBER HERE</a>';
                                $replace_ary['szLabelUrl'] = $szControlPanelLabelUrl;

                                $replace_ary['szBookingRef']=$bookingArr['szBookingRef'];
                                $replace_ary['iBookingLanguage']=$bookingArr['iBookingLanguage'];
                                $replace_ary['szOriginCountry']=$bookingArr['szOriginCountry'];
                                $replace_ary['szDestinationCountry']=$bookingArr['szDestinationCountry'];
                                $replace_ary['szForwarderDispName']=$bookingArr['szForwarderDispName'];

                                $replace_ary['szFirstName'] = $acceptedByUserAry['szFirstName'];
                                $replace_ary['szLastName'] = $acceptedByUserAry['szLastName'];
                                $replace_ary['dtAgreement'] = date('d/m/Y',strtotime($acceptedByUserAry['dtAgreement']));
                                if($iProfitType==1)
                                {
                                    $kConfig = new cConfig();
                                    $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',1);
                                    $szForwarderTerms_text1 = $configLangArr[1]['szForwarderTermsText1'];
                                    $szForwarderTerms_text2 = $configLangArr[1]['szForwarderTermsText2'];
                                    $szForwarderTerms_text3 = $configLangArr[1]['szForwarderTermsText3'];
                                    $iTermsConditionForReferralForwarder =$szForwarderTerms_text1." ".$replace_ary['szTermsCoditionUrl']."".$szForwarderTerms_text2." ".$replace_ary['szFirstName']." ".$replace_ary['szLastName']." ".$szForwarderTerms_text3." ".$replace_ary['dtAgreement'];
                                    $replace_ary['szTermsConditionForReferralForwarderText']="<br>".$iTermsConditionForReferralForwarder."<br>";
                                }
                                else
                                {
                                    $replace_ary['szTermsConditionForReferralForwarderText']='';
                                }
                                
                                
                                $replace_ary['szEmail']=$forwarderContactArys;   

                                if($bookingArr['iCourierBookingType']==1)
                                {
                                    $szBookingType="Courier";
                                }
                                else
                                {
                                    $szBookingType="Courier";
                                }        
                                $replace_ary['szBookingType']=$szBookingType;

                                $replace_ary['szTermsCoditionUrl'] = '<a href="http://'.$kForwarder->szControlPanelUrl.'/T&C/">Terms & Conditions</a>';
                                $kWhsSearch = new cWHSSearch();  
                                $forwarderBookingEmail = $kWhsSearch->getManageMentVariableByDescription('__FORWARDER_BOOKING_CONFIRMATION_EMAIL__');  
                                if((int)$courierBookingArr[0]['iCourierAgreementIncluded']==1)
                                {
                                    $to = $replace_ary['szEmail'];
                                    createEmail(__COURIER_BOOKING_RECEIVED_CREATE_LABEL_FORWARDER_EMAIL__, $replace_ary, $to ,'' , $forwarderBookingEmail,$bookingArr['idForwarder'] , $forwarderBookingEmail,__FLAG_FOR_FORWARDER__);
                                }
                                else
                                {
                                    $to = $replace_ary['szEmail'];
                                    $szSupportEmail = __STORE_SUPPORT_EMAIL__;
                                    
//                                    $to = 'ajay@whiz-solutions.com';
//                                    $szSupportEmail = 'ajay@whiz-solutions.com';
                                    
                                    try
                                    {
                                        if(doNotSendMailToThisDomainEmail($to))
                                        {
                                            createEmail(__FORWADER_COURIER_BOOKING_RECEIVED__, $replace_ary, $to,'' ,__STORE_SUPPORT_EMAIL__,$bookingArr['idForwarder'] , $forwarderBookingEmail,__FLAG_FOR_FORWARDER__); 
                                        }
                                       /*
                                        * FW: E-mail change 19-01-2017
                                        */
                                        //createEmail(__CREATE_LABEL_FOR_COURIER_BY_TRANSPORTECA_EMAIL__, $replace_ary, $szSupportEmail,'' , $forwarderBookingEmail,$bookingArr['idForwarder'] , $forwarderBookingEmail,__FLAG_FOR_FORWARDER__);
                                    } 
                                    catch (Exception $ex) 
                                    {
                                         
                                    } 
                                }
                            }  
                        }
                    }
                }
                else
                {
                    if($iUpdateBankTransfer)
                    { 
                        $kConfig = new cConfig();
                        $kForwarderContact= new cForwarderContact();
                        $kUser = new cUser();
                        $kWhsSearch = new cWHSSearch();
                        $kBooking->SendTransferConfirmation($bookingArr);
                    } 
                    if($checkComapnyBankDetils)
                    {
                        $replace_ary=array();
                        $ForwarderAdminDetails = $kAdmin->findForwarderAdminsByBookingId($idForwarder);
                        $replace_ary['szDate']=findPaymentFreq($ForwarderAdminDetails[0]['szPaymentFrequency']);
                        if($ForwarderAdminDetails!= array() && $bookingArr['idBookingStatus']=='3')
                        {
                            foreach($ForwarderAdminDetails AS $key=>$value)
                            {
                                $replace_ary['szFirstName']=$value['szFirstName'];
                                $replace_ary['szDisplayName']=$value['szDisplayName'];
                                $replace_ary['szHere']='<a href ="http://'.$value['szControlPanelUrl'].'/Company/BankDetails/">here</a>';
                                $to =  $value['szEmail'];
                                //$to = 'ajay@whiz-solutions.com';
                                createEmail(__UPDATE_BANK_DETAILS_FORWARDER__, $replace_ary, $to,'' , __FINANCE_CONTACT_EMAIL__,$value['id'] , __FINANCE_CONTACT_EMAIL__,__FLAG_FOR_FORWARDER__);
                            }
                        }
                    } 
                } 
            }
        }
    }
    
    function createCustomerByAPI($postSearchAry,$mode,$iCheckFlagBookingCreated=false)
    {
        if(!empty($postSearchAry))
        {
            $addCustomerAry = array();
            if($mode=='SHIPPER')
            { 
                /*szPhoneNumberUpdate
                 * 
                 * szAddress1
                 * 
                 */
                $addCustomerAry['szCompanyName'] = $postSearchAry['szShipperCompanyName'];
                $addCustomerAry['szFirstName'] = $postSearchAry['szShipperFirstName'];
                $addCustomerAry['szLastName'] = $postSearchAry['szShipperLastName'];
                $addCustomerAry['szEmail'] = $postSearchAry['szShipperEmail'];
                $addCustomerAry['szAddress'] = $postSearchAry['szShipperAddress'];
                $addCustomerAry['szPostCode'] = $postSearchAry['szOriginPostCode'];
                $addCustomerAry['szCity'] = $postSearchAry['szOriginCity'];
                $addCustomerAry['szState'] = $postSearchAry['szShipperState'];
                $addCustomerAry['idCountry'] = $postSearchAry['idOriginCountry'];
                $addCustomerAry['szPhone'] = $postSearchAry['szShipperPhoneNumber'];
                $addCustomerAry['idInternationalDialCode'] = $postSearchAry['idShipperDialCode']; 
                $addCustomerAry['iIncompleteProfile'] = 0;
                $addCustomerAry['iConfirmed'] = 0;
                $addCustomerAry['iFirstTimePassword'] = 1;
                $addCustomerAry['iLanguage'] = $postSearchAry['iBookingLanguage'];
                $addCustomerAry['szCurrency'] = $postSearchAry['idCustomerCurrency']; 
            }
            else if($mode=='CONSIGNEE')
            {
                $addCustomerAry['szCompanyName'] = $postSearchAry['szConsigneeCompanyName'];
                $addCustomerAry['szFirstName'] = $postSearchAry['szConsigneeFirstName'];
                $addCustomerAry['szLastName'] = $postSearchAry['szConsigneeLastName'];
                $addCustomerAry['szEmail'] = $postSearchAry['szConsigneeEmail'];
                $addCustomerAry['szAddress'] = $postSearchAry['szConsigneeAddress'];
                $addCustomerAry['szPostCode'] = $postSearchAry['szDestinationPostCode'];
                $addCustomerAry['szCity'] = $postSearchAry['szDestinationCity'];
                $addCustomerAry['szState'] = $postSearchAry['szConsigneeState'];
                $addCustomerAry['idCountry'] = $postSearchAry['idDestinationCountry'];
                $addCustomerAry['szPhone'] = $postSearchAry['szConsigneePhoneNumber'];
                $addCustomerAry['idInternationalDialCode'] = $postSearchAry['idConsigneeDialCode']; 
                $addCustomerAry['iIncompleteProfile'] = 0;
                $addCustomerAry['iConfirmed'] = 0;
                $addCustomerAry['iFirstTimePassword'] = 1;
                $addCustomerAry['iLanguage'] = $postSearchAry['iBookingLanguage'];
                $addCustomerAry['szCurrency'] = $postSearchAry['idCustomerCurrency'];
            } 
            else if($mode=='BILLING')
            {
                $addCustomerAry['szCompanyName'] = $postSearchAry['szBillingCompanyName'];
                $addCustomerAry['szFirstName'] = $postSearchAry['szBillingFirstName'];
                $addCustomerAry['szLastName'] = $postSearchAry['szBillingLastName'];
                $addCustomerAry['szEmail'] = $postSearchAry['szBillingEmail'];
                $addCustomerAry['szAddress'] = $postSearchAry['szBillingAddress'];
                $addCustomerAry['szPostCode'] = $postSearchAry['szBillingPostcode'];
                $addCustomerAry['szCity'] = $postSearchAry['szBillingCity'];
                $addCustomerAry['szState'] = '';
                $addCustomerAry['idCountry'] = $postSearchAry['idBillingCountry'];
                $addCustomerAry['szPhone'] = $postSearchAry['iBillingPhoneNumber'];
                $addCustomerAry['idInternationalDialCode'] = $postSearchAry['iBillingCountryDialCode']; 
                $addCustomerAry['iIncompleteProfile'] = 0;
                $addCustomerAry['iConfirmed'] = 0;
                $addCustomerAry['iFirstTimePassword'] = 1;
                $addCustomerAry['iLanguage'] = $postSearchAry['iBookingLanguage'];
                $addCustomerAry['szCurrency'] = $postSearchAry['idCustomerCurrency'];
            }
            if($addCustomerAry)
            {
                $iAddNewUser = false;
                $kUser = new cUser();
                if($kUser->isUserEmailExist($addCustomerAry['szEmail']))
                {  
                    $idCustomer = $kUser->idIncompleteUser ;
                }  
                else
                {
                    $iAddNewUser = true;
                }
                
                if($iAddNewUser)
                { 
                    if($iCheckFlagBookingCreated){
                        $iPrivate = 0 ;
                        if(count($postSearchAry['customerType'])==1)
                        {              
                            if(strtoupper($postSearchAry['customerType'][0])==__PARTNER_API_CUSTOMER_TYPE_PRIVATE__)
                            {
                                $iPrivate = 1 ;
                            }
                        }
                        else
                        {
                            if(trim($addCustomerAry['szCompanyName'])=='')
                            {
                                $iPrivate = 1 ;
                            }
                        }
                        
                    }
                    $kBooking = new cBooking(); 
                    $szPassword = mt_rand(0,99)."".$kBooking->generateUniqueToken(8);
                    $addCustomerAry['szPhoneNumberUpdate'] = $addCustomerAry['szPhone'];
                    $addCustomerAry['szCountry'] = $addCustomerAry['idCountry'];
                    $addCustomerAry['szAddress1'] = $addCustomerAry['szAddress']; 
                    $addCustomerAry['szPassword'] = $szPassword; 
                    $addCustomerAry['szConfirmPassword'] = $szPassword;  
                    $addCustomerAry['iPrivate']=$iPrivate;
                    if($kUser->createAccount($addCustomerAry,'PARTNER_API'))
                    { 
                        $idCustomer = $kUser->idApiCustomer ;  
                    } 
                }
                else
                {  
                    if($iCheckFlagBookingCreated){
                        if(count($postSearchAry['customerType'])==1)
                        {
                            if(strtoupper($postSearchAry['customerType'][0])==__PARTNER_API_CUSTOMER_TYPE_BUSINESS__)
                            {
                                $addCustomerAry['iPrivate'] = 0;
                            }              
                            if(strtoupper($postSearchAry['customerType'][0])==__PARTNER_API_CUSTOMER_TYPE_PRIVATE__)
                            {
                                $addCustomerAry['iPrivate'] = 1 ;
                            }
                        }
                        else
                        {
                            if(trim($addCustomerAry['szCompanyName'])=='')
                            {
                                $addCustomerAry['iPrivate'] = 1 ;
                            }
                        }
                    }
                    if(!empty($addCustomerAry))
                    {
                        $update_user_query = '';
                        foreach($addCustomerAry as $key=>$value)
                        {
                            $update_user_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                        }
                        $update_user_query = rtrim($update_user_query,","); 
                        $kRegisterShipCon = new cRegisterShipCon();
                        if($kRegisterShipCon->updateUserDetailsByQuotes($update_user_query,$idCustomer))
                        {
                            //TO DO
                        } 
                    }  
                } 
                $kUser = new cUser();
                $kUser->updateLastLoginUser($idCustomer);
                return $idCustomer;
            } 
        }
    }
    
    public function userLogin($data)
    {
        if(!empty($data))
        {  
            $this->set_szUserName(trim(sanitize_all_html_input($data['szUserName'])));
            $this->set_szPassword(trim(sanitize_all_html_input($data['szPassword'])));
            
            if($this->error==true)
            {
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage();  
            }  
            
            $kUser = new cUser();
            $loginAry = array();
            $loginAry['szEmail'] = $this->szUserName;
            $loginAry['szPassword'] = $this->szPassword;
            $kUser->userLogin($loginAry,true);
            
            if($kUser->iUserSuccessfullyLogin==1 && $kUser->idCustomer>0)
            { 
                $updateUserExpiryArr=array();
                $updateUserExpiryArr['szToken']=self::$szGlobalToken;
                $updateUserExpiryArr['idCustomer']=$kUser->idCustomer;
                $updateUserExpiryArr['iLoginType']=1;
                $kUser->updateCustomerExpiryTime($updateUserExpiryArr);
                               
                $idCustomer = $kUser->idCustomer;
                $kUser = new cUser();
                $userDataAry = array();
                $userDataAry = $kUser->getUserDetailsApi($idCustomer);
              
                if(!empty($userDataAry))
                {  
                    $updateUserExpiryArr=array();
                    $updateUserExpiryArr['szToken']=self::$szGlobalToken;
                    $updateUserExpiryArr['idCustomer']=$idCustomer;
                    $kUser->updateCustomerExpiryTime($updateUserExpiryArr);
                
                    //self::$szNotification = "E1125"; 
                    self::$arrServiceListAry = $userDataAry;
                    self::$szGlobalErrorType = 'SUCCESS'; 
                    return self::buildSuccessMessage('customer');   
                }
                else
                {
                    $errorFlag = true;
                    $this->buildErrorDetailAry('szCommunication','INVALID');   
                    self::$arrErrorMessageAry = $this->error_detail_ary; 
                    self::$szGlobalErrorType = 'VALIDATION';  
                    return self::buildErrorMessage(); 
                } 
            }
            else
            {
                $errorFlag = true;
                $this->buildErrorDetailAry('szInvalidUserNamePassword','AUTHENTICATION');   
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage(); 
            }
        }
    } 
    public function validateCustomerData($data,$szMode='CUSTOMER_SIGNUP')
    {
        if(!empty($data))
        {     
            if($szMode=='UPDATE_CUSTOMER_PROFILE')
            {
                $idCustomer = (int)$data['idCustomer'];
            }
            else
            {
                $idCustomer = false;
            }
            
            $this->set_szFirstName(trim(sanitize_all_html_input($data['szFirstName'])));
            $this->set_szLastName(trim(sanitize_all_html_input($data['szLastName'])));
            $this->set_szEmail(trim(sanitize_all_html_input($data['szEmail']))); 
            $this->set_iPhoneNumber(trim(sanitize_all_html_input($data['szPhone']))); 
            $this->set_iCountryDialCode(trim(sanitize_all_html_input($data['iInternationDialCode'])));
            if(strtolower($data['isPrivate'])=='yes' || (is_numeric($data['isPrivate']) && $data['isPrivate']==1))
            {
                $this->szCompanyName = $this->szFirstName." ".$this->szLastName; 
                $this->szCompanyRegNo = "Private";
                $this->iPrivateCustomer = 1;
                
            }
            else
            {
                $this->set_szCompanyName(trim(sanitize_all_html_input($data['szCompanyName']))); 
                $this->szCompanyRegNo = trim(sanitize_all_html_input($data['szCompanyRegNo']));
                $this->iPrivateCustomer = 0;
            }  
            
            if($szMode=='CUSTOMER_SIGNUP')
            {
                $this->set_szPassword(trim(sanitize_all_html_input($data['szPassword'])));
                $this->set_szConfirmPassword(trim(sanitize_all_html_input($data['szConfirmPassword'])));
            } 
            $this->set_szAddress(trim(sanitize_all_html_input($data['szAddress'])));
            $this->set_szPostCode(trim(sanitize_all_html_input($data['szPostCode']))); 
            $this->set_szCity(trim(sanitize_all_html_input($data['szCity'])));
            $this->set_szCountry(trim(sanitize_all_html_input($data['szCountry'])));
            $this->set_szCurrency(trim(sanitize_all_html_input($data['szCurrency'])),false);
            $this->set_szLanguage(trim(sanitize_all_html_input($data['szLanguage'])),false);
            $this->iAcceptNewsUpdate=trim(sanitize_all_html_input((int)$data['iAcceptNewsUpdate']));
            
            if($this->error==true)
            {
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return false;
            }  
            else
            {  
                $errorFlag = false;
                if($szMode=='CUSTOMER_SIGNUP')
                {
                    if(!empty($this->szPassword) && $this->szPassword!=$this->szConfirmPassword)
                    { 
                        $this->buildErrorDetailAry('szConfirmPassword','INVALID');   
                        $errorFlag = true;
                    }
                } 
                $kConfigNew =new cConfig();
                if(!empty($this->iCountryDialCode))
                {
                    $kConfigNew->loadCountry(false,false,__LANGUAGE_ID_ENGLISH__,$this->iCountryDialCode);
                    $this->iCountryDialCode=$kConfigNew->idCountry;
                }
                
                $kConfig =new cConfig();
                if(!empty($this->szLanguage))
                { 
                    $langArr = array(); 
                    $langArr = $kConfig->getLanguageDetails(strtolower($this->szLanguage));
                  
                    if(!empty($langArr))
                    {
                        $data['szLanguage'] = strtoupper($langArr[0]['szLanguageCode']); 
                    } 
                }  
                if(!empty($this->szCurrency))
                {
                    $currencyAry = array();
                    $currencyAry = $kConfig->getBookingCurrency(false,true,false,false,$this->szCurrency,true); 
                       
                    if(!empty($currencyAry))
                    {
                        $this->idCustomerCurrency = $currencyAry[0]['id']; 
                    }
                }
                
                
                                 
                $kConfig->loadCountry(false,$this->szCountry);  
                if($kConfig->idCountry>0)
                { 
                    $this->idCustomerCountry = $kConfig->idCountry;
                    $this->szCustomerCountryName = $kConfig->szCountryName;
                    if($this->idCustomerCurrency<=0)
                    {
                        /*
                        * If we don't get Currency code in API request then we have used default Currency of the country
                        */
                        $this->idCustomerCurrency = $kConfig->iDefaultCurrency;
                    }
                    if($this->idCustomerLanguage<=0)
                    {
                        /*
                        * If we don't get Language code in API request then we have used default Language of the country
                        */
                        $this->idCustomerLanguage = $kConfig->iDefaultLanguage;
                    } 
                } 
                else
                {  
                    $errorFlag = true;
                    $this->buildErrorDetailAry('szCountry','INVALID');   
                }  
                
                $kUserNew = new cUser();
                if($kUserNew->isUserEmailExist($this->szEmail,$idCustomer))
                { 
                    if((int)$kUserNew->idUserSignupApi==0)
                    {
                        $errorFlag = true;
                        $this->buildErrorDetailAry('szEmail','ALREADY_EXISTS');
                    }
                    else
                    {
                       $this->idUserForSignUpApi= $kUserNew->idUserSignupApi;
                    }                                     
                }   
                if($errorFlag)
                {
                    self::$arrErrorMessageAry = $this->error_detail_ary; 
                    self::$szGlobalErrorType = 'VALIDATION'; 
                    return false;
                }
                else
                {
                    return true;
                }
            }    
        }
    }
    function updateCustomerAccount($data)
    {
        if(!empty($data))
        {
            $szReferenceToken = self::$szGlobalToken; 
            $kTransportecaApi = new cTransportecaApi();
            $kTransportecaApi->isTokenValid($szReferenceToken); 
            $idCustomer = $kTransportecaApi->idApiCustomer;
             
            if($idCustomer>0)
            {
                $data['idCustomer'] = $idCustomer;
                $this->validateCustomerData($data,'UPDATE_CUSTOMER_PROFILE'); 
                if($this->error==true)
                { 
                    return self::buildErrorMessage();  
                } 
                $kUser = new cUser();
                $customerSignupAry = array();
                $customerSignupAry['szFirstName'] = $this->szFirstName;
                $customerSignupAry['szLastName'] = $this->szLastName;  
                $customerSignupAry['szEmail'] = $this->szEmail; 
                $customerSignupAry['iPrivate'] = $this->iPrivateCustomer; 
                $customerSignupAry['szCompanyName'] = $this->szCompanyName;
                $customerSignupAry['szCompanyRegNo'] = $this->szCompanyRegNo;
                $customerSignupAry['idInternationalDialCode'] = $this->iCountryDialCode;
                $customerSignupAry['szPhone'] = $this->iPhoneNumber;
                $customerSignupAry['szAddress'] = $this->szAddress;   
                $customerSignupAry['szPostCode'] = $this->szPostCode; 
                $customerSignupAry['szCity'] = $this->szCity;
                $customerSignupAry['idCountry'] = $this->idCustomerCountry;
                $customerSignupAry['iLanguage'] = $this->idCustomerLanguage;
                $customerSignupAry['szCurrency'] = $this->idCustomerCurrency;
                $customerSignupAry['iAcceptNewsUpdate'] = $this->iAcceptNewsUpdate; 
 
                $update_user_query = '';
                if(!empty($customerSignupAry))
                { 
                    foreach($customerSignupAry as $key=>$value)
                    {
                        $update_user_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                } 
                $kRegisterShipCon = new cRegisterShipCon();
                $update_user_query = rtrim($update_user_query,",");  
                if($kRegisterShipCon->updateUserDetailsByQuotes($update_user_query,$idCustomer))
                {
                    $updateUserExpiryArr=array();
                    $updateUserExpiryArr['szToken']=self::$szGlobalToken;
                    $updateUserExpiryArr['idCustomer']=$kUser->idApiCustomer; 

                    $kUser->updateCustomerExpiryTime($updateUserExpiryArr);

                    self::$szNotification = "E1138"; 
                    $succesResponseAry['szEmail'] = $kUser->szEmail;
                    self::$arrServiceListAry = $succesResponseAry;
                    self::$szGlobalErrorType = 'SUCCESS'; 
                    return self::buildSuccessMessage('dummy');  
                }  
                else
                {
                    $errorFlag = true;
                    $this->buildErrorDetailAry('szCommunication','INVALID');   
                    self::$arrErrorMessageAry = $this->error_detail_ary; 
                    self::$szGlobalErrorType = 'VALIDATION';  
                    return self::buildErrorMessage(); 
                } 
            }
            else
            {
                $errorFlag = true;
                $this->buildErrorDetailAry('szReferenceToken','EXPIRED');   
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage();
            }
        }
    }
    
    public function userSignup($data)
    {
        if(!empty($data))
        {   
            $this->validateCustomerData($data); 
            if($this->error==true)
            { 
                return self::buildErrorMessage();  
            }   
            $kUser = new cUser();
            $customerSignupAry = array();
            $customerSignupAry['szFirstName'] = $this->szFirstName;
            $customerSignupAry['szLastName'] = $this->szLastName;  
            $customerSignupAry['szEmail'] = $this->szEmail;
            $customerSignupAry['szPassword'] = $this->szPassword;
            $customerSignupAry['szConfirmPassword'] = $this->szConfirmPassword;
            $customerSignupAry['szCity'] = $this->szCity;
            $customerSignupAry['szCountry'] = $this->idCustomerCountry;
            $customerSignupAry['szCompanyName'] = $this->szCompanyName;
            $customerSignupAry['szCompanyRegNo'] = $this->szCompanyRegNo;
            $customerSignupAry['szPhoneNumberUpdate'] = $this->iPhoneNumber;
            $customerSignupAry['szAddress1'] = $this->szAddress;   
            $customerSignupAry['szPostCode'] = $this->szPostCode;
            $customerSignupAry['idInternationalDialCode'] = $this->iCountryDialCode;
            $customerSignupAry['iLanguage'] = $this->idCustomerLanguage;
            $customerSignupAry['szCurrency'] = $this->idCustomerCurrency;
            $customerSignupAry['iPrivate'] = $this->iPrivateCustomer; 
            $customerSignupAry['iSendUpdate'] = $this->iAcceptNewsUpdate;
            $customerSignupAry['iSignUp'] = 1;
            
            if((int)$this->idUserForSignUpApi>0)
            {
                $customerSignupAry['iUpdateExistUserNotCreatedFromSignApi'] = 1;
                $customerSignupAry['idUserForSignUpApi'] = $this->idUserForSignUpApi;
            }
            $kUser->createAccount($customerSignupAry,"PARTNER_API"); 
            
           
            if($kUser->idApiCustomer>0)
            {
                $idCustomer = $kUser->idApiCustomer; 
                $userDataAry = array();
                $userDataAry = $kUser->getUserDetailsApi($idCustomer);
                    
                $updateUserExpiryArr=array();
                $updateUserExpiryArr['szToken']=self::$szGlobalToken;
                $updateUserExpiryArr['idCustomer']=$kUser->idApiCustomer;
                $updateUserExpiryArr['iLoginType']=2;
                
                $kUser->updateCustomerExpiryTime($updateUserExpiryArr);
               
                //self::$szNotification = "E1125"; 
                $succesResponseAry['szEmail'] = $kUser->szEmail;
                self::$arrServiceListAry = $userDataAry;
                self::$szGlobalErrorType = 'SUCCESS'; 
                return self::buildSuccessMessage('customer');  
            }
            else
            {
                $errorFlag = true;
                $this->buildErrorDetailAry('szCommunication','INVALID');   
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage(); 
            }
        }
    }
    
    public function userBookingHistory($data,$flag='All')
    {
        if(!empty($data))
        {  
            $szReferenceToken = self::$szGlobalToken; 
            $kTransportecaApi = new cTransportecaApi();
            $kTransportecaApi->isTokenValid($szReferenceToken); 
            $idCustomer = $kTransportecaApi->idApiCustomer;
            if($idCustomer>0)
            {
                $updateUserExpiryArr=array();
                $updateUserExpiryArr['szToken']=self::$szGlobalToken;
                $updateUserExpiryArr['idCustomer']=$idCustomer;
                $kUser = new cUser();
                $kUser->updateCustomerExpiryTime($updateUserExpiryArr);
                    
                $kBilling = new cBilling();
                $succesResponse_Ary=$kBilling->userBookingDataList($idCustomer,$flag);
                if($succesResponse_Ary['szErrorMsg'])
                {
                    $this->buildErrorDetailAry($succesResponse_Ary['szKey'],$succesResponse_Ary['szValue']);   
                    self::$arrErrorMessageAry = $this->error_detail_ary; 
                    self::$szGlobalErrorType = 'VALIDATION';  
                    return self::buildErrorMessage(); 
                }
                else
                {
                    self::$arrServiceListAry = $succesResponse_Ary;
                    self::$szGlobalErrorType = 'SUCCESS'; 
                    return self::buildSuccessMessage('customer_booking_history');
                }
            }
            else
            {
                $errorFlag = true;
                $this->buildErrorDetailAry('szReferenceToken','EXPIRED');   
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage();
            }
        }
    }
    
    /*
    * @params Request Array
    * @author Ajay
    */
    function getUserProfileDetails($data)
    {
        if(!empty($data))
        { 
            $szReferenceToken = self::$szGlobalToken; 
            $kTransportecaApi = new cTransportecaApi();
            $kTransportecaApi->isTokenValid($szReferenceToken); 
            $idCustomer = $kTransportecaApi->idApiCustomer;
           
            if($idCustomer>0)
            {
                $kUser = new cUser();
                $userDataAry = array();
                $userDataAry = $kUser->getUserDetailsApi($idCustomer);
              
                if(!empty($userDataAry))
                {  
                    $updateUserExpiryArr=array();
                    $updateUserExpiryArr['szToken']=self::$szGlobalToken;
                    $updateUserExpiryArr['idCustomer']=$idCustomer;
                    $kUser->updateCustomerExpiryTime($updateUserExpiryArr);
                
                    self::$arrServiceListAry = $userDataAry;
                    self::$szGlobalErrorType = 'SUCCESS'; 
                    return self::buildSuccessMessage('customer');  
                }
                else
                {
                    $errorFlag = true;
                    $this->buildErrorDetailAry('szCommunication','INVALID');   
                    self::$arrErrorMessageAry = $this->error_detail_ary; 
                    self::$szGlobalErrorType = 'VALIDATION';  
                    return self::buildErrorMessage(); 
                }
            }
            else
            {
                $errorFlag = true;
                $this->buildErrorDetailAry('szReferenceToken','EXPIRED');   
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage(); 
            } 
        }
    }
    
    public function changePassword($data)
    {
        if(!empty($data))
        {
            $szReferenceToken = self::$szGlobalToken; 
            $kTransportecaApi = new cTransportecaApi();
            $kTransportecaApi->isTokenValid($szReferenceToken); 
            $idCustomer = $kTransportecaApi->idApiCustomer;
            
            $kUser = new cUser();
                
            $updateUserExpiryArr=array();
            $updateUserExpiryArr['szToken']=self::$szGlobalToken;
            $updateUserExpiryArr['idCustomer']=$idCustomer;
            $kUser->updateCustomerExpiryTime($updateUserExpiryArr);
            
            if($idCustomer>0)
            {
                
                $this->set_szCurrentPassword(trim(sanitize_all_html_input($data['szCurrentPassword'])));
                $this->set_szPassword(trim(sanitize_all_html_input($data['szPassword'])));
                $this->set_szConfirmPassword(trim(sanitize_all_html_input($data['szConfirmPassword'])));

                $errorFlag = false;
                $updatePassword = false;
                if($this->error==true)
                { 
                    self::$arrErrorMessageAry = $this->error_detail_ary; 
                    self::$szGlobalErrorType = 'VALIDATION';  
                    return self::buildErrorMessage(); 
                }  
                else
                {   
                    if(!empty($this->szPassword) && $this->szPassword!=$this->szConfirmPassword)
                    { 
                        $this->buildErrorDetailAry('szConfirmPassword','INVALID');   
                        $errorFlag = true;
                    } 
                }
                
                
                if(!$errorFlag)
                { 
                    if($kUser->validatePassword($idCustomer,$this->szCurrentPassword))
                    {
                        $updatePassword = true;
                    }
                    else if($kUser->iCustomerPasswordNotMatched==1)
                    {
                        $this->buildErrorDetailAry('szCurrentPassword','INVALID');   
                        $errorFlag = true;
                    }
                    else if($kUser->iCustomerNotFound==1)
                    {
                        $this->buildErrorDetailAry('szCommunication','INVALID');   
                        $errorFlag = true;
                    }
                }
                
                if($errorFlag)
                {
                    self::$arrErrorMessageAry = $this->error_detail_ary; 
                    self::$szGlobalErrorType = 'VALIDATION';  
                    return self::buildErrorMessage(); 
                }
                else if($updatePassword)
                {
                    if($kUser->updateUserPassword($idCustomer,$this->szPassword))
                    {
                        self::$szNotification = "E1129"; 
                        $succesResponseAry['szEmail'] = $kUser->szEmail;
                        self::$arrServiceListAry = $succesResponseAry;
                        self::$szGlobalErrorType = 'SUCCESS'; 
                        return self::buildSuccessMessage('dummy');  
                    }
                    else
                    {
                        $errorFlag = true;
                        $this->buildErrorDetailAry('szCommunication','INVALID');   
                        self::$arrErrorMessageAry = $this->error_detail_ary; 
                        self::$szGlobalErrorType = 'VALIDATION';  
                        return self::buildErrorMessage(); 
                    }
                }
            } 
            else
            {
                $errorFlag = true;
                $this->buildErrorDetailAry('szReferenceToken','EXPIRED');   
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage(); 
            }
        } 
    }
    
    public function forgotPassword($data)
    {
        if(!empty($data))
        { 
            $this->set_szEmail(trim(sanitize_all_html_input($data['szEmail']))); 
            $this->set_szLanguage(trim(sanitize_all_html_input($data['szLanguage']))); 

            $errorFlag = false;
            $updatePassword = false;
            if($this->error==true)
            { 
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage(); 
            }   

            $kUser = new cUser(); 
            if($kUser->isUserEmailExist($this->szEmail))
            { 
                $idCustomer = $kUser->idIncompleteUser;
            }
            else
            {
                $this->buildErrorDetailAry('szEmail','NOT_EXISTS');   
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage();
            }  
            
            $kConfig =new cConfig();
            if(!empty($this->szLanguage))
            { 
                $langArr = array(); 
                $langArr = $kConfig->getLanguageDetails(strtolower($this->szLanguage));

                if(!empty($langArr))
                {
                    $idLanguage = strtoupper($langArr[0]['id']); 
                } 
                else
                {
                    $errorFlag=true;
                    $this->buildErrorDetailAry('szLanguage','INVALID');                    
                    self::$arrErrorMessageAry = $this->error_detail_ary; 
                    self::$szGlobalErrorType = 'VALIDATION';  
                    return self::buildErrorMessage();
                }
            } 

            $bPasswordSentSuccessFully = false;
            if($idCustomer>0)
            {
                if($kUser->sendForgotPasswordEmail($idCustomer,$idLanguage))
                {
                    $bPasswordSentSuccessFully = true;
                }
                else
                {
                    $errorFlag = true; 
                } 
            }
            else
            {
                $errorFlag = true; 
            }
            
            if($bPasswordSentSuccessFully)
            {
                self::$szNotification = "E1135"; 
                $succesResponseAry['szEmail'] = $kUser->szEmail;
                self::$arrServiceListAry = $succesResponseAry;
                self::$szGlobalErrorType = 'SUCCESS'; 
                return self::buildSuccessMessage('dummy');
            }
            else
            {
                $this->buildErrorDetailAry('szCommunication','INVALID');   
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage();
            }
        } 
    }
    
    /* 
     * Deleting Customer Account from Transporteca
     * @param $data array
     * @return array
     */
    function deleteCustomerAccount($data)
    {
        if(!empty($data))
        {  
            $szReferenceToken = self::$szGlobalToken; 
            $kTransportecaApi = new cTransportecaApi();
            $kTransportecaApi->isTokenValid($szReferenceToken); 
            $idCustomer = $kTransportecaApi->idApiCustomer;
           
            if($idCustomer>0)
            {
                $kUser = new cUser();
                $updateUserExpiryArr=array();
                $updateUserExpiryArr['szToken']=self::$szGlobalToken;
                $updateUserExpiryArr['idCustomer']=$idCustomer;
                $kUser->updateCustomerExpiryTime($updateUserExpiryArr);
                
                if($this->checkDeleteUserAccountForApi($data,$idCustomer))
                {
                   if($kUser->deactivateUserAccount($idCustomer,true))
                   {
                        self::$szNotification = "E1137";
                        $succesResponseAry['szEmail'] = $data['szEmail'];
                        self::$arrServiceListAry = $succesResponseAry;
                        self::$szGlobalErrorType = 'SUCCESS'; 
                        return self::buildSuccessMessage('dummy');
                   }
                }
                else
                {
                    self::$arrErrorMessageAry = $this->error_detail_ary; 
                    self::$szGlobalErrorType = 'VALIDATION';  
                    return self::buildErrorMessage(); 
                }
                
                
                
            }
            else
            {
                $errorFlag = true;
                $this->buildErrorDetailAry('szReferenceToken','EXPIRED');   
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage();
            }
        }
    }
    
    
    public function checkDeleteUserAccountForApi($data,$idCustomer)
    {
        
        if(!empty($data))
        {
           
            if(is_array($data))
            {
                $this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szEmail']))));
                $this->set_szNonFrocePassword(trim(sanitize_all_html_input($data['szPassword'])));

               			
                //exit if error exist.
                if($this->error==true)
                {  
                    return false;
                }

                $query="
                    SELECT
                        szPassword
                    FROM
                        ".__DBC_SCHEMATA_USERS__."
                    WHERE
                        szEmail='".mysql_escape_custom($this->szEmail)."'
                    AND
                        iActive = '1'
                    AND
                        id='".(int)$idCustomer."'
                ";
                            //echo $query;
                if( ( $result = $this->exeSQL( $query ) ) )
                {
                    if( $this->getRowCnt() > 0 )
                    {
                        $row=$this->getAssoc($result);
                        $password=$row['szPassword'];
                        $idUser=$row['id'];
                        $iConfirmed=$row['iConfirmed'];
                    }
                    else
                    {
                        $this->buildErrorDetailAry('szDeleteUserEmailPassword','INVALID_EMAIL_PASSWORD');   
                        return false;
                    }
                }				
                if($password!=md5($data['szPassword']))
                {             
                    $this->buildErrorDetailAry('szDeleteUserEmailPassword','INVALID_EMAIL_PASSWORD'); 
                    return false;
                }
                else
                {
                  return true;
                }
            }
            else
            {
                return false;
            }
        }
    }
    
    function getAllRegisterdShipperConsigneeList($data,$flag)
    {
        if(!empty($data))
        {  
            $szReferenceToken = self::$szGlobalToken; 
            $kTransportecaApi = new cTransportecaApi();
            $kTransportecaApi->isTokenValid($szReferenceToken); 
            $idCustomer = $kTransportecaApi->idApiCustomer;
           
            if($idCustomer>0)
            {
                $updateUserExpiryArr=array();
                $updateUserExpiryArr['szToken']=self::$szGlobalToken;
                $updateUserExpiryArr['idCustomer']=$idCustomer;
                $kUser = new cUser();
                $kUser->updateCustomerExpiryTime($updateUserExpiryArr);
                
                
                $kRegisterShipCon = new cRegisterShipCon();
                $kUser->getUserDetails($idCustomer);
                
                $userMutliArr=$kUser->getMultiUserData($kUser->idGroup);
                if(!empty($userMutliArr))
                {
                    foreach($userMutliArr as $userMutliArrs)
                    {
                            $idUserArr[]=$userMutliArrs['id'];
                    }
                }
                
                if(!empty($idUserArr))
                {
                    $userStr=implode(",",$idUserArr);
                }
                else
                {
                    $userStr=$idCustomer;
                }
                $szCountryCode=trim($data['szCountryCode']);
                if($szCountryCode!='')
                {
                    $kConfig = new cConfig();
                    $kConfig->loadCountry(false,$szCountryCode);
                    if((int)$kConfig->idCountry>0){
                        $idCountry=$kConfig->idCountry;
                    }
                }
                $szCompanyName = trim($data['szCompanyName']);
                $szCity = trim($data['szCity']);
                $szPostcode = trim($data['szPostcode']);
                $regShipperArr=$kRegisterShipCon->getRegisteredShipperConsigness_bookingDetails($flag,$userStr,$szPostcode,$idCountry,$szCity,$szCompanyName);
                if(!empty($regShipperArr))
                {
                    $ctr=0;
                    foreach($regShipperArr as $regShipperArrs)
                    {
                        if($flag=='ship')
                        {
                            $regShipperListArr[$ctr]['idShipper']=$regShipperArrs['id'];
                        }
                        else
                        {
                            $regShipperListArr[$ctr]['idConsignee']=$regShipperArrs['id'];
                        }
                        $regShipperListArr[$ctr]['szCompanyName']= cApi::encodeString($regShipperArrs['szCompanyName']);
                        $regShipperListArr[$ctr]['szCountryName']= cApi::encodeString($regShipperArrs['szCountryName']);
                        ++$ctr;
                    }
                    
                    
                    self::$arrServiceListAry = $regShipperListArr;
                    self::$szGlobalErrorType = 'SUCCESS'; 
                    if($flag=='ship')
                    {
                        return self::buildSuccessMessage('customer_registered_shipper');
                    }
                    else
                    {
                        return self::buildSuccessMessage('customer_registered_consignee');
                    }
                }
                else
                {
                    $errorFlag = true;
                    if($flag=='ship')
                    {
                        $this->buildErrorDetailAry('iRegisteredShipper','NOT_FOUND');
                    }
                    else
                    {
                        $this->buildErrorDetailAry('iRegisteredConsignee','NOT_FOUND');
                    }
                    self::$arrErrorMessageAry = $this->error_detail_ary; 
                    self::$szGlobalErrorType = 'VALIDATION';  
                    return self::buildErrorMessage();
                }
            }
            else
            {
                $errorFlag = true;
                $this->buildErrorDetailAry('szReferenceToken','EXPIRED');   
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage();
            }
        }
    }
    
    
    function getRegisterdShipperConsigneeDetails($data,$flag)
    {
        if(!empty($data))
        {  
            $szReferenceToken = self::$szGlobalToken; 
            $kTransportecaApi = new cTransportecaApi();
            $kTransportecaApi->isTokenValid($szReferenceToken); 
            $idCustomer = $kTransportecaApi->idApiCustomer;
           
            if($idCustomer>0)
            {
                $updateUserExpiryArr=array();
                $updateUserExpiryArr['szToken']=self::$szGlobalToken;
                $updateUserExpiryArr['idCustomer']=$idCustomer;
                $kUser = new cUser();
                $kUser->updateCustomerExpiryTime($updateUserExpiryArr);
                
                $kUser->getUserDetails($idCustomer);
                
                $userMutliArr=$kUser->getMultiUserData($kUser->idGroup);
                if(!empty($userMutliArr))
                {
                    foreach($userMutliArr as $userMutliArrs)
                    {
                            $idUserArr[]=$userMutliArrs['id'];
                    }
                }

                if(!empty($idUserArr))
                {
                    $userStr=implode(",",$idUserArr);
                }
                else
                {
                    $userStr=$idCustomer;
                }
                if($flag=='ship')
                {
                    if((int)$data['idShipper']>0)
                    {
                         $kRegisterShipCon = new cRegisterShipCon();
                         $regShipperArr=$kRegisterShipCon->getRegisteredShipperConsigness($flag,$userStr,false,false,false,$data['idShipper']);
                         if(!empty($regShipperArr))
                         {
                            $shipperDetailArr=$kRegisterShipCon->getShipperConsignessDetail($data['idShipper']);
                            
                            $shipperDetail_Arr['szEmail'] = $shipperDetailArr['szEmail'];
                            $shipperDetail_Arr['id'] = $shipperDetailArr['id'];
                            $shipperDetail_Arr['idGroup'] = $shipperDetailArr['idShipConGroup'];
                            $shipperDetail_Arr['szFirstName'] = cApi::encodeString($shipperDetailArr['szFirstName']);
                            $shipperDetail_Arr['szLastName'] = cApi::encodeString($shipperDetailArr['szLastName']);
                            $shipperDetail_Arr['szCompanyName']= cApi::encodeString($shipperDetailArr['szCompanyName']);
                            $shipperDetail_Arr['szAddress1']= cApi::encodeString($shipperDetailArr['szAddress']);
                            $shipperDetail_Arr['szAddress2']= cApi::encodeString($shipperDetailArr['szAddress2']);
                            $shipperDetail_Arr['szAddress3']= cApi::encodeString($shipperDetailArr['szAddress3']);
                            $shipperDetail_Arr['szCity']= cApi::encodeString($shipperDetailArr['szCity']);
                            $shipperDetail_Arr['szState']= cApi::encodeString($shipperDetailArr['szState']);
                            $shipperDetail_Arr['szCountry']=$shipperDetailArr['idCountry'];
                            $shipperDetail_Arr['szPhoneNumber']=$shipperDetailArr['szPhone'];
                            $shipperDetail_Arr['iInternationalDialCode']=$shipperDetailArr['iInternationalDialCode']; 
                            $shipperDetail_Arr['szPostcode']=$shipperDetailArr['szPostCode'];
                            $iActiveStr='Inactive';
                            if($shipperDetailArr['iActive']==1)
                            {
                                $iActiveStr="Active";
                            }
                            $shipperDetail_Arr['Status']=$iActiveStr;
                            $shipperDetail_Arr['dtCreatedOn']=$shipperDetailArr['dtCreatedOn'];
                            
                            
                            self::$arrServiceListAry = $shipperDetail_Arr;
                            self::$szGlobalErrorType = 'SUCCESS'; 
                            return self::buildSuccessMessage('customer_registered_shipper_detail');
                            
                             
                         }
                         else
                         {
                            $errorFlag = true;
                            $this->buildErrorDetailAry('idShipper','INVALID');   
                            self::$arrErrorMessageAry = $this->error_detail_ary; 
                            self::$szGlobalErrorType = 'VALIDATION';  
                            return self::buildErrorMessage();
                         }
                    }
                    else
                    {
                        $errorFlag = true;
                        $this->buildErrorDetailAry('idShipper','INVALID');   
                        self::$arrErrorMessageAry = $this->error_detail_ary; 
                        self::$szGlobalErrorType = 'VALIDATION';  
                        return self::buildErrorMessage();
                    }
                }
                else if($flag=='con')
                {
                    if((int)$data['idConsignee']>0)
                    {
                         $kRegisterShipCon = new cRegisterShipCon();
                         $regShipperArr=$kRegisterShipCon->getRegisteredShipperConsigness($flag,$userStr,false,false,false,$data['idConsignee']);
                         if(!empty($regShipperArr))
                         {
                            $shipperDetailArr=$kRegisterShipCon->getShipperConsignessDetail($data['idConsignee']);
                            
                            $shipperDetail_Arr['szEmail']=$shipperDetailArr['szEmail'];
                            $shipperDetail_Arr['id']=$shipperDetailArr['id'];
                            $shipperDetail_Arr['idGroup']=$shipperDetailArr['idShipConGroup'];
                            $shipperDetail_Arr['szFirstName']= cApi::encodeString($shipperDetailArr['szFirstName']);
                            $shipperDetail_Arr['szLastName']= cApi::encodeString($shipperDetailArr['szLastName']);
                            $shipperDetail_Arr['szCompanyName']= cApi::encodeString($shipperDetailArr['szCompanyName']);
                            $shipperDetail_Arr['szAddress1']= cApi::encodeString($shipperDetailArr['szAddress']);
                            $shipperDetail_Arr['szAddress2']= cApi::encodeString($shipperDetailArr['szAddress2']);
                            $shipperDetail_Arr['szAddress3']= cApi::encodeString($shipperDetailArr['szAddress3']);
                            $shipperDetail_Arr['szCity']= cApi::encodeString($shipperDetailArr['szCity']);
                            $shipperDetail_Arr['szState']= cApi::encodeString($shipperDetailArr['szState']);
                            $shipperDetail_Arr['szCountry']=$shipperDetailArr['idCountry'];
                            $shipperDetail_Arr['szPhoneNumber']=$shipperDetailArr['szPhone'];
                            $shipperDetail_Arr['iInternationalDialCode']=$shipperDetailArr['iInternationalDialCode']; 
                            $shipperDetail_Arr['szPostcode']=$shipperDetailArr['szPostCode'];
                            $iActiveStr='Inactive';
                            if($shipperDetailArr['iActive']==1)
                            {
                                $iActiveStr="Active";
                            }
                            $shipperDetail_Arr['Status']=$iActiveStr;
                            $shipperDetail_Arr['dtCreatedOn']=$shipperDetailArr['dtCreatedOn'];
                            
                            
                            self::$arrServiceListAry = $shipperDetail_Arr;
                            self::$szGlobalErrorType = 'SUCCESS'; 
                            return self::buildSuccessMessage('customer_registered_consignee_detail');
                            
                             
                         }
                         else
                         {
                            $errorFlag = true;
                            $this->buildErrorDetailAry('idConsignee','INVALID');   
                            self::$arrErrorMessageAry = $this->error_detail_ary; 
                            self::$szGlobalErrorType = 'VALIDATION';  
                            return self::buildErrorMessage();
                         }
                    }
                    else
                    {
                        $errorFlag = true;
                        $this->buildErrorDetailAry('idConsignee','INVALID');   
                        self::$arrErrorMessageAry = $this->error_detail_ary; 
                        self::$szGlobalErrorType = 'VALIDATION';  
                        return self::buildErrorMessage();
                    }
                }
            }
            else
            {
                $errorFlag = true;
                $this->buildErrorDetailAry('szReferenceToken','EXPIRED');   
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage();
            }
        }
    }
    
    function validateShipperConsigneeData($data,$flag)
    {
        $this->set_szCompanyName(trim(sanitize_all_html_input($data['szCompanyName'])));
        $this->set_szFirstName(trim(sanitize_all_html_input($data['szFirstName'])));
        $this->set_szLastName(trim(sanitize_all_html_input($data['szLastName'])));
        $this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szEmail']))));	 
        $this->set_szCountry(trim(sanitize_all_html_input($data['szCountry'])));
        $this->set_iCountryDialCode(trim(sanitize_all_html_input($data['iInternationalDialCode']))); 
        $this->set_szCity(trim(sanitize_all_html_input($data['szCity'])));
        $this->set_szState(trim(sanitize_all_html_input($data['szState'])));
        $this->set_szAddress(trim(sanitize_all_html_input($data['szAddress'])));
        $this->set_iPhoneNumber(trim(sanitize_all_html_input($data['szPhoneNumber'])));
        $this->set_szAddress2(trim(sanitize_all_html_input($data['szAddress2'])));
        $this->set_szAddress3(trim(sanitize_all_html_input($data['szAddress3'])));
         $this->set_szPostcode(trim(sanitize_all_html_input($data['szPostCode'])));
        //$this->set_id($data['idShipperConsigness']);
        $this->set_idGroup(trim(sanitize_all_html_input(strtolower($data['idGroup']))));
        
        if($this->error==true)
        {
            self::$arrErrorMessageAry = $this->error_detail_ary; 
            self::$szGlobalErrorType = 'VALIDATION';  
            return false;
        }
        $kConfig = new cConfig();
        $kConfig->loadCountry(false,$this->szCountry);  
        if($kConfig->idCountry>0)
        { 
            $this->idCustomerCountry = $kConfig->idCountry;
            $this->szCustomerCountryName = $kConfig->szCountryName;
            if($this->idCustomerCurrency<=0)
            {
                /*
                * If we don't get Currency code in API request then we have used default Currency of the country
                */
                $this->idCustomerCurrency = $kConfig->iDefaultCurrency;
            }
            if($this->idCustomerLanguage<=0)
            {
                /*
                * If we don't get Language code in API request then we have used default Language of the country
                */
                $this->idCustomerLanguage = $kConfig->iDefaultLanguage;
            } 
        } 
        else
        {  
            $errorFlag = true;
            $this->buildErrorDetailAry('szCountry','INVALID');   
        }
    }
    function updateShipperConsigneeDetails($data,$flag,$addUpdateFlag='ADD')
    {
        if(!empty($data))
        {
            $szReferenceToken = self::$szGlobalToken; 
            $kTransportecaApi = new cTransportecaApi();
            $kTransportecaApi->isTokenValid($szReferenceToken); 
            $idCustomer = $kTransportecaApi->idApiCustomer;
             
            if($idCustomer>0)
            {
                $data['idCustomer'] = $idCustomer;
                
                $updateUserExpiryArr=array();
                $updateUserExpiryArr['szToken']=self::$szGlobalToken;
                $updateUserExpiryArr['idCustomer']=$idCustomer;
                $kUser = new cUser();
                $kUser->updateCustomerExpiryTime($updateUserExpiryArr);
                
                $kUser->getUserDetails($idCustomer);
                
                $userMutliArr=$kUser->getMultiUserData($kUser->idGroup);
                if(!empty($userMutliArr))
                {
                    foreach($userMutliArr as $userMutliArrs)
                    {
                            $idUserArr[]=$userMutliArrs['id'];
                    }
                }

                if(!empty($idUserArr))
                {
                    $userStr=implode(",",$idUserArr);
                }
                else
                {
                    $userStr=$idCustomer;
                }
                
                if($addUpdateFlag=='UPDATE')
                {
                    if($flag=='ship')
                    {
                        if((int)$data['idShipper']>0)
                        {
                             $idShipperConsignee=$data['idShipper'];
                             $kRegisterShipCon = new cRegisterShipCon();
                             $regShipperArr=$kRegisterShipCon->getRegisteredShipperConsigness('',$userStr,false,false,false,$data['idShipper']);
                             if(empty($regShipperArr))
                             {
                                $errorFlag = true;
                                $this->buildErrorDetailAry('idShipper','INVALID');   
                                self::$arrErrorMessageAry = $this->error_detail_ary; 
                                self::$szGlobalErrorType = 'VALIDATION';  
                                return self::buildErrorMessage();
                             }
                        }
                        else
                        {
                            $errorFlag = true;
                            $this->buildErrorDetailAry('idShipper','INVALID');   
                            self::$arrErrorMessageAry = $this->error_detail_ary; 
                            self::$szGlobalErrorType = 'VALIDATION';  
                            return self::buildErrorMessage();
                        }
                    }
                    else if($flag=='con')
                    {
                        if((int)$data['idConsignee']>0)
                        {
                            $idShipperConsignee=$data['idConsignee'];
                             $kRegisterShipCon = new cRegisterShipCon();
                             $regShipperArr=$kRegisterShipCon->getRegisteredShipperConsigness('',$userStr,false,false,false,$data['idConsignee']);
                             if(empty($regShipperArr))
                             {
                                $errorFlag = true;
                                $this->buildErrorDetailAry('idConsignee','INVALID');   
                                self::$arrErrorMessageAry = $this->error_detail_ary; 
                                self::$szGlobalErrorType = 'VALIDATION';  
                                return self::buildErrorMessage();
                             }
                        }
                        else
                        {
                            $errorFlag = true;
                            $this->buildErrorDetailAry('idConsignee','INVALID');   
                            self::$arrErrorMessageAry = $this->error_detail_ary; 
                            self::$szGlobalErrorType = 'VALIDATION';  
                            return self::buildErrorMessage();
                        }
                    }
                }
                
                if(strtolower($data['iRegisteredType'])=='shipper')
                {
                    $data['idGroup'] = 1;
                }
                else if(strtolower($data['iRegisteredType'])=='consignee')
                {
                    $data['idGroup'] = 2;
                }
                else if(strtolower($data['iRegisteredType'])=='both')
                {
                    $data['idGroup'] = 3;
                }
                $this->validateShipperConsigneeData($data,'UPDATE_DETAIL'); 
                if($this->error==true)
                { 
                    return self::buildErrorMessage();  
                }
                
                $kUser = new cUser();
                $shipconSignupAry = array();
                $shipconSignupAry['szFirstName'] = $this->szFirstName;
                $shipconSignupAry['szLastName'] = $this->szLastName;  
                $shipconSignupAry['szEmail'] = $this->szEmail;  
                $shipconSignupAry['szCompanyName'] = $this->szCompanyName;
                $shipconSignupAry['idCountry'] = $this->idCustomerCountry;
                $shipconSignupAry['iInternationalDialCode'] = $this->iCountryDialCode;
                $shipconSignupAry['szPhone'] = $this->iPhoneNumber;
                $shipconSignupAry['szAddress'] = $this->szAddress;   
                $shipconSignupAry['szPostCode'] = $this->szPostCode; 
                $shipconSignupAry['szCity'] = $this->szCity;
                $shipconSignupAry['szState'] = $this->szState;
                $shipconSignupAry['szAddress3'] = $this->szAddress3;
                $shipconSignupAry['szAddress2'] = $this->szAddress2; 
                $shipconSignupAry['idShipConGroup'] = $this->idGroup; 
 
                $update_shipcon_query = '';
                if(!empty($shipconSignupAry))
                { 
                    foreach($shipconSignupAry as $key=>$value)
                    {
                        $update_shipcon_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                } 
                
                $kRegisterShipCon = new cRegisterShipCon();
                $update_shipcon_query = rtrim($update_shipcon_query,",");
                if($kRegisterShipCon->updateShipperConsigneeApiDetails($update_shipcon_query,$idShipperConsignee))
                {
                    
                    if($flag=='ship')
                    {
                        self::$szNotification = "E1144"; 
                    }
                    else if($flag=='con')
                    {
                        self::$szNotification = "E1145"; 
                    }
                    $succesResponseAry['szEmail'] = $kUser->szEmail;
                    self::$arrServiceListAry = $succesResponseAry;
                    self::$szGlobalErrorType = 'SUCCESS'; 
                    return self::buildSuccessMessage('dummy');
                }
            }
            else
            {
                $errorFlag = true;
                $this->buildErrorDetailAry('szReferenceToken','EXPIRED');   
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage();
            }
        }
    }
    
    public function getForwarderRating($data)
    {
        if(!empty($data))
        { 
            $this->set_szForwarderAlies(trim(sanitize_all_html_input($data['szForwarderAlias'])));
              
            if($this->error==true)
            {
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return false;
            }
            $kForwarder = new cForwarder();
            $kWHSSearch = new cWHSSearch();
            
            $szForwarderAlies = $this->szForwarderAlies; 
            $kForwarder->load(false,$szForwarderAlies);
          
            if($kForwarder->id>0)
            {
                $idForwarder = $kForwarder->id;
                $forwarderRatingAry = array();
                $forwarderReviewsAry = array();
                $reviewDetailsAry = array();
                /*
                * Fetching Average rating of forwarder
                */
                $forwarderRatingAry = $kWHSSearch->getForwarderAvgRating($idForwarder);
                /*
                * Fetching all the reviews of forwarder
                */
                $forwarderReviewsAry = $kWHSSearch->getForwarderAvgReviews($idForwarder);
                
                /*
                * Fetching all the historical reviews of forwarder
                */ 
                $reviewDetailsAry = $kForwarder->getForwardRating($idForwarder,'24',true);
                
                $fAverageRating = $forwarderRatingAry['fAvgRating'];
                $iTotalNumRating = $forwarderRatingAry['iNumRating'];
                $iTotalNumReviews = $forwarderReviewsAry['iNumReview'];
                
                $ratingResponseAry = array();
                $ratingResponseAry['fAverageRating'] = round((float)$fAverageRating,2);
                $ratingResponseAry['iTotalNumRating'] = $iTotalNumRating; 
                //$ratingResponseAry['iTotalNumReviews'] = $iTotalNumReviews; 
                
                $reviewTempAry = array();
                $ctr=0;
                if(!empty($reviewDetailsAry))
                {
                    foreach($reviewDetailsAry as $iNumRating=>$reviewDetailsArys)
                    { 
                        $reviewTempAry[$ctr]['iStarRating'] = $iNumRating; 
                        if(!empty($reviewDetailsArys))
                        { 
                            $ctr_1=0;
                            foreach($reviewDetailsArys as $reviewDetailsAryss)
                            { 
                                $szReview = trim($reviewDetailsAryss['szReview']);
                                if(!empty($szReview))
                                {
                                    $reviewTempAry[$ctr]['description'][$ctr_1]['szReview'] = cApi::encodeString($szReview);
                                    $reviewTempAry[$ctr]['description'][$ctr_1]['dtDateTime'] = $reviewDetailsAryss['dtCompleted'];
                                    $ctr_1++;  
                                } 
                            }
                        } 
                        $ctr++;
                    }
                }
                if(!empty($reviewTempAry))
                {
                    $ratingResponseAry['reviews'] = $reviewTempAry;
                }    
                self::$arrServiceListAry = $ratingResponseAry;
                self::$szGlobalErrorType = 'SUCCESS'; 
                return self::buildSuccessMessage('forwarder_rating_reviews');
            }
            else
            {
                $errorFlag = true;
                $this->buildErrorDetailAry('szForwarderAlies','REQUIRED');   
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage();
            }
        }
    }
    
    
    public function getInsuranceValue($data)
    {
        if(!empty($data))
        { 
            $this->validateInsuranceDetails($data); 
            if($this->error==true)
            { 
                return self::buildErrorMessage();
            } 
            $iCheckForSalesRate=false;
            $iNoSalesRateFound=true;
            $szReferenceTokenTemp = self::$szGlobalToken; 
            $iCheckForSalesRateForAutomaticeBooking=false;
            if(self::$szApiRequestMode==__API_REQUEST_MODE_LIVE__ && !empty($this->szPriceToken) && (!empty($this->szQuoteID) || !empty($this->szServiceID)))
            {
                $iCheckForSalesRate=true;
                /*
                * Updating service pricing in same way what we do at old /service/ page by clicking on 'Book Now' button.
                */
                $responseArr=array();
                self::$szCalledFromAPIType = "INSURANCE";
                $kServices = new cServices();
                $responseArr=$kServices->updateServicePricingByInsuranceAPI($this->szPriceToken,$this->szServiceID,$this->szQuoteID);
                if(!empty($this->szPriceToken) && !empty($this->szServiceID))
                {
                    $iCheckForSalesRateForAutomaticeBooking=true;
                }
            }
            self::$szGlobalToken = $szReferenceTokenTemp;
            
            $kVatApplication = new cVatApplication();
            $fApplicableVatRate = 0;  
            
            $insuranceDataAry = array(); 
            $insuranceDataAry['idOriginCountry'] = $this->idOriginCountry;
            $insuranceDataAry['idDestinationCountry'] = $this->idDestinationCountry;
            $insuranceDataAry['idTransportMode'] = $this->idTransportMode ;
            $insuranceDataAry['szCargoType'] = $this->szCargoType ;
 
            $kInsurance = new cInsurance();
            $buyRateListAry = array();
            $buyRateListAry = $kInsurance->getBuyrateCombination($insuranceDataAry,__INSURANCE_BUY_RATE_TYPE__);
            
            $insuranceBuyrateListAry = array();
            $insuranceBuyrateListAry = $buyRateListAry[$this->idTransportMode];
            
            $insuranceResponseAry = array();
            $insuranceBuyResponseAry = array();
            $insuranceSellResponseAry = array(); 
            $currencyExchangeAry = array();
            $alreadyUsedCurrencyAry = array();
             
            $ctr = 0;
            if(!empty($insuranceBuyrateListAry))
            { 
                // No need to loop through because we are getting single buy rate row.
                $insuranceBuyrateListArys = $insuranceBuyrateListAry;
                $idInsuranceBuyRate = $insuranceBuyrateListArys['id']; 
                $insuranceBuyResponseAry[$ctr]['upto'] = $insuranceBuyrateListArys['fInsuranceUptoPrice'];
                $insuranceBuyResponseAry[$ctr]['currency'] = $insuranceBuyrateListArys['szInsuranceCurrency']; 
                $insuranceBuyResponseAry[$ctr]['rate'] = $insuranceBuyrateListArys['fInsuranceRate'];
                $insuranceBuyResponseAry[$ctr]['min'] = $insuranceBuyrateListArys['fInsuranceMinPrice'];  
                $ctr++; 
                $bInsuranceTariffApplicable = false;
                /*
                * We only search for sells traffis only if we get atleast 1 matching record for buy rate
                */
                if($idInsuranceBuyRate>0)
                {
                    /*
                    * Finding out Insurance sell rate tariffs
                    */

                    $insuranceSellRateAry = array();
                    $insuranceSellRateAry = $kInsurance->getBuyrateCombination($insuranceDataAry,__INSURANCE_SELL_RATE_TYPE__,true); 

                    $insuranceSellRateAry = $insuranceSellRateAry[$this->idTransportMode] ;
                    if(!empty($insuranceSellRateAry))
                    {
                        $ctr=0;
                        $bInsuranceTariffApplicable = true;
                        foreach($insuranceSellRateAry as $insuranceSellRateArys)
                        { 
                            $insuranceSellResponseAry[$ctr]['upto']=$insuranceSellRateArys['fInsuranceUptoPrice'];
                            $insuranceSellResponseAry[$ctr]['currency']=$insuranceSellRateArys['szInsuranceCurrency']; 
                            $insuranceSellResponseAry[$ctr]['rate']=$insuranceSellRateArys['fInsuranceRate'];
                            $insuranceSellResponseAry[$ctr]['min']=$insuranceSellRateArys['fInsuranceMinPrice']; 
                            ++$ctr;        
                        } 
                    }
                } 
                
                $kWHSSearch = new cWHSSearch();          
                $currencyExchangeAry = array();
                $currencyExchangeAry = $kWHSSearch->getCurrencyDetails(false,false,true,false,true);
              
                if($bInsuranceTariffApplicable)
                {
                    $insuranceResponseAry = array();
                    if($iCheckForSalesRateForAutomaticeBooking)
                    {
                        $responseArr1['bInsuranceAvailable']='Optional';
                        $responseArr = array_merge($responseArr1,$responseArr);
                    }
                    $iNoSalesRateFound=false;
                    $insuranceResponseAry= $responseArr;
                    $insuranceResponseAry['salesTarifs'] = $insuranceSellResponseAry;
                    $insuranceResponseAry['buyTarif'] = $insuranceBuyResponseAry;
                    $insuranceResponseAry['exchangeRates'] = $currencyExchangeAry;                    
                    
                    self::$arrServiceListAry = $insuranceResponseAry;
                    self::$szGlobalErrorType = 'SUCCESS'; 
                    return self::buildSuccessMessage('insurance_data');
                }
                else
                {
                    if($iCheckForSalesRate)
                    {
                        if($iCheckForSalesRateForAutomaticeBooking)
                        {
                            $responseArr1['bInsuranceAvailable']='No';
                            $responseArr = array_merge($responseArr1,$responseArr);
                        }
                        else if($iNoSalesRateFound)
                        {
                            $responseArr['bInsuranceAvailable']='No';
                        }
                        $insuranceResponseAry= $responseArr;
                        self::$arrServiceListAry = $insuranceResponseAry;
                        self::$szGlobalErrorType = 'SUCCESS'; 
                        return self::buildSuccessMessage('insurance_data');
                    }
                    else
                    {
                        $this->buildErrorDetailAry('iInsuranceRate','NOT_FOUND');
                        self::$arrErrorMessageAry = $this->error_detail_ary; 
                        self::$szGlobalErrorType = 'VALIDATION';  
                        return self::buildErrorMessage();
                    }
                }  
            }
            else
            {
                if($iCheckForSalesRate)
                {
                    if($iCheckForSalesRateForAutomaticeBooking)
                    {
                        $responseArr1['bInsuranceAvailable']='No';
                        $responseArr = array_merge($responseArr1,$responseArr);
                    }
                    else if($iNoSalesRateFound)
                    {
                        $responseArr['bInsuranceAvailable']='No';
                    }
                    $insuranceResponseAry= $responseArr;
                    self::$arrServiceListAry = $insuranceResponseAry;
                    self::$szGlobalErrorType = 'SUCCESS'; 
                    return self::buildSuccessMessage('insurance_data');
                }
                else
                {
                    $this->buildErrorDetailAry('iInsuranceRate','NOT_FOUND');
                    self::$arrErrorMessageAry = $this->error_detail_ary; 
                    self::$szGlobalErrorType = 'VALIDATION';  
                    return self::buildErrorMessage();
                }
            } 
        } 
    }
    
    function validateInsuranceDetails($data)
    { 
        if(!empty($data))
        {  
            $this->set_szOriginCountryStr(sanitize_all_html_input(trim($data['szOriginCountry'])));
            $this->set_szDestinationCountryStr(sanitize_all_html_input(trim($data['szDestinationCountry'])));
            $this->set_szTransportMode(sanitize_all_html_input(trim($data['szTransportMode'])));
            $this->set_szCargoType(sanitize_all_html_input(trim((int)$data['szCargoType']))); 
              
            if($this->error==true)
            {
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return false;
            }
            else
            {
                $this->szPriceToken = sanitize_all_html_input(trim($data['szPriceToken']));
                $this->szServiceID = sanitize_all_html_input(trim($data['szServiceID']));
                $this->szQuoteID = sanitize_all_html_input(trim($data['szQuoteID'])); 
                
                $errorFlag=false;
                $kConfig = new cConfig(); 
                if($this->szOriginCountryStr!='')
                {  
                    $kConfig->loadCountry(false,$this->szOriginCountryStr);
                    $idOriginCountry = $kConfig->idCountry ;                     
                }                
                if($idOriginCountry>0)
                {
                    $this->idOriginCountry = $idOriginCountry;
                }
                else
                {
                    $this->buildErrorDetailAry('szOriginCountryStr','INVALID');
                    $errorFlag = true;
                }
                
                if($this->szDestinationCountryStr!='')
                {  
                    $kConfig->loadCountry(false,$this->szDestinationCountryStr);
                    $idDestinationCountry = $kConfig->idCountry ;                     
                }
                if($idDestinationCountry>0)
                {
                    $this->idDestinationCountry = $idDestinationCountry;
                }
                else
                {
                    $this->buildErrorDetailAry('szDestinationCountryStr','INVALID');
                    $errorFlag = true;
                }   
                $kVatApplication = new cVatApplication();
                if((int)$this->szCargoType>0)
                {  
                    $szCargoTypeCode = $kVatApplication->getManualFeeCaroTypeList(true,'',$this->szCargoType);               
                }     
                if(!empty($szCargoTypeCode))
                {
                    $this->szCargoType = $szCargoTypeCode;
                }
                else
                {
                    $this->buildErrorDetailAry('szCargoType','INVALID');
                    $errorFlag = true;
                }
                
                if($this->szTransportMode!='')
                {  
                    $idTransportMode=$kConfig->getTransportIDByName($this->szTransportMode);               
                }                
                if((int)$idTransportMode>0)
                {
                    $this->idTransportMode = $idTransportMode;
                }
                else
                {
                    $this->buildErrorDetailAry('szTransportMode','INVALID');
                    $errorFlag = true;
                }
                
                /*$currencyBookingAry = $kConfig->getBookingCurrency(false,true,false,false,$this->szBookingCurrency,true); 
                if(!empty($currencyBookingAry))
                {
                    $this->idBookingCurrency=$currencyBookingAry[0]['id'];
                }
                else
                {
                    $this->buildErrorDetailAry('szBookingCurrency','INVALID');
                    $errorFlag = true;
                }
                
                $currencyCustomerAry = $kConfig->getBookingCurrency(false,true,false,false,$this->szCustomerCurrency,true); 
                if(!empty($currencyCustomerAry))
                {
                    $this->idCustomerCurrency=$currencyCustomerAry[0]['id'];
                }
                else
                {
                    $this->buildErrorDetailAry('szCustomerCurrency','INVALID');
                    $errorFlag = true;
                }
                
                $currencyGoodsInsuranceAry = $kConfig->getBookingCurrency(false,true,false,false,$this->szGoodsInsuranceCurrency,true); 
                if(!empty($currencyGoodsInsuranceAry))
                {
                    $this->idGoodsInsuranceCurrency=$currencyGoodsInsuranceAry[0]['id'];
                }
                else
                {
                    $this->buildErrorDetailAry('szGoodsInsuranceCurrency','INVALID');
                    $errorFlag = true;
                }*/
                
                
                if($this->szCustomerType!='')
                {
                    if($this->szCustomerType==__PARTNER_API_CUSTOMER_TYPE_PRIVATE__)
                    {
                        $this->iPrivateShipping=1;
                    }
                    else if($this->szCustomerType==__PARTNER_API_CUSTOMER_TYPE_BUSINESS__)
                    {
                        $this->iPrivateShipping=0;
                    }
                    else
                    {
                        $this->buildErrorDetailAry('szCustomerType','INVALID');
                        $errorFlag = true;
                    }
                }
                
                if($errorFlag)
                {
                   
                    self::$arrErrorMessageAry = $this->error_detail_ary; 
                    self::$szGlobalErrorType = 'VALIDATION';  
                    return false;
                }
                else
                {
                    return true;
                }
                
                
            } 
        }
    }
    
    public function confirmPayment($data,$mode)
    { 
        if(!empty($data))
        {    
            $kWHSSearch=new cWHSSearch();   
            $kConfig = new cConfig(); 
            $result_Ary = array();   
            self::$szDeveloperNotes .= ">. Initiated confirm payment for booking with szBookingRef: ".$data['szBookingRef'].PHP_EOL; 
            /*
            *  Validating request parameters
            */
            $this->set_szPaymentType(trim(sanitize_all_html_input($data['szPaymentType'])));  
            $this->set_szBookingRef(trim(sanitize_all_html_input($data['szBookingRef'])));  
            if($this->szPaymentType==__TRANSPORTECA_PAYMENT_TYPE_1__)
            {
                //$this->set_szStripeToken(trim(sanitize_all_html_input($data['szStripeToken'])));  
                $this->set_idTransaction(trim(sanitize_all_html_input($data['idTransaction'])));  
            }    
            else if(!empty($this->szPaymentType))
            {
                $validPaymentTypeAry = array(__TRANSPORTECA_PAYMENT_TYPE_1__);
                if(!in_array($this->szPaymentType,$validPaymentTypeAry))
                {
                    $errorFlag = true;
                    $this->buildErrorDetailAry('szPaymentType','INVALID');
                }
            }
            if($this->error==true)
            {       
                self::$szDeveloperNotes .= ">. Confirm payment validation failed.".PHP_EOL; 
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage();   
            }  
            
            $kBooking = new cBooking();
            $szBookingRef = $this->szBookingRef;
            $szPaymentType = $this->szPaymentType;
            $szStripeToken = $this->szStripeToken;
            
            $bookingData = array();
            $bookingData = $kBooking->load(false,$szBookingRef);     
            if($bookingData['id']>0)
            {
                $idBooking = $bookingData['id']; 
                self::$szDeveloperNotes .= ">. Booking found by using this szBookingRef. Going forward".PHP_EOL; 
                $bPaymentAcceptFlag = true; 
                if($bookingData['idBookingStatus']!=__BOOKING_STATUS_CONFIRMED__ && $bookingData['idBookingStatus']!=__BOOKING_STATUS_ARCHIVED__)
                { 
                    self::$szDeveloperNotes .=  ">. To accept payment for a booking via API, Booking must be marked as 'Confirmed' or 'Archived'".PHP_EOL;  
                    $bPaymentAcceptFlag = false;
                }
                if($bookingData['iBookingType']==__BOOKING_TYPE_RFQ__)
                {
                    self::$szDeveloperNotes .=  ">. We coulnot accept payments for Manual RFQs by this method.".PHP_EOL;  
                    $bPaymentAcceptFlag = false;
                }
                if($bookingData['iPaymentType']!=__TRANSPORTECA_PAYMENT_TYPE_3__)
                {
                    self::$szDeveloperNotes .=  ">. Payment type of the booking is not 'Transfer'".PHP_EOL;  
                    $bPaymentAcceptFlag = false;
                }
                if($bookingData['iTransferConfirmed']!=1)
                {
                    self::$szDeveloperNotes .=  ">. Booking has already been marked as a Paid ".PHP_EOL;  
                    $bPaymentAcceptFlag = false;
                }   
                if($bPaymentAcceptFlag)
                {
                    $bookingDataAry = array();
                    $bookingDataAry = $kBooking->getExtendedBookingDetails($idBooking);
                    
                    $stripeApiResponseAry = array();
                    $bReceivedPaymentSuccessFlag = false;
                    if($szPaymentType==__TRANSPORTECA_PAYMENT_TYPE_1__)
                    {
                        $kZoozLib = new cZoozLib(); 
                        $stripeApiResponseAry = $kZoozLib->processBookingPaymentByStripeToken($bookingDataAry,$szStripeToken,'API');
                        if(trim($stripeApiResponseAry['szPaymentStatus'])=='Completed')
                        {
                            $bReceivedPaymentSuccessFlag = true;
                            /*
                            * Updating booking file status and task status depending on 
                            */
                            $kBilling = new cBilling();
                            $kBilling->confirmBookingAfterPayment($bookingDataAry,$szPaymentType);
                        }
                    } 
                    
                    if($bReceivedPaymentSuccessFlag)
                    {
                        $confirmPayment = array();
                        $confirmPayment['szPaymentStatus'] = $stripeApiResponseAry['szPaymentStatus'];
                        $confirmPayment['szPaymentDescription'] = $stripeApiResponseAry['szPaymentDescription'];
                        $confirmPayment['szStripeTransactionID'] = $stripeApiResponseAry['szStripeTransactionID'];
                        $confirmPayment['szStripeBalanceTransactionID'] = $stripeApiResponseAry['szStripeBalanceTransactionID'];
                        
                        //Payment successfully accepted  
                        self::$arrServiceListAry = $confirmPayment;
                        self::$szGlobalErrorType = 'SUCCESS'; 
                        return self::buildSuccessMessage('confirmPayment');
                    }
                    else
                    {
                        $confirmPayment = array();
                        $confirmPayment['szPaymentStatus'] = $stripeApiResponseAry['szPaymentStatus'];
                        $confirmPayment['szPaymentDescription'] = $stripeApiResponseAry['szPaymentDescription']; 
                        
                        self::$szNotification = "E1178";
                        self::$arrErrorMessageAry = $confirmPayment; 
                        self::$szGlobalErrorType = 'VALIDATION';  
                        return self::buildErrorMessage();  
                    }
                }
                else
                {
                    $errorFlag = true;
                    $this->buildErrorDetailAry('szBookingPaid','INVALID');
                }
            } 
            else
            {
                self::$szDeveloperNotes .= ">. No booking found by using this szBookingRef.".PHP_EOL; 
                $errorFlag = true;
                $this->buildErrorDetailAry('szBookingRef','INVALID');
            } 
            if($errorFlag)
            {
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage();  
            }
        }
    }
    
    public function createAutomaticBooking($postSearchAry,$mode='')
    {
        if(!empty($postSearchAry))
        {
            $iCheckBillingAddress=false;
            $iAddNewUser = false;
            $szCustomerToken = $postSearchAry['szCustomerToken'];
            if(!empty($szCustomerToken))
            {
                /*
                * If we receive szCustomerToken in API request then we check if customer token is valid then using that customer for the booking  
                */
                $kBooking = new cBooking();
                $kTransportecaApi = new cTransportecaApi();
                $kTransportecaApi->isTokenValid($szCustomerToken); 
                $idCustomer = $kTransportecaApi->idApiCustomer;
            }
            
            if($idCustomer<=0 && !empty($postSearchAry['szBillingEmail']))
            { 
                /*
                * If we don't find  customer by token then we checks if customer is exists by szBillingEmail
                */
                $kUser = new cUser();
                if($kUser->isUserEmailExist($postSearchAry['szBillingEmail']))
                {  
                    $idCustomer = $kUser->idIncompleteUser ;
                    if($mode=='PRICE')
                    {
                        $iCheckBillingAddress=true;
                    }
                } 
                else
                {
                    /*
                    * If we don't find customer by either szCustomerToken or szBillingEmail then creates a new customer in the system
                    */
                    $iAddNewUser = true;
                }
            }
            
            if($idCustomer>0)
            {
                $kUserNew = new cUser();
                if($kUserNew->isUserEmailExist($postSearchAry['szBillingEmail'],$idCustomer))
                {  
                    /*
                    *  checking other user exists with the same email.
                    */
                    if((int)$kUserNew->idUserSignupApi==0)
                    {
//                        $errorFlag = true;
//                        $this->buildErrorDetailAry('szEmail','ALREADY_EXISTS');
                        return false;
                    }
                } 
            }
            $szBillingEmail=$postSearchAry['szBillingEmail'];
            $szReferenceToken = self::$szGlobalToken; 
            self::$szCalledFromAPIType = "PRICE";
            
            if($idCustomer>0)
            { 
                if(!empty($szCustomerToken))
                {
                    $updateUserExpiryArr=array();
                    $updateUserExpiryArr['szToken'] = $szCustomerToken ;
                    $updateUserExpiryArr['idCustomer'] = $idCustomer;
                    $kUser = new cUser();
                    $kUser->updateCustomerExpiryTime($updateUserExpiryArr); 
                }                                 
                /*
                * Updating account language of user
                */
                $updateUserDetailsAry = array();
                $updateUserDetailsAry['iLanguage'] = $postSearchAry['iBookingLanguage'] ; 
                
                /*
                 * Changes Done For API-57
                 */
                //if($iCheckBillingAddress)
                //{
                    if(trim($postSearchAry['szBillingCompanyName'])!='')
                    {
                        $updateUserDetailsAry['szCompanyName'] = $postSearchAry['szBillingCompanyName'] ;   
                    }
                    
                    if(trim($postSearchAry['szBillingFirstName'])!='')
                    {
                        $updateUserDetailsAry['szFirstName'] = $postSearchAry['szBillingFirstName'] ;  
                    }
                    if(trim($postSearchAry['szBillingLastName'])!='')
                    {
                        $updateUserDetailsAry['szLastName'] = $postSearchAry['szBillingLastName'] ; 
                    }
                    if(trim($postSearchAry['iBillingCountryDialCode'])!='')
                    {
                        $updateUserDetailsAry['idInternationalDialCode'] = $postSearchAry['iBillingCountryDialCode'] ; 
                    }
                    
                    if(trim($postSearchAry['iBillingPhoneNumber'])!='')
                    {
                        $updateUserDetailsAry['szPhone'] = $postSearchAry['iBillingPhoneNumber'] ;
                    }
                    
                    if(trim($postSearchAry['szBillingAddress'])!='')
                    {
                        $updateUserDetailsAry['szAddress'] = $postSearchAry['szBillingAddress'] ;
                    }
                    
                    if(trim($postSearchAry['szBillingCity'])!='')
                    {
                        $updateUserDetailsAry['szCity'] = $postSearchAry['szBillingCity'] ;
                    }
                    
                    if((int)$postSearchAry['idBillingCountry']>0)
                    {
                        $updateUserDetailsAry['idCountry'] = $postSearchAry['idBillingCountry'] ;
                    }
                    
                    if(trim($postSearchAry['szBillingPostcode'])!='')
                    {
                        $updateUserDetailsAry['szPostCode'] = $postSearchAry['szBillingPostcode'] ;
                    } 
                    if(count($postSearchAry['customerType'])==1)
                    {
                        if(strtoupper($postSearchAry['customerType'][0])==__PARTNER_API_CUSTOMER_TYPE_BUSINESS__)
                        {
                            $updateUserDetailsAry['iPrivate'] = 0;
                        }              
                        if(strtoupper($postSearchAry['customerType'][0])==__PARTNER_API_CUSTOMER_TYPE_PRIVATE__)
                        {
                            $updateUserDetailsAry['iPrivate'] = 1 ;
                        }
                    }
                    else
                    {
                        if(trim($postSearchAry['szBillingCompanyName'])=='')
                        {
                            $updateUserDetailsAry['iPrivate'] = 1 ;
                        }
                    }
                //} 
                if(!empty($updateUserDetailsAry))
                {
                    $update_user_query = '';
                    foreach($updateUserDetailsAry as $key=>$value)
                    {
                        $update_user_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                }  
                $kRegisterShipCon = new cRegisterShipCon();
                $update_user_query = rtrim($update_user_query,",");  
                $kRegisterShipCon->updateUserDetailsByQuotes($update_user_query,$idCustomer); 
            }
            
            
            if($iAddNewUser)
            {
                /*
                * creating new customer for the booking.
                */
                $iPrivate = 0 ;
                if(count($postSearchAry['customerType'])==1)
                {              
                    if(strtoupper($postSearchAry['customerType'][0])==__PARTNER_API_CUSTOMER_TYPE_PRIVATE__)
                    {
                        $iPrivate = 1 ;
                    }
                }
                else
                {
                    if(trim($postSearchAry['szBillingCompanyName'])=='')
                    {
                        $iPrivate = 1 ;
                    }
                }
                $billingDataAry = array();
                $billingDataAry['szBillingCompanyName'] = $postSearchAry['szBillingCompanyName'];
                $billingDataAry['szBillingFirstName'] = $postSearchAry['szBillingFirstName'];
                $billingDataAry['szBillingLastName'] = $postSearchAry['szBillingLastName'];
                $billingDataAry['iBillingCountryDialCode'] = $postSearchAry['iBillingCountryDialCode'];
                $billingDataAry['iBillingPhoneNumber'] = $postSearchAry['iBillingPhoneNumber'];
                $billingDataAry['szBillingEmail'] = $postSearchAry['szBillingEmail'];
                $billingDataAry['szBillingAddress'] = $postSearchAry['szBillingAddress'];
                $billingDataAry['szBillingPostcode'] = $postSearchAry['szBillingPostcode'];
                $billingDataAry['szBillingCity'] = $postSearchAry['szBillingCity'];
                $billingDataAry['idBillingCountry'] = $postSearchAry['idBillingCountry'];
                $billingDataAry['iShipperConsigneeType'] = $postSearchAry['iShipperConsigneeType'];
                $billingDataAry['iPrivate'] = $iPrivate;
                $billingDataAry['iBookingLanguage'] = $postSearchAry['iBookingLanguage'];
                /*
                * Updating Customer Details to bookings
                */
                $kCustomerEvents = new cCustomerEevent();
                $kCustomerEvents->updateCustomerDetailsToBooking($billingDataAry,$idBooking);
                $idCustomer = $kCustomerEvents->idCustomer;
            } 
            if($mode=='PRICE')
            {
                $iAddCompareButtonData=true;
                if((__ENVIRONMENT__=='LIVE' || __ENVIRONMENT__=='DEV_AWS')  && $szBillingEmail!='')
                {
                    $iAddCompareButtonData=false;
                    if(doNotCreateFileForThisDomainEmail($szBillingEmail))
                    {
                        $iAddCompareButtonData = true;
                    }
                }
                if($iAddCompareButtonData){
                    $compareBookingAry=array();
                    $compareBookingAry['idUser'] = $idCustomer ;
                    $compareBookingAry['idOriginCountry'] = $postSearchAry['idOriginCountry'] ;
                    $compareBookingAry['szOriginCountry'] = $postSearchAry['szOriginCountry'] ;
                    $compareBookingAry['idDestinationCountry'] = $postSearchAry['idDestinationCountry'] ;
                    $compareBookingAry['szDestinationCountry'] = $postSearchAry['szDestinationCountry'] ;
                    $compareBookingAry['szReferenceToken'] = $szReferenceToken;
                    $kBooking = new cBooking();
                    $kBooking->addCompareBooking($compareBookingAry);
                }
            }
            if($idCustomer>0)
            {                
                /*
                * At this point of time we only creates booking if we have idCustomer available for booking.
                */
                $searchResultArr=$this->serviceFoundForVogaQuote;
                $kServices = new cServices();
                $kServices->addAutomaticBookingDetailsByCustomerID($szReferenceToken,$postSearchAry,$idCustomer,$searchResultArr);  
            }
        }
    } 
    
    function customerEvents($data)
    {
        if(!empty($data))
        {    
            $this->validateCustomerEvents($data); 
            if($this->error==true)
            {    
                return self::buildErrorMessage(); 
            }    
            $kCustomerEvent = new cCustomerEevent(); 
            if($this->szEventType=='PriceRequestContact')
            { 
                $szReferenceToken = cPartner::$szGlobalToken;
                $data['szCargoType'] = $this->szCargoType;
                $data['iBillingCountryDialCode'] = $this->iBillingCountryDialCode;  
                $data['iPrivate'] = $this->iPrivateCustomer;  
                $data['szBillingCompanyName'] = $this->szBillingCompanyName;   
                if($kCustomerEvent->priceRequestContact($data))
                { 
                    $szCustomerNotes = $kCustomerEvent->getLastBookingsDetailsByCustomer($kCustomerEvent->idCustomerEvent);
            
                    cPartner::$szGlobalToken = $szReferenceToken;
                    $customerEvent['szMessage'] = "Event successfully executed.";
                    $customerEvent['szCustomerNotes'] = $szCustomerNotes;
                    //Payment successfully accepted  
                    self::$arrServiceListAry = $customerEvent;
                    self::$szGlobalErrorType = 'SUCCESS'; 
                    return self::buildSuccessMessage('customerEvent');
                }
                else
                {
                    cPartner::$szGlobalToken = $szReferenceToken;
                    self::$szGlobalErrorType = 'VALIDATION';  
                    return self::buildErrorMessage(); 
                } 
            } 
        }
    }
    
    public function validateCustomerEvents($data)
    {
        if(!empty($data))
        {
            $this->set_szEventType(trim(sanitize_all_html_input($data['szEventType'])));  
            if(!empty($this->szEventType))
            {
                $arCustomerEventTypes = self::$arCustomerEventTypes;
                $errorFlag = false;
                
                if(!in_array($this->szEventType,$arCustomerEventTypes))
                {
                    $errorFlag = true;
                    $this->buildErrorDetailAry('szEventType','INVALID');
                }
                else if($this->szEventType=='PriceRequestContact')
                {
                    $this->set_szPriceToken(trim(sanitize_all_html_input($data['szPriceToken'])));   
                    $this->set_szBillingFirstName(trim(sanitize_all_html_input($data['szBillingFirstName'])));
                    $this->set_szBillingLastName(trim(sanitize_all_html_input($data['szBillingLastName'])));
                    $this->set_szBillingEmail(trim(sanitize_all_html_input($data['szBillingEmail'])));
                    $this->set_iBillingCountryDialCode(trim(sanitize_all_html_input($data['iBillingCountryDialCode'])));
                    $this->set_iBillingPhoneNumber(trim(sanitize_all_html_input($data['iBillingPhoneNumber']))); 
                    $this->set_szCargoType(trim(sanitize_all_html_input($data['szCargoType']))); 
                    
                    if(strtolower($data['iPrivate'])=='yes' || (is_numeric($data['iPrivate']) && $data['iPrivate']==1))
                    {
                        $this->szBillingCompanyName = $data['szBillingFirstName']." ".$data['szBillingLastName'];
                        $this->iPrivateCustomer = 1;
                    }
                    else
                    {
                        $this->szBillingCompanyName = sanitize_all_html_input($data['szBillingCompanyName']);
                        $this->iPrivateCustomer = 0;
                    }
                    if($this->szCargoType>0)
                    {
                        $kVatApplication = new cVatApplication();
                        $kVatApplication->loadCargoTypes(false,$this->szCargoType);
                        if(!empty($kVatApplication->szCargoTypeCode))
                        {
                            $this->szCargoType = $kVatApplication->szCargoTypeCode;
                        }
                        else
                        { 
                            $this->buildErrorDetailAry("szCargoType",'INVALID'); 
                        }
                    }
                    if($this->iBillingCountryDialCode>0)
                    {
                        $kConfig = new cConfig();
                        $kConfig->loadCountry(false,false,false,$this->iBillingCountryDialCode); 
                        if($kConfig->idCountry>0)
                        {
                            $this->iBillingCountryDialCode = $kConfig->idCountry;
                        }
                    } 
                    $this->szCustomerToken = $data['szCustomerToken'];
                }
            } 
            if($this->error==true)
            {  
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION'; 
                return false;
            } 
            else
            { 
                return true;
            }
        }
    }
    
    function validateStandardCargoListData($data)
    {
        if(!empty($data))
        { 
            $this->set_iDataSet(sanitize_all_html_input(trim((int)$data['iDataSet'])));
            $this->set_iLanguage(sanitize_all_html_input(trim($data['iLanguage'])));            
            if($this->error==true)
            {
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return false;
            }
            else
            {
            
                if((int)$this->iDataSet==0)
                {
                    $errorFlag=true;
                    $this->buildErrorDetailAry('iDataSet','INVALID');
                }            

                if($this->iLanguage!='')
                {
                    $kConfig =new cConfig();
                    if((int)$this->iLanguage>0)
                    {
                        $langArr = array(); 
                        $langArr = $kConfig->getLanguageDetails('',$this->iLanguage);
                       
                        if(!empty($langArr))
                        {
                            $this->idLanguage=$this->iLanguage;
                        }
                        else
                        {
                            $errorFlag=true;
                            $this->buildErrorDetailAry('iLanguage','INVALID');
                        }   
                    }
                    else
                    {
                        $errorFlag=true;
                        $this->buildErrorDetailAry('iLanguage','INVALID');
                    }
                }
                else
                {
                    $this->idLanguage=__LANGUAGE_ID_ENGLISH__;
                }
                
                if($errorFlag)
                {                   
                    self::$arrErrorMessageAry = $this->error_detail_ary; 
                    self::$szGlobalErrorType = 'VALIDATION';  
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
    }
    
    public function getStandardCargo($data)
    {
        if(!empty($data))
        { 
            $this->validateStandardCargoListData($data); 
            if($this->error==true)
            { 
                return self::buildErrorMessage();
            }
            
            $kConfig = new cConfig();
            $standardCargoCategoryAry = array();
            $standardCargoCategoryAry = $kConfig->getStandardCargoCategoryList($this->idLanguage,false,$this->iDataSet);
            
            
            $standardSubCategoryAry = array();
            $standardVogaSubCategoryAry = array();
            if(!empty($standardCargoCategoryAry))
            {
                $ctr1=0;
                foreach($standardCargoCategoryAry as $standardCargoCategoryArys)
                { 
                    
                    $standardCargoAry = array();
                    $standardCargoAry = $kConfig->getStandardCargoList($standardCargoCategoryArys['id'],$this->idLanguage,'',$this->iDataSet);  
                    $idStandardCategory = $standardCargoCategoryArys['id'];
                    $standardSubCategoryAry[$ctr1]['szCategoryID']=$idStandardCategory;
                    $standardSubCategoryAry[$ctr1]['szCategory']= cApi::encodeString($standardCargoCategoryArys['szCategoryName']);
                    $ctr = 0;
                    $standardSubNewCategoryAry=array();
                    if(!empty($standardCargoAry))
                    {
                        foreach($standardCargoAry as $standardCargoArys)
                        {
                            $standardSubNewCategoryAry[$ctr]['szStandardCagoID'] = $standardCargoArys['id'];
                            $standardSubNewCategoryAry[$ctr]['szType'] = cApi::encodeString(stripslashes(addslashes($standardCargoArys['szLongName'])));
                            $ctr++;
                        }
                    } 
                    $standardSubCategoryAry[$ctr1]['szCargoList']=$standardSubNewCategoryAry;
                    $ctr1++;
                }
            }
            
            if(!empty($standardSubCategoryAry))
            {
                $iWeekDay = date('w');
                if($iWeekDay==5 || $iWeekDay==6) //5: Friday, 6: Saturday
                {
                    $iResponseDay = 1 ;
                }
                else
                {
                    $iResponseDay = 0 ; 
                }
                self::$iResponseDay = $iResponseDay;
                self::$arrServiceListAry = $standardSubCategoryAry;
                self::$szGlobalErrorType = 'SUCCESS'; 
                return self::buildSuccessMessage('standard_cargo_data');
            }
            else
            {
                $this->buildErrorDetailAry('iStandardCargo','NOT_FOUND');
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage();
            }
        }
        else
        {
            $this->buildErrorDetailAry('iStandardCargo','NOT_FOUND');
            self::$arrErrorMessageAry = $this->error_detail_ary; 
            self::$szGlobalErrorType = 'VALIDATION';  
            return self::buildErrorMessage();
        }
    }
    
    function getOfferTypeArray($iOfferType,$szTransportMode,$szTransportModeCode,$szTransportIcon,$szCustomerType)
    {
        $serviceAry['szServiceID'] = '4444'; 
        $serviceAry['iOfferType'] = $iOfferType;					
        /*$serviceAry['dtTransportStartDay'] = '';
        $serviceAry['dtTransportStartMonth'] = '';
        $serviceAry['dtTransportStartYear'] = '';
        $serviceAry['szTransportStartCity'] = '';
        $serviceAry['dtTransportFinishDay'] = '';
        $serviceAry['dtTransportFinishMonth'] = '';
        $serviceAry['dtTransportFinishYear'] = '';
        $serviceAry['szTransportFinishCity'] = '';                  
        $serviceAry['szPackingType'] = '';
        $serviceAry['szCurrency'] = ''; 
        $serviceAry['fBookingPrice'] = '';
        $serviceAry['fVatAmount'] = ''; */
        $serviceAry['szCustomerType'] = cApi::encodeString($szCustomerType);
        $serviceAry['szTransportMode'] = cApi::encodeString($szTransportMode);
        $serviceAry['szTransportModeCode'] = cApi::encodeString($szTransportModeCode);
        $serviceAry['szTransportIcon'] = $szTransportIcon;
        $serviceAry['szCommodity'] ='1';
        /*$serviceAry['szForwarderAlias'] = '';
        $serviceAry['szServiceDescription'] = '';
        $serviceAry['szServiceTerm'] = '';
        $serviceAry['szCollection'] = '';
        $serviceAry['szDelivery'] = '';
        $serviceAry['szShipperPostcodeRequired'] = '';
        $serviceAry['szConsigneePostcodeRequired'] = '';                        
        $serviceAry['szCarrierName'] = '';
        $serviceAry['szCarrierAddressLine1'] = '';
        $serviceAry['szCarrierAddressLine2'] = '';
        $serviceAry['szCarrierAddressLine3'] = '';
        $serviceAry['szCarrierPostcode'] = '';
        $serviceAry['szCarrierCity'] = '';
        $serviceAry['szCarrierRegion'] = '';
        $serviceAry['szCarrierCountry'] = '';
        $serviceAry['szCarrierCompanyRegistration'] = '';
        $serviceAry['szCarrierTerms'] = '';
        $serviceAry['szCargo'] = '';
        $serviceAry['bInsuranceAvailable'] = ''; 
        $serviceAry['dtLatestPaymentDay'] = ''; 
        $serviceAry['dtLatestPaymentMonth'] = ''; 
        $serviceAry['dtLatestPaymentYear'] = '';*/ 
        return $serviceAry;
    }
    
    function getQuoteDetails($data)
    {
        if(!empty($data))
        {
            $kQuote = new cQuote();
            $kBooking = new cBooking();
            $kConfig = new cConfig();
            $szQuotePriceKey = $data['szQuoteID'];
             
            $bookingQuoteAry = array();
            $bookingQuoteAry = $kQuote->getQuotePricingIDbyKey($szQuotePriceKey);
            $idBookingQuotePricing = $bookingQuoteAry['id']; 
            $idBooking = $bookingQuoteAry['idBooking'];
            
            if($data['iLanguage']>0)
            {
                $langArr = array();
                $langArr = $kConfig->getLanguageDetails('',$data['iLanguage']);
                if(!empty($langArr))
                {
                    $iBookingLanguage = $data['iLanguage'];
                } 
            }
            else
            {
                $postSearchAry = array();
                $postSearchAry = $kBooking->getBookingDetails($idBooking); 
                $iBookingLanguage = $postSearchAry['iBookingLanguage'];
            } 
            if($idBookingQuotePricing>0 && $idBooking>0 && $iBookingLanguage>0)
            {    
                $szReferenceToken = cPartner::$szGlobalToken;  
                $idPartnerCustomer = cPartner::$idPartnerCustomer;
                $idGlobalPartner = cPartner::$idGlobalPartner;
               
                $kServices = new cServices();
                $kServices->updateQuoteDetailsToBooking($idBookingQuotePricing,$iBookingLanguage); 
                
                cPartner::$szGlobalToken = $szReferenceToken; 
                cPartner::$idPartnerCustomer = $idPartnerCustomer;
                cPartner::$idGlobalPartner = $idGlobalPartner;
                
                if($kServices->iBookingAlreadyPaid==1)
                { 
                    self::$szDeveloperNotes .= ">. Booking is already placed by using same token ".PHP_EOL;
                    $errorFlag = true;
                    $this->buildErrorDetailAry('szServiceTokenAlreadyUsed','NOSERVICE');  
                }
                else if($kServices->iSuccess==1)
                { 
                    $postSearchAry = array();
                    $postSearchAry = $kBooking->getBookingDetails($idBooking); 
                       
                    $iTodayMinusOne = strtotime("-1 DAY"); 
                    $iTodayMinusOneDateTime = strtotime(date('Y-m-d 23:59:59',$iTodayMinusOne)); 
                    if($postSearchAry['iQuickQuote']<=0 && ((strtotime($postSearchAry['dtQuoteValidTo']) < $iTodayMinusOneDateTime ) || ($postSearchAry['iQuotesStatus']==__BOOKING_QUOTES_STATUS_EXPIRED__)))
                    {
                        $errorFlag = true;
                        self::$szDeveloperNotes .= ">. Quote has been expired. API called on: ".date('Y-m-d H:i:s')." dtQuoteValidTo: ".$postSearchAry['dtQuoteValidTo']." Status: ".$postSearchAry['iQuotesStatus'].PHP_EOL;
                        $this->buildErrorDetailAry('szQuoteExpired','INVALID');   
                    }
                    else
                    {
                        //Building response parameters for getQuote API
                        $kCustomerEvent = new cCustomerEevent(); 
                        $getQuoteResponseAry = array();
                        $getQuoteResponseAry = $kCustomerEvent->buildGetQuoteApiResponse($idBooking,$idBookingQuotePricing);
                        
                        if(!empty($getQuoteResponseAry))
                        {
                            self::$arrServiceListAry = $getQuoteResponseAry;
                            self::$szGlobalErrorType = 'SUCCESS'; 
                            return self::buildSuccessMessage('bookingQuote');
                        }
                        else
                        {
                            $errorFlag = true;
                            self::$szDeveloperNotes .= ">. Something went wrong while building response for the API ".PHP_EOL;
                            $this->buildErrorDetailAry('szCommunication','INVALID');   
                        }
                    }
                }
                else
                { 
                    $errorFlag = true;
                    self::$szDeveloperNotes .= ">. Something went wrong for quote and could not been updated correctly. ".print_R($kServices,true).PHP_EOL;
                    $this->buildErrorDetailAry('szCommunication','INVALID');    
                }  
            }
            else
            {
                $errorFlag = true;
                self::$szDeveloperNotes .= ">. Provided szQuoteID is not valid ".PHP_EOL;
                $this->buildErrorDetailAry('szCommunication','INVALID'); 
            } 
            
            if($errorFlag)
            {                   
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage();;
            }
        }
    }
    
    
    function getLabelDetails($data)
    {
        if(!empty($data))
        { 
            $kBooking = new cBooking();
            $kCourierServices= new cCourierServices();
            
            $this->set_iLanguage(sanitize_all_html_input(trim($data['iLanguage'])));   
            
            $idBooking = $kBooking->getBookingIdByBookngRef($data['szBookingKey']);
            if($idBooking<=0)
            {
                $errorFlag = true;
                self::$szDeveloperNotes .= ">. Provided szBookingKey is not valid ".PHP_EOL;
                $this->buildErrorDetailAry('szBookingKey','INVALID'); 
            }
            
            if($this->error==true)
            {
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return false;
            }
            
            $bookingDataArr = array();
            $courierBookingArr = array();
            $newCourierBookingAry = array();
            $newCourierBookingAry = array();
            
            $bookingDataArr = $kBooking->getExtendedBookingDetails($idBooking);     
            $newCourierBookingAry = $kCourierServices->getAllNewBookingWithCourier($idBooking);
            $courierBookingArr = $kCourierServices->getCourierBookingData($idBooking);  
            $cargoDetailArr = $kBooking->getCargoDeailsByBookingId($idBooking);
            
            $checkFlagLabelUploaded=false;
            if((int)$bookingDataArr['idFile']>0)
            {
                $kBookingFile = new cBooking();   
                $kBookingFile->loadFile($bookingDataArr['idFile']);

                if($kBookingFile->iLabelStatus!=__LABEL_SEND_FLAG__ && $kBookingFile->iLabelStatus!=__LABEL_DOWNLOAD_FLAG__)
                {
                    $checkFlagLabelUploaded = true;
                }
            }
            else
            {
                $checkFlagLabelUploaded = true;
            } 
            
            if($checkFlagLabelUploaded)
            {
                $errorFlag = true;
                self::$szDeveloperNotes .= ">. Label have not been uploaded yet. ".PHP_EOL;
                $this->buildErrorDetailAry('szBookingKey','INVALID');
            }
            
            if($errorFlag)
            {                   
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage();;
            }
            
            if($courierBookingArr[0]['szCollection']=='Scheduled')
            {
                $dtCollection = date('Y-m-d',strtotime($courierBookingArr[0]['dtCollection']));
                $iCollection = 1;
            }
            else
            {
                $dtCutoffDateTime = strtotime(date('Y-m-d',strtotime($bookingDataArr['dtCutOff'])));
                $dtTodayDateTime = strtotime(date('Y-m-d'));
                $iCollection = 0;

                if($dtCutoffDateTime>$dtTodayDateTime)
                {
                    $dtCollection = date('Y-m-d',strtotime($bookingDataArr['dtCutOff']));
                }
                else
                {
                    $dtCollection = date('Y-m-d');
                } 
            }
            
            $kConfig = new cConfig();
            $configLangArr = $kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$this->iLanguage);
            $szLabelPageHeading = $configLangArr[1]['szLabelPageHeading'];
            
            $szShipperDetail = $bookingDataArr['szConsigneeCompanyName'].", ".$bookingDataArr['szConsigneeCity'].", ".$bookingDataArr['szConsigneeCountry'];
            
            $szLabelPageHeading = str_replace("szProvider",$newCourierBookingAry[0]['szProviderName'],$szLabelPageHeading);
            $szLabelPageHeading = str_replace("szBookingReference",$bookingDataArr['szBookingRef'],$szLabelPageHeading);
            $szLabelPageHeading = str_replace("szDetial",$szShipperDetail,$szLabelPageHeading);
            
            $kConfig =new cConfig();                    
            $kConfig->loadCountry($bookingDataArr['idShipperCountry']); 
            $iCollectionDay = date('j.',strtotime($dtCollection)); 
            $iCollectionMonth = date('m',strtotime($dtCollection)); 
            
            if($kConfig->iDefaultLanguage>0)
            {
                $idShipperLanguage=$kConfig->iDefaultLanguage;
                $szMonthName = getMonthName($kConfig->iDefaultLanguage,$iCollectionMonth);
            }
            else
            {
                $idShipperLanguage=$this->iLanguage;
                $szMonthName = getMonthName($this->iLanguage,$iCollectionMonth);
            }            
            
            if((int)$bookingDataArr['idConsigneeCountry']>0)
            {
                $kConfig = new cConfig();
                $kConfig->loadCountry($bookingDataArr['idConsigneeCountry'],'',$idShipperLanguage);
                $szConsigneeCountryName=$kConfig->szCountryName;
            }
            $szPickupDate=$iCollectionDay." ".$szMonthName;
            
            if($bookingDataArr['szConsigneeCompanyName']!='')
            {
                $szConsigneeName=$bookingDataArr['szConsigneeCompanyName'];
            }
            else
            {
                $szConsigneeName=$bookingDataArr['szConsigneeFirstName']." ".$bookingDataArr['szConsigneeLastName'];
            }

            $shipperDetailsAry = array();
            $shipperDetailsAry['szShipperCompanyName'] = cApi::encodeString($bookingDataArr['szShipperCompanyName']);
            $shipperDetailsAry['szShipperFirstName'] = cApi::encodeString($bookingDataArr['szShipperFirstName']);
            $shipperDetailsAry['szShipperLastName'] = cApi::encodeString($bookingDataArr['szShipperLastName']);
            $shipperDetailsAry['szShipperEmail'] = cApi::encodeString($bookingDataArr['szShipperEmail']);
            $shipperDetailsAry['iShipperDialCode'] = cApi::encodeString($bookingDataArr['iShipperDialCode']);
            $shipperDetailsAry['szShipperPhone'] = cApi::encodeString($bookingDataArr['szShipperPhone']);
            $shipperDetailsAry['szConsigneeName']=cApi::encodeString($szConsigneeName);
            $shipperDetailsAry['szConsigneeCity']=cApi::encodeString($bookingDataArr['szConsigneeCity']);
            $shipperDetailsAry['szConsigneeCountry']=cApi::encodeString($szConsigneeCountryName);
            $shipperDetailsAry['szCourierCompany'] = cApi::encodeString($newCourierBookingAry[0]['szProviderName']);
            $shipperDetailsAry['szBookingRef']=cApi::encodeString($bookingDataArr['szBookingRef']);
            $shipperDetailsAry['iCollection'] = $iCollection;
            $shipperDetailsAry['dtCollectionDay'] = date('d',strtotime($dtCollection)); 
            $shipperDetailsAry['dtCollectionMonth'] = date('m',strtotime($dtCollection)); 
            $shipperDetailsAry['dtCollectionYear'] = date('Y',strtotime($dtCollection)); 
            $shipperDetailsAry['szPickUpDate'] = cApi::encodeString($szPickupDate);
            $shipperDetailsAry['szText'] = cApi::encodeString($szLabelPageHeading); 
            
            
            
            self::$arrServiceListAry = $shipperDetailsAry;
            self::$szGlobalErrorType = 'SUCCESS'; 
            return self::buildSuccessMessage('label');
        }
    }
    
    function getConfirmLabelDownload($data)
    {
        if(!empty($data))
        {
            $this->validateLabelShipperDetails($data);  
            if($this->error==true)
            {
                return self::buildErrorMessage();
            }
            
            $iBookingLanguage = $this->iLanguage;
            $idBooking = $this->idBooking;
            
            $labelDataAry = array();
            $labelDataAry['szShipperCompanyName'] = $this->szShipperCompanyName;
            $labelDataAry['szShipperFirstName'] = $this->szShipperFirstName;
            $labelDataAry['szShipperLastName'] = $this->szShipperLastName;
            $labelDataAry['szShipperEmail'] = $this->szShipperEmail;
            $labelDataAry['idShipperCountry'] = $this->idShipperDialCode;
            $labelDataAry['szShipperPhone'] = $this->iShipperPhoneNumber;
            $labelDataAry['dtShipperPickUp'] = $this->dtPickUpDay."/".$this->dtPickUpMonth."/".$this->dtPickUpYear;
            $labelDataAry['idBooking'] = $idBooking;
            $labelDataAry['iLanguage'] = $iBookingLanguage;
            
            $kCourierServices= new cCourierServices();
            $kBooking = new cBooking();
            $kCourierServices->updateShipperDetails($labelDataAry,true); 
            
            if($kCourierServices->iSuccessFlag==1)
            {
                $kConfig = new cConfig();
                $langArr = $kConfig->getLanguageDetails('',$iBookingLanguage); 
                //$szDomain = $langArr[0]['szDomain'];
                
                $szDomain = __MAIN_SITE_HOME_PAGE_SECURE_URL__; 
                $postSearchAry = array();
                $postSearchAry = $kBooking->getBookingDetails($idBooking); 
                
                $labelResponseAry = array();
                $labelResponseAry['szLabelUrl'] = $szDomain."/viewCourierLabel/".$postSearchAry['szBookingRef']."/";
                $labelResponseAry['szText'] = $kCourierServices->szLabelDownloadText;
                
                self::$arrServiceListAry = $labelResponseAry;
                self::$szGlobalErrorType = 'SUCCESS'; 
                return self::buildSuccessMessage('label');
            }
            else
            {
                $errorFlag = true;
                self::$szDeveloperNotes .= ">. Something went wrong in downloading the label. ".print_R($kCourierServices,true).PHP_EOL;
                $this->buildErrorDetailAry('szCommunication','INVALID');    
            }
            
            if($errorFlag)
            {                   
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage();;
            }
        }
    }
     
    function validateLabelShipperDetails($data)
    {
        if(!empty($data))
        {
            $this->set_iLanguage(sanitize_all_html_input(trim($data['iLanguage'])));  
            $this->set_szShipperCompanyName(trim(sanitize_all_html_input($data['szShipperCompanyName'])),true);
            $this->set_szShipperFirstName(trim(sanitize_all_html_input($data['szShipperFirstName'])));
            $this->set_szShipperLastName(trim(sanitize_all_html_input($data['szShipperLastName'])));
            $this->set_szShipperEmail(trim(sanitize_all_html_input($data['szShipperEmail'])));
            $this->set_iShipperCounrtyDialCode(trim(sanitize_all_html_input($data['iShipperCountryDialCode'])));
            $this->set_iShipperPhoneNumber(trim(sanitize_all_html_input($data['iShipperPhoneNumber'])));
            
            $this->set_dtPickUpDay(trim(sanitize_all_html_input($data['dtPickUpDay'])));
            $this->set_dtPickUpMonth(trim(sanitize_all_html_input($data['dtPickUpMonth'])));
            $this->set_dtPickUpYear(trim(sanitize_all_html_input($data['dtPickUpYear']))); 
            
            if($this->error==true)
            {  
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION'; 
                return true;
            } 
            else
            {  
                $kConfig = new cConfig();
                $kConfig->loadCountry(false,false,false,$data['iShipperCountryDialCode']); 
                if($kConfig->idCountry>0)
                {
                    $this->idShipperDialCode = $kConfig->idCountry;
                }
                
                $errorFlag = false;
                $dateErrorFlag = $this->validatePriceCalculationDate($data['dtPickUpDay'],$data['dtPickUpMonth'],$data['dtPickUpYear']); 

                if($dateErrorFlag)
                {  
                    $errorFlag = true;
                }
                if(trim($data['dtPickUpDay']) != '')
                {
                    if(!checkdate($data['dtPickUpMonth'], $data['dtPickUpDay'], $data['dtPickUpYear']))
                    {  
                        $errorFlag = true;
                    }
                }
                
                $kBooking = new cBooking();
                $idBooking = $kBooking->getBookingIdByBookngRef($data['szBookingKey']);
                if($idBooking<=0)
                {
                    $errorFlag = true;
                    self::$szDeveloperNotes .= ">. Provided szBookingKey is not valid ".PHP_EOL;
                    $this->buildErrorDetailAry('szBookingKey','INVALID'); 
                }
                else
                {
                    $this->idBooking = $idBooking;
                }
                if($errorFlag == true)
                {
                    $this->error = true;
                    self::$arrErrorMessageAry = $this->error_detail_ary; 
                    return true;
                }
                else
                {
                    return true;
                }  
            } 
            
        }
    }
    
    function submitNPS($data)
    {
        if(!empty($data))
        {
            $this->iScore = sanitize_all_html_input((int)$data['iScore']);
            $this->szComment = sanitize_all_html_input($data['szComment']); 
            $this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szEmail']))));
            
            if($this->iScore<1 || $this->iScore>10)
            {
                $errorFlag = true;
                self::$szDeveloperNotes .= ">. Score must be between 1 to 10".PHP_EOL;
                $this->buildErrorDetailAry('iScore','INVALID'); 
            }
            
            if($this->error === true)
            {
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage();
            }
            
            $kUser = new cUser();
            if($kUser->isUserEmailExist($this->szEmail))
            {  
                $idCustomer = $kUser->idIncompleteUser ;
            }
                
            if($idCustomer<=0)
            {
                $errorFlag = true;
                self::$szDeveloperNotes .= ">. Email address not exists.".PHP_EOL;
                $this->buildErrorDetailAry('szEmail','NOT_EXISTS'); 
            }
            
            if($errorFlag)
            {
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage();
            }
            $submitNpsAry = array();
            $submitNpsAry['iScore'] = $this->iScore;
            $submitNpsAry['szComment'] = $this->szComment;
            
            $kNPS = new cNPS();
            if($kNPS->addCustomerFeedBack($submitNpsAry,$idCustomer))
            {
                self::$szNotification = "E1217"; 
                $dummyAry = array();
                $dummyAry['szMessage'] = "Thanks! We received your valuable feedback.";
                self::$arrServiceListAry = $dummyAry;
                self::$szGlobalErrorType = 'SUCCESS'; 
                return self::buildSuccessMessage('dummy'); 
            }
            else
            { 
                $this->buildErrorDetailAry('szCommunication','INVALID'); 
                
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage();
            }
        }
    }
    
    function newsLetterSignup($data)
    {
        if(!empty($data))
        {
            $this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szEmail']))));
            $this->set_szName(trim(sanitize_all_html_input(strtolower($data['szName']))));
            $this->set_szLanguage(trim(sanitize_all_html_input($data['szLanguage']))); 
            
            if($this->error === true)
            {
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage();
            }
            
            $kConfig =new cConfig();
            if(!empty($this->szLanguage))
            { 
                $langArr = array(); 
                $langArr = $kConfig->getLanguageDetails(strtolower($this->szLanguage));

                if(!empty($langArr))
                {
                    $idLanguage = strtoupper($langArr[0]['id']); 
                } 
                else
                {
                    $errorFlag=true;
                    $this->buildErrorDetailAry('szLanguage','INVALID');                    
                    self::$arrErrorMessageAry = $this->error_detail_ary; 
                    self::$szGlobalErrorType = 'VALIDATION';  
                    return self::buildErrorMessage();
                }
            }
            
            $kUser = new cUser();
            if($kUser->isUserEmailExist($this->szEmail))
            {  
                $idCustomer = $kUser->idIncompleteUser ;
            }
            
            
            if((int)$idCustomer>0)
            {
                $query = "
                    UPDATE
                        ".__DBC_SCHEMATA_USERS__."
                    SET
                        iAcceptNewsUpdate = '1' 
                    WHERE
                        id='".(int)$idCustomer."'
                ";                
            }
            else
            {
                $szNameArr =explode(' ', $this->szName, 2);
                $szFirstName=$szNameArr[0];
                $szLastName=$szNameArr[1];
                
                $query = "
                    INSERT INTO
                        ".__DBC_SCHEMATA_USERS__."
                    (
                        szFirstName,
                        szLastName,
                        szEmail,
                        iAcceptNewsUpdate,
                        iLanguage,
                        dtCreateOn
                    )
                        VALUES
                    (
                        '".  mysql_escape_custom($szFirstName)."',
                        '".  mysql_escape_custom($szLastName)."',
                        '".  mysql_escape_custom($this->szEmail)."',
                        '1',
                        '".(int)$idLanguage."',
                        NOW()    
                    ) 
                ";
            }
            
            if($result=$this->exeSQL($query))
            { 
                $dummyAry = array();
                $dummyAry['szMessage'] = "Thanks! You have successfully signup for newsletter.";
                self::$arrServiceListAry = $dummyAry;
                self::$szGlobalErrorType = 'SUCCESS'; 
                return self::buildSuccessMessage('newsLetterSignup'); 
            }
            else
            {
                $this->buildErrorDetailAry('szCommunication','INVALID');
                self::$arrErrorMessageAry = $this->error_detail_ary; 
                self::$szGlobalErrorType = 'VALIDATION';  
                return self::buildErrorMessage();
            }           
            
        }
        
    }
    
    /*
    *
    */
    public static function get_dtShippingDay()
    {
        return self::$dtShippingDay;
    }
    public static function get_dtShippingMonth()
    {
        return self::$dtShippingMonth;
    }
    public static function get_dtShippingYear()
    {
        return self::$dtShippingYear;
    }
    public static function get_szPacketType()
    {
        return self::$szPacketType;
    }
    
    public static function get_szCargoDescription()
    {
        return self::$szCargoDescription;
    }
    public static function get_fCargoValue()
    {
        return self::$fCargoValue;
    }
    public static function get_szCargoCurrency()
    {
        return self::$szCargoCurrency;
    }
    public static function get_fTotalVolume()
    {
        return self::$fTotalVolume;
    }
    public static function get_fTotalWeight()
    {
        return self::$fTotalWeight;
    }
    public static function get_szServiceTerm()
    {
        return self::$szServiceTerm;
    }
    
    public static function get_szShipperCompanyName()
    {
        return self::$szShipperCompanyName;
    }
    public static function get_szShipperFirstName()
    {
        return self::$szShipperFirstName;
    }
    public static function get_szShipperLastName()
    {
        return self::$szShipperLastName;
    }
    public static function get_szShipperEmail()
    {
        return self::$szShipperEmail;
    }
    public static function get_iShipperCounrtyDialCode()
    {
        return self::$iShipperCounrtyDialCode;
    }
    public static function get_iShipperPhoneNumber()
    {
        return self::$iShipperPhoneNumber;
    }
    public static function get_szShipperAddress()
    {
        return self::$szShipperAddress;
    }
    public static function get_szShipperCity()
    {
        return self::$szShipperCity;
    }
    public static function get_szShipperPostcode()
    {
        return self::$szShipperPostcode;
    }
    public static function get_szShipperCountry()
    {
        return self::$szShipperCountry;
    }
    
    public static function get_szConsigneeCompanyName()
    {
        return self::$szConsigneeCompanyName;
    }
    public static function get_szConsigneeFirstName()
    {
        return self::$szConsigneeFirstName;
    }
    public static function get_szConsigneeLastName()
    {
        return self::$szConsigneeLastName;
    }
    public static function get_szConsigneeEmail()
    {
        return self::$szConsigneeEmail;
    }
    public static function get_iConsigneeCounrtyDialCode()
    {
        return self::$iConsigneeCounrtyDialCode;
    }
    public static function get_iConsigneePhoneNumber()
    {
        return self::$iConsigneePhoneNumber;
    }
    public static function get_szConsigneeAddress()
    {
        return self::$szConsigneeAddress;
    }
    public static function get_szConsigneeCity()
    {
        return self::$szConsigneeCity;
    }
    public static function get_szConsigneePostcode()
    {
        return self::$szConsigneePostcode;
    }
    public static function get_szConsigneeCountry()
    {
        return self::$szConsigneeCountry;
    }
    
    public static function get_szServiceType()
    {
        return self::$szServiceType;
    }
    public static function get_fDimension()
    {
        return self::$fDimension;
    }
    public static function get_iQuantity()
    {
        return self::$iQuantity;
    }	
    public static function get_iHeight()
    {
        return self::$iHeight;
    } 
    public static function get_iLength()
    {
        return self::$iLength;
    }
    public static function get_iWidth()
    {
        return self::$iWidth;
    }
    public static function get_szPalletType()
    {
        return self::$szPalletType;
    }
    public static function get_iWeight()
    {
        return self::$iWeight;
    }
    public static function get_szDimensionMeasure()
    {
        return self::$szDimensionMeasure;
    }
    public static function get_szWeightMeasure()
    {
        return self::$szWeightMeasure;
    }
    public static function get_szServiceRefrence()
    {
        return self::$szServiceRefrence;
    }
    public static function get_iInsuranceFlag()
    {
        return self::$iInsuranceFlag;
    }
    
    /*
    * Function For
    * Shipping Validations
    */
    public function set_dtShippingDay( $value,$flag=true )
    { 
        $this->dtShippingDay = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "dtShippingDay", t($this->t_base.'fields/shipping')." ".t($this->t_base.'fields/day'), 1, 31, $flag );  
    }
    public function set_dtShippingMonth( $value,$flag=true )
    {
        $this->dtShippingMonth = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "dtShippingMonth", t($this->t_base.'fields/shipping')." ".t($this->t_base.'fields/month'), 1, 12, $flag );  
    }
    public function set_dtShippingYear( $value,$flag=true )
    {
        $this->dtShippingYear = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "dtShippingYear", t($this->t_base.'fields/shipping')." ".t($this->t_base.'fields/year'), 2015, false, $flag );  
    }
    public function set_szPacketType($value ,$counter,$max_limit,$key_counter=false)
    {
        $this->szPacketType[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPacketType", t($this->t_base.'fields/packing_type'), false, false, $flag ); 
    }
    /*
     * Function For
     * Cargo Validations
     * 
     */
    public function set_szCargoDescription( $value,$flag=true )
    {
        $this->szCargoDescription = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCargoDescription", t($this->t_base.'fields/cargo')." ".t($this->t_base.'fields/description'), false, false, $flag );  
    }
    public function set_szCommodity( $value,$flag=true )
    {
        $this->szCommodity = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "szCommodity", t($this->t_base.'fields/commodity'), false, false, $flag );  
    }
    
    public function set_fCargoValue( $value,$flag=true )
    {
        $this->fCargoValue = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fCargoValue", t($this->t_base.'fields/cargo')." ".t($this->t_base.'fields/value'), 0, false, $flag );  
    }
    public function set_szCargoCurrency( $value,$flag=true )
    {
        $this->szCargoCurrency = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCargoCurrency",  t($this->t_base.'fields/cargo')." ".t($this->t_base.'fields/currency'), 0, false, $flag );  
    }
    public function set_fTotalVolume($value ,$counter,$max_limit,$key_counter=false)
    {
        $this->fTotalVolume[$counter] = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fTotalVolume", t($this->t_base.'fields/cargo')." ".t($this->t_base.'fields/volume'), 0, false, true,$counter); 
    }
    public function set_fTotalWeight($value ,$counter,$max_limit,$key_counter=false) 
    {
        $this->fTotalWeight[$counter] = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fTotalWeight", t($this->t_base.'fields/cargo')." ".t($this->t_base.'fields/weight'), 0, false, true ,$counter);  
    }
    public function set_szServiceTerm( $value,$flag=true )
    {
        $this->szServiceTerm = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szServiceTerm", t($this->t_base.'fields/service')." ".t($this->t_base.'fields/term'), false, false, $flag ); 
    }
    /*
     * Function For
     * Shipper Validations
     */
    public function set_szShipperCompanyName( $value,$flag=false )
    {
        $this->szShipperCompanyName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperCompanyName", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/company_name'), false, false, $flag );  
    }
    public function set_szShipperFirstName( $value,$flag=true )
    {
        $this->szShipperFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperFirstName", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/first_name'), false, false, $flag );
    }
    public function set_szShipperLastName( $value,$flag=true )
    {
        $this->szShipperLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperLastName", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/last_name'), false, false, $flag );
    }
    public function set_szShipperEmail( $value,$flag=true )
    {
        $this->szShipperEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szShipperEmail", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/email'), false, false, $flag );
    }
    public function set_iShipperCounrtyDialCode( $value,$flag=true )
    {
        $this->iShipperCounrtyDialCode = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iShipperCounrtyDialCode", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/country')." ".t($this->t_base.'fields/dial_code'), 0, false, $flag );
    }
    public function set_iShipperPhoneNumber( $value,$flag=true )
    {
        $this->iShipperPhoneNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iShipperPhoneNumber", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/phone_number'), false, false, $flag );
    }
    public function set_szShipperAddress( $value,$flag=true )
    {
        $this->szShipperAddress = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperAddress", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/address'), false, false, $flag );
    }
    public function set_szShipperCity( $value,$flag=true )
    {
        $this->szShipperCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperCity", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/city'), false, false, $flag );
    }
    public function set_szShipperPostcode( $value,$flag=true )
    {
        $this->szShipperPostcode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperPostcode", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/postcode'), false, false, $flag );
    }
    public function set_szShipperCountry( $value,$flag=true )
    {
        $this->szShipperCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperCountry",  t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/country'), false, false, $flag );
    }
     /*
     * Function For
     * Consignee Validations
     */
    public function set_szConsigneeCompanyName( $value,$flag=false )
    {
        $this->szConsigneeCompanyName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeCompanyName", t($this->t_base.'fields/consignee')." ".t($this->t_base.'fields/company_name'), false, false, $flag );
    }
    public function set_szConsigneeFirstName( $value,$flag=true )
    {
        $this->szConsigneeFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeFirstName", t($this->t_base.'fields/consignee')." ".t($this->t_base.'fields/first_name'), false, false, $flag );
    }
    public function set_szConsigneeLastName( $value,$flag=true )
    {
        $this->szConsigneeLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeLastName", t($this->t_base.'fields/consignee')." ".t($this->t_base.'fields/last_name'), false, false, $flag );
    }
    public function set_szConsigneeEmail( $value,$flag=true )
    {
        $this->szConsigneeEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szConsigneeEmail", t($this->t_base.'fields/consignee')." ".t($this->t_base.'fields/email'), false, false, $flag );
    }
    public function set_iConsigneeCounrtyDialCode( $value,$flag=true )
    {
        $this->iConsigneeCounrtyDialCode = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iConsigneeCounrtyDialCode", t($this->t_base.'fields/consignee')." ".t($this->t_base.'fields/country')." ".t($this->t_base.'fields/dial_code'), 0, false, $flag );
    }
    public function set_iConsigneePhoneNumber( $value,$flag=true )
    { 
        $this->iConsigneePhoneNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iConsigneePhoneNumber", t($this->t_base.'fields/consignee')." ".t($this->t_base.'fields/phone_number'), false, false, $flag );
    }
    public function set_szConsigneeAddress( $value,$flag=true )
    {
        $this->szConsigneeAddress = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeAddress", t($this->t_base.'fields/consignee')." ".t($this->t_base.'fields/address'), false, false, $flag );
    }
    public function set_szConsigneeCity( $value,$flag=true )
    {
        $this->szConsigneeCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeCity", t($this->t_base.'fields/consignee')." ".t($this->t_base.'fields/city'), false, false, $flag );
    }
    public function set_szConsigneePostcode( $value,$flag=true )
    {
        $this->szConsigneePostcode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneePostcode", t($this->t_base.'fields/consignee')." ".t($this->t_base.'fields/postcode'), false, false, $flag );
    }
    public function set_szConsigneeCountry( $value,$flag=true )
    {
        $this->szConsigneeCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeCountry",  t($this->t_base.'fields/consignee')." ".t($this->t_base.'fields/country'), false, false, $flag );
    }
    
    public function set_szPaymentType( $value,$flag=true )
    {
        $this->szPaymentType = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPaymentType",  t($this->t_base.'fields/payment_type'), false, false, $flag );
    }
    public function set_szBookingRef( $value,$flag=true )
    {
        $this->szBookingRef = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szBookingRef",  t($this->t_base.'fields/booking_ref'), false, false, $flag );
    }
    
    public function set_szStripeToken( $value,$flag=true )
    {
        $this->szStripeToken = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szStripeToken",  t($this->t_base.'fields/stripe_token'), false, false, $flag );
    }
    public function set_idTransaction( $value,$flag=true )
    {
        $this->idTransaction = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idTransaction",  t($this->t_base.'fields/stripe_token'), false, false, $flag );
    }
    
    
     /*
     * Function For
     * Dimensions Validations
     */
    public function set_szServiceType( $value,$flag=true )
    {
        $this->szServiceType = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szServiceType", t($this->t_base.'fields/service')." ".t($this->t_base.'fields/type'), false, false, $flag );
    }
    public function set_fDimension( $value , $counter,$max_limit=false,$key_counter=false)
    {
        if($max_limit<=0)
        {
            $max_limit = false;
        } 
        $szInputId = "fDimension"; 
        $this->fDimension[$counter] = $this->validateInput( $value, __VALIDATE_CARGO_DIMENSIONS__, $szInputId, t($this->t_base.'fields/dimension')." ".t($this->t_base.'error/for_line')." ".$key_counter, 0, $max_limit, true, $counter ); 
    }
    public function set_iQuantity( $value , $counter,$max_limit=false,$key_counter=false)
    { 
        if($max_limit<=0)
        {
            $max_limit = false;
        } 
        $szInputId = "iQuantity"; 
        
        $this->iQuantity[$counter] = $this->validateInput( $value, __VALIDATE_CARGO_DIMENSIONS__, $szInputId, t($this->t_base.'fields/quantity')." ".t($this->t_base.'error/for_line')." ".$key_counter, 0, $max_limit, true ,$counter);
        
    }	
    public function set_iHeight( $value , $counter,$max_limit,$key_counter=false)
    { 
        if($max_limit<=0)
        {
            $max_limit = false ;
        }		
        else if((int)$value > $max_limit && $max_limit>0)
        {
            $this->iCargoExceed = 1;
            $this->buildErrorDetailAry('iHeight','LESS_THEN',$counter);
            return false;
        }

        $szInputId = "iHeight";

        $this->iHeight[$counter] = $this->validateInput( $value, __VALIDATE_CARGO_DIMENSIONS__, $szInputId, t($this->t_base.'fields/height')." ".t($this->t_base.'error/for_line')." ".$key_counter, true, $max_limit, true ,$counter);
        
    } 
    public function set_iLength( $value ,$counter,$max_limit,$key_counter=false)
    {
        if($max_limit<=0)
        {
            $max_limit = false ;
        }		
        else if((int)$value > $max_limit)
        {
            $this->iCargoExceed = 1;
            $this->buildErrorDetailAry('iLength','LESS_THEN',$counter);
            return false;
        }

        $szInputId = "iLength";

        $this->iLength[$counter] = $this->validateInput( $value, __VALIDATE_CARGO_DIMENSIONS__, $szInputId, t($this->t_base.'fields/length')." ".t($this->t_base.'error/for_line')." ".$key_counter, true, $max_limit, true ,$counter);
        
    }
    public function set_iWidth( $value,$counter,$max_limit,$key_counter=false)
    {
        if($max_limit<=0)
        {
            $max_limit = false;
        }	
        else if((int)$value > $max_limit && $max_limit>0)
        {
            $this->iCargoExceed = 1;
            $this->buildErrorDetailAry('iWidth','LESS_THEN',$counter);
            return false;
        } 
        $szInputId = "iWidth";

        $this->iWidth[$counter] = $this->validateInput( $value, __VALIDATE_CARGO_DIMENSIONS__, $szInputId, t($this->t_base.'fields/width')." ".t($this->t_base.'error/for_line')." ".$key_counter, true, $max_limit, true ,$counter);
    }
    public function set_szPalletType($value,$counter,$key_counter=false,$flag=false)
    {
        if($max_limit<=0)
        {
            $max_limit = false;
        } 
        $szInputId = "szPalletType"; 
        
        $this->szPalletType[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPalletType", t($this->t_base.'fields/pallet')." ".t($this->t_base.'fields/type')." ".t($this->t_base.'error/for_line')." ".$key_counter, false, false, $flag ,$counter);
    }
   
    public function set_iWeight( $value,$counter , $max_limit,$key_counter=false)
    {
        if($max_limit<=0)
        {
            $max_limit = false;
        }
        else if((int)$value > $max_limit && $max_limit>0)
        {
            $this->iCargoExceed = 1;
            $this->buildErrorDetailAry('iWeight','LESS_THEN',$counter);
            return false;
        }

        $szInputId = "iWeight";

        $this->iWeight[$counter] = $this->validateInput( $value, __VALIDATE_CARGO_DIMENSIONS__, $szInputId, t($this->t_base.'fields/weight')." ".t($this->t_base.'error/for_line')." ".$key_counter, 0, $max_limit, true ,$counter);
        
    }
    public function set_szDimensionMeasure( $value ,$counter,$max_limit,$key_counter=false)
    {  
        $szInputId = "szDimensionMeasure"; 
        $this->szDimensionMeasure[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, $szInputId, t($this->t_base.'fields/pallet')." ".t($this->t_base.'fields/dimension')." ".t($this->t_base.'fields/unit'), false, false, true ,$counter);
        
    }
    public function set_szWeightMeasure( $value ,$counter,$max_limit,$key_counter=false)
    {
        if($max_limit<=0)
        {
            $max_limit = false ;
        }		
        else if((int)$value > $max_limit)
        {
            $this->iCargoExceed = 1;
            return false;
        } 
        $szInputId = "szWeightMeasure"; 
        $this->szWeightMeasure[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, $szInputId, t($this->t_base.'fields/pallet')." ".t($this->t_base.'fields/weight')." ".t($this->t_base.'fields/unit'), false, false, true ,$counter);
        
    }
    public function set_szServiceRefrence($value,$flag=true )
    {
        $this->szServiceRefrence = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szServiceRefrence", t($this->t_base.'fields/service')." ".t($this->t_base.'fields/reference'), false, false, $flag );
    }
    public function set_szPartnerReference($value)
    {
        $this->szPartnerReference = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPartnerReference", t($this->t_base.'fields/service')." ".t($this->t_base.'fields/reference'), false, false, false);
    }
    
    public function set_iInsuranceFlag($value,$flag=false )
    {
        $this->iInsuranceFlag = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iInsuranceFlag", t($this->t_base.'fields/insurance'), false, false, $flag );
    } 
    public function set_szCurrency($value,$flag=true )
    {
        $this->szCurrency = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCurrency", t($this->t_base.'fields/currency'), false, false, $flag ); 
    } 
    public function set_szLanguage($value,$flag=true )
    {
        $this->szLanguage = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLanguage", t($this->t_base.'fields/currency'), false, false, $flag ); 
    }  
    public function set_szUserName($value)
    { 
        $this->szUserName = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szUserName", t($this->t_base.'fields/email'), false, false, true );  
    }
    
    function set_szPassword( $value )
    {
        $this->szPassword = $this->validateInput( $value, __VLD_CASE_PASSWORD_NOFORCE_, "szPassword", t($this->t_base.'fields/password'), false, 255, true );
    }
    function set_szCurrentPassword( $value )
    {
        $this->szCurrentPassword = $this->validateInput( $value, __VLD_CASE_PASSWORD_NOFORCE_, "szCurrentPassword", t($this->t_base.'fields/password'), false, 255, true );
    }
    
    public function set_szFirstName($value,$flag=true )
    {
        $this->szFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szFirstName", false, false, false, $flag ); 
    }
    public function set_szLastName($value,$flag=true )
    {
        $this->szLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLastName", false, false, false, $flag ); 
    }
    public function set_szEmail($value,$flag=true )
    {
        $this->szEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szEmail", false, false, false, $flag ); 
    }
    
    public function set_iPhoneNumber($value,$flag=true )
    {
        $this->iPhoneNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iPhoneNumber", false, false, false, $flag ); 
    }
    public function set_iCountryDialCode($value,$flag=true )
    {
        $this->iCountryDialCode = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iCountryDialCode", false, false, false, $flag ); 
    }
    public function set_szCompanyName($value,$flag=true )
    {
        $this->szCompanyName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCompanyName", false, false, false, $flag ); 
    }
    public function set_szCompanyRegNo($value,$flag=true )
    {
        $this->szCompanyRegNo = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCompanyRegNo", false, false, false, $flag ); 
    }
    public function set_isPrivate($value,$flag=true )
    {
        $this->isPrivate = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "isPrivate", false, false, false, $flag ); 
    }
    public function set_szConfirmPassword($value,$flag=true )
    {
        $this->szConfirmPassword = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConfirmPassword", false, false, false, $flag ); 
    }
    public function set_szAddress($value,$flag=true )
    {
        $this->szAddress = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAddress", false, false, false, $flag ); 
    }
    public function set_szPostCode($value,$flag=true )
    {
        $this->szPostCode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPostCode", false, false, false, $flag ); 
    }
    public function set_szCity($value,$flag=true )
    {
        $this->szCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCity", false, false, false, $flag ); 
    }
    public function set_szCountry($value,$flag=true )
    {
        $this->szCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCountry", false, false, false, $flag ); 
    } 
    
    function set_szNonFrocePassword( $value )
    {
        $this->szNonFrocePassword = $this->validateInput( $value, __VLD_CASE_PASSWORD_NOFORCE_, "szPassword",false, false, 255, true );
    }
    
    function set_szAddress2( $value )
    {
        $this->szAddress2 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAddress2", false, false, 255, false );
    }
    function set_szAddress3( $value )
    {
        $this->szAddress3 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAddress3", false, false, 255, false );
    }
    
    function set_szState( $value )
    {
        $this->szState = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szState",false, false, 255, false );
    } 
    function set_idGroup( $value )
    {
        $this->idGroup = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idGroup", false, false, false, true );
    } 
    function set_szForwarderAlies($value)
    {
        $this->szForwarderAlies = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szForwarderAlies",false, false, false, true );
    } 
    function set_szShipperLatitude($value)
    {
        $this->szShipperLatitude = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperLatitude",false, false, false, false );
    }
    function set_szShipperLongitude($value)
    {
        $this->szShipperLongitude = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperLongitude",false, false, false, false );
    }
    function set_szConsigneeLatitude($value)
    {
        $this->szConsigneeLatitude = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeLatitude",false, false, false, false );
    }
    function set_szConsigneeLongitude($value)
    {
        $this->szConsigneeLongitude = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeLongitude",false, false, false, false );
    }
    
    public function set_szOriginCountryStr($value,$flag=true )
    {
        $this->szOriginCountryStr = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szOriginCountryStr", false, false, false, $flag ); 
    }
    
    public function set_szDestinationCountryStr($value,$flag=true )
    {
        $this->szDestinationCountryStr = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDestinationCountryStr", false, false, false, $flag ); 
    }
    
    public function set_szTransportMode($value,$flag=true )
    {
        $this->szTransportMode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szTransportMode", false, false, false, $flag ); 
    }
    
    public function set_szCargoType($value,$flag=true )
    {
        $this->szCargoType = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "szCargoType", false, false, false, $flag ); 
    }
    
    public function set_szGoodsInsuranceCurrency($value,$flag=true )
    {
        $this->szGoodsInsuranceCurrency = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szGoodsInsuranceCurrency", false, false, false, $flag ); 
    }
    
    public function set_fValueOfGoods($value,$flag=true )
    {
        $this->fValueOfGoods = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fValueOfGoods", false, false, false, $flag ); 
    }
    
    public function set_szCustomerCurrency($value,$flag=true )
    {
        $this->szCustomerCurrency = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCustomerCurrency", false, false, false, $flag ); 
    }
    
    public function set_fTotalPriceCustomerCurrency($value,$flag=true )
    {
        $this->fTotalPriceCustomerCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fTotalPriceCustomerCurrency", false, false, false, $flag ); 
    }
    
    public function set_szBookingCurrency($value,$flag=true )
    {
        $this->szBookingCurrency = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szBookingCurrency", false, false, false, $flag ); 
    }
    
    public function set_szForwarderCountry($value,$flag=true )
    {
        $this->szForwarderCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szForwarderCountry", false, false, false, $flag ); 
    }
    
    public function set_szCustomerType($value,$flag=true )
    {
        $this->szCustomerType = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCustomerType", false, false, false, $flag ); 
    }
    
    
    public function set_szBillingCompanyName( $value,$flag=false )
    {
        $this->szBillingCompanyName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szBillingCompanyName", t($this->t_base.'fields/billing')." ".t($this->t_base.'fields/company_name'), false, false, $flag );
    }
    public function set_szBillingFirstName( $value,$flag=true )
    {
        $this->szBillingFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szBillingFirstName", t($this->t_base.'fields/billing')." ".t($this->t_base.'fields/first_name'), false, false, $flag );
    }
    public function set_szBillingLastName( $value,$flag=true )
    {
        $this->szBillingLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szBillingLastName", t($this->t_base.'fields/billing')." ".t($this->t_base.'fields/last_name'), false, false, $flag );
    }
    public function set_szBillingEmail( $value,$flag=true )
    {
        $this->szBillingEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szBillingEmail", t($this->t_base.'fields/billing')." ".t($this->t_base.'fields/email'), false, false, $flag );
    }
    public function set_iBillingCountryDialCode( $value,$flag=true )
    {
        $this->iBillingCountryDialCode = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iBillingCountryDialCode", t($this->t_base.'fields/billing')." ".t($this->t_base.'fields/country')." ".t($this->t_base.'fields/dial_code'), 0, false, $flag );
    }
    public function set_iBillingPhoneNumber( $value,$flag=true )
    { 
        $this->iBillingPhoneNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iBillingPhoneNumber", t($this->t_base.'fields/billing')." ".t($this->t_base.'fields/phone_number'), false, false, $flag );
    }
    public function set_szBillingAddress( $value,$flag=true )
    {
        $this->szBillingAddress = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szBillingAddress", t($this->t_base.'fields/billing')." ".t($this->t_base.'fields/address'), false, false, $flag );
    }
    public function set_szBillingPostcode( $value,$flag=true )
    {
        $this->szBillingPostcode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szBillingPostcode", t($this->t_base.'fields/billing')." ".t($this->t_base.'fields/city'), false, false, $flag );
    }
    public function set_szBillingCity( $value,$flag=true )
    {
        $this->szBillingCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szBillingCity", t($this->t_base.'fields/billing')." ".t($this->t_base.'fields/postcode'), false, false, $flag );
    }
    public function set_szBillingCountry( $value,$flag=true )
    {
        $this->szBillingCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szBillingCountry",  t($this->t_base.'fields/billing')." ".t($this->t_base.'fields/country'), false, false, $flag );
    }
    public function set_szEventType( $value,$flag=true )
    {
        $this->szEventType = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szEventType",  false, false, false, $flag );
    }
    public function set_szPriceToken( $value,$flag=true )
    {
        $this->szPriceToken = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPriceToken",  false, false, false, $flag );
    }
    
    public function set_iDataSet( $value,$flag=true )
    {
        $this->iDataSet = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iDataSet",  false, false, false, $flag );
    }
    public function set_iLanguage( $value,$flag=true )
    {
        $this->iLanguage = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iLanguage",  false, false, false, $flag );
    }
    public function set_szInsurancePrice( $value,$flag=true )
    {
        $this->szInsurancePrice = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "szInsurancePrice",  false, false, false, $flag );
    } 
    public function set_dtPickUpDay( $value,$flag=true )
    { 
        $this->dtPickUpDay = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "dtPickUpDay", t($this->t_base.'fields/pickup')." ".t($this->t_base.'fields/day'), 1, 31, $flag );  
    } 
    public function set_dtPickUpMonth( $value,$flag=true )
    {
        $this->dtPickUpMonth = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "dtPickUpMonth", t($this->t_base.'fields/pickup')." ".t($this->t_base.'fields/month'), 1, 12, $flag );  
    }
    public function set_dtPickUpYear( $value,$flag=true )
    {
        $this->dtPickUpYear = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "dtPickUpYear", t($this->t_base.'fields/pickup')." ".t($this->t_base.'fields/year'), 2015, false, $flag );  
    }
    
    public function set_szName($value,$flag=true )
    {
        $this->szName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szName", false, false, false, $flag ); 
    }
}

?>
