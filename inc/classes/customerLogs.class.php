<?php
/**
 * This file is the container for partner api related functionality. 
 *
 * api.class.php
 *
 * @copyright Copyright (C) 2015 Transporteca
 * @author Ajay
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" ); 

class cCustomerLogs extends cDatabase
{    
    function __construct()
    {
        parent::__construct();
        return true;
    }
    
    function getAllFileEmailLogs($dtSent)
    {	    
        /*
         * id,
                idBooking,
                idUser, 
                szToAddress,
                szEmailSubject,
                szEmailBody,
                szFromEmail,
                dtSent,
                szEmailKey,
                szEmailStatus,
                iSuccess,
                dtEmailStatusUpdated,
                iIncomingEmail,
                iEmailNumber,
                iSnoozeEmail,
                dtSnoozeDate,
                iClosed,
                szCCEmailAddress,
                szBCCEmailAddress,
                iHasAttachments,
                szAttachmentFiles,
                szAttachmentFileName,
                szToUserName,
                szFromUserName,
                iSeen,
                iAlreadyUtf8Encoded,
                szDraftEmailBody,
                szDraftEmailSubject,
                iDSTDifference,
                iInvalidHtmlTags
         */
        $query="
            SELECT
                *
            FROM
                ".__DBC_SCHEMATA_EMAIL_LOG__." b 
            WHERE  
                DATE(dtSent) >= '".mysql_escape_custom($dtSent)."'
            ORDER BY 
                dtSent ASC 
        "; 
//        echo $query;
//        die;
        if($result = $this->exeSQL($query))
        {
            while($row = $this->getAssoc($result))
            { 
                $ret_ary[$ctr] = $row;  
                $ctr++; 
            }
            return $ret_ary;  
        }
        else
        {
            return false;
        }
    }
    
    function getAllFileReminderLogs($dtCreated)
    {  
        $query="
            SELECT
                id,
                idBooking,
                idFile,
                idUser,
                szFirstName,
                szLastName,
                szEmail,
                szSubject as szEmailSubject,
                szEmailBody,
                dtCreated as dtSent,
                idAdminAddedBy,
                iOverviewTask,
                dtRemindDate,
                idTaskOwner,
                iClosed,
                dtClosedOn,
                idAdminClosedBy,
                (SELECT CONCAT_WS(' ',szFirstName,szLastName) FROM ".__DBC_SCHEMATA_MANAGEMENT__." adm WHERE adm.id = b.idAdminAddedBy) as szAdminName, 
                (SELECT szFileRef FROM ".__DBC_SCHEMATA_FILE_LOGS__." bf WHERE bf.id = b.idFile) as szBookingFileNumber 
            FROM
                ".__DBC_SCHEMATA_BOOKING_FILE_EMAIL_LOGS__." b 
            WHERE   
                dtCreated >=  '".mysql_escape_custom($dtCreated)."'
        ";
        //echo "<br><br><br> ".$query;
        if($result = $this->exeSQL($query))
        {
            if($this->getRowCnt()>0)
            {
                $ret_ary = array();
                $ctr = 1;
                while($row = $this->getAssoc($result))
                { 
                    $ret_ary[$ctr] = $row;
                    $ret_ary[$ctr]['iReminder'] = 1;
                    $ctr++; 
                }
                return $ret_ary;
            }
            else
            {
                return array() ;
            }
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return array();
        } 
    }
    
    function getAllFileSearchLogs($dtCreatedOn)
    {  
        $query="
            SELECT
                id,
                idUser,
                idBooking,
                idFile,
                szFromLocation,
                szToLocation,
                fCargoVolume,
                fCargoWeight,
                dtTimingDate, 
                iQuickQuote,
                idAdmin,
                szAdminFirstName,
                szAdminLastName,
                dtCreatedOn as dtSent,
                idServiceType,
                iNumRecordFound,
                idDestinationCountry,
                idOriginCountry
            FROM
                ".__DBC_SCHEMATA_BOOKING_FILE_SEARCH_LOGS__."
            WHERE 
                dtCreatedOn >=  '".mysql_escape_custom($dtCreatedOn)."'       
        "; 
       // echo $query;
        if($result = $this->exeSQL($query))
        { 
            $ret_ary = array();
            $ctr=0;
            while($row = $this->getAssoc($result))
            {
                $ret_ary[$ctr] = $row; 
                $ctr++;
            }
            return $ret_ary;
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        } 
    }
    
    function getAllZopimChatLogs($dtCreatedOn)
    {  
        $query="
            SELECT 
                id,
                idUser, 
                szChatUniqueID,
                szAgentName,
                szAgentId, 
                szVisitorId,
                szVisitorName, 
                szVisitorEmail,
                szVisitorPhone,
                szWebPath,
                szTags,
                szRating,
                szUserSession,
                dtChatDateTime as dtSent,
                dtChatDateTime,
                szComments,
                dtCreatedOn
            FROM
                ".__DBC_SCHEMATA_ZOPIM_LOGS__."	
            WHERE 
                dtChatDateTime >=  '".mysql_escape_custom($dtCreatedOn)."'      
                
        ";
        //echo "<br><br>".$query ;
        if($result = $this->exeSQL($query))
        { 
            $retAry = array(); 
            $ctr = 0;
            while($row = $this->getAssoc($result))
            {
                $retAry[$ctr] = $row; 
                $ctr++;
            }
            return $retAry;
        } 
    }
    
    function addCustomerLogSnippet($data)
    {
        if(!empty($data))
        {
            $query="	
                INSERT INTO
                    ".__DBC_SCHEMATA_CUSTOMER_LOG_SNIPPET__."
                SET
                    idCustomerLogDataType = '".mysql_escape_custom(trim($data['idCustomerLogDataType']))."',
                    idUser = '".mysql_escape_custom(trim($data['idUser']))."',
                    idBooking = '".mysql_escape_custom(trim($data['idBooking']))."',
                    idPrimary = '".mysql_escape_custom(trim($data['idPrimary']))."',
                    szEmailSubject = '".mysql_escape_custom(trim($data['szEmailSubject']))."', 
                    szFromData = '".mysql_escape_custom(trim($data['szFromData']))."',
                    szToData = '".mysql_escape_custom(trim($data['szToData']))."',
                    dtSent = '".mysql_escape_custom(trim($data['dtSent']))."',
                    iQuickQuote = '".mysql_escape_custom(trim($data['iQuickQuote']))."', 
                    dtTimingDate = '".mysql_escape_custom(trim($data['dtTimingDate']))."',
                    fCargoVolume = '".mysql_escape_custom(trim($data['fCargoVolume']))."',
                    fCargoWeight = '".mysql_escape_custom(trim($data['fCargoWeight']))."',
                    szFileRef = '".mysql_escape_custom(trim($data['szFileRef']))."',
                    iEmailType = '".mysql_escape_custom(trim($data['iEmailType']))."',
                    iSeen = '".mysql_escape_custom(trim($data['iSeen']))."',
                    iSuccess = '".mysql_escape_custom(trim($data['iSuccess']))."', 
                    szCCEmailAddress = '".mysql_escape_custom(trim($data['szCCEmailAddress']))."',
                    szBCCEmailAddress = '".mysql_escape_custom(trim($data['szBCCEmailAddress']))."',
                    iDSTDifference = '".mysql_escape_custom(trim($data['iDSTDifference']))."',
                    iEmailNumber = '".mysql_escape_custom(trim($data['iEmailNumber']))."', 
                    szEmailKey = '".mysql_escape_custom(trim($data['szEmailKey']))."',
                    szEmailStatus = '".mysql_escape_custom(trim($data['szEmailStatus']))."',
                    dtEmailStatusUpdated = '".mysql_escape_custom(trim($data['dtEmailStatusUpdated']))."',  
                    iClosed = '".mysql_escape_custom(trim($data['iClosed']))."', 
                    idAdmin = '".mysql_escape_custom(trim($data['idAdmin']))."',  
                    idAdminClosedBy = '".mysql_escape_custom(trim($data['idAdminClosedBy']))."', 
                    dtClosedOn = '".mysql_escape_custom(trim($data['dtClosedOn']))."', 
                    idTaskOwner = '".mysql_escape_custom(trim($data['idTaskOwner']))."', 
                    dtRemindDate = '".mysql_escape_custom(trim($data['dtRemindDate']))."', 
                    szAdminName = '".mysql_escape_custom(trim($data['szAdminName']))."',   
                    iAlreadyUtf8Encoded = '".mysql_escape_custom(trim($data['iAlreadyUtf8Encoded']))."',   
                    szEmailBody = '".mysql_escape_custom(trim($data['szEmailBody']))."',   
                    iHasAttachments = '".mysql_escape_custom(trim($data['iHasAttachments']))."',   
                    szAttachmentFiles = '".mysql_escape_custom(trim($data['szAttachmentFiles']))."',   
                    szAttachmentFileName = '".mysql_escape_custom(trim($data['szAttachmentFileName']))."',    
                    iInvalidHtmlTags = '".mysql_escape_custom(trim($data['iInvalidHtmlTags']))."',     
                    dtCreatedOn = now() 
            "; 
//            echo $query ;
//            die;
            if($result=$this->exeSQL($query))
            { 
                $this->delete6plusMonthsOldData();
                return true;
            }
            else
            { 
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
        }
    }
    
    function getAllDataForCustomerLogs($idBooking,$szEmail=false,$idCustomer=false,$iPageNumber=1)
    {
        if($idBooking>0 || !empty($szEmail) || $idCustomer>0)
        { 
            if(!empty($szEmail))
            {
                $query_and .= " OR (szToData = '".mysql_escape_custom($szEmail)."' AND iLinkedFlag='0')";
            } 
            if((int)$idCustomer>0)
            {
                $query_and .= " OR (idUser='".(int)$idCustomer."')";
            } 

            if($iPageNumber>0)
            {
                $iRecordPerPage = __CONTENT_PER_PAGE_PENDING_TRAY_SEARCH__; 
                $query_limit ="
                    LIMIT
                        ".(($iPageNumber -1 ) * $iRecordPerPage ).",". $iRecordPerPage ."
                ";
            }

            $query="
                SELECT 
                    idCustomerLogDataType,
                    idPrimary as id,
                    idBooking,
                    idUser,   
                    dtSent,
                    szEmailKey,
                    szEmailStatus, 
                    dtEmailStatusUpdated, 
                    iEmailNumber, 
                    szCCEmailAddress,
                    szBCCEmailAddress, 
                    iSeen, 
                    iDSTDifference,
                    szFromData,
                    szToData,
                    szEmailSubject,
                    iClosed,
                    idAdminClosedBy,
                    dtClosedOn,
                    idTaskOwner,
                    dtRemindDate,
                    szAdminName,
                    szFileRef,
                    iQuickQuote,
                    dtTimingDate,
                    fCargoVolume,
                    fCargoWeight,
                    iAlreadyUtf8Encoded,
                    iHasAttachments,
                    szAttachmentFiles,
                    szAttachmentFileName,
                    szEmailBody,
                    iEmailType,
                    iSuccess,
                    szEmailStatus
                FROM
                    ".__DBC_SCHEMATA_CUSTOMER_LOG_SNIPPET__." b 
                WHERE  
                    iActive = '1'
                AND 
                    (
                        idBooking = '".(int)$idBooking."'
                        $query_and
                    )
                ORDER BY 
                    dtSent DESC 
                $query_limit
            "; 
            //  echo $query."<br>";
    //        die;
            if($result = $this->exeSQL($query))
            {
                while($row = $this->getAssoc($result))
                { 
                    $ret_ary[$ctr] = $row;  
                    if($row['iDSTDifference']>0)
                    {  
                        $ret_ary[$ctr]['dtSentOriginal'] = $row['dtSent'];
                        $ret_ary[$ctr]['dtSent'] = date('Y-m-d H:i:s',strtotime("+".(int)$row['iDSTDifference']." HOUR",strtotime($row['dtSent'])));  
                    }
                    else
                    {
                        $ret_ary[$ctr]['dtSentOriginal'] = $row['dtSent'];
                        $ret_ary[$ctr]['dtSent'] = getDateCustom($ret_ary[$ctr]['dtSent']);
                        /*
                        if(date('I')==1)
                        {  
                            //Our server's My sql time is 2 hours behind so that's why added 2 hours to sent date 
                            $ret_ary[$ctr]['dtSentOriginal'] = $row['dtSent'];
                            $ret_ary[$ctr]['dtSent'] = date('Y-m-d H:i:s',strtotime("+2 HOUR",strtotime($row['dtSent'])));  
                        }
                        else
                        {
                            $ret_ary[$ctr]['dtSentOriginal'] = $row['dtSent'];
                            $ret_ary[$ctr]['dtSent'] = date('Y-m-d H:i:s',strtotime("+1 HOUR",strtotime($row['dtSent'])));  
                        }
                         * 
                         */
                    }
                    if($row['idCustomerLogDataType']==__CUSTOMER_LOG_SNIPPET_TYPE_EMAIL__) 
                    {
                        $ret_ary[$ctr]['szFromEmail'] = $row['szFromData'];
                        $ret_ary[$ctr]['szToAddress'] = $row['szToData'];
                        
                        if($row['iEmailType']==__CUSTOMER_LOG_EMAIL_TYPE_INCOMING__) 
                        {
                            $ret_ary[$ctr]['iIncomingEmail'] = 1;
                        }
                        else
                        {
                            $ret_ary[$ctr]['iIncomingEmail'] = 0;
                        } 
                    }
                    else if($row['idCustomerLogDataType']==__CUSTOMER_LOG_SNIPPET_TYPE_REMINDER_NOTES__)
                    {
                        $ret_ary[$ctr]['szEmailBody'] = $row['szFromData'];
                        $ret_ary[$ctr]['szBookingFileNumber'] = $row['szFileRef'];
                    } 
                    else if($row['idCustomerLogDataType']==__CUSTOMER_LOG_SNIPPET_TYPE_OVERVIEW_TASK__)
                    {
                        $ret_ary[$ctr]['szEmailBody'] = $row['szFromData']; 
                    }
                    else if($row['idCustomerLogDataType']==__CUSTOMER_LOG_SNIPPET_TYPE_ZOPIM_CHAT_LOGS__)
                    {
                        $ret_ary[$ctr]['szAgentName'] = $row['szFromData'];
                    }
                    else if($row['idCustomerLogDataType']==__CUSTOMER_LOG_SNIPPET_TYPE_FILE_SEARCH_LOGS__)
                    {
                        $ret_ary[$ctr]['szFromLocation'] = $row['szFromData'];
                        $ret_ary[$ctr]['szToLocation'] = $row['szToData'];
                    }
                    $ctr++; 
                }
                return $ret_ary;  
            }
            else
            {
                return false;
            }
        } 
    }
    
    function addEmailDataToCustomerLogSnippet($emailLogsArys)
    {
        if(!empty($emailLogsArys))
        {
            $addCustomerLogAry = array();
            $addCustomerLogAry['idCustomerLogDataType'] = __CUSTOMER_LOG_SNIPPET_TYPE_EMAIL__;
            $addCustomerLogAry['idUser'] = $emailLogsArys['idUser'];
            $addCustomerLogAry['idBooking'] = $emailLogsArys['idBooking'];
            $addCustomerLogAry['idPrimary'] = $emailLogsArys['id'];
            $addCustomerLogAry['iSeen'] = $emailLogsArys['iSeen'];
            $addCustomerLogAry['iDSTDifference'] = $emailLogsArys['iDSTDifference'];
            $addCustomerLogAry['iEmailNumber'] = $emailLogsArys['iEmailNumber'];
            $addCustomerLogAry['dtSent'] = $emailLogsArys['dtSent'];
            $addCustomerLogAry['iLinkedFlag'] = $emailLogsArys['iLinkedFlag'];  
            $addCustomerLogAry['szFromData'] = $emailLogsArys['szFromEmail'];
            $addCustomerLogAry['szEmailSubject'] = $emailLogsArys['szEmailSubject'];
            $addCustomerLogAry['szToData'] = $emailLogsArys['szToAddress'];
            $addCustomerLogAry['szCCEmailAddress'] = $emailLogsArys['szCCEmailAddress'];
            $addCustomerLogAry['szBCCEmailAddress'] = $emailLogsArys['szBCCEmailAddress'];
            $addCustomerLogAry['szEmailKey'] = $emailLogsArys['szEmailKey']; 
            $addCustomerLogAry['szEmailStatus'] = $emailLogsArys['szEmailStatus'];
            $addCustomerLogAry['dtEmailStatusUpdated'] = $emailLogsArys['dtEmailStatusUpdated'];
            $addCustomerLogAry['iAlreadyUtf8Encoded'] = $emailLogsArys['iAlreadyUtf8Encoded']; 
            $addCustomerLogAry['szEmailBody'] = $emailLogsArys['szEmailBody'];
            $addCustomerLogAry['iHasAttachments'] = $emailLogsArys['iHasAttachments'];
            $addCustomerLogAry['szAttachmentFiles'] = $emailLogsArys['szAttachmentFiles'];
            $addCustomerLogAry['szAttachmentFileName'] = $emailLogsArys['szAttachmentFileName'];
            $addCustomerLogAry['iSuccess'] = $emailLogsArys['iSuccess'];
            $addCustomerLogAry['iInvalidHtmlTags'] = $emailLogsArys['iInvalidHtmlTags']; 
            
            if($emailLogsArys['iIncomingEmail']==1)
            { 
                $addCustomerLogAry['iEmailType'] = 2;
            }
            else
            { 
                $addCustomerLogAry['iEmailType'] = 1;
            }   
            $this->addCustomerLogSnippet($addCustomerLogAry);
        }
    }
    
    function addReminderNotesDataToCustomerLogs($reminderLogsArys)
    {
        if(!empty($reminderLogsArys))
        {
            $addCustomerLogAry = array(); 
            $addCustomerLogAry['idUser'] = $reminderLogsArys['idUser'];
            $addCustomerLogAry['idBooking'] = $reminderLogsArys['idBooking'];
            $addCustomerLogAry['idPrimary'] = $reminderLogsArys['id'];   
            $addCustomerLogAry['iClosed'] = $reminderLogsArys['iClosed'];  
            $addCustomerLogAry['idAdminClosedBy'] = $reminderLogsArys['idAdminClosedBy'];  
            $addCustomerLogAry['dtClosedOn'] = $reminderLogsArys['dtClosedOn'];  
            $addCustomerLogAry['idTaskOwner'] = $reminderLogsArys['idTaskOwner'];  
            $addCustomerLogAry['dtRemindDate'] = $reminderLogsArys['dtRemindDate'];  
            $addCustomerLogAry['szAdminName'] = $reminderLogsArys['szAdminName']; 
            $addCustomerLogAry['szFromData'] = $reminderLogsArys['szEmailBody'];
            $addCustomerLogAry['dtSent'] = $reminderLogsArys['dtSent'];
            $addCustomerLogAry['szFileRef'] = $reminderLogsArys['szBookingFileNumber'];

            if($reminderLogsArys['iOverviewTask']==1)
            {
                $addCustomerLogAry['idCustomerLogDataType'] = __CUSTOMER_LOG_SNIPPET_TYPE_OVERVIEW_TASK__;
            } 
            else
            {
                $addCustomerLogAry['idCustomerLogDataType'] = __CUSTOMER_LOG_SNIPPET_TYPE_REMINDER_NOTES__;
            }
            $this->addCustomerLogSnippet($addCustomerLogAry);
        }
    }
    
    function addBookingSearchDataToCustomerLogSnippet($bookingSearchLogsArys)
    {
        if(!empty($bookingSearchLogsArys))
        {
            $addCustomerLogAry = array(); 
            $addCustomerLogAry['idUser'] = $bookingSearchLogsArys['idUser'];
            $addCustomerLogAry['idBooking'] = $bookingSearchLogsArys['idBooking'];
            $addCustomerLogAry['idPrimary'] = $bookingSearchLogsArys['id'];      
            $addCustomerLogAry['idCustomerLogDataType'] = __CUSTOMER_LOG_SNIPPET_TYPE_FILE_SEARCH_LOGS__; 

            $addCustomerLogAry['szFromData'] = $bookingSearchLogsArys['szFromLocation'];      
            $addCustomerLogAry['szToData'] = $bookingSearchLogsArys['szToLocation'];      
            $addCustomerLogAry['fCargoVolume'] = $bookingSearchLogsArys['fCargoVolume'];      
            $addCustomerLogAry['fCargoWeight'] = $bookingSearchLogsArys['fCargoWeight'];      
            $addCustomerLogAry['dtTimingDate'] = $bookingSearchLogsArys['dtTimingDate'];      
            $addCustomerLogAry['iQuickQuote'] = $bookingSearchLogsArys['iQuickQuote'];      
            $addCustomerLogAry['idAdmin'] = $bookingSearchLogsArys['idAdmin'];      
            $addCustomerLogAry['szAdminName'] = $bookingSearchLogsArys['szAdminFirstName']." ".$bookingSearchLogsArys['szAdminLastName'];      
            $addCustomerLogAry['dtSent'] = $bookingSearchLogsArys['dtSent'];  

            $this->addCustomerLogSnippet($addCustomerLogAry);
        }
    }
    
    function addZopimChatDataToCustomerLogSnippet($zopimChatLogsArys)
    {
        if(!empty($zopimChatLogsArys))
        {
            $addCustomerLogAry = array(); 
            $addCustomerLogAry['idCustomerLogDataType'] = __CUSTOMER_LOG_SNIPPET_TYPE_ZOPIM_CHAT_LOGS__; 
            $addCustomerLogAry['idUser'] = $zopimChatLogsArys['idUser'];
            $addCustomerLogAry['idBooking'] = $zopimChatLogsArys['idBooking'];
            $addCustomerLogAry['idPrimary'] = $zopimChatLogsArys['id'];       
            $addCustomerLogAry['szFromData'] = $zopimChatLogsArys['szAgentName'];  
            $addCustomerLogAry['dtSent'] = $zopimChatLogsArys['dtSent'];  
 
            $this->addCustomerLogSnippet($addCustomerLogAry);
        }
    }
    
    function updateCustomerLogSnippet($data,$idPrimary,$idCustomerLogDataType)
    {
        if(!empty($data) && $idPrimary>0 && $idCustomerLogDataType>0)
        {  
            $update_query = '';
            if(!empty($data))
            { 
                foreach($data as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                } 
                $update_query = rtrim($update_query,",");  
            }
            
            if(empty($update_query))
            {
                /*
                * If update query log is empty then retur false
                */ 
                return false;
            }
            
            $query="	
                UPDATE
                    ".__DBC_SCHEMATA_CUSTOMER_LOG_SNIPPET__."
                SET
                    ".$update_query.",
                    dtUpdatedOn = now()
                WHERE
                    idPrimary = '".(int)$idPrimary."'
                AND
                  idCustomerLogDataType = '".(int)$idCustomerLogDataType."'  
            ";
            //echo $query ;
            if($result=$this->exeSQL($query))
            {  
                return true;
            }
            else
            { 
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    
    function delete6plusMonthsOldData()
    {
        /*
        * Deleting all the 6+ moths old data from tblcustomerlogsnippet
        */
        $dtSent = date('Y-m-d',strtotime("-6 MONTH"));
        $query="	
            DELETE FROM
                ".__DBC_SCHEMATA_CUSTOMER_LOG_SNIPPET__."
            WHERE
                DATE(dtSent) < '".mysql_escape_custom($dtSent)."'
        "; 
        //echo $query;
        if($result=$this->exeSQL($query))
        { 
            return true;
        }
        else
        {
            return false;
        } 
    }
    
    function getVogaEmailQuotesListing($data)
    {
        if(!empty($data))
        {  
            $dtFromDate = format_date($data['dtFromDate']);
            $dtToDate = format_date($data['dtToDate']); 
            
            $query="
                SELECT 
                    b.id, 
                    el.id,
                    el.idBooking,
                    el.idUser, 
                    el.szToAddress,
                    el.szEmailSubject,
                    el.szEmailBody,
                    el.szFromEmail,
                    el.dtSent,
                    el.szEmailKey,
                    el.szEmailStatus,
                    el.iSuccess,
                    el.dtEmailStatusUpdated,
                    el.iIncomingEmail,
                    el.iEmailNumber,
                    el.iSnoozeEmail,
                    el.dtSnoozeDate,
                    el.iClosed,
                    el.szCCEmailAddress,
                    el.szBCCEmailAddress,
                    el.iHasAttachments,
                    el.szAttachmentFiles,
                    el.szAttachmentFileName,
                    el.szToUserName,
                    el.szFromUserName,
                    el.iSeen,
                    el.iAlreadyUtf8Encoded,
                    el.szDraftEmailBody,
                    el.szDraftEmailSubject,
                    el.iDSTDifference
                FROM
                    ".__DBC_SCHEMATA_EMAIL_LOG__." el
                INNER JOIN
                    ".__DBC_SCHEMATA_BOOKING__." b
                ON
                    b.id = el.idBooking
                WHERE 
                    b.idLandingPage =  '16'  
                AND
                    DATE(el.dtSent) >= '".  mysql_escape_custom($dtFromDate)."'
                AND
                    DATE(el.dtSent) <= '".  mysql_escape_custom($dtToDate)."'
                AND
                    el.szEmailSubject LIKE 'Pris pÃ¥ din forsendelse fra%'
                ORDER BY
                    el.dtSent ASC 
            ";
            //echo "<br><br>".$query ;
            if($result = $this->exeSQL($query))
            { 
                $retAry = array(); 
                $ctr = 0;
                while($row = $this->getAssoc($result))
                {
                    $retAry[$ctr] = $row; 
                    $ctr++;
                }
                return $retAry;
            } 
        }
    }
}

?>