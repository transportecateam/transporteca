<?php
/**
 * This file is the container for partner api related functionality. 
 *
 * transportecaApi.class.php
 *
 * @copyright Copyright (C) 2016 Transporteca
 * @author Ajay
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_CLASSES__ . "/responseErrors.class.php");

class cApiValidation
{   
    public function get_szUserName()
    {
        return $this->szUserName;
    } 
    public function get_szPassword()
    {
        return $this->szPassword;
    }
    
    public function set_szUserName($value)
    { 
        $this->szUserName = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "szUserName", t($this->t_base.'fields/shipping')." ".t($this->t_base.'fields/day'), 1, 31, $flag );  
    }
    
    function set_szNonFrocePassword( $value )
    {
        $this->szPassword = $this->validateInput( $value, __VLD_CASE_PASSWORD_NOFORCE_, "szPassword", t($this->t_base.'fields/password'), false, 255, true );
    }
}
?>