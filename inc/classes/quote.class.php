<?php
/**
 * This file is the container for all admin related functionality.
 * All functionality related to admin details should be contained in this class.
 *
 * admin.class.php
 *
 * @copyright Copyright (C) 2012 Transporteca, LLC
 * @author Anil
 * @package Transporteca Development
 */
if(!isset($_SESSION))
{
    session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_INC__ . "/functions.php" );
require_once( __APP_PATH_CLASSES__ . "/database.class.php");

class cQuote extends cDatabase
{
    /**
    * These are the variables for each admin.
    */
    var $id;
    var $szFirstName;
    var $szLastName;
    var $szEmail; 
    var $t_base_error = "management/Error/";
    var $idDummyGlobalPartner = __QUICK_QUOTE_DEFAULT_PARTNER_ID__; 
    var $t_base_courier_label ="COURIERLABEL/";
    var $t_base = "management/pendingTray/";
    var $t_base_booking_confirmation ="BookingConfirmation/"; 

    function __construct()
    {
        parent::__construct();
        return true;
    }  
    
    function getAllExpiredQuotes()
    {
        $bookingStatusStr = __BOOKING_STATUS_DRAFT__.",".__BOOKING_STATUS_ON_HOLD__.",".__BOOKING_STATUS_ZERO_LEVEL__ ; 
        $fileStatusStr = "(S1,S2,S3,S4,S5)";
        
        $query="
            SELECT
                qp.idFile,
                bf.szFileRef
            FROM
                ".__DBC_SCHEMATA_QUOTE_PRICING_DETAILS__." qp
            INNER JOIN
                ".__DBC_SCHEMATA_FILE_LOGS__." bf
            ON
                bf.id = qp.idFile
            INNER JOIN
                ".__DBC_SCHEMATA_BOOKING__." b
            ON
                b.idFile = bf.id
            WHERE
                qp.dtQuoteValidTo !='0000-00-00 00:00:00'
            AND
                qp.dtQuoteValidTo < now()
            AND
                b.idBookingStatus IN (".$bookingStatusStr.")
            AND
                bf.szTransportecaStatus IN ('S1','S2','S3','S4','S5')
            GROUP BY
                qp.idFile
        ";
        //AND bf.szTransportecaStatus != 'S150'
        //echo $query ;
        if(($result = $this->exeSQL($query)))
        {
            // Check if the user exists
            if($this->getRowCnt()>0)
            {
                $ret_ary = array();
                $ctr=0;
                while($row = $this->getAssoc($result))
                {
                    $ret_ary[$ctr] = $row;
                    $ctr++;
                } 
                return $ret_ary;
            }
            else
            {
                return array();
            }
        }
        else
        {
                return array();
        }
    }
    
    function isAllQuoteExpiredByFile($idBookingFile)
    {  
        if($idBookingFile>0)
        {	 
            $query="
                SELECT
                    (SELECT count(qp.id) FROM ".__DBC_SCHEMATA_QUOTE_PRICING_DETAILS__." qp WHERE qp.idFile = '".(int)$idBookingFile."' AND qp.iActive = '1' AND qp.dtQuoteValidTo !='0000-00-00 00:00:00' AND qp.dtQuoteValidTo < now() ) as iNumExpiredQuote,
                    (SELECT count(qp2.id) FROM ".__DBC_SCHEMATA_QUOTE_PRICING_DETAILS__." qp2 WHERE qp2.idFile = '".(int)$idBookingFile."' AND qp2.iActive = '1' ) as iNumAllQuote 
            ";
            //echo "<br><br> ".$query."<br><br>" ;
            
            if($result = $this->exeSQL($query))
            {
                if($this->getRowCnt()>0)
                { 
                    $row = $this->getAssoc($result);   
                    //Calculating number of unclosed quotes 
                    $iNumUnClosedQuote = $row['iNumAllQuote'] - $row['iNumExpiredQuote'];  
                    if($iNumUnClosedQuote>0)
                    {
                        //If there is more then one unclosed quote exists in our system then we do not update File status. 
                        return false;
                    }
                    else
                    {
                        /*
                        * If there is 0 unclosed quote then we update File status
                        * If there is 1 unclosed quote then we assume last quote that is not closed is one on which we are working right now. so in that case we update file status.
                        */
                        return true;
                    } 
                }
                else
                {
                    return false ;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    
    function addCronScheduleLogs($data)
    {
        if(!empty($data))
        {
            $query="
                INSERT INTO
                    ".__DBC_SCHEMATA_CRON_SCHEDULE_LOGS__."
                (
                    iType,
                    dtLastUpdated, 
                    iActive,
                    dtCreatedOn
                )
                VALUES
                (
                    '".$data['iType']."', 
                    now(),
                    '1',
                    now()				
                )
            ";
            //echo "<br /> ".$query."<br /> ";
            if($result=$this->exeSQL($query))
            {
                //cargo added 
                // TO DO 
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        } 
    } 
    
    function getAllVogabookingCreatedByAPI()
    { 
        $query=" 
            SELECT 
                id 
            FROM 
                ".__DBC_SCHEMATA_BOOKING__." 
            WHERE  
                iCreateAutomaticQuote = '1' 
            AND 
                dtCreatedOn > '2017-05-18 00:00:00'
            ORDER BY
                dtCreatedOn ASC
            LIMIT
                0,5
        ";
        //echo "<br><br> ".$query."<br><br>" ; 
        if($result = $this->exeSQL($query))
        {
            $ctr=0;
            if($this->getRowCnt()>0)
            { 
                $ret_ary = array();
                while($row = $this->getAssoc($result))
                {
                    $ret_ary[$ctr] = $row;
                    $ctr++;
                }
                return $ret_ary;
            }
            else
            {
                return false ;
            }
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        } 
    }
    function getAllVogaAutomatedQuote()
    { 
        $query=" 
            SELECT 
                id, 
                iPartnerBooking,
                idServiceType,
                iOriginCC,
                szTrackingNumber,
                szCustomerCountry, 
                iDestinationCC,
                idOriginCountry,				
                idOriginPostCode,
                szOriginCountry,
                szOriginPostCode,
                szOriginCity,
                szPartnerReference,
                szDestinationAddress,
                szOriginAddress,
                iOriginPostCodeSpecific,					
                idDestinationCountry,				
                idDestinationPostCode,
                szDestinationCountry,
                szDestinationPostCode,
                szDestinationCity,
                iDestinationPostCodeSpecific,
                idTimingType,
                dtTimingDate,
                idCustomerCurrency as idCurrency,
                szCustomerCurrency as szCurrency,
                fCustomerExchangeRate as fExchangeRate,
                fOriginLatitude,
                fOriginLongitude,
                fDestinationLatitude,
                fDestinationLongitude,
                fCargoVolume,
                fCargoWeight,
                idBookingStatus,
                fTotalPriceCustomerCurrency,
                idForwarder,
                idWarehouseFrom,
                idWarehouseTo,
                idShipperConsignee,
                dtCreatedOn,
                fPriceForSorting,
                dtAvailable,
                idUser,
                szFirstName,
                szLastName,
                szCustomerCompanyName,
                szEmail,
                dtCutOff,
                dtAvailable,
                idPricingWtw,
                szForwarderDispName,
                szForwarderLogo,
                szWarehouseToCity,
                szWarehouseFromCity,
                idPricingWtw,
                szBookingRandomNum,
                iPaymentProcess,
                idBookingStatus,
                iDoNotKnowCargo,
                iAcceptedTerms,
                dtTimingDate,
                iShipperConsignee,
                iFromRequirementPage,
                iCustomClearance,
                szBookingRef,
                fTotalPriceNoMarkupForwarderCurrency,
                iIncludeInsuranceWithZeroCargoValue,
                iOptionalInsuranceWithZeroCargoValue,
                iUpdateInsuranceWithBlankValue,
                isManualCourierBooking,
                fReferalAmount,
                idForwarderCurrency,
                szForwarderCurrency,
                fForwarderExchangeRate,
                fReferalPercentage,
                fReferalAmountUSD,
                iBookingStep,
                iPaymentType,
                iTransferConfirmed,
                idUniqueWTW,
                idCustomerCurrency,
                fTotalPriceUSD,
                iInsuranceIncluded,
                szGoodsInsuranceCurrency,
                fValueOfGoods, 
                idGoodsInsuranceCurrency,
                fTotalInsuranceCostForBookingCustomerCurrency,
                fInsuranceRate,
                iBookingLanguage, 
                idLandingPage,
                dtQuoteValidTo,
                iQuotesStatus,
                fTotalInsuranceCostForBookingCustomerCurrency_buyRate,
                fTotalVat,
                szCustomerPhoneNumber,
                idCustomerDialCode,
                idTransportMode,
                szTransportMode,
                szOtherComments,
                iNumColli,
                idServiceTerms,
                iInsuranceChoice,
                iPrivateShipping,
                szInternalComment,
                szIsuranceCurrency,
                szForwarderComment,
                iInsuranceStatus,
                iPaymentReceived,
                fValueOfGoodsUSD,
                isMoving,
                iForwarderGPType,
                szHandoverCity,
                iBookingQuotes,
                idCourierPackingType,
                idServiceProviderProduct,
                idServiceProvider,
                idForwarderCurrency,
                iCourierBooking,
                idFile,
                iBookingType,
                iAnonymusBooking,
                idForwarderQuoteCurrency,
                szForwarderQuoteCurrency,
                fForwarderQuoteCurrencyExchangeRate,
                fForwarderTotalQuotePrice,
                fTotalVatForwarderCurrency,
                iMarkupPaid,
                dtMarkupPaid,
                szCargoDescription,
                idQuotePricingDetails,
                szMarkupPaymentComments,
                iSearchMiniVersion,
                iPalletType,
                szCustomerCity,
                iInsuranceUpdatedFlag,
                szCargoDescription,
                dtExpactedDelivery,
                dtShipmentDate,
                iPrivateShipping,
                szOriginPostcodeTemp,
                szDestinationPostcodeTemp,
                szInsuranceInvoice,
                szAutomatedRfqResponseCode
            FROM 
                ".__DBC_SCHEMATA_BOOKING__." 
            WHERE  
                iBookingType = '".(int)__BOOKING_TYPE_VOGA_AUTOMATIC__."'
            AND
                idBookingStatus != '5'
        ";
        //echo "<br><br> ".$query."<br><br>" ; 
        if($result = $this->exeSQL($query))
        {
            if($this->getRowCnt()>0)
            { 
                $ret_ary = array();
                while($row = $this->getAssoc($result))
                {
                    $ret_ary[$ctr] = $row;
                    $ctr++;
                }
                return $ret_ary;
            }
            else
            {
                return false ;
            }
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        } 
    }
    
    function getCronjobLastUpdatedDate($iType=false)
    {
        if($iType>0)
        {	 
            $query=" 
                SELECT 
                    dtLastUpdated 
                FROM 
                    ".__DBC_SCHEMATA_CRON_SCHEDULE_LOGS__." 
                WHERE 
                    iType = '".(int)$iType."' 
                ORDER BY
                    dtCreatedOn DESC
                LIMIT 0,1
            ";
            //echo "<br><br> ".$query."<br><br>" ; 
            if($result = $this->exeSQL($query))
            {
                if($this->getRowCnt()>0)
                { 
                    $row = $this->getAssoc($result);  
                    return $row['dtLastUpdated'];
                }
                else
                {
                    return false ;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    
    function addVogaAutomaticQuote($data)
    {
        if(!empty($data))
        {
            $kBooking = new cBooking();
            $iMaxRegionId = $kBooking->getMaxBookingQuotesId(); 
            $iQuotationId = 1000 + $iMaxRegionId + 1 ;
            
            $iAutoQuote=1;
            if($data['iQuickQuoteTryITOutFlag'])
            {
                $iAutoQuote=0;
            }
 
            $query = "
                INSERT INTO
                    ".__DBC_SCHEMATA_BOOKING_QUOTES__."
                (
                    idBooking,
                    iQuotationId,
                    idFile, 
                    dtTaskStatusUpdatedOn,
                    idTransportMode,
                    szTransportMode,
                    idForwarder,
                    szForwarderContact, 
                    szForwarderQuoteTask,
                    szForwarderQuoteStatus,
                    iSentToForwarder,
                    iActive, 
                    iOffLine,
                    iClosed,
                    dtCreatedOn,
                    dtStatusUpdatedOn,
                    iSendQuickQuote,
                    iAutoQuote,
                    iClosedForForwarder,
                    dtCutOffDate,
                    dtAvailableDate,
                    dtWhsCutOff,
                    dtWhsAvailable
                )
                VALUES
                (
                    '".mysql_escape_custom($data['idBooking'])."',
                    '".mysql_escape_custom($iQuotationId)."', 
                    '".mysql_escape_custom($data['idBookingFile'])."', 
                    now(),
                    '".mysql_escape_custom(trim($data['idTransportMode']))."',
                    '".mysql_escape_custom(trim($data['szTransportMode']))."',
                    '".mysql_escape_custom(trim($data['idForwarder']))."',
                    '".mysql_escape_custom(trim($data['szForwarderContactEmail']))."', 
                    '',
                    '',
                    '1',
                    '1',
                    '',
                    '1',
                    now(),
                    NOW(),
                    '".mysql_escape_custom($data['iSendQuickQuote'])."',
                    '".$iAutoQuote."',
                    '1',
                    '".mysql_escape_custom(trim($data['dtCutOffDate']))."',
                    '".mysql_escape_custom(trim($data['dtAvailableDate']))."',
                    '".mysql_escape_custom(trim($data['dtWhsCutOff']))."',
                    '".mysql_escape_custom(trim($data['dtWhsAvailable']))."'    
                )
            ";
            //echo $query;
            if($result = $this->exeSQL($query))
            {
                return $this->iLastInsertID;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    
    function addVogaQuotePricing($data)
    {
        if(!empty($data))
        {
            /*if((float)$data['fTotalVatCustomerCurrency']>0)
            {
                $fVatPercentage=round((float)(($data['fTotalVatCustomerCurrency']/$data['fTotalPriceCustomerCurrency'])*100),2);
            }*/
            $kQuote = new cQuote();
            $szQuotePricingKey = $kQuote->getQuotePricingKey();
            
            $query = "
                INSERT INTO
                    ".__DBC_SCHEMATA_QUOTE_PRICING_DETAILS__."
                (
                    szQuotePricingKey,
                    idBooking, 
                    idFile,
                    idQuote, 
                    fTotalPriceForwarderCurrency,
                    idForwarderCurrency,
                    szForwarderCurrency,
                    fForwarderCurrencyExchangeRate,
                    fTotalVat,
                    iTransitDays, 
                    dtQuoteValidTo,
                    szForwarderComment,
                    fReferalPercentage, 
                    fReferalAmount,
                    fTotalPriceCustomerCurrency,
                    idCustomerCurrency,
                    fTotalVatCustomerCurrency,
                    fTotalInsuranceCostForBookingCustomerCurrency,
                    fQuotePriceCustomerCurrency,
                    iInsuranceComments,   
                    fReferalPercentage_hidden,
                    fReferalAmount_hidden,
                    fTotalPriceCustomerCurrency_hidden,
                    fTotalVatCustomerCurrency_hidden,
                    fQuotePriceCustomerCurrency_hidden,
                    iCustomerPriceUpdated,
                    iActive,  
                    dtCreatedOn,                                        
                    idForwarderAccountCurrency,
                    szForwarderAccountCurrency,
                    fForwarderAccountCurrencyExchangeRate,
                    fTotalPriceForwarderAccountCurrency,
                    fTotalVatForwarderAccountCurrency,
                    iManualFeeApplicable,
                    iHandlingFeeApplicable,
                    iProductType,
                    idCourierProvider,
                    idCourierProviderProduct,
                    idHandlingCurrency,
                    szHandlingCurrency,
                    fHandlingCurrencyExchangeRate,
                    fHandlingFeePerBooking,
                    fHandlingMarkupPercentage,
                    idHandlingMinMarkupCurrency,
                    szHandlingMinMarkupCurrency,
                    fHandlingMinMarkupCurrencyExchangeRate,
                    fHandlingMinMarkupPrice,
                    fTotalHandlingFeeUSD,
                    fTotalHandlingFee,
                    szServiceID,
                    iAddHandlingFee,
                    fVATPercentage,
                    dtTransitDate,
                    iQuoteSentToCustomer,
                    fTotalForwarderManualFee,
                    fTotalForwarderManualFeeCustomterCurrency,
                    fTotalForwarderManualFeeCustomterCurrencyWithoutMarkup,
                    fTotalHandlingFeeCustomerCurrencyMarkup,
                    szHandoverCity,
                    iRemovePriceGuarantee
                )
                VALUES
                (
                    '".mysql_escape_custom($szQuotePricingKey)."',
                    '".mysql_escape_custom($data['idBooking'])."',
                    '".mysql_escape_custom($data['idBookingFile'])."', 
                    '".mysql_escape_custom($data['idBookingQuote'])."', 
                    '".mysql_escape_custom($data['fTotalPriceForwarderCurrency'])."',
                    '".mysql_escape_custom($data['idForwarderCurrency'])."',
                    '".mysql_escape_custom($data['szForwarderCurrency'])."',
                    '".mysql_escape_custom($data['fForwarderExchangeRate'])."',
                    '".mysql_escape_custom(trim($data['fTotalVatForwarder']))."', 
                    '".mysql_escape_custom(trim($data['szTransitTime']))."',
                    '".mysql_escape_custom($data['dtQuoteValidTo'])."', 
                    '".mysql_escape_custom($data['szForwarderComment'])."', 
                    '".mysql_escape_custom($data['fReferalPercentage'])."',
                    '".mysql_escape_custom($data['fReferalAmount'])."', 
                    '".mysql_escape_custom($data['fTotalPriceCustomerCurrency'])."', 
                    '".mysql_escape_custom($data['idCustomerCurrency'])."',
                    '".mysql_escape_custom($data['fTotalVatCustomerCurrency'])."', 
                    '".mysql_escape_custom($data['fTotalInsuranceCostForBookingCustomerCurrency'])."',  
                    '".mysql_escape_custom($data['fQuotePriceCustomerCurrency'])."', 
                    '".mysql_escape_custom($data['iInsuranceComments'])."',   
                    '".mysql_escape_custom($data['fReferalPercentage'])."', 
                    '".mysql_escape_custom($data['fReferalAmount_hidden'])."',
                    '".mysql_escape_custom($data['fTotalPriceCustomerCurrency'])."', 
                    '".mysql_escape_custom($data['fTotalVatCustomerCurrency'])."',
                    '".mysql_escape_custom($data['fQuotePriceCustomerCurrency'])."',
                    '1', 
                    '1',
                    now(),
                    '".(int)$data['idForwarderAccountCurrency']."',
                    '".mysql_escape_custom($data['szForwarderAccountCurrency'])."',   
                    '".(float)$data['fForwarderAccountExchangeRate']."',
                    '".(float)$data['fTotalPriceForwarderAccountCurrency']."',
                    '".(float)$data['fTotalVatForwarderAccountCurrency']."',   
                    '".mysql_escape_custom(trim($data['iManualFeeApplicable']))."',
                    '".mysql_escape_custom(trim($data['iHandlingFeeApplicable']))."',
                    '".mysql_escape_custom(trim($data['iProductType']))."',
                    '".mysql_escape_custom(trim($data['idCourierProvider']))."',
                    '".mysql_escape_custom(trim($data['idCourierProviderProduct']))."',
                    '".mysql_escape_custom(trim($data['idHandlingCurrency']))."',
                    '".mysql_escape_custom(trim($data['szHandlingCurrency']))."',
                    '".mysql_escape_custom(trim($data['fHandlingCurrencyExchangeRate']))."',
                    '".mysql_escape_custom(trim($data['fHandlingFeePerBooking']))."',
                    '".mysql_escape_custom(trim($data['fHandlingMarkupPercentage']))."',
                    '".mysql_escape_custom(trim($data['idHandlingMinMarkupCurrency']))."',
                    '".mysql_escape_custom(trim($data['szHandlingMinMarkupCurrency']))."',
                    '".mysql_escape_custom(trim($data['fHandlingMinMarkupCurrencyExchangeRate']))."',
                    '".mysql_escape_custom(trim($data['fHandlingMinMarkupPrice']))."',
                    '".mysql_escape_custom(trim($data['fTotalHandlingFeeUSD']))."',
                    '".mysql_escape_custom(trim($data['fTotalHandlingFee']))."',
                    '".mysql_escape_custom($data['szServiceID'])."',
                    '".mysql_escape_custom($data['iAddHandlingFee'])."',
                    '".(float)$data['fVATPercentage']."',
                    '".mysql_escape_custom($data['dtAvailableDate'])."',
                    '".mysql_escape_custom($data['iQuoteSentToCustomer'])."',
                    '".(float)$data['fTotalForwarderManualFee']."',
                    '".(float)$data['fTotalForwarderManualFeeCustomterCurrency']."',
                    '".(float)$data['fTotalForwarderManualFeeCustomterCurrencyWithoutMarkup']."',
                    '".mysql_escape_custom(trim($data['fTotalHandlingFeeCustomerCurrencyMarkup']))."',
                    '".mysql_escape_custom(trim($data['szHandoverCity']))."',
                    '".mysql_escape_custom(trim($data['iRemovePriceGuarantee']))."' 
                )
            ";
        // echo $query;
//            die;
            if($result = $this->exeSQL($query))
            {
                return true;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError("mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        } 
    }
    
    function createQuoteOfferTexts($quotesAry,$postSearchAry,$bTryitout=false)
    {
        $szQuotationBody = ''; 
        if(!empty($quotesAry))
        {
            $iBookingLanguage = $postSearchAry['iBookingLanguage'];   
            $szCustomerEmail = $postSearchAry['szEmail'];
            $idCustomer = $postSearchAry['idUser'] ; 
            $idBooking = $postSearchAry['id'];
            $szCustomerCurrency = $postSearchAry['szCurrency'];
            
            $kUser = new cUser(); 
            if($idCustomer>0)
            {
                $kUser->getUserDetails($idCustomer);
                $iPrivateCustomer = $kUser->iPrivate;
            } 
        
            $kConfig = new cConfig();
            $transportModeListAry = array();
            $transportModeListAry = $kConfig->getAllTransportMode(false,$iBookingLanguage,true,true);
            
            $languageArr=$kConfig->getLanguageDetails('',$iBookingLanguage);

            if(!empty($languageArr))
            {
                $szCbmText = $languageArr[0]['szDimensionUnit'];
            }
            else
            {
                $szCbmText = 'cbm';
            } 
            $kConfig = new cConfig();
            $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$iBookingLanguage);

            //print_r($configLangArr);
            $szOfferText = $configLangArr[1]['szOfferText_pt'] ;
            $szFromText =  $configLangArr[1]['szFromText_pt']  ;
            $szToText =  $configLangArr[1]['szToText_pt']  ;
            $szCargoText =  $configLangArr[1]['szCargoText_pt'] ;
            $szModeText =  $configLangArr[1]['szModeText_pt'] ;
            $szTransitTimeText =  $configLangArr[1]['szTransitTimeText_pt'];
            $szPriceText =  $configLangArr[1]['szPriceText_pt'];
            $OfferValidUntillText =  $configLangArr[1]['OfferValidUntillText_pt'];
            $szInsuranceText =  $configLangArr[1]['szInsuranceText_pt'];
            $szDaysText =  " ".$configLangArr[1]['szDaysText_pt'];
            $szNoVatOnBookingText =  $configLangArr[1]['szNoVatOnBookingText_pt'];
            $szVatExcludingText = $configLangArr[1]['szVatExcludingText_pt'];        
            $szOfferHeading =  $configLangArr[1]['szOfferHeading_pt'].":";
            $szMultipleOfferHeading = $configLangArr[1]['szMultipleOfferHeading_pt'].":";
            $szMultipleOfferHeading2 = $configLangArr[1]['szMultipleOfferHeading2_pt'].":";
            $szInsuranceValueUptoText = " ".$configLangArr[1]['szInsuranceValueUptoText_pt'];        
            $szVatTextExcluingTextWhenOneQuote = $configLangArr[1]['szVatTextExcluingTextWhenOneQuote_pt'];
            $szVatTextExcluingTextWhenOneQuote = $configLangArr[1]['szVatTextExcluingTextWhenOneQuote_pt'];
            $szClickOfferLink=$configLangArr[1]['szClickOfferLink_pt'];
             
            if($postSearchAry['idTransportMode']==4)//courier 
            { 
                $szVolWeight = format_volume($postSearchAry['fCargoVolume'],$iBookingLanguage).$szCbmText.", ".number_format_custom((float)$postSearchAry['fCargoWeight'],$iBookingLanguage)."kg,  ".number_format_custom((int)$postSearchAry['iNumColli'],$iBookingLanguage)."col";
            }
            else
            {
                $szVolWeight = format_volume($postSearchAry['fCargoVolume'],$iBookingLanguage).$szCbmText.",  ".number_format_custom((float)$postSearchAry['fCargoWeight'],$iBookingLanguage)."kg ";
            }
            $iQuoteCount = count($quotesAry);  
             
            $counter = 1;
            $num_counter = 1;  
            foreach($quotesAry as $quotesArys)
            { 
                if($quotesArys['iClosed']==1 || $bTryitout)
                {   
                    if($bTryitout)
                    {
                        $szTransitTime = $quotesArys['szTransitTime'];
                        $fTotalPriceCustomerCurrency = round((float)($quotesArys['fTotalPriceCustomerCurrency']));
                        $fTotalVat = round((float)$quotesArys['fTotalVatCustomerCurrency'],2);
                    }
                    else
                    {
                        $szTransitTime = $quotesArys['szTransitTime'];
                        $fTotalPriceCustomerCurrency = round((float)($quotesArys['fTotalPriceCustomerCurrency']));
                        $fTotalVat = round((float)$quotesArys['fTotalVatCustomerCurrency'],2);
                        
                        $szTransitTime = $quotesArys['iTransitHours'];
                    }  
                    if($iPrivateCustomer==1)
                    {
                        $iDecimalPlaces = false;  
                        if(__float($fTotalVat))
                        {
                            $iDecimalPlaces = 2;
                        } 
                        $szQuotePriceStr =  $szCustomerCurrency." ".number_format_custom((float)($fTotalPriceCustomerCurrency+$fTotalVat),$iBookingLanguage,$iDecimalPlaces)." all-in" ;
                        $szVatExcludingText =  $szVatTextExcluingTextWhenOneQuote;
                    }
                    else
                    {
                        $szQuotePriceStr =  $szCustomerCurrency." ".number_format_custom((float)$fTotalPriceCustomerCurrency,$iBookingLanguage)." all-in" ;
                    }
                    
                    if($fTotalVat>0)
                    {
                        $szQuotePriceStr .=", ".$szVatExcludingText." ".$szCustomerCurrency." ".number_format_custom((float)$fTotalVat,$iBookingLanguage,2) ;
                    }
                    $iMonth = (int)date("m",strtotime($quotesArys['dtQuoteValidTo']));
                    $szMonthName = getMonthName($iBookingLanguage,$iMonth,true);
                    
                    $szValidTo = date('j.',strtotime($quotesArys['dtQuoteValidTo']))." ".$szMonthName;
                    
                    //$szTransportMode = utf8_encode($transportModeListAry[$quotesArys['idTransportMode']]['szLongName']);
                    $szTransportMode = $transportModeListAry[$quotesArys['idTransportMode']]['szLongName'];
                    
                    $szBookingRandomKey = $postSearchAry['szBookingRandomNum'];
                    $idBookingQuotePricing = $quotesArys['idQuotePriceDetails'];
                    $szQuotePricingKey = $quotesArys['szQuotePricingKey'];
                    
                    $kConfig =new cConfig();
                    $langArr=$kConfig->getLanguageDetails('',$iBookingLanguage);
                         
                    $szDomain = $langArr[0]['szDomain'];
                    $szLanguageCode=$langArr[0]['szLanguageCode'];
                     
                    $kQuote = new cQuote();
                    $szControlPanelUrl = $kQuote->buildQuoteLink($idBookingQuotePricing,$iBookingLanguage);  
                    $szBookingQuoteLink = '<a href="'.$szControlPanelUrl.'">'.$szClickOfferLink.'</a>';  
                    
                    if($iQuoteCount==1)
                    {
                        $szQuotationBody .= $szModeText.': '.$szTransportMode.'<br>'. ''.$szTransitTimeText.': '.$szTransitTime.''.$szDaysText.'<br>'. ''.$szPriceText.': '.$szQuotePriceStr.'<br>'.$szBookingQuoteLink; 
                    }
                    else
                    {
                        if($num_counter>1)
                        {
                            $szQuotationBody .= '<br><br>';
                        }
                        $szQuotationBody .= "<strong><u>".$counter.'. '.$szOfferText.' </u></strong>'.'<br>'.$szModeText.': '.$szTransportMode.'<br>'.$szTransitTimeText.': '.$szTransitTime.''.$szDaysText.'<br>'.$szPriceText.': '.$szQuotePriceStr.'<br>'.$szBookingQuoteLink;
                    } 
                    $counter++;
                    $num_counter++; 
                }
            } 
        } 
        $ret_Ary = array();
        if($bTryitout)
        {
            $ret_Ary['szQuote'] = $szQuotationBody;
        }
        else
        {
            $ret_Ary['szQuote'] = nl2br($szQuotationBody);
        } 
        $ret_Ary['szValidTo'] = $szValidTo; 
        return $ret_Ary; 
    }
    
    /*
    * 
    */ 
    function copyBookingFileOptimized($idBookingFile,$iAnonymous=false,$anotherFlag=false,$bQuickQuote=false)
    {
        if($idBookingFile>0)
        {
            $kBooking = new cBooking();
            $kPartner = new cPartner();
            $kRegisterShipCon = new cRegisterShipCon(); 
            $kForwarder = new cForwarder(); 
            $kBooking->loadFile($idBookingFile); 
            if($kBooking->idBookingFile<=0)
            {
                $szDeveloperNotes .= ">. No File ID is associated with this booking ".date("Y-m-d H:i:s").PHP_EOL;
                $this->__copyBookingLogs($szReferenceID,$szDeveloperNotes);
                $this->szCopyBookingError = $szDeveloperNotes;
                return false;
            } 
            $idCrmEmailLogs = $kBooking->idEmailLogs; 
            $bAddEmailLog = false;
            if($bQuickQuote && $idCrmEmailLogs>0 && $kBooking->szTaskStatus=='T210')
            { 
                $bAddEmailLog = true;
            } 
            $dtCreatedOn = $kBooking->getRealNow();
            
            $szDeveloperNotes .= ">. Initiated copy booking task for File ID: ".$idBookingFile." ".date("Y-m-d H:i:s")." ".PHP_EOL; 
            
            $szReferenceID = md5(time()."_".mt_rand(0,9999));
            //$szErrorMessage = "Due to some internal server error we cannot process your request. Please ask your developer to look into it with reference: ".;
            $szErrorMessage = "Due to some internal server error we can't process your request this time. Please try again later or notify your developer with reference number: ".$szReferenceID;
            $idBooking = $kBooking->idBooking;  
            
            /*
            * Step 1: Creating empty bookings
            */
            $idNewBooking = $kPartner->createEmptyBooking();
            if($idNewBooking>0)
            {
                $szDeveloperNotes .= ">. Empty Booking has been created with ID: ".$idNewBooking." ".date("Y-m-d H:i:s").PHP_EOL;
            }
            else
            {
                $szDeveloperNotes .= ">. Empty booking could not be created. ".date("Y-m-d H:i:s").PHP_EOL;
                $this->__copyBookingLogs($szReferenceID,$szDeveloperNotes);
                $this->szCopyBookingError = $szErrorMessage;
                return false;
            }   
            
            $bookingDataAry = array();
            $bookingDataAry = $this->__getAllRecord(__DBC_SCHEMATA_BOOKING__,$idBooking);
            
            if($bQuickQuote)
            {
                if($bookingDataAry['iCargoPackingType']>0)
                {
                    $iCargoPackingType = $bookingDataAry['iCargoPackingType'];
                }
                else if($bookingDataAry['iCargoPackingType']<=0)
                {
                    if($bookingDataAry['iSearchMiniVersion']>0)
                    {
                        if($bookingDataAry['iSearchMiniVersion']==3 || $bookingDataAry['iSearchMiniVersion']==4)
                        {
                            $iCargoPackingType = 1; //Parcel booking
                        }
                        else if($bookingDataAry['iSearchMiniVersion']==5 || $bookingDataAry['iSearchMiniVersion']==6)
                        {
                            $iCargoPackingType = 2; //Pallet booking
                        }
                        else
                        {
                            $iCargoPackingType = 3; //Break bulk
                        }
                    }
                    else
                    {
                        $iCargoPackingType = 3;
                    }
                }
            }
            
            $idShipperConsignee = $bookingDataAry['idShipperConsignee'];
            $szDeveloperNotes .= ">. Fetching shipper consignee details with ID: ".$idShipperConsignee." ".date("Y-m-d H:i:s").PHP_EOL;
            
            /*
            * Step 2: Copy shipper consignee data
            */
            if($idShipperConsignee>0)
            {
                $shipperConsigneeDataAry = array();
                $shipperConsigneeDataAry = $this->__getAllRecord(__DBC_SCHEMATA_SHIPPER_CONSIGNEE__,$idShipperConsignee);

                $shipperConsigneeDataAry['idBooking'] = $idNewBooking;
                $shipperConsigneeDataAry['dtCreatedOn'] = $dtCreatedOn;
                $idNewShipperConsignee = $this->__buildInsertQuery($shipperConsigneeDataAry,__DBC_SCHEMATA_SHIPPER_CONSIGNEE__); 
                if($idNewShipperConsignee>0)
                {
                    $szDeveloperNotes .= ">. Shipper consignee data successfully copied with ID: ".$idNewShipperConsignee." ".date("Y-m-d H:i:s").PHP_EOL;
                }
                else
                {
                    $szDeveloperNotes .= ">. Shipper consignee data could not be copied. Please ask your dev with reference ".date("Y-m-d H:i:s").PHP_EOL;
                    $this->__copyBookingLogs($szReferenceID,$szDeveloperNotes);
                    $this->szCopyBookingError = $szErrorMessage;
                    return false;
                } 
            } 
            else
            {
                $szDeveloperNotes .= ">. There is no shipper/consignee data associated with booking ID: ".$idBooking." ".date("Y-m-d H:i:s").PHP_EOL;
            }
            
            /*
            * Step 3: Copy booking file data
            */ 
            
            $idBookingFile = $bookingDataAry['idFile'];
            
            $bookingFileDataAry = array();
            $bookingFileDataAry = $this->__getAllRecord(__DBC_SCHEMATA_FILE_LOGS__,$idBookingFile);
              
            $szDeveloperNotes .= ">. Fetched booking file data for ID: ".$idBookingFile." ".date("Y-m-d H:i:s").PHP_EOL;
            $max_number_used_for_file = $kForwarder->getMaximumUsedNumberByForwarder(false,false,'FILE');
            $szFileRef = $kRegisterShipCon->generateBookingFileRef($max_number_used_for_file);
                     
            $szDeveloperNotes .= ">. Created File References ".$szFileRef." ".date("Y-m-d H:i:s").PHP_EOL;
            
            $idAdmin = $_SESSION['admin_id'] ;
            
            $iSearchType = 2;
            $szTransportecaStatus = "S1";
            $szTransportecaTask = "T1";
                        
            $bookingFileDataAry['szFileRef'] = $szFileRef ; 
            $bookingFileDataAry['szTransportecaStatus'] = $szTransportecaStatus;
            $bookingFileDataAry['szTransportecaTask'] = $szTransportecaTask;
            $bookingFileDataAry['dtFileStatusUpdatedOn'] = $dtCreatedOn;
            $bookingFileDataAry['dtTaskStatusUpdatedOn'] = $dtCreatedOn;
            $bookingFileDataAry['iQuoteSent'] = 0;
            $bookingFileDataAry['szForwarderBookingStatus'] = "";
            $bookingFileDataAry['szForwarderBookingTask'] = "";
            $bookingFileDataAry['iClosed'] = ""; 
            $bookingFileDataAry['iStatus'] = ''; 
            $bookingFileDataAry['dtReminderDate'] = '';
            $bookingFileDataAry['dtCustomerRemindDate'] = '';
            $bookingFileDataAry['dtSnoozeDate'] = '';
            $bookingFileDataAry['iSnoozeAdded'] = '';
            $bookingFileDataAry['szLastTaskStatus'] = '';
            $bookingFileDataAry['iSearchType'] = $iSearchType;
            $bookingFileDataAry['isDeleted'] = 0;
            $bookingFileDataAry['iBookingFileStatus'] = "";
            $bookingFileDataAry['iBookingFlag'] = "";
            $bookingFileDataAry['idBooking'] = $idNewBooking;
            $bookingFileDataAry['dtCreatedOn'] = $dtCreatedOn; 
            $bookingFileDataAry['dtUpdatedOn'] = "";  
            if($idAdmin>0)
            {
                /*
                 * If Task created by Management User then we mark logged in user as File Owner.
                 */
                $bookingFileDataAry['idFileOwner'] = $idAdmin;
            }
            if($bAddEmailLog)
            {
                $bookingFileDataAry['idEmailLogs'] = $idCrmEmailLogs;  
            }
            else
            {
                $bookingFileDataAry['idEmailLogs'] = "";
            }
            
            
            if($anotherFlag)
            {
                $bookingFileDataAry['idUser'] = "0";
            }
            
            $idNewBookingFile = $this->__buildInsertQuery($bookingFileDataAry,__DBC_SCHEMATA_FILE_LOGS__); 
            if($idNewBookingFile>0)
            {
                $szDeveloperNotes .= ">. Booking file data successfully copied with ID: ".$idNewBookingFile." ".date("Y-m-d H:i:s").PHP_EOL;
            }
            else
            {
                $szDeveloperNotes .= ">. Booking File data could not be copied. ".date("Y-m-d H:i:s").PHP_EOL;
                $this->__copyBookingLogs($szReferenceID,$szDeveloperNotes);
                $this->szCopyBookingError = $szErrorMessage;
                return false;
            } 
            
            /*
            * Step 4: Update booking data to tblbookings
            */   
            $iBookingType = __BOOKING_TYPE_RFQ__ ;
            
            $res_ary = array();
            $res_ary = $bookingDataAry;
            unset($res_ary['id']);
            $res_ary['idShipperConsignee'] = $idNewShipperConsignee ; 
            $res_ary['dtQuotationExpired'] = '' ; 
            $res_ary['dtQuotationSent'] = '' ;  
            $res_ary['iQuotesStatus'] = __BOOKING_QUOTES_STATUS_NEW_BOOKING__ ;  
            $res_ary['iBookingQuotes '] = 1;  
            $res_ary['idFile'] = $idNewBookingFile;     
            $res_ary['iBookingInitiatedByAdmin'] = 0;
            $res_ary['iAnonymusBooking'] = 0;
            $res_ary['idAdminInitiatedBy'] = '';
            $res_ary['dtBookingInitiatedByAdmin'] = '' ;  
            $res_ary['idBookingStatus'] = __BOOKING_STATUS_DRAFT__ ; 
            $res_ary['szBookingRef'] = '';
            $res_ary['iAcceptedTerms'] = '';
            $res_ary['dtBookingConfirmed'] = '';
            $res_ary['szPaymentStatus'] = '';
            $res_ary['iPaymentType'] = '';
            $res_ary['iPaymentProcess'] = '';
            $res_ary['iInsuranceStatus'] = 0;
            $res_ary['iInsuranceIncluded'] = 0;
            $res_ary['szInsuranceInvoice'] = '';
            $res_ary['szInvoice'] = ''; 
            $res_ary['fCustomerPaidAmount'] = '';
            $res_ary['szForwarderReference'] = ''; 
            $res_ary['idParentBooking'] = $idBooking;
            $res_ary['idServiceProvider'] = 0;
            $res_ary['szTrackingNumber'] = '';
            $res_ary['dtLabelSent'] = '';
            $res_ary['idServiceProviderProduct'] = '';
            $res_ary['idServiceProviderProduct'] = '';
            $res_ary['szInsuranceCreditNoteInvoice'] = '';
            $res_ary['dtBookingCancelled'] = '';
            $res_ary['iBookingStep'] = '';
            $res_ary['iTransferConfirmed'] = '';
            $res_ary['iTransferConfirmed'] = '';
            $res_ary['iPaymentReceived'] = '';
            $res_ary['dtLabelDownloaded'] = '';
            $res_ary['idLabelDownloadedBy'] = "";
            $res_ary['iBookingInitiatedByAdmin'] = ""; 
            $res_ary['idAdminInitiatedBy'] = "";
            $res_ary['dtBookingInitiatedByAdmin'] = "";
            $res_ary['iBookingConfirmedByAdmin'] = "";
            $res_ary['idAdminConfirmedBy'] = "";
            $res_ary['iBookingType'] = $iBookingType;
            $res_ary['iMarkupPaid'] = 0;
            $res_ary['dtMarkupPaid'] = 0;
            $res_ary['iMarkupPaymentReceived'] = 0;
            $res_ary['dtCreatedOn'] = $dtCreatedOn;
            $res_ary['dtUpdatedOn'] = "";
            $res_ary['iStandardPricing'] = 0;
            $res_ary['iQuickQuote'] = 0;
            $res_ary['iIncludeInsuranceWithZeroCargoValue'] = 0;
            $res_ary['iUpdateInsuranceWithBlankValue'] = 0;
            $res_ary['iOptionalInsuranceWithZeroCargoValue'] = 0; 
            $res_ary['iPartnerBooking'] = 0;
            $res_ary['szReferenceToken'] = 0;
            $res_ary['szQuickQuotePaymentType'] = "";  
            $res_ary['iForwarderGPType'] = 0;
            $res_ary['iCourierBooking'] = 0; //change on 05-Dec-2016
            $res_ary['szOtherComments'] = "";
            $res_ary['szForwarderComment'] = "";
            $res_ary['dtCutOff'] = "0000-00-00 00:00:00";
            $res_ary['dtAvailable'] = "0000-00-00 00:00:00";
             
            if($bQuickQuote)
            {
                $res_ary['iQuickQuote'] = __QUICK_QUOTE_CONVERT_RFQ__; 
                if($iCargoPackingType>0)
                {
                    $res_ary['iCargoPackingType'] = $iCargoPackingType; 
                }
                $res_ary['iBookingQuotes'] = 1;
                $res_ary['iQuotesStatus'] = __BOOKING_QUOTES_STATUS_NEW_BOOKING__ ;  
                $res_ary['iBookingType'] = __BOOKING_TYPE_RFQ__;  
                //$res_ary['iCourierBooking'] = 0; //change on 05-Dec-2016
                $res_ary['dtTimingDate'] = date('Y-m-d');
            }
            if($anotherFlag)
            {
                $res_ary['idUser'] = 0;
                $res_ary['szFirstName'] = '';
                $res_ary['szLastName'] = '';
                $res_ary['szEmail'] = '';
                $res_ary['szCustomerCompanyName'] = "";
                $res_ary['szCustomerCompanyRegNo'] = "";
                $res_ary['szCustomerAddress1'] = "";
                $res_ary['szCustomerAddress2'] = "";
                $res_ary['szCustomerAddress3'] = "";
                $res_ary['szCustomerPostCode'] = "";
                $res_ary['szCustomerCity'] = "";
                $res_ary['szCustomerCountry'] = "";
                $res_ary['szCustomerState'] = "";
                $res_ary['idCustomerDialCode'] = "";
                $res_ary['szCustomerPhoneNumber'] = "";
                $res_ary['idCustomerCurrency'] = "";
                $res_ary['szCustomerCurrency'] = "";
                $res_ary['fCustomerExchangeRate'] = "";
                $res_ary['fTotalPriceCustomerCurrency'] = "";
                $res_ary['fPriceMarkupCustomerCurrency'] = "";
            } 
            $update_query = "";
            if(!empty($res_ary))
            {
                foreach($res_ary as $key=>$value)
                {
                    if($key!='szBookingRandomNum')
                    {
                        $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                }
            } 
            $update_query = rtrim($update_query,",");   
            
            if($kBooking->updateDraftBooking($update_query,$idNewBooking))
            {
                $szDeveloperNotes .= ">. Booking data successfully copied with ID: ".$idNewBooking." ".date("Y-m-d H:i:s").PHP_EOL;
            }
            else
            {
                $szDeveloperNotes .= ">. Booking data could not be copied.".date("Y-m-d H:i:s").PHP_EOL;
                $this->__copyBookingLogs($szReferenceID,$szDeveloperNotes);
                $this->szCopyBookingError = $szErrorMessage;
                return false;
            } 
            
            /*
            * Step 5: Copy cargo data to the booking
            */
            $cargoDetailsAry = array();
            $cargoDetailsAry = $this->__getAllCargoByBookingId($idBooking);
            if(!empty($cargoDetailsAry))
            {
                foreach($cargoDetailsAry as $cargoDetailsArys)
                {
                    $cargoDetailsArys['idBooking'] = $idNewBooking;
                    $cargoDetailsArys['dtCreatedOn'] = $dtCreatedOn; 
                    if($bQuickQuote)
                    {
                        if($iCargoPackingType==2)
                        {
                            $cargoDetailsArys['szShipmentType'] = 'PALLET';
                        }
                        else 
                        {
                            $cargoDetailsArys['szShipmentType'] = 'PARCEL'; 
                        }
                    }  
                    $idNewCargoDetails = $this->__buildInsertQuery($cargoDetailsArys,__DBC_SCHEMATA_CARGO__); 
                    if($idNewCargoDetails>0)
                    {
                        $szDeveloperNotes .= ">. Cargo data successfully copied with ID: ".$idNewCargoDetails." Booking ID: ".$idNewBooking." ".date("Y-m-d H:i:s").PHP_EOL;
                    }
                    else
                    {
                        $szDeveloperNotes .= ">. Cargo data could not be copied. for ID: ".$cargoDetailsArys['id']." Booking ID: ".$idBooking." ".date("Y-m-d H:i:s").PHP_EOL;
                        $this->__copyBookingLogs($szReferenceID,$szDeveloperNotes);
                        $this->szCopyBookingError = $szErrorMessage;
                        return false;
                    }
                }
            } 
            $szDeveloperNotes .= ">. Successfully complete copy booking process from Booking ID: ".$idBooking." to Booking ID: ".$idNewBooking." ".date("Y-m-d H:i:s").PHP_EOL;
            $this->__copyBookingLogs($szReferenceID,$szDeveloperNotes);
            $this->szBookingRandomNum = $szBookingRandomNum;
            $this->iNewFileID = $idNewBookingFile ;  
            return true;
        }
    }
    
    function __copyBookingLogs($szReferenceID,$szDeveloperNotes,$bUpdateTransfer)
    {
        $strdata=array();
        if($bUpdateTransfer)
        {
            $filename = __APP_PATH_ROOT__."/logs/automatic_transfer_".date('Ymd').".log";
            $strdata[0] = " \n\n *** Automatic self invoice transfer ".date("Y-m-d H:i:s")." \n\n"; 
        }
        else
        {
            $filename = __APP_PATH_ROOT__."/logs/copy_booking".date('Ymd').".log";
            $strdata[0] = " \n\n *** Copy Booking on ".date("Y-m-d H:i:s")." Reference: ".$szReferenceID." \n\n"; 
        }   
        $strdata[1] = $szDeveloperNotes;
        file_log(true, $strdata, $filename);
    }
    
    function __getMaxId($szTableName)
    {
        if(!empty($szTableName))
        {
            $query=" 
                SELECT 
                    max(id) as iMaxId 
                FROM 
                    $szTableName  
            ";
            //echo "<br><br> ".$query."<br><br>" ; 
            if($result = $this->exeSQL($query))
            {
                if($this->getRowCnt()>0)
                { 
                    $row = $this->getAssoc($result);  
                    return $row['iMaxId'];
                }
                else
                {
                    return false ;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    } 
    
    function __getAllRecord($szTableName,$idPrimary)
    {
        if(!empty($szTableName) && $idPrimary)
        {
            $query=" 
                SELECT 
                    * 
                FROM 
                    $szTableName  
                WHERE
                    id = '".(int)$idPrimary."'
            ";
            //echo "<br><br> ".$query."<br><br>" ; 
            if($result = $this->exeSQL($query))
            {
                if($this->getRowCnt()>0)
                { 
                    $row = $this->getAssoc($result);  
                    return $row;
                }
                else
                {
                    return false ;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    
    function __buildInsertQuery($dataAry,$szTableName)
    {
        if(!empty($dataAry))
        {
            $query = "INSERT INTO ".$szTableName." (".PHP_EOL;
            $queryKeys = '';
            $queryValues = '';
            foreach($dataAry as $key=>$values)
            {
                if($key=='id')
                {
                    continue;
                }
                $queryKeys .= $key.","; 
                $queryValues .= "'".  mysql_escape_custom($values)."',"; 
            } 
            $queryKeys = rtrim($queryKeys,","); 
            $queryValues = rtrim($queryValues,",");  
            
            $query = $query."".$queryKeys.") VALUES ( ".$queryValues.") ";
            //echo "<br><br> Query: ".$query; 
            if($result = $this->exeSQL($query))
            { 
                return $this->iLastInsertID ;    
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    } 
    
    public function __getAllCargoByBookingId($idBooking)
    { 
        if($idBooking>0)
        { 
            $query="
                SELECT
                    *
                FROM
                    ".__DBC_SCHEMATA_CARGO__." AS c 
                WHERE
                    c.idBooking = '".(int)$idBooking."'	 
                ORDER BY
                    c.id ASC
            ";
            //echo $query ;
            if($result = $this->exeSQL($query))
            {
                $cargoAry=array();
                if($this->iNumRows>0)
                {
                    $ctr=1;
                    while($row=$this->getAssoc($result))
                    {
                        $cargoAry[$ctr]=$row;
                        $ctr++;
                    }
                    return $cargoAry ;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    
    function validateQuickQuoteRequest($data,$mode=false)
    {
        if(!empty($data))
        { 
            $kCustomerCountry = new cConfig();
            $kWHSSearch = new cWHSSearch();
            $this->set_szOriginCountryStr(sanitize_all_html_input(trim($data['szOriginCountryStr'])));
            $this->set_szDestinationCountryStr(sanitize_all_html_input(trim($data['szDestinationCountryStr'])));
            $this->set_idServiceTerms(sanitize_all_html_input(trim($data['idServiceTerms'])));
            $this->set_dtShipmentDate(sanitize_all_html_input(trim($data['dtShipmentDate'])));
            $this->set_szCargoType(sanitize_all_html_input(trim($data['szCargoType']))); 
            $this->set_idCustomerCurrency(sanitize_all_html_input(trim($data['idCustomerCurrency']))); 
            
            if($mode=='FORWARDER_TRY_IT_OUT')
            {
                 //@TO DO
            }
            else
            {
                $this->set_idCustomerCountry(sanitize_all_html_input(trim($data['idCustomerCountry'])));  
            } 
            /*
            * Function For
            * Dimensions Validations
            */
            $kWHSSearch=new cWHSSearch();  
            $arrDescriptions = array("'__MAX_CARGO_DIMENSION_LENGTH__'","'__MAX_CARGO_DIMENSION_WIDTH__'","'__MAX_CARGO_DIMENSION_HEIGHT__'","'__MAX_CARGO_WEIGHT__'","'__MAX_CARGO_QUANTITY__'","'__MAX_CARGO_VOLUME__'"); 
            $bulkManagemenrVarAry = array();
            $bulkManagemenrVarAry = $kWHSSearch->getBulkManageMentVariableByDescription($arrDescriptions);      

            $maxCargoQty = $bulkManagemenrVarAry['__MAX_CARGO_QUANTITY__'];
            $maxCargoHeight = $bulkManagemenrVarAry['__MAX_CARGO_DIMENSION_HEIGHT__'];
            $maxCargoLength = $bulkManagemenrVarAry['__MAX_CARGO_DIMENSION_LENGTH__'];
            $maxCargoWidth = $bulkManagemenrVarAry['__MAX_CARGO_DIMENSION_WIDTH__'];
            $maxCargoWeight = $bulkManagemenrVarAry['__MAX_CARGO_WEIGHT__'];
            $maxCargoDimension = $bulkManagemenrVarAry['__MAX_CARGO_DIMENSION__'];
                 
            $tempCargoAry = array();
            $ctr = 0;
            if(!empty($data['szShipmentType']))
            {
                foreach($data['szShipmentType'] as $szShipmentType)
                {  
                    if($szShipmentType=='BREAK_BULK')
                    { 
                        $this->set_fTotalVolume(sanitize_all_html_input(trim($data[$szShipmentType]['fTotalVolume'])),$szShipmentType); 
                        $this->set_fTotalWeight(sanitize_all_html_input(trim($data[$szShipmentType]['fTotalWeight'])),$szShipmentType); 
                        
                        $tempCargoAry[$ctr]['szShipmentType'] = 'BREAK_BULK';
                        $tempCargoAry[$ctr]['fTotalVolume'] = $this->fTotalVolume;
                        $tempCargoAry[$ctr]['fTotalWeight'] = $this->fTotalWeight;
                        $tempCargoAry[$ctr]['szWeightMeasure'] = 'KG';
                        $tempCargoAry[$ctr]['szDimensionMeasure'] = 'CBM';
                        $ctr++;
                    }
                    else 
                    {
                        $parcelShipmentAry = array();
                        $lengthAry = array();
                        $parcelShipmentAry = $data[$szShipmentType];
                        $lengthAry = $parcelShipmentAry['iLength'];
                        $counter_parcel = 1;
                        if(!empty($lengthAry))
                        {
                            foreach($lengthAry as $key=>$lengthArys)
                            { 
                                $szCommodity='';
                                $fLength = sanitize_all_html_input($parcelShipmentAry['iLength'][$key]);
                                $fWidth = sanitize_all_html_input($parcelShipmentAry['iWidth'][$key]);
                                $fHeight = sanitize_all_html_input($parcelShipmentAry['iHeight'][$key]);
                                $fQuantity = sanitize_all_html_input($parcelShipmentAry['iQuantity'][$key]);
                                $fWeight = sanitize_all_html_input($parcelShipmentAry['iWeight'][$key]);
                                $idCargoMeasure = sanitize_all_html_input($parcelShipmentAry['idCargoMeasure'][$key]);
                                $idWeightMeasure = sanitize_all_html_input($parcelShipmentAry['idWeightMeasure'][$key]);
                                $szWeightType = sanitize_all_html_input($parcelShipmentAry['szWeightType'][$key]);
                                 
                                if($szShipmentType=='PALLET')
                                { 
                                    $iPalletType = sanitize_all_html_input($parcelShipmentAry['szPalletType'][$key]);
                                    $this->set_idWeightMeasure($iPalletType, $counter_parcel,$szShipmentType);
                                    $palletDetailAry = array();
                                    if($iPalletType>0)
                                    { 
                                        $palletDetailAry = $this->getPalletDetails($iPalletType); 
                                        //print_r($palletDetailAry);  
                                        $szCommodity =$palletDetailAry['szLongName']; 
                                    } 
                                    /*
                                     * Peviously we were picking thses values from constant but now we ae getting this from database 
                                     */
                                    if($iPalletType==1) // Euro pallet
                                    {
                                        $szPalletType = __PALLET_TYPE_EURO__;
                                    }
                                    else if($iPalletType==2) // Half pallet
                                    {
                                        $szPalletType = __PALLET_TYPE_HALF__;
                                    }
                                    else if($iPalletType==4) // Half pallet
                                    {
                                        $szPalletType = __PALLET_TYPE_HALF__;
                                    }
                                    else
                                    {
                                        $szPalletType = __PALLET_TYPE_OTHER__;
                                    }   
                                }   
                                if($szShipmentType=='PARCEL' || $szShipmentType=='PALLET')
                                {
                                    if($fWeight>0 && $fQuantity>0 && $szWeightType=='WEIGHT_IN_TOTAL')
                                    {
                                        $fWeight = round_up($fWeight/$fQuantity);
                                    } 
                                } 
                                if($idCargoMeasure>0)
                                {
                                    if($idCargoMeasure==1)
                                    {
                                        $szDimensionMeasure = 'CM';
                                        $fCmFactor  =1;
                                    }
                                    else
                                    {
                                        // If cargo measure is not CM then we conver this to CM
                                        $fCmFactor = $kWHSSearch->getCargoFactor($idCargoMeasure);
                                        $szDimensionMeasure = 'Inch';
                                        if($fCmFactor>0)
                                        {
                                           // $fLength = ceil($fLength / $fCmFactor); 
                                           // $fWidth = ceil($fWidth / $fCmFactor); 
                                           // $fHeight = ceil($fHeight / $fCmFactor); 
                                        } 
                                    } 
                                }  
                                if($idWeightMeasure>0)
                                {
                                    if($idWeightMeasure==1)
                                    {
                                        $szWeightMeasure = 'KG';
                                        $fKgFactor = 1;
                                    }
                                    else
                                    {
                                        $fKgFactor = $kWHSSearch->getWeightFactor($idWeightMeasure);
                                        $szWeightMeasure = "Pounds";
                                        //$fWeight = ceil($fWeight * $fKgFactor);
                                    } 
                                } 
                                if($szShipmentType=='PALLET' && $fHeight<=0)
                                {
                                    /*
                                    * If shipment type is pallet and height is empty then we use standard height as per pallet type
                                    */
                                    /*
                                    if($iPalletType==1) //Euro Pallet
                                    {
                                        //$fHeight = __STANDARD_HEIGHT_EURO_PALLET__;
                                        $fHeight = $palletDetailAry['fDefaultLength'];
                                    }
                                    else if($iPalletType==2) //Half Pallet
                                    {
                                        $fHeight = __STANDARD_HEIGHT_HALF_PALLET__;
                                    }
                                    * 
                                    */
                                    $fHeight = $palletDetailAry['fDefaultHeight'];
                                } 
                                
                                //echo "<br><br>Key: ".$key." <br> Length: ".$fLength."<br> Width: ".$fWidth." <br> Height: ".$fHeight." <br> Weight: ".$fWeight."<br> Count: ".$fQuantity; 
                                $this->set_iLength($fLength, $counter_parcel,$szShipmentType,$maxCargoLength);
                                $this->set_iWidth($fWidth, $counter_parcel,$szShipmentType,$maxCargoWidth);
                                $this->set_iHeight($fHeight, $counter_parcel,$szShipmentType,$maxCargoHeight);
                                $this->set_iWeight($fWeight, $counter_parcel,$szShipmentType,$maxCargoWeight);
                                $this->set_iQuantity($fQuantity, $counter_parcel,$szShipmentType,$maxCargoQty); 
                                $this->set_idCargoMeasure($idCargoMeasure, $counter_parcel,$szShipmentType);
                                $this->set_idWeightMeasure($idWeightMeasure, $counter_parcel,$szShipmentType);
                                  
                                $tempCargoAry[$ctr]['szShipmentType'] = $szShipmentType; 
                                $tempCargoAry[$ctr]['iLength'] = $fLength;
                                $tempCargoAry[$ctr]['iWidth'] = $fWidth;
                                $tempCargoAry[$ctr]['iHeight'] = $fHeight;
                                $tempCargoAry[$ctr]['iWeight'] = $fWeight;
                                $tempCargoAry[$ctr]['iQuantity'] = $fQuantity; 
                                $tempCargoAry[$ctr]['szWeightMeasure'] = $szWeightMeasure;
                                $tempCargoAry[$ctr]['szDimensionMeasure'] = $szDimensionMeasure;
                                $tempCargoAry[$ctr]['szCommodity'] = $szCommodity;
                                if($szShipmentType=='PALLET')
                                {
                                    $tempCargoAry[$ctr]['szPalletType'] = $szPalletType; 
                                }
                                $ctr++;
                                $counter_parcel++;
                            }
                        } 
                    } 
                }
            }
            else
            {
                $this->addError('szSpecialError','Please select packing type.');
            }
            if($this->error)
            {
                return false;
            }
            $dtShipmentDate = format_date($this->dtShipmentDate);
            
            if(strtotime($dtShipmentDate)<strtotime(date('Y-m-d')))
            {
                $this->addError('dtShipmentDate','Shipment date should not be less than today.');
                return false;
            } 
            
            $iLanguage = 1; //English
            $originCountryArr = reverse_geocode($this->szOriginCountryStr,true); 
            $kConfig = new cConfig(); 
            if(!empty($originCountryArr))
            {  
                $idOriginCountry=$kConfig->getCountryIdByCountryName($originCountryArr['szCountryName'],$iLanguage); 
                if($idOriginCountry<=0 && !empty($originCountryArr['szCountryCode']))
                {
                    $kConfig->loadCountry(false,$originCountryArr['szCountryCode']);
                    $idOriginCountry = $kConfig->idCountry ; 
                }  
                
                if(!empty($originCountryArr['szPostCode']))
                {
                    $szShipperPostcode = $originCountryArr['szPostCode']; 
                }
                else
                {
                    $szShipperPostcode = $originCountryArr['szPostcodeTemp']; 
                } 
                $szShipperLatitude = $originCountryArr['szLat'];
                $szShipperLongitude = $originCountryArr['szLng'];
                $szShipperCity = $originCountryArr['szCityName'];  
                $szShipperCountry = $originCountryArr['szCountryCode'];

                $this->idOriginCountry = $idOriginCountry;
                if(empty($szShipperPostcode))
                {
                    $postcodeCheckAry=array();
                    $postcodeResultAry = array();

                    $postcodeCheckAry['idCountry'] = $idOriginCountry ;
                    $postcodeCheckAry['szLatitute'] = $originCountryArr['szLat'] ;
                    $postcodeCheckAry['szLongitute'] = $originCountryArr['szLng'] ;
   
                    $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry); 
                    if(!empty($postcodeResultAry))
                    {
                        $szShipperPostcode = $postcodeResultAry['szPostCode']; 
                    }
                } 
            }
            if(empty($szShipperPostcode))
            {
                $this->addError('szSpecialError','Shipper postcode is required');
            }
            
            $destinationCountryArr = reverse_geocode($this->szDestinationCountryStr,true); 
            if(!empty($destinationCountryArr))
            {  
                $idDestinationCountry=$kConfig->getCountryIdByCountryName($destinationCountryArr['szCountryName'],$iLanguage);

                if($idDestinationCountry<=0 && !empty($destinationCountryArr['szCountryCode']))
                {
                    $kConfig->loadCountry(false,$destinationCountryArr['szCountryCode']);
                    $idDestinationCountry = $kConfig->idCountry ; 
                } 
                if(!empty($destinationCountryArr['szPostCode']))
                {
                    $szConsigneePostcode = $destinationCountryArr['szPostCode'];  
                }
                else
                {
                    $szConsigneePostcode = $destinationCountryArr['szPostcodeTemp'];  
                } 
                $szConsigneeLatitude = $destinationCountryArr['szLat'];
                $szConsigneeLongitude = $destinationCountryArr['szLng']; 
                $szConsigneeCity = $destinationCountryArr['szCityName'];  
                $szConsigneeCountry = $destinationCountryArr['szCountryCode'];
                $this->idDestinationCountry = $idDestinationCountry;

                if(empty($szConsigneePostcode))
                {
                    $postcodeCheckAry=array();
                    $postcodeResultAry = array();

                    $postcodeCheckAry['idCountry'] = $idDestinationCountry ;
                    $postcodeCheckAry['szLatitute'] = $destinationCountryArr['szLat'] ;
                    $postcodeCheckAry['szLongitute'] = $destinationCountryArr['szLng'] ;
 
                    $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry); 
                    if(!empty($postcodeResultAry))
                    {
                        $szConsigneePostcode = $postcodeResultAry['szPostCode']; 
                    }
                } 
            }  
            
            if(empty($szConsigneePostcode))
            {
                $this->addError('szSpecialError','Consignee postcode is required');
            }
            $kWhsSearch = new cWHSSearch(); 
            if($this->idCustomerCurrency==1)
            {
                $szCurrency = 'USD';
            } 
            else
            {
                $resultAry = $kWhsSearch->getCurrencyDetails($this->idCustomerCurrency);  
                $szCurrency = $resultAry['szCurrency'];  
            }
            
            $serviceTermsAry = array();
            $serviceTermsAry = $kConfig->getAllServiceTerms($this->idServiceTerms,false,true);
            if(!empty($serviceTermsAry))
            {
                $szServiceTerm = $serviceTermsAry[0]['szShortTerms'];
            }
            
            if($mode=='FORWARDER_TRY_IT_OUT')
            {
                //From forwarder quick quote we don't calculate VAT so customer billing country is not required. But as szBillingCountry is required field in partner API so that we are finding out szBillingCountry based on szServiceTerm.
                $kPartner = new cPartner();
                $serviceTermAry = array();
                $serviceTermAry['szServiceTerm'] = $szServiceTerm;
                $serviceTermAry['szShipperCountry'] = $szShipperCountry;
                $serviceTermAry['szConsigneeCountry'] = $szConsigneeCountry;
                $szBillingCountry = $kPartner->getBillingCountryByTerms($serviceTermAry);
            } 
            else
            { 
                $kCustomerCountry->loadCountry($this->idCustomerCountry);
                $szBillingCountry = $kCustomerCountry->szCountryISO; 
            } 
            
            $szShipperConsigneeType = "";
            if($szBillingCountry==$szShipperCountry)
            {
                $szShipperConsigneeType = "SHIPPER";
            }
            else if($szBillingCountry==$szConsigneeCountry)
            {
                $szShipperConsigneeType = "CONSIGNEE";
            }
            else
            {
                $szShipperConsigneeType = "BILLING";
            }
            
            $quickQuoteRequestAry = array();
            $quickQuoteRequestAry['dtDay'] = date('d',strtotime($dtShipmentDate));
            $quickQuoteRequestAry['dtMonth'] = date('m',strtotime($dtShipmentDate));
            $quickQuoteRequestAry['dtYear'] = date('Y',strtotime($dtShipmentDate));
            $quickQuoteRequestAry['szServiceTerm'] = $szServiceTerm; 
            $quickQuoteRequestAry['szShipperLatitude'] = $szShipperLatitude;
            $quickQuoteRequestAry['szShipperLongitude'] = $szShipperLongitude; 
            $quickQuoteRequestAry['szShipperPostcode'] = $szShipperPostcode;
            $quickQuoteRequestAry['szShipperCity'] = $szShipperCity;
            $quickQuoteRequestAry['szShipperCountry'] = $szShipperCountry;   
            $quickQuoteRequestAry['szConsigneeLatitude'] = $szConsigneeLatitude;
            $quickQuoteRequestAry['szConsigneeLongitude'] = $szConsigneeLongitude;
            $quickQuoteRequestAry['szConsigneePostcode'] = $szConsigneePostcode;
            $quickQuoteRequestAry['szConsigneeCity'] = $szConsigneeCity;
            $quickQuoteRequestAry['szConsigneeCountry'] = $szConsigneeCountry;
            $quickQuoteRequestAry['szCurrency'] = $szCurrency;   
            $quickQuoteRequestAry['szBillingCountry'] = $szBillingCountry;    
            $quickQuoteRequestAry['iShipperConsigneeType'] = $szShipperConsigneeType;    
            $quickQuoteRequestAry['szLanguage'] = 'EN';
            $quickQuoteRequestAry['szCargoType'] = $this->szCargoType; 
            $quickQuoteRequestAry['iCustomerType'] = $data['szCustomerType'];  
            $quickQuoteRequestAry['szOriginCountryStr'] = $this->szOriginCountryStr;
            $quickQuoteRequestAry['szDestinationCountryStr'] = $this->szDestinationCountryStr;
            $quickQuoteRequestAry['cargo'] = $tempCargoAry;    
            return $quickQuoteRequestAry;
        }
    }
    
    function searchQuickQuoteRequest($data)
    {
        if(!empty($data))
        {   
            $quickQuoteRequestAry = $this->validateQuickQuoteRequest($data); 
            
            if($this->error==true)
            {  
                return false;
            }   
            /*
             * This is just hardcoded dummy partner profile to satisfy partner API needs
             */
            $szPartnerAlies = "QQ";
            $idPartner = $this->idDummyGlobalPartner;
            /*
            * We create token only in case of price request and in case of booking we use token what we created o the time of service request.
            */
            $kPartner = new cPartner(); 
            $szReferenceToken = $kPartner->getPartnerReferenceToken($szPartnerAlies); 
            $szReferenceToken = $kPartner->isReferenceTokenExist($szReferenceToken,$szPartnerAlies);
                
            /*
            *  Initializing partner class variables to bypass authorization check and other stuff of Partner API
            */
            cPartner::$szApiRequestMode = 'QUICK_QUOTE';
            cPartner::$szGlobalPartnerAlies = $szPartnerAlies;
            cPartner::$idGlobalPartner = $idPartner; 
            cPartner::$szGlobalToken = $szReferenceToken;   
            cPartner::$szClientUserAgent = __HTTP_USER_AGENT__;
            cPartner::$szUserAgent = __HTTP_USER_AGENT__; 
             
            if($data['szCustomerType']==1)
            {
                $quickQuoteRequestAry['iPrivateShipping'] = 1;
            }  
            $apiResponseAry = array();
            $apiResponseAry = $kPartner->getPrice($quickQuoteRequestAry,'QUICK_QUOTE');
             
            
            if(!empty($kPartner->arErrorMessages))
            {
                $this->szApiErrorAry = $kPartner->arErrorMessages;
                return false;
            } 
            $this->szPriceCalculationLogs = cPartner::$priceCalculationLogs;
            $this->szQuickQuoteToken = $szReferenceToken;
            $this->idQuickQuotePartner = $idPartner; 
            return $apiResponseAry;            
        }
    } 
    function forwarder_try_it_out_search($data)
    { 
        if(!empty($data))
        {  
            $szShipmentType = $data['szShipmentType'];
            unset($data['szShipmentType']);
            
            $data['szCargoType'] = '__GENERAL_CODE__'; 
            $data['szShipmentType'][0] = $szShipmentType;
              
            $quickQuoteRequestAry = $this->validateQuickQuoteRequest($data,'FORWARDER_TRY_IT_OUT'); 
            if($this->error==true)
            {  
                return false;
            }  
            /*
             * This is just hardcoded dummy partner profile to satisfy partner API needs
             */
            $szPartnerAlies = "QQ";
            $idPartner = $this->idDummyGlobalPartner;
            /*
            * We create token only in case of price request and in case of booking we use token what we created o the time of service request.
            */
            $kPartner = new cPartner(); 
            $szReferenceToken = $kPartner->getPartnerReferenceToken($szPartnerAlies); 
            $szReferenceToken = $kPartner->isReferenceTokenExist($szReferenceToken,$szPartnerAlies);
                
            /*
            *  Initializing partner class variables to bypass authorization check and other stuff of Partner API
            */
            cPartner::$szApiRequestMode = 'QUICK_QUOTE';
            cPartner::$szGlobalPartnerAlies = $szPartnerAlies;
            cPartner::$idGlobalPartner = $idPartner; 
            cPartner::$szGlobalToken = $szReferenceToken;   
            cPartner::$szClientUserAgent = __HTTP_USER_AGENT__;
            cPartner::$szUserAgent = __HTTP_USER_AGENT__; 
             
            if($data['szCustomerType']==1)
            {
                $quickQuoteRequestAry['iPrivateShipping'] = 1;
            } 
            $apiResponseAry = array();
            $apiResponseAry = $kPartner->getPrice($quickQuoteRequestAry,'QUICK_QUOTE',1);
                
            if(!empty($kPartner->arErrorMessages))
            {
                $this->szApiErrorAry = $kPartner->arErrorMessages;
                return false;
            } 
            $this->szPriceCalculationLogs = cPartner::$priceCalculationLogs;
            $this->szQuickQuoteToken = $szReferenceToken;
            $this->idQuickQuotePartner = $idPartner;
            return $apiResponseAry;            
        } 
    }
    function quickQuoteBuildConvertToRFQData($data)
    {   
        if(!empty($data))
        { 
            $szReferenceToken = sanitize_all_html_input(trim($data['szToken'])); 
            if(!empty($data['szServiceIds']))
            {
                $serviceIDAry = $data['szServiceIds'];
            }
            else if(!empty($data['szAllServieIDS']))
            { 
                $szServicePipeLine = rtrim($data['szAllServieIDS'],"||||"); 
                $serviceIDAry = explode("||||",$szServicePipeLine);
            }  
            $kPartner = new cPartner();
            $shipmentTypeAry = array();
            if(!empty($serviceIDAry))
            {
                foreach($serviceIDAry as $serviceIDArys)
                {
                    $shipmentTypeAry[] = $data['idShipmentType'][$serviceIDArys];
                } 
                if(!empty($shipmentTypeAry))
                {
                    $shipmentTypeAry = array_unique($shipmentTypeAry);
                } 
                
                cPartner::$idGlobalPartner = $this->idDummyGlobalPartner;
                cPartner::$szGlobalToken = $data['szToken'];
                cPartner::$szGlobalMode = "QUICK_QUOTE";
                if(count($shipmentTypeAry)>1)
                {
                    foreach($shipmentTypeAry as $shipmentTypeArys)
                    {
                        $iBookingQuoteShipmentTypeIC = $shipmentTypeArys; //Parcel Shipment 
                        $parcelDetailsAry = array();
                        $parcelDetailsAry = $kPartner->updateBookingCargDetails($iBookingQuoteShipmentTypeIC);
                        if(!empty($parcelDetailsAry['szCargoTotalLine']))
                        {
                            $szInterCommentsCargoStr .= $parcelDetailsAry['szCargoTotalLine'];
                        }
                    } 
                }
            } 
            else
            {
                $idPartner = $this->idDummyGlobalPartner;
                cPartner::$idGlobalPartner = $idPartner;
                cPartner::$szGlobalToken = $szReferenceToken; 
                cPartner::$szApiRequestMode = "QUICK_QUOTE";

                $postSearchAry = $kPartner->getQuickQuoteRequestData();  
                if(!empty($postSearchAry))
                {
                    $ctr=0;
                    if($postSearchAry['iSearchParcel']==1)
                    {
                        $shipmentTypeAry[$ctr] = 1; //Parcel
                        $ctr++;
                    } 
                    if($postSearchAry['iSearchPallet']==1)
                    {
                        $shipmentTypeAry[$ctr] = 2; //Pallet
                        $ctr++;
                    }
                    if($postSearchAry['iSearchBreakBulk']==1)
                    {
                        $shipmentTypeAry[$ctr] = 3; //Break Bulk
                        $ctr++;
                    }  
                }
            } 
            $this->shipingTypesAry = $shipmentTypeAry;
            $this->szInterCommentsCargoStr = $szInterCommentsCargoStr; 
            return true;
        }
    } 
    function quickQuoteConvertToRFQ($data,$mode=false,$iQuickQuoteTryITOutFlag=false)
    {  
        if(!empty($data))
        {    
            $kPartner = new cPartner(); 
            $kRegisterShipCon = new cRegisterShipCon();
            $kConfig = new cConfig();
            $kBooking = new cBooking();
            $idBooking = (int)$data['idBooking'];
            $iSendQuickQuote = (int)$data['iSendQuickQuote'];
             
            $szReferenceToken = sanitize_all_html_input(trim($data['szToken']));
            $szServicePipeLine = sanitize_all_html_input(trim($data['szServicePipeLine']));
            
            $createQuotePricing = false;
            if(!empty($data['szServiceIds']))
            {
                $serviceIDAry = $data['szServiceIds'];
                /*
                * We only create quote pricng if customer has selected any service from service list.
                */
                $createQuotePricing = true;
            }
            else if(!empty($data['szAllServieIDS']))
            { 
                $szServicePipeLine = rtrim($data['szAllServieIDS'],"||||"); 
                $serviceIDAry = explode("||||",$szServicePipeLine);
            }  
            if(!empty($serviceIDAry))
            {
                $ctr = 0;
                foreach($serviceIDAry as $serviceIDArys)
                {
                    $idTransportMode = $data['idTransportMode'][$serviceIDArys];
                    $shipmentTypeAry[$ctr] = $data['idShipmentType'][$serviceIDArys];
                }
            } 
            $kConfig = new cConfig();
            $kConfig->loadCountry($data['szCustomerCountry']);
            if($kConfig->iDefaultLanguage>0)  
            {
                $iBookingLanguage = $kConfig->iDefaultLanguage;
            }
            else
            {
                $iBookingLanguage = __LANGUAGE_ID_ENGLISH__;
            } 
            $szInternalComment = $data['szInternalComment'];
            if($mode=='CONVERT_RFQ_CONFIRM')
            {
                $iBookingQuoteShipmentType = $data['idPackingType'];
            }
            else
            { 
                $iParcel = __PACKET_TYPE_ID_PARCEL__;
                $iPallet = __PACKET_TYPE_ID_PALLET__;
                $iBreakBulk = __PACKET_TYPE_ID_BREAK_BULK__;
                if(!empty($shipmentTypeAry) && in_array($iParcel,$shipmentTypeAry))
                {
                    $iBookingQuoteShipmentType = $iParcel;
                } 
                else if(!empty($shipmentTypeAry) && in_array($iPallet,$shipmentTypeAry))
                {
                    $iBookingQuoteShipmentType = $iPallet;
                } 
                else if(!empty($shipmentTypeAry) && in_array($iBreakBulk,$shipmentTypeAry))
                {
                    $iBookingQuoteShipmentType = $iBreakBulk;
                } 
            }
            
            $kConfig = new cConfig();
            $transportModeListAry = array();
            $transportModeListAry = $kConfig->getAllTransportMode(false,$iBookingLanguage,true,true);
            $szTransportMode = $transportModeListAry[$idTransportMode]['szShortName'];
            $szFrequency = $transportModeListAry[$idTransportMode]['szFrequency']; 
            
            $idPartner = $this->idDummyGlobalPartner;
            cPartner::$idGlobalPartner = $idPartner;
            cPartner::$szGlobalToken = $szReferenceToken; 
            cPartner::$szApiRequestMode = "QUICK_QUOTE";
        
            $postSearchAry = $kPartner->getQuickQuoteRequestData();
              
            if($data['idGoodsInsuranceCurrency']==1)
            {
                $szGoodsInsuranceCurrency = "USD";
                $fGoodsInsuranceExchangeRate = 1;
            }
            else
            {
                $kWarehouseSearch = new cWHSSearch();
                $currencyDetailsAry = $kWarehouseSearch->getCurrencyDetails($data['idGoodsInsuranceCurrency']);	 
                $fGoodsInsuranceExchangeRate = $currencyDetailsAry['fUsdValue'];
                $szGoodsInsuranceCurrency = $currencyDetailsAry['szCurrency'];
            }
            
            $postSearchAry['fCargoValue'] = $data['fCargoValue']; 
            $postSearchAry['szCargoDescription'] = $data['szCargoDescription'];
            $postSearchAry['idGoodsInsuranceCurrency'] = $data['idGoodsInsuranceCurrency'];
            $postSearchAry['szGoodsInsuranceCurrency'] = $szGoodsInsuranceCurrency;
            $postSearchAry['fGoodsInsuranceExchangeRate'] = $fGoodsInsuranceExchangeRate;
 
            $isMoving = false;
            $szCargoType = $postSearchAry['szCargoType'];
            if($szCargoType=='__PERSONAL_EFFECTS__' || $szCargoType=='__ART__')
            {
                $isMoving = 1;
            }
            
            $iPrivateCustomer = false;
            if($postSearchAry['iCustomerType']==1)
            {
                $iPrivateCustomer = 1;
            } 
            $kBooking_new = new cBooking();  
           
            if($idBooking>0)
            {
                //Booking has been already created                
                $bookingDetailsAry = $kBooking_new->getBookingDetails($idBooking);
                $idBookingFile = $bookingDetailsAry['idFile'];
                $szBookingRandomNum = $bookingDetailsAry['szBookingRandomNum'];
                $bAddingNewBooking = false;
            }
            else
            {
                /*
                * Creating actual booking into the system
                */
                $idBooking = $kPartner->createEmptyBooking();
                $szBookingRandomNum =$kPartner->szBookingRandomNum;
                $bAddingNewBooking = true;
                if($idBooking<=0)
                {
                    $errorFlag = true;
                    $this->szDeveloperNotes .= ">. Create booking function returning with error message because we could not able to create Booking ID in tblbookings ".PHP_EOL; 
                } 
                else
                {
                    $this->szDeveloperNotes .= ">. Empty booking created successfully. Booking id is:".$idBooking.", Going forward ".PHP_EOL;
                }
            } 
            
            $this->szBillingFirstName = $data['szFirstName'];
            $this->szBillingLastName = $data['szLastName'];
            $this->szBillingCompanyName = $data['szCustomerCompanyName'];
            $this->szBillingEmail = $data['szEmail'];
            $this->szBillingAddress = $data['szCustomerAddress1'];
            $this->szBillingPostcode = $data['szCustomerPostCode'];
            $this->szBillingCity = $data['szCustomerCity'];
            $this->szBillingState = $data['szBillingState'];
            $this->idBillingCountry = $data['szCustomerCountry'];
            $this->szBillingPhone = $data['szCustomerPhoneNumber'];
            $this->idCustomerDialCode = $data['idCustomerDialCode'];
            $iInsuranceIncluded = (int)$data['iInsuranceIncluded'];
               
            $kUser = new cUser();
            if(!empty($data['szEmail']))
            {
                if($kUser->isUserEmailExist($data['szEmail']))
                {  
                    $idCustomer = $kUser->idIncompleteUser ; 
                    
                    $updateUserDetailsAry = array(); 
                    $updateUserDetailsAry['szFirstName'] = $this->szBillingFirstName ;
                    $updateUserDetailsAry['szLastName'] = $this->szBillingLastName ; 
                    $updateUserDetailsAry['szPostCode'] = $this->szBillingPostcode ;
                    $updateUserDetailsAry['szCity'] = $this->szBillingCity ; 
                    $updateUserDetailsAry['szState'] = $this->szBillingState ;   
                    $updateUserDetailsAry['szCompanyName'] = $this->szBillingCompanyName ;  
                    $updateUserDetailsAry['szAddress'] = $this->szBillingAddress ;
                    $updateUserDetailsAry['szCurrency'] = $postSearchAry['idCurrency']; 
                    $updateUserDetailsAry['szPhone'] = $this->szBillingPhone;
                    $updateUserDetailsAry['idInternationalDialCode'] = $this->idBillingDialCode ;
                    $updateUserDetailsAry['idCountry'] = $this->idBillingCountry ; 
                    $updateUserDetailsAry['iPrivate'] = $iPrivateCustomer;
                    $updateUserDetailsAry['iLanguage'] = $iBookingLanguage; 
 
                    if(!empty($updateUserDetailsAry))
                    {
                        $update_user_query = '';
                        foreach($updateUserDetailsAry as $key=>$value)
                        {
                            $update_user_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                        }
                    } 
                    $update_user_query = rtrim($update_user_query,",");  
                    if($kRegisterShipCon->updateUserDetailsByQuotes($update_user_query,$idCustomer))
                    {
                         
                    }
                }
                else
                { 
                    $kRegisterShipCon = new cRegisterShipCon();  
                    $kBooking = new cBooking();
                    $szPassword = mt_rand(0,99)."".$kRegisterShipCon->generateUniqueToken(8);

                    //User is not loogedin then we create a new profile
                    $addUserAry = array();
                    $addUserAry['szFirstName'] = $this->szBillingFirstName ;
                    $addUserAry['szLastName'] = $this->szBillingLastName ;
                    $addUserAry['szEmail'] = $this->szBillingEmail ;
                    $addUserAry['szPassword'] = $szPassword ;
                    $addUserAry['szConfirmPassword'] = $szPassword ;
                    $addUserAry['szPostCode'] = $this->szBillingPostcode ;
                    $addUserAry['szCity'] = $this->szBillingCity ; 
                    $addUserAry['szState'] = $this->szBillingState ; 
                    $addUserAry['szCountry'] = $this->idBillingCountry ; 
                    $addUserAry['szPhoneNumberUpdate'] = $this->szBillingPhone ;
                    $addUserAry['szCompanyName'] = $this->szBillingCompanyName ;  
                    $addUserAry['szAddress1'] = $this->szBillingAddress ;
                    $addUserAry['szCurrency'] = $postSearchAry['idCurrency']; 
                    $addUserAry['iThisIsMe'] = $data['iThisIsMe'];
                    $addUserAry['iConfirmed'] = 0; 
                    $addUserAry['iFirstTimePassword'] = 1;  
                    $addUserAry['iPrivate'] = $iPrivateCustomer;  
                    $addUserAry['idInternationalDialCode'] = $this->idCustomerDialCode ;
                    $addUserAry['iLanguage'] = $iBookingLanguage;

                    if($kUser->createAccount($addUserAry,'QUICK_QUOTE'))
                    { 
                        $idCustomer = $kUser->id ;   
                    }  
                } 
            }
            if($idCustomer>0)
            {
                $kUser->getUserDetails($idCustomer);
            } 
            if($idBookingFile>0)
            {
                /*
                * While updating booking file we only updated file status if user clicks on 'Convert to RFQ' button 
                */
                if($mode=='CONVERT_RFQ_CONFIRM')
                {
                    $szFileStatus = 'S1';
                    $szTaskStatus = 'T1';
                    $iSearchType = 2; //RFQ
                    
                    $dtResponseTime = $kBooking_new->getRealNow();
                    $fileLogsAry = array(); 
                    $fileLogsAry['szTransportecaStatus'] = $szFileStatus;
                    $fileLogsAry['szTransportecaTask'] = $szTaskStatus; 
                    $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
                    $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime;
                    $fileLogsAry['idCustomerOwner'] = $kUser->idCustomerOwner ;
                    $fileLogsAry['idFileOwner'] = $kUser->idCustomerOwner ;
                    $fileLogsAry['iSearchType'] = $iSearchType;
                    
                    if(!empty($fileLogsAry))
                    {
                        $file_log_query = '';
                        foreach($fileLogsAry as $key=>$value)
                        {
                            $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                        } 
                        $file_log_query = rtrim($file_log_query,","); 
                        if($kBooking->updateBookingFileDetails($file_log_query,$idBookingFile))
                        {
                            //@TO DO
                        }
                    } 
                }
            }
            else
            {
                /*
                * Creating booking file into the system
                */
                $idBookingFile = $kPartner->createBookingFile($postSearchAry,$kUser,$idBooking,true,true);
                if($idBookingFile<=0)
                {
                    $errorFlag = true;
                    $this->szDeveloperNotes .= ">. Create booking function returning with error message because we could not able to create Booking file in tblbookingfiles ".PHP_EOL;
                    return false;  
                }   
                else
                {
                    $this->szDeveloperNotes .= ">. Booking file created successfully. File ID: ".$idBookingFile.", Going forward ".PHP_EOL;
                } 
            } 
            /*
            * Creating shipper consignee details into the system
            */ 
            $data['iCustomerType'] = $postSearchAry['iCustomerType']; 
            $iShipperConsignee = $data['iShipperConsignee'];
            
            $data['szShipperAddress'] = $data['szOriginAddress'];
            $data['szConsigneeAddress'] = $data['szDestinationAddress'];
            
           
            
            
            
            $idShipperConsignee = $kPartner->createShipperConsignee($data,$idBooking); 
            if($idShipperConsignee<=0)
            {
                $errorFlag = true;
                $this->szDeveloperNotes .= ">. Create booking function returning with error message because we could not able to create shipper consignee details ".PHP_EOL;
                return false; 
            }  
            else
            {
                $this->szDeveloperNotes .= ">. Shipper consignee row created successfully. ID: ".$idShipperConsignee.", Going forward ".PHP_EOL;
            } 
            $kWHSSearch = new cWHSSearch();
            $kBooking = new cBooking();
               
            /*
            * Adding search logs to file log 
            */
            $searchLogsAry = array();
            $searchLogsAry['idUser'] = $idCustomer;
            $searchLogsAry['id'] = $idBooking;
            $searchLogsAry['idFile'] = $idBookingFile;
            $searchLogsAry['szOriginCountry'] = $postSearchAry['szOriginCountryStr'];
            $searchLogsAry['szDestinationCountry'] = $postSearchAry['szDestinationCountryStr'];
            $searchLogsAry['fCargoVolume'] = $cargoDetailsAry['fTotalVolume'];
            $searchLogsAry['fCargoWeight'] = $cargoDetailsAry['fTotalWeight'];
            $searchLogsAry['dtTimingDate'] = $postSearchAry['dtShipping'];
            $searchLogsAry['iQuickQuote'] = 1;
            $searchLogsAry['iQuickQuoteTryITOutFlag']=$iQuickQuoteTryITOutFlag;
            
            $kBooking->insertBookingSearchLogs($searchLogsAry); 
                
            $kWarehouseSearch = new cWHSSearch();
            $resultAry = $kWarehouseSearch->getCurrencyDetails(20);	 
            $fDkkExchangeRate = 0;
            if($resultAry['fUsdValue']>0)
            {
                $fDkkExchangeRate = $resultAry['fUsdValue'] ;						 
            }   
            
            /*
            *  getting total of shipment value for the booking
            */
            $idShipmentType = $iBookingQuoteShipmentType; 
            $cargoDetailsAry = array();
            $cargoDetailsAry = $kPartner->updateBookingCargDetails($idShipmentType);
                 
            if(!empty($postSearchAry['szServiceTerm']))
            {
                $serviceTermsAry = array();
                $serviceTermsAry = $kConfig->getAllServiceTerms(false,$postSearchAry['szServiceTerm']);
                $idServiceTerms = $serviceTermsAry[0]['id'];
            }
            $bookingAry = array();
            $bookingAry['idServiceType'] = $postSearchAry['idServiceType']; 
            $bookingAry['iOriginCC'] = $postSearchAry['iOriginCC'];
            $bookingAry['iDestinationCC'] = $postSearchAry['iDestinationCC'];
            $bookingAry['idTimingType'] = $postSearchAry['idTimingType'];
            $bookingAry['dtTimingDate'] = $postSearchAry['dtShipping']; 
            
            $bookingAry['idOriginCountry'] = $postSearchAry['szShipperCountry'];
            $bookingAry['szOriginCountry'] = $postSearchAry['szOriginCountryStr'];
            $bookingAry['szOriginPostCode'] = $postSearchAry['szShipperPostCode'];
            $bookingAry['szOriginCity'] = $postSearchAry['szShipperCity'];
            $bookingAry['fOriginLatitude'] = $postSearchAry['szShipperLattitude'];
            $bookingAry['fOriginLongitude'] = $postSearchAry['szShipperLongitude'];  
            
            $bookingAry['idDestinationCountry'] = $postSearchAry['szConsigneeCountry'];
            $bookingAry['szDestinationCountry'] = $postSearchAry['szDestinationCountryStr'];
            $bookingAry['szDestinationPostCode'] = $postSearchAry['szConsigneePostCode'];
            $bookingAry['szDestinationCity'] = $postSearchAry['szConsigneeCity']; 
            $bookingAry['fDestinationLongitude'] = $postSearchAry['szConsigneeLongitude'];
            $bookingAry['fDestinationLatitude'] = $postSearchAry['szConsigneeLattitude'];  
            $bookingAry['isMoving'] = $isMoving;  
            $bookingAry['szCargoType'] = $szCargoType;   
            $bookingAry['idTransportMode'] = $idTransportMode;
            $bookingAry['szTransportMode'] = $szTransportMode; 
            $bookingAry['szFrequency'] = $szFrequency;
            $bookingAry['szPartnerReference'] = $postSearchAry['szPartnerReference'];
            $bookingAry['idServiceTerms'] = $idServiceTerms; 
            $bookingAry['fCargoVolume'] = $cargoDetailsAry['fTotalVolume'];
            $bookingAry['fCargoWeight'] = $cargoDetailsAry['fTotalWeight'];
            $bookingAry['idUser'] = $kUser->id ;				
            $bookingAry['szFirstName'] = $this->szBillingFirstName ;
            $bookingAry['szLastName'] = $this->szBillingLastName ;
            $bookingAry['szEmail'] = $this->szBillingEmail ;
            $bookingAry['szCustomerAddress1'] = $this->szBillingAddress;
            $bookingAry['szCustomerAddress2'] = $this->szBillingAddress2;
            $bookingAry['szCustomerAddress3'] = $this->szBillingAddress3;
            $bookingAry['szCustomerPostCode'] = $this->szBillingPostcode;
            $bookingAry['szCustomerCity'] = $this->szBillingCity;
            $bookingAry['szCustomerCountry'] = $this->idBillingCountry;
            $bookingAry['szCustomerState'] = $this->szBillingState;
            $bookingAry['szCustomerPhoneNumber'] = $this->szBillingPhone;
            $bookingAry['szCustomerCompanyName'] = $this->szBillingCompanyName;	
            $bookingAry['szCustomerCompanyRegNo'] = $this->szBillingCompanyRegNo; 
            $bookingAry['iPrivateShipping'] = $iPrivateCustomer;
            $bookingAry['iPrivateCustomer'] = $iPrivateCustomer;
            $bookingAry['idCustomerDialCode'] = $this->idCustomerDialCode;
            $bookingAry['idShipperConsignee'] = $idShipperConsignee; 
            $bookingAry['iBookingLanguage'] = $iBookingLanguage;
            $bookingAry['iBookingStep'] = 13;
            $bookingAry['dtBookingInitiated'] = $dtCurrentDateTime;
            $bookingAry['iFromRequirementPage'] = 2;
            $bookingAry['idCustomerCurrency'] = $postSearchAry['idCurrency'];
            $bookingAry['szCustomerCurrency'] = $postSearchAry['szCurrency'];
            $bookingAry['fCustomerExchangeRate'] = $postSearchAry['fExchangeRate'];
            $bookingAry['iShipperConsignee'] = $iShipperConsignee; 
            $bookingAry['iTotalQuantity'] = $cargoDetailsAry['iTotalQuantity']; 
            $bookingAry['idFile'] = $idBookingFile;
            $bookingAry['iNumColli'] = $cargoDetailsAry['iNumColli'];        
            $bookingAry['iPalletType'] = $iPalletType;
            $bookingAry['szCargoDescription'] = $postSearchAry['szCargoDescription'];
            $bookingAry['fDkkExchangeRate'] = $fDkkExchangeRate; 
            $bookingAry['szInvoiceComment'] = $postSearchAry['szParnerReference']; 
            $bookingAry['iCargoPackingType'] = $idShipmentType;
            
            if($iInsuranceIncluded==1) //Yes
            {
                $bookingAry['iInsuranceMandatory'] = 1;
                $bookingAry['iInsuranceIncluded'] = 1;
            }
            else if($iInsuranceIncluded==3) //Optional
            {
                $bookingAry['iInsuranceMandatory'] = 0;
                $bookingAry['iInsuranceIncluded'] = 0;
            }
            else
            {   //No
                $bookingAry['iInsuranceMandatory'] = 0;
                $bookingAry['iInsuranceIncluded'] = 0;
            } 
            $bookingAry['iInsuranceChoice'] = $iInsuranceIncluded ; 
             
            if((float)$postSearchAry['fCargoValue']>0)
            {
                $fValueOfGoodsUSD = round(($postSearchAry['fCargoValue']*$postSearchAry['fGoodsInsuranceExchangeRate']),4);
            }
            $bookingAry['idGoodsInsuranceCurrency'] = $postSearchAry['idGoodsInsuranceCurrency'];
            $bookingAry['szGoodsInsuranceCurrency'] = $postSearchAry['szGoodsInsuranceCurrency'];
            $bookingAry['fGoodsInsuranceExchangeRate'] = $postSearchAry['fGoodsInsuranceExchangeRate'];
            $bookingAry['fValueOfGoods'] = $postSearchAry['fCargoValue'];
            $bookingAry['fValueOfGoodsUSD'] = $fValueOfGoodsUSD; 
            $bookingAry['idBookingStatus'] = __BOOKING_STATUS_DRAFT__; 
            $bookingAry['szInternalComment'] = $szInternalComment; 
            $bookingAry['szReferenceToken'] = $szReferenceToken;
            
            if($mode=='CONVERT_RFQ_CONFIRM')
            {
                /*
                * Following fields are only updated in case when user clicks on 'Convert to RFQ' button on quick quote page.
                */
                $bookingAry['iBookingQuotes'] = 1;
                $bookingAry['iQuotesStatus'] = __BOOKING_QUOTES_STATUS_NEW_BOOKING__ ;  
                $bookingAry['iBookingType'] = __BOOKING_TYPE_RFQ__; 
                $bookingAry['iQuickQuote'] = __QUICK_QUOTE_CONVERT_RFQ__;
                $bookingAry['iCourierBooking'] = 0;
            } 
            else if($mode=='SEND_QUOTE' || $mode=='CREATE_BOOKING')
            {
                $bookingAry['iQuickQuote'] = __QUICK_QUOTE_DRAFT_QUOTE__; 
            }
            $update_query = ''; 
            if(!empty($bookingAry))
            {
                foreach($bookingAry as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            } 
            $update_query = rtrim($update_query,","); 
            if($kBooking->updateDraftBooking($update_query,$idBooking))
            { 
                $this->szDeveloperNotes .= ">. Finnally booking has been successfully created with data: ".print_R($bookingAry,true).", Going forward ".PHP_EOL;
                  
                /*
                *  IF shipment type is 'Parcel' or 'Pallet' then we update cargo lines in tblcargo
                */
                if($idShipmentType==1 || $idShipmentType==2)
                {
                    $cargoDetailsAry = array(); 
                    /*
                     * Deleting all previously added cargo Lines
                     */
                    $kBooking->deleteCargoByBookingId($idBooking);
                    /*
                     * Adding New cargo Lines to booking
                     */
                    $kPartner->updateBookingCargDetails($idShipmentType,$idBooking,true);
                }  
                else
                {
                    /*
                     * In case of Break bulk we delete all the cargo lines.
                     */
                    $kBooking->deleteCargoByBookingId($idBooking);
                }
                 
                $bookingDataAry = array();
                $bookingDataAry = $kBooking->getBookingDetails($idBooking);
               
                if(!empty($serviceIDAry) && $createQuotePricing)
                { 
                    cPartner::$idGlobalPartner = $this->idDummyGlobalPartner;
                    cPartner::$szGlobalToken = $szReferenceToken; 
                     
                    $kWHSSearch = new cWHSSearch();
                    $tempResultAry = $kWHSSearch->getSearchedDataFromTempData(false,false,false,true);
                    $searchResultAry = unserialize($tempResultAry['szSerializeData']);

                    $service_ctr = 0;
                    if(!empty($searchResultAry))
                    {
                        foreach($searchResultAry as $searchResultArys)
                        {
                            $szUniqueServiceID = $searchResultArys['unique_id']; 
                            if(in_array($szUniqueServiceID,$serviceIDAry))
                            { 
                                $searchResultTempAry[$service_ctr] = $searchResultArys ; 
                                if($searchResultArys['idCourierProvider']>0)
                                {
                                    $fCustomerCurrencyExchangeRate = $searchResultArys['fExchangeRate']; 
                                }
                                else
                                {
                                    $fCustomerCurrencyExchangeRate = $searchResultArys['fUsdValue'];
                                }

                                $idTransportMode = $data['idTransportMode'][$szUniqueServiceID];
                                $idManualFeeCurrency = $data['idManualFeeCurrency'][$szUniqueServiceID];
                                $fTotalManualFee = $data['fTotalManualFee'][$szUniqueServiceID];
                                $fTotalForwarderManualFee = $data['fTotalForwarderManualFee'][$szUniqueServiceID];

                                $searchResultTempAry[$service_ctr]['idTransportMode'] = $idTransportMode;
                                $searchResultTempAry[$service_ctr]['idManualFeeCurrency'] = $idManualFeeCurrency;
                                $searchResultTempAry[$service_ctr]['fTotalManualFee'] = $fTotalManualFee;
                                $searchResultTempAry[$service_ctr]['fExchangeRate'] = $fCustomerCurrencyExchangeRate;
                                $searchResultTempAry[$service_ctr]['fTotalForwarderManualFee'] = $fTotalForwarderManualFee;
                                $service_ctr++;
                                if($mode=='CREATE_BOOKING')
                                {
                                    break;
                                } 
                            }
                        }
                    } 
                    
                    $this->createQuotePricing($searchResultTempAry,$bookingDataAry,$szReferenceToken,$iSendQuickQuote,$iQuickQuoteTryITOutFlag); 
                    
                    if($mode=='CREATE_BOOKING')
                    { 
                        cPartner::$idPartnerCustomer = $idCustomer;  
                        
                        $kBooking = new cBooking();
                        $data = array();
                        $data['idBooking'] = $idBooking ;
                        $quotePricingDetailsAry = array();
                        $quotePricingDetailsAry = $kBooking->getAllBookingQuotesPricingDetails($data);
 
                        if(!empty($quotePricingDetailsAry))
                        {
                            foreach($quotePricingDetailsAry as $quotePricingDetailsArys)
                            {
                                $szServiceID = $quotePricingDetailsArys['szServiceID']; 
                                if($idTransportMode==__BOOKING_TRANSPORT_MODE_COURIER__)
                                {
                                    $kBooking->updateCourierServiceForwarderDetails($idBooking,$szServiceID,false,true,$quotePricingDetailsArys); 
                                }
                                else
                                {
                                    $kBooking->updateForwarderDetails($idBooking,$szServiceID,false,true,$quotePricingDetailsArys);
                                }
                            }
                        }  
                    }
                } 
                $this->szBookingRandomNum = $szBookingRandomNum;
                $this->idQuickQuoteBooking = $idBooking;
                return true;
            }
        }
    }
    
    function recalculateQuickQuotePricing($postSearchAry,$bPartnerApi=false,$bValidatePrice=false,$bQuickQuote=false)
    {
        if(!empty($postSearchAry))
        {
            $kBooking = new cBooking();
            if($bValidatePrice)
            {
                $idBooking = cPartner::$idBooking;
            }
            else
            {
                $idBooking = $postSearchAry['id'];
            } 
            $this->szBankTransferDueText = "";
            if($postSearchAry['iBookingType']==__BOOKING_TYPE_COURIER__)
            {  
                $kBooking->recalculateCourierPricing($idBooking,false,false,$bPartnerApi,false,false,false,$bValidatePrice,$bQuickQuote);  
                
                $this->szCourierCalculationLogString = $kBooking->szCargoLogString;
                
                $this->szBankTransferDueText = t($this->t_base_booking_confirmation.'messages/bank_transfer_text_courier');
                $this->szCustomerCurrencyVaildate = $kBooking->szCustomerCurrencyVaildate;
                $this->fTotalPriceCustomerCurrencyVaildate = $kBooking->fTotalPriceCustomerCurrencyVaildate;
                $this->fTotalVatCustomerCurrencyVaildate = $kBooking->fTotalVatCustomerCurrencyVaildate;
                    
                if($kBooking->iTolerablePrices ==1)
                {    
                    return "SUCCESS";
                }
                else if($this->bValidatePriceFlag==1 && $bPartnerApi && $bValidatePrice)
                {  
                    return "SUCCESS";
                }
                else if($kBooking->iDonotCalculateAgain==1)
                {
                    return "WARNING";
                } 
                else
                {
                    return "ERROR";
                }
            } 
            else
            { 
                $iLanguage = $postSearchAry['iBookingLanguage'];
                $kWHSSearch = new cWHSSearch();
                $resultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,'RECALCULATE_PRICING',false,true);
                 
                $szPartnerAPICalculationLogs .= "<br> <h3> Case: Searching for Parcel Shipment</h3> <br>";
                $szPartnerAPICalculationLogs .= $kWHSSearch->szCourierCalculationLogString;  
                
                $updateBookingAry = array(); 
                $updateBookingAry = $resultAry[0];
                
                /*
                * Recalculate lcl service prices 
                */  
                $kWhsSearch = new cWHSSearch();
                $iOverWeightTolerance = $kWhsSearch->getManageMentVariableByDescription('__OVER_WEIGHT_MEASURE_TOLERANCE_WITHOUT_PRICE_ADJUSTMENT__'); 

                $fTotalPriceCustomerCurrency = $postSearchAry['fDisplayPrice']; 
                
                $szCargoLogString = $szPartnerAPICalculationLogs;
                $szCargoLogString .= "<br><br> Booking Price: ".$fTotalPriceCustomerCurrency;
                $minTolerancePriceLevel = $fTotalPriceCustomerCurrency - ($fTotalPriceCustomerCurrency * $iOverWeightTolerance * .01) ;
                $maxTolerancePriceLevel = $fTotalPriceCustomerCurrency + ($fTotalPriceCustomerCurrency * $iOverWeightTolerance * .01) ;
                
                $szCargoLogString .= "<br> Booking Price(Min): ".$minTolerancePriceLevel;
                $szCargoLogString .= "<br> Booking Price(Max): ".$maxTolerancePriceLevel;
                
                $fShipmentUSDCost = $updateBookingAry['fDisplayPrice']; 
                $szCargoLogString .= "<br>Shipment cost (USD): ".number_format((float)$fShipmentUSDCost); 
                
                if($bValidatePrice)
                {
                    $this->szCustomerCurrencyVaildate = $updateBookingAry['szCurrency'];
                    $this->fTotalPriceCustomerCurrencyVaildate = round((float)$updateBookingAry['fDisplayPrice']);
                    $this->fTotalVatCustomerCurrencyVaildate = round((float)$updateBookingAry['fTotalVat'],2);
                } 
                
                
                $szBankTransferDueDateTime = $this->getBankTransferDueDateForLcl($updateBookingAry);
                $this->dtBankTransferDueDate = $dtBankTransferDueDate;
                $this->szBankTransferDueText = t($this->t_base_booking_confirmation.'messages/bank_transfer_text')." ".$szBankTransferDueDateTime;
                
                $this->szCourierCalculationLogString = $szCargoLogString;
                if($fShipmentUSDCost>=$minTolerancePriceLevel && $fShipmentUSDCost<=$maxTolerancePriceLevel)
                { 
                     
                    /*
                    * This means booking price is between tolerable level i.e +, - 3% of booking amount
                    */
                    return 'SUCCESS';
                }
                else if($bValidatePrice && !$bQuickQuote)
                {
                    /*
                    * In case when we call partner API method: validatePrice then we only check if price is lies in between tolerable prices or not  
                    */
                    if($idBooking>0)
                    {
                        $res_ary=array();
                        $updateBookingAry = $resultAry[0]; 

                        $this->updateNewPricesToBooking($updateBookingAry,$idBooking);
                    } 
                    return 'WARNING';
                }
                else if(!empty($updateBookingAry))
                { 
                    $res_ary=array();
                    $updateBookingAry = $resultAry[0]; 

                    $this->updateNewPricesToBooking($updateBookingAry,$idBooking);
                    return 'WARNING'; 
                } 
                else
                {
                    return "ERROR";
                }
            }   
        }
    } 
    
    function getBankTransferDueDateForLcl($updateBookingAry)
    {
        if(!empty($updateBookingAry))
        {
            $dtCutOffDate = format_date($updateBookingAry['dtCutOffDate']);
            $dtWhsCutOff = format_date($updateBookingAry['dtWhsCutOff']);

            if($updateBookingAry['idServiceType']==__SERVICE_TYPE_DTD__)
            {  
                $szBankTransferDueDateTime = strtotime($dtCutOffDate) - ($updateBookingAry['iBookingCutOffHours']*60*60); 

                $dtBankTransferDueDate = date('Y-m-d',$szBankTransferDueDateTime);
                $iMonth = date('m',$szBankTransferDueDateTime);

                $szBankTransferDueDateTime = date('j.',$szBankTransferDueDateTime); 
                $szMonthName = getMonthName($iLanguage,$iMonth);
                $szBankTransferDueDateTime .=" ".$szMonthName ;
            }
            else
            { 
                $szBankTransferDueDateTime = strtotime($dtWhsCutOff) - ($updateBookingAry['iBookingCutOffHours']*60*60); 
                $dtBankTransferDueDate = date('Y-m-d',$szBankTransferDueDateTime);
                $iMonth = date('m',$szBankTransferDueDateTime); 
                $szBankTransferDueDateTime = date('j.',$szBankTransferDueDateTime);  

                $szMonthName = getMonthName($iLanguage,$iMonth);
                $szBankTransferDueDateTime .=" ".$szMonthName ;
            }
            return $szBankTransferDueDateTime;
        }
    }
    function updateNewPricesToBooking($updateBookingAry,$idBooking)
    {
        if(!empty($updateBookingAry))
        {  
            $kBooking = new cBooking();
            $res_ary = updatePricingChanges($updateBookingAry); 
            if(!empty($res_ary))
            {
                foreach($res_ary as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            } 
            $update_query = rtrim($update_query,",");
            $kBooking->updateDraftBooking($update_query,$idBooking); 
        }
    }
    function createQuotePricing($searchResultAry,$postSearchAry,$szReferenceToken,$iSendQuickQuote=0,$iQuickQuoteTryITOutFlag=0)
    { 
        
        //print_r($searchResultAry);
        if(!empty($searchResultAry))
        {
            $kConfig = new cConfig();
            $kWhsSearch = new cWHSSearch();
            $kPartner = new cPartner();
            $kBooking = new cBooking();
            
            cPartner::$idGlobalPartner = $this->idDummyGlobalPartner;
            cPartner::$szGlobalToken = $szReferenceToken;
            cPartner::$szGlobalMode = "QUICK_QUOTE";
            
            $idBooking = $postSearchAry['id'];
            $idBookingFile = $postSearchAry['idFile'];
            
            $kBooking->deleteBookingQuoteHistory(array(),$idBookingFile);
            $kBooking->deletePricingByQuote(array(),$idBooking);
            
            $idOriginCountry = $postSearchAry['idOriginCountry'];
            $idDestinationCountry = $postSearchAry['idDestinationCountry'];
            
            $bDTDTradesFlag = false;
            $kCourierServices = new cCourierServices(); 
            if($kCourierServices->checkFromCountryToCountryExists($idOriginCountry,$idDestinationCountry,true))
            { 
                $bDTDTradesFlag = true;
            }
            
            $kPartner = new cPartner();
            $postSearchPartnerAry = $kPartner->getQuickQuoteRequestData('price','',$postSearchAry['szReferenceToken']);
            
            
          
            
            //print_r($searchResultAry);
            foreach($searchResultAry as $searchResults)
            {
                /*
                *  Following if will make sure we only add 1 quote for 1 product type
                */
                $vogaQuoteFound = true;
                $idTransportMode = $searchResults['idTransportMode'];
                $vogaQuoteAdded=true;
                $transportModeListAry = array();
                $transportModeListAry = $kConfig->getAllTransportMode($idTransportMode);

                $szTransportMode = $transportModeListAry[0]['szShortName']; 
                $idShipmentType = $searchResults['idShipmentType'];
                
                
                $parcelDetailsAry = array();
                $parcelDetailsAry = $kPartner->updateBookingCargDetails($idShipmentType);
                $szForwarderComment = '';
                if(!empty($parcelDetailsAry['szCargoTotalLine']))
                {
                    $szForwarderComment = $parcelDetailsAry['szCargoTotalLine'];
                }
                $searchResults['dtWhsCutOff_formated']='';
                if($searchResults['dtWhsCutOff_time']!='')
                {                    
                    $searchResults['dtWhsCutOff_formated']=date('Y-m-d H:i',$searchResults['dtWhsCutOff_time']);
                }
                $addVogaQuoteAry = array();
                $addVogaQuoteAry['idBooking'] = $postSearchAry['id'];
                $addVogaQuoteAry['idBookingFile'] = $postSearchAry['idFile'];
                $addVogaQuoteAry['idTransportMode'] = $idTransportMode;
                $addVogaQuoteAry['szTransportMode'] = $szTransportMode;
                $addVogaQuoteAry['idForwarder'] = $searchResults['idForwarder']; 
                $addVogaQuoteAry['iSendQuickQuote'] = $iSendQuickQuote;
                $addVogaQuoteAry['iQuickQuoteTryITOutFlag'] = $iQuickQuoteTryITOutFlag;
                //echo $szTransportMode."szTransportMode";
                if(strtolower($idTransportMode)=='4')
                {
                    $addVogaQuoteAry['dtCutOffDate'] = $searchResults['dtCutOffDate'];
                    $addVogaQuoteAry['dtAvailableDate'] = $searchResults['dtAvailableDate'];
                }
                else
                {
                    $addVogaQuoteAry['dtCutOffDate'] = $searchResults['dtCuttOffDate_formated'];
                    $addVogaQuoteAry['dtAvailableDate'] = $searchResults['dtAvailabeleDate_formated'];
                }
                $addVogaQuoteAry['dtWhsCutOff'] = $searchResults['dtWhsCutOff_formated'];
                $addVogaQuoteAry['dtWhsAvailable'] = $searchResults['dtWhsAvailable_formated'];
                
                if($searchResults['idCourierProvider']>0)
                {
                    $addVogaQuoteAry['szHandoverCity'] = $postSearchPartnerAry['szShipperCity'];
                }
                else
                {
                    if($bDTDTradesFlag)
                    {
                        $addVogaQuoteAry['szHandoverCity'] = $postSearchPartnerAry['szShipperCity'];
                    }
                    else
                    {
                        $railTransportAry = array();
                        $railTransportAry['idForwarder'] = $searchResults['idForwarder'];
                        $railTransportAry['idFromWarehouse'] = $searchResults['idWarehouseFrom'];
                        $railTransportAry['idToWarehouse'] = $searchResults['idWarehouseTo'];  
                        
                        $kServices = new cServices();
                        if($kServices->isRailTransportExists($railTransportAry))
                        {
                            $addVogaQuoteAry['szHandoverCity'] = $postSearchPartnerAry['szShipperCity']; 
                        }
                        else
                        {
                            if($searchResults['idServiceTerms']==__SERVICE_TERMS_EXW__)
                            {
                                $addVogaQuoteAry['szHandoverCity'] = $postSearchPartnerAry['szShipperCity'];
                            }
                            else if($searchResults['idServiceTerms']==__SERVICE_TERMS_EXW_WTD__  || $searchResults['idServiceTerms']==__SERVICE_TERMS_FOB__)
                            {
                                $addVogaQuoteAry['szHandoverCity'] = $searchResults['szFromWHSCity'];
                            }
                            else if($searchResults['idServiceTerms']==__SERVICE_TERMS_DAP__  || $searchResults['idServiceTerms']==__SERVICE_TERMS_DAP_NO_CC__)
                            {
                                $addVogaQuoteAry['szHandoverCity'] = $postSearchPartnerAry['szConsigneeCity'];
                            }
                            else if($searchResults['idServiceTerms']==__SERVICE_TERMS_CFR__)
                            {
                                $addVogaQuoteAry['szHandoverCity'] = $searchResults['szToWHSCity'];
                            }
                        }
                    }
                }
                
                if((int)$iQuickQuoteTryITOutFlag==1 && (int)$_SESSION['forwarder_admin_id']==0)
                {
                    $idForwarderUser = $_SESSION['forwarder_user_id']; 
                    $kForwarderContact = new cForwarderContact();
                    $kForwarderContact->load($idForwarderUser);
                    $addVogaQuoteAry['szForwarderContactEmail']= $kForwarderContact->szEmail;
                    
                    $szCustomerCareNumer=$kForwarderContact->szPhone;
                    $szForwarderEmail =$kForwarderContact->szEmail;
                    $replace_ary['szForwarderEmail'] = $szForwarderEmail;
                    
                    $replace_ary['szOfficePhone'] = $kForwarderContact->szPhone ;
                    $replace_ary['szForwarderEmail'] = $szFromEmail;

                    $kForwarder = new cForwarder();
                    $kForwarder->load($kForwarderContact->idForwarder);
                    $replace_ary['szForwarderDisplayName']=$kForwarder->szDisplayName;
                    $replace_ary['szForwarderTermsLink']=$kForwarder->szLink;
                    
                }
                else
                {
                    if((int)$_SESSION['forwarder_admin_id']>0)
                    {
                        $idAdmin = $_SESSION['forwarder_admin_id'] ;
                        $kAdmin = new cAdmin();
                        $kAdmin->getAdminDetails($idAdmin);
                        $addVogaQuoteAry['szForwarderContactEmail'] = $kAdmin->szEmail;
                        $replace_ary['szOfficePhone'] = $kAdmin->szPhone ;
                        
                        $kForwarder = new cForwarder();
                        $kForwarder->load($_SESSION['forwarder_id']);
                        $returnAry['szForwarderDisplayName']=$kForwarder->szDisplayName;
                        $returnAry['szForwarderTermsLink']=$kForwarder->szLink;
                    }
                    $replace_ary['szForwarderEmail'] = $kAdmin->szEmail;
                }
                $idVogaQuote = $this->addVogaAutomaticQuote($addVogaQuoteAry);  
                if($idVogaQuote>0)
                { 
                    $fCustomerCurrencyExchangeRate = $searchResults['fExchangeRate']; 
                    $fForwarderCurrencyExchangeRate = $searchResults['fForwarderCurrencyExchangeRate']; 
                    $fTotalPriceCustomerCurrency = round((float)$searchResults['fDisplayPrice']);  
                    
                   
                    
                    if($searchResults['dtAvailableDate']!='')
                    {
                        $dtAvailableDateArr=explode(" ",$searchResults['dtAvailableDate']);
                        $dtAvailableDateValue=$dtAvailableDateArr[0];
                        $dtAvailableTimeValue=$dtAvailableDateArr[1];
                        $dtAvailableDateValueArr=explode("/",$dtAvailableDateValue);
                        if(count($dtAvailableDateValueArr)==3)
                        {
                            $dtAvailableDate=$dtAvailableDateValueArr[2]."-".$dtAvailableDateValueArr[1]."-".$dtAvailableDateValueArr[0]." ".$dtAvailableTimeValue;
                        }
                        else
                        {
                            $dtAvailableDate=$searchResults['dtAvailableDate'];
                        }                        
                    }
                      
                    if($searchResults['iManualFeeApplicable']==1)
                    {
                        $idManualFeeCurrency = $searchResults['idManualFeeCurrency'];
                        $fManualFee = $searchResults['fTotalManualFee'];  
                        
                        if($idManualFeeCurrency==1)
                        {
                            $fTotalManualFeeUSD = $fManualFee;
                            $fManualFeeExchangeRates = 1;
                            $szManualFeeCurrency = 'USD';
                        }
                        else
                        {
                            $resultAry = $kWhsSearch->getCurrencyDetails($idManualFeeCurrency); 
                            $fManualFeeExchangeRates = $resultAry['fUsdValue']; 
                            $szManualFeeCurrency = $resultAry['szCurrency'];  

                            $fTotalManualFeeUSD = $fManualFee * $fManualFeeExchangeRates;
                        }  
                        /*
                        * Calculating Manual fee in customer currency
                        */
                        if($searchResults['idManualFeeCurrency']==$searchResults['idCurrency'])
                        {
                            $fTotalManualFeeCustomerCurrency = $fManualFee;
                        }
                        else if($searchResults['idCurrency']==1)
                        {
                            $fTotalManualFeeCustomerCurrency = $fTotalManualFeeUSD;
                        }
                        else if($fCustomerCurrencyExchangeRate>0)
                        {
                            $fTotalManualFeeCustomerCurrency = round((float)($fTotalManualFeeUSD/$fCustomerCurrencyExchangeRate));
                        }    
                        
                        /*
                        * If customer currency and Forwarder are not same then we apply currency mark-up on invoice
                        */
                        $fManualFeeCurrencyMarkup = 0;
                        if($searchResults['idForwarderCurrency']!=$searchResults['idCurrency'])
                        {
                            /*
                            * Adding currency mark-up on Manual Fee
                            */
                            $fManualFeeCurrencyMarkup = round((float)($fTotalManualFeeCustomerCurrency*.025),2);
                            $fTotalManualFeeCustomerCurrency = round((float)($fTotalManualFeeCustomerCurrency + $fManualFeeCurrencyMarkup)); 
                        } 
                        
                        $searchResults['fDisplayPrice'] = round((float)($fTotalPriceCustomerCurrency + $fTotalManualFeeCustomerCurrency));  
                        
                        $fVATPercentage = $searchResults['fVATPercentage'];  
                        $searchResults['fTotalVat'] = round((float)($searchResults['fDisplayPrice'] * $fVATPercentage * .01),2); 
                     
                        $fTotalHandlingFee = $fManualFee;
                        /*
                        * Calculating Manual fee in forwarder currency
                        */
                        if($searchResults['idManualFeeCurrency']==$searchResults['idForwarderCurrency'])
                        {
                            $fTotalManualFeeForwarderCurrency = $fManualFee;
                        }
                        else if($searchResults['idForwarderCurrency']==1)
                        {
                            $fTotalManualFeeForwarderCurrency = $fTotalManualFeeUSD;
                        }
                        else if($fForwarderCurrencyExchangeRate>0)
                        {
                            $fTotalManualFeeForwarderCurrency = round((float)($fTotalManualFeeUSD/$fForwarderCurrencyExchangeRate));
                        }  
                        //$searchResults['fForwarderCurrencyPrice_XE'] = round((float)($searchResults['fForwarderCurrencyPrice_XE'] + $fTotalManualFeeForwarderCurrency),2); 
                    }
                    
                    $fTotalForwarderManualFee=0.00;
                    $fTotalForwarderManualFeeUSD=0.00;
                    $fTotalForwarderManualFeeCurrencyMarkup=0.00;
                    $fTotalForwarderManualFeeCustomerCurrency=0.00;
                    if((float)$searchResults['fTotalForwarderManualFee']>0.00)
                    {
                        $fTotalForwarderManualFee=round((float)($searchResults['fTotalForwarderManualFee']));
                        $searchResults['fForwarderCurrencyPrice_XE'] = round((float)$searchResults['fForwarderCurrencyPrice_XE']+$fTotalForwarderManualFee);
                        $fTotalForwarderManualFeeUSD= (float)($fTotalForwarderManualFee*$searchResults['fForwarderCurrencyExchangeRate']);

                        if($searchResults['idForwarderCurrency']!=$searchResults['idCurrency'])
                        {
                            if($searchResults['idCurrency']==1)
                            {
                                $fTotalForwarderManualFeeCustomerCurrency=$fTotalForwarderManualFeeUSD;
                            }
                            else
                            {
                                $fTotalForwarderManualFeeCustomerCurrency=round((float)($fTotalForwarderManualFeeUSD/$fCustomerCurrencyExchangeRate));
                            }
                            $fTotalForwarderManualFeeCustomterCurrencyWithoutMarkup=round((float)($fTotalForwarderManualFeeCustomerCurrency));
                            $fTotalForwarderManualFeeCurrencyMarkup = round((float)($fTotalForwarderManualFeeCustomerCurrency*.025));
                            $fTotalForwarderManualFeeCustomerCurrency = round((float)($fTotalForwarderManualFeeCustomerCurrency + $fTotalForwarderManualFeeCurrencyMarkup)); 
                            $searchResults['fDisplayPrice'] = round((float)$searchResults['fDisplayPrice']+$fTotalForwarderManualFeeCustomerCurrency);

                        }
                        else
                        {
                            $fTotalForwarderManualFeeCustomterCurrencyWithoutMarkup=$fTotalForwarderManualFee;
                            $fTotalForwarderManualFeeCustomerCurrency=$fTotalForwarderManualFee;
                            $searchResults['fDisplayPrice'] = round((float)$searchResults['fDisplayPrice']+$fTotalForwarderManualFee);
                        }
                        
                        $fVATPercentage = $searchResults['fVATPercentage'];  
                        $searchResults['fTotalVat'] = round((float)($searchResults['fDisplayPrice'] * $fVATPercentage * .01),2); 
                     
                    }
                    
                    
                    if($searchResults['idForwarderCurrency']==$searchResults['idCurrency'])
                    {
                        /*
                        * If customer currency and forwarder currency are same then we round off both the prices
                        */
                        $searchResults['fForwarderCurrencyPrice_XE'] = round((float)$searchResults['fForwarderCurrencyPrice_XE']);
                        $searchResults['fDisplayPrice'] = round((float)$searchResults['fDisplayPrice']);
                    }
                    $addVogaQuotePricingAry = array();
                    $addVogaQuotePricingAry = $addVogaQuoteAry;
                    $addVogaQuotePricingAry['idBookingQuote'] = $idVogaQuote;
                    $addVogaQuotePricingAry['fTotalPriceForwarderCurrency'] = $searchResults['fForwarderCurrencyPrice_XE'];
                    $addVogaQuotePricingAry['idForwarderCurrency'] = $searchResults['idForwarderCurrency'];
                    $addVogaQuotePricingAry['szForwarderCurrency'] = $searchResults['szForwarderCurrency'];
                    $addVogaQuotePricingAry['fForwarderExchangeRate'] = $searchResults['fForwarderCurrencyExchangeRate']; 

                    $fVATPercentage = $searchResults['fVATPercentage'];
                    $fTotalPriceForwarderCurrency = round((float)$addVogaQuotePricingAry['fTotalPriceForwarderCurrency']);
                    $fTotalVatForwarder = round((float)($fTotalPriceForwarderCurrency * $fVATPercentage * .01),2);
                    $addVogaQuotePricingAry['fTotalVatForwarder'] = $fTotalVatForwarder;  
                    $addVogaQuotePricingAry['fVATPercentage'] = $fVATPercentage;

                    $addVogaQuotePricingAry['dtQuoteValidTo'] = date('Y-m-d',strtotime("+30 DAYS"));
                    $addVogaQuotePricingAry['szTransitTime'] = $searchResults['iDaysBetween'];
                    $addVogaQuotePricingAry['szForwarderComment'] = $szForwarderComment; 
                    
                    $fPriceExcludingManualFee = $searchResults['fDisplayPrice'] - $fTotalManualFeeCustomerCurrency ;
                    $fVatExcludingManualFee = round((float)($fPriceExcludingManualFee * $fVATPercentage * .01),2); 

                    $fReferalAmount_hidden = (($fPriceExcludingManualFee + $fVatExcludingManualFee) * $searchResults['fReferalPercentage'] * 0.01);
                    $fReferalAmount = round((float)$fReferalAmount_hidden,2); 
                    
                    /*
                    if($searchResults['iManualFeeApplicable']==1)
                    {
                        $fPriceExcludingManualFee = $searchResults['fDisplayPrice'] - $fTotalManualFeeCustomerCurrency ;
                        $fVatExcludingManualFee = round((float)($fPriceExcludingManualFee * $fVATPercentage * .01),2); 
                         
                        $fReferalAmount_hidden = (($fPriceExcludingManualFee + $fVatExcludingManualFee) * $searchResults['fReferalPercentage'] * 0.01);
                        $fReferalAmount = round((float)$fReferalAmount_hidden,2); 
                    }
                    else
                    {
                        $fReferalAmount_hidden = (($searchResults['fDisplayPrice'] + $searchResults['fTotalVat'] - $fTotalManualFeeCustomerCurrency) * $searchResults['fReferalPercentage'] * 0.01);
                        $fReferalAmount = round((float)$fReferalAmount_hidden,2); 
                    }  
                     * 
                     */
                    
                    $addVogaQuotePricingAry['fReferalPercentage'] = $searchResults['fReferalPercentage']; 
                    $addVogaQuotePricingAry['fReferalAmount'] = $fReferalAmount; 
                    $addVogaQuotePricingAry['fReferalAmount_hidden'] = $fReferalAmount_hidden; 
                    $addVogaQuotePricingAry['fTotalPriceCustomerCurrency'] = $searchResults['fDisplayPrice']; 
                    $addVogaQuotePricingAry['fQuotePriceCustomerCurrency'] = $searchResults['fDisplayPrice'];  
                    $addVogaQuotePricingAry['idCustomerCurrency'] = $searchResults['idCurrency'];
                    
                    $addVogaQuotePricingAry['idForwarderAccountCurrency'] = $addVogaQuotePricingAry['idForwarderCurrency'];
                    $addVogaQuotePricingAry['szForwarderAccountCurrency'] = $addVogaQuotePricingAry['szForwarderCurrency'];
                    $addVogaQuotePricingAry['fForwarderAccountExchangeRate'] = $addVogaQuotePricingAry['fForwarderExchangeRate'];
                    $addVogaQuotePricingAry['fTotalPriceForwarderAccountCurrency'] = $addVogaQuotePricingAry['fTotalPriceForwarderCurrency'];
                    $addVogaQuotePricingAry['fTotalVatForwarderAccountCurrency'] = $addVogaQuotePricingAry['fTotalVatForwarder'];  
                    $addVogaQuotePricingAry['fTotalVatCustomerCurrency'] = $searchResults['fTotalVat'];
                    $addVogaQuotePricingAry['szServiceID'] = $searchResults['unique_id']; 
                    $addVogaQuotePricingAry['dtAvailableDate'] = $dtAvailableDate; 
                    
                    if($iSendQuickQuote==1)
                    {
                        $addVogaQuotePricingAry['iQuoteSentToCustomer'] = 1;
                    }
                    else
                    {
                        $addVogaQuotePricingAry['iQuoteSentToCustomer'] = 0;
                    }
                    
                    $addVogaQuotePricingAry['iProductType'] = $searchResults['iProductType']; 
                    $addVogaQuotePricingAry['idCourierProvider'] = $searchResults['idCourierProvider'];
                    $addVogaQuotePricingAry['idCourierProviderProduct'] = $searchResults['idCourierProviderProduct'];
                    
                    if($searchResults['iHandlingFeeApplicable']==1)
                    {
                        $addVogaQuotePricingAry['iHandlingFeeApplicable'] = 1;
                        $addVogaQuotePricingAry['iManualFeeApplicable'] = 0; 
                        $addVogaQuotePricingAry['idHandlingCurrency'] = $searchResults['idHandlingCurrency'];
                        $addVogaQuotePricingAry['szHandlingCurrency'] = $searchResults['szHandlingCurrencyName'];
                        $addVogaQuotePricingAry['fHandlingCurrencyExchangeRate'] = $searchResults['fHandlingCurrencyExchangeRate'];
                        $addVogaQuotePricingAry['fHandlingFeePerBooking'] = $searchResults['fHandlingFeePerBooking'];

                        $addVogaQuotePricingAry['fHandlingMarkupPercentage'] = $searchResults['fHandlingMarkupPercentage'];
                        $addVogaQuotePricingAry['idHandlingMinMarkupCurrency'] = $searchResults['idHandlingMinMarkupCurrency'];
                        $addVogaQuotePricingAry['szHandlingMinMarkupCurrency'] = $searchResults['szHandlingMinMarkupCurrencyName'];
                        $addVogaQuotePricingAry['fHandlingMinMarkupCurrencyExchangeRate'] = $searchResults['fHandlingMinMarkupCurrencyExchangeRate'];
                        $addVogaQuotePricingAry['fHandlingMinMarkupPrice'] = $searchResults['fHandlingMinMarkupPrice'];  
                        $addVogaQuotePricingAry['fTotalHandlingFeeUSD'] = $searchResults['fTotalHandlingFeeUSD'];
                    }
                    else if($searchResults['iManualFeeApplicable']==1)
                    { 
                        $addVogaQuotePricingAry['iManualFeeApplicable'] = 1;
                        $addVogaQuotePricingAry['iHandlingFeeApplicable'] = 1;
                        $addVogaQuotePricingAry['idHandlingCurrency'] = $searchResults['idManualFeeCurrency'];
                        $addVogaQuotePricingAry['szHandlingCurrency'] = $szManualFeeCurrency;
                        $addVogaQuotePricingAry['fHandlingCurrencyExchangeRate'] = $fManualFeeExchangeRates;
                        $addVogaQuotePricingAry['fTotalHandlingFeeUSD'] = $fTotalManualFeeUSD;
                        $addVogaQuotePricingAry['fTotalHandlingFee'] = $fTotalHandlingFee; 
                    } 
                    else
                    {
                        $addVogaQuotePricingAry['iHandlingFeeApplicable'] = 0;
                        $addVogaQuotePricingAry['iManualFeeApplicable'] = 0;
                    }      
                    if((float)$searchResults['fTotalForwarderManualFee']>0.00)
                    {
                        $addVogaQuotePricingAry['fTotalForwarderManualFee'] = $searchResults['fTotalForwarderManualFee'];
                        $addVogaQuotePricingAry['fTotalForwarderManualFeeCustomterCurrency'] = $fTotalForwarderManualFeeCustomerCurrency;
                        $addVogaQuotePricingAry['fTotalForwarderManualFeeCustomterCurrencyWithoutMarkup'] = $fTotalForwarderManualFeeCustomterCurrencyWithoutMarkup;
                    }
                    else
                    {
                        $addVogaQuotePricingAry['fTotalForwarderManualFee'] = 0.00;
                        $addVogaQuotePricingAry['fTotalForwarderManualFeeCustomterCurrency'] = 0.00;
                        $addVogaQuotePricingAry['fTotalForwarderManualFeeCustomterCurrencyWithoutMarkup'] = 0.00;
                    }
                    $this->addVogaQuotePricing($addVogaQuotePricingAry);  
                }
                $ctr++;
            } 
        } 
    }
    
    function createSendQuoteEmail($postSearchAry,$mode='SEND_QUOTE',$iBookingLanguage=false,$szPaymentType=false,$iQuickQuoteFlag=false,$iQuickQuoteTryItOut=false)
    {
        if(!empty($postSearchAry))
        {
            $idBooking = $postSearchAry['id'];
            $idBookingFile = $postSearchAry['idFile'];
           
            $kConfig = new cConfig();
            $languageAry = array();
            $languageAry = $kConfig->getLanguageDetails(false,false,false,false,false,true);
            
            $paymentTypeAry = array();
            $paymentTypeAry = $kConfig->getAllPaymentTypes();
    
            if(!empty($languageAry))
            {
                foreach($languageAry as $languageArys)
                {
                    $iLanguage = $languageArys['id']; 
                    if($mode=='SEND_QUOTE')
                    {
                        $quoteEmailAry = array();
                        $quoteEmailAry = $this->buildSendQuoteEmail($postSearchAry,$iLanguage,$iQuickQuoteFlag,$iQuickQuoteTryItOut); 
                        
                        $returnAry['szEmailTemplateBody'][$iLanguage] = $quoteEmailAry['szEmailBody'];
                        $returnAry['szEmailTemplateSubject'][$iLanguage] = $quoteEmailAry['szEmailSubject'];
                    }
                    else if($iLanguage==$iBookingLanguage)
                    {
                        if(!empty($paymentTypeAry))
                        {
                            foreach($paymentTypeAry as $paymentTypeArys)
                            {
                                $szPaymentCode = $paymentTypeArys['szPaymentCode'];  
                                if($szPaymentType==$szPaymentCode)
                                {
                                    $quoteEmailAry = array();
                                    $quoteEmailAry = $this->buildMakeBookingEmail($postSearchAry,$szPaymentCode,$iLanguage,$iQuickQuoteTryItOut); 

                                    $returnAry['szEmailTemplateBody'][$szPaymentCode][$iLanguage] = $quoteEmailAry['szEmailBody'];
                                    $returnAry['szEmailTemplateSubject'][$szPaymentCode][$iLanguage] = $quoteEmailAry['szEmailSubject'];
                                } 
                            }
                        }
                    } 
                }
            }  
            return $returnAry;
        }
    }
    
    function buildMakeBookingEmail($postSearchAry,$szPaymentCode,$iLanguage,$iQuickQuoteTryItOut=false)
    {
        if(!empty($postSearchAry))
        {
            $kUser = new cUser();
            $kConfig = new cConfig();
            $kBooking = new cBooking();
            $kForwarder = new cForwarder();
             
            $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$iLanguage);
            
            $kWhsSearch = new cWHSSearch();  
            $iNumDaysPayBeforePickup = $kWhsSearch->getManageMentVariableByDescription('__NUMBER_OF_WEEKDAYS_PAYMENT_REQUIRED_BEFORE_REQUESTED_PICKUP__');
    
            $idBookingFile = $postSearchAry['idFile'];
            if($idBookingFile>0)
            { 
                $kBooking_new = new cBooking();
                $kBooking_new->loadFile($idBookingFile);
                $idFileOwner = $kBooking_new->idFileOwner;  
                $idCrmEmailLogs = $kBooking_new->idEmailLogs;
            } 
            if($postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_COURIER__)
            {
                $szBankTransferDueDateTime=date('Y-m-d',strtotime($postSearchAry['dtCutOff']));
                $szBankTransferDueDateTime=getBusinessDaysForCourier($szBankTransferDueDateTime,$iNumDaysPayBeforePickup.' DAY'); 
                
                $szBankTransferDueDateTime = strtotime($szBankTransferDueDateTime);
            }
            else if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__)
            {
                $szBankTransferDueDateTime = strtotime($postSearchAry['dtCutOff']) - ($postSearchAry['iBookingCutOffHours']*60*60);  
            }
            else
            {
                $szBankTransferDueDateTime = strtotime($postSearchAry['dtWhsCutOff']) - ($postSearchAry['iBookingCutOffHours']*60*60); 
            }  
            $iMonth = (int)date("m",$szBankTransferDueDateTime);
            $szMonthName = getMonthName($iLanguage,$iMonth,true);
            $iDate = (int)date('d.',$szBankTransferDueDateTime);
            $szBankTransferDueDateTime = $iDate.". ".$szMonthName;	
             
            $idServiceType = $postSearchAry['idServiceType'];
            $idServiceTerms = $postSearchAry['idServiceTerms'];
                      
            if($postSearchAry['iBookingType']==__BOOKING_TYPE_AUTOMATIC__)
            {
                if($idServiceTerms==__SERVICE_TERMS_FOB__)
                {
                    $szHandoverCity = $postSearchAry['szWarehouseFromCity'];
                }
                else if($idServiceTerms==__SERVICE_TERMS_CFR__)
                {
                    $szHandoverCity = $postSearchAry['szWarehouseToCity'];
                }
                else if($idServiceTerms==__SERVICE_TERMS_DAP__ || $idServiceTerms==__SERVICE_TERMS_DAP_NO_CC__)
                {
                    $szHandoverCity = $postSearchAry['szDestinationCity'];
                }
                else
                {
                    $szHandoverCity = $postSearchAry['szOriginCity']; 
                }
            }
            else
            {
                if($idServiceTerms==__SERVICE_TERMS_DAP__ || $idServiceTerms==__SERVICE_TERMS_DAP_NO_CC__)
                {
                    $szHandoverCity = $postSearchAry['szDestinationCity'];
                }
                else
                {
                    $szHandoverCity = $postSearchAry['szOriginCity']; 
                }
            } 
            $idCurrency = $postSearchAry['idCustomerCurrency'];
            $currencyDetailsAry = array();
            $currencyDetailsAry = $kBooking->loadCurrencyDetails($idCurrency);
 
            $idForwarder = $postSearchAry['idForwarder'] ;
            $customerServiceEmailAry = $kForwarder->getForwarderCustomerServiceEmail($idForwarder);
            $iBookingLanguage = $postSearchAry['iBookingLanguage'];
            
            //Send Bank Transfer recieved success email to user				
            $szOriginCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$postSearchAry['idOriginCountry'],false,$iBookingLanguage);
            $szDestinationCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$postSearchAry['idDestinationCountry'],false,$iBookingLanguage) ;

            $szOriginCountry = $szOriginCountryAry[$postSearchAry['idOriginCountry']]['szCountryName'] ;
            $szDestinationCountry = $szDestinationCountryAry[$postSearchAry['idDestinationCountry']]['szCountryName'] ;
            if($postSearchAry['iInsuranceIncluded']==1)
            {
                $fTotalBookingCost = ($postSearchAry['fTotalPriceCustomerCurrency'] + $postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency'] + $postSearchAry['fTotalVat']);
            }
            else
            {
                $fTotalBookingCost = ($postSearchAry['fTotalPriceCustomerCurrency'] + $postSearchAry['fTotalVat']);
            }
            $fTotalBookingCost = getPriceByLang($fTotalBookingCost,$iLanguage); 
            
            $szServiceDescriptionString = display_service_type_description($postSearchAry,$iLanguage,true); 
             
            $languageArr=$kConfig->getLanguageDetails('',$iLanguage); 
            
            $szHereText = $configLangArr[1]['szHereText_pt'];
            $szIncludingInsuranceText = $configLangArr[1]['szIncludingInsuranceText_pt'];
            $szTransportationInsurane = $configLangArr[1]['szTransportationInsurane_pt']; 
            $szCargoMeasure = $configLangArr[1]['szCbm'];
            $szServiceText =  $configLangArr[1]['szService']  ;
            $szPickupTimeDTText =  $configLangArr[1]['szPickupTimeDTText']  ;
            $szPickupTimePTWTText =  $configLangArr[1]['szPickupTimePTWTText']  ;
            $szDeliveryTimeTDText =  $configLangArr[1]['szDeliveryTimeTDText']  ;
            $szDeliveryTimeTPTWText =  $configLangArr[1]['szDeliveryTimeTPTWText']  ;
            
            $szServiceDescriptionString = $szServiceText.": ".$szServiceDescriptionString;
           
            if($postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_COURIER__)
            {
                $dtPickupDateDay=date("j",strtotime($postSearchAry['dtCutOff']));
                $dtPickupDateMonth=date("m",strtotime($postSearchAry['dtCutOff']));
                $dtPickupDateMonthName=getMonthName($iLanguage,$dtPickupDateMonth,true);
                $dtPickupDate=$dtPickupDateDay.". ".$dtPickupDateMonthName;
                $iPickHoursText=$szPickupTimeDTText.': '.$dtPickupDate;

                $dtAvailableDateDay=date("j",strtotime($postSearchAry['dtAvailable']));
                $dtAvailableDateMonth=date("m",strtotime($postSearchAry['dtAvailable']));
                $dtAvailableDateMonthName=getMonthName($iLanguage,$dtAvailableDateMonth,true);
                $dtAvailableDate=$dtAvailableDateDay.". ".$dtAvailableDateMonthName;
                $iDeliveryHoursText=$szDeliveryTimeTDText.': '.$dtAvailableDate;
            }
            else
            {
                if($postSearchAry['idServiceType'] == __SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType'] == __SERVICE_TYPE_DTW__ || $postSearchAry['idServiceType'] == __SERVICE_TYPE_DTP__)
                {
                    $dtPickupDateDay=date("j",strtotime($postSearchAry['dtCutOff']));
                    $dtPickupDateMonth=date("m",strtotime($postSearchAry['dtCutOff']));
                    $dtPickupDateMonthName=getMonthName($iLanguage,$dtPickupDateMonth,true);
                    $dtPickupDate=$dtPickupDateDay.". ".$dtPickupDateMonthName;
                    $iPickHoursText=$szPickupTimeDTText.': '.$dtPickupDate;
                }
                else
                {
                    $dtPickupDateDay=date("j",strtotime($postSearchAry['dtWhsCutOff']));
                    $dtPickupDateMonth=date("m",strtotime($postSearchAry['dtWhsCutOff']));
                    $dtPickupDateMonthName=getMonthName($iLanguage,$dtPickupDateMonth,true);
                    $dtPickupDate=$dtPickupDateDay.". ".$dtPickupDateMonthName;
                    $iPickHoursText=$szPickupTimePTWTText.': '.$dtPickupDate;
                }
                
                
                if($postSearchAry['idServiceType'] == __SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType'] == __SERVICE_TYPE_WTD__ || $postSearchAry['idServiceType'] == __SERVICE_TYPE_PTD__)
                {
                    $dtAvailableDateDay=date("j",strtotime($postSearchAry['dtAvailable']));
                    $dtAvailableDateMonth=date("m",strtotime($postSearchAry['dtAvailable']));
                    $dtAvailableDateMonthName=getMonthName($iLanguage,$dtAvailableDateMonth,true);
                    $dtAvailableDate=$dtAvailableDateDay.". ".$dtAvailableDateMonthName;
                    $iDeliveryHoursText=$szDeliveryTimeTDText.': '.$dtAvailableDate;
                }
                else
                {
                    $dtAvailableDateDay=date("j",strtotime($postSearchAry['dtWhsAvailabe']));
                    $dtAvailableDateMonth=date("m",strtotime($postSearchAry['dtWhsAvailabe']));
                    $dtAvailableDateMonthName=getMonthName($iLanguage,$dtAvailableDateMonth,true);
                    $dtAvailableDate=$dtAvailableDateDay.". ".$dtAvailableDateMonthName;
                    $iDeliveryHoursText=$szDeliveryTimeTPTWText.': '.$dtAvailableDate;
                }
            }
            
            $szWeightMeasure = 'kg';
            
            if(!empty($languageArr) && $iLanguage!=__LANGUAGE_ID_ENGLISH__) //Danish
            {
                $szLanguageCode=  strtolower($languageArr[0]['szLanguageCode']);
                $szBaseUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".$szLanguageCode;
            }
            else
            { 
                $szBaseUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__;
            } 
            
            $szTermsLink = $szBaseUrl."/booking-terms/".$postSearchAry['szBookingRandomNum']."/";  
            $szTermsLinkStr = '<a href="'.$szTermsLink.'" target="__blank">'.$szHereText.'</a>';
            
            $kConfig_new = new cConfig();
            $kConfig_new->loadCountry($postSearchAry['idOriginCountry']);
            $szCountryStrUrl = $kConfig_new->szCountryISO ; 
            $kConfig_new->loadCountry($postSearchAry['idDestinationCountry']); 
            $szCountryStrUrl .= $kConfig_new->szCountryISO ; 
            
            $szPaymentLink = $szBaseUrl."/pay/".$postSearchAry['szBookingRandomNum']."/complete-".$szCountryStrUrl."/";
             
            if($szPaymentCode=='UNKNOWN')
            {
                $kWhsSearch = new cWHSSearch();
                $ShowPaypalOption=false;
                $iPaypalStatus = $kWhsSearch->getManageMentVariableByDescription('__PAYPAL_STATUS__'); 
                if($iPaypalStatus=='ENABLED')
                {
                    if($postSearchAry['szCustomerCountry']>0)
                    {
                        $idCustomerCountry = $postSearchAry['szCustomerCountry'];
                    }
                    else if($postSearchAry['iShipperConsignee']>0)
                    {
                        if($postSearchAry['iShipperConsignee']==1)
                        {
                            $idCustomerCountry = $postSearchAry['idOriginCountry'];
                        }
                        else
                        {
                            $idCustomerCountry = $postSearchAry['idDestinationCountry'];
                        }
                    }  
                    $kAdmin = new cAdmin();
                    if($this->isPaypalAvailableForCountry($idCustomerCountry))
                    {  
                        $ShowPaypalOption=true;
                    } 
                }  
                if($ShowPaypalOption)
                {
                    $szPaymentType = $configLangArr[1]['szPaymentTypeCCP_pt'];
                    $szPaymentText = $configLangArr[1]['szPaymentTextCCP_pt'];
                }
                else
                {
                    $szPaymentType = $configLangArr[1]['szPaymentTypeCC_pt'];
                    $szPaymentText = $configLangArr[1]['szPaymentTextCC_pt'];
                }
            }
            else if($szPaymentCode=='CREDIT_CARD')
            {
                $szPaymentText = $configLangArr[1]['szPaymentTextCC_pt'];
            }
            else if($szPaymentCode=='PAYPAL')
            {
                $szPaymentText = $configLangArr[1]['szPaymentTextPP_pt'];
            }
            $szPaymentLinkStr = '<a href="'.$szPaymentLink.'" target="__blank">'.strtoupper($szPaymentText).'</a>';
             
            $cargo_volume = format_volume($postSearchAry['fCargoVolume']);
            $cargo_weight = get_formated_cargo_measure((float)($postSearchAry['fCargoWeight']));
            $cargo_weight = number_format_custom($cargo_weight,$iLanguage);
            
            $szCargoSring = $cargo_volume." ".$szCargoMeasure.", ".$cargo_weight." ".$szWeightMeasure."szCargoDescription"; 
              
            /*
            *  $szTransportationInsuraneString = '';
                if($postSearchAry['iInsuranceIncluded']==1)
                {
                    $fTotalPriceCustomerCurrency = $postSearchAry['fTotalPriceCustomerCurrency'] + round($postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']) ;
                    $fTotalPriceCustomerCurrency = getPriceByLang($fTotalPriceCustomerCurrency,$iLanguage);
                    $szPriceString = $postSearchAry['szCurrency']." ".$fTotalPriceCustomerCurrency." (".$szIncludingInsuranceText." ".$postSearchAry['szCurrency']." ".number_format((float)$postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']).")";

                    $szValueOfGoods = $postSearchAry['szGoodsInsuranceCurrency']." ".getPriceByLang($postSearchAry['fValueOfGoods'],$iLanguage);
                    $szTransportationInsuraneString = str_replace('szValueOfGoods',$szValueOfGoods,$szTransportationInsurane);
                }
                else
                {
                    $fTotalPriceCustomerCurrency = $postSearchAry['fTotalPriceCustomerCurrency'] ;
                    $fTotalPriceCustomerCurrency = getPriceByLang($fTotalPriceCustomerCurrency,$iLanguage);
                    $szPriceString = $postSearchAry['szCurrency']." ".$fTotalPriceCustomerCurrency;
                }
            */
            
            $kConfig = new cConfig();
            $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$iLanguage);
            
             $szVatExcludingText = $configLangArr[1]['szVatExcludingText_pt'];
             $szVatTextExcluingTextWhenOneQuote = $configLangArr[1]['szVatTextExcluingTextWhenOneQuote_pt'];
            
            $fTotalPriceCustomerCurrency = $postSearchAry['fTotalPriceCustomerCurrency'] ;
            $fTotalPriceCustomerCurrency = getPriceByLang($fTotalPriceCustomerCurrency,$iLanguage);
            $szPriceString = $postSearchAry['szCurrency']." ".$fTotalPriceCustomerCurrency;
            
            $fTotalVAT = getPriceByLang($postSearchAry['fTotalVat'],$iLanguage,2);
            if($postSearchAry['iPrivateCustomer']==1)
            {
                $iDecimalPlaces = false;  
                if(__float($postSearchAry['fTotalVat']))
                {
                    $iDecimalPlaces = 2;
                } 
                $szQuotePriceStr =  $postSearchAry['szCurrency']." ".number_format_custom((float)($postSearchAry['fTotalPriceCustomerCurrency']+$postSearchAry['fTotalVat']),$iLanguage,$iDecimalPlaces)." all-in" ;
                $szVatExcludingText =  $szVatTextExcluingTextWhenOneQuote;
            }
            else
            {
                $szQuotePriceStr =  $postSearchAry['szCurrency']." ".number_format_custom((float)$postSearchAry['fTotalPriceCustomerCurrency'],$iBookingLanguage)." all-in" ;
            }
                    
            if($postSearchAry['fTotalVat']>0)
            {
                $szVatString = ", ".$szVatExcludingText." ".$postSearchAry['szCurrency']." ".$fTotalVAT; 
            }
            $dtExpactedDelivery = date('j.',strtotime($postSearchAry['dtAvailable'])); 
            $iMonth = date('m',strtotime($postSearchAry['dtAvailable']));
            $szMonthName = getMonthName($iLanguage,$iMonth,true);
            $dtExpactedDelivery .=" ".$szMonthName ; 
              
            $kWHSSearch = new cWHSSearch();
            $szCustomerCareNumer = $kWHSSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__',$iLanguage);
            
            if((int)$_SESSION['forwarder_admin_id']>0)
            {
                $idAdmin = $_SESSION['forwarder_admin_id'] ;
            }
            else
            {
                $idAdmin = $_SESSION['admin_id'] ;
            }
            $szForwarderEmail='';
            $replace_ary = array();  
            if($iQuickQuoteTryItOut && (int)$_SESSION['forwarder_admin_id']==0)
            {
                $idForwarderUser = $_SESSION['forwarder_user_id']; 
                $kForwarderContact = new cForwarderContact();
                $kForwarderContact->load($idForwarderUser);
              
                $replace_ary['szAdminName'] = $kForwarderContact->szFirstName." ".$kForwarderContact->szLastName ;
                $szCustomerCareNumer=$kForwarderContact->szPhone;
                $szForwarderEmail =$kForwarderContact->szEmail;
                $replace_ary['szForwarderEmail'] = $szForwarderEmail;
                
                $kForwarder = new cForwarder();
                $kForwarder->load($kForwarderContact->idForwarder);
                $replace_ary['szForwarderDisplayName']=$kForwarder->szDisplayName;
                $replace_ary['szForwarderTermsLink']=$kForwarder->szLink;
            }
            else
            {
                $replace_ary['szAdminName'] = $kAdmin->szFirstName." ".$kAdmin->szLastName ;
                $szCustomerCareNumer = $kAdmin->szPhone;
                $replace_ary['szForwarderEmail'] = $kAdmin->szEmail;
            }
            
            $kAdmin = new cAdmin();
            $kAdmin->getAdminDetails($idAdmin);
 
            $kConfig = new cConfig();
            $kConfig->loadCountry($kAdmin->idInternationalDialCode);
            $iInternationDialCode = $kConfig->iInternationDialCode;
            $szMobile = "+".$iInternationDialCode." ".$kAdmin->szPhone; 
             
             
            $replace_ary['szEmail'] = $postSearchAry['szEmail'];
            $replace_ary['szForwarderDispName'] = $postSearchAry['szForwarderDispName'];
            $replace_ary['szForwarderCustServiceEmail'] = $customerServiceEmailAry[0]; 
            //$replace_ary['szTotalAmount'] = $postSearchAry['szCurrency']." ".$fTotalBookingCost;
            $replace_ary['szLatestPaymentDate'] =  $szBankTransferDueDateTime ;

            $replace_ary['szBank'] = $currencyDetailsAry['szBankName'] ;
            $replace_ary['szSortCode'] = $currencyDetailsAry['szSortCode'] ;
            $replace_ary['szAccountNumber'] = $currencyDetailsAry['szAccountNumber'] ;
            $replace_ary['szSwift'] = $currencyDetailsAry['szSwiftNumber'] ;
            $replace_ary['szNameOnAccount'] = $currencyDetailsAry['szNameOnAccount'] ;
            $replace_ary['szSuuportEmail'] = __STORE_SUPPORT_EMAIL__;
            $replace_ary['szSupportPhone'] = $szCustomerCareNumer;  
            $replace_ary['szOriginCFSContact'] = $postSearchAry['szWarehouseFromContactPerson'];;
            $replace_ary['szOriginCFSEmail'] = $postSearchAry['szWarehouseFromEmail'];
            $replace_ary['szShipperCompany'] = $postSearchAry['szShipperCompanyName']; 
            $replace_ary['szOriginCountry'] = $szOriginCountry;
            $replace_ary['szDestinationCountry'] = $szDestinationCountry;
            $replace_ary['szForwarderDispName'] = $postSearchAry['szForwarderDispName']; 
            $replace_ary['szServiceString'] = $szServiceDescriptionString; 
            $replace_ary['szCargoSring'] = $szCargoSring; 
            $replace_ary['szPriceString'] = $szQuotePriceStr;  
            $replace_ary['szPriceString'] = $szQuotePriceStr;
            $replace_ary['szVatString'] = $szVatString;
            $replace_ary['dtExpectedDate'] = $dtExpactedDelivery; 
            $replace_ary['szTermsLink'] = $szTermsLinkStr;
            $replace_ary['iBookingLanguage'] = $iLanguage;
            $replace_ary['szPaymentType'] = $szPaymentType;
            $replace_ary['szPaymentLink'] = $szPaymentLinkStr;
            
            $replace_ary['dtExpectedPickupDate'] = $iPickHoursText;
            $replace_ary['dtExpectedDeliveryDate'] = $iDeliveryHoursText; 
            
            
            $replace_ary['szTitle'] = $kAdmin->szTitle ;
            $replace_ary['szOfficePhone'] = $szCustomerCareNumer ;
            $replace_ary['szMobile'] = $szMobile;
            $replace_ary['szWebSiteUrl'] = remove_http_from_url(__MAIN_SITE_HOME_PAGE_URL__);
            $replace_ary['szSupportPhone'] = $szCustomerCareNumer ; 
            $returnAry['szHandoverCity'] = $szHandoverCity ;
            
            if($iQuickQuoteTryItOut)
            {
                if($szPaymentCode=='CREDIT_CARD')
                {
                    $szEmailTemplate = '__QUICK_QUOTE_BOOKING_BY_FORWARDER_WITH_CREDIT_CARD_EMAIL__';
                }
                else if($szPaymentCode=='PAYPAL')
                {
                    $szEmailTemplate = '__QUICK_QUOTE_BOOKING_BY_FORWARDER_WITH_PAYPAL_EMAIL__';
                }
                else if($szPaymentCode=='BANK_TRANSFER')
                {
                    $szEmailTemplate = '__QUICK_QUOTE_BOOKING_BY_FORWARDER_WITH_BANK_TRANSFER_EMAIL__';
                }
                else
                {
                    $szEmailTemplate = '__QUICK_QUOTE_BOOKING_BY_FORWARDER_WITH_UNKNOWN_EMAIL__';
                }
            }
            else
            {
                if($szPaymentCode=='CREDIT_CARD')
                {
                    $szEmailTemplate = '__QUICK_QUOTE_BOOKING_WITH_CREDIT_CARD_EMAIL__';
                }
                else if($szPaymentCode=='PAYPAL')
                {
                    $szEmailTemplate = '__QUICK_QUOTE_BOOKING_WITH_PAYPAL_EMAIL__';
                }
                else if($szPaymentCode=='BANK_TRANSFER')
                {
                    $szEmailTemplate = '__QUICK_QUOTE_BOOKING_WITH_BANK_TRANSFER_EMAIL__';
                }
                else
                {
                    $szEmailTemplate = '__QUICK_QUOTE_BOOKING_WITH_UNKNOWN_EMAIL__';
                }
            }
            
            /*
             * Building Email body and Subject
             */
            $emailMessageAry = array(); 
            $emailMessageAry = createEmailMessage($szEmailTemplate, $replace_ary);

            //$breaksAry = array("<br />","<br>","<br/>");  
            //$szEmailBody = str_replace($breaksAry, PHP_EOL, $emailMessageAry['szEmailBody']);  
            //$szEmailBody = str_replace($breaksAry, PHP_EOL, $szEmailBody);  
            
            $szEmailBody = $emailMessageAry['szEmailBody'];
            $szEmailSubject = $emailMessageAry['szEmailSubject']; 
            
            if (!empty($replace_ary))
            {
                foreach ($replace_ary as $replace_key => $replace_value)
                {   
                    if(!empty($replace_value))
                    { 
                        $szQuoteEmailBody = str_replace($replace_key, $replace_value, $szEmailBody);
                        $szQuoteEmailSubject= str_replace($replace_key, $replace_value, $szEmailSubject); 
                    } 
                }
            } 
            
            if($idCrmEmailLogs>0)
            {
                $crmEmailAry = array();
                $crmEmailAry = $this->getReplyEmailText($idCrmEmailLogs,$iBookingLanguage);
                if(!empty($crmEmailAry['szEmailSubject']))
                {
                    $szQuoteEmailSubject = $crmEmailAry['szEmailSubject'];
                }
                if(!empty($crmEmailAry['szEmailBody']))
                {
                    $szQuoteEmailBody = $szQuoteEmailBody."<br><br>".$crmEmailAry['szEmailBody'];
                }
            }
            
            $ret_ary = array();
            $ret_ary['szEmailBody'] = $szQuoteEmailBody;
            $ret_ary['szEmailSubject'] = $szQuoteEmailSubject;
            return $ret_ary;
        }
    } 
   
    function buildSendQuoteEmail($bookingDataArr,$iLanguage,$iQuickQuoteFlag=false,$iQuickQuoteTryItOut=false)
    {
        if(!empty($bookingDataArr))
        { 
            $kUser = new cUser();
            $kConfig = new cConfig();
            $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$iLanguage);

            //print_r($configLangArr);
            $szOfferText = $configLangArr[1]['szOfferText_pt'] ;
            $szFromText =  $configLangArr[1]['szFromText_pt']  ;
            $szToText =  $configLangArr[1]['szToText_pt']  ;
            $szCargoText =  $configLangArr[1]['szCargoText_pt'] ;
            $szModeText =  $configLangArr[1]['szModeText_pt'] ;
            $szTransitTimeText =  $configLangArr[1]['szTransitTimeText_pt'];
            $szPriceText =  $configLangArr[1]['szPriceText_pt'];
            $OfferValidUntillText =  $configLangArr[1]['OfferValidUntillText_pt'];
            $szInsuranceText =  $configLangArr[1]['szInsuranceText_pt'];
            $szDaysText =  " ".$configLangArr[1]['szDaysText_pt'];
            $szNoVatOnBookingText =  $configLangArr[1]['szNoVatOnBookingText_pt'];
            $szVatExcludingText = $configLangArr[1]['szVatExcludingText_pt'];        
            $szOfferHeading =  $configLangArr[1]['szOfferHeading_pt'].":";
            $szMultipleOfferHeading = $configLangArr[1]['szMultipleOfferHeading_pt'].":";
            $szMultipleOfferHeading2 = $configLangArr[1]['szMultipleOfferHeading2_pt'].":";
            $szInsuranceValueUptoText = " ".$configLangArr[1]['szInsuranceValueUptoText_pt'];        
            $szVatTextExcluingTextWhenOneQuote = $configLangArr[1]['szVatTextExcluingTextWhenOneQuote_pt'];
            $szCargoMeasure = $configLangArr[1]['szCbm'];
            $szServiceText =  $configLangArr[1]['szService']  ;
            $szPickupTimeDTText =  $configLangArr[1]['szPickupTimeDTText']  ;
            $szPickupTimePTWTText =  $configLangArr[1]['szPickupTimePTWTText']  ;
            $szDeliveryTimeTDText =  $configLangArr[1]['szDeliveryTimeTDText']  ;
            $szDeliveryTimeTPTWText =  $configLangArr[1]['szDeliveryTimeTPTWText']  ;
            $szHandoverCityText =  $configLangArr[1]['szHandoverCityText']  ;
            
            
            
            $kConfig = new cConfig();
            $kBooking_new = new cBooking();
            $iBookingLanguage = $iLanguage; 
            
            //$szServiceDescriptionString = display_service_type_description($bookingDataArr,$iBookingLanguage,true);
            
            $transportModeListAry = array();
            $transportModeListAry = $kConfig->getAllTransportMode(false,$iBookingLanguage,true,true);
    
            
            $idBooking = $bookingDataArr['id'];
            $idBookingFile = $bookingDataArr['idFile']; 
            $fCustomerExchangeRate = $bookingDataArr['fCustomerExchangeRate'];
            $szCustomerCurrency = $bookingDataArr['szCustomerCurrency'];
            $szBookingRandomKey = $bookingDataArr['szBookingRandomNum'];
            $idServiceType = $bookingDataArr['idServiceType'];
            $idServiceTerms = $bookingDataArr['idServiceType']; 
             
            if($bookingDataArr['iBookingType']==__BOOKING_TYPE_AUTOMATIC__)
            {
                if($idServiceTerms==__SERVICE_TERMS_FOB__)
                {
                    $szHandoverCity = $bookingDataArr['szWarehouseFromCity'];
                }
                else if($idServiceTerms==__SERVICE_TERMS_CFR__)
                {
                    $szHandoverCity = $bookingDataArr['szWarehouseToCity'];
                }
                else if($idServiceTerms==__SERVICE_TERMS_DAP__ || $idServiceTerms==__SERVICE_TERMS_DAP_NO_CC__)
                {
                    $szHandoverCity = $bookingDataArr['szDestinationCity'];
                }
                else
                {
                    $szHandoverCity = $bookingDataArr['szOriginCity']; 
                }
            }
            else
            {
                if($idServiceTerms==__SERVICE_TERMS_DAP__ || $idServiceTerms==__SERVICE_TERMS_DAP_NO_CC__)
                {
                    $szHandoverCity = $postSearchAry['szDestinationCity'];
                }
                else
                {
                    $szHandoverCity = $postSearchAry['szOriginCity']; 
                }
            }
            
            $idCustomer = $bookingDataArr['idUser'];
            if($idCustomer>0)
            {
                $kUser->getUserDetails($idCustomer);
                $iPrivateCustomer = $kUser->iPrivate;
            }  
            else
            {
                $iPrivateCustomer = $bookingDataArr['iPrivateCustomer'];
            }
            $languageArr=$kConfig->getLanguageDetails('',$iLanguage);
            
            if(!empty($languageArr))
            {
                $szCbmText=  strtolower($languageArr[0]['szDimensionUnit']);
            }
            else
            {
                $szCbmText= $szCargoMeasure;
            }
            
            if($bookingDataArr['idTransportMode']==4)//courier 
            { 
                $szVolWeight = format_volume($bookingDataArr['fCargoVolume'],$iBookingLanguage).$szCbmText.", ".number_format_custom((float)$bookingDataArr['fCargoWeight'],$iBookingLanguage)."kg,  ".number_format_custom((int)$bookingDataArr['iNumColli'],$iBookingLanguage)."col";
            }
            else
            {
                $szVolWeight = format_volume($bookingDataArr['fCargoVolume'],$iBookingLanguage).$szCbmText.", ".number_format_custom((float)$bookingDataArr['fCargoWeight'],$iBookingLanguage)."kg ";
            }  
            
            $szVolWeight = trim($szVolWeight);
            $returnAry = array();
            $returnAry['szFromPostcode'] = $bookingDataArr['szShipperPostCode'];
            $returnAry['szFromCity'] = $bookingDataArr['szShipperCity'];
            $returnAry['szFromCountry'] = $bookingDataArr['szShipperCountry'];
            $returnAry['szToPostcode'] = $bookingDataArr['szConsigneePostCode'];
            $returnAry['szToCity'] = $bookingDataArr['szConsigneeCity'];
            $returnAry['szToCountry'] = $bookingDataArr['szConsigneeCountry'];
            
            if(!empty($bookingDataArr['szCargoDescription']))
            {
                //$returnAry['szCargoDescription'] = ", ".$bookingDataArr['szCargoDescription'];
            }
            
            $returnAry['szVolume'] = format_volume($bookingDataArr['fCargoVolume'],$iBookingLanguage).$szCbmText;
            $returnAry['szWeight'] = number_format_custom((float)$bookingDataArr['fCargoWeight'],$iBookingLanguage)."kg"; 
            $returnAry['szEmail'] = $bookingDataArr['szEmail']; 
            $returnAry['iBookingLanguage'] = $iBookingLanguage;  
            $returnAry['szOriginCity'] = $bookingDataArr['szShipperCity'];
            $returnAry['szDestinationCity'] = $bookingDataArr['szConsigneeCity'];
            $returnAry['zCargoVolWeidghtColli'] = $szVolWeight."szCargoDescription";
            $returnAry['szHandoverCity'] = $szHandoverCity ;
            
            if($idBookingFile>0)
            { 
                $kBooking_new->loadFile($idBookingFile);
                $idFileOwner = $kBooking_new->idFileOwner;  
                $idCrmEmailLogs = $kBooking_new->idEmailLogs;
            }  
            $kWHSSearch = new cWHSSearch();
            $szCustomerCareNumer = $kWHSSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__',$iLanguage);
            
            if((int)$_SESSION['forwarder_admin_id']>0)
            {
                $idAdmin = $_SESSION['forwarder_admin_id'] ;
            }
            else
            {
                $idAdmin = $_SESSION['admin_id'] ;
            }
            
            $kAdmin = new cAdmin();
            $kAdmin->getAdminDetails($idAdmin);
 
            $kConfig = new cConfig();
            $kConfig->loadCountry($kAdmin->idInternationalDialCode);
            $iInternationDialCode = $kConfig->iInternationDialCode;
            $szMobile = "+".$iInternationDialCode." ".$kAdmin->szPhone; 
            
            if($iQuickQuoteTryItOut && (int)$_SESSION['forwarder_admin_id']==0)
            {
               
                $idForwarderUser = $_SESSION['forwarder_user_id']; 
                $kForwarderContact = new cForwarderContact();
                $kForwarderContact->load($idForwarderUser);
                $szFromEmail = $kForwarderContact->szEmail;
                $szFromUserName = $kForwarderContact->szFirstName." ".$kForwarderContact->szLastName ;
                $returnAry['szAdminName'] = $kForwarderContact->szFirstName." ".$kForwarderContact->szLastName ;               
                $returnAry['szOfficePhone'] = $kForwarderContact->szPhone ;
                $returnAry['szMobile'] = $kForwarderContact->szMobile;
                $returnAry['szWebSiteUrl'] = remove_http_from_url(__MAIN_SITE_HOME_PAGE_URL__);
                $returnAry['szForwarderEmail'] = $szFromEmail;
                
                $kForwarder = new cForwarder();
                $kForwarder->load($kForwarderContact->idForwarder);
                $returnAry['szForwarderDisplayName']=$kForwarder->szDisplayName;
                $returnAry['szForwarderTermsLink']=$kForwarder->szLink;
            }
            else
            {
                $returnAry['szAdminName'] = $kAdmin->szFirstName." ".$kAdmin->szLastName ;
                $returnAry['szTitle'] = $kAdmin->szTitle ;
                $returnAry['szOfficePhone'] = $kAdmin->szPhone  ;
                $returnAry['szMobile'] = $szMobile;
                $returnAry['szWebSiteUrl'] = remove_http_from_url(__MAIN_SITE_HOME_PAGE_URL__);
                $returnAry['szForwarderEmail'] = $kAdmin->szEmail;
                if((int)$_SESSION['forwarder_admin_id']>0)
                {
                    $kForwarder = new cForwarder();
                    $kForwarder->load($_SESSION['forwarder_id']);
                    $returnAry['szForwarderDisplayName']=$kForwarder->szDisplayName;
                    $returnAry['szForwarderTermsLink']=$kForwarder->szLink;
                }
            }
            

            $emailMessageAry = array(); 
            if($iQuickQuoteTryItOut)
            {
                $emailMessageAry = createEmailMessage('__CUSTOMER_SEND_QUICK_QUOTES_BY_FORWARDER__', $returnAry);
            }
            else
            {
                $emailMessageAry = createEmailMessage('__CUSTOMER_SEND_QUICK_QUOTES__', $returnAry);
            }
            $breaksAry = array("<br />","<br>","<br/>");  
            $szEmailBody = str_replace($breaksAry, "\r\n", $emailMessageAry['szEmailBody']); 

            $szEmailBody = str_replace($breaksAry, "\r\n", $szEmailBody);  
            $szEmailSubject = $emailMessageAry['szEmailSubject']; 
             
            $searchDataAry = array();
            $searchDataAry['iClosed']=1; 
            $quotesAry = array();  
            $quotesAry = $kBooking_new->getAllBookingQuotesForQuoteScreen($idBookingFile,$searchDataAry);
             
            $szQuotationBody = '';
            if(!empty($quotesAry))
            {
                $iQuoteCount = count($quotesAry);  
                if($iQuoteCount==1)
                {  
                    $szQuotationBody = $szOfferHeading."".PHP_EOL;  
                }
                else
                {  
                    //$bookingDataArr['szDestinationCountry']
                    $szQuotationBody = $szMultipleOfferHeading."".PHP_EOL."".PHP_EOL ; 
                    $szQuotationBody .= $szFromText.': szFromLocation'.PHP_EOL
                                .$szToText.': szToLocation'.PHP_EOL
                                //. ''.$szServiceText.': '.$szServiceDescriptionString.''.PHP_EOL
                                . ''.$szCargoText.': '.$szVolWeight.'szCargoDescription'.PHP_EOL;

                    $szQuotationBody .= PHP_EOL.$szMultipleOfferHeading2.PHP_EOL."".PHP_EOL ;  
                } 
                $counter = 1;
                $num_counter = 1;
                foreach($quotesAry as $quotesArys)
                { 
                    $idForwarder = $quotesArys['idForwarder']; 
                    $kForwarder = new cForwarder();
                    $kForwarder->load($idForwarder); 
                    $iProfitType = $kForwarder->iProfitType; 
                    $idBookingQuotePricing = $quotesArys['idQuotePriceDetails'];
                    
                    $szServiceDescriptionString='';
                    $szServiceDescriptionString = display_service_type_description($bookingDataArr,$iBookingLanguage,true,false,false,$quotesArys['szHandoverCity']);
                    
                    //print_r($quotesArys);
                    $szInsuranceText = "szInsuranceDetails_".$counter;
                    //$szInsuranceText = $this->updateInsuranceQuickQuote($quotesArys,$bookingDataArr); 
                    
                    $kConfig =new cConfig();
                    $langArr=$kConfig->getLanguageDetails('',$iBookingLanguage);
                    
                    $szClickOfferLink=$configLangArr[1]['szClickOfferLink_pt'];
                    
                    $kQuote = new cQuote();
                    $szControlPanelUrl = $kQuote->buildQuoteLink($idBookingQuotePricing,$iBookingLanguage);  
                    $szBookingQuoteLink = '<a href="'.$szControlPanelUrl.'">'.$szClickOfferLink.'</a>'; 
                    
                    if($quotesArys['iClosed']==1)
                    {    
                        if($fCustomerExchangeRate>0)
                        {
                            $fTotalPriceCustomerCurrency = round((float)$quotesArys['fTotalPriceCustomerCurrency']);
                            $fTotalVat = round((float)$quotesArys['fTotalVatCustomerCurrency'],2) ;
                        } 
                        if($iPrivateCustomer==1)
                        { 
                            $iDecimalPlaces = false;
                            if(__float($fTotalVat))
                            {
                                $iDecimalPlaces = 2;
                            }
                            $szQuotePriceStr =  $szCustomerCurrency." ".number_format_custom((float)($fTotalPriceCustomerCurrency + $fTotalVat),$iBookingLanguage,$iDecimalPlaces)." all-in" ;
                            $szVatExcludingText =  $szVatTextExcluingTextWhenOneQuote;
                        }
                        else
                        {
                            $szQuotePriceStr =  $szCustomerCurrency." ".number_format_custom((float)$fTotalPriceCustomerCurrency,$iBookingLanguage)." all-in" ;
                        }  
                        if($fTotalVat>0)
                        {
                            $szQuotePriceStr .=", ".$szVatExcludingText." ".$szCustomerCurrency." ".number_format_custom((float)$fTotalVat,$iBookingLanguage,2) ;
                        } 

                        $dtOfferValidDay = date('d',strtotime($quotesArys['dtQuoteValidTo']));

                        $dtOfferValidMonth = date('m',strtotime($quotesArys['dtQuoteValidTo']));
                        $dtOfferValidYear = date('Y',strtotime($quotesArys['dtQuoteValidTo']));
                        $szMonthName = getMonthName($iBookingLan,$dtOfferValidMonth,true);
                        $dtOfferValid=$dtOfferValidDay." ".$szMonthName." ".$dtOfferValidYear;
                        $szTransportMode = $transportModeListAry[$quotesArys['idTransportMode']]['szLongName'];
                        $iTransitHoursText=''; 
                        $iDeliveryHoursText='';
                        if($iQuickQuoteFlag)
                        {
                            /*$dtAvailableDay=date("j",strtotime(str_replace("/","-",trim($quotesArys['dtTransitDate'])))); 
                            $dtAvailableMonth=date("m",strtotime(str_replace("/","-",trim($quotesArys['dtTransitDate'])))); 
                            $dtAvailableMonthName=getMonthName($iLanguage,$dtAvailableMonth,true);
                            $dtAvailableDate=$dtAvailableDay.". ".$dtAvailableMonthName;

                            $iTransitHoursText=$szTransitTimeText.': '.$dtAvailableDate;*/
                            if($quotesArys['idTransportMode']==4)
                            {
                                $dtPickupDateDay=date("j",strtotime($quotesArys['dtCutOffDate']));
                                $dtPickupDateMonth=date("m",strtotime($quotesArys['dtCutOffDate']));
                                $dtPickupDateMonthName=getMonthName($iLanguage,$dtPickupDateMonth,true);
                                $dtPickupDate=$dtPickupDateDay.". ".$dtPickupDateMonthName;
                                $iTransitHoursText=$szPickupTimeDTText.': '.$dtPickupDate;
                                
                                $dtAvailableDateDay=date("j",strtotime($quotesArys['dtAvailableDate']));
                                $dtAvailableDateMonth=date("m",strtotime($quotesArys['dtAvailableDate']));
                                $dtAvailableDateMonthName=getMonthName($iLanguage,$dtAvailableDateMonth,true);
                                $dtAvailableDate=$dtAvailableDateDay.". ".$dtAvailableDateMonthName;
                                $iDeliveryHoursText=$szDeliveryTimeTDText.': '.$dtAvailableDate.''.PHP_EOL;
                            }
                            else
                            {
                                if($bookingDataArr['idServiceType'] == __SERVICE_TYPE_DTD__ || $bookingDataArr['idServiceType'] == __SERVICE_TYPE_DTW__ || $bookingDataArr['idServiceType'] == __SERVICE_TYPE_DTP__)
                                {
                                    $dtPickupDateDay=date("j",strtotime($quotesArys['dtCutOffDate']));
                                    $dtPickupDateMonth=date("m",strtotime($quotesArys['dtCutOffDate']));
                                    $dtPickupDateMonthName=getMonthName($iLanguage,$dtPickupDateMonth,true);
                                    $dtPickupDate=$dtPickupDateDay.". ".$dtPickupDateMonthName;
                                    $iTransitHoursText=$szPickupTimeDTText.': '.$dtPickupDate;
                                }
                                else
                                {
                                    $dtPickupDateDay=date("j",strtotime($quotesArys['dtWhsCutOff']));
                                    $dtPickupDateMonth=date("m",strtotime($quotesArys['dtWhsCutOff']));
                                    $dtPickupDateMonthName=getMonthName($iLanguage,$dtPickupDateMonth,true);
                                    $dtPickupDate=$dtPickupDateDay.". ".$dtPickupDateMonthName;
                                    $iTransitHoursText=$szPickupTimePTWTText.': '.$dtPickupDate;
                                }
                                
                                
                                if($bookingDataArr['idServiceType'] == __SERVICE_TYPE_DTD__ || $bookingDataArr['idServiceType'] == __SERVICE_TYPE_WTD__ || $bookingDataArr['idServiceType'] == __SERVICE_TYPE_PTD__)
                                {
                                    $dtAvailableDateDay=date("j",strtotime($quotesArys['dtAvailableDate']));
                                    $dtAvailableDateMonth=date("m",strtotime($quotesArys['dtAvailableDate']));
                                    $dtAvailableDateMonthName=getMonthName($iLanguage,$dtAvailableDateMonth,true);
                                    $dtAvailableDate=$dtAvailableDateDay.". ".$dtAvailableDateMonthName;
                                    $iDeliveryHoursText=$szDeliveryTimeTDText.': '.$dtAvailableDate.''.PHP_EOL;
                                }
                                else
                                {
                                    $dtAvailableDateDay=date("j",strtotime($quotesArys['dtWhsAvailable']));
                                    $dtAvailableDateMonth=date("m",strtotime($quotesArys['dtWhsAvailable']));
                                    $dtAvailableDateMonthName=getMonthName($iLanguage,$dtAvailableDateMonth,true);
                                    $dtAvailableDate=$dtAvailableDateDay.". ".$dtAvailableDateMonthName;
                                    $iDeliveryHoursText=$szDeliveryTimeTPTWText.': '.$dtAvailableDate.''.PHP_EOL;
                                }
                            }                                                       
                        }
                        else
                        {
                            $iTransitHoursText=$szTransitTimeText.': '.$quotesArys['iTransitHours'].''.$szDaysText;
                        }

                        if($iQuoteCount==1)
                        {  
                            //$bookingDataArr['szOriginCountry']
                            //$bookingDataArr['szDestinationCountry']
                            $szQuotationBody .= PHP_EOL.$szFromText.': szFromLocation'.PHP_EOL
                                . ''.$szToText.': szToLocation'.PHP_EOL                                
                                . ''.$szCargoText.': '.$szVolWeight.'szCargoDescription'.PHP_EOL
                                . ''.$szModeText.': '.$szTransportMode.''.PHP_EOL
                                . ''.$szServiceText.': '.$szServiceDescriptionString.''.PHP_EOL
                                . ''.$szHandoverCityText.': '.$quotesArys['szHandoverCity'].''.PHP_EOL
                                . ''.$iTransitHoursText.''.PHP_EOL
                                . ''.$iDeliveryHoursText    
                                . ''.$szPriceText.': '.$szQuotePriceStr.''.PHP_EOL.$szInsuranceText
                                . PHP_EOL.$szBookingQuoteLink.' 
                            '; 
                        }
                        else
                        { 
                            $szQuotationBody .= "<strong><u>".$counter.'. '.$szOfferText.' </u></strong>'.PHP_EOL.$szModeText.': '.$szTransportMode.''.PHP_EOL
                                . ''.$szServiceText.': '.$szServiceDescriptionString.''.PHP_EOL
                                . ''.$szHandoverCityText.': '.$quotesArys['szHandoverCity'].''.PHP_EOL    
                                . ''.$iTransitHoursText.PHP_EOL
                                . ''.$iDeliveryHoursText    
                                . ''.$szPriceText.': '.$szQuotePriceStr .PHP_EOL
                                . $szInsuranceText. PHP_EOL .$szBookingQuoteLink.''.PHP_EOL; 
                            
                            if($iQuoteCount!=$counter)
                            {
                                $szQuotationBody .= PHP_EOL;
                            }
                        } 
                        $counter++;
                        $num_counter++;
                    }
                } 
            } 
            $szQuotationBody = trim($szQuotationBody);
            $returnAry['szQuote'] = nl2br($szQuotationBody);
            
            $szQuoteEmailBody = $szEmailBody;
            $szQuoteEmailSubject = $szEmailSubject;

            if (!empty($returnAry))
            {
                foreach ($returnAry as $replace_key => $replace_value)
                { 
                    if(!empty($replace_value))
                    {
                        $szQuoteEmailBody = str_replace($replace_key, $replace_value, $szQuoteEmailBody);
                        $szQuoteEmailSubject= str_replace($replace_key, $replace_value, $szQuoteEmailSubject);
                    } 
                }
            } 
            
            if($idCrmEmailLogs>0)
            {
                $crmEmailAry = array();
                $crmEmailAry = $this->getReplyEmailText($idCrmEmailLogs,$iBookingLanguage);
                if(!empty($crmEmailAry['szEmailSubject']))
                {
                    $szQuoteEmailSubject = $crmEmailAry['szEmailSubject'];
                }
                if(!empty($crmEmailAry['szEmailBody']))
                {
                    $szQuoteEmailBody = $szQuoteEmailBody."<br/><br/>".$crmEmailAry['szEmailBody'];
                }
            } 
            $ret_ary = array();
            $ret_ary['szEmailBody'] = $szQuoteEmailBody;
            $ret_ary['szEmailSubject'] = $szQuoteEmailSubject;
            return $ret_ary;
        }
    }  
    
    function getReplyEmailText($idCrmEmailLogs,$iBookingLanguage=__LANGUAGE_ID_ENGLISH__,$ret_ary=false)
    {  
        if($idCrmEmailLogs>0)
        {
            $kSendEmail = new cSendEmail();
            $logEmailsArys = array();
            $logEmailsArys = $kSendEmail->getEmailLogDetails($idCrmEmailLogs);
            $szDraftFlag=false;
            if($logEmailsArys['szDraftEmailBody']!='')
            {
                $szDraftFlag=true;
                $logEmailsArys['szEmailBody']=$logEmailsArys['szDraftEmailBody'];
            }

            if($logEmailsArys['szDraftEmailSubject']!='')
            {
                $szDraftFlag=true;
                $logEmailsArys['szEmailSubject']=$logEmailsArys['szDraftEmailSubject'];
            }
            
            if(mb_detect_encoding($logEmailsArys['szEmailSubject']) == "UTF-8")
            {
                $szReminderSubject = utf8_decode($logEmailsArys['szEmailSubject']);
            }  
            else
            {
                $szReminderSubject = $logEmailsArys['szEmailSubject'];
            }
            
            if(mb_detect_encoding($logEmailsArys['szEmailBody']) == "UTF-8" && (int)$logEmailsArys['iAlreadyUtf8Encoded']==0)
            {
                $szOldEmailBody = utf8_decode($logEmailsArys['szEmailBody']);
            }  
            else
            {
                $szOldEmailBody = $logEmailsArys['szEmailBody'];
            } 
            $szOriginalEmailBody = $szOldEmailBody;
            
            if(__ENVIRONMENT__ == "LIVE")
            {
                if($logEmailsArys['id']<=64438)
                {
                    $szOldEmailBody = nl2br($szOldEmailBody);
                }
            }
            //&& $logEmailsArys['iIncomingEmail']!=1
            if(date('I')==1)
            { 
                //$logEmailsArys['dtSent'] = date('Y-m-d H:i:s',strtotime("+1 HOUR",strtotime($logEmailsArys['dtSent'])));  
            }
            $szCustomerTimeZone = $_SESSION['szCustomerTimeZoneGlobal']; 
            if(empty($szCustomerTimeZone))
            {
                $szCustomerTimeZone = getCustomerTimeZone();
            }
            $date = new DateTime($logEmailsArys['dtSent']); 
            $date->setTimezone(new DateTimeZone($szCustomerTimeZone)); 
                    
            $dtSentDate = $date->format('Y-m-d');
            $szCrmEmailTime = $date->format('H:i');
            
            $dtSentTime = strtotime($dtSentDate);
            $iMonth = date('m',$dtSentTime);

            $szSentDateTime = date('j.',$dtSentTime); 
            $szMonthName = getMonthName($iBookingLanguage,$iMonth);
            $szSentDateTime .=" ".$szMonthName." ".date('Y',$dtSentTime);
            
            if(!empty($logEmailsArys['szToUserName']))
            {  
                $szToEmailAddressStr = $logEmailsArys['szToUserName']." [".$logEmailsArys['szToAddress']."]";
            }
            else
            {
                $szToEmailAddressStr = $logEmailsArys['szToAddress'];
            }
            
            $kGmail = new cGmail();
            $szOldEmailBody = $kGmail->strip_html_tags($szOldEmailBody); 
            //$szOldEmailBody = $kGmail->custom_strip_tags($szOldEmailBody);
                              
            $kConfig = new cConfig();
            $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$mailBasicDetails['iBookingLanguage']);
                
            $szReplyEmailHeding = $configLangArr[1]['szReplyEmailHeding']; 
            
            $crmReplaceAry = array();
            $crmReplaceAry['szCrmEmailDate'] = $szSentDateTime;
            $crmReplaceAry['szCrmEmailTime'] = $szCrmEmailTime;
            $crmReplaceAry['szCustomerEmail'] = $szToEmailAddressStr;
               
            if($ret_ary)
            {
                return $crmReplaceAry;
            } 
            if (count($crmReplaceAry) > 0)
            {
                foreach ($crmReplaceAry as $replace_key => $replace_value)
                {
                    $szReplyEmailHeding = str_replace($replace_key, $replace_value, $szReplyEmailHeding); 
                }
            }   
            if(!empty($szOldEmailBody))
            {
                /*
                 * Removing Reference text if there is any in email body (Reference: 1609TSM014)
                 */
                $kSendEmail = new cSendEmail();
                $szOldEmailBody = $kSendEmail->removeReferenceText($szOldEmailBody,'REPLY_EMAIL');
                     
                //Add new Indets
                $szOldEmailBody = $this->addHtmlIndent($szOldEmailBody);  
            } 
             
            if($szDraftFlag)
            {
                $szEmailBody = $szOldEmailBody; 
            }
            else
            {
                $szEmailBody = $szReplyEmailHeding."<br><br>".$szOldEmailBody; 
            }
            $ret_ary = array();
            $ret_ary['szEmailSubject'] = trim($szReminderSubject);
            $ret_ary['szEmailBody'] = trim($szEmailBody); 
            $ret_ary['szDraftFlag'] = trim($szDraftFlag); 
            return $ret_ary; 
        }
    }
    function addHtmlIndent($szOldEmailBody)
    {
        if(!empty($szOldEmailBody))
        {
            $szIndentOpeningTag = "<div id='email_body_indent_span' style='margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex'>";
            $szIndentClosingTag = "</div>";
            
            /*
             * I there is already indent
             */
            $szOldEmailBody = str_ireplace($szIndentOpeningTag, "<div>", $szOldEmailBody);
            $szOldEmailBody = $szIndentOpeningTag."".$szOldEmailBody."".$szIndentClosingTag;
            return $szOldEmailBody;
        }
    }
    function addIndent($szOldEmailBody)
    {
        if(!empty($szOldEmailBody))
        {
            $oldEmailBodyarr = explode("\n", $szOldEmailBody);
            $szNewEmailBody = '';
            if(!empty($oldEmailBodyarr))
            {
                foreach($oldEmailBodyarr as $oldEmailBodyarrs)
                {
                    $oldEmailBodyarrs = trim($oldEmailBodyarrs);
                    if($oldEmailBodyarrs=='&nbsp;' || $oldEmailBodyarrs=='&nbsp;&nbsp;')
                    {
                        continue;
                    }
                    if(!empty($oldEmailBodyarrs))
                    {
                        $szNewEmailBody .= ">".$oldEmailBodyarrs.PHP_EOL;
                    }
                    else
                    {
                        $szNewEmailBody .= PHP_EOL;
                    } 
                }
            }  
            return $szNewEmailBody;  
        }
    }
    
    function sendQuickQuote($data,$szBookingType,$iQuickQuoteTryItOut=false)
    { 
       
        if(!empty($data))
        {
            $idBooking = (int)$data['idBooking']; 
            $kBooking = new cBooking();
            $kUser = new cUser();
            $postSearchAry = $kBooking->getBookingDetails($idBooking);
            $idBookingFile = $postSearchAry['idFile'];
            $idTransportMode = $postSearchAry['idTransportMode'];
            $iPrivateCustomer = $postSearchAry['iPrivateCustomer'];
            $data['idServiceType'] =$postSearchAry['idServiceType'];
            $data['idTransportMode'] =$postSearchAry['idTransportMode'];
            if($idBooking<=0)
            {
                $this->addError('szSpecialError','There is no Booking ID or File ID associated with this request. Please try again later.');
                return false;
            } 
            
            $this->validateBookingInformation($data,$szBookingType);
            if($this->error==true)
            { 
                return false;
            }    
            $kRegisterShipCon_new = new cRegisterShipCon();  
            $szCustomerEmail = $this->szEmail;
            if($this->idCustomer>0)
            {
                $idCustomer = $this->idCustomer; 
                $kUser->getUserDetails($idCustomer);

                $updateUserDetailsAry = array(); 
                $updateUserDetailsAry['szFirstName'] = $this->szFirstName ;
                $updateUserDetailsAry['szLastName'] = $this->szLastName ;   
                $updateUserDetailsAry['szCompanyName'] = $this->szCustomerCompanyName ;   
                $updateUserDetailsAry['szCurrency'] = $postSearchAry['idCurrency']; 
                $updateUserDetailsAry['szPhone'] = $this->szCustomerPhoneNumber; 
                $updateUserDetailsAry['idInternationalDialCode'] = $this->idCustomerDialCode ; 
                $updateUserDetailsAry['szEmail'] = $this->szEmail ;  
                $updateUserDetailsAry['iLanguage'] = $this->iBookingLanguage;  
                $updateUserDetailsAry['szAddress'] = $this->szCustomerAddress1 ;
                $updateUserDetailsAry['szPostCode'] = $this->szCustomerPostCode ;
                $updateUserDetailsAry['szCity'] = $this->szCustomerCity ; 
                $updateUserDetailsAry['idCountry'] = $this->szCustomerCountry ;
                $updateUserDetailsAry['iPrivate'] = $iPrivateCustomer; 

                if(!empty($updateUserDetailsAry))
                {
                    $update_user_query = '';
                    foreach($updateUserDetailsAry as $key=>$value)
                    {
                        $update_user_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                } 
                $update_user_query = rtrim($update_user_query,","); 

                if($kRegisterShipCon_new->updateUserDetailsByQuotes($update_user_query,$idCustomer))
                { 
                    //@TO DO
                }  
            }
            else
            { 
                $kRegisterShipCon = new cRegisterShipCon();
                $szPassword = mt_rand(0,99)."".$kRegisterShipCon->generateUniqueToken(8);

                //User is not loogedin then we create a new profile
                $addUserAry = array();
                $addUserAry['szFirstName'] = $this->szFirstName ;
                $addUserAry['szLastName'] = $this->szLastName ;
                $addUserAry['szEmail'] = $this->szEmail ;
                $addUserAry['szPassword'] = $szPassword ;
                $addUserAry['szConfirmPassword'] = $szPassword ;  
                $addUserAry['szCountry'] = $this->idCustomerDialCode ; 
                $addUserAry['szPhoneNumberUpdate'] = $this->szCustomerPhoneNumber ;
                $addUserAry['szCompanyName'] = $this->szCustomerCompanyName ; 
                $addUserAry['szCurrency'] = $postSearchAry['idCurrency'];  
                $addUserAry['iConfirmed'] = 0; 
                $addUserAry['iFirstTimePassword'] = 1;  
                $addUserAry['idInternationalDialCode'] = $this->idCustomerDialCode ; 
                $addUserAry['idCustomerOwner'] = $this->idCustomerOwner;
                $addUserAry['iLanguage'] = $this->iBookingLanguage;
                $addUserAry['szAddress1'] = $this->szCustomerAddress1 ;
                $addUserAry['szPostCode'] = $this->szCustomerPostCode ;
                $addUserAry['szCity'] = $this->szCustomerCity ;
                $addUserAry['szCountry'] = $this->szCustomerCountry ;
                $addUserAry['szCompanyRegNo'] = $data['szCustomerCompanyRegNo'];
                $addUserAry['iPrivate'] = $iPrivateCustomer;  
                 
                if($kUser->createAccount($addUserAry,'QUICK_QUOTE'))
                { 
                    $idCustomer = $kUser->id ;     
                } 
            }
            $kUser->getUserDetails($idCustomer); 
            
            /*
            * Updating Insurance details
            */  
            if($this->iInsuranceIncluded==1 || $this->iInsuranceIncluded==3) //1. Yes 3. Optional
            {  
                $bCheckInsuranceAvailability = false;
                if($szBookingType=='SEND_QUOTE_CONFIRM' || $szBookingType=='SEND_QUOTE')
                {
                    if($this->idGoodsInsuranceCurrency>0)
                    {
                        $bCheckInsuranceAvailability = true;
                    }
                }
                else
                {
                    $bCheckInsuranceAvailability = true;
                }
                
                if($bCheckInsuranceAvailability)
                {
                    $insuranceInputAry = array();
                    $insurancePriceAry = array();
                    $insuranceInputAry['iInsurance'] = 1;
                    $insuranceInputAry['idInsuranceCurrency'] = $this->idGoodsInsuranceCurrency;
                    $insuranceInputAry['fValueOfGoods'] = $this->fCargoValue;

                    if($kBooking->updateInsuranceDetails($insuranceInputAry,$postSearchAry))
                    {   
                       $szDeveloperNotes .= ">. Insurance data successfully updated. Going forward ".PHP_EOL;  
                    }
                    else if($kBooking->iInsuranceValidationFailed==1)
                    { 
                        $szInsuranceError .= ">. Inusrance data is not successfully updated into db ".PHP_EOL; 
                        $szDeveloperNotes .= $szInsuranceError;
                        $this->iInsuranceNotAvailable = 1;   
                        return false;
                    }
                    else if($kBooking->iInsuranceOtherErrors==1 || $kBooking->iInsuranceOtherErrors==2)
                    { 
                        $szInsuranceError = ">. Inusrance data is not successfully updated into db because of this error:\n\n ".print_R($kBooking->arErrorMessages,true)." ".PHP_EOL;
                        $szDeveloperNotes .= $szInsuranceError;
                        $this->iInsuranceNotAvailable = 1;   
                        return false;
                    }
                } 
            }
            
            /*
            * updating Booking File details
            */ 
            $fileLogsAry = array(); 

            $kBooking_new = new cBooking();  
            $kBooking = new cBooking();
             
            $dtResponseTime = $kBooking_new->getRealNow();
            if($szBookingType=='SEND_QUOTE_CONFIRM')
            {
                $szFileStatus = "S5";
                $szTaskStatus = "";
                $iStatus = 5;
                $iSearchType = 2;
            }
            else if($szBookingType=='CREATE_BOOKING_CONFIRM')
            {
                $szFileStatus = "S6";
                $szTaskStatus = ""; //T4
                $iStatus = 6;
                $iSearchType = 1;
            }
            $fileLogsAry['idUser'] = $idCustomer;
            $fileLogsAry['szTransportecaStatus'] = $szFileStatus;
            $fileLogsAry['szTransportecaTask'] = $szTaskStatus;
            $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
            $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime;
            $fileLogsAry['iStatus'] = $iStatus;
            $fileLogsAry['idFileOwner'] = $kUser->idCustomerOwner;
            $fileLogsAry['idCustomerOwner'] = $kUser->idCustomerOwner; 
            $fileLogsAry['iSearchType'] = $iSearchType;
            if($szBookingType=='SEND_QUOTE_CONFIRM')
            {
                $fileLogsAry['iQuoteSent'] = 1;
            }
             
            if(!empty($fileLogsAry))
            {
                $file_log_query = '';
                foreach($fileLogsAry as $key=>$value)
                {
                    $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                } 
                if($szBookingType=='SEND_CUSTOMER_QUOTES')
                {
                    $file_log_query .= " dtQuoteSend= NOW()," ;
                }
                $file_log_query = rtrim($file_log_query,","); 
                if($kBooking->updateBookingFileDetails($file_log_query,$idBookingFile))
                {
                    //@TO DO
                }
            }  
            
            /*
            *  Updating Shipper consignee Details
            */
            $iShipperConsignee = $this->iShipperConsignee;

            $kRegisterShipCon = new cRegisterShipCon();   
            $kRegisterShipCon->szShipperAddress = $this->szOriginAddress;
            $kRegisterShipCon->szShipperPostcode = $this->szOriginPostcode ;
            $kRegisterShipCon->szShipperCity = $this->szOriginCity ; 
            $kRegisterShipCon->szShipperState = ""; 
            $kRegisterShipCon->idShipperCountry = $this->idOriginCountry ;
            $kRegisterShipCon->idShipperDialCode = $this->idOriginCountry ;

            $kRegisterShipCon->szShipperAddress_pickup = $this->szOriginAddress;
            $kRegisterShipCon->szShipperPostcode_pickup = $this->szOriginPostcode ;
            $kRegisterShipCon->szShipperCity_pickup = $this->szOriginCity ; 
            $kRegisterShipCon->szShipperState_pickup = "";
            $kRegisterShipCon->idShipperCountry_pickup = $this->idOriginCountry ; 

            $kRegisterShipCon->iShipperConsignee = $iShipperConsignee;
            $kRegisterShipCon->szConsigneeAddress = $this->szDestinationAddress;
            $kRegisterShipCon->szConsigneePostcode = $this->szDestinationPostcode;
            $kRegisterShipCon->szConsigneeCity = $this->szDestinationCity;
            $kRegisterShipCon->szConsigneeState = "";
            $kRegisterShipCon->idConsigneeCountry =  $this->idDestinationCountry ; 
            $kRegisterShipCon->idConsigneeDialCode =  $this->idDestinationCountry ; 

            $kRegisterShipCon->szConsigneeAddress_pickup = $this->szDestinationAddress;
            $kRegisterShipCon->szConsigneePostcode_pickup = $this->szDestinationPostcode;
            $kRegisterShipCon->szConsigneeCity_pickup = $this->szDestinationCity;
            $kRegisterShipCon->szConsigneeState_pickup =  $this->idDestinationCountry ;
            $kRegisterShipCon->idConsigneeCountry_pickup =  $this->idDestinationCountry ; 

            $kRegisterShipCon->szShipperCompanyName = $this->szShipperCompanyName ;
            $kRegisterShipCon->szShipperFirstName = $this->szShipperFirstName ; 
            $kRegisterShipCon->szShipperLastName = $this->szShipperLastName ; 
            $kRegisterShipCon->szShipperPhone = $this->szShipperPhoneNumber ;
            $kRegisterShipCon->szShipperEmail = $this->szShipperEmail ;
            $kRegisterShipCon->idShipperDialCode = $this->idShipperDialCode ;    

            $kRegisterShipCon->szConsigneeCompanyName = $this->szConsigneeCompanyName ;
            $kRegisterShipCon->szConsigneeFirstName = $this->szConsigneeFirstName ; 
            $kRegisterShipCon->szConsigneeLastName = $this->szConsigneeLastName ; 
            $kRegisterShipCon->szConsigneePhone = $this->szConsigneePhoneNumber ;
            $kRegisterShipCon->szConsigneeEmail = $this->szConsigneeEmail ;
            $kRegisterShipCon->idConsigneeDialCode = $this->idConsigneeDialCode ; 

            $idShipperConsignee =  $kRegisterShipCon->addShipperConsignee_new($data,$idBooking,true);
            
            $res_ary = array();	    
            if($szBookingType=='CREATE_BOOKING_CONFIRM')
            {
                $idForwarder = $postSearchAry['idForwarder'];
                $kForwarder=new cForwarder();
                $kForwarder->load($idForwarder);
                $szCompanyRegistrationNum = $kForwarder->szCompanyRegistrationNum;
                
                $szReferenceType = 'BOOKING';
                $max_number_used_for_forwarder = $kForwarder->getMaximumUsedNumberByForwarder($idForwarder,false,$szReferenceType);
                $szBookingRef = $kBooking->generateBookingRef($max_number_used_for_forwarder,$kForwarder->szForwarderAlies);

                $bookingRefLogAry = array();
                $bookingRefLogAry['idForwarder'] = $idForwarder;
                $bookingRefLogAry['szBookingRef'] = $szBookingRef;	
                $bookingRefLogAry['szReferenceType'] = $szReferenceType;	
                $kForwarder->updateMaxNumBookingRef($bookingRefLogAry);

                
                //$szTransportecInvoice = $kBooking->generateInvoice($idForwarder,true);  
                $kAdmin = new cAdmin();
                $szTransportecInvoice = $kAdmin->generateInvoiceForPayment(true);
                
                $invoiceNumber = '';
                if($this->iInsuranceIncluded==1)
                { 
                    //$invoiceNumber = $kAdmin->generateInvoiceForPayment();
                    $invoiceNumber = $szTransportecInvoice;
                } 
                $kWarehouseSearch = new cWHSSearch();
                $dtBookingConfirmed = $kBooking->getRealNow();  
                $iNumDaysPayBeforePickup = $kWarehouseSearch->getManageMentVariableByDescription('__NUMBER_OF_WEEKDAYS_PAYMENT_REQUIRED_BEFORE_REQUESTED_PICKUP__'); 
             
                $resultAry = $kWarehouseSearch->getCurrencyDetails(20);	 
                $fDkkExchangeRate = 0;
                if($resultAry['fUsdValue']>0)
                {
                    $fDkkExchangeRate = $resultAry['fUsdValue'] ;						 
                }
                
                /*
                *  Booking confirmation fields will only be updated when we create actual booking into the system
                */
                $res_ary['idBookingStatus'] = __BOOKING_STATUS_CONFIRMED__;
                $res_ary['iAcceptedTerms'] = 0;
                $res_ary['iRemindToRate'] = 0;
                $res_ary['szBookingRef'] = $szBookingRef;
                $res_ary['iPaymentType'] = __TRANSPORTECA_PAYMENT_TYPE_3__;
                $res_ary['szInvoice'] = $szTransportecInvoice;
                $res_ary['dtBookingConfirmed'] = $dtBookingConfirmed;
                $res_ary['dtBooked'] = $dtBookingConfirmed;
                $res_ary['szPaymentStatus'] = 'Completed';
                $res_ary['iTransferConfirmed'] = 1;
                $res_ary['iNumDaysPayBeforePickup'] = $iNumDaysPayBeforePickup;
                $res_ary['szInsuranceInvoice'] = $invoiceNumber; 
                
                if($postSearchAry['iInsuranceIncluded']==1)
                {
                    $res_ary['fCustomerPaidAmount'] = $postSearchAry['fTotalPriceCustomerCurrency'] + $postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency'] + $postSearchAry['fTotalVat'];
                }
                else
                {
                    $res_ary['fCustomerPaidAmount'] = $postSearchAry['fTotalPriceCustomerCurrency'] + $postSearchAry['fTotalVat'];
                } 
                
                if($idTransportMode==__BOOKING_TRANSPORT_MODE_COURIER__)
                {
                    $res_ary['iCourierBooking'] = 1;
                    $res_ary['iBookingType'] = __BOOKING_TYPE_COURIER__;
                }
                else
                {
                    $res_ary['iBookingType'] = __BOOKING_TYPE_AUTOMATIC__;
                } 
                if($this->szPaymentType=='BANK_TRANSFER')
                {
                    $res_ary['iQuickQuote'] = __QUICK_QUOTE_CONFIRMED_BOOKING__;
                }
                else
                {
                    $res_ary['iQuickQuote'] = __QUICK_QUOTE_MAKE_BOOKING__;
                } 
                $res_ary['szQuickQuotePaymentType'] = $this->szPaymentType;
            }
            else
            {
                $res_ary['iQuickQuote'] = __QUICK_QUOTE_SEND_QUOTE__;
                $res_ary['iCourierBooking'] = 0; 
            } 
            $res_ary['idUser'] = $idCustomer ; 
            $res_ary['idShipperConsignee'] = $idShipperConsignee;  
            $res_ary['iShipperConsignee'] = $this->iShipperConsignee ;
            $res_ary['szFirstName'] = $this->szFirstName ;
            $res_ary['szLastName'] = $this->szLastName ;
            $res_ary['szCustomerCompanyName'] = $this->szCustomerCompanyName ;
            $res_ary['idCustomerDialCode'] = $this->idCustomerDialCode ;
            $res_ary['szCustomerPhoneNumber'] = $this->szCustomerPhoneNumber ; 
            $res_ary['szEmail'] = $this->szEmail ;  
            $res_ary['szCustomerAddress1'] = $this->szCustomerAddress1 ; 
            $res_ary['szCustomerPostCode'] = $this->szCustomerPostCode ;
            $res_ary['szCustomerCity'] = $this->szCustomerCity ;  
            $res_ary['szCustomerCountry'] = $this->szCustomerCountry ;   
            $res_ary['szOriginAddress'] = $this->szOriginAddress ; 
            $res_ary['szOriginPostCode'] = $this->szOriginPostcode ;
            $res_ary['szOriginCity'] = $this->szOriginCity ;  
            $res_ary['szDestinationAddress'] = $this->szDestinationAddress ;
            $res_ary['szDestinationPostCode'] = $this->szDestinationPostcode ;
            $res_ary['szDestinationCity'] = $this->szDestinationCity ;   
            $res_ary['iBookingLanguage'] = $this->iBookingLanguage ;     
            $res_ary['iSendTrackingUpdates'] = 1;   
            $res_ary['iNeverEdited'] = 0;
            $res_ary['iFromRequirementPage'] = 3;  
             
            if($this->iInsuranceIncluded==1) //Yes
            {
                $res_ary['iInsuranceMandatory'] = 1;
                $res_ary['iInsuranceIncluded'] = 1;
            }
            else if($this->iInsuranceIncluded==3) //Optional
            {
                $res_ary['iInsuranceMandatory'] = 0;
                $res_ary['iInsuranceIncluded'] = 0;
            }
            else
            {   //No
                $res_ary['iInsuranceMandatory'] = 0;
                $res_ary['iInsuranceIncluded'] = 0;
            } 
            $res_ary['iInsuranceChoice'] = $this->iInsuranceIncluded ;   
            $res_ary['idGoodsInsuranceCurrency'] = $this->idGoodsInsuranceCurrency ; 

            $kWhsSearch = new cWHSSearch(); 
            if($this->idGoodsInsuranceCurrency==1)
            { 
                $res_ary['szGoodsInsuranceCurrency'] = 'USD';
                $res_ary['fGoodsInsuranceExchangeRate'] = 1;
                $fValueOfGoodsUSD = $this->fCargoValue ;
            }
            else
            { 
                $resultAry = array();
                $resultAry = $kWhsSearch->getCurrencyDetails($this->idGoodsInsuranceCurrency);		 
                $res_ary['szGoodsInsuranceCurrency'] = $resultAry['szCurrency'];
                $res_ary['fGoodsInsuranceExchangeRate'] = $resultAry['fUsdValue']; 
                $fValueOfGoodsUSD = $this->fCargoValue * $resultAry['fUsdValue'] ;
            }  
            
            if(!empty($this->szCargoDescription))
            {
                $szCargoDescription = ", ".$this->szCargoDescription;
            }
             
            
            $res_ary['fValueOfGoods'] = $this->fCargoValue ; 
            $res_ary['fValueOfGoodsUSD'] = $fValueOfGoodsUSD;   
            $res_ary['szCargoDescription'] = utf8_encode($this->szCargoDescription) ; 
            $res_ary['iIncludeInsuranceWithZeroCargoValue'] = $this->iIncludeInsuranceWithZeroCargoValue ;  
            $res_ary['iUpdateInsuranceWithBlankValue'] = $this->iUpdateInsuranceWithBlankValue ; 
            $res_ary['iOptionalInsuranceWithZeroCargoValue'] = $this->iOptionalInsuranceWithZeroCargoValue;  
            if($szBookingType=='SEND_QUOTE_CONFIRM') //Insurance: 1.Yes, 3. Optional
            {
                /*
                 * updating Handover City variables 
                 */
                $kBooking_new = new cBooking();
                $searchDataAry = array();
                $searchDataAry['iClosed']=1; 
                $quotesAry = array();  
                $quotesAry = $kBooking_new->getAllBookingQuotesForQuoteScreen($idBookingFile,$searchDataAry);
                 $res_ary['szHandoverCity'] = $quotesAry[1]['szHandoverCity'];  
            }
 
              
            if(!empty($res_ary))
            {
                foreach($res_ary as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            } 
            $update_query = rtrim($update_query,",");   
            if($kBooking->updateDraftBooking($update_query,$idBooking))
            { 
                if($szBookingType=='CREATE_BOOKING_CONFIRM')
                {
                    /*
                    *  Adding Forwarder transaction data
                    */
                    $bookingDataArr = array();
                    $bookingDataArr = $kBooking->getExtendedBookingDetails($idBooking);
                    $bookingDataArr['iTransferConfirmed'] = 1; 
    
                    if($bookingDataArr['iCourierBooking']==1)
                    {
                        $kCourierServices=new cCourierServices();
                        $courierBookingArr=$kCourierServices->getCourierBookingData($bookingDataArr['id']);
                        if($courierBookingArr[0]['iCourierAgreementIncluded']==0)
                        {
                            $courLabelFee=$courierBookingArr[0]['fBookingLabelFeeRate'];
                            if($courierBookingArr[0]['idBookingLabelFeeCurrency']!=1)
                            {
                                $courLabelFee=$courierBookingArr[0]['fBookingLabelFeeRate']*$courierBookingArr[0]['fBookingLabelFeeROE'];
                            }
                            $bookingDataArr['fLabelFeeUSD']=$courLabelFee;
                        }
                    } 
                    $kBooking->addrForwarderTranscaction($bookingDataArr); 
                }
            }
            $this->idQuickQuoteBooking = $idBooking; 
            if($szBookingType=='SEND_QUOTE_CONFIRM' || $szBookingType=='CREATE_BOOKING_CONFIRM')
            {  
                $postSearchAry = $kBooking->getBookingDetails($idBooking);
                $iLanguage = $this->iBookingLanguage;
                
                $kConfig = new cConfig();
                $configLangArr = $kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$iLanguage);
                
                $szIncludingInsuranceText = $configLangArr[1]['szIncludingInsuranceText_pt'];
                $szTransportationInsurane = $configLangArr[1]['szTransportationInsurane_pt'];
                
                if($postSearchAry['iInsuranceIncluded']==1)
                {
                    $fTotalBookingCost = ($postSearchAry['fTotalPriceCustomerCurrency'] + $postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency'] + $postSearchAry['fTotalVat']);
                }
                else
                {
                    $fTotalBookingCost = ($postSearchAry['fTotalPriceCustomerCurrency'] + $postSearchAry['fTotalVat']);
                }
                $fTotalBookingCost = getPriceByLang($fTotalBookingCost,$iLanguage);  
                $szTransportationInsuraneString = '';
                $szPriceString = '';
                
                /*
                 * 
                    if($postSearchAry['iInsuranceIncluded']==1)
                    {
                        $fTotalPriceCustomerCurrency = $postSearchAry['fTotalPriceCustomerCurrency'] + round($postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']) ;
                        $fTotalPriceCustomerCurrency = getPriceByLang($fTotalPriceCustomerCurrency,$iLanguage);
                        $szPriceString = " (".$szIncludingInsuranceText." ".$postSearchAry['szCurrency']." ".number_format((float)$postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']).")";

                        $szValueOfGoods = $postSearchAry['szGoodsInsuranceCurrency']." ".getPriceByLang($postSearchAry['fValueOfGoods'],$iLanguage);
                        $szTransportationInsuraneString = str_replace('szValueOfGoods',$szValueOfGoods,$szTransportationInsurane);
                    } 
                 * 
                 */
                $szTransportationInsuraneString = '';
                if($postSearchAry['iInsuranceIncluded']==1)
                {
                    $fTotalPriceCustomerCurrency = $postSearchAry['fTotalPriceCustomerCurrency'] + round($postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']) ;
//                    $fTotalPriceCustomerCurrency = getPriceByLang($fTotalPriceCustomerCurrency,$iLanguage);
//                    $szPriceString = $postSearchAry['szCurrency']." ".$fTotalPriceCustomerCurrency." (".$szIncludingInsuranceText." ".$postSearchAry['szCurrency']." ".number_format((float)$postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']).")";

                    $fTotalInsurancePrice = $postSearchAry['szGoodsInsuranceCurrency']." ".getPriceByLang($postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency'],$iLanguage);
                    $szValueOfGoods = $postSearchAry['szGoodsInsuranceCurrency']." ".getPriceByLang($postSearchAry['fValueOfGoods'],$iLanguage);
                    
                    $szTransportationInsuraneString = str_replace('fTotalInsurancePrice',$fTotalInsurancePrice,$szTransportationInsurane);
                    $szTransportationInsuraneString = str_replace('szValueOfGoods',$szValueOfGoods,$szTransportationInsuraneString);
                    $szTransportationInsuraneString = str_replace('szInsuranceType','',$szTransportationInsuraneString);
                    $szTransportationInsuraneString = "<br />".$szTransportationInsuraneString."<br />";
                }
                else
                {
                    $fTotalPriceCustomerCurrency = $postSearchAry['fTotalPriceCustomerCurrency'] ;
                    //$fTotalPriceCustomerCurrency = getPriceByLang($fTotalPriceCustomerCurrency,$iLanguage);
                    //$szPriceString = $postSearchAry['szCurrency']." ".$fTotalPriceCustomerCurrency;
                }
                
                $returnAry = array();
                $returnAry['szFirstName'] = $postSearchAry['szFirstName'];
                $returnAry['szLastName'] = $postSearchAry['szLastName'];
                $returnAry['szInsuranceString'] = $szTransportationInsuraneString;
                //$returnAry['szPriceString'] = $szPriceString;
                $returnAry['szIncludeInsuranceWithPrice'] = "";
                $returnAry['szBookingRef'] = $postSearchAry['szBookingRef'];  
                $returnAry['szCargoDescription'] = $szCargoDescription;
                $returnAry['szTotalAmount'] = $postSearchAry['szCurrency']." ".$fTotalBookingCost;
                 
                $subject = $this->szEmailSubject ;
                $message = $this->szEmailBody;
                
                if($szBookingType=='SEND_QUOTE_CONFIRM') //Insurance: 1.Yes, 3. Optional
                {
                    /*
                     * updating Insurance variables 
                     */
                    $kBooking_new = new cBooking();
                    $searchDataAry = array();
                    $searchDataAry['iClosed']=1; 
                    $quotesAry = array();  
                    $quotesAry = $kBooking_new->getAllBookingQuotesForQuoteScreen($idBookingFile,$searchDataAry);
 
                    $szQuotationBody = '';
                    $qq_ctr = 1;
                    if(!empty($quotesAry))
                    {
                        foreach($quotesAry as $quotesArys)
                        { 
                            $szVariable = "szInsuranceDetails_".$qq_ctr; 
                            if($this->iInsuranceIncluded==1  && $this->fCargoValue>0)
                            {
                                $szInsuranceText = $this->updateInsuranceQuickQuote($quotesArys,$postSearchAry);
                            }
                            else if($this->iInsuranceIncluded==3 && $this->fCargoValue>0)
                            {
                                $szInsuranceText = $this->updateInsuranceQuickQuote($quotesArys,$postSearchAry,true);
                            } 
                            else
                            {
                                $szInsuranceText = "";
                            }
                            $message = str_replace($szVariable, $szInsuranceText, $message);
                            $qq_ctr++;
                        }
                    }
                } 
                
                if(!empty($returnAry))
                {
                    foreach($returnAry as $replace_key=>$replace_value)
                    {
                        $message = str_replace($replace_key, $replace_value, $message);
                        $subject = str_replace($replace_key, $replace_value, $subject);
                    }
                }  
                
                if($iQuickQuoteTryItOut && (int)$_SESSION['forwarder_admin_id']==0)
                {
                    $idForwarderUser = $_SESSION['forwarder_user_id']; 
                    $kForwarderContact = new cForwarderContact();
                    $kForwarderContact->load($idForwarderUser);
                    
                    $szFromUserName = $kForwarderContact->szFirstName." ".$kForwarderContact->szLastName ;
                    $replace_ary['szAdminName'] = $kForwarderContact->szFirstName." ".$kForwarderContact->szLastName ;
                    $szFromEmail = $kForwarderContact->szEmail;
                    $replace_ary['szOfficePhone'] = $kForwarderContact->szPhone ;
                    $replace_ary['szMobile'] = $kForwarderContact->szMobile;
                    $replace_ary['szWebSiteUrl'] = remove_http_from_url(__MAIN_SITE_HOME_PAGE_URL__);
                    $replace_ary['szForwarderEmail'] = $szFromEmail;

                    $kForwarder = new cForwarder();
                    $kForwarder->load($kForwarderContact->idForwarder);
                    $replace_ary['szForwarderDisplayName']=$kForwarder->szDisplayName;
                    $replace_ary['szForwarderTermsLink']=$kForwarder->szLink;
                    $szControlPanelUrl = $kForwarder->szControlPanelUrl;
                    $szControlPanelUrlArr=explode(".",$szControlPanelUrl);
                    $szFromEmail = $szFromEmail;
                    $reply_to=$kForwarderContact->szEmail;
                    $szForwarderLogo = $kForwarder->szLogo;
                }
                else
                {
                    if((int)$_SESSION['forwarder_admin_id']>0)
                    {
                        $idAdmin = $_SESSION['forwarder_admin_id'] ;
                        
                        $kForwarder = new cForwarder();
                        $kForwarder->load($_SESSION['forwarder_id']);
                        $replace_ary['szForwarderDisplayName']=$kForwarder->szDisplayName;
                        $replace_ary['szForwarderTermsLink']=$kForwarder->szLink;
                        $szControlPanelUrl = $kForwarder->szControlPanelUrl;
                        $szControlPanelUrlArr=explode(".",$szControlPanelUrl);
                        
                        $szForwarderLogo = $kForwarder->szLogo;
                        $kAdmin = new cAdmin();
                        $kAdmin->getAdminDetails($idAdmin);                        
                        $szFromUserName = $kAdmin->szFirstName." ".$kAdmin->szLastName ;
                        $replace_ary['szAdminName'] = $kAdmin->szFirstName." ".$kAdmin->szLastName ;
                        $reply_to=__STORE_SUPPORT_EMAIL__;
                        $replace_ary['szOfficePhone'] = $kAdmin->szPhone ;
                        $szFromEmail = $kAdmin->szEmail;
                    }
                    else
                    {
                        $idAdmin = $_SESSION['admin_id'] ;
                        $kAdmin = new cAdmin();
                        $kAdmin->getAdminDetails($idAdmin);
                        $szFromEmail = $kAdmin->szEmail;
                        $szFromUserName = $kAdmin->szFirstName." ".$kAdmin->szLastName ;
                        $replace_ary['szAdminName'] = $kAdmin->szFirstName." ".$kAdmin->szLastName ;
                        $reply_to=__STORE_SUPPORT_EMAIL__;
                        $replace_ary['szOfficePhone'] = $kAdmin->szPhone ;
                    }//$szFromEmail = $kAdmin->szEmail;
                    $replace_ary['szForwarderEmail'] = $szFromEmail;
                }
                
                if($iQuickQuoteTryItOut){
                    $messageHeader ='<p align="right"><img src="'.__BASE_STORE_INC_URL__.'/image.php?img='.$szForwarderLogo."&w=".__MAX_ALLOWED_FORWARDER_LOGO_EMAIL_WIDTH__."&h=".__MAX_ALLOWED_FORWARDER_LOGO_EMAIL_HEIGHT__.'" alt="'.$replace_ary['szForwarderDisplayName'].'" title="'.$replace_ary['szForwarderDisplayName'].'" border="0" /></p>';
                    $message = $messageHeader."".$message;
                }
                //adding Email logs
                $emailLogsAry = array();
                $emailLogsAry['idFile'] = $idBookingFile ;
                $emailLogsAry['idBooking'] = $idBooking ;
                $emailLogsAry['szEmail'] = $quotesArys['szForwarderContact'] ; 
                $emailLogsAry['szSubject'] = $subject ;
                $emailLogsAry['szEmailBody'] = $message ;  
                 
                
                $bAttachInvoice = false;
                if($szBookingType=='CREATE_BOOKING_CONFIRM' && ($this->szPaymentType=='BANK_TRANSFER' || $this->szPaymentType=='UNKNOWN'))
                {
                    /*
                    * When we create booking as Bank Transfer then we also send invoice pdf with email as an attachment. Like we do with a Bank Transfer booking from front end.
                    */
                    $bAttachInvoice = true;
                }
                
                sendQuatationEmail($szCustomerEmail,$szFromEmail,$subject,$message,$reply_to,$idCustomer,$idBooking,$szFromUserName,false,$bAttachInvoice); 
            }
            return true;
        }
    } 
    
    function updateInsuranceQuickQuote($quotePricingDetails,$postSearchAry,$bOptional=false)
    { 
        if(!empty($quotePricingDetails) && !empty($postSearchAry))
        {  
            $idBookingQuoteDetails = $quotePricingDetails['id'];
            $idBookingQuote = $quotePricingDetails['idQuote'];
            $fTotalPriceCustomerCurrency = $quotePricingDetails['fTotalPriceCustomerCurrency'];
            $szGoodsInsuranceCurrency = $postSearchAry['szGoodsInsuranceCurrency'];
            $fValueOfGoods = $postSearchAry['fValueOfGoods'];
            $fValueofGoodsUSD = $postSearchAry['fValueOfGoodsUSD']; 
            $fExchangeRate = $postSearchAry['fExchangeRate'];
            $szCustomerCurrency = $postSearchAry['szCurrency'];
            $iBookingLanguage = $postSearchAry['iBookingLanguage'];
              
            $bookingQuoteAry = array();
            $kBooking = new cBooking();
            $bookingQuoteAry = $kBooking->getAllBookingQuotesByFile(false,false,$idBookingQuote); 
            $bookingQuoteArys = $bookingQuoteAry['1']; 
            
            $idTransportMode = $bookingQuoteArys['idTransportMode'];
            
            $kConfig = new cConfig();
            $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$iBookingLanguage);
            
            $szInsuranceText =  $configLangArr[1]['szInsuranceText_pt'];
            $szTransportationInsurane = $configLangArr[1]['szTransportationInsurane_pt'];
            $szOptional = $configLangArr[1]['szOptional'];
            $szInsuranceValueUptoText = " ".$configLangArr[1]['szInsuranceValueUptoText_pt'];    

            $this->fTotalInsuranceCostForBookingCustomerCurrency = 0;
            $fTotalPriceUSD = $fTotalPriceCustomerCurrency * $fExchangeRate; 
              
            $insuranceInputAry = $postSearchAry ;
            $insuranceInputAry['idTransportMode'] = $idTransportMode ;
            $insuranceInputAry['fTotalBookingPriceUsd'] = $fTotalPriceUSD + $fValueofGoodsUSD;  
            
            $insuranceInputAry['fTotalPriceCustomerCurrency'] = $quotePricingDetails['fTotalPriceCustomerCurrency'];
            $insuranceInputAry['fTotalVat'] = $quotePricingDetails['fTotalVatCustomerCurrency'];
            $insuranceInputAry['fVATPercentage'] = $quotePricingDetails['fVATPercentage'];
            
            $idCustomerCurrency = $quotePricingDetails['idCustomerCurrency'];
            if($idCustomerCurrency==1)
            { 
                $szCustomerCurrency = 'USD';
                $fCustomerCurrencyExchangeRate = 1;   
            }
            else
            {
                $kWarehouseSearch = new cWHSSearch();
                $resultAry = $kWarehouseSearch->getCurrencyDetails($idCustomerCurrency);		
 
                $szCustomerCurrency = $resultAry['szCurrency'];
                $fCustomerCurrencyExchangeRate = $resultAry['fUsdValue']; 
            }
            $insuranceInputAry['idCurrency'] = $quotePricingDetails['idCustomerCurrency'];
            $insuranceInputAry['szCurrency'] = $szCustomerCurrency;
            $insuranceInputAry['fExchangeRate'] = $fCustomerCurrencyExchangeRate; 
            $insuranceInputAry['fValueOfGoods'] = $fValueofGoodsUSD;
            $insuranceInputAry['idInsuranceCurrency'] = 1; //USD
            
            $kInsurance = new cInsurance();
            if($kInsurance->getInsuranceDetailsFroBooking($insuranceInputAry,true))
            {  
                $iTransportationInsuranceAvailable = 1;  
                $fTotalInsuranceCostForBookingCustomerCurrency = round((float)$kInsurance->fTotalInsuranceCostForBookingCustomerCurrency) ;   
            }
            else
            {
               $fTotalInsuranceCostForBookingCustomerCurrency = 'Not available' ;
               $iTransportationInsuranceAvailable = 2;
            } 
            $szInsuranceType='';
            if($bOptional)
            {
                $szInsuranceType= " (". $szOptional.")";
            } 
            if($iTransportationInsuranceAvailable==1)
            { 
                
                $fTotalInsurancePrice = $szCustomerCurrency." ".getPriceByLang($fTotalInsuranceCostForBookingCustomerCurrency,$iBookingLanguage);
                $szValueOfGoods = $szGoodsInsuranceCurrency." ".getPriceByLang($fValueOfGoods,$iBookingLanguage);

                $szTransportationInsuraneString = str_replace('fTotalInsurancePrice',$fTotalInsurancePrice,$szTransportationInsurane);
                $szTransportationInsuraneString = str_replace('szValueOfGoods',$szValueOfGoods,$szTransportationInsuraneString);
                $szTransportationInsuraneString = str_replace('szInsuranceType',$szInsuranceType,$szTransportationInsuraneString);
                $szTransportationInsuraneString = $szTransportationInsuraneString;
                
                //$this->fTotalInsuranceCostForBookingCustomerCurrency = $fTotalInsuranceCostForBookingCustomerCurrency;
                //$szInsuranceDetailsString = $szInsuranceText.': '. " " . $szCustomerCurrency . " " .number_format_custom((float)$fTotalInsuranceCostForBookingCustomerCurrency,$iBookingLanguage) . $szInsuranceValueUptoText . " " . $szGoodsInsuranceCurrency . " " . number_format_custom($fValueOfGoods,$iBookingLanguage) ; 
                return $szTransportationInsuraneString."<br><br> ";
            }
            else
            {
                return "";
            } 
        }
    }
    
    function validateBookingInformation($data,$szBookingType)
    {
        if(!empty($data))
        {  
            $kUser = new cUser();
            /*
            * Validating Billing fields
            */ 
            if($szBookingType=='CREATE_BOOKING_CONFIRM')
            {
                $required_all_flag = true;
                $type="CREATE_BOOKING";
            }
            else
            {
                $required_all_flag = false;
                $type="SEND_QUOTE";
            }
            
            $this->set_szFirstName(trim(sanitize_all_html_input($data['szFirstName'])));
            $this->set_szLastName(trim(sanitize_all_html_input($data['szLastName'])));
            $this->set_szEmail(trim(sanitize_all_html_input($data['szEmail']))); 
            $this->set_idCustomerDialCode(trim(sanitize_all_html_input($data['idCustomerDialCode'])),$required_all_flag);
            $this->set_szCustomerPhoneNumber(trim(sanitize_all_html_input($data['szCustomerPhoneNumber'])),$required_all_flag);  
            $this->set_szCustomerAddress1(trim(sanitize_all_html_input($data['szCustomerAddress1'])),$required_all_flag);
            $this->set_szCustomerPostCode(trim(sanitize_all_html_input($data['szCustomerPostCode'])),$required_all_flag);				
            $this->set_szCustomerCity(trim(sanitize_all_html_input($data['szCustomerCity'])),$required_all_flag);
            $this->set_szCustomerCountry(trim(sanitize_all_html_input($data['szCustomerCountry'])),$required_all_flag);
            
            if($data['iPrivateCustomer']==1)
            {
                $this->szCustomerCompanyName = $this->szFirstName." ".$this->szLastName;
            }
            else
            {
                $this->set_szCustomerCompanyName(trim(sanitize_all_html_input($data['szCustomerCompanyName'])),$required_all_flag);
            }
            $this->set_iShipperConsignee(trim(sanitize_all_html_input($data['iShipperConsignee'])),$required_all_flag);
            $szCollection = 'Yes';
            $szDelivery = 'Yes';
			$szShipperPostcodeRequired="Yes";
			$szConsigneePostcodeRequired="Yes";
            if($szBookingType=='CREATE_BOOKING_CONFIRM'){
                $kAdmin = new cAdmin();
                $idShipperCountry=$data['idOriginCountry'];
                $shipperCountryArr=$kAdmin->countriesView($idShipperCountry,$idShipperCountry);
                
                
                if($data['idTransportMode']==4)
                {
                    $szCollection = 'Yes';
                    $szDelivery = 'Yes';
                }
                else
                {
                    if($data['idServiceType']==__SERVICE_TYPE_DTD__ || $data['idServiceType']==__SERVICE_TYPE_DTW__ || $data['idServiceType']==__SERVICE_TYPE_DTP__) // 1.DTD 2.DTD
                    {
                        $szCollection = 'Yes';
                    }
                    else
                    {
                        $szCollection = 'No';
                    } 
                    if($data['idServiceType']==__SERVICE_TYPE_DTD__ || $data['idServiceType']==__SERVICE_TYPE_WTD__ || $data['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD 2.DTD
                    {
                        $szDelivery = 'Yes';
                    }
                    else
                    {
                        $szDelivery = 'No';
                    }
                }

                if($szCollection=='Yes' && $shipperCountryArr['iUsePostcodes']==1)
                {
                    $szShipperPostcodeRequired="Yes";
                }
                else
                {
                    $szShipperPostcodeRequired="No";
                }

                $idConsigneeCountry==$data['idDestinationCountry'];
                $kAdmin = new cAdmin();
                $consigneeCountryArr=$kAdmin->countriesView($idConsigneeCountry,$idConsigneeCountry);
                if($szDelivery=='Yes' && $consigneeCountryArr['iUsePostcodes']==1)
                {
                    $szConsigneePostcodeRequired="Yes";
                }
                else
                {
                    $szConsigneePostcodeRequired="No";
                }
                
                
            }
            /*
            * Validating Shipper fields
            */
            $this->set_szShipperCompanyName(trim(sanitize_all_html_input($data['szShipperCompanyName'])),$required_all_flag);
            $this->set_szShipperFirstName(trim(sanitize_all_html_input($data['szShipperFirstName'])),$required_all_flag);
            $this->set_szShipperLastName(trim(sanitize_all_html_input($data['szShipperLastName'])),$required_all_flag);
            $this->set_szShipperEmail(trim(sanitize_all_html_input($data['szShipperEmail'])),$required_all_flag);				
            $this->set_szShipperPhone(trim(sanitize_all_html_input($data['szShipperPhoneNumber'])),$required_all_flag);	
            $this->set_idShipperDialCode(trim(sanitize_all_html_input($data['idShipperDialCode'])),$required_all_flag); 
            if($szCollection=='No'){
                $this->set_szOriginAddress(trim(sanitize_all_html_input($data['szOriginAddress'])),false);
            }else{
            $this->set_szOriginAddress(trim(sanitize_all_html_input($data['szOriginAddress'])),$required_all_flag);
            }
            if($szShipperPostcodeRequired=='No'){
               $this->set_szOriginPostcode(trim(sanitize_all_html_input($data['szOriginPostcode'])),false); 
            }else{
            $this->set_szOriginPostcode(trim(sanitize_all_html_input($data['szOriginPostcode'])),$required_all_flag);
            }
            $this->set_szOriginCity(trim(sanitize_all_html_input($data['szOriginCity'])),$required_all_flag); 
            $this->set_idOriginCountry(trim(sanitize_all_html_input($data['idOriginCountry'])),$required_all_flag);

            /*
            * Validating Consignee fields
            */
            $this->set_szConsigneeCompanyName(trim(sanitize_all_html_input($data['szConsigneeCompanyName'])),$required_all_flag);
            $this->set_szConsigneeFirstName(trim(sanitize_all_html_input($data['szConsigneeFirstName'])),$required_all_flag);
            $this->set_szConsigneeLastName(trim(sanitize_all_html_input($data['szConsigneeLastName'])),$required_all_flag);
            $this->set_szConsigneeEmail(trim(sanitize_all_html_input($data['szConsigneeEmail'])),$required_all_flag);				
            $this->set_szConsigneePhone(trim(sanitize_all_html_input($data['szConsigneePhoneNumber'])),$required_all_flag);	
            $this->set_idConsigneeDialCode(trim(sanitize_all_html_input($data['idConsigneeDialCode'])),$required_all_flag);  
            if($szDelivery == 'No'){
                $this->set_szDestinationAddress(trim(sanitize_all_html_input($data['szDestinationAddress'])),false);
            }else{
            $this->set_szDestinationAddress(trim(sanitize_all_html_input($data['szDestinationAddress'])),$required_all_flag);
            }
            if($szConsigneePostcodeRequired=='No')
            {
                $this->set_szDestinationPostcode(trim(sanitize_all_html_input($data['szDestinationPostcode'])),false); 
            }else{
            $this->set_szDestinationPostcode(trim(sanitize_all_html_input($data['szDestinationPostcode'])),$required_all_flag); 
            }
            $this->set_szDestinationCity(trim(sanitize_all_html_input($data['szDestinationCity'])),$required_all_flag); 
            $this->set_idDestinationCountry(trim(sanitize_all_html_input($data['idDestinationCountry'])),$required_all_flag); 
            $this->set_szCargoDescription(trim(sanitize_all_html_input($data['szCargoDescription'])),$required_all_flag);
            $this->set_iInsuranceIncluded($data[$type]['iInsuranceIncluded'],$required_all_flag);  

            $insurance_required_flag = false;
            if($this->iInsuranceIncluded==1 || $this->iInsuranceIncluded==3)
            {  
                if($type=='CREATE_BOOKING')
                {
                    $insurance_required_flag = true ; 
                }
            } 
            $this->set_idGoodsInsuranceCurrency($data['idGoodsInsuranceCurrency'],$insurance_required_flag); 
            $this->set_fCargoValue($data['fCargoValue'],false,$insurance_required_flag);  
            
            if($szBookingType=='SEND_QUOTE_CONFIRM')
            {
                $this->set_iBookingLanguage(trim(sanitize_all_html_input($data['SEND_QUOTE']['iBookingLanguage'])),'SEND_QUOTE');
                $this->set_szEmailSubject(trim($data['SEND_QUOTE']['szEmailSubject']),'SEND_QUOTE');
                $this->set_szEmailBody(trim($data['SEND_QUOTE']['szEmailBody']),'SEND_QUOTE');
            }
            else
            {
                $this->set_iBookingLanguage(trim(sanitize_all_html_input($data['CREATE_BOOKING']['iBookingLanguage'])),'CREATE_BOOKING');
                $this->set_szPaymentType(trim(sanitize_all_html_input($data['CREATE_BOOKING']['szPaymentType'])),'CREATE_BOOKING');
                $this->set_szEmailSubject(trim($data['CREATE_BOOKING']['szEmailSubject']),'CREATE_BOOKING');
                $this->set_szEmailBody(trim($data['CREATE_BOOKING']['szEmailBody']),'CREATE_BOOKING');
            }
            
            if($this->error==true)
            {
                return false;
            }
            else
            {
                $idUser = sanitize_all_html_input($data['idUser']);
                $idBooking = sanitize_all_html_input($data['idBooking']);
                $kBooking = new cBooking();
                $postSearchAry = array();
                $postSearchAry = $kBooking->getBookingDetails($idBooking);
                if($idUser>0)
                {
                    if($kUser->isUserEmailExist($this->szEmail,$idUser))
                    {
                        $this->addError('szEmail','An user with the following e-mail ID already exist.');
                        return false;
                    }
                }
                else
                {
                    if($kUser->isUserEmailExist($this->szEmail))
                    {
                        $idUser = $kUser->idIncompleteUser ;
                    }
                }
                $this->idCustomer = $idUser;
                
                $this->iIncludeInsuranceWithZeroCargoValue = 0;
                $this->iUpdateInsuranceWithBlankValue = 0;
                if($this->iInsuranceIncluded==1 || $this->iInsuranceIncluded==3)
                { 
                    /*
                    * If insurance is selected as 'Yes' or 'Optional' then we should check for insurance availability 
                    */
                    $insuranceInputAry = array();
                    $insuranceInputAry['idOriginCountry'] = $postSearchAry['idOriginCountry'];
                    $insuranceInputAry['idDestinationCountry'] = $postSearchAry['idDestinationCountry'];
                    $insuranceInputAry['idTransportMode'] = $postSearchAry['idTransportMode'];
                    $insuranceInputAry['isMoving'] = $postSearchAry['isMoving'];
                    $insuranceInputAry['fTotalBookingPriceUsd'] = $postSearchAry['fTotalPriceUSD']; 
                    $insuranceInputAry['szCargoType'] = $postSearchAry['szCargoType']; 
                    
                    $kInsurance = new cInsurance();
                    if($kInsurance->getInsuranceDetailsFroBooking($insuranceInputAry))
                    {
                        /*
                        *  This means there is insurance available for selected Trade
                        */
                        //TO DO
                        if($this->iInsuranceIncluded==1 && (int)$this->fCargoValue==0)
                        {
                            $this->iIncludeInsuranceWithZeroCargoValue = 1;
                            $this->iUpdateInsuranceWithBlankValue = 1;
                        } 
                        else if((int)$this->fCargoValue==0)
                        {
                            $this->iUpdateInsuranceWithBlankValue = 1;
                            $this->iOptionalInsuranceWithZeroCargoValue = 1;
                        }
                    }
                    else
                    {
                        //$this->addError('iInsuranceIncluded','Insurance not available');
                        $this->error = true;
                        $this->iInsuranceNotAvailable = 1;
                        return false;
                    }
                } 
                return true;
            } 
        }
    }
    
    function getPalletDetails($idPalletType=false,$iLanguage=false,$szApiName=false)
    {
        if($idPalletType>0 || !empty($szApiName))
        {  
            if(!empty($idPalletType))
            {
                $query_where = " id='".(int)$idPalletType."' ";
            }
            else if(!empty($szApiName))
            {
                $query_where = " szApiName = '".mysql_escape_custom($szApiName)."' ";
            }
            else
            {
                return false;
            }
            
            if((int)$iLanguage==0)
            {
                $iLanguage=__LANGUAGE_ID_ENGLISH__;
            }
            
            $kConfig = new cConfig();
            $configLangArr=$kConfig->getConfigurationLanguageData('__TABLE_PALLET_TYPE__',$iLanguage);
            
//            if($iLanguage==__LANGUAGE_ID_DANISH__)
//            {
//                $query_select = " szShortNameDanish as szShortName, szLongNameDanish as szLongName,";
//            }
//            else
//            {
//                $query_select = " szShortName, szLongName,";
//            }

            $query="
                SELECT
                    id,
                    fDefaultLength,
                    fDefaultWidth,
                    fDefaultHeight,
                    dtCreatedOn
                FROM	
                    ".__DBC_SCHEMATA_PALLET_TYPES__."
                WHERE
                  $query_where 					
            ";		
            //echo "<br>".$query."<br>" ;
            if($result=$this->exeSQL($query))
            { 
                $row = array();
                $row = $this->getAssoc($result); 
                
                if(!empty($configLangArr[$row['id']]))
                {
                    $countryAry= array_merge($configLangArr[$row['id']],$row);
                }else
                {
                    $countryAry = $row;
                }
                return $countryAry;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    
    function isPaypalAvailableForCountry($idCountry)
    {  
        $query="
            SELECT 
                pc.idCountry 
            FROM
                ".__DBC_SCHEMATA_PAYPAL_COUNTRIES__." AS pc 
            WHERE
                pc.idCountry='".(int)$idCountry."' 
        "; 
        if($result = $this->exeSQL( $query ))
        {
            if ($this->getRowCnt() > 0)
            {
                return true;
            }
            else
            { 
                return false;
            }
        }
        else
        { 
            return false;
        } 
    }
    
    
    function updateQuoteTaskDate()
    {
        $query="
            SELECT
                bq.id,
                qpd.dtCreatedOn
            FROM
                ".__DBC_SCHEMATA_QUOTE_PRICING_DETAILS__." AS qpd
            INNER JOIN
                ".__DBC_SCHEMATA_BOOKING_QUOTES__." AS bq
            ON
                bq.id=qpd.idQuote
            WHERE
                DATE(bq.dtStatusUpdatedOn)='0000-00-00'
            ORDER BY
                qpd.id DESC
            ";
            //echo $query;
            if($result = $this->exeSQL( $query ))
            {
                if ($this->getRowCnt() > 0)
                {
                    while($row=$this->getAssoc($result))
                    {
                        $queryUpdate="
                            UPDATE
                                ".__DBC_SCHEMATA_BOOKING_QUOTES__."
                            SET
                                dtStatusUpdatedOn='".  mysql_escape_custom($row['dtCreatedOn'])."'
                            WHERE
                                id='".(int)$row['id']."'
                            ";
                        //echo $queryUpdate."<br />";
                        $resultUpdate = $this->exeSQL( $queryUpdate );
                    }
                }
            }
                
    }
    
    
    function updateCustomerCurrencyExchangeRate($idBooking,$idCustomerCurrency)
    {
        if($idBooking>0 && $idCustomerCurrency>0)
        {
            if($idCustomerCurrency==1)
            { 
                $szCustomerCurrency = 'USD';
                $fCustomerExchangeRate =1;
            }
            else
            { 
                $kWhsSearch = new cWHSSearch();
                $resultAry = array();
                $resultAry = $kWhsSearch->getCurrencyDetails($idCustomerCurrency);		 
                $szCustomerCurrency = $resultAry['szCurrency'];
                $fCustomerExchangeRate = $resultAry['fUsdValue'];
            }
            $res_ary=array();
            $res_ary['szCustomerCurrency']=$szCustomerCurrency;
            $res_ary['idCustomerCurrency']=$idCustomerCurrency;
            $res_ary['fCustomerExchangeRate']=$fCustomerExchangeRate;
            if(!empty($res_ary))
            {
                $update_query = '';
                foreach($res_ary as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                } 
                $update_query = rtrim($update_query,","); 
                //echo $update_query;
                $kBooking = new cBooking();
                $kBooking->updateDraftBooking($update_query,$idBooking);
            }
            
            return $res_ary['fCustomerExchangeRate'];
        }
    }
    
    function convertBookingtoRFQ($postSearchAry,$iOfferType,$szFromPage)
    {
        if(!empty($postSearchAry))
        {
            $kBooking = new cBooking();
            $kRegisterShipCon = new cRegisterShipCon();
            $kConfig = new cConfig();
            
            $idBooking = $postSearchAry['id'];
            $res_ary=array();
            if((int)$postSearchAry['iBookingStep']<7)
            {
                $res_ary['iBookingStep'] = 7 ;
            }

            $res_ary['iBookingQuotes'] = 1 ;
            $res_ary['iQuotesStatus'] = 0;
            $res_ary['iGetQuotePageFlag'] = 1 ;

            if($szFromPage=='SEARCH_NOTIFICATION_POPUP')
            {
                $res_ary['szCargoType'] = '__PERSONAL_EFFECTS__';
            }
            else
            {
                $res_ary['szCargoType'] = '__GENERAL_CODE__';
            } 
            if(!empty($res_ary))
            {
                $update_query = '';
                foreach($res_ary as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            }
            $update_query = rtrim($update_query,",");
            if($kBooking->updateDraftBooking($update_query,$idBooking))
            {
                //booking moves on to second step means Trade Available.
            } 
            $postSearchAry = $kBooking->getBookingDetails($idBooking);
 
            if($_SESSION['user_id']>0)
            {
                $kUser = new cUser();
                $kUser->getUserDetails($_SESSION['user_id']);

                $szShipperCompanyName = $kUser->szCompanyName;
                $szShipperFirstName = $kUser->szFirstName;
                $szShipperLastName = $kUser->szLastName;
                $szShipperEmail = $kUser->szEmail;
                $szShipperPhone = $kUser->szPhoneNumber;
                $idShipperDialCode = $kUser->idInternationalDialCode; 
                $iPrivateShipping = $kUser->iPrivate;
            } 
            if(empty($szShipperCompanyName))
            {
                $szShipperCompanyName = 'Private';
            }

            $bookingQuotesAry = array();
            $bookingQuotesAry['szShipperCompanyName'] = $szShipperCompanyName;
            $bookingQuotesAry['szShipperFirstName'] = $szShipperFirstName;
            $bookingQuotesAry['szShipperLastName'] = $szShipperLastName ;
            $bookingQuotesAry['szShipperEmail'] = $szShipperEmail;
            $bookingQuotesAry['idShipperDialCode'] = $idShipperDialCode;
            $bookingQuotesAry['szShipperPhone'] = $szShipperPhone;
            $bookingQuotesAry['szOperationType'] = 'BOOKING_QUOTE_PAGE';
            $bookingQuotesAry['szBookingRandomNum'] = $postSearchAry['szBookingRandomNum'];
            $bookingQuotesAry['idServiceType'] = $postSearchAry['idServiceType'];
            $bookingQuotesAry['iFromGetOfferButton'] = 1;
            $bookingQuotesAry['iOfferType'] = $iOfferType;
            $bookingQuotesAry['iPrivateShipping'] = $iPrivateShipping;
            $szMode = 'GET_QUOTE';

            if($kRegisterShipCon->addBookingQuotesContactDetails($bookingQuotesAry,$idBooking,$szMode))
            { 
                return true;
            }  
            else
            {
                return false;
            }
        } 
    }
    
    function getQuotePricingKey()
    {
        $kBooking = new cBooking();
        $szQuotePricingKey = $kBooking->getUniqueBookingKey(16,true);
        
        $szQuotePricingKey = $szQuotePricingKey;
        $szQuotePricingKey = $this->isQuotePricingKeyExist($szQuotePricingKey); 
        return $szQuotePricingKey;
    }
    
    function isQuotePricingKeyExist($szQuotePricingKey)
    {
        if(!empty($szQuotePricingKey))
        { 
            $kBooking = new cBooking();
            $query="
                SELECT
                    id
                FROM
                    ".__DBC_SCHEMATA_QUOTE_PRICING_DETAILS__."
                WHERE
                    szQuotePricingKey = '".mysql_escape_custom(trim($szQuotePricingKey))."'
            ";
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {					
                    $szQuotePricingKey = $kBooking->getUniqueBookingKey(16,true); 
                    $szQuotePricingKey = $szQuotePricingKey;
                    $this->isQuotePricingKeyExist($szQuotePricingKey);
                }
                else
                {
                    return $szQuotePricingKey;	
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }	
        }
    } 
    
    function getQuotePricingKeybyID($idBookingQuotePricing)
    {
        if($idBookingQuotePricing>0)
        {  
            $query="
                SELECT
                    szQuotePricingKey
                FROM
                    ".__DBC_SCHEMATA_QUOTE_PRICING_DETAILS__."
                WHERE
                    id = '".(int)$idBookingQuotePricing."'
            ";
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {					 
                    $row = $row=$this->getAssoc($result);
                    return $row['szQuotePricingKey'];
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }	
        }
    }
    
    function getQuotePricingIDbyKey($szQuotePricingKey)
    {
        if(!empty($szQuotePricingKey))
        {  
            $query="
                SELECT
                    id,
                    idBooking
                FROM
                    ".__DBC_SCHEMATA_QUOTE_PRICING_DETAILS__."
                WHERE
                    szQuotePricingKey = '".mysql_escape_custom(trim($szQuotePricingKey))."'
                OR
                    id = '".mysql_escape_custom(trim($szQuotePricingKey))."'
            ";
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {					 
                    $row = $row=$this->getAssoc($result);
                    return $row;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }	
        }
    }
    
    function buildQuoteLink($idBookingQuotePricing,$iBookingLanguage=false)
    {
        if($idBookingQuotePricing>0)
        {
            if(empty($iBookingLanguage))
            {
                $iBookingLanguage = __LANGUAGE_ID_ENGLISH__;
            }
            $kQuote = new cQuote();
            $kConfig = new cConfig();
            $kBooking = new cBooking();
            $szQuotePricingKey = $kQuote->getQuotePricingKeybyID($idBookingQuotePricing);  
            $langArr = array();
            $langArr = $kConfig->getLanguageDetails('',$iBookingLanguage); 
            
           // $iOldSiteUrl = 1;
            if($iOldSiteUrl==1)
            {
                $quoteDataAry = array();
                $quoteDataAry = $this->getQuotePricingIDbyKey($szQuotePricingKey);
                $idBooking = $quoteDataAry['idBooking'];
                
                $postSearchAry = $kBooking->getBookingDetails($idBooking);
                $szBookingRandomKey = $postSearchAry['szBookingRandomNum'];
                
                if(!empty($langArr) && $iBookingLanguage!=__LANGUAGE_ID_ENGLISH__)
                {
                    $szLanguageCode = $langArr[0]['szLanguageCode'];
                    $szControlPanelUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".$szLanguageCode."/pendingQuotes/".$szBookingRandomKey."/".$idBookingQuotePricing."/"; 
                }
                else
                {
                    $szControlPanelUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/pendingQuotes/".$szBookingRandomKey."/".$idBookingQuotePricing."/";
                } 
                return $szControlPanelUrl;
            }
            else
            { 
                $szDomain = getDomainByIdLanguage($iBookingLanguage);                
                $szControlPanelUrl = $szDomain."/quote/#".$szQuotePricingKey; 
                return $szControlPanelUrl;
            }  
        }
    }
    
    function set_szOriginCountryStr($value,$flag=true)
    {
        $this->szOriginCountryStr = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szOriginCountryStr", '', false, false, $flag );
    }
    function set_szDestinationCountryStr($value,$flag=true)
    {
        $this->szDestinationCountryStr = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDestinationCountryStr", '', false, false, $flag );
    }
    function set_idServiceTerms($value,$flag=true)
    {
        $this->idServiceTerms = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idServiceTerms", '', false, false, $flag );
    } 
    function set_dtShipmentDate($value,$flag=true)
    {
        $this->dtShipmentDate = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "dtShipmentDate", '', false, false, $flag );
    }
    function set_szCargoType($value,$flag=true)
    {
        $this->szCargoType = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCargoType", '', false, false, $flag );
    }
    function set_szShipmentType($value,$flag=true)
    {
        $this->szShipmentType = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipmentType", '', false, false, $flag );
    }
    function set_fTotalVolume($value,$szShipmentType,$flag=true)
    {
        $this->fTotalVolume = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fTotalVolume_".$szShipmentType, '', false, false, $flag );
    }
    function set_fTotalWeight($value,$szShipmentType,$flag=true)
    {
        $this->fTotalWeight = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fTotalWeight_".$szShipmentType, '', false, false, $flag );
    } 
    function set_idCustomerCurrency($value,$flag=true)
    {
        $this->idCustomerCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idCustomerCurrency", '', false, false, $flag );
    } 
    function set_idCustomerCountry($value,$flag=true) 
    {
        $this->idCustomerCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idCustomerCountry", '', false, false, $flag );
    } 
    
    function set_iLength( $value , $counter,$szShipmentType,$max_limit,$flag=true)
    {  
        $szInputId = "iLength".$counter."_V_".$szShipmentType; 
        $this->iLength[$szShipmentType][$counter] = $this->validateInput( $value, __VLD_CASE_NUMERIC__, $szInputId, t($this->t_base.'errors/length'), false, $max_limit, $flag);
    }
    function set_iWidth($value , $counter,$szShipmentType,$max_limit,$flag=true)
    {  
        $szInputId = "iWidth".$counter."_V_".$szShipmentType; 
        $this->iWidth[$szShipmentType][$counter] = $this->validateInput( $value, __VLD_CASE_NUMERIC__, $szInputId, t($this->t_base.'errors/width'), false, $max_limit, $flag);
    }  
    function set_iHeight($value , $counter,$szShipmentType,$max_limit,$flag=true)
    {  
        $szInputId = "iHeight".$counter."_V_".$szShipmentType; 
        $this->iHeight[$szShipmentType][$counter] = $this->validateInput( $value, __VLD_CASE_NUMERIC__, $szInputId, t($this->t_base.'errors/height'), false, $max_limit, $flag);
    }
    function set_iQuantity($value , $counter,$szShipmentType,$max_limit,$flag=true)
    {  
        $szInputId = "iQuantity".$counter."_V_".$szShipmentType; 
        $this->iQuantity[$szShipmentType][$counter] = $this->validateInput( $value, __VLD_CASE_NUMERIC__, $szInputId, t($this->t_base.'errors/quantity'), false, $max_limit, $flag);
    }
    function set_iWeight($value , $counter,$szShipmentType,$max_limit,$flag=true)
    {  
        $szInputId = "iWeight".$counter."_V_".$szShipmentType; 
        $this->iWeight[$szShipmentType][$counter] = $this->validateInput( $value, __VLD_CASE_NUMERIC__, $szInputId, t($this->t_base.'errors/weight'), false, $max_limit, $flag);
    }
    function set_idWeightMeasure($value , $counter,$szShipmentType,$flag=true)
    {  
        $szInputId = "idWeightMeasure".$counter."_V_".$szShipmentType; 
        $this->idWeightMeasure[$szShipmentType][$counter] = $this->validateInput( $value, __VLD_CASE_NUMERIC__, $szInputId, t($this->t_base.'errors/weight'), false, false, $flag);
    }
    function set_idCargoMeasure($value , $counter,$szShipmentType,$flag=true)
    {  
        $szInputId = "idCargoMeasure".$counter."_V_".$szShipmentType; 
        $this->idCargoMeasure[$szShipmentType][$counter] = $this->validateInput( $value, __VLD_CASE_NUMERIC__, $szInputId, t($this->t_base.'errors/weight'), false, false, $flag);
    } 
    function set_szShipperFirstName( $value,$required_flag=true )
    {
        $this->szShipperFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperFirstName", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/f_name'), false, 255, $required_flag );
    }
    function set_szShipperLastName( $value ,$required_flag=true)
    {
        $this->szShipperLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperLastName", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/l_name'), false, 255, $required_flag );
    }

    function set_szShipperCompanyName( $value,$required_flag=true )
    {
        $this->szShipperCompanyName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperCompanyName", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/c_name'), false, 255, $required_flag );
    }
    function set_szShipperEmail( $value,$flag=true )
    {
            $this->szShipperEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szShipperEmail", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/email'), false, 255, $flag );
    }
    function set_idShipperDialCode( $value ,$flag=true)
    {
        $this->idShipperDialCode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idShipperDialCode", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/country'), false, 255, $flag );
    } 
    function set_szShipperPhone( $value,$flag=true )
    {
        $this->szShipperPhoneNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperPhoneNumber", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/p_number'), false, false, $flag );
    } 
    function set_szConsigneeFirstName( $value,$required_flag=true )
    {
        $this->szConsigneeFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeFirstName", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/f_name'), false, 255, $required_flag );
    }
    function set_szConsigneeLastName( $value,$required_flag=true )
    {
        $this->szConsigneeLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeLastName", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/l_name'), false, 255, $required_flag );
    } 
    function set_szConsigneeCompanyName( $value,$required_flag=true )
    {
        $this->szConsigneeCompanyName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeCompanyName", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/c_name'), false, 255, $required_flag );
    }
    function set_szConsigneeEmail( $value ,$flag=true)
    {
        $this->szConsigneeEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szConsigneeEmail", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/email'), false, 255, $flag );
    }
    function set_idConsigneeCountry( $value,$flag=true )
    {
        $this->idConsigneeCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idConsigneeCountry", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/country'), false, 255, $flag );
    }
    function set_szConsigneePhone( $value,$flag=true )
    {
        $this->szConsigneePhoneNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneePhoneNumber", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/p_number'), false, false, $flag );
    } 
    function set_idConsigneeDialCode( $value,$flag=true )
    {
        $this->idConsigneeDialCode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idConsigneeDialCode", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/country'), false, 255, $flag );
    }  
    function set_szCustomerAddress1( $value,$flag=true )
    {
        $this->szCustomerAddress1 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCustomerAddress1", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/p_number'), false, false, $flag );
    } 
    function set_szCustomerPostCode( $value,$flag=true )
    {
        $this->szCustomerPostCode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCustomerPostCode", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/p_number'), false, false, $flag );
    }
    function set_szCustomerCity( $value,$flag=true )
    {
        $this->szCustomerCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCustomerCity", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/p_number'), false, false, $flag );
    }
    function set_szCustomerCountry( $value,$flag=true )
    {
        $this->szCustomerCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCustomerCountry", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/p_number'), false, false, $flag );
    }  
    function set_szEmail( $value,$flag=true)
    {
        $this->szEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szEmail", t($this->t_base.'fields/email'), false, 255, $flag );
    } 
    function set_szEmailBody($value,$szBookingType,$flag=true)
    {
        $this->szEmailBody = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szEmailBody_".$szBookingType, t($this->t_base.'fields/email_body'), false, false, $flag );
    }
    function set_szEmailSubject($value,$szBookingType,$flag=true)
    {
        $this->szEmailSubject = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szEmailSubject_".$szBookingType, t($this->t_base.'fields/email_subject'), false, false, $flag );
    }
    function set_iInsuranceIncluded( $value,$flag=true )
    {
        $this->iInsuranceIncluded = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iInsuranceIncluded", t($this->t_base.'fields/insurance_included'), false, false, $flag );
    } 
    function set_fCargoValue( $value,$flag=true )
    { 
        $this->fCargoValue = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "fCargoValue", t($this->t_base.'fields/insurance_price'), false, false, $flag ); 
    } 
    function set_szCargoDescription( $value,$flag=false)
    {
        $this->szCargoDescription = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCargoDescription", t($this->t_base.'fields/cargo_description'), false, false, $flag);
    }
    function set_szOriginAddress( $value,$flag=false )
    {
        $this->szOriginAddress = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szOriginAddress", t($this->t_base.'fields/from_postcode'), false, false, $flag );
    }
    function set_szOriginPostcode( $value,$flag=true )
    {
        $this->szOriginPostcode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szOriginPostcode", t($this->t_base.'fields/from_postcode'), false, false, $flag );
    }
    function set_szOriginCity( $value,$flag=true )
    {
        $this->szOriginCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szOriginCity", t($this->t_base.'fields/from_city'), false, false, $flag );
    }
    function set_idOriginCountry( $value,$flag=true )
    {
        $this->idOriginCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idOriginCountry", t($this->t_base.'fields/from_country'), false, false, $flag );
    } 
    function set_szDestinationAddress( $value,$flag=false )
    {
        $this->szDestinationAddress = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDestinationAddress", t($this->t_base.'fields/to_address'), false, false, $flag );
    }
    function set_szDestinationPostcode( $value,$flag=true )
    {
        $this->szDestinationPostcode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDestinationPostcode", t($this->t_base.'fields/to_postcode'), false, false, $flag );
    }
    function set_szDestinationCity( $value,$flag=true )
    {
        $this->szDestinationCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDestinationCity", t($this->t_base.'fields/to_city'), false, false, $flag );
    }
    function set_idDestinationCountry( $value,$flag=true )
    {
        $this->idDestinationCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idDestinationCountry", t($this->t_base.'fields/to_country'), false, false, $flag );
    } 
    function set_iBookingLanguage( $value,$flag=true )
    {
        $this->iBookingLanguage = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iBookingLanguage", t($this->t_base.'fields/booking_language'), false, false, $flag );
    }
    function set_iShipperConsignee($value,$flag=true)
    {
        $this->iShipperConsignee = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "customer_role_radio_container", t($this->t_base.'fields/phone'), 1, 3, $flag );
    }
    function set_szFirstName( $value,$flag=true )
    {
        $this->szFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szFirstName", t($this->t_base.'fields/first_name'), false, false, $flag );
    }
    function set_szLastName( $value,$flag=true )
    {
        $this->szLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLastName", t($this->t_base.'fields/last_name'), false, false, $flag );
    } 
    function set_idCustomerDialCode( $value,$flag=true )
    {
        $this->idCustomerDialCode = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idCustomerDialCode", t($this->t_base.'fields/dial_code'), false, false, $flag );
    }
    function set_szCustomerPhoneNumber( $value,$flag=true )
    {
        $this->szCustomerPhoneNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCustomerPhoneNumber", t($this->t_base.'fields/phone'), false, false, $flag );
    }
    function set_szCustomerCompanyName( $value,$flag=true )
    {
        $this->szCustomerCompanyName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCustomerCompanyName", t($this->t_base.'fields/company_name'), false, false, $flag );
    }
    function set_idGoodsInsuranceCurrency( $value,$flag=true )
    {
        $this->idGoodsInsuranceCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idGoodsInsuranceCurrency", t($this->t_base.'fields/insurance')." ".t($this->t_base.'fields/currency'), false, false, $flag );
    }
    function set_szPaymentType( $value,$flag=true )
    {
        $this->szPaymentType = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPaymentType", t($this->t_base.'fields/company_name'), false, false, $flag );
    }
    
}

?>