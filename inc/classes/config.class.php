<?php
/**
 * This file is the container for all user related functionality.
 * All functionality related to user details should be contained in this class.
 *
 * config.class.php
 *
 * @copyright Copyright (C) 2012 Transport-ece
 * @author Ajay
 */
if(!isset($_SESSION))
{
    session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );


class cConfig extends cDatabase
{
    function __construct()
    {
        parent::__construct();
        return true;
    }
		
    var $foundServicesAry = array();
    var $idServiceType ;
    var $szOriginCountry ;
    var $szOriginCity ;
    var $szOriginPostCode;
    var $dtTiming;
    var $szDestinationCountry;
    var $szDestinationCity;
    var $szDestinationPostCode;
    var $iLength=array();
    var $iHeight=array();
    var $iWidth=array();
    var $iQuantity=array();
    var $iWeight=array() ;
    var $emptyPostcodeCtr;
    var $t_base = "home/homepage/";
    var $t_base_error_mang = "management/Error/";
    var $t_base_error_booking = "Booking/MyBooking/";
    var $idPostCode;
    var $szPostCode;
    var $szLat;
    var $szLng;
    var $szRegion1;
    var $szRegion2;
    var $szRegion3;
    var $szRegion4;
    var $szCity;
    var $szCity1;
    var $szCity2;
    var $szCity3;
    var $szCity4;
    var $szCity5;
    var $szCity6;
    var $szCity7;
    var $szCity8;
    var $szCity9;
    var $szCity10;
    var $idCountry;
    var $id;
    var $szRetypePassword;
    var $szPassword;
    var $szCurrency;
    var $t_base_pendingtray = "management/pendingTray/";
    
	/**
	 * This function return all the countries which has standard trucking rate is defined .
	 *
	 * @access public
	 * @return array
	 * @author Anil
	 */
	function getAllCountries($flag=false,$iLanguage=__LANGUAGE_ID_ENGLISH__,$iInternationDialCode=false)
	{				
		if(!$flag)
		{
                    $sql .=" AND fStandardTruckRate > 0 ";	 
		}
                
                if((int)$iLanguage==0)
                {
                    $iLanguage=__LANGUAGE_ID_ENGLISH__;
                }  
		if($iInternationDialCode)
		{
                    $query_order_by = "iInternationDialCode ASC ";
		}
		else
		{
                    $query_order_by = "szCountryName ASC ";
		}
		$query="
                    SELECT
                        c.id,
                        c.id as idCountry,
                        cnm.szName AS szCountryName, 
                        iInternationDialCode,
                        szCountryISO,
                        iDefaultCurrency
                    FROM	
                        ".__DBC_SCHEMATA_COUNTRY__." c
                    INNER JOIN
                        ".__DBC_SCHEMATA_COUNTRY_NAME_MAPPING__." AS cnm
                    ON
                        cnm.idCountry=c.id
                    WHERE
                        iActive = 1
                    AND
                        cnm.idLanguage='".(int)$iLanguage."'
                        ".$sql."	
                    ORDER BY 
                        $query_order_by						
		";		
		//echo "<br /> ".$query."<br />";
		if($result=$this->exeSQL($query))
		{
                    $countryAry=array();
                    $to_ctr=0;
                    $from_ctr = 0;
                    while($row=$this->getAssoc($result))
                    { 
                        $countryAry[]=$row; 
                    }
                    return $countryAry ;
		}
                else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}	
	}
	function isServiceExists($idCountry,$mode)
	{
		if($idCountry>0)
		{
			if($mode=='ORIGIN')
			{
				$query_and = " AND s.idOriginCountry = '".(int)$idCountry."' ";
			}
			
			if($mode=='DESTINATION')
			{
				$query_and = " AND s.idDestinationCountry = '".(int)$idCountry."'";
			}
			
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_SERVICE_AVAILABLE__." s
				WHERE
					s.dtValidTo > now()
				AND
					iStatus = '1'
				".$query_and."
			";
			//echo "<br />".$query."<br/>";
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			}
		}
	}
	
        function getServiceProviderCompanies($data)
        { 
            if(!empty($data))
            {  
                $idTransportMode = $data['idTransportMode'];
                $idOriginCountry = $data['idOriginCountry'];
                $idDestinationCountry = $data['idDestinationCountry'];
                 
                $this->loadCountry($idOriginCountry);
                $idOrgRegion = $this->idRegion ;
                
                $kAdmin = new cAdmin();
                if($idOrgRegion>0)
                {
                    $orgSearchAry = array();
                    $orgRegionAry = array();
                    
                    $orgSearchAry['idRegion'] = $idOrgRegion ;
                    $orgRegionAry = $kAdmin->getAllRegions($orgSearchAry);
                    
                    $idOriginRegion = $orgRegionAry[0]['idCountry'];
                } 
                $this->loadCountry($idDestinationCountry);
                $idDesRegion = $this->idRegion ;
                
                if($idDesRegion>0)
                {
                    $desSearchAry = array();
                    $desRegionAry = array();
                    
                    $desSearchAry['idRegion'] = $idDesRegion ;
                    $desRegionAry = $kAdmin->getAllRegions($desSearchAry);
                    
                    $idDestinationRegion = $desRegionAry[0]['idCountry'];
                }  
                
                $searchAry = array();
                $searchAry['arrTransportMode'][0] = $idTransportMode ;
                $searchAry['arrTransportMode'][1] = __ALL_WORLD_TRANSPORT_MODE_ID__ ;
                $searchAry['arrTransportMode'][2] = __REST_TRANSPORT_MODE_ID__ ;
                
                $kForwarderConact = new cForwarderContact();
                $forwarderContactListAry = $kForwarderConact->getAllQuotesAndBookingEmails(false,false,$searchAry);
                     
                $foundEmailaAry = array();
                if(!empty($forwarderContactListAry))
                { 
                    foreach($forwarderContactListAry as $forwarderContactListArys)
                    {  
                        if(($forwarderContactListArys['idOriginCountry']==$idOriginCountry) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationCountry))
                        {
                            $foundEmailaAry[] = $forwarderContactListArys['idForwarder'];
                        }
                        else if(($forwarderContactListArys['idOriginCountry']==$idOriginRegion) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationCountry))
                        {
                            $foundEmailaAry[] = $forwarderContactListArys['idForwarder'];
                        }
                        else if(($forwarderContactListArys['idOriginCountry']==$idOriginCountry) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationRegion))
                        {
                            $foundEmailaAry[] = $forwarderContactListArys['idForwarder'];
                        }
                        else if(($forwarderContactListArys['idOriginCountry']==$idOriginRegion) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationRegion))
                        {
                            $foundEmailaAry[] = $forwarderContactListArys['idForwarder'];
                        }
                        else if(($forwarderContactListArys['idOriginCountry']==__WHOLE_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationCountry))
                        {
                            $foundEmailaAry[] = $forwarderContactListArys['idForwarder'];
                        }
                        else if(($forwarderContactListArys['idOriginCountry']==$idOriginCountry) && ($forwarderContactListArys['idDestinationCountry']==__WHOLE_WORLD_PREFERENCES_ID__))
                        {
                            $foundEmailaAry[] = $forwarderContactListArys['idForwarder'];
                        } 
                        else if(($forwarderContactListArys['idOriginCountry']==$idOriginRegion) && ($forwarderContactListArys['idDestinationCountry']==__WHOLE_WORLD_PREFERENCES_ID__))
                        {
                            $foundEmailaAry[] = $forwarderContactListArys['idForwarder'];
                        }
                        else if(($forwarderContactListArys['idOriginCountry']==__WHOLE_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationRegion))
                        {
                            $foundEmailaAry[] = $forwarderContactListArys['idForwarder'];
                        }
                        else if(($forwarderContactListArys['idOriginCountry']==__WHOLE_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==__WHOLE_WORLD_PREFERENCES_ID__))
                        {
                            $foundEmailaAry[] = $forwarderContactListArys['idForwarder'];
                        }  
                        else if(($forwarderContactListArys['idOriginCountry']==__REST_OF_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationCountry))
                        {
                            $foundEmailaAry[] = $forwarderContactListArys['idForwarder'];
                        }
                        else if(($forwarderContactListArys['idOriginCountry']==$idOriginCountry) && ($forwarderContactListArys['idDestinationCountry']==__REST_OF_WORLD_PREFERENCES_ID__))
                        {
                            $foundEmailaAry[] = $forwarderContactListArys['idForwarder'];
                        }
                        else if(($forwarderContactListArys['idOriginCountry']==__REST_OF_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationRegion))
                        {
                            $foundEmailaAry[] = $forwarderContactListArys['idForwarder'];
                        }
                        else if(($forwarderContactListArys['idOriginCountry']==$idOriginRegion) && ($forwarderContactListArys['idDestinationCountry']==__REST_OF_WORLD_PREFERENCES_ID__))
                        {
                            $foundEmailaAry[] = $forwarderContactListArys['idForwarder'];
                        }
                        else if(($forwarderContactListArys['idOriginCountry']==__WHOLE_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==__REST_OF_WORLD_PREFERENCES_ID__))
                        {
                            $foundEmailaAry[] = $forwarderContactListArys['idForwarder'];
                        }
                        else if(($forwarderContactListArys['idOriginCountry']==__REST_OF_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==__WHOLE_WORLD_PREFERENCES_ID__))
                        {
                            $foundEmailaAry[] = $forwarderContactListArys['idForwarder'];
                        }
                        else if(($forwarderContactListArys['idOriginCountry']==__REST_OF_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==__REST_OF_WORLD_PREFERENCES_ID__))
                        {
                            $foundEmailaAry[] = $forwarderContactListArys['idForwarder'];
                        }
                    }
                }   
                return $foundEmailaAry ;
            }
        }
        
        function getForwarderContact($data)
        { 
            if(!empty($data))
            { 
                $idForwarder = $data['idForwarder'];
                $idTransportMode = $data['idTransportMode'];
                $idOriginCountry = $data['idOriginCountry'];
                $idDestinationCountry = $data['idDestinationCountry'];
                $iBookingSentFlag = $data['iBookingSentFlag'];
                
                $this->loadCountry($idOriginCountry);
                $idOrgRegion = $this->idRegion ;
                
                $kAdmin = new cAdmin();
                if($idOrgRegion>0)
                {
                    $orgSearchAry = array();
                    $orgRegionAry = array();
                    
                    $orgSearchAry['idRegion'] = $idOrgRegion ;
                    $orgRegionAry = $kAdmin->getAllRegions($orgSearchAry);
                    
                    $idOriginRegion = $orgRegionAry[0]['idCountry'];
                }
                 
                $this->loadCountry($idDestinationCountry);
                $idDesRegion = $this->idRegion ;
                
                if($idDesRegion>0)
                {
                    $desSearchAry = array();
                    $desRegionAry = array();
                    
                    $desSearchAry['idRegion'] = $idDesRegion ;
                    $desRegionAry = $kAdmin->getAllRegions($desSearchAry);
                    
                    $idDestinationRegion = $desRegionAry[0]['idCountry'];
                }  
                
                $searchAry = array();
                $searchAry['arrTransportMode'][0] = $idTransportMode ;
                $searchAry['arrTransportMode'][1] = __ALL_WORLD_TRANSPORT_MODE_ID__ ;
                $searchAry['arrTransportMode'][2] = __REST_TRANSPORT_MODE_ID__ ;
                
                $kForwarderConact = new cForwarderContact();
                $forwarderContactListAry = $kForwarderConact->getAllQuotesAndBookingEmails($idForwarder,false,$searchAry);
                  
                $foundEmailaAry = array();
                if(!empty($forwarderContactListAry))
                { 
                    foreach($forwarderContactListAry as $forwarderContactListArys)
                    { 
                        if($iBookingSentFlag==1)
                        {
                            $szEmail=$forwarderContactListArys['szBookingEmail'];
                        }
                        else
                        {
                            $szEmail=$forwarderContactListArys['szEmail'];
                        }
                        if(($forwarderContactListArys['idOriginCountry']==$idOriginCountry) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationCountry))
                        {
                            $foundEmailaAry[] = $szEmail;
                        }
                        else if(($forwarderContactListArys['idOriginCountry']==$idOriginRegion) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationCountry))
                        {
                            $foundEmailaAry[] = $szEmail;
                        }
                        else if(($forwarderContactListArys['idOriginCountry']==$idOriginCountry) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationRegion))
                        {
                            $foundEmailaAry[] = $szEmail;
                        }
                        else if(($forwarderContactListArys['idOriginCountry']==$idOriginRegion) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationRegion))
                        {
                            $foundEmailaAry[] = $szEmail;
                        }
                        else if(($forwarderContactListArys['idOriginCountry']==__WHOLE_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationCountry))
                        {
                            $foundEmailaAry[] = $szEmail;
                        }
                        else if(($forwarderContactListArys['idOriginCountry']==$idOriginCountry) && ($forwarderContactListArys['idDestinationCountry']==__WHOLE_WORLD_PREFERENCES_ID__))
                        {
                            $foundEmailaAry[] = $szEmail;
                        } 
                        else if(($forwarderContactListArys['idOriginCountry']==$idOriginRegion) && ($forwarderContactListArys['idDestinationCountry']==__WHOLE_WORLD_PREFERENCES_ID__))
                        {
                            $foundEmailaAry[] = $szEmail;
                        }
                        else if(($forwarderContactListArys['idOriginCountry']==__WHOLE_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationRegion))
                        {
                            $foundEmailaAry[] = $szEmail;
                        }
                        else if(($forwarderContactListArys['idOriginCountry']==__WHOLE_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==__WHOLE_WORLD_PREFERENCES_ID__))
                        {
                            $foundEmailaAry[] = $szEmail;
                        }  
                    }
                    
                    if(empty($foundEmailaAry))
                    {
                        //If we do not got any matching on based of country, region and whole then search for Rest of world option 
                        foreach($forwarderContactListAry as $forwarderContactListArys)
                        { 
                            if($iBookingSentFlag==1)
                            {
                                $szEmail=$forwarderContactListArys['szBookingEmail'];
                            }
                            else
                            {
                                $szEmail=$forwarderContactListArys['szEmail'];
                            }
                            if(($forwarderContactListArys['idOriginCountry']==__REST_OF_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationCountry))
                            {
                                $foundEmailaAry[] = $szEmail;
                            }
                            else if(($forwarderContactListArys['idOriginCountry']==$idOriginCountry) && ($forwarderContactListArys['idDestinationCountry']==__REST_OF_WORLD_PREFERENCES_ID__))
                            {
                                $foundEmailaAry[] = $szEmail;
                            }
                            else if(($forwarderContactListArys['idOriginCountry']==__REST_OF_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationRegion))
                            {
                                $foundEmailaAry[] = $szEmail;
                            }
                            else if(($forwarderContactListArys['idOriginCountry']==$idOriginRegion) && ($forwarderContactListArys['idDestinationCountry']==__REST_OF_WORLD_PREFERENCES_ID__))
                            {
                                $foundEmailaAry[] = $szEmail;
                            }
                            else if(($forwarderContactListArys['idOriginCountry']==__WHOLE_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==__REST_OF_WORLD_PREFERENCES_ID__))
                            {
                                $foundEmailaAry[] = $szEmail;
                            }
                            else if(($forwarderContactListArys['idOriginCountry']==__REST_OF_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==__WHOLE_WORLD_PREFERENCES_ID__))
                            {
                                $foundEmailaAry[] = $szEmail;
                            }
                            else if(($forwarderContactListArys['idOriginCountry']==__REST_OF_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==__REST_OF_WORLD_PREFERENCES_ID__))
                            {
                                $foundEmailaAry[] = $szEmail;
                            }  
                        }
                    } 
                } 
                if(empty($foundEmailaAry))
                {
                    $kForwarderContact = new cForwarderContact();
                    $catchUpBookingQuoteEmailAry = $kForwarderContact->getAllForwarderContactsEmail($idForwarder,__CATCH_UP_ALL_BOOKING_QUOTE_PROFILE_ID__);
                    $foundEmailaAry[] = $catchUpBookingQuoteEmailAry['szCatchUpBookingQuoteEmail'][0]['szEmail']; 
                }
                return $foundEmailaAry ;
            }
        }
        
        function checkPreferencesEmail($forwarderContactListArys)
        {
            if(!empty($forwarderContactListArys))
            {                
                $idOriginCountry = $forwarderContactListArys['idOriginCountry'] ;
                $idDestinationCountry = $forwarderContactListArys['idDestinationCountry'] ;
                $idOriginRegion = $forwarderContactListArys['idOriginRegion'] ;
                $idDestinationRegion = $forwarderContactListArys['idDestinationRegion'] ;
                
                if(($forwarderContactListArys['idOriginCountry']==$idOriginCountry) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationCountry))
                {
                    $foundEmailaAry[] = $forwarderContactListArys['szEmail'];
                }
                else if(($forwarderContactListArys['idOriginCountry']==$idOriginRegion) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationCountry))
                {
                    $foundEmailaAry[] = $forwarderContactListArys['szEmail'];
                }
                else if(($forwarderContactListArys['idOriginCountry']==$idOriginCountry) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationRegion))
                {
                    $foundEmailaAry[] = $forwarderContactListArys['szEmail'];
                }
                else if(($forwarderContactListArys['idOriginCountry']==$idOriginRegion) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationRegion))
                {
                    $foundEmailaAry[] = $forwarderContactListArys['szEmail'];
                }
                else if(($forwarderContactListArys['idOriginCountry']==__WHOLE_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationCountry))
                {
                    $foundEmailaAry[] = $forwarderContactListArys['szEmail'];
                }
                else if(($forwarderContactListArys['idOriginCountry']==$idOriginCountry) && ($forwarderContactListArys['idDestinationCountry']==__WHOLE_WORLD_PREFERENCES_ID__))
                {
                    $foundEmailaAry[] = $forwarderContactListArys['szEmail'];
                } 
                else if(($forwarderContactListArys['idOriginCountry']==$idOriginRegion) && ($forwarderContactListArys['idDestinationCountry']==__WHOLE_WORLD_PREFERENCES_ID__))
                {
                    $foundEmailaAry[] = $forwarderContactListArys['szEmail'];
                }
                else if(($forwarderContactListArys['idOriginCountry']==__WHOLE_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationRegion))
                {
                    $foundEmailaAry[] = $forwarderContactListArys['szEmail'];
                }
                else if(($forwarderContactListArys['idOriginCountry']==__WHOLE_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==__WHOLE_WORLD_PREFERENCES_ID__))
                {
                    $foundEmailaAry[] = $forwarderContactListArys['szEmail'];
                } 
                else if(($forwarderContactListArys['idOriginCountry']==__REST_OF_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationCountry))
                {
                    $foundEmailaAry[] = $forwarderContactListArys['szEmail'];
                }
                else if(($forwarderContactListArys['idOriginCountry']==$idOriginCountry) && ($forwarderContactListArys['idDestinationCountry']==__REST_OF_WORLD_PREFERENCES_ID__))
                {
                    $foundEmailaAry[] = $forwarderContactListArys['szEmail'];
                }
                else if(($forwarderContactListArys['idOriginCountry']==__REST_OF_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==$idDestinationRegion))
                {
                    $foundEmailaAry[] = $forwarderContactListArys['szEmail'];
                }
                else if(($forwarderContactListArys['idOriginCountry']==$idOriginRegion) && ($forwarderContactListArys['idDestinationCountry']==__REST_OF_WORLD_PREFERENCES_ID__))
                {
                    $foundEmailaAry[] = $forwarderContactListArys['szEmail'];
                }
                else if(($forwarderContactListArys['idOriginCountry']==__WHOLE_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==__REST_OF_WORLD_PREFERENCES_ID__))
                {
                    $foundEmailaAry[] = $forwarderContactListArys['szEmail'];
                }
                else if(($forwarderContactListArys['idOriginCountry']==__REST_OF_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==__WHOLE_WORLD_PREFERENCES_ID__))
                {
                    $foundEmailaAry[] = $forwarderContactListArys['szEmail'];
                }
                else if(($forwarderContactListArys['idOriginCountry']==__REST_OF_WORLD_PREFERENCES_ID__) && ($forwarderContactListArys['idDestinationCountry']==__REST_OF_WORLD_PREFERENCES_ID__))
                {
                    $foundEmailaAry[] = $forwarderContactListArys['szEmail'];
                } 
                return $foundEmailaAry ;
            }
        }
        
	/**
	 * This function return all the configuration data from DB according to table name.
	 *
	 * @access public
	 * @param $szTableName
	 * @return array
	 * @author Anil
	 */
	function getConfigurationData($szTableName,$id=false,$flag=false,$get_english_name=false)
	{
		if(!empty($szTableName))
		{	
			if($szTableName==__DBC_SCHEMATA_SERVICE_TYPE__)
			{
				if($get_english_name)
				{
					$query_select = "szShortName,szDescription";
				}
				else
				{
					if(__LANGUAGE__=='danish')
					{
						$query_select = "szShortNameDanish as szShortName, szDescriptionDanish as szDescription";
					}
					else
					{
						$query_select = "szShortName,szDescription";
					}
				}
			}
			else 
			{
				if(__LANGUAGE__=='danish')
				{
					$query_select = "szDescriptionDanish as szDescription";
				}
				else
				{
					$query_select = "szDescription";
				}
			}
			
			if($flag)
			{
				$orderby= "ORDER BY
							id DESC";
			}
			
			if($id>0)
			{
				$query_and = " WHERE id ='".(int)$id."'" ;
			}
			else if($szTableName ==__DBC_SCHEMATA_SERVICE_TYPE__)
			{
				$query_and = " WHERE iActive ='1' " ;
				$orderby = " ORDER BY iOrder ASC ";
			}
			$query="
				SELECT
					id,
					$query_select
				FROM	
					".mysql_escape_custom(trim($szTableName))."
					".$query_and."
					".$orderby."						
			";		
			//echo $query ;
			if($result=$this->exeSQL($query))
			{
				$serviceTypeAry=array();
				$ctr = 0;
				while($row=$this->getAssoc($result))
				{
					$serviceTypeAry[$ctr]=$row;
					$ctr++;
				}
				return $serviceTypeAry ;
			}	
		}
	}
/**
	 * This function return all the configuration data from DB according to table name.
	 *
	 * @access public
	 * @param $szTableName
	 * @return array
	 * @author Anil
	 */
	function getAllWeekDays($id=false)
	{
		if($id>0)
		{
			$query_and = " WHERE id ='".(int)$id."'" ;
		}
		$query="
			SELECT
				id,
				szWeekDay
			FROM	
				".__DBC_SCHEMATA_WEEK_DAYS__."
				".$query_and."						
		";	
		//echo $query ;
		if($result=$this->exeSQL($query))
		{
			$serviceTypeAry=array();
			$ctr = 0;
			while($row=$this->getAssoc($result))
			{
				$serviceTypeAry[$ctr]=$row;
				$ctr++;
			}
			return $serviceTypeAry ;
		}	
	}
	
	/**
	 * This function return all booking currency .
	 *
	 * @access public
	 * @param $szTableName
	 * @return array
	 * @author Anil
	 */
	 
	function getBookingCurrency($idCurrency=false,$iBooking=false,$iSettling=false,$iPricing=false,$szCurrencyCode=false,$latest_exchange_rate=false)
	{
		$sql='';
		if($idCurrency>0)
		{
                    $sql="WHERE
                            id ='".(int)$idCurrency."'
                    AND	
                            ";
		}		
		else if($iPricing)
		{
			if(!empty($sql))
			{
				$sql .="  iPricing ='1' AND " ;
			}
			else
			{
				$sql ="
					WHERE
						iPricing ='1'  AND
				";
			}
		}
		if($iBooking)
		{
			if(!empty($sql))
			{
				$sql .="  iBooking='1' AND" ;
			}
			else
			{
				$sql="
					WHERE
						iBooking ='1' AND
				";
			}
		}
		if($iSettling)
		{
			if(!empty($sql))
			{
				$sql .=" iSettling='1' AND" ;
			}
			else
			{
				$sql="
					WHERE
						iSettling ='1' AND
				";
			}
		}
                if(!empty($szCurrencyCode))
                {
                    if(!empty($sql))
                    {
                        $sql .=" LOWER(szCurrency) = '".mysql_escape_custom(strtolower($szCurrencyCode))."' AND" ;
                    }
                    else
                    { 
                        $sql =" WHERE LOWER(szCurrency) = '".mysql_escape_custom(strtolower($szCurrencyCode))."' AND" ;
                    }
                }
                
		$sql = trim($sql);
		if( $sql == '')
		{
                    $sql = " WHERE ";
			
		}
                if($latest_exchange_rate)
                {
                    $query_select = " , (SELECT rate.fUsdValue FROM ".__DBC_SCHEMATA_EXCHANGE_RATE__." rate WHERE rate.idCurrency = curr.id ORDER BY rate.dtDate DESC LIMIT 0,1) as fUsdValue ";
                }
		$query="
                    SELECT
                        id,
                        szCurrency
                        $query_select
                    FROM	
                        ".__DBC_SCHEMATA_CURRENCY__." curr
                        ".$sql."
                        iActive='1'
                    ORDER BY
                        szCurrency ASC				
            ";	
            //echo $query ;
            if($result=$this->exeSQL($query))
            {
                $currencyAry=array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    $currencyAry[$ctr]=$row;
                    $ctr++;
                }
                return $currencyAry ;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}	
        function getAllPalletTypes($id=false,$iLanguage=__LANGUAGE_ID_ENGLISH__)
	{
            if($id>0)
            {
                $query_and = " AND id='".(int)$id."'";
            } 
            
            
            if((int)$iLanguage==0)
            {
                $iLanguage=__LANGUAGE_ID_ENGLISH__;
            }
            
            
            $configLangArr=$this->getConfigurationLanguageData('__TABLE_PALLET_TYPE__',$iLanguage);
            
//            if($iLanguage==__LANGUAGE_ID_DANISH__)
//            {
//                $query_select = " szShortNameDanish as szShortName, szLongNameDanish as szLongName,";
//            }
//            else
//            {
//                $query_select = " szShortName, szLongName,";
//            }

            $query="
                SELECT
                    id,
                    fDefaultLength,
                    fDefaultWidth
                FROM	
                    ".__DBC_SCHEMATA_PALLET_TYPES__."
                WHERE
                    iActive = '1'
                    ".$query_and."					
            ";		
            //echo "<br>".$query."<br>" ;
            if($result=$this->exeSQL($query))
            {
                $countryAry=array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    if(!empty($configLangArr[$row['id']]))
                    {
                        $countryAry[$ctr] = array_merge($configLangArr[$row['id']],$row);
                    }else
                    {
                        $countryAry[$ctr] = $row;
                    }
                    //$countryAry[$ctr]=$row;
                    $ctr++;
                }
                $countryAry=sortArray($countryAry,'szLongName');
                return $countryAry ;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
	
	function getAllCountryInKeyValuePair($iBooking=false,$id=false,$idAry=array(),$iLanguage=__LANGUAGE_ID_ENGLISH__,$orderBY=false)
	{
            if($id>0)
            {
                $query_and = " AND c.id='".(int)$id."'";
            } 
            if(!empty($idAry))
            {
                $idAry = array_filter($idAry); 
                if(empty($idAry))
                {
                    return false;
                }
                $id_str = implode(',',$idAry);
                $query_and = " AND c.id IN (".$id_str.")";
            }
            
            if((int)$iLanguage==0)
            {
                $iLanguage=__LANGUAGE_ID_ENGLISH__;
            }
            /*
                if($iBooking)
                {
                        $query_and = " WHERE iBooking='1'";
                }
            */ 
            $qrderBy='';
            if($orderBY)
            {
                $qrderBy="ORDER BY szCountryName ASC";
            }
            $query="
                SELECT
                    c.id,
                    cnm.szName AS szCountryName,
                    szCountryISO, 
                    iInternationDialCode
                FROM	
                    ".__DBC_SCHEMATA_COUNTRY__." AS c
                INNER JOIN
                    ".__DBC_SCHEMATA_COUNTRY_NAME_MAPPING__." AS cnm
                ON
                    cnm.idCountry=c.id
                WHERE
                    cnm.idLanguage='".(int)$iLanguage."'
                    ".$query_and."
                    $qrderBy          
            ";		
            //echo "<br>".$query."<br>" ;
            if($result=$this->exeSQL($query))
            {
                $countryAry=array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    $countryAry[$row['id']]=$row;
                }
                return $countryAry ;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	} 
        function validateVogueLandingPage($data,$cargodetailAry=array(),$iSearchMiniPageVersion=false,$bTryitout=false)
        {   
            if(!empty($data))
            {   
                if($bTryitout)
                {
                    if($iSearchMiniPageVersion==8 || $iSearchMiniPageVersion==9)
                    {
                        //Validating data for Simple Search Page  
                        $this->set_szOriginCountry(sanitize_all_html_input(trim($data['szOriginCountry']))); // id country origin 
                        if((int)$data['szOriginCountry']>0)
                        {
                            $this->loadCountry($data['szOriginCountry']);  
                            if($this->iSmallCountry==1 && empty($data['szOriginCity']))
                            {
                                $data['szOriginCity'] = $this->szCountryName ;
                            }
                            if($data['szOriginCity']=='')
                            {
                                if($data['szSearchMode']=='FORWARDER_COURIER_TRY_IT_OUT')
                                {
                                    $this->addError("szOriginPostCode",t($this->t_base.'errors/city_name_is_required'));
                                }
                                else
                                {
                                    $this->addError("szOriginCountry",t($this->t_base.'errors/city_name_is_required'));
                                } 
                            }
                        } 
                        $this->set_szDestinationCountry(sanitize_all_html_input(trim($data['szDestinationCountry']))); 
                        if((int)$data['szDestinationCountry']>0)
                        {
                            $this->loadCountry($data['szDestinationCountry']);
                            if($this->iSmallCountry==1 && empty($data['szDestinationCity']))
                            {
                                $data['szDestinationCity'] = $this->szCountryName ;
                            }
                            if($data['szDestinationCity']=='')
                            {
                                if($data['szSearchMode']=='FORWARDER_COURIER_TRY_IT_OUT')
                                {
                                    $this->addError("szDestinationPostCode",t($this->t_base.'errors/city_name_is_required'));
                                }
                                else
                                {
                                    $this->addError("szDestinationCountry",t($this->t_base.'errors/city_name_is_required'));
                                }
                            }
                        } 
                        $this->set_szOriginCity(sanitize_all_html_input(trim($data['szOriginCity'])));  // city name 
                        $this->set_szOriginPostCode(sanitize_all_html_input(trim($data['szOriginPostCode'])));

                        $this->set_szDestinationCity(sanitize_all_html_input(trim($data['szDestinationCity'])));
                        $this->set_szDestinationPostCode(sanitize_all_html_input(trim($data['szDestinationPostCode'])));
                    }
                    else
                    {
                        //From admin try it out we don't have customer's details so ignored that
                        $this->set_szConsigneePostcode(sanitize_all_html_input(trim($data['szConsigneePostcode'])));
                        $this->set_szConsigneeCity(sanitize_all_html_input(trim($data['szConsigneeCity'])));
                        $this->set_idConsigneeCountry(sanitize_all_html_input(trim($data['idConsigneeCountry'])));
                    } 
                }
                else
                { 
                    $this->set_idServiceType(sanitize_all_html_input(trim($data['idServiceType']))); 
                    $this->set_szFirstName(sanitize_all_html_input(trim($data['szFirstName']))); 
                    $this->set_szLastName(sanitize_all_html_input(trim($data['szLastName']))); 
                    $this->set_szEmail(sanitize_all_html_input(trim($data['szEmail']))); 
                    $this->set_idDialCode(sanitize_all_html_input(trim($data['IDDialCode']))); 
                    $this->set_szPhone(sanitize_all_html_input(trim($data['szPhone'])));  
                    $this->set_iContactShipper(sanitize_all_html_input(trim($data['iContactShipper'])));  

                    if($iSearchMiniPageVersion==8 || $iSearchMiniPageVersion==9)
                    {
                        //Validating data for Simple Search Page  
                        $this->set_szOriginCountry(sanitize_all_html_input(trim($data['szOriginCountry']))); // id country origin 
                        if((int)$data['szOriginCountry']>0)
                        {
                            $this->loadCountry($data['szOriginCountry']);  
                            if($this->iSmallCountry==1 && empty($data['szOriginCity']))
                            {
                                $data['szOriginCity'] = $this->szCountryName ;
                            }
                            if($data['szOriginCity']=='')
                            {
                                if($data['szSearchMode']=='FORWARDER_COURIER_TRY_IT_OUT')
                                {
                                    $this->addError("szOriginPostCode",t($this->t_base.'errors/city_name_is_required'));
                                }
                                else
                                {
                                    $this->addError("szOriginCountry",t($this->t_base.'errors/city_name_is_required'));
                                } 
                            }
                        } 
                        $this->set_szDestinationCountry(sanitize_all_html_input(trim($data['szDestinationCountry']))); 
                        if((int)$data['szDestinationCountry']>0)
                        {
                            $this->loadCountry($data['szDestinationCountry']);
                            if($this->iSmallCountry==1 && empty($data['szDestinationCity']))
                            {
                                $data['szDestinationCity'] = $this->szCountryName ;
                            }
                            if($data['szDestinationCity']=='')
                            {
                                if($data['szSearchMode']=='FORWARDER_COURIER_TRY_IT_OUT')
                                {
                                    $this->addError("szDestinationPostCode",t($this->t_base.'errors/city_name_is_required'));
                                }
                                else
                                {
                                    $this->addError("szDestinationCountry",t($this->t_base.'errors/city_name_is_required'));
                                }
                            }
                        } 
                        $this->set_szOriginCity(sanitize_all_html_input(trim($data['szOriginCity'])));  // city name 
                        $this->set_szOriginPostCode(sanitize_all_html_input(trim($data['szOriginPostCode'])));

                        $this->set_szDestinationCity(sanitize_all_html_input(trim($data['szDestinationCity'])));
                        $this->set_szDestinationPostCode(sanitize_all_html_input(trim($data['szDestinationPostCode'])));
                    }
                    else
                    {
                        $this->set_szAddress(sanitize_all_html_input(trim($data['szAddress']))); 
                        $this->set_szVoguePostcode(sanitize_all_html_input(trim($data['szPostcode']))); 
                        $this->set_szVogueCity(sanitize_all_html_input(trim($data['szCity'])));
                        
                        $this->iDeliveryAddress = (int)$data['iDeliveryAddress'];
                        if($this->iDeliveryAddress==1)
                        {
                            $this->szConsigneeCompanyName = $this->szFirstName." ".$this->szLastName;  
                            $this->szConsigneeFirstName = $this->szFirstName;
                            $this->szConsigneeLastName = $this->szLastName;
                            $this->szConsigneeEmail = $this->szEmail;
                            $this->szConsigneePhone = $this->szPhone;
                            $this->idConsigneeDialCode = $this->idDialCode;
                            $this->idConsigneeCountry = $this->idDialCode;
                            $this->szConsigneeAddress = $this->szAddress;
                            $this->szConsigneePostcode = $this->szPostcode;
                            $this->szConsigneeCity = $this->szCity; 
                        }
                        else
                        {
                            $this->set_szConsigneeFirstName(sanitize_all_html_input(trim($data['szConsigneeFirstName'])));
                            $this->set_szConsigneeLastName(sanitize_all_html_input(trim($data['szConsigneeLastName'])));
                            $this->set_szConsigneeEmail(sanitize_all_html_input(trim($data['szConsigneeEmail'])));
                            $this->set_szConsigneePhone(sanitize_all_html_input(trim($data['szConsigneePhone'])));
                            $this->set_idConsigneeDialCode(sanitize_all_html_input(trim($data['IDConsigneeDialCode'])));
                            $this->set_szConsigneeAddress(sanitize_all_html_input(trim($data['szConsigneeAddress'])));
                            $this->set_szConsigneePostcode(sanitize_all_html_input(trim($data['szConsigneePostcode'])));
                            $this->set_szConsigneeCity(sanitize_all_html_input(trim($data['szConsigneeCity'])));
                            $this->set_idConsigneeCountry(sanitize_all_html_input(trim($data['idConsigneeCountry']))); 

                            /*
                            * From Voga pages we always create Private customer and for private cusomers company name should be 'First Name' 'Last Name'
                            */
                            $this->szConsigneeCompanyName = $this->szConsigneeFirstName." ".$this->szConsigneeLastName;  
                        }
                    } 
                }
                
                $cargoCategoryAry = $cargodetailAry['idCargoCategory']; 
                if(!empty($cargoCategoryAry))
                {
                    $ctr=1;
                    foreach($cargoCategoryAry as $key=>$cargoCategoryArys)
                    {   
                        $this->set_idCargoCategory(sanitize_all_html_input($cargodetailAry['idCargoCategory'][$key]),$ctr,$key,$iSearchMiniPageVersion,$bTryitout);
                        $this->set_idStandardCargoType(sanitize_all_html_input($cargodetailAry['idStandardCargoType'][$key]),$ctr,$key,$iSearchMiniPageVersion,$bTryitout); 
                        if($iSearchMiniPageVersion!=9){
                        $this->set_iVogueQuantity(sanitize_all_html_input($cargodetailAry['iQuantity'][$key]),$ctr,$key,$iSearchMiniPageVersion,$bTryitout); 
                        }
                        $ctr++;
                    }
                }  
                
                if($this->error==true)
                {
                    return false;
                }  
                $this->szPageSearchType='SIMPLEPAGE';
                $this->iSearchMiniVersion = $iSearchMiniPageVersion;
                $idDefaultLandingPage = $data['idDefaultLandingPage'];
                  
                $kExplain = new cExplain(); 
                if($iSearchMiniPageVersion==8 || $iSearchMiniPageVersion==9)
                {
                    $standardCargoAry = array();
                    $standardCargoAry = $kExplain->getAllNewLandingPageData($idDefaultLandingPage);
                
                    $this->szOriginCountryName = $data['szOriginCountryStr'];
                    $this->fOriginLatitude = $data['szOLat'];
                    $this->fOriginLongitude = $data['szOLng'];
                    
                    $this->szDestinationCountryName = $data['szDestinationCountryStr'];
                    $this->fDestinationLatitude = $data['szDLat'];
                    $this->fDestinationLongitude = $data['szDLng'];  
                    
                    $this->szCargoComodityCode = $standardCargoAry[0]['szCargoType'];
                    
                    $this->iInsuranceIncluded = $standardCargoAry[0]['iDefaultInsurance'];
                    $this->idInsuranceCurrency = $standardCargoAry[0]['idInsuranceCurrency'];
                    
                    $iPageSearchType=$standardCargoAry[0]['iPageSearchType'];
                    
                    $iShipperConsignee = 3;
                    if($this->idDialCode==$this->szOriginCountry)
                    {
                        $iShipperConsignee = 1;
                    }
                    else if($this->idDialCode==$this->szDestinationCountry)
                    {
                        $iShipperConsignee = 2;
                    } 
                    $this->iShipperConsignee  =$iShipperConsignee;
                    if($iShipperConsignee==1)
                    {
                        $this->szShipperCompanyName = $this->szFirstName." ".$this->szLastName;  
                        $this->szShipperFirstName = $this->szFirstName;
                        $this->szShipperLastName = $this->szLastName;
                        $this->szShipperEmail = $this->szEmail;
                        $this->szShipperPhone = $this->szPhone;
                        $this->idShipperDialCode = $this->idDialCode;
                        $this->idShipperCountry = $this->idDialCode;
                        
                        $this->szShipperAddress = $this->szAddress;
                        $this->szShipperPostcode = $this->szOriginPostCode;
                        $this->szShipperCity = $this->szOriginCity; 
                    }
                    else if($iShipperConsignee==2)
                    {
                        $this->szConsigneeCompanyName = $this->szFirstName." ".$this->szLastName;  
                        $this->szConsigneeFirstName = $this->szFirstName;
                        $this->szConsigneeLastName = $this->szLastName;
                        $this->szConsigneeEmail = $this->szEmail;
                        $this->szConsigneePhone = $this->szPhone;
                        $this->idConsigneeDialCode = $this->idDialCode;
                        $this->idConsigneeCountry = $this->idDialCode;
                        
                        $this->szConsigneeAddress = $this->szAddress;
                        $this->szConsigneePostcode = $this->szDestinationPostCode;
                        $this->szConsigneeCity = $this->szDestinationCity; 
                    } 
                }
                else
                {
                    $standardCargoAry = array();
                    $standardCargoAry = $kExplain->getStandardShipperDetails($idDefaultLandingPage);
                
                    /*
                    * Building Shipment origin address details
                    */
                    $kConfig_new  = new cConfig();
                    $kConfig_new->loadCountry($standardCargoAry['idShipperCountry']); 
                    $szOringinCountryName = $kConfig_new->szCountryName ; 
                    $szOriginCountryStr = $standardCargoAry['szShipperPostcode'].", ".$standardCargoAry['szShipperCity'].", ".$szOringinCountryName;

                    $this->szOriginCountry = $standardCargoAry['idShipperCountry'];
                    $this->szOriginCity = $standardCargoAry['szShipperCity'];
                    $this->szOriginPostCode = $standardCargoAry['szShipperPostcode'];
                    $this->szOriginCountryName = $szOriginCountryStr;
                    
                    $originAddressLatLong = reverse_geocode($szOriginCountryStr);
                  
                    $this->fOriginLatitude = $originAddressLatLong['szLat'];
                    $this->fOriginLongitude = $originAddressLatLong['szLng'];

                    /*
                    * Building Shipment origin address details
                    */
                    $kConfig_new  = new cConfig();
                    $kConfig_new->loadCountry($this->idConsigneeCountry); 
                    $szDestCountryName = $kConfig_new->szCountryName ; 
                    $this->szDestinationCountry = $this->idConsigneeCountry;
                    $this->szDestinationCity = $this->szConsigneeCity;
                    $this->szDestinationPostCode = $this->szConsigneePostcode;
                    $this->szDestinationAddress = $this->szConsigneeAddress; 
                    $this->szDestinationCountryName = $this->szConsigneePostcode.", ".$this->szConsigneeCity.", ".$szDestCountryName; 
 
                    $destinationAddressLatLong = reverse_geocode($this->szDestinationCountryName);

                    $this->fDestinationLatitude = $destinationAddressLatLong['szLat'];
                    $this->fDestinationLongitude = $destinationAddressLatLong['szLng']; 
                } 

                $this->iDeliveryAddress = (int)$data['iDeliveryAddress'];  
                $kCourierServices = new cCourierServices();
                if(!$kCourierServices->checkFromCountryToCountryExistsForCC($data['szOriginCountry'],$data['szDestinationCountry']))
                {
                    $this->iOrigingCC = 1;
                    $this->iDestinationCC = (int)$data['idCC'][1];
                    
                }
                    
                $szBookingRandomNum = trim($data['szBookingRandomNum']); 
                if(empty($szBookingRandomNum))
                {
                    $kBooking_new = new cBooking();
                    $szBookingRandomNum = $kBooking_new->getUniqueBookingKey(10);
                    $szBookingRandomNum = $kBooking_new->isBookingRandomNumExist($szBookingRandomNum);
                }
                $this->szBookingRandomNum = $szBookingRandomNum ;
                $this->iDoNotKnowCargo = (int)$data['iDoNotKnowCargo'];
                $this->idTimingType = (int)$data['idTimingType'];
                $this->iBookingQuote = (int)$data['iBookingQuote'];
                $this->szPageSearchType=='SIMPLEPAGE';
                 
                if(!empty($this->idCargoCategory))
                {
                    $ctr = 1;
                    $fTotalVolume = 0;
                    $fTotalWeight = 0;
                    $szInternalComments = '';
                    $szCargoDescription = '';
                    $iLanguage = getLanguageId();
                    $bCargoavailableForAllLine = false;
                    $iUpdateWeightToZeroFlag=false;
                    $iUpdateVolumeToZeroFlag=false;
                    $iShowCourierPriceFlag=0;
                    foreach($this->idCargoCategory as $key=>$idCargoCategory)
                    {
                        $idStandardCargoType = $this->idStandardCargoType[$key];
                        $iQunatity = $this->iQuantity[$key];
                        if($idStandardCargoType>0 && $idCargoCategory>0)
                        { 
                            $standardCargoAry = array();
                            $standardCargoAry = $this->getStandardCargoList($idCargoCategory,$iLanguage,$idStandardCargoType); 
                            
                            if(!empty($standardCargoAry))
                            {
                                foreach($standardCargoAry as $standardCargoArys)
                                { 
                                    if($iSearchMiniPageVersion==8 || $iSearchMiniPageVersion==9)
                                    {
                                        if(($standardCargoArys['fVolume']>0 || ($standardCargoArys['fHeight']>0 && $standardCargoArys['fWidth']>0 && $standardCargoArys['fLength']>0))&& $standardCargoArys['fWeight']>0   && $iEmptyRowFound!=1)
                                        { 
                                            $bCargoavailableForAllLine = true;
                                        }
                                        else
                                        { 
                                            $bCargoavailableForAllLine = false;
                                            $iEmptyRowFound = 1;
                                        }
                                    }
                                    else
                                    {
                                        if($standardCargoArys['fVolume']>0 && $standardCargoArys['fWeight']>0 && $standardCargoArys['fHeight']>0 && $standardCargoArys['fWidth']>0 && $standardCargoArys['fLength']>0  && $iEmptyRowFound!=1)
                                        { 
                                            $bCargoavailableForAllLine = true;
                                        }
                                        else
                                        { 
                                            $bCargoavailableForAllLine = false;
                                            $iEmptyRowFound = 1;
                                        }
                                    }
                                    
                                    $this->iLength[$ctr] = $standardCargoArys['fLength'];
                                    $this->iWidth[$ctr] = $standardCargoArys['fWidth'];
                                    $this->iHeight[$ctr] = $standardCargoArys['fHeight'];
                                    $this->iWeight[$ctr] = $standardCargoArys['fWeight'];  
                                    $this->szCommodity[$ctr] = $standardCargoArys['szLongName'];   
                                    $this->szSellersReference[$ctr] = $standardCargoArys['szSellerReference'];
                                    $this->iColli[$ctr] = $standardCargoArys['iColli'] * $iQunatity; 
                                    $this->fTotalVolumePerCargo[$ctr] = $standardCargoArys['fVolume']* $iQunatity; 
                                    $this->iVolumePerColli[$ctr] = $standardCargoArys['fVolume']; 
                                    
                                   // echo $iPageSearchType."iPageSearchType";
                                    //if($iPageSearchType=='5')
                                    //{
                                        //echo $standardCargoArys['fWeight']."fWeight";
                                        //echo $standardCargoArys['fVolume']."fVolume";
                                        if($standardCargoArys['fWeight']==0 )
                                        {
                                            $iUpdateWeightToZeroFlag=true;
                                        }
                                        if( $standardCargoArys['fVolume']==0)
                                        {
                                            $iUpdateVolumeToZeroFlag=true;
                                        }
                                    //}
                                    
                                    $this->idCargoMeasure[$ctr] = 1; //cm
                                    $this->idWeightMeasure[$ctr] = 1; //kg
                                    $this->iQuantity[$ctr] = $iQunatity;
                                    $this->idStandardCargoType[$ctr] = $this->idStandardCargoType[$key];
                                    $this->idCargoCategory[$ctr] = $this->idCargoCategory[$key];
                                    
                                    $lenWidthHeightStr='';
                                    if((float)$this->iLength[$ctr]==0.00 && (float)$this->iWidth[$ctr]==0.00 && (float)$this->iHeight[$ctr]==0.00)
                                    {
                                        $iShowCourierPriceFlag=1;
                                    }
                                    if((float)$this->iLength[$ctr]>0.00 || (float)$this->iWidth[$ctr]>0.00 || (float)$this->iHeight[$ctr]>0.00)
                                    {
                                       $lenWidthHeightStr = format_numeric_vals($this->iLength[$ctr]).'x'.format_numeric_vals($this->iWidth[$ctr]).'x'.format_numeric_vals($this->iHeight[$ctr]).'cm, ';
                                    }   
                                    $weightStr='';
                                   // if((int)$this->iWeight[$ctr]>0)
                                    //{
                                       $weightStr = format_numeric_vals($this->iWeight[$ctr]).'kg ';
                                    //}
                                    
                                    $szCDQuantityText='';   
                                    $szICQuantityText='';   
                                    if((int)$this->iQuantity[$key]>0)
                                    {
                                        $szCDQuantityText =$this->iQuantity[$key].' stk ';
                                        $szICQuantityText = $this->iQuantity[$key].' pcs: ';
                                    }
                                    $fTotalVolume += $standardCargoArys['fVolume'] * $iQunatity;
                                    $fTotalWeight += $standardCargoArys['fWeight'] * $iQunatity;
                                    $szInternalComments .= $szICQuantityText.$this->szCommodity[$ctr].', '.$lenWidthHeightStr.''.$weightStr.''.PHP_EOL;
                                    $szCargoDescription .= $szCDQuantityText.$this->szCommodity[$ctr].', ';
                                    
                                    $szVogaLogs = "Length: ".$this->iLength[$ctr]."".PHP_EOL;
                                    $szVogaLogs .= "Width: ".$this->iWidth[$ctr]."".PHP_EOL;
                                    $szVogaLogs .= "Height: ".$this->iHeight[$ctr]."".PHP_EOL;
                                    $szVogaLogs .= "Weight: ".$this->iWeight[$ctr]."".PHP_EOL;
                                    $szVogaLogs .= "Quantity: ".$this->iQuantity[$ctr]."".PHP_EOL; 
                                    $szVogaLogs .= "Total Volume: ".$fTotalVolume."".PHP_EOL;
                                    $szVogaLogs .= "Total Weight: ".$fTotalWeight."".PHP_EOL; 
                                }
                            }
                            $ctr++;
                        }  
                    }
                    if($bCargoavailableForAllLine)
                    { 
                        $kExplain = new cExplain();
                        $standardQuotePricingAry = array();
                        $standardQuotePricingAry = $kExplain->getStandardQuotePriceByPageID($idDefaultLandingPage);  

                        $standardHandlerAry = array();
                        $standardHandlerAry = $kExplain->getQuoteHandlerByPageID($idDefaultLandingPage,true); 
                        if(!empty($standardHandlerAry) && !empty($standardQuotePricingAry))
                        {
                            $this->iVogaSearchAutomatic = true;
                        }
                        else
                        {
                            $this->iVogaSearchAutomatic = false;
                        } 
                    } 
                    if($iUpdateWeightToZeroFlag)
                    {
                        $fTotalWeight=0;
                        $this->iUpdateWeightToZeroFlag = $iUpdateWeightToZeroFlag;
                    }
                    if($iUpdateVolumeToZeroFlag)
                    {
                        $fTotalVolume=0;
                        $this->iUpdateVolumeToZeroFlag = $iUpdateVolumeToZeroFlag;
                    }
                    //echo $this->iUpdateVolumeWeightToZeroFlag."iUpdateVolumeWeightToZeroFlag";
                    
                    $this->fTotalWeight = $fTotalWeight;
                    $this->fTotalVolume = $fTotalVolume;
                    $this->szInternalComments = $szInternalComments;
                    $szCargoDescription = trim($szCargoDescription);
                    $this->szCargoDescription = rtrim($szCargoDescription,',');  
                    $this->iShowCourierPriceFlag=$iShowCourierPriceFlag;
                } 
                return true;
            }
        }
        
        function createQuoteEmailForTryitout($postSearchAry,$searchResultAry,$idDefaultLandingPage)
        { 
            //TO DO  
            $quotesPricingAry = array();
            $quotesPricingAry = $this->createQuoteForVogaBooking($postSearchAry,$searchResultAry,$idDefaultLandingPage,true,1);
                  
            if(!empty($quotesPricingAry))
            { 
                $iBookingLanguage = $postSearchAry['iBookingLanguage']; 
                $langArr=$this->getLanguageDetails('',$iBookingLanguage);
                if(!empty($langArr))
                {
                    $szCbmText = $langArr[0]['szDimensionUnit'];
                }
                else
                {
                    $szCbmText = 'cbm';
                } 

                $kConfig_new = new cConfig();
                $kConfig_new->loadCountry($postSearchAry['idOriginCountry'],false,$iBookingLanguage);
                $szOriginCountryName = $kConfig_new->szCountryName;

                $kConfig_new = new cConfig();
                $kConfig_new->loadCountry($postSearchAry['idDestinationCountry'],false,$iBookingLanguage);
                $szDestinationCountryName = $kConfig_new->szCountryName;
            
                $returnAry = array();
                $returnAry['szFromPostcode'] = $postSearchAry['szOriginPostCode'];
                $returnAry['szFromCity'] = $postSearchAry['szOriginCity'];
                $returnAry['szFromCountry'] = $szOriginCountryName;
                $returnAry['szToPostcode'] = $postSearchAry['szDestinationPostCode'];
                $returnAry['szToCity'] = $postSearchAry['szDestinationCity'];
                $returnAry['szToCountry'] = $szDestinationCountryName;
                $returnAry['szCargoDescription'] = $postSearchAry['szCargoDescription'];
                $returnAry['szVolume'] = format_volume($postSearchAry['fCargoVolume'],$iBookingLanguage).$szCbmText;
                $returnAry['szWeight'] = number_format_custom((float)$postSearchAry['fCargoWeight'],$iBookingLanguage)."kg";
                $returnAry['szFirstName'] = $postSearchAry['szFirstName'];
                $returnAry['szLastName'] = $postSearchAry['szLastName'];

                $returnAry['szLastName'] = $postSearchAry['szLastName'];

                if($postSearchAry['idTransportMode']==4)//courier 
                { 
                    $szVolWeight = format_volume($postSearchAry['fCargoVolume'],$iBookingLanguage).$szCbmText.", ".number_format_custom((float)$postSearchAry['fCargoWeight'],$iBookingLanguage)."kg,  ".number_format_custom((int)$postSearchAry['iNumColli'],$iBookingLanguage)."col";
                }
                else
                {
                    $szVolWeight = format_volume($postSearchAry['fCargoVolume'],$iBookingLanguage).$szCbmText.",  ".number_format_custom((float)$postSearchAry['fCargoWeight'],$iBookingLanguage)."kg ";
                }
             
                $kQuote = new cQuote();
                $quoteDataAry = $kQuote->createQuoteOfferTexts($quotesPricingAry,$postSearchAry,true);

                $returnAry['szValidTo'] = $quoteDataAry['szValidTo'];
                $returnAry['szQuote'] = $quoteDataAry['szQuote'];
                //$returnAry = array();
                //$returnAry = $this->createQuoteEmailMessage($postSearchAry,$quotesPricingAry,true);
                   
                if((int)$postSearchAry['iCreateAutomaticQuote']==1)
                {
                    $kExplain = new cExplain();
                    $quoteHandlerAry = $kExplain->getQuoteHandlerByPageID($idDefaultLandingPage,'',true);
                }
                else
                {
                    $kExplain = new cExplain();
                    $quoteHandlerAry = $kExplain->getQuoteHandlerByPageID($idDefaultLandingPage);
                }    
                $szQuoteEmailBody = $quoteHandlerAry['szQuoteEmailBody'];
                $szQuoteEmailSubject = $quoteHandlerAry['szQuoteEmailSubject']; 
                $idFileOwner = $quoteHandlerAry['idFileOwner'];
                
                $kAdmin = new cAdmin();
                $kAdmin->getAdminDetails($idFileOwner);

                $kConfig = new cConfig();
                $kConfig->loadCountry($kAdmin->idInternationalDialCode);
                $iInternationDialCode = $kConfig->iInternationDialCode;
                $szMobile = "+".$iInternationDialCode." ".$kAdmin->szPhone; 
                
                unset($returnAry['szFirstName']);
                unset($returnAry['szLastName']);
                unset($returnAry['szEmail']);
                
                $kWhsSearch=new cWHSSearch();
//                if($iBookingLanguage ==__LANGUAGE_ID_DANISH__)
//                {
//                   $szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE_DANISH__');
//                }
//                else
//                {
//                    $szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__');
//                } 
                $szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__',$iBookingLanguage);
                $returnAry['szAdminName'] = $kAdmin->szFirstName." ".$kAdmin->szLastName ;
                $returnAry['szTitle'] = $kAdmin->szTitle ;
                $returnAry['szOfficePhone'] = $szCustomerCareNumer ;
                $returnAry['szMobile'] = $szMobile;
                $returnAry['szWebSiteUrl'] = remove_http_from_url(__MAIN_SITE_HOME_PAGE_URL__);
                $returnAry['szCargoDescription'] = $returnAry['szCargoDescription'];
                
                if (!empty($returnAry))
                {
                    foreach ($returnAry as $replace_key => $replace_value)
                    { 
                        $szQuoteEmailBody = str_replace($replace_key, $replace_value, $szQuoteEmailBody);
                        $szQuoteEmailSubject= str_replace($replace_key, $replace_value, $szQuoteEmailSubject);
                    }
                }  

                $szEmailBody = $szQuoteEmailBody;
                $szEmailSubject = $szQuoteEmailSubject;
                
                $ret_ary = array();
                $ret_ary['szQuoteEmailSubject'] = $szEmailSubject;
                $ret_ary['szQuoteEmailBody'] = nl2br($szEmailBody);
                return $ret_ary;
            }  
        }
        
        function createQuoteForVogaBooking($postSearchAry,$searchResultAry,$idDefaultLandingPage,$bTryitout=false,$iNumattempt=false)
        { 
            $kExplain = new cExplain();
            $kBooking = new cBooking(); 
            if((int)$postSearchAry['iCreateAutomaticQuote']==1)
            {
                $standardQuotePricingAry = array();
                $standardQuotePricingAry = $kExplain->getStandardQuotePriceByPageID($idDefaultLandingPage,false,0,false,true);  

                $standardHandlerAry = array();
                $standardHandlerAry = $kExplain->getQuoteHandlerByPageID($idDefaultLandingPage,true,true);
            }
            else
            {
                $standardQuotePricingAry = array();
                $standardQuotePricingAry = $kExplain->getStandardQuotePriceByPageID($idDefaultLandingPage);  

                $standardHandlerAry = array();
                $standardHandlerAry = $kExplain->getQuoteHandlerByPageID($idDefaultLandingPage,true); 
            }
            
            $iQuoteValidity = $standardHandlerAry['iQuoteValidity']; 
            $idFileOwner = $standardHandlerAry['idFileOwner'];
            
            $forwarderPricingAry = array();
            $forwarderPricingAry = $this->processSearchResults($postSearchAry,$searchResultAry);
            
            
            $idBooking = $postSearchAry['id'];
            $idBookingFile = $postSearchAry['idFile'];
            $idServiceTerms = $postSearchAry['idServiceTerms'];
            
            if($postSearchAry['iRemovePriceGuarantee']==1)
            {
                $iRemovePriceGuarantee = 2;
            }
            else
            {
                $iRemovePriceGuarantee = 1;
            } 
            $kConfig = new cConfig(); 
            $kCourierServices = new cCourierServices();
            $bDTDTrades = false;
            if($kCourierServices->checkFromCountryToCountryExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true))
            { 
               $bDTDTrades = true;
            }   
            $vogaQuoteAdded=false;
            $kQuote = new cQuote();
            $usedPricesAry = array();
            $bNoServiceFound = true;
            //$bookingQuoteDetailsAry = array();
            $ctr = 0; 
            $bLclQuoteFound = false;
            $bCourierQuoteFound = false;
            
            $bLclProductAddedForVogaPage = false;
            $quoteSearchAry = array();
            if($standardQuotePricingAry)
            {
                foreach($standardQuotePricingAry as $standardQuotePricingArys)
                {
                    $searchResults = array();
                    if($standardQuotePricingArys['iBookingType']==1)
                    { 
                        /*
                        * Flag $bLclProductAddedForVogaPage will set to true if Managment has added LCL for the selected page on 'Standard Cargo Pricing (/standardCargoPricing/)' page
                        */
                        $bLclProductAddedForVogaPage = true;
                        $key = $standardQuotePricingArys['idForwarder']; 
                        if(!empty($usedPricesAry) && in_array($key,$usedPricesAry))
                        {
                            continue;
                        }
                        else
                        {
                            $usedPricesAry[] = $key;
                        }
                        if(!empty($forwarderPricingAry[$key]['lcl']))
                        {
                            $searchResults = $forwarderPricingAry[$key]['lcl'];  
                            $bNoServiceFound = false;
                            $bLclQuoteFound = true;
                            if($bDTDTrades)
                            { 
                                $searchResults['idTransportMode'] = __BOOKING_TRANSPORT_MODE_ROAD__ ;
                            }
                            else
                            { 
                                $searchResults['idTransportMode'] = __BOOKING_TRANSPORT_MODE_SEA__ ;
                            } 
                            $searchResults['iBookingType'] = 1; //LCL
                        } 
                    }
                    else if($standardQuotePricingArys['iBookingType']==2)
                    { 
                        $key = $standardQuotePricingArys['idForwarder']."_".$standardQuotePricingArys['idCourierProvider']."_".$standardQuotePricingArys['idCourierProviderProduct'];
                     
                        $bNoServiceFound = false;
                        if(!empty($usedPricesAry) && in_array($key,$usedPricesAry))
                        {
                            continue;
                        }
                        else
                        {
                            $usedPricesAry[] = $key;
                        }
                        if(!empty($forwarderPricingAry[$key]['courier']))
                        {  
                            $searchResults = $forwarderPricingAry[$key]['courier'];   
                            $searchResults['idTransportMode'] = __BOOKING_TRANSPORT_MODE_COURIER__ ;
                            $searchResults['iBookingType'] = 2; //LCL
                            $bCourierQuoteFound = true;
                        } 
                    } 
                    if(!empty($searchResults))
                    { 
                        $vogaQuoteAdded=true;
                        $searchResults['idForwarder'] = $standardQuotePricingArys['idForwarder'];
                        $searchResults['szTransitTime'] = $standardQuotePricingArys['szTransitTime'];
                        $searchResults['szTransitTime'] = $standardQuotePricingArys['szTransitTime'];
                        $quoteSearchAry[] = $searchResults;
                    }
                }   
                if(!empty($this->foundServicesAry) && !empty($quoteSearchAry))
                {
                    $this->foundServicesAry = array_merge($this->foundServicesAry,$quoteSearchAry);
                }
                else if(!empty($quoteSearchAry))
                {
                    $this->foundServicesAry = $quoteSearchAry;
                }  
                if($iNumattempt==1 && !$bLclQuoteFound && $bLclProductAddedForVogaPage)
                { 
                    /*
                     * if user on a standard cargo landing page, e.g. https://www.transporteca.com/voga, makes a search where postcode and city does not match, 
                     * then we do not give a price. Could we include, when we get the price, that if, and only if, we do not get a price back, 
                     * then we make one more request to get a price based on the postcode alone (without city name).
                     * This should be for both courier and LCL/LTL services. Reason is that in our standard cargo landing pages we do not use the google drop down, so user can type in e.g. the right postcode, but a city name spelled different from what is in Google, and in that case, today we do not send a price. But if postcode is correct, then we would like to send a price anyway, based on that postcode.
                    */
                    $kConfig_new  = new cConfig();
                    $kConfig_new->loadCountry($postSearchAry['idDestinationCountry']); 
                    $szDestCountryName = $kConfig_new->szCountryName ;

                    $szDestinationCountryName = $postSearchAry['szDestinationPostCode'].", ".$szDestCountryName; 

                    $destinationAddressLatLong = reverse_geocode($szDestinationCountryName);

                    $postSearchAry['fDestinationLatitude'] = $destinationAddressLatLong['szLat'];
                    $postSearchAry['fDestinationLongitude'] = $destinationAddressLatLong['szLng']; 

                    $kWHSSearch = new cWHSSearch();
                    $searchResultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,false,$idBooking,true); 
                    $this->szCalculationLogString = "<h3>Attempt 2</h3>";
                    $this->szCalculationLogString .= $kWHSSearch->szCourierCalculationLogString;
                    if(!empty($searchResultAry))
                    {
                        $this->createQuoteForVogaBooking($postSearchAry,$searchResultAry,$idDefaultLandingPage,$bTryitout,2);
                        if(!$bTryitout)
                        {
                            return false;
                        } 
                    }  
                }    
            } 
            
            $searchResultAry = $this->foundServicesAry;  
            $counterHC=0;
            $vogaQuoteFound = false;
            $filename = __APP_PATH_ROOT__."/logs/voga_page_data_file.log";
            if(!empty($searchResultAry))
            { 
                foreach($searchResultAry as $searchResults)
                {
                    
                    
                    /*
                    *  Following if will make sure we only add 1 quote for 1 product type
                    */
                    $vogaQuoteFound = true;
                    $idTransportMode = $searchResults['idTransportMode'];
                    $vogaQuoteAdded=true;
                    $transportModeListAry = array();
                    $transportModeListAry = $kConfig->getAllTransportMode($idTransportMode);

                    $szTransportMode = $transportModeListAry[0]['szShortName'];

                    $addVogaQuoteAry = array();
                    $addVogaQuoteAry['idBooking'] = $postSearchAry['id'];
                    $addVogaQuoteAry['idBookingFile'] = $postSearchAry['idFile'];
                    $addVogaQuoteAry['idTransportMode'] = $idTransportMode;
                    $addVogaQuoteAry['szTransportMode'] = $szTransportMode;
                    $addVogaQuoteAry['idForwarder'] = $searchResults['idForwarder']; 
                    
                    
                    if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW__)
                    {
                        $addVogaQuoteAry['szHandoverCity'] = $postSearchAry['szOriginCity'];
                    }
                    else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW_WTD__  || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_FOB__)
                    {
                        $addVogaQuoteAry['szHandoverCity'] = $searchResults['szFromWHSCity'];
                    }
                    else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_DAP__  || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_DAP_NO_CC__)
                    {
                        $addVogaQuoteAry['szHandoverCity'] = $postSearchAry['szDestinationCity'];
                    }
                    else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_CFR__)
                    {
                        $addVogaQuoteAry['szHandoverCity'] = $searchResults['szToWHSCity'];
                    }
                    
                   // echo $addVogaQuoteAry['szHandoverCity']."szHandoverCity";
                    if($counterHC==0)
                    {
                        $szHandoverCity=$addVogaQuoteAry['szHandoverCity'];
                    }
                    ++$counterHC;
                    if($bTryitout)
                    { 
                        $bookingQuoteDetailsAry[$ctr] = $addVogaQuoteAry; 
                    }
                    else
                    {
                        $idVogaQuote = $kQuote->addVogaAutomaticQuote($addVogaQuoteAry);
                    } 
                    if($idVogaQuote>0 || $bTryitout)
                    { 
                        
                        if($searchResults['iHandlingFeeApplicable']==1)
                        {
                            $fHandlingFeeForwarderCurrency = 0;
                            if($searchResults['idForwarderCurrency']==1) //USD
                            {
                                $fHandlingFeeForwarderCurrency = $searchResults['fTotalHandlingFeeUSD'];
                            }
                            else if($searchResults['fForwarderCurrencyExchangeRate']>0)
                            {
                                $fHandlingFeeForwarderCurrency = round((float)($searchResults['fTotalHandlingFeeUSD']/$searchResults['fForwarderCurrencyExchangeRate']),4);
                            } 
                            
                            if($searchResults['idTransportMode']=='4')
                            {
                                $searchResults['fUsdValue']=$searchResults['fExchangeRate'];
                            }
                            
                           
                            $fHandlingFeeCustomerCurrency = 0;
                            if($searchResults['idCurrency']==1) //USD
                            {
                                $fHandlingFeeCustomerCurrency = $searchResults['fTotalHandlingFeeUSD'];
                            }
                            else if($searchResults['fUsdValue']>0)
                            {
                                $fHandlingFeeCustomerCurrency = round((float)($searchResults['fTotalHandlingFeeUSD']/$searchResults['fUsdValue']),4);
                            }
                            
                            $searchResults['fForwarderCurrencyPrice_XE'] = $searchResults['fForwarderCurrencyPrice_XE'] - $fHandlingFeeForwarderCurrency;  
                        } 
                        if($searchResults['idForwarderCurrency']==$searchResults['idCurrency'])
                        {
                            /*
                            * If customer currency and forwarder currency are same then we round off both the prices
                            */
                            $searchResults['fForwarderCurrencyPrice_XE'] = round((float)$searchResults['fForwarderCurrencyPrice_XE']);
                            $searchResults['fDisplayPrice'] = round((float)$searchResults['fDisplayPrice']);
                        }
                        $addVogaQuotePricingAry = array();
                        $addVogaQuotePricingAry = $addVogaQuoteAry;
                        $addVogaQuotePricingAry['idBookingQuote'] = $idVogaQuote;
                        $addVogaQuotePricingAry['fTotalPriceForwarderCurrency'] = $searchResults['fForwarderCurrencyPrice_XE'];
                        $addVogaQuotePricingAry['idForwarderCurrency'] = $searchResults['idForwarderCurrency'];
                        $addVogaQuotePricingAry['szForwarderCurrency'] = $searchResults['szForwarderCurrency'];
                        $addVogaQuotePricingAry['fForwarderExchangeRate'] = $searchResults['fForwarderCurrencyExchangeRate']; 

                        $fVATPercentage = $searchResults['fVATPercentage'];
                        $fTotalPriceForwarderCurrency = round((float)$addVogaQuotePricingAry['fTotalPriceForwarderCurrency']);
                        $fTotalVatForwarder = round((float)($fTotalPriceForwarderCurrency * $fVATPercentage * .01),2);
                        $addVogaQuotePricingAry['fTotalVatForwarder'] = $fTotalVatForwarder;
                        $addVogaQuotePricingAry['fVATPercentage'] = $fVATPercentage;

                        $addVogaQuotePricingAry['dtQuoteValidTo'] = date('Y-m-d',strtotime("+".$iQuoteValidity." DAYS"));
                        $addVogaQuotePricingAry['szTransitTime'] = $searchResults['szTransitTime'];
                        $addVogaQuotePricingAry['szForwarderComment'] = ''; 
                        
                        $fHandlingFeeCustomerCurrencyMarkup=0;
                        $fQuotePriceCustomerCurrency=0;
                        if($searchResults['idForwarderCurrency']==$searchResults['idCurrency'])
                        {
                            $fQuotePriceCustomerCurrency =$searchResults['fForwarderCurrencyPrice_XE'];
                        }
                        else
                        {
                            if((float)$searchResults['fMarkupPercentage']>0){
                                $fHandlingFeeCustomerCurrencyMarkup = round((float)($fHandlingFeeCustomerCurrency * $searchResults['fMarkupPercentage'] * .01),4);
                            }
                            
                            $fForwarderCurrencyPrice_XE = $searchResults['fForwarderCurrencyPrice_XE'] * (1+$searchResults['fMarkupPercentage']/100);
                            
                            $fForwarderCurrencyPrice_XEUSD = $fForwarderCurrencyPrice_XE * $searchResults['fForwarderCurrencyExchangeRate'];
                            
                            $fQuotePriceCustomerCurrency = $fForwarderCurrencyPrice_XEUSD/$searchResults['fUsdValue'];
                            
                        }
                        
                        
                        if($searchResults['idForwarderCurrency']==1)
                        {
                            $fForwarderCurrencyPrice_XEUSDForReferalFee = $searchResults['fForwarderCurrencyPrice_XE'];
                        }
                        else
                        {
                            $fForwarderCurrencyPrice_XEUSDForReferalFee = $searchResults['fForwarderCurrencyPrice_XE'] * $searchResults['fForwarderCurrencyExchangeRate'];
                        }
                        
                        
                        /*
                         * Change On  15-Feb-2017 For Calculating the Referal Fee In Customer Currency
                        */                        
                        $fPriceExcludingManualFeeUSD = $fForwarderCurrencyPrice_XEUSDForReferalFee;
                        /*
                        * Referal amount is calculated based on Forwarder price + Forwarder VAT excluding handling fee
                        */
                        $fReferalAmount_hiddenUSD = (($fPriceExcludingManualFeeUSD) * $searchResults['fReferalPercentage'] * 0.01);
                        if($searchResults['fUsdValue']>0)
                        {
                            $fReferalAmount_hidden = ($fReferalAmount_hiddenUSD/$searchResults['fUsdValue']);
                        } 
                        else
                        {
                            $fReferalAmount_hidden = 0;
                        }
                        $fReferalAmount = round((float)$fReferalAmount_hidden);
                         
                         
                         
                        $addVogaQuotePricingAry['fReferalPercentage'] = $searchResults['fReferalPercentage']; 
                        $addVogaQuotePricingAry['fReferalAmount'] = $fReferalAmount; 
                        $addVogaQuotePricingAry['fReferalAmount_hidden'] = $fReferalAmount_hidden; 
                        $addVogaQuotePricingAry['fTotalPriceCustomerCurrency'] = $searchResults['fDisplayPrice']; 
                        $addVogaQuotePricingAry['fQuotePriceCustomerCurrency'] = $fQuotePriceCustomerCurrency;  
                        $addVogaQuotePricingAry['idCustomerCurrency'] = $searchResults['idCurrency'];
                        $addVogaQuotePricingAry['idForwarderAccountCurrency'] = $addVogaQuotePricingAry['idForwarderCurrency'];
                        $addVogaQuotePricingAry['szForwarderAccountCurrency'] = $addVogaQuotePricingAry['szForwarderCurrency'];
                        $addVogaQuotePricingAry['fForwarderAccountExchangeRate'] = $addVogaQuotePricingAry['fForwarderExchangeRate'];
                        $addVogaQuotePricingAry['fTotalPriceForwarderAccountCurrency'] = $addVogaQuotePricingAry['fTotalPriceForwarderCurrency'];
                        $addVogaQuotePricingAry['fTotalVatForwarderAccountCurrency'] = $addVogaQuotePricingAry['fTotalVatForwarder'];  
                        $addVogaQuotePricingAry['fTotalVatCustomerCurrency'] = $searchResults['fTotalVat'];
                        $addVogaQuotePricingAry['iQuoteSentToCustomer'] = 1;

                        $addVogaQuotePricingAry['iProductType'] = $searchResults['iProductType']; 
                        $addVogaQuotePricingAry['idCourierProvider'] = $searchResults['idCourierProvider'];
                        $addVogaQuotePricingAry['idCourierProviderProduct'] = $searchResults['idCourierProviderProduct'];
                        $addVogaQuotePricingAry['iRemovePriceGuarantee'] = $iRemovePriceGuarantee;
                         
                        if($searchResults['iHandlingFeeApplicable']==1)
                        {
                            $addVogaQuotePricingAry['iHandlingFeeApplicable'] = 1;
                            
                            $addVogaQuotePricingAry['idHandlingCurrency'] = $searchResults['idHandlingCurrency'];
                            $addVogaQuotePricingAry['szHandlingCurrency'] = $searchResults['szHandlingCurrencyName'];
                            $addVogaQuotePricingAry['fHandlingCurrencyExchangeRate'] = $searchResults['fHandlingCurrencyExchangeRate'];
                            $addVogaQuotePricingAry['fHandlingFeePerBooking'] = $searchResults['fHandlingFeePerBooking'];
                            $addVogaQuotePricingAry['fTotalHandlingFeeCustomerCurrencyMarkup'] = $fHandlingFeeCustomerCurrencyMarkup;

                            $addVogaQuotePricingAry['fHandlingMarkupPercentage'] = $searchResults['fHandlingMarkupPercentage'];
                            $addVogaQuotePricingAry['idHandlingMinMarkupCurrency'] = $searchResults['idHandlingMinMarkupCurrency'];
                            $addVogaQuotePricingAry['szHandlingMinMarkupCurrency'] = $searchResults['szHandlingMinMarkupCurrencyName'];
                            $addVogaQuotePricingAry['fHandlingMinMarkupCurrencyExchangeRate'] = $searchResults['fHandlingMinMarkupCurrencyExchangeRate'];
                            $addVogaQuotePricingAry['fHandlingMinMarkupPrice'] = $searchResults['fHandlingMinMarkupPrice'];  
                            $addVogaQuotePricingAry['fTotalHandlingFeeUSD'] = $searchResults['fTotalHandlingFeeUSD'];
                            $addVogaQuotePricingAry['iAddHandlingFee'] = 1;
                        }
                        else
                        {
                            $addVogaQuotePricingAry['iHandlingFeeApplicable'] = 0;
                        }  
                        if($bTryitout)
                        { 
                            $bookingQuoteDetailsAry[$ctr] = array_merge($bookingQuoteDetailsAry[$ctr],$addVogaQuotePricingAry); 
                        }
                        else
                        {
                            $addVogaQuotePricingAry['iCheckForVatPercentage'] = 1;
                            $kQuote->addVogaQuotePricing($addVogaQuotePricingAry); 
                        }
                    }
                    $ctr++;
                } 
            } 
            if($bTryitout)
            {   
                return $bookingQuoteDetailsAry;
            } 

            $res_ary = array();
            $res_ary['idTransportMode'] = $idTransportMode ;
            $res_ary['szTransportMode'] = $szTransportMode;
            $res_ary['szHandoverCity'] = $szHandoverCity;

            if($vogaQuoteFound)
            {
                $res_ary['iStandardPricing'] = '1';
                $res_ary['iBookingType'] = __BOOKING_TYPE_VOGA_AUTOMATIC__;
            }
            else 
            {
                $szInternalComment = $postSearchAry['szInternalComment'].PHP_EOL."No automatic pricing available";
                $res_ary['szInternalComment'] = $szInternalComment;
                $res_ary['iBookingType'] = __BOOKING_TYPE_RFQ__;
                $res_ary['iBookingQuotes'] = 1;
                $res_ary['iQuotesStatus'] = __BOOKING_QUOTES_STATUS_NEW_BOOKING__;
            }  

            if(!empty($res_ary))
            {
                $update_query = '';
                foreach($res_ary as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            }  
            $update_query = rtrim($update_query,","); 
            //echo $update_query."update_query";
            if($kBooking->updateDraftBooking($update_query,$idBooking))
            {

            }
            //echo $vogaQuoteFound."vogaQuoteFound";
            if($vogaQuoteFound)
            {
                $dtResponseTime = $kBooking->getRealNow(); 
                $res_ary = array();
                $res_ary['szTransportecaStatus'] = "S4"; 
                $res_ary['szTransportecaTask'] = ''; 
                $res_ary['dtTaskStatusUpdatedOn'] = $dtResponseTime; 
                $res_ary['dtFileStatusUpdatedOn'] = $dtResponseTime;
                $res_ary['iStatus'] = 3 ; 
                $res_ary['idFileOwner'] = $idFileOwner ; 
                $res_ary['iSearchType']=2;

                if(!empty($res_ary))
                {
                    $update_query='';
                    foreach($res_ary as $key=>$value)
                    {
                        $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                } 
                $update_query = rtrim($update_query,",");  
                if($kBooking->updateBookingFileDetails($update_query,$idBookingFile))
                { 

                }
            }
            else
            {
                $kBooking = new cBooking();
                $dtResponseTime = $kBooking->getRealNow();

                $fileLogsAry = array(); 
                $fileLogsAry['szTransportecaStatus'] = 'S1';
                $fileLogsAry['szTransportecaTask'] = 'T1';
                $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
                $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime;
                $fileLogsAry['iSearchType'] = 2;
                if(!empty($fileLogsAry))
                {
                    $file_log_query = "";
                    foreach($fileLogsAry as $key=>$value)
                    {
                        $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                } 
                $kBooking_new = new cBooking();
                $file_log_query = rtrim($file_log_query,",");   
                //echo $file_log_query;
                $kBooking_new->updateBookingFileDetails($file_log_query,$idBookingFile);
            }
            if(!$vogaQuoteFound)
            {
                $this->iAutomaticQuoteAdded = 0;
                //Adding booking searh logs
                $kBookingLog = new cBooking();
                $postSearchAry = $kBookingLog->getBookingDetails($idBooking);  
                $kBookingLog->insertBookingSearchLogs($postSearchAry); 
            }
            else
            {
                $this->iAutomaticQuoteAdded = 1;
            }
        } 
        function createQuoteEmailMessage($postSearchAry,$quotesAry,$bTryitout=false)
        {
            $bookingDataArys = $postSearchAry;
            $iBookingLanguage = $postSearchAry['iBookingLanguage'];
            
            $transportModeListAry = array();
            $transportModeListAry = $this->getAllTransportMode(false,$iBookingLanguage,true,true);
        
            $langArr=$this->getLanguageDetails('',$iBookingLanguage);
            if(!empty($langArr))
            {
                $szCbmText = $langArr[0]['szDimensionUnit'];
            }
            else
            {
                $szCbmText = 'cbm';
            }
            
            $kConfig_new = new cConfig();
            $kConfig_new->loadCountry($postSearchAry['idOriginCountry'],false,$iBookingLanguage);
            $szOriginCountryName = $kConfig_new->szCountryName;
            
            $kConfig_new = new cConfig();
            $kConfig_new->loadCountry($postSearchAry['idDestinationCountry'],false,$iBookingLanguage);
            $szDestinationCountryName = $kConfig_new->szCountryName;
            
            
            
            $configLangArr=$this->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$iBookingLanguage);

            //print_r($configLangArr);
            $szOfferText = $configLangArr[1]['szOfferText_pt'] ;
            $szFromText =  $configLangArr[1]['szFromText_pt']  ;
            $szToText =  $configLangArr[1]['szToText_pt']  ;
            $szCargoText =  $configLangArr[1]['szCargoText_pt'] ;
            $szModeText =  $configLangArr[1]['szModeText_pt'] ;
            $szTransitTimeText =  $configLangArr[1]['szTransitTimeText_pt'];
            $szPriceText =  $configLangArr[1]['szPriceText_pt'];
            $OfferValidUntillText =  $configLangArr[1]['OfferValidUntillText_pt'];
            $szInsuranceText =  $configLangArr[1]['szInsuranceText_pt'];
            $szDaysText =  " ".$configLangArr[1]['szDaysText_pt'];
            $szNoVatOnBookingText =  $configLangArr[1]['szNoVatOnBookingText_pt'];
            $szVatExcludingText = $configLangArr[1]['szVatExcludingText_pt'];        
            $szOfferHeading =  $configLangArr[1]['szOfferHeading_pt'].":";
            $szMultipleOfferHeading = $configLangArr[1]['szMultipleOfferHeading_pt'].":";
            $szMultipleOfferHeading2 = $configLangArr[1]['szMultipleOfferHeading2_pt'].":";
            $szInsuranceValueUptoText = " ".$configLangArr[1]['szInsuranceValueUptoText_pt'];        
            $szVatTextExcluingTextWhenOneQuote = $configLangArr[1]['szVatTextExcluingTextWhenOneQuote_pt'];
            
//            if($iBookingLanguage==__LANGUAGE_ID_DANISH__) //danish
//            {
//                $szOfferText = 'TILBUD' ;
//                $szFromText = 'Fra' ;
//                $szToText = 'Til' ;
//                $szCargoText = 'Gods' ;
//                $szModeText = 'Transportform' ;
//                $szTransitTimeText = 'Transittid' ;
//                $szPriceText = 'Pris';
//                $OfferValidUntillText = 'Dette tilbud er gældende til den';
//                $szInsuranceText = 'Forsikring';
//                $szDaysText = ' dage';
//                $szNoVatOnBookingText = 'Der er ikke moms på denne afskibning';
//                $szVatExcludingText = 'eksklusiv moms på';
//
//                $szOfferHeading = "Tak for din forespørgsel. Vi fremsender hermed vores tilbud:";
//                $szMultipleOfferHeading = 'Tak for din forespørgsel:';
//                $szMultipleOfferHeading2 = 'Det glæder os at kunne give følgende tilbud:';
//                $szInsuranceValueUptoText = " for en vareværdi op til";
//            }
//            else
//            {
//                $szOfferText = 'OFFER' ;
//                $szFromText = 'From' ;
//                $szToText = 'To' ;
//                $szCargoText = 'Cargo' ;
//                $szModeText = 'Mode' ;
//                $szTransitTimeText = 'Transit time' ;
//                $szPriceText = 'Price';
//                $OfferValidUntillText = 'This offer is valid until';
//                $szInsuranceText = 'Insurance';
//                $szInsuranceValueUptoText = " for an insured value of";
//                $szDaysText = ' days';
//                $szNoVatOnBookingText = 'There is no VAT on this shipment.';
//                $szVatExcludingText = 'excluding VAT of';
//
//                $szOfferHeading = "Thank you for your request. We are happy to respond as follows:";
//                $szMultipleOfferHeading = 'Thank you for your request for a price for the following shipment:';
//                $szMultipleOfferHeading2 = 'In response to your request we are happy to offer the following services:';
//            } 
            
            $returnAry = array();
            $returnAry['szFromPostcode'] = $bookingDataArys['szOriginPostCode'];
            $returnAry['szFromCity'] = $bookingDataArys['szOriginCity'];
            $returnAry['szFromCountry'] = $szOriginCountryName;
            $returnAry['szToPostcode'] = $bookingDataArys['szDestinationPostCode'];
            $returnAry['szToCity'] = $bookingDataArys['szDestinationCity'];
            $returnAry['szToCountry'] = $szDestinationCountryName;
            $returnAry['szCargoDescription'] = $bookingDataArys['szCargoDescription'];
            $returnAry['szVolume'] = format_volume($bookingDataArys['fCargoVolume'],$iBookingLanguage).$szCbmText;
            $returnAry['szWeight'] = number_format_custom((float)$bookingDataArys['fCargoWeight'],$iBookingLanguage)."kg";
            $returnAry['szFirstName'] = $bookingDataArys['szFirstName'];
            $returnAry['szLastName'] = $bookingDataArys['szLastName'];

            $returnAry['szLastName'] = $bookingDataArys['szLastName'];

            if($bookingDataArys['idTransportMode']==4)//courier 
            { 
                $szVolWeight = format_volume($bookingDataArys['fCargoVolume'],$iBookingLanguage).$szCbmText.", ".number_format_custom((float)$bookingDataArys['fCargoWeight'],$iBookingLanguage)."kg,  ".number_format_custom((int)$bookingDataArys['iNumColli'],$iBookingLanguage)."col";
            }
            else
            {
                $szVolWeight = format_volume($bookingDataArys['fCargoVolume'],$iBookingLanguage).$szCbmText.",  ".number_format_custom((float)$bookingDataArys['fCargoWeight'],$iBookingLanguage)."kg ";
            }
            
            if(!empty($quotesAry))
            {
                $iQuoteCount = count($quotesAry); 
                if($iQuoteCount==1)
                { 
                    $szQuotationBody = $szOfferHeading."<br><br>" ; 
                }
                else
                { 
                    /*
                    $szQuotationBody = $szMultipleOfferHeading."<br><br>" ; 
                    $szQuotationBody .= $szFromText.': '.$bookingDataArys['szOriginCountry'].' <br>'
                                . ' '.$szToText.': '.$bookingDataArys['szDestinationCountry'].' <br>'
                                . ''.$szCargoText.': '.$szVolWeight.'  <br>';

                    $szQuotationBody .= "<br><br>".$szMultipleOfferHeading2." <br><br>" ; 
                     * 
                     */
                } 
                $counter = 1;
                $num_counter = 1;   
                $szCustomerCurrency = $bookingDataArys['szCurrency'];
                foreach($quotesAry as $quotesArys)
                {
                    if($quotesArys['iClosed']==1 || $bTryitout)
                    { 
                        $fTotalPriceCustomerCurrency = $quotesArys['fTotalPriceCustomerCurrency'];
                        $szQuotePriceStr =  $szCustomerCurrency." ".number_format_custom((float)$fTotalPriceCustomerCurrency,$iBookingLanguage)." all-in" ;
                        $fTotalVat = $quotesArys['fTotalVatCustomerCurrency'];
                        if($fTotalVat>0)
                        {
                            $szQuotePriceStr .=", excluding VAT of ".$szCustomerCurrency." ".number_format_custom((float)$fTotalVat,$iBookingLanguage) ;
                        }
                        
                        $iMonth = (int)date("m",strtotime($quotesArys['dtQuoteValidTo']));
                        $szMonthName = getMonthName($iBookingLanguage,$iMonth); 
                        $szValidTo = date('j.',strtotime($quotesArys['dtQuoteValidTo']))." ".$szMonthName;
                         
                        $szTransportMode = $transportModeListAry[$quotesArys['idTransportMode']]['szShortName'];

                        $szBookingRandomKey = $bookingDataArys['szBookingRandomNum'];
                        $idBookingQuotePricing = $quotesArys['idQuotePriceDetails'];
                        
                        if($idBookingQuotePricing>0)
                        {
                            $kConfig =new cConfig();
                            $langArr=$kConfig->getLanguageDetails('',$bookingDataArys['iBookingLanguage']);
                            $kQuote = new cQuote();
                            $szControlPanelUrl = $kQuote->buildQuoteLink($idBookingQuotePricing,$bookingDataArys['iBookingLanguage']);
                            $szBookingQuoteLink = '<a href="'.$szControlPanelUrl.'">'.t($this->t_base_error_booking.'links/click_here_to_use_offer').'</a>';
                            
                            /*
                             * In older version we were building quote link as follows
                            * if(!empty($langArr) && $bookingDataArys['iBookingLanguage']!=__LANGUAGE_ID_ENGLISH__)
                                {
                                    $szLanguageCode=$langArr[0]['szLanguageCode'];
                                    $szControlPanelUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".$szLanguageCode."/pendingQuotes/".$szBookingRandomKey."/".$idBookingQuotePricing."/"; 
                                    $szBookingQuoteLink = '<a href="'.$szControlPanelUrl.'">'.t($this->t_base_error_booking.'links/click_here_to_use_offer').'</a>';
                                }
                                else
                                {
                                    $szControlPanelUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/pendingQuotes/".$szBookingRandomKey."/".$idBookingQuotePricing."/"; 
                                    $szBookingQuoteLink = '<a href="'.$szControlPanelUrl.'">'.t($this->t_base_error_booking.'links/click_here_to_use_offer').'</a>';
                                }
                            */ 
                        } 
                        else
                        {
                            $kConfig =new cConfig();
                            $langArr=$kConfig->getLanguageDetails('',$bookingDataArys['iBookingLanguage']);
                            
                            $kQuote = new cQuote();
                            $szControlPanelUrl = $kQuote->buildQuoteLink($idBookingQuotePricing,$bookingDataArys['iBookingLanguage']); 
                            $szBookingQuoteLink = '<a href="'.$szControlPanelUrl.'">'.t($this->t_base_error_booking.'links/click_here_to_use_offer').'</a>'; 
                        }

                        if($iQuoteCount==1)
                        {
                            $szQuotationBody .=  $szModeText.': '.$szTransportMode.' <br>'
                                . ''.$szTransitTimeText.': '.$quotesArys['szTransitTime'].''.$szDaysText.' <br>'
                                . ''.$szPriceText.': '.$szQuotePriceStr.' <br>'
                                . '<br>'.$szBookingQuoteLink.'<br>
                            '; 
                        }
                        else
                        {
                            $szQuotationBody .= "<strong><u>".$counter.'. '.$szOfferText.' </u></strong><br>'. ' '.$szModeText.': '.$szTransportMode.' <br>'
                                . ''.$szTransitTimeText.': '.$quotesArys['szTransitTime'].''.$szDaysText.' <br>'
                                . ''.$szPriceText.': '.$szQuotePriceStr.' <br>'
                                . '<br>'.$szBookingQuoteLink.' <br>
                            ';
                        } 
                        $counter++;
                        $num_counter++;
                    }
                } 
            }
            $returnAry['szValidTo'] = $szValidTo;
            $returnAry['szQuote'] = $szQuotationBody; 
            return $returnAry;
        }
        
        function processSearchResults($postSearchAry,$searchResult)
        {
            //Checking if trades are added to courier excluded trade service list or not
            //If trade is added to excluded list then no need to check furthure

            $excludedTradeSearch = array(); 
            $excludedTradeSearch['idOriginCountry'] = $postSearchAry['idOriginCountry'];
            $excludedTradeSearch['idDestinationCountry'] = $postSearchAry['idDestinationCountry'];

            if($_SESSION['user_id']>0)
            {
                $kUser = new cUser();
                $kForwarder = new cForwarder();
                $kUser->getUserDetails($_SESSION['user_id']);

                if($kUser->iPrivate==1)
                {
                    $excludedTradeSearch['iCustomerType'] = __COURIER_EXCLUDED_TRADE_CUSTOMER_TYPE_PRIVATE__;
                }
                else
                {
                    $excludedTradeSearch['iCustomerType'] = __COURIER_EXCLUDED_TRADE_CUSTOMER_TYPE_BUSSINESS__;
                } 
            }

            $kCourierServices = new cCourierServices();

            $excludedTradesAry = array();
            $excludedTradesAry = $kCourierServices->isTradeAddedToExcludedList($excludedTradeSearch,false,true); 
  
            $szLogString = '';
            if(!empty($searchResult))
            {
                $ctr = 0; 
                foreach($searchResult as $searchResults)
                {
                    if($searchResults['idCourierProvider']>0)
                    {
                        $courierKey = $searchResults['idForwarder']."_".$searchResults['idCourierProvider'];  
                        if(!empty($excludedTradesAry) && in_array($courierKey,$excludedTradesAry))
                        { 
                            $szLogString .= "<br> ".$searchResults['szForwarderCompanyName']." - ".$searchResults['szName']." - ".$searchResults['szCourierProductName']." - Customer Price: ".$searchResults['szCurrency']." ".number_format((float)$searchResults['fDisplayPrice'],2)." - ".$searchResults['iDaysBetween']." days - NOT OK ";
                            $szLogString .= " <br> Description: This trade is added in excluded trade service list. <br>";
                        }
                        else
                        {
                            $courierServiceAry[$ctr] = $searchResults ;
                        } 
                    }
                    else
                    {
                        $lclServiceAry[$ctr] = $searchResults ;
                    }
                    $ctr++;
                }
            }
            
            $courierServiceAry = sortArray($courierServiceAry,'fDisplayPrice',false,'iDaysBetween',false,false,false,true);  
            $lclServiceAry = sortArray($lclServiceAry,'fDisplayPrice',false,'iDaysBetween',false,false,false,true);	
 
            $forwarderSearchAry = array();
            if(!empty($lclServiceAry))
            {
                foreach($lclServiceAry as $lclServiceArys)
                { 
                    $forwarderSearchAry[$lclServiceArys['idForwarder']]['lcl'] = $lclServiceArys;
                }
            } 
            if(!empty($courierServiceAry))
            {
                foreach($courierServiceAry as $courierServiceArys)
                { 
                    $key = $courierServiceArys['idForwarder']."_".$courierServiceArys['idCourierProvider']."_".$courierServiceArys['idCourierProviderProductid']; 
                    $forwarderSearchAry[$key]['courier'] = $courierServiceArys;
                }
            }
            return $forwarderSearchAry;
        } 
	// validate landing page 
	function validateLandingPage($data,$cargodetailAry=array(),$iSearchMiniPageVersion=false)
	{ 
            $this->set_idServiceType(sanitize_all_html_input(trim($data['idServiceType'])));
            $kCourierServices = new cCourierServices();
            if(!$kCourierServices->checkFromCountryToCountryExistsForCC($data['szOriginCountry'],$data['szDestinationCountry']))
            {
                 $this->iOrigingCC = (int)$data['idCC'][0];
                $this->iDestinationCC = (int)$data['idCC'][1];
            }
            
            
            
           
            $this->iSearchMiniVersion = $data['iSearchMiniPage'];
             
            $szBookingRandomNum = trim($data['szBookingRandomNum']); 
            if(empty($szBookingRandomNum))
            {
                $kBooking_new = new cBooking();
                $szBookingRandomNum = $kBooking_new->getUniqueBookingKey(10);
                $szBookingRandomNum = $kBooking_new->isBookingRandomNumExist($szBookingRandomNum);
            }
            $this->szBookingRandomNum = $szBookingRandomNum ;
            $this->iDoNotKnowCargo = (int)$data['iDoNotKnowCargo'];
            $this->idTimingType = (int)$data['idTimingType'];
            $this->iBookingQuote = (int)$data['iBookingQuote']; 

            $this->iDestinationPostcodeFoundAtStep = $data['iDestinationPostcodeFoundAtStep'];
            $this->iOriginPostcodeFoundAtStep = $data['iOriginPostcodeFoundAtStep']; 
            $this->szOriginPostCodeTemp = $data['szOriginPostCodeTemp']; 
            $this->szDestinationPostCodeTemp = $data['szDestinationPostCodeTemp']; 
            

            if(trim($data['szOriginCity'])==t($this->t_base.'fields/type_name'))
            {
                $data['szOriginCity'] = '';
            }
            if(trim($data['szOriginPostCode'])==t($this->t_base.'fields/optional') || trim($data['szOriginPostCode'])==t($this->t_base.'fields/type_code'))
            {
                $data['szOriginPostCode'] = '';
            }

            if(trim($data['szDestinationCity'])==t($this->t_base.'fields/type_name'))
            {
                $data['szDestinationCity'] = '';
            }
            if(trim($data['szDestinationPostCode'])==t($this->t_base.'fields/optional') || trim($data['szDestinationPostCode'])==t($this->t_base.'fields/type_code'))
            {
                $data['szDestinationPostCode'] = '';
            }

            if(trim($data['dtTiming'])==t($this->t_base.'fields/date_format'))
            {
                $data['dtTiming'] = '';
            }

            if($data['szPageName']=='SIMPLEPAGE')
            {
                $this->szPageSearchType='SIMPLEPAGE';
                //Validating data for Simple Search Page  
                $this->set_szOriginCountry(sanitize_all_html_input(trim($data['szOriginCountry']))); // id country origin 
                if((int)$data['szOriginCountry']>0)
                {
                    $this->loadCountry($data['szOriginCountry']);  
                    if($this->iSmallCountry==1 && empty($data['szOriginCity']))
                    {
                        $data['szOriginCity'] = $this->szCountryName ;
                    }
                    if($data['szOriginCity']=='')
                    {
                        if($data['szSearchMode']=='FORWARDER_COURIER_TRY_IT_OUT')
                        {
                            $this->addError("szOriginPostCode",t($this->t_base.'errors/city_name_is_required'));
                        }
                        else
                        {
                            $this->addError("szOriginCountry",t($this->t_base.'errors/city_name_is_required'));
                        } 
                    }
                } 
                $this->set_szDestinationCountry(sanitize_all_html_input(trim($data['szDestinationCountry']))); 
                if((int)$data['szDestinationCountry']>0)
                {
                    $this->loadCountry($data['szDestinationCountry']);
                    if($this->iSmallCountry==1 && empty($data['szDestinationCity']))
                    {
                        $data['szDestinationCity'] = $this->szCountryName ;
                    }
                    if($data['szDestinationCity']=='')
                    {
                        if($data['szSearchMode']=='FORWARDER_COURIER_TRY_IT_OUT')
                        {
                            $this->addError("szDestinationPostCode",t($this->t_base.'errors/city_name_is_required'));
                        }
                        else
                        {
                            $this->addError("szDestinationCountry",t($this->t_base.'errors/city_name_is_required'));
                        }
                    }
                }
                /*else if((int)$data['szDestinationCountry']==0)
                {
                        $this->addError("szDestinationCountry",t($this->t_base.'errors/do_not_service_from_import_country'));
                }*/

                $this->set_szOriginCity(sanitize_all_html_input(trim($data['szOriginCity'])));  // city name 
                $this->set_szOriginPostCode(sanitize_all_html_input(trim($data['szOriginPostCode'])));

                $this->set_szDestinationCity(sanitize_all_html_input(trim($data['szDestinationCity'])));
                $this->set_szDestinationPostCode(sanitize_all_html_input(trim($data['szDestinationPostCode'])));
            }
            else
            {
                $this->set_szOriginCountry(sanitize_all_html_input(trim($data['szOriginCountry']))); // id country origin
                $this->set_szDestinationCountry(sanitize_all_html_input(trim($data['szDestinationCountry'])));

                $this->set_szOriginCity(sanitize_all_html_input(trim($data['szOriginCity'])));  // city name 
                $this->set_szOriginPostCode(sanitize_all_html_input(trim($data['szOriginPostCode'])));

                $this->set_szDestinationCity(sanitize_all_html_input(trim($data['szDestinationCity'])));
                $this->set_szDestinationPostCode(sanitize_all_html_input(trim($data['szDestinationPostCode'])));
            } 
            if($data['dtTiming']!='' && $data['isMobileFlag']=='1')
            {
                $dtTimingArr=explode("-",$data['dtTiming']);
                $data['dtTiming']=$dtTimingArr[2]."/".$dtTimingArr[1]."/".$dtTimingArr[0];
            }
            $this->set_dtTiming(sanitize_all_html_input(trim($data['dtTiming']))); 
            if(!empty($this->dtTiming))
            {
                $dateAry = explode("/",$this->dtTiming); 
                $dtTiming_millisecond = strtotime($dateAry[2]."-".$dateAry[1]."-".$dateAry[0]);  // converting date 
                $dtDate = date("d/m/Y",$dtTiming_millisecond); 

                if(!checkdate($dateAry[1],$dateAry[0],$dateAry[2]))
                {
                    $this->addError("dtTiming",t($this->t_base.'errors/TIMING_DATE')." ".t('Error/valid_date_landing_page'));
                }
                else if(($dtTiming_millisecond <= strtotime(date('Y-m-d'))) && ($this->idTimingType==2)) // Available at destination 
                {
                   $this->addError("dtTiming",t('Error/greater_then_today'));
                }
                else
                {
                   $this->dtTiming = $dateAry[2]."-".$dateAry[1]."-".$dateAry[0]." 00:00:00";  // YYYY/MM/DD H:I:S
                }
            }
		  
            $kWHSSearch=new cWHSSearch();  
            $arrDescriptions = array("'__MAX_CARGO_DIMENSION_LENGTH__'","'__MAX_CARGO_DIMENSION_WIDTH__'","'__MAX_CARGO_DIMENSION_HEIGHT__'","'__MAX_CARGO_WEIGHT__'","'__MAX_CARGO_QUANTITY__'","'__MAX_CARGO_VOLUME__'"); 
            $bulkManagemenrVarAry = array();
            $bulkManagemenrVarAry = $kWHSSearch->getBulkManageMentVariableByDescription($arrDescriptions);      

            $maxCargoLength = $bulkManagemenrVarAry['__MAX_CARGO_DIMENSION_LENGTH__'];
            $maxCargoWidth = $bulkManagemenrVarAry['__MAX_CARGO_DIMENSION_WIDTH__'];
            $maxCargoHeight = $bulkManagemenrVarAry['__MAX_CARGO_DIMENSION_HEIGHT__'];
            $maxCargoWeight = $bulkManagemenrVarAry['__MAX_CARGO_WEIGHT__'];
            $maxCargoQty = $bulkManagemenrVarAry['__MAX_CARGO_QUANTITY__'];
            $maxVolume = $bulkManagemenrVarAry['__MAX_CARGO_VOLUME__'];
   
            if($iSearchMiniPageVersion>2)
            {
                $bSearchMiniPage = true;
            } 
            if(trim($data['szPageName'])=='SIMPLEPAGE' && $bSearchMiniPage)
            {
                if($iSearchMiniPageVersion==5 || $iSearchMiniPageVersion==6)
                {
                    $this->set_iNumColli(sanitize_all_html_input(trim($data['iNumColli'])));  // city name 
                    $this->set_iPalletType(sanitize_all_html_input(trim($data['iPalletType'])));
                    
                    $desc_required = false;
                    if($this->iPalletType==3)
                    {
                        $desc_required = true;
                    }
                    $this->set_szPalletDescriptions(sanitize_all_html_input(trim($data['szPalletDescriptions'])),$desc_required);
                    $this->set_fTotalWeight($data['iWeight'],$maxCargoWeight,$iSearchMiniPageVersion);  
                    
                    $iLanguage = getLanguageId();
                    $palletTypeAry = $this->getAllPalletTypes($this->iPalletType,$iLanguage);
                    $szCargoDescription = $palletTypeAry[0]['szLongName'];
                    $counter = 1;
                    if($this->iNumColli>0)
                    {
                        $palletWeight = round_up(($this->fTotalWeight/ $this->iNumColli),1); 
                        if($this->iPalletType==1) //Euro
                        {
                            $this->iLength[$counter] = 120;
                            $this->iWidth[$counter] = 80;  
                        }
                        else if($this->iPalletType==2) //Half
                        {
                            $this->iLength[$counter] = 80;
                            $this->iWidth[$counter] = 60;  
                        } 
                        else if($this->iPalletType==4) //English Pallet
                        {
                            $this->iLength[$counter] = 120;
                            $this->iWidth[$counter] = 100;  
                        }
                        $this->iQuantity[$counter] = $this->iNumColli; 
                        $this->idCargoMeasure[$counter] = 1; //Cm
                        $this->idWeightMeasure[$counter] = 1; //Kg
                        $this->iColli[$counter] = $this->iQuantity[$counter];
                        $this->szCommodity[$counter] = $szCargoDescription;
                        $this->iWeight[$counter] = $palletWeight;  
                    }    
                }
                else
                {
                    $lengthMeasureAry = $cargodetailAry['iLength'];   
                    if(!empty($lengthMeasureAry))
                    {
                        $ctr=1;
                        foreach($lengthMeasureAry as $key=>$lengthMeasureArys)
                        {
                            $this->idCargoMeasure[$ctr] = $cargodetailAry['idCargoMeasure'][$key];
                            $this->idWeightMeasure[$ctr] = $cargodetailAry['idWeightMeasure'][$key];

                            if($this->idCargoMeasure[$ctr]==1) //cm
                            { 
                                $cargo_measure = 'cm' ;
                                $maxCargoLength_temp = $maxCargoLength;
                                $maxCargoWidth_temp = $maxCargoWidth;
                                $maxCargoHeight_temp = $maxCargoHeight;
                            }
                            else
                            {
                                $fCmFactor = $kWHSSearch->getCargoFactor($this->idCargoMeasure[$ctr]);
                                $maxCargoLength_temp = ceil($maxCargoLength * $fCmFactor);
                                $maxCargoWidth_temp = ceil($maxCargoWidth * $fCmFactor);
                                $maxCargoHeight_temp = ceil($maxCargoHeight * $fCmFactor);
                                $cargo_measure = 'Inch' ;
                            }

                            if($this->idWeightMeasure[$i]==1)  // kg
                            {
                                $maxCargoWeight_temp = $maxCargoWeight ;
                                $weight_measure = 'Kg';
                            }
                            else
                            {
                                $fKgFactor = $kWHSSearch->getWeightFactor($this->idWeightMeasure[$i]);
                                $maxCargoWeight_temp = ceil($maxCargoWeight * $fKgFactor);
                                $weight_measure = 'Pounds';
                            }  
                            $data['iLength'][$key] = format_numeric_vals($cargodetailAry['iLength'][$key]);
                            $data['iHeight'][$key] = format_numeric_vals($cargodetailAry['iHeight'][$key]);
                            $data['iWidth'][$key] = format_numeric_vals($cargodetailAry['iWidth'][$key]);
                            $data['iWeight'][$key] = format_numeric_vals($cargodetailAry['iWeight'][$key]); 

    //                        echo "Length: ".$cargodetailAry['iLength'][$key]."<br>";
    //                        echo "Width: ".$cargodetailAry['iWidth'][$key]."<br>";
    //                        echo "Height: ".$cargodetailAry['iHeight'][$key]."<br>";
    //                        echo "Weight: ".$cargodetailAry['iWeight'][$key]."<br>"; 

                            $this->set_iLength(sanitize_all_html_input($cargodetailAry['iLength'][$key]),$ctr,$maxCargoLength_temp,$cargo_measure,$key,$iSearchMiniPageVersion);
                            $this->set_iHeight(sanitize_all_html_input($cargodetailAry['iHeight'][$key]),$ctr,$maxCargoHeight_temp,$cargo_measure,$key,$iSearchMiniPageVersion);
                            $this->set_iWidth(sanitize_all_html_input($cargodetailAry['iWidth'][$key]),$ctr,$maxCargoWidth_temp,$cargo_measure,$key,$iSearchMiniPageVersion);
                            $this->set_iQuantity(sanitize_all_html_input($cargodetailAry['iQuantity'][$key]),$ctr,$maxCargoQty,$key,$iSearchMiniPageVersion);
                            $this->set_iWeight(sanitize_all_html_input($cargodetailAry['iWeight'][$key]),$ctr,$maxCargoWeight_temp,$weight_measure,$key,$iSearchMiniPageVersion); 
                            $this->iColli[$ctr] = $this->iQuantity[$ctr];
                            $ctr++;
                        }
                    }
                } 
            } 
            else if(trim($data['szPageName'])=='SIMPLEPAGE')
            {		   
                $CargoMeasureCtr = count($data['idCargoMeasure']);
                for($i=1;$i<=$CargoMeasureCtr;$i++)
                {
                    $this->idCargoMeasure[$i]=$data['idCargoMeasure'][$i];
                } 
                $WeightMeasureCtr = count($data['idWeightMeasure']);
                for($i=1;$i<=$WeightMeasureCtr;$i++)
                {
                    $this->idWeightMeasure[$i]=$data['idWeightMeasure'][$i];
                } 
                $maxVolume = round((float)$maxVolume);
                $this->set_iVolume($data['iVolume'],$maxVolume); 
                $this->set_iNewWeight($data['iWeight'],$maxCargoWeight);
            }
            else
            {	 
                $lengthCtr = count($data['iLength']); 
                $tempCargoAry = array();
                $tempCargoAry['idCargo'] = $data['idCargo'];
                $tempCargoAry['iLength'] = $data['iLength'];
                $tempCargoAry['iWidth'] = $data['iWidth'];
                $tempCargoAry['iHeight'] = $data['iHeight'];
                $tempCargoAry['iWeight'] = $data['iWeight'];
                $tempCargoAry['idCargoMeasure'] = $data['idCargoMeasure'];
                $tempCargoAry['idWeightMeasure'] = $data['idWeightMeasure'];
                $tempCargoAry['iQuantity'] = $data['iQuantity'];
                $tempCargoAry['iDangerCargo'] = $data['iDangerCargo'];
                $this->tempCargoAry = $tempCargoAry ;
                
                $CargoMeasureCtr = count($data['idCargoMeasure']);
                for($i=1;$i<=$CargoMeasureCtr;$i++)
                {
                    $this->idCargoMeasure[$i]=$data['idCargoMeasure'][$i];
                }
                $WeightMeasureCtr = count($data['idWeightMeasure']);
                for($i=1;$i<=$WeightMeasureCtr;$i++)
                {
                    $this->idWeightMeasure[$i]=$data['idWeightMeasure'][$i];
                } 
                
                // putting data into temp varible 
                $maxCargoLength_temp = $maxCargoLength; 
                for($i=1;$i<=$lengthCtr;$i++)
                {
                    if($this->idCargoMeasure[$i]==1)  // cm
                    {
                        $maxCargoLength = $maxCargoLength_temp ;
                        $cargo_measure = 'cm' ;
                    }
                    else
                    {
                        $fCmFactor = $kWHSSearch->getCargoFactor($this->idCargoMeasure[$i]);
                        $maxCargoLength = ceil($maxCargoLength_temp * $fCmFactor);
                        $cargo_measure = 'Inch' ;
                    }
                    $this->set_iLength($data['iLength'][$i],$i,$maxCargoLength,$cargo_measure);
                } 
                $widthCtr = count($data['iWidth']); 
                // putting data into temp varible 
                $maxCargoWidth_temp = $maxCargoWidth ;
			
                for($i=1;$i<=$widthCtr;$i++)
                {
                    if($this->idCargoMeasure[$i]==1)  // cm
                    {
                        $maxCargoWidth = $maxCargoWidth_temp ;
                        $cargo_measure = 'cm' ;
                    }
                    else
                    {
                        $fCmFactor = $kWHSSearch->getCargoFactor($this->idCargoMeasure[$i]);
                        $maxCargoWidth = ceil($maxCargoWidth_temp * $fCmFactor);
                        $cargo_measure = 'Inch' ;
                    }
                    $this->set_iWidth($data['iWidth'][$i],$i,$maxCargoWidth,$cargo_measure);
                }

                $heightCtr = count($data['iHeight']);
                $maxCargoHeight_temp = $maxCargoHeight ;
                for($i=1;$i<=$heightCtr;$i++)
                {
                    if($this->idCargoMeasure[$i]==1)  // cm
                    {
                        $maxCargoHeight = $maxCargoHeight_temp ;
                        $cargo_measure = 'cm' ;
                    }
                    else
                    {
                        $fCmFactor = $kWHSSearch->getCargoFactor($this->idCargoMeasure[$i]);
                        $maxCargoHeight = ceil($maxCargoHeight_temp * $fCmFactor);
                        $cargo_measure = 'Inch' ;
                    }
                    $this->set_iHeight($data['iHeight'][$i],$i,$maxCargoHeight,$cargo_measure);
                }

                $qtyCtr = count($data['iQuantity']);
                for($i=1;$i<=$qtyCtr;$i++)
                {
                    $this->set_iQuantity($data['iQuantity'][$i],$i,$maxCargoQty);
                }

                $weightCtr = count($data['iWeight']); 
                $maxCargoWeight_temp = $maxCargoWeight ;
                for($i=1;$i<=$weightCtr;$i++)
                {
                    // converting cargo details into cubic meter 
                    if($this->idWeightMeasure[$i]==1)  // kg
                    {
                        $maxCargoWeight = $maxCargoWeight_temp ;
                        $weight_measure = 'Kg';
                    }
                    else
                    {
                        $fKgFactor = $kWHSSearch->getWeightFactor($this->idWeightMeasure[$i]);
                        $maxCargoWeight = ceil($maxCargoWeight_temp * $fKgFactor);
                        $weight_measure = 'Pounds';
                    } 
                    $this->set_iWeight($data['iWeight'][$i],$i,$maxCargoWeight,$weight_measure);
                }
                
                $idCargoCtr = count($data['idCargo']);
                for($i=1;$i<=$idCargoCtr;$i++)
                {
                    $this->idCargo[$i]=$data['idCargo'][$i];
                } 
                
                $iDangerCargoCtr = count($data['iDangerCargo']);
                for($i=1;$i<=$iDangerCargoCtr;$i++)
                {
                    if($data['iDangerCargo'][$i]=='Yes')
                    {
                        $this->iDangerCargoFlag = true;
                    }
                }
            }  
            
            if((int)$data['szBookingMode']>0)
            {
                $this->szBookingMode = $data['szBookingMode'] ;
            }	
			
            if($this->error===false)
            {
                $iLanguage = getLanguageId(); 
                if($data['szPageName']=='SIMPLEPAGE')
                {				
                    $this->szOriginCountryName=sanitize_all_html_input(trim($data['szOriginCountryStr']));
                    $this->szDestinationCountryName=sanitize_all_html_input(trim($data['szDestinationCountryStr']));
                }
                else
                {
                    $szOriginCountryAry=array();
                    $szDestinationCountryAry = array();

                    $szOriginCountryAry = $this->getAllCountryInKeyValuePair(false,$this->szOriginCountry,false,$iLanguage);
                    $szDestinationCountryAry =  $this->getAllCountryInKeyValuePair(false,$this->szDestinationCountry,false,$iLanguage);

                    $this->szOriginCountryName = $szOriginCountryAry[$this->szOriginCountry]['szCountryName'];
                    $this->szDestinationCountryName =  $szDestinationCountryAry[$this->szDestinationCountry]['szCountryName'];
                } 
                if($data['szPageName']=='SIMPLEPAGE')
                {
                    $this->fOriginLatitude = $data['szOLat']; 
                    $this->fOriginLongitude = $data['szOLng'];

                    $this->fDestinationLatitude = $data['szDLat'];
                    $this->fDestinationLongitude = $data['szDLng'];
                }
                else
                {
                    $this->validate_city_country_postcode_by_service_type($this);
                }
            }
            else
            {
                return false;
            }
	}	
        
        function isWeightMeasureExists($szWeightMeasure)
	{
            if(!empty($szWeightMeasure))
            {
               
                $configLangArr=$this->getConfigurationLanguageData('__TABLE_WEIGHT_MEASURE__','',true);
                $query = "
                    SELECT
                        id
                    FROM
                        ".__DBC_SCHEMATA_WEIGHT_MEASURE__."	
                ";
                if($result=$this->exeSQL($query))
                {
                    $ret_ary=array();
                    while($row=$this->getAssoc($result))
                    {
                       
                        if(strtolower($configLangArr[$row['id']]['szDescription'])==strtolower($szWeightMeasure))
                        {
                            //$configLangArr[$row['idTransportMode']]['szTransportMode']=$configLangArr[$row['idTransportMode']]['szShortName'];
                            $ret_ary = array_merge($configLangArr[$row['id']],$row);
                        }
                    }
                    return $ret_ary;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
            else
            {
                $this->error = true;
//                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
//                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
        
        function isCargoMeasureExists($szDimensionMeasure)
	{
            if(!empty($szDimensionMeasure))
            {
                
                //$kConfig = new cConfig();
                $configLangArr=$this->getConfigurationLanguageData('__TABLE_CARGO_MEASURE__','',true);
                $query = "
                    SELECT
                        id 
                    FROM
                        ".__DBC_SCHEMATA_CARGO_MEASURE__."
                ";
                if($result=$this->exeSQL($query))
                {
                    $ret_ary=array();
                    while($row=$this->getAssoc($result))
                    {
                       
                        if(strtolower($configLangArr[$row['id']]['szDescription'])==strtolower($szDimensionMeasure))
                        {
                            //$configLangArr[$row['idTransportMode']]['szTransportMode']=$configLangArr[$row['idTransportMode']]['szShortName'];
                            $ret_ary = array_merge($configLangArr[$row['id']],$row);
                        }
                    }
                    return $ret_ary;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
            else
            {
                $this->error = true;
//                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
//                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
        function validatePalletFields($cargodetailAry)
        {
            if(!empty($cargodetailAry))
            {
                $lengthMeasureAry = $cargodetailAry['iLength'];    
                $tempCargoARy = array();
                $szPalletDescriptionStr = '';
                $kWHSSearch = new cWHSSearch();
                if(!empty($lengthMeasureAry))
                {
                    $ctr=1;
                    foreach($lengthMeasureAry as $key=>$lengthMeasureArys)
                    {
                        $this->idCargoMeasure[$ctr] = $cargodetailAry['idCargoMeasure'][$key];
                        $this->idWeightMeasure[$ctr] = $cargodetailAry['idWeightMeasure'][$key];

                        if($this->idCargoMeasure[$ctr]==1)  // cm
                        { 
                            $cargo_measure = 'cm' ;
                            $maxCargoLength_temp = $maxCargoLength;
                            $maxCargoWidth_temp = $maxCargoWidth;
                            $maxCargoHeight_temp = $maxCargoHeight;
                        }
                        else
                        {
                            $fCmFactor = $kWHSSearch->getCargoFactor($this->idCargoMeasure[$ctr]);
                            $maxCargoLength_temp = ceil($maxCargoLength * $fCmFactor);
                            $maxCargoWidth_temp = ceil($maxCargoWidth * $fCmFactor); 
                            $cargo_measure = 'Inch' ;
                        }

                        $data['iLength'][$key] = format_numeric_vals($cargodetailAry['iLength'][$key]); 
                        $data['iWidth'][$key] = format_numeric_vals($cargodetailAry['iWidth'][$key]); 

//                        echo "Length: ".$cargodetailAry['iLength'][$key]."<br>";
//                        echo "Width: ".$cargodetailAry['iWidth'][$key]."<br>"; 

                        $this->set_iLength(sanitize_all_html_input($cargodetailAry['iLength'][$key]),$ctr,$maxCargoLength_temp,$cargo_measure,$iSearchMiniPageVersion);                    
                        $this->set_iWidth(sanitize_all_html_input($cargodetailAry['iWidth'][$key]),$ctr,$maxCargoWidth_temp,$cargo_measure,$iSearchMiniPageVersion);                    
                        $tempCargoARy[$ctr]['iLength'] = $this->iLength[$ctr];
                        $tempCargoARy[$ctr]['iWidth'] = $this->iWidth[$ctr];
                        $tempCargoARy[$ctr]['idCargoMeasure'] = $this->idCargoMeasure[$ctr];
                         
                        $iLanguage = getLanguageId(); 
                        $configLangArr=$this->getConfigurationLanguageData('__TABLE_COURIER_PROVIDER_PRODUCT_PACKING__',$iLanguage);
                       // $szPallet=$configLangArr[][];
//                        if($iLanguage==__LANGUAGE_ID_DANISH__)
//                        {
//                            $szPallet = $configLangArr[2]['szPackingSingle']." ";
//                        }
//                        else
//                        {
                            $szPallet = $configLangArr[2]['szPackingSingle']." ";
                        //}
                        $szPalletDescriptionStr .= $szPallet." ".$ctr.": ".$this->iLength[$ctr]."x".$this->iWidth[$ctr]."".$cargo_measure.", " ;
                        $ctr++;
                    }
                    if($this->error==true)
                    {
                        return false;
                    }
                    else
                    {
                        $this->tempCargoARy = $tempCargoARy;
                        $this->szPalletDescriptionStr = $szPalletDescriptionStr;
                        $this->iNumPallet = $ctr-1;
                        return true;
                    }
                }
            }
        }
        
	function loadCountry($idCountry=false,$szCountryISO=false,$iLanguage=__LANGUAGE_ID_ENGLISH__,$iInternationDialCode=false)
	{
            if($idCountry>0 || !empty($szCountryISO) || $iInternationDialCode>0)
            {
                if(!empty($szCountryISO))
                {
                    $query_where = " AND szCountryISO = '".$szCountryISO."' ";
                }
                else if($idCountry>0)
                {
                    $query_where = " AND c.id = '".(int)$idCountry."' ";
                }
                else if($iInternationDialCode>0)
                {
                    $query_where = " AND iInternationDialCode = '".(int)$iInternationDialCode."' ";
                }
                else
                {
                    return false;
                }
                
                if($iLanguage<=0)
                {
                    $iLanguage = getLanguageId();
                } 
//                if($iLanguage==__LANGUAGE_ID_DANISH__)
//                {
//                    $query_select = " szCountryDanishName as szCountryName, ";
//                }
//                else
//                {
//                    $query_select = " szCountryName, ";
//                }
                
                $query="
                    SELECT
                        c.id,
                        cnm.szName AS szCountryName,
                        szCountryISO,
                        szCountryISO3Code, 
                        iSmallCountry,
                        iInternationDialCode,
                        idRegion,
                        iDefaultCurrency,
                        iDefaultLanguage,
                        fMaxFobDistance,
                        fVATRate,
                        szVATRegistration,
                        szDefaultCourierPostcode,
                        szDefaultCourierCity,
                        c.iUsePostcodes
                    FROM
                        ".__DBC_SCHEMATA_COUNTRY__." AS c
                     INNER JOIN
                        ".__DBC_SCHEMATA_COUNTRY_NAME_MAPPING__." AS cnm
                    ON
                        cnm.idCountry=c.id
                    AND
                        cnm.idLanguage='".(int)$iLanguage."'
                    $query_where
                    LIMIT 0,1
                ";
         //echo $query;
                if($result = $this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        $row=$this->getAssoc($result);
                        $this->idCountry=$row['id'];
                        $this->szCountryName=$row['szCountryName'];
                        $this->szCountryISO=$row['szCountryISO'];
                        $this->iActiveFromDropdown=$row['iActiveFromDropdown'];
                        $this->iActiveToDropdown=$row['iActiveToDropdown'];
                        $this->iSmallCountry=$row['iSmallCountry'];
                        $this->iInternationDialCode=$row['iInternationDialCode'];
                        $this->idRegion=$row['idRegion']; 
                        $this->iDefaultCurrency = $row['iDefaultCurrency']; 
                        $this->iDefaultLanguage = $row['iDefaultLanguage'];  
                        $this->szCountryISO3Code = $row['szCountryISO3Code'];
                        $this->fMaxFobDistance = $row['fMaxFobDistance']; 
                        $this->fVATRate = $row['fVATRate'];
                        $this->szVATRegistration = $row['szVATRegistration'];
                        $this->szDefaultCourierPostcode = $row['szDefaultCourierPostcode'];
                        $this->szDefaultCourierCity = $row['szDefaultCourierCity'];
                        $this->iUsePostcodes = $row['iUsePostcodes']; 
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
	}
        function getAllTransportMode($idTransportMode=false,$iLanguage=__LANGUAGE_ID_ENGLISH__,$id_key=false,$preferences=false,$preferencesOrder=false)
        { 
            if($idTransportMode>0)
            {
                $query_and = " AND tm.id = '".(int)$idTransportMode."' ";
            }
            if((int)$iLanguage==0)
            {
                $iLanguage=__LANGUAGE_ID_ENGLISH__;
            }
//            if($iLanguage==2)
//            {
//                $query_select= "szShortNameDanish as szShortName, szLongNameDanish as szLongName, ";
//                $query_order_by = " ORDER BY szShortNameDanish ASC, szLongNameDanish ASC ";
//            }
//            else
//            {
//                $query_select= " szShortName, szLongName,";
//                $query_order_by = " ORDER BY szShortName ASC, szLongName ASC ";
//            }
            if($preferencesOrder)
            {
                $query_order_by = " ORDER BY iSortPreferences DESC, szShortName ASC, szLongName ASC ";
            }
            else if($preferences)
            {
                $query_order_by = " ORDER BY iPreferences DESC, szShortName ASC, szLongName ASC ";
            }
            else
            {
                $query_order_by = " ORDER BY szShortName ASC, szLongName ASC ";
            }
            if($preferences)
            {
                $query_and .= " AND tm.iPreferences IN ('0','1') ";
            }
            else
            {
                $query_and .= " AND tm.iPreferences = '0' ";
            } 
           
            $query="
                SELECT
                    tm.id,
                    tm.dtCreatedOn,
                    (SELECT szValue FROM ".__DBC_SCHEMATA_LANGUAGE_CONFIGURATION__." map1 WHERE map1.idMapped = tm.id AND map1.szFieldName = 'szLongName' AND map1.szTableKey = '__TABLE_TRANSPORT_MODE__' AND map1.idLanguage = '".$iLanguage."') as szLongName,
                    (SELECT szValue FROM ".__DBC_SCHEMATA_LANGUAGE_CONFIGURATION__." map2 WHERE map2.idMapped = tm.id AND map2.szFieldName = 'szShortName' AND map2.szTableKey = '__TABLE_TRANSPORT_MODE__' AND map2.idLanguage = '".$iLanguage."') as szShortName,
                    (SELECT szValue FROM ".__DBC_SCHEMATA_LANGUAGE_CONFIGURATION__." map3 WHERE map3.idMapped = tm.id AND map3.szFieldName = 'szFrequency' AND map3.szTableKey = '__TABLE_TRANSPORT_MODE__' AND map3.idLanguage = '".$iLanguage."') as szFrequency 
                FROM
                    ".__DBC_SCHEMATA_TRANSPORT_MODE__." AS tm
                WHERE
                    tm.iActive='1'
                   $query_and
                $query_order_by
            ";
            //echo $query;
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $forwarder_ary = array();
                    $ctr=0;
                    while($row=$this->getAssoc($result))
                    {
                        if($id_key)
                        {
                            if(!empty($configLangArr[$row['id']]))
                            {
                               $forwarder_ary[$row['id']] = array_merge($configLangArr[$row['id']],$row); 
                            }
                            else
                            {
                                $forwarder_ary[$row['id']] = $row; 
                            }
                            
                        }
                        else
                        {
                            if(!empty($configLangArr[$row['id']]))
                            {
                                $forwarder_ary[$ctr] = array_merge($configLangArr[$row['id']],$row); 
                            }
                            else
                            {
                                $forwarder_ary[$ctr] = $row; 
                            }
                            
                            $ctr++;
                        } 
                    } 
                    //$forwarder_ary =  sortArray($forwarder_ary, 'szShortName');
                    return $forwarder_ary;
                }
                else
                {
                    return array();
                }
            } 
        }
        
        function getAllServiceTerms($idServiceTerms=false,$szShortTerms=false,$bQuickQuote=false,$szTryOut=false)
        { 
            if($idServiceTerms>0)
            {
                $query_and = " AND id = '".(int)$idServiceTerms."' ";
            }
            if(!empty($szShortTerms))
            {
                $query_and .= " AND szShortTerms = '".mysql_escape_custom($szShortTerms)."' ";
            } 
            if($bQuickQuote)
            {
                $order_by = " ORDER BY szDisplayName ASC";
            }
            else if($szTryOut)
            {
                $order_by = " ORDER BY szDisplayNameForwarder ASC"; 
            }
            else
            {
                $order_by = " ORDER BY id ASC";
            }
             
            $query="
                SELECT
                    id, 
                    idServiceType,
                    szShortTerms,
                    szLongTerms,
                    szShortName,
                    szDisplayName,
                    szDisplayNameForwarder,
                    iActive
                FROM
                    ".__DBC_SCHEMATA_SERVICE_TERMS__."
                WHERE
                    iActive='1' 
                   $query_and
                   $order_by
            ";
            //echo $query;
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $forwarder_ary = array();
                    $alreadyAdded = array();
                    $ctr=0;
                    while($row=$this->getAssoc($result))
                    { 
                        if($bQuickQuote || $szTryOut)
                        {
                            $forwarder_ary[$ctr] = $row;
                            $ctr++; 
                        }
                        else
                        {
                            $szShortTerms = $row['szShortTerms'];
                            if(!empty($alreadyAdded) && !in_array($szShortTerms,$alreadyAdded))
                            {
                                $forwarder_ary[$ctr] = $row;
                                $alreadyAdded[] = $szShortTerms;
                                $ctr++;  
                            }
                            else if(empty($alreadyAdded))
                            {
                                $forwarder_ary[$ctr] = $row;
                                $alreadyAdded[] = $szShortTerms;
                                $ctr++;  
                            }
                        } 
                    }
                    return $forwarder_ary;
                }
                else
                {
                    return array();
                }
            } 
        }
        
        function getAllServiceTypes($idServiceType=false,$idLanguage=0)
        { 
            if($idServiceType>0)
            {
                $query_and = " AND id = '".(int)$idServiceType."' ";
            }
            
            if((int)$idLanguage==0)
            {
                $idLanguage=__LANGUAGE_ID_ENGLISH__;
            }
            $configLangArr=$this->getConfigurationLanguageData('__TABLE_SERVICE_TYPE__',$idLanguage);  
            $query=" 
                SELECT
                    id,
                    iActive
                FROM
                    ".__DBC_SCHEMATA_SERVICE_TYPE__."
                WHERE
                    iActive='1' 
                   $query_and
            ";
            //echo $query;
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $forwarder_ary = array();
                    $ctr=0;
                    while($row=$this->getAssoc($result))
                    { 
                        if(!empty($configLangArr[$row['id']]))
                        {
                           $forwarder_ary[$ctr] = array_merge($configLangArr[$row['id']],$row);
                        }else
                        {
                            $forwarder_ary[$ctr] = $row;
                        }
                        $forwarder_ary[$ctr] = $row;
                        $ctr++; 
                    }
                    return $forwarder_ary;
                }
                else
                {
                    return array();
                }
            } 
        }
        
	function isCityExistsByCountry($szCity,$idCountry,$szPostCode=false)
	{
		if($idCountry>0)
		{
			if(!empty($szCity))
			{
				//$query_and .= " AND szCity ='".mysql_escape_custom(trim($szCity))."'";
				$query_and = "
						AND 
					(
						p.szCity = '".mysql_escape_custom(trim($szCity))."'
					OR
						p.szCity1 = '".mysql_escape_custom(trim($szCity))."'
					OR
						p.szCity2 = '".mysql_escape_custom(trim($szCity))."'
					OR
						p.szCity3 = '".mysql_escape_custom(trim($szCity))."'
					OR
						p.szCity4 = '".mysql_escape_custom(trim($szCity))."'
					OR
						p.szCity5 = '".mysql_escape_custom(trim($szCity))."'
					OR
						p.szCity6 = '".mysql_escape_custom(trim($szCity))."'
					OR
						p.szCity7 = '".mysql_escape_custom(trim($szCity))."'
					OR
						p.szCity8 = '".mysql_escape_custom(trim($szCity))."'
					OR
						p.szCity9 = '".mysql_escape_custom(trim($szCity))."'
					OR
						p.szCity10 = '".mysql_escape_custom(trim($szCity))."'
					OR
						p.szRegion1 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						p.szRegion2 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						p.szRegion3 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						p.szRegion4 = '".mysql_escape_custom(trim($szCity))."'	
					)		
				";
			    $query_index = " USE INDEX (country_city_regions)";
			}		
			
			if(!empty($szPostCode))
			{
				$query_and .= " AND p.szPostCode='".mysql_escape_custom(trim($szPostCode))."'" ;
				if(!empty($query_index))
				{
					$query_index = " USE INDEX (country_postcode_city__regions)";
				}
				else
				{
					$query_index = " USE INDEX (idCountry_2)";
				}
			}	
			
			$query="
				SELECT
					p.id,
					p.szRegion1,
					p.szCity
				FROM
					".__DBC_SCHEMATA_POSTCODE__." p
					$query_index
				WHERE
					p.idCountry ='".(int)$idCountry."'
					$query_and	
				GROUP BY
					p.szRegion1	
			";
			//echo "<br>".$query."<br>" ;
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$szFirstRegion = '';
					$multi_region = false ;
					$arrRegion = array();
					$ctr=0;
					$this->loadCountry($idCountry);
					while($row=$this->getAssoc($result))
					{
						if(!empty($row['szRegion1']) && ((int)$this->iSmallCountry == 0))
						{
							if(empty($szFirstRegion))
							{
								$szFirstRegion =trim($row['szRegion1']);
							}
							if($szFirstRegion !=trim($row['szRegion1']))
							{
								$multi_region = true ;
							}
							$arrRegion[$row['id']]=trim($row['szCity']).', '.trim($row['szRegion1']);
							$ctr++;							
						}
						$this->idPostCode = $row['id'];
					}
					if($multi_region)
					{
						return 	$arrRegion ;
					}
					else
					{
						return true;					
					}
				}
				else
				{
					return array();
				}
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}	
	
	function isCityExistsByCountry_requirement($szCity,$idCountry)
	{
		if($idCountry>0)
		{
			if(!empty($szCity))
			{
				//$query_and .= " AND szCity ='".mysql_escape_custom(trim($szCity))."'";
				$query_and = "
						AND 
					(
						p.szCity = '".mysql_escape_custom(trim($szCity))."'
					OR
						p.szCity1 = '".mysql_escape_custom(trim($szCity))."'
					OR
						p.szCity2 = '".mysql_escape_custom(trim($szCity))."'
					OR
						p.szCity3 = '".mysql_escape_custom(trim($szCity))."'
					OR
						p.szCity4 = '".mysql_escape_custom(trim($szCity))."'
					OR
						p.szCity5 = '".mysql_escape_custom(trim($szCity))."'
					OR
						p.szCity6 = '".mysql_escape_custom(trim($szCity))."'
					OR
						p.szCity7 = '".mysql_escape_custom(trim($szCity))."'
					OR
						p.szCity8 = '".mysql_escape_custom(trim($szCity))."'
					OR
						p.szCity9 = '".mysql_escape_custom(trim($szCity))."'
					OR
						p.szCity10 = '".mysql_escape_custom(trim($szCity))."'
					OR
						p.szRegion1 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						p.szRegion2 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						p.szRegion3 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						p.szRegion4 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						p.szPostCode = '".mysql_escape_custom(trim($szCity))."'	
					)		
				";
			    $query_index = " USE INDEX (country_city_regions)";
			}		
						
			$query="
				SELECT
					p.id,
					p.szRegion1,
					p.szCity,
					p.szPostCode,
					szLat,
					szLng
				FROM
					".__DBC_SCHEMATA_POSTCODE__." p
					$query_index
				WHERE
					p.idCountry ='".(int)$idCountry."'
					$query_and	
				GROUP BY
					p.szRegion1	
				LIMIT
					0,100
			";
			//echo "<br>".$query."<br>" ;
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$szFirstRegion = '';
					$multi_region = false ;
					$arrRegion = array();
					$ctr=0;
					$this->loadCountry($idCountry);
					$szLongitudeSum = 0;
					$szLatitudeSum = 0;
					$iPostcodeFound = true;
					while($row=$this->getAssoc($result))
					{
						if(!empty($row['szRegion1']) && ((int)$this->iSmallCountry == 0))
						{
							if(empty($szFirstRegion))
							{
								$szFirstRegion =trim($row['szRegion1']);
							}
							if($szFirstRegion !=trim($row['szRegion1']))
							{
								$multi_region = true ;
							}
							$arrRegion[$row['id']]=trim($row['szCity']).', '.trim($row['szRegion1']);
							$ctr++;							
						}						
						
						if($this->comparePostcode($szCity,$row['szPostCode']))
						{
							$this->szPostCode = strtoupper(sanitize_all_html_input(trim($row['szPostCode'])));
							$this->szCity = sanitize_all_html_input(trim($row['szCity']));
							$this->idPostCode = $row['id'];
							$iPostcodeFound = false ;
						}
						else if($iPostcodeFound)
						{
							$this->szCity = $row['szCity'];
							$this->szPostCode = '';
							$this->idPostCode = $row['id'];
						}
						
						$szLatitudeSum = $szLatitudeSum + $row['szLat'] ; 
						$szLongitudeSum = $szLongitudeSum + $row['szLng'];
					}
					
					$szLatitude=0;
					$szLongitude=0;
					if($ctr>0)
					{
						$szLatitude = $szLatitudeSum/$ctr;
						$szLongitude = $szLongitudeSum/$ctr;
					}
					$this->szLatitude = $szLatitude;
					$this->szLongitude = $szLongitude;
					
					if($multi_region)
					{
						if(!empty($arrRegion))
						{
							asort($arrRegion);
							return $arrRegion;
						}
						else
						{
							return array();
						}
					}
					else
					{
						return true;					
					}
				}
				else
				{
					return array();
				}
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}	
	function isCityExistsByPostCode($szCity,$idCountry,$szPostCode)
	{
            if($idCountry>0)
            {
                /*
                if(!empty($szCity))
                {
                        $query_and .= " AND szCity = '".mysql_escape_custom(trim($szCity))."'" ;
                }
                */
                if(!empty($szCity))
                {
                    //$query_and .= " AND szCity ='".mysql_escape_custom(trim($szCity))."'";
                    $query_and = "
                            AND 
                        (
                            szCity = '".mysql_escape_custom(trim($szCity))."'
                        OR
                            szCity1 = '".mysql_escape_custom(trim($szCity))."'
                        OR
                            szCity2 = '".mysql_escape_custom(trim($szCity))."'
                        OR
                            szCity3 = '".mysql_escape_custom(trim($szCity))."'
                        OR
                            szCity4 = '".mysql_escape_custom(trim($szCity))."'
                        OR
                            szCity5 = '".mysql_escape_custom(trim($szCity))."'
                        OR
                            szCity6 = '".mysql_escape_custom(trim($szCity))."'
                        OR
                            szCity7 = '".mysql_escape_custom(trim($szCity))."'
                        OR
                            szCity8 = '".mysql_escape_custom(trim($szCity))."'
                        OR
                            szCity9 = '".mysql_escape_custom(trim($szCity))."'
                        OR
                            szCity10 = '".mysql_escape_custom(trim($szCity))."'
                        OR
                            szRegion1 = '".mysql_escape_custom(trim($szCity))."'	
                        OR
                            szRegion2 = '".mysql_escape_custom(trim($szCity))."'	
                        OR
                            szRegion3 = '".mysql_escape_custom(trim($szCity))."'	
                        OR
                            szRegion4 = '".mysql_escape_custom(trim($szCity))."'	
                        )		
                    ";
                }

                if(!empty($szPostCode))
                {
                    $query_and .= " AND szPostCode = '".mysql_escape_custom(trim($szPostCode))."'" ;
                }
                $query ="
                    SELECT
                        id
                    FROM
                        ".__DBC_SCHEMATA_POSTCODE__."	
                    WHERE
                        idCountry = '".(int)$idCountry."'
                        ".$query_and."					
                ";
                //echo $query."<br>" ;
                if($result = $this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        while($row=$this->getAssoc($result))
                        {
                            $this->idPostCode = $row['id'] ;
                        }
                        return true ;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
	}
	
	function isCityExistsByPostCode_requirement($szCity,$idCountry)
	{
		if($idCountry>0)
		{
			/*
			if(!empty($szCity))
			{
				$query_and .= " AND szCity = '".mysql_escape_custom(trim($szCity))."'" ;
			}
			*/
			if(!empty($szCity))
			{
				//$query_and .= " AND szCity ='".mysql_escape_custom(trim($szCity))."'";
				$query_and = "
						AND 
					(
						szCity = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity1 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity2 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity3 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity4 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity5 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity6 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity7 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity8 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity9 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity10 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szRegion1 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion2 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion3 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion4 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szPostCode = '".mysql_escape_custom(trim($szCity))."'	
					)		
				";
			}
			
			$query ="
				SELECT
					id,
					szPostCode,
					szCity
				FROM
					".__DBC_SCHEMATA_POSTCODE__."	
				WHERE
					idCountry = '".(int)$idCountry."'
					".$query_and."		
				LIMIT
					0,100			
			";
			//echo $query."<br>" ;
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					while($row=$this->getAssoc($result))
					{
						$this->idPostCode = $row['id'] ;						
						$this->szPostCode = strtoupper($row['szPostCode']);
						$this->szCity = $row['szCity'];
					}
					return true ;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	function getAllCityByPostCode($idCountry,$szPostCode)
	{
		if($idCountry>0)
		{
			$query="
				SELECT
					DISTINCT szCity
				FROM
					".__DBC_SCHEMATA_POSTCODE__."	
				WHERE
					idCountry ='".(int)$idCountry."'
				AND
					szPostCode = '".mysql_escape_custom(trim($szPostCode))."'	
			";
			//echo "<br> region check : ".$query."<br>";
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result) ;		
					return $row['szCity'] ;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	function getPostCodeDetails_requirement($idCountry,$szCity,$szPostcode=false)
	{
		if($idCountry>0)
		{
			if(!empty($szCity))
			{
				$query_and.= "
						AND 
					(
						szCity = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity1 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity2 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity3 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity4 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity5 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity6 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity7 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity8 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity9 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity10 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szRegion1 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion2 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion3 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion4 = '".mysql_escape_custom(trim($szCity))."'	
					OR 
						szPostCode='".mysql_escape_custom(trim($szCity))."'
					)		
				";
			}
			if(!empty($szPostcode))
			{
				$query_and .= " AND szPostCode='".mysql_escape_custom(trim($szPostcode))."' ";
			}
			
			$query="
				SELECT
					count(id) pscount,
					sum(szLat)/count(id) szLatitude,
					sum(szLng)/count(id) szLongitude,
					szCountry,
					szCity,
					szPostCode
				FROM
					".__DBC_SCHEMATA_POSTCODE__."	
				WHERE
					idCountry='".(int)$idCountry."'
					".$query_and."
				HAVING
					pscount>0	
				ORDER BY
					id ASC	
			";
			//echo $query;
			//die;
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result) ;		
					return $row;
				}
				else
				{
					return array();
				}
			}
			else
			{
				return false;
			}
		}
	}
	
	function getPostCodeDetails($idCountry,$szPostCode,$szCity=false)
	{
		if($idCountry>0)
		{
			if(!empty($szPostCode))
			{
				$query_and = " AND szPostCode='".mysql_escape_custom(trim($szPostCode))."'" ;
			}
			/*
			if(!empty($szcity))
			{
				$query_and .= " AND szCity='".mysql_escape_custom(trim($szcity))."'" ;
			}
				*/
			if(!empty($szCity))
			{
				//$query_and .= " AND szCity ='".mysql_escape_custom(trim($szCity))."'";
				$query_and.= "
						AND 
					(
						szCity = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity1 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity2 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity3 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity4 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity5 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity6 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity7 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity8 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity9 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity10 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szRegion1 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion2 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion3 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion4 = '".mysql_escape_custom(trim($szCity))."'	
					)		
				";
			}
			$query="
				SELECT
					count(id) pscount,
					sum(szLat)/count(id) szLatitude,
					sum(szLng)/count(id) szLongitude,
					szCountry,
					szCity
				FROM
					".__DBC_SCHEMATA_POSTCODE__."	
				WHERE
					idCountry='".(int)$idCountry."'
					".$query_and."
				HAVING
					pscount>0	
				ORDER BY
					id ASC	
			";
			//echo $query;
			//die;
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result) ;		
					return $row;
				}
				else
				{
					return array();
				}
			}
			else
			{
				return false;
			}
		}
	}
	function check_city_country($szCity,$idCountry)
	{
		if($idCountry>0)
		{
			if(!empty($szCity))
			{
				//$query_and .= " AND szCity ='".mysql_escape_custom(trim($szCity))."'";
				$query_and = "
						AND 
					(
						szCity = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity1 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity2 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity3 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity4 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity5 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity6 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity7 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity8 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity9 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity10 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szRegion1 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion2 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion3 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion4 = '".mysql_escape_custom(trim($szCity))."'	
					)		
				";
			    $query_index = " USE INDEX (country_city_regions)";
			}		
			
			if(!empty($szPostCode))
			{
				$query_and .= " AND szPostCode='".mysql_escape_custom(trim($szPostCode))."'" ;
				if(!empty($query_index))
				{
					$query_index = " USE INDEX (country_postcode_city__regions)";
				}
				else
				{
					$query_index = " USE INDEX (idCountry_2)";
				}
			}	
			
			$query="
				SELECT
					id,
					szRegion1,
					szRegion2,
					szRegion3,
					szRegion4,
					szCity,
					szPostCode,
					szArea1,
					szArea2
				FROM
					".__DBC_SCHEMATA_POSTCODE__."
					$query_index
				WHERE
					idCountry ='".(int)$idCountry."'
					$query_and	
				ORDER BY
					szRegion1 ASC,
					szRegion2 ASC,
					szRegion3 ASC,
					szRegion4 ASC,
					szCity ASC,
					szPostCode ASC,
					szArea1 ASC,
					szArea2 ASC
			";
			//echo "<br>".$query."<br>" ;
			$this->emptyPostcodeCtr = 0;
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$szFirstRegion = '';
					$multi_region = false ;
					$arrRegion = array();
					$ctr=0;
					$emptyCtr = 0;
					while($row=$this->getAssoc($result))
					{
						$region_str = '';
						if(!empty($row['szRegion1']))
						{
							$region_str .=$row['szRegion1'] ;
						}					
							
						if(!empty($row['szRegion2']) && !empty($region_str))
						{
							$region_str .=", ".$row['szRegion2'] ;
						}
						else if(!empty($row['szRegion2']))
						{
							$region_str .= $row['szRegion2'] ;
						}
						
						if(!empty($row['szRegion3']) && !empty($region_str))
						{
							$region_str .=", ".$row['szRegion3'];
						}
						else if(!empty($row['szRegion3']))
						{
							$region_str .=$row['szRegion3'] ;
						}
						
						if(!empty($row['szRegion4']) && !empty($region_str))
						{
							$region_str .=", ".$row['szRegion4'] ;
						}
						else if(!empty($row['szRegion4']))
						{
							$region_str .=$row['szRegion4'] ;						
						}
						
						if(!empty($row['szCity']) && !empty($region_str))
						{
							$region_str .=", ".$row['szCity'] ;
						}
						else if(!empty($row['szCity']))
						{
							$region_str .=$row['szCity'] ;
						}
						if(!empty($row['szArea1']) && !empty($region_str))
						{
							$region_str .=", ".$row['szArea1'] ;
						}
						else if(!empty($row['szArea1']))
						{
							$region_str .=$row['szArea1'] ;
						}
						
						if(!empty($row['szArea2']) && !empty($region_str))
						{
							$region_str .=", ".$row['szArea2'] ;
						}
						else if(!empty($row['szArea2']))
						{
							$region_str .=$row['szArea2'] ;
						}
						
						if(!empty($row['szPostCode']))
						{
							$region_str .=", ".$row['szPostCode'] ;
						}						
						else if(!empty($row['szPostCode']))
						{
							$region_str .=$row['szPostCode'] ;
						}
						
						$arrRegion[$row['id']] = $region_str;
						$ctr++;	
						if(empty($row['szPostCode']))
						{
							$emptyCtr ++ ;
						}
					}					
					if($emptyCtr==1)
					{
						$this->idEmptyPostCode = $row['id'];	
						//echo $this->idEmptyPostCode ;
					}
					if($emptyCtr!=$ctr)
					{
						$this->partialEmptyPostcodeCtr = $emptyCtr;
					}
					else
					{
						$this->emptyPostcodeCtr = $emptyCtr;
					}
					return $arrRegion ;
				}
				else
				{
					return array();
				}
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function check_city_country_requirement($szCity,$idCountry,$iLimitFrom=0,$fecth_limited_data=false,$icheck_counter=false,$szRegion1=false,$iCheckMultiRegion=false)
	{
		if($idCountry>0)
		{
			/*
			* Don't bother about this if block it checks multi region from few places  
			* If given location is exist in multiple location then it'll return multiregion popup.
			*/
			if($iCheckMultiRegion)
			{
				$multiReionCityAry =array();
				$multiReionCityAry = $this->isCityExistsByCountry_requirement($szCity,$idCountry) ;
				if(count($multiReionCityAry)>1)
				{
					$this->iMultiRegionFlag = 1;
					return $multiReionCityAry;
				}
			}
			if(!empty($szCity))
			{
				//$query_and .= " AND szCity ='".mysql_escape_custom(trim($szCity))."'";
				$query_and = "
						AND 
					(
						szCity = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity1 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity2 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity3 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity4 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity5 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity6 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity7 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity8 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity9 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity10 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szRegion1 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion2 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion3 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion4 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szPostCode = '".mysql_escape_custom(trim($szCity))."'
					)		
				";
			    $query_index = " USE INDEX (country_city_regions)";
			}
				
			if(!empty($szRegion1))
			{
				$query_and .=" AND
						szRegion1 = '".mysql_escape_custom(trim($szRegion1))."' " ;
			}
			
			if($fecth_limited_data)
			{
				$query_limit = " LIMIT ".(int)$iLimitFrom.",".(int)__POSTCODE_PER_PAGE_POSTCODE__ ;
			}
			else
			{
				$query_limit = " LIMIT 0,500 " ;
			}
			
			if($icheck_counter)
			{
				$query_select = " count(id) iCounter";
				$query_limit = " LIMIT 0,500 " ;
			}
			else
			{
				$query_select = "id,
					szRegion1,
					szRegion2,
					szRegion3,
					szRegion4,
					szCity,
					szPostCode,
					szArea1,
					szArea2,
					szCity,
					szLat,
					szLng ";
			}
			
			$query="
				SELECT
					$query_select
				FROM
					".__DBC_SCHEMATA_POSTCODE__."
					$query_index
				WHERE
					idCountry ='".(int)$idCountry."'
					$query_and	
				ORDER BY
					szRegion1 ASC
					$query_limit
			";
			//echo "<br>".$query."<br>";
			$this->emptyPostcodeCtr = 0;
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					if($icheck_counter)
					{	
						$row=$this->getAssoc($result);
						return $row['iCounter'];
					}
					else
					{	
						$szFirstRegion = '';
						$multi_region = false ;
						$arrRegion = array();
						$ctr=0;
						$emptyCtr = 0;
						$this->loadCountry($idCountry);
						$multi_region = false;
						while($row=$this->getAssoc($result))
						{
							$region_str = '';
							if(!empty($row['szPostCode']))
							{
								$region_str .=$row['szPostCode'] ;
							}				
							
							if(!empty($row['szRegion1']) && !empty($region_str))
							{
								$region_str .=", ".$row['szRegion1'] ;
							}
							else if(!empty($row['szRegion1']))
							{
								$region_str .= $row['szRegion1'] ;
							}
							
							if(!empty($row['szRegion2']) && !empty($region_str))
							{
								$region_str .=", ".$row['szRegion2'] ;
							}
							else if(!empty($row['szRegion2']))
							{
								$region_str .= $row['szRegion2'] ;
							}
							
							if(!empty($row['szRegion3']) && !empty($region_str))
							{
								$region_str .=", ".$row['szRegion3'];
							}
							else if(!empty($row['szRegion3']))
							{
								$region_str .=$row['szRegion3'] ;
							}
							
							if(!empty($row['szRegion4']) && !empty($region_str))
							{
								$region_str .=", ".$row['szRegion4'] ;
							}
							else if(!empty($row['szRegion4']))
							{
								$region_str .=$row['szRegion4'] ;						
							}
							
							if(!empty($row['szCity']) && !empty($region_str))
							{
								$region_str .=", ".$row['szCity'] ;
							}
							else if(!empty($row['szCity']))
							{
								$region_str .=$row['szCity'] ;
							}
							if(!empty($row['szArea1']) && !empty($region_str))
							{
								$region_str .=", ".$row['szArea1'] ;
							}
							else if(!empty($row['szArea1']))
							{
								$region_str .=$row['szArea1'] ;
							}
							
							if(!empty($row['szArea2']) && !empty($region_str))
							{
								$region_str .=", ".$row['szArea2'] ;
							}
							else if(!empty($row['szArea2']))
							{
								$region_str .=$row['szArea2'] ;
							}
							$arrRegion[$row['id']] = $region_str;
							$ctr++;	
							
							if(empty($row['szPostCode']))
							{
								$emptyCtr ++ ;
							}
							if(!empty($row['szPostCode']))
							{
								$notEmptyCtr ++ ;
							}
							
							if(!empty($row['szRegion1']) && ((int)$this->iSmallCountry == 0))
							{
								if(empty($szFirstRegion))
								{
									$szFirstRegion =trim($row['szRegion1']);
								}
								if(trim($szFirstRegion) != trim($row['szRegion1']))
								{
									$multi_region = true ;
								}
								
								$arrMultiRegion[$row['id']]=trim($row['szCity']).', '.trim($row['szRegion1']);			
							}
							$cityPostcodeAry = $row;
						}		
						
						$this->iMultiRegionFlag = 0;
						if($multi_region && $ctr>1)
						{
							$this->iMultiRegionFlag = 1;
							$ret_ary = array();
							if(!empty($arrMultiRegion))
							{
								$ret_ary = array_unique($arrMultiRegion);
							}
							return $ret_ary;
						}
						
						/*
						* If we have only 2 matching record and one having postcode and 2nd doesn't have postcode then in this case we show both option to select.
						**/
											
						if($emptyCtr==1 && $ctr==1)
						{
							$this->idEmptyPostCode = $cityPostcodeAry['id'];							
							if($this->comparePostcode($szCity,$this->szCity))
							{
								$this->szCity = $szCity ;
								$this->szPostCode = '';
							}
							else
							{
								$this->szCity = $cityPostcodeAry['szCity'];
								$this->szPostCode = strtoupper($cityPostcodeAry['szPostCode']);
							}
							$this->szLatitude = $cityPostcodeAry['szLat'];
							$this->szLongitude = $cityPostcodeAry['szLng'];
							
							//echo $this->idEmptyPostCode ;
							$this->emptyPostcodeCtr = $ctr;
						}
						else if($notEmptyCtr==1 && $ctr==1)
						{
							$this->idEmptyPostCode = $cityPostcodeAry['id'];
								
							if($this->comparePostcode($szCity,$this->szCity))
							{
								$this->szCity = $szCity ;
								$this->szPostCode = '';
							}
							else
							{
								$this->szCity = $cityPostcodeAry['szCity'];
								$this->szPostCode = strtoupper($cityPostcodeAry['szPostCode']);
							}
							
							$this->szLatitude = $cityPostcodeAry['szLat'];
							$this->szLongitude = $cityPostcodeAry['szLng'];
							
							$this->partialEmptyPostcodeCtr = $ctr;
						}		
						
						if($notEmptyCtr==1 && $emptyCtr==1 && $ctr==2)
						{
							$this->partialEmptyPostcodeCtr = $ctr;
						}
						else if($emptyCtr!=$ctr || ($notEmptyCtr == $ctr))
						{
							$this->partialEmptyPostcodeCtr = $notEmptyCtr;
						}
						else if($emptyCtr>1)
						{
							$this->emptyPostcodeCtr = $emptyCtr;
						}
						
						//echo "<br /> empty ctr ".$emptyCtr ;
						//echo "<br /> not empty ".$notEmptyCtr ;
						//echo "<br /> ctr ".$ctr ;
						//echo "<br /> empty counter ".$this->emptyPostcodeCtr ;
						//echo "<br /> partial empty counter ".$this->partialEmptyPostcodeCtr ;
						
						$ret_ary = array();
						if(!empty($arrRegion))
						{
							//$ret_ary = array_unique($arrRegion);
							$ret_ary = $arrRegion ;
							asort($ret_ary);
						}
						return $ret_ary;
					}
				}
				else
				{
					return array();
				}
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function validate_city_country_postcode_by_service_type($kObject)
	{
		$this->iOriginError = false ;
		$this->iDestinationError = false ;
		if($this->idServiceType== __SERVICE_TYPE_DTD__) //DTD
		{			
			// if we have both city and post code 
			if(!empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				if(!$this->isCityExistsByPostCode($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode))
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_FROM')." ".$this->szOriginCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_FROM');
					$this->addError('szOriginCity1',$error_messg);
					$this->iOriginError = true ;
				}																	
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}
			else if(empty($this->szOriginCity) && empty($this->szOriginPostCode)) // if we don't have any one 
			{
				$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_FROM');
				$this->addError('szOriginCity',$error_messg);
				$this->iOriginError = true ;
			}  
			else if(!empty($this->szOriginCity) && empty($this->szOriginPostCode)) // if we have only from city
			{
				$fromCityAry = $this->check_city_country($this->szOriginCity,$this->szOriginCountry);
				
				if(empty($fromCityAry))
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
					$this->addError('szOriginCity3',$error_messg);
					$this->iOriginError = true ;
				}
				else if(($this->emptyPostcodeCtr == 1 || $this->partialEmptyPostcodeCtr==1) && !empty($fromCityAry))
				{
					$this->idOriginPostCode = $this->idEmptyPostCode;
				}
				else if($this->emptyPostcodeCtr > 1 && !empty($fromCityAry))
				{
					//multi region; 
					$this->multiEmaptyPostcodeFromCityAry = $fromCityAry ;
				}
				else if($this->partialEmptyPostcodeCtr > 1 && !empty($fromCityAry))
				{
					//multi region; 
					$this->multiPartialEmaptyPostcodeFromCityAry = $fromCityAry ;
				}
				/*
				else if($this->emptyPostcodeCtr == 1 && !empty($fromCityAry))
				{
					$this->idOriginPostCode = $this->idEmptyPostCode;
				}
				*/
				else if(empty($this->szOriginPostCode) && ($this->szOriginCountry != __ID_COUNTRY_HONG_KONG__))
				{
					$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}																	
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}
			// if we have only post code the  we first fetch city which belongs to this country and post code then check for multiregion 
			else if(empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				$fromCityAry=array();
				$this->szOriginCity = $this->getAllCityByPostCode($this->szOriginCountry,$this->szOriginPostCode);
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode) ;
				if(!$fromCityAry)
				{ 
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
					$this->addError('szOriginCity4',$error_messg);
					$this->iOriginError = true ;
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}																	
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}		
			
			//if we have both to city and postcode .
			if(!empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				if(!$this->isCityExistsByPostCode($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode))
				{
					//call functions
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_TO')." ".$this->szDestinationCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_TO');
					$this->addError('szDestinationCity1',$error_messg);
					$this->iDestinationError = true ;
				}																	
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}	
			// if we don't have any 
			if(empty($this->szDestinationCity) && empty($this->szDestinationPostCode))
			{
				$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_TO');
				$this->addError('szDestinationCity4',$error_messg);
				$this->iDestinationError = true ;
			}
							
			// if we have onlyc city 
		    if(!empty($this->szDestinationCity) && empty($this->szDestinationPostCode))
			{		
				$toCityAry = array();		
				$toCityAry = $this->check_city_country($this->szDestinationCity,$this->szDestinationCountry) ;
				if(!$toCityAry)
				{
					//call function
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_TO');
					$this->addError('szDestinationCity3',$error_messg);
					$this->iDestinationError = true ;
				}
				else if(($this->emptyPostcodeCtr == 1 || $this->partialEmptyPostcodeCtr==1) && !empty($toCityAry))
				{
					$this->idDestinationPostCode = $this->idEmptyPostCode;
				}
				else if($this->emptyPostcodeCtr > 1 && !empty($toCityAry))
				{
					$this->multiEmaptyPostcodeToCityAry = $toCityAry ;
				}
				else if($this->partialEmptyPostcodeCtr > 1 && !empty($toCityAry))
				{
					$this->multiEmaptyPostcodeToCityAry = $toCityAry ;
				}
				/*
				else if($this->emptyPostcodeCtr == 1 && !empty($toCityAry))
				{
					$this->idDestinationPostCode = $this->idEmptyPostCode;
				}
				*/
				else if(empty($this->szDestinationPostCode) && ($this->szDestinationCountry != __ID_COUNTRY_HONG_KONG__))
				{
					$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_TO');
					$this->addError('szDestinationCity4',$error_messg);
					$this->iDestinationError = true ;
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
				}																	
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}				
			
			// if we have only post codes 
			if(empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				$toCityAry=array();
				$this->szDestinationCity = $this->getAllCityByPostCode($this->szDestinationCountry,$this->szDestinationPostCode);
				$toCityAry = $this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode) ;
				if(!$toCityAry)
				{
					//call function
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szDestinationCity4',$error_messg);
					$this->iDestinationError = true ;
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
				}													
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}
		}
		if(($this->idServiceType==(int)__SERVICE_TYPE_DTW__) || ($this->idServiceType==(int)__SERVICE_TYPE_DTP__)) //DTW
		{
			// if we have both city and post code 
			if(!empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				if(!$this->isCityExistsByPostCode($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode))
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_FROM')." ".$this->szOriginCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}													
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}
			
			// if we don't have any one 
			if(empty($this->szOriginCity) && empty($this->szOriginPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
				$this->addError('szOriginCity',$error_messg);
				$this->iOriginError = true ;
			}
			
			
			// if we have only from city  
			if(!empty($this->szOriginCity) && empty($this->szOriginPostCode))
			{			
				$fromCityAry = $this->check_city_country($this->szOriginCity,$this->szOriginCountry);
				if(empty($fromCityAry))
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
					$this->addError('szOriginCity3',$error_messg);
					$this->iOriginError = true ;
				}
				else if($this->emptyPostcodeCtr == 1 && !empty($fromCityAry))
				{
					$this->idOriginPostCode = $this->idEmptyPostCode;
				}
				else if($this->emptyPostcodeCtr > 1 && !empty($fromCityAry))
				{
					//multi region; 
					$this->multiEmaptyPostcodeFromCityAry = $fromCityAry ;
				}
				/*
				else if($this->emptyPostcodeCtr == 1 && !empty($fromCityAry))
				{
					$this->idOriginPostCode = $this->idEmptyPostCode;
				}
				*/
				else if(empty($this->szOriginPostCode) && ($this->szOriginCountry != __ID_COUNTRY_HONG_KONG__))
				{
					$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}																	
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}
			// if we have only post code the  we first fetch city which belongs to this country and post code then check for multiregion 
			if(empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				$fromCityAry=array();
				$this->szOriginCity = $this->getAllCityByPostCode($this->szOriginCountry,$this->szOriginPostCode);
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode);
				if(!$fromCityAry)
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}									
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}		
			
			//if we have both to city and postcode .
			if(!empty($this->szDestinationCity) && !empty($this->szDestinationCity))
			{
				if(!$this->isCityExistsByPostCode($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode))
				{	
					//call function
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_TO')." ".$this->szDestinationCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_TO');
					$this->addError('szOriginCity',$error_messg);
					$this->iDestinationError = true ;
				}									
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}	
			// if we don't have any 
			if(empty($this->szDestinationCity) && empty($this->szDestinationPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
				$this->addError('szOriginCity',$error_messg);
				$this->iDestinationError = true ;
			}
							
			// if we have onlyc city 
		    if(!empty($this->szDestinationCity) && empty($this->szDestinationPostCode) )
			{
				$toCityAry = $this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry) ;
				if(!$toCityAry)
				{	
					//call function
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_TO');
					$this->addError('szDestinationCity',$error_messg);
					$this->iDestinationError = true ;
				}
				elseif(count($toCityAry)>1)
				{
					$this->multiRegionToCityAry = $toCityAry ;					
				}
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
				
			}				
			
			// if we have only post codes 
			if(empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				$toCityAry=array();
				$this->szDestinationCity = $this->getAllCityByPostCode($this->szDestinationCountry,$this->szDestinationPostCode);
				$toCityAry = $this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode) ;
				if(!$toCityAry)
				{
					//call function
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szOriginCity',$error_messg);
					$this->iDestinationError = true ;
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
					$this->idDestinationPostCode = $this->idPostCode;
				}					
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
				
			}
		}
		if(($this->idServiceType==(int)__SERVICE_TYPE_WTD__) || ($this->idServiceType==(int)__SERVICE_TYPE_PTD__))
		{
			// if we have both city and post code 
			if(!empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				$this->iOriginError = false ;
				if(!$this->isCityExistsByPostCode($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode))
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_FROM')." ".$this->szOriginCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}			
			// if we don't have any one 
			if(empty($this->szOriginCity) && empty($this->szOriginPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
				$this->addError('szOriginCity',$error_messg);
				$this->iOriginError = true ;
			}
			// if we have only from city  
			if(!empty($this->szOriginCity) && empty($this->szOriginPostCode))
			{
				$this->iOriginError = false ;
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry) ;
				if(!$fromCityAry)
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}
			// if we have only post code the  we first fetch city which belongs to this country and post code then check for multiregion 
			if(empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				$this->iOriginError = false ;
				$fromCityAry=array();
				$this->szOriginCity = $this->getAllCityByPostCode($this->szOriginCountry,$this->szOriginPostCode);
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode);
				if(!$fromCityAry)
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}		
			
			//if we have both to city and postcode .
			if(!empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				$this->iDestinationError = false ;
				if(!$this->isCityExistsByPostCode($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode))
				{
					//call function
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szDestinationCity',$error_messg);
					$this->iDestinationError = true ;
				}				
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}				
			// if we don't have any 
			elseif(empty($this->szDestinationCity) && empty($this->szDestinationPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
				$this->addError('szDestinationCity',$error_messg);
				$this->iDestinationError = true ;
			}							
			// if we have only city 
		    if(!empty($this->szDestinationCity) && empty($this->szDestinationPostCode) )
			{		
				$this->iDestinationError = false ;
				$toCityAry = array();		
				$toCityAry = $this->check_city_country($this->szDestinationCity,$this->szDestinationCountry) ;
				//print_r($this->multiEmaptyPostcodeFromCityAry);
				if(!$toCityAry)
				{
					//call function
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_TO');
					$this->addError('szDestinationCity3',$error_messg);
					$this->iDestinationError = true ;
				}
				else if($this->emptyPostcodeCtr == 1 && !empty($toCityAry))
				{
					$this->idDestinationPostCode = $this->idEmptyPostCode;
				}
				else if($this->emptyPostcodeCtr > 1 && !empty($toCityAry))
				{
					$this->multiEmaptyPostcodeToCityAry = $toCityAry ;
				}
				/*
				else if($this->emptyPostcodeCtr == 1 && !empty($toCityAry))
				{
					$this->idDestinationPostCode = $this->idEmptyPostCode;
				}
				*/
				else if(empty($this->szDestinationPostCode) && ($this->szDestinationCountry != __ID_COUNTRY_HONG_KONG__))
				{
					$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_TO');
					$this->addError('szDestinationCity4',$error_messg);
					$this->iDestinationError = true ;
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
				}																	
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}	
			// if we have only post codes 
			if(empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				$this->iDestinationError = false ;
				$toCityAry=array();
				$this->szDestinationCity = $this->getAllCityByPostCode($this->szDestinationCountry,$this->szDestinationPostCode);
				$toCityAry = $this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode);
				if(!$toCityAry)
				{
					//call function
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szDestinationCity',$error_messg);
					$this->iDestinationError = true ;
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
				}
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}
		}
		if(($this->idServiceType==(int)__SERVICE_TYPE_WTW__) || ($this->idServiceType==(int)__SERVICE_TYPE_PTP__) || ($this->idServiceType==(int)__SERVICE_TYPE_WTP__) || ($this->idServiceType==(int)__SERVICE_TYPE_PTW__)) //WTW
		{
			// if we have both city and post code 
			if(!empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				if(!$this->isCityExistsByPostCode($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode))
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_FROM')." ".$this->szOriginCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}				
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			
			}
			
			// if we don't have any one 
			if(empty($this->szOriginCity) && empty($this->szOriginPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
				$this->addError('szOriginCity',$error_messg);
				$this->iOriginError = true ;
			}
			
			
			// if we have only from city  
			if(!empty($this->szOriginCity) && empty($this->szOriginPostCode))
			{
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry) ;
				if(!$fromCityAry)
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}			
	
			// if we have only post code the  we first fetch city which belongs to this country and post code then check for multiregion 
			if(empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				$fromCityAry=array();
				$this->szOriginCity = $this->getAllCityByPostCode($this->szOriginCountry,$this->szOriginPostCode);
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode) ;
				if(!$fromCityAry)
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}								
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}		
			
			//if we have both to city and postcode .
			if(!empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				if(!$this->isCityExistsByPostCode($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode))
				{
					//call function
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_TO')." ".$this->szDestinationCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_TO');
					$this->addError('szDestinationCity',$error_messg);
					$this->iDestinationError = true ;
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
					$this->idDestinationPostCode = $this->idPostCode;
				}	
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}	
			// if we don't have any 
			if(empty($this->szDestinationCity) && empty($this->szDestinationPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
				$this->addError('szDestinationCity',$error_messg);
				$this->iDestinationError = true ;
			}
							
			// if we have only city 
		    if(!empty($this->szDestinationCity) && empty($this->szDestinationPostCode) )
			{
				$toCityAry = $this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry) ;
				if(!$toCityAry)
				{
					//call function
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szDestinationCity',$error_messg);
					$this->iDestinationError = true ;
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
				}									
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}				
			
			// if we have only post codes 
			if(empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				$toCityAry=array();
				$this->szDestinationCity = $this->getAllCityByPostCode($this->szDestinationCountry,$this->szDestinationPostCode);
				$toCityAry = $this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode);
				if(!$toCityAry)
				{
					//call function
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szDestinationCity',$error_messg);
					$this->iDestinationError = true ;
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
				}				
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}
		}
	}
	
	function validate_city_country_postcode_by_service_type_requirement_Next_Step($kObject)
	{
		$this->iOriginError = false ;
		$this->iDestinationError = false ;
		
		/*
		* 1. In case of DT? or ?TD we first checking if city is in multiple region then show multi region popup
		* 2. If city is not in multi region then we check if there is postcode exist in all found rows.
		* 3. If all rows don't having postcode then we Multiempty post (Exact Location) popup
		* 4. If some rows having post and some don't then we show Partial Empty popup(Non-exact Popup).
		* 5. If there is only one row matched to our searching criteria then we don't show any popup and consider that row as final result.
		**/
		
		if($this->idServiceType== __SERVICE_TYPE_DTD__) //DTD
		{			
			if(empty($this->szOriginCity)) // if we don't have any one 
			{
				$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_FROM');
				$this->addError('szOriginCity',$error_messg);
				$this->iOriginError = true ;
			}  
			else if(!empty($this->szOriginCity)) // if we have only from city
			{
				$fromCityAry =array();
				$fromCityAry = $this->isCityExistsByCountry_requirement($this->szOriginCity,$this->szOriginCountry) ;
				if(!$fromCityAry)
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}
				else if(count($fromCityAry)>1)
				{
					// If city is exists in multiple regions then we show multiregion popup. 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}
				else
				{
					// If city is exists in single regions then we proceed for furthure calculation.
					$fromCityAry = array();
					$fromCityAry = $this->check_city_country_requirement($this->szOriginCity,$this->szOriginCountry);
					if(empty($fromCityAry))
					{
						//call function
						$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
						$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
						$this->addError('szOriginCity3',$error_messg);
						$this->iOriginError = true ;
					}
					else if($this->iMultiRegionFlag==1)
					{
						//we are having this block in assumption that in any certain case our First step may failed. then this block will work like Plan B and display Multiregion popup 
						$this->multiRegionFromCityAry = $fromCityAry ;
					}
					else if(($this->emptyPostcodeCtr == 1 || $this->partialEmptyPostcodeCtr==1) && !empty($fromCityAry))
					{
						$this->idOriginPostCode = $this->idEmptyPostCode;
						
						$this->szOriginCityPostcode = $this->szOriginCity ;
						if($this->comparePostcode($this->szOriginCity,$this->szPostCode))
						{
							$this->szOriginPostCode = $this->szPostCode;
							$this->szOriginCity = $this->szCity;
						}
						else
						{
							$this->szOriginPostCode = $this->szPostCode;
						}
						
						$this->fOriginLatitude = $this->szLatitude;
						$this->fOriginLongitude = $this->szLongitude;
						
					}
					else if($this->emptyPostcodeCtr > 1 && !empty($fromCityAry))
					{
						//multiempty postcode; 
						$this->multiEmaptyPostcodeFromCityAry = $fromCityAry ;
					}
					else if($this->partialEmptyPostcodeCtr > 1 && !empty($fromCityAry))
					{
						//partial empty popup; 
						$this->partialEmaptyPostcodeFromCityAry = $fromCityAry ;
					}
				}
			}
			
			// Performing checks for destination side.
			if(empty($this->szDestinationCity))
			{
				$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_TO');
				$this->addError('szDestinationCity4',$error_messg);
				$this->iDestinationError = true ;
			}			
		    else if(!empty($this->szDestinationCity))
			{		
				$this->iDestinationError = false ;
				$toCityAry = $this->isCityExistsByCountry_requirement($this->szDestinationCity,$this->szDestinationCountry) ;
				if(!$toCityAry)
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_TO');
					$this->addError('szOriginCity',$error_messg);
					$this->iDestinationError = true ;
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
				}
				else
				{
					$toCityAry = array();		
					$toCityAry = $this->check_city_country_requirement($this->szDestinationCity,$this->szDestinationCountry) ;
					if(!$toCityAry)
					{
						//call function
						$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
						$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_TO');
						$this->addError('szDestinationCity3',$error_messg);
						$this->iDestinationError = true ;
					}
					else if($this->iMultiRegionFlag==1)
					{
						//multi region; 
						$this->multiRegionToCityAry = $toCityAry ;
					}
					else if(($this->emptyPostcodeCtr == 1 || $this->partialEmptyPostcodeCtr==1) && !empty($toCityAry))
					{
						$this->idDestinationPostCode = $this->idEmptyPostCode;
					
						$this->szDestinationCityPostcode = $this->szDestinationCity ;
						if($this->comparePostcode($this->szDestinationCity,$this->szPostCode))
						{
							$this->szDestinationPostCode = $this->szPostCode;
							$this->szDestinationCity = $this->szCity;
						}
						else
						{
							$this->szDestinationPostCode = $this->szPostCode;
						}
						
						$this->fDestinationLatitude = $this->szLatitude;
						$this->fDestinationLongitude = $this->szLongitude;
					}
					else if($this->emptyPostcodeCtr > 1 && !empty($toCityAry))
					{
						$this->multiEmaptyPostcodeToCityAry = $toCityAry ;
					}
					else if($this->partialEmptyPostcodeCtr==1 && !empty($toCityAry))
					{
						$this->idDestinationPostCode = $this->idEmptyPostCode;
						$this->szDestinationCityPostcode = $this->szDestinationCity ;
						if($this->comparePostcode($this->szDestinationCity,$this->szPostCode))
						{
							$this->szDestinationPostCode = $this->szPostCode;
							$this->szDestinationCity = $this->szCity;
						}
						else
						{
							$this->szDestinationPostCode = $this->szPostCode;
						}
						$this->fDestinationLatitude = $this->szLatitude;
						$this->fDestinationLongitude = $this->szLongitude;
					}
					else if($this->partialEmptyPostcodeCtr > 1 && !empty($toCityAry))
					{
						$this->partialEmaptyPostcodeToCityAry = $toCityAry ;
					}
				}	
			}	
		}
		if(($this->idServiceType==(int)__SERVICE_TYPE_DTW__) || ($this->idServiceType==(int)__SERVICE_TYPE_DTP__)) //DTW
		{
			// if we don't have any one 
			if(empty($this->szOriginCity))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
				$this->addError('szOriginCity',$error_messg);
				$this->iOriginError = true ;
			}
			else if(!empty($this->szOriginCity))
			{			
				$fromCityAry = $this->check_city_country_requirement($this->szOriginCity,$this->szOriginCountry);
				
				if(empty($fromCityAry))
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
					$this->addError('szOriginCity3',$error_messg);
					$this->iOriginError = true ;
				}
				else if($this->emptyPostcodeCtr == 1 && !empty($fromCityAry))
				{
					$this->idOriginPostCode = $this->idEmptyPostCode;
					
					$this->szOriginCityPostcode = $this->szOriginCity ;
				
					if($this->comparePostcode($this->szOriginCity,$this->szPostCode))
					{
						$this->szOriginPostCode = $this->szPostCode;
						$this->szOriginCity = $this->szCity;
					}
					else
					{
						$this->szOriginPostCode = $this->szPostCode;
					}
					
					$this->fOriginLatitude = $this->szLatitude;
					$this->fOriginLongitude = $this->szLongitude;
				}
				else if($this->emptyPostcodeCtr > 1 && !empty($fromCityAry))
				{
					//multi region; 
					$this->multiEmaptyPostcodeFromCityAry = $fromCityAry ;
				}
				else if($this->partialEmptyPostcodeCtr==1 && !empty($fromCityAry))
				{
					$this->idOriginPostCode = $this->idEmptyPostCode;
					
					$this->szOriginCityPostcode = $this->szOriginCity ;
					if($this->comparePostcode($this->szOriginCity,$this->szPostCode))
					{
						$this->szOriginPostCode = $this->szPostCode;
						$this->szOriginCity = $this->szCity;
					}
					else
					{
						$this->szOriginPostCode = $this->szPostCode;
					}					
					$this->fOriginLatitude = $this->szLatitude;
					$this->fOriginLongitude = $this->szLongitude;
				}
				else if($this->partialEmptyPostcodeCtr>0 && !empty($fromCityAry))
				{
					$this->partialEmaptyPostcodeFromCityAry = $fromCityAry ;
				}
			}
			
			// if we don't have any 
			if(empty($this->szDestinationCity))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
				$this->addError('szOriginCity',$error_messg);
				$this->iDestinationError = true ;
			}
			else if(!empty($this->szDestinationCity))
			{
				$this->iDestinationError = false ;
				$toCityAry = $this->isCityExistsByCountry_requirement($this->szDestinationCity,$this->szDestinationCountry) ;
				if(!$toCityAry)
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_TO');
					$this->addError('szOriginCity',$error_messg);
					$this->iDestinationError = true ;
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
				}
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
					$this->szDestinationCityPostcode = $this->szDestinationCity ;
					
					if($this->comparePostcode($this->szDestinationCity,$this->szPostCode))
					{
						$this->szDestinationPostCode = $this->szPostCode;
						$this->szDestinationCity = $this->szCity;
					}
					$this->fDestinationLatitude = $this->szLatitude;
					$this->fDestinationLongitude = $this->szLongitude;
				}
			}	
		}
		if(($this->idServiceType==(int)__SERVICE_TYPE_WTD__) || ($this->idServiceType==(int)__SERVICE_TYPE_PTD__))
		{
			// if we don't have any one 
			if(empty($this->szOriginCity))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
				$this->addError('szOriginCity',$error_messg);
				$this->iOriginError = true ;
			}
			else if(!empty($this->szOriginCity))
			{
				$fromCityAry = $this->isCityExistsByCountry_requirement($this->szOriginCity,$this->szOriginCountry) ;
				if(!$fromCityAry)
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
					$this->szOriginCityPostcode = $this->szOriginCity ;
					
					if($this->comparePostcode($this->szOriginCity,$this->szPostCode))
					{
						$this->szOriginPostCode = $this->szPostCode;
						$this->szOriginCity = $this->szCity;
					}
					$this->fOriginLatitude = $this->szLatitude;
					$this->fOriginLongitude = $this->szLongitude;
				}
			}
			
			// if we don't have any 
			if(empty($this->szDestinationCity))
			{
				$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_TO');
				$this->addError('szDestinationCity4',$error_messg);
				$this->iDestinationError = true ;
			}			
		    else if(!empty($this->szDestinationCity))
			{		
				$toCityAry = array();		
				$toCityAry = $this->check_city_country_requirement($this->szDestinationCity,$this->szDestinationCountry) ;
				if(!$toCityAry)
				{
					//call function
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_TO');
					$this->addError('szDestinationCity3',$error_messg);
					$this->iDestinationError = true ;
				}
				else if(($this->emptyPostcodeCtr == 1 || $this->partialEmptyPostcodeCtr==1) && !empty($toCityAry))
				{
					$this->idDestinationPostCode = $this->idEmptyPostCode;
					$this->szDestinationCityPostcode = $this->szDestinationCity ;
					if($this->comparePostcode($this->szDestinationCity,$this->szPostCode))
					{
						$this->szDestinationPostCode = $this->szPostCode;
						$this->szDestinationCity = $this->szCity;
					}
					else
					{
						$this->szDestinationPostCode = $this->szPostCode;
					}
					$this->fDestinationLatitude = $this->szLatitude;
					$this->fDestinationLongitude = $this->szLongitude;
				}
				else if($this->emptyPostcodeCtr > 1 && !empty($toCityAry))
				{
					$this->multiEmaptyPostcodeToCityAry = $toCityAry ;
				}
				else if($this->partialEmptyPostcodeCtr==1 && !empty($toCityAry))
				{
					$this->idDestinationPostCode = $this->idEmptyPostCode;
					$this->szDestinationCityPostcode = $this->szDestinationCity ;
					
					if($this->comparePostcode($this->szDestinationCity,$this->szPostCode))
					{
						$this->szDestinationPostCode = $this->szPostCode;
						$this->szDestinationCity = $this->szCity;
					}
					else
					{
						$this->szDestinationPostCode = $this->szPostCode;
					}
					$this->fDestinationLatitude = $this->szLatitude;
					$this->fDestinationLongitude = $this->szLongitude;
				}
				else if($this->partialEmptyPostcodeCtr > 1 && !empty($toCityAry))
				{
					$this->partialEmaptyPostcodeToCityAry = $toCityAry ;
				}
			}
		}
		if(($this->idServiceType==(int)__SERVICE_TYPE_WTW__) || ($this->idServiceType==(int)__SERVICE_TYPE_PTP__) || ($this->idServiceType==(int)__SERVICE_TYPE_WTP__) || ($this->idServiceType==(int)__SERVICE_TYPE_PTW__)) //WTW
		{
			// if we don't have any one 
			if(empty($this->szOriginCity))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
				$this->addError('szOriginCity',$error_messg);
				$this->iOriginError = true ;
			}
			else if(!empty($this->szOriginCity))
			{
				$fromCityAry = $this->isCityExistsByCountry_requirement($this->szOriginCity,$this->szOriginCountry) ;
				if(!$fromCityAry)
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}
				else
				{
					$this->idOriginPostCode = $this->idPostCode;					
					$this->szOriginCityPostcode = $this->szOriginCity ;						
					
					if($this->comparePostcode($this->szOriginCity,$this->szPostCode))
					{
						$this->szOriginPostCode = $this->szPostCode;
						$this->szOriginCity = $this->szCity;
					}					
					$this->fOriginLatitude = $this->szLatitude;
					$this->fOriginLongitude = $this->szLongitude;
				}
			}
						
			// if we don't have any 
			if(empty($this->szDestinationCity))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
				$this->addError('szOriginCity',$error_messg);
				$this->iDestinationError = true ;
			}
			else if(!empty($this->szDestinationCity))
			{
				$this->iDestinationError = false ;
				$toCityAry = $this->isCityExistsByCountry_requirement($this->szDestinationCity,$this->szDestinationCountry) ;
				if(!$toCityAry)
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_TO');
					$this->addError('szOriginCity',$error_messg);
					$this->iDestinationError = true ;
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
				}
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;					
					$this->szDestinationCityPostcode = $this->szDestinationCity;
				
					if($this->comparePostcode($this->szDestinationCity,$this->szPostCode))
					{
						$this->szDestinationPostCode = $this->szPostCode;
						$this->szDestinationCity = $this->szCity;
					}
					$this->fDestinationLatitude = $this->szLatitude;
					$this->fDestinationLongitude = $this->szLongitude;
				}
			}
		}
	}
	
	function validate_city_country_postcode_by_service_type_requirement($kObject,$type)
	{
		if($type=='ORIGIN')
		{
			$this->iOriginError = false ;
			if($this->idServiceType== __SERVICE_TYPE_DTD__) //DTD
			{	
				if(empty($this->szOriginCity)) // if we don't have any one 
				{
					$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}  
				else if(!empty($this->szOriginCity)) // if we have only from city
				{
					$fromCityAry = $this->check_city_country_requirement($this->szOriginCity,$this->szOriginCountry);
					
					if(empty($fromCityAry))
					{
						//call function
						$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
						$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
						$this->addError('szOriginCity3',$error_messg);
						$this->iOriginError = true ;
					}
					else if(!empty($fromCityAry))
					{
						return true;
					}
				}
			}
			if(($this->idServiceType==(int)__SERVICE_TYPE_DTW__) || ($this->idServiceType==(int)__SERVICE_TYPE_DTP__)) //DTW
			{
				// if we don't have any one 
				if(empty($this->szOriginCity))
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}
				// if we have only from city  
				else if(!empty($this->szOriginCity))
				{			
					$fromCityAry = $this->check_city_country_requirement($this->szOriginCity,$this->szOriginCountry);
					if(empty($fromCityAry))
					{
						//call function
						$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
						$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
						$this->addError('szOriginCity3',$error_messg);
						$this->iOriginError = true ;
					}
					else if(!empty($fromCityAry))
					{
						return true;
					}
				}
			}
			if(($this->idServiceType==(int)__SERVICE_TYPE_WTD__) || ($this->idServiceType==(int)__SERVICE_TYPE_PTD__))
			{	
				// if we don't have any one 
				if(empty($this->szOriginCity) && empty($this->szOriginPostCode))
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}
				// if we have only from city  
				if(!empty($this->szOriginCity))
				{
					$this->iOriginError = false ;
					$fromCityAry = $this->isCityExistsByPostCode_requirement($this->szOriginCity,$this->szOriginCountry) ;
					if(!$fromCityAry)
					{
						//call function
						$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
						$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
						$this->addError('szOriginCity',$error_messg);
						$this->iOriginError = true ;
					}
					else if(count($fromCityAry)>1)
					{
						return true;
					}
				}
			}
			if(($this->idServiceType==(int)__SERVICE_TYPE_WTW__) || ($this->idServiceType==(int)__SERVICE_TYPE_PTP__) || ($this->idServiceType==(int)__SERVICE_TYPE_WTP__) || ($this->idServiceType==(int)__SERVICE_TYPE_PTW__)) //WTW
			{
				if(empty($this->szOriginCity) && empty($this->szOriginPostCode))
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}
				// if we have both city and post code 
				else if(!empty($this->szOriginCity))
				{
					if(!$this->isCityExistsByPostCode_requirement($this->szOriginCity,$this->szOriginCountry))
					{
						//call function
						$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
						$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_FROM')." ".$this->szOriginCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_FROM');
						$this->addError('szOriginCity',$error_messg);
						$this->iOriginError = true ;
					}				
					else
					{
						$this->idOriginPostCode = $this->idPostCode;
						return true ;
					}
			  }
		}
	}
		else if($type=='DESTINATION')
		{
			$this->iDestinationError = false ;
			if($this->idServiceType== __SERVICE_TYPE_DTD__) //DTD
			{			
				// if we don't have any 
				if(empty($this->szDestinationCity) && empty($this->szDestinationPostCode))
				{
					$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_TO');
					$this->addError('szDestinationCity4',$error_messg);
					$this->iDestinationError = true ;
				}								
				// if we have onlyc city 
			    if(!empty($this->szDestinationCity))
				{		
					$toCityAry = array();		
					$toCityAry = $this->check_city_country_requirement($this->szDestinationCity,$this->szDestinationCountry) ;
					if(!$toCityAry)
					{
						//call function
						$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
						$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
						$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_TO');
						$this->addError('szDestinationCity3',$error_messg);
						$this->iDestinationError = true ;
					}
					else if(!empty($toCityAry))
					{
						return true;
					}
				}
			}
			if(($this->idServiceType==(int)__SERVICE_TYPE_DTW__) || ($this->idServiceType==(int)__SERVICE_TYPE_DTP__) || ($this->idServiceType==(int)__SERVICE_TYPE_WTW__) || ($this->idServiceType==(int)__SERVICE_TYPE_PTP__) || ($this->idServiceType==(int)__SERVICE_TYPE_WTP__) || ($this->idServiceType==(int)__SERVICE_TYPE_PTW__)) //DTW
			{
				// if we don't have any 
				if(empty($this->szDestinationCity) && empty($this->szDestinationPostCode))
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szOriginCity',$error_messg);
					$this->iDestinationError = true ;
				}
			    else if(!empty($this->szDestinationCity) && empty($this->szDestinationPostCode) )
				{
					$toCityAry = $this->isCityExistsByCountry_requirement($this->szDestinationCity,$this->szDestinationCountry) ;
					if(!$toCityAry)
					{	
						//call function
						$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
						$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_TO');
						$this->addError('szDestinationCity',$error_messg);
						$this->iDestinationError = true ;
					}
					elseif(count($toCityAry)>1)
					{
						return true;
					}
				}
			}
			if(($this->idServiceType==(int)__SERVICE_TYPE_WTD__) || ($this->idServiceType==(int)__SERVICE_TYPE_PTD__))
			{
				
				// if we don't have any 
				if(empty($this->szDestinationCity))
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szDestinationCity',$error_messg);
					$this->iDestinationError = true ;
				}	
			    else if(!empty($this->szDestinationCity))
				{		
					$this->iDestinationError = false ;
					$toCityAry = array();		
					$toCityAry = $this->check_city_country_requirement($this->szDestinationCity,$this->szDestinationCountry) ;
					
					if(!$toCityAry)
					{
						//call function
						$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
						$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_TO');
						$this->addError('szDestinationCity3',$error_messg);
						$this->iDestinationError = true ;
					}
					else if(!empty($toCityAry))
					{
						return true;
					}
				}	
			}
		}
	}
	
	/*
	validation backup 
	
	function validate_city_country_postcode_by_service_type($kObject)
	{
		if($this->idServiceType==1) //DTD
		{
			
			// if we have both city and post code 
			if(!empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				if(!$this->isCityExistsByPostCode($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode))
				{
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_FROM')." ".$this->szOriginCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_FROM');
					$this->addError('szOriginCity1',$error_messg);
				}																	
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}
			else if(empty($this->szOriginCity) && empty($this->szOriginPostCode)) // if we don't have any one 
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
				$this->addError('szOriginCity2',$error_messg);
			}  
			else if(!empty($this->szOriginCity) && empty($this->szOriginPostCode)) // if we have only from city
			{
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry) ;
				if(!$fromCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
					$this->addError('szOriginCity3',$error_messg);
				}
				else if(empty($this->szOriginPostCode))
				{
					$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_FROM');
					$this->addError('szOriginCity',$error_messg);
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}																	
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}
			// if we have only post code the  we first fetch city which belongs to this country and post code then check for multiregion 
			else if(empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				$fromCityAry=array();
				$this->szOriginCity = $this->getAllCityByPostCode($this->szOriginCountry,$this->szOriginPostCode);
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry) ;
				if(!$fromCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
					$this->addError('szOriginCity4',$error_messg);
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}																	
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}		
			
			//if we have both to city and postcode .
			if(!empty($this->szDestinationCity) && !empty($this->szDestinationCity))
			{
				if(!$this->isCityExistsByPostCode($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode))
				{
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_TO')." ".$this->szDestinationCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_TO');
					$this->addError('szDestinationCity1',$error_messg);
				}																	
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}	
			// if we don't have any 
			if(empty($this->szDestinationCity) && empty($this->szDestinationPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
				$this->addError('szDestinationCity2',$error_messg);
			}
							
			// if we have onlyc city 
		    if(!empty($this->szDestinationCity) && empty($this->szDestinationPostCode) )
			{				
				$fromCityAry = $this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry) ;
				if(!$fromCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_TO');
					$this->addError('szDestinationCity3',$error_messg);
				}
				else if(empty($this->szDestinationPostCode))
				{
					$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_TO');
					$this->addError('szDestinationCity4',$error_messg);
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $fromCityAry ;
				}																	
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}				
			
			// if we have only post codes 
			if(empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				$toCityAry=array();
				$this->szDestinationCity = $this->getAllCityByPostCode($this->szDestinationCountry,$this->szDestinationPostCode);
				$toCityAry = $this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry) ;
				if(!$toCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szDestinationCity4',$error_messg);
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $fromCityAry ;
				}													
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}
		}
		if($this->idServiceType==2) //DTW
		{
			// if we have both city and post code 
			if(!empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				if(!$this->isCityExistsByPostCode($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode))
				{
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_FROM')." ".$this->szOriginCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_FROM');
					$this->addError('szOriginCity',$error_messg);
				}													
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}
			
			// if we don't have any one 
			if(empty($this->szOriginCity) && empty($this->szOriginPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
				$this->addError('szOriginCity',$error_messg);
			}
			
			
			// if we have only from city  
			if(!empty($this->szOriginCity) && empty($this->szOriginPostCode))
			{
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry) ;
				if(!$fromCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
					$this->addError('szOriginCity',$error_messg);
				}
				else if(empty($this->szOriginPostCode))
				{
					$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_FROM');
					$this->addError('szOriginCity',$error_messg);
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}													
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}			
	
			// if we have only post code the  we first fetch city which belongs to this country and post code then check for multiregion 
			if(empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				$fromCityAry=array();
				$this->szOriginCity = $this->getAllCityByPostCode($this->szOriginCountry,$this->szOriginPostCode);
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry) ;
				if(!$fromCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
					$this->addError('szOriginCity',$error_messg);
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}									
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}		
			
			//if we have both to city and postcode .
			if(!empty($this->szDestinationCity) && !empty($this->szDestinationCity))
			{
				if(!$this->isCityExistsByPostCode($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode))
				{
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_TO')." ".$this->szDestinationCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_TO');
					$this->addError('szOriginCity',$error_messg);
				}									
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}	
			// if we don't have any 
			if(empty($this->szDestinationCity) && empty($this->szDestinationPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
				$this->addError('szOriginCity',$error_messg);
			}
							
			// if we have onlyc city 
		    if(!empty($this->szDestinationCity) && empty($this->szDestinationPostCode) )
			{
				if(!$this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry))
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_TO');
					$this->addError('szDestinationCity',$error_messg);
				}					
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
				
			}				
			
			// if we have only post codes 
			if(empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				$toCityAry=array();
				$this->szDestinationCity = $this->getAllCityByPostCode($this->szDestinationCountry,$this->szDestinationPostCode);
				$toCityAry = $this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry) ;
				if(!$toCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szOriginCity',$error_messg);
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $fromCityAry ;
				}					
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
				
			}
		}
		if($this->idServiceType==3) //WTD
		{
			// if we have both city and post code 
			if(!empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				if(!$this->isCityExistsByPostCode($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode))
				{
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_FROM')." ".$this->szOriginCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_FROM');
					$this->addError('szOriginCity',$error_messg);
				}
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}
			
			// if we don't have any one 
			if(empty($this->szOriginCity) && empty($this->szOriginPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
				$this->addError('szOriginCity',$error_messg);
			}
			
			
			// if we have only from city  
			if(!empty($this->szOriginCity) && empty($this->szOriginPostCode))
			{
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry) ;
				if(!$fromCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
					$this->addError('szOriginCity',$error_messg);
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}			
	
			// if we have only post code the  we first fetch city which belongs to this country and post code then check for multiregion 
			if(empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				$fromCityAry=array();
				$this->szOriginCity = $this->getAllCityByPostCode($this->szOriginCountry,$this->szOriginPostCode);
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry) ;
				if(!$fromCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
					$this->addError('szOriginCity',$error_messg);
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}		
			
			//if we have both to city and postcode .
			if(!empty($this->szDestinationCity) && !empty($this->szDestinationCity))
			{
				if(!$this->isCityExistsByPostCode($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode))
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szDestinationCity',$error_messg);
				}				
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}
				
			// if we don't have any 
			if(empty($this->szDestinationCity) && empty($this->szDestinationPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
				$this->addError('szDestinationCity',$error_messg);
			}
							
			// if we have onlyc city 
		    if(!empty($this->szDestinationCity) && empty($this->szDestinationPostCode) )
			{
				if(!$this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry))
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_TO');
					$this->addError('szDestinationCity',$error_messg);
				}				
				else if(empty($this->szDestinationPostCode))
				{
					$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_TO');
					$this->addError('szDestinationCity',$error_messg);
				}				
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}				
			
			// if we have only post codes 
			if(empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				$toCityAry=array();
				$this->szDestinationCity = $this->getAllCityByPostCode($this->szDestinationCountry,$this->szDestinationPostCode);
				$toCityAry = $this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry) ;
				if(!$toCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szDestinationCity',$error_messg);
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $fromCityAry ;
				}
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}
		}
		if($this->idServiceType==4) //WTW
		{
			// if we have both city and post code 
			if(!empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				if(!$this->isCityExistsByPostCode($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode))
				{
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_FROM')." ".$this->szOriginCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_FROM');
					$this->addError('szOriginCity',$error_messg);
				}				
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			
			}
			
			// if we don't have any one 
			if(empty($this->szOriginCity) && empty($this->szOriginPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
				$this->addError('szOriginCity',$error_messg);
			}
			
			
			// if we have only from city  
			if(!empty($this->szOriginCity) && empty($this->szOriginPostCode))
			{
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry) ;
				if(!$fromCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
					$this->addError('szOriginCity',$error_messg);
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}			
	
			// if we have only post code the  we first fetch city which belongs to this country and post code then check for multiregion 
			if(empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				$fromCityAry=array();
				$this->szOriginCity = $this->getAllCityByPostCode($this->szOriginCountry,$this->szOriginPostCode);
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry) ;
				if(!$fromCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
					$this->addError('szOriginCity',$error_messg);
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}								
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}		
			
			//if we have both to city and postcode .
			if(!empty($this->szDestinationCity) && !empty($this->szDestinationCity))
			{
				if(!$this->isCityExistsByPostCode($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode))
				{
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_TO')." ".$this->szDestinationCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_TO');
					$this->addError('szDestinationCity',$error_messg);
				}
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}	
			// if we don't have any 
			if(empty($this->szDestinationCity) && empty($this->szDestinationPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
				$this->addError('szDestinationCity',$error_messg);
			}
							
			// if we have only city 
		    if(!empty($this->szDestinationCity) && empty($this->szDestinationPostCode) )
			{
				$toCityAry = $this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry) ;
				if(!$toCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szDestinationCity',$error_messg);
				}								
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}				
			
			// if we have only post codes 
			if(empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				$toCityAry=array();
				$this->szDestinationCity = $this->getAllCityByPostCode($this->szDestinationCountry,$this->szDestinationPostCode);
				$toCityAry = $this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry) ;
				if(!$toCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szDestinationCity',$error_messg);
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $fromCityAry ;
				}				
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}
		}
	}
	*/
	
	function loadPostCode($idPostCode)
	{
		if($idPostCode>0)
		{
			$query="
				SELECT
					id,
					szPostCode,
					szLat,
					szLng,
					szCity,
					szRegion1 
				FROM	
					".__DBC_SCHEMATA_POSTCODE__."	
				WHERE
					id = '".(int)$idPostCode."'	
			";
			//echo "<br /> ".$query ;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result);
					$this->idPostCode = sanitize_all_html_input(trim($row['id']));	
					$this->szPostCode =strtoupper(sanitize_all_html_input(trim($row['szPostCode'])));
					$this->szPostcodeLatitude = sanitize_all_html_input(trim($row['szLat']));
					$this->szPostcodeLongitude = sanitize_all_html_input(trim($row['szLng']));
					
					if(!empty($row['szCity']))
					{
						$this->szPostcodeCity = sanitize_all_html_input(trim($row['szCity']));
					}
					else
					{
						$this->szPostcodeCity = sanitize_all_html_input(trim($row['szRegion1']));
					}
					$this->szRegion1 = sanitize_all_html_input(trim($row['szRegion1']));
					return true;
				}
			}
			else
			{
				return false ;
				//log error 
			}
		}
		else
		{
			return false;
		}
	}
        
        function getAllPaymentTypes()
        {
            $query="
                SELECT
                    id, 
                    szFriendlyName,
                    szPaymentCode,
                    iActive 
                FROM	
                    ".__DBC_SCHEMATA_PAYMENT_TYPES__."
                WHERE
                  iActive = '1' 
                ORDER BY
                    szFriendlyName ASC
            ";		
            //echo "<br>".$query."<br>" ;
            if($result=$this->exeSQL($query))
            { 
                $ctr = 0;
                while($row = $this->getAssoc($result))
                {
                    $ret_ary[$ctr] = $row;
                    $ctr++;
                }
                return $ret_ary;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        } 
	
	function getCountryName($id=false,$szCountryIsoCode=false,$iLanguage=__LANGUAGE_ID_ENGLISH__)
	{
            if((int)$id>0 || !empty($szCountryIsoCode))
            {
                if($id>0)
                {
                    $query_and = " c.id = '".(int)$id."'";

                    $query_select = "cnm.szName AS szCountryName";
                }

                if(!empty($szCountryIsoCode))
                {
                    $query_and = " c.szCountryISO = '".mysql_escape_custom(trim($szCountryIsoCode))."' ";
                    $query_select = " c.id ";
                }

                $query="
                    SELECT
                        $query_select
                    FROM
                        ".__DBC_SCHEMATA_COUNTRY__." AS c
                    INNER JOIN
                        ".__DBC_SCHEMATA_COUNTRY_NAME_MAPPING__." AS cnm
                    ON
                        cnm.idCountry=c.id         
                    WHERE
                        cnm.idLanguage='".(int)$iLanguage."'
                    AND
                        $query_and		
                ";
                //echo "<br>".$query."<br />";
                if($result=$this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        $row=$this->getAssoc($result);

                        if($id>0)
                        {
                            return $row['szCountryName'];
                        }
                        else if(!empty($szCountryIsoCode))
                        {
                            return $row['id'];
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }		
	}
	
	function currencyConversion()
	{
	
		$f = fopen(__APP_PATH_LOGS__."/currencyConversion.log", "a");
		fwrite($f, "\n\n###################Cronjob Started-:".date("d-m-Y h:i:s")."#######################\n");
	
		$query="
			SELECT
				id,
				szCurrency
			FROM	
				".__DBC_SCHEMATA_CURRENCY__."
			WHERE
				szCurrency<>'USD'
		";
		fwrite($f, "\n------------------Select Query-:".$query."-----------------------\n");
		if($result=$this->exeSQL($query))
		{
			$currencyAry=array();
			$ctr = 0;
			while($row=$this->getAssoc($result))
			{
				fwrite($f, "\n------------------Started For Currency-:".$row['szCurrency']."-----------------------\n");
				$amount=currency_convert($row['szCurrency']);
				$amount=round($amount,6);
				fwrite($f, "\n------------------Convert Amount-:".$amount."-----------------------\n");
				$query="
					INSERT INTO
					".__DBC_SCHEMATA_CURRENCY_EXCHANGE_RATE__."
					(
						idCurrency,
						fUsdValue,
						dtDate
					)
						VALUES
					(
						'".(int)$row['id']."',
						'".$amount."',
						NOW()
					)
				";
				fwrite($f, "\n------------------Insert Query-:".$query."-----------------------\n");
				$result1=$this->exeSQL($query);
				
				fwrite($f, "\n------------------End For Currency-:".$row['szCurrency']."-----------------------\n");
			}
			//return $currencyAry ;
		}
		fwrite($f, "\n#########################Cronjob End-:".date("d-m-Y h:i:s")."##########################\n\n");
	}
	
	function deleteTempSearchLog()
	{
		$date=date('Y-m-d H:i:s',strtotime('-'.__TIME_DELETE_TEMP_SEARCH_LOG__));
		
		$query="
			DELETE FROM
				".__DBC_SCHEMATA_TEMP_SEARCH_RESULT__."
			WHERE
				dtCreatedOn<='".mysql_escape_custom($date)."'
		";
		if($result=$this->exeSQL($query))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function deleteSessionData()
	{
		$date=date('Y-m-d',strtotime('-'.__TIME_DELETE_SESSION_DATA__));
		
		$query="
			DELETE FROM
				".__DBC_SCHEMATA_SESSIONS__."
			WHERE
				date(session_expiration)<='".mysql_escape_custom($date)."'
		";
		if($result=$this->exeSQL($query))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function validateCargoDetails($data)
	{
		$kWHSSearch=new cWHSSearch();
		$maxCargoLength = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_DIMENSION_LENGTH__');
		$maxCargoWidth = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_DIMENSION_WIDTH__');
		$maxCargoHeight = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_DIMENSION_HEIGHT__');
		
		$maxCargoWeight = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_WEIGHT__');
		$maxCargoQty = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_QUANTITY__');
		
		$lengthCtr = count($data['iLength']);
		
		$tempCargoAry = array();
		$tempCargoAry['idCargo'] = $data['idCargo'];
		$tempCargoAry['iLength'] = $data['iLength'];
		$tempCargoAry['iWidth'] = $data['iWidth'];
		$tempCargoAry['iHeight'] = $data['iHeight'];
		$tempCargoAry['iWeight'] = $data['iWeight'];
		$tempCargoAry['idCargoMeasure'] = $data['idCargoMeasure'];
		$tempCargoAry['idWeightMeasure'] = $data['idWeightMeasure'];
		$tempCargoAry['iQuantity'] = $data['iQuantity'];
		$tempCargoAry['iDangerCargo'] = $data['iDangerCargo'];
		$this->tempCargoAry = $tempCargoAry ;
		
		$CargoMeasureCtr = count($data['idCargoMeasure']);
		for($i=1;$i<=$CargoMeasureCtr;$i++)
		{
			$this->idCargoMeasure[$i]=$data['idCargoMeasure'][$i];
		}
			
    	$WeightMeasureCtr = count($data['idWeightMeasure']);
		for($i=1;$i<=$WeightMeasureCtr;$i++)
		{
			$this->idWeightMeasure[$i]=$data['idWeightMeasure'][$i];
		}
		
		// putting data into temp varible 
		$maxCargoLength_temp = $maxCargoLength ;
		
		for($i=1;$i<=$lengthCtr;$i++)
		{
			if($this->idCargoMeasure[$i]==1)  // cm
			{
				$maxCargoLength = $maxCargoLength_temp ;
				$cargo_measure = 'cm' ;
			}
			else
			{
				$fCmFactor = $kWHSSearch->getCargoFactor($this->idCargoMeasure[$i]);
				$maxCargoLength = ceil($maxCargoLength_temp * $fCmFactor);
				$cargo_measure = 'Inch' ;
			}
			$this->set_iLength($data['iLength'][$i],$i,$maxCargoLength,$cargo_measure);
		}
		
		$widthCtr = count($data['iWidth']);
		
		// putting data into temp varible 
		$maxCargoWidth_temp = $maxCargoWidth ;
		
		for($i=1;$i<=$widthCtr;$i++)
		{
			if($this->idCargoMeasure[$i]==1)  // cm
			{
				$maxCargoWidth = $maxCargoWidth_temp ;
				$cargo_measure = 'cm' ;
			}
			else
			{
				$fCmFactor = $kWHSSearch->getCargoFactor($this->idCargoMeasure[$i]);
				$maxCargoWidth = ceil($maxCargoWidth_temp * $fCmFactor);
				$cargo_measure = 'Inch' ;
			}
			$this->set_iWidth($data['iWidth'][$i],$i,$maxCargoWidth,$cargo_measure);
		}
		
		$heightCtr = count($data['iHeight']);
		$maxCargoHeight_temp = $maxCargoHeight ;
		for($i=1;$i<=$heightCtr;$i++)
		{
			if($this->idCargoMeasure[$i]==1)  // cm
			{
				$maxCargoHeight = $maxCargoHeight_temp ;
				$cargo_measure = 'cm' ;
			}
			else
			{
				$fCmFactor = $kWHSSearch->getCargoFactor($this->idCargoMeasure[$i]);
				$maxCargoHeight = ceil($maxCargoHeight_temp * $fCmFactor);
				$cargo_measure = 'Inch' ;
			}
			$this->set_iHeight($data['iHeight'][$i],$i,$maxCargoHeight,$cargo_measure);
		}
		
		$qtyCtr = count($data['iQuantity']);
		for($i=1;$i<=$qtyCtr;$i++)
		{
			$this->set_iQuantity($data['iQuantity'][$i],$i,$maxCargoQty);
		}
		
	    $weightCtr = count($data['iWeight']);
	    $maxCargoWeight_temp = $maxCargoWeight ;
		for($i=1;$i<=$weightCtr;$i++)
		{
			// converting cargo details into cubic meter 
			if($this->idWeightMeasure[$i]==1)  // kg
			{
				$maxCargoWeight = $maxCargoWeight_temp ;
				$weight_measure = 'Kg';
			}
			else
			{
				$fKgFactor = $kWHSSearch->getWeightFactor($this->idWeightMeasure[$i]);
				$maxCargoWeight = ceil($maxCargoWeight_temp * $fKgFactor);
				$weight_measure = 'Pounds';
			}
			$this->set_iWeight($data['iWeight'][$i],$i,$maxCargoWeight,$weight_measure);
		}	
		
		
		$idCargoCtr = count($data['idCargo']);
	    for($i=1;$i<=$idCargoCtr;$i++)
		{
			$this->idCargo[$i]=$data['idCargo'][$i];
		}
		
		$iDangerCargoCtr = count($data['iDangerCargo']);
		for($i=1;$i<=$iDangerCargoCtr;$i++)
		{
			if($data['iDangerCargo'][$i]=='Yes')
			{
				//$this->addError('iDangerCargo',t($this->t_base.'errors/DANGEROUS_CARGO_NOT_AVAILABLE'));
				$this->iDangerCargoFlag = 1 ;
			}
		}
	}
	
	function addCityNotFound($idCountry,$szCity)
	{
		if($idCountry>0 && !empty($szCity))
		{
			$szCountry =$this->getCountryName($idCountry);
			
			$query="
				INSERT INTO
					".__DBC_SCHEMATA_CITY_NOT_FOUND__."
				(
					idCountry,
					szCountry,
					szCity,
					dtCreatedOn					
				)
				VALUES
				(
					'".mysql_escape_custom(trim($idCountry))."',
					'".mysql_escape_custom(trim($szCountry))."',
					'".mysql_escape_custom(trim($szCity))."',
					NOW()
				)
			";
			//echo "<br>".$query."<br>" ;
			//die;
			if($result = $this->exeSQL($query))
			{			
				return true;
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			
			}
		}
	}
	
	function selectPostcode($szpostCity,$city,$idCountry,$limit)
	{
		$szpostCity = 	trim(sanitize_all_html_input($szpostCity));
		$this->szOriginCity	=	trim(sanitize_all_html_input($city));
		$idCountry	=	(int)(sanitize_all_html_input($idCountry));
		$limit		=	(int)(sanitize_all_html_input($limit));
			$query="
				SELECT
					szPostCode
				FROM	
					".__DBC_SCHEMATA_POSTCODE__."	
				WHERE";
			
			if($idCountry>0)
			{
				$query.="
						idCountry =".mysql_escape_custom($idCountry)."
					AND	
				";
			}
			if($this->szOriginCity!='')
			{
				$query.="
			(
				szCity = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity1 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity2 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity3 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity4 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity5 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity6 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity7 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity8 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity9 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity10 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szRegion1 = '".mysql_escape_custom(trim($this->szOriginCity))."'	
			OR
				szRegion2 = '".mysql_escape_custom(trim($this->szOriginCity))."'	
			OR
				szRegion3 = '".mysql_escape_custom(trim($this->szOriginCity))."'	
			OR
				szRegion4 = '".mysql_escape_custom(trim($this->szOriginCity))."'	
			)
			AND	
				";
			}
			$query.="
					(
					szPostCode like '".mysql_escape_custom($szpostCity)."%'
					)
				GROUP BY 
						szPostCode		
				ORDER BY 
					iPriority DESC,szPostCode ASC 
					LIMIT 
						0,".mysql_escape_custom($limit)."
			";
			//echo "<br />".$query."<br />";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					while($row=$this->getAssoc($result))
					{
						$postcodeArr[]=$row;
					}
					return $postcodeArr;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			
			}
	
	}
	function selectCity($szpostCity,$idCountry,$limit)
	{
		$szPostCode = 	trim(sanitize_all_html_input($szpostCity));
		$idCountry	=	(int)(sanitize_all_html_input($idCountry));
		$limit		=	(int)(sanitize_all_html_input($limit));
			$query="
				SELECT
					szCity,
					szCity1,
					szCity2,
					szCity3,
					szCity4,
					szCity5,
					szCity6,
					szCity7,
					szCity8,
					szCity9,
					szCity10,
					szRegion1,
					szRegion2,
					szRegion3,
					szRegion4
				FROM	
					".__DBC_SCHEMATA_POSTCODE__."	
					 USE INDEX (country_city_regions)
				WHERE";
			
			if($idCountry>0)
			{
				$query.="
						idCountry =".mysql_escape_custom($idCountry)."
					AND	
				";
			}
			
			$query.="
			(
				szCity LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity1 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity2 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity3 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity4 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity5 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity6 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity7 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity8 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity9 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity10 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szRegion1 LIKE '".mysql_escape_custom(trim($szPostCode))."%'	
			OR
				szRegion2 LIKE '".mysql_escape_custom(trim($szPostCode))."%'	
			OR
				szRegion3 LIKE '".mysql_escape_custom(trim($szPostCode))."%'	
			OR
				szRegion4 LIKE '".mysql_escape_custom(trim($szPostCode))."%'	
			)			
				ORDER BY 
					iPriority DESC, szCity ASC	
					LIMIT 
						0,".mysql_escape_custom($limit)."
			";
			//echo "<br />".$query."<br />";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					while($row=$this->getAssoc($result))
					{
						$postcodeArr[]=$row;
					}
					return $postcodeArr;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			
			}
	
	}
	function selectPostCodeDetails($postCodeArr,$from=0,$perPage=__CONTENT_PER_PAGE_POSTCODE__)
	{	
		$cityPostCodeArr  		=	'';
		$this->set_szCountry((int)sanitize_all_html_input($postCodeArr['szOriginCountry']));
		$this->set_szSearchPostCode(trim(sanitize_all_html_input($postCodeArr['szOriginPostCode'])));
		$this->set_szSearchCity(trim(sanitize_all_html_input($postCodeArr['szOriginCity'])));
		if($this->szOriginCity =='' && $this->szOriginPostCode == '')
		{
			$this->error = true;
			$this->addError( "postcodeOrCity" , t($this->t_base_error_mang.'fields/postcodeOrCity_required') );
			return false;
		}
		if($this->error === true)
		{
			return false;
		}
		$query="
				SELECT
					`id`,
					`szPostCode`, 
					`szLat`, 
					`szLng`, 
					`szRegion1`, 
					`szRegion2`, 
					`szRegion3`, 
					`szRegion4`, 
					`szCity`
				FROM	
					".__DBC_SCHEMATA_POSTCODE__."	
				WHERE";
			
			if($this->szOriginCountry > 0)
			{
				$query.="
							idCountry =".mysql_escape_custom($this->szOriginCountry)."
						AND
							iActive = 1
				";
				
			}
		if($this->szOriginCity != '')
		{	
		$query.= "
			AND	
			(
				szCity = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity1 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity2 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity3 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity4 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity5 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity6 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity7 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity8 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity9 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity10 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szRegion1 = '".mysql_escape_custom(trim($this->szOriginCity))."'	
			OR
				szRegion2 = '".mysql_escape_custom(trim($this->szOriginCity))."'	
			OR
				szRegion3 = '".mysql_escape_custom(trim($this->szOriginCity))."'	
			OR
				szRegion4 = '".mysql_escape_custom(trim($this->szOriginCity))."'	
			)	
		";
		}
		if($this->szOriginPostCode != '')
		{
			
			$query.= " 
					 AND
						szPostCode = '".mysql_escape_custom(trim($this->szOriginPostCode))."'
						";
		}
			
			$query.= " 
					ORDER BY 
						szPostCode ASC
					LIMIT ".(int)$from.",".(int)$perPage."	
						";
			//echo $query;
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					while($row=$this->getAssoc($result))
					{
						$postcodeArr[]=$row;
					}
					return $postcodeArr;
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			
			}
	}
	function countPostCodeDetails($postCodeArr)
	{	
		$cityPostCodeArr  		=	'';
		$this->set_szCountry((int)sanitize_all_html_input($postCodeArr['szOriginCountry']));
		$this->set_szSearchPostCode(trim(sanitize_all_html_input($postCodeArr['szOriginPostCode'])));
		$this->set_szSearchCity(trim(sanitize_all_html_input($postCodeArr['szOriginCity'])));
		if($this->szOriginCity =='' && $this->szOriginPostCode == '')
		{
			$this->error = true;
			$this->addError( "postcodeOrCity" , t($this->t_base_error_mang.'fields/postcodeOrCity_required') );
			return false;
		}
		if($this->error === true)
		{
			return false;
		}
		$query="
				SELECT
					count(`id`) as total
				FROM	
					".__DBC_SCHEMATA_POSTCODE__."	
				WHERE";
			
			if($this->szOriginCountry > 0)
			{
				$query.="
							idCountry =".mysql_escape_custom($this->szOriginCountry)."
						AND
							iActive = 1
				";
				
			}
		if($this->szOriginCity != '')
		{	
		$query.= "
			AND	
			(
				szCity = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity1 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity2 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity3 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity4 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity5 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity6 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity7 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity8 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity9 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szCity10 = '".mysql_escape_custom(trim($this->szOriginCity))."'
			OR
				szRegion1 = '".mysql_escape_custom(trim($this->szOriginCity))."'	
			OR
				szRegion2 = '".mysql_escape_custom(trim($this->szOriginCity))."'	
			OR
				szRegion3 = '".mysql_escape_custom(trim($this->szOriginCity))."'	
			OR
				szRegion4 = '".mysql_escape_custom(trim($this->szOriginCity))."'	
			)	
		";
		}
		if($this->szOriginPostCode != '')
		{
			
			$query.= " 
					 AND
						szPostCode = '".mysql_escape_custom(trim($this->szOriginPostCode))."'
						";
		}
			
			if($result=$this->exeSQL($query))
			{
					$row=$this->getAssoc($result);
					return $row['total'];
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			
			}
	}
	function selectEachPostcode($id)
	{
		$this->set_idPostCode((int)sanitize_all_html_input($id));
		
		if($this->idPostCode>0)
		{
			$query="
					SELECT
					  `id`,
					  `szPostCode`,
					  `szLat`, 
					  `szLng`, 
					  `szRegion1`,
					  `szRegion2`,
					  `szRegion3`, 
					  `szRegion4`, 
					  `szCity`, 
					  `szCity1`, 
					  `szCity2`, 
					  `szCity3`, 
					  `szCity4`, 
					  `szCity5`, 
					  `szCity6`, 
					  `szCity7`, 
					  `szCity8`, 
					  `szCity9`, 
					  `szCity10`,
					  iPriority
					FROM	
						".__DBC_SCHEMATA_POSTCODE__."	
					WHERE
					    id =".(int)mysql_escape_custom($this->idPostCode)."
					";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{	
					$row=$this->getAssoc($result);
					return json_encode($row);
				}
				else
				{
					return array();
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			
			}	
		}
	}
	function updateTablePostcode($postCodeArr)
	{
		  $this->set_id((int)sanitize_all_html_input($postCodeArr['id']));
		  $this->set_idCountry((int)sanitize_all_html_input($postCodeArr['idCountry']));
		  $this->set_fPostCode(sanitize_all_html_input($postCodeArr['szPostCode']));
		  $this->set_szLat(sanitize_all_html_input($postCodeArr['szLat']));
		  if($this->szLat!=NULL)
		  {
			  if($this->szLat< -90 || $this->szLat>90)
			  {
			 	 $this->addError( "szLatitude" , t($this->t_base_error_mang.'fields/latitude_must_be'));
			  }
		
		  }
		  $this->set_szLng(sanitize_all_html_input($postCodeArr['szLng'])); 
		  if($this->szLng!=NULL)
		  {
			  if($this->szLng< -180 || $this->szLng>180)
			  {
				$this->addError( "szLongitude" , t($this->t_base_error_mang.'fields/longitude_must_be'));
			  }
		
	      }
		  
		  
		 
		  $this->set_szRegion1(sanitize_all_html_input($postCodeArr['szRegion1']));
		  $this->set_szRegion2(sanitize_all_html_input($postCodeArr['szRegion2']));
		  $this->set_szRegion3(sanitize_all_html_input($postCodeArr['szRegion3'])); 
		  $this->set_szRegion4(sanitize_all_html_input($postCodeArr['szRegion4'])); 
		  $this->set_szCity0(sanitize_all_html_input($postCodeArr['szCity'])); 
		  $this->set_szCity1(sanitize_all_html_input($postCodeArr['szCity1'])); 
		  $this->set_szCity2(sanitize_all_html_input($postCodeArr['szCity2'])); 
		  $this->set_szCity3(sanitize_all_html_input($postCodeArr['szCity3'])); 
		  $this->set_szCity4(sanitize_all_html_input($postCodeArr['szCity4'])); 
		  $this->set_szCity5(sanitize_all_html_input($postCodeArr['szCity5'])); 
		  $this->set_szCity6(sanitize_all_html_input($postCodeArr['szCity6'])); 
		  $this->set_szCity7(sanitize_all_html_input($postCodeArr['szCity7'])); 
		  $this->set_szCity8(sanitize_all_html_input($postCodeArr['szCity8'])); 
		  $this->set_szCity9(sanitize_all_html_input($postCodeArr['szCity9'])); 
		  $this->set_szCity10(sanitize_all_html_input($postCodeArr['szCity10']));
		   $this->set_iPriority(sanitize_all_html_input($postCodeArr['iPriority']));
		  if(empty($this->szPostCode) && empty($this->szCity))
		  {
		  	$this->addError('szPostCode','Enter city or postcode');
		  	$this->error = true;
		  }
		  if((int)$this->iPriority>0)
		  {
		  	 if($this->isPriorityExists($this->iPriority,$this->id))
		  	 {
		  	 	$this->addError('iPriority',t($this->t_base_error_mang.'fields/priority_already_exists'));
		  	 	return false;
		  	 }
		  }
		  if($this->error===true)
		  {
		  	return false;
		  	exit;
		  }
		  else
		  {
			$query="
		  		UPDATE
		  			".__DBC_SCHEMATA_POSTCODE__."
		  			SET
					  `szPostCode`	='".mysql_escape_custom($this->szPostCode)."',
					  `szLat`		='".mysql_escape_custom($this->szLat)."',
					  `szLng`		='".mysql_escape_custom($this->szLng)."', 
					  `szRegion1`	='".mysql_escape_custom($this->szRegion1)."',
					  `szRegion2`	='".mysql_escape_custom($this->szRegion2)."',
					  `szRegion3`	='".mysql_escape_custom($this->szRegion3)."',
					  `szRegion4`	='".mysql_escape_custom($this->szRegion4)."',
					  `szCity`		='".mysql_escape_custom($this->szCity)."',
					  `szCity1`		='".mysql_escape_custom($this->szCity1)."',
					  `szCity2`		='".mysql_escape_custom($this->szCity2)."',
					  `szCity3`		='".mysql_escape_custom($this->szCity3)."',
					  `szCity4`		='".mysql_escape_custom($this->szCity4)."',
					  `szCity5`		='".mysql_escape_custom($this->szCity5)."',
					  `szCity6`		='".mysql_escape_custom($this->szCity6)."',
					  `szCity7`		='".mysql_escape_custom($this->szCity7)."',
					  `szCity8`		='".mysql_escape_custom($this->szCity8)."',
					  `szCity9`		='".mysql_escape_custom($this->szCity9)."',
					  `szCity10`	='".mysql_escape_custom($this->szCity10)."',
					  `iPriority`	='".mysql_escape_custom($this->iPriority)."',					  
					   iManuallyUpdated = 1,
		  			   dtManuallyUpdated = NOW()
					WHERE
					    id =".(int)mysql_escape_custom($this->id)."
					";
		  		if($result=$this->exeSQL($query))
				{
					return true;
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				
				}	
		  }
	}
	function addTablePostcode($postCodeArr)
	{
		  $this->set_idCountry((int)sanitize_all_html_input($postCodeArr['idCountry']));
		  $this->set_fPostCode(sanitize_all_html_input($postCodeArr['szPostCode']));
		  $this->set_szLat(sanitize_all_html_input($postCodeArr['szLat']));
		  if($this->szLat!=NULL)
		  {
			  if($this->szLat< -90 || $this->szLat>90)
			  {
			 	 $this->addError( "szLatitude" , t($this->t_base_error_mang.'fields/latitude_must_be'));
			  }
		
		  }
		  $this->set_szLng(sanitize_all_html_input($postCodeArr['szLng'])); 
		  if($this->szLng!=NULL)
		  {
			  if($this->szLng< -180 || $this->szLng>180)
			  {
				$this->addError( "szLongitude" , t($this->t_base_error_mang.'fields/longitude_must_be'));
			  }
		
	      }
		  $this->set_szRegion1(sanitize_all_html_input($postCodeArr['szRegion1']));
		  $this->set_szRegion2(sanitize_all_html_input($postCodeArr['szRegion2']));
		  $this->set_szRegion3(sanitize_all_html_input($postCodeArr['szRegion3'])); 
		  $this->set_szRegion4(sanitize_all_html_input($postCodeArr['szRegion4'])); 
		  $this->set_szCity0(sanitize_all_html_input($postCodeArr['szCity'])); 
		  $this->set_szCity1(sanitize_all_html_input($postCodeArr['szCity1'])); 
		  $this->set_szCity2(sanitize_all_html_input($postCodeArr['szCity2'])); 
		  $this->set_szCity3(sanitize_all_html_input($postCodeArr['szCity3'])); 
		  $this->set_szCity4(sanitize_all_html_input($postCodeArr['szCity4'])); 
		  $this->set_szCity5(sanitize_all_html_input($postCodeArr['szCity5'])); 
		  $this->set_szCity6(sanitize_all_html_input($postCodeArr['szCity6'])); 
		  $this->set_szCity7(sanitize_all_html_input($postCodeArr['szCity7'])); 
		  $this->set_szCity8(sanitize_all_html_input($postCodeArr['szCity8'])); 
		  $this->set_szCity9(sanitize_all_html_input($postCodeArr['szCity9'])); 
		  $this->set_szCity10(sanitize_all_html_input($postCodeArr['szCity10']));
		  $this->set_iPriority(sanitize_all_html_input($postCodeArr['iPriority']));
		  
		  if(empty($this->szPostCode) && empty($this->szCity))
		  {
		  	$this->addError('szPostCode','Enter city or postcode');
		  	$this->error = true;
		  }
		  if((int)$this->iPriority>0)
		  {
		  	 if($this->isPriorityExists($this->iPriority))
		  	 {
		  	 	$this->addError('iPriority',t($this->t_base_error_mang.'fields/priority_already_exists'));
		  	 	return false;
		  	 }
		  }
		  if($this->error===true)
		  {
		  	return false;
		  	exit;
		  }
		  else
		  {
			$query="
		  		INSERT INTO
		  			".__DBC_SCHEMATA_POSTCODE__."
		  			SET
					  `szPostCode`	='".mysql_escape_custom($this->szPostCode)."',
					  `szLat`		='".mysql_escape_custom($this->szLat)."',
					  `szLng`		='".mysql_escape_custom($this->szLng)."', 
					  `szRegion1`	='".mysql_escape_custom($this->szRegion1)."',
					  `szRegion2`	='".mysql_escape_custom($this->szRegion2)."',
					  `szRegion3`	='".mysql_escape_custom($this->szRegion3)."',
					  `szRegion4`	='".mysql_escape_custom($this->szRegion4)."',
					  `szCity`		='".mysql_escape_custom($this->szCity)."',
					  `szCity1`		='".mysql_escape_custom($this->szCity1)."',
					  `szCity2`		='".mysql_escape_custom($this->szCity2)."',
					  `szCity3`		='".mysql_escape_custom($this->szCity3)."',
					  `szCity4`		='".mysql_escape_custom($this->szCity4)."',
					  `szCity5`		='".mysql_escape_custom($this->szCity5)."',
					  `szCity6`		='".mysql_escape_custom($this->szCity6)."',
					  `szCity7`		='".mysql_escape_custom($this->szCity7)."',
					  `szCity8`		='".mysql_escape_custom($this->szCity8)."',
					  `szCity9`		='".mysql_escape_custom($this->szCity9)."',
					  `szCity10`	='".mysql_escape_custom($this->szCity10)."',
					  `idCountry`	='".mysql_escape_custom($this->idCountry)."',
					  `iPriority`	='".mysql_escape_custom($this->iPriority)."',
					   iActive =1,
		  			   iManuallyUpdated = 1,
		  			   dtManuallyUpdated = NOW()
					";
				//echo $query;die;
		  		if($result=$this->exeSQL($query))
				{
					return true;
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				
				}	
		  }
	}
	function isPriorityExists($iPriority,$idPostcode=false)
	{
		if($iPriority)
		{
			if($idPostcode>0)
			{
				$query_and=" AND id <> '".(int)$idPostcode."' ";
			}
			
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_POSTCODE__."
				WHERE
				   iPriority = '".(int)$iPriority."'
				   $query_and
			";
			if($result=$this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					return true ;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$this->error = true;
				$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
				$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
				return false;
			
			}	
		}
	}
	function deletePostcode($id)
	{
		$id = (int)sanitize_all_html_input($id);
		if($id>0)
		{
			$query= 
				"
				UPDATE
		  			".__DBC_SCHEMATA_POSTCODE__."
		  		SET
		  			iActive =0,
		  			iManuallyUpdated = 1,
		  			dtManuallyUpdated = NOW()
		  		WHERE
		  			id = '".mysql_escape_custom($id)."'	
		  		";
				//echo $query;
				if($result=$this->exeSQL($query))
				{
					return true;
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				
				}
		  		
		}
	}
	function findForwarderOBOHaulageStauts($id)
	{
		$id = (int)sanitize_all_html_input($id);
		if($id>0)
		{
			$query= 
				"
				SELECT
				iHaulageOBO
				FROM
		  			".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
		  		WHERE
		  			id = '".mysql_escape_custom($id)."'	
		  		";
				//echo $query;
				if($result=$this->exeSQL($query))
				{
					$row=$this->getAssoc($result);
					return !(boolean)$row['iHaulageOBO'];
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				
				}
		  		
		}
	}
	function updateForwarderOBOHaulageStauts($id)
	{
		$id = (int)sanitize_all_html_input($id);
		if($id>0)
		{
			$query= 
				"
				UPDATE
		  			".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
		  		SET
		  			iHaulageOBO =1
		  		WHERE
		  			id = '".mysql_escape_custom($id)."'	
		  		";
				//echo $query;
				if($result=$this->exeSQL($query))
				{
					return true;
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				
				}
		  		
		}
	}
	function findForwarderTryItOutStauts($id)
	{
		$id = (int)sanitize_all_html_input($id);
		if($id>0)
		{
			$query= 
				"
				SELECT
					iTryItOut
				FROM
		  			".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
		  		WHERE
		  			id = '".mysql_escape_custom($id)."'	
		  		";
				//echo $query;
				if($result=$this->exeSQL($query))
				{
					$row=$this->getAssoc($result);
					return !(boolean)$row['iTryItOut'];
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				
				}
		  		
		}
	}
	function updateForwarderTryItOutStauts($id)
	{
		$id = (int)sanitize_all_html_input($id);
		if($id>0)
		{
			$query= 
				"
				UPDATE
		  			".__DBC_SCHEMATA_FORWARDERS_CONTACT__."
		  		SET
		  			iTryItOut =1
		  		WHERE
		  			id = '".mysql_escape_custom($id)."'	
		  		";
				//echo $query;
				if($result=$this->exeSQL($query))
				{
					return true;
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				
				}
		  		
		}
	}
	
	function validate_city_postcode_requirement($data,$type=false)
	{
		if(!empty($data))
		{
			if(!empty($type))
			{
				if($type =='ORIGIN')
				{
					$this->set_szOriginCountry(sanitize_all_html_input(trim($data['idOriginCountry']))); // id country origin
					$this->set_szOriginCity(sanitize_all_html_input(trim($data['szOriginCity'])));  // city name 
					//$this->set_szOriginPostCode(sanitize_all_html_input(trim($data['szOriginPostCode'])));
				}
				else if($type =='DESTINATION')
				{
					$this->set_szDestinationCountry(sanitize_all_html_input(trim($data['idDestinationCountry'])));
					$this->set_szDestinationCity(sanitize_all_html_input(trim($data['szDestinationCity'])));
					//$this->set_szDestinationPostCode(sanitize_all_html_input(trim($data['szDestinationPostCode'])));
				}
			}
			else
			{
				$this->set_szOriginCountry(sanitize_all_html_input(trim($data['idOriginCountry']))); // id country origin
				$this->set_szOriginCity(sanitize_all_html_input(trim($data['szOriginCity'])));  // city name 
				//$this->set_szOriginPostCode(sanitize_all_html_input(trim($data['szOriginPostCode'])));
				
				$this->set_szDestinationCountry(sanitize_all_html_input(trim($data['idDestinationCountry'])));
				$this->set_szDestinationCity(sanitize_all_html_input(trim($data['szDestinationCity'])));
				//$this->set_szDestinationPostCode(sanitize_all_html_input(trim($data['szDestinationPostCode'])));
			}
			$this->set_idServiceType(sanitize_all_html_input(trim($data['idServiceType'])));
			
			if($this->error===false)
			{			
				if(!empty($type))
				{
					// this function is called on blur 
					$this->validate_city_country_postcode_by_service_type_requirement($this,$type);
				}
				else
				{
					// this function called when clicked on Next Step button.
					$this->validate_city_country_postcode_by_service_type_requirement_Next_Step($this);
				}
			}
			else
			{
				return false;
			}
		}
	}
	function validate_timing_requirement($data)
	{
		if(!empty($data))
		{
			$this->set_dtTiming(sanitize_all_html_input(trim($data['dtTiming'])));
			
			if(!empty($this->dtTiming))
			{
				 $dateAry = explode("/",$this->dtTiming); 
				 $dtTiming_millisecond = strtotime($dateAry[2]."-".$dateAry[1]."-".$dateAry[0]);  // converting date 
				 $dtDate = date("d/m/Y",$dtTiming_millisecond);
				 
				 //echo " date time ms ".strtotime(date('Y-m-d'))."    ".date('d/m/Y');
				 //echo "<br><br> date time ms ".$dtTiming_millisecond."    ".date('d/m/Y',$dtTiming_millisecond);
				 
				 if(!checkdate($dateAry[1],$dateAry[0],$dateAry[2]))
	             {
	                $this->addError("dtTiming",t($this->t_base.'errors/TIMING_DATE')." ".t('Error/valid_date_landing_page'));
	             }
	             else if(($dtTiming_millisecond <= strtotime(date('Y-m-d'))) && ($this->idTimingType==2)) // Available at destination 
	             {
	             	$this->addError("dtTiming",t('Error/greater_then_today'));
	             }
	             else
	             {
	             	$this->dtTiming = $dateAry[2]."-".$dateAry[1]."-".$dateAry[0]." 00:00:00";  // YYYY/MM/DD H:I:S
	             }
			}
		}
	}	
	function validate_cargo_details_requirement($data)
	{
		if(!empty($data))
		{
			$kWHSSearch=new cWHSSearch();
			$maxCargoLength = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_DIMENSION_LENGTH__');
			$maxCargoWidth = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_DIMENSION_WIDTH__');
			$maxCargoHeight = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_DIMENSION_HEIGHT__');
			
			$maxCargoWeight = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_WEIGHT__');
			$maxCargoQty = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_QUANTITY__');
			
			$lengthCtr = count($data['iLength']);
			
			$CargoMeasureCtr = count($data['iLength']);
			for($i=1;$i<=$CargoMeasureCtr;$i++)
			{
				if($data['iDoNotKnowCargo']==1)
				{
					$this->idCargoMeasure[$i] = 1;
				}
				else 
				{
					$this->idCargoMeasure[$i] = $data['idCargoMeasure'][$i];
				}
			}
				
	    	$WeightMeasureCtr = count($data['iLength']);
			for($i=1;$i<=$WeightMeasureCtr;$i++)
			{
				if($data['iDoNotKnowCargo']==1)
				{
					$this->idWeightMeasure[$i] = 1;
				}
				else {
					$this->idWeightMeasure[$i] = $data['idWeightMeasure'][$i];
				}
			}
			// putting data into temp varible 
			$maxCargoLength_temp = $maxCargoLength ;
			
			for($i=1;$i<=$lengthCtr;$i++)
			{
				if($this->idCargoMeasure[$i]==1)  // cm
				{
					$maxCargoLength = $maxCargoLength_temp ;
					$cargo_measure = 'cm' ;
				}
				else
				{
					$fCmFactor = $kWHSSearch->getCargoFactor($this->idCargoMeasure[$i]);
					$maxCargoLength = ceil($maxCargoLength_temp * $fCmFactor);
					$cargo_measure = 'Inch' ;
				}
				$this->set_iLength($data['iLength'][$i],$i,$maxCargoLength,$cargo_measure);
			}
			
			$widthCtr = count($data['iWidth']);
			
			// putting data into temp varible 
			$maxCargoWidth_temp = $maxCargoWidth ;
			
			for($i=1;$i<=$widthCtr;$i++)
			{
				if($this->idCargoMeasure[$i]==1)  // cm
				{
					$maxCargoWidth = $maxCargoWidth_temp ;
					$cargo_measure = 'cm' ;
				}
				else
				{
					$fCmFactor = $kWHSSearch->getCargoFactor($this->idCargoMeasure[$i]);
					$maxCargoWidth = ceil($maxCargoWidth_temp * $fCmFactor);
					$cargo_measure = 'Inch' ;
				}
				$this->set_iWidth($data['iWidth'][$i],$i,$maxCargoWidth,$cargo_measure);
			}
			
			$heightCtr = count($data['iHeight']);
			$maxCargoHeight_temp = $maxCargoHeight ;
			for($i=1;$i<=$heightCtr;$i++)
			{
				if($this->idCargoMeasure[$i]==1)  // cm
				{
					$maxCargoHeight = $maxCargoHeight_temp ;
					$cargo_measure = 'cm' ;
				}
				else
				{
					$fCmFactor = $kWHSSearch->getCargoFactor($this->idCargoMeasure[$i]);
					$maxCargoHeight = ceil($maxCargoHeight_temp * $fCmFactor);
					$cargo_measure = 'Inch' ;
				}
				$this->set_iHeight($data['iHeight'][$i],$i,$maxCargoHeight,$cargo_measure);
			}
			
			$qtyCtr = count($data['iQuantity']);
			for($i=1;$i<=$qtyCtr;$i++)
			{
				$this->set_iQuantity($data['iQuantity'][$i],$i,$maxCargoQty);
			}
			
		    $weightCtr = count($data['iWeight']);
		    $maxCargoWeight_temp = $maxCargoWeight ;
			for($i=1;$i<=$weightCtr;$i++)
			{
				// converting cargo details into cubic meter 
				if($this->idWeightMeasure[$i]==1)  // kg
				{
					$maxCargoWeight = $maxCargoWeight_temp ;
					$weight_measure = 'Kg';
				}
				else
				{
					$fKgFactor = $kWHSSearch->getWeightFactor($this->idWeightMeasure[$i]);
					$maxCargoWeight = ceil($maxCargoWeight_temp * $fKgFactor);
					$weight_measure = 'Pounds';
				}
				$this->set_iWeight($data['iWeight'][$i],$i,$maxCargoWeight,$weight_measure);
			}	
			
			
			$idCargoCtr = count($data['idCargo']);
		    for($i=1;$i<=$idCargoCtr;$i++)
			{
				$this->idCargo[$i]=$data['idCargo'][$i];
			}
			
			$iDangerCargoCtr = count($data['iDangerCargo']);
			for($i=1;$i<=$iDangerCargoCtr;$i++)
			{
				if($data['iDangerCargo'][$i]=='Yes')
				{
					$this->addError('iDangerCargo',t($this->t_base.'errors/DANGEROUS_CARGO_NOT_AVAILABLE'));
					$this->iDangerousCargoFlag = 1; 
					return false;
				}
			}
		}
	}
	function updateForgotPassword($id=0,$data,$case='')
	{
		$this->set_id(trim(sanitize_all_html_input($id)));
		if($this->id>0)
		{
			$this->set_szPassword(trim(sanitize_all_html_input($data['szNewPassword'])));
			$this->set_szReTypePassword(trim(sanitize_all_html_input($data['szConPassword'])));
			
			if ($this->error == true)
			{
				return false;
			}
			if(!empty($this->szPassword) && $this->szPassword!=$this->szRetypePassword)
			{
				$this->addError( "szRetypePassword" , t($this->t_base_error_mang.'messages/match_password') );
				return false;
			}
		
			if($case == 'CUSTOMER')
			{
				$tbl =__DBC_SCHEMATA_USERS__;
			}
			else if($case == 'FORWARDER')
			{
				$tbl =__DBC_SCHEMATA_FORWARDERS_CONTACT__;
			}
			else if($case == 'MANAGEMENT')
			{
				$tbl =__DBC_SCHEMATA_MANAGEMENT__;
			}
			else
			{
				return false;
				exit;
			}
			$query = "
				UPDATE
					".$tbl."
				SET	
					szPassword='".mysql_escape_custom(md5($this->szPassword))."',
					iPasswordUpdated = 0
				WHERE
					id =".(int)$this->id."	
			";
			//echo $query;
				if($result=$this->exeSQL($query))
				{
					return true;
				}
				else
				{
					$this->error = true;
					$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
					$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
					return false;
				
				}
		}
		else 
		{
			return false;
		}
	} 
	
	function validate_city_postcode_()
	{
	
		$this->iOriginError = false ;
		$this->iDestinationError = false ;
		if($this->idServiceType== __SERVICE_TYPE_DTD__) //DTD
		{			
			// if we have both city and post code 
			if(!empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				if(!$this->isCityExistsByPostCode($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode))
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_FROM')." ".$this->szOriginCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_FROM');
					$this->addError('szOriginCity1',$error_messg);
					$this->iOriginError = true ;
				}																	
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}
			else if(empty($this->szOriginCity) && empty($this->szOriginPostCode)) // if we don't have any one 
			{
				$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_FROM');
				$this->addError('szOriginCity',$error_messg);
				$this->iOriginError = true ;
			}  
			else if(!empty($this->szOriginCity) && empty($this->szOriginPostCode)) // if we have only from city
			{
				$fromCityAry = $this->check_city_country($this->szOriginCity,$this->szOriginCountry);
				
				if(empty($fromCityAry))
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
					$this->addError('szOriginCity3',$error_messg);
					$this->iOriginError = true ;
				}
				else if(($this->emptyPostcodeCtr == 1 || $this->partialEmptyPostcodeCtr==1) && !empty($fromCityAry))
				{
					$this->idOriginPostCode = $this->idEmptyPostCode;
				}
				else if($this->emptyPostcodeCtr > 1 && !empty($fromCityAry))
				{
					//multi region; 
					$this->multiEmaptyPostcodeFromCityAry = $fromCityAry ;
				}
				else if($this->partialEmptyPostcodeCtr > 1 && !empty($fromCityAry))
				{
					//multi region; 
					$this->multiPartialEmaptyPostcodeFromCityAry = $fromCityAry ;
				}
				/*
				else if($this->emptyPostcodeCtr == 1 && !empty($fromCityAry))
				{
					$this->idOriginPostCode = $this->idEmptyPostCode;
				}
				*/
				else if(empty($this->szOriginPostCode) && ($this->szOriginCountry != __ID_COUNTRY_HONG_KONG__))
				{
					$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}																	
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}
			// if we have only post code the  we first fetch city which belongs to this country and post code then check for multiregion 
			else if(empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				$fromCityAry=array();
				$this->szOriginCity = $this->getAllCityByPostCode($this->szOriginCountry,$this->szOriginPostCode);
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode) ;
				if(!$fromCityAry)
				{ 
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
					$this->addError('szOriginCity4',$error_messg);
					$this->iOriginError = true ;
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}																	
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}		
			
			//if we have both to city and postcode .
			if(!empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				if(!$this->isCityExistsByPostCode($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode))
				{
					//call functions
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_TO')." ".$this->szDestinationCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_TO');
					$this->addError('szDestinationCity1',$error_messg);
					$this->iDestinationError = true ;
				}																	
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}	
			// if we don't have any 
			if(empty($this->szDestinationCity) && empty($this->szDestinationPostCode))
			{
				$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_TO');
				$this->addError('szDestinationCity4',$error_messg);
				$this->iDestinationError = true ;
			}
							
			// if we have onlyc city 
		    if(!empty($this->szDestinationCity) && empty($this->szDestinationPostCode))
			{		
				$toCityAry = array();		
				$toCityAry = $this->check_city_country($this->szDestinationCity,$this->szDestinationCountry) ;
				if(!$toCityAry)
				{
					//call function
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_TO');
					$this->addError('szDestinationCity3',$error_messg);
					$this->iDestinationError = true ;
				}
				else if(($this->emptyPostcodeCtr == 1 || $this->partialEmptyPostcodeCtr==1) && !empty($toCityAry))
				{
					$this->idDestinationPostCode = $this->idEmptyPostCode;
				}
				else if($this->emptyPostcodeCtr > 1 && !empty($toCityAry))
				{
					$this->multiEmaptyPostcodeToCityAry = $toCityAry ;
				}
				else if($this->partialEmptyPostcodeCtr > 1 && !empty($toCityAry))
				{
					$this->multiEmaptyPostcodeToCityAry = $toCityAry ;
				}
				/*
				else if($this->emptyPostcodeCtr == 1 && !empty($toCityAry))
				{
					$this->idDestinationPostCode = $this->idEmptyPostCode;
				}
				*/
				else if(empty($this->szDestinationPostCode) && ($this->szDestinationCountry != __ID_COUNTRY_HONG_KONG__))
				{
					$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_TO');
					$this->addError('szDestinationCity4',$error_messg);
					$this->iDestinationError = true ;
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
				}																	
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}				
			
			// if we have only post codes 
			if(empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				$toCityAry=array();
				$this->szDestinationCity = $this->getAllCityByPostCode($this->szDestinationCountry,$this->szDestinationPostCode);
				$toCityAry = $this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode) ;
				if(!$toCityAry)
				{
					//call function
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szDestinationCity4',$error_messg);
					$this->iDestinationError = true ;
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
				}													
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}
		}
		if(($this->idServiceType==(int)__SERVICE_TYPE_DTW__) || ($this->idServiceType==(int)__SERVICE_TYPE_DTP__)) //DTW
		{
			// if we have both city and post code 
			if(!empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				if(!$this->isCityExistsByPostCode($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode))
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_FROM')." ".$this->szOriginCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}													
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}
			
			// if we don't have any one 
			if(empty($this->szOriginCity) && empty($this->szOriginPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
				$this->addError('szOriginCity',$error_messg);
				$this->iOriginError = true ;
			}
			
			
			// if we have only from city  
			if(!empty($this->szOriginCity) && empty($this->szOriginPostCode))
			{			
				$fromCityAry = $this->check_city_country($this->szOriginCity,$this->szOriginCountry);
				if(empty($fromCityAry))
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
					$this->addError('szOriginCity3',$error_messg);
					$this->iOriginError = true ;
				}
				else if($this->emptyPostcodeCtr == 1 && !empty($fromCityAry))
				{
					$this->idOriginPostCode = $this->idEmptyPostCode;
				}
				else if($this->emptyPostcodeCtr > 1 && !empty($fromCityAry))
				{
					//multi region; 
					$this->multiEmaptyPostcodeFromCityAry = $fromCityAry ;
				}
				/*
				else if($this->emptyPostcodeCtr == 1 && !empty($fromCityAry))
				{
					$this->idOriginPostCode = $this->idEmptyPostCode;
				}
				*/
				else if(empty($this->szOriginPostCode) && ($this->szOriginCountry != __ID_COUNTRY_HONG_KONG__))
				{
					$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}																	
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}
			// if we have only post code the  we first fetch city which belongs to this country and post code then check for multiregion 
			if(empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				$fromCityAry=array();
				$this->szOriginCity = $this->getAllCityByPostCode($this->szOriginCountry,$this->szOriginPostCode);
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode);
				if(!$fromCityAry)
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}									
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}		
			
			//if we have both to city and postcode .
			if(!empty($this->szDestinationCity) && !empty($this->szDestinationCity))
			{
				if(!$this->isCityExistsByPostCode($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode))
				{	
					//call function
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_TO')." ".$this->szDestinationCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_TO');
					$this->addError('szOriginCity',$error_messg);
					$this->iDestinationError = true ;
				}									
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}	
			// if we don't have any 
			if(empty($this->szDestinationCity) && empty($this->szDestinationPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
				$this->addError('szOriginCity',$error_messg);
				$this->iDestinationError = true ;
			}
							
			// if we have onlyc city 
		    if(!empty($this->szDestinationCity) && empty($this->szDestinationPostCode) )
			{
				$toCityAry = $this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry) ;
				if(!$toCityAry)
				{	
					//call function
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_TO');
					$this->addError('szDestinationCity',$error_messg);
					$this->iDestinationError = true ;
				}
				elseif(count($toCityAry)>1)
				{
					$this->multiRegionToCityAry = $toCityAry ;					
				}
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}				
			
			// if we have only post codes 
			if(empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				$toCityAry=array();
				$this->szDestinationCity = $this->getAllCityByPostCode($this->szDestinationCountry,$this->szDestinationPostCode);
				$toCityAry = $this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode) ;
				if(!$toCityAry)
				{
					//call function
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szOriginCity',$error_messg);
					$this->iDestinationError = true ;
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
					$this->idDestinationPostCode = $this->idPostCode;
				}					
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
				
			}
		}
		if(($this->idServiceType==(int)__SERVICE_TYPE_WTD__) || ($this->idServiceType==(int)__SERVICE_TYPE_PTD__))
		{
			// if we have both city and post code 
			if(!empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				$this->iOriginError = false ;
				if(!$this->isCityExistsByPostCode($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode))
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_FROM')." ".$this->szOriginCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}			
			// if we don't have any one 
			if(empty($this->szOriginCity) && empty($this->szOriginPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
				$this->addError('szOriginCity',$error_messg);
				$this->iOriginError = true ;
			}
			// if we have only from city  
			if(!empty($this->szOriginCity) && empty($this->szOriginPostCode))
			{
				$this->iOriginError = false ;
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry) ;
				if(!$fromCityAry)
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}
			// if we have only post code the  we first fetch city which belongs to this country and post code then check for multiregion 
			if(empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				$this->iOriginError = false ;
				$fromCityAry=array();
				$this->szOriginCity = $this->getAllCityByPostCode($this->szOriginCountry,$this->szOriginPostCode);
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode);
				if(!$fromCityAry)
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}		
			
			//if we have both to city and postcode .
			if(!empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				$this->iDestinationError = false ;
				if(!$this->isCityExistsByPostCode($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode))
				{
					//call function
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szDestinationCity',$error_messg);
					$this->iDestinationError = true ;
				}				
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}				
			// if we don't have any 
			elseif(empty($this->szDestinationCity) && empty($this->szDestinationPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
				$this->addError('szDestinationCity',$error_messg);
				$this->iDestinationError = true ;
			}							
			// if we have only city 
		    if(!empty($this->szDestinationCity) && empty($this->szDestinationPostCode) )
			{		
				$this->iDestinationError = false ;
				$toCityAry = array();		
				$toCityAry = $this->check_city_country($this->szDestinationCity,$this->szDestinationCountry) ;
				//print_r($this->multiEmaptyPostcodeFromCityAry);
				if(!$toCityAry)
				{
					//call function
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_TO');
					$this->addError('szDestinationCity3',$error_messg);
					$this->iDestinationError = true ;
				}
				else if($this->emptyPostcodeCtr == 1 && !empty($toCityAry))
				{
					$this->idDestinationPostCode = $this->idEmptyPostCode;
				}
				else if($this->emptyPostcodeCtr > 1 && !empty($toCityAry))
				{
					$this->multiEmaptyPostcodeToCityAry = $toCityAry ;
				}
				/*
				else if($this->emptyPostcodeCtr == 1 && !empty($toCityAry))
				{
					$this->idDestinationPostCode = $this->idEmptyPostCode;
				}
				*/
				else if(empty($this->szDestinationPostCode) && ($this->szDestinationCountry != __ID_COUNTRY_HONG_KONG__))
				{
					$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_TO');
					$this->addError('szDestinationCity4',$error_messg);
					$this->iDestinationError = true ;
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
				}																	
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}	
			// if we have only post codes 
			if(empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				$this->iDestinationError = false ;
				$toCityAry=array();
				$this->szDestinationCity = $this->getAllCityByPostCode($this->szDestinationCountry,$this->szDestinationPostCode);
				$toCityAry = $this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode);
				if(!$toCityAry)
				{
					//call function
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szDestinationCity',$error_messg);
					$this->iDestinationError = true ;
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
				}
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}
		}
		if(($this->idServiceType==(int)__SERVICE_TYPE_WTW__) || ($this->idServiceType==(int)__SERVICE_TYPE_PTP__) || ($this->idServiceType==(int)__SERVICE_TYPE_WTP__) || ($this->idServiceType==(int)__SERVICE_TYPE_PTW__)) //WTW
		{
			// if we have both city and post code 
			if(!empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				if(!$this->isCityExistsByPostCode($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode))
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_FROM')." ".$this->szOriginCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}				
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			
			}
			
			// if we don't have any one 
			if(empty($this->szOriginCity) && empty($this->szOriginPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
				$this->addError('szOriginCity',$error_messg);
				$this->iOriginError = true ;
			}
			
			
			// if we have only from city  
			if(!empty($this->szOriginCity) && empty($this->szOriginPostCode))
			{
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry) ;
				if(!$fromCityAry)
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}			
	
			// if we have only post code the  we first fetch city which belongs to this country and post code then check for multiregion 
			if(empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				$fromCityAry=array();
				$this->szOriginCity = $this->getAllCityByPostCode($this->szOriginCountry,$this->szOriginPostCode);
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode) ;
				if(!$fromCityAry)
				{
					//call function
					$this->addCityNotFound($this->szOriginCountry,$this->szOriginCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
					$this->addError('szOriginCity',$error_messg);
					$this->iOriginError = true ;
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}								
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}		
			
			//if we have both to city and postcode .
			if(!empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				if(!$this->isCityExistsByPostCode($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode))
				{
					//call function
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_TO')." ".$this->szDestinationCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_TO');
					$this->addError('szDestinationCity',$error_messg);
					$this->iDestinationError = true ;
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
					$this->idDestinationPostCode = $this->idPostCode;
				}	
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}	
			// if we don't have any 
			if(empty($this->szDestinationCity) && empty($this->szDestinationPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
				$this->addError('szDestinationCity',$error_messg);
				$this->iDestinationError = true ;
			}
							
			// if we have only city 
		    if(!empty($this->szDestinationCity) && empty($this->szDestinationPostCode) )
			{
				$toCityAry = $this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry) ;
				if(!$toCityAry)
				{
					//call function
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szDestinationCity',$error_messg);
					$this->iDestinationError = true ;
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
				}									
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}				
			
			// if we have only post codes 
			if(empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				$toCityAry=array();
				$this->szDestinationCity = $this->getAllCityByPostCode($this->szDestinationCountry,$this->szDestinationPostCode);
				$toCityAry = $this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode);
				if(!$toCityAry)
				{
					//call function
					$this->addCityNotFound($this->szDestinationCountry,$this->szDestinationCity);
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szDestinationCity',$error_messg);
					$this->iDestinationError = true ;
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
				}				
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}
		}
	}
	
	function getAllCountryInPair()
	{		
		$query="
			SELECT
                            id,
                            szCountryName
			FROM	
				".__DBC_SCHEMATA_COUNTRY__." 
		";		
		//echo "<br>".$query."<br>" ;
		if($result=$this->exeSQL($query))
		{
			$countryAry=array();
			while($row=$this->getAssoc($result))
			{
                            $countryAry[]  =$row;
			}
			return $countryAry ;
		}
		else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}
	}
        
        //This is function is also got called from header_new.php
        function getAllCountryForMaxmindScript($bSmallCountry=false)
	{		
            if($bSmallCountry)
            {
                $query_and = " AND iSmallCountry = '1' ";
            }
            
            $query="
                SELECT
                    id,
                    szCountryName,
                    szCountryISO 
                FROM	
                    ".__DBC_SCHEMATA_COUNTRY__."
                WHERE
                    iActive = 1  
                $query_and
            ";		
            //echo "<br>".$query."<br>" ;
            if($result=$this->exeSQL($query))
            {
                $countryAry=array();
                while($row=$this->getAssoc($result))
                {
                    $countryAry[$row['szCountryISO']]  =$row;
                }
                return $countryAry ;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
	
	function deleteCurrencyConversionData($date)
	{
		$query="
			DELETE FROM
				".__DBC_SCHEMATA_CURRENCY_EXCHANGE_RATE__."
			WHERE
				DATE(dtDate)<='".mysql_escape_custom($date)."'
		";
		$result=$this->exeSQL($query);
	}
	
	function getAllCountriesOfVerifiedUser($flag=false)
	{				
				
		$query="
			SELECT
				c.id,
				c.szCountryName
			FROM	
				".__DBC_SCHEMATA_COUNTRY__." AS c
			INNER JOIN
				".__DBC_SCHEMATA_USERS__." AS u
			ON
				u.idCountry=c.id
			WHERE
				c.iActive = 1
			AND
				u.iConfirmed='1'	
				".$sql."
			GROUP BY
					c.id
			ORDER BY
				c.szCountryName ASC							
		";		
		//echo "<br /> ".$query."<br />";
		if($result=$this->exeSQL($query))
		{
			$countryAry=array();
			$to_ctr=0;
			$from_ctr = 0;
			while($row=$this->getAssoc($result))
			{
				$countryAry[]=$row;
			}
			return $countryAry ;
		}
	    else
		{
			$this->error = true;
			$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
			$this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
			return false;
		}	
	}
	
	function getAllCountryInPair_demo($bFlag=false)
	{
            if($bFlag)
            {
                $query_where = " idRegion = '2' ";
            }
            else
            {
                $query_where = " id IN (100,92,138,25,108) ";
            }
            
            $query="
                SELECT
                    id,
                    szCountryName,
                    szLatitude,
                    szLongitude
                FROM
                    ".__DBC_SCHEMATA_COUNTRY__."
                WHERE 
                    $query_where
            ";
            //echo "<br>".$query."<br>" ;
            if($result=$this->exeSQL($query))
            {
                $countryAry=array();
                $ctr=0;
                while($row=$this->getAssoc($result))
                {
                    $countryAry[$ctr]  =$row;
                    $ctr++;
                }
                return $countryAry ;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
	
	function comparePostcode($szInputPostcode,$szObjectPostcode)
	{
		$szInputPostcode = mb_strtolower($szInputPostcode,'UTF-8');
		$szObjectPostcode = mb_strtolower($szObjectPostcode,'UTF-8');
		
		if($szInputPostcode == $szObjectPostcode)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
	function getCountryIdByCountryName($szCountryName,$iLanguage=false)
	{	
            if(!empty($szCountryName))
            {
                $query_and = " szCountryName = '".mysql_escape_custom(trim($szCountryName))."' ";
                $query_select = " id ";
		 
                $query="
                    SELECT
                        $query_select
                    FROM
                        ".__DBC_SCHEMATA_COUNTRY__."
                    WHERE
                        $query_and		
                ";
                //echo "<br>".$query."<br />";
                if($result=$this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        $row=$this->getAssoc($result); 
                        return $row['id']; 
                    }
                    else
                    {
                        return false;
                    }
                }	
            }	
	}
	
	function getPostcodeFromGoogleCordinate($data)
	{
            if(!empty($data))
            {
                /*
                * When making a search:
                * 1. Search the postcode table for rows with the same country as returned by Google
                * 2. Filter only the rows where the lattitude = Google latitude � X/111 AND longitude = Google longitude � X/111 
                * 3. For those rows, calculate the distance between the Google coordinates and the postcode table coordinates = SQRT((googlelat-postcodetablellat)2 + (googlelon-postcodetablellon)2)
                * 4. Use the postcode with the smallest distance calculated above
                */
                 
                $iCallTable = true;
                if($iCallTable)
                {
                    $kWHSSearch=new cWHSSearch();
                    $iMaxDistanceToGoogle = $kWHSSearch->getManageMentVariableByDescription('__MAX_DISTANCE_FROM_GOOGLE_CORDINATE__');

                    $szGoogleLatPlus10 =  ($data['szLatitute'] + ($iMaxDistanceToGoogle/111));
                    $szGoogleLatMinus10 =  ($data['szLatitute'] - ($iMaxDistanceToGoogle/111));

                    $szGoogleLongPlus10 =  ($data['szLongitute'] + ($iMaxDistanceToGoogle/111)) ;
                    $szGoogleLongMinus10 =  ($data['szLongitute'] - ($iMaxDistanceToGoogle/111)) ;

                    $query="
                        SELECT
                            p.id,
                            p.szRegion1,
                            p.szCity,
                            p.szPostCode,
                            p.szLat,
                            p.szLng,
                            (SQRT(POW((".mysql_escape_custom(trim($data['szLatitute']))." - p.szLat),2) + POW((".mysql_escape_custom(trim($data['szLongitute']))." - p.szLng),2))) as iMinDistance
                        FROM
                            ".__DBC_SCHEMATA_POSTCODE__." p
                        WHERE
                            idCountry = '".(int)$data['idCountry']."'
                        AND
                            p.szLat >= '".mysql_escape_custom(trim($szGoogleLatMinus10))."' 
                        AND 
                            p.szLat <= '".mysql_escape_custom(trim($szGoogleLatPlus10))."'
                        AND
                            p.szLng >= '".mysql_escape_custom(trim($szGoogleLongMinus10))."'  
                         AND 
                           p.szLng <= '".mysql_escape_custom(trim($szGoogleLongPlus10))."'
                        ORDER BY
                            iMinDistance ASC
                        LIMIT
                            0,1
                    ";
                    //echo "<br>".$query."<br>";
                    if($result=$this->exeSQL($query))
                    {
                        if($this->iNumRows>0)
                        {
                            $ret_ary = array();
                            $row=$this->getAssoc($result); 
                            return $row ; 
                        }
                        else
                        {
                            $bCallAPIFlag = true;
                        }
                    }	
                    else
                    {
                        $bCallAPIFlag = true;
                    } 
                    
                    if($bCallAPIFlag)
                    {
                        $postcodeInputAry = array() ; 
                        $postcodeListAry = array();

                        $postcodeInputAry['szLatitute'] = $data['szLatitute'];
                        $postcodeInputAry['szLongitute'] = $data['szLongitute'];
                        $postcodeListAry = getAddressDetailsByCordinate($postcodeInputAry);
                        $iCallTable = false;
                        if(!empty($postcodeListAry))
                        {
                            if(!empty($postcodeListAry['szPostCode']))
                            {
                                $ret_ary = array();
                                $ret_ary['szPostCode'] = $postcodeListAry['szPostCode'];
                                $ret_ary['szCity'] = $postcodeListAry['szCity'];
                                $ret_ary['szLat'] =  $data['szLatitute'] ;
                                $ret_ary['szLng'] =  $data['szLongitute'] ;
                                return $ret_ary;
                            }
                            else
                            {
                                $iCallTable = true;
                            }
                        }
                        else
                        {
                            $iCallTable = true;
                        }
                    } 
                }
            }
	}
        
        function getAllActiveTaskStatus()
	{ 
            $query="
                SELECT
                    id,
                    szShortDesc,
                    szDescription,
                    iPendingTrayPriority
                FROM
                    ".__DBC_SCHEMATA_TASK_STATUS__."  
                WHERE
                    iActive = '1'
            ";
            //echo "<br>".$query."<br>";
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $ret_ary = array();
                    while($row=$this->getAssoc($result))
                    {
                        $ret_ary[$row['szShortDesc']] = $row ;
                    }
                    return $ret_ary ; 
                }
                else
                {
                        return false;
                }
            }	
            else
            {
                    return false;
            } 
	}
	
        function getAllActiveFileStatus($iQuickQuote=false)
	{ 
            if($iQuickQuote>0)
            {
                $query_select = ", szQuickQuoteDescription as szDescription";
            }
            else
            {
                $query_select = ", szDescription";
            } 
            $query="
                SELECT
                    id,
                    szShortDesc,
                    szQuickQuoteDescription
                    $query_select
                FROM
                    ".__DBC_SCHEMATA_FILE_STATUS__."  
                WHERE
                    iActive = '1'
                ORDER BY
                    szShortDesc ASC, szDescription ASC
            ";
            //echo "<br>".$query."<br>";
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $ret_ary = array();
                    while($row=$this->getAssoc($result))
                    {
                        $ret_ary[$row['szShortDesc']] = $row ;
                    }
                    return $ret_ary ; 
                }
                else
                {
                        return false;
                }
            }	
            else
            {
                    return false;
            } 
	}
         
        /**
        * This function return all the region,countries,whole wolrd and rest of world which has standard trucking rate is defined .
        *
        * @access public
        * @return array
        * @author Anil
        */
	function getAllCountriesForPreferences($bInsuranceRate=true)
	{ 
            if($bInsuranceRate)
            {
                $otherCountryAry = array();
                $otherCountryAry[0]['idCountry'] = __WHOLE_WORLD_PREFERENCES_ID__ ;
                $otherCountryAry[0]['szCountryName'] = 'Whole World';

               // $otherCountryAry[1]['idCountry'] = __REST_OF_WORLD_PREFERENCES_ID__ ;
               // $otherCountryAry[1]['szCountryName'] = 'Rest of World';
            }
            
            $allCountriesArr = array();
            $allCountriesArr = $this->getAllCountries(true);
            
            $regionSearchAry = array();
            $regionListAry = array();
            
            $regionSearchAry['iActive'] = 1;
            $kAdmin = new cAdmin();
            $regionListAry = $kAdmin->getAllRegions($regionSearchAry);
            if($bInsuranceRate)
            {
                if(!empty($regionListAry))
                {
                    $regionListAry = array_merge($otherCountryAry,$regionListAry);
                }
                else
                {
                    $regionListAry = $otherCountryAry ;
                }
            }
            if(!empty($regionListAry))
            {
                $allCountriesArr = array_merge($regionListAry,$allCountriesArr);
            } 
            $iCount = count($allCountriesArr); 
            return $allCountriesArr ;
	}
        
        function getAllCountriesForPreferencesKeyValuePair($bInsuranceRate=true)
	{
            $allCountriesArr = array();
            $allCountriesArr = $this->getAllCountriesForPreferences($bInsuranceRate);
            
            $final_ret_ary = array();
            if(!empty($allCountriesArr))
            {
                foreach($allCountriesArr as $allCountriesArrs)
                {
                    $final_ret_ary[$allCountriesArrs['idCountry']] = $allCountriesArrs ;
                }
            }
            return $final_ret_ary ;
        }
         
        function getAllTransportecaInvoices($dateAry)
        {
            if(!empty($dateAry))
            { 
                $query="
                    SELECT
                        id,
                        dtBookingConfirmed,
                        szBookingRef,
                        iInsuranceIncluded
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__."
                    WHERE 
                        dtBookingConfirmed >= '".  mysql_escape_custom($dateAry['dtFromDate'])."'
                    AND
                        dtBookingConfirmed <= '".  mysql_escape_custom($dateAry['dtToDate'])."'
                    AND
                        idBookingStatus IN (3,4)
                    ORDER BY
                        dtBookingConfirmed ASC
                "; 
                //echo $query ;
                //die;
                if($result = $this->exeSQL($query))
                { 
                    $ret_ary = array();
                    $ctr=0;
                    while($row = $this->getAssoc($result))
                    {
                        $ret_ary[$ctr] = $row; 
                        $ctr++;
                    }
                    return $ret_ary;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
        }
        
        
        function getStandardCargoList($idCargoCategory=0,$iLanguage=false,$idCargoType=false,$idSearchMini=0)
	{ 
            
            if((int)$iLanguage==0)
            {
                $iLanguage=__LANGUAGE_ID_ENGLISH__;
            }
            
             $configLangArr=$this->getConfigurationLanguageData('__TABLE_STANDARD_CARGO__',$iLanguage);
                
//            if($iLanguage==__LANGUAGE_ID_SWEDISH__)
//            {
//                $query_select = "szShortNameSwedish as szShortName, szLongNameSwedish as szLongName,";
//            }
//            else if($iLanguage==__LANGUAGE_ID_NORWEGIAN__)
//            {
//                $query_select = "szShortNameNorwegian as szShortName, szLongNameNorwegian as szLongName,";
//            }
//            else if($iLanguage==__LANGUAGE_ID_DANISH__)
//            {
//                $query_select = "szShortNameDanish as szShortName, szLongNameDanish as szLongName,";
//            } 
//            else
//            {
//                $query_select = "szShortName, szLongName,";
//            }
             
            if($idCargoCategory>0)
            {
                $query_and .= " AND idCategory = '".(int)$idCargoCategory."' ";
            } 
            if($idCargoType>0)
            {
                $query_and .= " AND id = '".(int)$idCargoType."' ";
            }
            
            if($idSearchMini>0)
            {
                $query_and .= " AND idSearchMini = '".(int)$idSearchMini."' ";
            }

            $query="
                SELECT
                    id,
                    idSearchMini,
                    szSellerReference,
                    fVolume,
                    fWeight,
                    fLength,
                    fWidth,
                    fHeight,
                    iColli,
                    dtCreatedOn,
                    iActive,
                    iOrder
                FROM
                    ".__DBC_SCHEMATA_STANDARD_CARGO__."
                WHERE 
                    iActive='1'
                    $query_and
                ORDER BY
                    szLongName ASC
                "; 
            //echo $query;
                //die;
                if($result = $this->exeSQL($query))
                { 
                    $ret_ary = array();
                    while($row = $this->getAssoc($result))
                    {
                        if(!empty($configLangArr[$row['id']]))
                        {
                            $ret_ary[] = array_merge($configLangArr[$row['id']],$row);
                        }else
                        {
                            $ret_ary[] = $row;
                        }
                        //$ret_ary[] = $row; 
                    }
                    if(count($ret_ary)>0)
                    {
                        $ret_ary=  sortArray($ret_ary,'iOrder','','szLongName');
                    }
                    return $ret_ary;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
	}
        /**
         * For Standard Cargo Category Listing
         * @param type $iLanguage
         * @param type $id
         * @param type $idSearchMini
         * @return array
         */
        function getStandardCargoCategoryList($iLanguage,$id=false,$idSearchMini=0)
	{
            if($id>0)
            {
                $query_and = " AND id='".(int)$id."'";
            } 
            $query_sub_where='';
            if((int)$idSearchMini)
            {
                $query_sub_where="AND sc.idSearchMini='".(int)$idSearchMini."'";
            }
            
            if((int)$iLanguage==0)
            {
                $iLanguage=__LANGUAGE_ID_ENGLISH__;
            }
            
            $configLangArr=$this->getConfigurationLanguageData('__TABLE_STANDARD_CARGO_CATEGORY__',$iLanguage);
            
            $query="
                    SELECT
                        id,
                        dtCreatedOn,
                        iActive,
                        (SELECT count(id) FROM ".__DBC_SCHEMATA_STANDARD_CARGO__." AS sc WHERE sc.idCategory = scc.id ".$query_sub_where.") AS stCargoTypeCount
                    FROM
                        ".__DBC_SCHEMATA_STANDARD_CARGO_CATEGORY__." AS scc
                    WHERE 
                        iActive='1'
                    $query_and
                        HAVING
                            stCargoTypeCount>0
                    ORDER BY
                        szCategoryName ASC
            ";
            if($result = $this->exeSQL($query))
            { 
                $ret_ary = array();
                while($row = $this->getAssoc($result))
                {
                    if(!empty($configLangArr[$row['id']]))
                    {
                        $ret_ary[] = array_merge($configLangArr[$row['id']],$row);
                    }else
                    {
                        $ret_ary[] = $row;
                    }
                }
                if(count($ret_ary)>0)
                {
                    $ret_ary=  sortArray($ret_ary,'szCategoryName');
                }
                return $ret_ary;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}
        
        function getRemindCmsSectionsList($iLanguage=false,$id=false,$szFromPage=false)
        { 
            $query_and = '';
            $innerJoin="";
            $queryLanguage="";
            if((int)$iLanguage>0)
            {
                $queryLanguage="AND
                        crm.idLanguage='".(int)$iLanguage."'";
            }
            if($id>0)
            {
                $query_and = "AND cr.id='".(int)$id."'";
                
                $column=",crm.subject,
                            crm.section_description";
                $innerJoin="INNER JOIN
                        ".__DBC_SCHEMATA_CMS_REMIND_MAPPING__." AS crm
                    ON
                        crm.idEmailTemplate=cr.id";
            } 
            else if($szFromPage=='PENDING_TRAY_CUSTOMER_LOG')
            {
                $query_and .= " AND iCustomerLog = '1' ";
            }
            else if($szFromPage=='PENDING_TRAY_LOG')
            {
                $query_and .= " AND iFileLog = '1' ";
            }
            else
            { 
                $query_and .= " AND iRemindScreen='1' ";
            }
            
            if((int)$iLanguage==0)
            {
                //$iLanguage=__LANGUAGE_ID_ENGLISH__;
            }
             
            
            $query="
                SELECT
                    cr.id,
                    cr.szFriendlyName,
                    cr.status,
                    cr.iLanguage,
                    cr.iUserType,
                    cr.iSequence,
                    cr.szAttachmentFileName,
                    cr.szAttachmentOriginalFileName,
                    cr.iAddInvoice,
                    cr.iAddBookingNotice,
                    cr.iAddBookingConfirmation,
                    cr.iRemindScreen,
                    cr.iFileLog,
                    cr.iCustomerLog
                    $column
                FROM
                    ".__DBC_SCHEMATA_REMIND_CMS_EMAIL__." AS cr
                $innerJoin
                WHERE
                    cr.isDeleted ='0'                    
                $query_and
                $queryLanguage
                ORDER BY
                    cr.szFriendlyName ASC
            "; 
           //echo $query ;
            //die;
            if($result = $this->exeSQL($query))
            { 
                $ret_ary = array();
                if($this->getRowCnt()>0)
                {
                    while($row = $this->getAssoc($result))
                    {
                        $ret_ary[] = $row; 
                    }
                }
                return $ret_ary;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return array();
            }
        }
        
        function deleteRemindEmailTemplate($id)
        {
            if($id>0)
            {
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_REMIND_CMS_EMAIL__."
                    SET
                        isDeleted ='1',
                        updated=NOW()
                    WHERE
                        id='".(int)$id."'
                    ";
                            
                if($result = $this->exeSQL($query))
                { 
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        
        function updateRemindEmailTemplate($data,$id)
        { 
            $this->set_szFriendlyName(trim($data['szFriendlyName'])); 
            $this->set_szReminderSubject(trim($data['szReminderSubject']));
            $this->set_szReminderEmailBody(trim($data['szReminderEmailBody']));
 
            if($this->error==true)
            {
                return false;
            }
            else
            { 
                $update_fields ="subject='".mysql_escape_custom(utf8_encode($this->szReminderSubject))."',
                                section_description='".mysql_escape_custom(utf8_encode($this->szReminderEmailBody))."',";
               

                $szAttachmentFileName = '';
                if(!empty($data['uploadedFileName']))
                {
                    $szAttachmentFileName = serialize($data['uploadedFileName']);
                }
                $szAttachmentOriginalFileName = '';
                if(!empty($data['originalFileName']))
                {
                    $szAttachmentOriginalFileName = serialize($data['originalFileName']);
                } 
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_REMIND_CMS_EMAIL__."
                    SET
                        szFriendlyName ='".mysql_escape_custom(trim($this->szFriendlyName))."',
                        szAttachmentFileName = '".mysql_escape_custom(trim($szAttachmentFileName))."',
                        szAttachmentOriginalFileName = '".mysql_escape_custom(trim($szAttachmentOriginalFileName))."',
                        iAddInvoice = '".(int)$data['iAddInvoice']."',
                        iAddBookingNotice = '".(int)$data['iAddBookingNotice']."',
                        iAddBookingConfirmation = '".(int)$data['iAddBookingConfirmation']."',
                        iRemindScreen = '".(int)$data['iRemindScreen']."',
                        iFileLog = '".(int)$data['iFileLog']."',
                        iCustomerLog = '".(int)$data['iCustomerLog']."',
                        updated=NOW()
                    WHERE
                        id='".(int)$id."'
                    ";
                //echo $query;
                //die;
                    if($result=$this->exeSQL($query))
                    {
                        $checkInsertFlag=true;
                        $query="
                            SELECT
                                id
                            FROM
                                ".__DBC_SCHEMATA_CMS_REMIND_MAPPING__."
                            WHERE
                                idEmailTemplate='".(int)$id."'
                            AND
                                idLanguage='".(int)$data['iRemindEmailLanguage']."'";
                         if($result = $this->exeSQL($query))
                         {
                             if($this->getRowCnt()>0)
                             {
                                 $checkInsertFlag=false;
                             }
                         }
                         
                         if($checkInsertFlag){
                         $query="
                            INSERT INTO
                                ".__DBC_SCHEMATA_CMS_REMIND_MAPPING__."
                            (
                                idEmailTemplate,
                                subject,
                                section_description,
                                idLanguage,
                                updated
                            )
                            VALUES
                            (           
                                '".mysql_escape_custom(trim($id))."',
                                '".mysql_escape_custom(utf8_encode($this->szReminderSubject))."',
                                '".mysql_escape_custom(utf8_encode($this->szReminderEmailBody))."',
                                '".(int)$data['iRemindEmailLanguage']."',
                                NOW()
                            )
                            ";
                         }else{
                        $query="
                            UPDATE
                                ".__DBC_SCHEMATA_CMS_REMIND_MAPPING__."
                            SET
                                $update_fields
                                    updated=NOW()
                                WHERE
                                    idEmailTemplate='".(int)$id."'
                                AND
                                    idLanguage='".(int)$data['iRemindEmailLanguage']."'
                                ";
                                
                         }
                         //echo $query;
                         $result=$this->exeSQL($query);
                              return true;
                        }
                        else
                        {
                              $this->error = true;
                              $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                              $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                              return false;
                        }
            }
        }
        
        function saveNewRemindEmailTemplate($data)
        {
            $this->set_szFriendlyName(trim($data['szFriendlyName'])); 
            $this->set_szReminderSubject(trim($data['szReminderSubject']));
            $this->set_szReminderEmailBody(trim($data['szReminderEmailBody']));
            

            if($this->error==true)
            {
                  return false;
            }
            else
            {
/*            if($data['iRemindEmailLanguage'] == __LANGUAGE_ID_DANISH__)
             {
                    $insert_fields ="szSubjectDanish,
                                    szSectionDescriptionDanish,";
                    
                    $body = utf8_encode($this->szReminderEmailBody);
                    $subject = utf8_encode($this->szReminderSubject);
                /*}
                else
                {*/
                    $insert_fields ="subject,
                                    section_description,";
                    
                     $body = utf8_encode($this->szReminderEmailBody);
                    $subject = utf8_encode($this->szReminderSubject);
                //}
                if((int)$data['idNewOldTemplate']==0)    
                {
                    $query="
                        INSERT INTO
                            ".__DBC_SCHEMATA_REMIND_CMS_EMAIL__."
                        (
                            szFriendlyName,
                            szAttachmentFileName,
                            szAttachmentOriginalFileName,
                            iAddInvoice,
                            iAddBookingNotice,
                            iAddBookingConfirmation,
                            iRemindScreen,
                            iFileLog,
                            iCustomerLog,
                            status,
                            created,
                            iLanguage,
                            iUserType
                        )
                        VALUES
                        (           
                            '".mysql_escape_custom(trim($this->szFriendlyName))."',
                            '".mysql_escape_custom(trim($szAttachmentFileName))."',
                            '".mysql_escape_custom(trim($szAttachmentOriginalFileName))."',
                            '".(int)$data['iAddInvoice']."',
                            '".(int)$data['iAddBookingNotice']."',
                            '".(int)$data['iAddBookingConfirmation']."',
                            '".(int)$data['iRemindScreen']."',
                            '".(int)$data['iFileLog']."',
                            '".(int)$data['iCustomerLog']."',
                            '1',
                            NOW(),
                            '".(int)$data['iRemindEmailLanguage']."',
                            '1'
                        )
                        ";
                        //echo $query;
                        if($result=$this->exeSQL($query))
                        {
                            $idLastInserted = $this->iLastInsertID;
                        }
                }
                else
                {
                    $idLastInserted = $data['idNewOldTemplate'];
                }
                            
                $query="
                    INSERT INTO
                        ".__DBC_SCHEMATA_CMS_REMIND_MAPPING__."
                    (
                        idEmailTemplate,
                        $insert_fields
                        idLanguage,
                        updated
                    )
                    VALUES
                    (           
                        '".mysql_escape_custom(trim($idLastInserted))."',
                        '".mysql_escape_custom(trim($subject))."',
                        '".mysql_escape_custom(trim($body))."',
                        '".(int)$data['iRemindEmailLanguage']."',
                        NOW()
                    )
                    ";
                $result=$this->exeSQL($query);
               // echo $query;
                return $idLastInserted;
                        
                        
            }
        }
        
        function getReminderTemplateIdByTaskStatus($szShortDesc)
        {
            $query="
                SELECT
                    id,
                    szShortDesc,
                    szDescription,
                    RemindMailTemplate,
                    iActive
                FROM
                    ".__DBC_SCHEMATA_TASK_STATUS__."
                WHERE
                    szShortDesc='".mysql_escape_custom(trim($szShortDesc))."'
            "; 
            if($result = $this->exeSQL($query))
            { 
                $row = $this->getAssoc($result);
                return $row['RemindMailTemplate'];
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
        
        function getDistinctStandardCargoList()
	{ 

            $query="
                SELECT
                    DISTINCT(idSearchMini)
                FROM
                    ".__DBC_SCHEMATA_STANDARD_CARGO__."
                WHERE 
                    iActive='1'
                "; 
                //echo $query ;
                //die;
                if($result = $this->exeSQL($query))
                { 
                    $ret_ary = array();
                    while($row = $this->getAssoc($result))
                    {
                        $ret_ary[] = $row; 
                    }
                    return $ret_ary;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
	}
        
        function getLanguageDetails($szLanaguageCode='',$idLanguage=0,$englishLangFlag=false,$szName='',$szLangCodeFlag=false,$iShowFrontend=true,$idCountry=0,$managementFlag=true,$langVerFlag=false)
        {
            $queryWhere='';
            $managementFlag=false;
            if($iShowFrontend || !$managementFlag)
            {
                $queryWhere= " WHERE iShowFrontend = '1' ";
            } 
            
            
            
             if($englishLangFlag)
            {
                $queryWhere .=" AND id<>'".(int)__LANGUAGE_ID_ENGLISH__."'";
            } 
            $query="
                SELECT
                    id,
                    szLanguageCode,
                    szDimensionUnit,
                    szThousandsSeparator,
                    szDecimalSeparator,
                    szLanguageHtmlCode,
                    iDoNotShow,
                    szComments,
                    szName,
                    szNameLocal,
                    szDomain
                FROM
                    ".__DBC_SCHEMATA_LANGUAGE__."
                $queryWhere                
            ";
            if((int)$idLanguage>0)
            {
               $query .="
                    AND
                        id='".(int)$idLanguage."'
                ";  
            }
            
            if($szName!='')
            {
                if($iShowFrontend  || !$managementFlag)
                {             
                    $query .="
                        AND LOWER(szName)='".mysql_escape_custom(strtolower($szName))."'
                     ";  
                }
                else
                {
                    $query .="
                        WHERE LOWER(szName)='".mysql_escape_custom(strtolower($szName))."'
                     ";
                }
            }
            
            if($szLanaguageCode!='')
            {
                
               $query .="
                  AND LOWER(szLanguageCode)='".mysql_escape_custom(strtolower($szLanaguageCode))."'
                ";  
            }
            
            
            if((int)$idCountry>0)
            {
                
               $query .="
                    AND idCountry='".mysql_escape_custom($idCountry)."'
                ";  
            }
           
            $query .="ORDER BY
                    szName ASC";
            
            //echo $query."<br /><br />";
            if($result = $this->exeSQL($query))
            { 
                $ret_ary = array();
                $ctr = 0;
                while($row = $this->getAssoc($result))
                {
                    if($szLangCodeFlag)
                    {
                        $ret_ary[$ctr] = $row['szLanguageCode'];
                    }
                    else
                    {
                        $ret_ary[$ctr] = $row;
                    }
                    $ctr++;
                }
                return $ret_ary;
            }
            else
            {
                return array();
            }
        }
        
        function getLanguageNameMapped($idLanguage=0)
        { 
            $query="
                SELECT
                    lnm.id,
                    lnm.idLanguage,
                    lnm.szName,
                    lnm.szLanguageCode
                FROM
                    ".__DBC_SCHEMATA_LANGUAGE_NAME_MAPPING__." AS lnm
                INNER JOIN
                    ".__DBC_SCHEMATA_LANGUAGE__." AS l
                ON
                    l.szLanguageCode=lnm.szLanguageCode
                WHERE
                    lnm.idLanguage='".(int)$idLanguage."'
                AND
                    l.iShowFrontend='1'
            ";
            
            //echo $query;
            if($result = $this->exeSQL($query))
            { 
                $ret_ary = array();
                while($row = $this->getAssoc($result))
                {
                    $ret_ary[] = $row; 
                }
                return $ret_ary;
            }
            else
            {
                return array();
            }
        }
        
        
        function getConfigurationLanguageData($szDataType,$idLanguage=__LANGUAGE_ID_ENGLISH__,$flag=false,$doNotWantId=false,$iType=false,$iServiceUpdateFlag=false)
        { 
            if((int)$idLanguage==0)
            {
                $idLanguage=__LANGUAGE_ID_ENGLISH__;
            } 
            
            if(!$iServiceUpdateFlag)
            {
                if($iType)
                {
                    $queryWhere="AND
                            iType='".(int) __SERVICE_TYPE_FOR_AIR_SERVICES__."'
                           ";
                }
                else
                {
                    $queryWhere="AND
                            iType='0'
                           ";
                }
            }
            $query="
                SELECT
                    id,
                    szFieldName,
                    szValue,
                    idMapped,
                    iType
                FROM
                    ".__DBC_SCHEMATA_LANGUAGE_CONFIGURATION__."
                WHERE
                    szTableKey='".  mysql_escape_custom($szDataType)."'
                $queryWhere
            ";
            if(!$flag)
            {
                $query .="
                    AND
                        idLanguage='".(int)$idLanguage."'
                
                    ";
            }
            
            //echo $query;
            if($result = $this->exeSQL($query))
            { 
                $ret_ary = array();
                while($row = $this->getAssoc($result))
                {
                    if($iServiceUpdateFlag)
                    {
                        
                        if($iServiceUpdateFlag && $row['iType']==1 && $row['szFieldName']=='szDescription')
                        {
                            $ret_ary[$row['idMapped']]['szDescription1'] = $row['szValue'];
                        }
                        else
                        {
                            $ret_ary[$row['idMapped']][$row['szFieldName']] = $row['szValue'];
                        }
                        
                        
                        if($row['iType']==1)
                        {
                            $ret_ary[$row['idMapped']][$row['szFieldName']."".$row['iType']] = $row['szValue'];
                        }

                        if(!$doNotWantId){
                        $ret_ary[$row['idMapped']]['id'] = $row['idMapped'];
                        }
                    }
                    else
                    {
                        $ret_ary[$row['idMapped']][$row['szFieldName']] = $row['szValue'];

                        if(!$doNotWantId){
                        $ret_ary[$row['idMapped']]['id'] = $row['idMapped'];
                        }
                    }
                }
                return $ret_ary;
            }
            else
            {
                return array();
            }
        }
        
        function getLanguageConfigValuesByFieldName($szFieldName,$szTableKey)
        {
            if($szFieldName)
            {
                $query="
                    SELECT 
                        szValue 
                    FROM
                        ".__DBC_SCHEMATA_LANGUAGE_CONFIGURATION__."
                    WHERE
                        szTableKey='".  mysql_escape_custom($szTableKey)."'
                    AND
                        szFieldName = '".mysql_escape_custom(trim($szFieldName))."'
                "; 

                //echo $query;
                if($result = $this->exeSQL($query))
                { 
                    $ret_ary = array();
                    $ctr = 0;
                    while($row = $this->getAssoc($result))
                    {
                        $ret_ary[$ctr] = $row['szValue'].":"; 
                        $ctr++;
                    }
                    return $ret_ary;
                }
                else
                {
                    return array();
                }
            } 
        }
        function getLanguageMetaTags($idLanguage)
        {
            $query="
                SELECT
                    szKey,
                    szValue
                FROM
                    ".__DBC_SCHEMATA_LAUGUAGE_META_TAGS__."
                WHERE
                    idLanguage='".(int)$idLanguage."'
            ";
            if($result = $this->exeSQL($query))
            { 
                $ret_ary = array();
                while($row = $this->getAssoc($result))
                {
                    $ret_ary[]=$row;
                }
            }
            
            return $ret_ary;
        }
        
        function updateLanguageConfigurationData($data,$idLaguage,$idLangConfigType)
        {
            
            
            if(!empty($data))
            {
                $totalCount=count($data);
                for($i=0;$i<count($data);++$i)
                {
                   if($idLangConfigType=='__TABLE_TRANSPORT_MODE__' || $idLangConfigType=='__TABLE_PALLET_TYPE__'){
                        $this->set_szShortName(sanitize_all_html_input(trim($data[$i]['szShortName'])), $i);
                        $this->set_szLongName(sanitize_all_html_input(trim($data[$i]['szLongName'])), $i);
                        if($idLangConfigType=='__TABLE_TRANSPORT_MODE__'){
                            if($data[$i]['id']!=__ALL_WORLD_TRANSPORT_MODE_ID__ && $data[$i]['id']!=__REST_TRANSPORT_MODE_ID__)
                            {
                                $this->set_szFrequency(sanitize_all_html_input(trim($data[$i]['szFrequency'])), $i);
                            }
                            else
                            {
                                $this->szFrequency[$i]='';
                            }
                        }
                    }
                    else if($idLangConfigType=='__TABLE_WEIGHT_MEASURE__' || $idLangConfigType=='__TABLE_CARGO_MEASURE__'  || $idLangConfigType=='__TABLE_TIMING_TYPE__' || $idLangConfigType=='__TABLE_ORIGIN_DESTINATION__' || $idLangConfigType=='__TABLE_SHIP_CON_GROUP__')
                    {
                        $this->set_szDescription(sanitize_all_html_input(trim($data[$i]['szDescription'])), $i);
                    }
                    else if($idLangConfigType=='__TABLE_COURIER_PROVIDER_PRODUCT_PACKING__')
                    {
                        $this->set_szPacking(sanitize_all_html_input(trim($data[$i]['szPacking'])), $i);
                        $this->set_szPackingSingle(sanitize_all_html_input(trim($data[$i]['szPackingSingle'])), $i);
                    }
                    else if($idLangConfigType=='__TABLE_STANDARD_CARGO_CATEGORY__')
                    {
                        $this->set_szCategoryName(sanitize_all_html_input(trim($data[$i]['szCategoryName'])), $i);
                    }
                    else if($idLangConfigType=='__TABLE_SERVICE_TYPE__')
                    {
                        $this->set_szDescription(sanitize_all_html_input(trim($data[$i]['szDescription'])), $i);
                        $this->set_szDescription1(sanitize_all_html_input(trim($data[$i]['szDescription1'])), $i);
                        $this->set_szShortName(sanitize_all_html_input(trim($data[$i]['szShortName'])), $i);
                    }
                    else if($idLangConfigType=='__TABLE_MONTH_NAME__')
                    {
                        $this->set_szMonthName(sanitize_all_html_input(trim($data[$i][$i])), $i);
                    }
                    else if($idLangConfigType=='__SITE_STANDARD_TEXT__')
                    {
                        $this->set_szFieldName(sanitize_all_html_input(trim($data[$i]['szFieldName'])), $i);
                        $this->set_szValue(sanitize_all_html_input(trim($data[$i]['szValue'])), $i);
                    }
                    
                    $this->id[$i]=$data[$i]['id'];
                }
            }
            if($this->error==true)
            {
                return false;
            }  
            //echo $idLangConfigType."idLangConfigType";
            if($totalCount>0)
            {
                for($k=0;$k<$totalCount;++$k)
                {
                    if($idLangConfigType=='__TABLE_TRANSPORT_MODE__'  || $idLangConfigType=='__TABLE_PALLET_TYPE__')
                    {
                        $idValue=$this->id[$k];
                        $dataNew[$idValue]['szShortName']=$this->szShortName[$k];
                        $dataNew[$idValue]['szLongName']=$this->szLongName[$k];
                        if($idLangConfigType=='__TABLE_TRANSPORT_MODE__'){
                            $dataNew[$idValue]['szFrequency']=$this->szFrequency[$k];
                        }
                    }
                    else if($idLangConfigType=='__TABLE_WEIGHT_MEASURE__' || $idLangConfigType=='__TABLE_CARGO_MEASURE__' || $idLangConfigType=='__TABLE_TIMING_TYPE__' || $idLangConfigType=='__TABLE_ORIGIN_DESTINATION__' || $idLangConfigType=='__TABLE_SHIP_CON_GROUP__')
                    {
                         $idValue=$this->id[$k];
                        $dataNew[$idValue]['szDescription']=$this->szDescription[$k];
                    }
                    else if($idLangConfigType=='__TABLE_COURIER_PROVIDER_PRODUCT_PACKING__')
                    {
                        $idValue=$this->id[$k];
                        $dataNew[$idValue]['szPacking']=$this->szPacking[$k];
                        $dataNew[$idValue]['szPackingSingle']=$this->szPackingSingle[$k];                        
                    }
                    else if($idLangConfigType=='__TABLE_STANDARD_CARGO_CATEGORY__')
                    {
                        $idValue=$this->id[$k];
                        $dataNew[$idValue]['szCategoryName']=$this->szCategoryName[$k];                  
                    }
                    else if($idLangConfigType=='__TABLE_SERVICE_TYPE__')
                    {
                        $idValue=$this->id[$k];
                        $dataNew[$idValue]['szShortName']=$this->szShortName[$k];
                        $dataNew[$idValue]['szDescription']=$this->szDescription[$k];  
                        $dataNew[$idValue]['szDescription1']=$this->szDescription1[$k];  
                    }
                    else if($idLangConfigType=='__TABLE_MONTH_NAME__')
                    {
                        $idValue=$this->id[$k];
                        $dataNew[$idValue][$idValue]=$this->szMonthName[$k];                  
                    }
                    else if($idLangConfigType=='__SITE_STANDARD_TEXT__')
                    {
                        $idValue=$this->id[$k];
                        $dataNew[$idValue][$this->szFieldName[$k]]=$this->szValue[$k];                  
                    }
                }
                
                if(!empty($dataNew))
                {
                    foreach($dataNew as $key=>$value)
                    {
                        $idValue=$key;
                       
                        foreach($value as $keys=>$values)
                        {
                            
                            if($keys=='szDescription1' && $idLangConfigType=='__TABLE_SERVICE_TYPE__')
                            {
                                $query="
                                UPDATE
                                    ".__DBC_SCHEMATA_LANGUAGE_CONFIGURATION__."
                                SET
                                    szValue='".mysql_escape_custom($values)."'
                                WHERE
                                    szFieldName='szDescription'
                                AND
                                    szTableKey='".  mysql_escape_custom($idLangConfigType)."'
                                AND
                                    idMapped='".(int)$idValue."'
                                AND
                                    idLanguage='".(int)$idLaguage."'
                                AND
                                    iType='1'        
                                ";
                                //echo $query."<br /><br />";
                                $result = $this->exeSQL($query);
                            }
                            else
                            {
                                $query="
                                    UPDATE
                                        ".__DBC_SCHEMATA_LANGUAGE_CONFIGURATION__."
                                    SET
                                        szValue='".mysql_escape_custom($values)."'
                                    WHERE
                                        szFieldName='".mysql_escape_custom($keys)."'
                                    AND
                                        szTableKey='".  mysql_escape_custom($idLangConfigType)."'
                                    AND
                                        idMapped='".(int)$idValue."'
                                    AND
                                        idLanguage='".(int)$idLaguage."'
                                    ";
                                //echo $query."<br /><br />";
                                $result = $this->exeSQL($query);
                            }
                            
                            
                            
                        }
                    }
                }
            }
            
        }
        
        function searchUserListData($data)
        {
            $kDashboardAdmin= new cDashboardAdmin();
            $transportecaUserAry=$kDashboardAdmin->getAllTransportecaUsers();
 
            if($data['dtSearch']>0)
            { 
                //$dtSearch=$dtSearchArr[2]."-".$dtSearchArr[1]."-".$dtSearchArr[0];
                $dtSearch = date("Y-m-d", strtotime("-".(int)$data['dtSearch']." DAYS"));
            }
            if(!empty($transportecaUserAry))
            {
                $user_str = implode(",",$transportecaUserAry);
                $query_and = " AND idUser NOT IN (".$user_str.") "; 
            }
            
            
            $query="
            SELECT	
                cb.idUser,
                u.szEmail,
                u.szFirstName,
                u.szLastName,
                cb.dtDateTime,
                cb.szOriginCountry,
                cb.szDestinationCountry
            FROM
                ".__DBC_SCHEMATA_COMPARE_BOOKING__." AS cb
            INNER JOIN
                ".__DBC_SCHEMATA_USERS__." AS u
            ON
                u.id=cb.idUser
            WHERE
                date_format(dtDateTime, '%Y-%m-%d' ) >= '".mysql_escape_custom($dtSearch)."' 
            AND 
                cb.idOriginCountry='".(int)$data['idFromCountry']."'
            AND
                cb.idDestinationCountry='".(int)$data['idToCountry']."' 
            $query_and 
            GROUP BY
                cb.idUser,cb.idOriginCountry
            ORDER BY
                cb.szOriginCountry
            ";
            /*
             *  )
            OR
            (        
                    cb.idOriginCountry='".(int)$data['idToCountry']."'
                AND
                    cb.idDestinationCountry='".(int)$data['idFromCountry']."'
            )
             */
//            echo "<br><br> ".$query."<br>";
//            die();
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                $dashBoardArr = array();
                while($row=$this->getAssoc($result))
                {
                    $resArr[]=$row;
                }

                
            }
            
            
            require_once( __APP_PATH_CLASSES__ . "/PHPExcel.php" );

            $objPHPExcel=new PHPExcel(); 

            $sheetIndex=0;
            $objPHPExcel = new PHPExcel();

            $objPHPExcel->createSheet();
            $objPHPExcel->setActiveSheetIndex($sheetIndex);
            $sheet = $objPHPExcel->getActiveSheet(); 

            $styleArray = array(
                    'font' => array(
                            'bold' => false,
                            'size' =>12,
                    ),
                    'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    ),
                    'borders' => array(
                            'top' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'bottom' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'right' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'left' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                    ),
            );

            $styleArray1 = array(
                    'font' => array(
                            'bold' => false,
                            'size' =>12,
                    ),
                    'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    ),
                    'borders' => array(
                            'top' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'bottom' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'right' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'left' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                    ),
            ); 

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);

            $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFill()->getStartColor()->setARGB('FFddd9c3');

            $objPHPExcel->getActiveSheet()->setCellValue('A1','From Country')->getStyle('A1');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray); 

            $objPHPExcel->getActiveSheet()->setCellValue('B1','To Country')->getStyle('B1');
            $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('C1','Email')->getStyle('C1');
            $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('D1','Name')->getStyle('D1');
            $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray); 

            $col = 2;
            /* 
            * Fetching data for last 2 months 
            */ 

            if(!empty($resArr))
            {
                foreach($resArr as $resArrs)
                {     
                    $szName=$resArrs['szFirstName']." ".$resArrs['szLastName'];
                    $objPHPExcel->getActiveSheet()->setCellValue("A".$col,$resArrs['szOriginCountry']);
                    $objPHPExcel->getActiveSheet()->setCellValue("B".$col,$resArrs['szDestinationCountry']);
                    $objPHPExcel->getActiveSheet()->setCellValue("C".$col,$resArrs['szEmail']);
                    $objPHPExcel->getActiveSheet()->setCellValue("D".$col,$szName);  
                    $col++;
                    
                }
            }         

            $title = "_SEARCH_USER_LIST";
            $objPHPExcel->getActiveSheet()->setTitle('Search User List');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->removeSheetByIndex(1);
            $szFileName = date('Ymdhs').$title.".xlsx";
            $fileName=__APP_PATH_ROOT__."/insuranceSheets/".$szFileName; 
            $objWriter->save($fileName); 
            return $fileName; 

        }
        
        function getLangugeDetailsById($idLanguage)
        {
            $query="
                SELECT
                    id,
                    szLanguageCode,
                    szName,
                    szDimensionUnit,
                    szThousandsSeparator,
                    szDecimalSeparator,
                    szLanguageHtmlCode,
                    iDoNotShow,
                    szComments,
                    szNameLocal,
                    szDomain
                FROM
                    ".__DBC_SCHEMATA_LANGUAGE__."
                WHERE
                    id='".(int)$idLanguage."'
            ";
            if(($result = $this->exeSQL( $query ) ) )
            {
                if( $this->getRowCnt() > 0 )
                {
                    $row=$this->getAssoc($result);
                    return $row;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
        
        function updateLanguageVersionsData($data,$idLanguage)
        {
            
            $this->set_szNameLocal(sanitize_all_html_input(trim($data['szName'])));
            $this->set_szDomain(sanitize_all_html_input(trim($data['szDomain'])));
            if($this->error==true)
            {
                return false;
            } 
            if (filter_var($this->szDomain, FILTER_VALIDATE_URL) === false) 
            {
                $this->addError("szDomain","Invalid domain url");
                return false;
            }
            $this->szDomain = rtrim($this->szDomain, "/");
            
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_LANGUAGE__."
                SET
                    iDoNotShow='".(int)$data['iDoNotShow']."',
                    szComments='".mysql_escape_custom(trim($data['szComments']))."',
                    szNameLocal='".mysql_escape_custom(trim($this->szNameLocal))."',
                    szDomain = '".mysql_escape_custom(trim($this->szDomain))."'  
                WHERE
                    id='".(int)$idLanguage."'
            ";
//            echo $query;
//            die;
            if($result=$this->exeSQL($query))
            {
                return true;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
        
        function updateCountryNameForLanguage($szName,$idCounty,$idLanguage)
        {
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_COUNTRY_NAME_MAPPING__."
                SET
                    szName='".  mysql_escape_custom($szName)."'
                       
                WHERE
                    idLanguage='".(int)$idLanguage."'
                AND        
                    idCountry='".(int)$idCounty."'
                ";
            $result=$this->exeSQL($query);
        }
        
        
        function checkRemindCMSTemplateExists($szFriendlyName,$iLanguage=false)
        { 
            $query_and = '';
            $innerJoin="";
            $queryLanguage="";
            if((int)$iLanguage>0)
            {
                $queryLanguage="AND
                        crm.idLanguage='".(int)$iLanguage."'";
            }            
            
            $query="
                SELECT
                    cr.id,
                    cr.szFriendlyName
                FROM
                    ".__DBC_SCHEMATA_REMIND_CMS_EMAIL__." AS cr
                INNER JOIN
                        ".__DBC_SCHEMATA_CMS_REMIND_MAPPING__." AS crm
                    ON
                        crm.idEmailTemplate=cr.id
                WHERE
                    cr.isDeleted ='0'                    
                AND
                    LOWER(cr.szFriendlyName)='".mysql_escape_custom(strtolower($szFriendlyName))."'
                $queryLanguage
                ORDER BY
                    cr.szFriendlyName ASC
            "; 
           //echo $query ;
            //die;
            if($result = $this->exeSQL($query))
            { 
                $ret_ary = array();
                if($this->getRowCnt()>0)
                {
                    $row = $this->getAssoc($result);                    
                    return $row['id'];                     
                }
                return false;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return array();
            }
        }
        
        
        function getTransportIDByName($szName='')
        {
            $queryWHERE='';
            if(trim($szName)!='')
            {
                $queryWHERE="WHERE LOWER(szShortName)='".mysql_escape_custom(strtolower($szName))."'"; 
            }
            $query="
                SELECT
                    id
                FROM
                    ".__DBC_SCHEMATA_TRANSPORT_MODE__."
                $queryWHERE
            ";
            //echo $query;
            if(($result = $this->exeSQL($query)))
            { 
                $resArr = array();
                $ctr = 0;
                while($row=$this->getAssoc($result))
                {
                    if(trim($szName)!='')
                    {
                        return $row['id'];
                    }                   
                }  
            }
        }
        
        
        function validateAutomaticeRFQPage($data,$cargodetailAry=array(),$iSearchMiniPageVersion=false,$bTryitout=false)
        {   
            if(!empty($data))
            {   
                    //Validating data for Simple Search Page  
                $this->set_szOriginCountry(sanitize_all_html_input(trim($data['szOriginCountry']))); // id country origin 
                if((int)$data['szOriginCountry']>0)
                {
                    $this->loadCountry($data['szOriginCountry']);  
                    if($this->iSmallCountry==1 && empty($data['szOriginCity']))
                    {
                        $data['szOriginCity'] = $this->szCountryName ;
                    }
                    if($data['szOriginCity']=='')
                    {
                        if($data['szSearchMode']=='FORWARDER_COURIER_TRY_IT_OUT')
                        {
                            $this->addError("szOriginPostCode",t($this->t_base.'errors/city_name_is_required'));
                        }
                        else
                        {
                            $this->addError("szOriginCountry",t($this->t_base.'errors/city_name_is_required'));
                        } 
                    }
                } 
                $this->set_szDestinationCountry(sanitize_all_html_input(trim($data['szDestinationCountry']))); 
                if((int)$data['szDestinationCountry']>0)
                {
                    $this->loadCountry($data['szDestinationCountry']);
                    if($this->iSmallCountry==1 && empty($data['szDestinationCity']))
                    {
                        $data['szDestinationCity'] = $this->szCountryName ;
                    }
                    if($data['szDestinationCity']=='')
                    {
                        if($data['szSearchMode']=='FORWARDER_COURIER_TRY_IT_OUT')
                        {
                            $this->addError("szDestinationPostCode",t($this->t_base.'errors/city_name_is_required'));
                        }
                        else
                        {
                            $this->addError("szDestinationCountry",t($this->t_base.'errors/city_name_is_required'));
                        }
                    }
                } 
                $this->set_szOriginCity(sanitize_all_html_input(trim($data['szOriginCity'])));  // city name 
                $this->set_szOriginPostCode(sanitize_all_html_input(trim($data['szOriginPostCode'])));

                $this->set_szDestinationCity(sanitize_all_html_input(trim($data['szDestinationCity'])));
                $this->set_szDestinationPostCode(sanitize_all_html_input(trim($data['szDestinationPostCode'])));
                   
                
                $cargoCategoryAry = $cargodetailAry['idCargoCategory']; 
                if(!empty($cargoCategoryAry))
                {
                    $ctr=1;
                    foreach($cargoCategoryAry as $key=>$cargoCategoryArys)
                    {   
                        $this->set_idCargoCategory(sanitize_all_html_input($cargodetailAry['idCargoCategory'][$key]),$ctr,$key,$iSearchMiniPageVersion,$bTryitout);
                        $this->set_idStandardCargoType(sanitize_all_html_input($cargodetailAry['idStandardCargoType'][$key]),$ctr,$key,$iSearchMiniPageVersion,$bTryitout); 
                        $this->set_iVogueQuantity(sanitize_all_html_input($cargodetailAry['iQuantity'][$key]),$ctr,$key,$iSearchMiniPageVersion,$bTryitout); 
                        $ctr++;
                    }
                }  
                
                if($this->error==true)
                {
                    return false;
                }  
                $this->szPageSearchType='SIMPLEPAGE';
                $this->iSearchMiniVersion = $iSearchMiniPageVersion;
                $idDefaultLandingPage = $data['idDefaultLandingPage'];
                  
                $kExplain = new cExplain(); 
               
                
                $this->szOriginCountryName = $data['szOriginCountryStr'];
                $this->fOriginLatitude = $data['szOLat'];
                $this->fOriginLongitude = $data['szOLng'];

                $this->szDestinationCountryName = $data['szDestinationCountryStr'];
                $this->fDestinationLatitude = $data['szDLat'];
                $this->fDestinationLongitude = $data['szDLng'];  

                $iShipperConsignee = 3;
                if($this->idDialCode==$this->szOriginCountry)
                {
                    $iShipperConsignee = 1;
                }
                else if($this->idDialCode==$this->szDestinationCountry)
                {
                    $iShipperConsignee = 2;
                } 
                $this->iShipperConsignee  =$iShipperConsignee;
                if($iShipperConsignee==1)
                {
                    $this->szShipperCompanyName = $this->szFirstName." ".$this->szLastName;  
                    $this->szShipperFirstName = $this->szFirstName;
                    $this->szShipperLastName = $this->szLastName;
                    $this->szShipperEmail = $this->szEmail;
                    $this->szShipperPhone = $this->szPhone;
                    $this->idShipperDialCode = $this->idDialCode;
                    $this->idShipperCountry = $this->idDialCode;

                    $this->szShipperAddress = $this->szAddress;
                    $this->szShipperPostcode = $this->szOriginPostCode;
                    $this->szShipperCity = $this->szOriginCity; 
                }
                else if($iShipperConsignee==2)
                {
                    $this->szConsigneeCompanyName = $this->szFirstName." ".$this->szLastName;  
                    $this->szConsigneeFirstName = $this->szFirstName;
                    $this->szConsigneeLastName = $this->szLastName;
                    $this->szConsigneeEmail = $this->szEmail;
                    $this->szConsigneePhone = $this->szPhone;
                    $this->idConsigneeDialCode = $this->idDialCode;
                    $this->idConsigneeCountry = $this->idDialCode;

                    $this->szConsigneeAddress = $this->szAddress;
                    $this->szConsigneePostcode = $this->szDestinationPostCode;
                    $this->szConsigneeCity = $this->szDestinationCity; 
                } 
                
                 

                $this->iDeliveryAddress = (int)$data['iDeliveryAddress'];  
                $kCourierServices = new cCourierServices();
                if(!$kCourierServices->checkFromCountryToCountryExistsForCC($data['szOriginCountry'],$data['szDestinationCountry']))
                {
                    $this->iOrigingCC = 1;
                    $this->iDestinationCC = (int)$data['idCC'][1];
                    
                }
                    
                $szBookingRandomNum = trim($data['szBookingRandomNum']); 
                if(empty($szBookingRandomNum))
                {
                    $kBooking_new = new cBooking();
                    $szBookingRandomNum = $kBooking_new->getUniqueBookingKey(10);
                    $szBookingRandomNum = $kBooking_new->isBookingRandomNumExist($szBookingRandomNum);
                }
                $this->szBookingRandomNum = $szBookingRandomNum ;
                $this->iDoNotKnowCargo = (int)$data['iDoNotKnowCargo'];
                $this->idTimingType = (int)$data['idTimingType'];
                $this->iBookingQuote = (int)$data['iBookingQuote'];
                $this->szPageSearchType=='SIMPLEPAGE';
                 
                if(!empty($this->idCargoCategory))
                {
                    $ctr = 1;
                    $fTotalVolume = 0;
                    $fTotalWeight = 0;
                    $szInternalComments = '';
                    $szCargoDescription = '';
                    $iLanguage = getLanguageId();
                    $bCargoavailableForAllLine = false;
                    $iUpdateWeightToZeroFlag=false;
                    $iUpdateVolumeToZeroFlag=false;
                    $iShowCourierPriceFlag=0;
                    foreach($this->idCargoCategory as $key=>$idCargoCategory)
                    {
                        $idStandardCargoType = $this->idStandardCargoType[$key];
                        $iQunatity = $this->iQuantity[$key];
                        if($idStandardCargoType>0 && $idCargoCategory>0)
                        { 
                            $standardCargoAry = array();
                            $standardCargoAry = $this->getStandardCargoList($idCargoCategory,$iLanguage,$idStandardCargoType); 
                            
                            if(!empty($standardCargoAry))
                            {
                                foreach($standardCargoAry as $standardCargoArys)
                                { 
                                    
                                    if($standardCargoArys['fVolume']>0 && $standardCargoArys['fWeight']>0 && $standardCargoArys['fHeight']>0 && $standardCargoArys['fWidth']>0 && $standardCargoArys['fLength']>0  && $iEmptyRowFound!=1)
                                    { 
                                        $bCargoavailableForAllLine = true;
                                    }
                                    else
                                    { 
                                        $bCargoavailableForAllLine = false;
                                        $iEmptyRowFound = 1;
                                    }                                    
                                    
                                    $this->iLength[$ctr] = $standardCargoArys['fLength'];
                                    $this->iWidth[$ctr] = $standardCargoArys['fWidth'];
                                    $this->iHeight[$ctr] = $standardCargoArys['fHeight'];
                                    $this->iWeight[$ctr] = $standardCargoArys['fWeight'];  
                                    $this->szCommodity[$ctr] = $standardCargoArys['szLongName'];   
                                    $this->szSellersReference[$ctr] = $standardCargoArys['szSellerReference'];
                                    $this->iColli[$ctr] = $standardCargoArys['iColli'] * $iQunatity; 
                                    $this->fTotalVolumePerCargo[$ctr] = $standardCargoArys['fVolume']* $iQunatity; 
                                    $this->iVolumePerColli[$ctr] = $standardCargoArys['fVolume']; 
                                    
                                   // echo $iPageSearchType."iPageSearchType";
                                    //if($iPageSearchType=='5')
                                    //{
                                        //echo $standardCargoArys['fWeight']."fWeight";
                                        //echo $standardCargoArys['fVolume']."fVolume";
                                        if($standardCargoArys['fWeight']==0 )
                                        {
                                            $iUpdateWeightToZeroFlag=true;
                                        }
                                        if( $standardCargoArys['fVolume']==0)
                                        {
                                            $iUpdateVolumeToZeroFlag=true;
                                        }
                                    //}
                                    
                                    $this->idCargoMeasure[$ctr] = 1; //cm
                                    $this->idWeightMeasure[$ctr] = 1; //kg
                                    $this->iQuantity[$ctr] = $iQunatity;
                                    $this->idStandardCargoType[$ctr] = $this->idStandardCargoType[$key];
                                    $this->idCargoCategory[$ctr] = $this->idCargoCategory[$key];
                                    
                                    $lenWidthHeightStr='';
                                    if((float)$this->iLength[$ctr]==0.00 && (float)$this->iWidth[$ctr]==0.00 && (float)$this->iHeight[$ctr]==0.00)
                                    {
                                        $iShowCourierPriceFlag=1;
                                    }
                                    if((float)$this->iLength[$ctr]>0.00 || (float)$this->iWidth[$ctr]>0.00 || (float)$this->iHeight[$ctr]>0.00)
                                    {
                                       $lenWidthHeightStr = format_numeric_vals($this->iLength[$ctr]).'x'.format_numeric_vals($this->iWidth[$ctr]).'x'.format_numeric_vals($this->iHeight[$ctr]).'cm, ';
                                    }   
                                    $weightStr='';
                                   // if((int)$this->iWeight[$ctr]>0)
                                    //{
                                       $weightStr = format_numeric_vals($this->iWeight[$ctr]).'kg ';
                                    //}
                                    
                                    $szCDQuantityText='';   
                                    $szICQuantityText='';   
                                    if((int)$this->iQuantity[$key]>0)
                                    {
                                        $szCDQuantityText =$this->iQuantity[$key].' stk ';
                                        $szICQuantityText = $this->iQuantity[$key].' pcs: ';
                                    }
                                    $fTotalVolume += $standardCargoArys['fVolume'] * $iQunatity;
                                    $fTotalWeight += $standardCargoArys['fWeight'] * $iQunatity;
                                    $szInternalComments .= $szICQuantityText.$this->szCommodity[$ctr].', '.$lenWidthHeightStr.''.$weightStr.''.PHP_EOL;
                                    $szCargoDescription .= $szCDQuantityText.$this->szCommodity[$ctr].', ';
                                    
                                    $szVogaLogs = "Length: ".$this->iLength[$ctr]."".PHP_EOL;
                                    $szVogaLogs .= "Width: ".$this->iWidth[$ctr]."".PHP_EOL;
                                    $szVogaLogs .= "Height: ".$this->iHeight[$ctr]."".PHP_EOL;
                                    $szVogaLogs .= "Weight: ".$this->iWeight[$ctr]."".PHP_EOL;
                                    $szVogaLogs .= "Quantity: ".$this->iQuantity[$ctr]."".PHP_EOL; 
                                    $szVogaLogs .= "Total Volume: ".$fTotalVolume."".PHP_EOL;
                                    $szVogaLogs .= "Total Weight: ".$fTotalWeight."".PHP_EOL; 
                                }
                            }
                            $ctr++;
                        }  
                    }
                    if($bCargoavailableForAllLine)
                    { 
                        $kExplain = new cExplain();
                        $standardQuotePricingAry = array();
                        $standardQuotePricingAry = $kExplain->getStandardQuotePriceByPageID($idDefaultLandingPage,false,false,false,true);  

                        $standardHandlerAry = array();
                        $standardHandlerAry = $kExplain->getQuoteHandlerByPageID($idDefaultLandingPage,true,true); 
                        if(!empty($standardHandlerAry) && !empty($standardQuotePricingAry))
                        {
                            $this->iVogaSearchAutomatic = true;
                        }
                        else
                        {
                            $this->iVogaSearchAutomatic = false;
                        } 
                    } 
                    if($iUpdateWeightToZeroFlag)
                    {
                        $fTotalWeight=0;
                        $this->iUpdateWeightToZeroFlag = $iUpdateWeightToZeroFlag;
                    }
                    if($iUpdateVolumeToZeroFlag)
                    {
                        $fTotalVolume=0;
                        $this->iUpdateVolumeToZeroFlag = $iUpdateVolumeToZeroFlag;
                    }
                    //echo $this->iUpdateVolumeWeightToZeroFlag."iUpdateVolumeWeightToZeroFlag";
                    
                    $this->fTotalWeight = $fTotalWeight;
                    $this->fTotalVolume = $fTotalVolume;
                    $this->szInternalComments = $szInternalComments;
                    $szCargoDescription = trim($szCargoDescription);
                    $this->szCargoDescription = rtrim($szCargoDescription,',');  
                    $this->iShowCourierPriceFlag=$iShowCourierPriceFlag;
                } 
                return true;
            }
        }
        
        function set_szNameLocal( $value)
	{
            $this->szNameLocal= $this->validateInput($value, __VLD_CASE_ANYTHING__, "szName", "Language Name", false, false, true );
	}
        function set_szDomain( $value)
	{
            $this->szDomain= $this->validateInput($value, __VLD_CASE_ANYTHING__, "szDomain", "Domain", false, false, true );
	}
        
        function set_szValue( $value ,$counter)
	{
            //echo $value."dd";
            $error_key="szValue_".$counter;
            $this->szValue[$counter] = $this->validateInput($value, __VLD_CASE_ANYTHING__, $error_key, t($this->t_base_error_mang.'fields/shortName'), false, false, true );
	}
        
        function set_szFieldName( $value ,$counter)
	{
            //echo $value."dd";
            $error_key="szFieldName_".$counter;
            $this->szFieldName[$counter] = $this->validateInput($value, __VLD_CASE_ANYTHING__, $error_key, t($this->t_base_error_mang.'fields/shortName'), false, false, true );
	}
        
        function set_szMonthName( $value ,$counter)
	{
            //echo $value."dd";
            $error_key=$counter."_".$counter;
            $this->szMonthName[$counter] = $this->validateInput($value, __VLD_CASE_ANYTHING__, $error_key, t($this->t_base_error_mang.'fields/shortName'), false, false, true );
	}
        
        function set_szCategoryName( $value ,$counter)
	{
            //echo $value."dd";
            $error_key="szCategoryName_".$counter;
            $this->szCategoryName[$counter] = $this->validateInput($value, __VLD_CASE_ANYTHING__, $error_key, t($this->t_base_error_mang.'fields/shortName'), false, false, true );
	}
        
        function set_szPackingSingle( $value ,$counter)
	{
            //echo $value."dd";
            $error_key="szPackingSingle_".$counter;
            $this->szPackingSingle[$counter] = $this->validateInput($value, __VLD_CASE_ANYTHING__, $error_key, t($this->t_base_error_mang.'fields/shortName'), false, false, true );
	}
        
        function set_szPacking( $value ,$counter)
	{
            //echo $value."dd";
            $error_key="szPacking_".$counter;
            $this->szPacking[$counter] = $this->validateInput($value, __VLD_CASE_ANYTHING__, $error_key, t($this->t_base_error_mang.'fields/shortName'), false, false, true );
	}
        
        function set_szDescription( $value ,$counter)
	{
            //echo $value."dd";
            $error_key="szDescription_".$counter;
            $this->szDescription[$counter] = $this->validateInput($value, __VLD_CASE_ANYTHING__, $error_key, t($this->t_base_error_mang.'fields/shortName'), false, false, true );
	}
        
        function set_szDescription1( $value ,$counter)
	{
            //echo $value."dd";
            $error_key="szDescription1_".$counter;
            $this->szDescription1[$counter] = $this->validateInput($value, __VLD_CASE_ANYTHING__, $error_key, t($this->t_base_error_mang.'fields/shortName'), false, false, true );
	}
        
        function set_szShortName( $value ,$counter)
	{
            //echo $value."dd";
            $error_key="szShortName_".$counter;
            $this->szShortName[$counter] = $this->validateInput($value, __VLD_CASE_ANYTHING__, $error_key, t($this->t_base_error_mang.'fields/shortName'), false, false, true );
	}
        
        function set_szLongName( $value ,$counter)
	{
            $error_key="szLongName_".$counter;
            $this->szLongName[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, $error_key, t($this->t_base_error_mang.'fields/longName'), false, false, true );
	}
        
        function set_szFrequency( $value ,$counter)
	{
            $error_key="szFrequency_".$counter;
            $this->szFrequency[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, $error_key, t($this->t_base_error_mang.'fields/frequency'), false, false, true );
	}
        
        
        function set_szPassword( $value )
	{
		$this->szPassword = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPassword", t($this->t_base_error_mang.'fields/new_password'), 8, 255, true );
	}
	function set_szRetypePassword( $value )
	{
		$this->szRetypePassword = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szReTypePassword", t($this->t_base_error_mang.'fields/re_enter_password'), false, 255, true );
	}
	function set_idServiceType( $value )
	{
		$this->idServiceType = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idServiceType", t($this->t_base.'errors/SERVICE_TYPE'), 0, false, true );
	}
			
	function set_szOriginCountry( $value )
	{
		$this->szOriginCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "szOriginCountry", t($this->t_base.'errors/FROM_COUNTRY'), 0, false, true );
	}

	function set_szOriginCity( $value,$flag=false )
	{
		$this->szOriginCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szOriginCity", t($this->t_base.'errors/FROM_CITY'), false, 255, $flag );
	}

	function set_szOriginPostCode( $value,$flag=false )
	{
		$this->szOriginPostCode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szOriginPostCode", t($this->t_base.'errors/FROM_POSTCODE'), false, 255, $flag );
	}
	function set_dtTiming( $value )
	{
		$this->dtTiming = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "dtTiming", t($this->t_base.'errors/TIMING_DATE'), false, 255, true );
	}
	function set_szDestinationCountry( $value )
	{
		$this->szDestinationCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "szDestinationCountry", t($this->t_base.'errors/TO_COUNTRY'), 0, false, true );
	}

	function set_szDestinationCity( $value,$flag=false )
	{
		$this->szDestinationCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDestinationCity", t($this->t_base.'errors/TO_CITY'), false, 255, $flag );
	}

	function set_szDestinationPostCode( $value,$flag=false )
	{
		$this->szDestinationPostCode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDestinationPostCode", t($this->t_base.'errors/TO_POSTCODE'), false, 255, false,$flag );
	}
	
	function set_iLength( $value ,$counter,$max_limit,$cargo_measure,$key_counter=false,$iSearchMiniPageVersion=false)
	{
            if($max_limit<=0)
            {
                $max_limit = false ;
            }		
            else if((int)$value > $max_limit)
            {
                $this->iCargoExceed = 1;
                //$this->addError('fCargo_lenght_landing_page_'.$counter,t($this->t_base.'errors/max_cargo_length_accepted')." ".number_format((float)$max_limit)." ".$cargo_measure); 
                return false;
            }
            
            if($iSearchMiniPageVersion>2)
            {
                $szInputId = "iLength".$key_counter."_V_".$iSearchMiniPageVersion;
            }
            else
            {
                $szInputId = "fCargoDimention_landing_page";
            }
            $this->iLength[$counter] = $this->validateInput( $value, __VALIDATE_CARGO_DIMENSIONS__, $szInputId, t($this->t_base.'errors/CARGO_DIMENTIONS'), true, $max_limit, true );
	}
	
	function set_iHeight( $value , $counter,$max_limit,$cargo_measure,$key_counter=false,$iSearchMiniPageVersion=false )
	{
            if($max_limit<=0)
            {
                $max_limit = false ;
            }		
            else if((int)$value > $max_limit && $max_limit>0)
            {
                $this->iCargoExceed = 1;
                //$this->addError('fCargo_height_landing_page_'.$counter,t($this->t_base.'errors/max_cargo_height_accepted')." ".number_format((float)$max_limit)." ".$cargo_measure); 
                return false;
            }
            
            if($iSearchMiniPageVersion>2)
            {
                $szInputId = "iHeight".$key_counter."_V_".$iSearchMiniPageVersion;
            }
            else
            {
                $szInputId = "fCargoDimention_landing_page";
            }            
            $this->iHeight[$counter] = $this->validateInput( $value, __VALIDATE_CARGO_DIMENSIONS__, $szInputId, t($this->t_base.'errors/CARGO_DIMENTIONS'), true, $max_limit, true );
	} 
	function set_iWidth( $value,$counter,$max_limit,$cargo_measure,$key_counter=false,$iSearchMiniPageVersion=false )
	{
            if($max_limit<=0)
            {
                $max_limit = false;
            }	
            else if((int)$value > $max_limit && $max_limit>0)
            {
                $this->iCargoExceed = 1;
                //$this->addError('fCargo_width_landing_page_'.$counter,t($this->t_base.'errors/max_cargo_width_accepted')." ".number_format((float)$max_limit)." ".$cargo_measure); 
                return false;
            }

            if($iSearchMiniPageVersion>2)
            {
                $szInputId = "iWidth".$key_counter."_V_".$iSearchMiniPageVersion;
            }
            else
            {
                $szInputId = "fCargoDimention_landing_page";
            }
            $this->iWidth[$counter] = $this->validateInput( $value, __VALIDATE_CARGO_DIMENSIONS__, $szInputId, t($this->t_base.'errors/CARGO_DIMENTIONS'), true, $max_limit, true );
	}
        function set_iQuantity( $value , $counter,$max_limit=false,$key_counter=false,$iSearchMiniPageVersion=false)
	{
            if($max_limit<=0)
            {
                $max_limit = false;
            }
            if($iSearchMiniPageVersion>2)
            {
                $szInputId = "iQuantity".$key_counter."_V_".$iSearchMiniPageVersion;
            }
            else
            {
                $szInputId = "iQuantity_landing_page";
            }
            $this->iQuantity[$counter] = $this->validateInput( $value, __VALIDATE_CARGO_DIMENSIONS__, $szInputId, t($this->t_base.'errors/CARGO_QUNTITY'), true, $max_limit, true );
	}	
	function set_iWeight( $value,$counter , $max_limit,$weight_measure,$key_counter=false,$iSearchMiniPageVersion=false)
	{
            if($max_limit<=0)
            {
                $max_limit = false;
            }
            else if((int)$value > $max_limit && $max_limit>0)
            {
                $this->iCargoExceed = 1;
                //$this->addError('fCargoWeight_landing_page_'.$counter,t($this->t_base.'errors/max_cargo_weight_accepted')." ".number_format((float)$max_limit)." ".$weight_measure); 
                return false;
            }
            
            if($iSearchMiniPageVersion>2)
            {
                $szInputId = "iWeight".$key_counter."_V_".$iSearchMiniPageVersion;
            }
            else
            {
                $szInputId = "fCargoWeight_landing_page";
            }
            $this->iWeight[$counter] = $this->validateInput( $value, __VALIDATE_CARGO_DIMENSIONS__, $szInputId, t($this->t_base.'errors/Cargo_weight'), true, $max_limit, true );
	}
        
	function set_id( $value )
	{
            $this->id = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "id", t($this->t_base_error_mang.'fields/id'), 0, false, true );
	}
	function set_idCountry( $value )
	{
            $this->idCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idCountry", t($this->t_base_error_mang.'fields/idCountry'), 0, false, true );
	}
	function set_fPostCode( $value )
	{
            $this->szPostCode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPostCode", t($this->t_base_error_mang.'fields/szPostCode'), false, 255, false );
	}
	function set_szLat( $value )
	{
            $this->szLat = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLat", t($this->t_base_error_mang.'fields/szLat'), 0, false, true );
	}
	function set_szLng( $value )
	{
            $this->szLng = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLng", t($this->t_base_error_mang.'fields/szLng'), 0, false, true );
	}
	function set_szRegion1( $value )
	{
            $this->szRegion1 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szRegion1", t($this->t_base_error_mang.'fields/szRegion1'), false, 255, false );
	}
	function set_szRegion2( $value )
	{
            $this->szRegion2 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szRegion1", t($this->t_base_error_mang.'fields/szRegion1'), false, 255, false );
	}
	function set_szRegion3( $value )
	{
            $this->szRegion3 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szRegion3", t($this->t_base_error_mang.'fields/szRegion3'), false, 255, false );
	}
	function set_szRegion4( $value )
	{
            $this->szRegion4 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szRegion4", t($this->t_base_error_mang.'fields/szRegion4'), false, 255, false );
	}
	function set_szCity0( $value )
	{
            $this->szCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCity", t($this->t_base_error_mang.'fields/szCity'), false, 255, false );
	}
	function set_szCity1( $value )
	{
            $this->szCity1 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCity1", t($this->t_base_error_mang.'fields/szCity1'), false, 255, false );
	}
	function set_szCity2( $value )
	{
            $this->szCity2 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCity2", t($this->t_base_error_mang.'fields/szCity2'), false, 255, false );
	}
	function set_szCity3( $value )
	{
            $this->szCity3 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCity3", t($this->t_base_error_mang.'fields/szCity3'), false, 255, false );
	}
	function set_szCity4( $value )
	{
            $this->szCity4 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCity4", t($this->t_base_error_mang.'fields/szCity4'), false, 255, false );
	}
	function set_szCity5( $value )
	{
            $this->szCity5 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCity5", t($this->t_base_error_mang.'fields/szCity5'), false, 255, false );
	}
	function set_szCity6( $value )
	{
            $this->szCity6 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCity6", t($this->t_base_error_mang.'fields/szCity6'), false, 255, false );
	}
	function set_szCity7( $value )
	{
            $this->szCity7 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCity7", t($this->t_base_error_mang.'fields/szCity7'), false, 255, false );
	}
	function set_szCity8( $value )
	{
            $this->szCity8 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCity8", t($this->t_base_error_mang.'fields/szCity8'), false, 255, false );
	}
	function set_szCity9( $value )
	{
            $this->szCity9 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCity9", t($this->t_base_error_mang.'fields/szCity9'), false, 255, false );
	}
	function set_szCity10( $value )
	{
            $this->szCity10 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCity10", t($this->t_base_error_mang.'fields/szCity10'), false, 255, false );
	}
	function set_szCountry( $value )
	{
		$this->szOriginCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "szCountry", t($this->t_base_error_mang.'fields/country'), 0, false, true );
	}

	function set_szCity( $value )
	{
            $this->szOriginCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCity", t($this->t_base_error_mang.'fields/city'), false, 255, false );
	}

	function set_szPostCode( $value )
	{
		$this->szOriginPostCode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPostCode", t($this->t_base_error_mang.'fields/postcode'), false, 255, false );
	}
	function set_idPostCode( $value )
	{
		$this->idPostCode = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idPostCode", t($this->t_base_error_mang.'fields/idpostcode'), 0, false, true );
	}
	function set_iPriority( $value)
	{
		$this->iPriority = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iPriority", t($this->t_base_error_mang.'fields/priority'), 0, false, false );
	}
	function set_szSearchCity( $value )
	{
		$this->szOriginCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szOriginCity", t($this->t_base.'errors/FROM_CITY'), false, 255, false );
	}

	function set_szSearchPostCode( $value )
	{
		$this->szOriginPostCode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szOriginPostCode", t($this->t_base.'errors/FROM_POSTCODE'), false, 255, false );
	}
	
	function set_iVolume( $value ,$maxVolume)
	{ 
            if($value!='')
            {
                $valueArr=explode(",",$value);
                if(count($valueArr)==2)
                {
                    $value=$valueArr[0].".".$valueArr[1];
                    $value=round($value,1);
                }
                else
                {
                    $value=$value;
                }
            }
            if($value<=0)
            {
                $value = '';
            }
            $maxVolume=round($maxVolume); 
            $this->iVolume = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iVolume", t($this->t_base.'errors/iVolume'), false, $maxVolume, true );
	}
	
	function set_szDestinationCountryStr($value)
	{
            $this->szDestinationCountryStr = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szDestinationCountryStr", t($this->t_base.'errors/TO_COUNTRY'), false, false, false );
	} 
	function set_iNumColli($value)
	{  
            $this->iNumColli = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iNumColli", t($this->t_base.'errors/pallet'), 1, false, true );
	}
        function set_iPalletType($value)
	{ 
            $this->iPalletType = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iPalletType", t($this->t_base.'errors/pallet_type'), false, false, true );
	}
        function set_szPalletDescriptions($value,$required_flag=false)
	{ 
            $this->szPalletDescriptions = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iPalletType", t($this->t_base.'errors/pallet_type'), false, false, $required_flag );
	} 
        
        function set_fTotalWeight( $value ,$maxCargoWeight)
	{
            if($value!='')
            {
                $valueArr=explode(",",$value);
                if(count($valueArr)==2)
                {
                        $value=$valueArr[0].".".$valueArr[1];
                        $value=round($value,1);
                }
                else
                {
                        $value=$value;
                }
            }		 
            if($value<=0)
            {
                $value = '';
            }
            $maxCargoWeight=round($maxCargoWeight,2);
            $this->fTotalWeight = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fCargoWeight_landing_page", t($this->t_base.'errors/Cargo_weight_simple'), false, $maxCargoWeight, true );
	}
        
	function set_iNewWeight( $value ,$maxCargoWeight)
	{ 
            if($value!='')
            {
                $valueArr=explode(",",$value);
                if(count($valueArr)==2)
                {
                        $value=$valueArr[0].".".$valueArr[1];
                        $value=round($value,1);
                }
                else
                {
                        $value=$value;
                }
            }	
            if($value<=0)
            {
                $value = '';
            }
            $maxCargoWeight=round($maxCargoWeight,2);
            $this->iWeight = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fCargoWeight_landing_page", t($this->t_base.'errors/Cargo_weight_simple'), false, $maxCargoWeight, true );
	}
        function set_szFirstName($value)
	{  
            $this->szFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szFirstName", t($this->t_base.'errors/f_name'), false, 255, true );
	}
        function set_szLastName($value)
	{  
            $this->szLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLastName", t($this->t_base.'errors/l_name'), false, 255, true );
	}
        function set_szAddress($value)
	{  
            $this->szAddress = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAddress", t($this->t_base.'errors/address'), false, 255, true );
	}
        function set_szVoguePostcode($value)
	{  
            $this->szPostcode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPostcode", t($this->t_base.'errors/postcode'), false, 255, true );
	}
        function set_szVogueCity($value)
	{  
            $this->szCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCity", t($this->t_base.'errors/city'), false, 255, true );
	}
        function set_szEmail($value)
	{  
            $this->szEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szEmail", t($this->t_base.'errors/email'), false, 255, true );
	}
        function set_szPhone($value)
	{  
            $this->szPhone = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPhone", t($this->t_base.'errors/phone'), false, false, true );
	}
        function set_idDialCode($value)
	{  
            $this->idDialCode = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idDialCode", t($this->t_base.'errors/dial_code'), false, false, true );
	}
        function set_iContactShipper($value)
	{  
            $this->iContactShipper = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iContactShipper", t($this->t_base.'errors/contact_shipper'), false, false, true );
	} 
        function set_idCargoCategory( $value , $counter,$key_counter,$iSearchMiniPageVersion,$bTryitout=false)
	{ 
            if($iSearchMiniPageVersion>2)
            {
                if($bTryitout)
                {
                    $szInputId = "idCargoCategory".$key_counter."_V_".$iSearchMiniPageVersion;
                }
                else
                {
                    $szInputId = "idCargoCategory".$key_counter."_V_".$iSearchMiniPageVersion."_container"; 
                } 
            }
            else
            {
                $szInputId = "idCargoCategory";
            }
            $this->idCargoCategory[$counter] = $this->validateInput( $value, __VLD_CASE_NUMERIC__, $szInputId, t($this->t_base.'errors/cargo_category'), false, false, true);
	}
        function set_idStandardCargoType( $value , $counter,$key_counter,$iSearchMiniPageVersion,$bTryitout=false)
	{ 
            if($iSearchMiniPageVersion>2)
            {
                if($bTryitout)
                {
                    $szInputId = "idStandardCargoType".$key_counter."_V_".$iSearchMiniPageVersion;
                }
                else
                {
                    $szInputId = "idStandardCargoType".$key_counter."_V_".$iSearchMiniPageVersion."_container";
                } 
            }
            else
            {
                $szInputId = "idStandardCargoType";
            }
            $this->idStandardCargoType[$counter] = $this->validateInput( $value, __VLD_CASE_NUMERIC__, $szInputId, t($this->t_base.'errors/cargo_type'), false, false, true);
	}
        
        function set_iVogueQuantity( $value , $counter,$key_counter=false,$iSearchMiniPageVersion=false,$bTryitout=false)
	{
            if($max_limit<=0)
            {
                $max_limit = false;
            }
            if($iSearchMiniPageVersion>2)
            {
                if($bTryitout)
                {
                    $szInputId = "iQuantity".$key_counter."_V_".$iSearchMiniPageVersion;
                }
                else
                {
                    $szInputId = "iQuantity".$key_counter."_V_".$iSearchMiniPageVersion."_container";
                } 
            }
            else
            {
                $szInputId = "iQuantity_landing_page";
            }
            $this->iQuantity[$counter] = $this->validateInput( $value, __VALIDATE_CARGO_DIMENSIONS__, $szInputId, t($this->t_base.'errors/CARGO_QUNTITY'), true, $max_limit, true );
	}
        function set_szFriendlyName($value)
        {
            $this->szFriendlyName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szFriendlyName", t($this->t_base_error_mang.'fields/szFriendlyName'), false, false, true );
        }
        function set_szReminderSubject($value,$flag=false)
        {
            $this->szReminderSubject = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szReminderSubject", t($this->t_base_error_mang.'fields/szReminderSubject'), false, false, $flag );
        }
        function set_szReminderEmailBody($value,$flag=false)
        {
            $this->szReminderEmailBody = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szReminderEmailBody", t($this->t_base_error_mang.'fields/szReminderEmailBody'), false, false, $flag );
        }
        
        function set_szConsigneeCompanyName($value,$flag=false)
        {
            $this->szConsigneeCompanyName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeCompanyName", '', false, false, $flag );
        }
        function set_szConsigneeFirstName($value,$flag=true)
        {
            $this->szConsigneeFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeFirstName", '', false, false, $flag );
        }
        function set_szConsigneeLastName($value,$flag=true)
        {
            $this->szConsigneeLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeLastName", '', false, false, $flag );
        }
        function set_szConsigneeEmail($value,$flag=true)
        {
            $this->szConsigneeEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szConsigneeEmail", '', false, false, $flag );
        }
        function set_szConsigneePhone($value,$flag=true)
        {
            $this->szConsigneePhone = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneePhone", '', false, false, $flag );
        }
        function set_idConsigneeDialCode($value,$flag=true)
        {
            $this->idConsigneeDialCode = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idConsigneeDialCode", '', false, false, $flag );
        }
        function set_idConsigneeCountry($value,$flag=true)
        {
            $this->idConsigneeCountry = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idConsigneeCountry", '', false, false, $flag );
        }
        function set_szConsigneeAddress($value,$flag=true)
        {
            $this->szConsigneeAddress = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeAddress", '', false, false, $flag );
        }
        function set_szConsigneePostcode($value,$flag=true)
        {
            $this->szConsigneePostcode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneePostcode", '', false, false, $flag );
        }
        function set_szConsigneeCity($value,$flag=true)
        {
            $this->szConsigneeCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeCity", '', false, false, $flag );
        } 
}	

?>
