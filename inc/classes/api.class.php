<?php
/**
 * This file is the container for partner api related functionality. 
 *
 * api.class.php
 *
 * @copyright Copyright (C) 2015 Transporteca
 * @author Ajay
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_CLASSES__ . "/responseErrors.class.php");

class cApi extends cDatabase
{   
    public static $idPartnerApiEventApi=false;
    public function callTransportecaPartnerAPI($clientApiHeaderAry,$priceInputAry,$szMethod='PRICE')
    {     
        $priceInputAry = $this->formatApiRequestParams($priceInputAry);  
        $kPartner = new cPartner();
        $idPartnerApiEventApi = $kPartner->addPartnerApiEventLogs($priceInputAry,$clientApiHeaderAry);
        $szReferenceToken = $priceInputAry['token']; 
         
        cApi::$idPartnerApiEventApi = $idPartnerApiEventApi;
        
        if($kPartner->validatePartnerKey($clientApiHeaderAry,$idPartnerApiEventApi,$szMethod,$szReferenceToken))
        {       
            if($szMethod=='BOOKING')
            { 
                /*
                * callig API to validate input and create booking on transporteca
                */ 
                $kPartner->createBooking($priceInputAry,$szMethod);    
            }
            else if($szMethod=='VALIDATE_PRICE')
            {
                /*
                * callig API to validate price after user has filled their address
                */ 
                $kPartner->validatePrice($priceInputAry,$szMethod);    
            }
            else if($szMethod=='CONFIRM_PAYMENT')
            {
                /*
                * callig API to validate price after user has filled their address
                */ 
                $kPartner->confirmPayment($priceInputAry,$szMethod);    
            }
            else
            {
                /*
                * callig API to validate input and respond with list of available service on transporteca.
                */
                $kPartner->getPrice($priceInputAry,$szMethod);   
                 
            }    
            if(cPartner::$szGlobalErrorType=='VALIDATION')
            { 
                $szResponseSerialized = cResponseError::getSerializedMessages();
                $kPartner->updateApiEventLogResponse($szResponseSerialized,404,$idPartnerApiEventApi);
                $szResponseJson = cResponseError::getMessages();
                ob_end_clean();
                header( 'HTTP/1.1 200 OK' );
                echo $szResponseJson;
                $priceCalculationLogs = cPartner::$priceCalculationLogs;
                /*
                if(!empty($priceCalculationLogs))
                {  
                    echo "<a href='javascript:void(0)' onclick=showHideCourierCal('courier_service_calculation')>.</a> <div id='courier_service_calculation' class='booking-page clearfix' style='display:none'><div id='popup-bg'></div><div id='popup-container'><div class='popup abandon-popup' style='margin-left:20px;width:85%;'><p class='close-icon' align='right'><a onclick=showHideCourierCal('courier_service_calculation') href='javascript:void(0);'><img alt='close' src='".__BASE_STORE_IMAGE_URL__."/close1.png'>
                        </a></p><div style='text-align:left;height:400px;overflow-y:auto;overflow-x:hidden;width:100%;'>".$priceCalculationLogs." </div></div> </div></div>";                    
                     
                } 
                 * 
                 */
                die;
            }
            else if(cPartner::$szGlobalErrorType=='NOTIFICATION')
            {
                $szResponseSerialized = cResponseError::getSerializedMessages();
                $kPartner->updateApiEventLogResponse($szResponseSerialized,200,$idPartnerApiEventApi);
                $szResponseJson = cResponseError::getMessages(); 
                ob_end_clean();
                header( 'HTTP/1.1 200 OK' );
                echo $szResponseJson;
                die;
            }
            else if(cPartner::$szGlobalErrorType=='SUCCESS')
            { 
                $szResponseSerialized = cResponseError::getSerializedMessages();
                $kPartner->updateApiEventLogResponse($szResponseSerialized,200,$idPartnerApiEventApi);
                $szResponseJson = cResponseError::getMessages(); 
                ob_end_clean();
                header( 'HTTP/1.1 200 OK' );
                echo $szResponseJson;
                $priceCalculationLogs = cPartner::$priceCalculationLogs;
                /*
                if(!empty($priceCalculationLogs))
                {  
                    echo "<a href='javascript:void(0)' onclick=showHideCourierCal('courier_service_calculation')>.</a> <div id='courier_service_calculation' class='booking-page clearfix' style='display:none'><div id='popup-bg'></div><div id='popup-container'><div class='popup abandon-popup' style='margin-left:20px;width:85%;'><p class='close-icon' align='right'><a onclick=showHideCourierCal('courier_service_calculation') href='javascript:void(0);'><img alt='close' src='".__BASE_STORE_IMAGE_URL__."/close1.png'>
                        </a></p><div style='text-align:left;height:400px;overflow-y:auto;overflow-x:hidden;width:100%;'>".$priceCalculationLogs." </div></div> </div></div>";
                     
                } 
                 * 
                 */
                die;
            }
        }
        else
        { 
        //    ob_end_clean();
        //    cResponseError::display_http_response_header(401);
            $szResponseSerialized = cResponseError::getSerializedMessages();
            $kPartner->updateApiEventLogResponse($szResponseSerialized,404,$idPartnerApiEventApi);
            $szResponseJson = cResponseError::getMessages();
            ob_end_clean();
            header( 'HTTP/1.1 200 Not Authorized' );
            echo $szResponseJson;
            die;
        } 
    }
    
    function logBookingACtivity($addBookingActivityLogs)
    { 
        if(!empty($addBookingActivityLogs))
        {      
            $idAdmin = $_SESSION['admin_id']; 
            if(empty($addBookingActivityLogs['szType']))
            {
                $addBookingActivityLogs['szType'] = "BOOKING";
            }
            $query="	
                INSERT INTO
                    ".__DBC_SCHEMATA_BOOKING_ACTIVITY__."
                (
                    idBooking, 
                    szDataTobeUpdated,
                    szType,
                    szIpAddress, 
                    szHttpReferrar,
                    szBrowser, 
                    idAdmin,
                    dtCreatedOn
                )
                VALUES
                (
                    '".mysql_escape_custom(trim($addBookingActivityLogs['idBooking']))."',
                    '".mysql_escape_custom(trim($addBookingActivityLogs['szDateTobeUpdated']))."', 
                    '".mysql_escape_custom(trim($addBookingActivityLogs['szType']))."',
                    '".mysql_escape_custom(__REMOTE_ADDR__)."',  
                    '".mysql_escape_custom(__HTTP_REFERER__)."',  
                    '".mysql_escape_custom(__HTTP_USER_AGENT__)."',
                    '".$idAdmin."', 
                    now()
                )  
            "; 
            //echo $query ;
            if($result=$this->exeSQL($query))
            { 
                return true;
            }
            else
            { 
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
        }
    }
    
    public static function encodeString($szRawString)
    {
        if(!empty($szRawString))
        {   
            if(is_string($szRawString) && (mb_detect_encoding($szRawString, "UTF-8", true) != "UTF-8"))
            {
                $szRawString = utf8_encode($szRawString);  
            }   
            return $szRawString; 
        }
    }
    
    public function formatApiRequestParams($priceInputAry)
    {
        if(!empty($priceInputAry))
        { 
            foreach($priceInputAry as $key=>$value)
            {
                if(is_array($value))
                {
                    $priceInputAry[$key] = $this->formatApiRequestParams($value);
                }
                else if(!is_numeric($value))
                { 
                    if((mb_detect_encoding($value, "UTF-8", true) == "UTF-8"))
                    {
                        $szValues = utf8_decode($value); 
                    }
                    else
                    {
                        $szValues = $value;
                    }
                    $priceInputAry[$key] = $szValues;
                }
            } 
            return $priceInputAry;
        }
    }
    
    function getAllErrorCode()
    {  
        $query="
            SELECT  
                DISTINCT szErrorCode 
            FROM	
                ".__DBC_SCHEMATA_ERROR_DETAIL__." 
            WHERE
                iInternalErrorCode = '0'
        "; 
        if($result=$this->exeSQL($query))
        {
            if($this->getRowCnt()>0)
            {
                $ctr = 0;
                $ret_ary = array();
                while($row = $this->getAssoc($result))
                {
                    $ret_ary[$ctr] = $row;
                    $ctr++;
                }
                return $ret_ary;
            }
            return false;
        }
        else
        {
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
    
    function getPartnerAPIEventLogs($idEventLogs)
    {  
        if($idEventLogs)
        {
            $query="
                SELECT  
                    * 
                FROM	
                    ".__DBC_SCHEMATA_PARTNERS_API_EVENT_LOG_CALL__." 
                WHERE
                    id = '".(int)$idEventLogs."'
            "; 
            if($result=$this->exeSQL($query))
            {
                if($this->getRowCnt()>0)
                { 
                    $row = $this->getAssoc($result); 
                    return $row;
                }
                return false;
            }
            else
            {
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        } 
    }
    
    function getPartnerAPIEventErrorLogs($flag=false)
    {    
        $date30Day =date('Y-m-d',strtotime('-30 DAY'));
        $query="
            SELECT  
                szResposeData,
                szRequestParams,
                dtCreated
            FROM	
                ".__DBC_SCHEMATA_PARTNERS_API_EVENT_LOG_CALL__." 
            WHERE
                szResponseCode <> '200'
            AND
                szResposeData<>''
            AND    
            (
                dtCreated>='".$date30Day."'
            OR
                dtUpdatedOn>='".$date30Day."'
            )
            ORDER BY
                dtCreated DESC
                
        "; 
        if($result=$this->exeSQL($query))
        {
            if($this->getRowCnt()>0)
            { 
                $ctr=0;
                while($row = $this->getAssoc($result))
                {
                    if($flag)
                    {
                        $res_arr[$ctr]=$row;
                    }
                    else
                    {
                        $res_arr[$ctr]=$row['szResposeData'];
                    }
                        
                    ++$ctr;
                }
                return $res_arr;
            }
            return array();
        }
        else
        {
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
        
    }
}
?>