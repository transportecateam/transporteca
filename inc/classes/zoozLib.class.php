<?php
/**
 * This file is the container for all customer's zooz payment related functionality.
 * All functionality related to user payment should be contained in this class.
 *
 * zooz.class.php
 *
 * @copyright Copyright (C) 2012 Transport-ece
 * @author Ashish
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once(__APP_PATH__."/stripe/init.php");

class cZoozLib extends cDatabase
{
    private static $sandbox   = __URL_FLAG__;
    private static $uniqueID  = __API_UNIQUE_KEY__;
    private static $appKey    = __API_KEY__;

    private static function Run() 
    {
        if( self::$sandbox )
            $url = 'https://sandbox.zooz.co/mobile/SecuredWebServlet';
        else
            $url = 'https://app.zooz.com/mobile/SecuredWebServlet';

        if( !function_exists( 'curl_init' ) )
                return false;

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $url );

        curl_setopt( $ch, CURLOPT_HTTPHEADER, array( 
                'ZooZUniqueID: ' . self::$uniqueID, 
                'ZooZAppKey: ' . urlencode( self::$appKey ), 
                'ZooZResponseType: NVP' 
        ) );
        curl_setopt( $ch, CURLOPT_TIMEOUT, 10 );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 ); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        return( $ch );
    }

    public static function newTransaction($transactionAry) 
    {
        if(!empty($transactionAry))
        { 
            $ch = self::Run();  
            
            $postFields = "cmd=openTrx&amount=" . (float)$transactionAry['fTotalAmount']  . "&currencyCode=" . $transactionAry['szCurrency'];
            
            $userSessionData = array();
            $userSessionData = get_user_session_data(true);
            $idUserSession = $userSessionData['idUser'];
        
            $idCustomer = (int)$idUserSession;
            $kUser = new cUser();
            $kUser->getUserDetails($idCustomer);
                   
            $kConfig = new cConfig();
            $kConfig->loadCountry($kUser->idInternationalDialCode);
            $idInternationalDialCode = $kConfig->iInternationDialCode;
            //
            $postFields .= "&email=" . $kUser->szEmail;
            $postFields .= "&firstName=" . $kUser->szFirstName;
            $postFields .= "&lastName=" . $kUser->szLastName;
            $postFields .= "&addressZip=" . $kUser->szPostCode; 
            $postFields .= "&countryCode= ".$idInternationalDialCode;
            $postFields .= "&phoneNumber= ". $kUser->szPhoneNumber;   
            $postFields .= "&isShippingRequired=false"; 
            $postFields .= "&invoice.additionalDetails=" . $transactionAry['szAdditionalDetails'];
            $postFields .= "&invoice.number=" . $transactionAry['szBookingRef']; 
             
            $postFields .= "&addresses.billing.address1 = ".$transactionAry['szShipperAddress'];
            $postFields .= "&addresses.billing.zipCode = ".$transactionAry['szShipperPostCode'];
            $postFields .= "&addresses.billing.countryCode = ".$transactionAry['szShipperCountryCode'];
            $postFields .= "&addresses.billing.city = ".$transactionAry['szShipperCity']; 
            
            
//            $postFields .= "&user.addresses.billing.zipCode = ".$transactionAry['szShipperPostCode'];
//            $postFields .= "&user.addresses.billing.countryCode = ".$transactionAry['szShipperCountryCode'];
//            $postFields .= "&user.addresses.billing.city = ".$transactionAry['szShipperCity']; 
//            $postFields .= "&user.addresses.shipping.zipCode = ".$transactionAry['szConsigneePostCode'];
//            $postFields .= "&user.addresses.shipping.countryCode = ".$transactionAry['szConsigneeCountryCode'];
//            $postFields .= "&user.addresses.shipping.city = ".$transactionAry['szConsigneeCity'];
            
            $f = fopen(__APP_PATH__."/logs/zoozApiLog.log", "a");
            fwrite($f, "\n\n Function: newTransaction \n");
            fwrite($f, "\n\n REQUEST PARAMS: \n".$postFields);
            
            curl_setopt( $ch, CURLOPT_POST, 47 );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $postFields );

            $chResult = curl_exec($ch);
            $infoAry = curl_getinfo($ch); 
            $vars = explode( '&', $chResult ); 
            $result = Array();
            foreach( $vars as $var ) {
                $actualVar = explode( '=', $var );
                $result[( $actualVar[0] )] = $actualVar[1];
            }
            fwrite($f, "\n\n RESPONSE PARAMS: \n".print_R($vars,true));
            
            if ($result[ 'statusCode' ] == 0) {
                // Get token from ZooZ server
                $trimmedSessionToken = rtrim($result[ 'sessionToken' ], "\n"); 
                // Send token back to page
                return( Array( 'result' => true, 'token' => $result[ 'token' ], 'sessionToken' => $result[ 'sessionToken' ], 'trimmedSessionToken' => $trimmedSessionToken ) );
            } else {
                return( Array( 'result' => false, 'token' => false, 'sessionToken' => 0, 'trimmedSessionToken' => 0, 'error' => urldecode( $result[ 'errorMessage' ] ) ) );
            } 
            curl_close($ch);
        }
    }

    public static function verifyTransaction( $id ) 
    {
        $ch = self::Run(); 
        $postFields = 'cmd=verifyTrx&trxId=' . urlencode( $id );
        
        $f = fopen(__APP_PATH__."/logs/zoozApiLog.log", "a");
        fwrite($f, "\n\n Function: verifyTransaction \n");
        fwrite($f, "\n\n REQUEST PARAMS: \n".$postFields);

        curl_setopt( $ch, CURLOPT_POST, 2 );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $postFields );

        $chResult = curl_exec($ch);
 
        $vars = explode( '&', $chResult );
        $result = Array();
        foreach( $vars as $var ) {
            $actualVar = explode( '=', $var );
            $result[( $actualVar[0] )] = $actualVar[1];
        }

        fwrite($f, "\n\n RESPONSE PARAMS: \n".print_R($vars,true));
        if( $result[ 'statusCode' ] == 0 ) {
            $trimmedSessionToken = rtrim($result[ 'sessionToken' ], "\n");
            $trimmedTrxToken = rtrim($result[ 'transactionID' ], "\n");

            // Verify sessionToken with newTransaction();
            return( Array( 'result' => true, 'sessionToken' => $trimmedSessionToken, 'trxToken' => $trimmedTrxToken ) );
        } else {
            return( Array( 'result' => false, 'sessionToken' => 0, 'trxToken' => 0, 'error' => urldecode( $result[ 'errorMessage' ] ) ) );
        }
    }
    
    public static function chargeAccountWithStripe($stripeChargeAry) 
    { 
        if(!empty($stripeChargeAry))
        { 
            $stripeResponseAry = array(); 
            try {
                 // Create the charge on Stripe's servers - this will charge the user's card
                $stripe = \Stripe\Stripe::setApiKey(__STRIPE_API_SECRET_KEY__);  
                $stripeResponse = \Stripe\Charge::create(array(
                    "amount" => $stripeChargeAry['fTotalCostForBooking'],
                    "currency" => $stripeChargeAry['szCurrency'],
                    "source" => $stripeChargeAry['szStripeToken'],
                    "description" => $stripeChargeAry['szDescription'] 
                ));    
                $stripeResponseAry = $stripeResponse->__toArray(true);  
                $szResponseData = print_R($stripeResponseAry,true);
            } 
            catch(Exception $e) 
            {
              // The card has been declined  
                $stripeResponseAry['status'] = 'DECLINE';
                $stripeResponseAry['szErrorMsg'] = $e->getMessage();
                $szResponseData = print_R($e,true);
            }  
            
            $stripeApiLogsAry = array();
            $stripeApiLogsAry['idBooking'] = $stripeChargeAry['idBooking'] ; 
            $stripeApiLogsAry['szRequestData'] = print_R($stripeChargeAry,true);
            $stripeApiLogsAry['szResponseData'] = $szResponseData;
            
            $kZoozLib = new cZoozLib();
            $kZoozLib->addStripeApiLogs($stripeApiLogsAry);  
            return $stripeResponseAry;
        } 
    }
    
    public function addStripeApiLogs($data)
    {
        if(!empty($data))
        {
            $query="
                INSERT INTO
                    ".__DBC_SCHEMATA_STRIPE_API_LOGS__."
                (
                    idBooking, 
                    szRequestData,
                    szResponseData,
                    szReferer,
                    szRemoteIP,
                    szUserAgent,
                    dtCreatedOn
                )
                VALUES
                (
                    '".mysql_escape_custom(trim($data['idBooking']))."',
                    '".mysql_escape_custom(trim($data['szRequestData']))."',
                    '".mysql_escape_custom(trim($data['szResponseData']))."', 
                    '".__HTTP_REFERER__."',
                    '".__REMOTE_ADDR__."',
                    '".__HTTP_USER_AGENT__."',
                    now()
                )
            ";		 
            if(($result = $this->exeSQL($query)))
            {
                return true;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }		
        }
    }
    
    public function processBookingPaymentByStripeToken($postSearchAry,$szStripeToken,$szSource='Transporteca')
    {
        if(!empty($postSearchAry) && !empty($szStripeToken))
        {  
            $idBooking = (int)$postSearchAry['id'];
            $kBooking = new cBooking();
            $kConfig = new cConfig();
            $kZoozLib = new cZoozLib();
            $kZooz = new cZooz();
            $iLanguage = getLanguageId();

            $kForwarderContact = new cForwarderContact();
            $kUser = new cUser();
            $kWHSSearch = new cWHSSearch();
    
            $item_ref_text = createStripeDescription($postSearchAry); 
            if($postSearchAry['iInsuranceIncluded']==1)
            {
                $fTotalCostForBooking =  $postSearchAry['fTotalPriceCustomerCurrency'] + round($postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']); 
            }
            else
            {
                $fTotalCostForBooking = $postSearchAry['fTotalPriceCustomerCurrency'];
            }   
            $fTotalCostForBooking = $fTotalCostForBooking + $postSearchAry['fTotalVat'];  
            $fTotalCostForBooking = round($fTotalCostForBooking,2);

            //converting values in cent
            $fTotalCostForBookingCent = $fTotalCostForBooking * 100;  

            //Adding payment initialization logs
            $addZoozLogsAry = array();
            $addZoozLogsAry['idBooking'] = $postSearchAry['id'];
            $addZoozLogsAry['szBookingRef'] = $postSearchAry['szBookingRef'];
            $addZoozLogsAry['szToken'] = $szStripeToken;
            $addZoozLogsAry['szSessionToken'] = $szStripeToken; 
            $addZoozLogsAry['szStatus'] = 'PAYMENT_INITIATED';  
            $addZoozLogsAry['szDeveloperNotes'] = "Initialize stripe payment token";
            $addZoozLogsAry['iPaymentMode'] = 2; 
            $addZoozLogsAry['szSource'] = $szSource;
            $kZooz->add($addZoozLogsAry);

            //Creating Input array to call stripe api
            $stripeChargeAry = array();
            $stripeChargeAry['fTotalCostForBooking'] = $fTotalCostForBookingCent; 
            $stripeChargeAry['szCurrency'] = $postSearchAry['szCurrency']; 
            $stripeChargeAry['szStripeToken'] = $szStripeToken; 
            $stripeChargeAry['szDescription'] = substr($item_ref_text,0,-1)." - ".$postSearchAry['szBookingRef']; 
            $stripeChargeAry['szEmail'] = $postSearchAry['szEmail'];
            $stripeChargeAry['szBookingRef'] = $postSearchAry['szBookingRef'];
            $stripeChargeAry['idBooking'] = $postSearchAry['id'];

            $stripeApiResponseAry = array();
            $stripeApiResponseAry = self::chargeAccountWithStripe($stripeChargeAry);

            $szPaymentStatus = "";
            $addZoozLogsAry = array(); 
            $addZoozLogsAry['szSessionToken'] = $szStripeToken;   

            if(!empty($stripeApiResponseAry) && $stripeApiResponseAry['status']!='DECLINE')
            { 
                $szStripeTransactionID = $stripeApiResponseAry['id'];
                $szStripeBalanceTransactionID = $stripeApiResponseAry['balance_transaction'];  
                
                $addZoozLogsAry['idTransaction'] = $stripeApiResponseAry['id']; 
                $addZoozLogsAry['idTransactionDisplay'] = $stripeApiResponseAry['balance_transaction'];  

                $status = 'Completed';
                $addZoozLogsAry['szStatus'] = "PAYMENT_COMPLETED";
                $addZoozLogsAry['szDeveloperNotes'] = "Credit Card payment successfully completed.";  
                $fCustomerPaidAmount = ($stripeApiResponseAry['amount']/100);
                
                $szPaymentStatus = "Completed";
                $szPaymentDescription = "Credit Card payment successfully completed.";
            }
            else
            {
                $status = 'Failed';
                $addZoozLogsAry['szStatus'] = "PAYMENT_FAILED";
                $addZoozLogsAry['szDeveloperNotes'] = "Credit Card payment failed. check tblstripelogs table for more details";
                
                $szPaymentStatus = "Failed";
                if($stripeApiResponseAry['szErrorMsg']!='')
                {
                    $szPaymentDescription = $stripeApiResponseAry['szErrorMsg'];
                }
                else
                {
                    if($szSource=='API')
                    {
                        $szPaymentDescription = "Due to internal server error we could not process your payment this time. Please use our 'payment' API to retry payment for this booking.";
                    }
                    else
                    {
                        $szPaymentDescription = "Credit Card payment failed";
                    } 
                }
            } 
            //Updating stripe logs in database
            $kZooz->updateTransactionDetails($addZoozLogsAry); 
            $kZooz->updateBookingZoozPaymentDetail($szStripeBalanceTransactionID,$status,true,$fCustomerPaidAmount);

            $retAry = array();
            $retAry['szPaymentStatus'] = $szPaymentStatus;
            $retAry['szPaymentDescription'] = $szPaymentDescription;
            $retAry['szStripeTransactionID'] = $szStripeTransactionID; 
            $retAry['szStripeBalanceTransactionID'] = $szStripeBalanceTransactionID; 
            return $retAry; 
        }
    }
    
    public static function updateStripeInvoiceDescription($stripeChargeAry) 
    { 
        if(!empty($stripeChargeAry))
        { 
            $stripeResponseAry = array(); 
            try {
                 // Create the charge on Stripe's servers - this will charge the user's card
                $stripe = \Stripe\Stripe::setApiKey(__STRIPE_API_SECRET_KEY__);  
                $stripeResponse = \Stripe\Charge::retrieve($stripeChargeAry['szCargeID']); 
                $stripeResponse->description .= " - ".$stripeChargeAry['szBookingRef'];
                $stripeResponse->save();   
                $stripeResponseAry = $stripeResponse->__toArray(true);   
                $szResponseData = print_R($stripeResponseAry,true);
            } 
            catch(Exception $e) 
            {
              // The card has been declined  
                $stripeResponseAry['status'] = 'DECLINE';
                $stripeResponseAry['szErrorMsg'] = $e->getMessage();
                $szResponseData = print_R($e,true);
            }  
            
            $stripeApiLogsAry = array();
            $stripeApiLogsAry['idBooking'] = $stripeChargeAry['idBooking'] ; 
            $stripeApiLogsAry['szRequestData'] = print_R($stripeChargeAry,true);
            $stripeApiLogsAry['szResponseData'] = $szResponseData;
            
            $kZoozLib = new cZoozLib();
            $kZoozLib->addStripeApiLogs($stripeApiLogsAry);  
            return $stripeResponseAry;
        } 
    } 
}
?>