<?php
/**
 * This file is the container for all registered  shipper and consignees related functionality.
 * All functionality related to user details should be contained in this class.
 *
 * registerShipperConsignees.class.php
 *
 * @copyright Copyright (C) 2012 Transporteca, LLC
 * @author Anil
 * @package Transporteca Development
 */
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_INC__ . "/functions.php" );
require_once( __APP_PATH_INC__ . "/classes/capsuleCrm.class.php" );

class cRegisterShipCon extends cDatabase
{
    /**
     * These are the variables for each user.
     */

    var $id;
    var $szFirstName;
    var $szLastName;
    var $szEmail;
    var $szPhoneNumber;
    var $szCountry;
    var $szCurrency;
    var $szAddress1;
    var $szAddress2;
    var $szAddress3;
    var $szCompanyName;
    var $szState;
    var $szCity;
    var $szPostcode;
    var $idRegistered;
    var $idGroup;
    var $idCustomer;
    var $dtCreatedOn;
    var $iActive;
    var $t_base = 'Users/ShipperConsigness/';
    
	/**
	 * class constructor for php5
	 *
	 * @return bool
	 */
	function __construct()
	{
		parent::__construct();

		// establish a Database connection.
		//$this->connect( __DBC_USER__, __DBC_PASSWD__, __DBC_SCHEMATA__, __DBC_HOST__);
		return true;
	}
	
	
	function shipconGroup($iLanguage)
	{
//            if($iLanguage==__LANGUAGE_ID_DANISH__)
//            {
//                $query_select = ", szDescriptionDanish as szDescription ";
//            }
//            else
//            {
//                $query_select = ", szDescription ";
//            }
            
            $kConfig = new cConfig();
            $configLangArr=$kConfig->getConfigurationLanguageData('__TABLE_SHIP_CON_GROUP__',$iLanguage);
		
            $query="
                SELECT
                    id
                FROM
                    ".__DBC_SCHEMATA_SHIPPER_CONSIGNEES_GROUP__."				
            ";
            if( ( $result = $this->exeSQL( $query ) ) )
            {
        	// Check if the user exists
                if( $this->getRowCnt() > 0 )
                {
                    while($row=$this->getAssoc($result))
                    {
                        if(!empty($configLangArr[$row['id']]))
                        {
                            $shipConGroup[] = array_merge($configLangArr[$row['id']],$row);
                        }else
                        {
                            $shipConGroup[] = $row;
                        }
                    } 
                    return $shipConGroup;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                    return array();
            }
	}
	
	function addRegisterShipperConsigness($data,$session_flag=false,$new_page_flag=false)
	{
            if(is_array($data))
            {			
                $this->set_szFirstName(trim(sanitize_all_html_input($data['szFirstName'])));
                $this->set_szLastName(trim(sanitize_all_html_input($data['szLastName'])));
                $this->set_szFirstName(trim(sanitize_all_html_input($data['szFirstName'])));
                $this->set_id(trim(sanitize_all_html_input($data['idCustomer'])));	
                $this->set_szCompanyName(trim(sanitize_all_html_input($data['szCompanyName'])));
                $this->set_szCountry(trim(sanitize_all_html_input($data['szCountry'])));
                $this->set_iInternationalDialCode(trim(sanitize_all_html_input($data['iInternationalDialCode']))); 

                if($new_page_flag)
                {
                    $this->set_szCity(trim(sanitize_all_html_input($data['szCity'])),false);
                    $this->set_szState(trim(sanitize_all_html_input($data['szProvince'])),false);
                    $this->set_szAddress1(trim(sanitize_all_html_input($data['szAddress1'])),false);
                    $this->set_szPhoneNumber(trim(sanitize_all_html_input($data['szPhoneNumberUpdate'])));
                }
                else
                {
                    $this->set_szCity(trim(sanitize_all_html_input($data['szCity'])));
                    $this->set_szState(trim(sanitize_all_html_input($data['szProvince'])));
                    $this->set_szAddress1(trim(sanitize_all_html_input($data['szAddress1'])));
                    $this->set_szPhoneNumber(trim(sanitize_all_html_input(urldecode(base64_decode($data['szPhoneNumberUpdate'])))));
                } 
                $this->set_szAddress2(trim(sanitize_all_html_input($data['szAddress2'])));
                $this->set_szAddress3(trim(sanitize_all_html_input($data['szAddress3'])));

                if($new_page_flag)
                {
                    $this->szPostcode = trim(sanitize_all_html_input($data['szPostCode'])) ;
                }
                else
                { 
                    if(!empty($data['szCity']) && (empty($data['szPostCode'])) && ($data['szCountry']>0))
                    {
                        /*			
                            $fromCityAry = $this->check_city_country($data['szCity'],$data['szCountry']);
                            if(empty($fromCityAry))
                            {
                                    $error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
                                    $this->addError('szOriginCity3',$error_messg);
                            }
                            else if($this->emptyPostcodeCtr > 0 && !empty($fromCityAry))
                            {
                                    $this->szPostcode = '';
                            }
                         */
                    }
                    else
                    {
                        $this->set_szPostcode(trim(sanitize_all_html_input($data['szPostCode'])));
                    }	
                } 
                $this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szEmail']))));
                $this->set_idGroup(trim(sanitize_all_html_input(strtolower($data['idGroup']))));

                if ($this->error == true)
                {
                    return false;
                }
                if(!$new_page_flag)
                {
                    /*
                    if($this->isCheckCityPostCode($this->szCountry,$this->szCity,$this->szPostcode))
                    {
                        $this->addError( "szPostcode" , t($this->t_base.'messages/added_postcode_does_not_match') );
                    }
                    */
                }
			
                //exit if error exist.
                if ($this->error == true)
                { 
                    return false;
                }
			
                if(!empty($this->szPhoneNumber))
                {
                    $this->szPhoneNumber = $this->szPhoneNumber ;	
                } 
                $query="
                    INSERT INTO
                        ".__DBC_SCHEMATA_REGISTER_SHIPPER_CONSIGNEES__."
                    (
                        idCustomer,
                        idShipConGroup,
                        szCompanyName,
                        szFirstName,
                        szLastName,
                        szAddress,
                        szAddress2,
                        szAddress3,
                        szCity,
                        szState,
                        idCountry,
                        szPhone,
                        iInternationalDialCode,
                        szEmail,
                        szPostCode,
                        iActive,
                        dtCreatedOn
                    )
                    VALUES
	            (
	            	'".(int)$this->id."',
	            	'".(int)$this->idGroup."',
	            	'".mysql_escape_custom($this->szCompanyName)."',
                        '".mysql_escape_custom(ucwords(strtolower($this->szFirstName)))."',
                        '".mysql_escape_custom(ucwords(strtolower($this->szLastName)))."',
                        '".mysql_escape_custom(ucwords(strtolower($this->szAddress1)))."',
                        '".mysql_escape_custom(ucwords(strtolower($this->szAddress2)))."',
                        '".mysql_escape_custom(ucwords(strtolower($this->szAddress3)))."',
                        '".mysql_escape_custom(ucwords(strtolower($this->szCity)))."',
                        '".mysql_escape_custom(ucwords(strtolower($this->szState)))."',
                        '".mysql_escape_custom($this->szCountry)."',
                        '".mysql_escape_custom($this->szPhoneNumber)."',
                        '".mysql_escape_custom($this->iInternationalDialCode)."', 
                        '".mysql_escape_custom($this->szEmail)."',
                        '".mysql_escape_custom($this->szPostcode)."',
                        '1',
                        NOW()
                    )
                ";
                //echo $query."<br/>";
                //echo $this->idGroup ;
                //die;
                if( ( $result = $this->exeSQL( $query ) ) )
                {
                    if($session_flag)
                    {
                        if($this->idGroup==1)
                        {
                            $_SESSION['booking_register_shipper_id'] = $this->iLastInsertID ; 
                        }
                        if($this->idGroup==2)
                        {
                            $_SESSION['booking_register_consignee_id'] = $this->iLastInsertID ; 
                        }
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
	}	
	
	function isEmailExist( $email,$id=0 )
	{
		$this->set_szEmail($email);

		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_REGISTER_SHIPPER_CONSIGNEES__."
			WHERE
				szEmail = '".mysql_escape_custom($this->szEmail)."'
		";
		if((int)$id>0)
		{
			$query .="
				AND
				    id<>'".(int)$id."'		
			";
		}
		//echo $query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
        	// Check if the user exists
            if( $this->getRowCnt() > 0 )
            {
                   return true;
            }
            else
            {
                    return false;
            }
		}
	}
	
	function getRegisteredShipperConsigness($flag,$id,$szPostCode=false,$idCountry=false,$szCity=false,$idShipperConsignee=0)
	{
		if($flag=='con')
		{
			$Sql="AND
				(
					rsc.idShipConGroup='3'
				OR
					rsc.idShipConGroup='2'
				)	
			";
		}
		else if($flag=='ship')
		{
			$Sql="AND
				(
					rsc.idShipConGroup='3'
				OR
					rsc.idShipConGroup='1'
				)	
			";
		}
		if((int)$idCountry == __ID_COUNTRY_HONG_KONG__)
		{
			if(!empty($szCity))
			{
				$Sql .=" AND szCity = '".mysql_escape_custom(trim($szCity))."'" ;
			}
		}
		else if(!empty($szPostCode))
		{
			$Sql .=" AND szPostCode = '".mysql_escape_custom(trim($szPostCode))."'" ;
		}
		if((int)$idShipperConsignee>0)
                {
                    $Sql .=" AND rsc.id = '".(int)$idShipperConsignee."'" ;
                }
		$query="
			SELECT
				rsc.id,
				rsc.idShipConGroup,
				rsc.szCompanyName,
				c.szCountryName
			FROM
				".__DBC_SCHEMATA_REGISTER_SHIPPER_CONSIGNEES__." AS rsc
			INNER JOIN
				".__DBC_SCHEMATA_COUNTRY__." AS c
			ON
				c.id=rsc.idCountry
			WHERE
				rsc.idCustomer IN (".$id.")
			
				".$Sql."
			ORDER BY
				rsc.szCompanyName ASC
			";
		//echo $query."<br/>";
                //die();
		if( ( $result = $this->exeSQL( $query ) ) )
		{
        	// Check if the user exists
            if( $this->getRowCnt() > 0 )
            {
            	$include_recently_added_shipper_consignee = true;
            	$shipper_consignee_id = array();
            	$ctr=0;
                while($row=$this->getAssoc($result))
            	{
            		$regShipCon[$ctr]=$row;
            		$shipper_consignee_id[$ctr] = $row['id'] ;
            		$ctr++;
            	}
            }
		}
		else
		{
			return array();
		}
		
        $ret_ary = array();
            	
       /*
      	* Following case only happens if a user added different shiiperPostcode and shipperPostcode_pickup at booking details.
      	*/
        if(($flag=='ship') && ($_SESSION['booking_register_shipper_id']>0))
		{
			$shipper_temp_ary = array();
			if(!empty($shipper_consignee_id) && !in_array($_SESSION['booking_register_shipper_id'],$shipper_consignee_id))
			{
				$this->getShipperConsignessDetail($_SESSION['booking_register_shipper_id']);					
				$shipper_temp_ary[0]['id'] = $_SESSION['booking_register_shipper_id'] ;
				$shipper_temp_ary[0]['idShipConGroup'] = $this->idGroup;
				$shipper_temp_ary[0]['szCompanyName'] = $this->szCompanyName;
				$shipper_temp_ary[0]['szCountryName'] = $this->idCountry;
				$ret_ary = array_merge($regShipCon,$shipper_temp_ary);
			}
			else if(empty($shipper_consignee_id))
			{
				$this->getShipperConsignessDetail($_SESSION['booking_register_shipper_id']);					
				$shipper_temp_ary[0]['id'] = $_SESSION['booking_register_shipper_id'] ;
				$shipper_temp_ary[0]['idShipConGroup'] = $this->idGroup;
				$shipper_temp_ary[0]['szCompanyName'] = $this->szCompanyName;
				$shipper_temp_ary[0]['szCountryName'] = $this->idCountry;						
				$ret_ary = $shipper_temp_ary;
			}
			else
			{
				$ret_ary = $regShipCon;	
			}					
		}
		else if(($flag=='con') && ($_SESSION['booking_register_consignee_id']>0))
		{
			$shipper_temp_ary = array();
			if(!empty($shipper_consignee_id) && !in_array($_SESSION['booking_register_consignee_id'],$shipper_consignee_id))
			{
				$this->getShipperConsignessDetail($_SESSION['booking_register_consignee_id']);					
				$shipper_temp_ary[0]['id'] = $_SESSION['booking_register_consignee_id'] ;
				$shipper_temp_ary[0]['idShipConGroup'] = $this->idGroup;
				$shipper_temp_ary[0]['szCompanyName'] = $this->szCompanyName;
				$shipper_temp_ary[0]['szCountryName'] = $this->idCountry;
				$ret_ary = array_merge($regShipCon,$shipper_temp_ary);
			}
			else if(empty($shipper_consignee_id))
			{
				$this->getShipperConsignessDetail($_SESSION['booking_register_consignee_id']);					
				$shipper_temp_ary[0]['id'] = $_SESSION['booking_register_consignee_id'] ;
				$shipper_temp_ary[0]['idShipConGroup'] = $this->idGroup;
				$shipper_temp_ary[0]['szCompanyName'] = $this->szCompanyName;
				$shipper_temp_ary[0]['szCountryName'] = $this->idCountry;						
				$ret_ary = $shipper_temp_ary;
			}
			else
			{
				$ret_ary = $regShipCon;	
			}					
		}
		else
		{
			$ret_ary = $regShipCon ;
		}				
            return $ret_ary;
	}
	
	function getRegisteredShipperConsigness_bookingDetails($flag,$id,$szPostCode=false,$idCountry=false,$szCity=false,$szCompanyName=false,$ret_flag=false)
	{
            if($flag=='con')
            {
                $Sql=" AND
                    (
                        rsc.idShipConGroup='3'
                    OR
                        rsc.idShipConGroup='2'
                    )	
                ";
            }
            else if($flag=='ship')
            {
                $Sql=" AND
                    (
                        rsc.idShipConGroup='3'
                    OR
                        rsc.idShipConGroup='1'
                    )	
                ";
            } 
            if(!empty($szPostCode))
            {
                $Sql .=" AND szPostCode = '".mysql_escape_custom(trim($szPostCode))."'" ;
            }
            else if(!empty($szCity))
            {
                $Sql .=" AND szCity = '".mysql_escape_custom(trim($szCity))."'" ;
            }

            if($idCountry>0)
            {
                $Sql .=" AND idCountry = '".(int)mysql_escape_custom(trim($idCountry))."'" ;
            }
            if(!empty($id))
            {
                $Sql .= " AND rsc.idCustomer IN (".$id.") ";
            }
            if(!empty($szCompanyName))
            {
                $Sql .=" AND LOWER(szCompanyName) LIKE '%".mysql_escape_custom(trim($szCompanyName))."%'" ;
            }

            $query="
                SELECT
                    rsc.id,
                    rsc.idShipConGroup,
                    rsc.szCompanyName,
                    c.szCountryName
                FROM
                    ".__DBC_SCHEMATA_REGISTER_SHIPPER_CONSIGNEES__." AS rsc
                INNER JOIN
                    ".__DBC_SCHEMATA_COUNTRY__." AS c
                ON
                    c.id=rsc.idCountry
                WHERE
                    rsc.iActive = '1'
                    ".$Sql."
                ORDER BY
                    rsc.szCompanyName ASC
            ";
            //echo $query."<br/>";
            if( ( $result = $this->exeSQL( $query ) ) )
            {
        	// Check if the user exists
                if( $this->getRowCnt() > 0 )
                {
                    $include_recently_added_shipper_consignee = true;
                    $shipper_consignee_id = array();
                    $ctr=0;
                    while($row=$this->getAssoc($result))
                    {
                        $regShipCon[$ctr]=$row;
                        $shipper_consignee_id[$ctr] = $row['id'] ;
                        $ctr++;
                    }
                }
            }
            else
            {
                    //return array();
            }
            if($ret_flag)
            {
                    return $regShipCon; 
            }
		/*
		* adding user's account details into shipper consignee drop downs
		*/
		if((int)$_SESSION['user_id']>0)
		{
                    $kUser=new cUser();
                    $kUser->getUserDetails($_SESSION['user_id']);
                    if((int)$kUser->iIncompleteProfile==0)
                    {
                        if(!empty($szPostCode) && (trim($kUser->szPostcode) == trim($szPostCode)))
                        {
                            $user_temp_ary = array();
                            $user_temp_ary[0]['id'] = $_SESSION['user_id']."_idUser";
                            $user_temp_ary[0]['idShipConGroup'] = $kUser->idGroup;
                            $user_temp_ary[0]['szCompanyName'] = $kUser->szCompanyName;
                            $user_temp_ary[0]['szCountryName'] = $kUser->szCountry;
                            if(!empty($regShipCon))
                            {
                                $regShipCon = array_merge($user_temp_ary,$regShipCon);
                            }
                            else
                            {
                                $regShipCon = $user_temp_ary ;
                            }
                        }
                        else if(!empty($szCity) && (trim($kUser->szCity) == trim($szCity)))
                        {
                            $user_temp_ary = array();
                            $user_temp_ary[0]['id'] = $_SESSION['user_id']."_idUser";
                            $user_temp_ary[0]['idShipConGroup'] = $kUser->idGroup;
                            $user_temp_ary[0]['szCompanyName'] = $kUser->szCompanyName;
                            $user_temp_ary[0]['szCountryName'] = $kUser->szCountry;
                            if(!empty($regShipCon))
                            {
                                $regShipCon = array_merge($user_temp_ary,$regShipCon);
                            }
                            else
                            {
                                $regShipCon = $user_temp_ary ;
                            }
                        }
                        else if(empty($szPostCode) && empty($szCity))
                        {					
                            $user_temp_ary = array();
                            $user_temp_ary[0]['id'] = $_SESSION['user_id']."_idUser";
                            $user_temp_ary[0]['idShipConGroup'] = $kUser->idGroup;
                            $user_temp_ary[0]['szCompanyName'] = $kUser->szCompanyName;
                            $user_temp_ary[0]['szCountryName'] = $kUser->szCountry;
                            if(!empty($regShipCon))
                            {
                                $regShipCon = array_merge($user_temp_ary,$regShipCon);
                            }
                            else
                            {
                                $regShipCon = $user_temp_ary ;
                            }
                        }
                    }
		}
        $ret_ary = array();
        $ret_ary =  $regShipCon ;   	
       /*
      	* Following case only happens if a user added different shiiperPostcode and shipperPostcode_pickup at booking details.
      	*/
        if(($flag=='ship') && ($_SESSION['booking_register_shipper_id']>0))
        {
            $shipper_temp_ary = array();
            if(!empty($shipper_consignee_id) && !in_array($_SESSION['booking_register_shipper_id'],$shipper_consignee_id))
            {
                $this->getShipperConsignessDetail($_SESSION['booking_register_shipper_id']);					
                $shipper_temp_ary[0]['id'] = $_SESSION['booking_register_shipper_id'] ;
                $shipper_temp_ary[0]['idShipConGroup'] = $this->idGroup;
                $shipper_temp_ary[0]['szCompanyName'] = $this->szCompanyName;
                $shipper_temp_ary[0]['szCountryName'] = $this->idCountry;
                $ret_ary = array_merge($regShipCon,$shipper_temp_ary);
            }
            else if(empty($shipper_consignee_id))
            {
                $this->getShipperConsignessDetail($_SESSION['booking_register_shipper_id']);					
                $shipper_temp_ary[0]['id'] = $_SESSION['booking_register_shipper_id'] ;
                $shipper_temp_ary[0]['idShipConGroup'] = $this->idGroup;
                $shipper_temp_ary[0]['szCompanyName'] = $this->szCompanyName;
                $shipper_temp_ary[0]['szCountryName'] = $this->idCountry;						
                $ret_ary = $shipper_temp_ary;
            }
            else
            {
                $ret_ary = $regShipCon;	
            }					
        }
        else if(($flag=='con') && ($_SESSION['booking_register_consignee_id']>0))
        {
            $shipper_temp_ary = array();
            if(!empty($shipper_consignee_id) && !in_array($_SESSION['booking_register_consignee_id'],$shipper_consignee_id))
            {
                $this->getShipperConsignessDetail($_SESSION['booking_register_consignee_id']);					
                $shipper_temp_ary[0]['id'] = $_SESSION['booking_register_consignee_id'] ;
                $shipper_temp_ary[0]['idShipConGroup'] = $this->idGroup;
                $shipper_temp_ary[0]['szCompanyName'] = $this->szCompanyName;
                $shipper_temp_ary[0]['szCountryName'] = $this->idCountry;
                $ret_ary = array_merge($regShipCon,$shipper_temp_ary);
            }
            else if(empty($shipper_consignee_id))
            {
                $this->getShipperConsignessDetail($_SESSION['booking_register_consignee_id']);					
                $shipper_temp_ary[0]['id'] = $_SESSION['booking_register_consignee_id'] ;
                $shipper_temp_ary[0]['idShipConGroup'] = $this->idGroup;
                $shipper_temp_ary[0]['szCompanyName'] = $this->szCompanyName;
                $shipper_temp_ary[0]['szCountryName'] = $this->idCountry;						
                $ret_ary = $shipper_temp_ary;
            }
            else
            {
                $ret_ary = $regShipCon;	
            }					
        }
        else
        {
            $ret_ary = $regShipCon ;
        }		
        $final_ary = sortArray($ret_ary,'szCompanyName'); 
        return $final_ary;
    }
	
	function getShipperConsignessDetail($idShipperConsign=false,$szCompanyName=false,$idCustomer=false)
	{
            if((int)$idShipperConsign>0 || !empty($szCompanyName))
            {
                if($idShipperConsign>0)
                {
                    $query_and = " id='".(int)$idShipperConsign."' ";
                }
                else if(!empty($szCompanyName))
                {
                    $query_and = " szCompanyName = '".mysql_escape_custom(trim($szCompanyName))."' ";
                }
                else
                {
                    return false;
                }

                if($idCustomer>0)
                {
                    $query_and .= " AND idCustomer = '".mysql_escape_custom(trim($idCustomer))."' ";
                }

                $query="
                    SELECT
                        id,
                        idCustomer,
                        idShipConGroup,
                        szCompanyName,
                        szFirstName,
                        szLastName,
                        szAddress,
                        szAddress2,
                        szAddress3,
                        szCity,
                        szState,
                        idCountry,
                        szPhone,
                        iInternationalDialCode,
                        szEmail,
                        szPostCode,
                        iActive,
                        dtCreatedOn
                    FROM
                        ".__DBC_SCHEMATA_REGISTER_SHIPPER_CONSIGNEES__."
                    WHERE
                        $query_and
                ";
                //echo $query ;
                if(($result = $this->exeSQL($query)))
                {
                    // Check if the user exists
	            if( $this->getRowCnt() > 0 )
	            {
	            	$row=$this->getAssoc($result);
	            	
	            	$this->szEmail=$row['szEmail'];
	            	$this->id=$row['id'];
	            	$this->idGroup=$row['idShipConGroup'];
	            	$this->szFirstName=$row['szFirstName'];
	            	$this->szLastName=$row['szLastName'];
	            	$this->szCompanyName=$row['szCompanyName'];
	            	$this->szAddress1=$row['szAddress'];
	            	$this->szAddress2=$row['szAddress2'];
	            	$this->szAddress3=$row['szAddress3'];
	            	$this->szCity=$row['szCity'];
	            	$this->szState=$row['szState'];
	            	$this->szCountry=$row['idCountry'];
	            	$this->szPhoneNumber=$row['szPhone'];
                        $this->iInternationalDialCode=$row['iInternationalDialCode']; 
	            	$this->szPostcode=$row['szPostCode'];
	            	$this->iActive=$row['iActive'];
	            	$this->dtCreateOn=$row['dtCreateOn']; 
	            	return $row;
	            }
	            else
	            {
	                 return array();
	            }
                }
                else
                {
                    return array();
                }
            }
            else
            {
                return array();
            }
	}
	
	function updateRegisterShipperConsigness($data,$iNewPage=false)
	{  
            if(is_array($data))
            {
                $this->set_szCompanyName(trim(sanitize_all_html_input($data['szCompanyName'])));
                $this->set_szFirstName(trim(sanitize_all_html_input($data['szFirstName'])));
                $this->set_szLastName(trim(sanitize_all_html_input($data['szLastName'])));
                $this->set_szEmail(trim(sanitize_all_html_input(strtolower($data['szEmail']))));	 
                $this->set_szCountry(trim(sanitize_all_html_input($data['szCountry'])));
                $this->set_iInternationalDialCode(trim(sanitize_all_html_input($data['iInternationalDialCode']))); 

                if($iNewPage)
                {
                    $this->set_szCity(trim(sanitize_all_html_input($data['szCity'])),false);
                    $this->set_szState(trim(sanitize_all_html_input($data['szProvince'])),false);
                    $this->set_szAddress1(trim(sanitize_all_html_input($data['szAddress1'])),false);
                    $this->set_szPhoneNumber(trim(sanitize_all_html_input($data['szPhoneNumberUpdate'])));
                }
                else
                {
                    $this->set_szCity(trim(sanitize_all_html_input($data['szCity'])));
                    $this->set_szState(trim(sanitize_all_html_input($data['szProvince'])));
                    $this->set_szAddress1(trim(sanitize_all_html_input($data['szAddress1'])));
                    $this->set_szPhoneNumber(trim(sanitize_all_html_input(urldecode(base64_decode($data['szPhoneNumberUpdate'])))));
                } 
                $this->set_szAddress2(trim(sanitize_all_html_input($data['szAddress2'])));
                $this->set_szAddress3(trim(sanitize_all_html_input($data['szAddress3'])));

                if(!$iNewPage)
                {
                    if(!empty($data['szCity']) && (empty($data['szPostCode'])) && ($data['szCountry']>0))
                    {			/*
                            $fromCityAry = $this->check_city_country($data['szCity'],$data['szCountry']);
                            if(empty($fromCityAry))
                            {
                                    $error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
                                    $this->addError('szOriginCity3',$error_messg);
                            }
                            else if($this->emptyPostcodeCtr > 0 && !empty($fromCityAry))
                            {
                                    $this->szPostcode = '';
                            }*/
                    }
                    else
                    {
                            $this->set_szPostcode(trim(sanitize_all_html_input($data['szPostCode'])));
                    }
                }
                else
                {
                    $this->szPostCode = trim(sanitize_all_html_input($data['szPostCode'])) ;
                }

                $this->set_id($data['idShipperConsigness']);
                $this->set_idGroup(trim(sanitize_all_html_input(strtolower($data['idGroup']))));

                if ($this->error == true)
                {
                        return false;
                } 

                if(!$iNewPage)
                {/*
                    if($this->isCheckCityPostCode($this->szCountry,$this->szCity,$this->szPostcode))
                    {
                        $this->addError( "szPostcode" , t($this->t_base.'messages/added_postcode_does_not_match') );
                        return false;
                    }*/
                }

                //exit if error exist.
                if ($this->error == true)
                {
                        return false;
                } 
                if(!empty($this->szPhoneNumber))
                {
                    $this->szPhoneNumber = $this->szPhoneNumber;	
                } 
            	$query="
                    UPDATE
                        ".__DBC_SCHEMATA_REGISTER_SHIPPER_CONSIGNEES__."
                    SET
                        szEmail='".mysql_escape_custom($this->szEmail)."',
                        szCompanyName='".mysql_escape_custom(ucwords(strtolower($this->szCompanyName)))."',
                        szFirstName='".mysql_escape_custom(ucwords(strtolower($this->szFirstName)))."',
                        szLastName='".mysql_escape_custom(ucwords(strtolower($this->szLastName)))."',
                        szAddress='".mysql_escape_custom(ucwords(strtolower($this->szAddress1)))."',
                        szAddress2='".mysql_escape_custom(ucwords(strtolower($this->szAddress2)))."',
                        szAddress3='".mysql_escape_custom(ucwords(strtolower($this->szAddress3)))."',
                        szCity='".mysql_escape_custom(ucwords(strtolower($this->szCity)))."',
                        szState='".mysql_escape_custom(ucwords(strtolower($this->szState)))."',
                        idCountry='".mysql_escape_custom($this->szCountry)."',
                        szPhone='".mysql_escape_custom($this->szPhoneNumber)."',
                        iInternationalDialCode = '".mysql_escape_custom($this->iInternationalDialCode)."',
                        szPostCode='".mysql_escape_custom($this->szPostCode)."',
                        idShipConGroup='".mysql_escape_custom($this->idGroup)."'
                    WHERE
                        id='".(int)$this->id."'
            	";       
              //  echo $query;
//                die;
                
            	if($result = $this->exeSQL( $query ))
                {
                    return true;
                }
                else
                {
                    return false;
                } 
            }
            else
            {
                return false;
            }
	}
	
	function removeRegShipperConsigneess($data)
	{
		if(!empty($data))
		{
			$query="
				DELETE FROM
					".__DBC_SCHEMATA_REGISTER_SHIPPER_CONSIGNEES__."
				WHERE
					id IN (".$data.")	
			";
			//echo $query;
			$result = $this->exeSQL( $query );
			return true;
			
		}
		else
		{
			return false;
		}
	}
	
	function isCheckCityPostCode($idCounty,$szCity='',$szPostCode='')
	{
		$query_and="";
		if(!empty($szCity))
		{
			$query_and ="
			AND
				(
					szCity='".mysql_escape_custom(trim($szCity))."'
				OR
					szRegion1='".mysql_escape_custom(trim($szCity))."'	
				OR
					szRegion2='".mysql_escape_custom(trim($szCity))."'	
				OR
					szRegion3='".mysql_escape_custom(trim($szCity))."'	
				OR
					szRegion4='".mysql_escape_custom(trim($szCity))."'
				)
			";
		}		
		if(!empty($szPostCode))
		{
			$query_and .="
			AND
				szPostCode='".mysql_escape_custom(trim($szPostCode))."'
			";
		}
		$query="
			SELECT
				id
			FROM
				".__DBC_SCHEMATA_POSTCODE__."
			WHERE
				idCountry='".(int)$idCounty."'
			".$query_and."
		";
		//echo $query;
		if( ( $result = $this->exeSQL( $query ) ) )
		{
	        
	    	if( $this->getRowCnt() > 0 )
	        {
	            return false;
	        }
	        else
	        {
	        	return true;
	        }
		}
	}
	
	function addShipperConsignee($data,$idBooking,$from_page=false)
	{
		if(!empty($data) && (int)$idBooking>0)
		{
			if(empty($data['szShipperCity']))
			{
				$data['szShipperCity'] = $data['szShipperCity_hidden'];
			}
			if(empty($data['szShipperPostcode']))
			{
				$data['szShipperPostcode'] = $data['szShipperPostcode_hidden'];
			}
			if(empty($data['idShipperCountry']))
			{
				$data['idShipperCountry'] = $data['idShipperCountry_hidden'];
			}
			
			if(empty($data['szConsigneeCity']))
			{
				$data['szConsigneeCity'] = $data['szConsigneeCity_hidden'];
			}
			if(empty($data['szConsigneePostcode']))
			{
				$data['szConsigneePostcode'] = $data['szConsigneePostcode_hidden'];
			}
			if(empty($data['idConsigneeCountry']))
			{
				$data['idConsigneeCountry'] = $data['idConsigneeCountry_hidden'];
			}
			
			if($data['szShipperCity']==t($this->t_base.'fields/type_name'))
			{
				$data['szShipperCity']='';
			}
			if($data['szShipperCity_pickup']==t($this->t_base.'fields/type_name'))
			{
				$data['szShipperCity_pickup']='';
			}
			if($data['szConsigneeCity']==t($this->t_base.'fields/type_name'))
			{
				$data['szConsigneeCity']='';
			}
			if($data['szConsigneeCity_pickup']==t($this->t_base.'fields/type_name'))
			{
				$data['szConsigneeCity_pickup']='';
			}
			
			if(($data['szShipperPostcode']==t($this->t_base.'fields/optional')) || ($data['szShipperPostcode']==t($this->t_base.'fields/type_code')))
			{
				$data['szShipperPostcode']='';
			}
			if(($data['szShipperPostcode_pickup']==t($this->t_base.'fields/optional')) || ($data['szShipperPostcode_pickup']==t($this->t_base.'fields/type_code')))
			{
				$data['szShipperPostcode_pickup']='';
			}
			
		    if(($data['szConsigneePostcode']==t($this->t_base.'fields/optional')) || ($data['szConsigneePostcode']==t($this->t_base.'fields/type_code')))
			{
				$data['szConsigneePostcode']='';
			}
			if(($data['szConsigneePostcode_pickup']==t($this->t_base.'fields/optional')) || ($data['szConsigneePostcode_pickup']==t($this->t_base.'fields/type_code')))
			{
				$data['szConsigneePostcode_pickup']='';
			}
			
			$kBooking = new cBooking();
			$postSearchAry = array();
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			
			
			$this->set_idServiceType(trim(sanitize_all_html_input($data['idServiceType'])));			
			$this->set_szShipperCompanyName(trim(sanitize_all_html_input($data['szShipperCompanyName'])));
			$this->set_szShipperFirstName(trim(sanitize_all_html_input($data['szShipperFirstName'])));
			$this->set_szShipperLastName(trim(sanitize_all_html_input($data['szShipperLastName'])));
			$this->set_szShipperEmail(trim(sanitize_all_html_input($data['szShipperEmail'])));				
			$this->set_szShipperPhone(trim(sanitize_all_html_input(urldecode(base64_decode($data['szShipperPhoneNumberUpdate'])))));	
			$this->set_idShipperCountry(trim(sanitize_all_html_input($data['idShipperCountry'])));
			
			if($this->idServiceType==__SERVICE_TYPE_DTD__ || $this->idServiceType==__SERVICE_TYPE_DTW__ || $this->idServiceType==__SERVICE_TYPE_DTP__) // 1-DTD, 2-DTW
			{
				$required_shipper_flag = true;
				if((int)$data['idShipperCountry']==__ID_COUNTRY_HONG_KONG__)
				{
					$this->szShipperPostcode = $data['szShipperPostcode'] ;
				}
				else
				{
					if(!empty($postSearchAry['szOriginPostCode']))
					{
						$required_shipper_flag = true;
					}
					else
					{
						$required_shipper_flag = false;
					}				
					$this->set_szShipperPostcode(trim(sanitize_all_html_input($data['szShipperPostcode'])),$required_shipper_flag);
				}				
				$this->set_szShipperCity(trim(sanitize_all_html_input($data['szShipperCity'])),!$required_shipper_flag);			
			}
			else
			{
				$this->szShipperPostcode = $data['szShipperPostcode'] ;		
				$this->set_szShipperCity(trim(sanitize_all_html_input($data['szShipperCity'])));
			}
			
			$this->set_szShipperAddress(trim(sanitize_all_html_input($data['szShipperAddress'])));
			$this->set_szShipperAddress2(trim(sanitize_all_html_input($data['szShipperAddress2'])));
			$this->set_szShipperAddress3(trim(sanitize_all_html_input($data['szShipperAddress3'])));
			$this->set_szShipperState(trim(sanitize_all_html_input($data['szShipperState'])));
			
			$this->set_szConsigneeCompanyName(trim(sanitize_all_html_input($data['szConsigneeCompanyName'])));
			$this->set_szConsigneeFirstName(trim(sanitize_all_html_input($data['szConsigneeFirstName'])));
			$this->set_szConsigneeLastName(trim(sanitize_all_html_input($data['szConsigneeLastName'])));
			$this->set_szConsigneeEmail(trim(sanitize_all_html_input($data['szConsigneeEmail'])));
			
			$this->set_szConsigneePhone(trim(sanitize_all_html_input(urldecode(base64_decode($data['szConsigneePhoneNumberUpdate'])))));	
			$this->set_idConsigneeCountry(trim(sanitize_all_html_input($data['idConsigneeCountry'])));			
			
			if($this->idServiceType==__SERVICE_TYPE_DTD__ || $this->idServiceType==__SERVICE_TYPE_WTD__ || $this->idServiceType==__SERVICE_TYPE_PTD__) // 1-DTD, 3-WTD
			{
				if((int)$data['idConsigneeCountry']==__ID_COUNTRY_HONG_KONG__)
				{
					$this->szConsigneePostcode = $data['szConsigneePostcode'] ;
				}
				else
				{
					if(!empty($postSearchAry['szDestinationPostCode']))
					{
						$required_consignee_flag = true;
					}
					else
					{
						$required_consignee_flag = false;
					}	
					$this->set_szConsigneePostcode(trim(sanitize_all_html_input($data['szConsigneePostcode'])),$required_consignee_flag);
				}
				$this->set_szConsigneeCity(trim(sanitize_all_html_input($data['szConsigneeCity'])));			
			}
			else
			{
				$this->szConsigneePostcode = $data['szConsigneePostcode'] ;	
				$this->set_szConsigneeCity(trim(sanitize_all_html_input($data['szConsigneeCity'])));
			}	
			$this->set_szConsigneeAddress(trim(sanitize_all_html_input($data['szConsigneeAddress'])));
			$this->set_szConsigneeAddress2(trim(sanitize_all_html_input($data['szConsigneeAddress2'])));
			$this->set_szConsigneeAddress3(trim(sanitize_all_html_input($data['szConsigneeAddress3'])));
			$this->set_szConsigneeState(trim(sanitize_all_html_input($data['szConsigneeState'])));
			
			if((int)$data['iPickupAddress_hidden']==1 || $this->idServiceType==__SERVICE_TYPE_WTD__ || $this->idServiceType==__SERVICE_TYPE_PTD__ || $this->idServiceType==__SERVICE_TYPE_WTW__ || $this->idServiceType==__SERVICE_TYPE_WTP__ || $this->idServiceType==__SERVICE_TYPE_PTW__ || $this->idServiceType==__SERVICE_TYPE_PTP__)
			{
				$this->idShipperCountry_pickup = $this->idShipperCountry ;
				$this->szShipperCity_pickup = $this->szShipperCity ;
				$this->szShipperPostcode_pickup = $this->szShipperPostcode ;				
				$this->szShipperAddress_pickup = $this->szShipperAddress ;
				$this->szShipperAddress2_pickup = $this->szShipperAddress2;
				$this->szShipperAddress3_pickup = $this->szShipperAddress3 ;
				$this->szShipperState_pickup = $this->szShipperState ;
			}
			else
			{
				$required_shipper_flag=true;
				$this->set_idShipperCountry_pickup(trim(sanitize_all_html_input($data['idShipperCountry_pickup'])));
				
				if((int)$data['idShipperCountry_pickup']==__ID_COUNTRY_HONG_KONG__)
				{
					$this->szShipperPostcode_pickup = $data['szShipperPostcode_pickup'] ;
				}
				else
				{					
					if(!empty($postSearchAry['szOriginPostCode']))
					{
						$required_shipper_flag = true;
					}
					else
					{
						$required_shipper_flag = false;
					}
					$this->set_szShipperPostcode_pickup(trim(sanitize_all_html_input($data['szShipperPostcode_pickup'])),$required_shipper_flag);
				}								
				$this->set_szShipperCity_pickup(trim(sanitize_all_html_input($data['szShipperCity_pickup'])),!$required_shipper_flag);
				$this->set_szShipperAddress_pickup(trim(sanitize_all_html_input($data['szShipperAddress_pickup'])));
				$this->set_szShipperAddress2_pickup(trim(sanitize_all_html_input($data['szShipperAddress2_pickup'])));
				$this->set_szShipperAddress3_pickup(trim(sanitize_all_html_input($data['szShipperAddress3_pickup'])));
				$this->set_szShipperState_pickup(trim(sanitize_all_html_input($data['szShipperState_pickup'])));
			}
			if((int)$data['iDeliveryAddress_hidden']==1 || $this->idServiceType==__SERVICE_TYPE_DTW__ || $this->idServiceType==__SERVICE_TYPE_WTW__ || $this->idServiceType==__SERVICE_TYPE_DTP__ || $this->idServiceType==__SERVICE_TYPE_PTP__ || $this->idServiceType==__SERVICE_TYPE_WTP__ || $this->idServiceType==__SERVICE_TYPE_PTW__)
			{
				$this->idConsigneeCountry_pickup = $this->idConsigneeCountry ;
				$this->szConsigneeCity_pickup = $this->szConsigneeCity ;
				$this->szConsigneePostcode_pickup = $this->szConsigneePostcode ;				
				$this->szConsigneeAddress_pickup = $this->szConsigneeAddress ;
				$this->szConsigneeAddress2_pickup = $this->szConsigneeAddress2;
				$this->szConsigneeAddress3_pickup = $this->szConsigneeAddress3 ;
				$this->szConsigneeState_pickup = $this->szConsigneeState ;
			}
			else
			{
				$this->set_idConsigneeCountry_pickup(trim(sanitize_all_html_input($data['idConsigneeCountry_pickup'])));
				
				if((int)$data['idConsigneeCountry_pickup']==__ID_COUNTRY_HONG_KONG__)
				{
					$this->szConsigneePostcode_pickup = $data['szConsigneePostcode_pickup'] ;
				}
				else
				{
					if(!empty($postSearchAry['szDestinationPostCode']))
					{
						$required_consignee_flag = true;
					}
					else
					{
						$required_consignee_flag = false;
					}
					$this->set_szConsigneePostcode_pickup(trim(sanitize_all_html_input($data['szConsigneePostcode_pickup'])),$required_consignee_flag);
				}	
				$this->set_szConsigneeCity_pickup(trim(sanitize_all_html_input($data['szConsigneeCity_pickup'])),!$required_consignee_flag);			
				$this->set_szConsigneeAddress_pickup(trim(sanitize_all_html_input($data['szConsigneeAddress_pickup'])));
				$this->set_szConsigneeAddress2_pickup(trim(sanitize_all_html_input($data['szConsigneeAddress2_pickup'])));
				$this->set_szConsigneeAddress3_pickup(trim(sanitize_all_html_input($data['szConsigneeAddress3_pickup'])));
				$this->set_szConsigneeState_pickup(trim(sanitize_all_html_input($data['szConsigneeState_pickup'])));
			}
			
			$CommodityCtr = count($data['szCargoCommodity']);
			$kForwarder=new cForwarder();
			$nonAcceptedGoodsAry = array();
			for($i=0;$i<$CommodityCtr;$i++)
			{
				$this->set_szCargoCommodity($data['szCargoCommodity'][$i],$i);
				if(!empty($data['szCargoCommodity'][$i]))
				{
					if($kForwarder->checkNonAcceptanceOfForwarder($data['szCargoCommodity'][$i],$data['idForwarder']))
					{
						$nonAcceptedGoodsAry[] = $data['szCargoCommodity'][$i];
					}
				}
			}
			if(!empty($nonAcceptedGoodsAry))
			{
				$this->addError('iNonAcceptedGoods',implode(',',$nonAcceptedGoodsAry).' is non accepted goods for '.$data['szForwarderDispName']);
				return false;		
			}
			if($this->error==true)
			{
				return false;				
			}			
			$this->validate_city_country_postcode_by_service_type($this);
			
			if(!empty($this->szShipperPhone))
			{
				$shipper_str = substr($this->szShipperPhone, 0, 1); 
				$shipper_substr=substr($this->szShipperPhone,1);
				$phone=str_replace("+"," ",$shipper_substr);
				
				$this->szShipperPhone = $this->szShipperPhone;	
			}
			if(!empty($this->szConsigneePhone))
			{
				$this->szConsigneePhone = $this->szConsigneePhone;
			}
			if($this->error==true)
			{
				return false;				
			}
			
			if(empty($this->szShipperCity))
			{
				$this->szShipperCity = $this->szOriginCity ;
			}
			if(empty($this->szConsigneeCity))
			{
				$this->szConsigneeCity = $this->szDestinationCity ;
			}
			
			if($this->isShipperConsigneeExist($idBooking))
			{
				$query = "
					UPDATE
						".__DBC_SCHEMATA_SHIPPER_CONSIGNEE__."
					SET
						szShipperCompanyName = '".mysql_escape_custom(trim($this->szShipperCompanyName))."',
						szShipperFirstName = '".mysql_escape_custom(trim($this->szShipperFirstName))."',
						szShipperLastName = '".mysql_escape_custom(trim($this->szShipperLastName))."',
						szShipperAddress = '".mysql_escape_custom(trim($this->szShipperAddress))."',
						szShipperAddress2 = '".mysql_escape_custom(trim($this->szShipperAddress2))."',
						szShipperAddress3 = '".mysql_escape_custom(trim($this->szShipperAddress3))."',
						szShipperPostCode = '".mysql_escape_custom(trim($this->szShipperPostcode))."',
						szShipperCity = '".mysql_escape_custom(trim($this->szShipperCity))."',
						szShipperState = '".mysql_escape_custom(trim($this->szShipperState))."',
						szShipperPhone = '".mysql_escape_custom(trim($this->szShipperPhone))."',
						szShipperEmail = '".mysql_escape_custom(trim($this->szShipperEmail))."',
						idShipperCountry = '".(int)$this->idShipperCountry."',				
						szShipperAddress_pickup = '".mysql_escape_custom(trim($this->szShipperAddress_pickup))."',
						szShipperAddress2_pickup = '".mysql_escape_custom(trim($this->szShipperAddress2_pickup))."',
						szShipperAddress3_pickup = '".mysql_escape_custom(trim($this->szShipperAddress3_pickup))."',
						szShipperPostCode_pickup = '".mysql_escape_custom(trim($this->szShipperPostcode_pickup))."',
						szShipperCity_pickup = '".mysql_escape_custom(trim($this->szShipperCity_pickup))."',
						szShipperState_pickup = '".mysql_escape_custom(trim($this->szShipperState_pickup))."',
						idShipperCountry_pickup = '".(int)$this->idShipperCountry_pickup."',
						idShipperGroup = '1',							
						szConsigneeCompanyName = '".mysql_escape_custom(trim($this->szConsigneeCompanyName))."',
						szConsigneeFirstName = '".mysql_escape_custom(trim($this->szConsigneeFirstName))."',
						szConsigneeLastName = '".mysql_escape_custom(trim($this->szConsigneeLastName))."',
						szConsigneeAddress = '".mysql_escape_custom(trim($this->szConsigneeAddress))."',
						szConsigneeAddress2 = '".mysql_escape_custom(trim($this->szConsigneeAddress2))."',
						szConsigneeAddress3 = '".mysql_escape_custom(trim($this->szConsigneeAddress3))."',
						szConsigneePostCode = '".mysql_escape_custom(trim($this->szConsigneePostcode))."',
						szConsigneeCity = '".mysql_escape_custom(trim($this->szConsigneeCity))."',
						szConsigneeState = '".mysql_escape_custom(trim($this->szConsigneeState))."',
						szConsigneePhone = '".mysql_escape_custom(trim($this->szConsigneePhone))."',
						szConsigneeEmail = '".mysql_escape_custom(trim($this->szConsigneeEmail))."',
						idConsigneeCountry = '".(int)$this->idConsigneeCountry."',					
						szConsigneeAddress_pickup = '".mysql_escape_custom(trim($this->szConsigneeAddress_pickup))."',
						szConsigneeAddress2_pickup = '".mysql_escape_custom(trim($this->szConsigneeAddress2_pickup))."',
						szConsigneeAddress3_pickup = '".mysql_escape_custom(trim($this->szConsigneeAddress3_pickup))."',
						szConsigneePostCode_pickup = '".mysql_escape_custom(trim($this->szConsigneePostcode_pickup))."',
						szConsigneeCity_pickup = '".mysql_escape_custom(trim($this->szConsigneeCity_pickup))."',
						szConsigneeState_pickup = '".mysql_escape_custom(trim($this->szConsigneeState_pickup))."',
						idConsigneeCountry_pickup = '".(int)$this->idConsigneeCountry_pickup."',
						idConsigneeGroup = '2',	
						iPickupAddress = '".(int)$data['iPickupAddress_hidden']."',
					    iDeliveryAddress = '".(int)$data['iDeliveryAddress_hidden']."'
					WHERE
						id='".(int)$this->idShipperConsignee."'
					AND
						idBooking = '".(int)$idBooking."'	
				";
				$added_flag =false ;
			}
			else
			{
				$added_flag = true;
				$query="
				INSERT INTO
					".__DBC_SCHEMATA_SHIPPER_CONSIGNEE__."
				(
					idBooking,
					szShipperCompanyName,
					szShipperFirstName,
					szShipperLastName,
					szShipperAddress,
					szShipperAddress2,
					szShipperAddress3,
					szShipperPostCode,
					szShipperCity,
					szShipperState,
					szShipperPhone,
					szShipperEmail,
					idShipperCountry,					
					szShipperAddress_pickup,
					szShipperAddress2_pickup,
					szShipperAddress3_pickup,
					szShipperPostCode_pickup,
					szShipperCity_pickup,
					szShipperState_pickup,	
					idShipperCountry_pickup,
					idShipperGroup,								
					szConsigneeCompanyName,
					szConsigneeFirstName,
					szConsigneeLastName,
					szConsigneeAddress,
					szConsigneeAddress2,
					szConsigneeAddress3,
					szConsigneePostCode,
					szConsigneeCity,
					szConsigneeState,
					szConsigneePhone,
					szConsigneeEmail,
					idConsigneeCountry,					
					szConsigneeAddress_pickup,
					szConsigneeAddress2_pickup,
					szConsigneeAddress3_pickup,
					szConsigneePostCode_pickup,
					szConsigneeCity_pickup,
					szConsigneeState_pickup,
					idConsigneeCountry_pickup,
					idConsigneeGroup,
					iPickupAddress,
					iDeliveryAddress,					
					iActive,
					dtCreatedOn				
				)	
				VALUES
				(
					'".(int)$idBooking."',
					'".mysql_escape_custom(trim($this->szShipperCompanyName))."',
					'".mysql_escape_custom(trim($this->szShipperFirstName))."',
					'".mysql_escape_custom(trim($this->szShipperLastName))."',
					'".mysql_escape_custom(trim($this->szShipperAddress))."',
					'".mysql_escape_custom(trim($this->szShipperAddress2))."',
					'".mysql_escape_custom(trim($this->szShipperAddress3))."',
					'".mysql_escape_custom(trim($this->szShipperPostcode))."',
					'".mysql_escape_custom(trim($this->szShipperCity))."',
					'".mysql_escape_custom(trim($this->szShipperState))."',
					'".mysql_escape_custom(trim($this->szShipperPhone))."',
					'".mysql_escape_custom(trim($this->szShipperEmail))."',
					'".(int)$this->idShipperCountry."',
					'".mysql_escape_custom(trim($this->szShipperAddress_pickup))."',
					'".mysql_escape_custom(trim($this->szShipperAddress2_pickup))."',
					'".mysql_escape_custom(trim($this->szShipperAddress3_pickup))."',
					'".mysql_escape_custom(trim($this->szShipperPostcode_pickup))."',
					'".mysql_escape_custom(trim($this->szShipperCity_pickup))."',
					'".mysql_escape_custom(trim($this->szShipperState_pickup))."',
					'".(int)$this->idShipperCountry_pickup."',
					'1',
					'".mysql_escape_custom(trim($this->szConsigneeCompanyName))."',
					'".mysql_escape_custom(trim($this->szConsigneeFirstName))."',
					'".mysql_escape_custom(trim($this->szConsigneeLastName))."',
					'".mysql_escape_custom(trim($this->szConsigneeAddress))."',
					'".mysql_escape_custom(trim($this->szConsigneeAddress2))."',
					'".mysql_escape_custom(trim($this->szConsigneeAddress3))."',
					'".mysql_escape_custom(trim($this->szConsigneePostcode))."',
					'".mysql_escape_custom(trim($this->szConsigneeCity))."',
					'".mysql_escape_custom(trim($this->szConsigneeState))."',
					'".mysql_escape_custom(trim($this->szConsigneePhone))."',
					'".mysql_escape_custom(trim($this->szConsigneeEmail))."',
					'".(int)$this->idConsigneeCountry."',
					'".mysql_escape_custom(trim($this->szConsigneeAddress_pickup))."',
					'".mysql_escape_custom(trim($this->szConsigneeAddress2_pickup))."',
					'".mysql_escape_custom(trim($this->szConsigneeAddress3_pickup))."',
					'".mysql_escape_custom(trim($this->szConsigneePostcode_pickup))."',
					'".mysql_escape_custom(trim($this->szConsigneeCity_pickup))."',
					'".mysql_escape_custom(trim($this->szConsigneeState_pickup))."',
					'".(int)$this->idConsigneeCountry_pickup."',
					'2',
					'".(int)$data['iPickupAddress_hidden']."',
					'".(int)$data['iDeliveryAddress_hidden']."',
					'1',
					now()
				)
			";
			}
			//echo "<br>".$query."<br>" ;
			//die;
			if($result=$this->exeSQL($query))
			{
				$idCustomer = $_SESSION['user_id'] ;
				if($added_flag)
				{
					$idShipperConsignee = $this->iLastInsertID ;
				}
				else
				{
					$idShipperConsignee = (int)$this->idShipperConsignee ;	
				}
				$kUser=new cUser();
				$kUser->getUserDetails($idCustomer);
				
				if($data['iAddRegistShipper']==1)
				{		
					$registShipperAry = array();			
					$registShipperAry['szCompanyName'] = $this->szShipperCompanyName ;
					$registShipperAry['szFirstName'] = $this->szShipperFirstName ;
					$registShipperAry['szLastName'] = $this->szShipperLastName ;
					$registShipperAry['idCustomer'] = $idCustomer;
					$registShipperAry['szCity'] = $this->szShipperCity ;
					$registShipperAry['szProvince'] = $this->szShipperState ;
					$registShipperAry['szCountry'] = $this->idShipperCountry ;
					$registShipperAry['szPhoneNumberUpdate'] = $data['szShipperPhoneNumberUpdate'];
					$registShipperAry['szAddress1'] = $this->szShipperAddress ;
					$registShipperAry['szAddress2'] = $this->szShipperAddress2 ;
					$registShipperAry['szAddress3'] = $this->szShipperAddress3 ;
					$registShipperAry['szPostCode'] = $this->szShipperPostcode ;
					$registShipperAry['szEmail'] = $this->szShipperEmail ;
					$registShipperAry['idGroup'] = 1 ;
					if((int)$_SESSION['booking_register_shipper_id']>0)
					{
						$registShipperAry['idShipperConsigness'] = $_SESSION['booking_register_shipper_id'] ;
						$this->updateRegisterShipperConsigness($registShipperAry);
					}
					else
					{
						$this->addRegisterShipperConsigness($registShipperAry,true);
					}	
				}				
				if($data['iAddRegistConsignee']==1)
				{		
					$registShipperAry = array();			
					$registShipperAry['szCompanyName'] = $this->szConsigneeCompanyName ;
					$registShipperAry['szFirstName'] = $this->szConsigneeFirstName ;
					$registShipperAry['szLastName'] = $this->szConsigneeLastName ;
					$registShipperAry['idCustomer'] = $idCustomer;
					$registShipperAry['szCity'] = $this->szConsigneeCity ;
					$registShipperAry['szProvince'] = $this->szConsigneeState ;
					$registShipperAry['szCountry'] = $this->idConsigneeCountry ;
					$registShipperAry['szPhoneNumberUpdate'] = $data['szConsigneePhoneNumberUpdate'];
					$registShipperAry['szAddress1'] = $this->szConsigneeAddress ;
					$registShipperAry['szAddress2'] = $this->szConsigneeAddress2 ;
					$registShipperAry['szAddress3'] = $this->szConsigneeAddress3 ;
					$registShipperAry['szPostCode'] = $this->szConsigneePostcode ;
					$registShipperAry['szEmail'] = $this->szConsigneeEmail ;
					$registShipperAry['idGroup'] = 2;
					
					if((int)$_SESSION['booking_register_consignee_id']>0)
					{
						$registShipperAry['idShipperConsigness'] = $_SESSION['booking_register_consignee_id'];
						$this->updateRegisterShipperConsigness($registShipperAry);
					}
					else
					{
						$this->addRegisterShipperConsigness($registShipperAry,true);
					}					
				}
				$kBooking = new cBooking();
				$res_ary = array();				
				$res_ary['idShipperConsignee'] = $idShipperConsignee; 
				$res_ary['idUser'] = $kUser->id ;
				$res_ary['szFirstName'] = $kUser->szFirstName ;
				$res_ary['szLastName'] = $kUser->szLastName ;
				$res_ary['szEmail'] = $kUser->szEmail ;
				if((int)$postSearchAry['iBookingStep']<11)
				{
					$res_ary['iBookingStep'] = 11 ;
				}
				if(!empty($res_ary))
				{
					foreach($res_ary as $key=>$value)
					{
						$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
					}
				}
				$update_query = rtrim($update_query,",");
				$kBooking->updateDraftBooking($update_query,$idBooking) ;
				
				if(!empty($data['szCargoCommodity']))
				{
					$cargoAry=array();
					$cargoAry['szCargoCommodity'] = $data['szCargoCommodity'];
					$cargoAry['idCargo'] = $data['idCargo'];
					$this->updateCargoCommodity($cargoAry);
				}
				return true;
			}
		}
	}
	function validate_city_country_postcode_by_service_type($kObject,$from_page=false)
	{	
		$kConfig = new cConfig();		
		$this->idServiceType = $kObject->idServiceType ;
		
		$this->szOriginCity = $kObject->szShipperCity ; 
		$this->szOriginPostCode = $kObject->szShipperPostcode ; 
		$this->szOriginCountry = $kObject->idShipperCountry ;
		
		$this->szDestinationCity = $kObject->szConsigneeCity ; 
		$this->szDestinationPostCode = $kObject->szConsigneePostcode ;
		$this->szDestinationCountry = $kObject->idConsigneeCountry ; 
		
		if($this->idServiceType==__SERVICE_TYPE_DTD__)//DTD
		{
			if($from_page=='NEW_BOOKING_PAGE' && $kObject->iDonotKnowShipperPostcode==1)
			{ 
				
			}
			else
			{
				// if we have both city and post code 
				if(!empty($this->szOriginCity) && !empty($this->szOriginPostCode))
				{
					if(!$this->isCityExistsByPostCode($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode))
					{
						$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_FROM')." ".$this->szOriginCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_FROM');
						$this->addError('szOriginCity1',$error_messg);
					}																	
					else
					{
						$this->idOriginPostCode = $this->idPostCode;
					}
				}
				else if(empty($this->szOriginCity) && empty($this->szOriginPostCode)) // if we don't have any one 
				{
					$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_FROM');
					$this->addError('szOriginCity',$error_messg);
				}  
				else if(!empty($this->szOriginCity) && empty($this->szOriginPostCode)) // if we have only from city
				{
					$fromCityAry = $this->check_city_country($this->szOriginCity,$this->szOriginCountry);
					
					if(empty($fromCityAry))
					{
						$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
						$this->addError('szOriginCity3',$error_messg);
					}
					else if($this->emptyPostcodeCtr > 0 && !empty($fromCityAry))
					{
						//multi region; 
						$this->multiEmaptyPostcodeFromCityAry = $fromCityAry ;
					}
					/*
					else if($this->emptyPostcodeCtr == 1 && !empty($fromCityAry))
					{
						$this->idOriginPostCode = $this->idEmptyPostCode;
					}
					*/
					else if(empty($this->szOriginPostCode) && ($this->szOriginCountry != __ID_COUNTRY_HONG_KONG__))
					{
						$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_FROM');
						$this->addError('szOriginCity',$error_messg);
					}
					else if(count($fromCityAry)>1)
					{
						//multi region; 
						$this->multiRegionFromCityAry = $fromCityAry ;
					}																	
					else
					{
						$this->idOriginPostCode = $this->idPostCode;
					}
				}
				// if we have only post code the  we first fetch city which belongs to this country and post code then check for multiregion 
				else if(empty($this->szOriginCity) && !empty($this->szOriginPostCode))
				{
					$fromCityAry=array();
					$this->szOriginCity = $this->getAllCityByPostCode($this->szOriginCountry,$this->szOriginPostCode);
					$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode) ;
					if(!$fromCityAry)
					{
						$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
						$this->addError('szOriginCity4',$error_messg);
					}
					else if(count($fromCityAry)>1)
					{
						//multi region; 
						$this->multiRegionFromCityAry = $fromCityAry ;
					}																	
					else
					{
						$this->idOriginPostCode = $this->idPostCode;
					}
				}		
			} 
			//if we have both to city and postcode .
			if(!empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				if(!$this->isCityExistsByPostCode($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode))
				{
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_TO')." ".$this->szDestinationCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_TO');
					$this->addError('szDestinationCity1',$error_messg);
				}																	
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}	
			// if we don't have any 
			if(empty($this->szDestinationCity) && empty($this->szDestinationPostCode))
			{
				$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_TO');
				$this->addError('szDestinationCity4',$error_messg);
			}
							
			// if we have onlyc city 
		    if(!empty($this->szDestinationCity) && empty($this->szDestinationPostCode))
			{		
				$toCityAry = array();		
				$toCityAry = $this->check_city_country($this->szDestinationCity,$this->szDestinationCountry) ;
				//print_r($this->multiEmaptyPostcodeFromCityAry);
				if(!$toCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_TO');
					$this->addError('szDestinationCity3',$error_messg);
				}
				else if($this->emptyPostcodeCtr > 0 && !empty($toCityAry))
				{
					$this->multiEmaptyPostcodeToCityAry = $toCityAry ;
				}
				/*
				else if($this->emptyPostcodeCtr == 1 && !empty($toCityAry))
				{
					$this->idDestinationPostCode = $this->idEmptyPostCode;
				}
				*/
				else if(empty($this->szDestinationPostCode) && ($this->szDestinationCountry != __ID_COUNTRY_HONG_KONG__))
				{
					$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_TO');
					$this->addError('szDestinationCity4',$error_messg);
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
				}																	
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}				
			
			// if we have only post codes 
			if(empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				$toCityAry=array();
				$this->szDestinationCity = $this->getAllCityByPostCode($this->szDestinationCountry,$this->szDestinationPostCode);
				$toCityAry = $this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode) ;
				if(!$toCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szDestinationCity4',$error_messg);
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
				}													
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}
		}
		if(($this->idServiceType==(int)__SERVICE_TYPE_DTW__) || ($this->idServiceType==(int)__SERVICE_TYPE_DTP__)) //DTW
		{
			// if we have both city and post code 
			if(!empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				if(!$this->isCityExistsByPostCode($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode))
				{
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_FROM')." ".$this->szOriginCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_FROM');
					$this->addError('szOriginCity',$error_messg);
				}													
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}
			
			// if we don't have any one 
			if(empty($this->szOriginCity) && empty($this->szOriginPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
				$this->addError('szOriginCity',$error_messg);
			}
			
			
			// if we have only from city  
			if(!empty($this->szOriginCity) && empty($this->szOriginPostCode))
			{			
				$fromCityAry = $this->check_city_country($this->szOriginCity,$this->szOriginCountry);
				if(empty($fromCityAry))
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
					$this->addError('szOriginCity3',$error_messg);
				}
				else if($this->emptyPostcodeCtr > 0 && !empty($fromCityAry))
				{
					//multi region; 
					$this->multiEmaptyPostcodeFromCityAry = $fromCityAry ;
				}
				/*
				else if($this->emptyPostcodeCtr == 1 && !empty($fromCityAry))
				{
					$this->idOriginPostCode = $this->idEmptyPostCode;
				}
				*/
				else if(empty($this->szOriginPostCode) && ($this->szOriginCountry != __ID_COUNTRY_HONG_KONG__))
				{
					$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_FROM');
					$this->addError('szOriginCity',$error_messg);
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}																	
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}
			// if we have only post code the  we first fetch city which belongs to this country and post code then check for multiregion 
			if(empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				$fromCityAry=array();
				$this->szOriginCity = $this->getAllCityByPostCode($this->szOriginCountry,$this->szOriginPostCode);
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode);
				if(!$fromCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
					$this->addError('szOriginCity',$error_messg);
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}									
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}		
			
			//if we have both to city and postcode .
			if(!empty($this->szDestinationCity) && !empty($this->szDestinationCity))
			{
				if(!$this->isCityExistsByPostCode($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode))
				{
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_TO')." ".$this->szDestinationCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_TO');
					$this->addError('szOriginCity',$error_messg);
				}									
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}	
			// if we don't have any 
			if(empty($this->szDestinationCity) && empty($this->szDestinationPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
				$this->addError('szOriginCity',$error_messg);
			}
							
			// if we have onlyc city 
		    if(!empty($this->szDestinationCity) && empty($this->szDestinationPostCode) )
			{
				$toCityAry = $this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry) ;
				if(!$toCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_TO');
					$this->addError('szDestinationCity',$error_messg);
				}
				elseif(count($toCityAry)>1)
				{
					$this->multiRegionToCityAry = $toCityAry ;					
				}
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
				
			}				
			
			// if we have only post codes 
			if(empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				$toCityAry=array();
				$this->szDestinationCity = $this->getAllCityByPostCode($this->szDestinationCountry,$this->szDestinationPostCode);
				$toCityAry = $this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode) ;
				if(!$toCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szOriginCity',$error_messg);
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
					$this->idDestinationPostCode = $this->idPostCode;
				}					
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
				
			}
		}
		if(($this->idServiceType==(int)__SERVICE_TYPE_WTD__) || ($this->idServiceType==(int)__SERVICE_TYPE_PTD__))
		{
			// if we have both city and post code 
			if(!empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				if(!$this->isCityExistsByPostCode($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode))
				{
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_FROM')." ".$this->szOriginCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_FROM');
					$this->addError('szOriginCity',$error_messg);
				}
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}
			
			// if we don't have any one 
			if(empty($this->szOriginCity) && empty($this->szOriginPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
				$this->addError('szOriginCity',$error_messg);
			}
			
			
			// if we have only from city  
			if(!empty($this->szOriginCity) && empty($this->szOriginPostCode))
			{
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry) ;
				if(!$fromCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
					$this->addError('szOriginCity',$error_messg);
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}			
	
			// if we have only post code the  we first fetch city which belongs to this country and post code then check for multiregion 
			if(empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				$fromCityAry=array();
				$this->szOriginCity = $this->getAllCityByPostCode($this->szOriginCountry,$this->szOriginPostCode);
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode);
				if(!$fromCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
					$this->addError('szOriginCity',$error_messg);
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}		
			
			//if we have both to city and postcode .
			if(!empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				if(!$this->isCityExistsByPostCode($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode))
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szDestinationCity',$error_messg);
				}				
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}
				
			// if we don't have any 
			if(empty($this->szDestinationCity) && empty($this->szDestinationPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
				$this->addError('szDestinationCity',$error_messg);
			}
							
			// if we have only city 
		    if(!empty($this->szDestinationCity) && empty($this->szDestinationPostCode) )
			{		
				$toCityAry = array();		
				$toCityAry = $this->check_city_country($this->szDestinationCity,$this->szDestinationCountry) ;
				//print_r($this->multiEmaptyPostcodeFromCityAry);
				if(!$toCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_TO');
					$this->addError('szDestinationCity3',$error_messg);
				}
				else if($this->emptyPostcodeCtr > 0 && !empty($toCityAry))
				{
					$this->multiEmaptyPostcodeToCityAry = $toCityAry ;
				}
				/*
				else if($this->emptyPostcodeCtr == 1 && !empty($toCityAry))
				{
					$this->idDestinationPostCode = $this->idEmptyPostCode;
				}
				*/
				else if(empty($this->szDestinationPostCode) && ($this->szDestinationCountry != __ID_COUNTRY_HONG_KONG__))
				{
					$error_messg = t($this->t_base.'errors/TYPE_POSTCODE_TO');
					$this->addError('szDestinationCity4',$error_messg);
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
				}																	
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}				
			
			// if we have only post codes 
			if(empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				$toCityAry=array();
				$this->szDestinationCity = $this->getAllCityByPostCode($this->szDestinationCountry,$this->szDestinationPostCode);
				$toCityAry = $this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode);
				if(!$toCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szDestinationCity',$error_messg);
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
				}
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}
		}
		if(($this->idServiceType==(int)__SERVICE_TYPE_WTW__) || ($this->idServiceType==(int)__SERVICE_TYPE_PTP__) || ($this->idServiceType==(int)__SERVICE_TYPE_WTP__) || ($this->idServiceType==(int)__SERVICE_TYPE_PTW__)) //WTW //WTW
		{
			// if we have both city and post code 
			if(!empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				if(!$this->isCityExistsByPostCode($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode))
				{
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_FROM')." ".$this->szOriginCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_FROM');
					$this->addError('szOriginCity',$error_messg);
				}				
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			
			}
			
			// if we don't have any one 
			if(empty($this->szOriginCity) && empty($this->szOriginPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
				$this->addError('szOriginCity',$error_messg);
			}
			
			
			// if we have only from city  
			if(!empty($this->szOriginCity) && empty($this->szOriginPostCode))
			{
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry) ;
				if(!$fromCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_CITY_FROM');
					$this->addError('szOriginCity',$error_messg);
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}			
	
			// if we have only post code the  we first fetch city which belongs to this country and post code then check for multiregion 
			if(empty($this->szOriginCity) && !empty($this->szOriginPostCode))
			{
				$fromCityAry=array();
				$this->szOriginCity = $this->getAllCityByPostCode($this->szOriginCountry,$this->szOriginPostCode);
				$fromCityAry = $this->isCityExistsByCountry($this->szOriginCity,$this->szOriginCountry,$this->szOriginPostCode) ;
				if(!$fromCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_FROM');
					$this->addError('szOriginCity',$error_messg);
				}
				else if(count($fromCityAry)>1)
				{
					//multi region; 
					$this->multiRegionFromCityAry = $fromCityAry ;
				}								
				else
				{
					$this->idOriginPostCode = $this->idPostCode;
				}
			}		
			
			//if we have both to city and postcode .
			if(!empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				if(!$this->isCityExistsByPostCode($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode))
				{
					$error_messg = t($this->t_base.'errors/POSTCODE_DOES_NOT_MATCH_WITH_CITY_TO')." ".$this->szDestinationCity." ".t($this->t_base.'errors/CHECK_INFO_PROVIDED_TO');
					$this->addError('szDestinationCity',$error_messg);
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
					$this->idDestinationPostCode = $this->idPostCode;
				}	
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}	
			// if we don't have any 
			if(empty($this->szDestinationCity) && empty($this->szDestinationPostCode))
			{
				$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
				$this->addError('szDestinationCity',$error_messg);
			}
							
			// if we have only city 
		    if(!empty($this->szDestinationCity) && empty($this->szDestinationPostCode) )
			{
				$toCityAry = $this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry) ;
				if(!$toCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szDestinationCity',$error_messg);
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
				}									
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}				
			
			// if we have only post codes 
			if(empty($this->szDestinationCity) && !empty($this->szDestinationPostCode))
			{
				$toCityAry=array();
				$this->szDestinationCity = $this->getAllCityByPostCode($this->szDestinationCountry,$this->szDestinationPostCode);
				$toCityAry = $this->isCityExistsByCountry($this->szDestinationCity,$this->szDestinationCountry,$this->szDestinationPostCode);
				if(!$toCityAry)
				{
					$error_messg = t($this->t_base.'errors/UNRECOGNISE_LOCATION_TO');
					$this->addError('szDestinationCity',$error_messg);
				}
				else if(count($toCityAry)>1)
				{
					//multi region; 
					$this->multiRegionToCityAry = $toCityAry ;
				}				
				else
				{
					$this->idDestinationPostCode = $this->idPostCode;
				}
			}
		}
	}

	function check_city_country($szCity,$idCountry)
	{
		if($idCountry>0)
		{
			if(!empty($szCity))
			{
				//$query_and .= " AND szCity ='".mysql_escape_custom(trim($szCity))."'";
				$query_and = "
						AND 
					(
						szCity = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity1 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity2 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity3 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity4 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity5 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity6 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity7 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity8 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity9 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity10 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szRegion1 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion2 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion3 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion4 = '".mysql_escape_custom(trim($szCity))."'	
					)		
				";
			    $query_index = " USE INDEX (country_city_regions)";
			}		
			
			if(!empty($szPostCode))
			{
				$query_and .= " AND szPostCode='".mysql_escape_custom(trim($szPostCode))."'" ;
				if(!empty($query_index))
				{
					$query_index = " USE INDEX (country_postcode_city__regions)";
				}
				else
				{
					$query_index = " USE INDEX (idCountry_2)";
				}
			}	
			
			$query="
				SELECT
					id,
					szRegion1,
					szRegion2,
					szRegion3,
					szRegion4,
					szCity,
					szPostCode,
					szArea1,
					szArea2
				FROM
					".__DBC_SCHEMATA_POSTCODE__."
					$query_index
				WHERE
					idCountry ='".(int)$idCountry."'
					$query_and	
			";
			//echo "<br>".$query."<br>" ;
			$this->emptyPostcodeCtr = 0;
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$szFirstRegion = '';
					$multi_region = false ;
					$arrRegion = array();
					$ctr=0;
					$emptyCtr = 0;
					while($row=$this->getAssoc($result))
					{
						$region_str = '';
						if(!empty($row['szRegion1']))
						{
							$region_str .=$row['szRegion1'] ;
						}					
							
						if(!empty($row['szRegion2']) && !empty($region_str))
						{
							$region_str .=", ".$row['szRegion2'] ;
						}
						else if(!empty($row['szRegion2']))
						{
							$region_str .= $row['szRegion2'] ;
						}
						
						if(!empty($row['szRegion3']) && !empty($region_str))
						{
							$region_str .=", ".$row['szRegion3'];
						}
						else if(!empty($row['szRegion3']))
						{
							$region_str .=$row['szRegion3'] ;
						}
						
						if(!empty($row['szRegion4']) && !empty($region_str))
						{
							$region_str .=", ".$row['szRegion4'] ;
						}
						else if(!empty($row['szRegion4']))
						{
							$region_str .=$row['szRegion4'] ;						
						}
						
						if(!empty($row['szCity']) && !empty($region_str))
						{
							$region_str .=", ".$row['szCity'] ;
						}
						else if(!empty($row['szCity']))
						{
							$region_str .=$row['szCity'] ;
						}
						
						if(!empty($row['szArea1']) && !empty($region_str))
						{
							$region_str .=", ".$row['szArea1'] ;
						}
						else if(!empty($row['szArea1']))
						{
							$region_str .=$row['szArea1'] ;
						}
						
						if(!empty($row['szArea2']) && !empty($region_str))
						{
							$region_str .=", ".$row['szArea2'] ;
						}
						else if(!empty($row['szArea2']))
						{
							$region_str .=$row['szArea2'] ;
						}
						
						if(!empty($row['szPostCode']))
						{
							$region_str .=", ".$row['szPostCode'] ;
						}						
						else if(!empty($row['szPostCode']))
						{
							$region_str .=$row['szPostCode'] ;
						}
						
						$arrRegion[$row['id']] = $region_str;
						$ctr++;	
						if(empty($row['szPostCode']))
						{
							$emptyCtr ++ ;
						}
					}				
					
					if($emptyCtr==1)
					{
						$this->idEmptyPostCode = $row['id'];	
						echo $this->idEmptyPostCode ;
					}
					$this->emptyPostcodeCtr = $emptyCtr;
					return $arrRegion ;
				}
				else
				{
					return array();
				}
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	function isCityExistsByCountry($szCity=false,$idCountry,$szPostCode=false)
	{
		if($idCountry>0)
		{
			if(!empty($szCity))
			{
				$query_and.= "
						AND 
					(
						szCity = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity1 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity2 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity3 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity4 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity5 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity6 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity7 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity8 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity9 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity10 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szRegion1 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion2 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion3 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion4 = '".mysql_escape_custom(trim($szCity))."'	
					)		
				";		
			}
			
			if(!empty($szPostCode))
			{
				$query_and = " AND szPostCode='".mysql_escape_custom(trim($szPostCode))."'" ;
			}
			
			$query="
				SELECT
					id,
					szRegion1,
					szCity
				FROM
					".__DBC_SCHEMATA_POSTCODE__."	
				WHERE
					idCountry ='".(int)$idCountry."'
					$query_and	
				GROUP BY
					szRegion1	
			";
			//echo "<br>".$query."<br>" ;
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$szFirstRegion = '';
					$multi_region = false ;
					$arrRegion = array();
					$ctr=0;
					while($row=$this->getAssoc($result))
					{
						if(!empty($row['szRegion1']))
						{
							if(empty($szFirstRegion))
							{
								$szFirstRegion =trim($row['szRegion1']);
							}
							if($szFirstRegion !=trim($row['szRegion1']))
							{
								$multi_region = true ;
							}
							$arrRegion[$row['id']]=trim($row['szRegion1']).','.trim($row['szCity']);
							$ctr++;							
						}
						$this->idPostCode = $row['id'];
					}
					if($multi_region)
					{
						return 	$arrRegion ;
					}
					else
					{
						return true;					
					}
				}
				else
				{
					return array();
				}
			}
			else
			{
				return array();
			}
		}
		else
		{
			return array();
		}
	}
	
	function isCityExistsByPostCode($szCity,$idCountry,$szPostCode)
	{
		if($idCountry>0)
		{
			if(!empty($szPostCode))
			{
				$query_and= " AND szPostCode = '".mysql_escape_custom(trim($szPostCode))."'" ;
			}
			if(!empty($szCity))
			{
				//$query_and .= " AND szCity = '".mysql_escape_custom(trim($szCity))."'" ;
				
				$query_and.= "
						AND 
					(
						szCity = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity1 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity2 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity3 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity4 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity5 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity6 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity7 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity8 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity9 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity10 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szRegion1 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion2 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion3 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion4 = '".mysql_escape_custom(trim($szCity))."'	
					)		
				";	
			}
			$query="
				SELECT
					id
				FROM
					".__DBC_SCHEMATA_POSTCODE__."	
				WHERE
					idCountry = '".(int)$idCountry."'
					".$query_and."					
			";
			//echo $query."<br>" ;
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					while($row=$this->getAssoc($result))
					{
						$this->idPostCode = $row['id'] ;
					}
					return true ;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	function getAllCityByPostCode($idCountry,$szPostCode)
	{
		if($idCountry>0)
		{
			$query="
				SELECT
					DISTINCT szCity
				FROM
					".__DBC_SCHEMATA_POSTCODE__."	
				WHERE
					idCountry ='".(int)$idCountry."'
				AND
					szPostCode = '".mysql_escape_custom(trim($szPostCode))."'	
			";
			//echo "<br> region check : ".$query."<br>";
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result) ;		
					return $row['szCity'] ;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function getPostCodeDetails($idCountry,$szPostCode,$szcity)
	{
		if($idCountry>0)
		{
			if(!empty($szPostCode))
			{
				$query_and = " AND szPostCode='".mysql_escape_custom(trim($szPostCode))."'" ;
			}
			if(!empty($szcity))
			{
				//$query_and .= " AND szCity='".mysql_escape_custom(trim($szcity))."'" ;				
				$query_and.= "
						AND 
					(
						szCity = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity1 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity2 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity3 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity4 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity5 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity6 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity7 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity8 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity9 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szCity10 = '".mysql_escape_custom(trim($szCity))."'
					OR
						szRegion1 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion2 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion3 = '".mysql_escape_custom(trim($szCity))."'	
					OR
						szRegion4 = '".mysql_escape_custom(trim($szCity))."'	
					)		
				";
			}
			$query="
				SELECT
					count(id) pscount,
					sum(szLat)/count(id) szLatitude,
					sum(szLng)/count(id) szLongitude
				FROM
					".__DBC_SCHEMATA_POSTCODE__."	
				WHERE
					idCountry='".(int)$idCountry."'
					".$query_and."
				HAVING
					pscount>0	
				ORDER BY
					id ASC	
			";
			//echo $query;
			//die;
			if($result = $this->exeSQL($query))
			{
				if($this->iNumRows>0)
				{
					$row=$this->getAssoc($result) ;		
					return $row;
				}
				else
				{
					return array();
				}
			}
			else
			{
				return false;
			}
		}
	}
	
	function updateCargoCommodity($cargoAry)
	{
		if(!empty($cargoAry))
		{
			$counterCom = count($cargoAry['idCargo']);
			for($i=0;$i<$counterCom;$i++)
			{
				$query="
					UPDATE
						".__DBC_SCHEMATA_CARGO__."
					SET
						szCommodity = '".mysql_escape_custom(trim($cargoAry['szCargoCommodity'][$i]))."'
					WHERE
						id='".(int)$cargoAry['idCargo'][$i]."'			
				";
				//echo "<br>".$query."<br>";
				if($result=$this->exeSQL($query))
				{
					// updated success fully 
				}
				else
				{
					//log error 
					return false ;
				}
			}	
		}
	} 
	function addUpdateCargoCommodity($cargoAry)
	{ 
            if(!empty($cargoAry))
            { 
                $res_ary = array();
                $res_ary['szCargoDescription'] = utf8_encode($cargoAry['szCargoCommodity']);  
                
                if(!empty($res_ary))
                {
                    foreach($res_ary as $key=>$value)
                    {
                        $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                }
                $update_query = rtrim($update_query,",");
                $kBooking = new cBooking();
                $kBooking->updateDraftBooking($update_query,$cargoAry['idBooking']);
            }
	}
	
	function getShipperConsigneeDetails($idBooking)
	{
		if($idBooking>0)
		{
			$query="
                            SELECT
                                idBooking,
                                szShipperCompanyName,
                                szShipperFirstName,
                                szShipperLastName,
                                szShipperAddress,
                                szShipperAddress2,
                                szShipperAddress3,
                                szShipperPostCode,
                                szShipperCity,
                                szShipperState,
                                szShipperPhone,
                                szShipperEmail,
                                idShipperCountry,					
                                szShipperAddress_pickup,
                                szShipperAddress2_pickup,
                                szShipperAddress3_pickup,
                                szShipperPostCode_pickup,
                                szShipperCity_pickup,
                                szShipperState_pickup,	
                                idShipperCountry_pickup,
                                idShipperGroup,								
                                szConsigneeCompanyName,
                                szConsigneeFirstName,
                                szConsigneeLastName,
                                szConsigneeAddress,
                                szConsigneeAddress2,
                                szConsigneeAddress3,
                                szConsigneePostCode,
                                szConsigneeCity,
                                szConsigneeState,
                                szConsigneePhone,
                                szConsigneeEmail,
                                idConsigneeCountry,					
                                szConsigneeAddress_pickup,
                                szConsigneeAddress2_pickup,
                                szConsigneeAddress3_pickup,
                                szConsigneePostCode_pickup,
                                szConsigneeCity_pickup,
                                szConsigneeState_pickup,
                                idConsigneeCountry_pickup,
                                idConsigneeGroup,
                                iPickupAddress,
                                iDeliveryAddress,
                                iDonotKnowShipperPostcode,
                                iDonotKnowShipperAddress,
                                idConsigneeDialCode,
                                idShipperDialCode,
                                iGetShipperCityFromGoogle,					
                                iGetShipperPostcodeFromGoogle,
                                iGetShipperAddressFromGoogle,
                                iGetConsigneeAddressFromGoogle,
                                iGetConsigneeCityFromGoogle,
                                iGetConsigneePostcodeFromGoogle,
                                iShipperConsignee,
                                szShipperHeading,
                                szConsigneeHeading,
                                iDonotKnowConsigneePostcode,
                                iDonotKnowConsigneeAddress,
                                iThisismeDisplayed
                            FROM
                                ".__DBC_SCHEMATA_SHIPPER_CONSIGNEE__."	
                            WHERE
                                idBooking='".(int)$idBooking."'	
			";
			//echo $query."<br>";
			if($result=$this->exeSQL($query))
			{
                            $shipperConsigneeAry = array();
                            while($row=$this->getAssoc($result))
                            {
                                    $shipperConsigneeAry=$row ;
                            }
                            return $shipperConsigneeAry ;
			}
			else
			{
				return false ;
			}
		}
	}
	
	function isShipperConsigneeExist($idBooking)
	{
            if($idBooking>0)
            {
                if(trim(cPartner::$szApiRequestMode) == __API_REQUEST_MODE_TEST__)
                {
                    $table = __DBC_SCHEMATA_CLONE_SHIPPER_CONSIGNEE__;
                }
                else
                {
                    $table = __DBC_SCHEMATA_SHIPPER_CONSIGNEE__;
                }
                $query="
                    SELECT
                        id
                    FROM
                        ".$table."
                    WHERE
                        idBooking = '".(int)$idBooking."'		
                ";
                if($result = $this->exeSQL($query))
                {
                    if($this->iNumRows>0)
                    {
                        $row=$this->getAssoc($result);
                        $this->idShipperConsignee = $row['id'];
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
	}
	
	function addShipperConsignee_new($data,$idBooking,$add_quote=false)
	{
            if(!empty($data) && (int)$idBooking>0)
            {
                if(!$add_quote)
                { 
                    /* 
                    * By pass all the checks in case of quotation. 
                    */

                    $kBooking = new cBooking();
                    $postSearchAry = array();
                    $postSearchAry = $kBooking->getBookingDetails($idBooking);
 
                    $bShipperCompanyRequiredFlag = true;
                    $bConsigneeCompanyRequiredFlag = true;
                    $bBillingCompanyRequiredFlag = true;
                    
                    if($_SESSION['user_id']>0)
                    {
                        $kUser=new cUser();
                        $kUser->getUserDetails($_SESSION['user_id']);
                        $iPrivateShipping = $kUser->iPrivate;
                    }
                    else
                    {
                        $iPrivateShipping = $postSearchAry['iPrivateShipping'];
                    }
                    if($iPrivateShipping==1)
                    {
                        /*
                        * If customer is Private and
                        * a. Customer is Shipper then Shipper company field is not required
                        * b. Customer is consignee then consignee company field is not required
                        * c. Customer is billing then billing company field is not required
                        */
                        if($data['iShipperConsignee']==1)
                        {
                            $bShipperCompanyRequiredFlag = false;
                        }
                        else if($data['iShipperConsignee']==2)
                        {
                            $bConsigneeCompanyRequiredFlag = false;
                        }
                        else
                        {
                            $bBillingCompanyRequiredFlag = false;
                        }
                    } 
                    $this->set_idServiceType(trim(sanitize_all_html_input($data['idServiceType'])));			
                    $this->set_szShipperCompanyName(trim(sanitize_all_html_input($data['szShipperCompanyName'])),$bShipperCompanyRequiredFlag);
                    $this->set_szShipperFirstName(trim(sanitize_all_html_input($data['szShipperFirstName'])));
                    $this->set_szShipperLastName(trim(sanitize_all_html_input($data['szShipperLastName'])));
                    $this->set_szShipperEmail(trim(sanitize_all_html_input($data['szShipperEmail'])),true);				
                    $this->set_szShipperPhone(trim(sanitize_all_html_input($data['szShipperPhone'])),true);	 
                    $this->set_idShipperDialCode(trim(sanitize_all_html_input($data['idShipperDialCode']))); 
                     
                    $this->set_iShipperConsignee(trim(sanitize_all_html_input($data['iShipperConsignee'])));
                    $this->set_szShipperTitleText(trim(sanitize_all_html_input($data['szShipperHeading'])),false);
                    $this->set_szConsigneeTitleText(trim(sanitize_all_html_input($data['szConsigneeHeading'])),false);
                    
                    $this->idConsigneeCountry = $postSearchAry['idDestinationCountry'];
                    $this->idShipperCountry = $postSearchAry['idOriginCountry'];

                    if($data['iGetShipperPostcodeFromGoogle']==1)
                    {
                        $this->szShipperPostcode = trim(sanitize_all_html_input($data['szShipperPostcode'])) ;
                    }
                    else if($data['iDonotKnowShipperPostcode']==1)
                    {
                        $this->szShipperPostcode = '';
                    }
                    else
                    {
                        $this->set_szShipperPostcode(trim(sanitize_all_html_input($data['szShipperPostcode'])),true);
                    }

                    if($data['iGetShipperAddressFromGoogle']==1)
                    {
                        $this->szShipperAddress = trim(sanitize_all_html_input($data['szShipperAddress'])) ;
                    }
                    else if($data['iDonotKnowShipperAddress']==1)
                    {
                        $this->szShipperAddress = '';
                    }
                    else
                    {
                        $this->set_szShipperAddress(trim(sanitize_all_html_input($data['szShipperAddress'])));
                    }

                    if($data['iGetShipperCityFromGoogle']==1)
                    {
                        $this->szShipperCity = trim(sanitize_all_html_input($data['szShipperCity'])) ;
                    }
                    else
                    {
                        $this->set_szShipperCity(trim(sanitize_all_html_input($data['szShipperCity'])),true); 
                    }

                    $this->set_szConsigneeCompanyName(trim(sanitize_all_html_input($data['szConsigneeCompanyName'])),$bConsigneeCompanyRequiredFlag);
                    $this->set_szConsigneeFirstName(trim(sanitize_all_html_input($data['szConsigneeFirstName'])));
                    $this->set_szConsigneeLastName(trim(sanitize_all_html_input($data['szConsigneeLastName'])));
                    $this->set_szConsigneeEmail(trim(sanitize_all_html_input($data['szConsigneeEmail'])),true); 
                    $this->set_szConsigneePhone(trim(sanitize_all_html_input($data['szConsigneePhone'])),true);	
                    $this->set_idConsigneeDialCode(trim(sanitize_all_html_input($data['idConsigneeDialCode'])));	 
 
                    if($data['iGetConsigneeCityFromGoogle']==1)
                    {
                        $this->szConsigneeCity = trim(sanitize_all_html_input($data['szConsigneeCity']));
                    }
                    else
                    {
                        $this->set_szConsigneeCity(trim(sanitize_all_html_input($data['szConsigneeCity'])),true);
                    }

                    if($data['iGetConsigneePostcodeFromGoogle']==1)
                    {
                        $this->szConsigneePostcode = trim(sanitize_all_html_input($data['szConsigneePostcode']));
                    }
                    else if($data['iDonotKnowConsigneePostcode']==1)
                    {
                            $this->szConsigneePostcode = '';
                    }
                    else
                    {
                        $this->set_szConsigneePostcode(trim(sanitize_all_html_input($data['szConsigneePostcode'])),true);
                    }

                    if($data['iGetConsigneeAddressFromGoogle']==1)
                    {
                        $this->szConsigneeAddress = trim(sanitize_all_html_input($data['szConsigneeAddress']));
                    }
                    else if($data['iDonotKnowConsigneeAddress']==1)
                    {
                        $this->szConsigneeAddress = '';
                    }
                    else
                    {
                        $this->set_szConsigneeAddress(trim(sanitize_all_html_input($data['szConsigneeAddress']))); 
                    }

                    $this->idShipperCountry_pickup = $this->idShipperCountry ;
                    $this->szShipperCity_pickup = $this->szShipperCity ;
                    $this->szShipperPostcode_pickup = $this->szShipperPostcode ;				
                    $this->szShipperAddress_pickup = $this->szShipperAddress ;
                    $this->szShipperAddress2_pickup = $this->szShipperAddress2;
                    $this->szShipperAddress3_pickup = $this->szShipperAddress3 ;
                    $this->szShipperState_pickup = $this->szShipperState ;

                    $this->idConsigneeCountry_pickup = $this->idConsigneeCountry ;
                    $this->szConsigneeCity_pickup = $this->szConsigneeCity ;
                    $this->szConsigneePostcode_pickup = $this->szConsigneePostcode ;				
                    $this->szConsigneeAddress_pickup = $this->szConsigneeAddress ;
                    $this->szConsigneeAddress2_pickup = $this->szConsigneeAddress2;
                    $this->szConsigneeAddress3_pickup = $this->szConsigneeAddress3 ;
                    $this->szConsigneeState_pickup = $this->szConsigneeState ;

                    $userSessionData = array();
                    $userSessionData = get_user_session_data();
                    $idCustomer = $userSessionData['idUser']; 
                    $iThisismeDisplayed = $data['iThisismeDisplayed'];
                    if($data['iThisIsMe']<=0)
                    {
                        $this->set_szBillingCompanyName(trim(sanitize_all_html_input($data['szBillingCompanyName'])),$bBillingCompanyRequiredFlag);
                        $this->set_szBillingFirstName(trim(sanitize_all_html_input($data['szBillingFirstName'])));
                        $this->set_szBillingLastName(trim(sanitize_all_html_input($data['szBillingLastName'])));
                        $this->set_szBillingEmail(trim(sanitize_all_html_input($data['szBillingEmail'])),true); 
                        $this->set_szBillingPhone(trim(sanitize_all_html_input($data['szBillingPhone'])),true);	
                        //$this->set_idBillingCountry(trim(sanitize_all_html_input($data['idBillingCountry'])));
                        $this->set_idBillingDialCode(trim(sanitize_all_html_input($data['idBillingDialCode'])));	 
                        $this->set_szBillingCity(trim(sanitize_all_html_input($data['szBillingCity'])),true);
                        $this->set_szBillingPostcode(trim(sanitize_all_html_input($data['szBillingPostcode'])),true);
                        $this->set_szBillingAddress(trim(sanitize_all_html_input($data['szBillingAddress']))); 
                        
                        $this->idBillingCountry = $this->idConsigneeCountry ;
                        $this->iShipperConsignee = 3;
                    }
                    else
                    {	
                        if($this->iShipperConsignee==1)
                        {
                            $this->szBillingCompanyName = $this->szShipperCompanyName ;
                            $this->szBillingFirstName = $this->szShipperFirstName ;
                            $this->szBillingLastName = $this->szShipperLastName ;
                            $this->szBillingEmail = $this->szShipperEmail ;
                            $this->szBillingPhone = $this->szShipperPhone ;
                            $this->idBillingDialCode = $this->idShipperDialCode ; 
                            $this->idBillingCountry = $this->idShipperCountry ;
                            $this->szBillingCity = $this->szShipperCity ;
                            $this->szBillingPostcode = $this->szShipperPostcode ;				
                            $this->szBillingAddress = $this->szShipperAddress ; 
                            $this->szBillingState = $this->szShipperState ;
                        }
                        else 
                        {
                            $this->szBillingCompanyName = $this->szConsigneeCompanyName ;
                            $this->szBillingFirstName = $this->szConsigneeFirstName ;
                            $this->szBillingLastName = $this->szConsigneeLastName ;
                            $this->szBillingEmail = $this->szConsigneeEmail ;
                            $this->szBillingPhone = $this->szConsigneePhone ;
                            $this->idBillingDialCode = $this->idConsigneeDialCode ; 
                            $this->idBillingCountry = $this->idConsigneeCountry ;
                            $this->szBillingCity = $this->szConsigneeCity ;
                            $this->szBillingPostcode = $this->szConsigneePostcode ;				
                            $this->szBillingAddress = $this->szConsigneeAddress ; 
                            $this->szBillingState = $this->szConsigneeState ;
                        }  
                    }

                    $this->set_szCargoCommodity($data['szCargoCommodity']);
                    $this->set_idCargo($data['idCargo']);
                    $kForwarder = new cForwarder();
                    
                    
                    $userSessionData = array();
                    $userSessionData = get_user_session_data();
                    $idCustomer = $userSessionData['idUser']; 
                    if((int)$idCustomer>0 && $data['iThisIsMe']==1)
                    {  
                        $kUser = new cUser();
                        if($kUser->isUserEmailExist($this->szBillingEmail,$idCustomer))
                        { 
                            if($this->iShipperConsignee==1)
                            {
                                 $szFieldName="szShipperEmail";
                            }
                            else
                            {
                                $szFieldName="szConsigneeEmail";
                            }
                            $this->addError($szFieldName,'An user with the following e-mail ID already exist.');
                            return false;
                        }
                         
                    }

                    if(!empty($data['szCargoCommodity']))
                    {
                        if($kForwarder->checkNonAcceptanceOfForwarder($data['szCargoCommodity'],$data['idForwarder']))
                        {
                            $nonAcceptedGoodsAry[] = $data['szCargoCommodity'];
                        }
                    }

                    if(!empty($nonAcceptedGoodsAry))
                    {
                        $this->addError('iNonAcceptedGoods',implode(',',$nonAcceptedGoodsAry).' is non accepted goods for '.$data['szForwarderDispName']);
                        return false;		
                    }
                    if($this->error==true)
                    {
                        return false;				
                    }					 
                    if($this->error==true)
                    {
                        return false;				
                    }

                    if(empty($this->szShipperCity))
                    {
                        $this->szShipperCity = $this->szOriginCity ;
                    }
                    if(empty($this->szConsigneeCity))
                    {
                        $this->szConsigneeCity = $this->szDestinationCity ;
                    }
                }
			
		if(trim(cPartner::$szApiRequestMode) == __API_REQUEST_MODE_TEST__)
                {
                    $table = __DBC_SCHEMATA_CLONE_SHIPPER_CONSIGNEE__;
                }
                else
                {
                    $table = __DBC_SCHEMATA_SHIPPER_CONSIGNEE__;
                }
                
                if($this->isShipperConsigneeExist($idBooking))
                {
                    $query = "
                        UPDATE
                            ".$table."
                        SET
                            szShipperCompanyName = '".mysql_escape_custom(trim($this->szShipperCompanyName))."',
                            szShipperFirstName = '".mysql_escape_custom(trim($this->szShipperFirstName))."',
                            szShipperLastName = '".mysql_escape_custom(trim($this->szShipperLastName))."',
                            szShipperAddress = '".mysql_escape_custom(trim($this->szShipperAddress))."',
                            szShipperAddress2 = '".mysql_escape_custom(trim($this->szShipperAddress2))."',
                            szShipperAddress3 = '".mysql_escape_custom(trim($this->szShipperAddress3))."',
                            szShipperPostCode = '".mysql_escape_custom(trim($this->szShipperPostcode))."',
                            szShipperCity = '".mysql_escape_custom(trim($this->szShipperCity))."',
                            szShipperState = '".mysql_escape_custom(trim($this->szShipperState))."',
                            szShipperPhone = '".mysql_escape_custom(trim($this->szShipperPhone))."',
                            szShipperEmail = '".mysql_escape_custom(trim($this->szShipperEmail))."',
                            idShipperCountry = '".(int)$this->idShipperCountry."',		
                            idShipperDialCode = '".(int)$this->idShipperDialCode."',	
                            idConsigneeDialCode = '".(int)$this->idConsigneeDialCode."',
                            szShipperAddress_pickup = '".mysql_escape_custom(trim($this->szShipperAddress_pickup))."',
                            szShipperAddress2_pickup = '".mysql_escape_custom(trim($this->szShipperAddress2_pickup))."',
                            szShipperAddress3_pickup = '".mysql_escape_custom(trim($this->szShipperAddress3_pickup))."',
                            szShipperPostCode_pickup = '".mysql_escape_custom(trim($this->szShipperPostcode_pickup))."',
                            szShipperCity_pickup = '".mysql_escape_custom(trim($this->szShipperCity_pickup))."',
                            szShipperState_pickup = '".mysql_escape_custom(trim($this->szShipperState_pickup))."',
                            idShipperCountry_pickup = '".(int)$this->idShipperCountry_pickup."',
                            idShipperGroup = '1',							
                            szConsigneeCompanyName = '".mysql_escape_custom(trim($this->szConsigneeCompanyName))."',
                            szConsigneeFirstName = '".mysql_escape_custom(trim($this->szConsigneeFirstName))."',
                            szConsigneeLastName = '".mysql_escape_custom(trim($this->szConsigneeLastName))."',
                            szConsigneeAddress = '".mysql_escape_custom(trim($this->szConsigneeAddress))."',
                            szConsigneeAddress2 = '".mysql_escape_custom(trim($this->szConsigneeAddress2))."',
                            szConsigneeAddress3 = '".mysql_escape_custom(trim($this->szConsigneeAddress3))."',
                            szConsigneePostCode = '".mysql_escape_custom(trim($this->szConsigneePostcode))."',
                            szConsigneeCity = '".mysql_escape_custom(trim($this->szConsigneeCity))."',
                            szConsigneeState = '".mysql_escape_custom(trim($this->szConsigneeState))."',
                            szConsigneePhone = '".mysql_escape_custom(trim($this->szConsigneePhone))."',
                            szConsigneeEmail = '".mysql_escape_custom(trim($this->szConsigneeEmail))."',
                            idConsigneeCountry = '".(int)$this->idConsigneeCountry."',					
                            szConsigneeAddress_pickup = '".mysql_escape_custom(trim($this->szConsigneeAddress_pickup))."',
                            szConsigneeAddress2_pickup = '".mysql_escape_custom(trim($this->szConsigneeAddress2_pickup))."',
                            szConsigneeAddress3_pickup = '".mysql_escape_custom(trim($this->szConsigneeAddress3_pickup))."',
                            szConsigneePostCode_pickup = '".mysql_escape_custom(trim($this->szConsigneePostcode_pickup))."',
                            szConsigneeCity_pickup = '".mysql_escape_custom(trim($this->szConsigneeCity_pickup))."',
                            szConsigneeState_pickup = '".mysql_escape_custom(trim($this->szConsigneeState_pickup))."',
                            idConsigneeCountry_pickup = '".(int)$this->idConsigneeCountry_pickup."',
                            idConsigneeGroup = '2',	
                            iPickupAddress = '".(int)$data['iPickupAddress_hidden']."',
                            iDeliveryAddress = '".(int)$data['iDeliveryAddress_hidden']."',
                            iDonotKnowShipperPostcode = '".(int)$data['iDonotKnowShipperPostcode']."',
                            iDonotKnowShipperAddress = '".(int)$data['iDonotKnowShipperAddress']."', 
                            iGetShipperCityFromGoogle = '".(int)$data['iGetShipperCityFromGoogle']."', 
                            iGetShipperPostcodeFromGoogle = '".(int)$data['iGetShipperPostcodeFromGoogle']."',
                            iGetShipperAddressFromGoogle = '".(int)$data['iGetShipperAddressFromGoogle']."',
                            iGetConsigneeAddressFromGoogle = '".(int)$data['iGetConsigneeAddressFromGoogle']."',
                            iGetConsigneeCityFromGoogle = '".(int)$data['iGetConsigneeCityFromGoogle']."',
                            iGetConsigneePostcodeFromGoogle = '".(int)$data['iGetConsigneePostcodeFromGoogle']."',   
                            iShipperConsignee = '".mysql_escape_custom(trim($this->iShipperConsignee))."',
                            szShipperHeading = '".mysql_escape_custom(trim($this->szShipperHeading))."',
                            szConsigneeHeading = '".mysql_escape_custom(trim($this->szConsigneeHeading))."', 
                            iDonotKnowConsigneePostcode = '".(int)$data['iDonotKnowConsigneePostcode']."',
                            iDonotKnowConsigneeAddress = '".(int)$data['iDonotKnowConsigneeAddress']."', 
                            iThisismeDisplayed = '".(int)$data['iThisismeDisplayed']."',  
                            dtUpdatedOn = now()
                        WHERE
                            id='".(int)$this->idShipperConsignee."'
                        AND
                            idBooking = '".(int)$idBooking."'	
                    ";
                    $added_flag =false ;
                }
                else
                {
                    $added_flag = true;
                    $query="
                        INSERT INTO
                            ".$table."
                        (
                            idBooking,
                            szShipperCompanyName,
                            szShipperFirstName,
                            szShipperLastName,
                            szShipperAddress,
                            szShipperAddress2,
                            szShipperAddress3,
                            szShipperPostCode,
                            szShipperCity,
                            szShipperState,
                            szShipperPhone,
                            szShipperEmail,
                            idShipperCountry,					
                            szShipperAddress_pickup,
                            szShipperAddress2_pickup,
                            szShipperAddress3_pickup,
                            szShipperPostCode_pickup,
                            szShipperCity_pickup,
                            szShipperState_pickup,	
                            idShipperCountry_pickup,
                            idShipperGroup,								
                            szConsigneeCompanyName,
                            szConsigneeFirstName,
                            szConsigneeLastName,
                            szConsigneeAddress,
                            szConsigneeAddress2,
                            szConsigneeAddress3,
                            szConsigneePostCode,
                            szConsigneeCity,
                            szConsigneeState,
                            szConsigneePhone,
                            szConsigneeEmail,
                            idConsigneeCountry,					
                            szConsigneeAddress_pickup,
                            szConsigneeAddress2_pickup,
                            szConsigneeAddress3_pickup,
                            szConsigneePostCode_pickup,
                            szConsigneeCity_pickup,
                            szConsigneeState_pickup,
                            idConsigneeCountry_pickup,
                            idConsigneeGroup,
                            idConsigneeDialCode, 
                            idShipperDialCode,
                            iPickupAddress,
                            iDeliveryAddress,	
                            iDonotKnowShipperPostcode,
                            iDonotKnowShipperAddress,	 
                            iGetShipperCityFromGoogle,					
                            iGetShipperPostcodeFromGoogle,
                            iGetShipperAddressFromGoogle,
                            iGetConsigneeAddressFromGoogle,
                            iGetConsigneeCityFromGoogle,
                            iGetConsigneePostcodeFromGoogle, 
                            iShipperConsignee,
                            szShipperHeading,
                            szConsigneeHeading,
                            iDonotKnowConsigneePostcode,
                            iDonotKnowConsigneeAddress,
                            iThisismeDisplayed,
                            iActive,
                            dtCreatedOn				
                        )	
                        VALUES
                        (
                            '".(int)$idBooking."',
                            '".mysql_escape_custom(trim($this->szShipperCompanyName))."',
                            '".mysql_escape_custom(trim($this->szShipperFirstName))."',
                            '".mysql_escape_custom(trim($this->szShipperLastName))."',
                            '".mysql_escape_custom(trim($this->szShipperAddress))."',
                            '".mysql_escape_custom(trim($this->szShipperAddress2))."',
                            '".mysql_escape_custom(trim($this->szShipperAddress3))."',
                            '".mysql_escape_custom(trim($this->szShipperPostcode))."',
                            '".mysql_escape_custom(trim($this->szShipperCity))."',
                            '".mysql_escape_custom(trim($this->szShipperState))."',
                            '".mysql_escape_custom(trim($this->szShipperPhone))."',
                            '".mysql_escape_custom(trim($this->szShipperEmail))."',
                            '".(int)$this->idShipperCountry."',
                            '".mysql_escape_custom(trim($this->szShipperAddress_pickup))."',
                            '".mysql_escape_custom(trim($this->szShipperAddress2_pickup))."',
                            '".mysql_escape_custom(trim($this->szShipperAddress3_pickup))."',
                            '".mysql_escape_custom(trim($this->szShipperPostcode_pickup))."',
                            '".mysql_escape_custom(trim($this->szShipperCity_pickup))."',
                            '".mysql_escape_custom(trim($this->szShipperState_pickup))."',
                            '".(int)$this->idShipperCountry_pickup."',
                            '1',
                            '".mysql_escape_custom(trim($this->szConsigneeCompanyName))."',
                            '".mysql_escape_custom(trim($this->szConsigneeFirstName))."',
                            '".mysql_escape_custom(trim($this->szConsigneeLastName))."',
                            '".mysql_escape_custom(trim($this->szConsigneeAddress))."',
                            '".mysql_escape_custom(trim($this->szConsigneeAddress2))."',
                            '".mysql_escape_custom(trim($this->szConsigneeAddress3))."',
                            '".mysql_escape_custom(trim($this->szConsigneePostcode))."',
                            '".mysql_escape_custom(trim($this->szConsigneeCity))."',
                            '".mysql_escape_custom(trim($this->szConsigneeState))."',
                            '".mysql_escape_custom(trim($this->szConsigneePhone))."',
                            '".mysql_escape_custom(trim($this->szConsigneeEmail))."',
                            '".(int)$this->idConsigneeCountry."',
                            '".mysql_escape_custom(trim($this->szConsigneeAddress_pickup))."',
                            '".mysql_escape_custom(trim($this->szConsigneeAddress2_pickup))."',
                            '".mysql_escape_custom(trim($this->szConsigneeAddress3_pickup))."',
                            '".mysql_escape_custom(trim($this->szConsigneePostcode_pickup))."',
                            '".mysql_escape_custom(trim($this->szConsigneeCity_pickup))."',
                            '".mysql_escape_custom(trim($this->szConsigneeState_pickup))."',
                            '".(int)$this->idConsigneeCountry_pickup."',
                            '2',
                            '".(int)$this->idConsigneeDialCode."',
                            '".(int)$this->idShipperDialCode."',  
                            '".(int)$data['iPickupAddress_hidden']."',
                            '".(int)$data['iDeliveryAddress_hidden']."',
                            '".(int)$data['iDonotKnowShipperPostcode']."', 
                            '".(int)$data['iDonotKnowShipperAddress']."',  
                            '".(int)$data['iGetShipperCityFromGoogle']."', 
                            '".(int)$data['iGetShipperPostcodeFromGoogle']."', 
                            '".(int)$data['iGetShipperAddressFromGoogle']."', 
                            '".(int)$data['iGetConsigneeAddressFromGoogle']."', 
                            '".(int)$data['iGetConsigneeCityFromGoogle']."', 
                            '".(int)$data['iGetConsigneePostcodeFromGoogle']."', 
                            '".mysql_escape_custom(trim($this->iShipperConsignee))."',
                            '".mysql_escape_custom(trim($this->szShipperHeading))."',
                            '".mysql_escape_custom(trim($this->szConsigneeHeading))."',  
                            '".(int)$data['iDonotKnowConsigneePostcode']."', 
                            '".(int)$data['iDonotKnowConsigneeAddress']."', 
                            '".(int)$data['iThisismeDisplayed']."',  
                            '1',
                            now()
                        )
                    "; 
		}
//		echo "<br>".$query."<br>" ; 
                if($result=$this->exeSQL($query))
                { 
                    if($added_flag)
                    {
                        $idShipperConsignee = $this->iLastInsertID ;
                    }
                    else
                    {
                        $idShipperConsignee = (int)$this->idShipperConsignee ;	
                    }  
                    $addFileActivityLogs=array();
                    $addFileActivityLogs['idBooking'] = $idBooking;  
                    $addFileActivityLogs['szType'] = "SHIPPER_CONSIGNEE";  
                    $addFileActivityLogs['szDateTobeUpdated'] = $query; 
 
                    $kApi = new cApi();
                    $kApi->logBookingACtivity($addFileActivityLogs); 
                    
                    if($add_quote)
                    {
                        return $idShipperConsignee ;
                    }
                    else
                    {
                        $szSrializedData = serialize($data);

                        //adding shipper consignee data to cookie 
                        if(__ENVIRONMENT__ == "LIVE")
                        {
                            setcookie("__TRANSPORTECA_SHIPPER_CONSIGNEE_DETAILS_COOKIE__", $szSrializedData, time()+(3600*24*30),'/',$_SERVER['HTTP_HOST'],true);
                        }
                        else
                        {
                            setcookie("__TRANSPORTECA_SHIPPER_CONSIGNEE_DETAILS_COOKIE__", $szSrializedData, time()+(3600*24*90),'/');
                        } 

                        $_SESSION['iCheckForUserDetails']=''; 

                        $userSessionData = array();
                        $userSessionData = get_user_session_data();
                        $idCustomer = $userSessionData['idUser']; 

                        $kUser=new cUser();
                        $iBookingLanguage = getLanguageId();                                
                        if(((int)$idCustomer<=0))
                        {  
                            $iAddNewUser = false;
                            if($kUser->isUserEmailExist($this->szBillingEmail))
                            {  
                                //New Line Added 15/03/2015
                                $idCustomer = $kUser->idIncompleteUser ;

                                //This session is available only for booking process   
                                $kUser->manualLogin($idCustomer);
                                $_SESSION['logged_in_by_booking_process'] = 1 ;
                            }
                            else
                            {
                                $iAddNewUser = true;
                            } 
                            if($iAddNewUser)
                            { 
                                $szPassword = mt_rand(0,99)."".$this->generateUniqueToken(8);

                                //User is not loogedin then we create a new profile
                                $addUserAry = array();
                                $addUserAry['szFirstName'] = $this->szBillingFirstName ;
                                $addUserAry['szLastName'] = $this->szBillingLastName ;
                                $addUserAry['szEmail'] = $this->szBillingEmail ;
                                $addUserAry['szPassword'] = $szPassword ;
                                $addUserAry['szConfirmPassword'] = $szPassword ;
                                $addUserAry['szPostCode'] = $this->szBillingPostcode ;
                                $addUserAry['szCity'] = $this->szBillingCity ; 
                                $addUserAry['szState'] = $this->szBillingState ; 
                                $addUserAry['szCountry'] = $this->idBillingCountry ; 
                                $addUserAry['szPhoneNumberUpdate'] = $this->szBillingPhone ;
                                $addUserAry['szCompanyName'] = $this->szBillingCompanyName ;  
                                $addUserAry['szAddress1'] = $this->szBillingAddress ;
                                $addUserAry['szCurrency'] = $postSearchAry['idCurrency'];
                                $addUserAry['iThisIsMe'] = $data['iThisIsMe'];
                                $addUserAry['iConfirmed'] = 0; 
                                $addUserAry['iFirstTimePassword'] = 1;  
                                $addUserAry['idInternationalDialCode'] = $this->idBillingCountry ;
                                $addUserAry['iLanguage'] = $iBookingLanguage;

                                if($kUser->createAccount($addUserAry,'BOOKING_PROCESS'))
                                { 
                                    $idCustomer = $kUser->id ;  
                                    $kUser->manualLogin($idCustomer);

                                    $this->updateCapsuleCrmDetails($idCustomer);

                                    $_SESSION['logged_in_by_booking_process'] = 1 ; 
                                } 
                            }
                        }
                            
                        if((!$iAddNewUser && $data['iThisIsMe']==1) || ($_SESSION['logged_in_by_booking_process']==1))
                        {  
                                $kUser->getUserDetails($idCustomer);
                                
                                $updateUserDetailsAry = array(); 
                                if($iPrivateShipping==1)
                                { 
                                    $updateUserDetailsAry['szCompanyName'] = $this->szBillingFirstName." ".$this->szBillingLastName ;  
                                    $updateUserDetailsAry['szCompanyRegNo'] = 'Private';
                                    $updateUserDetailsAry['iPrivate'] = 1;
                                }
                                else
                                {
                                    $updateUserDetailsAry['szCompanyName'] = $this->szBillingCompanyName ;  
                                } 
                                $updateUserDetailsAry['szFirstName'] = $this->szBillingFirstName ;
                                $updateUserDetailsAry['szLastName'] = $this->szBillingLastName ; 
                                $updateUserDetailsAry['szPostCode'] = $this->szBillingPostcode ;
                                $updateUserDetailsAry['szCity'] = $this->szBillingCity ; 
                                $updateUserDetailsAry['szState'] = $this->szBillingState ;   
                                
                                $updateUserDetailsAry['szAddress'] = $this->szBillingAddress ;
                                $updateUserDetailsAry['szCurrency'] = $postSearchAry['idCurrency']; 
                                $updateUserDetailsAry['szPhone'] = $this->szBillingPhone;
                                $updateUserDetailsAry['idInternationalDialCode'] = $this->idBillingDialCode ;
                                $updateUserDetailsAry['idCountry'] = $this->idBillingCountry ; 
                                $updateUserDetailsAry['iLanguage'] = $iBookingLanguage;
                                
                                if($kUser->iConfirmed!=1)
                                {
                                    $updateUserDetailsAry['szEmail'] = $this->szBillingEmail ; 
                                }  
                                if(!empty($updateUserDetailsAry))
                                {
                                    $update_user_query = '';
                                    foreach($updateUserDetailsAry as $key=>$value)
                                    {
                                        $update_user_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                                    }
                                } 
                                $update_user_query = rtrim($update_user_query,",");  
                                if($this->updateUserDetailsByQuotes($update_user_query,$idCustomer))
                                {
                                    //$this->updateCapsuleCrmDetails($idCustomer);
                                }  
                            }  
                            $BookingFileName = __APP_PATH_ROOT__."/logs/customerBookingFile.log";
                             
                            $kUser->getUserDetails($idCustomer);
                            $idBookingFile = 0;
                            if($postSearchAry['iSmallShipment']==1 && $postSearchAry['idFile']<=0)
                            {
                                $createBookingFileAry = array();
                                $createBookingFileAry['idBooking'] = $idBooking ;
                                $createBookingFileAry['idUser'] = $idCustomer;   
                                $createBookingFileAry['szFileStatus'] = 'S120';
                                $createBookingFileAry['szTaskStatus'] = 'T160';  
                                $createBookingFileAry['idCustomerOwner'] = $kUser->idCustomerOwner ;
                                $createBookingFileAry['idFileOwner'] = $kUser->idCustomerOwner ;
                                $createBookingFileAry['iSearchType'] = 1;
                                 
                                $kRegisterShipCon = new cRegisterShipCon(); 
                                $kRegisterShipCon->addBookingFile($createBookingFileAry);
                                $idBookingFile = $kRegisterShipCon->idBookingFile ; 
                            } 
                            
                            $registShipperAry = array();			
                            $registShipperAry['szCompanyName'] = $this->szShipperCompanyName ;
                            $registShipperAry['szFirstName'] = $this->szShipperFirstName ;
                            $registShipperAry['szLastName'] = $this->szShipperLastName ;
                            $registShipperAry['idCustomer'] = $idCustomer;
                            $registShipperAry['szCity'] = $this->szShipperCity ;
                            $registShipperAry['szProvince'] = $this->szShipperState ;
                            $registShipperAry['szCountry'] = $this->idShipperCountry ;
                            $registShipperAry['szPhoneNumberUpdate'] = $this->szShipperPhone;
                            $registShipperAry['iInternationalDialCode'] = $this->idShipperDialCode;
                            $registShipperAry['szAddress1'] = $this->szShipperAddress ;
                            $registShipperAry['szAddress2'] = $this->szShipperAddress2 ;
                            $registShipperAry['szAddress3'] = $this->szShipperAddress3 ;
                            $registShipperAry['szPostCode'] = $this->szShipperPostcode ;
                            $registShipperAry['szEmail'] = $this->szShipperEmail ;
                            $registShipperAry['idGroup'] = 1 ; 

                            $kRegisterShipCon = new cRegisterShipCon(); 
                            $kRegisterShipCon->getShipperConsignessDetail(false,$this->szShipperCompanyName,$idCustomer);
                            if((int)$_SESSION['booking_register_shipper_id']>0 || $kRegisterShipCon->id>0)
                            {
                                if($_SESSION['booking_register_shipper_id']>0)
                                {
                                    $registShipperAry['idShipperConsigness'] = $_SESSION['booking_register_shipper_id'] ;
                                }
                                else
                                {
                                    $registShipperAry['idShipperConsigness'] = $kRegisterShipCon->id ;
                                } 
                                $this->updateRegisterShipperConsigness($registShipperAry,true);
                            }
                            else
                            { 
                                $this->addRegisterShipperConsigness($registShipperAry,true,true);
                            }	 		

                            $registShipperAry = array();			
                            $registShipperAry['szCompanyName'] = $this->szConsigneeCompanyName ;
                            $registShipperAry['szFirstName'] = $this->szConsigneeFirstName ;
                            $registShipperAry['szLastName'] = $this->szConsigneeLastName ;
                            $registShipperAry['idCustomer'] = $idCustomer;
                            $registShipperAry['szCity'] = $this->szConsigneeCity ;
                            $registShipperAry['szProvince'] = $this->szConsigneeState ;
                            $registShipperAry['szCountry'] = $this->idConsigneeCountry ;
                            $registShipperAry['szPhoneNumberUpdate'] = $this->szConsigneePhone;
                            $registShipperAry['iInternationalDialCode'] = $this->idShipperDialCode;
                            $registShipperAry['szAddress1'] = $this->szConsigneeAddress ;
                            $registShipperAry['szAddress2'] = $this->szConsigneeAddress2 ;
                            $registShipperAry['szAddress3'] = $this->szConsigneeAddress3 ;
                            $registShipperAry['szPostCode'] = $this->szConsigneePostcode ;
                            $registShipperAry['szEmail'] = $this->szConsigneeEmail ;
                            $registShipperAry['idGroup'] = 2;

                            $kRegisterShipCon = new cRegisterShipCon(); 
                            $kRegisterShipCon->getShipperConsignessDetail(false,$this->szConsigneeCompanyName,$idCustomer);

                            if((int)$_SESSION['booking_register_consignee_id']>0 || $kRegisterShipCon->id>0)
                            {
                                if($_SESSION['booking_register_consignee_id']>0)
                                {
                                    $registShipperAry['idShipperConsigness'] = $_SESSION['booking_register_consignee_id'];
                                }
                                else
                                {
                                    $registShipperAry['idShipperConsigness'] = $kRegisterShipCon->id;
                                }			 
                                $this->updateRegisterShipperConsigness($registShipperAry,true);
                            }
                            else
                            { 
                                $this->addRegisterShipperConsigness($registShipperAry,true,true);
                            } 
                              
                            $kBooking = new cBooking();
                            $res_ary = array();				
                            $res_ary['idShipperConsignee'] = $idShipperConsignee; 
                            $res_ary['iShipperConsignee'] = $this->iShipperConsignee; 
                            $res_ary['idUser'] = $kUser->id ;
                            $res_ary['szFirstName'] = $kUser->szFirstName ;
                            $res_ary['szLastName'] = $kUser->szLastName ;
                            $res_ary['szEmail'] = $kUser->szEmail ;  
                            $res_ary['szCustomerCompanyName'] = $kUser->szCompanyName ;
                            $res_ary['szCustomerAddress1'] = $kUser->szAddress ;
                            $res_ary['szCustomerCity'] = $kUser->szCity ;
                            $res_ary['szCustomerPostCode'] = $kUser->szPostCode ;
                            $res_ary['szCustomerPhoneNumber'] = $kUser->szPhone ;
                            $res_ary['idCustomerDialCode'] = $kUser->idInternationalDialCode ; 
                            $res_ary['szCargoDescription'] = utf8_encode($this->szCargoCommodity);
                            $res_ary['iBookingLanguage'] = $iBookingLanguage; 
                            if($idBookingFile>0)
                            {
                                $res_ary['idFile'] = $idBookingFile;
                            } 
                            if((int)$postSearchAry['iBookingStep']<11)
                            {
                                $res_ary['iBookingStep'] = 11 ;
                            }
                            if(!empty($res_ary))
                            {
                                foreach($res_ary as $key=>$value)
                                {
                                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                                }
                            }
                            $update_query = rtrim($update_query,","); 

                            $kBooking->updateDraftBooking($update_query,$idBooking) ;

                            $cargoAry = array();
                            $cargoAry['szCargoCommodity'] = $this->szCargoCommodity;
                            $cargoAry['idCargo'] = $this->idCargo ;
                            $cargoAry['idBooking'] = $idBooking ;
                            $this->addUpdateCargoCommodity($cargoAry); 
                            return true;
                        }
                    }
		}
	} 
        
        function updateCapsuleCrmDetails($idCustomer,$szCustomerHistoryNotes=false,$bDonotAddNotes=false,$admin_flag=false)
        { 
            /*
             * From now we don't register anything for Capsule CRM @Ajay 15-12-2016
             */
            return false;
            if($idCustomer>0)
            { 
                $kCapsuleCrm = new cCapsuleCrm();
                $kUser = new cUser();
                $kUser->getUserDetails($idCustomer);
                 
                $kConfig_new = new cConfig();
                $kConfig_new->loadCountry($kUser->szCountry);
                $szCountryName = $kConfig_new->szCountryName; 

                $kConfig_new->loadCountry($kUser->idInternationalDialCode);
                $iInternationDialCode = $kConfig_new->iInternationDialCode; 
            
                $szStreetAddress = $kUser->szAddress1 ;
                if(!empty($kUser->szAddress2))
                {
                    $szStreetAddress .= " ".$kUser->szAddress2 ;
                }
                if(!empty($kUser->szAddress3))
                {
                    $szStreetAddress .= " ".$kUser->szAddress3 ;
                }
                $szPhoneNumber = "+".$iInternationDialCode." ".$kUser->szPhoneNumber;
 
                $capsuleSearchAry = array();
                $addCapsuleDetailsAry = array();
                
                $capsuleSearchAry['dtDateTime'] = date('Y-m-d');
                $capsuleSearchAry['idUser'] = $kUser->id ;
                $capsuleSearchAry['iDataType'] = 1 ;
                
                $capsuleSearchResAry = array();
                $capsuleSearchResAry = $kCapsuleCrm->getAllUnPostedDataForCronjob($capsuleSearchAry);
                
                if(!empty($capsuleSearchResAry))
                {
                    $addCapsuleDetailsAry['id'] = $capsuleSearchResAry[0]['id'] ;
                } 
                $szCapsulePrtyUserID = $kUser->szCapsulePrtyID ;
                
                $addCapsuleDetailsAry['idUser'] = $kUser->id ;
                $addCapsuleDetailsAry['szFirstName'] = $kUser->szFirstName ;
                $addCapsuleDetailsAry['szLastName'] = $kUser->szLastName ;
                $addCapsuleDetailsAry['szCompanyName'] = $kUser->szCompanyName ;
                $addCapsuleDetailsAry['szAddress'] = $szStreetAddress ;
                $addCapsuleDetailsAry['szPhone'] = $szPhoneNumber ;
                $addCapsuleDetailsAry['szEmail'] = $kUser->szEmail ;
                $addCapsuleDetailsAry['szCity'] = $kUser->szCity ;
                $addCapsuleDetailsAry['szPostCode'] = $kUser->szPostCode ;
                $addCapsuleDetailsAry['szState'] = $kUser->szState ;
                $addCapsuleDetailsAry['szCountryName'] = $szCountryName ;
                $addCapsuleDetailsAry['iDataType'] = 1 ; 
                     
                $kCapsuleCrm->addCapsuleCrmDataFroCronjob($addCapsuleDetailsAry);
                 
                /*if(!empty($szCapsulePrtyUserID))
                {
                    if($admin_flag)
                    {
                        $szCapsuleNotes = "Updated profile by management on ".date('d/m/Y H:i:s'); 
                    }
                    else
                    {
                        $szCapsuleNotes = "Updated profile on ".date('d/m/Y H:i:s'); 
                    }
                    if($szCustomerHistoryNotes)
                    {
                        $szCapsuleNotes = $szCapsuleNotes. "\n\n".$szCustomerHistoryNotes ;
                    }
                    //$this->updateCapsuleHistoryNotes($szCapsuleNotes,$idCustomer,2);
                }
                else
                {
                    $szCapsuleNotes = "Created profile on ".date('d/m/Y H:i:s');
                    if($szCustomerHistoryNotes)
                    {
                        $szCapsuleNotes = $szCapsuleNotes. "\n".$szCustomerHistoryNotes ;
                    } 
                    //$this->updateCapsuleHistoryNotes($szCapsuleNotes,$idCustomer,1);
                }*/ 
            }
        }
        
        function updateCapsuleHistoryNotes($szCapsuleNotes,$idCustomer,$iCreateTask=false)
        {  
            /*
            * From now we don't register anything for Capsule CRM @Ajay 15-12-2016
            */
            return false;
            if(!empty($szCapsuleNotes))
            {
                $addCapsuleDetailsAry = array(); 
                $addCapsuleDetailsAry['idUser'] = $idCustomer ; 
                $addCapsuleDetailsAry['iDataType'] = 2 ; 
                $addCapsuleDetailsAry['szNotes'] = $szCapsuleNotes ; 
                $addCapsuleDetailsAry['iCreateTask'] = $iCreateTask ; 
                
                     
                $kCapsuleCrm = new cCapsuleCrm();
                $kCapsuleCrm->addCapsuleCrmDataFroCronjob($addCapsuleDetailsAry);
            } 
        }
        
	function addBookingQuotesContactDetails($data,$idBooking,$mode,$bApiLogin=false,$bPartnerApi=false)
	{
            if(!empty($data))
            {
                $kBooking = new cBooking();
                $postSearchAry = array();
                $postSearchAry = $kBooking->getBookingDetails($idBooking);
                $iPalletType = $postSearchAry['iPalletType'];
                $idServiceType = $postSearchAry['idServiceType']; 
                $iFacebookLogin='0';
                $iGoogleLogin='0';
                if($bApiLogin)
                {
                    $required_flag = false;
                    $bCompanyRequired = false;
                    
                    $iFacebookLogin=$data['iFaceBookLogin'];
                    $iGoogleLogin=$data['iGoogleLogin']; 
                }
                else
                {
                    $required_flag = true;
                } 
                if($required_flag)
                {
                    if($data['iPrivateShipping']==1)
                    {
                        $bCompanyRequired = false;
                        $data['szShipperCompanyName'] = $data['szShipperFirstName'].' '.$data['szShipperLastName'];
                    }
                    else
                    {
                        $bCompanyRequired = true;
                    } 
                }
                
                $this->set_szShipperCompanyName(trim(sanitize_all_html_input($data['szShipperCompanyName'])),$bCompanyRequired);
                $this->set_szShipperFirstName(trim(sanitize_all_html_input($data['szShipperFirstName'])),$required_flag);
                $this->set_szShipperLastName(trim(sanitize_all_html_input($data['szShipperLastName'])),$required_flag);
                $this->set_szShipperEmail(trim(sanitize_all_html_input($data['szShipperEmail'])),true);				
                $this->set_szShipperPhone(trim(sanitize_all_html_input($data['szShipperPhone'])),$required_flag);	 
                $this->set_idShipperDialCode(trim(sanitize_all_html_input($data['idShipperDialCode'])),$required_flag);  
                $this->set_iFaceBookLogin(trim(sanitize_all_html_input($data['iFaceBookLogin'])));
                $this->set_iGoogleLogin(trim(sanitize_all_html_input($data['iGoogleLogin'])));
                
                if($this->error==true)
                {
                    return false;
                }  
                
                if($bPartnerApi)
                {
                    $idCustomerSession = cPartner::$idPartnerCustomer;
                }
                else
                {
                    $idCustomerSession = $_SESSION['user_id'];
                }
                $szShipperCompanyName = $this->szShipperCompanyName ;
                $szShipperFirstName = $this->szShipperFirstName ;
                $szShipperLastName = $this->szShipperLastName ;
                $szShipperEmail = $this->szShipperEmail ;
                $szShipperPhone = $this->szShipperPhone ;
                $idShipperDialCode = $this->idShipperDialCode ;
                 
                $iLanguage = getLanguageId(); 

                $kConfig = new cConfig(); 
                $countryAry = array();
                $countryAry[0] =  $postSearchAry['idOriginCountry'];
                $countryAry[1] =  $postSearchAry['idDestinationCountry'];

                $countryCodeAry = array();
                $countryCodeAry = $kConfig->getAllCountryInKeyValuePair(false,false,$countryAry,$iLanguage);

                $szOriginCountryCode = $countryCodeAry[$postSearchAry['idOriginCountry']]['szCountryISO'] ;
                $szDestinationCountryCode = $countryCodeAry[$postSearchAry['idDestinationCountry']]['szCountryISO'] ;

                $szOrigingCountry = $countryCodeAry[$postSearchAry['idOriginCountry']]['szCountryName'] ;
                $szDestinationCountry = $countryCodeAry[$postSearchAry['idDestinationCountry']]['szCountryName'] ;

                $kUser = new cUser();
                if((int)$idCustomerSession>0)
                {
                    $idCustomer = $idCustomerSession ; 
                    $kUser->getUserDetails($idCustomerSession); 
                    
                    $kConfig = new cConfig();
                    $kConfig->loadCountry($kUser->szCountry); 
                    $szUserCountryName = $kConfig->szCountryISO ; 
                }
                else
                {
                    /* We are no longer taking country code from Cookie
                    if(!empty($_COOKIE['__USER_COUNTRY_NAME___']))
                    {
                        $szUserCountryName = $_COOKIE['__USER_COUNTRY_NAME___'] ;
                    }
                    else
                    {
                        $userIpDetailsAry = array();
                        $userIpDetailsAry = getCountryCodeByIPAddress(); 
                        $szUserCountryName  = $userIpDetailsAry['szCountryCode'] ;
                    } 
                     * if(empty($szUserCountryName))
                    {
                        $kConfig = new cConfig();
                        $kConfig->loadCountry($idShipperDialCode); 
                        $szUserCountryName = $kConfig->szCountryISO ;
                    }  
                     * 
                     */
                    $kConfig = new cConfig();
                    $kConfig->loadCountry($idShipperDialCode); 
                    $szUserCountryName = $kConfig->szCountryISO ; 
                }
                
                $this->iAddShipperConsigneeFlag = false;
                $this->iShipperFlag = false;
                $this->iConsigneeFlag = false;
                   
                if($mode=='VOGUE_BOOKING')
                {
                    if($data['iDeliveryAddress']==1)
                    {
                        
                    }
                    else
                    {
                        
                    }
                }
                else
                {
                     //echo "User: ".$szUserCountryName." ORIG: ".$szOriginCountryCode." DEST: ".$szDestinationCountryCode;
                    if($szUserCountryName==$szOriginCountryCode && $szUserCountryName==$szDestinationCountryCode)
                    { 
                        $this->iAddShipperConsigneeFlag = true;
                        $this->iShipperFlag=true;
                        $this->iConsigneeFlag=true;
                        $iShipperConsignee = 1;
                    }
                    else if($szUserCountryName==$szOriginCountryCode)
                    {
                        $this->iAddShipperConsigneeFlag = true;
                        $this->iShipperFlag=true;
                        $this->iShipperConsignee = 1;
                        $iShipperConsignee = 1;
                    }
                    else if($szUserCountryName==$szDestinationCountryCode)
                    {
                        $this->iAddShipperConsigneeFlag = true;
                        $this->iConsigneeFlag=true;
                        $this->iShipperConsignee = 2;
                        $iShipperConsignee = 2;
                    } 
                    else
                    {
                        $iShipperConsignee = 3;
                        $this->iShipperConsignee = 3;
                        $this->iAddShipperConsigneeFlag = true;
                    }
                } 
                 
                $replaceAry = array(" ",",");
                $szCountryStrKey = str_replace($replaceAry, "-", $postSearchAry['szOriginCountry']); 
                if(!empty($_COOKIE[$szCountryStrKey]))
                {
                    $origionGeoCountryAry = unserialize($_COOKIE[$szCountryStrKey]); 
                }
                else
                {
                    //Fetching details of Origin location from google
                    $origionGeoCountryAry = array();
                    $origionGeoCountryAry = reverse_geocode($postSearchAry['szOriginCountry'],true); 
                }
                
                $szShipperAddressGoe = trim($origionGeoCountryAry['szStreetNumber']);
 
                $postcodeCheckAry=array();
                $postcodeResultAry = array();
                
                //Fetching postcode from tblpostcode for +/- 10KM of co-ordinate returned by google
                $postcodeCheckAry['idCountry'] = $postSearchAry['idOriginCountry'] ;
                $postcodeCheckAry['szLatitute'] = $origionGeoCountryAry['szLat'] ;
                $postcodeCheckAry['szLongitute'] = $origionGeoCountryAry['szLng'] ;
                $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry);
                    
                $iGotDataFromDB = false;
                if(!empty($postcodeResultAry))
                {
                    if(empty($origionGeoCountryAry['szCityName']))
                    {
                        $origionGeoCountryAry['szCityName'] = $postcodeResultAry['szCity'];
                    }
                    if(empty($origionGeoCountryAry['szPostCode']))
                    {
                        //$origionGeoCountryAry['szPostCode'] = $postcodeResultAry['szPostCode'];
                        //$iGotDataFromDB = true;
                    }
                } 
                
                $szCountryStrKey = str_replace($replaceAry, "-", $postSearchAry['szDestinationCountry']); 
                if(!empty($_COOKIE[$szCountryStrKey]))
                {
                    $destinationGeoCountryAry = unserialize($_COOKIE[$szCountryStrKey]); 
                }
                else
                {
                    //Fetching details of Origin location from google
                    $destinationGeoCountryAry = array();
                    $destinationGeoCountryAry = reverse_geocode($postSearchAry['szDestinationCountry'],true);
                }  
                $postcodeCheckAry=array();
                $postcodeResultAry = array();

                $postcodeCheckAry['idCountry'] = $postSearchAry['idDestinationCountry'] ;
                $postcodeCheckAry['szLatitute'] = $destinationGeoCountryAry['szLat'] ;
                $postcodeCheckAry['szLongitute'] = $destinationGeoCountryAry['szLng'] ;
                $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry);
                $iGotDataFromDB_dest = false;
                if(!empty($postcodeResultAry))
                {
                    if(empty($destinationGeoCountryAry['szCityName']))
                    {
                        $destinationGeoCountryAry['szCityName'] = trim($postcodeResultAry['szCity']);
                    }
                    if(empty($destinationGeoCountryAry['szPostCode']))
                    {
                        //$destinationGeoCountryAry['szPostCode'] = trim($postcodeResultAry['szPostCode']);
                        //$iGotDataFromDB_dest = true;
                    }
                }
                
                $idShipperConsignee = 0;
                if($this->iAddShipperConsigneeFlag)
                {   
                    if(!empty($szShipperAddressGoe) && !empty($origionGeoCountryAry['szRoute']))
                    {
                        $szShipperAddressGoe = ", ".$origionGeoCountryAry['szRoute'] ;
                    }
                    else
                    {
                        $szShipperAddressGoe = $origionGeoCountryAry['szRoute'] ;
                    }

                    if(!empty($szShipperAddressGoe))
                    {
                        $shipperConsigneeAry['iGetShipperAddressFromGoogle'] = 1 ;
                        $shipperConsigneeAry['szShipperAddress'] = $szShipperAddressGoe ;
                    }
                    if(!empty($origionGeoCountryAry['szCityName']))
                    {
                        $shipperConsigneeAry['szShipperCity'] = $origionGeoCountryAry['szCityName'];
                        $shipperConsigneeAry['iGetShipperCityFromGoogle'] = 1 ;
                    }
                    if(!empty($origionGeoCountryAry['szPostCode']))
                    {
                        $shipperConsigneeAry['szShipperPostcode'] = $origionGeoCountryAry['szPostCode']; 

                        if(!$iGotDataFromDB)
                        {
                            $shipperConsigneeAry['iGetShipperPostcodeFromGoogle'] = 1 ;		  
                        } 
                    } 
                    $shipperConsigneeAry['szShipperLatitute'] = $origionGeoCountryAry['szLat']; 
                    $shipperConsigneeAry['szShipperLongitute'] = $origionGeoCountryAry['szLng'];		
                    $shipperConsigneeAry['idShipperDialCode'] = $postSearchAry['idOriginCountry'];
                    $shipperConsigneeAry['idShipperCountry'] = $postSearchAry['idOriginCountry']; 

                    $szOriginCity = $shipperConsigneeAry['szShipperCity'] ;
                    $szOriginPostcode = $shipperConsigneeAry['szShipperPostcode'] ;
                    
                    $this->szShipperAddress = $shipperConsigneeAry['szShipperAddress'];
                    $this->szShipperPostcode = $shipperConsigneeAry['szShipperPostcode'];
                    $this->szShipperCity = $shipperConsigneeAry['szShipperCity'];
                    $this->szShipperState = $shipperConsigneeAry['szShipperState'];
                    $this->idShipperCountry = $shipperConsigneeAry['idShipperCountry'];
                    $this->idShipperDialCode = $shipperConsigneeAry['idShipperDialCode'];

                    $this->szShipperAddress_pickup = $shipperConsigneeAry['szShipperAddress'];
                    $this->szShipperPostcode_pickup = $shipperConsigneeAry['szShipperPostcode'];
                    $this->szShipperCity_pickup = $shipperConsigneeAry['szShipperCity'];
                    $this->szShipperState_pickup = $shipperConsigneeAry['szShipperState'];
                    $this->idShipperCountry_pickup = $shipperConsigneeAry['idShipperCountry'];
                    
                    if($this->iShipperFlag)
                    {
                        $szBillingPostcode = $shipperConsigneeAry['szShipperPostcode'];
                        $szBillingCity = $shipperConsigneeAry['szShipperCity'];
                        $szBillingAddress = $shipperConsigneeAry['szShipperAddress'];
                    }
                    else
                    {
                        $this->szShipperCompanyName = '';
                        $this->szShipperFirstName = '' ;
                        $this->szShipperLastName = '' ;
                        $this->szShipperEmail = '' ;
                        $this->szShipperPhone = '' ;
                        $this->idShipperDialCode = $postSearchAry['idOriginCountry'] ;
                    }

                    $fetchAddressFromGoogle = true ; 
 
                    $szShipperAddressGoe = trim($destinationGeoCountryAry['szStreetNumber']);

                    if(!empty($szShipperAddressGoe) && !empty($destinationGeoCountryAry['szRoute']))
                    {
                        $szShipperAddressGoe .= ", ".$destinationGeoCountryAry['szRoute'] ;
                    }
                    else
                    {
                        $szShipperAddressGoe .= $destinationGeoCountryAry['szRoute'] ;
                    }

                    $shipperConsigneeAry['iGetConsigneeAddressFromGoogle'] = 0;
                    $shipperConsigneeAry['iGetConsigneePostcodeFromGoogle'] = 0;
                    $shipperConsigneeAry['iGetConsigneeCityFromGoogle'] = 0;

                    if(!empty($szShipperAddressGoe))
                    {
                        $shipperConsigneeAry['szConsigneeAddress'] = $szShipperAddressGoe ;
                        $shipperConsigneeAry['iGetConsigneeAddressFromGoogle'] = 1 ;
                    }
                    if(!empty($destinationGeoCountryAry['szPostCode']))
                    {
                        $shipperConsigneeAry['szConsigneePostCode'] = $destinationGeoCountryAry['szPostCode'];
                        if(!$iGotDataFromDB_dest)
                        {
                            $shipperConsigneeAry['iGetConsigneePostcodeFromGoogle'] = 1 ;
                        }
                    }
                    if(!empty($destinationGeoCountryAry['szCityName']))
                    {
                        $shipperConsigneeAry['szConsigneeCity'] = $destinationGeoCountryAry['szCityName']; 
                        $shipperConsigneeAry['iGetConsigneeCityFromGoogle'] = 1 ;
                    }  

                    $szDestinationCity = $shipperConsigneeAry['szConsigneeCity'];
                    $szDestinationPostcode = $shipperConsigneeAry['szConsigneePostCode'];
                    
                    $shipperConsigneeAry['szConsigneeLatitute'] = $destinationGeoCountryAry['szLat']; 
                    $shipperConsigneeAry['szConsigneeLongitute'] = $destinationGeoCountryAry['szLng']; 

                    $shipperConsigneeAry['idConsigneeDialCode'] = $postSearchAry['idDestinationCountry']; 
                    $shipperConsigneeAry['idConsigneeCountry'] = $postSearchAry['idDestinationCountry']; 
 
                    $this->szConsigneeAddress = $shipperConsigneeAry['szConsigneeAddress'];
                    $this->szConsigneePostcode = $shipperConsigneeAry['szConsigneePostCode'];
                    $this->szConsigneeCity = $shipperConsigneeAry['szConsigneeCity'];
                    $this->szConsigneeState = $shipperConsigneeAry['szConsigneeState'];
                    $this->idConsigneeCountry = $shipperConsigneeAry['idConsigneeCountry'];
                    $this->idConsigneeDialCode = $shipperConsigneeAry['idConsigneeDialCode'];

                    $this->szConsigneeAddress_pickup = $shipperConsigneeAry['szConsigneeAddress'];
                    $this->szConsigneePostcode_pickup = $shipperConsigneeAry['szConsigneePostCode'];
                    $this->szConsigneeCity_pickup = $shipperConsigneeAry['szConsigneeCity'];
                    $this->szConsigneeState_pickup = $shipperConsigneeAry['szConsigneeState'];
                    $this->idConsigneeCountry_pickup = $shipperConsigneeAry['idConsigneeCountry'];
                         
                    if($this->iConsigneeFlag)
                    { 
                        $this->szConsigneeCompanyName = $szShipperCompanyName;
                        $this->szConsigneeFirstName = $szShipperFirstName ;
                        $this->szConsigneeLastName = $szShipperLastName ;
                        $this->szConsigneeEmail = $szShipperEmail ;
                        $this->szConsigneePhone = $szShipperPhone ;
                        $this->idConsigneeDialCode = $idShipperDialCode ;
                        
                        $szBillingPostcode = $shipperConsigneeAry['szConsigneePostCode'];
                        $szBillingCity = $shipperConsigneeAry['szConsigneeCity'];
                        $szBillingAddress = $shipperConsigneeAry['szConsigneeAddress'];
                    }   
                    else
                    {
                        $this->szConsigneeCompanyName = "";
                        $this->szConsigneeFirstName = "" ;
                        $this->szConsigneeLastName = "" ;
                        $this->szConsigneeEmail = "" ;
                        $this->szConsigneePhone = "" ;
                        $this->idConsigneeDialCode = $postSearchAry['idDestinationCountry'] ; ;
                    } 
                    $idShipperConsignee = $this->addShipperConsignee_new($shipperConsigneeAry,$idBooking,true); 
                } 
                else
                {
                    $szOriginCity = $origionGeoCountryAry['szCityName'];
                    $szOriginPostcode = $origionGeoCountryAry['szPostCode'] ;
                     
                    $szDestinationCity = $destinationGeoCountryAry['szCityName'];
                    $szDestinationPostcode = $destinationGeoCountryAry['szPostCode']; 
                } 
                
                $kUser = new cUser();
                $iAddNewUser = false;

                $loggedInByBookingProcess = false;
                if((int)$idCustomerSession<=0)
                {
                    if($kUser->isUserEmailExist($szShipperEmail))
                    { 
                        /*if($kUser->iIncompleteProfileExists)
                        {
                            //If user email already exists with the existing email then we just deactivate that account and create a new one.
                            $kUser->deactivateUserAccount($kUser->idIncompleteUser);
                            $iAddNewUser = true; 
                        }
                        else
                        {
                            //give an error that email is already exists
                            $idCustomer = $kUser->idIncompleteUser ;

                            //This session is available only for booking process   
                            $kUser->manualLogin($idCustomer,$iFacebookLogin,$iGoogleLogin);
                            $_SESSION['logged_in_by_booking_process'] = 1 ; 
                        }*/
                        //New Line Added 15/03/2015
                        $idCustomer = $kUser->idIncompleteUser ;

                        //This session is available only for booking process   
                        $kUser->manualLogin($idCustomer,$iFacebookLogin,$iGoogleLogin);
                        $_SESSION['logged_in_by_booking_process'] = 1 ;
                        $loggedInByBookingProcess = true;
                    }
                    else
                    {
                        $iAddNewUser = true;
                    }
                }

                $kConfig = new cConfig();
                $kConfig->loadCountry($idShipperDialCode);
                $iInternationDialCode = $kConfig->iInternationDialCode;

                $dtTimingDate = date('d. M, Y',strtotime($postSearchAry['dtTimingDate']));
                $fTotalVolume = format_volume($postSearchAry['fCargoVolume']);
                $fTotalWeight = number_format((float)$postSearchAry['fCargoWeight']);
                
                $idBookingFile = $postSearchAry['idFile'];
                
                $szCustomerHistoryNotes = "From: ".$postSearchAry['szOriginCountry']."\nTo: ".$postSearchAry['szDestinationCountry']." \nDate: ".$dtTimingDate." \nVolume: ".$fTotalVolume." \nWeight: ".$fTotalWeight." \n";
                
                $iLanguage = getLanguageId(); 
                
                if((int)$idCustomerSession>0)
                {
                    $idCustomer = $idCustomerSession ; 
                    $kUser->getUserDetails($idCustomer); 

                    if($bApiLogin)
                    {
                        /*
                         * When customer logs in using facebook/google then we just login user to the sytem and do not update details in tblcustomer.
                         * Because from Facebook/Google API we are not getting complete set of data
                         */
                        
                        $kRegisterShipCon = new cRegisterShipCon(); 
                        $szCustomerHistoryNotes = "RFQ:\n".$szCustomerHistoryNotes;
                        $kRegisterShipCon->updateCapsuleHistoryNotes($szCustomerHistoryNotes,$idCustomer); 
                    }
                    else
                    {
                        $updatedUserAccountDetails = false;
                        if($kUser->szCompanyName!=$szShipperCompanyName)
                        {
                            $updatedUserAccountDetails = true;
                        }
                        else if($kUser->szFirstName!=$szShipperFirstName)
                        {
                            $updatedUserAccountDetails = true;
                        }
                        else if($kUser->szLastName!=$szShipperLastName)
                        {
                            $updatedUserAccountDetails = true;
                        }
                        else if($kUser->szPhone!=$szShipperPhone)
                        {
                            $updatedUserAccountDetails = true;
                        }
                        else if($kUser->idInternationalDialCode!=$idShipperDialCode)
                        {
                            $updatedUserAccountDetails = true;
                        } 

                        $updateUserDetailsAry = array();
                        $updateUserDetailsAry['szCompanyName'] = $szShipperCompanyName;
                        $updateUserDetailsAry['szFirstName'] = $szShipperFirstName; 
                        $updateUserDetailsAry['szLastName'] = $szShipperLastName;  
                        $updateUserDetailsAry['szPhone'] = $szShipperPhone;
                        $updateUserDetailsAry['idInternationalDialCode'] = $idShipperDialCode ; 
                        $updateUserDetailsAry['iPrivate'] = $data['iPrivateShipping'];
                        $updateUserDetailsAry['iLanguage'] = $iLanguage;

                        if(empty($kUser->szCountry))
                        {
                            $updateUserDetailsAry['idCountry'] = $idShipperDialCode ;  
                        }
                        if(empty($kUser->szAddress1))
                        {
                            $updateUserDetailsAry['szAddress'] = $szBillingAddress ;
                        }
                        if(empty($kUser->szPostCode))
                        {
                            $updateUserDetailsAry['szPostCode'] = $szBillingPostcode;
                        }
                        if(empty($kUser->szCity))
                        {
                            $updateUserDetailsAry['szCity'] = $szBillingCity ; 
                        } 

                        if($kUser->iConfirmed!=1)
                        {
                            $updateUserDetailsAry['szEmail'] = $szShipperEmail;
                        } 
                        if(!empty($updateUserDetailsAry))
                        {
                            $update_user_query = '';
                            foreach($updateUserDetailsAry as $key=>$value)
                            {
                                $update_user_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                            }
                        } 
                        $update_user_query = rtrim($update_user_query,",");  
                        //echo " <br><br> ".$update_user_query;
                        if($this->updateUserDetailsByQuotes($update_user_query,$idCustomer))
                        { 
                            if($updatedUserAccountDetails)
                            {
                                $this->updateCapsuleCrmDetails($idCustomer,$szCustomerHistoryNotes);
                            }
                            else
                            { 
                                $kRegisterShipCon = new cRegisterShipCon(); 
                                $szCustomerHistoryNotes = "RFQ:\n".$szCustomerHistoryNotes;
                                $kRegisterShipCon->updateCapsuleHistoryNotes($szCustomerHistoryNotes,$idCustomer); 
                            }
                        } 
                    } 
                } 
                else
                { 
                    $szPassword = mt_rand(0,99)."".$this->generateUniqueToken(8); 

                    //User is not loogedin then we create a new profile
                    $addUserAry = array();
                    $addUserAry['szFirstName'] = $szShipperFirstName ;
                    $addUserAry['szLastName'] = $szShipperLastName ;
                    $addUserAry['szEmail'] = $szShipperEmail ;
                    $addUserAry['szPassword'] = $szPassword ;
                    $addUserAry['szConfirmPassword'] = $szPassword ; 
                    $addUserAry['szPhoneNumberUpdate'] = $szShipperPhone;
                    $addUserAry['szCompanyName'] = $szShipperCompanyName ; 
                    $addUserAry['szCurrency'] = $postSearchAry['idCurrency'];
                    $addUserAry['iThisIsMe'] = $data['iThisIsMe'];
                    $addUserAry['iConfirmed'] = 0; 
                    $addUserAry['iFirstTimePassword'] = 1;   
                    $addUserAry['szCountry'] = $idShipperDialCode ;
                    $addUserAry['idInternationalDialCode'] = $idShipperDialCode ;  
                    $addUserAry['szAddress1'] = $szBillingAddress ;
                    $addUserAry['szPostCode'] = $szBillingPostcode;
                    $addUserAry['szCity'] = $szBillingCity ;  
                    $addUserAry['iPrivate'] = $data['iPrivateShipping'];                       
                    $addUserAry['iLanguage'] = $iLanguage;
                    
                    if($kUser->createAccount($addUserAry,'BOOKING_QUOTE_PROCESS',$bApiLogin))
                    { 
                        $idCustomer = $kUser->id ;  
                        $kUser->manualLogin($idCustomer,$iFacebookLogin,$iGoogleLogin);
                        $this->updateCapsuleCrmDetails($idCustomer,$szCustomerHistoryNotes);
                        $_SESSION['logged_in_by_booking_process'] = 1 ; 
                        $loggedInByBookingProcess = true;
                    }  
                } 
                
                if($data['iOfferType']==1 || $iPalletType  || $data['iOfferType']==7 || $data['iOfferType']==11)
                { 
                    $idTransportMode = __BOOKING_TRANSPORT_MODE_ROAD__;
                }
                else if($data['iOfferType']==2 || $data['iOfferType']==8 || $data['iOfferType']==10)
                { 
                    $idTransportMode = __BOOKING_TRANSPORT_MODE_AIR__ ;
                }
                else if($data['iOfferType']==3 || $data['iOfferType']==6 || $data['iOfferType']==12)
                { 
                    $idTransportMode = __BOOKING_TRANSPORT_MODE_SEA__ ;
                } 
                $kUser->getUserDetails($idCustomer); 
                
                if($loggedInByBookingProcess)
                {
                    //$kMautic = new cMautic(); 
                    //$kMautic->updateDataToMauticApi($idCustomer);
                }
                //If customer role is billing then we update customer address as billing address for the booking.
                if($iShipperConsignee==3) 
                {
                    $szBillingAddress = $kUser->szAddress1 ;
                    $szBillingPostcode = $kUser->szPostCode;
                    $szBillingCity = $kUser->szCity ;  
                } 
                $res_ary = array();
                if($mode=='GET_RATES')
                {
                    $iBookingStep = 4;
                    $res_ary['iQuotesStatus'] = 0;
                    //$res_ary['idTransportMode'] = __TRANSPORT_MODE_LCL__ ;
                    $res_ary['iBookingType'] = __BOOKING_TYPE_AUTOMATIC__;
                    
                    if(!empty($data['szCargoType']))
                    {
                        $res_ary['szCargoType'] = $data['szCargoType'];
                    }
                    
                    
                }
                else
                {
                    $iBookingStep = 7;
                    $res_ary['iQuotesStatus'] = __BOOKING_QUOTES_STATUS_NEW_BOOKING__ ;
                    $res_ary['iBookingQuotes'] = 1; 
                    $res_ary['idTransportMode'] = $idTransportMode;
                    $res_ary['iBookingType'] = __BOOKING_TYPE_RFQ__;
                    
                    if($data['iPrivateShipping']==1)
                    {
                        $res_ary['szCargoType'] = "__PERSONAL_EFFECTS__";
                    }
                } 
                
                // Previously we are only creating files for RFQ. but from now we are creating files for both Automatic bookings and Manual Quotes (RFQ) 
                $kBooking_new = new cBooking(); 
                if($mode=='GET_RATES')
                {
                    if($idBookingFile>0)
                    { 
                        $kBooking = new cBooking();
                        $dtResponseTime = $kBooking->getRealNow();
                
                        $fileLogsAry = array();
                        $fileLogsAry['idBooking'] = $idBooking ;
                        $fileLogsAry['idUser'] = $idCustomer ; 
                        $fileLogsAry['szTransportecaStatus'] = 'S120';
                        $fileLogsAry['szTransportecaTask'] = 'T160';
                        $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
                        $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime;
                        $fileLogsAry['idCustomerOwner'] = $kUser->idCustomerOwner ;
                        $fileLogsAry['idFileOwner'] = $kUser->idCustomerOwner ;
                        $fileLogsAry['iSearchType'] = 1;
                        
                        if(!empty($fileLogsAry))
                        {
                            $file_log_query = "";
                            foreach($fileLogsAry as $key=>$value)
                            {
                                $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                            }
                        } 
                        $file_log_query = rtrim($file_log_query,",");   
                        $kBooking_new->updateBookingFileDetails($file_log_query,$idBookingFile,true); 
                    }
                    else
                    {
                        $BookingFileName = __APP_PATH_ROOT__."/logs/customerBookingFile.log";
                        $strdata=array();
                        $strdata[0]=" \n\n *** Creating Booking File From addBookingQuotesContactDetails  ".date("Y-m-d H:i:s")."\n\n"; 
                        $strdata[1]="Booking Id: ".$idBooking."\n" ;
                        file_log(true, $strdata, $BookingFileName);
                        
                        $iAddBookingFile = false;
                        if(__ENVIRONMENT__=='LIVE')
                        {
                            if(doNotCreateFileForThisDomainEmail($szShipperEmail))
                            {
                                $iAddBookingFile = true;
                            }
                            else
                            {
                                $strdata=array();
                                $strdata[0]=" \n\n *** Do Not Create File For Dpmain  From addBookingQuotesContactDetails Function ".date("Y-m-d H:i:s")."\n\n"; 
                                $strdata[1]="Booking Id: ".$idBooking."\n" ;
                                $strdata[2]="Customer Id: ".$idCustomer."\n" ;
                                $strdata[3]="Email Address: ".$szShipperEmail."\n" ;
                                file_log(true, $strdata, $BookingFileName);
                            }
                        }
                        else
                        {
                            $iAddBookingFile = true;
                        }
//                        //We donot create Task in Pending tray if user email's domain is @transporteca.com or whiz-solutions.com
//                        $excludedEmailAry = array('transporteca.com','whiz-solutions.com');
//                        $szShipperEmailAry = explode("@",$szShipperEmail);
//                        $szExcludedEmailDomain = $szShipperEmailAry[1];
//                         
//                        $iAddBookingFile = false;
//                        if(__ENVIRONMENT__=='LIVE')
//                        {
//                            if(!empty($excludedEmailAry) && !in_array($szExcludedEmailDomain,$excludedEmailAry))
//                            {
//                                $iAddBookingFile = true;
//                            }
//                        }
//                        else
//                        {
//                            $iAddBookingFile = true;
//                        } 
                        
                        if($iAddBookingFile)
                        { 
                            $createBookingFileAry = array();
                            $createBookingFileAry['idBooking'] = $idBooking ;
                            $createBookingFileAry['idUser'] = $idCustomer;  
                            $createBookingFileAry['szFileStatus'] = 'S120';
                            $createBookingFileAry['szTaskStatus'] = 'T160';
                            $createBookingFileAry['idCustomerOwner'] = $kUser->idCustomerOwner ;
                            $createBookingFileAry['idFileOwner'] = $kUser->idCustomerOwner ;
                            $createBookingFileAry['iSearchType'] = 1;
                             
                            $this->addBookingFile($createBookingFileAry);
                            $idBookingFile = $this->idBookingFile ; 
                        }  
                    }
                }
                else
                {
                    if($idBookingFile>0)
                    { 
                        $kBooking = new cBooking();
                        $dtResponseTime = $kBooking->getRealNow();
                        
                        $fileLogsAry = array();
                        $fileLogsAry['idBooking'] = $idBooking ;
                        $fileLogsAry['idUser'] = $idCustomer ; 
                        $fileLogsAry['szTransportecaStatus'] = 'S1';
                        $fileLogsAry['szTransportecaTask'] = 'T1';
                        $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
                        $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime;
                        $fileLogsAry['idCustomerOwner'] = $kUser->idCustomerOwner ;
                        $fileLogsAry['idFileOwner'] = $kUser->idCustomerOwner ;
                        $fileLogsAry['iSearchType'] = 2;
                        
                        if(!empty($fileLogsAry))
                        {
                            $file_log_query = "";
                            foreach($fileLogsAry as $key=>$value)
                            {
                                $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                            }
                        } 
                        $file_log_query = rtrim($file_log_query,",");   
                        $kBooking_new->updateBookingFileDetails($file_log_query,$idBookingFile,true); 
                    }
                    else
                    {
                        $createBookingFileAry = array();
                        $createBookingFileAry['idBooking'] = $idBooking ;
                        $createBookingFileAry['idUser'] = $idCustomer ; 
                        $createBookingFileAry['szFileStatus'] = 'S1';
                        $createBookingFileAry['szTaskStatus'] = 'T1';
                        $createBookingFileAry['idCustomerOwner'] = $kUser->idCustomerOwner ;
                        $createBookingFileAry['idFileOwner'] = $kUser->idCustomerOwner ;
                        $createBookingFileAry['iSearchType'] = 2;
                         
                        $this->addBookingFile($createBookingFileAry);
                        $idBookingFile = $this->idBookingFile ;
                         
                    } 
                }  
                if($idTransportMode>0)
                {
                    $kConfig = new cConfig();
                    $transportModeListNewAry = array();
                    $transportModeListNewAry = $kConfig->getAllTransportMode($idTransportMode); 
                    
                    $szTransportMode = $transportModeListNewAry[0]['szShortName']; 
                    $szTransportModeLongName = $transportModeListNewAry[0]['szLongName']; 
                }
                $kWarehouseSearch = new cWHSSearch();
                $resultAry = $kWarehouseSearch->getCurrencyDetails(20);		

                $fDkkExchangeRate = 0;
                if($resultAry['fUsdValue']>0)
                {
                    $fDkkExchangeRate = $resultAry['fUsdValue'] ;						 
                }    
                $kBooking = new cBooking();
                $idServiceTerms = $kBooking->getServiceTerms($idServiceType,$iShipperConsignee);
                
                $res_ary['idUser'] = $idCustomer ;
                $res_ary['idShipperConsignee'] = $idShipperConsignee;  
                $res_ary['idCustomerDialCode'] = $idShipperDialCode ;
                $res_ary['szCustomerCompanyName'] = $szShipperCompanyName ;
                $res_ary['szFirstName'] = $szShipperFirstName ;
                $res_ary['szLastName'] = $szShipperLastName ;
                $res_ary['szEmail'] = $szShipperEmail ; 
                $res_ary['szCustomerPhoneNumber'] = $szShipperPhone ;   
                $res_ary['iBookingStep'] = $iBookingStep;
                //$res_ary['iQuotesStatus'] = __BOOKING_QUOTES_STATUS_NEW_BOOKING__;
                $res_ary['szCustomerAddress1'] = $szBillingAddress ;
                $res_ary['szCustomerPostCode'] = $szBillingPostcode;
                $res_ary['szCustomerCity'] = $szBillingCity ; 
                $res_ary['szCustomerCountry'] = $idShipperDialCode ;
                $res_ary['idFile'] = $idBookingFile; 
                $res_ary['szOriginCity'] = $szOriginCity;
                $res_ary['szDestinationCity'] = $szDestinationCity; 
                $res_ary['szOriginPostCode'] = $szOriginPostcode;
                $res_ary['szDestinationPostCode'] = $szDestinationPostcode;
                $res_ary['fDkkExchangeRate'] = $fDkkExchangeRate ;
                $res_ary['idTransportMode'] = $idTransportMode;
                $res_ary['szTransportMode'] = $szTransportMode;
                $res_ary['iShipperConsignee'] = $iShipperConsignee; 
                $res_ary['iPrivateShipping'] = $data['iPrivateShipping'];
                $res_ary['iBookingLanguage'] = $iLanguage; 
                $res_ary['iGetQuotePageFlag'] = 0;
                
                if($data['iFromGetOfferButton']==1)
                {
                    if($data['iOfferType']==1)
                    {
                        $szInternalComment = 'Had sea options displayed, clicked GET OFFER for road transport';
                    }
                    else if($data['iOfferType']==2)
                    {
                        $szInternalComment = 'Had sea options displayed, clicked GET OFFER for airfreight';
                    } 
                    else if($data['iOfferType']==4)
                    {
                        $szInternalComment = 'Had courier options with cartons displayed, clicked GET OFFER for shipping on pallets';
                    } 
                    else if($data['iOfferType']==5)
                    {
                        $szInternalComment = "";
                    }
                    else if($data['iOfferType']==6)
                    {
                        $szInternalComment = "Had rail options displayed, clicked GET OFFER for ocean freight";
                    }
                    else if($data['iOfferType']==7)
                    {
                        $szInternalComment = 'Had rail options displayed, clicked GET OFFER for road transport';
                    }
                    else if($data['iOfferType']==8)
                    {
                        $szInternalComment = 'Had rail options displayed, clicked GET OFFER for airfreight';
                    }
                    else if($data['iOfferType']==9)
                    {
                        $szInternalComment = 'Had rail and courier options with cartons displayed, clicked GET OFFER for shipping on pallets';
                    }
                    else if($data['iOfferType']==10)
                    {
                        $szInternalComment = 'Had sea and rail options displayed, clicked GET OFFER for airfreight';
                    }
                    else if($data['iOfferType']==11)
                    {
                        $szInternalComment = 'Had sea and rail options displayed, clicked GET OFFER for airfreight';
                    }
                    else if($data['iOfferType']==12)
                    {
                        $szInternalComment = 'Had rail and courier options displayed, clicked GET OFFER for airfreight';
                    }
                    else
                    {
                        $szInternalComment = 'Had air options displayed, clicked GET OFFER for ocean freight';
                    }
                    
                    $res_ary['szInternalComment'] = $szInternalComment; 
                    $idServiceType = $data['idServiceType']; 
                    
                    if($idServiceType==__SERVICE_TYPE_DTD__)
                    {
                        $res_ary['idServiceTerms'] = __SERVICE_TERMS_EXW__;
                    }
                    else
                    {
                        $res_ary['idServiceTerms'] = __SERVICE_TERMS_FOB__;
                    }
                }  
                if(!empty($res_ary))
                {
                    foreach($res_ary as $key=>$value)
                    {
                        $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                }
                
                $kBooking = new cBooking();
                $update_query = rtrim($update_query,",");  
                if($kBooking->updateDraftBooking($update_query,$idBooking))
                {
                     //Adding booking searh logs
                    $kBookingLog = new cBooking();
                    $postSearchAry = $kBookingLog->getBookingDetails($idBooking);  
                    $kBookingLog->insertBookingSearchLogs($postSearchAry);
                    
                    if($mode=='GET_RATES')
                    {
                        return true ;
                    }
                    else
                    { 
                        $postSearchAry = array();
                        $postSearchAry = $kBooking->getBookingDetails($idBooking);
 
                        $_SESSION['quote_booking_id'] = $idBooking;
                        setcookie("__BOOKING_QUOTE_REF_NUMBER_COOKIE__", $idBooking, time()+(3600*24*30),'/',$_SERVER['HTTP_HOST'],true);
                         
                        $replace_ary = array(); 
                        $replace_ary['szOrigingCountryStr'] = $postSearchAry['szOriginCountry'];
                        $replace_ary['szDestinationCountryStr'] = $postSearchAry['szDestinationCountry'];
                        $replace_ary['szOriginCountry']=$szOrigingCountry;
                        $replace_ary['szDestinationCountry']=$szDestinationCountry;
                        $replace_ary['dtTimingDate'] = date('d. M, Y',strtotime($postSearchAry['dtTimingDate']));
                        $replace_ary['fTotalVolume'] = format_volume($postSearchAry['fCargoVolume']);
                        $replace_ary['fTotalWeight'] = number_format((float)$postSearchAry['fCargoWeight']);

                        $replace_ary['szCustomerCompanyName'] = $szShipperCompanyName; 
                        $replace_ary['szCustomerFirstName'] = $szShipperFirstName; 
                        $replace_ary['szCustomerLastName'] = $szShipperLastName; 
                        $replace_ary['szCustomerEmail'] = $szShipperEmail; 
                        $replace_ary['szCustomerPhone'] = "+".$iInternationDialCode." ".$szShipperPhone;
                        
                        if(empty($szTransportModeLongName))
                        {
                            $szTransportModeLongName = 'Not specified';
                        }
                        $replace_ary['szTransportMode'] = $szTransportModeLongName ;

                        //$szToEmail = 'ajay@whiz-solutions.com';
                        $szToEmail = __STORE_SUPPORT_EMAIL__ ;
                        
                        /*
                        * We no longer send this email from 18-04-2016
                        */
                        //createEmail(__TRANSPORTECA_NEW_RFQ__, $replace_ary,$szToEmail, '', __STORE_SUPPORT_EMAIL__,false,false,false,false,$idBooking);
                        return true;
                    }
                }
                else
                {
                    return false;
                } 
            }
	}
        
        function addBookingFile($data)
        {
            if(!empty($data))
            { 
                $kForwarder = new cForwarder(); 
                $max_number_used_for_file = $kForwarder->getMaximumUsedNumberByForwarder(false,false,'FILE');
                $szFileRef = $this->generateBookingFileRef($max_number_used_for_file); 
                
                //$dtFileCreatedTime = date('Y-m-d H:i:s');
                $kBooking = new cBooking();
                $dtFileCreatedTime = $kBooking->getRealNow();
                
                if(trim(cPartner::$szApiRequestMode) == __API_REQUEST_MODE_TEST__)
                {
                    $table = __DBC_SCHEMATA_CLONE_FILE_LOGS__;
                }
                else
                {
                    $table = __DBC_SCHEMATA_FILE_LOGS__;
                }
                
                $BookingFileName = __APP_PATH_ROOT__."/logs/customerBookingFile.log";
                $strdata=array();
                $strdata[0]=" \n\n *** Inserting Data Into Booking File From addBookingFile  ".date("Y-m-d H:i:s")."\n\n"; 
                $strdata[1]="Booking Id: ".$data['idBooking']."\n" ;
                file_log(true, $strdata, $BookingFileName);
                
                $query=" 
                    INSERT INTO
                        ".$table."  
                    (  
                        idBooking,
                        idUser,  
                        szTransportecaTask,
                        szTransportecaStatus,
                        szFileRef, 
                        idCustomerOwner,
                        idFileOwner,
                        dtFileStatusUpdatedOn,
                        dtTaskStatusUpdatedOn,
                        iSearchType, 
                        idAdminUpdatedBy,
                        iStatus,
                        szForwarderBookingStatus,
                        szForwarderBookingTask,
                        iBookingFlag, 
                        iFromApi,
                        idApiUser,
                        dtCreatedOn,
                        dtUpdatedOn,
                        iActive,
                        iLabelStatus,
                        idOfflineChat,
                        iNumUnreadMessage,
                        iNoReferenceFound
                    )
                    VALUE
                    (
                        '".mysql_escape_custom($data['idBooking'])."',
                        '".mysql_escape_custom($data['idUser'])."',
                        '".mysql_escape_custom($data['szTaskStatus'])."',
                        '".mysql_escape_custom($data['szFileStatus'])."',
                        '".mysql_escape_custom($szFileRef)."', 
                        '".mysql_escape_custom($data['idCustomerOwner'])."',  
                        '".mysql_escape_custom($data['idFileOwner'])."',   
                        '".mysql_escape_custom($dtFileCreatedTime)."',
                        '".mysql_escape_custom($dtFileCreatedTime)."',
                        '".mysql_escape_custom($data['iSearchType'])."',   
                        '".mysql_escape_custom(trim($data['idAdminUpdatedBy']))."',
                        '".mysql_escape_custom(trim((int)$data['iStatus']))."',
                        '".mysql_escape_custom(trim($data['szForwarderBookingStatus']))."',
                        '".mysql_escape_custom(trim($data['szForwarderBookingTask']))."',
                        '".mysql_escape_custom(trim((int)$data['iBookingFlag']))."',   
                        '".mysql_escape_custom(trim((int)$data['iFromApi']))."',   
                        '".mysql_escape_custom(trim((int)$data['idApiUser']))."',   
                        now(),
                        now(),
                        '1',
                        '".mysql_escape_custom(trim((int)$data['iLabelStatus']))."',
                        '".mysql_escape_custom(trim((int)$data['idOfflineChat']))."',
                        '".mysql_escape_custom(trim((int)$data['iNumUnreadMessage']))."',
                        '".mysql_escape_custom(trim((int)$data['iNoReferenceFound']))."'
                    )
                "; 
                $strdata=array();
                $strdata[0]=" \n\n *** Inserting Data Into Booking File From addBookingFile  ".date("Y-m-d H:i:s")."\n\n"; 
                $strdata[1]="Booking Id: ".$data['idBooking']."\n" ;
                $strdata[2]="Booking File Query: ".$query."\n" ;
                file_log(true, $strdata, $BookingFileName);
           //     echo "<br> ".$query."<br>";
//                die;
                if($result=$this->exeSQL($query))
                { 
                    $this->idBookingFile = $this->iLastInsertID;
                    
                    $strdata=array();
                    $strdata[0]=" \n\n *** Inserting Data Into Booking File From addBookingFile  ".date("Y-m-d H:i:s")."\n\n"; 
                    $strdata[1]="Booking Id: ".$data['idBooking']."\n" ;
                    $strdata[2]="Booking File ID: ".$this->idBookingFile."\n" ;
                    file_log(true, $strdata, $BookingFileName);
                    
                    $bookingRefLogAry = array(); 
                    $bookingRefLogAry['szBookingRef'] = $szFileRef;	 
                    $bookingRefLogAry['szReferenceType'] = 'FILE';
                    $kForwarder->updateMaxNumBookingRef($bookingRefLogAry); 
                    return true;
                }
                else
                {
                    //echo " not updated ";
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
        }
        
        function generateBookingFileRef($max_number_used_for_file)
	{		
            $kBooking = new cBooking();
            $formated_max_string = $kBooking->getFormattedString($max_number_used_for_file+1,4);
            $booking_file_ref = date('y').date('m')."TF".$formated_max_string ;

            if(($check = $this->fileRefAlreadyUsed($booking_file_ref))===true)
            {
                $max_number_used_for_file = $max_number_used_for_file + 1 ;
                $booking_file_ref = $this->generateBookingFileRef($max_number_used_for_file);
            }
            return $booking_file_ref;
	}
        
        function fileRefAlreadyUsed($code)
	{ 
            if(!empty($code))
            { 
                $query = "
                    SELECT
                        id
                    FROM
                        ".__DBC_SCHEMATA_FILE_LOGS__."
                    WHERE
                        szFileRef = '".mysql_escape_custom($code)."' 
                ";
                //echo "<br>".$query ;
                if( ( $result = $this->exeSQL( $query ) ) )
                {
                    if($this->iNumRows > 0)
                    {
                        return true;
                    }
                    return false;
                }
                else
                {
                    return false;
                }
            }
	}
        
        function updateUserDetailsByQuotes($update_str,$idUser)
	{
            if(!empty($update_str) && $idUser>0)
            {
                $query="	
                    UPDATE
                        ".__DBC_SCHEMATA_USERS__."
                    SET
                        ".$update_str." 
                    WHERE
                        id = '".(int)$idUser."';
                ";
                //echo $query ;
                //die;
                if($result=$this->exeSQL($query))
                {
                    $f = fopen(__APP_PATH_UPDATE_BOOKING_LOG__, "a");
                    fwrite($f, "\n------Start Update User Details By Quotes---------\n");
                    fwrite($f, date("d-m-Y h:i:s")."\n");
                    fwrite($f, $update_str."\n");
                    fwrite($f, "User Id:".$idUser."\n");
                    fwrite($f, "\n-------End Update User Details By Quotes--------\n");
                    fclose($f);
                    return true;
                }
                else
                {
                    //echo " not updated ";
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
        
        
        function updateShipperConsigneeDetails($update_str,$idShipperConsignee)
	{
            if(!empty($update_str) && $idShipperConsignee>0)
            {
                if(trim(cPartner::$szApiRequestMode) == __API_REQUEST_MODE_TEST__)
                {
                    $table = __DBC_SCHEMATA_CLONE_SHIPPER_CONSIGNEE__;
                }
                else
                {
                    $table = __DBC_SCHEMATA_SHIPPER_CONSIGNEE__;
                }
                
                
                $query="	
                    UPDATE
                        ".$table."
                    SET
                        ".$update_str." 
                    WHERE
                        id = '".(int)$idShipperConsignee."';
                ";
                //echo $query ;
                //die;
                if($result=$this->exeSQL($query))
                {
                    return true;
                }
                else
                {
                    //echo " not updated ";
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
	
	function generateUniqueToken($number)
	{
	    $arr = array('a', 'b', 'c', 'd', 'e', 'f',
	                 'g', 'h', 'i', 'j', 'k', 'l',
	                 'm', 'n', 'o', 'p', 'r', 's',
	                 't', 'u', 'v', 'x', 'y', 'z',
	                 'A', 'B', 'C', 'D', 'E', 'F',
	                 'G', 'H', 'I', 'J', 'K', 'L',
	                 'M', 'N', 'O', 'P', 'R', 'S',
	                 'T', 'U', 'V', 'X', 'Y', 'Z',
	                 '1', '2', '3', '4', '5', '6',
	                 '7', '8', '9', '0');
	    $token = "";
	    for ($i = 0; $i < $number; $i++) {
	        $index = rand(0, count($arr) - 1);
	        $token .= $arr[$index];
		}
		return $token ;
	}
        
        function addNewManualQuote($dtShippingDate,$from_new_process=false)
        { 
            //So from now we are adding a row in tblbooking all the time whenever user tries to create new manual quote. 
            //$idBooking = $this->getUneditedBookingQuotes();  
//            if($idBooking>0 && !$from_new_process)
//            {
//                return $idBooking ;
//            }
//            else
//            {  
                $kBooking = new cBooking();
                $szBookingRandomNum = $kBooking->getUniqueBookingKey(10);
                $szBookingRandomNum = $kBooking->isBookingRandomNumExist($szBookingRandomNum); 
                   
                $query=" 
                    INSERT INTO
                        ".__DBC_SCHEMATA_BOOKING__."  
                    (  
                        idBookingStatus,
                        szBookingRandomNum, 
                        iQuotesStatus,
                        iNeverEdited,  
                        iBookingQuotes,
                        idTimingType,
                        dtTimingDate,
                        dtCreatedOn,
                        dtBookingInitiated
                    )
                    VALUE
                    (
                        '".mysql_escape_custom(__BOOKING_STATUS_DRAFT__)."',
                        '".mysql_escape_custom($szBookingRandomNum)."', 
                        '".mysql_escape_custom(__BOOKING_QUOTES_STATUS_NEW_BOOKING__)."', 
                        '0',
                        '1',
                        '1',
                        '".mysql_escape_custom($dtShippingDate)."', 
                        now(),
                        now()
                    )
                "; 
                //echo $query ;
                //die;
                if($result=$this->exeSQL($query))
                {
                    return $this->iLastInsertID ;
                }
                else
                {
                    return false;
                }
//            }
        }
        
        function getUneditedBookingQuotes()
        {
            $query= " SELECT id FROM ".__DBC_SCHEMATA_BOOKING__." WHERE iNeverEdited = '1' " ;
            
            if($result = $this->exeSQL($query))
            {  
                $row = $this->getAssoc($result); 
                return $row['id'] ;
            }
            else
            {
                return false;
            }
        } 
        function updateManualQuotes($data,$idBooking)
        { 
            if(!empty($data) && (int)$idBooking>0)
            {  
                $this->validateManualQuotes($data);
                if($this->error==true)
                {
                    return false;
                } 
                $kForwarder = new cForwarder();
                $kWhsSearch = new cWHSSearch();
                
                $shipperConsigneeAry = array() ;
                $shipperConsigneeAry['idUser'] = $_SESSION['user_id'];
                $idShipperConsignee = $this->addShipperConsignee_new($shipperConsigneeAry,$idBooking,true);
                
                $res_ary = array();				
                $res_ary['idShipperConsignee'] = $idShipperConsignee;  
                $res_ary['idCustomerDialCode'] = $this->idBillingDialCode ;
                $res_ary['szCustomerCompanyName'] = $this->szBillingCompanyName ;
                $res_ary['szFirstName'] = $this->szBillingFirstName ;
                $res_ary['szLastName'] = $this->szBillingLastName ;
                $res_ary['szEmail'] = $this->szBillingEmail ; 
                $res_ary['szCustomerPhoneNumber'] = $this->szBillingPhone ; 
                $res_ary['szCustomerAddress1'] = $this->szBillingAddress ;
                $res_ary['szCustomerPostCode'] = $this->szBillingPostcode ; 
                $res_ary['szCustomerCity'] = $this->szBillingCity ; 
                $res_ary['szCustomerState'] = $this->szBillingState ;
                $res_ary['szCustomerCountry'] = $this->idBillingCountry ;  
                $res_ary['fCargoVolume'] = $this->fCargoVolume ; 
                $res_ary['fCargoWeight'] = ceil($this->fCargoWeight) ;    
                $res_ary['szServiceDescription'] = $this->szServiceDescription ;    
                $res_ary['idCustomerCurrency'] = $this->idCustomerCurrency ; 
                $res_ary['idOriginCountry'] = $this->idShipperCountry ;
                $res_ary['idDestinationCountry'] = $this->idConsigneeCountry ;
                $res_ary['iNeverEdited'] = 0;
                $res_ary['iInsuranceMandatory'] = $this->iInsuranceMandatory ;
                 
                if($this->idCustomerCurrency==1)
                { 
                    $res_ary['szCustomerCurrency'] = 'USD';
                    $res_ary['fCustomerExchangeRate'] = 1;
                }
                else
                {
                    $resultAry = $kWhsSearch->getCurrencyDetails($this->idCustomerCurrency);		 
                    $res_ary['szCustomerCurrency'] = $resultAry['szCurrency'];
                    $res_ary['fCustomerExchangeRate'] = $resultAry['fUsdValue'];
                }
                
                $res_ary['fTotalPriceCustomerCurrency'] = $this->fBookingAmount ; 
                $res_ary['fTotalPriceUSD'] = round(($this->fBookingAmount*$res_ary['fCustomerExchangeRate']),4);
                $res_ary['idForwarder'] = $this->idForwarder ;    
                 
                // initialising object of forwarder class with relevant data from database 
                
                $kForwarder->load($this->idForwarder);
  
                $res_ary['szForwarderDispName']=$kForwarder->szDisplayName;
                $res_ary['szForwarderRegistName']=$kForwarder->szCompanyName;
                $res_ary['szForwarderAddress']=$kForwarder->szAddress;
                $res_ary['szForwarderAddress2']=$kForwarder->szAddress2;
                $res_ary['szForwarderAddress3']=$kForwarder->szAddress3;
                $res_ary['szForwarderPostCode']=$kForwarder->szPostCode;
                $res_ary['szForwarderCity']=$kForwarder->szCity;
                $res_ary['szForwarderState']=$kForwarder->szState;
                $res_ary['idForwarderCountry']=$kForwarder->idCountry;
                $res_ary['szForwarderLogo']=$kForwarder->szLogo; 
                $res_ary['idForwarderCurrency'] = $kForwarder->szCurrency ;
                 
                if($kForwarder->szCurrency==1)
                { 
                    $res_ary['szForwarderCurrency'] = 'USD';
                    $res_ary['fForwarderExchangeRate'] = 1;
                }
                else
                {
                    $resultAry = $kWhsSearch->getCurrencyDetails($kForwarder->szCurrency);		 
                    $res_ary['szForwarderCurrency'] = $resultAry['szCurrency'];
                    $res_ary['fForwarderExchangeRate'] = $resultAry['fUsdValue'];
                }
                 
                $fTotalPriceIncludingVat = ($this->fBookingAmount + $this->fTotalVat) ;
                $fTotalPriceIncludingVatUsd = ($fTotalPriceIncludingVat * $res_ary['fCustomerExchangeRate']) ;
                
                if($kForwarder->szCurrency==$this->idCustomerCurrency)
                {
                    $res_ary['fTotalPriceForwarderCurrency'] = $fTotalPriceIncludingVat;
                    $res_ary['fTotalPriceNoMarkupForwarderCurrency'] = $fTotalPriceIncludingVat;
                    $res_ary['fTotalPriceNoMarkupForwarderCurrency_new'] = $fTotalPriceIncludingVat;
                }
                else
                {
                    if($res_ary['fForwarderExchangeRate']>0)
                    {
                        $res_ary['fTotalPriceForwarderCurrency'] = ($fTotalPriceIncludingVatUsd/$res_ary['fForwarderExchangeRate']);
                        $res_ary['fTotalPriceNoMarkupForwarderCurrency'] = ($fTotalPriceIncludingVatUsd/$res_ary['fForwarderExchangeRate']);
                        $res_ary['fTotalPriceNoMarkupForwarderCurrency_new'] = ($fTotalPriceIncludingVatUsd/$res_ary['fForwarderExchangeRate']);
                    }  
                } 
                                        
                $res_ary['dtQuoteValidTo'] = $this->dtQuotesValidTo ;    
                $res_ary['idServiceType'] = $this->idServiceType ;    
                $res_ary['idTransportMode'] = $this->idTransportMode ;    
                
                $kConfig = new cConfig();
                $transportModeListAry = array();
                $transportModeListAry = $kConfig->getAllTransportMode($this->idTransportMode);
     
                $res_ary['szTransportMode'] = $transportModeListAry[0]['szShortName']; 
                $res_ary['iTransitHours'] = $this->iTransitHours ;    
                $res_ary['szFrequency'] = $this->szFrequency ;     
                
                $res_ary['fReferalPercentage'] = $this->fReferalFee ;    
                $res_ary['fReferalAmount'] = round((($res_ary['fTotalPriceForwarderCurrency'] * $this->fReferalFee)/100),2);
                
                if($res_ary['fForwarderExchangeRate']>0)
                {
                    $res_ary['fReferalAmountUSD'] = ($res_ary['fReferalAmount'] * $res_ary['fForwarderExchangeRate']) ; 
                }
                
                $res_ary['fTotalVat'] = $this->fTotalVat ;    
                 
                $idInsuranceCurrency = $this->idInsuranceCurrency ;
                if($idInsuranceCurrency==1)
                {
                    $this->idGoodsInsuranceCurrency = 1;
                    $this->szGoodsInsuranceCurrency = 'USD';
                    $this->fGoodsInsuranceExchangeRate = 1;  
                    $this->fGoodsInsurancePriceUSD = $this->fInsuranceValue ; 
                    $fMaxBookingAmountToBeInsuranced = $fMaxBookingAmountToBeInsurancedUSD ;
                }
                else
                {
                    $resultAry = $kWhsSearch->getCurrencyDetails($idInsuranceCurrency);		

                    $this->idGoodsInsuranceCurrency = $resultAry['idCurrency'];
                    $this->szGoodsInsuranceCurrency = $resultAry['szCurrency'];
                    $this->fGoodsInsuranceExchangeRate = $resultAry['fUsdValue']; 
                    $this->fGoodsInsurancePriceUSD = $this->fInsuranceValue * $this->fGoodsInsuranceExchangeRate ;  
                } 
                
                $fTotalBookingPriceUsd = $res_ary['fTotalPriceUSD'] ;
                $fTotalBookingAmountToBeInsurancedUsd = $fTotalBookingPriceUsd + $this->fGoodsInsurancePriceUSD ; 
                $res_ary['fTotalAmountInsured'] = $fTotalBookingAmountToBeInsurancedUsd ;
                //$res_ary['iMinrateApplied'] = $iMinrateApplied;
                
                if($this->fInsuranceSellPrice>0)
                { 
                    $res_ary['iInsuranceIncluded'] = 1 ;
                } 
                $res_ary['fTotalInsuranceCostForBookingCustomerCurrency'] = $this->fInsuranceSellPrice ;    
                $res_ary['fTotalInsuranceCostForBookingCustomerCurrency_buyRate'] = $this->fInsuranceBuyPrice ;  
                $res_ary['fValueOfGoods'] = $this->fInsuranceValue ;   
                $res_ary['fValueOfGoodsUSD'] = $this->fInsuranceValue * $res_ary['fCustomerExchangeRate']  ; 
                $res_ary['idGoodsInsuranceCurrency'] = $this->idGoodsInsuranceCurrency ;
                $res_ary['szGoodsInsuranceCurrency'] = $this->szGoodsInsuranceCurrency ;
                $res_ary['fGoodsInsuranceExchangeRate'] = $this->fGoodsInsuranceExchangeRate ; 
                $res_ary['fTotalInsuranceCostForBooking'] = $this->fInsuranceSellPrice ;
                $res_ary['fTotalInsuranceCostForBookingUSD'] = $this->fInsuranceSellPrice * $res_ary['fCustomerExchangeRate'] ;
                $res_ary['fTotalInsuranceCostForBookingUSD_buyRate'] = $this->fInsuranceBuyPrice * $res_ary['fCustomerExchangeRate'] ;
                
                
                
                $res_ary['szOtherComments'] = $this->szOtherComments ;   
                /*
                echo " volume ".$this->fCargoVolume;
                echo " <br> weight ".$this->fCargoWeight ;
                echo " <br> service desc ".$this->szServiceDescription ;
                echo " <br> forwarder ".$this->idForwarder ;
                echo " <br> Transit Hour ".$this->iTransitHours ;
                echo " <br> Frequency ".$this->szFrequency ; 
                echo " szFrequency ".$this->szFrequency;
                echo " <br> dtQuotesValidTo ".$this->dtQuotesValidTo ;
                echo " <br> idCustomerCurrency ".$this->idCustomerCurrency ;
                echo " <br> fBookingAmount ".$this->fBookingAmount ;
                echo " <br> fReferalFee ".$this->fReferalFee ; 
                */
                if($this->fCargoVolume>0 && $this->fCargoWeight>0 && !empty($this->szServiceDescription) && $this->idForwarder>0 && !empty($this->iTransitHours) && !empty($this->szFrequency) && !empty($this->dtQuotesValidTo) && $this->idCustomerCurrency>0 && $this->fBookingAmount>0 && $this->fReferalFee>0)
                { 
                    $iQuotationReady = 1;
                }
                else
                {
                    $iQuotationReady = 0;
                }
                $res_ary['iQuotationReady'] = $iQuotationReady ;   
                
                if(!empty($res_ary))
                {
                    foreach($res_ary as $key=>$value)
                    {
                        $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                }
                
                $update_query = rtrim($update_query,",");  
                $kBooking = new cBooking();
                if($kBooking->updateDraftBooking($update_query,$idBooking))
                {
                    $cargoAry = array();
                    $cargoAry['szCargoCommodity'] = $this->szCargoCommodity;
                    $cargoAry['idCargo'] = $this->idCargo ;
                    $cargoAry['idBooking'] = $idBooking ;
                    $this->addUpdateCargoCommodity($cargoAry); 
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        function validateManualQuotes($data)
        {  
            if(!empty($data))
            {  
                if(!empty($data['dtQuotesValidTo']))
                {
                    $data['dtQuotesValidTo'] = format_date_time_admin($data['dtQuotesValidTo']);
                } 
                $required_flag = false;
                 /*
                if($data['iShipperFlag']==1)
                {
                    $this->iShipperConsignee = 1 ;
                }
                else if($data['iConsigneeFlag']==1)
                {
                    $this->iShipperConsignee = 2 ;
                }
                else
                {
                    $this->iShipperConsignee = 3;
                } */
                
                $this->iShipperConsignee = $data['iShipperConsignee'] ;
                $this->iInsuranceMandatory = $data['iInsuranceMandatory'] ;
                
                //$this->set_szShipperTitleText(trim(sanitize_all_html_input($data['szShipperHeading'])),$required_flag);			
                //$this->set_szConsigneeTitleText(trim(sanitize_all_html_input($data['szConsigneeHeading'])),$required_flag); 
                
                $this->set_idServiceType(trim(sanitize_all_html_input($data['idServiceType'])),$required_flag);			
                $this->set_szShipperCompanyName(trim(sanitize_all_html_input($data['szShipperCompanyName'])),$required_flag);
                $this->set_szShipperFirstName(trim(sanitize_all_html_input($data['szShipperFirstName'])),$required_flag);
                $this->set_szShipperLastName(trim(sanitize_all_html_input($data['szShipperLastName'])),$required_flag);
                $this->set_szShipperEmail(trim(sanitize_all_html_input($data['szShipperEmail'])),$required_flag);				
                $this->set_szShipperPhone(trim(sanitize_all_html_input($data['szShipperPhone'])),$required_flag);	 
                $this->set_idShipperDialCode(trim(sanitize_all_html_input($data['idShipperDialCode'])),$required_flag);  
                $this->set_szShipperAddress(trim(sanitize_all_html_input($data['szShipperAddress'])),$required_flag); 
                $this->set_szShipperPostcode(trim(sanitize_all_html_input($data['szShipperPostcode'])),$required_flag); 
                $this->set_szShipperCity(trim(sanitize_all_html_input($data['szShipperCity'])),$required_flag); 
                $this->set_idShipperCountry(trim(sanitize_all_html_input($data['idShipperCountry'])),$required_flag); 
                 
                $this->set_szConsigneeCompanyName(trim(sanitize_all_html_input($data['szConsigneeCompanyName'])),$required_flag);
                $this->set_szConsigneeFirstName(trim(sanitize_all_html_input($data['szConsigneeFirstName'])),$required_flag);
                $this->set_szConsigneeLastName(trim(sanitize_all_html_input($data['szConsigneeLastName'])),$required_flag);
                $this->set_szConsigneeEmail(trim(sanitize_all_html_input($data['szConsigneeEmail'])),$required_flag); 
                $this->set_szConsigneePhone(trim(sanitize_all_html_input($data['szConsigneePhone'])),$required_flag);	
                $this->set_idConsigneeDialCode(trim(sanitize_all_html_input($data['idConsigneeDialCode'])),$required_flag); 
                $this->set_idConsigneeCountry(trim(sanitize_all_html_input($data['idConsigneeCountry'])),$required_flag); 
				
                $this->set_szConsigneeAddress(trim(sanitize_all_html_input($data['szConsigneeAddress'])),$required_flag); 
                $this->set_szConsigneePostcode(trim(sanitize_all_html_input($data['szConsigneePostcode'])),$required_flag); 
                $this->set_szConsigneeCity(trim(sanitize_all_html_input($data['szConsigneeCity'])),$required_flag); 
                
                if($this->iShipperConsignee == 3)
                { 
                    $this->set_szBillingCompanyName(trim(sanitize_all_html_input($data['szBillingCompanyName'])),$required_flag);
                    $this->set_szBillingFirstName(trim(sanitize_all_html_input($data['szBillingFirstName'])),$required_flag);
                    $this->set_szBillingLastName(trim(sanitize_all_html_input($data['szBillingLastName'])),$required_flag);
                    $this->set_szBillingEmail(trim(sanitize_all_html_input($data['szBillingEmail'])),$required_flag); 
                    $this->set_szBillingPhone(trim(sanitize_all_html_input($data['szBillingPhone'])),$required_flag);	
                    $this->set_idBillingCountry(trim(sanitize_all_html_input($data['idBillingCountry'])),$required_flag);
                    $this->set_idBillingDialCode(trim(sanitize_all_html_input($data['idBillingDialCode'])),$required_flag);	 
                    $this->set_szBillingCity(trim(sanitize_all_html_input($data['szBillingCity'])),$required_flag);
                    $this->set_szBillingPostcode(trim(sanitize_all_html_input($data['szBillingPostcode'])),$required_flag);
                    $this->set_szBillingAddress(trim(sanitize_all_html_input($data['szBillingAddress'])),$required_flag);  
                }
                
                $this->set_fCargoVolume(trim(sanitize_all_html_input($data['fCargoVolume'])),$required_flag);
                $this->set_fCargoWeight(trim(sanitize_all_html_input($data['fCargoWeight'])),$required_flag); 
                $this->set_szCargoCommodity($data['szCargoCommodity'],false,$required_flag); 
                $this->set_idCargo($data['idCargo'],$required_flag); 
                
                $this->set_szServiceDescription(trim(sanitize_all_html_input($data['szServiceDescription'])),$required_flag); 
                $this->set_idForwarder(trim(sanitize_all_html_input($data['idForwarder'])),$required_flag);
                $this->set_dtQuotesValidTo(trim(sanitize_all_html_input($data['dtQuotesValidTo'])),$required_flag);
                $this->set_idTransportMode(trim(sanitize_all_html_input($data['idTransportMode'])),$required_flag);
                $this->set_iTransitHours(trim(sanitize_all_html_input($data['iTransitHours'])),$required_flag);
                $this->set_szFrequency(trim(sanitize_all_html_input($data['szFrequency'])),$required_flag); 
                $this->set_idCustomerCurrency(trim(sanitize_all_html_input($data['idCustomerCurrency'])),$required_flag);
                $this->set_fBookingAmount(trim(sanitize_all_html_input($data['fBookingAmount'])),$required_flag);
                
                $this->set_fReferalFee(trim(sanitize_all_html_input($data['fReferalFee'])),$required_flag);
                $this->set_fTotalVat(trim(sanitize_all_html_input($data['fTotalVat'])),$required_flag);
                $this->set_fInsuranceSellPrice(trim(sanitize_all_html_input($data['fInsuranceSellPrice'])),$required_flag);
                $this->set_fInsuranceBuyPrice(trim(sanitize_all_html_input($data['fInsuranceBuyPrice'])),$required_flag);
                $this->set_fInsuranceValue(trim(sanitize_all_html_input($data['fInsuranceValue'])),$required_flag); 
                $this->set_idInsuranceCurrency(trim(sanitize_all_html_input($data['idInsuranceCurrency'])),$required_flag); 
                
                $this->set_szOtherComments(trim(sanitize_all_html_input($data['szOtherComments'])),$required_flag);
               
                if($this->error==true)
                {
                    return false;
                }
                else
                { 
                    if($this->iShipperConsignee ==1)
                    {
                        $this->szBillingCompanyName = $this->szShipperCompanyName ;
                        $this->szBillingFirstName = $this->szShipperFirstName ;
                        $this->szBillingLastName = $this->szShipperLastName ;
                        $this->szBillingEmail = $this->szShipperEmail ;
                        $this->szBillingPhone = $this->szShipperPhone ;
                        $this->idBillingDialCode = $this->idShipperDialCode ; 
                        $this->idBillingCountry = $this->idShipperCountry ;
                        $this->szBillingCity = $this->szShipperCity ;
                        $this->szBillingPostcode = $this->szShipperPostcode ;				
                        $this->szBillingAddress = $this->szShipperAddress ; 
                        $this->szBillingState = $this->szShipperState ;
                    }
                    else if($this->iShipperConsignee==2)
                    {
                        $this->szBillingCompanyName = $this->szConsigneeCompanyName ;
                        $this->szBillingFirstName = $this->szConsigneeFirstName ;
                        $this->szBillingLastName = $this->szConsigneeLastName ;
                        $this->szBillingEmail = $this->szConsigneeEmail ;
                        $this->szBillingPhone = $this->szConsigneePhone ;
                        $this->idBillingDialCode = $this->idConsigneeDialCode ; 
                        $this->idBillingCountry = $this->idConsigneeCountry ;
                        $this->szBillingCity = $this->szConsigneeCity ;
                        $this->szBillingPostcode = $this->szConsigneePostcode ;				
                        $this->szBillingAddress = $this->szConsigneeAddress ; 
                        $this->szBillingState = $this->szConsigneeState ;
                    } 
                    $this->idShipperCountry_pickup = $this->idShipperCountry ;
                    $this->szShipperCity_pickup = $this->szShipperCity ;
                    $this->szShipperPostcode_pickup = $this->szShipperPostcode ;				
                    $this->szShipperAddress_pickup = $this->szShipperAddress ;
                    $this->szShipperAddress2_pickup = $this->szShipperAddress2;
                    $this->szShipperAddress3_pickup = $this->szShipperAddress3 ;
                    $this->szShipperState_pickup = $this->szShipperState ;

                    $this->idConsigneeCountry_pickup = $this->idConsigneeCountry ;
                    $this->szConsigneeCity_pickup = $this->szConsigneeCity ;
                    $this->szConsigneePostcode_pickup = $this->szConsigneePostcode ;				
                    $this->szConsigneeAddress_pickup = $this->szConsigneeAddress ;
                    $this->szConsigneeAddress2_pickup = $this->szConsigneeAddress2;
                    $this->szConsigneeAddress3_pickup = $this->szConsigneeAddress3 ;
                    $this->szConsigneeState_pickup = $this->szConsigneeState ;
                    return true;
                } 			
            }
        }
        
        function sendManualQuotes($data,$idBooking,$sent_from_new_rfq=false)
        {
            if(!empty($data))
            {
                $this->set_szUserEmail(trim(sanitize_all_html_input($data['szUserEmail'])));
                  
                if($this->error==true)
                {
                    return false;
                }                
                $this->iLanguage = $data['iLanguage']; 
                $idAdmin = $_SESSION['admin_id']; 
                
                $query=" 
                    INSERT INTO
                        ".__DBC_SCHEMATA_QUOTES_SENT_LOGS__."  
                    (  
                        idbooking,
                        szEmail, 
                        iLanguage,
                        idAdmin,
                        dtCreatedOn,
                        iActive
                    )
                    VALUE
                    (
                        '".mysql_escape_custom($idBooking)."',
                        '".mysql_escape_custom($this->szUserEmail)."',
                        '".mysql_escape_custom($this->iLanguage)."',
                        '".mysql_escape_custom($idAdmin)."',
                        now(),
                        '1' 
                    )
                ";
                //echo $query ;
                //die;
                if($result=$this->exeSQL($query))
                {
                    $res_ary = array();
                    $res_ary['iQuotesStatus'] = __BOOKING_QUOTES_STATUS_SENT__ ;   
                    $res_ary['dtQuotationSent'] = date('Y-m-d H:i:s');
                    $res_ary['iBookingLanguage'] = $this->iLanguage ;
  
                    if(!empty($res_ary))
                    {
                        foreach($res_ary as $key=>$value)
                        {
                            $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                        }
                    } 
                    
                    $update_query = rtrim($update_query,",");  
                    
                    $kBooking = new cBooking();
                    $kBooking->updateDraftBooking($update_query,$idBooking);
                     
                    if($sent_from_new_rfq)
                    {
                        return true ;
                    }
                    else
                    {
                        $postSearchAry = $kBooking->getExtendedBookingDetails($idBooking,$this->iLanguage);

                        $iLanguage = getLanguageId(); 

                        $cargoDetailsAry = $kBooking->getCargoComodityDeailsByBookingId($idBooking);
                        $szCargoCommodity = "";
                        if(!empty($cargoDetailsAry['1']['szCommodity']))
                        {
                            $szCargoCommodity = ", ".utf8_decode($cargoDetailsAry['1']['szCommodity']); 
                        }
                        $kConfig = new cConfig();
                        $kConfig->loadCountry($postSearchAry['idOriginCountry'],false,$this->iLanguage);
                        $szCountryStrUrl = $kConfig->szCountryISO ;
                        $szOrigingCountry = $kConfig->szCountryName ;
                        $kConfig->loadCountry($postSearchAry['idDestinationCountry'],false,$this->iLanguage);
                        $szCountryStrUrl .= $kConfig->szCountryISO ; 
                        $szDestinationCountry = $kConfig->szCountryName ; 

                        $szBookingRandomNumber = $postSearchAry['szBookingRandomNum'] ;
                        
                        $kConfig =new cConfig();
                        $langArr=$kConfig->getLanguageDetails('',$iBookingLanguage);

                        if(!empty($langArr) && $iBookingLanguage!=__LANGUAGE_ID_ENGLISH__)
                        {
                            $szLanguageCode=$langArr[0]['szLanguageCode'];
                            $szBaseUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".$szLanguageCode."/booking";
                            $szcbm = $langArr[0]['szDimensionUnit'] ;
                        }
                        else
                        {
                            $langEngArr=$kConfig->getLanguageDetails('',__LANGUAGE_ID_ENGLISH__);
                            $szBaseUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/booking";
                            if(!empty($langEngArr)){
                            $szcbm = $langEngArr[0]['szDimensionUnit'] ;
                            }else{
                                 $szcbm = "cbm" ;
                            }
                        } 
                        $szServiceQuoteUrl =  $szBaseUrl."/".$szBookingRandomNumber."/quote-".$szCountryStrUrl."/" ;

                        $fBookingAmount = getPriceByLang($postSearchAry['fTotalPriceCustomerCurrency'],$this->iLanguage) ;
                        $fInsuranceSellPrice = getPriceByLang($postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency'],$this->iLanguage) ; 

                        $replace_ary = array(); 
                        $replace_ary['szFirstname'] = $postSearchAry['szFirstName'];
                        $replace_ary['szLastname'] = $postSearchAry['szLastName'];
                        $replace_ary['szCargoDescription'] = trim($szCargoCommodity); 
                        $replace_ary['szOriginCountryStr'] = $postSearchAry['szShipperCity'].", ".$postSearchAry['szShipperCountry'];
                        $replace_ary['szDestinationCountryStr'] = $postSearchAry['szConsigneeCity'].", ".$postSearchAry['szConsigneeCountry'];

                        $replace_ary['szOriginCountry']= $szOrigingCountry;
                        $replace_ary['szDestinationCountry']=$szDestinationCountry;

                        $replace_ary['szDisplayName'] = $postSearchAry['szForwarderDispName']; 
                        $replace_ary['szServiceDescription'] = $postSearchAry['szServiceDescription'];
                        $replace_ary['fTotalBookingPrice'] = $postSearchAry['szCustomerCurrency']." ".$fBookingAmount;
                        $replace_ary['szLink'] = $szServiceQuoteUrl ;

                        if($postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']>0)
                        {
                            if($postSearchAry['iBookingLanguage']==2)
                            {
                                $replace_ary['szInsuranceText'] = "Transportforsikring: ".$postSearchAry['szCustomerCurrency']." ".$fInsuranceSellPrice."<br>";
                            }
                            else
                            {
                                $replace_ary['szInsuranceText'] = "Insurance: ".$postSearchAry['szCustomerCurrency']." ".$fInsuranceSellPrice."<br>";
                            }
                        } 
                        else
                        {
                            $replace_ary['szInsuranceText'] = "";
                        }

                        $replace_ary['szTransitTime'] = $postSearchAry['iTransitHours'] ;
                        if(!empty($postSearchAry['szOtherComments']))
                        {
                            $replace_ary['szInvoiceComment'] = $postSearchAry['szOtherComments'].".<br /><br />";
                        }
                        else
                        {
                            $replace_ary['szInvoiceComment'] = "";
                        }

                        if($postSearchAry['fTotalVat']>0)
                        { 
                            if($this->iLanguage==1)
                            {
                                $replace_ary['szVATText'] = ', excl. VAT' ;
                            }
                            else
                            {
                                $replace_ary['szVATText'] = ', ex moms' ;
                            }
                        }
                        else
                        {
                            $replace_ary['szVATText'] = '';
                        }

                        $dtQuoteValidTo = $postSearchAry['dtQuoteValidTo'] ;
                        $iMonth = date('m',strtotime($dtQuoteValidTo));

                        $dtQuoteValidTo_str = date('j.',strtotime($dtQuoteValidTo)); 
                        $szMonthName = getMonthName($this->iLanguage,$iMonth,true); 
                        $dtQuoteValidTo_str .=" ".strtolower($szMonthName)." ".date('Y',strtotime($dtQuoteValidTo)) ;

                        $replace_ary['dtQuoteValidTo'] = $dtQuoteValidTo_str; 
                        $replace_ary['dtTimingDate'] = date('d. F Y',strtotime($postSearchAry['dtTimingDate']));
                        $replace_ary['fCargoVolume'] = format_volume($postSearchAry['fCargoVolume'])." ".$szcbm;
                        $replace_ary['fCargoWeight'] = number_format((float)$postSearchAry['fCargoWeight'])." kg";
                        $replace_ary['iBookingLanguage'] = $this->iLanguage ;
                        $replace_ary['szEmailCustomer'] = $postSearchAry['szEmail'];

                        $szToEmail = $this->szUserEmail ;
                        //$szToEmail = 'ajay@whiz-solutions.com';
                        createEmail(__SEND_MANUAL_BOOKING_QUOTES__, $replace_ary,$szToEmail, '', __STORE_SUPPORT_EMAIL__,false,false,false,false,$idBooking);
                        return true;
                    } 
                }
                else
                {
                    //echo " not updated ";
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
        }
        function deleteManualQuotes($idBooking)
        {
            if($idBooking>0)
            {
                $res_ary = array();
                $res_ary['idBookingStatus'] = 5 ;    

                if(!empty($res_ary));
                {
                    foreach($res_ary as $key=>$value)
                    {
                        $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                } 
                $update_query = rtrim($update_query,",");  

                $kBooking = new cBooking();
                if($kBooking->updateDraftBooking($update_query,$idBooking))
                {
                    return true;
                }
                else
                {
                    
                }
            }
        }
        
        function expireManualQuotes($idBooking)
        {
            if($idBooking>0)
            {
                $res_ary = array();
                $res_ary['dtQuotationExpired'] = date('Y-m-d H:i:s') ;     
                $res_ary['iQuotesStatus'] = __BOOKING_QUOTES_STATUS_EXPIRED__ ;  

                if(!empty($res_ary));
                {
                    foreach($res_ary as $key=>$value)
                    {
                        $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                }
                
                $update_query = rtrim($update_query,",");  
                //echo $update_query ;
                $kBooking = new cBooking();
                if($kBooking->updateDraftBooking($update_query,$idBooking))
                {
                    return true;
                }
                else
                {
                    
                }
            }
        }
        
        function copyBookingQuote($idBooking,$iRepeatBookingFlag=false,$idBookingFile=false,$iAnonymous=false,$anotherFlag=false,$idCustomer=0)
        {
            if($idBooking>0)
            {
                $query = " DROP TABLE IF EXISTS tmptable ";  
                $result = $this->exeSQL($query); 
                /*
                *   getting max id from tblbooking
                */
                $query=" SELECT max(id) as iMaxId FROM ".__DBC_SCHEMATA_BOOKING__ ;
                if($result = $this->exeSQL($query))
                {  
                    $row=$this->getAssoc($result);
                    $iNewBookingID = $row['iMaxId'] + 1 ;
                    $this->iNewBookingID =  $iNewBookingID;
                            
                    /*
                    * Coping data to a temprary table
                    */
                             
                    $query = " 
                        CREATE TEMPORARY TABLE IF NOT EXISTS
                            tmptable 
                                SELECT 
                                    * 
                                FROM 
                                    ".__DBC_SCHEMATA_BOOKING__." 
                                WHERE 
                                    id = '".$idBooking."' 
                    ";  
                    $result = $this->exeSQL($query);
                            
                    /*
                    * updating primary id in temprary table
                    */
                    $query = " UPDATE tmptable SET id = '".$iNewBookingID."' WHERE id = '".$idBooking."' "; 
                    $result = $this->exeSQL($query);
                    
                    /*
                    * inserting data from temprary table to main table
                    */
                    $query = " INSERT INTO ".__DBC_SCHEMATA_BOOKING__." 
                            SELECT 
                                *
                            FROM 
                                tmptable
                            WHERE 
                                id = '".$iNewBookingID."' "; 
                    
                    $result = $this->exeSQL($query);  
                      
                    //checking if data is successfully inserted in tblbookings or not. 
                    
                    $kBooking = new cBooking();
                    $postSearchAry = array();
                    $postSearchAry = $kBooking->getBookingDetails($iNewBookingID);
                    if(!empty($postSearchAry))
                    {
                        if($postSearchAry['idShipperConsignee']>0)
                        {
                            $query=" SELECT max(id) as iMaxId FROM ".__DBC_SCHEMATA_SHIPPER_CONSIGNEE__ ;
                            $result = $this->exeSQL($query);

                            $row=$this->getAssoc($result);
                            $iNewShipperConsigneeID = $row['iMaxId'] + 1 ;

                            $query = " DROP TABLE tmptable ";  
                            $result = $this->exeSQL($query);

                            /*
                            * Coping data to a temprary table
                            */

                            $query = " 
                                CREATE TEMPORARY TABLE IF NOT EXISTS
                                    tmptable 
                                        SELECT 
                                            * 
                                        FROM 
                                            ".__DBC_SCHEMATA_SHIPPER_CONSIGNEE__." 
                                        WHERE 
                                            idBooking = '".$idBooking."' 
                            ";  
                           // echo "<br>".$query ;
                            $result = $this->exeSQL($query);

                            /*
                            * updating primary id and idBooking in temprary table
                            */
                            $query = " UPDATE tmptable SET id = '".$iNewShipperConsigneeID."', idBooking='".$iNewBookingID."' WHERE idBooking = '".$idBooking."' "; 
                            //echo "<br>".$query ;
                            $result = $this->exeSQL($query);

                            /*
                            * inserting data from temprary table to main table
                            */
                            $query = " INSERT INTO ".__DBC_SCHEMATA_SHIPPER_CONSIGNEE__." 
                                    SELECT 
                                        *
                                    FROM 
                                        tmptable
                                    WHERE 
                                        id = '".$iNewShipperConsigneeID."' 
                                    AND
                                        idBooking='".$iNewBookingID."'  
                            ";  
                            $result = $this->exeSQL($query);  
                        }
                        
                        $cargoDetailArr = array();
                        $cargoDetailArr=$kBooking->getCargoDeailsByBookingId($idBooking);
                        if(!empty($cargoDetailArr))
                        {
                            foreach($cargoDetailArr as $cargoDetailArrs)
                            {
                                $query=" SELECT max(id) as iMaxId FROM ".__DBC_SCHEMATA_CARGO__ ;
                                $result = $this->exeSQL($query);

                                $row=$this->getAssoc($result);
                                $iNewCargoID = $row['iMaxId'] + 1 ;

                                $query = " DROP TABLE tmptable ";  
                                $result = $this->exeSQL($query);

                                /*
                                * Coping data to a temprary table
                                */

                                $query = " 
                                    CREATE TEMPORARY TABLE IF NOT EXISTS
                                        tmptable 
                                            SELECT 
                                                * 
                                            FROM 
                                                ".__DBC_SCHEMATA_CARGO__." 
                                            WHERE 
                                                idBooking = '".$idBooking."' 
                                            AND 
                                                id = '".(int)$cargoDetailArrs['id']."'
                                ";  
                               // echo "<br>".$query ;
                                $result = $this->exeSQL($query);

                                /*
                                * updating primary id and idBooking in temprary table
                                */
                                $query = " UPDATE tmptable SET id = '".$iNewCargoID."', idBooking='".$iNewBookingID."' WHERE idBooking = '".$idBooking."' AND id = '".(int)$cargoDetailArrs['id']."' "; 
                                //echo "<br>".$query ;
                                $result = $this->exeSQL($query);

                                /*
                                * inserting data from temprary table to main table
                                */
                                $query = " INSERT INTO ".__DBC_SCHEMATA_CARGO__." 
                                        SELECT 
                                            *
                                        FROM 
                                            tmptable
                                        WHERE 
                                            id = '".$iNewCargoID."' 
                                        AND
                                            idBooking='".$iNewBookingID."'  
                                ";  
                                $result = $this->exeSQL($query);  
                            } 
                        }
                        
                        /* 
                         *  updating idShipperConsignee in tblbooking
                         */
                        
                        $kBooking = new cBooking();
                        
                        $szBookingRandomNum = $kBooking->getUniqueBookingKey(10);
                        $szBookingRandomNum = $kBooking->isBookingRandomNumExist($szBookingRandomNum);
            
                        $res_ary = array();
                        $res_ary['idShipperConsignee'] = $iNewShipperConsigneeID ; 
                        $res_ary['dtQuotationExpired'] = '' ; 
                        $res_ary['dtQuotationSent'] = '' ; 
                        
                        if($iRepeatBookingFlag || $iAnonymous)
                        { 
                            $res_ary['iQuotesStatus '] = 0;
                            $res_ary['iBookingQuotes '] = 0;
                        }
                        else
                        {
                            $res_ary['iQuotesStatus'] = __BOOKING_QUOTES_STATUS_NEW_BOOKING__ ;  
                            $res_ary['iBookingQuotes '] = 1;
                        }
                        
                        if($idBookingFile>0)
                        {
                            $res_ary['idFile'] = $idBookingFile ;  
                        }
                        
                        //This flag is set true when Admin clicks on TEST SEARCH button pending Tray=>Remind pane
                        if($iAnonymous)
                        {
                            $idAdmin = $_SESSION['admin_id'];
                            $dtCurrentDBTime = $kBooking->getRealNow();
                            
                            $iBookingType = __BOOKING_TYPE_AUTOMATIC__;
                            $res_ary['iBookingInitiatedByAdmin'] = 1;
                            $res_ary['iAnonymusBooking'] = 1;
                            $res_ary['idAdminInitiatedBy'] = $idAdmin;
                            $res_ary['dtBookingInitiatedByAdmin'] = $dtCurrentDBTime ; 
                        }
                        else
                        {
                            $iBookingType = __BOOKING_TYPE_RFQ__ ;
                            $res_ary['iBookingInitiatedByAdmin'] = 0;
                            $res_ary['iAnonymusBooking'] = 0;
                            $res_ary['idAdminInitiatedBy'] = '';
                            $res_ary['dtBookingInitiatedByAdmin'] = '' ;
                        }
                        
                        $res_ary['idBookingStatus'] = __BOOKING_STATUS_DRAFT__ ;
                        $res_ary['szBookingRandomNum'] = $szBookingRandomNum ;
                        $res_ary['szBookingRef'] = '';
                        $res_ary['iAcceptedTerms'] = '';
                        $res_ary['dtBookingConfirmed'] = '';
                        $res_ary['szPaymentStatus'] = '';
                        $res_ary['iPaymentType'] = '';
                        $res_ary['iPaymentProcess'] = '';
                        $res_ary['iInsuranceStatus'] = 0;
                        $res_ary['iInsuranceIncluded'] = 0;
                        $res_ary['szInsuranceInvoice'] = '';
                        $res_ary['szInvoice'] = ''; 
                        $res_ary['fCustomerPaidAmount'] = '';
                        $res_ary['szForwarderReference'] = '';
                        
                        $res_ary['idParentBooking'] = $idBooking;
                        $res_ary['idServiceProvider'] = 0;
                        $res_ary['szTrackingNumber'] = '';
                        $res_ary['dtLabelSent'] = '';
                        $res_ary['idServiceProviderProduct'] = '';
                        $res_ary['idServiceProviderProduct'] = '';
                        $res_ary['szInsuranceCreditNoteInvoice'] = '';
                        $res_ary['dtBookingCancelled'] = '';
                        $res_ary['iBookingStep'] = '';
                        $res_ary['iTransferConfirmed'] = '';
                        $res_ary['iTransferConfirmed'] = '';
                        $res_ary['iPaymentReceived'] = '';
                        $res_ary['dtLabelDownloaded'] = '';
                        $res_ary['idLabelDownloadedBy'] = "";
                        $res_ary['iBookingInitiatedByAdmin'] = ""; 
                        $res_ary['idAdminInitiatedBy'] = "";
                        $res_ary['dtBookingInitiatedByAdmin'] = "";
                        $res_ary['iBookingConfirmedByAdmin'] = "";
                        $res_ary['idAdminConfirmedBy'] = "";
                        $res_ary['iBookingType'] = $iBookingType;
                        $res_ary['iMarkupPaid'] = 0;
                        $res_ary['dtMarkupPaid'] = 0;
                        $res_ary['iMarkupPaymentReceived'] = 0;
                        
                        if($anotherFlag==true)
                        {
                            $res_ary['idUser'] = 0;
                            $res_ary['szFirstName'] = '';
                            $res_ary['szLastName'] = '';
                            $res_ary['szEmail'] = '';
                            $res_ary['szCustomerCompanyName'] = "";
                            $res_ary['szCustomerCompanyRegNo'] = "";
                            $res_ary['szCustomerAddress1'] = "";
                            $res_ary['szCustomerAddress2'] = "";
                            $res_ary['szCustomerAddress3'] = "";
                            $res_ary['szCustomerPostCode'] = "";
                            $res_ary['szCustomerCity'] = "";
                            $res_ary['szCustomerCountry'] = "";
                            $res_ary['szCustomerState'] = "";
                            $res_ary['idCustomerDialCode'] = "";
                            $res_ary['szCustomerPhoneNumber'] = "";
                            $res_ary['idCustomerCurrency'] = "";
                            $res_ary['szCustomerCurrency'] = "";
                            $res_ary['fCustomerExchangeRate'] = "";
                            $res_ary['fTotalPriceCustomerCurrency'] = "";
                            $res_ary['fPriceMarkupCustomerCurrency'] = "";
                        }

                        if(!empty($res_ary))
                        {
                            foreach($res_ary as $key=>$value)
                            {
                                $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                            }
                        }

                        $update_query = rtrim($update_query,",");   
                        $kBooking->updateDraftBooking($update_query,$iNewBookingID) ; 
                        $this->iNewBookingID = $iNewBookingID ;
                        
                        
                        $BookingFileName = __APP_PATH_ROOT__."/logs/customerBookingFile.log";
                        $strdata=array();
                        $strdata[0]=" \n\n *** Creating Booking File From copyBookingQuote function  ".date("Y-m-d H:i:s")."\n\n"; 
                        $strdata[1]="Booking Id: ".$iNewBookingID."\n" ;
                        file_log(true, $strdata, $BookingFileName);
                        
                        if($iRepeatBookingFlag && (int)$idCustomer>0){
                            
                            $kUser = new cUser();
                            $kUser->getUserDetails($idCustomer); 
                            
                            $iAddBookingFile = false;
                            if(__ENVIRONMENT__=='LIVE')
                            {
                                if(doNotCreateFileForThisDomainEmail($kUser->szEmail))
                                {
                                    $iAddBookingFile = true;
                                }
                                else
                                {
                                    $strdata=array();
                                    $strdata[0]=" \n\n *** Do Not Create File For Domain  From copyBookingQuote function ".date("Y-m-d H:i:s")."\n\n"; 
                                    $strdata[1]="Booking Id: ".$iNewBookingID."\n" ;
                                    $strdata[2]="Customer Id: ".$idCustomer."\n" ;
                                    $strdata[3]="Email Address: ".$kUser->szEmail."\n" ;
                                    file_log(true, $strdata, $BookingFileName);
                                }
                            }
                            else
                            {
                                $iAddBookingFile = true;
                            }
                            
                            if($iAddBookingFile)
                            {
                                $createBookingFileAry = array();
                                $createBookingFileAry['idBooking'] = $iNewBookingID ;
                                $createBookingFileAry['idUser'] = $idCustomer;  
                                if($postSearchAry['iSearchMiniVersion']==7 || $postSearchAry['iSearchMiniVersion']==8 || $postSearchAry['iSearchMiniVersion']==9)
                                {
                                    $createBookingFileAry['szFileStatus'] = 'S1';
                                    $createBookingFileAry['szTaskStatus'] = 'T1';
                                }
                                else
                                {
                                    $createBookingFileAry['szFileStatus'] = 'S120';
                                    $createBookingFileAry['szTaskStatus'] = 'T160';
                                }

                                $createBookingFileAry['idCustomerOwner'] = $kUser->idCustomerOwner ;
                                $createBookingFileAry['idFileOwner'] = $kUser->idCustomerOwner ;
                                $createBookingFileAry['iSearchType'] = 1;
                                
                                $strdata=array();
                                $strdata[0]=" \n\n *** Booking File Data For Domain  From copyBookingQuote function ".date("Y-m-d H:i:s")."\n\n"; 
                                $strdata[1]="Booking Id: ".$iNewBookingID."\n" ;
                                $strdata[2]="Customer Id: ".$idCustomer."\n" ;
                                //$strdata[3]="Booking File Data: ".print_r($createBookingFileAry)."\n" ;
                                file_log(true, $strdata, $BookingFileName);

                                $kRegisterShipCon = new cRegisterShipCon(); 
                                $kRegisterShipCon->addBookingFile($createBookingFileAry);
                                $idBookingFile = $kRegisterShipCon->idBookingFile ;
                                
                                $strdata=array();
                                $strdata[0]=" \n\n *** Booking File ID Created For Domain  From copyBookingQuote function ".date("Y-m-d H:i:s")."\n\n"; 
                                $strdata[1]="Booking Id: ".$iNewBookingID."\n" ;
                                $strdata[2]="Customer Id: ".$idCustomer."\n" ;
                                $strdata[3]="Booking File ID: ".$idBookingFile."\n" ;
                                file_log(true, $strdata, $BookingFileName);

                                $res_ary = array();
                                $res_ary['idFile'] = $idBookingFile;	

                                if(!empty($res_ary))
                                {
                                    $update_query = '';
                                    foreach($res_ary as $key=>$value)
                                    {
                                        $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                                    } 
                                    $update_query = rtrim($update_query,","); 
                                    //echo $update_query;
                                    $kBooking->updateDraftBooking($update_query,$iNewBookingID);
                                }
                            }
                        }
                    }  
                    return true ; 
                }
                else
                {
                    return false;
                }
            }
        }
        
        function updteShipperConsigneeDraft($update_str,$idShipperConsignee)
	{
            if(!empty($update_str) && $idShipperConsignee>0)
            {
                $query="	
                    UPDATE
                        ".__DBC_SCHEMATA_SHIPPER_CONSIGNEE__."
                    SET
                        ".$update_str.",
                        dtUpdatedOn = now()
                    WHERE
                        id = '".(int)$idShipperConsignee."';
                "; 
                if($result=$this->exeSQL($query))
                {
                    return true;
                }
                else
                { 
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	} 
        function updateShipperConsigneePartialDetails($searchFormAry,$idShipperConsignee)
        {
            if(!empty($searchFormAry) && $idShipperConsignee>0)
            { 
                $updateShipConsAry = array();
                $updateShipConsAry['szShipperPostCode'] = $searchFormAry['szOriginPostCode'];
                $updateShipConsAry['szShipperCity'] = $searchFormAry['szOriginCity'];
                $updateShipConsAry['idShipperCountry'] = $searchFormAry['szOriginCountry'];
                $updateShipConsAry['szShipperPostCode_pickup'] = $searchFormAry['szOriginPostCode'];
                $updateShipConsAry['szShipperCity_pickup'] = $searchFormAry['szOriginCity'];
                $updateShipConsAry['idShipperCountry_pickup'] = $searchFormAry['szOriginCountry']; 
                $updateShipConsAry['szConsigneePostCode'] = $searchFormAry['szOriginPostCode'];
                $updateShipConsAry['szConsigneeCity'] = $searchFormAry['szOriginCity'];
                $updateShipConsAry['idConsigneeCountry'] = $searchFormAry['szOriginCountry'];
                $updateShipConsAry['szConsigneePostCode_pickup'] = $searchFormAry['szOriginPostCode'];
                $updateShipConsAry['szConsigneeCity_pickup'] = $searchFormAry['szOriginCity'];
                $updateShipConsAry['idConsigneeCountry_pickup'] = $searchFormAry['szOriginCountry'];
 
                if(!empty($updateShipConsAry))
                {
                    foreach($updateShipConsAry as $key=>$value)
                    {
                        $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                }
                $update_query = rtrim($update_query,",");
                //$this->updteShipperConsigneeDraft($update_query,$idShipperConsignee);
            }
        }
        
        function updateShipperConsigneeApiDetails($update_str,$idShipperConsignee)
	{
            if(!empty($update_str) && $idShipperConsignee>0)
            {
                if(trim(cPartner::$szApiRequestMode) == __API_REQUEST_MODE_TEST__)
                {
                    $table = __DBC_SCHEMATA_REGISTER_SHIPPER_CONSIGNEES__;
                }
                else
                {
                    $table = __DBC_SCHEMATA_REGISTER_SHIPPER_CONSIGNEES__;
                }
                
                
                $query="	
                    UPDATE
                        ".$table."
                    SET
                        ".$update_str." 
                    WHERE
                        id = '".(int)$idShipperConsignee."';
                ";
                echo $query ;
                //die;
                if($result=$this->exeSQL($query))
                {
                    return true;
                }
                else
                {
                    //echo " not updated ";
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            }
	}
        
	function set_id( $value )
	{
            $this->id = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "id", t($this->t_base.'fields/customer_id'), false, false, true );
	}
	function set_idGroup( $value )
	{
            $this->idGroup = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "id", t($this->t_base.'fields/group_id'), false, false, true );
	}
	function set_szFirstName( $value )
	{
            $this->szFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szFirstName", t($this->t_base.'fields/f_name'), false, 255, true );
	}
	function set_szLastName( $value )
	{
            $this->szLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szLastName", t($this->t_base.'fields/l_name'), false, 255, true );
	}
	
        function set_idCustomerCurrency( $value,$flag=true )
	{
            $this->idCustomerCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idCustomerCurrency", t($this->t_base.'fields/idCustomerCurrency'), false, 255, $flag );
	}
        function set_idForwarder( $value,$flag=true )
	{
            $this->idForwarder = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idForwarder", t($this->t_base.'fields/idForwarder'), false, 255, $flag );
	}
        function set_szServiceDescription( $value,$flag=true )
	{
		$this->szServiceDescription = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szServiceDescription", t($this->t_base.'fields/ServiceDescription'), false, 255, $flag );
	}
        function set_dtQuotesValidTo( $value,$flag=true )
	{
            $this->dtQuotesValidTo = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "dtQuotesValidTo", t($this->t_base.'fields/dtQuotesValidTo'), false, 255, $flag );
	}
        function set_idTransportMode( $value,$flag=true )
	{
            $this->idTransportMode = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idTransportMode", t($this->t_base.'fields/TransportMode'), false, false, $flag );
	}
        function set_iTransitHours( $value,$flag=true )
	{
            $this->iTransitHours = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iTransitHours", t($this->t_base.'fields/TransitHours'), false, false, $flag );
	} 
        function set_szFrequency( $value,$flag=true )
	{
            $this->szFrequency = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szFrequency", t($this->t_base.'fields/Frequency'), false, false, $flag );
	}
        function set_fBookingAmount( $value,$flag=true )
	{
            $this->fBookingAmount = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fBookingAmount", t($this->t_base.'fields/BookingAmount'), false, false, $flag );
	} 
        function set_fReferalFee( $value,$flag=true )
	{
            $this->fReferalFee = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fReferalFee", t($this->t_base.'fields/fReferalFee'), false, 100, $flag );
	} 
        function set_fInsuranceSellPrice( $value,$flag=true )
	{
            $this->fInsuranceSellPrice = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fInsuranceSellPrice", t($this->t_base.'fields/InsuranceSellPrice'), false, false, $flag );
	} 
        function set_fInsuranceBuyPrice( $value,$flag=true )
	{
            $this->fInsuranceBuyPrice = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fInsuranceBuyPrice", t($this->t_base.'fields/fInsuranceBuyPrice'), false, false, $flag );
	}
        function set_fInsuranceValue( $value,$flag=true )
	{
            $this->fInsuranceValue = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fInsuranceValue", t($this->t_base.'fields/fInsuranceValue'), false, false, $flag );
	}
        function set_idInsuranceCurrency( $value,$flag=true )
	{
            $this->idInsuranceCurrency = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idInsuranceCurrency", t($this->t_base.'fields/fInsuranceValue'), false, false, $flag );
	}
        
        function set_szOtherComments( $value,$flag=true )
	{
            $this->szOtherComments = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szOtherComments", t($this->t_base.'fields/szOtherComments'), false, false, $flag );
	}
        function set_fTotalVat( $value,$flag=true )
	{
            $this->fTotalVat = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fTotalVat", t($this->t_base.'fields/fTotalVat'), false, false, $flag );
	}
	function set_szState( $value )
	{
		$this->szState = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szState", t($this->t_base.'fields/p_r_s'), false, 255, false );
	}
	function set_szCountry( $value )
	{
            $this->szCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCountry", t($this->t_base.'fields/country'), false, 255, true );
	}
        function set_iInternationalDialCode( $value )
	{
            $this->iInternationalDialCode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "iInternationalDialCode", t($this->t_base.'fields/country'), false, false, false );
	}
        
	function set_szPhoneNumber( $value )
	{
		$this->szPhoneNumber = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPhoneNumber", t($this->t_base.'fields/p_number'), false, false, false );
	}
	function set_szCompanyName( $value )
	{
		$this->szCompanyName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCompanyName", t($this->t_base.'fields/c_name'), false, 255, true );
	}
        function set_szConsigneeTitleText( $value,$flag=true )
	{
		$this->szConsigneeHeading = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeHeading", t($this->t_base.'fields/consignee_title'), false, 255, $flag );
	}
        function set_szShipperTitleText( $value,$flag=true )
	{
		$this->szShipperHeading = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperHeading", t($this->t_base.'fields/ShipperTitleText'), false, 255, $flag );
	} 
	function set_szAddress1( $value,$flag=true)
	{
            $this->szAddress1 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAddress1", t($this->t_base.'fields/address'), false, 255, $flag );
	}
	function set_szAddress2( $value )
	{
            $this->szAddress2 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAddress2", t($this->t_base.'fields/address_line2'), false, 255, false );
	}
	function set_szAddress3( $value )
	{
            $this->szAddress3 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szAddress3", t($this->t_base.'fields/address_line3'), false, 255, false );
	}
        function set_szPostcode( $value )
	{
            $this->szPostcode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szPostcode", t($this->t_base.'fields/postcode'), false, 255, true );
	}
	function set_szEmail( $value )
	{
		$this->szEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szEmail", t($this->t_base.'fields/email'), false, 255, false );
	}
        function set_szUserEmail( $value )
	{
		$this->szUserEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szUserEmail", t($this->t_base.'fields/email'), false, 255, false );
	}
        
	function set_szCity( $value )
	{
		$this->szCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCity", t($this->t_base.'fields/city'), false, 255, false );
	}
	
        function set_szShipperFirstName( $value,$required_flag=true )
	{
		$this->szShipperFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperFirstName", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/f_name'), false, 255, $required_flag );
	}
	function set_szShipperLastName( $value ,$required_flag=true)
	{
		$this->szShipperLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperLastName", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/l_name'), false, 255, $required_flag );
	}
	
	function set_szShipperCompanyName( $value,$required_flag=true )
	{
		$this->szShipperCompanyName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperCompanyName", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/c_name'), false, 255, $required_flag );
	}
	function set_szShipperAddress( $value ,$required_flag=true)
	{
		$this->szShipperAddress = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperAddress1", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/address'), false, 255, $required_flag );
	}
	function set_szShipperAddress2( $value )
	{
		$this->szShipperAddress2 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperAddress2", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/address_line2'), false, 255, false );
	}
	function set_szShipperAddress3( $value )
	{
		$this->szShipperAddress3 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperAddress3", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/address_line3'), false, 255, false );
	}
        function set_szShipperPostcode( $value,$flag=true)
	{
		$this->szShipperPostcode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperPostcode", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/postcode'), false, 255, $flag );
	}
	function set_szShipperEmail( $value,$flag=false )
	{
		$this->szShipperEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szShipperEmail", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/email'), false, 255, $flag );
	}
	function set_szShipperCity( $value,$flag=false)
	{
		$this->szShipperCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperCity", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/city'), false, 255, $flag );
	}
	function set_szShipperState( $value )
	{
		$this->szShipperState = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperState", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/state'), false, 255, false );
	}
        function set_idShipperCountry( $value,$flag=true )
	{
		$this->idShipperCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idShipperCountry", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/country'), false, 255, $flag );
	}
	
	function set_idShipperDialCode( $value ,$flag=true)
	{
		$this->idShipperDialCode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idShipperDialCode", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/country'), false, 255, $flag );
	}
	function set_idConsigneeDialCode( $value,$flag=true )
	{
            $this->idConsigneeDialCode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idConsigneeDialCode", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/country'), false, 255, $flag );
	} 
        function set_szShipperPhone( $value,$flag=false )
	{
            $this->szShipperPhone = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperPhone", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/p_number'), false, false, $flag );
	} 
        function set_iGoogleLogin($value)
	{
            $this->iGoogleLogin = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iGoogleLogin", '', false, false, false );
	}        
        function set_iFaceBookLogin($value)
	{
            $this->iFaceBookLogin = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iFaceBookLogin", '', false, false, false );
	} 
	function set_szShipperCity_pickup( $value,$flag=false )
	{
		$this->szShipperCity_pickup = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperCity_pickup", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/city_pickup'), false, 255, $flag );
	}
	function set_szShipperState_pickup( $value )
	{
		$this->szShipperState_pickup = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperState_pickup", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/state_pickup'), false, 255, false );
	}
    function set_idShipperCountry_pickup( $value )
	{
		$this->idShipperCountry_pickup = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idShipperCountry_pickup", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/country_pickup'), false, 255, true );
	}
    function set_szShipperAddress_pickup( $value )
	{
		$this->szShipperAddress_pickup = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperAddress1_pickup", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/address_pickup'), false, 255, true );
	}
	function set_szShipperAddress2_pickup( $value )
	{
		$this->szShipperAddress2_pickup = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperAddress2_pickup", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/address_line2_pickup'), false, 255, false );
	}
	function set_szShipperAddress3_pickup( $value )
	{
		$this->szShipperAddress3_pickup = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperAddress3_pickup", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/address_line3_pickup'), false, 255, false );
	}
        function set_szShipperPostcode_pickup( $value,$flag=true)
	{
		$this->szShipperPostcode_pickup = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szShipperPostcode_pickup", t($this->t_base.'fields/shipper')." ".t($this->t_base.'fields/postcode_pickup'), false, 255, $flag );
	}
        function set_szConsigneeFirstName( $value,$required_flag=true )
	{
		$this->szConsigneeFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeFirstName", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/f_name'), false, 255, $required_flag );
	}
	function set_szConsigneeLastName( $value,$required_flag=true )
	{
		$this->szConsigneeLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeLastName", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/l_name'), false, 255, $required_flag );
	}
	
	function set_szConsigneeCompanyName( $value,$required_flag=true )
	{
		$this->szConsigneeCompanyName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeCompanyName", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/c_name'), false, 255, $required_flag );
	}
	function set_szConsigneeAddress( $value ,$required_flag=true)
	{
		$this->szConsigneeAddress = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeAddress1", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/address'), false, 255, $required_flag );
	}
	function set_szConsigneeAddress2( $value )
	{
		$this->szConsigneeAddress2 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeAddress2", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/address_line2'), false, 255, false );
	}
	function set_szConsigneeAddress3( $value )
	{
		$this->szConsigneeAddress3 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeAddress3", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/address_line3'), false, 255, false );
	}
    function set_szConsigneePostcode( $value ,$flag=true)
	{
		$this->szConsigneePostcode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneePostcode", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/postcode'), false, 255, $flag );
	}
	function set_szConsigneeEmail( $value ,$flag=false)
	{
		$this->szConsigneeEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szConsigneeEmail", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/email'), false, 255, $flag );
	}
	function set_szConsigneeCity( $value,$flag=false)
	{
		$this->szConsigneeCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeCity", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/city'), false, 255, $flag );
	}
	function set_szConsigneeState( $value )
	{
		$this->szConsigneeState = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeState", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/state'), false, 255, false );
	}
        function set_idConsigneeCountry( $value,$flag=true )
	{
		$this->idConsigneeCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idConsigneeCountry", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/country'), false, 255, $flag );
	}
        function set_szConsigneePhone( $value,$flag=false )
	{
		$this->szConsigneePhone = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneePhone", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/p_number'), false, false, $flag );
	}
	
    function set_idServiceType( $value ,$required_flag=true)
	{
		$this->idServiceType = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idServiceType",t($this->t_base.'fields/service_type'), false, 10, $required_flag );
	}
	
	function set_szConsigneeAddress_pickup( $value )
	{
		$this->szConsigneeAddress_pickup = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeAddress1_pickup", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/address_delivery'), false, 255, true );
	}
	function set_szConsigneeAddress2_pickup( $value )
	{
		$this->szConsigneeAddress2_pickup = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeAddress2_pickup", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/address_line2_delivery'), false, 255, false );
	}
	function set_szConsigneeAddress3_pickup( $value )
	{
		$this->szConsigneeAddress3_pickup = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeAddress3_pickup", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/address_line3_delivery'), false, 255, false );
	}
    function set_szConsigneePostcode_pickup( $value,$flag=true)
	{
		$this->szConsigneePostcode_pickup = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneePostcode_pickup", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/postcode_delivery'), false, 255, $flag );
	}
	function set_szConsigneeCity_pickup( $value,$flag=false)
	{
		$this->szConsigneeCity_pickup = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeCity_pickup", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/city_delivery'), false, 255, $flag );
	}
	function set_szConsigneeState_pickup( $value )
	{
		$this->szConsigneeState_pickup = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szConsigneeState_pickup", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/state_delivery'), false, 255, false );
	}
    function set_idConsigneeCountry_pickup( $value )
	{
		$this->idConsigneeCountry_pickup = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idConsigneeCountry_pickup", t($this->t_base.'fields/Consignee')." ".t($this->t_base.'fields/country_delivery'), false, 255, true );
	}
		
	function set_szCargoCommodity( $value,$counter=false,$required_flag=true)
	{
            if(!$counter)
            {
                $this->szCargoCommodity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szCargoCommodity", t($this->t_base.'fields/commodity'), true, false, $required_flag );
            }
            else if(empty($value))
            {
                $this->addError('fCargoWeight_landing_page',t($this->t_base.'errors/accurate_cargo_description')); 
                return false;
            }
            else
            {
                    $this->szCargoCommodity[$counter] = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "fCargoWeight_landing_page", t($this->t_base.'fields/commodity'), true, false, $required_flag );
            }
	}
	function set_szBillingCompanyName( $value ,$required_flag=true)
	{
		$this->szBillingCompanyName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szBillingCompanyName", t($this->t_base.'fields/billing')." ".t($this->t_base.'fields/c_name'), false, 255, $required_flag );
	}
	function set_szBillingAddress( $value,$required_flag=true )
	{
		$this->szBillingAddress = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szBillingAddress1", t($this->t_base.'fields/billing')." ".t($this->t_base.'fields/address'), false, 255, $required_flag );
	}
	function set_szBillingAddress2( $value,$required_flag=false )
	{
		$this->szBillingAddress2 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szBillingAddress2", t($this->t_base.'fields/billing')." ".t($this->t_base.'fields/address_line2'), false, 255, $required_flag );
	}
	function set_szBillingAddress3( $value )
	{
		$this->szBillingAddress3 = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szBillingAddress3", t($this->t_base.'fields/billing')." ".t($this->t_base.'fields/address_line3'), false, 255, false );
	}
        function set_szBillingPostcode( $value ,$flag=true)
	{
            $this->szBillingPostcode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szBillingPostcode", t($this->t_base.'fields/billing')." ".t($this->t_base.'fields/postcode'), false, 255, $flag );
	}
	function set_szBillingEmail( $value ,$flag=false)
	{
            $this->szBillingEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szBillingEmail", t($this->t_base.'fields/billing')." ".t($this->t_base.'fields/email'), false, 255, $flag );
	}
	function set_szBillingCity( $value,$flag=false)
	{
            $this->szBillingCity = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szBillingCity", t($this->t_base.'fields/billing')." ".t($this->t_base.'fields/city'), false, 255, $flag );
	}
	function set_szBillingState( $value )
	{
            $this->szBillingState = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szBillingState", t($this->t_base.'fields/billing')." ".t($this->t_base.'fields/state'), false, 255, false );
	}
        function set_idBillingCountry( $value,$flag=true )
	{
            $this->idBillingCountry = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idBillingCountry", t($this->t_base.'fields/billing')." ".t($this->t_base.'fields/country'), false, 255, $flag );
	}
        function set_szBillingPhone( $value,$flag=false )
	{
            $this->szBillingPhone = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szBillingPhone", t($this->t_base.'fields/billing')." ".t($this->t_base.'fields/p_number'), false, false, $flag );
	}
	 
 	function set_szBillingFirstName( $value,$required_flag=true )
	{
            $this->szBillingFirstName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szBillingFirstName", t($this->t_base.'fields/billing')." ".t($this->t_base.'fields/f_name'), false, 255, $required_flag );
	}
	function set_szBillingLastName( $value,$required_flag=true )
	{
            $this->szBillingLastName = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "szBillingLastName", t($this->t_base.'fields/billing')." ".t($this->t_base.'fields/l_name'), false, 255, $required_flag );
	}
	function set_idBillingDialCode( $value,$required_flag=true )
	{
            $this->idBillingDialCode = $this->validateInput( $value, __VLD_CASE_ANYTHING__, "idBillingDialCode", t($this->t_base.'fields/billing')." ".t($this->t_base.'fields/country'), false, 255, $required_flag );
	} 
	function set_idCargo( $value )
	{
            $this->idCargo = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "idCargo", t($this->t_base.'fields/customer_id'), false, false, false );
	}
        function set_fCargoVolume( $value,$flag=true )
	{
            $this->fCargoVolume = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fCargoVolume", t($this->t_base.'fields/CargoVolume'), false, false, $flag );
	}
        function set_fCargoWeight( $value,$flag=true )
	{
            $this->fCargoWeight = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "fCargoWeight", t($this->t_base.'fields/fCargoWeight'), false, false, $flag );
	} 
        function set_iShipperConsignee( $value,$flag=false )
	{
            $this->iShipperConsignee = $this->validateInput( $value, __VLD_CASE_NUMERIC__, "iShipperConsignee", t($this->t_base.'fields/iShipperConsignee'), false, false, $flag );
	} 
        
        
	
}
?>