<?php
/**
 * This file is the container for pending tray related functionality. 
 *
 * pendingTray.class.php
 *
 * @copyright Copyright (C) 2016 Transporteca
 * @author Ajay
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" ); 
require_once( __APP_PATH__ . "/inc/functions.php" ); 

class cPendingTray extends cDatabase
{    
    function __construct()
    {
        parent::__construct();
        return true;
    }
    
    function getAllBookingWithOpenedTask($data,$row_count_flag=false,$ret_num_rows=false,$iPageNumber=false)
    {  
        if(!empty($data))
        { 
            if(count($data['idFileOwnerAry'])>0)
            {
                if((int)$data['iDonotAddNotServiceAdmin']==0)
                {
                    $kAdmin = new cAdmin();
                    $activeAdminNotCustomerServiceAgentAry = $kAdmin->getAllActiveStoreAdmin($_SESSION['admin_id'],false,true);
                    $data['idFileOwnerAry']=  array_merge($data['idFileOwnerAry'],$activeAdminNotCustomerServiceAgentAry['idFileOwnerAry']);
                }
                $idFileOwnerStr = implode(",",$data['idFileOwnerAry']);
                $query_and = " AND f.idFileOwner IN (".$idFileOwnerStr.") " ;
            }

            if(!empty($data['iInsuranceIncluded']) && ($data['iInsuranceIncluded']!=10001))
            {
                $query_and .= " AND b.iInsuranceChoice = '".(int)$data['iInsuranceIncluded']."' " ;
            }
            if(!empty($data['idFileOwner']) && ($data['idFileOwner']!=10001))
            { 
                if(trim($data['idFileOwner'])==10002) //Not Allocated
                {
                    $data['idFileOwner'] = 0 ;
                } 
                $query_and .= " AND f.idFileOwner = '".(int)$data['idFileOwner']."' " ;
            }
            if($data['idCustomerOwner']>0)
            {
                $query_and .= " AND f.idCustomerOwner = '".(int)$data['idCustomerOwner']."' " ;
            }
            if(!empty($data['szBookingRef']))
            {
                $query_and .= " AND b.szBookingRef LIKE '%".mysql_escape_custom(trim($data['szBookingRef']))."%' " ;
            } 
            if($data['idCustomer']>0)
            {
                $query_and .= " AND b.idUser = '".(int)$data['idCustomer']."' " ;
            }

            if($data['idForwarder']>0 || !empty($data['idForwarderContact']))
            {
                $idForwarder = false;
                $searchAry['iReturnFileIds'] = 1 ;
                if($data['idForwarder']>0)
                {
                    $idForwarder = $data['idForwarder'] ;
                }
                if(!empty($data['idForwarderContact']))
                {
                    $searchAry['idForwarderContact'] = $data['idForwarderContact'] ;
                }   

                $bookingFileIdAry = array();
                //$bookingFileIdAry = $this->getAllBookingQuotesByForwarder($idForwarder,false,false,false,false,$searchAry);

                if(!empty($bookingFileIdAry))
                {
                    $file_id_str = implode(",",$bookingFileIdAry);
                    $query_and .= " AND f.id IN (".$file_id_str.") ";
                } 
                else
                {
                    //This means do not show any record as we dont have any file for selected forwarder
                    //$query_and .= " AND f.id IN (0) ";
                }  
            }

            if(!empty($data['szFreeText']))
            {    
                if(!empty($bookingFileIDAry))
                {
                    $bookingFileIDStr = implode(",",$bookingFileIDAry);
                    $query_and .= " OR f.id IN (".$bookingFileIDStr.") ";
                } 

                if((int)$data['szFreeText']>0)
                {
                    //$query_or = " OR b.fValueOfGoods = '".mysql_escape_custom(trim($data['szFreeText']))."' ";    
                }

                $query_and .= " AND ( f.szFileRef LIKE '%".mysql_escape_custom(trim($data['szFreeText']))."%' "
                    . " OR f.szTransportecaStatus LIKE '%".mysql_escape_custom(trim($data['szFreeText']))."%' "
                    . " OR f.szTransportecaTask LIKE '%".mysql_escape_custom(trim($data['szFreeText']))."%' "
                    . " OR f.szCommentToForwarder = '".mysql_escape_custom(trim($data['szFreeText']))."' " 
                    . " OR b.szOriginCountry LIKE '%".mysql_escape_custom(trim($data['szFreeText']))."%' "
                    . " OR b.szFirstName LIKE '%".mysql_escape_custom(trim($data['szFreeText']))."%' "
                    . " OR ( CONCAT_WS(' ',szFirstName,szLastName) LIKE '%".mysql_escape_custom(trim($data['szFreeText']))."%') "  
                    . " OR b.szEmail LIKE '%".mysql_escape_custom(trim($data['szFreeText']))."%' "
                    . " OR b.szLastName LIKE '%".mysql_escape_custom(trim($data['szFreeText']))."%' "
                    . " OR b.szTrackingNumber LIKE '%".mysql_escape_custom(trim($data['szFreeText']))."%' "
                    . " OR b.szTrackingTag LIKE '%".mysql_escape_custom(trim($data['szFreeText']))."%' "
                    . " OR b.szTrackingTagDescription LIKE '%".mysql_escape_custom(trim($data['szFreeText']))."%' "
                    . " OR b.szCustomerCompanyName LIKE '%".mysql_escape_custom(trim($data['szFreeText']))."%' "
                    . " OR b.szCustomerPhoneNumber LIKE '%".mysql_escape_custom(trim($data['szFreeText']))."%' " 
                    . " OR b.szOriginPostCode LIKE '%".mysql_escape_custom(trim($data['szFreeText']))."%' "
                    . " OR b.szOriginCity LIKE '%".mysql_escape_custom(trim($data['szFreeText']))."%' "
                    . " OR b.szDestinationCountry LIKE '%".mysql_escape_custom(trim($data['szFreeText']))."%' "
                    . " OR b.szDestinationCity LIKE '%".mysql_escape_custom(trim($data['szFreeText']))."%' "
                    . " OR b.szDestinationPostCode LIKE '%".mysql_escape_custom(trim($data['szFreeText']))."%' "
                    . " OR b.szTransportMode LIKE '%".mysql_escape_custom(trim($data['szFreeText']))."%' "
                    . " OR b.szInternalComment LIKE '%".mysql_escape_custom(trim($data['szFreeText']))."%' "
                    . " OR b.szForwarderComment LIKE '%".mysql_escape_custom(trim($data['szFreeText']))."%' "  
                    . " OR b.szInternalComment LIKE '%".mysql_escape_custom(trim($data['szFreeText']))."%' "  
                    . " OR b.szBookingRef LIKE '%".mysql_escape_custom(trim($data['szFreeText']))."%' "  
                    . " OR b.szBookingRandomNum LIKE '%".mysql_escape_custom(trim($data['szFreeText']))."%' "  
                    . " OR (CASE WHEN st.szShortTerms is NULL THEN b.szForwarderComment ELSE st.szShortTerms END) LIKE '%". mysql_escape_custom(trim($data['szFreeText']))  ."%' "  
                    . " OR (CASE WHEN st.szLongTerms is NULL THEN b.szForwarderComment ELSE st.szLongTerms END) LIKE '%". mysql_escape_custom(trim($data['szFreeText']))  ."%' "  
                    . " OR (CASE WHEN b.szCargoDescription is NULL THEN b.szForwarderComment ELSE b.szCargoDescription END) LIKE '%". mysql_escape_custom(trim($data['szFreeText']))  ."%' "  
                    . " OR (CASE WHEN qt.szTransportMode is NULL THEN b.szForwarderComment ELSE qt.szTransportMode END) LIKE '%". mysql_escape_custom(trim($data['szFreeText']))  ."%' "   
                    . $query_or
                    . " ) ";   
            } 
            if($data['idTransportMode']>0)
            {
                $query_and .= " AND b.idTransportMode = '".(int)$data['idTransportMode']."' " ;
            }
            if($data['fCargoVolume']>0)
            {
                $query_and .= " AND b.fCargoVolume = '".(float)$data['fCargoVolume']."' " ;
            }
            if($data['fCargoWeight']>0)
            {
                $query_and .= " AND b.fCargoWeight = '".(float)$data['fCargoWeight']."' " ;
            }
            if($data['iNumColli']>0)
            {
                $query_and .= " AND b.iNumColli = '".(int)$data['iNumColli']."' " ;
            } 
            if(!empty($data['dtRequestReceivedAfter']))
            {
                $data['dtRequestReceivedAfter'] = format_date_time($data['dtRequestReceivedAfter']);
                $query_and .= " AND f.dtCreatedOn >= '".mysql_escape_custom(trim($data['dtRequestReceivedAfter']))."' " ;
            }
            if(!empty($data['dtRequestReceivedBefore']))
            {
                $data['dtRequestReceivedBefore'] = format_date_time($data['dtRequestReceivedBefore']);
                $query_and .= " AND f.dtCreatedOn <= '".mysql_escape_custom(trim($data['dtRequestReceivedBefore']))."' " ;
            }
            if(!empty($data['szFileRef']))
            {
                $query_and .= " AND f.szFileRef LIKE '%".mysql_escape_custom(trim($data['szFileRef']))."%' " ;
            }
            if(!empty($data['szCustomerName']))
            {
                $query_and .= " AND ( (CONCAT_WS(' ',szFirstName,szLastName) LIKE '%".mysql_escape_custom(trim($data['szCustomerName']))."%')  OR (szLastName LIKE '%".mysql_escape_custom(trim($data['szCustomerName']))."%') OR (szFirstName LIKE '%".mysql_escape_custom(trim($data['szCustomerName']))."%') ) ";
            } 
            if(!empty($data['szFileStatus']))
            {
                $query_and .= " AND f.szTransportecaStatus = '".mysql_escape_custom(trim($data['szFileStatus']))."' " ;
            }
            if(!empty($data['szOriginCity']))
            {
                $query_and .= " AND b.szOriginCity LIKE '%".mysql_escape_custom(trim($data['szOriginCity']))."%' " ;
            }
            if(!empty($data['szDestinationCity']))
            {
                $query_and .= " AND b.szDestinationCity LIKE '%".mysql_escape_custom(trim($data['szDestinationCity']))."%' " ;
            }
            if(!empty($data['szDestinationCity']))
            {
                $query_and .= " AND b.szDestinationCity LIKE '%".mysql_escape_custom(trim($data['szDestinationCity']))."%' " ;
            }
            if(!empty($data['szCompanyName']))
            {
                $query_and .= " AND ( (CONCAT_WS(' - ',CONCAT_WS (' ',szFirstName,szLastName),szCustomerCompanyName) LIKE '%".mysql_escape_custom(trim($data['szCompanyName']))."%') OR (szLastName LIKE '%".mysql_escape_custom(trim($data['szCompanyName']))."%') OR (szCustomerCompanyName LIKE '%".mysql_escape_custom(trim($data['szCompanyName']))."%') ) ";
               // $query_and .= " AND b.szCustomerCompanyName = '".mysql_escape_custom(trim($data['szCompanyName']))."' " ;
            } 
        }

        if(!$row_count_flag)
        {
            $query_having = " GROUP BY f.id "; 
        }

        if($data['iRecentPopup']==1)
        {
            $query_limit = " LIMIT 0,20 ";
        }
        else if($data['iFromPopup']==1)
        {
            if($iPageNumber>0)
            {
                $iRecordPerPage = __CONTENT_PER_PAGE_PENDING_TRAY_SEARCH__; 
                $query_limit ="
                    LIMIT
                        ".(($iPageNumber -1 ) * $iRecordPerPage ).",". $iRecordPerPage ."
                ";
            }
        }

        if($data['iFromPopup']!=1)
        {
            $query_and .= " AND f.szTransportecaTask != '' AND f.iClosed = '0' ";
            $query_order_by = " ORDER BY f.dtTaskStatusUpdatedOn ASC, f.szTransportecaTask ASC, b.szFirstName ASC, b.szLastName ASC";
        }
        else
        { 
            $query_order_by = " ORDER BY f.dtUpdatedOn DESC, f.szTransportecaTask ASC, b.szFirstName ASC, b.szLastName ASC";
        }
        $kConfig = new cConfig();
        $pendingTaskAry = array();
        $pendingTaskAry = $kConfig->getAllActiveTaskStatus();
         
        if($ret_num_rows)
        {
            $query_select = " f.id as idBookingFile";
        }
        else if($row_count_flag)
        {
            $query_select = " count(f.id) as iNumQuotes ";
        }
        else
        {
            $query_select = " 
                f.id as idBookingFile,
                f.idBooking,
                f.szFileRef,
                f.szTransportecaStatus as szFileStatus,
                f.szTransportecaTask as szTaskStatus,
                f.dtFileStatusUpdatedOn,
                f.dtTaskStatusUpdatedOn,
                f.idFileOwner,
                f.dtCreatedOn,
                b.idUser,
                b.idServiceType, 
                b.idOriginCountry,	 
                b.szOriginCountry,  
                b.szOriginPostCode,
                b.szOriginCity,
                b.idDestinationCountry,	 
                b.szDestinationCountry, 
                b.szDestinationCity,
                b.szDestinationPostCode,
                b.idTimingType,
                b.dtTimingDate,
                b.idCustomerCurrency as idCurrency,
                b.szCustomerCurrency as szCurrency,
                b.fCustomerExchangeRate as fExchangeRate,
                b.fOriginLatitude,
                b.fOriginLongitude,
                b.fDestinationLatitude,
                b.fDestinationLongitude,
                b.fCargoVolume,
                b.fCargoWeight,
                b.idBookingStatus,
                b.fTotalPriceCustomerCurrency,
                b.idForwarder,
                b.idWarehouseFrom,
                b.idWarehouseTo,
                b.idShipperConsignee,
                b.dtCreatedOn,
                b.fPriceForSorting,
                b.dtAvailable,
                b.idUser,
                b.szFirstName,
                b.szLastName,
                b.szCustomerCompanyName,
                b.szEmail,
                b.dtCutOff,
                b.dtAvailable, 
                b.szBookingRandomNum, 
                b.idBookingStatus, 
                b.dtTimingDate, 
                b.szBookingRef, 
                b.idCustomerCurrency,
                b.fTotalPriceUSD,
                b.iInsuranceIncluded,
                b.fValueOfGoods,
                b.idGoodsInsuranceCurrency,
                b.fTotalInsuranceCostForBookingCustomerCurrency,
                b.fInsuranceRate,
                b.iBookingLanguage, 
                b.idLandingPage,
                b.dtQuoteValidTo,
                b.iQuotesStatus,
                b.fTotalInsuranceCostForBookingCustomerCurrency_buyRate,
                b.fTotalVat,
                b.szTransportMode,
                b.idTransportMode,
                b.iNumColli,
                b.idCustomerDialCode,
                b.szBookingRef,
                f.iBookingFlag,
                b.iCourierBooking,
                f.iClosed,
                f.iNumUnreadMessage,
                f.idReminderTask,
                f.idOfflineChat
            ";
        } 
        $query="
            SELECT 
                $query_select
            FROM
                ".__DBC_SCHEMATA_FILE_LOGS__." AS f
            INNER JOIN
                ".__DBC_SCHEMATA_BOOKING__." AS b
            ON
                b.id = f.idBooking 
            WHERE
                f.isDeleted = '0' 
            AND
                b.idBookingStatus != '5'
            AND
                b.iCreateAutomaticQuote='0'
            $query_and
            $query_having
            $query_order_by
            $query_limit  
        "; 
       // echo $query ;
        if($result = $this->exeSQL($query))
        {
            if($this->iNumRows>0)
            {
                if($ret_num_rows)
                {
                    return $this->iNumRows ;
                }
                else
                { 
                    $ret_ary = array();
                    $ctr=0;
                    while($row=$this->getAssoc($result))
                    {
                        $checkFlag=true;
                        if($row['iCourierBooking'] == 1 && $data['iFromPopup']!=1)
                        {
                            if($row['szTaskStatus']=='')
                            {
                                $checkFlag=false;
                            }
                        }
                        if($checkFlag)
                        {
                            $ret_ary[$ctr] = $row;
                            if($data['iFromPopup']!=1)
                            {
                                /*
                                * Please don't add join with tblstasks because this may slow down pending tray page @Ajay
                                */
                                $ret_ary[$ctr]['iPendingTrayPriority'] = $pendingTaskAry[$row['szTaskStatus']]['iPendingTrayPriority'];
                            }
                            $ctr++;
                        }
                    }  
                    if($data['iFromPopup']!=1)
                    {
                       $ret_ary = sortArray($ret_ary,'iPendingTrayPriority',false,'dtTaskStatusUpdatedOn',false,"idBookingFile",false,true);
                    }
                    return $ret_ary ;
                } 
            }
            else
            {
                return false;
            }
        }
    }
    
    function addBookingFileVisitedLogs($idBookingFile)
    {
        if($idBookingFile>0)
        {
            $idAdmin = (int)$_SESSION['admin_id']; 
            $query = "
                INSERT INTO
                    ".__DBC_SCHEMATA_BOOKING_FILE_VISITOR_LOGS__."
                (
                    idBookingFile, 
                    idAdmin,
                    dtFileSeen, 
                    szIPAddress,
                    szReferer,
                    szUserAgent,
                    dtCreatedOn
                )
                VALUES
                ( 
                    '".(int)$idBookingFile."', 
                    '".(int)$idAdmin."',
                    now(), 
                    '".mysql_escape_custom(__REMOTE_ADDR__)."',
                    '".mysql_escape_custom(__HTTP_REFERER__)."',
                    '".mysql_escape_custom(__HTTP_USER_AGENT__)."', 
                    now()
                )
            ";
            //echo "<br />".$query."<br/>"; 
            if( ( $result = $this->exeSQL( $query ) ) )
            {  
                return true;
            }
            else
            {
                return false;
            } 
        }
    }
    
    function getAllVisitedFileByUser($idAdmin)
    {
        if($idAdmin>0)
        {
            $query="
                SELECT
                    idBookingFile,
                    dtFileSeen
                FROM
                    ".__DBC_SCHEMATA_BOOKING_FILE_VISITOR_LOGS__."
                WHERE 
                    idAdmin = '".(int)$idAdmin."' 
                GROUP BY
                    idBookingFile
                ORDER BY
                    dtFileSeen DESC
                LIMIT
                    0, ".__BOOKING_FILE_VISITED_LIMIT__."
            ";
            //echo $query;
            if($result = $this->exeSQL($query))
            {
                if($this->getRowCnt()>0)
                {
                    $ret_ary = array();
                    $ctr = 1;
                    while($row = $this->getAssoc($result))
                    { 
                        $ret_ary[$row['idBookingFile']] = $row; 
                    }
                    return $ret_ary;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        } 
    }
    
    /**
    * Alternative to json_encode() to handle big arrays
    * Regular json_encode would return NULL due to memory issues.
    * @param $arr
    * @return string
    * @author Ajay
    */
    public function jsonEncode($arr) 
    { 
        /*
         * This function will work only for 2-D array in following format
         * $arr[0]['szFirsName'] = "Ajay";
            $arr[0]['szLastName'] = "Jha";
            $arr[0]['szCompnaName'] = "Whiz";
            $arr[1]['szFirsName'] = "Ajay";
            $arr[1]['szLastName'] = "Jha";
            $arr[1]['szCompnaName'] = "adadsa";
         */
        $count = count($arr);
        $current = 0;
        $str .= '[';
        foreach ($arr as $key => $value) 
        { 
            if (is_array($value)) 
            { 
                $counter = 0;
                $AryCounter = count($value);
                $str .= "{";
                foreach ($value as $booking_key=>$val) 
                {
                    $val = $this->sanitizeForJSON($val); 
                    $str .= '"'.$booking_key.'":"'.$val.'"'; 
                    $counter++;
                    if ($counter < $AryCounter) 
                    {
                        $str .= ',';
                    }
                } 
                $str .= "}"; 
            }  
            $current ++;
            if ($current < $count) {
                $str .= ',';
            }
        }
        $str .= ']';  
        return $str;
    }
    
    /**
    * Alternative to json_encode() to handle big arrays
    * Regular json_encode would return NULL due to memory issues.
    * @param $arr
    * @return string
    */
    public function jsonEncode_backup($arr) 
    {
        $str = '{';
        $count = count($arr);
        $current = 0;

        foreach ($arr as $key => $value) {
            $str .= sprintf('"%s":', $this->sanitizeForJSON($key));

            if (is_array($value)) {
                $str .= '[';
                foreach ($value as &$val) {
                    $val = $this->sanitizeForJSON($val);
                }
                $str .= '"' . implode('","', $value) . '"';
                $str .= ']';
            } else {
                $str .= sprintf('"%s"', $this->sanitizeForJSON($value));
            }

            $current ++;
            if ($current < $count) {
                $str .= ',';
            }
        }

        $str.= '}';

        return $str;
    }

    /**
     * @param string $str
     * @return string
     */
    private function sanitizeForJSON($str)
    {
        // Strip all slashes:
        $str = stripslashes($str);

        // Only escape backslashes:
        $str = str_replace('"', '\"', $str);

        return $str;
    }
    
    function updateRemindForwarder($iType=1,$idBookingFile=false)
    {  
        $query_select = "";
        if($iType==1)
        {
            /*
            * Whenever the ‘Pending tray’ is refreshed a check should be made whether any quotes have remained unanswered past the reminder time. 
            * IF this is the case a ‘Task’ field in ‘tblquotes’ should be updated with the value ‘T110. 
            * Remind forwarder’ and the task should appear in the pending tray.
            */
            $query_and = " 
                AND
                    f.dtReminderDate != '0000-00-00 00:00:00' 
                AND
                    f.dtReminderDate < now()
                AND
                    f.iSnoozeAdded !=1
                AND
                    f.szTransportecaTask NOT IN ('T110','T120','T210','T220')  
                AND
                    iStatus NOT IN (1,5,6)
                AND
                    iQuoteSent != 1 
            "; 
        }
        else if($iType==2)
        {
            /*
            * Whenever the ‘Pending tray’ is refreshed a check should be made whether any quotes have not been updated with Status ‘S6. Booking received’ after the reminder time was passed. 
            * IF this is the case the ‘Task’ field in ‘tblbookings’ should be updated with the value ‘T120. 
            * Remind customer’ and the task should appear in the pending tray.
            */

            $query_and = " 
                AND
                    f.dtCustomerRemindDate != '0000-00-00 00:00:00' 
                AND
                    f.dtCustomerRemindDate < now()
                AND
                    f.iSnoozeAdded !=1
                AND
                    f.szTransportecaTask NOT IN ('T120','T210','T220') 
                AND
                    iStatus NOT IN (6)
            ";
            $query_select = ", (SELECT idBookingStatus FROM ".__DBC_SCHEMATA_BOOKING__." b1 WHERE b1.idFile = f.id) as idBookingStatus ";
            $query_having = " HAVING idBookingStatus NOT IN (3,4,7) ";
        }
        else if($iType==3)
        {
            /*
            * Whenever the ‘Pending tray’ is refreshed a check should be made whether a file has completed snooze timing or not 
            * At the date and time that was stored the the same task should then reappear in the file and the file should thus reappear in the pending tray
            */

            $query_and = " 
                AND
                    f.dtSnoozeDate != '0000-00-00 00:00:00' 
                AND
                    f.dtSnoozeDate < now()
                AND
                    f.iSnoozeAdded = 1  
                AND
                    f.szTransportecaTask NOT IN ('T210','T220') 
            ";
        }
        else if($iType==4 && $idBookingFile>0)
        {
            /*
            * 
            * When we send a price to a customer, so the next task which appear in PENDING TRAY is "Remind Customer" - today we add this task after the number of hours as defined in the management variable called "Customer response time". Instead of this, as soon as the message sent to the customer gets status "Opened" then add this task called "Remind Customer" but latest by the time defined in the management variable. So if the management variable is 24 hours, and we send a price to customer at 08.00 and the customer open the mail at 10.00, then the tast "Remind Customer" appear at 10.00. If the customer has not opened the mail within 24 hours (as defined by management variable), then create the "Remind Customer" task in pending tray as we do today, even if mail has not been opened
            * 
            */ 
            $query_and = " 
                AND
                    f.szTransportecaStatus = 'S5'  
                AND
                    f.iSnoozeAdded !=1 
                AND
                    f.szTransportecaTask NOT IN ('T120','T210','T220')
                AND
                    f.iStatus NOT IN (6)
                AND
                    f.id = '".(int)$idBookingFile."'
            ";
        }
        else
        {
            return false;
        } 
        $kBooking = new cBooking();
        
        $query="
            SELECT
                f.id,
                f.idBooking,
                f.dtReminderDate,
                f.szLastTaskStatus,
                f.szTransportecaTask,
                f.dtCustomerRemindDate,
                f.dtReminderDate,
                f.dtSnoozeDate,
                f.iLabelStatus,
                now() as currenDateTime,
                f.szTransportecaStatus,
                f.szPreviousTask
                $query_select
            FROM
                ".__DBC_SCHEMATA_FILE_LOGS__." as f
            WHERE 
                f.iActive = '1'
            AND
                f.isDeleted = '0'  
            AND
                f.iClosed = '0' 
            AND
                f.szTransportecaStatus != 'S100'
            $query_and
            $query_having
        ";  
        if($iType==2)
        {
         //  echo "<br><br>".$query;
        } 
       //die;
        //return false;

        if($result = $this->exeSQL($query))
        {
            if($this->getRowCnt()>0)
            {
                $ret_ary = array();
                $ctr = 1;
                while($row = $this->getAssoc($result))
                { 
                    $ret_ary[$ctr] = $row;
                    $ctr++; 
                }  
                if(!empty($ret_ary))
                {
                    foreach($ret_ary as $ret_arys)
                    {
                        $idBookingFile = $ret_arys['id']; 
                        $dtResponseTime = $kBooking->getRealNow();

                        $fileLogsAry = array(); 
                        if($iType==1)
                        {
                            if($iType==1)
                            {
                                $ret_arys['dtReminderDate']='0000-00-00 00:00:00';
                            }
                            $fileLogsAry['szTransportecaTask'] = "T110"; 
                            $dtResponseTime = $ret_arys['dtReminderDate'];
                        }
                        else if($iType==2 || $iType==4)
                        {
                            $fileLogsAry['szTransportecaTask'] = "T120"; 
                            if($iType==2)
                            {
                                $dtResponseTime = $ret_arys['dtCustomerRemindDate'];
                                $ret_arys['dtCustomerRemindDate']='0000-00-00 00:00:00';
                            }                            
                        }
                        else
                        {   
                            $szPreviousTask='';
                            if($ret_arys["szTransportecaStatus"]=='S160' && $ret_arys["szTransportecaTask"]=='T140')
                            {
                                $ret_arys["szTransportecaTask"]='';
                            }
                            $ret_arys['szTransportecaTask'] = trim($ret_arys['szTransportecaTask']); 
                            if(empty($ret_arys['szTransportecaTask']))
                            {
                                if($ret_arys["szLastTaskStatus"]!='')
                                {
                                    $szPreviousTask=$ret_arys["szLastTaskStatus"];
                                }
                                else if($ret_arys["szPreviousTask"]!='')
                                {
                                    $szPreviousTask=$ret_arys["szPreviousTask"];
                                }
                                if($ret_arys["szTransportecaStatus"]=='S160' && $szPreviousTask=='T140')
                                {
                                    $ret_arys["szLastTaskStatus"]='';
                                }
                                if($ret_arys['iLabelStatus']>1 && $szPreviousTask=='T140')
                                {
                                    $ret_arys["szLastTaskStatus"]='';
                                }
                                $fileLogsAry['szTransportecaTask'] = $szPreviousTask; 
                                $fileLogsAry['szLastTaskStatus'] = ''; 
                                $fileLogsAry['szPreviousTask'] = ''; 
                            } 
                            $fileLogsAry['iSnoozeAdded'] = 0;  
                            $dtResponseTime = $ret_arys['dtSnoozeDate']; 
                            if($iType==3)
                            {
                                $fileLogsAry['dtSnoozeDate']='0000-00-00 00:00:00';
                            }
                        }

                        $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime;  

                        if(!empty($fileLogsAry))
                        {
                            $file_log_query = '';
                            foreach($fileLogsAry as $key=>$value)
                            {
                                $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                            }
                        } 
                        $file_log_query = rtrim($file_log_query,",");   
                        //echo $file_log_query." FIle ID: ".$idBookingFile;
                        $kBooking->updateBookingFileDetails($file_log_query,$idBookingFile); 
                    }
                }
                return true;
            }
            else
            {
                return false ;
            }
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        } 
    }
    
    function updateBookingTaskStatusOnload()
    {
        $kBooking = new cBooking();
        /*
        * updating Task status T110: Remind forwarder
        */
        $kBooking->updateRemindForwarder(1); 
        /*
        * updating Task status T120: Remind Customer
        */
        $kBooking->updateRemindForwarder(2);
        /*
        * updating snoozed booking file
        */
        $kBooking->updateRemindForwarder(3); 
        /*
        * updating Task status: 'Solve Task' to booking file
        */
        $kBooking->updateSolveTaskStaus(); 
    }
    
    function getAllBookingstobeupdatetoAftership()
    {
        $query="
           SELECT 
               id,
               szTrackingNumber,
               idServiceProvider,
               idFile
           FROM 
               ".__DBC_SCHEMATA_BOOKING__." AS b 
           WHERE
               iUpdateToAftership = '1'  
           AND
               idBookingStatus IN ('3','4')
        "; 
        //echo $query ;
        if($result = $this->exeSQL($query))
        {
            if($this->iNumRows>0)
            { 
                $ret_ary = array();
                $ctr=0;
                while($row=$this->getAssoc($result))
                { 
                    $ret_ary[$ctr] = $row; 
                    $ctr++;
                }
            }   
            return $ret_ary ; 
        }
        else
        {
            return false;
        }
    }
    
    function addFileTaskQueue($taskQueueAry)
    {
        if(!empty($taskQueueAry))
        {
            $query = "
                INSERT INTO
                    ".__DBC_SCHEMATA_BOOKING_FILE_TASK_QUEUES__."
                (
                    idBookingFile, 
                    szTaskStatus, 
                    iPriority,
                    szDeveloperNotes,
                    dtCreatedOn
                )
                VALUES
                ( 
                    '".(int)$taskQueueAry['idBookingFile']."', 
                    '".mysql_escape_custom($taskQueueAry['szTaskStatus'])."',
                    '".mysql_escape_custom($taskQueueAry['iPriority'])."',
                    '".mysql_escape_custom($taskQueueAry['szDeveloperNotes'])."', 
                    now()    
                )
            "; 
            if(($result = $this->exeSQL($query)))
            {  
                return true;
            }
            else
            {
                return false;
            } 
        }
    }
    
    function getNextPriorityTaskByFile($idBookingFile)
    {
        if($idBookingFile)
        {
            $kBooking_new = new cBooking();
            $kBooking_new->loadFile($idBookingFile); 
            
            $idBooking = $kBooking_new->idBooking;
            $iLabelStatus=$kBooking_new->iLabelStatus;
            $iDeleteCreateLabelFlag=false;
            $iDeleteSolveTaskFlag=false;
            
            $kBooking = new cBooking();
            $kBooking->load($idBooking);
            $idBookingStatus = $kBooking->idBookingStatus;
            if($iLabelStatus>1 || $idBookingStatus==7)
            {
                $iDeleteCreateLabelFlag=true;
                
                if($idBookingStatus==7)
                {
                    $szDeletedNotes="Create Label Task deleted because booking is cancelled.";
                }
                else
                {
                    $szDeletedNotes="Create Label Task deleted because courier labels are already uploaded.";
                }
                
                $queryUpdateCreateLabel="
                    UPDATE
                        ".__DBC_SCHEMATA_BOOKING_FILE_TASK_QUEUES__."
                    SET
                        isDeleted = '1',
                        dtDeleted = NOW(),
                        szDeletedNotes='".  mysql_escape_custom($szDeletedNotes)."'
                    WHERE        
                        idBookingFile = '".(int)$idBookingFile."'  
                    AND
                        isDeleted = '0'
                    AND
                        szTaskStatus='T140'
                ";
                $resultUpdateCreateLabel = $this->exeSQL($queryUpdateCreateLabel);
            }
                            
            $query="
                SELECT 
                    id,
                    szTaskStatus 
                FROM 
                    ".__DBC_SCHEMATA_BOOKING_FILE_TASK_QUEUES__."
                WHERE
                    idBookingFile = '".(int)$idBookingFile."'  
                AND
                    isDeleted = '0'
                ORDER BY 
                    dtCreatedOn DESC
                
            "; 
            //echo $query ;
            if($result = $this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {  
                    while($row=$this->getAssoc($result))
                    {
                        $szTaskStatus=$row['szTaskStatus'];
                        
                        if($szTaskStatus=='T220')
                        {
                            $iTotalColseTask=$this->checkSolveTaskToAddInPendingTray($idBookingFile);
                            if((int)$iTotalColseTask>0)
                            {
                                return $szTaskStatus;
                            }
                            else
                            {
                                
                            }
                        }
                        
                    }
                    return $row;
                }   
                return $ret_ary ; 
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        } 
    }
    
    function deleteTaskFromQueue($idTaskQueue)
    {
        if($idTaskQueue>0)
        {
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_BOOKING_FILE_TASK_QUEUES__."
                SET
                    isDeleted = '1' 
                WHERE
                   id = '".(int)$idTaskQueue."'
            ";
            if($result = $this->exeSQL($query))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    
    function checkSolveTaskToAddInPendingTray($idBookingFile)
    {
        $query="	
            SELECT
                count(id) AS iTotal
            FROM    
                ".__DBC_SCHEMATA_BOOKING_FILE_EMAIL_LOGS__." 
            WHERE
                idFile = '".(int)$idBookingFile."'
            AND
                iClosed='0'
            AND
                dtRemindDate!= '0000-00-00 00:00:00' 
            AND
                dtRemindDate < now()               
        ";
        if($result = $this->exeSQL($query))
        {
            if($this->iNumRows>0)
            { 
                $row=$this->getAssoc($result);
                $iTotal=$row['iTotal'];
                
            }
        }
        return 0;
    }
    
    function removeSolveTaskFromTheQueue($idBookingFile)
    {
        $queryUpdateSolveTask="
            UPDATE
                ".__DBC_SCHEMATA_BOOKING_FILE_TASK_QUEUES__."
            SET
                isDeleted = '1',
                dtDeleted = NOW(),
                szDeletedNotes='".  mysql_escape_custom($szDeletedNotes)."'
            WHERE        
                idBookingFile = '".(int)$idBookingFile."'  
            AND
                isDeleted = '0'
            AND
                szTaskStatus='T220'
        ";
        $resultUpdateSolveTask = $this->exeSQL($queryUpdateSolveTask);
    }
    
    function isShipperDetailsChanged($postSearchAry,$kBooking)
    { 
        $bShipperDetailsUpdated = false; 
        if($postSearchAry['idShipperDialCode']>0)
        {
            $kConfig = new cConfig();
            $kConfig->loadCountry($postSearchAry['idShipperDialCode']);
            $iShipperInternationDialCode = $kConfig->iInternationDialCode." ";
        }
        
        if($kBooking->idShipperDialCode>0)
        {
            $kConfig = new cConfig();
            $kConfig->loadCountry($kBooking->idShipperDialCode);
            $iShipperInternationDialCodePost = $kConfig->iInternationDialCode." ";
        }
                
        $iDebug = 1;
        if($postSearchAry['szShipperCompanyName']!=$kBooking->szShipperCompanyName)
        {
            $bShipperDetailsUpdated = true;
            if($iDebug==1)
            {
                $szLogString .= $postSearchAry['szShipperCompanyName']." - ".$kBooking->szShipperCompanyName."".PHP_EOL;
            }
        }
        else if($postSearchAry['szShipperFirstName']!=$kBooking->szShipperFirstName)
        {
            $bShipperDetailsUpdated = true;
            if($iDebug==1)
            {
                $szLogString .= $postSearchAry['szShipperFirstName']." - ".$kBooking->szShipperFirstName."".PHP_EOL;
            }
        }
        else if($postSearchAry['szShipperLastName']!=$kBooking->szShipperLastName)
        {
            $bShipperDetailsUpdated = true;
            if($iDebug==1)
            {
                $szLogString .= $postSearchAry['szShipperLastName']." - ".$kBooking->szShipperLastName."".PHP_EOL;
            }
        }
        else if($postSearchAry['szShipperEmail']!=$kBooking->szShipperEmail)
        {
            $bShipperDetailsUpdated = true;
            if($iDebug==1)
            {
                $szLogString .= $postSearchAry['szShipperEmail']." - ".$kBooking->szShipperEmail."".PHP_EOL;
            }
        }
        else if((int)$iShipperInternationDialCode!=(int)$iShipperInternationDialCodePost)
        {
            $bShipperDetailsUpdated = true;
            if($iDebug==1)
            {
                $szLogString .= "Dail: ".$postSearchAry['idShipperDialCode']." - ".$kBooking->idShipperDialCode."".PHP_EOL;
            }
        }
        else if($postSearchAry['szShipperPhone']!=$kBooking->szShipperPhoneNumber)
        {
            $bShipperDetailsUpdated = true;
            if($iDebug==1)
            {
                $szLogString .= $postSearchAry['szShipperPhone']." - ".$kBooking->szShipperPhoneNumber."".PHP_EOL;
            }
        }
        else if($postSearchAry['szShipperAddress']!=$kBooking->szOriginAddress)
        {
            $bShipperDetailsUpdated = true;
            if($iDebug==1)
            {
                $szLogString .= $postSearchAry['szShipperAddress']." - ".$kBooking->szOriginAddress."".PHP_EOL;
            }
        }
        else if($postSearchAry['szShipperPostCode']!=$kBooking->szOriginPostcode)
        {
            $bShipperDetailsUpdated = true;
            if($iDebug==1)
            {
                $szLogString .= $postSearchAry['szShipperPostCode']." - ".$kBooking->szOriginPostcode."".PHP_EOL;
            }
        }
        else if($postSearchAry['szShipperCity']!=$kBooking->szOriginCity)
        {
            $bShipperDetailsUpdated = true;
            if($iDebug==1)
            {
                $szLogString .= $postSearchAry['szShipperCity']." - ".$kBooking->szOriginCity."".PHP_EOL;
            }
        }
        else if((int)$postSearchAry['idShipperCountry'] != (int)$kBooking->idOriginCountry)
        {
            $bShipperDetailsUpdated = true;
            if($iDebug==1)
            {
                $szLogString .= $postSearchAry['idShipperCountry']." - ".$kBooking->idOriginCountry."".PHP_EOL;
            }
        }  
        if($iDebug==1)
        {
            $filename = __APP_PATH_ROOT__."/logs/validate_pane_save_log_".date('Ymd').".log";
            $strdata=array();
            $strdata[0]=" \n\n ***Consignee details changed for booking ID: ".$postSearchAry['id']."\n\n";
            $strdata[1] = $szLogString;  
            file_log(true, $strdata, $filename);
        }
        return $bShipperDetailsUpdated;
    }
    
    function getShipperDetailsStr($postSearchAry)
    {
        if(!empty($postSearchAry))
        {
            $szShipperLogs = "";
            if(!empty($postSearchAry['szShipperCompanyName']))
            {
                $szShipperLogs .= $postSearchAry['szShipperCompanyName'].PHP_EOL;
            }
            if(!empty($postSearchAry['szShipperFirstName']) || !empty($postSearchAry['szShipperLastName']))
            {
                $szShipperLogs .= $postSearchAry['szShipperFirstName']." ".$postSearchAry['szShipperLastName'].PHP_EOL;
            }
            if(!empty($postSearchAry['szShipperEmail']))
            {
                $szShipperLogs .= $postSearchAry['szShipperEmail'].PHP_EOL;
            }
            if(!empty($postSearchAry['idShipperDialCode']) || !empty($postSearchAry['szShipperPhone']))
            {
                if(!empty($postSearchAry['idShipperDialCode']))
                {
                    $kConfig = new cConfig();
                    $kConfig->loadCountry($postSearchAry['idShipperDialCode']);
                    $szShipperLogs .= "+".$kConfig->iInternationDialCode." ";
                }
                $szShipperLogs .= "".$postSearchAry['szShipperPhone'].PHP_EOL;
            }
            if(!empty($postSearchAry['szShipperAddress']))
            {
                $szShipperLogs .= $postSearchAry['szShipperAddress'].PHP_EOL;
            }
            if(!empty($postSearchAry['szShipperPostCode']) || !empty($postSearchAry['szShipperCity']))
            {
                $szShipperLogs .= $postSearchAry['szShipperPostCode']." ".$postSearchAry['szShipperCity'].PHP_EOL;
            }
            if(!empty($postSearchAry['idShipperCountry']))
            { 
                $kConfig = new cConfig();
                $kConfig->loadCountry($postSearchAry['idShipperCountry']); 
                $szShipperLogs .= $kConfig->szCountryName.PHP_EOL;
            }
            return $szShipperLogs;
        }
    } 
    
    function isConsigneeDetailsChanged($postSearchAry,$kBooking)
    { 
        $bConsigneeDetailsUpdated = false;
        $iDebug = 1;
        if($postSearchAry['idConsigneeDialCode']>0)
        {
            $kConfig = new cConfig();
            $kConfig->loadCountry($postSearchAry['idConsigneeDialCode']);
            $iConsigneeInternationDialCode = $kConfig->iInternationDialCode." ";
        }
        
        if($kBooking->idConsigneeDialCode>0)
        {
            $kConfig = new cConfig();
            $kConfig->loadCountry($kBooking->idConsigneeDialCode);
            $iConsigneeInternationDialCodePost = $kConfig->iInternationDialCode." ";
        }
        if($postSearchAry['szConsigneeCompanyName']!=$kBooking->szConsigneeCompanyName)
        {
            $bConsigneeDetailsUpdated = true;
            if($iDebug==1)
            {
                $szLogString .= $postSearchAry['szConsigneeCompanyName']." - ".$kBooking->szConsigneeCompanyName."".PHP_EOL;
            }
        }
        else if($postSearchAry['szConsigneeFirstName']!=$kBooking->szConsigneeFirstName)
        {
            $bConsigneeDetailsUpdated = true;
            if($iDebug==1)
            {
                $szLogString .= $postSearchAry['szConsigneeFirstName']." - ".$kBooking->szConsigneeFirstName."".PHP_EOL;
            }
        }
        else if($postSearchAry['szConsigneeLastName']!=$kBooking->szConsigneeLastName)
        {
            $bConsigneeDetailsUpdated = true;
            if($iDebug==1)
            {
                $szLogString .= $postSearchAry['szConsigneeLastName']." - ".$kBooking->szConsigneeLastName."".PHP_EOL;
            }
        }
        else if($postSearchAry['szConsigneeEmail']!=$kBooking->szConsigneeEmail)
        {
            $bConsigneeDetailsUpdated = true;
            if($iDebug==1)
            {
                $szLogString .= $postSearchAry['szConsigneeEmail']." - ".$kBooking->szConsigneeEmail."".PHP_EOL;
            }
        }
        else if((int)$iConsigneeInternationDialCode!=(int)$iConsigneeInternationDialCodePost)
        {
            $bConsigneeDetailsUpdated = true;
            if($iDebug==1)
            {
                $szLogString .= "Dail: ".$iConsigneeInternationDialCode." - ".$iConsigneeInternationDialCodePost."".PHP_EOL;
            }
        }
        else if($postSearchAry['szConsigneePhone']!=$kBooking->szConsigneePhoneNumber)
        {
            $bConsigneeDetailsUpdated = true;
            if($iDebug==1)
            {
                $szLogString .= $postSearchAry['szConsigneePhone']." - ".$kBooking->szConsigneePhoneNumber."".PHP_EOL;
            }
        }
        else if($postSearchAry['szConsigneeAddress']!=$kBooking->szDestinationAddress)
        {
            $bConsigneeDetailsUpdated = true;
            if($iDebug==1)
            {
                $szLogString .= $postSearchAry['szConsigneeAddress']." - ".$kBooking->szDestinationAddress."".PHP_EOL;
            }
        }
        else if($postSearchAry['szConsigneePostCode']!=$kBooking->szDestinationPostcode)
        {
            $bConsigneeDetailsUpdated = true;
            if($iDebug==1)
            {
                $szLogString .= $postSearchAry['szConsigneePostCode']." - ".$kBooking->szDestinationPostcode."".PHP_EOL;
            }
        }
        else if($postSearchAry['szConsigneeCity']!=$kBooking->szDestinationCity)
        {
            $bConsigneeDetailsUpdated = true;
            if($iDebug==1)
            {
                $szLogString .= $postSearchAry['szConsigneeCity']." - ".$kBooking->szDestinationCity."".PHP_EOL;
            }
        }
        else if((int)$postSearchAry['idConsigneeCountry'] != (int)$kBooking->idDestinationCountry)
        {
            $bConsigneeDetailsUpdated = true;
            if($iDebug==1)
            {
                $szLogString .= $postSearchAry['idConsigneeCountry']." - ".$kBooking->idDestinationCountry."".PHP_EOL;
            }
        }  
        if($iDebug==1)
        {
            $filename = __APP_PATH_ROOT__."/logs/validate_pane_save_log_".date('Ymd').".log";
            $strdata=array();
            $strdata[0]=" \n\n ***Consignee details changed for booking ID: ".$postSearchAry['id']."\n\n";
            $strdata[1] = $szLogString;  
            file_log(true, $strdata, $filename);
        }
        return $bConsigneeDetailsUpdated;
    }
    function getConsigneeDetailsStr($postSearchAry)
    {
        
        if(!empty($postSearchAry))
        {
            $szConsigneeLogs = "";
            if(!empty($postSearchAry['szConsigneeCompanyName']))
            {
                $szConsigneeLogs .= $postSearchAry['szConsigneeCompanyName'].PHP_EOL;
            }
            if(!empty($postSearchAry['szConsigneeFirstName']) || !empty($postSearchAry['szConsigneeLastName']))
            {
                $szConsigneeLogs .= $postSearchAry['szConsigneeFirstName']." ".$postSearchAry['szConsigneeLastName'].PHP_EOL;
            }
            if(!empty($postSearchAry['szConsigneeEmail']))
            {
                $szConsigneeLogs .= $postSearchAry['szConsigneeEmail'].PHP_EOL;
            }
            if(!empty($postSearchAry['idConsigneeDialCode']) || !empty($postSearchAry['szConsigneePhone']))
            {
                if(!empty($postSearchAry['idConsigneeDialCode']))
                {
                    $kConfig = new cConfig();
                    $kConfig->loadCountry($postSearchAry['idConsigneeDialCode']);
                    $szConsigneeLogs .= $kConfig->iInternationDialCode." ";
                }
                $szConsigneeLogs .= "".$postSearchAry['szConsigneePhone'].PHP_EOL;
            }
            if(!empty($postSearchAry['szConsigneeAddress']))
            {
                $szConsigneeLogs .= $postSearchAry['szConsigneeAddress'].PHP_EOL;
            }
            if(!empty($postSearchAry['szConsigneePostCode']) || !empty($postSearchAry['szConsigneeCity']))
            {
                $szConsigneeLogs .= $postSearchAry['szConsigneePostCode']." ".$postSearchAry['szConsigneeCity'].PHP_EOL;
            }
            if(!empty($postSearchAry['idConsigneeCountry']))
            { 
                $kConfig = new cConfig();
                $kConfig->loadCountry($postSearchAry['idConsigneeCountry']); 
                $szConsigneeLogs .= $kConfig->szCountryName.PHP_EOL;
            }
            return $szConsigneeLogs;
        }
    } 
}