<?php
/**
 * This file is the container for all admin dashboard related functionality.
 * All functionality related to admin dashboard should be contained in this class.
 *
 * dashboardAdmin.class.php
 *
 * @copyright Copyright (C) 2012 Transporteca, LLC
 * @author Anil
 * @package Transporteca Development
 */
 
if(!isset($_SESSION))
{
    session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_INC__ . "/functions.php" );
require_once( __APP_PATH_CLASSES__ . "/database.class.php");

class cDashboardAdmin extends cDatabase
{
    /**
     * This function insert the details for every past month
     * and runs on every first day of month
     */
    
     public function bookingTurnoverPerMonthCronJob($dtDateTime=false)
     {  
     	$dtCronjob=date('Y-m-d',strtotime('- 1 MONTH')); 
        if(!empty($dtDateTime))
        {
            $query_where_bk = " date_format(dtBookingConfirmed,'%Y-%m') = '".  mysql_escape_custom(date('Y-m',strtotime($dtDateTime)))."' AND iTransferConfirmed='0' ";
            $query_where_ft = " date_format(dtPaymentConfirmed,'%Y-%m') = '".  mysql_escape_custom(date('Y-m',strtotime($dtDateTime)))."' AND iTransferConfirmed='0'  ";
            $query_where_sent = " date_format(dtReadMessageTaskToRead,'%Y-%m') = '".  mysql_escape_custom(date('Y-m',strtotime($dtDateTime)))."'";
            $query_where_search = " date_format(dtCreatedOn,'%Y-%m') = '".  mysql_escape_custom(date('Y-m',strtotime($dtDateTime)))."'";
        }
        else 
        {
            $query_where_bk = "date_format(dtBookingConfirmed,'%Y-%m') = date_format(now() - INTERVAL 1 MONTH , '%Y-%m' ) AND iTransferConfirmed='0' ";
            $query_where_ft = " date_format(dtPaymentConfirmed,'%Y-%m') = date_format(now() - INTERVAL 1 MONTH , '%Y-%m' ) AND iTransferConfirmed='0' ";
            $query_where_sent = " date_format(dtReadMessageTaskToRead,'%Y-%m') = date_format(now() - INTERVAL 1 MONTH , '%Y-%m' ) ";
            $query_where_search = " date_format(dtCreatedOn,'%Y-%m') = date_format(now() - INTERVAL 1 MONTH , '%Y-%m' ) ";
        }
        
     	$query="
            SELECT	
                fTotalPriceUSD,
                iInsuranceIncluded,
                fTotalInsuranceCostForBookingUSD, 
                iInsuranceStatus,
                fReferalAmountUSD
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE
                $query_where_bk
            AND 
                idBookingStatus IN(3,4) 
     	";
     	if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            $totalUSDPrice = 0;
            $fReferalAmountUSD = 0;
            while($row=$this->getAssoc($result))
            {
                $totalUSDPrice += $row['fTotalPriceUSD'] ;
                if($row['iInsuranceIncluded']==1 && $row['iInsuranceStatus']!=__BOOKING_INSURANCE_STATUS_CANCELLED__)
                {
                    $totalUSDPrice += $row['fTotalInsuranceCostForBookingUSD'] ;
                } 
                $fReferalAmountUSD += $row['fReferalAmountUSD'] ;
            }  
        }  
        
        $query="
            SELECT	
                IFNULL(SUM(fUSDReferralFee),0) as uploadServiceAmount				
            FROM
                ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
            WHERE
                $query_where_ft
            AND 
                iDebitCredit = 5 
            AND
                iStatus IN( 1,2 )
        ";
    	if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            $row=$this->getAssoc($result);

            $uploadServiceAmount=$row['uploadServiceAmount'];
        }
	
        //echo "<br> Upload Service: ".$uploadServiceAmount ;
        
     	$query="
            SELECT	
                IFNULL(SUM(fUSDReferralFee),0) as referralFeeTotal				
            FROM
                ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS FT
            WHERE
                $query_where_ft 
            AND 
                FT.iDebitCredit = 3 
            AND
                FT.iStatus IN ('1','2')
            AND
                FT.iBatchNo<>'0'
        ";
     	//echo $query;
    	if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            $row=$this->getAssoc($result);

            $referralFeeTotal = $row['referralFeeTotal'];
        }
        
        //echo "<br>Referal Fee: ".$referralFeeTotal ;
                     
        
        $query="
            SELECT	
                IFNULL(SUM(cbd.fBookingLabelFeeRate * cbd.fBookingLabelFeeROE),0) as labelFeeTotal				
            FROM
                ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__." AS cbd
            INNER JOIN
                ".__DBC_SCHEMATA_BOOKING__." as bt
            ON
                bt.id=cbd.idBooking
            WHERE    
                $query_where_bk
            AND 
                bt.idBookingStatus IN(3,4) 
            AND
                bt.iTransferConfirmed = '0'
        ";
     	//echo $query;
    	if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            $row=$this->getAssoc($result);

            $labelFeeTotal = $row['labelFeeTotal'];
        }
       
                    
        $query="
            SELECT	
                IFNULL(SUM(fTotalHandlingFeeUSD),0) as fTotalBookingHandlingFeeUSD				
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE
                $query_where_bk 
            AND 
                idBookingStatus IN(3,4)
            AND
                iHandlingFeeApplicable='1'
            AND
                iTransferConfirmed = '0'
        ";
     	//echo $query;
    	if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            $row=$this->getAssoc($result);

            $fTotalBookingHandlingFeeUSD = $row['fTotalBookingHandlingFeeUSD'];
        }
        
        //echo "<br>Label Fee: ".$labelFeeTotal ;
        
        $query="
            SELECT	
                (IFNULL(SUM(fTotalInsuranceCostForBookingUSD),0) - IFNULL(SUM(fTotalInsuranceCostForBookingUSD_buyRate),0)) AS fInsuranceRevenue		
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE
                ( 
                    $query_where_bk  
                AND 
                    idBookingStatus IN(3,4) 
                AND
                    iInsuranceIncluded = '1'
                AND
                    iInsuranceStatus !='3'
                )
            ";
            //echo $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                $dashBoardArr = array();
                $row=$this->getAssoc($result);

                $fInsuranceRevenueTotal = $row['fInsuranceRevenue'];
            } 
            $totalRevenue = $referralFeeTotal + $uploadServiceAmount + $fInsuranceRevenueTotal + $labelFeeTotal + $fTotalBookingHandlingFeeUSD;
            
            $query="
                SELECT	
                    SUM(fTotalInsuranceCostForBookingUSD) AS fTotalInsuranceSellPriceUSD		
                FROM
                    ".__DBC_SCHEMATA_BOOKING__."
                WHERE
                ( 
                    $query_where_bk  
                AND 
                    idBookingStatus IN(3,4) 
                AND
                    iInsuranceIncluded = '1'
                AND
                    iInsuranceStatus !='3'
                )
            ";
            
            //echo $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                $dashBoardArr = array();
                $row=$this->getAssoc($result);

                $fTotalInsuranceSellPriceUSD = $row['fTotalInsuranceSellPriceUSD'];
            }
            //echo "<br>Insurance Sell Amount : ".$fTotalInsuranceSellPriceUSD ;
            $query="
                SELECT	
                    SUM(fTotalInsuranceCostForBookingUSD_buyRate) AS fTotalInsuranceBuyPriceUSD		
                FROM
                    ".__DBC_SCHEMATA_BOOKING__."
                WHERE
                ( 
                    $query_where_bk  
                AND 
                    idBookingStatus IN(3,4) 
                AND
                    iInsuranceIncluded = '1'
                AND
                    iInsuranceStatus !='3'
                )
            ";
            //echo $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                $dashBoardArr = array();
                $row=$this->getAssoc($result);

                $fTotalInsuranceBuyPriceUSD = $row['fTotalInsuranceBuyPriceUSD'];
            }
//            echo "<br> Insurance Buy Amount: ".$fTotalInsuranceBuyPriceUSD ;
//            echo "<br> Instant Referal: ".$fReferalAmountUSD ;
            $fTotalSaleRevenue = ($fTotalInsuranceSellPriceUSD - $fTotalInsuranceBuyPriceUSD) + $fReferalAmountUSD ;
            
//            echo "<br> Sales Revenue : ".$fTotalSaleRevenue;
//            echo "<br> Total Revenue: ".$totalRevenue ;
            
     	$query="
            SELECT	
                IFNULL(COUNT(id),0) as iBookings			
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE
                $query_where_bk  
            AND 
                idBookingStatus IN(3,4) 
     	";
     	if(($result = $this->exeSQL($query)))
        {
            $dashBoardArr = array();
            $row=$this->getAssoc($result);

            $iBookings=$row['iBookings'];
        }
		
     	$query="
            SELECT
                IFNULL(COUNT(id),0) as iLCLServices
            FROM
                ".__DBC_SCHEMATA_SERVICE_AVAILABLE__."	
            WHERE 
                iStatus = 1	
     	";
     	if(($result = $this->exeSQL($query)))
        {
            $dashBoardArr = array();
            $row=$this->getAssoc($result);

            $iLCLServices=$row['iLCLServices'];
        }
		
        //To show searches by all user simply update following variable to false
        $iWithoutTransporteca = true;
        if($iWithoutTransporteca)
        {
            $transportecaUserAry = array();
            $transportecaUserAry = $this->getAllTransportecaUsers();

            if(!empty($transportecaUserAry))
            {
                $user_str = implode(",",$transportecaUserAry);
                $query_and = " AND idUser NOT IN (".$user_str.") "; 
            } 
        }
        
     	$query="
            SELECT	
                IFNULL(COUNT(id),0) AS iSearches		
            FROM
                ".__DBC_SCHEMATA_COMPARE_BOOKING__."
            WHERE
                date_format(dtDateTime,'%Y-%m') = date_format(now() - INTERVAL 1 MONTH , '%Y-%m' ) 
            $query_and 
     	";
        //echo "<br><br> ".$query."<br>";
     	if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            $row=$this->getAssoc($result);

            $iSearches=$row['iSearches'];
        }
		
     	$query="
     		SELECT	
                    IFNULL(SUM(IF( iOriginCC = 1 AND iDestinationCC = 1 AND iStatus = 1,2,IF( (iOriginCC = 1 OR iDestinationCC = 1) AND iStatus = 1,1,0))),0) AS iCfsCC
                FROM
                    ".__DBC_SCHEMATA_SERVICE_AVAILABLE__."		
                WHERE 
                    date_format(dtValidTo,'%Y-%m') >= date_format(now() - INTERVAL 1 MONTH , '%Y-%m' ) 
     	";
     	//echo $query;
     	if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            $row=$this->getAssoc($result); 
            $iCfsCC=$row['iCfsCC'];
        }
		
		
     	$query="
            SELECT	
                COUNT(DISTINCT(id)) as iCustomerLoggedIn	
            FROM
                ".__DBC_SCHEMATA_USERS__."
            WHERE 
                date_format(dtLastLogin,'%Y-%m') = date_format(now() - INTERVAL 1 MONTH , '%Y-%m' ) 
     	";
        //echo "<br><br> ".$query."<br>";
     	if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            $row=$this->getAssoc($result);

            $iCustomerLoggedIn=$row['iCustomerLoggedIn'];
        }
		
		
    	 $query="
            SELECT	
                SUM(iPTD)+SUM(iDTP) AS iCfsHaulage
            FROM
                ".__DBC_SCHEMATA_SERVICE_AVAILABLE__."	
            WHERE 
                date_format(dtValidTo,'%Y-%m') >= date_format(now() - INTERVAL 1 MONTH , '%Y-%m' ) 
            AND
                iStatus = '1'
     	";
     	if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            $row=$this->getAssoc($result);

            $iCfsHaulage=$row['iCfsHaulage'];
        } 
        
        $iAutomaticSearchResponseTime = 0;
        $iRFQSearchResponseTime = 0; 
        
        $query=" 
            SELECT	
                SUM(iTotalClearTime) as iAutomaticSearchResponseTime,
                count(id) as iNumAutomaticSearches
            FROM
                ".__DBC_SCHEMATA_RESPONSE_TIME_TRACKER__." as track1
            WHERE 
                date_format(dtCreatedOn,'%Y-%m') =  date_format(now() - INTERVAL 1 MONTH , '%Y-%m' ) 
            AND
                isDeleted = '0' 
            AND
                iSearchType = '1'  
     	";
        //echo "<br>".$query ;
     	if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            $row = $this->getAssoc($result); 
            if($row['iNumAutomaticSearches']>0)
            {
                $iAutomaticSearchResponseTime = ceil($row['iAutomaticSearchResponseTime']/$row['iNumAutomaticSearches']);
            } 
        } 
         
        $query=" 
            SELECT	
                SUM(iTotalClearTime) as iRFQSearchResponseTime,
                count(id) as iNumRFQNumSearches
            FROM
                ".__DBC_SCHEMATA_RESPONSE_TIME_TRACKER__." as track2
            WHERE 
                date_format(dtCreatedOn,'%Y-%m') =  date_format(now() - INTERVAL 1 MONTH , '%Y-%m' )
            AND
                isDeleted = '0' 
            AND
                iSearchType = '2'
            AND
                szButtonClicked = 'SEND_CUSTOMER_QUOTES'    
     	";
        //echo "<br>".$query ;
     	if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            $row = $this->getAssoc($result);
            if($row['iNumRFQNumSearches']>0)
            {
                $iRFQSearchResponseTime = ceil($row['iRFQSearchResponseTime']/($row['iNumRFQNumSearches']*60));
            }
        }
        
        $iReadMessageResponseTime=$this->getReadMessageResponseTime($query_where_sent);
        $iAutomaticSearchResponseTime=$this->getCallUserResponseTime($query_where_search);
        
        $kMetrics = new cMetics();
        $investorDetailsAry = array();
        $investorDetailsAry = $kMetrics->getLastMonthCumulativeRevenue(); 

        $fInvestorSaleTurnOver = $investorDetailsAry['fInvestorSaleTurnOver'];
        $iInvestorNumBooking = $investorDetailsAry['iInvestorNumBooking'];
        
        $query="
            INSERT INTO
                ".__DBC_SCHEMATA_ADMIN_DASHBOARD__."
            (
                fTurnover,
                fSaleRevenue,
                fRevenue,
                iBookings,
                iLCLServices,
                iSearches,
                iCfsCC,
                iCustomerLoggedIn,
                iCfsHaulage,
                iAutomaticSearchResponseTime,
                iRFQSearchResponseTime,
                fInvestorSaleTurnOver,
                iInvestorNumBooking,
                dtCreatedOn,
                iReadMessageResponseTime
            )
            VALUES
            (
                '".(float)$totalUSDPrice."',
                '".(float)$fTotalSaleRevenue."', 
                '".(float)$totalRevenue."',	
                '".(int)$iBookings."',
                '".(int)$iLCLServices."',						
                '".(int)$iSearches."',
                '".(int)$iCfsCC."',
                '".(int)$iCustomerLoggedIn."',
                '".(int)$iCfsHaulage."',
                '".(int)$iAutomaticSearchResponseTime."',
                '".(int)$iRFQSearchResponseTime."',  
                '".(int)$fInvestorSaleTurnOver."',
                '".(int)$iInvestorNumBooking."',
                '".mysql_escape_custom($dtCronjob)."',
                '".(float)$iReadMessageResponseTime."'    
            )";
            echo "<br><br>".$query."<br><br>";
            //return true;
             
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                return true;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	}  
        function getAllTransportecaUsers()
	{ 
            $query="
                SELECT
                    id
                FROM
                    ".__DBC_SCHEMATA_USERS__."
                WHERE 
                    iActive = '1'
                AND
                 szEmail LIKE '%@transporteca.com%'   
            ";
            //echo $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                if( $this->getRowCnt() > 0 )
                {
                    $ctr=0;
                    $ret_ary = array();
                    while($row=$this->getAssoc($result))
                    {
                        $ret_ary[$ctr]=$row['id'];
                        $ctr++;
                    }
                    return $ret_ary;
                }
                else
                {
                    return array();
                }
            } 
	}
        
        function getBookingSearches($dtDateTime,$iWithoutTransporteca=false)
        { 
            if($iWithoutTransporteca)
            {
                $transportecaUserAry = array();
                $transportecaUserAry = $this->getAllTransportecaUsers();

                if(!empty($transportecaUserAry))
                {
                    $user_str = implode(",",$transportecaUserAry);
                    $query_and = " AND idUser NOT IN (".$user_str.") "; 
                } 
            } 
            
            $query="
                SELECT	
                    IFNULL(COUNT(id),0) AS iSearches		
                FROM
                    ".__DBC_SCHEMATA_COMPARE_BOOKING__."
                WHERE
                    date_format(dtDateTime,'%Y-%m') = '".  mysql_escape_custom($dtDateTime)."'
                $query_and
            ";
            echo "<br><br> ".$query."<br>"; 
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                $dashBoardArr = array();
                $row=$this->getAssoc($result); 
                $iSearches = $row['iSearches'];
                return $iSearches ;
            }
        }
        
        function downloadSearchedBookingData()
        { 
            $kBooking = new cBooking(); 
            require_once( __APP_PATH_CLASSES__ . "/PHPExcel.php" );

            $objPHPExcel=new PHPExcel(); 

            $sheetIndex=0;
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->createSheet();
            $objPHPExcel->setActiveSheetIndex($sheetIndex);
            $sheet = $objPHPExcel->getActiveSheet(); 

            $styleArray = array(
                    'font' => array(
                            'bold' => false,
                            'size' =>12,
                    ),
                    'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    ),
                    'borders' => array(
                            'top' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'bottom' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'right' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'left' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                    ),
            );

            $styleArray1 = array(
                    'font' => array(
                            'bold' => false,
                            'size' =>12,
                    ),
                    'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    ),
                    'borders' => array(
                            'top' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'bottom' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'right' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'left' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                    ),
            ); 

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20); 

            $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFill()->getStartColor()->setARGB('FFddd9c3');
            //$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);

            $objPHPExcel->getActiveSheet()->setCellValue('A1','Month')->getStyle('A1');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray); 

            $objPHPExcel->getActiveSheet()->setCellValue('B1','with @transporteca.com')->getStyle('B1');
            $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('C1','without @transporteca.com')->getStyle('C1');
            $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setCellValue('D1','Total Searches')->getStyle('D1');
            $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray);
 
            $col = 2;
            /* 
            * Fetching data for last 2 months 
            */
            $dtStartDate = date("Y-m-01",strtotime("-12 MONTH"));
            $dtEndDate = date("Y-m-01");

            $fromDate = strtotime($dtStartDate);
            $toDate = strtotime($dtEndDate);
            $counter = $fromDate ; 
            $kConfig = new cConfig();
            
            while($counter <= $toDate)
            { 
                $key = date('Y-m',$counter);    
                $keyStr = date('M',$counter);   
 
                $iTotalSearches = $this->getBookingSearches($key);  
                $iWithoutTransporteca = $this->getBookingSearches($key,true);  
                
                $iWithTransporteca = $iTotalSearches - $iWithoutTransporteca ;
                
                $fTurnover = 0;
                $fSalesRevenue = 0; 
                $fTotalSaleRevenue = 0;
                $fTotalTurnover = 0; 
                $kBooking = new cBooking();
                 
                $objPHPExcel->getActiveSheet()->setCellValue("A".$col,date('M Y',$counter));
                $objPHPExcel->getActiveSheet()->setCellValue("B".$col,$iWithTransporteca);
                $objPHPExcel->getActiveSheet()->setCellValue("C".$col,$iWithoutTransporteca); 
                $objPHPExcel->getActiveSheet()->setCellValue("D".$col,$iTotalSearches);  
                
                $col++; 
                $counter = strtotime("+1 MONTH",$counter); 
            }
             
            $title = "_Transporteca_searched_booking_graph";
            $objPHPExcel->getActiveSheet()->setTitle('Transporteca Searched Booking');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->removeSheetByIndex(1);
            $file=date('Ymdhs').$title;
            $fileName=__APP_PATH_ROOT__."/insuranceSheets/".$file.".xlsx"; 
            $objWriter->save($fileName); 
            return $fileName;  
        } 
        function addGraphWorkingCapitalCurrent($data)
        { 
            if(!empty($data))
            { 
                if(!empty($data['dtDateTime']))
                {
                    $dtDateTime = $data['dtDateTime'] ;
                }
                else
                {
                    //$dtDateTime = "now()";
                    $kBooking = new cBooking();
                    $dtDateTime = $kBooking->getRealNow(); 
                }
                
                $query="
                    INSERT INTO
                        ".__DBC_SCHEMATA_GRAPH_WORKING_CAPITAL_CURRENT__."
                    (
                        fPriceUSD,
                        szAmountType,
                        iDebitCredit,
                        szReference,
                        dtDateTime,
                        iMarkupBooking,
                        idBooking,
                        dtCreatedOn, 
                        iActive
                    )
                    VALUES
                    ( 
                        '".mysql_escape_custom($data['fPriceUSD'])."',	
                        '".mysql_escape_custom($data['szAmountType'])."',
                        '".mysql_escape_custom($data['iDebitCredit'])."',
                        '".mysql_escape_custom($data['szReference'])."',
                        '".mysql_escape_custom($dtDateTime)."', 
                        '".mysql_escape_custom($data['iMarkupBooking'])."',
                        '".mysql_escape_custom($data['idBooking'])."',
                        now(),
                        '1'
                    )
                ";  
//                echo $query;
//                die; 
                if(($result = $this->exeSQL($query)))
                {
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            } 
        }
        
        function updateGraphWorkingCapitalCurrent($data)
        { 
            if(!empty($data))
            { 
                if(!empty($data['dtDateTime']))
                {
                    $dtDateTime = $data['dtDateTime'] ;
                }
                else
                {
                    $kBooking = new cBooking();
                    $dtDateTime = $kBooking->getRealNow(); 
                } 
                $query="
                    UPDATE
                        ".__DBC_SCHEMATA_GRAPH_WORKING_CAPITAL_CURRENT__."
                    SET
                        fPriceUSD = '".mysql_escape_custom($data['fPriceUSD'])."',
                        szAmountType = '".mysql_escape_custom($data['szAmountType'])."',
                        iDebitCredit = '".mysql_escape_custom($data['iDebitCredit'])."',
                        szReference = '".mysql_escape_custom($data['szReference'])."',
                        dtDateTime = $dtDateTime,
                        dtUpdatedOn = now()
                    WHERE
                        idBooking = '".mysql_escape_custom($data['idBooking'])."'	
                    AND
                        iMarkupBooking = 1
                    AND
                        iDebitCredit = 2
                ";   
//                echo $query;
//                die;
                if(($result = $this->exeSQL($query)))
                {
                    return true;
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                }
            } 
        }
        
    function isWorkingCapitalTransactionAlreadyAdded($idBooking)
    {
        if($idBooking>0)
        { 
            $query="
                SELECT
                    id
                FROM
                    ".__DBC_SCHEMATA_GRAPH_WORKING_CAPITAL_CURRENT__."
                WHERE
                    idBooking = '".mysql_escape_custom($idBooking)."'	
                AND
                    iMarkupBooking = 1
                AND
                    iDebitCredit = 2
            "; 
            //echo $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
        	// Check if the user exists
                if( $this->getRowCnt() > 0 )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    /**
     * This function select's out the content for the dashboard Graph page 
     * over the past eleven months
     */
	
    private function bookingTurnoverPastMonths()
    { 
    	$monthsArr =$this->prev11MonthArr(); 
        
    	if(!empty($monthsArr))
    	{ 
            foreach($monthsArr as $monthsArrs)
            {
                $query="
                    SELECT 
                        `fTurnover`, 
                        `fRevenue`, 
                        fSaleRevenue,
                        `iBookings`, 
                        `iLCLServices`, 
                        `iSearches`, 
                        `iCfsCC`, 
                        `iCustomerLoggedIn`, 
                        `iCfsHaulage`, 
                        iAutomaticSearchResponseTime,
                        iRFQSearchResponseTime,
                        fInvestorSaleTurnOver,
                        iInvestorNumBooking,
                        DATE_FORMAT(`dtCreatedOn`,'%b') as dateValue,
                        iReadMessageResponseTime
                    FROM 
                        ".__DBC_SCHEMATA_ADMIN_DASHBOARD__."
                    WHERE
                        date_format(dtCreatedOn,'%Y-%m') ='".mysql_escape_custom($monthsArrs)."'
                    ORDER BY 
                        dtCreatedOn DESC 
                    ";
                    //echo $query;
                    if( ( $result = $this->exeSQL( $query ) ) )
                    {
                        $key1 = strtoupper(date('M',strtotime($monthsArrs))); 
                        $row=$this->getAssoc($result); 
                        $dashBoardArr[$key1] =  $row; 
                    }
                    else
                    {
                        $this->error = true;
                        $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                        $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                        return false;
                    }
    		}
    		return $dashBoardArr;
    	}
    	else
    	{
            return array();
    	}
    } 
	
    /**
     * This function selects out the details for every current month
     * 
     */

    function getDataForDashboardGraphByMonth($dtDateTime)
    {  
       $query="
           SELECT *
               FROM
               (
                   SELECT	
                       IFNULL(COUNT(id),0) AS iBookings, 
                       ROUND(IFNULL(SUM(fTotalPriceUSD),0),1) AS fTurnover, 
                       SUM(fReferalAmountUSD) AS fTotalReferalAmountUSD,
                       SUM(fPriceMarkupCustomerCurrency * fCustomerExchangeRate) AS fPriceMarkupUSD
                   FROM
                       ".__DBC_SCHEMATA_BOOKING__." as b1
                   WHERE
                       ( 
                           date_format(dtBookingConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."'
                       AND 
                           idBookingStatus IN(3,4) 
                       AND
                           iTransferConfirmed = '0'
                       )
               ) AS fTurnoverTable, 
               (
                   SELECT	
                       SUM(fTotalInsuranceCostForBookingUSD) AS fTotalInsuranceSellPriceUSD,
                       SUM(fTotalInsuranceCostForBookingUSD_buyRate) AS fTotalInsuranceBuyPriceUSD,
                       ROUND(IFNULL(SUM(fTotalInsuranceCostForBookingUSD),0),1) AS fInsuranceTurnOver,
                       (IFNULL(SUM(fTotalInsuranceCostForBookingUSD),0) - IFNULL(SUM(fTotalInsuranceCostForBookingUSD_buyRate),0)) AS fInsuranceRevenue
                   FROM
                        ".__DBC_SCHEMATA_BOOKING__." as b2 
                   WHERE
                   ( 
                       date_format(dtBookingConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."'
                   AND 
                       idBookingStatus IN(3,4) 
                   AND
                       iInsuranceIncluded = '1'
                   AND
                       iInsuranceStatus !='3'
                    AND
                       iTransferConfirmed = '0'
                   )
               ) AS fTotalInsuranceSellPrice,  
               (
                   SELECT	
                       SUM(cbd.fBookingLabelFeeRate * cbd.fBookingLabelFeeROE ) AS fTotalLabelFeeAmountUSD		
                   FROM
                       ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__." AS cbd
                   INNER JOIN
                       ".__DBC_SCHEMATA_BOOKING__." as bt
                   ON
                       bt.id=cbd.idBooking
                   WHERE
                        date_format(bt.dtBookingConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."' 
                   AND 
                       bt.idBookingStatus IN(3,4) 
                   AND
                       bt.iTransferConfirmed = '0'
                   AND
                       cbd.iCourierAgreementIncluded = '0'    
                   AND
                   (
                       bt.iCourierBooking='1'
                   OR
                       bt.isManualCourierBooking='1'
                   )    
               ) AS fTotalLabelFeeAmount,
               (
                   SELECT	
                       SUM(fTotalHandlingFeeUSD) AS fTotalBookingHandlingFeeUSD		
                   FROM
                       ".__DBC_SCHEMATA_BOOKING__."
                   WHERE
                        date_format(dtBookingConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."' 
                   AND 
                       idBookingStatus IN(3,4)
                   AND
                       iHandlingFeeApplicable='1'
                   AND
                       iTransferConfirmed = '0'
               ) AS fTotalBookingHandlingFeeUSD, 
               (
                   SELECT	
                       SUM(fTotalPriceUSD) AS fMarkupInvoiceAmount		
                   FROM
                       ".__DBC_SCHEMATA_BOOKING__." as b5
                   WHERE
                       date_format(dtBookingConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."' 
                   AND 
                       idBookingStatus IN(3,4) 
                   AND
                       iTransferConfirmed = '0'  
                   AND
                       iForwarderGPType = '".__FORWARDER_PROFIT_TYPE_MARK_UP__."'
               ) AS fMarkupInvoiceAmt,
               (
                   SELECT	
                       SUM(fTotalPriceForwarderCurrency) AS fMarkupInvoicePaid		
                   FROM
                       ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                   WHERE
                        date_format(dtPaymentConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."'
                   AND 
                       iDebitCredit = '".__TRANSACTION_DEBIT_CREDIT_FLAG_10__."'  
               ) AS fMarkupInvoiceAmountPaid, 
               (
                   SELECT
                   (
                       SELECT	
                           IFNULL(SUM(fUSDReferralFee),0)				
                       FROM
                           ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                       WHERE 
                           date_format(dtPaymentConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."'
                       AND 
                           iDebitCredit = 5 
                       AND
                           iStatus IN( 1,2 )	
                       AND 
                           iTransferConfirmed = '0'
                       )+(
                           SELECT	
                               IFNULL(SUM(fLabelFeeUSD),0)				
                           FROM
                               ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS FT
                           WHERE
                               date_format(FT.dtPaymentConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."'
                           AND 
                               FT.iDebitCredit = '8' 
                           AND
                               FT.iStatus IN ('1','2')
                           AND
                               FT.iBatchNo<>'0'	
                           AND
                               FT.iTransferConfirmed = '0'
                       )+(
                           SELECT	
                               IFNULL(SUM(fUSDReferralFee),0) as referralFeeTotal				
                           FROM
                               ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS FT
                           WHERE
                               date_format(FT.dtPaymentConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."'
                           AND 
                               FT.iDebitCredit = '3' 
                           AND
                               FT.iStatus IN ('1','2')
                           AND
                               FT.iBatchNo<>'0'	
                           AND
                               FT.iTransferConfirmed = '0'
                       ) AS  fRevenue	
               ) AS  fRevenueTable 
           ";
            //echo "<br><br>".$query."<br><br>";
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                $row = $this->getAssoc($result);  
                $row['fSaleRevenue'] = ($row['fTotalInsuranceSellPriceUSD'] - $row['fTotalInsuranceBuyPriceUSD']) + $row['fTotalReferalAmountUSD']+$row['fTotalLabelFeeAmountUSD'] + $row['fTotalBookingHandlingFeeUSD'] + $row['fPriceMarkupUSD'];   
                $row['fMarkupRevenue'] = ($row['fMarkupInvoiceAmount'] - $row['fMarkupInvoicePaid']) ;  
                return $row; 
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
     }
     
     function getDataForDashboardGraphByMonth_backup($dtDateTime)
     {  
        $query="
            SELECT *
                FROM
                (
                    SELECT	
                        ROUND(IFNULL(SUM(fTotalPriceUSD),0),1) AS fTurnover		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__." as b1
                    WHERE
                        ( 
                            date_format(dtBookingConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."'
                        AND 
                            idBookingStatus IN(3,4) 
                        AND
                            iTransferConfirmed = '0'
                        )
                ) AS fTurnoverTable, 
                (
                    SELECT	
                        IFNULL(COUNT(id),0) AS iBookings		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__." as b2 
                    WHERE
                    ( 
                        date_format(dtBookingConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."'
                    AND 
                        idBookingStatus IN(3,4)  
                    AND
                        iTransferConfirmed = '0'
                    )
                ) AS iBookingsTable, 
                (
                    SELECT	
                        SUM(fTotalInsuranceCostForBookingUSD) AS fTotalInsuranceSellPriceUSD		
                    FROM
                            ".__DBC_SCHEMATA_BOOKING__." as b2 
                    WHERE
                    ( 
                        date_format(dtBookingConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."'
                    AND 
                        idBookingStatus IN(3,4) 
                    AND
                        iInsuranceIncluded = '1'
                    AND
                        iInsuranceStatus !='3'
                     AND
                        iTransferConfirmed = '0'
                    )
                ) AS fTotalInsuranceSellPrice,
                (
                    SELECT	
                        SUM(fTotalInsuranceCostForBookingUSD_buyRate) AS fTotalInsuranceBuyPriceUSD		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__." as b3
                    WHERE
                    ( 
                        date_format(dtBookingConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."' 
                    AND 
                        idBookingStatus IN(3,4) 
                    AND
                        iInsuranceIncluded = '1'
                    AND
                        iInsuranceStatus !='3'
                    AND
                        iTransferConfirmed = '0'
                    )
                ) AS fTotalInsuranceBuyPrice, 
                (
                    SELECT	
                        SUM(fReferalAmountUSD) AS fTotalReferalAmountUSD		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__." as b4
                    WHERE
                        date_format(dtBookingConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."' 
                    AND 
                        idBookingStatus IN(3,4) 
                    AND
                        iTransferConfirmed = '0' 
                ) AS fTotalReferalAmount, 
                (
                    SELECT	
                        SUM(cbd.fBookingLabelFeeRate * cbd.fBookingLabelFeeROE ) AS fTotalLabelFeeAmountUSD		
                    FROM
                        ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__." AS cbd
                    INNER JOIN
                        ".__DBC_SCHEMATA_BOOKING__." as bt
                    ON
                        bt.id=cbd.idBooking
                    WHERE
                         date_format(bt.dtBookingConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."' 
                    AND 
                        bt.idBookingStatus IN(3,4) 
                    AND
                        bt.iTransferConfirmed = '0'
                    AND
                        cbd.iCourierAgreementIncluded = '0'    
                    AND
                    (
                        bt.iCourierBooking='1'
                    OR
                        bt.isManualCourierBooking='1'
                    )    
                ) AS fTotalLabelFeeAmount,
                (
                    SELECT	
                        SUM(fTotalHandlingFeeUSD) AS fTotalBookingHandlingFeeUSD		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__."
                    WHERE
                         date_format(dtBookingConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."' 
                    AND 
                        idBookingStatus IN(3,4)
                    AND
                        iHandlingFeeApplicable='1'
                    AND
                        iTransferConfirmed = '0'
                ) AS fTotalBookingHandlingFeeUSD,
                (
                    SELECT	
                        SUM(fPriceMarkupCustomerCurrency * fCustomerExchangeRate) AS fPriceMarkupUSD		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__." as b4
                    WHERE
                        date_format(dtBookingConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."' 
                    AND 
                        idBookingStatus IN(3,4) 
                    AND
                        iTransferConfirmed = '0' 
                ) AS fTotalPriceMarkupCustomerCurrency,  
                (
                    SELECT	
                        SUM(fTotalPriceUSD) AS fMarkupInvoiceAmount		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__." as b5
                    WHERE
                        date_format(dtBookingConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."' 
                    AND 
                        idBookingStatus IN(3,4) 
                    AND
                        iTransferConfirmed = '0'  
                    AND
                        iForwarderGPType = '".__FORWARDER_PROFIT_TYPE_MARK_UP__."'
                ) AS fMarkupInvoiceAmt,
                (
                    SELECT	
                        SUM(fTotalPriceForwarderCurrency) AS fMarkupInvoicePaid		
                    FROM
                        ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                    WHERE
                         date_format(dtPaymentConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."'
                    AND 
                        iDebitCredit = '".__TRANSACTION_DEBIT_CREDIT_FLAG_10__."'  
                ) AS fMarkupInvoiceAmountPaid,
                (
                    SELECT	
                        ROUND(IFNULL(SUM(fTotalInsuranceCostForBookingUSD),0),1) AS fInsuranceTurnOver		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__." as b5
                    WHERE
                    (  
                        date_format(dtBookingConfirmed,'%Y-%m') =  '".date('Y-m',strtotime($dtDateTime))."'
                    AND 
                        idBookingStatus IN(3,4) 
                    AND
                        iInsuranceIncluded = '1'
                    AND
                        iInsuranceStatus !='3'
                    AND
                        iTransferConfirmed = '0'
                    )
                ) AS fInsuranceTurnoverTable,
                (
                    SELECT
                    (
                        SELECT	
                            IFNULL(SUM(fUSDReferralFee),0)				
                        FROM
                            ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                        WHERE 
                            date_format(dtPaymentConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."'
                        AND 
                            iDebitCredit = 5 
                        AND
                            iStatus IN( 1,2 )	
                        AND 
                            iTransferConfirmed = '0'
                        )+(
                            SELECT	
                                IFNULL(SUM(fLabelFeeUSD),0)				
                            FROM
                                ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS FT
                            WHERE
                                date_format(FT.dtPaymentConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."'
                            AND 
                                FT.iDebitCredit = '8' 
                            AND
                                FT.iStatus IN ('1','2')
                            AND
                                FT.iBatchNo<>'0'	
                            AND
                                FT.iTransferConfirmed = '0'
                        )+(
                            SELECT	
                                IFNULL(SUM(fUSDReferralFee),0) as referralFeeTotal				
                            FROM
                                ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS FT
                            WHERE
                                date_format(FT.dtPaymentConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."'
                            AND 
                                FT.iDebitCredit = '3' 
                            AND
                                FT.iStatus IN ('1','2')
                            AND
                                FT.iBatchNo<>'0'	
                            AND
                                FT.iTransferConfirmed = '0'
                        ) AS  fRevenue	
                ) AS  fRevenueTable,	
                (
                        SELECT	
                            (IFNULL(SUM(fTotalInsuranceCostForBookingUSD),0) - IFNULL(SUM(fTotalInsuranceCostForBookingUSD_buyRate),0))	AS fInsuranceRevenue		
                        FROM
                                ".__DBC_SCHEMATA_BOOKING__."
                        WHERE
                        ( 
                            date_format(dtBookingConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."'
                        AND 
                            idBookingStatus IN(3,4) 
                        AND
                            iInsuranceIncluded = '1'
                        AND
                            iInsuranceStatus !='3'
                        AND
                            iTransferConfirmed = '0'
                        )
                ) AS fTotalInsuranceRevenue
            ";
            //echo "<br><br>".$query."<br><br>";
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                $row=$this->getAssoc($result);    
                $row['fSaleRevenue'] = ($row['fTotalInsuranceSellPriceUSD'] - $row['fTotalInsuranceBuyPriceUSD']) + $row['fTotalReferalAmountUSD']+$row['fTotalLabelFeeAmountUSD'] + $row['fTotalBookingHandlingFeeUSD'] + $row['fPriceMarkupUSD'];   
                $row['fMarkupRevenue'] = ($row['fMarkupInvoiceAmount'] - $row['fMarkupInvoicePaid']) ;  
                return $row; 
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
     }  
     private function bookingTurnoverPerMonth()
     {
        //To show searches by all user simply update following variable to false
        $iWithoutTransporteca = true;
        if($iWithoutTransporteca)
        {
            $transportecaUserAry = array();
            $transportecaUserAry = $this->getAllTransportecaUsers();

            if(!empty($transportecaUserAry))
            {
                $user_str = implode(",",$transportecaUserAry);
                $query_and = " AND idUser NOT IN (".$user_str.") "; 
            } 
        }
        
        $query="
            SELECT *
                FROM
                (
                    SELECT	
                        ROUND(IFNULL(SUM(fTotalPriceUSD),0),1) AS fTurnover		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__."
                    WHERE
                        ( 
                            date_format(dtBookingConfirmed,'%Y-%m') = date_format(now() , '%Y-%m' ) 
                        AND 
                            idBookingStatus IN(3,4) 
                        AND
                            iTransferConfirmed = '0' 
                        )
                ) AS fTurnoverTable,
                (
                        SELECT	
                            ROUND(IFNULL(SUM(fTotalInsuranceCostForBookingUSD),0),1)	AS fInsuranceTurnOver		
                        FROM
                            ".__DBC_SCHEMATA_BOOKING__."
                        WHERE
                        ( 
                            date_format(dtBookingConfirmed,'%Y-%m') = date_format(now() , '%Y-%m' ) 
                        AND 
                            idBookingStatus IN(3,4) 
                        AND
                            iInsuranceIncluded = '1'
                        AND
                            iInsuranceStatus !='3'
                        AND
                            iTransferConfirmed = '0'
                        )
                ) AS fInsuranceTurnoverTable,
                (
                    SELECT
                    (
                        SELECT	
                            IFNULL(SUM(fUSDReferralFee),0)				
                        FROM
                            ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                        WHERE 
                            date_format(dtPaymentConfirmed,'%Y-%m') = date_format(now() , '%Y-%m' ) 
                        AND 
                            iDebitCredit = 5 
                        AND
                            iStatus IN( 1,2 )	
                        AND 
                            iTransferConfirmed = '0'
                        )+(
                            SELECT	
                                IFNULL(SUM(fLabelFeeUSD),0)				
                            FROM
                                ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS FT
                            WHERE
                                date_format(FT.dtPaymentConfirmed,'%Y-%m') = date_format(now(), '%Y-%m' ) 
                            AND 
                                FT.iDebitCredit = '8' 
                            AND
                                FT.iStatus IN ('1','2')
                            AND
                                FT.iBatchNo<>'0'	
                            AND
                                FT.iTransferConfirmed = '0'
                        )+(
                            SELECT	
                                IFNULL(SUM(fUSDReferralFee),0) as referralFeeTotal				
                            FROM
                                ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__." AS FT
                            WHERE
                                date_format(FT.dtPaymentConfirmed,'%Y-%m') = date_format(now(), '%Y-%m' ) 
                            AND 
                                FT.iDebitCredit = '3' 
                            AND
                                FT.iStatus IN ('1','2')
                            AND
                                FT.iBatchNo<>'0'	
                            AND
                                FT.iTransferConfirmed = '0'
                        ) AS  fRevenue	
                ) AS  fRevenueTable,	
                (
                        SELECT	
                            (IFNULL(SUM(fTotalInsuranceCostForBookingUSD),0) - IFNULL(SUM(fTotalInsuranceCostForBookingUSD_buyRate),0))	AS fInsuranceRevenue		
                        FROM
                                ".__DBC_SCHEMATA_BOOKING__."
                        WHERE
                        ( 
                            date_format(dtBookingConfirmed,'%Y-%m') = date_format(now()  , '%Y-%m' ) 
                        AND 
                            idBookingStatus IN(3,4) 
                        AND
                            iInsuranceIncluded = '1'
                        AND
                            iInsuranceStatus !='3'
                        AND
                            iTransferConfirmed = '0'
                        )
                ) AS fTotalInsuranceRevenue,
                (
                    SELECT	
                        SUM(fTotalPriceUSD) AS fMarkupInvoiceAmount		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__." as b5
                    WHERE
                        date_format(dtBookingConfirmed,'%Y-%m') = date_format(now()  , '%Y-%m' ) 
                    AND 
                        idBookingStatus IN(3,4) 
                    AND
                        iTransferConfirmed = '0'  
                    AND
                        iForwarderGPType = '".__FORWARDER_PROFIT_TYPE_MARK_UP__."' 
                ) AS fMarkupInvoiceAmt,
                (
                    SELECT	
                        SUM(fTotalPriceForwarderCurrency) AS fMarkupInvoicePaid		
                    FROM
                        ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                    WHERE
                         date_format(dtPaymentConfirmed,'%Y-%m') = date_format(now() , '%Y-%m' )
                    AND 
                        iDebitCredit = '".__TRANSACTION_DEBIT_CREDIT_FLAG_10__."'  
                ) AS fMarkupInvoiceAmountPaid,
                (
                    SELECT	
                        IFNULL(COUNT(id),0) AS iBookings		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__."
                    WHERE
                    ( 
                        date_format(dtBookingConfirmed,'%Y-%m') = date_format(now()  , '%Y-%m' ) 
                    AND 
                        idBookingStatus IN(3,4)  
                    AND
                        iTransferConfirmed = '0'
                    )
                ) AS iBookingsTable, 
                (
                    SELECT	
                        SUM(fTotalInsuranceCostForBookingUSD) AS fTotalInsuranceSellPriceUSD		
                    FROM
                            ".__DBC_SCHEMATA_BOOKING__."
                    WHERE
                    ( 
                        date_format(dtBookingConfirmed,'%Y-%m') = date_format(now()  , '%Y-%m' ) 
                    AND 
                        idBookingStatus IN(3,4) 
                    AND
                        iInsuranceIncluded = '1'
                    AND
                        iInsuranceStatus !='3'
                     AND
                        iTransferConfirmed = '0'
                    )
                ) AS fTotalInsuranceSellPrice,
                (
                    SELECT	
                        SUM(fTotalInsuranceCostForBookingUSD_buyRate) AS fTotalInsuranceBuyPriceUSD		
                    FROM
                            ".__DBC_SCHEMATA_BOOKING__."
                    WHERE
                    ( 
                        date_format(dtBookingConfirmed,'%Y-%m') = date_format(now()  , '%Y-%m' ) 
                    AND 
                        idBookingStatus IN(3,4) 
                    AND
                        iInsuranceIncluded = '1'
                    AND
                        iInsuranceStatus !='3'
                    AND
                        iTransferConfirmed = '0'
                    )
                ) AS fTotalInsuranceBuyPrice, 
                (
                    SELECT	
                        SUM(fReferalAmountUSD) AS fTotalReferalAmountUSD		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__."
                    WHERE
                         date_format(dtBookingConfirmed,'%Y-%m') = date_format(now()  , '%Y-%m' ) 
                    AND 
                        idBookingStatus IN(3,4) 
                    AND
                        iTransferConfirmed = '0'
                ) AS fTotalReferalAmount, 
                (
                    SELECT  
                        SUM(cbd.fBookingLabelFeeRate * cbd.fBookingLabelFeeROE ) AS fTotalLabelFeeAmountUSD     
                    FROM
                        ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__." AS cbd
                    INNER JOIN
                        ".__DBC_SCHEMATA_BOOKING__." as bt
                    ON
                        bt.id=cbd.idBooking
                    WHERE
                        date_format(dtBookingConfirmed,'%Y-%m') = date_format(now()  , '%Y-%m' ) 
                    AND 
                        bt.idBookingStatus IN(3,4) 
                    AND
                        bt.iTransferConfirmed = '0'
                    AND
                        cbd.iCourierAgreementIncluded = '0'    
                    AND
                    (
                        bt.iCourierBooking='1'
                    OR
                        bt.isManualCourierBooking='1'
                    )    
                ) AS fTotalLabelFeeAmount, 
                (
                    SELECT  
                        SUM(fTotalHandlingFeeUSD) AS fTotalBookingHandlingFeeUSD        
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__."
                    WHERE
                        date_format(dtBookingConfirmed,'%Y-%m') = date_format(now()  , '%Y-%m' ) 
                    AND 
                        idBookingStatus IN(3,4)
                    AND
                        iHandlingFeeApplicable='1'
                    AND
                        iTransferConfirmed = '0'
                ) AS fTotalBookingHandlingFeeUSD,
                (
                    SELECT	
                        SUM(fPriceMarkupCustomerCurrency * fCustomerExchangeRate) AS fPriceMarkupUSD		
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__." as b4
                    WHERE
                        date_format(dtBookingConfirmed,'%Y-%m') = date_format(now()  , '%Y-%m' ) 
                    AND 
                        idBookingStatus IN(3,4) 
                    AND
                        iTransferConfirmed = '0' 
                ) AS fTotalPriceMarkupCustomerCurrency,  
                (
                    SELECT	
                        COUNT(id) AS iLCLServices
                    FROM
                        ".__DBC_SCHEMATA_SERVICE_AVAILABLE__."	
                    WHERE 
                        iStatus = 1	
                ) AS iLCLServicesTable,

                (
                        SELECT	
                            IFNULL(COUNT(id),0)	AS iSearches		
                        FROM
                            ".__DBC_SCHEMATA_COMPARE_BOOKING__."
                        WHERE
                        ( 
                            date_format(dtDateTime,'%Y-%m') = date_format(now(), '%Y-%m' )  
                            $query_and
                        )
                ) AS iSearchesTable,

                (
                    SELECT	
                            SUM(IF( iOriginCC = 1 AND iDestinationCC = 1 AND iStatus = 1,2,IF( (iOriginCC = 1 OR iDestinationCC = 1) AND iStatus = 1, 1, 0))) AS iCfsCC
                    FROM
                            ".__DBC_SCHEMATA_SERVICE_AVAILABLE__."								
                ) AS iCfsCCTable,

                (
                    SELECT	
                            IFNULL(COUNT(id),0) AS iCustomerLoggedIn		
                    FROM

                        ".__DBC_SCHEMATA_USERS__."
                    WHERE
                    ( 
                        date_format(dtVerified,'%Y-%m') = date_format(now() , '%Y-%m' )
                    OR	
                        date_format(dtLastLogin,'%Y-%m') = date_format(now() , '%Y-%m' )
                    )	
                ) AS iCustomerLoggedInTable, 
                (
                        SELECT	
                                SUM(iPTD)+SUM(iDTP) AS iCfsHaulage
                        FROM
                                ".__DBC_SCHEMATA_SERVICE_AVAILABLE__."	
                        WHERE 
                                iStatus = 1	
                ) AS iCfsHaulageTable,
                (
                    SELECT
                        DATE_FORMAT(now(),'%b') AS dateValue
                ) AS dtCreatedOn,
                (
                    SELECT	
                        SUM(TIMESTAMPDIFF(MINUTE,dtFileCreated,dtResponseTime)) as iAutomaticSearchResponseTime,
                        count(id) as iNumAutomaticSearches
                    FROM
                        ".__DBC_SCHEMATA_RESPONSE_TIME_TRACKER__." as track1
                    WHERE 
                        date_format(dtCreatedOn,'%Y-%m') =  date_format(now() , '%Y-%m' ) 
                    AND
                        isDeleted = '0' 
                    AND
                        iSearchType = '1'
                ) AS iAutomaticSearch,
                (
                    SELECT	
                        SUM(iTotalClearTime) as iRFQSearchResponseTime,
                        count(id) as iNumRFQNumSearches
                    FROM
                        ".__DBC_SCHEMATA_RESPONSE_TIME_TRACKER__." as track2
                    WHERE 
                        date_format(dtCreatedOn,'%Y-%m') = date_format(now() , '%Y-%m' )
                    AND
                        isDeleted = '0' 
                    AND
                        iSearchType = '2'
                    AND
                        szButtonClicked = 'SEND_CUSTOMER_QUOTES'    
                ) AS iRFQSearch
            ";
            //echo $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                $row=$this->getAssoc($result);   
                $row['fSaleRevenue'] = ($row['fTotalInsuranceSellPriceUSD'] - $row['fTotalInsuranceBuyPriceUSD']) + $row['fTotalReferalAmountUSD'] + $row['fTotalLabelFeeAmountUSD'] + $row['fTotalBookingHandlingFeeUSD'] + $row['fPriceMarkupUSD'];
               
                $workingCapAry = $this->getWorkingCapitalDetails(); 
                $row['fTotalCapitalUSD'] = $workingCapAry['fTotalCapitalRevenueUSD'] ;
                 
                if($row['iNumRFQNumSearches']>0)
                {
                    $row['iRFQSearchResponseTime'] = ceil($row['iRFQSearchResponseTime']/($row['iNumRFQNumSearches']*60));
                }
                else
                {
                    $row['iRFQSearchResponseTime'] = 0;
                }
                
                /*if($row['iNumAutomaticSearches']>0)
                {
                    $row['iAutomaticSearchResponseTime'] = ceil($row['iAutomaticSearchResponseTime']/$row['iNumAutomaticSearches']);
                }
                else
                {
                    $row['iAutomaticSearchResponseTime'] = 0;
                } */
                
               
                
                $row['fMarkupRevenue'] = round((float)($row['fMarkupInvoiceAmount'] - $row['fMarkupInvoicePaid']));  
                return $row;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
	} 
        
        function getWorkingCapitalDetails()
        {
             $query = " 
                SELECT	
                    DISTINCT idBooking 
                FROM
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                WHERE
                ( 
                    date_format(dtPaymentConfirmed,'%Y-%m') = date_format(now()  , '%Y-%m' ) 
                AND  
                    iStatus<>'0' 
                AND
                    iDebitCredit = '1' 
                ) 
            ";
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                $booking_id_ary = array();
                $ctr=0;
                while($row=$this->getAssoc($result))
                { 
                    $booking_id_ary[$ctr] = $row['idBooking'];
                    $ctr++ ;
                } 
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
            
            if(!empty($booking_id_ary))
            {
                $booking_id_str = implode(",",$booking_id_ary);
                
                $query = " 
                    SELECT	
                        fReferalPercentage,
                        fTotalPriceCustomerCurrency,
                        fTotalPriceUSD,
                        fTotalVat,
                        fCustomerExchangeRate,
                        fTotalInsuranceCostForBookingUSD,
                        fTotalInsuranceCostForBookingUSD_buyRate,
                        fTotalInsuranceCostForBookingCustomerCurrency, 
                        dtCreatedOn
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__."
                    WHERE
                        id IN (".$booking_id_str.")
                "; 
                //echo $query ;
                if(($result = $this->exeSQL($query)))
                {
                    $booking_id_ary = array();
                    $ctr=0;
                    $fTotalCapitalRevenue = 0; 
                    $fTotalCapitalRevenueUSD = 0;
                    while($row=$this->getAssoc($result))
                    {
                        $fCapitalRevenue = 0;
                        $fCapitalRevenueUSD = 0;
                        
                        $fCapitalRevenue = $row['fTotalPriceCustomerCurrency'] + $row['fTotalInsuranceCostForBookingCustomerCurrency'] + $row['fTotalVat'];
                        
                        if($row['fCustomerExchangeRate']>0)
                        {
                            $fCapitalRevenueUSD = $fCapitalRevenue * $row['fCustomerExchangeRate'] ;
                        } 
                        $fTotalCapitalRevenue += $fCapitalRevenue ;
                        $fTotalCapitalRevenueUSD += $fCapitalRevenueUSD ;
                    } 
                }
                else
                {
                    $this->error = true;
                    $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                    $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                    return false;
                } 
            }
            
            $query = " 
                SELECT	
                    fTotalPriceForwarderCurrency,
                    fTotalAmount,
                    fExchangeRate,
                    fForwarderExchangeRate
                FROM
                    ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
                WHERE 
                    date_format(dtPaymentConfirmed,'%Y-%m') = date_format(now()  , '%Y-%m' ) 
                AND   
                    iBatchNo<>'0'   
            ";
            if( ( $result = $this->exeSQL( $query ) ) )
            {  
                $booking_id_ary = array();
                $ctr=0;
             
                while($row=$this->getAssoc($result))
                {
                     
                } 
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
             
            $final_ret_ary = array();
            $final_ret_ary['fTotalCapitalRevenue'] = $fTotalCapitalRevenue ;
            $final_ret_ary['fTotalCapitalRevenueUSD'] = $fTotalCapitalRevenueUSD ; 
            return $final_ret_ary ; 
        }
        function getSiteRevenueDetails()
        {
            $query = " 
                SELECT	
                    fReferalPercentage,
                    fTotalPriceCustomerCurrency,
                    fTotalPriceUSD,
                    fTotalVat,
                    fCustomerExchangeRate,
                    fTotalInsuranceCostForBookingUSD,
                    fTotalInsuranceCostForBookingUSD_buyRate,
                    dtCreatedOn
                FROM
                    ".__DBC_SCHEMATA_BOOKING__."
                WHERE
                ( 
                    date_format(dtBookingConfirmed,'%Y-%m') = date_format(now()  , '%Y-%m' ) 
                AND 
                    idBookingStatus IN(3,4) 
                ) 
            ";
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                $ret_ary = array();
                $ctr=0;
                while($row=$this->getAssoc($result))
                {
                    $arr_key = date('Ym',  strtotime($row['dtCreatedOn'])) ;
                    $ret_ary[$arr_key] = $row ;
                }
                return $row;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
	public function listAllDashboardDetails($ret_ary_flag=false,$dashboard_flag=false)
	{		
            $dashboardArr[] = $this->bookingTurnoverPastMonths(); 
              
            if(!empty($dashboardArr[0]))
            {
                foreach($dashboardArr[0] as $key=>$value)
                {
                    $newDashBoardArr[$key] = $value;
                } 
            }   
            
            /* 
            * Fetching data for last 2 months 
            */ 
            $dtStartDate = date("Y-m-01",strtotime(date('Y-m')." -2 month"));
             
            $dtEndDate = date("Y-m-01");
            
            $fromDate = strtotime($dtStartDate);
            $toDate = strtotime($dtEndDate);
            $counter = $fromDate ; 
            
            while($counter <= $toDate)
            { 
                $key = date('Y-m-01',$counter);    
                $keyStr = date('M',$counter);   
                
                $lastMonthDataAry = array();
                $lastMonthDataAry = $this->getDataForDashboardGraphByMonth($key);
                   
                if(!empty($lastMonthDataAry))
                {
                    $keyStr = strtoupper($keyStr);
                    $newDashBoardArr[$keyStr]['fTurnover'] = (float)$lastMonthDataAry['fTurnover'];
                    $newDashBoardArr[$keyStr]['fInsuranceTurnOver'] = $lastMonthDataAry['fInsuranceTurnOver'] ;
                    $newDashBoardArr[$keyStr]['fSaleRevenue'] = $lastMonthDataAry['fSaleRevenue'] ;
                    $newDashBoardArr[$keyStr]['iBookings'] = $lastMonthDataAry['iBookings'] ;
                    $newDashBoardArr[$keyStr]['fRevenue'] = $lastMonthDataAry['fRevenue'];
                    $newDashBoardArr[$keyStr]['fInsuranceRevenue'] = $lastMonthDataAry['fInsuranceRevenue'] ;
                    $newDashBoardArr[$keyStr]['fMarkupRevenue'] = $lastMonthDataAry['fMarkupRevenue'] ;
                }
                $counter = strtotime("+1 MONTH",$counter); 
            } 
            
            $key1 = strtoupper(date('M'));
            $newDashBoardArr[$key1] = $this->bookingTurnoverPerMonth();
            
            $readMessageTimeForCurrentMonth=$this->getReadMessageResponseTime();
            $iAutomaticSearchResponseTime=$this->getCallUserResponseTime();
            
            $kMetrics = new cMetics();
            $investorDetailsAry = array();
            $investorDetailsAry = $kMetrics->getLastMonthCumulativeRevenue(); 
            
            $newDashBoardArr[$key1]['fInvestorSaleTurnOver'] = $investorDetailsAry['fInvestorSaleTurnOver'];
            $newDashBoardArr[$key1]['iInvestorNumBooking'] = $investorDetailsAry['iInvestorNumBooking'];
            $newDashBoardArr[$key1]['iReadMessageResponseTime']= $readMessageTimeForCurrentMonth;
            $newDashBoardArr[$key1]['iAutomaticSearchResponseTime']= $iAutomaticSearchResponseTime;
            
            if(!empty($newDashBoardArr))
            {
                foreach($newDashBoardArr as $key=>$value)
                {   
                    $dtCreatedOn = '';
                    $dtCreatedOn = strtoupper($key); 
                    $fTurnover[$dtCreatedOn] = round_up((float)(($value['fTurnover']+$value['fInsuranceTurnOver'])/1000),1);
                    $fRevenue[$dtCreatedOn] = round((int)($value['fRevenue']+$value['fInsuranceRevenue']+$value['fMarkupRevenue']));
                    $iBookings[$dtCreatedOn] = round((int)$value['iBookings']);
                    $iLCLServices[$dtCreatedOn] = round((int)$value['iLCLServices']);
                    $iSearches[$dtCreatedOn] = round((int)$value['iSearches']);
                    $iCfsCC[$dtCreatedOn] = (int)$value['iCfsCC'];
                    $iCustomerLoggedIn[$dtCreatedOn] = (int)$value['iCustomerLoggedIn'];
                    $iCfsHaulage[$dtCreatedOn] = (int)$value['iCfsHaulage'];  
                    $salesRevenue[$dtCreatedOn] = round($value['fSaleRevenue']) ; 
                    $automaticSearchResponse[$dtCreatedOn] = round($value['iAutomaticSearchResponseTime']) ;
                    $rfqSearchResponse[$dtCreatedOn] = round($value['iRFQSearchResponseTime']);
                    $readMessageResponse[$dtCreatedOn] = ($value['iReadMessageResponseTime']);
                    
                    if($value['iInvestorNumBooking']>0)
                    {
                        $fInvestorSaleTurnOver[$dtCreatedOn] = round((float)($value['fInvestorSaleTurnOver']/$value['iInvestorNumBooking'])); 
                    }
                    else
                    {
                        $fInvestorSaleTurnOver[$dtCreatedOn] = 0.00;
                    }                      
                }
            } 
            
            $workingCaps = array();
            $workingCaps = $this->getWorkingCapitalGraphData();
            
            if($ret_ary_flag && $dashboard_flag)
            {
                $ret_ary = array();
                $ret_ary['turn_over'] = $fTurnover ;
                $ret_ary['sales_revenue'] = $salesRevenue ;
                $ret_ary['num_booking'] = $iBookings;
                $ret_ary['lcl_services'] = $iLCLServices;
                $ret_ary['actual_revenue'] = $fRevenue;
                return $ret_ary;
            }
            else if($ret_ary_flag)
            {
                $ret_ary = array();
                $ret_ary['dashboardData'] = $newDashBoardArr ;
                $ret_ary['workingCapital'] = $workingCaps ; 
                return $ret_ary;
            }
            
            $imgArr[] = $this->createImageDashBoard($fTurnover,1);
            $imgArr[] = $this->createImageDashBoard($fRevenue,2);
            $imgArr[] = $this->createImageDashBoard($iBookings,3);
            $imgArr[] = $this->createImageDashBoard($iLCLServices,4);
            $imgArr[] = $this->createImageDashBoard($iSearches,5);
            $imgArr[] = $this->createImageDashBoard($iCfsCC,6);
            $imgArr[] = $this->createImageDashBoard($iCustomerLoggedIn,7);
            $imgArr[] = $this->createImageDashBoard($iCfsHaulage,8); 
            $imgArr[] = $this->createImageDashBoard($salesRevenue,9);  
            $imgArr[] = $this->createImageDashBoard($workingCaps,10,true);  
            $imgArr[] = $this->createImageDashBoard($automaticSearchResponse,11); 
            $imgArr[] = $this->createImageDashBoard($rfqSearchResponse,12);
            $imgArr[] = $this->createImageDashBoard($readMessageResponse,13);
            $imgArr[] = $this->createImageDashBoard($fInvestorSaleTurnOver,14);
            return $imgArr; 
	}
         
        
        function getWorkingCapitalGraphData()
        {
            $kWHSSearch = new cWHSSearch();
            $iNumCreditDays = $kWHSSearch->getManageMentVariableByDescription('__CREDIT_DAYS_FOR_INSURANCE_VENODR__');
            
            $dateSearchAry = array();
            $dateSearchAry['dtDateTo'] = date('Y-m-d');
            
            if($iNumCreditDays>0)
            {
                $dateSearchAry['dtDateFrom'] = date('Y-m-d',strtotime("-".$iNumCreditDays." DAY"));
            }
            else
            {
                $dateSearchAry['dtDateFrom'] = date('Y-m-d');
            }     
            $monthsArr =$this->prev11MonthArr();
            //print_r($monthsArr);
            $dateSearchAry['dtDateFrom'] = $monthsArr[0]."-01";
            $dateSearchAry['dtDateTo'] = date('Y-m-01');
            
            $fromDate = strtotime($dateSearchAry['dtDateFrom']);
            $toDate = strtotime($dateSearchAry['dtDateTo']);
            $counter = $fromDate ;

            $graph_data_ary = array();
            $workingCapsAry = array();
            $workingCapsAry = $this->getCurrentWorkingCapital($dtDateTime);
            $tempDataAry = array();
            //echo date('Y-m-d',$counter);
            while($counter <= $toDate)
            { 
                $key = date('Ym01',$counter);    
                $keyStr = date('M',$counter);  
                $keyStr = strtoupper($keyStr); 
                
                if(!empty($workingCapsAry[$key]))
                {
                    $szLastMonthKey = date('M',strtotime("-1 MONTH",$counter));
                    $szLastMonthKey = strtoupper($szLastMonthKey);
                    
                    $accumalativePrice = $tempDataAry[$szLastMonthKey];  
                    
                    $fWorkingCapital = ($accumalativePrice + ($workingCapsAry[$key]['fTotalEarning'] - $workingCapsAry[$key]['fTotalAmountPaid']));
                    $tempDataAry[$keyStr] = $fWorkingCapital ;
                    
                    $fWorkingCapital = round((float)($fWorkingCapital/1000),1);
                    $graph_data_ary[$keyStr] =  $fWorkingCapital ;  
                }
                else
                {
                    $szLastMonthKey = date('M',strtotime("-1 MONTH",$counter));
                    $szLastMonthKey = strtoupper($szLastMonthKey); 
                    
                    $fWorkingCapital = $tempDataAry[$szLastMonthKey];  
                    $tempDataAry[$keyStr] = $fWorkingCapital ;
                    
                    $fWorkingCapital = round((float)($fWorkingCapital/1000),1);
                    $graph_data_ary[$keyStr] =  $fWorkingCapital ;  
                }  
                $counter = strtotime("+1 MONTH",$counter); 
            }    
            return $graph_data_ary ; 
        }
        
        
        function getCurrentWorkingCapital()
        {
            $query="
                SELECT	
                    SUM(fPriceUSD) as fTotalAmountUSD,
                    iDebitCredit,
                    dtDateTime
                FROM
                    ".__DBC_SCHEMATA_GRAPH_WORKING_CAPITAL_CURRENT__." 
                GROUP BY
                   MONTH(dtDateTime),iDebitCredit
                ORDER BY
                    dtDateTime ASC
            ";
            //echo $query ;
            if( ( $result = $this->exeSQL( $query ) ) )
            {  
                $fTotalEarning = 0;
                $fTotalAmountPaid = 0;
                while($row = $this->getAssoc($result))
                { 
                    $key = date('Ym',strtotime($row['dtDateTime']))."01" ;
                    if($row['iDebitCredit']==1)
                    {
                        $ret_ary[$key]['fTotalEarning'] = $row['fTotalAmountUSD'] ;
                    }
                    else
                    {
                        $ret_ary[$key]['fTotalAmountPaid'] = $row['fTotalAmountUSD'] ; 
                    }
                } 
                
                /*
                $fWorkingCapital = $fTotalEarning - $fTotalAmountPaid ; 
                $ret_ary=array();
                $ret_ary[date('Ymd',strtotime($dtDateTime))] = $fWorkingCapital ;
                 * 
                 */
                return $ret_ary;
            }
        }
        
	private function createImageDashBoard($values,$id,$working_capital_graph=false)
	{
		# ------- The graph values in the form of associative array
		
		$img_width=460;
		$img_height=260; 
		$margins=30;
		$imageName="images".$id.".png";
		$imageAppPath=__APP_PATH_ROOT__."/management/graph/dashboard/".$imageName;
		if (file_exists($imageAppPath)) 
                {
                    unlink($imageAppPath);
		}
	 
		# ---- Find the size of graph by substracting the size of borders
		//$graph_width=$img_width - $margins * 2;
		$graph_width = $img_width ;
		$graph_height=$img_height - $margins * 2; 
		$img=imagecreate($img_width,$img_height);
	
	 	
		$bar_width=25;
		$total_bars=count($values);
		$gap= ($graph_width- $total_bars * $bar_width ) / ($total_bars +1);
		$gap = 10;
		//echo $gap ;
	 
		# -------  Define Colors ----------------
		$bar_color=imagecolorallocatealpha($img,179,179,152,5);
		$border_color=imagecolorallocate($img,255,255,255);
		$base_line= imagecolorallocatealpha($img,125,116,104,5);
		$base_line_string = imagecolorallocatealpha($img,0,0,0,5);
		# ------ Create the border around the graph ------
		imagesetthickness($img, 2);
		imagefilledrectangle($img,0,0,$img_width,$img_height,$border_color);
	
	 
		# ------- Max value is required to adjust the scale	-------
		$max_value=max($values) + 2;
		$ratio= $graph_height/$max_value;
		imageline ( $img ,20,15, 20 , 230, $base_line);
		imageline ( $img ,20,15, 25 , 25, $base_line);
		imageline ( $img ,20,15, 15 , 25, $base_line);
		imageline ( $img ,440,230, 20 , 230, $base_line);
		
		#-------- Center align the value in the graph -------
		$position = strlen('"'.$max_value.'"');
		
		# ----------- Draw the bars here ------
		//$font = 'arial.ttf';
		$margins2= 20 ;
		$font_file_name = __APP_PATH__.'/fonts/font.ttf';
		$font_file_name2 = __APP_PATH__.'/fonts/Calibri.ttf';
		for($i=0;$i< $total_bars; $i++)
		{ 
                    # ------ Extract key and value pair from the current pointer position
                    list($key,$value)=each($values);

                    $x1= $margins2 + $gap + $i * ($gap+$bar_width) ;
                    $x2= $x1 + $bar_width; 
                    /*if($id==13)
                    {
                        $y1=$margins +$graph_height- floatval($value * $ratio) ;
                    }else{*/
                        $y1=$margins +$graph_height- intval($value * $ratio) ;
                    //}
                    
                    $y2=$img_height-$margins;
                    
                    # ----------- Setting the format for to view ------
                    if($id == 1 || $working_capital_graph)
                    {
                        $showValue = number_format($value,1);
                    }
                    /*else if($id==13)
                    {
                        $showValue = number_format($value,2);
                    }*/
                    else
                    {
                        $showValue = number_format($value);
                    } 
                    
                    if($value<0) 
                    {
                        imagettftext($img, 10, 0, $x1-10, 50, $base_line_string, $font_file_name,$showValue);
                    }
                    else
                    {
                        imagettftext($img, 10, 0, $x1-$position+10, $y1-5, $base_line_string, $font_file_name,$showValue);
                    }
                    
                    //imagestring($img,3,$x1+15,$y1-15,number_format($value),$bar_color);
                    //imagestring($img,4,$x1+4,$img_height-20,$key,$base_line_string);		
                    imagettftext($img, 10, 0, $x1+2, $img_height-10, $base_line_string, $font_file_name2, $key);
                    imagefilledrectangle($img,$x1,$y1,$x2,$y2,$base_line);
		} 
                
		imagepng($img,"graph/dashboard/images".$id.".png");
		imagedestroy($img);
		
		return "graph/dashboard/images".$id.".png";
	}
	
	function prev11MonthArr()
	{
            $monthsArr = array();
            for( $i = 11; $i >= 1 ; $i-- )
            {
               $monthsArr[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
            }
            return $monthsArr;
	}
        
        
	function downloadDashboardGraphDetails()
	{   
            $kBooking = new cBooking(); 
            require_once( __APP_PATH_CLASSES__ . "/PHPExcel.php" );

            $objPHPExcel=new PHPExcel(); 

            $sheetIndex=0;
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->createSheet();
            $objPHPExcel->setActiveSheetIndex($sheetIndex);
            $sheet = $objPHPExcel->getActiveSheet(); 

            $styleArray = array(
                    'font' => array(
                            'bold' => false,
                            'size' =>12,
                    ),
                    'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    ),
                    'borders' => array(
                            'top' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'bottom' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'right' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'left' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                    ),
            );

            $styleArray1 = array(
                    'font' => array(
                            'bold' => false,
                            'size' =>12,
                    ),
                    'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    ),
                    'borders' => array(
                            'top' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'bottom' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'right' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                            'left' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                            ),
                    ),
            ); 

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10); 
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10); 
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10); 
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10); 

            $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFill()->getStartColor()->setARGB('FFddd9c3');
            //$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);

            $objPHPExcel->getActiveSheet()->setCellValue('A1','Booking Date')->getStyle('A1');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray); 

            $objPHPExcel->getActiveSheet()->setCellValue('B1','Reference')->getStyle('B1');
            $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('C1','Customer name')->getStyle('C1');
            $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('D1','Type')->getStyle('D1');
            $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray); 
            
            $objPHPExcel->getActiveSheet()->setCellValue('E1','Price (USD)')->getStyle('E1');
            $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray); 
            
            $objPHPExcel->getActiveSheet()->setCellValue('F1','Referral/Markup (USD)')->getStyle('F1');
            $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setCellValue('G1','Curr. markup (USD)')->getStyle('G1');
            $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($styleArray); 
            
            $objPHPExcel->getActiveSheet()->setCellValue('H1','Insurance Sell Price (USD)')->getStyle('D1');
            $objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setCellValue('I1','Insurance Buy Price (USD)')->getStyle('D1');
            $objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($styleArray);
             
            $objPHPExcel->getActiveSheet()->setCellValue('J1','Label Fee (USD)')->getStyle('D1');
            $objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($styleArray); 
            
            $objPHPExcel->getActiveSheet()->setCellValue('K1','Handling Fee (USD)')->getStyle('D1');
            $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($styleArray); 
            
            $objPHPExcel->getActiveSheet()->setCellValue('L1','Turnover (USD)')->getStyle('D1');
            $objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setCellValue('M1','GP (USD)')->getStyle('D1');
            $objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($styleArray); 
            $col = 2;
            /* 
            * Fetching data for last 2 months 
            */
            $dtStartDate = date("Y-m-01",strtotime(date('Y-m')." -2 month"));
            $dtEndDate = date("Y-m-01");

            $fromDate = strtotime($dtStartDate);
            $toDate = strtotime($dtEndDate);
            $counter = $fromDate ; 
            $kConfig = new cConfig();
            
            while($counter <= $toDate)
            { 
                $key = date('Y-m-01',$counter);    
                $keyStr = date('M',$counter);   

                $lastMonthDataAry = array();
                $lastMonthDataAry = $this->getDataForDashboardCsvFile($key);
                
                $fTurnover = 0;
                $fSalesRevenue = 0; 
                $fTotalSaleRevenue = 0;
                $fTotalTurnover = 0; 
                $kBooking = new cBooking();
                if(!empty($lastMonthDataAry))
                {
                    foreach($lastMonthDataAry as $lastMonthDataArys)
                    {    
                        $fInsuranceRevenue = ($lastMonthDataArys['fTotalInsuranceCostForBookingUSD'] - $lastMonthDataArys['fTotalInsuranceCostForBookingUSD_buyRate']);
                        $fSaleRevenue = $fInsuranceRevenue + $lastMonthDataArys['fTotalReferalAmountUSD'] ;   
                        $fTurnover = $lastMonthDataArys['fTotalPriceUSD'] + $lastMonthDataArys['fTotalInsuranceCostForBookingUSD'] ; 

                        $fTotalSaleRevenue += $fSaleRevenue ;
                        $fTotalTurnover += $fTurnover ;
                        
                        $idBooking = $lastMonthDataArys['idBooking'];
                        $fLabelFeeUsd = 0;
                        if($lastMonthDataArys['iCourierBooking']==1 || $lastMonthDataArys['isManualCourierBooking']==1)
                        {
                           $courierBookingDetails = $kBooking->getCourierBookingDetails($idBooking);
                           
                           if(!empty($courierBookingDetails) && $courierBookingDetails['iCourierAgreementIncluded']==0)
                           {
                               $fLabelFeeUsd = $courierBookingDetails['fBookingLabelFeeRate'] * $courierBookingDetails['fBookingLabelFeeROE'];
                           }
                        }
                        if($lastMonthDataArys['iForwarderGPType']==__FORWARDER_PROFIT_TYPE_MARK_UP__)
                        {
                            $szMarkupType = "Markup";
                        }
                        else
                        {
                            $szMarkupType = "Referrel";
                        }
                        if($lastMonthDataArys['fCustomerExchangeRate'])
                        {
                            $fPriceMarkupCustomerCurrency = $lastMonthDataArys['fPriceMarkupCustomerCurrency'] * $lastMonthDataArys['fCustomerExchangeRate'];
                        }
                        else
                        {
                            $fPriceMarkupCustomerCurrency = 0;
                        }
                        
                        $fHandlingFeeUsd = 0;
                        if($lastMonthDataArys['iHandlingFeeApplicable']==1)
                        {
                            $fHandlingFeeUsd = $lastMonthDataArys['fTotalHandlingFeeUSD'];
                        } 
                        $fTotalGPUSD = round((float)$lastMonthDataArys['fReferalAmountUSD'],2) + round((float)$fPriceMarkupCustomerCurrency,2) + round((float)$lastMonthDataArys['fTotalInsuranceCostForBookingUSD'],2) - round((float)$lastMonthDataArys['fTotalInsuranceCostForBookingUSD_buyRate'],2) + round((float)$fLabelFeeUsd,2) + round((float)$fHandlingFeeUsd,2) ;
                        $objPHPExcel->getActiveSheet()->setCellValue("A".$col,date('Y-m-d',strtotime($lastMonthDataArys['dtBookingConfirmed'])));
                        $objPHPExcel->getActiveSheet()->setCellValue("B".$col,$lastMonthDataArys['szBookingRef']);
                        $objPHPExcel->getActiveSheet()->setCellValue("C".$col,$lastMonthDataArys['szCustomerName']);
                        $objPHPExcel->getActiveSheet()->setCellValue("D".$col,$szMarkupType); 
                        $objPHPExcel->getActiveSheet()->setCellValue("E".$col,round((float)$lastMonthDataArys['fTotalPriceUSD'],2));    
                        $objPHPExcel->getActiveSheet()->setCellValue("F".$col,round((float)$lastMonthDataArys['fReferalAmountUSD'],2));	
                        $objPHPExcel->getActiveSheet()->setCellValue("G".$col,round((float)$fPriceMarkupCustomerCurrency,2));
                        $objPHPExcel->getActiveSheet()->setCellValue("H".$col,round((float)$lastMonthDataArys['fTotalInsuranceCostForBookingUSD'],2));
                        $objPHPExcel->getActiveSheet()->setCellValue("I".$col,round((float)$lastMonthDataArys['fTotalInsuranceCostForBookingUSD_buyRate'],2));  
                        $objPHPExcel->getActiveSheet()->setCellValue("J".$col,round((float)$fLabelFeeUsd,2)); 
                        $objPHPExcel->getActiveSheet()->setCellValue("K".$col,round((float)$fHandlingFeeUsd,2)); 
                        $objPHPExcel->getActiveSheet()->setCellValue("L".$col,round((float)$fTurnover,2)); 
                        $objPHPExcel->getActiveSheet()->setCellValue("M".$col,round((float)$fTotalGPUSD,2)); 
                        $col++; 
                    } 
                }
                
                $szHeading = "Graph data for month ".date('F, Y',$counter);
                $col++;
                $objPHPExcel->getActiveSheet()->setCellValue("A".$col,$szHeading);
                $objPHPExcel->getActiveSheet()->setCellValue("H".$col,$fTotalSaleRevenue);
                $objPHPExcel->getActiveSheet()->setCellValue("I".$col,$fTotalTurnover);
                $col++; 
                $counter = strtotime("+1 MONTH",$counter); 
            }
             
            $title = "_Transporteca_dashboard_graph";
            $objPHPExcel->getActiveSheet()->setTitle('Transporteca Dashboard');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->removeSheetByIndex(1);
            $file=date('Ymdhs').$title;
            $fileName=__APP_PATH_ROOT__."/insuranceSheets/".$file.".xlsx"; 
            $objWriter->save($fileName); 
            return $fileName; 
    }  
    
    /**
    * This function selects out the details for every current month 
    */

     function getDataForDashboardCsvFile($dtDateTime)
     { 
        $query=" 
            SELECT	
                id as idBooking,
                fTotalPriceUSD, 
                fReferalAmountUSD, 
                dtBookingConfirmed,
                szBookingRef,
                iCourierBooking,
                fCustomerExchangeRate,
                fPriceMarkupCustomerCurrency,
                iForwarderGPType,
                iHandlingFeeApplicable,
                fTotalHandlingFeeUSD,
                isManualCourierBooking,
                CONCAT_WS(' ',szFirstName,szLastName) as szCustomerName,
                (SELECT fTotalInsuranceCostForBookingUSD FROM ".__DBC_SCHEMATA_BOOKING__." as b2 WHERE b2.id = b1.id AND iInsuranceIncluded = '1' AND iInsuranceStatus !='3') as fTotalInsuranceCostForBookingUSD,
                (SELECT fTotalInsuranceCostForBookingUSD_buyRate FROM ".__DBC_SCHEMATA_BOOKING__." as b2 WHERE b2.id = b1.id AND iInsuranceIncluded = '1' AND iInsuranceStatus !='3') as fTotalInsuranceCostForBookingUSD_buyRate
            FROM
                ".__DBC_SCHEMATA_BOOKING__." as b1
            WHERE 
                date_format(dtBookingConfirmed,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."'
            AND 
                idBookingStatus IN(3,4) 
            AND
                iTransferConfirmed = '0'  
            ORDER BY
                dtBookingConfirmed ASC
        ";
        //echo $query;
        if(($result = $this->exeSQL($query)))
        {
            $ctr=0;
            while($row=$this->getAssoc($result))
            {
                $ret_ary[$ctr] = $row ;
                $ctr++;
            } 
            return $ret_ary;
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
     
    function getWorkingCapitalGraphDataExcels()
    {   
        $kBooking = new cBooking(); 
        require_once( __APP_PATH_CLASSES__ . "/PHPExcel.php" );

        $objPHPExcel=new PHPExcel(); 

        $sheetIndex=0;
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex($sheetIndex);
        $sheet = $objPHPExcel->getActiveSheet(); 

        $styleArray = array(
                'font' => array(
                        'bold' => false,
                        'size' =>12,
                ),
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                ),
                'borders' => array(
                        'top' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'bottom' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'right' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'left' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                ),
        );

        $styleArray1 = array(
                'font' => array(
                        'bold' => false,
                        'size' =>12,
                ),
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                ),
                'borders' => array(
                        'top' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'bottom' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'right' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'left' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                ),
        ); 

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(40); 

        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->getStartColor()->setARGB('FFddd9c3');
        //$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
        //#ffff00

        $objPHPExcel->getActiveSheet()->setCellValue('A1','Date')->getStyle('A1');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray); 

        $objPHPExcel->getActiveSheet()->setCellValue('B1','Credit (USD)')->getStyle('B1');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->setCellValue('C1','Debit (USD)')->getStyle('C1');
        $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->setCellValue('D1','Working Capital (USD)')->getStyle('D1');
        $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray); 

        $objPHPExcel->getActiveSheet()->setCellValue('E1','Reference')->getStyle('D1');
        $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray); 
        
        $objPHPExcel->getActiveSheet()->setCellValue('F1','Description')->getStyle('D1');
        $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray); 

        $col = 2;
        /* 
        * Fetching data for last 5 months 
        */
        $dtStartDate = date("Y-m-01",strtotime("-5 MONTH"));
        $dtEndDate = date("Y-m-01");

        $fromDate = strtotime($dtStartDate);
        $toDate = strtotime($dtEndDate);
        $counter = $fromDate ; 
        $kConfig = new cConfig();
 
        $keyStr = date("M",strtotime("-7 MONTH"));
        $key = date("Y-m-01",strtotime("-7 MONTH"));
        $keyStr = strtoupper($keyStr);
          
        $lastMonthDataAry = array();
        $lastMonthDataAry = $this->getCurrentWorkingCapitalExcels($key); 
        
        $fTurnover = 0;
        $fSalesRevenue = 0; 
        $fTotalSaleRevenue = 0;
        $fTotalTurnover = 0; 

        if(!empty($lastMonthDataAry))
        {
            foreach($lastMonthDataAry as $lastMonthDataArys)
            {     
                $fCreditAmount = 0;
                $fDebitAmount = 0;
                if($lastMonthDataArys['iDebitCredit']==1)
                {
                    $fCreditAmount = $lastMonthDataArys['fTotalAmountUSD'];
                    $fWorkingCapital = ($fWorkingCapital + $fCreditAmount);
                }
                else
                {
                    $fDebitAmount = $lastMonthDataArys['fTotalAmountUSD'];
                    $fWorkingCapital = ($fWorkingCapital - $fCreditAmount);
                }
            }
        }
        
        $workingCaps = array();
        $workingCaps = $this->getWorkingCapitalGraphData();
        $fWorkingCapital = $workingCaps[$keyStr];
        
        $fWorkingCapital = $fWorkingCapital * 1000;
        
        $szHeading = "Graph data for month ".date('F, Y',strtotime("-7 MONTH"));
        $col++;
        
        $objPHPExcel->getActiveSheet()->getStyle('A'.$col.':F'.$col)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A'.$col.':F'.$col)->getFill()->getStartColor()->setARGB('FFFF00');
         
        $objPHPExcel->getActiveSheet()->setCellValue("A".$col,$szHeading);
        $objPHPExcel->getActiveSheet()->setCellValue("D".$col,$fWorkingCapital); 
        $col++; 
        
        while($counter <= $toDate)
        { 
            $key = date('Y-m-01',$counter);    
            $keyStr = date('M',$counter);   

            $lastMonthDataAry = array();
            $lastMonthDataAry = $this->getCurrentWorkingCapitalExcels($key);
            
            $fTurnover = 0;
            $fSalesRevenue = 0; 
            $fTotalSaleRevenue = 0;
            $fTotalTurnover = 0; 

            if(!empty($lastMonthDataAry))
            {
                foreach($lastMonthDataAry as $lastMonthDataArys)
                {     
                    $fCreditAmount = 0;
                    $fDebitAmount = 0;
                    if($lastMonthDataArys['iDebitCredit']==1)
                    {
                        $fCreditAmount = $lastMonthDataArys['fTotalAmountUSD'];
                        $fWorkingCapital = ($fWorkingCapital + $fCreditAmount);
                    }
                    else
                    {
                        $fDebitAmount = $lastMonthDataArys['fTotalAmountUSD'];
                        $fWorkingCapital = ($fWorkingCapital - $fDebitAmount);
                    }
                    
                    $objPHPExcel->getActiveSheet()->setCellValue("A".$col,date('Y-m-d',strtotime($lastMonthDataArys['dtDateTime'])));
                    $objPHPExcel->getActiveSheet()->setCellValue("B".$col,$fCreditAmount);
                    $objPHPExcel->getActiveSheet()->setCellValue("C".$col,$fDebitAmount);
                    $objPHPExcel->getActiveSheet()->setCellValue("D".$col,$fWorkingCapital);  
                    $objPHPExcel->getActiveSheet()->setCellValue("E".$col,$lastMonthDataArys['szReference']); 
                    $objPHPExcel->getActiveSheet()->setCellValue("E".$col,$lastMonthDataArys['szAmountType']); 
                    $col++; 
                } 
            }

            $szHeading = "Graph data for month ".date('F, Y',$counter);
            $col++;
            $objPHPExcel->getActiveSheet()->getStyle('A'.$col.':F'.$col)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$col.':F'.$col)->getFill()->getStartColor()->setARGB('FFFF00');
            
            $objPHPExcel->getActiveSheet()->setCellValue("A".$col,$szHeading);
            $objPHPExcel->getActiveSheet()->setCellValue("D".$col,$fWorkingCapital); 
            $col++; 
            $counter = strtotime("+1 MONTH",$counter); 
        }

        $title = "Transporteca_working_capital_graph";
        $objPHPExcel->getActiveSheet()->setTitle('Transporteca Working Capital');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->removeSheetByIndex(1);
        $file = $title;
        $fileName=__APP_PATH_ROOT__."/insuranceSheets/".$file.".xlsx"; 
        //echo "file: ".$fileName ;
        $objWriter->save($fileName); 
        return $fileName; 
    } 
    
    function createProductOrdersExcelFile()
    {   
        $kBooking = new cBooking(); 
        require_once( __APP_PATH_CLASSES__ . "/PHPExcel.php" );
  
        $sheetIndex=0;
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex($sheetIndex);
        $sheet = $objPHPExcel->getActiveSheet(); 

        $styleArray = array(
                'font' => array(
                        'bold' => false,
                        'size' =>12,
                ),
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                ),
                'borders' => array(
                        'top' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'bottom' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'right' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'left' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                ),
        );

        $styleArray1 = array(
                'font' => array(
                        'bold' => false,
                        'size' =>12,
                ),
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                ),
                'borders' => array(
                        'top' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'bottom' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'right' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'left' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                ),
        ); 

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40); 

        $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFill()->getStartColor()->setARGB('FFddd9c3'); 

        $objPHPExcel->getActiveSheet()->setCellValue('A1','First Name')->getStyle('A1');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray); 

        $objPHPExcel->getActiveSheet()->setCellValue('B1','Last Name')->getStyle('B1');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->setCellValue('C1','Phone')->getStyle('C1');
        $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->setCellValue('D1','Alternate Phone')->getStyle('D1');
        $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray); 

        $objPHPExcel->getActiveSheet()->setCellValue('E1','Postcode')->getStyle('D1');
        $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);  
        $col = 2; 
        
        $orderAry[0]['szFirstName'] = 'Ajay';
        $orderAry[0]['szLastName'] = 'Jha';
        $orderAry[0]['szPhone'] = '9911466405';
        $orderAry[0]['szAlternatePhone'] = '1299114664';
        $orderAry[0]['szPostcode'] = '201301';
        
        $orderAry[1]['szFirstName'] = 'Ram';
        $orderAry[1]['szLastName'] = 'Rawan';
        $orderAry[1]['szPhone'] = '9911466405';
        $orderAry[1]['szAlternatePhone'] = '1299114664';
        $orderAry[1]['szPostcode'] = '201301';
        
        //Putting data to the sheet 
        $i = $col;
        if(!empty($orderAry))
        {
            foreach($orderAry as $orderArys)
            {
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$orderArys['szFirstName']);
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$orderArys['szLastName']);
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$orderArys['szPhone']);
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$orderArys['szAlternatePhone']);
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$orderArys['szPostcode']);
                ++$i;
            }
        }  
 
        $objPHPExcel->getActiveSheet()->setTitle('Casey Client Data');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->removeSheetByIndex(1);
        $file = $title;
        $fileName=__APP_PATH_ROOT__."/insuranceSheets/".$file.".xlsx";  
        $objWriter->save($fileName); 
        return $fileName; 
    }
    
    function getCurrentWorkingCapitalExcels($dtDateTime)
    {
        if(!empty($dtDateTime))
        { 
            $query="
                SELECT	
                    fPriceUSD as fTotalAmountUSD,
                    iDebitCredit,
                    dtDateTime,
                    szAmountType,
                    szReference
                FROM
                    ".__DBC_SCHEMATA_GRAPH_WORKING_CAPITAL_CURRENT__."  
                WHERE
                    date_format(dtDateTime,'%Y-%m') = '".date('Y-m',strtotime($dtDateTime))."'
                ORDER BY
                    dtDateTime ASC
            ";
            //echo $query ;
            if(($result = $this->exeSQL($query)))
            {   
                $key = 0;
                while($row = $this->getAssoc($result))
                {   
                    $ret_ary[$key]= $row; 
                    $key++;
                }  
                return $ret_ary;
            }
        }
    }
    
    function updateLast3MonthsDataForDashboard()
    {
        $key = date('Y-m-01',strtotime("-3 MONTH"));
        $lastMonthDataAry = array();
        $lastMonthDataAry = $this->getDataForDashboardGraphByMonth($key);
 
        if(!empty($lastMonthDataAry))
        { 
            $fTurnover = (float)($lastMonthDataAry['fTurnover'] + $lastMonthDataAry['fInsuranceTurnOver'] );  
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_ADMIN_DASHBOARD__."
                SET
                    fTurnover = '".(float)$fTurnover."',
                    iBookings = '".(int)$lastMonthDataAry['iBookings']."',
                    fSaleRevenue = '".(int)$lastMonthDataAry['fSaleRevenue']."',
                    dtUpdatedOn = now()
                WHERE
                    MONTH(dtCreatedOn) = '".date('m',strtotime($key))."' 
                AND
                    YEAR(dtCreatedOn) = '".date('Y',strtotime($key))."' 
            "; 
            //echo "<br /><br /> ".$query; 
            //die;
            
            if($result=$this->exeSQL($query))
            {
                return true;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    
    
    function getFileResponseTrackingCsv()
    {   
        $kBooking = new cBooking(); 
        require_once( __APP_PATH_CLASSES__ . "/PHPExcel.php" );

        $objPHPExcel=new PHPExcel(); 

        $sheetIndex=0;
        $objPHPExcel = new PHPExcel();
        
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex($sheetIndex);
        $sheet = $objPHPExcel->getActiveSheet(); 

        $styleArray = array(
                'font' => array(
                        'bold' => false,
                        'size' =>12,
                ),
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                ),
                'borders' => array(
                        'top' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'bottom' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'right' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'left' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                ),
        );

        $styleArray1 = array(
                'font' => array(
                        'bold' => false,
                        'size' =>12,
                ),
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                ),
                'borders' => array(
                        'top' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'bottom' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'right' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'left' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                ),
        ); 

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(40); 

        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->getStartColor()->setARGB('FFddd9c3');
         
        $objPHPExcel->getActiveSheet()->setCellValue('A1','File #')->getStyle('A1');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray); 

        $objPHPExcel->getActiveSheet()->setCellValue('B1','File Created')->getStyle('B1');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->setCellValue('C1','Response Time')->getStyle('C1');
        $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
  
        $objPHPExcel->getActiveSheet()->setCellValue('D1','Time Difference(Minutes)')->getStyle('D1');
        $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray); 
        
        $objPHPExcel->getActiveSheet()->setCellValue('E1','Button Clicked')->getStyle('E1');
        $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('F1','Clicked By')->getStyle('F1');
        $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);  
        $col = 2;
        /* 
        * Fetching data for last 2 months 
        */ 
 
        $kConfig = new cConfig();
  
        $lastMonthDataAry = array();
        $lastMonthDataAry = $this->getTrackingDataForSheet(1); 
           
        $iTotalResponseTime = 0;
        if(!empty($lastMonthDataAry))
        {
            foreach($lastMonthDataAry as $lastMonthDataArys)
            {     
                $objPHPExcel->getActiveSheet()->setCellValue("A".$col,$lastMonthDataArys['szFileNumber']);
                $objPHPExcel->getActiveSheet()->setCellValue("B".$col,$lastMonthDataArys['dtFileCreated']);
                $objPHPExcel->getActiveSheet()->setCellValue("C".$col,$lastMonthDataArys['dtResponseTime']);
                $objPHPExcel->getActiveSheet()->setCellValue("D".$col,$lastMonthDataArys['iSearchResponseTime']);  
                $objPHPExcel->getActiveSheet()->setCellValue("E".$col,$lastMonthDataArys['szButtonClicked']); 
                $objPHPExcel->getActiveSheet()->setCellValue("F".$col,$lastMonthDataArys['szAdminName']); 
                $col++;
                $iTotalResponseTime += $lastMonthDataArys['iSearchResponseTime'];
            }
        } 
        
        $szHeading = "Graph data for month ".date('F, Y');
        $szSubHeading = " Total Response Time: ".$iTotalResponseTime ;
        $col++;
        
        $objPHPExcel->getActiveSheet()->getStyle('A'.$col.':F'.$col)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A'.$col.':F'.$col)->getFill()->getStartColor()->setARGB('FFFF00');
         
        $objPHPExcel->getActiveSheet()->setCellValue("A".$col,$szHeading);
        $objPHPExcel->getActiveSheet()->setCellValue("D".$col,$szSubHeading); 
        $col++; 

        $title = "Transporteca_response_tracker_graph";
        $objPHPExcel->getActiveSheet()->setTitle('Automatic Search');
         
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(1);
        $objPHPExcel->getActiveSheet()->setTitle('RFQ');
        
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(40); 

        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->getStartColor()->setARGB('FFddd9c3');
         
        $objPHPExcel->getActiveSheet()->setCellValue('A1','File #')->getStyle('A1');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray); 

        $objPHPExcel->getActiveSheet()->setCellValue('B1','File Created')->getStyle('B1');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->setCellValue('C1','Response Time')->getStyle('C1');
        $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
  
        $objPHPExcel->getActiveSheet()->setCellValue('D1','Time Difference(Hours)')->getStyle('D1');
        $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray); 
        
        $objPHPExcel->getActiveSheet()->setCellValue('E1','Button Clicked')->getStyle('E1');
        $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('F1','Clicked By')->getStyle('F1');
        $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);  
        $col = 2;
        /* 
        * Fetching data for last 2 months 
        */ 
 
        $kConfig = new cConfig();
  
        $lastMonthDataAry = array();
        $lastMonthDataAry = $this->getTrackingDataForSheet(2); 
           
        $iTotalResponseTime = 0;
        if(!empty($lastMonthDataAry))
        {
            foreach($lastMonthDataAry as $lastMonthDataArys)
            {     
                $objPHPExcel->getActiveSheet()->setCellValue("A".$col,$lastMonthDataArys['szFileNumber']);
                $objPHPExcel->getActiveSheet()->setCellValue("B".$col,$lastMonthDataArys['dtFileCreated']);
                $objPHPExcel->getActiveSheet()->setCellValue("C".$col,$lastMonthDataArys['dtResponseTime']);
                $objPHPExcel->getActiveSheet()->setCellValue("D".$col,$lastMonthDataArys['iSearchResponseTime']);  
                $objPHPExcel->getActiveSheet()->setCellValue("E".$col,$lastMonthDataArys['szButtonClicked']); 
                $objPHPExcel->getActiveSheet()->setCellValue("F".$col,$lastMonthDataArys['szAdminName']); 
                $col++;
                
                $iTotalResponseTime += $lastMonthDataArys['iSearchResponseTime'];
            }
        } 
        
        $szHeading = "Graph data for month ".date('F, Y');
        $szSubHeading = " Total Response Time: ".$iTotalResponseTime ;
        $col++;
        
        $objPHPExcel->getActiveSheet()->getStyle('A'.$col.':F'.$col)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A'.$col.':F'.$col)->getFill()->getStartColor()->setARGB('FFFF00');
         
        $objPHPExcel->getActiveSheet()->setCellValue("A".$col,$szHeading);
        $objPHPExcel->getActiveSheet()->setCellValue("D".$col,$szSubHeading); 
        $col++; 
                        
        $objPHPExcel->getActiveSheet()->setTitle('RFQ Search');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->removeSheetByIndex(2);
        
        $file = $title;
        $fileName=__APP_PATH_ROOT__."/insuranceSheets/".$file.".xlsx"; 
        
        $objWriter->save($fileName); 
        return $fileName; 
    }
    
    function getTrackingDataForSheet($iSearchType=1)
    {
        if($iSearchType>0)
        {
            if($iSearchType>0)
            {
                $query_and = " AND iSearchType = '".(int)$iSearchType."' ";
            }
            
            if($iSearchType==1)
            {
                $query_select = " TIMESTAMPDIFF(MINUTE,dtFileCreated,dtResponseTime) as iSearchResponseTime, ";
            }
            else
            {
                $query_select = " TIMESTAMPDIFF(HOUR,dtFileCreated,dtResponseTime) as iSearchResponseTime, ";
            }
            
            $query=" 
                SELECT	
                    idUser,
                    idBooking,
                    idFile,
                    dtFileCreated,
                    dtResponseTime,
                    iSearchType,
                    dtCreatedOn,
                    szButtonClicked,
                    idAdminAddedBy, 
                    $query_select
                    (SELECT f1.szFileRef FROM ".__DBC_SCHEMATA_FILE_LOGS__." f1 WHERE f1.id = track2.idFile) as szFileNumber,
                    (SELECT CONCAT_WS(' ',szFirstName,szLastName) FROM ".__DBC_SCHEMATA_MANAGEMENT__." adm WHERE adm.id = track2.idAdminAddedBy) as szAdminName
                FROM
                    ".__DBC_SCHEMATA_RESPONSE_TIME_TRACKER__." as track2
                WHERE   
                    isDeleted = '0'  
                    $query_and  
                ORDER BY
                  dtCreatedOn ASC 
            ";
            //echo $query;
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                $ret_ary = array();
                $ctr=0;
                while($row=$this->getAssoc($result))
                {
                    $ret_ary[$ctr] = $row;
                    $ctr++;
                }
                return $ret_ary;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
    }
    
    function updateEmailSequence($iSequence,$idCmsSection)
    {  
        if($idCmsSection>0 && $iSequence>0)
        {
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_CMS_EMAIL__."
                SET
                    iSequence = '".(float)$iSequence."'  
                WHERE 
                    id = '".(int)$idCmsSection."'
            ";  
//            echo "<br><br> ".$query."<br> ";
//            return false;
//            echo "updatede ";
            
            if($result=$this->exeSQL($query))
            {
                return true;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
        }
    }
    
    
    function insertDataPineData()
    {
        $transportecaUserAry=$this->getAllTransportecaUsers();
        
        
        if(!empty($transportecaUserAry))
        {
            $user_str = implode(",",$transportecaUserAry);
            $query_and = " AND idUser NOT IN (".$user_str.") "; 
        }
        
        $iTotalBookings=0;
        $iSearches=0;
        $query="
            SELECT 
                count( id )AS iTotalBookings
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE 
                `idBookingStatus` IN ( 3, 4 )
            AND 
                iTransferConfirmed = '0'
            AND 
                date_format( `dtBookingConfirmed` , '%Y-%m-%d' ) = DATE_FORMAT( NOW( ) , '%Y-%m-%d' )";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
           $row=$this->getAssoc($result);
           $iTotalBookings=$row['iTotalBookings'];
        }
        //echo "<br><br> ".$query."<br>";
        $ctr=0;
        $res[$ctr]['iTtotal']=$iTotalBookings;
        $res[$ctr]['szKey']="__TOTAL_BOOKING__";
        
        
        
        $query="
            SELECT	
                IFNULL(COUNT(id),0) AS iSearches		
            FROM
                ".__DBC_SCHEMATA_COMPARE_BOOKING__."
            WHERE
                date_format(dtDateTime, '%Y-%m-%d' ) = DATE_FORMAT( NOW( ) , '%Y-%m-%d' ) 
            $query_and 
     	";
        //echo "<br><br> ".$query."<br>";
     	if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            $row=$this->getAssoc($result);

            $iSearches=$row['iSearches'];
        }
        
//        $iNumBookingPerNumSearch="0.00";
//        if((float)$iSearches>0){
//            $iNumBookingPerNumSearch=$iTotalBookings/$iSearches;
//        }
        
        $res[++$ctr]['iTtotal']=$iSearches;
        $res[$ctr]['szKey']="__TOTAL_SEARCH__";
        
        
        $iUsers=0;
         $query="
            SELECT	
                IFNULL(COUNT(id),0) AS iUsers		
            FROM
                ".__DBC_SCHEMATA_USERS__."
            WHERE
                date_format(dtCreateOn, '%Y-%m-%d' ) = DATE_FORMAT( NOW( ) , '%Y-%m-%d' )
     	";
       // echo "<br><br> ".$query."<br>";
     	if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            $row=$this->getAssoc($result);

            $iUsers=$row['iUsers'];
        }
        
        $res[++$ctr]['iTtotal']=$iUsers;
        $res[$ctr]['szKey']="__TOTAL_NEW_USERS__";
        
//        $iNumNewUserPerNumSearch="0.00";
//        if((float)$iSearches>0){
//            $iNumNewUserPerNumSearch=$iUsers/$iSearches;
//        }
        
        
//        $res[++$ctr]['iTtotal']=$iNumNewUserPerNumSearch;
//        $res[$ctr]['szKey']="__NUM_NEW_USER_PER_NUM_SEARCH__";
        
        
//        $iNumBooingPerNumNewUser="0.00";
//        if((float)$iUsers>0){
//            $iNumBooingPerNumNewUser=$iTotalBookings/$iUsers;
//        }
        
//        $res[++$ctr]['iTtotal']=$iNumBooingPerNumNewUser;
//        $res[$ctr]['szKey']="__NUM_BOOKING_PER_NEW_USER__";
        
        
        $query="
            SELECT 
                count( id )AS iTotalAutomaticBookings
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE
                iBookingType IN ('1','3')
            AND
                `idBookingStatus` IN ( 3, 4 )
            AND 
                iTransferConfirmed = '0'
            AND 
                date_format( `dtBookingConfirmed` , '%Y-%m-%d' ) = DATE_FORMAT( NOW( ) , '%Y-%m-%d' )";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
           $row=$this->getAssoc($result);
           $iTotalAutomaticBookings=$row['iTotalAutomaticBookings'];
        }
        
        $res[++$ctr]['iTtotal']=$iTotalAutomaticBookings;
        $res[$ctr]['szKey']="__TOTAL_AUTOMATIC_BOOKING__";
        
        $iTotalUserCall=0;
        $query="
            SELECT 
                count( id )AS iTotalUserCall
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE
                iBookingType IN ('1','3')
             AND 
                date_format( `dtCreatedOn` , '%Y-%m-%d' ) = DATE_FORMAT( NOW( ) , '%Y-%m-%d' )";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
           $row=$this->getAssoc($result);
           $iTotalUserCall=$row['iTotalUserCall'];
        }
        
        $res[++$ctr]['iTtotal']=$iTotalUserCall;
        $res[$ctr]['szKey']="__TOTAL_USER_CALL__";
        
        //echo "<br><br> ".$query."<br>";
        
//        $iNumAutomaticBookingPerNumUserCall=0;
//        if($iTotalUserCall>0)
//        {
//            $iNumAutomaticBookingPerNumUserCall=$iTotalAutomaticBookings/$iTotalUserCall;
//        }
//        
//        $res[++$ctr]['iTtotal']=$iNumAutomaticBookingPerNumUserCall;
//        $res[$ctr]['szKey']="__NUM_AUTOMATIC_BOOKING_PER_USER_CALL__";
//        
        
        $iTotalValidateUserCall=0;
        $query="
            SELECT 
                count( b.id )AS iTotalValidateUserCall
            FROM
                ".__DBC_SCHEMATA_BOOKING__." AS b
            WHERE
                b.iBookingType IN ('2')
             AND 
                date_format( b.`dtCreatedOn` , '%Y-%m-%d' ) = DATE_FORMAT( NOW( ) , '%Y-%m-%d' )";
        //echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
           $row=$this->getAssoc($result);
           $iTotalValidateUserCall=$row['iTotalValidateUserCall'];
        }
        
        $res[++$ctr]['iTtotal']=$iTotalValidateUserCall;
        $res[$ctr]['szKey']="__TOTAL_VALIDATE_USER_CALL__";
        
        $iTotalCalls=(int)($iTotalUserCall+$iTotalValidateUserCall);
        $res[++$ctr]['iTtotal']=$iTotalCalls;
        $res[$ctr]['szKey']="__NUM_USER_CALL_AND_VALIDATE_USER_CALL__";
        
//        $iRatioUserCall=0;
//        if($iTotalCalls>0){
//            $iRatioUserCall=$iTotalUserCall/$iTotalCalls;
//        }
        
//        $res[++$ctr]['iTtotal']=$iRatioUserCall;
//        $res[$ctr]['szKey']="__NUM_USER_CALL_RATIO_TOTAL_USER_CALL__";
        
        $iTotalManualBookings=0;
        $query="
            SELECT 
                count( id )AS iTotalManualBookings
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE
                iBookingType IN ('2')
            AND
                `idBookingStatus` IN ( 3, 4 )
            AND 
                iTransferConfirmed = '0'
            AND 
                date_format( `dtBookingConfirmed` , '%Y-%m-%d' ) = DATE_FORMAT( NOW( ) , '%Y-%m-%d' )";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
           $row=$this->getAssoc($result);
           $iTotalManualBookings=$row['iTotalManualBookings'];
        }
        $res[++$ctr]['iTtotal']=$iTotalManualBookings;
        $res[$ctr]['szKey']="__TOTAL_MANUAL_BOOKING__";
        
        $query="
            SELECT	
                IFNULL(COUNT(cb.id),0) AS iSearches,
                l.szName,
                l.id AS idLanguage
            FROM
                ".__DBC_SCHEMATA_COMPARE_BOOKING__." AS cb
            INNER JOIN
                ".__DBC_SCHEMATA_CUSTOM_LANDING_PAGE__." AS clp
            ON
                cb.idLandingPage=clp.id
            INNER JOIN
                ".__DBC_SCHEMATA_LANGUAGE__." AS l
            ON
                clp.iLanguage=l.id    
            WHERE
                date_format(cb.dtDateTime, '%Y-%m-%d' ) = DATE_FORMAT( NOW( ) , '%Y-%m-%d' ) 
            $query_and 
            GROUP BY
                iLanguage
     	";
     	if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            while($row=$this->getAssoc($result))
            {

            //$iSearches=$row['iSearches'];
                $res[++$ctr]['iTtotal']=$row['iSearches'];
                $res[$ctr]['szKey']="__NUM_SEARCH_PER_LANGUAGE__";
                $res[$ctr]['idLanuage']=$row['idLanguage'];
            }
        
        }
        
//        $iLeadRatio=0;
//        if($iSearches>0){
//            $iLeadRatio=$iTotalCalls/$iSearches;
//        }
//        
//        $res[++$ctr]['iTtotal']=$iLeadRatio;
//        $res[$ctr]['szKey']="__NUM_LEAD_RATIO__";
        
         $query="
            SELECT 
                count( id )AS iTotalCheckOut
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE
            (
               date_format( `dtCreatedOn` , '%Y-%m-%d' ) = DATE_FORMAT( NOW( ), '%Y-%m-%d'  )
            OR
               date_format( `dtBookingConfirmed` , '%Y-%m-%d' ) = DATE_FORMAT( NOW( ), '%Y-%m-%d'  )
            )
            AND
                iBookingStep >='11'
            ";
            if( ( $result = $this->exeSQL( $query ) ) )
            {
               $row=$this->getAssoc($result);
               $iTotalCheckOut=$row['iTotalCheckOut'];
               
                $res[++$ctr]['iTtotal']=$iTotalCheckOut;
                $res[$ctr]['szKey']="__NUM_CHECK_OUT__";
            }
            
            
            
            $query="
                SELECT COUNT(*) AS iTotalUserOrderMoreThanOnce
                    FROM
                    (
                        SELECT
                            id
                        FROM
                            ".__DBC_SCHEMATA_BOOKING__."
                        WHERE  
                            idBookingStatus IN(3,4)
                        AND 
                            iTransferConfirmed = '0'
                        AND
                            date_format( `dtBookingConfirmed` , '%Y-%m' ) <= DATE_FORMAT( NOW( ), '%Y-%m'  )
                        GROUP BY 
                            idUser
                        HAVING
                            COUNT(*) > 1
                    ) T1
               ";
            if( ( $result = $this->exeSQL( $query ) ) )
            {
               $row=$this->getAssoc($result);
               $iTotalUserOrderMoreThanOnce=$row['iTotalUserOrderMoreThanOnce'];

                $res[++$ctr]['iTtotal']=$iTotalUserOrderMoreThanOnce;
                $res[$ctr]['szKey']="__TOTAL_USER_ORDER_MORE_THAN_ONCE__";
            }

            $query="
                SELECT COUNT(*) AS iTotalUserOrderOnce
                    FROM
                    (
                        SELECT
                            id
                        FROM
                            ".__DBC_SCHEMATA_BOOKING__."
                        WHERE  
                            idBookingStatus IN(3,4)
                        AND 
                            iTransferConfirmed = '0'
                        AND
                            date_format( `dtBookingConfirmed` , '%Y-%m' ) <= DATE_FORMAT( NOW( ), '%Y-%m'  )
                        GROUP BY 
                            idUser
                        HAVING
                            COUNT(*) > 0
                    ) T1
               ";
            if( ( $result = $this->exeSQL( $query ) ) )
            {
               $row=$this->getAssoc($result);
               $iTotalUserOrderOnce=$row['iTotalUserOrderOnce'];

                $res[++$ctr]['iTtotal']=$iTotalUserOrderOnce;
                $res[$ctr]['szKey']="__TOTAL_USER_ORDER_ONCE__";
            }
            
            $query="
            SELECT 
                count( b.id )AS iTotalQuoteSent
            FROM
                ".__DBC_SCHEMATA_BOOKING__." AS b
            INNER JOIN
            	".__DBC_SCHEMATA_FILE_LOGS__." AS bf
            ON
            	bf.idBooking=b.id    
            WHERE
                b.iBookingType IN ('2')
            AND
            	bf.iQuoteSent='1'
           AND 
                date_format( bf.`dtQuoteSend` , '%Y-%m-%d' ) = DATE_FORMAT( NOW( ), '%Y-%m-%d' )     ";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
           $row=$this->getAssoc($result);
           $iTotalQuoteSent=$row['iTotalQuoteSent'];
        }
        $res[++$ctr]['iTtotal']=$iTotalQuoteSent;
        $res[$ctr]['szKey']="__TOTAL_QUOTE_SENT__";
        
//        $iCheckOutRatio=0;
//        if($iTotalCalls>0){
//            $iCheckOutRatio=$iTotalCheckOut/$iTotalCalls;
//        }
//        
//        $res[++$ctr]['iTtotal']=$iCheckOutRatio;
//        $res[$ctr]['szKey']="__NUM_CHECK_OUT_RATIO__";
        
        
//        $iBookingRatio=0;
//        if($iTotalCheckOut>0){
//            $iBookingRatio=$iTotalBookings/$iTotalCheckOut;
//        }
//        
//        $res[++$ctr]['iTtotal']=$iBookingRatio;
//        $res[$ctr]['szKey']="__NUM_BOOKING_RATIO__";
//        
        $currentMonth=date('M-Y');
        $currentMonthDate=date('Y-m-d');
        if(!empty($res))
        {
            foreach($res as $ress)
            {
                if($ress['idLanuage']==0)
                {
                    $ress['idLanuage']=1;
                }
                
                if($ress['szKey']=='__TOTAL_USER_ORDER_ONCE__' || $ress['szKey']=='__TOTAL_USER_ORDER_MORE_THAN_ONCE__')
                {
                    $currentMonth=date('M-Y');
                    $currentMonthDate=date('Y-m-01');
                }else{
                    $currentMonth=date('M-Y');
                    $currentMonthDate=date('Y-m-d');
                }
                    
                $query="
                    SELECT
                        id 
                    FROM
                        ".__DBC_SCHEMATA_DATA_PINE__."
                    WHERE
                        szMonth='".mysql_escape_custom($currentMonth)."'
                    AND
                        szKey='".mysql_escape_custom($ress['szKey'])."'
                    AND        
                        DATE(dtMonth)='".mysql_escape_custom($currentMonthDate)."'
                    AND        
                        idLanuage='".mysql_escape_custom($ress['idLanuage'])."'        
                ";
                //echo $query."<br /><br />";
                if( ( $result = $this->exeSQL( $query ) ) )
                {
                    
                    if( $this->getRowCnt() <= 0 )
                    {
                        $query="
                            INSERT INTO
                                ".__DBC_SCHEMATA_DATA_PINE__."
                            (

                                szKey,
                                iTotal,
                                szMonth,
                                iType,
                                dtMonth,
                                idLanuage
                            )
                                VALUES
                            (
                                '".mysql_escape_custom($ress['szKey'])."',
                                '".round((float)$ress['iTtotal'],2)."',
                                '".mysql_escape_custom($currentMonth)."',
                                '1',
                                '".mysql_escape_custom($currentMonthDate)."',
                                '".mysql_escape_custom($ress['idLanuage'])."'    
                            )
                        ";
                    }
                    else
                    { 
                
                        $query="
                            UPDATE
                                ".__DBC_SCHEMATA_DATA_PINE__."
                            SET        
                                iTotal='".round((float)$ress['iTtotal'],2)."'
                            WHERE
                                szMonth='".mysql_escape_custom($currentMonth)."'
                            AND
                                szKey='".mysql_escape_custom($ress['szKey'])."'
                            AND        
                                dtMonth='".mysql_escape_custom($currentMonthDate)."'
                            AND        
                                idLanuage='".mysql_escape_custom($ress['idLanuage'])."'        
                        ";
                    }
                
                  // echo $query."<br /><br />";
                    $result = $this->exeSQL( $query );
                }
            }
        }
        
    }
    
    
    
    function insertDataPinePrevDayData()
    {
        $transportecaUserAry=$this->getAllTransportecaUsers();
        
        
        if(!empty($transportecaUserAry))
        {
            $user_str = implode(",",$transportecaUserAry);
            $query_and = " AND idUser NOT IN (".$user_str.") "; 
        }
        
        $iTotalBookings=0;
        $iSearches=0;
        $query="
            SELECT 
                count( id )AS iTotalBookings
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE 
                `idBookingStatus` IN ( 3, 4 )
            AND 
                iTransferConfirmed = '0'
            AND 
                date_format( `dtBookingConfirmed` , '%Y-%m-%d' ) = DATE_FORMAT( NOW( ) - INTERVAL 1 DAY , '%Y-%m-%d' )";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
           $row=$this->getAssoc($result);
           $iTotalBookings=$row['iTotalBookings'];
        }
       // echo "<br><br> ".$query."<br>";
        $ctr=0;
        $res[$ctr]['iTtotal']=$iTotalBookings;
        $res[$ctr]['szKey']="__TOTAL_BOOKING__";
        
        
        
        $query="
            SELECT	
                IFNULL(COUNT(id),0) AS iSearches		
            FROM
                ".__DBC_SCHEMATA_COMPARE_BOOKING__."
            WHERE
                date_format(dtDateTime, '%Y-%m-%d' ) = DATE_FORMAT( NOW( )  - INTERVAL 1 DAY, '%Y-%m-%d' ) 
            $query_and 
     	";
        //echo "<br><br> ".$query."<br>";
     	if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            $row=$this->getAssoc($result);

            $iSearches=$row['iSearches'];
        }
        
//        $iNumBookingPerNumSearch="0.00";
//        if((float)$iSearches>0){
//            $iNumBookingPerNumSearch=$iTotalBookings/$iSearches;
//        }
        
        $res[++$ctr]['iTtotal']=$iSearches;
        $res[$ctr]['szKey']="__TOTAL_SEARCH__";
        
        
        $iUsers=0;
         $query="
            SELECT	
                IFNULL(COUNT(id),0) AS iUsers		
            FROM
                ".__DBC_SCHEMATA_USERS__."
            WHERE
                date_format(dtCreateOn, '%Y-%m-%d' ) = DATE_FORMAT( NOW( ) - INTERVAL 1 DAY , '%Y-%m-%d' )
     	";
        //echo "<br><br> ".$query."<br>";
     	if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            $row=$this->getAssoc($result);

            $iUsers=$row['iUsers'];
        }
        
        $res[++$ctr]['iTtotal']=$iUsers;
        $res[$ctr]['szKey']="__TOTAL_NEW_USERS__";
        
//        $iNumNewUserPerNumSearch="0.00";
//        if((float)$iSearches>0){
//            $iNumNewUserPerNumSearch=$iUsers/$iSearches;
//        }
        
        
//        $res[++$ctr]['iTtotal']=$iNumNewUserPerNumSearch;
//        $res[$ctr]['szKey']="__NUM_NEW_USER_PER_NUM_SEARCH__";
        
        
//        $iNumBooingPerNumNewUser="0.00";
//        if((float)$iUsers>0){
//            $iNumBooingPerNumNewUser=$iTotalBookings/$iUsers;
//        }
        
//        $res[++$ctr]['iTtotal']=$iNumBooingPerNumNewUser;
//        $res[$ctr]['szKey']="__NUM_BOOKING_PER_NEW_USER__";
        
        
        $query="
            SELECT 
                count( id )AS iTotalAutomaticBookings
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE
                iBookingType IN ('1','3')
            AND
                `idBookingStatus` IN ( 3, 4 )
            AND 
                iTransferConfirmed = '0'
            AND 
                date_format( `dtBookingConfirmed` , '%Y-%m-%d' ) = DATE_FORMAT( NOW( ) - INTERVAL 1 DAY , '%Y-%m-%d' )";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
           $row=$this->getAssoc($result);
           $iTotalAutomaticBookings=$row['iTotalAutomaticBookings'];
        }
        
        $res[++$ctr]['iTtotal']=$iTotalAutomaticBookings;
        $res[$ctr]['szKey']="__TOTAL_AUTOMATIC_BOOKING__";
        
        $iTotalUserCall=0;
        $query="
            SELECT 
                count( id )AS iTotalUserCall
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE
                iBookingType IN ('1','3')
             AND 
                date_format( `dtCreatedOn` , '%Y-%m-%d' ) = DATE_FORMAT( NOW( ) - INTERVAL 1 DAY , '%Y-%m-%d' )";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
           $row=$this->getAssoc($result);
           $iTotalUserCall=$row['iTotalUserCall'];
        }
        
        $res[++$ctr]['iTtotal']=$iTotalUserCall;
        $res[$ctr]['szKey']="__TOTAL_USER_CALL__";
        
        //echo "<br><br> ".$query."<br>";
        
//        $iNumAutomaticBookingPerNumUserCall=0;
//        if($iTotalUserCall>0)
//        {
//            $iNumAutomaticBookingPerNumUserCall=$iTotalAutomaticBookings/$iTotalUserCall;
//        }
//        
//        $res[++$ctr]['iTtotal']=$iNumAutomaticBookingPerNumUserCall;
//        $res[$ctr]['szKey']="__NUM_AUTOMATIC_BOOKING_PER_USER_CALL__";
//        
        
        $iTotalValidateUserCall=0;
        $query="
            SELECT 
                count( b.id )AS iTotalValidateUserCall
            FROM
                ".__DBC_SCHEMATA_BOOKING__." AS b
            WHERE
                b.iBookingType IN ('2')
             AND 
                date_format( b.`dtCreatedOn` , '%Y-%m-%d' ) = DATE_FORMAT( NOW( )  - INTERVAL 1 DAY, '%Y-%m-%d' )";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
           $row=$this->getAssoc($result);
           $iTotalValidateUserCall=$row['iTotalValidateUserCall'];
        }
        
        $res[++$ctr]['iTtotal']=$iTotalValidateUserCall;
        $res[$ctr]['szKey']="__TOTAL_VALIDATE_USER_CALL__";
        
        $iTotalCalls=(int)($iTotalUserCall+$iTotalValidateUserCall);
        $res[++$ctr]['iTtotal']=$iTotalCalls;
        $res[$ctr]['szKey']="__NUM_USER_CALL_AND_VALIDATE_USER_CALL__";
        
//        $iRatioUserCall=0;
//        if($iTotalCalls>0){
//            $iRatioUserCall=$iTotalUserCall/$iTotalCalls;
//        }
        
//        $res[++$ctr]['iTtotal']=$iRatioUserCall;
//        $res[$ctr]['szKey']="__NUM_USER_CALL_RATIO_TOTAL_USER_CALL__";
        
        $iTotalManualBookings=0;
        $query="
            SELECT 
                count( id )AS iTotalManualBookings
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE
                iBookingType IN ('2')
            AND
                `idBookingStatus` IN ( 3, 4 )
            AND 
                iTransferConfirmed = '0'
            AND 
                date_format( `dtBookingConfirmed` , '%Y-%m-%d' ) = DATE_FORMAT( NOW( )  - INTERVAL 1 DAY, '%Y-%m-%d' )";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
           $row=$this->getAssoc($result);
           $iTotalManualBookings=$row['iTotalManualBookings'];
        }
        $res[++$ctr]['iTtotal']=$iTotalManualBookings;
        $res[$ctr]['szKey']="__TOTAL_MANUAL_BOOKING__";
        
        $query="
            SELECT	
                IFNULL(COUNT(cb.id),0) AS iSearches,
                l.szName,
                l.id AS idLanguage
            FROM
                ".__DBC_SCHEMATA_COMPARE_BOOKING__." AS cb
            INNER JOIN
                ".__DBC_SCHEMATA_CUSTOM_LANDING_PAGE__." AS clp
            ON
                cb.idLandingPage=clp.id
            INNER JOIN
                ".__DBC_SCHEMATA_LANGUAGE__." AS l
            ON
                clp.iLanguage=l.id    
            WHERE
                date_format(cb.dtDateTime, '%Y-%m-%d' ) = DATE_FORMAT( NOW( )  - INTERVAL 1 DAY, '%Y-%m-%d' ) 
            $query_and 
            GROUP BY
                iLanguage
     	";
     	if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            while($row=$this->getAssoc($result))
            {

            //$iSearches=$row['iSearches'];
                $res[++$ctr]['iTtotal']=$row['iSearches'];
                $res[$ctr]['szKey']="__NUM_SEARCH_PER_LANGUAGE__";
                $res[$ctr]['idLanuage']=$row['idLanguage'];
            }
        
        }
        
//        $iLeadRatio=0;
//        if($iSearches>0){
//            $iLeadRatio=$iTotalCalls/$iSearches;
//        }
//        
//        $res[++$ctr]['iTtotal']=$iLeadRatio;
//        $res[$ctr]['szKey']="__NUM_LEAD_RATIO__";
        
         $query="
            SELECT 
                count( id )AS iTotalCheckOut
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE
            (
               date_format( `dtCreatedOn` , '%Y-%m-%d' ) = DATE_FORMAT( NOW( ) - INTERVAL 1 DAY, '%Y-%m-%d'  )
            OR
               date_format( `dtBookingConfirmed` , '%Y-%m-%d' ) = DATE_FORMAT( NOW( ) - INTERVAL 1 DAY, '%Y-%m-%d'  )
            )
            AND
                iBookingStep >='11'
            ";
            if( ( $result = $this->exeSQL( $query ) ) )
            {
               $row=$this->getAssoc($result);
               $iTotalCheckOut=$row['iTotalCheckOut'];
                
                $res[++$ctr]['iTtotal']=$iTotalCheckOut;
                $res[$ctr]['szKey']="__NUM_CHECK_OUT__";
            }
        
        
//        $iCheckOutRatio=0;
//        if($iTotalCalls>0){
//            $iCheckOutRatio=$iTotalCheckOut/$iTotalCalls;
//        }
//        
//        $res[++$ctr]['iTtotal']=$iCheckOutRatio;
//        $res[$ctr]['szKey']="__NUM_CHECK_OUT_RATIO__";
        
        
//        $iBookingRatio=0;
//        if($iTotalCheckOut>0){
//            $iBookingRatio=$iTotalBookings/$iTotalCheckOut;
//        }
//        
//        $res[++$ctr]['iTtotal']=$iBookingRatio;
//        $res[$ctr]['szKey']="__NUM_BOOKING_RATIO__";
            
        
        $query="
            SELECT COUNT(*) AS iTotalUserOrderMoreThanOnce
                FROM
                (
                    SELECT
                        id
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__."
                    WHERE  
                        idBookingStatus IN(3,4)
                    AND 
                        iTransferConfirmed = '0'
                    AND
                        date_format( `dtBookingConfirmed` , '%Y-%m' ) <= DATE_FORMAT( NOW( ) - INTERVAL 1 MONTH, '%Y-%m'  )
                    GROUP BY 
                        idUser
                    HAVING
                        COUNT(*) > 1
                ) T1
           ";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
           $row=$this->getAssoc($result);
           $iTotalUserOrderMoreThanOnce=$row['iTotalUserOrderMoreThanOnce'];

            $res[++$ctr]['iTtotal']=$iTotalUserOrderMoreThanOnce;
            $res[$ctr]['szKey']="__TOTAL_USER_ORDER_MORE_THAN_ONCE__";
        }

        $query="
            SELECT COUNT(*) AS iTotalUserOrderOnce
                FROM
                (
                    SELECT
                        id
                    FROM
                        ".__DBC_SCHEMATA_BOOKING__."
                    WHERE  
                        idBookingStatus IN(3,4)
                    AND 
                        iTransferConfirmed = '0'
                    AND
                        date_format( `dtBookingConfirmed` , '%Y-%m' ) <= DATE_FORMAT( NOW( ) - INTERVAL 1 MONTH, '%Y-%m'  )
                    GROUP BY 
                        idUser
                    HAVING
                        COUNT(*) > 0
                ) T1
           ";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
           $row=$this->getAssoc($result);
           $iTotalUserOrderOnce=$row['iTotalUserOrderOnce'];

            $res[++$ctr]['iTtotal']=$iTotalUserOrderOnce;
            $res[$ctr]['szKey']="__TOTAL_USER_ORDER_ONCE__";
        }
        
        
        $query="
            SELECT 
                count( b.id )AS iTotalQuoteSent
            FROM
                ".__DBC_SCHEMATA_BOOKING__." AS b
            INNER JOIN
            	".__DBC_SCHEMATA_FILE_LOGS__." AS bf
            ON
            	bf.idBooking=b.id    
            WHERE
                b.iBookingType IN ('2')
            AND
            	bf.iQuoteSent='1'
           AND 
                date_format( bf.`dtQuoteSend` , '%Y-%m-%d' ) = DATE_FORMAT( NOW( )  - INTERVAL 1 DAY, '%Y-%m-%d' )     ";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
           $row=$this->getAssoc($result);
           $iTotalQuoteSent=$row['iTotalQuoteSent'];
        }
        $res[++$ctr]['iTtotal']=$iTotalQuoteSent;
        $res[$ctr]['szKey']="__TOTAL_QUOTE_SENT__";
        
        
        $query="
            SELECT
                DATE_FORMAT( NOW( )  - INTERVAL 1 DAY, '%Y-%m-%d'  ) AS szDate                
        ";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
           $row=$this->getAssoc($result);
           
           $szDate= $row['szDate'];
        }
        
        $prevMonth=date('M-Y',strtotime($szDate));
        $prevMonthDate=date('Y-m-d',strtotime($szDate));
        
        if(!empty($res))
        {
            foreach($res as $ress)
            {
                if($ress['idLanuage']==0)
                {
                    $ress['idLanuage']=1;
                }
                
                if($ress['szKey']=='__TOTAL_USER_ORDER_ONCE__' || $ress['szKey']=='__TOTAL_USER_ORDER_MORE_THAN_ONCE__')
                {
                    $prevMonthDate=date('Y-m-01',strtotime($szDate));


                     $query="
                            SELECT
                                DATE_FORMAT( NOW( )  - INTERVAL 1 MONTH, '%Y-%m-%d'  ) AS szDate                
                        ";
                        //echo $query."<br />";
                        if( ( $result = $this->exeSQL( $query ) ) )
                        {
                           $row=$this->getAssoc($result);

                           $szDate= $row['szDate'];
                        }

                        $prevMonth=date('M-Y',strtotime($szDate));
                        $prevMonthDate=date('Y-m-d',strtotime($szDate));
                }
                else
                {
                    $query="
                        SELECT
                            DATE_FORMAT( NOW( )  - INTERVAL 1 DAY, '%Y-%m-%d'  ) AS szDate                
                    ";
                    //echo $query."<br />";
                    if( ( $result = $this->exeSQL( $query ) ) )
                    {
                       $row=$this->getAssoc($result);

                       $szDate= $row['szDate'];
                    }

                    $prevMonth=date('M-Y',strtotime($szDate));
                    $prevMonthDate=date('Y-m-d',strtotime($szDate));
                }

                $query="
                    SELECT
                        id 
                    FROM
                        ".__DBC_SCHEMATA_DATA_PINE__."
                    WHERE
                        szMonth='".mysql_escape_custom($prevMonth)."'
                    AND
                        szKey='".mysql_escape_custom($ress['szKey'])."'
                    AND        
                        DATE(dtMonth)='".mysql_escape_custom($prevMonthDate)."'
                    AND        
                        idLanuage='".mysql_escape_custom($ress['idLanuage'])."'         
                ";
                //echo $query."<br /><br />";
                if( ( $result = $this->exeSQL( $query ) ) )
                {
                    
                    
                    if( $this->getRowCnt() <= 0 )
                    {
                        $query="
                            INSERT INTO
                                ".__DBC_SCHEMATA_DATA_PINE__."
                            (

                                szKey,
                                iTotal,
                                szMonth,
                                iType,
                                dtMonth,
                                idLanuage
                            )
                                VALUES
                            (
                                '".mysql_escape_custom($ress['szKey'])."',
                                '".round((float)$ress['iTtotal'],2)."',
                                '".mysql_escape_custom($prevMonth)."',
                                '1',
                                '".mysql_escape_custom($prevMonthDate)."',
                                '".mysql_escape_custom($ress['idLanuage'])."'    
                            )
                        ";
                    }
                    else
                    { 
                
                        $query="
                            UPDATE
                                ".__DBC_SCHEMATA_DATA_PINE__."
                            SET        
                                iTotal='".round((float)$ress['iTtotal'],2)."'
                            WHERE
                                szMonth='".mysql_escape_custom($prevMonth)."'
                            AND
                                szKey='".mysql_escape_custom($ress['szKey'])."'
                            AND        
                                dtMonth='".mysql_escape_custom($prevMonthDate)."'
                            AND        
                                idLanuage='".mysql_escape_custom($ress['idLanuage'])."'        
                        ";
                    }
                    $result = $this->exeSQL( $query );
                }
                
            }
        }
        
    }
    
    function insertDataPineImportOldData($number)
    {
         $transportecaUserAry=$this->getAllTransportecaUsers();
        
        
        if(!empty($transportecaUserAry))
        {
            $user_str = implode(",",$transportecaUserAry);
            $query_and = " AND idUser NOT IN (".$user_str.") "; 
        }
        
        $iTotalBookings=0;
        $iSearches=0;
        $query="
            SELECT 
                count( id )AS iTotalBookings
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE 
                `idBookingStatus` IN ( 3, 4 )
            AND 
                iTransferConfirmed = '0'
            AND 
                date_format( `dtBookingConfirmed` , '%Y-%m-%d' ) = DATE_FORMAT( NOW( ) - INTERVAL $number DAY , '%Y-%m-%d' )";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
           $row=$this->getAssoc($result);
           $iTotalBookings=$row['iTotalBookings'];
        }
        //echo "<br><br> ".$query."<br>";
        $ctr=0;
        $res[$ctr]['iTtotal']=$iTotalBookings;
        $res[$ctr]['szKey']="__TOTAL_BOOKING__";
        
        
        
        $query="
            SELECT	
                IFNULL(COUNT(id),0) AS iSearches		
            FROM
                ".__DBC_SCHEMATA_COMPARE_BOOKING__."
            WHERE
                date_format(dtDateTime, '%Y-%m-%d' ) = DATE_FORMAT( NOW( )  - INTERVAL $number DAY, '%Y-%m-%d' ) 
            $query_and 
     	";
        //echo "<br><br> ".$query."<br>";
     	if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            $row=$this->getAssoc($result);

            $iSearches=$row['iSearches'];
        }
        
//        $iNumBookingPerNumSearch="0.00";
//        if((float)$iSearches>0){
//            $iNumBookingPerNumSearch=$iTotalBookings/$iSearches;
//        }
        
        $res[++$ctr]['iTtotal']=$iSearches;
        $res[$ctr]['szKey']="__TOTAL_SEARCH__";
        
        
        $iUsers=0;
         $query="
            SELECT	
                IFNULL(COUNT(id),0) AS iUsers		
            FROM
                ".__DBC_SCHEMATA_USERS__."
            WHERE
                date_format(dtCreateOn, '%Y-%m-%d' ) = DATE_FORMAT( NOW( ) - INTERVAL $number DAY , '%Y-%m-%d' )
     	";
        //echo "<br><br> ".$query."<br>";
     	if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            $row=$this->getAssoc($result);

            $iUsers=$row['iUsers'];
        }
        
        $res[++$ctr]['iTtotal']=$iUsers;
        $res[$ctr]['szKey']="__TOTAL_NEW_USERS__";
        
//        $iNumNewUserPerNumSearch="0.00";
//        if((float)$iSearches>0){
//            $iNumNewUserPerNumSearch=$iUsers/$iSearches;
//        }
        
        
//        $res[++$ctr]['iTtotal']=$iNumNewUserPerNumSearch;
//        $res[$ctr]['szKey']="__NUM_NEW_USER_PER_NUM_SEARCH__";
        
        
//        $iNumBooingPerNumNewUser="0.00";
//        if((float)$iUsers>0){
//            $iNumBooingPerNumNewUser=$iTotalBookings/$iUsers;
//        }
        
//        $res[++$ctr]['iTtotal']=$iNumBooingPerNumNewUser;
//        $res[$ctr]['szKey']="__NUM_BOOKING_PER_NEW_USER__";
        
        
        $query="
            SELECT 
                count( id )AS iTotalAutomaticBookings
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE
                iBookingType IN ('1','3')
            AND
                `idBookingStatus` IN ( 3, 4 )
            AND 
                iTransferConfirmed = '0'
            AND 
                date_format( `dtBookingConfirmed` , '%Y-%m-%d' ) = DATE_FORMAT( NOW( ) - INTERVAL $number DAY , '%Y-%m-%d' )";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
           $row=$this->getAssoc($result);
           $iTotalAutomaticBookings=$row['iTotalAutomaticBookings'];
        }
        
        $res[++$ctr]['iTtotal']=$iTotalAutomaticBookings;
        $res[$ctr]['szKey']="__TOTAL_AUTOMATIC_BOOKING__";
        
        $iTotalUserCall=0;
        $query="
            SELECT 
                count( id )AS iTotalUserCall
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE
                iBookingType IN ('1','3')
             AND 
                date_format( `dtCreatedOn` , '%Y-%m-%d' ) = DATE_FORMAT( NOW( ) - INTERVAL $number DAY , '%Y-%m-%d' )";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
           $row=$this->getAssoc($result);
           $iTotalUserCall=$row['iTotalUserCall'];
        }
        
        $res[++$ctr]['iTtotal']=$iTotalUserCall;
        $res[$ctr]['szKey']="__TOTAL_USER_CALL__";
        
        //echo "<br><br> ".$query."<br>";
        
//        $iNumAutomaticBookingPerNumUserCall=0;
//        if($iTotalUserCall>0)
//        {
//            $iNumAutomaticBookingPerNumUserCall=$iTotalAutomaticBookings/$iTotalUserCall;
//        }
//        
//        $res[++$ctr]['iTtotal']=$iNumAutomaticBookingPerNumUserCall;
//        $res[$ctr]['szKey']="__NUM_AUTOMATIC_BOOKING_PER_USER_CALL__";
//        
        
        $iTotalValidateUserCall=0;
        $query="
            SELECT 
                count( b.id )AS iTotalValidateUserCall
            FROM
                ".__DBC_SCHEMATA_BOOKING__." AS b
            WHERE   
                b.iBookingType IN ('2')
             AND 
                date_format( b.`dtCreatedOn` , '%Y-%m-%d' ) = DATE_FORMAT( NOW( )  - INTERVAL $number DAY, '%Y-%m-%d' )";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
           $row=$this->getAssoc($result);
           $iTotalValidateUserCall=$row['iTotalValidateUserCall'];
        }
        
        $res[++$ctr]['iTtotal']=$iTotalValidateUserCall;
        $res[$ctr]['szKey']="__TOTAL_VALIDATE_USER_CALL__";
        
      $iTotalCalls=(int)($iTotalUserCall+$iTotalValidateUserCall);
      $res[++$ctr]['iTtotal']=$iTotalCalls;
      $res[$ctr]['szKey']="__NUM_USER_CALL_AND_VALIDATE_USER_CALL__";
        
//        $iRatioUserCall=0;
//        if($iTotalCalls>0){
//            $iRatioUserCall=$iTotalUserCall/$iTotalCalls;
//        }
        
//        $res[++$ctr]['iTtotal']=$iRatioUserCall;
//        $res[$ctr]['szKey']="__NUM_USER_CALL_RATIO_TOTAL_USER_CALL__";
        
        $iTotalManualBookings=0;
        $query="
            SELECT 
                count( id )AS iTotalManualBookings
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE
                iBookingType IN ('2')
            AND
                `idBookingStatus` IN ( 3, 4 )
            AND 
                iTransferConfirmed = '0'
            AND 
                date_format( `dtBookingConfirmed` , '%Y-%m-%d' ) = DATE_FORMAT( NOW( )  - INTERVAL $number DAY, '%Y-%m-%d' )";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
           $row=$this->getAssoc($result);
           $iTotalManualBookings=$row['iTotalManualBookings'];
        }
        $res[++$ctr]['iTtotal']=$iTotalManualBookings;
        $res[$ctr]['szKey']="__TOTAL_MANUAL_BOOKING__";
        
        
        $query="
            SELECT 
                count( b.id )AS iTotalQuoteSent
            FROM
                ".__DBC_SCHEMATA_BOOKING__." AS b
            INNER JOIN
            	".__DBC_SCHEMATA_FILE_LOGS__." AS bf
            ON
            	bf.idBooking=b.id    
            WHERE
                b.iBookingType IN ('2')
            AND
            	bf.iQuoteSent='1'
           AND 
                date_format( bf.`dtQuoteSend` , '%Y-%m-%d' ) = DATE_FORMAT( NOW( )  - INTERVAL $number DAY, '%Y-%m-%d' )     ";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
           $row=$this->getAssoc($result);
           $iTotalQuoteSent=$row['iTotalQuoteSent'];
        }
        $res[++$ctr]['iTtotal']=$iTotalQuoteSent;
        $res[$ctr]['szKey']="__TOTAL_QUOTE_SENT__";
        
        
        
        $query="
            SELECT	
                IFNULL(COUNT(cb.id),0) AS iSearches,
                l.szName,
                l.id AS idLanguage
            FROM
                ".__DBC_SCHEMATA_COMPARE_BOOKING__." AS cb
            INNER JOIN
                ".__DBC_SCHEMATA_CUSTOM_LANDING_PAGE__." AS clp
            ON
                cb.idLandingPage=clp.id
            INNER JOIN
                ".__DBC_SCHEMATA_LANGUAGE__." AS l
            ON
                clp.iLanguage=l.id    
            WHERE
                date_format(cb.dtDateTime, '%Y-%m-%d' ) = DATE_FORMAT( NOW( )  - INTERVAL $number DAY, '%Y-%m-%d' ) 
            $query_and 
            GROUP BY
                iLanguage
     	";
     	if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            while($row=$this->getAssoc($result))
            {

            //$iSearches=$row['iSearches'];
                $res[++$ctr]['iTtotal']=$row['iSearches'];
                $res[$ctr]['szKey']="__NUM_SEARCH_PER_LANGUAGE__";
                $res[$ctr]['idLanuage']=$row['idLanguage'];
            }
        
        }
        
//        $iLeadRatio=0;
//        if($iSearches>0){
//            $iLeadRatio=$iTotalCalls/$iSearches;
//        }
//        
//        $res[++$ctr]['iTtotal']=$iLeadRatio;
//        $res[$ctr]['szKey']="__NUM_LEAD_RATIO__";
        
         $query="
            SELECT 
                count( id )AS iTotalCheckOut
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE
            (
               date_format( `dtCreatedOn` , '%Y-%m-%d' ) = DATE_FORMAT( NOW( ) - INTERVAL $number DAY, '%Y-%m-%d'  )
            OR
               date_format( `dtBookingConfirmed` , '%Y-%m-%d' ) = DATE_FORMAT( NOW( ) - INTERVAL $number DAY, '%Y-%m-%d'  )
            )
            AND
                iBookingStep >='11'
            ";
            if( ( $result = $this->exeSQL( $query ) ) )
            {
               $row=$this->getAssoc($result);
               $iTotalCheckOut=$row['iTotalCheckOut'];
                
                $res[++$ctr]['iTtotal']=$iTotalCheckOut;
                $res[$ctr]['szKey']="__NUM_CHECK_OUT__";
            }
            
            if($number<=6)
            {
                $query="
                    SELECT COUNT(*) AS iTotalUserOrderMoreThanOnce
                        FROM
                        (
                            SELECT
                                id
                            FROM
                                ".__DBC_SCHEMATA_BOOKING__."
                            WHERE  
                                idBookingStatus IN(3,4)
                            AND 
                                iTransferConfirmed = '0'
                            AND
                                date_format( `dtBookingConfirmed` , '%Y-%m' ) <= DATE_FORMAT( NOW( ) - INTERVAL $number MONTH, '%Y-%m'  )
                            GROUP BY 
                                idUser
                            HAVING
                                COUNT(*) > 1
                        ) T1
                   ";
                if( ( $result = $this->exeSQL( $query ) ) )
                {
                   $row=$this->getAssoc($result);
                   $iTotalUserOrderMoreThanOnce=$row['iTotalUserOrderMoreThanOnce'];

                    $res[++$ctr]['iTtotal']=$iTotalUserOrderMoreThanOnce;
                    $res[$ctr]['szKey']="__TOTAL_USER_ORDER_MORE_THAN_ONCE__";
                }
                
                $query="
                    SELECT COUNT(*) AS iTotalUserOrderOnce
                        FROM
                        (
                            SELECT
                                id
                            FROM
                                ".__DBC_SCHEMATA_BOOKING__."
                            WHERE  
                                idBookingStatus IN(3,4)
                            AND 
                                iTransferConfirmed = '0'
                            AND
                                date_format( `dtBookingConfirmed` , '%Y-%m' ) <= DATE_FORMAT( NOW( ) - INTERVAL $number MONTH, '%Y-%m'  )
                            GROUP BY 
                                idUser
                            HAVING
                                COUNT(*) > 0
                        ) T1
                   ";
                if( ( $result = $this->exeSQL( $query ) ) )
                {
                   $row=$this->getAssoc($result);
                   $iTotalUserOrderOnce=$row['iTotalUserOrderOnce'];

                    $res[++$ctr]['iTtotal']=$iTotalUserOrderOnce;
                    $res[$ctr]['szKey']="__TOTAL_USER_ORDER_ONCE__";
                }
            }
        
        
//        $iCheckOutRatio=0;
//        if($iTotalCalls>0){
//            $iCheckOutRatio=$iTotalCheckOut/$iTotalCalls;
//        }
//        
//        $res[++$ctr]['iTtotal']=$iCheckOutRatio;
//        $res[$ctr]['szKey']="__NUM_CHECK_OUT_RATIO__";
        
        
//        $iBookingRatio=0;
//        if($iTotalCheckOut>0){
//            $iBookingRatio=$iTotalBookings/$iTotalCheckOut;
//        }
//        
//        $res[++$ctr]['iTtotal']=$iBookingRatio;
//        $res[$ctr]['szKey']="__NUM_BOOKING_RATIO__";
//   
        
        
        
        
        if(!empty($res))
        {
            foreach($res as $ress)
            {
                if($ress['idLanuage']==0)
                {
                    $ress['idLanuage']=1;
                }
                
                if($ress['szKey']=='__TOTAL_USER_ORDER_ONCE__' || $ress['szKey']=='__TOTAL_USER_ORDER_MORE_THAN_ONCE__')
                {
                    $prevMonthDate=date('Y-m-01',strtotime($szDate));
                    
                    
                     $query="
                            SELECT
                                DATE_FORMAT( NOW( )  - INTERVAL $number MONTH, '%Y-%m-%d'  ) AS szDate                
                        ";
                       echo $query."<br />";
                        if( ( $result = $this->exeSQL( $query ) ) )
                        {
                           $row=$this->getAssoc($result);

                           $szDate= $row['szDate'];
                        }

                        $prevMonth=date('M-Y',strtotime($szDate));
                        $prevMonthDate=date('Y-m-d',strtotime($szDate));
                }
                else
                {
                    $query="
                        SELECT
                            DATE_FORMAT( NOW( )  - INTERVAL $number DAY, '%Y-%m-%d'  ) AS szDate                
                    ";
                    echo $query."<br />";
                    if( ( $result = $this->exeSQL( $query ) ) )
                    {
                       $row=$this->getAssoc($result);

                       $szDate= $row['szDate'];
                    }

                    $prevMonth=date('M-Y',strtotime($szDate));
                    $prevMonthDate=date('Y-m-d',strtotime($szDate));
                }    
                $query="
                    SELECT
                        id 
                    FROM
                        ".__DBC_SCHEMATA_DATA_PINE__."
                    WHERE
                        szMonth='".mysql_escape_custom($prevMonth)."'
                    AND
                        szKey='".mysql_escape_custom($ress['szKey'])."'
                    AND        
                        DATE(dtMonth)='".mysql_escape_custom($prevMonthDate)."'
                    AND        
                        idLanuage='".mysql_escape_custom($ress['idLanuage'])."'         
                ";
                //echo $query."<br /><br />";
                if( ( $result = $this->exeSQL( $query ) ) )
                {
                    
                    
                    if( $this->getRowCnt() <= 0 )
                    {
                        $query="
                            INSERT INTO
                                ".__DBC_SCHEMATA_DATA_PINE__."
                            (

                                szKey,
                                iTotal,
                                szMonth,
                                iType,
                                dtMonth,
                                idLanuage
                            )
                                VALUES
                            (
                                '".mysql_escape_custom($ress['szKey'])."',
                                '".round((float)$ress['iTtotal'],2)."',
                                '".mysql_escape_custom($prevMonth)."',
                                '1',
                                '".mysql_escape_custom($prevMonthDate)."',
                                '".mysql_escape_custom($ress['idLanuage'])."'    
                            )
                        ";
                    }
                    else
                    { 
                
                        $query="
                            UPDATE
                                ".__DBC_SCHEMATA_DATA_PINE__."
                            SET        
                                iTotal='".round((float)$ress['iTtotal'],2)."'
                            WHERE
                                szMonth='".mysql_escape_custom($prevMonth)."'
                            AND
                                szKey='".mysql_escape_custom($ress['szKey'])."'
                            AND        
                                dtMonth='".mysql_escape_custom($prevMonthDate)."'
                            AND        
                                idLanuage='".mysql_escape_custom($ress['idLanuage'])."'        
                        ";
                    }
                    $result = $this->exeSQL( $query );
                }
                
            }
        }
        
    }
    
    
    function insertDataPineFinacialData()
    {
        $ctr=0;
        $query="
            SELECT	
                fTotalPriceUSD AS fTurnover,
                fReferalAmountUSD AS fTotalReferalAmountUSD,
                (fPriceMarkupCustomerCurrency * fCustomerExchangeRate) AS fPriceMarkupUSD,
                iPaymentType,
                id
            FROM
                ".__DBC_SCHEMATA_BOOKING__." as b1
            WHERE
                ( 
                    date_format(dtBookingConfirmed,'%Y-%m-%d') = DATE_FORMAT( NOW( ), '%Y-%m-%d'  )
                AND 
                    idBookingStatus IN(3,4) 
                AND
                    iTransferConfirmed = '0'
                )
        ";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                if(!empty($resArr[$row['id']]))
                {
                    $resArr[$row['id']]=  array_merge($resArr[$row['id']],$row);
                }
                else
                {
                    $resArr[$row['id']]=$row;
                }  
            }
        }
        $ctr=0;
        $query="
            SELECT	
                (cbd.fBookingLabelFeeRate * cbd.fBookingLabelFeeROE ) AS fTotalLabelFeeAmountUSD,
                bt.id		
            FROM
                ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__." AS cbd
            INNER JOIN
                ".__DBC_SCHEMATA_BOOKING__." as bt
            ON
                bt.id=cbd.idBooking
            WHERE
                date_format(dtBookingConfirmed,'%Y-%m-%d') = DATE_FORMAT( NOW( ), '%Y-%m-%d'  ) 
            AND 
                bt.idBookingStatus IN(3,4) 
            AND
                bt.iTransferConfirmed = '0'
            AND
                cbd.iCourierAgreementIncluded = '0'    
            AND
            (
                bt.iCourierBooking='1'
            OR
                bt.isManualCourierBooking='1'
            )    
        ";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                if(!empty($resArr[$row['id']]))
                {
                    $resArr[$row['id']]=  array_merge($resArr[$row['id']],$row);
                }
                else
                {
                    $resArr[$row['id']]=$row;
                }  
            }
        }
        $ctr=0;
        $query="
            SELECT	
                fTotalHandlingFeeUSD AS fTotalBookingHandlingFeeUSD,
                id		
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE
                 date_format(dtBookingConfirmed,'%Y-%m-%d') = DATE_FORMAT( NOW( ), '%Y-%m-%d'  )  
            AND 
                idBookingStatus IN(3,4)
            AND
                iHandlingFeeApplicable='1'
            AND
                iTransferConfirmed = '0'
        ";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                if(!empty($resArr[$row['id']]))
                {
                    $resArr[$row['id']]=  array_merge($resArr[$row['id']],$row);
                }
                else
                {
                    $resArr[$row['id']]=$row;
                }  
            }
        }
        $ctr=0;
        $query="
            SELECT	
                fTotalInsuranceCostForBookingUSD AS fTotalInsuranceSellPriceUSD,
                id		
            FROM
                    ".__DBC_SCHEMATA_BOOKING__." as b2 
            WHERE            
                date_format(dtBookingConfirmed,'%Y-%m-%d') = DATE_FORMAT( NOW( ), '%Y-%m-%d'  ) 
            AND 
                idBookingStatus IN(3,4) 
            AND
                iInsuranceIncluded = '1'
            AND
                iInsuranceStatus !='3'
             AND
                iTransferConfirmed = '0'            
        ";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                if(!empty($resArr[$row['id']]))
                {
                    $resArr[$row['id']]=  array_merge($resArr[$row['id']],$row);
                }
                else
                {
                    $resArr[$row['id']]=$row;
                }  
            }
        }
        //echo $query."<br /><br />";
        $ctr=0;
        $query="
            SELECT	
                fTotalInsuranceCostForBookingUSD_buyRate AS fTotalInsuranceBuyPriceUSD,
                id		
            FROM
                ".__DBC_SCHEMATA_BOOKING__." as b3
            WHERE
                date_format(dtBookingConfirmed,'%Y-%m-%d') = DATE_FORMAT( NOW( ), '%Y-%m-%d'  )
            AND 
                idBookingStatus IN(3,4) 
            AND
                iInsuranceIncluded = '1'
            AND
                iInsuranceStatus !='3'
            AND
                iTransferConfirmed = '0'
        ";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                if(!empty($resArr[$row['id']]))
                {
                    $resArr[$row['id']]=  array_merge($resArr[$row['id']],$row);
                }
                else
                {
                    $resArr[$row['id']]=$row;
                }  
            }
        }
       // echo $query."<br /><br />";
        $ctr=0;
        $query="
            SELECT	
                fTotalInsuranceCostForBookingUSD AS fInsuranceTurnOver,
                id		
            FROM
                ".__DBC_SCHEMATA_BOOKING__." as b5
            WHERE
                date_format(dtBookingConfirmed,'%Y-%m-%d') =  DATE_FORMAT( NOW( ), '%Y-%m-%d'  )
            AND 
                idBookingStatus IN(3,4) 
            AND
                iInsuranceIncluded = '1'
            AND
                iInsuranceStatus !='3'
            AND
                iTransferConfirmed = '0'
            ";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                if(!empty($resArr[$row['id']]))
                {
                    $resArr[$row['id']]=  array_merge($resArr[$row['id']],$row);
                }
                else
                {
                    $resArr[$row['id']]=$row;
                }  
            }
        }
        
        if(!empty($resArr))
        {
            
            foreach($resArr as $resArrs)
            {
                $query="
                    SELECT
                        id
                    FROM
                        ".__DBC_SCHEMATA_DATA_PINE_FINACIAL_DATA__."
                    WHERE
                        idBooking='".(int)$resArrs['id']."'
                ";
                if( ( $result = $this->exeSQL( $query ) ) )
                {
                    if( $this->getRowCnt() <= 0 )
                    {
                        $totalGP=($resArrs['fTotalInsuranceSellPriceUSD'] - $resArrs['fTotalInsuranceBuyPriceUSD']) + $resArrs['fTotalReferalAmountUSD'] + $resArrs['fTotalLabelFeeAmountUSD'] + $resArrs['fTotalBookingHandlingFeeUSD'] + $resArrs['fPriceMarkupUSD'];
                        $serviceChargeStripe=0.00;
                        if(strtolower($resArrs['iPaymentType'])=='stripe')
                        {
                            $serviceChargeStripe=$totalGP*.01*__SERVICE_CHARGE_STRIPE__;
                        }

                        $idGRange=$this->getGPRangeId($totalGP);
                        //echo $serviceChargeStripe."serviceChargeStripe";
                        $query1="
                            INSERT INTO
                                ".__DBC_SCHEMATA_DATA_PINE_FINACIAL_DATA__."
                            (
                                idBooking,
                                fTotalInsuranceSellPriceUSD,
                                fTotalInsuranceBuyPriceUSD,
                                fTotalReferalAmountUSD,
                                fTotalLabelFeeAmountUSD,
                                fTotalBookingHandlingFeeUSD,
                                fPriceMarkupUSD,
                                fServiceChargeStripe,
                                fTurnOver,
                                fInsuranceTurnOver,
                                dtCreated,
                                idGPRange
                            )
                                VALUES
                            (
                                '".(int)$resArrs['id']."',
                                '".(float)$resArrs['fTotalInsuranceSellPriceUSD']."',
                                '".(float)$resArrs['fTotalInsuranceBuyPriceUSD']."',
                                '".(float)$resArrs['fTotalReferalAmountUSD']."',
                                '".(float)$resArrs['fTotalLabelFeeAmountUSD']."',
                                '".(float)$resArrs['fTotalBookingHandlingFeeUSD']."',
                                '".(float)$resArrs['fPriceMarkupUSD']."',
                                '".(float)$serviceChargeStripe."',
                                '".(float)$resArrs['fTurnover']."',
                                '".(float)$resArrs['fInsuranceTurnOver']."',
                                NOW(),
                                '".(int)$idGRange."'
                            )
                        ";
                        $result1 = $this->exeSQL( $query1 );
                    }
                }
            }
        }
    }
    
    function insertDataPineFinacialPrevData($number)
    {
        $ctr=0;
        $query="
            SELECT	
                id,
                fTotalPriceUSD AS fTurnover,
                fReferalAmountUSD AS fTotalReferalAmountUSD,
                (fPriceMarkupCustomerCurrency * fCustomerExchangeRate) AS fPriceMarkupUSD,
                iPaymentType,
                dtBookingConfirmed
            FROM
                ".__DBC_SCHEMATA_BOOKING__." as b1
            WHERE
                ( 
                    date_format(dtBookingConfirmed,'%Y-%m-%d') = DATE_FORMAT( NOW( ) - INTERVAL $number DAY, '%Y-%m-%d'  )
                AND 
                    idBookingStatus IN(3,4) 
                AND
                    iTransferConfirmed = '0'
                )
        ";
        //echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                $resArr[$row['id']]=$row;
                ++$ctr;
            }
        }
        //print_r($resArr);
        $ctr=0;
        $query="
            SELECT	
                (cbd.fBookingLabelFeeRate * cbd.fBookingLabelFeeROE ) AS fTotalLabelFeeAmountUSD,
                bt.id
            FROM
                ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__." AS cbd
            INNER JOIN
                ".__DBC_SCHEMATA_BOOKING__." as bt
            ON
                bt.id=cbd.idBooking
            WHERE
                date_format(dtBookingConfirmed,'%Y-%m-%d') = DATE_FORMAT( NOW( ) - INTERVAL $number DAY, '%Y-%m-%d'  ) 
            AND 
                bt.idBookingStatus IN(3,4) 
            AND
                bt.iTransferConfirmed = '0'
            AND
                cbd.iCourierAgreementIncluded = '0'    
            AND
            (
                bt.iCourierBooking='1'
            OR
                bt.isManualCourierBooking='1'
            )    
        ";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                if(!empty($resArr[$row['id']]))
                {
                    $resArr[$row['id']]=  array_merge($resArr[$row['id']],$row);
                }
                else
                {
                    $resArr[$row['id']]=$row;
                }                
                ++$ctr;            
                
            }
        }
        $ctr=0;
        $query="
            SELECT	
                fTotalHandlingFeeUSD AS fTotalBookingHandlingFeeUSD,
                id
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE
                 date_format(dtBookingConfirmed,'%Y-%m-%d') = DATE_FORMAT( NOW( ) - INTERVAL $number DAY, '%Y-%m-%d'  )  
            AND 
                idBookingStatus IN(3,4)
            AND
                iHandlingFeeApplicable='1'
            AND
                iTransferConfirmed = '0'
        ";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                if(!empty($resArr[$row['id']]))
                {
                    $resArr[$row['id']]=  array_merge($resArr[$row['id']],$row);
                }
                else
                {
                    $resArr[$row['id']]=$row;
                }  
                ++$ctr;
            }
        }
        $ctr=0;
        $query="
            SELECT	
                fTotalInsuranceCostForBookingUSD AS fTotalInsuranceSellPriceUSD,
                id
            FROM
                    ".__DBC_SCHEMATA_BOOKING__." as b2 
            WHERE
            ( 
                date_format(dtBookingConfirmed,'%Y-%m-%d') = DATE_FORMAT( NOW( ) - INTERVAL $number DAY, '%Y-%m-%d'  ) 
            AND 
                idBookingStatus IN(3,4) 
            AND
                iInsuranceIncluded = '1'
            AND
                iInsuranceStatus !='3'
             AND
                iTransferConfirmed = '0'
            )
        ";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                if(!empty($resArr[$row['id']]))
                {
                    $resArr[$row['id']]=  array_merge($resArr[$row['id']],$row);
                }
                else
                {
                    $resArr[$row['id']]=$row;
                }  
                ++$ctr;
            }
        }
        $ctr=0;
        $query="
            SELECT	
                fTotalInsuranceCostForBookingUSD_buyRate AS fTotalInsuranceBuyPriceUSD,
                id		
            FROM
                ".__DBC_SCHEMATA_BOOKING__." as b3
            WHERE
                date_format(dtBookingConfirmed,'%Y-%m-%d') = DATE_FORMAT( NOW( ) - INTERVAL $number DAY, '%Y-%m-%d'  )
            AND 
                idBookingStatus IN(3,4) 
            AND
                iInsuranceIncluded = '1'
            AND
                iInsuranceStatus !='3'
            AND
                iTransferConfirmed = '0'
        ";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                if(!empty($resArr[$row['id']]))
                {
                    $resArr[$row['id']]=  array_merge($resArr[$row['id']],$row);
                }
                else
                {
                    $resArr[$row['id']]=$row;
                }  
                ++$ctr;
            }
        }
        
        $ctr=0;
        $query="
            SELECT	
                fTotalInsuranceCostForBookingUSD AS fInsuranceTurnOver,
                id
            FROM
                ".__DBC_SCHEMATA_BOOKING__." as b5
            WHERE
            (  
                date_format(dtBookingConfirmed,'%Y-%m-%d') =  DATE_FORMAT( NOW( ) - INTERVAL $number DAY, '%Y-%m-%d'  )
            AND 
                idBookingStatus IN(3,4) 
            AND
                iInsuranceIncluded = '1'
            AND
                iInsuranceStatus !='3'
            AND
                iTransferConfirmed = '0'
            )";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                if(!empty($resArr[$row['id']]))
                {
                    $resArr[$row['id']]=  array_merge($resArr[$row['id']],$row);
                }
                else
                {
                    $resArr[$row['id']]=$row;
                }  
                ++$ctr;
            }
        }
        
        if(!empty($resArr))
        {
            
            foreach($resArr as $resArrs)
            {
                $query="
                    SELECT
                        id
                    FROM
                        ".__DBC_SCHEMATA_DATA_PINE_FINACIAL_DATA__."
                    WHERE
                        idBooking='".(int)$resArrs['id']."'
                ";
                if( ( $result = $this->exeSQL( $query ) ) )
                {
                    if( $this->getRowCnt() <= 0 )
                    {
                        $totalGP=($resArrs['fTotalInsuranceSellPriceUSD'] - $resArrs['fTotalInsuranceBuyPriceUSD']) + $resArrs['fTotalReferalAmountUSD'] + $resArrs['fTotalLabelFeeAmountUSD'] + $resArrs['fTotalBookingHandlingFeeUSD'] + $resArrs['fPriceMarkupUSD'];
                        $serviceChargeStripe=0.00;
                        if(strtolower($resArrs['iPaymentType'])=='stripe')
                        {
                            $serviceChargeStripe=$totalGP*.01*__SERVICE_CHARGE_STRIPE__;
                        }

                        $dtBookingConfirmed=date("Y-m-d",  strtotime($resArrs['dtBookingConfirmed']));
                        
                       
                        //echo $totalGP."totalGP<br />";
                        $idGRange=$this->getGPRangeId($totalGP);
                        
                        //echo $serviceChargeStripe."serviceChargeStripe";
                        $query1="
                            INSERT INTO
                                ".__DBC_SCHEMATA_DATA_PINE_FINACIAL_DATA__."
                            (
                                idBooking,
                                fTotalInsuranceSellPriceUSD,
                                fTotalInsuranceBuyPriceUSD,
                                fTotalReferalAmountUSD,
                                fTotalLabelFeeAmountUSD,
                                fTotalBookingHandlingFeeUSD,
                                fPriceMarkupUSD,
                                fServiceChargeStripe,
                                fTurnOver,
                                fInsuranceTurnOver,
                                dtCreated,
                                idGPRange                       
                            )
                                VALUES
                            (
                                '".(int)$resArrs['id']."',
                                '".(float)$resArrs['fTotalInsuranceSellPriceUSD']."',
                                '".(float)$resArrs['fTotalInsuranceBuyPriceUSD']."',
                                '".(float)$resArrs['fTotalReferalAmountUSD']."',
                                '".(float)$resArrs['fTotalLabelFeeAmountUSD']."',
                                '".(float)$resArrs['fTotalBookingHandlingFeeUSD']."',
                                '".(float)$resArrs['fPriceMarkupUSD']."',
                                '".(float)$serviceChargeStripe."',
                                '".(float)$resArrs['fTurnover']."',
                                '".(float)$resArrs['fInsuranceTurnOver']."',
                                '".mysql_escape_custom($dtBookingConfirmed)."',
                                '".(int)$idGRange."'    
                            )
                        ";
                        $result1 = $this->exeSQL( $query1 );
                    }
                }
            }
        }
    }
    
    
    
    
    
    function insertDataPineFinacialOldData()
    {
        $ctr=0;
        $query="
            SELECT	
                id,
                fTotalPriceUSD AS fTurnover,
                fReferalAmountUSD AS fTotalReferalAmountUSD,
                (fPriceMarkupCustomerCurrency * fCustomerExchangeRate) AS fPriceMarkupUSD,
                iPaymentType,
                dtBookingConfirmed
            FROM
                ".__DBC_SCHEMATA_BOOKING__." as b1
            WHERE
                ( 
                    `dtBookingConfirmed` != '0000-00-00 00:00:00'
                    AND DATE( `dtBookingConfirmed` ) <= '2016-07-24'
                AND 
                    idBookingStatus IN(3,4) 
                AND
                    iTransferConfirmed = '0'
                )
        ";
        //echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                $resArr[$row['id']]=$row;
                ++$ctr;
            }
        }
        //print_r($resArr);
        $ctr=0;
        $query="
            SELECT	
                (cbd.fBookingLabelFeeRate * cbd.fBookingLabelFeeROE ) AS fTotalLabelFeeAmountUSD,
                bt.id
            FROM
                ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__." AS cbd
            INNER JOIN
                ".__DBC_SCHEMATA_BOOKING__." as bt
            ON
                bt.id=cbd.idBooking
            WHERE
                `dtBookingConfirmed` != '0000-00-00 00:00:00'
AND DATE( `dtBookingConfirmed` ) <= ''2016-07-24' 
            AND 
                bt.idBookingStatus IN(3,4) 
            AND
                bt.iTransferConfirmed = '0'
            AND
                cbd.iCourierAgreementIncluded = '0'    
        AND
        (
            bt.iCourierBooking='1'
        OR
            bt.isManualCourierBooking='1'
        )        
        ";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                if(!empty($resArr[$row['id']]))
                {
                    $resArr[$row['id']]=  array_merge($resArr[$row['id']],$row);
                }
                else
                {
                    $resArr[$row['id']]=$row;
                }                
                ++$ctr;            
                
            }
        }
        $ctr=0;
        $query="
            SELECT	
                fTotalHandlingFeeUSD AS fTotalBookingHandlingFeeUSD,
                id
            FROM
                ".__DBC_SCHEMATA_BOOKING__."
            WHERE
                 `dtBookingConfirmed` != '0000-00-00 00:00:00'
AND DATE( `dtBookingConfirmed` ) <= ''2016-07-24'  
            AND 
                idBookingStatus IN(3,4)
            AND
                iHandlingFeeApplicable='1'
            AND
                iTransferConfirmed = '0'
        ";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                if(!empty($resArr[$row['id']]))
                {
                    $resArr[$row['id']]=  array_merge($resArr[$row['id']],$row);
                }
                else
                {
                    $resArr[$row['id']]=$row;
                }  
                ++$ctr;
            }
        }
        $ctr=0;
        $query="
            SELECT	
                fTotalInsuranceCostForBookingUSD AS fTotalInsuranceSellPriceUSD,
                id
            FROM
                    ".__DBC_SCHEMATA_BOOKING__." as b2 
            WHERE
            ( 
                `dtBookingConfirmed` != '0000-00-00 00:00:00'
AND DATE( `dtBookingConfirmed` ) <= ''2016-07-24'
            AND 
                idBookingStatus IN(3,4) 
            AND
                iInsuranceIncluded = '1'
            AND
                iInsuranceStatus !='3'
             AND
                iTransferConfirmed = '0'
            )
        ";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                if(!empty($resArr[$row['id']]))
                {
                    $resArr[$row['id']]=  array_merge($resArr[$row['id']],$row);
                }
                else
                {
                    $resArr[$row['id']]=$row;
                }  
                ++$ctr;
            }
        }
        $ctr=0;
        $query="
            SELECT	
                fTotalInsuranceCostForBookingUSD_buyRate AS fTotalInsuranceBuyPriceUSD,
                id		
            FROM
                ".__DBC_SCHEMATA_BOOKING__." as b3
            WHERE
                `dtBookingConfirmed` != '0000-00-00 00:00:00'
AND DATE( `dtBookingConfirmed` ) <= ''2016-07-24'
            AND 
                idBookingStatus IN(3,4) 
            AND
                iInsuranceIncluded = '1'
            AND
                iInsuranceStatus !='3'
            AND
                iTransferConfirmed = '0'
        ";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                if(!empty($resArr[$row['id']]))
                {
                    $resArr[$row['id']]=  array_merge($resArr[$row['id']],$row);
                }
                else
                {
                    $resArr[$row['id']]=$row;
                }  
                ++$ctr;
            }
        }
        
        $ctr=0;
        $query="
            SELECT	
                fTotalInsuranceCostForBookingUSD AS fInsuranceTurnOver,
                id
            FROM
                ".__DBC_SCHEMATA_BOOKING__." as b5
            WHERE
            (  
                `dtBookingConfirmed` != '0000-00-00 00:00:00'
AND DATE( `dtBookingConfirmed` ) <= ''2016-07-24'
            AND 
                idBookingStatus IN(3,4) 
            AND
                iInsuranceIncluded = '1'
            AND
                iInsuranceStatus !='3'
            AND
                iTransferConfirmed = '0'
            )";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                if(!empty($resArr[$row['id']]))
                {
                    $resArr[$row['id']]=  array_merge($resArr[$row['id']],$row);
                }
                else
                {
                    $resArr[$row['id']]=$row;
                }  
                ++$ctr;
            }
        }
        
        if(!empty($resArr))
        {
            
            foreach($resArr as $resArrs)
            {
                $query="
                    SELECT
                        id
                    FROM
                        ".__DBC_SCHEMATA_DATA_PINE_FINACIAL_DATA__."
                    WHERE
                        idBooking='".(int)$resArrs['id']."'
                ";
                
                if( ( $result = $this->exeSQL( $query ) ) )
                {
                    if( $this->getRowCnt() <= 0 )
                    {//echo $query."<br />";
                        $totalGP=($resArrs['fTotalInsuranceSellPriceUSD'] - $resArrs['fTotalInsuranceBuyPriceUSD']) + $resArrs['fTotalReferalAmountUSD'] + $resArrs['fTotalLabelFeeAmountUSD'] + $resArrs['fTotalBookingHandlingFeeUSD'] + $resArrs['fPriceMarkupUSD'];
                        $serviceChargeStripe=0.00;
                        if(strtolower($resArrs['iPaymentType'])=='stripe')
                        {
                            $serviceChargeStripe=$totalGP*.01*__SERVICE_CHARGE_STRIPE__;
                        }
                        
                        
                        //echo $totalGP."totalGP<br />";
                        $idGRange=$this->getGPRangeId($totalGP);
                        $dtBookingConfirmed=date("Y-m-d",  strtotime($resArrs['dtBookingConfirmed']));
                        //echo $serviceChargeStripe."serviceChargeStripe";
                        $query1="
                            INSERT INTO
                                ".__DBC_SCHEMATA_DATA_PINE_FINACIAL_DATA__."
                            (
                                idBooking,
                                fTotalInsuranceSellPriceUSD,
                                fTotalInsuranceBuyPriceUSD,
                                fTotalReferalAmountUSD,
                                fTotalLabelFeeAmountUSD,
                                fTotalBookingHandlingFeeUSD,
                                fPriceMarkupUSD,
                                fServiceChargeStripe,
                                fTurnOver,
                                fInsuranceTurnOver,
                                dtCreated,
                                idGPRange
                            )
                                VALUES
                            (
                                '".(int)$resArrs['id']."',
                                '".(float)$resArrs['fTotalInsuranceSellPriceUSD']."',
                                '".(float)$resArrs['fTotalInsuranceBuyPriceUSD']."',
                                '".(float)$resArrs['fTotalReferalAmountUSD']."',
                                '".(float)$resArrs['fTotalLabelFeeAmountUSD']."',
                                '".(float)$resArrs['fTotalBookingHandlingFeeUSD']."',
                                '".(float)$resArrs['fPriceMarkupUSD']."',
                                '".(float)$serviceChargeStripe."',
                                '".(float)$resArrs['fTurnover']."',
                                '".(float)$resArrs['fInsuranceTurnOver']."',
                                '".mysql_escape_custom($dtBookingConfirmed)."',
                                '".(int)$idGRange."'
                            )
                        ";
                        //echo $query1."<br />";
                        //die();
                        $result1 = $this->exeSQL( $query1 );
                    }
                }
            }
        }
    }
    
    
    function getGPRangeId($fValue)
    {
        $query="
            SELECT
                id,
                fMinValue,
                fMaxValue
            FROM
                ".__DBC_SCHEMATA_GP_RANGE_DATA__."
            ";
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                while($row=$this->getAssoc($result))
                {
                    //echo $fValue."---".$row['fMaxValue'];
                   if($row['fMinValue']==0 && $row['fMaxValue']>$fValue)
                   {
                       return $row['id'];
                   }
                   else if($row['fMinValue']!=0 && $row['fMaxValue']!=0 && $row['fMinValue']<=$fValue && $row['fMaxValue']>$fValue)
                   {
                       return $row['id'];
                   }
                   else if($row['fMinValue']!=0 && $row['fMaxValue']==0 && $row['fMinValue']<=$fValue)
                   {
                       return $row['id'];
                   }
                }
            }
    }
    
    function updateQuoteSendPrevData()
    {
        $query="
            SELECT 
                b.id AS idBooking
            FROM
                ".__DBC_SCHEMATA_BOOKING__." AS b
            INNER JOIN
              tblbookingfiles AS bf
            ON
              bf.idBooking=b.id    
            WHERE
                b.iBookingType IN ('2')";
        echo $query."<br /><br />";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                
                $idBooking=$row['idBooking'];
                $query1="
                    SELECT
                        dtCreatedOn,
                        dtUpdatedOn
                    FROM
                        ".__DBC_SCHEMATA_QUOTE_PRICING_DETAILS__."
                    WHERE
                        idBooking='".(int)$idBooking."'
                    LIMIT
                        0,1
                ";
                
                if( ( $result1 = $this->exeSQL( $query1 ) ) )
                {
                    if( $this->getRowCnt() > 0 )
                    {
                        $row1=$this->getAssoc($result1);
                        $dtQuoteSend='';
                        if($row1['dtUpdatedOn']!='0000-00-00 00:00:00')
                        {
                            $dtQuoteSend=$row1['dtUpdatedOn'];
                        }
                        else
                        {
                            $dtQuoteSend=$row1['dtCreatedOn'];
                        } 
                        if($dtQuoteSend!='')
                        {
                            $query2="
                                UPDATE
                                    ".__DBC_SCHEMATA_FILE_LOGS__."
                                SET
                                    dtQuoteSend='".  mysql_escape_custom($dtQuoteSend)."'
                                WHERE
                                    idBooking='".(int)$idBooking."'        
                            ";
                            $result2 = $this->exeSQL( $query2 );
                            echo $query1."<br /><br />";
                            echo $query2."<br /><br />";
                        }
                        
                    }
                }
            }
        }
    }
    
    
    function  getSearchDataForDenmark()
    {
        $transportecaUserAry=$this->getAllTransportecaUsers();


        if(!empty($transportecaUserAry))
        {
            $user_str = implode(",",$transportecaUserAry);
            $query_and = " AND idUser NOT IN (".$user_str.") "; 
        }
        
        $query="
            SELECT	
                szBrowserDetails		
            FROM
                ".__DBC_SCHEMATA_COMPARE_BOOKING__."
            WHERE
                date_format(dtDateTime, '%Y-%m' ) = '2016-07' 
            $query_and 
            GROUP BY
                szBrowserDetails
     	";
        //echo "<br><br> ".$query."<br>";
     	if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            while($row=$this->getAssoc($result))
            {
                $res_arr[]=$row['szBrowserDetails'];
            }
            return $res_arr;
        }
    }
    
    function  getBussinessUserLists()
    { 
        $query="
            SELECT	
                id,
                szFirstName,
                szLastName,
                szEmail,
                (SELECT count(id) as iBookingCount FROM ".__DBC_SCHEMATA_BOOKING__." as b WHERE idBookingStatus IN(3,4) AND b.idUser = u.id AND DATE(dtBookingConfirmed)<='2016-10-15') as iBookingCount
            FROM
                ".__DBC_SCHEMATA_USERS__." as u
            WHERE
                iPrivate = '0'
            AND
                iActive = '1'
            HAVING
                iBookingCount>0
            ORDER BY
                szFirstName ASC, szLastName ASC, szEmail ASC
     	";
        //echo "<br><br> ".$query."<br>";
     	if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            $ctr = 0;
            while($row=$this->getAssoc($result))
            {
                $res_arr[$ctr] = $row;
                $ctr++;
            }
            return $res_arr;
        }
    }
    
    function downloadBussinessCusomerList()
    { 
        $kBooking = new cBooking(); 
        require_once( __APP_PATH_CLASSES__ . "/PHPExcel.php" );

        $objPHPExcel=new PHPExcel(); 

        $sheetIndex=0;
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex($sheetIndex);
        $sheet = $objPHPExcel->getActiveSheet(); 

        $styleArray = array(
                'font' => array(
                        'bold' => false,
                        'size' =>12,
                ),
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                ),
                'borders' => array(
                        'top' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'bottom' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'right' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'left' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                ),
        );

        $styleArray1 = array(
                'font' => array(
                        'bold' => false,
                        'size' =>12,
                ),
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                ),
                'borders' => array(
                        'top' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'bottom' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'right' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'left' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                ),
        ); 

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30); 

        $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->getStartColor()->setARGB('FFddd9c3');
        //$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);

        $objPHPExcel->getActiveSheet()->setCellValue('A1','Email')->getStyle('A1');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray); 

        $objPHPExcel->getActiveSheet()->setCellValue('B1','First Name')->getStyle('B1');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->setCellValue('C1','Last name')->getStyle('C1');
        $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
 
        $col = 2; 
        
        $bussinessCustomerAry = $this->getBussinessUserLists();
          
        if(!empty($bussinessCustomerAry))
        {
            foreach($bussinessCustomerAry as $bussinessCustomerArys)
            {     
                $objPHPExcel->getActiveSheet()->setCellValue("A".$col,$bussinessCustomerArys['szEmail']);
                $objPHPExcel->getActiveSheet()->setCellValue("B".$col,$bussinessCustomerArys['szFirstName']);
                $objPHPExcel->getActiveSheet()->setCellValue("C".$col,$bussinessCustomerArys['szLastName']); 
                $col++; 
            } 
        } 

        $title = "_Transporteca_bussiness_customer";
        $objPHPExcel->getActiveSheet()->setTitle('Transporteca Bussiness Customer');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->removeSheetByIndex(1);
        $file=date('Ymdhs').$title;
        $fileName=__APP_PATH_ROOT__."/insuranceSheets/".$file.".xlsx"; 
        $objWriter->save($fileName); 
        return $fileName; 
    }
    
    
    function getReadMessageResponseTime($MYStr='',$flag=false)
    {
        if($MYStr=='')
        {
            $MYStr = "date_format(dtReadMessageTaskToRead,'%Y-%m') = date_format(now() , '%Y-%m' )";
        }
        
        $orderBy='';
        if($flag)
        {
            $innerJoin="
                INNER JOIN
                    ".__DBC_SCHEMATA_FILE_LOGS__." AS bf
                ON
                    bf.idBooking=el.idBooking
                INNER JOIN
                    ".__DBC_SCHEMATA_MANAGEMENT__." AS m
                ON
                    m.id=el.idAdminAddedBy    
                    ";
            $column=",bf.szFileRef,m.szFirstName,m.szLastName";
            
            $MYStr = "date_format(el.dtReadMessageTaskToRead,'%Y-%m') >= date_format(now() - INTERVAL 5 MONTH , '%Y-%m' )";
            
            $orderBy="ORDER BY el.dtReadMessageTaskToRead DESC";
        }
        
        $transportecaOfficeHourArr=$this->getTransportecaOfficeHour();
        $query="
            SELECT	
                el.iTotalClearTime as iReadMessageResponseTime,
                el.idPrimary as iNumReadMessageEmail,
                el.dtSent,
                el.dtReadMessageTaskToRead,
                el.iEmailNumber,
                el.szEmailSubject,
                el.szToData AS szToAddress,
                el.iDSTDifference,
                el.iAlreadyUtf8Encoded
                $column
            FROM
                ".__DBC_SCHEMATA_CUSTOMER_LOG_SNIPPET__." as el
            $innerJoin        
            WHERE 
                el.dtReadMessageTaskToRead <> '0000-00-00 00:00:00'
            AND 
                $MYStr
            AND
                el.iEmailNumber >0
            
            $orderBy
            ";
        //echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            $ctr = 0;
            while($row=$this->getAssoc($result))
            {
                if($flag)
                {
                    $resArr[$ctr]=$row;
                }else{
                $iReadMessageResponseTime +=$row['iReadMessageResponseTime'];

                }
               // echo $iReadMessageResponseTime."iReadMessageResponseTime";
                ++$ctr;
                    
                
                
            }
            if($flag)
            {
                return $resArr;
            }else{
            $iReadMessageResponseTime= ($iReadMessageResponseTime/$ctr);
            return round((float)$iReadMessageResponseTime);
            }
        }
    }
    
    
    function getCallUserResponseTime($MYStr='',$flag=false)
    {
        if($MYStr=='')
        {
            $MYStr = "date_format(track1.dtCreatedOn,'%Y-%m') = date_format(now() , '%Y-%m' )";
        }
        $orderBy='';
        if($flag)
        {
            $innerJoin="
                INNER JOIN
                    ".__DBC_SCHEMATA_FILE_LOGS__." AS bf
                ON
                    bf.id=track1.idFile
                INNER JOIN
                    ".__DBC_SCHEMATA_MANAGEMENT__." AS m
                ON
                    m.id=track1.idAdminAddedBy    
                    ";
            $column=",bf.szFileRef,m.szFirstName,m.szLastName";
            
            $MYStr = "date_format(track1.dtCreatedOn,'%Y-%m') >= date_format(now() - INTERVAL 5 MONTH , '%Y-%m' )";
            
            $orderBy="ORDER BY track1.dtResponseTime DESC";
        }
        $query="
            SELECT	
                iTotalClearTime as iAutomaticSearchResponseTime,
                track1.id as iNumAutomaticSearches,
                track1.dtFileCreated,
                track1.dtResponseTime,
                TIMESTAMPDIFF(Second,dtFileCreated,dtResponseTime) as iTotalTimeResponse
                $column
            FROM
                ".__DBC_SCHEMATA_RESPONSE_TIME_TRACKER__." as track1
            $innerJoin
            WHERE 
                $MYStr 
            AND
                track1.isDeleted = '0' 
            AND
                track1.iSearchType = '1'   
            $orderBy
            ";
        //echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            $ctr = 0;
            while($row=$this->getAssoc($result))
            {                
                if($flag)
                {
                    $resArr[$ctr]=$row;
                }else{
                $iAutomaticSearchResponseTime +=$row['iAutomaticSearchResponseTime'];
                }
                ++$ctr;        
            }
            if($flag)
            {
                return $resArr;
            }else{
            $iAutomaticSearchResponseTime= ($iAutomaticSearchResponseTime/$ctr);
            return round($iAutomaticSearchResponseTime);
            }
        }
    }
    
    
    function downloadReadMessageTaskSheet($readMessageArr)
    {  
        
        require_once( __APP_PATH_CLASSES__ . "/PHPExcel.php" );

        $objPHPExcel=new PHPExcel(); 

        $sheetIndex=0;
        $objPHPExcel = new PHPExcel();
        
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex($sheetIndex);
        $sheet = $objPHPExcel->getActiveSheet(); 

        $styleArray = array(
                'font' => array(
                        'bold' => false,
                        'size' =>12,
                ),
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                ),
                'borders' => array(
                        'top' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'bottom' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'right' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'left' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                ),
        );

        $styleArray1 = array(
                'font' => array(
                        'bold' => false,
                        'size' =>12,
                ),
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                ),
                'borders' => array(
                        'top' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'bottom' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'right' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'left' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                ),
        ); 
        $styleArray2 = array(
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                ),
                
        );

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
        

        $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFill()->getStartColor()->setARGB('FFddd9c3');
         
        $objPHPExcel->getActiveSheet()->setCellValue('A1','Email Log Id')->getStyle('A1');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray); 

        $objPHPExcel->getActiveSheet()->setCellValue('B1','Gmail Mail Id')->getStyle('B1');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->setCellValue('C1','Subject')->getStyle('C1');
        $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
  
        $objPHPExcel->getActiveSheet()->setCellValue('D1','Email Address')->getStyle('D1');
        $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray); 
        
        $objPHPExcel->getActiveSheet()->setCellValue('E1','Receive Time (GMT)')->getStyle('E1');
        $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('F1','Clear Time (GMT)')->getStyle('F1');
        $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('G1','Response Time (Min)')->getStyle('G1');
        $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($styleArray1);
        
        $objPHPExcel->getActiveSheet()->setCellValue('H1','Response Time Office Hours (Min)')->getStyle('H1');
        $objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($styleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('I1','File')->getStyle('I1');
        $objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($styleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('J1','Closed By')->getStyle('J1');
        $objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($styleArray);
        
        
        
        $col = 2;
        /* 
        * Fetching data for last 2 months 
        */ 
   
        if(!empty($readMessageArr))
        {
            foreach($readMessageArr as $readMessageArrs)
            {     
                $dtSentTime = $readMessageArrs['dtSent'];
                $iDSTDifference=$readMessageArrs['iDSTDifference'];
                if($iDSTDifference>0){
                $dtSend = date('Y-m-d H:i:s',strtotime("-".$iDSTDifference." HOUR",strtotime($dtSentTime)));
                }
                else
                {
                    $dtSend = $dtSentTime;
                }
                //echo $dtSend."dtSend<br />";
                $dtReadMessageTaskToRead = $readMessageArrs['dtReadMessageTaskToRead'];
               // echo $dtReadMessageTaskToRead."dtReadMessageTaskToRead<br />";
                $iTotalReadMessageResponseTime = round((strtotime($dtReadMessageTaskToRead) - strtotime($dtSend))/60);
               
                if(is_string($readMessageArrs['szEmailSubject']) && (mb_detect_encoding($readMessageArrs['szEmailSubject'], "UTF-8", true) == "UTF-8"))
                {
                    $readMessageArrs['szEmailSubject']=(utf8_decode($readMessageArrs['szEmailSubject']));
                }
//                if($readMessageArrs['iAlreadyUtf8Encoded']=='1')
//                {
//                   $readMessageArrs['szEmailSubject']=utf8_decode($readMessageArrs['szEmailSubject']);
//                }
                //$iTotalReadMessageResponseTime
                $szManagmentName=$readMessageArrs['szFirstName']." ".$readMessageArrs['szLastName'];      
                $iReadMessageResponseTime=round($readMessageArrs['iReadMessageResponseTime']);
                $objPHPExcel->getActiveSheet()->setCellValue("A".$col,$readMessageArrs['iNumReadMessageEmail']);
                $objPHPExcel->getActiveSheet()->setCellValue("B".$col,$readMessageArrs['iEmailNumber']);
                $objPHPExcel->getActiveSheet()->setCellValue("C".$col,$readMessageArrs['szEmailSubject']);
                $objPHPExcel->getActiveSheet()->setCellValue("D".$col,$readMessageArrs['szToAddress']);
                $objPHPExcel->getActiveSheet()->setCellValue("E".$col,$dtSend);
                $objPHPExcel->getActiveSheet()->setCellValue("F".$col,$readMessageArrs['dtReadMessageTaskToRead']);
                $objPHPExcel->getActiveSheet()->setCellValue("G".$col,$iTotalReadMessageResponseTime);
                $objPHPExcel->getActiveSheet()->setCellValue("H".$col,$iReadMessageResponseTime);
                $objPHPExcel->getActiveSheet()->setCellValue("I".$col,$readMessageArrs['szFileRef']);  
                $objPHPExcel->getActiveSheet()->setCellValue("J".$col,$szManagmentName);  
                 
                $col++;
                $iTotalResponseTime += $lastMonthDataArys['iSearchResponseTime'];
            }
        }         
        
        $title = "_Read_Message_Task_Data";
        $objPHPExcel->getActiveSheet()->setTitle('Read Message');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->removeSheetByIndex(1);
        $szFileName = date('Ymdhsi').$title.".xlsx";
        $fileName=__APP_PATH_ROOT__."/logs/".$szFileName; 
        $objWriter->save($fileName); 
        return $fileName;  
    }
    
    
    function downloadCallUserTaskSheet($callUserArr)
    {  
        
        require_once( __APP_PATH_CLASSES__ . "/PHPExcel.php" );

        $objPHPExcel=new PHPExcel(); 

        $sheetIndex=0;
        $objPHPExcel = new PHPExcel();
        
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex($sheetIndex);
        $sheet = $objPHPExcel->getActiveSheet(); 

        $styleArray = array(
                'font' => array(
                        'bold' => false,
                        'size' =>12,
                ),
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                ),
                'borders' => array(
                        'top' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'bottom' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'right' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'left' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                ),
        );

        $styleArray1 = array(
                'font' => array(
                        'bold' => false,
                        'size' =>12,
                ),
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                ),
                'borders' => array(
                        'top' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'bottom' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'right' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'left' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                ),
        ); 
        $styleArray2 = array(
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                ),
                
        );

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);

        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->getStartColor()->setARGB('FFddd9c3');
         
        $objPHPExcel->getActiveSheet()->setCellValue('A1','File Reference')->getStyle('A1');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray); 
        
        $objPHPExcel->getActiveSheet()->setCellValue('B1','Receive Time (GMT)')->getStyle('B1');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('C1','Clear Time (GMT)')->getStyle('C1');
        $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('D1','Response Time (Min)')->getStyle('D1');
        $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray1);
        
        $objPHPExcel->getActiveSheet()->setCellValue('E1','Response Time Office Hours (Min)')->getStyle('J1');
        $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('F1','Closed By')->getStyle('E1');
        $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);
        
        
        
        $col = 2;
        /* 
        * Fetching data for last 2 months 
        */ 
   
        if(!empty($callUserArr))
        {
            foreach($callUserArr as $callUserArr)
            {     
                               
                $szManagmentName=$callUserArr['szFirstName']." ".$callUserArr['szLastName'];      
                $iAutomaticSearchResponseTime=round($callUserArr['iAutomaticSearchResponseTime']);
                $iTotalAutomaticSearchResponseTime=round($callUserArr['iTotalTimeResponse']/60);
                
                $dtFileCreated=$callUserArr['dtFileCreated'];
                $dtResponseTime=$callUserArr['dtResponseTime'];
                $iTotalAutomaticSearchResponseTime = round((strtotime($dtResponseTime) - strtotime($dtFileCreated))/60);
                
                
                
                $objPHPExcel->getActiveSheet()->setCellValue("A".$col,$callUserArr['szFileRef']);
                $objPHPExcel->getActiveSheet()->setCellValue("B".$col,$callUserArr['dtFileCreated']);
                $objPHPExcel->getActiveSheet()->setCellValue("C".$col,$callUserArr['dtResponseTime']);
                $objPHPExcel->getActiveSheet()->setCellValue("D".$col,$iTotalAutomaticSearchResponseTime);
                $objPHPExcel->getActiveSheet()->setCellValue("E".$col,$iAutomaticSearchResponseTime); 
                $objPHPExcel->getActiveSheet()->setCellValue("F".$col,$szManagmentName);  
                
                $col++;
            }
        }         
        
        $title = "_Call_User_Task_Data";
        $objPHPExcel->getActiveSheet()->setTitle('Call User');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->removeSheetByIndex(1);
        $szFileName = date('Ymdhsi').$title.".xlsx";
        $fileName=__APP_PATH_ROOT__."/logs/".$szFileName; 
        $objWriter->save($fileName); 
        return $fileName;  
    }
    
    
    function getManualRFQData()
    {            
        $query="
            SELECT	
                iTotalClearTime as iRFQSearchResponseTime,
                track1.id as iNumAutomaticSearches,
                track1.dtFileCreated,
                track1.dtResponseTime,
                bf.szFileRef,
                m.szFirstName,
                m.szLastName
            FROM
                ".__DBC_SCHEMATA_RESPONSE_TIME_TRACKER__." as track1
            INNER JOIN
                ".__DBC_SCHEMATA_FILE_LOGS__." AS bf
            ON
                bf.id=track1.idFile
            INNER JOIN
                ".__DBC_SCHEMATA_MANAGEMENT__." AS m
            ON
                m.id=track1.idAdminAddedBy        
            WHERE 
                date_format(track1.dtCreatedOn,'%Y-%m') >= date_format(now() - INTERVAL 5 MONTH, '%Y-%m' )
            AND
                track1.isDeleted = '0' 
            AND
                track1.iSearchType = '2'
            AND
                track1.szButtonClicked = 'SEND_CUSTOMER_QUOTES'    
            ORDER BY
                track1.dtResponseTime DESC
        ";
        //echo $query;
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            $dashBoardArr = array();
            $ctr = 0;
            while($row=$this->getAssoc($result))
            {
                $resArr[]=$row;                
            }
        }
        
         require_once( __APP_PATH_CLASSES__ . "/PHPExcel.php" );

        $objPHPExcel=new PHPExcel(); 

        $sheetIndex=0;
        $objPHPExcel = new PHPExcel();
        
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex($sheetIndex);
        $sheet = $objPHPExcel->getActiveSheet(); 

        $styleArray = array(
                'font' => array(
                        'bold' => false,
                        'size' =>12,
                ),
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                ),
                'borders' => array(
                        'top' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'bottom' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'right' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'left' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                ),
        );

        $styleArray1 = array(
                'font' => array(
                        'bold' => false,
                        'size' =>12,
                ),
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                ),
                'borders' => array(
                        'top' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'bottom' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'right' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'left' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                ),
        ); 
        $styleArray2 = array(
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                ),
                
        );

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25); 


        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->getStartColor()->setARGB('FFddd9c3');
         
        $objPHPExcel->getActiveSheet()->setCellValue('A1','File Reference')->getStyle('A1');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray); 
        
        $objPHPExcel->getActiveSheet()->setCellValue('B1','Receive Time (GMT)')->getStyle('B1');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('C1','Clear Time (GMT)')->getStyle('C1');
        $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('D1','Response Time (Min)')->getStyle('D1');
        $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray1);
        
        $objPHPExcel->getActiveSheet()->setCellValue('E1','Response Time Office Hours (Min)')->getStyle('E1');
        $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
        
        $objPHPExcel->getActiveSheet()->setCellValue('F1','Closed By')->getStyle('F1');
        $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);
        
        $col = 2;
        /* 
        * Fetching data for last 2 months 
        */ 
   
        if(!empty($resArr))
        {
            foreach($resArr as $resArrs)
            {     
                               
                $szManagmentName=$resArrs['szFirstName']." ".$resArrs['szLastName'];      
                $iRFQSearchResponseTime=round($resArrs['iRFQSearchResponseTime']);
                
                $dtFileCreated=$resArrs['dtFileCreated'];
                $dtResponseTime=$resArrs['dtResponseTime'];
                $iTotalRFQSearchResponseTime = round((strtotime($dtResponseTime) - strtotime($dtFileCreated))/60);
                
                
                
                $objPHPExcel->getActiveSheet()->setCellValue("A".$col,$resArrs['szFileRef']);
                $objPHPExcel->getActiveSheet()->setCellValue("B".$col,$resArrs['dtFileCreated']);
                $objPHPExcel->getActiveSheet()->setCellValue("C".$col,$resArrs['dtResponseTime']);
                $objPHPExcel->getActiveSheet()->setCellValue("D".$col,$iTotalRFQSearchResponseTime); 
                $objPHPExcel->getActiveSheet()->setCellValue("E".$col,$iRFQSearchResponseTime);  
                $objPHPExcel->getActiveSheet()->setCellValue("F".$col,$szManagmentName);  
                 
                $col++;
            }
        }         
        
        $title = "_Manual_RFQ";
        $objPHPExcel->getActiveSheet()->setTitle('Manual RFQ');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->removeSheetByIndex(1);
        $szFileName = date('Ymdhsi').$title.".xlsx";
        $fileName=__APP_PATH_ROOT__."/logs/".$szFileName; 
        $objWriter->save($fileName); 
        return $fileName;
    }
    
    
    function getTransportecaOfficeHour($flag=true)
    {
        $resArr=array();
        $query="
            SELECT
                idWeekDay,
                dtOpenGMT,
                dtCloseGMT
            FROM
                ".__DBC_SCHEMATA_TRANSPORTECA_OFFICE_HOURS__."
            ";
            if( ( $result = $this->exeSQL( $query ) ) )
            {
                while($row=$this->getAssoc($result))
                {
                    if($flag)
                    {
                        $dtOpenGMTArr=  explode(":", $row['dtOpenGMT']);
                        $resArr['iOpenTime'][$row['idWeekDay']][0]=$dtOpenGMTArr[0];
                        $resArr['iOpenTime'][$row['idWeekDay']][1]=$dtOpenGMTArr[1];
                        
                        $dtCloseGMTArr=  explode(":", $row['dtCloseGMT']);
                        $resArr['iCloseTime'][$row['idWeekDay']][0]=$dtCloseGMTArr[0];
                        $resArr['iCloseTime'][$row['idWeekDay']][1]=$dtCloseGMTArr[1];
                    }
                    else
                    {
                        $resArr[$row['idWeekDay']]['iOpenTime']=$row['dtOpenGMT'];
                        $resArr[$row['idWeekDay']]['iCloseTime']=$row['dtCloseGMT'];
                    }
                }
                
            }
            return $resArr;
    }
    
    function callUserRFQResponseTime()
    {
        $query="
            SELECT
                id,
                dtFileCreated,
                dtResponseTime
            FROM
                ".__DBC_SCHEMATA_RESPONSE_TIME_TRACKER__."
        ";   
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                $dtFileCreated = $row['dtFileCreated'];
                $dtResponseTime = $row['dtResponseTime'];
                $id = $row['id'];
                //echo $dtFileCreated."dtFileCreated========dtResponseTime".$dtResponseTime."<br />";
                $iTotalClearTime= get_working_hours_for_graph($dtFileCreated,$dtResponseTime);
                //echo $id."#########".$iTotalClearTime."iTotalClearTime<br />";
                
                if((float)$iTotalClearTime<0.00)
                {
                    $iTotalClearTime=0;
                }
                    
                
                $updateQuery="
                    UPDATE
                        ".__DBC_SCHEMATA_RESPONSE_TIME_TRACKER__."
                    SET
                        iTotalClearTime='".(float)$iTotalClearTime."'
                    WHERE        
                        id='".(int)$id."'
                    ";
                $updateresult = $this->exeSQL( $updateQuery );
                
            }
        }
    }
    
    
    function callReadMessageResponseTime()
    {
        $query="
            SELECT
                id,
                dtSent,
                dtReadMessageTaskToRead,
                iDSTDifference,
                idAdminAddedBy
            FROM
                ".__DBC_SCHEMATA_EMAIL_LOG__."
            WHERE
                dtReadMessageTaskToRead<>'0000-00-00 00:00:00'
        ";   
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            while($row=$this->getAssoc($result))
            {
                $dtSendTime = $row['dtSent'];
                $iDSTDifference=$row['iDSTDifference'];
                if((int)$iDSTDifference>0){
                    $dtSend = date('Y-m-d H:i:s',strtotime("-".$iDSTDifference." HOUR",strtotime($dtSendTime)));
                }
                else
                {
                    $dtSend =$dtSendTime;
                }
                $dtReadMessageTaskToRead = $row['dtReadMessageTaskToRead'];
                $idCrmEmailLogs = $row['id'];
                $idAdminAddedBy=$row['idAdminAddedBy'];
                echo $dtSend."dtFileCreated========dtResponseTime".$dtReadMessageTaskToRead."<br />";
                $iTotalClearTime= get_working_hours_for_graph($dtSend,$dtReadMessageTaskToRead);
                echo $idCrmEmailLogs."#########".$iTotalClearTime."iTotalClearTime<br />";
                
                if((float)$iTotalClearTime<0.00)
                {
                    $iTotalClearTime=0;
                }
                    
                
                    $updateQuery="
                        UPDATE
                            ".__DBC_SCHEMATA_EMAIL_LOG__."
                        SET
                            iTotalClearTime='".(float)$iTotalClearTime."'
                        WHERE        
                            id='".(int)$idCrmEmailLogs."'
                        ";
                    $updateresult = $this->exeSQL( $updateQuery );
                    
                     $updateQuery="
                        UPDATE                
                            ".__DBC_SCHEMATA_CUSTOMER_LOG_SNIPPET__."
                        SET
                            dtReadMessageTaskToRead='".mysql_escape_custom($dtReadMessageTaskToRead)."',
                            idAdminAddedBy='".(int)$idAdminAddedBy."',
                            iTotalClearTime='".(float)$iTotalClearTime."'    
                        WHERE
                            idPrimary='".(int)$idCrmEmailLogs."'
                        AND
                            idCustomerLogDataType=1
                    ";
                    //echo $query;
                    $updateresult = $this->exeSQL($updateQuery);
                
            }
        }
    }
    
    function downloadDashboardGraphDetails6Month()
    {   
        $kBooking = new cBooking(); 
        require_once( __APP_PATH_CLASSES__ . "/PHPExcel.php" );

        $objPHPExcel=new PHPExcel(); 

        $sheetIndex=0;
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex($sheetIndex);
        $sheet = $objPHPExcel->getActiveSheet(); 

        $styleArray = array(
                'font' => array(
                        'bold' => false,
                        'size' =>12,
                ),
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                ),
                'borders' => array(
                        'top' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'bottom' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'right' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'left' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                ),
        );

        $styleArray1 = array(
                'font' => array(
                        'bold' => false,
                        'size' =>12,
                ),
                'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                ),
                'borders' => array(
                        'top' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'bottom' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'right' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'left' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                ),
        ); 

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10); 

        $objPHPExcel->getActiveSheet()->getStyle('A1:N1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1:N1')->getFill()->getStartColor()->setARGB('FFddd9c3');
        //$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);

        $objPHPExcel->getActiveSheet()->setCellValue('A1','Booking Date')->getStyle('A1');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray); 

        $objPHPExcel->getActiveSheet()->setCellValue('B1','Reference')->getStyle('B1');
        $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->setCellValue('C1','Customer name')->getStyle('C1');
        $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->setCellValue('D1','Type')->getStyle('D1');
        $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray); 

        $objPHPExcel->getActiveSheet()->setCellValue('E1','Price (USD)')->getStyle('E1');
        $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray); 

        $objPHPExcel->getActiveSheet()->setCellValue('F1','Referral/Markup (USD)')->getStyle('F1');
        $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->setCellValue('G1','Curr. markup (USD)')->getStyle('G1');
        $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($styleArray); 

        $objPHPExcel->getActiveSheet()->setCellValue('H1','Insurance Sell Price (USD)')->getStyle('H1');
        $objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->setCellValue('I1','Insurance Buy Price (USD)')->getStyle('I1');
        $objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->setCellValue('J1','Label Fee (USD)')->getStyle('J1');
        $objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($styleArray); 

        $objPHPExcel->getActiveSheet()->setCellValue('K1','Handling Fee (USD)')->getStyle('K1');
        $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($styleArray); 
        
        $objPHPExcel->getActiveSheet()->setCellValue('L1','Net Markup (USD)')->getStyle('L1');
        $objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->setCellValue('M1','Turnover (USD)')->getStyle('M1');
        $objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->setCellValue('N1','GP (USD)')->getStyle('M1');
        $objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray($styleArray); 
        
        $col = 2;
        /* 
        * Fetching data for last 2 months 
        */
        $dtStartDate = date("Y-m-01",strtotime(date('Y-m')." -5 month"));
        $dtEndDate = date("Y-m-d");

        $fromDate = strtotime($dtStartDate);
        $toDate = strtotime($dtEndDate);
        $counter = $fromDate ; 
        $kConfig = new cConfig();

        while($counter <= $toDate)
        { 
            $key = date('Y-m-01',$counter);    
            $keyStr = date('M',$counter);   

            $lastMonthDataAry = array();
            $lastMonthDataAry = $this->getDataForDashboardCsvFile($key);

            $fTurnover = 0;
            $fSalesRevenue = 0; 
            $fTotalSaleRevenue = 0;
            $fTotalTurnover = 0; 
            $kBooking = new cBooking();
            if(!empty($lastMonthDataAry))
            {
                foreach($lastMonthDataAry as $lastMonthDataArys)
                {    
                    $fInsuranceRevenue = ($lastMonthDataArys['fTotalInsuranceCostForBookingUSD'] - $lastMonthDataArys['fTotalInsuranceCostForBookingUSD_buyRate']);
                    $fSaleRevenue = $fInsuranceRevenue + $lastMonthDataArys['fTotalReferalAmountUSD'] ;   
                    $fTurnover = $lastMonthDataArys['fTotalPriceUSD'] + $lastMonthDataArys['fTotalInsuranceCostForBookingUSD'] ; 

                    $fTotalSaleRevenue += $fSaleRevenue ;
                    $fTotalTurnover += $fTurnover ;

                    $idBooking = $lastMonthDataArys['idBooking'];
                    $fLabelFeeUsd = 0;
                    if($lastMonthDataArys['iCourierBooking']==1 || $lastMonthDataArys['isManualCourierBooking']==1)
                    {
                       $courierBookingDetails = $kBooking->getCourierBookingDetails($idBooking);

                       if(!empty($courierBookingDetails) && $courierBookingDetails['iCourierAgreementIncluded']==0)
                       {
                           $fLabelFeeUsd = $courierBookingDetails['fBookingLabelFeeRate'] * $courierBookingDetails['fBookingLabelFeeROE'];
                       }
                    }
                    if($lastMonthDataArys['iForwarderGPType']==__FORWARDER_PROFIT_TYPE_MARK_UP__)
                    {
                        $szMarkupType = "Markup";
                    }
                    else
                    {
                        $szMarkupType = "Referrel";
                    }
                    if($lastMonthDataArys['fCustomerExchangeRate'])
                    {
                        $fPriceMarkupCustomerCurrency = $lastMonthDataArys['fPriceMarkupCustomerCurrency'] * $lastMonthDataArys['fCustomerExchangeRate'];
                    }
                    else
                    {
                        $fPriceMarkupCustomerCurrency = 0;
                    }
                    
                    $fMarkupInvoiceAmount=0;
                    $fMarkupInvoicePaid=$this->getMarkupInvoicePaid($idBooking);
                    if($lastMonthDataArys['iForwarderGPType']==__FORWARDER_PROFIT_TYPE_MARK_UP__)
                    {                       
                        $fMarkupInvoiceAmount=$this->getfMarkupInvoiceAmount($idBooking);
          
                    }
                    $fHandlingFeeUsd = 0;
                    if($lastMonthDataArys['iHandlingFeeApplicable']==1)
                    {
                        $fHandlingFeeUsd = $lastMonthDataArys['fTotalHandlingFeeUSD'];
                    } 
                    
                    $netMarkup=(round((float)$fMarkupInvoiceAmount,2)-round((float)$fMarkupInvoicePaid,2));
                    $fTotalGPUSD =(round((float)$fMarkupInvoiceAmount,2)-round((float)$fMarkupInvoicePaid,2)) + round((float)$lastMonthDataArys['fReferalAmountUSD'],2) + round((float)$fPriceMarkupCustomerCurrency,2) + round((float)$lastMonthDataArys['fTotalInsuranceCostForBookingUSD'],2) - round((float)$lastMonthDataArys['fTotalInsuranceCostForBookingUSD_buyRate'],2) + round((float)$fLabelFeeUsd,2) + round((float)$fHandlingFeeUsd,2) ;
                    $objPHPExcel->getActiveSheet()->setCellValue("A".$col,date('Y-m-d',strtotime($lastMonthDataArys['dtBookingConfirmed'])));
                    $objPHPExcel->getActiveSheet()->setCellValue("B".$col,$lastMonthDataArys['szBookingRef']);
                    $objPHPExcel->getActiveSheet()->setCellValue("C".$col,$lastMonthDataArys['szCustomerName']);
                    $objPHPExcel->getActiveSheet()->setCellValue("D".$col,$szMarkupType); 
                    $objPHPExcel->getActiveSheet()->setCellValue("E".$col,round((float)$lastMonthDataArys['fTotalPriceUSD'],2));    
                    $objPHPExcel->getActiveSheet()->setCellValue("F".$col,round((float)$lastMonthDataArys['fReferalAmountUSD'],2));	
                    $objPHPExcel->getActiveSheet()->setCellValue("G".$col,round((float)$fPriceMarkupCustomerCurrency,2));
                    $objPHPExcel->getActiveSheet()->setCellValue("H".$col,round((float)$lastMonthDataArys['fTotalInsuranceCostForBookingUSD'],2));
                    $objPHPExcel->getActiveSheet()->setCellValue("I".$col,round((float)$lastMonthDataArys['fTotalInsuranceCostForBookingUSD_buyRate'],2));  
                    $objPHPExcel->getActiveSheet()->setCellValue("J".$col,round((float)$fLabelFeeUsd,2)); 
                    $objPHPExcel->getActiveSheet()->setCellValue("K".$col,round((float)$fHandlingFeeUsd,2)); 
                    $objPHPExcel->getActiveSheet()->setCellValue("L".$col,round((float)$netMarkup,2)); 
                    $objPHPExcel->getActiveSheet()->setCellValue("M".$col,round((float)$fTurnover,2));
                    $objPHPExcel->getActiveSheet()->setCellValue("N".$col,round((float)$fTotalGPUSD,2));
                    $col++; 
                } 
            }

            $szHeading = "Graph data for month ".date('F, Y',$counter);
            $col++;
            $objPHPExcel->getActiveSheet()->setCellValue("A".$col,$szHeading);
            $objPHPExcel->getActiveSheet()->setCellValue("H".$col,$fTotalSaleRevenue);
            $objPHPExcel->getActiveSheet()->setCellValue("I".$col,$fTotalTurnover);
            $col++; 
            $counter = strtotime("+1 MONTH",$counter); 
        }

        $title = "_Transporteca_dashboard_graph";
        $objPHPExcel->getActiveSheet()->setTitle('Transporteca Dashboard');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->removeSheetByIndex(1);
        $file=date('Ymdhs').$title;
        $fileName=__APP_PATH_ROOT__."/insuranceSheets/".$file.".xlsx"; 
        $objWriter->save($fileName); 
        return $fileName; 
    }

    function getMarkupInvoicePaid($idBooking)
    {
        $query=" 
            SELECT	
                SUM(fTotalPriceForwarderCurrency) AS fMarkupInvoicePaid		
            FROM
                ".__DBC_SCHEMATA_FORWARDER_TRANSACTION__."
            WHERE
                 idBooking='".(int)$idBooking."'
            AND 
                iDebitCredit = '".__TRANSACTION_DEBIT_CREDIT_FLAG_10__."'";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            $row=$this->getAssoc($result);
            
            return $row['fMarkupInvoicePaid'];
        }
                  
    }
    
    function getfMarkupInvoiceAmount($idBooking)
    {
        $query="
            SELECT	
                SUM(fTotalPriceUSD) AS fMarkupInvoiceAmount		
            FROM
                ".__DBC_SCHEMATA_BOOKING__." as b5
            WHERE
                id='".(int)$idBooking."' 
            AND 
                idBookingStatus IN(3,4) 
            AND
                iTransferConfirmed = '0'  
            AND
                iForwarderGPType = '".__FORWARDER_PROFIT_TYPE_MARK_UP__."'";
        if( ( $result = $this->exeSQL( $query ) ) )
        {
            $row=$this->getAssoc($result);
            
            return $row['fMarkupInvoiceAmount'];
        }
    }
       
}    
?>