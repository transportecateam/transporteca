<?php
/**
 * This file is the container for all user related functionality.
 * All functionality related to user details should be contained in this class.
 *
 * tracking.class.php
 *
 * @copyright Copyright (C) 2012 Transport-eca 
 * @author Ajay
 */
if(!isset($_SESSION))
{
   session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_INC__ . "/functions.php" );
require_once( __APP_PATH_INC__ . "/courier_functions.php" );
require_once( __APP_PATH_CLASSES__ . "/database.class.php");
require_once( __APP_PATH__ . "/inc/classes/parser.class.php" ); 

class cTracking extends cDatabase
{
    function __construct()
    {
        parent::__construct();
        return true;
    } 
    var $t_base_courier='management/providerProduct/'; 
    var $t_base_services = "SERVICEOFFERING/"; 
    var $t_base_courier_label ="COURIERLABEL/";
    var $tracking_error =false;
    var $tracking_error_array =array();
        
    /*
     * Function To Create New Aftership Tracking
     * param required tracking_number
     * return array
     * Slugs Values
     * For TNT Type Slug = tnt
     * For UPS Type slug = ups
     * For Fedex Type slug = fedex
     */
    function createAfterShipTracking($idBooking,$szTrackingNumber,$idServiceProvider=0)
    {
        if($idBooking>0)
        {
            $kBooking = new cBooking(); 
            $bookingDetailAry = $kBooking->getExtendedBookingDetails($idBooking); 
            if((int)$idServiceProvider>0 && $bookingDetailAry['iBookingType']==__BOOKING_TYPE_RFQ__ && $bookingDetailAry['isManualCourierBooking']=='1')
            {
                $idCourierProvider = $this->getIdCourierProviderByIdServiceProvider($idServiceProvider);
            }
            else
            {
                $idCourierProvider = $this->getIdCourierProviderByIdServiceProvider($bookingDetailAry['idServiceProvider']);
            }
            if(!empty($bookingDetailAry))
            { 
                if(empty($szTrackingNumber))
                {
                    $this->tracking_error = true;
                    $this->szAfterShipTrackingNotification = "Tracking number can't be empty";
                    return false;
                } 
                $URL = 'https://api.aftership.com/v4/trackings/'; 
                
                $params['tracking']['tracking_number']=trim($szTrackingNumber); 
                
                $kCourierServices = new cCourierServices(); 
                $courierProviderComanyAry = array();
                $courierProviderComanyAry = $kCourierServices->getCourierProviderDetails();
                
                $idDestinationCountry = $bookingDetailAry['idDestinationCountry'];
                $kConfig = new cConfig();
                $kConfig->loadCountry($idDestinationCountry);
                $szDestinationCountryCode = $kConfig->szCountryISO3Code;

                $szProviderName = $courierProviderComanyAry[$idCourierProvider]['szName'];
                $params['tracking']['slug'] = strtolower($szProviderName);
                /*
                if($idCourierProvider == __FEDEX_API__)
                {
                    $params['tracking']['slug'] = "fedex";
                }
                else if($idCourierProvider == __UPS_API__)
                {
                    $params['tracking']['slug'] = "ups";
                }
                else if($idCourierProvider == __TNT_API__)
                {
                    $params['tracking']['slug'] = "tnt";
                }
                */
                if($bookingDetailAry['iPrivateShipping']>0)
                {
                    $customer_name = $bookingDetailAry['szFirstName']." ".$bookingDetailAry['szLastName'];
                }
                else
                {
                    $customer_name = $bookingDetailAry['szCustomerCompanyName'];
                } 
                $params['tracking']['custom_fields']['Description'] = $bookingDetailAry['szBookingRef']." - ".$customer_name; 
                $params['tracking']['customer_name'] = $customer_name; 
                $params['tracking']['order_id'] = $bookingDetailAry['szBookingRef']; 
                $params['tracking']['title'] = $bookingDetailAry['szBookingRef']." - ".$customer_name; 
                
                $params['tracking']['destination_country_iso3'] = $szDestinationCountryCode;
                $params['tracking']['tracking_destination_country'] = $bookingDetailAry['szConsigneeCountryCode'];
                
                if(!empty($bookingDetailAry['szConsigneePostCode']))
                {
                    //$params['tracking']['tracking_postal_code'] = $bookingDetailAry['szConsigneePostCode'];
                }
                
                $idAftershipEventLog = $this->addAftershipEventLog($params); 
                $response = array(); 
                $data_string = json_encode($params);   
                $response = $this->get_afterShip_curl_result($URL,$data_string,'POST');  
                
                $decoded_response = json_decode($response); 
                $finalAry = toArray($decoded_response);
                
                $this->updateAftershipEventLog($idAftershipEventLog,$finalAry,$finalAry['meta']['code']); 
                $resultAry = array(); 
                if($finalAry['meta']['code'] == 201)
                { 
                    $trackingAry = $finalAry['data']['tracking'];  
                    $resultAry['szTrackingID'] =  $trackingAry['id'];
                    $resultAry['szTrackingNumber'] =  $trackingAry['tracking_number'];
                    $resultAry['szSlug'] =  $trackingAry['slug'];
                    $resultAry['dtCreatedAtAPI'] = date('Y-m-d H:i:s',strtotime($trackingAry['created_at']));
                    $resultAry['is_active'] =  $trackingAry['active'];
                    $resultAry['szTrackingTag'] =  $trackingAry['tag'];

                    if(!empty($resultAry['szTrackingTag']))
                    {
                        $tracking_tag_message = $this->get_aftership_tag_details($resultAry['szTrackingTag']); 
                        $resultAry['szTrackingTagDescription'] = $tracking_tag_message['tag_message'];
                    }
                    $resultAry['szCustomerName'] =  $trackingAry['customer_name'];
                    $resultAry['szUniqueToken'] =  $trackingAry['unique_token'];

                    if(!empty($trackingAry['custom_fields']))
                    {
                        $customfieldsAry = $trackingAry['custom_fields']; 
                        for($i=0;$i<count($customfieldsAry);$i++)
                        {
                            $resultAry['custom_fields'][$i]['Description'] =  $customfieldsAry[$i]['Description'];
                        }
                    }
                    else
                    {
                        $resultAry['custom_fields'] = array();
                    }
                    $this->addCourierTrackingLogDetail($resultAry,$idBooking,true); 
                    $this->szAfterShipTrackingNotification = $resultAry['szTrackingTag']." - ".$resultAry['szTrackingTagDescription'];
                }
                else
                {
                     
                    $resultAry['error']['code'] = $finalAry['meta']['code'];
                    $resultAry['error']['message'] = $finalAry['meta']['message'];
                    
                    if($finalAry['meta']['code']== "4003")
                    {
                        $tagDetailAry = $this->getBookingDetailByTrackingNumber($szTrackingNumber);
                        if($idBooking==$tagDetailAry['id'])
                        {
                            $this->szAfterShipTrackingNotification= $tagDetailAry['szTrackingTag']." - ".$tagDetailAry['szTrackingTagDescription'];
                        }
                        else
                        {
                            $this->tracking_error=true;
                            $this->szAfterShipTrackingNotification= $finalAry['meta']['message'];
                        }
                    }
                    else
                    {
                        $this->tracking_error=true;
                        $this->szAfterShipTrackingNotification = $finalAry['meta']['message']; 
                        
                    }
                     $this->tracking_error_array = $resultAry;
                    
                }
                return $resultAry;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    
    /*
     * Function To Get Tracking Detail
     * From After Ship API
     * param tracking array
     * return array
     * Slugs Values
     * For TNT Type Slug = tnt
     * For UPS Type slug = ups
     * For Fedex Type slug = fedex
     */
    function get_aftership_courier_tracking_detail($params)
    {
        /*
         * Url To Get All Records
         * $URL = 'https://api.aftership.com/v4/trackings';
         */
        
        /*
         * Url To Get Single Record
         * $URL = 'https://api.aftership.com/v4/trackings/:slug/:tracking_number;
         */
        
        if(!empty($params))
        {
            $idAftershipEventLog = $this->addAftershipEventLog($params);
            
            $response = array();
            
            $data_string = json_encode($params);

            $URL = 'https://api.aftership.com/v4/trackings/'.$params['slug'].'/'.$params['tracking_number']; 

            $response = $this->get_afterShip_curl_result($URL);
            
            $decoded_response = json_decode($response);
            
            $finalAry = toArray($decoded_response);  
            $this->updateAftershipEventLog($idAftershipEventLog,$finalAry,$finalAry['meta']['code']);
            
            $resultAry = array();
            
            if($finalAry['meta']['code'] == 200)
            {
                $trackingAry = $finalAry['data']['tracking'];
                
                $resultAry['tracking_id'] =  $trackingAry['id'];
                $resultAry['tracking_number'] =  $trackingAry['tracking_number'];
                $resultAry['slug'] =  $trackingAry['slug'];
                $resultAry['created_at'] = date('Y-m-d H:i:s',strtotime($trackingAry['created_at']));
                $resultAry['last_updated_at'] =  date('Y-m-d H:i:s',strtotime($trackingAry['last_updated_at']));
                $resultAry['is_active'] =  $trackingAry['active'];
                
                $resultAry['tracking_tag'] =  $trackingAry['tag'];
                
                if(!empty($resultAry['tracking_tag']))
                {
                    $tracking_tag_message = $this->get_aftership_tag_details($resultAry['tracking_tag']);
                    
                    $resultAry['tracking_tag_message'] = $tracking_tag_message['tag_message'];
                }
                
                
                $kConfig_new = new cConfig();
                
                if(!empty($trackingAry['origin_country_iso3']))
                {
                    $kConfig_new->loadCountry(false,$trackingAry['origin_country_iso3']);
                    $szOriginCountryName = $kConfig_new->szCountryName ; 
                    $resultAry['szOriginCountryName'] =  $szOriginCountryName;
                }
                
                if(!empty($trackingAry['destination_country_iso3']))
                {
                    $kConfig_new->loadCountry(false,$trackingAry['destination_country_iso3']);
                    $szDestinationCountryName = $kConfig_new->szCountryName ; 
                    $resultAry['szDestinationCountryName'] =  $szDestinationCountryName;
                }
                
                $resultAry['shipment_pickup_date'] =  $trackingAry['shipment_pickup_date'];
                $resultAry['expected_delivery'] =  $trackingAry['expected_delivery'];
                
                
                if(!empty($trackingAry['checkpoints']))
                {
                    $checkpointAry = $trackingAry['checkpoints'];
                    
                    for($i=0;$i<count($checkpointAry);$i++)
                    {
                        $resultAry['checkpoints'][$i]['city'] =  $checkpointAry[$i]['city'];
                        $resultAry['checkpoints'][$i]['location'] =  $checkpointAry[$i]['location'];
                        
                        $kConfig_new->loadCountry(false,$checkpointAry[$i]['country_iso3']);
                        $checkpoints_country = $kConfig_new->szCountryName ; 

                        $resultAry['checkpoints'][$i]['checkpoints_country'] = $checkpoints_country;
                        
                        $resultAry['checkpoints'][$i]['message'] =  $checkpointAry[$i]['message'];
                        $resultAry['checkpoints'][$i]['checkpoint_time'] = date('Y-m-d H:i:s',strtotime($checkpointAry[$i]['checkpoint_time']));
                        
                        $resultAry['checkpoints'][$i]['checkpoints_tag'] = $checkpointAry[$i]['tag'];
                        
                        if(!empty($resultAry['checkpoints'][$i]['checkpoints_tag']))
                        {
                            $checkpoints_tag_message = $this->get_aftership_tag_details($resultAry['checkpoints'][$i]['checkpoints_tag']);

                            $resultAry['checkpoints'][$i]['checkpoints_tag_message'] = $checkpoints_tag_message['tag_message'];
                        }
                        
                    }
                    
                }
                else
                {
                    $resultAry['checkpoints'] = array();
                }
                
                
                if(!empty($trackingAry['custom_fields']))
                {
                    $customfieldsAry = $trackingAry['custom_fields'];
                    
                    for($i=0;$i<count($customfieldsAry);$i++)
                    {
                        $resultAry['custom_fields'][$i]['szDescription'] =  $customfieldsAry[$i]['szDescription'];
                    }
                    
                }
                else
                {
                    $resultAry['custom_fields'] = array();
                }
                
            }
            else
            {
                $resultAry['error']['code'] = $finalAry['meta']['code'];
                $resultAry['error']['message'] = $finalAry['meta']['message'];
            }
            
            return $resultAry;
        }
        else
        {
            return false;
        }
        
    }

    function get_afterShip_curl_result($URL,$data_string=false,$method="GET")
    { 
        $aftership_api_key = __AFTER_SHIP_API_KEY__;  
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method); 
        
        if(!empty($data_string))
        {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);// Required to get all trecord tracking
        }

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'aftership-api-key:'.$aftership_api_key)
                );
        $contents = curl_exec($ch); 
        $err  = curl_getinfo($ch); 
        curl_close($ch);

        if(!empty($contents))
        {
            return $contents;
        }
        else
        {
            return FALSE;
        }
    }
    
    function get_aftership_tag_details($szTagName)
    {
        if(!empty($szTagName))
        {
            $tag_detail=array();
            
            switch (trim($szTagName))
            {
                case "InfoReceived":
                    $tag_message = "Carrier has received request from shipper and is about to pick up the shipment.";
                    $section_title = '';
                    break; 
                case "InTransit":
                    $tag_message = "Carrier has accepted or picked up shipment from shipper. The shipment is on the way.";
                    $section_title = "__COURIER_TRACKING_PICKED_UP__";
                    break; 
                case "OutForDelivery":
                    $tag_message = "Carrier is about to deliver the shipment , or it is ready to pickup.";
                    $section_title = "__COURIER_TRACKING_OUT_FOR_DELIVERY__";
                    break;
                case "AttemptFail":
                    $tag_message = "Carrier attempted to deliver but failed, and usually leaves a notice and will try to delivery again.";
                    $section_title = "__COURIER_TRACKING_FAILED_ATTEMPT__";
                    break;
                case "Delivered":
                    $tag_message = "The shipment was delivered sucessfully.";
                    $section_title = "__COURIER_TRACKING_DELIVERED__";
                    break;
                case "Exception":
                    $tag_message = "Custom hold, undelivered, returned shipment to sender or any shipping exceptions.";
                    $section_title = '';
                    break;
                case "Expired":
                    $tag_message = "Shipment has no tracking information for 7 days since added, or has no further updates for 30 days since last update.";
                    $section_title = '';
                    break;
                case "Pending":
                    $tag_message = "New shipments added that are pending to track, or new shipments without tracking information available yet.";
                    $section_title = '';
                    break; 
                default :
                    $tag_message = "Case not found for checkpoint tag";
                    $section_title = '';
                    break;
            } 
            
            $tag_detail['tag_message'] = $tag_message;
            $tag_detail['section_title'] = $section_title;
            
            return $tag_detail;
        }
        else
        {
            return false;
        }
    }
    
    
    function addAftershipEventLog($szRequestParams)
    {
        if(!empty($szRequestParams))
        {
            $szRequestParams = serialize($szRequestParams);
        }
        
        $query="
            INSERT INTO
                ".__DBC_SCHEMATA_AFTERSHIP_TRACKING_EVENT_LOG__."
            (
                szRequestParams,     
                dtCreated
            )
            VALUES
            (
                '".mysql_escape_custom($szRequestParams)."',
                NOW()
            )
        ";
        if($result = $this->exeSQL($query))
        {
            $idAftershipEventLog = $this->iLastInsertID; 
            return $idAftershipEventLog;  
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }      
    }
    
    function updateAftershipEventLog($idAftershipEventLog,$szResposeData,$szResponseCode)
    {
        if(!empty($szResposeData))
        {
            $szResposeData = serialize($szResposeData);
        }
        
        $query = "
                    UPDATE
                            ".__DBC_SCHEMATA_AFTERSHIP_TRACKING_EVENT_LOG__."
                    SET
                            szResponseCode = '".$szResponseCode."',
                            szResposeData = '".$szResposeData."',
                            dtUpdatedOn = NOW()
                    WHERE
                            id = '".(int)$idAftershipEventLog."' 	
        ";
        //echo $query ;
        if($result=$this->exeSQL($query))
        {
            return true ;
        }
        else
        {
            $this->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }
    }
    
    function sendTrackingNotificationEmail($trackingDetailAry)
    {
        if(!empty($trackingDetailAry))
        {
            $kUser = new cUser();
            $kConfig = new cConfig();
            $kBooking = new cBooking();
            $szTrackingNumber = $trackingDetailAry['szTrackingNumber']; 
            $szBookingRef = $trackingDetailAry['szBookingRef']; 
            $idBooking = $this->getBookingIdByTrackingNumber($szTrackingNumber,$szBookingRef); 
            $bookingDetailAry = $kBooking->getExtendedBookingDetails($idBooking);  
            
            $kTracking = new cTracking();
            $idCourierProvider = $kTracking->getIdCourierProviderByIdServiceProvider($bookingDetailAry['idServiceProvider']);
            
            $kConfigCountry = new cConfig();
            if($bookingDetailAry['idDestinationCountry']>0 && $idCourierProvider==__TNT_API__)
            {
                $kConfigCountry->loadCountry($bookingDetailAry['idDestinationCountry']);
                if(!empty($kConfigCountry->szCountryISO3Code))
                {
                    $szCountryISO3Code = ":".$kConfigCountry->szCountryISO3Code;
                } 
            }
            
            if($bookingDetailAry['iSendTrackingUpdates']==0)
            {
                return false;
            } 
            $szTagName = $trackingDetailAry['szTrackingTag'];  
            
            if($szTagName=='InTransit' && $bookingDetailAry['iInTransitEmailSend']==1)
            {
                 return false;
            }
            
           
            if(trim($szTagName) == "NEW_DELIVERY_DATE")
            {
                $section_title = "__COURIER_TRACKING_NEW_DELIVERED_DATE__";
            }
            else
            {
                $tagDetailAry = $this->get_aftership_tag_details($szTagName); 
                $section_title = $tagDetailAry['section_title'];
            } 
            
            
            if(!empty($section_title))
            {
                $replaceAry = array(); 
                if($bookingDetailAry['idServiceProvider']>0)
                {
                    $courierProviderName = $this->getCourierProviderNameById($bookingDetailAry['idServiceProvider']); 
                }
                else
                {
                    $courierProviderName = strtoupper($trackingDetailAry['szSlug']);
                } 
                
                if($bookingDetailAry['idServiceProvider']>0 && ($section_title=='__COURIER_TRACKING_PICKED_UP__' || $section_title=='__COURIER_TRACKING_OUT_FOR_DELIVERY__' || $section_title=='__COURIER_TRACKING_NEW_DELIVERED_DATE__'))
                {
                    $idCourierProvider = $this->getIdCourierProviderByIdServiceProvider($bookingDetailAry['idServiceProvider']);
                    
                    if($idCourierProvider>0)
                    {
                        $checkTimeArr=array();
                        $checkTimeArr['idCourierProvider']=$idCourierProvider;
                        $checkTimeArr['idCountry']=$bookingDetailAry['idConsigneeCountry'];
                        $checkTimeArr['szPostcode']=$bookingDetailAry['szConsigneePostCode'];                        
                        $resTimeArr=$this->getFromToTimeForCourierProvider($checkTimeArr);
                        
                    }
                }

                $kUser->getUserDetails($bookingDetailAry['idUser']);

                $replaceAry['szBookingRef'] = $szBookingRef;	
                $replaceAry['szFirstName'] = $kUser->szFirstName;
                $replaceAry['szLastName'] = $kUser->szLastName;
                $replaceAry['szCourierCompany'] = $courierProviderName;
                $replaceAry['szBookingReference'] = $szBookingRef;
                $replaceAry['iBookingLanguage'] = $bookingDetailAry['iBookingLanguage'];
                
                $szShipperCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDetailAry['idShipperCountry'],false,$bookingDetailAry['iBookingLanguage']);
                $szShipperCountry = $szShipperCountryAry[$bookingDetailAry['idShipperCountry']]['szCountryName'] ;
                
                $replaceAry['szShipperCompany'] = $bookingDetailAry['szShipperCompanyName'];
                $replaceAry['szShipperCountry'] = $szShipperCountry; 
                
                $szExpactedDeliveryText = ''; 
                if(!empty($bookingDetailAry['dtExpactedDelivery']) && $bookingDetailAry['dtExpactedDelivery']!='0000-00-00 00:00:00')
                {
                    $dtExpactedDelivery = $bookingDetailAry['dtExpactedDelivery'];
                    
                    $month = date('m',strtotime($dtExpactedDelivery));
                    $szMonth = getMonthName($bookingDetailAry['iBookingLanguage'],(int)$month,true);
                    $iDay = date('j',  strtotime($dtExpactedDelivery));
                    $dtExpactedDelivery = $iDay.". ".$szMonth;
                    
                    
                    $kConfig = new cConfig();
                    $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$bookingDetailAry['iBookingLanguage']);
                    $szExpactedDeliveryText_txt = $configLangArr[1]['szExpactedDeliveryText_txt'];
                    $szExpactedDeliveryTextForOutDeliveryEmail_txt = $configLangArr[1]['szExpactedDeliveryTextForOutDeliveryEmail_txt'];
                    $szExpactedDeliveryBetweenText_txt = $configLangArr[1]['szExpactedDeliveryBetweenText_txt'];
                    $szExpactedDeliveryAndText_txt = $configLangArr[1]['szExpactedDeliveryAndText_txt'];
                    $szClickHereText_txt = $configLangArr[1]['szClickHereText_txt'];
                    $szTrackShippingRefText_txt = $configLangArr[1]['szTrackShippingRefText_txt'];
                    $timeTextExpactedDelivery='';
                    $timeTextExpactedDeliveryDate='';
                    if(!empty($resTimeArr))
                    {
                        $iFromHour = str_pad($resTimeArr['iFromHour'], 2, '0', STR_PAD_LEFT);
                        $iFromMinute = str_pad($resTimeArr['iFromMinute'], 2, '0', STR_PAD_LEFT);
                        $iToHour = str_pad($resTimeArr['iToHour'], 2, '0', STR_PAD_LEFT);
                        $iToMinute = str_pad($resTimeArr['iToMinute'], 2, '0', STR_PAD_LEFT);                        
                        if($section_title=='__COURIER_TRACKING_NEW_DELIVERED_DATE__')
                        {
                            $timeTextExpactedDeliveryDate=" ".$szExpactedDeliveryBetweenText_txt." ".$iFromHour.":".$iFromMinute." ".$szExpactedDeliveryAndText_txt." ".$iToHour.":".$iToMinute."";
                        }
                        else{
                            $timeTextExpactedDelivery=" ".$szExpactedDeliveryBetweenText_txt." ".$iFromHour.":".$iFromMinute." ".$szExpactedDeliveryAndText_txt." ".$iToHour.":".$iToMinute."";
                        }
                    }
                    
                    if($section_title=='__COURIER_TRACKING_OUT_FOR_DELIVERY__')
                    {
                        if($bookingDetailAry['iBookingLanguage']==__LANGUAGE_ID_DANISH__)
                        {
                            $szExpactedDeliveryText = " ".$courierProviderName." ".$szExpactedDeliveryTextForOutDeliveryEmail_txt."".$timeTextExpactedDelivery.".";
                        }
                        else
                        {
                            $szExpactedDeliveryText = " ".$szExpactedDeliveryTextForOutDeliveryEmail_txt."".$timeTextExpactedDelivery.".";
                        }
                    }
                    else
                    {
                        if($bookingDetailAry['iBookingLanguage']==__LANGUAGE_ID_DANISH__)
                        {
                            $szExpactedDeliveryText = " ".$courierProviderName." ".$szExpactedDeliveryText_txt." ".$dtExpactedDelivery."".$timeTextExpactedDelivery.".";
                        }
                        else
                        {
                            $szExpactedDeliveryText = " ".$szExpactedDeliveryText_txt." ".$dtExpactedDelivery."".$timeTextExpactedDelivery.".";
                        }
                    }
                } 
                //echo $szExpactedDeliveryText;
                $replaceAry['szExpectedDeliveryText'] = $szExpactedDeliveryText;
                $replaceAry['szExpectedDeliveryDate'] = $dtExpactedDelivery."".$timeTextExpactedDeliveryDate;
                
                $replaceAry['szTrackingNumber']=$szTrackingNumber;
                $szConsigneeCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDetailAry['idConsigneeCountry'],false,$bookingDetailAry['iBookingLanguage']);
                $szConsigneeCountry = $szConsigneeCountryAry[$bookingDetailAry['idConsigneeCountry']]['szCountryName'] ;
                $replaceAry['szConsigneeCountry']=$szConsigneeCountry;
                $dtUpdatedAtAPIText='';
                
                if($trackingDetailAry['dtUpdatedAtAPI']!='')
                {
                    $dateUpdatedAtAPI = new DateTime($trackingDetailAry['dtUpdatedAtAPI']);
                    $dtUpdatedAtAPIDay=date('d',strtotime($trackingDetailAry['dtUpdatedAtAPI']));
                    $dtUpdatedAtAPIMonth=date('m',strtotime($trackingDetailAry['dtUpdatedAtAPI']));
                    $dtUpdatedAtAPIDateTime=$dateUpdatedAtAPI->format('Y-m-d H:i');
                    $dtUpdatedAtAPITime=date('H:i',strtotime($dtUpdatedAtAPIDateTime));
                     $szMonthName = getMonthName($bookingDetailAry['iBookingLanguage'],$dtUpdatedAtAPIMonth,true);
                     if($bookingDetailAry['iBookingLanguage']==2 || $bookingDetailAry['iBookingLanguage']==3)
                     {
                        $dtUpdatedAtAPIText = $dtUpdatedAtAPIDay.". ".$szMonthName." klokken ".$dtUpdatedAtAPITime;
                     }
                     else
                     {
                         $dtUpdatedAtAPIText = $dtUpdatedAtAPIDay.". ".$szMonthName." at ".$dtUpdatedAtAPITime;
                     }
                    
                }
              
                $replaceAry['szLastUpdateDate'] = $dtUpdatedAtAPIText;      
                $shipmentTrackURL = __AFTERSHIP_SHIPPMENT_TRACKING_LINKK__."/".$szTrackingNumber."".$szCountryISO3Code;
                
                
                $kConfig =new cConfig();
                $langArr=$kConfig->getLanguageDetails('',$bookingDetailAry['iBookingLanguage']);
                if(!empty($langArr) && $bookingDetailAry['iBookingLanguage']!=__LANGUAGE_ID_ENGLISH__)
                { 
                    $szLanguageCode=$langArr[0]['szLanguageCode'];
                    $szUnsubscribedValue = __BASE_STORE_URL__."/".$szLanguageCode."/myBooking/unsubscribe/".$bookingDetailAry['szBookingRandomNum']."/";
                    $szUnsubscribedLink="<a href='".$szUnsubscribedValue."'>".$szClickHereText_txt."</a>"; 
                    $shipmentTrackLink="<a href='".$shipmentTrackURL."'>".$szTrackShippingRefText_txt." ".$replaceAry['szBookingReference']."</a>";
                }
                else
                {  
                    $szUnsubscribedValue = __BASE_STORE_URL__."/myBooking/unsubscribe/".$bookingDetailAry['szBookingRandomNum']."/";
                    $szUnsubscribedLink="<a href='".$szUnsubscribedValue."'>".$szClickHereText_txt."</a>"; 
                    $shipmentTrackLink="<a href='".$shipmentTrackURL."'>".$szTrackShippingRefText_txt." ".$replaceAry['szBookingReference']."</a>";
                } 
                $replaceAry['szShipmentTrackLink'] = $shipmentTrackLink;
                $replaceAry['szShipmentTrackUrl'] = $shipmentTrackURL;
                $replaceAry['szUnsubscribedLink'] = $szUnsubscribedLink; 
                $replaceAry['iAdminFlag']='1';
                $replaceAry['idBooking']=$idBooking;
                $szToEmail = $bookingDetailAry['szEmail'];  
                createEmail($section_title, $replaceAry,$szToEmail, '', __STORE_SUPPORT_EMAIL__,$bookingDetailAry['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_CUSTOMER__,false,$idBooking,false,false,true,'',true); 
                
                /*
                *  Deprecated code block
                * 
                   $emailMessageAry = array(); 
                   $emailMessageAry = createEmailMessage($section_title,$replaceAry);

                   $szSubject = $emailMessageAry['szEmailSubject'];

                   $breaksAry = array("<br/>","<br>"); 
                   $szEmailBody = str_ireplace($breaksAry, "<br>".PHP_EOL, $emailMessageAry['szEmailBody']);  
                   sendQuatationEmail($szToEmail,'',$szSubject,$szEmailBody,__STORE_SUPPORT_EMAIL__,$bookingDetailAry['idUser'],$bookingDetailAry['id'],'',1);
                * 
                */
                if($bookingDetailAry['iInTransitEmailSend']==0 && $szTagName=='InTransit')
                {
                    $res_ary['iInTransitEmailSend'] = 1;
                }
                $res_ary['iTrackingEmailSend']='1';
                if(!empty($res_ary))
                {
                    $update_query = '';
                    foreach($res_ary as $key=>$value)
                    {
                        $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    } 
                    $update_query = rtrim($update_query,",");  
                    $kBooking->updateDraftBooking($update_query,$idBooking);
                }
                return true; 
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    
    function addCourierTrackingLogDetail($trackingDetailAry,$idBooking=false,$iFirstTimeCreated=false)
    {
        if(!empty($trackingDetailAry))
        {
            $szTrackingNumber = $trackingDetailAry['szTrackingNumber']; 
            $szBookingRef = $trackingDetailAry['szBookingRef'];  
            if($idBooking>0)
            {
                /*
                * We are calling this case when we actually adds data to after ship API
                */
            } 
            else
            {
                $idBooking = $this->getBookingIdByTrackingNumber($szTrackingNumber,$szBookingRef);  
            }    
            
            if(!empty($trackingDetailAry['szTrackingTag']))
            {
                $tagDetail = $this->get_aftership_tag_details($trackingDetailAry['szTrackingTag']);
                $trackingDetailAry['szTrackingTagDescription'] = $tagDetail['tag_message'];
            } 
            if($idBooking>0)
            {
                $query="
                    INSERT INTO
                        ".__DBC_SCHEMATA_COURIER_TRACKING_LOG__."
                    (
                        idBooking,
                        szTrackingNumber,
                        szTrackingID,
                        szSlug,
                        szTrackingTag,
                        szTrackingTagDescription, 
                        dtCreatedAtAPI,
                        dtUpdatedAtAPI,
                        dtExpactedDelivery,
                        dtShipmentDate,
                        szUniqueToken,
                        szCustomerName,
                        iCurrent,
                        dtCreatedOn,
                        iActive
                    )
                    VALUES
                    (
                        '".(int)$idBooking."',
                        '".mysql_escape_custom(trim($szTrackingNumber))."',
                        '".mysql_escape_custom(trim($trackingDetailAry['szTrackingID']))."',
                        '".mysql_escape_custom(trim($trackingDetailAry['szSlug']))."',
                        '".mysql_escape_custom(trim($trackingDetailAry['szTrackingTag']))."',
                        '".mysql_escape_custom(trim($trackingDetailAry['szTrackingTagDescription']))."',
                        '".mysql_escape_custom(trim($trackingDetailAry['dtCreatedAtAPI']))."',
                        '".mysql_escape_custom(trim($trackingDetailAry['dtUpdatedAtAPI']))."', 
                        '".mysql_escape_custom(trim($trackingDetailAry['dtExpactedDelivery']))."',
                        '".mysql_escape_custom(trim($trackingDetailAry['dtShipmentDate']))."', 
                        '".mysql_escape_custom(trim($trackingDetailAry['szUniqueToken']))."',
                        '".mysql_escape_custom(trim($trackingDetailAry['szCustomerName']))."',
                        '1',
                        NOW(),
                        '1'
                    )
                ";
               
                if($this->exeSQL($query))
                {
                    $idTrackingLog = $this->iLastInsertID; 
                    $this->changeTrackingLogCurrentStatus($idTrackingLog,$idBooking); 
                    $this->updateBookingTrackingTagStatus($idBooking,$trackingDetailAry['szTrackingTag'],$trackingDetailAry['szTrackingTagDescription']); 
                    $kBooking = new cBooking(); 
                    $postSearchAry = $kBooking->getExtendedBookingDetails($idBooking);
                    $dtExpactedDelivery = $postSearchAry['dtExpactedDelivery'];
                    
                    $res_ary = array();
                    if(!empty($trackingDetailAry['dtShipmentDate']))
                    {
                        $res_ary['dtShipmentDate'] = $trackingDetailAry['dtShipmentDate']; 
                        $res_ary['dtCutOff'] = $trackingDetailAry['dtShipmentDate'];
                        $res_ary['dtWhsCutOff'] = $trackingDetailAry['dtShipmentDate'];
                        
                        if(empty($postSearchAry['dtActualCutOff']) || $postSearchAry['dtActualCutOff']!='0000-00-00 00:00:00')
                        {
                            $res_ary['dtBookedCutOffDate'] = $postSearchAry['dtCutOff'];
                            $res_ary['dtBookedWhsCutOffDate'] = $postSearchAry['dtWhsCutOff'];
                        } 
                    }
                    if(!empty($trackingDetailAry['dtExpactedDelivery']))
                    {
                        $res_ary['dtExpactedDelivery'] = $trackingDetailAry['dtExpactedDelivery'];
                        $res_ary['dtAvailable'] = $trackingDetailAry['dtExpactedDelivery'];  
                        $res_ary['dtWhsAvailabe'] = $trackingDetailAry['dtExpactedDelivery'];  
                        
                        if(empty($postSearchAry['dtActualAvailable']) || $postSearchAry['dtActualAvailable']=='0000-00-00 00:00:00')
                        {
                            $res_ary['dtBookedWhsAvailabeDate'] = $postSearchAry['dtWhsAvailabe']; 
                            //$res_ary['dtBookedAvailableDate '] = $postSearchAry['dtAvailable']; 
                        } 
                        $iUpdateDeliveryDate = 1;
                    } 
                    
                    /*
                    * For UPS specifically, we send e-mail when we get the "tag" "In Transit" and "message" in the web hook is "Order Processed: Ready for UPS". 
                    * We will then send the mail for the next web hook we receive with "In Transit" for any other message.
                    */
                    $bSendTrackingEmail = false;
                    if($trackingDetailAry['szTrackingTag']=='InTransit' && strtolower($trackingDetailAry['szSlug'])=='ups')
                    {
                        $szOrderProcessedUps = 'Order Processed: Ready for UPS';
                        if(!empty($trackingDetailAry['checkpoints']) && is_array($trackingDetailAry['checkpoints']))
                        {
                            $checkPointsAry = $trackingDetailAry['checkpoints'];
                            foreach($checkPointsAry as $checkPointsArys)
                            {
                                if(!empty($checkPointsArys['message']))
                                {
                                    if(strtolower($checkPointsArys['message'])==strtolower($szOrderProcessedUps))
                                    {
                                        $bSendTrackingEmail = true;
                                        break;
                                    }
                                } 
                            }
                        }
                    }
                    else
                    {
                        //For FedEx and TNT this will as they are and we always updated this field with 1 
                        $bSendTrackingEmail = true;
                    }
                    $this->bSendTrackingEmail = 2;
                    if(strtolower($trackingDetailAry['szSlug'])=='tnt')
                    {
                        $checkPointTime=strtotime($trackingDetailAry['checkPointTime']);
                        $dtLabelSent=strtotime($postSearchAry['dtLabelSent']);
                        if($dtLabelSent>$checkPointTime)
                        {
                            $bSendTrackingEmail = false;
                        }                        
                    }
                    
                    
                    
                    if($bSendTrackingEmail && !$iFirstTimeCreated)
                    {
                        $res_ary['iShipmentPickedUp'] = 1;
                        $this->bSendTrackingEmail = 1;
                    }   
                    if($trackingDetailAry['szTrackingTag']=='InTransit' && strtolower($trackingDetailAry['szSlug'])=='tnt')
                    {
                        $szOrderProcessedTnt = 'Shipment presumed lost. Enquiries underway';
                        if(strtolower($trackingDetailAry['messageCheckPoint'])==strtolower($szOrderProcessedTnt))
                        {
                            $res_ary['iSendTrackingUpdates']=0;
                        }
                    
                    }
                    
                    
                    
                    if(!empty($res_ary))
                    {
                        $update_query = '';
                        foreach($res_ary as $key=>$value)
                        {
                            $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                        } 
                        $update_query = rtrim($update_query,",");  
                        $kBooking->updateDraftBooking($update_query,$idBooking);
                        
                        if($iUpdateDeliveryDate==1)
                        {
                            if(!empty($dtExpactedDelivery) && $dtExpactedDelivery!='0000-00-00 00:00:00')
                            {
                                $dtExpactedDeliveryDate = date('Y-m-d',strtotime($postSearchAry['dtExpactedDelivery']));
                                $dtExpactedDeliveryDateTime = strtotime($dtExpactedDeliveryDate);
                                
                                $dtNewExpactedDeliveryDate = date('Y-m-d',strtotime($trackingDetailAry['dtExpactedDelivery']));
                                $dtNewExpactedDeliveryDateTime = strtotime($dtNewExpactedDeliveryDate);
                                if($dtExpactedDeliveryDateTime!=$dtNewExpactedDeliveryDateTime)
                                {
                                    /*
                                    * Delivery date for shipment has been changed by Carrier
                                    */
                                    $trackingDetailAry['szTrackingTag'] = "NEW_DELIVERY_DATE";  
                                    $this->sendTrackingNotificationEmail($trackingDetailAry);
                                } 
                            }
                        } 
                    }
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        } 
    }
    
    function getBookingIdByTrackingNumber($szTrackingNumber,$szBookingRef)
    {
        if(!empty($szTrackingNumber) && !empty($szBookingRef))
        {
            $query="
                    SELECT
                            id
                    FROM
                            ".__DBC_SCHEMATA_BOOKING__."
                    WHERE
                            szBookingRef='".mysql_escape_custom(trim($szBookingRef))."'
                    AND
                            szTrackingNumber='".mysql_escape_custom(trim($szTrackingNumber))."'
                    ";
//            echo $query;
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $row = $this->getAssoc($result);
                    return $row['id'];
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    
    function changeTrackingLogCurrentStatus($idTrackingLog,$idBooking)
    {
        if($idTrackingLog>0 && $idBooking>0)
        {
            $query="
                UPDATE
                    ".__DBC_SCHEMATA_COURIER_TRACKING_LOG__."
                SET
                    iCurrent='0',
                    dtUpdatedOn=NOW()
                WHERE
                    id<>'".(int)$idTrackingLog."'
                AND
                    idBooking='".(int)$idBooking."'
            ";
            if($result=$this->exeSQL($query))
            {
                return true;
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    
    function updateBookingTrackingTagStatus($idBooking,$szTrackingTag,$szTrackingTagDescription)
    {
        if($idBooking>0)
        {
            $kBooking = new cBooking(); 
            
            $res_ary = array();
            $res_ary['szTrackingTag'] = $szTrackingTag;
            $res_ary['szTrackingTagDescription'] = $szTrackingTagDescription; 
            
            if(!empty($res_ary))
            {
                $update_query = '';
                foreach($res_ary as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                } 
                $update_query = rtrim($update_query,",");  
                if($kBooking->updateDraftBooking($update_query,$idBooking))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            } 
        }
        else
        {
            return false;
        }
    }
    function getCourierProviderNameById($idServiceProvider)
    {
        if($idServiceProvider>0)
        {
            $query="
                    SELECT 
                            id,
                            idCourierProvider
                    FROM
                            ".__DBC_SCHEMATA_COUIER_AGREEMENT_DATA__."
                    WHERE
                            id='".(int)$idServiceProvider."'
                    ";
            
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $row = $this->getAssoc($result);

                    $query2="
                            SELECT 
                                    szProviderName,
                                    szName
                            FROM
                                    ".__DBC_SCHEMATA_COUIER_PROVIDER__."
                            WHERE
                                    id='".(int)$row['idCourierProvider']."'
                            ";
                    if($result2=$this->exeSQL($query2))
                    {
                        if($this->iNumRows>0)
                        {
                            $row2 = $this->getAssoc($result2);
                            return $row2['szProviderName'];
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        $this->error = true;
                        $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                        $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    function getIdCourierProviderByIdServiceProvider($idServiceProvider)
    {
        if($idServiceProvider>0)
        {
            $query="
                    SELECT 
                            id,
                            idCourierProvider
                    FROM
                            ".__DBC_SCHEMATA_COUIER_AGREEMENT_DATA__."
                    WHERE
                            id='".(int)$idServiceProvider."'
                    ";
//            echo $query;
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $row = $this->getAssoc($result);
                    return $row['idCourierProvider'];
                }
                else
                {
                    return false;
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    function getBookingDetailByTrackingNumber($szTrackingNumber,$szBookingRef='')
    {
        if(!empty($szTrackingNumber))
        {
            $queryWhere='';
            if($szBookingRef!='')
            {
                $queryWhere="
                    AND
                        szBookingRef='".mysql_escape_custom($szBookingRef)."'
                ";
            }
            $query="
                SELECT
                    id,
                    szTrackingTag,
                    szTrackingTagDescription,
                    iShipmentPickedUp,
                    dtBookingConfirmed,
                    dtLabelSent
                FROM
                    ".__DBC_SCHEMATA_BOOKING__."
                WHERE
                    szTrackingNumber='".mysql_escape_custom(trim($szTrackingNumber))."'
                $queryWhere        
            ";
//            echo $query;
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $row = $this->getAssoc($result);
                    return $row;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    function updateOldAddNewAfterShipTracking($szTrackingNumber,$idBooking,$oldTrackingNumber)
    {
        if(empty($szTrackingNumber))
        {
            $this->tracking_error = true;
            $this->szAfterShipTrackingNotification = "Tracking number can't be empty";
            return false;
        }
        else
        {
            $trackingAddResponseAry = $this->createAfterShipTracking($idBooking,$szTrackingNumber); 
            
            if($this->tracking_error == true)
            {
                return false;
            }
            
            if($this->updateOldTrackingDetail($szTrackingNumber,$idBooking,$oldTrackingNumber))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
    }
    function updateOldTrackingDetail($newTrackingNumber,$idBooking,$oldTrackingNumber)
    {
        if($idBooking>0)
        {
            $kBooking = new cBooking(); 
            $bookingDetailAry = $kBooking->getBookingDetails($idBooking); 
            
            $idCourierProvider = $this->getIdCourierProviderByIdServiceProvider($bookingDetailAry['idServiceProvider']);
            
            if(!empty($bookingDetailAry))
            { 
                if(empty($oldTrackingNumber) && empty($newTrackingNumber))
                {
                    $this->tracking_error = true;
                    $this->szAfterShipTrackingNotification = "Tracking number can't be empty";
                    return false;
                } 
                
                $params['tracking']['tracking_number']=trim($oldTrackingNumber); 
                
                $kCourierServices = new cCourierServices(); 
                $courierProviderComanyAry = array();
                $courierProviderComanyAry = $kCourierServices->getCourierProviderDetails(); 

                $szProviderName = $courierProviderComanyAry[$idCourierProvider]['szName'];
                $params['tracking']['slug'] = strtolower($szProviderName);
/*
                if((int)$idCourierProvider == __FEDEX_API__)
                {
                    $params['tracking']['slug'] = "fedex";
                }
                else if((int)$idCourierProvider == __UPS_API__)
                {
                    $params['tracking']['slug'] = "ups";
                }
                else if((int)$idCourierProvider == __TNT_API__)
                {
                    $params['tracking']['slug'] = "tnt";
                }
 * 
 */
                
                $slug = $params['tracking']['slug'];
                
                $URL = 'https://api.aftership.com/v4/trackings/'.$slug.'/'.$oldTrackingNumber;
                
                $params['tracking']['title'] = "Changed to - ".$newTrackingNumber; 
                
                $idAftershipEventLog = $this->addAftershipEventLog($params); 
                
                $response = array(); 
                $data_string = json_encode($params);   
                
                $response = $this->get_afterShip_curl_result($URL,$data_string,'PUT');  

                $decoded_response = json_decode($response); 
                $finalAry = toArray($decoded_response);

                $this->updateAftershipEventLog($idAftershipEventLog,$finalAry,$finalAry['meta']['code']); 
                
                $resultAry = array(); 
                
                if($finalAry['meta']['code'] == 200)
                {
                    return true;
                }
                else
                {
                    $this->tracking_error=true; 
                    $resultAry['error']['code'] = $finalAry['meta']['code'];
                    $resultAry['error']['message'] = $finalAry['meta']['message'];
                    $this->szAfterShipTrackingNotification = $finalAry['meta']['message']; 
                    $this->tracking_error_array = $resultAry;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    
    function getFromToTimeForCourierProvider($data)
    {
        if(!empty($data))
        {
            $query="
                SELECT
                    iFromHour,
                    iFromMinute,
                    iToHour,
                    iToMinute
                FROM
                    ".__DBC_SCHEMATA_COURIER_DELIVERY_TIME__."
                WHERE
                    idCourierProvider='".(int)$data['idCourierProvider']."'
                AND
                    idCountry='".(int)$data['idCountry']."'
                AND
                    szPostcode='".(int)$data['szPostcode']."'
                LIMIT 0,1        
            ";
            
            if($result=$this->exeSQL($query))
            {
                if($this->iNumRows>0)
                {
                    $row = $this->getAssoc($result);
                    return $row;
                }
                else
                {
                    return array();
                }
            }
            else
            {
                return array();
            }
        }   
        else
        {
            return array();
        }
            
    }
    
    function checkInTransitEmailAlreadySend($szTagName,$idBooking,$szTrackingNumber)
    {
        $query="
            SELECT
                id
            FROM
                ".__DBC_SCHEMATA_COURIER_TRACKING_LOG__."
            WHERE
                szTrackingTag='".  mysql_escape_custom($szTagName)."'
            AND
                szTrackingNumber='".mysql_escape_custom($szTrackingNumber)."'
            AND
                idBooking='".(int)$idBooking."'        
            ";
        
        if($result=$this->exeSQL($query))
        {
            if($this->iNumRows>0)
            {
                return true;
            }   
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    
    
    function updateiInTransitEmailSend()
    {        
        $query="
            SELECT
                b.id AS idBooking,
                cb.dtCollection,
                cb.dtCollectionEndTime,
                b.szTrackingNumber,
                b.idFile,
                f.idFileOwner,
                b.szBookingRef,
                b.szTrackingTag
            FROM
                ".__DBC_SCHEMATA_FILE_LOGS__." AS f
            INNER JOIN
                ".__DBC_SCHEMATA_BOOKING__." AS b
            ON
                b.id = f.idBooking
            INNER JOIN
                ".__DBC_SCHEMATA_COURIER_BOOKING_DATA__." AS cb
            ON
                b.id = cb.idBooking
            WHERE
                f.iLabelStatus>='2'
            AND
                cb.dtCollection<>'0000-00-00'
            AND
                b.iSendTrackingUpdates='1'
        ";
        echo $query;
        if(($result = $this->exeSQL($query)))
        {
            if($this->getRowCnt()>0)
            { 
                $ctr=0;
                while($row = $this->getAssoc($result))
                {
                    $szTrackingTag=$row['szTrackingTag'];
                    $idBooking=$row['idBooking'];
                    if($szTrackingTag!='' && $szTrackingTag!='Pending')
                    {
                        $queryUpdate="
                            UPDATE
                                ".__DBC_SCHEMATA_BOOKING__."
                            SET 
                                iInTransitEmailSend = '1' 
                            WHERE
                                id = '".(int)$idBooking."'
                        ";
                        echo $queryUpdate."<br /><br />";
                       // $resultUpdate = $this->exeSQL( $queryUpdate );
                        
                    }
                }
            }
        }
    }
}