<?php
/**
 * This file is the container for partner api related functionality.
 * All functionality related to error message response from api should be contained in this class.
 *
 * responseError.class.php
 *
 * @copyright Copyright (C) 2015 Transporteca
 * @author Ajay
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

class cResponseError extends cDatabase
{  
    private static $status;
    private static $errorCode;
    public static $errorMessages;
    public static $serializedErrorMessages;
    /*
    * This is constructor of partner class which is reponsible to initiate data base connection and create instance of self
    */ 
    public function __construct($status, $message = null)
    { 
        parent::__construct();
    }

    public static function addMessage($errorMessageAry)
    { 
        if(!empty($errorMessageAry))
        {
            $szJsonEncodedString = json_encode($errorMessageAry);
            self::$errorMessages = $szJsonEncodedString;
            self::$serializedErrorMessages = serialize($errorMessageAry);
        } 
    } 
    public static function getMessages()
    {
        return self::$errorMessages;
    }
    public static function getSerializedMessages()
    {
        return self::$serializedErrorMessages;
    }

    public function getStatus()
    {
        return $this->status;
    } 
    
    public static function display_http_response_header($code = NULL) 
    { 
        if ($code !== NULL) 
        { 
            switch ($code) 
            {
                case 100: $text = 'Continue'; break;
                case 101: $text = 'Switching Protocols'; break;
                case 200: $text = 'OK'; break;
                case 201: $text = 'Created'; break;
                case 202: $text = 'Accepted'; break;
                case 203: $text = 'Non-Authoritative Information'; break;
                case 204: $text = 'No Content'; break;
                case 205: $text = 'Reset Content'; break;
                case 206: $text = 'Partial Content'; break;
                case 300: $text = 'Multiple Choices'; break;
                case 301: $text = 'Moved Permanently'; break;
                case 302: $text = 'Moved Temporarily'; break;
                case 303: $text = 'See Other'; break;
                case 304: $text = 'Not Modified'; break;
                case 305: $text = 'Use Proxy'; break;
                case 400: $text = 'Bad Request'; break;
                case 401: $text = 'Unauthorized'; break;
                case 402: $text = 'Payment Required'; break;
                case 403: $text = 'Forbidden'; break;
                case 404: $text = 'Not Found'; break;
                case 405: $text = 'Method Not Allowed'; break;
                case 406: $text = 'Not Acceptable'; break;
                case 407: $text = 'Proxy Authentication Required'; break;
                case 408: $text = 'Request Time-out'; break;
                case 409: $text = 'Conflict'; break;
                case 410: $text = 'Gone'; break;
                case 411: $text = 'Length Required'; break;
                case 412: $text = 'Precondition Failed'; break;
                case 413: $text = 'Request Entity Too Large'; break;
                case 414: $text = 'Request-URI Too Large'; break;
                case 415: $text = 'Unsupported Media Type'; break;
                case 500: $text = 'Internal Server Error'; break;
                case 501: $text = 'Not Implemented'; break;
                case 502: $text = 'Bad Gateway'; break;
                case 503: $text = 'Service Unavailable'; break;
                case 504: $text = 'Gateway Time-out'; break;
                case 505: $text = 'HTTP Version not supported'; break;
                default:
                    exit('Unknown http status code "' . htmlentities($code) . '"');
                break;
            } 
            $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');  
            header($protocol . ' ' . $code . ' ' . $text); 
            $GLOBALS['http_response_code'] = $code; 
        } else { 
            $code = (isset($GLOBALS['http_response_code']) ? $GLOBALS['http_response_code'] : 200); 
        } 
        return $code; 
    }
}
?>