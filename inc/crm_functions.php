<?php
if( !isset( $_SESSION ) )
{
    session_start();
}
/**
 * This file contains the functions that are generic for the store and can be used by all the php files in the store website.
 *
 * functiions.php
 * @copyright Copyright (C) 2016 Trnasporteca, LLC
 * @author Anil
 * @package Trnasporteca Development
 */
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/pdf_functions.php" ); 
require_once( __APP_PATH_CLASSES__ . "/database.class.php");
include_classes();

function display_crm_message_details($logEmailsArys,$kBooking,$szFromPage)
{
    $attachmentAry = array(); 
    $iAttachmentCount = 0;  
    if($logEmailsArys['iHasAttachments']==1 && !empty($logEmailsArys['szAttachmentFiles']))
    {
        $attachmentAry = unserialize($logEmailsArys['szAttachmentFiles']);
        $iAttachmentCount = count($attachmentAry);
    }  
    $idBookingFile = $kBooking->id;
    $idEmailLogs = $kBooking->idEmailLogs;
      
    $bDisplayDetails = false; 
    $bCallOperationNotes = false;  
    if($_POST['addCrmOperationAry']['idCrmEmailLogs']==$logEmailsArys['id'])
    {
        $bDisplayDetails = true; 
        if($_POST['addCrmOperationAry']['szOperationType']=='SAVE')
        {
            //$bCallOperationNotes = true;
        } 
    }
    else if($idEmailLogs==$logEmailsArys['id'] && $kBooking->szTaskStatus=='T210')
    {
        $bDisplayDetails = true;
    } 
    $szCommonUnseenClass = '';
    $bUpdateEmailSeenStatus = false; 
    if($logEmailsArys['iSeen']==0)
    {
        $szCommonUnseenClass = " unread-email unread-email-common-".$logEmailsArys['id'];
        $bUpdateEmailSeenStatus = true;
    } 
    $finalEmailAry = array();
    $finalEmailAry = getFormatedEmails($logEmailsArys);
    
    if($logEmailsArys['iDSTDifference']>0)
    {  
        $logEmailsArys['dtSent'] = date('Y-m-d H:i:s',strtotime("+".(int)$logEmailsArys['iDSTDifference']." HOUR",strtotime($logEmailsArys['dtSentOriginal'])));  
    }
    else
    {
        $logEmailsArys['dtSent'] = getDateCustom($gmailInboxArys['dtSent']);
        /*
        if(date('I')==1)
        {   
            $logEmailsArys['dtSent'] = date('Y-m-d H:i:s',strtotime("+2 HOUR",strtotime($logEmailsArys['dtSentOriginal'])));  
        }
        else
        {   
            $logEmailsArys['dtSent'] = date('Y-m-d H:i:s',strtotime("+1 HOUR",strtotime($logEmailsArys['dtSentOriginal'])));  
        }
         * 
         */
    }
    //display_crm_email_details('<?php echo $logEmailsArys['id']; ','<?php echo $idBookingFile; ');
    ?>
    <div class="clearfix email-fields-container" style="width:100%;">
        <span class="quote-field-container wd-10"><strong>From Email:</strong></span>
        <span class="quote-field-container wd-40"> 
            <?php echo $logEmailsArys['szToAddress']; ?>
        </span>
        <span class="quote-field-container wd-10"><strong>To Email:</strong></span>
        <span class="quote-field-container wd-40">
            <?php 
                if(count($finalEmailAry)>1)
                {
                    ?>
                    <span class="reassign-button-container" style="position:relative;"> 
                        <span onmouseover="showHideReAssignDrodown('<?php echo "crm-email-sender-list-container-".$logEmailsArys['id']; ?>','<?php echo $logEmailsArys['id']; ?>');" onmouseout="showHideReAssignDrodown('<?php echo "crm-email-sender-list-container-".$logEmailsArys['id']; ?>','<?php echo $logEmailsArys['id']; ?>');" >
                            <?php echo $logEmailsArys['szFromEmail']; ?>
                            <a href="javascript:void(0);" style="text-decoration:none;" onclick="$('crm-email-sender-list-container-<?php echo $logEmailsArys['id']; ?>').show();"><strong>+</strong></a>
                        </span>
                        <div style="display:none;" class="calender_more_popup " id="crm-email-sender-list-container-<?php echo $logEmailsArys['id']; ?>">   
                            <ul>   
                                <?php
                                    if(!empty($finalEmailAry))
                                    { 
                                        foreach($finalEmailAry as $finalEmailArys)
                                        { 
                                            ?>
                                            <li><?php echo $finalEmailArys; ?></li>
                                            <?php 
                                        }
                                    }
                                ?>
                            </ul>  
                        </div> 
                    </span>
                    <?php
                }
                else
                {
                    echo $logEmailsArys['szFromEmail'];
                }
            ?> 
        </span>
    </div> 
    <div class="clearfix email-fields-container<?php echo $szCommonUnseenClass;?>" style="width:100%;">
        <span class="quote-field-container wd-10"><strong>Subject:</strong></span>
        <span class="quote-field-container wd-40"><?php echo utf8_decode($logEmailsArys['szEmailSubject']); ?></span>
        <span class="quote-field-container wd-10"><strong>Received:</strong></span>
        <span class="quote-field-container wd-40">
            <?php echo date('d-M-Y H:i',strtotime($logEmailsArys['dtSent'])); ?> &nbsp;&nbsp; 
            <a href="javascript:void(0);" onclick="open_close_log('email_body_<?php echo $logEmailsArys['id']; ?>','<?php echo $logEmailsArys['id']; ?>');mark_crm_email_read('<?php echo $logEmailsArys['id']; ?>','<?php echo $idBookingFile; ?>');" id="email_view_details_link_<?php echo $logEmailsArys['id']; ?>">View Details</a> |
            <a href="javascript:void(0);" onclick="remove_crm_messages('<?php echo $logEmailsArys['id']; ?>','<?php echo $szFromPage ?>','<?php echo $idBookingFile; ?>')">Remove</a> 
        </span>
    </div> 
    <div class="clearfix email-fields-container" style="width:100%;"> 
        <div id="email_body_<?php echo $logEmailsArys['id']; ?>" style="display:none;" >
            <?php echo display_incoming_email_details($logEmailsArys,$idBookingFile,$szFromPage); ?>
        </div>
    </div> 
    <script>
        $('#email_body_html_<?php echo $logEmailsArys['id']; ?>').each(function() {
            $(this).find('a').attr('target','_blank');
        });
    </script>
    <div class="clearfix email-fields-container" style="width:100%;"><hr></div> 
    <script type="text/javascript">
        <?php if($bCallOperationNotes){ ?>
            display_crm_operations('<?php echo $idBookingFile; ?>','<?php echo $szFromPage; ?>','<?php echo $logEmailsArys['id']; ?>','CRM_NOTE');
        <?php  } if($bDisplayDetails){ ?> 
            open_close_log('email_body_<?php echo $logEmailsArys['id']; ?>','<?php echo $logEmailsArys['id']; ?>');
            <?php if($bUpdateEmailSeenStatus){ ?>
                mark_crm_email_read('<?php echo $logEmailsArys['id']; ?>','<?php echo $idBookingFile; ?>');
            <?php } ?>
        <?php }?>
    </script>
    <?php
} 

function display_incoming_email_details($logEmailsArys,$idBookingFile,$szFromPage=false)
{
    $iAttachmentCount = 0;  
    if($logEmailsArys['iHasAttachments']==1 && !empty($logEmailsArys['szAttachmentFiles']))
    {
        $attachmentAry = unserialize($logEmailsArys['szAttachmentFiles']);
        $iAttachmentCount = count($attachmentAry);
    }  
    ?>
    <div class="email-content" id="email_body_html_<?php echo $logEmailsArys['id']; ?>">
        <?php 
            if($logEmailsArys['id']<=64438 && __ENVIRONMENT__ == "LIVE")
            {
                $logEmailsArys['szEmailBody'] = trim($logEmailsArys['szEmailBody']);
                $logEmailsArys['szEmailBody'] = nl2br($logEmailsArys['szEmailBody']);
            }
            else if($logEmailsArys['id']<=11043 && __ENVIRONMENT__ == "DEV_LIVE")
            {
                $logEmailsArys['szEmailBody'] = trim($logEmailsArys['szEmailBody']);
                $logEmailsArys['szEmailBody'] = nl2br($logEmailsArys['szEmailBody']);
            }
            else if($logEmailsArys['id']<=600 && __ENVIRONMENT__ == "ANILDEV")
            {
                $logEmailsArys['szEmailBody'] = trim($logEmailsArys['szEmailBody']);
                $logEmailsArys['szEmailBody'] = nl2br($logEmailsArys['szEmailBody']);
            }
            else
            {
                $logEmailsArys['szEmailBody'] = trim($logEmailsArys['szEmailBody']);
            }
           
            
            /*$logEmailsArys['szEmailBody']  = preg_replace_callback("#(<br>){3,}#", "\\1", $logEmailsArys['szEmailBody'] );
            $logEmailsArys['szEmailBody'] = str_replace("</br>", "<br>", $logEmailsArys['szEmailBody']);
            $logEmailsArys['szEmailBody']  = preg_replace_callback("/(<br>\s*|<br \/>\s*){3,}/i", "<br><br>", $logEmailsArys['szEmailBody'] );*/
            $logEmailsArys['szEmailBody']  = str_replace("</br>", "", $logEmailsArys['szEmailBody']);
            if($logEmailsArys['iAlreadyUtf8Encoded']==1)
            {
                echo $logEmailsArys['szEmailBody']; 
            }
            else
            {
                echo utf8_decode($logEmailsArys['szEmailBody']);   
            }?>
        </div>
        <?php
            if($iAttachmentCount>0)
            {
                //echo '<br><p><strong>'.$iAttachmentCount." Attachments</strong></p><br>";
                echo '<br><br>';
                foreach($attachmentAry as $attachmentArys)
                {
                    ?>
                    <a href="<?php echo __BASE_URL_GMAIL_ATTACHMENT__."/".$logEmailsArys['iEmailNumber']."/".$attachmentArys; ?>" target="_blank"><?php echo ++$ctr.". ".$attachmentArys; ?></a><br>
                    <?php
                }
            }
        ?>
        <div class="clearfix dotted-line" style="width:100%;">&nbsp;</div>
        <div id="crm_buttons_main_container_<?php echo $logEmailsArys['id']; ?>" class="clearfix email-fields-container crm-button-main-container" style="width:100%;">
            <?php echo display_crm_email_buttons($logEmailsArys,$idBookingFile,$szFromPage); ?>
        </div>
    <?php
}

function display_crm_email_buttons($logEmailsArys,$idBookingFile,$szFromPage,$kCrm=false)
{   
    if(!empty($_POST['addCrmSnoozeAry']))
    {
        $addCrmSnoozeAry = $_POST['addCrmSnoozeAry'];
        $dtReminderDate = $addCrmSnoozeAry['dtReminderDate'];
        $szReminderTime = $addCrmSnoozeAry['szReminderTime'];
    }
    else
    {
        //$dtReminderDateTime = $logEmailsArys['dtSnoozeDate'];
        $remindDateAry = format_reminder_date_time($dtReminderDateTime);
        
        $dtReminderDate = $remindDateAry['dtReminderDate'];
        $szReminderTime = $remindDateAry['szReminderTime'];
    } 
    
    $kBooking = new cBooking();
    $kBooking->loadFile($idBookingFile);
    $idBooking = $kBooking->idBooking;
    $idCustomer = $kBooking->idUser; 
    
    $szFormId = "crm_email_snooze_form_".$logEmailsArys['id'];
    
    $activeAdminAry = array();
    $kAdmin = new cAdmin();
    $activeAdminAry = $kAdmin->getAllActiveStoreAdmin($_SESSION['admin_id'],true);  
    
    $kAdmin->getAdminDetails($_SESSION['admin_id']);
    $szOperatorName = $kAdmin->szFirstName." ".$kAdmin->szLastName;
    
    $finalEmailAry = array();
    $finalEmailAry = getFormatedEmails($logEmailsArys);
    
    $bReplyAllFlag = false;
    $szArrowClass = '';
    if(count($finalEmailAry)>1)
    {
        $bReplyAllFlag = true;
        $szReplyAllButtonText = "Reply All";
    }
    else
    {
        $szReplyAllButtonText = "Reply";
        $szArrowClass = ' crm-buttons-reply-all';
    } 
    $daftFlag=false;
    $daftFlagButtonClass="button1";    
    
    $kSendEmail = new cSendEmail(); 
    $logEmailsDraftArys = array();
    $logEmailsDraftArys = $kSendEmail->getEmailLogDetails($logEmailsArys['id']);
    
    if($logEmailsDraftArys['szDraftEmailBody']!='' || $logEmailsDraftArys['szDraftEmailSubject']!='')
    {
        $daftFlag=true;
        $daftFlagButtonClass="button2";
    }
    ?>
    <script type="text/javascript">
        $("#dtCrmReminderDate_"+'<?php echo $logEmailsArys['id']; ?>').datepicker();  
        var openDiv; 
            function toggleDiv(divID) {
                 $("#" + divID).fadeToggle(200, function() {
                     openDiv = $(this).is(':visible') ? divID : null;
                 });
            } 
            $(document).click(function(e) {
                 if (!$(e.target).closest('#'+openDiv).length) {
                     toggleDiv(openDiv);
                 }
            });  
    </script>  
    <form spellcheck="false" id="<?php echo $szFormId; ?>" name="<?php echo $szFormId; ?>" action="javascript:void(0);"> 
        <?php if($logEmailsArys['iIncomingEmail']==1){ ?>
            <div class="clearfix email-fields-container" style="width:100%;"> 
                <div class="quote-field-container wd-10">
                    <input type="text" name="addCrmSnoozeAry[dtReminderDate]" id="dtCrmReminderDate_<?php echo $logEmailsArys['id']; ?>" readonly value="<?php echo $dtReminderDate; ?>"> 
                </div>   
                <div class="quote-field-container wds-10">
                    <input type="text" maxlength="5" name="addCrmSnoozeAry[szReminderTime]" id="szCrmReminderTime_<?php echo $logEmailsArys['id']; ?>" onblur="format_reminder_time(this.value,3,'<?php echo $logEmailsArys['id']; ?>');" value="<?php echo $szReminderTime; ?>">  
                    <input type="hidden" name="addCrmSnoozeAry[idCrmEmailLogs]" id="idCrmEmailLogs" value="<?php echo $logEmailsArys['id']; ?>">    
                    <input type="hidden" name="addCrmSnoozeAry[idBookingFile]" id="idBookingFile" value="<?php echo $idBookingFile; ?>">
                    <input type="hidden" name="addCrmSnoozeAry[szFromPage]" id="szFromPage" value="<?php echo $szFromPage; ?>">
                </div>
                <div class="quote-field-container wds-80">
                    <ul class="clearfix crm-operation-button-container">  
                        <li><a href="javascript:void(0)" id="crm_email_buttons_<?php echo $logEmailsArys['id']."_CRM_SNOOZE"; ?>" class="<?php echo $daftFlagButtonClass;?> crm_email_buttons_<?php echo $logEmailsArys['id']; ?>" onclick="snooze_crm_email_message('<?php echo $szFormId; ?>','CRM_SNOOZE','<?php echo $logEmailsArys['id']; ?>');"><span>snooze</span></a></li>  
                        <li><a href="javascript:void(0)" id="crm_email_buttons_<?php echo $logEmailsArys['id']."_CRM_CLOSE"; ?>" class="<?php echo $daftFlagButtonClass;?> crm_email_buttons_<?php echo $logEmailsArys['id']; ?>" onclick="snooze_crm_email_message('<?php echo $szFormId; ?>','CRM_CLOSE','<?php echo $logEmailsArys['id']; ?>');"><span>close</span></a></li>
                        <li><a href="javascript:void(0)" id="crm_email_buttons_<?php echo $logEmailsArys['id']."_CRM_NOTE"; ?>" class="<?php echo $daftFlagButtonClass;?> crm_email_buttons_<?php echo $logEmailsArys['id']; ?>" onclick="display_crm_operations('<?php echo $idBookingFile; ?>','<?php echo $szFromPage; ?>','<?php echo $logEmailsArys['id']; ?>','CRM_NOTE');"><span>note</span></a></li>
                        <li><a href="javascript:void(0)" id="crm_email_buttons_<?php echo $logEmailsArys['id']."_CRM_LINK"; ?>" class="<?php echo $daftFlagButtonClass;?> crm_email_buttons_<?php echo $logEmailsArys['id']; ?>" onclick="display_crm_operations('<?php echo $idBookingFile; ?>','<?php echo $szFromPage; ?>','<?php echo $logEmailsArys['id']; ?>','CRM_LINK');"><span>link</span></a></li>
                        <li><a href="javascript:void(0)" id="crm_email_buttons_<?php echo $logEmailsArys['id']."_CRM_CREATE_USER"; ?>" class="<?php echo $daftFlagButtonClass;?> crm_email_buttons_<?php echo $logEmailsArys['id']; ?>" onclick="display_crm_operations('<?php echo $idBookingFile; ?>','<?php echo $szFromPage; ?>','<?php echo $logEmailsArys['id']; ?>','CRM_CREATE_USER');"><span><?php if($idCustomer>0){ echo "Update User"; } else { echo "create user"; }?></span></a></li>
                        <li class="reassign-button-container" onmouseover="showHideReAssignDrodown('<?php echo "crm-reassign-owner-list-container-".$logEmailsArys['id']; ?>','<?php echo $logEmailsArys['id']; ?>');" onmouseout="showHideReAssignDrodown('<?php echo "crm-reassign-owner-list-container-".$logEmailsArys['id']; ?>','<?php echo $logEmailsArys['id']; ?>');">
                            <a href="javascript:void(0)" id="crm_email_buttons_<?php echo $logEmailsArys['id']."_CRM_REASSIGN"; ?>" class="<?php echo $daftFlagButtonClass;?> crm_email_buttons_<?php echo $logEmailsArys['id']; ?>"><span>re-assign</span></a> 
                            <div style="display:none;" class="calender_more_popup" id="crm-reassign-owner-list-container-<?php echo $logEmailsArys['id']; ?>">  
                                <input type="hidden" name="idFileForClose" id="idFileForClose" value=""> 
                                <input type="hidden" name="idFileForCloseCount" id="idFileForCloseCount" value="0">
                                <input type="hidden" name="idFileForCloseCtr" id="idFileForCloseCtr" value="">
                                <img class="calender_more_popup_arrow" alt="arrow" src="<?php echo __BASE_STORE_IMAGE_URL__?>/calender_more_popup-arrow.png">  
                                <input type="hidden" name="transportecaTeamAry[idAdmin][0]" id="idAdmin_0" value="<?php echo $_SESSION['admin_id']; ?>"> 
                                <ul style="cursor:pointer;">  
                                    <li class="user-tray-popup-li" onclick="update_file_owner('0','<?php echo $logEmailsArys['id']; ?>','<?php echo $idBookingFile; ?>');"><?php echo "Not Allocated"; ?></li> 
                                    <li class="user-tray-popup-li" onclick="update_file_owner('<?php echo $_SESSION['admin_id']; ?>','<?php echo $logEmailsArys['id']; ?>','<?php echo $idBookingFile; ?>');"><?php echo $szOperatorName; ?></li> 
                                    <?php
                                        if(!empty($activeAdminAry))
                                        {
                                            $counter = 2;
                                            $transportecaTeamAry = $_SESSION['sessTransportecaTeamAry'];
                                            foreach($activeAdminAry as $activeAdminArys)
                                            { 
                                                ?>
                                                <li onclick="update_file_owner('<?php echo $activeAdminArys['id']; ?>','<?php echo $logEmailsArys['id']; ?>','<?php echo $idBookingFile; ?>');"><?php echo $activeAdminArys['szFirstName']." ".$activeAdminArys['szLastName']; ?></li>
                                                <?php
                                                $counter++;
                                            }
                                        }
                                    ?>
                                </ul>  
                            </div> 
                        </li> 
                        <li class="reply-button-container" onmouseover="showHideReplyButtons('<?php echo "crm_operation_reply_options_container_".$logEmailsArys['id']; ?>','<?php echo $logEmailsArys['id']; ?>','SHOW');" onmouseout="showHideReplyButtons('<?php echo "crm_operation_reply_options_container_".$logEmailsArys['id']; ?>','<?php echo $logEmailsArys['id']; ?>','HIDE');"> 
                            <?php if($bReplyAllFlag){ ?>
                                    <a href="javascript:void(0)" id="crm_email_buttons_<?php echo $logEmailsArys['id']."_CRM_REPLY_ALL"; ?>" class="<?php echo $daftFlagButtonClass;?> crm-buttons-reply-all crm_email_buttons_<?php echo $logEmailsArys['id']; ?>" onclick="display_crm_operations('<?php echo $idBookingFile; ?>','<?php echo $szFromPage; ?>','<?php echo $logEmailsArys['id']; ?>','CRM_REPLY_ALL');"><span>Reply All</span></a>
                            <?php } else { ?>
                                    <a href="javascript:void(0)" id="crm_email_buttons_<?php echo $logEmailsArys['id']."_CRM_REPLY"; ?>" class="button1<?php echo $szArrowClass; ?> crm_email_buttons_<?php echo $logEmailsArys['id']; ?>" onclick="display_crm_operations('<?php echo $idBookingFile; ?>','<?php echo $szFromPage; ?>','<?php echo $logEmailsArys['id']; ?>','CRM_REPLY');"><span>Reply</span></a>
                            <?php } ?>
                            <div style="display:none;" id="crm_operation_reply_options_container_<?php echo $logEmailsArys['id']; ?>">   
                                <?php if($bReplyAllFlag){ ?>
                                    <a href="javascript:void(0)" id="crm_email_buttons_<?php echo $logEmailsArys['id']."_CRM_REPLY"; ?>" class="button1<?php echo $szArrowClass; ?> crm_email_buttons_<?php echo $logEmailsArys['id']; ?>" onclick="display_crm_operations('<?php echo $idBookingFile; ?>','<?php echo $szFromPage; ?>','<?php echo $logEmailsArys['id']; ?>','CRM_REPLY');"><span>Reply</span></a>
                                <?php } ?>
                                <a href="javascript:void(0)" id="crm_email_buttons_<?php echo $logEmailsArys['id']."_CRM_FORWARD"; ?>" class="<?php echo $daftFlagButtonClass;?> crm_email_buttons_<?php echo $logEmailsArys['id']; ?>" onclick="display_crm_operations('<?php echo $idBookingFile; ?>','<?php echo $szFromPage; ?>','<?php echo $logEmailsArys['id']; ?>','CRM_FORWARD');"><span>Forward</span></a>
                                <a href="javascript:void(0)" id="crm_email_buttons_<?php echo $logEmailsArys['id']."_CRM_NEW_EMAIL"; ?>" class="<?php echo $daftFlagButtonClass;?> crm_email_buttons_<?php echo $logEmailsArys['id']; ?>" onclick="display_crm_operations('<?php echo $idBookingFile; ?>','<?php echo $szFromPage; ?>','<?php echo $logEmailsArys['id']; ?>','CRM_NEW_EMAIL');"><span>NEW</span></a>
                            </div>
                        </li>
                    </ul>
                     <?php 
                    if($kCrm->arErrorMessages['szTaskStatusCheckingFlag']!='')
                    {
                    ?>
                        <span style="color:red;"><br /><?php echo $kCrm->arErrorMessages['szTaskStatusCheckingFlag'];?></span>
                    <?php }?>
                </div>
                
            </div>
       
        <?php } else {?> 
            <div class="clearfix email-fields-container" style="width:100%;">
                <ul class="clearfix crm-operation-button-container">
                    <li class="reply-button-container" onmouseover="showHideReAssignDrodown('<?php echo "crm_operation_reply_options_container_".$logEmailsArys['id']; ?>','<?php echo $logEmailsArys['id']; ?>');" onmouseout="showHideReAssignDrodown('<?php echo "crm_operation_reply_options_container_".$logEmailsArys['id']; ?>','<?php echo $logEmailsArys['id']; ?>');" style="float:right;width:11.424%;"> 
                        <?php if($bReplyAllFlag){ ?>
                                <a href="javascript:void(0)" id="crm_email_buttons_<?php echo $logEmailsArys['id']."_CRM_REPLY_ALL"; ?>" class="<?php echo $daftFlagButtonClass;?> crm-buttons-reply-all crm_email_buttons_<?php echo $logEmailsArys['id']; ?>" onclick="display_crm_operations('<?php echo $idBookingFile; ?>','<?php echo $szFromPage; ?>','<?php echo $logEmailsArys['id']; ?>','CRM_REPLY_ALL');"><span>Reply All</span></a>
                        <?php } else { ?>
                                <a href="javascript:void(0)" id="crm_email_buttons_<?php echo $logEmailsArys['id']."_CRM_REPLY"; ?>" class="button1<?php echo $szArrowClass; ?> crm_email_buttons_<?php echo $logEmailsArys['id']; ?>" onclick="display_crm_operations('<?php echo $idBookingFile; ?>','<?php echo $szFromPage; ?>','<?php echo $logEmailsArys['id']; ?>','CRM_REPLY');"><span>Reply</span></a>
                        <?php } ?>
                        <div style="display:none;" id="crm_operation_reply_options_container_<?php echo $logEmailsArys['id']; ?>">   
                            <?php if($bReplyAllFlag){ ?>
                                <a href="javascript:void(0)" id="crm_email_buttons_<?php echo $logEmailsArys['id']."_CRM_REPLY"; ?>" class="button1<?php echo $szArrowClass; ?> crm_email_buttons_<?php echo $logEmailsArys['id']; ?>" onclick="display_crm_operations('<?php echo $idBookingFile; ?>','<?php echo $szFromPage; ?>','<?php echo $logEmailsArys['id']; ?>','CRM_REPLY');"><span>Reply</span></a>
                            <?php } ?>
                            <a href="javascript:void(0)" id="crm_email_buttons_<?php echo $logEmailsArys['id']."_CRM_FORWARD"; ?>" class="<?php echo $daftFlagButtonClass;?> crm_email_buttons_<?php echo $logEmailsArys['id']; ?>" onclick="display_crm_operations('<?php echo $idBookingFile; ?>','<?php echo $szFromPage; ?>','<?php echo $logEmailsArys['id']; ?>','CRM_FORWARD');"><span>Forward</span></a>
                            <a href="javascript:void(0)" id="crm_email_buttons_<?php echo $logEmailsArys['id']."_CRM_NEW_EMAIL"; ?>" class="<?php echo $daftFlagButtonClass;?> crm_email_buttons_<?php echo $logEmailsArys['id']; ?>" onclick="display_crm_operations('<?php echo $idBookingFile; ?>','<?php echo $szFromPage; ?>','<?php echo $logEmailsArys['id']; ?>','CRM_NEW_EMAIL');"><span>NEW</span></a>
                        </div>
                    </li>
                </ul>
            </div>
        <?php }?> 
    </form>
    <!--onclick="showHide('crm-reassign-owner-list-container-<?php echo $logEmailsArys['id']; ?>');"-->
    <div id="crm_buttons_operations_main_container_<?php echo $logEmailsArys['id']; ?>" <?php if(!$daftFlag){?> style="display:none;" <?php }?>>
    <?php 
    if($daftFlag)
    {
          echo display_pending_tray_crm_emails_operation($kCrm,'CRM_REPLY',$logEmailsArys,$idBookingFile,$szFromPage);  
    }?>
    </div>
    <?php 
    if(!empty($kCrm->arErrorMessages))
    {   
        display_crm_errors($kCrm->arErrorMessages,$szFormId,$logEmailsArys['id']);  
    }
} 
function display_pending_tray_crm_emails_operation($kCrm,$szAction,$logEmailsArys,$idBookingFile,$szFromPage,$idRemindEmailTemplate=false,$iTemplateLanguage=false,$szCrmEmail=false)
{
    $kBooking = new cBooking();
    $kBooking->loadFile($idBookingFile);
    $idBooking = $kBooking->idBooking;
    $idCustomer = $kBooking->idUser;
    $iLabelStatus = $kBooking->iLabelStatus;
    $szFormId = "crm_email_operations_form_".$logEmailsArys['id'];
    
    if($szAction=='CRM_REPLY' || $szAction=='CRM_REPLY_ALL' || $szAction=='CRM_FORWARD' || $szAction=='CRM_NEW_EMAIL')
    {
        $szFormAction = __BASE_MANAGEMENT_URL__."/uploadAttachment.php";
    }
    else
    {
        $szFormAction = 'javascript:void(0)';
    }  
   
    if((int)$kBooking->iLabelStatus==1){
    if($kBooking->szTaskStatus=='T140')
    {
        $szTaskStatus=$kBooking->szTaskStatus;
    }
    else
    {
        if($kBooking->szForwarderBookingTask=='T140'){
        $szTaskStatus=$kBooking->szForwarderBookingTask;
        }
    }}
    ?> 
    <div class="clearfix grey-line" style="width:100%;">&nbsp;</div> 
    <form spellcheck="false" id="<?php echo $szFormId; ?>" name="<?php echo $szFormId; ?>" action="<?php echo $szFormAction; ?>" enctype="multipart/form-data" method="post" > 
        <div id="crm_buttons_operations_container_<?php echo $logEmailsArys['id']; ?>">
            <?php  
                if($szAction=='CRM_NOTE')
                {
                    echo display_crm_operations_notes_fields($kCrm,$logEmailsArys);
                }
                else if($szAction=='CRM_LINK')
                {
                    echo display_crm_operations_file_link_fields($kCrm,$kBooking,$logEmailsArys);
                }
                else if($szAction=='CRM_CREATE_USER')
                {
                    echo display_crm_operations_create_crm_user_fields($kCrm,$kBooking,$logEmailsArys,$szCrmEmail);
                }
                else if($szAction=='CRM_REPLY' || $szAction=='CRM_REPLY_ALL' || $szAction=='CRM_FORWARD' || $szAction=='CRM_NEW_EMAIL')
                {
                    echo display_crm_operations_reply_email($kCrm,$kBooking,$logEmailsArys,$szAction,$szFromPage,$idRemindEmailTemplate,$iTemplateLanguage);
                } 
            ?>
        </div> 
        <input type="hidden" name="addCrmOperationAry[idCrmEmailLogs]" id="idCrmEmailLogs_<?php echo $logEmailsArys['id']; ?>" value="<?php echo $logEmailsArys['id']; ?>">    
        <input type="hidden" name="addCrmOperationAry[idBookingFile]" id="idBookingFile_<?php echo $logEmailsArys['id']; ?>" value="<?php echo $idBookingFile; ?>">
        <input type="hidden" name="addCrmOperationAry[szFromPage]" id="szFromPage_<?php echo $logEmailsArys['id']; ?>" value="<?php echo $szFromPage; ?>">
        <input type="hidden" name="addCrmOperationAry[szAction]" id="szAction_<?php echo $logEmailsArys['id']; ?>" value="<?php echo $szAction; ?>">
        <input type="hidden" name="addCrmOperationAry[idCustomer]" id="idCustomer_<?php echo $logEmailsArys['id']; ?>" value="<?php echo $idCustomer; ?>"> 
        <input type="hidden" name="addCrmOperationAry[iLabelStatus]" id="iLabelStatus_<?php echo $logEmailsArys['id']; ?>" value="<?php echo $iLabelStatus; ?>"> 
        <input type="hidden" name="addCrmOperationAry[szTaskStatusRemind]" id="szTaskStatusRemind_<?php echo $logEmailsArys['id']; ?>" value="<?php echo $szTaskStatus; ?>"> 
    </form>
    <span id="crm_operations_scrolling_div_container_<?php echo $logEmailsArys['id']; ?>"></span> 
    <?php if($szAction=='CRM_NOTE'){ ?>
        <script type="text/javascript">
            enableCrmSnoozeButton('<?php echo $szFormId; ?>',1,'<?php echo $logEmailsArys['id']; ?>');
        </script>
    <?php  } ?>
    
    <?php 
}

function display_pending_tray_email_logs($kBooking,$szFromPage)
{
    if($szFromPage=='PENDING_TRAY_LOG')
    {
        echo "LOG||||";
        echo display_task_logs_container($kBooking);
    }
    else
    {
        echo "CUSTOMER_LOG||||";
        echo display_task_customer_logs_container($kBooking); 
    } 
} 
function display_crm_operations_notes_fields($kCrm,$logEmailsArys)
{
    if(!empty($_POST['addCrmOperationAry']))
    {
        $addCrmSnoozeAry = $_POST['addCrmOperationAry'];
        $dtReminderDate = $addCrmSnoozeAry['dtReminderDate'];
        $szReminderTime = $addCrmSnoozeAry['szReminderTime'];
        $szReminderNotes = $addCrmSnoozeAry['szReminderNotes'];
    }
    else
    {
        //$dtReminderDateTime = $logEmailsArys['dtSnoozeDate'];
        $remindDateAry = format_reminder_date_time($dtReminderDateTime);
        
        $dtReminderDate = $remindDateAry['dtReminderDate'];
        $szReminderTime = $remindDateAry['szReminderTime'];
    } 
    $szFormId = "crm_email_operations_form_".$logEmailsArys['id'];
    ?>
    <script type="text/javascript">
        $("#dtReminderDate").datepicker();   
    </script>
    <div class="clearfix email-fields-container" style="width:100%;">  
        <textarea style="min-height: 100px;" name="addCrmOperationAry[szReminderNotes]" onkeyup="enableCrmSnoozeButton(this.form.id,1,'<?php echo $logEmailsArys['id']; ?>');" onblur="enableCrmSnoozeButton(this.form.id,1,'<?php echo $logEmailsArys['id']; ?>');" placeholder="Reminder Notes" id="szReminderNotes"><?php echo $szReminderNotes; ?></textarea> 
    </div>   
    <div class="clearfix email-fields-container" style="width:100%;">  
        <span class="quote-field-container wd-15">  
            <input type="text" name="addCrmOperationAry[dtReminderDate]" id="dtReminderDate" onkeyup="enableCrmSnoozeButton(this.form.id,1,'<?php echo $logEmailsArys['id']; ?>');" onblur="enableCrmSnoozeButton(this.form.id,1,'<?php echo $logEmailsArys['id']; ?>');" readonly value="<?php echo $dtReminderDate; ?>"> 
        </span> 
        <span class="quote-field-container wds-15">
            <input type="text" maxlength="5" name="addCrmOperationAry[szReminderTime]" onkeyup="enableCrmSnoozeButton(this.form.id,1,'<?php echo $logEmailsArys['id']; ?>');" id="szReminderTime" onblur="format_reminder_time(this.value);enableCrmSnoozeButton(this.form.id,1,'<?php echo $logEmailsArys['id']; ?>');" value="<?php echo $szReminderTime; ?>">   
        </span>
        <span class="quote-field-container wds-5" style="float: left;color:#999;font-style: italic;padding: 2px 0 0;width: 5%;">(hh:mm)</span>    
        <span class="quote-field-container wds-20">
            <a href="javascript:void(0);" id="request_reminder_snooze_button" style="float:left;opacity:0.4;" class="button1"><span>Save Snooze</span></a>
        </span>
        <span class="quote-field-container wds-45" style="text-align:right;">
            <a href="javascript:void(0);" id="request_reminder_close_task_button" class="button1" onclick="submit_crm_snooze_form('<?php echo $szFormId; ?>','CLOSE_TASK');"><span>Save & Close</span></a> 
            <a href="javascript:void(0);" id="request_reminder_save_button" style="opacity:0.4;" class="button1" ><span>Save</span></a> 
           <?php
           if($kCrm->arErrorMessages['szTaskStatusCheckingFlag']!='')
            {
            ?>
                <span style="color:red;"><br /><?php echo $kCrm->arErrorMessages['szTaskStatusCheckingFlag'];?></span>
            <?php }?>
        </span>
    </div>  
    <?php 
    if(!empty($kCrm->arErrorMessages))
    {  
        display_form_validation_error_message($szFormId,$kCrm->arErrorMessages);
    }
}

function display_crm_operations_file_link_fields($kCrm,$kBooking=false,$logEmailsArys=array())
{
    if(!empty($_POST['addCrmOperationAry']))
    {
        $idCustomer = $_POST['addCrmOperationAry']['idCustomer'];
    }
    else
    {
        $idCustomer = $kBooking->idUser; 
    }  
    $searchTaskAry = array();  
    $searchTaskAry['iFromPopup'] = 1 ; 
    if($idCustomer>0)
    {
        $searchTaskAry['idCustomer'] = $idCustomer;
    }  
    $searchTaskAry['idCRMEmailLogs'] = $logEmailsArys['id'];
    $szFormId = "crm_email_operations_form_".$logEmailsArys['id'];
    ?> 
    <div id="pending_task_searched_list_container_<?php echo $logEmailsArys['id']; ?>">
        <?php echo display_all_recent_task_by_operator($searchTaskAry,'CRM_FILE_LINKS',$kCrm,1); ?>
    </div>
    <div id="crm_operations_link_search_form">
        <div class="clearfix email-fields-container" style="width:100%;">  
            <span class="quote-field-container wd-25">  
                <a href="javascript:void(0);" id="pending_task_show_more_files_<?php echo $logEmailsArys['id']; ?>" onclick="show_more_files_search('<?php echo $szFormId; ?>','1','<?php echo $logEmailsArys['id']; ?>');" class="button1">Show more files</a> 
                <input type="hidden" name="addCrmOperationAry[iPageCounter]" id="iPageCounter_<?php echo $logEmailsArys['id']; ?>" value="1">
                <input type="hidden" name="addCrmOperationAry[iButtonAlreadyClicked]" id="iButtonAlreadyClicked" value="0">
            </span> 
            <span class="quote-field-container wd-60">
                <input type="text" name="addCrmOperationAry[szSearchField]" id="szSearchField" style="width: 70%;" onkeyup="return crm_operation_search_on_enter_key_press(event,'<?php echo $szFormId; ?>','SEARCH_FILE','<?php echo $logEmailsArys['id']; ?>');" value="<?php echo $szSearchField; ?>"> 
                <a href="javascript:void(0);" id="crm_operation_link_file_search_<?php echo $logEmailsArys['id']; ?>" onclick="submit_crm_snooze_form('<?php echo $szFormId; ?>','SEARCH_FILE','<?php echo $logEmailsArys['id']; ?>');" class="button1"><span>Search</span></a> 
            </span>
            <span class="quote-field-container wd-15" style="text-align:right;">
                <a href="javascript:void(0);" id="crm_operation_link_to_customer" class="button1" onclick="submit_crm_snooze_form('<?php echo $szFormId; ?>','CRM_EMAIL_LINK_CUSTOMER');"><span>Link to customer</span></a> 
            </span>
        </div>
    </div>
    <?php
    
    if(!empty($kCrm->arErrorMessages))
    { 
        display_crm_errors($kCrm->arErrorMessages,$szFormId,$logEmailsArys['id']);
    }
}

function format_reminder_date_time($dtReminderDateTime)
{
    if(!empty($dtReminderDateTime) && $dtReminderDateTime!='0000-00-00 00:00:00')
    { 
        if(date('I')==1)
        {
            //$dtReminderDateTime = date('Y-m-d H:i:s',strtotime("+1 HOUR",strtotime($dtReminderDateTime)));  
        } 
        $szCustomerTimeZone = $_SESSION['szCustomerTimeZoneGlobal'] ; 
        $date = new DateTime($dtReminderDateTime); 
        $date->setTimezone(new DateTimeZone($szCustomerTimeZone));   
        $dtReminderDate = $date->format('d/m/Y');
        $szReminderTime = $date->format('H:i');   
    }
    else
    {
        $iWeekDay = date('w');  
        //Default snooze time for automatic booking is 2 hours and for RFQ 1 days.

        if($iWeekDay==0 || $iWeekDay==6) //0: Sunday, 6: Saturday
        {
            $dtReminderDate = date("d/m/Y",strtotime("NEXT MONDAY"));
            $szReminderTime = date("H:i");
        }
        else
        { 
            $dtReminderDate = date("d/m/Y",strtotime("+3 HOUR"));
            $szReminderTime = date("H:i",strtotime("+3 HOUR")); 
            /*
            if(date('I')==1)
            {
                $dtReminderDate = date("d/m/Y",strtotime("+3 HOUR"));
                $szReminderTime = date("H:i",strtotime("+3 HOUR")); 
            } 
            else
            {
                $dtReminderDate = date("d/m/Y",strtotime("+2 HOUR"));
                $szReminderTime = date("H:i",strtotime("+2 HOUR")); 
            } 
             * 
             */
        } 
    }
    $ret_ary = array();
    $ret_ary['dtReminderDate'] = $dtReminderDate;
    $ret_ary['szReminderTime'] = $szReminderTime;
    return $ret_ary;
}

function display_link_customer_popup($emailInputAry)
{ 
    if(!empty($_POST['linkCustomerPopupAry']))
    {
        $szCrmEmail = $_POST['linkCustomerPopupAry']['szCrmEmail'];
        $idCrmEmail = $_POST['linkCustomerPopupAry']['idCrmEmail'];
        $szFromPage = $_POST['linkCustomerPopupAry']['szFromPage'];
        $idBookingFile = $_POST['linkCustomerPopupAry']['idBookingFile'];
    }
    else
    {  
        $idCrmEmail = $emailInputAry['idCrmEmail'];
        $szFromPage = $emailInputAry['szFromPage'];
        $idBookingFile = $emailInputAry['idBookingFile']; 
    } 
?>
    <div id="popup-bg" onclick="showHide('content_body')"></div>
    <div id="popup-container">
        <div class="popup contact-popup" style="width:500px;"> 
            <form id="link_customer_popup_form" name="link_customer_popup_form" action="javascript:void(0);">
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:75%;">
                        <input type="text" name="linkCustomerPopupAry[szSearchString]" id="szSearchString" placeholder="" value="<?php echo $szCrmEmail; ?>" onkeyup="submit_link_customer_by_enter(event,'link_customer_popup_form','<?php echo $idCrmEmail; ?>')" onfocus="check_form_field_not_required_courier(this.form.id,this.id);">
                        <input type="hidden" name="linkCustomerPopupAry[idCrmEmail]" id="idCrmEmail" value="<?php echo $idCrmEmail; ?>"> 
                        <input type="hidden" name="linkCustomerPopupAry[szFromPage]" id="szFromPage" value="<?php echo $szFromPage; ?>">
                        <input type="hidden" name="linkCustomerPopupAry[idBookingFile]" id="idBookingFile" value="<?php echo $idBookingFile; ?>">
                    </span>
                    <span class="quote-field-container" style="width:25%;">
                        <a href="javascript:void(0);" class="button1" id="link_customer_search_button" onclick="submit_link_customer_popup_form('link_customer_popup_form','<?php echo $idCrmEmail; ?>')"><span id="remove_crm_email_button_span">Search</span></a>			 
                    </span>
                </div>  
            </form>  
            <div class="clearfix dotted-line" style="width:100%;">&nbsp;</div>
            <div id="link_customer_list_container">
                <?php echo display_link_customer_listing($linkCustomerAry,$idCrmEmail,$szFromPage,$idBookingFile); ?>
            </div>
            <div class="btn-container" style="text-align: center;padding:10px 0;">
                <a href="javascript:void(0)" onclick="showHide('content_body')" class="button1"><span>Close</span></a>
            </div>
        </div>
    </div> 
    <?php
}

function display_cms_operation_replay_add_email_popup($emailInputAry)
{ 
    if(!empty($_POST['linkCustomerPopupAry']))
    {
        $szCrmEmail = $_POST['linkCustomerPopupAry']['szCrmEmail'];
        $idCrmEmail = $_POST['linkCustomerPopupAry']['idCrmEmail'];
        $szFromPage = $_POST['linkCustomerPopupAry']['szFromPage'];
        $idBookingFile = $_POST['linkCustomerPopupAry']['idBookingFile'];
        $szOperationType = $_POST['linkCustomerPopupAry']['szOperationType'];
        $szFlag = $_POST['linkCustomerPopupAry']['szFlag'];
    }
    else
    {  
        $idCrmEmail = $emailInputAry['idCrmEmail'];
        $szFromPage = $emailInputAry['szFromPage'];
        $idBookingFile = $emailInputAry['idBookingFile']; 
        $szOperationType = $emailInputAry['szOperationType']; 
        $szFlag = $emailInputAry['szFlag']; 
    } 
    if($szFlag=='')
    {
        $szFlag='PENDING_TRAY';
    }
    $popupAry = array();
    $popupAry['idCrmEmail'] = $idCrmEmail;
    $popupAry['idBookingFile'] = $idBookingFile;
    $popupAry['szFromPage'] = $szFromPage;
    $popupAry['szOperationType'] = $szOperationType;
    
    if($szFlag=='FROM_HEADER')
    {
        $szSearchStr=  sanitize_all_html_input(trim($_POST['szSearchStr']));
        
        if($szSearchStr!='')
        {
            $szCrmEmail =$szSearchStr;
            $ctr = 0; 
            $kForwarderContact = new cForwarderContact();
            $linkCustomerAry = array();
            $linkCustomerAry = $kForwarderContact->searchForwarderContacts($szSearchStr);
        }
        else
        {
            $idForwarderContact=(int)$_POST['idForwarderContact'];
            if($idForwarderContact>0){
                $kForwarderContact = new cForwarderContact();
                $linkCustomerAry = array();
                $linkCustomerAry = $kForwarderContact->searchForwarderContacts(array(),$idForwarderContact);
            }
           
        }
        
    }
    $widthText="75%";
        $widthBut="25%";
    if($szFlag=='FROM_HEADER'){
        $widthText="65%";
        $widthBut="35%";
    }
?>
    <div id="popup-bg" onclick="showHide('content_body')"></div>
    <div id="popup-container">
        <div class="popup contact-popup" style="width:80%;"> 
            <form id="cms_operation_search_user_email_form" name="cms_operation_search_user_email_form" action="javascript:void(0);">
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:<?php echo $widthText;?>;">
                        <input type="text" name="searchCrmEmailAry[szSearchString]" id="szSearchString" placeholder="" value="<?php echo $szCrmEmail; ?>" onkeyup="submit_link_customer_by_enter(event,'cms_operation_search_user_email_form','<?php echo $idCrmEmail; ?>')" onfocus="check_form_field_not_required_courier(this.form.id,this.id);">
                        <input type="hidden" name="searchCrmEmailAry[idCrmEmail]" id="idCrmEmail" value="<?php echo $idCrmEmail; ?>"> 
                        <input type="hidden" name="searchCrmEmailAry[szFromPage]" id="szFromPage" value="<?php echo $szFromPage; ?>">
                        <input type="hidden" name="searchCrmEmailAry[idBookingFile]" id="idBookingFile" value="<?php echo $idBookingFile; ?>">
                        <input type="hidden" name="searchCrmEmailAry[szOperationType]" id="szOperationType" value="<?php echo $szOperationType; ?>">
                        <input type="hidden" name="searchCrmEmailAry[szFlag]" id="szFlag" value="<?php echo $szFlag; ?>">
                    </span>
                    <span class="quote-field-container" style="width:<?php echo $widthBut;?>;">
                        <a href="javascript:void(0);" class="button1" id="link_customer_search_button" onclick="submit_link_customer_popup_form('cms_operation_search_user_email_form','<?php echo $idCrmEmail; ?>')"><span id="remove_crm_email_button_span">Search</span></a>
                        <a href="javascript:void(0);" class="button1" id="new_contact_forwarder" onclick="openNewContactOfForwarder('','<?php echo $idCrmEmail; ?>','<?php echo $szOperationType; ?>','<?php echo $szFlag;?>')"><span id="new_contact_forwarder_span">New</span></a>
                        <?php if($szFlag=='FROM_HEADER'){?>
                            <a href="javascript:void(0);" class="button2" id="delete_contact_forwarder" style="opacity:0.4"><span id="new_contact_forwarder_span">Delete</span></a>
                        <?php }?>
                    </span>
                </div>  
            </form>  
            <div class="clearfix dotted-line" style="width:100%;">&nbsp;</div>
            <div id="link_customer_list_container">
                <?php echo display_replay_customer_email_listing($linkCustomerAry,$popupAry,$szFlag); ?>
            </div>
            <div class="btn-container" style="text-align: center;padding:10px 0;">
                <?php if($szFlag=='FROM_HEADER'){?>
                    <a href="javascript:void(0)" onclick="showHide('contactPopup')" class="button1"><span>Close</span></a>
                <?php }else{?>
                <a href="javascript:void(0)" onclick="showHide('content_body')" class="button1"><span>Close</span></a>
                <?php }?>
            </div>
        </div>
    </div> 
    <?php
}

function display_link_customer_listing($linkCustomerAry,$idEmailLogs,$szFromPage,$idBookingFile,$szFlag='')
{
    $szStyle = 'style="max-height:250px;min-height:150px;"'; 
    $uagent = $_SERVER['HTTP_USER_AGENT'];
    $szExtraClass = '';
    $szOSName = os_info($uagent) ; 
    if($szOSName=='MacOS' || $szOSName=='iPad' || $szOSName=='iPhone')
    {
        $szExtraClass = ' mac-table-header'; 
    } 
    $szCrmEmail = "";
    if(!empty($_POST['linkCustomerPopupAry']['szSearchString']))
    {
        $szCrmEmail = $_POST['linkCustomerPopupAry']['szSearchString'];
    }  
    ?>
    <div>
    <table cellpadding="5" cellspacing="0" class="table-scroll-head <?php echo $szExtraClass; ?>" width="100%">
        <tr>
            <th style="width:33%;" valign="top">Name</th> 
            <th style="width:34%;" valign="top">Company</th>
            <th style="width:33%;" valign="top">Email</th> 
        </tr>
    </table> 
    <div id="pending_task_list_container_popup" class="pending-tray-scrollable-div"> 
        <div class="scrolling-div pending-tray-lists" <?php echo $szStyle; ?>>
            <table cellpadding="5" cellspacing="0" class="format-3" width="100%">
                <?php
                    if(!empty($linkCustomerAry))
                    {
                        foreach($linkCustomerAry as $linkCustomerArys)
                        {
                            if($linkCustomerArys['iPrivateShipping']==1)
                            {
                                $szCompanyName = "Private";
                            }
                            else
                            {
                                $szCompanyName = $linkCustomerArys['szCustomerCompanyName'];
                            }
                            ?>
                            <tr onclick="crm_operation_link_customer('<?php echo $idEmailLogs; ?>','<?php echo $linkCustomerArys['idUser']; ?>','<?php echo $idBookingFile; ?>','<?php echo $szFromPage; ?>');"> 
                                <td style="width:33%;" valign="top"><?php echo $linkCustomerArys['szFirstName']." ".$linkCustomerArys['szLastName']; ?></td>
                                <td style="width:34%;" valign="top"><?php echo $szCompanyName; ?></td>
                                <td style="width:33%;" valign="top"><?php echo $linkCustomerArys['szEmail']; ?></td>
                            </tr>
                            <?php
                        }
                    }
                    else
                    {
                        if(!empty($szCrmEmail))
                        {
                           ?>
                            <tr>
                                <td colspan="3" style="text-align:center"><br>E-mail address does not exist.<br><a href="javascript:void(0);" onclick="display_crm_operations('<?php echo $idBookingFile; ?>','<?php echo $szFromPage; ?>','<?php echo $idEmailLogs; ?>','CRM_CREATE_USER','<?php echo $szCrmEmail; ?>');">Click to create a new user with this e-mail.</a></td>
                            </tr> 
                           <?php
                        }
                    }
                ?>
            </table> 
        </div> 
    </div>
    </div>
    <?php
}


function display_replay_customer_email_listing($linkCustomerAry,$dataAry,$szFlag='')
{
    $szStyle = 'style="max-height:250px;min-height:150px;"'; 
    $uagent = $_SERVER['HTTP_USER_AGENT'];
    $szExtraClass = '';
    $szOSName = os_info($uagent) ; 
    if($szOSName=='MacOS' || $szOSName=='iPad' || $szOSName=='iPhone')
    {
        $szExtraClass = ' mac-popup-table-header'; 
    } 
    $idEmailLogs = $dataAry['idCrmEmail'];
    $szOperationType = $dataAry['szOperationType'];
    
    $alreadyUsedAry = array(); 
    ?>
    <div>
    <table cellpadding="5" cellspacing="0" class="table-scroll-head <?php echo $szExtraClass; ?>" width="100%">
        <tr>
            <th style="width:17%;" valign="top">Name</th> 
            <th style="width:20%;" valign="top">Company</th>
            <th style="width:20%;" valign="top">Email</th>
            <th style="width:20%;" valign="top">Phone</th>
            <th style="width:23%;" valign="top">Responsibility</th> 
        </tr>
    </table> 
    <div id="pending_task_list_container_popup" class="pending-tray-scrollable-div"> 
        <div class="scrolling-div pending-tray-lists" <?php echo $szStyle; ?>>
            <table cellpadding="5" cellspacing="0" class="format-3" width="100%" id="forwarderManagement">
                <?php
                    if(!empty($linkCustomerAry))
                    {
                        $ctr=1;
                        foreach($linkCustomerAry as $linkCustomerArys)
                        {
                            if($linkCustomerArys['iPrivateShipping']==1)
                            {
                                $szCompanyName = "Private (".$linkCustomerArys['szForwarderAlias'].")";
                            }
                            else
                            {
                                $szCompanyName = $linkCustomerArys['szCustomerCompanyName']." (".$linkCustomerArys['szForwarderAlias'].")";
                            } 
                            if(empty($linkCustomerArys['szEmail']))
                            {
                                continue;
                            }
                            $szStrKey = trim($linkCustomerArys['szEmail'])."_".trim($linkCustomerArys['szFirstName'])."_".trim($linkCustomerArys['szLastName'])."_".trim($szCompanyName);
                            if(!empty($alreadyUsedAry) && in_array($szStrKey,$alreadyUsedAry))
                            {
                                continue;
                            }
                            else
                            {
                                $alreadyUsedAry[] = $szStrKey;  
                            }
                            
                            $szPhoneNumberStr='';
                            if($linkCustomerArys['szPhone']!='' && $linkCustomerArys['szMobile']!='')
                            {
                                $szPhoneNumberStr=$linkCustomerArys['szPhone'].' / '.$linkCustomerArys['szMobile'];
                            }
                            else if($linkCustomerArys['szPhone']!='')
                            {
                                $szPhoneNumberStr=$linkCustomerArys['szPhone'];
                            }
                            else if($linkCustomerArys['szMobile']!='')
                            {
                                $szPhoneNumberStr=$linkCustomerArys['szMobile'];
                            }
                            ?>
                            <tr <?php if($szFlag=='FROM_HEADER'){?> id="forwarder_profile_<?php echo $ctr;?>" onclick="select_forwarder_management('<?php echo $linkCustomerArys['id'];?>','forwarder_profile_<?php echo $ctr;?>','<?php echo $linkCustomerArys['idForwarderContactRole'];?>','<?php echo $szFlag;?>');" <?php }else{ ?> onclick="update_crm_operation_reply('<?php echo $linkCustomerArys['szEmail']; ?>','<?php echo $idEmailLogs; ?>','<?php echo $szOperationType; ?>','<?php echo $linkCustomerArys['szFirstName']; ?>');<?php }?>" style="cursor:pointer;"> 
                                <td style="width:17%;" valign="top"><?php echo $linkCustomerArys['szFirstName']." ".$linkCustomerArys['szLastName']; ?></td>
                                <td style="width:20%;" valign="top"><?php echo $szCompanyName; ?></td>
                                <td style="width:20%;" valign="top"><?php echo $linkCustomerArys['szEmail']; ?></td>
                                <td style="width:20%;" valign="top"><?php echo $szPhoneNumberStr; ?></td>
                                <td style="width:23%;" valign="top"><?php echo $linkCustomerArys['szResponsibility']; ?></td>
                            </tr>
                            <?php
                            ++$ctr;
                        }
                    }
                    else
                    {
                        ?>
                        <!--<div class="clearfix email-fields-container" style="width:100%;height:150px;">&nbsp;</div>-->
                        <?php
                    }
                ?>
            </table> 
        </div> 
    </div>
    </div>
    <?php
}

function display_crm_operations_create_crm_user_fields($kCrm,$kBooking,$logEmailsArys,$szCrmEmail=false)
{
    if(!empty($_POST['addCrmOperationAry']))
    {
        $addCrmOperationAry = $_POST['addCrmOperationAry'] ;
        
        $szFirstName = $addCrmOperationAry['szFirstName'];
        $szLastName = $addCrmOperationAry['szLastName'];
        $szCustomerCompanyName = $addCrmOperationAry['szCustomerCompanyName'];
        $szCustomerPhoneNumber = $addCrmOperationAry['szCustomerPhoneNumber'];
        $idCustomerDialCode = $addCrmOperationAry['idCustomerDialCode'];
        $szEmail = $addCrmOperationAry['szEmail'];  
        $idUser = $addCrmOperationAry['idUser'];
        
        $kUser = new cUser();
        $kUser->getUserDetails($idUser);
        
        $idBookingFile = $addCrmOperationAry['idBookingFile'];  
        $idBooking = $addCrmOperationAry['idBooking']; 
        
        $iPrivateShipping = $addCrmOperationAry['iPrivateShipping'];  
        $idCustomerCurrency = $addCrmOperationAry['idCustomerCurrency'];  
        $iBookingLanguage = $addCrmOperationAry['iBookingLanguage'];  
        $iAcceptNewsUpdate = $addCrmOperationAry['iAcceptNewsUpdate'];  
        $idFileOwner = $addCrmOperationAry['idFileOwner']; 
        $idCustomerOwner = $addCrmOperationAry['idCustomerOwner'];  
        $idCustomerCountry = $addCrmOperationAry['idCustomerCountry'];  
           
        $szCustomerAddress1 = $addCrmOperationAry['szCustomerAddress1']; 
        $szCustomerPostCode = $addCrmOperationAry['szCustomerPostCode']; 
        $szCustomerCity = $addCrmOperationAry['szCustomerCity']; 
        $szCustomerCountry = $addCrmOperationAry['szCustomerCountry'];  
    }
    else
    {
        $szBookingFileStatus =  $kBooking->szFileStatus;
        $szBookingTaskStatus =  $kBooking->szTaskStatus;
        $iBookingSearchedType = $kBooking->iSearchType; 
        $idBooking = $kBooking->idBooking; 
        $idBookingFile = $kBooking->idBookingFile;   
        $idCustomerOwner = $kBooking->idCustomerOwner;   
        $idFileOwner = $kBooking->idFileOwner;
            
        $postSearchAry = array();
        $kBooking_new = new cBooking();
        $postSearchAry = $kBooking_new->getExtendedBookingDetails($idBooking);
        
        if($postSearchAry['idUser']>0)
        {
            $szFirstName = $postSearchAry['szFirstName'];
            $szLastName = $postSearchAry['szLastName'];
            $szCustomerCompanyName = $postSearchAry['szCustomerCompanyName'];
            $szCustomerPhoneNumber = $postSearchAry['szCustomerPhoneNumber'];
            $idCustomerDialCode = $postSearchAry['idCustomerDialCode']; 
            if(!empty($szCrmEmail))
            {
                $szEmail = $szCrmEmail;  
            }
            else
            {
                $szEmail = $postSearchAry['szEmail'];  
            }
            
            $idUser = $postSearchAry['idUser'];  
            $szCustomerCompanyRegNo = $postSearchAry['szCustomerCompanyRegNo'];

            $kUser = new cUser();
            $kUser->getUserDetails($idUser); 
            
            $iPrivateShipping = $postSearchAry['iPrivateShipping'];  
            $idCustomerCurrency = $postSearchAry['idCustomerCurrency'];   
            $iAcceptNewsUpdate = $kUser->iAcceptNewsUpdate;   
            $idCustomerCountry = $kUser->szCountry ;
                 
            $szCustomerAddress1 = $postSearchAry['szCustomerAddress1']; 
            $szCustomerPostCode = $postSearchAry['szCustomerPostCode']; 
            $szCustomerCity = $postSearchAry['szCustomerCity']; 
            $szCustomerCountry = $postSearchAry['szCustomerCountry'];  

            if($iAlreadyPaidBooking==1)
            {
                $iBookingLanguage = $postSearchAry['iBookingLanguage'];  
            }
            else
            {
                $iBookingLanguage = $kUser->iLanguage;   
            }
        }
        else
        {
            if(!empty($szCrmEmail))
            {
                $szEmail = $szCrmEmail;  
            }
            else
            {
                $szEmail = $logEmailsArys['szToAddress'];
            } 
            if(!empty($logEmailsArys['szToUserName']))
            {
                $userNameAry = explode(" ",$logEmailsArys['szToUserName']);
                $szFirstName = $userNameAry[0];
                $szLastName = $userNameAry[1];
            }
            $iBookingLanguage = $postSearchAry['iBookingLanguage'];  
        }    
    }  
    
    $t_base="management/pendingTray/";   
    $szMainContainerClass="overview-form";
    $szFirstSpanClass="field-name";
    $szSecondSpanClass="field-container";
    $szSecondSpanClass_name = "name-container";
    
    $kAdmin_new = new cAdmin();
    $kConfig = new cConfig();
    
    $customerOwnerAry = array();
    $customerOwnerAry = $kAdmin_new->viewManagementDetails(false,false,true);
    
    $langArr=$kConfig->getLanguageDetails();
    
    $activeAdminAry = $customerOwnerAry ;
    $allCountriesArr = array();
    $allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);
    
    $kConfig = new cConfig();
    $dialUpCodeAry = array();
    $dialUpCodeAry = $kConfig->getAllCountries(true,$iLanguage,true);
    
    $currencyAry = array();
    $currencyAry = $kConfig->getBookingCurrency(false,true); 
    $szFormId = "crm_email_operations_form_".$logEmailsArys['id']; 
    $szIDSuffix = "_".$logEmailsArys['id'];
    ?>
    <script type="text/javascript">
        enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');
        toggleCompanyField('iPrivateShipping'+'<?php echo $szIDSuffix; ?>','1','<?php echo $logEmailsArys['id']; ?>');
    </script> 
    <table cellpadding="5" cellspacing="0" class="task-table" width="100%" >
        <tr>
            <td style="width:50%;" valign="top" id="validate_pane_customer_container">
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/customer')?>:</span>
                    <span class="<?php echo $szSecondSpanClass_name; ?>">  
                        <input type="text" name="addCrmOperationAry[szFirstName]" onkeyup="enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" id="szFirstName<?php echo $szIDSuffix; ?>" placeholder="First Name" value="<?php echo $szFirstName; ?>">
                        <input type="text" name="addCrmOperationAry[szLastName]"  onkeyup="enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" id="szLastName<?php echo $szIDSuffix; ?>" placeholder="Last Name" value="<?php echo $szLastName; ?>">
                    </span>
                </div>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/company')?>:</span>
                    <span class="single-field">
                        <input type="text" name="addCrmOperationAry[szCustomerCompanyName]" onkeyup="enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" onblur="enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" onfocus="check_form_field_not_required(this.form.id,this.id);"  id="szCustomerCompanyName<?php echo $szIDSuffix; ?>" placeholder="Company Name" value="<?php echo $szCustomerCompanyName; ?>">
                    </span>
                </div>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/registration')?>:</span>
                    <span class="company-registration">
                        <input type="text" name="addCrmOperationAry[szCustomerCompanyRegNo]" onkeyup="enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" onfocus="check_form_field_not_required(this.form.id,this.id);"  id="szCustomerCompanyRegNo<?php echo $szIDSuffix; ?>" placeholder="Company Registration" value="<?php echo $szCustomerCompanyRegNo; ?>">
                    </span>
                    <span class="private-cb">    
                        <input type="checkbox" name="addCrmOperationAry[iPrivateShipping]"  onclick="toggleCompanyField(this.id,'1','<?php echo $logEmailsArys['id']; ?>');enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" onfocus="check_form_field_not_required(this.form.id,this.id);"  id="iPrivateShipping<?php echo $szIDSuffix; ?>" value="1" <?php echo (($iPrivateShipping==1)?'checked':''); ?>> <?php echo t($t_base.'fields/private')?>
                    </span>
                </div> 
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/phone')?>:</span>
                    <span class="phone-container">
                        <select size="1" name="addCrmOperationAry[idCustomerDialCode]" onchange="enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" id="idCustomerDialCode<?php echo $szIDSuffix; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');">
                            <?php
                               if(!empty($dialUpCodeAry))
                               {
                                   $usedDialCode = array();
                                   foreach($dialUpCodeAry as $dialUpCodeArys)
                                   {
                                        if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                                        {
                                            $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ;
                                            ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$idCustomerDialCode){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option><?php
                                        }
                                   }
                               }
                        ?>
                        </select> 
                        <input type="text" name="addCrmOperationAry[szCustomerPhoneNumber]" onkeyup="enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" id="szCustomerPhoneNumber<?php echo $szIDSuffix; ?>" value="<?php echo $szCustomerPhoneNumber; ?>" />
                    </span>
                </div>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/email')?>:</span>
                    <span class="single-field"> 
                        <input type="text" name="addCrmOperationAry[szEmail]" onkeyup="enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" id="szEmail<?php echo $szIDSuffix; ?>" value="<?php echo $szEmail; ?>" > 
                    </span> 
                </div>   
            </td> 
            <td style="width:50%;" valign="top">
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/billing')." ".t($t_base.'fields/address').":"?></span>
                    <span class="single-field">  
                        <input type="text" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" onblur="enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" name="addCrmOperationAry[szCustomerAddress1]" id="szCustomerAddress1<?php echo $szIDSuffix; ?>" placeholder="Billing Street Address" value="<?php echo $szCustomerAddress1; ?>">
                    </span>
                </div>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>">&nbsp;</span>
                    <span class="field-space">  
                        <input type="text" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" onblur="check_postcode_field(this.form.id,this.id);enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" name="addCrmOperationAry[szCustomerPostCode]" id="szCustomerPostCode<?php echo $szIDSuffix; ?>" placeholder="Billing Postcode" value="<?php echo $szCustomerPostCode; ?>">
                    </span>
                    <span class="field-space">
                        <input type="text" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" onblur="check_form_field_empty_standard(this.form.id,this.id);enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" name="addCrmOperationAry[szCustomerCity]" id="szCustomerCity<?php echo $szIDSuffix; ?>" placeholder="Billing City" value="<?php echo $szCustomerCity; ?>">
                    </span>
                    <span class="field-nospace">
                        <select name="addCrmOperationAry[szCustomerCountry]" id="szCustomerCountry<?php echo $szIDSuffix; ?>" onchange = "update_crm_dial_code('<?php echo $logEmailsArys['id']; ?>',this.value);enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');">
                            <option value="">Select</option>
                            <?php
                                if(!empty($allCountriesArr))
                                {
                                   foreach($allCountriesArr as $allCountriesArrs)
                                   {
                                    ?>
                                    <option value="<?php echo $allCountriesArrs['id']; ?>" <?php echo (($allCountriesArrs['id']==$szCustomerCountry)?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName']; ?></option>
                                    <?php
                                   }
                                }
                            ?> 
                        </select>
                    </span>
                </div>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/currency')?>:</span>
                    <span class="currency-dropdown"> 
                        <select name="addCrmOperationAry[idCustomerCurrency]" id="idCustomerCurrency<?php echo $szIDSuffix; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onchange="enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');">
                            <option value="">Select</option>
                            <?php
                                if(!empty($currencyAry))
                                {
                                   foreach($currencyAry as $currencyArys)
                                   {
                                    ?>
                                    <option value="<?php echo $currencyArys['id']; ?>" <?php echo (($currencyArys['id']==$idCustomerCurrency)?'selected':''); ?>><?php echo $currencyArys['szCurrency']; ?></option>
                                    <?php
                                   }
                                }
                            ?> 
                        </select> 
                    </span>
                    <span class="label-language"><?php echo t($t_base.'fields/language')?>:</span>
                    <span class="currency-dropdown"> 
                        <select name="addCrmOperationAry[iBookingLanguage]" id="iBookingLanguage<?php echo $szIDSuffix; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onchange="enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');">
                            <option value="">Select</option>
                            <?php
                            if(!empty($langArr))
                            {
                                foreach($langArr as $langArrs)
                                {?>
                                    <option value="<?php echo $langArrs['id']; ?>" <?php echo (($langArrs['id']==$iBookingLanguage)?'selected':''); ?>><?php echo ucfirst($langArrs['szName']); ?></option>
                                   <?php 
                                }
                            }?>
                            
<!--                            <option value="<?php echo __LANGUAGE_ID_ENGLISH__; ?>" <?php echo ((__LANGUAGE_ID_ENGLISH__==$iBookingLanguage)?'selected':''); ?>><?php echo ucfirst(__LANGUAGE_TEXT_ENGLISH__); ?></option>-->
                        </select> 
                    </span>
                </div> 
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/newsletter')?>:</span>
                    <span class="currency-dropdown"> 
                        <select name="addCrmOperationAry[iAcceptNewsUpdate]" id="iAcceptNewsUpdate<?php echo $szIDSuffix; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onchange="enableCrmOperationCreateUser('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');">
                            <option value="">Select</option>
                            <option value="1" <?php echo (($iAcceptNewsUpdate==1)?'selected':''); ?>>Yes</option>
                            <option value="2" <?php echo (($iAcceptNewsUpdate==2)?'selected':''); ?>>No</option> 
                        </select> 
                    </span>
                    <span class="label-language"><?php echo t($t_base.'fields/customer_owner')?>:</span>
                    <span class="currency-dropdown"> 
                        <select name="addCrmOperationAry[idCustomerOwner]" id="idCustomerOwner<?php echo $szIDSuffix; ?>">
                             <option value="">Not allocated</option> 
                            <?php
                                if(!empty($customerOwnerAry))
                                {
                                   foreach($customerOwnerAry as $activeAdminArys)
                                   {
                                    ?>
                                    <option value="<?php echo $activeAdminArys['id']; ?>" <?php echo (($activeAdminArys['id']==$idCustomerOwner)?'selected':''); ?>><?php echo $activeAdminArys['szFirstName']." ".$activeAdminArys['szLastName']; ?></option>
                                    <?php
                                   }
                                }
                            ?> 
                        </select>
                    </span>
                </div> 
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;">
                <a href="javascript:void(0);" class="button2" id="crm_options_createuser_cancel_button" onclick="cancel_crm_user_details('<?php echo $logEmailsArys['id']; ?>')"><span>Cancel</span></a>
                <a href="javascript:void(0);" class="button1" id="crm_options_createuser_save_button"><span>Save</span></a>
                <input type="hidden" name="addCrmOperationAry[idUser]" id="idUser" value="<?php echo $idUser; ?>">
                <input type="hidden" name="addCrmOperationAry[idBooking]" id="idBooking" value="<?php echo $idBooking; ?>">
            </td>
        </tr>
    </table> 
    <?php
    if(!empty($kCrm->arErrorMessages))
    {  
        display_crm_errors($kCrm->arErrorMessages,$szFormId,$logEmailsArys['id']);
    } 
}
function display_crm_operations_reply_email($kCrm,$kBooking,$logEmailsArys,$szAction=false,$szFromPage=false,$emailTemplate_ID=false,$iTemplateLanguage=false)
{
     
    $idBooking = $kBooking->idBooking ;    
    $t_base="management/pendingTray/";    
    $kConfig = new cConfig();
    $szFirstSpanClass="quote-field-label";
    $szSecondSpanClass="quote-field-container";    
    $replace_ary = array();   
    $kBooking_new  = new cBooking();
    $kBooking_new = $kBooking; //Creating object clone 
    if(!empty($_POST['addCrmOperationAry']))
    { 
        $addSnoozeAry = $_POST['addCrmOperationAry'];  
        $szReminderNotes = $addSnoozeAry['szReminderNotes'];  
        $idBookingFile = $addSnoozeAry['idBookingFile'];
        $dtReminderDate = $addSnoozeAry['dtReminderDate'];
        $szReminderTime = $addSnoozeAry['szReminderTime'];
        $emailTemplate_ID = $addSnoozeAry['idTemplate'];
        $szCustomerEmail = $addSnoozeAry['szCustomerEmail'];
        $szReminderSubject = $addSnoozeAry['szReminderSubject'];
        $szReminderEmailBody = $addSnoozeAry['szReminderEmailBody'];
        $dtEmailReminderDate = $addSnoozeAry['dtEmailReminderDate'];
        $szEmailReminderTime = $addSnoozeAry['szEmailReminderTime'];
        $szCustomerCCEmail = $addSnoozeAry['szCustomerCCEmail'];
        $szCustomerBCCEmail = $addSnoozeAry['szCustomerBCCEmail'];
        $szFromPage = $addSnoozeAry['szFromPage'];
        
        $szAction = $addSnoozeAry['szAction'];
        $iTemplateLanguage = $addSnoozeAry['iRemindTemplateLanguage'];
        
        $kBooking->loadFile($idBookingFile);
        $bookingDataArr = $kBooking->getBookingDetails($idBooking);
        $szRemindEmailTemplates = $kConfig->getRemindCmsSectionsList(false,false,$szFromPage);
    }  
    else
    { 
        $idBookingFile = $kBooking->idBookingFile;
        $idBooking = $kBooking->idBooking; 
        $dtReminderDateTime = $kBooking->dtSnoozeDate;
        $idOfflineChat = $kBooking->idOfflineChat;
        $bookingDataArr = $kBooking->getBookingDetails($idBooking); 
        if($szAction=='CRM_FORWARD' || $szAction=='CRM_NEW_EMAIL')
        {
            $szCustomerEmail = "";
        }
        else if(!empty($logEmailsArys['szToAddress']))
        {
            $szCustomerEmail = $logEmailsArys['szToAddress'];
        }
        else
        {
            $szCustomerEmail = $bookingDataArr['szEmail'];
        }  
        if($szAction=='CRM_REPLY_ALL')
        {
            $szCustomerCCEmail = $logEmailsArys['szCCEmailAddress'];
            $szCustomerBCCEmail = $logEmailsArys['szBCCEmailAddress'];
        }
        $szRemindEmailTemplates = $kConfig->getRemindCmsSectionsList(false,false,$szFromPage); 
        if(empty($iTemplateLanguage))
        {
            $iTemplateLanguage = $bookingDataArr['iBookingLanguage'];
        }
    }  
    $kConfig = new cConfig();
    $languageArr=$kConfig->getLanguageDetails('','','','','',true);
    
    $szFormId = "crm_email_operations_form_".$logEmailsArys['id'];  
    $idCrmEmail = $logEmailsArys['id'];
    ?>
    <script type="text/javascript">
        <?php if(!empty($szCustomerCCEmail)){ ?>
            display_email_fields('SHOW_CC','<?php echo $idCrmEmail; ?>');
        <?php } if(!empty($szCustomerBCCEmail)){ ?>
            display_email_fields('SHOW_BCC','<?php echo $idCrmEmail; ?>');
        <?php }?>
    </script>
    <div id="change_reminder_email_template_container">
        <div class="clearfix email-fields-container"> 
            <span class="<?php echo $szFirstSpanClass; ?>">Template</span>
            <span class="<?php echo $szSecondSpanClass; ?>" style="width:40%;" >
                <select name="addCrmOperationAry[idTemplate]" id="idTemplate_<?php echo $idCrmEmail; ?>" onchange="changeRemindEmailTemplateBody('CHANGE_REMIND_EMAIL_TEMPLATE_BODY','<?php echo $idBookingFile; ?>',this.value,'<?php echo $replace_ary['iBookingLanguage'];?>','CHANGE_TEMPLATE','CRM_TEMPLATE','<?php echo $logEmailsArys['id'];?>','<?php echo $szAction; ?>');enable_crm_operation_reply_tab_buttons('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');">
                    <?php 
                    if(!empty($szRemindEmailTemplates))
                    { 
                        if($emailTemplate_ID == 0)
                        {?>
                            <option value="0" <?php if($emailTemplate_ID==0){?>selected="selected"<?php }?> >Select</option>
                        <?php
                        }
                        foreach($szRemindEmailTemplates as $szRemindEmailTemplateDetail)
                        {?>
                            <option value="<?php echo $szRemindEmailTemplateDetail['id'];?>" <?php if($szRemindEmailTemplateDetail['id'] == $emailTemplate_ID){?>selected="selected"<?php }?> ><?php echo $szRemindEmailTemplateDetail['szFriendlyName'];?></option>
                        <?php
                        }
                    }
                    ?>
                </select>
            </span>
            <span class="<?php echo $szSecondSpanClass; ?>" style="width:10%;margin-left:5px;">
                <select name="addCrmOperationAry[iRemindTemplateLanguage]" id="iRemindTemplateLanguage_<?php echo $idCrmEmail; ?>" onchange="changeRemindEmailTemplateBody('CHANGE_REMIND_EMAIL_TEMPLATE_BODY','<?php echo $idBookingFile; ?>','<?php echo $emailTemplate_ID;?>',this.value,'CHANGE_LANGUAGE','CRM_TEMPLATE','<?php echo $logEmailsArys['id'];?>','<?php echo $szAction; ?>');">
                    <?php
                    if(!empty($languageArr))
                    {
                        foreach($languageArr as $languageArrs)
                        {?>
                            <option value="<?php echo $languageArrs['id'];?>" <?php if($replace_ary['iBookingLanguage'] == $languageArrs['id']){ ?> selected="selected"<?php }?>><?php echo ucfirst($languageArrs['szName']);?></option>
                           <?php 
                        }
                    }?>  
                </select>
            </span>
            <span class="<?php echo $szSecondSpanClass; ?>" style="width:25%;float:right;text-align:right;"> 
                <a href="javascript:void(0);" id="reminder_template_edit_button" class="button1" onclick="add_edit_RemindEmailTemplate('REMIND','<?php echo $idBookingFile; ?>','edit_template','','<?php echo $idCrmEmail; ?>','<?php echo $szFromPage; ?>','<?php echo $szAction; ?>');"><span>Edit</span></a> 
                <a href="javascript:void(0);" id="reminder_template_new_button" class="button1" onclick="add_edit_RemindEmailTemplate('REMIND','<?php echo $idBookingFile; ?>','new_template','','<?php echo $idCrmEmail; ?>','<?php echo $szFromPage; ?>','<?php echo $szAction; ?>');"><span>New</span></a> 
            </span> 
        </div>
        <div class="clearfix email-fields-container"> 
            <span class="quote-field-container wd-14">To</span>
            <span class="quote-field-container wd-40">
                <input type="text" name="addCrmOperationAry[szCustomerEmail]" onkeyup="enable_crm_operation_reply_tab_buttons('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" onblur="enable_crm_operation_reply_tab_buttons('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" placeholder="Email Addresses, separate with comma" id="szCustomerEmail_<?php echo $idCrmEmail; ?>" value="<?php echo $szCustomerEmail; ?>"> 
            </span>  
            <span class="quote-field-container wds-45">
                <a href="javascript:void(0);" onclick="display_remind_pane_add_popup('CUSTOMER_EMAIL','<?php echo $idBookingFile; ?>','CRM_OPERATION','<?php echo $idCrmEmail; ?>');">Add</a>
                <span id="crm_operation_cc_link_<?php echo $idCrmEmail; ?>">&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="display_email_fields('SHOW_CC','<?php echo $idCrmEmail; ?>')">Cc</a></span>
                <span id="crm_operation_bcc_link_<?php echo $idCrmEmail; ?>">&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="display_email_fields('SHOW_BCC','<?php echo $idCrmEmail; ?>')">Bcc</a></span>
            </span>
        </div>
        <div class="clearfix email-fields-container" id="crm_operation_cc_fields_container_<?php echo $idCrmEmail; ?>" <?php if($iAddCCEmail==1){ ?>style="display:block;"<?php }else{?> style="display:none;"<?php } ?>> 
            <span class="quote-field-container wd-14">Cc</span>
            <span class="quote-field-container wd-40">
                <input type="text" name="addCrmOperationAry[szCustomerCCEmail]" onkeyup="enable_crm_operation_reply_tab_buttons('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" onblur="enable_crm_operation_reply_tab_buttons('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" placeholder="Email Addresses, separate with comma" id="szCustomerCCEmail_<?php echo $idCrmEmail; ?>" value="<?php echo $szCustomerCCEmail; ?>"> 
            </span>  
            <span class="quote-field-container wds-45">
                <a href="javascript:void(0);" onclick="display_remind_pane_add_popup('CUSTOMER_CC_EMAIL','<?php echo $idBookingFile; ?>','CRM_OPERATION','<?php echo $idCrmEmail; ?>');">Add</a> 
                &nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="display_email_fields('HIDE_CC','<?php echo $idCrmEmail; ?>')" class="red_text"><strong>X</strong></a>
            </span>
        </div>
        <div class="clearfix email-fields-container" id="crm_operation_bcc_fields_container_<?php echo $idCrmEmail; ?>" <?php if($iAddBCCEmail==1){ ?>style="display:block;"<?php }else{?> style="display:none;"<?php } ?>> 
            <span class="quote-field-container wd-14">Bcc</span>
            <span class="quote-field-container wd-40">
                <input type="text" name="addCrmOperationAry[szCustomerBCCEmail]" onkeyup="enable_crm_operation_reply_tab_buttons('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" onblur="enable_crm_operation_reply_tab_buttons('<?php echo $szFormId; ?>','<?php echo $logEmailsArys['id']; ?>');" placeholder="Email Addresses, separate with comma" id="szCustomerBCCEmail_<?php echo $idCrmEmail; ?>" value="<?php echo $szCustomerBCCEmail; ?>"> 
            </span>  
            <span class="quote-field-container wds-45">
                <a href="javascript:void(0);" onclick="display_remind_pane_add_popup('CUSTOMER_BCC_EMAIL','<?php echo $idBookingFile; ?>','CRM_OPERATION','<?php echo $idCrmEmail; ?>');">Add</a> 
                &nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="display_email_fields('HIDE_BCC','<?php echo $idCrmEmail; ?>')" class="red_text"><strong>X</strong></a>
            </span>
        </div>
        <input type="hidden" name="addCrmOperationAry[iAddCCEmail]" id="iAddCCEmail_<?php echo $idCrmEmail; ?>" value="<?php echo $iAddCCEmail; ?>">
        <input type="hidden" name="addCrmOperationAry[iAddBCCEmail]" id="iAddBCCEmail_<?php echo $idCrmEmail; ?>" value="<?php echo $iAddBCCEmail; ?>">
    </div>
    <div id="send_reminder_email_container_<?php echo $idCrmEmail; ?>">
        <?php echo display_email_template_body($kBooking,$emailTemplate_ID,$iTemplateLanguage,$iSuccess,'CRM_TEMPLATE',$logEmailsArys,$szAction);?>
    </div> 
    <?php
    if(!empty($kCrm->arErrorMessages))
    { 
        display_crm_errors($kCrm->arErrorMessages,$szFormId,$idCrmEmail); 
    }
} 
function display_crm_errors($validationErrorAry,$szFormId,$idCrmEmail)
{ 
    if(!empty($validationErrorAry))
    {
        foreach($validationErrorAry as $key=>$validationErrorArys)
        {			
            ?>
            <script type="text/javascript">
                displayFormFieldIsRequired('<?php echo $szFormId; ?>', '<?php echo $key."_".$idCrmEmail; ?>', '<?php echo $validationErrorArys;?>');
            </script>
            <?php
        }
    }  
}

function display_email_template_listings($attachmentInputAry)
{
    $emailTemplate_ID = $attachmentInputAry['idTemplate'];
    $templateDetail = array();
    $kConfig = new cConfig();
    $templateDetail = $kConfig->getRemindCmsSectionsList($iBookingLanguage,$emailTemplate_ID); 

    $attachmentOriginalFileNameAry = array();
    $attachmentFileNameAry = array(); 
    if(!empty($templateDetail[0]['szAttachmentOriginalFileName']))
    {
        $attachmentOriginalFileNameAry = unserialize($templateDetail[0]['szAttachmentOriginalFileName']);
    } 
    if(!empty($templateDetail[0]['szAttachmentFileName']))
    {
        $attachmentFileNameAry = unserialize($templateDetail[0]['szAttachmentFileName']);
    }   
    
    $iAddInvoice = $templateDetail[0]['iAddInvoice'];
    $iAddBookingNotice = $templateDetail[0]['iAddBookingNotice'];
    $iAddBookingConfirmation = $templateDetail[0]['iAddBookingConfirmation'];
    
    $bDocumentAdded = false;
    if((int)$iAddInvoice==0 && (int)$iAddInvoice==0 && (int)$iAddInvoice==0)
    {
        $bDocumentAdded = true;
    }
    ?>
    <div id="popup-bg" onclick="showHide('content_body')"></div>
    <div id="popup-container">
        <div class="popup contact-popup" style="width:500px;"> 
            <h4>Please select attachment</h4>
            <form id="cms_operation_search_user_email_form" name="cms_operation_search_user_email_form" action="javascript:void(0);">
                <?php
                    if(!empty($attachmentFileNameAry))
                    {
                        foreach($attachmentFileNameAry as $key=>$attachmentFileNameArys)
                        {
                            ?>
                            <div class="clearfix email-fields-container" style="width:100%;"> 
                                <span class="quote-field-container wd-10">
                                    <input type="checkbox" name="addCrmAttachmentAry[attachments][<?php echo $Ctr; ?>]" id="attachments_<?php echo $Ctr; ?>" value="<?php echo $attachmentFileNameArys?>">
                                </span>
                                <span class="quote-field-container wds-90">
                                    <a href=""><?php echo $attachmentOriginalFileNameAry[$key]; ?></a>
                                </span> 
                            </div>
                            <?php
                        }
                    }
                    else if($bDocumentAdded)
                    {
                        ?>
                        <div class="clearfix email-fields-container" style="width:100%;"><strong>Tere is no attachment added with this template</strong> </div>
                        <?php
                    }
                    
                    if($iAddInvoice==1)
                    {
                        ?>
                        <div class="clearfix email-fields-container" style="width:100%;"> 
                            <span class="quote-field-container wd-10">
                                <input type="checkbox" name="addCrmAttachmentAry[attachments][<?php echo $Ctr; ?>]" id="attachments_<?php echo $Ctr; ?>" value="BOOKING_INVOICE">
                            </span>
                            <span class="quote-field-container wds-90">
                                Booking Invoice
                            </span> 
                        </div>
                        <?php
                    }
                ?> 
                <input type="hidden" name="addCrmAttachmentAry[idCrmEmail]" id="idCrmEmail" value="<?php echo $idCrmEmail; ?>"> 
                <input type="hidden" name="addCrmAttachmentAry[szFromPage]" id="szFromPage" value="<?php echo $szFromPage; ?>">
                <input type="hidden" name="addCrmAttachmentAry[idBookingFile]" id="idBookingFile" value="<?php echo $idBookingFile; ?>">
                <input type="hidden" name="addCrmAttachmentAry[szOperationType]" id="szOperationType" value="<?php echo $szOperationType; ?>">
            </form>   
            <div class="btn-container" style="text-align: center;padding:10px 0;">
                <a href="javascript:void(0)" onclick="showHide('content_body')" class="button1"><span>Close</span></a>
            </div>
        </div>
    </div>
    <?php
}

function format_emails($szToAddress,$szLabel)
{
    $emailAry = array();
    $ret_ary = array();
    if(!empty($szToAddress))
    {
        $emailAry = explode(",",$szToAddress);
        if(!empty($emailAry))
        {
            foreach($emailAry as $emailArys)
            {
                $ret_ary[] = $szLabel.": ".trim($emailArys);
            }
        }
    } 
    return $ret_ary;
}

function getFormatedEmails($logEmailsArys)
{
    $finalEmailAry = array();
    $finalEmailAry = format_emails($logEmailsArys['szToAddress'],'To'); 
    $ccEmailAry = format_emails($logEmailsArys['szCCEmailAddress'],'Cc');
    $bccEmailAry = format_emails($logEmailsArys['szBCCEmailAddress'],'Bcc');

    if(!empty($ccEmailAry))
    {
        $finalEmailAry = array_merge($finalEmailAry,$ccEmailAry);
    }
    if(!empty($bccEmailAry))
    {
        $finalEmailAry = array_merge($finalEmailAry,$bccEmailAry);
    }
    return $finalEmailAry;
}
function build_document_url($type,$idBooking)
{
    $szUrl = '';
    $szDisplayName = '';
    if($type=='BOOKING_INVOICE')
    {
        $szUrl = __BASE_MANAGEMENT_URL__."/invoice/".$idBooking."/";
        $szDisplayName = "Booking Invoice";
    }
    else if($type=='BOOKING_NOTICE')
    {
        $szUrl = __BASE_MANAGEMENT_URL__."/forwarderBooking/".$idBooking."/";
        $szDisplayName = "Booking Notice";
    }
    else if($type=='BOOKING_CONFIRMATION')
    {
        $szUrl = __BASE_MANAGEMENT_URL__."/viewForwarderBookingConfirmation/".$idBooking."/";
        $szDisplayName = "Booking Confirmation";
    } 
    else if($type=='BOOKING_INSURANCE_INVOICE')
    {
        $szUrl = __BASE_MANAGEMENT_URL__."/viewInsuranceInvoice/".$idBooking."/";
        $szDisplayName = "Booking Insurance Invoice";
    }
    else if($type=='BOOKING_INSURANCE_CREDIT_NOTE')
    {
        $szUrl = __BASE_MANAGEMENT_URL__."/viewInsuranceCreditNote/".$idBooking."/";
        $szDisplayName = "Booking Insurance Credit Note";
    }
    else if($type=='CREDIT_NOTE')
    {
        $szUrl = __BASE_MANAGEMENT_URL__."/creditNote/".$idBooking."/";
        $szDisplayName = "Credit Note";
    }
    else if($type=='SELF_CREDIT_NOTE')
    {
        $szUrl = __BASE_MANAGEMENT_URL__."/selfcreditNote/".$idBooking."/";
        $szDisplayName = "Self Credit Note";
    }
    $ret_ary = array();
    $ret_ary['szUrl'] = $szUrl;
    $ret_ary['szDisplayName'] = $szDisplayName;
    return $ret_ary;
}

function display_transporteca_new_task_popup($kBooking)
{
    if(!empty($_POST['addTransportecaTask']))
    {
        $addTransportecaTask = $_POST['addTransportecaTask'];
        $idBookingFile = $addTransportecaTask['idBookingFile'];
        $dtTaskDate = $addTransportecaTask['dtTaskDate'];
        $dtTaskTime = $addTransportecaTask['dtTaskTime'];
        $szReminderNotes = $addTransportecaTask['szReminderNotes'];
    }
    else
    {
        $idBookingFile = $kBooking->idBookingFile;
         
        $reminderNotesAry = array();
        $reminderNotesAry = format_reminder_date_time($dtReminderDateTime);
        $dtTaskDate = $reminderNotesAry['dtReminderDate'];
        $dtTaskTime = $reminderNotesAry['szReminderTime'];
    } 
    $kAdmin_new = new cAdmin();
    $customerOwnerAry = array();
    $customerOwnerAry = $kAdmin_new->viewManagementDetails(false,false,true);
    $date_picker_argument = "minDate: new Date(".date('Y').", ".date('m')."-1, ".date('d').")" ;
    ?>
    <script type="text/javascript"> 
        $().ready(function() {	
            $("#dtTaskDate").datepicker({<?php echo $date_picker_argument; ?>});  
        });
    </script>
    <div id="popup-bg" onclick="showHide('content_body')"></div>
    <div id="popup-container">
        <div class="popup contact-popup" style="width:500px;"> 
            <h4><strong>Add a new task to file</strong></h4>
            <form id="cms_operation_add_new_admin_task" name="cms_operation_add_new_admin_task" action="javascript:void(0);">
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:60%;">When should the task appear in Pending Tray?</span>
                    <span class="quote-field-container wds-20">
                        <input type="text" name="addTransportecaTask[dtTaskDate]" id="dtTaskDate" placeholder="" value="<?php echo $dtTaskDate; ?>" onkeyup="submit_link_customer_by_enter(event,'cms_operation_search_user_email_form','<?php echo $idCrmEmail; ?>')" onfocus="check_form_field_not_required_courier(this.form.id,this.id);">
                    </span>
                    <span class="quote-field-container wds-20">
                        <input type="text" name="addTransportecaTask[dtTaskTime]" id="dtTaskTime" placeholder="" value="<?php echo $dtTaskTime; ?>" onblur="format_reminder_time(this.value,2);" onfocus="check_form_field_not_required_courier(this.form.id,this.id);">
                    </span>
                </div>  
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <textarea name="addTransportecaTask[szReminderNotes]" id="szReminderNotes_New_Task_popup" rows="15" placeholder="Type text here" onfocus="check_form_field_not_required_courier(this.form.id,this.id);"><?php echo $szReminderNotes; ?></textarea>
                </div>
                <div class="clearfix email-fields-container" style="width:100%;">
                    Assign task to:<br>
                    <a href="javascript:void(0);" onclick="submit_new_transporteca_task_form('cms_operation_add_new_admin_task','0')">Not Allocated</a>
                    <?php 
                        if(!empty($customerOwnerAry))
                        {
                            foreach($customerOwnerAry as $customerOwnerArys)
                            {
                                ?>
                                &nbsp;|&nbsp;<a href="javascript:void(0);" id="add_task_admin_link_<?php echo $customerOwnerArys['id']; ?>" onclick="submit_new_transporteca_task_form('cms_operation_add_new_admin_task','<?php echo $customerOwnerArys['id']; ?>')"><?php echo $customerOwnerArys['szFirstName']." ".$customerOwnerArys['szLastName']?></a>
                                <?php
                            }
                        }
                    ?>
                </div>
                <input type="hidden" name="addTransportecaTask[idBookingFile]" id="idBookingFile" value="<?php echo $idBookingFile; ?>">
            </form>   
            <div class="btn-container" style="text-align: center;padding:10px 0;">
                <a href="javascript:void(0)" onclick="showHide('content_body')" class="button1"><span>Cancel</span></a>
            </div>
        </div>
    </div> 
    <?php
    if(!empty($kBooking->arErrorMessages))
    { 
        $formId = "cms_operation_add_new_admin_task";
        display_form_validation_error_message($formId,$kBooking->arErrorMessages);
    }
}

function display_overview_task_details($logEmailsArys,$kBooking,$szFromPage)
{
    $bShowCloseTaskLink = true; 
    if($logEmailsArys['iClosed']==1)
    {
        $idTaskOwner = $logEmailsArys['idAdminClosedBy'];
        $bShowCloseTaskLink = false;
        $szTitle = "Completed";
        $taskDateTime=$logEmailsArys['dtClosedOn'];
    }
    else
    {
        $idTaskOwner = $logEmailsArys['idTaskOwner'];
        $szTitle = "Due";
        $taskDateTime=$logEmailsArys['dtRemindDate'];
    } 
    $kAdmin = new cAdmin();
    $szTaskOwnedBy = "";  
    if($idTaskOwner>0)
    {
        $kAdmin->getAdminDetails($idTaskOwner); 
        $szTaskOwnedBy = " - ".$kAdmin->szFirstName." ".$kAdmin->szLastName ;
    }
    else
    {
        $szTaskOwnedBy = " - Not allocated";
    }
    $szAdminAddedBy = ""; 
    if(!empty($logEmailsArys['szAdminName']))
    {
        $szAdminAddedBy = " - ".$logEmailsArys['szAdminName'] ;
    }
    $idBookingFile = $kBooking->idBookingFile; 
    $bDisplayShowMoreClicked = false;
    $bLongBodyMessage = false;
    if($kBooking->szTaskStatus=='T220' && $kBooking->idReminderTask==$logEmailsArys['id'])
    {
        $bDisplayShowMoreClicked = true;
    }  
?> 
    <div class="clearfix email-fields-container" style="width:100%;" id="overview_task_pending_tray_<?php echo $logEmailsArys['id']; ?>">
        <span class="quote-field-container wd-10"><strong>Task:</strong> </span>
        <span class="quote-field-container wd-40">
            <?php if(strlen($logEmailsArys['szEmailBody'])>200){ $bLongBodyMessage = true; ?>
            <span id="display_less_content">
                <?php echo substr($logEmailsArys['szEmailBody'],0,197)."..."; ?><a href="javascript:void(0);" onclick="toggleReminderContent('display_full_content','display_less_content');">show more</a>
            </span>
            <span id="display_full_content" style="display:none;">
                <?php echo nl2br($logEmailsArys['szEmailBody']); ?>  <a href="javascript:void(0);" onclick="toggleReminderContent('display_less_content','display_full_content');">show less</a>
            </span> 
            <?php }else { echo nl2br($logEmailsArys['szEmailBody']); } ?> 
        </span> 
        <div class="quote-field-container wd-10">
            <p><strong>Added: </strong></p>
            <p><strong><?php echo $szTitle; ?>: </strong></p>
        </div>
        <div class="quote-field-container wd-40">
            <p><?php echo date('d-M-Y H:i',strtotime($logEmailsArys['dtSent'])).$szAdminAddedBy; ?></p>
            <p>
                <?php echo date('d-M-Y H:i',strtotime($taskDateTime)).$szTaskOwnedBy; ?>
                <?php if($bShowCloseTaskLink){?>
                    <a href="javascript:void(0)" onclick="close_reminder_task('<?php echo $szFromPage; ?>','<?php echo $logEmailsArys['id']; ?>','<?php echo $idBookingFile; ?>','CLOSE')">Close task</a>
                <?php } else { ?>
                    <a href="javascript:void(0)" onclick="close_reminder_task('<?php echo $szFromPage; ?>','<?php echo $logEmailsArys['id']; ?>','<?php echo $idBookingFile; ?>','OPEN')">Open task</a>
                <?php } ?>
            </p>
        </div>
    </div>  
    <span id="overview_task_pending_tray_hidden_span_<?php echo $logEmailsArys['id']; ?>"></span>
    <div class="clearfix email-fields-container" style="width:100%;"><hr></div>
    <script type="text/javascript">
        <?php if($bDisplayShowMoreClicked  && $bLongBodyMessage) {    ?>
            toggleReminderContent('display_full_content','display_less_content');
        <?php  } ?>
    </script>
    <?php
    
}

function display_crm_outgoing_emails($logEmailsArys,$kBooking,$szFromPage)
{
    if($logEmailsArys['iSuccess']==3) //Email Opened
    {
        $logEmailsArys['dtEmailStatusUpdated'] = getDateCustom($logEmailsArys['dtEmailStatusUpdated']);
        /*
        if(date('I')==1)
        {  
            //Our server's My sql time is 2 hours behind so that's why added 2 hours to sent date 
            $logEmailsArys['dtEmailStatusUpdated'] = date('Y-m-d H:i:s',strtotime("+2 HOUR",strtotime($logEmailsArys['dtEmailStatusUpdated'])));  
        }  
        else
        {
            $logEmailsArys['dtEmailStatusUpdated'] = date('Y-m-d H:i:s',strtotime("+1 HOUR",strtotime($logEmailsArys['dtEmailStatusUpdated'])));  
        }
         * 
         */
        $szOpenedText = date('d-M-Y H:i',strtotime($logEmailsArys['dtEmailStatusUpdated']));
    }
    else if($logEmailsArys['iSuccess']==4) //Email Bounced
    { 
        if(!empty($logEmailsArys['szEmailStatus']))
        {
            if(strtolower($logEmailsArys['szEmailStatus'])=='bounce')
            {
                $szOpenedText = "Bounced";
            }
            else
            {
                $szOpenedText = $logEmailsArys['szEmailStatus'];
            } 
            $szOpenedText = ucfirst($szOpenedText);
        }
        else
        {
            $szOpenedText = "Bounced";
        } 
    }
    else
    {  
        $szOpenedText = "Not Opened";
    }
    
    $idBookingFile = $kBooking->id;
    $idEmailLogs = $kBooking->idEmailLogs;
    
    $finalEmailAry = array();
    $finalEmailAry = getFormatedEmails($logEmailsArys);

    $attachmentAry = array(); 
    $iAttachmentCount = 0;
    if($logEmailsArys['iHasAttachments']==1 && !empty($logEmailsArys['szAttachmentFileName']))
    {
        $attachmentAry = unserialize($logEmailsArys['szAttachmentFileName']);
        $iAttachmentCount = count($attachmentAry);
    }
    $standardTextAry = array('BOOKING_INVOICE','BOOKING_NOTICE','BOOKING_CONFIRMATION','BOOKING_INSURANCE_INVOICE');
    //display_crm_email_details('<?php echo $logEmailsArys['id']; ','<?php echo $idBookingFile; ')
?>
    <div class="clearfix email-fields-container" style="width:100%;">
        <span class="quote-field-container wd-10"><strong>From Email: </strong></span>
        <span class="quote-field-container wd-40">
            <?php echo $logEmailsArys['szFromEmail']; ?>   
        </span>
        <span class="quote-field-container wd-10"><strong>To Email:</strong> </span>
        <span class="quote-field-container wd-40">
            <?php 
                if(count($finalEmailAry)>1)
                { 
                    ?> 
                    <span class="reassign-button-container" style="position:relative;">  
                        <span onmouseover="showHideReAssignDrodown('<?php echo "crm-email-sender-list-container-".$logEmailsArys['id']; ?>','<?php echo $logEmailsArys['id']; ?>');" onmouseout="showHideReAssignDrodown('<?php echo "crm-email-sender-list-container-".$logEmailsArys['id']; ?>','<?php echo $logEmailsArys['id']; ?>');">
                            <?php echo str_replace("To: ", "", $finalEmailAry[0]); ?>
                            <a href="javascript:void(0);" style="text-decoration:none;" onclick="$('crm-email-sender-list-container-<?php echo $logEmailsArys['id']; ?>').show();"><strong>+</strong></a>
                        </span> 
                        <div style="display:none;" class="calender_more_popup" id="crm-email-sender-list-container-<?php echo $logEmailsArys['id']; ?>">   
                            <ul>   
                                <?php
                                    if(!empty($finalEmailAry))
                                    { 
                                        foreach($finalEmailAry as $finalEmailArys)
                                        { 
                                            ?>
                                            <li><?php echo $finalEmailArys; ?></li>
                                            <?php 
                                        }
                                    }
                                ?>
                            </ul>  
                        </div> 
                    </span>
                    <?php
                }
                else
                {
                    echo $logEmailsArys['szToAddress']; 
                }
            ?>
         </span>  
    </div> 
    <div class="clearfix email-fields-container" style="width:100%;">
        <span class="quote-field-container wd-10"><strong>Subject:</strong></span>
        <span class="quote-field-container wd-40"><?php echo  utf8_decode($logEmailsArys['szEmailSubject']); ?></span>
        <span class="quote-field-container wd-10"><strong>Sent: </strong></span>
        <span class="quote-field-container wd-40"><?php echo date('d-M-Y H:i',strtotime($logEmailsArys['dtSent']))." / ".$szOpenedText; ?> &nbsp;&nbsp; 
            <a href="javascript:void(0);" onclick="open_close_log('email_body_<?php echo $logEmailsArys['id']; ?>','<?php echo $logEmailsArys['id']; ?>');" id="email_view_details_link_<?php echo $logEmailsArys['id']; ?>">View Details</a> |
            <a href="javascript:void(0);" onclick="resend_system_messages('<?php echo $logEmailsArys['id']; ?>','<?php echo $szFromPage; ?>','<?php echo $idBookingFile; ?>')">Resend</a> 
        </span>
    </div>
    <div class="clearfix email-fields-container" style="width:100%;">
        <div id="email_body_<?php echo $logEmailsArys['id']; ?>" class ="email_body_container" style="display:none;">
           <?php echo display_outgoing_email_details($logEmailsArys,$idBookingFile,$szFromPage); ?>
        </div>
    </div> 
    <script>
        $('#email_body_html_<?php echo $logEmailsArys['id']; ?>').each(function() {
            $(this).find('a').attr('target','_blank');
      });
    </script>
    <div class="clearfix email-fields-container" style="width:100%;"><hr></div>
    
    <?php
}
function displayDelayedResponseError()
{
    $szPendingTrayURL = __MANAGEMENT_OPERATIONS_PENDING_TRAYS__;
    ?>
    <div id="popup-bg" onclick="showHide('delayed_message_container_div')"></div>
    <div id="popup-container">
        <div class="popup contact-popup" style="width:350px;">  
            <h4><strong>Delayed Response</strong></h4>
            <div class="clearfix email-fields-container" style="width:100%;">
                System is taking longer time to auto reload pending tray list than usual. Please reload the page to start the process again.
            </div>
            <div class="btn-container" style="text-align: center;padding:10px 0;">
                <a href="javascript:void(0)" onclick="showHide('delayed_message_container_div')" class="button1"><span>close</span></a>
                <a href="javascript:void(0)" onclick="redirect_url('<?php echo $szPendingTrayURL; ?>')" class="button1"><span>Reload</span></a>
            </div>
        </div>
    </div>
    <?php
}

function removeEmojiNew($text)
{
    $cleanText = "";
    
    // Match ? icon
    $cleanText = preg_replace_callback('/[^\x00-\x7F]+/', '', $text);     
     
    // Match Emoticons
    $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
    $cleanText = preg_replace_callback($regexEmoticons, '', $cleanText);

    // Match Miscellaneous Symbols and Pictographs
    $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
    $cleanText = preg_replace_callback($regexSymbols, '', $cleanText);

    // Match Transport And Map Symbols
    $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
    $cleanText = preg_replace_callback($regexTransport, '', $cleanText);

    return $cleanText;
} 

function display_outgoing_email_details($logEmailsArys,$idBookingFile,$szFromPage)
{
    $attachmentAry = array(); 
    $iAttachmentCount = 0;
    if($logEmailsArys['iHasAttachments']==1 && !empty($logEmailsArys['szAttachmentFileName']))
    {
        $attachmentAry = unserialize($logEmailsArys['szAttachmentFileName']);
        $iAttachmentCount = count($attachmentAry);
    }
    $standardTextAry = array('BOOKING_INVOICE','BOOKING_NOTICE','BOOKING_CONFIRMATION','BOOKING_INSURANCE_INVOICE','BOOKING_INSURANCE_CREDIT_NOTE','CREDIT_NOTE','SELF_CREDIT_NOTE');
    ?>
     <div class="email-content" id="email_body_html_<?php echo $logEmailsArys['id']; ?>">
        <?php 
        /*$logEmailsArys['szEmailBody']  = preg_replace_callback("#(<br>){3,}#", "\\1", $logEmailsArys['szEmailBody'] );*/
        $logEmailsArys['szEmailBody'] = str_replace("</br>", "", $logEmailsArys['szEmailBody']);
        /*$logEmailsArys['szEmailBody']  = preg_replace_callback("/(<br>\s*|<br \/>\s*){3,}/i", "<br><br>", $logEmailsArys['szEmailBody'] );*/
        if((int)$logEmailsArys['iAlreadyUtf8Encoded']==1)
        {
            echo (trim($logEmailsArys['szEmailBody'])); 
        }
        else
        {
            echo utf8_decode(trim($logEmailsArys['szEmailBody'])); 
        } 
        ?>
        </div>
        <?php    
        if($iAttachmentCount>0)
        {
            echo '<br><br>';
            foreach($attachmentAry as $attachmentArys)
            {
                if(in_array($attachmentArys,$standardTextAry))
                {
                    $documentAry = array();
                    $documentAry = build_document_url($attachmentArys,$logEmailsArys['idBooking']);
                    ?>
                    <a href="<?php echo $documentAry['szUrl']; ?>" target="_blank" style="text-decoration:none;"><?php echo "Attachment ".++$ctr.": ".$documentAry['szDisplayName']; ?></a><br>
                    <?php
                }
                else
                {
                    ?>
                    <a href="<?php echo __BASE_URL_EMAIL_ATTACHMENT__."/".$attachmentArys; ?>" target="_blank" style="text-decoration:none;"><?php echo "Attachment ".++$ctr.": ".$attachmentArys; ?></a><br>
                    <?php
                } 
            }
        }
        ?>
        <div class="clearfix dotted-line" style="width:100%;">&nbsp;</div>
        <div id="crm_buttons_main_container_<?php echo $logEmailsArys['id']; ?>" class="clearfix email-fields-container crm-button-main-container" style="width:100%;">
            <?php echo display_crm_email_buttons($logEmailsArys,$idBookingFile,$szFromPage); ?>
        </div>
    <?php
}

function display_crm_email_details($logEmailsArys,$idBookingFile,$szFromPage)
{
    if($logEmailsArys['iIncomingEmail']==1) 
    {   
        echo display_incoming_email_details($logEmailsArys,$idBookingFile,$szFromPage);
    }  
    else 
    { 
        echo display_outgoing_email_details($logEmailsArys,$idBookingFile,$szFromPage);
    }
}

function display_reminder_log($logEmailsArys)
{
    $szAdminAddedBy = ""; 
    if(!empty($logEmailsArys['szAdminName']))
    {
        $szAdminAddedBy = " - ".$logEmailsArys['szAdminName'] ;
    }  
?>
<div class="clearfix email-fields-container" style="width:100%;">
    <span class="quote-field-container wd-10"><strong>Note:</strong></span>
    <span class="quote-field-container wd-40">
        <?php if(strlen($logEmailsArys['szEmailBody'])>200){ ?>
        <span id="display_less_content">
                <?php echo substr($logEmailsArys['szEmailBody'],0,197)."..."; ?><a href="javascript:void(0);" onclick="toggleReminderContent('display_full_content','display_less_content');">show more</a>
        </span>
        <span id="display_full_content" style="display:none;">
            <?php echo nl2br($logEmailsArys['szEmailBody']); ?>  <a href="javascript:void(0);" onclick="toggleReminderContent('display_less_content','display_full_content');">show less</a>
        </span> 
        <?php }else { echo nl2br($logEmailsArys['szEmailBody']); } ?> 
    </span> 
    <span class="quote-field-container wd-10"><strong>Added: </strong> <br><br> <strong>File: </strong></span>
    <span class="quote-field-container wd-40"><?php echo date('d-M-Y H:i',strtotime($logEmailsArys['dtSent'])).$szAdminAddedBy; ?><br><br><?php echo $logEmailsArys['szBookingFileNumber']; ?></span>
</div>  
<div class="clearfix email-fields-container" style="width:100%;"><hr></div> 
<?php 
}

function display_booking_searched_logs($logEmailsArys)
{
    ?>
    <div class="clearfix email-fields-container" style="width:100%;">
        <span class="quote-field-container wd-10"><strong>From:</strong></span>
        <span class="quote-field-container wd-40"><?php echo $logEmailsArys['szFromLocation']; ?></span>
        <span class="quote-field-container wd-10"><strong>To:</strong></span>
        <span class="quote-field-container wd-40"><?php echo $logEmailsArys['szToLocation']; ?></span>
    </div>    
    <?php
        if($logEmailsArys['iQuickQuote']==1)
        {
            ?>
            <div class="clearfix email-fields-container" style="width:100%;">
                <span class="quote-field-container wd-10"><strong>Type:</strong></span>
                <span class="quote-field-container wd-40"><?php echo "Quick quote by ".$logEmailsArys['szAdminName']; ?></span>
                <span class="quote-field-container wd-10"><strong>Created:</strong></span>
                <span class="quote-field-container wd-40"><?php echo date('d-M-Y H:i',strtotime($logEmailsArys['dtSent'])); ?></span>
            </div> 
            <?php
        }
        else
        {
            ?>
            <div class="clearfix email-fields-container" style="width:100%;">
                <span class="quote-field-container wd-10"><strong>Date & Cargo:</strong></span>
                <span class="quote-field-container wd-40"><?php echo date('d. M Y',strtotime($logEmailsArys['dtTimingDate']))." - ".format_volume($logEmailsArys['fCargoVolume'])."cbm / ".number_format((float)$logEmailsArys['fCargoWeight'])."kg"; ?></span>
                <span class="quote-field-container wd-10"><strong>Searched:</strong></span>
                <span class="quote-field-container wd-40"><?php echo date('d-M-Y H:i',strtotime($logEmailsArys['dtSent'])); ?></span>
            </div>  
            <?php
        }
    ?> 
    <div class="clearfix email-fields-container" style="width:100%;"><hr></div>
    <?php
}

function display_zopim_chat_details($logEmailsArys,$idBookingFile,$idOfflineChat=false)
{
    $bOpenedChatBox = false;
    if($idOfflineChat>0 && $idOfflineChat==$logEmailsArys['id'])
    {
        $bOpenedChatBox = true;
    }
    $szDivId = "zopim_chat_transcript_".$logEmailsArys['id'];
    $kChat = new cChat();
    if($bOpenedChatBox){ ?>
    <script type="text/javascript">
        $("#zopim_chat_transcript_"+'<?php echo $logEmailsArys['id']; ?>'+"_link").unbind("click"); 
        $("#zopim_chat_transcript_"+'<?php echo $logEmailsArys['id']; ?>'+"_link").click(function(){ showHidechatmessage('<?php echo $szDivId; ?>','HIDE'); });
    </script>
    <?php } ?> 
    <div class="clearfix email-fields-container" style="width:100%;">
        <span class="quote-field-container wd-10"><strong>Chat agent:</strong></span>
        <span class="quote-field-container wd-40"><?php echo !empty($logEmailsArys['szAgentName'])?$logEmailsArys['szAgentName']:'N/A'; ?></span> 
        <span class="quote-field-container wd-10"><strong>Date:</strong></span> 
        <span class="quote-field-container wd-40">
            <?php echo date('d-M-Y H:i',strtotime($logEmailsArys['dtSent'])); ?>
            &nbsp;&nbsp;<a href="javascript:void(0)" <?php if(!$bOpenedChatBox){ ?> onclick="display_chat_transcript('<?php echo $logEmailsArys['id']; ?>')" <?php } ?> id="zopim_chat_transcript_<?php echo $logEmailsArys['id']."_link"; ?>">View Detail</a>
            &nbsp;&nbsp;<a href="javascript:void(0)" onclick="display_task_menu_details('REMIND','<?php echo $idBookingFile; ?>','DISPLAY_TASK_FORM_MENU','<?php echo __OFFLINE_REMIND_TEMPLATE_ID__; ?>')" id="zopim_chat_transcript_reply_<?php echo $logEmailsArys['id']."_link"; ?>">Reply</a>
            &nbsp;&nbsp;<a href="javascript:void(0)" onclick="closeTask('<?php echo $idBookingFile; ?>')" id="zopim_chat_transcript_close_task_<?php echo $logEmailsArys['id']."_link"; ?>">Close task</a>
        </span>
    </div> 
    <div class="clearfix email-fields-container" style="width:100%;" id="zopim_chat_transcript_<?php echo $logEmailsArys['id']; ?>">
    <?php
        if($bOpenedChatBox)
        {
            $zopimChatTranscriptAry = array();
            $zopimChatTranscriptAry = $kChat->getAllZopimChatMessageLogs($idOfflineChat);

            $zopimChatAry = array();
            $zopimChatAry = $kChat->getAllZopimChatLogs(false,$idOfflineChat);

            echo display_zopim_chat_transcript($zopimChatTranscriptAry,$zopimChatAry[0]); 
        }
    ?>
    </div> 
    <div class="clearfix email-fields-container" style="width:100%;"><hr></div> 
    <?php
}

function showoverwritepopup($kConfig,$idBookingFile,$idNewTemplate,$szTaskStatus,$iTemplateLanguage,$closeFlag)
{?>
    <div id="popup-bg"></div>
    <div id="popup-container">
        <div class="popup contact-popup" style="width:350px;">  
            <h4><strong>Overwrite e-mail template</strong></h4>
            <p>
                A template with this name already exists in this language. You can either overwrite the old e-mail template, or go back and change the name of the e-mail template.
            </p>
            <div class="btn-container" style="text-align: center;padding:10px 0;">
                <a href="javascript:void(0)" onclick="showEmailTemplatepopupHideOverWritePopup();" class="button1"><span>back</span></a>
                <a href="javascript:void(0)" onclick="saveRemindEmailTemplateDetail('UPDATE_REMIND_EMAIL_TEMPLATE','<?php echo $szTaskStatus;?>','<?php echo $idBookingFile;?>','<?php echo $idNewTemplate;?>','<?php echo $iTemplateLanguage;?>','<?php echo $closeFlag;?>')" class="button1"><span>overwrite</span></a>
            </div>
        </div>
    </div>
    <?php
}
?>