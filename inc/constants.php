<?php 
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
/**
 * constants.php
 *
 * @copyright Copyright (C) 2009 Transporteca
 * @author Anil 
 */

/**
 * DB Connections Constant Definitions.
 *
 */
require_once(realpath(dirname(__FILE__)).'/../environment.php');

if(__ENVIRONMENT__ == "ANILDEV")
{
    define( "__DBC_HOST__", "localhost" );
    define( "__DBC_USER__", "root" );
    define( "__DBC_PASSWD__", "" );//abkjri3u
    define( "__DBC_PORT__", "3306" ); 
    
    if(__SUB_ENVIRONMENT__=='WHIZ1')
    {
        define( "__DBC_SCHEMATA__", "transportecadb"); 
        define ("__APP_PATH_ROOT__", "/xampp/htdocs/transporteca-bb"); 
    }
    else if(__SUB_ENVIRONMENT__=='WHIZ2')
    {
        define( "__DBC_SCHEMATA__", "transportecadb"); 
        define ("__APP_PATH_ROOT__", "/xampp/htdocs/transporteca-bb"); 
    } 
    
    define('__USE_SECURE__', false);

    define("__DAY_CONVERSION_UTC__",0);
    
    define("__DISPLAY_CALCULATION_DETAILS__",true);
    define("__MAIN_SITE_HOME_PAGE_URL__",'http://www.a.transport-eca.com');
    define("__MAIN_SITE_HOME_PAGE_URL_WITHOUT_HTTP__",'www.a.transport-eca.com'); 
    define("__MAIN_SITE_HOME_PAGE_SECURE_URL__",'http://www.a.transport-eca.com');
    define('__PAYPAL_PAYMENT_URL_SANDBOX__','https://www.sandbox.paypal.com/cgi-bin/webscr');
    define('__BUSINESS_PAYPAL_ACCOUNT__','ashish@whiz-solutions.com');
    define('__PAYPAL_PAYMENT_URL_LIVE__','https://www.paypal.com/cgi-bin/webscr');
    define('__PAYPAL_PAYMENT_URL_SANDBOX_TOKEN__','cDizjVSOdRYfxNY_Kr8xiIrbWpQjO066x5cBOkHmz5St0EOc-dxzuzcEzfW');
    define('__PAYPAL_PAYMENT_URL_TOKEN_LIVE__','cDizjVSOdRYfxNY_Kr8xiIrbWpQjO066x5cBOkHmz5St0EOc-dxzuzcEzfW');

    define('__PAYPAL_PAYMENT_URL_SANDBOX_HOST__','www.sandbox.paypal.com');
    define('__PAYPAL_PAYMENT_URL_LIVE_HOST__','www.sandbox.paypal.com');
    define('__LIVE_SSL_FLAG__',false);

    define("__PAYPAL_LIVE__",false);

    //zooz api details
    define("__API_KEY__","9365cd90-8092-410e-9287-b19e5cbdd87e");
    define("__API_UNIQUE_KEY__","com.application.transport");
    define("__URL_FLAG__",true);
    define("__API_DEVELOPER_ID__","ashish@whiz-solutions.com");
    define("__API_DEVELOPER_KEY__","IQRCETQJ7PISG56CUOCNYYY3LQ");
     

    define("__BASE_STORE_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/images");
 
    define("__BASE_MANAGEMENT_URL__", "http://management.a.transport-eca.com");
    define("__BASE_SECURE_MANAGEMENT_URL__", 'http://management.a.transport-eca.com');
    
    define("__BASE_METRICS_URL__", "http://www.a.transport-eca.com/metrics");
    define("__BASE_SECURE_METRICS_URL__", 'http://www.a.transport-eca.com/metrics');
     
    define( "__CAPSULE_API_USER_NAME__", "a814f6c73e220df112bb4df5aa8965ec");
    define( "__CAPSULE_API_PASSWORD__", "x" );
    define( "__CAPSULE_API_HOST_URL__", "https://whiz.capsulecrm.com" );  
    define( "__FEDEX_DEVELOPER_KEY__", "9BMKKAQH9XvTjCyg" );
    define( "__FEDEX_DEVELOPER_PASSWORD__", "dd60IwdrvQn1AoduIhmCE34Sp");
    define( "__FEDEX_ACCOUNT_NUMBER__", "342409865" );
    define( "__FEDEX_METER_NUMBER__", "106406688"); 
    define( "__UPS_ACCESS_KEY__", "2CE5BE3A18751274");
    define( "__UPS_API_USER_ID__", "hXuU27wq" );
    define( "__UPS_API_PASSWORD__", "7yp6Xaax" );  
    define("__ID_ANONYMUS_USER__",908); 
    define( "__EASYPOST_API_KEY__", "9RT5CVDLLBAVV7nwD1pY6w" );  
    define( "__STRIPE_API_KEY__", "pk_test_Haw0VsywabhVPV8i6ivOJ7D5"); 
    define( "__STRIPE_API_SECRET_KEY__", "sk_test_56OWhfOUYSUdpq45VeKNN2Rl");  
    define("__AFTER_SHIP_API_KEY__","a02bd542-9c43-4ef2-a000-2b847dbb1cd8");
    
    define( "__FB_API_ID_KEY__", "231695233840841");  
    define("__FB_API_SECRET_KEY__","3446c92ad45d8223d6299f2e16a4d5f9");
    
    define("__GOOGLE_PLUS_CLIENT_ID__","839612770288-k0fb207csjmtgsfgcmuo344nf8m022ip.apps.googleusercontent.com");
    define("__GOOGLE_PLUS_CLIENT_SECRET__","eSX20j5vbqzMBOifh9feAjFH");
    
    define("__GOOGLE_PLUS_REDIRECT_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__.'/googleLogin');
    
    define( "__MAUTIC_API_BASE_URL__", "https://whiz.mautic.com");
    
    define("__WORDPRESS_FOLDER__","content");
    define("__STANDARD_AGREEMENT_ID__","4");
     
    define("__GMAIL_HOST_NAME__","imap.gmail.com");
    define("__GMAIL_HOST_PORT__","993"); 
    define("__GMAIL_USER_NAME__","whizsolutions16@gmail.com");
    define("__GMAIL_PASSWORD__","Test1234!*"); 
    /*
    * Zopim API credentials
    */
    define("__ZOPIM_CHAT_BASE_URL__","https://www.zopim.com/api/v2/chats");
    define("__ZOPIM_CHAT_USER_NAME__","ajay@whiz-solutions.com");
    define("__ZOPIM_CHAT_PASSWORD__","Test1234");
    
    define("__OFFLINE_REMIND_TEMPLATE_ID__","23"); 
    define("__SHOW_VAT_WITHOUT_MARKUP__","2016-08-29"); 
    
    define("__POSTMEN_API_KEY__","22a35914-3345-463b-9cdc-5daf60409b98");
    define("__POSTMEN_API_ENDPOINT__","https://production-api.postmen.com/v3/rates"); 
}
else if(__ENVIRONMENT__ == "ONLINE")
{
    define( "__DBC_HOST__", "mysql51-003.wc1.dfw1.stabletransit.com" );
    define( "__DBC_USER__", "693463_trans" );
    define( "__DBC_PASSWD__", "Transporteca1" ); 
    define( "__DBC_SCHEMATA__", "693463_transporteca");
    define( "__DBC_PORT__", "3306" ); 
 
    define ( "__APP_PATH_ROOT__", "/mnt/stor2-wc1-dfw1/400738/693463/transporteca.xeroda.com/web/content" );
    define('__USE_SECURE__', false);
    
    define("__DAY_CONVERSION_UTC__",86400);
    define("__DISPLAY_CALCULATION_DETAILS__",true);
    define("__MAIN_SITE_HOME_PAGE_URL__",'http://transporteca.xeroda.com');
    define("__MAIN_SITE_HOME_PAGE_SECURE_URL__",'https://transporteca.xeroda.com');
	
    //zooz api details
    define("__API_KEY__","4c60588d-8226-4d84-9391-aea31bcc7d87");
    define("__API_UNIQUE_KEY__","transporteca.com");
    define("__URL_FLAG__",true);
    define("__API_DEVELOPER_ID__","ashish@whiz-solutions.com");
    define("__API_DEVELOPER_KEY__","IQRCETQJ7PISG56CUOCNYYY3LQ");

    define("__BASE_STORE_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/images");

    define ("__BASE_MANAGEMENT_URL__", "http://devmanagement.transporteca.com");
    define ("__BASE_SECURE_MANAGEMENT_URL__", 'https://devmanagement.transporteca.com');
    
    define ("__BASE_METRICS_URL__", "http://dev.transporteca.com/metrics");
    define ("__BASE_SECURE_METRICS_URL__", 'http://dev.transporteca.com/metrics');
     
    define( "__CAPSULE_API_USER_NAME__", "92b17614ea700d332b299bc54df5d2ff" );
    define( "__CAPSULE_API_PASSWORD__", "x" );
    define( "__CAPSULE_API_HOST_URL__", "https://transporteca.capsulecrm.com" ); 
    
    define("__ID_ANONYMUS_USER__",0);
    define( "__EASYPOST_API_KEY__", "9RT5CVDLLBAVV7nwD1pY6w" ); 
    
    define( "__STRIPE_API_KEY__", "pk_test_Haw0VsywabhVPV8i6ivOJ7D5"); 
    define( "__STRIPE_API_SECRET_KEY__", "sk_test_56OWhfOUYSUdpq45VeKNN2Rl");
    define("__AFTER_SHIP_API_KEY__","a02bd542-9c43-4ef2-a000-2b847dbb1cd8");
    
    define( "__MAUTIC_API_BASE_URL__", "https://whiz.mautic.com");
    
    define("__WORDPRESS_FOLDER__","wordpress");
    
    define("__GMAIL_HOST_NAME__","imap.gmail.com");
    define("__GMAIL_HOST_PORT__","993"); 
    define("__GMAIL_USER_NAME__","whizsolutions16@gmail.com");
    define("__GMAIL_PASSWORD__","Test1234!*");
    
    /*
    * Zopim API credentials
    */
    define("__ZOPIM_CHAT_BASE_URL__","https://www.zopim.com/api/v2/chats");
    define("__ZOPIM_CHAT_USER_NAME__","ajay@whiz-solutions.com");
    define("__ZOPIM_CHAT_PASSWORD__","Test1234");
    define("__OFFLINE_REMIND_TEMPLATE_ID__","23"); 
    define("__SHOW_VAT_WITHOUT_MARKUP__","2016-08-29"); 
}
else if(__ENVIRONMENT__ == "LIVE")
{
    
    define( "__DBC_HOST__", "apiserver-cluster.cluster-cjqjvr5itgtx.eu-west-1.rds.amazonaws.com" ); //api.cluster-cjqjvr5itgtx.eu-west-1.rds.amazonaws.com
    define( "__DBC_USER__", "awsuser" );
    define( "__DBC_PASSWD__", "RI8SpShLS1j59O4vdPUELHthpa4rE70L" );//
    define( "__DBC_PORT__", false );  
    define( "__DBC_SCHEMATA__", "transporteca");
   // define( "__DBC_HOST__", "localhost" );
//    define( "__DBC_USER__", "root" );
//    define( "__DBC_PASSWD__", "O(83r726TKBsP5f#U" );//
//    define( "__DBC_PORT__", "3306" );  
//    define( "__DBC_SCHEMATA__", "transporteca");
 
    define ( "__APP_PATH_ROOT__", "/var/www/html" );
    define('__USE_SECURE__', false);

    define("__DAY_CONVERSION_UTC__",86400);
    define("__DISPLAY_CALCULATION_DETAILS__",false);
    define("__MAIN_SITE_HOME_PAGE_URL__",'https://api.transporteca.com');
    define("__MAIN_SITE_HOME_PAGE_URL_WITHOUT_HTTP__",'api.transporteca.com');
    define("__MAIN_SITE_HOME_PAGE_SECURE_URL__",'https://api.transporteca.com');
    define('__PAYPAL_PAYMENT_URL_SANDBOX__','https://www.sandbox.paypal.com/cgi-bin/webscr');
    define('__PAYPAL_PAYMENT_URL_LIVE__','https://www.paypal.com/cgi-bin/webscr');
    define('__BUSINESS_PAYPAL_ACCOUNT__','accounts@transporteca.com');
    define("__PAYPAL_LIVE__",true);

    define('__PAYPAL_PAYMENT_URL_SANDBOX_TOKEN__','cDizjVSOdRYfxNY_Kr8xiIrbWpQjO066x5cBOkHmz5St0EOc-dxzuzcEzfW');
    define('__PAYPAL_PAYMENT_URL_TOKEN_LIVE__','ak_0A63qWMcgxwFNLeZ0IZEJGd02jkh8EYNKFAIJ-sfJ2pbOXj-oHga2OZW');

    define('__PAYPAL_PAYMENT_URL_SANDBOX_HOST__','www.sandbox.paypal.com');
    define('__PAYPAL_PAYMENT_URL_LIVE_HOST__','www.sandbox.paypal.com');
    define('__LIVE_SSL_FLAG__',true);
	 
    //zooz api details
    define("__API_KEY__","4c60588d-8226-4d84-9391-aea31bcc7d87");
    define("__API_UNIQUE_KEY__","transporteca.com");
    define("__URL_FLAG__",false);
    define("__API_DEVELOPER_ID__","morten.laerkholm@transporteca.com");
    define("__API_DEVELOPER_KEY__","HLRN74WUJNB6KK7HT73CZYFDGE");

    define("__BASE_STORE_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/images");

    define ("__BASE_MANAGEMENT_URL__", "http://management.transporteca.com");
    define ("__BASE_SECURE_MANAGEMENT_URL__", 'https://management.transporteca.com');
    
    define ("__BASE_METRICS_URL__", "http://metrics.transporteca.com");
    define ("__BASE_SECURE_METRICS_URL__", 'http://metrics.transporteca.com');
    
    define( "__CAPSULE_API_USER_NAME__", "92b17614ea700d332b299bc54df5d2ff" );
    define( "__CAPSULE_API_PASSWORD__", "x" );
    define( "__CAPSULE_API_HOST_URL__", "https://transporteca.capsulecrm.com" ); 
     
    define( "__FEDEX_DEVELOPER_KEY__", "9BMKKAQH9XvTjCyg" );
    define( "__FEDEX_DEVELOPER_PASSWORD__", "dd60IwdrvQn1AoduIhmCE34Sp");
    define( "__FEDEX_ACCOUNT_NUMBER__", "342409865" );
    define( "__FEDEX_METER_NUMBER__", "106406688");

    define( "__UPS_ACCESS_KEY__", "2CE5BE3A18751274" );
    define( "__UPS_API_USER_ID__", "hXuU27wq" );
    define( "__UPS_API_PASSWORD__", "7yp6Xaax" ); 
    define("__ID_ANONYMUS_USER__",1);
    
    define("__EASYPOST_API_KEY__", "9RT5CVDLLBAVV7nwD1pY6w");   
    define("__STRIPE_API_KEY__", "pk_live_utBqxxOO4Ywz9idHaONOmqX8"); 
    define("__STRIPE_API_SECRET_KEY__", "sk_live_wU2bku5ALNFLk6H3wWshkyAC");
    define("__AFTER_SHIP_API_KEY__","9bb6d415-9aeb-4ba8-bdb1-6636a6494004");
    define( "__MAUTIC_API_BASE_URL__", "https://transporteca.mautic.net");
    
    
    define( "__FB_API_ID_KEY__", "460759854132995");  
    define("__FB_API_SECRET_KEY__","8299a0028a3d1940c8ce0d172e014f82");
    
    define("__GOOGLE_PLUS_CLIENT_ID__","235636680399-ii3k2r1ss3t8ctr5mdvoo243jn6aluna.apps.googleusercontent.com");
    define("__GOOGLE_PLUS_CLIENT_SECRET__","TmSyt2dMnakr9skBf1U_oXsK");
    
    define("__GOOGLE_PLUS_REDIRECT_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__.'/googleLogin');
    
    define("__WORDPRESS_FOLDER__","content");
    define("__STANDARD_AGREEMENT_ID__","14");
    
    define("__GMAIL_HOST_NAME__","imap.gmail.com");
    define("__GMAIL_HOST_PORT__","993"); 
    define("__GMAIL_USER_NAME__","whizsolutions16@gmail.com");
    define("__GMAIL_PASSWORD__","Test1234!*");
    
    /*
    * Zopim API credentials
    */
    define("__ZOPIM_CHAT_BASE_URL__","https://www.zopim.com/api/v2/chats");
    define("__ZOPIM_CHAT_USER_NAME__","thorsten.boeck@transporteca.com");
    define("__ZOPIM_CHAT_PASSWORD__","a5P8t6102j"); 
    
    define("__OFFLINE_REMIND_TEMPLATE_ID__","41"); 
    define("__SHOW_VAT_WITHOUT_MARKUP__","2016-08-01"); 
    
    define("__POSTMEN_API_KEY__","22a35914-3345-463b-9cdc-5daf60409b98");
    define("__POSTMEN_API_ENDPOINT__","https://production-api.postmen.com/v3/rates");
}
else if(__ENVIRONMENT__ == "DEV_LIVE")
{
    define( "__DBC_HOST__", "localhost" );
    define( "__DBC_USER__", "root" );
    define( "__DBC_PASSWD__", "O(83r726TKBsP5f#U" );//
    define( "__DBC_PORT__", "3306" ); 
    
    define( "__DBC_SCHEMATA__", "transporteca_dev");

    //define ('__BASE_URL__', "http://5.79.32.96");
    //define ('__BASE_URL_SECURE__', "https://5.79.32.96");
    define ( "__APP_PATH_ROOT__", "/var/www/html_dev" );
    //define ("__APP_PATH_ROOT__",__APP_PATH__);
    define('__USE_SECURE__', false);

    define("__DAY_CONVERSION_UTC__",86400);
    define("__DISPLAY_CALCULATION_DETAILS__",true);
	
    define("__MAIN_SITE_HOME_PAGE_URL__",'http://dev.transporteca.com');
    define("__MAIN_SITE_HOME_PAGE_URL_WITHOUT_HTTP__",'dev.transporteca.com');
    define("__MAIN_SITE_HOME_PAGE_SECURE_URL__",'http://dev.transporteca.com');
    define('__PAYPAL_PAYMENT_URL_SANDBOX__','https://www.sandbox.paypal.com/cgi-bin/webscr');
    define('__BUSINESS_PAYPAL_ACCOUNT__','ashish@whiz-solutions.com');
    //define('__BUSINESS_PAYPAL_ACCOUNT__','accounts@transporteca.com');
    define('__PAYPAL_PAYMENT_URL_LIVE__','https://www.paypal.com/cgi-bin/webscr');
    define("__PAYPAL_LIVE__",false);

    define('__PAYPAL_PAYMENT_URL_SANDBOX_TOKEN__','cDizjVSOdRYfxNY_Kr8xiIrbWpQjO066x5cBOkHmz5St0EOc-dxzuzcEzfW');
    define('__PAYPAL_PAYMENT_URL_TOKEN_LIVE__','ak_0A63qWMcgxwFNLeZ0IZEJGd02jkh8EYNKFAIJ-sfJ2pbOXj-oHga2OZW');

    define('__PAYPAL_PAYMENT_URL_SANDBOX_HOST__','www.sandbox.paypal.com');
    define('__PAYPAL_PAYMENT_URL_LIVE_HOST__','www.sandbox.paypal.com');
    define('__LIVE_SSL_FLAG__',false);

    //zooz api details
    define("__API_KEY__","9365cd90-8092-410e-9287-b19e5cbdd87e");
    define("__API_UNIQUE_KEY__","com.application.transport");
    define("__URL_FLAG__",true);
    define("__API_DEVELOPER_ID__","ashish@whiz-solutions.com");
    define("__API_DEVELOPER_KEY__","IQRCETQJ7PISG56CUOCNYYY3LQ");
    
    //zooz api details
//    define("__API_KEY__","4c60588d-8226-4d84-9391-aea31bcc7d87");
//    define("__API_UNIQUE_KEY__","transporteca.com");
//    define("__URL_FLAG__",false);
//    define("__API_DEVELOPER_ID__","morten.laerkholm@transporteca.com");
//    define("__API_DEVELOPER_KEY__","HLRN74WUJNB6KK7HT73CZYFDGE");

    define("__BASE_STORE_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_URL__."/images");

    define ("__BASE_METRICS_URL__", "http://devmetrics.transporteca.com");
    define ("__BASE_SECURE_METRICS_URL__", 'http://devmetrics.transporteca.com');
    
    define ("__BASE_MANAGEMENT_URL__", "http://devmanagement.transporteca.com");
    define ("__BASE_SECURE_MANAGEMENT_URL__", 'https://devmanagement.transporteca.com');
    define("__BASE_STORE_IMAGE_URL__","/images");
    
    define( "__CAPSULE_API_USER_NAME__", "2daaf173aef29a250e548435df2b8358" );
    define( "__CAPSULE_API_PASSWORD__", "x" );
    define( "__CAPSULE_API_HOST_URL__", "https://devtransporteca.capsulecrm.com" ); 
     
    define( "__FEDEX_DEVELOPER_KEY__", "y79QiAm0xV3BOnS1" );
    define( "__FEDEX_DEVELOPER_PASSWORD__", "ve4ndAGpp7pQYdjnBKAGt05Ey");
    define( "__FEDEX_ACCOUNT_NUMBER__", "471156248" );
    define( "__FEDEX_METER_NUMBER__", "107472075");

    define( "__UPS_ACCESS_KEY__", "2CE5BE3A18751274" );
    define( "__UPS_API_USER_ID__", "hXuU27wq" );
    define( "__UPS_API_PASSWORD__", "7yp6Xaax" ); 
    
    define("__ID_ANONYMUS_USER__",1);
    define( "__EASYPOST_API_KEY__", "9RT5CVDLLBAVV7nwD1pY6w" );
    define( "__STRIPE_API_KEY__", "pk_test_Haw0VsywabhVPV8i6ivOJ7D5"); 
    define( "__STRIPE_API_SECRET_KEY__", "sk_test_56OWhfOUYSUdpq45VeKNN2Rl");
    define("__AFTER_SHIP_API_KEY__","a02bd542-9c43-4ef2-a000-2b847dbb1cd8");
    
    define( "__FB_API_ID_KEY__", "1719611308283905");  
    define("__FB_API_SECRET_KEY__","52e4f781756d03ce391ae9a7e6eb477d");
    
    define("__GOOGLE_PLUS_CLIENT_ID__","839612770288-v4jq42lsjqqq3h77bssav744jm7k9h0c.apps.googleusercontent.com");
    define("__GOOGLE_PLUS_CLIENT_SECRET__","a2JKqmlt2H2pLhJqScPHMvkV");
    
    define("__GOOGLE_PLUS_REDIRECT_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__.'/googleLogin/');
    define( "__MAUTIC_API_BASE_URL__", "https://transporteca.mautic.net");
    
    define("__WORDPRESS_FOLDER__","wordpress");
    define("__STANDARD_AGREEMENT_ID__","99999");
    
    define("__GMAIL_HOST_NAME__","imap.gmail.com");
    define("__GMAIL_HOST_PORT__","993"); 
    define("__GMAIL_USER_NAME__","whizsolutions16@gmail.com");
    define("__GMAIL_PASSWORD__","Test1234!*");
    
    /*
    * Zopim API credentials
    */
    define("__ZOPIM_CHAT_BASE_URL__","https://www.zopim.com/api/v2/chats");
    define("__ZOPIM_CHAT_USER_NAME__","anandj@whiz-solutions.com");
    define("__ZOPIM_CHAT_PASSWORD__","Test1234");
    
    define("__OFFLINE_REMIND_TEMPLATE_ID__","23"); 
    define("__SHOW_VAT_WITHOUT_MARKUP__","2016-08-29"); 
    
    define("__POSTMEN_API_KEY__","22a35914-3345-463b-9cdc-5daf60409b98");
    define("__POSTMEN_API_ENDPOINT__","https://production-api.postmen.com/v3/rates");
}
else if(__ENVIRONMENT__ == "DEV_AWS")
{
    define( "__DBC_HOST__", "development.cluster-cjqjvr5itgtx.eu-west-1.rds.amazonaws.com" );
    define( "__DBC_USER__", "awsuser" );
    define( "__DBC_PASSWD__", "3bZFBwyBN3EhNDHE4MVrnUc5RHQeTjhP" );//
    define( "__DBC_PORT__", false ); 
    
    define( "__DBC_SCHEMATA__", "transporteca_aws");

    //define ('__BASE_URL__', "http://5.79.32.96");
    //define ('__BASE_URL_SECURE__', "https://5.79.32.96");
    define ( "__APP_PATH_ROOT__", "/var/www/html" );
    //define ("__APP_PATH_ROOT__",__APP_PATH__);
    define('__USE_SECURE__', false);

    define("__DAY_CONVERSION_UTC__",86400);
    define("__DISPLAY_CALCULATION_DETAILS__",true);
	
    define("__MAIN_SITE_HOME_PAGE_URL__",'http://dev.transporteca.com');
    define("__MAIN_SITE_HOME_PAGE_URL_WITHOUT_HTTP__",'dev.transporteca.com');
    define("__MAIN_SITE_HOME_PAGE_SECURE_URL__",'http://dev.transporteca.com');
    define('__PAYPAL_PAYMENT_URL_SANDBOX__','https://www.sandbox.paypal.com/cgi-bin/webscr');
    define('__BUSINESS_PAYPAL_ACCOUNT__','ashish@whiz-solutions.com');
    //define('__BUSINESS_PAYPAL_ACCOUNT__','accounts@transporteca.com');
    define('__PAYPAL_PAYMENT_URL_LIVE__','https://www.paypal.com/cgi-bin/webscr');
    define("__PAYPAL_LIVE__",false);

    define('__PAYPAL_PAYMENT_URL_SANDBOX_TOKEN__','cDizjVSOdRYfxNY_Kr8xiIrbWpQjO066x5cBOkHmz5St0EOc-dxzuzcEzfW');
    define('__PAYPAL_PAYMENT_URL_TOKEN_LIVE__','ak_0A63qWMcgxwFNLeZ0IZEJGd02jkh8EYNKFAIJ-sfJ2pbOXj-oHga2OZW');

    define('__PAYPAL_PAYMENT_URL_SANDBOX_HOST__','www.sandbox.paypal.com');
    define('__PAYPAL_PAYMENT_URL_LIVE_HOST__','www.sandbox.paypal.com');
    define('__LIVE_SSL_FLAG__',false);

    //zooz api details
    define("__API_KEY__","9365cd90-8092-410e-9287-b19e5cbdd87e");
    define("__API_UNIQUE_KEY__","com.application.transport");
    define("__URL_FLAG__",true);
    define("__API_DEVELOPER_ID__","ashish@whiz-solutions.com");
    define("__API_DEVELOPER_KEY__","IQRCETQJ7PISG56CUOCNYYY3LQ");
    
    //zooz api details
//    define("__API_KEY__","4c60588d-8226-4d84-9391-aea31bcc7d87");
//    define("__API_UNIQUE_KEY__","transporteca.com");
//    define("__URL_FLAG__",false);
//    define("__API_DEVELOPER_ID__","morten.laerkholm@transporteca.com");
//    define("__API_DEVELOPER_KEY__","HLRN74WUJNB6KK7HT73CZYFDGE");

    define("__BASE_STORE_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_URL__."/images");

    define ("__BASE_METRICS_URL__", "http://devmetrics.transporteca.com");
    define ("__BASE_SECURE_METRICS_URL__", 'http://devmetrics.transporteca.com');
    
    define ("__BASE_MANAGEMENT_URL__", "http://devmanagement.transporteca.com");
    define ("__BASE_SECURE_MANAGEMENT_URL__", 'http://devmanagement.transporteca.com');
    //define("__BASE_STORE_IMAGE_URL__","/images");
    
    define( "__CAPSULE_API_USER_NAME__", "2daaf173aef29a250e548435df2b8358" );
    define( "__CAPSULE_API_PASSWORD__", "x" );
    define( "__CAPSULE_API_HOST_URL__", "https://devtransporteca.capsulecrm.com" ); 
     
    define( "__FEDEX_DEVELOPER_KEY__", "y79QiAm0xV3BOnS1" );
    define( "__FEDEX_DEVELOPER_PASSWORD__", "ve4ndAGpp7pQYdjnBKAGt05Ey");
    define( "__FEDEX_ACCOUNT_NUMBER__", "471156248" );
    define( "__FEDEX_METER_NUMBER__", "107472075");

    define( "__UPS_ACCESS_KEY__", "2CE5BE3A18751274" );
    define( "__UPS_API_USER_ID__", "hXuU27wq" );
    define( "__UPS_API_PASSWORD__", "7yp6Xaax" ); 
    
    define("__ID_ANONYMUS_USER__",1);
    define( "__EASYPOST_API_KEY__", "9RT5CVDLLBAVV7nwD1pY6w" );
    define( "__STRIPE_API_KEY__", "pk_test_Haw0VsywabhVPV8i6ivOJ7D5"); 
    define( "__STRIPE_API_SECRET_KEY__", "sk_test_56OWhfOUYSUdpq45VeKNN2Rl");
    define("__AFTER_SHIP_API_KEY__","a02bd542-9c43-4ef2-a000-2b847dbb1cd8");
    
    define( "__FB_API_ID_KEY__", "1719611308283905");  
    define("__FB_API_SECRET_KEY__","52e4f781756d03ce391ae9a7e6eb477d");
    
    define("__GOOGLE_PLUS_CLIENT_ID__","839612770288-v4jq42lsjqqq3h77bssav744jm7k9h0c.apps.googleusercontent.com");
    define("__GOOGLE_PLUS_CLIENT_SECRET__","a2JKqmlt2H2pLhJqScPHMvkV");
    
    define("__GOOGLE_PLUS_REDIRECT_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__.'/googleLogin/');
    define( "__MAUTIC_API_BASE_URL__", "https://transporteca.mautic.net");
    
    define("__WORDPRESS_FOLDER__","content");
    define("__STANDARD_AGREEMENT_ID__","99999");
    
    define("__GMAIL_HOST_NAME__","imap.gmail.com");
    define("__GMAIL_HOST_PORT__","993"); 
    define("__GMAIL_USER_NAME__","whizsolutions16@gmail.com");
    define("__GMAIL_PASSWORD__","Test1234!*");
    
    /*
    * Zopim API credentials
    */
    define("__ZOPIM_CHAT_BASE_URL__","https://www.zopim.com/api/v2/chats");
    define("__ZOPIM_CHAT_USER_NAME__","anandj@whiz-solutions.com");
    define("__ZOPIM_CHAT_PASSWORD__","Test1234");
    
    define("__OFFLINE_REMIND_TEMPLATE_ID__","23"); 
    define("__SHOW_VAT_WITHOUT_MARKUP__","2016-08-29"); 
    
    define("__POSTMEN_API_KEY__","22a35914-3345-463b-9cdc-5daf60409b98");
    define("__POSTMEN_API_ENDPOINT__","https://production-api.postmen.com/v3/rates");
} 
else if(__ENVIRONMENT__ == "AWS_STAGGING")
{ 
    define( "__DBC_HOST__", "apiserver-cluster.cluster-cjqjvr5itgtx.eu-west-1.rds.amazonaws.com" ); //api.cluster-cjqjvr5itgtx.eu-west-1.rds.amazonaws.com
    define( "__DBC_USER__", "awsuser" );
    define( "__DBC_PASSWD__", "RI8SpShLS1j59O4vdPUELHthpa4rE70L" );//
    define( "__DBC_PORT__", false );  
    define( "__DBC_SCHEMATA__", "transporteca");

    //define ('__BASE_URL__', "http://5.79.32.96");
    //define ('__BASE_URL_SECURE__', "https://5.79.32.96");
    define ( "__APP_PATH_ROOT__", "/var/www/html" );
    //define ("__APP_PATH_ROOT__",__APP_PATH__);
    define('__USE_SECURE__', false);

    define("__DAY_CONVERSION_UTC__",86400);
    define("__DISPLAY_CALCULATION_DETAILS__",true);
	
    define("__MAIN_SITE_HOME_PAGE_URL__",'https://api.transporteca.com');
    define("__MAIN_SITE_HOME_PAGE_URL_WITHOUT_HTTP__",'api.transporteca.com');
    define("__MAIN_SITE_HOME_PAGE_SECURE_URL__",'https://api.transporteca.com');
    define('__PAYPAL_PAYMENT_URL_SANDBOX__','https://www.sandbox.paypal.com/cgi-bin/webscr');
    define('__BUSINESS_PAYPAL_ACCOUNT__','ashish@whiz-solutions.com');
    //define('__BUSINESS_PAYPAL_ACCOUNT__','accounts@transporteca.com');
    define('__PAYPAL_PAYMENT_URL_LIVE__','https://www.paypal.com/cgi-bin/webscr');
    define("__PAYPAL_LIVE__",false);

    define('__PAYPAL_PAYMENT_URL_SANDBOX_TOKEN__','cDizjVSOdRYfxNY_Kr8xiIrbWpQjO066x5cBOkHmz5St0EOc-dxzuzcEzfW');
    define('__PAYPAL_PAYMENT_URL_TOKEN_LIVE__','ak_0A63qWMcgxwFNLeZ0IZEJGd02jkh8EYNKFAIJ-sfJ2pbOXj-oHga2OZW');

    define('__PAYPAL_PAYMENT_URL_SANDBOX_HOST__','www.sandbox.paypal.com');
    define('__PAYPAL_PAYMENT_URL_LIVE_HOST__','www.sandbox.paypal.com');
    define('__LIVE_SSL_FLAG__',false);

    //zooz api details
    define("__API_KEY__","9365cd90-8092-410e-9287-b19e5cbdd87e");
    define("__API_UNIQUE_KEY__","com.application.transport");
    define("__URL_FLAG__",true);
    define("__API_DEVELOPER_ID__","ashish@whiz-solutions.com");
    define("__API_DEVELOPER_KEY__","IQRCETQJ7PISG56CUOCNYYY3LQ");
    
    //zooz api details
//    define("__API_KEY__","4c60588d-8226-4d84-9391-aea31bcc7d87");
//    define("__API_UNIQUE_KEY__","transporteca.com");
//    define("__URL_FLAG__",false);
//    define("__API_DEVELOPER_ID__","morten.laerkholm@transporteca.com");
//    define("__API_DEVELOPER_KEY__","HLRN74WUJNB6KK7HT73CZYFDGE");

    define("__BASE_STORE_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_URL__."/images");

    define ("__BASE_METRICS_URL__", "http://metrics.transporteca.com");
    define ("__BASE_SECURE_METRICS_URL__", 'http://metrics.transporteca.com');
    
    define ("__BASE_MANAGEMENT_URL__", "http://management.transporteca.com");
    define ("__BASE_SECURE_MANAGEMENT_URL__", 'http://management.transporteca.com');
    //define("__BASE_STORE_IMAGE_URL__","/images");
    
    define( "__CAPSULE_API_USER_NAME__", "2daaf173aef29a250e548435df2b8358" );
    define( "__CAPSULE_API_PASSWORD__", "x" );
    define( "__CAPSULE_API_HOST_URL__", "https://devtransporteca.capsulecrm.com" ); 
     
    define( "__FEDEX_DEVELOPER_KEY__", "y79QiAm0xV3BOnS1" );
    define( "__FEDEX_DEVELOPER_PASSWORD__", "ve4ndAGpp7pQYdjnBKAGt05Ey");
    define( "__FEDEX_ACCOUNT_NUMBER__", "471156248" );
    define( "__FEDEX_METER_NUMBER__", "107472075");

    define( "__UPS_ACCESS_KEY__", "2CE5BE3A18751274" );
    define( "__UPS_API_USER_ID__", "hXuU27wq" );
    define( "__UPS_API_PASSWORD__", "7yp6Xaax" ); 
    
    define("__ID_ANONYMUS_USER__",1);
    define( "__EASYPOST_API_KEY__", "9RT5CVDLLBAVV7nwD1pY6w" );
    define( "__STRIPE_API_KEY__", "pk_test_Haw0VsywabhVPV8i6ivOJ7D5"); 
    define( "__STRIPE_API_SECRET_KEY__", "sk_test_56OWhfOUYSUdpq45VeKNN2Rl");
    define("__AFTER_SHIP_API_KEY__","a02bd542-9c43-4ef2-a000-2b847dbb1cd8");
    
    define( "__FB_API_ID_KEY__", "1719611308283905");  
    define("__FB_API_SECRET_KEY__","52e4f781756d03ce391ae9a7e6eb477d");
    
    define("__GOOGLE_PLUS_CLIENT_ID__","839612770288-v4jq42lsjqqq3h77bssav744jm7k9h0c.apps.googleusercontent.com");
    define("__GOOGLE_PLUS_CLIENT_SECRET__","a2JKqmlt2H2pLhJqScPHMvkV");
    
    define("__GOOGLE_PLUS_REDIRECT_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__.'/googleLogin/');
    define( "__MAUTIC_API_BASE_URL__", "https://transporteca.mautic.net");
    
    define("__WORDPRESS_FOLDER__","content");
    define("__STANDARD_AGREEMENT_ID__","99999");
    
    define("__GMAIL_HOST_NAME__","imap.gmail.com");
    define("__GMAIL_HOST_PORT__","993"); 
    define("__GMAIL_USER_NAME__","whizsolutions16@gmail.com");
    define("__GMAIL_PASSWORD__","Test1234!*");
    
    /*
    * Zopim API credentials
    */
    define("__ZOPIM_CHAT_BASE_URL__","https://www.zopim.com/api/v2/chats");
    define("__ZOPIM_CHAT_USER_NAME__","anandj@whiz-solutions.com");
    define("__ZOPIM_CHAT_PASSWORD__","Test1234");
    
    define("__OFFLINE_REMIND_TEMPLATE_ID__","23"); 
    define("__SHOW_VAT_WITHOUT_MARKUP__","2016-08-29"); 
    
    define("__POSTMEN_API_KEY__","22a35914-3345-463b-9cdc-5daf60409b98");
    define("__POSTMEN_API_ENDPOINT__","https://production-api.postmen.com/v3/rates");
}
define("__FORWARDER_CHAT_TAG_KEYWORD__","FORWARDER"); 
define('__AFTER_SHIP_TRACKING_URL__', "http://track.transporteca.com");
define("__GOOGLE_MAP_V3_API_KEY__",'AIzaSyB5qrAEl1OYv3rGr9rKA5AHewc__M33nYY'); 
define('__BASE_STORE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
define('__BASE_STORE_URL_SECURE__', __MAIN_SITE_HOME_PAGE_SECURE_URL__);

define('__PARAMS__', "1234567890");
define('__CC_AUTH_ONLY_AMOUNT__', '5.00');
define("__CMS_ADMIN_URL__", __MAIN_SITE_HOME_PAGE_SECURE_URL__."/cms-admin");

define('__TRANSPORTECA_DANISH_URL__', 'da');
define('__TRANSPORTECA_ENGLISH_URL__', 'en');
define('__TRANSPORTECA_SWEDISH_URL__', 'se');

/**
 * store Path Constant Definitions.
 *
 */
define( "__APP_PATH_INC__", __APP_PATH__ . "/inc" );
define( "__APP_PATH_CLASSES__", __APP_PATH_INC__ . "/classes" );
define( "__APP_PATH_IMAGES__", __APP_PATH__ . "/images" );
define( "__APP_PATH_JS__", __APP_PATH__ . "/js" );
define( "__APP_PATH_CSS__", __APP_PATH__ . "/css" );
define( "__APP_PATH_AJAX__", __APP_PATH__ . "/AJAX" );
define( "__APP_PATH_LAYOUT__", __APP_PATH__ . "/layout" );
define( "__APP_PATH_FORWARDER_IMAGES__", __APP_PATH_IMAGES__ . '/forwarders' );
define( "__APP_PATH_FORWARDER_IMAGES_TEMP__", __APP_PATH_FORWARDER_IMAGES__ .'/temp');
define( "__APP_PATH_FORWARDER_IMAGES__POP_UP_TEMP__", __APP_PATH_ROOT__ .'/images/forwarders/temp/');
define( "__APP_PATH_GMAIL_ATTACHMENT__", __APP_PATH__ . "/gmail-attachments" ); 
define('__BASE_URL_GMAIL_ATTACHMENT__', __MAIN_SITE_HOME_PAGE_URL__."/gmail-attachments");

define( "__APP_PATH_TNT__", __APP_PATH__ . "/TNT" );
define( "__APP_PATH_TNT_LABEL__", __APP_PATH_TNT__ . "/labels" );
define( "__APP_PATH_TNT_XMLS__", __APP_PATH_TNT__ . "/xmls" );
define( "__APP_PATH_TNT_XSL__", __APP_PATH_TNT__ . "/xsl" );
define( "__APP_PATH_TNT_HTML__", __APP_PATH_TNT__ . "/html" );
define( "__APP_PATH_TNT_CSS__", __APP_PATH_TNT__ . "/css" ); 
define( "__APP_PATH_TNT_IMAGES__", __APP_PATH_TNT__ . "/images" );
define( "__APP_PATH_TNT_BARCODE_GENERATOR__", __APP_PATH_TNT__ . "/barcode_generator" );

define( "__APP_PATH_TNT_BARCODE_IMAGES__", __APP_PATH_TNT_IMAGES__ . "/barcode" );
define( "__APP_PATH_TNT_BARCODE_GIF_IMAGES__", __APP_PATH_TNT_BARCODE_IMAGES__ . "/gifs" );
define( "__APP_PATH_TNT_BARCODE_PNG_IMAGES__", __APP_PATH_TNT_BARCODE_IMAGES__ . "/pngs" );
define( "__APP_PATH_TNT_BARCODE_DOWNLOAD_IMAGES__", __APP_PATH_TNT_BARCODE_IMAGES__ . "/downloaded" );


define( "__APP_PATH_EMAIL_ATTACHMENT__", __APP_PATH__ . "/attachments" ); 
define('__BASE_URL_EMAIL_ATTACHMENT__', __MAIN_SITE_HOME_PAGE_URL__."/attachments");

define('__BASE_URL_TNT__', __MAIN_SITE_HOME_PAGE_URL__."/TNT");
define('__BASE_URL_TNT_HTML__', __BASE_URL_TNT__."/html");
define('__BASE_URL_TNT_IMAGES__', __BASE_URL_TNT__."/images");
define('__BASE_URL_TNT_BARCODE_IMAGES__', __BASE_URL_TNT_IMAGES__."/barcode");
define('__BASE_URL_TNT_BARCODE_PNG_IMAGES__', __BASE_URL_TNT_BARCODE_IMAGES__."/pngs");
define('__BASE_URL_TNT_BARCODE_DOWNLOADED_IMAGES__', __BASE_URL_TNT_BARCODE_IMAGES__."/downloaded");

define('__BASE_URL_TNT_CSS__', __BASE_URL_TNT__."/css");

define('__BASE_URL_TNT_BARCODE_GENERATOR__', __BASE_URL_TNT__."/barcode_generator");
 
define ('__AJAX_URL__', __MAIN_SITE_HOME_PAGE_URL__."/AJAX");
define ('__AJAX_URL_SECURE__', __MAIN_SITE_HOME_PAGE_SECURE_URL__."/AJAX");
define("__BASE_STORE_MANAGEMENT_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/management");

define("__BASE_STORE_CSS_URL__",__MAIN_SITE_HOME_PAGE_URL__."/css");
define("__BASE_STORE_JS_URL__",__MAIN_SITE_HOME_PAGE_URL__."/js");
define("__BASE_TMP_PATH__", __APP_PATH__."/tmp");
define("__BASE_STORE_ANGULAR_JS_URL__",__MAIN_SITE_HOME_PAGE_URL__."/angular");

define("__BASE_STORE_SECURE_JS_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/js");
define("__BASE_STORE_SECURE_CSS_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/css");
define("__BASE_STORE_INC_URL__",__MAIN_SITE_HOME_PAGE_URL__);
define("__BASE_STORE_FORWARDER_TEMP_IMAGE_URL__",__BASE_STORE_IMAGE_URL__."/forwarders/temp");

define("__BASE_STORE_FORWARDER_CSS_URL__",__BASE_STORE_CSS_URL__."/forwarders");
define("__BASE_STORE_TRANPORTECA_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_URL_WITHOUT_HTTP__."/images");
/**
 * admin path Constants
 */
define( "__BASE_STORE_CSS_MANAGEMENT_URL__", __MAIN_SITE_HOME_PAGE_URL__."/css/management" );
//define( "__BASE_STORE_CSS_MANAGEMENT_URL__", __MAIN_SITE_HOME_PAGE_SECURE_URL__."/css/management" );
define( "__BASE_STORE_IMAGE_MANAGEMENT_URL__", __MAIN_SITE_HOME_PAGE_SECURE_URL__. "/images/management" );
//define( "__BASE_STORE_MANAGEMENT_INDEXPAGE__", __BASE_STORE_ADMIN_URL__.'/index_default.php' );
//define( "__BASE_STORE_MANAGEMENT_LOGINPAGE__", __BASE_STORE_ADMIN_URL__.'/login.php' );
define( "__DEFAULT_MANAGEMENT_PAGE_TITLE__", "Transporteca Admin");

define("__BASE_STORE_FORWARDER_IMAGE_URL__",__BASE_STORE_IMAGE_URL__."/forwarders");

if(defined('__APP_PATH_IMAGES_PRODUCTS__')) {
   define( "__APP_PATH_IMAGES_PRODUCTS_BANNERS__", __APP_PATH_IMAGES_PRODUCTS__ . "/banner" );
}

/**
 * Log file path Constants
 */
define( "__APP_PATH_LOGS__", __APP_PATH__ . "/logs" );
define( "__APP_PATH_LOG_CLASSES__", __APP_PATH_LOGS__ . "/classes" );

/**
 * Client IP Constant Definitions.
 *
 */

if(isset($_SERVER['REMOTE_ADDR'])) {
    define( "__REMOTE_ADDR__", $_SERVER['REMOTE_ADDR'] );
}

if(isset($_SERVER['HTTP_REFERER'])) {
    define( "__HTTP_REFERER__", $_SERVER['HTTP_REFERER'] );
}

if(isset($_SERVER['HTTP_USER_AGENT'])) {
    define( "__HTTP_USER_AGENT__",  $_SERVER['HTTP_USER_AGENT'] );
}


define( "__VLD_ERROR_DISPLAY__", "<div class=\"ErrorContent\">{ERROR}</div><div class=\"formErrorArrow\"><div class=\"line10\"></div><div class=\"line9\"></div><div class=\"line8\"></div><div class=\"line7\"></div><div class=\"line6\"></div><div class=\"line5\"></div><div class=\"line4\"></div><div class=\"line3\"></div><div class=\"line2\"></div><div class=\"line1\"></div></div>" );

/**
 * Validate an int, uses is_numeric
 *
 * @author Anil
 */
	define( "__VLD_CASE_NUMERIC__", "NUMERIC" );
/**
 * Validate an credit card, uses is_numeric
 *
 * @author Anil
 */
	define( "__VLD_CASE_CARD__", "CARD" );
/**
 * Validate numbers & letters, REGEX: /^[a-z0-9]+$/i
 *
 * @author Anil
 */
	define( "__VLD_CASE_ALPHANUMERIC__", "ALPHANUMERIC" );

/**
 * Validate URI, REGEX: /^[a-z0-9\-\_\.]+$/i
 *
 * @author Anil
 */
	define( "__VLD_CASE_URI__", "URI" );
/**
 * Validate alpha letters, REGEX: /^[a-z]+$/i
 *
 * @author Anil
 */
	define( "__VLD_CASE_ALPHA__", "ALPHA" );
/**
 * Validate anything, allows ANYTHING!
 *
 * @author Anil
 */
	define( "__VLD_CASE_ANYTHING__", "ANYTHING" );
/**
 * Validate email, REGEX: /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z0-9]{2,3})$/i
 *
 * @author Anil
 */
	define( "__VLD_CASE_EMAIL__", "EMAIL" );
/**
 * Validate boolean, uses is_bool || === 0 || === "0"
 *
 * @author Anil
 */
	define( "__VLD_CASE_BOOL__", "BOOL" );
/**
 * Validate boolean, uses is_bool
 *
 * @author Anil
 */
	define( "__VLD_CASE_STRICTBOOL__", "STRICTBOOL" );
/**
 * Validate address, REGEX: /^[a-z0-9,.#-_\s]+$/i
 *
 * @author Anil
 */
	define( "__VLD_CASE_ADDRESS__", "ADDRESS" );
/**
 * Validate name, REGEX: /^[a-z0-9,.#-_\s]+$/i
 *
 * @author Anil
 */
	define( "__VLD_CASE_NAME__", "NAME" );
/**
 * Validate url, REGEX: _^(?:([^:/?#]+):)?(?://([^/?#]*))?([^?#]*)(?:\?([^#]*))?(?:#(.*))?$_
 *
 * @author Anil
 */
	define( "__VLD_CASE_URL__", "URL" );
/**
 * Validate username, REGEX: /^[\S]+$/i
 *
 * @author Anil
 */
	define( "__VLD_CASE_USERNAME__", "USERNAME" );
/**
 * Validate password, REGEX: /^[\S]+$/i
 *
 * @author Anil
 */
	define( "__VLD_CASE_PASSWORD__", "PASSWORD" );
/**
 * Validate date, uses strtotime
 *
 * @author Anil
 */
	define( "__VLD_CASE_DATE__", "DATE" );
/**
 * Validate phone, REGEX: /^\d{3}-\d{3}-\d{4}$/
 *
 * @author Anil
 */
	define( "__VLD_CASE_PHONE__", "PHONE" );
	define( "__VLD_CASE_PHONE_2__", "PHONE_2" );
/**
 * Validate US money, REGEX: /^[0-9]+(\.[0-9]{2})*$/
 *
 * @author Anil
 */
	define( "__VLD_CASE_MONEY_US__", "MONEY_US" );
/**
 * Validate credit card expiration, REGEX: /^[0-9]{2}[-][0-9]{4}$/
 *
 * @author Anil
 */
	define( "__VLD_CASE_CC_EXP__", "CC_EXP" );
/**
 * Validate Array
 * @author Anil
 */
	define ("__VLD_CASE_ARRAY__", "ARRAY_TYPE");
        
/**
 * Validate Dimension Unit
 * @author Anil
 */
	define ("__VLD_CASE_DIMENSION_UNIT__", "DIMENSION_UNIT");
        
/**
 * Validate Weight Unit
 * @author Anil
 */
	define ("__VLD_CASE_WEIGHT_UNIT__", "WEIGHT_UNIT");

/**
 * Validate cargo measures 
 * @author Anil
 */
	define ("__VALIDATE_CARGO_DIMENSIONS__", "CARGO_MEASURE");


	define( "__VLD_CASE_PASSWORD_NOFORCE_", "PASSWORD_NOFORCE" );

	define('__404_PAGE__', __BASE_STORE_URL__."/404.php");




// database constants
define("__DBC_SCHEMATA_SESSIONS__",__DBC_SCHEMATA__.".tblsessions");
define("__DBC_SCHEMATA_ADMIN_SESSIONS__",__DBC_SCHEMATA__.".tbladminsessions");

define("__DBC_SCHEMATA_CFS_MODEL_MAPPING__",__DBC_SCHEMATA__.".tblhaulagecfsmodelsmapping");
define("__DBC_SCHEMATA_COUNTRY__",__DBC_SCHEMATA__.".tblcountry");
define("__DBC_SCHEMATA_WAREHOUSES__",__DBC_SCHEMATA__.".tblwarehouses");
define("__DBC_SCHEMATA_WAREHOUSES_PRICING__",__DBC_SCHEMATA__.".tblpricingwtw");
define("__DBC_SCHEMATA_SERVICE_TYPE__",__DBC_SCHEMATA__.".tblservicetype");
define("__DBC_SCHEMATA_TIMING_TYPE__",__DBC_SCHEMATA__.".tbltimingtype");
define("__DBC_SCHEMATA_WEIGHT_MEASURE__",__DBC_SCHEMATA__.".tblweightmeasure");
define("__DBC_SCHEMATA_CARGO_MEASURE__",__DBC_SCHEMATA__.".tblcargomeasure");
define("__DBC_SCHEMATA_CUSTOM_CLEARANCE__",__DBC_SCHEMATA__.".tblorigindestination");
define("__DBC_SCHEMATA_CURRENCY__",__DBC_SCHEMATA__.".tblcurrency");
define("__DBC_SCHEMATA_FROWARDER__",__DBC_SCHEMATA__.".tblforwarders");
define("__DBC_SCHEMATA_PRICING_CC__",__DBC_SCHEMATA__.".tblpricingcc");
define("__DBC_SCHEMATA_PRICING_HAULAGE__",__DBC_SCHEMATA__.".tblpricinghaulage");
define("__DBC_SCHEMATA_POSTCODE__",__DBC_SCHEMATA__.".tblpostcode");
define("__DBC_SCHEMATA_FORWARDERS__",__DBC_SCHEMATA__.".tblforwarders");
define("__DBC_SCHEMATA_FORWARDERS_CONTACT__",__DBC_SCHEMATA__.".tblforwardercontacts");
define("__DBC_SCHEMATA_FORWARDERS_CONTACT_ROLE__",__DBC_SCHEMATA__.".tblforwardercontactrole");

define("__DBC_SCHEMATA_USERS__",__DBC_SCHEMATA__.".tblcustomer");
define("__DBC_SCHEMATA_CMS_EMAIL__",__DBC_SCHEMATA__.".cms_sections");
define("__DBC_SCHEMATA_EMAIL_LOG__",__DBC_SCHEMATA__.".tblemaillog");
define("__DBC_SCHEMATA_SERVICE_AVAILABLE__",__DBC_SCHEMATA__.".tblservices");

define("__DBC_SCHEMATA_HAULAGE_COUNTRY__",__DBC_SCHEMATA__.".tblhaulagecountry");


define("__DBC_SCHEMATA_SHIPPER_CONSIGNEES_GROUP__",__DBC_SCHEMATA__.".tblshipcongroup");
define("__DBC_SCHEMATA_REGISTER_SHIPPER_CONSIGNEES__",__DBC_SCHEMATA__.".tblregisteredshipcons");

define("__DBC_SCHEMATA_CURRENCY_EXCHANGE_RATE__",__DBC_SCHEMATA__.".tblexchangerate");
define("__DBC_SCHEMATA_FREQUENCY__",__DBC_SCHEMATA__.".tblfrequency");
define("__DBC_SCHEMATA_EXCHANGE_RATE__",__DBC_SCHEMATA__.".tblexchangerate");
define("__DBC_SCHEMATA_MANAGEMENT_VARIABLE__",__DBC_SCHEMATA__.".tblmanagementvar");
define("__DBC_SCHEMATA_TEMP_SEARCH_RESULT__",__DBC_SCHEMATA__.".tbltempsearchresult");
 
define("__DBC_SCHEMATA_HAULAGE_TRANSIT_TIME__",__DBC_SCHEMATA__.".tblhaulagetransittime");
define("__DBC_SCHEMATA_WEEK_DAYS__",__DBC_SCHEMATA__.".tblweekdays");
define("__DBC_SCHEMATA_BOOKING_REFERENCE_LOGS__",__DBC_SCHEMATA__.".tblbookingreferencelogs");

define("__DBC_SCHEMATA_RATING_REVIEW__",__DBC_SCHEMATA__.".tblratingreview"); 
define("__DBC_SCHEMATA_USER_INVITE_HISTORY__",__DBC_SCHEMATA__.".tbluserinvitehistory");
define("__DBC_SCHEMATA_FORWARDER_SESSIONS__",__DBC_SCHEMATA__.".tblforwardersessions");

define("__DBC_SCHEMATA_FORWARDER_WTW_ARCHIVE__",__DBC_SCHEMATA__.".tblpricingwtwarchive");
define("__DBC_SCHEMATA_FORWARDER_HAULAGE_ARCHIVE__",__DBC_SCHEMATA__.".tblpricinghaulagearchive");
define("__DBC_SCHEMATA_FORWARDER_PRCIING_CC_ARCHIVE__",__DBC_SCHEMATA__.".tblpricingccarchive");
define("__DBC_SCHEMATA_FORWARDER_PREFERENCES__",__DBC_SCHEMATA__.".tblforwarderpreferences");
define("__DBC_SCHEMATA_FORWARDER_NON_ACCEPTANCE__",__DBC_SCHEMATA__.".tblnonacceptance");
define("__DBC_SCHEMATA_HOLDING_CONTACT_EMAIL__",__DBC_SCHEMATA__.".tblholdingcontactemail");

define("__DBC_SCHEMATA_MANAGEMENT__",__DBC_SCHEMATA__.".tblmanagement");
define("__DBC_SCHEMATA_INVESTORS__",__DBC_SCHEMATA__.".tblinvestors");

define("__DBC_SCHEMATA_EXPACTED_SERVICES__",__DBC_SCHEMATA__.".tblexpectedservices");
############################
# MANAGEMENT FORWARDER TBLE#
############################
define("__DBC_SCHEMATA_MANAGEMENT_TERMS__",__DBC_SCHEMATA__.".tbltermsandconditions");
define("__DBC_SCHEMATA_MANAGEMENT_FAQ__",__DBC_SCHEMATA__.".tblfaq");
define("__DBC_SCHEMATA_MANAGEMENT_TERMS_VERSION__",__DBC_SCHEMATA__.".tbltermsandconditionversions");
define("__DBC_SCHEMATA_MANAGEMENT_FAQ_VERSION__",__DBC_SCHEMATA__.".tblfaqversion");
############################
# MANAGEMENT CUSTOMER TBLE #
############################
define("__DBC_SCHEMATA_MANAGEMENT_TERMS_CUSTOMER__",__DBC_SCHEMATA__.".tbltermsandconditioncustomer");
define("__DBC_SCHEMATA_MANAGEMENT_FAQ_CUSTOMER__",__DBC_SCHEMATA__.".tblfaqcustomer");
define("__DBC_SCHEMATA_MANAGEMENT_TERMS_VERSION_CUSTOMER__",__DBC_SCHEMATA__.".tbltermsandconditioncustomerversions");
define("__DBC_SCHEMATA_MANAGEMENT_FAQ_VERSION_CUSTOMER__",__DBC_SCHEMATA__.".tblfaqcustomerversion");

define("__DBC_SCHEMATA_UPLOAD_FILES_ADMIN__",__DBC_SCHEMATA__.".tblbulkuploadrecordfile");
define("__DBC_SCHEMATA_PAYMENT_PAYPAL_LOG__",__DBC_SCHEMATA__.".tblpaymentpaypallog");
define("__DBC_SCHEMATA_FORWARDER_AGREEMENT_LOG__",__DBC_SCHEMATA__.".tblforwarderagreementlog");
define("__DBC_SCHEMATA_PAYMENT_ZOOZ_LOG__",__DBC_SCHEMATA__.".tblzoozpaymentlog");
define("__DBC_SCHEMATA_FORWARDER_TRANSACTION__",__DBC_SCHEMATA__.".tblforwardertransactions");
define("__DBC_SCHEMATA_TRANSACTION_BATCH_HISTORY__",__DBC_SCHEMATA__.".tbltransactionbatchhistory");
define("__DBC_SCHEMATA_BOOKING_ACKNOWLEDMENT__",__DBC_SCHEMATA__.".tblbookingaknowledgment");
define("__DBC_SCHEMATA_BOOKING_INVOICE_LOGS__",__DBC_SCHEMATA__.".tblbookinginvoicelogs");

define("__DBC_SCHEMATA_STARTING_NEW_TRADES__",__DBC_SCHEMATA__.".tblstartingnewtrades");
define("__DBC_SCHEMATA_PAYMENT_INVOICE_LOGS__",__DBC_SCHEMATA__.".tblpaymentinvoicelogs");
define("__DBC_SCHEMATA_FORWARDER_CURRENCY_LOGS__",__DBC_SCHEMATA__.".tblforwardercurrencychangelogs");
define("__DBC_SCHEMATA_PAYMENT_BATCH_NUMBER_LOGS__",__DBC_SCHEMATA__.".tblpaymentbatchnumberlogs"); 

define("__DBC_SCHEMATA_EDIT_CUSTOMER_TEMP_DATA__",__DBC_SCHEMATA__.".tbleditusertempdata");

define("__DBC_SCHEMATA_CITY_EXONYMS_DATA__",__DBC_SCHEMATA__.".tblcityexonyms");
define("__DBC_SCHEMATA_RSS_FEED__",__DBC_SCHEMATA__.".tblrssfeed");

define("__DBC_SCHEMATA_VERSION__",__DBC_SCHEMATA__.".tblversion");
define("__DBC_SCHEMATA_HAULAGE_MODELS__",__DBC_SCHEMATA__.".tblhaulagemodels");
define("__DBC_SCHEMATA_MESSAGES_LOG__",__DBC_SCHEMATA__.".tblmessagelog");

define("__DBC_SCHEMATA_CROWN_JOB_LOG__",__DBC_SCHEMATA__.".tblcrownjobs");
define("__DBC_SCHEMATA_TRANSPORTECA_API_LOGS__",__DBC_SCHEMATA__.".tbltransportecaapilogs");

define("__DBC_SCHEMATA_AVAILABLE_SELECTION__",__DBC_SCHEMATA__.".tblcountrtyavailableselection");

define("__DBC_SCHEMATA_HAULAGE_ZONE__",__DBC_SCHEMATA__.".tblhaulagezones");
define("__DBC_SCHEMATA_HAULAGE_MODELS_CFS_MAPPING__",__DBC_SCHEMATA__.".tblhaulagecfsmodelsmapping");
define("__DBC_SCHEMATA_HAULAGE_PRICING_MODELS__",__DBC_SCHEMATA__.".tblhaulagepricingmodels");
define("__DBC_SCHEMATA_HAULAGE_PRICING__",__DBC_SCHEMATA__.".tblhaulagepricing");
define("__DBC_SCHEMATA_HAULAGE_POSTCODE__",__DBC_SCHEMATA__.".tblhaulagepostcode");

define("__DBC_SCHEMATA_HAULAGE_DISTANCE__",__DBC_SCHEMATA__.".tblhaulagedistance");
define("__DBC_SCHEMATA_BOOKING_HAULAGE_DETAILS__",__DBC_SCHEMATA__.".tblhaulagebookingdetails");


define("__DBC_SCHEMATA_WAREHOUSE_TEMP_DATA__",__DBC_SCHEMATA__.".tblwarehousestempdata");
define("__DBC_SCHEMATA_AWATING_APPROVAL__",__DBC_SCHEMATA__.".tblawatingforapproval");
define("__DBC_SCHEMATA_HAULAGE_ZONES_COUNTRY_DETAILS__",__DBC_SCHEMATA__.".tblhaulagezonescountries");
define("__DBC_SCHEMATA_WAREHOUSE_COUNTRIES__",__DBC_SCHEMATA__.".tblwarehousecountries");
define("__DBC_SCHEMATA_YAHOO_API_LOGS__",__DBC_SCHEMATA__.".tblyahooapilogs");

define("__DBC_SCHEMATA_EXPLAIN_DATA__",__DBC_SCHEMATA__.".tblexplaindata");
define("__DBC_SCHEMATA_EXPLAIN_CONTENT__",__DBC_SCHEMATA__.".tblexplaincontent");
define("__DBC_SCHEMATA_BLOG__",__DBC_SCHEMATA__.".tblblog");
define("__DBC_SCHEMATA_EXPACTED_CARGO__",__DBC_SCHEMATA__.".tblexpectedcargo");
define("__DBC_SCHEMATA_EXPACTED_CARGO_DETAILS__",__DBC_SCHEMATA__.".tblexpectedcargodetails");
define("__DBC_SCHEMATA_ADMIN_DASHBOARD__",__DBC_SCHEMATA__.".tbldashboardgraph");
define("__DBC_SCHEMATA_COMPARE_BOOKING__",__DBC_SCHEMATA__.".tblcomparebutton");
define("__DBC_SCHEMATA_NOTIFICATION__",__DBC_SCHEMATA__.".tblnotifications");
define("__DBC_SCHEMATA_SELECT_SERVICE__",__DBC_SCHEMATA__.".tblselectservice");
define("__DBC_SCHEMATA_LANDING_PAGE_DATA__",__DBC_SCHEMATA__.".tbllandingpagedata");
define("__DBC_SCHEMATA_SEO_PAGE_DATA__",__DBC_SCHEMATA__.".tblseopagedata");
define("__DBC_SCHEMATA_SITEMAP_VIDEO_DATA__",__DBC_SCHEMATA__.".tblsitemapvideo");
define("__DBC_SCHEMATA_SEO_PAGE_TRACKER__",__DBC_SCHEMATA__.".tblseopagetracker");
define("__DBC_SCHEMATA_RSS_TEMPLATES__",__DBC_SCHEMATA__.".tblrsstemplates"); 
define("__DBC_SCHEMATA_SERVICE_TEMP_DATA__",__DBC_SCHEMATA__.".tbltempservicedata");
define("__DBC_SCHEMATA_CUSTOM_LANDING_PAGE__",__DBC_SCHEMATA__.".tblcustomlandingpages"); 
define("__DBC_SCHEMATA_CUSTOMER_TESTIMONIAL__",__DBC_SCHEMATA__.".tblcustomertestimonial");
define("__DBC_SCHEMATA_PARTNER_LOGO__",__DBC_SCHEMATA__.".tblpartnerlogos");
define("__DBC_SCHEMATA_INSURANCE_RATES__",__DBC_SCHEMATA__.".tblinsurancerates");
define("__DBC_SCHEMATA_INSURANCE_VENDOR_EMAIL_LOGS__",__DBC_SCHEMATA__.".tblinsurancevendoremaillogs"); 
define("__DBC_SCHEMATA_LINK_BUILDER__",__DBC_SCHEMATA__.".tbllinkbuilder");
define("__DBC_SCHEMATA_TRANSPORT_MODE__",__DBC_SCHEMATA__.".tbltransportmode");
define("__DBC_SCHEMATA_IP_LOCATION__",__DBC_SCHEMATA__.".tbliplocation");
define("__DBC_SCHEMATA_IP_LOCATION_DETAILS__",__DBC_SCHEMATA__.".tbliplocationdetails");

define("__DBC_SCHEMATA_CAPSULE_CRM_DATA__",__DBC_SCHEMATA__.".tblcapsulecrmdata");
define("__DBC_SCHEMATA_GRAPH_WORKING_CAPITAL_CURRENT__",__DBC_SCHEMATA__.".tblgraphworkingcapitalcurrent");
define("__DBC_SCHEMATA_CAPSULE_CRM_ID_LOGS__",__DBC_SCHEMATA__.".tblcapsulecrmidlogs");
  
define("__DBC_SCHEMATA_FILE_STATUS__",__DBC_SCHEMATA__.".tblfilestatus");
define("__DBC_SCHEMATA_TASK_STATUS__",__DBC_SCHEMATA__.".tbltasks"); 
define("__DBC_SCHEMATA_SERVICE_TERMS__",__DBC_SCHEMATA__.".tblserviceterms"); 

define("__DBC_SCHEMATA_REGIONS__",__DBC_SCHEMATA__.".tblregion"); 
define("__DBC_SCHEMATA_QUOTE_PRICING_DETAILS__",__DBC_SCHEMATA__.".tblquotepricingdetails");  
define("__DBC_SCHEMATA_INSURANCE__",__DBC_SCHEMATA__.".tblinsurance"); 
define("__DBC_SCHEMATA_SEARCHED_TRADES__",__DBC_SCHEMATA__.".tblsearchedtrades"); 
define("__DBC_SCHEMATA_OPERATION_STATISTICS__",__DBC_SCHEMATA__.".tbloperationstatistics"); 

define("__DBC_SCHEMATA_COUIER_PROVIDER__",__DBC_SCHEMATA__.".tblcourierproviders");
define("__DBC_SCHEMATA_COUIER_PROVIDER_PRODUCTS__",__DBC_SCHEMATA__.".tblcourierproviderproducts");
define("__DBC_SCHEMATA_COUIER_PROVIDER_PRODUCTS_PACKING__",__DBC_SCHEMATA__.".tblcourierproviderproductspacking");
define("__DBC_SCHEMATA_COUIER_CARGO_LIMITATION_TYPE_PACKING__",__DBC_SCHEMATA__.".tblcouriercargolimitationtypes");

define("__DBC_SCHEMATA_COUIER_CARGO_LIMITATION_DATA__",__DBC_SCHEMATA__.".tblcouriercargolimitations");

define("__DBC_SCHEMATA_COUIER_PRODUCT_TERMS_INSTRUCTION_DATA__",__DBC_SCHEMATA__.".tblcourierproviderproductinstructionandterms");

define("__DBC_SCHEMATA_COUIER_TRUCKICON_DATA__",__DBC_SCHEMATA__.".tblcouriertruckicon");


define("__DBC_SCHEMATA_COUIER_AGREEMENT_DATA__",__DBC_SCHEMATA__.".tblcourieragreement");
define("__DBC_SCHEMATA_COUIER_SERVICES_OFFERED_DATA__",__DBC_SCHEMATA__.".tblcourierservicesoffered");
define("__DBC_SCHEMATA_COUIER_TRADES_OFFERED_DATA__",__DBC_SCHEMATA__.".tblcouriertradesoffered");

define("__DBC_SCHEMATA_COUIER_SEARCH_DATA__",__DBC_SCHEMATA__.".tblcourierserachdata");

define("__DBC_SCHEMATA_VAT_APPLICATION_DATA__",__DBC_SCHEMATA__.".tblvatapplication");  
define("__DBC_SCHEMATA_COURIER_LABEL_UPLOADED_DATA__",__DBC_SCHEMATA__.".tblcourierlabeluploadedfile");
define("__DBC_SCHEMATA_BOOKING_FILE_EMAIL_LOGS__",__DBC_SCHEMATA__.".tblbookingfileemaillogs");
define("__DBC_SCHEMATA_COURIER_EXCLUDED_TRADES__",__DBC_SCHEMATA__.".tblcourierexcludedtrades"); 
define("__DBC_SCHEMATA_BOOKING_FILE_SEARCH_LOGS__",__DBC_SCHEMATA__.".tblbookingfilesearchlogs"); 
define("__DBC_SCHEMATA_RESPONSE_TIME_TRACKER__",__DBC_SCHEMATA__.".tblresponsetimetracker"); 
define("__DBC_SCHEMATA_VISITORS_REDIRECT_LOGS__",__DBC_SCHEMATA__.".tblvisitorredirectlogs"); 
define("__DBC_SCHEMATA_STRIPE_API_LOGS__",__DBC_SCHEMATA__.".tblstripeapilogs"); 
define("__DBC_SCHEMATA_STANDARD_CARGO__",__DBC_SCHEMATA__.".tblstandardcargo");
define("__DBC_SCHEMATA_STANDARD_CARGO_CATEGORY__",__DBC_SCHEMATA__.".tblstandardcargocategory");
define("__DBC_SCHEMATA_ERROR_DETAIL__",__DBC_SCHEMATA__.".tblerrordetails");
define("__DBC_SCHEMATA_PARTNERS__",__DBC_SCHEMATA__.".tblpartners"); 
define("__DBC_SCHEMATA_PARTNERS_API_CALL__",__DBC_SCHEMATA__.".tblpartnerapicall");
define("__DBC_SCHEMATA_PARTNERS_API_CARGO_DETAIL__",__DBC_SCHEMATA__.".tblpartnerapicargodetail"); 
define("__DBC_SCHEMATA_PARTNERS_API_EVENT_LOG_CALL__",__DBC_SCHEMATA__.".tblpartnerapieventlog");
define("__DBC_SCHEMATA_PARTNERS_API_RESPONSE__",__DBC_SCHEMATA__.".tblpartnerapiresponse");
define("__DBC_SCHEMATA_CUSTOMER_LOG_SNIPPET__",__DBC_SCHEMATA__.".tblcustomerlogsnippet"); 

#Defining clone databases 
#define("__DBC_SCHEMATA_INVESTORS__",__DBC_SCHEMATA__.".tblinvestors");

define("__DBC_SCHEMATA_CLONE_BOOKING__",__DBC_SCHEMATA__.".tblclonebookings");
define("__DBC_SCHEMATA_CLONE_FILE_LOGS__",__DBC_SCHEMATA__.".tblclonebookingfiles");
define("__DBC_SCHEMATA_CLONE_CARGO__",__DBC_SCHEMATA__.".tblclonecargo");
define("__DBC_SCHEMATA_COURIER_CLONE_BOOKING_DATA__",__DBC_SCHEMATA__.".tblclonecourierbooking");
define("__DBC_SCHEMATA_CLONE_SHIPPER_CONSIGNEE__",__DBC_SCHEMATA__.".tblcloneshipperconsignee");
define("__DBC_SCHEMATA_LCL_EXCLUDED_TRADES__",__DBC_SCHEMATA__.".tbllclexcludedtrades");
define("__DBC_MAUTIC_AUTH_CRIDENTIALS__",__DBC_SCHEMATA__.".tblmauticauthcridentials");
define("__DBC_SCHEMATA_MAUTIC_API_LOGS__",__DBC_SCHEMATA__.".tblmauticapilogs");
define("__DBC_SCHEMATA_USA_STATES__",__DBC_SCHEMATA__.".tblusastates");
define("__DBC_SCHEMATA_LOGIN_LOG__",__DBC_SCHEMATA__.".tblloginlog");
define("__DBC_SCHEMATA_WP_POSTS__",__DBC_SCHEMATA__.".wp_posts");
define("__DBC_SCHEMATA_WP_POST_META__",__DBC_SCHEMATA__.".wp_postmeta");
define("__DBC_SCHEMATA_WP_TERMS__",__DBC_SCHEMATA__.".wp_terms");
define("__DBC_SCHEMATA_WP_TERMS_RELATIONSHIPS__",__DBC_SCHEMATA__.".wp_term_relationships");
define("__DBC_SCHEMATA_STANDARD_CARGO_HANDLING__",__DBC_SCHEMATA__.".tblstandardcargohandling");
define("__DBC_SCHEMATA_STANDARD_PRICING__",__DBC_SCHEMATA__.".tblstandardcargopricing");
define("__DBC_SCHEMATA_BOOKING_HANDLING_FEE__",__DBC_SCHEMATA__.".tblbookingshandlingfee");

define("__DBC_SCHEMATA_ZOPIM_LOGS__",__DBC_SCHEMATA__.".tblzopimlogs");
define("__DBC_SCHEMATA_ZOPIM_CHAT_HISTORY_LOGS__",__DBC_SCHEMATA__.".tblzopimchatlogs");
define("__DBC_SCHEMATA_COURIER_PRODUCT_TNT_MAPPING_SERVICE_CODE__",__DBC_SCHEMATA__.".tblcourierproducttntapimappingcodes");
define("__DBC_SCHEMATA_TNT_SERVICE_CONVERTIONS__",__DBC_SCHEMATA__.".tbltntserviceconversions");

define("__DBC_SCHEMATA_WAREHOUSE_TYPE__",__DBC_SCHEMATA__.".tblwarehousetype");

  
 
/*
*   $partnerApiCall = (int)($_REQUEST['transporteca']['transportecaPartnerTestApiCall']); 
    if($partnerApiCall==1)
    {
        define("__DBC_SCHEMATA_BOOKING__",__DBC_SCHEMATA__.".tblclonebookings");
        define("__DBC_SCHEMATA_FILE_LOGS__",__DBC_SCHEMATA__.".tblclonebookingfiles");
        define("__DBC_SCHEMATA_CARGO__",__DBC_SCHEMATA__.".tblclonecargo");
        define("__DBC_SCHEMATA_COURIER_BOOKING_DATA__",__DBC_SCHEMATA__.".tblclonecourierbooking"); 
        define("__DBC_SCHEMATA_SHIPPER_CONSIGNEE__",__DBC_SCHEMATA__.".tblcloneshipperconsignee");
    }
    else
    {
        define("__DBC_SCHEMATA_BOOKING__",__DBC_SCHEMATA__.".tblbookings");
        define("__DBC_SCHEMATA_FILE_LOGS__",__DBC_SCHEMATA__.".tblbookingfiles");
        define("__DBC_SCHEMATA_CARGO__",__DBC_SCHEMATA__.".tblcargo");
        define("__DBC_SCHEMATA_COURIER_BOOKING_DATA__",__DBC_SCHEMATA__.".tblcourierbooking"); 
        define("__DBC_SCHEMATA_SHIPPER_CONSIGNEE__",__DBC_SCHEMATA__.".tblshipperconsignee");
    } 
*/

define("__DBC_SCHEMATA_BOOKING__",__DBC_SCHEMATA__.".tblbookings");
define("__DBC_SCHEMATA_FILE_LOGS__",__DBC_SCHEMATA__.".tblbookingfiles");
define("__DBC_SCHEMATA_CARGO__",__DBC_SCHEMATA__.".tblcargo");
define("__DBC_SCHEMATA_COURIER_BOOKING_DATA__",__DBC_SCHEMATA__.".tblcourierbooking"); 
define("__DBC_SCHEMATA_SHIPPER_CONSIGNEE__",__DBC_SCHEMATA__.".tblshipperconsignee");

define("__DBC_SCHEMATA_REMIND_CMS_EMAIL__",__DBC_SCHEMATA__.".cms_sections_remind");
define("__DBC_SCHEMATA_ADMIN_ROLES__",__DBC_SCHEMATA__.".tbladminroles");
define("__DBC_SCHEMATA_ADMIN_ROLES_PERMISSION_MAPPING__",__DBC_SCHEMATA__.".tbladminpermissionmapping");
define("__DBC_SCHEMATA_BOOKING_ACTIVITY__",__DBC_SCHEMATA__.".tblbookingactivity");
define("__DBC_SCHEMATA_GMAIL_INBOX_LOGS__",__DBC_SCHEMATA__.".tblgmailinboxlogs");
define("__DBC_SCHEMATA_GMAIL_SYNCHRONISATION_LOGS__",__DBC_SCHEMATA__.".tblgmailsynchronizationlogs");
define("__DBC_SCHEMATA_PRIVATE_CUSTOMER_SETTING__",__DBC_SCHEMATA__.".tblforwarderprivatecustomersetting"); 
define("__DBC_SCHEMATA_GMAIL_PROCESS_TRACKER__",__DBC_SCHEMATA__.".tblgmailprocesstracker");
define("__DBC_SCHEMATA_FILE_SEEN_LOGS__",__DBC_SCHEMATA__.".tblfileseenlogs");
define("__DBC_SCHEMATA_PAYMENT_FREQUENCY__",__DBC_SCHEMATA__.".tblpaymentfrequency");

define("__DBC_SCHEMATA_DEVELOPER_TASKS__",__DBC_SCHEMATA__.".tbldevelopertasks");
define("__DBC_SCHEMATA_FORWARDER_PRICING__",__DBC_SCHEMATA__.".tblforwarderpricing"); 

define ("__SESSION_LIFETIME__", "+ 12 Hours");
define("__MYSQL_DEBUG__",'true'); 
//Email Templates
define("__CREATE_USER_ACCOUNT__","__CREATE_USER_ACCOUNT__");
define("__CREATE_USER_ACCOUNT_SUBJECT__","Verification of e-mail address");

define("__UPDATE_CONTACT_INFORMATION__","__UPDATE_CONTACT_INFORMATION__");
define("__UPDATE_CONTACT_INFORMATION_SUBJECT__","Verification of e-mail address");

define("__USER_INVITATION__","__USER_INVITATION__");
define("__USER_INVITATION_SUBJECT__","User Invite E-mail For Verification.");

define('__APP_PATH_DATA_CSV__',__APP_PATH_ROOT__.'/importdb/dataxlsx');

define("__STORE_SUPPORT_EMAIL__","contact@transporteca.com");
define("__FINANCE_CONTACT_EMAIL__","accounts@transporteca.com");
define("__STORE_SUPPORT_NAME__","Transporteca");
define("__STORE_SUPPORT_PHONE_NUMBER__","7199 7299");
define("__STORE_SUPPORT_PHONE_NUMBER_COUNTRY_CODE__","45");

define("__GOOGLE_CURRENCY_CONVERSION_API__","http://www.google.com/ig/calculator");
define("__TO_CURRENCY_CONVERSION__","USD");

///SMTP Email Login Details
define("__SMTP_PASSWORD__","Tran5port");
define("__SMTP_USERNAME__","transporteca@xeroda.com");
define("__SMTP_HOST__","smtp.emailsrvr.com");
define("__DBC_SCHEMATA_POSTCODE1__",__DBC_SCHEMATA__.".tblpostcode");


define('__COOKIE_NAME__','Redirect_uri');
define('__COOKIE_TIME__','86400');

define('__SEND_FORWARDER_SUGGESTION__','__SEND_FORWARDER_SUGGESTION__');
define('__SEND_FORWARDER_SUGGESTION_SUBJECT__','Feedback on Transporteca');

define('__FORGOT_PASSWORD__','__FORGOT_PASSWORD__');
define('__FORGOT_PASSWORD_SUBJECT__','Forgot Password E-mail');

define('__RATING_STAR_WIDTH__','17');

define('__ASK_GEO_API_URL__','http://api.askgeo.com/v1');
define('__ASK_GEO_API_KEY__','1jt2m9e5j40gd9uaj2mfem6usavkv16njvp7fc1r9vrq5hu0s1qg');
define('__ASK_GEO_ACCOUNT_ID__','220');
define('__ASK_GEO_FORMAT__','json');
define('__ASK_GEO_DATABATE__','TimeZone');

define('__APP_PATH_INVOICE__',__APP_PATH_ROOT__.'/invoice/');
define('__APP_PATH_PAYPAL__',__APP_PATH_ROOT__.'/paypal/paypal_ipn.log');
define('__BUSINESS_NAME__','X32WCHBRUK8AE');
//define('__PAYPAL_PAYMENT_URL__','https://securepayments.sandbox.paypal.com/acquiringweb');
define('__PAYPAL_TEMPLATE__','templateD');

define('__FORWARDER_SIGNUP_EMAIL__','__FORWARDER_SIGNUP_EMAIL__');

define("__DBC_SCHEMATA_WAREHOUSES_PRICING_TEMP__",__DBC_SCHEMATA__.".tblpricingwtw_temp");
define("__DBC_SCHEMATA_CITY_NOT_FOUND__",__DBC_SCHEMATA__.".tblcitynotfound");
define("__DBC_SCHEMATA_SEARCH_PHRASE__",__DBC_SCHEMATA__.".tblsearchphrase");
define("__DBC_SCHEMATA_MANAGEMENT_MESSAGES__",__DBC_SCHEMATA__.".tbladminmessages");
 
define("__ID_COUNTRY_HONG_KONG__",'100');
define("__ID_COUNTRY_CHINA__",'46');
  
define("__FORWARDER_PROFIT_TYPE_REFERRAL_FEE__",'1');
define("__FORWARDER_PROFIT_TYPE_MARK_UP__",'2'); 

define("__FORWARDER_PROFIT_TYPE_REFERRAL_FEE_TEXT__",'Referral fee off invoice');
define("__FORWARDER_PROFIT_TYPE_MARK_UP_TEXT__",'Markup on quote'); 

define("__FORWARDER_BULK_UPLOAD_EMAIL__","__FORWARDER_BULK_UPLOAD_EMAIL__");
define("__FORWARDER_BULK_UPLOAD_EMAIL_SUBJECT__","Upload confirmation from Transporteca � LCL services");

define("__FORWARDER_BULK_UPLOAD_HAULAGE_EMAIL__","__FORWARDER_BULK_UPLOAD_HAULAGE_EMAIL__");
define("__FORWARDER_BULK_UPLOAD_HAULAGE_EMAIL_SUBJECT__","Upload confirmation from Transporteca - Haulage");


define("__FORWARDER_BULK_UPLOAD_CUSTOM_CLEARANCE_EMAIL__","__FORWARDER_BULK_UPLOAD_CUSTOM_CLEARANCE_EMAIL__");
define("__FORWARDER_BULK_UPLOAD_EMAIL_CUSTOM_CLEARANCE_SUBJECT__","Upload confirmation from Transporteca - Customs Clearance");

define("__MAX_ALLOWED_DRAFT__",'5');
define("__RECALCULATE_PRICE_INTERVAL__",'30');

define("__MAX_ALLOWED_FORWARDER_LOGO_WIDTH__",'80');
define("__MAX_ALLOWED_FORWARDER_LOGO_HEIGHT__",'30');

define("__ADMINISTRATOR_PROFILE_ID__",'1');
define("__PRICING_PROFILE_ID__",'3');
define("__BILLING_PROFILE_ID__",'5');

###########################
#  preferences constants  #
###########################
define("__BOOKING_PROFILE_ID__",1);  //Sea freight
define("__PAYMENT_AND_BILLING_PROFILE_ID__",2); 
define("__CUSTOMER_PROFILE_ID__",3);//3 

define("__AIR_FREIGHT_BOOKING_PROFILE_ID__",6);
define("__ROAD_FREIGHT_BOOKING_PROFILE_ID__",7);
define("__COURIER_FREIGHT_BOOKING_PROFILE_ID__",8);

define("__CATCH_UP_ALL_BOOKING_QUOTE_PROFILE_ID__",10);


define("__QUOTES_AND_BOOKING_PROFILE_ID__",9);

define("__WHOLE_WORLD_PREFERENCES_ID__",5001);
define("__REST_OF_WORLD_PREFERENCES_ID__",5002); 

define("__ALL_WORLD_TRANSPORT_MODE_ID__",8);
define("__REST_TRANSPORT_MODE_ID__",9);

//Service Terms ID

define("__SERVICE_TERMS_EXW__",1);
define("__SERVICE_TERMS_FOB__",2);
define("__SERVICE_TERMS_DAP__",3);
define("__SERVICE_TERMS_CFR__",4);
define("__SERVICE_TERMS_EXW_WTD__",5);
define("__SERVICE_TERMS_DAP_NO_CC__",6);

//Service Terms Short Terms 
define("__SERVICE_SHORT_TERMS_EXW__","EXW");
define("__SERVICE_SHORT_TERMS_FOB__","FOB");
define("__SERVICE_SHORT_TERMS_DAP__","DAP");
define("__SERVICE_SHORT_TERMS_CFR__","CFR");
define("__SERVICE_SHORT_TERMS_EXW_WTD__","EXW-WTD");
define("__SERVICE_SHORT_TERMS_DAP_NO_CC__","DAP-NON-CLEARED");
define("__SERVICE_SHORT_TERMS_DTD__","DTD");

#####################################
#  MANAGEMENT TEXT EDITOR CONSTANTS #
#####################################

define("__TEXT_EDIT_TERMS_FORWARDER__",'1'); //5
define("__TEXT_EDIT_FAQ_FORWARDER__",'2');//4
define("__TEXT_EDIT_TERMS_CUSTOMER__",'3');//3
define("__TEXT_EDIT_FAQ_CUSTOMER__",'4');//3
//define("__TEXT_EDIT_FAQ_CUSTOMER__",'1');//3
define("__ORDER_MOVE_DOWN__",'1');//1
define("__ORDER_MOVE_UP__",'2');//2

######### FORWARDER REFERAL FEE RATE ####
define("__FORWARDER_REFERAL_FEE__",9.5);
############ PER PAGE CONSTANT #########
define("__PER_PAGE__",'50');

define("__BOOKING_TRANSPORT_MODE_AIR__",1);  //Air freight 
define("__BOOKING_TRANSPORT_MODE_SEA__",2);  //Sea freight 
define("__BOOKING_TRANSPORT_MODE_ROAD__",3);  //Road freight 
define("__BOOKING_TRANSPORT_MODE_COURIER__",4);  //Courier freight
define("__BOOKING_TRANSPORT_MODE_FCL__",6);  //Sea freight
define("__BOOKING_TRANSPORT_MODE_FTL__",7);  //Road freight
define("__BOOKING_TRANSPORT_MODE_RAIL__",10);  //Rail freight
define("__BOOKING_TRANSPORT_MODE_MOVING__",11);  //Moving freight
                
define('__FROM_DATE__','7 DAY');
define('__TO_DATE__','14 DAY');

define("__SEND_GRID_USERNAME__","Transporteca");
define("__SEND_GRID_PASSWARD__","719y052e");

/*
* constants for service types 
*/
define("__SERVICE_TYPE_DTD__",1);
define("__SERVICE_TYPE_DTW__",2);
define("__SERVICE_TYPE_WTD__",3);
define("__SERVICE_TYPE_WTW__",4);
define("__SERVICE_TYPE_DTP__",5);
define("__SERVICE_TYPE_WTP__",6);
define("__SERVICE_TYPE_PTP__",7);
define("__SERVICE_TYPE_PTD__",8);
define("__SERVICE_TYPE_PTW__",9);
define("__MANAGEMENT_TEMPLATE_EMAIL_URL__","management.transporteca.com");
//define("__TRANSPORTECA_ADDRESS__","<br />Transporteca Limited<br />Suite 2009-10, 29/F<br />China Resources Building<br />No. 26 Harbour Road<br />Wanchai, Hong Kong<br />E-mail: contact@transporteca.com");
define("__TRANSPORTECA_ADDRESS__","<br />Transporteca Limited (reg. 1761706)<br />Office C, 23/F<br />COS Centre<br />56 Tsun Yip Street<br />Kwun Tong, Hong Kong<br />E-mail: contact@transporteca.com");
define("__BOOKING_EXPECTED_SERVICES_MESSAGE__","We had users search for following trades which you do not currently have online:");
define("__BOOKING_EXPECTED_SERVICES_NO_BOOKING_MESSAGE__","We had users search for following trades which you do not currently have online:");
//define("__BOOKING_EXPECTED_SERVICES_NO_BOOKING_MESSAGE_1__","If you offer services in any of these trades, please upload them to Transporteca to start receiving bookings. Transporteca will send the disappointed customer a notice when a service is available in the trade they are looking for, so they can revisit the portal and place their booking");
define("__BOOKING_EXPECTED_SERVICES_NO_BOOKING_MESSAGE_1__","");
define("__SERVICE_ONLINE_MESSAGE__",'In addition, we received requests for following services, which you currently do have online:');

define("__OFFLINE_FORWARDER_COMMENT__","<p>Customers on Transporteca are currently not able to find your services and book with you. Log in to your Transporteca Control Panel and follow the steps at the top of the screen to get online.</p>");
define("GET_COUNTRY_ISO_CODE__",'http://ws.geonames.org/countryCode');
//define("__YAHOO_GEOCODE_API_KEY__",'eI4L3YPV34FuuB8ScPF9XntmYClCyDFe7YYGKKwSJkBrFc4taMW1LtRjDP7M0gkiLA--');
define("__YAHOO_GEOCODE_API_URL__",'http://where.yahooapis.com/geocode?q=');
define("__YAHOO_GEOCODE_API_MAX_NUM_REQUEST__",'10000');

define("__MINI_RADIUS__","0");

define("__HAULAGE_ZONE_PRICING_MODEL_ID__",1);
define("__HAULAGE_CITY_PRICING_MODEL_ID__",2);
define("__HAULAGE_POSTCODE_PRICING_MODEL_ID__",3);
define("__HAULAGE_DISTANCE_PRICING_MODEL_ID__",4);
define("__CFS_UPDATED_BY_ADMIN_TEXT__",'Transporteca Support');

define("__MAX_LENGTH_ZONE__",30);
define("__WEIGHT_UP_TO__",100);
define("__DEFAULT_WM_FACTOR__",1000);
define("__DEFAULT_FUEL__",0);

define("__MAX_BULK_UPLOAD_FILE__",5);
define("__DBC_SCHEMATA_BULK_UPLOAD_SERVICES__",__DBC_SCHEMATA__.".tbluploadbulkservicefiles");
define("__DBC_SCHEMATA_BULK_UPLOAD_FILES__",__DBC_SCHEMATA__.".tbluploadfilesname");
define("__DBC_SCHEMATA_BULK_UPLOAD_COMMENT__",__DBC_SCHEMATA__.".tblbulkuploadservicecomment");

define("__DBC_SCHEMATA_HAULAGE_PRICING_ARCHIVE__",__DBC_SCHEMATA__.".tblhaulagepricingarchive");
define("__DBC_SCHEMATA_PRICING_CC_WAITING_FOR_APPROVAL__",__DBC_SCHEMATA__.".tblpricingccwaitingforapproval");
define("__DBC_SCHEMATA_WAREHOUSES_PRICING_APPROVAL_DATA__",__DBC_SCHEMATA__.".tblpricingwtwapprovaldata");
define("__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_APPROVAL_DATA__",__DBC_SCHEMATA__.".tblhaulagepricingapprovaldata");
define("__DBC_SCHEMATA_WAREHOUSES_HAULAGE_PRICING_MODEl_APPROVAL_DATA__",__DBC_SCHEMATA__.".tblhaulagepricingmodelsapprovaldata");

define("__DBC_SCHEMATA_GRAPH_USER__",__DBC_SCHEMATA__.".tblgraphusers");
define("__DBC_SCHEMATA_STATUS_GRAPH_USER__",__DBC_SCHEMATA__.".tblstatusgraphusers");

define("__DBC_SCHEMATA_CUSTOER_FEEDBACK__",__DBC_SCHEMATA__.".tblcustomerfeedback");

define("__DBC_SCHEMATA_CUSTOER_FEEDBACK_GRAPH__",__DBC_SCHEMATA__.".tblcustomerfeedbackgraph");
define("__DBC_SCHEMATA_QUOTES_SENT_LOGS__",__DBC_SCHEMATA__.".tblquotesentlogs");
define("__DBC_SCHEMATA_BOOKING_QUOTES__",__DBC_SCHEMATA__.".tblquotes");
define("__DBC_SCHEMATA_FEDEX_API_LOGS__", __DBC_SCHEMATA__.".tblfedexapilogs");

define("__DBC_SCHEMATA_COURIER_PRODUCT_GROUP__",__DBC_SCHEMATA__.".tblcourierproductgroup");
define("__DBC_SCHEMATA_BOOKING_FILE_ACTIVITY_LOGS__",__DBC_SCHEMATA__.".tblbookingfileactivitylogs");
define("__DBC_SCHEMATA_CRON_SCHEDULE_LOGS__",__DBC_SCHEMATA__.".tblcronjobschedulelogs");
define("__DBC_SCHEMATA_SEARCH_AID_WORDS__",__DBC_SCHEMATA__.".tblsearchadwords");
define("__DBC_SCHEMATA_PALLET_TYPES__",__DBC_SCHEMATA__.".tblpallettypes");
define("__DBC_SCHEMATA_PAYMENT_TYPES__",__DBC_SCHEMATA__.".tblpaymenttypes");


define("__DBC_SCHEMATA_AFTERSHIP_TRACKING_EVENT_LOG__",__DBC_SCHEMATA__.".tblaftershiptrackingslog");
define("__DBC_SCHEMATA_COURIER_TRACKING_LOG__",__DBC_SCHEMATA__.".tblcouriertrackinglogs");

define("__DBC_SCHEMATA_STANDARD_SHIPPERS__",__DBC_SCHEMATA__.".tblstandardshippers");

define("__DBC_SCHEMATA_SEARCH_NOTIFICATION__",__DBC_SCHEMATA__.".tblsearchnotifications");
define("__DBC_SCHEMATA_SEARCH_NOTIFICATION_PRIVATE_CUSTOMER__",__DBC_SCHEMATA__.".tblsearchnotificationsprivatecustomer");
 
define("__DBC_SCHEMATA_WORDPRESS_PAGE_URL__",__DBC_SCHEMATA__.".tblwordpresspageurl");

define("__DBC_SCHEMATA_SNOOZE_DATA__",__DBC_SCHEMATA__.".tblsnoozedata");


define("__DBC_SCHEMATA_PAGE_PERMISSION__",__DBC_SCHEMATA__.".tblpagepermission");

define("__DBC_SCHEMATA_MANUAL_FEE_CARGO_TYPE__",__DBC_SCHEMATA__.".tblcargotype");

define("__DBC_SCHEMATA_MANUAL_FEE__",__DBC_SCHEMATA__.".tblpricingmanualfee");

define("__DBC_SCHEMATA_LANGUAGE__",__DBC_SCHEMATA__.".tbllanguage");
define("__DBC_SCHEMATA_COUNTRY_NAME_MAPPING__",__DBC_SCHEMATA__.".tblcountrynamemapping");
define("__DBC_SCHEMATA_LANGUAGE_NAME_MAPPING__",__DBC_SCHEMATA__.".tbllanguagenamemapping");
define("__DBC_SCHEMATA_PAYPAL_COUNTRIES__",__DBC_SCHEMATA__.".tblpaypalcountries");

define("__DBC_SCHEMATA_LANGUAGE_CONFIGURATION__",__DBC_SCHEMATA__.".tbllanguageconfiguration");
define("__DBC_SCHEMATA_CMS_MAPPING__",__DBC_SCHEMATA__.".cms_sections_mapping");

define("__DBC_SCHEMATA_CMS_REMIND_MAPPING__",__DBC_SCHEMATA__.".cms_sections_remind_mapping");
define("__DBC_SCHEMATA_DUTY_PRODUCTS__",__DBC_SCHEMATA__.".tbldutyproducts");
define("__DBC_SCHEMATA_LAUGUAGE_META_TAGS__",__DBC_SCHEMATA__.".tbllanguagemetatags");
define("__DBC_SCHEMATA_DUMMY_PRICING__",__DBC_SCHEMATA__.".tbldummypricing");
define("__DBC_SCHEMATA_DUMMY_PRICING_EXCEPTION__",__DBC_SCHEMATA__.".tbldummypricingexception");
define("__DBC_SCHEMATA_RAIL_TRANSPORT__",__DBC_SCHEMATA__.".tblrailtransport");
define("__DBC_SCHEMATA_INSURANCE_BUY_SELL_RATES__",__DBC_SCHEMATA__.".tblinsurancesellbuyrate");
 

define("__DBC_SCHEMATA_DATA_PINE__",__DBC_SCHEMATA__.".tbldatapine");
define("__DBC_SCHEMATA_DATA_PINE_FINACIAL_DATA__",__DBC_SCHEMATA__.".tbldatapinefinacialdata");
define("__DBC_SCHEMATA_GP_RANGE_DATA__",__DBC_SCHEMATA__.".tblgprange");
define("__DBC_SCHEMATA_SIGNUPS_DATA__",__DBC_SCHEMATA__.".tblsignups");

define("__DBC_SCHEMATA_WP_ICL_TRANSLATION_BATCHES__",__DBC_SCHEMATA__.".wp_icl_translation_batches");
define("__DBC_SCHEMATA_WP_ICL_TRANSLATION_STATUS__",__DBC_SCHEMATA__.".wp_icl_translation_status");
define("__DBC_SCHEMATA_WP_ICL_TRANSLATIONS__",__DBC_SCHEMATA__.".wp_icl_translations");
define("__DBC_SCHEMATA_WP_ICL_TRANSLATE_JOB__",__DBC_SCHEMATA__.".wp_icl_translate_job");
define("__DBC_SCHEMATA_WP_ICL_TRANSLATE__",__DBC_SCHEMATA__.".wp_icl_translate");
define("__DBC_SCHEMATA_WP_TERM_TAXONOMY__",__DBC_SCHEMATA__.".wp_term_taxonomy");  
define("__DBC_SCHEMATA_BOOKING_REMIND_EMAIL_DRAFT__",__DBC_SCHEMATA__.".tblbookingremindemaildraft");
define("__DBC_SCHEMATA_BOOKING_FILE_VISITOR_LOGS__",__DBC_SCHEMATA__.".tblbookingfilevisitorlogs");

define("__DBC_SCHEMATA_CUSTOMER_API_LOGIN__",__DBC_SCHEMATA__.".tblcustomerapilogin");
define("__DBC_SCHEMATA_NO_CUSTOM_CLEARANCE__",__DBC_SCHEMATA__.".tblnocustomsclearance");
define("__DBC_SCHEMATA_FORWARDER_TOTAL_BOOKING_SNAPSHOT__",__DBC_SCHEMATA__.".tblforwardertotalbookingsnapshot");
define("__DBC_SCHEMATA_TNT_POSTCODE__",__DBC_SCHEMATA__.".tbltntpostcode");
define("__DBC_SCHEMATA_COURIER_DELIVERY_TIME__",__DBC_SCHEMATA__.".tblcourierdeliverytime");
define("__DBC_SCHEMATA_COURIER_TRACKING_NOT_COLLECTED__",__DBC_SCHEMATA__.".tblcouriertrackingnotcollected");
define("__DBC_SCHEMATA_TRANSPORTECA_OFFICE_HOURS__",__DBC_SCHEMATA__.".tbltransportecaofficehours"); 
define("__DBC_SCHEMATA_BOOKING_FILE_TASK_QUEUES__",__DBC_SCHEMATA__.".tblbookingfilestaskqueues");   
define("__DBC_SCHEMATA_AUTOMATED_RFQ_RESPONSE__",__DBC_SCHEMATA__.".tblautomatedrfqresponse");  
define("__DBC_SCHEMATA_PARTNER_RAW_DATA_LOG__",__DBC_SCHEMATA__.".tblpartnerrawdatalog");

 
define("__COST_AWAITING_APPROVAL__",2);
define("__DATA_SUBMITTED__",1);
define("__MANAGEMENT_COMPLETE_APPROVAL__",4);
define("__COST_APPROVED_BY_FORWARDER__",3);
define("__DATA_APPROVED_BY_FORWARDER__",5);
define("__DATA_REVIEWED_BY_FORWARDER__",6);

define("__UPLOAD_FORWARDER_BULK_SERVICES__",__APP_PATH_ROOT__."/forwarders/forwarderBulkUploadServices");
define("__UPLOAD_FORWARDER_BULK_SERVICES_TEMP__",__UPLOAD_FORWARDER_BULK_SERVICES__."/temp");


define("__TIME_PER_ROW_CREATE__","0.06");

define("__CONTENT_PER_PAGE_POSTCODE__",100);

//this is for Partial Empty postcode popup
define("__POSTCODE_PER_PAGE_POSTCODE__",100);
define("__PARTIAL_EMPTY_POSTCODE_LINKS_PER_PAGE__",10);

define("__CARGO_EXCEED_EMAIL_SUBJECT__","Quote request exceeding scope of Transporteca received");
define("__DANGEROUS_CARGO_EMAIL_SUBJECT__","Quote request for dangerous goods received on Transporteca");
//define("__CUSTOMER_CANCEL_BOOKING_EMAIL_SUBJECT__","Quote request exceeding scope of Transporteca received");

define("__SERVICE_TYPE_Q_CANVAS_HEIGHT__",140);
define("__SERVICE_TYPE_Q_CANVAS_WEIGHT__",900);

//DEFINE FLAGS FOR E-MAIL LOGS
define("__FLAG_FOR_DEFAULT__",0);
define("__FLAG_FOR_CUSTOMER__",1);
define("__FLAG_FOR_FORWARDER__",2);
define("__FLAG_FOR_MANAGEMENT__",3);

//Courier Agrrement Status 
define("__AGREEMENT_PENDING_REVIEW__","Pending review");
define("__AGREEMENT_WORKING__","Working");
define("__AGREEMENT_NO_TRADES__","No trades");
define("__AGREEMENT_NO_PRICING__","No pricing");
define("__AGREEMENT_NO_SERVICE__","No services");
  
//DEFINE FLAGS FOR Booking Status
define("__BOOKING_STATUS_DRAFT__",1);
define("__BOOKING_STATUS_ON_HOLD__",2);
define("__BOOKING_STATUS_CONFIRMED__",3);
define("__BOOKING_STATUS_ARCHIVED__",4);
define("__BOOKING_STATUS_DELETED__",5);
define("__BOOKING_STATUS_ZERO_LEVEL__",6);
define("__BOOKING_STATUS_CANCELLED__",7);

define("__PROCESS_BAR_WIDTH_ONE__",35);
define("__PROCESS_BAR_WIDTH_TWO__",50);
define("__PROCESS_BAR_WIDTH_THREE__",75);
define("__PROCESS_BAR_WIDTH_FOUR__",90);
define("__PROCESS_BAR_WIDTH_FIVE__",95);

define("__BOOKING_INSURANCE_STATUS_NEW_BOOKING__",0);
define("__BOOKING_INSURANCE_STATUS_SENT__",1);
define("__BOOKING_INSURANCE_STATUS_CONFIRMED__",2);
define("__BOOKING_INSURANCE_STATUS_CANCELLED__",3);

define("__BOOKING_QUOTES_STATUS_NEW_BOOKING__",1);
define("__BOOKING_QUOTES_STATUS_SENT__",2);
define("__BOOKING_QUOTES_STATUS_WON__",3);
define("__BOOKING_QUOTES_STATUS_EXPIRED__",4);

//constants for Notification types
define("__NOTIFICATION_TYPE_NO_SERVICE_COUNTRY_TO_COUNTRY___",1); //X1/X2
define("__NOTIFICATION_TYPE_NO_DELIVERY_TO_DESTINATION_COUNTRY___",2); // F1/F2/F7/F8
define("__NOTIFICATION_TYPE_NO_PICKUP_FROM_ORIGIN_COUNTRY___",3); //F3/F4/F5/F6
define("__NOTIFICATION_TYPE_NO_PICKUP_AND_NO_DELIVERY___",4); //F9/F10/F11/F12/F13/F14
define("__NOTIFICATION_TYPE_RP1_RP2___",5);
define("__NOTIFICATION_TYPE_S1_S2___",6);
define("__NOTIFICATION_TYPE_T1_T2___",7);
define("__NOTIFICATION_TYPE_SORRY_EMAIL_S1_S2___",8);
define("__NOTIFICATION_TYPE_SORRY_EMAIL_T1_T2___",9);


define("__MAX_COUNTRY_NAME_LENGTH_ON_BUTTONS___",18);
define("__NEW_FORWARDER_EMAIL_SUBJECT__","Add new forwarder profile");
define("__CUSTOMER_FEEDBACK_RATING__",10);

define("__CONTENT_PER_PAGE_CUSTOM_CLEARENCE__",50);

define("__CUSTOMER_FEEDBACK_RED_SCORE__",6);
define("__CUSTOMER_FEEDBACK_YELLOW_SCORE__",8);
define("__CUSTOMER_FEEDBACK_GREEN_SCORE__",10);

define("__CONTENT_PER_PAGE_MESSAGES_REVIEWS__",50);
define("__CONTENT_PER_PAGE_CUSTOMER_LOG__",25);
define("__MAX_NUM_RECORD_PER_FORWARDER__",5);
define("__MAX_CONFIRMED_BOOKING_PER_PAGE__",50);
define("__MAX_CUSTOMER_PER_PAGE__",50);
define("__MAX_CONFIRMED_BOOKING_NEW_PER_PAGE__",100);

define("__LANGUAGE_ID_ENGLISH__",1);
define("__LANGUAGE_ID_DANISH__",2);
define("__LANGUAGE_ID_SWEDISH__",3);
define("__LANGUAGE_ID_NORWEGIAN__",4);

define("__LANGUAGE_TEXT_ENGLISH__",'english');
define("__LANGUAGE_TEXT_DANISH__",'danish');
define("__LANGUAGE_TEXT_SWEDISH__",'swedish');
define("__LANGUAGE_TEXT_NORWEGIAN__",'norwegian');


define("__LANGUAGE_ID_ENGLISH_CODE__","en");

define("__MAX_ROW_COUNT_UPLOAD_FILE_EXCEEDS__",10000);

define("__DEFAULT_CARGO_VOLUME__",'5');
define("__DEFAULT_CARGO_WEIGHT__",'1000');

define("__DEFAULT_CARGO_PACKET_LENGTH__",'50');
define("__DEFAULT_CARGO_PACKET_HEIGHT__",'40');
define("__DEFAULT_CARGO_PACKET_WIDTH__",'30');
define("__DEFAULT_CARGO_PACKET_QUANTITY__",'1');
define("__DEFAULT_CARGO_PACKET_WEIGHT__",'8');

define("__CONTENT_PER_PAGE_PENDING_TRAY__",50);
define("__CONTENT_PER_PAGE_PENDING_TRAY_SEARCH__",25); 

define("__EXPLAIN_LEVEL2_IMAGES__",__APP_PATH__."/explainImages/level2");
define("__EXPLAIN_LEVEL3_IMAGES__",__APP_PATH__."/explainImages/level3");

define("__BASE_EXPLAIN_LEVEL2_IMAGES_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/explainImages/level2");
define("__BASE_EXPLAIN_LEVEL3_IMAGES_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/explainImages/level3");

define("__HOW_IT_WORKS__",'1');
define("__COMPANY__",'2');
define("__TRANSTPORTPEDIA__",'3');

define("__TRANSPORTECA_PAYMENT_TYPE_1__",'Stripe');
define("__TRANSPORTECA_PAYMENT_TYPE_2__",'Paypal');
define("__TRANSPORTECA_PAYMENT_TYPE_3__",'Transfer');
define("__TRANSPORTECA_PAYMENT_TYPE_4__",'Unknown');



define("__HOW_IT_WORKS_URL__",'how-it-works');
define("__COMPANY_URL__",'company');
define("__TRANSTPORTPEDIA_URL__",'transportpedia');

define("__HOW_IT_WORKS_URL_DA__",'saadan-virker-det');
define("__COMPANY_URL_DA__",'virksomheden');
define("__TRANSTPORTPEDIA_URL_DA__",'leksikon');

define("__INSURANCE_BUY_RATE_TYPE__",'1');
define("__INSURANCE_SELL_RATE_TYPE__",'2');
define("__MAXMIND_API_KEY__",'eWVIQwuqwvYz');
define("__TRANSPORTECA_DEFAULT_TRANSPORT_MODE__",'LCL');
define("__TRANSPORT_MODE_LCL__",'2');

define("__INSURANCE_IMAGINARY_PROFIT__",'10');

//tblcouriercargolimitationtypes
define("__WEIGHT_PER_COLI__",1);
define("__GIRTH_PER_COLI__",2);
define("__LENGTH_PER_COLI__",3);
define("__HEIGHT_PER_COLI__",4);
define("__NUMBER_OF_COLLI__",5); 
define("__TOTAL_WEIGHT_FOR_SHIPPMENT__",6);
define("__TOTAL_VALUME_FOR_SHIPPMENT__",7);
define("__CHARGEABLE_WEIGHT_PER_COLI__",8);

define("__TRADE_DOMESTIC__",3);
define("__TRADE_EXPORT__",2);
define("__TRADE_IMPORT__",1);
 
define("__TRANSACTION_DEBIT_CREDIT_FLAG_1__",1); //Customer New booking
define("__TRANSACTION_DEBIT_CREDIT_FLAG_2__",2); //Automatic transfer
define("__TRANSACTION_DEBIT_CREDIT_FLAG_3__",3); //Refferal Fee
define("__TRANSACTION_DEBIT_CREDIT_FLAG_4__",4); //Starting New
define("__TRANSACTION_DEBIT_CREDIT_FLAG_5__",5); //Upload Services
define("__TRANSACTION_DEBIT_CREDIT_FLAG_6__",6); //Cancelled Booking
define("__TRANSACTION_DEBIT_CREDIT_FLAG_7__",7);
define("__TRANSACTION_DEBIT_CREDIT_FLAG_8__",8); //Courier Booking and Labels Invoice
define("__TRANSACTION_DEBIT_CREDIT_FLAG_9__",9); //Adding Invoice value Mark-up Booking
define("__TRANSACTION_DEBIT_CREDIT_FLAG_10__",10); //Substract Forwarder quote Mark-up Bookings

define("__FEDEX_API__",1);
define("__UPS_API__",2);
define("__TNT_API__",3);
define("__DHL_API__",4);

define("__COURIER_PACKAGING_TYPE_ONLY_CARTONS__",1);
define("__COURIER_PACKAGING_TYPE_ONLY_PALLETS__",2);
define("__COURIER_PACKAGING_TYPE_PALLETS_AND_CARTONS__",3);

define("__LABEL_SENT__",'2');
define("__LABEL_NOT_SENT__",'1');
define("__LABEL_DOWNLOAD__",'3');

define("__COURIER_BOOKING_RECEIVED_NOT_PAYMENT__",8);
define("__COURIER_BOOKING_RECEIVED_PAYMENT__",9);
define("__COURIER_BOOKING_LABEL_SENT_TO_SHIPPER__",10);
define("__COURIER_BOOKING_LABEL_DOWNLOADED_BY_SHIPPER__",11);
 
define("__UPLOAD_COURIER_LABEL_PDF_MANAGEMENT__",__APP_PATH_ROOT__."/management/courierLabel");
define("__UPLOAD_COURIER_LABEL_PDF__",__APP_PATH_ROOT__."/courierLabel");
define("__UPLOAD_COURIER_LABEL_PDF_FORWARDER__",__APP_PATH_ROOT__."/forwarders/courierLabel");
define("__UPLOAD_COURIER_LABEL_PDF_URL__",__BASE_STORE_URL__."/courierLabel");

define("__BOOKING_TYPE_AUTOMATIC__",'1');
define("__BOOKING_TYPE_RFQ__",'2');
define("__BOOKING_TYPE_COURIER__",'3');
define("__BOOKING_TYPE_VOGA_AUTOMATIC__",'4');

define("__COURIER_EXCLUDED_TRADE_CUSTOMER_TYPE_BUSSINESS__",'1');
define("__COURIER_EXCLUDED_TRADE_CUSTOMER_TYPE_PRIVATE__",'2'); 
define("__COURIER_EXCLUDED_TRADE_CUSTOMER_TYPE_PRIVATE_AND_BUSSINESS__",'3');

define("__PACKET_TYPE_BREAK_BULK__",'BREAK_BULK'); 
define("__PACKET_TYPE_PARCEL__",'PARCEL'); 
define("__PACKET_TYPE_PALLET__",'PALLET');
define("__PACKET_TYPE_UNKNOWN__",'UNKNOWN');
define("__PACKET_TYPE_STANDARD_CARGO__",'STANDARD_CARGO');
 
define("__PACKET_TYPE_ID_PARCEL__",1); 
define("__PACKET_TYPE_ID_PALLET__",2);  
define("__PACKET_TYPE_ID_BREAK_BULK__",3); 

define("__STANDARD_HEIGHT_EURO_PALLET__",180); 
define("__STANDARD_HEIGHT_HALF_PALLET__",125);

define("__SEARCH_TYPE_BREAK_BULK__",'1');
define("__SEARCH_TYPE_PARCELS__",'2');
define("__SEARCH_TYPE_PALLETS__",'3');
define("__SEARCH_TYPE_VOGUE__",'4');
define("__SEARCH_TYPE_VOGUE_AUTOMATIC_",'5');
define("__SEARCH_TYPE_VOGUE_AUTOMATIC_SIMPLE_",'6');

define("__PALLET_TYPE_EURO__",'Euro');
define("__PALLET_TYPE_HALF__",'Half');
define("__PALLET_TYPE_ENGLISH_PALLET__",'English_Pallet');
define("__PALLET_TYPE_OTHER__",'Other');

define("__PALLET_ID_EURO__",'1');
define("__PALLET_ID_HALF__",'2');
define("__PALLET_ID_OTHER__",'3');
define("__PALLET_ID_ENGLISH_PALLET__",'3');

define("__API_REQUEST_MODE_TEST__",'TEST');
define("__API_REQUEST_MODE_LIVE__",'LIVE');

define("__AUTOMATIC_SEARCH_TRIED_CALLING_ID__",1);
define("__RFQ_REMIND_PANE_ID__",2);

define("__AFTER_SHIP_TRACKING_STATUC_TAG_INFO_RECEIVED__",""); 
define("__AFTERSHIP_SHIPPMENT_TRACKING_LINKK__","http://track.transporteca.com");

define("__API_SERVICE_ID_BILLING__",'1111');
define("__API_SERVICE_ID_SHIPPER__",'2222');
define("__API_SERVICE_ID_CONSIGNEE__",'3333');
define("__API_SERVICE_ID_BILLING_CUSTOMER__",'4444');

define('__APP_PATH_UPDATE_BOOKING_LOG__',__APP_PATH_ROOT__.'/logs/updateBookingInfo.log');

define('__APP_PATH_DOUBLE_EMAIL_LOG__',__APP_PATH_ROOT__.'/logs/doubleEmailLog.log');


define("__FB_REDIRECT_URL__",__BASE_STORE_URL__."/fblogin/?fbTrue=true");


define("__DANISH_CURRENCY_CODE__","DKK");

define("__ENGLISH_CODE_WP__","en");
define("__DANISH_CODE_WP__","da");

define("__WORDPRESS_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__);
define("__WORDPRESS_URL_DA__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/da");

define("__SUB_WORDPRESS_URL__","/wordpress");

define("__USD_CURRENCY_ID__","1");


define("__CLOSE_BUTTON__",1);
define("__CONTINUE_BUTTON__",2);
define("__GO_TO_BUTTON__",3);
define("__GO_TO_RFQ_BUTTON__",4);
define("__CONTINUE_BUTTON_2__",5);
define("__CONTINUE_BUTTON_3__",6);

define("__CLOSE_TEXT_BUTTON__","Close");
define("__CONTINUE_TEXT_BUTTON__","Continue");
define("__GO_TO_TEXT_BUTTON__","Go To");
define("__GO_TO_RFQ_TEXT_BUTTON__","Go To RFQ");

define("__CONTINUE_TEXT_BUTTON_2__","Continue 2");
define("__CONTINUE_TEXT_BUTTON_3__","Continue 3");


define("__LCL_DTD_VOLUME_CONSTANT__","1.8");
define("__LCL_DTD_WEIGHT_CONSTANT__","810");

define("__HEIGHT_PER__PALLET_COLI__","185");


define("__ENLISH_FOOTER_MENU__","FooterMenu");
define("__DANISH_FOOTER_MENU__","FooterMenuDA");
define("__LINK_CONFIRMED_BOOKING_NEW_PER_PAGE__","10");
define("__ENGLISH_RIGHT_FOOTER_MENU__","RightFooterMenu");
define("__DANISH_RIGHT_FOOTER_MENU__","RightFooterMenuDA");

define("__DATE_LIMIT_FOR_SNOOZE__",'60');

define("__HANDLING_FEE_INVOICE_TEXT__",'Transporteca Handling Fee Invoice');
define("__HANDLING_FEE_CREDIT_NOTE_TEXT__",'Handling fee invoice Credit Note');

define("__MANUAL_FEE_COURIER__",'1');
define("__MANUAL_FEE_LCL__",'2');
define("__MANUAL_FEE_LTL__",'3');
define("__MANUAL_FEE_RAIL__",'10');
define("__AUTOMATIC_MANUAL_FEE__",'4');

define("__DKK_CURRENCY_ID__",'20');
define("__RFQ_RESPONSE_IN_SYSTEM__",'In system');
define("__RFQ_RESPONSE_BY_EMAIL__",'By e-mail');

define("__QUICK_QUOTE_CONVERT_RFQ__",'1');
define("__QUICK_QUOTE_SEND_QUOTE__",'2');
define("__QUICK_QUOTE_MAKE_BOOKING__",'3');
define("__QUICK_QUOTE_CONFIRMED_BOOKING__",'4');
define("__QUICK_QUOTE_DRAFT_QUOTE__",'5');

define("__QUICK_QUOTE_DEFAULT_PARTNER_ID__",'99999');

define("__S1_DRAFT_QUOTE_DESCRIPTION__",'Draft quick quote');


define("__COOKIE_DANISH_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/da/cookie-politik/");

define("__LANGUAGE_ID_SWEDISH_CODE__","GE");

define("__LABEL_NOT_SEND_FLAG__","1");
define("__LABEL_SEND_FLAG__","2");
define("__LABEL_DOWNLOAD_FLAG__","3");
define("__SERVICE_CHARGE_STRIPE__","1.8");
 
define("__MINIMUM_GOOD_VALUE_IN_DKK__","1150");
define("__SHOW_MIN_HANDLING_FEE_DATE__","22-08-2016");

define("__UPLOAD_STRIPE_PAYMENT_CSV_MANAGEMENT__",__APP_PATH_ROOT__."/management/outstandingPayment");
define("__STRIPE_PAYMENT_CSV_COLUMN_COUNT__","10");

define("__BOOKING_FILE_VISITED_LIMIT__","10");

define("__FILE_RECENT_MODIFIED__","1");
define("__FILE_RECENT_VISITED__","2");

define("__TIME_FOR_EXPIRY_TOKEN__","24 HOUR");
 
//define("__GOOGLE_MAP_V3_API_KEY__",'AIzaSyC-unuKYaoz990OFUpxHgoSMjwPYjQ_mlo');
 /*
if(!function_exists('__autoload'))
{
    require_once( __APP_PATH__ . "/inc/preload.php" );
}
*/
//key to encrypt/decrypt
define("ENCRYPT_KEY","trasnport01!#12");

$replaceAry = array('http://','https://');

$replaceVogaPagesAry = array('da'=>'Voga','en'=>'Voga-Furniture','se'=>'Voga-Sverige','pl'=>'Voga-Polski','nn'=>'Voga-Norge','de'=>'Voga-Moebel');
$managementHost = str_replace($replaceAry,"",__BASE_MANAGEMENT_URL__);
if(isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!=$managementHost)
{
   // require_once( __APP_PATH__ . "/inc/preload.php" );
}

define("__VOLUME_CONVERT_INTO_WEIGHT_AIR__","167");
define("__VOLUME_CONVERT_INTO_WEIGHT_COURIER__","250");
define("__SHOWING_BOOKING_DOWNLOAD_DATE__","2016-09-22");

//define("__FORWARDER_PAYMENT_FREQUENCY_WEEKLY__","1");
define("__FORWARDER_PAYMENT_FREQUENCY_MONTHLY__","2");
define("__FORWARDER_PAYMENT_FREQUENCY_BY_MONTHLY__","1");

//define("__FORWARDER_PAYMENT_FREQUENCY_WEEKLY_TEXT__","Weekly");
define("__FORWARDER_PAYMENT_FREQUENCY_MONTHLY_TEXT__","Monthly");
define("__FORWARDER_PAYMENT_FREQUENCY_BY_MONTHLY_TEXT__","Every two months");

define("__MAX_ALLOWED_FORWARDER_LOGO_EMAIL_WIDTH__","217");
define("__MAX_ALLOWED_FORWARDER_LOGO_EMAIL_HEIGHT__","53");

define("__CUSTOMER_LOG_SNIPPET_TYPE_EMAIL__","1");
define("__CUSTOMER_LOG_SNIPPET_TYPE_REMINDER_NOTES__","2");
define("__CUSTOMER_LOG_SNIPPET_TYPE_OVERVIEW_TASK__","3");
define("__CUSTOMER_LOG_SNIPPET_TYPE_FILE_SEARCH_LOGS__","4");
define("__CUSTOMER_LOG_SNIPPET_TYPE_ZOPIM_CHAT_LOGS__","5");

define("__CUSTOMER_LOG_EMAIL_TYPE_OUTGOING__","1");
define("__CUSTOMER_LOG_EMAIL_TYPE_INCOMING__","2");

define("__TRANSPORTECA_FINANCIAL_VERSION__","2");

define("__TRANSPORTECA_FINANCIAL_TAGS_AUTOMATIC_TRANSFER__","Automatic transfer");
define("__TRANSPORTECA_FINANCIAL_TAGS_REFERAL_FEE__","Transporteca referral fee");

define("__USER_DID_NOT_GET_ANY_ONLINE_SERVICES__","User did not get any online prices for the request");

define("__TNT_ROUTING_LABEL_API_END_POINT__","https://express.tnt.com/expresslabel/documentation/getlabel");
define("__TNT_SHIPPING_API_END_POINT__","https://iconnection.tnt.com/ShipperGate2.asp");

define("__PARTNER_API_CUSTOMER_TYPE_BUSINESS__","BUSINESS");
define("__PARTNER_API_CUSTOMER_TYPE_PRIVATE__","PRIVATE");

define("__CHECK_DATE_FOR_FINANCIAL_BOOKING__","2016-02-21");
define("__VOGA_LANDING_PAGE__","16");

define("__WAREHOUSE_TYPE_CFS__","1");
define("__WAREHOUSE_TYPE_AIR__","2");

define("__REMINDER_TEXT_SLOVE_TASK__","Collection is overdue. Check with courier company if bookings has not been collected as scheduled, or if tracking not updated correctly by Aftership. Holding message has been sent to customer");
define("__CREDIT_NOTE_REASON_HEADING__","The reason stated for this cancellation is:");
define("__CANCEL_BOOKING_TEXT_SLOVE_TASK__","Cancel booking with szDisplayName, if already booked.");
define("__AFTER_SHIP_TEXT_SLOVE_TASK__","When trying to add the tracking number to Aftership, we received this error: szErrorText");

define('__SLOVE_TASK_FOR_STRIPE_PAYMENT__',"Booking has been cancelled, refund payment in Stripe, reference: szStripeReference");
define('__SLOVE_TASK_FOR_PAYPAL_PAYMENT__',"Booking has been cancelled, refund payment in PayPal");
define('__SLOVE_TASK_FOR_BANK_TRANSFER__',"Booking has been cancelled, refund payment by bank transfer");

define("__DISTANCE_MODEL_LONG_NAME_CFS__","Pricing based on distance from the CFS");
define("__DISTANCE_MODEL_LONG_NAME_AIR__","Pricing based on distance from the airport warehouse");

define('__AIR_ICON__','1');
define('__SEA_ICON__','2');
define('__TRUCK_ICON__','3');
define('__RAIL_ICON__','4');

define("__SERVICE_TYPE_FOR_AIR_SERVICES__","1");
define("__LOGO_CHANGE_DATE__","2017-05-22 23:59:59");

define("__API_REQUEST_PER_PAGE__","10");

if(isset($iDonotIncludePrelod) && $iDonotIncludePrelod==1)
{
    //echo "Do not include preload...";
}
else
{
    require_once( __APP_PATH__ . "/inc/preload.php" );
}

?>
