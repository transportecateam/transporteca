<?php
if(!isset($_SESSION))
{
    session_start();
} 
/**
 * This file contains the functions that are generic for the store and can be used by all the php files in the store website.
 *
 * functiions.php
 * @copyright Copyright (C) 2012 Trnasporteca, LLC
 * @author Anil
 * @package Trnasporteca Development
 */
if( !defined( "__APP_PATH__" ) )    
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once(__APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/crm_functions.php"); 
require_once(__APP_PATH__ . "/inc/booking_functions.php");
require_once(__APP_PATH__ . "/inc/search_functions.php");
require_once(__APP_PATH__ . "/inc/pdf_functions.php");
require_once(__APP_PATH__ . "/inc/insurance_functions.php");
require_once( __APP_PATH__ . "/inc/logs_functions.php" ); 


require_once( __APP_PATH_CLASSES__ . "/database.class.php");
require_once( __APP_PATH_CLASSES__ . "/booking.class.php");
require_once( __APP_PATH_CLASSES__ . "/sendEmail.class.php");

function checkAuth($redirect_url="")
{
    $redirect_url = (trim($redirect_url) == "") ? "" : "?redirect_url=".trim($redirect_url);
    if (!isset($_SESSION['user_id']) || ((int)$_SESSION['user_id'] <= 0))
    {
      header('Location: '.__BASE_URL__.$redirect_url);
      die();
    } 
    
    $kUser = new cUser();
    $kUser->getUserDetails((int)$_SESSION['user_id']);
    if ($kUser->error === true)
    {
      header('Location: '.__BASE_URL__.$redirect_url);
      die();
    }
}

function checkAuthAJAX($kUser)
{
  if (!isset($_SESSION['user_id']) || ((int)$_SESSION['user_id'] <= 0))
  {
	return false;
  }

  if(!is_object($kUser)) {
    $kUser = new CUser();
  }
  
  $kUser->load((int)$_SESSION['user_id']);
  if ($kUser->error === true)
  {
    header('Location: '.__USER_LOGIN_URL__);
    return false;
  } 
  return true;
}

function createEmail($email_template, $replace_ary, $to, $subject, $reply_to, $idUser=0, $from=__STORE_SUPPORT_EMAIL__,$flag = __FLAG_FOR_DEFAULT__,$attachment=false,$idBooking=0,$attacfilename='',$cc=false,$booking_logs=false,$szFromUserName=false,$iQuoteEmail=false,$attacFriendlyfilename='')
{
    $buildTemplateAry = array();
    $buildTemplateAry = $replace_ary;
    $buildTemplateAry['szEmailTemplate'] = $email_template;
    
    $emailTemplateAry = array(); 
    $kSendEmail = new cSendEmail();
    $emailTemplateAry = $kSendEmail->createEmailTemplate($buildTemplateAry);
    if(!empty($emailTemplateAry))
    {
        /*
        *  Adding Email header to E-mail message
        */
        ob_start();  
        include(__APP_PATH_ROOT__.'/layout/email_header.php');
        $message = ob_get_clean();
        
        /*
        *  Adding Email body to E-mail message
        */
        $message .= $emailTemplateAry['szEmailBody'];

        $subject = $emailTemplateAry['szEmailSubject'];
        /*
        *  Adding Email footer to E-mail message
        */
        /*$message .= file_get_contents(__APP_PATH__.'/templates/email-footer.php');*/
        ob_start();
        include(__APP_PATH_ROOT__.'/layout/email_footer.php');
        $message .= ob_get_clean();
        
        $iIgnoreTracking=0;
        if((int)$replace_ary['iIgnoreTracking']>0)
        {
            $iIgnoreTracking = $replace_ary['iIgnoreTracking'];
        }

        if(!empty($replace_ary['szBookingRef']))
        {
            $szBookingRef = $replace_ary['szBookingRef'] ; 
            if($replace_ary['iAdminFlag']==1)
            {
                $idBooking = $replace_ary['idBooking'];
            }
            else
            {
                $idBooking = $_SESSION['booking_id'];
            }
        }
        if (count($replace_ary) > 0)
        {
            foreach ($replace_ary as $replace_key => $replace_value)
            {
                $message = str_replace($replace_key, $replace_value, $message);
                $subject= str_replace($replace_key, $replace_value, $subject);
            }
        }

        if($_SESSION['user_id']>0)
        {
            if((!empty($specialTemplateAry) && in_array($email_template,$specialTemplateAry)) || ($iUserType==1))
            {
                $kRegisterShipCon = new cRegisterShipCon(); 
                $szCustomerHistoryNotes = "System e-mail on ".date('d/m/Y H:i:s')."\n".$subject; 
                $kRegisterShipCon->updateCapsuleHistoryNotes($szCustomerHistoryNotes,$_SESSION['user_id']);
            }
        } 

        if(empty($from))
        { 
            $from = __STORE_SUPPORT_EMAIL__ ;
        } 
        if(empty($szFromUserName))
        {
            $szFromUserName = __STORE_SUPPORT_NAME__;
        }

        $iBookingLanguage = getLanguageId();
        if($replace_ary['iBookingLanguage']>0)
        {
            $iBookingLanguage = $replace_ary['iBookingLanguage'] ;
        } 
        if($replace_ary['iLanguage']>0)
        {
            $iBookingLanguage = $replace_ary['iLanguage'] ;
        }

        /*
        * building block for send Transporteca E-mail
        */
        $mailBasicDetailsAry = array();
        $mailBasicDetailsAry['szEmailTo'] = $to;
        $mailBasicDetailsAry['szEmailFrom'] = $from;
        $mailBasicDetailsAry['szEmailCC'] = $cc;
        $mailBasicDetailsAry['szEmailSubject'] = $subject;
        $mailBasicDetailsAry['szEmailMessage'] = $message;
        $mailBasicDetailsAry['szReplyTo'] = $reply_to;
        $mailBasicDetailsAry['idUser'] = $idUser;
        $mailBasicDetailsAry['szBookingRef'] = $szBookingRef;
        $mailBasicDetailsAry['idBooking'] = $idBooking;
        $mailBasicDetailsAry['szUserLevel'] = $flag; 
        $mailBasicDetailsAry['bBookingLogs'] = $booking_logs;
        $mailBasicDetailsAry['szFromUserName'] = $szFromUserName;
        $mailBasicDetailsAry['bAttachments'] = $attachment;
        $mailBasicDetailsAry['szAttachmentFiles'] = $attacfilename; 
        $mailBasicDetailsAry['szAttachmentFileName'] = $attacFriendlyfilename; 
        $mailBasicDetailsAry['iQuoteEmail'] = $iQuoteEmail;  
        $mailBasicDetailsAry['iBookingLanguage'] = $iBookingLanguage;  
        //print_r($mailBasicDetailsAry);
        $kSendEmail->sendEmail($mailBasicDetailsAry);
    } 
}

function createEmail_backup($email_template, $replace_ary, $to, $subject, $reply_to, $idUser=0, $from=__STORE_SUPPORT_EMAIL__,$flag = __FLAG_FOR_DEFAULT__,$attachment=false,$idBooking=0,$attacfilename='',$cc=false,$booking_logs=false,$szFromUserName=false,$iQuoteEmail=false)
{ 
    $email_template = trim($email_template);
    $kDatabase = new cDatabase();
    $kDatabase->connect( __DBC_USER__, __DBC_PASSWD__, __DBC_SCHEMATA__, __DBC_HOST__,"normal",'bool',true);
 
    $specialTemplateAry = array('__CUSTOMER_CANCEL_BOOKING_EMAIL__','__CUSTOMER_CANCEL_NOT_CONFIRMED_BOOKING_EMAIL__','__BOOKING_CONFIRMATION_EMAIL__','__CUSTOMER_BOOKING_ACKNOWLEDGEMENT_EMAIL__','__UPDATE_CONTACT_INFORMATION__','__USER_INVITATION__','__INVITATION_TO_CONNECT_ON_TRANSPORTECA__','__CREATE_USER_ACCOUNT__','__SERVICE_NOTIFICATION_EMAIL__', '__RATE_FORWARDER__','__SHARE_THIS_SITE__','__BOOKING_BY_BANK_TRANSFER_CONFIRMATION_EMAIL__','__BANK_TRANSFER_RECEIVED_CONFIRMATION_EMAIL__');
   	 
    $iLanguage = getLanguageId();
    
    if($replace_ary['iBookingLanguage']>0)
    {
        $iLanguage = $replace_ary['iBookingLanguage'] ;
    }
    
    $query = "
        SELECT
            cmm.section_description, 
            cmm.subject,
            ce.iUserType
        FROM
            ".__DBC_SCHEMATA_CMS_EMAIL__." AS ce
        INNER JOIN
            ".__DBC_SCHEMATA_CMS_MAPPING__." AS cmm
        ON        
            cmm.idEmailTemplate=ce.id        
        WHERE
           ce.section_title = '".mysql_escape_custom($email_template)."'
        AND
            cmm.idLanguage='".(int)$iLanguage."'                
    "; 
    $iUserType = 0;
    ob_start();  
    include(__APP_PATH_ROOT__.'/layout/email_header.php');
    $message = ob_get_clean();
    //$message = file_get_contents(__BASE_URL_SECURE__.'/templates/email-header.php');
    if ($result = $kDatabase->exeSQL($query))
    {
        if ($kDatabase->iNumRows > 0)
        {
            $row = $kDatabase->getAssoc($result);
            $iUserType = $row['iUserType'] ;
            
            $iLanguage = getLanguageId();	 
            
            if($replace_ary['iBookingLanguage']>0)
            {
            	$iBookingLanguage = $replace_ary['iBookingLanguage'] ;
            }
            
            $iIgnoreTracking=0;
            if((int)$replace_ary['iIgnoreTracking']>0)
            {
                $iIgnoreTracking=$replace_ary['iIgnoreTracking'];
            }
            
            /*if(($iLanguage==__LANGUAGE_ID_DANISH__ && $row['iUserType']==1) || ($iBookingLanguage==__LANGUAGE_ID_DANISH__))
            { */
                if(mb_detect_encoding($row['section_description'], "UTF-8", true) == "UTF-8")
                {
                    $message .= utf8_decode($row['section_description']); 
                }
                else
                {
                    $message .= $row['section_description']; 
                }
                if(!empty($row['subject']))
                {
                    if(mb_detect_encoding($row['subject'], "UTF-8", true) == "UTF-8")
                    {
                        $subject = utf8_decode($row['subject']);
                    }
                    else
                    {
                        $subject = $row['subject'];
                    }
                }
            /*}
            else
            { 
                $message .= $row['section_description']; 
                if(!empty($row['subject']))
                {
                    $subject = $row['subject'];
                }
            }*/ 
        }
    }
    else
    {
        $kDatabase->error = true;
        $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
        $kDatabase->logError( "input", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
        return false;
    } 
    if(!empty($replace_ary['szBookingRef']))
    {
        $szBookingRef = $replace_ary['szBookingRef'] ; 
        if($replace_ary['iAdminFlag']==1)
        {
            $idBooking = $replace_ary['idBooking'];
        }
        else
        {
            $idBooking = $_SESSION['booking_id'];
        }
    }
    if (count($replace_ary) > 0)
    {
        foreach ($replace_ary as $replace_key => $replace_value)
        {
            $message = str_replace($replace_key, $replace_value, $message);
            $subject= str_replace($replace_key, $replace_value, $subject);
        }
    }
    
    if($_SESSION['user_id']>0)
    {
        if((!empty($specialTemplateAry) && in_array($email_template,$specialTemplateAry)) || ($iUserType==1))
        {
            $kRegisterShipCon = new cRegisterShipCon(); 
            $szCustomerHistoryNotes = "System e-mail on ".date('d/m/Y H:i:s')."\n".$subject; 
            $kRegisterShipCon->updateCapsuleHistoryNotes($szCustomerHistoryNotes,$_SESSION['user_id']);
        }
    }
    
    /*$message .= file_get_contents(__APP_PATH__.'/templates/email-footer.php');*/
    ob_start();
    include(__APP_PATH_ROOT__.'/layout/email_footer.php');
    $message .= ob_get_clean();
    
    if(empty($from))
    { 
        $from = __STORE_SUPPORT_EMAIL__ ;
    } 
    if(empty($szFromUserName))
    {
        $szFromUserName = __STORE_SUPPORT_NAME__;
    }

    $kSendEmail = new cSendEmail();      
    $mailBasicDetailsAry = array();
    $mailBasicDetailsAry['szEmailTo'] = $to;
    $mailBasicDetailsAry['szEmailFrom'] = $from;
    $mailBasicDetailsAry['szEmailCC'] = $cc;
    $mailBasicDetailsAry['szEmailSubject'] = $subject;
    $mailBasicDetailsAry['szEmailMessage'] = $message;
    $mailBasicDetailsAry['szReplyTo'] = $reply_to;
    $mailBasicDetailsAry['idUser'] = $idUser;
    $mailBasicDetailsAry['szBookingRef'] = $szBookingRef;
    $mailBasicDetailsAry['idBooking'] = $idBooking;
    $mailBasicDetailsAry['szUserLevel'] = $flag; 
    $mailBasicDetailsAry['bBookingLogs'] = $booking_logs;
    $mailBasicDetailsAry['szFromUserName'] = $szFromUserName;
    $mailBasicDetailsAry['bAttachments'] = $attachment;
    $mailBasicDetailsAry['szAttachmentFiles'] = $attacfilename; 
    $mailBasicDetailsAry['iQuoteEmail'] = $iQuoteEmail;  
    $kSendEmail->sendEmail($mailBasicDetailsAry);
    
    /*
     * Following function is no longer in use but have kept this for backup purpose @Ajay
    if ((__ENVIRONMENT__ == "ONLINE") || (__ENVIRONMENT__ == "LIVE"))
    {  
        sendEmail($to,$from,$subject,$message,$reply_to, $idUser,$attachment,$szBookingRef,$idBooking,$attacfilename,$flag,$cc,$booking_logs,$szFromUserName,$iQuoteEmail); 
    }
    else if(__ENVIRONMENT__ == "DEV_LIVE")
    {  
       sendEmail($to,$from,$subject,$message,$reply_to, $idUser,$attachment,$szBookingRef,$idBooking,$attacfilename,$flag,$cc,$booking_logs,$szFromUserName,$iQuoteEmail);
    }
    else
    {
//        echo "<br><br><br> To: ".$to;                                         
//        echo "<br> Subject: ".$subject;
//        echo "<br><br> Email Message: <br><br> ".$message;  
//        die();
      
        //For my local end we only we logs data in tblemaillog and donot send any email
        $emailLogsAry = array();
        $emailLogsAry['szFromEmail'] = $from ;
        $emailLogsAry['szEmailBody'] = $message ;
        $emailLogsAry['szEmailSubject'] = $subject ;
        $emailLogsAry['idUser'] = $idUser ;
        $emailLogsAry['idBooking'] = $idBooking ;
        $emailLogsAry['iQuoteEmail'] = $iQuoteEmail ;
        
        if(!empty($to) && is_array($to))
        {
           $emailLogsAry['szToAddress'] = implode(",",$to) ;
        }
        else
        { 
            $emailLogsAry['szToAddress'] = $to ;
        }   
        $idEmailLog = logEmails($emailLogsAry);
        return true;

        sendEmail($to,$from,$subject,$message,$reply_to, $idUser,$attachment,$szBookingRef,$idBooking,$attacfilename,$flag,$cc,$booking_logs,$szFromUserName,$iQuoteEmail);
        
    }
     * 
     */
} 

function sendQuatationEmail($to,$from,$subject,$message,$reply_to,$idUser,$idBooking,$szFromUserName,$iQuoteEmail=false,$attachment=false,$attacfilename=false)
{
    ob_start();
    include(__APP_PATH_ROOT__.'/layout/email_footer.php');
    $message .= ob_get_clean();
    
    if(empty($from))
    { 
        $from = __STORE_SUPPORT_EMAIL__ ;
    } 
    if(empty($szFromUserName))
    {
        $szFromUserName = __STORE_SUPPORT_NAME__;
    }

    $kSendEmail = new cSendEmail();      
    $mailBasicDetailsAry = array();
    $mailBasicDetailsAry['szEmailTo'] = $to;
    $mailBasicDetailsAry['szEmailFrom'] = $from;
    $mailBasicDetailsAry['szEmailCC'] = $cc;
    $mailBasicDetailsAry['szEmailSubject'] = $subject;
    $mailBasicDetailsAry['szEmailMessage'] = $message;
    $mailBasicDetailsAry['szReplyTo'] = $reply_to;
    $mailBasicDetailsAry['idUser'] = $idUser;
    $mailBasicDetailsAry['szBookingRef'] = $szBookingRef;
    $mailBasicDetailsAry['idBooking'] = $idBooking;
    $mailBasicDetailsAry['szUserLevel'] = $flag; 
    $mailBasicDetailsAry['bBookingLogs'] = $booking_logs;
    $mailBasicDetailsAry['szFromUserName'] = $szFromUserName;
    $mailBasicDetailsAry['bAttachments'] = $attachment;
    $mailBasicDetailsAry['szAttachmentFiles'] = $attacfilename; 
    $mailBasicDetailsAry['iQuoteEmail'] = $iQuoteEmail;  
    $mailBasicDetailsAry['bAttachments'] = $attachment;
    $mailBasicDetailsAry['szAttachmentFiles'] = $attacfilename; 
    $kSendEmail->sendEmail($mailBasicDetailsAry);
        
    /*
     * Following function is no longer in use but have kept this for backup purpose @Ajay
    if ((__ENVIRONMENT__ == "ONLINE") || (__ENVIRONMENT__ == "LIVE") || (__ENVIRONMENT__ == "DEV_LIVE"))
    {   
        sendEmail($to,$from,$subject,$message,$reply_to, $idUser,$attachment,$szBookingRef,$idBooking,$attacfilename,$flag,$cc,true,$szFromUserName,$iQuoteEmail);
    } 
    else
    { 
        return true;
        sendEmail($to,$from,$subject,$message,$reply_to, $idUser,$attachment,$szBookingRef,$idBooking,$attacfilename,$flag,$cc,true,$szFromUserName);    
    }
    * 
    */
}
function sendEmail($to,$from,$subject,$message,$reply_to, $idUser,$attachment=false,$szBookingRef=false,$idBooking=false,$attacfilename='',$flag=__FLAG_FOR_DEFAULT__,$cc=false,$booking_logs=false,$szFromUserName=false,$iQuoteEmail=false)
{ 
    require_once(__APP_PATH__.'/inc/SendGrid_loader.php');	
                  
    if(empty($from))
    { 
        $from = __STORE_SUPPORT_EMAIL__ ;
    }
        
    $sendingEmailLogging = 'Send Email Request Received for '.PHP_EOL;
    if(is_array($to))
    {
        $sendingEmailLogging = "To: ".print_R($to,true).PHP_EOL;
    }
    else
    {
        $sendingEmailLogging = "To: ".$to.PHP_EOL;
    }
    $sendingEmailLogging .= "From: ".$from.PHP_EOL;
    $sendingEmailLogging .= "Subject: ".$subject.PHP_EOL;
    
    if(!empty($attacfilename))
    {
        if(is_array($attacfilename))
        {
            $sendingEmailLogging .= "Attachment: ".print_R($attacfilename,true).PHP_EOL;
        }
        else
        {
            $sendingEmailLogging .= "Attachment: ".$attacfilename.PHP_EOL;
        }
    }
    else
    {
        $sendingEmailLogging .= "Attachment: NO".PHP_EOL;
    } 
    
    try
    {
        $sendgrid = new SendGrid(__SEND_GRID_USERNAME__,__SEND_GRID_PASSWARD__);
        $smtp = $sendgrid->smtp;

        $sendingEmailLogging .= PHP_EOL." Send grid object created successfully ".PHP_EOL;
        
        if(__ENVIRONMENT__ == "DEV_LIVE")
        {
            $subject = " TEST SITE: ".$subject;
        }
        $szEmailKey = md5($to."_".time()."_".mt_rand(0,9999));

        $emailLogsAry = array();
        $emailLogsAry['szFromEmail'] = $from;
        $emailLogsAry['szEmailBody'] = $message ;
        $emailLogsAry['szEmailSubject'] = $subject ;
        $emailLogsAry['idUser'] = $idUser ;
        $emailLogsAry['idBooking'] = $idBooking ;
        $emailLogsAry['szEmailKey'] = $szEmailKey ;
        $emailLogsAry['iQuoteEmail'] = $iQuoteEmail ; 

        if(empty($szFromUserName))
        { 
            $szFromUserName = __STORE_SUPPORT_NAME__ ;
        }
        //logEmails($idUser,$message,$subject,$emails,$success,$idBooking);
        if($attachment)
        {  
            if((int)$idBooking>0 && $attacfilename=='')
            {  
                require_once( __APP_PATH_CLASSES__ . "/booking.class.php");

                $kBooking = new cBooking();

                $postSearchAry = $kBooking->getBookingDetails($idBooking);

                // a random hash will be necessary to send mixed content
                $separator = md5(time());
                $iSendInsuranceInvoice = 0;
                if($postSearchAry['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_3__ && $postSearchAry['iTransferConfirmed']==1)
                {
                    $invoice_filename = "bookingInvoice_".$szBookingRef.".pdf";    
                    if($postSearchAry['iFinancialVersion']==2)
                    {
                        $invoice_pdf_file = getInvoiceConfirmationPdfFileHTML_v2($idBooking,false,true);
                    }else{
                        $invoice_pdf_file = getInvoiceConfirmationPdfFileHTML($idBooking,false,true);
                    }
                    if($postSearchAry['iInsuranceIncluded']==1)
                    {
                        $insurance_invoice_filename = "bookingInsuranceInvoice_".$szBookingRef.".pdf";    
                        $insurance_invoice_filename = getBookingInsuranceInvoicePdf($idBooking,false,true);
                        $iSendInsuranceInvoice=1;
                    }
                }
                else if($postSearchAry['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_3__)
                {
                    $invoice_filename = "bookingConfirmation_".$szBookingRef.".pdf"; 
                    $invoice_pdf_file = getBookingConfirmationPdfFileHTML($idBooking,false,true) ;
                }
                else
                {
                    $invoice_filename = "bookingInvoice_".$szBookingRef.".pdf";    
                    if($postSearchAry['iFinancialVersion']==2)
                    {
                        $invoice_pdf_file = getInvoiceConfirmationPdfFileHTML_v2($idBooking,false,true);
                    }else{
                        $invoice_pdf_file = getInvoiceConfirmationPdfFileHTML($idBooking,false,true); 
                    }    
                    $confirmation_filename = "bookingConfirmation_".$szBookingRef.".pdf";    
                    $confirmation_pdf_file = getBookingConfirmationPdfFileHTML($idBooking,false,true) ;

                    if($postSearchAry['iInsuranceIncluded']==1)
                    {
                        $insurance_invoice_filename = "bookingInsuranceInvoice_".$szBookingRef.".pdf";    
                        $insurance_invoice_filename = getBookingInsuranceInvoicePdf($idBooking,false,true);
                        $iSendInsuranceInvoice=1;
                    }
                }  

                if(!empty($to) && is_array($to))
                {
                    foreach($to as $emails)
                    {
                        if(!empty($emails))
                        {
                            if($iSendInsuranceInvoice==1)
                            {
                                if($postSearchAry['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_3__)
                                {
                                    try
                                    {
                                        $emailLogsAry['szToAddress'] = $emails ;
                                        $idEmailLog = logEmails($emailLogsAry);

                                        $message1 = new SendGrid\Mail();
                                          $message1->
                                          setFrom($from)->
                                          setFromName(__STORE_SUPPORT_NAME__)->
                                          setSubject($subject)->
                                              setHtml($message)->
                                          addTo($emails)->addAttachment($invoice_pdf_file)->
                                              addAttachment($insurance_invoice_filename)->addUniqueArgument('email_key',$szEmailKey);
                                          $smtp->send($message1); 
                                    }
                                    catch(Exception $e) 
                                    {
                                        $szExceptionMessage = " System registered exception in sending this email. \n\n ";
                                        $szExceptionMessage .= 'Exception Message: ' .$e->getMessage();  
                                    }
                                    $updateEmailLog = array();
                                    $updateEmailLog['iSuccess'] = 1;
                                    $updateEmailLog['idEmailLog'] = $idEmailLog;
                                    if(!empty($szExceptionMessage))
                                    {
                                        $updateEmailLog['szSendgridResponseLogs'] = $szExceptionMessage;
                                    }
                                    else
                                    {
                                        $updateEmailLog['szSendgridResponseLogs'] = print_R($message1,true); 
                                    }
                                    updateEmailLogs($updateEmailLog);
                                }
                                else
                                {
                                    try
                                    {
                                        $emailLogsAry['szToAddress'] = $emails ;
                                        $idEmailLog = logEmails($emailLogsAry);

                                        $message1 = new SendGrid\Mail();
                                        $message1->
                                          setFrom($from)->
                                          setFromName(__STORE_SUPPORT_NAME__)->
                                          setSubject($subject)->
                                              setHtml($message)->
                                          addTo($emails)->
                                              addAttachment($confirmation_pdf_file)->
                                              addAttachment($invoice_pdf_file)->
                                              addAttachment($insurance_invoice_filename)->addUniqueArgument('email_key',$szEmailKey);
                                          $smtp->send($message1); 
                                    }
                                    catch(Exception $e) 
                                    {
                                        $szExceptionMessage = " System registered exception in sending this email. \n\n ";
                                        $szExceptionMessage .= 'Exception Message: ' .$e->getMessage();  
                                    }
                                    $updateEmailLog = array();
                                    $updateEmailLog['iSuccess'] = 1;
                                    $updateEmailLog['idEmailLog'] = $idEmailLog;
                                    if(!empty($szExceptionMessage))
                                    {
                                            $updateEmailLog['szSendgridResponseLogs'] = $szExceptionMessage;
                                    }
                                    else
                                    {
                                            $updateEmailLog['szSendgridResponseLogs'] = print_R($message1,true); 
                                    }
                                    updateEmailLogs($updateEmailLog);
                                }
                            }
                            else
                            {
                                if($postSearchAry['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_3__)
                                {
                                    try
                                    {
                                        $emailLogsAry['szToAddress'] = $emails ;
                                        $idEmailLog = logEmails($emailLogsAry);

                                        $message1 = new SendGrid\Mail();
                                        $message1->
                                        setFrom($from)->
                                        setFromName(__STORE_SUPPORT_NAME__)->
                                        setSubject($subject)->
                                            setHtml($message)->
                                        addTo($emails)->addAttachment($invoice_pdf_file)->addUniqueArgument('email_key',$szEmailKey);
                                        $smtp->send($message1);  
                                    }
                                    catch(Exception $e) 
                                    {
                                        $szExceptionMessage = " System registered exception in sending this email. \n\n ";
                                        $szExceptionMessage .= 'Exception Message: ' .$e->getMessage();  
                                    }
                                    $updateEmailLog = array();
                                    $updateEmailLog['iSuccess'] = 1;
                                    $updateEmailLog['idEmailLog'] = $idEmailLog;
                                    if(!empty($szExceptionMessage))
                                    {
                                        $updateEmailLog['szSendgridResponseLogs'] = $szExceptionMessage;
                                    }
                                    else
                                    {
                                        $updateEmailLog['szSendgridResponseLogs'] = print_R($message1,true); 
                                    }
                                    updateEmailLogs($updateEmailLog);
                                }
                                else
                                {
                                    try
                                    {
                                        $emailLogsAry['szToAddress'] = $emails ;
                                        $idEmailLog = logEmails($emailLogsAry);
                                        $message1 = new SendGrid\Mail();
                                        $message1->
                                          setFrom($from)->
                                          setFromName(__STORE_SUPPORT_NAME__)->
                                          setSubject($subject)->
                                              setHtml($message)->
                                          addTo($emails)->
                                              addAttachment($confirmation_pdf_file)->
                                              addAttachment($invoice_pdf_file)->addUniqueArgument('email_key',$szEmailKey);
                                          $smtp->send($message1); 
                                    }
                                    catch(Exception $e) 
                                    {
                                        $szExceptionMessage = " System registered exception in sending this email. \n\n ";
                                        $szExceptionMessage .= 'Exception Message: ' .$e->getMessage();  
                                    }
                                    $updateEmailLog = array();
                                    $updateEmailLog['iSuccess'] = 1;
                                    $updateEmailLog['idEmailLog'] = $idEmailLog;
                                    if(!empty($szExceptionMessage))
                                    {
                                            $updateEmailLog['szSendgridResponseLogs'] = $szExceptionMessage;
                                    }
                                    else
                                    {
                                            $updateEmailLog['szSendgridResponseLogs'] = print_R($message1,true); 
                                    }
                                    updateEmailLogs($updateEmailLog);
                                }
                            }
                        }
                    }
                }
                else
                {
                    if(!empty($to))
                    {
                        if($iSendInsuranceInvoice==1)
                        {
                            if($postSearchAry['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_3__)
                            {
                                try
                                {
                                    $emailLogsAry['szToAddress'] = $to ;
                                    $idEmailLog = logEmails($emailLogsAry); 

                                    $message1 = new SendGrid\Mail();
                                    $message1->
                                    setFrom($from)->
                                    setFromName(__STORE_SUPPORT_NAME__)->
                                    setSubject($subject)->
                                        setHtml($message)->
                                    addTo($to)-> 
                                        addAttachment($invoice_pdf_file)->
                                        addAttachment($insurance_invoice_filename)->addUniqueArgument('email_key',$szEmailKey);
                                    $smtp->send($message1); 
                                }
                                catch(Exception $e) 
                                {
                                    $szExceptionMessage = " System registered exception in sending this email. \n\n ";
                                    $szExceptionMessage .= 'Exception Message: ' .$e->getMessage();  
                                }

                                $updateEmailLog = array();
                                $updateEmailLog['iSuccess'] = 1;
                                $updateEmailLog['idEmailLog'] = $idEmailLog;
                                if(!empty($szExceptionMessage))
                                {
                                        $updateEmailLog['szSendgridResponseLogs'] = $szExceptionMessage;
                                }
                                else
                                {
                                        $updateEmailLog['szSendgridResponseLogs'] = print_R($message1,true); 
                                }
                                updateEmailLogs($updateEmailLog);
                            }
                            else
                            {
                                try
                                {
                                    $emailLogsAry['szToAddress'] = $to ;
                                    $idEmailLog = logEmails($emailLogsAry);

                                    $message1 = new SendGrid\Mail();
                                    $message1->
                                    setFrom($from)->
                                    setFromName(__STORE_SUPPORT_NAME__)->
                                    setSubject($subject)->
                                        setHtml($message)->
                                    addTo($to)->
                                        addAttachment($confirmation_pdf_file)->
                                        addAttachment($invoice_pdf_file)->
                                        addAttachment($insurance_invoice_filename)->addUniqueArgument('email_key',$szEmailKey);
                                    $smtp->send($message1); 
                                }
                                catch(Exception $e) 
                                {
                                    $szExceptionMessage = " System registered exception in sending this email. \n\n ";
                                    $szExceptionMessage .= 'Exception Message: ' .$e->getMessage();  
                                }
                                $updateEmailLog = array();
                                $updateEmailLog['iSuccess'] = 1;
                                $updateEmailLog['idEmailLog'] = $idEmailLog;
                                if(!empty($szExceptionMessage))
                                {
                                    $updateEmailLog['szSendgridResponseLogs'] = $szExceptionMessage;
                                }
                                else
                                {
                                    $updateEmailLog['szSendgridResponseLogs'] = print_R($message1,true); 
                                }
                                updateEmailLogs($updateEmailLog);
                            } 
                        }
                        else
                        {
                            if($postSearchAry['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_3__)
                            {
                                try
                                {
                                    $emailLogsAry['szToAddress'] = $to ;
                                    $idEmailLog = logEmails($emailLogsAry);

                                    $message1 = new SendGrid\Mail();
                                    $message1->
                                    setFrom($from)->
                                    setFromName(__STORE_SUPPORT_NAME__)->
                                    setSubject($subject)->
                                        setHtml($message)->
                                    addTo($to)-> 
                                        addAttachment($invoice_pdf_file)->addUniqueArgument('email_key',$szEmailKey);
                                    $smtp->send($message1); 
                                }
                                catch(Exception $e) 
                                {
                                    $szExceptionMessage = " System registered exception in sending this email. \n\n ";
                                    $szExceptionMessage .= 'Exception Message: ' .$e->getMessage();  
                                }

                                $updateEmailLog = array();
                                $updateEmailLog['iSuccess'] = 1;
                                $updateEmailLog['idEmailLog'] = $idEmailLog;
                                if(!empty($szExceptionMessage))
                                {
                                    $updateEmailLog['szSendgridResponseLogs'] = $szExceptionMessage;
                                }
                                else
                                {
                                    $updateEmailLog['szSendgridResponseLogs'] = print_R($message1,true); 
                                }
                                updateEmailLogs($updateEmailLog);
                            }
                            else
                            {
                                try
                                {
                                    $emailLogsAry['szToAddress'] = $to ;
                                    $idEmailLog = logEmails($emailLogsAry);
                                    $message1 = new SendGrid\Mail();
                                    $message1->
                                    setFrom($from)->
                                    setFromName(__STORE_SUPPORT_NAME__)->
                                    setSubject($subject)->
                                        setHtml($message)->
                                    addTo($to)->
                                        addAttachment($confirmation_pdf_file)->
                                        addAttachment($invoice_pdf_file)->addUniqueArgument('email_key',$szEmailKey);
                                    $smtp->send($message1); 
                                }
                                catch(Exception $e) 
                                {
                                    $szExceptionMessage = " System registered exception in sending this email. \n\n ";
                                    $szExceptionMessage .= 'Exception Message: ' .$e->getMessage();  
                                }

                                $updateEmailLog = array();
                                $updateEmailLog['iSuccess'] = 1;
                                $updateEmailLog['idEmailLog'] = $idEmailLog;
                                if(!empty($szExceptionMessage))
                                {
                                    $updateEmailLog['szSendgridResponseLogs'] = $szExceptionMessage;
                                }
                                else
                                {
                                    $updateEmailLog['szSendgridResponseLogs'] = print_R($message1,true); 
                                }
                                updateEmailLogs($updateEmailLog);
                            } 
                        }
                    } 
                }
            } 
            if(!empty($attacfilename))
            { 
                if(!empty($to) && is_array($to))
                { 
                    foreach($to as $emails)
                    {
                        if(!empty($emails))
                        {
                            try
                            {
                                $emailLogsAry['szToAddress'] = $emails ;
                                $idEmailLog = logEmails($emailLogsAry);


                                if(is_array($attacfilename))
                                {
                                    $message1 = new SendGrid\Mail();
                                    $message1->
                                    setFrom($from)->
                                    setFromName(__STORE_SUPPORT_NAME__)->
                                    setSubject($subject)->
                                        setHtml($message)->
                                    addTo($emails)->
                                        addAttachment($attacfilename[0])->addAttachment($attacfilename[1])->addUniqueArgument('email_key',$szEmailKey);
                                    $smtp->send($message1); 
                                }
                                else
                                {

                                } 
                            }
                            catch(Exception $e) 
                            {
                                $szExceptionMessage = " System registered exception in sending this email. \n\n ";
                                $szExceptionMessage .= 'Exception Message: ' .$e->getMessage();  
                            } 	
                            $updateEmailLog = array();
                            $updateEmailLog['iSuccess'] = 1;
                            $updateEmailLog['idEmailLog'] = $idEmailLog;
                            if(!empty($szExceptionMessage))
                            {
                                $updateEmailLog['szSendgridResponseLogs'] = $szExceptionMessage;
                            }
                            else
                            {
                                $updateEmailLog['szSendgridResponseLogs'] = print_R($message1,true); 
                            }
                            updateEmailLogs($updateEmailLog);
                        }
                    }
                }
                else
                { 
                    if(!empty($to))
                    {
                       try
                       {
                            $emailLogsAry['szToAddress'] = $to ;
                            $idEmailLog = logEmails($emailLogsAry);

                            $message1 = new SendGrid\Mail();
                            $message1->
                            setFrom($from)->
                            setFromName(__STORE_SUPPORT_NAME__)->
                            setSubject($subject)->
                                setHtml($message)->
                            addTo($to)->
                                addAttachment($attacfilename)->addUniqueArgument('email_key',$szEmailKey);
                            $smtp->send($message1); 
                        }
                        catch(Exception $e) 
                        {
                            $szExceptionMessage = " System registered exception in sending this email. \n\n ";
                            $szExceptionMessage .= 'Exception Message: ' .$e->getMessage();  
                        }
                        $updateEmailLog = array();
                        $updateEmailLog['iSuccess'] = 1;
                        $updateEmailLog['idEmailLog'] = $idEmailLog;
                        if(!empty($szExceptionMessage))
                        {
                            $updateEmailLog['szSendgridResponseLogs'] = $szExceptionMessage;
                        }
                        else
                        {
                            $updateEmailLog['szSendgridResponseLogs'] = print_R($message1,true); 
                        }
                        updateEmailLogs($updateEmailLog);
                    } 
                }   		
            } 
        }
        else
        { 
            if(!empty($to) && is_array($to))
            {
                foreach($to as $emails)
                {
                    if(!empty($emails))
                    {
                       try
                       {
                            $emailLogsAry['szToAddress'] = $emails ;
                            $idEmailLog = logEmails($emailLogsAry); 
                            $message1 = new SendGrid\Mail();
                            $message1->
                            setFrom($from)->
                            setFromName($szFromUserName)->
                            setSubject($subject)->
                                setHtml($message)->
                            addTo($emails)->addUniqueArgument('email_key',$szEmailKey);
                            $smtp->send($message1); 
                        }
                        catch(Exception $e) 
                        {
                            $szExceptionMessage = " System registered exception in sending this email. \n\n ";
                            $szExceptionMessage .= 'Exception Message: ' .$e->getMessage();  
                        }
                        $updateEmailLog = array();
                        $updateEmailLog['iSuccess'] = 1;
                        $updateEmailLog['idEmailLog'] = $idEmailLog;
                        if(!empty($szExceptionMessage))
                        {
                            $updateEmailLog['szSendgridResponseLogs'] = $szExceptionMessage;
                        }
                        else
                        {
                            $updateEmailLog['szSendgridResponseLogs'] = print_R($message1,true); 
                        }
                        updateEmailLogs($updateEmailLog);
                    }
                }
            }
            else
            {
               if(!empty($cc))
               {
                   if(!empty($cc))
                   {
                       try
                       {
                            $emailLogsAry['szToAddress'] = $to ;
                            $idEmailLog = logEmails($emailLogsAry);  
                            $message1 = new SendGrid\Mail();
                            $message1->
                            setFrom($from)->
                            setFromName($szFromUserName)->
                            setSubject($subject)->
                                setHtml($message)->
                            addTo($to)->addTo($cc)->addUniqueArgument('email_key',$szEmailKey);
                            $smtp->send($message1); 
                        }
                        catch(Exception $e) 
                        {
                            $szExceptionMessage = " System registered exception in sending this email. \n\n ";
                            $szExceptionMessage .= 'Exception Message: ' .$e->getMessage();  
                        }

                        $updateEmailLog = array();
                        $updateEmailLog['iSuccess'] = 1;
                        $updateEmailLog['idEmailLog'] = $idEmailLog;
                        if(!empty($szExceptionMessage))
                        {
                            $updateEmailLog['szSendgridResponseLogs'] = $szExceptionMessage;
                        }
                        else
                        {
                            $updateEmailLog['szSendgridResponseLogs'] = print_R($message1,true); 
                        }
                        updateEmailLogs($updateEmailLog);
                   }
               }
               else
               {
                   if(!empty($to))
                   {
                       try
                       {
                            $emailLogsAry['szToAddress'] = $to ;
                            $idEmailLog = logEmails($emailLogsAry);  
                            $message1 = new SendGrid\Mail();
                            $message1->
                             setFrom($from)->
                             setFromName($szFromUserName)->
                             setSubject($subject)->
                                 setHtml($message)->
                             addTo($to)->addUniqueArgument('email_key',$szEmailKey);
                             $smtp->send($message1); 
                        }
                        catch(Exception $e) 
                        {
                            $szExceptionMessage = "System registered exception in sending this email. \n\n ";
                            $szExceptionMessage .= 'Exception Message: ' .$e->getMessage();  
                        }
                        $updateEmailLog = array();
                        $updateEmailLog['iSuccess'] = 1;
                        $updateEmailLog['idEmailLog'] = $idEmailLog;
                        if(!empty($szExceptionMessage))
                        {
                            $updateEmailLog['szSendgridResponseLogs'] = $szExceptionMessage;
                        }
                        else
                        {
                            $updateEmailLog['szSendgridResponseLogs'] = print_R($message1,true); 
                        }
                        updateEmailLogs($updateEmailLog); 
                   }
               } 		 
           }
        }  
        $sendingEmailLogging .= PHP_EOL." E-mail Sent successfully ".PHP_EOL;
    } 
    catch (Exception $ex) 
    {
        $sendingEmailLogging .= PHP_EOL." Exception occurs in sending email ".PHP_EOL;
        $sendingEmailLogging .= print_R($ex,true);
    }  
    
    $filename = __APP_PATH_ROOT__."/logs/sendEmailException.log";
    $strdata=array();
    $strdata[0]=" \n\n /******************** Sending E-mail on ".date('Y-m-d H:i:s')." \n\n";
    $strdata[1] = $sendingEmailLogging."\n\n"; 
    file_log(true, $strdata, $filename);
}
function updateEmailLogs($data)
{ 
    if(!empty($data))
    { 
        $kDatabase = new cDatabase(); 
        $query = "
            UPDATE
                ".__DBC_SCHEMATA_EMAIL_LOG__."
            SET 
                iSuccess = '".mysql_escape_custom($data['iSuccess'])."',
                szSendgridResponseLogs = '".mysql_escape_custom($data['szSendgridResponseLogs'])."',
                dtSent = now()
            WHERE
                id = '".$data['idEmailLog']."'
        ";
        //echo $query;
        if ($kDatabase->exeSQL($query))
        { 
            return true;
        }
        else
        {
            $kDatabase->error = true;
            $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
            $kDatabase->logError( "input", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
            return false;
        }  
    }
} 

function logEmails($emailLogsAry)
{
    if(!empty($emailLogsAry))
    {
        $kDatabase = new cDatabase();
        $query = "
            INSERT INTO
                ".__DBC_SCHEMATA_EMAIL_LOG__."
            (
                idUser,
                iMode,
                szEmailBody,
                szEmailSubject,
                szToAddress,
                szFromEmail,
                idBooking,
                szEmailKey,
                dtSent,
                iSuccess,
                iQuoteEmail
            )
            VALUES
            (
                ".(int)$emailLogsAry['idUser'].",
                ".(int)$emailLogsAry['iMode'].",
                '".mysql_escape_custom(utf8_encode($emailLogsAry['szEmailBody']))."',
                '".mysql_escape_custom(utf8_encode($emailLogsAry['szEmailSubject']))."',
                '".mysql_escape_custom($emailLogsAry['szToAddress'])."',
                '".mysql_escape_custom($emailLogsAry['szFromEmail'])."', 
                '".mysql_escape_custom($emailLogsAry['idBooking'])."', 
                '".mysql_escape_custom($emailLogsAry['szEmailKey'])."',  
                NOW(),
                ".(int)$success.",
                '".mysql_escape_custom($emailLogsAry['iQuoteEmail'])."'    
            )
        "; 

        if ($kDatabase->exeSQL($query))
        {
                return $kDatabase->iLastInsertID;
        }
        else
        {
                $kDatabase->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $kDatabase->logError( "input", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
        }
    }
}

function t()
{
	$args = func_get_args();
	return call_user_func_array(array('I18n', 'translate'), $args);
}

if (!function_exists('__autoload'))
{
	function __autoload($name)
	{
		if (function_exists('__cr_autoload'))
			return __cr_autoload($name);
	}
}
function __cr_autoload($name)
{
	if (strpos($name, 'Horde') === 0)
		require '../lib/' . str_replace('_', '/', $name) . '.php';
}
function filelog($log = false, $strdata, $filename){

    if($log)
    {
        $f = fopen($filename, "a");
        foreach($strdata as $key => $value)
        {
            fwrite($f, $value);
        }
        fclose($f); 
    }
}
function random_number_ConfirmKey()
{
		$chars = "234567890abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$i = 0;
		$password = "";
		while ($i <= 7)
		{
			$random_number .= $chars{mt_rand(0,strlen($chars)-1)};
			$i++;
		}
		return $random_number;
}
function sanitize_all_html_input($value)
{
	if(!empty($value))
	{
            $value=strip_tags($value);
	}
	return $value;
}
function sanitize_specific_html_input($value)
{
    $allwedTags="<p><br /><a><b><u><i>"; 
    if(!empty($value))
    {
        $value=strip_tags($value,$allwedTags);
    }
    return $value;
}
function sanitize_specific_tinymce_html_input($value)
{
	$html_tags_allowed='<a><p><i><br /><br><br ><center><em><head><h1><h2><h3><h4><h5><h6><strong><style><span>
						<table><tbody><td><th><thead><tr><tt><u><ul><li><small><param><object><ol><q><s><html>
						<body><button><div><small><textarea><input>
						<big><title><embed><b>';

	if(!empty($value))
	{
            $value = strip_tags($value,$html_tags_allowed);
	}

	return trim($value);
}
function sanitize_specific_tinymce_html_script_input($value)
{
	$html_tags_allowed='<script><a><p><img><br /><center><em><head><h1><h2><h3><h4><h5><h6><strong><style><span>
						<table><tbody><td><th><thead><tr><tt><u><ul><li><small><param><object><ol><q><s><html>
						<body><button><div><small><textarea><input>
						<big><title><embed><b>';

	if(!empty($value))
	{
		$value=strip_tags($value,$html_tags_allowed);
	}
	return $value;
}

function currency_convert($from,$to=__TO_CURRENCY_CONVERSION__, $amount = 1)
{
	    $options = array(
	        CURLOPT_RETURNTRANSFER => true, // return web page
	        CURLOPT_HEADER         => false,// don't return headers
	        CURLOPT_CONNECTTIMEOUT => 60     // timeout on connect
	    );
	 	
	    $ch = curl_init( "http://www.google.com/finance/converter?a=1&from=$from&to=$to");
	    curl_setopt_array( $ch, $options );
	    $result = curl_exec( $ch ); //let's fetch the result using cURL
 	     curl_close( $ch );
 
    if( $result === FALSE )
	        return $result;
	 
	    $result1 = explode('bld>',$result);
	    $result2 = explode($to,$result1[1]);
	    print_r($result1);
	    echo "<br/><br/>";
	    //$result = str_replace(chr(160), '', substr( $result[3], 0, strpos($result[3], ' ') ) );
	    //return ( $result == 0 ) ? FALSE : $result * $amount;
	    return $result2[0];
}
function sortArray($arrData, $p_sort_field, $p_sort_type = false,$szSecondSortField=false,$szSecondSortOrder=false,$szThirdSortField=false,$szThirdSortOrder=false,$iSearchServiceFlag=false )
{  
		if( !empty($arrData) )
		{
			foreach($arrData as $data)
			{
				$newData [] = $data;
			}
			for($i=0; $i<count($newData); $i++)
			{		                   	 
				if($iSearchServiceFlag)
				{
					$ar_sort_field[$p_sort_field][$i]=$newData[$i][$p_sort_field];
					$ar_sort_field[$szSecondSortField][$i]=$newData[$i][$szSecondSortField];
					if(!empty($szThirdSortField))
					{
						$ar_sort_field[$szThirdSortField][$i]=$newData[$i][$szThirdSortField];
					}
				}
				else
				{
			 		$ar_sort_field[$i]=$newData[$i][$p_sort_field];
				}
			} 
			if($iSearchServiceFlag)
			{				   
				if(!empty($szThirdSortField))
				{
					array_multisort($ar_sort_field[$p_sort_field], ($p_sort_type ? SORT_DESC : SORT_ASC), $ar_sort_field[$szSecondSortField],($szSecondSortOrder ? SORT_DESC : SORT_ASC), $ar_sort_field[$szThirdSortField],($szThirdSortOrder ? SORT_DESC : SORT_ASC),$newData);
				}
				else
				{
					array_multisort($ar_sort_field[$p_sort_field], ($p_sort_type ? SORT_DESC : SORT_ASC), $ar_sort_field[$szSecondSortField],($szSecondSortOrder ? SORT_DESC : SORT_ASC),$newData);
				} 
			}
			else
			{
				array_multisort($ar_sort_field, ($p_sort_type ? SORT_DESC : SORT_ASC), $newData);			
			}
			return $newData;
		}
}

function sortArrayInsenstive($arrData, $p_sort_field, $p_sort_type = false )
{	
		if( !empty($arrData) )
		{
			foreach($arrData as $data)
			{
				$newData [] = $data;
			}
			for($i=0; $i<count($newData); $i++)
			{		                   	 
			 	$ar_sort_field[$i]=strtolower($newData[$i][$p_sort_field]);
			}
			array_multisort($ar_sort_field, ($p_sort_type ? SORT_DESC : SORT_ASC), $newData);			
			return $newData;
		}
} 
function file_log($log = false, $strdata, $filename)
{  
    	if($log)
    	{
    		$f = fopen($filename, "a");
    		foreach($strdata as $key => $value)
    		{
    			fwrite($f, $value);
    		}
    		fclose($f);
    	}
}

function get_formated_cargo_measure($cargo_value,$flag=false)
{
    if($cargo_value>0)
    {
        $volumeAry = explode(".",$cargo_value);	
        $interger_value = $volumeAry[0];					
        $decimal_value = $volumeAry[1];
        //echo "<br> decimal value ".$decimal_value." cargo vol: ".$cargo_value ;
         
        if($decimal_value == 0)
        {
            $decimal_value = 0 ;
        }
        else if((substr($decimal_value,0,1) == 0))
        {
            $decimal_value = 1;
        }
        else if(strlen((int)$decimal_value)>=2)
        { 
            if($flag)
            {
                 return round((float)$cargo_value,3) ;
            }
            else
            {    
                return round((float)$cargo_value,2) ;
            }
        } 
        else if((substr($decimal_value,0,1) == 9))
        {
            $interger_value = $interger_value + 1 ;
            $decimal_value = 0;
        } 
        else
        {
            $decimal_value = $decimal_value ;
            $len = strlen($decimal_value);
            
            if($len>2)
            { 
                $xyz = substr($decimal_value,0,2);
                $decimal_value = ceil($xyz/10);
            }
            else
            {
                $decimal_value = $decimal_value ;
            }	
        } 
        $cargo_volume = $interger_value.".".$decimal_value;
        return $cargo_volume ;
    }
    else
    {
        return 0.0 ;
    }
}
function display_booikng_bread_crum($step,$t_base)
{
	?>
	<div class="breadcrumb">
		<p><?php echo t($t_base.'fields/start');?></p>
		<?php if($step==1){ ?>
			<p class="active"><span><?php echo t($t_base.'fields/select_ervice');?></span><span class="active-here"><?php echo t($t_base.'fields/you_are_here');?></span></p>
		<?php }else {?>
			<p><?php echo t($t_base.'fields/select_ervice');?></p>
		<?php }?>	
		
		<?php if($step==2){ ?>
			<p class="active"><span><?php echo t($t_base.'fields/booking_details');?></span><span class="active-here"><?php echo t($t_base.'fields/you_are_here');?></span></p>
		<?php }else {?>
			<p><?php echo t($t_base.'fields/booking_details');?></p>
		<?php }?>
		
		<?php if($step==3){ ?>
			<p class="active"><span><?php echo t($t_base.'fields/confirmation');?></span><span class="active-here"><?=t($t_base.'fields/you_are_here');?></span></p>
		<?php }else {?>
		<p><?php echo t($t_base.'fields/confirmation');?></p>
		<?php }?>		
		<?php if($step==4){ ?>
			<p class="active"><span><?php echo t($t_base.'fields/payment');?></span><span class="active-here"><?=t($t_base.'fields/you_are_here');?></span></p>
		<? } else {?>
			<p class="last"><?=t($t_base.'fields/payment');?></p>
		<? }?>
	</div>
	<?
}
function display_booikng_bread_crum_new($step,$t_base)
{
	$t_base = "BookingDetails/";
	?>
	<div class="breadcrumb"> 
		<ul class="clearfix">
		<? if($step==1){ ?>
			<li class="first active"><span><?=t($t_base.'fields/booking_information');?></span></li>
		<? }else {?>
			<li class="first"><span><?=t($t_base.'fields/booking_information');?></span></li>
		<? }?>	
		
		<? if($step==2){ ?>
			<li class="active"><span><?=t($t_base.'fields/payment');?></span></li>
		<? }else {?>
			<li><span><?=t($t_base.'fields/payment');?></span></li>
		<? }?>
		
		<? if($step==3){ ?>
			<li class="last active"><span><?=t($t_base.'fields/confirmation');?></span></li>
		<? }else {?>
			<li class="last"><span><?=t($t_base.'fields/confirmation');?></span></li>
		<? }?>		
		</ul>
	</div>
	<?php
}
function convert_time_to_UTC_date($datetime)
{  
	
 	$date_str = date('Y-m-d H:i:s',$datetime);
 	return $date_str ;
 	$datetime = new DateTime();
    $datetime->setTimezone(new DateTimeZone("UTC"));
    
    $date_ary = explode(" ",$date_str);
    
    $date = explode("-",$date_ary[0]);
    $time = explode(":",$date_ary[1]);
      
    $datetime->setDate($date[0], $date[1], $date[2]); 
    $datetime->setTime($time[0], $time[1], $time[2]); 
    //echo $datetime->format("Y-m-d H:i:s")."<br />";
    return $datetime->format("Y-m-d H:i:s");
}
 
function create_password()
{
	$chars = "2345678901ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	$i = 0;
	$password = "";
	while ($i <= 8)
	{
		$password .= $chars{mt_rand(0,strlen($chars)-1)};
		$i++;
	}
	return $password;
}

/*function getInvoiceConfirmationPdfFile($idBooking,$flag=0)
{
	$kBooking = new cBooking();
	$kConfig = new cConfig();
	require_once(__APP_PATH__.'/invoice/invoice.php');	
	
	$bookingDataArr=$kBooking->getExtendedBookingDetails($idBooking);
	
	$shipperCountry=$kConfig->getCountryName($bookingDataArr['idShipperCountry']);
	$pdf = new PDF_Invoice( 'P', 'mm', 'A4' );
	$pdf->AddPage();
	$pdf->invoiceTitle('Invoice');
	$pdf->addressDetials($bookingDataArr['szShipperCompanyName'],
	                  $bookingDataArr['szShipperAddress']." ".$bookingDataArr['szShipperAddress2']."\n".
	                  $bookingDataArr['szShipperPostCode']." ".$bookingDataArr['szShipperCity']."\n".
	                  $shipperCountry."\n".
	                  "Att :".$bookingDataArr['szShipperFirstName']." ".$bookingDataArr['szShipperLastName']);
	//$pdf->fact_dev( "Devis ", "TEMPO" );
	//$pdf->temporaire( "Transport-eca" );
	//$pdf->addDate( "03/12/2003");
	//$pdf->addClient("CL01");
	//$pdf->addPageNumber("");
	$pdf->invioceDetails("Invoice Number: ".$bookingDataArr['szInvoice']."\nInvoice Date: ".date('d/m/Y',strtotime($bookingDataArr['dtBookingConfirmed']))."\nBooking Date: ".date('d/m/Y',strtotime($bookingDataArr['dtBookingConfirmed']))."\nBooking Reference: ".$bookingDataArr['szBookingRef']);
	
	$pdf->bookedWith("Booked with:");
	$forwardCountry=$kConfig->getCountryName($bookingDataArr['idForwarderCountry']);
	$pdf->forwarderDetails($bookingDataArr['szForwarderDispName']."\n".$bookingDataArr['szForwarderAddress']." ".$bookingDataArr['szForwarderAddress2']."\n".$bookingDataArr['szForwarderPostCode']." ".$bookingDataArr['szForwarderCity']."\n".$forwardCountry);
	
	$pdf->service("Service :",$bookingDataArr['szServiceName']);
	
	$pdf->cutoff('Cut Off :',date('d/m/Y H:i',strtotime($bookingDataArr['dtCutOff'])));
	
	$pdf->available('Available :',date('d/m/Y H:i',strtotime($bookingDataArr['dtAvailable'])));
	
	$pdf->cargo('Cargo:');
	
	$cargoDetailArr=$kBooking->getCargoDeailsByBookingId($idBooking);
	if(!empty($cargoDetailArr))
	{
		$ctr=0;
		foreach($cargoDetailArr as $cargoDetailArrs)
		{
			$str='';
			$str=++$ctr.". ".$cargoDetailArrs['fWeight']."".strtoupper($cargoDetailArrs['wmdes']).",  ".$cargoDetailArrs['fLength']."x".$cargoDetailArrs['fWidth']."x".$cargoDetailArrs['fHeight']."".$cargoDetailArrs['cmdes'].",".$cargoDetailArrs['szCommodity'];	
			$pdf->cargoDetails($ctr,$str);
			
		}
	}
	$currencyClearance=$bookingDataArr['szCurrency']." ".$bookingDataArr['fTotalPriceCustomerCurrency'];
	$pdf->totalCustomClearance($ctr,'Freight and Customs Clearance, all inclusive',$currencyClearance);
	$gtotal=$bookingDataArr['szCurrency']." ".$bookingDataArr['fTotalPriceCustomerCurrency'];
	$pdf->total($ctr,'Total',$gtotal);
	
	$pdf->comment($ctr,'Your comments:',$bookingDataArr['szInvoiceComment']);
	
	$forwardersStr="For bookings related questions, please contact ".$bookingDataArr['szForwarderDispName']." customer service on customerservice@".$bookingDataArr['szForwarderDispName']."com.";
	$pdf->bookingQuestion($ctr,$forwardersStr);
		
	$invoicePaidDate="Invoice paid via PayPal on ".date('d F Y H:i',strtotime($bookingDataArr['dtBookingConfirmed']))." GMT";
	$pdf->invoicePaidDate($ctr,$invoicePaidDate);
	
	$pdf->thankyou($ctr,'Thank you!');
	$emailDetail="Transporteca � address � email: ".__STORE_SUPPORT_EMAIL__;
	$pdf->transportecaemail($emailDetail);
	

	if($flag)
	{
		$filename=__APP_PATH__."/invoice/bookingInvoice_".$bookingDataArr['szBookingRef'].".pdf";
		$pdf->Output($filename);
		return $filename;
	}
	else
	{
		$filename=__APP_PATH__."/invoice/bookingInvoice_".$bookingDataArr['szBookingRef'].".pdf";
		$pdf->Output($filename);
		return $filename;
	}
}*/

/*function getBookingConfirmationPdfFile($idBooking,$flag=false)
{
	require_once(__APP_PATH__.'/invoice/invoice.php');	
	//echo " booking".$idBooking ;
	$kBooking = new cBooking();
	$kForwarder = new cForwarder();
	$kConfig = new cConfig();
	$bookingDataArr=$kBooking->getExtendedBookingDetails($idBooking);
	
	$pdf = new PDF_Invoice( 'P', 'mm', 'A4' );
	$pdf->AddPage();
	$pdf->invoiceTitle('Booking Confirmation');
	$pdf->addressDetials("Booking Time: \n ","Booked By: \nBooking Reference:");
	//$pdf->temporaire( "Transport-eca" );
	
	$pdf->invioceDetails(date('d/m/Y',strtotime($bookingDataArr['dtBookingConfirmed']))."\n".$bookingDataArr['szFirstName']." ".$bookingDataArr['szLastName']."(".$bookingDataArr['szEmail'].")\n".$bookingDataArr['szBookingRef']);
	
	$pdf->bookedWith("Booked with:");
	$forwardCountry=$kConfig->getCountryName($bookingDataArr['idForwarderCountry']);
	$pdf->forwarderDetails($bookingDataArr['szForwarderDispName']."\n".$bookingDataArr['szForwarderAddress']." ".$bookingDataArr['szForwarderAddress2']."\n".$bookingDataArr['szForwarderPostCode']." ".$bookingDataArr['szForwarderCity']."\n".$forwardCountry);
	
	$pdf->service("Service :",$bookingDataArr['szServiceName']);
	
	//$shipperCountry=$kConfig->getCountryName($bookingDataArr['idShipperCountry']);
	$pdf->cutoff('Shipper :',$bookingDataArr['szShipperCompanyName']."\n".
					  $bookingDataArr['szShipperFirstName']." ".$bookingDataArr['szShipperLastName']."\n".
					  $bookingDataArr['szShipperPhone']."\n".  		
	                  $bookingDataArr['szShipperAddress']." ".$bookingDataArr['szShipperAddress2']."\n" .
	                  $bookingDataArr['szShipperPostCode']." ".$bookingDataArr['szShipperCity']."\n".
	                  $bookingDataArr['szShipperCountry']."\n");
	                  
	//$consigneesCountry=$kConfig->getCountryName($bookingDataArr['idConsigneeCountry']);                
	
	$pdf->available('Consignee :',$bookingDataArr['szConsigneeCompanyName']."\n".
					  $bookingDataArr['szConsigneeFirstName']." ".$bookingDataArr['szShipperLastName']."\n".
					  $bookingDataArr['szConsigneePhone']."\n".  		
	                  $bookingDataArr['szConsigneeAddress']." ".$bookingDataArr['szConsigneeAddress2']."\n" .
	                  $bookingDataArr['szConsigneePostCode']." ".$bookingDataArr['szConsigneeCity']."\n".
	                  $bookingDataArr['szConsigneeCountry']."\n");
	                  
	$pdf->cargoBooking('Cargo:'); 
	
	$cargoDetailArr=$kBooking->getCargoDeailsByBookingId($idBooking);
	if(!empty($cargoDetailArr))
	{
		$ctr=0;
		foreach($cargoDetailArr as $cargoDetailArrs)
		{
			$str='';
			$str=++$ctr.". ".$cargoDetailArrs['fWeight']."".strtoupper($cargoDetailArrs['wmdes']).",  ".$cargoDetailArrs['fLength']."x".$cargoDetailArrs['fWidth']."x".$cargoDetailArrs['fHeight']."".$cargoDetailArrs['cmdes'].",".$cargoDetailArrs['szCommodity'];	
			$pdf->cargoBookingDetails($ctr,$str);
			
		}
	}
	//$fromWareHouseCountry=$kConfig->getCountryName($bookingDataArr['idWarehouseFromCountry']);
	$pdf->cutoffBooking($ctr,'Cut Off:',date('d/m/Y',strtotime($bookingDataArr['dtCutOff']))."\n".$bookingDataArr['szWarehouseFromName']."\n".
					  $bookingDataArr['szWarehouseFromAddress']." ".$bookingDataArr['szWarehouseFromAddress2']."\n".	
	                  $bookingDataArr['szWarehouseFromAddress3']."\n" .
	                  $bookingDataArr['szWarehouseFromPostCode']." ".$bookingDataArr['szWarehouseFromCity']."\n".$bookingDataArr['szWarehouseFromCountry']);
	                  
	//$toWareHouseCountry=$kConfig->getCountryName($bookingDataArr['idWarehouseToCountry']);                  
	$pdf->availableBooking($ctr,'Available :',date('d/m/Y',strtotime($bookingDataArr['dtAvailable']))."\n".$bookingDataArr['szWarehouseToName']."\n".
					  $bookingDataArr['szWarehouseToAddress']." ".$bookingDataArr['szWarehouseToAddress2']."\n".	
	                  $bookingDataArr['szWarehouseToAddress3']."\n" .
	                  $bookingDataArr['szWarehouseToPostCode']." ".$bookingDataArr['szWarehouseToCity']."\n".$bookingDataArr['szWarehouseToCountry']);
	                  
	$pdf->nextBookingTitle($ctr,'Next steps after booking :');
	
	$pdf->nextBookingStepDetail($ctr,"Please ensure that the cargo is delivered to \n".$bookingDataArr['szWarehouseFromName']."\n".
					  $bookingDataArr['szWarehouseFromAddress']." ".$bookingDataArr['szWarehouseFromAddress2']."\n".	
	                  $bookingDataArr['szWarehouseFromAddress3']."\n" .
	                  $bookingDataArr['szWarehouseFromPostCode']." ".$bookingDataArr['szWarehouseFromCity']);
	$str='The booking was placed with '.$bookingDataArr['szForwarderDispName'].' on '.__MAIN_SITE_HOME_PAGE_URL__;
	      $kForwarder->load($bookingDataArr['idForwarder']);            
	$pdf->forwarderBooked($ctr,$str);      
	$strService="For bookings related questions, please contact ".$bookingDataArr['szForwarderDispName']." customer service on customerservice@dsv.com.";  
	$strService2="Standard Trading Terms and Conditions of ".$bookingDataArr['szForwarderDispName']." apply to this booking. These are available on ".$kForwarder->szLink;
	
	$pdf->forwarderService1($ctr,$strService);
	$pdf->forwarderService2($ctr,$strService2);      
	$filename="bookingConfirmation_".$bookingDataArr['szBookingRef'].".pdf";
	//$pdf->Output();
	//print_r($pdf);
	
	if($flag)
	{
		$filename=__APP_PATH__."/invoice/bookingConfirmation_".$bookingDataArr['szBookingRef'].".pdf";
		$pdf->Output($filename);
		return $filename;
	}
	else
	{
		$filename=__APP_PATH__."/invoice/bookingConfirmation_".$bookingDataArr['szBookingRef'].".pdf";
		$pdf->Output($filename);
		return $filename;
	}
}*/

function terms_condition_popup_html($bookingDetailsAry,$t_base,$iLanguage,$overview_page=false)
{
	$kForwarder = new cForwarder();
	$kWHSSearch = new cWHSSearch();
	$kForwarder->load($bookingDetailsAry['idForwarder']);
		
	if($overview_page)
	{
		if(!empty($kForwarder->szLink))
		{
			// if there is a http in url we remove. for e.g http://www.transporteca.com/tnc/ => www.transporteca.com/tnc/
			$szTncUrl_str = remove_http_from_url($kForwarder->szLink);	
			$szTncUrl = addhttp($kForwarder->szLink);
			
			$szTncUrl_link = "<a href='".$szTncUrl."' target='_blank'>".$szTncUrl."</a>";
		}
		else
		{
			$szTncUrl_link = '';
		}
		
		$szDisplayName = $kForwarder->szCompanyName ;
		$iLanguage = getLanguageId();
		$szBookingTnc = $kWHSSearch->getManageMentVariableByDescription('__BOOKING_TERMS__',$iLanguage);
		if($szBookingTnc=='')
		{	
                    $szBookingTnc = $kWHSSearch->getManageMentVariableByDescription('__BOOKING_TERMS__',__LANGUAGE_ID_ENGLISH__);
		}
		$replace_key = 'szForwarderName';
		$replace_key_2 = 'szForwarderTC';
		
		$szTncText = str_replace($replace_key, $szDisplayName, $szBookingTnc);
        $szTncText= str_replace($replace_key_2, $szTncUrl_link, $szTncText);
        
        return html_entity_decode($szTncText) ;
	}
	else
	{
		$customerServiceEmailAry = array();
		$idForwarder = $bookingDetailsAry['idForwarder'];
		$customerServiceEmailAry = $kForwarder->getForwarderCustomerServiceEmail($idForwarder);
		$tnc=$kWHSSearch->selectQuery(4,$iLanguage);
		
		if(!empty($kForwarder->szLink))
		{
			// if there is a http in url we remove. for e.g http://www.transporteca.com/tnc/ => www.transporteca.com/tnc/
			$szTncUrl_str = remove_http_from_url($kForwarder->szLink);	
			$szTncUrl = addhttp($kForwarder->szLink);
		}
?>
	<div id="popup-bg"></div>
		<div id="popup-container">			
			<div class="popup" style="width:850px;padding:10px 0;">
			<p class="close-icon" align="right">
			<a onclick="showHide('terms_condition_popup');" href="javascript:void(0);">
			<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
			</a>
			</p>
			<div class="terms-condition-popup link16">
			<h5><?=t($t_base.'title/term_condition')?></h5>
			<p>
			<?php
				if(!empty($kForwarder->szLink))
				{
			?>
				Standard Trading Terms of  <strong><?=$bookingDetailsAry['szForwarderDispName']?></strong> apply to this booking. Please visit <a href="<?=$szTncUrl?>" target="__blank"><?=$szTncUrl_str?></a> for details.
			<?php
				}
				else
				{
					?>
					Standard Terms and Conditions of <strong><?=$bookingDetailsAry['szForwarderDispName']?></strong> apply to this booking. Please e-mail <a href="mailto:<?=$customerServiceEmailAry[0]?>"><?=$customerServiceEmailAry[0]?></a> for details.
					<?
				}
			?>	
			<br />
			<?php  echo $szTermsCondition;  ?> 
			</p>
			<? 
			if(!empty($tnc)){
			foreach($tnc as $terms){?>
			<p><h5><strong><?=ucfirst($terms['szHeading'])?></strong></h5></p>
			<p><?=$terms['szDescription']?></p>
			<? }}?>
			<br/>
			<p align="center">
				<a href="javascript:void(0);" onclick="showHide('terms_condition_popup');" class="button1"><span><?=t($t_base.'fields/close');?></span></a>
			</p>
		</div>
	</div>	
	</div>	
	<?php
	}
}

function addhttp($url) 
{
	if (strpos($url,"://")==false) 
	{
		$url = "http://" . $url;
	}
	return $url;
}

function createEmailMessage($email_template, $replace_ary, $remindTemplateFlag=false,$szEmailTemplate_ID=false)
{
    $email_template = trim($email_template);
    $kDatabase = new cDatabase();
    $kDatabase->connect( __DBC_USER__, __DBC_PASSWD__, __DBC_SCHEMATA__, __DBC_HOST__);

                                                
        $query_select = "cmm.section_description,
                            cmm.subject"; 
    if($remindTemplateFlag)
    {
        $table =" ".__DBC_SCHEMATA_REMIND_CMS_EMAIL__." AS cm";
        
        $tableInnerJoin = "INNER JOIN ".__DBC_SCHEMATA_CMS_REMIND_MAPPING__." AS cmm
            ON
            cmm.idEmailTemplate=cm.id
                ";
        $query_where = "WHERE cm.id ='".(int)$szEmailTemplate_ID."'";
        
    }
    else
    {
        $table =" ".__DBC_SCHEMATA_CMS_EMAIL__." AS cm";
        
        $tableInnerJoin = "INNER JOIN ".__DBC_SCHEMATA_CMS_MAPPING__." AS cmm
            ON
            cmm.idEmailTemplate=cm.id
                ";
        
        $query_where = "WHERE cm.section_title = '".mysql_escape_custom($email_template)."'";
        
    }
    $query = "
        SELECT
            $query_select
        FROM
            ".$table."
            ".$tableInnerJoin."    
        $query_where
        AND
            cmm.idLanguage = '".(int)$replace_ary['iBookingLanguage']."'	
    ";
    //echo $query."<br><br>";   
    if ($result = $kDatabase->exeSQL($query))
    {
        if ($kDatabase->iNumRows > 0)
        {
            $row = $kDatabase->getAssoc($result);   
            
            if(mb_detect_encoding($row['section_description'], "UTF-8", true) == "UTF-8")
            {
                $message = utf8_decode($row['section_description']); 
            }
            else
            {
                $message = $row['section_description'];	 
            } 
            if(!empty($row['subject']))
            { 
                if(mb_detect_encoding($row['subject'], "UTF-8", true) == "UTF-8")
                {
                    $subject = utf8_decode($row['subject']);	
                }
                else
                {
                    $subject = $row['subject'];	
                }
            } 
        }
    }
    else
    {
        $kDatabase->error = true;
        $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
        $kDatabase->logError( "input", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
        return false;
    }
    if (count($replace_ary) > 0)
    {
        foreach ($replace_ary as $replace_key => $replace_value)
        {
            $message = str_replace($replace_key, $replace_value, $message);
            $subject= str_replace($replace_key, $replace_value, $subject);
        }
    } 
    $szEmailBody = htmlentities($message, ENT_QUOTES,'UTF-8');
    $szEmailSubject = htmlentities($subject, ENT_QUOTES,'UTF-8');  
    
    if(empty($szEmailBody))
    {
        $szEmailBody = $message;
    }
    if(empty($szEmailSubject))
    {
        $szEmailSubject = $subject;
    }
    $ret_ary['szEmailBody'] = $szEmailBody;
    $ret_ary['szEmailSubject'] =  $szEmailSubject;
    
//    $ret_ary['szEmailBody'] = $message;
//    $ret_ary['szEmailSubject'] = $subject;    
    return $ret_ary ;
}
function display_shipper_information_html($shipperConsigneeAry,$postSearchAry,$t_base,$allCountriesArr,$display_pickup_address,$disable_shipper_fields,$show_from_landing_page=false)
{
	?>
<script type="text/javascript">
function initAutodropDowns_shipper() 
{	
	$("#szShipperPostcode").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
	
	$("#szShipperCity").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});	
}
initAutodropDowns_shipper();
</script>
	<div class="oh">
		<p class="fl-40"><?=t($t_base.'fields/c_name');?></p>
		<p class="fl-60">
			<input type="text" onblur="check_shipper_consignee_feilds('SHIPPER')" name="shipperConsigneeAry[szShipperCompanyName]" id="szShipperCompanyName" value="<?=$shipperConsigneeAry['szShipperCompanyName']?>"/>
			<input type="hidden"  name="shipperConsigneeAry[szShipperCompanyName_old]" id="szShipperCompanyName_old" value="<?=$shipperConsigneeAry['szShipperCompanyName']?>"/>
		</p>
	</div>
	<div class="oh">
		<p class="fl-40"><?=t($t_base.'fields/f_name');?></p>
		<p class="fl-60">
			<input type="text" onblur="check_shipper_consignee_feilds('SHIPPER')" name="shipperConsigneeAry[szShipperFirstName]" id="szShipperFirstName" value="<?=$shipperConsigneeAry['szShipperFirstName']?>"/>
			<input type="hidden" name="shipperConsigneeAry[szShipperFirstName_old]" id="szShipperFirstName_old" value="<?=$shipperConsigneeAry['szShipperFirstName']?>"/>
		</p>
	</div>				
	<div class="oh">
		<p class="fl-40"><?=t($t_base.'fields/l_name');?></p>
		<p class="fl-60">
			<input type="text" onblur="check_shipper_consignee_feilds('SHIPPER')" name="shipperConsigneeAry[szShipperLastName]" id="szShipperLastName" value="<?=$shipperConsigneeAry['szShipperLastName']?>"/>
			<input type="hidden" name="shipperConsigneeAry[szShipperLastName_old]" id="szShipperLastName_old" value="<?=$shipperConsigneeAry['szShipperLastName']?>"/>
		</p>
	</div>
	<div class="oh">
		<p class="fl-40"><?=t($t_base.'fields/email');?>&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>&nbsp;</p>
		<p class="fl-60">
			<input type="text" onblur="check_shipper_consignee_feilds('SHIPPER')" name="shipperConsigneeAry[szShipperEmail]" id="szShipperEmail" value="<?=$shipperConsigneeAry['szShipperEmail']?>"/>
			<input type="hidden" name="shipperConsigneeAry[szShipperEmail_old]" id="szShipperEmail_old" value="<?=$shipperConsigneeAry['szShipperEmail']?>"/>
		</p>
	</div>
	<div class="oh">
		<p class="fl-40"><?=t($t_base.'fields/p_number');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/PHONE_NUMBER_TOOL_TIP_TEXT');?>','<?=t($t_base.'messages/PHONE_NUMBER_TOOL_TIP_TEXT_EXAMPLE');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a>&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span></p>
		<p class="fl-60">
			<input type="text" onblur="check_shipper_consignee_feilds('SHIPPER')" name="shipperConsigneeAry[szShipperPhone]" id="szShipperPhone" value="<?=$shipperConsigneeAry['szShipperPhone']?>"/>
			<input type="hidden" name="shipperConsigneeAry[szShipperPhone_old]" id="szShipperPhone_old" value="<?=$shipperConsigneeAry['szShipperPhone']?>"/>
		</p>
	</div>
	<div class="oh">
		<p class="fl-40"><?=t($t_base.'fields/country');?></p>
		<p class="fl-60">
			<select size="1" onblur="check_shipper_consignee_feilds('SHIPPER')" name="shipperConsigneeAry[idShipperCountry]" <? if($display_pickup_address=='none' && $disable_shipper_fields){ ?> disabled <? }?>  onchange="activatePostCodeField(this.value,'szShipperCity','szShipperPostcode','<?=t($t_base.'fields/type_name');?>','<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')" id="idShipperCountry">
			<?php
				if(!empty($allCountriesArr))
				{
					foreach($allCountriesArr as $allCountriesArrs)
					{
						?><option value="<?=$allCountriesArrs['id']?>" <?php if(($allCountriesArrs['id']==$shipperConsigneeAry['idShipperCountry']) || (($allCountriesArrs['id']==$postSearchAry['idOriginCountry']) && $show_from_landing_page)){?> selected <?php }?>><?=$allCountriesArrs['szCountryName']?></option>
						<?php
					}
				}
			?>
		   </select>
		   <input type="hidden" name="shipperConsigneeAry[idShipperCountry_old]" id="idShipperCountry_old" value="<?=!$show_from_landing_page?$shipperConsigneeAry['idShipperCountry']:$postSearchAry['idOriginCountry']?>"/>
		</p>
	</div>
	<div class="oh">
		<p class="fl-40"><?=t($t_base.'fields/city');?>&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>&nbsp;</p>
		<p class="fl-60">
			<input type="text" <? if($display_pickup_address=='none' && $disable_shipper_fields){?> disabled <? }?> onfocus="blank_me(this.id,'<?=t($t_base.'fields/type_name');?>')" onblur="show_me(this.id,'<?=t($t_base.'fields/type_name');?>');check_shipper_consignee_feilds('SHIPPER')"  name="shipperConsigneeAry[szShipperCity]" id="szShipperCity" value="<?=!$show_from_landing_page?$shipperConsigneeAry['szShipperCity']:$postSearchAry['szOriginCity']?>"/>
			<input type="hidden" name="shipperConsigneeAry[szShipperCity_old]" id="szShipperCity_old" value="<?=!$show_from_landing_page?$shipperConsigneeAry['szShipperCity']:$postSearchAry['szOriginCity']?>"/>
		</p>
	</div>
	<div class="oh">
		<p class="fl-40"><?=t($t_base.'fields/postcode');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/POSTCODE_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/POSTCODE_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
		<p class="fl-60">
			<input type="text" <? if($display_pickup_address=='none' && $disable_shipper_fields){?> disabled <? }?> name="shipperConsigneeAry[szShipperPostcode]" id="szShipperPostcode" onfocus="blank_me(this.id,'<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')" onblur="show_me(this.id,'<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>');check_shipper_consignee_feilds('SHIPPER');"  value="<?=!$show_from_landing_page ?$shipperConsigneeAry['szShipperPostCode']:$postSearchAry['szOriginPostCode']?>"/>
			<input type="hidden" name="shipperConsigneeAry[szShipperPostcode_old]" id="szShipperPostcode_old" value="<?=!$show_from_landing_page ?$shipperConsigneeAry['szShipperPostCode']:$postSearchAry['szOriginPostCode']?>"/>
		</p>
	</div>		
	<div class="oh">
		<p class="fl-40"><?=t($t_base.'fields/address');?></p>
		<p class="fl-60">
			<input type="text" onblur="check_shipper_consignee_feilds('SHIPPER')" name="shipperConsigneeAry[szShipperAddress]" id="szShipperAddress" value="<?=$shipperConsigneeAry['szShipperAddress']?>"/>
			<input type="hidden" name="shipperConsigneeAry[szShipperAddress_old]" id="szShipperAddress_old" value="<?=$shipperConsigneeAry['szShipperAddress']?>"/>
		</p>
	</div>
	<div class="oh">
		<p class="fl-40"><?=t($t_base.'fields/address2');?>&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>&nbsp;</p>
		<p class="fl-60">
			<input type="text" onblur="check_shipper_consignee_feilds('SHIPPER')" name="shipperConsigneeAry[szShipperAddress2]" id="szShipperAddress2" value="<?=$shipperConsigneeAry['szShipperAddress2']?>"/>
			<input type="hidden" name="shipperConsigneeAry[szShipperAddress2_old]" id="szShipperAddress2_old" value="<?=$shipperConsigneeAry['szShipperAddress2']?>"/>
		</p>
	</div>
	<div class="oh">
		<p class="fl-40"><?=t($t_base.'fields/address3');?>&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>&nbsp;</p>
		<p class="fl-60">
			<input type="text" onblur="check_shipper_consignee_feilds('SHIPPER')" name="shipperConsigneeAry[szShipperAddress3]" id="szShipperAddress3" value="<?=$shipperConsigneeAry['szShipperAddress3']?>"/>
			<input type="hidden" name="shipperConsigneeAry[szShipperAddress3_old]" id="szShipperAddress3_old" value="<?=$shipperConsigneeAry['szShipperAddress3']?>"/></p>
		</p>
	</div>
	<div class="oh">
		<p class="fl-40"><?=t($t_base.'fields/province');?>&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>&nbsp;</p>
		<p class="fl-60">
			<input type="text" onblur="check_shipper_consignee_feilds('SHIPPER')" name="shipperConsigneeAry[szShipperState]" id="szShipperState" value="<?=$shipperConsigneeAry['szShipperState']?>"/>
			<input type="hidden" name="shipperConsigneeAry[szShipperState_old]" id="szShipperState_old" value="<?=$shipperConsigneeAry['szShipperState']?>"/>
		</p>
	</div>
	<?php
}
function display_consignee_information_html($shipperConsigneeAry,$postSearchAry,$t_base,$allCountriesArr,$display_delivery_address,$disable_consignee_fields,$show_from_landing_page=false)
{
	//echo "<br><br>";
	//print_r($postSearchAry);
	?>
	
<script type="text/javascript">
function initAutodropDowns_consignee() 
{	
	$("#szConsigneePostcode").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
	
	$("#szConsigneeCity").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
}
initAutodropDowns_consignee();
</script>
	<div class="oh">
		<p>
			<input type="text" onblur="check_shipper_consignee_feilds('CONSIGNEE');" name="shipperConsigneeAry[szConsigneeCompanyName]" id="szConsigneeCompanyName" value="<?=$shipperConsigneeAry['szConsigneeCompanyName']?>"/>
			<input type="hidden" name="shipperConsigneeAry[szConsigneeCompanyName_old]" id="szConsigneeCompanyName_old" value="<?=$shipperConsigneeAry['szConsigneeCompanyName']?>"/>
		</p>
	</div>
	<div class="oh">
		<p>
			<input type="text" onblur="check_shipper_consignee_feilds('CONSIGNEE');" name="shipperConsigneeAry[szConsigneeFirstName]" id="szConsigneeFirstName" value="<?=$shipperConsigneeAry['szConsigneeFirstName']?>"/>
			<input type="hidden" name="shipperConsigneeAry[szConsigneeFirstName_old]" id="szConsigneeFirstName_old" value="<?=$shipperConsigneeAry['szConsigneeFirstName']?>"/>
		</p>
	</div>
	<div class="oh">
		<p>
			<input type="text" onblur="check_shipper_consignee_feilds('CONSIGNEE');" name="shipperConsigneeAry[szConsigneeLastName]" id="szConsigneeLastName" value="<?=$shipperConsigneeAry['szConsigneeLastName']?>"/>
			<input type="hidden" name="shipperConsigneeAry[szConsigneeLastName_old]" id="szConsigneeLastName_old" value="<?=$shipperConsigneeAry['szConsigneeLastName']?>"/>
		</p>
	</div>
	<div class="oh">
		<p>
			<input type="text" onblur="check_shipper_consignee_feilds('CONSIGNEE');" name="shipperConsigneeAry[szConsigneeEmail]" id="szConsigneeEmail" value="<?=$shipperConsigneeAry['szConsigneeEmail']?>"/>
			<input type="hidden" name="shipperConsigneeAry[szConsigneeEmail_old]" id="szConsigneeEmail_old" value="<?=$shipperConsigneeAry['szConsigneeEmail']?>"/>
		</p>
	</div>
	<div class="oh">
		<p>
			<input type="text" onblur="check_shipper_consignee_feilds('CONSIGNEE');" name="shipperConsigneeAry[szConsigneePhone]" id="szConsigneePhone" value="<?=$shipperConsigneeAry['szConsigneePhone']?>"/>
			<input type="hidden" name="shipperConsigneeAry[szConsigneePhone_old]" id="szConsigneePhone_old" value="<?=$shipperConsigneeAry['szConsigneePhone']?>"/>
		</p>
	</div>
	<div class="oh">
		<p>
			<select size="1" name="shipperConsigneeAry[idConsigneeCountry]" <? if($display_delivery_address=='none' && $disable_consignee_fields) {?> onfocus="this.blur();" disabled <? }?>  onchange="activatePostCodeField(this.value,'szConsigneeCity','szConsigneePostcode','<?=t($t_base.'fields/type_name');?>','<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')" id="idConsigneeCountry">
			<?php
				if(!empty($allCountriesArr))
				{
					foreach($allCountriesArr as $allCountriesArrs)
					{
						?><option value="<?=$allCountriesArrs['id']?>" <?php if(($allCountriesArrs['id']==$shipperConsigneeAry['idConsigneeCountry']) || (($allCountriesArrs['id']==$postSearchAry['idDestinationCountry']) && $show_from_landing_page)){?> selected <?php }?>><?=$allCountriesArrs['szCountryName']?></option>
						<?php
					}
				}
			?>
		   </select>
		   <input type="hidden" name="shipperConsigneeAry[idConsigneeCountry_old]" id="idConsigneeCountry_old" value="<?=$shipperConsigneeAry['idConsigneeCountry']?>"/>
		</p>
	</div>
	<div class="oh">					
		<p>
			<input type="text" <? if($display_delivery_address=='none' && $disable_consignee_fields){?> disabled <? }?> onfocus="blank_me(this.id,'<?=t($t_base.'fields/type_name');?>')" onblur="show_me(this.id,'<?=t($t_base.'fields/type_name');?>');check_shipper_consignee_feilds('CONSIGNEE');" name="shipperConsigneeAry[szConsigneeCity]" id="szConsigneeCity" value="<?=!$show_from_landing_page?$shipperConsigneeAry['szConsigneeCity']:$postSearchAry['szDestinationCity']?>"/>
			<input type="hidden" name="shipperConsigneeAry[szConsigneeCity_old]" id="szConsigneeCity_old" value="<?=!$show_from_landing_page?$shipperConsigneeAry['szConsigneeCity']:$postSearchAry['szDestinationCity']?>"/>
		</p>
	</div>
	<div class="oh">
		<p>
			<input type="text" <? if($display_delivery_address=='none' && $disable_consignee_fields){?> disabled <? }?> onfocus="blank_me(this.id,'<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')" onblur="show_me(this.id,'<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>');check_shipper_consignee_feilds('CONSIGNEE');"  name="shipperConsigneeAry[szConsigneePostcode]" id="szConsigneePostcode" value="<?=!$show_from_landing_page?$shipperConsigneeAry['szConsigneePostCode']:$postSearchAry['szDestinationPostCode']?>"/>
			<input type="hidden" name="shipperConsigneeAry[szConsigneePostcode_old]" id="szConsigneePostcode_old" value="<?=!$show_from_landing_page?$shipperConsigneeAry['szConsigneePostCode']:$postSearchAry['szDestinationPostCode']?>"/>
		</p>
	</div>
	<div class="oh">
		<p>
			<input type="text" onblur="check_shipper_consignee_feilds('CONSIGNEE');" name="shipperConsigneeAry[szConsigneeAddress]" id="szConsigneeAddress" value="<?=$shipperConsigneeAry['szConsigneeAddress']?>"/>
			<input type="hidden" name="shipperConsigneeAry[szConsigneeAddress_old]" id="szConsigneeAddress_old" value="<?=$shipperConsigneeAry['szConsigneeAddress']?>"/>
		</p>
	</div>
	<div class="oh">
		<p>
			<input type="text" onblur="check_shipper_consignee_feilds('CONSIGNEE');" name="shipperConsigneeAry[szConsigneeAddress2]" id="szConsigneeAddress2" value="<?=$shipperConsigneeAry['szConsigneeAddress2']?>"/>
			<input type="hidden" name="shipperConsigneeAry[szConsigneeAddress2_old]" id="szConsigneeAddress2_old" value="<?=$shipperConsigneeAry['szConsigneeAddress2']?>"/></p>
		</p>
	</div>
	<div class="oh">
		<p>
			<input type="text" onblur="check_shipper_consignee_feilds('CONSIGNEE');" name="shipperConsigneeAry[szConsigneeAddress3]" id="szConsigneeAddress3" value="<?=$shipperConsigneeAry['szConsigneeAddress3']?>"/>
			<input type="hidden" name="shipperConsigneeAry[szConsigneeAddress3_old]" id="szConsigneeAddress3_old" value="<?=$shipperConsigneeAry['szConsigneeAddress3']?>"/>
		</p>
	</div>
	<div class="oh">
		<p>
			<input type="text" name="shipperConsigneeAry[szConsigneeState]" id="szConsigneeState" value="<?=$shipperConsigneeAry['szConsigneeState']?>"/>
			<input type="hidden" name="shipperConsigneeAry[szConsigneeState_old]" id="szConsigneeState_old" value="<?=$shipperConsigneeAry['szConsigneeState']?>"/>
		</p>
	</div>
	<?php
}
function checkAuthForwarder()
{
    if((int)($_SESSION['forwarder_user_id']<=0))
    {
        header('Location:'.__FORWARDER_HOME_PAGE_URL__);
        die();
    }
} 
function checkAuthMetrics()
{
    if((int)($_SESSION['metrics_user_id']<=0))
    {
        header('Location:'.__METRICS_URL__);
        die();
    }
}

function checkAuthForwarder_ajax()
{
    if((int)($_SESSION['forwarder_user_id']<=0))
    {
        $redirect_url = __FORWARDER_HOME_PAGE_URL__ ;
        ?>
        <script type="text/javascript">
                $(location).attr("href",'<?=$redirect_url?>');
        </script>
        <?
    }
}

function test($iTransitDays,$iCutoffDay)
{
	if($iTransitDays>0 && $iCutoffDay>0)
	{
		$x = floor(((($iTransitDays/7) - floor(($iTransitDays/7))) * 7)) ;
		
		if($iCutoffDay > 7)
		{
			$y = ($x + $iCutoffDay) - 7 ;
		}
		else
		{
			$y = ($x + $iCutoffDay) ;
		}
	}
}

function display_forwarder_bank_details($kForwarder,$t_base)
{
	?>
	<div class="oh">
		<p class="fl-30"><?=t($t_base.'titles/bank_name');?></p>
		<p class="field-container" style="font-style:italic;"><?=$kForwarder->szBankName?></p>
	</div>
	<div class="oh">
		<p class="fl-30"><?=t($t_base.'titles/swift_code');?></p>
		<p class="field-container" style="font-style:italic;"><?=$kForwarder->szSwiftCode?></p>
	</div>
	<div class="oh">
		<p class="fl-30"><?=t($t_base.'titles/country');?></p>
		<p class="field-container" style="font-style:italic;"><?=$kForwarder->szCountryName?></p>
	</div>
	<br><br>
	<div class="oh">
		<p class="fl-30"><?=t($t_base.'titles/account_name');?></p>
		<p class="field-container" style="font-style:italic;"><?=$kForwarder->szNameOnAccount?></p>
	</div>
	<div class="oh">
		<p class="fl-30"><?=t($t_base.'titles/sort_code');?>&nbsp;<span class="optional">(<?=t($t_base.'titles/optional');?>)</span></p>
		<p class="field-container" style="font-style:italic;"><?=($kForwarder->iSortCode>0)?$kForwarder->iSortCode:''?></p>
	</div>
	
	<div class="oh">
		<p class="fl-30"><?=t($t_base.'titles/account_number');?></p>
		<p class="field-container" style="font-style:italic;"><?=(($kForwarder->iAccountNumber) > 0)?$kForwarder->iAccountNumber:'';?></p>
	</div>
	<div class="oh">
		<p class="fl-30"><?=t($t_base.'titles/IBAN_account_number');?>&nbsp;<span class="optional">(<?=t($t_base.'titles/optional');?>)</span></p>
		<p class="field-container" style="font-style:italic;"><?=$kForwarder->szIBANAccountNumber?></p>
	</div>
	<?
}
function display_forwarder_company_details($kForwarder,$t_base)
{
	?>	
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/display_name');?></p>
		<p  style="font-style:italic;float:left;"><?=$kForwarder->szDisplayName?></p>
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/general_email_address');?></p>
		<p  style="font-style:italic;float:left;"><?=$kForwarder->szGeneralEmailAddress?></p>
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/standard_terms');?></p>
		<p style="font-style:italic;float:left;"><?=$kForwarder->szLink?></p>
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/display_logo');?></p>
		<p class="field-container">
			<?
				if(!empty($kForwarder->szLogo))
				{
			?>
			<span class="forwarder_logo"><img id="image_src" src="<?=__BASE_STORE_INC_URL__.'/image.php?img='.$kForwarder->szLogo."&w=".__MAX_ALLOWED_FORWARDER_LOGO_WIDTH__."&h=".__MAX_ALLOWED_FORWARDER_LOGO_HEIGHT__?>" alt="<?=$kForwarder->szDisplayName?>"></span>
			<?
				}
			?>
		</p>
	</div>
	<?
}
function display_registered_company_details($kForwarder,$t_base)
{
	?>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/company_name');?></p>
		<p class="field-container" style="font-style:italic;"><?=$kForwarder->szCompanyName?></p>
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/address1');?></p>
		<p class="field-container" style="font-style:italic;"><?=$kForwarder->szAddress?></p>
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/address2');?> <span class="optional">(<?=t($t_base.'titles/optional');?>)</span></p>
		<p class="field-container" style="font-style:italic;"><?=$kForwarder->szAddress2?></p>
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/address3');?> <span class="optional">(<?=t($t_base.'titles/optional');?>)</span></p>
		<p class="field-container" style="font-style:italic;"><?=$kForwarder->szAddress3?></p>
	</div>
	
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/postcode');?></p>
		<p class="field-container" style="font-style:italic;"><?=$kForwarder->szPostCode?></p>
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/city');?></p>
		<p class="field-container" style="font-style:italic;"><?=$kForwarder->szCity?></p>
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/province');?> <span class="optional">(<?=t($t_base.'titles/optional');?>)</span></p>
		<p class="field-container" style="font-style:italic;"><?=$kForwarder->szState?></p>
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/country');?></p>
		<p class="field-container" style="font-style:italic;"><?=$kForwarder->szCountryName?></p>
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/phone_number');?></p>
		<p class="field-container" style="font-style:italic;"><?=$kForwarder->szPhone?></p>
	</div>
	<br><br>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/company_reg_num');?></p>
		<p class="field-container" style="font-style:italic;"><?=$kForwarder->szCompanyRegistrationNum?></p>
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/vat_reg_num');?> <span class="optional">(<?=t($t_base.'titles/optional');?>)</span></p>
		<p class="field-container" style="font-style:italic;"><?=$kForwarder->szVatRegistrationNum?></p>
	</div>
	<?
}

function get_file_extension($file_name)
{
  return substr(strrchr($file_name,'.'),1);
}

function displayStyle($i)
{ 
	if($i>2){
		$display="block";
	}
		else{
		$display="none";
		}
	return $display;	
}

function createAvtarThumbs($pathToImages,$fname, $pathToThumbs, $thumbWidth , $thumbHieght,$fileName='')
{
  // open the directory
  $dir = opendir( $pathToImages );


  // loop through it, looking for any/all JPG files:
  //while (false !== ($fname = readdir( $dir ))) {
    // parse path for the extension

   // echo $pathToImages."==".$fname."<br />";

    $info = pathinfo($pathToImages . $fname);

    // continue only if this is a JPEG image
    //if ( strtolower($info['extension']) == 'jpg' )
   // {
      //echo "Creating thumbnail for {$fname} <br />";

    	$max_width = 150;
    	$max_height = 150;

      // load image and get image size
		$filetype=explode(".",$fname);
		if(!empty($fileName))
		{
			$fil_name=$fileName.".".$filetype[1];
		}
		else
		{
			$fil_name=$filetype[0]."_thumb.".$filetype[1];
		}
		$file_name=$pathToImages.$fname;

		$ext=$filetype[1];
		//echo $ext;
	  if($ext=='jpeg' || $ext=='jpg')
      {
		   $img = imagecreatefromjpeg( "{$pathToImages}{$fname}" );
		  // echo $pathToImages."**";
      }
      else if($ext=='gif')
      {
      		$img = imagecreatefromgif( "{$pathToImages}{$fname}" );
      }

      elseif($ext=='png')
      {
        $img = imagecreatefrompng( "{$pathToImages}{$fname}" );
      }
      //echo $img;
     //$getimagesize=getimagesize($filewithpath);
      $width = imagesx($img);
      $height = imagesy($img);

      // calculate thumbnail size
      $new_width = $width;
      //$new_height = floor( $height * ( $thumbWidth / $width ) );
      $new_height = $height;

       // create a new tempopary image
      $tmp_img = imagecreatetruecolor( $new_width, $new_height );

      // copy and resize old image into new image
      imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
      // save thumbnail into a file
      //$newfilename=explode(".",$file_name);
      //$newfilename1=$pathToImages.$newfilename[0]."_thumb.".$newfilename[1];

      imagejpeg($tmp_img,"{$pathToThumbs}{$fileName}");

   // }
  //}
  // close the directory
  closedir( $dir );
  return $fil_name;
}

function incomplete_comapny_details_popup($t_base,$incomplete_user_info=false)
{
	if($incomplete_user_info)
	{
		$navigation_pop_up_message_header =  t($t_base.'messages/incomplete_profile_header');
		$navigation_pop_up_message =  t($t_base.'messages/incomplete_profile_message');
	}
	else
	{
		$navigation_pop_up_message_header =  t($t_base.'forwarder_header/navigation_popup_message_header');
		$navigation_pop_up_message =  t($t_base.'forwarder_header/navigation_popup_message');
	}
	?>
	  <div id="popup-bg"></div>
		 <div id="popup-container">			
		  <div class="popup compare-popup">
		  	<h5><strong><?=$navigation_pop_up_message_header?></strong></h5>
			<p><?=$navigation_pop_up_message?></p>
			<br /><br />
			 <p align="center">
			 <?
			 	if($incomplete_user_info)
			 	{
			 ?>
			 <a href="<?=__FORWARDER_MY_ACCOUNT_URL__?>" class="button1"><span>Ok</span></a>
			 <? } else {?>
				<a href="<?=__FORWARDER_COMPANY_PREFERENCES_URL__?>" onclick="" class="button1"><span>Go</span></a>
				
			<? }?>
			</p>	
		  </div>
	   </div>
	</div>
	<?php
}
function display_obo_cc_bottom_html($t_base,$disable_feilds,$searchedDataAry=false,$formData=false,$field_name=false)
{	
	$disabled_str='';
	$display = 'none';
	if($disable_feilds)
	{
            $disabled_str = 'disabled="disabled"';
            $opacity_str = 'style="opacity:0.4;"';
	}
	else
	{
            $textmss=t($t_base."title/get_data");
            $textmss1=t($t_base."fields/change_data");
            $save_onclick = ' onclick = "update_obo_cc_data(\''.$textmss.'\',\''.$textmss1.'\');"';
	}
	$kConfig = new cConfig();
	$allCurrencyArr=$kConfig->getBookingCurrency(false,false,false,true);
	if(!empty($searchedDataAry))
	{
            $field_name = 'editOBOCCData';

            //putting data to the variables
            $szOriginCountry = $searchedDataAry['export']['szCountryName'] ;
            $szDestinationCountry = $searchedDataAry['import']['szCountryName'] ;
		
	}
	elseif(!empty($formData))
	{
            $field_name = 'addOBOCCData';
            //$save_onclick = " onclick = 'update_obo_cc_data();'";
            $kConfi=new cConfig();
            $szOriginCountryAry = $kConfi->getAllCountryInKeyValuePair(false,$formData['idOriginCountry']);
            $szDestinationCountryAry = $kConfi->getAllCountryInKeyValuePair(false,$formData['idDestinationCountry']) ;

            $szOriginCountry = $szOriginCountryAry[$formData['idOriginCountry']]['szCountryName'] ;
            $szDestinationCountry = $szDestinationCountryAry[$formData['idDestinationCountry']]['szCountryName'] ;		
            $display = 'block';
	}
	$idOriginWarehouse = $formData['idOriginWarehouse'];
	$idDestinationWarehouse = $formData['idDestinationWarehouse'];
	
	$kWHSSearch=new cWHSSearch();
	$kWHSSearch->load($formData['idOriginWarehouse']);
	$fromWarehouseName = $kWHSSearch->szWareHouseName;
        $iWarehouseType = $kWHSSearch->iWarehouseType;
	
	$kWHSSearch->load($formData['idDestinationWarehouse']);
	$toWarehouseName=$kWHSSearch->szWareHouseName;
	
        if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
        {
            $szSectionTitle = t($t_base.'title/air_obo_cc_no_service_found');
            $szCFSName = "airport warehouse";
        }
        else
        {
            $szSectionTitle = t($t_base.'title/obo_cc_no_service_found');
            $szCFSName = "CFS";
        }
        
	if($formData['idDestinationWarehouse']>0 && $formData['idOriginWarehouse']>0)
	{
	?>
	<strong><?=t($t_base.'messages/customs_clearance_rates_from')?> <?=t($t_base.'messages/rates_from')?> <?=$fromWarehouseName?> <?=t($t_base.'messages/to')?> <?=$toWarehouseName?></strong>
	<?php }else {?>
	<strong><?php echo t($t_base.'messages/customs_clearance_rates_from'); ?></strong>
	<?php }?>
	<form id="update_obo_cc_form" method="post" action="">
	
	<div id="obo_cc_no_record_found" style="margin: 5px 0 12px 0;display:<?=$display?>">
            <ul id="message_ul" style="padding:5px 5px 5px 10px;">
            <li><?=$szSectionTitle;?></li></ul>
	</div>
	<?php
            if($szOriginCountry=='' && empty($szOriginCountry))
            {
                $szOriginCountry="";
            }

            if($szDestinationCountry=='' && empty($szDestinationCountry))
            {
                $szDestinationCountry="";
            }
	?>
	<div id="obo_cc_error"  class="errorBox" style="display:none;"></div>
	<div class="oh">
		<p class="fl-40 gap-text-field" style="width:487px;"><?=t($t_base.'title/export_CC');?>&nbsp;<?=($szOriginCountry?'for '.$szOriginCountry:'')?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/EXPORT_CC_TOOL_TIP_HEADER_TEXT')." ".$szCFSName."?";?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
		<!-- <p class="fl-15" style="width: 117px;"><span class="f-size-12"><?=t($t_base.'title/country');?></span><br /><input disabled id="szOriginCountry" type="text" style="width: 107px;" name="<?=$field_name?>[szOriginCountry]" value="<?=$szOriginCountry?>" size="10" /></p>-->
		<p class="fl-15" style="width: 119px;"><span class="f-size-12"><?=t($t_base.'title/per_booking');?></span><br /><input <?=$disabled_str?> id="fOriginPrice" type="text" style="width: 107px;" name="<?=$field_name?>[fOriginPrice]" value="<?=$searchedDataAry['export']['fPrice']?>" size="10" onkeyup="checkCCField(this.value,'idOriginCurrency')" /></p>
		<p class="fl-20" style="width: 117px;"><span class="f-size-12"><?=t($t_base.'title/currency');?></span><br />
                    <select size="1" <?=$disabled_str?> name="<?=$field_name?>[idOriginCurrency]" id="idOriginCurrency" >
			<option value=""><?=t($t_base.'fields/select');?></option>			
			<?php
                            if(!empty($allCurrencyArr))
                            {
                                foreach($allCurrencyArr as $allCurrencyArrs)
                                {
                                    ?><option value="<?=$allCurrencyArrs['id']?>" <?=($searchedDataAry['export']['szCurrency'] ==  $allCurrencyArrs['id'] ) ? "selected":""?>><?=$allCurrencyArrs['szCurrency']?></option><?php
                                }
                            }
                        ?>
                    </select>
		</p>
	</div>
	<div class="oh">
		<p class="fl-40 gap-text-field" style="width:487px;"><?=t($t_base.'title/import_cc');?>&nbsp;<?=($szDestinationCountry?'for '.$szDestinationCountry:'')?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/IMPORT_CC_TOOL_TIP_HEADER_TEXT')." ".$szCFSName."?";?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
		<!--  <p class="fl-15" style="width: 117px;"><span class="f-size-12"><?=t($t_base.'title/country');?></span><br /><input disabled id="szDestinationCountry" type="hidden" style="width: 107px;" name="<?=$field_name?>[szDestinationCountry]" value="<?=$szDestinationCountry?>"  size="10" onkeyup="checkCCField(this.value,'fDestinationPrice')" /></p>-->
		<p class="fl-15" style="width: 119px;"><span class="f-size-12"><?=t($t_base.'title/per_booking');?></span><br /><input <?=$disabled_str?> id="fDestinationPrice" type="text" style="width: 107px;" name="<?=$field_name?>[fDestinationPrice]" value="<?=$searchedDataAry['import']['fPrice']?>" size="10" onkeyup="checkCCField(this.value,'idDestinationCurrency')" /></p>
		<p class="fl-20" style="width: 117px;"><span class="f-size-12"><?=t($t_base.'title/currency');?></span><br />
			<select size="1" <?=$disabled_str?> name="<?=$field_name?>[idDestinationCurrency]" id="idDestinationCurrency" >
				<option value=""><?=t($t_base.'fields/select');?></option>
				<?php
					if(!empty($allCurrencyArr))
					{
						foreach($allCurrencyArr as $allCurrencyArrs)
						{
							?><option value="<?=$allCurrencyArrs['id']?>" <?=($searchedDataAry['import']['szCurrency'] ==  $allCurrencyArrs['id'] ) ? "selected":""?>><?=$allCurrencyArrs['szCurrency']?></option>
							<?php
						}
					}
				?>
			</select>
		</p>
	</div>
	<br />
	<? if(isset($_SESSION['admin_id']) && (int)$_SESSION['admin_id']>0){?>
	<div class="oh" style="float: right;">
		<p class="fl-20" align="right"><a href="javascript:void(0);" <?=$save_onclick?> class="button1" <?=$opacity_str?>><span><?=t($t_base.'title/save');?></span></a></p>
	</div>
	<? }else{ ?>
	<div class="oh">
		<p class="fl-40"><a href="javascript:void(0)" onclick="cancel_obo_cc('<?=t($t_base.'fields/change_data');?>','<?=t($t_base.'title/get_data');?>')" class="button2" <?=$opacity_str?>><span><?=t($t_base.'title/cancel');?></span></a>
		 <a href="javascript:void(0);" onclick="clear_all_data_obo_cc();" class="button2" <?=$opacity_str?>><span><?=t($t_base.'title/clear_data');?></span></a></p>
		<p class="fl-40" align="right" style="padding-top:5px"><?=t($t_base.'title/click_to_update_changes');?></p>
		<p class="fl-20" align="right"><a href="javascript:void(0);" <?=$save_onclick?> class="button1" <?=$opacity_str?>><span><?=t($t_base.'title/save');?></span></a></p>
	</div>
	<? } ?>
	<input disabled id="szOriginCountry" type="hidden" style="width: 107px;" name="<?=$field_name?>[szOriginCountry]" value="<?=$szOriginCountry?>" size="10" />
	<input disabled id="szDestinationCountry" type="hidden" style="width: 107px;" name="<?=$field_name?>[szDestinationCountry]" value="<?=$szDestinationCountry?>"  size="10" />
	<input type="hidden" name="<?=$field_name?>[idOriginWarehouse]" value="<?=$idOriginWarehouse?>">
	<input type="hidden" name="<?=$field_name?>[idDestinationWarehouse]" value="<?=$idDestinationWarehouse?>">
</form>	
	<?php
}

function obo_data_export_top_form($idForwarder,$t_base,$iWarehouseType,$szFromPage=false)
{
    $kExport_Import=new cExport_Import();

    $forwardWareHouseArr = array();
    $forwardCountriesArr = array();
    $forwardWareHouseCityAry = array();
    // getting all countries where forwarder has warehouse listed.
    $forwardCountriesArr=$kExport_Import->getForwaderCountries($idForwarder,$iWarehouseType);
    $idOriginCountry = $forwardCountriesArr[0]['idCountry'];
    $idDestinationCountry = $forwardCountriesArr[1]['idCountry'];

    // getting all warehouse's of this forwarder.
    $forwardWareHouseArr_origin=$kExport_Import->getForwaderWarehouses($idForwarder,$idOriginCountry,false,false,$iWarehouseType);

    $forwardWareHouseCityAry_origin = $kExport_Import->getForwaderWarehousesCity($idForwarder,$idOriginCountry,$iWarehouseType);

    // getting all warehouse's of this forwarder.
    $forwardWareHouseArr_dest=$kExport_Import->getForwaderWarehouses($idForwarder,$idDestinationCountry,false,false,$iWarehouseType);

    $forwardWareHouseCityAry_dest = $kExport_Import->getForwaderWarehousesCity($idForwarder,$idDestinationCountry,$iWarehouseType); 
    if(!empty($forwardCountriesArr))
    {
        if(count($forwardCountriesArr)==1)
        {
            $modeSelect = "selected=\"selected\"";
            ?>
            <script type="text/javascript">
                show_forwarder_warehouse('<?=$idForwarder?>',<?=$forwardCountriesArr[0]['idCountry']?>,'origin_warehouse_span','ORIGIN_WHS_COUNTRY','<?php echo $iWarehouseType; ?>');
            </script>
            <?php
        }
        else
        {
            $modeSelect = '';
        }
    } 
    $szDestinationCountryDisable = "";
    if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
    {
        $szWarehouseTitle = "Warehouse Name";
    }
    else
    {
        if($szFromPage=='OBO_CUSTOM_CLEARANCE')
        {
            $szWarehouseTitle = "Name";
            $szDestinationCountryDisable = "disabled";
        }
        else
        {
            $szWarehouseTitle = t($t_base.'title/cfs_name'); 
        }  
    }
	
	?>
	<div class="oh">
		<div class="fl-49">
			<p><strong><?=t($t_base.'title/origin');?></strong></p>
			<div class="oh">
                            <div class="fl-33 s-field">
                                <span class="f-size-12"><?=t($t_base.'title/country');?></span>
                                <span id="origin_country_span">
                                    <select name="oboDataExportAry[idOriginCountry]" id="idOriginCountry" onchange="show_forwarder_warehouse('<?=$idForwarder?>',this.value,'origin_warehouse_span','ORIGIN_WHS_COUNTRY','<?php echo $iWarehouseType; ?>')">
                                        <option value=""><?=t($t_base.'fields/select');?></option>
                                        <?php
                                            if(!empty($forwardCountriesArr))
                                            {
                                                foreach($forwardCountriesArr as $forwardCountriesArrs)
                                                {
                                                    ?>
                                                    <option value="<?=$forwardCountriesArrs['idCountry']?>" <?=$modeSelect?>><?=$forwardCountriesArrs['szCountryName']?></option>
                                                    <?php
                                                }
                                            }
                                        ?>
                                    </select>
                                </span> 
                            </div>
                            <div class="fl-33 s-field">
                                <span class="f-size-12"><?=t($t_base.'title/city');?></span>
                                <span id="origin_city_span">
                                    <select id="szOriginCity" name="oboDataExportAry[szOriginCity]" disabled onchange="show_forwarder_warehouse_by_city('<?=$idForwarder?>',this.value,'origin_warehouse_span','ORIGIN_WHS_CITY','<?php echo $iWarehouseType; ?>')">
                                        <option value=""><?=t($t_base.'title/all');?></option>
                                        <?php
                                            if(!empty($forwardWareHouseCityAry_origin))
                                            {
                                                foreach($forwardWareHouseCityAry_origin as $forwardWareHouseCityArys)
                                                {
                                                    ?>
                                                    <option value="<?=$forwardWareHouseCityArys['szCity']?>"><?=$forwardWareHouseCityArys['szCity']?></option>
                                                    <?php
                                                }
                                            }
                                        ?>
                                    </select>
                                </span>
                            </div>
                            <div class="fl-33">
                                <span class="f-size-12"><?php echo $szWarehouseTitle; ?></span>
                                <span id="origin_warehouse_span">
                                    <select name="oboDataExportAry[idOriginWarehouse]" onchange="enable_get_data('<?=t($t_base.'fields/change_data');?>');" disabled id="idOriginWarehouse">
                                        <option value=""><?=t($t_base.'fields/select');?></option>
                                        <?php
                                            if(!empty($forwardWareHouseArr_origin))
                                            {
                                                foreach($forwardWareHouseArr_origin as $forwardWareHouseArrs)
                                                {
                                                    ?>
                                                    <option value="<?=$forwardWareHouseArrs['id']?>"><?=$forwardWareHouseArrs['szWareHouseName']?></option>
                                                    <?php
                                                }
                                            }
                                        ?>
                                    </select>
                                </span>
                            </div>
			</div>
		</div>
		
            <div class="fl-49" style="float:right;">
            <?php
		if(!empty($forwardCountriesArr))
		{
                    if(count($forwardCountriesArr)==1 && $szFromPage!='OBO_CUSTOM_CLEARANCE')
                    {
                        $modeSelect = "selected=\"selected\"";
                        ?>
                        <script type="text/javascript">
                            show_forwarder_warehouse('<?=$idForwarder?>',<?=$forwardCountriesArr[0]['idCountry']?>,'destination_warehouse_span','DESTINATION_WHS_COUNTRY','<?php echo $iWarehouseType; ?>');
                        </script>
                        <?php
                    }
                    else
                    {
                        $modeSelect = ''; 
                    }
		}  
            ?>
            <p><strong><?=t($t_base.'title/destination');?></strong></p>
            <div class="oh">
                <div class="fl-33 s-field">
                    <span class="f-size-12"><?=t($t_base.'title/country');?></span>
                    <span id="destination_country_span">
                        <select name="oboDataExportAry[idDestinationCountry]" <?php echo $szDestinationCountryDisable; ?> id="idDestinationCountry" onchange="show_forwarder_warehouse('<?=$idForwarder?>',this.value,'destination_warehouse_span','DESTINATION_WHS_COUNTRY','<?php echo $iWarehouseType; ?>')">
                            <option value=""><?=t($t_base.'fields/select');?></option>
                            <?php
                                if(!empty($forwardCountriesArr))
                                {
                                    foreach($forwardCountriesArr as $forwardCountriesArrs)
                                    {
                                        ?>
                                        <option value="<?=$forwardCountriesArrs['idCountry']?>" <?=$modeSelect?>><?=$forwardCountriesArrs['szCountryName']?></option>
                                        <?php
                                    }
                                }
                            ?>
                        </select>
                    </span> 
                </div>
                <div class="fl-33 s-field">
                    <span class="f-size-12"><?=t($t_base.'title/city');?></span>
                    <span id="destination_city_span">
                        <select id="szDestinationCity" name="oboDataExportAry[szDestinationCity]" disabled onchange="show_forwarder_warehouse_by_city('<?=$idForwarder?>',this.value,'destination_warehouse_span','DESTINATION_WHS_CITY','<?php echo $iWarehouseType; ?>')">
                            <option value=""><?=t($t_base.'title/all');?></option>
                            <?php
                                if(!empty($forwardWareHouseCityAry_dest))
                                {
                                    foreach($forwardWareHouseCityAry_dest as $forwardWareHouseCityArys)
                                    {
                                        ?>
                                        <option value="<?=$forwardWareHouseCityArys['szCity']?>"><?=$forwardWareHouseCityArys['szCity']?></option>
                                        <?php
                                    }
                                }
                            ?>
                        </select>
                    </span>
                </div>
                <div class="fl-33">
                    <span class="f-size-12"><?=$szWarehouseTitle;?></span>
                    <span id="destination_warehouse_span">
                        <select name="oboDataExportAry[idDestinationWarehouse]" onchange="enable_get_data('<?=t($t_base.'fields/change_data');?>');" disabled id="idDestinationWarehouse">
                            <option value=""><?=t($t_base.'fields/select');?></option>
                            <?php
                                if(!empty($forwardWareHouseArr_dest))
                                {
                                    foreach($forwardWareHouseArr_dest as $forwardWareHouseArrs)
                                    {
                                        ?>
                                        <option value="<?=$forwardWareHouseArrs['id']?>"><?=$forwardWareHouseArrs['szWareHouseName']?></option>
                                        <?php
                                    }
                                }
                            ?>
                        </select>
                    </span>
                </div>
            </div>
        </div>	
        <input type="hidden" name="oboDataExportAry[iWarehouseType]" id="iWarehouseType" value='<?php echo $iWarehouseType; ?>'>
        <input type="hidden" name="oboDataExportAry[szFromPage]" id="szFromPage" value='<?php echo $szFromPage; ?>'> 
	<?php
}

function display_obo_lcl_bottom_html($t_base,$disable_feilds,$pricingWtwAry=array(),$formData=false,$field_name=false)
{	
    $kConfig = new cConfig();
    $frequencyAry=$kConfig->getConfigurationData(__DBC_SCHEMATA_FREQUENCY__);
    $weekDaysAry = $kConfig->getAllWeekDays();
    $cutoffLocalTimeAry = fColumnData();
    $allCurrencyArr=$kConfig->getBookingCurrency(false,false,false,true);
    $cuttOffAry = cutoff_key_value_pair();

    if($disable_feilds)
    {
        $disabled_str = 'disabled="disabled"';
        $opacity_str = 'style="opacity:0.4;"';
    }
    else
    {
        $textmss=t($t_base."title/get_data");
        $textmss1=t($t_base."fields/change_data");
        $save_onclick = ' onclick = "update_obo_lcl_data(\''.$textmss.'\',\''.$textmss1.'\');"';
        $cancel_onclick = ' onclick ="cancel_obo_lcl(\''.(isset($pricingWtwAry["id"])?$pricingWtwAry["id"]:'').'\')"';
        $clear_data_onclick = " onclick ='clear_all_data_obo_lcl()'";
    }
    if(!empty($pricingWtwAry))
    {
        $field_name="editOBOLCLData";
        $display = 'none';
        $submit_button_value = t($t_base.'title/save');
    }
    else
    {
        $field_name="addOBOLCLData";
        $submit_button_value = t($t_base.'title/add');
        $display = 'block';
    } 
    $idOriginWarehouse = $formData['idOriginWarehouse'];
    $idDestinationWarehouse = $formData['idDestinationWarehouse'];

    $em_style="style='font-style:normal;'" ;
    $kWHSSearch = new cWHSSearch();
    $illustration_text = '';
    //$iWarehouseType=__WAREHOUSE_TYPE_CFS__; //Default value for warehouse type
    
    if($formData['idOriginWarehouse']>0)
    {
        $kWHSSearch->load($formData['idOriginWarehouse']);
        $iWarehouseType = $kWHSSearch->iWarehouseType; 
        
        $illustration_text .= " from ".$kWHSSearch->szWareHouseName ;
        if(strlen($kWHSSearch->szWareHouseName)>30)
        {
            $szOriginWhsName = mb_substr($kWHSSearch->szWareHouseName,0,27,'UTF8')."..";
        }
        else
        {
            $szOriginWhsName = $kWHSSearch->szWareHouseName ;
        }
    }

    if($formData['idDestinationWarehouse']>0)
    {
        $kWHSSearch->load($formData['idDestinationWarehouse']);
        if($iWarehouseType<=0)
        {
            $iWarehouseType = $kWHSSearch->iWarehouseType; 
        }
        $illustration_text .= " to ".$kWHSSearch->szWareHouseName ;
        if(strlen($kWHSSearch->szWareHouseName)>30)
        {
            $szDestinationWhsName = mb_substr($kWHSSearch->szWareHouseName,0,27,'UTF8').'..';
        }
        else
        {
            $szDestinationWhsName = $kWHSSearch->szWareHouseName ;
        }
    }

    if($pricingWtwAry['iOriginChargesApplicable']!=1)
    {
        $pricingWtwAry['fOriginRateWM'] = (float)$pricingWtwAry['fOriginRateWM']>0?$pricingWtwAry['fOriginRateWM']:'';
        $pricingWtwAry['fOriginMinRateWM'] = (float)$pricingWtwAry['fOriginMinRateWM']>0?$pricingWtwAry['fOriginMinRateWM']:'';
        $pricingWtwAry['fOriginRate'] = (float)$pricingWtwAry['fOriginRate']>0?$pricingWtwAry['fOriginRate']:'';
    }	
    if($pricingWtwAry['iDestinationChargesApplicable']!=1)
    {
        $pricingWtwAry['fDestinationRateWM'] = (float)$pricingWtwAry['fDestinationRateWM']>0?$pricingWtwAry['fDestinationRateWM']:'';
        $pricingWtwAry['fDestinationMinRateWM'] = (float)$pricingWtwAry['fDestinationMinRateWM']>0?$pricingWtwAry['fDestinationMinRateWM']:'';
        $pricingWtwAry['fDestinationRate'] = (float)$pricingWtwAry['fDestinationRate']>0?$pricingWtwAry['fDestinationRate']:'';
    }

    if($pricingWtwAry['dtValidFrom']!="" && $pricingWtwAry['dtValidFrom']!="0000-00-00  00:00:00" && $pricingWtwAry['dtValidFrom']!="0000-00-00")
    {
        $todayDateTime=strtotime(date("Y-m-d"));
        if($todayDateTime>strtotime($pricingWtwAry['dtValidFrom']))
        {
            $pricingWtwAry['dtValidFrom']=date("Y-m-d");
        }            
    }
    $date_picker_argument = "minDate: new Date(".date('Y',$minDateTime).", ".date('m',$minDateTime)."-1, ".date('d',$minDateTime).")" ;
    
    if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
    {
        $szPerWM = "Per KG";
        $szOriginChargeTipHeading = "";
        $szOriginChargeTipHeading = t($t_base.'messages/AIR_FREIGHT_ORIGIN_CHARGES_TOOL_TIP_HEADER_TEXT');
        $szFreightChargeTipHeading = t($t_base.'messages/AIR_FREIGHT_RATE_TOOL_TIP_HEADER_TEXT'); 
        $szDestinationChargeTipHeading = t($t_base.'messages/AIR_DESTINATION_CHARGES_TOOL_TIP_HEADER_TEXT'); 
        $szFrequencyTipHeading = t($t_base.'messages/AIR_SERVICE_FREQUENCY_TOOL_TIP_HEADER_TEXT'); 
        $szFrequencyTipText = t($t_base.'messages/AIR_SERVICE_FREQUENCY_TOOL_TIP_TEXT');
        $szCutOffTitle = t($t_base.'fields/airpot_cut_off_day');  
        $szCutoffTipHeading = t($t_base.'messages/AIRPORT_CUT_OFF_TOOL_TIP_HEADER_TEXT');
        $szTransitDaysTitle = t($t_base.'fields/airport_transit_hour'); 
        $szTransitDaysTipHeading = t($t_base.'messages/AIRPORT_TRANSIT_TOOL_TIP_HEADER_TEXT');
        $szAvailabelDaysTitle = t($t_base.'fields/airport_available_day'); 
        $szImageName = "CargoFlowAirfreight.png"; 
        $szAvailableTipHeading = t($t_base.'messages/AIR_CFS_AVAILABLE_TOOL_TIP_HEADER_TEXT');
        $szIllustrationTitle = t($t_base.'title/illustration_of_air_service');
    }
    else
    {
        $szPerWM = t($t_base.'fields/per_wm'); 
        $szOriginChargeTipHeading = t($t_base.'messages/ORIGIN_CHARGES_TOOL_TIP_HEADER_TEXT');
        $szFreightChargeTipHeading = t($t_base.'messages/FRAIGHT_RATE_TOOL_TIP_HEADER_TEXT'); 
        $szDestinationChargeTipHeading = t($t_base.'messages/DESTINATION_CHARGES_TOOL_TIP_HEADER_TEXT'); 
        $szFrequencyTipHeading = t($t_base.'messages/SERVICE_FREQUENCY_TOOL_TIP_HEADER_TEXT'); 
        $szFrequencyTipText = "";
        $szCutOffTitle = t($t_base.'fields/cut_off_day'); 
        $szCutoffTipHeading = t($t_base.'messages/CFS_CUT_OFF_TOOL_TIP_HEADER_TEXT');
        
        $szTransitDaysTitle = t($t_base.'fields/transit_hour'); 
        $szTransitDaysTipHeading = t($t_base.'messages/CFS_TRANSIT_TOOL_TIP_HEADER_TEXT');
        $szAvailabelDaysTitle = t($t_base.'fields/available_day');  
        
        $szAvailableTipHeading = t($t_base.'messages/CFS_AVAILABLE_TOOL_TIP_HEADER_TEXT');
        $szIllustrationTitle = t($t_base.'title/illustration_of_lcl');
        $szImageName = "CargoFlowLCL.png"; 
    } 
?>	
<script type="text/javascript">
    function initDatePicker() 
    {
        $("#datepicker1").datepicker({'minDate': '0'});
        $("#datepicker2").datepicker({'minDate': '0'});
    }
    initDatePicker();
</script>
<form name="obo_lcl_bottom_form" id="obo_lcl_bottom_form" method="post">
    <div id="obo_cc_no_record_found" style="display:none;">
        <ul id="message_ul" style="padding:5px 5px 5px 35px;">
            <li><?=t($t_base.'title/obo_lcl_no_service_found');?></li>
        </ul>
    </div>	
	
	<!-- 
	<div id="pointer_div" onclick="point_it(event)">
	<img src="<?=__BASE_STORE_IMAGE_URL__?>/CargoFlowLCL.png" id="cross" >
	</div>
	 -->
	<br/>
	<div id="obo_lcl_error"  class="errorBox" style="display:none;"></div>
	<br/>
	<div class="oh">
            <p class="fl-40 gap-text-field"><?=t($t_base.'fields/origin_freight_rate');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=$szOriginChargeTipHeading;?>','<?=t($t_base.'messages/ORIGIN_CHARGES_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
            <p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=$szPerWM;?></span><br /><input <?=$disabled_str?> name="<?=$field_name?>[fOriginRateWM]" id="fOriginRateWM" value="<?=$pricingWtwAry['fOriginRateWM']?>" type="text" onkeyup="check_applicable_charges_checkbox('ORIGIN');" size="10" /></p>
            <p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/minimum');?></span><br /><input <?=$disabled_str?> type="text" name="<?=$field_name?>[fOriginMinRateWM]" id="fOriginMinRateWM" value="<?=$pricingWtwAry['fOriginMinRateWM']?>" onkeyup="check_applicable_charges_checkbox('ORIGIN');" size="10" /></p>
            <p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/per_booking');?></span><br /><input <?=$disabled_str?> type="text" name="<?=$field_name?>[fOriginRate]" id="fOriginRate" value="<?=$pricingWtwAry['fOriginRate']?>" size="10" onkeyup="check_applicable_charges_checkbox('ORIGIN');" /></p>
            <p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/currency');?></span><br />
                <select size="1" <?=$disabled_str?> name="<?=$field_name?>[szOriginFreightCurrency]" id="szOriginFreightCurrency" onchange="check_applicable_charges_checkbox('ORIGIN');" >
                    <option value=""><?=t($t_base.'fields/select');?></option>
                    <?php
                        if(!empty($allCurrencyArr))
                        {
                            foreach($allCurrencyArr as $allCurrencyArrs)
                            {
                                ?><option value="<?=$allCurrencyArrs['id']?>" <?=($pricingWtwAry['szOriginRateCurrency'] ==  $allCurrencyArrs['id'] ) ? "selected":""?>><?=$allCurrencyArrs['szCurrency']?></option><?php
                            }
                        }
                    ?>
                </select>
            </p>
            <input name="<?=$field_name?>[iOriginChargesApplicable]" id="iOriginChargesApplicable" value="<?=$pricingWtwAry['iDestinationChargesApplicable']?>" type="hidden" />
	</div>
	<div class="oh">
            <p class="fl-40 gap-text-field"><?=t($t_base.'fields/freight_rate');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=$szFreightChargeTipHeading;?>','<?=t($t_base.'messages/FRAIGHT_RATE_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
            <p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=$szPerWM;?></span><br /><input <?=$disabled_str?> name="<?=$field_name?>[fRateWM]" id="fRateWM" value="<?=(isset($pricingWtwAry['fRateWM'])?$pricingWtwAry['fRateWM']:'')?>" type="text" size="10" /></p>
            <p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/minimum');?></span><br /><input <?=$disabled_str?> type="text" name="<?=$field_name?>[fMinRateWM]" id="fMinRateWM" value="<?=(isset($pricingWtwAry['fMinRateWM'])?$pricingWtwAry['fMinRateWM']:'')?>" size="10" /></p>
            <p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/per_booking');?></span><br /><input <?=$disabled_str?> type="text" name="<?=$field_name?>[fRate]" id="fRate" value="<?=(isset($pricingWtwAry['fRate'])?$pricingWtwAry['fRate']:'')?>" size="10" /></p>
            <p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/currency');?></span><br />
                <select size="1" <?=$disabled_str?> name="<?=$field_name?>[szFreightCurrency]" id="szFreightCurrency" >
                    <option value=""><?=t($t_base.'fields/select');?></option>
                    <?php
                        if(!empty($allCurrencyArr))
                        {
                            foreach($allCurrencyArr as $allCurrencyArrs)
                            {
                                ?><option value="<?=$allCurrencyArrs['id']?>" <?=($pricingWtwAry['szFreightCurrency'] ==  $allCurrencyArrs['id'] ) ? "selected":""?>><?=$allCurrencyArrs['szCurrency']?></option><?php
                            }
                        }
                    ?>
                </select>
            </p>
	</div>
	<div class="oh">
            <p class="fl-40 gap-text-field"><?=t($t_base.'fields/destination_freight_rate');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=$szDestinationChargeTipHeading;?>','<?=t($t_base.'messages/DESTINATION_CHARGES_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
            <p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=$szPerWM;?></span><br /><input <?=$disabled_str?> name="<?=$field_name?>[fDestinationRateWM]" id="fDestinationRateWM" value="<?=$pricingWtwAry['fDestinationRateWM']?>" type="text" size="10" onkeyup="check_applicable_charges_checkbox('DESTINATION');" /></p>
            <p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/minimum');?></span><br /><input <?=$disabled_str?> type="text" name="<?=$field_name?>[fDestinationMinRateWM]" id="fDestinationMinRateWM" value="<?=$pricingWtwAry['fDestinationMinRateWM']?>" size="10" onkeyup="check_applicable_charges_checkbox('DESTINATION');" /></p>
            <p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/per_booking');?></span><br /><input <?=$disabled_str?> type="text" name="<?=$field_name?>[fDestinationRate]" id="fDestinationRate" value="<?=$pricingWtwAry['fDestinationRate']?>" size="10" onkeyup="check_applicable_charges_checkbox('DESTINATION');" /></p>
            <p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/currency');?></span><br />
                <select size="1" <?=$disabled_str?> name="<?=$field_name?>[szDestinationFreightCurrency]" id="szDestinationFreightCurrency" onchange = "check_applicable_charges_checkbox('DESTINATION');">
                    <option value=""><?=t($t_base.'fields/select');?></option>
                    <?php
                        if(!empty($allCurrencyArr))
                        {
                            foreach($allCurrencyArr as $allCurrencyArrs)
                            {
                                ?><option value="<?=$allCurrencyArrs['id']?>" <?=($pricingWtwAry['szDestinationRateCurrency'] ==  $allCurrencyArrs['id'] ) ? "selected":""?>><?=$allCurrencyArrs['szCurrency']?></option><?php
                            }
                        }
                    ?>
                </select>
            </p>
            <input name="<?=$field_name?>[iDestinationChargesApplicable]" id="iDestinationChargesApplicable" value="<?=$pricingWtwAry['iDestinationChargesApplicable']?>" type="hidden" />
	</div>
	<div class="oh">
            <p class="fl-40 gap-text-field"><?=t($t_base.'fields/validity');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/VALIDITY_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/VALIDITY_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
            <p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/from');?></span><br /><input size="10" <?=$disabled_str?> id="datepicker1" name="<?=$field_name?>[dtValidFrom]" type="text" value="<? if($pricingWtwAry['dtValidFrom']!='0000-00-00 00:00:00' && !empty($pricingWtwAry['dtValidFrom'])) {?><?=date('d/m/Y',strtotime($pricingWtwAry['dtValidFrom']))?><? }?>"></p>
            <p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/to');?></span><br /><input size="10" <?=$disabled_str?> id="datepicker2" name="<?=$field_name?>[dtExpiry]" type="text" value="<? if($pricingWtwAry['dtExpiry']!='0000-00-00 00:00:00' && !empty($pricingWtwAry['dtExpiry'])) {?><?=date('d/m/Y',strtotime($pricingWtwAry['dtExpiry']))?><? }?>"></p>
	</div>
	<div class="oh">
            <p class="fl-40 gap-text-field"><?=t($t_base.'fields/booking_cut_off');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/BOOKING_CUT_OFF_TOOL_TIP_HEADER_TEXT');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
            <p class="fl-20"><span class="f-size-12">&nbsp;</span><br />
                <select size="1" style="width:90px;" <?=$disabled_str?> name="<?=$field_name?>[iBookingCutOffHours]" id="iBookingCutOffHours" >
                    <option value=""><?=t($t_base.'fields/select');?></option>
                    <?php
                        if(!empty($cuttOffAry))
                        {
                            foreach($cuttOffAry as $key=>$cuttOffArys)
                            {
                                ?><option value="<?=$key?>" <?=($pricingWtwAry['iBookingCutOffHours'] ==  $key ) ? "selected":""?>><?=$cuttOffArys?></option><?php
                            }
                        }
                    ?>
                </select>
            </p>
	</div>
	<div class="oh">
		<p class="fl-40 gap-text-field"><?=t($t_base.'fields/service_frequency');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=$szFrequencyTipHeading;?>','<?php echo $szFrequencyTipText;?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
		<p class="fl-20"><span class="f-size-12">&nbsp;</span><br />
			<select size="1" style="width:90px;" <?=$disabled_str?> name="<?=$field_name?>[idFrequency]" id="idFrequency" >
				<option value=""><?=t($t_base.'fields/select');?></option>
				<?php
					if(!empty($frequencyAry))
					{
						foreach($frequencyAry as $frequencyArys)
						{
							?><option value="<?=$frequencyArys['id']?>" <?=($pricingWtwAry['idFrequency'] == $frequencyArys['id'] ) ? "selected":""?>><?=$frequencyArys['szDescription']?></option>
							<?php
						}
					}
				?>
			</select>
		</p>
	</div>
	<div class="oh">
		<p class="fl-40 gap-text-field"><?=$szCutOffTitle; ?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=$szCutoffTipHeading;?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
		<p class="fl-15" ><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/weekday');?></span><br />
                    <select size="1" style="width:90px;" <?=$disabled_str?> name="<?=$field_name?>[idCutOffDay]" onchange="getAvailableDay();" id="idCutOffDay" >
                        <option value=""><?=t($t_base.'fields/select');?></option>
			<?php
                            if(!empty($weekDaysAry))
                            {
                                foreach($weekDaysAry as $weekDaysArys)
                                {
                                    ?><option value="<?=$weekDaysArys['id']?>" <?=($pricingWtwAry['idCutOffDay'] ==  $weekDaysArys['id'] ) ? "selected":""?>><?=$weekDaysArys['szWeekDay']?></option><?php
                                }
                            }
			?>
                    </select>
		</p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/local_time');?></span><br />
			<select size="1" style="width:90px;" <?=$disabled_str?> name="<?=$field_name?>[szCutOffLocalTime]" id="szCutOffLocalTime" >
			<option value=""><?=t($t_base.'fields/select');?></option>
			<?php
				if(!empty($cutoffLocalTimeAry))
				{
					foreach($cutoffLocalTimeAry as $cutoffLocalTimeArys)
					{
						?><option value="<?=$cutoffLocalTimeArys?>" <?=(trim($pricingWtwAry['szCutOffLocalTime']) ==  $cutoffLocalTimeArys) ? "selected":""?>><?=$cutoffLocalTimeArys?></option>
						<?php
					}
				}
			?>
			</select>
		</p>
	</div>
	<div class="oh">
            <p class="fl-40 gap-text-field"><?php echo $szTransitDaysTitle; ?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=$szTransitDaysTipHeading; ?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
            <p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/days');?></span><br /><input <?=$disabled_str?> id="iTransitHours"  name="<?=$field_name?>[iTransitHours]" value="<?=$pricingWtwAry['iTransitDays']?>" onblur="getAvailableDay();" type="text" size="10" /></p>
	</div>
	<div class="oh">
		<p class="fl-40 gap-text-field"><?php echo $szAvailabelDaysTitle; ?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=$szAvailableTipHeading;?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
		<p class="fl-15" ><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/weekday');?></span><br />
			<input type="text" size="10" disabled name="<?=$field_name?>[idAvailableDay_dis]" value="<?=$pricingWtwAry['szAvailableDay']?>" id="idAvailableDay" />			
			<input type="hidden" name="<?=$field_name?>[idAvailableDay]" id="idAvailableDay_hidden" value="<?=$pricingWtwAry['idAvailableDay']?>" />
		</p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/local_time');?></span><br />
			<select size="1" style="width:90px;" <?=$disabled_str?> name="<?=$field_name?>[szAvailableLocalTime]" id="szAvailableLocalTime" >
			<option value=""><?=t($t_base.'fields/select');?></option>
			<?php
				if(!empty($cutoffLocalTimeAry))
				{
					foreach($cutoffLocalTimeAry as $cutoffLocalTimeArys)
					{
						?><option value="<?=$cutoffLocalTimeArys?>" <?=($pricingWtwAry['szAvailableLocalTime'] ==  $cutoffLocalTimeArys) ? "selected":""?>><?=$cutoffLocalTimeArys?></option>
						<?php
					}
				}
			?>
			</select>
		</p>
	</div>
	
	<br />
	<div class="oh">
		<p class="fl-40">
			<a href="javascript:void(0);" class="button2" <?=$cancel_onclick?> <?=$opacity_str?>><span><?=t($t_base.'title/cancel');?></span></a> 
			<a href="javascript:void(0);" <?=$clear_data_onclick?> class="button2" <?=$opacity_str?>><span><?=t($t_base.'title/clear_data');?></span></a></p>
		<p class="fl-40" align="right" valign="middle"  style="padding-top:5px"><?=t($t_base.'title/click_to_update_changes');?></p>
		<p class="fl-20" align="right"><a href="javascript:void(0);" <?=$save_onclick?> <?=$opacity_str?> class="button1" id="save_lcl"><span><?=$submit_button_value?></span></a></p>
	</div>
	<input type="hidden" name="<?=$field_name?>[idOriginWarehouse]" id="idOriginWarehouse_hidden" value="<?=$idOriginWarehouse?>">
	<input type="hidden" name="<?=$field_name?>[idDestinationWarehouse]" id="idDestinationWarehouse_hidden" value="<?=$idDestinationWarehouse?>">
	<input type="hidden" name="<?=$field_name?>[id]" value="<?=$pricingWtwAry['id']?>">
</form>	
<hr>
<h5><?=$szIllustrationTitle;?></h5>
<p><img width="100%" src="<?=__BASE_STORE_IMAGE_URL__.'/'.$szImageName; ?>"></p> 
<script type="text/javascript">
    var availabelDayAry = new Array();
    <?php
        if(!empty($weekDaysAry))
        {
            foreach($weekDaysAry as $weekDaysArys)
            {
                ?>
                availabelDayAry[<?php echo $weekDaysArys['id']; ?>] = '<?php echo $weekDaysArys['szWeekDay']; ?>'
                <?php
            }
        }
    ?> 
        function getAvailableDay()
        {
            var iTransitHours = $("#iTransitHours").attr('value');
            var idCutOffDay = $("#idCutOffDay").attr('value');
            iTransitHours = parseInt(iTransitHours);
            idCutOffDay = parseInt(idCutOffDay);
            if(idCutOffDay>0 && iTransitHours>0)
            {
                var rawDayId = ((((iTransitHours/7) - Math.floor(iTransitHours/7)) *7) + 0.0001) ;
                var dayid = Math.floor(rawDayId);

                if(idCutOffDay > 7)
                {
                    var AvailableDay = (dayid + idCutOffDay) - 7 ;
                }
                else
                {
                    var AvailableDay = (dayid + idCutOffDay) ;
                } 
                if(AvailableDay>7)
                {
                    AvailableDay = AvailableDay-7 ;
                }
                $("#idAvailableDay_hidden").attr('value',AvailableDay);
                $("#idAvailableDay").attr('value',availabelDayAry[AvailableDay]);
                console.log("Available Day: "+AvailableDay+ " Days: "+availabelDayAry[AvailableDay]);
            }
        }
</script>
	<?php
}

function forwarder_booking_search_form($t_base)
{	
    $idForwarder = $_SESSION['forwarder_id'];
    $kBooking = new cBooking();
    $bookingYearAry = $kBooking->getAllForwarderBookingYear($idForwarder);
    $bookingMonthAry = month_key_value_pair();
   
    ?>
    <script type="text/javascript">
        function initDatePicker() 
        {
            $("#datepicker1").datepicker();
            $("#datepicker2").datepicker();
        }
        initDatePicker();
    </script>
<form action="" id="forwarder_booking_seach_form" method="post">
	
		<div class="oh">
		<p class="fl-5"  style="padding-top:16px;"><?=t($t_base.'title/select');?></p>
		<p class="fl-10"><span class="f-size-12"><?=t($t_base.'title/year');?></span><br />
		<span id="origin_country_span">
                    <select id="iBookingYear" name="bookingSearchAry[iBookingYear]">
                    <option value=""><?=t($t_base.'title/all');?></option>
                    <?php
                        if(!empty($bookingYearAry))
                        {
                            foreach($bookingYearAry as $bookingYearArys)
                            {
                                ?>
                                <option value="<?=$bookingYearArys['iYear']?>" <?php echo (($bookingYearArys['iYear']==date('Y'))?'selected':''); ?>><?=$bookingYearArys['iYear']?></option>
                                <?
                            }
                        }
                    ?>
                    </select>
		</span>
		</p> 
		<p class="fl-10"><span class="f-size-12"><?=t($t_base.'title/month');?></span><br />
		<span id="origin_city_span">
		<select id="iBookingMonth" name="bookingSearchAry[iBookingMonth]">
			<option value=""><?=t($t_base.'title/all');?></option>
			<?php
                            if(!empty($bookingMonthAry))
                            {
                                foreach($bookingMonthAry as $key=>$bookingMonthArys)
                                { 
                                    ?>
                                    <option value="<?=$key?>" <?php echo (((int)$key==(int)date('m'))?'selected':''); ?>> <?=$bookingMonthArys?></option>
                                    <?php
                                }
                            }
			?>
		</select>
		</span>
		</p>			
			
		<p class="fl-10">
			<span class="f-size-12"><?=t($t_base.'title/from_date');?></span><br />
			<span id="origin_warehouse_span">
				<input type="text" id="datepicker1" name="bookingSearchAry[dtFromDate]" value="<?=$post_data['dtFromDate']?>">
			</span>
		</p>	
		
		<p class="fl-10">
			<span class="f-size-12"><?=t($t_base.'title/to_date');?></span><br />
			<span id="origin_warehouse_span">
				<input type="text" id="datepicker2" name="bookingSearchAry[dtToDate]" value="<?=$post_data['dtToDate']?>">
			</span>
		</p>	
				
		<p class="fl-10">
			<span class="f-size-12"><?=t($t_base.'title/booking_no');?></span><br />
			<span id="origin_warehouse_span">
				<input type="text" name="bookingSearchAry[szBookingRef]" value="<?=$post_data['szBookingRef']?>">
			</span>
		</p>
		
		<p class="fl-10">
			<span class="f-size-12"><?=t($t_base.'title/your_reference');?></span><br />
			<span id="origin_warehouse_span">
				<input type="text" name="bookingSearchAry[szForwarderReference]" value="<?=$post_data['szInvoiceComment']?>">
			</span>
		</p>	
			
		<p class="fl-10">
			<span class="f-size-12"><?=t($t_base.'title/invoice_no');?></span><br />
			<span id="origin_warehouse_span">
				<input type="text" name="bookingSearchAry[szInvoice]" value="<?=$post_data['szInvoice']?>">
			</span>
		</p>	
		
		<p class="fr-20" align="right">
			<a href="javascript:void(0)" onclick="serch_forwarder_booking()" class="button1"><span><?=t($t_base.'fields/go');?></span></a>
			<a href="javascript:void(0)" onclick="clear_booking_form()" class="button2"><span><?=t($t_base.'fields/clear');?></span></a>
		</p>
	</div>	
</form>	
	<?php
}

function forwarder_booking_bottom_html($searchResult,$t_base)  
{	
	//$table_width = round((float)(100/11),2) ;
	$table_width = (int)(100/11);
        
	?>
	<br/>
    <table cellpadding="0" id="booking_table" cellspacing="0" class="format-4" width="100%">
        <tr>
            <th width="<?=$table_width?>%" valign="top">				
                <a href="javascript:void(0);" style="text-decoration:none;"  onclick="sort_booking_table('b.dtBookingConfirmed','DESC','booking_date')" id="booking_date">
                        <span class="sort-arrow-down">&nbsp;</span>
                </a>
                <?=t($t_base.'fields/date')?>
            </th>
            <th width="<?=$table_width?>%" valign="top">			
                <a href="javascript:void(0);" style="text-decoration:none;"  onclick="sort_booking_table('b.szBookingRef','ASC','booking_no')" id="booking_no">
                        <span class="sort-arrow-up-grey">&nbsp;</span>
                </a>
                <?=t($t_base.'fields/booking')?>
            </th>
            <th width="<?=$table_width+1?>%" valign="top">				
                <a href="javascript:void(0);" style="text-decoration:none;" onclick="sort_booking_table('b.dtCutOff','ASC','cargo_date')" id="cargo_date">
                        <span class="sort-arrow-up-grey">&nbsp;</span>
                </a>
                <?=t($t_base.'fields/cargo');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/CARGO_RECEIVING_TOOL_TIP_TEXT');?>','<?=t($t_base.'messages/CARGO_RECEIVING_TOOL_TIP_TEXT_TWO');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a>
            </th>
            <th width="<?=$table_width?>%" valign="top"><a href="javascript:void(0)" onclick="open_service_form_booking_screen();"><?=t($t_base.'fields/service');?></a><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/SERVICE_FROM_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/SERVICE_FROM_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></th>
            <th width="<?=$table_width*2-4 ?>%" valign="top"><?=t($t_base.'fields/from');?>&nbsp;<?=t($t_base.'fields/city');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/CITY_FROM_TOOL_TIP_HEADER_TEXT');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></th>
            <th width="<?=$table_width*2-4 ?>%" valign="top"><?=t($t_base.'fields/to');?>&nbsp;<?=t($t_base.'fields/city');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/CITY_TO_TOOL_TIP_HEADER_TEXT');?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')"></a></th>
            <th width="<?=$table_width+7?>%" valign="top">				
                <a href="javascript:void(0);" style="text-decoration:none;" onclick="sort_booking_table('b.szCustomerCompanyName','ASC','cfs_name')" id="cfs_name">
                        <span class="sort-arrow-up-grey">&nbsp;</span>
                </a>
                <?=t($t_base.'fields/customer');?>
            </th>
            <th width="<?=$table_width-2?>%" valign="top"><?=t($t_base.'fields/weight');?> / <?=t($t_base.'fields/volume');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/WEIGHT_VOLUME_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/WEIGHT_VOLUME_TOOL_TIP_HEADER_TEXT_DETAILS');?>','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')"></a></th>
            <th width="<?=$table_width+2?>%" valign="top"><?=t($t_base.'fields/reference');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/YOUR_REFERENCE_TOOL_TIP_HEADER_TEXT');?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')"></a></th>
        </tr>
            <?php
                if(!empty($searchResult))
                {
                    $ctr = 1;
                    $kConfig = new cConfig();
                    $transportModeListAry = array();
                    $transportModeListAry = $kConfig->getAllTransportMode(false,false,true);
                    $z=0;
                    foreach($searchResult as $searchResults)
                    { 
                        if($searchResults['iQuotesStatus']==__BOOKING_QUOTES_STATUS_WON__ || $searchResults['iCourierBooking']==1)
                        {
                            $szWarehouseFromCity=createSubString($searchResults['szShipperCity'],0,10);
                            $szWarehouseToCity=createSubString($searchResults['szConsigneeCity'],0,10); 
                            $idWarehouseFromCountry = $searchResults['idOriginCountry'] ;
                            $idWarehouseToCountry = $searchResults['idDestinationCountry'] ;

                            $szCuttOffDate = "TBD";
                            $szServiceType = $searchResults['szTransportMode'];
                             
                        }
                        else
                        {  
                            $szWarehouseFromCity=createSubString($searchResults['szWarehouseFromCity'],0,10);  
                            $szWarehouseToCity=createSubString($searchResults['szWarehouseToCity'],0,10); 
                            $idWarehouseFromCountry = $searchResults['idWarehouseFromCountry'] ;
                            $idWarehouseToCountry = $searchResults['idWarehouseToCountry'] ;
                            if((!empty($searchResults['dtCutOff'])  && $searchResults['dtCutOff']!='0000-00-00 00:00:00'))
                            {
                                $szCuttOffDate = date('d/m/Y',strtotime($searchResults['dtCutOff']));
                            }
                            else
                            {
                                $szCuttOffDate = '';
                            }
                            $szServiceType='';
                            if($searchResults['idServiceType']== __SERVICE_TYPE_DTD__) //DTD
                            {
                                    $szServiceType = "Door-Door" ;
                            }
                            elseif($searchResults['idServiceType']==__SERVICE_TYPE_DTW__) //DTW
                            {
                                    $szServiceType = "Door-CFS" ;
                            }
                            elseif($searchResults['idServiceType']==__SERVICE_TYPE_WTD__) //WTD
                            {
                                    $szServiceType = "CFS-Door" ;
                            }
                            elseif($searchResults['idServiceType']==__SERVICE_TYPE_WTW__) //WTW
                            {
                                    $szServiceType = "CFS-CFS" ;
                            }
                            elseif($searchResults['idServiceType']== __SERVICE_TYPE_PTP__) //DTD
                            {
                                    $szServiceType = "Port-Port" ;
                            }
                            elseif($searchResults['idServiceType']==__SERVICE_TYPE_DTP__) //DTW
                            {
                                    $szServiceType = "Door-Port" ;
                            }
                            elseif($searchResults['idServiceType']==__SERVICE_TYPE_PTD__) //WTD
                            {
                                    $szServiceType = "Port-Door" ;
                            }
                            elseif($searchResults['idServiceType']==__SERVICE_TYPE_WTP__) //WTW
                            {
                                    $szServiceType = "CFS-Port" ;
                            }
                            elseif($searchResults['idServiceType']==__SERVICE_TYPE_PTW__) //WTD
                            {
                                    $szServiceType = "Port-CFS" ;
                            } 
                        }
                        if($searchResults['idTransportMode']>0)
                        {
                            $szServiceType = $transportModeListAry[$searchResults['idTransportMode']]['szShortName'] ; 
                        }
                        else
                        {
                            $szServiceType = __TRANSPORTECA_DEFAULT_TRANSPORT_MODE__;
                        }
                        if($searchResults['iBookingType']==3)//Courier Booking
                        {
                            if(!empty($searchResults['dtLabelDownloaded']) && $searchResults['dtLabelDownloaded']!='0000-00-00 00:00:00')
                            {                           
                                $szCuttOffDate = date('d/m/Y',strtotime($searchResults['dtLabelDownloaded']));
                            }
                        }
                        else if($searchResults['iBookingType']==2)//Manual Booking
                        {
                            
                            if(!empty($searchResults['dtTimingDate']) && $searchResults['dtTimingDate']!='0000-00-00 00:00:00')
                            {
                                $szCuttOffDate = date('d/m/Y',strtotime($searchResults['dtTimingDate']));
                            }
                            if($szServiceType=='Courier')
                            {
                                if(!empty($searchResults['dtLabelDownloaded']) && $searchResults['dtLabelDownloaded']!='0000-00-00 00:00:00')
                                {                           
                                    $szCuttOffDate = date('d/m/Y',strtotime($searchResults['dtLabelDownloaded']));
                                }
                                
                            }
                        }
                        $fiveBackDate=date('Y-m-d',strtotime('-5 DAY'));
                        $fiveDateAckFlag=false;
                        if(!empty($searchResults['dtBookingConfirmed']) && $searchResults['dtBookingConfirmed']!='0000-00-00 00:00:00')
                        {
                            $date = date('d/m/Y',strtotime($searchResults['dtBookingConfirmed']));
                            $dtBookingConfirmedCal = date('Y-m-d',strtotime($searchResults['dtBookingConfirmed']));
                           
                            if(strtotime($dtBookingConfirmedCal)>strtotime($fiveBackDate))
                            {
                                $fiveDateAckFlag=true;
                            }   
                        }
                        else
                        {
                            $date = '';
                        }
                        
                        
                        if(!empty($searchResults['szCustomerCompanyName']))
                        {
                            $szCustomerCompanyName = $searchResults['szCustomerCompanyName'];
                        }
                        else
                        {
                            $szCustomerCompanyName = $searchResults['szFirstName']." ".$searchResults['szLastName'];
                        }
                        $iBookingAcknowledge = $searchResults['iBookingAcknowledge']; 
                        if($iBookingAcknowledge==1 || $searchResults['iCourierBooking']==1)
                        {
                            $szBoldClass = '';
                        }
                        else
                        {
                            $szBoldClass = ' class="bold-text" ';
                        }
                        $iHidePriceFlag=0;
                        if($searchResults['iHandlingFeeApplicable']==1 || (float)$searchResults['fTotalForwarderManualFee']>0.00)
                        {
                            $iHidePriceFlag=1;
                        }
                        ?>						
                        <tr id="booking_data_<?=$ctr?>" onclick="select_forwarder_booking_tr('booking_data_<?=$ctr?>','<?=$searchResults['id']?>','<?=$iHidePriceFlag?>')" <?php echo $szBoldClass; ?>>
                            <td><?php echo $date;?></td>
                            <td><?=$searchResults['szBookingRef']?></td>
                            <td><?=$szCuttOffDate?></td> 
                            <td><?=$szServiceType?></td> 
                            <!--<td><a href="javascript:void(0)" onclick="open_service_form_booking_screen();" style="text-decoration: none;color:#000;font-style:inherit;"><?=$szServiceType?></a></td> -->
                            
                            <td><?=$szWarehouseFromCity." (".isoidCountryCode($idWarehouseFromCountry).")"?></td>
                            <td><?=$szWarehouseToCity." (".isoidCountryCode($idWarehouseToCountry).")"?></td>
                            <td><?=createSubString($szCustomerCompanyName,0,20)?></td>
                            <td><?=(get_formated_cargo_measure($searchResults['fCargoWeight']/1000)) ." / ".format_volume($searchResults['fCargoVolume'])?></td>
                            <td><span id="invoice_comment_<?=$searchResults['id']?>"><?=createSubString($searchResults['szForwarderReference'],0,12)?></span><br />
                                    <a href="javascript:void(0)" onclick="update_invoice_comment('<?=$searchResults['id']?>','<?=$searchResults['szBookingRef']?>')" style="text-decoration:none;font-size:11px;"><img src="<?=__BASE_STORE_IMAGE_URL__?>/Icon - Edit line.png" alt = "Click to update" style="float: right;margin-top:-17px;"></a>
                            </td>
                        </tr>
                        <?php
                        $ctr++;
                    }
                }
                else
                {
                    ?>
                    <tr>
                        <td colspan="11" align="center" style='padding:5px 5px 5px 5px; font-style:bold;'><?=t($t_base.'messages/NO_BOOKING_FOUND');?></td>
                    </tr>
                    <?php
                }
            ?>
    </table>
    <div id="invoice_comment_div" style="display:none">
    </div>
    <br>
    <div align="right">
            <?=t($t_base.'title/page_bottom_title');?>
            <a href="javascript:void(0)" style="opacity:0.4" id="download_booking" class="button1"><span><?=t($t_base.'fields/download_booking');?></span></a>
            &nbsp;<a href="javascript:void(0)" id="download_table" onclick="$('#forwarder_booking_seach_form').submit()" class="button1"><span><?=t($t_base.'fields/download_table');?></span></a>
    </div>
    <?php
}
function display_company_information_edit_form($t_base,$kForwarder)
{
	$idForwarder = $kForwarder->id ;
	$kConfig = new cConfig();
	$iLanguage = getLanguageId();
	$allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);
?>
<form name="updateRegistComapnyForm" id="updateRegistComapnyForm" method="post" >	
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/company_name');?></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szCompanyName]" value="<?=(isset($_POST['updateRegistComapnyAry']['szCompanyName'])?$_POST['updateRegistComapnyAry']['szCompanyName']:$kForwarder->szCompanyName)?>" onblur="closeTip('name');" onfocus="openTip('name');"></p>
		<div class="field-alert"><div id="name" style="display:none;"><?=t($t_base.'messages/company_name');?></div></div>
		
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/address1');?></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szAddress]" value="<?=(isset($_POST['updateRegistComapnyAry']['szAddress'])?$_POST['updateRegistComapnyAry']['szAddress']:$kForwarder->szAddress)?>" onblur="closeTip('add');" onfocus="openTip('add');"></p>
		<div class="field-alert"><div id="add" style="display:none;"><?=t($t_base.'messages/address_line_1');?></div></div>
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/address2');?> <span class="optional">(<?=t($t_base.'titles/optional');?>)</span></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szAddress2]" value="<?=(isset($_POST['updateRegistComapnyAry']['szAddress2'])?$_POST['updateRegistComapnyAry']['szAddress2']:$kForwarder->szAddress2)?>" onblur="closeTip('add2');" onfocus="openTip('add2');"></p>
		<div class="field-alert"><div id="add2" style="display:none;"><?=t($t_base.'messages/address_line_2');?></div></div>
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/address3');?> <span class="optional">(<?=t($t_base.'titles/optional');?>)</span></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szAddress3]" value="<?=(isset($_POST['updateRegistComapnyAry']['szAddress3'])?$_POST['updateRegistComapnyAry']['szAddress3']:$kForwarder->szAddress3)?>" onblur="closeTip('add3');" onfocus="openTip('add3');"></p>
		<div class="field-alert"><div id="add3" style="display:none;"><?=t($t_base.'messages/address_line_3');?></div></div>
	</div>
	
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/postcode');?></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szPostCode]" value="<?=(isset($_POST['updateRegistComapnyAry']['szPostCode'])?$_POST['updateRegistComapnyAry']['szPostCode']:$kForwarder->szPostCode)?>" onblur="closeTip('postcode');" onfocus="openTip('postcode');"></p>
		<div class="field-alert"><div id="postcode" style="display:none;"><?=t($t_base.'messages/postal_code');?></div></div>
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/city');?></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szCity]" value="<?=(isset($_POST['updateRegistComapnyAry']['szCity'])?$_POST['updateRegistComapnyAry']['szCity']:$kForwarder->szCity)?>" onblur="closeTip('city');" onfocus="openTip('city');"></p>
		<div class="field-alert"><div id="city" style="display:none;"><?=t($t_base.'messages/city');?></div></div>
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/province');?> <span class="optional">(<?=t($t_base.'titles/optional');?>)</span></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szState]" value="<?=(isset($_POST['updateRegistComapnyAry']['szState'])?$_POST['updateRegistComapnyAry']['szState']:$kForwarder->szState)?>" onblur="closeTip('state');" onfocus="openTip('state');"></p>
		<div class="field-alert"><div id="state" style="display:none;"><?=t($t_base.'messages/state');?></div></div>
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/country');?></p>
		<p class="field-container">
			<select size="1" name="updateRegistComapnyAry[idCountry]" style="width:212px;" id="idCountry">
			<option value=""><?=t($t_base.'titles/select_country');?></option>
			<?php
				if(!empty($allCountriesArr))
				{
					foreach($allCountriesArr as $allCountriesArrs)
					{
						?><option value="<?=$allCountriesArrs['id']?>" <?php if(($allCountriesArrs['id']==(isset($_POST['updateRegistComapnyAry']['idCountry'])?$_POST['updateRegistComapnyAry']['idCountry']:$kForwarder->idCountry))){?> selected <?php }?>><?=$allCountriesArrs['szCountryName']?></option>
						<?php
					}
				}
			?>
		   </select></p>
		<div class="field-alert"><div id="state" style="display:none;"><?=t($t_base.'messages/state');?></div></div>
	</div>
	
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/phone_number');?></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szPhone]" value="<?=(isset($_POST['updateRegistComapnyAry']['szPhone'])?$_POST['updateRegistComapnyAry']['szPhone']:$kForwarder->szPhone)?>" onblur="closeTip('phone');" onfocus="openTip('phone');" id="szPhone"></p>
		<div class="field-alert"><div id="phone" style="display:none;"><?=t($t_base.'messages/phone');?></div></div>
	</div>
	<br><br>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/company_reg_num');?></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szCompanyRegistrationNum]" value="<?=(isset($_POST['updateRegistComapnyAry']['szCompanyRegistrationNum'])?$_POST['updateRegistComapnyAry']['szCompanyRegistrationNum']:$kForwarder->szCompanyRegistrationNum)?>" onblur="closeTip('cmp');" onfocus="openTip('cmp');"></p>
		<div class="field-alert"><div id="cmp" style="display:none;"><?=t($t_base.'messages/company_reg_number');?></div></div>
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/vat_reg_num');?> <span class="optional">(<?=t($t_base.'titles/optional');?>)</span></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szVatRegistrationNum]" value="<?=(isset($_POST['updateRegistComapnyAry']['szVatRegistrationNum'])?$_POST['updateRegistComapnyAry']['szVatRegistrationNum']:$kForwarder->szVatRegistrationNum)?>" onblur="closeTip('reg');" onfocus="openTip('reg');"></p>
		<div class="field-alert"><div id="reg" style="display:none;"><?=t($t_base.'messages/company_vat_reg_number');?></div></div>
	</div>
	<br /><br />
	<div class="oh">	
		<p align="right" class="fl-30"><a href="javascript:void(0);" onclick="encode_string('szPhone','szPhoneUpdate');submit_registered_company_details('<?=$idForwarder?>')"  class="button1"><span><?=t($t_base.'feilds/save');?></span></a></p>
		<p class="field-container"><a href="javascript:void(0);" onclick="show_registered_company_details('<?=$idForwarder?>')"  class="button2"><span><?=t($t_base.'feilds/cancel');?></span></a></p>
		<input type="hidden" name="updateRegistComapnyAry[idForwarder]" value="<?=$idForwarder?>">
		<input type="hidden" name="updateRegistComapnyAry[szPhoneUpdate]" value="" id="szPhoneUpdate">			
	</div>
</form>
<?php
}
function edit_company_information($t_base,$kForwarder,$updateForwarderComapnyAry=array())
{
	$idForwarder = $kForwarder->id;
        //echo __BASE_URL__;
?>

        <link href="<?=__BASE_STORE_CSS_URL__?>/uploadify.css" type="text/css" rel="stylesheet" />
        
	<script type="text/javascript">
	/*$().ready(function() {
	/* on change, focus or click call function fileName */
	//$('input[type=file]').bind('change focus click', function() {$(this).fileName()});
        
          /*  $('#file_upload').uploadify({ 	
                'uploader'  : '<?=__BASE_URL__?>/uploadify.swf',
                'script'    : '<?=__BASE_URL__?>/popupUploadify.php',
                'cancelImg' : '<?=__BASE_STORE_IMAGE_URL__?>/cancel.png',
                'folder'    : '<?=__APP_PATH_FORWARDER_IMAGES__POP_UP_TEMP__?>',
                'buttonClass' : 'uploadifyProgressBar',
                'buttonText' : 'UPLOAD',
                'sizeLimit': 5*1024*1024,
                'auto'      : true,
                'height'        : 33,
                'multi': false,
                'queueSizeLimit': 1,
                'removeCompleted' : true,
                'fileDesc' : 'Image files',
                'removeTimeout' : 200,
                'successTimeout' : 30,
                'method' : 'post',
                'progressData' : 'all',
                'requeueErrors' : true,
                'fileExt': '*.jpg;*.jpeg;*.gif;*.png;',
                'formData'        : {},               
                'preventCaching'  : true,
                'onQueueFull': function(event, queueSizeLimit) {
                    alert("Please don't put anymore files in me! You can upload " + queueSizeLimit + " files at once");
                    return false;
            },
             'onError': function (event, ID, fileObj, errorObj) {
                alert(errorObj.type + ' Error: ' + errorObj.info);
            },
            'onSelect': function(e, q, f) {
                    $('#loaderIn').css({'display':'block'});
                var validExtensions = new Array('jpg','jpeg','gif','png');				
                var fileName = f.name;
                var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
                var res_ex=jQuery.inArray(fileNameExt,validExtensions);

                if (res_ex == -1)
                {
                   //convert_image_to_jpg(fileName);
                }
                $('#file_uploadQueue').css( {'display':'none'});
            },
                    'onComplete': function(event, ID, fileObj, response, data) {
                            $('#loaderIn').css({'display':'none'});
                            $('#file_uploadQueue').html( '');
                            $('#uploadme').css({'display':'none'});
                           //$('.uploadfilelist').css({'display':'block'});
                            $("#upload_file").attr('style','display:none;padding-left:218px;')
                            var result = response.split('|||||');
                            var filename_all = '';
                            var newvalue='';	
                            
                            if(result[0]== true)
                            {
                                
                                    var size=fileObj.size;
                                    var sizeValue=size/1024;
                                    $('#file_uploadQueue').css( {'display':'block'});
                                    //alert("Filename: " + fileObj.name + "\nSize: " + fileObj.size + "\nFilepath: " + fileObj.filePath);
                                    //$("#nofile-selected").attr('style','display:none;');	
                                    //$('#file_uploadQueue').html( '<span class="uploadfilelist" style="padding-left: 99px;"><a href="javascript:void(0)" onclick="deleteLogo();"></a></span>');
                                    var filename_all=$("#file_name").val();
                                    $("#upload_file_success").attr('style','display:block');
                                    //var filecount=$("#filecount").attr('value');
                                    //var newfilecount=parseInt(filecount)+1;
                                    //$("#filecount").attr('value',newfilecount);
                                    $('#upload_file_success').html('<img alt="Logo" src="<?=__BASE_STORE_IMAGE_URL__?>/forwarders/temp/'+result[1]+'">');
                                    //$('#upload_file_success').html('<img alt="testign" src="<?=__BASE_STORE_IMAGE_URL__?>/forwarders/temp/'+result[1]+'">');
                                    $('#fileUpload').val(result[1]);
                                   
                                    //checkTheEnteties('updateRegistComapnyForm','updateRegistComapnyForm2');
                                    
                                    
                                    if(filename_all!='')
                                    {
                                            var newvalue=filename_all+";"+fileObj.name+"|||||"+result[1]+"|||||"+sizeValue;
                                    }
                                    else
                                    {
                                            var newvalue=fileObj.name+"|||||"+result[1]+"|||||"+sizeValue;	
                                    }
                                    console.log(newvalue+"newvalue");
                                    $("#file_name").attr('value',newvalue);

                            }			
                                    // you can use here jQuery AJAX method to send info at server-side.
                    } 
      });
	});
	*/
           
           $(document).ready(function() {
                var settings = {
                url: "<?=__BASE_URL__?>/popupUploadify.php",
                method: "POST",
                allowedTypes:"jpg,jpeg,gif,png",
                fileName: "myfile",
                multiple: false,
                dragDrop: false,
                uploadButtonClass: "forwarder-logo-upload-button",
                maxFileSize: 1000000,
                dragDropStr:'Upload',
                onSuccess:function(files,data,xhr)
                { 
                    var IS_JSON = true;
                    try
                    {
                        var json = $.parseJSON(data);
                    }
                    catch(err)
                    {
                        IS_JSON = false;
                    } 
                    if(IS_JSON)
                    { 
                        var obj = JSON.parse(data); 

                        var iFileCounter = 1;
                        
                        var filecount=$("#filecount").attr('value');
                        var filecountOld=$("#filecount").attr('value');
                        
                        for (var i = 0, len = obj.length; i < len; i++)
                        { 
                            console.log(obj[i]);
                            var newfilecount = parseInt(filecount) + iFileCounter;  
                            var uploadedFileName = obj[i]['name']; 
                            var FileUrl="<?php echo __BASE_STORE_URL__?>/forwarders/forwarderBulkUploadServices/temp/"+uploadedFileName; 
                            var size  = obj[i]['size'];
                            var container_li_id = "id_"+newfilecount;
                            //var size=obj[i]['size'];
                            if(parseInt(filecount)==0)
                            {
                                $('#fileList').html('');
                            }
                            $("#upload_file_success").attr('style','display:block');
                            $('#upload_file_success').html( '<img alt="Logo" src="<?=__BASE_STORE_IMAGE_URL__?>/forwarders/temp/'+uploadedFileName+'">');  
                            //$('#'+container_li_id).html( '<img src="<?=__BASE_STORE_INC_URL__.'/image.php?img=Page.png&temp=2&w=50&h=50'?>" border="0" /><span class="page">'+newfilecount+'</span>');

                            iFileCounter++; 

                            var filename_all=$("#file_name").val();  
                            if(filename_all!='')
                            { 
                                var newvalue=filename_all+";"+files+"|||||"+uploadedFileName+"|||||"+size;
                                $("#fileuploadlink").attr('style','display:block');
                                $("#file_uploade_plus_link").attr('style','display:block');
                            }
                            else
                            { 
                                var newvalue=files+"|||||"+uploadedFileName+"|||||"+size;
                                $("#fileuploadlink").attr('style','display:block');	
                                $("#file_uploade_plus_link").attr('style','display:block');
                            }
                        }
                        $("#filecount").attr('value',newfilecount);
                        $("#file_name").attr('value',newvalue);
                        $("#upload_process_list").attr('style','display:none');
                        $("#upload_process_list").html('');
                        
                        
                       // $(".upload").attr('style','position: relative; overflow: hidden; cursor: default;display:none;');
                        
                        

                         
                    }
                    else
                    {  
                       // $("#upload_process_list").append("<p style='color:red;'>"+data+" ... <a hre='javascript:void(0);' onclick='showHide(\"upload_process_list\")'>close</a></p>");
                    }

                },onSelect:function(s){
                    $("#upload_process_list").attr('style','display:block;top:93%;width:345px');
                    $("#upload_error_container").attr('style','display:none');
                },
                    afterUploadAll:function()
                    {
                        //alert("all images uploaded!!");
                    },
                    onError: function(files,status,errMsg)
                    {
                        //$("#status").html("<font color='red'>Upload is Failed</font>");
                       // alert('hi');
                    }
                }
                $("#upload_file").uploadFile(settings);
                
                
            });
	</script>
	<div id="Error"></div>
	<form name="updateRegistComapnyForm" id="updateRegistComapnyForm" method="post">	
		<div class="profile-fields">
			<p class="fl-30"><?=t($t_base.'titles/display_name');?></p>
			<p class="field-container"><input type="text" name="updateForwarderComapnyAry[szDisplayName]" value="<?=(isset($_POST['updateForwarderComapnyAry']['szDisplayName'])?$_POST['updateForwarderComapnyAry']['szDisplayName']:$kForwarder->szDisplayName);?>" onblur="closeTip('display_name');" onfocus="openTip('display_name');"></p>
			<div class="field-alert"><div id="display_name" style="display:none;"><?=t($t_base.'messages/display_name');?></div></div>
		</div>
		<div class="profile-fields">
			<p class="fl-30"><?=t($t_base.'titles/general_email_address');?></p>
			<p class="field-container"><input type="text" name="updateForwarderComapnyAry[szGeneralEmailAddress]" value="<?=(isset($_POST['updateForwarderComapnyAry']['szGeneralEmailAddress'])?$_POST['updateForwarderComapnyAry']['szGeneralEmailAddress']:$kForwarder->szGeneralEmailAddress);?>" onblur="closeTip('general_email');" onfocus="openTip('general_email');"></p>
			<div class="field-alert"><div id="general_email" style="display:none;"><?=t($t_base.'messages/general_email');?></div></div>
		</div>
		<div class="profile-fields">
			<p class="fl-30"><?=t($t_base.'titles/standard_terms');?></p>
			<p class="field-container"><input type="text" name="updateForwarderComapnyAry[szLink]" value="<?=(isset($_POST['updateForwarderComapnyAry']['szLink'])?$_POST['updateForwarderComapnyAry']['szLink']:$kForwarder->szLink);?>" onblur="closeTip('terms_standard');" onfocus="openTip('terms_standard');"></p>
			<div class="field-alert"><div id="terms_standard" style="display:none;"><?=t($t_base.'messages/terms_standard');?></div></div>
		</div>
		<div class="profile-fields">
			<p class="fl-30"><?=t($t_base.'titles/display_logo');?></p>
			<p class="field-container" style="width: 69%;height: 40px;">
			<?php
			if(isset($_POST['updateForwarderComapnyAry']['szForwarderLogo'])?$_POST['updateForwarderComapnyAry']['szForwarderLogo']:$kForwarder->szLogo)
			{
				// if forwarder logo already uploaded then temp will image will render in this span after upload
				?>
				<span id="upload_file_success" class="forwarder_logo"><img src="<?=__BASE_STORE_INC_URL__.'/image.php?img='.($_POST['updateForwarderComapnyAry']['szForwarderLogo']?$_POST['updateForwarderComapnyAry']['szForwarderLogo']:$kForwarder->szLogo)."&w=".__MAX_ALLOWED_FORWARDER_LOGO_WIDTH__."&h=".__MAX_ALLOWED_FORWARDER_LOGO_HEIGHT__?>" alt="<?=$kForwarder->szDisplayName?>"></span>
				<span class="fl-50"><a href="javascript:void(0);" id="forwarder_logo_delete_link"  onclick="delete_forwarder_logo('<?=$idForwarder?>')"><?=t($t_base.'feilds/delete');?></a></span>
				<?php 
			}
			else
			{
				// if there is no image uploaded for the forwarder then temp logo will render here. 
				?>
                                <span id="upload_file_success" class="forwarder_logo" style="display:none;">
                                    
                                </span>
                               
				<span class="fl-50"><a href="javascript:void(0);" style="display:none;" id="forwarder_logo_delete_link"  onclick="delete_forwarder_logo('<?=$idForwarder?>')"><?=t($t_base.'feilds/delete');?></a></span>
				<?php
			}
			?>
				<span class="fl-50">
                                    
<!--				<a onclick="show_upload_file_form()" href="javascript:void(0);"></a>-->
				</span>
                                <input type="hidden" id="file_name" name="updateForwarderComapnyAry[szForwarderLogo]" value="<?php echo $_POST['updateForwarderComapnyAry']['szForwarderLogo'];?>">
				<input type="hidden" name="updateForwarderComapnyAry[idForwarder]" value="<?=$idForwarder?>">
				<input type="hidden" name="updateForwarderComapnyAry[iDeleteLogo]" id="delete_logo_hidden" value="">
				<input type="hidden" name="updateForwarderComapnyAry[szOldLogo]" id="szOldLogo" value="<?=$kForwarder->szLogo?>">
			</p>
		</div>
                <div class="profile-fields">
                    <div class="fl-30">&nbsp;</div>
                    <div style="max-width:210px;position:relative;float:left;">
                        <div id="upload_file" class="upload_label">
                            <span><?=t($t_base.'feilds/upload_new_file');?></span>
                        </div>
                        <a href="javascript:void(0);" style="position:absolute;right:-15px;top:0;" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/upload_image_tootltip_heading');?>','<?=t($t_base.'messages/upload_image_tootltip_text');?>','company_information_tooltip_right',event);" onmouseout="hide_tool_tip('company_information_tooltip_right')">&nbsp;</a>
				
                    </div>    
                </div>    
		<div style="clear: both;"></div>
	</form>
        
<!--		 <div id="upload_file" style="display:none;padding-left:218px;">
		 	<input type="file" id="file_upload" name="updateRegistComapnyAry[szForwarderLogo]" value="<?=$kForwarder->szLogo?>" onblur="closeTip('company_logo');" onfocus="openTip('company_logo');" />	
                </div>-->
		 <br class="clear-all">
		<div class="oh">
			<p align="right" class="fl-30">
				<a href="javascript:void(0);" onclick="submit_forwarder_company_cust_details('<?=$idForwarder?>')"  class="button1"><span><?=t($t_base.'feilds/save');?></span></a>
			</p>
			<p class="field-container">
				<a href="javascript:void(0);" onclick="show_forwarder_company_cust_details('<?=$idForwarder?>')"  class="button2"><span><?=t($t_base.'feilds/cancel');?></span></a>			
			</p>
		</div>
<?php
}
function forwarder_billing_search_form($idForwarder,$t_base)
{
	//$idForwarder = $_SESSION['forwarder_id'];
	//$kBooking = new cBooking();
	//$bookingYearAry = $kBooking->getAllForwarderBookingYear($idForwarder);
	//$billingMonthAry = month_key_value_pair();
	?>
<div class="oh" style="float: right;">
			<select id="forwarder_billing_search_form" name="billingSearchAry[iBillingMonth]" onchange="selectBillingSearchDetails(this.value)" style="max-width: none;width: 275px;">
				<option value='1'>Last 7 Days</option>
				<option value='2'>This Month</option>
				<option value='3'>Previous month</option>
				<option value='4' selected="selected">Last 3 month</option>
				<option value='5'>This quarter</option>
				<option value='6'>Previous quarter</option>
				<option value='7'>This Year</option>
				<option value='8'>Previous Year</option>
				<option value='9'>All time</option>
			</select>
		</div>
	<?php

}

function display_multiple_lcl_services($lclServiceAry,$t_base,$requestAry=array(),$iWarehouseType=__WAREHOUSE_TYPE_CFS__)
{
     
    $width_td = 11.11 ;
    if(!empty($lclServiceAry))
    {
        $fromWarehouseId=$lclServiceAry[0]['idWarehouseFrom'];
        $kWHSSearch=new cWHSSearch();
        $kWHSSearch->load($fromWarehouseId);
        $fromWarehouseName=$kWHSSearch->szWareHouseName;
        $iWarehouseType=$kWHSSearch->iWarehouseType;
        $toWarehouseId=$lclServiceAry[0]['idWarehouseTo'];
        $kWHSSearch->load($toWarehouseId);
        $toWarehouseName=$kWHSSearch->szWareHouseName; 
        
        if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
        {
            $szSectionHeading = t($t_base.'title/obo_air_middle_box_from_heading');
        } 
        else
        { 
            $szSectionHeading = t($t_base.'title/obo_lcl_middle_box_from_heading');
        }
    ?>	
    <h5><strong><?php echo $szSectionHeading; ?> <?=$fromWarehouseName?> <?=t($t_base.'title/to')?> <?=$toWarehouseName?></strong></h5>
    <?php } else if(!empty($requestAry)) {

            $fromWarehouseId = $requestAry['idOriginWarehouse'];
            $kWHSSearch=new cWHSSearch();
            $kWHSSearch->load($fromWarehouseId);
            $fromWarehouseName=$kWHSSearch->szWareHouseName;
            $iWarehouseType=$kWHSSearch->iWarehouseType;
            $toWarehouseId = $requestAry['idDestinationWarehouse'];
            $kWHSSearch->load($toWarehouseId);
            $toWarehouseName=$kWHSSearch->szWareHouseName;
            if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
            {
                $szSectionHeading = t($t_base.'title/obo_air_middle_box_from_heading');
            } 
            else
            { 
                $szSectionHeading = t($t_base.'title/obo_lcl_middle_box_from_heading');
            }
    ?>	
    <h5><strong><?php echo $szSectionHeading; ?> <?=$fromWarehouseName?> <?=t($t_base.'title/to')?> <?=$toWarehouseName?></strong></h5>
    <?php } else { 
        if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
        {
            $szHeading = t($t_base.'title/air_feright');
        } 
        else
        {
            $szHeading = t($t_base.'title/obo_lcl_middle_box_heading');
        }
?>
        <h5><strong><?php echo $szHeading; ?></strong></h5>
    <?php }
    
    if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
    {
        $szPerWM = "Per KG"; 
    } 
    else
    {
        $szPerWM = t($t_base.'fields/per_wm');
    }
    ?>
	<table cellspacing="0" id="obo_lcl_table" cellpadding="0" border="0" class="format-2" width="100%">
		<tr>
                    <td rowspan="2" valign="top"  style="width:<?=$width_td?>%"><strong><?=t($t_base.'fields/cut_off')?></strong></td>
                    <td rowspan="2" valign="top" style="width:<?=$width_td?>%"><strong><?=t($t_base.'fields/service_frequency')?></strong></td>
                    <td rowspan="2" valign="top" style="width:<?=$width_td?>%"><strong><?=t($t_base.'fields/transit_hour_1')?></strong></td>
                    <td colspan="4" valign="top" align="left" style="width:<?=$width_td*4?>%"><strong><?=t($t_base.'fields/freight_rate')?></strong></td>
                    <td colspan="2" valign="top" align="left" style="width:<?=$width_td*2?>%"><strong><?=t($t_base.'fields/validity')?></strong></td>
		</tr>
		<tr>
                    <td style="width:<?=$width_td?>%"><strong><?php echo $szPerWM?></strong></td>
                    <td style="width:<?=$width_td?>%"><strong><?=t($t_base.'fields/minimum')?></strong></td>
                    <td style="width:<?=$width_td?>%"><strong><?=t($t_base.'fields/per_booking')?></strong></td>
                    <td style="width:<?=$width_td?>%"><strong><?=t($t_base.'fields/currency')?></strong></td>
                    <td style="width:<?=$width_td?>%"><strong> <?=t($t_base.'fields/from');?></strong></td>
                    <td style="width:<?=$width_td?>%"><strong><?=t($t_base.'fields/to');?></strong></td>
		</tr>
		<?php
                if(!empty($lclServiceAry))
                {
                    $ctr = 1;
                    foreach($lclServiceAry as $searchResults)
                    {
                        ?>						
                        <tr id="obo_lcl_<?=$ctr?>" onclick="select_obo_lcl_tr('obo_lcl_<?=$ctr?>','<?=$searchResults['id']?>')">
                            <td><?=$searchResults['szCutOffDay']?></td>
                            <td><?=$searchResults['szFrequency']?></td>
                            <td><?=$searchResults['iTransitDays']?> <?=strtolower(t($t_base.'fields/days'))?></td>
                            <td><?=number_format((float)$searchResults['fRateWM'],2)?></td>
                            <td><?=number_format((float)$searchResults['fMinRateWM'],2)?></td>
                            <td><?=number_format((float)$searchResults['fRate'],2)?></td>
                            <td><?=$searchResults['szCurrency']?></td>
                            <td><?=(!empty($searchResults['dtValidFrom']) && $searchResults['dtValidFrom']!='0000-00-00 00:00:00')?date('d/m/Y',strtotime($searchResults['dtValidFrom'])):''?></td>
                            <td><?=(!empty($searchResults['dtExpiry'])  && $searchResults['dtExpiry']!='0000-00-00 00:00:00')?date('d/m/Y',strtotime($searchResults['dtExpiry'])):''?></td>
                        </tr>
                        <?php
                        $ctr++;
                    } 
                }
                else
                { 	
                ?>
                <tr>
                    <td colspan="9" align="center"><?=t($t_base.'title/obo_lcl_no_services_updated');?></td>
                </tr>
	<?php } ?>
	</table> 
	<br />
	<div align="right">
            <a href="javascript:void(0)" class="button2" style="opacity:0.4;" onclick="" id="obo_lcl_delete_button"><span><?=t($t_base.'title/delete_line')?></span></a> 
            <a href="javascript:void(0)" class="button1" onclick="" style="opacity:0.4;" id="obo_lcl_edit_button"><span><?=t($t_base.'title/edit')?></span></a>
	</div>
	<br />
	<?php
}

function check_price_30_minute($postSearchAry,$page_url,$ret_flag=false,$idBooking=false)
{
	$kBooking = new cBooking();
	$kWHSSearch=new cWHSSearch();	
	$kExportImport = new cExport_Import();
	
	if($postSearchAry['iDoNotKnowCargo']==1)
	{
		if(empty($page_url))
		{
			$page_url = __BOOKING_DETAILS_PAGE_URL__.'/'.$postSearchAry['szBookingRandomNum'] ; 
		}
		$_SESSION['booking_page_url'] = $page_url ;	
		if($postSearchAry['iFromRequirementPage']==2 || $postSearchAry['iFromRequirementPage']==3)
		{
			header("Location:".__BOOKING_PRICE_CHANGED_SIMPLE_PAGE_URL__.'/'.$postSearchAry['szBookingRandomNum'].'/');
		 }else{
			header("Location:".__BOOKING_PRICE_CHANGED_PAGE_URL__.'/'.$postSearchAry['szBookingRandomNum'].'/');
		}
		die;
	}
	
	if(!$kBooking->check_30_minute_window($postSearchAry))
	{
		
		//If booking time interval ends then we restart timing counter.
		$update_query = " dtCreatedOn = now() ";
		$kBooking->updateDraftBooking($update_query,$idBooking);
		
		$cargoAry = $kBooking->getCargoDeailsByBookingId($postSearchAry['id']);
		
		$resultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,'RECALCULATE_PRICING',$idBooking);
		if(empty($resultAry))
		{
			$t_base = "Booking/MyBooking/";
			$redirect_url = __HOME_PAGE_URL__.'/lcl_not_found/'.$postSearchAry['szBookingRandomNum']."/";
			
			header("Location:".$redirect_url);
			die;
			//echo display_rate_expire_popup($t_base,$idBooking,'hold');
			//die;
		}
		else
		{
			$pop_up_flag = false;		
			$idPricingWtw = $postSearchAry['idPricingWtw'];
			$kExportImport->loadPricingWtw($idPricingWtw);
			
			$dtValidFromDate = 	strtotime($kExportImport->dtValidFrom);
			$dtExpiryFromDate = strtotime($kExportImport->dtExpiry);
			//$idBooking = $_SESSION['booking_id'];
			
			if(round((float)$resultAry[0]['fDisplayPrice'],2) != round((float)$postSearchAry['fTotalPriceCustomerCurrency'],2))
			{
				$pop_up_flag = true ;
				/*
				$filename = __APP_PATH__."/logs/recalculate_price.log";
				$strdata=array();
				$strdata[0]=" \n\n Price Recalculated on ".date("Y-m-d H:i:s")."\n\n";
				$strdata[1] = print_r($resultAry,true)."\n";
				file_log(true, $strdata, $filename);
				*/
				if(!$ret_flag)
				{
					if(empty($page_url))
					{
						$page_url = __BOOKING_DETAILS_PAGE_URL__.'/'.$postSearchAry['szBookingRandomNum'] ; 
					}
					$_SESSION['booking_page_url'] = $page_url ;	
					header("Location:".__BASE_URL__.'/myBooking/price/'.$postSearchAry['szBookingRandomNum'].'/');
					die;
				}	
			}
			if((strtotime($postSearchAry['dtCutOff']) <= $dtValidFromDate) || (strtotime($postSearchAry['dtCutOff']) > $dtExpiryFromDate) || (strtotime($postSearchAry['dtCutOff']) < time()))
			{
				$t_base = "Booking/MyBooking/";
				echo display_booking_expire_popup($t_base,$idBooking,'draft');
			}
			if($ret_flag)
			{
				return $pop_up_flag ;
			}
		}		
	}
}

function define_forwarder_constants()
{
    //url for forwarder module 
    define("__FORWARDER_HOME_PAGE_URL__",__BASE_URL__);
    define("__FORWARDER_HOME_PAGE_URL_SECURE__",__BASE_URL_SECURE__);
    define("__FORWARDER_COMPANY_URL__",__BASE_URL__."/Company");
    define("__FORWARDER_COMPANY_PAGE_URL__",__FORWARDER_COMPANY_URL__."/Users");
    define("__FORWARDER_MY_ACCOUNT_URL__",__BASE_URL__."/Profile");
    define("__FORWARDER_LOGOUT_URL__",__BASE_URL__."/logout");
    define("__FORWARDER_COMPANY_PREFERENCES_URL__",__FORWARDER_COMPANY_URL__."/Preferences/");
    define("__FORWARDER_COMPANY_INFORMATION_URL__",__FORWARDER_COMPANY_URL__."/Information/");
    define("__FORWARDER_COMPANY_BANK_DETAILS_URL__",__FORWARDER_COMPANY_URL__."/BankDetails/");
    define("__FORWARDER_OBO_LCL_SERVICES_URL__",__BASE_URL__.'/LCLServices');
    define("__FORWARDER_OBO_CC_SERVICES_URL__",__BASE_URL__.'/CustomsClearance');
    define("__FORWARDER_OBO_HAULAGE_URL__",__BASE_URL__.'/Haulage');
    define("__FORWARDER_CFS_LOCATIONS_URL__",__BASE_URL__.'/CFS');
    define("__FORWARDER_AIRPORT_WAREHOUSE_URL__",__BASE_URL__.'/airportsWarehouse/');
    define("__FORWARDER_BULK_AIRPORT_WAREHOUSE_URL__",__BASE_URL__.'/airportWarehouseLocationBulk/');
    define("__FORWARDER_FEEDBACK_URL__",__BASE_URL__.'/feedback');
    define("__FORWARDER_DASHBOARD_URL__",__BASE_URL__.'/dashboard');
    define("__FORWARDER_BOOKING_URL__",__BASE_URL__.'/booking/');
    define("__FORWARDER_BILLING_URL__",__BASE_URL__.'/billings/');
    define("__FORWARDER_NON_ACCEPTANCE_URL__",__BASE_URL__.'/NonAcceptance/');
    define("__FORWARDER_PRIVATE_CUSTOMER_URL__",__BASE_URL__.'/privateCustomers/');
    define("__FORWARDER_TRY_IT_OUT_URL__",__BASE_URL__.'/Try/');
    define("__FORWARDER_INCOMPLETE_PROFILE_URL__",__BASE_URL__.'/Profile/incomplete/');
    define("__FORWARDER_TERM_CONDITION_URL__",__BASE_URL__.'/T&C/');
    define("__FORWARDER_WELCOME_PAGE_URL__",__BASE_URL__.'/welcome/');
    define("__FORWARDER_NON_VERIFIED_PROFILE_URL__",__BASE_URL__.'/Profile/nonVerified/');
    define("__FORWARDER_INCOMPLETE_COMPANY_URL__",__BASE_URL__.'/company/incomplete/');
    define("__FORWARDER_CONTACT_EDIT_PROFILE_URL__",__BASE_URL__.'/profile/edit/');
    define("__FORWARDER_SERVICE_UPDATE_URL__",__BASE_URL__."/updateServiceExpiryDate.php");
    define("__FORWARDER_COMPANY_PAGE_INTRODUCTION_URL__",__BASE_URL__."/companyIntroduction/");
    define("__FORWARDER_TRANSPORTECA_PRICING_URL__",__BASE_URL__."/Company/Pricing/");
    define("__FORWARDER_CFS_BULK_LOCATIONS_URL__",__BASE_URL__."/CFSLocationBulk/");
    define("__FORWARDER_CFS_UPLOAD_INSTRUCTION_URL__",__BASE_URL__."/UploadServices/Instructions/");
    define("__FORWARDER_PRICING_SERVICING_INTRODUCTION_URL__",__BASE_URL__."/PricingServicingIntroduction/");
    define("__FORWARDER_BULK_UPLOAD_SERVICES_URL__",__BASE_URL__."/BulkUploadServices/");
    define("__FORWARDER_BULK_SERVICES_APPROVAL_URL__",__BASE_URL__."/BulkServiceApproval/");

    define("__FORWARDER_PENDING_QUOTES_URL__",__BASE_URL__."/pendingQuotes/");
    define("__FORWARDER_PAST_QUOTES_URL__",__BASE_URL__."/pastQuotes/");

    define("__FORWARDER_SERVICE_OFFERING_URL__",__BASE_URL__."/serviceOffering/");
    define("__FORWARDER_QUICK_QUOTE_URL__",__BASE_URL__."/quickQuote/");
    define("__FORWARDER_EMAIL_TEMPLATE_URL__",__BASE_URL__."/templateEmail/");
    
    define("__FORWARDER_AIR_FREIGHT_SERVICE_URL__",__BASE_URL__.'/Airfreight/');
    define("__FORWARDER_AIR_FREIGHT_BULK_SERVICE_URL__",__BASE_URL__.'/AirfreightServicesBulk/');
    
    define("__BASE_URL_FORWARDER_LOGO__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/images/forwarders");
}

function exchangeView()
{
	$t_base ='management/currency/';
?>
<div style="clear: both;"></div>
<label class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/currency');?></span>
	<span class="field-container">	
		<input type="text" name="arrExchange[currency]" class="addExchangeOnFocus" size="6" onfocus="addExchangeRate();" style="width: 94px;"/> 
	</span>
</label>
<label class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/should1');?></span>
	<span class="field-container">		
		<select name="arrExchange[pricing]" class="addExchangeOnChange" onchange="addExchangeRate();" >
			<option value="1">Yes</option>
			<option value="0">No</option>
		</select>
	</span>
</label>
<label class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/should2');?></span>
	<span class="field-container">			
		<select name="arrExchange[booking]" class="addExchangeOnChange" onchange="addExchangeRate();">
			<option value="1">Yes</option>
			<option value="0">No</option>
		</select>
	</span>
</label>
<label class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/can');?></span>
	<span class="field-container">			
		<select name="arrExchange[settling]" class="addExchangeOnChange" onchange="addExchangeRate();">
			<option value="1">Yes</option>
			<option value="0">No</option>
		</select>
	</span>
</label>	
<label class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/google');?></span>
	<span class="field-container">		
		<input type="text" name="arrExchange[feed]" class="addExchangeOnFocus" onfocus="addExchangeRate();" />
	</span>
</label>	
<label class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/bank');?></span>
	<span class="field-container">		
		<input type="text" name="arrExchange[szBankName]"  value="<?php echo $detail['szBankName']; ?>" onfocus="addExchangeRate();"   > 
	</span>
</label>
<label class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/sort_code');?></span>
	<span class="field-container">			
		<input type="text" name="arrExchange[szSortCode]" id="szSortCode" value="" onfocus="addExchangeRate();">
	</span>
</label>
<label class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/account_number');?></span>
	<span class="field-container">			
		<input type="text" name="arrExchange[szAccountNumber]" id="szAccountNumber" value="" onfocus="addExchangeRate();">
	</span>
</label>
    <label class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/iban');?></span>
	<span class="field-container">			
            <input type="text" name="arrExchange[szIBANNumber]" id="szIBANNumber" value="<?php echo $detail['szIBANNumber']; ?>">
	</span>
    </label>
<label class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/swift');?></span>
	<span class="field-container">			
		<input type="text" name="arrExchange[szSwiftNumber]" id="szSwiftNumber" value="" onfocus="addExchangeRate();">
	</span>
</label>
<label class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/account_name');?></span>
	<span class="field-container">			
		<input type="text" name="arrExchange[szNameOnAccount]" id="szNameOnAccount" value="" onfocus="addExchangeRate();">
	</span>
</label>
	<input type="hidden" name="mode" value="SAVE_DETAIL" />
	<input type="hidden" name="id" value="0" />
<?}
function exchangeRatesView($exchangeRates)
{ $t_base="management/currency/";
?>
<table cellspacing="0" cellpadding="0" border="0" class="format-4" width="100%" id="excangeTable" >
			<tr>
				<th colspan="5" valign="top"><a href="#" <? if(!empty($exchangeRates)){ ?>onclick="download_table(1)" <? } ?>> <?=t($t_base.'fields/download')?></a></th>
				<th colspan="2" valign="top" ><?=t($t_base.'fields/exchanage_rates')?></th>
			</tr>
			<tr>
				<th width="55px"><?=t($t_base.'fields/currency')?></th>
				<th width="55px"><?=t($t_base.'fields/pricing')?></th>
				<th width="55px"><?=t($t_base.'fields/booking')?></th>
				<th width="55px"><?=t($t_base.'fields/settling')?></th>
				<th><?=t($t_base.'fields/feed')?></th>
				<th width="55px"><?=t($t_base.'fields/latest')?></th>
				<th><?=t($t_base.'fields/update')?></th>
			</tr>
<?
	 if(!empty($exchangeRates))
	{
		foreach($exchangeRates as $key=>$value)
		{
?>
			<tr class="selectCurrency" id="<?=$value['id'];?>" <? if($value['fUsdValue']!=''){?> onclick="selectExchangeRates('<?=$value['id']."','".$value['szCurrency'];?>')" <? } ?> style="cursor: pointer;">
				<td><?=$value['szCurrency'];?></td>
				<td><?=($value['iPricing'])?(t($t_base.'fields/yes')):(t($t_base.'fields/no'));?></td>
				<td><?=($value['iBooking'])?(t($t_base.'fields/yes')):(t($t_base.'fields/no'));?></td>
				<td><?=($value['iSettling'])?(t($t_base.'fields/yes')):(t($t_base.'fields/no'));?></td>
				<td><?=$value['szFeed'];?></td>
				<td><?=$value['fUsdValue'];?></td>
				<td><?=$value['dtDate'];?></td>
			</tr>
<?
		}
	}	
	?></table><?	

}
function non_verified_email_popup($t_base,$kForwarderContact)
{
	$navigation_pop_up_message_header =  t($t_base.'messages/non_verified_profile_header');
	$navigation_pop_up_message =  t($t_base.'messages/non_verified_profile_message1');
	$navigation_pop_up_message2 =  t($t_base.'messages/non_verified_profile_message2');
	$or =  t($t_base.'messages/or');
	$click =  t($t_base.'messages/click');
	$here =  t($t_base.'messages/here');
	$close =  t($t_base.'messages/close');
	
	?>
	  <div id="popup-bg"></div>
		 <div id="popup-container">			
		  <div class="popup compare-popup">
		  	<h5><strong><?=$navigation_pop_up_message_header?></strong></h5>
			<p><?=$navigation_pop_up_message." ".date('d/m/Y',strtotime($kForwarderContact->dtActivationCodeSent))." ".$or." ".$click?>
				<a href="javascript:void(0)" style="color:#000000;font-size:14px;" onclick="resendVerificationCode('<?=__BASE_URL__?>')"><?=$here?></a> <?=$navigation_pop_up_message2?>
				<span id="activationkey"></span>
			</p>
			<br /><br />
			 <p align="center">			
			 	<a href="javascript:void(0);" onclick="showHide('incomplete_profile_error_div')" class="button1"><span><?=$close?></span></a>			
			</p>	
		  </div>
	   </div>
	</div>
	<?
}

function selectmaxEmailLogs($count)
{
	$pages = ceil($count/__PER_PAGE__);
	return $pages;
	
}
function emailLogTable($data=array(),$paging,$pageCurrent=1)
{ 	
	$pages=selectmaxEmailLogs($paging);
	$t_base="management/e_mail_log_mgmt/";
?>
	<table cellspacing="0" cellpadding="0" border="0" class="format-4" width="99%"  >
		<tr>
			<th valign="top" width="30%"><?=t($t_base.'fields/to_email_address')?></th>
			<th valign="top" width="38%" ><?=t($t_base.'fields/subject')?></th>
			<th valign="top" width="12%"><?=t($t_base.'fields/dt_sent')?></th>
			<th valign="top" width="20%"><?=t($t_base.'fields/view_message')?></th>
		</tr>
<?
	
	if(!empty($data))
	{	
		foreach ( $data as $logos )
		{	
	?>
		<tr>
			<td style="max-width: 30%;"><?=str_replace(',','<br />',$logos['szToAddress']);?></td>
			<td style="max-width: 38%;"><?=$logos['szEmailSubject'];?></td>
			<td style="max-width: 12%;"><?=$logos['dtSents'];?></td>
			<td style="max-width: 20%;"><a class="button1" style="cursor: pointer;"  onclick="view_body_details(<?=$logos['id'];?>)"><span><?=t($t_base.'fields/view_content');?></span></a></td>
		</tr>
	<?	
		}
	}
	else
	{
		echo "<tr><td  colspan='4' style='text-align:center;'><strong>NO DATA TO PREVIEW</strong></td></tr>";
	}
	
?></table>
		<div id="paging_button" style="text-align: center;">
		<ul>
		<?php
		for($i=1; $i<=$pages; $i++)
		{
			echo '<li id="'.$i.'" onclick="searchingDataPagination(this.id);">'.$i.'</li>';
		}
		?>
		</ul>
	</div>
	<div style="clear: both"></div>
<?php
}
	function createHtml()
	{ 	
		  if( !defined( "__APP_PATH__" ) )
		  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
		  $filename=__APP_PATH__.'/forwarders/html2pdf/terms_conditon.html';
	      $handle = fopen($filename, "rb");
	      $contents = fread($handle, filesize($filename));
	      fclose($handle);
	      $kForwarder = new cForwarder();
		  $version=$kForwarder->selectVersion();
		  require_once(__APP_PATH_CLASSES__.'/warehouseSearch.class.php');
		  $KwarehouseSearch=new cWHSSearch();
		  $tnc=$KwarehouseSearch->selectQuery(1);
		  $contents = str_replace("VERSION_DATE", date('d F Y',strtotime($version['dtVersionUpdated'])), $contents);
		  $data='';
		  $count=1;
		  if($tnc)
		  {
			foreach($tnc AS $terms)
			{
			
			$data.="<h4><strong>".$count.". ".ucfirst($terms['szHeading'])."</strong></h4><p>".$terms['szDescription']."</p><br>";
			$count++;
			}
		  }
		  
	  	  $contents = str_replace("CONTENT", $data, $contents);
	  	  $contents = str_replace("<p><p>","<p>",$contents);
	  	  $contents = str_replace("</p></p>","</p>",$contents); 
	  	  $contents = str_replace("<p>&nbsp;</p>","",$contents); 
	  	  $contents = str_replace("<p","<p style='TEXT-ALIGN:JUSTIFY'",$contents);
		 	
      $fp = fopen(__APP_PATH__."/forwarders/html2pdf/terms_condition_pdf.html", "w+");
      fwrite($fp, $contents);
      fclose($fp); 
      return $contents;
}  
function display_booking_expire_popup($t_base,$idBooking,$status='draft')
{
    $kBooing = new cBooking();
    $szBookingRandomNum = $kBooing->getBookingRandomNum($idBooking);
    $ret_url = __LANDING_PAGE_URL__.'/'.$szBookingRandomNum.'/'; 
?>
    <div id="popup-bg"></div>
    <div id="popup-container">			
        <div class="popup abandon-popup">
           <h5><?=t($t_base.'title/booking_expire');?></h5>
           <p><?=t($t_base.'messages/booking_expire_message');?></p>
           <br>
           <div class="oh">
            <p align="center">
                <a href="javascript:void(0);" onclick="change_booking_status('<?=$idBooking?>','delete','<?=$status?>')" class="button1"><span>DELETE</span></a>
                <a href="javascript:void(0);" onclick="redirect_url('<?=$ret_url?>')" class="button1"><span>CHANGE</span></a>
            </p>
           </div>
        </div>
    </div>	
<?php
}
function display_booking_already_paid($t_base)
{
	$t_base = "home/homepage/";
	?>
	<div id="popup-bg"></div>
	 <div id="popup-container">			
		<div class="popup abandon-popup">
		<h5><?=t($t_base.'fields/notification');?></h5>
		<p><?=t($t_base.'messages/notification_message_already_paid_booking');?></p>
		<br>
		<div class="oh">
			<p align="center">	
				<a href="javascript:void(0);" onclick="redirect_url('<?=__HOME_PAGE_URL__?>')" class="button1"><span><?=t($t_base.'fields/continue');?></span></a>			
			</p>
		</div>
		</div>
	</div>	
	<?
}

function display_booking_invalid_user_popup($t_base)
{
	$t_base = "BookingDetails/";
	?>
	<div id="popup-bg"></div>
	 <div id="popup-container">			
		<div class="popup abandon-popup">
		<p class="close-icon" align="right">
		<a onclick="redirect_url('<?=__HOME_PAGE_URL__?>');" href="javascript:void(0);">
		<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
		</a>
		</p>
		<h5><?=t($t_base.'fields/notification');?></h5>
		<p><?=t($t_base.'messages/notification_message_invalid_user');?></p>
		<br>
		<div class="oh">
			<p align="center">	
				<a href="javascript:void(0);" onclick="redirect_url('<?=__HOME_PAGE_URL__?>')" class="button1"><span><?=t($t_base.'fields/continue');?></span></a>			
			</p>
		</div>
		</div>
	</div>	
	<?php
}
function display_rate_expire_popup($t_base,$idBooking,$status='draft')
{
	$kBooing = new cBooking();
	$szBookingRandomNum = $kBooing->getBookingRandomNum($idBooking);
	$ret_url = __LANDING_PAGE_URL__.'/'.$szBookingRandomNum.'/';
	
	?>
	<div id="popup-bg"></div>
	 <div id="popup-container">			
		<div class="popup abandon-popup">
		<p class="close-icon" align="right">
		<a onclick="redirect_url('<?=$ret_url?>');" href="javascript:void(0);">
		<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
		</a>
		</p>
		<h5><?=t($t_base.'title/rate_expired');?></h5>
		<p><?=t($t_base.'messages/pricing_not_available');?></p>
		<br>
		<div class="oh">
			<p align="center">
				<a href="javascript:void(0);" onclick="change_booking_status('<?=$idBooking?>','delete','<?=$status?>')" class="button1"><span><?=t($t_base.'fields/delete_booking');?></span></a>
				<a href="javascript:void(0);" onclick="redirect_url('<?=$ret_url?>')" class="button1"><span><?=t($t_base.'fields/new_search');?></span></a>
			</p>
		</div>
		</div>
	</div>	
	<?php
}

function display_quote_expire_popup($t_base,$idBooking,$status='draft')
{
	$kBooing = new cBooking();
	$szBookingRandomNum = $kBooing->getBookingRandomNum($idBooking);
	$ret_url = __LANDING_PAGE_URL__;
	
	?>
	<div id="popup-bg"></div>
	 <div id="popup-container">			
            <div class="popup abandon-popup">
            <p class="close-icon" align="right">
            <a onclick="showHide('change_price_div')" href="javascript:void(0);">
            <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
            </a>
            </p>
            <h5><?=t($t_base.'title/price_expired');?></h5>
            <p><?=t($t_base.'title/price_expired_message');?></p>
            <br>
            <div class="oh">
                <p align="center">
                    <a href="javascript:void(0);" onclick="redirect_url('<?php echo $ret_url; ?>')" class="button1"><span><?=t($t_base.'fields/close');?></span></a>
                </p>
            </div>
            </div>
	</div>	
	<?php
}
function format_date($dtDateTime)
{
	if(!empty($dtDateTime))
	{
		$dateTimeAry=explode(" ",$dtDateTime);
		if(!empty($dateTimeAry[0]))
		{
			$dateAry = explode('/',$dateTimeAry[0]);
			
			$iDate = $dateAry[0];
			$iMonth = $dateAry[1];
			$iYear = $dateAry[2];
			
			$standart_date = $iYear."-".$iMonth."-".$iDate." ".$dateTimeAry[1];
			return $standart_date ;
		}	 
	}
}
function format_fowarder_emails($customerServiceEmailAry)
{
    if(!empty($customerServiceEmailAry))
    {
        $iTotalCounter = count($customerServiceEmailAry);
        $ctr=0;
        foreach($customerServiceEmailAry as $customerServiceEmailArys)
        {
            if(empty($customerServiceEmailStr))
            {
                $customerServiceEmailStr = $customerServiceEmailArys ;
                $customerServiceEmailLink = $customerServiceEmailArys;
            }
            else
            {
                $customerServiceEmailLink .= ', '.$customerServiceEmailArys;

                if($ctr>0 && $ctr==($iTotalCounter-1))
                {
                    $customerServiceEmailStr .= ' and '.$customerServiceEmailArys;
                }
                else
                {
                    $customerServiceEmailStr .= ', '.$customerServiceEmailArys;
                }
            }							
            $ctr++;
        }	
        $ret_ary=array();
        $ret_ary[0] = $customerServiceEmailLink ;
        $ret_ary[1] = $customerServiceEmailStr ;
        return 	$ret_ary;
    }
}
function createSubString($value,$from=0,$length)	
{
    if(strlen($value) > $length || $from > 0)
    {
        $value = mb_substr($value,$from,$length-2,'UTF8')."..";
    }
    return $value;
}

function display_rate_expire_popup_non_acceptance($t_base,$idBooking)
{
	?>	
	<div id="popup-bg"></div>
	 <div id="popup-container">			
		<div class="popup abandon-popup">
		<p class="close-icon" align="right">
		<a onclick="redirect_url('<?=__HOME_PAGE_URL__?>');" href="javascript:void(0);">
		<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
		</a>
		</p>
		<h5><?=t($t_base.'title/rate_expired');?></h5>
		<p><?=t($t_base.'messages/pricing_not_available');?></p>
		<br>
		<div class="oh">
			<p align="center">
				<a href="javascript:void(0);" onclick="redirect_url('<?=__HOME_PAGE_URL__?>')" class="button1"><span><?=t($t_base.'fields/new_search');?></span></a>
			</p>
		</div>
		</div>
	</div>	
	<?php
}
function isoidCountryCode($id)
{
    if($id>0)
    {
        $kBooking =	new cBooking();
        $ISOCode=$kBooking->findISOcode($id);
        return $ISOCode;

    }
    else
    {
        return NULL;
    }
}
function pageNotFound()
{ 
    header("HTTP/1.0 404 Not Found"); 
    $t_base = "Error/";
?>
<div id="hsbody">
<div id="page_not_found"  style="padding:30px 0 80px 30px;">
<br><br>
<div style="float: left;">
<img src="<?=__BASE_STORE_IMAGE_URL__?>/page_not_found.jpg">
</div>
<div style="padding:0 100px 0 30px;">
<p style="font-size:22pt;padding-bottom: 50px;"><?=t($t_base.'page_not_found');?></p>
<p style="font-size:large;padding-bottom: 50px;"><?=t($t_base.'page_not_found_detail');?></p> 
<a href="<?=__BASE_URL__?>" style="font-size:large;"><?=t($t_base.'comparing_rates_at_transporteca');?></a> 
</div>
</div>
</div>
<?php
}
function convertUTF($data)
{
 $value=html_entities_flag($data,false);
 return $value;
}
function display_booking_notification($redirect_url,$idBooking=false)
{
	$t_base = "home/";
	//$idBooking = $_SESSION['booking_id'];
	?>	
	<div id="popup-bg"></div>
	 <div id="popup-container">			
		<div class="popup abandon-popup">
		<h5><?=t($t_base.'header/notification');?></h5>
		<p><?=t($t_base.'header/notification_message');?></p>
		<br>
		<div class="oh">
                    <p align="center">		
                        <a href="javascript:void(0);" onclick="showHide('change_price_div');" class="button1"><span><?=t($t_base.'header/close');?></span></a>			
                    </p>
		</div>
		</div>
	</div>	
	<?php
}

function html_form_random_booking_number($redirect_url,$szBookingRandomNum)
{
	$t_base_rand_key = "LandingPage/";
	$iLanguage = getLanguageId();
        
        $kConfig =new cConfig();
        $langArr=$kConfig->getLanguageDetails('',$iLanguage,'','','','','',false);
	if(!empty($langArr))
	{
            $szLangParam = ucwords($langArr[0]['szName']);
	}
	else
	{
		$szLangParam = 'english';
	}
	?>
        <form name="booking_random_number_form" action="<?=$redirect_url?>" method="get" id="booking_random_number_form">
            <input type="hidden" name="szBookingRandomNum" id="szBookingRandomNum_hidden" value="<?=$szBookingRandomNum?>" />
            <input type="hidden" name="iSiteLanguageFooter" id="iSiteLanguageFooter_hidden" value="<?=$szLangParam?>" />
            <input type="hidden" name="idUser_bottom_form_hidden" id="idUser_bottom_form_hidden" value="<?=$_SESSION['user_id']?>" />

            <input type="hidden" name="next_step_text_bottom" id="next_step_text_bottom" value="<?=t($t_base_rand_key.'fields/next_step');?>" />
        </form>
	<?php
}

function display_booking_not_found_popup($redirect_url,$idBooking=false)
{
	$t_base = "home/";
	$idBooking = $_SESSION['booking_id'];
	?>	
	<div id="popup-bg"></div>
	 <div id="popup-container">			
		<div class="popup abandon-popup">
		<h5><?=t($t_base.'header/notification');?></h5>
		<p><?=t($t_base.'header/notification_message');?></p>
		<br>
		<div class="oh">
			<p align="center">			
				<a href="javascript:void(0);" onclick="showHide('change_price_div');" class="button1"><span><?=t($t_base.'header/close');?></span></a>			
			</p>
		</div>
		</div>
	</div>	
	<?php

}

function adminDetails($t_base,$transportecaManagement,$idAdmin)
{ 
	if($idAdmin>0)
	{  
            echo selectPageFormat('Transporteca');
    ?> 
            <table cellpadding="0" id="management" cellspacing="0" border="0" class="format-4" width="100%">
		<tr>
                    <th width="10%" align="center" valign="top"><?=t($t_base.'fields/status');?></th>
                    <th width="18%" align="center" valign="top"><?=t($t_base.'fields/first_name')?></th> 
                    <th width="18%" align="center" valign="top"><?=t($t_base.'fields/l_name')?></th>
                    <th width="20%" align="center" valign="top"><?=t($t_base.'fields/email');?></th>
                    <th width="15%" align="center" valign="top"><?=t($t_base.'fields/profile');?></th>
                    <th width="19%" align="center" valign="top"><?=t($t_base.'fields/pending_tray');?></th> 
		</tr>
    <?php
		$i=1;
		if(!empty($transportecaManagement))
		{
                    foreach($transportecaManagement as $admins)
                    {
                        if($admins['id']<=0)
                        {
                            /*
                            *  If there is any record where id is 0 then we don't display that row.
                            */ 
                            continue;
                        }
                        $str='';
                        $str = "<tr id=admin".$i.">
                                <td onclick=\"changeManagementActiveStatus('".$admins['id']."','".$admins['iActive']."','".$admins['iMode']."','admin".$i."')\">".($admins['iActive']?'Active':'Inactive')."</td>
                                <td onclick=\"changeManagementActiveStatus('".$admins['id']."','".$admins['iActive']."','".$admins['iMode']."','admin".$i."')\">".$admins['szFirstName']."</td>
                                <td onclick=\"changeManagementActiveStatus('".$admins['id']."','".$admins['iActive']."','".$admins['iMode']."','admin".$i."')\">".$admins['szLastName']."</td>
                                <td onclick=\"changeManagementActiveStatus('".$admins['id']."','".$admins['iActive']."','".$admins['iMode']."','admin".$i."')\">".$admins['szEmail']."</td>
                                <td onclick=\"changeManagementActiveStatus('".$admins['id']."','".$admins['iActive']."','".$admins['iMode']."','admin".$i."')\">".$admins['szProfileName']."</td>";
                        if($admins['iActive']==1){
                           $str .= "<td align='center'><a href='javascript:void(0);' onclick=\"changeCustomerServiceAgentStatus('".$admins['id']."')\" id='link_".$admins['id']."'  style='text-decoration:underline;font-weight:normal;'>".($admins['iCustomerServiceAgent']?'Yes':'No')."</a></td>";
                                }else
                                {
                                   $str .= "<td align='center'>-</td>"; 
                                }
                                
                           $str .= " </tr>";
                           echo $str;
                       $i++;
                    }
		}
		?>
		</table>
		<br />
			<div style="float: right;">
				<a class="button1" id="new_admin" onclick="createNewAdminProfile('<?=$idAdmin?>','CREATE_NEW_ADMIN_PROFILE')" style="opacity:1;"><span><?=t($t_base.'fields/new');?></span></a>
				<a class="button2" id="changeStatus" style="opacity:0.4;"><span><?=t($t_base.'fields/inactive');?></span></a>	
			</div>
	
		<div style="clear: both;"></div>
	</div>
	<?php
	}
}



function adminForwarderProfileDetails($t_base,$forwarderContactProfile)
{ 
?>
	<?=selectPageFormat('ForwarderProfile');?>
            <table cellpadding="0" cellspacing="0" border="0" id="forwarderManagement" class="format-4" width="100%" style="margin-bottom:5px;">
		<tr>
                    <th width="18%" align="center" valign="top" ><?=t($t_base.'fields/forwarder_name')?><a  id="sort_by_forwarder_name" onclick="detailsOrderByForwarder('1','DESC','sort_by_forwarder_name')" ><span class="sort-arrow-up" >&nbsp;</span></th> 
                    <th width="15%" align="center" valign="top" ><?=t($t_base.'fields/profile')?><a id="sort_by_profile" onclick="detailsOrderByForwarder('2','ASC','sort_by_profile')" ><span class="sort-arrow-up-grey" >&nbsp;</span></a></th>
                    <th width="18%" align="center" valign="top" ><?=t($t_base.'fields/name');?><!-- <a id="sort_by_name" onclick="detailsOrderByForwarder('3','DESC','sort_by_name')" ><span class="sort-arrow-down">&nbsp;</span></a> --></th>
                    <th width="29%" align="center" valign="top"><?=t($t_base.'fields/email');?></th>
                    <th width="20%" align="center" valign="top"><?=t($t_base.'fields/last_login');?><a id="sort_by_name" onclick="detailsOrderByForwarder('3','DESC','sort_by_name')" ><span class="sort-arrow-down-grey">&nbsp;</span></a></th> 
		</tr>
		<?php	
			if($forwarderContactProfile!=array() && $forwarderContactProfile!='')
			{
				$count=1;
				foreach($forwarderContactProfile as $profile)
				{
                                    
                                        $szContactRole='';
					switch($profile['idForwarderContactRole'])
					{
						CASE 1:
						{
						$szContactRole ="Administrator";
						BREAK;
						}
						CASE 3:
						{
						$szContactRole ="Pricing & Services";
						BREAK;
						}
						CASE 5:
						{
						$szContactRole ="Bookings & Billing";
						BREAK;
						}
                                                CASE 7:
						{
						$szContactRole ="Contact Only";
						BREAK;
						}
					}
					echo "
						<tr id='forwarder_profile_".$count."' onclick=\"select_forwarder_management('".$profile['id']."','forwarder_profile_".$count."','".$profile['idForwarderContactRole']."')\">
						<td>".returnLimitData($profile['szDisplayName'],15)." (".$profile['szForwarderAlias'].")</td>
						<td>".$szContactRole."</td>
						<td>".returnLimitData($profile['szName'],18)."</td>
						<td>".returnLimitData($profile['szEmail'],30)."</td>
						<td>".(($profile['dtLastLogin']!='00/00/0000 0:00')?$profile['dtLastLogin']:'N/A')."</td>
						</tr>
						";
					$count++;
				}
			}
			else
			{
				echo "<tr><td colspan ='5' align='center'><b>".t($t_base.'title/no_content_to_display')."</b></td></tr>";
			}
		?>
		</table>
                <div style="float: right;">
                    <a class="button1" id="new_contact_forwarder" onclick="openNewContactOfForwarder();"><span><?=t($t_base.'fields/new_contact');?></span></a>
                    <a class="button1" id="delete_forwarder"  style="opacity:0.4;"><span><?=t($t_base.'fields/delete');?></span></a>
                    <a class="button1" id="forwarder_history"  style="opacity:0.4;"><span><?=t($t_base.'fields/history');?></span></a>
                    <a class="button1" id="edit_forwarder" style="opacity:0.4;"><span><?=t($t_base.'fields/edit');?></span></a>	
                </div>	
	
	<?php
}


function selectPageFormat($activeFlag =null)
{ 	
	$t_base = "management/leftNav/";
	$id = $_SESSION['admin_id'];
        $checkPerissionCustomersFlag=false;
        $checkPerissionFreightForwarderFlag=false;
        $checkPerissionForwarderProfileFlag=false;
        $checkPerissionTransportecaFlag=false;
        $checkPerissionInvestorsFlag=false;
        $checkPerissionRolePermissionFlag=false;
        
        if($_SESSION['szProfileType']!='__ADMIN__' && $_SESSION['szProfileType']!='')
        {
            
            $checkPerissionCustomersFlag=true;
            $checkPerissionFreightForwarderFlag=true;
            $checkPerissionForwarderProfileFlag=true;
            $checkPerissionTransportecaFlag=true;
            $checkPerissionInvestorsFlag=true;
            $checkPerissionRolePermissionFlag=true;
            
            $kAdmin = new cAdmin();
            $permissionArr= $kAdmin->getAllPermissionByRoles($_SESSION['szProfileType']);
            if(count($permissionArr[2])>0){
                if((int)$permissionArr[2][__USERS_CUSTOMERS__]>0)
                {
                    $checkPerissionCustomersFlag=false;
                }
                if((int)$permissionArr[2][__MESSAGE_FREIGHT_FORWARDERS__]>0)
                {
                    $checkPerissionFreightForwarderFlag=false;
                }
                if((int)$permissionArr[2][__USERS_FORWARDER_PROFILES__]>0)
                {
                    $checkPerissionForwarderProfileFlag=false;
                }
                if((int)$permissionArr[2][__MESSAGE_TRANSPORTECA__]>0)
                {
                    $checkPerissionTransportecaFlag=false;
                }
                if((int)$permissionArr[2][__USERS_INVESTORS__]>0)
                {
                    $checkPerissionInvestorsFlag=false;
                }
                if((int)$permissionArr[2][__MESSAGE_ASSIGN_ROLE_PERMISSION__]>0)
                {
                    $checkPerissionRolePermissionFlag=false;
                }
            }
        }
	?>
	<div class="hsbody-2-left account-links">
            <ul>
                <?php if(!$checkPerissionCustomersFlag){?>
                <li <?php if(trim($activeFlag)=='user'){?>       class="active" <? }?>><a onclick="showProfilePagination(0,'cust')" class="cursor-pointer"><?=t($t_base.'fields/customers')?></a></li>
                <?php } if(!$checkPerissionFreightForwarderFlag){?>
                <li <?php if($activeFlag=='ForwarderDetail'){?>  class="active" <? }?>><a onclick="showProfilePagination(0,'forwarderComp')" class="cursor-pointer"><?=t($t_base.'fields/freight_forwarder')?></a></li>
                <?php } if(!$checkPerissionForwarderProfileFlag){?>
                <li <?php if($activeFlag=='ForwarderProfile'){?> class="active" <? }?>><a onclick="showDifferentProfile('forwarderProife')" class="cursor-pointer"><?=t($t_base.'fields/forwarder_profiles')?></a></li>
                <?php } if(!$checkPerissionTransportecaFlag){?>
                <li <?php if($activeFlag=='Transporteca'){?>     class="active" <? }?>><a onclick="showDifferentProfile('Transporteca')" class="cursor-pointer"><?=t($t_base.'fields/transporteca')?></a></li>
                <?php } if(!$checkPerissionInvestorsFlag){?>
                <li <?php if($activeFlag=='Investors'){?> class="active" <? }?>><a onclick="showDifferentProfile('Investors')" class="cursor-pointer"><?=t($t_base.'fields/investers')?></a></li>
                <?php } if(!$checkPerissionRolePermissionFlag){?>
                <li <?php if($activeFlag=='RolePermission'){?> class="active" <? }?>><a onclick="showDifferentProfile('RolePermission')" class="cursor-pointer"><?=t($t_base.'fields/role_peression')?></a></li>
                <?php }?>
            </ul>
        </div>	
        <div class="hsbody-2-right">
	<?php 
}


function transporteca_billing_search_form($forwarderIdName,$idForwarder,$t_base)
{
	//$idForwarder = $_SESSION['forwarder_id'];
	//$kBooking = new cBooking();
	//$bookingYearAry = $kBooking->getAllForwarderBookingYear($idForwarder);
	//$billingMonthAry = month_key_value_pair();
	?>
    <div class="oh">
        <select id="idForwarder" name="billingSearchAry[iForwarderId]"  onchange="selectAdminBillingSearchDetails()" style="max-width: none;width: 275px;margin-left: 5px;">
            <?php
            if($forwarderIdName!=array())
            {
                foreach($forwarderIdName as $fwdName)
                {
                    echo "<option value='".$fwdName['id']."'>".$fwdName['szDisplayName']." (".$fwdName['szForwarderAlias'].")</option>";
                }
            }
            ?>
        </select>
        <span>
            <select id="forwarder_billing_search_form" name="billingSearchAry[iBillingMonth]" onchange="selectAdminBillingSearchDetails()" style="max-width: none;width: 175px;float: right;">
                    <option value='1'>Last 7 Days</option>
                    <option value='2'>This Month</option>
                    <option value='3'>Previous month</option>
                    <option value='4' selected="selected">Last 3 month</option>
                    <option value='5'>This quarter</option>
                    <option value='6'>Previous quarter</option>
                    <option value='7'>This Year</option>
                    <option value='8'>Previous Year</option>
                    <option value='9'>All time</option>
            </select>
        </span>
    </div>
	<?php
}
function forwarder_currency_change_notifivcation($idForwarder,$t_base,$szNewCurrency,$szOldCurrency)
{
    ?>	
    <div id="popup-bg"></div>
     <div id="popup-container">			
        <div class="popup compare-popup">
            <h5><?=t($t_base.'titles/notification');?></h5>
            <p>
                <?=t($t_base.'messages/notification_message');?> <?=t($t_base.'messages/notification_message2');?> <?=$szNewCurrency?>. <?=t($t_base.'messages/notification_message3');?> <?=$szOldCurrency?><?=t($t_base.'messages/notification_message4');?>
            </p>
            <br>
            <div class="oh">
                <p align="center">			
                    <a href="javascript:void(0);" onclick="submit_preference_control(1);" class="button1"><span><?=t($t_base.'titles/confirm');?></span></a>
                    <a href="javascript:void(0);" onclick="removeInnerContent('ajaxLogin');" class="button2"><span><?=t($t_base.'titles/cancel');?></span></a>			
                </p>
            </div>
        </div>
    </div>	
    <?php
}
function showCustomerAdminBillingSettled($billingDetails,$idAdmin,$iPage=1,$limit=__MAX_CONFIRMED_BOOKING_NEW_PER_PAGE__)
{	
    
$kAdmin = new cAdmin();
    $kAdmin->getAdminDetails($idAdmin);
   
    $updateCommentFlag = checkManagementPermission($kAdmin,"__UPDATE_COMMENT__",3);
$t_base="management/billings/";

        $iGrandTotal=count($billingDetails);
        require_once (__APP_PATH__ ."/inc/classes/pagination.class.php");
        $kPagination  = new Pagination($iGrandTotal,$limit,__LINK_CONFIRMED_BOOKING_NEW_PER_PAGE__);
        //echo $limit;
        if($iPage==1)
        {
            $start=0;
            $end=$limit;
            if($iGrandTotal<$end)
            {
                $end=$iGrandTotal;
            }
        }
        else
        {
            $start=$limit*($iPage-1);
            $end=$iPage*$limit;
            if($end>$iGrandTotal)
            {
                $end=$iGrandTotal;
            }
            $startValue=$start+1;
            if($startValue>$iGrandTotal)
            {
                $iPage=$iPage-1;
                $start=$limit*($iPage-1);
                $end=$iPage*$limit;
                if($end>$iGrandTotal)
                {
                    $end=$iGrandTotal;
                }

            }
        }
?>
    <input type="hidden" id="page" name="page" value="<?php echo $iPage;?>">
	<table cellpadding="0"  cellspacing="0" border="0" class="format-2" width="100%" style="margin-bottom:20px;" id="booking_table">
		<tr>
			<td width="10%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/payment_date')?></strong> </td>
			<td width="10%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/booking_reference')?></strong></td>
			<td width="10%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/billing_invoice')?></strong> </td>
			<td width="15%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/forwarder')?></strong> </td>
			<td width="15%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/customer')?></strong> </td>
			<td width="15%" style='text-align:right;' valign="top"><strong><?=t($t_base.'fields/price')?></strong> </td>
			<td width="25%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/comment')?></strong> </td>
		</tr>
			<?php
				if($billingDetails!=array())
				{	$ctr=1;
					for($i=$start;$i<$end;++$i)
					{	
					
						$fInsurancePrice = 0;
						if($billingDetails[$i]['iInsuranceIncluded']==1)
						{
							$fInsurancePrice = $billingDetails[$i]['fTotalInsuranceCostForBookingCustomerCurrency'];
							$bills['fTotalAmount'] +=  $fInsurancePrice ;
						} 
                                                
						$dtPaymentConfirmed='';
						if($billingDetails[$i]['dtPaymentSettled']!='' && $billingDetails[$i]['dtPaymentSettled']!='0000-00-00 00:00:00')
						{
                                                    $dtPaymentConfirmed = date('d/m/Y',strtotime($billingDetails[$i]['dtPaymentSettled']));
						}
                                                else if($billingDetails[$i]['dtPaymentConfirmed']!='' && $billingDetails[$i]['dtPaymentConfirmed']!='0000-00-00 00:00:00')
						{
                                                    $dtPaymentConfirmed=date('d/m/Y',strtotime($billingDetails[$i]['dtPaymentConfirmed']));
						}
						
						if(strlen($billingDetails[$i]['szEmail'])>25)
						{
							$szEmail=substr_replace($billingDetails[$i]['szEmail'],'..',25,strlen($billingDetails[$i]['szEmail']));
						}
						else
						{
							$szEmail=$billingDetails[$i]['szEmail'];
						}
						
						$id=base64_encode($billingDetails[$i]['id']);
						
						if(!empty($billingDetails[$i]['szDisplayName']) && strlen($billingDetails[$i]['szDisplayName'])>15)
						{
							$szForwarderName = mb_substr($billingDetails[$i]['szDisplayName'],0,13,'UTF8')."..";
						}
						else
						{
							$szForwarderName = $billingDetails[$i]['szDisplayName'];
						}
						$mode = '';
						if($billingDetails[$i]['iDebitCredit']==6)
						{
                                                    $mode = 'CREDIT_NOTE';
                                                    $szAmountStr = "(".$billingDetails[$i]['szCurrency']." ".number_format((float)($billingDetails[$i]['fTotalAmount']+$fInsurancePrice),2).")";
                                                    $szInvoiceStr = $billingDetails[$i]['szInvoice'];
						}
						else
						{
                                                    $szAmountStr = $billingDetails[$i]['szCurrency']." ".number_format((float)($billingDetails[$i]['fTotalAmount']+$fInsurancePrice),2);
                                                    $szInvoiceStr = $billingDetails[$i]['szInvoice'];
						}
						
						?>
						<tr align='center'  id='booking_data_<?=$ctr?>' onclick=select_admin_tr('booking_data_<?=$ctr++?>','<?=$billingDetails[$i]['idBooking']?>','<?=$mode?>')>
                                                    <td style='text-align:left;'><?=$dtPaymentConfirmed?></td>
                                                    <td style='text-align:left;'><a class="booking-ref-link" href="<?php echo __MANAGEMENT_OPERATIONS_PENDING_TRAYS__;?><?php echo $billingDetails[$i]['szBookingRandomNum'];?>/"><?=$billingDetails[$i]['szBookingRef']?></a></td>
                                                    <td style='text-align:left;'><?=$szInvoiceStr?></td>
                                                    <td style='text-align:left;'><?=$szForwarderName?></td>
                                                    <td style='text-align:left;'><?=$szEmail?></td>
                                                    <td style='text-align:right;'><?=$szAmountStr?></td>
                                                    <td style='text-align:left;'><span id="payment_comment_<?=$billingDetails[$i]['id']?>"><?=createSubString($billingDetails[$i]['szComment'],0,21)?></span>
                                                    <?php if($updateCommentFlag){?><a href="javascript:void(0)" onclick="update_payment_comment('<?=$billingDetails[$i]['id']?>','<?=$billingDetails[$i]['szComment']?>','<?=$billingDetails[$i]['szBooking']?>')" id="update_comment_link_<?=$billingDetails[$i]['id']?>" style="text-decoration:none;font-size:11px;"><img src="<?=__BASE_STORE_IMAGE_URL__?>/Icon - Edit line.png" alt ="Click to update" style="float: right;margin-top:-1px;"></a><?php }?>
                                                    </td>
							</tr>	
								<?php
						$id='';
					}
				 }
				else
				{
					echo "<tr><td colspan='7'><CENTER><b>".t($t_base.'fields/no_record_found')."</b><CENTER></td></tr>";
				}
			?>
		</table>
		<div id="invoice_comment_div" style="display:none">
	</div>
    <div class="page_limit clearfix">
        <div class="pagination">
            <?php

                $kPagination->szMode = "DISPLAY_BOOKING_LIST_PAGINATION";
                //$kPagination->idBookingFile = $idBookingFile;
                $kPagination->paginate('display_booking_list_pagination'); 
                if($kPagination->links_per_page>1)
                {
                    echo $kPagination->renderFullNav();
                }        
            ?>
               </div>
               <div class="limit">Records per page
                   <select id="limit" name="limit" onchange="display_booking_list_pagination('1')">
                       <option <?php if($limit=='100' || $_POST['limit']==''){?> selected <?php }?> value="100">100</option>
                        <option <?php if($limit=='250'){?> selected <?php }?> value="250">250</option>
                        <option <?php if($limit=='500'){?> selected <?php }?> value="500">500</option>
                        <option <?php if($limit=='1000'){?> selected <?php }?> value="1000">1000</option>
                   </select>
               </div>    
            </div>
			<div id="viewInvoiceComplete" style="float: right;">
				<a class="button1" id="download_invoice" style="opacity:0.4;"><span><?=t($t_base.'fields/view_invoice');?></span></a>  
				<a class="button1" id="download_booking"  style="opacity:0.4;"><span><?=t($t_base.'fields/view_booking');?></span></a>
			</div>	
<?php
}
function define_metrics_constants()
{
    //url for management module 
    define ("__METRICS_URL__", __BASE_URL__);
    define ("__MANAGEMENT_URL__", __BASE_URL__);
    define("__METRICS_DASHBOARD_URL__",__METRICS_URL__."/dashboard/"); 
    define("__METRICS_MY_ACCOUNT_URL__",__METRICS_URL__."/profile/");
    define("__METRICS_LOGOUT_URL__",__METRICS_URL__."/logout/"); 
}

function define_management_constants()
{
	//url for management module 
	define ("__MANAGEMENT_URL__", __BASE_URL__);
	define("__MANAGEMENT_CURRENCY_URL__",__MANAGEMENT_URL__."/currencyVariable/");
	define("__MANAGEMENT_COUNTRY_URL__",__MANAGEMENT_URL__."/countriesVariable/");
        define("__MANAGEMENT_REGION_URL__",__MANAGEMENT_URL__."/regionVariable/"); 
	define("__MANAGEMENT_TEXT_EDITOR_URL__",__MANAGEMENT_URL__."/customerDetails/");
	define("__MANAGEMENT_TERMS_AND_CONDITION_URL__",__MANAGEMENT_URL__."/forwarderTNC/");
	define("__MANAGEMENT_FAQ_URL__",__MANAGEMENT_URL__."/forwarderFAQ/");
	define("__MANAGEMENT_TERMS_AND_CONDITION_CUSTOMER_URL__",__MANAGEMENT_URL__."/customerTNC/");
	define("__MANAGEMENT_FAQ_CUSTOMER_URL__",__MANAGEMENT_URL__."/customerFAQ/");
	define("__MANAGEMENT_EMAIL_LOGS_URL__",__MANAGEMENT_URL__."/eMailLogManagement.php/");
	define("__MANAGEMENT_VARIABLE_URL__",__MANAGEMENT_URL__."/settingVariable/");
	define("__MANAGEMENT_BILLING_URL__",__MANAGEMENT_URL__."/outstandingPayment/");
	//define("__MANAGEMENT_DASHBOARD_URL__",__MANAGEMENT_URL__."/dashboard.php/");
	define("__MANAGEMENT_MY_ACCOUNT_URL__",__MANAGEMENT_URL__."/profile/");
	define("__MANAGEMENT_LOGOUT_URL__",__MANAGEMENT_URL__."/logout/");
	//define("__MANAGEMENT_FORWARDER_TRANSFER_URL__",__MANAGEMENT_URL__."/forwarderTransfer.php/");
	define("__MANAGEMENT_SETTLED_URL__",__MANAGEMENT_URL__."/settledPayment/");
	define("__MANAGEMENT_FORWARDER_PAYMENT_URL__",__MANAGEMENT_URL__."/forwarderPayment/");
	define("__MANAGEMENT_FORWARDER_TRANSFER_URL__",__MANAGEMENT_URL__."/forwarderTransfer/");
	define("__MANAGEMENT_FORWARDER_PAYMENT_HISTORY_URL__",__MANAGEMENT_URL__."/forwarderPaymentsHistory/");
	define("__MANAGEMENT_TRANSPORTECA_INVOICE_URL__",__MANAGEMENT_URL__."/forwarderTransportecaInvoice/");
	define("__MANAGEMENT_TRANSACTION_HISTORY_URL__",__MANAGEMENT_URL__."/transactionHistory/");
        define("__MANAGEMENT_FORWARDER_TURNOVER_URL__",__MANAGEMENT_URL__."/forwarderTurnover/");
	define("__MANAGEMENT_TEXT_EDITOR_FORWARDER_URL__",__MANAGEMENT_URL__."/forwarderDetails/");
	define("__MANAGEMENT_USERS_URL__",__MANAGEMENT_URL__."/allUsers/");
	define("__MANAGEMENT_HISTORY_URL__",__MANAGEMENT_URL__."/history/");
	define("__MANAGEMENT_MESSAGE_CUSTOMER_URL__",__MANAGEMENT_URL__."/customerMessage/");
	define("__MANAGEMENT_MESSAGE_FORWARDER_URL__",__MANAGEMENT_URL__."/forwarderMessages/");
	define("__MANAGEMENT_POSTCODE_URL__",__MANAGEMENT_URL__."/postcodeVariable/");
	define("__MANAGEMENT_CRON_JOB_URL__",__MANAGEMENT_URL__."/cronJob/");
	define("__MANAGEMENT_DNS_APACHE_URL__",__MANAGEMENT_URL__."/DNSApache/");
        define("__MANAGEMENT_API_LOGS_URL__",__MANAGEMENT_URL__."/apiLogs/");
        
	define("__MANAGEMENT_OPERATIONS__STATISTICS_URL__",__MANAGEMENT_URL__."/bookingStatistics/");
        define("__MANAGEMENT_OPERATIONS_PENDING_TRAYS__",__MANAGEMENT_URL__."/pendingTray/"); 
	define("__MANAGEMENT_LOCATION_DESCRIPTION_URL__",__MANAGEMENT_URL__."/locationVariable/");
	define("__MANAGEMENT_OPERATIONS__CONFIRMED_URL__",__MANAGEMENT_URL__."/bookingConfirmed/");
	define("__MANAGEMENT_OPERATIONS_NOT_CONFIRMED_URL__",__MANAGEMENT_URL__."/bookingNotConfirmed/");
	define("__MANAGEMENT_OPERATIONS__NON_ACKNOWLEDGE_URL__",__MANAGEMENT_URL__."/bookingNonAcknowledge/");
	
	define("__MANAGEMENT_OPERATIONS__CANCELLED_BOOKING_URL__",__MANAGEMENT_URL__."/cancelledBooking/");
	
	define("__MANAGEMENT_OPERATIONS__DETAILS_URL__",__MANAGEMENT_URL__."/DetailsFeedBack/");
	define("__MANAGEMENT_OPERATIONS__PERFORMANCE_URL__",__MANAGEMENT_URL__."/PerformanceFeedBack/");
	define("__MANAGEMENT_MESSAGE_REVIEW_URL__",__MANAGEMENT_URL__."/sendMessageReview/");
	define("__MANAGEMENT_OPERATIONS__WTW_URL__",__MANAGEMENT_URL__."/currentWTW/");
	define("__MANAGEMENT_MESSAGE_EMAIL_TEMPLATES_URL__",__MANAGEMENT_URL__."/templateEmail/");
	define("__MANAGEMENT_MESSAGE_EMAIL_TIMING_URL__",__MANAGEMENT_URL__."/Timing/");
	define("__MANAGEMENT_MESSAGE_CFS_LIST_URL__",__MANAGEMENT_URL__."/CFSList/");
	define("__MANAGEMENT_GUIDE_URL__",__MANAGEMENT_URL__."/guideForwarder/");
	define("__MANAGEMENT_FORWARDER_GUIDE_URL__",__MANAGEMENT_URL__."/ForwardersGuide/");
	define("__MANAGEMENT_FORWARDER_ON_MAP_URL__",__MANAGEMENT_URL__."/CFSMapLocation/");
	define("__MANAGEMENT_SERVICES_SUBMITTED_URL__",__MANAGEMENT_URL__."/servicingUrl/");
	define("__MANAGEMENT_WORK_IN_PROGRESS_URL__",__MANAGEMENT_URL__."/workInProgress/");
	define("__MANAGEMENT_RSS_URL__",__MANAGEMENT_URL__."/rss/");
	define("__MANAGEMENT_GRAPH_URL__",__MANAGEMENT_URL__."/userDetail/");
	define("__MANAGEMENT_CC_URL__",__MANAGEMENT_URL__."/customerClearence/");
	define("__MANAGEMENT_EXPIRING_WTW_URL__",__MANAGEMENT_URL__."/expiringWTW/");
	define("__MANAGEMENT_TRY_IT_OUT_URL__",__MANAGEMENT_URL__."/Try/");
	define("__MANAGEMENT_LINK_BUILDER_URL__",__MANAGEMENT_URL__."/link-builder/");
	define("__MANAGEMENT_OPERATIONS__HAULAGE_URL__",__MANAGEMENT_URL__."/currentHaulage/");
        define("__MANAGEMENT_RAIL_TRANSPORT_URL__",__MANAGEMENT_URL__."/railTransport/");
	
	define("__MANAGEMENT_EXPLAIN_URL__",__MANAGEMENT_URL__."/howitworks/");	
	define("__MANANAGEMENT_ADD_EXPLAIN_PAGE_URL__",__MANAGEMENT_URL__."/createExplainPage/");
	define("__MANANAGEMENT_MANAGE_EXPLAIN_PAGE_URL__",__MANAGEMENT_URL__."/manageExplainPages/");
	
	define("__MANAGEMENT_MANAGE_COMPANY_EXPLIAN_URL__",__MANAGEMENT_URL__."/manageCompanyPages/");
	define("__MANAGEMENT_COMPANY_EXPLIAN_URL__",__MANAGEMENT_URL__."/company/"); 
	define("__MANAGEMENT_CREATE_COMPANY_EXPLIAN_URL__",__MANAGEMENT_URL__."/createCompanyPage/"); 
	
	define("__MANAGEMENT_MANAGE_TRANSPORTPEDIA_EXPLIAN_URL__",__MANAGEMENT_URL__."/manageTransportpediaPages/");
	define("__MANAGEMENT_TRANSPORTPEDIA_EXPLIAN_URL__",__MANAGEMENT_URL__."/transportpedia/"); 
	define("__MANAGEMENT_CREATE_TRANSPORTPEDIA_EXPLIAN_URL__",__MANAGEMENT_URL__."/createTransportpediaPage/");
	
	
	define("__MANANAGEMENT_SEARCH_EXPLAIN_PAGE_URL__",__MANAGEMENT_URL__."/searchPhrase/");
	define("__MANANAGEMENT_ADD_BLOG_PAGE_URL__",__MANAGEMENT_URL__."/createBlogPage/");
	define("__MANANAGEMENT_MANAGE_BLOG_PAGE_URL__",__MANAGEMENT_URL__."/manageBlogPages/");
	define("__MANANAGEMENT_DASHBOARD_URL__",__MANAGEMENT_URL__."/dashboard/");
	
	define("__MANAGEMENT_OPERATIONS_VISUALIZATION_URL__",__MANAGEMENT_URL__."/visualizastion/");
	define("__MANAGEMENT_OPERATIONS_MOTNTHLY_URL__",__MANAGEMENT_URL__."/monthly/");
	define("__MANAGEMENT_OPERATIONS_SELLING_TRADE_URL__",__MANAGEMENT_URL__."/selling_trade/");
	define("__MANAGEMENT_OPERATIONS_SEARCHED_TRADE_URL__",__MANAGEMENT_URL__."/searched_trade/");
	define("__MANAGEMENT_LANDING_PAGE_EDITOR_URL__",__MANAGEMENT_URL__."/manageLandingPages/");
	define("__MANAGEMENT_NEW_LANDING_PAGE_EDITOR_URL__",__MANAGEMENT_URL__."/manageNewLandingPages/");
	define("__MANANAGEMENT_ADD_LANDING_PAGE_URL__",__MANAGEMENT_URL__."/addLandingPage/");
	define("__MANANAGEMENT_EDIT_LANDING_PAGE_URL__",__MANAGEMENT_URL__."/editLandingPage/");
	
	define("__MANANAGEMENT_ADD_NEW_LANDING_PAGE_URL__",__MANAGEMENT_URL__."/addNewLandingPage/");
	define("__MANANAGEMENT_EDIT_NEW_LANDING_PAGE_URL__",__MANAGEMENT_URL__."/editNewLandingPage/");
	
	define("__MANAGEMENT_SEO_PAGE_EDITOR_URL__",__MANAGEMENT_URL__."/manageSeoPages/");
	define("__MANANAGEMENT_ADD_SEO_PAGE_URL__",__MANAGEMENT_URL__."/addSeoPage/");
	define("__MANANAGEMENT_EDIT_SEO_PAGE_URL__",__MANAGEMENT_URL__."/editSeoPage/");
	define("__MANAGEMENT_SITE_MAP_VIDEO_URL__",__MANAGEMENT_URL__."/sitemapVideo");
	define("__MANAGEMENT_OPERATIONS__CUSTOMER_FEEDBACK_URL__",__MANAGEMENT_URL__."/NPS/");
	define("__MANAGEMENT_SITE_MAP_VIDEO_DOWLOAD_URL__",__MANAGEMENT_URL__."/downloadVideo");
	define("__MANAGEMENT_MESSAGE_RSS_TEMPLATES_URL__",__MANAGEMENT_URL__."/templateRss/");
	
	define("__MANAGEMENT_MESSAGE_REVIEW_QUEUE_URL__",__MANAGEMENT_URL__."/reviewQueueMessage/");
	
	define("__MANAGEMENT_EDIT_NEWS_URL__",__MANAGEMENT_URL__."/editNews/");
	define("__MANAGEMENT_MESSAGE_REVIEW_EMAIL_LOG_URL__",__MANAGEMENT_URL__."/systemMessages/");
	
	define("__MANAGEMENT_SERVICES_NEW_BOOKING__",__MANAGEMENT_URL__."/newBookings/");
	define("__MANAGEMENT_SERVICES_BOOKING_SENT__",__MANAGEMENT_URL__."/bookingSent/");
	define("__MANAGEMENT_SERVICES_BOOKING_CONFIRMED__",__MANAGEMENT_URL__."/ServiceBookingConfirmed/");
	define("__MANAGEMENT_SERVICES_BOOKING_CANCELLED__",__MANAGEMENT_URL__."/bookingCancelled/"); 
	define("__MANAGEMENT_SERVICES_INSURANCE_RATES__",__MANAGEMENT_URL__."/insuranceRates/");
	
	define("__MANAGEMENT_OPRERATION_NEW_BOOKING_QUOTES__",__MANAGEMENT_URL__."/newQuotes/");
	define("__MANAGEMENT_OPRERATION_SENT_BOOKING_QUOTES__",__MANAGEMENT_URL__."/sentQuotes/");
	define("__MANAGEMENT_OPRERATION_WON_BOOKING_QUOTES__",__MANAGEMENT_URL__."/wonQuotes/");
	define("__MANAGEMENT_OPRERATION_EXPIRED_BOOKING_QUOTES__",__MANAGEMENT_URL__."/expiredQuotes/");
	
	
	define("__MANAGEMENT_OPRERATION_COUIER_PROVIDER_PRODUCT__",__MANAGEMENT_URL__."/providerProduct/");
	define("__MANAGEMENT_OPRERATION_COUIER_MODE_TRANSPORT__",__MANAGEMENT_URL__."/modeTransport/");
	define("__MANAGEMENT_OPRERATION_VAT_APPLICATION__",__MANAGEMENT_URL__."/vatApplication/");
	define("__MANAGEMENT_OPRERATION_COUIER_AGREEMENT__",__MANAGEMENT_URL__."/currentAgreement/");
	define("__MANAGEMENT_OPRERATION_COUIER_LABEL_NOT_SEND__",__MANAGEMENT_URL__."/labelNotSend/");
	define("__MANAGEMENT_OPRERATION_COUIER_LABEL_SEND__",__MANAGEMENT_URL__."/labelSent/");
	define("__MANAGEMENT_OPRERATION_COUIER_LABEL_DOWNLOAD__",__MANAGEMENT_URL__."/labelDownload/");
        define("__MANAGEMENT_PAY_FORWARDER_INVOICE_URL__",__MANAGEMENT_URL__."/payForwarderInvoice/");
        define("__MANAGEMENT_PAID_FORWARDER_INVOICE_URL__",__MANAGEMENT_URL__."/paidForwarderInvoices/"); 
        define("__MANAGEMENT_DTD_TRADES__",__MANAGEMENT_URL__."/dtdTrades/");
        define("__MANAGEMENT_STANDARD_CARGO_PRICING__",__MANAGEMENT_URL__."/standardCargoPricing/");
        define("__MANAGEMENT_SEARCH_NOTIFICATION__",__MANAGEMENT_URL__."/searchNotifications/");
        define("__MANAGEMENT_MANUAL_FEE__",__MANAGEMENT_URL__."/pricingmanualfee/");
        define("__MANAGEMENT_BLANK_PAGE_FEE__",__MANAGEMENT_URL__."/noAccess/");  
        define("__MANAGEMENT_LANGUAGE_CONFIGURATION_URL__",__MANAGEMENT_URL__."/languageConfiguration/");
        define("__MANAGEMENT_DUMMY_SERVICES__",__MANAGEMENT_URL__."/dummyservices/");
        define("__MANAGEMENT_SEARCH_USER_LIST__",__MANAGEMENT_URL__."/searchuserlist/");
        define("__MANAGEMENT_TRANSPORTECA_TURNOVER_URL__",__MANAGEMENT_URL__."/transportecaTurnover/");
        define("__MANAGEMENT_LANGUAGE_VERSIONS_URL__",__MANAGEMENT_URL__."/languageVersions/");
        define("__MANAGEMENT_LANGUAGE_PAGE_META_TAGS_URL__",__MANAGEMENT_URL__."/pageMetaTags/");
        define("__MANAGEMENT_LANGUAGE_STANDARD_CARGO_URL__",__MANAGEMENT_URL__."/standardCargo/");
        define("__MANAGEMENT_TRADE_CUSTOMER_CLEARANCE_URL__",__MANAGEMENT_URL__."/customClearance/");
        define("__MANAGEMENT_OPERATIONS_AIR_SERVICES_URL__",__MANAGEMENT_URL__."/airfreightServices/");
        define("__MANAGEMENT_API_ERROR_URL__",__MANAGEMENT_URL__."/apiErrorLog/");
        
} 

function showForwarderTransferPayment($billingTransferDetails,$idAdmin)
{
	$kForwarder = new cForwarder();
	$t_base="management/forwarderpayment/"; 
        $count = 1;
?>
	<table cellpadding="0"  cellspacing="0" border="0" class="format-2" width="100%" style="margin-bottom:20px;" id="booking_table">
		<tr>
			<td width="10%" style="text-align:left;" valign="top"><strong>Due date</strong></td>
			<td width="20%" style="text-align:left;" valign="top"><strong><?=t($t_base.'fields/forwarder')?></strong></td>
			<td width="15%" style="text-align:left;" valign="top"><strong><?=t($t_base.'fields/bank')?></strong></td>
			<td width="10%" style="text-align:left;" valign="top"><strong><?=t($t_base.'fields/account')?></strong></td>
			<td width="10%" style="text-align:right;" valign="top"><strong><?=t($t_base.'fields/amount')?></strong></td>
			<td width="15%" style="text-align:left;" valign="top"><strong><?=t($t_base.'fields/action')?></strong></td>
		</tr>
		<?php
                    if(!empty($billingTransferDetails))
                    {
                        foreach($billingTransferDetails as $billingTransferDetail)
                        {
                            if(!empty($billingTransferDetail['dtPaymentScheduled']) && $billingTransferDetail['dtPaymentScheduled']!='0000-00-00 00:00:00')
                            {
                                $dtTransferDueDate = date('d/m/Y',strtotime($billingTransferDetail['dtPaymentScheduled']));
                            }
                            else
                            {
                                $dtTransferDueDate = date('d/m/Y',strtotime($billingTransferDetail['dtCreatedOn']));
                            }
		?>
                    <tr id="booking_data_<?=$count?>"  onclick="viewNoticeTransactionDetails('booking_data_<?=$count?>', '<?php echo $billingTransferDetail['iBatchNo']?>', '<?php echo $billingTransferDetail['idForwarder']; ?>','1')">
			<td valign="top"><?php echo $dtTransferDueDate; ?></td>
			<td valign="top"><?=returnLimitData($billingTransferDetail['szDisplayName'],20)." (".$billingTransferDetail['szForwarderAlias'].")"; ?></td>
			<td valign="top"><?=returnLimitData($billingTransferDetail['szBankName'],20)?></td>
			<td valign="top"><?=$billingTransferDetail['iAccountNumber']?></td>
			<td style="text-align:right;" valign="top"><?=$billingTransferDetail['szCurrency']?>  <?=number_format((float)$billingTransferDetail['fForwarderTotalPrice'],2)?></td>
			<?php if($kForwarder->checkForwarderComleteBankDetails($billingTransferDetail['idForwarder']))
			{
				echo "<td>Awaiting Bank Details</td>";
			}
			else{
			?>
			<td valign="top"><a href="javascript:void(0)" onclick="confirm_forwarder_transfer_due('<?=$billingTransferDetail['id']?>','<?=$billingTransferDetail['iBatchNo']?>','<?=$billingTransferDetail['szDisplayName']?>','<?=$billingTransferDetail['idForwarder']?>');" id="confirm_bill_payment"><span><?=t($t_base.'fields/confirm_scheduled');?></span></a></td>
			<?php } $count++; ?>
		</tr>
		<?php     }
			}
			else
			{
				echo "<tr><td colspan='6' align='center'><strong>".t($t_base.'fields/no_record_found')."</strong></td></tr>";
			}
			?>
	</table>
    <a class="button1" id='view_notice' style="opacity:0.4;float: right;"><span>View Notice</span></a>
<?php			
}
function forwarder_currency_change_notifivcation_dashboard($idForwarder,$t_base,$div_id=false)
{
	if(empty($div_id))
	{
		$div_id = 'contactPopup';
	}	
	?>	
	<div id="popup-bg"></div>
	 <div id="popup-container">			
		<div class="popup compare-popup">
		<p class="close-icon" align="right">
		<a onclick="showHide('<?=$div_id?>');" href="javascript:void(0);">
		<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
		</a>
		</p>
		<h5><?=t($t_base.'title/notification');?></h5>
		<p><?=t($t_base.'title/notification_message');?></p>
		<br>
		<div class="oh">
			<p align="center">	
				<a href="javascript:void(0);" onclick="showHide('<?=$div_id?>');" class="button2"><span><?=t($t_base.'fields/close');?></span></a>			
			</p>
		</div>
		</div>
	</div>	
	<?php
}

function changeAdminProfileStatus($id,$t_base,$status=0)
{	
	$t_base=trim($t_base);
	$id=(int)$id;
	if($id>0)
	{
		?>
	 <div id="popup-bg"></div>
	 <div id="popup-container">			
		<div class="popup abandon-popup">
                        <h5><?=t($t_base.'title/confirm_are_you_sure');?><?php if($status==1){?> <?=t($t_base.'title/confirm_are_you_sure_management_text');?><?php }?></h5>
			<div class="oh">
				<p align="center">	
					<a href="javascript:void(0);" onclick="changeStatusAdmin('<?=$id.'\',\'CHANGE_TRANSPORTECA_STATUS\',\''.$status?>')" class="button1"><span><?=t($t_base.'fields/save');?></span></a>			
					<a href="javascript:void(0);" onclick="cancelEditMAnageVar(0)" class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>			
				</p>
			</div>
		</div>
	</div>	
			
		<?php
	}

}

function changeInvestorProfileStatus($id,$t_base)
{	
    $t_base=trim($t_base);
    $id=(int)$id;
    if($id>0)
    {
        ?>
     <div id="popup-bg"></div>
     <div id="popup-container">			
        <div class="popup abandon-popup">
            <h5><?=t($t_base.'title/confirm_are_you_sure');?></h5>
            <div class="oh">
                <p align="center">	
                    <a href="javascript:void(0);" onclick="changeStatusAdmin('<?=$id.'\',\'CHANGE_TRANSPORTECA_INVESTOR_STATUS'?>')" class="button1"><span><?=t($t_base.'fields/save');?></span></a>			
                    <a href="javascript:void(0);" onclick="cancelEditMAnageVar(0)" class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>			
                </p>
            </div>
        </div>
    </div>	 	
    <?php
    } 
}

function define_constants($szLanguage,$admin_flag=false)
{	
    if($admin_flag)
    {		
        // booking pages url 
        $_SESSION['user_selected_language'] = '';
        unset($_SESSION['user_selected_language']);
        
        $_SESSION['user_selected_language'] = $szLanguage ;
        
        define("__LANGUAGE__",$szLanguage);
        define("__SELECT_SERVICES_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/services");
        //define("__LANDING_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/search");
        //define("__REQUIREMENT_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/requirement");
        define("__HOME_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__);
        //define("__BOOKING_DETAILS_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/bookings_details");
        //define("__BOOKING_CONFIRMATION_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/bookings_confirmation");
        define("__BOOKING_RECEIPT_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/booking_receipt/");
        define("__BOOKING_PAYMENT_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/payment");
        define("__RSS_FEED_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/news");
        define("__RSS_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/rss");
        define("__BOOKING_PRICE_CHANGED_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/search/price_changed");
        define("__EXPLAIN_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/explain/");
        define("__BLOG_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/learn/");
        define("__SITE_MAP_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/sitemap");

        define("__TRANSPORTECA_LANDING_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/landing_page");

        define("__FAQ_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/faq");
        define("__CREATE_ACCOUNT_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/createAccount");
        define("__BASE_URL_FORWARDER_LOGO__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/images/forwarders");
        define("__INFORMATION_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/information");

        define("__OUR_COMPANY_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/about");
        define("__STARTING_NEW_TRADE_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/expansion");

        define("__HOW_IT_WORK_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/forwarder");
        define("__FORWARDER_SIGNUP_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/forwarderSignUp");

        define("__PARTNER_PROGRAM_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/partnerProgramme");

        define("__TERMS_CONDITION_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/terms");
        define("__PRIVACY_NOTICE_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/privacyNotice");	
        define("__USER_LOGIN_URL__", __MAIN_SITE_HOME_PAGE_SECURE_URL__."/login.php");
        define("__USER_LOGOUT_URL__", __MAIN_SITE_HOME_PAGE_SECURE_URL__."/logout");
        define("__USER_MY_ACCOUNT_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/myAccount");	

        //define("__SELECT_SIMPLE_SERVICES_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/simpleServices");
        //define("__SELECT_SIMPLE_SEARCH_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/simpleSearch/");


        define("__NEW_SEARCH_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/newSearch/");
        
        //When you'll change /service/ please do remember to change url on management/ajax_PendingTray.php too. 
        define("__NEW_SERVICES_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/services"); 
        
        define("__NEW_BOOKING_DETAILS_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/details");
        define("__NEW_BOOKING_OVERVIEW_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/overview");
        define("__NEW_BOOKING_TERMS_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/booking-terms");
        define("__THANK_YOU_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/thank-you"); 

        define("__LANDING_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__);
        define("__REQUIREMENT_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__);
        define("__BOOKING_CONFIRMATION_PAGE_URL__",__NEW_BOOKING_OVERVIEW_PAGE_URL__);
        define("__SELECT_SIMPLE_SEARCH_URL__",__NEW_SEARCH_PAGE_URL__);
        define("__SELECT_SIMPLE_SERVICES_URL__",__NEW_SERVICES_PAGE_URL__);
        define("__BOOKING_DETAILS_PAGE_URL__",__NEW_BOOKING_DETAILS_PAGE_URL__);
        define("__PRE_SERVICES_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/service");
        define("__BOOKING_PRICE_CHANGED_SIMPLE_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/simpleServices/price_changed");
        define("__SERVICE_PAGE_URL_SUFFIX__","best");
        define("__QUOTATION_PAGE_URL_SUFFIX__","search");
        define("__THANK_YOU_PAGE_URL_SUFFIX__","booking");  
        define("__BOOKING_GET_QUOTE_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/get-quote");
        define("__BOOKING_QUOTE_THANK_YOU_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/thanks"); 
        define("___BOOKING_QUOTE_THANK_YOU_PAGE_URL_SUFFIX__","rfq"); 
        define("__BOOKING_GET_RATES_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/get-rates");
        define("__BOOKING_TEST_GET_RATES_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/get-rates-v2");
        define("__BOOKING_GET_RATES_PAGE_V3_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/get-rates-v3");

        define("__OVERVIEW_PAGE_URL_SUFFIX__","complete");
        
        define("__BOOKING_QUOTE_SERVICE_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/booking");
        define("__BOOKING_QUOTE_SERVICE_PAGE_SUFFIX__","quote"); 
        define("__BOOKING_QUOTE_PAY_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/pay");  
        define("__BOOKING_QUOTE_EXPIRED_PAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/expired");  
        
    }
    else
    { 
        // booking pages url 
        define("__LANGUAGE__",$szLanguage);
        
        $_SESSION['user_selected_language'] = '';
        unset($_SESSION['user_selected_language']);
        
        $_SESSION['user_selected_language'] = $szLanguage ;
        define("__SELECT_SERVICES_URL__",__BASE_URL__."/services");
        //define("__LANDING_PAGE_URL__",__BASE_URL__."/search");
        //define("__REQUIREMENT_PAGE_URL__",__BASE_URL__."/requirement");
        define("__HOME_PAGE_URL__",__BASE_URL__);
        //define("__BOOKING_DETAILS_PAGE_URL__",__BASE_URL__."/bookings_details");
        //define("__BOOKING_CONFIRMATION_PAGE_URL__",__BASE_URL__."/bookings_confirmation");
        define("__BOOKING_RECEIPT_PAGE_URL__",__BASE_URL__."/booking_receipt/");
        define("__BOOKING_PAYMENT_PAGE_URL__",__BASE_URL__."/payment");
        define("__RSS_FEED_PAGE_URL__",__BASE_URL__."/news");
        define("__RSS_PAGE_URL__",__BASE_URL__."/rss");
        define("__BOOKING_PRICE_CHANGED_PAGE_URL__",__BASE_URL__."/search/price_changed");
        define("__EXPLAIN_PAGE_URL__",__BASE_URL__."/explain/");
        define("__BLOG_PAGE_URL__",__BASE_URL__."/learn/");
        define("__SITE_MAP_PAGE_URL__",__BASE_URL__."/sitemap");

        define("__TRANSPORTECA_LANDING_PAGE_URL__",__BASE_URL__."/landing_page");

        define("__FAQ_PAGE_URL__",__BASE_URL__."/faq");
        define("__CREATE_ACCOUNT_PAGE_URL__",__BASE_URL__."/createAccount/");
        define("__BASE_URL_FORWARDER_LOGO__",__BASE_URL__."/images/forwarders");
        define("__INFORMATION_PAGE_URL__",__BASE_URL__."/information");

        define("__OUR_COMPANY_PAGE_URL__",__BASE_URL__."/about");
        define("__STARTING_NEW_TRADE_PAGE_URL__",__BASE_URL__."/expansion");

        define("__HOW_IT_WORK_PAGE_URL__",__BASE_URL__."/forwarder");
        define("__FORWARDER_SIGNUP_PAGE_URL__",__BASE_URL__."/forwarderSignUp");

        define("__PARTNER_PROGRAM_PAGE_URL__",__BASE_URL__."/partnerProgramme");

        define("__TERMS_CONDITION_PAGE_URL__",__BASE_URL__."/terms");
        define("__PRIVACY_NOTICE_PAGE_URL__",__BASE_URL__."/privacyNotice");	
        define("__USER_LOGIN_URL__", __BASE_URL__."/login.php");
        define("__USER_LOGOUT_URL__", __BASE_URL__."/logout");
        define("__USER_MY_ACCOUNT_URL__",__BASE_URL__."/myAccount/");	
        //define("__SELECT_SIMPLE_SERVICES_URL__",__BASE_URL__."/simpleServices");
        //define("__SELECT_SIMPLE_SEARCH_URL__",__BASE_URL__."/simpleSearch/");

        define("__LANDING_PAGE_URL__",__BASE_URL__);
        define("__REQUIREMENT_PAGE_URL__",__BASE_URL__);

        define("__NEW_SEARCH_PAGE_URL__",__BASE_URL__."/newSearch/");
        define("__NEW_SERVICES_PAGE_URL__",__BASE_URL__."/services");
        define("__NEW_BOOKING_DETAILS_PAGE_URL__",__BASE_URL__."/details");
        define("__NEW_BOOKING_OVERVIEW_PAGE_URL__",__BASE_URL__."/overview");
        define("__NEW_BOOKING_TERMS_PAGE_URL__",__BASE_URL__."/booking-terms");
        define("__THANK_YOU_PAGE_URL__",__BASE_URL__."/thank-you");
        define("__SELECT_SIMPLE_SEARCH_URL__",__NEW_SEARCH_PAGE_URL__);
        define("__SELECT_SIMPLE_SERVICES_URL__",__NEW_SERVICES_PAGE_URL__);
        define("__BOOKING_CONFIRMATION_PAGE_URL__",__NEW_BOOKING_OVERVIEW_PAGE_URL__);
        define("__BOOKING_DETAILS_PAGE_URL__",__NEW_BOOKING_DETAILS_PAGE_URL__);

        define("__BOOKING_PRICE_CHANGED_SIMPLE_PAGE_URL__",__BASE_URL__."/simpleServices/price_changed");
        define("__SERVICE_PAGE_URL_SUFFIX__","best");
        define("__THANK_YOU_PAGE_URL_SUFFIX__","booking");

        define("__BOOKING_GET_QUOTE_PAGE_URL__",__BASE_URL__."/get-quote");
        define("__BOOKING_QUOTE_THANK_YOU_PAGE_URL__",__BASE_URL__."/thanks"); 
        define("___BOOKING_QUOTE_THANK_YOU_PAGE_URL_SUFFIX__","rfq");
        define("__BOOKING_GET_RATES_PAGE_URL__",__BASE_URL__."/get-rates"); 
        define("__BOOKING_TEST_GET_RATES_PAGE_URL__",__BASE_URL__."/get-rates-v2");
        define("__BOOKING_GET_RATES_PAGE_V3_URL__",__BASE_URL__."/get-rates-now");

        define("__QUOTATION_PAGE_URL_SUFFIX__","search");
        define("__OVERVIEW_PAGE_URL_SUFFIX__","complete");
        define("__BOOKING_QUOTE_SERVICE_PAGE_URL__",__BASE_URL__."/booking"); 
        define("__BOOKING_QUOTE_SERVICE_PAGE_SUFFIX__","quote"); 
        define("__BOOKING_QUOTE_PAY_PAGE_URL__",__BASE_URL__."/pay"); 
        define("__BOOKING_QUOTE_EXPIRED_PAGE_URL__",__BASE_URL__."/expired");  
    }
}
function createNewProfileAdmin($id,$t_base)
{	
	$t_base=trim($t_base);
	$id=(int)sanitize_all_html_input($id);
        $kConfig = new cConfig();
        $dialUpCodeAry = array();
        $dialUpCodeAry = $kConfig->getAllCountries(true,$iLanguage,true);
	if($id>0)
	{
            $kAdmin = new cAdmin(); 
            $adminRoleAry = array();
            $adminRoleAry = $kAdmin->getAllAdminRoles(); 
            ?>
            <div id="popup-bg"></div>
            <div id="popup-container">
                <div class="popup user-popup" style="width:450px;"> 
                        <div id="Error"></div>		
                        <h5><?=t($t_base.'title/create_new_user');?></h5>
                        <form id="createNewAdminProfile">
                                <label class="profile-fields">
                                        <span class="field-name"><?=t($t_base.'fields/f_name');?></span>
                                        <span class="field-container">
                                                <input type="text" name="createProfile[fName]" >
                                        </span>
                                </label> 
                                <label class="profile-fields">
                                        <span class="field-name"><?=t($t_base.'fields/l_name');?></span>
                                        <span class="field-container">
                                                <input type="text" name="createProfile[lName]">
                                        </span>
                                </label> 
                                <label class="profile-fields">
                                        <span class="field-name"><?=t($t_base.'fields/email');?></span>
                                        <span class="field-container">
                                                <input type="text" name="createProfile[eMail]" >
                                        </span>
                                </label>
                                <label class="profile-fields">
                                    <span class="field-name"><?=t($t_base.'fields/p_number');?><span></span></span>
                                    <span class="phone-container">
                                        <select name="createProfile[idInternationalDialCode]" id="idInternationalDialCode">
                                           <?php
                                              if(!empty($dialUpCodeAry))
                                              {
                                                   $usedDialCode = array();
                                                  foreach($dialUpCodeAry as $dialUpCodeArys)
                                                  {
                                                       if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                                                       {
                                                           $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ;
                                                       ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$idInternationalDialCode){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option>
                                                       <?php
                                                       }
                                                  }
                                              }
                                          ?>
                                        </select><input type="text" name="createProfile[szPhone]" id="szPhone" value="<?php echo $transportecaManagement['szPhone'] ?>" />
                                       </span>
                                </label> 
                                <label class="profile-fields">
                                        <span class="field-name"><?=t($t_base.'fields/password');?></span>
                                        <span class="field-container">
                                                <input type="password" name="createProfile[password]" size="26" >
                                        </span>
                                </label>
                                <label class="profile-fields">
                                        <span class="field-name"><?=t($t_base.'fields/all_access');?></span>
                                        <span class="field-container">
                                                <select name="createProfile[allAccess]">
                                                        <option value="0">No, cannot log in</option>
                                                        <option value="1"<?=($transportecaManagement['iAllAccess']?'selected=selected':'') ?>>Yes, user can log in</option>
                                                </select>
                                        </span>
                                </label> 
                                <label class="profile-fields">
                                    <span class="field-name"><?=t($t_base.'fields/profile');?></span>
                                    <span class="field-container">
                                        <select name="createProfile[szProfileType]" size="1" id="szProfileType"> 
                                           <?php
                                            if(!empty($adminRoleAry))
                                            { 
                                                foreach($adminRoleAry as $adminRoleArys)
                                                { 
                                                    ?><option value="<?php echo $adminRoleArys['szRoleCode']?>" <?php if($adminRoleArys['szRoleCode']==$szProfileType){?> selected <?php }?>><?php echo $adminRoleArys['szFriendlyName']?></option><?php
                                                }
                                            }
                                          ?>
                                        </select>
                                    </span>
                                </label>
                                <!--<label class="profile-fields">
                                    <span class="field-name"><?=t($t_base.'fields/capsule');?></span>
                                    <span class="field-container">
                                        <input type="text" name="createProfile[szCapsule]" value ="<?=$transportecaManagement['szCapsule']?>">
                                    </span>
                                </label>-->
                                <label class="profile-fields">
                                    <span class="field-name"><?=t($t_base.'fields/show_owner_dropdown');?></span>
                                    <span class="field-container">
                                        <input type="checkbox" name="createProfile[iShowInDropDown]" value ="1" <?php if($transportecaManagement['iShowInDropDown']=='1'){?> checked <?php }?>>
                                    </span>
                                </label> 
                                <input type="hidden" name="flag" value="CREATE_NEW_PROFILE_ADMIN_CONFIRM"> 
                        </form>
                        <br />
                        <div class="oh">
                            <p align="center">	
                                <a href="javascript:void(0);" onclick="submitAdminProfileDetails(<?=$id?>)" class="button1"><span><?=t($t_base.'fields/confirm');?></span></a>			
                                <a href="javascript:void(0);" onclick="cancelEditMAnageVar(0)" class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>			
                            </p>
                        </div>
                </div></div>
		<?php
	}
}

function format_volume($fCargoVolume,$iLanguage=false)
{  
    $fRetCargoVol = round($fCargoVolume,4);   
    //echo $fRetCargoVol."fRetCargoVol";
    if(!empty($fCargoVolume))
    {
       $fCargoVolume = round($fCargoVolume,3); 
      // echo "<br> Vol: ".$fCargoVolume."/<br>";
        $volumeAry = explode(".",$fCargoVolume);
        if($volumeAry[0]>99)
        {
            $fRetCargoVol = number_format_custom($fCargoVolume,$iLanguage,0);
        }
        else if($volumeAry[0]>9 && $volumeAry[0]<=99)
        { 
            $fRetCargoVol = number_format_custom($fCargoVolume,$iLanguage,1);
        }
        else
        { 
            if($volumeAry[0]<1)
            {
                $iDecimalVal=substr($volumeAry[1],0,2);
                if($iDecimalVal=='00')
                {
                    $fRetCargoVol = number_format_custom($fCargoVolume,$iLanguage,3);
                }
                else
                {
                    $fRetCargoVol = number_format_custom($fCargoVolume,$iLanguage,2);  
                }
            }
            else
            {
                
                $fRetCargoVol = number_format_custom($fCargoVolume,$iLanguage,2);
            }
        }
    } 
    return $fRetCargoVol; 
}
function format_volume_backup($fCargoVolume,$iLanguage=false)
{
    $fRetCargoVol = 0.0; 
    if(!empty($fCargoVolume))
    {
        $volumeAry = explode(".",$fCargoVolume);
        if($volumeAry[0]>99)
        {
            $fRetCargoVol = number_format_custom($fCargoVolume,$iLanguage,0);
        }
        else if($volumeAry[0]>9 && $volumeAry[0]<=99)
        { 
            $fRetCargoVol = number_format_custom($fCargoVolume,$iLanguage,1);
        }
        else
        { 
            $fRetCargoVol = number_format_custom($fCargoVolume,$iLanguage,2);
        }
    } 
    return $fRetCargoVol; 
}
function number_format_custom($fValue,$iLanguage,$iDecimals=0)
{
    if($iLanguage>0)
    {
        $kConfig =new cConfig();
        $langArr=$kConfig->getLanguageDetails('',$iLanguage);
    }
    
    if(!empty($langArr))
    {
        $szDecimalSeprator = $langArr[0]['szDecimalSeparator'];
        $szThousandsSeprator = $langArr[0]['szThousandsSeparator'];
    } 
    else
    {
        $szDecimalSeprator = ".";
        $szThousandsSeprator = ",";
    }
    return number_format($fValue,$iDecimals,$szDecimalSeprator,$szThousandsSeprator);
}
function editProfileAdmin($transportecaManagement,$t_base)
{	
	//print_r($transportecaManagement);
	$t_base=trim($t_base);
	$id=(int)sanitize_all_html_input($id);
	if($transportecaManagement!=array())
	{
            $kConfig = new cConfig();
            $kAdmin = new cAdmin();
            
            $dialUpCodeAry = array();
            $dialUpCodeAry = $kConfig->getAllCountries(true,$iLanguage,true); 
            $idInternationalDialCode = $transportecaManagement['idInternationalDialCode'];
            
            $szProfileType = $transportecaManagement['szProfileType'];
            
            $adminRoleAry = array();
            $adminRoleAry = $kAdmin->getAllAdminRoles();
        ?>
            <div id="popup-bg"></div>
            <div id="popup-container">
                <div class="popup user-popup" style="width:450px;"> 
                <div id="Error"></div>		
                <h5><?=t($t_base.'title/edit_admin_details');?></h5>
                <form id="createNewAdminProfile">
                    <label class="profile-fields">
                        <span class="field-name"><?=t($t_base.'fields/f_name');?></span>
                        <span class="field-container">
                            <input type="text" name="createProfile[fName]" value ="<?=$transportecaManagement['szFirstName']?>">
                        </span>
                    </label> 
                    <label class="profile-fields">
                        <span class="field-name"><?=t($t_base.'fields/l_name');?></span>
                        <span class="field-container">
                            <input type="text" name="createProfile[lName]" value ="<?=$transportecaManagement['szLastName']?>">
                        </span>
                    </label> 
                    <label class="profile-fields">
                        <span class="field-name"><?=t($t_base.'fields/email');?></span>
                        <span class="field-container">
                            <input type="text" name="createProfile[eMail]" value ="<?=$transportecaManagement['szEmail']?>" >
                        </span>
                    </label>
                    <label class="profile-fields">
                        <span class="field-name"><?=t($t_base.'fields/p_number');?><span></span></span>
                        <span class="phone-container">
                            <select size="1"  name="createProfile[idInternationalDialCode]" id="idInternationalDialCode">
                               <?php
                                  if(!empty($dialUpCodeAry))
                                  {
                                    $usedDialCode = array();
                                    foreach($dialUpCodeAry as $dialUpCodeArys)
                                    {
                                           if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                                           {
                                               $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ;
                                           ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$idInternationalDialCode){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option>
                                           <?php
                                           }
                                      }
                                  }
                              ?>
                            </select>
                            <input type="text" name="createProfile[szPhone]" id="szPhone" value="<?php echo $transportecaManagement['szPhone'] ?>" />
                           </span>
                    </label>
                    <label class="profile-fields">
                        <span class="field-name"><?=t($t_base.'fields/all_access');?></span>
                        <span class="field-container">
                            <select name="createProfile[allAccess]">
                                <option value="0">No, cannot log in</option>
                                <option value="1"<?=($transportecaManagement['iAllAccess']?'selected=selected':'') ?>>Yes, user can log in</option>
                            </select>
                        </span>
                    </label> 
                    <label class="profile-fields">
                        <span class="field-name"><?=t($t_base.'fields/profile');?></span>
                        <span class="field-container">
                            <select name="createProfile[szProfileType]" size="1" id="szProfileType"> 
                               <?php
                                if(!empty($adminRoleAry))
                                { 
                                    foreach($adminRoleAry as $adminRoleArys)
                                    { 
                                        ?><option value="<?php echo $adminRoleArys['szRoleCode']?>" <?php if($adminRoleArys['szRoleCode']==$szProfileType){?> selected <?php }?>><?php echo $adminRoleArys['szFriendlyName']?></option><?php
                                    }
                                }
                              ?>
                            </select>
                        </span>
                    </label>
                    <!--<label class="profile-fields">
                        <span class="field-name"><?=t($t_base.'fields/capsule');?></span>
                        <span class="field-container">
                            <input type="text" name="createProfile[szCapsule]" value ="<?=$transportecaManagement['szCapsule']?>">
                        </span>
                    </label>--> 
                    <label class="profile-fields">
                        <span class="field-name"><?=t($t_base.'fields/show_owner_dropdown');?></span>
                        <span class="field-container">
                            <input type="checkbox" name="createProfile[iShowInDropDown]" value ="1" <?php if($transportecaManagement['iShowInDropDown']=='1'){?> checked <?php }?>>
                        </span>
                    </label> 
                    <input type="hidden" name="flag" value="EDIT_TRANSPORTECA_ADMIN_DETAILS_CONFIRM"> 
                    <input type="hidden" name="createProfile[id]" value="<?=$transportecaManagement['id']?>">
                </form>
                <br />
                <div class="oh">
                    <p align="center">	
                        <a href="javascript:void(0);" onclick="editAdminProfileDetails()" class="button1"><span><?=t($t_base.'fields/confirm');?></span></a>			
                        <a href="javascript:void(0);" onclick="cancelEditMAnageVar(0)" class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>			
                    </p>
                </div>
            </div>
        </div>
		<?php
	}
}
function forwarderProfileHeader($activeFlag,$idForwarder)
{	
	$t_base = "management/AllProfiles/";
	//$activeFlag='';
	?>
	<div class="hsbody-2-left account-links">
	<h5><strong><?=t($t_base.'title/edit_forwarder_profile');?></strong></h5>
			
		<ul>
			<li onclick="forwarder_profile_edit(<?=$idForwarder;?>)" <? if(trim($activeFlag)=='user' || $activeFlag==''){?> style="border-bottom: 3px solid #C4BDA1;margin-bottom: 0;" <? }?>>Settings</li>
			<li onclick="forwarder_profile_show_history('<?=$idForwarder;?>')"  <? if($activeFlag=='ForwarderHistory'){?> style="border-bottom: 3px solid #C4BDA1;margin-bottom: 0;" <? }?>>Profile history</li>
			<li onclick="deleteForwarderProfile(<?=$idForwarder;?>)"  <? if($activeFlag=='ForwarderProfile'){?> style="border-bottom: 3px solid #C4BDA1;margin-bottom: 0;" <? }?>>Delete profile</li>
		</ul>
	</div>
	<?php
}
function deleteForwarderByAdmin($t_base,$idForwarder,$szFlag='',$szSearchStr='')
{
	?>
		<div id="popup-bg"></div>
			 <div id="popup-container">	
				<div class="popup contact-popup">
					<h5><?=t($t_base.'title/delete_profile');?></h5>	
					<p><?=t($t_base.'title/confirm_delete_profile');?></p><br />
					<div class="oh">
						<p align="center">	
							<a href="javascript:void(0);" onclick="deleteForwarderProfileConfirm('<?=$idForwarder?>','<?php echo $szFlag;?>','<?php echo $szSearchStr;?>')" class="button1"><span><?=t($t_base.'fields/yes');?></span></a>
                                                        <?php if($szFlag=='FROM_HEADER'){?>
                                                            <a href="javascript:void(0);" onclick="display_remind_pane_add_popup('','','','','FROM_HEADER','<?php echo $szSearchStr;?>')" class="button2"><span><?=t($t_base.'fields/no');?></span></a>
                                                        <?php }else{?>
							<a href="javascript:void(0);" onclick="cancelDeleteForwarderProfile(0)" class="button2"><span><?=t($t_base.'fields/no');?></span></a>
                                                        <?php }?>
						</p>
					</div>
				</div>
			</div>
	<?php
}
function createNewForwarderProfileByAdmin($t_base,$forwarderIdName)
{	
	$t_base=trim($t_base);
	
	switch(!empty($_POST['editUserInfoArr']['idForwarderContactRole'])?$_POST['editUserInfoArr']['idForwarderContactRole']:$kForwarderContact->idForwarderContactRole)
	{
		CASE 1:
		{
		$szContactRole1 ="selected='selected'";
		BREAK;
		}
		CASE 3:
		{
		$szContactRole3 ="selected='selected'";
		BREAK;
		}
		CASE 5:
		{
		$szContactRole5 ="selected='selected'";
		BREAK;
		}
	}
	?>
		<div id="popup-bg"></div>
					<div id="popup-container">
						<div class="popup"> 
			<div id="Error"></div>				
				<h5><strong><?=t($t_base.'title/create_new_forwarder');?></strong></h5>
				<form id="createNewAdminProfile">
					<label class="profile-fields">
						<span class="field-name"><?=t($t_base.'fields/forwarder');?></span>
						<span class="field-container">
							<select name="createProfile[forwarderId]" style="width:289px;">
								<?
								if($forwarderIdName!=array())
								{
									foreach($forwarderIdName as $fwdName)
									{
										echo "<option value='".$fwdName['id']."'>".$fwdName['szDisplayName']."</option>";
									}
								}
								?>
						</select>
						</span>
					</label>
					
					<label class="profile-fields">
						<span class="field-name"><?=t($t_base.'fields/profile');?></span>
						<span class="field-container">
							<select name="createProfile[idForwarderContactRole]" id="szCountry" style="width:289px;">
								<option value="">Select <?=t($t_base.'fields/access_rights');?> </option>
								<option value="1" <?=$szContactRole1?>>Administrator</option>
								<option value="3" <?=$szContactRole3?>>Pricing & Services</option>
								<option value="5" <?=$szContactRole5?>>Bookings & Billing</option>	
							</select>
						</span>
					</label>
					
					<label class="profile-fields">
						<span class="field-name"><?=t($t_base.'fields/email');?></span>
						<span class="field-container">
							<input type="text" name="createProfile[eMail]">
						</span>
					</label>
					
					<input type="hidden" name="newflag" value="CREATE_NEW_FORWARDER_PROFILE_CONFIRM">
				</form>
				<br />				
				<p align="center">	
					<a href="javascript:void(0);" onclick="cancelEditMAnageVar(0)" class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>
					<a href="javascript:void(0);" onclick="submitForwarderProfileDetails()" class="button1"><span><?=t($t_base.'fields/create');?></span></a>			
				</p>
			</div></div>	
			
	<?
}
function editCustomerMenu($activeFlag,$idUser,$divId)
{
	$kUser = new cUser();
	$kUser->getUserDetails($idUser);
	$t_base="management/AllProfiles/";
?>
<div class="hsbody-2-left account-links">
	<h4 style="padding-bottom: 5px;"><?=t($t_base.'title/edit_customer');?></h4>
		
	<ul>
			<li <? if(trim($activeFlag)=='setting'){?> class="active" <? }?>><a class="cursor-pointer" onclick="showEditCustomerPages('setting','<?=$idUser?>')"><?=t($t_base.'links/settings');?></a></li>
			<li <? if($activeFlag=='shipperConsign'){?> class="active" <? }?>><a class="cursor-pointer" onclick="showEditCustomerPages('shipperConsign','<?=$idUser?>')"><?=t($t_base.'links/registered_shippers_and_consignees')?></a></li>
			<li <? if($activeFlag=='multiUser'){?> class="active" <? }?>><a class="cursor-pointer" onclick="showEditCustomerPages('multiUser','<?=$idUser?>')"><?=t($t_base.'links/multi_user_access');?></a></li>
			<li <? if($activeFlag=='accountHis'){?> class="active" <? }?>><a class="cursor-pointer" onclick="showEditCustomerPages('accountHis','<?=$idUser?>')"><?=t($t_base.'links/history');?></a></li>
			<? if((int)$kUser->iActive=='1') {?>
			<li     <? if($activeFlag=='deleteAcc'){?> class="active" <? }?>><a class="cursor-pointer" onclick="showEditCustomerPages('deleteAcc','<?=$idUser?>')"><?=t($t_base.'links/delete_account');?></a></li>
			<? }else {?>
				<li    <? if($activeFlag=='deleteAcc'){?> class="active" <? }?>><a class="cursor-pointer" onclick="showEditCustomerPages('reActive','<?=$idUser?>')"><?=t($t_base.'links/reactive_account');?></a></li>
			<? }?>
			<li><a onclick="showDifferentProfileForCustomer('cust','<?=$divId;?>');" class="cursor-pointer" >Back</a></li>		
		</ul>
	</div>	
<?php	
}

function userProfilePage($t_base,$idUser,$error='',$divId)
{
	editCustomerMenu('setting',$idUser,$divId);
	?>
	<div class="hsbody-2-right">
	<?php	
	if(!empty($error)){
	$t_base_error = "Error";
	?>
	
	<div id="regError" class="errorBox">
	<div class="header"><?=t($t_base_error.'/please_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
		foreach($error as $key=>$values)
		{
		?><li><?=$values?></li>
		<?php	
		}
	?>
	</ul>
	</div>
	</div>
	<?php }
	$kUser = new cUser();
	$kConfig = new cConfig();
	$kAdmin = new cAdmin();
	
	$kUser->getUserDetails($idUser);
	
	//print_r($_POST);
	//print_r($_POST['editUserInfoArr']['szFirstName']);
	$iLanguage = getLanguageId();
	$allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);
	$allCurrencyArr=$kConfig->getBookingCurrency(false,true);
        
        $kConfig = new cConfig();
        $kConfig->loadCountry($kUser->idInternationalDialCode);
        $iInternationDialCode = $kConfig->iInternationDialCode;
	?>	
	
	<div id="myaccount_info">
				<div id="user_information">
				<h5><strong><?=t($t_base.'fields/user_info');?></strong> <a href="javascript:void(0)" onclick="editCustomerInformation(<?=$idUser?>);"><?=t($t_base.'fields/edit');?></a></h5>	
				
				<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/f_name');?></span>
					<span class="field-container"><?=$kUser->szFirstName?></span>
				</div>
				<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/l_name');?></span>
					<span class="field-container"><?=$kUser->szLastName?></span>
				</div>
                                <div class="ui-fields">
                                    <span class="field-name">&nbsp;</span>
                                    <span class="field-container checkbox-container"><input type="checkbox" disabled="disabled" <?php echo ($kUser->iPrivate==1)?'checked="checked"':''; ?> > <span class="private-text">Private</span></span>
                                </div>
                                <div class="ui-fields">
                                        <span class="field-name"><?=t($t_base.'fields/language');?></span>
                                        <span class="field-container"><?php echo ($kUser->szCustomerLanguage!='')?ucwords($kUser->szCustomerLanguage):'N/A'; ?></span>
                                </div>
                                <?php if($kUser->iPrivate!=1){?>
                                <div class="ui-fields">
                                    <span class="field-name"><?=t($t_base.'fields/c_name');?></span>
                                    <span class="field-container"><?=$kUser->szCompanyName?></span>
                                </div>
                                <div class="ui-fields">
                                    <span class="field-name"><?=t($t_base.'fields/c_reg_n');?></span>
                                    <span class="field-container"><?=$kUser->szCompanyRegNo?></span>
                                </div>
                                <?php } ?>  
				<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/address_line1');?></span>
					<span class="field-container"><?=$kUser->szAddress1?></span>
				</div>
				<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/address_line2');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
					<span class="field-container"><?=$kUser->szAddress2?></span>
				</div>
				<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/address_line3');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
					<span class="field-container"><?=$kUser->szAddress3?></span>
				</div>
				<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/postcode');?></span>
					<span class="field-container"><?=$kUser->szPostcode?></span>
				</div>
				<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/city');?></span>
					<span class="field-container"><?=$kUser->szCity?></span>
				</div>
				<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/p_r_s');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
					<span class="field-container"><?=$kUser->szState?></span>
				</div>
				<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/country');?></span>
					<span class="field-container"><?=$kUser->szCountryName?></span>
				</div>
				<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/prefer_currency');?></span>
					<span class="field-container">
						<?=$kUser->szCurrencyName?>
					</span>
				</div>
				</div>
				<br />
				<br />
				<div id="contact_info">
					<h5><strong><?=t($t_base.'messages/contact_info');?></strong> <a href="javascript:void(0)" onclick="edit_customer_contact_information(<?=$idUser?>);"><?=t($t_base.'fields/edit');?></a></h5>
					
					<div class="ui-fields">
                                            <span class="field-name"><?=t($t_base.'fields/email');?></span>
                                            <span class="field-container"><?=$kUser->szEmail?> 
                                                <?php if($kUser->iConfirmed!='1'){?>
                                                    <span style='color:red;font-size:13px;'>(<?=t($t_base.'messages/not_verified');?>)</span> 
                                                    <a href="javascript:void(0)" onclick="display_verify_popup('<?php echo $kUser->id; ?>','VERIFY_USER_EMAIL');">E-mail verified</a>
                                                <?php }?>
                                            </span>					
					</div>
					<div class="ui-fields">
						<span class="field-name"><?=t($t_base.'fields/contact_by_email');?></span>
						<span class="field-container checkbox-ab"><input type="checkbox" <?php if((int)$kUser->iAcceptNewsUpdate=='1'){?> checked <?php }?> disabled="disabled"/><?=t($t_base.'messages/please_news_updates');?></span>
					</div>
					<div class="ui-fields">
						<span class="field-name"><?=t($t_base.'fields/p_number');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
						<span class="field-container"><?php echo "+".$iInternationDialCode." ".$kUser->szPhoneNumber; ?></span>
					</div>
				</div>
				<br />
				<br/>
				<div id="user_change_password">					
						<h5 style="width:30%;float:left;"><strong><?=t($t_base.'fields/password');?></strong> </h5>
						<span class="field-container" style="float:left;width:65%;font-style: italic;cursor: pointer;" id="customerSend"><u onclick="send_new_password_customer(<?=$idUser?>)"><?=t($t_base.'fields/send_new_password');?></u></span>					
				</div>
			</div>
			</div>
	<?php
}
function userShipperConsignees($t_base,$idUser,$divId)
{
	$kRegisterShipCon = new cRegisterShipCon();
	editCustomerMenu('shipperConsign',$idUser,$divId);
	$regShipperArr=$kRegisterShipCon->getRegisteredShipperConsigness('ship',$idUser);
	$regConArr=$kRegisterShipCon->getRegisteredShipperConsigness('con',$idUser);
	
	?>
	<div class="hsbody-2-right">
	<div id="registerShipConsignList"></div>
	<form name="removeShipperConsigness" id="removeShipperConsigness" method="post"> 
	<div class="oh registered-textarea">
		<div class="fl-49">
			<h5><?=t($t_base.'fields/register_shipper');?></h5>
			<p>
				<select multiple="multiple" name="regShipper[]" id="regShipper" onClick="remove_value_select_box_admin('shipper','<?=$idUser?>');" ondblclick="edit_register_shipper_consigness_by_admin(this.value,'<?=$idUser?>');">
				<?php
					if(!empty($regShipperArr)){
						foreach($regShipperArr as $regShipperArrs)
						{
						?>
							<option value="<?=$regShipperArrs['id']?>" ><?=$regShipperArrs['szCompanyName']?> (<?=$regShipperArrs['szCountryName']?>)</option>
						<? }
					}
					?>
				</select>
			</p>
		</div>
		<div class="fl-49 consignee">
			<h5><?=t($t_base.'fields/register_consignees');?></h5>
			<p>
				<select multiple="multiple" name="regCon[]" id="regCon" onClick="remove_value_select_box_admin('consignes','<?=$idUser?>');">
				<?php
					if(!empty($regConArr)){
						foreach($regConArr as $regConArrs)
						{?>
							<option value="<?=$regConArrs['id']?>" ><?=$regConArrs['szCompanyName']?> (<?=$regConArrs['szCountryName']?>)</option>
						<? }
					}
					?>
				</select>
			</p>
		</div>
	</div>
	
	<div class="oh">
		<input type="hidden" name="remove" value="1">
		<input type="hidden" name="idUser" value="<?=$idUser?>">
		
		<p align="left"  class="fl-80 f-size-12">
		<em>* <?=t($t_base.'title/register_by_another');?></em></p>
		<p class="fl-20" align="right" >
			<a href="javascript:void(0)" class="button1" id="remove" style="opacity:0.4;float: right;" onclick=""><span><?=t($t_base.'fields/remove');?></span></a>
		</p>
	</div>
</form>

<?
}

function remove_shipper_consignees($t_base,$idUser)
{
$kRegisterShipCon = new cRegisterShipCon();
?>
	<div id="remove_ship_con_id">
			<div id="popup-bg"></div>
			<div id="popup-container">
			<div class="popup remove-shippers-popup">
			
				<?php
				$heading='';
				if(count($_REQUEST['regShipper'])>0)
				{
					$shipperConsignFlag=false;
					$remove_arr=implode(",",$_REQUEST['regShipper']);
					
					if(!empty($_REQUEST['regShipper']))
					{
						$regShipper=$_REQUEST['regShipper'];
						for($i=0;$i<count($regShipper);$i++)
						{
							$kRegisterShipCon->getShipperConsignessDetail($regShipper[$i]);
							$Type=$kRegisterShipCon->idGroup;
							if($Type==3)
							{
								$shipperConsignFlag=true;
							}
						}
					}
					
					//$remove_msg=t($t_base.'messages/shipper');
					
					if(count($_REQUEST['regShipper'])==1)
					{
						$heading=t($t_base.'messages/remove_shipper');
						$remove_msg=t($t_base.'messages/shipper');
					}
					else
					{
						$total=count($_REQUEST['regShipper']);
						$heading=t($t_base.'messages/remove_shippers');
						$remove_msg=t($t_base.'messages/shippers');
					}
				}
				if(count($_REQUEST['regCon'])>0)
				{
					$shipperConsignFlag=false;
					$remove_arr=implode(",",$_REQUEST['regCon']);
					
					if(!empty($_REQUEST['regCon']))
					{
						$regCon=$_REQUEST['regCon'];
						for($i=0;$i<count($regCon);$i++)
						{
							$kRegisterShipCon->getShipperConsignessDetail($regCon[$i]);
							$Type=$kRegisterShipCon->idGroup;
							if($Type==3)
							{
								$shipperConsignFlag=true;
							}
						}
					}
					//echo $Type;
					if(count($_REQUEST['regCon'])==1)
					{
						$heading=t($t_base.'messages/remove_consignee');
						$remove_msg=t($t_base.'messages/consignee');
					}
					else
					{
						$total=count($_REQUEST['regCon']);
						$heading=t($t_base.'messages/remove_consignees');
						$remove_msg=t($t_base.'messages/consign');
					}
				}
				?>
				<form name="remove_confirm" id="remove_confirm" method="post">
				<h5><?=$heading?></h5>
				<p><?=t($t_base.'messages/would_you_like');?> <?=$total?> <?=t($t_base.'messages/selected');?> <?=$remove_msg?>?
				<?php 
					if($shipperConsignFlag)
					{
						echo "<br/><br>".t($t_base.'messages/reg_shipper_consignees_removed_both');
					}
				?>
				</p>
				<input type="hidden" name="removeArr" id="removeArr" value="<?=$remove_arr?>">
				<input type="hidden" name="flag" id="flag" value="deleteShipperCongign">
				<input type="hidden" name="idUser" id="idUser" value="<?=$idUser?>">
				<br />
				<p align="center"><a href="javascript:void(0)" class="button1" onclick="remove_shipper_con_data_admin();"><span><?=t($t_base.'fields/confirm');?></span></a> <a href="javascript:void(0)" class="button1" onclick="cancel_remove_popup_admin('shipperConsign');"><span><?=t($t_base.'fields/cancel');?></span></a></p>
				</form>
			</div>
			</div>
			</div>
		</div>	
	<?php
}

function shipperConsignEditForm($t_base,$idUser=0,$shipConsignId=0,$errorArr='')
{
	$kRegisterShipCon = new cRegisterShipCon();
	$kConfig = new cConfig();
	$iLanguage = getLanguageId();
	$allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);
	$shipcongroupArr=$kRegisterShipCon->shipconGroup($iLanguage);
	if((int)$shipConsignId>0)
	{
		$kRegisterShipCon->getShipperConsignessDetail($shipConsignId);
	} 
	?>
<div id="registerShipConsignEdit">
<form name="registerShipperConsigness" id="registerShipperConsigness" method="post">
	<?php
		//print_r($_POST['registerShipperCongArr']);
	if(!empty($errorArr)){
	$t_base_error = "Error";
	?>
	<div id="regError" class="errorBox">
	<div class="header"><?=t($t_base_error.'/please_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
		foreach($errorArr as $key=>$values)
		{
		?><li><?=$values?></li>
		<?php	
		}
	?>
	</ul>
	</div>
	</div>
	<script type="text/javascript">
	$("#shipper_consignee_button").attr("style","opacity:1;");
	$("#shipper_consignee_button").attr("onclick","save_register_shiper_consign()");
	</script>
	<?php }?>
	<script type="text/javascript">
	$().ready(function() {	
	$("#szPostCode").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
	
	$("#szCity").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});	
});
	</script>
	<p><?=t($t_base.'title/dbl_clk_to');?></p>
	<br/>
	<label class="profile-fields">
		<span class="field-name"><?=t($t_base.'title/registered_as');?></span>
		<span class="field-container" >
			<select style="min-width:220px;" name="registerShipperCongArr[idGroup]" id="idGroup" onblur="closeTip('shipcong');" onfocus="openTip('shipcong');">
				<?php
					if(!empty($shipcongroupArr)){
						foreach($shipcongroupArr as $shipcongroupArrs)
						{
					?><option value="<?=$shipcongroupArrs['id']?>" <?=((($_POST['registerShipperCongArr']['idGroup'])?$_POST['registerShipperCongArr']['idGroup']:$kRegisterShipCon->idGroup) ==  $shipcongroupArrs['id'] ) ? "selected":""?>><?=$shipcongroupArrs['szDescription']?></option>	
					<?php	}
					}
					?>
			</select>
		</span>
		<div class="field-alert"><div id="shipcong" style="display:none;"><?=t($t_base.'messages/register_as_msg');?></div></div>
	</label>
	<label class="profile-fields">
		<span class="field-name"><?=t($t_base.'fields/c_name');?> </span>
		<span class="field-container"><input type="text" name="registerShipperCongArr[szCompanyName]" id="szCompanyName" onblur="closeTip('cname');" onfocus="openTip('cname');" value="<?=$_POST['registerShipperCongArr']['szCompanyName'] ? $_POST['registerShipperCongArr']['szCompanyName'] : $kRegisterShipCon->szCompanyName ?>"/></span>
		<div class="field-alert"><div id="cname" style="display:none;"><?=t($t_base.'messages/company_name');?></div></div>
	</label>
	<label class="profile-fields">
		<span class="field-name"><?=t($t_base.'fields/f_name');?></span>
		<span class="field-container"><input type="text" name="registerShipperCongArr[szFirstName]" id="szFirstName" value="<?=$_POST['registerShipperCongArr']['szFirstName'] ? $_POST['registerShipperCongArr']['szFirstName'] : $kRegisterShipCon->szFirstName ?>" onblur="closeTip('fname');" onfocus="openTip('fname');"/></span>
		<div class="field-alert"><div id="fname" style="display:none;"><?=t($t_base.'messages/contact_person');?></div></div>
	</label>
	<label class="profile-fields">
		<span class="field-name"><?=t($t_base.'fields/l_name');?></span>
		<span class="field-container"><input type="text" name="registerShipperCongArr[szLastName]" id="szLastName" value="<?=$_POST['registerShipperCongArr']['szLastName'] ? $_POST['registerShipperCongArr']['szLastName'] : $kRegisterShipCon->szLastName ?>" onblur="closeTip('lname');" onfocus="openTip('lname');"/></span>
		<div class="field-alert"><div id="lname" style="display:none;"><?=t($t_base.'messages/contact_person');?></div></div>
	</label>
	<label class="profile-fields">
		<span class="field-name"><?=t($t_base.'fields/email');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
		<span class="field-container"><input type="text" name="registerShipperCongArr[szEmail]" id="szEmail" value="<?=$_POST['registerShipperCongArr']['szEmail'] ? $_POST['registerShipperCongArr']['szEmail'] : $kRegisterShipCon->szEmail ?>" onblur="closeTip('email');" onfocus="openTip('email');"/></span>
		<div class="field-alert"><div id="email" style="display:none;"><?=t($t_base.'messages/email_msg');?></div></div>
	</label>
	<label class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/p_number');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
			<span class="field-container"><input type="text" name="registerShipperCongArr[szPhoneNumber]" id="szPhoneNumber" onblur="closeTip('pNumber');" onfocus="openTip('pNumber');" value="<?=$_POST['registerShipperCongArr']['szPhoneNumber'] ? $_POST['registerShipperCongArr']['szPhoneNumber'] : $kRegisterShipCon->szPhoneNumber ?>"/></span>
			<div class="field-alert1" style="left:60.5%;width: 290px;"><div id="pNumber" style="display:none;"><?=t($t_base.'messages/phone_no_msg');?></div></div>
		</label>
	<label class="profile-fields">
		<span class="field-name"><?=t($t_base.'fields/country');?></span>
		<span class="field-container">
			<select style="max-width:220px;" size="1" name="registerShipperCongArr[szCountry]" id="szCountry">
				<?php
					if(!empty($allCountriesArr))
					{
						foreach($allCountriesArr as $allCountriesArrs)
						{
							?><option value="<?=$allCountriesArrs['id']?>" <?=((($_POST['registerShipperCongArr']['szCountry'])?$_POST['registerShipperCongArr']['szCountry']:$kRegisterShipCon->szCountry) ==  $allCountriesArrs['id'] ) ? "selected":""?>><?=$allCountriesArrs['szCountryName']?></option>
							<?php
						}
					}
				?>
			</select>
		</span>
		</label>
		<label class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/city');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
			<span class="field-container"><input type="text" name="registerShipperCongArr[szCity]" id="szCity" onblur="closeTip('city');" onfocus="openTip('city');" value="<?=$_POST['registerShipperCongArr']['szCity'] ? $_POST['registerShipperCongArr']['szCity'] : $kRegisterShipCon->szCity ?>"/></span>
			<div class="field-alert"><div id="city" style="display:none;"><?=t($t_base.'messages/city_blue_box_msg');?></div></div>
		</label>
		<label class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/postcode');?></span>
			<span class="field-container"><input type="text" name="registerShipperCongArr[szPostCode]" id="szPostCode" onblur="closeTip('postcode');" onfocus="openTip('postcode');" value="<?=$_POST['registerShipperCongArr']['szPostCode'] ? $_POST['registerShipperCongArr']['szPostCode'] : $kRegisterShipCon->szPostcode ?>"/></span>
			<div class="field-alert"><div id="postcode" style="display:none;"><?=t($t_base.'messages/postcode_blue_box_msg');?></div></div>
		</label>
	<label class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/address');?></span>
			<span class="field-container"><input type="text" name="registerShipperCongArr[szAddress1]" id="szAddress1" onblur="closeTip('address1');" onfocus="openTip('address1');" value="<?=$_POST['registerShipperCongArr']['szAddress1'] ? $_POST['registerShipperCongArr']['szAddress1'] : $kRegisterShipCon->szAddress1 ?>"/></span>
			<div class="field-alert"><div id="address1" style="display:none;"><?=t($t_base.'messages/address_blue_box_msg');?></div></div>
		</label>
		<label class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/address1');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
			<span class="field-container"><input type="text" name="registerShipperCongArr[szAddress2]" id="szAddress2" onblur="closeTip('address2');" onfocus="openTip('address2');" value="<?=$_POST['registerShipperCongArr']['szAddress2'] ? $_POST['registerShipperCongArr']['szAddress2'] : $kRegisterShipCon->szAddress2 ?>"/></span>
			<div class="field-alert"><div id="address2" style="display:none;"><?=t($t_base.'messages/address_blue_box_msg');?></div></div>
			
		</label>
		<label class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/address2');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
			<span class="field-container"><input type="text" name="registerShipperCongArr[szAddress3]" id="szAddress3" onblur="closeTip('address3');" onfocus="openTip('address3');" value="<?=$_POST['registerShipperCongArr']['szAddress3'] ? $_POST['registerShipperCongArr']['szAddress3'] : $kRegisterShipCon->szAddress3 ?>"/></span>
			<div class="field-alert1" style=" left: 60.5%; width: 290px;"><div id="address3" style="display:none;"><?=t($t_base.'messages/address_blue_box_msg');?></div></div>
		</label>
		<label class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/province');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
			<span class="field-container"><input type="text" name="registerShipperCongArr[szProvince]" id="szProvince" onblur="closeTip('p_r_s');" onfocus="openTip('p_r_s');" value="<?=$_POST['registerShipperCongArr']['szProvince'] ? $_POST['registerShipperCongArr']['szProvince'] : $kRegisterShipCon->szState ?>"/></span>
			<div class="field-alert1" style=" left: 60.5%; width: 290px;"><div id="p_r_s" style="display:none;"><?=t($t_base.'messages/p_r_s');?></div></div>
		</label>
	<br />
	<br />
	<p align="center">
	<input type="hidden" name="registerShipperCongArr[idShipperConsigness]" id="idShipperConsigness" value="<?=$_POST['registerShipperCongArr']['idShipperConsigness'] ? $_POST['registerShipperCongArr']['idShipperConsigness'] : $shipConsignId ?>">
	<input type="hidden" name="flag" id="flag" value="<?=$_REQUEST['flag']?>">
	<input type="hidden" name="registerShipperCongArr[idCustomer]" id="idCustomer" value="<?=$idUser?>">
	<input type="hidden" name="idUser" id="idUser" value="<?=$idUser?>">
	<input type="hidden" name="registerShipperCongArr[szPhoneNumberUpdate]" id="szPhoneNumberUpdate" value="">
	<a href="javascript:void(0);" class="button1" style="opacity:0.4" id="shipper_consignee_button"><span><?php echo t($t_base.'fields/save');?></span></a>  </p>
</form>	
</div>
<?php
}

function adminCustomerMultiUserAccess($t_base,$idUser,$divId)
{
	editCustomerMenu('multiUser',$idUser,$divId);
	$kUser = new cUser();
	$kUser->getUserDetails($idUser);
	if((int)$idUser>0)
	{
		$userMutliArr=$kUser->getMultiUserData($kUser->idGroup);
		$pendingUserAry = $kUser->getPendingInvitedUser($idUser,$kUser->idGroup);
	}
?>
<div class="hsbody-2-right">
	<div id="multiUserList"></div>
		<p><?=t($t_base.'title/heading1');?></p>
		<br />
		<table cellpadding="0"  cellspacing="0" border="0" class="format-2" width="100%" style="margin-bottom:20px;" id="multiUsertable">
			<tr>
				<td><strong><?=t($t_base.'fields/name');?></strong></td>
				<td><strong><?=t($t_base.'fields/c_name');?></strong></td>
				<td><strong> <?=t($t_base.'fields/email');?></strong></td>
				<td><strong><?=t($t_base.'fields/status');?></strong></td>
			</tr>
			<? 
				$l=0;
			   if(!empty($userMutliArr))
			   {
			   		foreach($userMutliArr as $userMutliArrs)
			   		{
			   			if($userMutliArrs['id']!=$idUser)
			   			{
			   			?>
			   				<tr id="multi_user_tr_<?=++$l?>" onclick="sel_remove_multi_access('<?=$idUser?>','<?=$userMutliArrs['id']?>','confirm','<?=$l?>');"> 
			   					<td><?=$userMutliArrs['szFirstName']." ".$userMutliArrs['szLastName']?></td>
			   					<td><?=$userMutliArrs['szCompanyName']?></td>
			   					<td><?=$userMutliArrs['szEmail']?></td>
			   					<td><?php 
										if($userMutliArrs['iInvited']!='1') { 
											echo t($t_base.'messages/not_confirmed');
										  }else
										  {
										  	echo t($t_base.'messages/confirmed');
										  }
										  
			   					?></td>
			   				</tr>
			   			<?
						}
			   		}
			   }
			?>
			<? if(!empty($pendingUserAry))
			   {
			   		foreach($pendingUserAry as $pendingUserArys)
			   		{
			   			?>
			   				<tr id="multi_user_tr_<?=++$l?>" onclick="sel_remove_multi_access('<?=$idUser?>','<?=$pendingUserArys['invtid']?>','pending','<?=$l?>');">
			   					<td><?=$pendingUserArys['szFirstName']." ".$pendingUserArys['szLastName']?></td>
			   					<td><?=$pendingUserArys['szCompanyName']?></td>
			   					<td><?=$pendingUserArys['szEmail']?></td>
			   					<td><?php 							
										echo t($t_base.'messages/pending');										  
			   					?></td>
			   				</tr>
			   			<?
			   		}
			   }
			?>
		</table>
		<div id="multiUserAdd"></div>
			<form name="addMultiUser" id="addMultiUser" method="post">
				<table cellpadding="2"  cellspacing="2" border="0" width="100%" style="margin-bottom:20px;">
					<tr>
						<td colspan="2"><?=t($t_base.'title/email_of_user_to_be_added')?></td>
						<td align="right"><input type="text" name="multiUserArr[szEmail]" id="szEmail" value="<?=$_POST['multiUserArr']['szEmail'] ? $_POST['multiUserArr']['szEmail'] : ''?>" size="50"/></td>
						<td align="right"><a href="javascript:void(0);" class="button1" onclick="check_email_address_by_admin();"><span><?=t($t_base.'fields/add');?></span></a></td>
					</tr>
					<tr>
						<td colspan="2">
						<input type="hidden" name="idUser" id="idUser" value="<?=$idUser?>" />
						<input type="hidden" name="flag" id="flag" value="addMultiAccess" />
						<input type="hidden" name="multiUserArr[idUser]" id="idUser" value="<?=$_POST['multiUserArr']['idUser'] ? $_POST['multiUserArr']['idUser'] :$idUser?>" size="50"/></td>
						<td align="right"><a <?php if($l>0) {?> href="javascript:void(0);" onclick="remove_multi_access('<?=$idUser?>','confirm','<?=$idUser?>');" <? }?>><span><?=t($t_base.'links/remove_user_from_current_group');?></span></a></td>
						<td align="right"><a href="javascript:void(0);" class="button2" id="remove_multi_user_access" style="opacity:0.4"><span><?=t($t_base.'fields/remove');?></span></a></td>
					</tr>
					
				</table>
			</form>	
		</div>
<?php
}

function adminAccountHistoryDetails($t_base,$idUser,$divId)
{
	editCustomerMenu('accountHis',$idUser,$divId);
	$kAdmin = new cAdmin();
	$kUser = new cUser();
	$userAllBookingArr=$kAdmin->getUserAllBookings($idUser);
	$kUser->getUserDetails($idUser);
	$userEmailLogArr=$kAdmin->getUserEmailLog($idUser,true)
?>
<div class="hsbody-2-right">
	<p><h4><?=$kUser->szFirstName?> <?=$kUser->szLastName?> <?=t($t_base.'fields/booking_details')?></h4></p>
	<table cellpadding="0"  cellspacing="0" border="0" class="format-2" width="100%" style="margin-bottom:20px;">
		<tr>
			<td><strong><?=t($t_base.'fields/created_date')?></strong></td>
			<td><strong><?=t($t_base.'fields/reference_number')?></strong></td>
			<td><strong><?=t($t_base.'fields/forwarder_name')?></strong></td>
			<td><strong><?=t($t_base.'fields/service')?></strong></td>	
			<td><strong><?=t($t_base.'fields/from')?></strong></td>	
			<td><strong><?=t($t_base.'fields/to')?></strong></td>			
			<td><strong><?=t($t_base.'fields/confirm_date')?></strong></td>	
			<td style="text-align: right;"><strong><?=t($t_base.'fields/amount')?></strong></td>		
			<td><strong><?=t($t_base.'fields/status')?></strong></td>
		</tr>
		<?php 
			if(!empty($userAllBookingArr))
			{
				foreach($userAllBookingArr as $userAllBookingArrs)
				{
				
					if($userAllBookingArrs['idBookingStatus']=='1')
					{
						$status="Draft";
					}
					else if($userAllBookingArrs['idBookingStatus']=='2')
					{
						$status="On Hold";
					}
					else if($userAllBookingArrs['idBookingStatus']=='3')
					{
						$status="Confirmed";
					}
					else if($userAllBookingArrs['idBookingStatus']=='4')
					{
						$status="Archived";
					}
					else if($userAllBookingArrs['idBookingStatus']=='5')
					{
						$status="Deleted";
					}
					preg_match('/\(([A-Za-z0-9 ]+?)\)/',$userAllBookingArrs['szServiceName'],$match);
					$origin = $kAdmin->countriesView($_SESSION['admin_id'],$userAllBookingArrs['idOriginCountry']);
					$destination = $kAdmin->countriesView($_SESSION['admin_id'],$userAllBookingArrs['idDestinationCountry']);
					?>
					<tr>
						<td width="12%"><?=((!empty($userAllBookingArrs['dtCreatedOn']) && $userAllBookingArrs['dtCreatedOn']!="0000-00-00 00:00:00")?DATE("d/m/Y",strtotime($userAllBookingArrs["dtCreatedOn"])):"")?></td>
						<td width="13%"><?=$userAllBookingArrs['szBookingRef']?></td>
						<td width="28%"><?=returnLimitData($userAllBookingArrs['szForwarderDispName'],25)?></td>
						<td width="10%"><?=$match[1]?></td>
						<td width="6%"><?=$origin['szCountryISO']?></td>
						<td width="6%"> <?=$destination['szCountryISO']?></td>						
						<td width="10%"><?=((!empty($userAllBookingArrs['dtBookingConfirmed']) && $userAllBookingArrs['dtBookingConfirmed']!="0000-00-00 00:00:00")?DATE("d/m/Y",strtotime($userAllBookingArrs["dtBookingConfirmed"])):"")?></td>
						<td width="12%" style="text-align: right;"><?if((int)$userAllBookingArrs['fTotalPriceCustomerCurrency']>0){?><?=$userAllBookingArrs['szCurrency']?>  <?=number_format((float)$userAllBookingArrs['fTotalPriceCustomerCurrency'],2);?><?}else{echo 'N/A';}?></td>
						<td width="8%"><?=$status?></td>
					</tr>
				<?
				}
			}
		?>
	</table>
	<p><h4><?=$kUser->szFirstName?> <?=$kUser->szLastName?> <?=t($t_base.'fields/email_log')?></h4></p>
	<table cellpadding="0"  cellspacing="0" border="0" class="format-2" width="100%" style="margin-bottom:20px;">
		<tr>
			<td><?=t($t_base.'fields/send_date')?></td>
			<td colspan="5"><?=t($t_base.'fields/subject')?></td>
			<td><?=t($t_base.'fields/action')?></td>
		</tr>
		<?php
			if(!empty($userEmailLogArr))
			{
				foreach($userEmailLogArr as $userEmailLogArrs)
				{
					?>
						<tr>
							<td width="10%"><?=((!empty($userEmailLogArrs['dtSent']) && $userEmailLogArrs['dtSent']!="0000-00-00 00:00:00")?DATE("d/m/Y",strtotime($userEmailLogArrs["dtSent"])):"")?></td>
							<td width="80%" colspan="5"><?=$userEmailLogArrs['szEmailSubject']?></td>
							<td width="10%"><a href="javascript:void(0)" onclick="send_email('<?=$userEmailLogArrs['id']?>','<?=$idUser?>');"><?=t($t_base.'fields/resend')?></a></td>
						</tr>
					<?
				}
			}
			?>
		
		
	</table>
	
	</div>
	<?php
}


function adminForwarderAccessHistory($t_base,$idForwarder)
{ 
	$sql="
		idForwarder='".(int)$idForwarder."' 	
	";
	$kAdmin= new cAdmin();
	$forwarderProfileHistory=$kAdmin->getForwarderAccessHistory($sql);
?>
	<?=forwarderCompaniesEdit($idForwarder,'forwarderHistory');?>
	<div class="hsbody-2-right">
	<table cellpadding="0"  cellspacing="0" border="0" class="format-2" width="100%" style="margin-bottom:20px;">
		<tr>
			<td><strong><?=t($t_base.'fields/created_date')?></strong></td>
			<td><strong><?=t($t_base.'fields/activity')?></strong></td>
		</tr>	
		<?
			if(!empty($forwarderProfileHistory))
			{
				foreach($forwarderProfileHistory as $forwarderProfileHistorys)
				{
				?>
		<tr>
			<td width="20%"><?=((!empty($forwarderProfileHistorys['dtCreatedOn']) && $forwarderProfileHistorys['dtCreatedOn']!="0000-00-00 00:00:00")?DATE("d/m/Y H:i",strtotime($forwarderProfileHistorys["dtCreatedOn"])):"")?></td>
			<td width="80%"><?=$forwarderProfileHistorys['szTitle']?></td>
		</tr>	
		<? }}?>
		</table>
		<p align="center">
		<a href="javascript:void(0);" class="button2"  onclick="showDifferentProfile('forwarderComp');"><span><?=t($t_base.'fields/cancel');?></span></a>
		</p>
		</div>
<?	
}
function forwarderCompaniesEdit($id,$activeFlag)
{	
	$t_base = 'management/leftNav/';
	$kForwarder	=	new	cForwarder;
	$kForwarder->load($id);
	$status=$kForwarder->selectIactiveStatus($id);
	if($status['iActive']==0)
	{
		$showStatus = "Reactivate";
	}
	if($status['iActive']==1)
	{
		$showStatus = "Inactivate";
	}
	?>
	<div class="hsbody-2-left account-links">
	<? if($kForwarder->szLogo!='')
	{
		echo "<img id='source_image' src ='".__MAIN_SITE_HOME_PAGE_URL__."/image.php?img=".$kForwarder->szLogo."&w=80&h=30'>";
	}
	else
	{
		echo "<img id='source_image' src ='#'>";
	}
	switch($activeFlag) 
	{
		case 'user_access':
		{
			$title = 'Transporteca | Edit Forwarder - Users and Access';
			BREAK;
		}
		case 'preferences':
		{
			$title = 'Transporteca | Edit Forwarder - Preferences';
			BREAK;
		}
		case 'companyInformation':
		{
			$title = 'Transporteca | Edit Forwarder - Company Information';
			BREAK;
		}
		case 'bankDetails':
		{
			$title = 'Transporteca | Edit Forwarder - Bank Details';
			BREAK;
		}
		case 'settings_pricing':
		{
			$title = 'Transporteca | Edit Forwarder - Settings and Pricing';
			BREAK;
		}
		case 'forwarderHistory':
		{
			$title = 'Transporteca | Edit Forwarder - Forwarder History';
			BREAK;
		}
	}	
	echo "<script type = 'text/javascript'>$('#metatitle').html('".$title."');</script>";
	?>
	<br />
	<h4 style="padding-bottom: 5px;"><?=t($t_base.'fields/edit_forwarder');?></h4>
	
		<ul>
			<li <? if(trim($activeFlag)=='user_access'){?> class="active" <? }?>><a class="cursor-pointer" onclick="showForwarderCompany('<?=$id?>','user_access')" ><?=t($t_base.'fields/user_access');?></a></li>
			<li <? if(trim($activeFlag)=='preferences'){?> class="active" <? }?>><a class="cursor-pointer" onclick="showPreferences('<?=$id?>','preferences')" ><?=t($t_base.'fields/preferences');?></a></li>
			<li <? if($activeFlag=='companyInformation'){?>class="active"  <? }?>><a class="cursor-pointer" onclick="showCompanyInformation('<?=$id?>')"><?=t($t_base.'fields/compny_information');?></a></li>
			<li <? if($activeFlag=='bankDetails'){?>class="active" <? }?>><a class="cursor-pointer" onclick="showForwarderBAnkDetails('<?=$id?>')"><?=t($t_base.'fields/bank_details');?></a></li>
			<li <? if($activeFlag=='settings_pricing'){?>class="active" <? }?>><a class="cursor-pointer" onclick="showForwarderCompany('<?=$id?>','settings_pricing')"><?=t($t_base.'fields/setting_pricing');?></a></li>
			<li <? if($activeFlag=='forwarderHistory'){?> class="active" <? }?>><a class="cursor-pointer" onclick="showForwarderCompany('<?=$id?>','forwarderHistory')"><?=t($t_base.'fields/forwarder_history');?></a></li>
			<li <? if($activeFlag=='Inactive'){?> class="active" <? }?>><a class="cursor-pointer" onclick="inactiveForwarderCompany('<?=$id?>','<?=$showStatus;?>')"><?=$showStatus?></a></li>
			<li><a class="cursor-pointer" onclick="showDifferentProfile('forwarderComp');">Back</a></li>
		</ul>	
	</div>
	<?
}
function forwarderCompanyPrederences($t_base,$preferencesArr,$idForwarder)
{
	$arrBook=BookingReportsArr();
	//$bookingReports	= $arrBook[0];
	$paymentReports	= $arrBook[1];
	$pricngUpdates	= $arrBook[2];
	$kForwarderContact		=	new	cForwarderContact;	
	$forwarderCP=$kForwarderContact->getSystemPanelsDetails($idForwarder);
	$forwarderCP2=$kForwarderContact->getSystemPanelforwarderAdmin($idForwarder);
	$controlPanel=$kForwarderContact->findSubDomainName($forwarderCP['PanelUrl']);
	?>
	<div class="hsbody-2-right"><?
	if(!empty($kForwarderContact->arErrorMessages)){
		?>
		<div id="regError" class="errorBox">
		<div class="header"><?=t($t_base_error.'/please_following');?></div>
		<div id="regErrorList">
		<ul>
		<?php
			foreach($kForwarderContact->arErrorMessages as $key=>$values)
			{
			?><li><?=$key." ".$values?></li>
			<?php	
			}
		?>
		</ul>
		</div>
		</div>
		<?php } 
		?>
		<div id="Error-log" ></div>
		<div class="profile-fields">
			<span class="field-name"><b><?=t($t_base.'title/email_address');?></b></span>
			<span class="field-container" style="width:60%">
				<table cellpadding='0' cellspacing="0" class="format-2"  width="100%" id="history_table">
					<tr>
						<td width="30%" ><b>Profile</b></td>
						<td width="60%" ><b>e-mail</b></td>
					</tr>
				<?
				if(!empty($preferencesArr))
				{	$count = 1;
					foreach($preferencesArr as $pref)
					{
						if($pref['idForwarderContactRole'] 		== __BOOKING_PROFILE_ID__)
						{
						$role = 'Bookings';
						}
						else if($pref['idForwarderContactRole'] == __PAYMENT_AND_BILLING_PROFILE_ID__)
						{
						$role = 'Payments & Billing';
						}
						else if($pref['idForwarderContactRole'] == __CUSTOMER_PROFILE_ID__)
						{
						$role = 'Customer Service';
						}
						echo "
							<tr id =history_tr_".$count." onclick = \"editForwarderPref('history_tr_".$count."','".$pref['id']."');\">
							<td>".$role."</td>
							<td>".$pref['szEmail']."</td>
							</tr>
							";
						$count++;
					}
				}
				?>
				</table>
			</span>
			<span class="field-container" style="width:15%;text-align:right;">
				<a class="button1" style="opacity:0.4;" id = "editPreferences" ><span><?=t($t_base.'fields/edit');?></span></a>
			<br />	
				<a class="button2" style="opacity:0.4;" id = "removePreferences" ><span><?=t($t_base.'fields/remove');?></span></a>
			</span>
		</div>	
		<div style="clear: both;"></div>
		
		<div class="profile-fields" id= "addForwarderProfiler">
			<span class="field-name">&nbsp;</span>
			<span class="field-container" style="width:60%;margin-top:4px;">
			<form id="updateForwarderContact">
				<select name="addPreferenceArr[szContactRole]" style="width:190px">
					<option value="<?=__BOOKING_PROFILE_ID__;?>">Bookings</option>
					<option value="<?=__PAYMENT_AND_BILLING_PROFILE_ID__;?>">Payments & Billing</option>
					<option value="<?=__CUSTOMER_PROFILE_ID__;?>">Customer Service</option>
				</select>
				<input type="text" name="addPreferenceArr[szEmail]" style="width: 367px;">
				<input type="hidden" name="addPreferenceArr[idForwarder]" value="<?=$idForwarder?>">
				<input type="hidden" name="flag" value="ADD_PREFERENCES_ARRAY">
				</form>
			</span>
			<span class="field-container" style="width:15%;text-align:right;">
				<a class="button1" id = "addPreferences" onclick="addForwarderPreferences()"><span><?=t($t_base.'fields/add');?></span></a>
			</span>
		</div>
		<div style="clear: both;"></div>
	

<div id="update_preference_control">
<script type="text/javascript">
 $("#datepicker1").datepicker();
</script>
	<div id="ajax_edit_mode">
	<div id="Error"></div>
	<form id="ajax_prefrence_control" method="post">
	<h4 style="margin-top:5px;"><strong><?=t($t_base.'title/system_config');?></strong></h4>					
	<? /*
					<div class="profile-fields">
						<span class="field-name"><?=t($t_base.'fields/booking_panel');?></span>
					
						
						<?
							if(isset($kForwarderContact->controlBooking))
							{
								$book=$kForwarderContact->controlBooking;
							}
							else
							{
								$book=$forwarderCP['BookingFrequency'];
							}
						?>
						<select name="updatePrefControlArr[szBooking]" >
						<?
							foreach($bookingReports as $key=>$value)
							{
							echo '<option value="'.($key+1) .'"';
							if($key+1==$book)
							{
								echo "selected=selected";
							}
							echo '>'.$value.'</option>';
								
							}
						?>
						</select>
					</div>					
			*/ ?>		
					
					
					<div class="profile-fields">
						<span class="field-name"><?=t($t_base.'fields/payment_frequency');?></span>
						
						<?
							if(isset($kForwarderContact->controlPayment))
							{
								$pay=$kForwarderContact->controlPayment;
							}
							else
							{
								$pay=$forwarderCP['PaymentFrequency'];
							}
						?>
						<span class="field-container">
						<select name="updatePrefControlArr[szPayment]">
						<?
							foreach($paymentReports as $key=>$value)
							{
							echo '<option value="'.($key+1) .'"';
							if($key+1==$pay)
							{
								echo "selected=selected";
							}
							echo '>'.$value.'</option>';
								
							}
							?>
						</select>
						</span>
					</div>
						
						
					
					<div class="profile-fields">
						<span class="field-name"><?=t($t_base.'fields/pricing_updates_on_rss');?></span>
						
						<?
							if(isset($kForwarderContact->controlRss))
							{
								$rss=$kForwarderContact->controlRss;
							}
							else
							{
								$rss=$forwarderCP['Rss'];
							}
						?>
						<span class="field-container">
						<select name="updatePrefControlArr[szRss]">
						<?
							foreach($pricngUpdates as $key=>$value)
							{
							echo '<option value="'.($key+1).'"';
							if($key+1==$rss)
							{
								echo "selected=selected";
							}
							echo '>'.$value.'</option>';
								
							}
						?>
						</select>
						</span>
						</div>
		<h4 style="margin-top:5px;"><strong><?=t($t_base.'title/admin');?>  <?=t($t_base.'title/cannot_by_forwarder');?></strong></h4>					
			
		<div class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/codeForBooking');?></span>
			<span class="field-container"><input style="width:60px;" type="text" name="updatePrefControlArr[BookingRef]" value='<?=$forwarderCP2['szForwarderAlias']?>' > <?=t($t_base.'fields/two_cap_letter');?></span>
		</div>
		<div class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/urlDns');?></span>
			<span class="field-container" style="width:50%;" ><input style="width:181px;" type="text" name="updatePrefControlArr[dnsURL]" value=<?=$controlPanel;?>> <?=t($t_base.'fields/tenasporetca_com');?></span>
		</div>
		<div class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/referalFee');?></span>
			<span class="field-container"><input style="width:58px;" type="text" name="updatePrefControlArr[RefFee]" value='<?=$forwarderCP2['fReferalFee']?>'> <?=t($t_base.'fields/pctg_of_invoice_value');?></span>
		</div>
		<div class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/validto');?></span>
			<span class="field-container"><input type="text" style="width: 100px;" id="datepicker1" onblur="show_me(this.id,'dd/mm/yyyy')" onfocus="blank_me(this.id,'dd/mm/yyyy')" name="updatePrefControlArr[dtValidTo]" value='<?=(($forwarderCP2['dtReferealFeeValid']!='0000-00-00 00:00:00' && $forwarderCP2['dtReferealFeeValid']!=''  )?date('d/m/Y',strtotime($forwarderCP2['dtReferealFeeValid'])):date('d/m/Y'))?>'></span>
		</div>
		<div class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/automatic_pay');?></span>
			<span class="field-container">
			<select name="updatePrefControlArr[iAutomaticPaymentInvoicing]">
			<option value="1" <? echo ($forwarderCP2['iAutomaticPaymentInvoicing']?"selected=selected":'')?>>Yes</option>
			<option value="0" <? echo ($forwarderCP2['iAutomaticPaymentInvoicing']?'':"selected=selected")?>>No</option>
			</select>
		</div>
		<div class="profile-fields">
			<p><?=t($t_base.'fields/comments_for_pricing');?></p><br />
			<span class="field-container"><textarea name="updatePrefControlArr[szComment]" rows="10" cols="70" id="szCommentText" maxlength="400" onkeyup="checkLimit(this.id)"><?=str_replace('<br />',"",$forwarderCP2['szReferalFeeComment'])?></textarea></span>
		</div>
			<input type="hidden" name="flag" value="UPDATE_FORWARDER_PREFERENCES">							
			<input type="hidden" name="idForwarder" value="<?=$idForwarder;?>">	
		<div style="clear: both;"></div>
		<span style="font: italic;float: right;">You have <span id="char_left">400</span> characters left to write</span>			
		<p align="center"><br/><br/>
			<a href="javascript:void(0);" onclick="showDifferentProfile('forwarderComp');"  class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>			
			<a href="javascript:void(0);" onclick="submit_preference_controls_del()"  class="button1"><span><?=t($t_base.'fields/save');?></span></a>	
		</p>
	
</form>	
</div>
</div>		
</div> 	
<?
}
function BookingReportsArr()
{
    $arrBook[]=$bookingReports=array('Daily','Weekly','Monthy','Never');
    $arrBook[]=$paymentReports=array('Every two months','Monthly');
    $arrBook[]=$pricngUpdates=array('No','Yes');
    return $arrBook;
}
function viewForwarderEachPreferences($t_base,$preferencesArr)
{	
	if($preferencesArr == array())
	{
		return false;
	}
?>
	<div id="Error-log"  style="width: 450px;text-align: center;padding-left: 220px;"></div>
	<span class="field-name">&nbsp;&nbsp;</span>
			<span class="field-container">
				
				<form id ="updateForwarderContact">
					<select name="updatePreferenceArr[szContactRole]" style="width:10px">
						<option value="<?=__BOOKING_PROFILE_ID__;?>" <?=($preferencesArr['idForwarderContactRole']==__BOOKING_PROFILE_ID__)?'selected':'';?>>Bookings</option>
						<option value="<?=__PAYMENT_AND_BILLING_PROFILE_ID__;?>" <?=($preferencesArr['idForwarderContactRole']==__PAYMENT_AND_BILLING_PROFILE_ID__)?'selected':'';?>>Payments & Billing</option>
						<option value="<?=__CUSTOMER_PROFILE_ID__;?>" <?=($preferencesArr['idForwarderContactRole']==__CUSTOMER_PROFILE_ID__)?'selected':'';?>>Customer Service</option>
					</select>
					<input type="text" name ="updatePreferenceArr[szEmail]" style="width: 174px;" value = "<?=$preferencesArr['szEmail']?>">
					<input type="hidden" name="updatePreferenceArr[forwarderContactId]" value="<?=$preferencesArr['id']?>">
					<input type="hidden" name="updatePreferenceArr[idForwarder]" value="<?=$preferencesArr['idForwarder']?>">
					<input type="hidden" name="flag" value="UPDATE_PREFERENCES_ARRAY">
				</form>
			</span>
			<span class="field-container">
				<a class="button1" id = "addPreferences" onclick="addForwarderPreferences()"><span><?=t($t_base.'fields/save');?></span></a>
			</span>
<?php }
function ForwarderCompanyInformation($idForwarder)
{	$t_base = "ForwardersCompany/company_information/";
	$kForwarder = new cForwarder();
	$kForwarder->load($idForwarder);
	$idForwarder = $kForwarder->id;
?>
<div class="hsbody-2-right">
	<script type="text/javascript">
	$().ready(function() {
	/* on change, focus or click call function fileName */
	$('input[type=file]').bind('change focus click', function() {$(this).fileName()});
	});
	
	</script>
	<div id="Error-log"></div>
	<h5><b><?=t($t_base.'titles/display_for_customer');?></b></h5>
	<form name="updateRegistComapnyForm" class="updateRegistComapnyForm" method="post">	
		<div class="profile-fields">
			<p class="fl-25"><?=t($t_base.'titles/display_name');?></p>
			<p class="field-container"><input type="text" name="updateRegistComapnyAry[szDisplayName]" value="<?=$kForwarder->szDisplayName?>" onblur="closeTip('display_name');" onfocus="openTip('display_name');"></p>
			<div class="field-alert"><div id="display_name" style="display:none;"><?=t($t_base.'messages/display_name');?></div></div>
		</div>
		<div class="profile-fields">
			<p class="fl-25"><?=t($t_base.'titles/standard_terms');?></p>
			<p class="field-container"><input type="text" name="updateRegistComapnyAry[szLink]" value="<?=$kForwarder->szLink?>" onblur="closeTip('terms_standard');" onfocus="openTip('terms_standard');"></p>
			<div class="field-alert"><div id="terms_standard" style="display:none;"><?=t($t_base.'messages/terms_standard');?></div></div>
		</div>
		<div class="profile-fields">
			<p class="fl-25"><?=t($t_base.'titles/display_logo');?></p>
			<p class="field-container" style="width: 69%;">
			<?php
			if(!empty($kForwarder->szLogo))
			{
				// if forwarder logo already uploaded then temp will image will render in this span after upload
				?>
				<span id="upload_file_success" class="forwarder_logo"><img src="<?=__BASE_STORE_INC_URL__.'/image.php?img='.$kForwarder->szLogo."&w=".__MAX_ALLOWED_FORWARDER_LOGO_WIDTH__."&h=".__MAX_ALLOWED_FORWARDER_LOGO_HEIGHT__?>" alt="<?=$kForwarder->szDisplayName?>"></span>
				<span class="fl-50"><a href="javascript:void(0);" id="forwarder_logo_delete_link"  onclick="delete_forwarder_logo('<?=$idForwarder?>')"><?=t($t_base.'feilds/delete');?></a></span>
				<?php 
			}
			else
			{
				// if there is no image uploaded for the forwarder then temp logo will render here. 
				?>
				<span id="upload_file_success" class="forwarder_logo" style="display:none;">
				</span>
				<span class="fl-50"><a href="javascript:void(0);" style="display:none;" id="forwarder_logo_delete_link"  onclick="delete_forwarder_logo('<?=$idForwarder?>')"><?=t($t_base.'feilds/delete');?></a></span>
				<?
			}
			?>
				<span class="fl-50">
				<a onclick="show_upload_file_form()" href="javascript:void(0);"><?=t($t_base.'feilds/upload_new_file');?></a>
				<a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/upload_image_tootltip');?>','','company_information_tooltip_right',event);" onmouseout="hide_tool_tip('company_information_tooltip_right')">&nbsp;</a>
				</span>
				<input type="hidden" name="updateRegistComapnyAry[idForwarder]" value="<?=$idForwarder?>">
				<input type="hidden" name="updateRegistComapnyAry[iDeleteLogo]" id="delete_logo_hidden" value="">
				<input type="hidden" name="updateRegistComapnyAry[szOldLogo]" id="szOldLogo" value="<?=$kForwarder->szLogo?>">
			</p>
		</div>
	</form>
		 <div id="upload_file" style="display:none;padding-left:218px;">
		 	<form name="updateLogoForm" id="updateLogoForm" action="<?=__BASE_URL__;?>/upload.php" id="updateRegistComapnyForm" method="post" enctype="multipart/form-data" target="upload_target" onsubmit="startUpload();">
			<p id="f1_upload_process" style="display:none;">Loading...<br/><img src="<?=__BASE_STORE_IMAGE_URL__?>/file_loader.gif" /><br/></p>
		 	<div id="f1_upload_form" class="profile-fields">
		 	 <div class="file">
					<input type="file" id="fileUpload" name="updateRegistComapnyAry[szForwarderLogo]"  onblur="closeTip('company_logo');" onfocus="openTip('company_logo');" />
					<span class="button"><?=t($t_base.'feilds/upload_new_file');?></span>
				</div>	
			 	<div class="field-alert" style="width:180px;"><div id="company_logo" style="display:none;"><?=t($t_base.'messages/company_logo');?></div></div>
		 	 	<input type="hidden" name="updateRegistComapnyAry[szMode]" value="<?=$operation_mode?>">
				<input type="hidden" name="updateRegistComapnyAry[idForwarder]" value="<?=$idForwarder?>">
			 	<a href="javascript:void(0);" onclick="$('#updateLogoForm').submit();" class="button1"><span><?=t($t_base.'titles/upload');?></span></a>
			 </div>			
			 <iframe id="upload_target" name="upload_target" src="#" style="width:0;height:0;border:0px solid #fff;"></iframe>	
		 </form>	
	    </div>
		 <br class="clear-all">
		 <br />
	<?php
	$kConfig = new cConfig();
	$iLanguage = getLanguageId();
	$allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);
?>
<h5><b>Registered Company Details</b></h5>
<form name="updateRegistComapnyForm" class="updateRegistComapnyForm" method="post" >	
	<div class="profile-fields">
		<p class="fl-25"><?=t($t_base.'titles/company_name');?></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szCompanyName]" value="<?=$kForwarder->szCompanyName?>" onblur="closeTip('name');" onfocus="openTip('name');"></p>
		<div class="field-alert"><div id="name" style="display:none;"><?=t($t_base.'messages/company_name');?></div></div>
		
	</div>
	<div class="profile-fields">
		<p class="fl-25"><?=t($t_base.'titles/address1');?></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szAddress]" value="<?=$kForwarder->szAddress?>" onblur="closeTip('add');" onfocus="openTip('add');"></p>
		<div class="field-alert"><div id="add" style="display:none;"><?=t($t_base.'messages/address_line_1');?></div></div>
	</div>
	<div class="profile-fields">
		<p class="fl-25"><?=t($t_base.'titles/address2');?> <span class="optional">(<?=t($t_base.'titles/optional');?>)</span></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szAddress2]" value="<?=$kForwarder->szAddress2?>" onblur="closeTip('add2');" onfocus="openTip('add2');"></p>
		<div class="field-alert"><div id="add2" style="display:none;"><?=t($t_base.'messages/address_line_2');?></div></div>
	</div>
	<div class="profile-fields">
		<p class="fl-25"><?=t($t_base.'titles/address3');?> <span class="optional">(<?=t($t_base.'titles/optional');?>)</span></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szAddress3]" value="<?=$kForwarder->szAddress3?>" onblur="closeTip('add3');" onfocus="openTip('add3');"></p>
		<div class="field-alert"><div id="add3" style="display:none;"><?=t($t_base.'messages/address_line_3');?></div></div>
	</div>
	
	<div class="profile-fields">
		<p class="fl-25"><?=t($t_base.'titles/postcode');?></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szPostCode]" value="<?=$kForwarder->szPostCode?>" onblur="closeTip('postcode');" onfocus="openTip('postcode');"></p>
		<div class="field-alert"><div id="postcode" style="display:none;"><?=t($t_base.'messages/postal_code');?></div></div>
	</div>
	<div class="profile-fields">
		<p class="fl-25"><?=t($t_base.'titles/city');?></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szCity]" value="<?=$kForwarder->szCity?>" onblur="closeTip('city');" onfocus="openTip('city');"></p>
		<div class="field-alert"><div id="city" style="display:none;"><?=t($t_base.'messages/city');?></div></div>
	</div>
	<div class="profile-fields">
		<p class="fl-25"><?=t($t_base.'titles/province');?> <span class="optional">(<?=t($t_base.'titles/optional');?>)</span></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szState]" value="<?=$kForwarder->szState?>" onblur="closeTip('state');" onfocus="openTip('state');"></p>
		<div class="field-alert"><div id="state" style="display:none;"><?=t($t_base.'messages/state');?></div></div>
	</div>
	<div class="profile-fields">
		<p class="fl-25"><?=t($t_base.'titles/country');?></p>
		<p class="field-container">
			<select size="1" name="updateRegistComapnyAry[idCountry]" style="width:289px;" id="idCountry">
			<?php
				if(!empty($allCountriesArr))
				{
					foreach($allCountriesArr as $allCountriesArrs)
					{
						?><option value="<?=$allCountriesArrs['id']?>" <?php if(($allCountriesArrs['id']==$kForwarder->idCountry)){?> selected <?php }?>><?=$allCountriesArrs['szCountryName']?></option>
						<?php
					}
				}
			?>
		   </select></p>
		<div class="field-alert"><div id="state" style="display:none;"><?=t($t_base.'messages/state');?></div></div>
	</div>
	
	<div class="profile-fields">
		<p class="fl-25"><?=t($t_base.'titles/phone_number');?></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szPhone]" value="<?=$kForwarder->szPhone?>" onblur="closeTip('phone');" onfocus="openTip('phone');"></p>
		<div class="field-alert"><div id="phone" style="display:none;"><?=t($t_base.'messages/phone');?></div></div>
	</div>
	<br><br>
	<div class="profile-fields">
		<p class="fl-25"><?=t($t_base.'titles/company_reg_num');?></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szCompanyRegistrationNum]" value="<?=$kForwarder->szCompanyRegistrationNum?>" onblur="closeTip('cmp');" onfocus="openTip('cmp');"></p>
		<div class="field-alert"><div id="cmp" style="display:none;"><?=t($t_base.'messages/company_reg_number');?></div></div>
	</div>
	<div class="profile-fields">
		<p class="fl-25"><?=t($t_base.'titles/vat_reg_num');?> <span class="optional">(<?=t($t_base.'titles/optional');?>)</span></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szVatRegistrationNum]" value="<?=$kForwarder->szVatRegistrationNum?>" onblur="closeTip('reg');" onfocus="openTip('reg');"></p>
		<div class="field-alert"><div id="reg" style="display:none;"><?=t($t_base.'messages/company_vat_reg_number');?></div></div>
	</div>
	<br /><br />
	<p align="center"><a href="javascript:void(0);" onclick="showDifferentProfile('forwarderComp');"  class="button2"><span><?=t($t_base.'feilds/cancel');?></span></a>	
		<a href="javascript:void(0);" onclick="submit_registered_company('<?=$idForwarder?>')"  class="button1"><span><?=t($t_base.'feilds/save');?></span></a></p>
		<input type="hidden" name="updateRegistComapnyAry[idForwarder]" value="<?=$idForwarder?>">
		<input type="hidden" name="flag" value="SAVE_FORWARDER_COMPANY_DETAILS">
					
	
</form>
</div>
<?php
}
function displayForwarderBankDetailsAdmin($t_base,$idForwarder)
{	
	$kForwarder = new cForwarder();
	$kForwarder->load($idForwarder);
	$allCountriesArr =array();
	$allCurrencyArr = array();
	$kConfig = new cConfig();	
	$iLanguage = getLanguageId();
	$allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);
	$allCurrencyArr=$kConfig->getBookingCurrency(false,false,true);
	
	?>

<?php echo forwarderCompaniesEdit($idForwarder,'bankDetails');?>
<div class="hsbody-2-right">
<div id="Error-log"></div>
<form name="updateRegistBankForm" id="updateRegistBankForm" method="post" >	
	<div class="profile-fields">
		<p class="fl-25"><?=t($t_base.'titles/bank_name');?></p>
		<p class="field-container"><input type="text" name="updateRegistBankAry[szBankName]" value="<?=!empty($_POST['updateRegistBankAry']['szBankName'])?$_POST['updateRegistBankAry']['szBankName']:$kForwarder->szBankName?>" onblur="closeTip('bank_name');" onfocus="openTip('bank_name');"></p>
		<div class="field-alert"><div id="bank_name" style="display:none;"><?=t($t_base.'messages/bank_name');?></div></div>
		
	</div>
	<div class="profile-fields">
		<p class="fl-25"><?=t($t_base.'titles/swift_code');?></p>
		<p class="field-container"><input type="text" name="updateRegistBankAry[szSwiftCode]" value="<?=!empty($_POST['updateRegistBankAry']['szSwiftCode'])?$_POST['updateRegistBankAry']['szSwiftCode']:$kForwarder->szSwiftCode?>" onblur="closeTip('swift_code');" onfocus="openTip('swift_code');"></p>
		<div class="field-alert"><div id="swift_code" style="display:none;"><?=t($t_base.'messages/swift_code');?></div></div>
	</div>
	<div class="profile-fields">
		<p class="fl-25"><?=t($t_base.'titles/country');?></p>
		<p class="field-container">
			<select size="1" style="width:289px;" name="updateRegistBankAry[idBankCountry]" id="idBankCountry" onblur="closeTip('bank_country');" onfocus="openTip('bank_country');">
				<?php
					if(!empty($allCountriesArr))
					{
						foreach($allCountriesArr as $allCountriesArrs)
						{
							?><option value="<?=$allCountriesArrs['id']?>" <?=((($_POST['updateRegistBankAry']['idBankCountry'])?$_POST['updateRegistBankAry']['idBankCountry']:$kForwarder->idBankCountry) ==  $allCountriesArrs['id'] ) ? "selected":""?>><?=$allCountriesArrs['szCountryName']?></option>
							<?php
						}
					}
				?>
			</select>
		</p>
		<div class="field-alert"><div id="bank_country" style="display:none;"><?=t($t_base.'messages/bank_country');?></div></div>
		
	</div>
	<br/><br/>
	<div class="profile-fields">
		<p class="fl-25"><?=t($t_base.'titles/account_name');?></p>
		<p class="field-container"><input type="text" name="updateRegistBankAry[szNameOnAccount]" value="<?=!empty($_POST['updateRegistBankAry']['szNameOnAccount'])?$_POST['updateRegistBankAry']['szNameOnAccount']:$kForwarder->szNameOnAccount?>" onblur="closeTip('name_on_account');" onfocus="openTip('name_on_account');"></p>
		<div class="field-alert"><div id="name_on_account" style="display:none;"><?=t($t_base.'messages/name_on_account');?></div></div>
		
	</div>
	<div class="profile-fields">
		<p class="fl-25"><?=t($t_base.'titles/sort_code');?>&nbsp;<span class="optional">(<?=t($t_base.'titles/optional');?>)</span> </p>
		<p class="field-container"><input type="text" name="updateRegistBankAry[iSortCode]" value="<?=!empty($_POST['updateRegistBankAry']['iSortCode'])?$_POST['updateRegistBankAry']['iSortCode']:(($kForwarder->iSortCode >0)?$kForwarder->iSortCode:'')?>" onblur="closeTip('sort_code');" onfocus="openTip('sort_code');"></p>
		<div class="field-alert"><div id="sort_code" style="display:none;"><?=t($t_base.'messages/sort_code');?></div></div>
		
	</div>
	
	<div class="profile-fields">
		<p class="fl-25"><?=t($t_base.'titles/account_number');?></p>
		<p class="field-container"><input type="text" name="updateRegistBankAry[iAccountNumber]" value="<?=!empty($_POST['updateRegistBankAry']['iAccountNumber'])?$_POST['updateRegistBankAry']['iAccountNumber']:(($kForwarder->iAccountNumber >0)?$kForwarder->iAccountNumber:'')?>" onblur="closeTip('account_number');" onfocus="openTip('account_number');"></p>
		<div class="field-alert"><div id="account_number" style="display:none;"><?=t($t_base.'messages/account_number');?></div></div>
		
	</div>
	<div class="profile-fields">
		<p class="fl-25"><?=t($t_base.'titles/IBAN_account_number');?> <span class="optional">(<?=t($t_base.'titles/optional');?>)</span></p>
		<p class="field-container"><input type="text" name="updateRegistBankAry[szIBANAccountNumber]" value="<?=!empty($_POST['updateRegistBankAry']['szIBANAccountNumber'])?$_POST['updateRegistBankAry']['szIBANAccountNumber']:$kForwarder->szIBANAccountNumber?>" onblur="closeTip('ibn_acc_number');" onfocus="openTip('ibn_acc_number');"></p>
		<div class="field-alert"><div id="ibn_acc_number" style="display:none;"><?=t($t_base.'messages/ibn_acc_number');?></div></div>
		
	</div>
	<div class="profile-fields">
		<p class="fl-25"><?=t($t_base.'titles/account_currency');?></p>
		<p class="field-container">
			<select size="1" name="updateRegistBankAry[szCurrency]" id="szCurrency" onblur="closeTip('exchange');" onfocus="openTip('exchange');">
				<?php
					if(!empty($allCurrencyArr))
					{
						foreach($allCurrencyArr as $allCurrencyArrs)
						{
							?><option value="<?=$allCurrencyArrs['id']?>" <?=((($_POST['updateRegistBankAry']['szCurrency'])?$_POST['updateRegistBankAry']['szCurrency']:$kForwarder->szCurrency) ==  $allCurrencyArrs['id'] ) ? "selected":""?>><?=$allCurrencyArrs['szCurrency']?></option>
							<?php
						}
					}
				?>
			</select>
		</p>
		<div class="field-alert"><div id="exchange" style="display:none;"><?=t($t_base.'messages/exchange');?></div></div>	
	</div>
	<br><br>
	
		<input type="hidden" name="updateRegistBankAry[szCurrency_hidden]" id="szCurrency_hidden" value="<?=$kForwarder->szCurrency?>">
		<p align="center"><a href="javascript:void(0);" onclick="showDifferentProfile('forwarderComp');"  class="button2"><span><?=t($t_base.'feilds/cancel');?></span></a>
		<a href="javascript:void(0);" onclick="submitBankDetails('<?=$idForwarder?>')"  class="button1"><span><?=t($t_base.'feilds/save');?></span></a>
			<input type="hidden" name="updateRegistBankAry[idForwarder]" value="<?=$idForwarder?>">
			<input type="hidden" name="flag" value="UPDATE_FORWARDER_BANK_DETAILS">
		</p>
	
</form>
</div>
	<?
}
function sendBulkEmail($to,$from,$subject,$message,$reply_to,$idUser,$userFlag)
{ 
	//echo $to='';
	require_once(__APP_PATH_ROOT__.'/inc/SendGrid_loader.php');
	$sendgrid = new SendGrid(__SEND_GRID_USERNAME__,__SEND_GRID_PASSWARD__);
	$smtp = $sendgrid->smtp;
		 
	if(!empty($to) && is_array($to))
	{
	 	foreach($to as $emails)
	 	{
 		  $message1 = new SendGrid\Mail();
	      $message1->
	      setFrom($from)->
	      setFromName(__STORE_SUPPORT_NAME__)->
	      setSubject($subject)->
		  setHtml($message)->
	      addTo($emails);
	      $smtp->send($message1);
	      logMessageEmails($idUser,$message,$subject,$emails,$success,$userFlag);
	 	}
	 }
	 else
	 {
	 	  $message1 = new SendGrid\Mail();
	      $message1->
	      setFrom($from)->
	      setFromName(__STORE_SUPPORT_NAME__)->
	      setSubject($subject)->
		  setHtml($message)->
	      addTo($to);
	      $smtp->send($message1);
	     // print_r($smtp);
	      logMessageEmails($idUser,$message,$subject,$to,$success,$userFlag);
	 }   
}

function logMessageEmails($idUser,$message,$subject,$to,$success,$userFlag)
{
 	$kDatabase = new cDatabase();
    $query = "
    	INSERT INTO
    		".__DBC_SCHEMATA_MESSAGES_LOG__."
    		(
    			idUser,
    			szBody,
    			szSubject,
    			szEmail,
    			dtSend,
    			iSuccess,
    			iUserType
    		)
    	VALUES
    		(
    			".(int)$idUser.",
    			'".mysql_escape_custom($message)."',
    			'".mysql_escape_custom($subject)."',
    			'".mysql_escape_custom($to)."',
    			NOW(),
    			'1',
    			'".(int)$userFlag."'
    		)
    ";
	//echo $query;
	if ($kDatabase->exeSQL($query))
	{
		return true;
	}
	else
	{
		$kDatabase->error = true;
		$szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
		$kDatabase->logError( "input", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
		return false;
	}
}
function get_quarter($i=0) {
	$y = date('Y');
	$m = date('m');
	if($i > 0) {
		for($x = 0; $x < $i; $x++) {
			if($m <= 3) { $y--; }
			$diff = $m % 3;
			$m = ($diff > 0) ? $m - $diff:$m-3;
			if($m == 0) { $m = 12; }
		}
	}
	switch($m) {
		case $m >= 1 && $m <= 3:
			$start = $y.'-01-01 00:00:01';
			$end = $y.'-03-31 00:00:00';
			break;
		case $m >= 4 && $m <= 6:
			$start = $y.'-04-01 00:00:01';
			$end = $y.'-06-30 00:00:00';
			break;
		case $m >= 7 && $m <= 9:
			$start = $y.'-07-01 00:00:01';
			$end = $y.'-09-30 00:00:00';
			break;
		case $m >= 10 && $m <= 12:
			$start = $y.'-10-01 00:00:01';
			$end = $y.'-12-31 00:00:00';
	    		break;
	}
	return array(
		'start' => $start,
		'end' => $end,
		'start_nix' => strtotime($start),
		'end_nix' => strtotime($end)							
	);
}

function http_200_error_status()
{ 
	$t_base = "Error/";
?>
<div id="hsbody">
<div id="page_not_found"  style="padding:30px 0 80px 30px;">
<br><br>
<div style="float: left;">
<img src="<?=__BASE_STORE_IMAGE_URL__?>/page_not_found.jpg">
</div>
<div style="padding:0 100px 0 30px;">
<p style="font-size:22pt;padding-bottom: 50px;"><?=t($t_base.'page_not_found');?></p>
<p style="font-size:large;padding-bottom: 50px;"><?=t($t_base.'page_not_found_detail');?></p> 
<a href="<?=__BASE_URL__?>" style="font-size:large;"><?=t($t_base.'comparing_rates_at_transporteca');?></a> 
</div>
</div>
</div>
<?php
}

function searchPostCode($t_base)
{
	$kConfig 			=	new cConfig;
	$iLanguage = getLanguageId();
	$allCountriesArr	=	$kConfig->getAllCountries(true,$iLanguage);

?>
<script type="text/javascript">
$().ready(function() {	
	$("#szOriginPostCode").autocomplete(__JS_ONLY_SITE_BASE__+"/ajax_postcode.php", {
			width: 260,
			matchContains: true,
			mustMatch: true,
			selectFirst: false
	});
	$("#szOriginCity").autocomplete(__JS_ONLY_SITE_BASE__+"/ajax_postcode.php", {
			width: 260,
			matchContains: true,
			mustMatch: true,
			selectFirst: false
	});
});
</script>
<div id="error"></div>
<form name="searchPostcode" id="searchPostcode" >
	<select size="1" style="width:300px;" name="searchAry[szOriginCountry]" onchange="activate_city_field(this.value)" id="szOriginCountry">
			<?php
				if(!empty($allCountriesArr))
				{
					foreach($allCountriesArr as $allCountriesArrs)
					{
						?><option value="<?=$allCountriesArrs['id']?>"><?=$allCountriesArrs['szCountryName']?></option>
						<?php
					}
				}
			?>
			</select>
			<input type="text" name="searchAry[szOriginCity]" id='szOriginCity'  autocomplete="off" placeholder="Type City" style="width: 150px;">
			<input type="text" name="searchAry[szOriginPostCode]" id='szOriginPostCode'  autocomplete="off" placeholder="Type Postcode" style="width: 100px;">
			<input type="hidden" name="action" value="SUBMIT_SEARCH_FORM">
			<a style="float: right;" href="javascript:void(0);" onclick="searchPostCodeCountry()" id="searchPostCode"  class="button1"><span><?=t($t_base.'fields/search');?></span></a>
</form>
<br/><br/>
<div id="content">
	<table cellpadding="0" id="headings"  cellspacing="0" border="0" class="format-4" width="100%">
		<tr >
			<th width="12%" align="center" valign="top"><?=t($t_base.'fields/Postcode')?> </th>
			<th width="12%" align="center" valign="top"><?=t($t_base.'fields/Latitude')?> </th>
			<th width="12%" align="center" valign="top"><?=t($t_base.'fields/Longitude')?> </th>
			<th width="12%" align="center" valign="top"><?=t($t_base.'fields/Region_1')?> </th>
			<th width="12%" align="center" valign="top"><?=t($t_base.'fields/Region_2')?> </th>
			<th width="12%" align="center" valign="top"><?=t($t_base.'fields/Region_3')?> </th>
			<th width="12%" align="center" valign="top"><?=t($t_base.'fields/Region_4')?> </th>
			<th align="center" valign="top"><?=t($t_base.'fields/City')?> </th>
		</tr>
		<tr style="height: 150px;">
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		</tr>
</table>
</div>
<?
}
function showPostcodeDetails($showCurrentPostcode =array())
{
	if($showCurrentPostcode	!=	array())
	{
		$postcodeDetails	=	json_decode($showCurrentPostcode, true);
	}
	$t_base	=	"management/postcode/";	
?>
<div style="padding: 30px 0 2px 0;">
<form id="postCodeTable">
	<div class="oh">
		<div class="fl-12 s-field">
			<span class="f-size-12"><?=t($t_base.'fields/Postcode');?></span>
			<br>
			<span id="origin_country_span">
			<input type="text" size='9' name="postcodeArr[szPostCode]" value='<?=(isset($postcodeDetails['szPostCode'])?$postcodeDetails['szPostCode']:'');?>'>
			</span>
		</div>
		<div class="fl-12 s-field">
			<span class="f-size-12"><?=t($t_base.'fields/Latitude');?></span>
			<br>
			<span id="origin_country_span">
			<input type="text" size='9' name="postcodeArr[szLat]" value='<?=(isset($postcodeDetails['szLat'])?$postcodeDetails['szLat']:'');?>'>
			</span>
		</div>
		<div class="fl-12 s-field">
			<span class="f-size-12"><?=t($t_base.'fields/Longitude');?></span>
			<br>
			<span id="origin_country_span">
			<input type="text" size='9' name="postcodeArr[szLng]" value='<?=(isset($postcodeDetails['szLng'])?$postcodeDetails['szLng']:'');?>'>
			</span>
		</div>
		<div class="fl-12 s-field">
			<span class="f-size-12"><?=t($t_base.'fields/Region_1');?></span>
			<br>
			<span id="origin_country_span">
			<input type="text" size='9' name="postcodeArr[szRegion1]" value='<?=(isset($postcodeDetails['szRegion1'])?$postcodeDetails['szRegion1']:'');?>'>
			</span>
		</div>
		<div class="fl-12 s-field">
			<span class="f-size-12"><?=t($t_base.'fields/Region_2');?></span>
			<br>
			<span id="origin_country_span">
			<input type="text" size='9' name="postcodeArr[szRegion2]" value='<?=(isset($postcodeDetails['szRegion2'])?$postcodeDetails['szRegion2']:'');?>'>
			</span>
		</div>
		<div class="fl-12 s-field">
			<span class="f-size-12"><?=t($t_base.'fields/Region_3');?></span>
			<br>
			<span id="origin_country_span">
			<input type="text" size='9' name="postcodeArr[szRegion3]" value='<?=(isset($postcodeDetails['szRegion3'])?$postcodeDetails['szRegion3']:'');?>'>
			</span>
		</div>
		<div class="fl-12 s-field">
			<span class="f-size-12"><?=t($t_base.'fields/Region_4');?></span>
			<br>
			<span id="origin_country_span">
			<input type="text" size='9' name="postcodeArr[szRegion4]" value='<?=(isset($postcodeDetails['szRegion4'])?$postcodeDetails['szRegion4']:'');?>'>
			</span>
		</div>
		<div class="fl-12 s-field">
			<span class="f-size-12"><?=t($t_base.'fields/City');?></span>
			<br>
			<span id="origin_country_span">
			<input type="text" size='9' name="postcodeArr[szCity]" value='<?=(isset($postcodeDetails['szCity'])?$postcodeDetails['szCity']:'');?>' id="szCity">
			</span>
		</div>
	</div>
	<div style="clear: both;"></div>
	<div class="oh">
		<div class="fl-12 s-field">
			<span class="f-size-12"><?=t($t_base.'fields/City1');?></span>
			<br>
			<span id="origin_country_span">
			<input type="text" size='9' name="postcodeArr[szCity1]" value='<?=(isset($postcodeDetails['szCity1'])?$postcodeDetails['szCity1']:'');?>'>
			</span>
		</div>
		<div class="fl-12 s-field">
			<span class="f-size-12"><?=t($t_base.'fields/City2');?></span>
			<br>
			<span id="origin_country_span">
			<input type="text" size='9' name="postcodeArr[szCity2]" value='<?=(isset($postcodeDetails['szCity2'])?$postcodeDetails['szCity2']:'');?>'>
			</span>
		</div>
		<div class="fl-12 s-field">
			<span class="f-size-12"><?=t($t_base.'fields/City3');?></span>
			<br>
			<span id="origin_country_span">
			<input type="text" size='9' name="postcodeArr[szCity3]" value='<?=(isset($postcodeDetails['szCity3'])?$postcodeDetails['szCity3']:'');?>'>
			</span>
		</div>
		<div class="fl-12 s-field">
			<span class="f-size-12"><?=t($t_base.'fields/City4');?></span>
			<br>
			<span id="origin_country_span">
			<input type="text" size='9' name="postcodeArr[szCity4]" value='<?=(isset($postcodeDetails['szCity4'])?$postcodeDetails['szCity4']:'');?>'>
			</span>
		</div>
		<div class="fl-12 s-field">
			<span class="f-size-12"><?=t($t_base.'fields/City5');?></span>
			<br>
			<span id="origin_country_span">
			<input type="text" size='9' name="postcodeArr[szCity5]" value='<?=(isset($postcodeDetails['szCity5'])?$postcodeDetails['szCity5']:'');?>'>
			</span>
		</div>
		<div class="fl-12 s-field">
			<span class="f-size-12"><?=t($t_base.'fields/City6');?></span>
			<br>
			<span id="origin_country_span">
			<input type="text" size='9' name="postcodeArr[szCity6]" value='<?=(isset($postcodeDetails['szCity6'])?$postcodeDetails['szCity6']:'');?>'>
			</span>
		</div>
		<div class="fl-12 s-field">
			<span class="f-size-12"><?=t($t_base.'fields/City7');?></span>
			<br>
			<span id="origin_country_span">
			<input type="text" size='9' name="postcodeArr[szCity7]" value='<?=(isset($postcodeDetails['szCity7'])?$postcodeDetails['szCity7']:'');?>'>
			</span>
		</div>
		<div class="fl-12 s-field">
			<span class="f-size-12"><?=t($t_base.'fields/City8');?></span>
			<br>
			<span id="origin_country_span">
			<input type="text" size='9' name="postcodeArr[szCity8]" value='<?=(isset($postcodeDetails['szCity8'])?$postcodeDetails['szCity8']:'');?>'>
			</span>
		</div>
	</div>
	<div style="clear: both;"></div>
	<div class="oh">
		<div class="fl-12 s-field">
			<span class="f-size-12"><?=t($t_base.'fields/City9');?></span>
			<br>
			<span id="origin_country_span">
			<input type="text" size='9' name="postcodeArr[szCity9]" value='<?=(isset($postcodeDetails['szCity9'])?$postcodeDetails['szCity9']:'');?>'>
			</span>
		</div>
		<div class="fl-12 s-field">
			<span class="f-size-12"><?=t($t_base.'fields/City10');?></span>
			<br>
			<span id="origin_country_span">
			<input type="text" size='9' name="postcodeArr[szCity10]" value='<?=(isset($postcodeDetails['szCity10'])?$postcodeDetails['szCity10']:'');?>'>
			<input type="hidden" name="action" value="<?=(isset($postcodeDetails)?'UPDATE_TABLE_POSTCODE':'ADD_TABLE_POSTCODE');?>">
			<input type="hidden" name="postcodeArr[idCountry]" id="countryId" value ='0'>
			</span>
		</div>
		<div class="oh">
		<div class="fl-12 s-field">
			<span class="f-size-12"><?=t($t_base.'fields/priority');?></span>
			<br>
			<span id="origin_country_span">
			<input type="text" size='6' name="postcodeArr[iPriority]" value='<?=(isset($postcodeDetails['iPriority'])?$postcodeDetails['iPriority']:'');?>'>
			</span>
		</div>
		<div class="oh">
			<div class="fl-40 s-field">
				<span class="f-size-40">&nbsp;</span>
				<br>
				<span>
					<a onclick="showPriorityBasedList();" style="cursor: pointer;float: right;"><?=t($t_base.'fields/show_priority_based');?></a>
				</span>
			</div>
		</div>
		<input type="hidden" name="postcodeArr[id]" value='<?=(isset($postcodeDetails['id'])?$postcodeDetails['id']:null);?>'>
	</div>
</form>	
</div>	
<?
}
function showPostcode($t_base)
{
?>		<input type="hidden" id="szHiddenCountry">
		<input type="hidden" id="szHiddenCity">
		<input type="hidden" id="szHiddenPostCode">
		<?=searchPostCode($t_base)?>
		<div id="Error-log" style="padding: 10px 0px 2px 0px"></div>
			<div id="show_me_post_code">
				<?=showPostcodeDetails();?>
			</div>
			</div>	
				<div style="float: right;margin-top: -50px;">
					<a href="javascript:void(0);" id="delete_post_code" class="button2" style="opacity:0.4"><span><?=t($t_base.'fields/delete');?></span></a>
					<a href="javascript:void(0);" id="add_post_code" class="button1" style="opacity:0.4"><span><?=t($t_base.'fields/add');?></span></a>
				</div>
			
			<div style="clear: both;"></div>
			<br />
			<div><?=showCityNotFound()?></div>
			
<? 
}
function showCityNotFound()
{	
	$t_base="management/postcode/";
	$kWHSSearch = new cWHSSearch;
	$countryDetails=$kWHSSearch->cityNotFoundDetails();
	if($countryDetails!=array())
	{
	?>
		<h4><b><?=t($t_base.'fields/city_no_match')?></b></h4>
		
		<table cellpadding="0" id="booking_table" cellspacing="0" class="format-4" width="100%">
		<tr>
			<th width="25%" valign="top"><?=t($t_base.'fields/country')?></th>
			<th width="25%" valign="top"><?=t($t_base.'fields/city')?></th>
			<th width="25%" valign="top"><?=t($t_base.'fields/date');?></th>
			<th width="25%" valign="top"><?=t($t_base.'fields/count');?></th>
		</tr>
		<?
		if($countryDetails!=array())
		{
			foreach($countryDetails as $country)
			{
				//$countryCount=$kWHSSearch->cityCountNotFoundDetails($country['idCountry']);
		?>
		<tr>
			<td align="left" valign="top"><?=$country['szCountry']?></td>
			<td align="left" valign="top"><?=$country['szCity']?></td>
			<td align="left" valign="top"><?=$country['dtCreatedOn']?></td>
			<td align="left" valign="top"><?=$country['value'];?></td>
		</tr>
	<?		}
	?>
	</table>
	<br />
	<a href="javascript:void(0);" style="float: right;" class="button1" onclick="clearCityNotFound(<?=$idCountry;?>)"><span><?=t($t_base.'fields/clear');?></span></a>
	<?
		}
		else
		{
			?>
				<tr>
					<td align="center" valign="top" colspan="3"><b><?=t($t_base.'fields/no_data_found');?></b></td>
				</tr>
			<?
		}
	}
}

function operationFeedbackDetails($idForwarder)
{	
$idForwarder = (int)sanitize_all_html_input($idForwarder);
if(!($idForwarder>0))
{
	header('location:'.__MANAGEMENT_URL__);
}
$kForwarder = new cForwarder;
$szFeedbackArr30=$kForwarder->forwarderFeedback(30,$idForwarder);
$szFeedbackArr90=$kForwarder->forwarderFeedback(90,$idForwarder);
$szFeedbackArr365=$kForwarder->forwarderFeedback(365,$idForwarder);
$szFeedbackArrLt=$kForwarder->forwarderFeedback(0,$idForwarder);
$szFeedbackreview=$kForwarder->forwarderOperationReviewSugg('szReview',$idForwarder,"");
$szFeedbackSuggestion=$kForwarder->forwarderOperationReviewSuggs('szSuggestion',$idForwarder,"");
$t_base = "Feedback/";
//$data=$kForwarder->downloadFeedbackHistory('1');
//print_r($data);
?>
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="format-5" style="border-bottom-color:#FFFFFF;border-top-color:#FFFFFF;">
				<thead>
					<tr>
						<td width="45%" style="border-left-color:#FFFFFF;background-color:#FFFFFF;border-top:#FFFFFF;"><h4><strong><?=t($t_base."title/Feedback_history")?></th>
						<td width="15%" style="border-left-color:#FFFFFF;background-color:#FFFFFF;border-top:#FFFFFF;">&nbsp;</th>
						<td width="10%" align="center" style="background-color:#FFFFFF;border-top:#FFFFFF;"><?=t($t_base."days/30")?></th>
						<td width="10%" align="center" style="background-color:#FFFFFF;border-top:#FFFFFF;"><?=t($t_base."days/90")?></th>
						<td width="10%" align="center" style="background-color:#FFFFFF;border-top:#FFFFFF;"><?=t($t_base."days/365")?></th>
						<td width="10%" align="center" style="background-color:#FFFFFF;border-top:#FFFFFF;"><?=t($t_base."days/lt")?></th>
					</tr>
				</thead>
				<tr>
					<td rowspan="3" valign="top" style="background-color:#FFFFFF;border-left-color:#FFFFFF;"><?=t($t_base."heading/shipment_available_time")?></td>
					<td style="border-left-color:#FFFFFF;"><?=t($t_base."fields/yes")?></td>
					<td align="center"><?if($szFeedbackArr30['totalTimely']=='0'){echo "N/A";}else{echo $szFeedbackArr30['timelyYesPct']."%";}?></td>
					<td align="center"><?if($szFeedbackArr90['totalTimely']=='0'){echo "N/A";}else{echo $szFeedbackArr90['timelyYesPct']."%";}?></td>
					<td align="center"><?if($szFeedbackArr365['totalTimely']=='0'){echo "N/A";}else{echo $szFeedbackArr365['timelyYesPct']."%";}?></td>
					<td align="center"><?if($szFeedbackArrLt['totalTimely']=='0'){echo "N/A";}else{echo $szFeedbackArrLt['timelyYesPct']."%";}?></td>
				</tr>
				<tr>
					<td style="border-left-color:#FFFFFF;"><?=t($t_base."fields/no")?></td>
					<td align="center"><?if($szFeedbackArr30['totalTimely']=='0'){echo "N/A";}else{echo $szFeedbackArr30['timelyNoPct']."%";}?></td>
					<td align="center"><?if($szFeedbackArr90['totalTimely']=='0'){echo "N/A";}else{echo $szFeedbackArr90['timelyNoPct']."%";}?></td>
					<td align="center"><?if($szFeedbackArr365['totalTimely']=='0'){echo "N/A";}else{echo $szFeedbackArr365['timelyNoPct']."%";}?></td>
					<td align="center"><?if($szFeedbackArrLt['totalTimely']=='0'){echo "N/A";}else{echo $szFeedbackArrLt['timelyNoPct']."%";}?></td>
				</tr>
				<tr>
					<td style="background-color:#FFFFFF;border-left-color:#FFFFFF;"><?=t($t_base."fields/count")?></td>
					<td align="center" style="background-color:#FFFFFF;"><?=$szFeedbackArr30['totalTimely'];?></td>
					<td align="center" style="background-color:#FFFFFF;"><?=$szFeedbackArr90['totalTimely'];?></td>
					<td align="center" style="background-color:#FFFFFF;"><?=$szFeedbackArr365['totalTimely'];?></td>
					<td align="center" style="background-color:#FFFFFF;"><?=$szFeedbackArrLt['totalTimely'];?></td>
				</tr>
				<tr>
					<td rowspan="3" valign="top" style="background-color:#FFFFFF;border-left-color:#FFFFFF;"><?=t($t_base."heading/customer_service_prompt_courteous")?></td>
					<td style="border-left-color:#FFFFFF;"><?=t($t_base."fields/yes")?></td>
					<td align="center"><? if($szFeedbackArr30['totalCourtous']=='0'){echo "N/A";}else{echo $szFeedbackArr30['corteousYesPct']."%";}?></td>
					<td align="center"><? if($szFeedbackArr90['totalCourtous']=='0'){echo "N/A";}else{echo $szFeedbackArr90['corteousYesPct']."%";}?></td>
					<td align="center"><? if($szFeedbackArr365['totalCourtous']=='0'){echo "N/A";}else{echo $szFeedbackArr365['corteousYesPct']."%";}?></td>
					<td align="center"><? if($szFeedbackArrLt['totalCourtous']=='0'){echo "N/A";}else{echo $szFeedbackArrLt['corteousYesPct']."%";}?></td>
				</tr>
				<tr>
					<td style="border-left-color:#FFFFFF;"><?=t($t_base."fields/no")?></td>
					<td align="center"><? if($szFeedbackArr30['totalCourtous']=='0'){echo "N/A";}else{echo $szFeedbackArr30['corteousNoPct']."%";}?></td>
					<td align="center"><? if($szFeedbackArr90['totalCourtous']=='0'){echo "N/A";}else{echo $szFeedbackArr90['corteousNoPct']."%";}?></td>
					<td align="center"><? if($szFeedbackArr365['totalCourtous']=='0'){echo "N/A";}else{echo $szFeedbackArr365['corteousNoPct']."%";}?></td>
					<td align="center"><? if($szFeedbackArrLt['totalCourtous']=='0'){echo "N/A";}else{echo $szFeedbackArrLt['corteousNoPct']."%";}?></td>
				</tr>
				<tr>
					<td style="border-left-color:#FFFFFF;background-color:#FFFFFF;"><?=t($t_base."fields/count")?></td>
					<td align="center" style="background-color:#FFFFFF;"><?=$szFeedbackArr30['totalCourtous'];?></td>
					<td align="center" style="background-color:#FFFFFF;"><?=$szFeedbackArr90['totalCourtous'];?></td>
					<td align="center" style="background-color:#FFFFFF;"><?=$szFeedbackArr365['totalCourtous'];?></td>
					<td align="center" style="background-color:#FFFFFF;"><?=$szFeedbackArrLt['totalCourtous'];?></td>
				</tr>
				<tr>
					<td rowspan="4" valign="top" style="background-color:#FFFFFF;border-left-color:#FFFFFF;"><?=t($t_base."heading/Rating_history")?></td>
					<td style="border-left-color:#FFFFFF;"><?=t($t_base."fields/positive")?></td>
					<td align="center"><?if($szFeedbackArr30['totalRating']=='0'){echo "N/A";}else{echo $szFeedbackArr30['positive']."%";}?></td>
					<td align="center"><?if($szFeedbackArr90['totalRating']=='0'){echo "N/A";}else{echo $szFeedbackArr90['positive']."%";}?></td>
					<td align="center"><?if($szFeedbackArr365['totalRating']=='0'){echo "N/A";}else{echo $szFeedbackArr365['positive']."%";}?></td>
					<td align="center"><?if($szFeedbackArrLt['totalRating']=='0'){echo "N/A";}else{echo $szFeedbackArrLt['positive']."%";}?></td>
				</tr>
				<tr>
					<td style="border-left-color:#FFFFFF;"><?=t($t_base."fields/neutral")?></td>
					<td align="center"><?if($szFeedbackArr30['totalRating']=='0'){echo "N/A";}else{echo $szFeedbackArr30['neutral']."%";}?></td>
					<td align="center"><?if($szFeedbackArr90['totalRating']=='0'){echo "N/A";}else{echo $szFeedbackArr90['neutral']."%";}?></td>
					<td align="center"><?if($szFeedbackArr365['totalRating']=='0'){echo "N/A";}else{echo $szFeedbackArr365['neutral']."%";}?></td>
					<td align="center"><?if($szFeedbackArrLt['totalRating']=='0'){echo "N/A";}else{echo $szFeedbackArrLt['neutral']."%";}?></td>
				</tr>
				<tr>
					<td style="border-left-color:#FFFFFF;"><?=t($t_base."fields/negative")?></td>
					<td align="center"><?if($szFeedbackArr30['totalRating']=='0'){echo "N/A";}else{echo $szFeedbackArr30['negative']."%";}?></td>
					<td align="center"><?if($szFeedbackArr90['totalRating']=='0'){echo "N/A";}else{echo $szFeedbackArr90['negative']."%";}?></td>
					<td align="center"><?if($szFeedbackArr365['totalRating']=='0'){echo "N/A";}else{echo $szFeedbackArr365['negative']."%";}?></td>
					<td align="center"><?if($szFeedbackArrLt['totalRating']=='0'){echo "N/A";}else{echo $szFeedbackArrLt['negative']."%";}?></td>
				</tr>
				<tr>
					<td style="border-left-color:#FFFFFF;background-color:#FFFFFF;"><?=t($t_base."fields/count")?></td>
					<td align="center" style="background-color:#FFFFFF;"><?=$szFeedbackArr30['totalRating'];?></td>
					<td align="center" style="background-color:#FFFFFF;"><?=$szFeedbackArr90['totalRating'];?></td>
					<td align="center" style="background-color:#FFFFFF;"><?=$szFeedbackArr365['totalRating'];?></td>
					<td align="center" style="background-color:#FFFFFF;"><?=$szFeedbackArrLt['totalRating'];?></td>
				</tr>
			</table>
			<br />
			
			<div class="oh">
				<div class="fl-40" style="width:43%;">
					<h4><strong><?=t($t_base."title/Recent_reviews")?></strong></h4>
					<div class="feedback-box">
					<? 
					foreach($szFeedbackreview as $key=>$value)
					{	
						echo "<p>".$value['reviewDate']." - ".$value['iRating']." stars: \"".$value['szReview']."\"</p>";
						
					}
					?>
					</div>
				</div>
				
				<div class="fr-55">
					<h4><strong><?=t($t_base."title/Recent_suggestions_for_improvements")?></strong></h4>
					<div class="feedback-box">
					<? 
					foreach($szFeedbackSuggestion as $key=>$value)
					{	
						echo "<p>".$value['reviewDate']."- \"".$value['szSuggestion']."\", ".$value['szFirstName']." ".$value['szLastName']."</p>";
						
					}
					?>
					</div>
				</div>				
			
	
<?
}
function format_date_time($dtDateTitme)
{
	if(!empty($dtDateTitme))
	{
		$dateTimeAry = explode(" ",$dtDateTitme);
		
		if(!empty($dateTimeAry[0]))
		{
			$dateAry = explode('/',$dateTimeAry[0]);
			
			$date = $dateAry[0];
			$month = $dateAry[1];
			$year = $dateAry[2];
			
			$time = $dateTimeAry[1];
			
			$ret_date_time = $year.'-'.$month.'-'.$date." ".$time ;
			return $ret_date_time ;
		}
	}
}
function display_incomplete_user_popup($szBookingRandomNum,$szText)
{	
	$t_base = "BookingConfirmation/";
	//$idBooking = $_SESSION['booking_id'];
	?>	
	<div id="popup-bg"></div>
	 <div id="popup-container">			
		<div class="popup abandon-popup">
		<h5><?=t($t_base.'title/complete_your_profile');?></h5>
		<p><?=t($t_base.'messages/in_order')." ".$szText."".t($t_base.'messages/message_2')?></p>
		<br>
		<div class="oh">
			<p align="center">		
				<a href="javascript:void(0);" onclick="showHide('change_price_div');" class="button1"><span><?=t($t_base.'messages/cancel');?></span></a>
				<a href="<?=__CREATE_ACCOUNT_PAGE_URL__.$szBookingRandomNum.'/'?>"  class="button1"><span><?=t($t_base.'messages/complete_profile');?></span></a>			
			</p>
		</div>
		</div>
	</div>	
	<?php
}

function showForwarderDetails($bOnload=false)
{	
    $t_top_nav="popup/message/";
    if((int)$_SESSION['forwarder_admin_id']==0)
    {
        $idForwarder =	(int)$_SESSION['forwarder_id']; 
        $idForwarderUser = (int)$_SESSION['forwarder_user_id']; 
        $kForwarder = new cForwarder(); 
        $forwarderProfileContactRole = $kForwarder->adminEachForwarder($idForwarderUser);	 
        $kForwarder->load($idForwarder); 
        $logo =$kForwarder->szLogo; 
        
        if(isset($_SESSION['message']))
        {
            $message = $_SESSION['message'];
            $style ='style="display:none;"';
?>
		<script type="text/javascript">
			$(document).ready(function()
			{
				//$('#foo').<? // $message;?>();
				
				var a= $('#message').html();
				
				if(a=='Show less')
				{
					$('#message').html('Show more');
				}
				else
				{
		
				$('#message').html('Show less');
				
				}
		
		});
		
	</script>
		
	<?php
		
	}
        $kBooking = new cBooking();
        $kForwarderBooking = new cForwarderBooking();
        
        $iNumAckBlueIcon = false;
        $iNumAckRedIcon = false;
        $iNumLabelBlueIcon = false;
        $iNumLabelRedIcon = false;
        
        if($bOnload)
        {
            if(!empty($_SESSION['forwarderHeaderNoticationFlagsAry']))
            {
                $notificationFlagsAry = array();
                $notificationFlagsAry = $_SESSION['forwarderHeaderNoticationFlagsAry']; 
                $iNumAckRedIcon = $notificationFlagsAry['iNumAckRedIcon'];
                if($iNumAckRedIcon<=0)
                {
                    $iNumAckBlueIcon = $notificationFlagsAry['iNumAckBlueIcon'];
                }
                
                $iNumLabelRedIcon = $notificationFlagsAry['iNumLabelRedIcon'];
                if($iNumLabelRedIcon<=0)
                {
                    $iNumLabelBlueIcon = $notificationFlagsAry['iNumLabelBlueIcon'];  
                }
                
            }
        }else
        {
            
                unset($_SESSION['forwarderHeaderNoticationFlagsAry']);
                $_SESSION['forwarderHeaderNoticationFlagsAry']=array();
                $iNumAckRedIcon = $kBooking->bookingNotConfirmedDateMoreThanFiveDays($idForwarder,true,false,true);    
                $_SESSION['forwarderHeaderNoticationFlagsAry']['iNumAckRedIcon']=$iNumAckRedIcon;
                if($iNumAckRedIcon<=0)
                {
                    $iNumAckBlueIcon = $kBooking->bookingNotConfirmedDateMoreThanFiveDays($idForwarder,true,true); 
                    $_SESSION['forwarderHeaderNoticationFlagsAry']['iNumAckBlueIcon']=$iNumAckBlueIcon;
                } 
                
                $iNumLabelRedIcon = $kForwarderBooking->getForwarderNotificationForLabel($idForwarder); 
                $_SESSION['forwarderHeaderNoticationFlagsAry']['iNumLabelRedIcon']=$iNumLabelRedIcon;
                if($iNumLabelRedIcon<=0)
                {
                    $iNumLabelBlueIcon = $kForwarderBooking->getForwarderNotificationForLabel($idForwarder,true);  
                    $_SESSION['forwarderHeaderNoticationFlagsAry']['iNumLabelBlueIcon']=$iNumLabelBlueIcon;
                } 
                
        }
        $information  = ($iNumAckBlueIcon || $iNumLabelBlueIcon);
        $warning = ($iNumAckRedIcon || $iNumLabelRedIcon);
        
	if($forwarderProfileContactRole == __ADMINISTRATOR_PROFILE_ID__)		
	{	 
            echo "<div style='clear:both;'></div>"; 
           

            if($bOnload)
            {
                if(!empty($_SESSION['forwarderHeaderNoticationFlagsAry']))
                {
                    $checkComapnyProfile = $notificationFlagsAry['checkComapnyProfile']; 
                    $checkComapnyEmails  = $notificationFlagsAry['checkComapnyEmails']; 
                    
                    $checkTnc = $notificationFlagsAry['checkTnc']; 
                    $checkCFS = $notificationFlagsAry['checkCFS']; 
                    $checkPricingWareh = $notificationFlagsAry['checkPricingWareh'];
                }
            }
            else
            {
                $checkComapnyProfile = $kForwarder->checkForwarderComleteCompanyInfomation($idForwarder); 
                $checkComapnyEmails  = $kForwarder->checkBillingBookingCustomer($idForwarder); 
                //$checkComapnyBankDet = $kForwarder->checkForwarderComleteBankInfomation($idForwarder); 
                $checkTnc = $kForwarder->checkAgreeTNC($idForwarder); 
                $checkCFS = $kForwarder->checkCFS($idForwarder); 
                $checkPricingWareh = $kForwarder->checkWarehouses($idForwarder);
                
                
                $_SESSION['forwarderHeaderNoticationFlagsAry']['checkComapnyProfile']=$checkComapnyProfile;
                $_SESSION['forwarderHeaderNoticationFlagsAry']['checkComapnyEmails']=$checkComapnyEmails;
                $_SESSION['forwarderHeaderNoticationFlagsAry']['checkTnc']=$checkTnc;
                $_SESSION['forwarderHeaderNoticationFlagsAry']['checkCFS']=$checkCFS;
                $_SESSION['forwarderHeaderNoticationFlagsAry']['checkPricingWareh']=$checkPricingWareh;

                
            }
            if($checkComapnyProfile || $checkComapnyEmails  || $checkTnc || (!$checkCFS) || (!$checkPricingWareh)   || ($logo==''))
            {	 
                $offline  = 1; 
            }
            else
            { 
                $offline  = 0;	 
            }
            if($offline) {   ?> 
            <div id="show_message"> 
                <div class="warning"> 
                    <?=t($t_top_nav.'message1');?>: 
                    <ul id="foo" <?=$style?>> 
                        <?php if($checkComapnyProfile){?><li><?=t($t_top_nav.'message2');?> <a href="<?=__FORWARDER_COMPANY_INFORMATION_URL__?>"><?=t($t_top_nav.'here');?></a></li><?php } ?>

                        <?php if($checkComapnyEmails){?><li><?=t($t_top_nav.'message3');?> <a href="<?=__FORWARDER_COMPANY_PREFERENCES_URL__?>"><?=t($t_top_nav.'here');?></a></li><?php } ?> 

                        <?php if( !$checkComapnyEmails && !$checkComapnyProfile && ($logo=='')){?><li><?=t($t_top_nav.'message4');?> <a href="<?=__FORWARDER_COMPANY_INFORMATION_URL__?>"><?=t($t_top_nav.'here');?></a></li><?php } ?>

                        <?php if(!$checkCFS){?><li><?=t($t_top_nav.'message5');?> <a href="<?=__FORWARDER_CFS_LOCATIONS_URL__;?>"><?=t($t_top_nav.'here');?></a></li><?php } ?>

                        <?php if($checkCFS && !$checkPricingWareh){?><li><?=t($t_top_nav.'message6');?> <a href="<?=__FORWARDER_HOME_PAGE_URL__?>/LCLServicesBulk/"><?=t($t_top_nav.'here');?></a></li><?php } ?>

                        <?php if(!$checkComapnyProfile && $checkTnc){?><li><?=t($t_top_nav.'message7');?> <a href="<?=__BASE_URL__?>/T&C/"><?=t($t_top_nav.'here');?></a></li><?php } ?>	 
                    </ul> 
                </div> 
                <div class="show-more-less-alert" onclick="toggle();"><a id="message" href="javascript:void(0);"><?=t($t_top_nav.'show_less');?></a></div> 
            </div> 
            <?php } else {
                
                
               
                if($bOnload)
                {
                    if(!empty($_SESSION['forwarderHeaderNoticationFlagsAry']))
                    {
                        $versionUpdate=$notificationFlagsAry['versionUpdate'];
                        $checkPricingHaulage=$notificationFlagsAry['checkPricingHaulage'];
                        $checkCC=$notificationFlagsAry['checkCC'];
                        $checkforwarderContacts=$notificationFlagsAry['checkforwarderContacts'];
                        $checkComapnyBankDet=$notificationFlagsAry['checkComapnyBankDet'];
                        $checkComapnyBankDetils=$notificationFlagsAry['checkComapnyBankDetils'];
                        $updateLclServicesOBO=$notificationFlagsAry['updateLclServicesOBO'];
                        $updateTryItOut=$notificationFlagsAry['updateTryItOut'];
                        $priceAwaitingForApproval=$notificationFlagsAry['priceAwaitingForApproval'];
                        $waitingForwarderDataApproval=$notificationFlagsAry['waitingForwarderDataApproval'];
                    }
                }
                else
                {
                        $versionUpdate = $kForwarder->iVersionUpdate;		

                        $checkPricingHaulage = $kForwarder->checkPricingHaulage($idForwarder);

                        $checkCC = $kForwarder->checkCC($idForwarder);

                        $checkforwarderContacts = $kForwarder->checkforwarderContacts($idForwarder); //default true
                        //status 2 for payment paid by ADMIN
                        $checkComapnyBankDet = $kForwarder->checkForwarderComleteBankInfomation($idForwarder,2);
                        //status 1 for payment recieved by ADMIN
                        $checkComapnyBankDetils	= $kForwarder->checkForwarderComleteBankInfomation($idForwarder,1);

                        $kConfig = new cConfig; 
                        $updateLclServicesOBO = $kConfig->findForwarderOBOHaulageStauts($idForwarderUser);

                        $updateTryItOut	= $kConfig->findForwarderTryItOutStauts($idForwarderUser); //default true

                        $kUploadBulkService = new cUploadBulkService(); 
                        $priceAwaitingForApproval = $kUploadBulkService->findStatusOfCostApprovalAlarm(__COST_AWAITING_APPROVAL__,$idForwarder); 
                        $waitingForwarderDataApproval = $kUploadBulkService->findStatusOfCostApprovalAlarm(__MANAGEMENT_COMPLETE_APPROVAL__,$idForwarder,__DATA_REVIEWED_BY_FORWARDER__); 
                        
                        $_SESSION['forwarderHeaderNoticationFlagsAry']['versionUpdate']=$versionUpdate;
                        $_SESSION['forwarderHeaderNoticationFlagsAry']['checkPricingHaulage']=$checkPricingHaulage;
                        $_SESSION['forwarderHeaderNoticationFlagsAry']['checkCC']=$checkCC;
                        $_SESSION['forwarderHeaderNoticationFlagsAry']['checkforwarderContacts']=$checkforwarderContacts;
                        $_SESSION['forwarderHeaderNoticationFlagsAry']['checkComapnyBankDet']=$checkComapnyBankDet;
                        $_SESSION['forwarderHeaderNoticationFlagsAry']['checkComapnyBankDetils']=$checkComapnyBankDetils;
                        $_SESSION['forwarderHeaderNoticationFlagsAry']['updateLclServicesOBO']=$updateLclServicesOBO;
                        $_SESSION['forwarderHeaderNoticationFlagsAry']['updateTryItOut']=$updateTryItOut;
                        $_SESSION['forwarderHeaderNoticationFlagsAry']['priceAwaitingForApproval']=$priceAwaitingForApproval;
                        $_SESSION['forwarderHeaderNoticationFlagsAry']['waitingForwarderDataApproval']=$waitingForwarderDataApproval;
                }
		$counter = 0; 
		if( !(boolean)$checkPricingHaulage || !(boolean)$checkCC || !(boolean)$checkforwarderContacts || (boolean)$updateLclServicesOBO || (boolean)$updateTryItOut || $updateLclServicesOBO || $checkComapnyBankDet || $checkComapnyBankDetils || $versionUpdate || $priceAwaitingForApproval || $waitingForwarderDataApproval || $warning)
		{	 
                    if( $checkPricingHaulage && $checkCC )		
                    {		
                        $online = 0;
                    }
                    else
                    {
                        $online = 1;
                    }
		?>		
		<div  id="show_message">
                    <?php if($warning) { ?>
                        <?php if($iNumAckRedIcon>0){ ?>
                            <div class="warning" id="NONACKNOWLEDGE">
                                <p><?php echo $iNumAckRedIcon." ".(($iNumAckRedIcon>1)?t($t_top_nav.'message28'):t($t_top_nav.'message29')) ;?> <a href="<?php echo __FORWARDER_BOOKING_URL__; ?>"><?=t($t_top_nav.'here');?></a>  </p>
                            </div> 
                        <?php } if($iNumLabelRedIcon) {?>
                            <div class="warning" id="NONACKNOWLEDGE">
                                <p><?php echo t($t_top_nav.'message30'); ?> <a href="<?php echo __FORWARDER_PENDING_QUOTES_URL__; ?>"><?=t($t_top_nav.'here');?></a></p>
                            </div>
                        <?php } ?>
                    <?php } ?>
			<div class="information">
			<?php
				if($versionUpdate)
				{
					$counter++; 
					?>
					<p><?=t($t_top_nav.'message24_1');?> <a href="<?=__BASE_URL__?>/T&C/"><?=t($t_top_nav.'here');?></a>  <?=t($t_top_nav.'message24_2');?> <?=date('d F Y',strtotime ( '+30 DAY' ,strtotime($kForwarder->dtVersionUpdate)))?></p>
					<?php 
				} 
				elseif($checkComapnyBankDet)
				{			
					$counter++; 
			?>
					<p><?=t($t_top_nav.'message8');?> <a href="<?=__FORWARDER_COMPANY_BANK_DETAILS_URL__;?>"><?=t($t_top_nav.'here');?></a></p>
			<?php 
				}
				else if($checkComapnyBankDetils)
				{ 
                                    $counter++; 
			?>
				 	<p><?=t($t_top_nav.'message9');?> <a href="<?=__FORWARDER_COMPANY_BANK_DETAILS_URL__;?>"><?=t($t_top_nav.'here');?></a></p>
			<?php  } if($iNumAckBlueIcon) { $counter++;  ?>
                                <p id="NONACKNOWLEDGE"><?php echo $iNumAckBlueIcon." ".(($iNumAckBlueIcon>1)?t($t_top_nav.'message28'):t($t_top_nav.'message29')) ;?> <a href="<?php echo __FORWARDER_BOOKING_URL__; ?>"><?=t($t_top_nav.'here');?></a>  </p> 
			<?php
                            } if($iNumLabelBlueIcon){ $counter++;  ?>
                                <p id="NONACKNOWLEDGE"><?php echo t($t_top_nav.'message31'); ?> <a href="<?php echo __FORWARDER_PENDING_QUOTES_URL__; ?>"><?=t($t_top_nav.'here');?></a>  </p> 
                            <?php }
                            if(($checkComapnyBankDetils || $checkComapnyBankDet || $versionUpdate ) && $online)
                            {
                                echo "<div id=\"foo\" ".$style.">";
                                $ch = 1;
                            }
			?>
			<?php 
                            if($online)
                            { 
				?>
				<?=t($t_top_nav.'message10');?>:
				<?php
				if($online && (!$checkComapnyBankDetils && !$checkComapnyBankDet && !$versionUpdate))
				{	
					echo "<div id=\"foo\" ".$style.">";
					$ch = 1;
				}
					
				?>
				<ul>	
					<?php if(!$checkPricingHaulage){ $counter++; ?><li><?=t($t_top_nav.'message11');?> <a href="<?=__FORWARDER_HOME_PAGE_URL__?>/Haulage/"><?=t($t_top_nav.'here');?></a></li><?php } ?>
					<?php if(!$checkCC){ $counter++; ?><li><?=t($t_top_nav.'message12');?>  <a href="<?__FORWARDER_HOME_PAGE_URL__?>/CustomsClearanceBulk/"><?=t($t_top_nav.'here');?></a></li><?php } ?> 
			    </ul>
                            <?php 
			}
			else if(!$online)
			{	
                            if(!$online && ($checkComapnyBankDetils || $checkComapnyBankDet || $versionUpdate))
                            {
                                echo "<div id=\"foo\" ".$style.">";
                                $ch = 1;
                            }
			}
			
			if(!$checkforwarderContacts)
			{ 
				$counter++;
		?>
				<p <?php if($counter>1){echo "style='margin-left:0px;'"; }?>><?=t($t_top_nav.'message13');?> <a href="<?=__FORWARDER_COMPANY_PAGE_URL__;?>"><?=t($t_top_nav.'here');?></a></p>
		<?php  }		 
			if($updateTryItOut && ($counter<4) )
			{ 
				$counter++; 
				if( (!$online && !$checkComapnyBankDetails && !$checkComapnyBankDet && !$versionUpdate) &&  ($counter == 2) )
				{
					echo '<div id="foo" '.$style.'>'; 
					$ch = 1;
				}
		?>
				<p><?=t($t_top_nav.'message14');?>  <a href="<?=__FORWARDER_TRY_IT_OUT_URL__;?>"><?=t($t_top_nav.'here');?></a></p>
		<?php   }
		 
			 if($updateLclServicesOBO && ($counter<5) )
			 { 
			 	$counter++; 
			 	if(!$online && (!$checkComapnyBankDetails && !$checkComapnyBankDet && !$versionUpdate) &&  ($counter == 2) )
			 	{
			 		echo '<div id="foo" '.$style.'>';
			 		$ch = 1; 
			 	}
		 ?>
		 		<p><?=t($t_top_nav.'message15');?>  <a href="<?=__FORWARDER_OBO_LCL_SERVICES_URL__?>"><?=t($t_top_nav.'here');?></a></p>
		 	<?php } 
		 	if($priceAwaitingForApproval || $waitingForwarderDataApproval)
		 	{	$ed = 1;
		 		//$counter++; 
		 		if($priceAwaitingForApproval && $waitingForwarderDataApproval && $counter ==1)
				{	
					echo '<div id="foo" '.$style.'>'; 
					
				}
		 		if($priceAwaitingForApproval)
		 		{	$ch = 1;
		 	?>
		 		<p><?=t($t_top_nav.'message26');?>  <a href="<?=__FORWARDER_BULK_SERVICES_APPROVAL_URL__?>"><?=t($t_top_nav.'here');?></a></p>
		 	<?php  }
		 		if($waitingForwarderDataApproval)
		 		{ 	
		 			if($ch == 1 && $ed >0 && $counter ==0)
		 			{
		 				echo '<div id="foo" '.$style.'>'; 
						
		 			}
		 			$ch++;
		 		?>
		 		<p><?=t($t_top_nav.'message27');?>  <a href="<?=__FORWARDER_BULK_SERVICES_APPROVAL_URL__?>"><?=t($t_top_nav.'here');?></a></p>	
		 	<?php  }
					if($ch==2)
		 			{
		 				echo "</div>";
		 			}	
		 	}
		 	?>	
		 			
			</div>
			<?php	
			   if( ( $online || (int)$counter>1 ) || ($ch == 2))
			   {
                                ?>
					<div  style="position: absolute; bottom: 1px; right: 26px;" onclick="toggle();"><a id="message" href="javascript:void(0);"><?=t($t_top_nav.'show_less');?></a></div>
			<?php  } ?>
			</div>
		</div>
		<?php
			}
		
		}
		
	}	
	else if($forwarderProfileContactRole == __PRICING_PROFILE_ID__)	
	{
		$kConfig = new cConfig;
                $kUploadBulkService = new cUploadBulkService();
               
                if($bOnload)
                {
                    if(!empty($_SESSION['forwarderHeaderNoticationFlagsAry']))
                    {
                        $versionUpdate = $notificationFlagsAry['versionUpdate'];
                        $checkComapnyProfile = $notificationFlagsAry['checkComapnyProfile'];
                        $checkComapnyEmails=$notificationFlagsAry['checkComapnyEmails'];
                        $checkCFS=$notificationFlagsAry['checkCFS'];
                        $checkTnc=$notificationFlagsAry['checkTnc'];
                        $updateLclServicesOBO=$notificationFlagsAry['updateLclServicesOBO'];
                        $priceAwaitingForApproval=$notificationFlagsAry['priceAwaitingForApproval'];
                        $waitingForwarderDataApproval=$notificationFlagsAry['waitingForwarderDataApproval'];
                    }
                }
                else
                {
                        $versionUpdate			= $kForwarder->iVersionUpdate;

                        $checkComapnyProfile 	= $kForwarder->checkForwarderComleteCompanyInfomation($idForwarder);

                        $checkComapnyEmails 	= $kForwarder->checkBillingBookingCustomer($idForwarder);

                        $checkCFS			 	= $kForwarder->checkCFS($idForwarder);	

                        $checkTnc			 	= $kForwarder->checkAgreeTNC($idForwarder);

                        $updateLclServicesOBO	= $kConfig->findForwarderOBOHaulageStauts($idForwarderUser);
                        $priceAwaitingForApproval = $kUploadBulkService->findStatusOfCostApprovalAlarm(__COST_AWAITING_APPROVAL__,$idForwarder);

                        $waitingForwarderDataApproval = $kUploadBulkService->findStatusOfCostApprovalAlarm(__MANAGEMENT_COMPLETE_APPROVAL__,$idForwarder);

                        $_SESSION['forwarderHeaderNoticationFlagsAry']['versionUpdate']=$versionUpdate;
                        $_SESSION['forwarderHeaderNoticationFlagsAry']['checkComapnyProfile']=$checkComapnyProfile;
                        $_SESSION['forwarderHeaderNoticationFlagsAry']['checkComapnyEmails']=$checkComapnyEmails;
                        $_SESSION['forwarderHeaderNoticationFlagsAry']['checkCFS']=$checkCFS;
                        $_SESSION['forwarderHeaderNoticationFlagsAry']['checkTnc']=$checkTnc;
                        $_SESSION['forwarderHeaderNoticationFlagsAry']['updateLclServicesOBO']=$updateLclServicesOBO;
                        $_SESSION['forwarderHeaderNoticationFlagsAry']['priceAwaitingForApproval']=$priceAwaitingForApproval;
                        $_SESSION['forwarderHeaderNoticationFlagsAry']['waitingForwarderDataApproval']=$waitingForwarderDataApproval;
                }
		if((!$checkCFS) || $checkTnc || $checkComapnyProfile || $checkComapnyEmails || ($logo==''))
		{ 
                    echo "<div style='clear:both;'></div>"; 
                    ?> 
                    <div  id="show_message"> 
                        <div class="warning"> 
                            <?=t($t_top_nav.'message16');?>: 
                            <ul id="foo" <?=$style?>> 
                                <?php if(!$checkCFS){?><li><?=t($t_top_nav.'message17');?> <a href="<?=__BASE_URL__?>/T&C/"><?=t($t_top_nav.'here');?></a></li><? } ?> 
                                <?php if($checkComapnyProfile || $checkComapnyEmails ){?><li><?=t($t_top_nav.'message18');?></li><? } ?> 
                                <?php if( !$checkComapnyEmails && !$checkComapnyProfile && ($logo=='')){?><li><?=t($t_top_nav.'message19');?></li><? } ?> 
                                <?php if($checkTnc){?><li><?=t($t_top_nav.'message20');?> <a href="<?=__BASE_URL__?>/T&C/"><?=t($t_top_nav.'here');?></a></li><? } ?> 
                            </ul> 
                        </div>	 
                        <div style="position: absolute; bottom: 1px; right: 26px;" onclick="toggle();"><a id="message" href="javascript:void(0);"><?=t($t_top_nav.'show_less');?></a></div> 
                    </div> 
                    <?php 
		}
		else if($updateLclServicesOBO || $versionUpdate || $priceAwaitingForApproval || $waitingForwarderDataApproval)
		{ 
                    $count = 0;
		?> 
		 <div  id="show_message"> 
                     <?php if($warning) { ?>
                        <?php if($iNumAckRedIcon>0){ ?>
                            <div class="warning" id="NONACKNOWLEDGE">
                                <p><?php echo $iNumAckRedIcon." ".(($iNumAckRedIcon>1)?t($t_top_nav.'message28'):t($t_top_nav.'message29')) ;?> <a href="<?php echo __FORWARDER_BOOKING_URL__; ?>"><?=t($t_top_nav.'here');?></a>  </p>
                            </div> 
                        <?php } if($iNumLabelRedIcon) {?>
                            <div class="warning" id="NONACKNOWLEDGE">
                                <p><?php echo t($t_top_nav.'message30'); ?> <a href="<?php echo __FORWARDER_PENDING_QUOTES_URL__; ?>"><?=t($t_top_nav.'here');?></a></p>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <div class="information">
                        <?php if($versionUpdate) { ?>
                            <p><?=t($t_top_nav.'message25_1');?>  <?=date('d F Y',strtotime("+ 30 DAY",strtotime($kForwarder->dtVersionUpdate)))?> <?=t($t_top_nav.'message25_2');?></p>
                        <?php  } if($versionUpdate && ($updateLclServicesOBO || $waitingForwarderDataApproval || $priceAwaitingForApproval )) { $count++; ?>
                                <div id="foo" <?=$style?>> 
                        <?php } if($updateLclServicesOBO ) { ?>
                                <p><?=t($t_top_nav.'message21');?>  <a href="<?=__FORWARDER_OBO_LCL_SERVICES_URL__?>"><?=t($t_top_nav.'here');?></a></p>
                        <?php  } if($updateLclServicesOBO && $count == 0) { $count++; ?>
                                <div id="foo" <?=$style?>> 
                        <?php } if($waitingForwarderDataApproval) {
                                $ch == 0;
                                $ch++;
                        ?>	
                            <p><?=t($t_top_nav.'message26');?>  <a href="<?=__FORWARDER_BULK_SERVICES_APPROVAL_URL__?>"><?=t($t_top_nav.'here');?></a></p>
                        <?php  }
			 		if($priceAwaitingForApproval)
			 		{ 	
			 			
			 			if($ch == 1 && $count == 0)
			 			{
			 				$count++;
			 			?>
			 				<div id="foo" <?=$style?>>
			 			<?php
			 			}
			 		?>
			 		<p><?=t($t_top_nav.'message27');?>  <a href="<?=__FORWARDER_BULK_SERVICES_APPROVAL_URL__?>"><?=t($t_top_nav.'here');?></a></p>	
			 	<?php  }	
						?>
		 		</div>
		 			<?php if($count > 0 || $ch == 1)
					{
					?>
		 			<div  style="position: absolute; bottom: 1px; right: 26px;" onclick="toggle();"><a id="message" href="javascript:void(0);"><?=t($t_top_nav.'show_less');?></a></div>
					<?php } ?>
				</div>		
		 	</div>	
		
		 <?php	
		}	
	}
	else if($forwarderProfileContactRole == __BILLING_PROFILE_ID__)	
	{

        if($bOnload)
        {
            if(!empty($_SESSION['forwarderHeaderNoticationFlagsAry']))
            {
                $versionUpdate = $notificationFlagsAry['versionUpdate'];
                $checkComapnyBankDet = $notificationFlagsAry['checkComapnyBankDet'];
                $checkComapnyBankDetils=$notificationFlagsAry['checkComapnyBankDetils'];
            }
        }
        else
        {
            $versionUpdate = $kForwarder->iVersionUpdate;
            $checkComapnyBankDet = $kForwarder->checkForwarderComleteBankInfomation($idForwarder,2);
            $checkComapnyBankDetils = $kForwarder->checkForwarderComleteBankInfomation($idForwarder,1);

            $_SESSION['forwarderHeaderNoticationFlagsAry']['versionUpdate']=$versionUpdate;
            $_SESSION['forwarderHeaderNoticationFlagsAry']['checkComapnyProfile']=$checkComapnyBankDet;
            $_SESSION['forwarderHeaderNoticationFlagsAry']['checkComapnyBankDetils']=$checkComapnyBankDetils;
            
        }

            if( $checkComapnyBankDetils || $checkComapnyBankDet || $versionUpdate )
            {
	?> 
                <div  id="show_message">
                    <?php if($warning) { ?>
                       <?php if($iNumAckRedIcon>0){ ?>
                           <div class="warning" id="NONACKNOWLEDGE">
                               <p><?php echo $iNumAckRedIcon." ".(($iNumAckRedIcon>1)?t($t_top_nav.'message28'):t($t_top_nav.'message29')) ;?> <a href="<?php echo __FORWARDER_BOOKING_URL__; ?>"><?=t($t_top_nav.'here');?></a>  </p>
                           </div> 
                       <?php } if($iNumLabelRedIcon) {?>
                           <div class="warning" id="NONACKNOWLEDGE">
                               <p><?php echo t($t_top_nav.'message30'); ?> <a href="<?php echo __FORWARDER_PENDING_QUOTES_URL__; ?>"><?=t($t_top_nav.'here');?></a></p>
                           </div>
                       <?php } ?>
                   <?php } ?>
                   <div class="information">
                       <?php if($versionUpdate) { ?>
                           <p><?=t($t_top_nav.'message25_1');?>   <?=date('d F Y',strtotime("+ 30 DAY",strtotime($kForwarder->dtVersionUpdate)))?> <?=t($t_top_nav.'message25_2');?></p>
                       <?php } ?>
                           <div id="foo" <?=$style?>>
                               <?php if($checkComapnyBankDet){ ?><p><?=t($t_top_nav.'message22');?></p>
                               <?php  }else if($checkComapnyBankDetils){ ?><p><?=t($t_top_nav.'message23');?></p><?php } ?>
                           </div>	
                       <?php if($versionUpdate && ($checkComapnyBankDet || $checkComapnyBankDetils)) { ?>
                           <div  style="position: absolute; bottom: 1px; right: 26px;" onclick="toggle();"><a id="message" href="javascript:void(0);"><?=t($t_top_nav.'show_less');?></a></div>
                       <?php } ?>
                   </div> 
               </div>	 
            <?php  } ?>
            <div style="clear: both"></div>
	<?php
	}
    }
}

function showBillingDetails($t_base,$invoiceBillConfirmed,$invoiceBillPending,$sumPendingBalance,$idForwarder,$invoicePendingByAdmin,$invoiceSumPendingByAdmin='',$detailsBankAcc,$dateFormSearchResult,$sumCurrentBalance='',$referalAndTransferFee='',$totalAmountAry=array(),$currentAvailableAmountAry=array(),$totalAmountOfUploadServiceAry=array())
{		
	$kBooking = new cBooking();
	$kForwarder = new cForwarder();
	$data=array();
	$data['iBillingYear']=date('Y');
	$booking_searched_data = $kBooking->search_forwarder_billings($data);
                
        $kForwarderNew = new cForwarder();
        $paymentFrequency = $kForwarderNew->getAllPaymentFrequency();
        $frequency = $paymentFrequency[$detailsBankAcc['szPaymentFrequency']]['szPaymentFrequency'];
                
	$strlen=strlen($detailsBankAcc['iAccountNumber']);
	if($strlen>10)
	{
		for($i=6;$i>0;$i--)
		{
			$detailsBankAccount.='x';
		}
		$detailsBankAccount.=mb_substr($detailsBankAcc['iAccountNumber'],$strlen-4,4,'UTF8');
	}
	else
	{
		$detailsBankAccount=$detailsBankAcc['iAccountNumber'];
	}
	$strlenBank=strlen($detailsBankAcc['szBankName']);
	if($strlenBank > 12)
	{
		$detailsBankName=mb_substr($detailsBankAcc['szBankName'],0,12,'UTF8')."...";
	}
	else
	{
		$detailsBankName=$detailsBankAcc['szBankName'];
	}
	$currencyArr=array();
	$newCurrentBalanceStr='';
	$newCurrentBalanceString='';
	if(!empty($totalAmountAry))
	{
		$r=1;
		foreach($totalAmountAry as $key=>$totalAmountArys)
		{
			//print_r($totalAmountArys);
			
			$currencyArr[]=$key;
			$subTotalCurrency=0;
			if(!empty($totalAmountOfUploadServiceAry))
			{
				$subTotalCurrency=$totalAmountOfUploadServiceAry[$key]['fTotalBalance'];
			}
			//echo $subTotalCurrency;
			$newAmount=0;
			//echo $totalAmountArys['fTotalBalance']."<br/>";
			if((float)$subTotalCurrency>0)
			{
				$newAmount=$totalAmountArys['fTotalBalance']-$subTotalCurrency;
			}
			else
			{
				$newAmount=$totalAmountArys['fTotalBalance'];
			}
			//echo $newAmount."<br/>";
			if($newAmount>0)
			{
				$newCurrentBalanceArr[]=$totalAmountArys['szForwarderCurrency']." ".number_format((float)$newAmount,2);
			}
			else
			{
				$newAmount=str_replace("-","",$newAmount);
				$newCurrentBalanceArr[]="(".$totalAmountArys['szForwarderCurrency']." ".number_format((float)$newAmount,2).")";
			}
			//$newCurrentBalanceSumArr[$referalAndTransferFees['szForwarderCurrency']]=$referalAndTransferFees['iCreditBalance'];
			//$total=$total+$totalAmountArys['fTotalBalance'];
		}
				
		if(!empty($newCurrentBalanceArr))
		{
			$newCurrentBalanceAry = format_fowarder_emails($newCurrentBalanceArr);
			$newCurrentBalanceStr = $newCurrentBalanceAry[1];
		}
	}
	//print_r($totalAmountOfUploadServiceAry);
	$subAmount=0;
	if(!empty($totalAmountOfUploadServiceAry))
	{
		foreach($totalAmountOfUploadServiceAry as $key=>$totalAmountOfUploadServiceArrs)
		{
			if(!in_array($key,$currencyArr))
			{
				if($detailsBankAcc['fwdCurrency']==$key)
				{
					$subAmount=$totalAmountOfUploadServiceArrs['fTotalBalance'];
				}
				else
				{
					$newUploadServicePaymentArr[]="(".$key." ".number_format((float)$totalAmountOfUploadServiceArrs['fTotalBalance'],2).")";
				}
			}
		}
		if(!empty($newUploadServicePaymentArr))
		{
			$newServiceBalanceAry = format_fowarder_emails($newUploadServicePaymentArr);
			$newServiceBalanceStr = $newServiceBalanceAry[1];
		}
	}
	
	$newPendingBalanceStr='';
	
	if(!empty($sumPendingBalance))
	{
		$pendCount=count($sumPendingBalance);
		$l=1;
		foreach($sumPendingBalance as $sumPendingBalances)
		{	
			$newPendingBalanceArr[]=$sumPendingBalances['szForwarderCurrency']." ".number_format((float)$sumPendingBalances['iCredit'],2);
		}
		if(!empty($newPendingBalanceArr))
		{
			$newPendingBalanceStrAry = format_fowarder_emails($newPendingBalanceArr);
			$newPendingBalanceStr = $newPendingBalanceStrAry[1];
		}
	}
	
	$currentBalanceAmount=number_format((float)($referalAndTransferFee['iCreditBalance']-$subAmount),2);
	if($currentBalanceAmount>0)
	{
		$currentBalanceAmount=$detailsBankAcc['fwdCurrency']." ".$currentBalanceAmount;
	}
	else
	{
		$currentBalanceAmount=$detailsBankAcc['fwdCurrency']." ".$currentBalanceAmount;
	}
?>
<table style="font-size: 17px;margin-left:-3px;">
	<tr>
		<td>
			<?=t($t_base.'title/current_balance');?> <a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/current_balance');?>','<?=t($t_base.'messages/current_balance_details');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td><td></td><td id="payment_pending" style="font-style: italic;">
			<?=!empty($newCurrentBalanceStr)?$newCurrentBalanceStr:$currentBalanceAmount?> <?=!empty($newServiceBalanceStr)?$newServiceBalanceStr:""?>
		</td>
		<td class="bank_name">
			<?=t($t_base."title/transfer_are_made_to")?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/transfer_frequency');?>','<?=t($t_base.'messages/transfer_frequency_details');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td><td></td><td style="font-style: italic;"> <?if(!$kForwarder->checkForwarderComleteBankDetails($idForwarder)){ ?><?=$detailsBankName;?></td> <td style="font-style: italic;">account  <?=$detailsBankAccount;?><?}else{echo 'To be updated';}?>
		</td>
	</tr>
	<tr>
		<td width="200px"> 
			<?=t($t_base.'title/pending_settlements');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/transfer_are_made_to');?>','<?=t($t_base.'messages/transfer_are_made_to_details');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td><td></td><td style="font-style: italic;">
			<?=!empty($newPendingBalanceStr)?$newPendingBalanceStr:$detailsBankAcc['fwdCurrency']." ".($sumPendingBalance['iCredit']?number_format($sumPendingBalance['iCredit'],2):' 0.00');?>
		</td>
		<td><?=t($t_base."title/transfer_frequency")?> <a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/pending_settlements');?>','<?=t($t_base.'messages/pending_settlements_details');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
		<td></td>
		<td style="font-style: italic;"><?=ucfirst($frequency)?></td>
	</tr>	
</table>
<div style="clear: both"></div>
<div id="forwarder_billing_bottom_form" class="oh" style="padding-top: 20px;padding-bottom: 0px;">

	<table cellpadding="0" style="width: 100%;" cellspacing="0" class="format-4" id="booking_table">
		<tr >
			<th width="8%" valign="top" ><?=t($t_base.'fields/date')?></th>
			<th width="23%" valign="top" ><?=t($t_base.'fields/description')?></th>
			<th width="13%" valign="top" ><?=t($t_base.'fields/your');?> <?=t($t_base.'fields/reference');?></th>	
			<th width="10%" valign="top" ><?=t($t_base.'fields/invoice')?> <?=t($t_base.'fields/number')?></th>				
			<th width="14%" valign="top" style="text-align: right;"><?=t($t_base.'fields/invoice')?> <?=t($t_base.'fields/value')?></th>
			<th width="7%" valign="top" style="text-align: right;"><?=t($t_base.'fields/roe')?>*</th>
			<th width="12%" valign="top" style="text-align: right;"><?=t($t_base.'fields/your_currency')?> </th>
			<th width="13%" valign="top" style="text-align: right;"><?=t($t_base.'fields/balance')?></th>
		</tr>
		<?	
			if(!empty($invoiceBillConfirmed) && count($invoiceBillConfirmed)>1)
			{ 	
				if($referalAndTransferFee)
				{
					$sum=(float)$total;
					$gtotal=$newCurrentBalanceString;
				}
				else
				{
					$sum=0;
					$gtotal="0.00";
				}
				
				$launchDate=strtotime('01-10-2012 00:00:00');		
				$ctr=1;
				foreach($invoiceBillConfirmed as $searchResults)
				{					
					if($searchResults["dtPaymentConfirmed"]!='' && $searchResults["dtPaymentConfirmed"]!='0000-00-00 00:00:00')
					{	
						if($searchResults['iDebitCredit']==1)
						{	
							$idCurency = $searchResults['szForwarderCurrency'] ;
							
							$i=1;
							$currency=$searchResults['szForwarderCurrency'];
							$minDate=strtotime($searchResults['dtCreditOn']);
							if($minDate<$launchDate)
							{
								$launchDate=$minDate;
							}
							
							$sumAry[$idCurency] = (float)($currentAvailableAmountAry[$idCurency]['fTotalBalance'] + $searchResults['fTotalPriceForwarderCurrency']);
							$curr_sum='';
							if($sumAry[$idCurency]>0)
							{
								$curr_sum=$searchResults["szForwarderCurrency"]." ".number_format((float)$sumAry[$idCurency],2);
							}
							else
							{
								$newAmount=str_replace("-","",$sumAry[$idCurency]);
								$curr_sum="(".$searchResults["szForwarderCurrency"]." ".number_format((float)$newAmount,2).")";
							}
							$forwarderTransferDetails[]='			
								<tr id="booking_data_'.$ctr.'" onclick="select_forwarder_billings_tr(\'booking_data_'.$ctr++.'\',\''.$searchResults[idBooking].'\')">
								<td>'.((!empty($searchResults["dtPaymentConfirmed"]) && $searchResults["dtPaymentConfirmed"]!="0000-00-00 00:00:00")?DATE("d-M",strtotime($searchResults["dtPaymentConfirmed"])):"").'</td>
								<td>'.$searchResults["szBooking"].'</td>
								<td>'.$searchResults["szBookingNotes"].'</td>
								<td>'.$searchResults["szInvoice"].'</td>
								<td style="text-align: right;">'.$searchResults["szCurrency"].' '.number_format($searchResults["fTotalAmount"],2).'</td>
								<td style="text-align: right;">'.number_format((float)$searchResults["fExchangeRate"],4).'</td>
								<td style="text-align: right;">'.$searchResults["szForwarderCurrency"].' '.number_format(abs((float)$searchResults["fTotalPriceForwarderCurrency"]),2).'</td>
								<td style="text-align: right;">'.$curr_sum.'</td>	
								</tr> ';
							 //echo "<br> substracting ".$sumAry[$idCurency]." and ".$searchResults['fTotalPriceForwarderCurrency'] ;
							
							 $currentAvailableAmountAry[$idCurency]['fTotalBalance'] = $sumAry[$idCurency];
						}
						else if(($searchResults['iDebitCredit']==2 || $searchResults['iDebitCredit']==3) && $searchResults['iStatus']==2)
						{ 
							$idCurency = $searchResults['szCurrency'] ;		
							//$sumAry[$idCurency]= ;
							$sumAry[$idCurency] = (float)($currentAvailableAmountAry[$idCurency]['fTotalBalance'] - $searchResults['fTotalPriceForwarderCurrency']);
							$curr_sum='';
							if($sumAry[$idCurency]>0)
							{
								$curr_sum=$searchResults['szCurrency']." ".number_format((float)$sumAry[$idCurency],2);
							}
							else
							{
								$newAmount=str_replace("-","",$sumAry[$idCurency]);
								$curr_sum="(".$searchResults['szCurrency']." ".number_format((float)$newAmount,2).")";
							}
							$flag='';
							if(trim($searchResults['szBooking'])==='Transporteca referral fee')
							{
								$flag="Referral";	 
							}
							else if(trim($searchResults['szBooking'])==="Automatic transfer")
							{
								$flag="Transfer";
							}							
					
							$forwarderTransfer.="			
							<tr id='booking_data_".$ctr."' onclick='select_forwarder_billings_referral_tr(\"booking_data_".$ctr++."\",\"".$searchResults['iBatchNo']."\",\"".$idForwarder."\",\"".$flag."\")'><td>".((!empty($searchResults['dtPaymentConfirmed']) && $searchResults['dtPaymentConfirmed']!='0000-00-00 00:00:00')?DATE('d-M',strtotime($searchResults['dtPaymentConfirmed'])):'')."</td>";
							if(trim($searchResults['szBooking'])=='Transporteca referral fee')
							{ 
								$forwarderTransfer.= "<td>Transporteca Referral Fee Invoice</td><td></td>";
							}
							if(trim($searchResults['szBooking'])==='Automatic transfer')
							{
								$forwarderTransfer.= "<td>".$searchResults['szBooking']."</td><td></td>";
							} 
							$forwarderTransfer.= "<td>";
							if(trim($searchResults['szBooking'])==="Transporteca referral fee")
							{ 
								$forwarderTransfer.= $searchResults['szInvoice'];
							}
								$forwarderTransfer.= "</td><td></td><td></td>
								<td style=\"text-align: right;\"> (".$searchResults['szCurrency'] ." ".number_format(abs((float)$searchResults['fTotalPriceForwarderCurrency']),2).")</td>
								<td style=\"text-align: right;\">".$curr_sum."</td>
							</tr>";
								
							//echo "<br> adding ".$sumAry[$idCurency]." and ".$searchResults['fTotalPriceForwarderCurrency'] ;
							
							$currentAvailableAmountAry[$idCurency]['fTotalBalance'] = $sumAry[$idCurency];
							$forwarderTransferDetails[] = $forwarderTransfer;
							$forwarderTransfer='';
						}
						else if(($searchResults['iDebitCredit']==6 || $searchResults['iDebitCredit']==7)) // Cancelled booking
						{ 
							$idCurency = $searchResults['szForwarderCurrency'] ;		
							//$sumAry[$idCurency]= ;
							$flag = 'CREDIT_NOTE';
							
							$sumAry[$idCurency] = (float)($currentAvailableAmountAry[$idCurency]['fTotalBalance'] - $searchResults['fTotalPriceForwarderCurrency']);
							
							$curr_sum='';
							if($sumAry[$idCurency]>0)
							{
								$curr_sum=$searchResults["szForwarderCurrency"]." ".number_format((float)$sumAry[$idCurency],2);
							}
							else
							{
								$newAmount=str_replace("-","",$sumAry[$idCurency]);
								$curr_sum="(".$searchResults["szForwarderCurrency"]." ".number_format((float)$newAmount,2).")";
							}
							
							$forwarderTransfer.="			
							<tr id='booking_data_".$ctr."' onclick='select_forwarder_billings_tr(\"booking_data_".$ctr++."\",\"".$searchResults['idBooking']."\",\"".$flag."\")'><td>".((!empty($searchResults['dtPaymentConfirmed']) && $searchResults['dtPaymentConfirmed']!='0000-00-00 00:00:00')?DATE('d-M',strtotime($searchResults['dtPaymentConfirmed'])):'')."</td>";
							$forwarderTransfer.= "<td>".$searchResults['szBooking']."</td><td></td>";
							
							$forwarderTransfer.= "<td>";
							$forwarderTransfer.= $searchResults['szInvoice'];
							
								$forwarderTransfer.= "</td><td></td><td></td>
								<td style=\"text-align: right;\"> (".$searchResults['szForwarderCurrency'] ." ".number_format(abs((float)$searchResults['fTotalPriceForwarderCurrency']),2).")</td>
								<td style=\"text-align: right;\">".$curr_sum."</td>
							</tr>";
								
							//echo "<br> adding ".$sumAry[$idCurency]." and ".$searchResults['fTotalPriceForwarderCurrency'] ;
							
							$currentAvailableAmountAry[$idCurency]['fTotalBalance'] = $sumAry[$idCurency];
							$forwarderTransferDetails[] = $forwarderTransfer;
							$forwarderTransfer='';

						}
						else if($searchResults['iDebitCredit']=='5')
						{
							$idCurency = $searchResults['szForwarderCurrency'] ;		
							//$sumAry[$idCurency]= ;
							//print_r($searchResults);
							//echo $searchResults['szForwarderCurrency']."----".$idCurency."<br/><br/>";
							//echo $currentAvailableAmountAry[$idCurency]['fTotalBalance']."<br/>";
							if($idCurency==$searchResults['szForwarderCurrency'])
							{
								//echo "ddd1";
								$sumAry[$idCurency] = (float)($currentAvailableAmountAry[$idCurency]['fTotalBalance'] - $searchResults['fTotalPriceForwarderCurrency']);
							}
							else
							{
								//echo "ddd";
								$sumAry[$idCurency] = (float)$currentAvailableAmountAry[$idCurency]['fTotalBalance'];
								//echo $sumAry[$idCurency]."<br/>";
							}
							$curr_sum='';
							if($sumAry[$idCurency]>0)
							{
								$curr_sum=$searchResults['szForwarderCurrency']." ".number_format((float)$sumAry[$idCurency],2);
							}
							else
							{
								$newAmount=str_replace("-","",$sumAry[$idCurency]);
								$curr_sum="(".$searchResults['szForwarderCurrency']." ".number_format((float)$newAmount,2).")";
							}
							$flag="uploadService";
							$forwarderTransfer.="			
							<tr id='booking_data_".$ctr."' onclick='select_forwarder_billings_referral_tr(\"booking_data_".$ctr++."\",\"".$searchResults['szInvoice']."\",\"".$idForwarder."\",\"".$flag."\")'><td>".((!empty($searchResults['dtPaymentConfirmed']) && $searchResults['dtPaymentConfirmed']!='0000-00-00 00:00:00')?DATE('d-M',strtotime($searchResults['dtPaymentConfirmed'])):'')."</td>";
							
							$forwarderTransfer.= "<td>".$searchResults['szBooking']."</td><td></td>";
							 
							$forwarderTransfer.= "<td>";
								$forwarderTransfer.= $searchResults['szInvoice'];
							
								$forwarderTransfer.= "</td><td></td><td></td>
								<td style=\"text-align: right;\"> (".$searchResults['szForwarderCurrency'] ." ".number_format(abs((float)$searchResults['fTotalPriceForwarderCurrency']),2).")</td>
								<td style=\"text-align: right;\">".$curr_sum."</td>
							</tr>";
								
							//echo "<br> adding ".$sumAry[$idCurency]." and ".$searchResults['fTotalPriceForwarderCurrency'] ;
							
							$currentAvailableAmountAry[$idCurency]['fTotalBalance'] = $sumAry[$idCurency];
							$forwarderTransferDetails[] = $forwarderTransfer;
							$forwarderTransfer='';
						}
						else if($searchResults['iDebitCredit']=='4')
						{
							$dateOfjoining = strtotime($searchResults["dtCreatedOn"]);
				
							if((!empty($invoicePendingByAdmin) || !empty($invoiceBillConfirmed)) && ($dateFormSearchResult<=$dateOfjoining ))
							{	
								$date=DATE('d-M',$dateOfjoining);
							
							$forwarderTransfer="<tr>
								<td>".$date."</td><td>".$searchResults['szBooking']."</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td style='text-align: right;'>".$searchResults["szForwarderCurrency"]." 0.00</td>
							</tr>";
							}	
							$forwarderTransferDetails[]=$forwarderTransfer;
							$forwarderTransfer='';
						}
						
						}
										
				}
				$tableForwarderAdmin=array_reverse($forwarderTransferDetails);
				//$tableForwarderAdmin=$forwarderTransferDetails;
				if($tableForwarderAdmin!=array())
				{
					foreach($tableForwarderAdmin as $key=>$value)
					{
						echo $value;
					}
				}
				
			}
			
			if(empty($invoicePendingByAdmin) && empty($invoiceBillConfirmed) || count($invoiceBillConfirmed)<=1)
			{	
				$style="style='opacity:0.4'";
				?>
				<tr>
					<td colspan="9" align="center" style="padding: 5px 0 5px 0;"><?=t($t_base.'messages/no_transactions_in_the_selected_date_range');?></td>
				</tr>
				<?
			}
			else
			{
				$onClickFunction="forwarderBillingSearchFormSubmit()";
			}
		?>
	</table>
	<div id="invoice_comment_div" style="display:none">
	</div>
	<br/>

	<div id="viewInvoiceComplete" style="float: right;">
	<a class="button1" id="download_invoice" style="opacity:0.4;"><span><?=t($t_base.'fields/view_invoice');?></span></a>  
	<a class="button1" id="download_booking"  style="opacity:0.4;"><span><?=t($t_base.'fields/view_booking');?></span></a>
	<a href="javascript:void(0)"  id="download_table" onclick= "<?echo $onClickFunction;?>" <?=$style; ?> class="button1"><span><?=t($t_base.'fields/download_table');?></span></a>
	</div>	
	
	</div>

	<div>
	<h4><strong><?=t($t_base.'title/invoice_pending_settlements');?></strong></h4>
	<h4><?=t($t_base.'title/overview');?></h4>
	
	<div style="padding: 10px 0 10px 0;">
	<table cellpadding="0" cellspacing="1" style="width: 100%" class="format-4" id="booking_table_pending">
		<tr>
			<th width="8%" valign="top"><?=t($t_base.'fields/invoice')?> <?=t($t_base.'fields/date')?></th>
			<th width="23%" valign="top"><?=t($t_base.'fields/description')?></th>
			<th width="13%" valign="top"><?=t($t_base.'fields/your');?> <?=t($t_base.'fields/reference');?></th>	
			<th width="10%" valign="top"><?=t($t_base.'fields/invoice')?> <?=t($t_base.'fields/number')?></th>				
			<th width="14%" valign="top" style="text-align: right;"><?=t($t_base.'fields/invoice')?> <?=t($t_base.'fields/value')?></th>
			<th width="7%" valign="top" style="text-align: right;"><?=t($t_base.'fields/roe')?>*</th>
			<th width="12%" valign="top" style="text-align: right;"><?=t($t_base.'fields/pending_credit')?> **</th>
			<th width="13%" valign="top" style="text-align: right;"><?=t($t_base.'fields/expected_date')?></th>
			</tr>
			<?	
			$ctr=1;
			if($invoiceBillPending)
			{ 
				foreach($invoiceBillPending as $searchResults)
				{	
					?>						
					<tr id="booking_data_pend_<?=$ctr?>" onclick="select_forwarder_billings_pend_tr('booking_data_pend_<?=$ctr++?>','<?=$searchResults['idBooking']?>')">
						<td><?=(!empty($searchResults['dtInvoiceOn']) && $searchResults['dtInvoiceOn']!='0000-00-00 00:00:00')?DATE('d-M',strtotime($searchResults['dtInvoiceOn'])):''?></td>
						<td><?=$searchResults['szBooking']?></td>
						<td><?=$searchResults['szBookingNotes']?></td>
						<td ><?=$searchResults['szInvoice']?></td>
						<td style="text-align: right;"><?=$searchResults['szCurrency']." ".number_format($searchResults['fTotalAmount'],2)?></td>
						<td style="text-align: right;"><?=number_format((float)$searchResults['fExchangeRate'],4)?></td>
						<td style="text-align: right;"><?=$searchResults['szForwarderCurrency']." ".number_format($searchResults['fTotalPriceForwarderCurrency'],2)?></td>
						<? $expectedDate=DATE('d-M',strtotime( '+7 days' , strtotime($searchResults['dtInvoiceOn'])));?>
						<td style="text-align: right;"><?=$expectedDate;?></td>
					</tr>
					<?
					//$ctr++;
				}
			}
			else
			{
				echo "<tr><td colspan='8' align='center' style='padding:5px 5px 5px 5px;'>".t($t_base.'messages/no_invoices_pending_settlement')."</td></tr>";
			}
		?>
		</table>
		<br/>
		<div id="viewInvoiceComplete" style="float: right;">
			<a class="button1" id="download_invoice_pend" style="opacity:0.4;"><span><?=t($t_base.'fields/view_invoice');?></span></a>  
			<a class="button1" id="download_booking_pend"  style="opacity:0.4;"><span style="min-width:108px"><?=t($t_base.'fields/view_booking');?></span></a>
		</div>			
		<div style="font-size:12px;">* <?=t($t_base.'title/dob');?></div>
		<div style="font-size:12px;">** <?=t($t_base.'title/typically_allow');?></div>
		<br/>
	</div>
	<?
}
function display_no_service_available_popup($t_base,$landing_page_url,$idBooking,$idExpactedService,$kBooking,$postSearchAry)
{
	if($postSearchAry['iFromRequirementPage']==2)
	{
		$landing_page_url = __SELECT_SIMPLE_SEARCH_URL__ ;
	}
	?>	
	<script type="text/javascript">
		$().ready( function(){
			default_cursor('<?=$_SESSION['user_id']?>');			
		});
	</script>
	<div id="popup-bg" class="white-bg"></div>
	<form id="no_services_form" method="post" name="no_services_form" action="">
		<div id="popup-container">
		<div class="standard-popup-x popup" >
		<p class="close-icon" align="right"><a heref="javascript:void(0);" onclick="closePopup('all_available_service');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
		<h5><?=t($t_base.'title/no_service_popup_header_new')?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'fields/to');?> <?=$postSearchAry['szDestinationCountry']?>, <?=t($t_base.'fields/but');?>...</h5>
		
	<?
	$notify_me_class = "class='gray-button1'";
	if($_SESSION['user_id']<=0)
	{
		?>
		<p class="heading-space" style="margin-bottom:15px;"><?=t($t_base.'title/no_service_popup_text_new');?> </p>
		<div id="popupRegError" class="errorBox1-requirement" style="display:none;margin-bottom:15px;"></div>
		
		<div class="oh name-space">
			<p class="fl-20"><?=t($t_base.'fields/name');?></p>
			<p class="fl-80">
				<input type="text" onkeyup="enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event);" id="szCustomerName_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_NAME_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>');"  name="addExpactedServiceAry[szCustomerName]" value="<?=$postSearchResult['szCustomerName']?>">
				<span id="szCustomerName_popup_error" class="red_text" style="display:none;float:right;"></span>
			</p>
		</div>
		<?
		$submit_button_text = t($t_base.'fields/submit');
	}
	else
	{
		$email_loggedin_class="email-space";
		$submit_button_text = t($t_base.'fields/yes_please');
		
		$kUser = new cUser();
		$kUser->getUserDetails($_SESSION['user_id']);
		if(empty($postSearchResult['szEmail']))
		{
			$postSearchResult['szCustomerPhone'] = $kUser->szPhoneNumber ;
			$postSearchResult['szEmail'] = $kUser->szEmail ;
			if(!empty($postSearchResult['szEmail']))
			{
				$notify_me_class = "class='orange-button2'";
				$notify_me_onclick = "onclick=\"update_expacted_services_requirement('".$landing_page_url."')\"";
			}
		}
		?>
		<p style="margin-bottom:15px;"><?=t($t_base.'title/no_service_popup_text_new');?> </p>
		<div id="popupRegError" class="errorBox1-requirement" style="display:none;margin-bottom:15px;"></div>
		
		<?
	}
	?>
	<div class="oh <?=$email_loggedin_class;?>">
		<p class="fl-20"><?=t($t_base.'fields/email');?></p>
		<p class="fl-80">
			<input type="text" id="szCustomerEmail_popup" onkeyup="enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event);" onblur="Valid_Form_Feild_Check(this.value,'VALID_EMAIL_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>');"  name="addExpactedServiceAry[szEmail]" value="<?=$postSearchResult['szEmail']?>">
			<span id="szCustomerEmail_popup_error" class="red_text" style="display:none;float:right;"></span>
		</p>
	</div>
	<?php
	$iLoggedInUser=1;
	?>
	<div class="oh" style="margin-top:-5px;">
		<p class="fl-20"><?=t($t_base.'fields/phone');?></p>
		<p class="fl-80">
			<input type="text" id="szCustomerPhone_popup" name="addExpactedServiceAry[szCustomerPhone]" value="<?=$postSearchResult['szCustomerPhone']?>">
		</p>
	</div>
	<p align="center" style="padding-left:12px;">
		<input type="hidden"  name="addExpactedServiceAry[iSendMailToAdmin]" id="iSendMailToAdmin" value="sendEmail">
		<input type="hidden" name="addExpactedServiceAry[iLoggedInUser]" id="iLoggedInUser" value="<?=$iLoggedInUser?>">
		<input type="hidden" name="addExpactedServiceAry[idBooking]" value="<?=$idBooking?>">
		<input type="hidden" name="addExpactedServiceAry[idNotificationType]" value="<?=__NOTIFICATION_TYPE_NO_SERVICE_COUNTRY_TO_COUNTRY___?>">
		<input type="hidden" name="addExpactedServiceAry[idExpactedService]" value="<?=$idExpactedService?>">
		<a href="javascript:void(0);" <?=$notify_me_class?> <?=$notify_me_onclick?> id="notify_me_button"><span><?=$submit_button_text?></span></a>
		&nbsp;&nbsp;&nbsp;<span id="success_message_expacted_services" style="color:green;font-weight:bolder;font-size:16px;"></span>
	</p>	
</div>		
</div>
</form>
	<?
}
function display_no_service_available_popup_requirement($t_base,$landing_page_url,$idBooking,$idExpactedService,$kBooking,$postSearchAry)
{
	$szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
	if($postSearchAry['iFromRequirementPage']==2)
	{
		$landing_page_url = __SELECT_SIMPLE_SEARCH_URL__ ;
	}
	?>	
	<script type="text/javascript">
		$().ready( function(){
			default_cursor('<?=$_SESSION['user_id']?>');		
			addPopupScrollClass('all_available_service');	
		});
	</script>
	
	<div id="popup-bg" class="white-bg"></div>
	<form id="no_services_form" method="post" name="no_services_form" action="">
		<div id="popup-container">
		<div class="standard-popup-x popup" >
		<p class="close-icon" align="right"><a heref="javascript:void(0);" onclick="closePopup('all_available_service');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
		<h5><?=t($t_base.'title/rp1_popup_title_1')?></h5>
		
	<?
	$notify_me_class = "class='gray-button1'";
	if($_SESSION['user_id']<=0)
	{
		?>
		<p><?=t($t_base.'title/no_service_popup_text_new');?> <!--<?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/to');?> <?=$postSearchAry['szDestinationCountry']?>. <? //t($t_base.'title/rp1_details_text_2');?>--></p>
		<!--  <p><?=t($t_base.'title/rp1_details_text_3');?></p>-->
		<div id="popupRegError" class="errorBox1-requirement" style="display:none;margin-bottom:15px;"></div>
		
		<div class="oh name-space">
			<p class="fl-20"><?=t($t_base.'fields/name');?></p>
			<p class="fl-80">
				<input type="text" id="szCustomerName_popup" onkeyup="enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event,'','<?=$szBookingRandomNum?>');" onblur="Valid_Form_Feild_Check(this.value,'VALID_NAME_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>','REQUIREMENT_PAGE');"  name="addExpactedServiceAry[szCustomerName]" value="<?=$postSearchResult['szCustomerName']?>">
				<span id="szCustomerName_popup_error" class="red_text" style="display:none;float:right;"></span>
			</p>
		</div>
		<?
		$submit_button_text = t($t_base.'fields/submit');
	}
	else
	{
		$email_loggedin_class="email-space";		
		$submit_button_text = t($t_base.'fields/yes_please');
		
		$kUser = new cUser();
		$kUser->getUserDetails($_SESSION['user_id']);
		if(empty($postSearchResult['szEmail']))
		{
			$postSearchResult['szCustomerPhone'] = $kUser->szPhoneNumber ;
			$postSearchResult['szEmail'] = $kUser->szEmail ;
			if(!empty($postSearchResult['szEmail']))
			{
				$notify_me_class = "class='orange-button2'";
				$notify_me_onclick = "onclick=\"update_expacted_services_requirement('".$landing_page_url."')\"";
			}
		}
		?>
		<p><?=t($t_base.'title/no_service_popup_text_new');?> <!--<?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/to');?> <?=$postSearchAry['szDestinationCountry']?>.--></p>
		<!--  <p><?=t($t_base.'title/rp1_details_text_4');?></p>-->
		<div id="popupRegError" class="errorBox1-requirement" style="display:none;margin-bottom:15px;"></div>
		
		<?
	}
//	onclick="update_expacted_services('REQUIREMENT','<?=$requirement_page_url','szBookingRandomNum');"
	?>
	<div class="oh <?=$email_loggedin_class;?>">
		<p class="fl-20"><?=t($t_base.'fields/email');?></p>
		<p class="fl-80">
			<input type="text" id="szCustomerEmail_popup" onkeyup="enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event,'','<?=$szBookingRandomNum?>');" onblur="Valid_Form_Feild_Check(this.value,'VALID_EMAIL_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>','REQUIREMENT_PAGE');"  name="addExpactedServiceAry[szEmail]" value="<?=$postSearchResult['szEmail']?>">
			<span id="szCustomerEmail_popup_error" class="red_text" style="display:none;float:right;"></span>
		</p>
	</div>		
	<div class="oh" style="margin-top:-5px;">
		<p class="fl-20"><?=t($t_base.'fields/phone');?></p>
		<p class="fl-80">
			<input type="text" id="szCustomerPhone_popup" name="addExpactedServiceAry[szCustomerPhone]" value="<?=$postSearchResult['szCustomerPhone']?>">
		</p>
	</div>
	<input type="hidden" name="addExpactedServiceAry[idBooking]" value="<?=$idBooking?>">
	<input type="hidden" name="addExpactedServiceAry[idExpactedService]" value="<?=$idExpactedService?>">
	<input type="hidden" name="addExpactedServiceAry[idNotificationType]" value="<?=__NOTIFICATION_TYPE_RP1_RP2___?>">
	<input type="hidden" name="addExpactedServiceAry[landingUrl]" id="landingUrl" value="<?=$landing_page_url?>">
	<p align="center" class="button-container">	
		<a href="javascript:void(0);" <?=$notify_me_class?> <?=$notify_me_onclick?>  id="notify_me_button"><span><?=$submit_button_text;?></span></a>
	</p>
	<p align="center" class="button-container">	
		<a href="javascript:void(0);" class="orange-button2" onclick="display_requirement_edit_box('SERVICE_TYPE_EDIT','<?=$szBookingRandomNum?>')"><span><?=t($t_base.'fields/go_back_change_requirement');?></span></a>
	</p>
	<p align="center" class="button-container">	
		<a href="javascript:void(0);" class="orange-button2" onclick="update_expacted_services('clear','<?=__HOME_PAGE_URL__?>','<?=$szBookingRandomNum?>');"><span><?=t($t_base.'fields/make_new_search');?></span></a>
	</p>	
</div>		
</div>
</form>
	<?
}

function display_F3_F4_popup($postSearchAry,$idService,$idExpactedService)
{
	$t_base = "LandingPage/";
	$idBooking = $postSearchAry['id'];
	$landing_page_url = __LANDING_PAGE_URL__ ;
	$szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
	if($postSearchAry['iFromRequirementPage']==2)
	{
		$landing_page_url = __SELECT_SIMPLE_SEARCH_URL__ ;
	}
	?>	
	<script type="text/javascript">
		$().ready( function(){
			default_cursor('<?=$_SESSION['user_id']?>');
		});
	</script>
	<div id="popup-bg" class="white-bg"></div>
	<form id="no_services_form" method="post" name="no_services_form" action="">
		<div id="popup-container">
		<div class="standard-popup-f popup" >
		<p class="close-icon" align="right"><a heref="javascript:void(0);" onclick="closePopup('all_available_service');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
		<h5><?=t($t_base.'title/f4_popup_title_1')?> <?=$postSearchAry['szOriginCountry']?><?=t($t_base.'title/f4_popup_title_2')?></h5>
		
<?
	$notify_me_class = "class='gray-button1'";
	if($_SESSION['user_id']<=0)
	{
		$submit_button_text = t($t_base.'fields/submit');
		?>
		<!--<h3>F3</h3>-->
		<p style="margin-bottom:15px;" class="heading-space"><?=t($t_base.'title/no_service_popup_text_new');?> <!--<?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/f4_details_text_2');?>--> </p>
			<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>
	
		<div class="oh name-space">
			<p class="fl-20"><?=t($t_base.'fields/name');?></p>
			<p class="fl-80">
				<input type="text" id="szCustomerName_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_NAME_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>');" onkeyup="enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event);" name="addExpactedServiceAry[szCustomerName]" value="<?=$postSearchResult['szCustomerName']?>">
				<span id="szCustomerName_popup_error" class="red_text" style="display:none;float:right;"></span>
			</p>
		</div>
		<?
	}
	else
	{
		$submit_button_text = t($t_base.'fields/yes_please');
		$email_loggedin_class="email-space";
		$kUser = new cUser();
		$kUser->getUserDetails($_SESSION['user_id']);
		if(empty($postSearchResult['szEmail']))
		{
			$postSearchResult['szCustomerPhone'] = $kUser->szPhoneNumber ;
			$postSearchResult['szEmail'] = $kUser->szEmail ;
			if(!empty($postSearchResult['szEmail']))
			{
				$notify_me_class = "class='orange-button2'";
				$notify_me_onclick = "onclick=\"update_expacted_services_requirement('".$landing_page_url."')\"";
			}
		}
		?>
		<!--<h3>F4</h3>-->
		<p style="margin-bottom:15px;" class="heading-space"><?=t($t_base.'title/no_service_popup_text_new');?> <!--<?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/f4_details_text_2');?>--> </p>
	
		<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>
		<?
	}
	?>
	<div class="oh <?=$email_loggedin_class;?>">
		<p class="fl-20"><?=t($t_base.'fields/email');?></p>
		<p class="fl-80">
			<input type="text" id="szCustomerEmail_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_EMAIL_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>');" onkeyup="enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event);" name="addExpactedServiceAry[szEmail]" value="<?=$postSearchResult['szEmail']?>">
			<span id="szCustomerEmail_popup_error" class="red_text" style="display:none;float:right;"></span>
		</p>
	</div>
	<div class="oh" style="margin-top:-5px;">
		<p class="fl-20"><?=t($t_base.'fields/phone');?></p>
		<p class="fl-80">
			<input type="text" id="szCustomerPhone_popup" name="addExpactedServiceAry[szCustomerPhone]" value="<?=$postSearchResult['szCustomerPhone']?>">
		</p>
	</div>
	<?
		if(!empty($postSearchAry['szOriginCountry']) && strlen($postSearchAry['szOriginCountry'])>__MAX_COUNTRY_NAME_LENGTH_ON_BUTTONS___)
		{
			$newLength = __MAX_COUNTRY_NAME_LENGTH_ON_BUTTONS___ - 2 ;
			$originCountryName = mb_substr($postSearchAry['szOriginCountry'],0,$newLength,'UTF8')."..";
		}
		else
		{
			$originCountryName = $postSearchAry['szOriginCountry'];
		}	
	?>
	<p align="center" class="button-container">		
		<a href="javascript:void(0);" <?=$notify_me_class?> <?=$notify_me_onclick?>  id="notify_me_button"><span><?=$submit_button_text;?></span></a>
	</p>
	<p align="center" class="button-container">
		<input type="hidden" name="addExpactedServiceAry[idBooking]" value="<?=$idBooking?>">
		<input type="hidden" name="addExpactedServiceAry[idNotificationType]" value="<?=__NOTIFICATION_TYPE_NO_PICKUP_FROM_ORIGIN_COUNTRY___?>">
		<input type="hidden" name="addExpactedServiceAry[idExpactedService]" value="<?=$idExpactedService?>">
		<input type="hidden" name="addExpactedServiceAry[idServiceType]" id="idServiceType" value="<?=$idService?>">
		<a href="javascript:void(0);" class="orange-button1" onclick="find_transportation('OPTION_K1A','<?=$szBookingRandomNum?>');"><span><?=t($t_base.'fields/continue_button');?> <?=$originCountryName?></span></a>
	</p>
	
</div>		
</div>
</form>
	<?
}

function display_F11_F12_popup($postSearchAry,$idService,$idExpactedService)
{
	$t_base = "LandingPage/";
	$idBooking = $postSearchAry['id'];
	$landing_page_url = __LANDING_PAGE_URL__ ;
	$szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
	if($postSearchAry['iFromRequirementPage']==2)
	{
		$landing_page_url = __SELECT_SIMPLE_SEARCH_URL__ ;
	}
	?>	
	<script type="text/javascript">
		$().ready( function(){
			default_cursor('<?=$_SESSION['user_id']?>');
			
		});
	</script>
	<div id="popup-bg" class="white-bg"></div>
	<form id="no_services_form" method="post" name="no_services_form" action="">
		<div id="popup-container">
		<div class="standard-popup-f popup" >
		<p  class="close-icon" align="right"><a heref="javascript:void(0);" onclick="closePopup('all_available_service');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
		<h5><?=t($t_base.'title/f11_popup_title_1')?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/f11_popup_title_2')?> <?=$postSearchAry['szDestinationCountry']?><?=t($t_base.'title/f11_popup_title_3')?></h5>
		
<?php
	$notify_me_class = "class='gray-button1'";
	if($_SESSION['user_id']<=0)
	{
		$submit_button_text = t($t_base.'fields/submit');
		?>
		<!-- <h3>F11</h3> -->
		<p class="heading-space" style="margin-bottom:15px;"><?=t($t_base.'title/no_service_popup_text_new');?> <!--<?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/f11_details_text_2');?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/f11_details_text_3')?>--></p>
		<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>

		<div class="oh name-space">
			<p class="fl-20"><?=t($t_base.'fields/name');?></p>
			<p class="fl-80">
				<input type="text" id="szCustomerName_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_NAME_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>');" onkeyup = "enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event);" name="addExpactedServiceAry[szCustomerName]" value="<?=$postSearchResult['szCustomerName']?>">
				<span id="szCustomerName_popup_error" class="red_text" style="display:none;float:right;"></span>
			</p>
		</div>
		<?
	}
	else
	{
		$submit_button_text = t($t_base.'fields/yes_please');
		$email_loggedin_class="email-space";
		$kUser = new cUser();
		$kUser->getUserDetails($_SESSION['user_id']);
		if(empty($postSearchResult['szEmail']))
		{
			$postSearchResult['szCustomerPhone'] = $kUser->szPhoneNumber ;
			$postSearchResult['szEmail'] = $kUser->szEmail ;
			if(!empty($postSearchResult['szEmail']))
			{
				$notify_me_class = "class='orange-button1'";
				$notify_me_onclick = "onclick=\"update_expacted_services_requirement('".$landing_page_url."')\"";
			}
		}
		?>
		<!--<h3>F12</h3>-->
		<p class="heading-space" style="margin-bottom:15px;"><?=t($t_base.'title/no_service_popup_text_new');?> <!--<?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/f11_details_text_2');?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/f11_details_text_3')?>--></p>
		<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>

		<?
	}
	?>
	<div class="oh <?=$email_loggedin_class;?>">
		<p class="fl-20"><?=t($t_base.'fields/email');?></p>
		<p class="fl-80">
			<input type="text" id="szCustomerEmail_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_EMAIL_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>');" onkeyup="enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event);" name="addExpactedServiceAry[szEmail]" value="<?=$postSearchResult['szEmail']?>">
			<span id="szCustomerEmail_popup_error" class="red_text" style="display:none;float:right;"></span>
		</p>
	</div>
	<div class="oh" style="margin-top:-5px;">
		<p class="fl-20"><?=t($t_base.'fields/phone');?></p>
		<p class="fl-80">
			<input type="text" id="szCustomerPhone_popup" name="addExpactedServiceAry[szCustomerPhone]" value="<?=$postSearchResult['szCustomerPhone']?>">
		</p>
	</div>
	<?
		if(!empty($postSearchAry['szOriginCountry']) && strlen($postSearchAry['szOriginCountry'])>__MAX_COUNTRY_NAME_LENGTH_ON_BUTTONS___)
		{
			$newLength = __MAX_COUNTRY_NAME_LENGTH_ON_BUTTONS___ - 2 ;
			$originCountryName = mb_substr($postSearchAry['szOriginCountry'],0,$newLength,'UTF8')."..";
		}
		else
		{
			$originCountryName = $postSearchAry['szOriginCountry'];
		}
		
		if(!empty($postSearchAry['szDestinationCountry']) && strlen($postSearchAry['szDestinationCountry'])>__MAX_COUNTRY_NAME_LENGTH_ON_BUTTONS___)
		{
			$newLength = __MAX_COUNTRY_NAME_LENGTH_ON_BUTTONS___ - 2 ;
			$szDestinationCountryName = mb_substr($postSearchAry['szDestinationCountry'],0,$newLength,'UTF8')."..";
		}
		else
		{
			$szDestinationCountryName = $postSearchAry['szDestinationCountry'];
		}
	?>
	<p align="center" class="button-container">	
		<a href="javascript:void(0);" <?=$notify_me_class?> <?=$notify_me_onclick?>  id="notify_me_button"><span><?=$submit_button_text;?></span></a>
	</p>
	<p align="center" class="button-container">
		<input type="hidden" name="addExpactedServiceAry[idBooking]" value="<?=$idBooking?>">
		<input type="hidden" name="addExpactedServiceAry[idExpactedService]" value="<?=$idExpactedService?>">
		<input type="hidden" name="addExpactedServiceAry[idNotificationType]" value="<?=__NOTIFICATION_TYPE_NO_PICKUP_AND_NO_DELIVERY___?>">
		<input type="hidden" name="addExpactedServiceAry[idServiceType]" id="idServiceType" value="<?=$idService?>">
		<a href="javascript:void(0);" class="orange-button1" onclick="find_transportation('OPTION_N1B','<?=$szBookingRandomNum?>');"><span><?=t($t_base.'fields/continue_button');?> <?=$originCountryName?></span></a>
	</p>
	<p align="center" class="button-container">	
		<a href="javascript:void(0);" class="orange-button1" onclick="find_transportation('OPTION_N1A','<?=$szBookingRandomNum?>');"><span><?=t($t_base.'fields/continue_button_delivery');?> <?=$szDestinationCountryName?></span></a>
	</p>
	

</div>		
</div>
</form>
	<?
}
function display_F1_popup($postSearchAry,$idService,$idExpactedService,$mode)
{
	$t_base = "LandingPage/";
	$idBooking = $postSearchAry['id'];
	$landing_page_url = __LANDING_PAGE_URL__ ;
	
	if($mode=='OPTION_YCL1H1B')
	{
		$operation = "OPTION_YCL1H1A";
	}
	else if($mode == 'OPTION_YCH1B')
	{
		$operation = "OPTION_YCH1A";
	}
	else if($mode == 'OPTION_YCG1H1B')
	{
		$operation = "OPTION_YCG1H1A";
	}
	else if($mode == 'OPTION_H1B')
	{
		$operation = "OPTION_H1A";
	}	
	else
	{
		$operation = "OPTION_N1A";
	}
	
	$szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
	
	if($postSearchAry['iFromRequirementPage']==2)
	{
		$landing_page_url = __SELECT_SIMPLE_SEARCH_URL__ ;
	}
?>	
<script type="text/javascript">
	$().ready( function(){
		default_cursor('<?=$_SESSION['user_id']?>');
	});
</script>
	<div id="popup-bg" class="white-bg"></div>
	<form id="no_services_form" method="post" name="no_services_form" action="">
		<div id="popup-container">
		<div class="standard-popup-f popup" >
		<p class="close-icon" align="right"><a heref="javascript:void(0);" onclick="closePopup('all_available_service');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
		<h5><?=t($t_base.'title/f1_header_text_1')?> <?=$postSearchAry['szDestinationCountry']?><?=t($t_base.'title/f1_header_text_2')?></h5>
		
<?php
	$notify_me_class = "class='gray-button1'";
	if($_SESSION['user_id']<=0)
	{
		$submit_button_text = t($t_base.'fields/submit');
		?>
		<!--<h3>F1</h3> -->
		<p class="heading-space" style="margin-bottom:15px;"><?=t($t_base.'title/no_service_popup_text_new');?><!--  <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/f1_details_text_2');?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/f1_details_text_3');?>--></p>
		<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>

		<div class="oh name-space">
			<p class="fl-20"><?=t($t_base.'fields/name');?></p>
			<p class="fl-80">
				<input type="text" id="szCustomerName_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_NAME_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>');" onkeyup="enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event);" name="addExpactedServiceAry[szCustomerName]" value="<?=$postSearchResult['szCustomerName']?>">
				<span id="szCustomerName_popup_error" class="red_text" style="display:none;float:right;"></span>
			</p>
		</div>
		<?
	}
	else
	{
		$submit_button_text = t($t_base.'fields/yes_please');
		$email_loggedin_class="email-space";
		$kUser = new cUser();
		$kUser->getUserDetails($_SESSION['user_id']);
		if(empty($postSearchResult['szEmail']))
		{
			$postSearchResult['szCustomerPhone'] = $kUser->szPhoneNumber ;
			$postSearchResult['szEmail'] = $kUser->szEmail ;
			if(!empty($postSearchResult['szEmail']))
			{
				$notify_me_class = "class='orange-button1'";
				$notify_me_onclick = "onclick=\"update_expacted_services_requirement('".$landing_page_url."')\"";
			}
		}
		?>
		<!--<h3>F2</h3> -->
		<p class="heading-space" style="margin-bottom:15px;"><?=t($t_base.'title/no_service_popup_text_new');?><!-- <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/f2_details_text_2');?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/f1_details_text_3');?>--></p>
		<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>

		<?
	}
	?>
	<div class="oh <?=$email_loggedin_class;?>">
		<p class="fl-20"><?=t($t_base.'fields/email');?></p>
		<p class="fl-80">
			<input type="text" id="szCustomerEmail_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_EMAIL_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>');" onkeyup="enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event);" name="addExpactedServiceAry[szEmail]" value="<?=$postSearchResult['szEmail']?>">
			<span id="szCustomerEmail_popup_error" class="red_text" style="display:none;float:right;"></span>
		</p>
	</div>
	
	<div class="oh" style="margin-top:-5px;">
		<p class="fl-20"><?=t($t_base.'fields/phone');?></p>
		<p class="fl-80">
			<input type="text" id="szCustomerPhone_popup" name="addExpactedServiceAry[szCustomerPhone]" value="<?=$postSearchResult['szCustomerPhone']?>">
		</p>
	</div>

	<?	
		if(!empty($postSearchAry['szDestinationCountry']) && strlen($postSearchAry['szDestinationCountry'])>__MAX_COUNTRY_NAME_LENGTH_ON_BUTTONS___)
		{
			$newLength = __MAX_COUNTRY_NAME_LENGTH_ON_BUTTONS___ - 2 ;
			$szDestinationCountryName = mb_substr($postSearchAry['szDestinationCountry'],0,$newLength,'UTF8')."..";
		}
		else
		{
			$szDestinationCountryName = $postSearchAry['szDestinationCountry'];
		}
	?>
	<p align="center" class="button-container">	
		<a href="javascript:void(0);" <?=$notify_me_class?> <?=$notify_me_onclick?>  id="notify_me_button"><span><?=$submit_button_text;?></span></a>
	</p>
	<p align="center" class="button-container">
		<input type="hidden" name="addExpactedServiceAry[idBooking]" value="<?=$idBooking?>">
		<input type="hidden" name="addExpactedServiceAry[idExpactedService]" value="<?=$idExpactedService?>">
		<input type="hidden" name="addExpactedServiceAry[idNotificationType]" value="<?=__NOTIFICATION_TYPE_NO_DELIVERY_TO_DESTINATION_COUNTRY___?>">
		<input type="hidden" name="addExpactedServiceAry[idServiceType]" id="idServiceType" value="<?=$idService?>">
		<a href="javascript:void(0);" class="orange-button1" onclick="find_transportation('<?=$operation?>','<?=$szBookingRandomNum?>');"><span><?=t($t_base.'fields/continue_button_delivery');?> <?=$szDestinationCountryName?></span></a>
	</p>
	

</div>		
</div>
</form>
	<?
}
function display_F7_popup($postSearchAry,$idService,$idExpactedService,$mode)
{
	$t_base = "LandingPage/";
	$idBooking = $postSearchAry['id'];
	$landing_page_url = __LANDING_PAGE_URL__ ;
	$szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
	if($postSearchAry['iFromRequirementPage']==2)
	{
		$landing_page_url = __SELECT_SIMPLE_SEARCH_URL__ ;
	}
	
	if($mode == 'OPTION_B1B')
	{
		$szOperation = 'OPTION_B1A';
	}
	else
	{
		$szOperation = 'OPTION_MA1B';
	}	
?>	
	<script type="text/javascript">
		$().ready( function(){
			default_cursor('<?=$_SESSION['user_id']?>');
		});
	</script>
	<div id="popup-bg" class="white-bg"></div>
	<form id="no_services_form" method="post" name="no_services_form" action="">
		<div id="popup-container">
		<div class="standard-popup-f popup" >
		<p class="close-icon" align="right"><a heref="javascript:void(0);" onclick="closePopup('all_available_service');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
		<h5><?=t($t_base.'title/f7_popup_title_1')?> <?=$postSearchAry['szDestinationCountry']?><?=t($t_base.'title/f7_popup_title_2')?></h5>
		
<?php
	$notify_me_class = "class='gray-button1'";
	if($_SESSION['user_id']<=0)
	{
		$submit_button_text = t($t_base.'fields/submit');
		?>
		<!--<h3>F7</h3>-->
		<p class="heading-space" style="margin-bottom:15px;"><?=t($t_base.'title/no_service_popup_text_new');?><!--  <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/f7_details_text_2');?> --></p>
		<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>

		<div class="oh name-space">
			<p class="fl-20"><?=t($t_base.'fields/name');?></p>
			<p class="fl-80">
				<input type="text" id="szCustomerName_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_NAME_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>');" onkeyup = "enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event);" name="addExpactedServiceAry[szCustomerName]" value="<?=$postSearchResult['szCustomerName']?>">
				<span id="szCustomerName_popup_error" class="red_text" style="display:none;float:right;"></span>
			</p>
		</div>
		<?
	}
	else
	{
		$submit_button_text = t($t_base.'fields/yes_please');
		$email_loggedin_class="email-space";
		$kUser = new cUser();
		$kUser->getUserDetails($_SESSION['user_id']);
		if(empty($postSearchResult['szEmail']))
		{
			$postSearchResult['szCustomerPhone'] = $kUser->szPhoneNumber ;
			$postSearchResult['szEmail'] = $kUser->szEmail ;
			if(!empty($postSearchResult['szEmail']))
			{
				$notify_me_class = "class='orange-button1'";
				$notify_me_onclick = "onclick=\"update_expacted_services_requirement('".$landing_page_url."')\"";
			}
		}
		?>
		<!--<h3>F8</h3>-->
		<p class="heading-space" style="margin-bottom:15px;"><?=t($t_base.'title/no_service_popup_text_new');?><!--  <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/f7_details_text_2');?>--></p>
		<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>
		<?
	}
?>
	<div class="oh <?=$email_loggedin_class;?>">
		<p class="fl-20"><?=t($t_base.'fields/email');?></p>
		<p class="fl-80">
			<input type="text" id="szCustomerEmail_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_EMAIL_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>');" onkeyup="enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event);" name="addExpactedServiceAry[szEmail]" value="<?=$postSearchResult['szEmail']?>">
			<span id="szCustomerEmail_popup_error" class="red_text" style="display:none;float:right;"></span>
		</p>
	</div>
	<div class="oh" style="margin-top:-5px;">
		<p class="fl-20"><?=t($t_base.'fields/phone');?></p>
		<p class="fl-80">
			<input type="text" id="szCustomerPhone_popup" name="addExpactedServiceAry[szCustomerPhone]" value="<?=$postSearchResult['szCustomerPhone']?>">
		</p>
	</div>
	<input type="hidden" name="addExpactedServiceAry[idBooking]" value="<?=$idBooking?>">
	<input type="hidden" name="addExpactedServiceAry[idExpactedService]" value="<?=$idExpactedService?>">
	<input type="hidden" name="addExpactedServiceAry[idNotificationType]" value="<?=__NOTIFICATION_TYPE_NO_DELIVERY_TO_DESTINATION_COUNTRY___?>">
	<input type="hidden" name="addExpactedServiceAry[idServiceType]" id="idServiceType" value="<?=$idService?>">

	<?
			
		if(!empty($postSearchAry['szDestinationCountry']) && strlen($postSearchAry['szDestinationCountry'])>__MAX_COUNTRY_NAME_LENGTH_ON_BUTTONS___)
		{
			$newLength = __MAX_COUNTRY_NAME_LENGTH_ON_BUTTONS___ - 2 ;
			$szDestinationCountryName = mb_substr($postSearchAry['szDestinationCountry'],0,$newLength,'UTF8')."..";
		}
		else
		{
			$szDestinationCountryName = $postSearchAry['szDestinationCountry'];
		}
	?>
	<p align="center" class="button-container">	
		<a href="javascript:void(0);" <?=$notify_me_class?> <?=$notify_me_onclick?>  id="notify_me_button"><span><?=$submit_button_text;?></span></a>
	</p>
	<p align="center" class="button-container">
		<a href="javascript:void(0);" class="orange-button1" onclick="find_transportation('<?=$szOperation?>','<?=$szBookingRandomNum?>');"><span><?=t($t_base.'fields/continue_button_delivery');?> <?=$szDestinationCountryName?></span></a>
	</p>
	

</div>		
</div>
</form>
	<?
}
function display_F13_F14_popup($postSearchAry,$idService,$idExpactedService)
{
	$t_base = "LandingPage/";
	$idBooking = $postSearchAry['id'];
	$landing_page_url = __LANDING_PAGE_URL__ ;
	$notify_me_class = "class='gray-button1'";
	$szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
if($postSearchAry['iFromRequirementPage']==2)
	{
		$landing_page_url = __SELECT_SIMPLE_SEARCH_URL__ ;
	}
	?>	
	<script type="text/javascript">
		$().ready( function(){
			default_cursor('<?=$_SESSION['user_id']?>');
			
		});
	</script>
	<div id="popup-bg" class="white-bg"></div>
	<form id="no_services_form" method="post" name="no_services_form" action="">
		<div id="popup-container">
		<div class="standard-popup-f popup" >
		<p class="close-icon" align="right"><a heref="javascript:void(0);" onclick="closePopup('all_available_service');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
		<h5><?=t($t_base.'title/f13_popup_title_1')?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/f13_popup_title_2')?> <?=$postSearchAry['szDestinationCountry']?><?=t($t_base.'title/f13_popup_title_3')?></h5>
		
	<?
	if($_SESSION['user_id']<=0)
	{
		$submit_button_text = t($t_base.'fields/submit');
		?>
		<!--<h3>F13</h3>-->
		<p class="heading-space" style="margin-bottom:15px;"><?=t($t_base.'title/no_service_popup_text_new');?> <!--<?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/f13_details_text_2');?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/f13_details_text_3')?>--></p>
		<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>

		<div class="oh name-space">
			<p class="fl-20"><?=t($t_base.'fields/name');?></p>
			<p class="fl-80">
				<input type="text" id="szCustomerName_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_NAME_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>');" onkeyup="enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event);" name="addExpactedServiceAry[szCustomerName]" value="<?=$postSearchResult['szCustomerName']?>">
				<span id="szCustomerName_popup_error" class="red_text" style="display:none;float:right;"></span>
			</p>
		</div>
		<?
	}
	else
	{
		$submit_button_text = t($t_base.'fields/yes_please');
		$email_loggedin_class="email-space";
		$kUser = new cUser();
		$kUser->getUserDetails($_SESSION['user_id']);
		if(empty($postSearchResult['szEmail']))
		{
			$postSearchResult['szCustomerPhone'] = $kUser->szPhoneNumber ;
			$postSearchResult['szEmail'] = $kUser->szEmail ;
			if(!empty($postSearchResult['szEmail']))
			{
				$notify_me_class = "class='orange-button1'";
				$notify_me_onclick = "onclick=\"update_expacted_services_requirement('".$landing_page_url."')\"";
			}
		}
		
		?>
		<!--<h3>F14</h3>-->
		<p class="heading-space" style="margin-bottom:15px;"><?=t($t_base.'title/no_service_popup_text_new');?> <!--<?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/f13_details_text_2');?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/f13_details_text_3')?>--></p>
		<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>

		<?
	}
	?>
	<div class="oh <?=$email_loggedin_class;?>">
		<p class="fl-20"><?=t($t_base.'fields/email');?></p>
		<p class="fl-80">
			<input type="text" id="szCustomerEmail_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_EMAIL_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>');" onkeyup="enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event);" name="addExpactedServiceAry[szEmail]" value="<?=$postSearchResult['szEmail']?>">
			<span id="szCustomerEmail_popup_error" class="red_text" style="display:none;float:right;"></span>
		</p>
	</div>
	<div class="oh" style="margin-top:-5px;">
		<p class="fl-20"><?=t($t_base.'fields/phone');?></p>
		<p class="fl-80">
			<input type="text" id="szCustomerPhone_popup" name="addExpactedServiceAry[szCustomerPhone]" value="<?=$postSearchResult['szCustomerPhone']?>">
		</p>
	</div>
	<?
		if(!empty($postSearchAry['szOriginCountry']) && strlen($postSearchAry['szOriginCountry'])>__MAX_COUNTRY_NAME_LENGTH_ON_BUTTONS___)
		{
			$newLength = __MAX_COUNTRY_NAME_LENGTH_ON_BUTTONS___ - 2 ;
			$originCountryName = mb_substr($postSearchAry['szOriginCountry'],0,$newLength,'UTF8')."..";
		}
		else
		{
			$originCountryName = $postSearchAry['szOriginCountry'];
		}	
			
		if(!empty($postSearchAry['szDestinationCountry']) && strlen($postSearchAry['szDestinationCountry'])>__MAX_COUNTRY_NAME_LENGTH_ON_BUTTONS___)
		{
			$newLength = __MAX_COUNTRY_NAME_LENGTH_ON_BUTTONS___ - 2 ;
			$szDestinationCountryName = mb_substr($postSearchAry['szDestinationCountry'],0,$newLength,'UTF8')."..";
		}
		else
		{
			$szDestinationCountryName = $postSearchAry['szDestinationCountry'];
		}
	?>
	<p align="center" class="button-container">	
		<a href="javascript:void(0);" <?=$notify_me_class?> <?=$notify_me_onclick?>  id="notify_me_button"><span><?=$submit_button_text;?></span></a>
	</p>
	<p align="center" class="button-container">
		<input type="hidden" name="addExpactedServiceAry[idBooking]" value="<?=$idBooking?>">
		<input type="hidden" name="addExpactedServiceAry[idExpactedService]" value="<?=$idExpactedService?>">
		<input type="hidden" name="addExpactedServiceAry[idNotificationType]" value="<?=__NOTIFICATION_TYPE_NO_PICKUP_AND_NO_DELIVERY___?>">
		<input type="hidden" name="addExpactedServiceAry[idServiceType]" id="idServiceType" value="<?=$idService?>">
		<a href="javascript:void(0);" class="orange-button1" onclick="find_transportation('OPTION_R1B','<?=$szBookingRandomNum?>');"><span><?=t($t_base.'fields/continue_button');?> <?=$originCountryName?></span></a>
	</p>
	<p align="center" class="button-container">	
		<a href="javascript:void(0);" class="orange-button1" onclick="find_transportation('OPTION_R1A','<?=$szBookingRandomNum?>');"><span><?=t($t_base.'fields/continue_button_delivery');?> <?=$szDestinationCountryName?></span></a>
	</p>
	

</div>		
</div>
</form>
	<?
}

function display_d1_popup($landing_page_url,$countinue_button=false,$use_param_url=false)
{
	if(!$use_param_url)
	{
		$landing_page_url = __HOME_PAGE_URL__ ;
	}
	else if(empty($landing_page_url))
	{
		$landing_page_url = __HOME_PAGE_URL__ ;
	}
	$t_base = "LandingPage/";
	$idBooking = $postSearchAry['id'];
	
	if($countinue_button)
	{
		$button_text = t($t_base.'fields/countinue');
	}
	else
	{
		$button_text = t($t_base.'fields/back_to_start');
	}
	
	$kWHSSearch = new cWHSSearch();
			$toEmail=$kWHSSearch->getManageMentVariableByDescription('__CONTACT_EMAIL__');
	?>	
	<div id="popup-bg" class="white-bg" ></div>
	<div id="popup-container">
	 <div class="standard-popup-d popup" >
		<h5><?=t($t_base.'fields/thank_you')?></h5>
		<p><?=t($t_base.'title/d1_details_text_new');?><br/><?=$toEmail?></p>		
		<p align="center" style="margin-top:8px;">	
			<a href="javascript:void(0);" class="orange-button1" onclick="redirect_url('<?=$landing_page_url?>');"><span><?=$button_text?></span></a>
		</p>
	</div>		
</div>
<?
}

function display_d2_popup($postSearchAry,$landing_page_url,$countinue_button=false)
{
	if(empty($landing_page_url))
	{
		$landing_page_url = __HOME_PAGE_URL__ ;
	}
	$landing_page_url = __HOME_PAGE_URL__ ;
	$t_base = "LandingPage/";
	$idBooking = $postSearchAry['id'];
	
	if($countinue_button)
	{
		$button_text = t($t_base.'fields/countinue');
	}
	else
	{
		$button_text = t($t_base.'fields/back_to_start');
	}
	$kWHSSearch = new cWHSSearch();
			$toEmail=$kWHSSearch->getManageMentVariableByDescription('__CONTACT_EMAIL__');
	?>	
	<div id="popup-bg" class="white-bg" ></div>
	<div id="popup-container">
	 <div class="standard-popup-d popup" >
		<h5><?=t($t_base.'fields/thank_you')?></h5>
		<p><?=t($t_base.'title/d2_details_text_new');?><br/><?=$toEmail?></p>		
		<p align="center" style="margin-top:8px;">	
			<a href="javascript:void(0);" class="orange-button1" onclick="redirect_url('<?=$landing_page_url?>');"><span><?=$button_text?></span></a>
		</p>
	</div>		
</div>
<?
}
function display_d3_popup($landing_page_url,$countinue_button=false,$postSearchAry)
{
	if(empty($landing_page_url))
	{
		$landing_page_url = __REQUIREMENT_PAGE_URL__ ;
	}
	
	$t_base = "LandingPage/";
	$idBooking = $postSearchAry['id'];
	
	if($countinue_button)
	{
		$button_text = t($t_base.'fields/countinue');
	}
	else
	{
		$button_text = t($t_base.'fields/back_to_start');
	}
	?>	
	<div id="popup-bg" class="white-bg" ></div>
	<div id="popup-container">
	 <div class="standard-popup-d popup" >
		<h5><?=t($t_base.'fields/thank_you')?></h5>
		<p><?=t($t_base.'title/d3_details_text_1');?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/to');?> <?=$postSearchAry['szDestinationCountry']?>. <?=t($t_base.'title/d3_details_text_2');?> </p>		
		<p align="center">	
			<a href="javascript:void(0);" class="orange-button1" onclick="redirect_url('<?=$landing_page_url?>');"><span><?=$button_text?></span></a>
		</p>
	</div>		
</div>
<?
}

function display_F5_popup($postSearchAry,$idService,$idExpactedService,$mode)
{
	$t_base = "LandingPage/";
	$idBooking = $postSearchAry['id'];
	$landing_page_url = __LANDING_PAGE_URL__ ;
	$szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
	if($postSearchAry['iFromRequirementPage']==2)
	{
		$landing_page_url = __SELECT_SIMPLE_SEARCH_URL__ ;
	}
	if($mode == 'OPTION_YCM1B')
	{
		$operation = "OPTION_YCM1A";
	}
	else
	{
		$operation = "OPTION_M1A";
	}
	?>	
	<script type="text/javascript">
		$().ready( function(){
			default_cursor('<?=$_SESSION['user_id']?>');
			
		});
	</script>
	<div id="popup-bg" class="white-bg"></div>
	<form id="no_services_form" method="post" name="no_services_form" action="">
		<div id="popup-container">
		<div class="standard-popup-f popup" >
		<p class="close-icon" align="right"><a heref="javascript:void(0);" onclick="closePopup('all_available_service');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
		<h5><?=t($t_base.'title/f5_popup_title_1')?> <?=$postSearchAry['szOriginCountry']?><?=t($t_base.'title/f5_popup_title_2')?></h5>
		
<?php
	$notify_me_class = "class='gray-button1'";
	if($_SESSION['user_id']<=0)
	{
		$submit_button_text = t($t_base.'fields/submit');
		?>
		<!--<h3>F5</h3>-->
		<p class="heading-space" style="margin-bottom:15px;"><?=t($t_base.'title/no_service_popup_text_new');?> <!--<?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/f5_details_text_2');?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/f5_details_text_3');?>--></p>
		<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>

		<div class="oh name-space">
			<p class="fl-20"><?=t($t_base.'fields/name');?></p>
			<p class="fl-80">
				<input type="text" id="szCustomerName_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_NAME_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>');" onkeyup="enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event);" name="addExpactedServiceAry[szCustomerName]" value="<?=$postSearchResult['szCustomerName']?>">
				<span id="szCustomerName_popup_error" class="red_text" style="display:none;float:right;"></span>
			</p>
		</div>
		<?
	}
	else
	{
		$submit_button_text = t($t_base.'fields/yes_please');
		$email_loggedin_class="email-space";
		$kUser = new cUser();
		$kUser->getUserDetails($_SESSION['user_id']);
		if(empty($postSearchResult['szEmail']))
		{
			$postSearchResult['szCustomerPhone'] = $kUser->szPhoneNumber ;
			$postSearchResult['szEmail'] = $kUser->szEmail ;
			if(!empty($postSearchResult['szEmail']))
			{
				$notify_me_class = "class='orange-button1'";
				$notify_me_onclick = "onclick=\"update_expacted_services_requirement('".$landing_page_url."')\"";
			}
		}
		?>
		<!--<h3>F6</h3>-->
		<p class="heading-space" style="margin-bottom:15px;"><?=t($t_base.'title/no_service_popup_text_new');?> <!--<?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/f6_details_text_2');?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/f5_details_text_3');?>--></p>
		<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>

		<?
	}
?>
	<div class="oh <?=$email_loggedin_clas;?>">
		<p class="fl-20"><?=t($t_base.'fields/email');?></p>
		<p class="fl-80">
			<input type="text" id="szCustomerEmail_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_EMAIL_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>');" onkeyup="enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event);" name="addExpactedServiceAry[szEmail]" value="<?=$postSearchResult['szEmail']?>">
			<span id="szCustomerEmail_popup_error" class="red_text" style="display:none;float:right;"></span>
		</p>
		<input type="hidden" name="addExpactedServiceAry[idBooking]" value="<?=$idBooking?>">
		<input type="hidden" name="addExpactedServiceAry[idNotificationType]" value="<?=__NOTIFICATION_TYPE_NO_PICKUP_FROM_ORIGIN_COUNTRY___?>">
		<input type="hidden" name="addExpactedServiceAry[idExpactedService]" value="<?=$idExpactedService?>">
		<input type="hidden" name="addExpactedServiceAry[idServiceType]" id="idServiceType" value="<?=$idService?>">
	</div>
	<div class="oh" style="margin-top:-5px;">
		<p class="fl-20"><?=t($t_base.'fields/phone');?></p>
		<p class="fl-80">
			<input type="text" id="szCustomerPhone_popup" name="addExpactedServiceAry[szCustomerPhone]" value="<?=$postSearchResult['szCustomerPhone']?>">
		</p>
	</div>
	<?
		if(!empty($postSearchAry['szOriginCountry']) && strlen($postSearchAry['szOriginCountry'])>__MAX_COUNTRY_NAME_LENGTH_ON_BUTTONS___)
		{
			$newLength = __MAX_COUNTRY_NAME_LENGTH_ON_BUTTONS___ - 2 ;
			$originCountryName = mb_substr($postSearchAry['szOriginCountry'],0,$newLength,'UTF8')."..";
		}
		else
		{
			$originCountryName = $postSearchAry['szOriginCountry'];
		}	
			
		if(!empty($postSearchAry['szDestinationCountry']) && strlen($postSearchAry['szDestinationCountry'])>__MAX_COUNTRY_NAME_LENGTH_ON_BUTTONS___)
		{
			$newLength = __MAX_COUNTRY_NAME_LENGTH_ON_BUTTONS___ - 2 ;
			$szDestinationCountryName = mb_substr($postSearchAry['szDestinationCountry'],0,$newLength,'UTF8')."..";
		}
		else
		{
			$szDestinationCountryName = $postSearchAry['szDestinationCountry'];
		}
	?>
	<p align="center" class="button-container">	
		<a href="javascript:void(0);" <?=$notify_me_class?> <?=$notify_me_onclick?>  id="notify_me_button"><span><?=$submit_button_text;?></span></a>
	</p>
	<p align="center" class="button-container">		
		<a href="javascript:void(0);" class="orange-button1" onclick="find_transportation('<?=$operation?>','<?=$szBookingRandomNum?>');"><span><?=t($t_base.'fields/continue_button');?> <?=$originCountryName?></span></a>
	</p>
	
</div>		
</div>
</form>
	<?
}

function display_F9_F10_popup($postSearchAry,$idService,$idExpactedService)
{
	$t_base = "LandingPage/";
	$idBooking = $postSearchAry['id'];
	$landing_page_url = __LANDING_PAGE_URL__ ;
	$szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
	?>	
	<script type="text/javascript">
		$().ready( function(){
			default_cursor('<?=$_SESSION['user_id']?>');
			
		});
	</script>
	<div id="popup-bg" class="white-bg"></div>
	<form id="no_services_form" method="post" name="no_services_form" action="">
		<div id="popup-container">
		<div class="standard-popup-f popup" >
		<p class="close-icon" align="right"><a heref="javascript:void(0);" onclick="closePopup('all_available_service');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
		<h5><?=t($t_base.'title/f9_popup_title_1')?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/f9_popup_title_2')?> <?=$postSearchAry['szDestinationCountry']?><?=t($t_base.'title/f9_popup_title_3')?></h5>
		
<?php
	$notify_me_class = "class='gray-button1'";
	if($_SESSION['user_id']<=0)
	{
		$submit_button_text = t($t_base.'fields/submit');
		?>
		<!-- <h3 class="heading-space" style="margin-bottom:15px;">F9</h3> -->
		<p><?=t($t_base.'title/no_service_popup_text_new');?> <!--<?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/f9_details_text_2');?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/f9_details_text_3')?>--></p>
		<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>

		<div class="oh name-space">
			<p class="fl-20"><?=t($t_base.'fields/name');?></p>
			<p class="fl-80">
				<input type="text" id="szCustomerName_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_NAME_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>');" onkeyup="enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event);" name="addExpactedServiceAry[szCustomerName]" value="<?=$postSearchResult['szCustomerName']?>">
				<span id="szCustomerName_popup_error" class="red_text" style="display:none;float:right;"></span>
			</p>
		</div>
		<?
	}
	else
	{
		$submit_button_text = t($t_base.'fields/yes_please');
		$email_loggedin_class="email-space";
		$kUser = new cUser();
		$kUser->getUserDetails($_SESSION['user_id']);
		if(empty($postSearchResult['szEmail']))
		{
			$postSearchResult['szCustomerPhone'] = $kUser->szPhoneNumber ;
			$postSearchResult['szEmail'] = $kUser->szEmail ;
			if(!empty($postSearchResult['szEmail']))
			{
				$notify_me_class = "class='orange-button1'";
				$notify_me_onclick = "onclick=\"update_expacted_services_requirement('".$landing_page_url."')\"";
			}
		}
		?>
		<!--<h3>F10</h3>-->
		<p class="heading-space" style="margin-bottom:15px;"><?=t($t_base.'title/no_service_popup_text_new');?> <!--<?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/f9_details_text_2');?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/f9_details_text_3')?>--></p>
		<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>

		<?
	}
	?>
	<div class="oh <?=$email_loggedin_class;?>">
		<p class="fl-20"><?=t($t_base.'fields/email');?></p>
		<p class="fl-80">
			<input type="text" id="szCustomerEmail_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_EMAIL_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>');" onkeyup="enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event);" name="addExpactedServiceAry[szEmail]" value="<?=$postSearchResult['szEmail']?>">
			<span id="szCustomerEmail_popup_error" class="red_text" style="display:none;float:right;"></span>
		</p>
		<input type="hidden" name="addExpactedServiceAry[idBooking]" value="<?=$idBooking?>">
		<input type="hidden" name="addExpactedServiceAry[idExpactedService]" value="<?=$idExpactedService?>">
		<input type="hidden" name="addExpactedServiceAry[idNotificationType]" value="<?=__NOTIFICATION_TYPE_NO_PICKUP_AND_NO_DELIVERY___?>">
		<input type="hidden" name="addExpactedServiceAry[idServiceType]" id="idServiceType" value="<?=$idService?>">
	</div>
	<div class="oh" style="margin-top:-5px;">
		<p class="fl-20"><?=t($t_base.'fields/phone');?></p>
		<p class="fl-80">
			<input type="text" id="szCustomerPhone_popup" name="addExpactedServiceAry[szCustomerPhone]" value="<?=$postSearchResult['szCustomerPhone']?>">
		</p>
	</div>
	<p align="center" class="button-container">		
		<a href="javascript:void(0);" <?=$notify_me_class?> <?=$notify_me_onclick?>  id="notify_me_button"><span><?=$submit_button_text;?></span></a>
	</p>
	<p align="center" class="button-container">		
		<a href="javascript:void(0);" class="orange-button1" onclick="find_transportation('OPTION_YCG1N1B','<?=$szBookingRandomNum?>');"><span><?=t($t_base.'fields/continue_button');?> <?=$postSearchAry['szOriginCountry']?></span></a>
	</p>
	<p align="center" class="button-container">	
		<a href="javascript:void(0);" class="orange-button1" onclick="find_transportation('OPTION_YCG1H1A','<?=$szBookingRandomNum?>');"><span><?=t($t_base.'fields/continue_button_delivery');?> <?=$postSearchAry['szDestinationCountry']?></span></a>
	</p>
	
</div>		
</div>
</form>
	<?
}
	if(!function_exists('mime_content_type')) {
	function mime_content_type($filename)
	{
        $mime_types = array(

            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        $ext = strtolower(array_pop(explode('.',$filename)));
        if (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
        }
        elseif (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            return $mimetype;
        }
        else {
            return 'application/octet-stream';
        }
    }
}
function Y1_popup_top($iAvailableServiceCount,$postSearchAry)
{
	$t_base = "LandingPage/";
	if($iAvailableServiceCount==1)
	{
		$service_text = t($t_base.'title/y1_popup_text5');
	}
	else
	{
		$service_text = t($t_base.'title/y1_popup_text2');
	}
		
	if($iAvailableServiceCount<=10)
	{
		$last_text = t($t_base.'title/y1_popup_text6');
	}
	else
	{
		$last_text = t($t_base.'title/y1_popup_text4');
	}
?>
	<p class="question-heading"><?=t($t_base.'title/y1_popup_text1')?> <?=number_format($iAvailableServiceCount)?> <?=$service_text?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/to')?> <?=$postSearchAry['szDestinationCountry']?> (<a href="javascript:void(0);" onclick="open_available_service_popup('<?=$postSearchAry['idOriginCountry']?>','<?=$postSearchAry['idDestinationCountry']?>','<?=$postSearchAry['szBookingRandomNum']?>')"><?=t($t_base.'title/y1_popup_text3')?></a>) - <?=$last_text?>:</p>
<?
}

function display_service_image($postSearchAry,$idServiceType,$szBookingRefNum)
{
	$t_base = "LandingPage/";
	?>
	<script type="text/javascript">
            $().ready(function(){
                $("#transportation-box").attr("style","cursor:pointer");
                $("#transportation-box").unbind("click");
                $("#transportation-box").click(function(){ display_requirement_edit_box_popup('SERVICE_TYPE_EDIT_POPUP','<?=$szBookingRefNum?>') });
            });
	</script>
	<?
	$transportation_header_text = t($t_base.'title/transportation_from')." ";

	if($postSearchAry['iShipperConsignee']==1) // Shipper
	{
		if($idServiceType == __SERVICE_TYPE_PTP__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/shipper_ptp_text_1')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/shipper_ptp_text_2')." ".$postSearchAry['szDestinationCountry'] ;
		}
		elseif($idServiceType == __SERVICE_TYPE_PTD__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/shipper_ptp_text_1')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/shipper_destination_D_text')." ".$postSearchAry['szDestinationCountry'] ;
		}
		elseif($idServiceType == __SERVICE_TYPE_PTW__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/shipper_ptp_text_1')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/shipper_destination_W_text')." ".$postSearchAry['szDestinationCountry'] ;
		}
		elseif($idServiceType == __SERVICE_TYPE_WTP__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/shipper_origin_W_text')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/shipper_ptp_text_2')." ".$postSearchAry['szDestinationCountry'] ;
		}
		elseif($idServiceType == __SERVICE_TYPE_WTD__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/shipper_origin_W_text')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/shipper_destination_D_text')." ".$postSearchAry['szDestinationCountry'] ;
		}
		elseif($idServiceType == __SERVICE_TYPE_WTW__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/shipper_origin_W_text')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/shipper_destination_W_text')." ".$postSearchAry['szDestinationCountry'] ;
		}
		elseif($idServiceType == __SERVICE_TYPE_DTP__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/shipper_origin_D_text')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/shipper_ptp_text_2')." ".$postSearchAry['szDestinationCountry'] ;
		}
		else if($idServiceType == __SERVICE_TYPE_DTW__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/shipper_origin_D_text')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/shipper_destination_W_text')." ".$postSearchAry['szDestinationCountry'] ;
		}
		elseif($idServiceType == __SERVICE_TYPE_DTD__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/shipper_origin_D_text')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/shipper_destination_D_text')." ".$postSearchAry['szDestinationCountry'] ;
		}
	}
	else if($postSearchAry['iShipperConsignee']==2) // Consignee
	{
		if($idServiceType == __SERVICE_TYPE_PTP__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/consignee_origin_P_text')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/consignee_destination_P_text')." ".$postSearchAry['szDestinationCountry'] ;
		}
		elseif($idServiceType == __SERVICE_TYPE_PTW__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/consignee_origin_P_text')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/consignee_destination_W_text')." ".$postSearchAry['szDestinationCountry'] ;
		}
		elseif($idServiceType == __SERVICE_TYPE_PTD__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/consignee_origin_P_text')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/consignee_destination_D_text')." ".$postSearchAry['szDestinationCountry'] ;
		}
		elseif($idServiceType == __SERVICE_TYPE_DTP__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/consignee_origin_D_text')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/consignee_destination_P_text')." ".$postSearchAry['szDestinationCountry'] ;
		}
		elseif($idServiceType == __SERVICE_TYPE_DTW__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/consignee_origin_D_text')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/consignee_destination_W_text')." ".$postSearchAry['szDestinationCountry'] ;
		}
		elseif($idServiceType == __SERVICE_TYPE_DTD__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/consignee_origin_D_text')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/consignee_destination_D_text')." ".$postSearchAry['szDestinationCountry'] ;
		}
		elseif($idServiceType == __SERVICE_TYPE_WTP__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/consignee_origin_W_text')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/consignee_destination_P_text')." ".$postSearchAry['szDestinationCountry'] ;
		}
		elseif($idServiceType == __SERVICE_TYPE_WTD__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/consignee_origin_W_text')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/consignee_destination_D_text')." ".$postSearchAry['szDestinationCountry'] ;
		}
		elseif($idServiceType == __SERVICE_TYPE_WTW__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/consignee_origin_W_text')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/consignee_destination_W_text')." ".$postSearchAry['szDestinationCountry'] ;
		}
	}
	else if($postSearchAry['iShipperConsignee']==3) // Both or None
	{
		if($idServiceType == __SERVICE_TYPE_PTP__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/consignee_origin_P_text')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/shipper_ptp_text_2')." ".$postSearchAry['szDestinationCountry'] ;
		}		
		elseif($idServiceType == __SERVICE_TYPE_PTW__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/consignee_origin_P_text')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/shipper_destination_W_text')." ".$postSearchAry['szDestinationCountry'] ;
		}
		elseif($idServiceType == __SERVICE_TYPE_PTD__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/consignee_origin_P_text')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/shipper_destination_D_text')." ".$postSearchAry['szDestinationCountry'] ;
		}		
		elseif($idServiceType == __SERVICE_TYPE_WTP__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/consignee_origin_W_text')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/shipper_ptp_text_2')." ".$postSearchAry['szDestinationCountry'] ;
		}
		elseif($idServiceType == __SERVICE_TYPE_WTD__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/consignee_origin_W_text')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/shipper_destination_D_text')." ".$postSearchAry['szDestinationCountry'] ;
		}
		elseif($idServiceType == __SERVICE_TYPE_WTW__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/consignee_origin_W_text')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/shipper_destination_W_text')." ".$postSearchAry['szDestinationCountry'] ;
		}
		elseif($idServiceType == __SERVICE_TYPE_DTD__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/consignee_origin_D_text')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/shipper_destination_D_text')." ".$postSearchAry['szDestinationCountry'] ;
		}
		elseif($idServiceType == __SERVICE_TYPE_DTP__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/consignee_origin_D_text')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/shipper_ptp_text_2')." ".$postSearchAry['szDestinationCountry'] ;
		}
		elseif($idServiceType == __SERVICE_TYPE_DTW__)	//DTD
		{
			$transportation_header_text .= t($t_base.'title/consignee_origin_D_text')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/shipper_destination_W_text')." ".$postSearchAry['szDestinationCountry'] ;
		}
	}
	?>
	<p><?=$transportation_header_text?></p>
	<?php
	$iLanguage = getLanguageId();
	$szImageInitial = "service-type";
	if($iLanguage==__LANGUAGE_ID_DANISH__)
	{
		//$szImageInitial .= "-da"; 
	}
	if($idServiceType == __SERVICE_TYPE_DTD__)	//DTD
	{
		?>
		<img src="<?=__BASE_STORE_IMAGE_URL__?>/<?php echo $szImageInitial;?>-DTD.png" alt="From door at origin to door at destination (DTD)" />
		<?
	}
	elseif($idServiceType == __SERVICE_TYPE_DTP__)	//DTD
	{
		?>
		<img src="<?=__BASE_STORE_IMAGE_URL__?>/<?php echo $szImageInitial;?>-DTP.png" alt="From door at origin to port at destination (DTP)" />
		<?
	}
	elseif($idServiceType == __SERVICE_TYPE_DTW__)	//DTD
	{
		?>
		<img src="<?=__BASE_STORE_IMAGE_URL__?>/<?php echo $szImageInitial;?>-DTW.png" alt="From door at origin to warehouse at destination (DTW)" />
		<?
	}
	elseif($idServiceType == __SERVICE_TYPE_WTW__)	//DTD
	{
		?>
		<img src="<?=__BASE_STORE_IMAGE_URL__?>/<?php echo $szImageInitial;?>-WTW.png" alt="From warehouse at origin to warehouse at destination (WTW)" />
		<?
	}
	elseif($idServiceType == __SERVICE_TYPE_WTP__)	//DTD
	{
		?>
		<img src="<?=__BASE_STORE_IMAGE_URL__?>/<?php echo $szImageInitial;?>-WTP.png" alt="From warehouse at origin to port at destination (WTP)" />
		<?
	}
	elseif($idServiceType == __SERVICE_TYPE_WTD__)	//DTD
	{
		?>
		<img src="<?=__BASE_STORE_IMAGE_URL__?>/<?php echo $szImageInitial;?>-WTD.png" alt="From warehouse at origin to door at destination (WTD)" />
		<?
	}
	elseif($idServiceType == __SERVICE_TYPE_PTP__)	//DTD
	{
		?>
		<img src="<?=__BASE_STORE_IMAGE_URL__?>/<?php echo $szImageInitial;?>-PTP.png" alt="From port at origin to port at destination (PTP)" />
		<?
	}
	elseif($idServiceType == __SERVICE_TYPE_PTD__)	//DTD
	{
		?>
		<img src="<?=__BASE_STORE_IMAGE_URL__?>/<?php echo $szImageInitial;?>-PTD.png" alt="From port at origin to door at destination (PTD)" />
		<?
	}
	elseif($idServiceType == __SERVICE_TYPE_PTW__)	//DTD
	{
		?>
		<img src="<?=__BASE_STORE_IMAGE_URL__?>/<?php echo $szImageInitial;?>-PTW.png" alt="From port at origin to warehouse at destination (PTW)" />
		<?
	}
}

function city_postcode_active_box($postSearchAry,$disable=false)
{
	$t_base = "LandingPage/";
	if($disable)
	{
		echo city_postcode_box($postSearchAry);
	}
	else
	{
		$kUser =new cUser();
		$szOriginCityPostcode='';
		if(!empty($postSearchAry['szOriginPostCode']))
		{
			$szOriginCityPostcode = $postSearchAry['szOriginPostCode'];
		}	
		else if(!empty($postSearchAry['szOriginCity']))
		{
			$szOriginCityPostcode = $postSearchAry['szOriginCity'] ;
		}
			
		if(empty($szOriginCityPostcode) && $postSearchAry['iShipperConsignee']==1 && $_SESSION['user_id']>0)
		{
			$kUser->getUserDetails($_SESSION['user_id']);
			if($kUser->szCountry == $postSearchAry['idOriginCountry'])
			{
				$szOriginCityPostcode = $kUser->szPostcode ;
			}
		}	
			
		$szDestinationCityPostcode='';
		if(!empty($postSearchAry['szDestinationPostCode']))
		{
			$szDestinationCityPostcode = $postSearchAry['szDestinationPostCode'];
		}
		else if(!empty($postSearchAry['szDestinationCity']))
		{
			$szDestinationCityPostcode = $postSearchAry['szDestinationCity'];
		}	
		if(empty($szDestinationCityPostcode) && $_SESSION['user_id']>0 && $postSearchAry['iShipperConsignee']==2)
		{
			$kUser->getUserDetails($_SESSION['user_id']);
			if($kUser->szCountry == $postSearchAry['idDestinationCountry'])
			{
				$szDestinationCityPostcode = $kUser->szPostcode ;
			}
		}
		
		$idServiceType = $postSearchAry['idServiceType'];
		$city_postcode_box_top_title = t($t_base.'title/city_postcode_box_ttle_1')." ";
		if($postSearchAry['iShipperConsignee']==1) // Shipper
		{
			if($idServiceType == __SERVICE_TYPE_PTP__ || $idServiceType == __SERVICE_TYPE_WTP__ || $idServiceType == __SERVICE_TYPE_PTW__ || $idServiceType == __SERVICE_TYPE_WTW__)	//DTD
			{
				$city_postcode_box_top_title .= $postSearchAry['szOriginCountry']." ".t($t_base.'title/shipper_location_origin_P_W_text')." ".$postSearchAry['szDestinationCountry']." ".t($t_base.'title/shipper_location_destination_P_W_text') ;
			}
			else if($idServiceType == __SERVICE_TYPE_PTD__ || $idServiceType == __SERVICE_TYPE_WTD__)	//DTD
			{
				$city_postcode_box_top_title .= $postSearchAry['szOriginCountry']." ".t($t_base.'title/shipper_location_origin_P_W_text')." ".$postSearchAry['szDestinationCountry']." ".t($t_base.'title/shipper_location_destination_D_text') ;
			}
			elseif($idServiceType == __SERVICE_TYPE_DTP__ || $idServiceType == __SERVICE_TYPE_DTW__)	//DTD
			{
				$city_postcode_box_top_title .= $postSearchAry['szOriginCountry']." ".t($t_base.'title/shipper_location_origin_D_text')." ".$postSearchAry['szDestinationCountry']." ".t($t_base.'title/shipper_location_destination_P_W_text') ;
			}
			elseif($idServiceType == __SERVICE_TYPE_DTD__)	//DTD
			{
				$city_postcode_box_top_title .= $postSearchAry['szOriginCountry']." ".t($t_base.'title/shipper_location_origin_D_text')." ".$postSearchAry['szDestinationCountry']." ".t($t_base.'title/shipper_location_destination_D_text') ;
			}
		}
		else if($postSearchAry['iShipperConsignee']==2) // Consignee
		{
			if($idServiceType == __SERVICE_TYPE_PTP__ || $idServiceType == __SERVICE_TYPE_PTW__ || $idServiceType == __SERVICE_TYPE_PTW__ || $idServiceType == __SERVICE_TYPE_WTW__)	//DTD
			{
				$city_postcode_box_top_title .= $postSearchAry['szOriginCountry']." ".t($t_base.'title/consignee_location_origin_P_W_text')." ".$postSearchAry['szDestinationCountry']." ".t($t_base.'title/consignee_location_destination_P_W_text') ;
			}
			elseif($idServiceType == __SERVICE_TYPE_PTD__ || $idServiceType == __SERVICE_TYPE_WTD__)	//DTD
			{
				$city_postcode_box_top_title .= $postSearchAry['szOriginCountry']." ".t($t_base.'title/consignee_location_origin_P_W_text')." ".$postSearchAry['szDestinationCountry']." ".t($t_base.'title/consignee_location_destination_D_text') ;
			}
			else if($idServiceType == __SERVICE_TYPE_DTP__ || $idServiceType == __SERVICE_TYPE_DTW__)	//DTD
			{
				$city_postcode_box_top_title .= $postSearchAry['szOriginCountry']." ".t($t_base.'title/consignee_location_origin_D_text')." ".$postSearchAry['szDestinationCountry']." ".t($t_base.'title/consignee_location_destination_P_W_text') ;
			}
			elseif($idServiceType == __SERVICE_TYPE_DTD__)	//DTD
			{
				$city_postcode_box_top_title .= $postSearchAry['szOriginCountry']." ".t($t_base.'title/consignee_location_origin_D_text')." ".$postSearchAry['szDestinationCountry']." ".t($t_base.'title/consignee_location_destination_D_text') ;
			}
		}
		else if($postSearchAry['iShipperConsignee']==3) // Both or none
		{
			if($idServiceType == __SERVICE_TYPE_PTP__ || $idServiceType == __SERVICE_TYPE_PTW__ || $idServiceType == __SERVICE_TYPE_WTP__ || $idServiceType == __SERVICE_TYPE_WTW__)	//DTD
			{
				$city_postcode_box_top_title .= $postSearchAry['szOriginCountry']." ".t($t_base.'title/consignee_location_origin_P_W_text')." ".$postSearchAry['szDestinationCountry']." ".t($t_base.'title/shipper_location_destination_P_W_text') ;
			}
			else if($idServiceType == __SERVICE_TYPE_PTD__ || $idServiceType == __SERVICE_TYPE_WTD__)	//DTD
			{
				$city_postcode_box_top_title .= $postSearchAry['szOriginCountry']." ".t($t_base.'title/consignee_location_origin_P_W_text')." ".$postSearchAry['szDestinationCountry']." ".t($t_base.'title/shipper_location_destination_D_text') ;
			}
			else if($idServiceType == __SERVICE_TYPE_DTP__ || $idServiceType == __SERVICE_TYPE_DTW__)	//DTD
			{
				$city_postcode_box_top_title .= $postSearchAry['szOriginCountry']." ".t($t_base.'title/consignee_location_origin_D_text')." ".$postSearchAry['szDestinationCountry']." ".t($t_base.'title/shipper_location_destination_P_W_text') ;
			}
			elseif($idServiceType == __SERVICE_TYPE_DTD__)	//DTD
			{
				$city_postcode_box_top_title .= $postSearchAry['szOriginCountry']." ".t($t_base.'title/consignee_location_origin_D_text')." ".$postSearchAry['szDestinationCountry']." ".t($t_base.'title/shipper_location_destination_D_text') ;
			}
		}
/*
		
		if($idServiceType == __SERVICE_TYPE_DTP__ || $idServiceType == __SERVICE_TYPE_DTW__ || $idServiceType == __SERVICE_TYPE_DTD__)
		{
			$first_line_text = t($t_base.'title/exact_location');
			$first_line_text .=" ".$postSearchAry['szOriginCountry'];
			$first_line_text .= " <a href='javascript:void(0);' onclick=\"open_do_not_know_popup('DON_NOT_KNOW_POPUP','".$postSearchAry['szBookingRandomNum']."','1');\">".t($t_base.'title/i_dont_know')."</a>";
		}
		else 
		{
			$first_line_text = t($t_base.'title/city_postcode_box_shipper_text')." ".$postSearchAry['szOriginCountry'];
		}
		
		if($idServiceType == __SERVICE_TYPE_PTD__ || $idServiceType == __SERVICE_TYPE_WTD__ || $idServiceType == __SERVICE_TYPE_DTD__)
		{
			$second_line_text = t($t_base.'title/exact_location_destination');
			$second_line_text .=$postSearchAry['szDestinationCountry'];
			$second_line_text .= " <a href='javascript:void(0);' onclick=\"open_do_not_know_popup('DON_NOT_KNOW_POPUP','".$postSearchAry['szBookingRandomNum']."','2');\">".t($t_base.'title/i_dont_know')."</a>";
		}
		else 
		{
			$second_line_text = t($t_base.'title/city_postcode_box_consignee_text')." ".$postSearchAry['szDestinationCountry'] ;
		}
*/
		$iLanguage = getLanguageId();
		$originCountryAry = array();
		$kWHSSearch = new cWHSSearch();
		$originCountryAry = $kWHSSearch->getCountryLocationDetails($postSearchAry['idOriginCountry'],$iLanguage);
		$szOriginNotExactLocation = $originCountryAry['LocationDescriptionNotExact'] ;
		$szOriginExactLocation = $originCountryAry['LocationDescriptionExact'] ;
		
		$destinationCountryAry = array();
		$destinationCountryAry = $kWHSSearch->getCountryLocationDetails($postSearchAry['idDestinationCountry'],$iLanguage);
		$szDestinationNotExactLocation = $destinationCountryAry['LocationDescriptionNotExact'] ;
		$szDestinationExactLocation = $destinationCountryAry['LocationDescriptionExact'] ;
		
		if($postSearchAry['iShipperConsignee']==1) // Shipper
		{
			if($idServiceType == __SERVICE_TYPE_PTP__ || $idServiceType == __SERVICE_TYPE_WTP__ || $idServiceType == __SERVICE_TYPE_WTW__ || $idServiceType == __SERVICE_TYPE_PTW__)
			{
				$first_line_text = $szOriginNotExactLocation." ".t($t_base.'title/for_your_location_in'). " ".$postSearchAry['szOriginCountry'];
				$second_line_text = $szDestinationNotExactLocation." ".t($t_base.'title/for_the_consignee_in'). " ".$postSearchAry['szDestinationCountry'];
			}
			else if($idServiceType == __SERVICE_TYPE_DTP__ || $idServiceType == __SERVICE_TYPE_DTW__)
			{
				$first_line_text = $szOriginExactLocation." ".t($t_base.'title/for_pick_up_in'). " ".$postSearchAry['szOriginCountry'];
				$first_line_text .= " <a href='javascript:void(0);' class='f-size-12' onclick=\"open_do_not_know_popup('DON_NOT_KNOW_POPUP','".$postSearchAry['szBookingRandomNum']."','1');\">".t($t_base.'title/i_dont_know')."</a>";
				
				$second_line_text = $szDestinationNotExactLocation." ".t($t_base.'title/for_the_consignee_in'). " ".$postSearchAry['szDestinationCountry'];
			}
			else if($idServiceType == __SERVICE_TYPE_PTD__ || $idServiceType == __SERVICE_TYPE_WTD__)
			{
				$first_line_text = $szOriginNotExactLocation." ".t($t_base.'title/for_your_location_in'). " ".$postSearchAry['szOriginCountry'];
				$second_line_text = $szDestinationExactLocation." ".t($t_base.'title/for_delivery_in'). " ".$postSearchAry['szDestinationCountry'];
				$second_line_text .= " <a href='javascript:void(0);' class='f-size-12' onclick=\"open_do_not_know_popup('DON_NOT_KNOW_POPUP','".$postSearchAry['szBookingRandomNum']."','2');\">".t($t_base.'title/i_dont_know')."</a>";
			}
			else if($idServiceType == __SERVICE_TYPE_DTD__)
			{
				$first_line_text = $szOriginExactLocation." ".t($t_base.'title/for_pick_up_in'). " ".$postSearchAry['szOriginCountry'];
				$first_line_text .= " <a href='javascript:void(0);' class='f-size-12' onclick=\"open_do_not_know_popup('DON_NOT_KNOW_POPUP','".$postSearchAry['szBookingRandomNum']."','1');\">".t($t_base.'title/i_dont_know')."</a>";
				
				$second_line_text = $szDestinationExactLocation." ".t($t_base.'title/for_delivery_in'). " ".$postSearchAry['szDestinationCountry'];
				$second_line_text .= " <a href='javascript:void(0);' class='f-size-12' onclick=\"open_do_not_know_popup('DON_NOT_KNOW_POPUP','".$postSearchAry['szBookingRandomNum']."','2');\">".t($t_base.'title/i_dont_know')."</a>";
			}
		}
		else if($postSearchAry['iShipperConsignee']==2) // Shipper
		{
			if($idServiceType == __SERVICE_TYPE_PTP__ || $idServiceType == __SERVICE_TYPE_WTP__ || $idServiceType == __SERVICE_TYPE_WTW__ || $idServiceType == __SERVICE_TYPE_PTW__)
			{
				$first_line_text = $szOriginNotExactLocation." ".t($t_base.'title/for_the_shipper_in'). " ".$postSearchAry['szOriginCountry'];
				$second_line_text = $szDestinationNotExactLocation." ".t($t_base.'title/for_your_location_in'). " ".$postSearchAry['szDestinationCountry'];
			}
			else if($idServiceType == __SERVICE_TYPE_DTP__ || $idServiceType == __SERVICE_TYPE_DTW__)
			{
				$first_line_text = $szOriginExactLocation." ".t($t_base.'title/for_pick_up_in'). " ".$postSearchAry['szOriginCountry'];
				$first_line_text .= " <a href='javascript:void(0);' class='f-size-12' onclick=\"open_do_not_know_popup('DON_NOT_KNOW_POPUP','".$postSearchAry['szBookingRandomNum']."','1');\">".t($t_base.'title/i_dont_know')."</a>";
				
				$second_line_text = $szDestinationNotExactLocation." ".t($t_base.'title/for_your_location_in'). " ".$postSearchAry['szDestinationCountry'];
			}
			else if($idServiceType == __SERVICE_TYPE_PTD__ || $idServiceType == __SERVICE_TYPE_WTD__)
			{
				$first_line_text = $szOriginNotExactLocation." ".t($t_base.'title/for_the_shipper_in'). " ".$postSearchAry['szOriginCountry'];
				
				$second_line_text = $szDestinationExactLocation." ".t($t_base.'title/for_delivery_in'). " ".$postSearchAry['szDestinationCountry'];
				$second_line_text .= " <a href='javascript:void(0);' class='f-size-12' onclick=\"open_do_not_know_popup('DON_NOT_KNOW_POPUP','".$postSearchAry['szBookingRandomNum']."','2');\">".t($t_base.'title/i_dont_know')."</a>";
			}
			else if($idServiceType == __SERVICE_TYPE_DTD__)
			{
				$first_line_text = $szOriginExactLocation." ".t($t_base.'title/for_pick_up_in'). " ".$postSearchAry['szOriginCountry'];
				$first_line_text .= " <a href='javascript:void(0);' class='f-size-12' onclick=\"open_do_not_know_popup('DON_NOT_KNOW_POPUP','".$postSearchAry['szBookingRandomNum']."','1');\">".t($t_base.'title/i_dont_know')."</a>";
				
				$second_line_text = $szDestinationExactLocation." ".t($t_base.'title/for_delivery_in'). " ".$postSearchAry['szDestinationCountry'];
				$second_line_text .= " <a href='javascript:void(0);' class='f-size-12' onclick=\"open_do_not_know_popup('DON_NOT_KNOW_POPUP','".$postSearchAry['szBookingRandomNum']."','2');\">".t($t_base.'title/i_dont_know')."</a>";
			}
		}
		else 
		{
			if($idServiceType == __SERVICE_TYPE_PTP__ || $idServiceType == __SERVICE_TYPE_WTP__ || $idServiceType == __SERVICE_TYPE_WTW__ || $idServiceType == __SERVICE_TYPE_PTW__)
			{
				$first_line_text = $szOriginNotExactLocation." ".t($t_base.'title/for_the_shipper_in'). " ".$postSearchAry['szOriginCountry'];
				$second_line_text = $szDestinationNotExactLocation." ".t($t_base.'title/for_the_consignee_in'). " ".$postSearchAry['szDestinationCountry'];
			}
			else if($idServiceType == __SERVICE_TYPE_DTP__ || $idServiceType == __SERVICE_TYPE_DTW__)
			{
				$first_line_text = $szOriginExactLocation." ".t($t_base.'title/for_pick_up_in'). " ".$postSearchAry['szOriginCountry'];
				$first_line_text .= " <a href='javascript:void(0);' class='f-size-12' onclick=\"open_do_not_know_popup('DON_NOT_KNOW_POPUP','".$postSearchAry['szBookingRandomNum']."','1');\">".t($t_base.'title/i_dont_know')."</a>";
				
				$second_line_text = $szDestinationNotExactLocation." ".t($t_base.'title/for_the_consignee_in'). " ".$postSearchAry['szDestinationCountry'];
			}
			else if($idServiceType == __SERVICE_TYPE_PTD__ || $idServiceType == __SERVICE_TYPE_WTD__)
			{
				$first_line_text = $szOriginNotExactLocation." ".t($t_base.'title/for_the_shipper_in'). " ".$postSearchAry['szOriginCountry'];
				$second_line_text = $szDestinationExactLocation." ".t($t_base.'title/for_delivery_in'). " ".$postSearchAry['szDestinationCountry'];
				$second_line_text .= " <a href='javascript:void(0);' class='f-size-12' onclick=\"open_do_not_know_popup('DON_NOT_KNOW_POPUP','".$postSearchAry['szBookingRandomNum']."','2');\">".t($t_base.'title/i_dont_know')."</a>";
			}
			else if($idServiceType == __SERVICE_TYPE_DTD__)
			{
				$first_line_text = $szOriginExactLocation." ".t($t_base.'title/for_pick_up_in'). " ".$postSearchAry['szOriginCountry'];
				$first_line_text .= " <a href='javascript:void(0);' class='f-size-12' onclick=\"open_do_not_know_popup('DON_NOT_KNOW_POPUP','".$postSearchAry['szBookingRandomNum']."','1');\">".t($t_base.'title/i_dont_know')."</a>";
				
				$second_line_text = $szDestinationExactLocation." ".t($t_base.'title/for_delivery_in'). " ".$postSearchAry['szDestinationCountry'];
				$second_line_text .= " <a href='javascript:void(0);' class='f-size-12' onclick=\"open_do_not_know_popup('DON_NOT_KNOW_POPUP','".$postSearchAry['szBookingRandomNum']."','2');\">".t($t_base.'title/i_dont_know')."</a>";
			}
		}
		
?>
		<script type="text/javascript">
		$().ready(function() {	
			$("#szOriginCityPostcode").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
				width: 260,
				matchContains: true,
				mustMatch: true,
				//minChars: 0,
				//multiple: true,
				//highlight: false,
				//multipleSeparator: ",",
				selectFirst: false
			});
			
			$("#szDestinationCityPostcode").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
				width: 260,
				matchContains: true,
				mustMatch: true,
				//minChars: 0,
				//multiple: true,
				//highlight: false,
				//multipleSeparator: ",",
				selectFirst: false
			});	
			
			check_empty_city_post_code('city_postcode_box_form','<?=t($t_base.'fields/type_select')?>',window.event);
			$("#shipper-consignee-box").unbind("click");
			$("#shipper-consignee-box").attr("style","cursor:default");	
			$("#szOriginCityPostcode").focus();
			
			$('body').click( function() {
			    $('#szOriginCityPostcode_autofill').hide('');
			    $('#szDestinationCityPostcode_autofill').hide('');
			});
			
			 $(".ac_results").remove();
		});
		
		//onkeyup = "check_empty_city_post_code('city_postcode_box_form','t($t_base.'fields/type_select')','DESTINATION')"
		//onkeyup = "check_empty_city_post_code('city_postcode_box_form','t($t_base.'fields/type_select')','ORIGIN')"
		
		</script>
			<p><?=$city_postcode_box_top_title?></p>
			<br />
			<form action="" id="city_postcode_box_form" method="post">
			<div class="oh">
				<p class="fl-49"><?=$first_line_text?></p>
				<p class="fl-33" style="width:35%;text-align:right;">
					<input type="text" <?=$disable_text?> onkeyup="check_empty_city_post_code('city_postcode_box_form','<?=t($t_base.'fields/type_select')?>','ORIGIN',event);"  onblur = "check_empty_city_post_code('city_postcode_box_form','<?=t($t_base.'fields/type_select')?>','ORIGIN',event,'2');validate_city_postcode(this.value,'ORIGIN','<?=$postSearchAry['szBookingRandomNum']?>','<?=t($t_base.'fields/type_select')?>');show_default_text(this.id,'<?=t($t_base.'fields/type_select')?>');" onfocus="blank_me(this.id,'<?=t($t_base.'fields/type_select')?>')" name="szOriginCityPostcode" id="szOriginCityPostcode" value="<?=!empty($szOriginCityPostcode)?$szOriginCityPostcode:t($t_base.'fields/type_select');?>"  />
				</p>
				<span class="red_text" id="origin_city_country_error_span" style="float:right;font-style:italic;width:124px;margin-top:3px;"></span>				
			</div>
			<div class="oh">
				<p class="fl-49"><?=$second_line_text?></p>
				<p class="fl-33" style="width:35%;text-align:right;">
					<input type="text" <?=$disable_text?> onkeyup="check_empty_city_post_code('city_postcode_box_form','<?=t($t_base.'fields/type_select')?>','DESTINATION',event);"  onblur="check_empty_city_post_code('city_postcode_box_form','<?=t($t_base.'fields/type_select')?>','DESTINATION',event,'2');validate_city_postcode(this.value,'DESTINATION','<?=$postSearchAry['szBookingRandomNum']?>','<?=t($t_base.'fields/type_select')?>');show_default_text(this.id,'<?=t($t_base.'fields/type_select')?>');" onfocus="blank_me(this.id,'<?=t($t_base.'fields/type_select')?>')" name="szDestinationCityPostcode" id="szDestinationCityPostcode" value="<?=!empty($szDestinationCityPostcode)?$szDestinationCityPostcode:t($t_base.'fields/type_select');?>" />
				</p>
				<span class="red_text" id="destination_city_country_error_span" style="float:right;font-style:italic;width:124px;margin-top:3px;"></span>
			</div>
			<div class="oh next-step" align="right"><a href="javascript:void(0);" id="city_postcode_next_step_button" class="gray-button1"><span><?=t($t_base.'title/next_step')?></span></a></div>
			<input type="hidden" name="mode" value="CITY_POSTCODE">
			<input type="hidden" name="iOriginError" id="iOriginError" value="">
			<input type="hidden" name="iDestinationError" id="iDestinationError" value="">
			<input type="hidden" name="booking_key" value="<?=$postSearchAry['szBookingRandomNum']?>">
			<input type="hidden" name="idOriginCountry" id="idOriginCountry" value="<?=$postSearchAry['idOriginCountry']?>">
			<input type="hidden" name="idDestinationCountry" id="idDestinationCountry" value="<?=$postSearchAry['idDestinationCountry']?>">	
		</form>
	   <?
	}
}

function display_timing_box_active($postSearchAry,$titiming_type_description_text=false,$minDateTime=false,$maxDateTime=false,$idTimingType=false)
{
	$t_base = "LandingPage/";
	$dtTimingDate='';
	if(!empty($postSearchAry['dtTimingDate']) && ($postSearchAry['dtTimingDate']!='0000-00-00 00:00:00'))
	{
		$dtTimingDate = date('d/m/Y',strtotime($postSearchAry['dtTimingDate']));
	}
	$origin_location_text = '';
	$destination_location_text = '';
	if(($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_DTW__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_DTP__))
	{
		$origin_location_text = $postSearchAry['szOriginPostCode'] ;
		if(!empty($origin_location_text))
		{
			$origin_location_text .= " ".$postSearchAry['szOriginCity'];
		}
		else if(empty($origin_location_text))
		{
			$origin_location_text = $postSearchAry['szOriginCity'];
		}
	}
	else 
	{
		$origin_location_text = $postSearchAry['szOriginCountry'] ;
	}
	
	if(($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_WTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_PTD__))
	{
		$destination_location_text = $postSearchAry['szDestinationPostCode'] ;
		if(!empty($destination_location_text))
		{
			$destination_location_text .= " ".$postSearchAry['szDestinationCity'];
		}
		else if(empty($destination_location_text))
		{
			$destination_location_text = $postSearchAry['szDestinationCity'];
		}
	}
	else 
	{
		$destination_location_text = $postSearchAry['szDestinationCountry'] ;
	}
	
	$first_combo_text = '';
	$idServiceType = $postSearchAry['idServiceType'];
	if($idServiceType == __SERVICE_TYPE_PTP__ || $idServiceType == __SERVICE_TYPE_WTP__ || $idServiceType == __SERVICE_TYPE_PTW__ || $idServiceType == __SERVICE_TYPE_WTW__)	//DTD
	{		
		if($postSearchAry['iShipperConsignee']==1)
		{
			$first_combo_text_1 = t($t_base.'title/we');
		}
		else
		{
			$first_combo_text_1 = t($t_base.'title/the_shipper');
		}
		
		if($postSearchAry['iShipperConsignee']==1 || $postSearchAry['iShipperConsignee']==3)
		{
			$second_combo_text_1 = t($t_base.'title/the_consignee_in')." ".$postSearchAry['szDestinationCountry'];
		}
		else
		{
			$second_combo_text_1 = t($t_base.'title/we');
		}		
		$first_combo_text = $first_combo_text_1." ".t($t_base.'title/can_have_the_cargo_ready_in')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/for_hand_over_at_warehouse')." ".t($t_base.'title/active_by')." ".t($t_base.'title/this_date')." ";
		$second_combo_text = $second_combo_text_1." ".t($t_base.'title/must_able_to_collect')." ".t($t_base.'title/this_date')." " ;
		
	}
	else if($idServiceType == __SERVICE_TYPE_PTD__ || $idServiceType == __SERVICE_TYPE_WTD__)	//DTD
	{
		if($postSearchAry['iShipperConsignee']==1)
		{
			$first_combo_text_1 = t($t_base.'title/we');
		}
		else
		{
			$first_combo_text_1 = t($t_base.'title/the_shipper');
		}
		
		if($postSearchAry['iShipperConsignee']==1 || $postSearchAry['iShipperConsignee']==3)
		{
			$second_combo_text_1 = t($t_base.'title/the_consignee');
		}
		else
		{
			$second_combo_text_1 = t($t_base.'title/we');
		}
		
		$first_combo_text = $first_combo_text_1." ".t($t_base.'title/can_have_the_cargo_ready_in')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/for_hand_over_at_warehouse')." ".t($t_base.'title/active_by')." ".t($t_base.'title/this_date')." ";
		$second_combo_text = $second_combo_text_1." ".t($t_base.'title/must_have_cargo_delivered')." ".$postSearchAry['szDestinationCity']." ".t($t_base.'title/least_by')." ".t($t_base.'title/this_date')." " ;
	}
	else if($idServiceType == __SERVICE_TYPE_DTP__ || $idServiceType == __SERVICE_TYPE_DTW__)	//DTD
	{
		if($postSearchAry['iShipperConsignee']==1)
		{
			$first_combo_text_1 = t($t_base.'title/we_have');
		}
		else
		{
			$first_combo_text_1 = t($t_base.'title/the_shipper_has');
		}
		
		if($postSearchAry['iShipperConsignee']==1 || $postSearchAry['iShipperConsignee']==3)
		{
			$second_combo_text_1 = t($t_base.'title/the_consignee_in')." ".$postSearchAry['szDestinationCountry'];
		}
		else
		{
			$second_combo_text_1 = t($t_base.'title/we');
		}
		
		$first_combo_text = $first_combo_text_1." ".t($t_base.'title/cargo_ready_for_pickup_by_forwarder')." ".$postSearchAry['szOriginCity']." ".t($t_base.'title/active_by')." ".t($t_base.'title/this_date')." ";
		$second_combo_text = $second_combo_text_1." ".t($t_base.'title/must_able_to_collect')." ".t($t_base.'title/this_date')." " ;
	}
	else if($idServiceType == __SERVICE_TYPE_DTD__)	//DTD
	{
		if($postSearchAry['iShipperConsignee']==1)
		{
			$first_combo_text_1 = t($t_base.'title/we_have');
		}
		else
		{
			$first_combo_text_1 = t($t_base.'title/the_shipper_has');
		}
		
		if($postSearchAry['iShipperConsignee']==1 || $postSearchAry['iShipperConsignee']==3)
		{
			$second_combo_text_1 = t($t_base.'title/the_consignee'); ;
		}
		else
		{
			$second_combo_text_1 = t($t_base.'title/we');
		}	
		$first_combo_text = $first_combo_text_1." ".t($t_base.'title/cargo_ready_for_pickup_by_forwarder')." ".$postSearchAry['szOriginCity']." ".t($t_base.'title/active_by')." ".t($t_base.'title/this_date')." ";
		$second_combo_text = $second_combo_text_1." ".t($t_base.'title/must_have_cargo_delivered')." ".$postSearchAry['szDestinationCity']." ".t($t_base.'title/least_by')." ".t($t_base.'title/this_date')." ";
	}
	
	if($idTimingType == 1 && !empty($minDateTime))
	{
		//,maxDate: new Date(".date('Y',$minDateTime).", ".date('m',$minDateTime)."-1, ".date('d',$minDateTime).")
		
		$date_picker_argument = "minDate: new Date(".date('Y').", ".date('m')."-1, ".date('d').")" ;
		
		
		$dtMininmuDateTime = time();
		$dtMaximumDateTime = $maxDateTime;
	}	
	if($idTimingType == 2 && !empty($minDateTime) && !empty($maxDateTime))
	{
		//,maxDate: new Date(".date('Y',$maxDateTime).", ".date('m',$maxDateTime)."-1, ".date('d',$maxDateTime).")
		
		$date_picker_argument = "minDate: new Date(".date('Y',$minDateTime).", ".date('m',$minDateTime)."-1, ".date('d',$minDateTime).")" ;
		
		
		$dtMininmuDateTime = $minDateTime ;
		$dtMaximumDateTime = $maxDateTime;
	}
?>
	<script type="text/javascript">
		$().ready(function() {
			<?
				if(!empty($date_picker_argument))
				{				
					?>
					 $("#datepicker1_requirement").datepicker({<?=$date_picker_argument?>});
					<?
				}
				else
				{
					?>
					$("#datepicker1_requirement").datepicker();
					<?
				}
				if(!empty($titiming_type_description_text))
				{
					?>
						//$('#cargo_timing_description').attr('style','display:block;');	 
					<?
				}
				
				if($postSearchAry['idTimingType']>0 && (empty($dtMininmuDateTime) || empty($dtMaximumDateTime)))
				{
					?>
					display_description_text('<?=$postSearchAry['idTimingType']?>','<?=$postSearchAry['szBookingRandomNum']?>');
					<?
				}
			?>
			$("#cargo-box").unbind("click");	
			$("#cargo-box").attr("style","cursor:default");		
			enable_timing_box_button('<?=$postSearchAry['szBookingRandomNum']?>');
			Custom.init();
			<?
				if($postSearchAry['idTimingType']==1)
				{
			?>
				//$(".radio").attr('style','background-position: 0px -50px;');
			<?
				}
				else if($postSearchAry['idTimingType']==2)
				{
			?>
				//$(".radio").attr('style','background-position: 0px -50px;');
			<?
				}
				else
				{
					?>
						
					<?
				}
			?>
		});
	</script>
	<?
		if($idTimingType>0)
		{
			$postSearchAry['idTimingType'] = $idTimingType;
		}
	?>
	<form action="" id="timing_box_form" method="post">
		<p><?=t($t_base.'title/cargo_timing_box_title_1');?> <?=$origin_location_text?> <?=t($t_base.'title/cargo_timing_box_title_2');?> <?=$destination_location_text?></p>
		<p class="checkbox-ab"><label><input name="timingAry[idTimingType]" class="styled" <? if($postSearchAry['idTimingType']==1){?>checked<? } ?> value="1" id="szTiming_origin" onclick = "display_description_text(this.value,'<?=$postSearchAry['szBookingRandomNum']?>');enable_timing_box_button('<?=$postSearchAry['szBookingRandomNum']?>');" type="radio"   /><?=$first_combo_text?></label></p>
		<p class="checkbox-ab"><label><input name="timingAry[idTimingType]" class="styled" <? if($postSearchAry['idTimingType']==2){?>checked<? } ?> value="2" id="szTiming_destination" onclick = "display_description_text(this.value,'<?=$postSearchAry['szBookingRandomNum']?>');enable_timing_box_button('<?=$postSearchAry['szBookingRandomNum']?>');" type="radio"  /><?=$second_combo_text?></label></p>
		<div class="clearfix">
			<p style="margin-left:25px;float:left;min-height: 30px;" class="datefield">
				<input style="float:left;" id="datepicker1_requirement" onblur="enable_timing_box_button('<?=$postSearchAry['szBookingRandomNum']?>');" name="timingAry[dtTiming]" readonly type="text" value="<?=$dtTimingDate?>" />
				<!-- <a href="javascript:void(0);" style="float:left;margin-left:-10px;" onclick="jQuery.datepicker._hideDatepicker();"><img src="<?=__BASE_STORE_IMAGE_URL__?>/down-icon.png" class="close" /></a> -->
				<?
					if(!empty($titiming_type_description_text))
					{
						echo '<span style="float:right" id="cargo_timing_description" class="red_text timing-text" > '.$titiming_type_description_text."</span>";
					}
				?>			
			</p>
			<p align="right" class="fr"><a href="javascript:void(0);" id="timing_box_next_step_button" class="gray-button1" style="margin-top: 3px;"><span><?=t($t_base.'title/next_step')?></span></a></p>
		</div>
		
		<input type="hidden" name="timingAry[dtMinDate]" id="dtMinDate" value="<?=$dtMininmuDateTime?>">
		<input type="hidden" name="timingAry[dtMaxDate]" id="dtMinDate" value="<?=$dtMaximumDateTime?>">
		<input type="hidden" name="mode" value="CARGO_TIMING">
		<input type="hidden" name="booking_key" value="<?=$postSearchAry['szBookingRandomNum']?>">
	</form>
	<?
}
function city_postcode_box($postSearchAry,$disable=false)
{
	$t_base = "LandingPage/";
	if((empty($postSearchAry['szDestinationCity']) && empty($postSearchAry['szDestinationPostCode'])) && (empty($postSearchAry['szOriginCity']) && empty($postSearchAry['szOriginPostCode'])))
	{
		?>
		<script type="text/javascript">
			$().ready(function(){
				$("#shipper-consignee-box").attr("style","cursor:default");
				$("#shipper-consignee-box").unbind("click");
			});
		</script>
			<p><?=t($t_base.'title/specify_location')?></p>
		<?
	}
	else if($postSearchAry['idServiceType']<=0)
	{
		// If user is is moving into Decision Tree then we show default text.
		?>
		<script type="text/javascript">
			$().ready(function(){
				$("#shipper-consignee-box").attr("style","cursor:default");
				$("#shipper-consignee-box").unbind("click");
			});
		</script>
			<p><?=t($t_base.'title/specify_location')?></p>
		<?
	}
	else
	{
		$szBookingRefNum = $postSearchAry['szBookingRandomNum'] ;
		?>
		<script type="text/javascript">
			$().ready(function(){
				$("#shipper-consignee-box").attr("style","cursor:pointer");
				$("#shipper-consignee-box").unbind("click");
				$("#shipper-consignee-box").click(function(){ display_requirement_edit_box_popup('CITY_POSTCODE_EDIT_POPUP','<?=$szBookingRefNum?>') });
			});
		</script>
		<?php
		
		$iLanguage = getLanguageId();
		$originCountryAry = array();
		$kWHSSearch = new cWHSSearch();
		$originCountryAry = $kWHSSearch->getCountryLocationDetails($postSearchAry['idOriginCountry'],$iLanguage);
		$szOriginNotExactLocation = $originCountryAry['LocationDescriptionNotExact'] ;
		$szOriginExactLocation = $originCountryAry['LocationDescriptionExact'] ;
		
		$destinationCountryAry = array();
		$destinationCountryAry = $kWHSSearch->getCountryLocationDetails($postSearchAry['idDestinationCountry'],$iLanguage);
		$szDestinationNotExactLocation = $destinationCountryAry['LocationDescriptionNotExact'] ;
		$szDestinationExactLocation = $destinationCountryAry['LocationDescriptionExact'] ;
		
		$szOriginCityPostcode='';
		if(!empty($postSearchAry['szOriginPostCode']))
		{
			$szOriginCityPostcode = $szOriginExactLocation." ".$postSearchAry['szOriginPostCode'];
		}
		else if(!empty($postSearchAry['szOriginCity']))
		{
			$szOriginCityPostcode = $postSearchAry['szOriginCity'];
		}	
			
		$szDestinationCityPostcode='';
		if(!empty($postSearchAry['szDestinationPostCode']))
		{
			$szDestinationCityPostcode = $szDestinationExactLocation." ".$postSearchAry['szDestinationPostCode'];
		}
		else if(!empty($postSearchAry['szDestinationCity']))
		{
			$szDestinationCityPostcode = $postSearchAry['szDestinationCity'];
		}	
		
		$disable_text = 'disabled';
		?>
		<div class="clearfix nor-form">
			<p class="text"><?=t($t_base.'title/shipper_is_in');?></p>
			<p><input type="text" <?=$disable_text?> style="cursor:pointer;" name="szOriginCityPostcode" id="szOriginCityPostcode" value="<?=$szOriginCityPostcode?>" /></p>
			<p class="text" align="right"><?=t($t_base.'title/consignee_is_in');?></p>
			<p align="right"><input type="text" <?=$disable_text?> style="cursor:pointer;" name="szDestinationCityPostcode" id="szDestinationCityPostcode" value="<?=$szDestinationCityPostcode?>" /></p>
		</div>
		<?
	}
}
function display_timing_box($postSearchAry,$default_text=false)
{
	$t_base = "LandingPage/";
	if($default_text)
	{
		?>
		<script type="text/javascript">
			$().ready(function(){
				$("#cargo-box").attr("style","cursor:default");
				$("#cargo-box").unbind("click");
			});
		</script>
		<p><?=t($t_base.'title/select_timing')?> </p>
		<?
	}
	else if($postSearchAry['idTimingType']<=0)
	{
		?>
		<script type="text/javascript">
			$().ready(function(){
				$("#cargo-box").attr("style","cursor:default");
				$("#cargo-box").unbind("click");
			});
		</script>
		<p><?=t($t_base.'title/select_timing')?> </p>
		<?
	}
	else
	{
		$szBookingRefNum = $postSearchAry['szBookingRandomNum'] ;
		
		$origin_location_text = '';
		$destination_location_text = '';
		if(($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_DTW__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_DTP__))
		{
			$origin_location_text = $postSearchAry['szOriginPostCode'] ;
			if(!empty($origin_location_text))
			{
				$origin_location_text .= " ".$postSearchAry['szOriginCity'];
			}
		}
		else 
		{
			$origin_location_text = $postSearchAry['szOriginCountry'] ;
		}
		
		if(($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_WTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_PTD__))
		{
			$destination_location_text = $postSearchAry['szDestinationCode'] ;
			if(!empty($origin_location_text))
			{
				$destination_location_text .= " ".$postSearchAry['szDestinationCity'];
			}
		}
		else 
		{
			$destination_location_text = $postSearchAry['szDestinationCountry'] ;
		}
		
		?>
		<script type="text/javascript">
			$().ready(function(){
				$("#cargo-box").attr("style","cursor:pointer");
				$("#cargo-box").unbind("click");
				$("#cargo-box").click(function(){ display_requirement_edit_box_popup('CARGO_TIMING_EDIT_POPUP','<?=$szBookingRefNum?>') });
				//Custom.init();
			});
		</script>
		<?
		$idServiceType = $postSearchAry['idServiceType'];
		if($idServiceType == __SERVICE_TYPE_PTP__ || $idServiceType == __SERVICE_TYPE_WTP__ || $idServiceType == __SERVICE_TYPE_PTW__ || $idServiceType == __SERVICE_TYPE_WTW__)	//DTD
		{		
			if($postSearchAry['iShipperConsignee']==1)
			{
				$first_combo_text_1 = t($t_base.'title/we');
			}
			else
			{
				$first_combo_text_1 = t($t_base.'title/the_shipper'); 
			}
			
			if($postSearchAry['iShipperConsignee']==1 || $postSearchAry['iShipperConsignee']==3)
			{
				$second_combo_text_1 = t($t_base.'title/the_consignee_in')." ".$postSearchAry['szDestinationCountry'];
			}
			else
			{
				$second_combo_text_1 = t($t_base.'title/we');
			}		
			$first_combo_text = $first_combo_text_1." ".t($t_base.'title/can_have_the_cargo_ready_in')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/for_hand_over_at_warehouse');
			$second_combo_text = $second_combo_text_1." ".t($t_base.'title/must_able_to_collect')." ";
		}
		else if($idServiceType == __SERVICE_TYPE_PTD__ || $idServiceType == __SERVICE_TYPE_WTD__)	//DTD
		{
			if($postSearchAry['iShipperConsignee']==1)
			{
				$first_combo_text_1 = t($t_base.'title/we');
			}
			else
			{
				$first_combo_text_1 = t($t_base.'title/the_shipper'); 
			}
			
			if($postSearchAry['iShipperConsignee']==1 || $postSearchAry['iShipperConsignee']==3)
			{
				$second_combo_text_1 = t($t_base.'title/the_consignee');
			}
			else
			{
				$second_combo_text_1 = t($t_base.'title/we');
			}
			
			$first_combo_text = $first_combo_text_1." ".t($t_base.'title/can_have_the_cargo_ready_in')." ".$postSearchAry['szOriginCountry']." ".t($t_base.'title/for_hand_over_at_warehouse')." ";
			$second_combo_text = $second_combo_text_1." ".t($t_base.'title/must_have_cargo_delivered')." ".$postSearchAry['szDestinationCity']." ".t($t_base.'title/least_by');
		}
		else if($idServiceType == __SERVICE_TYPE_DTP__ || $idServiceType == __SERVICE_TYPE_DTW__)	//DTD
		{
			if($postSearchAry['iShipperConsignee']==1)
			{
				$first_combo_text_1 = t($t_base.'title/we_have');
			}
			else
			{
				$first_combo_text_1 = t($t_base.'title/the_shipper_has');
			}
			
			if($postSearchAry['iShipperConsignee']==1 || $postSearchAry['iShipperConsignee']==3)
			{
				$second_combo_text_1 = t($t_base.'title/the_consignee_in')." ".$postSearchAry['szDestinationCountry'];
			}
			else
			{
				$second_combo_text_1 = t($t_base.'title/we');
			}
			$first_combo_text = $first_combo_text_1." ".t($t_base.'title/cargo_ready_for_pickup_by_forwarder')." ".$postSearchAry['szOriginCity']." ".t($t_base.'title/by')." ";
			$second_combo_text = $second_combo_text_1." ".t($t_base.'title/must_able_to_collect')." " ;
		}
		else if($idServiceType == __SERVICE_TYPE_DTD__)	//DTD
		{
			if($postSearchAry['iShipperConsignee']==1)
			{
				$first_combo_text_1 = t($t_base.'title/we_have');
			}
			else
			{
				$first_combo_text_1 = t($t_base.'title/the_shipper_has');
			}
			
			if($postSearchAry['iShipperConsignee']==1 || $postSearchAry['iShipperConsignee']==3)
			{
				$second_combo_text_1 = t($t_base.'title/the_consignee');
			}
			else
			{
				$second_combo_text_1 = t($t_base.'title/we');
			}	
			$first_combo_text = $first_combo_text_1." ".t($t_base.'title/cargo_ready_for_pickup_by_forwarder')." ".$postSearchAry['szOriginCity']." ".t($t_base.'title/by')." ";
			$second_combo_text = $second_combo_text_1." ".t($t_base.'title/must_have_cargo_delivered')." ".$postSearchAry['szDestinationCity']." ".t($t_base.'title/to') ." ".t($t_base.'title/least_by')  ;
		}
		if($postSearchAry['idTimingType']==1) // ready at origin
		{
			?>
			<p class="fl"><?=$first_combo_text?></p>
			<?
		}
		else if($postSearchAry['idTimingType']==2) // Available at destination
		{
			?>
			<p class="fl"><?=$second_combo_text?></p>
			<?
		}
		
		$iMonth = (int)date("m",strtotime($postSearchAry['dtTimingDate']));
		$szMonthName = getMonthName($iLanguage,$iMonth);
	?>
		<div class="fr" style="width:27%;" align="right"><input type="text" disabled value="<?=date('j.',strtotime($postSearchAry['dtTimingDate']))." ".$szMonthName." ".date('Y',strtotime($postSearchAry['dtTimingDate']))?>"/></div>
		<?php
	}
}
function display_custom_clearance_box_active($postSearchAry)
{
	$t_base = "LandingPage/";
	$szBookingRefNum = $postSearchAry['szBookingRandomNum'] ;
	//class="styled"
	$kWhsSearch = new cWHSSearch();
	
	$iLanguage = getLanguageId();
//	if($iLanguage==__LANGUAGE_ID_DANISH__)
//	{
//		$szVariableName_export = '__NEED_OF_EXPORT_CUSTOM_CLEARENCE_DANISH__' ;
//		$szVariableName_import = '__NEED_OF_IMPORT_CUSTOM_CLEARENCE_DANISH__' ;
//	}
//	else
//	{
		$szVariableName_export = '__NEED_OF_EXPORT_CUSTOM_CLEARENCE__' ;
		$szVariableName_import = '__NEED_OF_IMPORT_CUSTOM_CLEARENCE__' ;
	//}
	$szOriginCCExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName_export,$iLanguage);
	$szDestinationCCExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName_import,$iLanguage);
	
	if(empty($szOriginCCExplainUrl))
	{
		$szOriginCCExplainUrl = __EXPLAIN_PAGE_URL__ ;
	}	
	if(empty($szDestinationCCExplainUrl))
	{
		$szDestinationCCExplainUrl = __EXPLAIN_PAGE_URL__ ;
	}
	$kWhsSearch->isCustomClearanceExist($postSearchAry);
	
	$display_popup_flag = false;
	if($postSearchAry['idOriginCountry']==__ID_COUNTRY_CHINA__ && ($postSearchAry['iShipperConsignee']==2 || $postSearchAry['iShipperConsignee']==3))
	{
		$display_popup_flag=true;
	}
?>
	<script type="text/javascript">
		$().ready(function(){
			$("#import-box").unbind("click");
			Custom.init();
		});
	</script>
	<p><?=t($t_base.'title/cc_box_title');?></p>
	<form action="" id="custom_clearance_form" method="post">		
		<?
			if($kWhsSearch->iOriginCC==1 && $kWhsSearch->iDestinationCC==1)
			{
				?>	
				
				<p class="checkbox-ab" <?php if($display_popup_flag){?> onclick="display_additional_cc_popup('<?=$postSearchAry['szBookingRandomNum']?>')" <?php }?>><input class="styled"  type="checkbox" <? if($postSearchAry['iOriginCC']==1){?>checked<? }?> name="customClearanceAry[iOriginCC]" value="1"  /><?=t($t_base.'title/export_cc_in');?> <?=$postSearchAry['szOriginCountry']?> <a href="<?=$szOriginCCExplainUrl?>" target="__blank" class="f-size-12" > <?=t($t_base.'title/do_we_need_this');?>?</a></p>
				<p class="checkbox-ab"><input class="styled" type="checkbox" <? if($postSearchAry['iDestinationCC']==2){?>checked<? }?> name="customClearanceAry[iDestinationCC]" value="2" /><?=t($t_base.'title/import_cc_in');?> <?=$postSearchAry['szDestinationCountry']?> <a href="<?=$szDestinationCCExplainUrl?>" target="__blank" class="f-size-12" ><?=t($t_base.'title/do_we_need_this');?>?</a></p>
				
				<?
			}
			else if($kWhsSearch->iOriginCC==1)
			{
				?>				
				<p class="checkbox-ab" <?php if($display_popup_flag){?> onclick="display_additional_cc_popup('<?=$postSearchAry['szBookingRandomNum']?>')" <?php } ?> ><input class="styled" type="checkbox" <? if($postSearchAry['iOriginCC']==1){?>checked<? }?> name="customClearanceAry[iOriginCC]" value="1"  /><?=t($t_base.'title/export_cc_in');?> <?=$postSearchAry['szOriginCountry']?> <a href="<?=$szOriginCCExplainUrl?>" target="__blank" class="f-size-12" ><?=t($t_base.'title/do_we_need_this');?>?</a></p>
				<?php
			}
			else if($kWhsSearch->iDestinationCC==1)
			{
				?>
				
				<p class="checkbox-ab"><input class="styled" type="checkbox" <? if($postSearchAry['iDestinationCC']==2){?>checked<? }?> name="customClearanceAry[iDestinationCC]" value="2" /><?=t($t_base.'title/import_cc_in');?> <?=$postSearchAry['szDestinationCountry']?> <a href="<?=$szDestinationCCExplainUrl?>" target="__blank" class="f-size-12" ><?=t($t_base.'title/do_we_need_this');?>?</a></p>	
				
				<?
			}
			else
			{
				?>
				<script type="text/javascript">						
					$('#import-box').attr('style','display:none;');	 		
				</script>
				<?
			}
		?>
			
		<p align="right" class="fr-next-button"><a href="javascript:void(0);" onclick="update_booking_service_type(false,'<?=$postSearchAry['szBookingRandomNum']?>','CUSTOM_CLEARANCE');" class="orange-button1"><span><?=t($t_base.'title/next_step')?></span></a></p>
		<input type="hidden" name="mode" value="CUSTOM_CLEARANCE">
		<input type="hidden" name="booking_key" value="<?=$postSearchAry['szBookingRandomNum']?>">		
	</form>
	<?php
}

function display_custom_clearance_addition_popup()
{
	$t_base_additional_cc = "LandingPage/";
	
	$iLanguage = getLanguageId();
	$kWhsSearch = new cWHSSearch();
        $szVariableName = '__CHINA_EXPORT_LICENSE_HERE__' ;
//	if($iLanguage==__LANGUAGE_ID_DANISH__)
//	{
//		$szVariableName = '__CHINA_EXPORT_LICENSE_HERE_DANISH__' ;
//	}
//	else
//	{
//		$szVariableName = '__CHINA_EXPORT_LICENSE_HERE__' ;
//	}
	$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
	
		
	if(empty($szExplainUrl))
	{
		$szExplainUrl = __MAIN_SITE_HOME_PAGE_URL__."/learn/";
	}
?>
	
	<div id="popup-bg" class="white-bg"></div>
		<div id="popup-container">
		<div class="standard-popup-x popup" >
		<p class="close-icon" align="right"><a heref="javascript:void(0);" onclick="showHide('additional_cc_popup');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
		<h5><?php echo t($t_base_additional_cc.'title/notice').": ".t($t_base_additional_cc.'title/china_export_licence'); ?></h5>
		<p class="heading-space" ><?php echo t($t_base_additional_cc.'title/china_export_notice_message'); ?></p>
		<p><?php echo t($t_base_additional_cc.'title/learn_more_about'); ?> <a target="_blank" style="color:#FFFFFF;font-style:normal;" href="<?php echo $szExplainUrl; ?>"><?php echo t($t_base_additional_cc.'title/china_export_licence_here'); ?></a></p>
		<p align="center">
			<a href="javascript:void(0);" class="orange-button1" onclick="showHide('additional_cc_popup');"><span><?=t($t_base_additional_cc.'fields/close');?></span></a>&nbsp;&nbsp;
			<a class="orange-button1" target="_blank" href="<?php echo $szExplainUrl; ?>"> <span><?=t($t_base_additional_cc.'fields/more');?></span></a>
		</p>
	</div>
	</div>
	<?php
}
function display_custom_clearance_box($postSearchAry,$default_text=false)
{
	$t_base = "LandingPage/";	
	$szBookingRefNum = $postSearchAry['szBookingRandomNum'] ;
	
	$kWhsSearch = new cWHSSearch();
	$kWhsSearch->isCustomClearanceExist($postSearchAry);
	if(((int)$kWhsSearch->iOriginCC == 0) && ((int)$kWhsSearch->iDestinationCC == 0))
	{
		?>
			<script type="text/javascript">						
				$('#import-box').attr('style','display:none;');	 		
			</script>
		<?
		return ;
	}
	?>
	<script type="text/javascript">
		$().ready(function(){
			$("#import-box").attr("style","cursor:pointer");
			$("#import-box").unbind("click");
			$("#import-box").click(function(){ display_requirement_edit_box_popup('CUSTOM_CLEARANCE_EDIT_POPUP','<?=$szBookingRefNum?>') });
		});
	</script>
	<?php
	if($postSearchAry['iOriginCC']==1 && $postSearchAry['iDestinationCC']==2)
	{
		?>
		<p><?=t($t_base.'title/export_cc_in');?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/and');?> <?=t($t_base.'title/import_cc_in');?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/required_from_forwarder');?> </p>
		<?
	}
	else if($postSearchAry['iOriginCC']==1)
	{
		?>
			<p><?=t($t_base.'title/export_cc_in');?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/required_from_forwarder');?> </p>
		<?
	}
	else if($postSearchAry['iDestinationCC']==2)
	{
		?>
			<p><?=t($t_base.'title/import_cc_in');?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/required_from_forwarder');?> </p>
		<?
	}
	else if($postSearchAry['iCustomClearance']==3)
	{
		?>
		<p><?=t($t_base.'title/no_cc_error_message');?></p>
		<?
	}
	else if($postSearchAry['iOriginCC']<=0 && $postSearchAry['iDestinationCC']<=0)
	{
		?>
		<script type="text/javascript">
			$().ready(function(){
				$("#import-box").attr("style","cursor:default");
				$("#import-box").unbind("click");
			});
		</script>
			<p><?=t($t_base.'title/cc_default_text')?> </p>
		<?php
	}
}

function cargo_details_box_active($postSearchAry,$disabled=false,$use_session_flag=false,$iDonotincludeCustomjs=false)
{
	$t_base = "LandingPage/";
	if($disabled)
	{	
            // this block of code will called once any 1 of the upper box was clicked for edit. 
            ?>
            <p><?=t($t_base.'title/complete_cargo')?> </p>
            <?php
	}
	else
	{
        $kBooking = new cBooking();
        $kConfig = new cConfig();
        $idBooking = $postSearchAry['id'];

        // geting all weight measure 
        $weightMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_WEIGHT_MEASURE__');

        // geting all cargo measure 
        $cargoMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_CARGO_MEASURE__');
        $number = 1;
        if($use_session_flag)
        {
                $cargoAry = $_SESSION['cargo_details_'.$idBooking];
                $cargo_counter = count($cargoAry['iLength']);
                //echo "<br /> use session ";
        }
        else
        {
                $cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);
                $cargo_counter = count($cargoAry);
                //echo "<br /> use db ";
        }
        if($cargo_counter<=0)
        {
                $cargo_counter = 1;
        }
        if(!empty($cargoAry))
        {
                if($use_session_flag)
                {
                    $fLength = ceil($cargoAry['iLength'][$number]);
                    $fWidth = ceil($cargoAry['iWidth'][$number]);
                    $fHeight = ceil($cargoAry['iHeight'][$number]);
                    $fWeight = ceil($cargoAry['iWeight'][$number]);
                    $iQuantity = ceil($cargoAry['iQuantity'][$number]);
                    $idCargo = $cargoAry['id'][$number];
                    $iDangerCargo = $cargoAry['iDangerCargo'][$number];

                }
                else
                {
                    $fLength = ceil($cargoAry[$number]['fLength']);
                    $fWidth = ceil($cargoAry[$number]['fWidth']);
                    $fHeight = ceil($cargoAry[$number]['fHeight']);
                    $fWeight = ceil($cargoAry[$number]['fWeight']);
                    $iQuantity = ceil($cargoAry[$number]['iQuantity']);
                    $idCargo = $cargoAry[$number]['id'];
                    $iDangerCargo = $cargoAry[$number]['iDangerCargo'];
                }
        }
        if($disabled)
        {
                $disabled_str = "disabled='disabled'";
        }
        $szBookingRefNum = $postSearchAry['szBookingRandomNum'] ;

        $kWhsSearch = new cWHSSearch();

        $iLanguage = getLanguageId();
        //        if($iLanguage==__LANGUAGE_ID_DANISH__)
//        {
//            $szVariableName = '__IS_DANGEROUS_CARGO_DANISH__' ;
//        }
//        else
//        {
//            $szVariableName = '__IS_DANGEROUS_CARGO__' ;
//        }
        $szVariableName = '__IS_DANGEROUS_CARGO__' ;
        $szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
        
        if(empty($szExplainUrl))
        {
            $szExplainUrl = __EXPLAIN_PAGE_URL__ ;
        }

        $iLanguage = getLanguageId();		
        if($iLanguage==__LANGUAGE_ID_DANISH__)
        {
            $szYouAreDoneImageName = "done-arrow-da.png";
        }
        else
        {
            $szYouAreDoneImageName = "done-arrow.png";
        }

        //if($iLanguage==__LANGUAGE_ID_DANISH__)
        //{
            $szDangor_cargo_yes = t($t_base.'fields/yes');
            $szDangor_cargo_no = t($t_base.'fields/no');
        /*}
        else
        {
            $szDangor_cargo_yes = 'Yes';
            $szDangor_cargo_no = 'No';
        }*/ 
	?>		
	<script type="text/javascript">
	$().ready(function(){
		<?php
		if(!$disabled && !$use_session_flag)
		{
	    	if((int)$postSearchAry['iDoNotKnowCargo']==1)
			{
				?>
				$("#iDoNotKnow").attr('checked','checked');			
				autofill_cargo_dimentions('REQUIREMENT_PAGE',false,'<?php echo $weightMeasureAry[0]['szDescription']?>','<?php echo $cargoMeasureAry[0]['szDescription']?>','<?php echo $szDangor_cargo_no; ?>');
				<?php
			}
		}
		if(!$disabled)
		{
			?>
			enable_cargo_show_option_button();
			<?php 
		}
		if(!$iDonotincludeCustomjs)
		{
			?>
			Custom.init();
			<?php 
		}
		?>
	});
	</script>
	<form action="" id="cargo_details_form" method="post">
		<img src="<?=__BASE_STORE_IMAGE_URL__."/".$szYouAreDoneImageName; ?>" class="done-text" /> 
		<p class="cargo-heading"><?=t($t_base.'title/cargo_details_title');?></p>
		<p class="checkbox-ab">
			<input type="checkbox" class="styled" <?=$disabled_str?> name="cargodetailAry[iDoNotKnowCargo]" onclick="autofill_cargo_dimentions('REQUIREMENT_PAGE',false,'<?php echo $weightMeasureAry[0]['szDescription']?>','<?php echo $cargoMeasureAry[0]['szDescription']?>','<?php echo $szDangor_cargo_no; ?>');" value="1" id="iDoNotKnow">
			<?=t($t_base.'title/donot_know_cargo_title');?>
		</p>	
		<div class="cargo1 clearfix">
			<p class="field-name"><?=t($t_base.'title/outer_dimensions');?></p>
			<p class="input-title">
				<span><?=t($t_base.'fields/length');?></span>
				<input type="hidden" name="cargodetailAry[idCargo][<?=$number?>]" value="<?=$idCargo?>" />
				<input type="text" <?=$disabled_str?> pattern="[0-9]*" onblur="validate_cargo_fields('Length',this.id,'<?=$number?>');" onkeyup="enable_cargo_show_option_button(event,'Length');validate_cargo_fields('Length',this.id,'<?=$number?>');" name="cargodetailAry[iLength][<?=$number?>]" value="<?=$fLength?>" id= "iLength<?=$number?>" />
			</p>
			<p class="sep">x</p>
			<p class="input-title">
				<span><?=t($t_base.'fields/width');?></span>
				<input type="text" <?=$disabled_str?> pattern="[0-9]*" onblur="validate_cargo_fields('Width',this.id,'<?=$number?>');" onkeyup="enable_cargo_show_option_button(event,'Width');validate_cargo_fields('Width',this.id,'<?=$number?>');"  name="cargodetailAry[iWidth][<?=$number?>]" value="<?=$fWidth?>" id= "iWidth<?=$number?>" />
			</p>
			<p class="sep">x</p>
			<p class="input-title">
				<span><?=t($t_base.'fields/height');?></span>
				<input type="text" <?=$disabled_str?> pattern="[0-9]*" onblur="validate_cargo_fields('Height',this.id,'<?=$number?>');" onkeyup="enable_cargo_show_option_button(event,'Height');validate_cargo_fields('Height',this.id,'<?=$number?>');" name="cargodetailAry[iHeight][<?=$number?>]" value="<?=$fHeight?>" id= "iHeight<?=$number?>" />
			</p>
			<p class="input-title"><span>&nbsp;</span>
				<select name="cargodetailAry[idCargoMeasure][<?=$number?>]" id= "idCargoMeasure<?=$number?>"  <?=$disabled_str?> class="styled">
					 <?
					 	if(!empty($cargoMeasureAry))
					 	{
					 		foreach($cargoMeasureAry as $cargoMeasureArys)
					 		{
					 			?>
					 				<option value="<?=$cargoMeasureArys['id']?>" <? if($cargoAry[$number]['idCargoMeasure']==$cargoMeasureArys['id']){?> selected <? }?>><?=$cargoMeasureArys['szDescription']?></option>
					 			<?
					 		}
					 	}
					 ?>
				</select>
			</p>
			<p class="sep">x</p>
			<p class="input-title">
				<span><?=t($t_base.'fields/quantity');?></span>
				<input type="text" <?=$disabled_str?> pattern="[0-9]*" onblur="validate_cargo_fields('Quantity',this.id,'<?=$number?>');" onkeyup="enable_cargo_show_option_button(event,'Quantity');validate_cargo_fields('Quantity',this.id,'<?=$number?>');" name="cargodetailAry[iQuantity][<?=$number?>]" value="<?=$iQuantity?>" id= "iQuantity<?=$number?>" />
			</p>
		</div>
		<div class="cargo2 clearfix">
			<p class="field-name"><?=t($t_base.'fields/total_weight');?></p>
			<p class="input-title">
				<input type="text" <?=$disabled_str?> pattern="[0-9]*" name="cargodetailAry[iWeight][<?=$number?>]" onblur="validate_cargo_fields('Weight',this.id,'<?=$number?>');" onkeyup="enable_cargo_show_option_button(event,'Weight');validate_cargo_fields('Weight',this.id,'<?=$number?>');" value="<?=$fWeight?>" id= "iWeight<?=$number?>" />
			</p>
			<p class="sep">&nbsp;</p>
			<p class="input-title">
				<select size="1" id= "idWeightMeasure<?=$number?>" <?=$disabled_str?> name="cargodetailAry[idWeightMeasure][<?=$number?>]" class="styled">
					 <?
					 	if(!empty($weightMeasureAry))
					 	{
					 		foreach($weightMeasureAry as $weightMeasureArys)
					 		{
					 			?>
					 				<option value="<?=$weightMeasureArys['id']?>" <? if($cargoAry[$number]['idWeightMeasure']==$weightMeasureArys['id']){?> selected <? }?>><?=$weightMeasureArys['szDescription']?></option>
					 			<?
					 		}
					 	}
					 ?>
				</select>
			</p>
			<p class="text"><?=t($t_base.'fields/dangerous_cargo');?>? <a href="<?=$szExplainUrl?>" target="__blank"><?=t($t_base.'fields/help');?>!</a></p>
			<p class="sep">&nbsp;</p>
			<p class="input-title">
				<select size="1" <?=$disabled_str?> id= "iDangerCargo<?=$number?>"  name="cargodetailAry[iDangerCargo][<?=$number?>]" class="styled">
					<option value="No" <? if($iDangerCargo == 'No'){?>selected<? } ?> ><?=$szDangor_cargo_no?></option>
					<option value="Yes" <? if($iDangerCargo == 'Yes'){?>selected<? } ?>><?=$szDangor_cargo_yes?></option>
				</select>
			</p>
		</div>
		
		<?php
		//echo $cargo_counter ;
		if($cargo_counter>1)
		{	
			echo "<div id='cargo'>";
				$ctr=0;
				for($number=2;$number<=$cargo_counter;$number++)
				{
					if(!empty($cargoAry))
					{
						if($use_session_flag)
						{
							$fLength = ceil($cargoAry['iLength'][$number]);
							$fWidth = ceil($cargoAry['iWidth'][$number]);
							$fHeight = ceil($cargoAry['iHeight'][$number]);
							$fWeight = ceil($cargoAry['iWeight'][$number]);
							$iQuantity = ceil($cargoAry['iQuantity'][$number]);
							$idCargo = $cargoAry['id'][$number];
							$iDangerCargo = $cargoAry['iDangerCargo'][$number];
						}
						else
						{
							$fLength = ceil($cargoAry[$number]['fLength']);
							$fWidth = ceil($cargoAry[$number]['fWidth']);
							$fHeight = ceil($cargoAry[$number]['fHeight']);
							$fWeight = ceil($cargoAry[$number]['fWeight']);
							$iQuantity = ceil($cargoAry[$number]['iQuantity']);
							$idCargo = $cargoAry[$number]['id'];
							$iDangerCargo = $cargoAry[$number]['iDangerCargo'];
						}
					}	
			?>
			<div id="cargo<?=$number-1?>">
			<hr>
			<div class="cargo1 clearfix">
			<p class="field-name"><?=t($t_base.'title/outer_dimensions');?></p>
			<p class="input-title">
				<span><?=t($t_base.'fields/length');?></span>
				<input type="hidden" name="cargodetailAry[idCargo][<?=$number?>]" value="<?=$idCargo?>" />
				<input type="text" <?=$disabled_str?> pattern="[0-9]*" onblur="validate_cargo_fields('Length',this.id,'<?=$number?>');" onkeyup="enable_cargo_show_option_button(event,'Length');validate_cargo_fields('Length',this.id,'<?=$number?>');" name="cargodetailAry[iLength][<?=$number?>]" value="<?=$fLength?>" id= "iLength<?=$number?>" />
			</p>
			<p class="sep">x</p>
			<p class="input-title">
				<span><?=t($t_base.'fields/width');?></span>
				<input type="text" <?=$disabled_str?> pattern="[0-9]*" onblur="validate_cargo_fields('Width',this.id,'<?=$number?>');" onkeyup="enable_cargo_show_option_button(event,'Width');validate_cargo_fields('Width',this.id,'<?=$number?>');"  name="cargodetailAry[iWidth][<?=$number?>]" value="<?=$fWidth?>" id= "iWidth<?=$number?>" />
			</p>
			<p class="sep">x</p>
			<p class="input-title">
				<span><?=t($t_base.'fields/height');?></span>
				<input type="text" <?=$disabled_str?> pattern="[0-9]*" onblur="validate_cargo_fields('Height',this.id,'<?=$number?>');" onkeyup="enable_cargo_show_option_button(event,'Height');validate_cargo_fields('Height',this.id,'<?=$number?>');" name="cargodetailAry[iHeight][<?=$number?>]" value="<?=$fHeight?>" id= "iHeight<?=$number?>" />
			</p>
			<p class="input-title"><span>&nbsp;</span>
				<select name="cargodetailAry[idCargoMeasure][<?=$number?>]" id= "idCargoMeasure<?=$number?>" <?=$disabled_str?> class="styled">
					 <?
					 	if(!empty($cargoMeasureAry))
					 	{
					 		foreach($cargoMeasureAry as $cargoMeasureArys)
					 		{
					 			?>
					 				<option value="<?=$cargoMeasureArys['id']?>" <? if($cargoAry[$number]['idCargoMeasure']==$cargoMeasureArys['id']){?> selected <? }?>><?=$cargoMeasureArys['szDescription']?></option>
					 			<?
					 		}
					 	}
					 ?>
				</select>
			</p>
			<p class="sep">x</p>
			<p class="input-title">
				<span><?=t($t_base.'fields/quantity');?></span>
				<input type="text" <?=$disabled_str?> pattern="[0-9]*" onblur="validate_cargo_fields('Quantity',this.id,'<?=$number?>');" onkeyup="enable_cargo_show_option_button(event,'Quantity');validate_cargo_fields('Quantity',this.id,'<?=$number?>');" name="cargodetailAry[iQuantity][<?=$number?>]" value="<?=$iQuantity?>" id= "iQuantity<?=$number?>" />
			</p>
		</div>
		<div class="cargo2 clearfix">
			<p class="field-name"><?=t($t_base.'fields/total_weight');?></p>
			<p class="input-title">
				<input type="text" <?=$disabled_str?> pattern="[0-9]*" onblur="validate_cargo_fields('Weight',this.id,'<?=$number?>');" name="cargodetailAry[iWeight][<?=$number?>]" onkeyup="enable_cargo_show_option_button(event,'Weight');validate_cargo_fields('Weight',this.id,'<?=$number?>');" value="<?=$fWeight?>" id= "iWeight<?=$number?>" />
			</p>
			<p class="sep">&nbsp;</p>
			<p class="input-title">
				<select size="1" <?=$disabled_str?> id= "idWeightMeasure<?=$number?>" name="cargodetailAry[idWeightMeasure][<?=$number?>]" class="styled">
					 <?
					 	if(!empty($weightMeasureAry))
					 	{
					 		foreach($weightMeasureAry as $weightMeasureArys)
					 		{
					 			?>
					 				<option value="<?=$weightMeasureArys['id']?>" <? if($cargoAry[$number]['idWeightMeasure']==$weightMeasureArys['id']){?> selected <? }?>><?=$weightMeasureArys['szDescription']?></option>
					 			<?
					 		}
					 	}
					 ?>
				</select>
			</p>
			<p class="text"><?=t($t_base.'fields/dangerous_cargo');?>? <a href="<?=$szExplainUrl?>" target="__blank"><?=t($t_base.'fields/help');?>!</a></p>
			<p class="sep">&nbsp;</p>
			<p class="input-title">
				<select size="1" id= "iDangerCargo<?=$number?>" <?=$disabled_str?> name="cargodetailAry[iDangerCargo][<?=$number?>]" class="styled">
					<option value="No" <? if($iDangerCargo =='No' || empty($iDangerCargo)){?>selected<? } ?> ><?=$szDangor_cargo_no?></option>
					<option value="Yes" <? if($iDangerCargo == 'Yes'){?>selected<? } ?> ><?=$szDangor_cargo_yes?></option>
				</select>
			</p>
		</div>	
	</div>
			<?
			$ctr++;				
		}
		echo "<div id='cargo".$cargo_counter."'></div></div>";
		$remove_cargo_link_style = " style='display:block;'";
	}
	else
	{
		$remove_cargo_link_style = " style='display:none;'";
		?>
		<div id="cargo">
			<div id="cargo<?=$cargo_counter?>"></div>
		</div>
		<?
	}
	if(!$disabled)
	{
	?>
	<div class="clearfix" style="margin-top: -6px;">
		<p class="fl" style="width:138px;"><a href="javascript:void(0);" onclick="add_more_cargo('REQUIREMENT_PAGE')">+ <?=t($t_base.'title/add_more_cargo');?></a></p>
		<p class="red_text" style="display:none;" id="cargo_measure_error_span">&nbsp;</p>
		<p style="margin-top:6px;margin-bottom:11px;" align="right" class="fr"><a href="javascript:void(0);" id="cargo_detail_button_show_option" class="gray-button1"><span><?=t($t_base.'title/show_options');?></span></a></p>
		<p class="fr" align="right" style="margin-right:30px"><a href="javascript:void(0);" id="remove" <?=$remove_cargo_link_style?> onclick="remove_cargo('REQUIREMENT_PAGE');"><?=t($t_base.'fields/remove_cargo');?></a></p>
		<input type="hidden" name="cargodetailAry[hiddenPosition]" id="hiddenPosition" value="<?=$cargo_counter?>" />
		<input type="hidden" name="cargodetailAry[hiddenPosition1]" id="hiddenPosition1" value="<?=$cargo_counter?>" />			
	</div>
	<? }?>
	<input type="hidden" name="idCargoMeasure_hidden" id="idCargoMeasure_hidden" value="1">
	<input type="hidden" name="idWeightMeasure_hidden" id="idWeightMeasure_hidden" value="1">
	<input type="hidden" name="iDangerCargo_hidden" id="iDangerCargo_hidden" value="No">
	<input type="hidden" name="mode" value="CARGO_DETAILS">
	<input type="hidden" id="booking_key" name="booking_key" value="<?=$postSearchAry['szBookingRandomNum']?>">
	</form>
	<?php
}
}

function check_user_type($postSearchAry)
{
	if((int)$_SESSION['user_id']>0)
	{
		$kUser = new cUser();
		$kUser->getUserDetails($_SESSION['user_id']);
		if($kUser->szCountry == $postSearchAry['idOriginCountry'])
		{
			return 'SHIPPER';
		}
		else if($kUser->szCountry == $postSearchAry['idDestinationCountry'])
		{
			return 'CONSIGNEE';
		}
		else
		{
			return 'BOTH';
		}
	}
	else
	{
		return 'BOTH';
	}
}
function display_service_type_box_default($postSearchAry)
{
	$t_base = "LandingPage/";
	?>
		<script type="text/javascript">
		$().ready(function(){
			$("#transportation-box").attr("style","cursor:default;");
			$("#transportation-box").unbind("click");
		});
	</script>
		<p><?=t($t_base.'title/select_transportation_default_text')?></p>
	<?php
}

function display_service_type_box_active($postSearchAry)
{
	$t_base = "LandingPage/";
	$kWHSSearch = new cWHSSearch();
	$kBooking = new cBooking();
	$idBooking = $postSearchAry['id'];
	$szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
	$originCountryText = get_country_name($postSearchAry['szOriginCountry'],'ORIGIN');
	$destinationCountryText = get_country_name($postSearchAry['szDestinationCountry'],'DESTINATION');
	if((int)$postSearchAry['idServiceType']<=0 && (int)$postSearchAry['idOriginCountry']<=0 && (int)$postSearchAry['idDestinationCountry']<=0)
	{
		?>
			<p><?=t($t_base.'title/select_transportation_default_text')?></p>
			</div> <!-- closing div for requirement-content -->
		<?php
	}
	else if((int)$_SESSION['user_id']>0)
	{
		$kUser = new cUser();
		$kUser->getUserDetails($_SESSION['user_id']);
		if($kUser->szCountry == $postSearchAry['idOriginCountry'])
		{
			$res_ary=array();			
			$res_ary['iShipperConsignee']= 1;
			$res_ary['idServiceType']= '';
			
			if(!empty($res_ary))
			{
				$update_query = '';
				foreach($res_ary as $key=>$value)
				{
					$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
				}
			}
			
			$update_query = rtrim($update_query,",");
			
			if($kBooking->updateDraftBooking($update_query,$idBooking))
			{
				
			}
			
			//check if DTP is available or not. 
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iDTP'))
			{
				?>
				<ol>
					<li><?=t($t_base.'title/P1_box_title_1');?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/P1_box_title_2');?>?</li>
				</ol>
				</div> <!-- closing div for requirement-content -->
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_P1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/P1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_K1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/P1_box_option_B');?></span></a></p>
				<p class="active-orange-btn"><a href="<?=__EXPLAIN_PAGE_URL__?>" target="_blank" class="orange-button1"><span><?=t($t_base.'title/P1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:30%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?php
			}
			else
			{
				?>
				<ol>
					<li><?=t($t_base.'title/k1_box_title_1');?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/k1_box_title_2');?>?</li>
				</ol>
				</div> <!-- closing div for requirement-content -->
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_K1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/k1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_K1B','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/k1_box_option_B');?> <?=$originCountryText?></span></a></p>
				<p class="active-orange-btn"><a href="<?=__EXPLAIN_PAGE_URL__?>" target="_blank" class="orange-button1"><span><?=t($t_base.'title/k1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:30%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0)" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?php
			}
		}
		else if($kUser->szCountry == $postSearchAry['idDestinationCountry'])
		{
			$res_ary=array();			
			$res_ary['iShipperConsignee']= 2;
			$res_ary['idServiceType']= '';
			if(!empty($res_ary))
			{
				$update_query = '';
				foreach($res_ary as $key=>$value)
				{
					$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
				}
			}
			
			$update_query = rtrim($update_query,",");
			
			if($kBooking->updateDraftBooking($update_query,$idBooking))
			{
				
			}
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iDTP'))
			{
				
				?>
				<ol>
					<li><?=t($t_base.'title/g1_box_title_1');?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/g1_box_title_2');?>?</li>
				</ol>
				</div> <!-- closing div for requirement-content -->
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_G1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/g1_box_option_A');?> <?=$originCountryText?> (<?=t($t_base.'title/g1_box_option_A_2');?>)</span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_M1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/g1_box_option_B');?></span></a></p>
				<p class="active-orange-btn"><a href="<?=__EXPLAIN_PAGE_URL__?>" target="_blank" class="orange-button1"><span><?=t($t_base.'title/g1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:30%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0)" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?
			}
			else
			{
				?>
				<ol >
					<li><?=t($t_base.'title/m1_box_title_1');?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/m1_box_title_2');?>?</li>
				</ol>
				</div> <!-- closing div for requirement-content -->
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_M1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/m1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_M1B','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/m1_box_option_B');?> <?=$originCountryText?> (<?=t($t_base.'title/m1_box_option_B_2');?>)</span></a></p>
				<p class="active-orange-btn"><a href="<?=__EXPLAIN_PAGE_URL__?>" target="_blank" class="orange-button1"><span><?=t($t_base.'title/m1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:30%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0)" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?
			}
		}
		else
		{
			?>
				<ol>
					<li><?=t($t_base.'title/y1_box_title');?>?</li>
				</ol>
				</div> <!-- closing div for requirement-content -->
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('SHIPPER','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/y1_box_option_A');?> <?=$originCountryText?> (<?=t($t_base.'title/the_shipper');?>)</span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('CONSIGNEE','<?=$szBookingRandomNum?>')" class="orange-button1"><span><?=t($t_base.'title/y1_box_option_B');?> <?=$destinationCountryText?> (<?=t($t_base.'title/the_consignee');?>)</span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('BOTH','<?=$szBookingRandomNum?>')" class="orange-button1"><span><?=t($t_base.'title/y1_box_option_C');?></span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:30%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0)" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
			<?php
		}
	}
	else
	{
		?>
			<ol>
				<li><?=t($t_base.'title/y1_box_title');?>?</li>
			</ol>
			</div> <!-- closing div for requirement-content -->
			<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('SHIPPER','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/y1_box_option_A');?> <?=$originCountryText?> (<?=t($t_base.'title/the_shipper');?>)</span></a></p>
			<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('CONSIGNEE','<?=$szBookingRandomNum?>')" class="orange-button1"><span><?=t($t_base.'title/y1_box_option_B');?> <?=$destinationCountryText?> (<?=t($t_base.'title/the_consignee');?>)</span></a></p>
			<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('BOTH','<?=$szBookingRandomNum?>')" class="orange-button1"><span><?=t($t_base.'title/y1_box_option_C');?></span></a></p>
			<div class="process-bar">
				<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:30%;">&nbsp;</span><span class="right">&nbsp;</span></p>
				<a href="javascript:void(0)" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
			</div>
		<?php
	}
}
function display_country_drop_down_active($countryAry,$postSearchAry)
{
	$t_base_landing_page = "LandingPage/";
	$t_base = "LandingPage/";
	$szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
?>
<script type="text/javascript">
$().ready(function() {
		enable_next_step_country();
		<?php
			if($postSearchAry['idOriginCountry']>0 || (int)$postSearchAry['idDestinationCountry']>0)
			{
				?>
				Custom.init();
				<?php
			}
		?>
});
</script>
	<form action="" id="start_search_form" name="start_search_form" method="post">
					<p><?=t($t_base_landing_page.'title/select_country_default_text')?></p>
					<div class="clearfix nor-form">
						<p class="text"><?=t($t_base_landing_page.'fields/shipping_from')?></p>
						<p>
							<select name="landingPageAry[szOriginCountry]" tabindex="1" class="styled" onblur="enable_next_step_country();" id="szOriginCountry">
								<?
									if((int)$postSearchAry['idOriginCountry']<=0)
									{
								?>
									<option value=""><?=t($t_base.'fields/select_country');?></option>
								<?
									}	
								?>
								<?
								 	if(!empty($countryAry['fromCountry']))
								 	{
								 		foreach($countryAry['fromCountry'] as $countryArys)
								 		{
								 			?>
								 				<option value="<?=$countryArys['id']?>" <? if($postSearchAry['idOriginCountry']==$countryArys['id']){?> selected <? }?>><?=mb_substr($countryArys['szCountryName'],0,25,'UTF8')?></option>
								 			<?
								 		}
								 	}
								 ?>
							</select>
						</p>
						<p class="text" align="right"><?=t($t_base_landing_page.'fields/shipping_to')?></p>
						<p style="width:232px;float:right;">
							<select name="landingPageAry[szDestinationCountry]" tabindex="2" onblur="enable_next_step_country();" onkeyup="on_enter_key_press(event,'compare_validate_home_page_requirement');" class="styled" id="szDestinationCountry">
								<?
									if((int)$postSearchAry['idDestinationCountry']<=0)
									{
								?>
									<option value=""><?=t($t_base.'fields/select_country');?></option>
								<?
									}
								?>
								 <?
								 	if(!empty($countryAry['toCountry']))
								 	{
								 		foreach($countryAry['toCountry'] as $countryArys)
								 		{
								 			?>
								 				<option value="<?=$countryArys['id']?>" <? if($postSearchAry['idDestinationCountry']==$countryArys['id']){?> selected <? }?>><?=mb_substr($countryArys['szCountryName'],0,25,'UTF8')?></option>
								 			<?
								 		}
								 	}
								 ?>
							</select>
							<input name="landingPageAry[szBookingRandomNum]" type="hidden" value="<?=$szBookingRandomNum?>" id="szBookingRandomNum">
						</p>
					</div>
					<p align="right"><a href="javascript:void(0);" id="country_next_step_button" class="gray-button1"><span><?=t($t_base.'fields/next_step');?></span></a></p>
				</form>	
	<?php
}

function display_orgin_haulage_not_exist_popup($postSearchAry,$mode=false,$idFromPostcode=false,$szDestinationCity=false)
{
	$t_base = "LandingPage/";
	$idBooking = $postSearchAry['id'];
	$landing_page_url = __LANDING_PAGE_URL__ ;
	if($postSearchAry['iFromRequirementPage']==2)
	{
		$landing_page_url = __SELECT_SIMPLE_SEARCH_URL__ ;
	}
	$szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
	$kWHSSearch = new cWHSSearch();
	if($postSearchAry['idServiceType']== __SERVICE_TYPE_DTW__)
	{
		if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTW'))
		{
			$idExpactedServiceType = __SERVICE_TYPE_WTW__ ;
		}
		elseif($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iPTW'))
		{
			$idExpactedServiceType = __SERVICE_TYPE_PTW__ ;
		}
		else
		{
			$idExpactedServiceType = __SERVICE_TYPE_PTP__ ;
		}		
	}
	else if($postSearchAry['idServiceType']== __SERVICE_TYPE_DTD__)
	{
		if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTD'))
		{
			$idExpactedServiceType = __SERVICE_TYPE_WTD__ ;
		}
		elseif($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iPTD'))
		{
			$idExpactedServiceType = __SERVICE_TYPE_DTP__ ;
		}
		elseif($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTW'))
		{
			$idExpactedServiceType = __SERVICE_TYPE_WTW__ ;
		}
		else
		{
			$idExpactedServiceType = __SERVICE_TYPE_PTP__ ;
		}		
	}
	else if($postSearchAry['idServiceType']== __SERVICE_TYPE_DTP__)
	{
		if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTP'))
		{
			$idExpactedServiceType = __SERVICE_TYPE_WTP__ ;
		}
		else
		{
			$idExpactedServiceType = __SERVICE_TYPE_PTP__ ;
		}	
	}
	?>	
	<div id="popup-bg" class="white-bg"></div>
	<form id="no_services_form" method="post" name="no_services_form" action="">
		<div id="popup-container">
		<div class="standard-popup-x popup" >
		<p class="close-icon" align="right"><a heref="javascript:void(0);" onclick="closePopup('all_available_service');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
		<h5><?=t($t_base.'title/origin_haulage_not_exist_title')?> <?=!empty($postSearchAry['szOriginPostCode'])?$postSearchAry['szOriginPostCode']." ":''?><?=$postSearchAry['szOriginCity']?>, <?=t($t_base.'title/origin_haulage_not_exist_title_1')?></h5>
		
<?php
	
	$iLanguage = getLanguageId();
	$notify_me_class_1 = "class='orange-button1'";
	$originCountryAry = array();
	$kWHSSearch = new cWHSSearch();
	$originCountryAry = $kWHSSearch->getCountryLocationDetails($postSearchAry['idOriginCountry'],$iLanguage);
	$szOriginNotExactLocation = $originCountryAry['LocationDescriptionNotExact'] ;
	$szOriginExactLocation = $originCountryAry['LocationDescriptionExact'];
	
	$szOriginCityPostcode='';
	if(!empty($postSearchAry['szOriginPostCode']))
	{
		$szOriginCityPostcode = $postSearchAry['szOriginPostCode'];
	}	
	else if(!empty($postSearchAry['szOriginCity']))
	{
		$szOriginCityPostcode = $postSearchAry['szOriginCity'] ;
	}
?>
	<p class="heading-space" ><?=t($t_base.'title/no_service_popup_text_new');?> <!--<?=strtolower($szOriginExactLocation)?> <?=t($t_base.'title/origin_haulage_not_detail_text_2');?> <?=t($t_base.'title/in');?> <?=$postSearchAry['szOriginCountry']?>. <?=t($t_base.'title/origin_haulage_not_detail_text_3');?>--></p>
	<?php
	$notify_me_class = "class='gray-button1'";
	if($_SESSION['user_id']<=0)
	{
		$submit_button_text = t($t_base.'fields/submit');
		?>
		<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>

		<div class="oh name-space">
			<p class="fl-20"><?=t($t_base.'fields/name');?></p>
			<p class="fl-80">
				<input type="text" id="szCustomerName_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_NAME_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>');" onkeyup = "enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event);" name="addExpactedServiceAry[szCustomerName]" value="<?=$postSearchResult['szCustomerName']?>">
				<span id="szCustomerName_popup_error" class="red_text" style="display:none;float:right;"></span>
			</p>
		</div>
		<?php
	}
	else
	{
		$submit_button_text = t($t_base.'fields/yes_please');
		$email_loggedin_class="email-space";
		$kUser = new cUser();
		$kUser->getUserDetails($_SESSION['user_id']);
		if(empty($postSearchResult['szEmail']))
		{
			$postSearchResult['szCustomerPhone'] = $kUser->szPhoneNumber ;
			$postSearchResult['szEmail'] = $kUser->szEmail ;
			if(!empty($postSearchResult['szEmail']))
			{
				$notify_me_class = "class='orange-button1'";
				$notify_me_onclick = "onclick=\"update_expacted_services_requirement('".$landing_page_url."')\"";
			}
		}
		?>
		<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>

		<?php
	}
	?>
	<div class="oh <?=$email_loggedin_class;?>">
		<p class="fl-20"><?=t($t_base.'fields/email');?></p>
		<p class="fl-80">
			<input type="text" id="szCustomerEmail_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_EMAIL_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>');" onkeyup="enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event);" name="addExpactedServiceAry[szEmail]" value="<?=$postSearchResult['szEmail']?>">
			<span id="szCustomerEmail_popup_error" class="red_text" style="display:none;float:right;"></span>
		</p>
	</div>
	<div class="oh" style="margin-top:-5px;">
		<p class="fl-20"><?=t($t_base.'fields/phone');?></p>
		<p class="fl-80">
			<input type="text" id="szCustomerPhone_popup" name="addExpactedServiceAry[szCustomerPhone]" value="<?=$postSearchResult['szCustomerPhone']?>">
		</p>
	</div>
	<div class="new-button-container-topgap">
	<p align="center" class="button-container">	
		<input type="hidden" name="addExpactedServiceAry[idBooking]" value="<?=$idBooking?>">
		<input type="hidden" name="addExpactedServiceAry[idExpactedService]" value="<?=$idExpactedServiceType?>">
		<input type="hidden" name="addExpactedServiceAry[idNotificationType]" value="<?=__NOTIFICATION_TYPE_NO_DELIVERY_TO_DESTINATION_COUNTRY___?>">
		<input type="hidden" name="addExpactedServiceAry[idServiceType]" id="idServiceType" value="<?=$postSearchAry['idServiceType']?>">
		
		<a href="javascript:void(0);" <?=$notify_me_class?> <?=$notify_me_onclick?>  id="notify_me_button"><span><?=$submit_button_text;?></span></a>
	</p>
	<p align="center" class="button-container">
		<a href="javascript:void(0);" class="orange-button1" onclick="back_to_city_post_code_box('szOriginCityPostcode','<?=$szOriginCityPostcode?>')" ><span><?=t($t_base.'fields/try_anothor_postcode');?> <?=strtoupper($szOriginExactLocation)?></span></a>
	</p>
	<p align="center" class="button-container">
	<?php
		if($mode =='VALIDATE_POSTCODE_AFTER_HAULAGE_POPUP')
		{	
	?>
			<a href="javascript:void(0);" <?=$notify_me_class_1?> onclick="update_booking_service_type_requirement('<?=$idExpactedServiceType?>','<?=$szBookingRandomNum?>','<?=$mode?>','<?=$idFromPostcode?>','<?=$szDestinationCity?>','ORIGIN');" id="notify_me_button_1"><span><?=t($t_base.'fields/continue_button');?> <?=$postSearchAry['szOriginCountry']?></span></a>		
	<?php
		}
		else
		{
	?>
			<a href="javascript:void(0);" <?=$notify_me_class_1?> onclick="update_booking_service_type('<?=$idExpactedServiceType?>','<?=$szBookingRandomNum?>','SERVICE_TYPE','','UPDATE_SERVICE');" id="notify_me_button_1"><span><?=t($t_base.'fields/continue_button');?> <?=$postSearchAry['szOriginCountry']?></span></a>
	<?php
		}
	?>		
		
	</p>
	</div>
</div>		
</div>
</form>
	<?php
}
function display_destination_haulage_not_exist_popup($postSearchAry)
{
	$t_base = "LandingPage/";
	$idBooking = $postSearchAry['id'];
	$landing_page_url = __LANDING_PAGE_URL__ ;
	$szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
	if($postSearchAry['iFromRequirementPage']==2)
	{
		$landing_page_url = __SELECT_SIMPLE_SEARCH_URL__ ;
	}
	$kWHSSearch = new cWHSSearch();
	if($postSearchAry['idServiceType']== __SERVICE_TYPE_DTD__)
	{
		if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iDTW'))
		{
			$idExpactedServiceType = __SERVICE_TYPE_DTW__ ;
		}
		elseif($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iDTP'))
		{
			$idExpactedServiceType = __SERVICE_TYPE_DTP__ ;
		}
		elseif($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTW'))
		{
			$idExpactedServiceType = __SERVICE_TYPE_WTW__ ;
		}
		else
		{
			$idExpactedServiceType = __SERVICE_TYPE_PTP__ ;
		}		
	}
	else if($postSearchAry['idServiceType']== __SERVICE_TYPE_WTD__)
	{
		if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTW'))
		{
			$idExpactedServiceType = __SERVICE_TYPE_WTW__ ;
		}
		elseif($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTP'))
		{
			$idExpactedServiceType = __SERVICE_TYPE_WTP__ ;
		}
		else
		{
			$idExpactedServiceType = __SERVICE_TYPE_PTP__ ;
		}		
	}
	else if($postSearchAry['idServiceType']== __SERVICE_TYPE_PTD__)
	{
		if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iPTW'))
		{
			$idExpactedServiceType = __SERVICE_TYPE_PTW__ ;
		}
		else
		{
			$idExpactedServiceType = __SERVICE_TYPE_PTP__ ;
		}	
	}	
	$iLanguage = getLanguageId();
	$destinationCountryAry = array();
	$destinationCountryAry = $kWHSSearch->getCountryLocationDetails($postSearchAry['idDestinationCountry'],$iLanguage);
	$szDestinationNotExactLocation = $destinationCountryAry['LocationDescriptionNotExact'] ;
	$szDestinationExactLocation = $destinationCountryAry['LocationDescriptionExact'] ;
	
	$szOriginCityPostcode='';
	if(!empty($postSearchAry['szOriginPostCode']))
	{
		$szOriginCityPostcode = $postSearchAry['szOriginPostCode'];
	}	
	else if(!empty($postSearchAry['szOriginCity']))
	{
		$szOriginCityPostcode = $postSearchAry['szOriginCity'] ;
	}
	
	$szDestinationCityPostcode='';
	if(!empty($postSearchAry['szDestinationPostCode']))
	{
		$szDestinationCityPostcode = $postSearchAry['szDestinationPostCode'];
	}	
	else if(!empty($postSearchAry['szDestinationPostCode']))
	{
		$szDestinationCityPostcode = $postSearchAry['szDestinationPostCode'] ;
	}
?>	
	<div id="popup-bg" class="white-bg"></div>
	<form id="no_services_form" method="post" name="no_services_form" action="">
		<div id="popup-container">
		<div class="standard-popup-x popup" >
		<p class="close-icon" align="right"><a heref="javascript:void(0);" onclick="closePopup('all_available_service');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
		<h5><?=t($t_base.'title/destination_haulage_not_exist_title')?> <?=!empty($postSearchAry['szDestinationPostCode'])?$postSearchAry['szDestinationPostCode']." ":''?><?=$postSearchAry['szDestinationCity']?>, <?=t($t_base.'title/destination_haulage_not_exist_title_1')?></h5>
		
	<p class="heading-space"><?=t($t_base.'title/no_service_popup_text_new');?> <!--<?=strtolower($szDestinationExactLocation)?> <?=t($t_base.'title/destination_haulage_not_detail_text_2');?> <?=t($t_base.'title/in');?> <?=$postSearchAry['szDestinationCountry']?>. <?=t($t_base.'title/destination_haulage_not_detail_text_3');?>--></p>
	<?php
		$notify_me_class = "class='gray-button1'";
		if($_SESSION['user_id']<=0)
		{
			$submit_button_text = t($t_base.'fields/submit');
			?>
			<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>
	
			<div class="oh name-space">
				<p class="fl-20"><?=t($t_base.'fields/name');?></p>
				<p class="fl-80">
					<input type="text" id="szCustomerName_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_NAME_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>');" onkeyup = "enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event);" name="addExpactedServiceAry[szCustomerName]" value="<?=$postSearchResult['szCustomerName']?>">
					<span id="szCustomerName_popup_error" class="red_text" style="display:none;float:right;"></span>
				</p>
			</div>
			<?php
		}
		else
		{
			$submit_button_text = t($t_base.'fields/yes_please');
			$email_loggedin_class="email-space";
			$kUser = new cUser();
			$kUser->getUserDetails($_SESSION['user_id']);
			if(empty($postSearchResult['szEmail']))
			{
				$postSearchResult['szCustomerPhone'] = $kUser->szPhoneNumber ;
				$postSearchResult['szEmail'] = $kUser->szEmail ;
				if(!empty($postSearchResult['szEmail']))
				{
					$notify_me_class = "class='orange-button1'";
					$notify_me_onclick = "onclick=\"update_expacted_services_requirement('".$landing_page_url."')\"";
				}
			}
			?>
			<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>
	
			<?
		}
		?>
		<div class="oh <?=$email_loggedin_class;?>">
			<p class="fl-20"><?=t($t_base.'fields/email');?></p>
			<p class="fl-80">
				<input type="text" id="szCustomerEmail_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_EMAIL_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>');" onkeyup="enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event);" name="addExpactedServiceAry[szEmail]" value="<?=$postSearchResult['szEmail']?>">
				<span id="szCustomerEmail_popup_error" class="red_text" style="display:none;float:right;"></span>
			</p>
		</div>
		<div class="oh" style="margin-top:-5px;">
			<p class="fl-20"><?=t($t_base.'fields/phone');?></p>
			<p class="fl-80">
				<input type="text" id="szCustomerPhone_popup" name="addExpactedServiceAry[szCustomerPhone]" value="<?=$postSearchResult['szCustomerPhone']?>">
			</p>
		</div>
		
		
<?php
	
	$notify_me_class_1 = "class='orange-button1'";
?>
	<div class="new-button-container-topgap">
	<p align="center" class="button-container">	
		<input type="hidden" name="addExpactedServiceAry[idBooking]" value="<?=$idBooking?>">
		<input type="hidden" name="addExpactedServiceAry[idExpactedService]" value="<?=$idExpactedServiceType?>">
		<input type="hidden" name="addExpactedServiceAry[idNotificationType]" value="<?=__NOTIFICATION_TYPE_NO_DELIVERY_TO_DESTINATION_COUNTRY___?>">
		<input type="hidden" name="addExpactedServiceAry[idServiceType]" id="idServiceType" value="<?=$postSearchAry['idServiceType']?>">
		
		<a href="javascript:void(0);" <?=$notify_me_class?> <?=$notify_me_onclick?>  id="notify_me_button"><span><?=$submit_button_text;?></span></a>
	</p>	
	<p align="center" class="button-container">		
		<a href="javascript:void(0);" class="orange-button1" onclick="back_to_city_post_code_box('szDestinationCityPostcode','<?=$szOriginCityPostcode?>','<?=$szDestinationCityPostcode?>')" ><span><?=t($t_base.'fields/try_anothor_postcode');?> <?=strtoupper($szDestinationExactLocation)?></span></a>
	</p>
	<p align="center" class="button-container">
		<a href="javascript:void(0);" <?=$notify_me_class_1?> onclick="update_booking_service_type('<?=$idExpactedServiceType?>','<?=$szBookingRandomNum?>','SERVICE_TYPE','','UPDATE_SERVICE');" id="notify_me_button"><span><?=t($t_base.'fields/continue_button_delivery');?> <?=$postSearchAry['szDestinationCountry']?></span></a>
	</p>
	</div>
</div>		
</div>
</form>
	<?php
}
function display_country_change_popup($postSearchAry,$szBookingRandomNum)
{
	$t_base = "LandingPage/";
	?>
	<div id="popup-bg" class="white-bg"></div>
	<form id="no_services_form" method="post" name="no_services_form" action="">
		<div id="popup-container">
		<div class="standard-popup-z popup" >
		<p class="close-icon" align="right"><a heref="javascript:void(0);" onclick="closePopup('all_available_service');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
		<h5><?=t($t_base.'title/change_country')?></h5>
		<p class="heading-space" ><?=t($t_base.'title/change_country_popup_text_1');?></p>
		<p><?=t($t_base.'title/change_country_popup_text_2');?></p>	
	<p align="center" class="small-popup-doublebutton">		
		<a href="javascript:void(0);" class="orange-button1" onclick="showHide('all_available_service')" ><span><?=t($t_base.'fields/cancel');?></span></a>
		<a href="javascript:void(0);" class="orange-button1 right-button" onclick="display_requirement_edit_box('COUNTRY_BOX_EDIT','<?=$szBookingRandomNum?>')" id="notify_me_button"><span><?=t($t_base.'fields/yes_button');?></span></a>
	</p>
</div>		
</div>
	<?
}
function display_service_change_popup($postSearchAry,$szBookingRandomNum)
{
	$t_base = "LandingPage/";
	?>
	<div id="popup-bg" class="white-bg"></div>
	<form id="no_services_form" method="post" name="no_services_form" action="">
		<div id="popup-container">
		<div class="standard-popup-z popup" >
		<p class="close-icon" align="right"><a heref="javascript:void(0);" onclick="closePopup('all_available_service');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
		<h5><?=t($t_base.'title/change_transportation')?></h5>
		<p class="heading-space" ><?=t($t_base.'title/change_transportation_detail_text');?></p>
		<p><?=t($t_base.'title/change_transportation_detail_text_2');?></p>
	<p align="center" class="small-popup-doublebutton">		
		<a href="javascript:void(0);" class="orange-button1" onclick="showHide('all_available_service')" ><span><?=t($t_base.'fields/cancel');?></span></a>
		<a href="javascript:void(0);" class="orange-button1 right-button" onclick="display_requirement_edit_box('SERVICE_TYPE_EDIT','<?=$szBookingRandomNum?>')" id="notify_me_button"><span><?=t($t_base.'fields/yes_button');?></span></a>
	</p>
</div>		
</div>
	<?
}

function display_city_postcode_change_popup($postSearchAry,$szBookingRandomNum)
{
	$t_base = "LandingPage/";
	?>
	<div id="popup-bg" class="white-bg"></div>
	<form id="no_services_form" method="post" name="no_services_form" action="">
		<div id="popup-container">
		<div class="standard-popup-z popup" >
		<p class="close-icon" align="right"><a heref="javascript:void(0);" onclick="closePopup('all_available_service');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
		<h5><?=t($t_base.'title/change_city_postcode')?></h5>
		<p class="heading-space" ><?=t($t_base.'title/change_city_postcode_detail_text');?></p>
	<p align="center" class="small-popup-doublebutton">		
		<a href="javascript:void(0);" class="orange-button1" onclick="showHide('all_available_service')" ><span><?=t($t_base.'fields/cancel');?></span></a>
		<a href="javascript:void(0);" class="orange-button1 right-button" onclick="display_requirement_edit_box('CITY_POSTCODE_EDIT','<?=$szBookingRandomNum?>')" id="notify_me_button"><span><?=t($t_base.'fields/yes_button');?></span></a>
	</p>

</div>		
</div>
	<?
}
function display_cargo_timing_change_popup($postSearchAry,$szBookingRandomNum)
{
	$t_base = "LandingPage/";
	?>
	<div id="popup-bg" class="white-bg"></div>
	<form id="no_services_form" method="post" name="no_services_form" action="">
		<div id="popup-container">
		<div class="standard-popup-z popup" >
		<p align="right" class="close-icon"><a heref="javascript:void(0);" onclick="closePopup('all_available_service');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
		<h5><?=t($t_base.'title/change_cargo_timing')?></h5>
		<p class="heading-space" ><?=t($t_base.'title/change_cargo_timing_detail_text');?></p>
	<p align="center" class="small-popup-doublebutton">		
		<a href="javascript:void(0);" class="orange-button1" onclick="showHide('all_available_service')" ><span><?=t($t_base.'fields/cancel');?></span></a>
		<a href="javascript:void(0);" class="orange-button1 right-button" onclick="display_requirement_edit_box('CARGO_TIMING_EDIT','<?=$szBookingRandomNum?>')" id="notify_me_button"><span><?=t($t_base.'fields/yes_button');?></span></a>
	</p>

</div>		
</div>
	<?
}
function display_custom_clearance_change_popup($postSearchAry,$szBookingRandomNum)
{
	$t_base = "LandingPage/";
	?>
	<div id="popup-bg" class="white-bg"></div>
	<form id="no_services_form" method="post" name="no_services_form" action="">
		<div id="popup-container">
		<div class="standard-popup-z popup" >
		<p class="close-icon" align="right"><a heref="javascript:void(0);" onclick="closePopup('all_available_service');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
		<h5><?=t($t_base.'title/change_CC_popup_title')?></h5>
		<p class="heading-space" ><?=t($t_base.'title/change_CC_popup_detail_text');?></p>
	<p align="center" class="small-popup-doublebutton">	
		<a href="javascript:void(0);" class="orange-button1" onclick="showHide('all_available_service')" ><span><?=t($t_base.'fields/cancel');?></span></a>
		<a href="javascript:void(0);" class="orange-button1 right-button" onclick="display_requirement_edit_box('CUSTOM_CLEARANCE_EDIT','<?=$szBookingRandomNum?>')" id="notify_me_button"><span><?=t($t_base.'fields/yes_button');?></span></a>
	</p>

</div>		
</div>
	<?
}
function checkForgotPasswordUpdated()
{	
	$t_base="passwordconfirm/";
?>
<div id="update_password_div" style="display:block;">
	<div id="popup-bg"></div>
	<div id="popup-container">
		<div class="popup-forgot-password popup"> 						
		<div id="Error-log"></div>
		<form method="post" id="chagePassword">
			<h5><b><?=t($t_base."messages/select_new_password");?></b></h5>
			<label class="ui-full-fields">
				<span class="field-name"><?=t($t_base."messages/new_password")?></span>
				<span class="field-container">	
					<input type="password" id="szNewPassword" name="update[szNewPassword]" value="" /> 
				</span>
			</label>
			<label class="ui-full-fields">
				<span class="field-name"><?=t($t_base."messages/confirm_password")?></span>
				<span class="field-container">	
					<input type="password" onkeyup="on_enter_key_press(event,'submitPasswordUpdate');" name="update[szConPassword]" value="" /> 
				</span>
			</label>
			</form>
			<br />
			<div align ="center">
				<a class="button1" onclick="submitPasswordUpdate()"><span><?=t($t_base.'fields/ok')?></span></a>
			</div>
		</div>
	</div>
</div>
<?
}

function display_OP1_OP2_popup($postSearchAry,$szBookingRandomNum,$iOriginDestination)
{
	$t_base = "LandingPage/";
	$idBooking = $postSearchAry['id'];
	$landing_page_url = __LANDING_PAGE_URL__ ;
	$szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
	$kWHSSearch = new cWHSSearch();
	$popup_name = '';
	if($iOriginDestination==2)
	{
		if($postSearchAry['idServiceType']== __SERVICE_TYPE_WTD__)
		{
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTW'))
			{
				$idExpactedServiceType = __SERVICE_TYPE_WTW__ ;
			}
			elseif($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTP'))
			{
				$idExpactedServiceType = __SERVICE_TYPE_WTP__ ;
			}
			else
			{
				$idExpactedServiceType = __SERVICE_TYPE_PTP__ ;
			}	
			//$popup_name = "OP2";	
		}
		else if($postSearchAry['idServiceType']== __SERVICE_TYPE_PTD__)
		{
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iPTW'))
			{
				$idExpactedServiceType = __SERVICE_TYPE_PTW__ ;
			}
			else
			{
				$idExpactedServiceType = __SERVICE_TYPE_PTP__ ;
			}	
			//$popup_name = "OP2";
		}
		else if($postSearchAry['idServiceType']== __SERVICE_TYPE_DTD__)
		{
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iDTW'))
			{
				$idExpactedServiceType = __SERVICE_TYPE_DTW__ ;
			}
			elseif($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iDTP'))
			{
				$idExpactedServiceType = __SERVICE_TYPE_DTP__ ;
			}
			elseif($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTW'))
			{
				$idExpactedServiceType = __SERVICE_TYPE_WTW__ ;
			}
			else
			{
				$idExpactedServiceType = __SERVICE_TYPE_PTP__ ;
			}		
		}
	}
	else
	{ 
		if($postSearchAry['idServiceType']== __SERVICE_TYPE_DTW__)
		{
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTW'))
			{
				$idExpactedServiceType = __SERVICE_TYPE_WTW__ ;
			}
			elseif($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iPTW'))
			{
				$idExpactedServiceType = __SERVICE_TYPE_PTW__ ;
			}
			else
			{
				$idExpactedServiceType = __SERVICE_TYPE_PTP__ ;
			}	
			//$popup_name = "OP1";	
		}
		else if($postSearchAry['idServiceType']== __SERVICE_TYPE_DTP__)
		{
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTP'))
			{
				$idExpactedServiceType = __SERVICE_TYPE_WTP__ ;
			}
			else
			{
				$idExpactedServiceType = __SERVICE_TYPE_PTP__ ;
			}	
			//$popup_name = "OP1";
		}
		else if($postSearchAry['idServiceType']== __SERVICE_TYPE_DTD__)
		{
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTD'))
			{
				$idExpactedServiceType = __SERVICE_TYPE_WTD__ ;
			}
			elseif($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iPTD'))
			{
				$idExpactedServiceType = __SERVICE_TYPE_DTP__ ;
			}
			elseif($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTW'))
			{
				$idExpactedServiceType = __SERVICE_TYPE_WTW__ ;
			}
			else
			{
				$idExpactedServiceType = __SERVICE_TYPE_PTP__ ;
			}		
		}
	}
	
	?>	
	<div id="popup-bg" class="white-bg"></div>
	<form id="no_services_form" method="post" name="no_services_form" action="">
		<div id="popup-container">
		<div class="standard-popup popup" >
		<p class="close-icon" align="right"><a heref="javascript:void(0);" onclick="closePopup('all_available_service');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
		<!-- <h3><?=$popup_name?></h3>  -->
<?php
	
	$notify_me_class = "class='orange-button1'";
	$kWHSSearch = new cWHSSearch();
	$iLanguage = getLanguageId();
	if($iOriginDestination ==1)
	{
		$originCountryAry = array();
		$originCountryAry = $kWHSSearch->getCountryLocationDetails($postSearchAry['idOriginCountry'],$iLanguage);
		$szOriginNotExactLocation = $originCountryAry['LocationDescriptionNotExact'] ;
		$szOriginExactLocation = $originCountryAry['LocationDescriptionExact'] ;
		
		?>
		<h5><?=$szOriginExactLocation?> <?=t($t_base.'title/for_pick_up_in');?> <?=$postSearchAry['szOriginCountry']?></h5>
		<p><?=t($t_base.'title/op1_details_text_1');?> <?=strtolower($szOriginExactLocation)?> <?=t($t_base.'title/for_pick_up_in');?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/op1_details_text_2');?> <?=strtolower($szOriginExactLocation)?> <?=t($t_base.'title/op1_details_text_3');?></p>
		<?
	}
	else
	{
		
		$destinationCountryAry = array();
		$destinationCountryAry = $kWHSSearch->getCountryLocationDetails($postSearchAry['idDestinationCountry'],$iLanguage);
		$szDestinationNotExactLocation = $destinationCountryAry['LocationDescriptionNotExact'] ;
		$szDestinationExactLocation = $destinationCountryAry['LocationDescriptionExact'] ;
		?>
		<h5><?=$szDestinationExactLocation?> <?=t($t_base.'title/for_delivery_in');?> <?=$postSearchAry['szDestinationCountry']?></h5>
		<p class="heading-space" ><?=t($t_base.'title/op1_details_text_1');?> <?=strtolower($szDestinationExactLocation)?> <?=t($t_base.'title/for_delivery_in');?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/op1_details_text_2');?> <?=strtolower($szDestinationExactLocation)?> <?=t($t_base.'title/op2_details_text_3');?></p>
		<?
	}
?>
	<br/>
	<?
		if($iOriginDestination == 1)
		{
	?>
			<p align="center" class="button-container">
				<a href="javascript:void(0);" <?=$notify_me_class?> onclick="update_booking_service_type('<?=$idExpactedServiceType?>','<?=$szBookingRandomNum?>','SERVICE_TYPE','','DON_NOT_KNOW_POPUP');" id="notify_me_button"><span><?=t($t_base.'fields/continue_button');?> <?=$postSearchAry['szOriginCountry']?></span></a>
			</p>
	<?
		}
		else
		{	
	?>
			<p align="center" class="button-container">
				<a href="javascript:void(0);" <?=$notify_me_class?> onclick="update_booking_service_type('<?=$idExpactedServiceType?>','<?=$szBookingRandomNum?>','SERVICE_TYPE','','DON_NOT_KNOW_POPUP');" id="notify_me_button"><span><?=t($t_base.'fields/continue_button_delivery');?> <?=$postSearchAry['szDestinationCountry']?></span></a>
			</p>
	<?
		}
	?>
</div>		
</div>
</form>
	<?
}
function display_service_type_canvas($postSearchAry,$idServiceType,$ret_img_name=false,$img_q_count=false)
{
	//$idServiceType = $postSearchAry['idServiceType'];
	$t_base_landing_page = "LandingPage/";
	$szImageName = __BASE_STORE_IMAGE_URL__."/";
	
	$iLanguage = getLanguageId();
	
	$szImageInitial = "transportation";
	if($iLanguage==__LANGUAGE_ID_DANISH__)
	{
		$szImageInitial .= "-da"; 
	}
	
	if($idServiceType == __SERVICE_TYPE_DTD__)	//DTD
	{
		$szImageName .= $szImageInitial."-DTD.png";
	}
	else if($idServiceType == __SERVICE_TYPE_DTP__)	//DTD
	{
		if(!empty($img_q_count))
		{
			$szImageName .= $szImageInitial."-DTP-".$img_q_count.".png";
		}
		else {
			$szImageName .= $szImageInitial."-DTP.png";
		}
	}
	else if($idServiceType == __SERVICE_TYPE_DTW__)	//DTD
	{
		$szImageName .= $szImageInitial."-DTW.png";
	}
	else if($idServiceType == __SERVICE_TYPE_WTW__)	//DTD
	{
		$szImageName .= $szImageInitial."-WTW.png";
	}
	else if($idServiceType == __SERVICE_TYPE_WTP__)	//DTD
	{
		if(!empty($img_q_count))
		{
			$szImageName .= $szImageInitial."-WTP-".$img_q_count.".png";
		}
		else {
		
			$szImageName .= $szImageInitial."-WTP.png";
		}
	}
	else if($idServiceType == __SERVICE_TYPE_WTD__)	//DTD
	{
		$szImageName .= $szImageInitial."-WTD.png";
	}
	else if($idServiceType == __SERVICE_TYPE_PTP__)	//DTD
	{
		if(!empty($img_q_count))
		{
			$szImageName .= $szImageInitial."-PTP-".$img_q_count.".png";
		}
		else 
		{
			$szImageName .= $szImageInitial."-PTP.png";
		}
	}
	elseif($idServiceType == __SERVICE_TYPE_PTD__)	//DTD
	{
		if(!empty($img_q_count))
		{
			$szImageName .= $szImageInitial."-PTD-".$img_q_count.".png";
		}
		else
		{		
			$szImageName .= $szImageInitial."-PTD.png";
		}
	}
	else if($idServiceType == __SERVICE_TYPE_PTW__)	//DTD
	{
		if(!empty($img_q_count))
		{
			$szImageName .= $szImageInitial."-PTW-".$img_q_count.".png";
		}
		else
		{
			$szImageName .= $szImageInitial."-PTW.png";
		}
	}
	
	if($ret_img_name)
	{
		return $szImageName ;
	}
	else
	{
		
		if($postSearchAry['iShipperConsignee'] == 1)
		{
			$szOriginText = t($t_base_landing_page.'title/shipper')." (".t($t_base_landing_page.'title/you').") ".t($t_base_landing_page.'title/in')." ".$postSearchAry['szOriginCountry'];
			$szDestinationText = t($t_base_landing_page.'title/consignee_in')." ".$postSearchAry['szDestinationCountry'];
		}
		else if($postSearchAry['iShipperConsignee'] == 2)
		{
			$szOriginText = t($t_base_landing_page.'title/shipper_in')." ".$postSearchAry['szOriginCountry'];
			$szDestinationText = t($t_base_landing_page.'title/consignee')." (".t($t_base_landing_page.'title/you').") ".t($t_base_landing_page.'title/in')." ".$postSearchAry['szDestinationCountry'];
		}
		else
		{
			$szOriginText = t($t_base_landing_page.'title/shipper_in')." ".$postSearchAry['szOriginCountry'];
			$szDestinationText = t($t_base_landing_page.'title/consignee_in')." ".$postSearchAry['szDestinationCountry'];
		}
		
		?>	
		<div class="image-text-div">
		<div class="image-origin-text"><?=$szOriginText?></div>
		<div class="image-dest-text"><?=$szDestinationText?></div>
		</div>
		<div class="service-image">
		<img src="<?=$szImageName?>" alt="<?=$alt_text?>" title="<?=$alt_text?>" >	
		</div>
		
<?
	}
}

function display_service_type_image_search_service($postSearchAry,$idServiceType,$alt_text,$szImageInitial=false,$ret_img_name=false)
{
	$t_base_landing_page = "LandingPage/";
	$szImageName = __BASE_STORE_IMAGE_URL__."/";
		
	if(empty($szImageInitial))
	{
		$szImageInitial = "search-service";
	}
	$iLanguage = getLanguageId();
	if($iLanguage==__LANGUAGE_ID_DANISH__)
	{
		$szImageInitial .= "-da"; 
	}
	if($idServiceType == __SERVICE_TYPE_DTD__)	//DTD
	{
		$szImageName .= $szImageInitial."-DTD.png";
	}
	elseif($idServiceType == __SERVICE_TYPE_DTP__)	//DTD
	{
		$szImageName .= $szImageInitial."-DTP.png";
	}
	elseif($idServiceType == __SERVICE_TYPE_DTW__)	//DTD
	{
		$szImageName .= $szImageInitial."-DTW.png";
	}
	elseif($idServiceType == __SERVICE_TYPE_WTW__)	//DTD
	{
		$szImageName .= $szImageInitial."-WTW.png";
	}
	elseif($idServiceType == __SERVICE_TYPE_WTP__)	//DTD
	{
		$szImageName .= $szImageInitial."-WTP.png";
	}
	elseif($idServiceType == __SERVICE_TYPE_WTD__)	//DTD
	{
		$szImageName .= $szImageInitial."-WTD.png";
	}
	elseif($idServiceType == __SERVICE_TYPE_PTP__)	//DTD
	{
		$szImageName .= $szImageInitial."-PTP.png";
	}
	elseif($idServiceType == __SERVICE_TYPE_PTD__)	//DTD
	{
		$szImageName .= $szImageInitial."-PTD.png";
	}
	elseif($idServiceType == __SERVICE_TYPE_PTW__)	//DTD
	{
		$szImageName .= $szImageInitial."-PTW.png";
	}
		
	if($ret_img_name)
	{
		return $szImageName;
	}
	else
	{
		?>
			<img id="search_service_image" src="<?=$szImageName?>" alt="<?=$alt_text?>" title="<?=$alt_text?>" >
		<?
	}
}

function display_service_not_found_seach_service_popup($postSearchAry,$idExpactedService,$kBooking=false,$request_ary=false,$iNewSearchPage=false)
{
	$t_base = "home/homepage/";
	$idBooking = $postSearchAry['id'];
	$szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
	
	$landing_page_url = __LANDING_PAGE_URL__ ;
	$requirement_page_url =  __REQUIREMENT_PAGE_URL__ ;
	if($postSearchAry['iFromRequirementPage']==2)
	{
		$landing_page_url = __SELECT_SIMPLE_SEARCH_URL__ ;
	} 
	?>	
	<script type="text/javascript">
		$().ready(function(){
			
			enable_notify_me_popup_search_service('<?=$_SESSION['user_id']?>','<?=$landing_page_url?>',window.event,'<?=$szBookingRandomNum?>','<?=$requirement_page_url?>');
			
		});
	</script>
	<div id="popup-bg" class="white-bg"></div>
	<form id="no_services_form" method="post" name="no_services_form" action="">
		<div id="popup-container">
		<div class="standard-popup-f popup">
		<p class="close-icon" align="right"><a heref="javascript:void(0);" onclick="closePopup('service_not_found');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
		<h5><?=t($t_base.'messages/no_service_pop_header')?></h5>
		<p class="heading-space"><?=t($t_base.'messages/no_service_pop_detail_text1');?></p>
		<p><?=t($t_base.'messages/no_service_pop_detail_text5');?></p>
		<p style="margin-bottom:15px;"><?=t($t_base.'messages/no_service_pop_detail_text6');?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'messages/to');?> <?=$postSearchAry['szDestinationCountry']?>.</p>
		<p class="clearfix"><input class="styled" type="checkbox" value="1" onclick = "enable_notify_me_popup_search_service('<?=$_SESSION['user_id']?>','<?=$landing_page_url?>',event,'<?=$szBookingRandomNum?>','<?=$requirement_page_url?>');" name="addExpactedServiceAry[iSendNotification]" id="iSendNotification_search_service_popup"> <?=t($t_base.'messages/no_service_pop_detail_text7');?></p>
<?php
	$notify_me_class = "class='gray-button1'";
	if($_SESSION['user_id']<=0)
	{
		?>
		<div class="oh name-space text-field-align">
			<p class="fl-20 text-field-align-name"><?=t($t_base.'fields/name');?></p>
			<p class="fl-80">
				<input type="text" id="szCustomerName_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_NAME_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>','SEARCH_SERVICE','<?=$landing_page_url?>','<?=$requirement_page_url?>');" onkeyup = "enable_notify_me_popup_search_service('<?=$_SESSION['user_id']?>','<?=$landing_page_url?>',event,'<?=$szBookingRandomNum?>','<?=$requirement_page_url?>');" name="addExpactedServiceAry[szCustomerName]" value="<?=$postSearchResult['szCustomerName']?>">
				<span id="szCustomerName_popup_error" class="red_text" style="display:none;float:right;vetical-align:bottom;"></span>
			</p>
		</div>
		<?
	}
	else
	{
		$email_loggedin_class="email-space";
		$kUser = new cUser();
		$kUser->getUserDetails($_SESSION['user_id']);
		if(empty($postSearchResult['szEmail']))
		{
			$postSearchResult['szEmail'] = $kUser->szEmail ;
			if(!empty($postSearchResult['szEmail']))
			{
				//$notify_me_class = "class='orange-button1'";
				//$notify_me_onclick = "onclick=\"update_expacted_services_requirement('".$landing_page_url."')\"";
			}
		}
	}
	?>
	<div class="oh <?=$email_loggedin_class;?> text-field-align">
		<p class="fl-20 text-field-align-name" style="vertical-align:bottom;"><?=t($t_base.'fields/email');?></p>
		<p class="fl-80">
			<input type="text" id="szCustomerEmail_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_EMAIL_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>','SEARCH_SERVICE','<?=$landing_page_url?>','<?=$requirement_page_url?>');" onkeyup="enable_notify_me_popup_search_service('<?=$_SESSION['user_id']?>','<?=$landing_page_url?>',event,'<?=$szBookingRandomNum?>','<?=$requirement_page_url?>');" name="addExpactedServiceAry[szEmail]" value="<?=$postSearchResult['szEmail']?>">
			<span id="szCustomerEmail_popup_error" class="red_text" style="display:none;float:right;"></span>
		</p>
		<input type="hidden" name="addExpactedServiceAry[idBooking]" value="<?=$idBooking?>">
		<input type="hidden" name="addExpactedServiceAry[idExpactedService]" value="<?=$idExpactedService?>">
		<input type="hidden" name="addExpactedServiceAry[type]" id="opration_type" value="">
		
		<input type="hidden" name="addExpactedServiceAry[szLandingPageUrl]" id="szLandingPageUrl" value="<?=$landing_page_url?>">
		<input type="hidden" name="addExpactedServiceAry[szReuirementPageUrl]" id="szReuirementPageUrl" value="<?=$requirement_page_url?>">
		
	</div>

	<p align="center" class="button-container">	
		<a href="javascript:void(0);" class="gray-button1" id="try_step_by_step_button" ><span><?=t($t_base.'messages/try_step_by_step');?></span></a>
	</p>
	<p align="center" class="button-container">	
		<a href="javascript:void(0);" class="gray-button1" id="go_back_change_button"><span><?=t($t_base.'messages/go_back_change_requirement');?></span></a>
	</p>
	<p align="center" class="button-container">	
		<a href="javascript:void(0);" class="gray-button1" id="make_new_search_button"><span><?=t($t_base.'messages/make_new_search');?></span></a>
	</p>
</div>		
</div>
</form>
	<?
}

function display_empty_postcode_popup($postSearchAry,$multiEmaptyPostcodeCityAry,$type,$szCity,$iDestinationFlag=false,$szDestinationCity=false)
{
	$t_base = "home/homepage/";
	$kBooking=new cBooking();
	$header_text = t($t_base.'fields/empty_postcode_origin');
	$from_postcode = false;
	if($type=='ORIGIN')
	{
		$header_text .= " ".t($t_base.'fields/origin');
	}			
	else if($type=='DESTINATION')
	{
		$header_text .=" ".t($t_base.'fields/destination');
	}
?>
	<script type="text/javascript">
		$("#boxscroll2").niceScroll({touchbehavior:false,cursorcolor:"#17375E",cursoropacitymax:0.7,cursorwidth:7,autohidemode:false}).show();
	</script>
	<div id="popup-bg" class="white-bg"></div>
	<form name="multi_empty_postcode_from_form" id="multi_empty_postcode_from_form" action="" method="post">
	<div id="popup-container">
	<div class="service-popup popup">		
	<p class="close-icon" align="right"><a heref="javascript:void(0);" onclick="closeScrollablePopup('all_available_service');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
	<h5><?=$header_text?></h5>
	<div id="popupError" class="errorBox" style="display:none;">
	</div>		
		<?
		if($type=='ORIGIN')
		{
			if(!empty($multiEmaptyPostcodeCityAry))
			{	
				?>
				<p><?=t($t_base.'messages/which_part_of');?> <?=$szCity?> <?=t($t_base.'messages/need_transportation');?></p>
				<div class="container2" id="boxscroll2">				
					<?
						foreach($multiEmaptyPostcodeCityAry as $key=>$fromRegions)
						{
					?>
						<p class="checkbox-ab"><input type="radio" onclick="enable_submit_button('multi_empty_postcode_from_form','multiEmptyPostcodeCityAry[szRegionFrom]')" name="multiEmptyPostcodeCityAry[szRegionFrom]" value="<?=$key?>"><?=$fromRegions?></p>
					<?
						}
				echo '</div>';
			}
		?>
			<input type="hidden" name="multiEmptyPostcodeCityAry[iRegionFrom]" value="1">
			<input type="hidden" name="multiEmptyPostcodeCityAry[szDestinationCity]" value="<?=$szDestinationCity?>">
			<?
		}
		else
		{
			if(!empty($multiEmaptyPostcodeCityAry))
			{	
				?>
				<p><?=t($t_base.'messages/which_part_of');?> <?=$szCity?> <?=t($t_base.'messages/need_transportation_to');?></p>
				<div class="container2" id="boxscroll2">				
					<?
						foreach($multiEmaptyPostcodeCityAry as $key=>$fromRegions)
						{
					?>
						<p class="checkbox-ab"><input type="radio" onclick="enable_submit_button('multi_empty_postcode_from_form','multiEmptyPostcodeCityAry[szRegionTo]')" name="multiEmptyPostcodeCityAry[szRegionTo]" value="<?=$key?>"><?=$fromRegions?></p>
					<?
						}
				echo '</div>';
			}
			?>
			<input type="hidden" name="multiEmptyPostcodeCityAry[iRegionTo]" value="1">
			<?
		}
	?>
	<input type="hidden" name="multiEmptyPostcodeCityAry[type]" value="<?=$type?>">
	<input type="hidden" name="multiEmptyPostcodeCityAry[iCheckDestination]" value="<?=$iDestinationFlag?>">	
	<p align="center">
		<input type="hidden" name="booking_key" value="<?=$postSearchAry['szBookingRandomNum']?>">
		<input type="hidden" name="multiEmptyPostcodeCityAry[szBookingRandomNum]" id="szBookingRandomNum" value="<?=$postSearchAry['szBookingRandomNum']?>">
		<a href="javascript:void(0);" id="submit_button" class="gray-button1">
		<span><?=t($t_base.'fields/select');?></span></a>
	</p>
	</div>
	</div>
</form>	
	<?
}

function display_partial_empty_postcode_popup($postSearchAry,$multiEmaptyPostcodeCityAry,$type,$szCity,$iDestinationFlag=false,$szDestinationCity=false,$szRegion1=false)
{	

	$t_base = "home/homepage/";
	$t_base_landing =  "LandingPage/";
	
	require_once (__APP_PATH_ROOT__ ."/inc/classes/pagination.class.php");
	
	$kBooking=new cBooking();
	$header_text = t($t_base.'fields/your_input');
	$from_postcode = false;
	$kWHSSearch = new cWHSSearch();
	$iLanguage = getLanguageId();
	if($type=='ORIGIN')
	{
		$originCountryAry = array();
		$originCountryAry = $kWHSSearch->getCountryLocationDetails($postSearchAry['idOriginCountry'],$iLanguage);
		$szNotExactLocation = $originCountryAry['LocationDescriptionNotExact'] ;
		$szExactLocation = $originCountryAry['LocationDescriptionExact'] ;
		
		
		$header_text .= " ".$postSearchAry['szOriginCountry'];
	}			
	else if($type=='DESTINATION')
	{
		$header_text .= " ".$postSearchAry['szDestinationCountry'];
		
		$destinationCountryAry = array();
		$destinationCountryAry = $kWHSSearch->getCountryLocationDetails($postSearchAry['idDestinationCountry'],$iLanguage);
		$szNotExactLocation = $destinationCountryAry['LocationDescriptionNotExact'] ;
		$szExactLocation = $destinationCountryAry['LocationDescriptionExact'] ;
	}
	$header_text .= " ".t($t_base.'fields/does_not_seem')." ".$szExactLocation ;
?>
	<script type="text/javascript">
		$("#boxscroll2").niceScroll({touchbehavior:false,cursorcolor:"#17375E",cursoropacitymax:0.7,cursorwidth:7,autohidemode:false}).show();
	</script>
	<div id="popup-bg" class="white-bg"></div>
	<form name="partial_empty_postcode_from_form" id="partial_empty_postcode_from_form" action="" method="post">
	<div id="popup-container">
	<div class="service-popup popup">		
	<p class="close-icon" align="right"><a heref="javascript:void(0);" onclick="closeScrollablePopup('all_available_service');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
	<h5><?=$header_text?></h5>
	<div id="popupError" class="errorBox" style="display:none;">
	
	</div>		
		<?
		$kWHSSearch = new cWHSSearch();
		if($type=='ORIGIN')
		{
			$cityPostcodeAry = array();
			$kConfig = new cConfig();
			//$cityPostcodeAry = $kConfig->check_city_country_requirement($szCity,$postSearchAry['idOriginCountry'],$from,true,false,$szRegion1);
			
			//$total = $kConfig->check_city_country_requirement($szCity,$postSearchAry['idOriginCountry'],false,false,true,$szRegion1);
			
			if(!empty($multiEmaptyPostcodeCityAry))
			{
				//$cityPostcodeAry = array_slice($multiEmaptyPostcodeCityAry,0,__POSTCODE_PER_PAGE_POSTCODE__);
				$postcode_counter = 0;
				
				//slicing array to avooid recall the above function 
				foreach($multiEmaptyPostcodeCityAry as $key=>$fromRegions)
				{
					$cityPostcodeAry[$key] = $fromRegions ;
					$postcode_counter++;
					if($postcode_counter==__POSTCODE_PER_PAGE_POSTCODE__)
					{
						break;
					}
				}
				$total = count($multiEmaptyPostcodeCityAry);
			}			
			$kPagination  = new Pagination($total,__POSTCODE_PER_PAGE_POSTCODE__,5);
			$kPagination->paginate();
			
			if($postSearchAry['idServiceType']== __SERVICE_TYPE_DTW__)
			{
				if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTW'))
				{
					$idExpactedServiceType = __SERVICE_TYPE_WTW__ ;
				}
				elseif($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iPTW'))
				{
					$idExpactedServiceType = __SERVICE_TYPE_PTW__ ;
				}
				else
				{
					$idExpactedServiceType = __SERVICE_TYPE_PTP__ ;
				}		
			}
			else if($postSearchAry['idServiceType']== __SERVICE_TYPE_DTP__)
			{
				if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTP'))
				{
					$idExpactedServiceType = __SERVICE_TYPE_WTP__ ;
				}
				else
				{
					$idExpactedServiceType = __SERVICE_TYPE_PTP__ ;
				}	
			}
			else if($postSearchAry['idServiceType']== __SERVICE_TYPE_DTD__)
			{
				if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTD'))
				{
					$idExpactedServiceType = __SERVICE_TYPE_WTD__ ;
				}
				elseif($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iPTD'))
				{
					$idExpactedServiceType = __SERVICE_TYPE_DTP__ ;
				}
				elseif($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTW'))
				{
					$idExpactedServiceType = __SERVICE_TYPE_WTW__ ;
				}
				else
				{
					$idExpactedServiceType = __SERVICE_TYPE_PTP__ ;
				}		
			}
			
			if(!empty($cityPostcodeAry))
			{	
				?>
				<p><?=t($t_base.'messages/xp1_detail_text_1');?> <?=$szExactLocation?> <?=t($t_base.'messages/xp1_detail_text_2');?> <?=$postSearchAry['szOriginCountry']?>. <?=t($t_base.'messages/xp1_detail_text_3');?></p>
				<div id="data_container">
				<div class="container2" id="boxscroll2">				
					<?
						foreach($cityPostcodeAry as $key=>$fromRegions)
						{
					?>
						<p class="checkbox-ab"><input type="radio" onclick="enable_submit_button('partial_empty_postcode_from_form','partialEmptyPostcodeCityAry[szRegionFrom]')" name="partialEmptyPostcodeCityAry[szRegionFrom]" value="<?=$key?>"><?=$fromRegions?></p>
					<?
						}
				echo '</div>';
			}
			echo '<div class="page_nav" align="right">';
			if($kPagination->links_per_page>1)
			{
				$kPagination->ajax_function = "show_next_page";
				$kPagination->append = "'MULTI_PARTIAL_EMPTY_POSTCODE_NEXT_PAGE'";
				$kPagination->iIncludeDots = 1 ;
				echo $kPagination->renderFullNav();
			}
			echo '</div>';
		?>
			</div>
			<input type="hidden" name="partialEmptyPostcodeCityAry[iRegionFrom]" value="1">
			<input type="hidden" name="partialEmptyPostcodeCityAry[szDestinationCity]" value="<?=$szDestinationCity?>">
			<?
			$button_text = t($t_base_landing.'fields/continue_button')." ".$postSearchAry['szOriginCountry'];
		}
		else
		{
			$cityPostcodeAry = array();
			$kConfig = new cConfig();
			//$cityPostcodeAry = $kConfig->check_city_country_requirement($szCity,$postSearchAry['idDestinationCountry'],$from,true,false,$szRegion1);			
			//$total = $kConfig->check_city_country_requirement($szCity,$postSearchAry['idDestinationCountry'],false,false,true,$szRegion1);
			
			if(!empty($multiEmaptyPostcodeCityAry))
			{
				//$cityPostcodeAry = array_slice($multiEmaptyPostcodeCityAry,0,__POSTCODE_PER_PAGE_POSTCODE__);
				$postcode_counter = 0;
				
				//slicing array to avooid recall the above function 
				foreach($multiEmaptyPostcodeCityAry as $key=>$fromRegions)
				{
					$cityPostcodeAry[$key] = $fromRegions ;
					$postcode_counter++;
					if($postcode_counter==__POSTCODE_PER_PAGE_POSTCODE__)
					{
						break;
					}
				}
				$total = count($multiEmaptyPostcodeCityAry);
			}
			$kPagination  = new Pagination($total,__POSTCODE_PER_PAGE_POSTCODE__,5);
			$kPagination->paginate();
			
			if($postSearchAry['idServiceType']== __SERVICE_TYPE_WTD__)
			{
				if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTW'))
				{
					$idExpactedServiceType = __SERVICE_TYPE_WTW__ ;
				}
				elseif($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTP'))
				{
					$idExpactedServiceType = __SERVICE_TYPE_WTP__ ;
				}
				else
				{
					$idExpactedServiceType = __SERVICE_TYPE_PTP__ ;
				}		
			}
			else if($postSearchAry['idServiceType']== __SERVICE_TYPE_PTD__)
			{
				if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iPTW'))
				{
					$idExpactedServiceType = __SERVICE_TYPE_PTW__ ;
				}
				else
				{
					$idExpactedServiceType = __SERVICE_TYPE_PTP__ ;
				}	
			}
			else if($postSearchAry['idServiceType']== __SERVICE_TYPE_DTD__)
			{
				if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iDTW'))
				{
					$idExpactedServiceType = __SERVICE_TYPE_DTW__ ;
				}
				elseif($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iDTP'))
				{
					$idExpactedServiceType = __SERVICE_TYPE_DTP__ ;
				}
				elseif($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTW'))
				{
					$idExpactedServiceType = __SERVICE_TYPE_WTW__ ;
				}
				else
				{
					$idExpactedServiceType = __SERVICE_TYPE_PTP__ ;
				}		
			}
			if(!empty($cityPostcodeAry))
			{	
				?>
				<p><?=t($t_base.'messages/xp1_detail_text_1');?> <?=$szExactLocation?> <?=t($t_base.'messages/xp2_detail_text_2');?> <?=$postSearchAry['szDestinationCountry']?>. <?=t($t_base.'messages/xp1_detail_text_3');?></p>
				<div id="data_container">
				<div class="container2" id="boxscroll2">				
					<?
						foreach($cityPostcodeAry as $key=>$fromRegions)
						{
					?>
						<p class="checkbox-ab"><input type="radio" onclick="enable_submit_button('partial_empty_postcode_from_form','partialEmptyPostcodeCityAry[szRegionTo]')" name="partialEmptyPostcodeCityAry[szRegionTo]" value="<?=$key?>"><?=$fromRegions?></p>
					<?
						}
				echo '</div>';
			}
			echo '<div class="page_nav" align="right">';
			if($kPagination->links_per_page>1)
			{
				$kPagination->ajax_function = "show_next_page";
				$kPagination->append = "'MULTI_PARTIAL_EMPTY_POSTCODE_NEXT_PAGE'";
				$kPagination->iIncludeDots = 1 ;
				echo $kPagination->renderFullNav();
			}
			echo '</div>';
			?>
		</div>
			<input type="hidden" name="partialEmptyPostcodeCityAry[iRegionTo]" value="1">
			<?
			$button_text = t($t_base_landing.'fields/continue_button_delivery')." ".$postSearchAry['szDestinationCountry'];
		}
	?>
	
	<input type="hidden" id="szCityName" name="partialEmptyPostcodeCityAry[szCityName]" value="<?=$szCity?>">
	<input type="hidden" id="szRegion1" name="partialEmptyPostcodeCityAry[szRegion1]" value="<?=$szRegion1?>">
	<input type="hidden" id="szOriginDestination" name="partialEmptyPostcodeCityAry[type]" value="<?=$type?>">
	<input type="hidden" name="partialEmptyPostcodeCityAry[iCheckDestination]" value="<?=$iDestinationFlag?>">
	<p align="center" class="button-container" style="margin-top:17px;">
		<input type="hidden" name="booking_key" value="<?=$postSearchAry['szBookingRandomNum']?>">
		<input type="hidden" name="partialEmptyPostcodeCityAry[szBookingRandomNum]" id="szBookingRandomNum" value="<?=$postSearchAry['szBookingRandomNum']?>">
		<a href="javascript:void(0);" id="submit_button" class="gray-button1">
		<span><?=t($t_base_landing.'fields/use_this');?> <?=$szExactLocation?></span></a>
	</p>
	<p align="center" class="button-container">
		<?
			if($type=='ORIGIN')
			{
		?>
			<a href="javascript:void(0);" onclick="update_booking_service_type_requirement('<?=$idExpactedServiceType?>','<?=$postSearchAry['szBookingRandomNum']?>','VALIDATE_POSTCODE_AFTER_HAULAGE_POPUP','<?=$szCity?>','<?=$szDestinationCity?>','<?=$type?>');" class="orange-button2">
		<?
			}
			else
			{
		?>
			<a href="javascript:void(0);" onclick="update_booking_service_type('<?=$idExpactedServiceType?>','<?=$postSearchAry['szBookingRandomNum']?>','SERVICE_TYPE','','UPDATE_SERVICE','<?=$szCity?>','<?=$type?>');" class="orange-button2">
		<?
			}
		?>
		<span><?=$button_text?></span></a>
	</p>
	</div>
	</div>
</form>	
	<?
}
function display_multiregion_popup($postSearchAry,$multiEmaptyPostcodeCityAry,$type,$szCity,$iDestinationFlag=0,$szDestinationCity=false)
{
	$t_base = "home/homepage/";
	$kBooking=new cBooking();
	$header_text = t($t_base.'messages/which');
	$from_postcode = false;
	
	if($type=='ORIGIN')
	{
		$header_text .= " ".$szCity." ".t($t_base.'messages/do_you_mean');
		$szCountryName = $postSearchAry['szOriginCountry'];
	}			
	else if($type=='DESTINATION')
	{
		$header_text .= " ".$szCity." ".t($t_base.'messages/do_you_mean');
		$szCountryName = $postSearchAry['szDestinationCountry'];
	}
?>
	<script type="text/javascript">
		$("#boxscroll2").niceScroll({touchbehavior:false,cursorcolor:"#17375E",cursoropacitymax:0.7,cursorwidth:7,autohidemode:false}).show();
	</script>
	<div id="popup-bg" class="white-bg"></div>
	<form name="multiregion_select_form" id="multiregion_select_form" action="" method="post">
	<div id="popup-container">
	<div class="service-popup popup">		
	<p class="close-icon" align="right"><a heref="javascript:void(0);" onclick="closeScrollablePopup('all_available_service');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
	<H5><?=$header_text?></H5>
	<div id="popupError" class="errorBox" style="display:none;">
	</div>		
		<?
		if($type=='ORIGIN')
		{
			if(!empty($multiEmaptyPostcodeCityAry))
			{	
				?>
				<p><?=$szCity?> <?=t($t_base.'messages/available_in_multi_region');?> <?=$szCountryName?>. <?=t($t_base.'messages/select_right');?>.</p>
				<div class="container2" id="boxscroll2">				
					<?
						foreach($multiEmaptyPostcodeCityAry as $key=>$fromRegions)
						{
					?>
						<p class="checkbox-ab"><input type="radio" onclick="enable_submit_button('multiregion_select_form','multiregionCityAry[szRegionFrom]')" name="multiregionCityAry[szRegionFrom]" value="<?=$key?>"><?=$fromRegions?></p>
					<?
						}
				echo '</div>';
			}
		?>
			<input type="hidden" name="multiregionCityAry[iRegionFrom]" value="1">
			<input type="hidden" name="multiregionCityAry[szDestinationCity]" value="<?=$szDestinationCity?>">
			<?
		}
		else
		{
			if(!empty($multiEmaptyPostcodeCityAry))
			{	
				?>
				<p><?=$szCity?> <?=t($t_base.'messages/available_in_multi_region');?> <?=$szCountryName?>. <?=t($t_base.'messages/select_right');?>.</p>
				<div class="container2">				
					<?
						foreach($multiEmaptyPostcodeCityAry as $key=>$fromRegions)
						{
					?>
						<p class="checkbox-ab"><input type="radio"  onclick="enable_submit_button('multiregion_select_form','multiregionCityAry[szRegionTo]')" name="multiregionCityAry[szRegionTo]" value="<?=$key?>"><?=$fromRegions?></p>
					<?
						}
				echo '</div>';
			}
			?>
			<input type="hidden" name="multiregionCityAry[iRegionTo]" value="1">
			<?
		}
	?> 
	<input type="hidden" name="multiregionCityAry[type]" value="<?=$type?>"> 
	<input type="hidden" name="multiregionCityAry[szCityPostcode]" value="<?=$szCity?>">
	<input type="hidden" name="multiregionCityAry[iCheckDestination]" value="<?=$iDestinationFlag?>">	
	<p align="center">
		<input type="hidden" name="booking_key" value="<?=$postSearchAry['szBookingRandomNum']?>">
		<input type="hidden" name="multiregionCityAry[szBookingRandomNum]" id="szBookingRandomNum" value="<?=$postSearchAry['szBookingRandomNum']?>">
		<a href="javascript:void(0);" id="submit_button" class="gray-button1">
		<span><?=t($t_base.'fields/select');?></span></a>
	</p>
	</div>
	</div>
</form>	
	<?php
}

function get_service_field_name($idServiceType)
{
	if($idServiceType == __SERVICE_TYPE_DTD__)	//DTD
	{
		$field_name ="iDTD";
	}
	elseif($idServiceType == __SERVICE_TYPE_DTP__)	//DTD
	{
		$field_name ="iDTP";
	}
	elseif($idServiceType == __SERVICE_TYPE_DTW__)	//DTD
	{
		$field_name ="iDTW";
	}
	elseif($idServiceType == __SERVICE_TYPE_WTW__)	//DTD
	{
		$field_name ="iWTW";
	}
	elseif($idServiceType == __SERVICE_TYPE_WTP__)	//DTD
	{
		$field_name ="iWTP";
	}
	elseif($idServiceType == __SERVICE_TYPE_WTD__)	//DTD
	{
		$field_name ="iWTD";
	}
	elseif($idServiceType == __SERVICE_TYPE_PTP__)	//DTD
	{
		$field_name ="iPTP";
	}
	elseif($idServiceType == __SERVICE_TYPE_PTD__)	//DTD
	{
		$field_name ="iPTD";
	}
	elseif($idServiceType == __SERVICE_TYPE_PTW__)	//DTD
	{
		$field_name ="iPTW";
	}
	return $field_name ;
}

function updatePricingChanges($updateBookingAry)
{
    $res_ary = array();
    $res_ary['iBookingCutOffHours'] = $updateBookingAry['iBookingCutOffHours'];

    $res_ary['fOriginChargeRateWM'] = $updateBookingAry['fOriginRateWM'];
    $res_ary['fOriginChargeMinRateWM']= $updateBookingAry['fOriginMinRateWM'];
    $res_ary['fOriginChargeBookingRate'] = $updateBookingAry['fOriginRate'];					
    $res_ary['szOriginChargeCurrency'] = $updateBookingAry['szOriginPortCurrency'];
    $res_ary['fOriginChargeExchangeRate']= $updateBookingAry['fOriginFreightExchangeRate'];
    $res_ary['fOriginChargePriceUSD'] = $updateBookingAry['fTotalOriginPortPrice'];

    $res_ary['fFreightRateWM'] = $updateBookingAry['fRateWM'];
    $res_ary['fFreightMinRateWM'] = $updateBookingAry['fMinRateWM'];
    $res_ary['fFreightBookingRate'] = $updateBookingAry['fRate'];
    $res_ary['fFreightExchangeRate'] = $updateBookingAry['fFreightExchangeRate'];
    $res_ary['fFreightPriceUSD'] = $updateBookingAry['fWTWPrice'];
    $res_ary['idFreightCurrency'] = $updateBookingAry['szFreightCurrency'];					
    $res_ary['szFreightCurrency'] = $updateBookingAry['szFreightCurrencyName'];

    $res_ary['fDestinationChargeRateWM'] = $updateBookingAry['fDestinationRateWM'];
    $res_ary['fDestinationChargeMinRateWM']= $updateBookingAry['fDestinationMinRateWM'];
    $res_ary['fDestinationChargeBookingRate'] = $updateBookingAry['fDestinationRate'];					
    $res_ary['szDestinationChargeCurrency'] = $updateBookingAry['szDestinationPortCurrency'];
    $res_ary['fDestinationChargeExchangeRate']= $updateBookingAry['fDestinationFreightExchangeRate'];
    $res_ary['fDestinationChargePriceUSD'] = $updateBookingAry['fTotalDestinationPortPrice'];

    $res_ary['fOriginCCPriceUSD'] = $updateBookingAry['fOriginCCPrice'];
    $res_ary['idOriginCCCurrency'] = $updateBookingAry['fOriginCCCurrency'];
    $res_ary['szOriginCCCurrency'] = $updateBookingAry['szOriginCCCurrency'];
    $res_ary['fOriginCCExchangeRate'] = $updateBookingAry['fOriginCCExchangeRate'];

    $res_ary['fOriginHaulageDistance'] = $updateBookingAry['fOriginHaulageDistance'];
    $res_ary['fOriginHaulageTransitTime'] = $updateBookingAry['fOriginHaulageTransitTime'];
    $res_ary['fOriginHaulagePricePer100Kg'] = $updateBookingAry['fOriginHaulageRateWM_KM'];
    $res_ary['fOriginHaulageMinimumPrice'] = $updateBookingAry['fOriginHaulageMinRateWM_KM'];
    $res_ary['fOriginHaulagePricePerBooking'] = $updateBookingAry['fOriginHaulageBookingRateWM_KM'];
    $res_ary['fOriginHaulageExchangeRate'] = $updateBookingAry['fOriginHaulageExchangeRate'];
    $res_ary['fOriginHaulageChargableWeight'] = $updateBookingAry['fOriginHaulageChargableWeight'];
    $res_ary['fOriginHaulageFuelPercentage'] = $updateBookingAry['fOriginHaulageFuelPercentage'];
    $res_ary['fOriginHaulageFuelSurcharge'] = $updateBookingAry['fOriginHaulageFuelSurcharge'];
    $res_ary['iOriginHaulageDistanceUptoKm'] = $updateBookingAry['iOriginHaulageDistanceUptoKm'];
    $res_ary['szOriginHaulageModelName'] = $updateBookingAry['szOriginHaulageModelName'];
    $res_ary['fOriginHaulagePriceUSD'] = $updateBookingAry['fOriginHaulagePrice'];
    $res_ary['szOriginHaulageCurrency'] = $updateBookingAry['szOriginHaulageCurrency'];
    $res_ary['idOriginHaulageCurrency'] = $updateBookingAry['idOriginHaulageCurrency'];

    $res_ary['fDestinationCCPriceUSD'] = $updateBookingAry['fDestinationCCPrice'];
    $res_ary['fDestinationCCExchangeRate']= $updateBookingAry['fDestinationCCExchangeRate'];
    $res_ary['idDestinationCCCurrency'] = $updateBookingAry['fDestinationCCCurrency'];
    $res_ary['szDestinationCCCurrency'] = $updateBookingAry['szDestinationCCCurrency'];

    $res_ary['fDestinationHaulageDistance'] = $updateBookingAry['fDestinationHaulageDistance'];
    $res_ary['fDestinationHaulageTransitTime'] = $updateBookingAry['fDestinationHaulageTransitTime'];
    $res_ary['fDestinationHaulagePricePer100Kg'] = $updateBookingAry['fDestinationHaulageRateWM_KM'];
    $res_ary['fDestinationHaulageMinimumPrice'] = $updateBookingAry['fDestinationHaulageMinRateWM_KM'];
    $res_ary['fDestinationHaulagePricePerBooking'] = $updateBookingAry['fDestinationHaulageBookingRateWM_KM'];
    $res_ary['fDestinationHaulageExchangeRate']= $updateBookingAry['fDestinationHaulageExchangeRate'];
    $res_ary['fDestinationHaulageChargableWeight']= $updateBookingAry['fDestinationHaulageChargableWeight'];
    $res_ary['fDestinationHaulageFuelPercentage'] = $updateBookingAry['fDestinationHaulageFuelPercentage'];
    $res_ary['fDestinationHaulageFuelSurcharge']= $updateBookingAry['fDestinationHaulageFuelSurcharge'];
    $res_ary['iDestinationHaulageDistanceUptoKm'] = $updateBookingAry['iDestinationHaulageDistanceUptoKm'];
    $res_ary['szDestinationHaulageModelName'] = $updateBookingAry['szDestinationHaulageModelName'];
    $res_ary['fDestinationHaulagePriceUSD'] = $updateBookingAry['fDestinationHaulagePrice'];
    $res_ary['szDestinationHaulageCurrency'] =$updateBookingAry['szDestinationHaulageCurrency'];
    $res_ary['idDestinationHaulageCurrency'] = $updateBookingAry['idDestinationHaulageCurrency'];


    $res_ary['fTotalPriceUSD'] = round((float)$updateBookingAry['fBookingPriceUSD'],6); // this is the total booking price in USD.
    $res_ary['fTotalPriceNoMarkupUSD'] = $updateBookingAry['fPrice'];
    $res_ary['fPriceMarkupCustomerCurrency'] = $updateBookingAry['fTotalMarkupPrice'];
    $res_ary['fMarkupPercentage'] = $updateBookingAry['fMarkupPercentage'];

    $res_ary['idForwarderCurrency'] = $updateBookingAry['idForwarderCurrency'];
    $res_ary['szForwarderCurrency'] = $updateBookingAry['szForwarderCurrency'];
    $res_ary['fForwarderExchangeRate'] = $updateBookingAry['fForwarderCurrencyExchangeRate'];
    $res_ary['fTotalPriceForwarderCurrency'] = $updateBookingAry['fForwarderCurrencyPrice'];
    $res_ary['fTotalPriceNoMarkupForwarderCurrency'] = $updateBookingAry['fForwarderCurrencyPrice_XE'];
    $res_ary['fTotalPriceNoMarkupForwarderCurrency_new'] = $updateBookingAry['fForwarderCurrencyPrice_XE'];								
    $res_ary['fReferalPercentage'] = $updateBookingAry['fReferalPercentage'];
    $res_ary['fReferalAmount'] = $updateBookingAry['fReferalAmount']; 
    $res_ary['fTotalVat'] = $updateBookingAry['fTotalVat'];

    $res_ary['fOriginTruckingRate'] = $updateBookingAry['fOriginTruckingRate'];
    $res_ary['fDestinationTruckingRate']= $updateBookingAry['fDestinationTruckingRate'];
    $res_ary['fPriceForSorting'] = $updateBookingAry['fTotalPriceCustomerCurrency']; // this is the price used for sorting . 

    $res_ary['fTotalPriceCustomerCurrency'] = $updateBookingAry['fDisplayPrice'];  // this is the which we are showing to user
    $res_ary['iTransitHours']= $updateBookingAry['iTransitHours'];
    $res_ary['dtCreatedOn'] = date('Y-m-d H:i:s');
    $res_ary['iDoNotKnowCargo'] = 0;
    return $res_ary;
}

function display_S1_S2_popup($postSearchAry,$idService,$idExpactedService)
{
	$t_base = "LandingPage/";
	$idBooking = $postSearchAry['id'];
	$landing_page_url = __LANDING_PAGE_URL__ ;
	$szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
	
	$iMonth = (int)date("m",$postSearchAry['dtMaxDate']);
	$szMonthName = getMonthName($iLanguage,$iMonth);
		
	$dtTimingText = date('j.',$postSearchAry['dtMaxDate'])." ".$szMonthName." ".date('Y',$postSearchAry['dtMaxDate']);
	
	$iMonth = (int)date("m",$postSearchAry['dtTimingDate']);
	$szMonthName = getMonthName($iLanguage,$iMonth);
	
	$dtUserTimingText = date('j.',$postSearchAry['dtTimingDate'])." ".$szMonthName." ".date('Y',$postSearchAry['dtTimingDate']);
		
	?>	
	<script type="text/javascript">
		$().ready( function(){
			default_cursor('<?=$_SESSION['user_id']?>');
			
		});
	</script>
	<div id="popup-bg" class="white-bg"></div>
	<form id="no_services_form" method="post" name="no_services_form" action="">
		<div id="popup-container">
		<div class="standard-popup-f popup" >
		<p  class="close-icon" align="right"><a heref="javascript:void(0);" onclick="closePopup('all_available_service');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
		<h5><?=t($t_base.'title/s1_popup_title_1')?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/s1_popup_title_2')?> <?=$dtTimingText?> <?=t($t_base.'title/s1_popup_title_3')?></h5>
		
<?php
	$notify_me_class = "class='gray-button1'";
	if($_SESSION['user_id']<=0)
	{
		$submit_button_text = t($t_base.'fields/submit');
		?>
		<!-- <h3>S1</h3> -->
		<p class="heading-space" style="margin-bottom:15px;"><?=t($t_base.'title/s1_details_text_1');?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/to');?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/s1_details_text_2');?> <?=$dtTimingText?>.</p>
		<p><?=t($t_base.'title/s1_details_text_3');?> <?=$dtUserTimingText?>. <?=t($t_base.'title/s1_details_text_4');?> </p>
		<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>
		
		<div class="oh name-space">
			<p class="fl-20"><?=t($t_base.'fields/name');?></p>
			<p class="fl-80">
				<input type="text" id="szCustomerName_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_NAME_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>');" onkeyup = "enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event);" name="addExpactedTimingAry[szCustomerName]" value="<?=$postSearchResult['szCustomerName']?>">
				<span id="szCustomerName_popup_error" class="red_text" style="display:none;float:right;"></span>
			</p>
		</div>
		<?
	}
	else
	{
		$submit_button_text = t($t_base.'fields/yes_please');
		$email_loggedin_class="email-space";
		$kUser = new cUser();
		$kUser->getUserDetails($_SESSION['user_id']);
		if(empty($postSearchResult['szEmail']))
		{
			$postSearchResult['szCustomerPhone'] = $kUser->szPhoneNumber ;
			$postSearchResult['szEmail'] = $kUser->szEmail ;
			if(!empty($postSearchResult['szEmail']))
			{
				$notify_me_class = "class='orange-button2'";
				$notify_me_onclick = "onclick=\"update_expacted_services_requirement('".$landing_page_url."')\"";
			}
		}
		?>
		<!-- <h3>S2</h3> -->
		<p class="heading-space" style="margin-bottom:15px;"><?=t($t_base.'title/s1_details_text_1');?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/to');?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/s1_details_text_2');?> <?=$dtTimingText?>.</p>
		<p><?=t($t_base.'title/s1_details_text_3');?> <?=$dtUserTimingText?>.</p>
		<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>	
		<?
	}
	?>
	<div class="oh <?=$email_loggedin_class;?>">
		<p class="fl-20"><?=t($t_base.'fields/email');?></p>
		<p class="fl-80">
			<input type="text" id="szCustomerEmail_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_EMAIL_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>');" onkeyup="enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event);" name="addExpactedTimingAry[szEmail]" value="<?=$postSearchResult['szEmail']?>">
			<span id="szCustomerEmail_popup_error" class="red_text" style="display:none;float:right;"></span>
		</p>
	</div>
	<div class="oh" style="margin-top:-5px;">
		<p class="fl-20"><?=t($t_base.'fields/phone');?></p>
		<p class="fl-80">
			<input type="text" id="szCustomerPhone_popup" name="addExpactedServiceAry[szCustomerPhone]" value="<?=$postSearchResult['szCustomerPhone']?>">
		</p>
	</div>
	<p align="center" class="button-container">	
		<a href="javascript:void(0);" <?=$notify_me_class?> <?=$notify_me_onclick?>  id="notify_me_button"><span><?=$submit_button_text;?></span></a>
	</p>
	<p align="center" class="button-container">
		<input type="hidden" name="addExpactedTimingAry[idTimingType]" value="1">
		<input type="hidden" name="addExpactedTimingAry[idBooking]" value="<?=$idBooking?>">
		<input type="hidden" name="addExpactedTimingAry[idExpactedService]" value="<?=$idExpactedService?>">
		<input type="hidden" name="addExpactedTimingAry[idNotificationType]" value="<?=__NOTIFICATION_TYPE_S1_S2___?>">
		<input type="hidden" name="addExpactedTimingAry[dtExpactedTiming]" id="dtExpactedTiming" value="<?=$postSearchAry['dtTimingDate']?>">
		<a href="javascript:void(0);" class="orange-button2" onclick="update_booking_service_type(1,'<?=$szBookingRandomNum?>','CARGO_TIMING_UPDATE','<?=$postSearchAry['dtMaxDate']?>');"><span><?=t($t_base.'fields/search_for_closest_possible_dates');?></span></a>
	</p>
	

</div>		
</div>
</form>
	<?
}

function display_T1_T2_popup($postSearchAry,$idService,$idExpactedService)
{
	$t_base = "LandingPage/";
	$idBooking = $postSearchAry['id'];
	$landing_page_url = __LANDING_PAGE_URL__ ;
	$szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
	if($postSearchAry['iFromRequirementPage']==2)
	{
		$landing_page_url = __SELECT_SIMPLE_SEARCH_URL__ ;
	}
	
	$iMonth = (int)date("m",$postSearchAry['dtMaxDate']);
	$szMonthName = getMonthName($iLanguage,$iMonth);
	$dtTimingText = date('j.',$postSearchAry['dtMaxDate'])." ".$szMonthName." ".date('Y',$postSearchAry['dtMaxDate']);
	
	$iMonth = (int)date("m",$postSearchAry['dtTimingDate']);
	$szMonthName = getMonthName($iLanguage,$iMonth);
	$dtUserTimingText = date('j.',$postSearchAry['dtTimingDate'])." ".$szMonthName." ".date('Y',$postSearchAry['dtTimingDate']);
	
	?>	
	<script type="text/javascript">
		$().ready( function(){
			default_cursor('<?=$_SESSION['user_id']?>');
			
		});
	</script>
	<div id="popup-bg" class="white-bg"></div>
	<form id="no_services_form" method="post" name="no_services_form" action="">
		<div id="popup-container">
		<div class="standard-popup-f popup" >
		<p  class="close-icon" align="right"><a heref="javascript:void(0);" onclick="closePopup('all_available_service');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
		<h5><?=t($t_base.'title/t1_popup_title_1')?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/t1_popup_title_2')?> <?=$dtTimingText?></h5>
		
<?php
	$notify_me_class = "class='gray-button1'";
	if($_SESSION['user_id']<=0)
	{
		?>
		<!-- <h3>T1</h3> -->
		<p class="heading-space" style="margin-bottom:15px;"><?=t($t_base.'title/t1_details_text_1');?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/and');?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/t1_details_text_2');?> <?=$dtTimingText?>.</p>
		<p><?=t($t_base.'title/t1_details_text_3');?> <?=$dtUserTimingText?>. <?=t($t_base.'title/s1_details_text_4');?> </p>
		<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>		
		<div class="oh name-space">
			<p class="fl-20"><?=t($t_base.'fields/name');?></p>
			<p class="fl-80">
				<input type="text" id="szCustomerName_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_NAME_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>');" onkeyup = "enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event);" name="addExpactedTimingAry[szCustomerName]" value="<?=$postSearchResult['szCustomerName']?>">
				<span id="szCustomerName_popup_error" class="red_text" style="display:none;float:right;"></span>
			</p>
		</div>
		<?
	}
	else
	{
		$email_loggedin_class="email-space";
		$kUser = new cUser();
		$kUser->getUserDetails($_SESSION['user_id']);
		if(empty($postSearchResult['szEmail']))
		{
			$postSearchResult['szEmail'] = $kUser->szEmail ;
			if(!empty($postSearchResult['szEmail']))
			{
				$notify_me_class = "class='orange-button2'";
				$notify_me_onclick = "onclick=\"update_expacted_services_requirement('".$landing_page_url."')\"";
			}
		}
		?>
		<!-- <h3>T2</h3> -->
		<p class="heading-space" style="margin-bottom:15px;"><?=t($t_base.'title/t1_details_text_1');?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/and');?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/t1_details_text_2');?> <?=$dtTimingText?>.</p>
		<p><?=t($t_base.'title/t1_details_text_3');?> <?=$dtUserTimingText?>.</p>
		<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>

		<?
	}
	?>
	<div class="oh <?=$email_loggedin_class;?>">
		<p class="fl-20"><?=t($t_base.'fields/email');?></p>
		<p class="fl-80">
			<input type="text" id="szCustomerEmail_popup" onblur="Valid_Form_Feild_Check(this.value,'VALID_EMAIL_CHECK','<?=$szBookingRandomNum?>','<?=(int)$_SESSION['user_id']?>');" onkeyup="enable_notify_me_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>',event);" name="addExpactedTimingAry[szEmail]" value="<?=$postSearchResult['szEmail']?>">
			<span id="szCustomerEmail_popup_error" class="red_text" style="display:none;float:right;"></span>
		</p>
	</div>
	<p align="center" class="button-container">
	
		<input type="hidden" name="addExpactedTimingAry[idTimingType]" value="2">
		<input type="hidden" name="addExpactedTimingAry[idBooking]" value="<?=$idBooking?>">
		<input type="hidden" name="addExpactedTimingAry[idExpactedService]" value="<?=$idExpactedService?>">
		<input type="hidden" name="addExpactedTimingAry[idServiceType]" id="idServiceType" value="<?=$idService?>">
		<input type="hidden" name="addExpactedTimingAry[idNotificationType]" value="<?=__NOTIFICATION_TYPE_T1_T2___?>">		
		<input type="hidden" name="addExpactedTimingAry[dtExpactedTiming]" id="dtExpactedTiming" value="<?=$postSearchAry['dtTimingDate']?>">
		<a href="javascript:void(0);" class="orange-button2" onclick="update_booking_service_type(2,'<?=$szBookingRandomNum?>','CARGO_TIMING_UPDATE','<?=$postSearchAry['dtMaxDate']?>');"><span><?=t($t_base.'fields/show_closest_possible_date');?></span></a>
	</p>
	<p align="center" class="button-container">	
		<a href="javascript:void(0);" <?=$notify_me_class?> <?=$notify_me_onclick?>  id="notify_me_button"><span><?=t($t_base.'fields/notify_when_i_can_book');?> <?=$dtUserTimingText?></span></a>
	</p>

</div>		
</div>
</form>
	<?
}

function get_country_name($szCountryName,$type)
{
	if(!empty($szCountryName) && strlen($szCountryName)>__MAX_COUNTRY_NAME_LENGTH_ON_BUTTONS___)
	{
		if($type=='ORIGIN')
		{
			$szCountryName = "ORIGIN COUNTRY";
		}
		else
		{
			$szCountryName = "DEST COUNTRY";
		}
		return $szCountryName;
	}
	else
	{
		return $szCountryName;
	}
}
function random_sort_array($list) 
{ 
  if (!is_array($list)) return $list; 

  $keys = array_keys($list); 
  shuffle($keys); 
  $random = array(); 
  foreach ($keys as $key) { 
    $random[] = $list[$key]; 
  }
  return $random; 
}

function displayCurrencyChangePopup($idOldCurrency,$mode=false,$redirect_url=false)
{
	$t_base = "LandingPage/";
	$kConfig=new cConfig();
	$kUserNew =new cUser();
	$currencyAry = array();
	$currencyAry = $kConfig->getBookingCurrency(false,true);
	$kUserNew->loadCurrency($idOldCurrency);
	if(empty($redirect_url))
	{
		$redirect_url = __REQUIREMENT_PAGE_URL__ ;
	}
	?>
	
	<div id="popup-bg"  class="white-bg"></div>
	<form action="" method="post" id="currency_popup_form" name="currency_popup_form" >
			<div id="popup-container">	
			<script>
				Custom.init();
		</script>		
			<div class="standard-popup" style="margin-top:120px;">
			<!--  <p class="close-icon" align="right"><a href="javascript:void(0);" onclick="redirect_url('<?=__USER_LOGOUT_URL__?>');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>-->
			<h5><?=t($t_base.'title/update_preffered_currency');?></h5>
			<p><?=$kUserNew->szCurrencyName?> <?=t($t_base.'title/update_preffered_currency_description');?></p>			
			<p style="margin: 17px 129px 10px;min-height: 30px;" class="currency-option">
				<select class="styled" name="currencyAry[idCurrency]" id="idCurrency" onkeyup="on_enter_key_press(event,'submit_currency_popup','');" >
				<?
				 	if(!empty($currencyAry))
				 	{
				 		foreach($currencyAry as $currencyArys)
				 		{
				 			?>
				 				<option value="<?=$currencyArys['id']?>"><?=$currencyArys['szCurrency']?></option>
				 			<?
				 		}
				 	}
				 ?>
				</select>
			</p>			
			<div class="oh">
				<p class="fl-4" align="center">
					<a href="javascript:void(0);" onclick="update_user_currency('<?=$redirect_url?>')" class="orange-button2"><span><?=t($t_base.'fields/save');?></span></a>
					<input type="hidden" name="currencyAry[szMode]" id="szMode" value="<?=$mode?>">
					<input type="hidden" name="currencyAry[szRedirectUrl]" id="szRedirectUrl" value="<?=$redirect_url?>">
					<input type="hidden" name="currencyAry[idTempUser]" id="idTempUser" value="<?=$_SESSION['user_temp_id']?>">
				</p>
			</div>
		</div>
		</div>
	</form>
	<?php
}

function checkUserPrefferedCurrencyActive($mode=false)
{
	$userSessionData = array();
	$userSessionData = get_user_session_data(true);
	$idUserSession = $userSessionData['idUser'];

	$kUser= new  cUser();
	$kUser->getUserDetails($idUserSession);
	$kUserNew = new cUser();
	$kUserNew->loadCurrency($kUser->szCurrency);
	if($kUserNew->iBooking<=0) // User preffered currency is not active.
	{
		echo "<div id='Transporteca_popup'>";
		echo displayCurrencyChangePopup($kUser->szCurrency,$mode);
		echo "</div>";
	}
}
function showAllListing($SearchExplain,$searchField,$fieldName,$linkName = '',$url)
{	
	$i = 0;	
	$searchFields = strtolower(trim($searchField));
	$replacearray = array(',',' ','?');
	$searchArr = array();
	if($SearchExplain != array())
	{
		foreach($SearchExplain as $search)
		{	
			$searchCount = 0;
			$dataFrom = trim($search[$fieldName]);
			/////CHECKING HEADING FIELD///
			if(isset($search['szHeading']) && $fieldName != 'szContent')
			{	
				$headingArr = strtolower($search['szHeading']);
				$searchCount += substr_count($headingArr, $searchFields);
			}
			
			$dataFrom = strtolower($search[$fieldName]);
			$searchCount += substr_count($dataFrom,$searchFields);	
			if($searchCount > 0)
			{	
				$searchArr[$i][] = "<a target='_blank' href='".$url.$search['szLink']."#".str_replace($replacearray,"_",strtolower($search['szHeading']))."'>".(isset($search['szPageHeading'])?$search['szPageHeading']:$linkName)." - ".$search['szHeading'].' - '.$searchCount.(($searchCount)>1?' mentions':' mention')."</a>"; 
				$searchArr[$i][] = $searchCount;
				$i++;
			}
		}
	}	
			return $searchArr;
}
function getModifiedArray($inputAry,$formated=false)
{
	if(!empty($inputAry))
	{
		$final_ary = array();
		$ctr=0;
		$warehAry = $inputAry;
		if(!empty($warehAry))
		{
			foreach($warehAry as $warehArys)
			{
				$warehouse_pair_ary = explode("||||",$warehArys);
				if(!empty($warehouse_pair_ary))
				{
					foreach($warehouse_pair_ary as $warehouse_pair_arys)
					{
						if(!empty($warehouse_pair_arys))
						{
							$warehouseAry = explode("||",$warehouse_pair_arys);
							if($formated)
							{
								$final_ary[$ctr]['idWarehouseFrom'] =  $warehouseAry[0];
								$final_ary[$ctr]['idWarehouseTo'] =  $warehouseAry[1];
							}
							else
							{
								$final_ary['idWarehouseFrom'][$ctr] =  $warehouseAry[0];
								$final_ary['idWarehouseTo'][$ctr] =  $warehouseAry[1];
							}
							$ctr++;
						}
					}
				}
			}
		}
		return $final_ary ;
	}
}
function getLanguageId($szLanguage=false)
{
    //$languageAry = array(__LANGUAGE_TEXT_DANISH__,__LANGUAGE_TEXT_SWEDISH__,__LANGUAGE_TEXT_NORWEGIAN__); 
    if(!empty($szLanguage))
    {
        $kConfig = new cConfig();
        $languageArr=$kConfig->getLanguageDetails('','',false,$szLanguage);
        if(!empty($languageArr))
        {
            $iLanguage=$languageArr[0]['id'];
        }
        else
        {
            $iLanguage=__LANGUAGE_ID_ENGLISH__;
        }
    }
    /*
    else if(!empty($_SESSION['user_selected_language']))
    {
        if($_SESSION['user_selected_language']==__LANGUAGE_TEXT_ENGLISH__)
        {
            $iLanguage = __LANGUAGE_ID_ENGLISH__ ;
        }
        else
        {
            $iLanguage = __LANGUAGE_ID_DANISH__ ;
        }
    }*/
    else
    {
        $szLanguage = __LANGUAGE__; 
        $kConfig = new cConfig();
        $languageArr=$kConfig->getLanguageDetails('','',false,$szLanguage);
        if(!empty($languageArr))
        {
            $iLanguage=$languageArr[0]['id'];
        }
        else
        {
            $iLanguage=__LANGUAGE_ID_ENGLISH__;
        }
//        if(in_array($szLanguage,$languageAry))
//        { 
//            $iLanguage = __LANGUAGE_ID_DANISH__ ;
//        }
//        else
//        {
//            $iLanguage = __LANGUAGE_ID_ENGLISH__ ;
//        }
    } 
    return $iLanguage;
} 
function refineDescriptionData($content)
{
    $content = str_replace('&nbsp;</div>','</div>',$content);
    $content = str_replace('&#39;','\'',$content); 
    $content = str_replace('&#039;','\'',$content); 
    return $content;
}
function mb_substr_replace($input, $tail, $count, $heapLength,$format) 
{ 
	if($heapLength > $count)
	{
		return mb_substr($input, 0, $count,$format).$tail;
	}
	else
	{
		return $input;
	}
}
function display_no_booking_details($szBookingStatus)
{
    $t_base = "Booking/MyBooking/";
    ?>	
    <div class="oh">
        <p class="fl-40" style="width: 256px;"><img src="<?=__BASE_STORE_IMAGE_URL__.'/CabinetEmpty.jpg'?>"  border="0" width="240"/></p>
        <div class="fl-60" style="margin-top: 18px;">		
        <?php if($szBookingStatus=='active') { ?>
            <h2 class="create-account-heading"><?=t($t_base.'title/empty_booking_active_bold_text');?></h2>
            <p class="f-size-22"><?=t($t_base.'title/empty_booking_active_details_text');?></p>				
        <?php } elseif($szBookingStatus=='hold') { ?>
                <h2 class="create-account-heading"><?=t($t_base.'title/empty_booking_hold_bold_text');?></h2> 
                <p class="f-size-22"><?=t($t_base.'title/empty_booking_hold_details_text');?></p> 
        <?php }elseif($szBookingStatus=='draft')  {
                $kWhsSearch = new cWHSSearch();
                $iMaxNumBooking = $kWhsSearch->getManageMentVariableByDescription('__MAX_ALLOWED_DRAFT__');
        ?> 
                <h2 class="create-account-heading"><?=t($t_base.'title/empty_booking_draft_bold_text_1').' '.$iMaxNumBooking.' '.t($t_base.'title/empty_booking_draft_bold_text_2')?></h2>
                <p class="f-size-22"><?=t($t_base.'title/empty_booking_draft_details_text');?></p>
        <?php } elseif($szBookingStatus=='archive') {  ?>
                <h2 class="create-account-heading"><?=t($t_base.'title/empty_booking_archive_bold_text')?></h2>
                <p class="f-size-22"><?=t($t_base.'title/empty_booking_archive_details_text');?></p>
        <?php } ?> 
                <br />
                <p>
            <?php if($szBookingStatus=='archive'){ ?>
                    <a href="<?=__HOME_PAGE_URL__.'/userMultiAccess/'?>" ><?php echo t($t_base.'title/archived_booking_link');?></a>
            <?php } else { ?>				
                    <a href="<?=__HOME_PAGE_URL__?>" > <?php echo t($t_base.'title/comparing_rates_at_transporteca');?></a>
            <?php } ?>
                </p>
            </div>
        </div>	
	<?php 
}
function display_dangerous_cargo_popup($postSearchAry,$mode=false)
{
	$kWHSSearch = new cWHSSearch();
	$t_base = "LandingPage/";
	
	$iLanguage = getLanguageId();
//	if($iLanguage==__LANGUAGE_ID_DANISH__)
//	{
//		$szVariableName = '__IS_DANGEROUS_CARGO_DANISH__' ;
//	}
//	else
//	{
//		$szVariableName = '__IS_DANGEROUS_CARGO__' ;
//	}
        $szVariableName = '__IS_DANGEROUS_CARGO__' ;
	$szExplainUrl = $kWHSSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
	if(empty($szExplainUrl))
	{
		$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
	}
	$szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
	
	if($mode=='POP_UP')
	{
		$close_function_call = "update_booking_cargo_details('".$szBookingRandomNum."','POP_UP')";
	}
	else 
	{
		$close_function_call = "showHide('all_available_service')";
		$background = 'class="white-bg"';
	}
	?>
	<script type="text/javascript">
	$().ready(function(){
		 Custom.init();
	});
	</script>
	<div id="popup-bg" <?=$background?>></div>
	<div id="popup-container">
	<div id="loader" class="loader_popup_bg" style="display:none;">
		<div class="popup_loader"></div>				
			<img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
	</div>	
	<form id="dangerous_cargo_form" method="post" name="dangerous_cargo_form" action="">
		<div class="standard-popup-rk popup">	
			<p align="right" class="close-icon"><a heref="javascript:void(0);" onclick="<?=$close_function_call?>"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
			<h5><?=t($t_base.'title/rq1_popup_header')?></h5>
			<p><?=t($t_base.'title/rq1_popup_detail_text1');?> <a href="<?=$szExplainUrl?>" target="__blank" style="color:#FFFFFF;"><?=t($t_base.'title/rq1_popup_detail_text3')?></a>.</p>
			<p><?=t($t_base.'title/rq1_popup_detail_text2');?></p>
			<p><?=t($t_base.'title/rq1_popup_detail_text4');?></p>
			<p style="margin-top:14px;"><input class="styled" type="checkbox" value="1" name="addDangerCargoExpactedServiceAry[iSendNotification]" onblur="enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>','DC',this.id);" onclick ="enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>','DC',this.id);" id="iSendNotification_popup" > <?=t($t_base.'title/rk1_popup_checkbox_text');?></p>
			<?php
			$notify_me_class = "class='gray-button1'";
			if($_SESSION['user_id']<=0)
			{
				?>
				<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>					
				<div class="oh" style="margin-top:10px;">
					<p class="fl-20"><?=t($t_base.'fields/name');?></p>
					<p class="fl-80">
						<input type="text" id="szCustomerName_popup" onblur="enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>','DC',this.id);" onkeyup = "enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>','DC',this.id);" name="addDangerCargoExpactedServiceAry[szCustomerName]" value="<?=$postSearchResult['szCustomerName']?>">
						<span id="szCustomerName_popup_error" class="red_text" style="display:none;float:right;"></span>
					</p>
				</div>
				<?
			}
			else
			{
				$emailfieldtopspace="style='margin-top:10px;'";
				$kUser = new cUser();
				$kUser->getUserDetails($_SESSION['user_id']);
				if(empty($postSearchResult['szEmail']))
				{
					$postSearchResult['szEmail'] = $kUser->szEmail ;
					if(!empty($postSearchResult['szEmail']))
					{
						//$notify_me_class = "class='orange-button1'";
					//	$notify_me_onclick = "onclick=\"send_danger_cargo_email_forwarder('".$szBookingRandomNum."')\"";
					}
				}
				?>
				<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>					
				<?
			}
			?>
			<div class="oh" <?=$emailfieldtopspace;?>>
				<p class="fl-20"><?=t($t_base.'fields/email');?></p>
				<p class="fl-80">
					<input type="text" id="szCustomerEmail_popup" name="addDangerCargoExpactedServiceAry[szEmail]" onblur="enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>','DC',this.id);" onkeyup = "enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>','DC',this.id);" value="<?=$postSearchResult['szEmail']?>">
					<span id="szCustomerEmail_popup_error" class="red_text" style="display:none;float:right;"></span>
				</p>
			</div>
			<div class="new-button-container-topgap">
			<p align="center" class="button-container">		
				<a href="javascript:void(0);" <?=$notify_me_class?> <?=$notify_me_onclick?> id="notify_me_button" ><span><?=t($t_base.'fields/send_my_requirement');?></span></a>
			</p>
			<p align="center" class="button-container">	
				<a href="javascript:void(0);" class="orange-button2" id="go_back_change_cargo_button" onclick="<?=$close_function_call?>" ><span><?=t($t_base.'fields/go_back_and_change_cargo');?></span></a>
			</p>
			</div>
			<input type="hidden" name="booking_key" value="<?=$postSearchAry['szBookingRandomNum']?>">				
		</div>
	</form>
	</div>
<?
}
function display_cargo_exceed_popup($postSearchAry,$mode=false)
{
	$t_base = "LandingPage/";
	$kWHSSearch = new cWHSSearch();
	$maxCargoLength = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_DIMENSION_LENGTH__');
	$maxCargoWidth = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_DIMENSION_WIDTH__');
	$maxCargoHeight = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_DIMENSION_HEIGHT__');
		
	$maxCargoWeight = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_WEIGHT__');
		
	$fKgFactor = $kWHSSearch->getWeightFactor(2);
	$maxCargoWeight_pounds = round((float)$maxCargoWeight * $fKgFactor);
	$maxCargoWeight_pounds = (int)(($maxCargoWeight_pounds)/1000) * 1000;
	$szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
	
	if($mode=='POP_UP')
	{
		$close_function_call = "update_booking_cargo_details('".$szBookingRandomNum."','POP_UP')";
	}
	else
	{
		$close_function_call = "showHide('all_available_service')";
		$background = 'class="white-bg"';
	}
	?>
	<script type="text/javascript">
	$().ready(function(){
		 Custom.init();
	});
	</script>
	<div id="popup-bg" <?=$background?>></div>
	<div id="popup-container">
	<div id="loader" class="loader_popup_bg" style="display:none;">
		<div class="popup_loader"></div>				
			<img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
	</div>	
	<form id="cargo_exceed_form" method="post" name="cargo_exceed_form" action="">
		<div class="standard-popup-rk popup">	
			<p align="right" class="close-icon" ><a heref="javascript:void(0);" onclick="<?php echo $close_function_call;?>"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
			<h5><?=t($t_base.'title/rk1_popup_header')?></h5>
			<p><?=t($t_base.'title/rk1_popup_detail_text1');?>: </p>
			<p align="center">
				<?
					echo $maxCargoLength." cm x ".$maxCargoWidth." cm X ".$maxCargoHeight." cm<br /> (length x width x height)";						
				?>
			</p>
			<p align="center">
				<?
				echo number_format((float)$maxCargoWeight)." kg ~ ".number_format((float)$maxCargoWeight_pounds)." pounds "; 
				?>
			</p>
			<p><?=t($t_base.'title/rk1_popup_detail_text2');?></p>
			<p style="margin-top:14px;"><input class="styled" type="checkbox" value="1" onclick = "enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>',false,this.id);" name="addExpactedServiceAry[iSendNotification]" id="iSendNotification_popup" > <?=t($t_base.'title/rk1_popup_checkbox_text');?></p>
			<?php
			$notify_me_class = "class='gray-button1'";
			
			if($_SESSION['user_id']<=0)
			{
				?>
				<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>				
				<div class="oh" style="margin-top:10px;">
					<p class="fl-20"><?=t($t_base.'fields/name');?></p>
					<p class="fl-80">
						<input type="text" id="szCustomerName_popup"  onkeyup = "enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>',false,this.id);" onblur="enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>',false,this.id);" name="addExpactedServiceAry[szCustomerName]" value="<?=$postSearchResult['szCustomerName']?>">
						<span id="szCustomerName_popup_error" class="red_text" style="display:none;float:right;"></span>
					</p>
				</div>
				<?
			}
			else
			{
				$emailfieldtopspace="style='margin-top:10px;'";
				$kUser = new cUser();
				$kUser->getUserDetails($_SESSION['user_id']);
				if(empty($postSearchResult['szEmail']))
				{
					$postSearchResult['szEmail'] = $kUser->szEmail ;
					if(!empty($postSearchResult['szEmail']))
					{
						//$notify_me_class = "class='orange-button1'";
						//$notify_me_onclick = "onclick=\"send_quotation_to_forwarder('".$szBookingRandomNum."')\"";
					}
				}
				?>
				<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>					
				<?
			}
			?>
			<div class="oh" <?=$emailfieldtopspace;?>>
				<p class="fl-20"><?=t($t_base.'fields/email');?></p>
				<p class="fl-80">
					<input type="text" id="szCustomerEmail_popup" name="addExpactedServiceAry[szEmail]" onkeyup = "enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>',false,this.id);" onblur="enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>',false,this.id);" value="<?=$postSearchResult['szEmail']?>">
					<span id="szCustomerEmail_popup_error" class="red_text" style="display:none;float:right;"></span>
				</p>
			</div>
			<div class="new-button-container-topgap">
			<p align="center" class="button-container">	
				<a href="javascript:void(0);" <?=$notify_me_class?> <?=$notify_me_onclick?> id="notify_me_button" ><span><?=t($t_base.'fields/send_my_requirement');?></span></a>
			</p>
			<p align="center" class="button-container">	
				<a href="javascript:void(0);" class="orange-button2" id="go_back_change_cargo_button" onclick="<?php echo $close_function_call;?>" ><span><?=t($t_base.'fields/go_back_and_change_cargo');?></span></a>
			</p>
			</div>
			<input type="hidden" name="booking_key" value="<?=$postSearchAry['szBookingRandomNum']?>">				
		</div>
	</form>
	</div>
	<?
}
function html_entities_flag($value,$flag=false)
{
	//echo "ff";
	if(!$flag)
		$value=htmlentities($value,ENT_COMPAT, "UTF-8");
	else
		$value=$value;

	return $value;	

}
function serviceOnlinePopUpHeader($flag = null)
{
$class1='gray';
$class2='gray';
$class3='gray';
if($flag == 1)
{
	$class1='';
}
if($flag == 2)
{
	$class2='';
}
if($flag == 3)
{
	$class2='dark-gray';
	$class3='';
}
$t_base = "ForwardersCompany/popupViedo/";
?>
<div>
<h3 style="margin-bottom:10px;"><strong><?=t($t_base.'titles/get_your_service_online_in_three_simple_steps');?></strong></h3>	
<ul class="show-form-step">
     <li class="one <?=$class1?>" >Company Information</li>
     <li class="two <?=$class2?>" >Preferences</li>
     <li class="three <?=$class3?>" >Publish your services </li>
</ul>
<div>
<?php
}
function fillPopupDetailsDetails()
{
    if(!isset($_SESSION['popUpForm']))
    {
        $_SESSION['popUpForm'] = 1;
    }
?>
<script type="text/javascript" src="https://www.transporteca.com/evp/framework.php?div_id=evp-2bd3b4f08a54988563665d97c84bb5c4&id=dGVzdC0xLm1wNA%3D%3D&v=1361400004&profile=default"></script>
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/swfobject.js"></script>
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery.uploadify.v2.1.4.min.js"></script>		
	
<div id="showFillForm">
<div id="popup-bg"></div>
<div id="popup-container">
<div class="popup" style="width: 755px;margin-top: 10px;padding-right: 0px;" >
<p class="close-icon" align="right">
<a id="closePopUp" onclick="cancel_remove_popup_info('showFillForm','1');" href="javascript:void(0);">
<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
</a>
</p>
		
<style type="text/css">
    
    .profile-fields .field-container input[type="text"]
    {
        box-sizing: border-box;
        width: 50%;
    }
    #registered_company_details .profile-fields .field-container input[type="text"]
    {
        box-sizing: border-box;
        width: 100%;
    }
    #update_preference_control .profile-fields .field-container select {width:50%;min-width:50px;}
</style>
<div id="content" class="scroll-div" style="min-height:600px;height:auto;border:none;overflow:visible;">

<?php
	if(isset($_SESSION['popUpForm']))
	{
            SWITCH((int)$_SESSION['popUpForm'])
            {
                CASE 2:
                {	
                    require_once(__APP_PATH_ROOT__ ."/forwarders/popupPreferences.php");
                    BREAK;
                }
                CASE 3:
                {	
                    require_once(__APP_PATH_ROOT__ ."/forwarders/popVideoSeeHow.php");
                    BREAK;
                }
                DEFAULT:
                {	
                    $_SESSION['popUpForm'] = 1;
                    require_once(__APP_PATH_ROOT__ ."/forwarders/forwardercompanyPopup.php");
                    BREAK;
                }
            }
	}
?></div>
</div>
	</div>
</div>	
</div></div>
</div>	
</div></div>
<?
}

function remove_http_from_url($szUrl)
{
	if(!empty($szUrl))
	{
		$http_string = strchr($szUrl,'http://');
		$https_string = strchr($szUrl,'https://');
		
		if(!empty($http_string))
		{
			return str_replace('http://',"",$szUrl);
		}
		else if(!empty($https_string))
		{
			return str_replace('https://',"",$szUrl);
		}
		else 
		{
			return $szUrl ;
		}

	}
}

function display_no_pricing_available_popup($szBookingRandomNum)
{
	$t_base_confimation = "BookingConfirmation/";
	if($mode=='POP_UP')
	{
		$close_function_call = "";
	}
	else
	{
		$close_function_call = "showHide('all_available_service')";
		$background = 'class="white-bg"';
	}
	
	$_SESSION['came_from_landing_page'.'_'.$szBookingRandomNum] = 1;
	$page_url = __SELECT_SERVICES_URL__.'/'.$szBookingRandomNum.'/';
	?>
	<div id="popup-bg"></div>
		<div id="popup-container">			
			<div class="popup abandon-popup">
			<p class="close-icon" align="right">
			<a onclick="update_booking_cargo_details('<?=$szBookingRandomNum?>','RECALCULATE_PRICING_ERROR');" href="javascript:void(0);">
			<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
			</a>
			</p>
			<h5><?=t($t_base_confimation.'title/cargo_exceeding_capicity_header')?></h5>
			<p><?=t($t_base_confimation.'title/cargo_exceeding_capicity_details_1')?></p>
			<p><?=t($t_base_confimation.'title/cargo_exceeding_capicity_details_2')?></p>
			<br>
			<div class="oh">
				<p align="center">
					<a href="javascript:void(0);" onclick="update_booking_cargo_details('<?=$szBookingRandomNum?>','RECALCULATE_PRICING_ERROR')" class="button1"><span><?=t($t_base_confimation.'fields/back')?></span></a>
					<a href="javascript:void(0);" onclick="redirect_url('<?=$page_url?>')" class="button1"><span><?=t($t_base_confimation.'fields/show_option')?></span></a>
				</p>
				</div>
		</div>
	</div>				
		<?
}

function previewLandingPage()
{
	?>
 	
	<div id="hsbody" class="search-option">
			<div class="search-new">
				<h6 align="center">Start Here</h6>
				<form action="" id="start_search_form" name="start_search_form" method="post">
					<div class="fields">
						<p>
							<select name="landingPageAry[szOriginCountry]" class="styled" id="szOriginCountry">
								<option value="">Shipping From</option>
							</select>
						</p>
						<p>
							<select name="landingPageAry[szDestinationCountry]" class="styled" id="szDestinationCountry">
								<option value="">Shipping To</option>
							</select>
							<input name="landingPageAry[szBookingRandomNum]" type="hidden" value="" id="szBookingRandomNum">
							<input name="landingPageAry[iCheck]" type="hidden" value="1" id="iCheck">
						</p>
						<p>
						<a href="javascript:void(0);" class="orange-button2" onclick = "compare_validate_home_page()" tabindex="3"><span>COMPARE SCHEDULES &AMP; PRICES</span></a>
						</p>
					</div>
				</form>
				<div class="text">
					<p class="book_in_minutes">Compare and Book in minutes</p>
					<ul>
						<li class="one">Complete your requirements and get instant overview of freight forwarder shipping options</li>						
						<li class="two">Select forwarder and service based on price, timing and reviews in the freight price calculator</li>						
						<li class="three">Book directly with the forwarder and pay securely online</li>						
					</ul>	
					<p>You immediately receive your booking confirmation and invoice - the freight forwarder takes care of the rest.</p>				
				</div>
			</div>
			<div class="home-new">
				<h2>The easiest way to book with a freight forwarder</h2>
				<p>Here to help businesses save time by making it easy to find and book international ocean freight. If it is more than a letter and less than a full container, you have come to the right place!</p>
							
				<div class="intro_video">	
							
				<div align="center" style="width:500px;height:280px;" id="evp-c4e48429cc58c6896f1cb705d41ec6da-wrap" class="evp-video-wrap"></div>
				<script type="text/javascript" src="https://www.transporteca.com/evp/framework.php?div_id=evp-c4e48429cc58c6896f1cb705d41ec6da&id=dHJhbnNwb3J0ZWNhLWludHJvZHVjdGlvbi0xLm1wNA%3D%3D&v=1352203827&profile=default"></script>
				<script type="text/javascript"><!--
				_evpInit('dHJhbnNwb3J0ZWNhLWludHJvZHVjdGlvbi0xLm1wNA==[evp-c4e48429cc58c6896f1cb705d41ec6da]');//-->
				</script>
							  <!-- <img src="images/see-yourself-transporteca.jpg" alt="" title="" /> -->
			 </div>					
				<h3>Benefits of registering with Transporteca</h3>
				<div class="benefits">					
					<div class="first common">
						<h5>Book online</h5>
						<p>Compare, book, get immediate confirmation, and keep track of it all in one place. You can even share and repeat bookings.</p>
					</div>
					<div class="common">
						<h5>Peace of mind</h5>
						<p>When you book and pay securely online, you know that the forwarder has everything needed to deliver on time.</p>
					</div>
					<div class="last common">
						<h5>Support transparency</h5>
						<p>Help us develop the initiative to cover more trades. With your registration, we can get more forwarders online.</p>
					</div>
											<p align="center" class="button"><a href="http://www.a.transport-eca.com/createAccount/" class="register-button">&nbsp;</a></p>
									</div>
			</div>
		</div>
 	<?php
 }
 
 function getAddressDetails($szLatitude,$szLongitude,$idCountry=false)
 {
    $kWHSSearch = new cWHSSearch();
    $url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=".round($szLatitude,6).",".round($szLongitude,6)."&sensor=false"; 
    //$url = "http://ws.geonames.org/countryCode?type=JSON&lat=".$szLatitude."&lng=".$szLongitude;
    $geocode = $kWHSSearch->curl_get_file_contents($url); 
    $response = json_decode($geocode,true); 
    $iLastounter = count($response['results']); 
    $specialCountryAry = array();
    $specialCountryAry['0'] = 100;  //Hong kong
    $specialCountryAry['1'] = 25;  //Bermuda
    $specialCountryAry['2'] = 138;  //Malta
    $specialCountryAry['3'] = 92;  //Guernsey 
    $responseArr=$response['results'][$iLastounter-1]['address_components'];  
    $countryCode='';
    $countryName='';
    if(!empty($responseArr))
    { 
        foreach($responseArr as $key=>$responseArrs)
        { 
            if($responseArrs['types'][0]=='country')
            {
                $countryCode=$responseArrs['short_name'];
                $countryName=$responseArrs['long_name']; 
            }
        }
    } 
    if(!empty($countryCode))
    {
        $kWHSSearch = new cWHSSearch();
        $countryDetailAry = array();        
        $countryDetailAry = $kWHSSearch->setCountryNameISO($countryCode); 
        if(!empty($countryDetailAry['szCountryISO']))
        {
            $countryCode = $countryDetailAry['szCountryISO'];
        }
    }
    $countryDetailAry = array();
    $countryDetailAry['szCountryName'] = $countryName ;
    $countryDetailAry['szCountryCode'] = $countryCode ;
    
    return $countryDetailAry ;
 }
 
 function constantApiKey()
 {
    require_once( __APP_PATH_CLASSES__ . "/warehouseSearch.class.php");
    $kWhsSearch = new cWHSSearch();
    $szYahooApiKey = $kWhsSearch->getManageMentVariableByDescription('__YAHOO_GEOCODE_API_KEY__');
    $szGoogleApiKey = $kWhsSearch->getManageMentVariableByDescription('__GOOGLE_MAP_V3_API_KEY__');
    define("__YAHOO_GEOCODE_API_KEY__",$szYahooApiKey);
    define("__GOOGLE_MAP_V3_API_KEY__",$szGoogleApiKey);
    //define("__GOOGLE_MAP_CLIENT_ID__","48839239952.apps.googleusercontent.com");
    define("__GOOGLE_MAP_JSON_URL__","http://maps.googleapis.com/maps/api/js?v=3&amp;key=".__GOOGLE_MAP_V3_API_KEY__."&sensor=true");
 }
 
 
 function checkPaypalTranscationAmount($idTranscation)
 {
 
 	if(__PAYPAL_LIVE__==true)
 	{
			$url = __PAYPAL_PAYMENT_URL_LIVE__;
			$auth_token=__PAYPAL_PAYMENT_URL_TOKEN_LIVE__;
			$host=__PAYPAL_PAYMENT_URL_LIVE_HOST__;
 	}
	else
	{
		$url = __PAYPAL_PAYMENT_URL_SANDBOX__; 
		$auth_token=__PAYPAL_PAYMENT_URL_SANDBOX_TOKEN__;
		$host=__PAYPAL_PAYMENT_URL_SANDBOX_HOST__;
	}
	// read the post from PayPal system and add 'cmd'
	$req = 'cmd=_notify-synch';
	 
	$tx_token = $idTranscation;
	$req .= "&tx=$tx_token&at=$auth_token";		
			
 	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "$url");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,__LIVE_SSL_FLAG__);
	//set cacert.pem verisign certificate path in curl using 'CURLOPT_CAINFO' field here,
	//if your server does not bundled with default verisign certificates.
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: $host"));
	$res = curl_exec($ch);
	curl_close($ch);
	if($res)
	{
		$lines = explode("\n", $res);
	    $keyarray = array();
	    if (strcmp ($lines[0], "SUCCESS") == 0) 
	    {
	        for ($i=1; $i<count($lines);$i++)
	        {
	        	list($key,$val) = explode("=", $lines[$i]);
	        	$keyarray[urldecode($key)] = urldecode($val);
	    	}
	    	
	    	return $keyarray;
	    }
	    else
	    {
	    	return $keyarray;
	    }
	}
	else
	{
		return array();
	}
 }
 
function get_ip_details($ip_address)  
{  
	$ip_address = $_SERVER['REMOTE_ADDR'] ;
    //function to find country, city and country code from IP.  
    //verify the IP.  
   // ip2long($ip_address)== -1 || ip2long($ip_address) === false ? trigger_error("Invalid IP ".$ip_address, E_USER_ERROR) : "";  
  
    //get the JSON result from hostip.info  
    $result = file_get_contents("http://api.hostip.info/get_json.php?ip=".$ip_address);  
  
    $result = json_decode($result, 1);  
  
    //return the array containing city, country and country code  
    return $result;  
  
}  

function getMonthName($iLanguage,$iMonth,$admin_flag=false)
{ 
    if(!$admin_flag)
    {
        $iLanguage = getLanguageId();
    }
    
    if($iMonth>0 && $iMonth<=12)
    {
        $iMonth = (int)$iMonth; 
        
        $kConfig = new cConfig();
        $configLangArr=$kConfig->getConfigurationLanguageData('__TABLE_MONTH_NAME__',$iLanguage);
        return $configLangArr[$iMonth][$iMonth];
                
//        $EnglishMonthNameAry = array(1=>'January',2=>'February',3=>'March',4=>'April',5=>'May',6=>'June',7=>'July',8=>'August',9=>'September',10=>'October',11=>'November',12=>'December');
//        $DanishMonthNameAry = array(1=>'januar',2=>'februar',3=>'marts',4=>'april',5=>'maj',6=>'juni',7=>'juli',8=>'august',9=>'september',10=>'oktober',11=>'november',12=>'december');
//        $swedishMonthNameAry = array(1=>'januar',2=>'februar',3=>'marts',4=>'april',5=>'maj',6=>'juni',7=>'juli',8=>'august',9=>'september',10=>'oktober',11=>'november',12=>'december');

        
        if($iLanguage==__LANGUAGE_ID_DANISH__)
        {
            return $DanishMonthNameAry[$iMonth];
        }
        else
        {
            return $EnglishMonthNameAry[$iMonth];
        }
    }
}
 
function display_zooz_api_error_popup($errorMessage)
{
	$t_base = "BookingConfirmation/";
	?>	
	<div id="popup-bg"></div>
	 <div id="popup-container">			
		<div class="popup abandon-popup">
		<p class="close-icon" align="right">
		<a onclick="redirect_url('<?=__HOME_PAGE_URL__?>');" href="javascript:void(0);">
		<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
		</a>
		</p>
		<h5><?=t($t_base.'title/zooz_api_error');?></h5>
		<p><?=t($t_base.'title/zooz_api_error_details');?> : <br><br> <?php echo $errorMessage; ?></p>
		<br>
		<div class="oh">
			<p align="center">
				<a href="javascript:void(0);" onclick="redirect_url('<?=__HOME_PAGE_URL__?>')" class="button1"><span><?=t($t_base.'title/new_search');?></span></a>
			</p>
		</div>
		</div>
	</div>	
    <?php
} 
function display_payment_gateway_disabled_popup()
{
    $t_base = "home/";
    ?>	
    <div id="popup-bg"></div>
     <div id="popup-container">			
        <div class="popup abandon-popup">
            <h5><?=t($t_base.'header/paymet_method');?></h5>
            <p><?=t($t_base.'header/paymet_method_message');?></p>
            <br>
            <div class="oh">
                <p align="center">			
                    <a href="javascript:void(0);" onclick="showHide('change_price_div');" class="button1"><span><?=t($t_base.'header/ok');?></span></a>			
                </p>
            </div>
        </div>
    </div>	
    <?php
} 
function display_new_services_listing($postSearchAry,$searchResult,$t_base,$countryKeyValueAry,$forwarder_flag=false,$szCourierCalculationLogString=false) //WTW
{	  
    $classNameSearchMiniVersion ="";
    if($postSearchAry['iSearchMiniVersion']==5 || $postSearchAry['iSearchMiniVersion']==6)
    {
        $kCourierServices= new cCourierServices();
        $bDTDTradesFlag = false;
        if($kCourierServices->checkFromCountryToCountryExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true))
        { 
            $classNameSearchMiniVersion='class="search-mini-container version-'.$postSearchAry['iSearchMiniVersion'].'"';
        }
    }
    
    $bDisplaySearchForm = true;
    if($postSearchAry['iQuickQuote']==__QUICK_QUOTE_SEND_QUOTE__)
    {
        //@To Do
        $bDisplaySearchForm = false;
    } 
    if($bDisplaySearchForm)
    {
        echo '<div id="service-search-form-container" '.$classNameSearchMiniVersion.'>';
        echo display_new_search_form($kBooking,$postSearchAry,false,true);
        echo '</div>';   
    }
    
    if(!empty($_SESSION['load-booking-details-page-for-booking-id']) && $_SESSION['load-booking-details-page-for-booking-id']==$postSearchAry['szBookingRandomNum'])
    { 
        $kRegisterShipCon = new cRegisterShipCon();
        $kWHSSearch=new cWHSSearch();
        $kConfig=new cConfig();

        $idBooking = $postSearchAry['id'];

        $idWTW = $postSearchAry['idUniqueWTW']; 
        $t_base_select_service = "SelectService/";  
        if($_SESSION['iSessionPageNumber'][$postSearchAry['szBookingRandomNum']]>0)
        {
            $postSearchAry['iPageNumber'] = $_SESSION['iSessionPageNumber'][$postSearchAry['szBookingRandomNum']];
        }
        else
        {
            $postSearchAry['iPageNumber'] = 1;
        }
        
        ?> 
        <div id="service-listing-container">
            <?php 
            if($postSearchAry['iQuickQuote']==__QUICK_QUOTE_SEND_QUOTE__)
            {
                echo display_quick_quotes_searvice_details($postSearchAry);
            }
            else
            {
                echo display_new_searvice_listing($postSearchAry,$searchResult,$t_base_select_service,$countryKeyValueAry,false,true,false,$szCourierCalculationLogString);
            }
         ?>
        </div>
        <script type="text/javascript">
            toggleServiceList('<?php echo $idWTW; ?>','<?=__NEW_BOOKING_DETAILS_PAGE_URL__?>','<?=$_SESSION['user_id']?>',1,1);
        </script>
        <div id="booking_details_container"  class="booking-page clearfix">
            <?php echo display_booking_details($postSearchAry,$kRegisterShipCon);?>
        </div>
        <?php
    }
    else
    {
        echo '<div id="service-listing-container">';
        echo display_new_searvice_listing($postSearchAry,$searchResult,$t_base,$countryKeyValueAry,$forwarder_flag,false,false,$szCourierCalculationLogString);
        echo '</div>';
        ?>
        <div id="selected-service-listing-container" style="display:none;"></div>
        <div id="booking_details_container" style="display:none;" class="booking-page clearfix">
            <?php echo display_booking_details($postSearchAry,$kRegisterShipCon);?>
        </div>
        <?php
    } 
    ?> 
    <div id="update_cargo_details_container_div"  style="display:none;">
        <?php echo update_cargo_dimension($kBooking,$postSearchAry); ?>
    </div> 
    <div id="rating_popup"></div>  
    <div id="additional_service_list_container" style="display:none;">
        <h3 class="service-waiting-text"><?=t($t_base.'fields/service_waiting_text');?></h3>
    </div>  
    <?php
}  
function display_new_searvice_listing($postSearchAry,$searchResult,$t_base,$countryKeyValueAry,$forwarder_flag=false,$selected_flag=false,$management_flag=false,$szCourierCalculationLogString=false)
{ 
    
    
    $kWHSSearch=new cWHSSearch();
    $kCourierServices= new cCourierServices(); 
                
    $kConfig=new cConfig();
    $currencyAry = array();
    $currencyAry = $kConfig->getBookingCurrency(false,true); 
    $szBookingRandomNum = $postSearchAry['szBookingRandomNum'] ; 
                  
    $bDisplayBooknowButton = true;
    if(isset($_SESSION['admin_test_search_'.$szBookingRandomNum]))
    {
        /* When admin clicked on TEST SEARCH button under Pending Tray=>Task=>Remind Pane than we do not allow user to changes anything in original booking.
        * So this is why we hiding Search/Update button in this case. 
        */ 
        $bDisplayBooknowButton = false;
    }
    if((int)$postSearchAry['iPageNumber']==0)
    {
        $postSearchAry['iPageNumber'] = 1;
    } 
    $kCourierService_new = new cCourierServices();  
    $modeTransport=false;
    $bDTDTradesFlag = false;
    if($kCourierServices->checkFromCountryToCountryExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true))
    { 
        $bDTDTradesFlag = true;
    }

    if($kCourierServices->checkFromCountryToCountryExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry']))
    {
        $modeTransport=true;
    }  
    $searchResult = fetch_selected_services($searchResult,$kCourierService_new,$postSearchAry);  
    
    if($bDTDTradesFlag && $modeTransport)
    {
        //if all lines is truck icon then default sorting should be ascending on price. 
        $searchResult = sortArray($searchResult,'fDisplayPrice',false,'iDaysBetween',false,'iNumRating',true,true);  
    }   
    $kServices = new cServices();
    $iPrivateShipping = false;
    if($_SESSION['user_id']>0)
    {
        $kUser = new cUser();
        $kForwarder = new cForwarder();
        $kUser->getUserDetails($_SESSION['user_id']);
        
        if($kUser->iPrivate==1)
        {
            $iPrivateShipping = true;
        } 
    }
    else if($postSearchAry['iPrivateShipping']==1)
    {
        $iPrivateShipping = true;
    }
    
    $szContainerClass = "";
    $iQuickQuoteBooking = false;
    if($postSearchAry['iQuickQuote']==__QUICK_QUOTE_SEND_QUOTE__)
    {
        $szContainerClass = " quick-quotes-results";
        $iQuickQuoteBooking = 1;
    } 
    
    if(empty($searchResult))
    { 
        $kConfig_new = new cConfig(); 
        $countryAry = array();
        $searchCountryAry = array();
                
        $kConfig_new->loadCountry($postSearchAry['idOriginCountry']);  
        $szCountryStrUrl = $kConfig_new->szCountryISO;
            
        $kConfig_new = new cConfig(); 
        $kConfig_new->loadCountry($postSearchAry['idDestinationCountry']);  
        $szCountryStrUrl .= $kConfig_new->szCountryISO;
        
        if((int)$postSearchAry['id']>0)
        {
            
            $kWHSSearch=new cWHSSearch();
            $szUserDidNotGetAnyOnlineService = $kWHSSearch->getManageMentVariableByDescription('__USER_DID_NOT_GET_ANY_ONLINE_SERVICES__');
    
            $res_ary=array(); 
            $res_ary['szInternalComment'] = $szUserDidNotGetAnyOnlineService ;
            if(!empty($res_ary))
            {
                $update_query = '';
                foreach($res_ary as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            } 
            $update_query = rtrim($update_query,",");
            if($kBooking->updateDraftBooking($update_query,$postSearchAry['id']))
            {

            }
        }
                
        $redirectQuoteUrl  =  __BOOKING_GET_QUOTE_PAGE_URL__."/".$szBookingRandomNum."/".__QUOTATION_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;  
?>
        <script type="text/javascript">
            redirect_url('<?php echo $redirectQuoteUrl; ?>');
        </script>
        <?php
    }
    $iLanguage = getLanguageId(); 
    $kService = new cServices(); 
    $dummyServiceAry = array();
    $dummyServiceAry = $kService->getDummyServicesList($postSearchAry);
                
    $iDisplayLCLDummyService = $kService->iDisplayLCLDummyService;
    $iDisplayCouirierDummyService = $kService->iDisplayCouirierDummyService; 
    ?>
    <script type="text/javascript">
        Custom.init();
    </script>
    <div class="results<?php echo $szContainerClass; ?>"> 
        <div class="head clearfix">
            <div class="logo" onclick="sort_service_list('szDisplayName','<?php echo $szBookingRandomNum; ?>','<?php echo $_REQUEST['lang']?>');" style="cursor: pointer;"><span class="service-sorter-span" id="service_sorter_szDisplayName"><?php echo t($t_base.'fields/mode_of_transport'); ?></span></div>
            <div class="rating" onclick="sort_service_list('iNumRating','<?php echo $szBookingRandomNum; ?>','<?php echo $_REQUEST['lang']?>');" style="cursor: pointer;"><span class="service-sorter-span" id="service_sorter_iNumRating"><?php echo t($t_base.'fields/rating'); ?></span></div>
            <div class="cut-off"><?php if($postSearchAry['idServiceType']==__SERVICE_TYPE_PTD__){?><?=t($t_base.'fields/packing');?> <?php }else{?> <?=t($t_base.'fields/packing');?> <?php }?></div>
            <?php if($bDTDTradesFlag){ ?>  
                <div class="delivery" onclick="sort_service_list('iCuttOffDate_time','<?php echo $szBookingRandomNum; ?>','<?php echo $_REQUEST['lang']?>');" style="cursor: pointer;"><span class="service-sorter-span" id="service_sorter_iCuttOffDate_time"><?php echo ucwords(t($t_base.'fields/pick_up'));?></span></div>
                <div class="days" onclick="sort_service_list('iAvailabeleDate_time','<?php echo $szBookingRandomNum; ?>','<?php echo $_REQUEST['lang']?>');" style="cursor: pointer;"><span class="service-sorter-span" id="service_sorter_iAvailabeleDate_time"><?php echo t($t_base.'fields/delivery');?></span></div>
            <?php } else { ?>
                <div class="delivery" onclick="sort_service_list('iAvailabeleDate_time','<?php echo $szBookingRandomNum; ?>','<?php echo $_REQUEST['lang']?>');" style="cursor: pointer;"><span class="service-sorter-span" id="service_sorter_iAvailabeleDate_time"><?php echo t($t_base.'fields/delivery');?></span></div>
                <div class="days" onclick="sort_service_list('iDaysBetween','<?php echo $szBookingRandomNum; ?>','<?php echo $_REQUEST['lang']?>');" style="cursor: pointer;"><span class="service-sorter-span" id="service_sorter_iDaysBetween"><?php echo ucwords(t($t_base.'fields/days'));?></span></div>
            <?php } ?> 
            <div class="price"> 
                <span class="service-sorter-span" id="service_sorter_fDisplayPrice" onclick="sort_service_list('fDisplayPrice','<?php echo $szBookingRandomNum; ?>','<?php echo $_REQUEST['lang']?>');" style="cursor: pointer;"><?php echo t($t_base.'fields/all_in_price'); ?> </span> <?php if(!$management_flag && !$iQuickQuoteBooking){?>
                <select name="idBookingCurrency" id="idBookingCurrency" onfocus="on_focus_currency_dropdown(this.id)" class="styled" onchange="update_booking_currency(this.value,'<?php echo $szBookingRandomNum; ?>','<?php echo $iQuickQuoteBooking; ?>')">
                    <?php
                        if(!empty($currencyAry))
                        {
                            foreach($currencyAry as $currencyArys)
                            {
                                ?>
                                <option value="<?php echo $currencyArys['id']?>" <?php echo (($currencyArys['id']==$searchResult[0]['idCurrency'])?'selected':''); ?>><?=$currencyArys['szCurrency']?></option>
                                <?php
                            }
                        }
                     ?>
                </select>
                <?php } ?> 
            </div>
        </div>
        <?php 
        
        if(!empty($searchResult))
        {
            $google_map_anchor_style="style='text-decoration:none;color:black;font-style:normal;font-size:12px;'";
            $minRatingToShowStars = $kWHSSearch->getManageMentVariableByDescription('__MINIMUM_RATING_TO_SHOW_RATING_STARS__');
            $tr_counter = 1;
            $filterServiceAry = array();
            $courier_service_ctr = 0;
            $lcl_service_counter = 0;
            $iOnlyCartonServiceDisplayedCtr = 0;
            $showOfferFlag=true; 
            $cheapesLclServiceAry = array();
            $cheapesCourierServiceAry = array();
            $rail_service_counter=0;
            foreach($searchResult as $searchResults)
            {   
                $LCDTDQuantityFlag=0;   
                $fTotalPriceDisplay = $searchResults['fDisplayPrice'];

                $start = date("Y-m-d",strtotime(str_replace("/","-",trim($searchResults['dtCutOffDate']))));
                $end = date("Y-m-d",strtotime(str_replace("/","-",trim($searchResults['dtAvailableDate']))));
                $days_between = ceil(abs(strtotime($end) - strtotime($start)) / 86400);

                $szClassName = "odd";
                if($tr_counter%2==0)
                {
                    $szClassName = "";
                }
                $iLanguage = getLanguageId(); 
                $dtCutOffMonth=date("m",strtotime(str_replace("/","-",trim($searchResults['dtCutOffDate'])))); 
                $dtCutOffMonthName=getMonthName($iLanguage,$dtCutOffMonth,true);
                $dtCutOffDay=date("j",strtotime(str_replace("/","-",trim($searchResults['dtCutOffDate'])))); 
                $dtCutOffDate=$dtCutOffDay.". ".$dtCutOffMonthName;  

                $dtAvailableDay=date("j",strtotime(str_replace("/","-",trim($searchResults['dtAvailableDate'])))); 
                $dtAvailableMonth=date("m",strtotime(str_replace("/","-",trim($searchResults['dtAvailableDate'])))); 
                $dtAvailableMonthName=getMonthName($iLanguage,$dtAvailableMonth,true);
                $dtAvailableDate=$dtAvailableDay.". ".$dtAvailableMonthName;  

                $dtCutOffDate_str = date("j F",strtotime(str_replace("/","-",trim($searchResults['dtCutOffDate'])))); 
                $dtAvailableDate_str = date("j F",strtotime(str_replace("/","-",trim($searchResults['dtAvailableDate']))));
                
                $iCuttOffDateTime = strtotime($dtCutOffDate_str) ;
                $iAvailableDateTime = strtotime($dtAvailableDate_str) ;

                if($searchResults['idCourierProvider']>0) 
                {
                    $packingArr = $kCourierServices->getCourierProviderProductList($searchResults['idCourierProvider'],$searchResults['idCourierProviderProductid'],$iLanguage); 
                    
                    $idCourierPacking = $packingArr[0]['id'];
                    $szFilterString = $searchResults['idForwarder']."-".$iCuttOffDateTime."-".$iAvailableDateTime."-".$days_between."-".round((float)$searchResults['fDisplayPrice'],2)."-".$searchResults['idCourierProvider']."-".$searchResults['idCourierProviderProductid']."-".$idCourierPacking;
                } 
                else
                {
                    $szFilterString = $searchResults['idForwarder']."-".$iCuttOffDateTime."-".$iAvailableDateTime."-".$days_between."-".round((float)$searchResults['fDisplayPrice'],2)."-".$searchResults['idWarehouseFrom']."-".$searchResults['idWarehouseTo'];
                }                   
                if(empty($filterServiceAry) || !in_array($szFilterString,$filterServiceAry))
                {
                    $filterServiceAry[] = $szFilterString ;  
                    
                    $fTotalPriceCustomerCurrency = $searchResults['fDisplayPrice']; 
                    $fTotalVatCustomerCurrency = $searchResults['fTotalVat'];
                    
                    if($postSearchAry['iQuickQuote']==__QUICK_QUOTE_SEND_QUOTE__)
                    { 
                        $iHandlingFeeApplicable = $postSearchAry['iHandlingFeeApplicable'];
                        $idManualFeeCurrency = $postSearchAry['idHandlingFeeLocalCurrency'];
                        $fTotalManualFee = $postSearchAry['fTotalHandlingFeeLocalCurrency'];
                        $fTotalManualFeeUSD = $postSearchAry['fTotalHandlingFeeUSD'];
                        $fCustomerCurrencyExchangeRate = $postSearchAry['fExchangeRate'];
                
                        if($iHandlingFeeApplicable==1)
                        {
                            if($idManualFeeCurrency == $searchResults['idCurrency'])
                            {
                                $fTotalManualFeeCustomerCurrency = $fTotalManualFee;
                            } 
                            else if($searchResults['idCurrency']==1)
                            {
                                $fTotalManualFeeCustomerCurrency = $fTotalManualFeeUSD;
                            }
                            else if($fCustomerCurrencyExchangeRate>0)
                            {
                                $fTotalManualFeeCustomerCurrency = round((float)($fTotalManualFeeUSD/$fCustomerCurrencyExchangeRate));
                            }  

                            /*
                            * If Manual fee currency is not same as customer currency then currency mark-up should be applied on this 
                            */ 
                            if($searchResults['idForwarderCurrency']!=$searchResults['idCurrency'])
                            {
                                /*
                                * Adding currency mark-up on Manual Fee
                                */
                                $fTotalManualFeeCustomerCurrency = round((float)($fTotalManualFeeCustomerCurrency + ($fTotalManualFeeCustomerCurrency * .025))); 
                            }  
                            
                            $fTotalPriceCustomerCurrency = round((float)($fTotalPriceCustomerCurrency + $fTotalManualFeeCustomerCurrency));
                            $fTotalVatCustomerCurrency = round((float)($fTotalPriceCustomerCurrency * $searchResults['fVATPercentage'] * 0.01),4); 
                        } 
                        $bDisplayBooknowButton = false;
                        $szClassName = "odd";
                    }
                    
                    if($iPrivateShipping)
                    {
                        //if user is Private, then add VAT to the price we show
                        $fTotalDisplayPrice = $fTotalPriceCustomerCurrency + $fTotalVatCustomerCurrency;
                    } 
                    else
                    {
                        $fTotalDisplayPrice = $fTotalPriceCustomerCurrency;
                    }  
                    $fTotalBookingPrice = number_format_custom((float)$fTotalDisplayPrice,$iLanguage); 
                    $iCourierBooking = 0;
                    $packingText= t($t_base.'fields/pallets_and_cartons'); 
                    $packingText_only= t($t_base.'fields/only'); 
                
                    if($searchResults['idCourierProvider']>0)
                    {  
                        $iCourierBooking = 1;  
                        $courier_service_ctr++;

                        $packingText='';
                
                        if($packingArr[0]['idPackingType'] == __COURIER_PACKAGING_TYPE_PALLETS_AND_CARTONS__)
                        {
                            $packingText = ucfirst($packingArr[0]['szPacking']);
                        } 
                        else
                        {
                            $packingText = $packingText_only." ".strtolower($packingArr[0]['szPacking']);
                        }  
                        $idPackingType = $packingArr[0]['idPackingType'];
                        if($idPackingType==1)
                        {
                            $iOnlyCartonServiceDisplayedCtr++;
                        }
                        if($modeTransport)
                        { 
                            $showOfferFlag=false;
                            $szForwarderImage = "Truck.png"; 
                        }
                        else
                        {  
                            $szForwarderImage = "Plane.png"; 
                        } 
                    }
                    else
                    {  
                        
                        $railTransportAry = array();
                        $railTransportAry['idForwarder'] = $searchResults['idForwarder'];
                        $railTransportAry['idFromWarehouse'] = $searchResults['idWarehouseFrom'];
                        $railTransportAry['idToWarehouse'] = $searchResults['idWarehouseTo'];  
                        if($kServices->isRailTransportExists($railTransportAry))
                        {
                            $szForwarderImage = "Rail.png"; 
                            $bDTDTradesFlag = false;
                            $rail_service_counter++;
                        }
                        else if($bDTDTradesFlag)
                        { 
                            if($postSearchAry['fCargoVolume']>0.6 || $postSearchAry['fCargoWeight']>270)
                            {
                                $packingText = t($t_base.'fields/euro_pallets'); 
                                $LCDTDQuantityFlag=2;
                            }
                            else
                            {
                                $packingText = t($t_base.'fields/half_euro_pallets'); 
                                $LCDTDQuantityFlag=1;
                            }
                            $szForwarderImage = "Truck.png";
                            $lcl_service_counter++; 
                        } 
                        else 
                        {  
                            
                            $lcl_service_counter++; 
                            $szForwarderImage = "Ship.png"; 
                        }  
                    } 
                    
                    $displayServiceLineAry = array();
                    $displayServiceLineAry = $searchResults;
                    $displayServiceLineAry['iMinRatingToShowStars'] = $minRatingToShowStars;
                    $displayServiceLineAry['szForwarderImage'] = $szForwarderImage;
                    $displayServiceLineAry['szPackingType'] = $packingText;
                    $displayServiceLineAry['szAvailableDateStr'] = $dtAvailableDate;
                    $displayServiceLineAry['szCutoffDateStr'] = $dtCutOffDate; 
                    $displayServiceLineAry['iDaysBetween'] = $days_between;
                    $displayServiceLineAry['szCustomerCurrency'] = $searchResult[0]['szCurrency'];
                    $displayServiceLineAry['fTotalBookingPrice'] = $fTotalBookingPrice;
                    $displayServiceLineAry['iCourierBooking'] = $iCourierBooking;
                    $displayServiceLineAry['idPackingType'] = $idPackingType;
                    $displayServiceLineAry['LCDTDQuantityFlag'] = $LCDTDQuantityFlag;
                    $displayServiceLineAry['fTotalDisplayPrice'] = $fTotalDisplayPrice; 
                    if($bDTDTradesFlag)
                    {
                        $displayServiceLineAry['iDTDTradesFlag'] = 1; 
                    }
                    else
                    {
                        $displayServiceLineAry['iDTDTradesFlag'] = 0; 
                    }
                
                    if($iCourierBooking==1)
                    {
                        if(empty($cheapesCourierServiceAry))
                        {
                            $cheapesCourierServiceAry = $displayServiceLineAry;
                        }
                        else if($cheapesCourierServiceAry['fTotalDisplayPrice']>$fTotalDisplayPrice)
                        {
                            $cheapesCourierServiceAry = $displayServiceLineAry;
                        }
                    }
                    else
                    {
                        if(empty($cheapesLclServiceAry))
                        {
                            $cheapesLclServiceAry = $displayServiceLineAry;
                        }
                        else if($cheapesLclServiceAry['fTotalDisplayPrice']>$fTotalDisplayPrice)
                        {
                            $cheapesLclServiceAry = $displayServiceLineAry;
                        }
                    } 
                    display_service_line($displayServiceLineAry,$postSearchAry,$tr_counter,$t_base,$bDisplayBooknowButton,$forwarder_flag);
                    $tr_counter++;
                } 
            } 
            
            if($iDisplayLCLDummyService==1 && !empty($dummyServiceAry['lcl']) && !empty($cheapesLclServiceAry))
            {
                /*
                * Display LCL dummy services
                */
                foreach($dummyServiceAry['lcl'] as $dummyServiceArys)
                {
                    $displayServiceLineAry = $cheapesLclServiceAry;
                    
                    $dtAvailableTime = strtotime(str_replace("/","-",trim($displayServiceLineAry['dtAvailableDate']))); 
                    $dtDeliveryDateTime = strtotime("+".$dummyServiceArys['iTransitTimeIncrease']." day", $dtAvailableTime);
                    $dtDeliveryDate = date('Y-m-d',$dtDeliveryDateTime);
                
                    $dtAvailableDay=date("j",strtotime($dtDeliveryDate)); 
                    $dtAvailableMonth=date("m",strtotime($dtDeliveryDate)); 
                    
                    $dtAvailableMonthName=getMonthName($iLanguage,$dtAvailableMonth,true);
                    $dtAvailableDate=$dtAvailableDay.". ".$dtAvailableMonthName;  
                    $fDummyTotalPrice = round((float)($displayServiceLineAry['fTotalDisplayPrice'] + ($displayServiceLineAry['fTotalDisplayPrice'] * $dummyServiceArys['fPriceIncrease'] * 0.01)));
                    $fTotalBookingPrice = number_format_custom((float)$fDummyTotalPrice,$iLanguage); 
                    
                    $displayServiceLineAry['fTotalDisplayPrice'] = $fDummyTotalPrice;
                    $displayServiceLineAry['fTotalBookingPrice'] = $fTotalBookingPrice;
                    $displayServiceLineAry['iDaysBetween'] = $displayServiceLineAry['iDaysBetween'] + $dummyServiceArys['iTransitTimeIncrease']; 
                    $displayServiceLineAry['szAvailableDateStr'] = $dtAvailableDate;
                    $displayServiceLineAry['fAvgRating'] = mt_rand(3.5,4.3);
                    $displayServiceLineAry['iNumRating'] = mt_rand(6,15); 
                
                    display_service_line($displayServiceLineAry,$postSearchAry,$tr_counter,$t_base,$bDisplayBooknowButton,$forwarder_flag,1);
                    $tr_counter++;
                }
            } 
            
            if($iDisplayCouirierDummyService==1 && !empty($dummyServiceAry['couier']) && !empty($cheapesCourierServiceAry))
            {
                /*
                * Display Courier dummy services
                */
                foreach($dummyServiceAry['couier'] as $dummyServiceArys)
                {  
                    $displayServiceLineAry = $cheapesCourierServiceAry;
                    
                    $dtAvailableTime = strtotime(str_replace("/","-",trim($displayServiceLineAry['dtAvailableDate']))); 
                    $dtDeliveryDateTime = strtotime("+".$dummyServiceArys['iTransitTimeIncrease']." day", $dtAvailableTime);
                    $dtDeliveryDate = date('Y-m-d',$dtDeliveryDateTime);
                
                    $dtAvailableDay=date("j",strtotime($dtDeliveryDate)); 
                    $dtAvailableMonth=date("m",strtotime($dtDeliveryDate)); 
                    
                    $dtAvailableMonthName=getMonthName($iLanguage,$dtAvailableMonth,true);
                    $dtAvailableDate=$dtAvailableDay.". ".$dtAvailableMonthName;  
                    $fDummyTotalPrice = round((float)($displayServiceLineAry['fTotalDisplayPrice'] + ($displayServiceLineAry['fTotalDisplayPrice'] * $dummyServiceArys['fPriceIncrease'] * 0.01)));
                    $fTotalBookingPrice = number_format_custom((float)$fDummyTotalPrice,$iLanguage); 
                    
                    $displayServiceLineAry['fTotalDisplayPrice'] = $fDummyTotalPrice;
                    $displayServiceLineAry['fTotalBookingPrice'] = $fTotalBookingPrice;
                    $displayServiceLineAry['iDaysBetween'] = $displayServiceLineAry['iDaysBetween'] + $dummyServiceArys['iTransitTimeIncrease']; 
                    $displayServiceLineAry['szAvailableDateStr'] = $dtAvailableDate;
                    $displayServiceLineAry['fAvgRating'] = mt_rand(3.5,4.3);
                    $displayServiceLineAry['iNumRating'] = mt_rand(6,15); 
                    
                    display_service_line($displayServiceLineAry,$postSearchAry,$tr_counter,$t_base,$bDisplayBooknowButton,$forwarder_flag,1);
                    $tr_counter++;
                }
            }
            
            if($bDisplayBooknowButton && !$bDTDTradesFlag)
            { 
                if($lcl_service_counter==0 && $courier_service_ctr>0 && $modeTransport && $iOnlyCartonServiceDisplayedCtr==$courier_service_ctr)
                {
                    $courierServiceRoad=4;
                    if($rail_service_counter>0)
                    {
                        $courierServiceRoad=9;
                    }
                    echo display_other_service_offer($postSearchAry,$courierServiceRoad,$tr_counter); 
                }
                else
                {
                    
                    $iOfferTypeForLCL=3;
                    if((int)$courier_service_ctr>0 && $rail_service_counter>0)
                    {
                        $iOfferTypeForLCL=12;
                    }
                    if($courier_service_ctr==0)
                    {
                        $kCourierService = new cCourierServices();  
                        if($kCourierService->checkFromCountryToCountryExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry']))
                        {
                            
                            if((int)$lcl_service_counter>0 && $rail_service_counter==0)
                            {
                                $iOfferTypeForCourier=1;
                            }
                            else  if((int)$lcl_service_counter>0 && $rail_service_counter>0)
                            {
                                $iOfferTypeForCourier=11;
                            }
                            else if((int)$lcl_service_counter==0 && $rail_service_counter>0)
                            {
                                $iOfferTypeForCourier=7;
                            }                            
                            //If Searched country combination is exists in truckin icon table then we show trucking
                            echo display_other_service_offer($postSearchAry,$iOfferTypeForCourier,$tr_counter); 
                        } 
                        else
                        {
                            
                            if((int)$lcl_service_counter>0 && $rail_service_counter>0)
                            {
                                $iOfferTypeForCourier=10;
                            }
                            else if((int)$lcl_service_counter>0 && $rail_service_counter==0)
                            {
                                $iOfferTypeForCourier=2;
                            }
                            else if((int)$lcl_service_counter==0 && $rail_service_counter>0)
                            {
                                $iOfferTypeForCourier=8;
                            }
                            echo display_other_service_offer($postSearchAry,$iOfferTypeForCourier,$tr_counter); 
                        } 
                        $iOfferTypeForLCL=6;
                        $tr_counter++;
                    } 
                    if($lcl_service_counter==0 && $showOfferFlag)
                    {
                        echo display_other_service_offer($postSearchAry,$iOfferTypeForLCL,$tr_counter); 
                    }
                }
                
            }
        }
        else
        {
            ?>
            <div class="odd clearfix" >
                <h2><?=t($t_base.'messages/NO_SERVICE_MATCHING');?></h2>
            </div>
        <?php  }
        
            if($kCourierService_new->iDisplaySeeMoreLink==1)
            {
                if(!empty($postSearchAry['szServiceSortField']))
                {
                    $szSortField = $postSearchAry['szServiceSortField'];
                } 
                else
                {
                    $szSortField = 'SEE_MORE_LINK';
                }
                ?>
                <div style="text-align:center" class="see-more-button" id="see-more-button-link">
                    <a href="javascript:void(0)" onclick="display_more_service_result('<?php echo $postSearchAry['iPageNumber']+1; ?>','<?php echo $szSortField; ?>','<?php echo $szBookingRandomNum; ?>','<?php echo $_REQUEST['lang']?>');" ><span><?php echo t($t_base.'fields/see_more_results'); ?></span></a>
                    <!--<span>... <img src="<?php echo __BASE_STORE_IMAGE_URL__."/loader.gif"; ?>" style="padding: 0 0 0 13px;height:20px;width:20px;"></span>-->
                </div>
                <?php
            }  
            if(!empty($szCourierCalculationLogString))
            { 
                echo "<a href='javascript:void(0)' style='color:#eff0f2;' onclick=showHideCourierCal('courier_service_calculation')>.</a> <div id='courier_service_calculation' class='booking-page clearfix' style='display:none'><div id='popup-bg'></div><div id='popup-container'><div class='popup abandon-popup' style='margin-left:20px;width:85%;'><p class='close-icon' align='right'><a onclick=showHideCourierCal('courier_service_calculation') href='javascript:void(0);'><img alt='close' src='".__BASE_STORE_IMAGE_URL__."/close1.png'>
                    </a></p><div style='text-align:left;height:400px;overflow-y:auto;overflow-x:hidden;width:100%;'>".$szCourierCalculationLogString." ".$kCourierService_new->szLogString." </div></div> </div></div>";

            }
     ?>
    </div> 
    <input type="hidden" name="szOldField" id="szOldField" value="fDisplayPrice">
    <input type="hidden" name="szSortType" id="szSortType" value="">
    <input type="hidden" name="iPageNumber" id="iPageNumber" value="<?php echo $postSearchAry['iPageNumber']; ?>">
    <input type="hidden" name="iDisplaySeeMoreLink" id="iDisplaySeeMoreLink" value="<?php echo $kCourierService_new->iDisplaySeeMoreLink;?>">
    <?php 
} 
function display_service_line($searchResults,$postSearchAry,$tr_counter,$t_base,$bDisplayBooknowButton,$forwarder_flag,$iDummyService=false)
{
    $minRatingToShowStars = $searchResults['iMinRatingToShowStars']; 
    $szForwarderImage = $searchResults['szForwarderImage']; 
    $szPackingType = $searchResults['szPackingType']; 
    $szAvailableDateStr = $searchResults['szAvailableDateStr'];
    $szCutoffDateStr = $searchResults['szCutoffDateStr'];
    $iDaysBetween = $searchResults['iDaysBetween'];
    $szCustomerCurrency = $searchResults['szCustomerCurrency'];
    $fTotalBookingPrice = $searchResults['fTotalBookingPrice'];
    $iCourierBooking = $searchResults['iCourierBooking'];
    $iCourierBooking = $searchResults['iCourierBooking'];
    $idPackingType = $searchResults['idPackingType']; 
    $LCDTDQuantityFlag = $searchResults['LCDTDQuantityFlag']; 
    
    $szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
    
    if($iDummyService==1)
    {
        $szUniqueKey = $searchResults['unique_id'] + "_dummy";
    }
    else
    {
        $szUniqueKey = $searchResults['unique_id'];
    }
    if($postSearchAry['iQuickQuote']==__QUICK_QUOTE_SEND_QUOTE__)
    {
        $szClassName = "odd";
    }
    else
    {
        $szClassName = "odd";
        if($tr_counter%2==0)
        {
            $szClassName = "";
        }
    }  
    $szWarehouseCityName = '';
    if($searchResults['iDTDTradesFlag']==1 || $iCourierBooking==1)
    {
        /*
        * If either is trade is added into /dtdTrades/ screen or is a courier booking then we don't city name other we display from/to city below the Ship or Rail icon
        */
    }
    else
    {
        if($postSearchAry['idServiceType']==__SERVICE_TYPE_PTD__)
        {
            $szWarehouseCityName = $searchResults['szFromWHSCity'];
        }
        else if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTP__)
        {
            $szWarehouseCityName = $searchResults['szToWHSCity'];
        }
    } 
?>
    <div class="<?php echo $szClassName; ?> search-result-show-hide-div clearfix" id="select_services_tr_<?php echo $searchResults['unique_id']; ?>">
        <div class="logo">
            <a href="javascript:void(0)" <?php if($iDummyService!=1){ ?> onclick="openForwarderRatingPopUp('<?=$searchResults['idForwarder']?>')" <?php } ?> style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;">
                <img src="<?=__BASE_STORE_INC_URL__.'/image.php?img='.$szForwarderImage.'&temp=2&w=138&h=61'?>" alt="<?=$searchResults['szDisplayName']?>" border="0" />
            </a>
            <?php if(!empty($szWarehouseCityName)){ ?>
                <span class="cfs_name"><?php echo $szWarehouseCityName; ?></span>
            <?php } ?>
        </div>
        <div class="rating">
            <a href="javascript:void(0)" <?php if($iDummyService!=1){ ?> onclick="openForwarderRatingPopUp('<?=$searchResults['idForwarder']?>')" <?php } ?> style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;"> 
                <?php 
                    if($searchResults['iNumRating']<$minRatingToShowStars)
                    { 
                        $szStarRatingStr = displayRatingStar(0); 
                    }  
                    else 
                    {		 
                        // one filled star's width is 10px so that we are multiplying Avg Rating with 10 
                        $fAvgRating = round(($searchResults['fAvgRating']*__RATING_STAR_WIDTH__),2);
                        $szStarRatingStr = displayRatingStar($fAvgRating);
                    }?><span>(<?php echo (int)$searchResults['iNumRating']?>)</span>
            </a>
        </div>
        <div class="cut-off"><?php echo $szPackingType; ?></div>
        <?php if($searchResults['iDTDTradesFlag']==1){?>
            <div class="delivery"><?php echo $szCutoffDateStr;?></div>
            <div class="days"><?php echo $szAvailableDateStr;?></div>
        <?php } else { ?>
            <div class="delivery"><?php echo $szAvailableDateStr;?></div>
            <div class="days"><?php echo $iDaysBetween; ?></div>
        <?php } ?> 
        <div class="price"><span id="fullResolution"><?php echo $szCustomerCurrency ?></span>&nbsp;<span id="select_services_tr_price_container_<?php echo $searchResults['unique_id']; ?>"><?php echo $fTotalBookingPrice; ?></span></div>
        <?php if($bDisplayBooknowButton){ ?>
            <div class="btn">  
                <?php if(!$forwarder_flag){  ?>
                    <a href="javascript:void(0)" onclick="toggleServiceList('<?=$searchResults['unique_id']?>','<?=$searchResults['idForwarder']?>','<?=$_SESSION['user_id']?>','<?=__NEW_BOOKING_DETAILS_PAGE_URL__?>','NEW_BOOKING_PROCESS','<?php echo $iCourierBooking; ?>');" class="button-yellow" style="display:none;" id="hidden-search-service-book-now-button_<?php echo $searchResults['unique_id']; ?>"><span><?php echo t($t_base.'fields/selected'); ?></span></a>
                    <a href="javascript:void(0)" onclick="display_forwarder_non_accepted_popup('<?=$searchResults['unique_id']?>','<?=$searchResults['idForwarder']?>','<?=$_SESSION['user_id']?>','<?=__NEW_BOOKING_DETAILS_PAGE_URL__?>','NEW_BOOKING_PROCESS','<?php echo $iCourierBooking; ?>','<?php echo $idPackingType; ?>','<?php echo $_REQUEST['lang']?>','<?php echo $LCDTDQuantityFlag;?>','<?php echo $iDummyService; ?>');" class="button-green1" id="search-service-book-now-button_<?php echo $searchResults['unique_id']; ?>"><span><?php echo t($t_base.'fields/book_now'); ?></span></a>
                <?php } else { ?>
                    <a href="javascript:void(0)" onclick="display_fowarder_price_details('<?=$searchResults['unique_id']?>','select_services_tr_<?=$tr_counter?>','<?php echo $szBookingRandomNum; ?>')" style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;">
                    <?=$searchResults['szCurrency']?><br /><?=number_format((float)$searchResults['fDisplayPrice'],2)?></a>
                <?php } ?>  
            </div>
        <?php } ?>
    </div>	
    <?php
}
function fetch_selected_services($searchResult,$kCourierService,$postSearchAry)
{
    $iPageCount = $postSearchAry['iPageNumber'];  
    
    $kWHSSearch=new cWHSSearch();
    $iMaxLclOptionsToShow = $kWHSSearch->getManageMentVariableByDescription('__TOTAL_NUM_SEARCH_RESULT__');
    $iMaxFastestCourierOptionsToShow = $kWHSSearch->getManageMentVariableByDescription('__NUMBER_OF_FASTEST_OPTION__');
    $iMaxCheapestCourierOptionsToShow = $kWHSSearch->getManageMentVariableByDescription('__NUMBER_OF_CHEAPEST_OPTION__'); 
    $iMaxRailOptionsToShow = $kWHSSearch->getManageMentVariableByDescription('__TOTAL_NUM_RAIL_SERVICE_DISPLAY__');
    $iMaxRecordPerForwarder =(int)$kWHSSearch->getManageMentVariableByDescription('__MAX_NUM_RECORD_PER_FORWARDER__');
    $iMinVolumeToDisplayRailTransport = $kWHSSearch->getManageMentVariableByDescription('__MINIMUM_VOLUME_TO_DISPLAY_RAIL_TRANSPORT__');
    $iMaxAirOptionsToShow = $kWHSSearch->getManageMentVariableByDescription('__TOTAL_NUM_AIR_FREIGHT_SEARCH_RESULT__');
    
                
    $iMaxLclOptionsToShow = $iMaxLclOptionsToShow * $iPageCount;
    $iMaxRailOptionsToShow = $iMaxRailOptionsToShow * $iPageCount;
    $iMaxAirOptionsToShow = $iMaxAirOptionsToShow * $iPageCount; 
    $iMaxFastestCourierOptionsToShow = $iMaxFastestCourierOptionsToShow * $iPageCount ;
    $iMaxCheapestCourierOptionsToShow = $iMaxCheapestCourierOptionsToShow * $iPageCount ;
    
    $iTotalNumberofOptionsToShow = $iMaxRailOptionsToShow + $iMaxLclOptionsToShow + $iMaxAirOptionsToShow + $iMaxFastestCourierOptionsToShow + $iMaxCheapestCourierOptionsToShow ; 
    $iTotalCourierOptionsToShow = $iMaxFastestCourierOptionsToShow + $iMaxCheapestCourierOptionsToShow;
    
    $lclServiceAry = array();
    $airServiceAry = array();
    $railServiceAry = array();
    $courierServiceAry = array(); 
                  
     //Checking if trades are added to courier excluded trade service list or not
    //If trade is added to excluded list then no need to check furthure

    $excludedTradeSearch = array(); 
    $excludedTradeSearch['idOriginCountry'] = $postSearchAry['idOriginCountry'];
    $excludedTradeSearch['idDestinationCountry'] = $postSearchAry['idDestinationCountry'];
                
    if($_SESSION['user_id']>0)
    {
        $kUser = new cUser();
        $kForwarder = new cForwarder();
        $kUser->getUserDetails($_SESSION['user_id']);

        if($kUser->iPrivate==1)
        {
            $excludedTradeSearch['iCustomerType'] = __COURIER_EXCLUDED_TRADE_CUSTOMER_TYPE_PRIVATE__;
        }
        else
        {
            $excludedTradeSearch['iCustomerType'] = __COURIER_EXCLUDED_TRADE_CUSTOMER_TYPE_BUSSINESS__;
        } 
    }
    
    $kCourierServices = new cCourierServices();
    
    $excludedTradesAry = array();
    $excludedTradesAry = $kCourierServices->isTradeAddedToExcludedList($excludedTradeSearch,false,true); 
                
    $szLogString = '';
    if(!empty($searchResult))
    {
        $ctr = 0; 
        foreach($searchResult as $searchResults)
        {
            if($searchResults['idCourierProvider']>0)
            {
                $courierKey = $searchResults['idForwarder']."_".$searchResults['idCourierProvider'];  
                if(!empty($excludedTradesAry) && in_array($courierKey,$excludedTradesAry))
                { 
                    $szLogString .= "<br> ".$searchResults['szForwarderCompanyName']." - ".$searchResults['szName']." - ".$searchResults['szCourierProductName']." - Customer Price: ".$searchResults['szCurrency']." ".number_format((float)$searchResults['fDisplayPrice'],4)." - ".$searchResults['iDaysBetween']." days - NOT OK ";
                    $szLogString .= " <br> Description: This trade is added in excluded trade service list. <br>";
                }
                else
                {
                    $courierServiceAry[$ctr] = $searchResults ;
                } 
            }
            else if($searchResults['iWarehouseType']==__WAREHOUSE_TYPE_AIR__)
            {
                $airServiceAry[$ctr] = $searchResults ;
            }
            else if($searchResults['iRailTransport']==1)
            {
                //echo "Min: ".$iMinVolumeToDisplayRailTransport." ".
                if($postSearchAry['fCargoVolume']>=$iMinVolumeToDisplayRailTransport)
                {
                    /*
                    * We only display Rail service if fCargoVolume of booking is greater then the linit defined @Management setting page
                    */
                    $railServiceAry[$ctr] = $searchResults ;
                } 
                else
                {
                    $szLogString .= "<br><strong> Rail: - Customer Price: ".$searchResults['szCurrency']." ".number_format((float)$searchResults['fDisplayPrice'],4)." - ".$searchResults['iDaysBetween']." days - NOT OK </strong>";
                    $szLogString .= "<br><i> Description: Minimum volume for rail services to be displayed is ".$iMinVolumeToDisplayRailTransport."cbm </i><br>";
                }
            }
            else
            {
                $lclServiceAry[$ctr] = $searchResults ;
            }
            $ctr++;
        }
    }   
    $filename = __APP_PATH_ROOT__."/logs/airServiceAryLast.log";
    
    if(!empty($airServiceAry))
    {
        /*
        * 28. Airfreight could be a service 5 days per week (in which case forwarder will add 5 lines with weekly service, one for Monday, one for Tuesday, etc.). If that is the case, then there is a risk that we will offer too many airfreight services in the API response, where for airfreight, the only service which is relevant for a given warehouse to warehouse combination is only the first one available at destination. So when calculating which services to offer, we only give 1 service per warehouse-warehouse combination, and if there is more than one identified, then it should only take the one with the earliest available date, per warehouse-warehouse combination. 
        */ 
        $tempAirServiceAry = array();
        $processedAirServiceAry = array();
        $tempAirServiceAry = $airServiceAry;
        //Sorting Air services ASC to delivery time
        $tempAirServiceAry = sortArray($tempAirServiceAry,'iAvailabeleDate_time',false,'fDisplayPrice',false,false,false,true);  
        
        $warehouseCombinationAry = array();
        $airCtr = 0;
        if(!empty($tempAirServiceAry))
        {
            foreach($tempAirServiceAry as $tempAirServiceArys)
            {
                $idWarehouseFrom = $tempAirServiceArys['idFromWHS'];
                $idWarehouseTo = $tempAirServiceArys['idToWHS'];
                $szCustomerType = $tempAirServiceArys['szCustomerType'];
                
                $szWhsKey = $idWarehouseFrom."_".$idWarehouseTo."_".$szCustomerType;
                $strdata=array();
                $strdata[0]=" \n\n ************ \n";
                $strdata[1]=$szWhsKey."\n\n";
                file_log_session(true, $strdata, $filename);
                if(!empty($warehouseCombinationAry) && !in_array($szWhsKey,$warehouseCombinationAry))
                {
                    $warehouseCombinationAry[] = $szWhsKey;
                    $processedAirServiceAry[$airCtr] = $tempAirServiceArys;
                    $airCtr++;
                }
                else if(empty($warehouseCombinationAry))
                {
                    $warehouseCombinationAry[] = $szWhsKey;
                    $processedAirServiceAry[$airCtr] = $tempAirServiceArys;
                    $airCtr++;
                }
            }
        }
    }
    
    $strdata=array();
    $strdata[0]=" \n\n ************ \n";
    $strdata[1]=print_r($processedAirServiceAry,true);
    file_log_session(true, $strdata, $filename);
    $airServiceAry = $processedAirServiceAry; 
    $fastestCourierAry = array();
    $cheapestCourierAry = array(); 
    $alreadyAddedFlag = false;
                
    $bDTDServiceFlag = false; 
    if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__)
    { 
        $courierServiceAry = sortArray($courierServiceAry,'fDisplayPrice',false,'iDaysBetween',false,false,false,true);  
        $lclServiceAry = sortArray($lclServiceAry,'fDisplayPrice',false,'iDaysBetween',false,false,false,true);  
        $railServiceAry = sortArray($railServiceAry,'fDisplayPrice',false,'iDaysBetween',false,false,false,true);  
        $airServiceAry = sortArray($airServiceAry,'fDisplayPrice',false,'iDaysBetween',false,false,false,true);  
    } 
    else
    { 
        $courierServiceAry = sortArray($courierServiceAry,'fTotalBookingPriceIncludingTrucking',false,'iDaysBetween',false,false,false,true);  
        $lclServiceAry = sortArray($lclServiceAry,'fTotalBookingPriceIncludingTrucking',false,'iDaysBetween',false,false,false,true);  
        $railServiceAry = sortArray($railServiceAry,'fTotalBookingPriceIncludingTrucking',false,'iDaysBetween',false,false,false,true);  
        $airServiceAry = sortArray($airServiceAry,'fTotalBookingPriceIncludingTrucking',false,'iDaysBetween',false,false,false,true);  
    } 
    $iLclRowsCount = count($lclServiceAry);
    $iCourierCount = count($courierServiceAry);
    $iRailServiceCount = count($railServiceAry);
    $iAirServiceCount = count($airServiceAry);
                
    $iDisplaySeeMoreLink = false;
    if($iLclRowsCount >$iMaxLclOptionsToShow) 
    {
        $iDisplaySeeMoreLink = true ;
    }
    else if($iRailServiceCount>$iMaxRailOptionsToShow)
    {
        $iDisplaySeeMoreLink = true ;
    }
    else if($iCourierCount>$iTotalCourierOptionsToShow) 
    {
        $iDisplaySeeMoreLink = true ;
    } 
    else if($iAirServiceCount>$iMaxAirOptionsToShow)
    {
        $iDisplaySeeMoreLink = true ;
    }
    if($iDisplaySeeMoreLink)
    {
        $kCourierService->iDisplaySeeMoreLink = 1;
    }
    $globalCounter = 0;
    
    if(!empty($courierServiceAry) && $iMaxCheapestCourierOptionsToShow>0)
    {
        $ctr1 = 0;                                       
        foreach($courierServiceAry as $courierServiceArys)
        { 
            $szUniqueKey = $courierServiceArys['unique_id'];  
            $cheapestCourierAry[$ctr1] = $courierServiceArys;
            $alreadyAddedFlag[] = $courierServiceArys['unique_id'];
            $szLogString .= "<br> ".($globalCounter+1).". ".$courierServiceArys['szForwarderCompanyName']." - ".$courierServiceArys['szName']." - ".$courierServiceArys['szCourierProductName']." - Customer Price: ".$courierServiceArys['szCurrency']." ".number_format((float)$courierServiceArys['fDisplayPrice'],4)." - ".$courierServiceArys['iDaysBetween']." days - cheapest ".($ctr1+1) ;
            $ctr1++;  
            $globalCounter++; 
            if($ctr1==$iMaxCheapestCourierOptionsToShow)
            {
                break;
            }
        }
    }  
    $courierServiceAry = sortArray($courierServiceAry,'iDaysBetween',false,'fDisplayPrice',false,false,false,true);   
    
    if(!empty($courierServiceAry) && $iMaxFastestCourierOptionsToShow>0)
    {
        $ctr1 = 0;                                       
        foreach($courierServiceAry as $courierServiceArys)
        {
            $szUniqueKey = $courierServiceArys['unique_id']; 
            if(!empty($alreadyAddedFlag) && !in_array($szUniqueKey,$alreadyAddedFlag))
            {
                $fastestCourierAry[$ctr1] = $courierServiceArys;
                $alreadyAddedFlag[] = $courierServiceArys['unique_id'];
                
                $szLogString .= "<br> ".($globalCounter+1).". ".$courierServiceArys['szForwarderCompanyName']." - ".$courierServiceArys['szName']." - ".$courierServiceArys['szCourierProductName']." - Customer Price: ".$courierServiceArys['szCurrency']." ".number_format((float)$courierServiceArys['fDisplayPrice'],4)." - ".$courierServiceArys['iDaysBetween']." days - fastest ".($ctr1+1) ;
                $ctr1++;
                $globalCounter++;
            } 
            else if(empty($alreadyAddedFlag))
            {
                $fastestCourierAry[$ctr1] = $courierServiceArys;
                $alreadyAddedFlag[] = $courierServiceArys['unique_id']; 
                
                $szLogString .= "<br> ".($globalCounter+1).$courierServiceArys['szForwarderCompanyName']." - ".$courierServiceArys['szName']." - ".$courierServiceArys['szCourierProductName']." - Customer Price: ".$courierServiceArys['szCurrency']." ".number_format((float)$courierServiceArys['fDisplayPrice'],4)." - ".$courierServiceArys['iDaysBetween']." days - fastest ".($ctr1+1) ;
                $ctr1++;
                $globalCounter++;
            } 
            if($ctr1==$iMaxFastestCourierOptionsToShow)
            {
                break;
            }
        }
    } 
                                                
    $lclServicesOptions = array();
    if(!empty($lclServiceAry))
    {
        $ctr1=0; 
        foreach($lclServiceAry as $lclServiceArys)
        {
            $dtCutOffDate_str = date("j F",strtotime(str_replace("/","-",trim($lclServiceArys['dtCutOffDate'])))); 
            $dtAvailableDate_str = date("j F",strtotime(str_replace("/","-",trim($lclServiceArys['dtAvailableDate']))));

            $iCuttOffDateTime = strtotime($dtCutOffDate_str) ;
            $iAvailableDateTime = strtotime($dtAvailableDate_str) ;

            $start = date("Y-m-d",strtotime(str_replace("/","-",trim($lclServiceArys['dtCutOffDate']))));
            $end = date("Y-m-d",strtotime(str_replace("/","-",trim($lclServiceArys['dtAvailableDate']))));
            $days_between = ceil(abs(strtotime($end) - strtotime($start)) / 86400); 
            $szFilterString = $lclServiceArys['idForwarder']."-".$lclServiceArys['szCustomerType']."-".$iCuttOffDateTime."-".$iAvailableDateTime."-".$days_between."-".round((float)$lclServiceArys['fDisplayPrice'],2)."-".$lclServiceArys['idWarehouseFrom']."-".$lclServiceArys['idWarehouseTo']; 
            
            if(empty($filterServiceAry) || !in_array($szFilterString,$filterServiceAry))
            {
                $filterServiceAry[] = $szFilterString ;  
                $lclServicesOptions[$ctr1] = $lclServiceArys ;
                $ctr1++;
                if($ctr1>=$iMaxLclOptionsToShow)
                {
                    break;
                }
            }
        }
    } 
    
    $railServicesOptions = array();
    if(!empty($railServiceAry))
    {
        $ctr1=0; 
        foreach($railServiceAry as $railServiceArys)
        {
            $dtCutOffDate_str = date("j F",strtotime(str_replace("/","-",trim($railServiceArys['dtCutOffDate'])))); 
            $dtAvailableDate_str = date("j F",strtotime(str_replace("/","-",trim($railServiceArys['dtAvailableDate']))));

            $iCuttOffDateTime = strtotime($dtCutOffDate_str) ;
            $iAvailableDateTime = strtotime($dtAvailableDate_str) ;

            $start = date("Y-m-d",strtotime(str_replace("/","-",trim($railServiceArys['dtCutOffDate']))));
            $end = date("Y-m-d",strtotime(str_replace("/","-",trim($railServiceArys['dtAvailableDate']))));
            $days_between = ceil(abs(strtotime($end) - strtotime($start)) / 86400); 
            $szFilterString = $railServiceArys['idForwarder']."-".$railServiceArys['szCustomerType']."-".$iCuttOffDateTime."-".$iAvailableDateTime."-".$days_between."-".round((float)$railServiceArys['fDisplayPrice'],2)."-".$railServiceArys['idWarehouseFrom']."-".$railServiceArys['idWarehouseTo'];  
            
            if(empty($filterServiceAry) || !in_array($szFilterString,$filterServiceAry))
            {
                $filterServiceAry[] = $szFilterString ;   
                $railServicesOptions[$ctr1] = $railServiceArys;
                $ctr1++;
                if($ctr1>=$iMaxRailOptionsToShow)
                {
                    break;
                }
            }
        }
    }
    
    $airServicesOptions = array();
    $filterServiceAry = array();
    if(!empty($airServiceAry))
    {
        $ctr1=0; 
        foreach($airServiceAry as $airServiceArys)
        {
            $dtCutOffDate_str = date("j F",strtotime(str_replace("/","-",trim($airServiceArys['dtCutOffDate'])))); 
            $dtAvailableDate_str = date("j F",strtotime(str_replace("/","-",trim($airServiceArys['dtAvailableDate']))));

            $iCuttOffDateTime = strtotime($dtCutOffDate_str) ;
            $iAvailableDateTime = strtotime($dtAvailableDate_str) ;

            $start = date("Y-m-d",strtotime(str_replace("/","-",trim($airServiceArys['dtCutOffDate']))));
            $end = date("Y-m-d",strtotime(str_replace("/","-",trim($airServiceArys['dtAvailableDate']))));
            $days_between = ceil(abs(strtotime($end) - strtotime($start)) / 86400); 
            $szFilterString = $airServiceArys['idForwarder']."-".$airServiceArys['szCustomerType']."-".$iCuttOffDateTime."-".$iAvailableDateTime."-".$days_between."-".round((float)$airServiceArys['fDisplayPrice'],2)."-".$airServiceArys['idWarehouseFrom']."-".$airServiceArys['idWarehouseTo'];  
            
            if(!empty($filterServiceAry) && !in_array($szFilterString,$filterServiceAry))
            {
                $filterServiceAry[] = $szFilterString ;   
                $airServicesOptions[$ctr1] = $airServiceArys;
                $ctr1++;
                if($ctr1>=$iMaxAirOptionsToShow)
                {
                    break;
                }
            }
            else if(empty($filterServiceAry))
            {
                $filterServiceAry[] = $szFilterString ;   
                $airServicesOptions[$ctr1] = $airServiceArys;
                $ctr1++;
                if($ctr1>=$iMaxAirOptionsToShow)
                {
                    break;
                }
            }
        }
    } 
    $kCourierService->szLogString = $szLogString;
    
    $szSortField = $postSearchAry['szServiceSortField'];
    $szSortType = $postSearchAry['szServiceSortType'];
    
    if($szSortField=='SEE_MORE_LINK')
    {
        $szSortField = '';
        $szSortType = '';
    }
    if(!empty($szSortField) && !empty($szSortType))
    {  
        $searchResultAry = array();
        $searchResultAry = $lclServicesOptions;
        
        if(!empty($railServicesOptions) && !empty($searchResultAry))
        {
            $searchResultAry = array_merge($searchResultAry,$railServicesOptions); 
        }
        else if(!empty($railServicesOptions))
        {
            $searchResultAry = $railServicesOptions;
        } 
        
        if(!empty($airServicesOptions) && !empty($searchResultAry))
        {
            $searchResultAry = array_merge($searchResultAry,$airServicesOptions); 
        }
        else if(!empty($airServicesOptions))
        {
            $searchResultAry = $airServicesOptions;
        } 
        
        if(!empty($fastestCourierAry) && !empty($searchResultAry))
        {
            $searchResultAry = array_merge($searchResultAry,$fastestCourierAry); 
        }
        else if(!empty($fastestCourierAry))
        {
            $searchResultAry = $fastestCourierAry;
        }
        
        if(!empty($cheapestCourierAry) && !empty($searchResultAry))
        {
            $searchResultAry = array_merge($searchResultAry,$cheapestCourierAry); 
        }
        else if(!empty($cheapestCourierAry))
        {
            $searchResultAry = $cheapestCourierAry;
        } 
        if($szSortType=='ASC')
        {
            $searchResultAry = sortArray($searchResultAry,$szSortField);
        }
        else
        {
            $searchResultAry = sortArray($searchResultAry,$szSortField,$szSortType);
        } 
                
        $finalReturnAry = array();
        if(!empty($searchResultAry))
        {
            foreach($searchResultAry as $searchResultArys)
            {
                if($searchResultArys['idCourierProvider']>0)
                {
                    $finalReturnAry[$ctr] = $searchResultArys;
                    $ctr++;
                }
                else
                {
                    $counterAry[$searchResultArys['idForwarder']]  += 1 ; 
                    if($counterAry[$searchResultArys['idForwarder']]>$iMaxRecordPerForwarder)
                    {
                        continue;
                    }
                    else
                    { 
                        $finalReturnAry[$ctr] = $searchResultArys;
                        $ctr++;
                    }
                }
            }
        }
        return $finalReturnAry;
    }
    else
    { 
        /*
        * The way we will sort these options on the screen as follows:
            i.	First put the fastest courier service
            ii.	Then put the cheapest courier service
            iii.Then put the cheapest LCL service
            iii.Then put the cheapest Rail service
            iv.	Then put second fastest courier service
            v.	Then put the second cheapest courier service
            vi.	Then put the second cheapest LCL service
            vii. Then put third fastest courier service
            viii. Etc, etc. 
        */
                
        $finalResultAry = array();
        $iCheapest_ctr = 0;
        $ifastest_ctr = 0;
        $iLcl_ctr = 0;
        $iRail_ctr = 0;
        $iAir_ctr = 0;
        $ctr = 0; 
        $counterAry = array();
        for($i=0;$i<$iTotalNumberofOptionsToShow;$i++)
        {
            if(!empty($fastestCourierAry[$ifastest_ctr]))
            {
                $finalResultAry[] = $fastestCourierAry[$ifastest_ctr] ;
                $ifastest_ctr++;
            }
            if(!empty($cheapestCourierAry[$iCheapest_ctr]))
            {
                $finalResultAry[] = $cheapestCourierAry[$iCheapest_ctr] ;
                $iCheapest_ctr++;
            }
            if(!empty($lclServicesOptions[$iLcl_ctr]))
            {
                $counterAry[$lclServicesOptions[$iLcl_ctr]['idForwarder']]  += 1 ;
                if($counterAry[$lclServicesOptions[$iLcl_ctr]['idForwarder']]>$iMaxRecordPerForwarder)
                {
                    continue;
                } 
                $finalResultAry[] = $lclServicesOptions[$iLcl_ctr] ;
                $iLcl_ctr++;
            }
            if(!empty($railServicesOptions[$iRail_ctr]))
            {
                $counterAry[$railServicesOptions[$iRail_ctr]['idForwarder']]  += 1 ;
                if($counterAry[$railServicesOptions[$iRail_ctr]['idForwarder']]>$iMaxRecordPerForwarder)
                {
                    continue;
                } 
                $finalResultAry[] = $railServicesOptions[$iRail_ctr] ;
                $iRail_ctr++;
            }
            if(!empty($airServicesOptions[$iAir_ctr]))
            {
                $counterAry[$airServicesOptions[$iAir_ctr]['idForwarder']]  += 1 ;
                if($counterAry[$airServicesOptions[$iAir_ctr]['idForwarder']]>$iMaxRecordPerForwarder)
                {
                    continue;
                } 
                $finalResultAry[] = $airServicesOptions[$iAir_ctr] ;
                $iAir_ctr++;
            }
            
        } 
        return $finalResultAry;
    }
}
/*
function display_new_searvice_listing($postSearchAry,$searchResult,$t_base,$countryKeyValueAry,$forwarder_flag=false,$selected_flag=false,$management_flag=false)
{
	$kWHSSearch=new cWHSSearch();
	$distance_1_header = t($t_base.'messages/DISTANCE_TO_WAREHOUSE_1_TOOL_TIP_HEADER_TEXT_1')." ".$postSearchAry['szOriginCity'].".".t($t_base.'messages/DISTANCE_TO_WAREHOUSE_1_TOOL_TIP_HEADER_TEXT_2');
	$distance_2_header = t($t_base.'messages/DISTANCE_TO_WAREHOUSE_2_TOOL_TIP_HEADER_TEXT_1')." ".$postSearchAry['szDestinationCity'].".".t($t_base.'messages/DISTANCE_TO_WAREHOUSE_2_TOOL_TIP_HEADER_TEXT_2');
	$detail_style="style='text-decoration:none;color:white;'" ;
	
	$danishMonthArr=array('Januar','Februar','Marts','April','Maj','Juni','Juli','August','September','Oktober','November','December');
	
	$kConfig=new cConfig();
	$currencyAry = array();
	$currencyAry = $kConfig->getBookingCurrency(false,true);
	 
	$szBookingRandomNum = $postSearchAry['szBookingRandomNum'] ; 
	?>
	<script type="text/javascript">
		Custom.init();
	</script>
	<div class="results">
		<div class="head clearfix">
			<div class="rating"><?php echo t($t_base.'fields/rating'); ?></div>
			<div class="cut-off"><?php if($postSearchAry['idServiceType']==__SERVICE_TYPE_PTD__){?><?=t($t_base.'fields/cut_off');?> <?php }else{?> <?=t($t_base.'fields/pick_up_value');?> <?php }?></div>
			<div class="delivery"><?php echo t($t_base.'fields/delivery');?></div>
			<div class="days"><?php echo ucwords(t($t_base.'fields/days'));?></div>
			<div class="price">
				<?php echo t($t_base.'fields/all_in_price'); if(!$management_flag){?>
				<select name="idBookingCurrency" id="idBookingCurrency" onfocus="on_focus_currency_dropdown(this.id)" class="styled" onchange="update_booking_currency(this.value,'<?php echo $szBookingRandomNum; ?>')">
					<?php
					 	if(!empty($currencyAry))
					 	{
					 		foreach($currencyAry as $currencyArys)
					 		{
					 			?>
					 				<option value="<?php echo $currencyArys['id']?>" <?php echo (($currencyArys['id']==$searchResult[0]['idCurrency'])?'selected':''); ?>><?=$currencyArys['szCurrency']?></option>
					 			<?php
					 		}
					 	}
					 ?>
				</select>
				<?php } ?>
			</div>
		</div>
		<?php
			if(!empty($searchResult))
			{
				$google_map_anchor_style="style='text-decoration:none;color:black;font-style:normal;font-size:12px;'";
				$minRatingToShowStars = $kWHSSearch->getManageMentVariableByDescription('__MINIMUM_RATING_TO_SHOW_RATING_STARS__');
				$tr_counter = 1;
				$filterServiceAry = array();
			 
				foreach($searchResult as $searchResults)
				{  
					if($forwarder_flag)
					{
						$postSearchAry = array();
						$postSearchAry = $_SESSION['try_it_out_searched_data'] ;
						
						$originPostCodeAry=array();
						$kConfig = new cConfig();
						$originPostCodeAry = $kConfig->getPostCodeDetails($postSearchAry['szOriginCountry'],$postSearchAry['szOriginPostCode'],$postSearchAry['szOriginCity']);
						
						$szOriginAddress =  $postSearchAry['szOriginPostCode']." - ".$postSearchAry['szOriginCity']."<br /> ".$originPostCodeAry['szCountry'];
					    $szDestinationAddress = $searchResults['szFromWHSPostCode']." - ".$searchResults['szFromWHSCity']."<br />".$searchResults['szFromWHSCountry'];
					    $origin_lat_lang_pipeline = $originPostCodeAry['szLatitude']."|".$originPostCodeAry['szLongitude']."|".$searchResults['szFromWHSLat']."|".$searchResults['szFromWHSLong']."|".$searchResults['iDistanceToWHS']."|".$szOriginAddress."|".$szDestinationAddress;
					
						$destinationPostCodeAry=array();
						$destinationPostCodeAry = $kConfig->getPostCodeDetails($postSearchAry['szDestinationCountry'],$postSearchAry['szDestinationPostCode'],$postSearchAry['szDestinationCity']);
						
						$szOriginAddress2 =  $postSearchAry['szDestinationPostCode']." - ".$postSearchAry['szDestinationCity']."<br /> ".$destinationPostCodeAry['szCountry'];
					    $szDestinationAddress2 = $searchResults['szToWHSPostCode']." - ".$searchResults['szToWHSCity']."<br />".$searchResults['szToWHSCountry'];
					
					    $destination_lat_lang_pipeline = $destinationPostCodeAry['szLatitude']."|".$destinationPostCodeAry['szLongitude']."|".$searchResults['szToWHSLat']."|".$searchResults['szToWHSLong']."|".$searchResults['iDistanceToWHS']."|".$szOriginAddress2."|".$szDestinationAddress2;
					}
					else
					{
						$szOriginAddress =  $postSearchAry['szOriginPostCode']." - ".$postSearchAry['szOriginCity']."<br /> ".$postSearchAry['szOriginCountry'];
		                $szDestinationAddress = $searchResults['szFromWHSPostCode']." - ".$searchResults['szFromWHSCity']."<br />".$searchResults['szFromWHSCountry'];
		    
						$origin_lat_lang_pipeline = $postSearchAry['fOriginLatitude']."|".$postSearchAry['fOriginLongitude']."|".$searchResults['szFromWHSLat']."|".$searchResults['szFromWHSLong']."|".$searchResults['iDistanceFromWHS']."|".$szOriginAddress."|".$szDestinationAddress;
						
						$szOriginAddress2 =  $postSearchAry['szDestinationPostCode']." - ".$postSearchAry['szDestinationCity']."<br /> ".$postSearchAry['szDestinationCountry'];
		                $szDestinationAddress2 = $searchResults['szToWHSPostCode']." - ".$searchResults['szToWHSCity']."<br />".$searchResults['szToWHSCountry'];
						$destination_lat_lang_pipeline = $postSearchAry['fDestinationLatitude']."|".$postSearchAry['fDestinationLongitude']."|".$searchResults['szToWHSLat']."|".$searchResults['szToWHSLong']."|".$searchResults['iDistanceToWHS']."|".$szOriginAddress2."|".$szDestinationAddress2;
					}
					
					$szWarehousecityFrom = $searchResults['szFromWHSCity'];
					$szWarehousecityTo = $searchResults['szToWHSCity'];
					
					$fTotalPriceDisplay = $searchResults['fDisplayPrice'];
					
					$start=date("Y-m-d",strtotime(str_replace("/","-",trim($searchResults['dtCutOffDate']))));
					$end=date("Y-m-d",strtotime(str_replace("/","-",trim($searchResults['dtAvailableDate']))));
					$days_between = ceil(abs(strtotime($end) - strtotime($start)) / 86400);
					
					$szClassName = "odd";
					if($tr_counter%2==0)
					{
						$szClassName = "";
					}
					$iLanguage = getLanguageId();
					if($iLanguage==2)
					{
						$dtCutOffMonth=date("m",strtotime(str_replace("/","-",trim($searchResults['dtCutOffDate']))));
						
						$dtCutOffDay=date("j",strtotime(str_replace("/","-",trim($searchResults['dtCutOffDate']))));
						
						$dtCutOffDate=$dtCutOffDay." ".$danishMonthArr[$dtCutOffMonth-1]; 
						
						
						$dtAvailableDay=date("j",strtotime(str_replace("/","-",trim($searchResults['dtAvailableDate']))));
						
						$dtAvailableMonth=date("m",strtotime(str_replace("/","-",trim($searchResults['dtAvailableDate']))));
						
						$dtAvailableDate=$dtAvailableDay." ".$danishMonthArr[$dtAvailableMonth-1];  

						$dtCutOffDate_str = date("j F",strtotime(str_replace("/","-",trim($searchResults['dtCutOffDate'])))); 
                                          $dtAvailableDate_str = date("j F",strtotime(str_replace("/","-",trim($searchResults['dtAvailableDate']))));
					}
					else
					{
						$dtCutOffDate=date("j F",strtotime(str_replace("/","-",trim($searchResults['dtCutOffDate']))));						
						$dtAvailableDate=date("j F",strtotime(str_replace("/","-",trim($searchResults['dtAvailableDate']))));

						$dtCutOffDate_str = $dtCutOffDate ;
                                          $dtAvailableDate_str = $dtAvailableDate ;
					}
					 
					$iCuttOffDateTime = strtotime($dtCutOffDate_str) ;
					$iAvailableDateTime = strtotime($dtAvailableDate_str) ;
					
					$szFilterString = $searchResults['idForwarder']."-".$iCuttOffDateTime."-".$iAvailableDateTime."-".$days_between."-".round((float)$searchResults['fDisplayPrice'],2);

					if(empty($filterServiceAry) || !in_array($szFilterString,$filterServiceAry))
					{
						$filterServiceAry[] = $szFilterString ;
						
						if($iLanguage==__LANGUAGE_ID_DANISH__)
						{ 
							$fTotalBookingPrice = number_format((float)$searchResults['fDisplayPrice'],0,'.','.');
						}
						else
						{ 
							$fTotalBookingPrice = number_format((float)$searchResults['fDisplayPrice'],0,'.',',');
						}
					?>		
					<div class="<?php echo $szClassName; ?> search-result-show-hide-div clearfix" id="select_services_tr_<?php echo $searchResults['unique_id']; ?>">
						<div class="logo">
							<a href="javascript:void(0)" onclick="openForwarderRatingPopUp('<?=$searchResults['idForwarder']?>')" style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;">
								<?php if(!empty($searchResults['szLogo'])){ ?>
									<img src="<?=__BASE_STORE_INC_URL__.'/image.php?img='.$searchResults['szLogo']."&w=138&h=61"?>" alt="<?=$searchResults['szDisplayName']?>" border="0" />
								<?php }else { echo (strlen($searchResults['szDisplayName'])<=22)?$searchResults['szDisplayName']:mb_substr($searchResults['szDisplayName'],0,20,'UTF8')."..";	 } ?>
							</a>
						</div>
						<div class="rating">
							<a href="javascript:void(0)" onclick="openForwarderRatingPopUp('<?=$searchResults['idForwarder']?>')" style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;"><? 
								if($searchResults['iNumRating']<$minRatingToShowStars){ 
									echo displayRatingStar(0); 
								}else {									
									// one filled star's width is 10px so that we are multiplying Avg Rating with 10 
									$fAvgRating = round(($searchResults['fAvgRating']*__RATING_STAR_WIDTH__),2);
									echo displayRatingStar($fAvgRating);
								}
							?>
							<span>(<?php echo (int)$searchResults['iNumRating']?>)</span></a>
						</div>
						<div class="cut-off"><?php echo $dtCutOffDate;?></div>
						<div class="delivery"><?php echo $dtAvailableDate;?></div>
						<div class="days"><?php echo $days_between?></div>
						<div class="price"><span id="fullResolution"><?php echo $searchResult[0]['szCurrency']?></span> <?php echo $fTotalBookingPrice; ?></div>
						<div class="btn"> 
							<?php if(!$forwarder_flag){  ?>
								<a href="javascript:void(0)" onclick="toggleServiceList('<?=$searchResults['unique_id']?>','<?=$searchResults['idForwarder']?>','<?=$_SESSION['user_id']?>','<?=__NEW_BOOKING_DETAILS_PAGE_URL__?>','NEW_BOOKING_PROCESS');" class="button-yellow" style="display:none;" id="hidden-search-service-book-now-button_<?php echo $searchResults['unique_id']; ?>"><span><?php echo t($t_base.'fields/selected'); ?></span></a>
								<a href="javascript:void(0)" onclick="display_forwarder_non_accepted_popup('<?=$searchResults['unique_id']?>','<?=$searchResults['idForwarder']?>','<?=$_SESSION['user_id']?>','<?=__NEW_BOOKING_DETAILS_PAGE_URL__?>','NEW_BOOKING_PROCESS');" class="button-green1" id="search-service-book-now-button_<?php echo $searchResults['unique_id']; ?>"><span><?php echo t($t_base.'fields/book_now'); ?></span></a>
							<?php }else{ ?>
								<a href="javascript:void(0)" onclick="display_fowarder_price_details('<?=$searchResults['unique_id']?>','select_services_tr_<?=$tr_counter?>','<?php echo $szBookingRandomNum; ?>')" style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;">
								<?=$searchResults['szCurrency']?><br /><?=number_format((float)$searchResults['fDisplayPrice'],2)?></a>
							<?php }?> 
						</div>
					</div>	
					<?php
					$tr_counter++;
					} 
				} 
			}
			else
			{
                            ?>
                            <div class="odd clearfix" >
                                    <h2><?=t($t_base.'messages/NO_SERVICE_MATCHING');?></h2>
                            </div>
                            <?php
			}
		?>
	</div>
	<?php
}
 * */ 

function display_other_service_offer($postSearchAry,$iOfferType,$tr_counter=0)
{
    $t_base = "SelectService/";
    if($iOfferType==1 || $iOfferType==7 || $iOfferType==11) //Trucking Icon
    {
        $szImageName = 'Truck.png';
        $szImageAlt = t($t_base.'messages/road_transport');
    }
    else if($iOfferType==2  || $iOfferType==8 || $iOfferType==10) //Plane Icon
    {
        $szImageName = 'Plane.png';
        $szImageAlt = t($t_base.'messages/airfreight');
    }
    else if($iOfferType==4 || $iOfferType==9) //Only cartons displayed
    {
        $szImageName = 'Truck.png';
        $szImageAlt = t($t_base.'messages/only_catorn_displayed');
    }
    else if($iOfferType==6 || $iOfferType==12) //Only Rail displayed
    {
        $szImageName = 'Ship.png';
        $szImageAlt = t($t_base.'messages/ocean_freight');
    }
    else
    {
        $iOfferType = 3; 
        $szImageName = 'Ship.png';
        $szImageAlt = t($t_base.'messages/ocean_freight');
    }
    if($tr_counter%2==0)
    {
        $class="";
    }   
    else
    {
        $class="odd";
    }    
    ?>
    <div class="<?=$class;?> search-result-show-hide-div clearfix" >
        <div class="logo"> 
            <img src="<?php echo __BASE_STORE_IMAGE_URL__."/".$szImageName ?>" alt="<?php echo $szImageAlt; ?>" border="0" /> 
        </div>
        <?php if($iOfferType==4){  ?>
            <div class="service-offer-text"><?php echo t($t_base.'messages/only_catorn_displayed')."? <br>".t($t_base.'messages/receiver_best_offer') ?></div> 
        <?php } else {?>
            <div class="service-offer-text"><?php echo t($t_base.'messages/do_you_need')." ".$szImageAlt."? <br>".t($t_base.'messages/receiver_best_offer') ?></div> 
        <?php }?>
        <div class="btn">  
            <a href="javascript:void(0)" class="button-yellow" onclick="add_courier_service_rfq('<?php echo $postSearchAry['szBookingRandomNum']; ?>','<?php echo $iOfferType; ?>','<?php echo $_REQUEST['lang']; ?>')"><span><?php echo t($t_base.'fields/get_offer'); ?></span></a>                                         
        </div>
    </div>
    <?php
}
function simple_serach_html($postSearchAry,$searchResult,$t_base,$countryKeyValueAry,$forwarder_flag=false) //WTW
{	
    $kWHSSearch=new cWHSSearch();
    $distance_1_header = t($t_base.'messages/DISTANCE_TO_WAREHOUSE_1_TOOL_TIP_HEADER_TEXT_1')." ".$postSearchAry['szOriginCity'].".".t($t_base.'messages/DISTANCE_TO_WAREHOUSE_1_TOOL_TIP_HEADER_TEXT_2');
    $distance_2_header = t($t_base.'messages/DISTANCE_TO_WAREHOUSE_2_TOOL_TIP_HEADER_TEXT_1')." ".$postSearchAry['szDestinationCity'].".".t($t_base.'messages/DISTANCE_TO_WAREHOUSE_2_TOOL_TIP_HEADER_TEXT_2');
    $detail_style="style='text-decoration:none;color:white;'" ;
	
	?>	
	<script type="text/javascript">
		$().ready(function() {
			$("#datepicker1_search_services").datepicker();
		});
	</script>
	<div id="regError" class="errorBox" style="display:none;width:850px;">
	</div>
	<form action="<?=__SELECT_SERVICES_URL__?>" method="post" id="landing_page_form">
	<div class="clearfix services-filters">
            <span><a href="<?php echo __SELECT_SIMPLE_SEARCH_URL__?><?php echo $postSearchAry['szBookingRandomNum']?>/"><img src="<?php echo __BASE_STORE_IMAGE_URL__?>/services-field-arrow-left.png"/></a></span> 
            <span class="one">
            <?=t($t_base.'fields/pick_up');?><br/>
            <input type="checkbox" name="searchAry[szPickBox]" id="szPickBox" <?php if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__){?> checked <?php }?> value="<?php echo __SERVICE_TYPE_DTD__;?>" onclick="change_service_type_image_new(this.value,'<?php echo $iLoadDanishImage; ?>');change_postcode_city_default_text(this.value,'<?=t($t_base.'fields/type_name');?>','<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>');">
            <input type="hidden" name="searchAry[idServiceType]" id="szTransportation" value="<?php echo $postSearchAry['idServiceType'];?>">
            <input type="hidden" name="dServiceTypeValue" id="dServiceType" value="<?php echo __SERVICE_TYPE_PTD__;?>"/>			
            <input type="hidden" id="iDestinationCC" name="searchAry[idCC][1]" value="1"  />		
            </span>
            <span class="two">
                    <input type="text"  style="width:200px;" name="searchAry[szOriginCountryStr]" id="szOriginCountryStr"  value="<?php echo $postSearchAry['szOriginCountry'];?>">
                    <input type="hidden" style="width:200px;" name="searchAry[szOriginCountry]" id="szOriginCountry"  value="<?php echo $postSearchAry['szOriginCountry'];?>">
            </span>
            <span class="three">
                    <input type="text" style="width:200px;" name="searchAry[szDestinationCountryStr]" id="szDestinationCountryStr" value="<?php echo $postSearchAry['szDestinationCountry'];?>" >
                    <input type="hidden" style="width:200px;" name="searchAry[szDestinationCountry]" id="szDestinationCountry" value="<?php echo $postSearchAry['szDestinationCountry'];?>" >
            </span>
            <span class="four">
                    <input type="hidden" name="searchAry[idTimingType]" id="szTiming" value="1">
                    <input id="datepicker1_search_services" name="searchAry[dtTiming]" onfocus="blank_me(this.id,'<?=t($t_base.'fields/date_format');?>')" onblur="show_me(this.id,'<?=t($t_base.'fields/date_format');?>')" type="text" value="<? if($postSearchAry['dtTimingDate']!='0000-00-00 00:00:00' && !empty($postSearchAry['dtTimingDate'])) {?><?=date('d/m/Y',strtotime($postSearchAry['dtTimingDate']))?><? }else {?><?=date('d/m/Y')?><? }?>" />
            </span>
            <span class="five">
                    <?=t($t_base.'fields/cm');?><br/>
                    <input type="text" style="width:40px;" name="searchAry[iVolume]" id="iVolume"  value="<?php echo round((float)$postSearchAry['fCargoVolume'],1);?>">
            </span>
            <span class="six">
                    <?=t($t_base.'fields/kg');?><br/>
                    <input id="iWeight" type="text" onkeypress='return isNumberKey(event)' style="width:60px;" value="<?php echo ceil($postSearchAry['fCargoWeight']);?>" name="searchAry[iWeight]" >
            </span>
            <span class="seven"><a href="javascipt:void(0);" onclick="return validateLandingPageForm('landing_page_form','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>');" class="button-green1"><span><?=t($t_base.'fields/update');?></span> </a></span>
	</div>
	<input type="hidden" name="searchAry[szPageLoaction]" id="szPageLoaction" value="1" />
	<input type="hidden" name="searchAry[szBookingRandomNum]" id="szBookingRandomNum" value="<?=$postSearchAry['szBookingRandomNum']?>" />
	<input type="hidden" name="searchAry[szPageName]" id="szPageName" value="SIMPLEPAGE" />
	<input type="hidden" name="szBookingRandomNum_hidden" id="szBookingRandomNum_hidden" value="<?php echo $postSearchAry['szBookingRandomNum']?> ">
</form><br/><br/>
<?php
	echo display_searvice_details($postSearchAry,$searchResult,$t_base,$countryKeyValueAry,$forwarder_flag);
?>
	<script type="text/javascript">
		function initialize2() {
               			//var options = {componentRestrictions: { country: 'US' }};
                       var input1 = document.getElementById('szOriginCountryStr');
                      //alert(input);
                       var autocomplete1 = new google.maps.places.Autocomplete(input1);
                       
                       var options2 = {componentRestrictions: { country: 'DK' }};
                       var input2 = document.getElementById('szDestinationCountryStr');
                      //alert(input);
                       var autocomplete2 = new google.maps.places.Autocomplete(input2,options2);
               }
		$().ready(function() {	
		
				initialize2();
               //google.maps.event.addDomListener(window, 'load', initialize1);
               });
              </script>
	<div id="rating_popup"></div>
	<?
}

function display_searvice_details($postSearchAry,$searchResult,$t_base,$countryKeyValueAry,$forwarder_flag=false)
{
	$kWHSSearch=new cWHSSearch();
	$distance_1_header = t($t_base.'messages/DISTANCE_TO_WAREHOUSE_1_TOOL_TIP_HEADER_TEXT_1')." ".$postSearchAry['szOriginCity'].".".t($t_base.'messages/DISTANCE_TO_WAREHOUSE_1_TOOL_TIP_HEADER_TEXT_2');
	$distance_2_header = t($t_base.'messages/DISTANCE_TO_WAREHOUSE_2_TOOL_TIP_HEADER_TEXT_1')." ".$postSearchAry['szDestinationCity'].".".t($t_base.'messages/DISTANCE_TO_WAREHOUSE_2_TOOL_TIP_HEADER_TEXT_2');
	$detail_style="style='text-decoration:none;color:white;'" ;
	?>
	
	<table cellpadding="0" id="service_table"  cellspacing="0" border="0" class="format-1" width="100%">
		<tr>
			
			<th width="12%" align="center" valign="top"></th>
			<th width="11%" align="center" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=t($t_base.'fields/rating');?></th>
			<th width="11%" align="center" valign="top">&nbsp;&nbsp;&nbsp;<?php if($postSearchAry['idServiceType']==__SERVICE_TYPE_PTD__){?><?=t($t_base.'fields/cut_off');?> <?php }else{?> <?=t($t_base.'fields/pick_up_value');?> <?php }?></th>
			<th width="11%" align="center" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=t($t_base.'fields/delivery');?></th>
			<th width="11%" align="center" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;<?=ucwords(t($t_base.'fields/days'));?></th>
			<th width="11%" align="center" valign="top"><?=t($t_base.'fields/all_in_price');?> </th>
			<?php
				if(!$forwarder_flag){
			?>
			<th width="11%"></th>
			<?php }?>
		</tr>
		<?
			if(!empty($searchResult))
			{
				$google_map_anchor_style="style='text-decoration:none;color:black;font-style:normal;font-size:12px;'";
				$minRatingToShowStars = $kWHSSearch->getManageMentVariableByDescription('__MINIMUM_RATING_TO_SHOW_RATING_STARS__');
				$tr_counter = 1;
				foreach($searchResult as $searchResults)
				{
					if($forwarder_flag)
					{
						$postSearchAry = array();
						$postSearchAry = $_SESSION['try_it_out_searched_data'] ;
						
						$originPostCodeAry=array();
						$kConfig = new cConfig();
						$originPostCodeAry = $kConfig->getPostCodeDetails($postSearchAry['szOriginCountry'],$postSearchAry['szOriginPostCode'],$postSearchAry['szOriginCity']);
						
						$szOriginAddress =  $postSearchAry['szOriginPostCode']." - ".$postSearchAry['szOriginCity']."<br /> ".$originPostCodeAry['szCountry'];
					    $szDestinationAddress = $searchResults['szFromWHSPostCode']." - ".$searchResults['szFromWHSCity']."<br />".$searchResults['szFromWHSCountry'];
					    $origin_lat_lang_pipeline = $originPostCodeAry['szLatitude']."|".$originPostCodeAry['szLongitude']."|".$searchResults['szFromWHSLat']."|".$searchResults['szFromWHSLong']."|".$searchResults['iDistanceToWHS']."|".$szOriginAddress."|".$szDestinationAddress;
					
						$destinationPostCodeAry=array();
						$destinationPostCodeAry = $kConfig->getPostCodeDetails($postSearchAry['szDestinationCountry'],$postSearchAry['szDestinationPostCode'],$postSearchAry['szDestinationCity']);
						
						$szOriginAddress2 =  $postSearchAry['szDestinationPostCode']." - ".$postSearchAry['szDestinationCity']."<br /> ".$destinationPostCodeAry['szCountry'];
					    $szDestinationAddress2 = $searchResults['szToWHSPostCode']." - ".$searchResults['szToWHSCity']."<br />".$searchResults['szToWHSCountry'];
					
					    $destination_lat_lang_pipeline = $destinationPostCodeAry['szLatitude']."|".$destinationPostCodeAry['szLongitude']."|".$searchResults['szToWHSLat']."|".$searchResults['szToWHSLong']."|".$searchResults['iDistanceToWHS']."|".$szOriginAddress2."|".$szDestinationAddress2;
					}
					else
					{
						$szOriginAddress =  $postSearchAry['szOriginPostCode']." - ".$postSearchAry['szOriginCity']."<br /> ".$postSearchAry['szOriginCountry'];
		                $szDestinationAddress = $searchResults['szFromWHSPostCode']." - ".$searchResults['szFromWHSCity']."<br />".$searchResults['szFromWHSCountry'];
		    
						$origin_lat_lang_pipeline = $postSearchAry['fOriginLatitude']."|".$postSearchAry['fOriginLongitude']."|".$searchResults['szFromWHSLat']."|".$searchResults['szFromWHSLong']."|".$searchResults['iDistanceFromWHS']."|".$szOriginAddress."|".$szDestinationAddress;
						
						$szOriginAddress2 =  $postSearchAry['szDestinationPostCode']." - ".$postSearchAry['szDestinationCity']."<br /> ".$postSearchAry['szDestinationCountry'];
		                $szDestinationAddress2 = $searchResults['szToWHSPostCode']." - ".$searchResults['szToWHSCity']."<br />".$searchResults['szToWHSCountry'];
						$destination_lat_lang_pipeline = $postSearchAry['fDestinationLatitude']."|".$postSearchAry['fDestinationLongitude']."|".$searchResults['szToWHSLat']."|".$searchResults['szToWHSLong']."|".$searchResults['iDistanceToWHS']."|".$szOriginAddress2."|".$szDestinationAddress2;
					}
					
					$szWarehousecityFrom = $searchResults['szFromWHSCity'];
					$szWarehousecityTo = $searchResults['szToWHSCity'];
					
					$fTotalPriceDisplay = $searchResults['fDisplayPrice'];
					?>						
					<tr id="select_services_tr_<?=$tr_counter?>">
						<td><a href="javascript:void(0)" onclick="openForwarderRatingPopUp('<?=$searchResults['idForwarder']?>')" style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;">
							<? 
							if(!empty($searchResults['szLogo'])){ ?>
								<img src="<?=__BASE_STORE_INC_URL__.'/image.php?img='.$searchResults['szLogo']."&w=".__MAX_ALLOWED_FORWARDER_LOGO_WIDTH__."&h=".__MAX_ALLOWED_FORWARDER_LOGO_HEIGHT__?>" alt="<?=$searchResults['szDisplayName']?>" border="0" />
							<? }else {
								echo (strlen($searchResults['szDisplayName'])<=22)?$searchResults['szDisplayName']:mb_substr($searchResults['szDisplayName'],0,20,'UTF8')."..";
							 }
							 ?>
							</a>
						</td>
						<td>
							<a href="javascript:void(0)" onclick="openForwarderRatingPopUp('<?=$searchResults['idForwarder']?>')" style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;"><? 
								if($searchResults['iNumRating']<$minRatingToShowStars){ 
									echo displayRatingStar(0); 
								}else {									
									// one filled star's width is 18px so that we are multiplying Avg Rating with 10 
									$fAvgRating = round(($searchResults['fAvgRating']*18),2); 
									echo displayRatingStar($fAvgRating);
								}
							?>
							<span>(<?=(int)$searchResults['iNumRating']?>)</a></span>
						</td>
						
						<td><?=date("j F",strtotime(str_replace("/","-",trim($searchResults['dtCutOffDate']))));?></td>
						<td><?=date("j F",strtotime(str_replace("/","-",trim($searchResults['dtAvailableDate']))));?></td>
						
						<?php
						
								$start=date("Y-m-d",strtotime(str_replace("/","-",trim($searchResults['dtCutOffDate']))));
								$end=date("Y-m-d",strtotime(str_replace("/","-",trim($searchResults['dtAvailableDate']))));
								$days_between = ceil(abs(strtotime($end) - strtotime($start)) / 86400);
								?>
							<td><?=$days_between?></td>
						<? if(!$forwarder_flag){ 
							?>
							<td><?=$searchResults['szCurrency']?> <?=number_format((float)$searchResults['fDisplayPrice'])?><br /></td>
							<td>
								<a href="javascript:void(0)" onclick="display_forwarder_non_accepted_popup('<?=$searchResults['unique_id']?>','<?=$searchResults['idForwarder']?>','<?=$_SESSION['user_id']?>','<?=__BOOKING_DETAILS_PAGE_URL__?>');" class="button-green1"><span><?php echo t($t_base.'fields/book_now'); ?></span></a><br />
								<?
							if(__DISPLAY_CALCULATION_DETAILS__)
							{
							?>
								<a href="<?=__BASE_URL__.'/ajax_displayCalculationDetails.php?idWTW='.$searchResults['unique_id'].'&booking_random_num='.$postSearchAry['szBookingRandomNum']?>" <?=$detail_style?> target="_blank"> Details</a>
							<?
							}?>
							</td>
						<?php						
						}	
						else {
						
						// else part is called from forwarder try it now section. 
						?>
						<td>
							<a href="javascript:void(0)" onclick="display_fowarder_price_details('<?=$searchResults['unique_id']?>','select_services_tr_<?=$tr_counter?>')" style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;">
							<?=$searchResults['szCurrency']?><br /><?=number_format((float)$searchResults['fDisplayPrice'],2)?></a><br />
						</td>
					<?
						}
					?>	
					</tr>
					<?
					$tr_counter++;
				}
			}
			else
			{
				?>
				<tr>
					<td colspan="9"><h2><?=t($t_base.'messages/NO_SERVICE_MATCHING');?></h2> </td>
				</tr>
				<?
			}
		?>
	</table>
	
	<?php
}
function toArray($obj)
{
    if (is_object($obj)) $obj = (array)$obj;
    if (is_array($obj)) {
        $new = array();
        foreach ($obj as $key => $val) {
            $new[$key] = toArray($val);
        }
    } else {
        $new = $obj;
    } 
    return $new;
}

function getCountryIdByAddress($szCountryStr)
{
    $szCountryStr = str_replace(' ','+',$szCountryStr);
    $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$szCountryStr.'&sensor=false');
    $output= json_decode($geocode);
    $geocodeArr=toArray($output);
    //print_r($geocodeArr);
    $resultArr['szCountryName']=$geocodeArr['results'][0]['address_components'][2]['long_name'];
}

function reverse_geocode($szCountryStr,$iLandingPageFlag=false) 
{  
    $replaceArr=array(" ",",");
    $address = str_replace($replaceArr, "+", "$szCountryStr");
    
    $googleMapCookieListAry = array();
    
    if(preg_match('/copenhagen/',strtolower($address)))
    {
        $replaceTextArr=array("Copenhagen","copenhagen");
        $address=str_replace($replaceTextArr,"Cøpenhagen",$address);
    } 
    $kWhsSearch = new cWHSSearch();
    $szGoogleMapv3APIKey = $kWhsSearch->getManageMentVariableByDescription('__GOOGLE_MAP_V3_API_KEY__'); 
    
    try
    {
        $url = "https://maps.google.com/maps/api/geocode/json?address=$address&sensor=false"; 
        $c = curl_init(); 
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt ($c, CURLOPT_SSL_VERIFYHOST, false); 
        $result = curl_exec($c);
        $err  = curl_getinfo($c,CURLINFO_HTTP_CODE); 
        curl_close($c); 

        $json = json_decode($result); 
        $GeoResultAry = array();                                      
        $address_ctr = 0; 
        if(!empty($json->results))
        { 
            foreach ($json->results as $result)
            {  
                foreach($result->address_components as $addressPart) 
                {   
                    if(((in_array('locality', $addressPart->types)) && (in_array('political', $addressPart->types))) || (in_array('sublocality', $addressPart->types)) && (in_array('political', $addressPart->types)))
                    {
                        $city = $addressPart->long_name;
                    }
                    if(((in_array('establishment', $addressPart->types)) && (in_array('natural_feature', $addressPart->types))))
                    {
                        $city = $addressPart->long_name;
                    }
                    else if((in_array('administrative_area_level_1', $addressPart->types)) && (in_array('political', $addressPart->types)))
                    {
                        $state = $addressPart->long_name;
                        $stateCode = $addressPart->short_name;
                    }
                    else if((in_array('country', $addressPart->types)) && (in_array('political', $addressPart->types)))
                    {
                        $country = $addressPart->long_name;
                        $countryISOCode = $addressPart->short_name;
                    }
                    else if((in_array('postal_code', $addressPart->types)))
                    {
                        $postalCode = $addressPart->long_name;
                    }
                    else if((in_array('postal_town', $addressPart->types)))
                    {
                        $city = $addressPart->long_name;
                    }
                    else if((in_array('route', $addressPart->types)))
                        $route = $addressPart->long_name;
                    else if((in_array('street_number', $addressPart->types)))
                        $street_number = $addressPart->long_name;					
                }

                if(trim($country)=='Kingdom of Norway')
                {
                    $country = 'Norway' ;
                }
                if(empty($city))
                {
                    $city = $state ;
                }  
                if($iLandingPageFlag)
                {
                    $szLatititude = $result->geometry->location->lat;
                    $szLongitude = $result->geometry->location->lng; 
                    if((strtolower($city)=='shanghai'))
                    {
                        $postalCode = "200010" ; 
                        $GeoResultAry[$address_ctr]['szPostCode'] = $postalCode ;
                    }
                    else if($iLandingPageFlag && !empty($postalCode))
                    { 
                        $postalCode_lw = strtolower($postalCode);
                        $szCountryStr_lw = strtolower($szCountryStr); 

                        $pos = strpos($szCountryStr_lw, $postalCode_lw);    
                        if(trim($pos)!='')
                        { 
                            $GeoResultAry[$address_ctr]['szPostCode'] = $postalCode ; 
                        } 
                        else
                        { 
                            $GeoResultAry[$address_ctr]['szPostCode'] = "" ; 
                        }  
                    }
                    else if((strtolower($city)=='madrid') && empty($postalCode))
                    {
                        $postalCode = "28013" ; 
                        $GeoResultAry[$address_ctr]['szPostCode'] = "28013" ;
                    } 
                    else
                    {
                        $GeoResultAry[$address_ctr]['szPostCode'] = $postalCode ;
                    }
                    $GeoResultAry[$address_ctr]['szPostcodeTemp'] = $postalCode;
                    $GeoResultAry[$address_ctr]['szStreetNumber'] = $street_number ; 
                    $GeoResultAry[$address_ctr]['szRoute'] = $route ;
                    $GeoResultAry[$address_ctr]['szCityName'] = $city ;
                    $GeoResultAry[$address_ctr]['szState'] = $state ; 
                    $GeoResultAry[$address_ctr]['szStateCode'] = $stateCode ;  
                    $GeoResultAry[$address_ctr]['szCountryName'] = $country ;
                    $GeoResultAry[$address_ctr]['szCountryCode'] = $countryISOCode ; 
                    $GeoResultAry[$address_ctr]['szLat'] = $szLatititude;
                    $GeoResultAry[$address_ctr]['szLng'] = $szLongitude;
                    $address_ctr++; 
                }
            }
        }  
        if($iLandingPageFlag)
        { 
            return $GeoResultAry[0];
        }
        else
        { 
            /*
             * If user searched for Madrid and donot putted in postcode in search field then we use default postcode '28013'
             */ 
            if((strtolower($city)=='madrid'))
            { 
                $postalCode_lw = strtolower($postalCode);
                $szCountryStr_lw = strtolower($szCountryStr); 

                $pos = strpos($szCountryStr_lw, $postalCode_lw);  
                if(trim($pos)!='')
                { 
                    $postalCode = $postalCode ; 
                } 
                else
                { 
                    $postalCode = "28013" ; 
                }  
            } 

            if(($city != '') && ($state != '') && ($postalCode!='') && ($country != ''))
                $address = $city.', '.$state.', '.$postalCode.', '.$country;
            else if(($city != '') && ($state != '') && ($postalCode!=''))
                $address = $city.', '.$state.', '.$postalCode;
            else if(($state != '') && ($country != ''))
                $address = $state.', '.$country;
            else if($country != '')
                $address = $country;

            if(trim($country)=='Kingdom of Norway')
            {
                $country = 'Norway' ;
            }
            if(empty($city))
            {
                $city = $state ;
            } 
            $szLatititude = $json->results[0]->geometry->location->lat;
            $szLongitude = $json->results[0]->geometry->location->lng;

            $res_arr['szPostcodeTemp'] = $postalCode;
            $res_arr['szStreetNumber'] = $street_number;		
            $res_arr['szPostCode'] = $postalCode;	
            $res_arr['szRoute'] = $route;	
            $res_arr['szState'] = $state;	
            $res_arr['szStateCode'] = $stateCode ; 
            $res_arr['szCityName'] = $city;		
            $res_arr['szCountryName'] = $country;	
            $res_arr['szCountryCode'] = $countryISOCode ; 
            $res_arr['szLat'] = $szLatititude;
            $res_arr['szLng'] = $szLongitude;     
            return $res_arr; 
        }
    } 
    catch (Exception $ex) 
    {
        //@TO DO        
    } 
}

function reverse_geocode_backup($szCountryStr,$iLandingPageFlag=false) 
{  
    $replaceArr=array(" ",",");
    $address = str_replace($replaceArr, "+", "$szCountryStr");
    
    $googleMapCookieListAry = array();
    
    if(preg_match('/copenhagen/',strtolower($address)))
    {
        $replaceTextArr=array("Copenhagen","copenhagen");
        $address=str_replace($replaceTextArr,"Cøpenhagen",$address);
    } 
    $url = "https://maps.google.com/maps/api/geocode/json?address=$address&sensor=false";
    //echo $url;
    $c = curl_init();
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_URL, $url);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($c);
    $err  = curl_getinfo($c,CURLINFO_HTTP_CODE);
//    $err  = curl_getinfo($c);
    curl_close($c); 
    
    $json = json_decode($result); 
    $GeoResultAry = array();                                      
    $address_ctr = 0; 
    if(!empty($json->results))
    { 
        foreach ($json->results as $result)
        {  
            foreach($result->address_components as $addressPart) 
            {   
                if(((in_array('locality', $addressPart->types)) && (in_array('political', $addressPart->types))) || (in_array('sublocality', $addressPart->types)) && (in_array('political', $addressPart->types)))
                {
                    $city = $addressPart->long_name;
                }
                if(((in_array('establishment', $addressPart->types)) && (in_array('natural_feature', $addressPart->types))))
                {
                    $city = $addressPart->long_name;
                }
                else if((in_array('administrative_area_level_1', $addressPart->types)) && (in_array('political', $addressPart->types)))
                {
                    $state = $addressPart->long_name;
                    $stateCode = $addressPart->short_name;
                }
                else if((in_array('country', $addressPart->types)) && (in_array('political', $addressPart->types)))
                {
                    $country = $addressPart->long_name;
                    $countryISOCode = $addressPart->short_name;
                }
                else if((in_array('postal_code', $addressPart->types)))
                {
                    $postalCode = $addressPart->long_name;
                }
                else if((in_array('postal_town', $addressPart->types)))
                {
                    $city = $addressPart->long_name;
                }
                else if((in_array('route', $addressPart->types)))
                    $route = $addressPart->long_name;
                else if((in_array('street_number', $addressPart->types)))
                    $street_number = $addressPart->long_name;					
            }
            
            if(trim($country)=='Kingdom of Norway')
            {
                $country = 'Norway' ;
            }
            if(empty($city))
            {
                $city = $state ;
            }  
            if($iLandingPageFlag)
            {
                $szLatititude = $result->geometry->location->lat;
                $szLongitude = $result->geometry->location->lng; 
                if((strtolower($city)=='shanghai'))
                {
                    $postalCode = "200010" ; 
                    $GeoResultAry[$address_ctr]['szPostCode'] = $postalCode ;
                }
                else if($iLandingPageFlag && !empty($postalCode))
                { 
                    $postalCode_lw = strtolower($postalCode);
                    $szCountryStr_lw = strtolower($szCountryStr); 
                    
                    $pos = strpos($szCountryStr_lw, $postalCode_lw);    
                    if(trim($pos)!='')
                    { 
                        $GeoResultAry[$address_ctr]['szPostCode'] = $postalCode ; 
                    } 
                    else
                    { 
                        $GeoResultAry[$address_ctr]['szPostCode'] = "" ; 
                    }  
                }
                else if((strtolower($city)=='madrid') && empty($postalCode))
                {
                    $postalCode = "28013" ; 
                    $GeoResultAry[$address_ctr]['szPostCode'] = "28013" ;
                } 
                else
                {
                    $GeoResultAry[$address_ctr]['szPostCode'] = $postalCode ;
                }
                $GeoResultAry[$address_ctr]['szPostcodeTemp'] = $postalCode;
                $GeoResultAry[$address_ctr]['szStreetNumber'] = $street_number ; 
                $GeoResultAry[$address_ctr]['szRoute'] = $route ;
                $GeoResultAry[$address_ctr]['szCityName'] = $city ;
                $GeoResultAry[$address_ctr]['szState'] = $state ; 
                $GeoResultAry[$address_ctr]['szStateCode'] = $stateCode ;  
                $GeoResultAry[$address_ctr]['szCountryName'] = $country ;
                $GeoResultAry[$address_ctr]['szCountryCode'] = $countryISOCode ; 
                $GeoResultAry[$address_ctr]['szLat'] = $szLatititude;
                $GeoResultAry[$address_ctr]['szLng'] = $szLongitude;
                $address_ctr++; 
            }
        }
    }  
    if($iLandingPageFlag)
    { 
        return $GeoResultAry[0];
    }
    else
    { 
        /*
         * If user searched for Madrid and donot putted in postcode in search field then we use default postcode '28013'
         */ 
        if((strtolower($city)=='madrid'))
        { 
            $postalCode_lw = strtolower($postalCode);
            $szCountryStr_lw = strtolower($szCountryStr); 

            $pos = strpos($szCountryStr_lw, $postalCode_lw);  
            if(trim($pos)!='')
            { 
                $postalCode = $postalCode ; 
            } 
            else
            { 
                $postalCode = "28013" ; 
            }  
        } 
        
        if(($city != '') && ($state != '') && ($postalCode!='') && ($country != ''))
            $address = $city.', '.$state.', '.$postalCode.', '.$country;
        else if(($city != '') && ($state != '') && ($postalCode!=''))
            $address = $city.', '.$state.', '.$postalCode;
        else if(($state != '') && ($country != ''))
            $address = $state.', '.$country;
        else if($country != '')
            $address = $country;

        if(trim($country)=='Kingdom of Norway')
        {
            $country = 'Norway' ;
        }
        if(empty($city))
        {
            $city = $state ;
        } 
        $szLatititude = $json->results[0]->geometry->location->lat;
        $szLongitude = $json->results[0]->geometry->location->lng;
                                                
        $res_arr['szPostcodeTemp'] = $postalCode;
        $res_arr['szStreetNumber'] = $street_number;		
        $res_arr['szPostCode'] = $postalCode;	
        $res_arr['szRoute'] = $route;	
        $res_arr['szState'] = $state;	
        $res_arr['szStateCode'] = $stateCode ; 
        $res_arr['szCityName'] = $city;		
        $res_arr['szCountryName'] = $country;	
        $res_arr['szCountryCode'] = $countryISOCode ; 
        $res_arr['szLat'] = $szLatititude;
        $res_arr['szLng'] = $szLongitude;     
        return $res_arr; 
    } 
}

function get_user_session_data($booking_process=false)
{ 
    $userSessionAry = array();
    $userSessionAry['idUser'] = $_SESSION['user_id'] ; 
    return $userSessionAry;
}
function showSearchResultLevel3($kExplain,$level3DataArr,$szSearchText,$iLanguage)
{	

	$t_base = "ExplainPage/";
	if(!empty($level3DataArr))
	{
		foreach($level3DataArr as $level3DataArrs)
		{
			if($level3DataArrs['szLink']!='')
			{
				$szPageUrl=$level3DataArrs['szLink'];
			}
			else if($level3DataArrs['iKnowledgeCenter']==1)
			{ 
				$level3DataArrs['szPageHeading'] = t($t_base.'fields/knowledge_centre');
				$level3DataArrs['szDescription'] = html_entity_decode($level3DataArrs['szDescription']);
				
				$szPageUrl='';
				$szPageUrl=__BLOG_PAGE_URL__."".$level3DataArrs['szUrl']."/";
			}
//			else if($level3DataArrs['iSeoPage']==1)
//			{ 
//				$level3DataArrs['szPageHeading'] = t($t_base.'fields/information');
//				$level3DataArrs['szDescription'] = $level3DataArrs['szDescription'];
//				
//				$szPageUrl='';
//				$szPageUrl=__INFORMATION_PAGE_URL__."/".$level3DataArrs['szUrl']."/";
//			} 
			
			?>
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td><a href="<?php echo $szPageUrl;?>" target="_blank"><?php echo $level3DataArrs['szPageHeading']?> | <?php echo $level3DataArrs['szHeading']?></a></td>
				</tr>
				<tr>
					<td class="pagelink"><?php echo $szPageUrl;?></td>
				</tr>
				<tr>
					<td class="des"><?php 
						$totalCount=count(explode(" ", $level3DataArrs['szDescription']));
						
						$replaceText="<strong>".$szSearcformat_post_arrayhText."</strong>";
						if($totalCount>20)
						{
							
							$msg=limit_text(sanitize_all_html_input($level3DataArrs['szDescription']),20);
							$msg = str_replace($szSearchText,$replaceText,$msg);
							echo $msg;
						}
						else
						{
							$msg = str_replace($szSearchText,$replaceText,sanitize_all_html_input($level3DataArrs['szDescription']));
							echo $msg;
							
						}
					?></td>
				</tr>
			</table>
		<?php		
		}
	}
	else
	{
		echo t($t_base.'fields/no_record_found');
	}
}
function limit_text($text, $limit) 
{
    $strings = $text;  
    if (strlen($text) > $limit) 
    { 
        $words = str_word_count($text, 2);  
        $pos = array_keys($words); 
        if(sizeof($pos) >$limit)
        {
          $text = substr($text, 0, $pos[$limit]) . '...';
        } 
        return $text;
    }
    return $text; 
 }
 function limit_text_forwarder($text, $limit) 
{
    $strings = $text;  
    if (strlen($text) > $limit) 
    {  
        $text = substr($text, 0, ($limit-3)) . '...'; 
        return $text;
    }
    return $text; 
 }
    
    function checkPasswordAuthentication($kUser)
    {
        $errorFlag = 1 ;
         
        if($kUser->iConfirmed==1)
        {
            if($kUser->iFirstTimePassword==1)
            { 
                $errorFlag = 2;
            }
            else
            { 
                //check if there is cookie for password is set or not
                if(empty($_COOKIE['__USER_PASSWORD_COOKIE__']) || ($_COOKIE['__USER_PASSWORD_COOKIE__']!=$kUser->szUserPasswordKey))
                {
                    $errorFlag = 2;
                } 
            } 
        }
        else
        {
            $errorFlag = 2;
        } 
		
        if($errorFlag==2)
        {
            //ob_end_clean();
            //header('Location: '.__BASE_URL__);
            $t_base_user = "Users/AccountPage/";
            ?>
            <div id="email_vification_popup">
            <div id="popup-bg"></div>
            <div id="popup-container">
            <div class="popup signin-popup signin-popup-verification">
            <h5><?=t($t_base_user.'fields/authenication_header')?></h5> 
            <p><?=t($t_base_user.'fields/authenication_message');?></p><br/>
            <p align="center">
                    <a href="javascript:void(0);" onclick="redirect_url('<?php echo __LANDING_PAGE_URL__; ?>');" class="button1"><span><?=t($t_base_user.'messages/close');?></span></a>
            </p> 
            </div>
            </div>
            </div>
            <?php
            die();
        }
  }
  
  function include_classes()
  {
    require_once(__APP_PATH_CLASSES__."/error.class.php");
    require_once(__APP_PATH_CLASSES__."/database.class.php");
    require_once(__APP_PATH_CLASSES__."/config.class.php");
    require_once(__APP_PATH_CLASSES__."/warehouseSearch.class.php");
    require_once(__APP_PATH__."/lib/Horde/Yaml.php");
    require_once(__APP_PATH__."/lib/Horde/Yaml/Loader.php");
    require_once(__APP_PATH__."/lib/Horde/Yaml/Node.php");
    require_once(__APP_PATH__."/lib/Horde/Yaml/Dumper.php");
    require_once(__APP_PATH__."/lib/Horde/Yaml/Exception.php");
    require_once(__APP_PATH_CLASSES__.'/admin.class.php');
    require_once(__APP_PATH_CLASSES__."/services.class.php");
    require_once(__APP_PATH_CLASSES__."/user.class.php");  
    require_once(__APP_PATH_CLASSES__."/registeredShippersConsignees.class.php"); 
    require_once(__APP_PATH_CLASSES__."/booking.class.php");
    require_once(__APP_PATH_CLASSES__."/forwarder.class.php");
    require_once(__APP_PATH__.'/forwarders/html2pdf/html2fpdf.php');
    require_once(__APP_PATH__.'/invoice/fpdf.php'); 
    require_once(__APP_PATH_CLASSES__.'/exportimport.class.php');
    require_once(__APP_PATH_CLASSES__.'/paypal.class.php'); 
    require_once(__APP_PATH_CLASSES__.'/zooz.class.php');
    require_once(__APP_PATH__.'/forwarders/html2pdf/fpdf.php');
    require_once(__APP_PATH_CLASSES__.'/forwarderContact.class.php'); 
    require_once(__APP_PATH_CLASSES__.'/uploadBulkService.class.php'); 
    require_once(__APP_PATH_CLASSES__.'/affiliate.class.php'); 
    require_once(__APP_PATH_CLASSES__.'/dbimport.class.php');
    require_once(__APP_PATH_CLASSES__.'/admin.class.php'); 
    require_once(__APP_PATH_CLASSES__.'/rss.class.php'); 
    require_once(__APP_PATH_CLASSES__.'/haulagePricing.class.php'); 
    require_once(__APP_PATH_CLASSES__.'/explain.class.php'); 
    require_once(__APP_PATH_CLASSES__.'/billing.class.php'); 
    require_once(__APP_PATH_CLASSES__.'/dashboardAdmin.class.php');
    require_once(__APP_PATH_CLASSES__.'/report.class.php'); 
    require_once(__APP_PATH_CLASSES__.'/nps.class.php');
    require_once(__APP_PATH_CLASSES__.'/seo.class.php'); 
    require_once(__APP_PATH_CLASSES__.'/insurance.class.php'); 
    require_once(__APP_PATH_CLASSES__.'/courierServices.class.php');  
    require_once(__APP_PATH_CLASSES__."/easyPost.class.php");
    require_once(__APP_PATH_CLASSES__."/vatApplication.class.php");
    require_once(__APP_PATH_CLASSES__."/webServices.class.php");
    require_once(__APP_PATH_CLASSES__."/mautic.class.php"); 
    require_once(__APP_PATH_CLASSES__."/quote.class.php"); 
    
} 
  
function getPriceByLang($fPrice,$iLanguage=false,$iRoundTo=0)
{
    //    if($iLanguage==__LANGUAGE_ID_DANISH__)
//    { 
//        $fFormatedPrice = number_format((float)$fPrice,(int)$iRoundTo,',','.');
//    }
//    else
//    { 
//        $fFormatedPrice = number_format((float)$fPrice,(int)$iRoundTo,'.',',');
//    }
    $fFormatedPrice = number_format_custom((float)$fPrice,$iLanguage,(int)$iRoundTo);
    
    return $fFormatedPrice; 
}

function format_post_array($data)
{
    $postDataAry = array() ;
    $postDataAry = $data ; 
    $tempPostSearchary = $data;  
    
    /*
     *                                             
    ksort($postDataAry['iActive']); 
    ksort($postDataAry['iInsuranceComments']);
    
    if(!empty($data['idForwarder']))
    {
        $postDataAry['idForwarder'] = array_values($data['idForwarder']) ;
    }
    if(!empty($data['idTransportMode']))
    {
        $postDataAry['idTransportMode'] = array_values($data['idTransportMode']) ;
    }
    if(!empty($data['idBookingQuote']))
    {
        $postDataAry['idBookingQuote'] = array_values($data['idBookingQuote']) ;
    }
    if(!empty($data['idForwarderCurrency']))
    {
        $postDataAry['idForwarderCurrency'] = array_values($data['idForwarderCurrency']) ;
    }
    if(!empty($data['fTotalPriceForwarderCurrency']))
    {
        $postDataAry['fTotalPriceForwarderCurrency'] = array_values($data['fTotalPriceForwarderCurrency']) ;
    }
    if(!empty($data['fTotalVatForwarder']))
    {
        $postDataAry['fTotalVatForwarder'] = array_values($data['fTotalVatForwarder']) ;
    }
    if(!empty($data['iTransitHours']))
    {
        $postDataAry['iTransitHours'] = array_values($data['iTransitHours']) ;
    }
    if(!empty($data['dtQuoteValidTo']))
    {
        $postDataAry['dtQuoteValidTo'] = array_values($data['dtQuoteValidTo']) ;
    }
    if(!empty($data['szForwarderComment']))
    {
        $postDataAry['szForwarderComment'] = array_values($data['szForwarderComment']) ;
    }
    if(!empty($data['fQuotePriceCustomerCurrency']))
    {
        $postDataAry['fQuotePriceCustomerCurrency'] = array_values($data['fQuotePriceCustomerCurrency']) ;
    }
    if(!empty($data['fReferalPercentage']))
    {
        $postDataAry['fReferalPercentage'] = array_values($data['fReferalPercentage']) ; 
    }
    if(!empty($data['fReferalAmount']))
    {
        $postDataAry['fReferalAmount'] = array_values($data['fReferalAmount']) ;  
    }
    if(!empty($data['fTotalPriceCustomerCurrency']))
    {
        $postDataAry['fTotalPriceCustomerCurrency'] = array_values($data['fTotalPriceCustomerCurrency']) ; 
    }
    if(!empty($data['fTotalVat']))
    {
        $postDataAry['fTotalVat'] = array_values($data['fTotalVat']) ; 
    }
    if(!empty($data['fTotalInsuranceCostForBookingCustomerCurrency']))
    {
        $postDataAry['fTotalInsuranceCostForBookingCustomerCurrency'] = array_values($data['fTotalInsuranceCostForBookingCustomerCurrency']) ; 
    }  
    
    if(!empty($data['fCustomerExchangeRate']))
    {
        $postDataAry['fCustomerExchangeRate'] = array_values($data['fCustomerExchangeRate']) ;  
    } 
    if(!empty($data['fForwarderExchangeRate']))
    {
        $postDataAry['fForwarderExchangeRate'] = array_values($data['fForwarderExchangeRate']) ;  
    }
    if(!empty($data['fTotalVatCustomerCurrency']))
    {
        $postDataAry['fTotalVatCustomerCurrency'] = array_values($data['fTotalVatCustomerCurrency']) ;  
    }
    if(!empty($data['fPriceMarkUp']))
    {
        $postDataAry['fPriceMarkUp'] = array_values($data['fPriceMarkUp']) ;  
    }
    if(!empty($data['fTotalPriceForwarderCurrency_usd']))
    {
        $postDataAry['fTotalPriceForwarderCurrency_usd'] = array_values($data['fTotalPriceForwarderCurrency_usd']) ;  
    }
    if(!empty($data['fTotalVat_usd']))
    {
        $postDataAry['fTotalVat_usd'] = array_values($data['fTotalVat_usd']) ;  
    } 
    if(!empty($data['szTransportMode']))
    {
        $postDataAry['szTransportMode'] = array_values($data['szTransportMode']) ;  
    }
    if(!empty($data['szForwarderDispName']))
    {
        $postDataAry['szForwarderDispName'] = array_values($data['szForwarderDispName']) ;  
    }
    if(!empty($data['szQuotePrice']))
    {
        $postDataAry['szQuotePrice'] = array_values($data['szQuotePrice']) ;  
    }
    if(!empty($data['szTotalVatForwarderPrice']))
    {
        $postDataAry['szTotalVatForwarderPrice'] = array_values($data['szTotalVatForwarderPrice']) ;  
    }
    if(!empty($data['szTransitTime']))
    {
        $postDataAry['szTransitTime'] = array_values($data['szTransitTime']) ;  
    }
    if(!empty($data['dtValidity']))
    {
        $postDataAry['dtValidity'] = array_values($data['dtValidity']) ;  
    }
    if(!empty($data['iAddingNewQuote']))
    {
        $postDataAry['iAddingNewQuote'] = array_values($data['iAddingNewQuote']) ;  
    }
    if(!empty($data['iProfitType']))
    {
        $postDataAry['iProfitType'] = array_values($data['iProfitType']) ;  
    }  
    */
    $data = $tempPostSearchary; 
    $idBookingQuotePricingAry = $data['idBookingQuotePricing'];  
    $postDataAry = array(); 
    if(!empty($idBookingQuotePricingAry))
    {
        $i=0;
        foreach($idBookingQuotePricingAry as $key=>$idBookingQuotePricingArys)
        {
            $postDataAry['iActive'][$i] = $data['iActive'][$key];
            $postDataAry['iRemovePriceGuarantee'][$i] = $data['iRemovePriceGuarantee'][$key]; 
            $postDataAry['iInsuranceComments'][$i] = $data['iInsuranceComments'][$key] ; 
            $postDataAry['iTransitHours'][$i] = $data['iTransitHours'][$key] ; 
            $postDataAry['fCustomerExchangeRate'][$i] = $data['fCustomerExchangeRate'][$key] ; 
            $postDataAry['fForwarderExchangeRate'][$i] = $data['fForwarderExchangeRate'][$key] ; 
            $postDataAry['fTotalVatCustomerCurrency'][$i] = $data['fTotalVatCustomerCurrency'][$key] ; 
            $postDataAry['fPriceMarkUp'][$i] = $data['fPriceMarkUp'][$key] ; 
            $postDataAry['fTotalPriceForwarderCurrency_usd'][$i] = $data['fTotalPriceForwarderCurrency_usd'][$key] ; 
            $postDataAry['fTotalVat_usd'][$i] = $data['fTotalVat_usd'][$key] ; 
            $postDataAry['idForwarderAccountCurrency'][$i] = $data['idForwarderAccountCurrency'][$key] ; 
            $postDataAry['szTransportMode'][$i] = $data['szTransportMode'][$key] ; 
            $postDataAry['idForwarder'][$i] = $data['idForwarder'][$key] ; 
            $postDataAry['szForwarderDispName'][$i] = $data['szForwarderDispName'][$key] ; 
            $postDataAry['iAddingNewQuote'][$i] = $data['iAddingNewQuote'][$key] ; 
            $postDataAry['iProfitType'][$i] = $data['iProfitType'][$key] ; 
            $postDataAry['fInsuranceValueUpto'][$i] = $data['fInsuranceValueUpto'][$key] ; 
            $postDataAry['idInsuranceSellRate'][$i] = $data['idInsuranceSellRate'][$key] ; 
            $postDataAry['idInsuranceBuyRate'][$i] = $data['idInsuranceBuyRate'][$key] ; 
            $postDataAry['idInsuranceSellCurrency'][$i] = $data['idInsuranceSellCurrency'][$key] ; 
            $postDataAry['fInsuranceSellCurrencyExchangeRate'][$i] = $data['fInsuranceSellCurrencyExchangeRate'][$key]; 
            $postDataAry['szTransitTime'][$i] = $data['szTransitTime'][$key]; 
            $postDataAry['idHandlingCurrency'][$i] = $data['idHandlingCurrency'][$key]; 
            $postDataAry['fTotalHandlingFee'][$i] = $data['fTotalHandlingFee'][$key];  
            
            $postDataAry['dtQuoteValidTo'][$i] = $data['dtQuoteValidTo'][$key] ; 
            $postDataAry['dtValidity'][$i] = $data['dtValidity'][$key] ; 
            $postDataAry['fTotalVatForwarder'][$i] = $data['fTotalVatForwarder'][$key] ; 
            $postDataAry['szTotalVatForwarderPrice'][$i] = $data['szTotalVatForwarderPrice'][$key] ; 
            
            $postDataAry['szForwarderComment'][$i] = $data['szForwarderComment'][$key] ; 
            $postDataAry['fQuotePriceCustomerCurrency'][$i] = $data['fQuotePriceCustomerCurrency'][$key] ; 
            $postDataAry['fReferalPercentage'][$i] = $data['fReferalPercentage'][$key] ; 
            $postDataAry['fReferalAmount'][$i] = $data['fReferalAmount'][$key] ; 
            $postDataAry['fTotalPriceCustomerCurrency'][$i] = $data['fTotalPriceCustomerCurrency'][$key] ; 
            $postDataAry['fTotalVat'][$i] = $data['fTotalVat'][$key] ; 
            $postDataAry['fTotalInsuranceCostForBookingCustomerCurrency'][$i] = $data['fTotalInsuranceCostForBookingCustomerCurrency'][$key] ; 
            $postDataAry['iInsuranceAvailabilityFlag'][$i] = $data['iInsuranceAvailabilityFlag'][$key] ; 
            
            $postDataAry['fReferalPercentageHidden'][$i] = $data['fReferalPercentageHidden'][$key] ; 
            $postDataAry['fReferalPercentageStandarad'][$i] = $data['fReferalPercentageStandarad'][$key] ; 
            $postDataAry['fReferalPercentageHidden2'][$i] = $data['fReferalPercentageHidden2'][$key] ; 
            $postDataAry['fReferalAmountHidden'][$i] = $data['fReferalAmountHidden'][$key] ; 
            $postDataAry['fReferalAmountHidden2'][$i] = $data['fReferalAmountHidden2'][$key] ; 
            $postDataAry['fTotalPriceCustomerCurrencyHidden'][$i] = $data['fTotalPriceCustomerCurrencyHidden'][$key] ; 
                                                
            $postDataAry['fTotalPriceCustomerCurrencyHidden2'][$i] = $data['fTotalPriceCustomerCurrencyHidden2'][$key] ; 
            $postDataAry['fTotalVatHidden'][$i] = $data['fTotalVatHidden'][$key] ; 
            $postDataAry['iPickVatFromHidden'][$i] = $data['iPickVatFromHidden'][$key] ;  
            $postDataAry['fTotalPriceForwarderCurrencyHidden'][$i] = $data['fTotalPriceForwarderCurrencyHidden'][$key] ; 
            $postDataAry['fTotalPriceForwarderCurrencyHidden2'][$i] = $data['fTotalPriceForwarderCurrencyHidden2'][$key] ; 
            $postDataAry['szForwarderLogoPath'][$i] = $data['szForwarderLogoPath'][$key] ; 
            $postDataAry['idForwarderCurrency'][$i] = $data['idForwarderCurrency'][$key] ; 
            $postDataAry['fTotalPriceForwarderCurrency'][$i] = $data['fTotalPriceForwarderCurrency'][$key] ; 
            
            $postDataAry['fTotalVatForwarder'][$i] = $data['fTotalVatForwarder'][$key] ; 
            $postDataAry['fTotalPriceForwarderCurrency'][$i] = $data['fTotalPriceForwarderCurrency'][$key] ; 
            $postDataAry['fTotalPriceForwarderCurrency'][$i] = $data['fTotalPriceForwarderCurrency'][$key] ; 
            $postDataAry['fTotalPriceForwarderCurrency'][$i] = $data['fTotalPriceForwarderCurrency'][$key] ; 
            $postDataAry['fTotalPriceForwarderCurrency'][$i] = $data['fTotalPriceForwarderCurrency'][$key] ; 
            $postDataAry['fTotalPriceForwarderCurrency'][$i] = $data['fTotalPriceForwarderCurrency'][$key] ; 
            $postDataAry['fTotalPriceForwarderCurrency'][$i] = $data['fTotalPriceForwarderCurrency'][$key] ; 
                                                
            $postDataAry['idTransportMode'][$i] = $data['idTransportMode'][$key] ; 
            $postDataAry['idBookingQuote'][$i] = $data['idBookingQuote'][$key] ; 
            $postDataAry['szQuotePrice'][$i] = $data['szQuotePrice'][$key] ;  
            $postDataAry['idBookingQuotePricing'][$i] = $data['idBookingQuotePricing'][$key];  
            
            $postDataAry['iManualFeeApplicable'][$i] = $data['iManualFeeApplicable'][$key] ; 
            $postDataAry['idHandlingCurrency'][$i] = $data['idHandlingCurrency'][$key] ; 
            $postDataAry['szHandlingCurrency'][$i] = $data['szHandlingCurrency'][$key] ;  
            $postDataAry['fHandlingCurrencyExchangeRate'][$i] = $data['fHandlingCurrencyExchangeRate'][$key];   
            $postDataAry['fTotalHandlingFee'][$i] = $data['fTotalHandlingFee'][$key] ; 
            $postDataAry['fTotalHandlingFeeCustomerCurrency'][$i] = $data['fTotalHandlingFeeCustomerCurrency'][$key] ;
            $postDataAry['iHandlingFeeApplicable'][$i] = $data['iHandlingFeeApplicable'][$key] ;
            $postDataAry['fPriceMarkUp'][$i] = $data['fPriceMarkUp'][$key] ;
            $postDataAry['iOffLineQuotes'][$i] = $data['iOffLineQuotes'][$key] ;
            $i++;
        }  
    }      
    $postDataAry['szEmailSubject'] = $data['szEmailSubject'];
    $postDataAry['szEmailBody'] = $data['szEmailBody'];
    $postDataAry['szQuotationBody'] = $data['szQuotationBody'];
    $postDataAry['dtReminderDate'] = $data['dtReminderDate'];
    $postDataAry['szReminderTime'] = $data['szReminderTime'];
    $postDataAry['hiddenPosition'] = $data['hiddenPosition'];
    $postDataAry['hiddenPosition1'] = $data['hiddenPosition1'];
    $postDataAry['idBookingFile'] = $data['idBookingFile'];
    $postDataAry['idBooking'] = $data['idBooking'];
    $postDataAry['iSectionEdited'] = $data['iSectionEdited'];
    $postDataAry['idCustomerCurrency'] = $data['idCustomerCurrency'];
    $postDataAry['fCustomerExchangeRateGlobal'] = $data['fCustomerExchangeRateGlobal'];
    $postDataAry['fPriceMarkUpGlobal'] = $data['fPriceMarkUpGlobal'];
    $postDataAry['szOperationType'] = $data['szOperationType']; 
    $postDataAry['iStandardPricing'] = $data['iStandardPricing'];
    return $postDataAry; 
} 
function format_cargo_details_post_array($data)
{
    $postDataAry = array() ;
    $postDataAry = $data ; 
    if(!empty($data['iLength']))
    {
        $postDataAry['iLength'] = array_values($data['iLength']) ;
    } 
    if(!empty($data['iWidth']))
    {
        $postDataAry['iWidth'] = array_values($data['iWidth']) ;
    }
    if(!empty($data['iHeight']))
    {
        $postDataAry['iHeight'] = array_values($data['iHeight']) ;
    }
    if(!empty($data['idCargoMeasure']))
    {
        $postDataAry['idCargoMeasure'] = array_values($data['idCargoMeasure']) ;
    }
    
    if(!empty($data['iWeight']))
    {
        $postDataAry['iWeight'] = array_values($data['iWeight']) ;
    }
    if(!empty($data['idWeightMeasure']))
    {
        $postDataAry['idWeightMeasure'] = array_values($data['idWeightMeasure']) ;
    }
    if(!empty($data['iQuantity']))
    {
        $postDataAry['iQuantity'] = array_values($data['iQuantity']) ;
    }
    if(!empty($data['idCargo']))
    {
        $postDataAry['idCargo'] = array_values($data['idCargo']) ;
    }
    return $postDataAry;
}
function get_time_zone($country,$region) 
{ 
  switch ($country) 
  { 
    case "US":
        switch ($region) 
        {  
            case "AL":
                $timezone = "America/Chicago";
                break;
            case "AK":
                $timezone = "America/Anchorage";
                break;
            case "AZ":
                $timezone = "America/Phoenix";
                break;
            case "AR":
                $timezone = "America/Chicago";
                break;
            case "CA":
                $timezone = "America/Los_Angeles";
                break;
            case "CO":
                $timezone = "America/Denver";
                break;
            case "CT":
                $timezone = "America/New_York";
                break;
            case "DE":
                $timezone = "America/New_York";
                break;
            case "DC":
                $timezone = "America/New_York";
                break;
            case "FL":
                $timezone = "America/New_York";
                break;
            case "GA":
                $timezone = "America/New_York";
                break;
            case "HI":
                $timezone = "Pacific/Honolulu";
                break;
            case "ID":
                $timezone = "America/Denver";
                break;
            case "IL":
                $timezone = "America/Chicago";
                break;
            case "IN":
                $timezone = "America/Indianapolis";
                break;
            case "IA":
                $timezone = "America/Chicago";
                break;
            case "KS":
                $timezone = "America/Chicago";
                break;
            case "KY":
                $timezone = "America/New_York";
                break;
            case "LA":
                $timezone = "America/Chicago";
                break;
            case "ME":
                $timezone = "America/New_York";
                break;
            case "MD":
                $timezone = "America/New_York";
                break;
            case "MA":
                $timezone = "America/New_York";
                break;
            case "MI":
                $timezone = "America/New_York";
                break;
            case "MN":
                $timezone = "America/Chicago";
                break;
            case "MS":
                $timezone = "America/Chicago";
                break;
            case "MO":
                $timezone = "America/Chicago";
                break;
            case "MT":
                $timezone = "America/Denver";
                break;
            case "NE":
                $timezone = "America/Chicago";
                break;
            case "NV":
                $timezone = "America/Los_Angeles";
                break;
            case "NH":
                $timezone = "America/New_York";
                break;
            case "NJ":
                $timezone = "America/New_York";
                break;
            case "NM":
                $timezone = "America/Denver";
                break;
            case "NY":
                $timezone = "America/New_York";
                break;
            case "NC":
                $timezone = "America/New_York";
                break;
            case "ND":
                $timezone = "America/Chicago";
                break;
            case "OH":
                $timezone = "America/New_York";
                break;
            case "OK":
                $timezone = "America/Chicago";
                break;
            case "OR":
                $timezone = "America/Los_Angeles";
                break;
            case "PA":
                $timezone = "America/New_York";
                break;
            case "RI":
                $timezone = "America/New_York";
                break;
            case "SC":
                $timezone = "America/New_York";
                break;
            case "SD":
                $timezone = "America/Chicago";
                break;
            case "TN":
                $timezone = "America/Chicago";
                break;
            case "TX":
                $timezone = "America/Chicago";
                break;
            case "UT":
                $timezone = "America/Denver";
                break;
            case "VT":
                $timezone = "America/New_York";
                break;
            case "VA":
                $timezone = "America/New_York";
                break;
            case "WA":
                $timezone = "America/Los_Angeles";
                break;
            case "WV":
                $timezone = "America/New_York";
                break;
            case "WI":
                $timezone = "America/Chicago";
                break;
            case "WY":
                $timezone = "America/Denver";
            default:
                $timezone = "America/Chicago";
                break;
        } 
    break;
    case "CA":
        switch ($region) 
        { 
            case "AB":
                $timezone = "America/Edmonton";
                break;
            case "BC":
                $timezone = "America/Vancouver";
                break;
            case "MB":
                $timezone = "America/Winnipeg";
                break;
            case "NB":
                $timezone = "America/Halifax";
                break;
            case "NL":
                $timezone = "America/St_Johns";
                break;
            case "NT":
                $timezone = "America/Yellowknife";
                break;
            case "NS":
                $timezone = "America/Halifax";
                break;
            case "NU":
                $timezone = "America/Rankin_Inlet";
                break;
            case "ON":
                $timezone = "America/Rainy_River";
                break;
            case "PE":
                $timezone = "America/Halifax";
                break;
            case "QC":
                $timezone = "America/Montreal";
                break;
            case "SK":
                $timezone = "America/Regina";
                break;
            case "YT":
                $timezone = "America/Whitehorse";
            default:
                $timezone = "America/Vancouver";
                break;
        } 
        break;
        case "AU":
        switch ($region) 
        { 
            case "01":
                $timezone = "Australia/Canberra";
                break;
            case "02":
                $timezone = "Australia/NSW";
                break;
            case "03":
                $timezone = "Australia/North";
                break;
            case "04":
                $timezone = "Australia/Queensland";
                break;
            case "05":
                $timezone = "Australia/South";
                break;
            case "06":
                $timezone = "Australia/Tasmania";
                break;
            case "07":
                $timezone = "Australia/Victoria";
                break;
            case "08":
                $timezone = "Australia/West";
            default:
                $timezone = "Australia/Canberra";
                break;
        } 
        break;
        case "AS":
            $timezone = "US/Samoa";
            break;
        case "CI":
            $timezone = "Africa/Abidjan";
            break;
        case "GH":
            $timezone = "Africa/Accra";
            break;
        case "DZ":
            $timezone = "Africa/Algiers";
            break;
        case "ER":
            $timezone = "Africa/Asmera";
            break;
        case "ML":
            $timezone = "Africa/Bamako";
            break;
        case "CF":
            $timezone = "Africa/Bangui";
            break;
        case "GM":
            $timezone = "Africa/Banjul";
            break;
        case "GW":
            $timezone = "Africa/Bissau";
            break;
        case "CG":
            $timezone = "Africa/Brazzaville";
            break;
        case "BI":
            $timezone = "Africa/Bujumbura";
            break;
        case "EG":
            $timezone = "Africa/Cairo";
            break;
        case "MA":
            $timezone = "Africa/Casablanca";
            break;
        case "GN":
            $timezone = "Africa/Conakry";
            break;
        case "SN":
            $timezone = "Africa/Dakar";
            break;
        case "DJ":
            $timezone = "Africa/Djibouti";
            break;
        case "SL":
            $timezone = "Africa/Freetown";
            break;
        case "BW":
            $timezone = "Africa/Gaborone";
            break;
        case "ZW":
            $timezone = "Africa/Harare";
            break;
        case "ZA":
            $timezone = "Africa/Johannesburg";
            break;
        case "UG":
            $timezone = "Africa/Kampala";
            break;
        case "SD":
            $timezone = "Africa/Khartoum";
            break;
        case "RW":
            $timezone = "Africa/Kigali";
            break;
        case "NG":
            $timezone = "Africa/Lagos";
            break;
        case "GA":
            $timezone = "Africa/Libreville";
            break;
        case "TG":
            $timezone = "Africa/Lome";
            break;
        case "AO":
            $timezone = "Africa/Luanda";
            break;
        case "ZM":
            $timezone = "Africa/Lusaka";
            break;
        case "GQ":
            $timezone = "Africa/Malabo";
            break;
        case "MZ":
            $timezone = "Africa/Maputo";
            break;
        case "LS":
            $timezone = "Africa/Maseru";
            break;
        case "SZ":
            $timezone = "Africa/Mbabane";
            break;
        case "SO":
            $timezone = "Africa/Mogadishu";
            break;
        case "LR":
            $timezone = "Africa/Monrovia";
            break;
        case "KE":
            $timezone = "Africa/Nairobi";
            break;
        case "TD":
            $timezone = "Africa/Ndjamena";
            break;
        case "NE":
            $timezone = "Africa/Niamey";
            break;
        case "MR":
            $timezone = "Africa/Nouakchott";
            break;
        case "BF":
            $timezone = "Africa/Ouagadougou";
            break;
        case "ST":
            $timezone = "Africa/Sao_Tome";
            break;
        case "LY":
            $timezone = "Africa/Tripoli";
            break;
        case "TN":
            $timezone = "Africa/Tunis";
            break;
        case "AI":
            $timezone = "America/Anguilla";
            break;
        case "AG":
            $timezone = "America/Antigua";
            break;
        case "AW":
            $timezone = "America/Aruba";
            break;
        case "BB":
            $timezone = "America/Barbados";
            break;
        case "BZ":
            $timezone = "America/Belize";
            break;
        case "CO":
            $timezone = "America/Bogota";
            break;
        case "VE":
            $timezone = "America/Caracas";
            break;
        case "KY":
            $timezone = "America/Cayman";
            break;
        case "CR":
            $timezone = "America/Costa_Rica";
            break;
        case "DM":
            $timezone = "America/Dominica";
            break;
        case "SV":
            $timezone = "America/El_Salvador";
            break;
        case "GD":
            $timezone = "America/Grenada";
            break;
        case "FR":
            $timezone = "Europe/Paris";
            break;
        case "GP":
            $timezone = "America/Guadeloupe";
            break;
        case "GT":
            $timezone = "America/Guatemala";
            break;
        case "GY":
            $timezone = "America/Guyana";
            break;
        case "CU":
            $timezone = "America/Havana";
            break;
        case "JM":
            $timezone = "America/Jamaica";
            break;
        case "BO":
            $timezone = "America/La_Paz";
            break;
        case "PE":
            $timezone = "America/Lima";
            break;
        case "NI":
            $timezone = "America/Managua";
            break;
        case "MQ":
            $timezone = "America/Martinique";
            break;
        case "UY":
            $timezone = "America/Montevideo";
            break;
        case "MS":
            $timezone = "America/Montserrat";
            break;
        case "BS":
            $timezone = "America/Nassau";
            break;
        case "PA":
            $timezone = "America/Panama";
            break;
        case "SR":
            $timezone = "America/Paramaribo";
            break;
        case "PR":
            $timezone = "America/Puerto_Rico";
            break;
        case "KN":
            $timezone = "America/St_Kitts";
            break;
        case "LC":
            $timezone = "America/St_Lucia";
            break;
        case "VC":
            $timezone = "America/St_Vincent";
            break;
        case "HN":
            $timezone = "America/Tegucigalpa";
            break;
        case "YE":
            $timezone = "Asia/Aden";
            break;
        case "JO":
            $timezone = "Asia/Amman";
            break;
        case "TM":
            $timezone = "Asia/Ashgabat";
            break;
        case "IQ":
            $timezone = "Asia/Baghdad";
            break;
        case "BH":
            $timezone = "Asia/Bahrain";
            break;
        case "AZ":
            $timezone = "Asia/Baku";
            break;
        case "TH":
            $timezone = "Asia/Bangkok";
            break;
        case "LB":
            $timezone = "Asia/Beirut";
            break;
        case "KG":
            $timezone = "Asia/Bishkek";
            break;
        case "BN":
            $timezone = "Asia/Brunei";
            break;
        case "IN":
            $timezone = "Asia/Calcutta";
            break;
        case "MN":
            $timezone = "Asia/Choibalsan";
            break;
        case "LK":
            $timezone = "Asia/Colombo";
            break;
        case "BD":
            $timezone = "Asia/Dhaka";
            break;
        case "AE":
            $timezone = "Asia/Dubai";
            break;
        case "TJ":
            $timezone = "Asia/Dushanbe";
            break;
        case "HK":
            $timezone = "Asia/Hong_Kong";
            break;
        case "TR":
            $timezone = "Asia/Istanbul";
            break;
        case "IL":
            $timezone = "Asia/Jerusalem";
            break;
        case "AF":
            $timezone = "Asia/Kabul";
            break;
        case "PK":
            $timezone = "Asia/Karachi";
            break;
        case "NP":
            $timezone = "Asia/Katmandu";
            break;
        case "KW":
            $timezone = "Asia/Kuwait";
            break;
        case "MO":
            $timezone = "Asia/Macao";
            break;
        case "PH":
            $timezone = "Asia/Manila";
            break;
        case "OM":
            $timezone = "Asia/Muscat";
            break;
        case "CY":
            $timezone = "Asia/Nicosia";
            break;
        case "KP":
            $timezone = "Asia/Pyongyang";
            break;
        case "QA":
            $timezone = "Asia/Qatar";
            break;
        case "MM":
            $timezone = "Asia/Rangoon";
            break;
        case "SA":
            $timezone = "Asia/Riyadh";
            break;
        case "KR":
            $timezone = "Asia/Seoul";
            break;
        case "SG":
            $timezone = "Asia/Singapore";
            break;
        case "TW":
            $timezone = "Asia/Taipei";
            break;
        case "GE":
            $timezone = "Asia/Tbilisi";
            break;
        case "BT":
            $timezone = "Asia/Thimphu";
            break;
        case "JP":
            $timezone = "Asia/Tokyo";
            break;
        case "LA":
            $timezone = "Asia/Vientiane";
            break;
        case "AM":
            $timezone = "Asia/Yerevan";
            break;
        case "BM":
            $timezone = "Atlantic/Bermuda";
            break;
        case "CV":
            $timezone = "Atlantic/Cape_Verde";
            break;
        case "FO":
            $timezone = "Atlantic/Faeroe";
            break;
        case "IS":
            $timezone = "Atlantic/Reykjavik";
            break;
        case "GS":
            $timezone = "Atlantic/South_Georgia";
            break;
        case "SH":
            $timezone = "Atlantic/St_Helena";
            break;
        case "CL":
            $timezone = "Chile/Continental";
            break;
        case "NL":
            $timezone = "Europe/Amsterdam";
            break;
        case "AD":
            $timezone = "Europe/Andorra";
            break;
        case "GR":
            $timezone = "Europe/Athens";
            break;
        case "YU":
            $timezone = "Europe/Belgrade";
            break;
        case "DE":
            $timezone = "Europe/Berlin";
            break;
        case "SK":
            $timezone = "Europe/Bratislava";
            break;
        case "BE":
            $timezone = "Europe/Brussels";
            break;
        case "RO":
            $timezone = "Europe/Bucharest";
            break;
        case "HU":
            $timezone = "Europe/Budapest";
            break;
        case "DK":
            $timezone = "Europe/Copenhagen";
            break;
        case "IE":
            $timezone = "Europe/Dublin";
            break;
        case "GI":
            $timezone = "Europe/Gibraltar";
            break;
        case "FI":
            $timezone = "Europe/Helsinki";
            break;
        case "SI":
            $timezone = "Europe/Ljubljana";
            break;
        case "GB":
            $timezone = "Europe/London";
            break;
        case "LU":
            $timezone = "Europe/Luxembourg";
            break;
        case "MT":
            $timezone = "Europe/Malta";
            break;
        case "BY":
            $timezone = "Europe/Minsk";
            break;
        case "MC":
            $timezone = "Europe/Monaco";
            break;
        case "NO":
            $timezone = "Europe/Oslo";
            break;
        case "CZ":
            $timezone = "Europe/Prague";
            break;
        case "LV":
            $timezone = "Europe/Riga";
            break;
        case "IT":
            $timezone = "Europe/Rome";
            break;
        case "SM":
            $timezone = "Europe/San_Marino";
            break;
        case "BA":
            $timezone = "Europe/Sarajevo";
            break;
        case "MK":
            $timezone = "Europe/Skopje";
            break;
        case "BG":
            $timezone = "Europe/Sofia";
            break;
        case "SE":
            $timezone = "Europe/Stockholm";
            break;
        case "EE":
            $timezone = "Europe/Tallinn";
            break;
        case "AL":
            $timezone = "Europe/Tirane";
            break;
        case "LI":
            $timezone = "Europe/Vaduz";
            break;
        case "VA":
            $timezone = "Europe/Vatican";
            break;
        case "AT":
            $timezone = "Europe/Vienna";
            break;
        case "LT":
            $timezone = "Europe/Vilnius";
            break;
        case "PL":
            $timezone = "Europe/Warsaw";
            break;
        case "HR":
            $timezone = "Europe/Zagreb";
            break;
        case "IR":
            $timezone = "Asia/Tehran";
            break;
        case "MG":
            $timezone = "Indian/Antananarivo";
            break;
        case "CX":
            $timezone = "Indian/Christmas";
            break;
        case "CC":
            $timezone = "Indian/Cocos";
            break;
        case "KM":
            $timezone = "Indian/Comoro";
            break;
        case "MV":
            $timezone = "Indian/Maldives";
            break;
        case "MU":
            $timezone = "Indian/Mauritius";
            break;
        case "YT":
            $timezone = "Indian/Mayotte";
            break;
        case "RE":
            $timezone = "Indian/Reunion";
            break;
        case "FJ":
            $timezone = "Pacific/Fiji";
            break;
        case "TV":
            $timezone = "Pacific/Funafuti";
            break;
        case "GU":
            $timezone = "Pacific/Guam";
            break;
        case "NR":
            $timezone = "Pacific/Nauru";
            break;
        case "NU":
            $timezone = "Pacific/Niue";
            break;
        case "NF":
            $timezone = "Pacific/Norfolk";
            break;
        case "PW":
            $timezone = "Pacific/Palau";
            break;
        case "PN":
            $timezone = "Pacific/Pitcairn";
            break;
        case "CK":
            $timezone = "Pacific/Rarotonga";
            break;
        case "WS":
            $timezone = "Pacific/Samoa";
            break;
        case "KI":
            $timezone = "Pacific/Tarawa";
            break;
        case "TO":
            $timezone = "Pacific/Tongatapu";
            break;
        case "WF":
            $timezone = "Pacific/Wallis";
            break;
        case "TZ":
            $timezone = "Africa/Dar_es_Salaam";
            break;
        case "VN":
            $timezone = "Asia/Phnom_Penh";
            break;
        case "KH":
            $timezone = "Asia/Phnom_Penh";
            break;
        case "CM":
            $timezone = "Africa/Lagos";
            break;
        case "DO":
            $timezone = "America/Santo_Domingo";
            break;
        case "ET":
            $timezone = "Africa/Addis_Ababa";
            break;
        case "FX":
            $timezone = "Europe/Paris";
            break;
        case "HT":
            $timezone = "America/Port-au-Prince";
            break;
        case "CH":
            $timezone = "Europe/Zurich";
            break;
        case "AN":
            $timezone = "America/Curacao";
            break;
        case "BJ":
            $timezone = "Africa/Porto-Novo";
            break;
        case "EH":
            $timezone = "Africa/El_Aaiun";
            break;
        case "FK":
            $timezone = "Atlantic/Stanley";
            break;
        case "GF":
            $timezone = "America/Cayenne";
            break;
        case "IO":
            $timezone = "Indian/Chagos";
            break;
        case "MD":
            $timezone = "Europe/Chisinau";
            break;
        case "MP":
            $timezone = "Pacific/Saipan";
            break;
        case "MW":
            $timezone = "Africa/Blantyre";
            break;
        case "NA":
            $timezone = "Africa/Windhoek";
            break;
        case "NC":
            $timezone = "Pacific/Noumea";
            break;
        case "PG":
            $timezone = "Pacific/Port_Moresby";
            break;
        case "PM":
            $timezone = "America/Miquelon";
            break;
        case "PS":
            $timezone = "Asia/Gaza";
            break;
        case "PY":
            $timezone = "America/Asuncion";
            break;
        case "SB":
            $timezone = "Pacific/Guadalcanal";
            break;
        case "SC":
            $timezone = "Indian/Mahe";
            break;
        case "SJ":
            $timezone = "Arctic/Longyearbyen";
            break;
        case "SY":
            $timezone = "Asia/Damascus";
            break;
        case "TC":
            $timezone = "America/Grand_Turk";
            break;
        case "TF":
            $timezone = "Indian/Kerguelen";
            break;
        case "TK":
            $timezone = "Pacific/Fakaofo";
            break;
        case "TT":
            $timezone = "America/Port_of_Spain";
            break;
        case "VG":
            $timezone = "America/Tortola";
            break;
        case "VI":
            $timezone = "America/St_Thomas";
            break;
        case "VU":
            $timezone = "Pacific/Efate";
            break;
        case "RS":
            $timezone = "Europe/Belgrade";
            break;
        case "ME":
            $timezone = "Europe/Podgorica";
            break;
        case "AX":
            $timezone = "Europe/Mariehamn";
            break;
        case "GG":
            $timezone = "Europe/Guernsey";
            break;
        case "IM":
            $timezone = "Europe/Isle_of_Man";
            break;
        case "JE":
            $timezone = "Europe/Jersey";
            break;
        case "BL":
            $timezone = "America/St_Barthelemy";
            break;
        case "MF":
            $timezone = "America/Marigot";
            break;
        case "AR":
            switch ($region) 
            { 
                case "01":
                    $timezone = "America/Argentina/Buenos_Aires";
                    break;
                case "02":
                    $timezone = "America/Argentina/Catamarca";
                    break;
                case "03":
                    $timezone = "America/Argentina/Tucuman";
                    break;
                case "04":
                    $timezone = "America/Argentina/Rio_Gallegos";
                    break;
                case "05":
                    $timezone = "America/Argentina/Cordoba";
                    break;
                case "06":
                    $timezone = "America/Argentina/Tucuman";
                    break;
                case "07":
                    $timezone = "America/Argentina/Buenos_Aires";
                    break;
                case "08":
                    $timezone = "America/Argentina/Buenos_Aires";
                    break;
                case "09":
                    $timezone = "America/Argentina/Tucuman";
                    break;
                case "10":
                    $timezone = "America/Argentina/Jujuy";
                    break;
                case "11":
                    $timezone = "America/Argentina/San_Luis";
                    break;
                case "12":
                    $timezone = "America/Argentina/La_Rioja";
                    break;
                case "13":
                    $timezone = "America/Argentina/Mendoza";
                    break;
                case "14":
                    $timezone = "America/Argentina/Buenos_Aires";
                    break;
                case "15":
                    $timezone = "America/Argentina/San_Luis";
                    break;
                case "16":
                    $timezone = "America/Argentina/Buenos_Aires";
                    break;
                case "17":
                    $timezone = "America/Argentina/Salta";
                    break;
                case "18":
                    $timezone = "America/Argentina/San_Juan";
                    break;
                case "19":
                    $timezone = "America/Argentina/San_Luis";
                    break;
                case "20":
                    $timezone = "America/Argentina/Rio_Gallegos";
                    break;
                case "21":
                    $timezone = "America/Argentina/Buenos_Aires";
                    break;
                case "22":
                    $timezone = "America/Argentina/Catamarca";
                    break;
                case "23":
                    $timezone = "America/Argentina/Ushuaia";
                    break;
                case "24":
                    $timezone = "America/Argentina/Tucuman";
                    break;
            } 
        break;
        case "BR":
            switch ($region) 
            { 
                case "01":
                    $timezone = "America/Rio_Branco";
                    break;
                case "02":
                    $timezone = "America/Maceio";
                    break;
                case "03":
                    $timezone = "America/Sao_Paulo";
                    break;
                case "04":
                    $timezone = "America/Manaus";
                    break;
                case "05":
                    $timezone = "America/Bahia";
                    break;
                case "06":
                    $timezone = "America/Fortaleza";
                    break;
                case "07":
                    $timezone = "America/Sao_Paulo";
                    break;
                case "08":
                    $timezone = "America/Sao_Paulo";
                    break;
                case "11":
                    $timezone = "America/Campo_Grande";
                    break;
                case "13":
                    $timezone = "America/Belem";
                    break;
                case "14":
                    $timezone = "America/Cuiaba";
                    break;
                case "15":
                    $timezone = "America/Sao_Paulo";
                    break;
                case "16":
                    $timezone = "America/Belem";
                    break;
                case "17":
                    $timezone = "America/Recife";
                    break;
                case "18":
                    $timezone = "America/Sao_Paulo";
                    break;
                case "20":
                    $timezone = "America/Fortaleza";
                    break;
                case "21":
                    $timezone = "America/Sao_Paulo";
                    break;
                case "22":
                    $timezone = "America/Recife";
                    break;
                case "23":
                    $timezone = "America/Sao_Paulo";
                    break;
                case "24":
                    $timezone = "America/Porto_Velho";
                    break;
                case "25":
                    $timezone = "America/Boa_Vista";
                    break;
                case "26":
                    $timezone = "America/Sao_Paulo";
                    break;
                case "27":
                    $timezone = "America/Sao_Paulo";
                    break;
                case "28":
                    $timezone = "America/Maceio";
                    break;
                case "29":
                    $timezone = "America/Sao_Paulo";
                    break;
                case "30":
                    $timezone = "America/Recife";
                    break;
                case "31":
                    $timezone = "America/Araguaina";
                    break;
            } 
        break;
    case "CD":
        switch ($region) 
        { 
            case "02":
                $timezone = "Africa/Kinshasa";
                break;
            case "05":
                $timezone = "Africa/Lubumbashi";
                break;
            case "06":
                $timezone = "Africa/Kinshasa";
                break;
            case "08":
                $timezone = "Africa/Kinshasa";
                break;
            case "10":
                $timezone = "Africa/Lubumbashi";
                break;
            case "11":
                $timezone = "Africa/Lubumbashi";
                break;
            case "12":
                $timezone = "Africa/Lubumbashi";
                break;
        } 
    break;
    case "CN":
        switch ($region) 
        { 
            case "01":
                $timezone = "Asia/Shanghai";
                break;
            case "02":
                $timezone = "Asia/Shanghai";
                break;
            case "03":
                $timezone = "Asia/Shanghai";
                break;
            case "04":
                $timezone = "Asia/Shanghai";
                break;
            case "05":
                $timezone = "Asia/Harbin";
                break;
            case "06":
                $timezone = "Asia/Chongqing";
                break;
            case "07":
                $timezone = "Asia/Shanghai";
                break;
            case "08":
                $timezone = "Asia/Harbin";
                break;
            case "09":
                $timezone = "Asia/Shanghai";
                break;
            case "10":
                $timezone = "Asia/Shanghai";
                break;
            case "11":
                $timezone = "Asia/Chongqing";
                break;
            case "12":
                $timezone = "Asia/Shanghai";
                break;
            case "13":
                $timezone = "Asia/Urumqi";
                break;
            case "14":
                $timezone = "Asia/Chongqing";
                break;
            case "15":
                $timezone = "Asia/Chongqing";
                break;
            case "16":
                $timezone = "Asia/Chongqing";
                break;
            case "18":
                $timezone = "Asia/Chongqing";
                break;
            case "19":
                $timezone = "Asia/Harbin";
                break;
            case "20":
                $timezone = "Asia/Harbin";
                break;
            case "21":
                $timezone = "Asia/Chongqing";
                break;
            case "22":
                $timezone = "Asia/Harbin";
                break;
            case "23":
                $timezone = "Asia/Shanghai";
                break;
            case "24":
                $timezone = "Asia/Chongqing";
                break;
            case "25":
                $timezone = "Asia/Shanghai";
                break;
            case "26":
                $timezone = "Asia/Chongqing";
                break;
            case "28":
                $timezone = "Asia/Shanghai";
                break;
            case "29":
                $timezone = "Asia/Chongqing";
                break;
            case "30":
                $timezone = "Asia/Chongqing";
                break;
            case "31":
                $timezone = "Asia/Chongqing";
                break;
            case "32":
                $timezone = "Asia/Chongqing";
                break;
            case "33":
                $timezone = "Asia/Chongqing";
                break;
        } 
    break;
    case "EC":
    switch ($region) 
    { 
        case "01":
            $timezone = "Pacific/Galapagos";
            break;
        case "02":
            $timezone = "America/Guayaquil";
            break;
        case "03":
            $timezone = "America/Guayaquil";
            break;
        case "04":
            $timezone = "America/Guayaquil";
            break;
        case "05":
            $timezone = "America/Guayaquil";
            break;
        case "06":
            $timezone = "America/Guayaquil";
            break;
        case "07":
            $timezone = "America/Guayaquil";
            break;
        case "08":
            $timezone = "America/Guayaquil";
            break;
        case "09":
            $timezone = "America/Guayaquil";
            break;
        case "10":
            $timezone = "America/Guayaquil";
            break;
        case "11":
            $timezone = "America/Guayaquil";
            break;
        case "12":
            $timezone = "America/Guayaquil";
            break;
        case "13":
            $timezone = "America/Guayaquil";
            break;
        case "14":
            $timezone = "America/Guayaquil";
            break;
        case "15":
            $timezone = "America/Guayaquil";
            break;
        case "17":
            $timezone = "America/Guayaquil";
            break;
        case "18":
            $timezone = "America/Guayaquil";
            break;
        case "19":
            $timezone = "America/Guayaquil";
            break;
        case "20":
            $timezone = "America/Guayaquil";
            break;
        case "22":
            $timezone = "America/Guayaquil";
            break;
    } 
    break;
case "ES":
    switch ($region) { 
  case "07":
      $timezone = "Europe/Madrid";
      break;
  case "27":
      $timezone = "Europe/Madrid";
      break;
  case "29":
      $timezone = "Europe/Madrid";
      break;
  case "31":
      $timezone = "Europe/Madrid";
      break;
  case "32":
      $timezone = "Europe/Madrid";
      break;
  case "34":
      $timezone = "Europe/Madrid";
      break;
  case "39":
      $timezone = "Europe/Madrid";
      break;
  case "51":
      $timezone = "Africa/Ceuta";
      break;
  case "52":
      $timezone = "Europe/Madrid";
      break;
  case "53":
      $timezone = "Atlantic/Canary";
      break;
  case "54":
      $timezone = "Europe/Madrid";
      break;
  case "55":
      $timezone = "Europe/Madrid";
      break;
  case "56":
      $timezone = "Europe/Madrid";
      break;
  case "57":
      $timezone = "Europe/Madrid";
      break;
  case "58":
      $timezone = "Europe/Madrid";
      break;
  case "59":
      $timezone = "Europe/Madrid";
      break;
  case "60":
      $timezone = "Europe/Madrid";
      break;
  } 
  break;
case "GL":
    switch ($region) { 
  case "01":
      $timezone = "America/Thule";
      break;
  case "02":
      $timezone = "America/Godthab";
      break;
  case "03":
      $timezone = "America/Godthab";
      break;
  } 
  break;
case "ID":
    switch ($region) { 
  case "01":
      $timezone = "Asia/Pontianak";
      break;
  case "02":
      $timezone = "Asia/Makassar";
      break;
  case "03":
      $timezone = "Asia/Jakarta";
      break;
  case "04":
      $timezone = "Asia/Jakarta";
      break;
  case "05":
      $timezone = "Asia/Jakarta";
      break;
  case "06":
      $timezone = "Asia/Jakarta";
      break;
  case "07":
      $timezone = "Asia/Jakarta";
      break;
  case "08":
      $timezone = "Asia/Jakarta";
      break;
  case "09":
      $timezone = "Asia/Jayapura";
      break;
  case "10":
      $timezone = "Asia/Jakarta";
      break;
  case "11":
      $timezone = "Asia/Pontianak";
      break;
  case "12":
      $timezone = "Asia/Makassar";
      break;
  case "13":
      $timezone = "Asia/Makassar";
      break;
  case "14":
      $timezone = "Asia/Makassar";
      break;
  case "15":
      $timezone = "Asia/Jakarta";
      break;
  case "16":
      $timezone = "Asia/Makassar";
      break;
  case "17":
      $timezone = "Asia/Makassar";
      break;
  case "18":
      $timezone = "Asia/Makassar";
      break;
  case "19":
      $timezone = "Asia/Pontianak";
      break;
  case "20":
      $timezone = "Asia/Makassar";
      break;
  case "21":
      $timezone = "Asia/Makassar";
      break;
  case "22":
      $timezone = "Asia/Makassar";
      break;
  case "23":
      $timezone = "Asia/Makassar";
      break;
  case "24":
      $timezone = "Asia/Jakarta";
      break;
  case "25":
      $timezone = "Asia/Pontianak";
      break;
  case "26":
      $timezone = "Asia/Pontianak";
      break;
  case "30":
      $timezone = "Asia/Jakarta";
      break;
  case "31":
      $timezone = "Asia/Makassar";
      break;
  case "33":
      $timezone = "Asia/Jakarta";
      break;
  } 
  break;
case "KZ":
    switch ($region) { 
  case "01":
      $timezone = "Asia/Almaty";
      break;
  case "02":
      $timezone = "Asia/Almaty";
      break;
  case "03":
      $timezone = "Asia/Qyzylorda";
      break;
  case "04":
      $timezone = "Asia/Aqtobe";
      break;
  case "05":
      $timezone = "Asia/Qyzylorda";
      break;
  case "06":
      $timezone = "Asia/Aqtau";
      break;
  case "07":
      $timezone = "Asia/Oral";
      break;
  case "08":
      $timezone = "Asia/Qyzylorda";
      break;
  case "09":
      $timezone = "Asia/Aqtau";
      break;
  case "10":
      $timezone = "Asia/Qyzylorda";
      break;
  case "11":
      $timezone = "Asia/Almaty";
      break;
  case "12":
      $timezone = "Asia/Qyzylorda";
      break;
  case "13":
      $timezone = "Asia/Aqtobe";
      break;
  case "14":
      $timezone = "Asia/Qyzylorda";
      break;
  case "15":
      $timezone = "Asia/Almaty";
      break;
  case "16":
      $timezone = "Asia/Aqtobe";
      break;
  case "17":
      $timezone = "Asia/Almaty";
      break;
  } 
  break;
case "MX":
    switch ($region) { 
  case "01":
      $timezone = "America/Mexico_City";
      break;
  case "02":
      $timezone = "America/Tijuana";
      break;
  case "03":
      $timezone = "America/Hermosillo";
      break;
  case "04":
      $timezone = "America/Merida";
      break;
  case "05":
      $timezone = "America/Mexico_City";
      break;
  case "06":
      $timezone = "America/Chihuahua";
      break;
  case "07":
      $timezone = "America/Monterrey";
      break;
  case "08":
      $timezone = "America/Mexico_City";
      break;
  case "09":
      $timezone = "America/Mexico_City";
      break;
  case "10":
      $timezone = "America/Mazatlan";
      break;
  case "11":
      $timezone = "America/Mexico_City";
      break;
  case "12":
      $timezone = "America/Mexico_City";
      break;
  case "13":
      $timezone = "America/Mexico_City";
      break;
  case "14":
      $timezone = "America/Mazatlan";
      break;
  case "15":
      $timezone = "America/Chihuahua";
      break;
  case "16":
      $timezone = "America/Mexico_City";
      break;
  case "17":
      $timezone = "America/Mexico_City";
      break;
  case "18":
      $timezone = "America/Mazatlan";
      break;
  case "19":
      $timezone = "America/Monterrey";
      break;
  case "20":
      $timezone = "America/Mexico_City";
      break;
  case "21":
      $timezone = "America/Mexico_City";
      break;
  case "22":
      $timezone = "America/Mexico_City";
      break;
  case "23":
      $timezone = "America/Cancun";
      break;
  case "24":
      $timezone = "America/Mexico_City";
      break;
  case "25":
      $timezone = "America/Mazatlan";
      break;
  case "26":
      $timezone = "America/Hermosillo";
      break;
  case "27":
      $timezone = "America/Merida";
      break;
  case "28":
      $timezone = "America/Monterrey";
      break;
  case "29":
      $timezone = "America/Mexico_City";
      break;
  case "30":
      $timezone = "America/Mexico_City";
      break;
  case "31":
      $timezone = "America/Merida";
      break;
  case "32":
      $timezone = "America/Monterrey";
      break;
  } 
  break;
case "MY":
    switch ($region) { 
  case "01":
      $timezone = "Asia/Kuala_Lumpur";
      break;
  case "02":
      $timezone = "Asia/Kuala_Lumpur";
      break;
  case "03":
      $timezone = "Asia/Kuala_Lumpur";
      break;
  case "04":
      $timezone = "Asia/Kuala_Lumpur";
      break;
  case "05":
      $timezone = "Asia/Kuala_Lumpur";
      break;
  case "06":
      $timezone = "Asia/Kuala_Lumpur";
      break;
  case "07":
      $timezone = "Asia/Kuala_Lumpur";
      break;
  case "08":
      $timezone = "Asia/Kuala_Lumpur";
      break;
  case "09":
      $timezone = "Asia/Kuala_Lumpur";
      break;
  case "11":
      $timezone = "Asia/Kuching";
      break;
  case "12":
      $timezone = "Asia/Kuala_Lumpur";
      break;
  case "13":
      $timezone = "Asia/Kuala_Lumpur";
      break;
  case "14":
      $timezone = "Asia/Kuala_Lumpur";
      break;
  case "15":
      $timezone = "Asia/Kuching";
      break;
  case "16":
      $timezone = "Asia/Kuching";
      break;
  } 
  break;
case "NZ":
    switch ($region) { 
  case "85":
      $timezone = "Pacific/Auckland";
      break;
  case "E7":
      $timezone = "Pacific/Auckland";
      break;
  case "E8":
      $timezone = "Pacific/Auckland";
      break;
  case "E9":
      $timezone = "Pacific/Auckland";
      break;
  case "F1":
      $timezone = "Pacific/Auckland";
      break;
  case "F2":
      $timezone = "Pacific/Auckland";
      break;
  case "F3":
      $timezone = "Pacific/Auckland";
      break;
  case "F4":
      $timezone = "Pacific/Auckland";
      break;
  case "F5":
      $timezone = "Pacific/Auckland";
      break;
  case "F7":
      $timezone = "Pacific/Chatham";
      break;
  case "F8":
      $timezone = "Pacific/Auckland";
      break;
  case "F9":
      $timezone = "Pacific/Auckland";
      break;
  case "G1":
      $timezone = "Pacific/Auckland";
      break;
  case "G2":
      $timezone = "Pacific/Auckland";
      break;
  case "G3":
      $timezone = "Pacific/Auckland";
      break;
  } 
  break;
case "PT":
    switch ($region) { 
  case "02":
      $timezone = "Europe/Lisbon";
      break;
  case "03":
      $timezone = "Europe/Lisbon";
      break;
  case "04":
      $timezone = "Europe/Lisbon";
      break;
  case "05":
      $timezone = "Europe/Lisbon";
      break;
  case "06":
      $timezone = "Europe/Lisbon";
      break;
  case "07":
      $timezone = "Europe/Lisbon";
      break;
  case "08":
      $timezone = "Europe/Lisbon";
      break;
  case "09":
      $timezone = "Europe/Lisbon";
      break;
  case "10":
      $timezone = "Atlantic/Madeira";
      break;
  case "11":
      $timezone = "Europe/Lisbon";
      break;
  case "13":
      $timezone = "Europe/Lisbon";
      break;
  case "14":
      $timezone = "Europe/Lisbon";
      break;
  case "16":
      $timezone = "Europe/Lisbon";
      break;
  case "17":
      $timezone = "Europe/Lisbon";
      break;
  case "18":
      $timezone = "Europe/Lisbon";
      break;
  case "19":
      $timezone = "Europe/Lisbon";
      break;
  case "20":
      $timezone = "Europe/Lisbon";
      break;
  case "21":
      $timezone = "Europe/Lisbon";
      break;
  case "22":
      $timezone = "Europe/Lisbon";
      break;
  } 
  break;
case "RU":
    switch ($region) { 
  case "01":
      $timezone = "Europe/Volgograd";
      break;
  case "02":
      $timezone = "Asia/Irkutsk";
      break;
  case "03":
      $timezone = "Asia/Novokuznetsk";
      break;
  case "04":
      $timezone = "Asia/Novosibirsk";
      break;
  case "05":
      $timezone = "Asia/Vladivostok";
      break;
  case "06":
      $timezone = "Europe/Moscow";
      break;
  case "07":
      $timezone = "Europe/Volgograd";
      break;
  case "08":
      $timezone = "Europe/Samara";
      break;
  case "09":
      $timezone = "Europe/Moscow";
      break;
  case "10":
      $timezone = "Europe/Moscow";
      break;
  case "11":
      $timezone = "Asia/Irkutsk";
      break;
  case "13":
      $timezone = "Asia/Yekaterinburg";
      break;
  case "14":
      $timezone = "Asia/Irkutsk";
      break;
  case "15":
      $timezone = "Asia/Anadyr";
      break;
  case "16":
      $timezone = "Europe/Samara";
      break;
  case "17":
      $timezone = "Europe/Volgograd";
      break;
  case "18":
      $timezone = "Asia/Krasnoyarsk";
      break;
  case "20":
      $timezone = "Asia/Irkutsk";
      break;
  case "21":
      $timezone = "Europe/Moscow";
      break;
  case "22":
      $timezone = "Europe/Volgograd";
      break;
  case "23":
      $timezone = "Europe/Kaliningrad";
      break;
  case "24":
      $timezone = "Europe/Volgograd";
      break;
  case "25":
      $timezone = "Europe/Moscow";
      break;
  case "26":
      $timezone = "Asia/Kamchatka";
      break;
  case "27":
      $timezone = "Europe/Volgograd";
      break;
  case "28":
      $timezone = "Europe/Moscow";
      break;
  case "29":
      $timezone = "Asia/Novokuznetsk";
      break;
  case "30":
      $timezone = "Asia/Vladivostok";
      break;
  case "31":
      $timezone = "Asia/Krasnoyarsk";
      break;
  case "32":
      $timezone = "Asia/Omsk";
      break;
  case "33":
      $timezone = "Asia/Yekaterinburg";
      break;
  case "34":
      $timezone = "Asia/Yekaterinburg";
      break;
  case "35":
      $timezone = "Asia/Yekaterinburg";
      break;
  case "36":
      $timezone = "Asia/Anadyr";
      break;
  case "37":
      $timezone = "Europe/Moscow";
      break;
  case "38":
      $timezone = "Europe/Volgograd";
      break;
  case "39":
      $timezone = "Asia/Krasnoyarsk";
      break;
  case "40":
      $timezone = "Asia/Yekaterinburg";
      break;
  case "41":
      $timezone = "Europe/Moscow";
      break;
  case "42":
      $timezone = "Europe/Moscow";
      break;
  case "43":
      $timezone = "Europe/Moscow";
      break;
  case "44":
      $timezone = "Asia/Magadan";
      break;
  case "45":
      $timezone = "Europe/Samara";
      break;
  case "46":
      $timezone = "Europe/Samara";
      break;
  case "47":
      $timezone = "Europe/Moscow";
      break;
  case "48":
      $timezone = "Europe/Moscow";
      break;
  case "49":
      $timezone = "Europe/Moscow";
      break;
  case "50":
      $timezone = "Asia/Yekaterinburg";
      break;
  case "51":
      $timezone = "Europe/Moscow";
      break;
  case "52":
      $timezone = "Europe/Moscow";
      break;
  case "53":
      $timezone = "Asia/Novosibirsk";
      break;
  case "54":
      $timezone = "Asia/Omsk";
      break;
  case "55":
      $timezone = "Europe/Samara";
      break;
  case "56":
      $timezone = "Europe/Moscow";
      break;
  case "57":
      $timezone = "Europe/Samara";
      break;
  case "58":
      $timezone = "Asia/Yekaterinburg";
      break;
  case "59":
      $timezone = "Asia/Vladivostok";
      break;
  case "60":
      $timezone = "Europe/Kaliningrad";
      break;
  case "61":
      $timezone = "Europe/Volgograd";
      break;
  case "62":
      $timezone = "Europe/Moscow";
      break;
  case "63":
      $timezone = "Asia/Yakutsk";
      break;
  case "64":
      $timezone = "Asia/Sakhalin";
      break;
  case "65":
      $timezone = "Europe/Samara";
      break;
  case "66":
      $timezone = "Europe/Moscow";
      break;
  case "67":
      $timezone = "Europe/Samara";
      break;
  case "68":
      $timezone = "Europe/Volgograd";
      break;
  case "69":
      $timezone = "Europe/Moscow";
      break;
  case "70":
      $timezone = "Europe/Volgograd";
      break;
  case "71":
      $timezone = "Asia/Yekaterinburg";
      break;
  case "72":
      $timezone = "Europe/Moscow";
      break;
  case "73":
      $timezone = "Europe/Samara";
      break;
  case "74":
      $timezone = "Asia/Krasnoyarsk";
      break;
  case "75":
      $timezone = "Asia/Novosibirsk";
      break;
  case "76":
      $timezone = "Europe/Moscow";
      break;
  case "77":
      $timezone = "Europe/Moscow";
      break;
  case "78":
      $timezone = "Asia/Yekaterinburg";
      break;
  case "79":
      $timezone = "Asia/Irkutsk";
      break;
  case "80":
      $timezone = "Asia/Yekaterinburg";
      break;
  case "81":
      $timezone = "Europe/Samara";
      break;
  case "82":
      $timezone = "Asia/Irkutsk";
      break;
  case "83":
      $timezone = "Europe/Moscow";
      break;
  case "84":
      $timezone = "Europe/Volgograd";
      break;
  case "85":
      $timezone = "Europe/Moscow";
      break;
  case "86":
      $timezone = "Europe/Moscow";
      break;
  case "87":
      $timezone = "Asia/Novosibirsk";
      break;
  case "88":
      $timezone = "Europe/Moscow";
      break;
  case "89":
      $timezone = "Asia/Vladivostok";
      break;
  } 
  break;
case "UA":
    switch ($region) { 
  case "01":
      $timezone = "Europe/Kiev";
      break;
  case "02":
      $timezone = "Europe/Kiev";
      break;
  case "03":
      $timezone = "Europe/Uzhgorod";
      break;
  case "04":
      $timezone = "Europe/Zaporozhye";
      break;
  case "05":
      $timezone = "Europe/Zaporozhye";
      break;
  case "06":
      $timezone = "Europe/Uzhgorod";
      break;
  case "07":
      $timezone = "Europe/Zaporozhye";
      break;
  case "08":
      $timezone = "Europe/Simferopol";
      break;
  case "09":
      $timezone = "Europe/Kiev";
      break;
  case "10":
      $timezone = "Europe/Zaporozhye";
      break;
  case "11":
      $timezone = "Europe/Simferopol";
      break;
  case "13":
      $timezone = "Europe/Kiev";
      break;
  case "14":
      $timezone = "Europe/Zaporozhye";
      break;
  case "15":
      $timezone = "Europe/Uzhgorod";
      break;
  case "16":
      $timezone = "Europe/Zaporozhye";
      break;
  case "17":
      $timezone = "Europe/Simferopol";
      break;
  case "18":
      $timezone = "Europe/Zaporozhye";
      break;
  case "19":
      $timezone = "Europe/Kiev";
      break;
  case "20":
      $timezone = "Europe/Simferopol";
      break;
  case "21":
      $timezone = "Europe/Kiev";
      break;
  case "22":
      $timezone = "Europe/Uzhgorod";
      break;
  case "23":
      $timezone = "Europe/Kiev";
      break;
  case "24":
      $timezone = "Europe/Uzhgorod";
      break;
  case "25":
      $timezone = "Europe/Uzhgorod";
      break;
  case "26":
      $timezone = "Europe/Zaporozhye";
      break;
  case "27":
      $timezone = "Europe/Kiev";
      break;
  } 
  break;
case "UZ":
    switch ($region) { 
  case "01":
      $timezone = "Asia/Tashkent";
      break;
  case "02":
      $timezone = "Asia/Samarkand";
      break;
  case "03":
      $timezone = "Asia/Tashkent";
      break;
  case "06":
      $timezone = "Asia/Tashkent";
      break;
  case "07":
      $timezone = "Asia/Samarkand";
      break;
  case "08":
      $timezone = "Asia/Samarkand";
      break;
  case "09":
      $timezone = "Asia/Samarkand";
      break;
  case "10":
      $timezone = "Asia/Samarkand";
      break;
  case "12":
      $timezone = "Asia/Samarkand";
      break;
  case "13":
      $timezone = "Asia/Tashkent";
      break;
  case "14":
      $timezone = "Asia/Tashkent";
      break;
  } 
  break;
case "TL":
    $timezone = "Asia/Dili";
    break;
case "PF":
    $timezone = "Pacific/Marquesas";
    break;
case "SX":
    $timezone = "America/Curacao";
    break;
case "BQ":
    $timezone = "America/Curacao";
    break;
case "CW":
    $timezone = "America/Curacao";
    break;
  } 
  return $timezone; 
}

function download_booking_pdf_file($szPdfFilePath)
{
    if(!empty($szPdfFilePath) && file_exists($szPdfFilePath))
    {	 
       /* ob_end_clean();
        header('Content-Description: File Transfer');
        header('Content-type: application/pdf');
        header("Content-Type: application/force-download"); 
        header('Content-Disposition: attachment; filename=' . urlencode(basename($szPdfFilePath))); 
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($szPdfFilePath));*/
        ob_clean();
        //flush();
        header('Content-type: application/pdf');
        readfile($szPdfFilePath);    
       // @unlink($szPdfFilePath);
        exit; 
    }
} 
function editInvestorProfile($transportecaManagement,$t_base)
{	
    //print_r($transportecaManagement);
    $t_base=trim($t_base);
    $id=(int)sanitize_all_html_input($id);
    if($transportecaManagement!=array())
    {
        ?>
        <div id="popup-bg"></div>
        <div id="popup-container">
            <div class="popup" style="width: 300px;"> 
            <div id="Error"></div>		
            <h5><?=t($t_base.'title/edit_admin_details');?></h5>
            <form id="createNewAdminProfile">
                <label class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/f_name');?></span>
                    <span class="field-container">
                        <input type="text" name="createProfile[fName]" value ="<?=$transportecaManagement['szFirstName']?>" style="width: 190px;">
                    </span>
                </label>
                <div style="clear: both;"></div>
                <label class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/l_name');?></span>
                    <span class="field-container">
                        <input type="text" name="createProfile[lName]" value ="<?=$transportecaManagement['szLastName']?>" style="width: 190px;">
                    </span>
                </label>
                <div style="clear: both;"></div>
                <label class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/email');?></span>
                    <span class="field-container">
                        <input type="text" name="createProfile[eMail]" value ="<?=$transportecaManagement['szEmail']?>" style="width: 190px;">
                    </span>
                </label> 
                <div style="clear: both;"></div> 
                <input type="hidden" name="flag" value="EDIT_TRANSPORTECA_INVESTOR_DETAILS_CONFIRM">
                <div style="clear: both;"></div>
                <input type="hidden" name="createProfile[id]" value="<?=$transportecaManagement['id']?>">
            </form>
            <br />
            <div class="oh">
                <p align="center">	
                    <a href="javascript:void(0);" onclick="editAdminProfileDetails()" class="button1"><span><?=t($t_base.'fields/confirm');?></span></a>			
                    <a href="javascript:void(0);" onclick="cancelEditMAnageVar(0)" class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>			
                </p>
            </div>
        </div>
    </div>
            <?php
    }
} 
function os_info($uagent)
{
    // the order of this array is important
    global $uagent;
    $oses   = array(
        'Win311' => 'Win16',
        'Win95' => '(Windows 95)|(Win95)|(Windows_95)',
        'WinME' => '(Windows 98)|(Win 9x 4.90)|(Windows ME)',
        'Win98' => '(Windows 98)|(Win98)',
        'Win2000' => '(Windows NT 5.0)|(Windows 2000)',
        'WinXP' => '(Windows NT 5.1)|(Windows XP)',
        'WinServer2003' => '(Windows NT 5.2)',
        'WinVista' => '(Windows NT 6.0)',
        'Windows 7' => '(Windows NT 6.1)',
        'Windows 8' => '(Windows NT 6.2)',
        'WinNT' => '(Windows NT 4.0)|(WinNT4.0)|(WinNT)|(Windows NT)',
        'OpenBSD' => 'OpenBSD',
        'SunOS' => 'SunOS',
        'Ubuntu' => 'Ubuntu',
        'Android' => 'Android',
        'Linux' => '(Linux)|(X11)',
        'iPhone' => 'iPhone',
        'iPad' => 'iPad',
        'MacOS' => '(Mac_PowerPC)|(Macintosh)',
        'QNX' => 'QNX',
        'BeOS' => 'BeOS',
        'OS2' => 'OS/2',
        'SearchBot' => '(nuhk)|(Googlebot)|(Yammybot)|(Openbot)|(Slurp)|(MSNBot)|(Ask Jeeves/Teoma)|(ia_archiver)'
    );
    $uagent = strtolower($uagent ? $uagent : $_SERVER['HTTP_USER_AGENT']);
    foreach ($oses as $os => $pattern)
        if (preg_match('/' . $pattern . '/i', $uagent))
            return $os;
    return 'Unknown';
} 

function display_service_type_description($postSearchAry,$iLanguage,$bAdminFlag=false,$iPartnerApi=false,$iManualQuoteSend=false,$szHandoverCity='')
{
    $kBooing = new cBooking();
    $kConfig_new = new cConfig();
    if(!$iPartnerApi)
    {
        $postSearchAry =  $kBooing->getExtendedBookingDetails($postSearchAry['id']);  
    }
    $t_base = "BookingConfirmation/"; 
    $kConfig_new->loadCountry($postSearchAry['idOriginCountry'],false,$iLanguage);
    $szOriginCountryName = $kConfig_new->szCountryName ; 

    $kConfig_new->loadCountry($postSearchAry['idDestinationCountry'],false,$iLanguage); 
    $szDestinationCountryName = $kConfig_new->szCountryName ;
  
    if($bAdminFlag)
    {
        $configLangArr = $kConfig_new->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$iLanguage);
        $szTransportationFrom = $configLangArr[1]['szTransportationFrom'];
        $szShipperDoorIn = $configLangArr[1]['szShipperDoorIn'];
        $szConsigneeDoorIn = $configLangArr[1]['szConsigneesDoorIn'];
        $szWarehouseIn = $configLangArr[1]['szWarehouseIn'];
        $szYourDoorIn = $configLangArr[1]['szYourDoorIn'];
        $szAirportIn = $configLangArr[1]['szAirportIn'];
        $szSeaportIn = $configLangArr[1]['szSeaportIn'];
        
        $szServiceErrorIn = $configLangArr[1]['szServiceErrorIn'];
        $szTo = $configLangArr[1]['szTo'];
        $szCustomIn = $configLangArr[1]['szCustomIn'];
        $szAnd = $configLangArr[1]['szAnd'];
        $szThisIsExw = $configLangArr[1]['szThisIsExw'];
        $szThisIsExwDtd = $configLangArr[1]['szThisIsExwDtd'];
        $szThisIsDap = $configLangArr[1]['szThisIsDap'];
        $szThisIsFob = $configLangArr[1]['szThisIsFob'];
        $szThisIsCfr = $configLangArr[1]['szThisIsCfr']; 
    }
    else
    {
        $szTransportationFrom = t($t_base.'fields/transportation_from_the');
        $szShipperDoorIn = t($t_base.'fields/shippers_door_in');
        $szConsigneeDoorIn = t($t_base.'fields/consignees_door_in'); 
        $szWarehouseIn = t($t_base.'fields/cfs_in');
        $szYourDoorIn = t($t_base.'fields/your_door_in');
        $szAirportIn = t($t_base.'fields/airport_in');
        $szSeaportIn = t($t_base.'fields/port_in');
        $szServiceErrorIn = t($t_base.'fields/error');
        $szTo = t($t_base.'fields/to');
        $szCustomIn = t($t_base.'fields/customs_in');
        $szAnd = t($t_base.'fields/and');
        $szThisIsExw = t($t_base.'fields/this_is_exw');
        $szThisIsExwDtd = t($t_base.'fields/this_is_exw_wtd');
        $szThisIsDap = t($t_base.'fields/this_is_dap');
        $szThisIsFob = t($t_base.'fields/this_is_fob');
        $szThisIsCfr = t($t_base.'fields/this_is_cfr'); 
    }
    
    if(empty($postSearchAry['szWarehouseFromCity']))
    {
        $postSearchAry['szWarehouseFromCity'] = $postSearchAry['szShipperCity'];
    }
    
    if(empty($postSearchAry['szWarehouseToCity']))
    {
        $postSearchAry['szWarehouseToCity'] = $postSearchAry['szConsigneeCity'];
    } 
    $bDisplayExwDescrition = true;
    if($postSearchAry['iRailTransport']==1 && $postSearchAry['idServiceType']!=__SERVICE_TYPE_DTD__)
    {
        /*
        * If Trade is added as Rail Transport - DTD: Yes then we always consider srvice term for that service as EXW - DTD
        */
        $postSearchAry['idServiceTerms']=__SERVICE_TERMS_EXW__;
        $postSearchAry['idServiceType']=__SERVICE_TYPE_DTD__;
        
        $bDisplayExwDescrition = false;
    } 
    
    if($postSearchAry['idServiceTerms']<=0)
    {
        /*
        *  For automatic LCL/LTL bookings we don't have idServiceTerms this will be decided based on idServiceType
        */ 
        
        if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__)
        {
            $idServiceTerms = __SERVICE_TERMS_EXW__;
        } 
        else  
        {
            $idServiceTerms = __SERVICE_TERMS_FOB__;
        } 
        $postSearchAry['idServiceTerms'] = $idServiceTerms;
    }
    
    $szServiceDescriptionString = $szTransportationFrom;
    if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW__)
    {					
        $szServiceDescriptionString .= " ".$szShipperDoorIn ; 
    }
    else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW_WTD__)
    {
        $szServiceDescriptionString .= " ".$szWarehouseIn ; 
    }
    else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_DAP__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_DAP_NO_CC__)
    {
        if($postSearchAry['iShipperConsignee']==1)
        {
            $szServiceDescriptionString .= " ".$szYourDoorIn ; 
        }
        else
        {
            $szServiceDescriptionString .= " ".$szShipperDoorIn ; 
        }
    }
    else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_CFR__)
    {
        if($postSearchAry['iShipperConsignee']==1)
        {
            $szServiceDescriptionString .= " ".$szYourDoorIn ; 
        }
        else
        {
            $szServiceDescriptionString .= " ".$szShipperDoorIn ; 
        }
    }
    else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_FOB__)
    {
        if($postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_AIR__)
        {
            $szServiceDescriptionString .= " ".$szAirportIn ; 
        }
        else if($postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_SEA__ || $postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_FCL__)
        {
            $szServiceDescriptionString .= " ".$szSeaportIn ; 
        }
    } 
    
    if($szHandoverCity!='' && ($postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_FOB__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW_WTD__))
    {
        $szServiceDescriptionString .= " ".$szHandoverCity ; 
    }
    else
    {
        if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_DAP__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_CFR__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_DAP_NO_CC__)
        {					
            $szServiceDescriptionString .= " ".$postSearchAry['szShipperCity'] ; 
        }
        else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_FOB__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW_WTD__)
        {
            $szServiceDescriptionString .= " ".$postSearchAry['szWarehouseFromCity'] ; 
        }
        else
        { 
            $szServiceDescriptionString .= " ".$szServiceErrorIn ; 
        }
    }
    $szServiceDescriptionString .= " ".$szTo ; 
    
    if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW_WTD__)
    {					
        if($postSearchAry['iShipperConsignee']==2) //Consignee
        {
            $szServiceDescriptionString .= " ".$szYourDoorIn ; 
        }
        else
        {
            $szServiceDescriptionString .= " ".$szConsigneeDoorIn ; 
        }
    }
    else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_DAP__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_DAP_NO_CC__)
    {
        $szServiceDescriptionString .= " ".$szConsigneeDoorIn ; 
    }
    else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_CFR__)
    {
        if($postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_AIR__)
        {
            $szServiceDescriptionString .= " ".$szAirportIn ; 
        }
        else if($postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_SEA__ || $postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_FCL__)
        {
            $szServiceDescriptionString .= " ".$szSeaportIn; 
        }
    }
    else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_FOB__)
    {
        if($postSearchAry['iShipperConsignee']==2) //Consignee
        {
            $szServiceDescriptionString .= " ".$szYourDoorIn ; 
        }
        else
        {
            $szServiceDescriptionString .= " ".$szConsigneeDoorIn ; 
        }    
    }
    
    
    if($szHandoverCity!='' && ($postSearchAry['idServiceTerms']==__SERVICE_TERMS_CFR__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_DAP__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_DAP_NO_CC__))
    {
        $szServiceDescriptionString .= " ".$szHandoverCity ; 
    }
    else
    {
        if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_DAP__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_FOB__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_DAP_NO_CC__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW_WTD__)
        {	
            $szServiceDescriptionString .= " ".$postSearchAry['szConsigneeCity'] ; 
        }
        else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_CFR__)
        {
            $szServiceDescriptionString .= " ".$postSearchAry['szWarehouseToCity'] ; 
        }
        else
        { 
            $szServiceDescriptionString .= " ".$szServiceErrorIn ; 
        }
    }
    
    $kCourierServices = new cCourierServices();
    if(!$kCourierServices->checkFromCountryToCountryExistsForCC($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry']))
    {
        //echo $postSearchAry['iOriginCC']."iOriginCC";
        if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_DAP__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW_WTD__)
        {
            
            /*if($iManualQuoteSend)
            {
                if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW__)
                {
                    $szServiceDescriptionString .= " ".$szCustomIn." ".$szOriginCountryName." ".$szAnd." ".$szDestinationCountryName."." ;
                }
                else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_DAP__)
                {
                    $szServiceDescriptionString .= " ".$szCustomIn." ".$szOriginCountryName."." ;
                }
                else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW_WTD__)
                {
                    $szServiceDescriptionString .= " ".$szCustomIn." ".$szDestinationCountryName."." ;
                } 
                else
                {
                    $szServiceDescriptionString .=".";
                }
            }
            else
            {*/
                if($postSearchAry['iOriginCC']==1 && $postSearchAry['iDestinationCC']==2)
                {
                    $szServiceDescriptionString .= " ".$szCustomIn." ".$szOriginCountryName." ".$szAnd." ".$szDestinationCountryName."." ;
                }
                else if($postSearchAry['iOriginCC']==1)
                {
                    $szServiceDescriptionString .= " ".$szCustomIn." ".$szOriginCountryName."." ;
                }
                else if($postSearchAry['iDestinationCC']==2)
                {
                    $szServiceDescriptionString .= " ".$szCustomIn." ".$szDestinationCountryName."." ;
                } 
                else
                {
                    $szServiceDescriptionString .=".";
                }
            //}
        }
        else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_FOB__)
        {
            $szServiceDescriptionString .= " ".$szCustomIn." ".$szDestinationCountryName."." ;
        }
        else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_CFR__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_DAP_NO_CC__)
        {
            $szServiceDescriptionString .= " ".$szCustomIn." ".$szOriginCountryName."." ;
        } 
    }
    else
    {
        $szServiceDescriptionString .=".";
    }
    
    if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW__ && $bDisplayExwDescrition)
    {
        $szServiceDescriptionString .= " ".$szThisIsExw ;
    }
    if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW_WTD__)
    {
        $szServiceDescriptionString .= " ".$szThisIsExwDtd;
    } 
    else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_DAP__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_DAP_NO_CC__)
    {
        $szServiceDescriptionString .= " ".$szThisIsDap ;
    }
    else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_FOB__)
    {
        $szServiceDescriptionString .= " ".$szThisIsFob ;
    }
    else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_CFR__)
    {
        $szServiceDescriptionString .= " ".$szThisIsCfr;
    }
    return $szServiceDescriptionString;
} 

function display_service_type_description_forwarder($postSearchAry,$iLanguage)
{
    $t_base = "BookingConfirmation/";
    
    $kConfig_new = new cConfig();
    $kConfig_new->loadCountry($postSearchAry['idOriginCountry']);
    $szOriginCountryName = $kConfig_new->szCountryName ; 

    $kConfig_new->loadCountry($postSearchAry['idDestinationCountry']); 
    $szDestinationCountryName = $kConfig_new->szCountryName ;
    
    //a. Terms
    if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW__)
    {
        $szServiceDescriptionString .= t($t_base.'fields/exw_booking') ;
    }
    else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_DAP__)
    {
        $szServiceDescriptionString .= t($t_base.'fields/dap_booking') ;
    }
    else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_FOB__)
    {
        $szServiceDescriptionString .= t($t_base.'fields/fob_booking') ;
    }
    else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_CFR__)
    {
        $szServiceDescriptionString .= t($t_base.'fields/cfr_booking') ;
    }
    
    //b. It includes transportation from the
    $szServiceDescriptionString .= " ".t($t_base.'fields/includes_transportation_from_the');
    
    //c. From handover place
    if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_CFR__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_DAP__)
    {					
        $szServiceDescriptionString .= " ".$szShipperDoorIn ; 
    }  
    else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_FOB__)
    {
        if($postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_AIR__)
        {
            $szServiceDescriptionString .= " ".t($t_base.'fields/airport_in') ; 
        }
        else if($postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_SEA__ || $postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_FCL__)
        {
            $szServiceDescriptionString .= " ".t($t_base.'fields/port_in') ; 
        }
    } 
    
    if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_DAP__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_CFR__)
    {					
        $szServiceDescriptionString .= " ".$postSearchAry['szShipperCity'] ; 
    }
    else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_FOB__)
    {
        $szServiceDescriptionString .= " ".$postSearchAry['szWarehouseFromCity'] ; 
    }
    else
    { 
        $szServiceDescriptionString .= " ".t($t_base.'fields/error') ; 
    }
    $szServiceDescriptionString .= " ".t($t_base.'fields/to') ; 
    
    //f. To handover place
    
    if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_FOB__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_DAP__)
    {
        $szServiceDescriptionString .= " ".t($t_base.'fields/consignees_door_in') ; 
    }
    else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_CFR__)
    {
        if($postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_AIR__)
        {
            $szServiceDescriptionString .= " ".t($t_base.'fields/airport_in') ; 
        }
        else if($postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_SEA__ || $postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_FCL__)
        {
            $szServiceDescriptionString .= " ".t($t_base.'fields/port_in') ; 
        }
    } 
    
    if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_DAP__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_FOB__)
    {					
        $szServiceDescriptionString .= " ".$postSearchAry['szConsigneeCity'] ; 
    }
    else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_CFR__)
    {
        $szServiceDescriptionString .= " ".$postSearchAry['szWarehouseToCity'] ; 
    }
    else
    { 
        $szServiceDescriptionString .= " ".t($t_base.'fields/error') ; 
    }
    
    if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW__ || $postSearchAry['idServiceTerms']==__SERVICE_TERMS_DAP__)
    {
        $szServiceDescriptionString .= " ".t($t_base.'fields/customs_in')." ".$szOriginCountryName." ".t($t_base.'fields/and')." ".$szDestinationCountryName."." ;
    }
    else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_FOB__)
    {
        $szServiceDescriptionString .= " ".t($t_base.'fields/customs_in')." ".$szDestinationCountryName."." ;
    }
    else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_CFR__)
    {
        $szServiceDescriptionString .= " ".t($t_base.'fields/customs_in')." ".$szOriginCountryName."." ;
    } 
    return $szServiceDescriptionString;
} 

function getBusinessDaysForCourier($startdate, $days,$flag=false){

    if(!is_numeric($date1)){
        $date1 = strtotime($date1);
    }
	$i = 1;
        $date = date('Y-m-d',strtotime($startdate));
       //echo (int)$days;    
        while($i <= (int)$days){  
            $date = date('Y-m-d',strtotime($date. ' -1 Weekday'));
            //if(!in_array($date,$holidays)) 
            $i++;
        }  
   
    
	if(strtotime($startdate)>=strtotime(date('Y-m-d')))
	{
            if(strtotime($date)<strtotime(date('Y-m-d')))
            {			
                    $date=date('Y-m-d');
            }
	}
	 return date('Y-m-d',strtotime($date));
}

function getBusinessDaysForCourierDeliveryDate($startdate, $days){

    if(!is_numeric($date1)){
        $date1 = strtotime($date1);
    }
	$i = 1;
        $date = date('Y-m-d',strtotime($startdate));
       //echo (int)$days;    
        while($i <= (int)$days){  
            $date = date('Y-m-d',strtotime($date. ' +1 Weekday'));
            //if(!in_array($date,$holidays)) 
            $i++;
        }  
   
    
	if(strtotime($startdate)>strtotime(date('Y-m-d')))
	{
            if(strtotime($date)<strtotime(date('Y-m-d')))
            {			
                $date=date('Y-m-d');
            }
	}
	 return date('Y-m-d',strtotime($date));
} 

function exp2dec($number) {
    preg_match('/(.*)E-(.*)/', str_replace(".", "", $number), $matches);
    $num = "0.";
    while ($matches[2] > 0) {
        $num .= "0";
        $matches[2]--;
    }
    return $num . $matches[1];
}

function get_formated_cargo_volume($cargo_value)
{ 
    if($cargo_value>0)
    { 
        $cargo_value = (float)$cargo_value;   
        $volumeAry = explode(".",$cargo_value);	
        $interger_value = $volumeAry[0];					
        $decimal_value = $volumeAry[1];  
        //echo $decimal_value."decimal_value  ".substr($decimal_value,0,1);
        if(substr($decimal_value,0,1)>0 && substr($decimal_value,1,1)>0)
        { 
            return round($cargo_value,2);
        }
        else if(substr($decimal_value,0,1)==0 && substr($decimal_value,1,1)==0 && $decimal_value>0)
        { 
            $cargo_volume = $interger_value.".01";
            return $cargo_volume ;
        }
        else if((substr($decimal_value,0,1)==0 || substr($decimal_value,0,1)!=9) && substr($decimal_value,1,1)!=9)
        { 
            $second_digit = substr($decimal_value,1); 
            $decimal_value = substr($decimal_value,0,1);     
            if($second_digit>0)
            {
                //$decimal_value = "0".(substr($second_digit,0,1)+ 1);
                $decimal_value .= (substr($second_digit,0,1))+1;
            }  
            $cargo_volume = $interger_value.".".$decimal_value; 
            return $cargo_volume ;
        }
        else if(substr($decimal_value,0,1)==9 && substr($decimal_value,1,1)>0)
        {
            $decimal_value = (substr($decimal_value,0,1) + 1) . "0";
            $interger_value = $interger_value + 1;
            $cargo_volume = $interger_value.".".$decimal_value;
            return $cargo_volume ;
        } 
        else if(substr($decimal_value,1,1)==9)
        {
            $decimal_value = (substr($decimal_value,0,1) + 1) . "0";
            $interger_value = $interger_value ;
            $cargo_volume = $interger_value.".".$decimal_value;
            return $cargo_volume ;
        } 
        else if($decimal_value>0)
        { 
            if(substr($decimal_value,0,1)==9)
            {
                $decimal_value = 0 ;
                $interger_value = $interger_value + 1;
            } 
            else
            {
                $devisor = strlen($decimal_value);
                if($devisor>0)
                {
                    $devisorVal = pow(10 , ($devisor-1));
                }
                else
                {
                    $devisorVal = 10 ;
                } 
                $decimal_value = ceil($decimal_value / $devisorVal);
            }
            $cargo_volume = $interger_value.".".$decimal_value; 
            return $cargo_volume ;
        } 
    }
    else
    {
        return 0.0 ;
    }
}
function format_numeric_vals($value)
{
    if($value!='')
    {
        $valueArr=explode(",",$value);
        if(count($valueArr)==2)
        {
            $value=$valueArr[0].".".$valueArr[1]; 
        }
        else
        {
            $value = $value;
        } 
        return round_up($value,1);
    }
}
function round_up ( $value, $precision=0 ) 
{ 
    $pow = pow ( 10, $precision ); 
    return ( ceil ( $pow * $value ) + ceil ( $pow * $value - ceil ( $pow * $value ) ) ) / $pow; 
} 

function getAddressDetailsFromIP()
{
    if(!empty($_COOKIE['__USER_IP_DETAILS___']))
    {
        $userIpDetailsStr = $_COOKIE['__USER_IP_DETAILS___'] ; 
        $userIpDetailsStr = stripslashes($userIpDetailsStr);
        $userIpDetailsAry = array();
        $userIpDetailsAry = unserialize($userIpDetailsStr);
        $szUserCountryName  = $userIpDetailsAry['szCountryCode'] ;  
    }
    else
    {   
        $userIpDetailsAry = array();
        $userIpDetailsAry = getCountryCodeByIPAddress();

        $szUserCountryName  = $userIpDetailsAry['szCountryCode'] ; 
        $userIpDetailsStr = serialize($userIpDetailsAry);

        //setting cookien variable
        if($_COOKIE['__USER_COUNTRY_NAME___']!=$szUserCountryName)
        {
            setCookie('__USER_COUNTRY_NAME___', $szUserCountryName ,time()+3600, '', $_SERVER['HTTP_HOST']);
        }  
        if($_COOKIE['__USER_IP_DETAILS___']!=$userIpDetailsStr)
        {
            setCookie('__USER_IP_DETAILS___', $userIpDetailsStr ,time()+3600, '', $_SERVER['HTTP_HOST']);  
        } 
    }  
    $visitor_location = array();
    $visitor_location['CityName'] = $userIpDetailsAry['szCityName'] ;
    $visitor_location['RegionName'] = $userIpDetailsAry['szRegionName'] ;
    $visitor_location['szZipCode'] = $userIpDetailsAry['szZipCode'] ;
    $visitor_location['szCountryName'] = $userIpDetailsAry['szCountryName'] ;
  
    $toAddress='';
    if(!empty($visitor_location['szZipCode']))
    {
        $toAddress =$visitor_location['szZipCode']." ";
    }
    if(!empty($visitor_location['CityName']))
    {
        $toAddress .=$visitor_location['CityName'].", ";
    } 
    if(!empty($visitor_location['RegionName']))
    {
        $toAddress .=$visitor_location['RegionName'].", ";
    } 
    $toAddress .= $visitor_location['szCountryName'];
    return $toAddress;
}

function replaceSearchMiniConstants($szPageContent,$idSeoPage=false)
{
    $kWhsSearch = new cWHSSearch();     
    $szPageVersion_1 = '<div id="search_mini_global_conatiner_1"><script type="text/javascript">display_search_mini_pages(1,\''.$idSeoPage.'\')</script></div>';
    $szPageVersion_2 = '<div id="search_mini_global_conatiner_2"><script type="text/javascript">display_search_mini_pages(2,\''.$idSeoPage.'\')</script></div>'; 
    $szPageVersion_3 = '<div id="search_mini_global_conatiner_3"><script type="text/javascript">display_search_mini_pages(3,\''.$idSeoPage.'\')</script></div>';
    $szPageVersion_4 = '<div id="search_mini_global_conatiner_4"><script type="text/javascript">display_search_mini_pages(4,\''.$idSeoPage.'\')</script></div>';
    $szPageVersion_5 = '<div id="search_mini_global_conatiner_5"><script type="text/javascript">display_search_mini_pages(5,\''.$idSeoPage.'\')</script></div>';
    $szPageVersion_6 = '<div id="search_mini_global_conatiner_6"><script type="text/javascript">display_search_mini_pages(6,\''.$idSeoPage.'\')</script></div>';
    $szPageVersion_duty = '<div id="duty_calculator"><script type="text/javascript">duty_calculator_show()</script></div>';
    

    $pageConstantAry = array('__INCLUDE_SEARCH_MINI_1__','__INCLUDE_SEARCH_MINI_2__','__INCLUDE_SEARCH_MINI_3__','__INCLUDE_SEARCH_MINI_4__','__INCLUDE_SEARCH_MINI_5__','__INCLUDE_SEARCH_MINI_6__','__INCLUDE_DUTY_CALCULATOR__'); 
    $pageContentAry = array($szPageVersion_1,$szPageVersion_2,$szPageVersion_3,$szPageVersion_4,$szPageVersion_5,$szPageVersion_6,$szPageVersion_duty);

    $szSeoPageContent = str_replace($pageConstantAry, $pageContentAry, $szPageContent);
    return $szSeoPageContent;
}

function createStripeDescription($postSearchAry)
{
    $t_base = "BookingConfirmation/";  
    $iLanguage = getLanguageId();
    $szServiceDescriptionText = display_service_type_description($postSearchAry,$iLanguage);
    return $szServiceDescriptionText;
    
    /*
     * $fCargoWeight=$postSearchAry['fCargoWeight']/1000; 
    $kConfig =new cConfig();
    $langArr=$kConfig->getLanguageDetails('',$iLanguage);
    if(!empty($langArr))
    {
        $szDimensionUnit = $langArr[0]['szDimensionUnit'];
    }
    else 
    {
        $szDimensionUnit = "cbm";
    }
    if($postSearchAry['fCargoWeight']<1000)
    {
        $fCargoWeight=round(get_formated_cargo_measure($postSearchAry['fCargoWeight']))." kg";
    }
    else
    {    
        $fCargoWeight=get_formated_cargo_measure($postSearchAry['fCargoWeight']/1000)." mt"; 
    } 
    //echo $postSearchAry['idServiceType']."idServiceType".$postSearchAry['idServiceTerms'];
    if($postSearchAry['iCourierBooking']==1)
    { 
        $szConsigneeCity = html_entities_flag($postSearchAry['szConsigneeCity'],true);
        $szShipperCity = html_entities_flag($postSearchAry['szShipperCity'],true);
        $szForwarderDispName = html_entities_flag($postSearchAry['szForwarderDispName'],true);
        $szWarehouseFromCity = html_entities_flag($postSearchAry['szWarehouseFromCity'],true);

        //$item_ref_text = "Courier services from door-to-door, all inclusive, from ".$szShipperCity." to ".$szConsigneeCity." with ".$szForwarderDispName." (".get_formated_cargo_measure($postSearchAry['fCargoVolume'])." cbm / ".$fCargoWeight." / ".$postSearchAry['iNumColli']." pieces)";
        $item_ref_text = t($t_base.'fields/courier_service_door_to_door')." ".$szShipperCity." ".t($t_base.'fields/to')." ".$szConsigneeCity." ".t($t_base.'fields/with')." ".$szForwarderDispName." (".format_volume($postSearchAry['fCargoVolume'])." ".$szDimensionUnit." / ".$fCargoWeight." / ".$postSearchAry['iNumColli']." ".t($t_base.'fields/pieces').")"; 
    }
    else if($postSearchAry['idServiceType']==__SERVICE_TYPE_PTD__)  // 3- PTD
    {	
        $szConsigneeCity = html_entities_flag($postSearchAry['szConsigneeCity'],true);
        $szForwarderDispName = html_entities_flag($postSearchAry['szForwarderDispName'],true);
        $szWarehouseFromCity = html_entities_flag($postSearchAry['szWarehouseFromCity'],true);

        //$item_ref_text = "FOB ".$szWarehouseFromCity." to ".$szConsigneeCity." with ".$szForwarderDispName." (".get_formated_cargo_measure($postSearchAry['fCargoVolume'])." cbm / ".get_formated_cargo_measure($fCargoWeight)." mt)";
        $item_ref_text = "FOB ".$szWarehouseFromCity." ".t($t_base.'fields/to')." ".$szConsigneeCity." ".t($t_base.'fields/with')." ".$szForwarderDispName." (".format_volume($postSearchAry['fCargoVolume'])." ".$szDimensionUnit." / ".$fCargoWeight.")";
    }
    else
    {
        $szConsigneeCity = html_entities_flag($postSearchAry['szConsigneeCity'],true);
        $szForwarderDispName = html_entities_flag($postSearchAry['szForwarderDispName'],true);
        $szShipperCity = html_entities_flag($postSearchAry['szShipperCity'],true);
        //echo $postSearchAry['idServiceType']."idServiceType".$postSearchAry['idServiceTerms']."--".$postSearchAry['iShipperConsignee'];
        if(($postSearchAry['iShipperConsignee']==1 || $postSearchAry['iShipperConsignee']==3) && $postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__)
        {
            if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW__)
            {        
                $item_ref_text = "DAP ".$szShipperCity." ".t($t_base.'fields/to')." ".$szConsigneeCity." ".t($t_base.'fields/with')." ".$szForwarderDispName." (".format_volume($postSearchAry['fCargoVolume'])." ".$szDimensionUnit." / ".$fCargoWeight.")";
            }
            else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_CFR__)
            {
                $item_ref_text = "CFR ".$szShipperCity." ".t($t_base.'fields/to')." ".$szConsigneeCity." ".t($t_base.'fields/with')." ".$szForwarderDispName." (".format_volume($postSearchAry['fCargoVolume'])." ".$szDimensionUnit." / ".$fCargoWeight.")";
            }
            else
            {
                $item_ref_text = "EXW ".$szShipperCity." ".t($t_base.'fields/to')." ".$szConsigneeCity." ".t($t_base.'fields/with')." ".$szForwarderDispName." (".format_volume($postSearchAry['fCargoVolume'])." ".$szDimensionUnit." / ".$fCargoWeight.")";
            }
            
        }
        else if(($postSearchAry['iShipperConsignee']==1 || $postSearchAry['iShipperConsignee']==3) && $postSearchAry['idServiceType']==__SERVICE_TYPE_DTP__)
        {
            $item_ref_text = "CFR ".$szShipperCity." ".t($t_base.'fields/to')." ".$szConsigneeCity." ".t($t_base.'fields/with')." ".$szForwarderDispName." (".format_volume($postSearchAry['fCargoVolume'])." ".$szDimensionUnit." / ".$fCargoWeight.")";
            
        }
        else{
            $item_ref_text = "EXW ".$szShipperCity." ".t($t_base.'fields/to')." ".$szConsigneeCity." ".t($t_base.'fields/with')." ".$szForwarderDispName." (".format_volume($postSearchAry['fCargoVolume'])." ".$szDimensionUnit." / ".$fCargoWeight.")";
        }
    }
    return $item_ref_text;
     * 
     */
} 
function jsonRemoveUnicodeSequences($struct) 
{   
   return preg_replace_callback("/\\\\u([a-f0-9]{4})/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", json_encode($struct));
}
function display_api_response($szApiEndPoint,$szApiRequestHeaders,$szApiRequestParams,$szApiResponseParams,$szApiResponseCode,$szApiResponseHeaders,$szExceptionMessage=false,$szMethod='price')
{
    if(!empty($szApiRequestParams))
    { 
        $szApiRequestParams = jsonRemoveUnicodeSequences($szApiRequestParams);  
    }  
    if(!empty($szApiResponseParams) && is_array($szApiResponseParams))
    { 
        $szApiResponseParams = jsonRemoveUnicodeSequences($szApiResponseParams);  
        $szApiResponseParams = stripslashes($szApiResponseParams);
    } 
?>  
    <span class="api_area_heading">
        <h3>
            <span class="api_hdng1">API End Point:</span>
            <span class="inpt1 api-flo"><?php echo $szApiEndPoint;?></span>
        </h3> 
    </span>  
    <div class="api_subheading_1"> 
        <h2>API Request</h2>
        <span class="api_dscrarea">
            <span class="api_ori_hdng">Request Type :</span>
            <span class="api_dp_menu_1"><?php echo $szMethod; ?></span>
            <span class="api_clear"></span>
            <span class="api_ori_hdng">HTTP Method :</span>
            <span class="api_dp_menu_1">POST</span>
            <span class="api_clear"></span>
            <span class="api_ori_hdng">Request Headers :</span>
            <span class="api_dp_menu_1"><?php echo print_R($szApiRequestHeaders,true); ?></span>
            <span class="api_clear"></span>
            <span class="api_ori_hdng">Request Params :</span>
            <span class="api_dp_menu_1"><?php echo print_R($szApiRequestParams,true); ?></span>
            <span class="api_clear"></span>
        </span>
    </div>
    <br>   
    <div class="api_subheading_1"> 
        <h2>API Response</h2>
        <span class="api_dscrarea">
            <span class="api_ori_hdng">Response Code:</span>
            <span class="api_dp_menu_1"><?php echo $szApiResponseCode; ?></span>
            <span class="api_clear"></span> 
            <span class="api_ori_hdng">Response Headers :</span>
            <span class="api_dp_menu_1"><?php echo print_R($szApiResponseHeaders,true); ?></span>
            <span class="api_clear"></span>
            <span class="api_ori_hdng">API Response :</span>
            <span class="api_dp_menu_1"><?php echo print_R($szApiResponseParams,true); ?></span>
            <span class="api_clear"></span>
            <?php if(!empty($szExceptionMessage)){ ?>
                <span class="api_ori_hdng">Exception :</span>
                <span class="api_dp_menu_1"><?php echo $szExceptionMessage; ?></span>
                <span class="api_clear"></span>
            <?php } ?> 
        </span>
    </div>
  <?php
}
function parseResponseAry($szApiResponseParams)
{ 
    $encodedAry = json_decode($szApiResponseParams,true);
    if($encodedAry['status']=='Error')
    {
        $processingAry = $encodedAry['errors'];
        if(!empty($processingAry))
        {
            foreach($processingAry as $key=>$processingArys)
            { 
               if(is_string($processingArys['description']))
               {
                   echo " Values: ".$processingArys['description']."  Encode: ".  utf8_decode($processingArys['description']); 
               }
            }
        }
    }
    else
    {
        if(!empty($encodedAry['service']))
        {
            
        }
    } 
}
function getServiceDescription($postSearchAry)
{
    $t_base = "BookingConfirmation/"; 
    if($postSearchAry['iCourierBooking']==1)
    { 
        //$szServiceDescription = t($t_base.'fields/courier_service_from_door_to_door')." ".$postSearchAry['szShipperCity']." ".t($t_base.'fields/to')." ".$postSearchAry['szConsigneeCity'].". ".t($t_base.'fields/tracking_number_will_be_provided');
        $iLanguage = getLanguageId(); 
        $szServiceDescription = display_service_type_description($postSearchAry,$iLanguage);
    
    }
    else
    {
        $iLanguage = getLanguageId(); 
        $szServiceDescription = display_service_type_description($postSearchAry,$iLanguage); 
        
        /*
        if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__)
        {					
            $szServiceDescription = t($t_base.'fields/transportation_from_the')." ".t($t_base.'fields/supplier_door')." ".$postSearchAry['szShipperCity']." ".t($t_base.'fields/to_your_door')." ".$postSearchAry['szConsigneeCity'].", ".t($t_base.'fields/all_inclusive').". ".t($t_base.'fields/this_is_exw');
        }
        else
        {
            $szServiceDescription = t($t_base.'fields/transportation_from_the')." ".t($t_base.'fields/port')." ".$postSearchAry['szWarehouseFromCity']." ".t($t_base.'fields/to_your_door')." ".$postSearchAry['szConsigneeCity'].", ".t($t_base.'fields/all_inclusive').". ".t($t_base.'fields/this_is_fob');
        }
        * 
        */
    }
    return $szServiceDescription;
} 
function getRequestAry($caseAry,$mode)
{  
    if(trim($mode) == "php")
    { 
        $szResponseString = ''; 
        
        if(!empty($caseAry))
        {  
            
            $totalCount=count($caseAry);
            $counter=1;
            
            foreach ($caseAry as $key => $value) 
            {  
                if($key == "cargo")
                {   
                    $cargoAry = $caseAry['cargo'];
                    if(!empty($cargoAry))
                    {
                        $cargoCtr=0;
                        $szResponseString .= "<span class='attribute'>".$key."</span> => "; 
                        foreach($cargoAry as $cargoArys)
                        { 
                            if(!empty($cargoArys))
                            {
                                $szResponseString .= "array([".$cargoCtr."]=><br>"; 
                                $lineResponseString = getRequestAry($cargoArys,$mode);
                                $szResponseString .= $lineResponseString;
                                $szResponseString .= "),<br>";
                                $cargoCtr++; 
                            } 
                        }
                        $szResponseString .= '</span>';
                    }  
                }
                if($key == "customerType")
                {   
                    $customerTypeAry = $caseAry['customerType'];
                    if(!empty($customerTypeAry))
                    {
                        $customerTypeCtr=0;
                        $szResponseString .= "<span class='attribute'>".$key."</span> => "; 
                        $szResponseString .= "array(";  
                        foreach($customerTypeAry as $customerTypeArys)
                        { 
                            if(!empty($customerTypeArys))
                            { 
                                $szResponseString .= '"'.$customerTypeArys.'"';
                                $szResponseString .= ", ";
                                $customerTypeCtr++; 
                            } 
                        }
                        $szResponseString = trim($szResponseString);
                        $szResponseString = rtrim($szResponseString, ",");
                        $szResponseString .= ')</span><br/>';
                    }  
                }
                else if($key == "customer")
                {   
                    $cargoAry = $caseAry['customer'];
                    if(!empty($cargoAry))
                    {
                        $cargoCtr=0;
                        $t=1;
                        $szResponseString .= "<span class='attribute'>".$key."</span> => "; 
                        $total_Count=count($cargoAry);
                        foreach($cargoAry as $keys=>$cargoArys)
                        { 
                            //if(!empty($cargoArys))
                            //{
                                $szResponseString .= "array([".$keys."]=><br>"; 
                                $lineResponseString = $cargoArys;
                                $szResponseString .= $lineResponseString;
                                if($total_Count==$t)
                                {
                                    $szResponseString .= ")<br>";
                                }
                                else
                                {
                                    $szResponseString .= "),<br>";
                                }
                                ++$t;
                                $cargoCtr++; 
                                
                            //} 
                        }
                        $szResponseString .= '</span>';
                    }  
                }
                else
                { 
                    if(is_numeric($value))
                    {
                        $class = "number";
                    }
                    else
                    {
                        $class = "string";
                    }
                    
                    if(trim($key) == "szReference")
                    {
                        $comma = '';
                    }
                    else
                    {
                        
                        if($counter==$totalCount)
                        {
                            $comma="<br>";
                        }else{
                            $comma=",<br>";
                        }
                        
                    }
                    $szResponseString .= "<span class='attribute'>".$key."</span> => <span class='value'><span class=".$class.">".$value."</span>".$comma;
                } 
                
                ++$counter;
            } 
        }
        $szResponseString .= "))";
        return $szResponseString;
    }
    elseif(trim($mode) == "json")
    {
        $szResponseString = ''; 
        
        if(!empty($caseAry))
        {  
            $counter=1;
            $totalCount=count($caseAry);
            foreach ($caseAry as $key => $value) 
            {  
                if($key == "cargo")
                {   
                    $cargoAry = $caseAry['cargo'];
                    if(!empty($cargoAry))
                    { 
                        $totalCount = count($cargoAry); 
                        $cargoCtr=0;
                        $szResponseString .= '<span class="attribute">"'.$key.'"</span>:['; 
                        foreach($cargoAry as $cargoArys)
                        { 
                            if(!empty($cargoArys))
                            {
                                $cargoCtr++; 
                                $szResponseString .= "{<br>"; 
                                $lineResponseString = getRequestAry($cargoArys,$mode); 
                                $szResponseString .= $lineResponseString;
                                $szResponseString .= "}";
                                if($totalCount == $cargoCtr )
                                {
                                    $szResponseString .= "],<br>";
                                }
                                else
                                {
                                    $szResponseString .= ",<br>";
                                } 
                            } 
                        }
                        $szResponseString .= '</span>';
                    }  
                }
                if($key == "customerType")
                {   
                    $customerTypeAry = $caseAry['customerType'];
                    if(!empty($customerTypeAry))
                    { 
                        $totalCount = count($customerTypeAry); 
                        $cargoCtr=0;
                        $szResponseString .= '<span class="attribute">"'.$key.'"</span>:['; 
                        foreach($customerTypeAry as $customerTypeArys)
                        { 
                            if(!empty($customerTypeArys))
                            {
                                $cargoCtr++; 
                                $szResponseString .= '"';  
                                $szResponseString .= $customerTypeArys;
                                $szResponseString .= '"';
                                if($totalCount == $cargoCtr )
                                {
                                    $szResponseString .= "],";
                                }
                                else
                                {
                                    $szResponseString .= ",";
                                } 
                            } 
                        }
                        $szResponseString .= '</span><br>';
                    }  
                }
                else if($key == "customer")
                {   
                    $cargoAry = $caseAry['customer'];
                    
                    if(!empty($cargoAry))
                    {
                        
                        $totalCount = count($cargoAry);
                        
                        $cargoCtr=0;
                        $szResponseString .= '<span class="attribute">"'.$key.'"</span>:[<br>'; 
                        foreach($cargoAry as $keys=>$cargoArys)
                        { 
                            //if(!empty($cargoArys))
                            //{
                                $cargoCtr++; 
                                //$szResponseString .= "<br>"; 
                                //$lineResponseString = getRequestAry($cargoArys,$mode);
                                $szResponseString .= $keys.":".$cargoArys." ";
                                //$szResponseString .= $lineResponseString;
                                $szResponseString .= "";
                                if($totalCount == $cargoCtr )
                                {
                                    $szResponseString .= "]<br>";
                                }
                                else
                                {
                                    $szResponseString .= ",<br>";
                                } 
                            //} 
                        }
                        $szResponseString .= '</span>';
                    }  
                }
                else
                { 
                    if(is_numeric($value))
                    {
                        $class = "number";
                    }
                    else
                    {
                        $class = "string";
                    } 
                    if(trim($key) == "szReference")
                    {
                        $comma = '<br>});';
                    }
                    elseif(trim($key) == "szWeightMeasure")
                    {
                        $comma = '<br>';
                    }
                    else
                    {
                        
                        if($counter==$totalCount)
                        {
                            $comma="<br>";
                        }
                        else
                        {
                            $comma=",<br>";
                        }
                    }
                    $szResponseString .= '<span class="attribute">"'.$key.'"</span>:<span class="value"><span class="'.$class.'">"'.$value.'"</span>'.$comma;
                } 
                ++$counter;
            } 
        } 
        $szResponseString .= "})";
        return $szResponseString;
    }  
} 

function getResponseAry($responseAry,$mode)
{   
    if(trim($mode) == "php")
    {
        $szResponseString = ''; 
        if(!empty($responseAry))
        {  
            foreach ($responseAry as $key => $value) 
            {  
                if($key == "service")
                {   
                    $serviceAry = $responseAry['service'];
                    if(!empty($serviceAry))
                    {
                        $serviceCtr=0;
                        $szResponseString .= "<span class='attribute'>".$key."</span> => "; 
                        foreach($serviceAry as $serviceArys)
                        { 
                            if(!empty($serviceArys))
                            {
                                $szResponseString .= "array([".$serviceCtr."]=><br>"; 
                                $lineResponseString = getResponseAry($serviceArys,$mode);
                                $szResponseString .= $lineResponseString;
                                $szResponseString .= "),<br>";
                                $serviceCtr++; 
                            } 
                        }
                        $szResponseString .= '</span>';
                    }  
                }
                elseif($key == "bookings")
                {
                    $bookingAry = $responseAry['bookings'];
                    if(!empty($bookingAry))
                    {
                        $bookingCtr=0;
                        $szResponseString .= "<span class='attribute'>".$key."</span> => "; 
                        foreach($bookingAry as $bookingArys)
                        { 
                            if(!empty($bookingArys))
                            {
                                $szResponseString .= "array([".$bookingCtr."]=><br>"; 
                                $lineResponseString = getResponseAry($bookingArys,$mode);
                                $szResponseString .= $lineResponseString;
                                $szResponseString .= ")";
                                $bookingCtr++; 
                            } 
                        }
                        $szResponseString .= '</span>';
                    }  
                }
                elseif($key == "insuranceRate" || $key=='errors' || $key=='label')
                { 
                    $bookingAry = $responseAry[$key];
  
                    if(!empty($bookingAry))
                    {
                        $bookingCtr=0;
                        $szResponseString .= "<span class='attribute'>".$key."</span> => "; 
                        foreach($bookingAry as $bookingArys)
                        { 
                            if(!empty($bookingArys))
                            {
                                $szResponseString .= "array([".$bookingCtr."]=><br>"; 
                                $lineResponseString = getResponseAry($bookingArys,$mode);
                                $szResponseString .= $lineResponseString;
                                $szResponseString .= ")";
                                $bookingCtr++; 
                            } 
                        }
                        $szResponseString .= '</span>';
                    }  
                }
                elseif($key == "szCargoList")
                {
                    
                    $bookingAry = $responseAry['szCargoList'];
                    if(!empty($bookingAry))
                    {
                        $bookingCtr=0;
                        $szResponseString .= "<span class='attribute'>".$key."</span> => "; 
                        foreach($bookingAry as $bookingArys)
                        { 
                            if(!empty($bookingArys))
                            {
                                $szResponseString .= "array([".$bookingCtr."]=><br>"; 
                                $lineResponseString = getResponseAry($bookingArys,$mode);
                                $szResponseString .= $lineResponseString;
                                $szResponseString .= ")";
                                $bookingCtr++; 
                            } 
                        }
                        $szResponseString .= '</span>';
                    }
                }
                else
                { 
                    if(is_numeric($value))
                    {
                        $class = "number";
                    }
                    else
                    {
                        $class = "string";
                    }
                    
                    $szResponseString .= "<span class='attribute'>".$key."</span> => <span class='value'><span class=".$class.">".$value.",<br></span>";
                } 
            }
            
        }
    }
    elseif(trim($mode) == "json")
    { 
        $szResponseString = ''; 
        if(!empty($responseAry))
        {  
            foreach ($responseAry as $key => $value) 
            {  
                if($key == "service")
                {   
                    $serviceAry = $responseAry['service'];
                    if(!empty($serviceAry))
                    {
                        $totalServices= count($serviceAry);
                        $serviceCtr=0;
                        $szResponseString .= '<span class="attribute">"'.$key.'"</span>:['; 
                        foreach($serviceAry as $serviceArys)
                        { 
                            if(!empty($serviceArys))
                            {
                                $serviceCtr++; 
                                $szResponseString .= "{<br>"; 
                                $lineResponseString = getResponseAry($serviceArys,$mode);
                                $szResponseString .= $lineResponseString;
                                $szResponseString .= "}";
                                if($totalServices == $serviceCtr)
                                {
                                    $szResponseString .= "]<br>";
                                }
                                else
                                {
                                    $szResponseString .= ",<br>";
                                }
                                
                            } 
                        }
                        $szResponseString .= '</span>';
                    }  
                }
                elseif($key == "bookings")
                {
                    $bookingAry = $responseAry['bookings'];
                    if(!empty($bookingAry))
                    {
                        $totalBooking= count($bookingAry);
                        $serviceCtr=0;
                        $bookingCtr=0;
                        $szResponseString .= '<span class="attribute">"'.$key.'"</span>:['; 
                        foreach($bookingAry as $bookingArys)
                        { 
                            if(!empty($bookingArys))
                            {
                                $bookingCtr++;
                                $szResponseString .= "{<br>"; 
                                $lineResponseString = getResponseAry($bookingArys,$mode);
                                $szResponseString .= $lineResponseString;
                                $szResponseString .= "}]";
                            } 
                        }
                        $szResponseString .= '</span>';
                    }  
                }
                else if($key == "insuranceRate" || $key == "label")
                { 
                    $bookingAry = $responseAry[$key];
                    if(!empty($bookingAry))
                    {
                        $totalBooking= count($bookingAry);
                        $serviceCtr=0;
                        $bookingCtr=0;
                        $szResponseString .= '<span class="attribute">"'.$key.'"</span>:['; 
                        foreach($bookingAry as $bookingArys)
                        { 
                            if(!empty($bookingArys))
                            {
                                $bookingCtr++;
                                $szResponseString .= "{<br>"; 
                                $lineResponseString = getResponseAry($bookingArys,$mode);
                                $szResponseString .= $lineResponseString;
                                $szResponseString .= "}]";
                            } 
                        }
                        $szResponseString .= '</span>';
                    }  
                }
                elseif($key == "szCargoList")
                {
                    $bookingAry = $responseAry['szCargoList'];
                    if(!empty($bookingAry))
                    {
                        $totalBooking= count($bookingAry);
                        $serviceCtr=0;
                        $bookingCtr=0;
                        $szResponseString .= '<span class="attribute">"'.$key.'"</span>:['; 
                        foreach($bookingAry as $bookingArys)
                        { 
                            if(!empty($bookingArys))
                            {
                                $bookingCtr++;
                                $szResponseString .= "{<br>"; 
                                $lineResponseString = getResponseAry($bookingArys,$mode);
                                $szResponseString .= $lineResponseString;
                                $szResponseString .= "}]";
                            } 
                        }
                        $szResponseString .= '</span>';
                    }  
                }
                else
                { 
                    if(is_numeric($value))
                    {
                        $class = "number";
                    }
                    else
                    {
                        $class = "string";
                    }
                    
                    $szResponseString .= '<span class="attribute">"'.$key.'"</span>:<span class="value"><span class="'.$class.'">"'.$value.'",<br></span>';
                } 
            } 
        }
    }  
    return $szResponseString; 
} 

function getLanguageName($idLanguage)
{
    //    if($idLanguage==__LANGUAGE_ID_DANISH__)
//    {
//        $szLanguage = __LANGUAGE_TEXT_DANISH__ ;
//    }
//    else if($idLanguage==__LANGUAGE_ID_SWEDISH__)
//    {
//        $szLanguage = __LANGUAGE_TEXT_SWEDISH__ ;
//    }
//    else if($idLanguage==__LANGUAGE_ID_NORWEGIAN__)
//    {
//        $szLanguage = __LANGUAGE_TEXT_NORWEGIAN__ ;
//    }
//    else
//    {
//        $szLanguage = __LANGUAGE_TEXT_ENGLISH__ ;
//    }
    
    $kConfig = new cConfig();
    $languageArr=$kConfig->getLanguageDetails('',$idLanguage,'','','','','',false);
    if(!empty($languageArr))
    {
        $szLanguage=$languageArr[0]['szName'];
    }
    else
    {
        $szLanguage=__LANGUAGE_TEXT_ENGLISH__;
    }
    return $szLanguage;
}  

function getLanguageActualID($szLanguage)
{
    //    if($szLanguage==__LANGUAGE_TEXT_DANISH__)
//    {
//        $idLanguage =__LANGUAGE_ID_DANISH__  ;
//    }
//    else if($szLanguage==__LANGUAGE_TEXT_SWEDISH__ )
//    {
//        $idLanguage = __LANGUAGE_ID_SWEDISH__ ;
//    }
//    else if($szLanguage== __LANGUAGE_TEXT_NORWEGIAN__)
//    {
//        $idLanguage = __LANGUAGE_ID_NORWEGIAN__ ;
//    }
//    else
//    {
//        $idLanguage = __LANGUAGE_ID_ENGLISH__ ;
//    }
    $kConfig = new cConfig();
    $languageArr=$kConfig->getLanguageDetails('','',false,$szLanguage);
    if(!empty($languageArr))
    {
        $idLanguage=$languageArr[0]['id'];
    }
    else
    {
        $idLanguage=__LANGUAGE_ID_ENGLISH__;
    }
    return $idLanguage;
}

function display_change_booking_notes_popup()
{ 
    $idBooking = sanitize_all_html_input(trim($_REQUEST['id']));
    $szBookingRef = sanitize_all_html_input(trim($_REQUEST['ref']));
    $kBooking=new cBooking();
    $kBooking->load($idBooking);
    $t_base = "ForwarderBookings/";
    ?>
    <div id="popup-bg"></div>
    <div id="popup-container">	
	<div class="compare-popup popup" >
            <p class="close-icon" align="right">
                <a onclick="showHide('invoice_comment_div');" href="javascript:void(0);">
                <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
                </a>
            </p>
            <h5><strong><?=t($t_base.'title/booking_reference');?> <?=$szBookingRef?></strong></h5>
            <p><?=t($t_base.'title/type_reference');?>:</p>
            <p>
                <input type="text"  name="updateInvoiceAry[szForwarderReference]" onkeypress="return submitForm(event)"  id="fwdReference"  value="<?=$kBooking->szInvoiceFwdRef?>" size="43" AUTOCOMPLETE ="off">
            </p>
            <br>
            <p align="center">
                <input type="hidden" id="idBooking" name="updateInvoiceAry[idBooking]" value="<?=$idBooking?>">
                <input type="hidden" id="szRef" name="updateInvoiceAry[szBookingRef]" value="<?=$szBookingRef?>">
                <a href="javascript:void(0)" class="button1" onclick="update_invoice_comment_confirm()"><span><?=t($t_base.'fields/save');?></span></a>
                <a href="javascript:void(0)" class="button2" onclick="showHide('invoice_comment_div')"><span><?=t($t_base.'fields/cancel');?></span></a>&nbsp;
            </p>
	</div>
    </div>
    <?php
}

function substrwords($text, $maxchar, $end='...') {
    if (strlen($text) > $maxchar || $text == '') {
        $words = preg_split('/\s/', $text);      
        $output = '';
        $i      = 0;
        while (1) {
            $length = strlen($output)+strlen($words[$i]);
            if ($length > $maxchar) {
                break;
            } 
            else {
                $output .= " " . $words[$i];
                ++$i;
            }
        }
        $output .= $end;
    } 
    else {
        $output = $text;
    }
    return $output;
} 

function doNotCreateFileForThisDomainEmail($szEmail)
{
    
    if(__ENVIRONMENT__=='LIVE')
    {
        $excludedEmailAry = array('transporteca.com','whiz-solutions.com');
    }
    else{
        $excludedEmailAry = array('transporteca.com');
    }
    
    $szShipperEmailAry = explode("@",$szEmail);
    $szExcludedEmailDomain = $szShipperEmailAry[1];
    
    if(!empty($excludedEmailAry) && !in_array($szExcludedEmailDomain,$excludedEmailAry))
    {
        return true;
    }
    
    return false;
}

function __float($fFloatValue)
{ 
    if($fFloatValue>0)
    {
        $iIntValueArr = explode(".",$fFloatValue);
        if((int)$iIntValueArr[1]>0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}
function __isWeekend($dtDateTime)
{
    /*
    * $dtDateTime should be in YYYY-MM-DD i.e 2016-05-10
    */
    if(__isValidDate($dtDateTime))
    {
        $iWeekDay = date('w',strtotime($dtDateTime)); 
        if($iWeekDay==0 || $iWeekDay==6) //0: Sunday, 6: Saturday
        {
            return true;
        }
        else
        { 
            return false;
        }
    }
}  
function __isValidDate($dtDateTime)
{ 
    $dtNewDate = date('Y-m-d',strtotime($dtDateTime));  
    if(!empty($dtDateTime) && $dtNewDate!='1970-01-01')
    {
        return true;
    }
    else
    {
        return false;
    } 
}
function __nextMonday($dtDateTime)
{
    /*
    * $dtDateTime should be in YYYY-MM-DD i.e 2016-05-10
    */ 
    if(__isValidDate($dtDateTime))
    {
        $timestamp = strtotime($dtDateTime); 
        return date('Y-m-d',strtotime("next Monday " . date('Y-m-d', $timestamp), $timestamp));
    }
}  

function getServiceName($idServiceType)
{
    if($idServiceType==__SERVICE_TYPE_DTD__)
    {
        $szServiceName = "DTD";
    }
    else if($idServiceType==__SERVICE_TYPE_DTW__)
    {
        $szServiceName = "DTW";
    }
    else if($idServiceType==__SERVICE_TYPE_WTD__)
    {
        $szServiceName = "WTD";
    }
    else if($idServiceType==__SERVICE_TYPE_WTW__)
    {
        $szServiceName = "WTW";
    }
    else if($idServiceType==__SERVICE_TYPE_DTP__)
    {
        $szServiceName = "DTP";
    }
    else if($idServiceType==__SERVICE_TYPE_WTP__)
    {
        $szServiceName = "WTP";
    }
    else if($idServiceType==__SERVICE_TYPE_PTP__)
    {
        $szServiceName = "PTP";
    }
    else if($idServiceType==__SERVICE_TYPE_PTD__)
    {
        $szServiceName = "PTD";
    }
    else if($idServiceType==__SERVICE_TYPE_PTW__)
    {
        $szServiceName = "PTW";
    }
    return $szServiceName;
}

function getFooterLanguageUrlNew($szUri,$szLanguage,$szLanguageCurrent)
{
    if($szLanguageCurrent=='en')
    {
        $szRequestUri = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".$szLanguage."".$szUri ;
        
        return $szRequestUri;
    }
    else
    {
        $szRequestUriOld = __MAIN_SITE_HOME_PAGE_SECURE_URL__."".$szUri ;
        if($szLanguage==__LANGUAGE_ID_ENGLISH_CODE__){
            
            $removeText=$szLanguageCurrent."/";
            $szRequestUri=str_replace($szLanguageCurrent."/",'',$szRequestUriOld);
        }
        else
        {
            
            $szLanguageCurrent = $szLanguageCurrent."/";
            $szLanguage = $szLanguage."/";
            $szRequestUri=str_replace($szLanguageCurrent,$szLanguage,$szRequestUriOld);
        }
        return $szRequestUri;
    }
}

function sortArrayNew($arrData,$sortFieldArr)
{          //print_r($sortFieldArr);
    if( !empty($arrData) )
    {
            foreach($arrData as $data)
            {
                    $newData [] = $data;
            }
            for($i=0; $i<count($newData); $i++)
            {		                   	 
                    if(!empty($sortFieldArr))
                    {

                        foreach($sortFieldArr as $sortFieldArrs)
                        {

                            $p_sort_fieldArr=explode("_",$sortFieldArrs);
                            $ar_sort_field[$p_sort_fieldArr[0]][$i]=$newData[$i][$p_sort_fieldArr[0]];
                        }
                    }
            } 

            $p_sort_fieldArr=explode("_",$sortFieldArr[0]);
            $p_sort_fieldArr1=explode("_",$sortFieldArr[1]);
            $p_sort_fieldArr2=explode("_",$sortFieldArr[2]);
            $p_sort_fieldArr3=explode("_",$sortFieldArr[3]);
            array_multisort($ar_sort_field[$p_sort_fieldArr[0]], ($p_sort_fieldArr[1] ? SORT_DESC : SORT_ASC),$ar_sort_field[$p_sort_fieldArr1[0]], ($p_sort_fieldArr1[1] ? SORT_DESC : SORT_ASC),$ar_sort_field[$p_sort_fieldArr2[0]], ($p_sort_fieldArr2[1] ? SORT_DESC : SORT_ASC),$ar_sort_field[$p_sort_fieldArr3[0]], ($p_sort_fieldArr3[1] ? SORT_DESC : SORT_ASC),$newData);

            return $newData;
    }
}

function setSiteUrlsByLanguageCode($szLanguageCode)
{
    $kConfig = new cConfig();
    $languageArr=$kConfig->getLanguageDetails($szLanguageCode,'','','','','','',false);
    if(!empty($languageArr))
    {
        if($languageArr[0]['id']==__LANGUAGE_ID_ENGLISH__)
        {
            $szLanguage = strtolower($languageArr[0]['szName']);
            define ('__BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
            define ('__BASE_URL_SECURE__', __MAIN_SITE_HOME_PAGE_SECURE_URL__); 
            define ('__HOME_PAGE_BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
            if($szLanguage!=$_SESSION['transporteca_language_en'])
            {
                $_SESSION['HOLDININGPAGE']='';
                unset($_SESSION['HOLDININGPAGE']);
            }
            define_constants($szLanguage);
            $_SESSION['transporteca_language_en'] = $szLanguage; 
        }
        else
        {
            $szLanguage = strtolower($languageArr[0]['szName']);
            define ('__BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__."/".strtolower($languageArr[0]['szLanguageCode']));
            define ('__BASE_URL_SECURE__', __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".strtolower($languageArr[0]['szLanguageCode']));
            define ('__HOME_PAGE_BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
            //echo $_SESSION['transporteca_language_en']."transporteca_language_en2";
            if($szLanguage!=$_SESSION['transporteca_language_en'])
            {
                $_SESSION['HOLDININGPAGE']='';
                unset($_SESSION['HOLDININGPAGE']);
            }
            define_constants($szLanguage);
            $_SESSION['transporteca_language_en'] = $szLanguage ;
        }
    }
    else
    {
        $szLanguage = strtolower(__LANGUAGE_TEXT_ENGLISH__);
        define ('__BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
        define ('__BASE_URL_SECURE__', __MAIN_SITE_HOME_PAGE_SECURE_URL__); 
        define ('__HOME_PAGE_BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
        //echo $_SESSION['transporteca_language_en']."---".$szLanguage;
        if($szLanguage!=$_SESSION['transporteca_language_en'])
        {
            $_SESSION['HOLDININGPAGE']='';
            unset($_SESSION['HOLDININGPAGE']);
        }
        define_constants($szLanguage);
        $_SESSION['transporteca_language_en'] = $szLanguage;        
    }
    
     return $szLanguage;
}

function setSiteUrlsByLanguageName($szLanguageName)
{
    $kConfig = new cConfig();
    $languageArr=$kConfig->getLanguageDetails('','',false,$szLanguageName,'','','',false);
    if(!empty($languageArr))
    {
        if($languageArr[0]['id']==__LANGUAGE_ID_ENGLISH__)
        {
            $szLanguage = strtolower($languageArr[0]['szName']);
            define ('__BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
            define ('__BASE_URL_SECURE__', __MAIN_SITE_HOME_PAGE_SECURE_URL__); 
            define ('__HOME_PAGE_BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
            if($szLanguage!=$_SESSION['transporteca_language_en'])
            {
                $_SESSION['HOLDININGPAGE']='';
                unset($_SESSION['HOLDININGPAGE']);
            }
            define_constants($szLanguage);
            $_SESSION['transporteca_language_en'] = $szLanguage; 
        }
        else
        {
            $szLanguage = strtolower($languageArr[0]['szName']);
            define ('__BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__."/".strtolower($languageArr[0]['szLanguageCode']));
            define ('__BASE_URL_SECURE__', __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".strtolower($languageArr[0]['szLanguageCode']));
            define ('__HOME_PAGE_BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
            //echo $_SESSION['transporteca_language_en']."transporteca_language_en".$szLanguage;
            if($_SESSION['transporteca_language_en']!='')
            {
                if($szLanguage!=$_SESSION['transporteca_language_en'])
                {
                    $_SESSION['HOLDININGPAGE']='';
                    unset($_SESSION['HOLDININGPAGE']);
                }
            }
            define_constants($szLanguage); 
            $_SESSION['transporteca_language_en'] = $szLanguage ;
        }
    }
    else
    {
        $szLanguage = strtolower(__LANGUAGE_TEXT_ENGLISH__);
        define ('__BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
        define ('__BASE_URL_SECURE__', __MAIN_SITE_HOME_PAGE_SECURE_URL__); 
        define ('__HOME_PAGE_BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
        if($szLanguage!=$_SESSION['transporteca_language_en'])
        {
            $_SESSION['HOLDININGPAGE']='';
            unset($_SESSION['HOLDININGPAGE']);
        }
        define_constants($szLanguage);
        $_SESSION['transporteca_language_en'] = $szLanguage; 
    }
    
     return $szLanguage;
}
function getTimeZoneByCountryCode($szCountryCode)
{ 
    if(!empty($szCountryCode))
    {
        $timezoneArr = array(); 
        $timezoneArr = DateTimeZone::listIdentifiers(DateTimeZone::PER_COUNTRY, $szCountryCode); 
        return $timezoneArr[0];
    }
} 
function getDomainByIdLanguage($iBookingLanguage)
{
    
    $kConfig = new cConfig();
    $langArr = array();
    $langArr = $kConfig->getLanguageDetails('',$iBookingLanguage);
    
    $szDomain = $langArr[0]['szDomain'];

    if(empty($szDomain))
    {
        $szDomain = __MAIN_SITE_HOME_PAGE_SECURE_URL__;
    } 
    return $szDomain;
}

function createDownlabelLink($iUrlType,$iBookingLanguage,$szBookingRandamKey)
{ 
    //$iUrlType=1;
    if($iUrlType==1)
    {
        if($iBookingLanguage==1 || $iBookingLanguage==0)
        {
            $szUrl=__MAIN_SITE_HOME_PAGE_SECURE_URL__."/labels/".$szBookingRandamKey."/";
        }
        else
        {
            $kConfig = new cConfig();
            $langArr = array();
            $langArr = $kConfig->getLanguageDetails('',$iBookingLanguage);
            $szUrl=__MAIN_SITE_HOME_PAGE_SECURE_URL__."/".$langArr[0]['szLanguageCode']."/labels/".$szBookingRandamKey."/";
        }
        
        return $szUrl;
    }
    else
    {
       $szDomain = getDomainByIdLanguage($iBookingLanguage);
       return $szDomain."/labels/#".$szBookingRandamKey;
    }
}

function getUrlForLanguage($szLanguageCode)
{
    if($szLanguageCode=='GB')
    {
        $szLanguageCode = "UK";
    }
    else if($szLanguageCode=='DK')
    {
        $szLanguageCode = "da";
    }
    $szLanguageCode = strtolower($szLanguageCode);
    $szLanguageCodeStr = '/'.trim($szLanguageCode).'/';
    if(substr($_SERVER['REQUEST_URI'], 0, 4) === $szLanguageCodeStr)
    {
        $redir_da_url = __MAIN_SITE_HOME_PAGE_SECURE_URL__."".$_SERVER['REQUEST_URI'];
    }
    else
    {
        $redir_da_url = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".$szLanguageCode."".$_SERVER['REQUEST_URI'];
    }
    return $redir_da_url;
}

function getDateCustom($dtDateTime)
{
    /*
    * This function will add 1 hours to the passed dates
    */
    if(!empty($dtDateTime))
    {
        $dtDateTime = date('Y-m-d H:i:s',strtotime("+1 HOUR",strtotime($dtDateTime)));  
    }
    return $dtDateTime;
}
function get_youtube_id($url)
{
    preg_match_all("#(?<=v=|v\/|vi=|vi\/|youtu.be\/)[a-zA-Z0-9_-]{11}#", $url, $matches);
    return $matches[0][0];

}
?>