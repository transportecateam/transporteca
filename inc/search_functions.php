<?php
if(!isset($_SESSION))
{
    session_start();
} 
/**
 * This file contains the functions that are generic for the store and can be used by all the php files in the store website.
 *
 * functiions.php
 * @copyright Copyright (C) 2012 Trnasporteca, LLC
 * @author Ajay
 * @package Trnasporteca Development
 */
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/booking_functions.php" );
require_once( __APP_PATH__ . "/inc/search_functions.php" );
require_once( __APP_PATH_CLASSES__ . "/database.class.php");
require_once( __APP_PATH_CLASSES__ . "/booking.class.php"); 

function display_wtw_services_list($postSearchAry,$searchResult,$t_base,$countryKeyValueAry,$forwarder_flag=false) //WTW
{
	$kWHSSearch=new cWHSSearch();
	$distance_1_header = t($t_base.'messages/DISTANCE_TO_WAREHOUSE_1_TOOL_TIP_HEADER_TEXT_1')." ".$postSearchAry['szOriginCity'].".".t($t_base.'messages/DISTANCE_TO_WAREHOUSE_1_TOOL_TIP_HEADER_TEXT_2');
	$distance_2_header = t($t_base.'messages/DISTANCE_TO_WAREHOUSE_2_TOOL_TIP_HEADER_TEXT_1')." ".$postSearchAry['szDestinationCity'].".".t($t_base.'messages/DISTANCE_TO_WAREHOUSE_2_TOOL_TIP_HEADER_TEXT_2');
	$detail_style="style='text-decoration:none;color:white;'" ;
	
	?>
	<table cellpadding="0" id="service_table"  cellspacing="0" border="0" class="format-1" width="100%">
		<tr>
			<th width="12%" align="center" valign="top"><?=t($t_base.'fields/forwarder')?> <a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/FORWARDER_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/FORWARDER_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></th>
			<th width="11%" align="center" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=t($t_base.'fields/average')?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/AVG_RATING_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/AVG_RATING_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a><br /> <?=t($t_base.'fields/rating');?></th>
			<th width="11%" align="center" valign="top">&nbsp;&nbsp;<?=t($t_base.'fields/distance_to');?> <a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=$distance_1_header?>','<?=t($t_base.'messages/DISTANCE_TO_WAREHOUSE_1_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a><br /> <?=t($t_base.'fields/warehouse_1');?></th>
			<th width="11%" align="center" valign="top">&nbsp;&nbsp;&nbsp;<?=t($t_base.'fields/warehouse_2');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/WAREOUSE_CUTOFF_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/WAREOUSE_CUTOFF_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a><br />  <?=t($t_base.'fields/cut_off');?></th>
			<th width="11%" align="center" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=t($t_base.'fields/available');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/AVAILABLE_FROM_TOOL_TIP_TEXT');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a><br /> <?=t($t_base.'fields/from');?></th>
			<th width="11%" align="center" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;<?=t($t_base.'fields/transport');?> <a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/TRANSPORTATION_TIME_TOOL_TIP_TEXT');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a><br /> <?=t($t_base.'fields/ation_time');?></th>
			<th width="11%" align="center" valign="top">&nbsp;&nbsp;<?=t($t_base.'fields/distance_to');?> <a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=$distance_2_header?>','<?=t($t_base.'messages/DISTANCE_TO_WAREHOUSE_2_TOOL_TIP_TEXT');?>','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a><br /> <?=t($t_base.'fields/warehouse_3');?></th>
			<th width="11%" align="center" valign="top"><?=t($t_base.'fields/price');?> <a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/PRICE_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/PRICE_TOOL_TIP_TEXT');?>','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></th>
			<?php
				if(!$forwarder_flag){
			?>
			<th width="11%"></th>
			<?php } ?>
		</tr>
		<?php
			if(!empty($searchResult))
			{
				$google_map_anchor_style="style='text-decoration:none;color:black;font-style:normal;font-size:12px;'";
				$minRatingToShowStars = $kWHSSearch->getManageMentVariableByDescription('__MINIMUM_RATING_TO_SHOW_RATING_STARS__');
				$tr_counter = 1;
				foreach($searchResult as $searchResults)
				{
					if($forwarder_flag)
					{
						$postSearchAry = array();
						$postSearchAry = $_SESSION['try_it_out_searched_data'] ;
						
						$originPostCodeAry=array();
						$kConfig = new cConfig();
						$originPostCodeAry = $kConfig->getPostCodeDetails($postSearchAry['szOriginCountry'],$postSearchAry['szOriginPostCode'],$postSearchAry['szOriginCity']);
						
						$szOriginAddress =  $postSearchAry['szOriginPostCode']." - ".$postSearchAry['szOriginCity']."<br /> ".$originPostCodeAry['szCountry'];
					    $szDestinationAddress = $searchResults['szFromWHSPostCode']." - ".$searchResults['szFromWHSCity']."<br />".$searchResults['szFromWHSCountry'];
					    $origin_lat_lang_pipeline = $originPostCodeAry['szLatitude']."|".$originPostCodeAry['szLongitude']."|".$searchResults['szFromWHSLat']."|".$searchResults['szFromWHSLong']."|".$searchResults['iDistanceToWHS']."|".$szOriginAddress."|".$szDestinationAddress;
					
						$destinationPostCodeAry=array();
						$destinationPostCodeAry = $kConfig->getPostCodeDetails($postSearchAry['szDestinationCountry'],$postSearchAry['szDestinationPostCode'],$postSearchAry['szDestinationCity']);
						
						$szOriginAddress2 =  $postSearchAry['szDestinationPostCode']." - ".$postSearchAry['szDestinationCity']."<br /> ".$destinationPostCodeAry['szCountry'];
					    $szDestinationAddress2 = $searchResults['szToWHSPostCode']." - ".$searchResults['szToWHSCity']."<br />".$searchResults['szToWHSCountry'];
					
					    $destination_lat_lang_pipeline = $destinationPostCodeAry['szLatitude']."|".$destinationPostCodeAry['szLongitude']."|".$searchResults['szToWHSLat']."|".$searchResults['szToWHSLong']."|".$searchResults['iDistanceToWHS']."|".$szOriginAddress2."|".$szDestinationAddress2;
					}
					else
					{
						$szOriginAddress =  $postSearchAry['szOriginPostCode']." - ".$postSearchAry['szOriginCity']."<br /> ".$postSearchAry['szOriginCountry'];
		                $szDestinationAddress = $searchResults['szFromWHSPostCode']." - ".$searchResults['szFromWHSCity']."<br />".$searchResults['szFromWHSCountry'];
		    
						$origin_lat_lang_pipeline = $postSearchAry['fOriginLatitude']."|".$postSearchAry['fOriginLongitude']."|".$searchResults['szFromWHSLat']."|".$searchResults['szFromWHSLong']."|".$searchResults['iDistanceFromWHS']."|".$szOriginAddress."|".$szDestinationAddress;
						
						$szOriginAddress2 =  $postSearchAry['szDestinationPostCode']." - ".$postSearchAry['szDestinationCity']."<br /> ".$postSearchAry['szDestinationCountry'];
		                $szDestinationAddress2 = $searchResults['szToWHSPostCode']." - ".$searchResults['szToWHSCity']."<br />".$searchResults['szToWHSCountry'];
						$destination_lat_lang_pipeline = $postSearchAry['fDestinationLatitude']."|".$postSearchAry['fDestinationLongitude']."|".$searchResults['szToWHSLat']."|".$searchResults['szToWHSLong']."|".$searchResults['iDistanceToWHS']."|".$szOriginAddress2."|".$szDestinationAddress2;
					}
					
					$szWarehousecityFrom = $searchResults['szFromWHSCity'];
					$szWarehousecityTo = $searchResults['szToWHSCity'];
					
					$fTotalPriceDisplay = $searchResults['fDisplayPrice'];
					?>						
					<tr id="select_services_tr_<?=$tr_counter?>">
						<td><a href="javascript:void(0)" onclick="openForwarderRatingPopUp('<?=$searchResults['idForwarder']?>')" style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;">
							<?php 
							if(!empty($searchResults['szLogo'])){ ?>
								<img src="<?=__BASE_STORE_INC_URL__.'/image.php?img='.$searchResults['szLogo']."&w=".__MAX_ALLOWED_FORWARDER_LOGO_WIDTH__."&h=".__MAX_ALLOWED_FORWARDER_LOGO_HEIGHT__?>" alt="<?=$searchResults['szDisplayName']?>" border="0" />
							<?php }else {
								echo (strlen($searchResults['szDisplayName'])<=22)?$searchResults['szDisplayName']:mb_substr($searchResults['szDisplayName'],0,20,'UTF8')."..";
							 }
							 ?>
							</a>
						</td>
						<td>
							<a href="javascript:void(0)" onclick="openForwarderRatingPopUp('<?=$searchResults['idForwarder']?>')" style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;">
                                                            <?php 
								if($searchResults['iNumRating']<$minRatingToShowStars){ 
									echo 'N/A'.'<br/>'; 
								}else {									
									// one filled star's width is 10px so that we are multiplying Avg Rating with 10 
									$fAvgRating = round(($searchResults['fAvgRating']*10),2);	
									echo displayRatingStar($fAvgRating);
								}
							?>
							<span><?=(int)$searchResults['iNumRating']?> Reviews</a></span>
						</td>
						<td>
							<a href="javascript:void(0)" <?=$google_map_anchor_style?> onclick="open_google_map_popup('<?=$origin_lat_lang_pipeline?>');"><?=number_format((float)$searchResults['iDistanceFromWHS'])?> Km</a><br />
							<a href="javascript:void(0)" <?=$google_map_anchor_style?> onclick="open_google_map_popup('<?=$origin_lat_lang_pipeline?>');"><?=$szWarehousecityFrom?></a>
						</td>
						<td><?=str_replace(" ","<br />",trim($searchResults['dtCutOffDate']))?></td>
						<td><?=str_replace(" ","<br />",trim($searchResults['dtAvailableDate']))?></td>
						<td><?=$searchResults['szTotalTransportationTime']?></td>
						<td>
						<a href="javascript:void(0)" <?=$google_map_anchor_style?>  onclick="open_google_map_popup('<?=$destination_lat_lang_pipeline?>');"><?=number_format((float)$searchResults['iDistanceToWHS'])?> Km</a><br />
						<a href="javascript:void(0)" <?=$google_map_anchor_style?>  onclick="open_google_map_popup('<?=$destination_lat_lang_pipeline?>');"><?=$szWarehousecityTo?></a></td>
						<!--  <td><?=$searchResults['szCurrency']?><br /><?=number_format((float)$fTotalPriceDisplay,2)?><br /></td>-->
						<?php if(!$forwarder_flag){ ?>
						<td><?=$searchResults['szCurrency']?><br /><?=number_format((float)$searchResults['fDisplayPrice'],2)?><br /></td>
						<td>
							<a href="javascript:void(0)" onclick="display_forwarder_non_accepted_popup('<?=$searchResults['unique_id']?>','<?=$searchResults['idForwarder']?>','<?=$_SESSION['user_id']?>','<?=__BOOKING_DETAILS_PAGE_URL__?>');" class="button1"><span><?php echo t($t_base.'fields/select'); ?></span></a><br />
						<?php
							if(__DISPLAY_CALCULATION_DETAILS__)
							{
						?>
						<a href="<?=__BASE_URL__.'/ajax_displayCalculationDetails.php?idWTW='.$searchResults['unique_id'].'&booking_random_num='.$postSearchAry['szBookingRandomNum']?>" <?=$detail_style?> target="_blank"> Details</a>
						<?php
							}
						?>	
						</td>
						<? } else {
						
						// else part is called from forwarder try it now section. 
						?>
						<td>
							<a href="javascript:void(0)" onclick="display_fowarder_price_details('<?=$searchResults['unique_id']?>','select_services_tr_<?=$tr_counter?>')" style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;">
							<?=$searchResults['szCurrency']?><br /><?=number_format((float)$searchResults['fDisplayPrice'],2)?></a><br />
						</td>
					<?php
						}
					?>	
					</tr>
					<?php
					$tr_counter++;
				}
			}
			else
			{
				?>
				<tr>
					<td colspan="9"><h2><?=t($t_base.'messages/NO_SERVICE_MATCHING');?></h2> </td>
				</tr>
				<?
			}
		?>
	</table>
	<div id="rating_popup"></div>
	<?php
}

function display_wtd_services_list($postSearchAry,$searchResult,$t_base,$countryKeyValueAry,$forwarder_flag=false)  //WTD 
{
	$kWHSSearch=new cWHSSearch();
	
	$distance_1_header = t($t_base.'messages/DISTANCE_TO_WAREHOUSE_1_TOOL_TIP_HEADER_TEXT_1')." ".$postSearchAry['szOriginCity'].".".t($t_base.'messages/DISTANCE_TO_WAREHOUSE_1_TOOL_TIP_HEADER_TEXT_2');
	$distance_2_header = t($t_base.'messages/DISTANCE_TO_WAREHOUSE_2_TOOL_TIP_HEADER_TEXT_1')." ".$postSearchAry['szDestinationCity'].".".t($t_base.'messages/DISTANCE_TO_WAREHOUSE_2_TOOL_TIP_HEADER_TEXT_2');
	$detail_style="style='text-decoration:none;color:white;'" ;
	?>
	<table cellpadding="0" id="service_table"  cellspacing="0" border="0" class="format-1" width="100%">
		<tr>
			<th width="12%" align="center" valign="top"><?=t($t_base.'fields/forwarder')?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/FORWARDER_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/FORWARDER_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></th>
			<th width="11%" align="center" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=t($t_base.'fields/average')?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/AVG_RATING_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/AVG_RATING_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a><br /> <?=t($t_base.'fields/rating');?></th>
			<th width="11%" align="center" valign="top">&nbsp;&nbsp;&nbsp;<?=t($t_base.'fields/distance_to');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=$distance_1_header?>','<?=t($t_base.'messages/DISTANCE_TO_WAREHOUSE_1_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a><br /> <?=t($t_base.'fields/warehouse');?></th>
			<th width="11%" align="center" valign="top">&nbsp;&nbsp;&nbsp;<?=t($t_base.'fields/warehouse');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/WAREOUSE_CUTOFF_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/WAREOUSE_CUTOFF_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a><br />  <?=t($t_base.'fields/cut_off');?></th>
			<th width="11%" align="center" valign="top">&nbsp;&nbsp;&nbsp;<?=t($t_base.'fields/expected');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/DELIVERY_FROM_TOOL_TIP_TEXT');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a><br /> <?=t($t_base.'fields/delivery');?></th>
			<th width="11%" align="center" valign="top">&nbsp;&nbsp;<?=t($t_base.'fields/transport');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/TRANSPORTATION_TIME_TOOL_TIP_TEXT');?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a><br /> <?=t($t_base.'fields/ation_time');?></th>
			<th width="11%" align="center" valign="top"><?=t($t_base.'fields/price');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/PRICE_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/PRICE_TOOL_TIP_TEXT');?>','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></th>
			<? if(!$forwarder_flag){?>
			<th width="11%" align="center"></th>
			<?
				}
			?>
		</tr>
		<?php
			if(!empty($searchResult))
			{
				$google_map_anchor_style="style='text-decoration:none;color:black;font-style:normal;font-size:12px;'";
				$minRatingToShowStars = $kWHSSearch->getManageMentVariableByDescription('__MINIMUM_RATING_TO_SHOW_RATING_STARS__');
				$tr_counter = 1;
				foreach($searchResult as $searchResults)
				{
					if($forwarder_flag)
					{
						$postSearchAry = array();
						$postSearchAry = $_SESSION['try_it_out_searched_data'] ;
						
						$originPostCodeAry=array();
						$kConfig = new cConfig();
						$originPostCodeAry = $kConfig->getPostCodeDetails($postSearchAry['szOriginCountry'],$postSearchAry['szOriginPostCode'],$postSearchAry['szOriginCity']);
						
						$szOriginAddress =  $postSearchAry['szOriginPostCode']." - ".$postSearchAry['szOriginCity']."<br /> ".$originPostCodeAry['szCountry'];
					    $szDestinationAddress = $searchResults['szFromWHSPostCode']." - ".$searchResults['szFromWHSCity']."<br />".$searchResults['szFromWHSCountry'];
					    $origin_lat_lang_pipeline = $originPostCodeAry['szLatitude']."|".$originPostCodeAry['szLongitude']."|".$searchResults['szFromWHSLat']."|".$searchResults['szFromWHSLong']."|".$searchResults['iDistanceToWHS']."|".$szOriginAddress."|".$szDestinationAddress;
					}
					else
					{
						$szOriginAddress =  $postSearchAry['szOriginPostCode']." - ".$postSearchAry['szOriginCity']."<br /> ".$postSearchAry['szOriginCountry'];
		                $szDestinationAddress = $searchResults['szFromWHSPostCode']." - ".$searchResults['szFromWHSCity']."<br />".$searchResults['szFromWHSCountry'];
		    
						$origin_lat_lang_pipeline = $postSearchAry['fOriginLatitude']."|".$postSearchAry['fOriginLongitude']."|".$searchResults['szFromWHSLat']."|".$searchResults['szFromWHSLong']."|".$searchResults['iDistanceFromWHS']."|".$szOriginAddress."|".$szDestinationAddress;
					}
					$szWarehousecityFrom = $searchResults['szFromWHSCity'];
							
					?>						
					<tr id="select_services_tr_<?=$tr_counter?>">
						<td>
							<a href="javascript:void(0)" onclick="openForwarderRatingPopUp('<?=$searchResults['idForwarder']?>')" style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;">
							<? if(!empty($searchResults['szLogo'])) {?>
								<img src="<?=__BASE_STORE_INC_URL__.'/image.php?img='.$searchResults['szLogo']."&w=".__MAX_ALLOWED_FORWARDER_LOGO_WIDTH__."&h=".__MAX_ALLOWED_FORWARDER_LOGO_HEIGHT__?>" alt="<?=$searchResults['szDisplayName']?>" border="0" />
								<? } else {
								echo (strlen($searchResults['szDisplayName'])<=22)?$searchResults['szDisplayName']:mb_substr($searchResults['szDisplayName'],0,20,'UTF8')."..";
							 }?>
							</a>
						</td>
						<td>
							<a href="javascript:void(0)" onclick="openForwarderRatingPopUp('<?=$searchResults['idForwarder']?>')" style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;"><? 
								if($searchResults['iNumRating']<$minRatingToShowStars){ 
									echo 'N/A'.'<br/>';  
								}else {
									// one filled star's width is 10px so that we are multiplying Avg Rating with 10 
									$fAvgRating = round(($searchResults['fAvgRating']*10),2);	
									echo displayRatingStar($fAvgRating);
								}
							?>
							<span><?=(int)$searchResults['iNumRating']?> Reviews</a></span>
						</td>
						<td><a href="javascript:void(0)" <?=$google_map_anchor_style?> onclick="open_google_map_popup('<?=$origin_lat_lang_pipeline?>');"><?=number_format((float)$searchResults['iDistanceFromWHS'])?> Km</a><br/>
							<a href="javascript:void(0)" <?=$google_map_anchor_style?> onclick="open_google_map_popup('<?=$origin_lat_lang_pipeline?>');"><?=$szWarehousecityFrom?></a>
						</td>
						<td><?=str_replace(" ","<br />",trim($searchResults['dtCutOffDate']))?></td>
						<td><?=str_replace(" ","<br />",trim($searchResults['dtAvailableDate']))?></td>
						<td><?=$searchResults['szTotalTransportationTime']?></td>
						<? if(!$forwarder_flag){ ?>
						<td><?=$searchResults['szCurrency']?><br /><?=number_format((float)$searchResults['fDisplayPrice'],2)?><br /></td>
						<td>
							<a href="javascript:void(0)" onclick="display_forwarder_non_accepted_popup('<?=$searchResults['unique_id']?>','<?=$searchResults['idForwarder']?>','<?=$_SESSION['user_id']?>','<?=__BOOKING_DETAILS_PAGE_URL__?>');" class="button1"><span><?php echo t($t_base.'fields/select'); ?></span></a><br />
						<?
							if(__DISPLAY_CALCULATION_DETAILS__)
							{
						?>
						<a href="<?=__BASE_URL__.'/ajax_displayCalculationDetails.php?idWTW='.$searchResults['unique_id'].'&booking_random_num='.$postSearchAry['szBookingRandomNum']?>" <?=$detail_style?> target="_blank"> Details</a>
						<?
							}
						?>	
						</td>
						<? } else {
						
						// else part is called from forwarder try it now section. 
						?>
						<td>
							<a href="javascript:void(0)" onclick="display_fowarder_price_details('<?=$searchResults['unique_id']?>','select_services_tr_<?=$tr_counter?>')" style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;">
							<?=$searchResults['szCurrency']?><br /><?=number_format((float)$searchResults['fDisplayPrice'],2)?></a><br />
						</td>
					<?
						}
					?>										
						</td>
					</tr>
					<?
					$tr_counter++;
				}
			}
			else
			{
				?>
				<tr>
					<td  colspan="8"><h2><?=t($t_base.'messages/NO_SERVICE_MATCHING');?></h2> </td>
				</tr>
				<?
			}
		?>
	</table>
	<div id="rating_popup"></div>
	<?
}


function display_dtw_services_list($postSearchAry,$searchResult,$t_base,$countryKeyValueAry,$forwarder_flag=false)  //DTW
{ 
	$kWHSSearch=new cWHSSearch();
	
	$distance_1_header = t($t_base.'messages/DISTANCE_TO_WAREHOUSE_1_TOOL_TIP_HEADER_TEXT_1')." ".$postSearchAry['szOriginCity'].".".t($t_base.'messages/DISTANCE_TO_WAREHOUSE_1_TOOL_TIP_HEADER_TEXT_2');
	$distance_2_header = t($t_base.'messages/DISTANCE_TO_WAREHOUSE_2_TOOL_TIP_HEADER_TEXT_1')." ".$postSearchAry['szDestinationCity'].".".t($t_base.'messages/DISTANCE_TO_WAREHOUSE_2_TOOL_TIP_HEADER_TEXT_2');
	$detail_style="style='text-decoration:none;color:white;'" ;
	?>
	<table cellpadding="0" cellspacing="0" border="0" id="service_table" class="format-1" width="100%">
		<tr>
			<th width="12%" align="center" valign="top"><?=t($t_base.'fields/forwarder')?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/FORWARDER_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/FORWARDER_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></th>
			<th width="11%" align="center" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=t($t_base.'fields/average')?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/AVG_RATING_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/AVG_RATING_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a><br /> <?=t($t_base.'fields/rating');?></th>
			<th width="11%" align="center" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=t($t_base.'fields/expected');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/WAREOUSE_PICKUP_AFTER_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/WAREOUSE_PICKUP_AFTER_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a><br />  <?=t($t_base.'fields/pick_up');?></th>
			<th width="11%" align="center" valign="top">&nbsp;&nbsp;&nbsp;<?=t($t_base.'fields/available');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/AVAILABLE_FROM_TOOL_TIP_TEXT');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a><br /> <?=t($t_base.'fields/from');?></th>
			<th width="11%" align="center" valign="top">&nbsp;&nbsp;&nbsp;<?=t($t_base.'fields/transport');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/TRANSPORTATION_TIME_TOOL_TIP_TEXT');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a><br /> <?=t($t_base.'fields/ation_time');?></th>
			<th width="11%" align="center" valign="top">&nbsp;&nbsp;<?=t($t_base.'fields/distance_to');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=$distance_2_header?>','<?=t($t_base.'messages/DISTANCE_TO_WAREHOUSE_2_TOOL_TIP_TEXT');?>','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a><br /> <?=t($t_base.'fields/warehouse');?></th>
			<th width="11%" align="center" valign="top"><?=t($t_base.'fields/price');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/PRICE_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/PRICE_TOOL_TIP_TEXT');?>','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></th>
			<?
				if(!$forwarder_flag)
				{
			?>
			<th width="11%" align="center"></th>
			<? }?>
		</tr>
		<?php
			if(!empty($searchResult))
			{
				$google_map_anchor_style="style='text-decoration:none;color:black;font-style:normal;font-size:12px;'";
				$minRatingToShowStars = $kWHSSearch->getManageMentVariableByDescription('__MINIMUM_RATING_TO_SHOW_RATING_STARS__');
				//echo $minRatingToShowStars ;
				$tr_counter = 1;
				foreach($searchResult as $searchResults)
				{
					//$destination_lat_lang_pipeline = $postSearchAry['fDestinationLatitude']."|".$postSearchAry['fDestinationLongitude']."|".$searchResults['szToWHSLat']."|".$searchResults['szToWHSLong']."|".$searchResults['iDistanceToWHS'];
					
					if($forwarder_flag)
					{
						$postSearchAry = array();
						$postSearchAry = $_SESSION['try_it_out_searched_data'] ;
						
						$kConfig = new cConfig();
						
					    $destinationPostCodeAry=array();
						$destinationPostCodeAry = $kConfig->getPostCodeDetails($postSearchAry['szDestinationCountry'],$postSearchAry['szDestinationPostCode'],$postSearchAry['szDestinationCity']);
						
						$szOriginAddress2 =  $postSearchAry['szDestinationPostCode']." - ".$postSearchAry['szDestinationCity']."<br /> ".$destinationPostCodeAry['szCountry'];
					    $szDestinationAddress2 = $searchResults['szToWHSPostCode']." - ".$searchResults['szToWHSCity']."<br />".$searchResults['szToWHSCountry'];
					
					    $destination_lat_lang_pipeline = $destinationPostCodeAry['szLatitude']."|".$destinationPostCodeAry['szLongitude']."|".$searchResults['szToWHSLat']."|".$searchResults['szToWHSLong']."|".$searchResults['iDistanceToWHS']."|".$szOriginAddress2."|".$szDestinationAddress2;
					    
					    $onclick_function ='onclick="select_obo_lcl_tr(\'select_services_tr_'.$tr_counter.'\')"' ;
					}
					else
					{
						$szOriginAddress2 =  $postSearchAry['szDestinationPostCode']." - ".$postSearchAry['szDestinationCity']."<br /> ".$postSearchAry['szDestinationCountry'];
		                $szDestinationAddress2 = $searchResults['szToWHSPostCode']." - ".$searchResults['szToWHSCity']."<br />".$searchResults['szToWHSCountry'];
						$destination_lat_lang_pipeline = $postSearchAry['fDestinationLatitude']."|".$postSearchAry['fDestinationLongitude']."|".$searchResults['szToWHSLat']."|".$searchResults['szToWHSLong']."|".$searchResults['iDistanceToWHS']."|".$szOriginAddress2."|".$szDestinationAddress2;
					}
					
					$szWarehousecityTo = $searchResults['szToWHSCity'];
					
					?>						
					<tr id="select_services_tr_<?=$tr_counter?>">
						<td>
						<a href="javascript:void(0)" onclick="openForwarderRatingPopUp('<?=$searchResults['idForwarder']?>')" style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;">
							<? if(!empty($searchResults['szLogo'])) {?>
							<img src="<?=__BASE_STORE_INC_URL__.'/image.php?img='.$searchResults['szLogo']."&w=".__MAX_ALLOWED_FORWARDER_LOGO_WIDTH__."&h=".__MAX_ALLOWED_FORWARDER_LOGO_HEIGHT__?>" alt="<?=$searchResults['szDisplayName']?>" border="0" />
							<? } else {
							echo (strlen($searchResults['szDisplayName'])<=22)?$searchResults['szDisplayName']:mb_substr($searchResults['szDisplayName'],0,20,'UTF8')."..";
							}?>
						</a>
						</td>
						<td>
							<a href="javascript:void(0)" onclick="openForwarderRatingPopUp('<?=$searchResults['idForwarder']?>')" style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;"><? 
								if($searchResults['iNumRating']<$minRatingToShowStars){ 
									echo 'N/A'.'<br/>'; 
								}else {
									// one filled star's width is 10px so that we are multiplying Avg Rating with 10 
									$fAvgRating = round(($searchResults['fAvgRating']*10),2);	
									echo displayRatingStar($fAvgRating);
								}
							?>
								<span><?=(int)$searchResults['iNumRating']?> Reviews</a></span>
						</td>
						<td><?=str_replace(" ","<br />",trim($searchResults['dtCutOffDate']))?></td>
						<td><?=str_replace(" ","<br />",trim($searchResults['dtAvailableDate']))?></td>
						<td><?=$searchResults['szTotalTransportationTime']?></td>
						<td><a href="javascript:void(0)" <?=$google_map_anchor_style?> onclick="open_google_map_popup('<?=$destination_lat_lang_pipeline?>');"><?=number_format((float)$searchResults['iDistanceToWHS'])?> Km</a><br />
						<a href="javascript:void(0)" <?=$google_map_anchor_style?> onclick="open_google_map_popup('<?=$destination_lat_lang_pipeline?>');"><?=$szWarehousecityTo?></a></td>
						<? if(!$forwarder_flag){ ?>
						<td><?=$searchResults['szCurrency']?><br /><?=number_format((float)$searchResults['fDisplayPrice'],2)?><br /></td>
						<td>
							<a href="javascript:void(0)" onclick="display_forwarder_non_accepted_popup('<?=$searchResults['unique_id']?>','<?=$searchResults['idForwarder']?>','<?=$_SESSION['user_id']?>','<?=__BOOKING_DETAILS_PAGE_URL__?>');" class="button1"><span><?php echo t($t_base.'fields/select'); ?></span></a><br />
						<?
							if(__DISPLAY_CALCULATION_DETAILS__)
							{
						?>
						<a href="<?=__BASE_URL__.'/ajax_displayCalculationDetails.php?idWTW='.$searchResults['unique_id'].'&booking_random_num='.$postSearchAry['szBookingRandomNum']?>" <?=$detail_style?> target="_blank"> Details</a>
						<?
							}
						?>	
						</td>
						<? } else {
						
						// else part is called from forwarder try it now section. 
						?>
						<td>
							<a href="javascript:void(0)" onclick="display_fowarder_price_details('<?=$searchResults['unique_id']?>','select_services_tr_<?=$tr_counter?>')" style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;">
							<?=$searchResults['szCurrency']?><br /><?=number_format((float)$searchResults['fDisplayPrice'],2)?></a><br />
						</td>
						<?
							}
						?>	
					</tr>
					<?
					$tr_counter++;
				}
			}
			else
			{
				?>
				<tr>
					<td colspan="8"><h2><?=t($t_base.'messages/NO_SERVICE_MATCHING');?></h2> </td>
				</tr>
				<?
			}
		?>
	</table>
	<div id="rating_popup"></div>
	<?
}

function display_dtd_services_list($postSearchAry,$searchResult,$t_base,$countryKeyValueAry=false,$forwarder_flag=false)  //DTD
{
	$kWHSSearch=new cWHSSearch(); 
	
	$distance_1_header = t($t_base.'messages/DISTANCE_TO_WAREHOUSE_1_TOOL_TIP_HEADER_TEXT_1')." ".$postSearchAry['szOrigingCity']." - ".$postSearchAry['szOriginPostCode']."."." ".t($t_base.'messages/DISTANCE_TO_WAREHOUSE_1_TOOL_TIP_HEADER_TEXT_2');
	$distance_2_header = t($t_base.'messages/DISTANCE_TO_WAREHOUSE_2_TOOL_TIP_HEADER_TEXT_1')." ".$postSearchAry['szDestinationCity']." - ".$postSearchAry['szDestinationPostCode']."."." ".t($t_base.'messages/DISTANCE_TO_WAREHOUSE_2_TOOL_TIP_HEADER_TEXT_2');
	$detail_style="style='text-decoration:none;color:white;'" ;
	?>
	<table cellpadding="0" id="service_table"  cellspacing="0" border="0" class="format-1" width="100%">
		<tr>
			<th width="12%" align="center" valign="top"><?=t($t_base.'fields/forwarder')?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/FORWARDER_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/FORWARDER_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></th>
			<th width="11%" align="center" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=t($t_base.'fields/average')?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/AVG_RATING_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/AVG_RATING_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a><br /> <?=t($t_base.'fields/rating');?></th>
			<th width="11%" align="center" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=t($t_base.'fields/expected');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/WAREOUSE_PICKUP_AFTER_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/WAREOUSE_PICKUP_AFTER_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a><br />  <?=t($t_base.'fields/pick_up');?></th>
			<th width="11%" align="center" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=t($t_base.'fields/expected');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/DELIVERY_FROM_TOOL_TIP_TEXT');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a><br /> <?=t($t_base.'fields/delivery');?></th>
			<th width="11%" align="center" valign="top">&nbsp;&nbsp;&nbsp;<?=t($t_base.'fields/transport');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/TRANSPORTATION_TIME_TOOL_TIP_TEXT');?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a><br /> <?=t($t_base.'fields/ation_time');?></th>
			<th width="11%" align="center" valign="top"><?=t($t_base.'fields/price');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/PRICE_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/PRICE_TOOL_TIP_TEXT');?>','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></th>
			<? if(!$forwarder_flag){?>
			<th width="11%"></th>
			<? }?>
		</tr>
		<?php
			if(!empty($searchResult))
			{
				$minRatingToShowStars = $kWHSSearch->getManageMentVariableByDescription('__MINIMUM_RATING_TO_SHOW_RATING_STARS__');
				$tr_counter = 1;
				foreach($searchResult as $searchResults)
				{
					if($searchResults['idWTW']==85)
					{
						//print_r($searchResults);
					}
					?>						
					<tr id="select_services_tr_<?=$tr_counter?>">
						<td>
							<a href="javascript:void(0)" onclick="openForwarderRatingPopUp('<?=$searchResults['idForwarder']?>')" style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;">
								<? if(!empty($searchResults['szLogo'])) {?>
								<img src="<?=__BASE_STORE_INC_URL__.'/image.php?img='.$searchResults['szLogo']."&w=".__MAX_ALLOWED_FORWARDER_LOGO_WIDTH__."&h=".__MAX_ALLOWED_FORWARDER_LOGO_HEIGHT__?>" alt="<?=$searchResults['szDisplayName']?>" border="0" />
								<? } else {
								echo (strlen($searchResults['szDisplayName'])<=22)?$searchResults['szDisplayName']:mb_substr($searchResults['szDisplayName'],0,20,'UTF8')."..";
								}
							?>
							</a>
						</td>
						<td>
							<a href="javascript:void(0)" onclick="openForwarderRatingPopUp('<?=$searchResults['idForwarder']?>')" style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;"><? 
								if($searchResults['iNumRating']<$minRatingToShowStars){ 
									echo 'N/A'.'<br/>';  
								}else {
									
									// one filled star's width is 10px so that we are multiplying Avg Rating with 10 
									$fAvgRating = round(($searchResults['fAvgRating']*10),2);	
									echo displayRatingStar($fAvgRating);
								}
							?>
							<span><?=(int)$searchResults['iNumRating']?> Reviews</a></span>
						</td>
						<td><?=str_replace(" ","<br />",trim($searchResults['dtCutOffDate']))?></td>
						<td><?=str_replace(" ","<br />",trim($searchResults['dtAvailableDate']))?></td>
						<td><?=$searchResults['szTotalTransportationTime']?></td>
						
						
						<?php if(!$forwarder_flag){ ?>
						<td><?=$searchResults['szCurrency']?><br /><?=number_format((float)$searchResults['fDisplayPrice'],2)?><br /></td>
						<td>
							<a href="javascript:void(0)" onclick="display_forwarder_non_accepted_popup('<?=$searchResults['unique_id']?>','<?=$searchResults['idForwarder']?>','<?=$_SESSION['user_id']?>','<?=__BOOKING_DETAILS_PAGE_URL__?>');" class="button1"><span><?php echo t($t_base.'fields/select'); ?></span></a><br />
						<?php
							if(__DISPLAY_CALCULATION_DETAILS__)
							{
						?>
						<a href="<?=__BASE_URL__.'/ajax_displayCalculationDetails.php?idWTW='.$searchResults['unique_id'].'&booking_random_num='.$postSearchAry['szBookingRandomNum']?>" <?=$detail_style?> target="_blank"> Details</a>
						<?php
							}
						?>	
						</td>
						<?php } else {
						
						// else part is called from forwarder try it now section. 
						?>
						<td>
							<a href="javascript:void(0)" onclick="display_fowarder_price_details('<?=$searchResults['unique_id']?>','select_services_tr_<?=$tr_counter?>')" style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;">
							<?=$searchResults['szCurrency']?><br /><?=number_format((float)$searchResults['fDisplayPrice'],2)?></a><br />
						</td>
					<?php
						}
					?>	
					</tr>
					<?php
					$tr_counter++;
				}
			}
			else
			{
				?>
				<tr>
                                        <td colspan="8"><h2><?=t($t_base.'messages/NO_SERVICE_MATCHING');?></h2> </td>
				</tr>
				<?php
			}
		?>
	</table>
	<div id="rating_popup"></div>
	<?php
}


function displayRatingStar($width=0)
{
	?>
	<div id="ratings-star">
	  <span style="width:<?=$width?>px;"></span>
	</div>	
	<?php
}

function display_select_service_slider($kWHSSearch,$postSearchAry,$t_base)
{
	$minprice=floor($kWHSSearch->fMinPriceForSlider);
	$maxprice=floor($kWHSSearch->fMaxPriceForSlider);
	
	$mintransitTime=floor(($kWHSSearch->fMinTransitHourForSlider/24)) ;
	$maxtransitTime=ceil(($kWHSSearch->fMaxTransitHourForSlider/24));
						
	$maxrating=(int)round($kWHSSearch->fMaxAvgRatingForSlider);
	//$minrating=(int)round($kWHSSearch->fMinAvgRatingForSlider);
	$minrating = 0;
	$mincutoff = $kWHSSearch->fMinCutOffForSlider ;
	$maxcutoff = $kWHSSearch->fMaxCutOffForSlider ;
	
	$strtotime_for_one_day = 86400 ;	
	//$mincutoff =(floor($mincutoff/$strtotime_for_one_day) * $strtotime_for_one_day);	
	//$maxcutoff =(ceil($maxcutoff/$strtotime_for_one_day) * $strtotime_for_one_day);
	
	$formated_mincutoff_date = date('d/m/Y',$mincutoff);
	$formated_maxcutoff_date = date('d/m/Y',$maxcutoff);
	
	if($minrating==0)
	{
		$minrating_text='N/A';
	}
	else
	{
		$minrating_text = $minrating;
	}
	if($maxrating==0)
	{
		$maxrating_text='N/A';
	}
	else
	{
		$maxrating_text = $maxrating;
	}
	$t_base = "SelectServiceLeftPane/";
	
	?>
				<h5><?=t($t_base.'fields/filter');?></h5>
				<span class="servicess_slider_header_text"><?=t($t_base.'fields/price');?></span><br />
				 <div class="layout-slider" style="width: 90%">
				<span class="filter_top_text" style="display:inline">
				 <span id="p_from"> <?=$postSearchAry['szCurrency']?> <?=number_format((float)$minprice)?></span> <?=t($t_base.'fields/to');?>  <span id="p_to"> <?=$postSearchAry['szCurrency']?> <?=number_format((float)$maxprice)?></span></span>
					      <input id="fTotalPrice" type="text" name="selectServiceAry[fTotalPrice]" value="<?=$minprice?>;<?=$maxprice?>" />
						  <span class="range_value_left"><?=$postSearchAry['szCurrency']?> <?=number_format((float)floor($minprice))?></span><span class="range_value_right" id="price_to"><?=$postSearchAry['szCurrency']?> <?=number_format((float)ceil($maxprice))?></span>
					    </div>
						
					    <script type="text/javascript" charset="utf-8">
					      jQuery("#fTotalPrice").slider({
					      	 from:<?=$minprice ?>,
					      	 to:<?=$maxprice?>,
					      	 step:1,
					      	 smooth: false,
					      	 round: 0,
					      	 dimension: "&nbsp;",
					      	 skin: "plastic",
					      	 onstatechange: function( value ){
					      	 	var limit_val = document.getElementById("fTotalPrice").value;
					      	 	if(limit_val!="")
					      	 	{
					      	 		var limit_val_arr = limit_val.split(";");
									document.getElementById("p_from").innerHTML='<?=$postSearchAry['szCurrency']?>'+" "+dollarAmount(limit_val_arr[0]);
									document.getElementById("p_to").innerHTML='<?=$postSearchAry['szCurrency']?>'+" "+dollarAmount(limit_val_arr[1]);
									//console.dir( this );
								}
								else
								{
									$("#fTotalPrice").attr("value",'<?=$minprice?>;<?=$maxprice?>');
								}	
							} 
					      });
					    </script> 
			   
				<?php
					//print_r($postSearchAry);
					if($postSearchAry['idTimingType']==1) // Ready at origin
					{
						if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType'] ==__SERVICE_TYPE_DTW__ || $postSearchAry['idServiceType'] ==__SERVICE_TYPE_DTP__) // 1-DTD , 2-DTW
						{
						/*
							if(!empty($postSearchAry['dtTimingDate']))
							{
								$mincutoff = strtotime($postSearchAry['dtTimingDate']);
								$pick_up_m_days = $kWHSSearch->getManageMentVariableByDescription('__PICK_UP_AFTER_M_DAYS__');
								$maxcutoff = strtotime($postSearchAry['dtTimingDate']. ' + '.(int)$pick_up_m_days.' day');
							}
*/
							?>
							<span class="servicess_slider_header_text"><?=t($t_base.'fields/pickup_after');?></span>
								<div class="layout-slider" style="width: 90%">
								<span class="filter_top_text" style="display:inline" id="cutoff_from"><?=$formated_mincutoff_date?>  <?=t($t_base.'fields/to');?> <?=$formated_maxcutoff_date?></span>
								    <input id="dtCutOffDate" type="hidden" name="selectServiceAry[dtCutOffDate]" value="<?=$mincutoff?>;<?=$maxcutoff?>" />
								    <span class="range_value_left" ><?=$formated_mincutoff_date?></span><span class="range_value_right" id="cutoff_to"><?=$formated_maxcutoff_date?></span>
								 </div>
								 <script type="text/javascript" charset="utf-8">
								    jQuery("#dtCutOffDate").slider({
								     from:<?=$mincutoff ?>,
								     to:<?=$maxcutoff?>,
								     step:<?=$strtotime_for_one_day?>,
								     smooth: false,
								     round: 0,
								     dimension: "&nbsp;",
								     skin: "plastic",
								     onstatechange: function( value ){
								      	 	var limit_val = document.getElementById("dtCutOffDate").value;
								      	 	if(limit_val!="")
								      	 	{
								      	 		var limit_val_arr = limit_val.split(";");
								      	
										      	//converting milliseconds to date string 
									      	 	var cutoff_from = convert_time_to_date(limit_val_arr[0]);
									      	 	var cutoff_to = convert_time_to_date(limit_val_arr[1]);
									      	 	
												document.getElementById("cutoff_from").innerHTML = cutoff_from +' <?=t($t_base.'fields/to');?> '+ cutoff_to;
												//document.getElementById("cutoff_to").innerHTML = cutoff_to;
											}
											else
											{
												$("#dtCutOffDate").attr("value",'<?=$mincutoff?>;<?=$maxcutoff?>');
											}	
										} 
								    });
								 </script>
							 
							<?php
						}
						else if($postSearchAry['idServiceType']==__SERVICE_TYPE_WTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_WTW__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_PTW__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_WTP__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_PTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_PTP__) // 3-WTD , 4-WTW
						{
							/*
							if(!empty($postSearchAry['dtTimingDate']))
							{
							//	$dateAry = explode("/",$postSearchAry['dtTiming']); 
								$dtTimingDate = $postSearchAry['dtTimingDate'] ;
								$mincutoff = strtotime($dtTimingDate);
								$pick_up_m_days = $kWHSSearch->getManageMentVariableByDescription('__CUTOFF_N_DAYS__');
								$maxcutoff = strtotime($dtTimingDate. ' + '.(int)$pick_up_m_days.' day');
							}
 							*/
							?>
							<span class="servicess_slider_header_text"><?=t($t_base.'fields/cut_off');?></span>
								<div class="layout-slider" style="width: 90%">
								<span class="filter_top_text" style="display:inline" id="cutoff_from"><?=$formated_mincutoff_date?>  <?=t($t_base.'fields/to');?> <?=$formated_maxcutoff_date?></span>
							      <input id="dtCutOffDate" type="hidden" name="selectServiceAry[dtCutOffDate]" value="<?=$mincutoff?>;<?=$maxcutoff?>" />
							       <span class="range_value_left" ><?=$formated_mincutoff_date?></span>
							       <span class="range_value_right" id="cutoff_to"><?=$formated_maxcutoff_date?></span>
							    </div>
							    <script type="text/javascript" charset="utf-8">
							      jQuery("#dtCutOffDate").slider({
							      	 from:<?=$mincutoff ?>,
							      	 to:<?=$maxcutoff?>,
							      	 step:<?=$strtotime_for_one_day?>,
							      	 smooth: false,
							      	 round: 0,
							      	 dimension: "&nbsp;",
							      	 skin: "plastic",
							      	 onstatechange: function( value ){
							      	 	var limit_val = document.getElementById("dtCutOffDate").value;
							      	 	if(limit_val!="")
							      	 	{
							      	 		var limit_val_arr = limit_val.split(";");
								      	 	//converting milliseconds to date string 
								      	 	var cutoff_from = convert_time_to_date(limit_val_arr[0]);
								      	 	var cutoff_to = convert_time_to_date(limit_val_arr[1]);
								      	 	
											document.getElementById("cutoff_from").innerHTML = cutoff_from +" "+'<?=t($t_base.'fields/to');?>'+" "+cutoff_to ;
											//document.getElementById("cutoff_to").innerHTML = cutoff_to;
										}
										else
										{
											$("#dtCutOffDate").attr("value",'<?=$mincutoff?>;<?=$maxcutoff?>');
										}		
									} 
							      });
							    </script>
							
							<?php
						}
					}
					else if($postSearchAry['idTimingType']==2) // Available at Destination
					{
						if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTW__ || $postSearchAry['idServiceType'] ==__SERVICE_TYPE_DTP__ || $postSearchAry['idServiceType'] ==__SERVICE_TYPE_WTW__ || $postSearchAry['idServiceType'] ==__SERVICE_TYPE_WTP__ || $postSearchAry['idServiceType'] ==__SERVICE_TYPE_PTP__ || $postSearchAry['idServiceType'] ==__SERVICE_TYPE_PTW__)  // 2- DTW , 4- WTW
						{
							/*
						    if(!empty($postSearchAry['dtTimingDate']))
							{
								//$dateAry = explode("/",$postSearchAry['dtTiming']); 
								$dtTimingDate = $postSearchAry['dtTimingDate'];
								$mincutoff = time();
								$maxcutoff = strtotime($dtTimingDate);
							}
							*/
							?>
							<span class="servicess_slider_header_text"><?=t($t_base.'fields/available');?></span>
								<div class="layout-slider" style="width: 90%">
								<span class="filter_top_text" style="display:inline" id="cutoff_from"><?=$formated_mincutoff_date?>  <?=t($t_base.'fields/to');?> <?=$formated_maxcutoff_date?></span>
							      <input id="dtCutOffDate" type="hidden" name="selectServiceAry[dtCutOffDate]" value="<?=$mincutoff?>;<?=$maxcutoff?>" />
							      <span class="range_value_left"><?=$formated_mincutoff_date?></span><span class="range_value_right" id="cutoff_to"><?=$formated_maxcutoff_date?></span>
							    </div>
							    <script type="text/javascript" charset="utf-8">
							      jQuery("#dtCutOffDate").slider({
							      	 from:<?=$mincutoff ?>,
							      	 to:<?=$maxcutoff?>,
							      	 step:<?=$strtotime_for_one_day?>,
							      	 smooth: false,
							      	 round: 0,
							      	 dimension: "&nbsp;",
							      	 skin: "plastic" ,
							      	 onstatechange: function( value ){
							      	 	var limit_val = document.getElementById("dtCutOffDate").value;
							      	 	if(limit_val!="")
							      	 	{
							      	 		var limit_val_arr = limit_val.split(";");
								      	 	//converting milliseconds to date string 
								      	 	var cutoff_from = convert_time_to_date(limit_val_arr[0]);
								      	 	var cutoff_to = convert_time_to_date(limit_val_arr[1]);
								      	 	
											document.getElementById("cutoff_from").innerHTML = cutoff_from +" "+'<?=t($t_base.'fields/to');?>'+" "+cutoff_to ;
											//document.getElementById("cutoff_to").innerHTML = cutoff_to;
										}
										else
										{
											$("#dtCutOffDate").attr("value",'<?=$mincutoff?>;<?=$maxcutoff?>');
										}		
									 } 
							      	});
							    </script>
						    
							<?php
						}
						elseif($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType'] ==__SERVICE_TYPE_WTD__ || $postSearchAry['idServiceType'] ==__SERVICE_TYPE_PTD__) // 3-WTD , 1-DTD
						{
						   /*
						    if(!empty($postSearchAry['dtTimingDate']))
							{
								//$dateAry = explode("/",$postSearchAry['dtTiming']); 
								$dtTimingDate = $postSearchAry['dtTimingDate'];
								$mincutoff = time();
								$maxcutoff = strtotime($dtTimingDate);
							}
							*/
							?>
							<span class="servicess_slider_header_text"><?=t($t_base.'fields/delivery_before');?></span>
								<div class="layout-slider" style="width: 90%">
								<span class="filter_top_text" style="display:inline" id="cutoff_from"><?=$formated_mincutoff_date?>  <?=t($t_base.'fields/to');?> <?=$formated_maxcutoff_date?></span>
							      <input id="dtCutOffDate" type="hidden" name="selectServiceAry[dtCutOffDate]" value="<?=$mincutoff?>;<?=$maxcutoff?>" />
							      <span class="range_value_left" ><?=$formated_mincutoff_date?></span><span class="range_value_right" id="cutoff_to"><?=$formated_maxcutoff_date?></span>
							    </div>
							    <script type="text/javascript" charset="utf-8">
							      jQuery("#dtCutOffDate").slider({
							      	 from:<?=$mincutoff ?>,
							      	 to:<?=$maxcutoff?>,
							      	 step:<?=$strtotime_for_one_day?>,
							      	 smooth: false,
							      	 round: 0,
							      	 dimension: "&nbsp;",
							      	 skin: "plastic",
							      	 onstatechange: function( value ){
							      	 	var limit_val = document.getElementById("dtCutOffDate").value;
							      	 	if(limit_val!="")
							      	 	{
							      	 		var limit_val_arr = limit_val.split(";");
								      	 	//converting milliseconds to date string 
								      	 	var cutoff_from = convert_time_to_date(limit_val_arr[0]);
								      	 	var cutoff_to = convert_time_to_date(limit_val_arr[1]);
								      	 	
											document.getElementById("cutoff_from").innerHTML = cutoff_from +" "+'<?=t($t_base.'fields/to');?>'+" "+cutoff_to ;
											//document.getElementById("cutoff_to").innerHTML = cutoff_to;
										}
										else
										{
											$("#dtCutOffDate").attr("value",'<?=$mincutoff?>;<?=$maxcutoff?>');
										}	
									 }	
							      });
							    </script>							
							<?php
						}
					}
				?>
				
				<span class="servicess_slider_header_text"><?=t($t_base.'fields/transit_time');?></span>
					<div class="layout-slider" style="width: 90%">
					    <span class="filter_top_text" style="display:inline" id="transit_from"><?=$mintransitTime?> <?=t($t_base.'fields/days');?> <?=t($t_base.'fields/to');?> <?=$maxtransitTime?> <?=t($t_base.'fields/days');?></span>
					    <input id="iTransitTime" type="hidden" name="selectServiceAry[iTransitTime]" value="<?=$mintransitTime?>;<?=$maxtransitTime?>" />
					    <span class="range_value_left" ><?=$mintransitTime?> <?=t($t_base.'fields/days');?></span><span class="range_value_right" id="transit_to"><?=$maxtransitTime?> <?=t($t_base.'fields/days');?></span>
					  </div>
					    <script type="text/javascript" charset="utf-8">
					      jQuery("#iTransitTime").slider({
					      	 from:<?=$mintransitTime ?>, 
					      	 to:<?=$maxtransitTime?>,
					      	 step: 1,
					      	 smooth: false,
					      	 round: 0,
					      	 dimension: "&nbsp;",
					      	 skin: "plastic" ,
					      	 onstatechange: function( value ){
					      	 	var limit_val = document.getElementById("iTransitTime").value;
					      	 	if(limit_val!="")
					      	 	{
					      	 		var limit_val_arr = limit_val.split(";");    	 		
									document.getElementById("transit_from").innerHTML=limit_val_arr[0]+" "+'<?=t($t_base.'fields/days');?>'+" "+'<?=t($t_base.'fields/to');?>'+" "+limit_val_arr[1]+" "+'<?=t($t_base.'fields/days');?>';
									//document.getElementById("transit_to").innerHTML=limit_val_arr[1]+" Days";
								}
								else
								{
									$("#iTransitTime").attr("value",'<?=$mintransitTime?>;<?=$maxtransitTime?>');
								}	
							} 
					     });
					    </script>
				
				<span class="servicess_slider_header_text"><?=t($t_base.'fields/avg_rating');?></span>
					<div class="layout-slider" style="width: 90%">
					     <span class="filter_top_text" style="display:inline" id="rating_from"> <?='N/A'?> <?=t($t_base.'fields/to');?> <?=$maxrating?> </span>
					      <input id="iRatingReview" type="hidden" name="selectServiceAry[iRatingReview]" value="<?=$minrating?>;<?=$maxrating?>" />
						  <span class="range_value_left" ><?=$minrating_text?></span><span class="range_value_right" id="rating_to"><?=$maxrating?></span>					    
					    </div>
					    <script type="text/javascript" charset="utf-8">
					      jQuery("#iRatingReview").slider({
					       from:<?=$minrating ?>,
					       to:<?=$maxrating?>,
					       step: 1,
					       smooth: false,
					       round: 0,
					       dimension: "&nbsp;",
					       skin: "plastic",
					       onstatechange: function( value ){
					      	 	var limit_val = document.getElementById("iRatingReview").value;
					      	 	if(limit_val!="")
					      	 	{
					      	 		var limit_val_arr = limit_val.split(";");   
					      	 		
					      	 	    var rating_from = 'N/A';
					      	 		if(limit_val_arr[0]==0)
					      	 		{
					      	 			rating_from = 'N/A';
					      	 		} 
					      	 		else
					      	 		{
					      	 			rating_from = limit_val_arr[0] ;
					      	 		}	 		
									document.getElementById("rating_from").innerHTML=rating_from+" "+'<?=t($t_base.'fields/to');?>'+" "+limit_val_arr[1];
								}
								else
								{
									$("#iRatingReview").attr("value",'<?=$minrating_text?>;<?=$maxrating_text?>');
								}
							}  
					    });
					    </script>	
				
				<input type="hidden" name="selectServiceAry[searched_by]" value="" id="searched_by">
<?php } 

function display_services_requirements_left($postSearchAry,$cargoAry,$t_base,$booking_details=false)
{
	$kConfig=new cConfig();	
	// geting all service type 
	//$serviceTypeAry=$kConfig->getConfigurationData(__DBC_SCHEMATA_SERVICE_TYPE__,$postSearchAry['idServiceType']);
        $configLangArr=$kConfig->getConfigurationLanguageData('__TABLE_SERVICE_TYPE__');
        $serviceTypeAry = $configLangArr[$postSearchAry['idServiceType']];
	
	// geting all Timing type 
	//$timingTypeAry=$kConfig->getConfigurationData(__DBC_SCHEMATA_TIMING_TYPE__,$postSearchAry['idTimingType']);
        $configLangArr=$kConfig->getConfigurationLanguageData('__TABLE_TIMING_TYPE__');
        $timingTypeAry = $configLangArr[$postSearchAry['idTimingType']];
	
	$t_base = "SelectServiceLeftPane/";
?>
	<h5><?=t($t_base.'fields/service_requirements');?></h5>
				<p>
					<?php
						if($booking_details)   // should show pick-up date or available date on booking details page 
						{
							echo t($t_base.'fields/transportation').': '.$serviceTypeAry[0]['szShortName']."<br />";
														
							if($postSearchAry['idServiceType']== __SERVICE_TYPE_DTD__) // 1-DTD 
							{
								if(strlen($postSearchAry['szOriginCity'])>22)
								{
									$originCity=substr_replace($postSearchAry['szOriginCity'],'...',22,strlen($postSearchAry['szOriginCity']));
								}
								else
								{
									$originCity=$postSearchAry['szOriginCity'];
								}
								if(strlen($postSearchAry['szDestinationCity'])>22)
								{
									$desCity=substr_replace($postSearchAry['szDestinationCity'],'...',22,strlen($postSearchAry['szDestinationCity']));
								}
								else
								{
									$desCity=$postSearchAry['szDestinationCity'];
								}					
								echo t($t_base.'fields/from').": ".$originCity."<br />";		
								echo t($t_base.'fields/pickup_after').": <br/>".date('d/m/Y  H:i',strtotime($postSearchAry['dtCutOff']))."<br />";
								
								echo t($t_base.'fields/to_left').": ".$desCity."<br />"; 
								echo t($t_base.'fields/delivery_before').": <br/>".date('d/m/Y H:i',strtotime($postSearchAry['dtAvailable']))."<br />";
							}
							else if($postSearchAry['idServiceType']== __SERVICE_TYPE_DTW__ || $postSearchAry['idServiceType']== __SERVICE_TYPE_DTP__) // 2-DTW
							{	
								if(strlen($postSearchAry['szOriginCity'])>22)
								{
									$originCity=substr_replace($postSearchAry['szOriginCity'],'...',22,strlen($postSearchAry['szOriginCity']));
								}
								else
								{
									$originCity=$postSearchAry['szOriginCity'];
								}
								if(strlen($postSearchAry['szWarehouseToCity'])>22)
								{
									$desCity=substr_replace($postSearchAry['szWarehouseToCity'],'...',22,strlen($postSearchAry['szWarehouseToCity']));
								}
								else
								{
									$desCity=$postSearchAry['szWarehouseToCity'];
								}					
								echo t($t_base.'fields/from').": ".$originCity."<br />";		
								echo t($t_base.'fields/pickup_after').": <br/>".date('d/m/Y  H:i',strtotime($postSearchAry['dtCutOff']))."<br />";
								
								echo t($t_base.'fields/to_left').": ".$desCity."<br />";
								echo t($t_base.'fields/available').": <br/>".date('d/m/Y H:i',strtotime($postSearchAry['dtAvailable']))."<br />";
							}
							else if($postSearchAry['idServiceType']==__SERVICE_TYPE_WTD__ || $postSearchAry['idServiceType']== __SERVICE_TYPE_PTD__)  // 3- WTD
							{	
								if(strlen($postSearchAry['szWarehouseFromCity'])>22)
								{
									$originCity=substr_replace($postSearchAry['szWarehouseFromCity'],'...',22,strlen($postSearchAry['szWarehouseFromCity']));
								}
								else
								{
									$originCity=$postSearchAry['szWarehouseFromCity'];
								}
								if(strlen($postSearchAry['szDestinationCity'])>22)
								{
									$desCity=substr_replace($postSearchAry['szDestinationCity'],'...',22,strlen($postSearchAry['szDestinationCity']));
								}
								else
								{
									$desCity=$postSearchAry['szDestinationCity'];
								}							
								echo t($t_base.'fields/from').": ".$originCity."<br />";
								echo t($t_base.'fields/cut_off').": <br/>".date('d/m/Y H:i',strtotime($postSearchAry['dtCutOff']))."<br />";
							
								echo t($t_base.'fields/to_left').": ".$desCity."<br />";
								echo t($t_base.'fields/delivery_before').": <br/>".date('d/m/Y H:i',strtotime($postSearchAry['dtAvailable']))."<br />";
							}
							else if($postSearchAry['idServiceType']==__SERVICE_TYPE_WTW__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_PTP__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_PTW__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_WTP__) // WTW
							{
								if(strlen($postSearchAry['szWarehouseFromCity'])>22)
								{
									$originCity=substr_replace($postSearchAry['szWarehouseFromCity'],'...',22,strlen($postSearchAry['szWarehouseFromCity']));
								}
								else
								{
									$originCity=$postSearchAry['szWarehouseFromCity'];
								}
								if(strlen($postSearchAry['szWarehouseToCity'])>22)
								{
									$desCity=substr_replace($postSearchAry['szWarehouseToCity'],'...',22,strlen($postSearchAry['szWarehouseToCity']));
								}
								else
								{
									$desCity=$postSearchAry['szWarehouseToCity'];
								}	
								echo t($t_base.'fields/from').": ".$originCity."<br />";
								echo t($t_base.'fields/cut_off').": <br/>".date('d/m/Y H:i',strtotime($postSearchAry['dtCutOff']))."<br />";
								
								echo t($t_base.'fields/to_left').": ".$desCity."<br />";
								echo t($t_base.'fields/available').": <br/>".date('d/m/Y H:i',strtotime($postSearchAry['dtAvailable']))."<br />";
							}	
						}
						else  // Ready/Available only show on service page 
						{
							if(strlen($postSearchAry['szOriginCity'])>22)
							{
								$originCity=substr_replace($postSearchAry['szOriginCity'],'...',22,strlen($postSearchAry['szOriginCity']));
							}
							else
							{
								$originCity=$postSearchAry['szOriginCity'];
							}
							echo t($t_base.'fields/transportation').': '.$serviceTypeAry[0]['szShortName']."<br />";
							echo t($t_base.'fields/from').": ".$originCity."<br />";
							
							if($postSearchAry['idTimingType']==1)
							{
						?>
						<?=t($t_base.'fields/after');?>: <?=date('d/m/Y',strtotime($postSearchAry['dtTimingDate']))?><br />
						<?
							}
							else if($postSearchAry['idTimingType']==2)
							{
						?>
							<?=t($t_base.'fields/before');?>: <?=date('d/m/Y',strtotime($postSearchAry['dtTimingDate']))?><br />
						  <?
							}
							if(strlen($postSearchAry['szDestinationCity'])>22)
							{
								$desCity=substr_replace($postSearchAry['szDestinationCity'],'...',22,strlen($postSearchAry['szDestinationCity']));
							}
							else
							{
								$desCity=$postSearchAry['szDestinationCity'];
							}
							echo t($t_base.'fields/to_left').": ".$desCity."<br />";
						}
					  ?>					
					<?=t($t_base.'fields/customs_clearance');?>:<br/>
					<?php
						$cc_str = "" ;
						if($postSearchAry['iOriginCC']==1)
						{
							$cc_str = t($t_base.'fields/origin');
						}
						if($postSearchAry['iDestinationCC']==2)
						{
							if(!empty($cc_str))
							{
							   $cc_str .= " ".t($t_base.'fields/and')." ".t($t_base.'fields/destination') ;
							}else{
								$cc_str = t($t_base.'fields/destination');
							}
						}						
						if(!empty($cc_str))
							echo $cc_str ;
						else
							echo t($t_base.'fields/none')." ";	
					?>
					<? /* t($t_base.'fields/custom_cl_origin'); : $postSearchAry['iOriginCC']==1?'Yes':'No' <br />
					t($t_base.'fields/customs_cl_destination'); : $postSearchAry['iDestinationCC']==2?'Yes':'No' */
					?>
				</p>
				<br/>
				<h5><?=t($t_base.'fields/cargo_details');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/CARGO_DETAILS_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/CARGO_DETAILS_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></h5>
				<p>
				<?php
                                    $cargo_volume = format_volume($postSearchAry['fCargoVolume']); 
                                    $cargo_weight = get_formated_cargo_measure((float)($postSearchAry['fCargoWeight']/1000)) ;
				?>
                                    <?=t($t_base.'fields/volume');?>: <?=$cargo_volume." ".t($t_base.'fields/cbm')?><br />
                                    <?=t($t_base.'fields/weight');?>: <?=$cargo_weight." ".t($t_base.'fields/mt')?>
				</p>
	<?php
}


function display_service_not_found_html($t_base,$landing_page_url,$idBooking,$idExpactedService,$kBooking)
{
	$postSearchResult = $kBooking->getBookingDetails($idBooking);
	//clear_search_result('$landing_page_url')
	?>	
    <div id="popup-bg"></div>
    <form id="update_expacted_services_form" method="post" name="update_expacted_services_form" action="">
        <div id="popup-container">
            <div class="no_service_found_div popup" style="width:550px;TEXT-ALIGN:JUSTIFY;">
	<h5><?=nl2br(t($t_base.'messages/NO_RECORD_FOUND_POPUP_HEADER'));?></h5>
	<?php
	if(!empty($kBooking->arErrorMessages))
	{
	?>
	<div id="regError" class="errorBox" style="display:block;">
	<div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
	      foreach($kBooking->arErrorMessages as $key=>$values)
	      {
	      ?><li><?=$values?></li>
	      <?php 
	      }
	?>
	</ul>
	</div>
	</div>
	<?php
            }
	?>
	<p><?=t($t_base.'messages/NO_RECORD_FOUND_MESSAGE_1');?> </p><br/>
	<p><?=t($t_base.'messages/NO_RECORD_FOUND_MESSAGE_2');?> <?=$postSearchResult['szOriginCountry']?> <?=t($t_base.'messages/TO');?> <?=$postSearchResult['szDestinationCountry']?> <?=t($t_base.'messages/NO_RECORD_FOUND_MESSAGE_3');?></p>
	<br />
	<p>
		<?=t($t_base.'messages/send_notification_to');?> <input type="text" name="addExpactedServiceAry[szEmail]" value="<?=$postSearchResult['szEmail']?>" size="30">&nbsp;&nbsp;<a href="javascript:void(0);" style="font-size:18px;" onclick="update_expacted_services_email('<?=$landing_page_url?>');"><?=t($t_base.'fields/submit');?></a>
		&nbsp;&nbsp;&nbsp;<span id="success_message_expacted_services" style="color:green;font-weight:bolder;font-size:16px;"></span>
	</p>
	<br/>
	<p><?=t($t_base.'messages/NO_RECORD_FOUND_MESSAGE_4');?></p>
	<br/>
	<p align="center">
            <input type="hidden" name="addExpactedServiceAry[idBooking]" value="<?=$idBooking?>">
            <input type="hidden" name="addExpactedServiceAry[idExpactedService]" value="<?=$idExpactedService?>">
            <input type="hidden" name="addExpactedServiceAry[idNotificationType]" value="<?=__NOTIFICATION_TYPE_NO_SERVICE_COUNTRY_TO_COUNTRY___?>">

            <a href="javascript:void(0);" onclick="update_expacted_services('back','<?=$landing_page_url?>','<?=$postSearchResult['szBookingRandomNum']?>');" class="button1"><span><?=t($t_base.'fields/back');?></span></a>
            <a href="javascript:void(0);" onclick="update_expacted_services('clear','<?=$landing_page_url?>','<?=$postSearchResult['szBookingRandomNum']?>');" class="button1"><span><?=t($t_base.'fields/clear');?></span></a>
        </p>
    </div>		
</div>
</form>
    <?php
}
 
function confirmBookingAfterPayment($postSearchAry,$idBooking=false)
{
    $kBilling = new cBilling(); 
    $kForwarder = new cForwarder();
    $bookingRefLogAry = array();
    $kConfig = new cConfig();
    $kBooking = new cBooking();
    $iLanguage = getLanguageId();
    $idBooking = $postSearchAry['id'];
    $updateAry = array();
    $kForwarder->load($postSearchAry['idForwarder']);
    $iCreditDays= $kForwarder->iCreditDays;
    $szForwarderVatRegistrationNum= $kForwarder->szVatRegistrationNum;
    $bQuickQuoteBooking = false;
    if($postSearchAry['iQuickQuote']==__QUICK_QUOTE_MAKE_BOOKING__)
    {
        $bQuickQuoteBooking = true;
    }
    
    $iBookingType = $postSearchAry['iBookingType'];
    /*
    * Re-validating iBookingType Flag to make system robust
    */
    if($postSearchAry['idServiceProvider']>0 && $postSearchAry['iCourierBooking']==1)
    {
        $iBookingType = __BOOKING_TYPE_COURIER__ ;
    }
    else if($postSearchAry['iQuotesStatus']>0)
    {
        $iBookingType = __BOOKING_TYPE_RFQ__ ;
    }
    else
    {
        $iBookingType = __BOOKING_TYPE_AUTOMATIC__ ;
    } 
        
    if($postSearchAry['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_1__)
    {
        $dtBookingConfirmed = $kBooking->getRealNow(); 
        if($bQuickQuoteBooking)
        {
            $updateAry['iQuickQuote']=__QUICK_QUOTE_CONFIRMED_BOOKING__;
        } 
        else
        {
            //$kBooking->generateInvoice($postSearchAry['idForwarder'],true);
            $kAdmin  =new cAdmin();
            $szInvoice = $kAdmin->generateInvoiceForPayment(true);
            
            $invoiceNumber = ''; 
            $updateAry['szInvoice'] = $szInvoice;
            
            if($postSearchAry['iInsuranceIncluded']==1)
            { 
                /*
                * From now we are not 
                */
                //$szInsuranceInvoice = $kAdmin->generateInvoiceForPayment();
                $szInsuranceInvoice = $szInvoice;
            }
            $updateAry['szInsuranceInvoice'] = $szInsuranceInvoice;
        } 
        
        //$max_number_used_for_forwarder = $kForwarder->getMaximumUsedNumberByForwarder($postSearchAry['idForwarder']);
        
        $updateAry['dtBookingConfirmed'] = $dtBookingConfirmed;
        $updateAry['dtBooked'] = $dtBookingConfirmed; 
        $updateAry['idBookingStatus'] = 3 ;
        $updateAry['iBookingStep'] = 14 ;   
        $updateAry['iBookingType'] = $iBookingType; 
        $updateAry['iTransferConfirmed'] = 0;
        $updateAry['iCreditDays'] = $iCreditDays;
        $updateAry['szForwarderVatRegistrationNum']=$szForwarderVatRegistrationNum;
        if($postSearchAry['iRailTransport']==1 && $postSearchAry['idServiceType']!=__SERVICE_TYPE_DTD__)
        {
            $updateAry['idServiceType'] = __SERVICE_TYPE_DTD__;
            $updateAry['idServiceTerms'] = __SERVICE_TERMS_EXW__;
        } 
        if(!empty($updateAry))
        {
            foreach($updateAry as $key=>$value)
            {
                $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
            }
        }
        $update_query = rtrim($update_query,",");
        $kBooking->updateDraftBooking($update_query,$idBooking);
    }
    else
    {
        $updateAry = array(); 
        if($bQuickQuoteBooking)
        {
            $updateAry['iQuickQuote']=__QUICK_QUOTE_CONFIRMED_BOOKING__;
        } 
        $updateAry['iPaypayBookingConfirmed'] = 1 ;
        $updateAry['iTransferConfirmed'] = 0;
        $updateAry['iBookingType'] = $iBookingType;
        $updateAry['iCreditDays'] = $iCreditDays;
        $updateAry['szForwarderVatRegistrationNum']=$szForwarderVatRegistrationNum;
        if($postSearchAry['iRailTransport']==1 && $postSearchAry['idServiceType']!=__SERVICE_TYPE_DTD__)
        {
            $updateAry['idServiceType'] = __SERVICE_TYPE_DTD__;
            $updateAry['idServiceTerms'] = __SERVICE_TERMS_EXW__;
        }
        if(!empty($updateAry))
        {
            foreach($updateAry as $key=>$value)
            {
                $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
            }
        }
        $update_query = rtrim($update_query,",");
        $kBooking->updateDraftBooking($update_query,$idBooking);
    }

    $bookingDataArr=$kBooking->getExtendedBookingDetails($_SESSION['booking_id']);
    $kBooking->updateBookingFileAfterPaymentConfirm($bookingDataArr);
    $isManualCourierBooking = $bookingDataArr['isManualCourierBooking'];
    if($bookingDataArr['iBookingType']==__BOOKING_TYPE_RFQ__)
    { 
        $updateAry =array();
        $updateAry['iQuotesStatus'] = __BOOKING_QUOTES_STATUS_WON__ ;   

        if($bookingDataArr['iIncludeComments']==1)
        {
            $updateAry['szOtherComments'] = $bookingDataArr['szForwarderComment'];
        }

        $update_query = '';
        if(!empty($updateAry))
        {
            foreach($updateAry as $key=>$value)
            {
                $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
            }
        }
        $update_query = rtrim($update_query,",");
        $kBooking->updateDraftBooking($update_query,$_SESSION['booking_id']); 
        $iBookingQuotes = 1; 
    }
    else
    {
        $iBookingQuotes = 0;
    }

    if($bookingDataArr['iCourierBooking']==1)
    { 
        $bookingDataArr['szForwarderReference']=$bookingDataArr['szForwarderReference'];
    }

    if($postSearchAry['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_1__ && !$bQuickQuoteBooking)
    {
        if($bookingDataArr['iCourierBooking']==1)
        {
            $kCourierServices=new cCourierServices();
            $courierBookingArr=$kCourierServices->getCourierBookingData($bookingDataArr['id']);
            if($courierBookingArr[0]['iCourierAgreementIncluded']==0)
            {
                $courLabelFee=$courierBookingArr[0]['fBookingLabelFeeRate'];
                if($courierBookingArr[0]['idBookingLabelFeeCurrency']!=1)
                {
                    $courLabelFee=$courierBookingArr[0]['fBookingLabelFeeRate']*$courierBookingArr[0]['fBookingLabelFeeROE'];
                }
                $bookingDataArr['fLabelFeeUSD']=$courLabelFee;
            }
        }
        $kBooking->addrForwarderTranscaction($bookingDataArr);
    }
    else if($bQuickQuoteBooking)
    {
        //updating iTransferConfirm into tblforwardertransaction after successful payment
        $kAdmin = new cAdmin();
        $bookingTransactionAry = array();
        $bookingTransactionAry = $kAdmin->showBookingConfirmedTransaction($idBooking);
        
        if($isManualCourierBooking==1)
        {
            $kCourierServices=new cCourierServices();
            $courierBookingArr=$kCourierServices->getCourierBookingData($bookingDataArr['id']);
            if($courierBookingArr[0]['iCourierAgreementIncluded']==1)
            {
                $idQuotePriceDetails = $bookingDataArr['idQuotePricingDetails'];
                
                $data = array();
                $data['idBookingQuotePricing'] = $idQuotePriceDetails ;
                $quotePricingAry = array();
                $quotePricingAry = $kBooking->getAllBookingQuotesPricingDetails($data);
        
                if(!empty($quotePricingAry))
                {
                    $idBookingQuote = $quotePricingAry[1]['idQuote'];
                    $dtStatusUpdatedOnTime=$kBooking->getRealNow();
                    $res_ary = array();
                    $res_ary['szForwarderQuoteStatus'] = 'FQS2' ; 
                    $res_ary['szForwarderQuoteTask'] = '' ; 
                    $res_ary['dtStatusUpdatedOn'] = $dtStatusUpdatedOnTime;
                    $res_ary['iWonQuote'] = 1;
                    $res_ary['iClosed'] = 1;
                    $res_ary['iClosedForForwarder'] = 0;
                   
                    if(!empty($res_ary))
                    {
                        $update_query='';
                        foreach($res_ary as $key=>$value)
                        {           
                            $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                        }
                    } 
                    $update_query = rtrim($update_query,",");    
                    $kBooking->updateBookingQuotesDetails($update_query,$idBookingQuote); 
                    
                }
            }
        }

        if(!empty($bookingTransactionAry))
        {
            $idForwarderTransaction = $bookingTransactionAry[0]['id']; 
            $kBilling->changeTransferStatus($idForwarderTransaction,$bookingDataArr);
        }
    }
    
    /*
    * Now for the booking which is paid by using credit card/paypal, Systme marks the booking as Received
    */
    $kBilling = new cBilling();
    //$kBilling->markTransportecaBookingReceived($bookingDataArr);
    
    $szOriginCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idOriginCountry'],false,$iLanguage);
    $szOriginCountry = $szOriginCountryAry[$bookingDataArr['idOriginCountry']]['szCountryName'] ;

    $replace_ary = array();

    $idForwarder = $postSearchAry['idForwarder'] ;
    $customerServiceEmailAry = $kForwarder->getForwarderCustomerServiceEmail($idForwarder);
    $replace_ary['idUser'] = $postSearchAry['idUser'];
    $replace_ary['szFirstName'] = $postSearchAry['szFirstName'];
    $replace_ary['szLastName'] = $postSearchAry['szLastName'];
    $replace_ary['szEmail'] = $postSearchAry['szEmail'];
    $replace_ary['szForwarderDispName'] = $postSearchAry['szForwarderDispName'];
    $replace_ary['szForwarderCustServiceEmail'] = $customerServiceEmailAry[0];
    $replace_ary['szBookingRef'] = $postSearchAry['szBookingRef']; 
    $replace_ary['supportEmail'] = __STORE_SUPPORT_EMAIL__;        
    $replace_ary['szOriginCFSContact'] = $bookingDataArr['szWarehouseFromContactPerson'];;
    $replace_ary['szOriginCFSEmail'] = $bookingDataArr['szWarehouseFromEmail'];        
    $replace_ary['szShipperCompany'] = $bookingDataArr['szShipperCompanyName']; 
    $replace_ary['szOriginCountry'] = $szOriginCountry;
    $replace_ary['szDestinationCountry'] = $szDestinationCountry;
    
    if($bookingDataArr['iCourierBooking']==1)
    {
        $replace_ary['szName']=$postSearchAry['szFirstName']." ".$postSearchAry['szLastName'];
        $replace_ary['szSenderEmail']=$postSearchAry['szShipperEmail'];
        createEmail(__COURIER_BOOKING_CONFIRMATION_AND_INVOICE_EMAIL__, $replace_ary,$replace_ary['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__,true,$idBooking);
    }
    else
    {
        if($bookingDataArr['iBookingType']==__BOOKING_TYPE_RFQ__)
        {
            createEmail(__MANUAL_BOOKING_CONFIRMATION_EMAIL__, $replace_ary,$replace_ary['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__,true,$idBooking);
        }
        else
        {
            createEmail(__BOOKING_CONFIRMATION_EMAIL__, $replace_ary,$replace_ary['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__,true,$idBooking);
        } 
    }

    // creating email content for forwarder email 

    $idForwarder = $postSearchAry['idForwarder'];
    $kForwarder = new cForwarder(); 
    $acceptedByUserAry = $kForwarder->getTermsConditionAcceptedByUserDetails($idForwarder);

    $kForwarder->load($idForwarder);
    $iProfitType = $kForwarder->iProfitType;
    $replace_ary = array();
    $replace_ary['szForwarderDispName'] = $kForwarder->szDisplayName;

     $szControlPanelUrl = "http://".$kForwarder->szControlPanelUrl."/booking/".$_SESSION['booking_id']."__".md5(time())."/"; 

    $replace_ary['szLink'] = '<a href="'.$szControlPanelUrl.'"> CLICK HERE</a>';
    $replace_ary['szUrl'] = $szControlPanelUrl;
    $replace_ary['szTermsCoditionUrl'] = '<a href="http://'.$kForwarder->szControlPanelUrl.'/T&C/">Terms & Conditions</a>';	

    $replace_ary['szFirstName'] = utf8_encode($acceptedByUserAry['szFirstName']);
    $replace_ary['szLastName'] = utf8_encode($acceptedByUserAry['szLastName']);
    $replace_ary['dtAgreement'] = date('d/m/Y',strtotime($acceptedByUserAry['dtAgreement']));

    $kConfig = new cConfig();
    $szOriginCountryAry = array();
    $szDestinationCountryAry = array();
    
    if($iProfitType==1)
    {
        $kConfig = new cConfig();
        $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',1);
        $szForwarderTerms_text1 = $configLangArr[1]['szForwarderTermsText1'];
        $szForwarderTerms_text2 = $configLangArr[1]['szForwarderTermsText2'];
        $szForwarderTerms_text3 = $configLangArr[1]['szForwarderTermsText3'];
        $iTermsConditionForReferralForwarder =$szForwarderTerms_text1." ".$replace_ary['szTermsCoditionUrl']."".$szForwarderTerms_text2." ".$replace_ary['szFirstName']." ".$replace_ary['szLastName']." ".$szForwarderTerms_text3." ".$replace_ary['dtAgreement'];
        $replace_ary['szTermsConditionForReferralForwarderText']="<br>".$iTermsConditionForReferralForwarder.".<br>";
    }
    else
    {
        $replace_ary['szTermsConditionForReferralForwarderText']='';
    }

    if($bookingDataArr['iBookingType']==__BOOKING_TYPE_RFQ__)
    {
        $szOriginCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idOriginCountry']);
        $szDestinationCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idDestinationCountry']) ;

        $szOriginCountry = $szOriginCountryAry[$bookingDataArr['idOriginCountry']]['szCountryName'] ;
        $szDestinationCountry = $szDestinationCountryAry[$bookingDataArr['idDestinationCountry']]['szCountryName'] ;	

        $szOriginCountryStr = '';
        if(!empty($bookingDataArr['szShipperCity']))
        {
            $szOriginCountryStr = $bookingDataArr['szShipperCity'].", ";
        }
        $szOriginCountryStr .= $szOriginCountry ;

        $szDestinationCountryStr = '';
        if(!empty($bookingDataArr['szConsigneeCity']))
        {
            $szDestinationCountryStr = $bookingDataArr['szConsigneeCity'].", ";
        }
        $szDestinationCountryStr .= $szDestinationCountry ;
    }
    else
    {
        $szOriginCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idWarehouseFromCountry']);
        $szDestinationCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idWarehouseToCountry']) ;

        $szOriginCountry = $szOriginCountryAry[$bookingDataArr['idWarehouseFromCountry']]['szCountryName'] ;
        $szDestinationCountry = $szDestinationCountryAry[$bookingDataArr['idWarehouseToCountry']]['szCountryName'] ;	

        $szOriginCountryStr = '';
        if(!empty($bookingDataArr['szWarehouseFromCity']))
        {
            $szOriginCountryStr = $bookingDataArr['szWarehouseFromCity'].", ";
        }
        $szOriginCountryStr .= $szOriginCountry ;


        $szDestinationCountryStr = '';
        if(!empty($bookingDataArr['szWarehouseToCity']))
        {
            $szDestinationCountryStr = $bookingDataArr['szWarehouseToCity'].", ";
        }
        $szDestinationCountryStr .= $szDestinationCountry ;
    } 

    $replace_ary['szBookingRef'] = $postSearchAry['szBookingRef'];
    $replace_ary['supportEmail'] = __STORE_SUPPORT_EMAIL__;
    $replace_ary['szOriginCountry'] = $szOriginCountryStr;
    $replace_ary['szDestinationCountry'] = $szDestinationCountryStr;

    $email_address_forwarders_Arrs=$kForwarder->getForwarderCustomerServiceEmail($idForwarder,1);
    //$email_address_forwardersAry = format_fowarder_emails($email_address_forwarders_Arrs);
    //$forwarder_booking_email = $email_address_forwardersAry[0];

    if($postSearchAry['idTransportMode']>0)
    {
        $kConfig = new cConfig();
        $transportModeListAry = array();
        $transportModeListAry = $kConfig->getAllTransportMode(false,false,true); 

        $replace_ary['szBookingType'] = $transportModeListAry[$postSearchAry['idTransportMode']]['szLongName'] ;
    }
    else
    {
        $replace_ary['szBookingType'] = 'Seafreight';
    }

    $searchDataAry = array();
    $searchDataAry['idForwarder'] = $bookingDataArr['idForwarder'];
    $searchDataAry['idTransportMode'] = $bookingDataArr['idTransportMode'];
    $searchDataAry['idOriginCountry'] = $bookingDataArr['idOriginCountry'];
    $searchDataAry['idDestinationCountry'] = $bookingDataArr['idDestinationCountry'];
    $searchDataAry['iBookingSentFlag'] = '1';

    $forwarderContactAry = array(); 
    $kConfig = new cConfig();
    $forwarderContactAry = $kConfig->getForwarderContact($searchDataAry);

    if(($bookingDataArr['iBookingType']==__BOOKING_TYPE_COURIER__) || ($isManualCourierBooking==1 && $bookingDataArr['iBookingType']==__BOOKING_TYPE_RFQ__))
    { 
        $kCourierServices= new cCourierServices();
        $newCourierBookingAry=$kCourierServices->getAllNewBookingWithCourier($idBooking);

        $shipperDetailsArr=$kCourierServices->getShipperNameById($bookingArr['idShipperConsignee'],true);

        if((int)$newCourierBookingAry[0]['iCourierAgreementIncluded']==1)
        {
             $szSender=$newCourierBookingAry[0]['szDisplayName'];
        }
        else
        {    
            if($bookingArr['iBookingLanguage']==2)
              $szSender="Vi";
            else
                $szSender="we have";
        }

        //echo $customerServiceEmailStr;
        //die();
        $replace_ary=array();
        $replace_ary['szBookingRef']=$bookingDataArr['szBookingRef'];
        $replace_ary['iBookingLanguage']=$bookingDataArr['iBookingLanguage'];
        $replace_ary['szOriginCountry']=$bookingDataArr['szOriginCountry'];
        $replace_ary['szDestinationCountry']=$bookingDataArr['szDestinationCountry'];
        $replace_ary['szSender']=$szSender;
        $replace_ary['szShipperCompany']=$shipperDetailsArr['szShipperCompanyName'];
        $replace_ary['szSuuportEmail']=__STORE_SUPPORT_EMAIL__;
        $replace_ary['szSupportPhone']=__STORE_SUPPORT_PHONE_NUMBER__;
        $replace_ary['szForwarderDispName']=$bookingDataArr['szForwarderDispName'];
        $replace_ary['szForwarderCustServiceEmail']=$customerServiceEmailStr;
        $replace_ary['szName']=$bookingDataArr['szFirstName']." ".$bookingDataArr['szLastName'];
        $replace_ary['szEmail']=$bookingDataArr['szEmail'];     
        $replace_ary['szBooking'] = $bookingDataArr['szBookingRef'];
        $replace_ary['iAdminFlag']=1; 
        $replace_ary['idBooking'] = $idBooking; 

        //createEmail(__COURIER_BOOKING_CONFIRNATION_AFTER_TRANSFER_EMAIL__, $replace_ary,$bookingDataArr['szEmail'], "", __STORE_SUPPORT_EMAIL__,$bookingDataArr['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_CUSTOMER__);

        $courierBookingArr=$kCourierServices->getCourierBookingData($idBooking);
       //print_r($courierBookingArr);

        $kConfig = new cConfig();
        $data = array();
        $data['idForwarder'] = $bookingDataArr['idForwarder'];
        $data['idTransportMode'] = $bookingDataArr['idTransportMode'];
        $data['idOriginCountry'] = $bookingDataArr['idOriginCountry'];
        $data['idDestinationCountry'] = $bookingDataArr['idDestinationCountry'];

        $kForwarder = new cForwarder();
        $kForwarder->load($bookingDataArr['idForwarder']);
        $iProfitType = $kForwarder->iProfitType;
        
        $kBooking = new cBooking();
        $dtResponseTime = $kBooking->getRealNow();
        
        if((int)$courierBookingArr[0]['iCourierAgreementIncluded']==1)
        {
            $fileLogsAry = array();
            $fileLogsAry['szTransportecaStatus'] = "S7";
            $fileLogsAry['iLabelStatus'] = __LABEL_NOT_SEND_FLAG__;
            $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
            $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime; 
            $fileLogsAry['szForwarderBookingStatus'] = '';                                    
            $fileLogsAry['szForwarderBookingTask'] = 'T140'; 
            $fileLogsAry['idBooking'] = (int)$idBooking;
            $fileLogsAry['idUser'] = (int)$bookingDataArr['idUser'];  
            $fileLogsAry['szTransportecaTask'] = '';

           $kBooking->insertCourierBookingFileDetails($fileLogsAry); 
        }
        else
        {
            $fileLogsAry = array();
            $fileLogsAry['szTransportecaStatus'] = "S7";
            $fileLogsAry['iLabelStatus'] = __LABEL_NOT_SEND_FLAG__;
            $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
            $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime; 
            //$fileLogsAry['szForwarderBookingStatus'] = '';                                    
            $fileLogsAry['szTransportecaTask'] = 'T140';  
            $fileLogsAry['idBooking'] = (int)$idBooking;        
            $fileLogsAry['idUser'] = (int)$bookingDataArr['idUser']; 
            $kBooking->insertCourierBookingFileDetails($fileLogsAry);
                    
        } 

        $replace_ary=array(); 
        $szControlPanelUrl = "http://".$kForwarder->szControlPanelUrl."/booking/".$idBooking."__".md5(time())."/"; 

        $replace_ary['szLink'] = '<a href="'.$szControlPanelUrl.'"> CLICK HERE</a>';
        $replace_ary['szUrl'] = $szControlPanelUrl;


        $quotesAry =$kCourierServices->getAllBookingQuotesByFileByBookingId($idBooking);

        $szControlPanelLabelUrl='http://'.$kForwarder->szControlPanelUrl."/pendingQuotes/createLabel/".$idBooking."__".md5(time())."/";


        $replace_ary['szLabelLink'] = '<a href="'.$szControlPanelLabelUrl.'" target="_blank"> UPLOAD LABLES AND TRACKING NUMBER HERE</a>';
        $replace_ary['szLabelUrl'] = $szControlPanelLabelUrl;

        $replace_ary['szBookingRef']=$bookingDataArr['szBookingRef'];
        $replace_ary['iBookingLanguage']=$bookingDataArr['iBookingLanguage'];
        $replace_ary['szOriginCountry']=$bookingDataArr['szOriginCountry'];
        $replace_ary['szDestinationCountry']=$bookingDataArr['szDestinationCountry'];
        $replace_ary['szForwarderDispName']=$bookingDataArr['szForwarderDispName'];

        $replace_ary['szFirstName'] = html_entities_flag($acceptedByUserAry['szFirstName'],false);
        $replace_ary['szLastName'] = html_entities_flag($acceptedByUserAry['szLastName'],false);
        $replace_ary['dtAgreement'] = date('d/m/Y',strtotime($acceptedByUserAry['dtAgreement']));
        $replace_ary['szEmail']=$forwarderContactArys;   

        if($bookingArr['iCourierBookingType']==1)
        {
            $szBookingType="Courier";
        }
        else
        {
            $szBookingType="Courier";
        }        
        $replace_ary['szBookingType']=$szBookingType;

        $replace_ary['szTermsCoditionUrl'] = '<a href="http://'.$kForwarder->szControlPanelUrl.'/T&C/">Terms & Conditions</a>'; 
        $replace_ary['iAdminFlag']=1; 
        $replace_ary['idBooking'] = $idBooking;
        
        if($iProfitType==1)
        {
            $kConfig = new cConfig();
            $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',1);
            $szForwarderTerms_text1 = $configLangArr[1]['szForwarderTermsText1'];
            $szForwarderTerms_text2 = $configLangArr[1]['szForwarderTermsText2'];
            $szForwarderTerms_text3 = $configLangArr[1]['szForwarderTermsText3'];
            $iTermsConditionForReferralForwarder =$szForwarderTerms_text1." ".$replace_ary['szTermsCoditionUrl']."".$szForwarderTerms_text2." ".$replace_ary['szFirstName']." ".$replace_ary['szLastName']." ".$szForwarderTerms_text3." ".$replace_ary['dtAgreement'];
            $replace_ary['szTermsConditionForReferralForwarderText']="<br>".$iTermsConditionForReferralForwarder.".<br>";
        }
        else
        {
            $replace_ary['szTermsConditionForReferralForwarderText']='';
        }
        
        $kWhsSearch = new cWHSSearch();  
        $forwarderBookingEmail = $kWhsSearch->getManageMentVariableByDescription('__FORWARDER_BOOKING_CONFIRMATION_EMAIL__');  
        //$forwarderBookingEmail = __FINANCE_CONTACT_EMAIL__;
        
        if((int)$courierBookingArr[0]['iCourierAgreementIncluded']==1)
        {
            createEmail(__COURIER_BOOKING_RECEIVED_CREATE_LABEL_FORWARDER_EMAIL__, $replace_ary, $forwarderContactAry,'' , $forwarderBookingEmail,$bookingArr['idForwarder'] , $forwarderBookingEmail,__FLAG_FOR_FORWARDER__);
        }
        else 
        {
            
            /*
            * FW: E-mail change 19-01-2017
            */
            $forwarderContactNewAry=array();
            if(!empty($forwarderContactAry))
            {
                
                $ctr=0;
                foreach($forwarderContactAry as $forwarderContactArys)
                {
                    if(doNotSendMailToThisDomainEmail($forwarderContactArys))
                    {
                        $forwarderContactNewAry[$ctr]=$forwarderContactArys;
                        ++$ctr;
                    }
                }
            }
            if(!empty($forwarderContactNewAry)){
                createEmail(__FORWADER_COURIER_BOOKING_RECEIVED__, $replace_ary, $forwarderContactNewAry,'' , $forwarderBookingEmail,$bookingArr['idForwarder'] , $forwarderBookingEmail,__FLAG_FOR_FORWARDER__); 
            }
           /*
            * FW: E-mail change 19-01-2017
            */
            // createEmail(__CREATE_LABEL_FOR_COURIER_BY_TRANSPORTECA_EMAIL__, $replace_ary, __STORE_SUPPORT_EMAIL__,'' , $forwarderBookingEmail,$bookingArr['idForwarder'] , $forwarderBookingEmail,__FLAG_FOR_FORWARDER__);
        } 
    }
    else
    {    
        /*if($isManualCourierBooking==1 && $bookingDataArr['iBookingType']==__BOOKING_TYPE_RFQ__)
        { 
            $kBooking = new cBooking();
            $dtResponseTime = $kBooking->getRealNow();

            /*
            *   If forwarder has no automatic courier services online, then management makes labels
            *   If forwarder has automatic courier services online, and all has label included in pricing, then forwarder makes labels
            *   Otherwise, management makes labels 
            */
           /* $kCourierServices = new cCourierServices();
            $courierBookingArr=$kCourierServices->getCourierBookingData($idBooking);
             
            $kBooking = new cBooking();
            $dtResponseTime = $kBooking->getRealNow();
        
            if((int)$courierBookingArr[0]['iCourierAgreementIncluded']==1)
            { 
                $fileLogsAry = array();
                $fileLogsAry['szTransportecaStatus'] = "S7";
                $fileLogsAry['iLabelStatus'] = __LABEL_NOT_SEND_FLAG__;
                $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
                $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime; 
                $fileLogsAry['szForwarderBookingStatus'] = '';                                    
                $fileLogsAry['szForwarderBookingTask'] = 'T140'; 
                $fileLogsAry['idBooking'] = (int)$idBooking;
                $fileLogsAry['idUser'] = (int)$bookingDataArr['idUser'];  

               $kBooking->insertCourierBookingFileDetails($fileLogsAry); 
            }
            else
            {
                $fileLogsAry = array();
                $fileLogsAry['szTransportecaStatus'] = "S7";
                $fileLogsAry['iLabelStatus'] = __LABEL_NOT_SEND_FLAG__;
                $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
                $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime; 
                //$fileLogsAry['szForwarderBookingStatus'] = '';                                    
                $fileLogsAry['szTransportecaTask'] = 'T140';  
                $fileLogsAry['idBooking'] = (int)$idBooking;        
                $fileLogsAry['idUser'] = (int)$bookingDataArr['idUser']; 
                $kBooking->insertCourierBookingFileDetails($fileLogsAry);
            }
        }*/ 
        createEmail('__FORWARDER_BOOKING_CONFIRMATION_EMAIL__', $replace_ary,$forwarderContactAry ,' ', $forwarderBookingEmail,$replace_ary['idUser'], $forwarderBookingEmail,__FLAG_FOR_FORWARDER__,false,$idBooking);

        /* 
         * In Case of RFQ we do not send email confirmation to shipper we send this only in case of Automatic booking 
         */
        if($bookingDataArr['iBookingType']==__BOOKING_TYPE_AUTOMATIC__ || $bQuickQuoteBooking)
        {
            $szWareHouseMapStr = '';

            if(!empty($bookingDataArr['szWarehouseFromName']))
            { 
                $szWareHouseMapStr.=$bookingDataArr['szWarehouseFromName']." <br>";
            }
            if(!empty($bookingDataArr['szWarehouseFromAddress']))
            { 
                $szWareHouseMapStr .= $bookingDataArr['szWarehouseFromAddress'];
            }
            if(!empty($bookingDataArr['szWarehouseFromAddress2']))
            { 
                $szWareHouseMapStr .= ", ".$bookingDataArr['szWarehouseFromAddress2'];
            }
            if(!empty($bookingDataArr['szWarehouseFromAddress3']))
            { 
                $szWareHouseMapStr .= ", ".$bookingDataArr['szWarehouseFromAddress3'];
            }

            if(!empty($bookingDataArr['szWarehouseFromPostCode']) || !empty($bookingDataArr['szWarehouseFromCity']))
            {
                if(!empty($bookingDataArr['szWarehouseFromPostCode']))
                {
                    $szWareHouseFromStr.="<br> ".$bookingDataArr['szWarehouseFromPostCode'];
                    $szWareHouseMapStr .="<br> ".$bookingDataArr['szWarehouseFromPostCode'];
                }

                if(!empty($bookingDataArr['szWarehouseFromPostCode']) && !empty($bookingDataArr['szWarehouseFromCity']))
                {
                    $szWareHouseFromStr.=" ".$bookingDataArr['szWarehouseFromCity']."<br> ";
                    $szWareHouseMapStr .=" ".$bookingDataArr['szWarehouseFromCity']."<br> ";
                }
                else if(!empty($bookingDataArr['szWarehouseFromCity']))
                {
                    $szWareHouseFromStr.= "<br> ".$bookingDataArr['szWarehouseFromCity']."<br> ";
                    $szWareHouseMapStr .= "<br> ".$bookingDataArr['szWarehouseFromCity']."<br> ";
                }
                else if(!empty($bookingDataArr['szWarehouseFromPostCode']))
                {
                    $szWareHouseMapStr .= "<br /> ";
                    $szWareHouseFromStr .= "<br /> ";
                }
            }						
            if(!empty($bookingDataArr['szWarehouseFromCountry']) || !empty($bookingDataArr['szWarehouseFromState']))
            {
                if(!empty($bookingDataArr['szWarehouseFromState']))
                {
                    $szWareHouseFromStr.=$bookingDataArr['szWarehouseFromState'];
                    $szWareHouseMapStr .= $bookingDataArr['szWarehouseFromState'];
                }

                if(!empty($bookingDataArr['szWarehouseFromState']) && !empty($bookingDataArr['szWarehouseFromCountry']))
                {
                    $szWareHouseFromStr.=" ".$bookingDataArr['szWarehouseFromCountry']."<br> ";
                    $szWareHouseMapStr .=" ".$bookingDataArr['szWarehouseFromCountry']."<br> ";
                }
                else if(!empty($bookingDataArr['szWarehouseToCountry']))
                {
                    $szWareHouseFromStr.= $bookingDataArr['szWarehouseFromCountry']."<br> ";
                    $szWareHouseMapStr .= $bookingDataArr['szWarehouseFromCountry']."<br> ";
                }
            }

            $serviceTypeEnglishAry = $kConfig->getConfigurationData(__DBC_SCHEMATA_SERVICE_TYPE__,$bookingDataArr['idServiceType'],false,true);
            $configLangArr=$kConfig->getConfigurationLanguageData('__TABLE_SERVICE_TYPE__');
            $serviceTypeEnglishAry = $configLangArr[$bookingDataArr['idServiceType']];            
            $services_string = $serviceTypeEnglishAry['szDescription'];

            $cc_string = '';
            if((int)$postSearchAry['iOriginCC']==1)
            {
                $cc_string = t($t_base.'fields/and')." ".t($t_base.'fields/customs_clearance_at')." ".t($t_base.'fields/origin');
                $cc_string_english = "and customs clearance at origin";
            }								
            if((int)$postSearchAry['iDestinationCC']==2)
            {
                if(empty($cc_string))
                {
                    $cc_string = t($t_base.'fields/and')." ".t($t_base.'fields/customs_clearance_at')." ".t($t_base.'fields/destination');
                    $cc_string_english = "and customs clearance at destination";
                }
                else
                {
                    $cc_string.= " ".t($t_base.'fields/and')." ".t($t_base.'fields/destination');
                    $cc_string_english .= " and destination";
                }
            }

            $customerServiceEmailAry = array();
            $idForwarder = $bookingDataArr['idForwarder'] ;
            $customerServiceEmailAry = $kForwarder->getForwarderCustomerServiceEmail($idForwarder);

            $customerServiceEmailLinkAry = format_fowarder_emails($customerServiceEmailAry) ;
            $customerServiceEmailLink = $customerServiceEmailLinkAry[0];
            $customerServiceEmailStr = $customerServiceEmailLinkAry[1];

            $cargoDetailsAry = $kBooking->getCargoComodityDeailsByBookingId($bookingDataArr['id']);
            $szCargoCommodity = $cargoDetailsAry['1']['szCommodity'];

            $cargo_volume = format_volume($bookingDataArr['fCargoVolume']);
            $cargo_weight = round((float)$bookingDataArr['fCargoWeight']);							
            $szCargoDetails_str = $cargo_volume." cbm, ".$cargo_weight." kg, ".utf8_decode($szCargoCommodity);

            /*if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__)
            {
                $services_string = "EXW ".$bookingDataArr['szShipperCity'].", with delivery in ".$bookingDataArr['szConsigneeCity'].", including customs clearance. ";
            }
            else
            {
                $services_string = "FOB ".$bookingDataArr['szWarehouseFromCity'].", with delivery in ".$bookingDataArr['szConsigneeCity'].", including import customs clearance. ";
            }*/	 
            
            $services_string =display_service_type_description($bookingDataArr,__LANGUAGE_ID_ENGLISH__,true);

            $replace_ary = array();
            $replace_ary['szBookingRef'] = $bookingDataArr['szBookingRef'];
            $replace_ary['szFirstName'] = $bookingDataArr['szFirstName'] ;
            $replace_ary['szLastName'] = $bookingDataArr['szLastName'] ;

            $replace_ary['szShipperFirstName'] = $bookingDataArr['szShipperFirstName'] ;
            $replace_ary['szShipperLastName'] = $bookingDataArr['szShipperLastName'] ;

            $replace_ary['szForwarderDispName'] = $bookingDataArr['szForwarderDispName']; 
            $replace_ary['szServiceType'] = $services_string  ;
            $replace_ary['szShipperCompany'] = $bookingDataArr['szShipperCompanyName'] ;
            $replace_ary['szConsigneeCompany'] = $bookingDataArr['szConsigneeCompanyName'] ;
            $replace_ary['szCargoDetails'] = $szCargoDetails_str ;
            $replace_ary['szForwarderEmail'] = $customerServiceEmailStr; 

            $replace_ary['szOriginCFSContact'] = $bookingDataArr['szWarehouseFromContactPerson'];;
            $replace_ary['szOriginCFSEmail'] = $bookingDataArr['szWarehouseFromEmail'];

            if($bookingDataArr['idServiceType']==__SERVICE_TYPE_WTD__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_WTW__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_PTW__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_WTP__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_PTP__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_PTD__ )  // WTW=4 , WTD = 3
            {					
                $szCutoffAvailableString = "Please ensure that the cargo is delivered to: <br> ".nl2br($szWareHouseMapStr)."<br>Latest by cut-off: ".date('d/m/Y H:i',strtotime($bookingDataArr['dtCutOff'])) ;
                $replace_ary['szCutoffAvailableString'] = $szCutoffAvailableString ; 
            }
            else
            {
                $szCutoffAvailableString = $bookingDataArr['szForwarderDispName']." will contact you directly to arrange exact details for pick-up. You should expect to have the shipment ready for pick-up by ".date('d/m/Y',strtotime($bookingDataArr['dtCutOff'])).".";
                $replace_ary['szCutoffAvailableString'] = $szCutoffAvailableString ;
            } 

            $replace_ary['iAdminFlag']=1; 
            $replace_ary['idBooking'] = $idBooking; 
            
            $shipper_mail = $bookingDataArr['szShipperEmail'] ;
            $ccEmail = $bookingDataArr['szWarehouseFromEmail']; 
            $shipperEmailAry = array();
            $shipperEmailAry[0] = $shipper_mail ;
            $shipperEmailAry[1] = $ccEmail ;
            $replace_ary['szToEmail'] = $shipper_mail;
            $replace_ary['szCCEmail '] = $ccEmail;
            $ccEmail='';

            createEmail('__SHIPPER_CONFIRMATION_EMAIL_TEXT__', $replace_ary,$shipperEmailAry ,' ', __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__,false,$idBooking,false,$ccEmail);
        } 
    }
}

function display_booking_quotes_details_form($postSearchAry,$kRegisterShipCon,$mode=false,$bAutoPostForm=false)
{  
    $kRegisterShipCon_new = new cRegisterShipCon(); 
    $kConfig = new cConfig();
    $kBooking= new cBooking();
    $idBooking = $postSearchAry['id'] ;

    $t_base = "BookingDetails/";
    $t_base_user = "Users/AccountPage/"; 

    $iLanguage = getLanguageId(); 
    $idUserSession = $_SESSION['user_id'];

    if($mode=='GET_RATES')
    {
        $szButtonText = t($t_base.'fields/see_price_now') ;
        $szBeforeLoginText = '';
    }
    else
    {
        $szButtonText = t($t_base.'fields/get_offer_now') ;
        $szBeforeLoginText = t($t_base.'fields/recieve_price_texts');
    }
    $redirect_url = __MAIN_SITE_HOME_PAGE_URL__.$_SERVER['REQUEST_URI']; 
    
    
    $_SESSION['redirect_url_site']=$redirect_url;
    
    ?>    
    <form action="" id="booking_quote_contact_detail_form" name="booking_quote_contact_detail_form" action="javascript:void(0);" onsubmit="return false;" method="post"> 
        <div id="booking-quotes-form-container">
            <?php 
                echo display_booking_quotes_contact_details($postSearchAry,$allCountriesArr,$dialUpCodeAry,$shipperConsigneeAry,$kRegisterShipCon);
            ?>
        </div>  
        <script type="text/javascript">
            Custom.init();
        </script>
        <input type="hidden" id="szOperationType" name="bookingQuotesAry[szOperationType]" value="BOOKING_QUOTE_PAGE">
        <input type="hidden" name="bookingQuotesAry[szBookingRandomNum]" id="szBookingRandomNum_booking_details" value=""> 
        <?php if($_SESSION['user_id']<=0){ /* ?>
            <p class="read-more"><a href="javascript:void(0)" onclick="ajaxLogin('<?php echo $redirect_url; ?>','','<?php echo $postSearchAry['szBookingRandomNum'] ?>','<?php echo $mode; ?>');"><?php echo t($t_base.'fields/or_login') ; ?></a></p> 
        <?php */ } ?>
        <p class="read-more">&nbsp;</p>
        <div class="price-btn-text"> 
            <?php echo $szBeforeLoginText;  ?>
            <button class="button-green1 get-rate-quote-submit-button" id="get-quote-price-button" <?php if($mode=='GET_QUOTES'){ ?> onclick="validate_booking_quote_form('booking_quote_contact_detail_form','<?php echo $mode; ?>');" <?php } ?>><span class="tiny-loader-span"></span><?php echo $szButtonText ;?></button>
            
            <button class="button-green1 fb-submit-button" id="fb-button" onclick="signupWithFacebook('<?php echo $redirect_url;?>');"><span class="tiny-loader-span"></span><span class="social-icon"><I class="fa fa-facebook"></I></span><?php echo t($t_base.'fields/fill_with_facebook') ;?></button>
            <button class="button-green1 gplus-submit-button" id="gplus-button" onclick="signupWithGooglePlus('<?php echo $redirect_url;?>');"><span class="tiny-loader-span"></span><span class="social-icon"><I class="fa fa-google-plus"></I></span><?php echo t($t_base.'fields/fill_with_google') ;?></button>
        </div> 
    </form>  
    <?php
    if($bAutoPostForm)
    {
        ?>
        <script type="text/javascript">
            validate_booking_quote_form('booking_quote_contact_detail_form','<?php echo $mode; ?>');
        </script>
        <?php
    }
}

function display_booking_quotes_contact_details($postSearchAry,$kRegisterShipCon,$mode=false)
{  
    $kConfig = new cConfig();	
    $t_base = "BookingDetails/";     
    $szClass_1 = 'fl-45' ;
    //$szClass_2 = 'fl-55' ;
    $iLanguage=  getLanguageId();
    $dialUpCodeAry = array();
    $dialUpCodeAry = $kConfig->getAllCountries(true,$iLanguage,true);

    if(!empty($_POST['bookingQuotesAry']))
    {
        $bookingQuotesAry = $_POST['bookingQuotesAry'] ;
    } 
    else
    {
        if($_SESSION['user_id']>0)
        {
            $kUser = new cUser();
            $kUser->getUserDetails($_SESSION['user_id']);
            
            $bookingQuotesAry['szShipperCompanyName'] = $kUser->szCompanyName ;
            $bookingQuotesAry['szShipperFirstName'] = $kUser->szFirstName ;
            $bookingQuotesAry['szShipperLastName'] = $kUser->szLastName ;
            $bookingQuotesAry['szShipperEmail'] = $kUser->szEmail ;
            $bookingQuotesAry['szShipperPhone'] = $kUser->szPhoneNumber ; 
            $bookingQuotesAry['iPrivateShipping'] = $kUser->iPrivate ;  
            
            if(empty($kUser->idInternationalDialCode))
            {
                $bookingQuotesAry['idShipperDialCode'] = $kUser->szCountry ;
            }
            else
            {
                $bookingQuotesAry['idShipperDialCode'] = $kUser->idInternationalDialCode ;
            }
        }
        else
        {
        
            if(!empty($_SESSION['google_plus_data']))
            {
                $bookingQuotesAry['szShipperFirstName'] = $_SESSION['google_plus_data']['szFirstName'];
                $bookingQuotesAry['szShipperLastName'] = $_SESSION['google_plus_data']['szLastName'] ;
                $bookingQuotesAry['szShipperEmail'] = $_SESSION['google_plus_data']['szEmail'] ;
                $_SESSION['google_plus_data']=array();
                unset($_SESSION['google_plus_data']);
            }
            
            if(!empty($_SESSION['fb_data']))
            {
                $bookingQuotesAry['szShipperFirstName'] = $_SESSION['fb_data']['szFirstName'];
                $bookingQuotesAry['szShipperLastName'] = $_SESSION['fb_data']['szLastName'] ;
                $bookingQuotesAry['szShipperEmail'] = $_SESSION['fb_data']['szEmail'] ;
                $_SESSION['fb_data']=array();
                unset($_SESSION['fb_data']);
            }
        }

        if(!empty($_COOKIE['__USER_COUNTRY_NAME___']))
        {
            $szUserCountryName = $_COOKIE['__USER_COUNTRY_NAME___'] ;
        }
        else
        {
            $userIpDetailsAry = array();
            $userIpDetailsAry = getCountryCodeByIPAddress();

            $szUserCountryName  = $userIpDetailsAry['szCountryCode'] ;
        }
        if(!empty($szUserCountryName))
        {
            $kConfig->loadCountry(false,$szUserCountryName);  
            $bookingQuotesAry['idShipperDialCode'] = $kConfig->idCountry ;
        }
    } 
    if(!empty($kRegisterShipCon->arErrorMessages))
    {
        $formId = 'booking_quote_contact_detail_form';
        $szValidationErrorKey = '';
        foreach($kRegisterShipCon->arErrorMessages as $errorKey=>$errorValue)
        {
            if(empty($szValidationErrorKey))
            {
                $szValidationErrorKey = $errorKey;
            }
            ?>
            <script type="text/javascript">
                $("#"+'<?php echo $errorKey; ?>').attr('class','red_border');
            </script>
            <?php
        }
    }  
    
    $isMobileValue= checkMobileDevice();
    
    $typeForDate='text';         
    
    $weightVolumePattern='';
    $weightVolumeInputMode='';
    if((int)$isMobileValue==1)
    {
        $typeForDate='number';
        
        $weightVolumePattern='pattern="[0-9]*"';
        $weightVolumeInputMode='inputmode="numeric"';
    }
?> 
    <div class="form-line1 clearfix">
        <input type="text" placeholder="<?=t($t_base.'fields/f_name');?>" onfocus="check_form_field_not_required(this.form.id,this.id);" name="bookingQuotesAry[szShipperFirstName]" id="szShipperFirstName" value="<?php echo $bookingQuotesAry['szShipperFirstName']?>"/>
        <input type="text" placeholder="<?=t($t_base.'fields/l_name');?>" onfocus="check_form_field_not_required(this.form.id,this.id);" name="bookingQuotesAry[szShipperLastName]" id="szShipperLastName" value="<?php echo $bookingQuotesAry['szShipperLastName']?>"/>
    </div>
    <div class="form-line2 clearfix">
        <input type="text" placeholder="<?=t($t_base.'fields/c_name');?>" onfocus="check_form_field_not_required(this.form.id,this.id);" name="bookingQuotesAry[szShipperCompanyName]" id="szShipperCompanyName" <?php if($bookingQuotesAry['iPrivateShipping']==1){ ?> value="<?php echo t($t_base.'fields/private');?>" disabled="true" <?php }else{ ?> value="<?=$bookingQuotesAry['szShipperCompanyName']?>" <?php }?> />
        <span class="get-quote-checkbox-container"><input type="checkbox" class="styled" name="bookingQuotesAry[iPrivateShipping]" id="iPrivateShipping" value="1" <?php if($bookingQuotesAry['iPrivateShipping']==1){ echo 'checked'; }?> onclick="lock_company_field('<?php echo t($t_base.'fields/private');?>') "><?=t($t_base.'fields/private');?></span>
    </div>
    <div class="form-line3 clearfix">
        <input type="text" placeholder="<?=t($t_base.'fields/email');?>" onfocus="check_form_field_not_required(this.form.id,this.id);" name="bookingQuotesAry[szShipperEmail]" id="szShipperEmail" value="<?php echo $bookingQuotesAry['szShipperEmail']?>" autocapitalize="none"/>
        <div class="get-quote-phone-container">
            <select size="1" name="bookingQuotesAry[idShipperDialCode]" class="styled" id="idShipperDialCode" onfocus="change_dropdown_value(this.id);check_form_field_not_required(this.form.id,this.id);" onblur="change_dropdown_value(this.id,'1');">
                <?php
                   if(!empty($dialUpCodeAry))
                   {
                       $usedDialCode = array();
                       foreach($dialUpCodeAry as $dialUpCodeArys)
                       {
                            if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                            {
                                $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ;
                                ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$bookingQuotesAry['idShipperDialCode']){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option><?php
                            }
                       }
                   }
               ?>
            </select>
        </div>
        <input type="<?php echo $typeForDate;?>"  <?php echo $weightVolumePattern;?> <?php echo $weightVolumeInputMode;?> class="get-quote-phone" placeholder="<?=t($t_base.'fields/phone_number');?>" onfocus="check_form_field_not_required(this.form.id,this.id);" name="bookingQuotesAry[szShipperPhone]" id="szShipperPhone" value="<?=$bookingQuotesAry['szShipperPhone']?>"/>
        <input type="hidden" name="bookingQuotesAry[szDefaultCompanyText]" id="szDefaultCompanyText" value="<?php echo t($t_base.'fields/private');?>">
    </div>  
   <?php
}
function display_get_quote_get_rate_bottom_section($mode)
{  
    $t_base = "BookingDetails/";
    $t_base_user = "Users/AccountPage/"; 

    $iLanguage = getLanguageId(); 
    $idUserSession = $_SESSION['user_id'];

    if($mode=='GET_RATES')
    {
        $szButtonText = t($t_base.'fields/see_price_now') ;
        $szBeforeLoginText = '';
    }
    else
    {
        $szButtonText = t($t_base.'fields/get_offer_now') ; 
    } 
    ?> 
    <div class="get-rate-section2">
        <ul class="search-compare-select row">
            <li class="search-get-rate col-sm-4"><span>&nbsp;</span><?php echo t($t_base.'fields/search'); ?></li>
            <li class="compare-get-rate col-sm-4"><span>&nbsp;</span><?php echo t($t_base.'fields/compare'); ?></li>
            <li class="select-get-rate col-sm-4"><span>&nbsp;</span><?php echo t($t_base.'fields/select'); ?></li>
        </ul> 
        <div class="time-price-communication">
            <p><?php echo t($t_base.'fields/we_care_fully_select_forwarder'); ?></p>
            
            <ul class="row">
                <li class="time-get-rate col-sm-4"><span>&nbsp;</span><?php echo t($t_base.'fields/delivery_on_time'); ?></li>
                <li class="price-get-rate col-sm-4"><span>&nbsp;</span><?php echo t($t_base.'fields/sharp_prices'); ?></li>
                <li class="communication-get-rate col-sm-4"><span>&nbsp;</span><?php echo t($t_base.'fields/good_commu'); ?></li>
            </ul>
        </div>
        <p><button class="button-green1 get-quote-button get-rate-quote-submit-button" <?php if($mode=='GET_QUOTES'){ ?> onclick="validate_booking_quote_form('booking_quote_contact_detail_form','<?php echo $mode; ?>');" <?php } ?>><?php echo $szButtonText; ?></button></p>
    </div> 
    <div class="get-rate-section3" id='price-guarantee-container'>
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="price-guarantee">
                        <h3><?php echo t($t_base.'fields/price_guarantee'); ?></h3>
                        <ol>
                            <li><?php echo t($t_base.'fields/price_guarantee_details_text_1'); ?></li>
                            <li><?php echo t($t_base.'fields/price_guarantee_details_text_2'); ?></li>
                        </ol>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="insurance">
                        <h3><?php echo t($t_base.'fields/insurance'); ?></h3>
                        <p><?php echo t($t_base.'fields/insurance_details_text_1'); ?></p>
                        <p><?php echo t($t_base.'fields/insurance_details_text_2'); ?></p>
                        <p><?php echo t($t_base.'fields/insurance_details_text_3'); ?></p>
                    </div>
                </div>
            </div>
            <p align="center"><button class="button-green1 get-quote-button get-rate-quote-submit-button" <?php if($mode=='GET_QUOTES'){ ?> onclick="validate_booking_quote_form('booking_quote_contact_detail_form','<?php echo $mode; ?>');" <?php } ?>><?php echo $szButtonText; ?></button></p>
        </div>
    </div> 
    <?php
}
 
function display_booking_quotes_details_form_old($postSearchAry,$kRegisterShipCon,$mode=false)
{  
    $kRegisterShipCon_new = new cRegisterShipCon(); 
    $kConfig = new cConfig();
    $kBooking= new cBooking();
    $idBooking = $postSearchAry['id'] ;

    $t_base = "BookingDetails/";
    $t_base_user = "Users/AccountPage/"; 

    $iLanguage = getLanguageId();

    $idUserSession = $_SESSION['user_id'];

    if($mode=='GET_RATES')
    {
        $szButtonText = t($t_base.'fields/see_price_now') ;
    }
    else
    {
        $szButtonText = t($t_base.'fields/get_offer_now') ;
    }
    ?> 
    <script type="text/javascript">
        $().ready(function(){ 
            Custom.init();
        });
    </script>
    <form action="" id="booking_quote_contact_detail_form" name="booking_quote_contact_detail_form" method="post"> 
        <div id="booking-quotes-form-container">
            <?php 
                echo display_booking_quotes_contact_details_old($postSearchAry,$allCountriesArr,$dialUpCodeAry,$shipperConsigneeAry,$kRegisterShipCon);
            ?>
        </div>  
        <input type="hidden" id="szOperationType" name="bookingQuotesAry[szOperationType]" value="BOOKING_QUOTE_PAGE">
        <input type="hidden" name="bookingQuotesAry[szBookingRandomNum]" id="szBookingRandomNum_booking_details" value=""/>
         <div class="booking_quotes_buttons clearfix">  
            <a href="javascript:void(0);" onclick="validate_booking_quote_form('booking_quote_contact_detail_form','<?php echo $mode; ?>');"  class="button1"><span><?php echo $szButtonText ;?></span></a>
        </div> 
    </form> 
    <?php
}

function display_booking_quotes_contact_details_old($postSearchAry,$kRegisterShipCon,$mode=false)
{  
    $kConfig = new cConfig();	
    $t_base = "BookingDetails/";     
    $szClass_1 = 'fl-45' ;
    //$szClass_2 = 'fl-55' ;
    $iLanguage = getLanguageId();
    $dialUpCodeAry = array();
    $dialUpCodeAry = $kConfig->getAllCountries(true,$iLanguage,true);

    if(!empty($_POST['bookingQuotesAry']))
    {
        $bookingQuotesAry = $_POST['bookingQuotesAry'] ;
    } 
    else
    {
        if($_SESSION['user_id']>0)
        {
            $kUser = new cUser();
            $kUser->getUserDetails($_SESSION['user_id']);
            
            $bookingQuotesAry['szShipperCompanyName'] = $kUser->szCompanyName ;
            $bookingQuotesAry['szShipperFirstName'] = $kUser->szFirstName ;
            $bookingQuotesAry['szShipperLastName'] = $kUser->szLastName ;
            $bookingQuotesAry['szShipperEmail'] = $kUser->szEmail ;
            $bookingQuotesAry['szShipperPhone'] = $kUser->szPhoneNumber ; 
            if(empty($kUser->idInternationalDialCode))
            {
                $bookingQuotesAry['idShipperDialCode'] = $kUser->szCountry ;
            }
            else
            {
                $bookingQuotesAry['idShipperDialCode'] = $kUser->idInternationalDialCode ;
            }
        } 
        if(!empty($_COOKIE['__USER_COUNTRY_NAME___']))
        {
            $szUserCountryName = $_COOKIE['__USER_COUNTRY_NAME___'] ;
        }
        else
        {
            $userIpDetailsAry = array();
            $userIpDetailsAry = getCountryCodeByIPAddress();

            $szUserCountryName  = $userIpDetailsAry['szCountryCode'] ;
        }
        if(!empty($szUserCountryName))
        {
            $kConfig->loadCountry(false,$szUserCountryName);  
            $bookingQuotesAry['idShipperDialCode'] = $kConfig->idCountry ;
        }
    }

    if(!empty($kRegisterShipCon->arErrorMessages))
    {
        $formId = 'booking_quote_contact_detail_form';
        $szValidationErrorKey = '';
        foreach($kRegisterShipCon->arErrorMessages as $errorKey=>$errorValue)
        {
            if(empty($szValidationErrorKey))
            {
                $szValidationErrorKey = $errorKey;
            }
            ?>
            <script type="text/javascript">
                $("#"+'<?php echo $errorKey; ?>').attr('class','red_border');
            </script>
            <?php
        }
    }  
?> 
   <div class="oh even"> 
       <p class="<?php echo $szClass_2; ?>">
           <input type="text" placeholder="<?=t($t_base.'fields/c_name');?>" onfocus="check_form_field_not_required(this.form.id,this.id);" name="bookingQuotesAry[szShipperCompanyName]" id="szShipperCompanyName" value="<?=$bookingQuotesAry['szShipperCompanyName']?>"/>
       </p>
   </div>
   <div class="oh odd"> 
       <p class="<?php echo $szClass_2; ?>">
           <input type="text" placeholder="<?=t($t_base.'fields/f_name');?>" onfocus="check_form_field_not_required(this.form.id,this.id);" name="bookingQuotesAry[szShipperFirstName]" id="szShipperFirstName" value="<?php echo $bookingQuotesAry['szShipperFirstName']?>"/>
       </p>
   </div>				
   <div class="oh even"> 
       <p class="<?php echo $szClass_2; ?>">
           <input type="text" placeholder="<?=t($t_base.'fields/l_name');?>" onfocus="check_form_field_not_required(this.form.id,this.id);" name="bookingQuotesAry[szShipperLastName]" id="szShipperLastName" value="<?php echo $bookingQuotesAry['szShipperLastName']?>"/>
       </p>
   </div>
   <div class="oh odd"> 
       <p class="<?php echo $szClass_2; ?>">
           <input type="text" placeholder="<?=t($t_base.'fields/email');?>" onfocus="check_form_field_not_required(this.form.id,this.id);" name="bookingQuotesAry[szShipperEmail]" id="szShipperEmail" value="<?php echo $bookingQuotesAry['szShipperEmail']?>"/>
       </p>
   </div>
   <div class="oh even"> 
       <p class="<?php echo $szClass_2; ?> telephone oh">
           <select size="1" class="styled" name="bookingQuotesAry[idShipperDialCode]" id="idShipperDialCode" onfocus="change_dropdown_value(this.id);check_form_field_not_required(this.form.id,this.id);" onblur="change_dropdown_value(this.id,'1');">
            <?php
               if(!empty($dialUpCodeAry))
               {
                    $usedDialCode = array();
                   foreach($dialUpCodeAry as $dialUpCodeArys)
                   {
                        if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                        {
                            $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ;
                        ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$bookingQuotesAry['idShipperDialCode']){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option>
                        <?php
                        }
                   }
               }
           ?>
           </select>
           <input type="text" placeholder="<?=t($t_base.'fields/phone_number');?>" onfocus="check_form_field_not_required(this.form.id,this.id);" name="bookingQuotesAry[szShipperPhone]" id="szShipperPhone" value="<?=$bookingQuotesAry['szShipperPhone']?>"/>
           <input type="hidden" id="iPrivateShipping" name="bookingQuotesAry[iPrivateShipping]" value="0">
       </p>
   </div>   
   <?php
}

function display_local_search_popup()
{ 
    $t_base = "BookingDetails/"; 
    $iLanguage = getLanguageId();
    
    $kWHSSearch = new cWHSSearch();
//    if($iLanguage==__LANGUAGE_ID_DANISH__)
//    {	
//        $szLocalSearchText = $kWHSSearch->getManageMentVariableByDescription('__LOCAL_SEARCH_POPUP_DANISH__');
//    }
//    else
//    {
//        $szLocalSearchText = $kWHSSearch->getManageMentVariableByDescription('__LOCAL_SEARCH_POPUP_ENGLISH__');
//    } 
    $szLocalSearchText = $kWHSSearch->getManageMentVariableByDescription('__LOCAL_SEARCH_POPUP__',$iLanguage);
?>
    <div id="popup-bg"></div>
    <div id="popup-container">			
        <div class="popup" style="width:500px;">
            <p class="close-icon" align="right">
                <a onclick="showHide('ajaxLogin');" href="javascript:void(0);">
                <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
                </a>
            </p>
            <div class="terms-condition-popup link16">
                <h5><?=t($t_base.'fields/local_search_header')?></h5>
                <p>
                    <?php  echo html_entity_decode($szLocalSearchText) ;  ?> 
                </p> 
                <br/>
                <p align="center">
                    <a href="javascript:void(0);" onclick="showHide('ajaxLogin');" class="button1"><span><?=t($t_base.'fields/close');?></span></a>
                </p>
            </div>
        </div>
    </div>	
    <?php
}

function display_pallet_details_popup($kBooking,$iNumPallets=false,$iSearchMiniPage=false,$palletDimensionAry=array())
{
    $t_base = "LandingPage/";   
    $iLanguage = getLanguageId();   
    $szDivKey = "_POP_V_".$iSearchMiniPage; 
    ?> 
    <div id="popup-bg"></div>
    <div id="popup-container">			
        <div class="popup" style="width:550px;">
            <p class="close-icon" align="right">
                <a onclick="showHide('ajaxLogin');" href="javascript:void(0);">
                <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
                </a>
            </p>
            <div class="pallet-feilds-container scroll-div" style="max-height:300px;border:none;height:auto;">
                <form id="pallet_details_form<?php echo $szDivKey; ?>" name="pallet_details_form<?php echo $szDivKey; ?>">
                    <div id="complete-box" class="active transporteca-box">   
                        <h4><?php echo t($t_base.'title/pallet_popup_title')?></h4> 
                        <div id="horizontal-scrolling-div-id<?php echo $szDivKey;?>">
                            <?php 
                                $counter = 1; 
                                if(count($_POST['palletDetailAry']['iLength'])>0 || $iNumPallets>0)
                                {
                                    if(count($_POST['palletDetailAry']['iLength'])>0)
                                    {
                                       $loop_counter = count($_POST['palletDetailAry']['iLength']);  
                                       $postDataFlag = true;
                                    }
                                    else
                                    {
                                        $loop_counter = $iNumPallets;  
                                    }
                                    for($j=0;$j<$loop_counter;$j++)
                                    {
                                        $counter = $j+1;
                                        if($postDataFlag)
                                        {
                                            $_POST['palletDetailAry']['iLength'][$counter-1] = format_numeric_vals($_POST['palletDetailAry']['iLength'][$counter-1]); 
                                            $_POST['palletDetailAry']['iWidth'][$counter-1] = format_numeric_vals($_POST['palletDetailAry']['iWidth'][$counter-1]);  
                                        }
                                        else
                                        { 
                                            $_POST['palletDetailAry']['iLength'][$counter-1] = format_numeric_vals($palletDimensionAry[$counter]['iLength']); 
                                            $_POST['palletDetailAry']['iWidth'][$counter-1] = format_numeric_vals($palletDimensionAry[$counter]['iWidth']);  
                                        }
                                        ?>
                                        <div id="add_booking_quote_container_<?php echo $j."".$szDivKey; ?>"> 
                                            <?php
                                                echo display_pallet_details_form($counter,$iSearchMiniPage);
                                            ?>
                                        </div>
                                        <?php
                                    }
                                }
                                else
                                {   
                                    ?>
                                    <div id="add_booking_quote_container_0<?php echo $szDivKey;?>"> 
                                        <?php
                                           echo display_pallet_details_form($counter,$iSearchMiniPage);
                                        ?>
                                    </div>
                                    <?php 
                                }
                            ?>
                            <div id="add_booking_quote_container_<?php echo $counter."".$szDivKey; ?>" style="display:none;"></div>
                        </div>   
                    </div>    
                    <input type="hidden" name="palletDetailAry[hiddenPosition]" id="hiddenPosition<?php echo $szDivKey;?>" value="<?=$counter?>" />
                    <input type="hidden" name="palletDetailAry[hiddenPosition1]" id="hiddenPosition1<?php echo $szDivKey;?>" value="<?=$counter?>" />
                    <input type="hidden" name="palletDetailAry[iPalletCounter]" id="iPalletCounter<?php echo $szDivKey;?>" value="<?=$counter?>" />
                    <input type="hidden" name="palletDetailAry[idSearchMiniPage]" id="idSearchMiniPage<?php echo $szDivKey;?>" value="<?php echo $iSearchMiniPage?>" /> 
                </form>
            </div>
            <div class="btn-container" style="text-align:center;">
                <a href="javascript:void(0);" class="button-green1" id="pallet_popup_submit<?php echo $szDivKey; ?>" onclick="" ><span><?=t($t_base.'fields/ok');?></span></a>  
            </div> 
            
            <script type="text/javascript" > 
                validate_pallet_fields('','<?php echo $iSearchMiniPage; ?>');
            </script>
            <?php  
                if(!empty($kBooking->arErrorMessages))
                {  
                    $formId = 'pallet_details_form'.$szDivKey; 
                    display_form_validation_error_message($formId,$kBooking->arErrorMessages);
                }
            ?>
        </div>
    </div> 
<?php
}

function display_pallet_details_form($number,$iSearchMiniPage=false)
{  
    if(!empty($_POST['palletDetailAry']))
    {
        $cargoAry = $_POST['palletDetailAry'] ; 
        $llop_counter = $number - 1 ;
        $fLength = $cargoAry['iLength'][$llop_counter];
        $fWidth = $cargoAry['iWidth'][$llop_counter];  
        $idCargoMeasure = $cargoAry['idCargoMeasure'][$llop_counter];  
    } 
    
    $kBooking = new cBooking();
    $kConfig = new cConfig();  

    // geting all cargo measure 
    $cargoMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_CARGO_MEASURE__');
    $t_base = "LandingPage/";  
    
    $szStandardClass="search-mini-cargo-fields"; 
    $idSuffix = "_POP_V_".$iSearchMiniPage ;
?>
    <script type="text/javascript">
        Custom.init();
    </script>
    <div class="cargo1 clearfix">
        <p class="field-name"> 
            <img src="<?=__BASE_STORE_INC_URL__.'/image.php?img=Pallet.png&temp=2&w=30&h=32'?>" alt="<?=$searchResults['szDisplayName']?>" border="0" />  
        </p> 
        <div class="cargo-volume-measures"> 
            <div class="search-mini-cargo-text"> 
                <p class="input-fields"><?php echo ucfirst(t($t_base.'fields/pallet'))." ".$number; ?></p> 
            </div> 
            <div class="<?php echo $szStandardClass; ?>"> 
                <p class="input-fields">  
                    <input type="text" class="first-cargo-fields<?php echo $idSuffix; ?>" <?=$disabled_str?> onkeypress="return isNumberKey(event);" onkeyup="validate_pallet_fields(event,'<?php echo $iSearchMiniPage; ?>');" onblur="check_form_field_empty_standard(this.form.id,this.id);validate_pallet_fields(event,'<?php echo $iSearchMiniPage; ?>');" onfocus="check_form_field_not_required(this.form.id,this.id);validate_pallet_fields(event,'<?php echo $iSearchMiniPage; ?>');" placeholder="<?php echo ucfirst(t($t_base.'fields/length')); ?>" name="palletDetailAry[iLength][<?=$number?>]" value="<?php if((float)$fLength>0.00){ echo round_up($fLength,1); }?>" id= "iLength<?php echo $number."".$idSuffix; ?>" />
                </p> 
            </div> 
            <div class="<?php echo $szStandardClass; ?>"> 
                <p class="input-fields"> 
                    <input type="text" <?=$disabled_str?> onkeypress="return isNumberKey(event);validate_pallet_fields(event,'<?php echo $iSearchMiniPage; ?>');" onkeyup="validate_pallet_fields(event,'<?php echo $iSearchMiniPage; ?>');" onblur="check_form_field_empty_standard(this.form.id,this.id);validate_pallet_fields(event,'<?php echo $iSearchMiniPage; ?>');" onfocus="check_form_field_not_required(this.form.id,this.id);validate_pallet_fields(event,'<?php echo $iSearchMiniPage; ?>');" placeholder="<?php echo ucfirst(t($t_base.'fields/width')); ?>" name="palletDetailAry[iWidth][<?=$number?>]" value="<?php if((float)$fLength>0.00){ echo round_up($fWidth,1);}?>" id= "iWidth<?php echo $number."".$idSuffix; ?>" />
                </p> 
            </div> 
            <div class="<?php echo $szStandardClass; ?>"> 
                <p class="input-fields"><span>&nbsp;</span>
                    <select name="palletDetailAry[idCargoMeasure][<?=$number?>]" onchange="validate_pallet_fields(event,'<?php echo $iSearchMiniPage; ?>');" onblur="validate_pallet_fields(event,'<?php echo $iSearchMiniPage; ?>');" id="idCargoMeasure<?php echo $number."".$idSuffix; ?>"  <?=$disabled_str?> class="styled">
                        <?php
                            if(!empty($cargoMeasureAry))
                            {
                                foreach($cargoMeasureAry as $cargoMeasureArys)
                                {
                                    ?>
                                    <option value="<?=$cargoMeasureArys['id']?>" <?php if($idCargoMeasure==$cargoMeasureArys['id']){?> selected <?php }?>><?=$cargoMeasureArys['szDescription']?></option>
                                    <?php
                                }
                            }
                        ?>
                    </select>
                </p> 
            </div>  
        </div>
        <div class="cargo-weight-measures"> 
            <div class="cargo-button-container">
                <a href="javascript:void(0);" onclick="add_more_cargo_details('PALLET_DETAILS_PAGE','<?php echo $postSearchAry['szBookingRandomNum']?>','<?php echo $iSearchMiniPage; ?>')" class="cargo-add-button">+</a>  
                <a href="javascript:void(0);" class="cargo-remove-button" onclick="remove_cargo_details_block('add_booking_quote_container_<?php echo ($number-1)."".$idSuffix ?>','<?php echo $number; ?>','<?php echo $iSearchMiniPage; ?>');">-</a>
            </div>
        </div>
    </div> 
    <?php
}

function display_transporteca_menu()
{ 
    $t_base = "home/";
    $kExplain = new cExplain();

    $iLanguage = getLanguageId();
    
    $kConfig = new cConfig();
    $languageArr=$kConfig->getLanguageDetails('',$iLanguage);
    $szLanguageCode=  strtolower($languageArr[0]['szLanguageCode']);
    
    
    
    $kWhsSearch = new cWHSSearch();  
    $arrDescriptions = array("'__FACEBOOK_URL__'","'__TWITTER_URL__'","'__LINKEDIN_URL__'","'__YOUTUBE_URL__'"); 
    $bulkManagemenrVarAry = array();
    $bulkManagemenrVarAry = $kWhsSearch->getBulkManageMentVariableByDescription($arrDescriptions);      

    $szFacebookUrl = $bulkManagemenrVarAry['__FACEBOOK_URL__'];
    $szTwitterUrl = $bulkManagemenrVarAry['__TWITTER_URL__'];
    $szLinkedinUrl = $bulkManagemenrVarAry['__LINKEDIN_URL__'];
    $szYoutubeUrl = $bulkManagemenrVarAry['__YOUTUBE_URL__'];
    
    
    $szFacebookUrl= addhttp($szFacebookUrl);
    $szTwitterUrl= addhttp($szTwitterUrl);
    $szLinkedinUrl= addhttp($szLinkedinUrl); 
    
//    $contentFolder='';
//    if(__ENVIRONMENT__ =='LIVE'){ 
//        $contentFolder="/content";
//    }
    $kSEO =new cSEO();
    $seoPageHeaderAry = array();
    $seoPageHeaderAry = $kSEO->getAllPublishedSeoPages($iLanguage,true);


    $mainMenuArr=$kExplain->getMainMenuParentData($szLanguageCode);
    require(__APP_PATH_ROOT__."/".__WORDPRESS_FOLDER__.'/wp-config.php');
?>
    <nav>
        <a href="javascript:void(0);" class="menu"><?=t($t_base.'header/menu');?></a>
        <div class="sub-nav-overlay">&nbsp;</div>
        <div class="sub-nav" id="sub-nav">
            <ul> 
                <?php
            if(!empty($mainMenuArr))
            {?>   
            
                <?php
                foreach($mainMenuArr as $mainMenuArrs)
                {
                    $mainPageUrl='';
                    $mainPageUrl=$mainMenuArrs['post_name'];
?>
                <li id="first-nav-<?php echo $mainMenuArrs['ID'];?>" <?php if($_REQUEST['tabFlag']==1 && $_REQUEST['menuflag']=='agg'){?> class="main-menu active"<?php }else{?> class="main-menu" <?php }?>><a href="javascript:void(0);"><?=$mainMenuArrs['post_title'];?></a>
    <?php 
            $howDoesworksLinks=$kExplain->getWordpressExplainPageDataForMenu($mainMenuArrs['ID']);
            if(!empty($howDoesworksLinks))
            {
                    echo '<ul>';
                    foreach($howDoesworksLinks as $howDoesworksLinkss)
                    {
                        if($howDoesworksLinkss['post_title']!=''){                           
                        $redir_da_url='';
                        $redir_da_url = get_post_meta($howDoesworksLinkss['id'],'_custom_permalink',true);
                        if(empty($redir_da_url))
                        {
                           $redir_da_url = get_post_meta($howDoesworksLinkss['id'],'custom_permalink',true); 
                        }
                        if($redir_da_url!='')
                        {
                            $redir_da_url= __MAIN_SITE_HOME_PAGE_URL__.'/'.$redir_da_url;
                        }
                        else
                        {
                            $redir_da_url = get_page_link( $howDoesworksLinkss['id'] );
                        }
                        
                        if($_REQUEST['menuflag'] == $howDoesworksLinkss['szLink'])
                        {
                            $szClass = 'class="active-triangle"';
                            
                            $szUrl = $redir_da_url;
                                                                                                                                                                            
                        }
                        else 
                        {
                            $szClass = 'class="triangle" ' ;
                           
                            $szUrl = $redir_da_url;
                            
                        }
                ?>
                    <li><a href="<?php echo $szUrl; ?>" title="<?php echo $szUrl; ?>"> <?=$howDoesworksLinkss['post_title']?></a></li>
                    <?php }}           ?>
            </ul>
    <?php } ?>
</li>
    <?php }?>
    
            
<?php }?>

<!--<li id="first-nav-10000" <?php if($_REQUEST['menuflag']=='info'){?> class="main-menu active"<?php }else{?> class="main-menu" <?php }?>><a href="javascript:void(0);"><?=t($t_base.'footer/information');?></a>
<ul>
<?php
    if(!empty($seoPageHeaderAry))
    { 
        foreach($seoPageHeaderAry as $seoPageHeaderArys)
        { 
    ?>
            <li><a href="<?php echo __INFORMATION_PAGE_URL__.'/'.$seoPageHeaderArys['szUrl'] ?>" title="<?php echo $seoPageHeaderArys['szLinkTitle']; ?>"> <?=$seoPageHeaderArys['szPageHeading']?></a></li>
    <?php   }
    }	 
        if(!empty($blogPageHeaderAry))
        { 
                foreach($blogPageHeaderAry as $blogPageHeaderArys)
                { 
?>
                <li><a href="<?php echo __INFORMATION_PAGE_URL__.'/'.$blogPageHeaderArys['szLink'] ?>" title="<?php echo $blogPageHeaderArys['szLinkTitle']; ?>"> <?=$blogPageHeaderArys['szHeading']?></a></li>
<?php }	 } ?>
        </ul> 
</li> -->
<?php
    $szMyBookingUrl_onclick='';
    if((int)$_SESSION['user_id']>0)
    {
        $kUser = new cUser();
        $kUser->getUserDetails($_SESSION['user_id']); 
        
        if($kUser->iConfirmed==1)
        {
            if($kUser->iFirstTimePassword==1)
            {
                $redirect_booking_url = __BASE_URL__."/myBooking" ;
                $szMyBookingUrl = "javascript:void(0);" ;
                $szMyBookingUrl_onclick="onclick='display_first_time_password_popup(1)'";
            }
            else
            {
                //check if there is cookie for password is set or not
                if(empty($_COOKIE['__USER_PASSWORD_COOKIE__']) || ($_COOKIE['__USER_PASSWORD_COOKIE__']!=$kUser->szUserPasswordKey))
                {
                    $szMyBookingUrl = "javascript:void(0);" ;
                    $szMyBookingUrl_onclick="onclick='display_check_for_password_popup(2)'";
                }
                else
                {
                    $szMyBookingUrl = __BASE_URL__."/myBooking" ;
                } 
            }
        }
        else
        {
            $szMyBookingUrl = __BASE_URL__."/verify-email" ;
        }  
?> 
    <li id="fifth-nav"><a href="<?php echo $szMyBookingUrl; ?>" <?php echo $szMyBookingUrl_onclick; ?>><?=t($t_base.'header/my_bookings');?></a></li>
<?php }  ?>
    </ul>
    <?php
 $t_base_explain="ExplainPage/";
if(__LANGUAGE__==__LANGUAGE_TEXT_DANISH__) 
{
    $szSearchUrl=__BASE_URL__."/searchword/";
}
else
{
    $szSearchUrl=__BASE_URL__."/searchword/";
} 
?>
            <form name="wordSearchLeftForm" id="wordSearchLeftForm" method="post" action="<?php echo $szSearchUrl;?>">
                <div class="clearfix">
                    <input type="text" size="20" name="szKeyWordLeftValue" autocomplete="off" onkeyup="submitenterKeyValue(event,'wordSearchLeftForm');" id="szKeyWordLeftValue" value="" placeholder="<?php echo t($t_base_explain.'fields/search');?>">	
                    <input type="text" style="display: none;" />
                    <img align="absmiddle" style="opacity:0.4" id="searchButtonIdLeft" src="<?php echo __MAIN_SITE_HOME_PAGE_URL__?>/images/magnifying-icon.jpg">
                </div>
            </form>			
            <div class="social-icons"><a href="<?php echo $szFacebookUrl;?>" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a><a href="<?php echo $szTwitterUrl;?>" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a><a href="<?php echo $szLinkedinUrl;?>" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a><a href="<?php echo $szYoutubeUrl;?>" target="_blank" class="youtube"><i class="fa fa-youtube-play"></i></a></div>
    </div>
</nav>
    <?php 
}

function display_local_search_popup_new($searchNotificationArr,$idBooking,$szCountryStrUrl,$iNumRecordFound,$szFlag='',$iHiddenValue='',$iSearchMiniPageVersion=0,$additionalAry=array())
{  
    $t_base = "BookingDetails/"; 
    $iLanguage = getLanguageId(); 
    $kWHSSearch = new cWHSSearch(); 
    if($searchNotificationArr['idButtonType']!="")
    {
        $idButtonTypeArr=explode(";",$searchNotificationArr['idButtonType']);
    }  
    if(!empty($searchNotificationArr['szContinueButtonText']))
    {
        $szContinueButtonText = $searchNotificationArr['szContinueButtonText'];
    }
    else
    {
        $szContinueButtonText = t($t_base.'fields/continue_search_noti');
    }
    
    if(!empty($idButtonTypeArr) && (in_array(__GO_TO_RFQ_BUTTON__, $idButtonTypeArr) || in_array(__GO_TO_BUTTON__, $idButtonTypeArr))) 
    {
        //In case when we need to display Creat RFQ button then we 
        $kBooking = new cBooking();
        $szBookingRandomNum = $kBooking->getBookingRandomNum($idBooking);
        $iOfferType = 5; //Anonymous
        $szLanguageName = getLanguageName($iLanguage);
        $szLanguageName = strtolower($szLanguageName);
    }
    $iPrivateNotificationAvailable = false;
    $iDonotShowPrivatePopup=true;
    if($iSearchMiniPageVersion==7 || $iSearchMiniPageVersion==8 || $iSearchMiniPageVersion==9)
    {
        $iDonotShowPrivatePopup=false;
    }
    if($iDonotShowPrivatePopup && $searchNotificationArr['iPrivateNotificationAvailable']>0 )
    {
        $iPrivateNotificationAvailable = $searchNotificationArr['iPrivateNotificationAvailable'];
    }
    $idUser = $_SESSION['user_id'];
?>
    <div id="popup-bg"></div>
    <div id="popup-container">			
        <div class="popup" style="width:500px;">
            <p class="close-icon" align="right">
                <a onclick="showHide('ajaxLogin');" href="javascript:void(0);">
                <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
                </a>
            </p>
            <div class="terms-condition-popup link16">
                <h5><?=$searchNotificationArr['szHeading']?></h5>
                <p>
                    <?php  echo $searchNotificationArr['szMessage'] ;  ?> 
                </p> 
                <br/>
                <p align="center">
                    <?php  
                        //Building link for close button
                        if(!empty($idButtonTypeArr) && in_array(__CLOSE_BUTTON__, $idButtonTypeArr)){?>
                            <a href="javascript:void(0);" onclick="showHide('ajaxLogin');" class="button1"><span><?=t($t_base.'fields/close');?></span></a>
                    <?php } ?> 
                    <?php
                        /*
                        * Building link for continue button
                        */
                        if(!empty($idButtonTypeArr) && in_array(__CONTINUE_BUTTON__, $idButtonTypeArr))
                        {
                            if($szFlag=='UPDATE_SERVICE_LIST')
                            {
                                ?>
                                <a href="javascript:void(0);" onclick="update_service_listing('landing_page_form','<?php echo $iHiddenValue;?>','<?php echo $iNumRecordFound;?>','1');$('#ajaxLogin').attr('style','display:none;')" class="button1"><span><?php echo $szContinueButtonText; ?></span></a>
                                <?php
                            }
                            else if($iPrivateNotificationAvailable==1)
                            {
                                ?>
                                <a href="javascript:void(0);" onclick="display_private_customer_notification('<?php echo $idBooking;?>','<?php echo $szCountryStrUrl;?>','<?php echo $iNumRecordFound;?>','<?php echo $iSearchMiniPageVersion?>','<?php echo $additionalAry['szFromPage']; ?>','<?php echo $additionalAry['szFormId']; ?>','<?php echo $additionalAry['iHiddenFlag']; ?>','<?php echo $additionalAry['szPageUrl']; ?>');" class="button1"><span><?php echo $szContinueButtonText; ?></span></a>
                                <?php
                            } 
                            else if($idBooking>0)
                            {
                                ?>
                                <a href="javascript:void(0);" onclick="continueSearching('<?php echo $idBooking;?>','<?php echo $szCountryStrUrl;?>','<?php echo $iNumRecordFound;?>','<?php echo $iSearchMiniPageVersion?>');$('#ajaxLogin').attr('style','display:none;');" class="button1"><span><?php echo $szContinueButtonText; ?></span></a>
                                <?php  
                            }
                            else
                            {
                                ?>
                                <a href="javascript:void(0);" onclick="validateNewLandingPageForm('<?php echo $additionalAry['szFormId']; ?>','<?php echo $idUser; ?>','<?php echo $additionalAry['szPageUrl']; ?>','<?php echo $additionalAry['iHiddenFlag']; ?>','<?php echo $additionalAry['iSearchMiniVersion']; ?>','<?php echo $additionalAry['szFromPage']; ?>','1');$('#ajaxLogin').attr('style','display:none;');" class="button1"><span><?php echo $szContinueButtonText; ?></span></a>
                                <?php
                            }
                        } 
                        /*
                        * Building link for Go to Button - This button olny exists in case of non-private cutomer
                        */
                    if(!empty($idButtonTypeArr) && in_array(__GO_TO_BUTTON__, $idButtonTypeArr)){  
                        $pos = strpos(strtolower($searchNotificationArr['szButtonUrl']),strtolower(__MAIN_SITE_HOME_PAGE_SECURE_URL__));
                        if ($pos !== false && !empty($szBookingRandomNum)) 
                        {
                            $szButtonUrlForVoga=  str_replace(__MAIN_SITE_HOME_PAGE_SECURE_URL__,"", $searchNotificationArr['szButtonUrl']);
                            $szURLAry = explode("/",urldecode($szButtonUrlForVoga));  
                            $szURL = $szURLAry[1]; 
                            if(!empty($szURL))
                            {
                                $kExplain = new cExplain();
                                $landingPageDataArys = $kExplain->getAllNewLandingPageData(false,$szURL);
                                $landingPageDataAry = $landingPageDataArys[0];
                                if($landingPageDataAry['iPageSearchType']==__SEARCH_TYPE_VOGUE__ || $landingPageDataAry['iPageSearchType']==__SEARCH_TYPE_VOGUE_AUTOMATIC_ || $landingPageDataAry['iPageSearchType']==__SEARCH_TYPE_VOGUE_AUTOMATIC_SIMPLE_)
                                {
                                    $searchNotificationArr['szButtonUrl'] = $searchNotificationArr['szButtonUrl']."?szBookingRandomNum=".$szBookingRandomNum;
                                }
                            }
                        }
                    ?>
                        <a href="javascript:void(0);" onclick="redirect_url('<?=$searchNotificationArr['szButtonUrl'];?>');" class="button1"><span><?=$searchNotificationArr['szButtonText'];?></span></a>
                    <?php }  
                    
                    /*
                     * Bulding link for Go To RFQ button - This button olny exists in case of private cutomer
                     */
                     
                    if(!empty($idButtonTypeArr) && in_array(__GO_TO_RFQ_BUTTON__, $idButtonTypeArr)) 
                    {
                        if(!empty($szBookingRandomNum))
                        {
                           ?>
                           <a href="javascript:void(0);" onclick="add_courier_service_rfq('<?php echo $szBookingRandomNum; ?>','<?php echo $iOfferType; ?>','<?php echo $szLanguageName; ?>','SEARCH_NOTIFICATION_POPUP')" class="button1"><span><?=$searchNotificationArr['szButtonTextRfq'];?></span></a>
                           <?php
                        }
                        else
                        {
                           ?>
                           <a href="javascript:void(0);" onclick="validateNewLandingPageForm('<?php echo $additionalAry['szFormId']; ?>','<?php echo $idUser; ?>','<?php echo $additionalAry['szPageUrl']; ?>','<?php echo $additionalAry['iHiddenFlag']; ?>','<?php echo $additionalAry['iSearchMiniVersion']; ?>','SEARCH_NOTIFICATION_POPUP','1','1','<?php echo $iOfferType; ?>');" class="button1"><span><?=$searchNotificationArr['szButtonTextRfq'];?></span></a>
                           <?php
                        }
                        ?> 
                    <?php } ?>
                </p>
            </div>
        </div>
    </div>	
    <?php
} 

function holdingpageHtml($landingPageDataAry,$idLandingPage,$iSearchMini=true,$iSearchPageVersion='0')
{
     $iLanguage = getLanguageId();
    $userIpDetailsAry = array();
    $userIpDetailsAry = getCountryCodeByIPAddress();
    $szUserCountryName  = $userIpDetailsAry['szCountryCode'] ;
    
    $kConfig = new cConfig();
    $allCountriesArr = array();
    $allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);
    
   
?>
            <?php if($iSearchMini){?>
            <h1><?php echo $landingPageDataAry['szHoldingHeading1']; ?><br><span><?php echo $landingPageDataAry['szHoldingHeading2']; ?></span></h1>
            <?php }?>
            <div class="form-box">
                    <p><strong><?php echo $landingPageDataAry['szHoldingTextBold1']; ?></strong> <?php echo $landingPageDataAry['szHoldingTextPlain1']; ?></p>
                    <p><strong><?php echo $landingPageDataAry['szHoldingTextBold2']; ?></strong> <?php echo $landingPageDataAry['szHoldingTextPlain2']; ?></p>
                    <form name='userSignupForm<?php echo $iSearchPageVersion;?>' id='userSignupForm<?php echo $iSearchPageVersion;?>' method="post" action="javascript:void(0);">
                    <div class="clearfix form-fields">
                            <div class="field-one">
                                    <label><?php echo $landingPageDataAry['szHoldingEmailHeading']; ?></label>
                                    <input type="text" id="szEmail<?php echo $iSearchPageVersion;?>" name="signupArr[szEmail]" onkeypress="submitEnterKeyHoldingPage(event,'HoldingPage','<?php echo $landingPageDataAry['szHoldingThanksButtonText']; ?>','<?php echo $landingPageDataAry['szHoldingSubmitButtonText']; ?>','<?php echo $iSearchPageVersion;?>');">
                                    
                            </div>
                            <div class="field-two flagstrap-icon" id="field-two">
                                    <label><?php echo $landingPageDataAry['szHoldingFromHeading']; ?></label>
                                    <input type="hidden" id="idFromCountry<?php echo $iSearchPageVersion;?>" name="signupArr[idFromCountry]">
                                    <div  id="idFromCountryDD<?php echo $iSearchPageVersion;?>" class="country-div" onclick="submitEnterKeyHoldingPage(event,'HoldingPage','<?php echo $landingPageDataAry['szHoldingThanksButtonText']; ?>','<?php echo $landingPageDataAry['szHoldingSubmitButtonText']; ?>','<?php echo $iSearchPageVersion;?>');"></div>
                                    <select  id="idFromCountryUl<?php echo $iSearchPageVersion;?>" name="idFromCountryUl" onchange="setValueInTextField('FROM',this.value,'<?php echo $iSearchPageVersion;?>');submitEnterKeyHoldingPage(event,'HoldingPage','<?php echo $landingPageDataAry['szHoldingThanksButtonText']; ?>','<?php echo $landingPageDataAry['szHoldingSubmitButtonText']; ?>','<?php echo $iSearchPageVersion;?>');"> 
                                        <?php
                                        if(!empty($allCountriesArr))
                                        {
                                            foreach($allCountriesArr as $allCountriesArrs)
                                            {?>
                                                <option  value="<?php echo strtolower($allCountriesArrs['szCountryISO']).'_'.$allCountriesArrs['szCountryName'];?>"><?php echo $allCountriesArrs['szCountryName'];?></option>
                                            <?php 
                                            }
                                        }
                                        ?>
                                        </select>        
                                    
                            </div>
                            <div class="field-two flagstrap-icon" id="field-three">
                                    <label><?php echo $landingPageDataAry['szHoldingToHeading']; ?></label>
                                    <input type="hidden" id="idToCountry<?php echo $iSearchPageVersion;?>" name="signupArr[idToCountry]" >
                                    <div  id="idToCountryDD<?php echo $iSearchPageVersion;?>" class="country-div" name="idToCountryDD" onclick="submitEnterKeyHoldingPage(event,'HoldingPage','<?php echo $landingPageDataAry['szHoldingThanksButtonText']; ?>','<?php echo $landingPageDataAry['szHoldingSubmitButtonText']; ?>','<?php echo $iSearchPageVersion;?>');"></div>
                                    <select  id="idToCountryUl<?php echo $iSearchPageVersion;?>" name="idToCountryUl" onchange="setValueInTextField('TO',this.value,'<?php echo $iSearchPageVersion;?>');submitEnterKeyHoldingPage(event,'HoldingPage','<?php echo $landingPageDataAry['szHoldingThanksButtonText']; ?>','<?php echo $landingPageDataAry['szHoldingSubmitButtonText']; ?>','<?php echo $iSearchPageVersion;?>');"> 
                                        <?php
                                        if(!empty($allCountriesArr))
                                        {
                                            foreach($allCountriesArr as $allCountriesArrs)
                                            {?>
                                                <option  value="<?php echo strtolower($allCountriesArrs['szCountryISO']).'_'.$allCountriesArrs['szCountryName'];?>"><?php echo $allCountriesArrs['szCountryName'];?></option>
                                            <?php 
                                            }
                                        }
                                        ?>
                                    </select>
                                    
                            </div>
                        <div class="field-btn">
                            <input type="hidden" id="iSuccessSubmit<?php echo $iSearchPageVersion;?>" name="iSuccessSubmit" value='0'>
                            <input type="hidden" id="idLanguage<?php echo $iSearchPageVersion;?>" name="signupArr[idLanguage]" value='<?php echo $iLanguage;?>'>
                            <input type="hidden" id="idIpCountry<?php echo $iSearchPageVersion;?>" name="signupArr[idIpCountry]" value='<?php echo $szUserCountryName;?>'>
                            <input type="hidden" id="idLandingPage<?php echo $iSearchPageVersion;?>" name="signupArr[idLandingPage]" value='<?php echo $idLandingPage;?>'>
                            <input onclick="submitSignupForm('<?php echo $landingPageDataAry['szHoldingThanksButtonText']; ?>','<?php echo $iSearchPageVersion;?>');" type="button" id="holding_button_submit<?php echo $iSearchPageVersion;?>" value="<?php echo $landingPageDataAry['szHoldingSubmitButtonText']; ?>"></div>
                    </div>
                   </form>     
            </div>

     <?php
     if((int)$iSearchPageVersion==0){?>
    <script type="text/javascript">
        jQuery().ready(function(){ 
            $("#szEmail<?php echo $iSearchPageVersion;?>").focus(); 

         });
    </script> 
   <?php 
     }
}

function get_youtube_embed_video($url)
{
    $httpValueArr=explode(":",$url);
    $httpValue=$httpValueArr[0];

               preg_match(
        '/[\\?\\&]v=([^\\?\\&]+)/',
        $url,
        $matches
    );
    if (empty($matches[1]))
    {
        $matches = explode("/v/", $url);
    }
   // print_r($matches);
                $id = $matches[1];

                $width = '495';
                $height = '350';
                return '<object width="' . $width . '" height="' . $height . '"><param name="movie" value="'.$httpValue.'://www.youtube.com/v/' . $id . '&amp;hl=en_US&amp;fs=1?rel=0"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="'.$httpValue.'://www.youtube.com/v/' . $id . '&amp;hl=en_US&amp;fs=1?rel=0" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="' . $width . '" height="' . $height . '"></embed></object>';
}

function display_calculation_logs($szPriceCalculationLogs,$div_id='courier_service_calculation')
{
    ?>
    <div id='<?php echo $div_id; ?>' class='booking-page clearfix' style='display:none'>
        <div id='popup-bg'></div>
        <div id='popup-container'>
            <div class='popup abandon-popup' style='margin-left:20px;width:85%;'>
                <p class='close-icon' align='right'>
                    <a onclick="showHideCourierCal('<?php echo $div_id; ?>')" href='javascript:void(0);'>
                        <img alt='close' src="<?php echo __BASE_STORE_IMAGE_URL__.'/close1.png'; ?>">
                    </a>
                </p>
                <div style='text-align:left;height:400px;overflow-y:auto;overflow-x:hidden;width:100%;'><?php echo $szPriceCalculationLogs; ?></div> 
            </div> 
        </div> 
    </div>
    <?php
}
function doNotSendMailToThisDomainEmail($szEmail)
{
    $excludedEmailAry = array('transporteca.com');
    $szShipperEmailAry = explode("@",$szEmail);
    $szExcludedEmailDomain = $szShipperEmailAry[1];
    
    if(!empty($excludedEmailAry) && !in_array($szExcludedEmailDomain,$excludedEmailAry))
    {
        return true;
    }
    
    return false;
}
?>