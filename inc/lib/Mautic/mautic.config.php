<?php

// Require this file if you're not using composer's vendor/autoload

// Required PHP extensions
if (!function_exists('curl_init')) {
  throw new Exception('Mautic needs the CURL PHP extension.');
}
if (!function_exists('json_decode')) {
  throw new Exception('Mautic needs the JSON PHP extension.');
} 
//Mautic base class
require(dirname(__FILE__) . '/MauticApi.php'); 

// Auth utility classes
require(dirname(__FILE__) . '/Auth/ApiAuth.php');
require(dirname(__FILE__) . '/Auth/AuthInterface.php'); 
require(dirname(__FILE__) . '/Auth/OAuth.php'); 
 
//Api library classes
require(dirname(__FILE__) . '/Api/Api.php'); 
require(dirname(__FILE__) . '/Api/Assets.php'); 
require(dirname(__FILE__) . '/Api/Forms.php'); 
require(dirname(__FILE__) . '/Api/Leads.php'); 
require(dirname(__FILE__) . '/Api/Lists.php'); 

//Exception classes
require(dirname(__FILE__) . '/Exception/ActionNotSupportedException.php');
require(dirname(__FILE__) . '/Exception/ContextNotFoundException.php');
require(dirname(__FILE__) . '/Exception/IncorrectParametersReturnedException.php');
require(dirname(__FILE__) . '/Exception/UnexpectedResponseFormatException.php');

//Logger files
/*
require(dirname(__FILE__) . '/psr/Log/AbstractLogger.php');
require(dirname(__FILE__) . '/psr/Log/InvalidArgumentException.php');
require(dirname(__FILE__) . '/psr/Log/LoggerAwareInterface.php');
require(dirname(__FILE__) . '/psr/Log/LoggerAwareTrait.php');
require(dirname(__FILE__) . '/psr/Log/LoggerInterface.php');
require(dirname(__FILE__) . '/psr/Log/LoggerTrait.php');
require(dirname(__FILE__) . '/psr/Log/LogLevel.php');
require(dirname(__FILE__) . '/psr/Log/NullLogger.php');
 * 
 */