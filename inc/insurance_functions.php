<?php
if(!isset($_SESSION))
{
    session_start();
}
//error_reporting(E_ALL);
//ini_set('display_error','1');
/**
 * This file contains the functions that are generic for the store and can be used by all the php files in the store website.
 *
 * functiions.php
 * @copyright Copyright (C) 2016 Trnasporteca, LLC
 * @author Anil
 * @package Trnasporteca Development
 */
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/pdf_functions.php" ); 
require_once( __APP_PATH_CLASSES__ . "/database.class.php");
include_classes();
 
function display_insurance_rates_listing($iType,$kInsurance,$sortingAry=false)
{
    $t_base="management/railTransport/"; 
    $insuranceRateListAry = array();
    $kInsuranceNew = new cInsurance();
    if(!empty($sortingAry))
    { 
        $insuranceSearchAry = array();
        $insuranceSearchAry['szSortField'] = $sortingAry['idPrimarySortKey'];
        $insuranceSearchAry['szSortOrder'] = $sortingAry['szSortValue'];
         
        $insuranceRateListAry = $kInsuranceNew->getAllInsuranceRates($iType,$insuranceSearchAry);
    }
    else
    {
        //$railTransportListAry = $kServices->getAllRailTransport(); 
        $insuranceRateListAry = $kInsuranceNew->getAllInsuranceRates($iType);
    } 
    
    if($szSortField=='szFromWarehouse')
    {
        if($szSortOrder=='DESC')
        {
            $szFromWHSSortClass = "sort-arrow-down";
        }
        else
        {
            $szFromWHSSortClass = "sort-arrow-up";
        }
    }
    else if($szSortField=='szToWarehouse')
    {
        if($szSortOrder=='DESC')
        {
            $szToWHSSortClass = "sort-arrow-down";
        }
        else
        {
            $szToWHSSortClass = "sort-arrow-up";
        }
    }
    else if($szSortField=='iDTD')
    {
        if($szSortOrder=='DESC')
        {
            $iDTDSortClass = "sort-arrow-down";
        }
        else
        {
            $iDTDSortClass = "sort-arrow-up";
        }
    }
    else if($szSortField=='szDisplayName' || empty($szSortField))
    {
        if($szSortOrder=='DESC')
        {
            $szFwdSortOrder = "sort-arrow-down";
        }
        else
        {
            $szFwdSortOrder = "sort-arrow-up";
        }
    }  
    $counter=count($courierTrackiocnArr); 
    if($iType==__INSURANCE_SELL_RATE_TYPE__)
    {
        $szInsuranceTableHeading = "Insurance sales tariff";
        $szMainContainerDiv = "insurance_sell_rates_container_div";
    }
    else 
    {
        $szInsuranceTableHeading = "Insurance buy rate";
        $szMainContainerDiv = "insurance_buy_rates_container_div";
    }
    $_POST['addInsuranceAry'][$iType] = array();  
?>   
    <script type="text/javascript">
        $(function () {
            $('#'+'<?php echo $szMainContainerDiv; ?>').multiSelect({
                actcls: 'selected_row', //Select Styles
                selector: 'tbody tr', //Row element selected
                except: ['tbody'], //Not after removing the effect of multiple-choice elements selected queue
                statics: ['#table-header', '[data-no="1"]'], //Conditions are excluded row element
                callback: function (items) 
                {
                    multi_select_call_back('<?php echo $iType; ?>');
                }
            });
        })
        function multi_select_call_back(iType)
        { 
            /* 
            * @type String
            * div and input fields ids
            */ 
            var container_div_id = '<?php echo $szMainContainerDiv; ?>';
            var primary_id_field = "idInsurance_"+iType;
            var clear_button_id = "insurance_rates_clear_button_"+iType;
            var add_button_id = "insurance_rates_add_button_"+iType;
            var selected_id_pipe_line = "insuranceRateIds_"+iType;
            var formId = "insurance_form_type_"+iType;
            
            var res_ary_new = new Array();
            var ctr_1 = 0;
            var idModeTransport = 0;
            if(iType==1)
            {
                $("#insurance_buy_rates_container_div .selected_row").each(function(input_obj){
                    var tr_id = this.id;
                    var tr_id_ary = tr_id.split("____");
                    idModeTransport = parseInt(tr_id_ary[1]); 
                    //console.log("ID: "+tr_id);
                    if(!isNaN(idModeTransport))
                    {
                        res_ary_new[ctr_1] = tr_id_ary[1];
                        ctr_1++;
                    } 
                });
            }
            else if(iType==2)
            {
                $("#insurance_sell_rates_container_div .selected_row").each(function(input_obj){
                    var tr_id = this.id;
                    var tr_id_ary = tr_id.split("____");
                    idModeTransport = parseInt(tr_id_ary[1]);  
                    if(!isNaN(idModeTransport))
                    {
                        res_ary_new[ctr_1] = tr_id_ary[1];
                        ctr_1++;
                    } 
                });
            }
            if(res_ary_new.length)
            {
                var idRailTransport = $("#"+primary_id_field).val();
                idRailTransport = parseInt(idRailTransport);
                if(idRailTransport>0 && idModeTransport==idRailTransport && parseInt(res_ary_new.length)==1)
                {
                    /*
                    * If a record is already selected for edit then we don't overwrite buttons texts
                    */
                }
                else
                {
                    //console.log("In delete section...");
                    var szModeOfTransIds = res_ary_new.join(';'); 
                    $("#"+selected_id_pipe_line).attr('value',szModeOfTransIds);
                    
                    $("#"+clear_button_id).unbind("click");
                    $("#"+clear_button_id).click(function(){editInsuranceRates(iType,'DELETE_INSURANCE_RATE_CONFIRM');});
                    $("#"+clear_button_id).attr("style",'opacity:1'); 
                    $("#"+clear_button_id + " span").html("Delete"); 
                } 
            }
            else
            {
                var szModeOfTransIds = '';
                //$("#"+).attr('value',szModeOfTransIds); 
                console.log("Clear...");
                $("#"+clear_button_id).unbind("click");
                $('#'+clear_button_id).attr('style','opacity:1');
                $("#"+clear_button_id).click(function(){ editInsuranceRates(iType,"CLEAR_ISURANCE_FORM_DATA"); }); 
                $("#"+clear_button_id+" span").html("Clear"); 
            } 
            if(parseInt(res_ary_new.length)==1)
            {
                var idRailTransport = $("#"+primary_id_field).val();
                idRailTransport = parseInt(idRailTransport);
                if(idRailTransport>0 && idModeTransport==idRailTransport)
                {
                    /*
                    * If a record is already selected for edit then we don't overwrite buttons texts
                    */
                }
                else
                { 
                    $("#"+add_button_id).unbind("click");
                    $("#"+add_button_id).click(function(){editInsuranceRates(iType,'EDIT_INSURANCE'); });
                    $("#"+add_button_id).attr("style",'opacity:1');
                    $("#"+add_button_id+" span").html("Edit"); 
                    resetForm(formId);
                } 
            }
            else
            { 
                $("#"+add_button_id).unbind("click");
                $("#"+add_button_id).attr("style",'opacity:0.4');
                $("#"+add_button_id+" span").html("ADD"); 
            }
        }
    </script>  
    <div id="<?php echo $szMainContainerDiv; ?>"> 
        <h4><strong><?php echo $szInsuranceTableHeading; ?></strong></h4>
        <div class="clearfix courier-provider courier-provider-scroll" style="max-height:620px;">	
          <table class="format-2" width="100%" cellspacing="0" cellpadding="0" border="0" id="mode-transport-table">
            <tr id="table-header">
                <td style="width:15%;cursor:pointer" onclick="sort_insurance_rates(event,'szOriginCountry','<?php echo $iType; ?>');"><strong>From</strong> <span class="moe-transport-sort-span_<?php echo $iType; ?> <?php echo $szOriginCountry; ?>" id="<?php echo "moe-transport-sort-span-id-szOriginCountry-".$iType; ?>">&nbsp;</span></td>
                <td style="width:15%;cursor:pointer" onclick="sort_insurance_rates(event,'szDestinationCountry','<?php echo $iType; ?>');"><strong>To</strong> <span class="moe-transport-sort-span_<?php echo $iType; ?> <?php echo $szDestinationCountry; ?>" id="<?php echo "moe-transport-sort-span-id-szDestinationCountry-".$iType; ?>">&nbsp;</span></td>
                <td style="width:10%;cursor:pointer" onclick="sort_insurance_rates(event,'szTransportMode','<?php echo $iType; ?>');"><strong>Mode</strong> <span class="moe-transport-sort-span_<?php echo $iType; ?> <?php echo $szTransportMode; ?>" id="<?php echo "moe-transport-sort-span-id-szTransportMode-".$iType; ?>">&nbsp;</span></td> 
                <td style="width:15%;cursor:pointer" onclick="sort_insurance_rates(event,'szCommodityName','<?php echo $iType; ?>');"><strong>Commodity</strong> <span class="moe-transport-sort-span_<?php echo $iType; ?> <?php echo $szCommodityName; ?>" id="<?php echo "moe-transport-sort-span-id-szCommodityName-".$iType; ?>">&nbsp;</span></td> 
                <td style="width:15%;cursor:pointer" onclick="sort_insurance_rates(event,'fInsuranceUptoPrice','<?php echo $iType; ?>');"><strong>Value up to</strong> <span class="moe-transport-sort-span_<?php echo $iType; ?> <?php echo $fInsuranceUptoPrice; ?>" id="<?php echo "moe-transport-sort-span-id-fInsuranceUptoPrice-".$iType; ?>">&nbsp;</span></td>
                <td style="width:15%;cursor:pointer" onclick="sort_insurance_rates(event,'fInsuranceRate','<?php echo $iType; ?>');"><strong>Rate (%)</strong> <span class="moe-transport-sort-span_<?php echo $iType; ?> <?php echo $fInsuranceRate; ?>" id="<?php echo "moe-transport-sort-span-id-fInsuranceRate-".$iType; ?>">&nbsp;</span></td> 
                <td style="width:15%;cursor:pointer" onclick="sort_insurance_rates(event,'fInsuranceMinPrice','<?php echo $iType; ?>');"><strong>Minimum</strong> <span class="moe-transport-sort-span_<?php echo $iType; ?> <?php echo $fInsuranceMinPrice; ?>" id="<?php echo "moe-transport-sort-span-id-fInsuranceMinPrice-".$iType; ?>">&nbsp;</span></td> 
            </tr>   	
        <?php
            if(!empty($insuranceRateListAry))
            {
                foreach($insuranceRateListAry as $insuranceRateListArys)
                { 
                    $szClassName = '';
                    if($iType==__INSURANCE_SELL_RATE_TYPE__ && $insuranceRateListArys['iBuyrateAvailable']==0)
                    {
                        //$szClassName = 'class="red_text"';
                    }
                    ?>
                    <tr id="truckin_tr____<?php echo $insuranceRateListArys['id']; ?>" <?php echo $szClassName; ?>>
                        <td id="from_to_country_1_<?php echo $insuranceRateListArys['id']?>" ><?php echo $insuranceRateListArys['szOriginCountry']; ?></td>
                        <td id="from_to_country_2_<?php echo $insuranceRateListArys['id']?>" ><?php echo $insuranceRateListArys['szDestinationCountry']; ?></td>
                        <td id="from_to_country_3_<?php echo $insuranceRateListArys['id']?>" ><?php echo $insuranceRateListArys['szTransportMode']; ?></td>
                        <td id="from_to_country_4_<?php echo $insuranceRateListArys['id']?>" ><?php echo $insuranceRateListArys['szCommodityName']; ?></td>
                        <td id="from_to_country_5_<?php echo $insuranceRateListArys['id']?>" ><?php echo $insuranceRateListArys['szInsuranceCurrency']." ".number_format((float)$insuranceRateListArys['fInsuranceUptoPrice']); ?></td>
                        <td id="from_to_country_6_<?php echo $insuranceRateListArys['id']?>" ><?php echo number_format((float)$insuranceRateListArys['fInsuranceRate'],3); ?></td>
                        <td id="from_to_country_7_<?php echo $insuranceRateListArys['id']?>" ><?php echo $insuranceRateListArys['szInsuranceCurrency']." ".number_format((float)$insuranceRateListArys['fInsuranceMinPrice']); ?></td>
                    </tr>
                <?php		
                } 				 
            }
            else
            {?>
                <tr>
                   <td colspan="7" align="center">
                       <h4><b><?=t($t_base.'fields/no_courier_provider_found');?></b></h4>
                   </td>
                </tr>
            <?php } ?>  
           </table>
       </div> 
   </div> 
   <div class="clear-all"></div>
    <br>
   <div id="insurance_add_edit_form_container_<?php echo $iType; ?>">
        <?php
            echo display_insurance_sell_buy_rate_addedit_form($iType,$kInsurance);
        ?>	
    </div> 
<?php  
} 

function display_insurance_sell_buy_rate_addedit_form($iType,$kInsurance)
{  
    $t_base="management/insurance/";
    if(!empty($kInsurance->arErrorMessages))
    {
        $formId = 'insurance_form_type_'.$iType;
        $szValidationErrorKey = '';
        foreach($kInsurance->arErrorMessages as $errorKey=>$errorValue)
        {
            if(empty($szValidationErrorKey))
            {
                $szValidationErrorKey = $errorKey;
            }
            if($errorKey=='szSpecialError')
            {
                $szSpecialErrorMessage = $errorValue ;
            }
            else
            {
                ?>
                <script type="text/javascript">
                    $("#"+'<?php echo $errorKey."_".$iType; ?>').addClass('red_border');
                </script>
            <?php }
        }
    }
    $kConfig=new cConfig();
    $currencyAry = array();
    $currencyAry = $kConfig->getBookingCurrency(false,true);  

    if(!empty($_POST['addInsuranceAry'][$iType]))
    {
        $postAddInsuranceAry = $_POST['addInsuranceAry'][$iType];
        
        $idInsuranceCurrency = $postAddInsuranceAry['idInsuranceCurrency'];
        $fInsuranceUptoPrice = $postAddInsuranceAry['fInsuranceUptoPrice'];
        $fInsuranceRate = $postAddInsuranceAry['fInsuranceRate'];
        $fInsuranceMinPrice = $postAddInsuranceAry['fInsuranceMinPrice']; 
        
        $idOriginCountry = $postAddInsuranceAry['idOriginCountry'];
        $idDestinationCountry = $postAddInsuranceAry['idDestinationCountry'];
        $idTransportMode = $postAddInsuranceAry['idTransportMode'];
        $szCargoType = $postAddInsuranceAry['szCargoType'];
        $idInsurance = $postAddInsuranceAry['idInsurance']; 
    }
    else if($kInsurance->idInsurance>0)
    {
        $idInsuranceCurrency = $kInsurance->idInsuranceCurrency;
        if(!empty($kInsurance->fInsuranceUptoPrice))
        { 
            $fInsuranceUptoPrice = round((float)$kInsurance->fInsuranceUptoPrice);
        }  
        if(!empty($kInsurance->fInsuranceRate))
        {
            $fInsuranceRate = round((float)$kInsurance->fInsuranceRate,3);
        }
        if(!empty($kInsurance->fInsuranceMinPrice))
        {
            $fInsuranceMinPrice = round((float)$kInsurance->fInsuranceMinPrice);
        }  
        $idOriginCountry = $kInsurance->idOriginCountry ;
        $idDestinationCountry = $kInsurance->idDestinationCountry ;
        $idTransportMode = $kInsurance->idTransportMode ;
        $szCargoType = $kInsurance->szCargoTypeCode ;
        $idInsurance = $kInsurance->idInsurance ;
    }      
    
    $transportModeListAry = array();
    $transportModeListAry = $kConfig->getAllTransportMode(false,false,true,true);

    $allCountriesArr = array();
    $allCountriesArr = $kConfig->getAllCountriesForPreferencesKeyValuePair();  
    
    $kVatApplication = new cVatApplication();
    $manualFeeCargoTypeArr = array();
    $manualFeeCargoTypeArr = $kVatApplication->getManualFeeCaroTypeList(); 
    $szClearButtonId = "insurance_rates_clear_button_".$iType; 
?>
	
    <script type="text/javascript">
        $().ready(function(){
            enableInsuranceBuySellButton('<?php echo $iType; ?>'); 
            $("#"+'<?php echo $szClearButtonId; ?>').unbind("click");
            $('#'+'<?php echo $szClearButtonId; ?>').attr('style','opacity:1');
            $("#"+'<?php echo $szClearButtonId; ?>').click(function(){ editInsuranceRates('<?php echo $iType; ?>',"CLEAR_ISURANCE_FORM_DATA"); });  
        });
    </script>
    <div id="isurance_buy_rate_form" style="width:100%;">
        <?php if(!empty($szSpecialErrorMessage)){ ?>
            <p style="font-style:italic;color:red;font-size:14px;"> <?php echo $szSpecialErrorMessage; ?></p>
        <?php  }  ?>
	<form action="javascript:void(0);" name="insurance_form_type_<?php echo $iType; ?>" id="insurance_form_type_<?php echo $iType; ?>">
            <table cellpadding="0" cellspacing="2" width="100%">
		<tr>  
                    <td class="wd-15"><?=t($t_base.'fields/from')?></td>
                    <td class="wd-15"><?=t($t_base.'fields/to')?></td>
                    <td class="wd-14"><?=t($t_base.'fields/mode')?></td>
                    <td class="wd-14"><?=t($t_base.'fields/commodity')?></td>
                    <td class="wd-12"><?=t($t_base.'fields/currency')?></td>
                    <td class="wd-10"><?=t($t_base.'fields/value_up_to')?></td>
                    <td class="wd-10"><?=t($t_base.'fields/rate')." (%)"?></td>
                    <td class="wd-10"><?=t($t_base.'fields/minimum')?></td>
		</tr>
		<tr> 
                    <td>
                        <select onchange="enableInsuranceBuySellButton('<?php echo $iType; ?>');" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="addInsuranceAry[<?php echo $iType; ?>][idOriginCountry]" id="idOriginCountry_<?php echo $iType; ?>">
                            <option value="">Select</option>
                        <?php
                            if(!empty($allCountriesArr))
                            {
                                foreach($allCountriesArr as $allCountriesArrs)
                                {
                                    ?>
                                    <option value="<?php echo $allCountriesArrs['idCountry']; ?>" <?php echo (($idOriginCountry==$allCountriesArrs['idCountry'])?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName']?></option>
                                    <?php
                                }
                            }
                         ?>
                        </select>
                    </td>
                    <td>
                        <select onchange="enableInsuranceBuySellButton('<?php echo $iType; ?>');" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="addInsuranceAry[<?php echo $iType; ?>][idDestinationCountry]" id="idDestinationCountry_<?php echo $iType; ?>">
                            <option value="">Select</option>
                        <?php
                            if(!empty($allCountriesArr))
                            {
                                foreach($allCountriesArr as $allCountriesArrs)
                                {
                                    ?>
                                    <option value="<?php echo $allCountriesArrs['idCountry']; ?>" <?php echo (($idDestinationCountry==$allCountriesArrs['idCountry'])?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName']?></option>
                                    <?php
                                }
                            }
                         ?>
                        </select>
                    </td>
                    <td>
                        <select onchange="enableInsuranceBuySellButton('<?php echo $iType; ?>');" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="addInsuranceAry[<?php echo $iType; ?>][idTransportMode]" id="idTransportMode_<?php echo $iType; ?>">
                            <option value="">Select</option>
                        <?php
                            if(!empty($transportModeListAry))
                            {
                                foreach($transportModeListAry as $transportModeListArys)
                                {
                                    ?>
                                    <option value="<?php echo $transportModeListArys['id']; ?>" <?php echo (($idTransportMode==$transportModeListArys['id'])?'selected':''); ?>><?php echo $transportModeListArys['szShortName']?></option>
                                    <?php
                                }
                            }
                         ?>
                        </select>
                    </td>
                    <td>
                        <select onchange="enableInsuranceBuySellButton('<?php echo $iType; ?>');" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="addInsuranceAry[<?php echo $iType; ?>][szCargoType]" id="szCargoType_<?php echo $iType; ?>">
                            <option value="">Select</option>
                            <?php
                                if(!empty($manualFeeCargoTypeArr))
                                {
                                    foreach($manualFeeCargoTypeArr as $manualFeeCargoTypeArrs)
                                    {
                                       ?>
                                       <option value="<?php echo $manualFeeCargoTypeArrs['szCode']; ?>" <?php echo (($manualFeeCargoTypeArrs['szCode']==$szCargoType)?'selected':''); ?>><?php echo $manualFeeCargoTypeArrs['szName']; ?></option>
                                       <?php
                                    }
                                }
                           ?>  
                        </select>
                    </td> 
                    <td>
                        <select onchange="enableInsuranceBuySellButton('<?php echo $iType; ?>');" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="addInsuranceAry[<?php echo $iType; ?>][idInsuranceCurrency]" id="idInsuranceCurrency_<?php echo $iType; ?>">
                            <option value="">Select</option>
                        <?php
                            if(!empty($currencyAry))
                            {
                                foreach($currencyAry as $currencyArys)
                                {
                                    ?>
                                    <option value="<?=$currencyArys['id']?>" <?php echo (($idInsuranceCurrency==$currencyArys['id'])?'selected':''); ?>><?=$currencyArys['szCurrency']?></option>
                                    <?php
                                }
                            }
                         ?>
                        </select>  
                    </td>
                    <td>
                        <input type="text" onkeypress="return format_decimat(this,event)" onkeyup="enableInsuranceBuySellButton('<?php echo $iType; ?>');" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="addInsuranceAry[<?php echo $iType; ?>][fInsuranceUptoPrice]" id="fInsuranceUptoPrice_<?php echo $iType; ?>" value="<?php echo $fInsuranceUptoPrice; ?>">
                    </td>
                    <td>
                        <input type="text" onkeypress="return format_decimat(this,event)" onkeyup="enableInsuranceBuySellButton('<?php echo $iType; ?>');" class="addInsurateRatesFields_<?php echo $iType; ?>" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="addInsuranceAry[<?php echo $iType; ?>][fInsuranceRate]" id="fInsuranceRate_<?php echo $iType; ?>" value="<?php echo $fInsuranceRate; ?>">
                    </td> 
                    <td> 
                        <input type="text" onkeypress="return format_decimat(this,event)" onkeyup="enableInsuranceBuySellButton('<?php echo $iType; ?>');" class="addInsurateRatesFields_<?php echo $iType; ?>" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="addInsuranceAry[<?php echo $iType; ?>][fInsuranceMinPrice]" id="fInsuranceMinPrice_<?php echo $iType; ?>" value="<?php echo $fInsuranceMinPrice; ?>">
                    </td>
		</tr>  
                <tr>
                    <td colspan="3">&nbsp;</td>
                    <td colspan="5" style="text-align:right;"><br>
                        <a href="javascript:void(0);" class="button1" style="opacity:0.4" id="insurance_rates_add_button_<?php echo $iType; ?>"><span style="min-width:50px;" id="dummy_services_add_button_span"><?php echo (($idInsurance>0)?t($t_base.'fields/save'):t($t_base.'fields/add')); ?></span></a>
                        <a href="javascript:void(0);" class="button2" style="opacity:1.0" id="insurance_rates_clear_button_<?php echo $iType; ?>" ><span>Clear</span></a> 
                    </td>
                </tr>
            </table>
            <input type="hidden" name="addInsuranceAry[<?php echo $iType; ?>][iInsuranceType]" id="iInsuranceType_<?php echo $iType; ?>" value="<?php echo $iType; ?>">
            <input type="hidden" name="addInsuranceAry[iType]" id="iType_<?php echo $iType; ?>" value="<?php echo $iType; ?>"> 
            <input type="hidden" name="addInsuranceAry[<?php echo $iType; ?>][idInsurance]" id="idInsurance_<?php echo $iType; ?>" value="<?php echo $idInsurance; ?>"> 
        </form>
        <script type="text/javascript">
            enableInsuranceBuySellButton('<?php echo $iType; ?>');
        </script>
    </div>
	<?php
}
function display_insurance_try_it_out($kInsurance)
{
    ?>
    <div id="insurance_try_it_out_form_container_div">
        <?php echo display_try_it_out_search_form($kInsurance); ?>
    </div> 
    <br class="clear-all">
    <div id="insurance_try_it_out_results_container_div">
        <?php 
            //echo display_try_it_out_result($insuranceTryResultAry);
        ?>
    </div>
    <?php
}

function display_try_it_out_search_form($kInsurance)
{
    if(!empty($kInsurance->arErrorMessages))
    {
        $formId = 'insurance_form_type_'.$iType;
        $szValidationErrorKey = '';
        foreach($kInsurance->arErrorMessages as $errorKey=>$errorValue)
        {
            if(empty($szValidationErrorKey))
            {
                $szValidationErrorKey = $errorKey;
            }
            if($errorKey=='szSpecialError')
            {
                $szSpecialErrorMessage = $errorValue ;
            }
            else
            {
                ?>
                <script type="text/javascript">
                    $("#"+'<?php echo $errorKey; ?>').addClass('red_border');
                </script>
            <?php }
        }
    }
    
    if(!empty($_POST['insuranceTryitOut']))
    {
        $insuranceTryitOut = $_POST['insuranceTryitOut'];
        
        $szOriginCountryStr = $insuranceTryitOut['szOriginCountryStr'];
        $szDestinationCountryStr = $insuranceTryitOut['szDestinationCountryStr'];
        $idTransportMode = $insuranceTryitOut['idTransportMode'];
        $szCargoType = $insuranceTryitOut['szCargoType'];
        $idGoodsInsuranceCurrency = $insuranceTryitOut['idGoodsInsuranceCurrency'];
        $fValueOfGoods = $insuranceTryitOut['fValueOfGoods'];
        $idCustomerCurrency = $insuranceTryitOut['idCustomerCurrency'];
        $fTotalPriceCustomerCurrency = $insuranceTryitOut['fTotalPriceCustomerCurrency'];
        $idCustomerCountry = $insuranceTryitOut['idCustomerCountry'];
        $idBookingCurrency = $insuranceTryitOut['idBookingCurrency'];
        $iPrivate = $insuranceTryitOut['iPrivate']; 
    }
    else
    {
        $idBookingCurrency=__USD_CURRENCY_ID__;
        $idGoodsInsuranceCurrency=__USD_CURRENCY_ID__;
        $idCustomerCurrency=__USD_CURRENCY_ID__;
    }
    $kConfig = new cConfig();
    $transportModeListAry = array();
    $transportModeListAry = $kConfig->getAllTransportMode(false,false,true,true);
  
    $kVatApplication = new cVatApplication();
    $manualFeeCargoTypeArr = array();
    $manualFeeCargoTypeArr = $kVatApplication->getManualFeeCaroTypeList(); 
    
    $currencyAry = array();
    $currencyAry = $kConfig->getBookingCurrency(false,true);
    
    $allCountriesArr = array();
    $allCountriesArr = $kConfig->getAllCountries(true); 
    
    ?>
    <script type="text/javascript" defer="">
        $().ready(function() {	 
            var autocomplete1,place ; 
            function initialize1() 
            {
                var input1 = document.getElementById('szOriginCountryStr');
                var options = {types: ['regions']};

                var autocomplete1 = new google.maps.places.Autocomplete(input1); 
                $(".pac-container:last").attr("id", 'szOriginCountryStrPacContainer');
                google.maps.event.addListener(autocomplete1, 'place_changed', function() 
                {    
                    var szOriginAddress_js = $("#szOriginCountryStr").val();   
                    //checkFromAddressAdmin(szOriginAddress_js,'FROM_COUNTRY','',true);
                }); 
                var input2 = document.getElementById('szDestinationCountryStr');
                var autocomplete2 = new google.maps.places.Autocomplete(input2); 
                $(".pac-container:last").attr("id", 'szDestinationCountryStrPacContainer');
                google.maps.event.addListener(autocomplete2, 'place_changed', function() 
                {
                    var szDestinationAddress_js = $("#szDestinationCountryStr").val();  
                    //checkFromAddressAdmin(szDestinationAddress_js,'TO_COUNTRY','',true);
                }); 

                $('#szOriginCountryStr').on('paste focus keypress keyup', function (e){  
                    $('#szOriginCountryStr').bind('keydown keyup keypress click', function (e) {  
                        var pac_container_id = 'szOriginCountryStrPacContainer';
                        if($("#"+pac_container_id).length)
                        {
                            //This means id already there no need for further checks
                        }
                        else
                        {
                            $(".pac-container").each(function(pac)
                            {
                                var disp = $(this).css('display'); 
                                if(disp!='none')
                                {
                                    $(this).attr('id',pac_container_id);
                                }
                            });
                        }
                        var key_code = e.keyCode || e.which;
                        //console.log("ORG Code: "+key_code);
                        if (key_code == 9 || key_code == 13) {
                            prefillAddressOnTabPress('szOriginCountryStr',pac_container_id);
                        }  
                    }); 
                });

                $('#szDestinationCountryStr').on('paste focus', function (e){  
                    $('#szDestinationCountryStr').bind('keydown keyup keypress click', function (e) {  
                        var pac_container_id = 'szDestinationCountryStrPacContainer';
                        if($("#"+pac_container_id).length)
                        {
                            //This means id already there no need for further checks 
                            //console.log("div exist called");
                        }
                        else
                        {
                            var iFoundDiv = 1;
                            $(".pac-container").each(function(pac)
                            {
                                var disp = $(this).css('display');  
                                if(disp!='none')
                                {
                                    $(this).attr('id',pac_container_id);  
                                    iFoundDiv = 2;
                                } 
                            });

                            if(iFoundDiv==1)
                            {
                                $(".pac-container").each(function(pac)
                                {
                                    var container_id = $(this).attr('id');  
                                    if(container_id=='' || container_id === undefined)
                                    {
                                        $(this).attr('id',pac_container_id); 
                                        return true;
                                    }
                                });
                            }
                        } 
                        //console.log("Dest Code: "+e.keyCode);
                        if (e.keyCode == 9 || e.keyCode == 13) {
                            prefillAddressOnTabPress('szDestinationCountryStr',pac_container_id);
                        }  
                    });
                }); 
            } 
            function prefillAddressOnTabPress(input_field_id,pac_container_id)
            {  
                $(".pac-container").each(function(pac){
                   var disp = $(this).css('display'); 
                   var contaier_id = $(this).attr('id');  

                   //console.log("Container: "+contaier_id + " pac id: "+pac_container_id); 
                    if(contaier_id==pac_container_id)
                    {
                        var spanObj = $(this).children(".pac-item:first" );
                        var firstResult = ""; 
                        spanObj.children("span").each(function(){
                            var spanText = $(this).text(); 
                            spanText = jQuery.trim(spanText);
                            //console.log("Span Text: "+spanText);
                            if(spanText!='')
                            {
                                if(firstResult=='')
                                {
                                    firstResult += spanText;
                                }
                                else
                                {
                                    firstResult += ", "+ spanText;
                                }
                                //$(this).children( ".pac-item:first" ).addClass("pac-selected");
                                //$(this).css("display","none"); 
                                if(jQuery.trim(input_field_id)=='szOriginCountryStr')
                                {
                                    //console.log("Mathed: "+firstResult);
                                    $("#szOriginCountryStr").val(firstResult);
                                    $("#szOriginCountryStr").select();
                                }
                                else if(jQuery.trim(input_field_id)=='szDestinationCountryStr')
                                { 
                                    //console.log("Not Mathed");
                                    $("#szDestinationCountryStr").val(firstResult); 
                                    $("#szDestinationCountryStr").select();
                                } 
                            }
                        });  
                    }
                }); 
            } 
            function clean_pac_container()
            { 
                $("div").each(function(index){
                    if($( this ).hasClass("pac-container"))
                    {
                        $( this ).remove();
                    }
                }); 
            }  
            initialize1();   
            enableInsuranceTryitoutForm();
    });  
    </script> 
    <h4><strong>Try it out</strong></h4>
    <form id="insurance_try_it_out_form" name="insurance_try_it_out_form" method="post" action="javascript:void(0);"> 
        <?php if(!empty($szSpecialErrorMessage)){ ?>
            <p style="font-style:italic;color:red;font-size:14px;"> <?php echo $szSpecialErrorMessage; ?></p>
        <?php  }  ?>
        <div class="clearfix email-fields-container" style="width:100%;"> 
            <span class="quote-field-container wd-30">
                From<br>
                <input type="text" name="insuranceTryitOut[szOriginCountryStr]" placeholder="Start typing" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" id="szOriginCountryStr" value="<?php echo $szOriginCountryStr; ?>" onkeyup="enableInsuranceTryitoutForm();">
            </span> 
            <span class="quote-field-container wds-30">
                To<br>
                <input type="text" name="insuranceTryitOut[szDestinationCountryStr]" placeholder="Start typing" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" id="szDestinationCountryStr" value="<?php echo $szDestinationCountryStr; ?>" onkeyup="enableInsuranceTryitoutForm();">
            </span>
            <span class="quote-field-container wds-20">
                Mode<br> 
                <select name="insuranceTryitOut[idTransportMode]" id="idTransportMode" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onchange="enableInsuranceTryitoutForm();">
                   <option value="">Select</option>
                   <?php
                       if(!empty($transportModeListAry))
                       {
                          foreach($transportModeListAry as $transportModeListArys)
                          {
                              if($transportModeListArys['id']==8)
                              {
                                  //On try-it-out we don't display All mode option
                                  continue;
                              }
                              ?>
                           <option value="<?php echo $transportModeListArys['id']; ?>" <?php echo (($transportModeListArys['id']==$idTransportMode)?'selected':''); ?>><?php echo $transportModeListArys['szShortName']; ?></option>
                           <?php
                          }
                       }
                   ?> 
                </select>
            </span>
            <span class="quote-field-container wds-20">
                Commodity<br>
                <select name="insuranceTryitOut[szCargoType]" id="szCargoType" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onchange="enableInsuranceTryitoutForm();" >
                    <option value="">Select</option>
                   <?php
                        if(!empty($manualFeeCargoTypeArr))
                        {
                            foreach($manualFeeCargoTypeArr as $manualFeeCargoTypeArrs)
                            {
                               ?>
                               <option value="<?php echo $manualFeeCargoTypeArrs['szCode']; ?>" <?php echo (($manualFeeCargoTypeArrs['szCode']==$szCargoType)?'selected':''); ?>><?php echo $manualFeeCargoTypeArrs['szName']; ?></option>
                               <?php
                            }
                        }
                   ?> 
                </select> 
            </span> 
        </div>  
        <div class="clearfix email-fields-container" style="width:100%;"> 
            <span class="quote-field-container wd-19">
                Cargo value<br>
                <select name="insuranceTryitOut[idGoodsInsuranceCurrency]" class="wd-40" id="idGoodsInsuranceCurrency" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onchange="enableInsuranceTryitoutForm();">
                    
                    <?php
                        if(!empty($currencyAry))
                        {
                           foreach($currencyAry as $currencyArys)
                           {
                            ?>
                            <option value="<?php echo $currencyArys['id']; ?>" <?php echo (($currencyArys['id']==$idGoodsInsuranceCurrency)?'selected':''); ?>><?php echo $currencyArys['szCurrency']; ?></option>
                            <?php
                           }
                        }
                    ?> 
                </select> 
                <input type="text" name="insuranceTryitOut[fValueOfGoods]" class="wd-56" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" id="fValueOfGoods" value="<?php echo $fValueOfGoods; ?>" onkeyup="enableInsuranceTryitoutForm();">
            </span> 
            <span class="quote-field-container wds-19">
                Transportation value<br>
                <select name="insuranceTryitOut[idCustomerCurrency]" class="wd-40" id="idCustomerCurrency" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onchange="enableInsuranceTryitoutForm();"> 
                    <?php
                        if(!empty($currencyAry))
                        {
                           foreach($currencyAry as $currencyArys)
                           {
                                ?>
                                <option value="<?php echo $currencyArys['id']; ?>" <?php echo (($currencyArys['id']==$idCustomerCurrency)?'selected':''); ?>><?php echo $currencyArys['szCurrency']; ?></option>
                                <?php
                           }
                        }
                    ?> 
                </select> 
                <input type="text" name="insuranceTryitOut[fTotalPriceCustomerCurrency]" class="wd-56"  id="fTotalPriceCustomerCurrency" value="<?php echo $fTotalPriceCustomerCurrency; ?>" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onkeyup="enableInsuranceTryitoutForm();" >
            </span>
            <span class="quote-field-container wds-14">
                Customer country<br>
                <select name="insuranceTryitOut[idCustomerCountry]" id="idCustomerCountry" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onchange="enableInsuranceTryitoutForm();">
                    <option value="">Select</option>
                    <?php
                        if(!empty($allCountriesArr))
                        {
                            foreach($allCountriesArr as $allCountriesArrs)
                            {
                                ?>
                                <option value="<?php echo $allCountriesArrs['id']; ?>" <?php echo (($allCountriesArrs['id']==$idCustomerCountry)?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName']; ?></option>
                                <?php
                            }
                        }
                    ?> 
                </select>
            </span>
            <span class="quote-field-container wds-9">
                Currency<br>
                <select name="insuranceTryitOut[idBookingCurrency]" id="idBookingCurrency" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onchange="enableInsuranceTryitoutForm();">
                    <?php
                        if(!empty($currencyAry))
                        {
                            foreach($currencyAry as $currencyArys)
                            {
                                ?>
                                <option value="<?php echo $currencyArys['id']; ?>" <?php echo (($currencyArys['id']==$idBookingCurrency)?'selected':''); ?>><?php echo $currencyArys['szCurrency']; ?></option>
                                <?php
                            }
                        }
                    ?> 
                </select>
            </span>
            <span class="quote-field-container wds-8">&nbsp;<br>  
                <span class="checkbox-container">
                    <input type="checkbox" name="insuranceTryitOut[iPrivate]" id="iPrivate" value="1" <?php if($iPrivate==1){ ?>checked<?php } ?> onclick="enableInsuranceTryitoutForm();"> Private
                </span>
            </span>
            <span class="quote-field-container wds-31" style="text-align:right;">&nbsp;<br>
                <a href="javascript:void(0)" class="button1 mwd-50" style="opacity:0.4" id="insurance_try_it_out_test_button"><span>TEST</span></a>
                <a href="javascript:void(0)" class="button2 mwd-50" onclick="clearInsuranceTryitOutForm();"><span>Clear</span></a>
            </span>
        </div> 
    </form>  
   <?php
} 

function display_try_it_out_result($insuranceTryResultAry,$kInsurance,$idCustomerCurrency)
{
    $t_base="management/insurance/";
    //print_r($insuranceTryResultAry);
    $szCargoValueLocal = $insuranceTryResultAry['szGoodsInsuranceCurrency']." ".number_format((float)$insuranceTryResultAry['fValueOfGoods'],2);
    $szCargoValueCustomer = $insuranceTryResultAry['szBookingCurrency']." ".number_format((float)$insuranceTryResultAry['fValueOfGoodsBookingCurrency'],2);
    
    $szTransportationValueLocal = $insuranceTryResultAry['szCustomerCurrency']." ".number_format((float)$insuranceTryResultAry['fTotalPriceCustomerCurrency'],2);
    $szTransportationValueCustomer = $insuranceTryResultAry['szBookingCurrency']." ".number_format((float)$insuranceTryResultAry['fTransportationCostBookingCurrency'],2);
    
    $szVatValueLocal = $insuranceTryResultAry['szCustomerCurrency']." ".number_format((float)$insuranceTryResultAry['fTotalVat'],2);
    $szVatValueCustomer = $insuranceTryResultAry['szBookingCurrency']." ".number_format((float)$insuranceTryResultAry['fTotalVatBookingCurrency'],2);
    
    $szImaginaryProfitValueLocal = $insuranceTryResultAry['szImaginaryProfitCurrency']." ".number_format((float)$insuranceTryResultAry['fImaginaryProfit'],2);
    $szImaginaryProfitValueCustomer = $insuranceTryResultAry['szBookingCurrency']." ".number_format((float)$insuranceTryResultAry['iImaginaryProfitBookingCurrency'],2);
    
    $szTotalInsuranceValueLocal = $insuranceTryResultAry['fTotalInsuranceValueLocalCurrency']." ".number_format((float)$insuranceTryResultAry['fTotalInsuranceValueLocal'],2);
    $szTotalInsuranceValueCustomer = $insuranceTryResultAry['szBookingCurrency']." ".number_format((float)$insuranceTryResultAry['fTotalInsuranceValueCustomer'],2);
     
    $kWarehouseSearch = new cWHSSearch();
    if($idCustomerCurrency==1)
    {
        $fTotalInsuranceExcahngeRate=1.0000;
        $szTotalInsuranceValueLocal = $insuranceTryResultAry['szBookingCurrency']." ".number_format((float)$insuranceTryResultAry['fTotalInsuranceValueCustomer'],2);
    }
    else
    {
        $resultAry = $kWarehouseSearch->getCurrencyDetails($idCustomerCurrency);		

        $idCurrency = $resultAry['idCurrency'];
        $szCurrency = $resultAry['szCurrency'];
        $fTotalInsuranceExcahngeRate = $resultAry['fUsdValue'];
                    
        $szTotalInsuranceValueLocal = $szCurrency." ".number_format((float)$insuranceTryResultAry['fTotalInsuranceValueCustomer']/$fTotalInsuranceExcahngeRate,2);
    }
    $szTotalInsuranceSellRateValue='';
    if($insuranceTryResultAry['idInsuranceMinCurrency']>0)
    {
        if($insuranceTryResultAry['idInsuranceMinCurrency']==1)
        {
           // $fTotalInsuranceExcahngeRate=1.0000;
            $szTotalInsuranceSellRateValue = "USD ".number_format((float)$insuranceTryResultAry['fTotalInsuranceValueCustomer'],2);
        }
        else
        {
            $resultAry = $kWarehouseSearch->getCurrencyDetails($insuranceTryResultAry['idInsuranceMinCurrency']);		

            $idCurrency = $resultAry['idCurrency'];
            $szCurrency = $resultAry['szCurrency'];
            $fTotalInsuranceExcahngeRateMinCurrency = $resultAry['fUsdValue'];

            $szTotalInsuranceSellRateValue = $szCurrency." ".number_format((float)$insuranceTryResultAry['fTotalInsuranceValueCustomer']/$fTotalInsuranceExcahngeRateMinCurrency,2);
        }
    }
    
    $szTotalInsuranceBuyRateValue='';
    if($insuranceTryResultAry['idInsuranceMinCurrency_buyRate']>0)
    {
        if($insuranceTryResultAry['idInsuranceMinCurrency_buyRate']==1)
        {
           // $fTotalInsuranceExcahngeRate=1.0000;
            $szTotalInsuranceBuyRateValue = "USD ".number_format((float)$insuranceTryResultAry['fTotalInsuranceValueCustomer'],2);
        }
        else
        {
            $resultAry = $kWarehouseSearch->getCurrencyDetails($insuranceTryResultAry['idInsuranceMinCurrency_buyRate']);		

            $idCurrency = $resultAry['idCurrency'];
            $szCurrency = $resultAry['szCurrency'];
            $fTotalInsuranceExcahngeRateMinCurrency = $resultAry['fUsdValue'];

            $szTotalInsuranceBuyRateValue = $szCurrency." ".number_format((float)$insuranceTryResultAry['fTotalInsuranceValueCustomer']/$fTotalInsuranceExcahngeRateMinCurrency,2);
        }
    }
    
    $bDisplayEmptyRows = false;
    if(!empty($kInsurance->arErrorMessages['szSpecialError']))
    {
        $szSpecialErrorMessage = $kInsurance->arErrorMessages['szSpecialError'];
        //$bDisplayEmptyRows = true;
    } 
    $idInsuranceRate = $insuranceTryResultAry['idInsuranceRate'];
    $idInsuranceRate_buyRate = $insuranceTryResultAry['idInsuranceRate_buyRate'];
    ?>
    <?php if(!empty($szSpecialErrorMessage)){ ?>
        <p style="font-style:italic;color:red;font-size:14px;"> <?php echo $szSpecialErrorMessage; ?></p>
    <?php  }  ?>
    <table cellpadding="0" cellspacing="0" width="100%" class="format-4">
        <tr>  
            <td class="wd-35"><strong><?=t($t_base.'fields/items_for_insurance')?></strong></td>
            <td class="wd-13"><strong><?=t($t_base.'fields/rate')?></strong></td>
            <td class="wd-13"><strong><?=t($t_base.'fields/minimum')?></strong></td>
            <td class="wd-13"><strong><?=t($t_base.'fields/value')?></strong></td>
            <td class="wd-13"><strong><?=t($t_base.'fields/roe')?></strong></td>
            <td class="wd-13"><strong><?=t($t_base.'fields/value')." (".$insuranceTryResultAry['szBookingCurrency'].") ";?></strong></td> 
        </tr> 
        <tr>  
            <td>Cargo Value</td>
            <td>-</td>
            <td>-</td>
            <td><?php echo display_insurance_value($szCargoValueLocal); ?></td>
            <td><?php echo number_format((float)$insuranceTryResultAry['fGodsBookingExchangeRate'],4); ?></td>
            <td><?php echo display_insurance_value($szCargoValueCustomer); ?></td> 
        </tr>
        <tr>  
            <td>Transportation Value</td>
            <td>-</td>
            <td>-</td>
            <td><?php echo display_insurance_value($szTransportationValueLocal); ?></td>
            <td><?php echo number_format((float)$insuranceTryResultAry['fCustomerBookingCurrency'],4); ?></td>
            <td><?php echo display_insurance_value($szTransportationValueCustomer); ?></td> 
        </tr>
        <tr>  
            <td>VAT on transportation (private only)</td>
            <td><?php if((int)$insuranceTryResultAry['fVatPercentage']>0){ echo number_format((float)$insuranceTryResultAry['fVatPercentage'],3)."%";}else { echo number_format((float)$insuranceTryResultAry['fVatPercentage'],3)."%";} ?></td>
            <td>-</td>
            <td><?php echo display_insurance_value($szVatValueLocal); ?></td>
            <td><?php echo number_format((float)$insuranceTryResultAry['fCustomerBookingCurrency'],4); ?></td>
            <td><?php echo display_insurance_value($szVatValueCustomer); ?></td> 
        </tr>
        <tr>  
            <td><?php echo "10% imaginary profit (business only)"; ?></td>
            <td><?php echo number_format((float)$insuranceTryResultAry['iImaginaryProfitPecentage'],3)."%"; ?></td>
            <td>-</td>
            <td><?php echo display_insurance_value($szImaginaryProfitValueLocal); ?></td>
            <td><?php echo number_format((float)$fTotalInsuranceExcahngeRate,4); ?></td>
            <td><?php echo display_insurance_value($szImaginaryProfitValueCustomer); ?></td> 
        </tr>
        <tr style="font-weight: bold;">  
            <td>Total insurance value</td>
            <td>-</td>
            <td>-</td>
            <td><?php echo display_insurance_value($szTotalInsuranceValueLocal); ?></td>
            <td><?php echo number_format((float)$fTotalInsuranceExcahngeRate,4); ?></td>
            <td><?php echo display_insurance_value($szTotalInsuranceValueCustomer); ?></td> 
        </tr>
        <tr>
            <td colspan="6">&nbsp;</td>
        </tr>
        <?php if(!empty($bDisplayEmptyRows)){ ?>
            <tr>  
                <td>Insurance buy rate</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
            </tr>
            <tr>  
                <td>Insurance sell rate</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
            </tr>
        <?php } else { ?>
            <tr>  
                <td>Insurance buy rate <?php if($szTotalInsuranceBuyRateValue!=''){ ?> @ <?php echo display_insurance_value($szTotalInsuranceBuyRateValue); ?> value <?php }?></td>
                <td><?php if($idInsuranceRate_buyRate>0){ echo number_format((float)$insuranceTryResultAry['fInsuranceRate_buyRate'],3)."%";}else { echo "-" ;}  ?></td>
                <td><?php if($idInsuranceRate_buyRate>0){ echo $insuranceTryResultAry['szInsuranceMinCurrency_buyRate']." ".number_format($insuranceTryResultAry['fInsuranceMinPrice_buyRate'],2);}else { echo "-" ;}  ?></td>
                <td><?php if($idInsuranceRate_buyRate>0){ echo $insuranceTryResultAry['szInsuranceMinCurrency_buyRate']." ".number_format((float)$insuranceTryResultAry['fTotalInsuranceCostForBooking_buyRate'],2);}else { echo "-" ;}  ?></td>
                <td><?php if($idInsuranceRate_buyRate>0){ echo number_format((float)$insuranceTryResultAry['fInsuranceBookingExchangeRate_buyRate'],4);}else { echo "-" ;}  ?></td>
                <td><?php if($idInsuranceRate_buyRate>0){ echo $insuranceTryResultAry['szBookingCurrency']." ".number_format((float)$insuranceTryResultAry['fTotalInsuranceCostForBookingCustomerCurrency_buyRate'],2);}else { echo "-" ;}  ?></td>
            </tr>
            <tr>  
                <td>Insurance sell rate <?php if($szTotalInsuranceSellRateValue!=''){ ?> @ <?php echo display_insurance_value($szTotalInsuranceSellRateValue); ?> value <?php }?></td>
                <td><?php if($idInsuranceRate>0){ echo number_format((float)$insuranceTryResultAry['fInsuranceRate'],3)."%";}else { echo "-" ;} ?></td>
                <td><?php if($idInsuranceRate>0){ echo $insuranceTryResultAry['szInsuranceMinCurrency']." ".number_format($insuranceTryResultAry['fInsuranceMinPrice'],2);}else { echo "-" ;}  ?></td>
                <td><?php if($idInsuranceRate>0){ echo $insuranceTryResultAry['szInsuranceMinCurrency']." ".number_format((float)$insuranceTryResultAry['fTotalInsuranceCostForBooking'],2);}else { echo "-" ;}  ?></td>
                <td><?php if($idInsuranceRate>0){ echo number_format((float)$insuranceTryResultAry['fInsuranceBookingExchangeRate'],4);}else { echo "-" ;}  ?></td>
                <td><?php if($idInsuranceRate>0){ echo $insuranceTryResultAry['szBookingCurrency']." ".number_format((float)$insuranceTryResultAry['fTotalInsuranceCostForBookingCustomerCurrency'],2);}else { echo "-" ;} ?></td>
            </tr>
        <?php } ?> 
            <input type="hidden" name="idInsuranceSellRateApplied" id="idInsuranceSellRateApplied" value="<?php echo $idInsuranceRate; ?>">
            <input type="hidden" name="idInsuranceBuyRateApplied" id="idInsuranceBuyRateApplied" value="<?php echo $idInsuranceRate_buyRate; ?>">
    </table>
    <?php
}

function display_insurance_value($szInsuranceString)
{
    if(!empty($szInsuranceString))
    {
        return $szInsuranceString;
    }
    else
    {
        return "-";
    }
} 
?>