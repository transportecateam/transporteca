<?php
ob_start();
session_start(); 
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(E_ALL); 

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );

require_once(__APP_PATH__ . "/inc/constants.php");
require_once(__APP_PATH__ . "/inc/functions.php");

//Configuration
$access = "7D117F2BF75DE788";
$userid = "efisherqvm";
$passwd = "Nissan1980";
$wsdl = __APP_PATH__."/wsdl/Track.wsdl";
$operation = "ProcessTrack";
$endpointurl = 'https://onlinetools.ups.com/webservices/Track';
$outputFileName = "XOLTResult.xml";

function processTrack()
{
    //create soap request
    $req['RequestOption'] = '15';
    $tref['CustomerContext'] = 'Add description here';
    $req['TransactionReference'] = $tref;
    $request['Request'] = $req;
    $request['InquiryNumber'] = '1ZRE98009098651192';
    $request['TrackingOption'] = '02';

    echo "Request.......\n";
    print_r($request);
    echo "\n\n";
    return $request;
}

try
{

  $mode = array
  (
       'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
       'trace' => 1
  );

  // initialize soap client
      $client = new SoapClient($wsdl , $mode);

      //set endpoint url
      $client->__setLocation($endpointurl);


  //create soap header
  $usernameToken['Username'] = $userid;
  $usernameToken['Password'] = $passwd;
  $serviceAccessLicense['AccessLicenseNumber'] = $access;
  $upss['UsernameToken'] = $usernameToken;
  $upss['ServiceAccessToken'] = $serviceAccessLicense;

  $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0','UPSSecurity',$upss);
  $client->__setSoapHeaders($header);


  //get response
  $resp = $client->__soapCall($operation ,array(processTrack()));

  $filename = __APP_PATH__."/logs/ups_tracking_api_".date('Ymd').".log";
  echo "Log Path: ".$filename."<br><br>";
  $f = fopen($filename, "a");
  $value = print_R($resp,true);
  fwrite($f, $value);
  fclose($f);

  //get status
  echo "Response Status: " . $resp->Response->ResponseStatus->Description ."\n";

  //save soap request and response to file
  $fw = fopen($outputFileName , 'w');
  fwrite($fw , "Request: \n" . $client->__getLastRequest() . "\n");
  fwrite($fw , "Response: \n" . $client->__getLastResponse() . "\n");
  fclose($fw); 
}
catch(Exception $ex)
{
    print_r ($ex);
}

?>
