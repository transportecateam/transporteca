<?php
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_header.php" );
$t_base = "Booking/MyBooking/";
$t_base_error = "Error";
$idBooking=$_REQUEST['idBooking'];

$kForwarder = new cForwarder();
$kBooking = new cBooking();
if(!empty($_POST['ratingArr']))
{
    if($kBooking->addBookingRating($_POST['ratingArr']))
    {
            //show_booking_list('$showBookingFlag');
            ?>
                    <script type="text/javascript">
                    update_rating_div('<?=$idBooking?>');
                    </script>	
            <?php
    }
    else
    {
            if($_POST['ratingArr']['iRating']>0)
            {
    ?>
            <script type="text/javascript">
            customer_forwarder_rating('<?=$_POST['ratingArr']['iRating']?>','select');
            </script>
            <?php
            }
    }
        
}
$myBooking=$kBooking->getBookingDetails($idBooking);
$kForwarder->load($myBooking['idForwarder']);
if($idBooking>0 && $_REQUEST['showFlag']!='details' && $_REQUEST['showFlag']!='rating_update_link')
{
?>

<form name="ratingForm_<?=$idBooking?>" id="ratingForm_<?=$idBooking?>" method="post">
<div class="booking-box-part1">
<?php
if(!empty($kBooking->arErrorMessages)){
?>
<div id="regError" class="errorBox ">
<div class="header"><?=t($t_base_error.'/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
	foreach($kBooking->arErrorMessages as $key=>$values)
	{
	?><li><?=$values?></li>
	<?php	
	}
?>
</ul>
</div>
</div>
<?php }?>
		<div class="fl-70">
                    <p><strong><?=t($t_base.'title/how_did_dsv');?> <?=$kForwarder->szDisplayName?> <?=t($t_base.'title/do');?>?</strong></p>
			<div class="oh" style="padding-bottom: 0;">
                            <?php  if($myBooking['iQuotesStatus']==__BOOKING_QUOTES_STATUS_WON__){ ?>
                                <p class="fl-80"><?=t($t_base.'title/was_your_shipment_available_on_time');?>?</p>
                            <?php }else {?>
                                <p class="fl-80"><?=t($t_base.'title/was_your_shipment');?> <?=date('d/m/Y',strtotime($myBooking['dtAvailable']))?> <?=t($t_base.'title/at');?> <?=date('H:i',strtotime($myBooking['dtAvailable']))?>?</p>
                            <?php }?>
				
				<p class="fr-5 checkbox-ab" style="margin-left:7px;"><input type="radio" name="ratingArr[iTimely]" id="iTimely_<?=$idBooking?>" value="2" <?php if($_POST['ratingArr']['iTimely']=='2'){ ?> checked <?php }?>/><?php echo t($t_base.'fields/rate_no');?></p>
				<p class="fr-5 checkbox-ab"><input type="radio" name="ratingArr[iTimely]" id="iTimely_<?=$idBooking?>" value="1" <?php if($_POST['ratingArr']['iTimely']=='1'){ ?> checked <?php }?>/><?php echo t($t_base.'fields/rate_yes');?></p>
			</div>
			<div class="oh">
				<p class="fl-80"><?=t($t_base.'title/was_the_customer_service');?>?</p>
				<p class="fr-5 checkbox-ab" style="margin-left:7px;"><input type="radio" name="ratingArr[iCourtous]" id="iCourtous" value="2" <?php if($_POST['ratingArr']['iCourtous']=='2'){ ?> checked <?php }?> /><?=t($t_base.'fields/rate_no');?></p>
				<p class="fr-5 checkbox-ab"><input type="radio" name="ratingArr[iCourtous]" id="iCourtous" value="1" <?php if($_POST['ratingArr']['iCourtous']=='1'){ ?> checked <?php }?> /><?=t($t_base.'fields/rate_yes');?></p>
			</div>
			<div class="oh">
				<p class="fl-49"><?=t($t_base.'title/forwarder_rating');?>:</p>				
				<div id="forwader_ratings" class="fl-49" style="margin:3px 0 0 0">
            		<a href="javascript:void(0)" onClick="javascript:customer_forwarder_rating('1','select','<?=t($t_base.'messages/rating_msg_1')?>','<?=$idBooking?>');"
					onmouseover="javascript:customer_forwarder_rating('1','mouse_over','<?=t($t_base.'messages/rating_msg_1')?>','<?=$idBooking?>');"  onmouseout="javascript:customer_forwarder_rating('0','mouse_out','0','<?=$idBooking?>');" class="RatingBox1" id="booking_star_1_<?=$idBooking?>">&nbsp;</a>
					<a href="javascript:void(0)" onClick="javascript:customer_forwarder_rating('2','select','<?=t($t_base.'messages/rating_msg_2')?>','<?=$idBooking?>');"
					onmouseover="javascript:customer_forwarder_rating('2','mouse_over','<?=t($t_base.'messages/rating_msg_2')?>','<?=$idBooking?>');" onmouseout="javascript:customer_forwarder_rating('0','mouse_out','0','<?=$idBooking?>');" class="RatingBox1" id="booking_star_2_<?=$idBooking?>">&nbsp;</a>
					<a href="javascript:void(0)" onClick="javascript:customer_forwarder_rating('3','select','<?=t($t_base.'messages/rating_msg_3')?>','<?=$idBooking?>');"
					onmouseover="javascript:customer_forwarder_rating('3','mouse_over','<?=t($t_base.'messages/rating_msg_3')?>','<?=$idBooking?>');" onmouseout="javascript:customer_forwarder_rating('0','mouse_out','0','<?=$idBooking?>');" class="RatingBox1" id="booking_star_3_<?=$idBooking?>">&nbsp;</a>
					<a href="javascript:void(0)" onClick="javascript:customer_forwarder_rating('4','select','<?=t($t_base.'messages/rating_msg_4')?>','<?=$idBooking?>');"
					onmouseover="javascript:customer_forwarder_rating('4','mouse_over','<?=t($t_base.'messages/rating_msg_4')?>','<?=$idBooking?>');" onmouseout="javascript:customer_forwarder_rating('0','mouse_out','0','<?=$idBooking?>');" class="RatingBox1" id="booking_star_4_<?=$idBooking?>">&nbsp;</a>
					<a href="javascript:void(0)" onClick="javascript:customer_forwarder_rating('5','select','<?=t($t_base.'messages/rating_msg_5')?>','<?=$idBooking?>');"
					onmouseover="javascript:customer_forwarder_rating('5','mouse_over','<?=t($t_base.'messages/rating_msg_5')?>','<?=$idBooking?>');" onmouseout="javascript:customer_forwarder_rating('0','mouse_out','0','<?=$idBooking?>');" class="RatingBox1" id="booking_star_5_<?=$idBooking?>">&nbsp;</a>
					<div id="show_rating_msg_<?=$idBooking?>" style="color:#000000;font-family: Calibri,arial,tahoma,verdana;font-size:14px;max-height: 19px;min-height: 19px;line-height: 19px;"></div>
				</div>
				<div id="rating_value_<?=$idBooking?>"></div>
				<input type="hidden" value="" name="ratingArr[iRating]" id="iRating_<?=$idBooking?>">
				<input type="hidden" value="" name="iRating_msg" id="iRating_msg_<?=$idBooking?>">
				
			</div>
			<p><?=t($t_base.'title/tell_other_about_forwarder');?>:<br /><em class="f-size-12">(<?=t($t_base.'title/cust_exper_time_value');?>)</em></p>
			<p class="booking-textarea">
                            <textarea name="ratingArr[szReview]" rows="3" id="szReview" onkeyup="show_remain_character(this.value,'szReview','<?=$idBooking?>')" maxlength="400"><?=$_POST['ratingArr']['szReview']?></textarea>
                            <span><?=t($t_base.'title/your_name_will_not_display');?> <?=t($t_base.'title/character_remaining');?>: <span id="szReview_<?=$idBooking?>">400</span> </span>
			</p>
			<br/>
			<p><?=t($t_base.'title/send_suggest_for_improvement');?> <?=$kForwarder->szDisplayName?>:</p>
			<p class="booking-textarea">
				<textarea name="ratingArr[szSuggestion]" id="szSuggestion" onkeyup="show_remain_character(this.value,'szSuggestion','<?=$idBooking?>')" maxlength="400"><?=$_POST['ratingArr']['szSuggestion']?></textarea>
				<span><?=t($t_base.'title/this_will_be_sent_only_sdv_1');?> <?=$kForwarder->szDisplayName?> <?=t($t_base.'title/this_will_be_sent_only_sdv_2');?> (<?=$kUser->szFirstName?> <?=$kUser->szLastName?>) <?=t($t_base.'title/displayed');?>. <?=t($t_base.'title/character_remaining');?>: <span id="szSuggestion_<?=$idBooking?>">400</span></span>
			</p>
		</div>
		<div class="buttons">
                    <p class="color"><em><?=t($t_base.'fields/note');?>: <?=t($t_base.'messages/note_1');?>.</em></p>
                    <?php if(strtotime($myBooking['dtAvailable'])>time()){?><p class="color"><em><?=t($t_base.'fields/note');?>: <?=t($t_base.'messages/note_2');?>.</em></p><?php } ?>
                    <p class="color"><em><?=t($t_base.'fields/note');?>: <?=t($t_base.'messages/note_3');?>.</em></p>
                    <div class="rating_buttons">
                    <p>
                        <input type="hidden" name="ratingArr[idBookingRate]" id="idBookingRate" value="<?=$idBooking?>">
                        <input type="hidden" name="ratingArr[idForwarder]" id="idForwarder" value="<?=$myBooking['idForwarder']?>">
                        <input type="hidden" name="idBooking" value="<?=$idBooking?>">
                        <a href="javascript:void(0)" class="button1" style="min-width:100%;box-sizing: border-box;padding:6px 10px;" id="submit_rating_button" onclick="submit_rating_form('<?=$idBooking?>')"><span><?=t($t_base.'fields/submit_feedback');?></span></a> 
                    </p>
                    <p>
                        <a href="javascript:void(0)" class="button2" style="min-width:100%;box-sizing: border-box;padding:6px 10px;" onclick="clear_form('<?=$idBooking?>');"><span><?=t($t_base.'fields/clear_form');?></span></a>
                    </p>
                </div>
            </div>
	</div>
</form>	
<?php }
else if($idBooking>0 && $_REQUEST['showFlag']=='rating_update_link'){?>
<?php if($kBooking->getBookingRatingDetails($idBooking)){ 
	$width=$kBooking->iRating * __RATING_STAR_WIDTH__;
	?>
<div class="oh booking_ratings">
<p align="center" class="fl-49"><a href="javascript:void(0)" onclick="show_booking_rating_details('<?=$idBooking?>','<?=$kBooking->id?>')"><span><?=t($t_base.'links/your_rateing');?></span></a></p>
						
						<div id="ratings-star" class="fl-49">
            			<span style="width:<?=$width?>px;">&nbsp;</span>
</div>

 </div>
<?php }?>
<?php
}
else {
$kBooking->getBookingRatingDetails($idBooking);
	?>
<div id="booking_rating_detail_id_<?=$idBooking?>"></div>	
<div class="booking-box-part1">
<div class="fl-70 rating_content">
	<p><?=t($t_base.'title/thank_you_for_your_feedback');?></p>
	<p><?=t($t_base.'title/was_your_shipment');?> <?=date('d/m/Y',strtotime($myBooking['dtAvailable']))?> <?=t($t_base.'title/at');?> <?=date('H:i',strtotime($myBooking['dtAvailable']))?> ?<em class="color">
	<?php
		if((int)$kBooking->iTimely=='1')
		{
			echo t($t_base.'fields/rate_yes');
		}
		else
		{
			echo t($t_base.'fields/rate_no');
		}
	?>
	</em></p>
	
	<p><?=t($t_base.'title/was_the_customer_service');?>? <em class="color">
	<?php
		if((int)$kBooking->iCourtous=='1')
		{
			echo t($t_base.'fields/rate_yes');
		}
		else
		{
			echo t($t_base.'fields/rate_no');
		}
	?>
	</em></p>
	<p class="rating_comment"><?=t($t_base.'title/your_review_of_sdv_1');?> <?=$kForwarder->szDisplayName?> <?=t($t_base.'title/your_review_of_sdv_2');?>:</p>
	<p class="color"><em><?=$kBooking->szSuggestion;?></em></p>
	
	<p class="rating_comment"><?=t($t_base.'title/your_suggestion_for_improvements');?> <?=$kForwarder->szDisplayName?>:</p>
	<p class="color"><em><?=$kBooking->szReview;?></em></p>	
</div>
<div class="buttons">
<p class="bottom_align" align="right"><a href="javascript:void(0)" class="button2" onclick="show_booking_rating_details('<?=$idBooking?>','<?=$kBooking->id?>');"><span><?=t($t_base.'fields/collapse');?></span></a></p>
</div>
</div>
<script type="text/javascript">
var div_id_new="booking_rating_detail_id_+<?=$idBooking?>";
setTimeout("$('#"+div_id_new+"').focus()", 50);
</script>
<?php }?>