<?php
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

$t_base = "Booking/MyBooking/";
$showBookingFlag=$_REQUEST['showBookingFlag'];

if($showBookingFlag=='')
{
	$showBookingFlag='active';
}

if($showBookingFlag=='active')
{	
	$szMetaTitle = t($t_base.'title/CONFIRM_BOOKING_META_TITLE');
}
else if($showBookingFlag=='hold')
{
	$szMetaTitle = t($t_base.'title/HOLD_BOOKING_META_TITLE');
}
else if($showBookingFlag=='draft')
{
	$szMetaTitle = t($t_base.'title/DRAFT_BOOKING_META_TITLE');
}
else if($showBookingFlag=='archive')
{
	$szMetaTitle = t($t_base.'title/ARCHIVE_BOOKING_META_TITLE');
}

if(!empty($szMetaTitle))
{
	//$szMetaTitle = utf8_decode($szMetaTitle);
	?>
	<script type="text/javascript">
	document.getElementById("metatitle").innerHTML = '<?php echo $szMetaTitle; ?>';
	</script>
	<?php
}
$kBooking = new cBooking();
$idUserArr=array();
if((int)$kUser->id>0){
$userMutliArr='';
if((int)$kUser->iInvited=='1')
{
	$userMutliArr=$kUser->getMultiUserData($kUser->idGroup,true);
}
if(!empty($userMutliArr))
{
	foreach($userMutliArr as $userMutliArrs)
	{
		$idUserArr[]=$userMutliArrs['id'];
		
	}
	$idUserStr=implode(",",$idUserArr);
}
else
{
	$idUserStr=$kUser->id;
}	
$myBookingArr=$kBooking->getAllMyBooking($idUserStr,$showBookingFlag);
}
if(!empty($myBookingArr))
{
//echo $kBooking->minBookingDate;

?>
<style type="text/css" media="screen">
 .layout { padding: 50px; font-family: Georgia, serif; }
 .layout-slider { margin-bottom: 20px; width: 50%; }
 .layout-slider-settings { font-size: 12px; padding-bottom: 10px; }
 .layout-slider-settings pre { font-family: Courier; }
</style>
<form name="searchMyBooking" id="searchMyBooking" method="post">
	<h5><?=t($t_base.'fields/filter');?></h5>
	<?php 
		$minbooktime='0';
		$maxbooktime='0';
		if($kBooking->minBookingDate!='0000-00-00 00:00:00' && $kBooking->maxBookingDate!='0000-00-00 00:00:00')
		{
			if($showBookingFlag=='active' || $showBookingFlag=='archive'){
			$minbooktime=strtotime(convert_time_to_UTC_date(strtotime($kBooking->minBookingDate)));
			$minbook_date=date('Y-m-d',$minbooktime);
			$minbooktime=strtotime($minbook_date);
			$maxbooktime=strtotime(convert_time_to_UTC_date(strtotime($kBooking->maxBookingDate)));
			$maxbook_date=date('Y-m-d',$maxbooktime);
			$maxbooktime=strtotime($maxbook_date);
			
			if($maxbooktime==$minbooktime)
			{		
		 		// $minbooktime=$maxbooktime-86400;
			}
			
			$minbookdate=date('d/m/Y',$minbooktime);
			$maxbookdate=date('d/m/Y',$maxbooktime);
		?>
		
		<?=t($t_base.'fields/booking_date');?><br/>
		<div class="layout-slider" style="width: 90%">
		<span class="filter_top_text" style="display:inline" id="bookingDate_from"><?=$minbookdate?>&nbsp;<?=t($t_base.'fields/filter_to');?>&nbsp;<?=$maxbookdate?></span>
	      <input id="dtbookingdate" type="hidden" name="myBookingSearchArr[dtbookingdate]" value="<?=$minbooktime?>;<?=$maxbooktime?>" />
	       <input id="bookDate" type="hidden" name="myBookingSearchArr[bookDate]" value="<?=$minbookdate?>;<?=$maxbookdate?>" />
	       <span class="range_value_left" ><?=$minbookdate?></span>
	       <span class="range_value_right" id="bookingDate_to"><?=$maxbookdate?></span>
	    </div>
	    <script type="text/javascript" charset="utf-8">
	      jQuery("#dtbookingdate").slider({
	      	 from:<?=$minbooktime ?>,
	      	 to:<?=$maxbooktime?>,
	      	 step: '86400',
	      	 smooth: false,
	      	 round: 0,
	      	 dimension: "&nbsp;",
	      	 skin: "plastic" ,
	      	  onstatechange: function( value ){
	      	 	limit_val = document.getElementById("dtbookingdate").value;
	      	 	if(limit_val!="")
	      	 	{
	      	 		limit_val_arr = limit_val.split(";");
	      	 	
	      	 	//converting milliseconds to date string 
	      	 	var bookingDate_from = convert_time_to_date(limit_val_arr[0]);
	      	 	var bookingDate_to = convert_time_to_date(limit_val_arr[1]);
	      	 	
				document.getElementById("bookingDate_from").innerHTML = bookingDate_from +" "+'<?=t($t_base.'fields/filter_to');?>'+" "+bookingDate_to ;
				document.getElementById("bookDate").value=bookingDate_from +" "+';'+" "+bookingDate_to ;
				//document.getElementById("cutoff_to").innerHTML = cutoff_to;
				}
			} 
	      });
	    </script> 
	    <? }}?>

	<?php
	if($kBooking->minCutOffDate!="" && $kBooking->maxCutOffDate!="" && $kBooking->minCutOffDate!='0000-00-00 00:00:00' && $kBooking->maxCutOffDate!='0000-00-00 00:00:00')
	{
		$mincutofftime='0';
		$maxcutofftime='0';
		$mincutofftime=strtotime(convert_time_to_UTC_date(strtotime($kBooking->minCutOffDate)));
		$mincutoff_date=date('Y-m-d',$mincutofftime);
		$mincutofftime=strtotime($mincutoff_date);
		$maxcutofftime=strtotime(convert_time_to_UTC_date(strtotime($kBooking->maxCutOffDate)));
		$maxcutoff_date=date('Y-m-d',$maxcutofftime);
		$maxcutofftime=strtotime($maxcutoff_date);
		if($maxcutofftime==$mincutofftime)
		{		
		 // $mincutofftime=$maxcutofftime-86400;
		}
		
		$maxcutoffdate=date('d/m/Y',$maxcutofftime);
		$mincutoffdate=date('d/m/Y',$mincutofftime);
		?>
		
		<?=t($t_base.'fields/cut_off_pick_date');?><br/>
		<div class="layout-slider" style="width: 90%">
			 <span class="filter_top_text" style="display:inline" id="cutoffdate_from"><?=$mincutoffdate?>&nbsp;<?=t($t_base.'fields/filter_to');?>&nbsp;<?=$maxcutoffdate?></span>
	      <input id="dtCutoffdate" type="hidden" name="myBookingSearchArr[dtCutoffdate]" value="<?=$mincutofftime?>;<?=$maxcutofftime?>" />
	   	  <input id="cutOffDate" type="hidden" name="myBookingSearchArr[cutOffDate]" value="<?=$mincutoffdate?>;<?=$maxcutoffdate?>" />
	   	   <span class="range_value_left" ><?=$mincutoffdate?></span>
	       <span class="range_value_right" id="cutoff_to"><?=$maxcutoffdate?></span>	
	    </div>
	    <script type="text/javascript" charset="utf-8">
	      jQuery("#dtCutoffdate").slider({
	      	 from:<?=$mincutofftime ?>,
	      	 to:<?=$maxcutofftime?>,
	      	 step: '86400',
	      	 smooth: false,
	      	 round: 0,
	      	 dimension: "&nbsp;",
	      	 skin: "plastic",
		      	 onstatechange: function( value ){
		      	 	limit_val = document.getElementById("dtCutoffdate").value;
		      	 	if(limit_val!="")
		      	 	{
		      	 		limit_val_arr = limit_val.split(";");
		      	 	
		      	 	//converting milliseconds to date string 
		      	 	var cutoff_from = convert_time_to_date(limit_val_arr[0]);
		      	 	var cutoff_to = convert_time_to_date(limit_val_arr[1]);
		      	 	
					document.getElementById("cutoffdate_from").innerHTML = cutoff_from +" "+'<?=t($t_base.'fields/filter_to');?>'+" "+cutoff_to ;
					document.getElementById("cutOffDate").value=cutoff_from +" "+';'+" "+cutoff_to ;
				//document.getElementById("cutoff_to").innerHTML = cutoff_to;
				}
			}  
	      });
	    </script> 
	
		<?php
	}
	if($kBooking->minAvialDate!="" && $kBooking->maxAvialDate!="" && $kBooking->minAvialDate!='0000-00-00 00:00:00' && $kBooking->maxAvialDate!='0000-00-00 00:00:00')
	{
		$minavialtime='0';
		$maxavialtime='0';
		$minavialtime=strtotime(convert_time_to_UTC_date(strtotime($kBooking->minAvialDate)));
		$minavial_date=date('Y-m-d',$minavialtime);
		$minavialtime=strtotime($minavial_date);
		$maxavialtime=strtotime(convert_time_to_UTC_date(strtotime($kBooking->maxAvialDate)));
		$maxavial_date=date('Y-m-d',$maxavialtime);
		$maxavialtime=strtotime($maxavial_date);
		if($minavialtime==$maxavialtime)
		{		
		 // $minavialtime=$maxavialtime-86400;
		}
		
		$maxavialdate=date('d/m/Y',$maxavialtime);
		$minavialdate=date('d/m/Y',$minavialtime);
		?>
		
		<?=t($t_base.'fields/avail_delivery_date');?><br/>
		<div class="layout-slider" style="width: 90%">
			<span class="filter_top_text" style="display:inline" id="availdate_from"><?=$minavialdate?>&nbsp;<?=t($t_base.'fields/filter_to');?>&nbsp;<?=$maxavialdate?></span>
	      <input id="dtAvailable" type="hidden" name="myBookingSearchArr[dtAvailable]" value="<?=$minavialtime?>;<?=$maxavialtime?>" />
	       <input id="availableDate" type="hidden" name="myBookingSearchArr[availableDate]" value="<?=$minavialdate?>;<?=$maxavialdate?>" />
	      <span class="range_value_left" ><?=$minavialdate?></span>
	       <span class="range_value_right" id="availdate_to"><?=$maxavialdate?></span>
	    </div>
	    <script type="text/javascript" charset="utf-8">
	      jQuery("#dtAvailable").slider({
	      	 from:<?=$minavialtime?>,
	      	 to:<?=$maxavialtime?>,
	      	 step: '86400',
	      	 smooth: false,
	      	 round: 0,
	      	 dimension: "&nbsp;",
	      	 skin: "plastic",
		      	 onstatechange: function( value ){
		      	 	limit_val = document.getElementById("dtAvailable").value;
		      	 	if(limit_val!="")
		      	 	{
		      	 		limit_val_arr = limit_val.split(";");
		      	 	
		      	 	//converting milliseconds to date string 
		      	 	var avail_from = convert_time_to_date(limit_val_arr[0]);
		      	 	var avail_to = convert_time_to_date(limit_val_arr[1]);
		      	 //alert(avail_from);
		      	 	//alert(avail_to);
					document.getElementById("availdate_from").innerHTML = avail_from +" "+'<?=t($t_base.'fields/filter_to');?>'+" "+avail_to ;
					document.getElementById("availableDate").value=avail_from +" "+';'+" "+avail_to ;
				//document.getElementById("cutoff_to").innerHTML = cutoff_to;
				}
			} 
	      });
	    </script> 
	    <? }?>

	    
	<p><?=t($t_base.'fields/from');?>
		<br />
			<select size="1" name="myBookingSearchArr[originCountry]" id="originCountry" onchange="search_mybooking();" style="width:120px;">
			<option value="" selected="">All</option>
		<?php 
			if($kBooking->szOriginCityAry)
			{
			  foreach($kBooking->szOriginCityAry as $key=>$values)
			  {?>
			  	<option value="<?=$values?>"><?=$values?></option>
			 <?php
			  }
			}
		?>
		</select>
	</p>
	<p><?=t($t_base.'fields/to');?>
		<br />
		<select size="1" name="myBookingSearchArr[destinationCountry]" id="destinationCountry" onchange="search_mybooking();" style="width:120px;">
			<option value="" selected="">All</option>
			<?php 
			if($kBooking->szDestinationCity)
			{
			  foreach($kBooking->szDestinationCity as $keys=>$value)
			  {?>
			  	<option value="<?=$value?>"><?=$value?></option>
			 <?php
			  }
			}
		?>
		</select>
	</p>
	<br />
	<h5><?=t($t_base.'fields/forwarders');?></h5>
	<div class="forwarders">
		<?php 
			if($kBooking->forwarderAry)
			{
			  $ctr=0;	
			  foreach($kBooking->forwarderAry as $keys=>$value)
			  {
			  	if($keys>0)
			  	{
			  	?>
			  		<label><input type="checkbox" name="myBookingSearchArr[forawardArrs][<?=$ctr?>]"  id="forawardArrs" value="<?=$keys?>" onclick="search_mybooking();" checked/><?=$value?></label>
			 <?php
				}
			 	$ctr++;
			  }
			}
			if($showBookingFlag=='draft')
			{
		?>
		<label><input type="checkbox" name="myBookingSearchArr[noforawardArrs]"  id="noforawardArrs" value="1" onclick="search_mybooking();" checked/><?=t($t_base.'fields/no_forwarder_selected');?></label>
		<? }?>
	</div>
	<input type="hidden" name="showBookingFlag" id="showBookingFlag" value="<?=$showBookingFlag?>">
</form>
<? }?>