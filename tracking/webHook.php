<?php
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once(__APP_PATH_LAYOUT__."/ajax_header.php"); 

$kTracking = new cTracking();
        
$f = fopen("webhook-notification-".date("Ymd").".log", "a");
fwrite($f, "\n---------------\n");
fwrite($f, date("d-m-Y h:i:s")."\n");
fwrite($f, "\n---------------\n");
  
fwrite($f, "\n-------REQUEST LOGS--------\n");
fwrite($f, print_r($_REQUEST,true)."\n");
fwrite($f, "\n---------------\n");
 
if(!empty($_POST['webhookDemoAry']))
{
    $webhookResponse = $_POST['webhookDemoAry'];
}
else
{
    $webhookResponse = file_get_contents('php://input');
     
    fwrite($f, "\n-------JSON INPUT LOGS--------\n");
    fwrite($f, print_r($webhookResponse,true)."\n");
    fwrite($f, "\n---------END---------\n");  
}

$jsonNotification = $webhookResponse; 
if(!empty($jsonNotification))
{
    $jsonNotificationAry = json_decode($jsonNotification,true);
    $jsonNotificationMsgAry = $jsonNotificationAry['msg'];
    
    fwrite($f, "\n-------Decoded Message--------\n");
    fwrite($f, print_r($jsonNotificationMsgAry,true)."\n");
    fwrite($f, "\n---------------\n");

    $szTrackingSring = '';
    if(!empty($jsonNotificationMsgAry))
    {
        $szTrackingSring .= "\n";
        $szTrackingSring .= "\n Tracking: ".$jsonNotificationMsgAry['tracking_number'];
        $szTrackingSring .= "\n Slug: ".$jsonNotificationMsgAry['slug'];
        $szTrackingSring .= "\n Title: ".$jsonNotificationMsgAry['title'];
        $szTrackingSring .= "\n Order ID: ".$jsonNotificationMsgAry['order_id'];
        $szTrackingSring .= "\n Tag: ".$jsonNotificationMsgAry['tag'];
        $szTrackingSring .= "\n Tag: ".$jsonNotificationMsgAry['message']; 
        $szTrackingSring .= "\n Expected Delivery: ".$jsonNotificationMsgAry['expected_delivery'];
        $szTrackingSring .= "\n Track Count: ".$jsonNotificationMsgAry['tracked_count'];
        $szTrackingSring .= "\n Track Postcode: ".$jsonNotificationMsgAry['tracking_postal_code'];
        $szTrackingSring .= "\n Shipping Date: ".$jsonNotificationMsgAry['tracking_ship_date'];
        $szTrackingSring .= "\n Created On: ".$jsonNotificationMsgAry['created_at'];
        $szTrackingSring .= "\n Updated On: ".$jsonNotificationMsgAry['updated_at']; 

        $courierTrackingLogsAry = array();
        $courierTrackingLogsAry['szTrackingNumber'] = $jsonNotificationMsgAry['tracking_number'];
        $courierTrackingLogsAry['szBookingRef'] = $jsonNotificationMsgAry['order_id'];
        $courierTrackingLogsAry['szTrackingTag'] = $jsonNotificationMsgAry['tag'];
        $courierTrackingLogsAry['szTrackingID'] = $jsonNotificationMsgAry['id'];
        $courierTrackingLogsAry['szSlug'] = $jsonNotificationMsgAry['slug'];
        $courierTrackingLogsAry['dtCreatedAtAPI'] = $jsonNotificationMsgAry['created_at'];
        $courierTrackingLogsAry['szUniqueToken'] = $jsonNotificationMsgAry['unique_token'];
        $courierTrackingLogsAry['szCustomerName'] = $jsonNotificationMsgAry['customer_name'];
        $courierTrackingLogsAry['szTitle'] = $jsonNotificationMsgAry['title'];
        $courierTrackingLogsAry['dtExpactedDelivery'] = $jsonNotificationMsgAry['expected_delivery'];
        $courierTrackingLogsAry['dtShipmentDate'] = $jsonNotificationMsgAry['tracking_ship_date']; 
        $courierTrackingLogsAry['dtUpdatedAtAPI'] = $jsonNotificationMsgAry['updated_at']; 
        $courierTrackingLogsAry['checkpoints'] = $jsonNotificationMsgAry['checkpoints'];
        
        
        if(!empty($jsonNotificationMsgAry['checkpoints']))
        {
            foreach($jsonNotificationMsgAry['checkpoints'] as $checkpoints)
            {
                if($jsonNotificationMsgAry['tag']==$checkpoints['tag'])
                {
                   $courierTrackingLogsAry['checkPointTime']=$checkpoints['checkpoint_time'];
                   $courierTrackingLogsAry['messageCheckPoint']=$checkpoints['message'];
                }
            }
        }
        
        $oldBookingDetail = $kTracking->getBookingDetailByTrackingNumber($courierTrackingLogsAry['szTrackingNumber'],$courierTrackingLogsAry['szBookingRef']);
        $iShipmentPickedUp = $oldBookingDetail['iShipmentPickedUp'];
        
        $szTrackingSring .= "\n Old Tracking Tag : ".$oldBookingDetail['szTrackingTag']; 
        $szTrackingSring .= "\n New Tracking Tag : ".$courierTrackingLogsAry['szTrackingTag']; 
         
        $dtBugFixedDateTime = strtotime('2017-02-18');
        $dtBookingConfirmationTime = strtotime(date('Y-m-d',strtotime($oldBookingDetail['dtLabelSent'])));
        
        if($kTracking->addCourierTrackingLogDetail($courierTrackingLogsAry))
        {  
            $szOldTrackingTag = strtolower(trim($oldBookingDetail['szTrackingTag']));
            $szCurrentTrackingTag = strtolower(trim($courierTrackingLogsAry['szTrackingTag']));
            
            if($szOldTrackingTag != $szCurrentTrackingTag)
            {
                $bSendTrackingEmail = true;
            }
            else
            {
                $bSendTrackingEmail = false;
            }
            
            if(strtolower($courierTrackingLogsAry['szSlug'])=='tnt')
            {
                $checkPointTime=strtotime($courierTrackingLogsAry['checkPointTime']);
                $dtLabelSentTime=strtotime($oldBookingDetail['dtLabelSent']);
                if($dtLabelSentTime>$checkPointTime)
                {
                    $bSendTrackingEmail = false;
                }
            }
            
            if($bSendTrackingEmail)
            {
                if($kTracking->bSendTrackingEmail==1)
                {
                    $bSendTrackingEmail = true;
                }
                else if($iShipmentPickedUp==1 && strtolower($courierTrackingLogsAry['szSlug'])!='')
                {
                    if(strtolower(trim($courierTrackingLogsAry['szTrackingTag']))=='intransit')
                    {
                        if($dtBookingConfirmationTime>=$dtBugFixedDateTime)
                        {
                            $bSendTrackingEmail = true;
                        }
                        else
                        {
                            $bSendTrackingEmail = false;
                        } 
                    }
                    else
                    {
                        $bSendTrackingEmail = true;
                    }
                }
                else
                {
                    $bSendTrackingEmail = false;
                }
            }
            if($bSendTrackingEmail)
            {
                $kTracking->sendTrackingNotificationEmail($courierTrackingLogsAry);
            }  
        }
    }
}

fwrite($f, "\n\n Logging Data \n\n");
fwrite($f, $szTrackingSring);
//fclose($f);
die;
?>