<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
$display_abandon_message = true;

$szMetaTitle = __BOOKING_CONFIRMATION_META_TITLE__;
$szMetaKeywords = __BOOKING_CONFIRMATION_META_KEYWORDS__;
$szMetaDescription = __BOOKING_CONFIRMATION_META_DESCRIPTION__;

require_once(__APP_PATH_LAYOUT__."/header.php");
$t_base = "BookingConfirmation/";
$t_base_details = "BookingDetails/";

$iLanguage = getLanguageId();

constantApiKey();
$kBooking = new cBooking();
$kConfig = new cConfig();
$kRegisterShipCon = new cRegisterShipCon();
$kWHSSearch = new cWHSSearch();
//if($iLanguage==__LANGUAGE_ID_DANISH__)
//{
//	$szVariableName = '__TRANSPORTECA_EXPLAIN_PAGE_LINK_DANISH__' ;
//}
//else
//{
	$szVariableName = '__TRANSPORTECA_EXPLAIN_PAGE_LINK__' ;
//}
	
$explainPageLink = $kWHSSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);

$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num'])); 
$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);

$postSearchAry = array();
$postSearchAry = $kBooking->getBookingDetails($idBooking);
            
$kConfig_new = new cConfig();
$kConfig_new->loadCountry($postSearchAry['idOriginCountry']);
$szCountryStrUrl = $kConfig_new->szCountryISO ; 
$kConfig_new->loadCountry($postSearchAry['idDestinationCountry']); 
$szCountryStrUrl .= $kConfig_new->szCountryISO ; 

$redirect_url = __NEW_BOOKING_OVERVIEW_PAGE_URL__."/".$szBookingRandomNum."/".__OVERVIEW_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;

//redirectting to new booking pages 
ob_end_clean();
header("Location:".$redirect_url);
die;


if(!$kBooking->isBookingBelongsToCustomer($idBooking,$_SESSION['user_id']))
{
	echo display_booking_invalid_user_popup($t_base_details);
	die;
}
if((int)$_SESSION['user_id']<=0)
{
	header("Location:".__BOOKING_DETAILS_PAGE_URL__.'/'.$szBookingRandomNum.'/');
	die;
}
if($_SESSION['booking_id']==$idBooking)
{
	$kBooking->updatePaymentFlag($idBooking,0);
	$_SESSION['booking_id'] = '';
	unset($_SESSION['booking_id']);
}
if((int)$_REQUEST['idBooking']>0)
{
	$idBooking = (int)$_REQUEST['idBooking'] ;
	//$_SESSION['booking_id'] = $idBooking ;
}
if((int)$idBooking>0)
{
	$serviceTypeAry= array();
	$postSearchAry = array();
	$cargoAry = array();
	$shipperConsigneeAry = array();
	
	if((int)$idBooking>0 && (int)$_SESSION['user_id']>0)
	{
		$kUser->getUserDetails($_SESSION['user_id']);
		$res_ary=array();
		$res_ary['idUser'] = $kUser->id ;				
		$res_ary['szFirstName'] = $kUser->szFirstName ;
		$res_ary['szLastName'] = $kUser->szLastName ;
		$res_ary['szEmail'] = $kUser->szEmail ;	
		$res_ary['szCustomerAddress1'] = $kUser->szAddress1;
		$res_ary['szCustomerAddress2'] = $kUser->szAddress2;
		$res_ary['szCustomerAddress3'] = $kUser->szAddress3;
		$res_ary['szCustomerPostCode'] = $kUser->szPostcode;
		$res_ary['szCustomerCity'] = $kUser->szCity;
		$res_ary['szCustomerCountry'] = $kUser->szCountry;
		$res_ary['szCustomerState'] = $kUser->szState;
		$res_ary['szCustomerPhoneNumber'] = $kUser->szPhoneNumber;
		$res_ary['szCustomerCompanyName'] = $kUser->szCompanyName;	
		$res_ary['szCustomerCompanyRegNo'] = $kUser->szCompanyRegNo;	
		if((int)$postSearchAry['iBookingStep']<11)
		{
			$res_ary['iBookingStep'] = 11 ;
		}
		
		if(!empty($res_ary))
		{
			foreach($res_ary as $key=>$value)
			{
				$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
			}
		}		
		$update_query = rtrim($update_query,",");
		$kBooking->updateDraftBooking($update_query,$idBooking);
	}
	$iLanguage = getLanguageId();
	$postSearchAry = $kBooking->getExtendedBookingDetails($idBooking,$iLanguage);
	if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
	{
		echo display_booking_already_paid($t_base,$idBooking);
		die;
	}
	if(($postSearchAry['idForwarder']<=0) || ($postSearchAry['idShipperConsignee']<=0) || ($postSearchAry['idWarehouseTo']<=0) || ($postSearchAry['idWarehouseFrom']<=0) || ($postSearchAry['iPaymentProcess']==1))
	{
		$redirect_url = __BOOKING_DETAILS_PAGE_URL__.'/'.$szBookingRandomNum.'/';
		?>
		<div id="change_price_div">
		<?php
		display_booking_notification($redirect_url);
		echo "</div>";
	}
	else
	{
		echo '<div id="change_price_div" style="display:none;"></div>';
	}

	//print_r($postSearchAry);

	$cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);
	// geting  service type 
	$serviceTypeAry=$kConfig->getConfigurationLanguageData('__TABLE_SERVICE_TYPE__');
	$serviceTypeAry=$serviceTypeAry[$postSearchAry['idServiceType']];
	//getting shipper consignee details
	//$shipperConsigneeAry = $kRegisterShipCon->getShipperConsigneeDetails($idBooking);
	
	
	$szOriginAddress =  $postSearchAry['szOriginPostCode']." - ".$postSearchAry['szOriginCity']."<br> ".$postSearchAry['szOriginCountry'];
    //$szDestinationAddress = $postSearchAry['szWarehouseFromPostCode']." - ".$postSearchAry['szWarehouseFromCity']."<br>".$postSearchAry['szWarehouseFromCountry'];
				
	$szOriginAddress2 =  $postSearchAry['szDestinationPostCode']." - ".$postSearchAry['szDestinationCity']."<br> ".$postSearchAry['szDestinationCountry'];
    //$szDestinationAddress2 = $postSearchAry['szWarehouseToPostCode']." - ".$postSearchAry['szWarehouseToCity']."<br>".$postSearchAry['szWarehouseToCountry'];
}
else
{
	//header("Location:".__HOME_PAGE_URL__);
	//die;
	$redirect_url = __HOME_PAGE_URL__ ;
	?>
	<div id="change_price_div">
	<?php
	display_booking_notification($redirect_url);
	echo "</div>";
}

if(((int)$_SESSION['user_id']>0) && ($_REQUEST['err']<=0))
{
	checkUserPrefferedCurrencyActive();
}

/**
* This function recalculate pricing after a perticular interval set in constants.
* 
**/
$red_url = __BOOKING_CONFIRMATION_PAGE_URL__.'/'.$szBookingRandomNum.'/';
echo check_price_30_minute($postSearchAry,$red_url,false,$idBooking);


if($iLanguage==__LANGUAGE_ID_DANISH__)
{
	$szSecureImageName	= "secure-payment-da.png";
}
else
{
	$szSecureImageName = "secure-payment.png";
}
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<!-- Include the ZooZ script that contains the zoozStartCheckout() function -->
<script src="https://app.zooz.com/mobile/js/zooz-ext-web.js"></script>
<style type="text/css">
      #map_canvas { height: 100% }     
      .map-content h3 { margin: 0; padding: 5px 0 0 0; }    
      .map-content p{font-size:12px !important;}
      .map_note p{font-size:12px;font-style:italic;display:inline-block;margin:0 0 5px; 0;color:#808080;}
    </style>
    <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=<?php echo __GOOGLE_MAP_V3_API_KEY__?>&sensor=true">
    </script>
    <script type="text/javascript">
	  //var destinationIcon = __JS_ONLY_SITE_BASE__ + "/images/red-marker.png";
      var destinationIcon = __JS_ONLY_SITE_BASE__ + "/images/blue-marker.png";
	  var map;
	  
	// deafult value for longs and lats  
    var olat1 = 51.144894 ;
    var olang1 = 1.2854;
    var dlat1 = 51.905307 ;
    var dlang1 = 4.465942;
    var pipe_line_string = olat1 + "|" + olang1 +"|" + dlat1 + "|" + dlang1 ;
    
    function open_google_map_popup(pipe_line_string)
    {    	
    	var result_ary = pipe_line_string.split("|");
    	$("#map_popup_div").attr("style","display:block;");
    	$("#map_popup_div").focus();
    	initialize(result_ary);
    	addPopupScrollClass("map_popup_div");
    	//alert(result_ary);
    }
    	  
	function initialize(result_ary) {
	  var dlat = result_ary[0];
	  var dlang = result_ary[1];
	  var destination_address = result_ary[2];
	  var google_map_header = result_ary[3];
	  var map_zoom=8 ;
	   
      var destination = new google.maps.LatLng(dlat, dlang);
	  var myLatLng = new google.maps.LatLng(dlat, dlang);
	  var myOptions = {
		zoom: map_zoom,
		center: myLatLng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	  };	 
	 
	  $("#distance_div").html(google_map_header);
	  map = new google.maps.Map(document.getElementById("map_canvas"),
		  myOptions);
	  geocoder = new google.maps.Geocoder();
	  var flightPlanCoordinates = [
		new google.maps.LatLng(dlat, dlang)   
	  ];
	  var flightPath = new google.maps.Polyline({
		path: flightPlanCoordinates,
		strokeColor: "#7b6493",
		strokeOpacity: 1.0,		
		strokeWeight: 2
	  });

	  flightPath.setMap(map);

	  function infoCallback(infowindow, marker) { 
          return function() {
            infowindow.open(map, marker);
          };
        }
	 
	  var daddress = '<div class="map-content"><p>'+destination_address+'</p></div>';

	  var destinationmarker = new google.maps.Marker({
		  position: destination,
		  map: map,
		  icon: destinationIcon,
		  visible: true,
		  title:"<?=t($t_base.'fields/destination');?>"
	  });    
	  
	  var dinfowindow = new google.maps.InfoWindow();
	  dinfowindow.setContent(daddress);
	  google.maps.event.addListener(
		destinationmarker, 
		'click', 
		infoCallback(dinfowindow, destinationmarker)
	  );
	  dinfowindow.open(map,destinationmarker);        
}
</script>
	<div id="hsbody-2">		
	<div id="customs_clearance_pop" class="help-pop">
	</div>
	<div id="customs_clearance_pop_right" class="help-pop right">
	</div>
	<div id="loader" class="loader_popup_bg" style="display:none;">
	<div class="popup_loader"></div>				
		<img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
	</div>
	<div id="rating_popup"></div>
	<div id="change_price_div" style="display:none"></div>	
		<div id="map_popup_div" style="display:none;">		
				<div id="popup-bg"></div>
				 <div id="popup-container">			
				  <div class="popup map-popup">
				  <p class="close-icon" align="right">
					<a onclick="showHide('map_popup_div');" href="javascript:void(0);">
					<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
					</a>
					</p>
				    <div id="distance_div" style="font-weight:bold"></div><br>
				  	<div id="map_canvas" style="width:500px;height:500px;border: 1px solid #C4BDA1;"></div>
				  	<br>
				  	 	<p align="center"><a href="javascript:void(0);" onclick="showHide('map_popup_div');" class="button1"><span><?=t($t_base.'fields/close');?></span></a></p>
				  </div>
			   </div>
		  </div>
		  <form action="" name="booking_confirmation_form" id="booking_confirmation_form" method="post">
		  
			<div class="hsbody-2-left">
				<p><img src="<?=__BASE_STORE_IMAGE_URL__."/".$szSecureImageName; ?>" /></p>
			</div>
			<div class="hsbody-2-right">
			<div id="booking_confirm_error" class="errorBox " style="display:none;"></div>
			<div id="account_confirm_resend_email" style="display:none;"></div>	
				<?
				/**
				* display_booikng_bread_crum is used to display bread crum, it accepts two param first one is step and second is $t_base
				*/ 
				echo display_booikng_bread_crum(3,$t_base);		
			   ?>
				
				<br />
				<br />
				<?
					if($_SESSION['statusFailed']=='Failed')
					{
						unset($_SESSION['statusFailed']);
						$_SESSION['statusFailed']='';
						?>
						<div id="regError" class="errorBox ">
						<div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
						<div id="regErrorList">
						<ul>
						<li><?=t($t_base_details.'messages/payment_transaction_is_failed');?></li>
						</ul>
						</div>
						</div>
						<?
					}?>
				<div class="oh">
					<p class="fl-20"><strong><?=t($t_base.'fields/forwarder');?></strong>:<a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/FORWARDER_TOOL_TIP_HEADER_TEXT');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a>
					</p>
					<p class="fl-80">
						<a href="javascript:void(0)" onclick="openForwarderRatingPopUp('<?=$postSearchAry['idForwarder']?>')">
							<? 
							if($postSearchAry['szForwarderLogo']!='')
							{
							?>
								<img src="<?=__BASE_STORE_INC_URL__.'/image.php?img='.$postSearchAry['szForwarderLogo']."&w=180&h=70"?>" alt="<?=$postSearchAry['szForwarderDispName']?>" border="0" />
							<? }
							else{
									echo $postSearchAry['szForwarderDispName'];	
							}?>
						</a>
					</p>
				</div>
				<div class="oh">
					<p class="fl-20"><strong><?=t($t_base.'fields/service');?></strong>:</p>
					<p class="fl-80">
					<?php
						$services_string = $serviceTypeAry['szDescription'] ;
						$cc_string = '';
						if((int)$postSearchAry['iOriginCC']==1)
						{
							if($iLanguage==__LANGUAGE_ID_DANISH__)
							{
								$cc_string = " ".t($t_base.'fields/customs_clearance_at')." ".t($t_base.'fields/origin');
							}
							else
							{
								$cc_string = t($t_base.'fields/and')." ".t($t_base.'fields/customs_clearance_at')." ".t($t_base.'fields/origin');
							}
						}		
						if((int)$postSearchAry['iDestinationCC']==2)
						{
							if(empty($cc_string))
							{
								if($iLanguage==__LANGUAGE_ID_DANISH__)
								{
									$cc_string = " ".t($t_base.'fields/customs_clearance_at')." ".t($t_base.'fields/destination');
								}
								else
								{
									$cc_string = t($t_base.'fields/and')." ".t($t_base.'fields/customs_clearance_at')." ".t($t_base.'fields/destination');
								}								
							}
							else
							{
								$cc_string.= " ".t($t_base.'fields/and_destination');
							}
						}
						echo $services_string." ".$cc_string.' <a href="'.__BASE_URL_SECURE__."".$explainPageLink.'" target="_blank" class="f-size-12">'.t($t_base.'fields/what_does_this_mean').'?</a>' ;
					?>	
				</p>
				</div>
				<br><hr><br>
				<div class="oh">
					<p class="fl-20"><strong><?=t($t_base.'fields/shipper');?></strong>:</p>
					<p class="fl-30">
					<?
						$shipperAddress = '';
						if(!empty($postSearchAry['szShipperCompanyName']))
						{
							$shipperAddress.=$postSearchAry['szShipperCompanyName']."<br>";
						}
						if(!empty($postSearchAry['szShipperFirstName']) || $postSearchAry['szShipperLastName'])
						{
							$shipperAddress.=$postSearchAry['szShipperFirstName']." ".$postSearchAry['szShipperLastName']."<br>";
						}
						if(!empty($postSearchAry['szShipperPhone']))
						{
							$shipperAddress.=$postSearchAry['szShipperPhone']."<br>";
						}
						if(!empty($postSearchAry['szShipperAddress_pickup']))
						{
							$shipperAddress.=$postSearchAry['szShipperAddress'];
						}
						if(!empty($postSearchAry['szShipperAddress2']))
						{
							$shipperAddress .=", ".$postSearchAry['szShipperAddress2'] ;
						}
						if(!empty($postSearchAry['szShipperAddress3']))
						{
							$shipperAddress .=", ".$postSearchAry['szShipperAddress3'];
						}
						if(!empty($postSearchAry['szShipperPostCode']) || !empty($postSearchAry['szShipperCity']))
						{
							$shipperAddress.="<br>".$postSearchAry['szShipperPostCode']." ".$postSearchAry['szShipperCity']."<br>";
						}
						if(!empty($postSearchAry['szShipperCountry']))
						{
							$shipperAddress.=$postSearchAry['szShipperCountry']."<br>";
						}
						echo $shipperAddress
					?>
					</p>
					<p class="fl-20"><strong><?=t($t_base.'fields/consignee');?></strong>:</p>
					<p class="fl-30">
							<?
						$ConsigneeAddress = '';
						if(!empty($postSearchAry['szConsigneeCompanyName']))
						{
							$ConsigneeAddress.=$postSearchAry['szConsigneeCompanyName']."<br>";
						}
						if(!empty($postSearchAry['szConsigneeFirstName']) || $postSearchAry['szConsigneeLastName'])
						{
							$ConsigneeAddress.=$postSearchAry['szConsigneeFirstName']." ".$postSearchAry['szConsigneeLastName']."<br>";
						}
						if(!empty($postSearchAry['szConsigneePhone']))
						{
							$ConsigneeAddress.=$postSearchAry['szConsigneePhone']."<br>";
						}
						if(!empty($postSearchAry['szConsigneeAddress']))
						{
							$ConsigneeAddress.=$postSearchAry['szConsigneeAddress'];
						}
						if(!empty($postSearchAry['szConsigneeAddress2']))
						{
							$ConsigneeAddress .=", ".$postSearchAry['szConsigneeAddress2'];
						}
						if(!empty($postSearchAry['szConsigneeAddress3']))
						{
							$ConsigneeAddress .=", ".$postSearchAry['szConsigneeAddress3'];
						}
						if(!empty($postSearchAry['szConsigneePostCode']) || !empty($postSearchAry['szConsigneeCity']))
						{
							$ConsigneeAddress.="<br>".$postSearchAry['szConsigneePostCode']." ".$postSearchAry['szConsigneeCity']."<br>";
						}
						if(!empty($postSearchAry['szConsigneeCountry']))
						{
							$ConsigneeAddress.=$postSearchAry['szConsigneeCountry']."<br>";
						}
						echo $ConsigneeAddress
					?>
					</p>
				</div>
				<div class="oh">
					<p class="fl-20">
					<?
					if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType'] ==__SERVICE_TYPE_DTW__ || $postSearchAry['idServiceType'] ==__SERVICE_TYPE_DTP__) // 1-DTD , 2-DTW
					{
						echo "<strong>".t($t_base.'fields/pickup_after')."</strong>:"; 
						$szWareHouseFromStr = t($t_base.'fields/after').' ';
						
						?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/PICK_UP_ADDRESS_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/PICK_UP_ADDRESS_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a>	
						<?
						$show_left_map = true;
					}
					else
					{
						echo "<strong>".t($t_base.'fields/cut_off')."</strong>:";
						$szWareHouseFromStr = t($t_base.'fields/latest').' ';
						?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/CUT_OFF_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/CUT_OFF_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a>	
						<?
						$show_left_map = true;
					}
					?>
					</p>
					<p class="fl-30">
					<?
						//$szWareHouseFromStr = '';
						if(!empty($postSearchAry['dtCutOff']))
						{
							$szWareHouseFromStr.=date('d/m/Y H:i',strtotime($postSearchAry['dtCutOff']))."<br>";
						}
						// For DTD and DTW, the pick-up address should be actual pick-up address
						if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType'] ==__SERVICE_TYPE_DTW__ || $postSearchAry['idServiceType'] ==__SERVICE_TYPE_DTP__) // 1-DTD , 2-DTW
						{						
							$kConfig = new cConfig();
							$originPostCodeAry=array();
							$originPostCodeAry = $kConfig->getPostCodeDetails($postSearchAry['idShipperCountry_pickup'],$postSearchAry['szShipperPostCode_pickup'],$postSearchAry['szShipperCity_pickup']);
							$origin_lat_lang_pipeline = $originPostCodeAry['szLatitude']."|".$originPostCodeAry['szLongitude'];
							
							$mapHeaderFrom = "<strong>".t($t_base.'fields/pickup_address')."</strong><br>"; 
							if(!empty($postSearchAry['szShipperAddress_pickup']))
							{
								$szWareHouseFromStr.=$postSearchAry['szShipperAddress_pickup'];
								//$szWareHouseMapStr.=$postSearchAry['szShipperAddress_pickup'];
							}
							if(!empty($postSearchAry['szShipperAddress2_pickup']))
							{
								$szWareHouseFromStr.= ", ".$postSearchAry['szShipperAddress2_pickup'] ;
								//$szWareHouseMapStr.= ", ".$postSearchAry['szShipperAddress2_pickup'] ;
							}
							if(!empty($postSearchAry['szShipperAddress3_pickup']))
							{
								$szWareHouseFromStr.= ", ".$postSearchAry['szShipperAddress3_pickup'];
								//$szWareHouseMapStr.= ", ".$postSearchAry['szShipperAddress3_pickup'];
							}
							if(!empty($postSearchAry['szShipperPostCode_pickup']) || !empty($postSearchAry['szShipperCity_pickup']))
							{
								$szWareHouseFromStr .="<br>".$postSearchAry['szShipperPostCode_pickup']." ".$postSearchAry['szShipperCity_pickup']."<br>";
								$szWareHouseMapStr.= $postSearchAry['szShipperPostCode_pickup']." ".$postSearchAry['szShipperCity_pickup']."<br>";
							}
							if(!empty($postSearchAry['szShipperCountry_pickup']))
							{
								$szWareHouseFromStr.=$postSearchAry['szShipperCountry_pickup']."<br>";
								$szWareHouseMapStr.=$postSearchAry['szShipperCountry_pickup']."<br>";
							}
						}
						else
						{
							$origin_lat_lang_pipeline = $postSearchAry['fWarehouseFromLatitude']."|".$postSearchAry['fWarehouseFromLongitude'];
							$mapHeaderFrom = "<strong>".t($t_base.'fields/forwarder_warehouse_address')."</strong><br>";
							if(!empty($postSearchAry['szWarehouseFromName']))
							{
								$szWareHouseFromStr.=$postSearchAry['szWarehouseFromName']."<br>";
								$szWareHouseMapStr.=$postSearchAry['szWarehouseFromName']."<br>";
							}
							if(!empty($postSearchAry['szWarehouseFromAddress']))
							{
								$szWareHouseFromStr.= $postSearchAry['szWarehouseFromAddress'];
								//$szWareHouseMapStr .= $postSearchAry['szWarehouseFromAddress'];
							}
							if(!empty($postSearchAry['szWarehouseFromAddress2']))
							{
								$szWareHouseFromStr .= ", ".$postSearchAry['szWarehouseFromAddress2'] ;
								//$szWareHouseMapStr .= ", ".$postSearchAry['szWarehouseFromAddress2'] ;
							}
							if(!empty($postSearchAry['szWarehouseFromAddress3']))
							{
								$szWareHouseFromStr .= ", ".$postSearchAry['szWarehouseFromAddress3'] ;
								//$szWareHouseMapStr .= ", ".$postSearchAry['szWarehouseFromAddress3'] ;
							}
							if(!empty($postSearchAry['szWarehouseFromPostCode']) || !empty($postSearchAry['szWarehouseFromCity']))
							{
								$szWareHouseFromStr.="<br>".$postSearchAry['szWarehouseFromPostCode']." ".$postSearchAry['szWarehouseFromCity']."<br>";
								$szWareHouseMapStr .= $postSearchAry['szWarehouseFromPostCode']." ".$postSearchAry['szWarehouseFromCity']."<br>";
							}						
							if(!empty($postSearchAry['szWarehouseFromCountry']) || !empty($postSearchAry['szWarehouseFromState']))
							{
								$szWareHouseFromStr.=$postSearchAry['szWarehouseFromCountry']."<br>";
								$szWareHouseMapStr .=$postSearchAry['szWarehouseFromCountry']."<br>";
							}
						}	
						echo $szWareHouseFromStr ;
						if(!empty($szWareHouseMapStr) && $show_left_map)
						{
					?>
						
						<a href="javascript:void(0);" class="f-size-12" onclick="open_google_map_popup('<?=$origin_lat_lang_pipeline."|".$szWareHouseMapStr."|".$mapHeaderFrom?>')"><?=t($t_base.'fields/view_on_map');?></a>
					<? }  ?>	
					</p>
					<p class="fl-20">
					<?
						if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTW__ || $postSearchAry['idServiceType'] ==__SERVICE_TYPE_WTW__ || $postSearchAry['idServiceType'] ==__SERVICE_TYPE_PTW__ || $postSearchAry['idServiceType'] ==__SERVICE_TYPE_WTP__ || $postSearchAry['idServiceType'] ==__SERVICE_TYPE_PTP__ || $postSearchAry['idServiceType'] ==__SERVICE_TYPE_DTP__)  // 2- DTW , 4- WTW
						{
							echo "<strong>".t($t_base.'fields/available')."</strong>:";
							$szWareHouseToStr = t($t_base.'fields/after').' ';
							?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/AVAILABLE_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/AVAILABLE_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a>	
							<?
							$show_right_map = true;
						}
						else
						{
							echo "<strong>".t($t_base.'fields/delivery_before')."</strong>:";
							$szWareHouseToStr = t($t_base.'fields/before').' ' ; 
							?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/DELIVERY_BEFORE_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/DELIVERY_BEFORE_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a>	
							<?
							$show_right_map = true;
						}	
					?>
					</p>
					<p class="fl-30">
					<?
						if(!empty($postSearchAry['dtAvailable']))
						{
							$szWareHouseToStr.=date('d/m/Y H:i',strtotime($postSearchAry['dtAvailable']))."<br>";
						}
						if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType'] ==__SERVICE_TYPE_WTD__ || $postSearchAry['idServiceType'] ==__SERVICE_TYPE_PTD__) // 1-DTD , 3-WTD
						{		
							$kConfig = new cConfig();
							$destinationPostCodeAry=array();
							$destinationPostCodeAry = $kConfig->getPostCodeDetails($postSearchAry['idConsigneeCountry_pickup'],$postSearchAry['szConsigneePostCode_pickup'],$postSearchAry['szConsigneeCity_pickup']);
							$destination_lat_lang_pipeline = $destinationPostCodeAry['szLatitude']."|".$destinationPostCodeAry['szLongitude'];
							$mapHeaderTo = "<strong>".t($t_base.'fields/delivery_address')."</strong><br>";						
							
							if(!empty($postSearchAry['szConsigneeAddress_pickup']))
							{
								$szWareHouseToStr.=$postSearchAry['szConsigneeAddress_pickup'];
								//$szWareHouseToMapStr.=$postSearchAry['szConsigneeAddress_pickup'];
							}
							if(!empty($postSearchAry['szConsigneeAddress2_pickup']))
							{
								$szWareHouseToStr .=", ".$postSearchAry['szConsigneeAddress2_pickup'] ;
								//$szWareHouseToMapStr.= ", ".$postSearchAry['szConsigneeAddress2_pickup'];
							}
							if(!empty($postSearchAry['szConsigneeAddress3_pickup']))
							{
								$szWareHouseToStr .=", ".$postSearchAry['szConsigneeAddress3_pickup'] ;
								//$szWareHouseToMapStr.= ", ".$postSearchAry['szConsigneeAddress3_pickup'];
							}
							if(!empty($postSearchAry['szConsigneePostCode_pickup']) || !empty($postSearchAry['szConsigneeCity_pickup']))
							{
								$szWareHouseToStr.= "<br>".$postSearchAry['szConsigneePostCode_pickup']." ".$postSearchAry['szConsigneeCity_pickup']."<br>";
								$szWareHouseToMapStr.= $postSearchAry['szConsigneePostCode_pickup']." ".$postSearchAry['szConsigneeCity_pickup']."<br>";
							}
							if(!empty($postSearchAry['szConsigneeCountry_pickup']))
							{
								$szWareHouseToStr.=$postSearchAry['szConsigneeCountry_pickup']."<br>";
								$szWareHouseToMapStr.=$postSearchAry['szConsigneeCountry_pickup']."<br>";
							}
						}
						else
						{
							$destination_lat_lang_pipeline = $postSearchAry['fWarehouseToLatitude']."|".$postSearchAry['fWarehouseToLongitude'];
							//$szWareHouseToMapStr = "<strong>Forwarder\'s warehouse address</strong>:<br>";
							$mapHeaderTo = "<strong>".t($t_base.'fields/forwarder_warehouse_address')."</strong><br>";
							if(!empty($postSearchAry['szWarehouseToName']))
							{
								$szWareHouseToStr.=$postSearchAry['szWarehouseToName']."<br>";
								$szWareHouseToMapStr .=$postSearchAry['szWarehouseToName']."<br>";
							}
							if(!empty($postSearchAry['szWarehouseToAddress']))
							{
								$szWareHouseToStr.=$postSearchAry['szWarehouseToAddress'];
								//$szWareHouseToMapStr .=$postSearchAry['szWarehouseToAddress'];
							}
							if(!empty($postSearchAry['szWarehouseToAddress2']))
							{
								$szWareHouseToStr .=", ".$postSearchAry['szWarehouseToAddress2'];
								//$szWareHouseToMapStr .= ", ".$postSearchAry['szWarehouseToAddress2'];
							}
							if(!empty($postSearchAry['szWarehouseToAddress3']))
							{
								$szWareHouseToStr .=", ".$postSearchAry['szWarehouseToAddress3'];
								//$szWareHouseToMapStr .= ", ".$postSearchAry['szWarehouseToAddress3'];
							}
							if(!empty($postSearchAry['szWarehouseToPostCode']) || !empty($postSearchAry['szWarehouseToCity']))
							{
								$szWareHouseToStr.= "<br>".$postSearchAry['szWarehouseToPostCode']." ".$postSearchAry['szWarehouseToCity']."<br>";
								$szWareHouseToMapStr .= $postSearchAry['szWarehouseToPostCode']." ".$postSearchAry['szWarehouseToCity']."<br>";
							}						
							if(!empty($postSearchAry['szWarehouseToCountry']) || !empty($postSearchAry['szWarehouseToState']))
							{
								$szWareHouseToStr.=$postSearchAry['szWarehouseToCountry']."<br>";
								$szWareHouseToMapStr .= $postSearchAry['szWarehouseToCountry']."<br>";
							}	
						}
						echo $szWareHouseToStr ;
						if(!empty($szWareHouseToMapStr) && $show_right_map)
						{
					?>
						<a href="javascript:void(0);" class="f-size-12" onclick="open_google_map_popup('<?=$destination_lat_lang_pipeline.'|'.$szWareHouseToMapStr."|".$mapHeaderTo?>')"><?=t($t_base.'fields/view_on_map');?></a>
					<? }  ?>	
					</p>
				</div>
				<br><hr><br>
				<div class="oh">
					<p class="fl-20"><strong><?=t($t_base.'fields/cargo');?></strong>:</p>
					<div class="fl-80">
					<?
						$count_cargo = count($cargoAry);
						$counter_1=0;
						//print_r($cargoAry);
						for($i=1;$i<=$count_cargo;$i++)
						{
							if(!empty($cargoAry))
							{
								$fLength = ceil($cargoAry[$i]['fLength']);
								$fWidth = ceil($cargoAry[$i]['fWidth']);
								$fHeight = ceil($cargoAry[$i]['fHeight']);
								$fWeight = ceil($cargoAry[$i]['fWeight']);
								$iQuantity = ceil($cargoAry[$i]['iQuantity']);
								$szCommodity = $cargoAry[$i]['szCommodity'];
								$szCargoMeasure = $cargoAry[$i]['cmdes'];
								$szWeightMeasure =  $cargoAry[$i]['wmdes'];
								$szCommodity = $cargoAry[$i]['szCommodity'];
							}
						
							echo '<p>'.$i.": ".$iQuantity."x".$fLength."x".$fWidth."x".$fHeight."".$szCargoMeasure.", ".number_format((float)$fWeight)." ".$szWeightMeasure.", ".$szCommodity."</p>";
							$counter_1++;
						}
					?>
					</div>
				</div>
				<br><hr><br>
				<div class="oh">
					<p class="fl-20"><strong><?=t($t_base.'fields/invoice_details');?></strong>:</p>
					<p class="fl-60" style="width:50%;">
						<?			
							if((int)$postSearchAry['iDestinationCC']==2 || $postSearchAry['iOriginCC']==1)
							{
						?>
							<?=t($t_base.'fields/freight_and_customs');?>:<? } else {?><?=t($t_base.'fields/freight_all_inclusive');?>:<? }?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/FREIGHT_CUSTOM_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/FREIGHT_CUSTOM_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
					<p class="fl-20" align="left"><?=$postSearchAry['szCurrency']." ".number_format((float)$postSearchAry['fTotalPriceCustomerCurrency'],2)?></p>
				</div>
				<div class="oh">
					<p class="fl-20">&nbsp;</p>
					<p><?=t($t_base.'fields/booking_comment');?> <span class="optional">(<?=t($t_base.'fields/optional');?>)</span>:<a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/INVOICE_COMMENT_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/INVOICE_COMMENT_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a>
						
					</p>
				</div>
				<div class="oh">	
					<p class="fl-20">&nbsp;</p>
					<p class="fl-80" align="left"><textarea  style="width: 572px;" name="bookingConfirmAry[szInvoiceComment]" rows="2" cols="77"><?=$postSearchAry['szInvoiceComment']?></textarea></p>
					<input type="hidden" name="bookingConfirmAry[iBookingStatus]" id="iBookingStatus" value="3">
					
				</div>
				<br><hr><br>
				
				<div class="oh">
					<p class="fl-20"><strong><?=t($t_base.'fields/next_step');?></strong>:</p>
					<p class="fl-80"><?=t($t_base.'fields/confirm_booking')." "; ?> <?=t($t_base.'fields/price_adjustment');?></p>
				</div>
				<div class="oh">
					<p class="fl-20">&nbsp;</p>
					<p class="fl-75 checkbox-ab">
						<input type="checkbox" name="bookingConfirmAry[iRemindToRate]" <? if((int)$postSearchAry['iRemindToRate']==1 || empty($_POST['bookingConfirmAry'])) {?>checked<? }?> value="1"  /><?=t($t_base.'fields/remind_forwarder');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/REMIND_ME_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/REMIND_ME_TOOL_TIP_TEXT');?>','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a>
					</p>
				</div>
				<div class="oh">
					<p class="fl-20">&nbsp;</p>
					<p class="fl-75 checkbox-ab">
						<input type="checkbox" name="bookingConfirmAry[iTerms]" <? if((int)$postSearchAry['iAcceptedTerms']==1){ ?>checked<? } ?>  value="1" /><?=t($t_base.'fields/read_terms_condition');?> <a href="javascript:void(0)" class="f-size-12" onclick="addPopupScrollClass(); showHide('terms_condition_popup')"><?=t($t_base.'fields/terms_condition');?></a><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/TERMS_CONDITION_TOOL_TIP_HEADER_TEXT');?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a>
					</p>
				</div>	
				<br />
				<? 
				if($postSearchAry['szCurrency']!='HKD')
				{?>
				<div class="oh">
					<p class="fl-20">&nbsp;</p>
					<div class="fl-75">
						<p class="checkbox-ab fl-15" style="width:13%;padding-top:7px;">
						<input type="radio" name="bookingConfirmAry[iBookingPayment]" style="top:10px;" id="ZoozPayment" value="Zooz" checked="checked"/><?=t($t_base.'fields/pay_with');?> 
						</p>
						<p class="fl-30" style="padding-top:2px;">
						<img src="<?=__MAIN_SITE_HOME_PAGE_URL__?>/images/Zooz-cards.png" width="160" valign="top" >
						</p>
						<p class="fl-20" align="center" style="padding-top:7px;">
						<?=t($t_base.'fields/powered_by');?>
						</p>
						<p class="fl-25">
						<img src="<?=__MAIN_SITE_HOME_PAGE_URL__?>/images/Zooz.png" width="110" valign="top">
						</p>
					</div>
				</div>				
				<? }?>
				<div class="oh">
					<p class="fl-20">&nbsp;</p>
					<div class="fl-75">
						<p class="checkbox-ab fl-15" style="width:13%;padding-top:5px;">
							<input type="radio" name="bookingConfirmAry[iBookingPayment]" style="top:9px;" id="PaypalPayment" value="Paypal" <? if($postSearchAry['szCurrency']=='HKD'){ echo "checked"; }?>/><?=t($t_base.'fields/pay_with');?>
							<input type="hidden" name="bookingConfirmAry[szBookingRandomNum]" id="szBookingRandomNum_booking_conf" value="">
						</p>	
						<!-- <p class="fl-25" style="width:23%;padding-top:3px;">
						<img src="<?=__MAIN_SITE_HOME_PAGE_URL__?>/images/PayPal-cards.png" width="160" valign="top">
						</p>
						<p class="fl-20" align="center" style="padding-top:4px;">
						<?=t($t_base.'fields/powered_by');?>
						</p>-->
						<p class="fl-25">
						<img src="<?=__MAIN_SITE_HOME_PAGE_URL__?>/images/PayPal.png" width="90" valign="top">
						</p>
					</div>
				</div>
				<br/>
				<div class="oh">
					<p class="fl-33"><a href="<?=__BOOKING_DETAILS_PAGE_URL__.'/'.$_REQUEST['booking_random_num'].'/'?>" class="button2"><span><?=t($t_base.'fields/back');?></span></a></p>
					<p class="fl-40" align="center">
						<a href="javascript:void(0)" onclick="booking_on_hold('ON_HOLD');" class="button1"><span><?=t($t_base.'fields/hold');?></span></a>
						<a  href="javascript:void(0)" onclick="booking_on_hold('CONFIRMED');" class="button1"><span><?=t($t_base.'fields/confirm');?></span></a>
						<!--<a  href="javascript:void(0)" onclick="booking_on_hold('CONFIRMED');"><img src="<?=__BASE_URL__?>/images/paypal_button.jpg" width="100" height="50"></a>
						 <a  href="javascript:void(0)" onclick="booking_on_hold_zooz('CONFIRMED','<?=__API_UNIQUE_KEY__?>');"><img src="<?=__BASE_URL__?>/images/zooz_button.jpg" width="100" height="50"></a> -->
					</p>
				</div>				
			</div>
			</form>	
		</div>
	<div id="terms_condition_popup" style="display:none;">
	<?php
		echo terms_condition_popup_html($postSearchAry,$t_base,$iLanguage);
	?>
	</div>
<?
echo html_form_random_booking_number(__BOOKING_CONFIRMATION_PAGE_URL__,$_REQUEST['booking_random_num']);
require_once(__APP_PATH_LAYOUT__."/footer.php");
?>		