<?php 
/**
 * View Invoice
 */ 
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH__ . "/inc/functions.php" );
include( __APP_PATH_LAYOUT__ . "/header_new.php");  
ini_set (max_execution_time,360000);

$kMautic = new cMautic();

//$kMautic->validateMauticApiAuthentication();

$mauticLeadApiAry = $kMautic->getMauticApiLogsData();
?> 
<h1>Transporteca Mautic API Logs</h1>
<table class="format-1" width="100%">
    <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Request Data</th>
        <th>Response Data</th> 
        <th>Added On</th> 
    </tr>
    <?php
        if(!empty($mauticLeadApiAry))
        {
            $ipAddressCount=0;
            $ctr = 0;
            foreach($mauticLeadApiAry as $mauticLeadApiArys)
            {
                $style = ""; 
                if(empty($mauticLeadApiArys['szResponseData']) || trim($mauticLeadApiArys['szResponseData'])=='Array ( )')
                {
                    $style = "style='background: #FF0000 none repeat scroll 0 0;color: #FFFFFF' ";
                    echo "Blank";
                    $ipAddressCount++;
                }
                else
                {
                    $style = "style='background: #e5e5e7 none repeat scroll 0 0;color: #424242' ";
                }
                $ctr++;
                ?>
                <tr <?php echo $style; ?>>
                    <td><?php echo $mauticLeadApiArys['szFirstName']." ".$mauticLeadApiArys['szLastName']; ?></td>
                    <td><?php echo $mauticLeadApiArys['szEmail']; ?></td>
                    <td><?php echo $mauticLeadApiArys['szRequestData']; ?></td>
                    <td><?php echo $mauticLeadApiArys['szResponseData']; ?></td> 
                    <td><?php echo $mauticLeadApiArys['dtCreatedOn']; ?></td> 
                </tr>
                <?php
            }
        } 
    ?>
</table>
<?php

die;
$mauticLeadApiAry = $kMautic->getMauticApiList();

?>

<table class="format-1" width="100%">
    <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Country</th>
        <th>Type</th>
        <th>IP Address</th>
        <th>Added On</th>
        <th>Updated On</th>
    </tr>
    <?php
        if(!empty($mauticLeadApiAry))
        {
            $ipAddressCount=0;
            $ctr = 0;
            foreach($mauticLeadApiAry as $mauticLeadApiArys)
            {
                $style = "";
                if(empty($mauticLeadApiArys['szIpAddress']))
                {
                    $style = "style='background: #FF0000 none repeat scroll 0 0;color: #FFFFFF' ";
                    $ipAddressCount++;
                }
                else
                {
                    $style = "style='background: #e5e5e7 none repeat scroll 0 0;color: #424242' ";
                }
                $ctr++;
                ?>
                <tr <?php echo $style; ?>>
                    <td><?php echo $mauticLeadApiArys['szCustomerName']; ?></td>
                    <td><?php echo $mauticLeadApiArys['szEmail']; ?></td>
                    <td><?php echo $mauticLeadApiArys['szCountryName']; ?></td>
                    <td><?php echo $mauticLeadApiArys['szType']; ?></td>
                    <td><?php echo $mauticLeadApiArys['szIpAddress']; ?></td>
                    <td><?php echo $mauticLeadApiArys['dtDateAdded']; ?></td>
                    <td><?php echo $mauticLeadApiArys['dtDateModified']; ?></td>
                </tr>
                <?php
            }
        } 
    ?>
</table>
<br><br> 
<h4>Summery</h4>
<p>Total Records: <?php echo $ctr; ?></p>
<p>Empty IP Address: <?php echo $ipAddressCount; ?></p>
<p>With IP Address: <?php echo $ctr - $ipAddressCount; ?></p>
<?php

die;
 
$mauticRequestParamsAry = array();
$mauticRequestParamsAry['oauth_callback'] = 'http://www.a.transport-eca.com/mauticTest2.php';
$mauticRequestParamsAry['oauth_consumer_key'] = '5dckgmjxp808coosos8sw0oowg8ck8o48kgk804w8ksscgwk40';
$mauticRequestParamsAry['oauth_nonce'] = '13239ac7d3090560';
$mauticRequestParamsAry['oauth_signature'] = '8DmRgyQh1ff8TlbOytPz0xZvUjg=';
$mauticRequestParamsAry['oauth_signature_method'] = 'HMAC-SHA1';
$mauticRequestParamsAry['oauth_timestamp'] = time();
$mauticRequestParamsAry['oauth_verifier'] = '59b77q9bai88cc8sc0sckwgo8wckckgocgccgo8kskssog0oow';
$mauticRequestParamsAry['oauth_version'] = '1.0';

$url = "https://transporteca.mautic.com/oauth/v1/access_token"; 
$headers = array ('Authorization: OAuth oauth_callback="http://www.a.transport-eca.com/mauticTest2.php",
                    oauth_consumer_key="5dckgmjxp808coosos8sw0oowg8ck8o48kgk804w8ksscgwk40",
                    oauth_nonce="13239ac7d3090560",
                    oauth_signature="8DmRgyQh1ff8TlbOytPz0xZvUjg=",
                    oauth_signature_method="HMAC-SHA1",
                    oauth_timestamp="'.time().'", 
                    oauth_verifier="59b77q9bai88cc8sc0sckwgo8wckckgocgccgo8kskssog0oow",
                    oauth_version="1.0"'
            );  
$ch = curl_init($url);   
$options = array(  
    CURLOPT_HTTPHEADER => $headers,    
); 

curl_setopt_array($ch, $options);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 

$cUrlResponseAry = array();
$cUrlResponseXml = curl_exec($ch);  
$culrInfoAry = curl_getinfo($ch);  

echo "<br> Response Ary <br> ";
print_R($cUrlResponseXml);
echo "<br>Curl Info <br>";
print_R($culrInfoAry);
die;

?>
<script type="text/javascript">

    /** This section is only needed once per page if manually copying **/
    if (typeof MauticSDKLoaded == 'undefined') {
        var MauticSDKLoaded = true;
        var head            = document.getElementsByTagName('head')[0];
        var script          = document.createElement('script');
        script.type         = 'text/javascript';
        script.src          = 'https://whiz.mautic.com/mautic/media/js/mautic-form.js';
        script.onload       = function() {
            MauticSDK.onLoad();
        };
        head.appendChild(script);
        var MauticDomain = 'https://whiz.mautic.com';
        var MauticLang   = {
            'submittingMessage': "Please wait..."
        }
    }
</script>

<style type="text/css" scoped>
    .mauticform_wrapper {}
    .mauticform-innerform {}
    .mauticform-post-success {}
    .mauticform-name { font-weight: bold; font-size: 1.5em; margin-bottom: 3px; }
    .mauticform-description { margin-top: 2px; margin-bottom: 10px; }
    .mauticform-error { margin-bottom: 10px; color: red; }
    .mauticform-message { margin-bottom: 10px;color: green; }
    .mauticform-row { display: block; margin-bottom: 20px; }
    .mauticform-label { font-size: 1.1em; display: block; font-weight: bold; margin-bottom: 5px; }
    .mauticform-row.mauticform-required .mauticform-label:after { color: #e32; content: " *"; display: inline; }
    .mauticform-helpmessage { display: block; font-size: 0.9em; margin-bottom: 3px; }
    .mauticform-errormsg { display: block; color: red; margin-top: 2px; }
    .mauticform-selectbox, .mauticform-input, .mauticform-textarea { width: 50%; padding: 0.2em 0.2em; border: 1px solid #CCC; box-shadow: 0px 1px 3px #DDD inset; border-radius: 4px; box-sizing: border-box; }
    .mauticform-checkboxgrp-row {}
    .mauticform-checkboxgrp-label { font-weight: normal; }
    .mauticform-checkboxgrp-checkbox {}
    .mauticform-radiogrp-row {}
    .mauticform-radiogrp-label { font-weight: normal; }
    .mauticform-radiogrp-radio {}
</style>
<div id="mauticform_wrapper_customers" class="mauticform_wrapper">
    <form autocomplete="false" role="form" method="post" action="https://whiz.mautic.com/form/submit?formId=1" id="mauticform_customers" data-mautic-form="customers">
        <div class="mauticform-error" id="mauticform_customers_error"></div>
        <div class="mauticform-message" id="mauticform_customers_message"></div>
        <div class="mauticform-innerform">

            <div id="mauticform_customers_first_name"  data-validate="first_name" data-validation-type="text" class="mauticform-row mauticform-text mauticform-required">
                <label id="mauticform_label_customers_first_name" for="mauticform_input_customers_first_name" class="mauticform-label">First Name</label>
                <input id="mauticform_input_customers_first_name" name="mauticform[first_name]" value="" class="mauticform-input" type="text" />
                <span class="mauticform-errormsg" style="display: none;">This is required.</span>
            </div>

            <div id="mauticform_customers_last_name"  data-validate="last_name" data-validation-type="text" class="mauticform-row mauticform-text mauticform-required">
                <label id="mauticform_label_customers_last_name" for="mauticform_input_customers_last_name" class="mauticform-label">Last Name</label>
                <input id="mauticform_input_customers_last_name" name="mauticform[last_name]" value="" class="mauticform-input" type="text" />
                <span class="mauticform-errormsg" style="display: none;">This is required.</span>
            </div>

            <div id="mauticform_customers_email"  data-validate="email" data-validation-type="text" class="mauticform-row mauticform-text mauticform-required">
                <label id="mauticform_label_customers_email" for="mauticform_input_customers_email" class="mauticform-label">Email</label>
                <input id="mauticform_input_customers_email" name="mauticform[email]" value="" class="mauticform-input" type="text" />
                <span class="mauticform-errormsg" style="display: none;">This is required.</span>
            </div>

            <div id="mauticform_customers_phone"  data-validate="phone" data-validation-type="tel" class="mauticform-row mauticform-tel mauticform-required">
                <label id="mauticform_label_customers_phone" for="mauticform_input_customers_phone" class="mauticform-label">Phone</label>
                <input id="mauticform_input_customers_phone" name="mauticform[phone]" value="" class="mauticform-input" type="tel" />
                <span class="mauticform-errormsg" style="display: none;">This is required.</span>
            </div>

            <div id="mauticform_customers_developer_notes"  class="mauticform-row mauticform-freetext">
                <h3 id="mauticform_label_customers_developer_notes" for="mauticform_input_customers_developer_notes" class="mauticform-label" id="mauticform_label_developer_notes for="mauticform_input_customers_developer_notes">
                    Developer Notes
                </h3>
                <div id="mauticform_input_customers_developer_notes" name="mauticform[developer_notes]" value="" class="mauticform-freetext" id="mauticform_input_customers_developer_notes">
                    
                </div>
            </div>

            <div id="mauticform_customers_submit"  class="mauticform-row mauticform-button-wrapper">
                <button type="submit" name="mauticform[submit]" id="mauticform_input_customers_submit" name="mauticform[submit]" value="" class="mauticform-button" value="1">Submit</button>
            </div>

            <input type="hidden" name="mauticform[formId]" id="mauticform_customers_id" value="1" />
            <input type="hidden" name="mauticform[return]" id="mauticform_customers_return" value="" />
            <input type="hidden" name="mauticform[formName]" id="mauticform_customers_name" value="customers" />

        </div>
    </form>
</div>
<?php 
include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>
