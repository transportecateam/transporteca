<?php
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
$iGetLanguageFromRequest=1;
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

$t_base="ExplainPage/";
$kExplain= new cExplain();
$mode=trim($_POST['mode']);

$iLanguage = getLanguageId();
if($mode=='SEARCH_KEYWORD')
{
	$szSearchKeyWord=sanitize_all_html_input(trim($_REQUEST['szKeyWordValue']));
	$kExplain->submitSearchFields($szSearchKeyWord);
	$level3DataArr=$kExplain->getExplainContentSearchByKeyword($szSearchKeyWord,$iLanguage);

	showSearchResultLevel3($kExplain,$level3DataArr,$szSearchKeyWord,$iLanguage);
	
}
?>