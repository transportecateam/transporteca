<?php
/**
 * Blog  
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );

$style="style='top: 0px; bottom: auto; display: block;'";
$headerSearchFlag=1;
require_once( __APP_PATH_LAYOUT__ . "/header_new.php" );
$t_base="TermCondition/";
$kExplain = new cExplain();
$link = array();
$oldUrl="/blog/".$_REQUEST['flag']."/";

$iLanguage = getLanguageId();
if($_SERVER['REDIRECT_URL']==$oldUrl)
{
	$newredirectUrl=__BLOG_PAGE_URL__."".$_REQUEST['flag']."/";
	header('Location:'.$newredirectUrl );
	exit();
}
if(isset($_REQUEST['flag'])  && isset($_REQUEST['menuflag']) && $_REQUEST['menuflag'] == 'blg')
{	
	if(!empty($_REQUEST['flag']))
	{	
		$link = $kExplain->selectBlogLink($_REQUEST['flag'],$iLanguage);
	}
	else
	{	
		$link = $kExplain->selectBlogLink(false,$iLanguage);
	}
}

$blogContent = $kExplain->serviceBlog($link['id']);
if($link == array())
{
    echo "on here ";
    pageNotFound();
    
}
else{

if($link['iLanguage']!=$iLanguage)
{
	$redirect_url = __BASE_URL__.'/explain/';
	header("Location:".$redirect_url);
	exit;
}

$blogPageRelatedLinksAry = array();
$blogPageRelatedLinksAry = $kExplain->selectBlogDetailsActive($iLanguage);

?>
<div>
<div id="hsbody" class="body-color-white">  
		<?php 
			echo refineDescriptionData(html_entity_decode($blogContent['szContent'])); 
				
		if(count($blogPageRelatedLinksAry)>1){?>
			<br/>
			<p class="para"><strong><?php echo t($t_base_explain.'fields/knowledge_center_other_topics');?></strong></p>			 
			<ul class="ulclass">
			<?php
				foreach($blogPageRelatedLinksAry as $blogPageRelatedLinksArys)
				{
					if($blogPageRelatedLinksArys['id']!=$blogContent['id'])
					{?>
						<li><a href="<?php echo __BLOG_PAGE_URL__.$blogPageRelatedLinksArys['szLink'].'/'?>"><?php echo $blogPageRelatedLinksArys['szHeading']?></a></li>
					<?php
					}
				}
			?>
			</ul>	
		<?php }   ?>
	</div>
</div> 
<?php
}
include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>