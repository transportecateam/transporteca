<?php
namespace Transporteca;

class Transporteca
{
    // @var string The Transporteca API key to be used for requests.
    public static $apiKey;

    // @var string The base URL for the Transporteca API.
    public static $apiBase;
    
    // @var string mode for the Transporteca API.
    public static $apiMode;
    
  
    // @var string|null The version of the Transporteca API to use for requests.
    public static $apiVersion = "1.0";

    // @var boolean Defaults to true.
    public static $verifySslCerts = false;
    
    // @var boolean Defaults to false.
    public static $mode = true;
    public static $applicationType = 'json';

    const VERSION = '1.0';
    public static $apiEndPoint;
    public static $apiMethod; 
    public static $apiHeader;
    public static $apiParams;
    public static $apiResponse;
    public static $apiResponseCode; 
    public static $apiResponseHeaders; 

    /**
     * @return string The API key used for requests.
     */
    public static function getApiKey()
    {
        return self::$apiKey;
    } 
    /**
     * Sets the API key to be used for requests.
     *
     * @param string $apiKey
     */
    public static function setApiKey($apiKey,$mode='TEST')
    {
        self::$apiKey = $apiKey;
        self::setApiMode($mode);
        self::$apiBase = __TRANSPORTECA_API_END_POINT__;
        return new Transporteca();
    }
    
    /**
     * @return string The API Base url used for requests.
     */
    public static function getApiBase()
    {
        return self::$apiBase;
    } 
    
    /**
     * Sets the API mode to be used for requests.
     *
     * @param string $mode
     */
    public static function setApiMode($mode)
    {
        self::$apiMode = $mode;
    } 
    /**
     * @return string The API version used for requests. null if we're using the
     *    latest version.
     */
    public static function getApiVersion()
    {
        return self::$apiVersion;
    }

    /**
     * @param string $apiVersion The API version to use for requests.
     */
    public static function setApiVersion($apiVersion)
    {
        self::$apiVersion = $apiVersion;
    }

    /**
    * @return boolean
    */
    public static function getVerifySslCerts()
    {
        return self::$verifySslCerts;
    }

    /**
    * @param boolean $verify
    */
    public static function setVerifySslCerts($verify)
    {
        self::$verifySslCerts = $verify;
    }
    
    public static function __convertToJson($requestAry)
    {
        return  Util\Util::__toJson($requestAry);
    }
}
