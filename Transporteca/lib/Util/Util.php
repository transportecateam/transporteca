<?php

namespace Transporteca\Util;
use Transporteca\Error;

use Transporteca\TransportecaObject;

abstract class Util
{
    /**
     * Whether the provided array (or other) is a list rather than a dictionary.
     *
     * @param array|mixed $array
     * @return boolean True if the given object is a list.
     */
    public static function isList($array)
    {
        if (!is_array($array)) {
            return false;
        }

      // TODO: generally incorrect, but it's correct given Transporteca's response
        foreach (array_keys($array) as $k) {
            if (!is_numeric($k)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Recursively converts the PHP Transporteca object to an array.
     *
     * @param array $values The PHP Transporteca object to convert.
     * @return array
     */
    public static function convertTransportecaObjectToArray($values)
    {
        $results = array();
        foreach ($values as $k => $v) {
            // FIXME: this is an encapsulation violation
            if ($k[0] == '_') {
                continue;
            }
            if ($v instanceof TransportecaObject) {
                $results[$k] = $v->__toArray(true);
            } elseif (is_array($v)) {
                $results[$k] = self::convertTransportecaObjectToArray($v);
            } else {
                $results[$k] = $v;
            }
        }
        return $results;
    }

    /**
     * Converts a response from the Transporteca API to the corresponding PHP object.
     *
     * @param array $resp The response from the Transporteca API.
     * @param array $opts
     * @return TransportecaObject|array
     */
    public static function convertToTransportecaObject($resp, $opts)
    {
        $types = array(
            'account' => 'Transporteca\\Account',
            'alipay_account' => 'Transporteca\\AlipayAccount',
            'bank_account' => 'Transporteca\\BankAccount',
            'balance_transaction' => 'Transporteca\\BalanceTransaction',
            'card' => 'Transporteca\\Card',
            'charge' => 'Transporteca\\Charge',
            'coupon' => 'Transporteca\\Coupon',
            'customer' => 'Transporteca\\Customer',
            'dispute' => 'Transporteca\\Dispute',
            'list' => 'Transporteca\\Collection',
            'invoice' => 'Transporteca\\Invoice',
            'invoiceitem' => 'Transporteca\\InvoiceItem',
            'event' => 'Transporteca\\Event',
            'file' => 'Transporteca\\FileUpload',
            'token' => 'Transporteca\\Token',
            'transfer' => 'Transporteca\\Transfer',
            'order' => 'Transporteca\\Order',
            'plan' => 'Transporteca\\Plan',
            'product' => 'Transporteca\\Product',
            'recipient' => 'Transporteca\\Recipient',
            'refund' => 'Transporteca\\Refund',
            'sku' => 'Transporteca\\SKU',
            'subscription' => 'Transporteca\\Subscription',
            'fee_refund' => 'Transporteca\\ApplicationFeeRefund',
            'bitcoin_receiver' => 'Transporteca\\BitcoinReceiver',
            'bitcoin_transaction' => 'Transporteca\\BitcoinTransaction',
        );
        if (self::isList($resp)) {
            $mapped = array();
            foreach ($resp as $i) {
                array_push($mapped, self::convertToTransportecaObject($i, $opts));
            }
            return $mapped;
        } elseif (is_array($resp)) {
            if (isset($resp['object']) && is_string($resp['object']) && isset($types[$resp['object']])) {
                $class = $types[$resp['object']];
            } else {
                $class = 'Transporteca\\TransportecaObject';
            }
            return $class::constructFrom($resp, $opts);
        } else {
            return $resp;
        }
    }

    /**
     * @param string|mixed $value A string to UTF8-encode.
     *
     * @return string|mixed The UTF8-encoded string, or the object passed in if
     *    it wasn't a string.
     */
    public static function utf8($value)
    { 
        if (is_string($value) && mb_detect_encoding($value, "UTF-8", true) == "UTF-8") { 
            return utf8_encode($value);
        } else {
            return $value;
        }
    }
    
    public static function __interpretUtf8String($responseAry)
    { 
        if(!empty($responseAry))
        { 
            foreach($responseAry as $key=>$value)
            {   
                if(is_array($value))
                {
                    $responseAry[$key] = self::__interpretUtf8String($value);
                }
                else if(is_string($value) && (mb_detect_encoding($value, "UTF-8", true) != "UTF-8"))
                {
                    $szDecodedString = utf8_decode($value); 
                    $responseAry[$key] = $szDecodedString; 
                }
            } 
            return $responseAry; 
        }
    }

    public static function __toJson($requestAry)
    {
        if(empty($requestAry) || !is_array($requestAry))
        {
            $message = 'The argument must be in php array format';
            throw new Error\Api($message);
        }
        else
        { 
            $requestJson = json_encode($requestAry); 
            return $requestJson;
        }
    }
    public static function __decodeJson()
    {
        try { 
            $resp = json_decode($rbody, true); 
        } catch (Exception $e) {
            $msg = "Invalid response body from API: $rbody "
              . "(HTTP response code was $rcode)";
            throw new Error\Api($msg, $rcode, $rbody);
        } 
        if ($rcode < 200 || $rcode >= 300) {
            $this->handleApiError($rbody, $rcode, $rheaders, $resp);
        }
        return $resp;
    }
}
