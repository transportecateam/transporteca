<?php

namespace Transporteca\HttpClient;

use Transporteca\Transporteca;
use Transporteca\Error;
use Transporteca\Util;

class CurlClient implements ClientInterface
{
    private static $instance;

    public static function instance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function request($method, $absUrl, $headers, $params, $hasFile)
    { 
        $curl = curl_init();
        $method = strtolower($method);
        $opts = array();
        if ($method == 'get') { 
            $opts[CURLOPT_HTTPGET] = 1;
            if (count($params) > 0) {
                $encoded = self::encode($params,'transporteca');
                $absUrl = "$absUrl?$encoded";
            }
        } elseif ($method == 'post') { 
            $opts[CURLOPT_POST] = 1; 
            $opts[CURLOPT_POSTFIELDS] = self::encodeParams($params,'transporteca');   
        } elseif ($method == 'delete') {
            $opts[CURLOPT_CUSTOMREQUEST] = 'DELETE';
            if (count($params) > 0) {
                $encoded = self::paramEncode($params);
                $absUrl = "$absUrl?$encoded";
            }
        } else {
            throw new Error\Api("Unrecognized method $method");
        }
        
        // Create a callback to capture HTTP headers for the response
        $rheaders = array();
        $headerCallback = function ($curl, $header_line) use (&$rheaders) {
            // Ignore the HTTP request line (HTTP/1.1 200 OK)
            if (strpos($header_line, ":") === false) {
                return strlen($header_line);
            }
            list($key, $value) = explode(":", trim($header_line), 2);
            $rheaders[trim($key)] = trim($value);
            return strlen($header_line);
        }; 
        
        $absUrl = Util\Util::utf8($absUrl);  
        $opts[CURLOPT_URL] = $absUrl;
        $opts[CURLOPT_USERPWD] = 'Test123456';
        $opts[CURLOPT_RETURNTRANSFER] = true;
        $opts[CURLOPT_CONNECTTIMEOUT] = 30;
        $opts[CURLOPT_TIMEOUT] = 80;
        $opts[CURLOPT_RETURNTRANSFER] = true;
        $opts[CURLOPT_HEADERFUNCTION] = $headerCallback;
        $opts[CURLOPT_HTTPHEADER] = $headers;
        if (!Transporteca::$verifySslCerts) {
            $opts[CURLOPT_SSL_VERIFYPEER] = false;
        } 
        curl_setopt_array($curl, $opts);
        $rbody = curl_exec($curl);
            
        if (!defined('CURLE_SSL_CACERT_BADFILE')) {
            define('CURLE_SSL_CACERT_BADFILE', 77);  // constant not defined in PHP
        } 
        if ($rbody === false) {
            $errno = curl_errno($curl);
            $message = curl_error($curl);
            curl_close($curl);
            $this->handleCurlError($absUrl, $errno, $message);
        }  
        $rcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        return array($rbody, $rcode, $rheaders);
    }
    /** 
     * @param string $params 
     */
    private static function paramEncode($params)
    {
        return "transportecaParams = ".$params; 
    }

    /**
     * @param number $errno
     * @param string $message
     * @throws Error\ApiConnection
     */
    private function handleCurlError($url, $errno, $message)
    {
        switch ($errno) {
            case CURLE_COULDNT_CONNECT:
            case CURLE_COULDNT_RESOLVE_HOST:
            case CURLE_OPERATION_TIMEOUTED:
                $msg = "Could not connect to Transporteca ($url).  Please check your "
                 . "internet connection and try again.  If this problem persists, "
                 . "you should check Transporteca's service status at "
                 . "https://partner.transporteca.com/docs/errorCodes, or";
                break;
            case CURLE_SSL_CACERT:
            case CURLE_SSL_PEER_CERTIFICATE:
                $msg = "Could not verify Transporteca's SSL certificate.  Please make sure "
                 . "that your network is not intercepting certificates.  "
                 . "(Try going to $url in your browser.)  "
                 . "If this problem persists,";
                break;
            default:
                $msg = "Unexpected error communicating with Transporteca.  "
                 . "If this problem persists,";
        }
        $msg .= " let us know at contact@transporteca.com";

        $msg .= "\n\n(Network error [errno $errno]: $message)";
        throw new Error\ApiConnection($msg);
    } 
    private static function caBundle()
    {
        return dirname(__FILE__) . '/../../data/ca-certificates.crt';
    }
    
    /**
     * @param array $arr An map of param keys to values.
     * @param string|null $prefix
     *
     * Only public for testability, should not be called outside of CurlClient
     *
     * @return string A querystring, essentially.
     */
    
    public static function encodeParams($arr,$prefix=null)
    {
        if (!is_array($arr)) {
            return $arr;
        }  
        $tempCargoAry = array();
        $tempCustomAry = array();
        if(isset($arr['cargo']))
        {
            $tempCargoAry = $arr['cargo']; 
            unset($arr['cargo']); 
        }
        $tempCustomAry = $arr;  
        if(strtoupper(Transporteca::$apiMode)=='LIVE')
        {
            $tempCustomAry['transportecaPartnerTestApiCall'] = 0;
        }
        else
        {
            $tempCustomAry['transportecaPartnerTestApiCall'] = 1;
        } 
        $returnAry = self::encode($tempCustomAry,$prefix,true); 
        
        $cargoArt = self::encodeCargo($tempCargoAry,'transporteca[cargo]'); 
        $returnStr  = implode("&", $returnAry)."&".$cargoArt;  
        return $returnStr; 
    }
    
    /**
     * @param array $arr An map of param keys to values.
     * @param string|null $prefix
     *
     * This function is created to encode cargo array in predefined format. This may not work for other encoding tasks
     *
     * @return string A querystring, essentially.
     */
    public static function encodeCargo($cargoAry,$prefix=null)
    {  
        if(!is_array($cargoAry))
        {
            return $cargoAry;
        }
        $r = array();
        $ctr=0;
        foreach ($cargoAry as $cargoArys) 
        {
            foreach($cargoArys as $k => $v)
            { 
                $r[] = urlencode($prefix.'['.$ctr.']['.$k.']'). "=". $v; 
            } 
            $ctr++;
        } 
        return implode("&", $r);
    }

    /**
     * @param array $arr An map of param keys to values.
     * @param string|null $prefix
     *
     * Only public for testability, should not be called outside of CurlClient
     *
     * @return string A querystring, essentially.
     */
    public static function encode($arr, $prefix = null, $customEncode=false)
    { 
        if (!is_array($arr)) {
            return $arr;
        }  
        $r = array();
        foreach ($arr as $k => $v) {
            if (is_null($v)) {
                continue;
            } 
            if(!is_numeric($v))
            {
                $v = Util\Util::utf8($v);
            }
            if ($prefix && $k && !is_int($k)) {
                $k = $prefix."[".$k."]";
            } elseif ($prefix) {
                $k = $prefix."[]";
            }

            if (is_array($v)) {
                $enc = self::encode($v, $k);
                if ($enc) {
                    $r[] = $enc;
                } 
            } else {
                $r[] = urlencode($k)."=".urlencode($v);
            }
        }   
        if($customEncode)
        { 
            return $r;
        }
        else
        {
            return implode("&", $r);
        }
    }
}
