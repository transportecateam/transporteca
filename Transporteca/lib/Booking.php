<?php

namespace Transporteca;

class Booking extends ApiResource
{ 
    /**
     * @param array $params
     * @param array|string|null $options
     *
     * @return servie list in json object.
     */
    public static function createBooking($params = null, $options = null)
    {
        return self::_create($params, $options);
    } 
    
    public static function confirmPayment($params = null, $options = null)
    {
        $szApiEndPoint = "/api/confirmPayment/";
        return self::_create($params, $options,$szApiEndPoint);
    } 
    public static function getLabels($params = null, $options = null)
    {
        $szApiEndPoint = "/api/getLabels/";
        return self::_create($params, $options,$szApiEndPoint);
    }  
    public static function confirmDownload($params = null, $options = null)
    {
        $szApiEndPoint = "/api/confirmDownload/";
        return self::_create($params, $options,$szApiEndPoint);
    }  
    public static function submitNPS($params = null, $options = null)
    {
        $szApiEndPoint = "/api/submitNPS/";
        return self::_create($params, $options,$szApiEndPoint);
    } 
    
}
