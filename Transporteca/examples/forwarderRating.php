<?php  
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) ); 
require_once(__APP_PATH__."/config.php");
//ini_set('display_errors',false); 
//error_reporting(0); 
 
$apiRequestParams = array();   
$apiRequestParams['szForwarderAlias'] = 'SM'; //Unique alias name of Transporteca's Freight forwarder 

$szTransportecApiKey = "3f31e6d2729d90a98024468a3f3b2bf5"; //Please update your Partner API Key
$kTransporteca = \Transporteca\Transporteca::setApiKey($szTransportecApiKey,'LIVE');  
 
try
{
    $transportecaResponseAry = \Transporteca\Service::forwarderRating($apiRequestParams);     
    
    echo "<br><br> Your API Request <br><br> ";
    print_R($apiRequestParams);
    
    echo "<br><br> Your API Response <br><br> ";
    print_R($transportecaResponseAry);
} 
catch (Exception $e)
{
    echo "Exception: ".$e->getMessage();
}

?>
