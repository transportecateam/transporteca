<?php
/*
 * This files loads all the class and interface that is being used in this client library
 * @author Transporteca Ltd
 * @package Transporteca
 * @version 1.0
 */
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
  
define( "__TRANSPORTECA_APP_PATH__", __APP_PATH__."/Transporteca" );
define( "__TRANSPORTECA_API_END_POINT__", "http://api.transporteca.com");
 
require(__TRANSPORTECA_APP_PATH__ . '/lib/Transporteca.php');

// Utilities
require(__TRANSPORTECA_APP_PATH__. '/lib/Util/RequestOptions.php');
require(__TRANSPORTECA_APP_PATH__ . '/lib/Util/Set.php');
require(__TRANSPORTECA_APP_PATH__ . '/lib/Util/Util.php');

// HttpClient
require(__TRANSPORTECA_APP_PATH__ . '/lib/HttpClient/ClientInterface.php');
require(__TRANSPORTECA_APP_PATH__ . '/lib/HttpClient/CurlClient.php');

// Errors
require(__TRANSPORTECA_APP_PATH__ . '/lib/Error/Base.php');
require(__TRANSPORTECA_APP_PATH__ . '/lib/Error/Api.php');
require(__TRANSPORTECA_APP_PATH__ . '/lib/Error/ApiConnection.php');
require(__TRANSPORTECA_APP_PATH__ . '/lib/Error/Authentication.php');
require(__TRANSPORTECA_APP_PATH__ . '/lib/Error/Service.php');
require(__TRANSPORTECA_APP_PATH__ . '/lib/Error/InvalidRequest.php'); 

//Parsers and processors
require(__TRANSPORTECA_APP_PATH__ . '/lib/JsonSerializable.php');
require(__TRANSPORTECA_APP_PATH__ . '/lib/ApiRequestor.php');
require(__TRANSPORTECA_APP_PATH__ . '/lib/ApiResource.php');

//Transporteca API Resources
require(__TRANSPORTECA_APP_PATH__ . '/lib/Services.php'); 
require(__TRANSPORTECA_APP_PATH__ . '/lib/Booking.php'); 
require(__TRANSPORTECA_APP_PATH__ . '/lib/Customer.php'); 



?>
