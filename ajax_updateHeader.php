<?php
/**
 * Change Customer Account Password
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );
$t_base = "home/";
$kUser = new cUser();
$mode = sanitize_all_html_input(trim($_REQUEST['mode']));
if($mode=='SHOW_CONFIRMATION')
{
	$page_url = sanitize_all_html_input(trim($_REQUEST['page_url']));
	?>
	 <div id="popup-bg"></div>
		 <div id="popup-container">			
		  <div class="popup compare-popup">
		  	<h5><strong>Notification</strong></h5>
			<p>The booking is saved in Draft</p>
			<br /><br />
			 <p align="center">
			 	<a href="<?=$page_url?>" class="button1"><span>Ok</span></a>
			</p>	
		  </div>
	   </div>
	</div>
	<?php 
}
else if(!empty($_REQUEST['currencyAry']))
{
	if($kUser->updateUserWithCurrency($_REQUEST['currencyAry']['idCurrency']))
	{
		if(!empty($_REQUEST['currencyAry']['szRedirectUrl']))
		{
			echo "REDIRECT||||";
			die;
		}
		else
		{
			echo "SUCCESS||||";
			die;
		}
	}
}
else if((int)$_SESSION['user_id']>0)
{
	$kUser = new cUser();
	$kUser->getUserDetails($_SESSION['user_id']);
?>
	<div class="user" onmouseover='$("#show_link").attr("style","display:block;");' onmouseout='$("#show_link").attr("style","display:none;");'>
		<a href="<?=__BASE_URL__?>/myAccount/" >
			<? if($kUser->iIncompleteProfile == 1){
				 if(!empty($kUser->szFirstName) || !empty($kUser->szLastName)){?>
				 	<p id="userInfo"><?=$kUser->szFirstName?>&nbsp;<?=$kUser->szLastName?></p>
				 <?php } else {?>
					<p id="userInfo"><?=$kUser->szEmail?></p>
			<? } } else {?>					
				<p id="userInfo"><?=$kUser->szFirstName?>&nbsp;<?=$kUser->szLastName?></p>
			<? } ?>
		</a>
		<div id="show_link" style="display:none;">
			<p class="logout_account"><a href="<?=__USER_LOGOUT_URL__?>"><img src="<?=__BASE_STORE_IMAGE_URL__?>/logout-icon.png" alt="Logout" title="Logout" border="0" /></a></p>
			<p class="user_account"><a href="<?=__USER_MY_ACCOUNT_URL__?>"><?=t($t_base.'forwarder_header/myAccount');?></a></p>
		</div>				
	</div>
<?php }else{  
?>
				<p class="user" >
				<div class="header_login">
					<form id="loginForm_header" name="loginForm" method="post" action="">
						<input type="email" name="loginArr[szEmail]" id="szHeaderEmail" value="<?=!empty($_COOKIE['__USER_EMAIL_COOKIE__']) ? $_COOKIE['__USER_EMAIL_COOKIE__'] : t($t_base.'header/email');?>" onfocus="blank_me(this.id,'<?=t($t_base.'header/email');?>')" onblur="show_me(this.id,'<?=t($t_base.'header/email');?>')"  >
						<input type="text" name="loginArr[szPassword]" id="szHeaderPassword" value="<?=t($t_base.'header/password');?>" onfocus="blank_me_password(this.id,'<?=t($t_base.'header/password');?>')" onblur="show_me_password(this.id,'<?=t($t_base.'header/password');?>')" onkeyup="on_enter_key_press(event,'header_login');">
						<input type="hidden" name="loginArr[szRedirectUrl]" id="szHeaderRedirectUrl" value="<?=__LANDING_PAGE_URL__?>" >
					</form>
				</div>
				<a href="javascript:void(0)" onclick="userLogin('loginForm_header');" id="login"><?=t($t_base.'header/sign_in');?></a>
				</p>				
				
				<?php }?>