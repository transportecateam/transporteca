<?php
ob_start();
session_start();

//ini_set('display_error',1);
//error_reporting(E_ALL);
//ini_set('error_reporting',E_ALL);

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
$display_abandon_message = true;

$szMetaTitle = __BOOKING_QUOTE_SERVICE_PAGE_META_TITLE__;
$szMetaKeywords = __SERVICES_PAGE_META_KEYWORDS__;
$szMetaDescription = __SERVICES_PAGE_META_DESCRIPTION__;

$iNewServicePage = 1;
$iNewServicePageFlag = 1;
$iDonotIncludeHiddenSearchHeader = 1;
require_once(__APP_PATH_LAYOUT__."/header_new.php");

$t_base = "SelectService/";
constantApiKey();
 
/*
if(!empty($_POST['searchAry']))
{
	$_SESSION['sess_search_ary_ajax'] = $_POST['searchAry'];
}
*/
if(!empty($_REQUEST['booking_random_num']))
{
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num']));
}
$kBooking = new cBooking();
$kConfig = new cConfig();
$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);


$postSearchAry = array();
$postSearchAry = $kBooking->getBookingDetails($idBooking); 
//
//echo "<br> Ref (%): ".$postSearchAry['fReferalPercentage'];
//echo "<br> Ref amount: ".$postSearchAry['fReferalAmount'];
//echo "<br> Exchange rate: ".$postSearchAry['fForwarderExchangeRate'];
//echo "<br> Ref (USD): ".$postSearchAry['fReferalAmountUSD'];

$iTodayMinusOne = strtotime("-1 DAY"); 
$iTodayMinusOneDateTime = strtotime(date('Y-m-d 23:59:59',$iTodayMinusOne)); 
 
if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
{
    echo display_booking_already_paid($t_base,$idBooking);
    die;
} 
else if((strtotime($postSearchAry['dtQuoteValidTo']) < $iTodayMinusOneDateTime ) || ($postSearchAry['iQuotesStatus']==__BOOKING_QUOTES_STATUS_EXPIRED__))
{ 
    $kConfig->loadCountry($postSearchAry['idOriginCountry']);
    $szCountryStrUrl = $kConfig->szCountryISO ;
    $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
    $szCountryStrUrl .= $kConfig->szCountryISO ; 

    $redirect_url =  __BOOKING_QUOTE_EXPIRED_PAGE_URL__."/".$szBookingRandomNum."/".__BOOKING_QUOTE_SERVICE_PAGE_SUFFIX__."-".$szCountryStrUrl."/" ;
   
    ob_end_clean();
    header("location:".$redirect_url);
    exit;
} 
$szResultPageMetaTitle = t($t_base.'messages/SERVICE_PAGE_META_TITLE');
$iLanguage = getLanguageId();
$kConfig = new cConfig();
$languageArr=$kConfig->getLanguageDetails('',$iLanguage,'','','','','',false);
if(!empty($languageArr))
{
    $szLangParam = strtolower($languageArr[0]['szName']);
}
else
{
    $szLangParam = 'english';
}
/*
 * 
 */
$kVatApplication = new cVatApplication();
$kVatApplication->updateVatonBooking($idBooking);

$bookingIdAry = array();
$bookingIdAry[0] = $idBooking ;
$newInsuredBookingAry = array();
$newBookingQuotesAry = $kBooking->getAllBookingQuotes(false,$bookingIdAry);

$postSearchAry = array();
$postSearchAry = $newBookingQuotesAry[0];   
 
?>
<style type="text/css" media="screen">
 .layout { padding: 50px; font-family: Georgia, serif; }
 .layout-slider { margin-bottom: 20px; width: 50%; }
 .layout-slider-settings { font-size: 12px; padding-bottom: 10px; }
 .layout-slider-settings pre { font-family: Courier; }
</style>

<div id="customs_clearance_pop_right" class="help-pop right">
</div>  
<div id="all_available_service" style="display:none;"></div>
<div id="customs_clearance_pop1" style="display:none;"></div>
<div id="support_heading_bubble" style="display:none;"></div>
<div id="change_price_div" style="display:none"></div>

<section id="search-result">
	<div id="service-search-form-container">
            <?php
                echo display_services_quotes_listing($postSearchAry,false,false,true);
            ?>
	</div>
    <div id="service-listing-container"></div>
    <div id="rating_popup"></div>
</section>  
<input type="hidden" name="showBookingFieldsFlag" value="" id="showBookingFieldsFlag">
<input type="hidden" name="szServiceUniqueKey" value="" id="szServiceUniqueKey">
<input type="hidden" name="iShipperConsigneeSubmit" value="1" id="iShipperConsigneeSubmit">

<?php
$iServicePageAdword = 1 ;
$iDonotShowLanguageOption = 1;
echo html_form_random_booking_number(__BOOKING_DETAILS_PAGE_URL__,$szBookingRandomNum);
require_once(__APP_PATH_LAYOUT__."/footer_new.php");

?>