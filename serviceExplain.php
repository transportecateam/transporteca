<?php
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/header_new.php" );
$t_base="management/explaindata/";

if($idExplain !='')
{
	$headingDetails = $kExplain->selectPageDetails($idExplain);
	
	$iLanguage = getLanguageId();
	if($headingDetails[0]['iLanguage']!=$iLanguage)
	{
		$redirect_url = __BASE_URL__.'/explain/';
		header("Location:".$redirect_url);
		exit;
	}
	
	$heading = $headingDetails[0]['szPageHeading'];
	$replacearray=array(',',' ','?');
	unset($headingDetails);


?>
<div id="hsbody-2">
<?php 

	require_once( __APP_PATH_LAYOUT__ ."/explain_left_nav_user.php" );
	$t_base="management/explaindata/"; 
?> 
	<div class="hsbody-2-right explain-content">
		<h2><?=$heading?></h2>	
		<br />	
		<?
		if($previewData != array())
		{
			foreach($previewData as $preview){?>
			<a name="<?=str_replace($replacearray,"_",strtolower($preview['szHeading']))?>" href="javascript:void(0);" ></a>
			<h5 class="clearfix terms-heading">
			<span class="fl-80"><strong><?=ucfirst($preview['szHeading'])?></strong></span>
			<a class="fr" href="#top"><strong><?=t($t_base.'title/top')?></strong></a>
			</h5>
			<div class="link16">
			<?=refineDescriptionData($preview['szDescription'])?>
			</div>			
			<br/>
			<? } 
			}	?>
			<br />		
	</div>	
</div>	
<?php
}
else
{
	pageNotFound();
}
require_once( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>