<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

$szMetaTitle= __PARTNER_PROGRAM_PAGE_META_TITLE__;
$szMetaDescription= __PARTNER_PROGRAM_PAGE_META_DESCRIPTION__;
$szMetaKeywords= __PARTNER_PROGRAM_PAGE_META_KEYWORDS__;
$style="style='top: 0px; bottom: auto; display: block;'";
$headerSearchFlag=1;
require_once( __APP_PATH_LAYOUT__ . "/header_new.php" );
$t_base = "Affiliate/";
?>
<div id="hsbody">
	<h1><?=t($t_base.'title/the_transporteca_affiliate_programme');?></h1><br/>
	<p><img style="float: right; margin-left: 15px; margin-right: 0px;" title="Transporteca Partner Program" src="http://staging.transporteca.com/images/PartnerProgramme.gif" alt="Transporteca Partner Program" width="200" height="170" /></p>
	<p><?=t($t_base.'notes/notes_1');?></p>
	<br/>
	<p><?=t($t_base.'notes/notes_2');?></p>
	<br/>
	<p><?=t($t_base.'notes/notes_3');?></p>
	<br/>
	<div id="affiliatesignup">
	<?
		require_once( __APP_PATH__ . "/ajax_affiliateProgram.php" ); ?>
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>