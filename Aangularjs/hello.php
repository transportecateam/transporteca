<?php
ob_start();
session_start();
 
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" ); 
require_once( __APP_PATH_LAYOUT__ . "/admin_header.php" );
?>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.0-beta.17/angular.min.js"></script>
<div ng-app="myapp" ng-init="countries=[{locale:'en-US',name:'United States'},{locale:'en-GB',name:'United Kingdom'},{locale:'en-FR',name:'France'}]">
    <div ng-controller="HelloController">
        <h2>Welcome {{helloTo.title}} to the world of</h2>
        <p>Enter your Name: <input type="text" ng-model="name"></p>
        <p>Hello <span ng-bind="name"></span>!</p>
        
        Enter first name: <input type="text" ng-model="student.firstName"><br><br>
        Enter last name: <input type="text" ng-model="student.lastName"><br>
        <br>
        You are entering: {{student.fullName()}}
    </div> 
    <p>List of Countries with locale:</p>
    <ol>
        <li ng-repeat="country in countries">
            {{ 'Country: ' + country.name + ', Locale: ' + country.locale }}
        </li>
    </ol>
</div> 

<script>
    angular.module("myapp", []).controller("HelloController", function($scope) {
        $scope.helloTo = {};
        $scope.helloTo.title = "Transporteca Apps";
        
        $scope.student = {
                firstName: "Ajay",
                lastName: "Jha",
                fullName: function() {
                var studentObject;
                studentObject = $scope.student;
                return studentObject.firstName + " " + studentObject.lastName;
            }
        };
    });
</script>
<?php
    //require_once( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>