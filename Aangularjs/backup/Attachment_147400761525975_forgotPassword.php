<?php
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

$t_base = "Users/AccountPage/";
$t_base_error="Error";
$kUser =new cUser();
$successFlag=false;
$mode = sanitize_all_html_input(trim($_REQUEST['mode']));
$szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page']));
if(!empty($_POST))
{
	if($kUser->forgotPassword($_POST['szEmail']))
	{
		$successFlag=true;
	}
}
if(!empty($_REQUEST['redirect_url']))
{
	$_POST['szRedirectUrl'] = sanitize_all_html_input(trim($_REQUEST['redirect_url'])); ;
}

if($mode=='CLICKED_ON_BOOK')
{
	$close_onclick = "close_benefit_popup('".$redirect_url."','".$mode."')";		
	$class_name ='class="compare-popup popup"';
}
else
{
	$class_name = 'class="popup signin-popup signin-popup-verification"';
}

if($mode=='CLICKED_ON_BOOK')
{
	$div_id="customs_clearance_pop1";
}
else if($mode=='TYPE_PASSWORD')
{
	$div_id="user_account_login_popup";
}
?>
<div id="forgot_password">
<div id="popup-bg"></div>
<div id="popup-container">
<div <?=$class_name?> style="width:350px;">
<p class="close-icon" align="right">
<?php if($mode=='TYPE_PASSWORD'){?>
	<a onclick="showHide('user_account_login_popup')" href="javascript:void(0);"><img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png"></a>
<?php }else{ ?>
	<a onclick="close_benefit_popup('<?=$_POST['szRedirectUrl']?>','<?=$mode?>');" href="javascript:void(0);"><img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png"></a>
<?php }?>
</p>
<h5><strong><?=t($t_base.'messages/forgot_password_heading');?></strong></h5>
			<?php
	if($successFlag)
	{
		?>
			<p><?=t($t_base.'messages/forgot_password_success_msg1');?> <?=$_POST['szEmail']?>. <?=t($t_base.'messages/forgot_password_success_msg2');?></p>
			<br />
			<p align="center"><a href="javascript:void(0);" class="button1" onclick="close_benefit_popup('<?=$_POST['szRedirectUrl']?>','<?=$mode?>');"><span><?=t($t_base.'fields/close')?></span></a></p>
		<?			
	}
	else
	{
			if(!empty($kUser->arErrorMessages)){
			?>
			<div id="regError" class="errorBox ">
			<div class="header"><?=t($t_base_error.'/please_following');?></div>
			<div id="regErrorList">
			<ul>
			<?php
				foreach($kUser->arErrorMessages as $key=>$values)
				{
				?><li><?=$values?></li>
				<?php	
				}
			?>
			</ul>
			</div>
			</div>
			<?php }?>
			<form name="forgotPassword" action="javascript:void(0)" id="forgotPassword" method="post">
			<p><?=t($t_base.'messages/type_your_email_address_user_profile');?></p><br>
			<div class="oh">
				<p class="fl-33"><?=t($t_base.'fields/signin_email');?></p>
				<p class="fl-65"><input type="text" style="width:210px;" onkeyup="on_enter_key_press(event,'forgot_passward_popup','')" name="szEmail" id="szEmail" value="<?=$_REQUEST['szEmail']?>"/></p>
			</div>
			<br />
			<input type="hidden" name="szRedirectUrl"  value="<?=$_POST['szRedirectUrl']?>">
			<input type="hidden" name="mode"  value="<?=$mode?>">
			
			<p align="center">	
			 <?php if($mode=='TYPE_PASSWORD'){?> 
				<a href="javascript:void(0);" class="button1" onclick="showHide('user_account_login_popup')" ><span><?=t($t_base.'fields/close')?></span></a>
			<?php } else {?>
				<a href="javascript:void(0);" class="button1" onclick="close_benefit_popup('<?=$_POST['szRedirectUrl']?>','<?=$mode?>');"><span><?=t($t_base.'fields/close')?></span></a>
			<?php }?>
				<a href="javascript:void(0);" class="button1" onclick="forgot_passward_popup('<?php echo $div_id;?>');"><span><?=t($t_base.'fields/send_me_pass');?></span></a></p>
			</form>
		<?
		}
		?>	
			</div>
		</div>
	</div>