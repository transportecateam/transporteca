<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

$szMetaTitle= __FAQ_PAGE_META_TITLE__;
$szMetaKeywords = __FAQ_PAGE_META_KEYWORDS__;
$szMetaDescription = __FAQ_PAGE_META_DESCRIPTION__;

require_once( __APP_PATH_LAYOUT__ . "/header_new.php" );

$iLanguage = getLanguageId();

$KwarehouseSearch=new cWHSSearch();
$faq=$KwarehouseSearch->selectQuery(3,$iLanguage);
$replacearray=array(',',' ','?');
$t_base="FAQ/"

?>

<div id="hsbody-2">
<?php 
	require_once( __APP_PATH_LAYOUT__ ."/explain_left_nav_user.php" ); 
	$t_base="FAQ/"
?> 
	<div class="hsbody-2-right explain-content link16">
	<h2><?=t($t_base.'title/frequently_ask_question')?></h2>	
	<br/>
	<? if($faq!= array()){?>
		<? foreach($faq as $Faq){?>
	<a name="<?=str_replace($replacearray,"_",strtolower($Faq['szHeading']))?>" href="javascript:void(0);" ></a>	
	<p><?=t($t_base.'title/q')?>: <?=$Faq['szHeading'];?></p>
	<p style="float: left;padding-right: 5px;"><?=t($t_base.'title/a')?>:<?=$Faq['szDescription'];?></p>
	<br />
		<? }
	 } ?>
	
	
	<br />
	<!-- <p>If we receive the same question frequently, we will update it on this page.</p> -->
</div>
</div>
<?
/*
?>
<div id="hsbody">
			<h5><strong><?=t($t_base.'title/frequently_ask_question')?></strong></h5>
			
		
		<? if(!empty($faq))
				{
			foreach($faq AS $faqs)
			{				
		?>		
		
		<p>Q: <?=$faqs['szHeading']?></p>
		<p style="float: left;">A: <?=$faqs['szDescription']?></p>
		 <br/>
		 <? }} ?>
			<br/>
		</div>
<?
*/
	//require_once( __APP_PATH__ . "/footer_holding_page.php" );
	include( __APP_PATH_LAYOUT__ . "/footer.php" );
?>