<?php
ob_start();
session_start();
 
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" ); 
require_once( __APP_PATH_LAYOUT__ . "/admin_header.php" );
?>
<link rel="stylesheet" type="text/css" href="<?php echo __BASE_STORE_CSS_MANAGEMENT_URL__."/bootstrap.min.css"; ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo __BASE_STORE_CSS_MANAGEMENT_URL__."/taskman.css"; ?>"/> 
<script src="<?php echo __BASE_STORE_JS_URL__."/angular.min.js"?>"></script>
<!--<script src="<?php echo __BASE_STORE_JS_URL__."/taskController.js"?>"></script>-->

<div id="hsbody" ng-app="myApp">   
    <div id="quick_quote_booking_popup" style="display:none;"></div>  
    <div id="quick_quote_main_container" class="quick_quote_search_form_container">  
          <?php echo displayTaskManagerForm(); ?> 
    </div> 
</div> 

<?php
    require_once( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>