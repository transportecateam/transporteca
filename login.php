<?php
/**
 * Customer Login
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

$kUser = new cUser();
$kBooking = new cBooking();
$t_base_error = "Error";
$t_base='Users/AccountPage/';

if(!empty($_POST['loginArr']))
{
	if($kUser->userLogin($_POST['loginArr']))
	{
		$szBookingRandomNum = $_POST['loginArr']['szBookingRandomNum_hidden'];
		$redirect_url=__BOOKING_DETAILS_PAGE_URL__.'/'.$szBookingRandomNum.'/';
		$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		
		if((int)$idBooking>0 && (int)$_SESSION['user_id']>0)
		{
			$kUser->getUserDetails($_SESSION['user_id']);
			$res_ary=array();
			$res_ary['idUser'] = $kUser->id ;				
			$res_ary['szFirstName'] = $kUser->szFirstName ;
			$res_ary['szLastName'] = $kUser->szLastName ;
			$res_ary['szEmail'] = $kUser->szEmail ;	
			$res_ary['szCustomerAddress1'] = $kUser->szAddress1;
			$res_ary['szCustomerAddress2'] = $kUser->szAddress2;
			$res_ary['szCustomerAddress3'] = $kUser->szAddress3;
			$res_ary['szCustomerPostCode'] = $kUser->szPostcode;
			$res_ary['szCustomerCity'] = $kUser->szCity;
			$res_ary['szCustomerCountry'] = $kUser->szCountry;
			$res_ary['szCustomerState'] = $kUser->szState;
			$res_ary['szCustomerPhoneNumber'] = $kUser->szPhoneNumber;
			$res_ary['szCustomerCompanyName'] = $kUser->szCompanyName;	
			$res_ary['szCustomerCompanyRegNo'] = $kUser->szCompanyRegNo;	
			if((int)$postSearchAry['iBookingStep']<10)
			{
				$res_ary['iBookingStep'] = 10 ;
			}
			if(!empty($res_ary))
			{
				foreach($res_ary as $key=>$value)
				{
					$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
				}
			}
			
			$update_query = rtrim($update_query,",");
			//echo "<br>".$update_query ;
			$kBooking->updateDraftBooking($update_query,$idBooking);
		}
		if((int)$_SESSION['user_id']>0)
		{
			$kUser->getUserDetails($_SESSION['user_id']);
			$kUserNew = new cUser();
			$kUserNew->loadCurrency($kUser->szCurrency);
			
			if(($kUserNew->iBooking<=0)) // User is in booking steps and currency is not active.
			{
				echo "CURRENCY_CHANGED||||";
				echo displayCurrencyChangePopup($kUser->szCurrency,false,$redirect_url);
				die;
			}
		}
		if($postSearchAry['iDoNotKnowCargo']==1)
		{
			echo "UPDATE_CARGO||||".$szBookingRandomNum;
			die;
		}
		else
		{
			echo "SUCCESS||||".$redirect_url;
			die;
		}
/*
	?>
		<script type="text/javascript">
		$(location).attr('href','<?=$redirect_url?>');
		</script>
	<?	
*/
		exit();
	}
}

if(!empty($_REQUEST['szBookingRandomNum']))
{
	$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['szBookingRandomNum'])); 
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	$_POST['loginArr']['szBookingRandomNum_hidden'] =  $szBookingRandomNum ;
}
else if(!empty($_POST['loginArr']['szBookingRandomNum_hidden']))
{
	$szBookingRandomNum = $_POST['loginArr']['szBookingRandomNum_hidden'] ;
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
}

$postSearchAry = $kBooking->getBookingDetails($idBooking);
if((int)$_SESSION['user_id']>0)
{

 $szBookingRandomNum = $_POST['loginArr']['szBookingRandomNum_hidden'];
 $kUser->getUserDetails($_SESSION['user_id']);
 $redirect_url=__BOOKING_DETAILS_PAGE_URL__.'/'.$szBookingRandomNum."/";
 
 ?>
	<script type="text/javascript">
	//$(location).attr('href','<?=$redirect_url?>');
	</script>
  <?	
  die;
}
if($reload_page)
{
	$cancel_url = __HOME_PAGE_URL__ ;
}

if((int)$_POST['loginArr']['iUserType']<=0)
{
	$_POST['loginArr']['iUserType'] = 2;
}

if((int)$_POST['loginArr']['iUserType'] ==1)
{
	$button_text = t($t_base.'fields/sign_in');
}
else
{
	$button_text = t($t_base.'fields/continue');
}

if((int)$_POST['loginArr']['szCurrency']<=0)
{
	$_POST['loginArr']['idCurrency'] = $postSearchAry['idCurrency'];
}
if(empty($_POST['loginArr']['szEmail']))
{
	//$_POST['loginArr']['szEmail'] = $_COOKIE['__USER_EMAIL_COOKIE__'] ;
}
?>
<script type="text/javascript">
<?
	if(!empty($_POST['loginArr']['szEmail']))
	{
		?>
		$("#szExistingUserPassword").focus().selectionEnd;
		<?
	}
	else
	{
?>
	    $("#szExistingUserEmail").focus().selectionEnd;
<?  } ?>
</script>
<div id="popup-bg"></div>
<form name="bookingLoginForm" id="bookingLoginForm" method="post" action="">
	<div id="popup-container">			
			<div class="compare-popup popup" style="margin-top:-30px;">
			<p class="close-icon" align="right">
			<a onclick="cancel_signin('<?=$cancel_url?>','customs_clearance_pop1');" href="javascript:void(0);">
			<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
			</a>
			</p>
	<?php
	if(!empty($kUser->arErrorMessages)){
	?>
	<div id="regError" class="errorBox ">
	<div class="header"><?=t($t_base_error.'/please_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
		foreach($kUser->arErrorMessages as $key=>$values)
		{
		?><li><?=$values?></li>
		<?php	
		}
	?>
	</ul>
	</div>
	</div>
	<?php }	?>
	<h5><?=t($t_base.'title/signin')?></h5>
	<div class="oh">
		<p><?=t($t_base.'messages/booking_sigin_message')?></p>
	</div>
	<div class="oh">
		<p style="margin-left:3px;"><input type="radio" tabindex="1" onclick="change_button('<?=t($t_base.'fields/continue')?>');" name="loginArr[iUserType]" id="iNewUser" <? if($_POST['loginArr']['iUserType'] == 2){ ?>checked<? } ?> value="2" > <?=t($t_base.'messages/new_profile')?></p>
	</div>
	<div class="oh">
		<p class="fl-25" style="margin-left:20px;"><?=t($t_base.'fields/signin_email')?></p>
		<p class="fl-65"><input type="email" tabindex="2" id="szNewEmail" style="width:215px;" onkeyup="change_button('<?=t($t_base.'fields/continue')?>','2');on_enter_key_press(event,'userBookingLogin');" name="loginArr[szNewEmail]" value="<?=!empty($_POST['loginArr']['szNewEmail'])?$_POST['loginArr']['szNewEmail']:''?>" tabindex="4"/></p>	
	</div>	
	<p align="right"><a href="javascript:void(0)" class="f-size-14" tabindex="4" onclick="open_benefit_popup('customs_clearance_pop1','<?=$redirect_url?>','CLICKED_ON_BOOK');"><?=t($t_base.'links/benefits_of_sign')?></a> <a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/benefits_of_signing_in_1');?>','<?=t($t_base.'messages/benefits_of_signing_in_2');?>','signin_help_pop',event);" onmouseout="hide_tool_tip('signin_help_pop')">&nbsp;</a></p>
	<hr>
	<div class="oh">
		<p style="margin-left:3px;"><input type="radio" tabindex="3" name="loginArr[iUserType]" onclick="change_button('<?=t($t_base.'fields/sign_in')?>');" id="iExistingUser" value="1" <? if($_POST['loginArr']['iUserType'] == 1){ ?>checked<? } ?>> <?=t($t_base.'messages/have_profile')?></p>
	</div>
	<div class="oh">
		<p class="fl-25" style="margin-left:20px;"><?=t($t_base.'fields/signin_email')?></p>
		<p class="fl-65"><input type="email" tabindex="4" style="width:215px;" id="szExistingUserEmail" name="loginArr[szEmail]" onkeyup="change_button('<?=t($t_base.'fields/sign_in')?>','1');" value="<?=!empty($_POST['loginArr']['szEmail'])?$_POST['loginArr']['szEmail']:$_COOKIE['__USER_EMAIL_COOKIE__']?>" tabindex="1"/></p>	
	</div>
	<div class="oh">
		<p class="fl-25" style="margin-left:20px;"><?=t($t_base.'fields/password')?></p>
		<p class="fl-65"><input type="password" tabindex="5" style="width:215px;" id="szExistingUserPassword"  name="loginArr[szPassword]" tabindex="2" onkeyup="change_button('<?=t($t_base.'fields/sign_in')?>','1');on_enter_key_press(event,'userBookingLogin');"/></p>
	</div>
	
	<p align="right"><a href="javascript:void(0)" tabindex="6" class="f-size-14" tabindex="3" onclick="open_forgot_passward_popup('customs_clearance_pop1','<?=$redirect_url?>','CLICKED_ON_BOOK');"><?=t($t_base.'links/forgot_pass')?></a></p>
	
	
	<br style="line-height: 16px;" />
	<p align="center">
		<a href="javascript:void(0)" tabindex="7" class="button1" onclick="cancel_signin('<?=$cancel_url?>','customs_clearance_pop1');" tabindex="6"><span><?=t($t_base.'fields/cancel')?></span></a> 
		<a href="javascript:void(0)" tabindex="8" class="button1" onclick="userBookingLogin();" tabindex="7"><span id="sign_in_span"><?=$button_text?></span></a>
	</p>
	</div>
	
	<input type="hidden" id="idCurrency" name="loginArr[idCurrency]" value="<?=$_POST['loginArr']['idCurrency']?>" />
	<input type="hidden" id="szEmail" name="loginArr[szBookingRandomNum_hidden]" value="<?=$_POST['loginArr']['szBookingRandomNum_hidden']?>" />
  </div>
</form>	