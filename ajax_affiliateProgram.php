<?php
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );


$t_base = "Affiliate/";
$t_base_error="Error";
$successFlag=false;
$kConfig = new cConfig();
$kAffiliate = new cAffiliate();

$subject="Forwarder Contact Form submitted on"; 
if(!empty($_POST['affiliateProgrammArr']))
{
 	if($kAffiliate->affiliateSignUp($_POST['affiliateProgrammArr'],$subject))
 	{
 		$successFlag=true;
 	}
}
if(!empty($_POST['affiliateProgrammArr']['szPhoneNumberUpdate']))
{
	$_POST['affiliateProgrammArr']['szPhoneNumber'] = urldecode(base64_decode($_POST['affiliateProgrammArr']['szPhoneNumberUpdate']));
}
$iLanguage = getLanguageId();	
$allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);
?>
<?php
if(!empty($kAffiliate->arErrorMessages)){
?>
<div id="regError" class="errorBox ">
<div class="header"><?=t($t_base_error.'/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
	foreach($kAffiliate->arErrorMessages as $key=>$values)
	{
	?><li><?=$values?></li>
	<?php	
	}
?>
</ul>
</div>
</div>
<?php }
if($successFlag){
?>
<table border="0" cellpadding="0" cellspacing="0" class="profile-done-alert-affiliate" >
	<tr>
	<td valign="middle">
		<div class="profile-alert-content">
		   <div id="activationkey"></div>
			<p><?=t($t_base.'messages/success_msg1');?></p>			
			<p><?=t($t_base.'messages/success_msg2');?></p>
		</div>
	</div>
	</td>
	</tr>
</table>
<script type="text/javascript">
 $('input, select').attr('disabled', 'disabled');
 $("#submit_affiliate_button").removeAttr('href');
 $("#submit_affiliate_button").attr("onclick","");
 $("#submit_affiliate_button").attr("style","opacity:0.4;");
  $("#clear_affiliate_button").removeAttr('href');
 $("#clear_affiliate_button").attr("onclick","");
 $("#clear_affiliate_button").attr("style","opacity:0.4;");
 </script>
<? }?>
<form name="affiliateSignup" id="affiliateSignup" method="post">
	
		<label class="profile-fields  clearfix">
			<span class="field-name"><?=t($t_base.'fields/c_name');?> (<?=t($t_base.'fields/optional');?>)</span>
			<span class="field-container"><span><input type="text" name="affiliateProgrammArr[szCompanyName]" id="szCompanyName" value="<?=$_POST['affiliateProgrammArr']['szCompanyName']?>"/></span></span>
		</label>
		<label class="profile-fields  clearfix">
			<span class="field-name"><?=t($t_base.'fields/f_name');?></span>
			<span class="field-container"><span><input type="text" name="affiliateProgrammArr[szFirstName]" id="szFirstName" value="<?=$_POST['affiliateProgrammArr']['szFirstName']?>" /></span></span>
		</label>
		<label class="profile-fields  clearfix">
			<span class="field-name"><?=t($t_base.'fields/l_name');?></span>
			<span class="field-container"><span><input type="text" name="affiliateProgrammArr[szLastName]" id="szLastName" value="<?=$_POST['affiliateProgrammArr']['szLastName']?>" /></span></span>
		</label>
		<label class="profile-fields  clearfix">
			<span class="field-name"><?=t($t_base.'fields/email');?></span>
			<span class="field-container"><span><input type="text" name="affiliateProgrammArr[szEmail]" id="szEmail" value="<?=$_POST['affiliateProgrammArr']['szEmail']?>" /></span></span>
		</label>
		<label class="profile-fields  clearfix">
			<span class="field-name"><?=t($t_base.'fields/country');?></span>
			<span class="field-container"><span>
				<select size="1" name="affiliateProgrammArr[szCountry]" id="szCountry">
					<option value=""><?=t($t_base.'fields/select_country');?></option>
					<?php
						if(!empty($allCountriesArr))
						{
							foreach($allCountriesArr as $allCountriesArrs)
							{
								?><option value="<?=$allCountriesArrs['szCountryName']?>" <?php if($allCountriesArrs['szCountryName']==$_POST['affiliateProgrammArr']['szCountry']){?> selected <?php }?>><?=$allCountriesArrs['szCountryName']?></option>
								<?php
							}
						}
					?>
				</select>
			</span></span>
		</label>
		<label class="profile-fields  clearfix">
			<span class="field-name"><?=t($t_base.'fields/p_number');?></span>
			<span class="field-container"><span><input type="text"  name="affiliateProgrammArr[szPhoneNumber]" id="szPhoneNumber" value="<?=$_POST['affiliateProgrammArr']['szPhoneNumber']?>" onfocus="openTip('pno');" onblur="closeTip('pno');"/></span></span>
			<div class="field-alert">
				<div id="pno" style="display:none"><?=t($t_base.'messages/p_number_message');?></div>
			</div>
		</label>
				
		<br />
		<br />
		<p align="center">
		<input type="hidden" name="affiliateProgrammArr[szPhoneNumberUpdate]" id="szPhoneNumberUpdate" value="" />
		<a href="javascript:void(0)" class="button1" onclick="encode_string('szPhoneNumber','szPhoneNumberUpdate');signup_affiliate();" id="submit_affiliate_button"><span><?=t($t_base.'fields/submit');?></span></a> <a href="javascript:void(0)" class="button2" id="clear_affiliate_button" onclick="clear_affiliate_form();"><span><?=t($t_base.'fields/clear');?></span></a></p>
		</form>