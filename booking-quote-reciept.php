<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/booking_functions.php" );
  
$szMetaTitle = __BOOKING_QUOTE_THANKS_PAGE_META_TITLE__ ;
$szMetaKeywords = __BOOKING_QUOTE_THANKS_PAGE__META_KEYWORDS__;
$szMetaDescription = __BOOKING_QUOTE_THANKS_PAGE__META_DESCRIPTION__; 

require_once(__APP_PATH_LAYOUT__."/header_new.php");
$t_base = "BookingReceipt/";
$kBooking = new cBooking();  
$kConfig = new cConfig(); 
   
$idBooking = $_SESSION['quote_booking_id'] ;
 //$idBooking = 1022 ;
if((int)$_COOKIE['__BOOKING_QUOTE_REF_NUMBER_COOKIE__']>0 && $idBooking<=0)
{
    $idBooking = $_COOKIE['__BOOKING_QUOTE_REF_NUMBER_COOKIE__'] ;
}

if((int)$idBooking<=0)
{
    //ob_end_clean();
   // $redirect_url = __HOME_PAGE_URL__ ;
    //header("Location:".$redirect_url);
   // die;
}
//echo "ID: ".$idBooking ;
$postSearchAry = $kBooking->getBookingDetails($idBooking);  
 
$idLandingPage = $postSearchAry['idLandingPage'];
$iLanguage = getLanguageId();

if($idLandingPage<=0)
{ 
    //fetching default Landing page.
    $landingPageDataArys = $kExplain->getAllNewLandingPageData(false,false,true,$iLanguage);
    $landingPageDataAry = $landingPageDataArys[0];
    $idLandingPage = $landingPageDataAry['id'];
}  
$customerTestimonialAry = array();
$customerTestimonialAry = $kExplain->getAllTestimonials($idLandingPage,'DESC');
 
$iWeekDay = date('w');
if($iWeekDay==5 || $iWeekDay==6) //5: Friday, 6: Saturday
{
    $szOfferRecievedText = t($t_base.'fields/latest_by_details') ;
}
else
{
    $szOfferRecievedText = t($t_base.'fields/quote_offer_details_text_2') ; 
} 
$iVogaSearchAutomaticService = $postSearchAry['iVogaSearchAutomaticService'];
?>  
<div id="hsbody-2"> 
<script type="text/javascript">
jQuery().ready(function(){	 
    $('.sl-slider').bxSlider();
    setTimeout(function(){ $(".sl-slider-div").css('visibility','visible'); },'100');  
}); 
</script> 
    <div class="hsbody-right">
        <h2 class="thank-you-page-heading"><?=t($t_base.'fields/thanks_for_details');?>!</h2>
        <div class="booking-reciept-main-container clearfix" id="booking_details_container"> 
            <div class="oh even first icons">
                <p><?php echo t($t_base.'fields/quote_request');?> - <?php echo t($t_base.'fields/quote_request_details_text');?></p>
            </div>
            <div class="oh odd">
                <p><?php echo t($t_base.'fields/quote_contact')." - ".t($t_base.'fields/quote_contact_details_text'); ?></p>
            </div> 
            <div class="oh even">
                <p><?php echo t($t_base.'fields/quote_offer');?> - <?php echo t($t_base.'fields/quote_offer_details_text_1')." ".$postSearchAry['szEmail']." ".$szOfferRecievedText ; ?></p>
            </div>  
            <div class="oh even">
                <p><?php echo t($t_base.'fields/quote_accept');?> - <?php echo t($t_base.'fields/quote_accept_details_text'); ?></p>
            </div>
            <div class="oh even last">
                <p><?php echo t($t_base.'fields/quote_transportation');?> - <?php echo t($t_base.'fields/quote_transportation_details_text'); ?></p>
            </div> 
        </div> 

        <section id="testimonials" class="pages" style="padding:0px 0; ">  
            <div class="testimonials-container" id="testimonials-container"> 
                <ul class="sl-slider">	
                    <?php 
                        if(!empty($customerTestimonialAry)) 
                        {
                            foreach($customerTestimonialAry as $customerTestimonialArys)
                            {
                                ?>
                                <li>
                                    <div class="sl-slider-div" style="visibility:hidden;">
                                        <div class="deco"><img src="<?php echo $customerTestimonialArys['szImageURL']; ?>" alt="<?php echo $customerTestimonialArys['szImageTitle']?>" title="<?php echo $customerTestimonialArys['szImageDesc']; ?>"></div>
                                        <h3><?php echo $customerTestimonialArys['szHeading']; ?></h3>
                                        <blockquote><p><?php echo nl2br($customerTestimonialArys['szTextDesc']); ?></p><cite><?php echo $customerTestimonialArys['szTitleName']; ?></cite></blockquote>
                                    </div>
                                </li>
                                <?php
                            }
                        }
                    ?>
                </ul>  
            </div> 
        </section>
    </div> 
</div> 
<?php  

//$kWhsSearch = new cWHSSearch();
//$URL = __MAIN_SITE_HOME_PAGE_URL__."/ajax_misc.php?mode=CREATE_VOGA_AUTOMATED_BOOKINGS&idBooking=".$idBooking;
//$kWhsSearch->curl_get_file_contents($URL);

$_SESSION['quote_booking_id'] = '';
unset($_SESSION['quote_booking_id']);

$_COOKIE['__BOOKING_QUOTE_REF_NUMBER_COOKIE__']='';
unset($_COOKIE['__BOOKING_QUOTE_REF_NUMBER_COOKIE__']);
setcookie("__BOOKING_QUOTE_REF_NUMBER_COOKIE__", "", time()-10, "/",$_SERVER['HTTP_HOST'],true);
require_once(__APP_PATH_LAYOUT__."/footer_new.php");

if($iVogaSearchAutomaticService==1)
{
    ?>
    <script type="text/javascript">  
        $().ready(function(){ 
            calculate_voga_search('<?php echo $idBooking; ?>'); 
        }); 
    </script>
    <?php
}

 