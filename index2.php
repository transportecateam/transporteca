<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
$iNewLayoutDesign = 1;

$szURL = $_SERVER['REQUEST_URI'] ;

$kExplain= new cExplain();
$kWHSSearch=new cWHSSearch();
$szLanguage = sanitize_all_html_input($_REQUEST['lang']); 
$iLanguage = getLanguageId($szLanguage);
  
$landingPageDataArys = array();
$landingPageDataAry = array(); 
 
//fetching default Landing page.
$landingPageDataArys = $kExplain->getAllNewLandingPageData(false,false,true,$iLanguage);
$landingPageDataAry = $landingPageDataArys[0]; 

$idLandingPage = $landingPageDataAry['id'] ; 
$iPageSearchType = $landingPageDataAry['iPageSearchType'];
  
$_SESSION['Landing_Page_ID']=$idLandingPage;
$customerTestimonialAry = array();
$customerTestimonialAry = $kExplain->getAllTestimonials($idLandingPage);

$partnerLogoAry = array();
$partnerLogoAry = $kExplain->getAllPartnerLogo($idLandingPage);

if(!empty($landingPageDataAry['szMetaTitle']))
{
    $szMetaTitle = $landingPageDataAry['szMetaTitle'] ;
}
else 
{
    $szMetaTitle = __LANDING_PAGE_META_TITLE__ ;
}

if(!empty($landingPageDataAry['szMetaKeywords']))
{
    $szMetaKeywords = $landingPageDataAry['szMetaKeywords'] ;
}
else
{
    $szMetaKeywords = __LANDING_PAGE_META_KEYWORDS__;
}

if(!empty($landingPageDataAry['szMetaDescription']))
{
    $szMetaDescription = $landingPageDataAry['szMetaDescription'];
}
else
{
    $szMetaDescription = __LANDING_PAGE_META_DESCRIPTION__;
} 

$szDefaultFromField = $landingPageDataAry['szTextB2'];
$idDefaultLandingPage = $landingPageDataAry['id'];
$iFromLandingPage = 1;
$TestLandingPage = 1;
require_once(__APP_PATH_LAYOUT__."/header_new.php");
 
if($_SESSION['valid_confirmation']>0 || $_SESSION['display_booking_saved_popup']>0)
{
    echo display_confirmation_popup();
}  
$t_base = "home/homepage/"; 
$kConfig=new cConfig();
$number = 1;
$kBooking = new cBooking();

$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num'])); 
$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
  
unset($_SESSION['valid_confirmation']);
$_SESSION['__BOOKING_TIMESTAMP__'] = '';
unset($_SESSION['__BOOKING_TIMESTAMP__']);
  
$tst = '';
$arry['test'] = '';
$arry['test_1'] = '';
if (!array_filter($arry)) 
{ 
    //echo "All Empty IF";
}
else
{
   // echo "All Empty ELSE";
} 
/**
* $_REQUEST['err']=1 means no record found on service page
* $_REQUEST['err']=2 means force user to login for confirming multiuser access ;
* $_REQUEST['err']=3 means no record found for this user either lcl pricing is deleted or user email is in selected forwarders non-acceptance list
 * * $_REQUEST['err']=4 means quotation is expured
**/
if($_REQUEST['err']==1)
{
    $kBooking->addExpactedServiceData($idBooking);
    //$t_base = "SelectService/";
    $landing_page_url = __LANDING_PAGE_URL__ ;
    $idExpactedService = $kBooking->idExpactedService ;
    $postSearchAry = $kBooking->getBookingDetails($idBooking);
    ?>
    <script type="text/javascript">
            addPopupScrollClass('service_not_found');
    </script>
    <div id="service_not_found" style="display:block;">	
    <?php
    //echo display_service_not_found_html($t_base,$landing_page_url,$idBooking,$idExpactedService,$kBooking);
    echo display_service_not_found_seach_service_popup($postSearchAry,$idExpactedService,$kBooking);

    echo "</div>";
    ?>
    <script type="text/javascript">
        addPopupScrollClass('all_available_service');
    </script>
    <?php
}
else if(($_REQUEST['err']==2) && !empty($_SESSION['multi_user_key']))
{
    $redirect_url =__BASE_URL__."/inviteConfirmation.php?confirmationKey=".trim($_SESSION['multi_user_key']);
    //echo $_SESSION['multi_user_key'] ;
    //die;
    ?>
    <script type="text/javascript">
            ajaxLogin('<?=$redirect_url?>')
    </script>
    <?php
}
else if(($_REQUEST['err']==3))
{
    $t_base_mybooking = "Booking/MyBooking/";
    ?>
    <div id="change_price_div">
    <?php
    echo display_rate_expire_popup($t_base_mybooking,$idBooking);
    echo '</div>';
}
else if(($_REQUEST['err']==4))
{
    $t_base_mybooking = "Booking/MyBooking/";
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num'])); 

    ?>
    <script type="text/javascript">
        display_price_changed_popup_cargo_changed('<?=$_SESSION['booking_page_url']?>','<?=$szBookingRandomNum?>');
    </script>	
    <?php
    /**
    * we are updating idForwarder,idWarehouseFrom and idWarehouseTo to prevent Multiple tab problem.
    */
    $res_ary=array();
    $res_ary['iDoNotKnowCargo']= '0';

    if(!empty($res_ary))
    {
        foreach($res_ary as $key=>$value)
        {
            $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
        }
    }
    $update_query = rtrim($update_query,",");
    if($kBooking->updateDraftBooking($update_query,$idBooking))
    {
        //echo "SUCCESS";
    }
} 
else if($_REQUEST['err']==5)
{ 
    $t_base_mybooking = "Booking/MyBooking/"; 
    echo '<div id="change_price_div">' ; 
    echo display_quote_expire_popup($t_base_mybooking,$idBooking);
    echo '</div>';
}
// geting all service type 
$serviceTypeAry=$kConfig->getConfigurationData(__DBC_SCHEMATA_SERVICE_TYPE__,false,true);

$postSearchAry = array();
$cargoAry = array();   

if((int)$_REQUEST['idBooking']>0)
{
    //This block of code is called in repeat booking  
    $idBooking = (int)$_REQUEST['idBooking'] ;
    //echo $idBooking ;

    if($_SESSION['user_id']>0)
    {
        if($kBooking->isBookingBelongsToCustomer($idBooking,$_SESSION['user_id']))
        {
            $postSearchAry = $kBooking->getBookingDetails($idBooking);
            $postSearchAry['szBookingRandomNum'] = '';
            $cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);
            $booking_mode = 2;
        }
        else
        {
            header("Location:".__LANDING_PAGE_URL__);
            die;
        }
    }
    else
    {
        header("Location:".__LANDING_PAGE_URL__);
        die;
    }
}
else if($idBooking>0)
{
    $kBooking = new cBooking();
    $postSearchAry = $kBooking->getBookingDetails($idBooking);
    $cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);

    if(($postSearchAry['iPaymentProcess']==1) && ($booking_mode==2))
    {
        header("Location:".__LANDING_PAGE_URL__);
        exit;
    }
    else if(($_SESSION['user_id']>0) && (!$kBooking->isBookingBelongsToCustomer($idBooking,$_SESSION['user_id'])))
    {
        echo display_booking_invalid_user_popup($t_base_details);
        die;
    }
    else if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
    {
        $t_base = "home/homepage/";
        echo display_booking_already_paid($t_base,$idBooking);
        die;
    }
    else if($_REQUEST['err'] !=4) // That means user donot come from cargo changed price popup
    {
        /**
        * we are updating idForwarder,idWarehouseFrom and idWarehouseTo to prevent Multiple tab problem.
        *

        $res_ary=array();
        $res_ary['idForwarder']= '';
        $res_ary['idWarehouseFrom']= '';
        $res_ary['idWarehouseTo']= '';

        if(!empty($res_ary))
        {
            foreach($res_ary as $key=>$value)
            {
                $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
            }
        }
        $update_query = rtrim($update_query,",");
        if($kBooking->updateDraftBooking($update_query,$idBooking))
        {
                //echo "SUCCESS";
        }
         * 
         */
    }	
}
else
{
    $_SESSION['booking_register_shipper_id']='';
    $_SESSION['booking_register_shipper_id'] = '';
    unset($_SESSION['booking_register_shipper_id']);
    unset($_SESSION['booking_register_shipper_id']);
}

if(((int)$_SESSION['user_id']>0) && ($_REQUEST['err']<=0))
{
    //checkUserPrefferedCurrencyActive();
} 
 
$compareButtonDataAry = array();
$compareButtonDataAry = $kBooking->getCompareButtonDetails();
$iAvailableLclService = $kBooking->getAvailableLclService(); 
 
if($iLanguage==__LANGUAGE_ID_DANISH__)
{ 
    $iTotalUniqueIP = number_format((float)$compareButtonDataAry['iTotalUniqueIP'],0,'.','.');
    $iTotalNumRows = number_format((float)$compareButtonDataAry['iTotalNumRows'],0,'.','.'); 
    $iAvailableLclService = number_format((float)$iAvailableLclService,0,'.','.'); 
	
}
else
{
    $iTotalUniqueIP = number_format((float)$compareButtonDataAry['iTotalUniqueIP'],0,'.',',');
    $iTotalNumRows = number_format((float)$compareButtonDataAry['iTotalNumRows'],0,'.',','); 
    $iAvailableLclService = number_format((float)$iAvailableLclService,0,'.',','); 
} 
if($_REQUEST['err']==5)
{
    $postSearchAry['dtTimingDate'] = date('Y-m-d');
}      
if(!empty($_GET['search_params']))
{ 
    $searchParamsAry = explode("_",$_GET['search_params']);  
    if(!empty($searchParamsAry[0]))
    {
        $szOriginKey = trim($searchParamsAry[0]);
        if($szOriginKey=='IP')
        { 
            $szOriginAddressStr = getAddressDetailsFromIP();
           
            $postSearchAry['szOriginCountryStr'] = $szOriginAddressStr;
            $postSearchAry['szOriginCountry'] = $szOriginAddressStr; 
        }
        else
        { 
            $searchAidwordsAry = array();
            $searchAidwordsAry = $kExplain->getAllsearchAidWords(false,$szOriginKey); 
            if(!empty($searchAidwordsAry[0]))
            {
                $postSearchAry['szOriginCountryStr'] = $searchAidwordsAry[0]['szOriginStr'];
                $postSearchAry['szOriginCountry'] = $searchAidwordsAry[0]['szOriginStr']; 
            }
        }
    }
    if(!empty($searchParamsAry[2]))
    {
        $szDestinationKey = trim($searchParamsAry[2]);
        if($szDestinationKey=='IP')
        {
            $szDestinationAddressStr = getAddressDetailsFromIP();
            $postSearchAry['szDestinationCountryStr'] = $szDestinationAddressStr;
            $postSearchAry['szDestinationCountry'] = $szDestinationAddressStr; 
        }
        else
        { 
            $searchAidwordsAry = array();
            $searchAidwordsAry = $kExplain->getAllsearchAidWords(false,$szDestinationKey);  

            if(!empty($searchAidwordsAry[0]))
            {
                $postSearchAry['szDestinationCountryStr'] = $searchAidwordsAry[0]['szOriginStr'];
                $postSearchAry['szDestinationCountry'] = $searchAidwordsAry[0]['szOriginStr'];
            }
        }
    }
}
 
$iSearchMiniVesion = false;
if($iPageSearchType==__SEARCH_TYPE_PARCELS__)
{
    $iSearchMiniVesion = 3;
}
else if($iPageSearchType==__SEARCH_TYPE_PALLETS__)
{
    $iSearchMiniVesion = 5;
}  
else if($iPageSearchType==__SEARCH_TYPE_VOGUE__)
{
    $iSearchMiniVesion = 7;
}
//$iSearchMiniVesion = 5;
$szSearchFormContainerOpenTag = '';
$szSearchFormContainerCloseTag = '';

if($iSearchMiniVesion>0)
{
    $szVersionClass = "version-".$iSearchMiniVesion;
    $szSearchFormContainerOpenTag = '<div id="search_mini_global_conatiner_'.$iSearchMiniVesion.'"><div id="transporteca_search_form_container" class="search-mini-container '.$szVersionClass.'"><div class="search-mini-ifram-container">';
    $szSearchFormContainerCloseTag = '</div></div></div>';
}  
if($iSearchMiniVesion==3)
{
    $szSectionId = 'v3-search-freight';
}
else
{
   $szSectionId = 'search-freight';
} 
?>  
<link rel="stylesheet" type="text/css" href="<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/fancy-box-ie.css" />
<!--[if lt IE 9]>
<link rel="stylesheet" type="text/css" href="<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/fancy-box-ie.css" />
<![endif]--> 
<div id="Transportation_pop" style="display:none;"></div> 
<?php
    if($iSearchMiniVesion==7)
    {
         $szContactMe = $landingPageDataAry['szContactMe'];
         $szMiddleText = $landingPageDataAry['szMiddleText'];
         $postSearchAry['idSearchMini'] = $landingPageDataAry['idSearchMini'];
        ?>
        <section id="v7-search-freight" data-type="background" data-speed="10" class="pages searchmini-main-container" style="background-image:url('<?php echo $landingPageDataAry['szTextA1']; ?>');">    
            <article>
               <h1><?php echo $landingPageDataAry['szTextB']; ?></h1>
               <?php if(!empty($landingPageDataAry['szTextB3'])){ ?>
                   <div class="search-description-box">
                       <?php echo $landingPageDataAry['szTextB3']; ?>
                   </div>
               <?php } ?>               
               <div id="transporteca_search_form_container">
                   <?php 
                       echo $szSearchFormContainerOpenTag; 
                       echo display_new_search_form($kBooking,$postSearchAry,false,false,false,$szDefaultFromField,false,true,$idDefaultLandingPage,false,$iSearchMiniVesion,$szContactMe,$szMiddleText); 
                       echo $szSearchFormContainerCloseTag; 
                   ?>
               </div>
           </article>
       </section> 
        <?php
    }
    else
    {
        ?>
        <section id="<?php echo $szSectionId; ?>" data-type="background" data-speed="10" class="pages searchmini-main-container" style="background-image:url('<?php echo $landingPageDataAry['szTextA1']; ?>');">    
            <article>
               <h1><?php echo $landingPageDataAry['szTextB']; ?></h1>
               <div class="checkbox-mobile">
                    <span class="checkbox-image-mobile">                       
                        <?php echo t($t_base.'fields/we_have_best_price_guaranteed'); ?>
                    </span>
                    <span class="checkbox-image-mobile">                      
                        <?php echo t($t_base.'fields/you_get_no_unexpected_costs'); ?>
                    </span>
                </div>
               <div id="transporteca_search_form_container">
                   <?php 
                        if($iSearchMiniVesion==3)
                        {
                            ?>
                            <div class="location-pointer"><img src="<?php echo __BASE_STORE_IMAGE_URL__?>/search-from-to-arrow-transparent.png" width="219px" alt=""></div>
                            <?php
                        }
                        else if($iSearchMiniVesion==5)
                        {
                            ?>
                            <div class="location-pointer-v5"><img src="<?php echo __BASE_STORE_IMAGE_URL__?>/pallet-swush-image.png" alt=""></div>
                            <?php
                        } 
                        else
                        {
                            ?>
                            <div class="location-pointer-v1"><img src="<?php echo __BASE_STORE_IMAGE_URL__?>/search-from-to-arrow.png" width="219px" alt=""></div>
                            <?php
                        }
                        echo $szSearchFormContainerOpenTag; 
                        echo display_new_search_form($kBooking,$postSearchAry,false,false,false,$szDefaultFromField,false,true,$idDefaultLandingPage,false,$iSearchMiniVesion); 
                        echo $szSearchFormContainerCloseTag; 
                   ?>
               </div>
               <?php if(!empty($landingPageDataAry['szTextB3'])){ ?>
                   <div class="search-description-box">
                       <?php echo $landingPageDataAry['szTextB3']; ?>
                   </div>
               <?php } ?>
           </article>
       </section>   
        <?php
    }
?> 
<section class="what-is-in pages">
    <h2><?php echo $landingPageDataAry['szTextC']; ?></h2>
    <ul class="clearfix">
        <li class="quick">
            <div class="icon" style="background-image:url('<?php echo $landingPageDataAry['szTextD1']; ?>');">&nbsp;</div>
            <h3><?php echo $landingPageDataAry['szTextE1']; ?></h3>
            <p><?php echo $landingPageDataAry['szTextE2']; ?></p>
        </li>
        <li class="all-price">
            <div class="icon" style="background-image:url('<?php echo $landingPageDataAry['szTextD4']; ?>');">&nbsp;</div>
            <h3><?php echo $landingPageDataAry['szTextE3']; ?></h3>
            <p><?php echo $landingPageDataAry['szTextE4']; ?></p>
        </li>
        <li class="on-time">
            <div class="icon" style="background-image:url('<?php echo $landingPageDataAry['szTextD7']; ?>');">&nbsp;</div>
            <h3><?php echo $landingPageDataAry['szTextE5']; ?></h3>
            <p><?php echo $landingPageDataAry['szTextE6']; ?></p>
        </li>
    </ul>
</section>
<section class="how-does-work pages">
    <h2><?php echo $landingPageDataAry['szTextF1']; ?></h2>
    <ul class="clearfix">
        <li class="quick">
            <div class="icon" style="background:url('<?php echo $landingPageDataAry['szTextG1']; ?>')no-repeat 0 0;border:0;height:120px;width:120px;">&nbsp;</div>
            <p><?php echo $landingPageDataAry['szTextH1']; ?></p>
        </li>
        <li class="all-price">
            <div class="icon" style="background:url('<?php echo $landingPageDataAry['szTextG4']; ?>')no-repeat 0 0;border:0;height:120px;width:120px;">&nbsp;</div>
            <p><?php echo $landingPageDataAry['szTextH2']; ?></p>
        </li>
        <li class="on-time">
            <div class="icon" style="background:url('<?php echo $landingPageDataAry['szTextG7']; ?>')no-repeat 0 0;border:0;height:120px;width:120px;">&nbsp;</div>
            <p><?php echo $landingPageDataAry['szTextH3']; ?></p>
        </li>
    </ul>
</section>
<section id="partners" data-type="background" data-speed="15" class="pages" style='background-image:url("<?php echo $landingPageDataAry['szTextI1']; ?>");'>
    <article>
        <h2><?php echo $landingPageDataAry['szTextI4']; ?></h2>
        <div class="partners-number clearfix">
            <div class="professionals"><span><?php echo $iTotalUniqueIP; ?></span><br><?php echo $landingPageDataAry['szTextI5']; ?></div>
            <div class="companies"><span><?php echo $iTotalNumRows;?></span><br><?php echo $landingPageDataAry['szTextI6']; ?></div>
            <div class="routes"><span><?php echo $iAvailableLclService ; ?></span><br><?php echo $landingPageDataAry['szTextI7']; ?></div>
        </div>
            <ul class="clearfix">
            <?php
                if(!empty($partnerLogoAry))
                { 
                    $i=1;
                    foreach($partnerLogoAry as $partnerLogoArys)
                    {
                        $idImage="img_".$i;
                        ?>
                        <li id="<?=$idImage?>">
                            <img class="normal" src="<?php echo $partnerLogoArys['szLogoImageUrl']; ?>" alt="<?php echo $partnerLogoArys['szImageDesc']; ?>">
                            <img class="hover" src="<?php echo $partnerLogoArys['szWhiteLogoImageURL']; ?>" alt="<?php echo $partnerLogoArys['szImageDesc']; ?>">
                            <div><div><?php echo $partnerLogoArys['szToolTipText']; ?></div></div>
                        </li>
                        <?php
                        ++$i;
                    }
                }
            ?>
            </ul>
    </article>
</section> 
 
<section id="testimonials" class="pages">
	<article>
		<h2><?php echo $landingPageDataAry['szTextJ1']; ?></h2>
		<div class="testimonials-container" id="testimonials-container"> 
			<ul class="sl-slider">	
				<?php 
					if(!empty($customerTestimonialAry)) 
					{
						foreach($customerTestimonialAry as $customerTestimonialArys)
						{
							?>
							<li>
								<div class="sl-slider-div" style="visibility:hidden;">
									<div class="deco"><img src="<?php echo $customerTestimonialArys['szImageURL']; ?>" alt="<?php echo $customerTestimonialArys['szImageTitle']?>" title="<?php echo $customerTestimonialArys['szImageDesc']; ?>"></div>
									<h3><?php echo $customerTestimonialArys['szHeading']; ?></h3>
									<blockquote><p><?php echo nl2br($customerTestimonialArys['szTextDesc']); ?></p><cite><?php echo $customerTestimonialArys['szTitleName']; ?></cite></blockquote>
								</div>
							</li>
							<?php
						}
					}
				?>
			</ul>  
		</div>
	</article>
</section> 
<div style="display:none;">
    <div id="video_container_div" style="width:1020px;height:580px;overflow:hidden;">	
        <?php echo $landingPageDataAry['szTextL4']; ?>
    </div>
</div>
<?php if($landingPageDataAry['iDisplaySeoText']==1){  
    $bgColor='';
    $bgColor="background-color:".$landingPageDataAry['szBackgroundColor'].""; 
    ?> 
    <div class="body-color <?php if($landingPageDataAry['szBackgroundColor']=='#fff' || $landingPageDataAry['szBackgroundColor']=='' || $landingPageDataAry['szBackgroundColor']=='#ffffff'){?> white<?php }?>" style="<?php echo $bgColor;?>">
        <div id="hsbody"> 
            <h2 align="center" class="heading"><?php echo $landingPageDataAry['szHeading']?></h2> 
            <?php echo str_replace("&#39;","'",$landingPageDataAry['szSeoDescription']);?>
        </div>
    </div>
    <?php	
}?>
<div id="all_available_service" style="display:none;"></div>
<input type="hidden" name="iLandingPageFlag" id="iLandingPageFlag" value="1">

<script type="text/javascript">
    jQuery().ready(function(){ 
        $('#video').click(function() {
            jQuery.fancybox(
            { 
                'href' : "#video_container_div"
            }); 
        });	
        $('.sl-slider').bxSlider();
        setTimeout(function(){ $(".sl-slider-div").css('visibility','visible'); },'500'); 
    }); 
</script>

<?php
echo html_form_random_booking_number(__BOOKING_DETAILS_PAGE_URL__,$szBookingRandomNum);
require_once(__APP_PATH_LAYOUT__."/footer_new.php");
?>