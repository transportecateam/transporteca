<?
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
$display_abandon_message = true;
$szMetaTitle = __BOOKING_DETAILS_META_TITLE__;
$szMetaKeywords = __BOOKING_DETAILS_META_KEYWORDS__;
$szMetaDescription = __BOOKING_DETAILS_META_DESCRIPTION__;

$iDonotIncludeHiddenSearchHeader = 1;
require_once(__APP_PATH_LAYOUT__."/header_new.php");
$t_base = "BookingDetails/";
$t_base_user = "Users/AccountPage/";
$t_base_error = "Error";
$kBooking = new cBooking();
$kConfig = new cConfig();
$kWHSSearch=new cWHSSearch();

$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num'])); 

//redirectting to new booking pages 
ob_end_clean();
header("Location:".__NEW_SERVICES_PAGE_URL__.'/'.$szBookingRandomNum.'/');
die;

$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
$postSearchAry = $kBooking->getBookingDetails($idBooking);

$userSessionData = array();
$userSessionData = get_user_session_data();
$idUserSession = $userSessionData['idUser'];

if($_SESSION['booking_id']==$idBooking)
{
	$kBooking->updatePaymentFlag($idBooking,0); 
}
 
if((int)$_REQUEST['idBooking']>0)
{
	$idBooking = (int)$_REQUEST['idBooking'] ;
	$idBooking = $idBooking ;

	$kBooking = new cBooking();
	if($idUserSession>0)
	{
		if($kBooking->isBookingBelongsToCustomer($idBooking,$idUserSession))
		{
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			$cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);
			$booking_mode = 2;
		}
		else
		{
			header("Location:".__NEW_SEARCH_PAGE_URL__);
			die;
		}
	}
}
else if((int)$idBooking>0)
{
	$postSearchAry = $kBooking->getBookingDetails($idBooking);
	$cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);	
	
	if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
	{
		echo display_booking_already_paid($t_base,$idBooking);
		die;
	}
}
else
{
	$redirect_url = __NEW_SEARCH_PAGE_URL__ ;
	?>
	<div id="change_price_div">
	<?
	display_booking_notification($redirect_url);
	echo "</div>";
} 
if(($postSearchAry['idForwarder']<=0) || ($postSearchAry['idWarehouseTo']<=0) || ($postSearchAry['idWarehouseFrom']<=0) || ($postSearchAry['iPaymentProcess']==1))
{
	$redirect_url = __NEW_SERVICES_PAGE_URL__.'/'.$szBookingRandomNum.'/';
	header("Location:".$redirect_url);
	exit; 
}

if(((int)$idUserSession>0) && ($_REQUEST['err']<=0))
{
	checkUserPrefferedCurrencyActive();
}

/**
* This function recalculate pricing after a perticular interval set in constants.
* 
**/ 
$ret_url = __NEW_BOOKING_DETAILS_PAGE_URL__.'/'.$szBookingRandomNum.'/'; 
echo check_price_30_minute($postSearchAry,$ret_url,false,$idBooking);

$kUser = new cUser();  
$kRegisterShipCon = new cRegisterShipCon();

?>
<div id="hsbody-2">
<div id="all_available_service" style="display:none;"></div>
	<div id="customs_clearance_pop_right" class="help-pop right"></div> 
	<div id="customs_clearance_pop" class="help-pop"></div>
	<div id="change_price_div" style="display:none"></div> 
	
	<div class="hsbody-2-right"> 
		<?php
		/**
		* display_booikng_bread_crum is used to display bread crum, it accepts two param first one is step and second is $t_base
		*/ 
		echo display_booikng_bread_crum_new(1,$t_base);		
	   ?> 
		<br />
		<br />
		<div id="booking_details_container" class="clearfix">
		<?php
			echo display_booking_details($postSearchAry,$kRegisterShipCon);
		?>
		</div>
	</div>
</div>
<?php
	echo html_form_random_booking_number(__BOOKING_CONFIRMATION_PAGE_URL__,$_REQUEST['booking_random_num']);
?> 
<?php
  require_once(__APP_PATH_LAYOUT__."/footer_new.php");
?>				