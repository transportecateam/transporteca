<?php
/**
 * Customer Login
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );
require_once (__APP_PATH__ ."/inc/classes/pagination.class.php");
	
$t_base = "home/homepage/";
$t_base_landing_page = "LandingPage/";
$mode = sanitize_all_html_input(trim($_REQUEST['mode']));
$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_key']));
$kConfig=new cConfig();
$kWHSSearch = new cWHSSearch();
$kBooking = new cBooking();

if($mode=='SERVICE_TYPE')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);	
	$szUpdateServiceFlag = sanitize_all_html_input(trim($_REQUEST['update_srvice_flag']));
	
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		
		if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
		{
                    echo "POPUP||||" ;
                    $t_base_booking_details = "BookingDetails/";
                    echo display_booking_already_paid($t_base_booking_details,$idBooking);
                    die;
		}
	
		$res_ary=array();
		$res_ary['idServiceType']= sanitize_all_html_input(trim($_REQUEST['type_id']));
		
		if((int)$postSearchAry['iBookingStep']<3)
		{
			$res_ary['iBookingStep'] = 3 ;
		}
		
		if(!empty($res_ary))
		{
			$update_query = '';
			foreach($res_ary as $key=>$value)
			{
				$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
			}
		}
		
		$idServiceType = sanitize_all_html_input(trim($_REQUEST['type_id']));
		$update_query = rtrim($update_query,",");
		if($kBooking->updateDraftBooking($update_query,$idBooking))
		{
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			if($szUpdateServiceFlag == 'UPDATE_SERVICE') // This block is called when haulage not exist for perticular city.
			{
				$szOriginDestination = sanitize_all_html_input(trim($_REQUEST['szOriginDestination']));
				$szCityName = sanitize_all_html_input(trim($_REQUEST['szCityName']));
				
				if(!empty($szCityName) && $szOriginDestination=='ORIGIN')
				{
					$destinationPostCodeAry=array();
					$destinationPostCodeAry = $kConfig->getPostCodeDetails_requirement($postSearchAry['idOriginCountry'],$szCityName);
					
					$res_ary = array();
					$res_ary['fOriginLatitude']= round((float)$destinationPostCodeAry['szLatitude'],5); 
					$res_ary['fOriginLongitude']= round((float)$destinationPostCodeAry['szLongitude'],5);
					
					$res_ary['szOriginCity']= $szCityName ;
					$res_ary['szOriginPostCode']= '' ;

					$kBooking->updateBooking($res_ary,$idBooking);
					
					$postSearchAry = $kBooking->getBookingDetails($idBooking);
				}
				else if(!empty($szCityName) && $szOriginDestination=='DESTINATION')
				{
					$destinationPostCodeAry=array();
					$destinationPostCodeAry = $kConfig->getPostCodeDetails_requirement($postSearchAry['idDestinationCountry'],$szCityName);
					
					$res_ary = array();
					$res_ary['fDestinationLatitude']= round((float)$destinationPostCodeAry['szLatitude'],5); 
					$res_ary['fDestinationLongitude']= round((float)$destinationPostCodeAry['szLongitude'],5);
					
					$res_ary['szDestinationCity']= $szCityName ;
					$res_ary['szDestinationPostCode']= '' ;
					
					if((int)$postSearchAry['iBookingStep']<4)
					{
						$res_ary['iBookingStep'] = 4 ;
					}
					
					$kBooking->updateBooking($res_ary,$idBooking);
					
					$postSearchAry = $kBooking->getBookingDetails($idBooking);
				}
				
				echo "SUCCESS||||";
				echo display_service_image($postSearchAry,$idServiceType,$szBookingRandomNum);
				echo "||||";
				echo city_postcode_box($postSearchAry);
				echo "||||";
				echo display_timing_box_active($postSearchAry);
				echo "||||";
				echo display_custom_clearance_box($postSearchAry);
			}
			else if($szUpdateServiceFlag =='DON_NOT_KNOW_POPUP') // This block is called when we clicked on 'I don't know' link from city post code box.
			{
				$postSearchAry = $kBooking->getBookingDetails($idBooking);
				$idServiceType = $postSearchAry['idServiceType'];
				
				echo "SUCCESS||||";
				echo display_service_image($postSearchAry,$idServiceType,$szBookingRandomNum);
				echo "||||";
				echo city_postcode_active_box($postSearchAry);
				echo "||||";
				echo display_custom_clearance_box($postSearchAry);
			}
			else
			{
				echo "SUCCESS||||";
				echo display_service_image($postSearchAry,$idServiceType,$szBookingRandomNum);
				echo "||||";
				echo city_postcode_active_box($postSearchAry);
				echo "||||";
				echo display_custom_clearance_box($postSearchAry);
			}
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking ID. ";
	}
}
else if($mode=='VALIDATE_CITY_POSTCODE')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);	
	
	if($idBooking>0)
	{
		$kConfig = new cConfig();
		$data=array();
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		$szCityPostcode = sanitize_all_html_input(trim($_REQUEST['szCityPostcode']));
		$type = sanitize_all_html_input(trim($_REQUEST['type']));
		
		
		if($type == 'ORIGIN')
		{				
			$data['idOriginCountry'] = $postSearchAry['idOriginCountry'];
			$data['szOriginCity'] = $szCityPostcode;
			//$data['szOriginPostCode'] = $szCityPostcode;
		}
		else if($type == 'DESTINATION')
		{
			$data['idDestinationCountry'] = $postSearchAry['idDestinationCountry'];
			$data['szDestinationCity'] = $szCityPostcode;
			//$data['szDestinationPostCode'] = $szCityPostcode;
		}
		
		$data['idServiceType'] = $postSearchAry['idServiceType'];
		$kConfig->validate_city_postcode_requirement($data,$type);
		if(!empty($kConfig->arErrorMessages))
		{
			echo "ERROR_POPUP ||||ORIGIN||||" ;
			if($kConfig->iOriginError)
			{
				echo t($t_base_landing_page.'title/not_recognize');
			}
			echo "||||DESTINATION||||";
			if($kConfig->iDestinationError)
			{
				echo t($t_base_landing_page.'title/not_recognize');
			}
		}
	}
}
else if($mode=='CITY_POSTCODE')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);	
	if($idBooking>0)
	{
		$kConfig = new cConfig();
		$data=array();
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		
		$szOriginCityPostcode = sanitize_all_html_input(trim($_REQUEST['szOriginCityPostcode']));
		$szDestinationCityPostcode = sanitize_all_html_input(trim($_REQUEST['szDestinationCityPostcode']));
		if(!empty($szOriginCityPostcode))
		{
			$szOriginCityPostcodeAry = explode("/",$szOriginCityPostcode);
			$szOriginCity = $szOriginCityPostcodeAry[0];
			$szOriginPostcode = $szOriginCityPostcodeAry[1];
		}
		else
		{
			$kConfig->addError('szOriginCityPostcode',t($t_base.'errors/enter_shipper_city_or_postcode'));
		}
		
		if(!empty($szDestinationCityPostcode))
		{
			$szDestinationCityPostcodeAry = explode("/",$szDestinationCityPostcode);
			$szDestinationCity = $szDestinationCityPostcodeAry[0];
			$szDestinationPostcode = $szDestinationCityPostcodeAry[1];
		}
		else
		{
			$kConfig->addError('szOriginCityPostcode',t($t_base.'errors/enter_shipper_city_or_postcode'));
		}
		
		$data['idOriginCountry'] = $postSearchAry['idOriginCountry'];
		$data['szOriginCity'] = $szOriginCity;
		$data['szOriginPostCode'] = $szOriginPostcode;
		
		$data['idDestinationCountry'] = $postSearchAry['idDestinationCountry'];
		$data['szDestinationCity'] = $szDestinationCity;
		$data['szDestinationPostCode'] = $szDestinationPostcode;
		$data['idServiceType'] = $postSearchAry['idServiceType'];
			
		$kConfig->validate_city_postcode_requirement($data);
		if(!empty($kConfig->arErrorMessages))
		{
			echo "ERROR_POPUP ||||ORIGIN||||" ;
			if($kConfig->iOriginError)
			{
				echo t($t_base_landing_page.'title/not_recognize');
			}
			echo "||||DESTINATION||||";
			if($kConfig->iDestinationError)
			{
				echo t($t_base_landing_page.'title/not_recognize');
			}
			die;
		}
		
		$idServiceType = $postSearchAry['idServiceType'];
		
		// checking all the things at origin side .
		if(($idServiceType == __SERVICE_TYPE_DTD__) || ($idServiceType == __SERVICE_TYPE_DTW__) || ($idServiceType == __SERVICE_TYPE_DTP__ ))
		{
			if(!empty($kConfig->multiRegionFromCityAry))
			{
				echo "MULTIREGIO||||";
				$kBooking=new cBooking();
				$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
				$szOriginCity = $szOriginCity;
				$szDestinationCity = $szDestinationCity;
				echo display_multiregion_popup($postSearchAry, $kConfig->multiRegionFromCityAry, 'ORIGIN', $szOriginCity, $iCheckDestination,$szDestinationCity);
				die;
			}
			else if(!empty($kConfig->multiEmaptyPostcodeFromCityAry))
			{
				echo "MULTI_EMPTY_POSTCODE||||" ;
				$postSearchAry = array();
				$postSearchAry = $kBooking->getBookingDetails($idBooking);
				$szOriginCity = $szOriginCity;
				$szDestinationCity = $szDestinationCity;
				echo display_empty_postcode_popup($postSearchAry, $kConfig->multiEmaptyPostcodeFromCityAry, 'ORIGIN', $szOriginCity, $iCheckDestination,$szDestinationCity);
				die;
			}
			else if(!empty($kConfig->partialEmaptyPostcodeFromCityAry))
			{
				echo "MULTI_PARTIAL_EMPTY_POSTCODE||||" ;
				$postSearchAry = array();
				$postSearchAry = $kBooking->getBookingDetails($idBooking);
				$szOriginCity = $szOriginCity;
				$szDestinationCity = $szDestinationCity;
				echo display_partial_empty_postcode_popup($postSearchAry, $kConfig->partialEmaptyPostcodeFromCityAry, 'ORIGIN', $szOriginCity, $iCheckDestination,$szDestinationCity);
				die;
			}
			else
			{
				$iOriginFlag = true ;
			}
		}
		else
		{
			if(!empty($kConfig->multiRegionFromCityAry))
			{
				echo "MULTIREGIO||||";
				$kBooking=new cBooking();
				$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
				$szOriginCity = $szOriginCity;
				$szDestinationCity = $szDestinationCity;
				echo display_multiregion_popup($postSearchAry, $kConfig->multiRegionFromCityAry, 'ORIGIN', $szOriginCity, $iCheckDestination,$szDestinationCity);
				die;
			}
			else
			{
				$iOriginFlag = true ;
			}
		}
		
		if($iOriginFlag)
		{
			$originPostCodeAry=array();
			$originPostCodeAry = $kConfig->getPostCodeDetails_requirement($kConfig->szOriginCountry,$kConfig->szOriginCity);
							
			$res_ary=array();
			$res_ary['fOriginLatitude']= round((float)$originPostCodeAry['szLatitude'],5); 
			$res_ary['fOriginLongitude']= round((float)$originPostCodeAry['szLongitude'],5);
			$res_ary['idOriginPostCode']= $kConfig->idOriginPostCode ;
			$res_ary['szOriginCity'] = $kConfig->szOriginCity ;
			$res_ary['szOriginPostCode']= strtoupper($kConfig->szOriginPostCode);
			
			$kBooking->updateBooking($res_ary,$idBooking);
						
			$postSearchAry = array();
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			
			if(($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_DTW__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_DTP__))
			{
				if($kWHSSearch->isHaulageExists($postSearchAry,1))
				{
					echo "POPUP||||";
					$szDestinationCity = $szDestinationCity;
					echo display_orgin_haulage_not_exist_popup($postSearchAry,'VALIDATE_POSTCODE_AFTER_HAULAGE_POPUP',$kConfig->szOriginCityPostcode,$szDestinationCity);
					die;
				}
			}
		}
		
		// checking all the things at destination side .
		if(($idServiceType == __SERVICE_TYPE_DTD__) || ($idServiceType == __SERVICE_TYPE_WTD__) || ($idServiceType == __SERVICE_TYPE_PTD__ ))
		{
			if(!empty($kConfig->multiRegionToCityAry))
			{
				echo "MULTIREGIO||||";
				$kBooking=new cBooking();
				$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
				$szOriginCity = $szOriginCity;
				$szDestinationCity = $szDestinationCity;
				$postSearchAry = $kBooking->getBookingDetails($idBooking);
				echo display_multiregion_popup($postSearchAry, $kConfig->multiRegionToCityAry, 'DESTINATION', $szDestinationCity);
				die;
			}
			else if(!empty($kConfig->multiEmaptyPostcodeToCityAry))
			{
				echo "MULTI_EMPTY_POSTCODE||||" ;
				$postSearchAry = array();
				$postSearchAry = $kBooking->getBookingDetails($idBooking);
				$szOriginCity = $szOriginCity;
				$szDestinationCity = $szDestinationCity;
				echo display_empty_postcode_popup($postSearchAry,$kConfig->multiEmaptyPostcodeToCityAry, 'DESTINATION', $szDestinationCity);
				die;
			}
			else if(!empty($kConfig->partialEmaptyPostcodeToCityAry))
			{
				echo "MULTI_PARTIAL_EMPTY_POSTCODE||||" ;
				$postSearchAry = array();
				$postSearchAry = $kBooking->getBookingDetails($idBooking);
				$szOriginCity = $szOriginCity;
				$szDestinationCity = $szDestinationCity;
				echo display_partial_empty_postcode_popup($postSearchAry,$kConfig->partialEmaptyPostcodeToCityAry, 'DESTINATION', $szDestinationCity);
				die;
			}
			else
			{
				$iDestinationFlag = true ;
			}
		}
		else
		{
			if(!empty($kConfig->multiRegionToCityAry))
			{
				echo "MULTIREGIO||||";
				$kBooking=new cBooking();
				$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
				$postSearchAry = array();
				$postSearchAry = $kBooking->getBookingDetails($idBooking);
				$szOriginCity = $szOriginCity;
				$szDestinationCity = $szDestinationCity;
				echo display_multiregion_popup($postSearchAry, $kConfig->multiRegionToCityAry, 'DESTINATION', $szDestinationCity);
				die;
			}
			else
			{
				$iDestinationFlag = true ;
			}
		}
		
		if($iDestinationFlag)
		{
			$destinationPostCodeAry=array();
			$destinationPostCodeAry = $kConfig->getPostCodeDetails_requirement($kConfig->szDestinationCountry,$kConfig->szDestinationCity);
			
			$res_ary = array();
			$res_ary['szDestinationCity']= $kConfig->szDestinationCity ;
			$res_ary['szDestinationPostCode']= strtoupper($kConfig->szDestinationPostCode);
			$res_ary['idDestinationPostCode']= $kConfig->idDestinationPostCode ;
			$res_ary['fDestinationLatitude']= round((float)$destinationPostCodeAry['szLatitude'],5); 
			$res_ary['fDestinationLongitude']= round((float)$destinationPostCodeAry['szLongitude'],5);
			if((int)$postSearchAry['iBookingStep']<4)
			{
				$res_ary['iBookingStep'] = 4 ;
			}
			
			$kBooking->updateBooking($res_ary,$idBooking);
			$postSearchAry = array();
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			
			$kWHSSearch = new cWHSSearch();
		
			if(($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_WTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_PTD__))
			{
				if($kWHSSearch->isHaulageExists($postSearchAry,2))
				{
					echo "POPUP||||";
					echo display_destination_haulage_not_exist_popup($postSearchAry);
					die;
				}
				else
				{
					echo "SUCCESS||||";
					echo display_timing_box_active($postSearchAry);
					echo "||||";
					echo city_postcode_active_box($postSearchAry,true);
					echo "||||";
					echo display_custom_clearance_box($postSearchAry);
					die;
				}
			}
			else
			{
				echo "SUCCESS||||";
				echo display_timing_box_active($postSearchAry);
				echo "||||";
				echo city_postcode_active_box($postSearchAry,true);
				echo "||||";
				echo display_custom_clearance_box($postSearchAry);
				die;
			}
		}
	}
}
else if(!empty($_POST['multiregionCityAry']))
{
	$res_ary=array();
	
	$error_message = "";	
	if(((int)$_POST['multiregionCityAry']['iRegionFrom']>0) && ((int)$_POST['multiregionCityAry']['szRegionFrom']<=0))
	{
		$error_message .= "<li> Please select exact origin </li>";
	}
	if(((int)$_POST['multiregionCityAry']['iRegionTo']>0) && ((int)$_POST['multiregionCityAry']['szRegionTo']<=0))
	{
		$error_message .= "<li> Please select exact destination </li>";
	}
	if(!empty($error_message))
	{
		echo "ERROR||||".$error_message;
	}
	else
	{
		$szCityPostcode = trim($_POST['multiregionCityAry']['szCityPostcode']);
		$szDestinationCity = trim($_POST['multiregionCityAry']['szDestinationCity']);
		$szBookingRandomNum = trim($_POST['multiregionCityAry']['szBookingRandomNum']);
		$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
		
		if((int)$_POST['multiregionCityAry']['szRegionFrom']>0)
		{
			$idFromPostcode = sanitize_all_html_input(trim($_POST['multiregionCityAry']['szRegionFrom'])) ;
			$kConfig->loadPostCode($idFromPostcode);
			$postSearchAry = array();
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			$idServiceType = $postSearchAry['idServiceType'];
			$idOriginCountry = $postSearchAry['idOriginCountry'];
			$szDestinationCity = trim($_POST['multiregionCityAry']['szDestinationCity']);
			$szOriginRegion1 = $kConfig->szRegion1 ;
			
			if(($idServiceType == __SERVICE_TYPE_DTD__) || ($idServiceType == __SERVICE_TYPE_DTW__) || ($idServiceType == __SERVICE_TYPE_DTP__ ))
			{				
				if($kConfig->comparePostcode($szCityPostcode,$kConfig->szPostCode))
				{
					$szOriginCityPostCode = $kConfig->szPostCode;
				}
				else
				{
					$szOriginCityPostCode = $kConfig->szPostcodeCity;
				}
				
				$fromCityAry = $kConfig->check_city_country_requirement($szOriginCityPostCode,$idOriginCountry,false,false,false,$szOriginRegion1);
				if(($kConfig->emptyPostcodeCtr == 1 || $kConfig->partialEmptyPostcodeCtr==1) && !empty($fromCityAry))
				{
					$res_ary = array();
					$res_ary['idOriginPostCode'] = $kConfig->idEmptyPostCode;
					
					if($kConfig->comparePostcode($kConfig->szOriginCity,$kConfig->szPostCode))
					{
						$res_ary['szOriginPostCode'] = strtoupper($kConfig->szPostCode);
						$res_ary['szOriginCity'] = $kConfig->szCity;
					}
					else
					{
						$res_ary['szOriginPostCode'] = strtoupper($kConfig->szPostCode);
						$res_ary['szOriginCity'] = $kConfig->szCity;
					}					
					$res_ary['fOriginLatitude'] = $kConfig->szLatitude;
					$res_ary['fOriginLongitude'] = $kConfig->szLongitude;
					$iCheckDestination = 1;
				}
				else if($kConfig->emptyPostcodeCtr > 1 && !empty($fromCityAry))
				{
					echo "MULTI_EMPTY_POSTCODE||||";
					$szOriginCity = $szOriginCityPostCode;
					echo display_empty_postcode_popup($postSearchAry, $fromCityAry, 'ORIGIN', $szOriginCity, $iCheckDestination,$szDestinationCity);
					die;
				}
				else if($kConfig->partialEmptyPostcodeCtr > 1 && !empty($fromCityAry))
				{
					echo "MULTI_PARTIAL_EMPTY_POSTCODE||||" ;
					$szOriginCity = $szOriginCityPostCode;
					echo display_partial_empty_postcode_popup($postSearchAry, $fromCityAry, 'ORIGIN', $szOriginCity, $iCheckDestination,$szDestinationCity,$szOriginRegion1);
					die;
				}
			}
			else
			{	
				$res_ary = array();
				$res_ary['idOriginPostCode'] = $kConfig->idPostCode;
				
				if($kConfig->comparePostcode($szCityPostcode,$kConfig->szPostCode))
				{
					$res_ary['szOriginPostCode'] = strtoupper($kConfig->szPostCode);
					$res_ary['szOriginCity'] = $kConfig->szPostcodeCity;
				}
				else
				{
					$res_ary['szOriginCity'] = $kConfig->szPostcodeCity;
					$res_ary['szOriginPostCode'] = '';
				}
								
				$res_ary['fOriginLatitude']= $kConfig->szPostcodeLatitude;
				$res_ary['fOriginLongitude'] = $kConfig->szPostcodeLongitude; 
				$iCheckDestination = 1;
			}
		}
		if((int)$_POST['multiregionCityAry']['szRegionTo']>0)
		{
			$idFromPostcode = sanitize_all_html_input(trim($_POST['multiregionCityAry']['szRegionTo'])) ;
			$kConfig->loadPostCode($idFromPostcode);
			
			$postSearchAry = array();
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			$idServiceType = $postSearchAry['idServiceType'];
			$idDestinationCountry = $postSearchAry['idDestinationCountry'];
			$szDestinationCity = trim($_POST['multiregionCityAry']['szDestinationCity']);
			$szDestinationRegion1 = $kConfig->szRegion1 ;
			
			if(($idServiceType == __SERVICE_TYPE_DTD__) || ($idServiceType == __SERVICE_TYPE_WTD__) || ($idServiceType == __SERVICE_TYPE_PTD__ ))
			{				
				if($kConfig->comparePostcode($szDestinationCity,$kConfig->szPostCode))
				{
					$szDestinationCityPostCode = $kConfig->szPostCode;
				}
				else
				{
					$szDestinationCityPostCode = $kConfig->szPostcodeCity;
				}
				
				$fromCityAry = $kConfig->check_city_country_requirement($szDestinationCityPostCode,$idDestinationCountry,false,false,false,$szDestinationRegion1);
			
				if(($kConfig->emptyPostcodeCtr == 1 || $kConfig->partialEmptyPostcodeCtr==1) && !empty($fromCityAry))
				{
					$res_ary = array();
					$res_ary['idDestinationPostCode'] = $kConfig->idEmptyPostCode;
					
					if($kConfig->comparePostcode($kConfig->szDestinationCity,$kConfig->szPostCode))
					{
						$res_ary['szDestinationPostCode'] = strtoupper($kConfig->szPostCode);
						$res_ary['szDestinationCity'] = $kConfig->szCity;
					}
					else
					{
						$res_ary['szDestinationPostCode'] = strtoupper($kConfig->szPostCode);
						$res_ary['szDestinationCity'] = $kConfig->szCity;
					}					
					$res_ary['fDestinationLatitude'] = $kConfig->szLatitude;
					$res_ary['fDestinationLongitude'] = $kConfig->szLongitude;
					if((int)$postSearchAry['iBookingStep']<4)
					{
						$res_ary['iBookingStep'] = 4 ;
					}
				}
				else if($kConfig->emptyPostcodeCtr > 1 && !empty($fromCityAry))
				{
					echo "MULTI_EMPTY_POSTCODE||||";
					$szOriginCity = $szOriginCityPostCode;
					echo display_empty_postcode_popup($postSearchAry,$fromCityAry, 'DESTINATION', $szDestinationCityPostCode);
					die;
				}
				else if($kConfig->partialEmptyPostcodeCtr > 1 && !empty($fromCityAry))
				{
					echo "MULTI_PARTIAL_EMPTY_POSTCODE||||" ;
					$szOriginCity = $szOriginCityPostCode;
					echo display_partial_empty_postcode_popup($postSearchAry,$fromCityAry, 'DESTINATION', $szDestinationCityPostCode,false,false,$szDestinationRegion1);
					die;
				}
			}
			else
			{
				$res_ary['idDestinationPostCode'] = $kConfig->idPostCode;
				
				if($kConfig->comparePostcode($szDestinationCity,$kConfig->szPostCode))
				{
					$res_ary['szDestinationPostCode']= strtoupper($kConfig->szPostCode);
					$res_ary['szDestinationCity'] = $kConfig->szPostcodeCity;
				}
				else
				{
					$res_ary['szDestinationCity'] = $kConfig->szPostcodeCity;
					$res_ary['szDestinationPostCode'] = '';
				}
				
				$res_ary['fDestinationLatitude']= $kConfig->szPostcodeLatitude;
				$res_ary['fDestinationLongitude'] = $kConfig->szPostcodeLongitude;
				if((int)$postSearchAry['iBookingStep']<4)
				{
					$res_ary['iBookingStep'] = 4 ;
				}
			}
		}
		
		if($kBooking->updateBooking($res_ary,$idBooking))
		{
			$postSearchAry = array();
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			$idServiceType = $postSearchAry['idServiceType'];
			$idDestinationCountry = $postSearchAry['idDestinationCountry'];
			if($iCheckDestination==1)
			{
				
				/*
				* 1. If we updated origin data into database then we are checking if there is any haulage exist for this location
				* 2. We check haulage only if DT? Service 
				**/
				if(($idServiceType == __SERVICE_TYPE_DTD__) || ($idServiceType == __SERVICE_TYPE_DTW__) || ($idServiceType == __SERVICE_TYPE_DTP__ ))
				{	
					$check_origin_haulage = $kWHSSearch->isHaulageExists($postSearchAry,1);
					if($check_origin_haulage)
					{
						echo "POPUP||||";
						echo display_orgin_haulage_not_exist_popup($postSearchAry,'VALIDATE_POSTCODE_AFTER_HAULAGE_POPUP',$kConfig->szPostCode,$szDestinationCity);
						die;
					}
				}
				
				if(($idServiceType == __SERVICE_TYPE_DTD__) || ($idServiceType == __SERVICE_TYPE_WTD__) || ($idServiceType == __SERVICE_TYPE_PTD__ ))
				{	
					//Last true parameter forcing to first check multiregion then go for next level comparison
					$fromCityAry = $kConfig->check_city_country_requirement($szDestinationCity,$idDestinationCountry,false,false,false,false,true);
					
					if($kConfig->iMultiRegionFlag == 1)
					{
						echo "MULTIREGIO||||";
						echo display_multiregion_popup($postSearchAry, $fromCityAry, 'DESTINATION', $szDestinationCity);
						die;
					}
					else if(($kConfig->emptyPostcodeCtr == 1 || $kConfig->partialEmptyPostcodeCtr==1) && !empty($fromCityAry))
					{
						$res_ary = array();
						$res_ary['idDestinationPostCode'] = $kConfig->idEmptyPostCode;
						
						if($kConfig->comparePostcode($kConfig->szDestinationCity,$kConfig->szPostCode))
						{
							$res_ary['szDestinationPostCode'] = strtoupper($kConfig->szPostCode);
							$res_ary['szDestinationCity'] = $kConfig->szCity;
						}
						else
						{
							$res_ary['szDestinationPostCode'] = strtoupper($kConfig->szPostCode);
							$res_ary['szDestinationCity'] = $kConfig->szCity;
						}					
						$res_ary['fDestinationLatitude'] = $kConfig->szLatitude;
						$res_ary['fDestinationLongitude'] = $kConfig->szLongitude;
						if((int)$postSearchAry['iBookingStep']<4)
						{
							$res_ary['iBookingStep'] = 4 ;
						}
						
						if($kBooking->updateBooking($res_ary,$idBooking))
						{
							$postSearchAry = array();
							$postSearchAry = $kBooking->getBookingDetails($idBooking);
							$check_origin_haulage = $kWHSSearch->isHaulageExists($postSearchAry,2);
							if($check_origin_haulage)
							{
								echo "POPUP||||";
								echo display_destination_haulage_not_exist_popup($postSearchAry);
								die;
							}
							else
							{
								echo "SUCCESS||||";
								echo display_timing_box_active($postSearchAry);
								echo "||||";
								echo city_postcode_active_box($postSearchAry,true);
								echo "||||";
								echo display_custom_clearance_box($postSearchAry);
								die;
							}
						}
					}
					else if($kConfig->emptyPostcodeCtr > 1 && !empty($fromCityAry))
					{
						echo "MULTI_EMPTY_POSTCODE||||";
						$szOriginCity = $szOriginCityPostCode;
						echo display_empty_postcode_popup($postSearchAry,$fromCityAry, 'DESTINATION', $szDestinationCity);
						die;
					}
					else if($kConfig->partialEmptyPostcodeCtr > 1 && !empty($fromCityAry))
					{
						echo "MULTI_PARTIAL_EMPTY_POSTCODE||||" ;
						$szOriginCity = $szOriginCityPostCode;
						echo display_partial_empty_postcode_popup($postSearchAry,$fromCityAry, 'DESTINATION', $szDestinationCity);
						die;
					}
				}
				else
				{
					$toCityAry = $kConfig->isCityExistsByCountry_requirement($szDestinationCity,$idDestinationCountry) ;
					if(count($toCityAry)>1)
					{
						echo "MULTIREGIO||||";
						echo display_multiregion_popup($postSearchAry, $toCityAry, 'DESTINATION', $szDestinationCity);
						die;
					}
					else
					{
						$res_ary = array();
						$res_ary['idDestinationPostCode'] = $kConfig->idPostCode;
						
						if($kConfig->comparePostcode($szDestinationCity,$kConfig->szPostCode))
						{
							$res_ary['szDestinationPostCode'] = $kConfig->szPostCode;
							$res_ary['szDestinationCity'] = $kConfig->szCity;
						}
						else
						{
							$res_ary['szDestinationCity'] = $kConfig->szCity;
							$res_ary['szDestinationPostCode'] = '';
						}					
						$res_ary['fDestinationLatitude'] = $kConfig->szLatitude;
						$res_ary['fDestinationLongitude'] = $kConfig->szLongitude;
						if((int)$postSearchAry['iBookingStep']<4)
						{
							$res_ary['iBookingStep'] = 4 ;
						}
						
						if($kBooking->updateBooking($res_ary,$idBooking))
						{
							$postSearchAry = array();
							$postSearchAry = $kBooking->getBookingDetails($idBooking);
							
							echo "SUCCESS||||";
							echo display_timing_box_active($postSearchAry);
							echo "||||";
							echo city_postcode_active_box($postSearchAry,true);
							echo "||||";
							echo display_custom_clearance_box($postSearchAry);
							die;
						}
					}
				}
			}
			else
			{
				/*
				* 1. If we updated origin data into database then we are checking if there is any haulage exist for this location
				* 2. We check haulage only if DT? Service 
				**/
				if(($idServiceType == __SERVICE_TYPE_DTD__) || ($idServiceType == __SERVICE_TYPE_DTW__) || ($idServiceType == __SERVICE_TYPE_DTP__ ))
				{	
					$postSearchAry = array();
					$postSearchAry = $kBooking->getBookingDetails($idBooking);
					
					$check_origin_haulage = $kWHSSearch->isHaulageExists($postSearchAry,1);
					if($check_origin_haulage)
					{
						echo "POPUP||||";
						echo display_orgin_haulage_not_exist_popup($postSearchAry,'VALIDATE_POSTCODE_AFTER_HAULAGE_POPUP',$kConfig->szPostCode,$szDestinationCity);
						die;
					}
					else
					{
						echo "SUCCESS||||";
						echo display_timing_box_active($postSearchAry);
						echo "||||";
						echo city_postcode_active_box($postSearchAry,true);
						echo "||||";
						echo display_custom_clearance_box($postSearchAry);
						die;
					}
				}
				else
				{
					$postSearchAry = array();
					$postSearchAry = $kBooking->getBookingDetails($idBooking);
					echo "SUCCESS||||";
					echo display_timing_box_active($postSearchAry);
					echo "||||";
					echo city_postcode_active_box($postSearchAry,true);	
					echo "||||";
					echo display_custom_clearance_box($postSearchAry);
					die;
				}
			}
		}
	}
}
else if(!empty($_POST['multiEmptyPostcodeCityAry']))
{
	$res_ary=array();
	
	$error_message = "";	
	if(((int)$_POST['multiEmptyPostcodeCityAry']['iRegionFrom']>0) && ((int)$_POST['multiEmptyPostcodeCityAry']['szRegionFrom']<=0))
	{
		$error_message .= "<li> Please select exact origin </li>";
	}
	if(((int)$_POST['multiEmptyPostcodeCityAry']['iRegionTo']>0) && ((int)$_POST['multiEmptyPostcodeCityAry']['szRegionTo']<=0))
	{
		$error_message .= "<li> Please select exact destination </li>";
	}
	if(!empty($error_message))
	{
		echo "ERROR||||".$error_message;
	}
	else
	{
		if((int)$_POST['multiEmptyPostcodeCityAry']['szRegionFrom']>0)
		{
			$idFromPostcode = sanitize_all_html_input(trim($_POST['multiEmptyPostcodeCityAry']['szRegionFrom'])) ;
			$kConfig->loadPostCode($idFromPostcode);
			
			$szBookingRandomNum = trim($_POST['multiEmptyPostcodeCityAry']['szBookingRandomNum']);
			$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
		
			$postSearchAry = array();
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			$idServiceType = $postSearchAry['idServiceType'];
			$idOriginCountry = $postSearchAry['idOriginCountry'];
			$szDestinationCity = trim($_POST['multiEmptyPostcodeCityAry']['szDestinationCity']);
			$szOriginRegion1 = $kConfig->szRegion1 ;
			$idDestinationCountry = $postSearchAry['idDestinationCountry'];
			
			if(($idServiceType == __SERVICE_TYPE_DTD__) || ($idServiceType == __SERVICE_TYPE_DTW__) || ($idServiceType == __SERVICE_TYPE_DTP__ ))
			{	
				$res_ary['idOriginPostCode'] = $kConfig->idPostCode;
				$res_ary['szOriginPostCode']= $kConfig->szPostCode;
				$res_ary['szOriginCity'] = $kConfig->szPostcodeCity;
				$res_ary['fOriginLatitude']= $kConfig->szPostcodeLatitude;
				$res_ary['fOriginLongitude'] = $kConfig->szPostcodeLongitude; 
				
				$kBooking->updateBooking($res_ary,$idBooking);
			
				$postSearchAry = array();
				$postSearchAry = $kBooking->getBookingDetails($idBooking);
				$check_origin_haulage = $kWHSSearch->isHaulageExists($postSearchAry,1);
				if($check_origin_haulage)
				{
					echo "POPUP||||";
					echo display_orgin_haulage_not_exist_popup($postSearchAry,'VALIDATE_POSTCODE_AFTER_HAULAGE_POPUP',$kConfig->idPostCode,$szDestinationCity);
					die;
				}
				else
				{
					if(($idServiceType == __SERVICE_TYPE_DTD__) || ($idServiceType == __SERVICE_TYPE_WTD__) || ($idServiceType == __SERVICE_TYPE_PTD__ ))
					{	
						//Last true param forcing to first check multiregion then go for next level comparison
						$fromCityAry = $kConfig->check_city_country_requirement($szDestinationCity,$idDestinationCountry,false,false,false,false,true);
						if($kConfig->iMultiRegionFlag == 1)
						{
							echo "MULTIREGIO||||";
							echo display_multiregion_popup($postSearchAry, $fromCityAry, 'DESTINATION', $szDestinationCity);
							die;
						}
						else if(($kConfig->emptyPostcodeCtr == 1 || $kConfig->partialEmptyPostcodeCtr==1) && !empty($fromCityAry))
						{
							
							$res_ary = array();
							$res_ary['idDestinationPostCode'] = $kConfig->idEmptyPostCode;
							
							if($kConfig->comparePostcode($kConfig->szDestinationCity,$kConfig->szPostCode))
							{
								$res_ary['szDestinationPostCode'] = $kConfig->szPostCode;
								$res_ary['szDestinationCity'] = $kConfig->szCity;
							}
							else
							{
								$res_ary['szDestinationPostCode'] = $kConfig->szPostCode;
								$res_ary['szDestinationCity'] = $kConfig->szCity;
							}					
							$res_ary['fDestinationLatitude'] = $kConfig->szLatitude;
							$res_ary['fDestinationLongitude'] = $kConfig->szLongitude;
							if((int)$postSearchAry['iBookingStep']<4)
							{
								$res_ary['iBookingStep'] = 4 ;
							}
							
							if($kBooking->updateBooking($res_ary,$idBooking))
							{
								$postSearchAry = array();
								$postSearchAry = $kBooking->getBookingDetails($idBooking);
								$check_origin_haulage = $kWHSSearch->isHaulageExists($postSearchAry,2);
								
								if($check_origin_haulage)
								{
									echo "POPUP||||";
									echo display_destination_haulage_not_exist_popup($postSearchAry);
									die;
								}
								else
								{
									$postSearchAry = array();
									$postSearchAry = $kBooking->getBookingDetails($idBooking);
									echo "SUCCESS||||";
									echo display_timing_box_active($postSearchAry);
									echo "||||";
									echo city_postcode_active_box($postSearchAry,true);	
									echo "||||";
									echo display_custom_clearance_box($postSearchAry);
									die;
								}
							}
						}
						else if($kConfig->emptyPostcodeCtr > 1 && !empty($fromCityAry))
						{
							echo "MULTI_EMPTY_POSTCODE||||";
							$szOriginCity = $szOriginCityPostCode;
							echo display_empty_postcode_popup($postSearchAry,$fromCityAry, 'DESTINATION', $szDestinationCity);
							die;
						}
						else if($kConfig->partialEmptyPostcodeCtr > 1 && !empty($fromCityAry))
						{
							echo "MULTI_PARTIAL_EMPTY_POSTCODE||||" ;
							$szOriginCity = $szOriginCityPostCode;
							echo display_partial_empty_postcode_popup($postSearchAry,$fromCityAry, 'DESTINATION', $szDestinationCity);
							die;
						}
					}
					else
					{
						$toCityAry = $kConfig->isCityExistsByCountry_requirement($szDestinationCity,$idDestinationCountry) ;
						if(count($toCityAry)>1)
						{
							echo "MULTIREGIO||||";
							echo display_multiregion_popup($postSearchAry, $toCityAry, 'DESTINATION', $szDestinationCity);
							die;
						}
						else
						{
							$res_ary = array();
							$res_ary['idDestinationPostCode'] = $kConfig->idPostCode;
							
							if($kConfig->comparePostcode($szDestinationCity,$kConfig->szPostCode))
							{
								$res_ary['szDestinationPostCode'] = $szDestinationCity;
								$res_ary['szDestinationCity'] = $kConfig->szCity;
							}
							else
							{
								$res_ary['szDestinationPostCode'] = '';
								$res_ary['szDestinationCity'] = $szDestinationCity;
							}					
							$res_ary['fDestinationLatitude'] = $kConfig->szLatitude;
							$res_ary['fDestinationLongitude'] = $kConfig->szLongitude;
							if((int)$postSearchAry['iBookingStep']<4)
							{
								$res_ary['iBookingStep'] = 4 ;
							}
							
							if($kBooking->updateBooking($res_ary,$idBooking))
							{
								$postSearchAry = array();
								$postSearchAry = $kBooking->getBookingDetails($idBooking);
								echo "SUCCESS||||";
								echo display_timing_box_active($postSearchAry);
								echo "||||";
								echo city_postcode_active_box($postSearchAry,true);	
								echo "||||";
								echo display_custom_clearance_box($postSearchAry);
								die;
							}
						}
					}
				}
			}
			else
			{
				$res_ary = array();
				$res_ary['idOriginPostCode'] = $kConfig->idPostCode;
				$res_ary['szOriginPostCode']= $kConfig->szPostCode;
				$res_ary['szOriginCity'] = $kConfig->szPostcodeCity;
				$res_ary['fOriginLatitude']= $kConfig->szPostcodeLatitude;
				$res_ary['fOriginLongitude'] = $kConfig->szPostcodeLongitude; 
								
				if($kBooking->updateBooking($res_ary,$idBooking))
				{
					$postSearchAry = array();
					$postSearchAry = $kBooking->getBookingDetails($idBooking);
					
					if(($idServiceType == __SERVICE_TYPE_DTD__) || ($idServiceType == __SERVICE_TYPE_WTD__) || ($idServiceType == __SERVICE_TYPE_PTD__ ))
					{	
						//Last true parameter forcing to first check multiregion then go for next level comparison
						$fromCityAry = $kConfig->check_city_country_requirement($szDestinationCity,$idDestinationCountry,false,false,false,false,true);
						
						if($kConfig->iMultiRegionFlag == 1)
						{
							echo "MULTIREGIO||||";
							echo display_multiregion_popup($postSearchAry, $fromCityAry, 'DESTINATION', $szDestinationCity);
							die;
						}
						else if(($kConfig->emptyPostcodeCtr == 1 || $kConfig->partialEmptyPostcodeCtr==1) && !empty($fromCityAry))
						{
							$res_ary = array();
							$res_ary['idDestinationPostCode'] = $kConfig->idEmptyPostCode;
							
							if($kConfig->comparePostcode($kConfig->szDestinationCity,$kConfig->szPostCode))
							{
								$res_ary['szDestinationPostCode'] = $kConfig->szPostCode;
								$res_ary['szDestinationCity'] = $kConfig->szCity;
							}
							else
							{
								$res_ary['szDestinationPostCode'] = $kConfig->szPostCode;
								$res_ary['szDestinationCity'] = $kConfig->szCity;
							}					
							$res_ary['fDestinationLatitude'] = $kConfig->szLatitude;
							$res_ary['fDestinationLongitude'] = $kConfig->szLongitude;
							if((int)$postSearchAry['iBookingStep']<4)
							{
								$res_ary['iBookingStep'] = 4 ;
							}
							
							if($kBooking->updateBooking($res_ary,$idBooking))
							{
								$postSearchAry = array();
								$postSearchAry = $kBooking->getBookingDetails($idBooking);
								$check_origin_haulage = $kWHSSearch->isHaulageExists($postSearchAry,2);
								if($check_origin_haulage)
								{
									echo "POPUP||||";
									echo display_destination_haulage_not_exist_popup($postSearchAry);
									die;
								}
								else
								{
									$postSearchAry = array();
									$postSearchAry = $kBooking->getBookingDetails($idBooking);
									
									echo "SUCCESS||||";
									echo display_timing_box_active($postSearchAry);
									echo "||||";
									echo city_postcode_active_box($postSearchAry,true);	
									echo "||||";
									echo display_custom_clearance_box($postSearchAry);
									die;
								}
							}
						}
						else if($kConfig->emptyPostcodeCtr > 1 && !empty($fromCityAry))
						{
							echo "MULTI_EMPTY_POSTCODE||||";
							$szOriginCity = $szOriginCityPostCode;
							echo display_empty_postcode_popup($postSearchAry,$fromCityAry, 'DESTINATION', $szDestinationCity);
							die;
						}
						else if($kConfig->partialEmptyPostcodeCtr > 1 && !empty($fromCityAry))
						{
							echo "MULTI_PARTIAL_EMPTY_POSTCODE||||" ;
							$szOriginCity = $szOriginCityPostCode;
							echo display_partial_empty_postcode_popup($postSearchAry,$fromCityAry, 'DESTINATION', $szDestinationCity);
							die;
						}
					}
					else
					{
						$toCityAry = $kConfig->isCityExistsByCountry_requirement($szDestinationCity,$idDestinationCountry) ;
						if(count($toCityAry)>1)
						{
							echo "MULTIREGIO||||";
							echo display_multiregion_popup($postSearchAry, $toCityAry, 'DESTINATION', $szDestinationCity);
							die;
						}
						else
						{
							$res_ary = array();
							$res_ary['idDestinationPostCode'] = $kConfig->idPostCode;
							
							if($kConfig->comparePostcode($szDestinationCity,$kConfig->szPostCode))
							{
								$res_ary['szDestinationPostCode'] = $kConfig->szPostCode;
								$res_ary['szDestinationCity'] = $kConfig->szCity;
							}
							else
							{
								$res_ary['szDestinationPostCode'] = '';
								$res_ary['szDestinationCity'] = $kConfig->szCity;
							}					
							$res_ary['fDestinationLatitude'] = $kConfig->szLatitude;
							$res_ary['fDestinationLongitude'] = $kConfig->szLongitude;
							if((int)$postSearchAry['iBookingStep']<4)
							{
								$res_ary['iBookingStep'] = 4 ;
							}
							
							if($kBooking->updateBooking($res_ary,$idBooking))
							{
								$postSearchAry = array();
								$postSearchAry = $kBooking->getBookingDetails($idBooking); 
									
								echo "SUCCESS||||";
								echo display_timing_box_active($postSearchAry);
								echo "||||";
								echo city_postcode_active_box($postSearchAry,true);	
								echo "||||";
								echo display_custom_clearance_box($postSearchAry);
								die;
							}
						}
					}
				}
			}
		}
		if((int)$_POST['multiEmptyPostcodeCityAry']['szRegionTo']>0)
		{
			$idFromPostcode = sanitize_all_html_input(trim($_POST['multiEmptyPostcodeCityAry']['szRegionTo'])) ;
			$kConfig->loadPostCode($idFromPostcode);
			
			$szBookingRandomNum = trim($_POST['multiEmptyPostcodeCityAry']['szBookingRandomNum']);
			$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
		
			$postSearchAry = array();
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			$idServiceType = $postSearchAry['idServiceType'];
			$idOriginCountry = $postSearchAry['idOriginCountry'];
			$szDestinationCity = trim($_POST['multiEmptyPostcodeCityAry']['szDestinationCity']);
			$szOriginRegion1 = $kConfig->szRegion1 ;
			if(($idServiceType == __SERVICE_TYPE_DTD__) || ($idServiceType == __SERVICE_TYPE_WTD__) || ($idServiceType == __SERVICE_TYPE_PTD__ ))
			{	
				$res_ary['idDestinationPostCode'] = $kConfig->idPostCode;
				$res_ary['szDestinationPostCode']= $kConfig->szPostCode;
				$res_ary['szDestinationCity'] = $kConfig->szPostcodeCity;
				$res_ary['fDestinationLatitude']= $kConfig->szPostcodeLatitude;
				$res_ary['fDestinationLongitude'] = $kConfig->szPostcodeLongitude;
				if((int)$postSearchAry['iBookingStep']<4)
				{
					$res_ary['iBookingStep'] = 4 ;
				}
				
				$kBooking->updateBooking($res_ary,$idBooking);
				
				$postSearchAry = array();
				$postSearchAry = $kBooking->getBookingDetails($idBooking);
				$check_origin_haulage = $kWHSSearch->isHaulageExists($postSearchAry,2);
				if($check_origin_haulage)
				{
					echo "POPUP||||";
					echo display_destination_haulage_not_exist_popup($postSearchAry);
					die;
				}
				else
				{
					$postSearchAry = array();
					$postSearchAry = $kBooking->getBookingDetails($idBooking);
					echo "SUCCESS||||";
					echo display_timing_box_active($postSearchAry);
					echo "||||";
					echo city_postcode_active_box($postSearchAry,true);
					echo "||||";
					echo display_custom_clearance_box($postSearchAry);
					die;
				}
			}
			else
			{
				$res_ary['idDestinationPostCode'] = $kConfig->idPostCode;
				$res_ary['szDestinationPostCode']= $kConfig->szPostCode;
				$res_ary['szDestinationCity'] = $kConfig->szPostcodeCity;
				$res_ary['fDestinationLatitude']= $kConfig->szPostcodeLatitude;
				$res_ary['fDestinationLongitude'] = $kConfig->szPostcodeLongitude;
				if((int)$postSearchAry['iBookingStep']<4)
				{
					$res_ary['iBookingStep'] = 4 ;
				}
				
				
				if($kBooking->updateBooking($res_ary,$idBooking))
				{
					if(($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_WTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_PTD__))
					{
						$postSearchAry = array();
						$postSearchAry = $kBooking->getBookingDetails($idBooking);
						if($kWHSSearch->isHaulageExists($postSearchAry,2))
						{
							echo "POPUP||||";
							echo display_destination_haulage_not_exist_popup($postSearchAry);
							die;
						}
						else
						{
							$postSearchAry = array();
							$postSearchAry = $kBooking->getBookingDetails($idBooking);
							echo "SUCCESS||||";
							echo display_timing_box_active($postSearchAry);
							echo "||||";
							echo city_postcode_active_box($postSearchAry,true);
							echo "||||";
							echo display_custom_clearance_box($postSearchAry);
							die;
						}
					}
					else
					{
						$postSearchAry = array();
						$postSearchAry = $kBooking->getBookingDetails($idBooking);
						echo "SUCCESS||||";
						echo display_timing_box_active($postSearchAry);
						echo "||||";
						echo city_postcode_active_box($postSearchAry,true);
						echo "||||";
						echo display_custom_clearance_box($postSearchAry);
						die;
					}
				}
			}
		}
	}
}
else if(!empty($_POST['partialEmptyPostcodeCityAry']))
{
	$res_ary=array();
	
	$error_message = "";	
	if(((int)$_POST['partialEmptyPostcodeCityAry']['iRegionFrom']>0) && ((int)$_POST['partialEmptyPostcodeCityAry']['szRegionFrom']<=0))
	{
		$error_message .= "<li> Please select exact origin </li>";
	}
	if(((int)$_POST['partialEmptyPostcodeCityAry']['iRegionTo']>0) && ((int)$_POST['partialEmptyPostcodeCityAry']['szRegionTo']<=0))
	{
		$error_message .= "<li> Please select exact destination </li>";
	}
	if(!empty($error_message))
	{
		echo "ERROR||||".$error_message;
	}
	else
	{
		$szBookingRandomNum = trim($_POST['partialEmptyPostcodeCityAry']['szBookingRandomNum']);
		$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
		
		if((int)$_POST['partialEmptyPostcodeCityAry']['szRegionFrom']>0)
		{
			$idFromPostcode = sanitize_all_html_input(trim($_POST['partialEmptyPostcodeCityAry']['szRegionFrom'])) ;
			$kConfig->loadPostCode($idFromPostcode);
			$postSearchAry = array();
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			$idServiceType = $postSearchAry['idServiceType'];
			$idOriginCountry = $postSearchAry['idOriginCountry'];
			$szDestinationCity = trim($_POST['partialEmptyPostcodeCityAry']['szDestinationCity']);
			$szOriginRegion1 = $kConfig->szRegion1 ;
			$idDestinationCountry = $postSearchAry['idDestinationCountry'];
			
			if(($idServiceType == __SERVICE_TYPE_DTD__) || ($idServiceType == __SERVICE_TYPE_DTW__) || ($idServiceType == __SERVICE_TYPE_DTP__ ))
			{	
				$res_ary = array();
				$res_ary['idOriginPostCode'] = $kConfig->idPostCode;
				$res_ary['szOriginPostCode']= $kConfig->szPostCode;
				$res_ary['szOriginCity'] = $kConfig->szPostcodeCity;
				$res_ary['fOriginLatitude']= $kConfig->szPostcodeLatitude;
				$res_ary['fOriginLongitude'] = $kConfig->szPostcodeLongitude; 
				
				$kBooking->updateBooking($res_ary,$idBooking);
				
				$postSearchAry = array();
				$postSearchAry = $kBooking->getBookingDetails($idBooking);
				$check_origin_haulage = $kWHSSearch->isHaulageExists($postSearchAry,1);
				
				if($check_origin_haulage)
				{
					echo "POPUP||||";
					echo display_orgin_haulage_not_exist_popup($postSearchAry,'VALIDATE_POSTCODE_AFTER_HAULAGE_POPUP',$kConfig->szPostCode,$szDestinationCity);
					die;
				}
				else
				{
					if(($idServiceType == __SERVICE_TYPE_DTD__) || ($idServiceType == __SERVICE_TYPE_WTD__) || ($idServiceType == __SERVICE_TYPE_PTD__ ))
					{	
						//Last true parameter forcing to first check multiregion then go for next level comparison
						$fromCityAry = $kConfig->check_city_country_requirement($szDestinationCity,$idDestinationCountry,false,false,false,false,true);
					
						if($kConfig->iMultiRegionFlag == 1)
						{
							echo "MULTIREGIO||||";
							echo display_multiregion_popup($postSearchAry, $fromCityAry, 'DESTINATION', $szDestinationCity);
							die;
						}
						else if(($kConfig->emptyPostcodeCtr == 1 || $kConfig->partialEmptyPostcodeCtr==1) && !empty($fromCityAry))
						{
							$res_ary = array();
							$res_ary['idDestinationPostCode'] = $kConfig->idEmptyPostCode;
							
							if($kConfig->comparePostcode($kConfig->szDestinationCity,$kConfig->szPostCode))
							{
								$res_ary['szDestinationPostCode'] = $kConfig->szPostCode;
								$res_ary['szDestinationCity'] = $kConfig->szCity;
							}
							else
							{
								$res_ary['szDestinationPostCode'] = $kConfig->szPostCode;
								$res_ary['szDestinationCity'] = $kConfig->szCity;
							}					
							$res_ary['fDestinationLatitude'] = $kConfig->szLatitude;
							$res_ary['fDestinationLongitude'] = $kConfig->szLongitude;
							if((int)$postSearchAry['iBookingStep']<4)
							{
								$res_ary['iBookingStep'] = 4 ;
							}
							
							if($kBooking->updateBooking($res_ary,$idBooking))
							{
								$postSearchAry = array();
								$postSearchAry = $kBooking->getBookingDetails($idBooking);
								$check_origin_haulage = $kWHSSearch->isHaulageExists($postSearchAry,2);
								if($check_origin_haulage)
								{
									echo "POPUP||||";
									echo display_destination_haulage_not_exist_popup($postSearchAry);
									die;
								}
								else
								{
									$postSearchAry = array();
									$postSearchAry = $kBooking->getBookingDetails($idBooking);
									echo "SUCCESS||||";
									echo display_timing_box_active($postSearchAry);
									echo "||||";
									echo city_postcode_active_box($postSearchAry,true);	
									echo "||||";
									echo display_custom_clearance_box($postSearchAry);
									die;
								}
							}
						}
						else if($kConfig->emptyPostcodeCtr > 1 && !empty($fromCityAry))
						{
							echo "MULTI_EMPTY_POSTCODE||||";
							$szOriginCity = $szOriginCityPostCode;
							echo display_empty_postcode_popup($postSearchAry,$fromCityAry, 'DESTINATION', $szDestinationCity);
							die;
						}
						else if($kConfig->partialEmptyPostcodeCtr > 1 && !empty($fromCityAry))
						{
							echo "MULTI_PARTIAL_EMPTY_POSTCODE||||" ;
							$szOriginCity = $szOriginCityPostCode;
							echo display_partial_empty_postcode_popup($postSearchAry,$fromCityAry, 'DESTINATION', $szDestinationCity);
							die;
						}
					}
					else
					{
						$toCityAry = $kConfig->isCityExistsByCountry_requirement($szDestinationCity,$idDestinationCountry) ;
						if(count($toCityAry)>1)
						{
							echo "MULTIREGIO||||";
							echo display_multiregion_popup($postSearchAry, $toCityAry, 'DESTINATION', $szDestinationCity);
							die;
						}
						else
						{
							$res_ary = array();
							$res_ary['idDestinationPostCode'] = $kConfig->idPostCode;
							
							if($kConfig->comparePostcode($szDestinationCity,$kConfig->szPostCode))
							{
								$res_ary['szDestinationPostCode'] = $kConfig->szPostCode;
								$res_ary['szDestinationCity'] = $kConfig->szCity;
							}
							else
							{
								$res_ary['szDestinationPostCode'] = '';
								$res_ary['szDestinationCity'] = $kConfig->szCity;
							}					
							$res_ary['fDestinationLatitude'] = $kConfig->szLatitude;
							$res_ary['fDestinationLongitude'] = $kConfig->szLongitude;
							if((int)$postSearchAry['iBookingStep']<4)
							{
								$res_ary['iBookingStep'] = 4 ;
							}
							
							if($kBooking->updateBooking($res_ary,$idBooking))
							{
								$postSearchAry = array();
								$postSearchAry = $kBooking->getBookingDetails($idBooking);
								echo "SUCCESS||||";
								echo display_timing_box_active($postSearchAry);
								echo "||||";
								echo city_postcode_active_box($postSearchAry,true);	
								echo "||||";
								echo display_custom_clearance_box($postSearchAry);
								die;
							}
						}
					}
				}
			}
			else
			{
				$res_ary = array();
				$res_ary['idOriginPostCode'] = $kConfig->idPostCode;
				$res_ary['szOriginPostCode']= $kConfig->szPostCode;
				$res_ary['szOriginCity'] = $kConfig->szPostcodeCity;
				$res_ary['fOriginLatitude']= $kConfig->szPostcodeLatitude;
				$res_ary['fOriginLongitude'] = $kConfig->szPostcodeLongitude; 
				
				if($kBooking->updateBooking($res_ary,$idBooking))
				{
					$postSearchAry = array();
					$postSearchAry = $kBooking->getBookingDetails($idBooking);
					
					if(($idServiceType == __SERVICE_TYPE_DTD__) || ($idServiceType == __SERVICE_TYPE_WTD__) || ($idServiceType == __SERVICE_TYPE_PTD__ ))
					{	
						//Last true parameter forcing to first check multiregion then go for next level comparison
						$fromCityAry = $kConfig->check_city_country_requirement($szDestinationCity,$idDestinationCountry,false,false,false,false,true);
						if($kConfig->iMultiRegionFlag == 1)
						{
							echo "MULTIREGIO||||";
							echo display_multiregion_popup($postSearchAry, $fromCityAry, 'DESTINATION', $szDestinationCity);
							die;
						}
						else if(($kConfig->emptyPostcodeCtr == 1 || $kConfig->partialEmptyPostcodeCtr==1) && !empty($fromCityAry))
						{
							$res_ary = array();
							$res_ary['idDestinationPostCode'] = $kConfig->idEmptyPostCode;
							
							if($kConfig->comparePostcode($kConfig->szDestinationCity,$kConfig->szPostCode))
							{
								$res_ary['szDestinationPostCode'] = $kConfig->szPostCode;
								$res_ary['szDestinationCity'] = $kConfig->szCity;
							}
							else
							{
								$res_ary['szDestinationPostCode'] = $kConfig->szPostCode;
								$res_ary['szDestinationCity'] = $kConfig->szCity;
							}					
							$res_ary['fDestinationLatitude'] = $kConfig->szLatitude;
							$res_ary['fDestinationLongitude'] = $kConfig->szLongitude;
							if((int)$postSearchAry['iBookingStep']<4)
							{
								$res_ary['iBookingStep'] = 4 ;
							}
							
							if($kBooking->updateBooking($res_ary,$idBooking))
							{
								$postSearchAry = array();
								$postSearchAry = $kBooking->getBookingDetails($idBooking);
								$check_origin_haulage = $kWHSSearch->isHaulageExists($postSearchAry,2);
								if($check_origin_haulage)
								{
									echo "POPUP||||";
									echo display_destination_haulage_not_exist_popup($postSearchAry);
									die;
								}
								else
								{
									$postSearchAry = array();
									$postSearchAry = $kBooking->getBookingDetails($idBooking);
									
									echo "SUCCESS||||";
									echo display_timing_box_active($postSearchAry);
									echo "||||";
									echo city_postcode_active_box($postSearchAry,true);	
									echo "||||";
									echo display_custom_clearance_box($postSearchAry);
									die;
								}
							}
						}
						else if($kConfig->emptyPostcodeCtr > 1 && !empty($fromCityAry))
						{
							echo "MULTI_EMPTY_POSTCODE||||";
							$szOriginCity = $szOriginCityPostCode;
							echo display_empty_postcode_popup($postSearchAry,$fromCityAry, 'DESTINATION', $szDestinationCity);
							die;
						}
						else if($kConfig->partialEmptyPostcodeCtr > 1 && !empty($fromCityAry))
						{
							echo "MULTI_PARTIAL_EMPTY_POSTCODE||||" ;
							$szOriginCity = $szOriginCityPostCode;
							echo display_partial_empty_postcode_popup($postSearchAry,$fromCityAry, 'DESTINATION', $szDestinationCity);
							die;
						}
					}
					else
					{
						$toCityAry = $kConfig->isCityExistsByCountry_requirement($szDestinationCity,$idDestinationCountry) ;
						if(count($toCityAry)>1)
						{
							echo "MULTIREGIO||||";
							echo display_multiregion_popup($postSearchAry, $toCityAry, 'DESTINATION', $szDestinationCity);
							die;
						}
						else
						{
							$res_ary = array();
							$res_ary['idDestinationPostCode'] = $kConfig->idPostCode;
							
							if($kConfig->comparePostcode($szDestinationCity,$kConfig->szPostCode))
							{
								$res_ary['szDestinationPostCode'] = $kConfig->szPostCode;
								$res_ary['szDestinationCity'] = $kConfig->szCity;
							}
							else
							{
								$res_ary['szDestinationPostCode'] = '';
								$res_ary['szDestinationCity'] = $kConfig->szCity;
							}					
							$res_ary['fDestinationLatitude'] = $kConfig->szLatitude;
							$res_ary['fDestinationLongitude'] = $kConfig->szLongitude;
							if((int)$postSearchAry['iBookingStep']<4)
							{
								$res_ary['iBookingStep'] = 4 ;
							}
							
							if($kBooking->updateBooking($res_ary,$idBooking))
							{
								$postSearchAry = array();
								$postSearchAry = $kBooking->getBookingDetails($idBooking);
								$check_origin_haulage = $kWHSSearch->isHaulageExists($postSearchAry,2);
								if($check_origin_haulage)
								{
									echo "POPUP||||";
									echo display_destination_haulage_not_exist_popup($postSearchAry);
									die;
								}
								else
								{
									$postSearchAry = array();
									$postSearchAry = $kBooking->getBookingDetails($idBooking);
									echo "SUCCESS||||";
									echo display_timing_box_active($postSearchAry);
									echo "||||";
									echo city_postcode_active_box($postSearchAry,true);	
									echo "||||";
									echo display_custom_clearance_box($postSearchAry);
									die;
								}
							}
						}
					}
				}
			}
		}
		if((int)$_POST['partialEmptyPostcodeCityAry']['szRegionTo']>0)
		{
			$idFromPostcode = sanitize_all_html_input(trim($_POST['partialEmptyPostcodeCityAry']['szRegionTo'])) ;
			$kConfig->loadPostCode($idFromPostcode);
			
			$szBookingRandomNum = trim($_POST['partialEmptyPostcodeCityAry']['szBookingRandomNum']);
			$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
		
			$postSearchAry = array();
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			$idServiceType = $postSearchAry['idServiceType'];
			$idOriginCountry = $postSearchAry['idOriginCountry'];
			$szDestinationCity = trim($_POST['partialEmptyPostcodeCityAry']['szDestinationCity']);
			$szOriginRegion1 = $kConfig->szRegion1 ;
			if(($idServiceType == __SERVICE_TYPE_DTD__) || ($idServiceType == __SERVICE_TYPE_WTD__) || ($idServiceType == __SERVICE_TYPE_PTD__ ))
			{	
				$res_ary['idDestinationPostCode'] = $kConfig->idPostCode;
				$res_ary['szDestinationPostCode']= $kConfig->szPostCode;
				$res_ary['szDestinationCity'] = $kConfig->szPostcodeCity;
				$res_ary['fDestinationLatitude']= $kConfig->szPostcodeLatitude;
				$res_ary['fDestinationLongitude'] = $kConfig->szPostcodeLongitude;
				if((int)$postSearchAry['iBookingStep']<4)
				{
					$res_ary['iBookingStep'] = 4 ;
				}
				
				$kBooking->updateBooking($res_ary,$idBooking);
				
				$postSearchAry = array();
				$postSearchAry = $kBooking->getBookingDetails($idBooking);
				$check_origin_haulage = $kWHSSearch->isHaulageExists($postSearchAry,2);
				if($check_origin_haulage)
				{
					echo "POPUP||||";
					echo display_destination_haulage_not_exist_popup($postSearchAry);
					die;
				}
				else
				{
					$postSearchAry = array();
					$postSearchAry = $kBooking->getBookingDetails($idBooking);
					echo "SUCCESS||||";
					echo display_timing_box_active($postSearchAry);
					echo "||||";
					echo city_postcode_active_box($postSearchAry,true);
					echo "||||";
					echo display_custom_clearance_box($postSearchAry);
					die;
				}
			}
			else
			{
				$res_ary['idDestinationPostCode'] = $kConfig->idPostCode;
				$res_ary['szDestinationPostCode']= $kConfig->szPostCode;
				$res_ary['szDestinationCity'] = $kConfig->szPostcodeCity;
				$res_ary['fDestinationLatitude']= $kConfig->szPostcodeLatitude;
				$res_ary['fDestinationLongitude'] = $kConfig->szPostcodeLongitude;
				if((int)$postSearchAry['iBookingStep']<4)
				{
					$res_ary['iBookingStep'] = 4 ;
				}
								
				if($kBooking->updateBooking($res_ary,$idBooking))
				{
					$postSearchAry = array();
					$postSearchAry = $kBooking->getBookingDetails($idBooking);
					echo "SUCCESS||||";
					echo display_timing_box_active($postSearchAry);
					echo "||||";
					echo city_postcode_active_box($postSearchAry,true);
					echo "||||";
					echo display_custom_clearance_box($postSearchAry);
					die;
				}
			}
		}
	}
}
else if($mode == 'CARGO_TIMING')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);	
	if($idBooking>0)
	{
		$kConfig = new cConfig();
		$data=array();
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		
		$dtTiming = sanitize_all_html_input(trim($_REQUEST['timingAry']['dtTiming']));
		$idTimingType = sanitize_all_html_input(trim($_REQUEST['timingAry']['idTimingType']));
		
		
		$data['dtTiming'] = $dtTiming;			
		$data['idTimingType'] = $idTimingType;
		if((int)$idTimingType<=0)
		{
			$kConfig->addError('szOriginCityPostcode',t($t_base.'errors/select_timing_type'));
		}
		$kConfig->validate_timing_requirement($data);
		if(!empty($dtTiming))
		{
			$dtTimingAry = explode('/',$dtTiming);
			$dtTiming_str = $dtTimingAry[2]."-".$dtTimingAry[1]."-".$dtTimingAry[0]." 00:00:00";
			$dtTiming_str_time = strtotime($dtTiming_str);
			
			$dtMaximumDateTime = sanitize_all_html_input(trim($_REQUEST['timingAry']['dtMaxDate']));
			$dtMinimumDateTime = sanitize_all_html_input(trim($_REQUEST['timingAry']['dtMinDate']));
			//echo "<br /> min date ",date('Y-m-d',$dtMinimumDateTime);
			//echo "<br /> max date ",date('Y-m-d',$dtMaximumDateTime);
			if($idTimingType==1) // ready at origin 
			{
				if($dtTiming_str_time > $dtMaximumDateTime)
				{
					echo "POPUP||||";
					$postSearchAry['dtTimingDate'] = $dtTiming_str_time;
					$postSearchAry['dtMinDate'] = $dtMinimumDateTime;
					$postSearchAry['dtMaxDate'] = $dtMaximumDateTime;
					
					$kBooking->addExpactedServiceData($idBooking);
					$landing_page_url = __LANDING_PAGE_URL__ ;
					$idExpactedService = $kBooking->idExpactedService ;
					
					display_S1_S2_popup($postSearchAry,$idService,$idExpactedService);
					die;
				}
			}				
			else if($idTimingType==2) // Available at destination
			{
				$dtMaximumDate = date('Y-m-d H:i:s',$dtMaximumDateTime);
				
				$dtMaximumDateTime_14_days = strtotime($dtMaximumDate. ' + 14 days');
				if($dtTiming_str_time > $dtMaximumDateTime_14_days)
				{
					echo "POPUP||||";
					$postSearchAry['dtTimingDate'] = $dtTiming_str_time;
					$postSearchAry['dtMinDate'] = $dtMinimumDateTime;
					$postSearchAry['dtMaxDate'] = $dtMaximumDateTime;
					
					$kBooking->addExpactedServiceData($idBooking);
					$landing_page_url = __LANDING_PAGE_URL__ ;
					$idExpactedService = $kBooking->idExpactedService ;
					
					display_T1_T2_popup($postSearchAry,$idService,$idExpactedService);
					die;
				}
			}
		}
		if(!empty($kConfig->arErrorMessages))
		{
			echo "ERROR_POPUP||||" ;
		?>
			<div id="popup-bg"></div>
			<div id="popup-container">
			<div class="standard-popup popup">	
			<div id="regError" class="errorBox1-requirement">
			<div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
			<div id="regErrorList">
			<ul>
			<?php
			      foreach($kConfig->arErrorMessages as $key=>$values)
			      {
			      ?><li><?=$values?></li>
			      <?php 
			      }
			?>
			</ul>
			</div>
		</div>
		<br />
		<p align="center">
			<a heref="javascript:void(0);" class="orange-button1" onclick="closePopup('all_available_service');"><span><?=t($t_base.'fields/error_close');?></span></a></p>
		</p>
		</div>
	</div>
		<?php
		}
		else
		{
			if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
			{
				echo "POPUP||||" ;
				$t_base_booking_details = "BookingDetails/";
				echo display_booking_already_paid($t_base_booking_details,$idBooking);
				die;
			}
			
			$res_ary=array();
			$res_ary['dtTimingDate'] = $kConfig->dtTiming;
			$res_ary['idTimingType']= $idTimingType;
			if((int)$postSearchAry['iBookingStep']<5)
			{
				$res_ary['iBookingStep'] = 5 ;
			}
			if(!empty($res_ary))
			{
				$update_query='';
				foreach($res_ary as $key=>$value)
				{
					$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
				}
			}				
			$update_query = rtrim($update_query,",");
			
			if($kBooking->updateDraftBooking($update_query,$idBooking))
			{
				$postSearchAry = array();
				$postSearchAry = $kBooking->getBookingDetails($idBooking);
				
				$kWhsSearch = new cWHSSearch();
				$kWhsSearch->isCustomClearanceExist($postSearchAry);
				
				
				echo "SUCCESS||||";
				echo display_timing_box($postSearchAry);
				echo "||||";
				if($kWhsSearch->iOriginCC==1 || $kWhsSearch->iDestinationCC==1)
				{
					echo "CC_EXISTS";
					echo "||||";
					echo display_custom_clearance_box_active($postSearchAry);
				}
				else
				{
					echo "NO_CC_EXIST";
					echo "||||";
					echo cargo_details_box_active($postSearchAry);
				}
			}
		}
	}
}
else if($mode=='CUSTOM_CLEARANCE')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$kConfig = new cConfig();
		$data=array();
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		
		$iOriginCC = sanitize_all_html_input(trim($_REQUEST['customClearanceAry']['iOriginCC']));
		$iDestinationCC = sanitize_all_html_input(trim($_REQUEST['customClearanceAry']['iDestinationCC']));
		
		$res_ary=array();
		if($iOriginCC<=0 && $iDestinationCC<=0)
		{
			$res_ary['iCustomClearance'] = 3; // No CC exists 
		}
		else if($iOriginCC > 0 && $iDestinationCC > 0)
		{
			$res_ary['iCustomClearance'] = 1; // Both origin and destination CC exists 
		}
		else
		{
			$res_ary['iCustomClearance'] = 2; // any one CC exists 
		}
		
		if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
		{
			echo "POPUP||||" ;
			$t_base_booking_details = "BookingDetails/";
			echo display_booking_already_paid($t_base_booking_details,$idBooking);
			die;
		}
			
		$res_ary['iOriginCC'] = $iOriginCC;
		$res_ary['iDestinationCC']= $iDestinationCC;
		
		if(!empty($res_ary))
		{
			$update_query='';
			foreach($res_ary as $key=>$value)
			{
				$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
			}
		}				
		$update_query = rtrim($update_query,",");
		
		if($kBooking->updateDraftBooking($update_query,$idBooking))
		{
			$postSearchAry = array();
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			
			echo "SUCCESS||||";
			echo display_custom_clearance_box($postSearchAry);
			echo "||||";
			echo cargo_details_box_active($postSearchAry);
			
			if($res_ary['iOriginCC']==1 && $postSearchAry['idOriginCountry']==__ID_COUNTRY_CHINA__)
			{
				//echo "||||EXPORT_POPUP||||";
				//echo display_custom_clearance_addition_popup();
			}
			die;
		}
	}
}
else if($mode=='CARGO_DETAILS')
{
	$t_base = "LandingPage/";
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$kConfig = new cConfig();
		$data=array();
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		$data = $_REQUEST['cargodetailAry'] ;
		$kConfig->validate_cargo_details_requirement($data);
		
		if($kConfig->iCargoExceed==1)
		{
			echo "ERROR_POPUP||||" ;
			$kWHSSearch = new cWHSSearch();
			$maxCargoLength = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_DIMENSION_LENGTH__');
			$maxCargoWidth = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_DIMENSION_WIDTH__');
			$maxCargoHeight = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_DIMENSION_HEIGHT__');
			
			$maxCargoWeight = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_WEIGHT__');
			
			$fKgFactor = $kWHSSearch->getWeightFactor(2);
			$maxCargoWeight_pounds = round((float)$maxCargoWeight * $fKgFactor);
			$maxCargoWeight_pounds = (int)(($maxCargoWeight_pounds)/1000) * 1000;
			
			$_SESSION['cargo_details_'.$idBooking] = $data ;
		?>
		<script type="text/javascript">
		$().ready(function(){
			 Custom.init();
		});
		</script>
		<div id="popup-bg" class="white-bg"></div>
		<div id="popup-container">
		<div id="loader" class="loader_popup_bg" style="display:none;">
			<div class="popup_loader"></div>				
				<img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
		</div>	
		<form id="cargo_exceed_form" method="post" name="cargo_exceed_form" action="">
			<div class="standard-popup-rk popup">	
				<p align="right" class="close-icon" ><a heref="javascript:void(0);" onclick="reloadCargoDetail('<?=$szBookingRandomNum?>');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
				<h5><?=t($t_base.'title/rk1_popup_header')?></h5>
				<p><?=t($t_base.'title/rk1_popup_detail_text1');?>: </p>
				<p align="center">
					<?
						echo $maxCargoLength." ".t($t_base.'title/cm')." x ".$maxCargoWidth." ".t($t_base.'title/cm')." X ".$maxCargoHeight." cm<br /> (".t($t_base.'title/length')." x ".t($t_base.'title/width')." x ".t($t_base.'title/height').")";						
					?>
				</p>
				<p align="center">
					<?
					echo number_format((float)$maxCargoWeight)." ".t($t_base.'title/kg')." ~ ".number_format((float)$maxCargoWeight_pounds)." ".t($t_base.'title/pounds')." "; 
					?>
				</p>
				<p><?=t($t_base.'title/rk1_popup_detail_text2');?></p>
				<p style="margin-top:14px;"><input class="styled" type="checkbox" value="1" onclick = "enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>',false,this.id);" name="addExpactedServiceAry[iSendNotification]" id="iSendNotification_popup" > <?=t($t_base.'title/rk1_popup_checkbox_text');?></p>
				<?php
				$notify_me_class = "class='gray-button1'";
				
				if($_SESSION['user_id']<=0)
				{
					?>
					<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>				
					<div class="oh" style="margin-top:10px;">
						<p class="fl-20"><?=t($t_base.'fields/name');?></p>
						<p class="fl-80">
							<input type="text" id="szCustomerName_popup"  onkeyup = "enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>',false,this.id);" onblur="enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>',false,this.id);" name="addExpactedServiceAry[szCustomerName]" value="<?=$postSearchResult['szCustomerName']?>">
							<span id="szCustomerName_popup_error" class="red_text" style="display:none;float:right;"></span>
						</p>
					</div>
					<?
				}
				else
				{
					$emailfieldtopspace="style='margin-top:10px;'";
					$kUser = new cUser();
					$kUser->getUserDetails($_SESSION['user_id']);
					if(empty($postSearchResult['szEmail']))
					{
						$postSearchResult['szEmail'] = $kUser->szEmail ;
						if(!empty($postSearchResult['szEmail']))
						{
							//$notify_me_class = "class='orange-button1'";
							//$notify_me_onclick = "onclick=\"send_quotation_to_forwarder('".$szBookingRandomNum."')\"";
						}
					}
					?>
					<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>					
					<?
				}
				?>
				<div class="oh" <?=$emailfieldtopspace;?>>
					<p class="fl-20"><?=t($t_base.'fields/email');?></p>
					<p class="fl-80">
						<input type="text" id="szCustomerEmail_popup" name="addExpactedServiceAry[szEmail]" onkeyup = "enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>',false,this.id);" onblur="enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>',false,this.id);" value="<?=$postSearchResult['szEmail']?>">
						<span id="szCustomerEmail_popup_error" class="red_text" style="display:none;float:right;"></span>
					</p>
				</div>
				<div class="new-button-container-topgap">
				<p align="center" class="button-container">	
					<a href="javascript:void(0);" <?=$notify_me_class?> <?=$notify_me_onclick?> id="notify_me_button" ><span><?=t($t_base.'fields/send_my_requirement');?></span></a>
				</p>
				<p align="center" class="button-container">	
					<a href="javascript:void(0);" class="orange-button1" id="go_back_change_cargo_button" onclick="showHide('all_available_service');" ><span><?=t($t_base.'fields/go_back_and_change_cargo');?></span></a>
				</p>
				</div>
				<input type="hidden" name="booking_key" value="<?=$postSearchAry['szBookingRandomNum']?>">				
			</div>
		</form>
		</div>
		<?php
		}
		elseif($kConfig->iDangerousCargoFlag==1)
		{
			echo "ERROR_POPUP||||" ;
			$kWHSSearch = new cWHSSearch();
			$_SESSION['cargo_details_'.$idBooking] = $data ;
			
			$kWhsSearch = new cWHSSearch();
			$iLanguage = getLanguageId();
//			if($iLanguage==__LANGUAGE_ID_DANISH__)
//			{
//				$szVariableName = '__IS_DANGEROUS_CARGO_DANISH__' ;
//			}
//			else
//			{
				$szVariableName = '__IS_DANGEROUS_CARGO__' ;
			//}
			$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
			if(empty($szExplainUrl))
			{
				$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
			}
		?>
		<script type="text/javascript">
		$().ready(function(){
			 Custom.init();
		});
		</script>
		<div id="popup-bg" class="white-bg"></div>
		<div id="popup-container">
		<div id="loader" class="loader_popup_bg" style="display:none;">
			<div class="popup_loader"></div>				
				<img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
		</div>	
		<form id="dangerous_cargo_form" method="post" name="dangerous_cargo_form" action="">
			<div class="standard-popup-rk popup">	
				<p align="right" class="close-icon"><a heref="javascript:void(0);" onclick="reloadCargoDetail('<?=$szBookingRandomNum?>');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
				<h5><?=t($t_base.'title/rq1_popup_header')?></h5>
				<p><?=t($t_base.'title/rq1_popup_detail_text1');?> <a href="<?=$szExplainUrl?>" target="__blank" style="color:#FFFFFF;"><?=t($t_base.'title/rq1_popup_detail_text3')?></a>.</p>
				<p><?=t($t_base.'title/rq1_popup_detail_text2');?></p>
				<p><?=t($t_base.'title/rq1_popup_detail_text4');?></p>
				<p style="margin-top:14px;"><input class="styled" type="checkbox" value="1" name="addDangerCargoExpactedServiceAry[iSendNotification]" onblur="enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>','DC',this.id);" onclick ="enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>','DC',this.id);" id="iSendNotification_popup" > <?=t($t_base.'title/rk1_popup_checkbox_text');?></p>
				<?php
				$notify_me_class = "class='gray-button1'";
				if($_SESSION['user_id']<=0)
				{
					?>
					<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>					
					<div class="oh" style="margin-top:10px;">
						<p class="fl-20"><?=t($t_base.'fields/name');?></p>
						<p class="fl-80">
							<input type="text" id="szCustomerName_popup" onblur="enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>','DC',this.id);" onkeyup = "enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>','DC',this.id);" name="addDangerCargoExpactedServiceAry[szCustomerName]" value="<?=$postSearchResult['szCustomerName']?>">
							<span id="szCustomerName_popup_error" class="red_text" style="display:none;float:right;"></span>
						</p>
					</div>
					<?
				}
				else
				{
					$emailfieldtopspace="style='margin-top:10px;'";
					$kUser = new cUser();
					$kUser->getUserDetails($_SESSION['user_id']);
					if(empty($postSearchResult['szEmail']))
					{
						$postSearchResult['szEmail'] = $kUser->szEmail ;
						if(!empty($postSearchResult['szEmail']))
						{
							//$notify_me_class = "class='orange-button1'";
						//	$notify_me_onclick = "onclick=\"send_danger_cargo_email_forwarder('".$szBookingRandomNum."')\"";
						}
					}
					?>
					<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>					
					<?
				}
				?>
				<div class="oh" <?=$emailfieldtopspace;?>>
					<p class="fl-20"><?=t($t_base.'fields/email');?></p>
					<p class="fl-80">
						<input type="text" id="szCustomerEmail_popup" name="addDangerCargoExpactedServiceAry[szEmail]" onblur="enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>','DC',this.id);" onkeyup = "enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>','DC',this.id);" value="<?=$postSearchResult['szEmail']?>">
						<span id="szCustomerEmail_popup_error" class="red_text" style="display:none;float:right;"></span>
					</p>
				</div>
				<div class="new-button-container-topgap">
				<p align="center" class="button-container">		
					<a href="javascript:void(0);" <?=$notify_me_class?> <?=$notify_me_onclick?> id="notify_me_button" ><span><?=t($t_base.'fields/send_my_requirement');?></span></a>
				</p>
				<p align="center" class="button-container">	
					<a href="javascript:void(0);" class="orange-button1" id="go_back_change_cargo_button" onclick="showHide('all_available_service');" ><span><?=t($t_base.'fields/go_back_and_change_cargo');?></span></a>
				</p>
				</div>
				<input type="hidden" name="booking_key" value="<?=$postSearchAry['szBookingRandomNum']?>">				
			</div>
		</form>
		</div>
		<?php
		}
		else
		{
			$_SESSION['cargo_details_'.$idBooking] = array();
			unset($_SESSION['cargo_details_'.$idBooking]);
			$fTotalVolume = 0;					
			$fTotalWeight = 0;	
			// converting cargo details into cubic meter 			
			$counter_cargo = count($kConfig->iLength);
			for($i=1;$i<=$counter_cargo;$i++)
			{
				$fLength = $kConfig->iLength[$i] ;
				$fWidth = $kConfig->iWidth[$i] ;
				$fHeight = $kConfig->iHeight[$i] ;
				$iQuantity = $kConfig->iQuantity[$i] ;
				
				if($kConfig->idCargoMeasure[$i]==1)  // cm
				{
					$fTotalVolume += ($fLength/100) * ($fWidth/100) * ($fHeight/100) * $iQuantity;
				}
				else
				{
					$fCmFactor = $kWHSSearch->getCargoFactor($kConfig->idCargoMeasure[$i]);
					//echo "<br> cargo factor ".$fCmFactor ;
					
					if($fCmFactor>0)
					{
						$fTotalVolume += (($fLength/$fCmFactor )/100) * (($fWidth/$fCmFactor)/100) * (($fHeight/$fCmFactor)/100) * $iQuantity;
					}	
					//echo "<br> total volume ".$fTotalVolume ;
				}
			}
			
			$weight_cargo = count($kConfig->iWeight);
			for($i=1;$i<=$weight_cargo;$i++)
			{
				// converting cargo details into cubic meter 
				if($kConfig->idWeightMeasure[$i]==1)  // kg
				{
					$fTotalWeight += ($kConfig->iWeight[$i]);
				}
				else
				{
					$fKgFactor = $kWHSSearch->getWeightFactor($kConfig->idWeightMeasure[$i]);
					//echo "<br>  factor value ".$fKgFactor;
					if($fKgFactor>0)
					{
						$fTotalWeight += (($kConfig->iWeight[$i]/$fKgFactor ));
					}	
					//echo "<br> total weight ".$fTotalWeight ;
				}
			}
			
			if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
			{
				echo "POPUP||||" ;
				$t_base_booking_details = "BookingDetails/";
				echo display_booking_already_paid($t_base_booking_details,$idBooking);
				die;
			}
			
			$res_ary=array();
			$res_ary['fCargoVolume'] = $fTotalVolume;
			$res_ary['fCargoWeight']= ceil($fTotalWeight);
			$res_ary['iDoNotKnowCargo']= $data['iDoNotKnowCargo'];
			$res_ary['iFromRequirementPage']= 1;
			$res_ary['idBookingStatus'] = 1;
			$res_ary['dtCreatedOn'] = date('Y-m-d H:i:s');
			if((int)$postSearchAry['iBookingStep']<6)
			{
				$res_ary['iBookingStep'] = 6 ;
			}
			if(!empty($res_ary))
			{
				$update_query='';
				foreach($res_ary as $key=>$value)
				{
					$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
				}
			}				
			$update_query = rtrim($update_query,",");
			
			if($kBooking->updateDraftBooking($update_query,$idBooking))
			{
				if($kBooking->updateCargoDetails_requirement($kConfig,$idBooking))
				{
					if($_SESSION['user_id']>0)
					{
						$kBooking->checkAndDeleteMaxFiveDraft($_SESSION['user_id']);
					}
					$postSearchAry = array();
					$postSearchAry = $kBooking->getBookingDetails($idBooking);
					$redirect_url = __SELECT_SERVICES_URL__.'/'.$szBookingRandomNum.'/';
					echo "SUCCESS||||".$_SESSION['user_id']."||||".$redirect_url ;			
				}
			}
		}
	}
}
else if($mode == 'SERVICE_TYPE_EDIT')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$kConfig = new cConfig();
		$data=array();
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
	
		$res_ary=array();
		//$res_ary['szDestinationPostCode'] = '';
		//$res_ary['szOriginPostCode'] = '';
		
		//$res_ary['szOriginCity'] = '';
		//$res_ary['szDestinationCity'] = '';
		
		//$res_ary['fDestinationLatitude'] = '';
		//$res_ary['fDestinationLongitude'] = '';
		
		//$res_ary['fOriginLatitude'] = '';
	//	$res_ary['fOriginLongitude'] = '';
		
		$res_ary['iOriginCC'] = 0;
		$res_ary['iDestinationCC'] = 0;
		$res_ary['iCustomClearance'] = 0;
		$res_ary['fCargoVolume'] = 0;
		$res_ary['fCargoWeight'] = 0;
		$res_ary['idTimingType'] = 0;
		$res_ary['dtTimingDate'] = '';
		$res_ary['idServiceType'] = '';
		if((int)$postSearchAry['iBookingStep']<6)
		{
			$res_ary['iBookingStep'] = 6 ;
		}
		if(!empty($res_ary))
		{
			$update_query='';
			foreach($res_ary as $key=>$value)
			{
				$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
			}
		}
						
		$update_query = rtrim($update_query,",");
		
		if($kBooking->updateDraftBooking($update_query,$idBooking))
		{
			//$kBooking->deleteCargoByBookingId($idBooking);
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			$kWhsSearch = new cWHSSearch();
			$iAvailableServiceCount = $kWhsSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
			echo "SUCCESS||||";
			?>
			<div class="requirement-content">
				<a href="<?=$landing_page_url?>" class="act-question"><?=t($t_base_landing_page.'title/dont_want_to_answer_these_question');?></a>
			<?
			echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
			echo display_service_type_box_active($postSearchAry);
			echo "||||";
			echo city_postcode_box($postSearchAry);
			echo "||||";
			echo display_timing_box($postSearchAry);
			echo "||||";
			echo display_custom_clearance_box($postSearchAry);
			echo "||||";
			echo cargo_details_box_active($postSearchAry,true);
		}
	}
}
else if($mode == 'COUNTRY_BOX_EDIT')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$kConfig = new cConfig();
		$data=array();
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		
		if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
		{
			echo "POPUP||||" ;
			$t_base_booking_details = "BookingDetails/";
			echo display_booking_already_paid($t_base_booking_details,$idBooking);
			die;
		}
			
		$res_ary=array();
		/*
		$res_ary['idOriginCountry'] = 0;
		$res_ary['szOriginCountry'] = '';
		
		$res_ary['idDestinationCountry'] = 0;
		$res_ary['szDestinationCountry'] = '';
		*/
		$res_ary['szDestinationPostCode'] = '';
		$res_ary['szOriginPostCode'] = '';
		
		$res_ary['szOriginCity'] = '';
		$res_ary['szDestinationCity'] = '';
		
		$res_ary['fDestinationLatitude'] = '';
		$res_ary['fDestinationLongitude'] = '';
		$res_ary['iBookingStep'] = 1 ;
		$res_ary['fOriginLatitude'] = '';
		$res_ary['fOriginLongitude'] = '';
		
		$res_ary['idTimingType'] = '';
		$res_ary['dtTimingDate'] = '';
		$res_ary['iOriginCC'] = '';
		$res_ary['iDestinationCC'] = '';
		$res_ary['iCustomClearance'] = 0;
		
		$res_ary['idServiceType'] = '';
		if(!empty($res_ary))
		{
			$update_query='';
			foreach($res_ary as $key=>$value)
			{
				$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
			}
		}				
		$update_query = rtrim($update_query,",");
		
		//echo "<br />".$update_query;
		if($kBooking->updateDraftBooking($update_query,$idBooking))
		{
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			$redirect_url = __REQUIREMENT_PAGE_URL__.'/'.$szBookingRandomNum.'/';
			$kConfig = new cConfig();
			$iLanguage = getLanguageId();
			$countryAry=$kConfig->getAllCountries(false,$iLanguage);
			
			echo "SUCCESS||||";
			echo display_country_drop_down_active($countryAry,$postSearchAry);
			echo "||||";
			echo display_service_type_box_default($postSearchAry);
			echo "||||";
			echo city_postcode_box($postSearchAry,true);
			echo "||||";
			echo display_timing_box($postSearchAry,true);
			echo "||||";
			echo display_custom_clearance_box($postSearchAry,true);
			echo "||||";
			echo cargo_details_box_active($postSearchAry,true);
		}
	}
}
else if($mode == 'CITY_POSTCODE_EDIT')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$kConfig = new cConfig();
		$data=array();
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		
		if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
		{
			echo "POPUP||||" ;
			$t_base_booking_details = "BookingDetails/";
			echo display_booking_already_paid($t_base_booking_details,$idBooking);
			die;
		}
			
		$res_ary=array();
		$res_ary['iOriginCC'] = 0;
		$res_ary['iDestinationCC'] = 0;
		$res_ary['iCustomClearance'] = 0;
		$res_ary['fCargoVolume'] = 0;
		$res_ary['fCargoWeight'] = 0;
		$res_ary['idTimingType'] = 0;
		$res_ary['dtTimingDate'] = '';
		if((int)$postSearchAry['iBookingStep']<3)
		{
			$res_ary['iBookingStep'] = 3 ;
		}
		if(!empty($res_ary))
		{
			$update_query='';
			foreach($res_ary as $key=>$value)
			{
				$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
			}
		}				
		$update_query = rtrim($update_query,",");
		
		if($kBooking->updateDraftBooking($update_query,$idBooking))
		{
			//$kBooking->deleteCargoByBookingId($idBooking);
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			$idServiceType = $postSearchAry['idServiceType'];
			
			echo "SUCCESS||||";
			echo display_service_image($postSearchAry,$idServiceType,$szBookingRandomNum);
			echo "||||";
			echo city_postcode_active_box($postSearchAry);
			echo "||||";
			echo display_timing_box($postSearchAry);
			echo "||||";
			echo display_custom_clearance_box($postSearchAry);
			echo "||||";
			echo cargo_details_box_active($postSearchAry,true);
		}
	}
}
else if($mode == 'CARGO_TIMING_EDIT')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$kConfig = new cConfig();
		$data=array();
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		
		if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
		{
			echo "POPUP||||" ;
			$t_base_booking_details = "BookingDetails/";
			echo display_booking_already_paid($t_base_booking_details,$idBooking);
			die;
		}
		
		$res_ary=array();
		$res_ary['iOriginCC'] = 0;
		$res_ary['iDestinationCC'] = 0;
		$res_ary['iCustomClearance'] = 0;
		$res_ary['fCargoVolume'] = 0;
		$res_ary['fCargoWeight'] = 0;
		if((int)$postSearchAry['iBookingStep']<4)
		{
			$res_ary['iBookingStep'] = 4 ;
		}
		if(!empty($res_ary))
		{
			$update_query='';
			foreach($res_ary as $key=>$value)
			{
				$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
			}
		}				
		$update_query = rtrim($update_query,",");
		
		if($kBooking->updateDraftBooking($update_query,$idBooking))
		{
			//$kBooking->deleteCargoByBookingId($idBooking);
			
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			$idServiceType = $postSearchAry['idServiceType'];
			echo "SUCCESS||||";
			echo display_service_image($postSearchAry,$idServiceType,$szBookingRandomNum);
			echo "||||";
			echo city_postcode_box($postSearchAry);
			echo "||||";
			echo display_timing_box_active($postSearchAry);
			echo "||||";
			echo display_custom_clearance_box($postSearchAry);
			echo "||||";
			echo cargo_details_box_active($postSearchAry,true);
		}
	}
}
else if($mode == 'CUSTOM_CLEARANCE_EDIT')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$kConfig = new cConfig();
		$data=array();
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		
		$idServiceType = $postSearchAry['idServiceType'];
		echo "SUCCESS||||";
		echo display_service_image($postSearchAry,$idServiceType,$szBookingRandomNum);
		echo "||||";
		echo city_postcode_box($postSearchAry);
		echo "||||";
		echo display_timing_box($postSearchAry);
		echo "||||";
		echo display_custom_clearance_box_active($postSearchAry);
		echo "||||";
		echo cargo_details_box_active($postSearchAry,true);
	}
}
else if($mode == 'CARGO_DETAILS_EDIT')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$kConfig = new cConfig();
		$data=array();
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		
			$idServiceType = $postSearchAry['idServiceType'];
			echo "SUCCESS||||";
			echo display_service_image($postSearchAry,$idServiceType,$szBookingRandomNum);
			echo "||||";
			echo city_postcode_box($postSearchAry);
			echo "||||";
			echo display_timing_box($postSearchAry);
			echo "||||";
			echo display_custom_clearance_box($postSearchAry);
			echo "||||";
			echo cargo_details_box_active($postSearchAry);
	}
}
else if($mode == 'COUNTRY_BOX_EDIT_POPUP')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		echo "SUCCESS||||";
		echo display_country_change_popup($postSearchAry,$szBookingRandomNum);
	}
}
else if($mode == 'SERVICE_TYPE_EDIT_POPUP')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		echo "SUCCESS||||";
		echo display_service_change_popup($postSearchAry,$szBookingRandomNum);
	}
}
else if($mode == 'CITY_POSTCODE_EDIT_POPUP')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		echo "SUCCESS||||";
		echo display_city_postcode_change_popup($postSearchAry,$szBookingRandomNum);
	}
}
else if($mode == 'DISPLAY_TIMING_DESCRIPTION')
{
	$t_base = "LandingPage/";
	
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	$idTimingType = sanitize_all_html_input(trim($_REQUEST['idTimingType']));
	$dtTiming = sanitize_all_html_input(trim($_REQUEST['dtTiming']));
	$kWHSSearch = new cWHSSearch();
	if($idBooking>0)
	{
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		
		if($idTimingType==1) // Ready at origin
		{
			/*			
			* mindate = max(Today ; Max dtValidTo minus the above 14 days)
			* maxdate = largest dtValidTo in tblservices 
			* */
			$icutoffDays =(int) $kWHSSearch->getManageMentVariableByDescription('__CUTOFF_N_DAYS__');
			$iMaxExpiryDate = $kWHSSearch->getMaxExpiryDay($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],$postSearchAry['idServiceType']);
			
			echo "SUCCESS||||";
			$before_14_day_date = date('Y-m-d H:i:s', strtotime($iMaxExpiryDate. ' - '.$icutoffDays.' days'));
			$before_14_day_time = strtotime($before_14_day_date);
			$iLanguage = getLanguageId();
			if($before_14_day_time > time() )
			{
				$iMonth = date("m",$before_14_day_time);
				$szMonthName = getMonthName($iLanguage,$iMonth);
				
				$minDate = date("j.",$before_14_day_time)." ".$szMonthName;
				$minDateTime = $before_14_day_time ;
			}
			else
			{
				$iMonth = (int)date("m");
				$szMonthName = getMonthName($iLanguage,$iMonth);
				$minDate = date("j.")." ".$szMonthName;
				$minDateTime = time();
			}	
			
			$maxDateTime = strtotime($iMaxExpiryDate);
			/*
			* If in any odd situation date at Line1(Min Date) is greater then date at Line 2(Max date) then date line1=line2
			* */
			if($minDateTime > $maxDateTime)
			{
				$minDateTime = $maxDateTime ;
				$iMonth = (int)date("m",$minDateTime);
				$szMonthName = getMonthName($iLanguage,$iMonth);
				$minDate = date("j.",$minDateTime)." ".$szMonthName;
			}
			
			$iMonth = (int)date("m",$maxDateTime);
			$szMonthName = getMonthName($iLanguage,$iMonth);
				
			$maxDate = date("j.",$maxDateTime)." ".$szMonthName;
			$titiming_type_description_text =  t($t_base.'title/date_after')." ".$minDate." ".t($t_base.'title/are_relevant').":<br/>".t($t_base.'title/we_show')." ".$icutoffDays." ".t($t_base.'title/days_window_service_after')." ".$maxDate." ".t($t_base.'title/not_yet_available');
			
			if(!empty($dtTiming))
			{
				$dtTimingAry = explode("/",$dtTiming);
				$valid_date = $dtTimingAry[2]."-".$dtTimingAry[1]."-".$dtTimingAry[0]." 00:00:00";
				$postSearchAry['dtTimingDate'] = $valid_date ;
			}
			else
			{
				$postSearchAry['dtTimingDate'] = date('Y-m-d H:i:s');
			}
			
			echo display_timing_box_active($postSearchAry,$titiming_type_description_text,$minDateTime,$maxDateTime,$idTimingType);
		}
		else if($idTimingType==2) // Available at destination 
		{
			/*			
			* mindate = today + minimum(iTransitTime for relevant services from tbl services) + Management Variable 5
			* maxdate = Maximum(dtValidTo+iTransitTime)
			* 
			* */
		
			$iAvailableDays =(int) $kWHSSearch->getManageMentVariableByDescription('__PICK_UP_AFTER_M_DAYS__');
			
			$iMinTransitTime = $kWHSSearch->getMinTransitDaysFromAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],$postSearchAry['idServiceType']);
			
			$iMinTransitDay = ceil($iMinTransitTime/24);
			$expacted_date = date('Y-m-d H:i:s', strtotime('+ '.$iMinTransitDay.' days'));		
			$expacted_date = date('Y-m-d H:i:s', strtotime($expacted_date.' + '.$iAvailableDays.' days'));
	
			
			$expacted_date_time = strtotime($expacted_date);
			
			echo "SUCCESS||||";
			
			$iMaxExpiryDate = $kWHSSearch->getMaxExpiryDay($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],$postSearchAry['idServiceType']);
			
			$iMonth = (int)date("m",$expacted_date_time);
			$szMonthName = getMonthName($iLanguage,$iMonth);
			
			$minDate = date("j.",$expacted_date_time)." ".$szMonthName;
			
			$iMonth = (int)date("m",strtotime($iMaxExpiryDate." + ".$iMinTransitDay." days"));
			$szMonthName = getMonthName($iLanguage,$iMonth);
			
			$maxDate = date("j.",strtotime($iMaxExpiryDate." + ".$iMinTransitDay." days"))." ".$szMonthName;
			
			$minDateTime = $expacted_date_time ;
			$maxDateTime = strtotime($iMaxExpiryDate." + ".$iMinTransitDay." days");
			
			$titiming_type_description_text = t($t_base.'title/see_first_available_service')." ".$minDate." <br/>".t($t_base.'title/service_after')." ".$maxDate." ".t($t_base.'title/not_yet_available');
			
			if(!empty($dtTiming))
			{
				$dtTimingAry = explode("/",$dtTiming);
				$valid_date = $dtTimingAry[2]."-".$dtTimingAry[1]."-".$dtTimingAry[0]." 00:00:00";
				$valid_date_time = strtotime($valid_date);
				if($valid_date_time < $expacted_date_time)
				{
					$postSearchAry['dtTimingDate'] = date('Y-m-d H:i:s',$expacted_date_time);
				}
				else
				{
					$postSearchAry['dtTimingDate'] = $valid_date ;
				}
			}
			else
			{
				$postSearchAry['dtTimingDate'] = date('Y-m-d H:i:s',$expacted_date_time);
			}
			echo display_timing_box_active($postSearchAry,$titiming_type_description_text,$minDateTime,$maxDateTime,$idTimingType);
		}
	}
}
else if($mode == 'CARGO_TIMING_EDIT_POPUP')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		echo "SUCCESS||||";
		echo display_cargo_timing_change_popup($postSearchAry,$szBookingRandomNum);
	}
}
else if($mode == 'CUSTOM_CLEARANCE_EDIT_POPUP')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		echo "SUCCESS||||";
		echo display_custom_clearance_change_popup($postSearchAry,$szBookingRandomNum);
	}
}
else if(!empty($_REQUEST['addExpactedServiceAry']))
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if($kBooking->sendCargoExceedEmail($_REQUEST['addExpactedServiceAry'],$postSearchAry))
		{
			echo "SUCCESS||||";
			$landing_page_url = __HOME_PAGE_URL__ ;
			echo display_d3_popup($landing_page_url,false,$postSearchAry);
			die;
		}
	}
}
else if(!empty($_REQUEST['addDangerCargoExpactedServiceAry']))
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	$landing_page_url = __HOME_PAGE_URL__ ;
	if($idBooking>0)
	{
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		$landing_page_url = __HOME_PAGE_URL__ ;
	}
	if($kBooking->sendDangerousCargoEmail($_REQUEST['addDangerCargoExpactedServiceAry'],$postSearchAry))
	{
		echo "SUCCESS||||";
		echo display_d3_popup($landing_page_url,false,$postSearchAry);
		die;
	}
}
else if($mode == 'DON_NOT_KNOW_POPUP')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	$iOriginDestination = (int)$_REQUEST['iOriginDestination'];
	if($idBooking>0)
	{
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		echo "SUCCESS||||";
		echo display_OP1_OP2_popup($postSearchAry,$szBookingRandomNum,$iOriginDestination);
	}
}
else if($mode == 'CARGO_TIMING_UPDATE')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	
	if($idBooking>0)
	{
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		$dtTimingDate = sanitize_all_html_input(trim($_REQUEST['dtTiming']));
		$idTimingType = sanitize_all_html_input(trim($_REQUEST['type_id']));
		
		if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
		{
			echo "POPUP||||" ;
			$t_base_booking_details = "BookingDetails/";
			echo display_booking_already_paid($t_base_booking_details,$idBooking);
			die;
		}
		
		$res_ary=array();
		$res_ary['dtTimingDate'] = date('Y-m-d H:i:s',$dtTimingDate); ;
		$res_ary['idTimingType']= $idTimingType;
		if((int)$postSearchAry['iBookingStep']<5)
		{
			$res_ary['iBookingStep'] = 5 ;
		}
		if(!empty($res_ary))
		{
			$update_query='';
			foreach($res_ary as $key=>$value)
			{
				$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
			}
		}				
		$update_query = rtrim($update_query,",");
		
		if($kBooking->updateDraftBooking($update_query,$idBooking))
		{
			$postSearchAry = array();
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			
			$kWhsSearch = new cWHSSearch();
			$kWhsSearch->isCustomClearanceExist($postSearchAry);
			
			
			echo "SUCCESS||||";
			echo display_timing_box($postSearchAry);
			echo "||||";
			if($kWhsSearch->iOriginCC==1 || $kWhsSearch->iDestinationCC==1)
			{
				echo "CC_EXISTS";
				echo "||||";
				echo display_custom_clearance_box_active($postSearchAry);
			}
			else
			{
				echo "NO_CC_EXIST";
				echo "||||";
				echo cargo_details_box_active($postSearchAry);
			}
		}
	}
}
else if($mode == 'MULTI_PARTIAL_EMPTY_POSTCODE_NEXT_PAGE')
{	
	$searchAry = array();
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	
	if($idBooking>0)
	{
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		$iOriginDestination = sanitize_all_html_input(trim($_POST['szOriginDestination']));
		$szRegion1 = sanitize_all_html_input(trim($_POST['szRegion1']));
		
		if($iOriginDestination=='ORIGIN')
		{
			$searchAry['idCountry'] = $postSearchAry['idOriginCountry'];
			$searchAry['szCity'] =sanitize_all_html_input(trim($_POST['szCityName']));
			
			$iPageNum = (int)$_POST['page_num'];
			
			$from = ($iPageNum-1)*__POSTCODE_PER_PAGE_POSTCODE__;
			
			$cityPostcodeAry = $kConfig->check_city_country_requirement($searchAry['szCity'],$searchAry['idCountry'],$from,true,false,$szRegion1);
			
			$total = $kConfig->check_city_country_requirement($searchAry['szCity'],$searchAry['idCountry'],false,false,true,$szRegion1);
			
			if($total>500)
			{
				//$total = 500;
			}
			$kPagination  = new Pagination($total,__POSTCODE_PER_PAGE_POSTCODE__,__PARTIAL_EMPTY_POSTCODE_LINKS_PER_PAGE__);
			$kPagination->paginate();
			echo "SUCCESS||||";
			if(!empty($cityPostcodeAry))
			{	
				echo '<div class="container2" id="boxscroll2">';
				foreach($cityPostcodeAry as $key=>$fromRegions)
				{
					?>
					<p class="checkbox-ab"><input type="radio" onclick="enable_submit_button('partial_empty_postcode_from_form','partialEmptyPostcodeCityAry[szRegionFrom]')" name="partialEmptyPostcodeCityAry[szRegionFrom]" value="<?=$key?>"><?=$fromRegions?></p>
					<?php
				}
				echo '</div>';
			}
			echo '<div class="page_nav" align="right">';
			if($kPagination->links_per_page>1)
			{
				$kPagination->ajax_function = "show_next_page";
				$kPagination->append = "'MULTI_PARTIAL_EMPTY_POSTCODE_NEXT_PAGE'";
				$kPagination->page = $iPageNum ;
				$kPagination->iIncludeDots = 1 ;
				echo $kPagination->renderFullNav();
			}
			echo '</div>';
		}
		else
		{			
			$searchAry['idCountry'] = $postSearchAry['idDestinationCountry'];
			$searchAry['szCity'] =sanitize_all_html_input(trim($_POST['szCityName']));
			
			$iPageNum = (int)$_POST['page_num'];
			
			$from = ($iPageNum-1)*__POSTCODE_PER_PAGE_POSTCODE__;
			
			$cityPostcodeAry = $kConfig->check_city_country_requirement($searchAry['szCity'],$searchAry['idCountry'],$from,true,false,$szRegion1);
			$total = $kConfig->check_city_country_requirement($searchAry['szCity'],$searchAry['idCountry'],false,false,true,$szRegion1);
			
			$kPagination  = new Pagination($total,__POSTCODE_PER_PAGE_POSTCODE__,__PARTIAL_EMPTY_POSTCODE_LINKS_PER_PAGE__);
			$kPagination->paginate();
			echo "SUCCESS||||";
			if(!empty($cityPostcodeAry))
			{	
				echo '<div class="container2" id="boxscroll2">';
				foreach($cityPostcodeAry as $key=>$fromRegions)
				{
					?>
					<p class="checkbox-ab"><input type="radio" onclick="enable_submit_button('partial_empty_postcode_from_form','partialEmptyPostcodeCityAry[szRegionTo]')" name="partialEmptyPostcodeCityAry[szRegionTo]" value="<?=$key?>"><?=$fromRegions?></p>
					<?php
				}
				echo '</div>';
			}
			echo '<div class="page_nav" align="right">';
		
			if($kPagination->links_per_page>1)
			{
				$kPagination->ajax_function = "show_next_page";
				$kPagination->append = "'MULTI_PARTIAL_EMPTY_POSTCODE_NEXT_PAGE'";
				$kPagination->page = $iPageNum ;
				$kPagination->iIncludeDots = 1 ;
				echo $kPagination->renderFullNav();
			}
			echo '</div>';
		}
	}
}

if($mode=='RELOAD_CARGO_DETAILS')
{
	$searchAry = array();
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	
	if($idBooking>0)
	{
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		$cargoAry = $_SESSION['cargo_details_'.$idBooking];
		echo "SUCCESS||||";
		echo cargo_details_box_active($postSearchAry,false,true);
		die;
	}
}
if($mode=='VALIDATE_POSTCODE_AFTER_HAULAGE_POPUP')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);	
	$szUpdateServiceFlag = sanitize_all_html_input(trim($_REQUEST['update_srvice_flag']));
	
	if($idBooking>0)
	{
		$res_ary=array();
		$res_ary['idServiceType']= sanitize_all_html_input(trim($_REQUEST['type_id']));
		
		if(!empty($res_ary))
		{
			$update_query = '';
			foreach($res_ary as $key=>$value)
			{
				$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
			}
		}
		
		$idServiceType = sanitize_all_html_input(trim($_REQUEST['type_id']));
		$update_query = rtrim($update_query,",");
		if($kBooking->updateDraftBooking($update_query,$idBooking))
		{
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			
			$szOriginDestination = sanitize_all_html_input(trim($_REQUEST['szOriginDestination']));
			$szCityName = sanitize_all_html_input(trim($_REQUEST['szCityName']));
				
			if(!empty($szCityName) && $szOriginDestination=='ORIGIN')
			{
				$postSearchAry = $kBooking->getBookingDetails($idBooking);
				
				$idDestinationCountry = $postSearchAry['idDestinationCountry'];
				$idServiceType = $postSearchAry['idServiceType'];
				$szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
				
				$szOriginCityPostcode='';
				if(!empty($postSearchAry['szOriginPostCode']))
				{
					$szOriginCityPostcode = $postSearchAry['szOriginPostCode'];
				}	
				else if(!empty($postSearchAry['szOriginCity']))
				{
					$szOriginCityPostcode = $postSearchAry['szOriginCity'] ;
				}
				
				if(($idServiceType == __SERVICE_TYPE_DTD__) || ($idServiceType == __SERVICE_TYPE_WTD__) || ($idServiceType == __SERVICE_TYPE_PTD__ ))
				{
					//Last true parameter forcing to first check multiregion then go for next level comparison
					$fromCityAry = $kConfig->check_city_country_requirement($szCityName,$idDestinationCountry,false,false,false,false,true);
					
					if(($kConfig->emptyPostcodeCtr == 1 || $kConfig->partialEmptyPostcodeCtr==1) && !empty($fromCityAry))
					{
						$res_ary['idDestinationPostCode'] = $kConfig->idEmptyPostCode;
						
						if($kConfig->comparePostcode($kConfig->szDestinationCity,$kConfig->szPostCode))
						{
							$res_ary['szDestinationPostCode'] = $kConfig->szPostCode;
							$res_ary['szDestinationCity'] = $kConfig->szCity;
						}
						else
						{
							$res_ary['szDestinationPostCode'] = $kConfig->szPostCode;
							$res_ary['szDestinationCity'] = $kConfig->szCity;
						}					
						$res_ary['fDestinationLatitude'] = $kConfig->szLatitude;
						$res_ary['fDestinationLongitude'] = $kConfig->szLongitude;
						if((int)$postSearchAry['iBookingStep']<4)
						{
							$res_ary['iBookingStep'] = 4 ;
						}
						if(!empty($res_ary))
						{
							$update_query = '';
							foreach($res_ary as $key=>$value)
							{
								$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
							}
						}					
						$update_query = rtrim($update_query,",");
						if($kBooking->updateDraftBooking($update_query,$idBooking))
						{
							//TO DO
							$postSearchAry = $kBooking->getBookingDetails($idBooking);
							$idServiceType = $postSearchAry['idServiceType'];
							$szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
							if($kWHSSearch->isHaulageExists($postSearchAry,2))
							{
								echo "POPUP||||";
								echo display_destination_haulage_not_exist_popup($postSearchAry);
								echo "||||";
								echo display_service_image($postSearchAry,$idServiceType,$szBookingRandomNum);
								die;
							}
							else
							{
								echo "SUCCESS||||";
								echo display_service_image($postSearchAry,$idServiceType,$szBookingRandomNum);
								echo "||||";
								echo city_postcode_box($postSearchAry);
								echo "||||";
								echo display_timing_box_active($postSearchAry);
								echo "||||";
								echo display_custom_clearance_box($postSearchAry);
								die;
							}
						}
					}
					else if($kConfig->iMultiRegionFlag == 1)
					{
						echo "MULTIREGIO||||";
						$szDestinationCity = $szCityName;
						echo display_multiregion_popup($postSearchAry, $fromCityAry, 'DESTINATION', $szDestinationCity);
						echo "||||";
						echo display_service_image($postSearchAry,$idServiceType,$szBookingRandomNum);
						die;
					}
					else if($kConfig->emptyPostcodeCtr > 1 && !empty($fromCityAry))
					{
						echo "MULTI_EMPTY_POSTCODE||||";
						$szDestinationCity = $szCityName;
						echo display_empty_postcode_popup($postSearchAry,$fromCityAry, 'DESTINATION', $szDestinationCity);
						echo "||||";
						echo display_service_image($postSearchAry,$idServiceType,$szBookingRandomNum);
						die;
					}
					else if($kConfig->partialEmptyPostcodeCtr > 1 && !empty($fromCityAry))
					{
						echo "MULTI_PARTIAL_EMPTY_POSTCODE||||" ;
						$szDestinationCity = $szCityName;
						echo display_partial_empty_postcode_popup($postSearchAry,$fromCityAry, 'DESTINATION', $szDestinationCity);
						echo "||||";
						echo display_service_image($postSearchAry,$idServiceType,$szBookingRandomNum);
						die;
					}
				}
				else
				{
					$szDestinationCity = $szCityName ;
					$toCityAry = $kConfig->isCityExistsByCountry_requirement($szDestinationCity,$idDestinationCountry) ;
					if(count($toCityAry)>1)
					{
						echo "MULTIREGIO||||";
						echo display_multiregion_popup($postSearchAry, $toCityAry, 'DESTINATION', $szDestinationCity);
						die;
					}
					else
					{
						$res_ary = array();
						$res_ary['idDestinationPostCode'] = $kConfig->idPostCode;
						
						if($kConfig->comparePostcode($szDestinationCity,$kConfig->szPostCode))
						{
							$res_ary['szDestinationPostCode'] = $kConfig->szPostCode;
							$res_ary['szDestinationCity'] = $kConfig->szCity;
						}
						else
						{
							$res_ary['szDestinationPostCode'] = '';
							$res_ary['szDestinationCity'] = $kConfig->szCity;
						}
						$res_ary['fDestinationLatitude'] = $kConfig->szLatitude;
						$res_ary['fDestinationLongitude'] = $kConfig->szLongitude;
						if((int)$postSearchAry['iBookingStep']<4)
						{
							$res_ary['iBookingStep'] = 4 ;
						}
						if(!empty($res_ary))
						{
							$update_query = '';
							foreach($res_ary as $key=>$value)
							{
								$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
							}
						}
							
						$update_query = rtrim($update_query,",");
						if($kBooking->updateDraftBooking($update_query,$idBooking))
						{
							$postSearchAry = $kBooking->getBookingDetails($idBooking);
							$idServiceType = $postSearchAry['idServiceType'];
							$szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
							
							echo "SUCCESS||||";
							echo display_service_image($postSearchAry,$idServiceType,$szBookingRandomNum);
							echo "||||";
							echo display_timing_box_active($postSearchAry);
							echo "||||";
							echo city_postcode_active_box($postSearchAry,true);
							echo "||||";
							echo display_custom_clearance_box($postSearchAry);
							die;
						}
					}
				}
			}
			else if(!empty($szCityName) && $szOriginDestination=='DESTINATION')
			{
				$destinationPostCodeAry=array();
				$destinationPostCodeAry = $kConfig->getPostCodeDetails_requirement($postSearchAry['idDestinationCountry'],$szCityName);
				
				$res_ary = array();
				$res_ary['fDestinationLatitude']= round((float)$destinationPostCodeAry['szLatitude'],5); 
				$res_ary['fDestinationLongitude']= round((float)$destinationPostCodeAry['szLongitude'],5);
				
				$res_ary['szDestinationCity']= $szCityName ;
				if((int)$postSearchAry['iBookingStep']<4)
				{
					$res_ary['iBookingStep'] = 4 ;
				}
				if(!empty($res_ary))
				{
					$update_query = '';
					foreach($res_ary as $key=>$value)
					{
						$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
					}
				}					
				$update_query = rtrim($update_query,",");
				$kBooking->updateDraftBooking($update_query,$idBooking);
				
				$postSearchAry = $kBooking->getBookingDetails($idBooking);
				
				echo "SUCCESS||||";
				echo display_service_image($postSearchAry,$idServiceType,$szBookingRandomNum);
				echo "||||";
				echo city_postcode_box($postSearchAry);
				echo "||||";
				echo display_timing_box_active($postSearchAry);
				echo "||||";
				echo display_custom_clearance_box($postSearchAry);
			}
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking ID. ";
		
	}
}
?>