//04-04-2017 @Ajay

CREATE TABLE IF NOT EXISTS `tblwarehousetype` (
`id` int(11) NOT NULL,
  `szTypeCode` varchar(255) NOT NULL,
  `szFriendlyName` varchar(255) NOT NULL,
  `iActive` int(11) NOT NULL DEFAULT '1',
  `dtCreatedOn` datetime NOT NULL
);

INSERT INTO `tblwarehousetype` (`id`, `szTypeCode`, `szFriendlyName`, `iActive`, `dtCreatedOn`) VALUES
(1, 'CFS', 'CFS', 1, '2017-04-04 00:00:00'),
(2, 'AIR', 'Airport warehouse', 1, '2017-04-04 00:00:00');

ALTER TABLE `tblwarehousetype` ADD PRIMARY KEY (`id`);
ALTER TABLE `tblwarehousetype` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;

ALTER TABLE `tblwarehouses` ADD `iWarehouseType` INT NOT NULL DEFAULT '1' ; 
ALTER TABLE `tblwarehousestempdata` ADD `iWarehouseType` INT NOT NULL DEFAULT '1' ;
