ALTER TABLE `tblbookings` ADD `szVATRegistration` VARCHAR( 100 ) NOT NULL AFTER `iInTransitEmailSend`;
ALTER TABLE `tblclonebookings` ADD `szVATRegistration` VARCHAR( 100 ) NOT NULL AFTER `iInTransitEmailSend`; 


INSERT INTO `cms_sections` (`id`, `szFriendlyName`, `section_title`, `subject`, `section_description`, `szSubjectDanish`, `szSectionDescriptionDanish`, `status`, `created`, `updated`, `iLanguage`, `iUserType`, `iSequence`, `iShowInForwarderOnly`)
VALUES (NULL, 'Forwarder - Markup Booking Cancelled', '__FORWARDER_MARKUP_CANCEL_BOOKING_EMAIL__', '', '', '', '', '1', '2017-03-07 12:39:06', '2017-03-07 12:39:10', '1', '2', '', '0');

INSERT INTO `cms_sections_mapping` (`idEmailTemplate`, `subject`, `section_description`, `idLanguage`, `updated`) VALUES
('118', 'szBookingType booking cancelled on Transporteca - szBookingRef', '<div align="left">Your booking szBookingRef on Transporteca has been cancelled. Please find enclosed the original booking notice, for your easy convenience.<br><br>szCancelReason Please do not invoice us for this booking. If you have any questions, please call us on +45 7199 7299 or just respond to this e-mail.<br><br>Kind regards,<br><br>Transporteca<br></div>', 1, '2017-03-07 15:07:50');


ALTER TABLE `tblemaillog` ADD `dtReadMessageTaskToRead` DATETIME NOT NULL AFTER `iInvalidHtmlTags`; 
ALTER TABLE `tbldashboardgraph` ADD `iReadMessageResponseTime` FLOAT( 8, 2 ) NOT NULL AFTER `iRFQSearchResponseTime` ;

ALTER TABLE `tblforwardertransactions` ADD `szSelfInvoice` VARCHAR( 100 ) NOT NULL AFTER `dtPaymentScheduled`;

CREATE TABLE IF NOT EXISTS `tblnocustomsclearance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idFromCountry` int(11) NOT NULL,
  `idToCountry` int(11) NOT NULL,
  `szFromCountryName` varchar(150) NOT NULL,
  `szFromRegionName` varchar(150) NOT NULL,
  `szToCountryName` varchar(150) NOT NULL,
  `szToRegionName` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE `tblnocustomsclearance` ADD `isDeleted` TINYINT( 1 ) NOT NULL DEFAULT '0' AFTER `szToRegionName`; 

ALTER TABLE `tblresponsetimetracker` CHANGE `iSearchType` `iSearchType` INT( 11 ) NOT NULL COMMENT '1.Automatic booking 2. RFQ 3. Voga';

ALTER TABLE `tblnocustomsclearance` DROP `szFromCountryName` ,
DROP `szFromRegionName` ,
DROP `szToCountryName` ,
DROP `szToRegionName` ;

INSERT INTO `tblpagepermission` (`id`, `idMenu`, `szPageName`, `szFriendlyName`, `iType`) VALUES
 (NULL, '6', '__SYSTEM_CUSTOMER_CLEARANCE__', 'Customer Clearance', '2');

INSERT INTO `tblpagepermission` (`id`, `idMenu`, `szPageName`, `szFriendlyName`, `iType`) VALUES
 (NULL, '6', '__RAIL_TRANSPORT__', 'Rail Transport', '2');


ALTER TABLE `tblcountry` ADD `szDefaultCourierPostcode` VARCHAR( 50 ) NOT NULL ,
ADD `szDefaultCourierCity` VARCHAR( 255 ) NOT NULL ;


--27-Mar-2017
CREATE TABLE `tbltransportecaofficehours` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`idWeekDay` INT NOT NULL ,
`dtOpenGMT` TIME NOT NULL ,
`dtCloseGMT` TIME NOT NULL
) ENGINE = MYISAM CHARACTER SET utf8 COLLATE utf8_general_ci;

INSERT INTO `tbltransportecaofficehours` (`id`, `idWeekDay`, `dtOpenGMT`, `dtCloseGMT`) VALUES
(1, 1, '07:30:00', '15:30:00'),
(2, 2, '07:30:00', '15:30:00'),
(3, 3, '07:30:00', '15:30:00'),
(4, 4, '07:30:00', '15:30:00'),
(5, 5, '07:30:00', '15:30:00');

--05-Apr-17 @Ashish
ALTER TABLE `tblbookings` ADD `iCreditDays` INT NOT NULL; 
ALTER TABLE `tblbookings` ADD `szForwarderVatRegistrationNum` VARCHAR( 100 ) NOT NULL;
ALTER TABLE `tblemaillog` ADD `iTotalClearTime` FLOAT( 8, 2 ) NOT NULL AFTER `idAdminAddedBy`;
ALTER TABLE `tblcustomerlogsnippet` ADD `idAdminAddedBy` INT NOT NULL ;
ALTER TABLE `tblcustomerlogsnippet` ADD `iTotalClearTime` FLOAT( 8, 2 ) NOT NULL;
ALTER TABLE `tblcustomerlogsnippet` ADD `dtReadMessageTaskToRead` DATETIME NOT NULL;

ALTER TABLE `tblresponsetimetracker` ADD `iTotalClearTime` FLOAT( 8, 2 ) NOT NULL AFTER `idAdminAddedBy` ;

ALTER TABLE `tblemaillog` ADD INDEX `idBooking_iSeen_iIncomingEmail` ( `idBooking` , `iSeen` , `iIncomingEmail` ); 

ALTER TABLE `tblcountry` ADD `iUsePostcodes` TINYINT( 1 ) NOT NULL DEFAULT '1' AFTER `szDefaultCourierCity`; 

ALTER TABLE `tblpartnerapiresponse` ADD `szCollection` VARCHAR( 10 ) NOT NULL ,
ADD `szDelivery` VARCHAR( 10 ) NOT NULL ,
ADD `szShipperPostcodeRequired` VARCHAR( 10 ) NOT NULL ,
ADD `szConsigneePostcodeRequired` VARCHAR( 10 ) NOT NULL ;

ALTER TABLE `tblclonebookings` ADD `iCreditDays` INT NOT NULL; 
ALTER TABLE `tblclonebookings` ADD `szForwarderVatRegistrationNum` VARCHAR( 100 ) NOT NULL;


--19-Apr-2017--
ALTER TABLE `tblbookingfilestaskqueues` ADD `dtDeleted` DATETIME NOT NULL AFTER `szDeveloperNotes` ,
ADD `szDeletedNotes` VARCHAR( 255 ) NOT NULL AFTER `dtDeleted`;

ALTER TABLE `tblbookingfilestaskqueues` CHANGE `dtCreatedOn` `dtCreatedOn` DATETIME NOT NULL;  


--01-May-2017
ALTER TABLE `tbllanguageconfiguration` ADD `iType` TINYINT(1) NOT NULL DEFAULT '0' AFTER `idLanguage`;
UPDATE `tblhaulagemodels` SET `szModelLongName` = 'Pricing based on distance' WHERE `tblhaulagemodels`.`id` =4;

--04-May-2017
CREATE TABLE IF NOT EXISTS `tblautomatedrfqresponse` (
  `id` int(11) NOT NULL,
  `szResponse` varchar(512) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `dtCreated` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `tblautomatedrfqresponse`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `tblautomatedrfqresponse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `tblautomatedrfqresponse` ADD `szCode` VARCHAR(100) NOT NULL AFTER `szResponse`;
ALTER TABLE `tblcustomlandingpages` ADD `szAutomatedRfqResponseCode` VARCHAR(50) NOT NULL AFTER `idInsuranceCurrency`;

--08-May-2017
INSERT INTO `tblerrordetails` (`szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`) VALUES ('iLanguage', 'E1196', 'INVALID', '4');
INSERT INTO `tblerrordetails` (`szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`) VALUES ('iDataSet', 'E1197', 'INVALID', '4');
INSERT INTO `tblerrordetails` (`id`, `szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`) VALUES (NULL, 'iStandardCargo', 'E1198', 'NOT_FOUND', '9');
INSERT INTO `tblerrordetails` (`szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`) VALUES ('szAutomatedRfqResponseCode', 'E1199', 'REQUIRED', '5');
INSERT INTO `tblerrordetails` (`szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`) VALUES ('szAutomatedRfqResponseCode', 'E1200', 'INVALID', '4');

--10-May-2017
ALTER TABLE `tblpartnerapicall` ADD `iVogaSearchAutomatic` TINYINT(1) NOT NULL DEFAULT '0' AFTER `iShipmentType`,
ADD `iSearchStandardCargo` TINYINT(1) NOT NULL DEFAULT '0' AFTER `iVogaSearchAutomatic`;
ALTER TABLE `tblpartnerapicall` ADD `idDefaultLandingPage` INT NOT NULL AFTER `iSearchStandardCargo`;
ALTER TABLE `tblpartnerapicall` ADD `iSearchMiniVesion` INT NOT NULL AFTER `iSearchStandardCargo`;

--12-May-2017
ALTER TABLE `tblcustomer` ADD `iSignUp` TINYINT(1) NOT NULL DEFAULT '0' AFTER `szBrowser`;
UPDATE `tblcustomer` SET `iSignUp`='1' WHERE `dtLastLogin`<>'0000-00-00 00:00:00';

ALTER TABLE `tblpartnerapicargodetail` ADD `szSellersReference` VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `dtCreatedOn`;
ALTER TABLE `tblpartnerapicall` ADD `szInternalComments` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `idDefaultLandingPage`;


--15-May-2017
ALTER TABLE `tblpartnerapicall` ADD `iShipperDetailsLocked` TINYINT(1) NOT NULL DEFAULT '0' AFTER `szInternalComments`, ADD `iConsigneeDetailsLocked` TINYINT(1) NOT NULL DEFAULT '0' AFTER `iShipperDetailsLocked`, ADD `iCargoDescriptionLocked` TINYINT(1) NOT NULL DEFAULT '0' AFTER `iConsigneeDetailsLocked`;

ALTER TABLE `tblclonebookings` ADD `iShipperDetailsLocked` TINYINT(1) NOT NULL DEFAULT '0',
 ADD `iConsigneeDetailsLocked` TINYINT(1) NOT NULL DEFAULT '0' AFTER `iShipperDetailsLocked`,
 ADD `iCargoDescriptionLocked` TINYINT(1) NOT NULL DEFAULT '0' AFTER `iConsigneeDetailsLocked`;


ALTER TABLE `tblbookings` ADD `iShipperDetailsLocked` TINYINT(1) NOT NULL DEFAULT '0',
 ADD `iConsigneeDetailsLocked` TINYINT(1) NOT NULL DEFAULT '0' AFTER `iShipperDetailsLocked`,
 ADD `iCargoDescriptionLocked` TINYINT(1) NOT NULL DEFAULT '0' AFTER `iConsigneeDetailsLocked`;

ALTER TABLE `tblpartnerapicall` ADD `szAutomatedRfqResponseCode` VARCHAR(50) NOT NULL;
ALTER TABLE `tblpartnerapicall` ADD `iCreateAutomaticQuote` TINYINT(1) NOT NULL DEFAULT '0';

ALTER TABLE `tblclonebookings` ADD `szAutomatedRfqResponseCode` VARCHAR(50) NOT NULL;
ALTER TABLE `tblbookings` ADD `szAutomatedRfqResponseCode` VARCHAR(50) NOT NULL;


INSERT INTO `tbllanguageconfiguration` (`idMapped`, `szFieldName`, `szValue`, `szTableKey`, `idLanguage`) VALUES
(1, 'szDay_txt', 'days', '__SITE_STANDARD_TEXT__', 1),
(1, 'szDay_txt', 'dage', '__SITE_STANDARD_TEXT__', 2),
(1, 'szDay_txt', 'dage', '__SITE_STANDARD_TEXT__', 3),
(1, 'szDay_txt', 'dage', '__SITE_STANDARD_TEXT__', 4),
(1, 'szDay_txt', 'dage', '__SITE_STANDARD_TEXT__', 5),
(1, 'szDay_txt', 'dage', '__SITE_STANDARD_TEXT__', 6),
(1, 'szDay_txt', 'dage', '__SITE_STANDARD_TEXT__', 7),
(1, 'szDay_txt', 'dage', '__SITE_STANDARD_TEXT__', 8),
(1, 'szDay_txt', 'dage', '__SITE_STANDARD_TEXT__', 9),
(1, 'szDay_txt', 'dage', '__SITE_STANDARD_TEXT__', 10),
(1, 'szDay_txt', 'dage', '__SITE_STANDARD_TEXT__', 11),
(1, 'szDay_txt', 'dage', '__SITE_STANDARD_TEXT__', 12);


INSERT INTO `tblerrordetails` (`szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`) VALUES ('fCargoValue', 'E1218', 'GREATER_THEN', '2');


ALTER TABLE `tblbookings` CHANGE `iCreateAutomaticQuote` `iCheckHandlingFeeForAutomaticQuote` INT(11) NOT NULL;
ALTER TABLE `tblbookings` CHANGE `iCreateVogaAutomaticQuote` `iCreateAutomaticQuote` INT(11) NOT NULL;


ALTER TABLE `tblclonebookings` CHANGE `iCreateAutomaticQuote` `iCheckHandlingFeeForAutomaticQuote` INT(11) NOT NULL;
ALTER TABLE `tblclonebookings` CHANGE `iCreateVogaAutomaticQuote` `iCreateAutomaticQuote` INT(11) NOT NULL;

ALTER TABLE `tblbookings` ADD `iRemovePriceGuarantee` TINYINT(1) NOT NULL DEFAULT '0';
ALTER TABLE `tblclonebookings` ADD `iRemovePriceGuarantee` TINYINT(1) NOT NULL DEFAULT '0';
ALTER TABLE `tblpartnerapicall` ADD `iRemovePriceGuarantee` TINYINT(1) NOT NULL DEFAULT '0';


--22-May-2017
ALTER TABLE `tblbookings` ADD `iPaymentTypePartner` TINYINT(1) NOT NULL DEFAULT '0';
ALTER TABLE `tblclonebookings` ADD `iPaymentTypePartner` TINYINT(1) NOT NULL DEFAULT '0';
ALTER TABLE `tblpartnerapicall` ADD `iPaymentTypePartner` TINYINT(1) NOT NULL DEFAULT '0';
 
ALTER TABLE `tblbookings` ADD INDEX `iCreateAutomaticQuote_DTCREATED` ( `iCreateAutomaticQuote`,`dtCreatedOn`);

INSERT INTO `tblerrordetails` (`id`, `szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`, `iInternalErrorCode`) VALUES (NULL, 'szPriceCurrency', 'E1222', 'INVALID', '4', '');

--26-May-2017
ALTER TABLE `tblquotepricingdetails` ADD `szHandoverCity` VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `iPriceAddedByAddButton`;

--29-May-2017
INSERT INTO `tbllanguageconfiguration` (`idMapped`, `szFieldName`, `szValue`, `szTableKey`, `idLanguage`) VALUES
(1, 'szHandoverCityText', 'Handover City', '__SITE_STANDARD_TEXT__', 1),
(1, 'szHandoverCityText', 'Vi stiller prisgaranti', '__SITE_STANDARD_TEXT__', 2),
(1, 'szHandoverCityText', 'Vi stiller prisgaranti', '__SITE_STANDARD_TEXT__', 3),
(1, 'szHandoverCityText', 'Handover City', '__SITE_STANDARD_TEXT__', 4),
(1, 'szHandoverCityText', 'Handover City', '__SITE_STANDARD_TEXT__', 5),
(1, 'szHandoverCityText', 'Handover City', '__SITE_STANDARD_TEXT__', 6),
(1, 'szHandoverCityText', 'Handover City', '__SITE_STANDARD_TEXT__', 7),
(1, 'szHandoverCityText', 'Handover City', '__SITE_STANDARD_TEXT__', 8),
(1, 'szHandoverCityText', 'Handover City', '__SITE_STANDARD_TEXT__', 9),
(1, 'szHandoverCityText', 'Handover City', '__SITE_STANDARD_TEXT__', 10),
(1, 'szHandoverCityText', 'Handover City', '__SITE_STANDARD_TEXT__', 11),
(1, 'szHandoverCityText', 'Handover City', '__SITE_STANDARD_TEXT__', 12);


INSERT INTO `tblerrordetails` (`id`, `szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`, `iInternalErrorCode`) VALUES (NULL, 'szLanguage', 'E1223', 'INVALID', '4', '0');
INSERT INTO `tblerrordetails` (`id`, `szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`, `iInternalErrorCode`) VALUES (NULL, 'szLanguage', 'E1224', 'REQUIRED', '5', '0');


ALTER TABLE `tblstandardcargohandling` CHANGE `szQuoteEmailSubject` `szQuoteEmailSubject` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, CHANGE `szQuoteEmailBody` `szQuoteEmailBody` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

--31-May-2017
INSERT INTO `tbllanguageconfiguration` (`idMapped`, `szFieldName`, `szValue`, `szTableKey`, `idLanguage`) VALUES
(1, 'szClickHereText_txt', 'Click here', '__SITE_STANDARD_TEXT__', 1),
(1, 'szClickHereText_txt', 'Klik her', '__SITE_STANDARD_TEXT__', 2),
(1, 'szClickHereText_txt', 'klicka här', '__SITE_STANDARD_TEXT__', 3),
(1, 'szClickHereText_txt', 'Click here', '__SITE_STANDARD_TEXT__', 4),
(1, 'szClickHereText_txt', 'Click here', '__SITE_STANDARD_TEXT__', 5),
(1, 'szClickHereText_txt', 'Click here', '__SITE_STANDARD_TEXT__', 6),
(1, 'szClickHereText_txt', 'Click here', '__SITE_STANDARD_TEXT__', 7),
(1, 'szClickHereText_txt', 'Click here', '__SITE_STANDARD_TEXT__', 8),
(1, 'szClickHereText_txt', 'Click here', '__SITE_STANDARD_TEXT__', 9),
(1, 'szClickHereText_txt', 'Click here', '__SITE_STANDARD_TEXT__', 10),
(1, 'szClickHereText_txt', 'Click here', '__SITE_STANDARD_TEXT__', 11),
(1, 'szClickHereText_txt', 'Click here', '__SITE_STANDARD_TEXT__', 12);


INSERT INTO `tbllanguageconfiguration` (`idMapped`, `szFieldName`, `szValue`, `szTableKey`, `idLanguage`) VALUES
(1, 'szTrackShippingRefText_txt', 'TRACK SHIPPING REFERENCE', '__SITE_STANDARD_TEXT__', 1),
(1, 'szTrackShippingRefText_txt', 'SE STATUS PÅ', '__SITE_STANDARD_TEXT__', 2),
(1, 'szTrackShippingRefText_txt', 'SE STATUS PÅ', '__SITE_STANDARD_TEXT__', 3),
(1, 'szTrackShippingRefText_txt', 'TRACK SHIPPING REFERENCE', '__SITE_STANDARD_TEXT__', 4),
(1, 'szTrackShippingRefText_txt', 'TRACK SHIPPING REFERENCE', '__SITE_STANDARD_TEXT__', 5),
(1, 'szTrackShippingRefText_txt', 'TRACK SHIPPING REFERENCE', '__SITE_STANDARD_TEXT__', 6),
(1, 'szTrackShippingRefText_txt', 'TRACK SHIPPING REFERENCE', '__SITE_STANDARD_TEXT__', 7),
(1, 'szTrackShippingRefText_txt', 'TRACK SHIPPING REFERENCE', '__SITE_STANDARD_TEXT__', 8),
(1, 'szTrackShippingRefText_txt', 'TRACK SHIPPING REFERENCE', '__SITE_STANDARD_TEXT__', 9),
(1, 'szTrackShippingRefText_txt', 'TRACK SHIPPING REFERENCE', '__SITE_STANDARD_TEXT__', 10),
(1, 'szTrackShippingRefText_txt', 'TRACK SHIPPING REFERENCE', '__SITE_STANDARD_TEXT__', 11),
(1, 'szTrackShippingRefText_txt', 'TRACK SHIPPING REFERENCE', '__SITE_STANDARD_TEXT__', 12);

--01-June-2017
INSERT INTO `tblerrordetails` (`id`, `szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`, `iInternalErrorCode`) VALUES (NULL, 'szName', 'E1225', 'REQUIRED', '5', '0');

--07-June-2017
CREATE TABLE IF NOT EXISTS `tblpartnerrawdatalog` (
  `id` int(11) NOT NULL,
  `szReferenceToken` varchar(255) NOT NULL,
  `idPartner` int(11) NOT NULL,
  `szServiceId` varchar(100) NOT NULL,
  `szQuoteID` varchar(100) NOT NULL,
  `szMethod` varchar(100) NOT NULL,
  `szData` longtext NOT NULL,
  `dtCreated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `tblpartnerrawdatalog`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tblpartnerrawdatalog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `tblbookings` ADD `iSuccessValidatePrice` TINYINT(1) NOT NULL DEFAULT '0';
ALTER TABLE `tblclonebookings` ADD `iSuccessValidatePrice` TINYINT(1) NOT NULL DEFAULT '0';


--14-June-2017---
INSERT INTO `tblerrordetails` (`szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`) VALUES ('iQuantity', 'E1226', 'INVALID', '4');
ALTER TABLE `tblcomparebutton` ADD `szReferenceToken` varchar(100) NOT NULL;

--16-June-2017--
INSERT INTO `tblpagepermission` (`id`, `idMenu`, `szPageName`, `szFriendlyName`, `iType`)
VALUES (NULL, '6', '__API_ERROR_LOG__', 'Error messages', '2');


--23-June-2017--
INSERT INTO `tblmanagementvar` (`id`, `szDescription`, `szValue`, `szDeveloperNotes`, `iActive`, `szFriendlyName`, `iLanguage`) VALUES 
(NULL, '__LCL_SERVICE_VIDEO__', 'https://youtu.be/6VOFeKGsdjE', 'LCL Service Video', '1', 'LCL Service Video', '1'),
(NULL, '__HAULAGE_VIDEO__', 'https://youtu.be/8CZlvFiVYcY', 'Haulage Video', '1', 'Haulage Video', '1'),
(NULL, '__LCL_SERVICE_BULK_VIDEO__', 'https://youtu.be/mI3lfWHN9uA', 'LCL Service Bulk Video', '1', 'LCL Service Bulk Video', '1'),
(NULL, '__HAULAGE_BULK_VIDEO__', 'https://youtu.be/m34l7hiGGFE', 'Haulage Bulk Video', '1', 'Haulage Bulk Video', '1'),
(NULL, '__CUSTOM_CLEARANCE_BULK_VIDEO__', 'https://youtu.be/f4UaBJKZXIY', 'Custom Clearance Bulk Video', '1', 'Custom Clearance Bulk Video', '1');