<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

$szMetaTitle= __HOW_IT_WORK_PAGE_META_TITLE__;
$szMetaKeywords = __HOW_IT_WORK_PAGE_META_KEYWORDS__;
$szMetaDescription = __HOW_IT_WORK_PAGE_META_DESCRIPTION__;
$style="style='top: 0px; bottom: auto; display: block;'";
$headerSearchFlag=1;
require_once( __APP_PATH_LAYOUT__ . "/header_new.php" );
$KwarehouseSearch=new cWHSSearch();
$iLanguage = getLanguageId();
//if($iLanguage ==__LANGUAGE_ID_DANISH__)
//{
//    $fwdWork=$KwarehouseSearch->getManageMentVariableByDescription(__WORK_FOR_FORWARDER_DANISH__);
//}
//else
//{
    $fwdWork=$KwarehouseSearch->getManageMentVariableByDescription(__WORK_FOR_FORWARDER__,$iLanguage);
//}

$fwdWork = html_entity_decode($fwdWork);
$t_base="ForwardWorks/"
?>
<div id="hsbody" class="link16">
    <h1><?=t($t_base.'title/how_does_work_forward')?> ?</h1><br/>
    <?php if($fwdWork){    echo refineDescriptionData($fwdWork);      } ?> 
    <?php /*<p><a href="<?=__BASE_URL__?>/home/"><?=t($t_base.'title/booking_link')?></a></p>*/ ?>
</div> 
<?php
include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>