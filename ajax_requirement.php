<?php
/**
 * Customer Login
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

$t_base = "LandingPage/";
$t_base_landing_page = "LandingPage/";
$mode = sanitize_all_html_input(trim($_REQUEST['mode']));
$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['szBookingRandomNum']));

$kConfig=new cConfig();
$kWHSSearch = new cWHSSearch();
$kBooking = new cBooking();
if($mode=='SHIPPER')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{			
			$res_ary=array();			
			$res_ary['iShipperConsignee']= 1;
			$res_ary['idServiceType']= '';
			if(!empty($res_ary))
			{
				$update_query = '';
				foreach($res_ary as $key=>$value)
				{
					$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
				}
			}
			
			$update_query = rtrim($update_query,",");
			
			if($kBooking->updateDraftBooking($update_query,$idBooking))
			{
				
			}
			//check if DTP is available or not. 
			$iAvailableServiceCount = $kWHSSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iDTP'))
			{
				echo "SUCCESS|||| ";
				
				$kWhsSearch = new cWHSSearch();
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__OPTION_EXPORT_HAULAGE_NOT_DEFINED_DANISH__' ;
//				}
//				else
//				{
                                    $szVariableName = '__OPTION_EXPORT_HAULAGE_NOT_DEFINED__' ;
				//}
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}
				
				?>
				<div class="requirement-content">
				<!-- <h3>P1</h3> -->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="2">
					<li><?=t($t_base.'title/P1_box_title_1');?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/P1_box_title_2');?>?</li>
				</ol>
				</div> <!-- closing div for requirement-content -->
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_P1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/P1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_K1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/P1_box_option_B');?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank" class="orange-button1"><span><?=t($t_base.'title/P1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:50%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="#" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?php
			}
			else
			{
				echo "SUCCESS|||| ";
				$originCountryText = get_country_name($postSearchAry['szOriginCountry'],'ORIGIN');
				
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__OPTION_EXPORT_HAULAGE_NOT_DEFINED_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__OPTION_EXPORT_HAULAGE_NOT_DEFINED__' ;
				//}
				
				$kWhsSearch = new cWHSSearch();
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}	
				
				?>
				<div class="requirement-content">
				<!-- <h3>K1</h3> -->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="2">
					<li><?=t($t_base.'title/k1_box_title_1');?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/k1_box_title_2');?>?</li>
				</ol>
				</div> <!-- closing div for requirement-content -->
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_K1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/k1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_K1B','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/k1_box_option_B');?> <?=$originCountryText?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank" class="orange-button1"><span><?=t($t_base.'title/k1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:50%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascrip:void(0);" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?php
			}
		}
		else
		{
			echo "ERROR|||| 2Invalid booking id ";
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_P1A')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{
			//check if DTP is available or not. 
			$iAvailableServiceCount = $kWHSSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iDTD'))
			{
				echo "SUCCESS|||| ";
				$userType = check_user_type($postSearchAry);
				if($userType=='SHIPPER')
				{
					$step_count = 2;
					$proces_bar_width = __PROCESS_BAR_WIDTH_TWO__;
				}
				else
				{
					$step_count = 3;
					$proces_bar_width = __PROCESS_BAR_WIDTH_THREE__;
				}
				
				$destinationCountryText = get_country_name($postSearchAry['szDestinationCountry'],'DESTINATION');
				
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED_DANISH__' ;
//				}
//				else
//				{
                                    $szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED__' ;
				//}

				$kWhsSearch = new cWHSSearch();
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}
				
				?>
				<div class="requirement-content">
				<!-- <h3>E1</h3> -->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/e1_box_title_1');?> <?=$postSearchAry['szDestinationCountry']?>?</li>
				</ol>
				</div> <!-- closing div for requirement-content -->
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_PE1A','<?=$szBookingRandomNum?>');"  class="orange-button1"><span><?=t($t_base.'title/e1_box_option_A');?> <?=$destinationCountryText?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_PE1B','<?=$szBookingRandomNum?>');"  class="orange-button1"><span><?=t($t_base.'title/e1_box_option_B');?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank"  class="orange-button1"><span><?=t($t_base.'title/e1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?
			}
			else if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iPTD'))
			{
				echo "SUCCESS|||| ";
				
				$userType = check_user_type($postSearchAry);
				if($userType=='SHIPPER')
				{
					$step_count = 2;
					$proces_bar_width = __PROCESS_BAR_WIDTH_TWO__;
				}
				else
				{
					$step_count = 3;
					$proces_bar_width = __PROCESS_BAR_WIDTH_THREE__;
				}
				$destinationCountryText = get_country_name($postSearchAry['szDestinationCountry'],'DESTINATION');
				$originCountryText = get_country_name($postSearchAry['szOriginCountry'],'ORIGIN');
				?>
				<div class="requirement-content">
				<!--<h3>N1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/n1_box_title_1');?> <?=$postSearchAry['szDestinationCountry']?>? <?=t($t_base.'title/n1_box_title_2');?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/n1_box_title_3');?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/n1_box_title_4');?> <?=$postSearchAry['szOriginCountry']?>.</li>
				</ol>
				</div> <!-- closing div for requirement-content -->
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_N1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/n1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_N1B','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/n1_box_option_B_1');?> <?=$destinationCountryText?> - <?=t($t_base.'title/n1_box_option_B_2');?> <?=$originCountryText?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_N1C','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/n1_box_option_C_1');?> <?=$originCountryText?> <?=t($t_base.'title/n1_box_option_C_2');?> <?=$destinationCountryText?></span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="#" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?php
			}
			else
			{
				echo "SUCCESS|||| ";
				$userType = check_user_type($postSearchAry);
				if($userType=='SHIPPER')
				{
					$step_count = 2;
					$proces_bar_width = __PROCESS_BAR_WIDTH_TWO__;
				}
				else
				{
					$step_count = 3;
					$proces_bar_width = __PROCESS_BAR_WIDTH_THREE__;
				}
				$destinationCountryText = get_country_name($postSearchAry['szDestinationCountry'],'DESTINATION');
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED__' ;
				//}
				
				$kWhsSearch = new cWHSSearch();
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}
				
				?>
				<div class="requirement-content">
				<!--<h3>H1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/h1_box_title_1');?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/h1_box_title_2');?>?</li>
				</ol>
				</div> <!-- closing div for requirement-content -->
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_PH1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/h1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_PH1B','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/h1_box_option_B');?> <?=$destinationCountryText?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank" class="orange-button1"><span><?=t($t_base.'title/h1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="#" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?php
			}
		}
		else
		{
			echo "ERROR|||| Invalid booking id ";
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_K1A')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{
			//check if DTP is available or not. 
			$iAvailableServiceCount = $kWHSSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iPTD'))
			{
				echo "SUCCESS|||| ";
				
				$userType = check_user_type($postSearchAry);
				if($userType=='SHIPPER')
				{
					$step_count = 2;
					$proces_bar_width = __PROCESS_BAR_WIDTH_TWO__;
				}
				else
				{
					$step_count = 3;
					$proces_bar_width = __PROCESS_BAR_WIDTH_THREE__;
				}
				$destinationCountryText = get_country_name($postSearchAry['szDestinationCountry'],'DESTINATION');
				
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED__' ;
				//}
				
				$kWhsSearch = new cWHSSearch();
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}
				
				?>
				<div class="requirement-content">
				<!--<h3>E1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/e1_box_title_1');?> <?=$postSearchAry['szDestinationCountry']?>?</li>
				</ol>
				</div> <!-- closing div for requirement-content -->
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_E1A','<?=$szBookingRandomNum?>');"  class="orange-button1"><span><?=t($t_base.'title/e1_box_option_A');?> <?=$destinationCountryText?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_E1B','<?=$szBookingRandomNum?>');"  class="orange-button1"><span><?=t($t_base.'title/e1_box_option_B');?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank"  class="orange-button1"><span><?=t($t_base.'title/e1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="#" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?php
			}
			else
			{
				echo "SUCCESS|||| ";
				
				$userType = check_user_type($postSearchAry);
				if($userType=='SHIPPER')
				{
					$step_count = 2;
					$proces_bar_width = __PROCESS_BAR_WIDTH_TWO__;
				}
				else
				{
					$step_count = 3;
					$proces_bar_width = __PROCESS_BAR_WIDTH_THREE__;
				}
				$destinationCountryText = get_country_name($postSearchAry['szDestinationCountry'],'DESTINATION');
				
				$kWhsSearch = new cWHSSearch();
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED__' ;
				//}
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}
				?>
				<div class="requirement-content">
				<!--<h3>H1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/h1_box_title_1');?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/h1_box_title_2');?>?</li>
				</ol>
				</div> <!-- closing div for requirement-content -->
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_H1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/h1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_H1B','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/h1_box_option_B');?> <?=$destinationCountryText?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="__blank" class="orange-button1"><span><?=t($t_base.'title/h1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?
			}
		}
		else
		{
			echo "ERROR|||| Invalid booking id ";
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_K1B')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{
			echo "POPUP||||";
			$res_ary=array();
			$res_ary['idServiceType'] = __SERVICE_TYPE_DTW__ ;
			if($_SESSION['user_id']>0)
			{
				$kUser = new cUser();
				$kUser->getUserDetails($_SESSION['user_id']);
				$res_ary['idUser'] = $kUser->id ;				
				$res_ary['szFirstName'] = $kUser->szFirstName ;
				$res_ary['szLastName'] = $kUser->szLastName ;
				$res_ary['szEmail'] = $kUser->szEmail ;	
				$res_ary['szCustomerAddress1'] = $kUser->szAddress1;
				$res_ary['szCustomerAddress2'] = $kUser->szAddress2;
				$res_ary['szCustomerAddress3'] = $kUser->szAddress3;
				$res_ary['szCustomerPostCode'] = $kUser->szPostcode;
				$res_ary['szCustomerCity'] = $kUser->szCity;
				$res_ary['szCustomerCountry'] = $kUser->szCountry;
				$res_ary['szCustomerState'] = $kUser->szState;
				$res_ary['szCustomerPhoneNumber'] = $kUser->szPhoneNumber;
				$res_ary['szCustomerCompanyName'] = $kUser->szCompanyName;	
				$res_ary['szCustomerCompanyRegNo'] = $kUser->szCompanyRegNo;			
			}
			
			if(!empty($res_ary))
			{
				foreach($res_ary as $key=>$value)
				{
					$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
				}
			}
			$update_query = rtrim($update_query,",");
			if($kBooking->updateDraftBooking($update_query,$idBooking))
			{
				//echo "SUCCESS";
			}
			$kBooking->addExpactedServiceData($idBooking);
			$idExpactedService = $kBooking->idExpactedService ;
			echo display_F3_F4_popup($postSearchAry,__SERVICE_TYPE_DTW__,$idExpactedService);
		}
		else
		{
			echo "ERROR|||| Invalid booking id ";
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_H1A' || $mode == 'OPTION_E1B')
{
	$t_base_landing_page = "LandingPage/";
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTP'))
		{
			echo "SUCCESS|||| ";
			?>
				<div class="requirement-content-final">
				<!--<h3>Q3</h3>-->
				<p class="trans-box-complete"><?=t($t_base_landing_page.'title/q3_header_text')?></p>
				<?
						echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_WTP__,false,'q3');
				?>
				</div>
				<div class="process-bar process-completed">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_WTP__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
				</div>
			<?
		}
		else
		{
			echo "SUCCESS|||| ";
			?>
			<div class="requirement-content-final">
			<!--<h3>Q4</h3>-->
			<p class="trans-box-complete"><?=t($t_base_landing_page.'title/q4_header_text')?></p>
					<?
						echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_PTP__,false,'q4');
					?>
			</div>
				<div class="process-bar process-completed">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_PTP__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
			</div>
			<?
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_PE1A')
{
	$t_base_landing_page = "LandingPage/";
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		echo "SUCCESS|||| ";
		?>
		<div class="requirement-content-final">
		<!--<h3>Q2</h3>-->
		<p class="trans-box-complete"><?=t($t_base_landing_page.'title/q2_header_text_1')." ".$postSearchAry['szOriginCountry']." ".t($t_base_landing_page.'title/q2_header_text_2')." ".$postSearchAry['szDestinationCountry']."".t($t_base_landing_page.'title/q2_header_text_3')?></p>
		
		<?
			echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_DTD__);
		?>
		
		</div>
		<div class="process-bar process-completed">
			<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
			<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_DTD__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
		</div>
		<?
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_PH1A' || $mode == 'OPTION_PE1B')
{
	$t_base_landing_page = "LandingPage/";
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		echo "SUCCESS|||| ";
		$szOriginText = t($t_base_landing_page.'title/shipper_in')." ".$postSearchAry['szOriginCountry'];
		$szDestinationText = t($t_base_landing_page.'title/consignee_in')." ".$postSearchAry['szDestinationCountry'];
		?>
		<div class="requirement-content-final">
		<!--<h3>Q1</h3>-->
		<p class="trans-box-complete"><?=t($t_base_landing_page.'title/ph1_header_text_1')." ".$postSearchAry['szOriginCountry']." ".t($t_base_landing_page.'title/ph1_header_text_2')?></p>
		<?
				echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_DTP__,false,'q1');
		?>
		</div>
		<div class="process-bar process-completed">
			<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
			<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_DTP__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
		</div>
		<?
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_PH1B' || $mode == 'OPTION_YCL1H1B' || $mode == 'OPTION_YCG1H1B' || $mode == 'OPTION_YCH1B' || $mode == 'OPTION_H1B') 
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);	
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{
			echo "POPUP||||";
			$res_ary=array();
			$res_ary['idServiceType'] = __SERVICE_TYPE_DTD__ ;
			if($_SESSION['user_id']>0)
			{
				$kUser = new cUser();
				$kUser->getUserDetails($_SESSION['user_id']);
				$res_ary['idUser'] = $kUser->id ;				
				$res_ary['szFirstName'] = $kUser->szFirstName ;
				$res_ary['szLastName'] = $kUser->szLastName ;
				$res_ary['szEmail'] = $kUser->szEmail ;	
				$res_ary['szCustomerAddress1'] = $kUser->szAddress1;
				$res_ary['szCustomerAddress2'] = $kUser->szAddress2;
				$res_ary['szCustomerAddress3'] = $kUser->szAddress3;
				$res_ary['szCustomerPostCode'] = $kUser->szPostcode;
				$res_ary['szCustomerCity'] = $kUser->szCity;
				$res_ary['szCustomerCountry'] = $kUser->szCountry;
				$res_ary['szCustomerState'] = $kUser->szState;
				$res_ary['szCustomerPhoneNumber'] = $kUser->szPhoneNumber;
				$res_ary['szCustomerCompanyName'] = $kUser->szCompanyName;	
				$res_ary['szCustomerCompanyRegNo'] = $kUser->szCompanyRegNo;			
			}
			
			if(!empty($res_ary))
			{
				foreach($res_ary as $key=>$value)
				{
					$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
				}
			}
			$update_query = rtrim($update_query,",");
			if($kBooking->updateDraftBooking($update_query,$idBooking))
			{
				//echo "SUCCESS";
			}
			$kBooking->addExpactedServiceData($idBooking);
			$idExpactedService = $kBooking->idExpactedService ;
			echo display_F1_popup($postSearchAry,__SERVICE_TYPE_DTD__,$idExpactedService,$mode);
		}
		else
		{
			echo "ERROR|||| Invalid booking id ";
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_N1A')
{
	$t_base_landing_page = "LandingPage/";
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		echo "SUCCESS|||| ";
	?>
		<div class="requirement-content-final">
		<!--<h3>Q1</h3>-->
		<p class="trans-box-complete"><?=t($t_base_landing_page.'title/ph1_header_text_1')." ".$postSearchAry['szOriginCountry']." ".t($t_base_landing_page.'title/ph1_header_text_2')?></p>
		<?
				echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_DTP__,false,'q1');
		?>
		</div>
		<div class="process-bar process-completed">
			<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
			<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_DTP__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
		</div>
	<?
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_N1B' || $mode == 'OPTION_E1A')
{
	$t_base_landing_page = "LandingPage/";
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{
			//check if DTP is available or not. 
			$iAvailableServiceCount = $kWHSSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTD'))
			{
				echo "SUCCESS|||| ";
				?>
				<div class="requirement-content-final">
				<!--<h3>Q5</h3>-->
				<p class="trans-box-complete"><?=t($t_base_landing_page.'title/n1b_header_text_1')." ".$postSearchAry['szDestinationCountry']."".t($t_base_landing_page.'title/n1b_header_text_2')?></p>
				<?
						echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_WTD__);
				?>				
				</div>
				<div class="process-bar process-completed">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_WTD__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
				</div>
				<?
			}
			else
			{
				echo "SUCCESS|||| ";
				?>
				<div class="requirement-content-final">
				<!--<h3>Q6</h3>-->
				<p class="trans-box-complete"><?=t($t_base_landing_page.'title/n1b_header_text_3')." ".$postSearchAry['szDestinationCountry']."".t($t_base_landing_page.'title/n1b_header_text_4')?></p>
				<?
						echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_PTD__,false,'q6');
				?>				
				</div>
				<div class="process-bar process-completed">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_PTD__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
				</div>
				<?php
			}
		}
		else
		{
			echo "ERROR|||| Invalid booking id ";
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_N1C')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{
			echo "POPUP||||";
			
			$res_ary=array();
			$res_ary['idServiceType'] = __SERVICE_TYPE_DTD__ ;
			if($_SESSION['user_id']>0)
			{
				$kUser = new cUser();
				$kUser->getUserDetails($_SESSION['user_id']);
				$res_ary['idUser'] = $kUser->id ;				
				$res_ary['szFirstName'] = $kUser->szFirstName ;
				$res_ary['szLastName'] = $kUser->szLastName ;
				$res_ary['szEmail'] = $kUser->szEmail ;	
				$res_ary['szCustomerAddress1'] = $kUser->szAddress1;
				$res_ary['szCustomerAddress2'] = $kUser->szAddress2;
				$res_ary['szCustomerAddress3'] = $kUser->szAddress3;
				$res_ary['szCustomerPostCode'] = $kUser->szPostcode;
				$res_ary['szCustomerCity'] = $kUser->szCity;
				$res_ary['szCustomerCountry'] = $kUser->szCountry;
				$res_ary['szCustomerState'] = $kUser->szState;
				$res_ary['szCustomerPhoneNumber'] = $kUser->szPhoneNumber;
				$res_ary['szCustomerCompanyName'] = $kUser->szCompanyName;	
				$res_ary['szCustomerCompanyRegNo'] = $kUser->szCompanyRegNo;			
			}
			
			if(!empty($res_ary))
			{
				foreach($res_ary as $key=>$value)
				{
					$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
				}
			}
			$update_query = rtrim($update_query,",");
			if($kBooking->updateDraftBooking($update_query,$idBooking))
			{
				//echo "SUCCESS";
			}
			$kBooking->addExpactedServiceData($idBooking);
			$idExpactedService = $kBooking->idExpactedService ;
			echo display_F11_F12_popup($postSearchAry,__SERVICE_TYPE_DTD__,$idExpactedService);
		}
		else
		{
			echo "ERROR|||| Invalid booking id ";
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode=='CONSIGNEE')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{
			$res_ary=array();			
			$res_ary['iShipperConsignee']= 2;
			$res_ary['idServiceType']= '';
			if(!empty($res_ary))
			{
				$update_query = '';
				foreach($res_ary as $key=>$value)
				{
					$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
				}
			}
			
			$update_query = rtrim($update_query,",");
			
			if($kBooking->updateDraftBooking($update_query,$idBooking))
			{
				
			}
			//check if DTP is available or not. 
			$iAvailableServiceCount = $kWHSSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iDTP'))
			{
				echo "SUCCESS|||| ";
				$step_count = 2;
				$proces_bar_width = __PROCESS_BAR_WIDTH_TWO__;
				$originCountryText = get_country_name($postSearchAry['szOriginCountry'],'ORIGIN');
				
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__OPTION_EXPORT_HAULAGE_NOT_DEFINED_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__OPTION_EXPORT_HAULAGE_NOT_DEFINED__' ;
				//}
				
				$kWhsSearch = new cWHSSearch();
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}	
				?>
				<div class="requirement-content">
				<!--<h3>G1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/g1_box_title_1');?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/g1_box_title_2');?>?</li>
				</ol>
				</div> <!-- closing div for requirement-content -->
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_G1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/g1_box_option_A');?> <?=$originCountryText?> (<?=t($t_base.'title/g1_box_option_A_2');?>)</span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_M1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/g1_box_option_B');?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank" class="orange-button1"><span><?=t($t_base.'title/g1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="#" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?php
			}
			else
			{
				echo "SUCCESS|||| ";
				
				$step_count = 2;
				$proces_bar_width = __PROCESS_BAR_WIDTH_TWO__;
				$originCountryText = get_country_name($postSearchAry['szOriginCountry'],'ORIGIN');
				
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__OPTION_EXPORT_HAULAGE_NOT_DEFINED_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__OPTION_EXPORT_HAULAGE_NOT_DEFINED__' ;
				//}
				
				$kWhsSearch = new cWHSSearch();
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}	
				
				?>
				<div class="requirement-content">
				<!--<h3>M1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/m1_box_title_1');?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/m1_box_title_2');?>?</li>
				</ol>
				</div> <!-- closing div for requirement-content -->
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_M1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/m1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_M1B','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/m1_box_option_B');?> <?=$originCountryText?> (<?=t($t_base.'title/m1_box_option_B_2');?>)</span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank" class="orange-button1"><span><?=t($t_base.'title/m1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="#" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?php
			}
		}
		else
		{
			echo "ERROR|||| Invalid booking id ";
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode=='OPTION_G1A')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{
			//check if DTP is available or not. 
			$iAvailableServiceCount = $kWHSSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iDTD'))
			{
				echo "SUCCESS|||| ";
				
				$userType = check_user_type($postSearchAry);
				if($userType=='CONSIGNEE')
				{
					$step_count = 2;
					$proces_bar_width = __PROCESS_BAR_WIDTH_TWO__;
				}
				else
				{
					$step_count = 3;
					$proces_bar_width = __PROCESS_BAR_WIDTH_THREE__;
				}
				
				$kWhsSearch = new cWHSSearch();
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED__' ;
				//}
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}
				?>
				<div class="requirement-content">
				<!--<h3>A1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/a1_box_title_1');?> <?=$postSearchAry['szDestinationCountry']?>?</li>
				</ol>
				</div> <!-- closing div for requirement-content -->
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_A1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/a1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_A1B','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/a1_box_option_B');?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank" class="orange-button1"><span><?=t($t_base.'title/a1_box_option_C');?></span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?
			}
			else if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iPTD'))
			{
				echo "SUCCESS|||| ";
				$userType = check_user_type($postSearchAry);
				if($userType=='CONSIGNEE')
				{
					$step_count = 2;
					$proces_bar_width = __PROCESS_BAR_WIDTH_TWO__;
				}
				else
				{
					$step_count = 3;
					$proces_bar_width = __PROCESS_BAR_WIDTH_THREE__;
				}
				$destinationCountryText = get_country_name($postSearchAry['szDestinationCountry'],'DESTINATION');
				$originCountryText = get_country_name($postSearchAry['szOriginCountry'],'ORIGIN');
				?>
				<div class="requirement-content">
				<!--<h3>R1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/r1_box_title_1');?> <?=$postSearchAry['szDestinationCountry']?>? <?=t($t_base.'title/r1_box_title_2');?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/r1_box_title_3');?> <?=$postSearchAry['szDestinationCountry']?><?=t($t_base.'title/r1_box_title_4');?> <?=$postSearchAry['szOriginCountry']?>.</li>
				</ol>
				</div> <!-- closing div for requirement-content -->
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_R1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/r1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_R1B','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/r1_box_option_B');?> <?=$originCountryText?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_R1C','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/r1_box_option_C_1');?> <?=$originCountryText?> <?=t($t_base.'title/r1_box_option_C_2');?> <?=$destinationCountryText?></span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="#" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?php
			}
			else
			{
				echo "SUCCESS|||| ";
				
				$userType = check_user_type($postSearchAry);
				if($userType=='CONSIGNEE')
				{
					$step_count = 2;
					$proces_bar_width = __PROCESS_BAR_WIDTH_TWO__;
				}
				else
				{
					$step_count = 3;
					$proces_bar_width = __PROCESS_BAR_WIDTH_THREE__;
				}
				$destinationCountryText = get_country_name($postSearchAry['szDestinationCountry'],'DESTINATION');
				
				$kWhsSearch = new cWHSSearch();
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED__' ;
				//}
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}
				?>
				<div class="requirement-content">
				<!--<h3>B1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/b1_box_title_1');?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/b1_box_title_2');?>?</li>
				</ol>
				</div> <!-- closing div for requirement-content -->
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_B1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/b1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_B1B','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/b1_box_option_B');?> <?=$destinationCountryText?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank" class="orange-button1"><span><?=t($t_base.'title/b1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="#" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?
			}
		}
		else
		{
			echo "ERROR|||| Invalid booking id ";
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_A1A')
{
	$t_base_landing_page = "LandingPage/";
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		echo "SUCCESS|||| ";
			?>
			<div class="requirement-content-final">
			<!--<h3>Q7</h3>-->
			<p class="trans-box-complete"><?=t($t_base_landing_page.'title/q7_header_text_1')." ".$postSearchAry['szOriginCountry']." ".t($t_base_landing_page.'title/q7_header_text_2')." ".$postSearchAry['szDestinationCountry']."".t($t_base_landing_page.'title/q7_header_text_3')?></p>
			<?
					echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_DTD__);
			?>
			</div>
			<div class="process-bar process-completed">
				<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
				<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_DTD__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
			</div>
			<?
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_A1B' || $mode == 'OPTION_B1A' || $mode == 'OPTION_R1A')
{
	$t_base_landing_page = "LandingPage/";
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{
			//check if DTP is available or not. 
			$iAvailableServiceCount = $kWHSSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iDTW'))
			{
				echo "SUCCESS|||| ";
				?>
				<div class="requirement-content-final">
				<!--<h3>Q8</h3>-->
				<p class="trans-box-complete"><?=t($t_base_landing_page.'title/a1b_header_text_1')." ".$postSearchAry['szOriginCountry']." ".t($t_base_landing_page.'title/a1b_header_text_2')?></p>
				<?
						echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_DTW__);
				?>
				</div>
				<div class="process-bar process-completed">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_DTW__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
				</div>
				<?
			}
			else
			{
				echo "SUCCESS|||| ";
				?>
				<div class="requirement-content-final">
				<!--<h3>Q9</h3>-->
				<p class="trans-box-complete"><?=t($t_base_landing_page.'title/a1b_header_text_3')." ".$postSearchAry['szOriginCountry']." ".t($t_base_landing_page.'title/a1b_header_text_4')?></p>
				<?
						echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_DTP__,false,'q9');
				?>
				</div>
				<div class="process-bar process-completed">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_DTP__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
				</div>
				<?
			}
		}
		else
		{
			echo "ERROR|||| Invalid booking id ";
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_B1B' || $mode == 'OPTION_MB1B')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{
			echo "POPUP||||";
			$res_ary=array();
			$res_ary['idServiceType'] = __SERVICE_TYPE_WTD__ ;
			if($_SESSION['user_id']>0)
			{
				$kUser = new cUser();
				$kUser->getUserDetails($_SESSION['user_id']);
				$res_ary['idUser'] = $kUser->id ;				
				$res_ary['szFirstName'] = $kUser->szFirstName ;
				$res_ary['szLastName'] = $kUser->szLastName ;
				$res_ary['szEmail'] = $kUser->szEmail ;	
				$res_ary['szCustomerAddress1'] = $kUser->szAddress1;
				$res_ary['szCustomerAddress2'] = $kUser->szAddress2;
				$res_ary['szCustomerAddress3'] = $kUser->szAddress3;
				$res_ary['szCustomerPostCode'] = $kUser->szPostcode;
				$res_ary['szCustomerCity'] = $kUser->szCity;
				$res_ary['szCustomerCountry'] = $kUser->szCountry;
				$res_ary['szCustomerState'] = $kUser->szState;
				$res_ary['szCustomerPhoneNumber'] = $kUser->szPhoneNumber;
				$res_ary['szCustomerCompanyName'] = $kUser->szCompanyName;	
				$res_ary['szCustomerCompanyRegNo'] = $kUser->szCompanyRegNo;			
			}
			
			if(!empty($res_ary))
			{
				foreach($res_ary as $key=>$value)
				{
					$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
				}
			}
			$update_query = rtrim($update_query,",");
			if($kBooking->updateDraftBooking($update_query,$idBooking))
			{
				//echo "SUCCESS";
			}
			$kBooking->addExpactedServiceData($idBooking);
			$idExpactedService = $kBooking->idExpactedService ;
			echo display_F7_popup($postSearchAry,__SERVICE_TYPE_WTD__,$idExpactedService,$mode);
		}
		else
		{
			echo "ERROR|||| Invalid booking id ";
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_R1B')
{
	$t_base_landing_page = "LandingPage/";
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		echo "SUCCESS|||| ";
		?>
		<div class="requirement-content-final">
		<!--<h3>Q10</h3>-->
			<p class="trans-box-complete"><?=t($t_base_landing_page.'title/r1b_header_text_1')." ".$postSearchAry['szDestinationCountry']."".t($t_base_landing_page.'title/r1b_header_text_2')?></p>
			<?
					echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_PTD__,false,'q10');
			?>
			</div>
			<div class="process-bar process-completed">
				<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
				<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_PTD__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
			</div>
		<?php
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_R1C')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);	
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{
			echo "POPUP||||";
			$res_ary=array();
			$res_ary['idServiceType'] = __SERVICE_TYPE_DTD__ ;
			if($_SESSION['user_id']>0)
			{
				$kUser = new cUser();
				$kUser->getUserDetails($_SESSION['user_id']);
				$res_ary['idUser'] = $kUser->id ;				
				$res_ary['szFirstName'] = $kUser->szFirstName ;
				$res_ary['szLastName'] = $kUser->szLastName ;
				$res_ary['szEmail'] = $kUser->szEmail ;	
				$res_ary['szCustomerAddress1'] = $kUser->szAddress1;
				$res_ary['szCustomerAddress2'] = $kUser->szAddress2;
				$res_ary['szCustomerAddress3'] = $kUser->szAddress3;
				$res_ary['szCustomerPostCode'] = $kUser->szPostcode;
				$res_ary['szCustomerCity'] = $kUser->szCity;
				$res_ary['szCustomerCountry'] = $kUser->szCountry;
				$res_ary['szCustomerState'] = $kUser->szState;
				$res_ary['szCustomerPhoneNumber'] = $kUser->szPhoneNumber;
				$res_ary['szCustomerCompanyName'] = $kUser->szCompanyName;	
				$res_ary['szCustomerCompanyRegNo'] = $kUser->szCompanyRegNo;			
			}
			
			if(!empty($res_ary))
			{
				foreach($res_ary as $key=>$value)
				{
					$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
				}
			}
			$update_query = rtrim($update_query,",");
			if($kBooking->updateDraftBooking($update_query,$idBooking))
			{
				//echo "SUCCESS";
			}
			$kBooking->addExpactedServiceData($idBooking);
			$idExpactedService = $kBooking->idExpactedService ;
			echo display_F13_F14_popup($postSearchAry,__SERVICE_TYPE_DTD__,$idExpactedService);
		}
		else
		{
			echo "ERROR|||| Invalid booking id ";
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode=='OPTION_M1A')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{
			//check if DTP is available or not. 
			$iAvailableServiceCount = $kWHSSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iPTD'))
			{
				echo "SUCCESS|||| ";
				
				$userType = check_user_type($postSearchAry);
				if($userType=='CONSIGNEE')
				{
					$step_count = 2;
					$proces_bar_width = __PROCESS_BAR_WIDTH_TWO__;
				}
				else
				{
					$step_count = 3;
					$proces_bar_width = __PROCESS_BAR_WIDTH_THREE__;
				}
				$kWhsSearch = new cWHSSearch();
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED__' ;
				//}
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}
				?>
				<div class="requirement-content">
				<!--<h3>A1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/a1_box_title_1');?> <?=$postSearchAry['szDestinationCountry']?>?</li>
				</ol>
				</div> <!-- closing div for requirement-content -->
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_MA1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/a1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_MA1B','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/a1_box_option_B');?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank" class="orange-button1"><span><?=t($t_base.'title/a1_box_option_C');?></span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="#" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?php
			}
			else
			{
				echo "SUCCESS|||| ";
				
				$userType = check_user_type($postSearchAry);
				if($userType=='CONSIGNEE')
				{
					$step_count = 2;
					$proces_bar_width = __PROCESS_BAR_WIDTH_TWO__;
				}
				else
				{
					$step_count = 3;
					$proces_bar_width = __PROCESS_BAR_WIDTH_THREE__;
				}
				$destinationCountryText = get_country_name($postSearchAry['szDestinationCountry'],'DESTINATION');
				
				$kWhsSearch = new cWHSSearch();
				
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED__' ;
				//}
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}
				?>
				<div class="requirement-content">
				<!--<h3>B1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/b1_box_title_1');?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/b1_box_title_2');?>?</li>
				</ol>
				</div> <!-- closing div for requirement-content -->
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_MB1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/b1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_MB1B','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/b1_box_option_B');?> <?=$destinationCountryText?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank" class="orange-button1"><span><?=t($t_base.'title/b1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="#" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?
			}
		}
		else
		{
			echo "ERROR|||| Invalid booking id ";
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_MA1A')
{
	$t_base_landing_page = "LandingPage/";
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		echo "SUCCESS|||| ";
		?>
		<div class="requirement-content-final">
		<!--<h3>Q10</h3>-->
		<p class="trans-box-complete"><?=t($t_base_landing_page.'title/r1b_header_text_1')." ".$postSearchAry['szDestinationCountry']."".t($t_base_landing_page.'title/r1b_header_text_2')?></p>
		<?
				echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_PTD__,false,'q10');
		?>
		</div>
		<div class="process-bar process-completed">
			<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
			<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_PTD__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
		</div>
		<?
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_MA1B' || $mode =='OPTION_MB1A')
{
	$t_base_landing_page = "LandingPage/";
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{
			//check if DTP is available or not. 
			$iAvailableServiceCount = $kWHSSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iPTW'))
			{
				echo "SUCCESS|||| ";
				?>
				<div class="requirement-content-final">
				<!--<h3>Q11</h3>-->
				<p class="trans-box-complete"><?=t($t_base_landing_page.'title/ma1b_header_text_1')?></p>
				<?
						echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_PTW__,false,'q11');
				?>				
				</div>
				<div class="process-bar process-completed">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_PTW__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
				</div>
				<?
			}
			else
			{
				echo "SUCCESS|||| ";
				?>
				<div class="requirement-content-final">
				<!--<h3>Q12</h3>-->
				<p class="trans-box-complete"><?=t($t_base_landing_page.'title/ma1b_header_text_2')?></p>
				<?
						echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_PTP__,false,'q12');
				?>
				</div>
				<div class="process-bar process-completed">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_PTP__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
				</div>
				<?php
			}
		}
		else
		{
			echo "ERROR|||| Invalid booking id ";
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_M1B' || $mode=="OPTION_YCM1B")
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);	
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{
			echo "POPUP||||";
			$res_ary=array();
			$res_ary['idServiceType'] = __SERVICE_TYPE_DTD__ ;
			if($_SESSION['user_id']>0)
			{
				$kUser = new cUser();
				$kUser->getUserDetails($_SESSION['user_id']);
				$res_ary['idUser'] = $kUser->id ;				
				$res_ary['szFirstName'] = $kUser->szFirstName ;
				$res_ary['szLastName'] = $kUser->szLastName ;
				$res_ary['szEmail'] = $kUser->szEmail ;	
				$res_ary['szCustomerAddress1'] = $kUser->szAddress1;
				$res_ary['szCustomerAddress2'] = $kUser->szAddress2;
				$res_ary['szCustomerAddress3'] = $kUser->szAddress3;
				$res_ary['szCustomerPostCode'] = $kUser->szPostcode;
				$res_ary['szCustomerCity'] = $kUser->szCity;
				$res_ary['szCustomerCountry'] = $kUser->szCountry;
				$res_ary['szCustomerState'] = $kUser->szState;
				$res_ary['szCustomerPhoneNumber'] = $kUser->szPhoneNumber;
				$res_ary['szCustomerCompanyName'] = $kUser->szCompanyName;	
				$res_ary['szCustomerCompanyRegNo'] = $kUser->szCompanyRegNo;			
			}
			
			if(!empty($res_ary))
			{
				foreach($res_ary as $key=>$value)
				{
					$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
				}
			}
			$update_query = rtrim($update_query,",");
			if($kBooking->updateDraftBooking($update_query,$idBooking))
			{
				//echo "SUCCESS";
			}
			$kBooking->addExpactedServiceData($idBooking);
			$idExpactedService = $kBooking->idExpactedService ;
			echo display_F5_popup($postSearchAry,__SERVICE_TYPE_DTD__,$idExpactedService,$mode);
		}
		else
		{
			echo "ERROR|||| Invalid booking id ";
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode=='BOTH')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{
			$res_ary=array();			
			$res_ary['iShipperConsignee']= 3;
			$res_ary['idServiceType']= '';
			if(!empty($res_ary))
			{
				$update_query = '';
				foreach($res_ary as $key=>$value)
				{
					$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
				}
			}
			
			$update_query = rtrim($update_query,",");
			
			if($kBooking->updateDraftBooking($update_query,$idBooking))
			{
				
			}
			//check if DTP is available or not. 
			$iAvailableServiceCount = $kWHSSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iDTP'))
			{
				echo "SUCCESS|||| ";
				$originCountryText = get_country_name($postSearchAry['szOriginCountry'],'ORIGIN');
				
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__OPTION_EXPORT_HAULAGE_NOT_DEFINED_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__OPTION_EXPORT_HAULAGE_NOT_DEFINED__' ;
				//}
				
				$kWhsSearch = new cWHSSearch();
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}	
				?>
				<div class="requirement-content">
				<!--<h3>G1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="2">
					<li><?=t($t_base.'title/g1_box_title_1');?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/g1_box_title_2');?>?</li>
				</ol>
				</div>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCG1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/g1_box_option_A');?> <?=$originCountryText?> (<?=t($t_base.'title/g1_box_option_A_2');?>)</span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCM1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/g1_box_option_B');?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank" class="orange-button1"><span><?=t($t_base.'title/g1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:50%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?php
			}
			else
			{
				echo "SUCCESS|||| ";
				$originCountryText = get_country_name($postSearchAry['szOriginCountry'],'ORIGIN');
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__OPTION_EXPORT_HAULAGE_NOT_DEFINED_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__OPTION_EXPORT_HAULAGE_NOT_DEFINED__' ;
				//}
				$kWhsSearch = new cWHSSearch();
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}	
				?>
				<div class="requirement-content">
				<!--<h3>M1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="2">
					<li><?=t($t_base.'title/m1_box_title_1');?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/m1_box_title_2');?>?</li>
				</ol>
				</div>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCM1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/m1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCM1B','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/m1_box_option_B');?> <?=$originCountryText?> (<?=t($t_base.'title/m1_box_option_B_2');?>)</span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank" class="orange-button1"><span><?=t($t_base.'title/m1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:50%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?php
			}
		}
		else
		{
			echo "ERROR|||| Invalid booking id ";
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_YCG1A')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{
			//check if DTP is available or not. 
			$iAvailableServiceCount = $kWHSSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iDTD'))
			{
				echo "SUCCESS|||| ";
				$step_count = 3;
				$proces_bar_width = __PROCESS_BAR_WIDTH_THREE__;
				$destinationCountryText = get_country_name($postSearchAry['szDestinationCountry'],'DESTINATION');
				
				$kWhsSearch = new cWHSSearch();
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED__' ;
				//}
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}
				?>
				<div class="requirement-content">
				<!--<h3>E1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/e1_box_title_1');?> <?=$postSearchAry['szDestinationCountry']?>?</li>
				</ol>
				</div>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCG1E1A','<?=$szBookingRandomNum?>');"  class="orange-button1"><span><?=t($t_base.'title/e1_box_option_A');?> <?=$destinationCountryText?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCG1H1A','<?=$szBookingRandomNum?>');"  class="orange-button1"><span><?=t($t_base.'title/e1_box_option_B');?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank"  class="orange-button1"><span><?=t($t_base.'title/e1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="#" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?
			}
			else if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iPTD'))
			{
				echo "SUCCESS|||| ";
				$step_count = 3;
				$proces_bar_width = __PROCESS_BAR_WIDTH_THREE__;
				$destinationCountryText = get_country_name($postSearchAry['szDestinationCountry'],'DESTINATION');
				$originCountryText = get_country_name($postSearchAry['szOriginCountry'],'ORIGIN');
				?>
				<div class="requirement-content">
				<!--<h3>N1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/n1_box_title_1');?> <?=$postSearchAry['szDestinationCountry']?>? <?=t($t_base.'title/n1_box_title_2');?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'title/n1_box_title_3');?> <?=$destinationCountryText?> <?=t($t_base.'title/n1_box_title_4');?> <?=$postSearchAry['szOriginCountry']?>.</li>
				</ol>
				</div>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCG1H1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/n1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCG1N1B','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/n1_box_option_B_1');?> <?=$destinationCountryText?> - <?=t($t_base.'title/n1_box_option_B_2');?> <?=$originCountryText?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCG1N1C','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/n1_box_option_C_1');?> <?=$originCountryText?> <?=t($t_base.'title/n1_box_option_C_2');?> <?=$destinationCountryText?></span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="#" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?php
			}
			else
			{
				echo "SUCCESS|||| ";
				$step_count = 3;
				$proces_bar_width = __PROCESS_BAR_WIDTH_THREE__;
				
				$destinationCountryText = get_country_name($postSearchAry['szDestinationCountry'],'DESTINATION');
				$kWhsSearch = new cWHSSearch();
				
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED__' ;
				//}
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}
				?>
				<div class="requirement-content">
				<!--<h3>H1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/h1_box_title_1');?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/h1_box_title_2');?>?</li>
				</ol>
				</div>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCG1H1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/h1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCG1H1B','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/h1_box_option_B');?> <?=$destinationCountryText?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank" class="orange-button1"><span><?=t($t_base.'title/h1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="#" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?
			}
		}
		else
		{
			echo "ERROR|||| Invalid booking id ";
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_YCG1E1A')
{
	$t_base_landing_page = "LandingPage/";
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		echo "SUCCESS|||| ";
			?>
			<div class="requirement-content-final">
			<!--<h3>Q15</h3>-->
			<p class="trans-box-complete"><?=t($t_base_landing_page.'title/q15_header_text_1')." ".$postSearchAry['szOriginCountry']." ".t($t_base_landing_page.'title/q15_header_text_2')." ".$postSearchAry['szDestinationCountry']."".t($t_base_landing_page.'title/q15_header_text_3')?></p>
			<?
					echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_DTD__);
			?>			
			</div>
			<div class="process-bar process-completed">
				<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
				<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_DTD__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
			</div>
			<?php
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_YCG1H1A')
{
	$step_count = 4;
	$proces_bar_width = __PROCESS_BAR_WIDTH_FOUR__;
				
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{
			//check if DTP is available or not. 
			$iAvailableServiceCount = $kWHSSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iDTW'))
			{
				echo "SUCCESS|||| ";
				$kWhsSearch = new cWHSSearch();
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__WHO_SHOULD_PAY_DESTINATION_CHARGES_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__WHO_SHOULD_PAY_DESTINATION_CHARGES__' ;
				//}
				
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}
				?>
				<div class="requirement-content">
				<!--<h3>J1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/j1_box_title_1');?>?</li>
				</ol>
				</div>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCG1AJ1A','<?=$szBookingRandomNum?>');"  class="orange-button1"><span><?=t($t_base.'title/j1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCG1AJ1B','<?=$szBookingRandomNum?>');"  class="orange-button1"><span><?=t($t_base.'title/j1_box_option_B');?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank"  class="orange-button1"><span><?=t($t_base.'title/j1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="#" class="gray-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
				</div>
				<?
			}
			else
			{
				echo "SUCCESS|||| ";
				?>
				<div class="requirement-content-final">
				<!--<h3>Q14</h3>-->
				<p class="trans-box-complete"><?=t($t_base_landing_page.'title/q14_header_text_1')." ".$postSearchAry['szOriginCountry']." ".t($t_base_landing_page.'title/q14_header_text_2')?></p>
				<?
						echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_DTP__,false,'q14');
				?>
				</div>
				<div class="process-bar process-completed">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_DTP__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
				</div>
				<?
			}
		}
		else
		{
			echo "ERROR|||| Invalid booking id ";
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_YCG1AJ1A')
{
	$t_base_landing_page = "LandingPage/";
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		echo "SUCCESS|||| ";
		?>
		<div class="requirement-content-final">
		<!--<h3>Q13</h3>-->
		<p class="trans-box-complete"><?=t($t_base_landing_page.'title/q13_header_text_1')." ".$postSearchAry['szOriginCountry']." ".t($t_base_landing_page.'title/q13_header_text_2')?></p>
		<?
				echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_DTW__);
		?>
		</div>
		<div class="process-bar process-completed">
			<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
			<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_DTW__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
		</div>
		<?
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_YCG1AJ1B')
{
	$t_base_landing_page = "LandingPage/";
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		echo "SUCCESS|||| ";
		?>
		<div class="requirement-content-final">
		<!--<h3>Q14</h3>-->
		<p class="trans-box-complete"><?=t($t_base_landing_page.'title/q14_header_text_1')." ".$postSearchAry['szOriginCountry']." ".t($t_base_landing_page.'title/q14_header_text_2')?></p>
		<?php
				echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_DTP__,false,'q14');
		?>
		</div>
		<div class="process-bar process-completed">
			<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
			<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_DTP__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
		</div>
		<?php
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_YCG1N1B')
{
	$step_count = 4;
	$proces_bar_width = __PROCESS_BAR_WIDTH_FOUR__;
	
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{
			//check if DTP is available or not. 
			$iAvailableServiceCount = $kWHSSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTD'))
			{
				echo "SUCCESS|||| ";
				
				$kWhsSearch = new cWHSSearch();
				$iLanguage = getLanguageId();
//        			if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__WHO_SHOULD_PAY_ORIGN_CHARGES_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__WHO_SHOULD_PAY_ORIGN_CHARGES__' ;
				//}
				
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}				
				?>
				<div class="requirement-content">
				<!--<h3>L1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/l1_box_title_1');?>?</li>
				</ol>
				</div>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCG1AL1A','<?=$szBookingRandomNum?>');"  class="orange-button1"><span><?=t($t_base.'title/l1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCG1AL1B','<?=$szBookingRandomNum?>');"  class="orange-button1"><span><?=t($t_base.'title/l1_box_option_B');?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank" class="orange-button1"><span><?=t($t_base.'title/l1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?
			}
			else
			{
				echo "SUCCESS|||| ";
				?>
				<div class="requirement-content-final">
				<!--<h3>Q21</h3>-->
				<p class="trans-box-complete"><?=t($t_base_landing_page.'title/q21_header_text_1')." ".$postSearchAry['szDestinationCountry']."".t($t_base_landing_page.'title/q21_header_text_2')?></p>
				<?
						echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_PTD__,false,'q21');
				?>
				</div>
				<div class="process-bar process-completed">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);"  onclick="update_booking_service_type('<?=__SERVICE_TYPE_PTD__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
				</div>
				<?
			}
		}
		else
		{
			echo "ERROR|||| Invalid booking id ";
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_YCG1N1C')
{
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{
			echo "POPUP||||";
			$res_ary=array();
			$res_ary['idServiceType'] = __SERVICE_TYPE_DTD__ ;
			if($_SESSION['user_id']>0)
			{
				$kUser = new cUser();
				$kUser->getUserDetails($_SESSION['user_id']);
				$res_ary['idUser'] = $kUser->id ;				
				$res_ary['szFirstName'] = $kUser->szFirstName ;
				$res_ary['szLastName'] = $kUser->szLastName ;
				$res_ary['szEmail'] = $kUser->szEmail ;	
				$res_ary['szCustomerAddress1'] = $kUser->szAddress1;
				$res_ary['szCustomerAddress2'] = $kUser->szAddress2;
				$res_ary['szCustomerAddress3'] = $kUser->szAddress3;
				$res_ary['szCustomerPostCode'] = $kUser->szPostcode;
				$res_ary['szCustomerCity'] = $kUser->szCity;
				$res_ary['szCustomerCountry'] = $kUser->szCountry;
				$res_ary['szCustomerState'] = $kUser->szState;
				$res_ary['szCustomerPhoneNumber'] = $kUser->szPhoneNumber;
				$res_ary['szCustomerCompanyName'] = $kUser->szCompanyName;	
				$res_ary['szCustomerCompanyRegNo'] = $kUser->szCompanyRegNo;			
			}
			
			if(!empty($res_ary))
			{
				foreach($res_ary as $key=>$value)
				{
					$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
				}
			}
			$update_query = rtrim($update_query,",");
			if($kBooking->updateDraftBooking($update_query,$idBooking))
			{
				//echo "SUCCESS";
			}
			$kBooking->addExpactedServiceData($idBooking);
			$idExpactedService = $kBooking->idExpactedService ;
			echo display_F9_F10_popup($postSearchAry,__SERVICE_TYPE_DTD__,$idExpactedService);
		}
		else
		{
			echo "ERROR|||| Invalid booking id ";
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_YCG1AL1A')
{
	$t_base_landing_page = "LandingPage/";
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		echo "SUCCESS|||| ";
			?>
			<div class="requirement-content-final">
			<!--<h3>Q18</h3>-->
				<p class="trans-box-complete"><?=t($t_base_landing_page.'title/q18_header_text_1')." ".$postSearchAry['szDestinationCountry']."".t($t_base_landing_page.'title/q18_header_text_2')?></p>
				<?
						echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_WTD__);
				?>
				</div>	
			<div class="process-bar process-completed">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_WTD__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
				</div>
			<?
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_YCG1AL1B')
{
	$t_base_landing_page = "LandingPage/";
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		echo "SUCCESS|||| ";
		?>
		<div class="requirement-content-final">
		<!--<h3>Q21</h3>-->
		<p class="trans-box-complete"><?=t($t_base_landing_page.'title/q21_header_text_1')." ".$postSearchAry['szDestinationCountry']."".t($t_base_landing_page.'title/q21_header_text_2')?></p>
		<?
				echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_PTD__,false,'q21');
		?>		
		</div>
		<div class="process-bar process-completed">
			<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
			<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_PTD__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
		</div>
		<?php
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_YCM1A')
{
	$step_count = 3;
	$proces_bar_width = __PROCESS_BAR_WIDTH_THREE__;
	
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{
			//check if DTP is available or not. 
			$iAvailableServiceCount = $kWHSSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTP'))
			{
				echo "SUCCESS|||| ";
				$kWhsSearch = new cWHSSearch();
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__WHO_SHOULD_PAY_ORIGN_CHARGES_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__WHO_SHOULD_PAY_ORIGN_CHARGES__' ;
				//}
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}
				?>
				<div class="requirement-content">
				<!--<h3>L1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/l1_box_title_1');?>?</li>
				</ol>
				</div>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCL1A','<?=$szBookingRandomNum?>');"  class="orange-button1"><span><?=t($t_base.'title/l1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCL1B','<?=$szBookingRandomNum?>');"  class="orange-button1"><span><?=t($t_base.'title/l1_box_option_B');?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank" class="orange-button1"><span><?=t($t_base.'title/l1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="#" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?php
			}
			else if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iPTD'))
			{
				echo "SUCCESS|||| ";
				$destinationCountryText = get_country_name($postSearchAry['szDestinationCountry'],'DESTINATION');
				$kWhsSearch = new cWHSSearch();
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED__' ;
				//}
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}
				?>
				<div class="requirement-content">
				<!--<h3>E1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/e1_box_title_1');?> <?=$postSearchAry['szDestinationCountry']?>?</li>
				</ol>
				</div>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCE1A','<?=$szBookingRandomNum?>');"  class="orange-button1"><span><?=t($t_base.'title/yce1_box_option_A');?> <?=$destinationCountryText?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCH1A','<?=$szBookingRandomNum?>');"  class="orange-button1"><span><?=t($t_base.'title/yce1_box_option_B');?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank"  class="orange-button1"><span><?=t($t_base.'title/yce1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?php
			}
			else
			{
				echo "SUCCESS|||| ";
				$destinationCountryText = get_country_name($postSearchAry['szDestinationCountry'],'DESTINATION');
				
				$kWhsSearch = new cWHSSearch();
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED__' ;
				//}
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}
				?>
				<div class="requirement-content">
				<!--<h3>H1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/h1_box_title_1');?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/h1_box_title_2');?>?</li>
				</ol>
				</div>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCH1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/h1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCH1B','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/h1_box_option_B');?> <?=$destinationCountryText?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank" class="orange-button1"><span><?=t($t_base.'title/h1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="#" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?php
			}
		}
		else
		{
			echo "ERROR|||| Invalid booking id ";
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_YCL1A')
{
	$step_count = 4;
	$proces_bar_width = __PROCESS_BAR_WIDTH_FOUR__;
	
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{
			//check if DTP is available or not. 
			$iAvailableServiceCount = $kWHSSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTD'))
			{
				echo "SUCCESS|||| ";
				$destinationCountryText = get_country_name($postSearchAry['szDestinationCountry'],'DESTINATION');
				
				$kWhsSearch = new cWHSSearch();
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED__' ;
				//}
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}
				
				?>
				<div class="requirement-content">
				<!--<h3>E1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/e1_box_title_1');?> <?=$postSearchAry['szDestinationCountry']?>?</li>
				</ol>
				</div>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCL1E1A','<?=$szBookingRandomNum?>');"  class="orange-button1"><span><?=t($t_base.'title/yce1_box_option_A');?> <?=$destinationCountryText?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCL1H1A','<?=$szBookingRandomNum?>');"  class="orange-button1"><span><?=t($t_base.'title/yce1_box_option_B');?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank"  class="orange-button1"><span><?=t($t_base.'title/yce1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?php
			}
			else
			{
				echo "SUCCESS|||| ";
				$destinationCountryText = get_country_name($postSearchAry['szDestinationCountry'],'DESTINATION');
				$kWhsSearch = new cWHSSearch();
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED__' ;
				//}
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}
				?>
				<div class="requirement-content">
				<!--<h3>H1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/h1_box_title_1');?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/h1_box_title_2');?>?</li>
				</ol>
				</div>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCL1H1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/h1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCL1H1B','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/h1_box_option_B');?> <?=$destinationCountryText?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank" class="orange-button1"><span><?=t($t_base.'title/h1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="#" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?
			}
		}
		else
		{
			echo "ERROR|||| Invalid booking id ";
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_YCE1A')
{
	$t_base_landing_page = "LandingPage/";
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		echo "SUCCESS|||| ";
			?>
			<div class="requirement-content-final">
			<!--<h3>Q21</h3>-->
			<p class="trans-box-complete"><?=t($t_base_landing_page.'title/YCE1A_header_text_1')." ".$postSearchAry['szDestinationCountry']."".t($t_base_landing_page.'title/YCE1A_header_text_2')?></p>
			<?
					echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_PTD__,false,'q21');
			?>
			</div>
			<div class="process-bar process-completed">
				<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
				<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_PTD__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
			</div>
			<?
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_YCL1E1A')
{
	$t_base_landing_page = "LandingPage/";
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		echo "SUCCESS|||| ";
			?>
			<div class="requirement-content-final">
			<!--<h3>Q18</h3>-->
			<p class="trans-box-complete"><?=t($t_base_landing_page.'title/YCL1E1A_header_text_1')." ".$postSearchAry['szDestinationCountry']."".t($t_base_landing_page.'title/YCL1E1A_header_text_2')?></p>
			<?
					echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_WTD__);
			?>
			</div>
			<div class="process-bar process-completed">
				<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
				<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_WTD__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
			</div>
			<?php
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_YCL1H1A')
{
	$step_count = 5;
	$proces_bar_width = __PROCESS_BAR_WIDTH_FIVE__;
	
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{
			//check if DTP is available or not. 
			$iAvailableServiceCount = $kWHSSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iWTW'))
			{
				echo "SUCCESS|||| ";
				$kWhsSearch = new cWHSSearch();
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__WHO_SHOULD_PAY_DESTINATION_CHARGES_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__WHO_SHOULD_PAY_DESTINATION_CHARGES__' ;
				//}
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}
				
				?>
				<div class="requirement-content">
				<!--<h3>J1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/j1_box_title_1');?>?</li>
				</ol>
				</div>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCL1AJ1A','<?=$szBookingRandomNum?>');"  class="orange-button1"><span><?=t($t_base.'title/j1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCL1AJ1B','<?=$szBookingRandomNum?>');"  class="orange-button1"><span><?=t($t_base.'title/j1_box_option_B');?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank"  class="orange-button1"><span><?=t($t_base.'title/j1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?
			}
			else
			{
				echo "SUCCESS|||| ";
				?>
				<div class="requirement-content-final">
				<!--<h3>Q17</h3>-->
				<p class="trans-box-complete"><?=t($t_base_landing_page.'title/q17_header_text_1')?></p>
				<?
						echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_WTP__,false,'q17');
				?>
				</div>
				<div class="process-bar process-completed">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_WTP__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
				</div>
				<?
			}
		}
		else
		{
			echo "ERROR|||| Invalid booking id ";
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_YCL1AJ1A')
{
	$t_base_landing_page = "LandingPage/";
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		echo "SUCCESS|||| ";
			?>
			<div class="requirement-content-final">
			<!--<h3>Q16</h3>-->
			<p class="trans-box-complete"><?=t($t_base_landing_page.'title/q16_header_text_1')?></p>
			<?
					echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_WTW__);
			?>
			</div>
			<div class="process-bar process-completed">
				<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
				<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_WTW__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
			</div>
			<?
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_YCL1AJ1B')
{
	$t_base_landing_page = "LandingPage/";
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		echo "SUCCESS|||| ";
		?>
		<div class="requirement-content-final">
		<!--<h3>Q17</h3>-->
			<p class="trans-box-complete"><?=t($t_base_landing_page.'title/q17_header_text_1')?></p>
			<?
					echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_WTP__,false,'q17');
			?>
		</div>
			<div class="process-bar process-completed">
				<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
				<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_WTP__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
			</div>
		<?php
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_YCL1B')
{
	$step_count = 4;
	$proces_bar_width = __PROCESS_BAR_WIDTH_FOUR__;
	
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{
			//check if DTP is available or not. 
			$iAvailableServiceCount = $kWHSSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iPTD'))
			{
				echo "SUCCESS|||| ";
				$destinationCountryText = get_country_name($postSearchAry['szDestinationCountry'],'DESTINATION');
				
				$kWhsSearch = new cWHSSearch();
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED__' ;
				//}
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}
				
				?>
				<div class="requirement-content">
				<!--<h3>E1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/e1_box_title_1');?> <?=$postSearchAry['szDestinationCountry']?>?</li>
				</ol>
				</div>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCE1A','<?=$szBookingRandomNum?>');"  class="orange-button1"><span><?=t($t_base.'title/yce1_box_option_A');?> <?=$destinationCountryText?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCH1A','<?=$szBookingRandomNum?>');"  class="orange-button1"><span><?=t($t_base.'title/yce1_box_option_B');?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank"  class="orange-button1"><span><?=t($t_base.'title/yce1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?php
			}
			else
			{
				echo "SUCCESS|||| ";
				$destinationCountryText = get_country_name($postSearchAry['szDestinationCountry'],'DESTINATION');
				
				$kWhsSearch = new cWHSSearch();
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__OPTION_IMPORT_HAULAGE_NOT_DEFINED__' ;
				//}
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}
				
				?>
				<div class="requirement-content">
				<!--<h3>H1</h3>-->
				<?
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/h1_box_title_1');?> <?=$postSearchAry['szDestinationCountry']?> <?=t($t_base.'title/h1_box_title_2');?>?</li>
				</ol>
				</div>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCH1A','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/h1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCH1B','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'title/h1_box_option_B');?> <?=$destinationCountryText?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank" class="orange-button1"><span><?=t($t_base.'title/h1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" class="gray-button1"><span><?=t($t_base.'title/next_step');?></span></a>
				</div>
				<?php
			}
		}
	}
}
else if($mode == 'OPTION_YCH1A')
{
	$step_count = 5;
	$proces_bar_width = __PROCESS_BAR_WIDTH_FIVE__;
	
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
		{
			//check if DTP is available or not. 
			$iAvailableServiceCount = $kWHSSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
			if($kWHSSearch->isServiceExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],'iPTW'))
			{
				echo "SUCCESS|||| ";
				$kWhsSearch = new cWHSSearch();
				$iLanguage = getLanguageId();
//				if($iLanguage==__LANGUAGE_ID_DANISH__)
//				{
//					$szVariableName = '__WHO_SHOULD_PAY_DESTINATION_CHARGES_DANISH__' ;
//				}
//				else
//				{
					$szVariableName = '__WHO_SHOULD_PAY_DESTINATION_CHARGES__' ;
				//}
				$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
				if(empty($szExplainUrl))
				{
					$szExplainUrl = __EXPLAIN_PAGE_URL__ ;
				}
			?>
				<div class="requirement-content">
				<!--<h3>J1</h3>-->
				<?php
					echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
				?>
				<ol start="<?=$step_count?>">
					<li><?=t($t_base.'title/j1_box_title_1');?>?</li>
				</ol>
				</div>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCL1BJ1A','<?=$szBookingRandomNum?>');"  class="orange-button1"><span><?=t($t_base.'title/j1_box_option_A');?></span></a></p>
				<p class="active-orange-btn"><a href="javascript:void(0);" onclick="find_transportation('OPTION_YCL1BJ1B','<?=$szBookingRandomNum?>');"  class="orange-button1"><span><?=t($t_base.'title/j1_box_option_B');?></span></a></p>
				<p class="active-orange-btn"><a href="<?=$szExplainUrl?>" target="_blank"  class="orange-button1"><span><?=t($t_base.'title/j1_box_option_C');?>?</span></a></p>
				<div class="process-bar">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:<?=$proces_bar_width?>%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" class="gray-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
				</div>
				<?
			}
			else
			{
				echo "SUCCESS|||| ";
				?>
				<div class="requirement-content-final">
				<!--<h3>Q20</h3>-->
				<p class="trans-box-complete"><?=t($t_base_landing_page.'title/q20_header_text_1')?></p>
				<?
						echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_PTP__,false,'q20');
				?>
				</div>
				<div class="process-bar process-completed">
					<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
					<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_PTP__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
				</div>
				<?
			}
		}
		else
		{
			echo "ERROR|||| Invalid booking id ";
		}
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}

else if($mode == 'OPTION_YCL1BJ1A')
{
	$t_base_landing_page = "LandingPage/";
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		echo "SUCCESS|||| ";
			?>
			<div class="requirement-content-final">
			<!--<h3>Q19</h3>-->
			<p class="trans-box-complete"><?=t($t_base_landing_page.'title/q19_header_text_1')?></p>
			<?
					echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_PTW__,false,'q19');
			?>
			</div>
			<div class="process-bar process-completed">
				<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
				<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_PTW__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
			</div>
			<?
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'OPTION_YCL1BJ1B')
{
	$t_base_landing_page = "LandingPage/";
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		echo "SUCCESS|||| ";
		?>
		<div class="requirement-content-final">
		<!--<h3>Q20</h3>-->
			<p class="trans-box-complete"><?=t($t_base_landing_page.'title/q20_header_text_1')?></p>
			<?
					echo display_service_type_canvas($postSearchAry,__SERVICE_TYPE_PTP__,false,'q20');
			?>
		</div>	
		<div class="process-bar process-completed">
				<p><span class="text">100%</span><span class="left">&nbsp;</span><span class="main" style="width:100%;">&nbsp;</span><span class="right">&nbsp;</span></p>
				<a href="javascript:void(0);" onclick="update_booking_service_type('<?=__SERVICE_TYPE_PTP__?>','<?=$szBookingRandomNum?>','SERVICE_TYPE');" class="orange-button1"><span><?=t($t_base_landing_page.'title/next_step')?></span></a>
			</div>
		<?php
	}
	else
	{
		echo "ERROR|||| Invalid booking id ";
	}
}
else if($mode == 'VALID_EMAIL_CHECK')
{
	$szEmail = sanitize_all_html_input(trim($_POST['val']));
	$kBooking->set_szEmail($szEmail);
	if(!empty($kBooking->arErrorMessages))
	{
		echo "ERROR||||" ;
		echo "!";
		die;
	}
	else
	{
		echo "SUCCESS||||";
	}
}
else if($mode == 'VALID_NAME_CHECK')
{
	$szCustomerName = sanitize_all_html_input(trim($_POST['val']));
	if(empty($szCustomerName))
	{
		echo "ERROR||||" ;
		echo "!";
		die;
	}
	else
	{
		echo "SUCCESS||||";
		die;
	}
}
else if($mode == 'DISPLAY_ABANDON_POPUP')
{
	$t_base = "home/";
	$landing_page_url = __LANDING_PAGE_URL__ ;
	
	$iLanguage = getLanguageId();
//	if($iLanguage==__LANGUAGE_ID_DANISH__)
//	{
		$szDangor_cargo_yes = t($t_base.'header/yes');
		$szDangor_cargo_no = t($t_base.'header/no');
//	}
//	else
//	{
//		$szDangor_cargo_yes = 'Yes';
//		$szDangor_cargo_no = 'No';
//	}
	
	if($_SESSION['user_id']>0)
	{
		$kWhsSearch = new cWHSSearch();
		$iMaxDraftAllowed = $kWhsSearch->getManageMentVariableByDescription('__MAX_ALLOWED_DRAFT__');
		
		$navigation_pop_up_message_header =  t($t_base.'header/navigation_popup_message_for_signed_in_user_header');
		$navigation_pop_up_message =  t($t_base.'header/navigation_popup_message_for_signed_in_user')." ".$iMaxDraftAllowed." ".t($t_base.'header/navigation_popup_message_for_signed_in_user_2');
	}
	else
	{
		$navigation_pop_up_message_header =  t($t_base.'header/navigation_popup_message_for_not_signed_in_user_header');
		$navigation_pop_up_message =  t($t_base.'header/navigation_popup_message_for_not_signed_in_user');
	}
	$login_flag = sanitize_all_html_input(trim($_REQUEST['login_flag']));
	$page_url = sanitize_all_html_input(trim($_REQUEST['page_url']));
	if(empty($page_url))
	{
		$page_url = __HOME_PAGE_URL__ ;
	}
	echo "SUCCESS||||";
	?>
		<div id="popup-bg"></div>
		 <div id="popup-container">			
		  <div class="popup abandon-popup">
		  <p class="close-icon" align="right">
			<a onclick="showHide('leave_page_div');" href="javascript:void(0);">
			<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
			</a>
			</p>
		  	<h5><strong><?=$navigation_pop_up_message_header?></strong></h5>
			<p><?=$navigation_pop_up_message?></p>
			<br/>
			<p align="center">
				<a href="javascript:void(0);" onclick="clear_search_result('<?=$page_url?>','<?=$_SESSION['user_id']?>')" class="button1"><span><?php echo $szDangor_cargo_yes;?></span></a>
				<a href="javascript:void(0);" onclick="showHide('leave_page_div');" class="button1"><span><?php echo $szDangor_cargo_no;?></span></a>
				<input type="hidden" name="abandonPopupAry[redirect_abandon_popup_url]" value="<?=$page_url?>" id="redirect_abandon_popup_url" />
				<input type="hidden" name="abandonPopupAry[login_flag]" value="<?php echo $login_flag;?>" id="login_flag" />
			</p>	
		  </div>
	   </div>
	<?php 
}
else if(!empty($_POST['addExpactedServiceAry']))
{
	$kBooking = new cBooking();
	$landing_page_url = __LANDING_PAGE_URL__ ;
	$idBooking = (int)$_POST['addExpactedServiceAry']['idBooking'];
	$idExpactedService = trim($_POST['addExpactedServiceAry']['idExpactedService']);
	$iSendMailToAdmin = trim($_POST['addExpactedServiceAry']['iSendMailToAdmin']);
	$szBookingRandomNumber = $kBooking->getBookingRandomNum($idBooking);
	$_POST['addExpactedServiceAry']['szCustomerPhone']=urldecode(base64_decode($_REQUEST['szCustomerPhone']));
	$postSearchAry = array();
	$postSearchAry = $kBooking->getBookingDetails($idBooking);		
	if((int)$_SESSION['user_id']<=0)
	{
		//$kBooking->set_szCustomerName(sanitize_all_html_input(trim($_POST['addExpactedServiceAry']['szCustomerName'])));
	}
	$t_base = "home/homepage/";
	
	if($kBooking->updateExpactedServices($_POST['addExpactedServiceAry']))
	{
		//if($iSendMailToAdmin=='sendEmail')
		//{
			$kWHSSearch = new cWHSSearch();
			$toEmail=$kWHSSearch->getManageMentVariableByDescription('__CONTACT_EMAIL__');
			
			$ret_ary= array();
			if((int)$_SESSION['user_id']>0)
			{
				$kUser = new cUser();
				$kUser->getUserDetails($_SESSION['user_id']);
				
				$ret_ary['szName'] = trim($kUser->szFirstName." ".$kUser->szLastName);
				$ret_ary['szPhoneNumber'] = trim($kUser->szPhoneNumber);
				$szLoggin="Yes";
			}
			else
			{
				$ret_ary['szName'] = trim($_POST['addExpactedServiceAry']['szCustomerName']);
				$ret_ary['szPhoneNumber'] = trim($_POST['addExpactedServiceAry']['szCustomerPhone']);
				$szLoggin="No";
			}
			
			if($postSearchAry['iShipperConsignee']=='1')
			{
				$szType="Shipper";
			}
			else if($postSearchAry['iShipperConsignee']=='2')
			{
				$szType="Consignee";
			}
			else if($postSearchAry['iShipperConsignee']=='3')
			{
				$szType="Both";
			}
			else
			{
				$szType="Unknown";
			}
			$idServiceType=$postSearchAry['idServiceType'];
			if($idServiceType == __SERVICE_TYPE_PTP__)	//PTP
			{
				$szOriginService="Port";
				$szDestinationService="Port";
			}
			elseif($idServiceType == __SERVICE_TYPE_PTD__)	//PTD
			{
				$szOriginService="Port";
				$szDestinationService="Door";
			}
			elseif($idServiceType == __SERVICE_TYPE_PTW__)	//PTW
			{
				$szOriginService="Port";
				$szDestinationService="Warehouse";
			}
			elseif($idServiceType == __SERVICE_TYPE_WTP__)	//WTP
			{
				$szOriginService="Warehouse";
				$szDestinationService="Port";
			}
			elseif($idServiceType == __SERVICE_TYPE_WTD__)	//WTD
			{
				$szOriginService="Warehouse";
				$szDestinationService="Door";
			}
			elseif($idServiceType == __SERVICE_TYPE_WTW__)	//WTW
			{
				$szOriginService="Warehouse";
				$szDestinationService="Warehouse";
			}
			elseif($idServiceType == __SERVICE_TYPE_DTP__)	//DTP
			{
				$szOriginService="Door";
				$szDestinationService="Port";
			}
			else if($idServiceType == __SERVICE_TYPE_DTW__)	//DTW
			{
				$szOriginService="Door";
				$szDestinationService="Warehouse";
			}
			elseif($idServiceType == __SERVICE_TYPE_DTD__)	//DTD
			{
				$szOriginService="Door";
				$szDestinationService="Door";
			}
			else
			{
				$szOriginService="Unknown";
				$szDestinationService="Unknown";
			}
			
			$szOriginString='';
			if($postSearchAry['szOriginPostCode']!='')
			{
				$szOriginString=$postSearchAry['szOriginPostCode'];
			}
			
			if($postSearchAry['szOriginCity']!='')
			{
				if($szOriginString!='')
				{
					$szOriginString .=", ";
				}
				$szOriginString .=$postSearchAry['szOriginCity'];
			}
			
			
			$szDestinationLocation='';
			if($postSearchAry['szDestinationPostCode']!='')
			{
				$szDestinationLocation=$postSearchAry['szDestinationPostCode'];
			}
			
			if($postSearchAry['szDestinationCity']!='')
			{
				if($szDestinationLocation!='')
				{
					$szDestinationLocation .=", ";
				}
				$szDestinationLocation .=$postSearchAry['szDestinationCity'];
			}
			$szTimingType='';
			if($postSearchAry['idTimingType']==1)
			{
				$szTimingType='Ready at origin';
			}
			else if($postSearchAry['idTimingType']==2)
			{
				$szTimingType='Available at destination';
			}
			
			$dtTimeing='';
			if($postSearchAry['dtTimingDate']!='' && $postSearchAry['dtTimingDate']!='0000-00-00 00:00:00')
			{
				$dtTimeing=date('d-m-Y',strtotime($postSearchAry['dtTimingDate']));
			}
			
			if($postSearchAry['iOriginCC']==1)
			{
				$szOCC="Yes";
			}
			else
			{
				if($postSearchAry['iBookingStep']<=5)
				{
					$szOCC="Unknown";
				}
				else
				{
					$szOCC="No";
				}
				
			}
			
			if($postSearchAry['iDestinationCC']==2)
			{
				$szDCC="Yes";
			}
			else
			{
				if($postSearchAry['iBookingStep']<=5)
				{
					$szDCC="Unknown";
				}
				else
				{
					$szDCC="No";
				}
			}
			
			///Cargo detail array////
				$cargoDetailArr=$kBooking->getCargoDeailsByBookingId($idBooking);
				$cargoDetailsStr='';
				if(!empty($cargoDetailArr))
				{
					$i=1;
					foreach($cargoDetailArr as $cargoDetailArrs)
					{
						$fLength = ceil($cargoDetailArrs['fLength']);
						$fWidth = ceil($cargoDetailArrs['fWidth']);
						$fHeight = ceil($cargoDetailArrs['fHeight']);
						$fWeight = ceil($cargoDetailArrs['fWeight']);
						$iQuantity = ceil($cargoDetailArrs['iQuantity']);
						$szDangerCargo = $cargoDetailArrs['iDG'];
						
						if((int)$szDangerCargo==1)
						{
							$szDangerCargo="Yes";
						}
						else
						{
							$szDangerCargo="No";
						}
						
						$idCargoMeasure = $cargoDetailArrs['idCargoMeasure'];
						$cargoMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_CARGO_MEASURE__');
						$szCargoMeasure = $cargoMeasureAry[$idCargoMeasure]['szDescription'];
						
						$idWeightMeasure = $cargoDetailArrs['idWeightMeasure'];
						$weightMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_WEIGHT_MEASURE__');
						$szWeightMeasure = $weightMeasureAry[$idWeightMeasure]['szDescription'];
							
						
						if(!empty($cargoDetailsStr))
						{
							$cargoDetailsStr .= "<br />".$i.": ".$iQuantity." x ".number_format((float)$fLength).$szCargoMeasure." x ".number_format((float)$fWidth).$szCargoMeasure." x ".number_format((float)$fHeight).$szCargoMeasure." (LxWxH), ".number_format((float)$fWeight)." ".$szWeightMeasure;
						}
						else
						{
							$cargoDetailsStr = ''.$i.": ".$iQuantity." x ".number_format((float)$fLength).$szCargoMeasure." x ".number_format((float)$fWidth).$szCargoMeasure." x ".number_format((float)$fHeight).$szCargoMeasure." (LxWxH), ".number_format((float)$fWeight)." ".$szWeightMeasure ;
						}
						++$i;
					}
				}
			
			$ret_ary['szUserEmail'] = trim($_POST['addExpactedServiceAry']['szEmail']);
			$ret_ary['szIPAddress'] = trim($_SERVER['REMOTE_ADDR']);
			$ret_ary['szFromCountry'] = trim($postSearchAry['szOriginCountry']);
			$ret_ary['szToCountry'] = trim($postSearchAry['szDestinationCountry']);
			$ret_ary['szType'] = $szType;
			$ret_ary['szOriginService'] = $szOriginService;
			$ret_ary['szDestinationService'] = $szDestinationService;
			$ret_ary['szOriginLocation']=$szOriginString;
			$ret_ary['szDestinationLocation']=$szDestinationLocation;
			$ret_ary['szTimingType']=$szTimingType;
			$ret_ary['dtTimeing']=$dtTimeing;
			$ret_ary['szOCC']=$szOCC;
			$ret_ary['szDCC']=$szDCC;
			$ret_ary['szCargoDetail']=$cargoDetailsStr;
			$ret_ary['szLoggin']=$szLoggin;
			$ret_ary['szEmail']=$toEmail;
			//print_r($ret_ary);
			createEmail(__REQUEST_FOR_SERVICE_NOT_FOUND_SEND_TO_MANAGEMENT__, $ret_ary,$ret_ary['szEmail'],' ', __STORE_SUPPORT_EMAIL__,false, __STORE_SUPPORT_EMAIL__,__FLAG_FOR_CUSTOMER__,false);	
		//}
		echo "SUCCESS||||";
		$landing_page_url = __LANDING_PAGE_URL__ ;
		echo display_d1_popup($landing_page_url);
		die;
	}
	
	if(!empty($kBooking->arErrorMessages))
	{
		echo "ERROR ||||" ;
	?>
	<div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
	      foreach($kBooking->arErrorMessages as $key=>$values)
	      {
	      ?><li><?=$values?></li>
	      <?php 
	      }
	?>
	</ul>
	</div>
	<?
		die;
	}
}
else if(!empty($_POST['addExpactedTimingAry']))
{
	$kBooking = new cBooking();
	$landing_page_url = __LANDING_PAGE_URL__ ;
	$idBooking = (int)$_POST['addExpactedTimingAry']['idBooking'];
	$idExpactedService =(int)trim($_POST['addExpactedTimingAry']['idExpactedService']);
	if(!empty($_POST['addExpactedTimingAry']['dtExpactedTiming']))
	{
		$dtTimingDate = date('Y-m-d H:i:s',sanitize_all_html_input(trim($_POST['addExpactedTimingAry']['dtExpactedTiming'])));
	}
	$idTimingType = sanitize_all_html_input(trim($_POST['addExpactedTimingAry']['idTimingType']));
	
	$szBookingRandomNumber = $kBooking->getBookingRandomNum($idBooking);
	if((int)$_SESSION['user_id']<=0)
	{
		//$kBooking->set_szCustomerName(sanitize_all_html_input(trim($_POST['addExpactedServiceAry']['szCustomerName'])));
	}
	$t_base = "home/homepage/";
	$res_ary=array();
	$res_ary['dtTimingDate'] = $dtTimingDate;
	$res_ary['idTimingType']= $idTimingType;
	if(!empty($res_ary))
	{
		$update_query='';
		foreach($res_ary as $key=>$value)
		{
			$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
		}
	}				
	$update_query = rtrim($update_query,",");
	if($kBooking->updateExpactedBooking($update_query,$idBooking))
	{
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		
		$bookingNotificationAry=array();
		$bookingNotificationAry = $_POST['addExpactedTimingAry'] ;
		$bookingNotificationAry['dtTiming'] = $dtTimingDate ;
		$kBooking->addNotification($bookingNotificationAry);
		
		echo "SUCCESS||||";
		$landing_page_url = __LANDING_PAGE_URL__ ;
		echo display_d2_popup($postSearchAry,$landing_page_url);
		die;
	}
	if(!empty($kBooking->arErrorMessages))
	{
		echo "ERROR ||||" ;
	?>
	<div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
	      foreach($kBooking->arErrorMessages as $key=>$values)
	      {
	      ?><li><?=$values?></li>
	      <?php 
	      }
	?>
	</ul>
	</div>
	<?php
		die;
	}
}

$mode = sanitize_all_html_input(trim($_REQUEST['mode']));
$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_key']));

if($mode=='CARGO_DETAILS')
{
	$t_base = "LandingPage/";
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
            $kConfig = new cConfig();
            $data=array();
            $postSearchAry = array();
            $postSearchAry = $kBooking->getBookingDetails($idBooking);
            $data = $_REQUEST['cargodetailAry'];
            $field_name = sanitize_all_html_input(trim($_REQUEST['fieldName']));
            $field_number = sanitize_all_html_input(trim($_REQUEST['field_number']));
            $lengthCtr = count($data['iLength']);
            $error_message1 = t($t_base.'title/please_check_for_input');
            $error_message2 = t($t_base.'title/we_cannot_understand_it');

            $szErrorMessage_length = $error_message1." Length ".$error_message2 ;
            $szErrorMessage_width = $error_message1." Width ".$error_message2 ;
            $szErrorMessage_height = $error_message1." Height ".$error_message2 ;
            $szErrorMessage_quantity = $error_message1." Quantity ".$error_message2 ;
            $szErrorMessage_weight = $error_message1." Weight ".$error_message2 ;

            for($i=1;$i<=$lengthCtr;$i++)
            {
                if($field_number>1)
                {
                    /*
                    * In order to make error messaging work we first validate all previous lines if there is anything empty or non-numeric then we prompt error message.
                    * so suppose we are filling data in cargo line 3 then we first check line 1 and 2 if there is any thing which we can't accept then prompt error message.
                    * 
                    * */
                    for($j=1;$j<$field_number;$j++)
                    {
                        if($data['iLength'][$j] ==='')
                        {
                            $iLength = '';
                        }
                        else if(is_numeric($data['iLength'][$j]))
                        {
                            $iLength = $data['iLength'][$j];
                        }
                        else
                        {
                            $iLength = 0;
                        }
					
                            if($data['iHeight'][$j] ==='')
                            {
                                    $iHeight = '';
                            }
                            else if(is_numeric($data['iHeight'][$j]))
                            {
                                    $iHeight = $data['iHeight'][$j];
                            }
                            else
                            {
                                    $iHeight = 0;
                            }

                            if($data['iWidth'][$j] ==='')
                            {
                                    $iWidth = '';
                            }
                            else if(is_numeric($data['iWidth'][$j]))
                            {
                                    $iWidth = $data['iWidth'][$j];
                            }
                            else
                            {
                                    $iWidth = 0;
                            }

                            if($data['iWeight'][$j] ==='')
                            {
                                    $iWeight = '';
                            }
                            else if(is_numeric($data['iWeight'][$j]))
                            {
                                    $iWeight = $data['iWeight'][$j];
                            }
                            else
                            {
                                    $iWeight = 0;
                            }

                            if($data['iQuantity'][$j] ==='')
                            {
                                    $iQuantity = '';
                            }
                            else if(is_numeric($data['iQuantity'][$j]))
                            {
                                    $iQuantity = $data['iQuantity'][$j];
                            }
                            else
                            {
                                    $iQuantity = 0;
                            }

                            //echo "<br / field_number = ".$field_number;
                            //echo "<br /> value of j ".$j;
                            //echo "<br /> Length ".$iLength ;
                            //echo "<br /> Width ".$iWidth ;
                            //echo "<br /> Height ".$iHeight ;
                            //echo "<br /> Quantity ".$iQuantity ;
                            //echo "<br /> Weigth ".$iWeight ;

                            if($iLength <= 0)
                            {
                                    echo "ERROR||||".$szErrorMessage_length;
                                    die;
                            }
                            else if($iWidth <= 0)
                            {
                                    echo "ERROR||||".$szErrorMessage_width;
                                    die;
                            }
                            else if($iHeight <= 0)
                            {
                                    echo "ERROR||||".$szErrorMessage_height;
                                    die;
                            }
                            else if($iQuantity <= 0)
                            {
                                    echo "ERROR||||".$szErrorMessage_quantity;
                                    die;
                            }
                            else if($iWeight <= 0)
                            {
                                    echo "ERROR||||".$szErrorMessage_weight;
                                    die;
                            }
                        }
				
				/*
				* If all previous lines are filled correctly then we check for current line 
				* **/
				if($field_number==$j)
				{
					if($data['iLength'][$j] ==='')
					{
						$iLength = '';
					}
					else if(is_numeric($data['iLength'][$j]))
					{
						$iLength = $data['iLength'][$j];
					}
					else
					{
						$iLength = 0;
					}
					
					if($data['iHeight'][$j] ==='')
					{
						$iHeight = '';
					}
					else if(is_numeric($data['iHeight'][$j]))
					{
						$iHeight = $data['iHeight'][$j];
					}
					else
					{
						$iHeight = 0;
					}
					
					if($data['iWidth'][$j] ==='')
					{
						$iWidth = '';
					}
					else if(is_numeric($data['iWidth'][$j]))
					{
						$iWidth = $data['iWidth'][$j];
					}
					else
					{
						$iWidth = 0;
					}
					
					if($data['iWeight'][$j] ==='')
					{
						$iWeight = '';
					}
					else if(is_numeric($data['iWeight'][$j]))
					{
						$iWeight = $data['iWeight'][$j];
					}
					else
					{
						$iWeight = 0;
					}
					
					if($data['iQuantity'][$j] ==='')
					{
						$iQuantity = '';
					}
					else if(is_numeric($data['iQuantity'][$j]))
					{
						$iQuantity = $data['iQuantity'][$j];
					}
					else
					{
						$iQuantity = 0;
					}	
							
					if($field_name == 'Length')
					{
                                            if($iLength <= 0)
                                            {
                                                echo "ERROR||||".$szErrorMessage_length;
                                                die;
                                            }
                                            else if(($iWidth <= 0) && ($iWidth !==''))
                                            {
                                                echo "ERROR||||".$szErrorMessage_width;
                                                die;
                                            }
                                            else if(($iHeight <= 0) && ($iHeight !==''))
                                            {
                                                echo "ERROR||||".$szErrorMessage_height;
                                                die;
                                            }
                                            else if(($iQuantity <= 0) && ($iQuantity !==''))
                                            {
                                                echo "ERROR||||".$szErrorMessage_quantity;
                                                die;
                                            }
                                            else if(($iWeight <= 0) && ($iWeight !==''))
                                            {
                                                echo "ERROR||||".$szErrorMessage_weight;
                                                die;
                                            }
					}
					if($field_name == 'Width')
					{
                                            if(($iLength <= 0) || ($iLength ===''))  // if moving ahead then previous fields must be filled.
                                            {
                                                echo "ERROR||||".$szErrorMessage_length;
                                                die;
                                            }
                                            else if($iWidth<=0)
                                            {
                                                echo "ERROR||||".$szErrorMessage_width;
                                                die;
                                            }
                                            else if(($iHeight<=0) && ($iHeight!=='')) // and check next fields if they empty its ok but if they filled with wrong data show error message. 
                                            {
                                                echo "ERROR||||".$szErrorMessage_height;
                                                die;
                                            }
                                            else if(($iQuantity<=0) && ($iQuantity!==''))
                                            {
                                                echo "ERROR||||".$szErrorMessage_quantity;
                                                die;
                                            }
                                            else if(($iWeight<=0) && ($iWeight!==''))
                                            {
                                                echo "ERROR||||".$szErrorMessage_weight;
                                                die;
                                            }
					}
					
					if($field_name == 'Height')
					{
                                            if(($iLength <= 0) || ($iLength ===''))
                                            {
                                                echo "ERROR||||".$szErrorMessage_length;
                                                die;
                                            }
                                            else if(($iWidth <= 0) || ($iWidth ===''))
                                            {
                                                echo "ERROR||||".$szErrorMessage_width;
                                                die;
                                            }
                                            else if(($iHeight <= 0))
                                            {
                                                echo "ERROR||||".$szErrorMessage_height;
                                                die;
                                            }
                                            else if(($iQuantity <= 0) && ($iQuantity !==''))
                                            {
                                                echo "ERROR||||".$szErrorMessage_quantity;
                                                die;
                                            }
                                            else if(($iWeight <= 0) && ($iWeight !==''))
                                            {
                                                echo "ERROR||||".$szErrorMessage_weight;
                                                die;
                                            }
					}
					
					if($field_name == 'Quantity')
					{
                                            if(($iLength <= 0) || ($iLength ===''))
                                            {
                                                echo "ERROR||||".$szErrorMessage_length;
                                                die;
                                            }
                                            else if(($iWidth <= 0) || ($iWidth ===''))
                                            {
                                                echo "ERROR||||".$szErrorMessage_width;
                                                die;
                                            }
                                            else if(($iHeight <= 0) || ($iHeight ===''))
                                            {
                                                echo "ERROR||||".$szErrorMessage_height;
                                                die;
                                            }
                                            else if($iQuantity <= 0)
                                            {
                                                echo "ERROR||||".$szErrorMessage_quantity;
                                                die;
                                            }
                                            else if(($iWeight <= 0) && ($iWeight !==''))
                                            {
                                                echo "ERROR||||".$szErrorMessage_weight;
                                                die;
                                            }
					}			
					if($field_name == 'Weight')
					{
						if(($iLength <= 0) || ($iLength ===''))
						{
							echo "ERROR||||".$szErrorMessage_length;
							die;
						}
						else if(($iWidth <= 0) || ($iWidth ===''))
						{
							echo "ERROR||||".$szErrorMessage_width;
							die;
						}
						else if(($iHeight <= 0) || ($iHeight ===''))
						{
							echo "ERROR||||".$szErrorMessage_height;
							die;
						}
						else if(($iQuantity <= 0) || ($iQuantity ===''))
						{
							echo "ERROR||||".$szErrorMessage_quantity;
							die;
						}
						else if($iWeight <= 0)
						{
							echo "ERROR||||".$szErrorMessage_weight;
							die;
						}
					}
					break;
				}
			}
			else
			{
				/*
				* We'll enter in this block if and only we are checking in cargo line 1. 
				* */
				if($data['iLength'][$i] ==='')
				{
					$iLength = '';
				}
				else if(is_numeric($data['iLength'][$i]))
				{
					$iLength = $data['iLength'][$i];
				}
				else
				{
					$iLength = 0;
				}
				
				if($data['iHeight'][$i] ==='')
				{
					$iHeight = '';
				}
				else if(is_numeric($data['iHeight'][$i]))
				{
					$iHeight = $data['iHeight'][$i];
				}
				else
				{
					$iHeight = 0;
				}
				
				if($data['iWidth'][$i] ==='')
				{
					$iWidth = '';
				}
				else if(is_numeric($data['iWidth'][$i]))
				{
					$iWidth = $data['iWidth'][$i];
				}
				else
				{
					$iWidth = 0;
				}
				
				if($data['iWeight'][$i] ==='')
				{
					$iWeight = '';
				}
				else if(is_numeric($data['iWeight'][$i]))
				{
					$iWeight = $data['iWeight'][$i];
				}
				else
				{
					$iWeight = 0;
				}
				
				if($data['iQuantity'][$i] ==='')
				{
					$iQuantity = '';
				}
				else if(is_numeric($data['iQuantity'][$i]))
				{
					$iQuantity = $data['iQuantity'][$i];
				}
				else
				{
					$iQuantity = 0;
				}	
						
				if($field_name == 'Length')
				{
					if($iLength <= 0)
					{
						echo "ERROR||||".$szErrorMessage_length;
						die;
					}
					else if(($iWidth <= 0) && ($iWidth !==''))
					{
						echo "ERROR||||".$szErrorMessage_width;
						die;
					}
					else if(($iHeight <= 0) && ($iHeight !==''))
					{
						echo "ERROR||||".$szErrorMessage_height;
						die;
					}
					else if(($iQuantity <= 0) && ($iQuantity !==''))
					{
						echo "ERROR||||".$szErrorMessage_quantity;
						die;
					}
					else if(($iWeight <= 0) && ($iWeight !==''))
					{
						echo "ERROR||||".$szErrorMessage_weight;
						die;
					}
				}
				if($field_name == 'Width')
				{
					if(($iLength <= 0) || ($iLength ===''))  // if moving ahead then previous fields must be filled.
					{
						echo "ERROR||||".$szErrorMessage_length;
						die;
					}
					else if($iWidth <= 0)
					{
						echo "ERROR||||".$szErrorMessage_width;
						die;
					}
					else if(($iHeight <= 0) && ($iHeight !=='')) // and check next fields if they empty its ok but if they filled with wrong data show error message. 
					{
						echo "ERROR||||".$szErrorMessage_height;
						die;
					}
					else if(($iQuantity <= 0) && ($iQuantity !==''))
					{
						echo "ERROR||||".$szErrorMessage_quantity;
						die;
					}
					else if(($iWeight <= 0) && ($iWeight !==''))
					{
						echo "ERROR||||".$szErrorMessage_weight;
						die;
					}
				}
				
				if($field_name == 'Height')
				{
					if(($iLength <= 0) || ($iLength ===''))
					{
						echo "ERROR||||".$szErrorMessage_length;
						die;
					}
					else if(($iWidth <= 0) || ($iWidth ===''))
					{
						echo "ERROR||||".$szErrorMessage_width;
						die;
					}
					else if(($iHeight <= 0))
					{
						echo "ERROR||||".$szErrorMessage_height;
						die;
					}
					else if(($iQuantity <= 0) && ($iQuantity !==''))
					{
						echo "ERROR||||".$szErrorMessage_quantity;
						die;
					}
					else if(($iWeight <= 0) && ($iWeight !==''))
					{
						echo "ERROR||||".$szErrorMessage_weight;
						die;
					}
				}
				
				if($field_name == 'Quantity')
				{
					if(($iLength <= 0) || ($iLength ===''))
					{
						echo "ERROR||||".$szErrorMessage_length;
						die;
					}
					else if(($iWidth <= 0) || ($iWidth ===''))
					{
						echo "ERROR||||".$szErrorMessage_width;
						die;
					}
					else if(($iHeight <= 0) || ($iHeight ===''))
					{
						echo "ERROR||||".$szErrorMessage_height;
						die;
					}
					else if($iQuantity <= 0)
					{
						echo "ERROR||||".$szErrorMessage_quantity;
						die;
					}
					else if(($iWeight <= 0) && ($iWeight !==''))
					{
						echo "ERROR||||".$szErrorMessage_weight;
						die;
					}
				}			
				if($field_name == 'Weight')
				{
					if(($iLength <= 0) || ($iLength ===''))
					{
						echo "ERROR||||".$szErrorMessage_length;
						die;
					}
					else if(($iWidth <= 0) || ($iWidth ===''))
					{
						echo "ERROR||||".$szErrorMessage_width;
						die;
					}
					else if(($iHeight <= 0) || ($iHeight ===''))
					{
						echo "ERROR||||".$szErrorMessage_height;
						die;
					}
					else if(($iQuantity <= 0) || ($iQuantity ===''))
					{
						echo "ERROR||||".$szErrorMessage_quantity;
						die;
					}
					else if($iWeight <= 0)
					{
						echo "ERROR||||".$szErrorMessage_weight;
						die;
					}
				}
				break;
			}
		}	
	}
}
?>