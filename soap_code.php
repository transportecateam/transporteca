Array
(
	[0] => RetrieveIncidentResponse RetrieveIncident(RetrieveIncidentRequest $RetrieveIncidentRequest)
    [1] => RetrieveIncidentKeysListResponse RetrieveIncidentKeysList(RetrieveIncidentKeysListRequest $RetrieveIncidentKeysListRequest)
    [2] => RetrieveIncidentListResponse RetrieveIncidentList(RetrieveIncidentListRequest $RetrieveIncidentListRequest)
	[3] => CreateIncidentResponse CreateIncident(CreateIncidentRequest $CreateIncidentRequest)
	[4] => CloseIncidentResponse CloseIncident(CloseIncidentRequest $CloseIncidentRequest)
    [5] => ReopenIncidentResponse ReopenIncident(ReopenIncidentRequest $ReopenIncidentRequest)
    [6] => ResolveIncidentResponse ResolveIncident(ResolveIncidentRequest $ResolveIncidentRequest)
	[7] => UpdateIncidentResponse UpdateIncident(UpdateIncidentRequest $UpdateIncidentRequest)

    [8] => RetrieveIncidentwithoutJournalUpdatesResponse RetrieveIncidentwithoutJournalUpdates(RetrieveIncidentwithoutJournalUpdatesRequest $RetrieveIncidentwithoutJournalUpdatesRequest)
    [9] => RetrieveIncidentwithoutJournalUpdatesKeysListResponse RetrieveIncidentwithoutJournalUpdatesKeysList(RetrieveIncidentwithoutJournalUpdatesKeysListRequest $RetrieveIncidentwithoutJournalUpdatesKeysListRequest)
    [10] => RetrieveIncidentwithoutJournalUpdatesListResponse RetrieveIncidentwithoutJournalUpdatesList(RetrieveIncidentwithoutJournalUpdatesListRequest $RetrieveIncidentwithoutJournalUpdatesListRequest)
    [11] => RetrieveOLDSM7IncidentResponse RetrieveOLDSM7Incident(RetrieveOLDSM7IncidentRequest $RetrieveOLDSM7IncidentRequest)
    [12] => RetrieveOLDSM7IncidentKeysListResponse RetrieveOLDSM7IncidentKeysList(RetrieveOLDSM7IncidentKeysListRequest $RetrieveOLDSM7IncidentKeysListRequest)
    [13] => RetrieveOLDSM7IncidentListResponse RetrieveOLDSM7IncidentList(RetrieveOLDSM7IncidentListRequest $RetrieveOLDSM7IncidentListRequest)
    [14] => CreateOLDSM7IncidentResponse CreateOLDSM7Incident(CreateOLDSM7IncidentRequest $CreateOLDSM7IncidentRequest)
    [15] => CloseOLDSM7IncidentResponse CloseOLDSM7Incident(CloseOLDSM7IncidentRequest $CloseOLDSM7IncidentRequest)
    [16] => ReopenOLDSM7IncidentResponse ReopenOLDSM7Incident(ReopenOLDSM7IncidentRequest $ReopenOLDSM7IncidentRequest)
    [17] => ResolveOLDSM7IncidentResponse ResolveOLDSM7Incident(ResolveOLDSM7IncidentRequest $ResolveOLDSM7IncidentRequest)
    [18] => UpdateOLDSM7IncidentResponse UpdateOLDSM7Incident(UpdateOLDSM7IncidentRequest $UpdateOLDSM7IncidentRequest)
)
--------------------------------PHP CODE START------------------------------------------------
<?php
ini_set("soap.wsdl_cache_enabled", "0");
//need to close instance after every request.
$options=array(
			'login'                         =>              'falcon',
			'password'                      =>              '',
			'trace'                         =>              true,
			'exceptions'                    =>              true,
			'connection_timeout'            =>              9999,
			'features'                      =>              SOAP_SINGLE_ELEMENT_ARRAYS,
			'soap_version'                  =>              SOAP_1_1,
			'encoding'                      =>              'ISO-8859-1'
        );

$loginFlag=checkLogin($options);
if($loginFlag!='SUCCESS')
{
                                print "<h3>".$loginFlag."</h3>";
}else
{
                                print "##$$## ##$$## ##$$##";
}

function checkLogin($options){
        global $options;

        $sessionUrl="./IncidentManagement.wsdl";
		print "======Befor Request============".date("H:i:s");
		$loginClient = new SoapClient($sessionUrl,$options);
		try{
            $xml    =       '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://schemas.hp.com/SM/7" xmlns:com="http://schemas.hp.com/SM/7/Common" xmlns:xm="http://www.w3.org/2005/05/xmlmime">
   <soapenv:Header/>
   <soapenv:Body>
      <ns:RetrieveIncidentRequest attachmentInfo="false" attachmentData="false" ignoreEmptyElements="true" updatecounter="" handle="" count="" start="">
         <ns:model query="">
            <ns:keys query="" updatecounter="">
               <!--Optional:-->
               <ns:IncidentID type="String" mandatory="" readonly="">IM00414581</ns:IncidentID>
            </ns:keys>
            <ns:instance query="" uniquequery="" recordid="" updatecounter="">
               <!--Optional:-->
               <ns:IncidentID type="String" mandatory="" readonly="">IM00414581</ns:IncidentID>
            </ns:instance>
            <!--Optional:-->
            <ns:messages>
               <!--Zero or more repetitions:-->
               <com:message type="String" mandatory="" readonly="" severity="" module=""></com:message>
            </ns:messages>
         </ns:model>
      </ns:RetrieveIncidentRequest>
   </soapenv:Body>
</soapenv:Envelope>';
                $dataobj=new SoapVar($xml, XSD_ANYXML);
				var_dump($dataobj);
                $assignSeatData= $loginClient->RetrieveIncident($dataobj);
                print "======After Request============".date("H:i:s");
                return "SUCCESS";
		}catch (SoapFault $sf) {
                //print_r($sf);
				print_r($loginClient->__getLastRequest());
				echo "<br>Response==============================================";
				print_r($loginClient->__getLastResponse());
                echo "Error";
                $ErrorText=$sf->getMessage();
                return $ErrorText;
        }

}
?>
