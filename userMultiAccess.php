<?php
 ob_start();
session_start();
$szMetaTitle= __USER_MULTI_ACCESS_META_TITLE__;
$szMetaKeywords = __USER_MULTI_ACCESS_META_KEYWORDS__;
$szMetaDescription = __USER_MULTI_ACCESS_META_DESCRIPTION__;

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

$style="style='top: 0px; bottom: auto; display: block;'";
$headerSearchFlag=1;
require_once( __APP_PATH_LAYOUT__ . "/header_new.php" );

$t_base = "Users/MultiUserAccess/";
checkAuth();
/*if($kUser->iConfirmed!='1')
{
	header('Location:'.__BASE_URL__);
	exit();
}*/
?>
<div id="my-account-body">
			<?php require_once( __APP_PATH_LAYOUT__ . "/nav.php" ); ?> 
			<div class="hsbody-2-right">
				<div id="linked_user_account">
					<?php
					require_once( __APP_PATH__ . "/linkedUserList.php" ); ?>
				</div>
				<br />
				<div id="map_user_account">
					<?php
					require_once( __APP_PATH__ . "/mappedUsers.php" ); ?>
				</div>
				<br />
				
				<br />
				
			</div>
		</div>




<?php
include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>