<?php
 ob_start();
session_start();
$page_title="My Account";
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

$t_base = "Users/ShipperConsigness/";
$t_base_error = "Error";
$successAddflag=false;
$successUpdatedflag=false;
$successflag=false;
$kRegisterShipCon = new cRegisterShipCon();
$kConfig = new cConfig();
$idRegisterShipperConsignees=0;
if(((int)$_REQUEST['idShipperConsigness']>0 && $_REQUEST['flag']=='update') || ((int)$_POST['registerShipperCongArr']['idShipperConsigness']>0 && $_POST['registerShipperCongArr']['flag']=='update') )
{
	$kRegisterShipCon->getShipperConsignessDetail($_REQUEST['idShipperConsigness']);
}
if($_POST['registerShipperCongArr']['szCity']=='' && $_POST['registerShipperCongArr']['szPostCode']!='')
{
	$_POST['registerShipperCongArr']['szCity']=$kConfig->getAllCityByPostCode($_POST['registerShipperCongArr']['szCountry'],$_POST['registerShipperCongArr']['szPostCode']);
}
if((int)$_POST['registerShipperCongArr']['idShipperConsigness']>0 && $_POST['registerShipperCongArr']['flag']=='update')
{
	if(!empty($_POST['registerShipperCongArr']))
	{
		//print_r($_POST['registerShipperCongArr']);
		if($kRegisterShipCon->updateRegisterShipperConsigness($_POST['registerShipperCongArr']))
		{
			$idRegisterShipperConsignees=$_POST['registerShipperCongArr']['idShipperConsigness'];
			$successUpdatedflag=true;
			$successflag=true;
		}
	}
}
else
{
	if(!empty($_POST['registerShipperCongArr']))
	{
		//print_r($_POST['registerShipperCongArr']);
		if($kRegisterShipCon->addRegisterShipperConsigness($_POST['registerShipperCongArr']))
		{
			$successAddflag=true;
			$successflag=true;
		}
	}
}
//print_r($_POST['registerShipperCongArr']);
if(!empty($_POST['registerShipperCongArr']['szPhoneNumberUpdate']))
{
	$_POST['registerShipperCongArr']['szPhoneNumber']=urldecode(base64_decode($_POST['registerShipperCongArr']['szPhoneNumberUpdate']));
}
$iLanguage = getLanguageId();
$allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);

$shipcongroupArr=$kRegisterShipCon->shipconGroup($iLanguage);
$szLabelClassName = "class='common-fields-container clearfix' ";
if($successflag)
{
	if($successAddflag)
	{
		$flag="add";
	}
	else if($successUpdatedflag)
	{
		$flag="update";
	}
        
?>
<script type="text/javascript">
register_shipper_consigness('<?=$flag?>','<?=$idRegisterShipperConsignees?>');
</script>
<?php }?>
<script type="text/javascript">
$().ready(function() {	
/*
	$("#szPostCode").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
	
	$("#szCity").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});	
	*/
});
	</script>
<p class="f-size-22" ><?=t($t_base.'title/shipper_consignee_modify');?></p>
	<br />
	<form name="registerShipperConsigness" id="registerShipperConsigness" method="post">
		<?php
	if(!empty($kRegisterShipCon->arErrorMessages)){
	?>
	<div id="regError" class="errorBox ">
	<div class="header"><?=t($t_base_error.'/please_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
		foreach($kRegisterShipCon->arErrorMessages as $key=>$values)
		{
		?><li><?=$values?></li>
		<?php	
		}
	?>
	</ul>
	</div>
	</div>
	<?php }?>
	<label <?php echo $szLabelClassName; ?>>
		<span class="field-name"><?=t($t_base.'title/registered_as');?></span>
		<span class="field-container" >
			<select style="min-width:220px;" name="registerShipperCongArr[idGroup]" id="idGroup" onblur="closeTip('shipcong');" onfocus="openTip('shipcong');">
				<?php
					if(!empty($shipcongroupArr)){
						foreach($shipcongroupArr as $shipcongroupArrs)
						{
					?><option value="<?=$shipcongroupArrs['id']?>" <?=((($_POST['registerShipperCongArr']['idGroup'])?$_POST['registerShipperCongArr']['idGroup']:$kRegisterShipCon->idGroup) ==  $shipcongroupArrs['id'] ) ? "selected":""?>><?=$shipcongroupArrs['szDescription']?></option>	
					<?php	}
					}
					?>
			</select>
		</span>
		<div class="field-alert"><div id="shipcong" style="display:none;"><?=t($t_base.'messages/register_as_msg');?></div></div>
	</label>
	<label <?php echo $szLabelClassName; ?>>
		<span class="field-name"><?=t($t_base.'fields/c_name');?> </span>
		<span class="field-container"><input type="text" name="registerShipperCongArr[szCompanyName]" id="szCompanyName" onblur="closeTip('cname');" onfocus="openTip('cname');" value="<?=$_POST['registerShipperCongArr']['szCompanyName'] ? $_POST['registerShipperCongArr']['szCompanyName'] : $kRegisterShipCon->szCompanyName ?>"/></span>
		<div class="field-alert"><div id="cname" style="display:none;"><?=t($t_base.'messages/company_name');?></div></div>
	</label>
	<label <?php echo $szLabelClassName; ?>>
		<span class="field-name"><?=t($t_base.'fields/f_name');?></span>
		<span class="field-container"><input type="text" name="registerShipperCongArr[szFirstName]" id="szFirstName" value="<?=$_POST['registerShipperCongArr']['szFirstName'] ? $_POST['registerShipperCongArr']['szFirstName'] : $kRegisterShipCon->szFirstName ?>" onblur="closeTip('fname');" onfocus="openTip('fname');"/></span>
		<div class="field-alert"><div id="fname" style="display:none;"><?=t($t_base.'messages/contact_person');?></div></div>
	</label>
	<label <?php echo $szLabelClassName; ?>>
		<span class="field-name"><?=t($t_base.'fields/l_name');?></span>
		<span class="field-container"><input type="text" name="registerShipperCongArr[szLastName]" id="szLastName" value="<?=$_POST['registerShipperCongArr']['szLastName'] ? $_POST['registerShipperCongArr']['szLastName'] : $kRegisterShipCon->szLastName ?>" onblur="closeTip('lname');" onfocus="openTip('lname');"/></span>
		<div class="field-alert"><div id="lname" style="display:none;"><?=t($t_base.'messages/contact_person');?></div></div>
	</label>
	<label <?php echo $szLabelClassName; ?>>
		<span class="field-name"><?=t($t_base.'fields/email');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
		<span class="field-container"><input type="text" name="registerShipperCongArr[szEmail]" id="szEmail" value="<?=$_POST['registerShipperCongArr']['szEmail'] ? $_POST['registerShipperCongArr']['szEmail'] : $kRegisterShipCon->szEmail ?>" onblur="closeTip('email');" onfocus="openTip('email');"/></span>
		<div class="field-alert"><div id="email" style="display:none;"><?=t($t_base.'messages/email_msg');?></div></div>
	</label>
	<label <?php echo $szLabelClassName; ?>>
			<span class="field-name"><?=t($t_base.'fields/p_number');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
			<span class="field-container"><input type="text" name="registerShipperCongArr[szPhoneNumber]" id="szPhoneNumber" onblur="closeTip('pNumber');" onfocus="openTip('pNumber');" value="<?=$_POST['registerShipperCongArr']['szPhoneNumber'] ? $_POST['registerShipperCongArr']['szPhoneNumber'] : $kRegisterShipCon->szPhoneNumber ?>"/></span>
			<div class="field-alert1"><div id="pNumber" style="display:none;"><?=t($t_base.'messages/phone_no_msg');?></div></div>
		</label>
	<label <?php echo $szLabelClassName; ?>>
		<span class="field-name"><?=t($t_base.'fields/country');?></span>
		<span class="field-container">
			<select style="max-width:220px;" size="1" name="registerShipperCongArr[szCountry]" id="szCountry">
				<option value=""><?=t($t_base.'fields/select');?></option>
					<?php
					if(!empty($allCountriesArr))
					{
						foreach($allCountriesArr as $allCountriesArrs)
						{
							?><option value="<?=$allCountriesArrs['id']?>" <?=((($_POST['registerShipperCongArr']['szCountry'])?$_POST['registerShipperCongArr']['szCountry']:$kRegisterShipCon->szCountry) ==  $allCountriesArrs['id'] ) ? "selected":""?>><?=$allCountriesArrs['szCountryName']?></option>
							<?php
						}
					}
				?>
			</select>
		</span>
		</label>
		<label <?php echo $szLabelClassName; ?>>
			<span class="field-name"><?=t($t_base.'fields/city');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
			<span class="field-container"><input type="text" name="registerShipperCongArr[szCity]" id="szCity" onblur="closeTip('city');" onfocus="openTip('city');" value="<?=$_POST['registerShipperCongArr']['szCity'] ? $_POST['registerShipperCongArr']['szCity'] : $kRegisterShipCon->szCity ?>"/></span>
			<div class="field-alert"><div id="city" style="display:none;"><?=t($t_base.'messages/city_blue_box_msg');?></div></div>
		</label>
		<label <?php echo $szLabelClassName; ?>>
			<span class="field-name"><?=t($t_base.'fields/postcode');?></span>
			<span class="field-container"><input type="text" name="registerShipperCongArr[szPostCode]" id="szPostCode" onblur="closeTip('postcode');" onfocus="openTip('postcode');" value="<?=$_POST['registerShipperCongArr']['szPostCode'] ? $_POST['registerShipperCongArr']['szPostCode'] : $kRegisterShipCon->szPostcode ?>"/></span>
			<div class="field-alert"><div id="postcode" style="display:none;"><?=t($t_base.'messages/postcode_blue_box_msg');?></div></div>
		</label>
	<label <?php echo $szLabelClassName; ?>>
			<span class="field-name"><?=t($t_base.'fields/address');?></span>
			<span class="field-container"><input type="text" name="registerShipperCongArr[szAddress1]" id="szAddress1" onblur="closeTip('address1');" onfocus="openTip('address1');" value="<?=$_POST['registerShipperCongArr']['szAddress1'] ? $_POST['registerShipperCongArr']['szAddress1'] : $kRegisterShipCon->szAddress1 ?>"/></span>
			<div class="field-alert"><div id="address1" style="display:none;"><?=t($t_base.'messages/address_blue_box_msg');?></div></div>
		</label>
		<label <?php echo $szLabelClassName; ?>>
			<span class="field-name"><?=t($t_base.'fields/address1');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
			<span class="field-container"><input type="text" name="registerShipperCongArr[szAddress2]" id="szAddress2" onblur="closeTip('address2');" onfocus="openTip('address2');" value="<?=$_POST['registerShipperCongArr']['szAddress2'] ? $_POST['registerShipperCongArr']['szAddress2'] : $kRegisterShipCon->szAddress2 ?>"/></span>
			<div class="field-alert"><div id="address2" style="display:none;"><?=t($t_base.'messages/address_blue_box_msg');?></div></div>
			
		</label>
		<label <?php echo $szLabelClassName; ?>>
			<span class="field-name"><?=t($t_base.'fields/address2');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
			<span class="field-container"><input type="text" name="registerShipperCongArr[szAddress3]" id="szAddress3" onblur="closeTip('address3');" onfocus="openTip('address3');" value="<?=$_POST['registerShipperCongArr']['szAddress3'] ? $_POST['registerShipperCongArr']['szAddress3'] : $kRegisterShipCon->szAddress3 ?>"/></span>
			<div class="field-alert1"><div id="address3" style="display:none;"><?=t($t_base.'messages/address_blue_box_msg');?></div></div>
		</label>
		<label <?php echo $szLabelClassName; ?>>
			<span class="field-name"><?=t($t_base.'fields/province');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
			<span class="field-container"><input type="text" name="registerShipperCongArr[szProvince]" id="szProvince" onblur="closeTip('p_r_s');" onfocus="openTip('p_r_s');" value="<?=$_POST['registerShipperCongArr']['szProvince'] ? $_POST['registerShipperCongArr']['szProvince'] : $kRegisterShipCon->szState ?>"/></span>
			<div class="field-alert1" ><div id="p_r_s" style="display:none;"><?=t($t_base.'messages/p_r_s');?></div></div>
		</label>
	<br />
	<br />
	<p align="center">
	<input type="hidden" name="registerShipperCongArr[idShipperConsigness]" id="idShipperConsigness" value="<?=$_POST['registerShipperCongArr']['idShipperConsigness'] ? $_POST['registerShipperCongArr']['idShipperConsigness'] : $_REQUEST['idShipperConsigness'] ?>">
	<input type="hidden" name="registerShipperCongArr[flag]" id="flag" value="<?=$_POST['registerShipperCongArr']['flag'] ? $_POST['registerShipperCongArr']['flag'] :$_REQUEST['flag']?>">
	<input type="hidden" name="registerShipperCongArr[idCustomer]" id="idCustomer" value="<?=$_SESSION['user_id']?>">
	<input type="hidden" name="registerShipperCongArr[szPhoneNumberUpdate]" id="szPhoneNumberUpdate" value="">
	<a href="javascript:void(0);" class="button1" onclick="encode_string('szPhoneNumber','szPhoneNumberUpdate');add_edit_register_shiper_consign();"><span><?php if($_REQUEST['flag']=='update' || $_POST['registerShipperCongArr']['flag']=='update') { echo t($t_base.'fields/save'); }else { echo t($t_base.'fields/add'); }?></span></a> <a href="javascript:void(0)" class="button2" onclick="clear_shipper_consigness();"><span><?=t($t_base.'fields/clear');?></span></a> </p>
</form>	