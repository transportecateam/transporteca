<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

$szMetaTitle= __TERMS_CONDITION_PAGE_META_TITLE__;
$szMetaKeywords = __TERMS_CONDITION_PAGE_META_KEYWORDS__;
$szMetaDescription = __TERMS_CONDITION_PAGE_META_DESCRIPTION__;
$style="style='top: 0px; bottom: auto; display: block;'";
$headerSearchFlag=1;
require_once( __APP_PATH_LAYOUT__ . "/header_new.php" );
$KwarehouseSearch=new cWHSSearch();
$iLanguage = getLanguageId();
$tnc=$KwarehouseSearch->selectQuery(4,$iLanguage);
$t_base="TermCondition/";
$replacearray = array(',',' ');
?>
<style>
<!--
.heading {color: black;font-style:normal;text-decoration: none;font-size: 16px}
-->
</style>
<div id="two-column" class="clearfix">
	<div class="hsbody-2-left">
	

			<ol class="customer_tandc">
	<?if($tnc)
	{
		$heading=$tnc[0]['szHeading'];
		$description=$tnc[0]['szDescription'];
		echo  '<li><a href="#'.str_replace($replacearray,"_",strtolower($heading)).'" style="color:#000;font-style:normal;text-decoration:none;" > '.ucfirst($heading).'</a></li>';	
		array_shift($tnc);
		foreach($tnc as $terms)
		{	
			echo '<li><a href="#'.str_replace($replacearray,"_",strtolower($terms['szHeading'])).'"  > '.ucfirst($terms['szHeading']).'</a></li>';
		}
	?>

	</div>	

	<div class="hsbody-2-right">	
		<div class="main-body-container">
			<h5 class="clearfix terms-heading">
				<span class="fl-80"><strong><?=ucfirst($heading)?></strong></span>	
				<a  class="fr" href="#top"><strong><?=t($t_base.'title/top')?></strong></a>
			</h5>
			
			<p><?=$description?></p>
			<br />
			<? foreach($tnc as $terms){?>
			<a name="<?=str_replace($replacearray,"_",strtolower($terms['szHeading']))?>" href="javascript:void(0);" ></a>
			<h5 class="clearfix terms-heading"><span class="fl-80"><strong><?=ucfirst($terms['szHeading'])?></strong></span>
			<a  class="fr" href="#top"><strong><?=t($t_base.'title/top')?></strong></a>
			</h5>
			<div class="link16">
			<?=$terms['szDescription']?>
			</div>
			<br />
			<? }} ?>
			
			<br />
			
			<br />
			<p><a href="<?=__REQUIREMENT_PAGE_URL__?>"><?=t($t_base.'title/booking_link')?></a></p>
			<br />
			<br />
		</div>	
	</div>

</div>
<?php
//include( __APP_PATH__ . "/footer_holding_page.php" );
include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>