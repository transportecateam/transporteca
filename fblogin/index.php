<?php
 ob_start();
session_start(); 
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require( __APP_PATH__ . "/inc/constants.php" );
require ( __APP_PATH__ . '/fblogin/src/facebook.php');
// Create our Application instance (replace this with your appId and secret).
$facebook = new Facebook(array(
  'appId'  => __FB_API_ID_KEY__,
  'secret' => __FB_API_SECRET_KEY__,
  'cookie' => true
));
$_SESSION['google_plus_data']=array();
unset($_SESSION['google_plus_data']);
if(isset($_GET['logout']))       
{
    $url = 'https://www.facebook.com/logout.php?next=' . urlencode('http://demo.phpgang.com/facebook_login_graph_api/') .
      '&access_token='.$_GET['tocken'];
    session_destroy();
    header('Location: '.$url);
}

if(isset($_GET['fbTrue']))
{
    
    $token_url = "https://graph.facebook.com/oauth/access_token?"
       . "client_id=".__FB_API_ID_KEY__."&redirect_uri=" . urlencode(__FB_REDIRECT_URL__)
       . "&client_secret=".__FB_API_SECRET_KEY__."&code=" . $_GET['code']; 
    $response = curl_get_file_contents ($token_url);
    //echo "<br /><br/>";
    //print_r($response);
     $params = null;
     parse_str($response, $params);

     $graph_url = "https://graph.facebook.com/me?access_token=" 
       . $params['access_token']."&fields=name,email";

    $graph_response = curl_get_file_contents ($graph_url);
//    echo "<br /><br/>";
  
     $user = json_decode($graph_response);
     
     if($user->name!='')
     {
        $nameArr=explode(" ",$user->name);
        $_SESSION['fb_data']['szFirstName']=$nameArr[0];
        $_SESSION['fb_data']['szLastName']=$nameArr[1];
     }
     if($user->email!=''){
     $_SESSION['fb_data']['szEmail']=$user->email;
     }    
     //echo "<br /><br/>";
    //print_r($user);
    //echo "<br /><br/>";
   
    if((int)$_REQUEST['iFlag']==1)
    {
        
        echo $_REQUEST['szUrl'];
        die();
    }
    else
    {
        header("Location: ".$_SESSION['redirect_url_site']);
        die();
    }
     //print_r($content);
}
else
{
   // $_SESSION['redirect_url_site']=$_REQUEST['szUrl'];
    $redirectUrl="https://www.facebook.com/dialog/oauth?client_id=".__FB_API_ID_KEY__."&redirect_uri=".__FB_REDIRECT_URL__."&scope=email,public_profile";
   if((int)$_REQUEST['iFlag']==1)
   {
        echo $redirectUrl;
        die();
   }
 else {
       header("Location: ".$redirectUrl);
        die();
   }
    
}
function curl_get_file_contents($URL,$bGeoplugin=false) 
{
    $c = curl_init();
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_URL, $URL);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, FALSE);
    if($bGeoplugin)
    { 
        curl_setopt($c, CURLOPT_TIMEOUT_MS, '5000');
    }

    $contents = curl_exec($c);
    $err  = curl_getinfo($c,CURLINFO_HTTP_CODE);
    curl_close($c);
    if ($contents) return $contents;
    else return FALSE;
 }
?>