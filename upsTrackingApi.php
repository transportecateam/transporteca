<?php
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_header.php" );
//require_once(__APP_PATH_CLASSES__.'/fedex-common.php');
$kBooking = new cBooking();
$kCourierServices = new cCourierServices();
$kWebServices = new cWebServices();
$idBooking = (int)$_POST['idBooking']; 

$szCourierProvider = sanitize_all_html_input(trim($_REQUEST['courier_provider']));

if((int)$idBooking>0)
{
    $bookingArr=$kBooking->getExtendedBookingDetails($idBooking); 
    $courierForwarderAgreementArr=$kCourierServices->getCourierAgreementData($bookingArr['idServiceProvider']);
    $szAccountNumber = decrypt($courierForwarderAgreementArr[0]['szAccountNumber'],ENCRYPT_KEY);
    $szUsername = decrypt($courierForwarderAgreementArr[0]['szUsername'],ENCRYPT_KEY);
    $szPassword = decrypt($courierForwarderAgreementArr[0]['szPassword'],ENCRYPT_KEY); 
    
    $courierTrackingAry = array();
    $courierTrackingAry['szUsername'] = $szUsername;
    $courierTrackingAry['szPassword'] = $szPassword;
    $courierTrackingAry['szAccountNumber'] = $szAccountNumber;
    $courierTrackingAry['szTrackingNumber'] = $bookingArr['szTrackingNumber'];
    
    if($szCourierProvider=='TNT')
    {
        $trackingResponseAry = array();
        $trackingResponseAry = $kWebServices->getTrackingDetailsFromTNT($courierTrackingAry);
    }
    else 
    {
        $trackingResponseAry = array();
        $trackingResponseAry = $kWebServices->getTrackingDetailsFromUPS($courierTrackingAry);
    } 
    $kCourierServices->updateBookingCourierStatus($trackingResponseAry,$idBooking);
    echo $trackingStatusArr['Description'];
}

?>

