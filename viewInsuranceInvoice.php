<?php
/**
 * View Invoice
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

$kBooking = new cBooking();
checkAuth();
$idBooking=$_REQUEST['idBooking'];
if($kBooking->isBookingBelongsToCustomer($idBooking,$_SESSION['user_id']))
{
    $flag=$_REQUEST['flag'];
    $pdfhtmlflag=true;
    $flag = 'pdf'; //We have removed to show HTML so this flag will always be pdf now.
    
    if($flag=='pdf')
    {
        $pdfhtmlflag=false;
    }
    
    $bookingInvoicePdf = getBookingInsuranceInvoicePdf($idBooking,$pdfhtmlflag);
    if($flag=='pdf')
    {
        download_booking_pdf_file($bookingInvoicePdf);
        die;
    }
}
else
{
    header('Location:'.__LANDING_PAGE_URL__);
    exit(); 
} 

?>