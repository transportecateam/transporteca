<?php
ini_set('max_execution_time', 5000);

ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ )  ) );


require_once( __APP_PATH__ . "/src/index.php" );

?>