<?php
/**
 * Customer My Account  
 */
 ob_start();
session_start();

$szMetaTitle= __MY_ACCOUNT_META_TITLE__;
$szMetaKeywords = __MY_ACCOUNT_META_KEYWORDS__;
$szMetaDescription = __MY_ACCOUNT_META_DESCRIPTION__;

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
 
$style="style='top: 0px; bottom: auto; display: block;'";
$headerSearchFlag=1;
require_once( __APP_PATH_LAYOUT__ . "/header_new.php" );

$t_base = "Users/AccountPage/";
checkAuth();
if($_SESSION['user_id']>0)
{
    unset($_SESSION['valid_confirmation']);	
}
//checkPasswordAuthentication($kUser);
 
$kConfig = new cConfig();
$kConfig->loadCountry($kUser->idInternationalDialCode);
$iInternationDialCode = $kConfig->iInternationDialCode;
 
?>
<div id="my-account-body">
			<?php require_once( __APP_PATH_LAYOUT__ ."/nav.php" ); ?> 
			<div class="hsbody-2-right">
			<div id="myaccount_info">
				<div id="user_information">
				<h2 class="create-account-heading"><?=t($t_base.'fields/user_info');?> <a href="javascript:void(0)" onclick="editUserInformation();"><?=t($t_base.'fields/edit');?></a></h2>	
				
				<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/f_name');?></span>
					<span class="field-container"><?=$kUser->szFirstName?></span>
				</div>
                                <div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/l_name');?></span>
					<span class="field-container"><?=$kUser->szLastName?></span>
				</div>
				<div class="ui-fields">
                                    <span class="field-name">&nbsp;</span>
                                    <span class="field-container checkbox-container"><input type="checkbox" disabled="disabled" <?php echo ($kUser->iPrivate==1)?'checked="checked"':''; ?> > <span class="private-text"><?=t($t_base.'fields/private');?></span></span>
				</div>
                                <?php if($kUser->iPrivate!=1){ ?>
				<div class="ui-fields">
                                    <span class="field-name"><?=t($t_base.'fields/c_name');?></span>
                                    <span class="field-container"><?=$kUser->szCompanyName?></span>
				</div>
				<div class="ui-fields">
                                    <span class="field-name"><?=t($t_base.'fields/c_reg_n');?></span>
                                    <span class="field-container"><?=$kUser->szCompanyRegNo?></span>
				</div>
                                <?php } ?>
				<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/address_line1');?></span>
					<span class="field-container"><?=$kUser->szAddress1?></span>
				</div>
				<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/address_line2');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
					<span class="field-container"><?=$kUser->szAddress2?></span>
				</div>
				<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/address_line3');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
					<span class="field-container"><?=$kUser->szAddress3?></span>
				</div>
				<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/postcode');?></span>
					<span class="field-container"><?=$kUser->szPostcode?></span>
				</div>
				<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/city');?></span>
					<span class="field-container"><?=$kUser->szCity?></span>
				</div>
				<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/p_r_s');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
					<span class="field-container"><?=$kUser->szState?></span>
				</div>
				<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/country');?></span>
					<span class="field-container"><?=$kUser->szCountryName?></span>
				</div>
				<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/prefer_currency');?></span>
					<span class="field-container">
						<?=$kUser->szCurrencyName?>
					</span>
				</div>
				</div>
				<br />
				<br />
				<div id="contact_info">
					<h2 class="create-account-heading"><?=t($t_base.'messages/contact_info');?> <a href="javascript:void(0)" onclick="edit_contact_information();"><?=t($t_base.'fields/edit');?></a></h2>
					
					<div class="ui-fields">
						<span class="field-name"><?=t($t_base.'fields/email');?></span>
						<span class="field-container"><?=$kUser->szEmail?> <?php if($kUser->iConfirmed!='1')
						 {?><span style='color:red;font-size:13px;'>(<?=t($t_base.'messages/not_verified');?>)</span> <? }?></span>					
					</div>
					<div class="ui-fields">
						<span class="field-name"><?=t($t_base.'fields/contact_by_email');?></span>
						<span class="field-container checkbox-ab"><input type="checkbox" <?php if($kUser->iIncompleteProfile=='1'){ ?>checked<?php }else if((int)$kUser->iAcceptNewsUpdate=='1'){?> checked <?php }?> disabled="disabled"/><?=t($t_base.'messages/please_news_updates');?></span>
					</div>
					<div class="ui-fields">
						<span class="field-name"><?=t($t_base.'fields/p_number');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
						<span class="field-container"><?php echo "+".$iInternationDialCode." ".$kUser->szPhoneNumber; ?></span>
					</div>
				</div>
				<br />
				<br/>
				<div id="user_change_password">					
						<h2 class="create-account-heading"><?=t($t_base.'fields/password');?> <a href="javascript:void(0);" onclick="change_password();"><?=t($t_base.'fields/edit');?></a></h2>
						<span class="field-container" style="float:left;width:65%;"><?=($kUser->iIncompleteProfile==1)?'<span style="font-style:italic">No password selected</span>':'*************'?></span>					
				</div>
			</div>
			</div>
		</div>
<?php
include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>