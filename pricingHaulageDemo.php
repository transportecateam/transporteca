<?php
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH_LAYOUT__ . "/header.php" );

echo write_text_on_image();
die;

//error_reporting (E_ALL);
//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 1);
$kHaulagePricing = new cHaulagePricing();
//$distance = $kHaulagePricing->getDistance($distanceMeasureAry);
//echo "<br/> distance ".$distance ;


echo '<div id="hsbody">';
if(!empty($_POST['haulagePostAry']))
{
	if(empty($_POST['haulagePostAry']['szLatitude']))
	{
		$error_message = "<br />Latitude is required.";
	}
	if(empty($_POST['haulagePostAry']['szLongitude']))
	{
		$error_message .= "<br />Longitude is required.";
	}
	
	if(empty($_POST['haulagePostAry']['fTotalWeight']))
	{
		$error_message .= "<br />Weight is required.";
	}
	if(empty($_POST['haulagePostAry']['fVolume']))
	{
		$error_message .= "<br />Volume is required.";
	}
/*
	if(empty($_POST['haulagePostAry']['szCityLatitude']))
	{
		$error_message .= "<br />City Latitude is required.";
	}
	if(empty($_POST['haulagePostAry']['szCityLongitude']))
	{
		$error_message .= "<br />City Longitude is required.";
	}	
	if(empty($_POST['haulagePostAry']['szCity']))
	{
		$error_message .= "<br />City is required.";
	}
*/	
	if(empty($_POST['haulagePostAry']['szPostcode']))
	{
		$error_message .= "<br />Postcode is required.";
	}
	if(empty($_POST['haulagePostAry']['iDistance']))
	{
		$error_message .= "<br />Distance is required.";
	}
	
	if(!empty($error_message))
	{
		echo "<p style='color:red;'>".$error_message."</p>";
	}
	else
	{
		$idWarehouse=1;
		$haulageModelsAry = array();
		$iDirection = 1;
		$haulageModelsAry = $kHaulagePricing->getAllActiveHaulageModelsByCFS($idWarehouse,$iDirection);
		
		if(!empty($haulageModelsAry))
		{
			foreach($haulageModelsAry as $haulageModelsArys)
			{
				//zone pricing
				if($haulageModelsArys['idHaulageModel']==1)
				{
					echo "<br /> ****** Searching in Zones ******* <br />";
					$data=array();
					$data['szLatitude'] = $_POST['haulagePostAry']['szLatitude'] ;
					$data['szLongitude'] = $_POST['haulagePostAry']['szLongitude'] ;
					$data['idHaulagePricingModel'] = $haulageModelsArys['id'];
					echo "<br> Started search in ".$haulageModelsArys['szName']." zone.<br>";
					if($kHaulagePricing->isLocationExistsInZone($data))
					{
						$haulageDataAry=array();
						$haulageDataAry['idHaulagePricingModel'] = $haulageModelsArys['id'];
						$haulageDataAry['fTotalWeight'] = $_POST['haulagePostAry']['fTotalWeight'] ;
						$haulageDataAry['fVolume'] = $_POST['haulagePostAry']['fVolume'];
						$haulageDataAry['iHaulageTransitTime'] =  $haulageModelsArys['iTransitHours'];
						$haulageDataAry['fWmFactor'] =  $haulageModelsArys['fWmFactor'];
						$haulageDataAry['fFuelPercentage'] =  $haulageModelsArys['fFuelPercentage'];
						$haulagePricingAry = $kHaulagePricing->calculateHaulageZonePricing($haulageDataAry);
						if(!empty($haulagePricingAry))
						{
							echo "<br />";
							echo "<br /><h4 > Total Haulage price: <span style='color:green;'>".$haulagePricingAry['szCurrency']." ".$haulagePricingAry['fTotalHaulgePrice']."</span></h4>";
							echo "<br /><h4> Total Transit time: <span style='color:green;'>".$haulagePricingAry['iHaulageTransitTime']." hours</span></h4>";
							echo "<br />";
							break;
						}
						else
						{
							echo "<br><br> Location found but pricing not found.<br>" ;
						}
					}
					echo "<br><br>";
				}
				if($haulageModelsArys['idHaulageModel']==2)
				{
					echo "<br /> ****** Searching in City pricing ******* <br />";
					echo "<br> Started search in ".$haulageModelsArys['szName'].".<br>";
					
					$data=array();
					$data['iDistance'] = $_POST['haulagePostAry']['iDistance'];
					$data['szCity'] = $_POST['haulagePostAry']['szCity'];
					$data['szLatitude'] = $_POST['haulagePostAry']['szLatitude'];
					$data['szLongitude'] = $_POST['haulagePostAry']['szLongitude'];
					
					$data['szCityLatitude'] = $_POST['haulagePostAry']['szCityLatitude'];
					$data['szCityLongitude'] = $_POST['haulagePostAry']['szCityLongitude'];
					
					$data['idHaulagePricingModel'] = $haulageModelsArys['id'];
					$data['iRadiousKM'] = $haulageModelsArys['iRadiousKM'];
					
					if($kHaulagePricing->isLocationExistsByCity($data))
					{						
						$haulageDataAry=array();
						$haulageDataAry['idHaulagePricingModel'] = $haulageModelsArys['id'];
						$haulageDataAry['fTotalWeight'] = $_POST['haulagePostAry']['fTotalWeight'] ;
						$haulageDataAry['fVolume'] = $_POST['haulagePostAry']['fVolume'];
						$haulageDataAry['iHaulageTransitTime'] =  $haulageModelsArys['iTransitHours'];
						$haulageDataAry['fWmFactor'] =  $haulageModelsArys['fWmFactor'];
						$haulageDataAry['fFuelPercentage'] =  $haulageModelsArys['fFuelPercentage'];
						//print_r($haulageDataAry);
						
						$haulagePricingAry = $kHaulagePricing->calculateHaulageZonePricing($haulageDataAry);
						//echo "<br> ";
						//print_R($haulagePricingAry);
						if(!empty($haulagePricingAry))
						{
							echo "<br />";
							echo "<br /><h4 > Total Haulage price: <span style='color:green;'>".$haulagePricingAry['szCurrency']." ".$haulagePricingAry['fTotalHaulgePrice']."</span></h4>";
							echo "<br /><h4> Total Transit time: <span style='color:green;'>".$haulagePricingAry['iHaulageTransitTime']." hours</span></h4>";
							echo "<br />";
							break;
						}
						else
						{
							echo "<br><br> Location found but pricing not found.<br>" ;
						}
					}
				}
				if($haulageModelsArys['idHaulageModel']==3)
				{
					echo "<br /> ****** Searching in postcode pricing ******* <br />";
					//echo "<br> Search postcode started with ".$haulageModelsArys['szName'].".<br>";
					$data=array();
					$data['szPostcode'] = $_POST['haulagePostAry']['szPostcode'] ;
					$data['idHaulagePricingModel'] = $haulageModelsArys['id'];
					
					if($kHaulagePricing->isLocationExistsByPostcode($data))
					{
						$haulageDataAry=array();
						$haulageDataAry['idHaulagePricingModel'] = $haulageModelsArys['id'];
						$haulageDataAry['fTotalWeight'] = $_POST['haulagePostAry']['fTotalWeight'] ;
						$haulageDataAry['fVolume'] = $_POST['haulagePostAry']['fVolume'];
						$haulageDataAry['iHaulageTransitTime'] =  $haulageModelsArys['iTransitHours'];
						$haulageDataAry['fWmFactor'] =  $haulageModelsArys['fWmFactor'];
						$haulageDataAry['fFuelPercentage'] =  $haulageModelsArys['fFuelPercentage'];
						$haulagePricingAry = $kHaulagePricing->calculateHaulageZonePricing($haulageDataAry);
						if(!empty($haulagePricingAry))
						{
							echo "<br />";
							echo "<br /><h4 > Total Haulage price: <span style='color:green;'>".$haulagePricingAry['szCurrency']." ".$haulagePricingAry['fTotalHaulgePrice']."</span></h4>";
							echo "<br /><h4> Total Transit time: <span style='color:green;'>".$haulagePricingAry['iHaulageTransitTime']." hours</span></h4>";
							echo "<br />";
							break;
						}
						else
						{
							echo "<br><br> Location found but pricing not found.<br>" ;
						}
					}
				}
				if($haulageModelsArys['idHaulageModel']==4)
				{
					echo "<br /> ****** Searching by Distance pricing ******* <br />";
					echo "<br> Started search at ".$haulageModelsArys['iDistanceUpToKm']." km.<br>";
					
					$data=array();
					$data['iDistance'] = $_POST['haulagePostAry']['iDistance'];
					$data['idHaulagePricingModel'] = $haulageModelsArys['id'];
					if($kHaulagePricing->isLocationExistsByDistance($data))
					{						
						$haulageDataAry=array();
						$haulageDataAry['idHaulagePricingModel'] = $haulageModelsArys['id'];
						$haulageDataAry['fTotalWeight'] = $_POST['haulagePostAry']['fTotalWeight'] ;
						$haulageDataAry['fVolume'] = $_POST['haulagePostAry']['fVolume'];
						$haulageDataAry['iHaulageTransitTime'] =  $haulageModelsArys['iTransitHours'];
						$haulageDataAry['fWmFactor'] =  $haulageModelsArys['fWmFactor'];
						$haulageDataAry['fFuelPercentage'] =  $haulageModelsArys['fFuelPercentage'];
						$haulagePricingAry = $kHaulagePricing->calculateHaulageZonePricing($haulageDataAry);
						if(!empty($haulagePricingAry))
						{
							echo "<br />";
							echo "<br /><h4 > Total Haulage price: <span style='color:green;'>".$haulagePricingAry['szCurrency']." ".$haulagePricingAry['fTotalHaulgePrice']."</span></h4>";
							echo "<br /><h4> Total Transit time: <span style='color:green;'>".$haulagePricingAry['iHaulageTransitTime']." hours</span></h4>";
							echo "<br />";
							break;
						}
						else
						{
							echo "<br><br> Location found but pricing not found.<br>" ;
						}
					}
				}
			}
		}
	}
}
?>

	<form action="" method="post" >
		<table>
			<tr>
				<td colspan="2"><h3>Haulage pricing calculation demo</h3></td>
			</tr>
			<tr>
				<td>Latitude</td>
				<td><input type="text" name="haulagePostAry[szLatitude]" value="<?=$_POST['haulagePostAry']['szLatitude']?>" ></td>
			</tr>
			<tr>
				<td>Longitude</td>
				<td><input type="text" name="haulagePostAry[szLongitude]" value="<?=$_POST['haulagePostAry']['szLongitude']?>" ></td>
			</tr>
			<tr>
				<td>Weight(kg)</td>
				<td><input type="text" name="haulagePostAry[fTotalWeight]" value="<?=$_POST['haulagePostAry']['fTotalWeight']?>" ></td>
			</tr>
			<tr>
				<td>Volume(cbm)</td>
				<td><input type="text" name="haulagePostAry[fVolume]" value="<?=$_POST['haulagePostAry']['fVolume']?>" ></td>
			</tr>
			<!-- 
			<tr>
				<td>City</td>
				<td><input type="text" name="haulagePostAry[szCity]" value="<?=$_POST['haulagePostAry']['szCity']?>" ></td>
			</tr>
			<tr>
				<td>City Latitude</td>
				<td><input type="text" name="haulagePostAry[szCityLatitude]" value="<?=$_POST['haulagePostAry']['szCityLatitude']?>" ></td>
			</tr>
			<tr>
				<td>City Longitude</td>
				<td><input type="text" name="haulagePostAry[szCityLongitude]" value="<?=$_POST['haulagePostAry']['szCityLongitude']?>" ></td>
			</tr>
			 -->
			<tr>
				<td>Postcode Starting with</td>
				<td><input type="text" name="haulagePostAry[szPostcode]" value="<?=$_POST['haulagePostAry']['szPostcode']?>" ></td>
			</tr>
			<tr>
				<td>Distance(km)</td>
				<td><input type="text" name="haulagePostAry[iDistance]" value="<?=$_POST['haulagePostAry']['iDistance']?>" ></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<input type="submit" name="haulagePostAry[submit]" value="Search & Calculate">
				</td>
			</tr>
		</table>
	</form>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/footer.php" );
?>