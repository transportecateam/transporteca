<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
//ini_set('error_reporting',E_ALL);
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

$iGetLanguageFromRequest = 1; 
require_once(__APP_PATH_LAYOUT__."/ajax_header.php");
$t_base = "SelectService/";
$kWHSSearch=new cWHSSearch();
$kConfig = new cConfig();
$szAction = sanitize_all_html_input(trim($_REQUEST['operation'])); 
if((int)$_SESSION['booking_id']<=0)
{
    $_SESSION['sess_search_ary_ajax'] = array();
    unset($_SESSION['sess_search_ary_ajax']);
}
//echo "Action: ".$szAction;
if($szAction == 'CLEAR_SEARCH')
{
    if((int)$_SESSION['booking_id']>0)
    {
        $kBooking = new cBooking();
        if($kBooking->deleteTempSearchedDataByBookingId($_SESSION['booking_id']))
        {
            $_SESSION['booking_id']='';
            $_SESSION['sess_search_ary_ajax'] = array();
            unset($_SESSION['sess_search_ary_ajax']);
            unset($_SESSION['booking_id']);		
            $_SESSION['booking_register_shipper_id'] = '';
            $_SESSION['booking_register_consignee_id'] = '';
            unset($_SESSION['booking_register_shipper_id']);
            unset($_SESSION['booking_register_consignee_id']);
            die;
        }
    }
    die;
}
else if($szAction=='UPDATE_SERVICE_LISTING')
{	
    $iIgnoreSearchNotificationPopup = (int)sanitize_all_html_input($_REQUEST['ignore_notification_popup']);
    
    $postSearchAry=$_REQUEST['searchAry'];
    $kConfig=new cConfig();
    $updateShipConDetailsAry = array();
    $origionGeoCountryAry = array();
    $destinationGeoCountryAry = array(); 
    
    $kBooking=new cBooking();
    $szBookingRandomNum = $postSearchAry['szBookingRandomNum'];		
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
    
    $postSearchNewAry = array();
    $postSearchNewAry = $kBooking->getBookingDetails($idBooking); 

    if((($postSearchNewAry['idBookingStatus']=='3') || ($postSearchNewAry['idBookingStatus']=='4')) && ($postSearchNewAry['iQuickQuote']!=__QUICK_QUOTE_MAKE_BOOKING__))
    {
        echo "POPUP||||";
        echo display_booking_already_paid($t_base,$idBooking);
        die;
    } 
        
    if($postSearchAry['szOriginCountryStr']!='')
    {
        $szOriginCountryArr=reverse_geocode($postSearchAry['szOriginCountryStr'],true);
        $origionGeoCountryAry = $szOriginCountryArr ;
        $postSearchAry['szOriginCountry']=$szOriginCountryArr['szCountryName'];
        $idOriginCountry=$kConfig->getCountryIdByCountryName($szOriginCountryArr['szCountryName'],$iLanguage);
        
        if($idOriginCountry<=0 && !empty($szOriginCountryArr['szCountryCode']))
        {
            $kConfig->loadCountry(false,$szOriginCountryArr['szCountryCode']);
            $idOriginCountry = $kConfig->idCountry ; 
        }
        
        $postSearchAry['szOriginCountry']=$idOriginCountry;
        $postSearchAry['szOLat']=$szOriginCountryArr['szLat'];
        $postSearchAry['szOLng']=$szOriginCountryArr['szLng'];
        $postSearchAry['szOriginCity']=$szOriginCountryArr['szCityName']; 
        $postSearchAry['szOriginPostCode'] = $szOriginCountryArr['szPostCode']; 
        $postSearchAry['szOriginPostCodeTemp'] = $szOriginCountryArr['szPostcodeTemp'];
        
        if(empty($postSearchAry['szOriginPostCode']))
        {
            $postcodeCheckAry=array();
            $postcodeResultAry = array();

            $postcodeCheckAry['idCountry'] = $idOriginCountry ;
            $postcodeCheckAry['szLatitute'] = $szOriginCountryArr['szLat'];
            $postcodeCheckAry['szLongitute'] = $szOriginCountryArr['szLng'];

            $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry); 
            
            if(!empty($postcodeResultAry))
            {
                $postSearchAry['szOriginPostCode'] = $postcodeResultAry['szPostCode'];
                $postSearchAry['szOriginPostCodeTemp'] = $postcodeResultAry['szPostCode']; 
            }
        } 
    }
    if($postSearchAry['szDestinationCountryStr']!='')
    {
        $szDesCountryArr = reverse_geocode($postSearchAry['szDestinationCountryStr'],true);
        $destinationGeoCountryAry = $szDesCountryArr ;
        $postSearchAry['szDestinationCountry']=$szDesCountryArr['szCountryName'];
        $idDesCountry=$kConfig->getCountryIdByCountryName($szDesCountryArr['szCountryName'],$iLanguage);
        
        if($idDesCountry<=0 && !empty($szDesCountryArr['szCountryCode']))
        {
            $kConfig->loadCountry(false,$szDesCountryArr['szCountryCode']);
            $idDesCountry = $kConfig->idCountry ; 
        }
        
        $postSearchAry['szDestinationCountry']=$idDesCountry;
        $postSearchAry['szDLat']=$szDesCountryArr['szLat'];
        $postSearchAry['szDLng']=$szDesCountryArr['szLng'];
        $postSearchAry['szDestinationCity']=$szDesCountryArr['szCityName']; 
        $postSearchAry['szDestinationPostCode'] = $szDesCountryArr['szPostCode']; 
        $postSearchAry['szDestinationPostCodeTemp'] = $szDesCountryArr['szPostcodeTemp'];
        
        if(empty($postSearchAry['szDestinationPostCode']))
        {
            $postcodeCheckAry = array(); 
            $postcodeResultAry = array(); 
            $postcodeCheckAry['idCountry'] = $idDesCountry ;
            $postcodeCheckAry['szLatitute'] = $szDesCountryArr['szLat'] ;
            $postcodeCheckAry['szLongitute'] = $szDesCountryArr['szLng'] ;
            $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry);

            if(!empty($postcodeResultAry))
            {
                $postSearchAry['szDestinationPostCode'] = $postcodeResultAry['szPostCode'];
                $postSearchAry['szDestinationPostCodeTemp'] = $postcodeResultAry['szPostCode'];
            }
        } 
    } 
    
    if($iIgnoreSearchNotificationPopup!=1)
    {
        /*
        * checking for search notification popup
        */
        $dataSearchNotification = array();
        $dataSearchNotification = $postSearchAry;
        $dataSearchNotification['idOriginCountry'] = $idOriginCountry;
        $dataSearchNotification['idDestinationCountry'] = $idDesCountry;  

        $searchNotificationArr = array();
        $kExplain = new cExplain();
        $searchNotificationArr = $kExplain->isSearchNotificationExists($dataSearchNotification,false,true);

        if(!empty($searchNotificationArr))
        {
            /*
            * Displaying search notification popup
             */ 

            $kConfig = new cConfig();
            $kConfig->loadCountry($idOriginCountry);
            $szCountryStrUrl = $kConfig->szCountryISO ;
            $kConfig->loadCountry($idDesCountry);
            $szCountryStrUrl .= $kConfig->szCountryISO ;

            echo "SUCCESS_LOCAL_SEARCH||||";
            echo display_local_search_popup_new($searchNotificationArr,$idBooking,$szCountryStrUrl,$iNumRecordFound,"UPDATE_SERVICE_LIST",$iHiddenValue);
            die; 
        }
    } 
    
    $iShipperConsigneeFlag = false;
    if((int)$_SESSION['user_id']>0)
    {
        $kUser = new cUser();
        $kUser->getUserDetails($_SESSION['user_id']); 
        $idCustomerCountry = $kUser->szCountry;  
    } 
    else 
    {
        $userIpDetailsAry = array();
        $userIpDetailsAry = getCountryCodeByIPAddress(); 
        $szUserCountryName  = $userIpDetailsAry['szCountryCode'] ; 

        if(!empty($szUserCountryName))
        {
            $kConfigCountry = new cConfig();
            $kConfigCountry->loadCountry(false,$szUserCountryName);

            $idCustomerCountry = $kConfigCountry->idCountry;
        }
    }
    
    /*
    * If customer country = Origin country the we searches for service type: DTP
    */
    if($idCustomerCountry == $idOriginCountry)
    {
        $iShipperConsigneeFlag = 1;
    }
    else if($idCustomerCountry == $idDestinationCountry)
    {
        $iShipperConsigneeFlag = 1;
    }
    else
    {
        $iShipperConsigneeFlag = 3;
    }
    
    if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__)
    {
        $postSearchAry['idCC'][0] = '1';
        $postSearchAry['idCC'][1]='2';
        
        $postSearchAry['iOriginCC'] = 1;
        $postSearchAry['iDestinationCC'] = 2;
    }
    else if($iShipperConsigneeFlag==1) //DTP with Origin custom clearance
    {
        $postSearchAry['idCC'][0] = '1';
        $postSearchAry['idServiceType'] = __SERVICE_TYPE_DTP__;
        
        $postSearchAry['iOriginCC'] = 1;
    } 
    else if($iShipperConsigneeFlag==3) //DTP with Origin custom clearance
    {
        if($postSearchAry['iPickup']==1)
        {
            $idServiceType = __SERVICE_TYPE_DTD__;
            $iOriginCC = 1;
            $iDestinationCC = 1;
        } 
        else if($postSearchAry['iPickup']==2)
        {
            $idServiceType = __SERVICE_TYPE_DTP__;
            $iOriginCC = 1;
            $iDestinationCC = 0;
        } 
        else
        {
            $idServiceType = __SERVICE_TYPE_PTD__;
            $iOriginCC = 0;
            $iDestinationCC = 2;
        } 
        $postSearchAry['idCC'][0] = $iOriginCC;
        $postSearchAry['idCC'][1]= $iDestinationCC;

        $postSearchAry['iOriginCC'] = $iOriginCC;
        $postSearchAry['iDestinationCC'] = $iDestinationCC;
        $postSearchAry['idServiceType'] = $idServiceType;
    }
    else
    {
        $postSearchAry['idServiceType'] = __SERVICE_TYPE_PTD__; 
        $postSearchAry['idCC'][1]='2';
        $postSearchAry['iDestinationCC'] = 2;
    }  
    $iSearchMiniPage = (int)sanitize_all_html_input($postSearchAry['iSearchMiniPage']);
    
    $iSearchMiniPageVersion = 0;
    if($iSearchMiniPage>2)
    {
        $iSearchMiniPageVersion = $iSearchMiniPage;
    }

    $searchFormAry = array();
    $searchFormAry = $postSearchAry ;
    $searchFormAry['szPageName'] = 'SIMPLEPAGE'; 
    
    $kConfig->validateLandingPage($searchFormAry,array(),$iSearchMiniPageVersion);

    $postSearchAry['iBookingStep']=1;
    $postSearchAry['iWeight']=$postSearchAry['iWeight'];
    $postSearchAry['iQuantity'][1]='1';
    $postSearchAry['idWeightMeasure'][1]='1';
    $postSearchAry['iFromRequirementPage']=2; 
    
    if(!empty($kConfig->arErrorMessages))
    {
        echo "ERROR||||";			
        $hidden_search_form=false;	
        $id_suffix = "";		
        if($postSearchAry['iHiddenValue']==1)
        {
            $id_suffix = "_hiden";
        }
        $szErrorMessageStr = '';
        foreach($kConfig->arErrorMessages as $errorKey=>$errorValue)
        {
            if($errorKey=='szOriginCountry')
            {
                $errorKey = 'szOriginCountryStr'.$id_suffix.'_container+++++'.$errorValue.'+++++szOriginCountryStr'.$id_suffix;
            } 
            if($errorKey=='szDestinationCountry')
            {
                $errorKey = 'szDestinationCountryStr'.$id_suffix.'_container+++++'.$errorValue;
            } 
            if($errorKey=='szDestinationCountry')
            {
                $errorKey = 'szDestinationCountryStr'.$id_suffix.'_container+++++'.$errorValue;
            }
            if($errorKey=='fCargoWeight_landing_page')
            {
                $errorKey = 'iWeight'.$id_suffix.'_container+++++'.$errorValue;
            }
            if($errorKey=='iVolume')
            {
                $errorKey = 'iVolume'.$id_suffix.'_container+++++'.$errorValue;
            }
            if($errorKey=='dtTiming')
            {
                $errorKey = 'dtTiming'.$id_suffix.'_container+++++'.$errorValue;
            }				
            $szErrorMessageStr .= $errorKey."$$$$";
        }		
        echo $szErrorMessageStr ; 
        die;
    }
    else
    {
        $kBooking=new cBooking();
        $szBookingRandomNum = $postSearchAry['szBookingRandomNum'];		
        $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
        
        $searchedBookingAry = array();
        $searchedBookingAry = $kBooking->getBookingDetails($idBooking);
        
        $fOldCargoVolume = (float)$searchedBookingAry['fCargoVolume'];
        $fOldCargoWeight = (float)$searchedBookingAry['fCargoWeight'];
        
        if(!empty($_SESSION['load-booking-details-page-for-booking-id']))
        {
            $_SESSION['load-booking-details-page-for-booking-id'] = '';
            unset($_SESSION['load-booking-details-page-for-booking-id']);
        }

        $cookieValue=$kConfig->szOriginCountryName."||||".$kConfig->szOriginCountry."||||".$kConfig->szDestinationCountryName."||||".$kConfig->szDestinationCountry."||||".$kConfig->idServiceType."||||".$kConfig->iVolume."||||".$kConfig->iWeight."||||".$kConfig->dtTiming;
 
        if(__ENVIRONMENT__ == "LIVE")
        {
            setcookie("__SIMPLE_SEARCH_COOKIE__", $cookieValue, time()+(3600*24*90),'/',$_SERVER['HTTP_HOST'],true);
        }
        else
        {
            setcookie("__SIMPLE_SEARCH_COOKIE__", $cookieValue, time()+(3600*24*90),'/');
        }

        $szReuquestPageUrl = __SELECT_SERVICES_URL__."/".$szBookingRandomNum."/";
        if($idBooking>0)
        {
            $kBooking->save($kConfig,$idBooking,$szReuquestPageUrl,true);
        }
        else
        {
            $kBooking->add($kConfig,__BOOKING_STATUS_ZERO_LEVEL__,$szReuquestPageUrl,true);
            $idBooking = $kBooking->idBooking;
        }	 
        $postSearchAry = $kBooking->getBookingDetails($idBooking);  
        if($postSearchAry['idShipperConsignee']>0)
        {
            $shipperConsigneeAry = array();
            $idShipperConsignee = $postSearchAry['idShipperConsignee'];  

            $szShipperAddressGoe = trim($origionGeoCountryAry['szStreetNumber']);

            $postcodeCheckAry=array();
            $postcodeResultAry = array();
            $kConfig_new = new cConfig();

            $postcodeCheckAry['idCountry'] = $postSearchAry['idOriginCountry'] ;
            $postcodeCheckAry['szLatitute'] = $origionGeoCountryAry['szLat'] ;
            $postcodeCheckAry['szLongitute'] = $origionGeoCountryAry['szLng'] ;
            $postcodeResultAry = $kConfig_new->getPostcodeFromGoogleCordinate($postcodeCheckAry);

            $origionGeoCountryAry['szShipperCity'] = "";
            $origionGeoCountryAry['szShipperPostcode'] = "";
            $origionGeoCountryAry['szShipperAddress'] = "";

            $iGotDataFromDB = false;
            if(!empty($postcodeResultAry))
            {
                if(empty($origionGeoCountryAry['szCityName']))
                {
                    $origionGeoCountryAry['szCityName'] = $postcodeResultAry['szCity'];
                }
                if(empty($origionGeoCountryAry['szPostCode']))
                {
                    $origionGeoCountryAry['szPostCode'] = $postcodeResultAry['szPostCode'];
                    $iGotDataFromDB = true;
                }
            }
            if(!empty($szShipperAddressGoe) && !empty($origionGeoCountryAry['szRoute']))
            {
                $szShipperAddressGoe = ", ".$origionGeoCountryAry['szRoute'] ;
            }
            else
            {
                $szShipperAddressGoe = $origionGeoCountryAry['szRoute'] ;
            }

            $shipperConsigneeAry['iGetShipperAddressFromGoogle'] = 0;
            $shipperConsigneeAry['iGetShipperPostcodeFromGoogle'] = 0;

            if(!empty($szShipperAddressGoe))
            {
                $shipperConsigneeAry['iGetShipperAddressFromGoogle'] = 1 ;
                $shipperConsigneeAry['szShipperAddress'] = $szShipperAddressGoe ;
            }
            if(!empty($origionGeoCountryAry['szCityName']))
            {
                $shipperConsigneeAry['szShipperCity'] = $origionGeoCountryAry['szCityName'];
                $shipperConsigneeAry['iGetShipperCityFromGoogle'] = 1 ;
            }
            if(!empty($origionGeoCountryAry['szPostCode']))
            {
                $shipperConsigneeAry['szShipperPostcode'] = $origionGeoCountryAry['szPostCode']; 

                if(!$iGotDataFromDB)
                {
                    $shipperConsigneeAry['iGetShipperPostcodeFromGoogle'] = 1 ;		  
                } 
            }  	
            $shipperConsigneeAry['idShipperDialCode'] = $postSearchAry['idOriginCountry'];
            $shipperConsigneeAry['idShipperCountry'] = $postSearchAry['idOriginCountry']; 

            $postcodeCheckAry=array();
            $postcodeResultAry = array();

            $postcodeCheckAry['idCountry'] = $postSearchAry['idDestinationCountry'] ;
            $postcodeCheckAry['szLatitute'] = $destinationGeoCountryAry['szLat'] ;
            $postcodeCheckAry['szLongitute'] = $destinationGeoCountryAry['szLng'] ;
            $postcodeResultAry = $kConfig_new->getPostcodeFromGoogleCordinate($postcodeCheckAry);
            $iGotDataFromDB = false;
            if(!empty($postcodeResultAry))
            {
                if(empty($destinationGeoCountryAry['szCityName']))
                {
                    $destinationGeoCountryAry['szCityName'] = trim($postcodeResultAry['szCity']);
                }
                if(empty($destinationGeoCountryAry['szPostCode']))
                {
                    $destinationGeoCountryAry['szPostCode'] = trim($postcodeResultAry['szPostCode']);
                    $iGotDataFromDB = true;
                }
            }

            $szShipperAddressGoe = trim($destinationGeoCountryAry['szStreetNumber']); 
            
            if(!empty($szShipperAddressGoe) && !empty($destinationGeoCountryAry['szRoute']))
            {
                $szShipperAddressGoe .= ", ".$destinationGeoCountryAry['szRoute'] ;
            }
            else
            {
                $szShipperAddressGoe .= $destinationGeoCountryAry['szRoute'] ;
            }

            $shipperConsigneeAry['iGetConsigneeAddressFromGoogle'] = 0;
            $shipperConsigneeAry['iGetConsigneePostcodeFromGoogle'] = 0;
            $shipperConsigneeAry['iGetConsigneeCityFromGoogle'] = 0;
            $shipperConsigneeAry['szConsigneeCity'] = '';
            //$shipperConsigneeAry['szConsigneeAddress'] = '';
            $shipperConsigneeAry['szConsigneePostCode'] = '';

            if(!empty($szShipperAddressGoe))
            {
                $shipperConsigneeAry['szConsigneeAddress'] = $szShipperAddressGoe ;
                $shipperConsigneeAry['iGetConsigneeAddressFromGoogle'] = 1 ;
            }
            if(!empty($destinationGeoCountryAry['szPostCode']))
            {
                $shipperConsigneeAry['szConsigneePostCode'] = $destinationGeoCountryAry['szPostCode'];
                if(!$iGotDataFromDB)
                {
                    $shipperConsigneeAry['iGetConsigneePostcodeFromGoogle'] = 1 ;
                }
            }
            if(!empty($destinationGeoCountryAry['szCityName']))
            {
                $shipperConsigneeAry['szConsigneeCity'] = $destinationGeoCountryAry['szCityName']; 
                $shipperConsigneeAry['iGetConsigneeCityFromGoogle'] = 1 ;
            }   
            $shipperConsigneeAry['idConsigneeDialCode'] = $postSearchAry['idDestinationCountry']; 
            $shipperConsigneeAry['idConsigneeCountry'] = $postSearchAry['idDestinationCountry']; 
               
            $kUser = new cUser();
            $kUser->getUserDetails($_SESSION['user_id']);
            $iShipperConsignee = 3;
            if(($kUser->szCountry == $shipperConsigneeAry['idShipperCountry']) && ($kUser->szCity == $shipperConsigneeAry['szShipperCity']))
            {
                $iShipperConsignee = 1;
            }
            else if(($kUser->szCountry == $shipperConsigneeAry['idConsigneeCountry']) && ($kUser->szCity == $shipperConsigneeAry['szConsigneeCity']))
            {
                $iShipperConsignee = 2;
            } 
            $shipperConsigneeAry['iShipperConsignee'] = $iShipperConsignee ; 
            if(!empty($shipperConsigneeAry))
            {
                $update_ship_con_query = '';
                foreach($shipperConsigneeAry as $key=>$value)
                {
                    $update_ship_con_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                } 
                $kRegisterShipCon = new cRegisterShipCon();
                $update_ship_con_query = rtrim($update_ship_con_query,",");  
                //echo $update_ship_con_query ;
                $kRegisterShipCon->updateShipperConsigneeDetails($update_ship_con_query,$idShipperConsignee);
            }  
        } 
        $fCargoVolume = (float)$postSearchAry['fCargoVolume'];
        $CargoWeight = (float)$postSearchAry['fCargoWeight'];
    }	   
    
    if(($fOldCargoVolume*200)>$fOldCargoWeight)
    {
        $fOldChargeableWeight = round($fOldCargoVolume*200) ;
    }
    else
    {
        $fOldChargeableWeight = $fOldCargoWeight ;
    }
    
    if(($fCargoVolume*200)>$CargoWeight)
    {
        $fNewChargeableWeight = round($fCargoVolume*200) ;
    }
    else
    {
        $fNewChargeableWeight = $CargoWeight ;
    }
    //echo "Oldcgb: ".$fOldChargeableWeight." New wt: ".$fNewChargeableWeight ;
    if((int)$fNewChargeableWeight!=(int)$fOldChargeableWeight)
    { 
        //If User changes Volume or weight then we kind of remove old package details
        $kBooking->deleteCargoByBookingId($idBooking);
    }  
    $searchResultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,false,$idBooking); 
    
    $szCourierCalculationLogString = $kWHSSearch->szCalculationLogString ;
      
    $postSearchAry['iNumRecordFound'] = count($searchResultAry);
    $kBooking->addSelectServiceData($postSearchAry);
    $searchResultAry = sortArray($searchResultAry,'fDisplayPrice',false,'iCuttOffDate_time',false,'iAvailabeleDate_time',false,true);
    $kBooking->insertBookingSearchLogs($postSearchAry); 
    if(!empty($searchResultAry))
    {
        echo "SUCCESS||||";
        echo display_new_searvice_listing($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,$forwarder_flag,false,false,$szCourierCalculationLogString);
        echo "||||".round($postSearchAry['fCargoVolume'],2);
        echo "||||".round($postSearchAry['fCargoWeight'],2);
        echo "||||".ucwords(strtolower($postSearchAry['szOriginCountry']));
        echo "||||".ucwords(strtolower($postSearchAry['szDestinationCountry']));
        die;
    }
    else
    {
        $res_ary=array();
        if((int)$postSearchAry['iBookingStep']<7)
        {
            $res_ary['iBookingStep'] = 7 ;
        }
        $res_ary['iBookingQuotes'] = 1 ;
        $res_ary['iQuotesStatus'] = 0 ;
        $res_ary['iGetQuotePageFlag'] = 1 ;
        
        if(!empty($res_ary))
        {
            $update_query = '';
            foreach($res_ary as $key=>$value)
            {
                $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
            }
        } 
        $update_query = rtrim($update_query,",");
        if($kBooking->updateDraftBooking($update_query,$idBooking))
        { 
            if($postSearchAry['idShipperConsignee']>0)
            {
                $shipperConsigneeAry = array();
                $idShipperConsignee = $postSearchAry['idShipperConsignee'];  

                $szShipperAddressGoe = trim($origionGeoCountryAry['szStreetNumber']);

                $postcodeCheckAry=array();
                $postcodeResultAry = array();
                $kConfig_new = new cConfig();

                $postcodeCheckAry['idCountry'] = $postSearchAry['idOriginCountry'] ;
                $postcodeCheckAry['szLatitute'] = $origionGeoCountryAry['szLat'] ;
                $postcodeCheckAry['szLongitute'] = $origionGeoCountryAry['szLng'] ;
                $postcodeResultAry = $kConfig_new->getPostcodeFromGoogleCordinate($postcodeCheckAry);

                $origionGeoCountryAry['szShipperCity'] = "";
                $origionGeoCountryAry['szShipperPostcode'] = "";
                $origionGeoCountryAry['szShipperAddress'] = "";

                $iGotDataFromDB = false;
                if(!empty($postcodeResultAry))
                {
                    if(empty($origionGeoCountryAry['szCityName']))
                    {
                        $origionGeoCountryAry['szCityName'] = $postcodeResultAry['szCity'];
                    }
                    if(empty($origionGeoCountryAry['szPostCode']))
                    {
                        $origionGeoCountryAry['szPostCode'] = $postcodeResultAry['szPostCode'];
                        $iGotDataFromDB = true;
                    }
                }
                if(!empty($szShipperAddressGoe) && !empty($origionGeoCountryAry['szRoute']))
                {
                    $szShipperAddressGoe = ", ".$origionGeoCountryAry['szRoute'] ;
                }
                else
                {
                    $szShipperAddressGoe = $origionGeoCountryAry['szRoute'] ;
                }

                $shipperConsigneeAry['iGetShipperAddressFromGoogle'] = 0;
                $shipperConsigneeAry['iGetShipperPostcodeFromGoogle'] = 0;

                if(!empty($szShipperAddressGoe))
                {
                    $shipperConsigneeAry['iGetShipperAddressFromGoogle'] = 1 ;
                    $shipperConsigneeAry['szShipperAddress'] = $szShipperAddressGoe ;
                }
                if(!empty($origionGeoCountryAry['szCityName']))
                {
                    $shipperConsigneeAry['szShipperCity'] = $origionGeoCountryAry['szCityName'];
                    $shipperConsigneeAry['iGetShipperCityFromGoogle'] = 1 ;
                }
                if(!empty($origionGeoCountryAry['szPostCode']))
                {
                    $shipperConsigneeAry['szShipperPostcode'] = $origionGeoCountryAry['szPostCode']; 

                    if(!$iGotDataFromDB)
                    {
                        $shipperConsigneeAry['iGetShipperPostcodeFromGoogle'] = 1 ;		  
                    } 
                }  	
                $shipperConsigneeAry['idShipperDialCode'] = $postSearchAry['idOriginCountry'];
                $shipperConsigneeAry['idShipperCountry'] = $postSearchAry['idOriginCountry']; 

                $postcodeCheckAry=array();
                $postcodeResultAry = array();

                $postcodeCheckAry['idCountry'] = $postSearchAry['idDestinationCountry'] ;
                $postcodeCheckAry['szLatitute'] = $destinationGeoCountryAry['szLat'] ;
                $postcodeCheckAry['szLongitute'] = $destinationGeoCountryAry['szLng'] ;
                $postcodeResultAry = $kConfig_new->getPostcodeFromGoogleCordinate($postcodeCheckAry);
                $iGotDataFromDB = false;
                if(!empty($postcodeResultAry))
                {
                    if(empty($destinationGeoCountryAry['szCityName']))
                    {
                        $destinationGeoCountryAry['szCityName'] = trim($postcodeResultAry['szCity']);
                    }
                    if(empty($destinationGeoCountryAry['szPostCode']))
                    {
                        $destinationGeoCountryAry['szPostCode'] = trim($postcodeResultAry['szPostCode']);
                        $iGotDataFromDB = true;
                    }
                }

                $szShipperAddressGoe = trim($destinationGeoCountryAry['szStreetNumber']);

                if(!empty($szShipperAddressGoe) && !empty($destinationGeoCountryAry['szRoute']))
                {
                    $szShipperAddressGoe .= ", ".$destinationGeoCountryAry['szRoute'] ;
                }
                else
                {
                    $szShipperAddressGoe .= $destinationGeoCountryAry['szRoute'] ;
                }

                $shipperConsigneeAry['iGetConsigneeAddressFromGoogle'] = 0;
                $shipperConsigneeAry['iGetConsigneePostcodeFromGoogle'] = 0;
                $shipperConsigneeAry['iGetConsigneeCityFromGoogle'] = 0;
                $shipperConsigneeAry['szConsigneeCity'] = '';
                $shipperConsigneeAry['szConsigneeAddress'] = '';
                $shipperConsigneeAry['szConsigneePostCode'] = '';

                if(!empty($szShipperAddressGoe))
                {
                    $shipperConsigneeAry['szConsigneeAddress'] = $szShipperAddressGoe ;
                    $shipperConsigneeAry['iGetConsigneeAddressFromGoogle'] = 1 ;
                }
                if(!empty($destinationGeoCountryAry['szPostCode']))
                {
                    $shipperConsigneeAry['szConsigneePostCode'] = $destinationGeoCountryAry['szPostCode'];
                    if(!$iGotDataFromDB)
                    {
                        $shipperConsigneeAry['iGetConsigneePostcodeFromGoogle'] = 1 ;
                    }
                }
                if(!empty($destinationGeoCountryAry['szCityName']))
                {
                    $shipperConsigneeAry['szConsigneeCity'] = $destinationGeoCountryAry['szCityName']; 
                    $shipperConsigneeAry['iGetConsigneeCityFromGoogle'] = 1 ;
                }   
                $shipperConsigneeAry['idConsigneeDialCode'] = $postSearchAry['idDestinationCountry']; 
                $shipperConsigneeAry['idConsigneeCountry'] = $postSearchAry['idDestinationCountry']; 

                if(!empty($shipperConsigneeAry))
                {
                    $update_ship_con_query = '';
                    foreach($shipperConsigneeAry as $key=>$value)
                    {
                        $update_ship_con_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    } 
                    $kRegisterShipCon = new cRegisterShipCon();
                    $update_ship_con_query = rtrim($update_ship_con_query,",");  
                    //echo $update_ship_con_query ;
                    $kRegisterShipCon->updateShipperConsigneeDetails($update_ship_con_query,$idShipperConsignee);
                }  
            }
        } 
        $kConfig = new cConfig();
        $kConfig->loadCountry($postSearchAry['idOriginCountry']);
        $szCountryStrUrl = $kConfig->szCountryISO ;
        $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
        $szCountryStrUrl .= $kConfig->szCountryISO ;
        
        $kWHSSearch=new cWHSSearch();
        $szUserDidNotGetAnyOnlineService = $kWHSSearch->getManageMentVariableByDescription('__USER_DID_NOT_GET_ANY_ONLINE_SERVICES__');
    
        $res_ary=array(); 
        $res_ary['szInternalComment'] = $szUserDidNotGetAnyOnlineService ;
        if(!empty($res_ary))
        {
            $update_query = '';
            foreach($res_ary as $key=>$value)
            {
                $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
            }
        } 
        $update_query = rtrim($update_query,",");
        if($kBooking->updateDraftBooking($update_query,$idBooking))
        {

        }
                                        
        echo "DISPLAY_QUOTATION_PAGE||||";
        echo __BOOKING_GET_QUOTE_PAGE_URL__."/".$szBookingRandomNum."/".__QUOTATION_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
        die;
    }
}
else if($szAction=='CHANGE_BOOKING_CURRENCY')
{
    $kBooking = new cBooking();
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_key']));
    $idCurrency = sanitize_all_html_input(trim($_REQUEST['booking_currency']));
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page']));
    $iQuickQuoteBooking = sanitize_all_html_input(trim($_REQUEST['iQuickQuoteBooking']));
     
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
    $postSearchAry = $kBooking->getBookingDetails($idBooking);

    if(!empty($postSearchAry) && $idCurrency>0)
    {
        if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
        {
            echo "BOOKINGNOTIFICATION||||";
            echo display_booking_already_paid($t_base,$idBooking);
            die;
        }

        if(__ENVIRONMENT__ == "LIVE")
        {
            setcookie("__SIMPLE_SEARCH_CURRENCY_COOKIE__", $idCurrency, time()+(3600*24*90),'/',$_SERVER['HTTP_HOST'],true);
        }
        else
        {
            setcookie("__SIMPLE_SEARCH_CURRENCY_COOKIE__", $idCurrency, time()+(3600*24*90),'/');
        } 
        
        $kWHSSearch= new cWHSSearch();
        $kConfig=new cConfig();
        
        if($_SESSION['user_id']>0)
        {
            $kUser = new cUser();
            $kUser->updateUserWithCurrency($idCurrency);
        } 
        $postSearchAry = $kBooking->getBookingDetails($idBooking);

        if($szFromPage=='SERVICE_QUOTE_PAGE')
        {
            if($kWHSSearch->updateBookingQuotesWithCurrency($idCurrency,$szBookingRandomNum))
            {
                $bookingIdAry = array();
                $bookingIdAry[0] = $idBooking ;
                $newInsuredBookingAry = array();
                $newBookingQuotesAry = $kBooking->getAllBookingQuotes(false,$bookingIdAry);

                $postSearchAry = array();
                $postSearchAry = $newBookingQuotesAry[0];  

                echo "SUCCESS||||";
                echo display_quotes_searvice_details($postSearchAry);
                die;
            } 
            else
            {
                echo "ERROR||||";
                die;
            }
        }
        else if($iQuickQuoteBooking==1)
        {
            //@To Do
        }
        else
        {  
            $kWHSSearch->updateBookingWithCurrency($idCurrency,$szBookingRandomNum); 
            
            $iLanguage = getLanguageId();
            $searchResult=array();
            $countryKeyValueAry = $kConfig->getAllCountryInKeyValuePair(true,false,false,$iLanguage);
        
            $postSearchAry = $kBooking->getBookingDetails($idBooking);
            $searchResultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,false,$idBooking); 
            $szCourierCalculationLogString = $kWHSSearch->szCourierCalculationLogString ;
            
            $postSearchAry['iNumRecordFound'] = count($searchResultAry);
            $kBooking->addSelectServiceData($postSearchAry);
            $searchResultAry = sortArray($searchResultAry,'fDisplayPrice',false,'iCuttOffDate_time',false,'iAvailabeleDate_time',false,true);

            if(!empty($searchResultAry))
            {
                echo "SUCCESS||||";
                echo display_new_searvice_listing($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,$forwarder_flag,false,false,$szCourierCalculationLogString); 
                die;
            }
        }
    } 
}
else if($szAction=='GET_COURIER_RFQ')
{
    $kBooking = new cBooking();
    $szBookingRandomNum = sanitize_all_html_input(trim($_POST['booking_key'])); 
    $iOfferType = sanitize_all_html_input(trim($_POST['offer_type'])); 
    $szFromPage = sanitize_all_html_input(trim($_POST['from_page'])); 
    
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
    $postSearchAry = $kBooking->getBookingDetails($idBooking);
    
    if(empty($postSearchAry))
    {
        $t_base = "Booking/MyBooking/";
        echo "ERROR||||";
        echo display_quote_expire_popup($t_base,$idBooking);
        die;
    } 
    if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
    {
        echo "ERROR||||" ;
        $t_base_booking_details = "BookingDetails/";
        echo display_booking_already_paid($t_base_booking_details,$idBooking);
        die;
    }
    
    $kQuote = new cQuote();
    if($kQuote->convertBookingtoRFQ($postSearchAry,$iOfferType,$szFromPage))
    { 
        $kConfig->loadCountry($postSearchAry['idOriginCountry']);
        $szCountryStrUrl = $kConfig->szCountryISO ;
        $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
        $szCountryStrUrl .= $kConfig->szCountryISO ;  
        $redirect_url = __BOOKING_QUOTE_THANK_YOU_PAGE_URL__."/".___BOOKING_QUOTE_THANK_YOU_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ; 
        echo "SUCCESS||||".$redirect_url;
        die; 
    }
}
else if($szAction=='DISPLAY_MORE_SERVICE_LIST')
{
    $kBooking = new cBooking();
    $szBookingRandomNum = sanitize_all_html_input(trim($_POST['booking_key']));  
    $iPageNumber = sanitize_all_html_input(trim($_POST['page_number'])); 
     
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
    $postSearchAry = $kBooking->getBookingDetails($idBooking);
    
    $tempResultAry = $kWHSSearch->getSearchedDataFromTempData($idBooking);
    $searchResultAry = unserialize($tempResultAry['szSerializeData']); 
    $szCourierCalculationLogString = $tempResultAry['szCourierCalculationLogString'];
    $iSearchResultLimit = count($searchResultAry);
   
    $postSearchAry['szServiceSortField'] = 'SEE_MORE_LINK' ; 
    $postSearchAry['iPageNumber'] = $iPageNumber; 
    
    $_SESSION[$postSearchAry['szBookingRandomNum']]['iSessionPageNumber'] = $iPageNumber;
     
    $kCourierService_new = new cCourierServices();  
    $searchResult = fetch_selected_services($searchResultAry,$kCourierService_new,$postSearchAry); 
     
    //echo "SUCCESS||||"; 
    //echo display_new_services_listing($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,false,$szCourierCalculationLogString);
   // die;
    die;
}
else if($szAction=='SORT_DISPLAYED_SERVICE_LIST')
{
    $kBooking = new cBooking();
    $szBookingRandomNum = sanitize_all_html_input(trim($_POST['booking_key'])); 
    $szSortField = sanitize_all_html_input(trim($_POST['sort_field'])); 
    $szSortType = sanitize_all_html_input(trim($_POST['sort_by'])); 
    $iPageNumber = sanitize_all_html_input(trim($_POST['page_number'])); 
     
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
    $postSearchAry = $kBooking->getBookingDetails($idBooking);
    
    if($postSearchAry['iQuickQuote']>0)
    {
        /*
        * We don't allow to sort the if booking is created from Quick Quote
        */
        echo "NO SORTING";
        die;
    }
    else
    {
        
        $tempResultAry = $kWHSSearch->getSearchedDataFromTempData($idBooking);
        $searchResultAry = unserialize($tempResultAry['szSerializeData']); 
        $szCourierCalculationLogString = $tempResultAry['szCourierCalculationLogString'];
        $iSearchResultLimit = count($searchResultAry);

        $postSearchAry['szServiceSortField'] = $szSortField ;
        $postSearchAry['szServiceSortType'] = $szSortType ;
        $postSearchAry['iPageNumber'] = $iPageNumber; 

        $_SESSION['iSessionPageNumber'][$postSearchAry['szBookingRandomNum']] = $iPageNumber;

        echo "SUCCESS||||"; 
        //echo display_new_searvice_listing($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,false,$szCourierCalculationLogString);
        echo display_new_searvice_listing($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,false,false,false,$szCourierCalculationLogString);
        die;
    }
    die;
}
else if($szAction=='DISPLAY_BOOKING_DETAILS_FIELDS')
{
    $kBooking = new cBooking();
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['szBookingRandomNum'])); 
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
    $postSearchAry = $kBooking->getBookingDetails($idBooking);
    $kRegisterShipCon = new cRegisterShipCon();

    echo "SUCCESS||||";
    echo display_booking_details($postSearchAry,$kRegisterShipCon);
    die;
}
else if($szAction=='UPDATE_SERVICE_LIST_AFTER_CARGO_CHANGE')
{
    $kBooking = new cBooking();
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_key'])); 
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
    $postSearchAry = $kBooking->getBookingDetails($idBooking);
    
    if(!empty($postSearchAry))
    {
        $searchResultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,false,$idBooking);   
        $szCourierCalculationLogString = $kWHSSearch->szCalculationLogString ;
        $t_base_select_service = "SelectService/"; 
        $searchResultAry = sortArray($searchResultAry,'fDisplayPrice',false,'iCuttOffDate_time',false,'iAvailabeleDate_time',false,true);
        echo "SUCCESS||||"; 
        echo display_new_searvice_listing($postSearchAry,$searchResultAry,$t_base_select_service,$countryKeyValueAry,false,true,false,$szCourierCalculationLogString);
        die;
    }
    die;
}
else if(!empty($_REQUEST['cargodetailAry']))
{
    $kBooking = new cBooking();
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_key'])); 
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
    $postSearchAry = $kBooking->getBookingDetails($idBooking);
       
    $_POST['cargodetailAry'] = format_cargo_details_post_array($_POST['cargodetailAry']); 
    
    $iDonotCalculateAgain = $_POST['iDonotCalculateAgain']; 
    $redirect_to_quote = false;  
    
    if($kBooking->updateCargoDescriptionforCourierService($_POST['cargodetailAry'],$postSearchAry))
    {
        $postSearchAry = $kBooking->getBookingDetails($idBooking);   
        if($iDonotCalculateAgain==1) //This means booking prices has been changed.
        {  
            /*
             * Previously we are calculating entire service list if prices for the booking got changed but now we do this calculation in background to reduce waiting time of the user
             */
             $searchResultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,false,$idBooking);   
            $szCourierCalculationLogString = $kWHSSearch->szCalculationLogString ; 
            $searchResultAry = sortArray($searchResultAry,'fDisplayPrice',false,'iCuttOffDate_time',false,'iAvailabeleDate_time',false,true);
              
            if($kBooking->fNewTotalPriceCustomerCurrency>0)
            {
                $fNewTotalPriceCustomerCurrency = number_format_custom((float)$kBooking->fNewTotalPriceCustomerCurrency,$iLanguage);
//                if($iLanguage==__LANGUAGE_ID_DANISH__)
//                { 
//                    $fNewTotalPriceCustomerCurrency = number_format((float)$kBooking->fNewTotalPriceCustomerCurrency,0,'.','.');
//                }
//                else
//                { 
//                    $fNewTotalPriceCustomerCurrency = number_format((float)$kBooking->fNewTotalPriceCustomerCurrency,0,'.',',');
//                }
                $t_base_select_service = "SelectService/"; 
                echo "SUCCESS_UPDATE_SERVICE||||";
                echo display_booking_details($postSearchAry,$kRegisterShipCon,false,$kBooking->szCargoLogString);
                //echo $kBooking->szCargoLogString;
                echo "||||";
                //echo display_new_searvice_listing($postSearchAry,$searchResultAry,$t_base_select_service,$countryKeyValueAry,false,true,false,$szCourierCalculationLogString);
                echo $fNewTotalPriceCustomerCurrency;
                echo "||||".format_volume($postSearchAry['fCargoVolume']);
                echo "||||".ceil($postSearchAry['fCargoWeight']);
                echo "||||".$postSearchAry['idUniqueWTW'];
                echo "||||".__NEW_BOOKING_DETAILS_PAGE_URL__;
                echo "||||".$_SESSION['user_id'] ;
                die;
            }
            else
            {
                $redirect_to_quote = true;
            } 
        } 
        else
        {
            echo "SUCCESS||||";
            echo update_cargo_dimension($kBooking,$postSearchAry);
            echo "||||";
            //echo $kBooking->szCargoLogString;
            echo display_booking_details($postSearchAry,$kRegisterShipCon,false,$kBooking->szCargoLogString);
            echo "||||".format_volume($postSearchAry['fCargoVolume']);
            echo "||||".ceil($postSearchAry['fCargoWeight']);
        }
        die;
    }
    else
    {  
//        print_R($kBooking);
//        echo "<br> Calcu : ".$iDonotCalculateAgain." OBJ vall: ".$kBooking->iDonotCalculateAgain;
        if($iDonotCalculateAgain==2 && $kBooking->iDonotCalculateAgain==2)
        {
            $postSearchAry = $kBooking->getBookingDetails($idBooking);    
            $searchResultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,false,$idBooking);   
            $szCourierCalculationLogString = $kWHSSearch->szCalculationLogString ;
   
            if(!empty($searchResultAry))
            {
                $t_base_select_service = "SelectService/"; 
                $searchResultAry = sortArray($searchResultAry,'fDisplayPrice',false,'iCuttOffDate_time',false,'iAvailabeleDate_time',false,true);
                
                echo "ERROR_UPDATE_SERVICE||||";  
                echo display_new_services_listing($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,false,$szCourierCalculationLogString);
                echo "||||".format_volume($postSearchAry['fCargoVolume']);
                echo "||||".round($postSearchAry['fCargoWeight']);  
                die;
            }
            else
            {
                $redirect_to_quote = true;
            } 
        } 
        else
        {
            echo "ERROR_CARGO||||";
            echo update_cargo_dimension($kBooking,$postSearchAry,$kBooking->szCargoLogString);
            die;
        }
    }
    
    if($redirect_to_quote)
    { 
        $res_ary=array();
        if((int)$postSearchAry['iBookingStep']<7)
        {
            $res_ary['iBookingStep'] = 7 ;
        }

        $res_ary['iBookingQuotes'] = 1 ;
        $res_ary['iQuotesStatus'] = 0;
        $res_ary['iGetQuotePageFlag'] = 1 ;    
        if(!empty($res_ary))
        {
            $update_query = '';
            foreach($res_ary as $key=>$value)
            {
                $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
            }
        }
        $update_query = rtrim($update_query,",");
        if($kBooking->updateDraftBooking($update_query,$idBooking))
        {
            //booking moves on to second step means Trade Available.
        } 
        $postSearchAry = $kBooking->getBookingDetails($idBooking);

        $kConfig_new = new cConfig(); 
        $kConfig_new->loadCountry($postSearchAry['idOriginCountry']);
        $szCountryStrUrl = $kConfig_new->szCountryISO ;  
        $kConfig_new->loadCountry($postSearchAry['idDestinationCountry']); 
        $szCountryStrUrl .= $kConfig_new->szCountryISO ; 
        
        
        $kWHSSearch=new cWHSSearch();
        $szUserDidNotGetAnyOnlineService = $kWHSSearch->getManageMentVariableByDescription('__USER_DID_NOT_GET_ANY_ONLINE_SERVICES__');
    
        $res_ary=array(); 
        $res_ary['szInternalComment'] = $szUserDidNotGetAnyOnlineService ;
        if(!empty($res_ary))
        {
            $update_query = '';
            foreach($res_ary as $key=>$value)
            {
                $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
            }
        } 
        $update_query = rtrim($update_query,",");
        if($kBooking->updateDraftBooking($update_query,$idBooking))
        {

        }
 
        $landing_page_url =  __BOOKING_GET_QUOTE_PAGE_URL__."/".$szBookingRandomNum."/".__QUOTATION_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
        echo "NO_RECORD_FOUND||||".$landing_page_url;
    } 
    die;
}
 
$kConfig=new cConfig();
$kWHSSearch=new cWHSSearch();

$iLanguage = getLanguageId();
$searchResult=array();
$countryKeyValueAry=$kConfig->getAllCountryInKeyValuePair(true,false,false,$iLanguage);
$kBooking = new cBooking();
$searchResultAry=array();
//$postSearchAry = $_SESSION['sess_search_ary_ajax'];
$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['szBookingRandomNum'])); 
$hideDivFlag = sanitize_all_html_input(trim($_REQUEST['hideDivFlag'])); 
$szFlag = sanitize_all_html_input(trim($_REQUEST['flag'])); 
$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);

if($idBooking>0)
{
    $postSearchAry = $kBooking->getBookingDetails($idBooking);
    $cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);
    $searchResultAry = array();
    if($hideDivFlag=='SIMPLE_SERVICES')
    {	  
        $searchResultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,false,$idBooking);   
        $szCourierCalculationLogString = $kWHSSearch->szCourierCalculationLogString ;

        $postSearchAry['iNumRecordFound'] = count($searchResultAry);
        $kBooking->addSelectServiceData($postSearchAry);
        $searchResultAry = sortArray($searchResultAry,'fDisplayPrice',false,'iCuttOffDate_time',false,'iAvailabeleDate_time',false,true); 
    }
    else
    {
        if($_SESSION['came_from_landing_page'.'_'.$szBookingRandomNum]==1)
        {
            $searchResultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,false,$idBooking); 
            $postSearchAry['iNumRecordFound'] = count($searchResultAry);
            $kBooking->addSelectServiceData($postSearchAry); 
        }
        else if($kBooking->check_30_minute_window($postSearchAry))
        {
            $tempResultAry = $kWHSSearch->getSearchedDataFromTempData($idBooking);
            $searchResultAry = unserialize($tempResultAry['szSerializeData']);		

            $iSearchResultLimit = count($searchResultAry);

            $res_ary = $searchResultAry ;
            $final_ary = sortArray($searchResultAry,'fTotalPriceCustomerCurrency');  // sorting array ASC to fPrice 

            $final_ary=array();
            $final_ary = sortArray($res_ary,'fDisplayPrice');								

            $kWHSSearch->fMinPriceForSlider = $final_ary[0]['fDisplayPrice'];

            $kWHSSearch->fMaxPriceForSlider = $final_ary[$iSearchResultLimit-1]['fDisplayPrice'];

            // sorting resulting array according to Transit hour to get min and max transit hours 
            $final_ary=array();
            $final_ary = sortArray($res_ary,'iTransitHour');					
            $kWHSSearch->fMinTransitHourForSlider = $final_ary[0]['iTransitHour'];
            $kWHSSearch->fMaxTransitHourForSlider = $final_ary[$iSearchResultLimit-1]['iTransitHour'];

            if($postSearchAry['idTimingType']==1)
            {					
                $final_ary=array();					
                $final_ary = sortArray($res_ary,'iCuttOffDate_time');					
                $kWHSSearch->fMinCutOffForSlider = $final_ary[0]['iCuttOffDate_time'];
                $kWHSSearch->fMaxCutOffForSlider = $final_ary[$iSearchResultLimit-1]['iCuttOffDate_time'];
            }
            else
            {
                $final_ary=array();					
                $final_ary = sortArray($res_ary,'iAvailabeleDate_time');					
                $kWHSSearch->fMinCutOffForSlider = $final_ary[0]['iAvailabeleDate_time'];
                $kWHSSearch->fMaxCutOffForSlider = $final_ary[$iSearchResultLimit-1]['iAvailabeleDate_time'];
            }

            // sorting resulting array according to Transit hour to get min and max transit hours 
            $final_ary=array();
            $final_ary = sortArray($res_ary,'fAvgRating');					
            $kWHSSearch->fMinAvgRatingForSlider = $final_ary[0]['fAvgRating'];
            $kWHSSearch->fMaxAvgRatingForSlider = $final_ary[$iSearchResultLimit-1]['fAvgRating'];		
            $kWHSSearch->forwarderAry = unserialize($tempResultAry['szForwarderData']);  
        } 
        if(!empty($searchResultAry))
        {
            $searchResultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,false,$idBooking); 
            $postSearchAry['iNumRecordFound'] = count($searchResultAry);
            $kBooking->addSelectServiceData($postSearchAry); 
        }	
    }

    $_SESSION['came_from_landing_page'.'_'.$szBookingRandomNum]=0;
    unset($_SESSION['came_from_landing_page']);

    // initialising arrays
    $countryAry=array();
    $serviceTypeAry=array();
    $weightMeasureAry=array();
    $cargoMeasureAry = array();
    $timingTypeAry=array();
    $forwarderAry=array();
}
else
{
    $postSearchAry = array();
    $cargoAry = array();
}
if(!empty($kWHSSearch->forwarderAry))
{
    asort($kWHSSearch->forwarderAry);
} 
if(!empty($searchResultAry))
{ 	
    echo "SUCCESS|||| ";
    if($szFlag=='NEW_LANDING_PAGE')	
    {  
        $searchResultAry = sortArray($searchResultAry,'fDisplayPrice',false,'iCuttOffDate_time',false,'iAvailabeleDate_time',false,true);
        
        echo display_new_services_listing($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,false,$szCourierCalculationLogString);
    }
    else if($hideDivFlag=='SIMPLE_SERVICES')
    {
        if($_SESSION['user_id']>0)
        { 
            //Updating search details to CRM
            $idCustomer = $_SESSION['user_id'] ;
            $dtTimingDate = date('d. M, Y',strtotime($postSearchAry['dtTimingDate']));
            $fTotalVolume = format_volume($postSearchAry['fCargoVolume']);
            $fTotalWeight = number_format((float)$postSearchAry['fCargoWeight']);

            $kRegisterShipCon = new cRegisterShipCon();

            $szCustomerHistoryNotes = "From: ".$postSearchAry['szOriginCountry']."\nTo: ".$postSearchAry['szDestinationCountry']." \nDate: ".$dtTimingDate." \nVolume: ".$fTotalVolume." \nWeight: ".$fTotalWeight." \n";

            $kRegisterShipCon->updateCapsuleHistoryNotes($szCustomerHistoryNotes,$idCustomer);
        } 
        ?>
        <div id="right_data_container">
        <?php
            $postSearchAry['simpleServiceSearch']=$hideDivFlag;
            echo simple_serach_html($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry);
        ?>
        </div>
        <?php
    }
    else
    {
        if($postSearchAry['iFromRequirementPage']==1)
        {
            $return_url = __REQUIREMENT_PAGE_URL__.'/'.$szBookingRandomNum.'/' ;
        }
        else
        {
            $return_url = __LANDING_PAGE_URL__.'/'.$szBookingRandomNum.'/';
        }
    ?>			
        <form id="select_service_search" action="" method="post">
            <div class="hsbody-2-left filter_style">	
            <?php		
                    //display html for service requirements and cargo details  .
                    echo display_services_requirements_left($postSearchAry,$cargoAry,$t_base);				
            ?>
            <br/>
            <div id="left_slider_div">
            <?php				
                // display slider of select services page 
                echo display_select_service_slider($kWHSSearch,$postSearchAry,$t_base);
            ?>
            </div>
            <h5><?=t($t_base.'fields/forwarders');?></h5>
            <div class="forwarders">
            <?php
                if(!empty($kWHSSearch->forwarderAry))
                {
                    $ctr=0;
                    foreach($kWHSSearch->forwarderAry as $idForwarder=>$szDisplayName)
                    {
                        ?>
                        <label ><input type="checkbox" onclick="search_select_service('Forwarder');"  name="selectServiceAry[idForwarder][<?=$ctr?>]" value="<?=$idForwarder?>" checked /><?=$szDisplayName?></label>
                        <?php
                        $ctr++;
                    }
                }
            ?>					
            </div>
        </div>
        <input type="hidden" name="selectServiceAry[szBookingRandomNum]" value="<?=$szBookingRandomNum?>" id="service_szBookingRandomNum"> 
    </form>
    <div class="hsbody-2-right">
        <?php
                /**
                * display_booikng_bread_crum is used to display bread crum, it accepts two param first one is step and second is $t_base
                */ 
                echo display_booikng_bread_crum(1,$t_base);
        ?>
        <p align="right"><a href="javascript:void(0)" onclick="open_sorted_popup('<?=$postSearchAry['idServiceType']?>');"><?=t($t_base.'fields/how_is_this_sorted');?></a></p>
        <div id="forwarder_sorted_popup"></div>
        <br />		
        <div id="sevice_container">
            <?php
                if($postSearchAry['idServiceType']== __SERVICE_TYPE_DTD__)
                {
                    display_dtd_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry);
                }
                else if(($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_DTW__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_DTP__) )
                {
                    display_dtw_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry);
                }
                else if(($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_WTD__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_PTD__) )
                {
                    display_wtd_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry);
                }
                else if(($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_WTW__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_WTP__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_PTP__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_PTW__))
                {
                    display_dtd_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry);
                }
            ?>
        </div>
        <p align="center"><a href="<?=$return_url?>" class="button2"><span><?=t($t_base.'fields/back');?></span></a></p>
    </div>
        <?php 
    }

    $res_ary=array();
    if((int)$postSearchAry['iBookingStep']<8)
    {
        $res_ary['iBookingStep'] = 8 ;
    }
    if(!empty($res_ary))
    {
        $update_query = '';
        foreach($res_ary as $key=>$value)
        {
            $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
        }
    }		
    if(!empty($update_query))
    {
        $update_query = rtrim($update_query,",");
        if($kBooking->updateDraftBooking($update_query,$idBooking))
        {
            //booking moves on to 8 step means Service options displayed.
        }
    }
    die;
}
else
{
    echo "NO_RECORD_FOUND||||"; 
    $res_ary=array();
    if((int)$postSearchAry['iBookingStep']<7)
    {
        $res_ary['iBookingStep'] = 7 ;
    }
    
    $res_ary['iBookingQuotes'] = 1 ;
    $res_ary['iQuotesStatus'] = 0;
    
    if(!empty($res_ary))
    {
        $update_query = '';
        foreach($res_ary as $key=>$value)
        {
            $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
        }
    }
    $update_query = rtrim($update_query,",");
    if($kBooking->updateDraftBooking($update_query,$idBooking))
    {
        //booking moves on to second step means Trade Available.
    } 
    $postSearchAry = $kBooking->getBookingDetails($idBooking);

    if($hideDivFlag=='SIMPLE_SERVICES')
    {		
        $kConfig_new = new cConfig(); 
        $kConfig_new->loadCountry($postSearchAry['idOriginCountry']);
        $szCountryStrUrl = $kConfig_new->szCountryISO ;  
        $kConfig_new->loadCountry($postSearchAry['idDestinationCountry']); 
        $szCountryStrUrl .= $kConfig_new->szCountryISO ; 
        
        
        $kWHSSearch=new cWHSSearch();
        $szUserDidNotGetAnyOnlineService = $kWHSSearch->getManageMentVariableByDescription('__USER_DID_NOT_GET_ANY_ONLINE_SERVICES__');
    
        $res_ary=array(); 
        $res_ary['szInternalComment'] = $szUserDidNotGetAnyOnlineService;
        if(!empty($res_ary))
        {
            $update_query = '';
            foreach($res_ary as $key=>$value)
            {
                $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
            }
        } 
        $update_query = rtrim($update_query,",");
        if($kBooking->updateDraftBooking($update_query,$idBooking))
        {

        }

        //$landing_page_url = __SELECT_SIMPLE_SEARCH_URL__.'service_not_found/'.$szBookingRandomNum.'/' ;
        $landing_page_url =  __BOOKING_GET_QUOTE_PAGE_URL__."/".$szBookingRandomNum."/".__QUOTATION_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
    }
    else
    {	
        if($postSearchAry['iFromRequirementPage']==1)
        {
                $landing_page_url = __REQUIREMENT_PAGE_URL__."/service_not_found/".$szBookingRandomNum.'/' ;
        }
        else
        {
                $landing_page_url = __LANDING_PAGE_URL__."/service_not_found/".$szBookingRandomNum.'/' ;
        }
    } 
    ?>
    <script type="text/javascript">
        var page_url = '<?=$landing_page_url?>'
        redirect_url(page_url);
    </script>
    <?php
    die;
}
?>	