<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

$szMetaTitle= __STARTING_NEW_TRADE_PAGE_META_TITLE__;
$szMetaKeywords = __STARTING_NEW_TRADE_PAGE_META_KEYWORDS__;
$szMetaDescription = __STARTING_NEW_TRADE_PAGE_META_DESCRIPTION__;

require_once( __APP_PATH_LAYOUT__ . "/header.php" );
$KwarehouseSearch=new cWHSSearch();
$kExportImport = new cExport_Import();
$importExportAry = array();
$iLanguage = getLanguageId();
//if($iLanguage ==__LANGUAGE_ID_DANISH__)
//{
//	$newTrends=$KwarehouseSearch->getManageMentVariableByDescription(__STARTING_NEW_TRADES_DANISH__);
//}
//else
//{
	$newTrends=$KwarehouseSearch->getManageMentVariableByDescription(__STARTING_NEW_TRADES__,$iLanguage);
//}

$newTrends =  html_entity_decode($newTrends);

$originCountryNameAry = $kExportImport->getAllServicingCountry('s.szOriginCountry',$iLanguage);
$destCountryNameAry = $kExportImport->getAllServicingCountry('s.szDestinationCountry',$iLanguage);

$t_base="NewsTrade/"
?>
<div id="hsbody">
	<h5><strong><?=t($t_base.'title/starting_new_trades')?></strong></h5>
	<? if($newTrends)
	{
		echo refineDescriptionData($newTrends);
	}?>
	 <br />
	  <br />
	 <div class="oh">
		<div class="fl-40" style="width:43%;">
			<h4><strong><?=t($t_base.'title/sorted_origin_country')?></strong></h4>
			<!-- <div class="statingnewtrade-box"> -->
			<?
				if(!empty($originCountryNameAry))
				{		
					$countrt=1;		
					foreach($originCountryNameAry as $key=>$originCountryNameArys)
					{	
						if(!empty($key))
						{
							$countryAry = explode('_',$key);	
							$idOriginCountry = $countryAry[0];
							$idDestinationCountry = $countryAry[1]; 			
						}
						$form_id=$key."_".$countrt;
						if($idOriginCountry != $idDestinationCountry)
						{
							?>
							<form action="" id="<?=$form_id?>" method="post">
								<input type="hidden" name="landingPageAry[szOriginCountry]" value="<?=$idOriginCountry?>">
								<input type="hidden" name="landingPageAry[szDestinationCountry]" value="<?=$idDestinationCountry?>">
								<input name="landingPageAry[iCheck]" type="hidden" value="1" id="iCheck">
								<input name="landingPageAry[key]" type="hidden" value="<?=$form_id?>" id="form_key_<?=$countrt?>">
								
							</form>
							<?		
							echo "<a href='javascript:void(0);' style='font-style:italic;'  onclick='stating_new_trade_requirement(".$countrt.")'>".$originCountryNameArys."</a> <br/>";
							$countrt++;		
						}
					}		
				}
			?>
			<!--</div> -->
		</div>
		
		<div class="fr-55">
			<h4><strong><?=t($t_base.'title/sorted_destination_country')?></strong></h4>
			<!-- <div class="statingnewtrade-box"> -->
			<?
				if(!empty($destCountryNameAry))
				{					
					foreach($destCountryNameAry as $key=>$destCountryNameArys)
					{
						if(!empty($key))
						{
							$countryAry = explode('_',$key);	
							$idOriginCountry = $countryAry[0];
							$idDestinationCountry = $countryAry[1]; 			
						}
						$form_id=$key."_".$countrt;
						if($idOriginCountry != $idDestinationCountry)
						{
							?>
							<form action="" id="<?=$form_id?>" method="post">
								<input type="hidden" name="landingPageAry[szOriginCountry]" value="<?=$idOriginCountry?>">
								<input type="hidden" name="landingPageAry[szDestinationCountry]" value="<?=$idDestinationCountry?>">
								<input name="landingPageAry[iCheck]" type="hidden" value="1" id="iCheck">
								<input name="landingPageAry[key]" type="hidden" value="<?=$form_id?>" id="form_key_<?=$countrt?>">
							</form>
							<?		
							echo "<a href='javascript:void(0);' style='font-style:italic;' onclick='stating_new_trade_requirement(".$countrt.")'>".$destCountryNameArys."</a> <br/>";
							$countrt++;		
						}
					}					
				}
			?>
			<!--</div> -->
		</div>
	</div>
	<br />
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/footer.php" );
?>