var app = angular.module('pendingTrayApp', []);  
app.controller('tasksController', function($scope, $http, $timeout) {  
    //$scope.pendingTasks = __PENDING_TRAY_JSON_DATA__;  
    auto_load_pending_tray_listing(1); 
    $("#delayed_message_container_div").css('display','none');

    var iNumCounter = 0;
    window.setInterval(function(){
        var iPendingTrayAutoRefreshCalled = $("#iPendingTrayAutoRefreshCalled").val();
        iPendingTrayAutoRefreshCalled = parseInt(iPendingTrayAutoRefreshCalled);
        iNumCounter = parseInt(iNumCounter);
        if(iPendingTrayAutoRefreshCalled==1)
        {
            //auto_load_pending_tray_listing();  
            iNumCounter = 0;
        } 
        else if(iNumCounter>1)
        {
            //$("#delayed_message_container_div").css('display','block');
            iNumCounter = 0;
        }
        else
        {
            iNumCounter++;
        }  
    }, 15000); 

    function auto_load_pending_tray_listing(onLoad)
    {
        $("#iPendingTrayAutoRefreshCalled").val(2);
        var file_id = $("#hidden_selected_file_id").val();
        var disp = $("#pending_task_main_list_container_div").css("display");
        $("#iStatusChanged").val('1'); 
        var ajaxUrl = __JS_ONLY_SITE_BASE__ + "/ajax_reloadPendingTray.php?mode=AUTO_LOAD_PENDING_TRAY_TASK_LIST&file_id="+file_id;
 
        var form_data = JSON.stringify($("#transporteca_team_form").serializeArray());
        $http.post(ajaxUrl,form_data).success(function(response){  
            var iStatusChanged = $("#iStatusChanged").val();
            iStatusChanged = parseInt(iStatusChanged); 
            if(iStatusChanged==1 || onLoad==1)
            {   
                $scope.pendingTasks = response;   
                $scope.$watch('$viewContentLoaded', function(){ 
                    $timeout(function() { 
                        $("#pending_tray_listing_loader_div").attr('style','display:none;'); 
                        $("#pending-tray-angular-lis-container").attr('style','display:block'); 
                        var idFileForCloseCtrValue=$("#idFileForClose").val(); 
                        var idFileForCloseCtrValueArr=idFileForCloseCtrValue.split(";");
                        var len=idFileForCloseCtrValueArr.length;
                        var file_id = $("#hidden_selected_file_id").val();
                        if(parseInt(len)>0)
                        {
                            for(i=0;i<len;++i)
                            {
                                var divValue = "pendingTrayList_"+idFileForCloseCtrValueArr[i];  
                                if($("#"+divValue).length)
                                { 
                                    $("#"+divValue).prop("checked","checked"); 
                                } 
                            }
                        } 
                        var tr_id = "pending_tray_listing_row_"+file_id;
                        scrollPendingTask(tr_id,1);
                    },0); 
                }); 
            } 
            else
            { 
                console.log("Already updated by Team selection...");
                //display_pending_task_list_byteam();
            }
            $("#iPendingTrayAutoRefreshCalled").val(1); 
        }).error(function(error){
            'Unable to load data: ' + error.message;
        });  
    }
    
    $scope.display_pending_task_details = function(szTrId,idBookingFile,iDisplayPopup,idBookingQuote,iQuoteClosed,iCourierBooking,szCopyMode,iCrmEmail,idReminderTask,idOfflineChat){ 
        display_pending_task_details(szTrId,idBookingFile,iDisplayPopup,idBookingQuote,iQuoteClosed,iCourierBooking,szCopyMode,iCrmEmail,idReminderTask,idOfflineChat);
    };
    $scope.selectLineForClosingTheFile = function(idBookingFile,iCheckBoxID,iCounter) {
        selectLineForClosingTheFile(idBookingFile,iCheckBoxID,iCounter); 
    };

    /*
     * function getData()
    {  
        $http.post(ajaxUrl).success(function(data){
            $scope.pendingTasks = data; 
        });   
    }
     */
}); 