<?php
//session_start();
/**
 * Currency conversion cronjob
 */

if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
 
$f = fopen(__APP_PATH_LOGS__."/sendGridNotify.log", "a");
 
if(!empty($HTTP_RAW_POST_DATA))
{
    $responseAry = json_decode($HTTP_RAW_POST_DATA);
    $responseArys = $responseAry[0];
    
    $newResponseAry = array();
    if(!empty($responseArys))
    {
        foreach($responseArys as $key=>$responseAryss)
        {
            $newResponseAry[$key] = $responseAryss ;
        }
    }
    
    fwrite($f, "\n\n################### Sendgrid Notification Received: ".date("d-m-Y h:i:s")."#######################\n");
    $szLognString = print_r($responseArys, true)." \n\n New Array \n\n ".print_R($newResponseAry,true);  
    fwrite($f,$szLognString); 
    fclose($f);  
    
    $szEmailKey = $newResponseAry['email_key']; 
    if(!empty($szEmailKey))
    {
        $kReport = new cReport();
        if($kReport->isEmailKeyExists($szEmailKey))
        {
            $kReport->updateEmailLogs($newResponseAry);
        }
    }
} 
