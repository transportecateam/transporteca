<?php
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" ); 
require_once(__APP_PATH_LAYOUT__."/ajax_header.php");
require_once(__APP_PATH__."/stripe/init.php");
          
//Creating Input array to call stripe api
$stripeChargeAry = array();
$stripeChargeAry['fTotalCostForBooking'] = $fTotalCostForBookingCent; 
$stripeChargeAry['szCurrency'] = $postSearchAry['szCurrency']; 
$stripeChargeAry['szStripeToken'] = $szStripeToken; 
$stripeChargeAry['szDescription'] = substr($item_ref_text,0,-1)." - ".$postSearchAry['szBookingRef']; 
$stripeChargeAry['szEmail'] = $postSearchAry['szEmail'];
$stripeChargeAry['szBookingRef'] = $postSearchAry['szBookingRef'];
$stripeChargeAry['idBooking'] = $postSearchAry['id'];

$stripeApiResponseAry = array();
$stripeApiResponseAry = cZoozLib::chargeSubscriptionPlan($stripeChargeAry);
          
?>