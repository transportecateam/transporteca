<?php
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" ); 
require_once(__APP_PATH_LAYOUT__."/ajax_header.php");
require_once(__APP_PATH__."/stripe/init.php");
        
//$_SESSION['booking_id'] = 0;
//unset($_SESSION['booking_id']);

// Set your secret key: remember to change this to your live secret key in production
// See your keys here https://dashboard.stripe.com/account/apikeys 
 
//$szStripeToken = sanitize_all_html_input($_POST['stripeToken']); 
//if(empty($szStripeToken))
//{
//    $szStripeToken = sanitize_all_html_input($_GET['sessionToken']); 
//}


$szStripeToken = sanitize_all_html_input($_REQUEST['stripeToken']);  
if(!empty($szStripeToken) && (int)$_SESSION['booking_id']>0)
{ 
    $t_base = "BookingConfirmation/";
    $idBooking = (int)$_SESSION['booking_id'] ;
    $kBooking = new cBooking();
    $kConfig = new cConfig();
    $kZoozLib = new cZoozLib();
    $kZooz = new cZooz();
    $iLanguage = getLanguageId();
    
    $kForwarderContact = new cForwarderContact();
    $kUser = new cUser();
    $kWHSSearch = new cWHSSearch();
    $postSearchAry = $kBooking->getExtendedBookingDetails($idBooking);
    if(!empty($postSearchAry))
    { 
        $kConfig_new = new cConfig();
        $kConfig_new->loadCountry($postSearchAry['idOriginCountry']);
        $szCountryStrUrl = $kConfig_new->szCountryISO ; 
        $kConfig_new->loadCountry($postSearchAry['idDestinationCountry']); 
        $szCountryStrUrl .= $kConfig_new->szCountryISO ; 
            
        $stripeApiResponseAry = array();
        $stripeApiResponseAry = $kZoozLib->processBookingPaymentByStripeToken($postSearchAry, $szStripeToken);
        
        if(!empty($stripeApiResponseAry))
        {
            if(trim($stripeApiResponseAry['szPaymentStatus'])=='Completed')
            {
                $postSearchAry = $kBooking->getExtendedBookingDetails($idBooking);
                confirmBookingAfterPayment($postSearchAry);

                $szRedirectUrl =  __THANK_YOU_PAGE_URL__."/".__THANK_YOU_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
            }
            else
            {
                $_SESSION['statusFailed'] = 'Failed';
                $_SESSION['szErrorMsg'] = $stripeApiResponseAry['szPaymentDescription'];
                
                $szBookingRandomNum = $postSearchAry['szBookingRandomNum']; 
                if($postSearchAry['iBookingType']==__BOOKING_TYPE_RFQ__)
                { 
                    $szRedirectUrl = __BOOKING_QUOTE_PAY_PAGE_URL__."/".$szBookingRandomNum."/".__OVERVIEW_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
                }
                else
                { 
                    $szRedirectUrl = __NEW_BOOKING_OVERVIEW_PAGE_URL__."/".$szBookingRandomNum."/".__OVERVIEW_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
                }
            }
        }
        else
        {
            $_SESSION['sessionBookingId']='Not Found'; 
            $szRedirectUrl = __HOME_PAGE_URL__;
        }
    }
    else
    { 
        $_SESSION['sessionBookingId']='Not Found'; 
        $szRedirectUrl = __HOME_PAGE_URL__;
    }
}
else
{
    $_SESSION['sessionBookingId']='Not Found';
    $szRedirectUrl = __HOME_PAGE_URL__;
}

if(!empty($szRedirectUrl))
{ 
    ob_end_clean();
    header("Location:".$szRedirectUrl);
    die();
} 
?>