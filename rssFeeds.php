<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

$szMetaTitle = __RSS_FEED_PAGE_META_TITLE__;
$szMetaKeywords = __RSS_FEED_PAGE_META_KEYWORDS__;
$szMetaDescription = __RSS_FEED_PAGE_META_DESCRIPTION__;

$style="style='top: 0px; bottom: auto; display: block;'";
$headerSearchFlag=1;
require_once( __APP_PATH_LAYOUT__ . "/header_new.php" );
$kRss = new cRSS();
$rssFeedsAry = array();
$rssFeedsAry = $kRss->getAllRssFeedsHtml();
$t_base="TermCondition/";
$replacearray=array(',',' ');

$php_self = $_SERVER['PHP_SELF'];
//print_r($_REQUEST['dtDate']);
if(!empty($_REQUEST['dtDate']))
{
	$dtDateTime = sanitize_all_html_input($_REQUEST['dtDate']);
}
echo $dtDateTime ;
?>
<script type="text/javascript">
$().ready(function() {
<?
	if(!empty($dtDateTime))
	{
?>
	if ($("#<?=$dtDateTime?>").exists())
	{
		<? 
		 header("Location:".__RSS_FEED_PAGE_URL__.'/#'.$dtDateTime);
		die; 
		?>
	}
	else
	{
		<? 
		 header("Location:".__BASE_URL__.'/200.php');
		 die; 
		?>
	}
<? 
} ?>
});
</script>
<div id="two-column" class="clearfix">
	<div class="hsbody-2-left">
			<ul class="customer_tandc" style="list-style-type: none;">
				<h4 style="margin-left:-10px;"><?=t($t_base.'title/news_feed')?></h4>
	<?
	if($rssFeedsAry)
	{
		foreach($rssFeedsAry as $key=>$rssFeedsArys)
		{				
			echo '<li><a href="#'.$key.'"  > '.date('d M Y',strtotime($rssFeedsArys['dtCreatedOn'])).'</a></li>';
		}
	?>
</ul>
	</div>	

	<div class="hsbody-2-right">
		<div class="main-body-container">	
	<? 
	foreach($rssFeedsAry as $key=>$rssFeedsArys)
	{
		?>
			<a name="<?=$key?>" id="<?=$key?>" href="javascript:void(0);" ></a>
			<h5 class="clearfix terms-heading">
			<span class="fl-80">
			<strong><?=date('d M Y',strtotime($rssFeedsArys['dtCreatedOn']))?></strong>
			</span>
			<a class="fr" href="#top"><strong><?=t($t_base.'title/top')?></strong></a>
			</h5>
			<div class="link16">			
			<?=$rssFeedsArys['szDescription']?>
			</div>
			<br />
		<? 
    }
}
?>
	</div>
	<br />
	
	<br />
	
</div>

</div>
<?php
//include( __APP_PATH__ . "/footer_holding_page.php" );
include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>