<?
ob_start();
session_start();

//ini_set('display_error',1);
//error_reporting(E_ALL);
//ini_set('error_reporting',E_ALL);

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
$display_abandon_message = true;
$szMetaTitle = __SERVICES_PAGE_META_TITLE__;
$szMetaKeywords = __SERVICES_PAGE_META_KEYWORDS__;
$szMetaDescription = __SERVICES_PAGE_META_DESCRIPTION__;
require_once(__APP_PATH_LAYOUT__."/header.php");

$t_base = "SelectService/";
constantApiKey();
/*
if(!empty($_POST['searchAry']))
{
	$_SESSION['sess_search_ary_ajax'] = $_POST['searchAry'];
}
*/
if(!empty($_REQUEST['booking_random_num']))
{
	$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num']));
}
else
{
	//header("Location:");
}
 
if((int)$_SESSION['user_id']>0)
{	
	$kBooking= new cWHSSearch();
	
	//get customer currency
	$kBooking->updateBookingWithCurrency($kUser->szCurrency,$szBookingRandomNum);
}

$szResultPageMetaTitle = t($t_base.'messages/SERVICE_PAGE_META_TITLE');
?>
<link rel="stylesheet" href="<?=__BASE_STORE_CSS_URL__?>/jslider.css" type="text/css" />
<link rel="stylesheet" href="<?=__BASE_STORE_CSS_URL__?>/jslider.blue.css" type="text/css" />
<link rel="stylesheet" href="<?=__BASE_STORE_CSS_URL__?>/jslider.plastic.css" type="text/css" />
<link rel="stylesheet" href="<?=__BASE_STORE_CSS_URL__?>/jslider.plastic.css" type="text/css" />
<link rel="stylesheet" href="<?=__BASE_STORE_CSS_URL__?>/jslider.round.plastic.css" type="text/css" />


<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__;?>/jshashtable-2.1_src.js"></script>
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__;?>//jquery.numberformatter-1.2.3.js"></script>
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__;?>//tmpl.js"></script>
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__;?>/jquery.dependClass-0.1.js"></script>
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__;?>/draggable-0.1.js"></script>
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__;?>/jquery.slider.js"></script>
<style type="text/css" media="screen">
 .layout { padding: 50px; font-family: Georgia, serif; }
 .layout-slider { margin-bottom: 20px; width: 50%; }
 .layout-slider-settings { font-size: 12px; padding-bottom: 10px; }
 .layout-slider-settings pre { font-family: Courier; }
</style>
<div id="loader" class="loader_popup_bg" style="display:none;">
	<div class="popup_loader"></div>				
		<img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
</div>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places&key=AIzaSyC-unuKYaoz990OFUpxHgoSMjwPYjQ_mlo" type="text/javascript"></script>
<script type="text/javascript">
$().ready(function() {
$("#Transportation_pop").attr("style","display:block;");
$.post(__JS_ONLY_SITE_BASE__+"/ajax_processing.php",{szBookingRandomNum:'<?=$szBookingRandomNum?>',hideDivFlag:'SIMPLE_SERVICES'},function(result){		
	//setTimeout(function(){
	    $("#Transportation_pop").attr('style','display:none;');		
		result_ary = result.split("||||");
		//alert(result_ary);
		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{
			$("#hsbody").html(result_ary[1]);
		}
		if(result_ary[0]=='NO_RECORD_FOUND')
		{
			$("#customs_clearance_pop1").html(result_ary[1]);
		}	
		document.title = '<?php echo $szResultPageMetaTitle; ?>';		
  	//},1000);		
   });	
});
</script>
<div id="customs_clearance_pop_right" class="help-pop right">
</div>
<div id="all_available_service" style="display:none;"></div>
<div id="customs_clearance_pop1">
</div>
<div id="Transportation_pop">
<h3 style="padding:20px 0 0;margin-bottom:5px;"><?=t($t_base.'messages/PROCESSING_MESSAGE_HEADER');?></h3>
<p align="center"><?=t($t_base.'messages/PROCESSING_MESSAGE_TEXT');?></p>
<br />
 <img src="<?=__BASE_STORE_IMAGE_URL__?>/Intermediate.gif" alt="Intermediate.gif" />
<br />
</div>
<div id="hsbody">	
						
</div>
<?php
require_once(__APP_PATH_LAYOUT__."/footer.php");

?>