<?php
ob_start();
session_start();
ini_set ('max_execution_time',360000);
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );

require_once( __APP_PATH__ . "/inc/constants.php" ); 
require_once( __APP_PATH__ . "/inc/functions.php" ); 

$kExplain = new cExplain();
$vogaPageAry = $kExplain->getAllNewLandingPageData(false,false,false,false,false,false,true);

if(!empty($vogaPageAry))
{
    foreach($vogaPageAry as $vogaPageArys)
    {
        $szLanguage = getLanguageName($vogaPageArys['iLanguage']);
        $szLanguage = ucfirst($szLanguage);
        $szNameAutomatedRfqResponse=$vogaPageArys['szSeoKeywords']." - ".$szLanguage;
        
        $kExplain->addUpdateAutomatedRfqResponse($szNameAutomatedRfqResponse);
        
        echo $kExplain->idAutomatedRFQ."<br>";
        
        $kExplain->updateIdAutomatedPricingEmailTemplateForStandardCargo($kExplain->idAutomatedRFQ,$vogaPageArys['id'],$kExplain->szAutomateRfqResponseCode);
    }
}

