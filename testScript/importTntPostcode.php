<?php
ini_set (max_execution_time,360000);
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

$host = __DBC_HOST__;
$uname = __DBC_USER__;
$pass = __DBC_PASSWD__;
$database = __DBC_SCHEMATA__; //Change Your Database Name
$conn = new mysqli($host, $uname, $pass, $database);
$filename = __APP_PATH__ . '/mysql/dump.sql'; //How to Create SQL File Step : url:http://localhost/phpmyadmin->detabase select->table select->Export(In Upper Toolbar)->Go:DOWNLOAD .SQL FILE
$op_data = '';
$lines = file($filename);
foreach ($lines as $line)
{
    if (substr($line, 0, 2) == '--' || $line == '')//This IF Remove Comment Inside SQL FILE
    {
        continue;
    }
    $op_data .= $line;
    if (substr(trim($line), -1, 1) == ';')//Breack Line Upto ';' NEW QUERY
    {
        $op_data=  str_replace('LMF_ALL Export',"tbltntpostcode", $op_data);
        $op_data=  str_replace('`Country`',"szCountryCode", $op_data);
        $op_data=  str_replace('`Country_name`',"szCountryName", $op_data);
        $op_data=  str_replace('`Depotname`',"szDepotName", $op_data);
        $op_data=  str_replace('`Postcode_from`',"szPostcodeFrom", $op_data);
        $op_data=  str_replace('`Postcode_until`',"szPostcodeUntil", $op_data);
        $op_data=  str_replace('`Townname`',"szTownName", $op_data);
        $op_data=  str_replace('`Province`',"szProvince", $op_data);
        $op_data=  str_replace('`Destination_station`',"szDestinationStation", $op_data);
        $op_data=  str_replace('`Controlling_station`',"szControllingStation", $op_data);
        $op_data=  str_replace('`Zone`',"szZone", $op_data);
        $op_data=  str_replace('`Satellite_code`',"szSatelliteCode", $op_data);
        $op_data=  str_replace('`Guaranteed`',"szGuaranteed", $op_data);
        $op_data=  str_replace('`Elite`',"szElite", $op_data);
        $op_data=  str_replace('`Delivery_Party`',"szDeliveryParty", $op_data);
        $op_data=  str_replace('`Saturday`',"szSaturday", $op_data);
        $op_data=  str_replace('`Domflag`',"szDomFlag", $op_data);
        $conn->query($op_data);
        $op_data = '';
    }
}
echo "Table Created Inside " . $database . " Database.......";

echo ("Done");
die();
?>
