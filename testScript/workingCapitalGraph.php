<?php
/**
 * Admin Control Panel  
 */
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );

require_once( __APP_PATH__ . "/inc/constants.php" ); 
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");

$szFileName = sanitize_all_html_input(trim($_REQUEST['file_name']));

$kDashboardAdmin = new cDashboardAdmin();
$szFileName = $kDashboardAdmin->getWorkingCapitalGraphDataExcels();
 
if(!empty($szFileName) && file_exists($szFileName))
{	
    $filename = "Transporteca_Working_Capital.xlsx";
    $file = $szFileName ;

    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header("Content-Type: application/force-download");
    header('Content-Disposition: attachment; filename=' . urlencode(basename($filename)));
    // header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    ob_clean();
    flush();
    readfile($file);    
    @unlink($file);
    exit;	
}

?>