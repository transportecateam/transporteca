<?php
ob_start();
session_start();
ini_set ('max_execution_time',360000);
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );

require_once( __APP_PATH__ . "/inc/constants.php" ); 
require_once( __APP_PATH__ . "/inc/functions.php" ); 
 
$kForwarder = new cForwarder();
$kConfig = new cConfig();
$kReferralPricing = new cReferralPricing();

$forwarderAry = array();
$forwarderAry = $kForwarder->getAllForwarder(false,true,false);

$transportModeAry = array();
$transportModeAry = $kConfig->getAllTransportMode();

if(!empty($forwarderAry))
{
    foreach($forwarderAry as $forwarderArys)
    { 
        $addReferalFeeAry = array();
        $addReferalFeeAry['idForwarder'] = $forwarderArys['id'];
        $addReferalFeeAry['fReferralFee'] = $forwarderArys['fReferalFee'];
        
        if(!empty($transportModeAry))
        {
            foreach($transportModeAry as $transportModeArys)
            {
                if($transportModeArys['id']==8 || $transportModeArys['id']==9) //8. All mode, 9. Rest
                {
                    continue;
                } 
                $addReferalFeeAry['idTransportMode'] = $transportModeArys['id'];
                $kReferralPricing->addReferalFeeByTransportMode($addReferalFeeAry); 
            }
        }
    }
}

