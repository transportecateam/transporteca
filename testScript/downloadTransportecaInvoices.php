<?php
/**
 * Admin Control Panel  
 */
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );

require_once( __APP_PATH__ . "/inc/constants.php" ); 
require_once(__APP_PATH_LAYOUT__ ."/header_new.php");
require_once(__APP_PATH_ROOT__.'/forwarders/html2pdf/html2pdfBooking.php');

$szFileName = sanitize_all_html_input(trim($_REQUEST['file_name']));

$_POST['dateFormAry']['dtFromDate'] = '2014-07-01';
$_POST['dateFormAry']['dtToDate'] = '2015-06-30';

if(!empty($_POST['dateFormAry']))
{
    $iSuccess = 1;
    if($iSuccess==1)
    { 
        $dateAry = array();
        $dateAry['dtFromDate'] = $_POST['dateFormAry']['dtFromDate'].' 00:00:00';
        $dateAry['dtToDate'] = $_POST['dateFormAry']['dtToDate'].' 23:59:59';
        $kConfig = new cConfig();
        $bookingListAry = $kConfig->getAllTransportecaInvoices($dateAry); 

        $pdf=new HTML2FPDFBOOKING(); 
        if(!empty($bookingListAry))
        {
            foreach($bookingListAry as $bookingListArys)
            {
                $idBooking = $bookingListArys['id'];
                if($idBooking>0)
                {
                    $bookingInvoiceHtml = getInvoiceConfirmationPdfFileHTML($idBooking,false,false,true);
                    $pdf->AddPage('',true); 
                    $pdf->WriteHTML($bookingInvoiceHtml,true);

                    if($bookingListArys['iInsuranceIncluded']==1)
                    {
                        $bookingInsuranceInvoicePdf = getBookingInsuranceInvoicePdf($idBooking,$pdfhtmlflag,false,true); 
                        $pdf->AddPage('',true);
                        $pdf->WriteHTML($bookingInsuranceInvoicePdf,true);
                    }
                } 
            }
        }  
        $szFileName = __APP_PATH_ROOT__."/invoice/Transporteca-Booking-Invoice.pdf"; 
        if(file_exists($szFileName))
        {
            @unlink($szFileName);
        } 
        $pdf->Output($szFileName,'F');    

        if(!empty($szFileName) && file_exists($szFileName))
        {	
            $filename = "Transporteca-Booking-Invoice.pdf";
            $file = $szFileName ;

            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header("Content-Type: application/force-download");
            header('Content-Disposition: attachment; filename=' . urlencode(basename($filename)));
            // header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);    
            @unlink($file);
            exit;	
        }
    }
}  

die;
$dtFromDate = '01/07/2014';
$dtToDate = '31/12/2014';
?>
<div id="hsbody">
    <script type="text/javascript">
        $("#dtFromDate").datepicker();
	$("#dtToDate").datepicker();
    </script>
    <form action="" method="post">
        <table class="format-1" style="width:30%">
            <tr>
                <td>
                    <input type="text" name="dateFormAry[dtFromDate]" readonly="" id="dtFromDate" value="<?php echo $dtFromDate; ?>">
                </td>
                <td>
                    <input type="text" name="dateFormAry[dtToDate]" readonly="" id="dtToDate" value="<?php echo $dtToDate; ?>">
                </td>
                <td>
                    <input type="submit" name="dateFormAry[szSubmit]" id="szSubmit" value="Download">
                </td>
            </tr>
        </table>
    </form>
</div> 
<?php 
    require_once(__APP_PATH_LAYOUT__."/footer_new.php");
?>	