<?php
//session_start();
/**
 * Currency conversion cronjob
 */
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
    ini_set (max_execution_time,360000);
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" ); 
 
$kHaulagePricing = new cHaulagePricing();
$countryAry = array();
$idHaulagePricingModelAry = array();
if(!empty($_REQUEST['mode']))
{
    $mode = 'DEMO';
}
else
{
    $mode = 'LIVE';
}

if($mode=='LIVE')
{
	echo "<br /> Script started on: ".date('d/m/Y H:i:s');
	$countryAry = $kHaulagePricing->getAllActiveCountry();
	$idHaulagePricingModelAry = $kHaulagePricing->getAllUpdatableZones();
	
	$today = date('Y-m-d');
	$iTotalNumRequestSent = $kHaulagePricing->getTotalNumRequestSent($today);
	$iNumRequestSent = $iTotalNumRequestSent;
	if(!empty($countryAry) && !empty($idHaulagePricingModelAry))
	{
		$counter=0;
		foreach($idHaulagePricingModelAry as $idHaulagePricingModelArys)
		{
			echo "<br /> Started working for Pricing Id: ".$idHaulagePricingModelArys['idHaulagePricingModel']."<br /><br />";
			$countryIsoAry = array();
			echo "<br /> checking in country for Zone ID:".$idHaulagePricingModelArys['idHaulagePricingModel']." on ".date('d/m/Y H:i:s');
			
			$zoneVertexAry = $kHaulagePricing->getAllVerticesOfZone($idHaulagePricingModel);
			$zoneLatLongAry = array();
			
			$zoneLatLongAry['szMaxLatitude'] = $kHaulagePricing->szMaxLatitude ;
			$zoneLatLongAry['szMinLatitude'] = $kHaulagePricing->szMinLatitude ;
			$zoneLatLongAry['szMaxLongitude'] = $kHaulagePricing->szMaxLongitude ;
			$zoneLatLongAry['szMinLongitude'] = $kHaulagePricing->szMinLongitude ;
			
			foreach($countryAry as $countryArys)
			{			
                            $data = array();
                            $latLongPointsAry = array();
                            $data['szLatitude'] = $countryArys['szLatitude'] ;
                            $data['idCountry'] = $countryArys['id'] ;
                            $data['szLongitude'] = $countryArys['szLongitude'] ;
                            $data['idHaulagePricingModel'] = $idHaulagePricingModelArys['idHaulagePricingModel'];	

                            if($kHaulagePricing->isLocationExistsInZone($data,$zoneVertexAry,$zoneLatLongAry))
                            {
                                $countryIsoAry['szCountryCode'][$counter] = $countryArys['szCountryISO'];
                                $countryIsoAry['idCountry'][$counter] = $countryArys['id'] ;
                                $countryIsoAry['idHaulagePricingModel'][$counter] = $idHaulagePricingModelArys['idHaulagePricingModel'] ;
                                $countryIsoAry['szGoogleCode'][$counter] = $url ;
                                $counter++;			    	 
                                echo "<br/> ".$countryArys['szCountryName']." exists in Zone ID ".$idHaulagePricingModelArys['idHaulagePricingModel']." with Latitude: ".$countryArys['szLatitude']." and Longitude: ".$countryArys['szLongitude']."<br/>";
                            }
			}
                        
			$data = array();
			$latLongPointsAry = array();
			$zoneVertexAry = array();
			$data['idHaulagePricingModel'] = $idHaulagePricingModelArys['idHaulagePricingModel'];
			$latLongPointsAry = $kHaulagePricing->isLocationExistsInZone_cronjob($data);
			$zoneVertexAry = $kHaulagePricing->zoneVertexGlobalAry;
		
                        $szGlobalMaxLatitute = $kHaulagePricing->szGlobalMaxLatitute;
                        $szGlobalMinLatitute = $kHaulagePricing->szGlobalMinLatitute; 
                        $szGlobalMaxLongitute = $kHaulagePricing->szGlobalMaxLongitute;
                        $szGlobalMinLongitute = $kHaulagePricing->szGlobalMinLongitute;
                        
			if(!empty($zoneVertexAry))
			{
                            ?>	
                            <table cellspacing="0" cellpadding="0" border="0" class="format-3" width="60%" >
                                <tr>
                                    <th align="left">PricingID</th>
                                    <th align="left">Latitude</th>
                                    <th align="left">Longitude</th>
                                    <th align="left">Length</th>
                                    <th align="center">Points</th>
                                </tr>
                                <?php
                                $vertex_ctr=0;
                                foreach($zoneVertexAry as $zoneVertexArys)
                                {
                                    if($kHaulagePricing->Y_axis>0)
                                    {
                                        $loop_counter = ceil($zoneVertexAry[$vertex_ctr-1]['iLength']/$kHaulagePricing->Y_axis);
                                    }
                                    ?>
                                    <tr>
                                        <td><?=$data['idHaulagePricingModel']?></td>
                                        <td><?=$zoneVertexArys['szLatitude']?></td>
                                        <td><?=$zoneVertexArys['szLongitude']?></td>
                                        <td><?=$vertex_ctr>0?$zoneVertexAry[$vertex_ctr-1]['iLength']:''?></td>
                                        <td><?=$loop_counter?></td>
                                    </tr>
                            <?php   $vertex_ctr++; 
				} 
                            ?>
			</table>
		<?php
			}
		?>
		<?php
			echo "<br /> Number of points to test: ".$kHaulagePricing->iNumPointsToTest;
			echo "<br /> Y = ".$kHaulagePricing->Y_axis."<br />" ;
			
			$verticesDifferenceAry = $kHaulagePricing->verticesDifferenceAry ;
			
			if(!empty($verticesDifferenceAry))
			{
                            ?>	
                            <table cellspacing="0" cellpadding="0" border="0" class="format-3" width="20%" >
                                <tr>
                                    <th align="left">Vertices</th>
                                    <th align="center">X</th>
                                </tr>
				<?php
				$vertex_ctr=0;
				foreach($verticesDifferenceAry as $verticesDifferenceArys)
				{
                                    if($vertex_ctr>0)
                                    {						
                                        ?>
                                        <tr>
                                            <td><?=$vertex_ctr?> to <?=($vertex_ctr+1)==count($verticesDifferenceAry)?1:$vertex_ctr+1?></td>
                                            <td><?=$verticesDifferenceAry[$vertex_ctr]['X_Axis']?></td>
                                        </tr>
                                        <?php	
                                    }			
                                    $vertex_ctr++; 
				}
			?>
			</table>
		<?php
			}
			
		if(!empty($latLongPointsAry))
		{
			/*
			?>	
			<br /><br />
			<table cellspacing="0" cellpadding="0" border="0" class="format-3" width="40%" >
			<tr>
				<th align="left">Points</th>
				<th align="left">Latitude</th>
				<th align="left">Longitude</th>				
			</tr>				
			<?
			$vertex_ctr=1;
			foreach($latLongPointsAry as $latLongPointsArys)
			{
				$loop_coun = count($latLongPointsArys['szLatitude']) ;
				echo "<br /> vertex coun ".$vertex_ctr ;
				echo "<br /> lop cou ".count($latLongPointsAry);
				if($vertex_ctr<count($latLongPointsAry))
				{
					for($i=0;$i<($loop_coun);$i++)
					{						
						?>
						<tr>
							<td>Point <?=$i+1?></td>
							<td><?=$latLongPointsArys['szLatitude'][$i]?></td>
							<td><?=$latLongPointsArys['szLongitude'][$i]?></td>							
						</tr>
						<?	
					}		
				}	
				$vertex_ctr++; 
			}
		?>
		</table>
	<?
		*/
		}
                    //print_R($latLongPointsAry);

                    if($iTotalNumRequestSent>0)
                    {
                        $expactedNumRequest = $iNumRequestSent + ($kHaulagePricing->iTotalNumRequest);
                        $kWHSSearch = new cWHSSearch();
                        $szGoogleApiMaxRequestNumber = $kWHSSearch->getManageMentVariableByDescription('__GOGGLE_GEOCODE_API_MAX_NUM_REQUEST__');

                        /*if($szGoogleApiMaxRequestNumber <= $expactedNumRequest)
                        {
                                //echo "<br /> Num request sent ".$iNumRequestSent ;		
                                //echo "<br /> expacted request ".$expactedNumRequest;
                                //echo "<br /> total nu req :".$iTotalNumRequestSent ;
                                //echo "<br /> max num req ".__YAHOO_GEOCODE_API_MAX_NUM_REQUEST__ ;
                                //echo "<br />zone processed ".$iHaulageZoneProcess ;	
                                $kHaulagePricing->addYahooApiLogs($iNumRequestSent);
                                echo "<br /> API call reaches its max limit could not process furthur today. Please try again tomorrow ";
                                exit;
                        }*/
                    }
                    else
                    {
                            $iTotalNumRequestSent = $iNumRequestSent ;
                    }
                    if(!empty($latLongPointsAry))
                    {
                        $kWHSSearch = new cWHSSearch();
                        $ctr=0;

                        echo "<br /> started calling API for Zone ID:".$idHaulagePricingModelArys['idHaulagePricingModel']." on ".date('d/m/Y H:i:s');
                        $counter = 0;
                        foreach($latLongPointsAry as $latLongPointsArys)
                        {
                            $loop_coun = count($latLongPointsArys['szLatitude']);			  		
                            for($i=0;$i<$loop_coun;$i++)
                            {
                                $lok2 = $latLongPointsArys['szLatitude'][$i];
                                $lok1 = $latLongPointsArys['szLongitude'][$i];
                                /*$url = __YAHOO_GEOCODE_API_URL__.$latLongPointsArys['szLatitude'][$i].',+'.$latLongPointsArys['szLongitude'][$i].'&gflags=R&flags=J&appid='.__YAHOO_GEOCODE_API_KEY__;
                                $geocode = $kWHSSearch->curl_get_file_contents($url);

                                $response = json_decode($geocode);
                                $location = $response->ResultSet->Results[0]->countrycode;
                                $iErrorCode = $response->ResultSet->Error;
                                $szErrorMessage = $response->ResultSet->ErrorMessage;*/
                                $response=array();
                                $geocode='';
                                //?lat=47.03&lng=10.2&username=demo
                                //$url = "http://ws.geonames.org/countryCode?type=JSON&lat=".round($lok2,6)."&lng=".round($lok1,6);
                                $url = "http://api.geonames.org/countryCode?type=JSON&lat=".round($lok2,6)."&lng=".round($lok1,6)."&username=ajaywhiz";

//                                echo "<br/><br/>".$url."<br/><br/>";
                                $geocode = $kWHSSearch->curl_get_file_contents($url);

                                $response = json_decode($geocode,true);
                                if(!empty($response))
                                {
//					 		print_r($response);
//					 		echo "<br/><br/>";
//                                                      die;
                                }
                                if($response['countryCode']!='')
                                {
                                    $iErrorCode=0;
                                }  
                                /*
                                    The Web service returns the following error codes in the response:
                                    0: No error
                                    1: Feature not supported
                                    100: No input parameters
                                    102: Address data not recognized as valid UTF-8
                                    103: Insufficient address data
                                    104: Unknown language
                                    105: No country detected
                                    106: Country not supported
                                    10NN: Internal problem detected				  			
                                */
                                //var_dump($response);
                                if($iErrorCode==0)
                                {
                                    $szCountryCode = $response['countryCode'];
                                    $szCity = $response->ResultSet->Results[0]->city;
                                    $szState = $response->ResultSet->Results[0]->state; ;
                                    $szCountry =$response->ResultSet->Results[0]->country; 

                                    //echo "<br /> <b> City: </b> ".$szCity;
                                    //echo "<br /> <b>State:</b> ".$szState;
                                    //echo "<br /> <b>Country:</b> ".$szCountry;
                                    //echo "<br /> <b>Country ISO :</b> ".$szCountryCode;
                                    //die;
                                    //echo "<br /><br /><br />Calling API for Latititude: ".$latLongPointsArys['szLatitude'][$i].", Longitude: ".$latLongPointsArys['szLongitude'][$i] ;

                                    if(!empty($szCountryCode))
                                    {
                                        $kConfig = new cConfig();
                                        $latLongCountry = $kConfig->getCountryName(false,$szCountryCode);

                                        if(!empty($countryIsoAry) && !in_array($latLongCountry,$countryIsoAry['idCountry']))
                                        {
                                             $countryIsoAry['szCountryCode'][$counter] = $szCountryCode ;
                                             $countryIsoAry['idCountry'][$counter] = $latLongCountry ;
                                             $countryIsoAry['idHaulagePricingModel'][$counter] = $idHaulagePricingModelArys['idHaulagePricingModel'] ;
                                             $countryIsoAry['szGoogleCode'][$counter] = $url ;
                                             $counter++;
                                        }
                                        else if(empty($countryIsoAry))
                                        {
                                             $countryIsoAry['szCountryCode'][$counter] = $szCountryCode ;
                                             $countryIsoAry['idHaulagePricingModel'][$counter] = $idHaulagePricingModelArys['idHaulagePricingModel'] ;
                                             $countryIsoAry['idCountry'][$counter] = $latLongCountry ;
                                             $countryIsoAry['szGoogleCode'][$counter] = $url ;
                                             $counter++;
                                        }
                                    }
                                }
                                else if($iErrorCode>0)
                                {					    	  
                                    //echo "<br /> Error Code: ".$iErrorCode ;
                                   // echo "<br /> Error Message : ".$szErrorMessage ;
                                    $szCountry = $szErrorMessage ;
                                    $szCountryCode = $iErrorCode ;
                                }
                                $iNumRequestSent++;
                                $apiResponseAry[$ctr]['szApiResponse'][] = $szCountry."(".$szCountryCode.")";


                                $globallatLongPointsAry[$idHaulagePricingModelArys['idHaulagePricingModel']]['szLatitude'][] = $latLongPointsArys['szLatitude'][$i] ;
                                $globallatLongPointsAry[$idHaulagePricingModelArys['idHaulagePricingModel']]['szLongitude'][] = $latLongPointsArys['szLongitude'][$i] ;
                                $globallatLongPointsAry[$idHaulagePricingModelArys['idHaulagePricingModel']]['szApiResponse'][] = $szCountry."(".$szCountryCode.")";
                            }
                            $ctr++;
                        }
                        
                        if(empty($countryIsoAry))
                        {
                            /*
                            * We didn't found any country from API in Step-1, So now we are picking country name by taking average latitute and longitute
                            */
                            echo "<br> Max Latitute: ".$szGlobalMaxLatitute;
                            echo "<br> Min Latitute: ".$szGlobalMinLatitute; 
                            echo "<br> Max Longitute: ".$szGlobalMaxLongitute;
                            echo "<br> Min Longitute: ".$szGlobalMinLongitute;
                            
                            $szAverageLatitute = round((float)(($szGlobalMaxLatitute + $szGlobalMinLatitute)/2),10);
                            $szAverageLongitute = round((float)(($szGlobalMaxLongitute + $szGlobalMinLongitute)/2),10);
                            
                            echo "<br><br> Average Latitute: ".$szAverageLatitute;
                            echo "<br> Average Longitute: ".$szAverageLongitute;
                            
                            $postcodeInputAry['szLatitute'] = $szAverageLatitute;
                            $postcodeInputAry['szLongitute'] = $szAverageLongitute;
                            $postcodeListAry = getAddressDetailsByCordinate($postcodeInputAry);
                            
                            if(!empty($postcodeListAry['szCountryCode']))
                            {
                                echo "<br> Country: ".$postcodeListAry['szCountry']." (".$postcodeListAry['szCountryCode'].") ";
                                $szCountryCode = $postcodeListAry['szCountryCode'] ;
                                $kConfig = new cConfig();
                                $latLongCountry = $kConfig->getCountryName(false,$szCountryCode);
                                if($latLongCountry>0)
                                {
                                    $countryIsoAry['szCountryCode'][$counter] = $szCountryCode ;
                                    $countryIsoAry['idHaulagePricingModel'][$counter] = $idHaulagePricingModelArys['idHaulagePricingModel'] ;
                                    $countryIsoAry['idCountry'][$counter] = $latLongCountry ;
                                    $countryIsoAry['szGoogleCode'][$counter] = $url ;
                                    $counter++;
                                    
                                    $globallatLongPointsAry[$idHaulagePricingModelArys['idHaulagePricingModel']]['szLatitude'][] = $szAverageLatitute ;
                                    $globallatLongPointsAry[$idHaulagePricingModelArys['idHaulagePricingModel']]['szLongitude'][] = $szAverageLongitute ;
                                    $globallatLongPointsAry[$idHaulagePricingModelArys['idHaulagePricingModel']]['szApiResponse'][] = $szCountry."(".$szCountryCode.")";
                                } 
                            }
                        }

                        if(!empty($countryIsoAry))
                        {
                            $kConfig = new cConfig();
                            //echo "<br /> List of countries covered in zone ID :".$idHaulagePricingModelArys['idHaulagePricingModel']." <br />";
                            $kHaulagePricing->deleteOldHaulageCountry($idHaulagePricingModelArys['idHaulagePricingModel']); 
                            for($x=0;$x<count($countryIsoAry['szCountryCode']);$x++)
                            {
                                $data=array();
                                $data['idCountry'] = $countryIsoAry['idCountry'][$x];
                                $data['szGoogleCode'] = $countryIsoAry['szGoogleCode'][$x];
                                $data['szCountryCode'] = $countryIsoAry['szCountryCode'][$x];
                                $data['idHaulagePricingModel'] = $countryIsoAry['idHaulagePricingModel'][$x];
                                $countryAry = $kConfig->getAllCountryInKeyValuePair($data['idCountry']);
                                echo $countryAry[$data['idCountry']]['szCountryName']."<br/>";
                                $kHaulagePricing->addHaulageZonesCountries($data);

                                //echo "<br /> Putting data for warehouse id ".$kHaulagePricing->idWarehouse ;
                                //echo " and country id ".$data['idCountry']."<br />";
                                //$kHaulagePricing->addHaulageWarehouseCountries($data['idCountry'],$kHaulagePricing->idWarehouse);
                            }
                            $kHaulagePricing->updateHaulageZones($idHaulagePricingModelArys['idHaulagePricingModel']);
                        }
                        else
                        {
                            echo "<br/><br/> Zone ID: ".$idHaulagePricingModelArys['idHaulagePricingModel']." does not belongs to any country.";			
                        }			    
		   }
		   $iHaulageZoneProcess++ ;
		   $iTotalNumRequestSent += $iNumRequestSent ;
			
		   echo "<br /> Execution completed for Zone ID:".$idHaulagePricingModelArys['idHaulagePricingModel']." on ".date('d/m/Y H:i:s');
		}
		//print_r($latLongPointsAry);	
		if(!empty($globallatLongPointsAry))
		{
                    ?>	
			<br /><br />
			<table cellspacing="0" cellpadding="0" border="0" class="format-3" width="40%" >
			<tr>
                            <th align="left">Points</th>
                            <th align="left">Latitude</th>
                            <th align="left">Longitude</th>	
                            <th align="left">Api Response</th>				
			</tr>				
			<?php
			$vertex_ctr=1;
			foreach($globallatLongPointsAry as $key=>$latLongPointsArys)
			{
				$loop_coun = count($latLongPointsArys['szLatitude']) ;
				?>
				<tr>
					<td colspan="4"><h3>Points for Haulage pricing ID: <?=$key?></h3></td>
				</tr>
				<?php
				for($i=0;$i<($loop_coun);$i++)
				{	
					if(!empty($latLongPointsArys['szLatitude'][$i]))
					{					
						?>
						<tr>
							<td>Point <?=$i+1?></td>
							<td><?=round($latLongPointsArys['szLatitude'][$i],6)?></td>
							<td><?=round($latLongPointsArys['szLongitude'][$i],6)?></td>
							<td><?=$latLongPointsArys['szApiResponse'][$i]?></td>								
						</tr>
						<?php
					}	
				}	
			  }		
			  $vertex_ctr++; 
			}
		?>
		</table>
	<?php
		//print_r($apiResponseAry);
		$kHaulagePricing->addYahooApiLogs($iNumRequestSent);
		
		echo "<br><br> Script stoped on ".date('d/m/Y H:i:s');
	}
}
else
{
	$data['szLatitude'] = $_REQUEST['szLatitude'] ;
	$data['szLongitude'] = $_REQUEST['szLongitude'];
	$data['idHaulagePricingModel'] = $_REQUEST['id'];
	/*
	$data['szLatitude'] = 37.090240;
	$data['szLongitude'] = -95.712891 ;
	$data['idHaulagePricingModel'] = 2;
	*/
	echo "<br /> Yahoo Geocode Demo<br /> ";
	echo "<br/> Asked Latitude: ".$data['szLatitude'] ;
	echo "<br/> Asked Longitude: ".$data['szLongitude'] ;
	/*
	$kWHSSearch = new cWHSSearch();
	$url = __YAHOO_GEOCODE_API_URL__.$data['szLatitude'].',+'.$data['szLongitude'].'&gflags=R&flags=P&appid='.__YAHOO_GEOCODE_API_KEY__;
	$geocode = $kWHSSearch->curl_get_file_contents($url);
	//$geocode=file_get_contents('http://where.yahooapis.com/geocode?q='.$data['szLatitude'].',+'.$data['szLongitude'].'&gflags=R&flags=P&appid='.__YAHOO_GEOCODE_API_KEY__);
  
	$result_ary = unserialize ($geocode);
	echo "<br /><h3>Result from API: </h3> ";
  	echo "<br /><strong> City</strong> : ".$result_ary['ResultSet']['Result'][0]['city'] ;
  	echo "<br /><strong> State</strong>: ".$result_ary['ResultSet']['Result'][0]['state'] ;
  	echo "<br /><strong> Country</strong>: ".$result_ary['ResultSet']['Result'][0]['country'] ;
  	echo "<br /><strong> Country ISO Code </strong>: ".$result_ary['ResultSet']['Result'][0]['countrycode'] ;
	echo "<br />";
  	die;
 */
 
  $latLongPointsAry = $kHaulagePricing->isLocationExistsInZone_cronjob($data);
  if(!empty($latLongPointsAry))
  {
  		$countryIsoAry = array();
  		echo "<br /> <h3>Result from API: </h3> ";
	  	foreach($latLongPointsAry as $latLongPointsArys)
	  	{
	  		for($i=0;$i<count($latLongPointsArys['szLatitude']);$i++)
	  		{
	  			$kWHSSearch = new cWHSSearch();
	  			$result_ary = array();
	  			$url = "http://ws.geonames.org/countryCode?type=JSON&lat=".$latLongPointsArys['szLatitude'][$i]."&lng=".$latLongPointsArys['szLongitude'][$i];
				//$url = __YAHOO_GEOCODE_API_URL__.$latLongPointsArys['szLatitude'][$i].',+'.$latLongPointsArys['szLongitude'][$i].'&gflags=R&flags=P&appid='.__YAHOO_GEOCODE_API_KEY__;
				$geocode = $kWHSSearch->curl_get_file_contents($url);
				$result_ary = json_decode ($geocode);
	  			echo "<br /> Point".($i+1)." : ".$latLongPointsArys['szLatitude'][$i].", ".$latLongPointsArys['szLongitude'][$i] ;
				//echo "<br /><br /><strong> City</strong> : ".$result_ary['ResultSet']['Result'][0]['city'] ;
			  	//echo "<br /><strong> State</strong>: ".$result_ary['ResultSet']['Result'][0]['state'] ;
			  	echo "<br /><strong> Country</strong>: ".$result_ary['countryName'] ;
			  	echo "<br /><strong> Country ISO Code </strong>: ".$result_ary['countryCode'] ;
			  	echo "<br />";
			}		
	    }
   }
}

?>