<?php
/**
 * Change Customer Account Password
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

$successPasswordUpdatedFlag=false;
$t_base = "Users/AccountPage/";
$t_base_error = "Error";
$kConfig = new cConfig();
$kUser = new cUser();
$mode = sanitize_all_html_input(trim($_REQUEST['mode']));

if($mode=='DISPLAY_FIRST_TIME_PASSWORD_POPUP')
{
    $kUser->getUserDetails($_SESSION['user_id']);
    $iPageType = sanitize_all_html_input(trim($_REQUEST['type']));
    echo "SUCCESS||||";
    echo display_first_time_password_popup($kUser,$iPageType);
    die;
}
else if($mode=='TYPE_PASSWORD_POPUP')
{
    $kUser->getUserDetails($_SESSION['user_id']);
    $iPageType = sanitize_all_html_input(trim($_REQUEST['type']));
    echo "SUCCESS||||";
    echo display_authentication_password($kUser,$iPageType);
    die;
}
else if(!empty($_POST['firstTimePassword']))
{ 
    if($kUser->firstTimeChangePassword($_POST['firstTimePassword'],$_SESSION['user_id']))
    {
        $iPageType = sanitize_all_html_input(trim($_POST['firstTimePassword']['iPageType']));

        if($iPageType==1)
        {
            $szRedirectUrl = __BASE_URL__."/myBooking/" ;
        }
        else
        {
            $szRedirectUrl = __BASE_URL__."/myAccount/" ;
        }

        echo "REDIRECT||||".$szRedirectUrl."||||";
//        echo "SUCCESS||||";
//        $szTitle = t($t_base.'fields/new_password');
//        $szMessage = t($t_base.'fields/success_message');
//        echo success_message_popup($szTitle,$szMessage,'user_account_login_popup',true,$szRedirectUrl);
//        die;
    }
    else
    {
        echo "ERROR||||";
        echo display_first_time_password_popup($kUser);
        die;
    }
}
else if(!empty($_POST['typePasswordAry']))
{ 
	if($kUser->checkUserAccountAuthentication($_POST['typePasswordAry'],$_SESSION['user_id']))
	{
		$iPageType = sanitize_all_html_input(trim($_POST['typePasswordAry']['iPageType']));
		
		if($iPageType==1)
		{
			$szRedirectUrl = __BASE_URL__."/myBooking/" ;
		}
		else
		{
			$szRedirectUrl = __BASE_URL__."/myAccount/" ;
		}
		
		echo "REDIRECT||||"; 
		echo $szRedirectUrl; 
		die;
	}
	else
	{
		echo "ERROR||||";
		echo display_authentication_password($kUser);
		die;
	}
}