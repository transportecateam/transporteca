<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );

$kMetrics = new cMetics();

$t_base = "management/forgotPassword/";
$t_base_error="Error";
$successFlag=false;
if(!empty($_POST))
{
    if($kMetrics->updatePassword($_POST,$_SESSION['metrics_user_id']))
    {
            $successFlag=true;
    }
}


?>
<div id="forgot_password">
    <div id="popup-bg"></div>
    <div id="popup-container">
    <div class="popup signin-popup signin-popup-verification" style="width:300px;">
    <h5><strong><?=t($t_base.'messages/change_password');?></strong></h5>
    <?php if($successFlag) {?>
        <p><?=t($t_base.'messages/change_password_success_msg1');?></p>
        <br />
        <p align="center"><a href="javascript:void(0);" class="button1" onclick="showHide('ajaxLogin');"><span><?=t($t_base.'fields/close')?></span></a></p>
    <?php } 
        else
        {
            if(!empty($kMetrics->arErrorMessages)){ ?>
                <div id="regError" class="errorBox ">
                    <div class="header"><?=t($t_base_error.'/please_following');?></div>
                    <div id="regErrorList">
                        <ul>
                        <?php
                                foreach($kMetrics->arErrorMessages as $key=>$values)
                                {
                                ?><li><?=$values?></li>
                                <?php	
                                }
                        ?>
                        </ul>
                    </div>
                </div>
            <?php }?>
            <form name="changePassword" action="javascript:void(0)" id="changePassword" method="post">
                    <div class="oh">
                        <p class="fl-33 metrics-label"><?=t($t_base.'fields/new_password');?></p>
                        <p class="fl-65" style="width:67%;"><input type="password" onkeyup="on_enter_key_press(event,'forgot_passward_popup','')" name="szNewPassword" id="szNewPassword" value=""/></p>
                    </div>
                    <div class="oh">
                        <p class="fl-33 metrics-label"><?=t($t_base.'fields/repeat_password');?></p>
                        <p class="fl-65" style="width:67%;"><input type="password" onkeyup="on_enter_key_press(event,'forgot_passward_popup','')" name="szConPassword" id="szConPassword" value=""/></p>
                    </div>
                    <br />
                    <input type="hidden" name="szRedirectUrl"  value="<?=$_POST['szRedirectUrl']?>">
                    <p align="center"><a href="javascript:void(0);" class="button2 same-width-button" onclick="redirect_url('<?php echo __BASE_METRICS_URL__."/logout/";?>')"><span><?=t($t_base.'fields/close')?></span></a> <a href="javascript:void(0);" class="button1 same-width-button" onclick="updateMetricsPassword();"><span><?=t($t_base.'fields/confirm');?></span></a></p>
            </form>
            <?php } ?>	
        </div>
    </div>
</div>