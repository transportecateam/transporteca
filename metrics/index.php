<?php
/**
 * Forwarder My Company  
 */
 ob_start();
session_start();
$szMetaTitle="Transporteca Control Panel - Investor's Log-in";
$szMetaKeywords = "lcl, bookings, booking, leading, world, transport, control, panel, pricing";
$szMetaDescription="Freight forwarders upload best services and rates to the Transporteca Control Panel - The World's Leading Site for LCL Bookings. Customer booking and pay online";
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
 
require_once( __APP_PATH_LAYOUT__ . "/metrics_header.php" );

$kMetrics = new cMetics();
$t_base = "management/home/";
if(!empty($_POST['loginArr']))
{
    if($kMetrics->metricsLogin($_POST['loginArr']))
    {
        $id = $_SESSION['metrics_user_id'];
        $redirect_url=__METRICS_DASHBOARD_URL__;
        header("Location:".$redirect_url);
        exit(); 
    }
} 
if((int)$_SESSION['metrics_user_id']>0)
{
    $details=$kMetrics->getInvestorsDetails($_SESSION['metrics_user_id']);
   
    $redirect_url=__METRICS_DASHBOARD_URL__;
    header("Location:".$redirect_url);
    exit(); 
}

?>
<div id='ajaxLogin' style="display: none;"></div>
<div id="hsbody" style="padding-bottom: 100px;">
<form id="admin_login_form"  method="post">
    <div align="center">
        <br/>
        <br/>
        <h2><?=t($t_base.'title/welcome_to_transporteca_metrics')?> </h2><br/>
        <div class="metrics-login-box">		
        <?php
            if(!empty($kMetrics->arErrorMessages)){
            ?>
            <div align="left" id="regError" class="errorBox ">
                <div class="header"><?=t($t_base.'fields/please_following');?></div>
                <div id="regErrorList">
                    <ul>
                    <?php
                        foreach($kMetrics->arErrorMessages as $key=>$values)
                        {
                            ?><li><?=$values?></li><?php	
                        }
                    ?>
                    </ul>
                </div>
            </div>
            <?php } ?>
            <br/>
            <br/>
            <div class="oh">
                <p class="fl-30 metrics-label"><?=t($t_base.'fields/signin_email')?></p>
                <p class="fl-70 metrics-text"><input type="text" id="szEmail" name="loginArr[szEmail]" value="<?=$_POST['loginArr']['szEmail']?>" tabindex="1"/></p>
            </div>
            <div class="oh">
                <p class="fl-30 metrics-label"><?=t($t_base.'fields/password')?></p>
                <p class="fl-70 metrics-text"><input type="password" id="szPassword" name="loginArr[szPassword]" tabindex="2" onkeyup="on_enter_key_press(event,'admin_login_form');"/></p>
            </div>
            <p align="right"><a href="javascript:void(0)" onclick="open_forgot_passward_popup('ajaxLogin')" class="f-size-12"><?=t($t_base.'links/forgot_pass')?></a></p>
            <br style="line-height: 16px;" />
            <p align="center">
                <a href="javascript:void(0)" class="button1" onclick="$('#admin_login_form').submit()" tabindex="7"><span><?=t($t_base.'fields/sign_in')?></span></a>
            </p>
            <br/> 
            <br/>
        </div>			
    </div>	
    <input type="submit" style="visibility: hidden;" />
</form>	
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>