<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
if(!defined('__BASE_URL__')) 
{
    define ('__BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
    define ('__BASE_URL_SECURE__', __MAIN_SITE_HOME_PAGE_URL__);
}

$_SESSION['metrics_user_id']='';
unset($_SESSION['metrics_user_id']); 

header('Location:'.__MAIN_SITE_HOME_PAGE_URL__);
exit();
?>