<?php 
/**
  *  Metrics--- Dashboard
  */

ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Dashboard";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once( __APP_PATH_LAYOUT__ . "/metrics_header.php" );
checkAuthMetrics();

$t_base	= "management/dashboard/";
$kMetrics = new cMetics();
$imgArr = $kMetrics->getMetricsDashboardDataGraph(); 
  

$details=$kMetrics->getInvestorsDetails($_SESSION['metrics_user_id']);
?> 
<div id="hsbody">
    <div style="direction: ltr;">
        <div style="float:left;">
            <p style="padding-left: 0px;font-weight:bolder;font-size:18px;"><?=t($t_base."title/number_of_booking");?></p>
            <img src="<?=__MANAGEMENT_URL__."/".$imgArr[1]."?n=".time(); ?>" alt="<?=t($t_base."title/number_of_booking");?>">
        </div> 
        <div style="float:right;">
            <p style="padding-left: 17px;font-weight:bolder;font-size:18px;"><?=t($t_base."title/turnover_in_usd_equivalent");?></p>
            <img src="<?=__MANAGEMENT_URL__."/".$imgArr[2]."?n=".time(); ?>" alt="<?=t($t_base."title/turnover_in_usd_equivalent");?>">
        </div>
    </div>
    <div class="clear-all"></div>
    <br />
    <div style="direction: ltr;"> 
        <div style="float:left;">
            <p style="padding-left: 0px;font-weight:bolder;font-size:18px;"><?php echo t($t_base."title/number_of_booking"). " (".t($t_base."title/accumulated").")"; ?></p>
            <img src="<?=__MANAGEMENT_URL__."/".$imgArr[3]."?n=".time();?>" alt="<?php echo t($t_base."title/number_of_booking"). " (".t($t_base."title/accumulated").")"; ?>">
        </div>
        <div style="float:right;">
            <p style="padding-left: 17px;font-weight:bolder;font-size:18px;"><?php echo t($t_base."title/sales_revenue_usd"); ?></p>
            <img src="<?=__MANAGEMENT_URL__."/".$imgArr[4]."?n=".time();?>" alt="<?php echo t($t_base."title/sales_revenue_usd"); ?>">
        </div>
    </div>
    <div class="clear-all"></div>
    <br />
    <div style="direction: ltr;"> 
        <div style="float:left;">
            <p style="padding-left: 0px;font-weight:bolder;font-size:18px;"><?php echo t($t_base."title/number_of_customer"). " (".t($t_base."title/accumulated").")"; ?></p>
            <img src="<?=__MANAGEMENT_URL__."/".$imgArr[5]."?n=".time();?>" alt="<?php echo t($t_base."title/number_of_customer"). " (".t($t_base."title/accumulated").")"; ?>">
        </div>
        <div style="float:right;">
            <p style="padding-left: 17px;font-weight:bolder;font-size:18px;"><?php echo t($t_base."title/number_of_searches"); ?></p>
            <img src="<?=__MANAGEMENT_URL__."/".$imgArr[6]."?n=".time();?>" alt="<?php echo t($t_base."title/number_of_searches"); ?>">
        </div>
    </div>  
    <div class="clear-all"></div>
    <br /> 
    <div style="direction: ltr;"> 
        <div style="float:left;">
            <p style="padding-left: 0px;font-weight:bolder;font-size:18px;"><?php echo t($t_base."title/number_of_repeat_customer"). " (".t($t_base."title/accumulated").")";; ?></p>
            <img src="<?=__MANAGEMENT_URL__."/".$imgArr[7]."?n=".time();?>" alt="<?php echo t($t_base."title/number_of_repeat_customer"); ?>">
        </div>
        <div style="float:right;">
            <p style="padding-left: 17px;font-weight:bolder;font-size:18px;"><?php echo t($t_base."title/customer_logged_in_unique"); ?></p>
            <img src="<?=__MANAGEMENT_URL__."/".$imgArr[8]."?n=".time();?>" alt="<?php echo t($t_base."title/customer_logged_in_unique"); ?>">
        </div> 
    </div>
    <div class="clear-all"></div>
    <br /> 
    <div style="direction: ltr;"> 
        <div style="float:left;">
            <p style="padding-left: 0px;font-weight:bolder;font-size:18px;"><?php echo t($t_base."title/working_capital_usd"); ?></p>
            <img src="<?=__MANAGEMENT_URL__."/".$imgArr[9]."?n=".time();?>" alt="<?php echo t($t_base."title/working_capital_usd"); ?>">
        </div>
        <div style="float:right;">
            <p style="padding-left: 17px;font-weight:bolder;font-size:18px;"><?php echo t($t_base."title/average_revenue_per_booking_accumulated"); ?></p>
            <img src="<?=__MANAGEMENT_URL__."/".$imgArr[10]."?n=".time();?>" alt="<?php echo t($t_base."title/average_revenue_per_booking_accumulated"); ?>">
        </div> 
    </div>
</div>
<?php
	if($details['iPasswordUpdated']=='1')
	{
	?>
	<script type="text/javascript">
		open_change_passward_form_popup();
	</script>
<?php
	} 
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" ); 
?>
