<?php
/**
 * Customer Account Confirmation 
 */
 ob_start();
session_start();
$szMetaTitle= __EMAIL_CONFIRMATION_META_TITLE__;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/header_new.php" );

$t_base = "Users/AccountPage/";

$kUser = new cUser();

$szConfirmationKey=trim($_REQUEST['confirmationKey']);
$szBookingRandomKey=trim($_REQUEST['booking_random_key']);
?>
<div id="hsbody">
<?php
if(!empty($_COOKIE[__COOKIE_NAME__]))
{
	$redirect_url=$_COOKIE[__COOKIE_NAME__];
	if($redirect_url==__CREATE_ACCOUNT_PAGE_URL__)
	{
		$redirect_url=__HOME_PAGE_URL__;
	}
}
else
{
	$redirect_url=__HOME_PAGE_URL__;
}

if(!empty($szBookingRandomKey))
{
	$kBooking = new cBooking();
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomKey);
	$postSearchAry = $kBooking->getBookingDetails($idBooking);
	$_SESSION['booking_confirmation_random_key'] = $szBookingRandomKey ;
	
	if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
	{
            $redirect_url = __HOME_PAGE_URL__ ;
	}
	else if(($postSearchAry['idForwarder']>0) && ($postSearchAry['idWarehouseTo']>0) && ($postSearchAry['idWarehouseFrom']>0) && ($postSearchAry['idShipperConsignee']>0))
	{
            $redirect_url = __BOOKING_CONFIRMATION_PAGE_URL__.'/'.$szBookingRandomKey.'/';
	}
	else if(($postSearchAry['idForwarder']>0) || ($postSearchAry['idWarehouseTo']>0) || ($postSearchAry['idWarehouseFrom']>0))
	{
            $redirect_url =__BOOKING_DETAILS_PAGE_URL__.'/'.$szBookingRandomKey.'/';
	}
	else
	{
            if($postSearchAry['iFromRequirementPage']==1)
            {
                $redirect_url = __REQUIREMENT_PAGE_URL__."/".$szBookingRandomKey.'/' ;
            }
            else
            {
                $redirect_url = __LANDING_PAGE_URL__."/".$szBookingRandomKey.'/' ;
            }	
	}
} 
if(!empty($szConfirmationKey))
{
	if($kUser->checkConfirmationKey($szConfirmationKey))
	{
            $_SESSION['valid_confirmation']='1';
            header('Location:'.$redirect_url);
            exit();
	}
	else
	{
            $_SESSION['valid_confirmation']='2';
            header('Location:'.$redirect_url);
            exit();
	}
}
else
{
	//echo "<br /> OUTER ELSE ";
	$_SESSION['valid_confirmation']='2';
	header('Location:'.$redirect_url);
	exit();
}
?>
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/footer.php" );
?>