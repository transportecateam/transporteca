<?php
/**
 * Edit User Information
 */
  ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_header.php" );
$t_base = "Users/AccountPage/";

$kUser = new cUser();
$kUser->getUserDetails($_SESSION['user_id']);
if($_REQUEST['successFlg']=='2')
{
	echo "<p style=color:green>".t($t_base.'messages/user_info_successfully_updated')."</p>";
}
?>
<div id="user_information">
	<h5><strong><?=t($t_base.'fields/user_info');?></strong> <a href="javascript:void(0)" onclick="cancel_user_info(1);"><?=t($t_base.'fields/edit');?></a></h5>
	
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/f_name');?></span>
		<span class="field-container"><?=$kUser->szFirstName?></span>
	</div>
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/l_name');?></span>
		<span class="field-container"><?=$kUser->szLastName?></span>
	</div>
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/c_name');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
		<span class="field-container"><?=$kUser->szCompanyName?></span>
	</div>
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/address_line1');?></span>
		<span class="field-container"><?=$kUser->szAddress1?></span>
	</div>
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/address_line2');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
		<span class="field-container"><?=$kUser->szAddress2?></span>
	</div>
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/address_line3');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
		<span class="field-container"><?=$kUser->szAddress3?></span>
	</div>
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/postcode');?></span>
		<span class="field-container"><?=$kUser->szPostcode?></span>
	</div>
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/city');?></span>
		<span class="field-container"><?=$kUser->szCity?></span>
	</div>
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/p_r_s');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
		<span class="field-container"><?=$kUser->szState?></span>
	</div>
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/country');?></span>
		<span class="field-container"><?=$kUser->szCountryName?></span>
	</div>
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/prefer_currency');?></span>
		<span class="field-container">
			<?=$kUser->szCurrencyName?>
		</span>
	</div>
	</div>
	<br />
	<br />
	<div id="contact_info">
		<h5><strong><?=t($t_base.'messages/contact_info');?></strong> <a href="javascript:void(0)" onclick="cancel_user_info(2);"><?=t($t_base.'fields/edit');?></a></h5>
		
		<div class="ui-fields">
			<span class="field-name"><?=t($t_base.'fields/email');?></span>
			<span class="field-container"><?=$kUser->szEmail?> <?php if($kUser->iConfirmed=='1')
			{?><span style='color:#8A9454'>(<?=t($t_base.'messages/verified');?></span>)<?php }else {?><span style='color:red'>(<?=t($t_base.'messages/not_verified');?>)</span><?php }?></span>
		</div>
		<div class="ui-fields">
			<span class="field-name"><?=t($t_base.'fields/contact_by_email');?></span>
			<span class="field-container checkbox-ab"><input type="checkbox" <?php if((int)$kUser->iAcceptNewsUpdate=='1'){?> checked <?php }?> disabled="disabled"/><?=t($t_base.'messages/please_news_updates');?></span>
		</div>
		<div class="ui-fields">
			<span class="field-name"><?=t($t_base.'fields/p_number');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
			<span class="field-container"><?=$kUser->szPhoneNumber?></span>
		</div>
	</div>
	<br />
	<div id="user_change_password">
					<div class="ui-fields">
						<span class="field-name"><strong><?=t($t_base.'fields/password');?></strong> <a href="javascript:void(0);" onclick="cancel_user_info(3);"><?=t($t_base.'fields/edit');?></a></span>
						<span class="field-container">*************</span>
					</div>
				</div>
