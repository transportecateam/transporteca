<?php

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once(__APP_PATH_LAYOUT__."/ajax_header.php");

$kBooking = new cBooking();
$kConfig=new cConfig();

$t_base="LandingPage/";
$idUser = $_SESSION['user_id'] ;
if($idUser<=0)
{
	?>
	<script type="text/javascript">
		redirect_url('<?=__BASE_URL__?>');
	</script>
	<?
}
if(!empty($_REQUEST['arrUserEmail']))
{
	if($_REQUEST['arrUserEmail']['szEmail']==t($t_base.'fields/email'))
	{
		$_REQUEST['arrUserEmail']['szEmail'] = '';
	}
	if($kUser->sendShareSiteEmail($_REQUEST['arrUserEmail']))
	{
		//echo "SUCCESS||||";
		die;
	}
}
$kUser->getUserDetails($idUser);
$replace_ary = array();
$replace_ary['szFirstName'] = $kUser->szFirstName ;
$replace_ary['szLastName'] = $kUser->szLastName ;
$replace_ary['szSiteUrlLink'] ="<a href='".__BASE_URL_SECURE__."'>".__BASE_URL_SECURE__."</a>" ;
$replace_ary['szSiteUrl'] =__BASE_URL_SECURE__ ;
 
$cmsDetailsAry = createEmailMessage('__SHARE_THIS_SITE__',$replace_ary); 


/*
onfocus="blank_text(this.id,'<?=t($t_base.'fields/email');?>')" onblur="show_text(this.id,'<?=t($t_base.'fields/email');?>')"
* */
?>
		<div id="popup-bg"></div>
			<div id="popup-container">
				<div class="popup signin-popup signin-popup-verification" style="width:480px;margin-top:-10px;">
					<h5><?=t($t_base.'title/share_this_site')?></h5>
					<p><?=t($t_base.'title/share_this_site_message')?></p>
					<br />
					<? if(!empty($kUser->arErrorMessages)){
						//echo "ERROR||||";
						$t_base_error = "Error";
						?>
						<div id="regError" class="errorBox">
						<div class="header"><?=t($t_base_error.'/please_following');?></div>
						<div id="regErrorList">
						<ul>
						<?php
							foreach($kUser->arErrorMessages as $key=>$values)
							{
							?><li><?=$values?></li>
							<?php	
							}
						?>
						</ul>
						</div>
						</div>
						<?php }?>
						<form name="sendUserEmail" id="sendUserEmail" method="post">
						<table>
							<tr>
								<td><?=t($t_base.'fields/email')?></td>
								<td> <input type="text" name="arrUserEmail[szEmail]" id="szShareLinkEmail" value="<?=!empty($_POST['arrUserEmail']['szEmail'])?$_POST['arrUserEmail']['szEmail']:''?>" style="width: 350px;"/></td>
							</tr>
							<tr>
								<td><?=t($t_base.'fields/subject')?></td>
								<td><input type="text" name="arrUserEmail[szSubject]" id="szSubject" value="<?=!empty($_POST['arrUserEmail']['szSubject'])?$_POST['arrUserEmail']['szSubject']:$cmsDetailsAry['szEmailSubject']?>" style="width: 350px;"/></td>
							</tr>
							<tr>
								<td valign="top"><?=t($t_base.'fields/message')?></td>
								<td>
									<textarea style="width: 350px;" cols="53" rows="8" name="arrUserEmail[szMessage]" id="szMessage"><?=!empty($_POST['arrUserEmail']['szMessage'])?$_POST['arrUserEmail']['szMessage']:$cmsDetailsAry['szEmailBody']?></textarea>
								</td>
							</tr>
							<tr>
								<td colspan="2" align="center">
									<a href="javascript:void(0)" class="button1" tabindex="7" onclick="send_share_site_link()" id="confirm_email"><span><?=t($t_base.'fields/send')?></span></a>
									<a href="javascript:void(0)" class="button2" tabindex="7" onclick="showHide('share_link_popup_div')" id="cancel_email"><span><?=t($t_base.'fields/cancel')?></span></a>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>	
