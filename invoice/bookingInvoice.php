<?php
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_header.php" );
require('invoice.php');

$kBooking = new cBooking();
$kConfig = new cConfig();
$bookingDataArr=$kBooking->getExtendedBookingDetails('19');


$shipperCountry=$kConfig->getCountryName($bookingDataArr['idShipperCountry']);
$pdf = new PDF_Invoice( 'P', 'mm', 'A4' );
$pdf->AddPage();
$pdf->invoiceTitle('Invoice');
$pdf->addressDetials($bookingDataArr['szShipperCompanyName'],
                  $bookingDataArr['szShipperAddress']." ".$bookingDataArr['szShipperAddress2']."\n".
                  $bookingDataArr['szShipperPostCode']." ".$bookingDataArr['szShipperCity']."\n".
                  $shipperCountry."\n".
                  "Att :".$bookingDataArr['szShipperFirstName']." ".$bookingDataArr['szShipperLastName']);
//$pdf->fact_dev( "Devis ", "TEMPO" );
$pdf->temporaire( "Transport-eca" );
//$pdf->addDate( "03/12/2003");
//$pdf->addClient("CL01");
//$pdf->addPageNumber("");
$pdf->invioceDetails("Invoice Number: ".$bookingDataArr['szInvoice']."\nInvoice Date: ".date('d/m/Y',strtotime($bookingDataArr['dtBookingConfirmed']))."\nBooking Date: ".date('d/m/Y',strtotime($bookingDataArr['dtBookingConfirmed']))."\nBooking Reference: ".$bookingDataArr['szBookingRef']);

$pdf->bookedWith("Booked with:");
$forwardCountry=$kConfig->getCountryName($bookingDataArr['idForwarderCountry']);
$pdf->forwarderDetails($bookingDataArr['szForwarderDispName']."\n".$bookingDataArr['szForwarderAddress']." ".$bookingDataArr['szForwarderAddress2']."\n".$bookingDataArr['szForwarderPostCode']." ".$bookingDataArr['szForwarderCity']."\n".$forwardCountry);

$pdf->service("Service :",$bookingDataArr['szServiceName']);

$pdf->cutoff('Cut Off :',date('d/m/Y H:i',strtotime($bookingDataArr['dtCutOff'])));

$pdf->available('Available :',date('d/m/Y H:i',strtotime($bookingDataArr['dtAvailable'])));

$pdf->cargo('Cargo:');

$cargoDetailArr=$kBooking->getCargoDeailsByBookingId('19');
if(!empty($cargoDetailArr))
{
	$ctr=0;
	foreach($cargoDetailArr as $cargoDetailArrs)
	{
		$str='';
		$str=++$ctr.". ".$cargoDetailArrs['fWeight']."".strtoupper($cargoDetailArrs['wmdes']).",  ".$cargoDetailArrs['fLength']."x".$cargoDetailArrs['fWidth']."x".$cargoDetailArrs['fHeight']."".$cargoDetailArrs['cmdes'].",".$cargoDetailArrs['szCommodity'];	
		$pdf->cargoDetails($ctr,$str);
		
	}
}
$currencyClearance=$bookingDataArr['szCurrency']." ".$bookingDataArr['fTotalPriceUSD'];
$pdf->totalCustomClearance($ctr,'Freight and Customs Clearance, all inclusive',$currencyClearance);
$gtotal=$bookingDataArr['szCurrency']." ".$bookingDataArr['fTotalPriceUSD'];
$pdf->total($ctr,'Total',$gtotal);

$pdf->comment($ctr,'Your comments:',$bookingDataArr['szInvoiceComment']);

$forwardersStr="For bookings related questions, please contact ".$bookingDataArr['szForwarderDispName']." customer service on customerservice@".$bookingDataArr['szForwarderDispName']."com.";
$pdf->bookingQuestion($ctr,$forwardersStr);
	
$invoicePaidDate="Invoice paid via PayPal on ".date('d F Y H:i',strtotime($bookingDataArr['dtBookingConfirmed']))." GMT";
$pdf->invoicePaidDate($ctr,$invoicePaidDate);

$pdf->thankyou($ctr,'Thank you!');
$emailDetail="Transporteca � address � email: ".__STORE_SUPPORT_EMAIL__;
$pdf->transportecaemail($emailDetail);
$filename="bookingInvoice_".$bookingDataArr['szBookingRef'].".pdf";
$pdf->Output();
?>
