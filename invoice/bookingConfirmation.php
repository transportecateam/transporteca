<?php
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_header.php" );
require('invoice.php');

$kBooking = new cBooking();
$kForwarder = new cForwarder();
$kConfig = new cConfig();
$kUser = new cUser();
$bookingDataArr=$kBooking->getExtendedBookingDetails('19');

$kUser->getUserDetails($bookingDataArr['idUser']);

$pdf = new PDF_Invoice( 'P', 'mm', 'A4' );
$pdf->AddPage();
$pdf->invoiceTitle('Booking Confirmation');
$pdf->addressDetials("Booking Time: \n ","Booked By: \nBooking Reference:");
$pdf->temporaire( "Transport-eca" );

$pdf->invioceDetails(date('d/m/Y',strtotime($bookingDataArr['dtBookingConfirmed']))."\n".$kUser->szFirstName." ".$kUser->szLastName."(".$kUser->szEmail.")\n".$bookingDataArr['szBookingRef']);

$pdf->bookedWith("Booked with:");
$forwardCountry=$kConfig->getCountryName($bookingDataArr['idForwarderCountry']);
$pdf->forwarderDetails($bookingDataArr['szForwarderDispName']."\n".$bookingDataArr['szForwarderAddress']." ".$bookingDataArr['szForwarderAddress2']."\n".$bookingDataArr['szForwarderPostCode']." ".$bookingDataArr['szForwarderCity']."\n".$forwardCountry);

$pdf->service("Service :",$bookingDataArr['szServiceName']);

$shipperCountry=$kConfig->getCountryName($bookingDataArr['idShipperCountry']);
$pdf->cutoff('Shipper :',$bookingDataArr['szShipperCompanyName']."\n".
				  $bookingDataArr['szShipperFirstName']." ".$bookingDataArr['szShipperLastName']."\n".
				  $bookingDataArr['szShipperPhone']."\n".  		
                  $bookingDataArr['szShipperAddress']." ".$bookingDataArr['szShipperAddress2']."\n" .
                  $bookingDataArr['szShipperPostCode']." ".$bookingDataArr['szShipperCity']."\n".
                  $shipperCountry."\n");
                  
$consigneesCountry=$kConfig->getCountryName($bookingDataArr['idConsigneeCountry']);                

$pdf->available('Consignee :',$bookingDataArr['szConsigneeCompanyName']."\n".
				  $bookingDataArr['szConsigneeFirstName']." ".$bookingDataArr['szShipperLastName']."\n".
				  $bookingDataArr['szConsigneePhone']."\n".  		
                  $bookingDataArr['szConsigneeAddress']." ".$bookingDataArr['szConsigneeAddress2']."\n" .
                  $bookingDataArr['szConsigneePostCode']." ".$bookingDataArr['szConsigneeCity']."\n".
                  $consigneesCountry."\n");
                  
$pdf->cargoBooking('Cargo:'); 

$cargoDetailArr=$kBooking->getCargoDeailsByBookingId('19');
if(!empty($cargoDetailArr))
{
	$ctr=0;
	foreach($cargoDetailArr as $cargoDetailArrs)
	{
		$str='';
		$str=++$ctr.". ".$cargoDetailArrs['fWeight']."".strtoupper($cargoDetailArrs['wmdes']).",  ".$cargoDetailArrs['fLength']."x".$cargoDetailArrs['fWidth']."x".$cargoDetailArrs['fHeight']."".$cargoDetailArrs['cmdes'].",".$cargoDetailArrs['szCommodity'];	
		$pdf->cargoBookingDetails($ctr,$str);
		
	}
}
$fromWareHouseCountry=$kConfig->getCountryName($bookingDataArr['idWarehouseFromCountry']);
$pdf->cutoffBooking($ctr,'Cut Off:',date('d/m/Y',strtotime($bookingDataArr['dtCutOff']))."\n".$bookingDataArr['szWarehouseFromName']."\n".
				  $bookingDataArr['szWarehouseFromAddress']." ".$bookingDataArr['szWarehouseFromAddress2']."\n".	
                  $bookingDataArr['szWarehouseFromAddress3']."\n" .
                  $bookingDataArr['szWarehouseFromPostCode']." ".$bookingDataArr['szWarehouseFromCity']."\n".$fromWareHouseCountry);
                  
$toWareHouseCountry=$kConfig->getCountryName($bookingDataArr['idWarehouseToCountry']);                  
$pdf->availableBooking($ctr,'Available :',date('d/m/Y',strtotime($bookingDataArr['dtAvailable']))."\n".$bookingDataArr['szWarehouseToName']."\n".
				  $bookingDataArr['szWarehouseToAddress']." ".$bookingDataArr['szWarehouseToAddress2']."\n".	
                  $bookingDataArr['szWarehouseToAddress3']."\n" .
                  $bookingDataArr['szWarehouseToPostCode']." ".$bookingDataArr['szWarehouseToCity']."\n".$toWareHouseCountry);
                  
$pdf->nextBookingTitle($ctr,'Next steps after booking :');

$pdf->nextBookingStepDetail($ctr,"Please ensure that the cargo is delivered to \n".$bookingDataArr['szWarehouseFromName']."\n".
				  $bookingDataArr['szWarehouseFromAddress']." ".$bookingDataArr['szWarehouseFromAddress2']."\n".	
                  $bookingDataArr['szWarehouseFromAddress3']."\n" .
                  $bookingDataArr['szWarehouseFromPostCode']." ".$bookingDataArr['szWarehouseFromCity']);
$str='The booking was placed with '.$bookingDataArr['szForwarderDispName'].' on'.__BASE_URL__;
      $kForwarder->load($bookingDataArr['idForwarder']);            
$pdf->forwarderBooked($ctr,$str);      
$strService="For bookings related questions, please contact ".$bookingDataArr['szForwarderDispName']." customer service on customerservice@dsv.com.";  
$strService2="Standard Trading Terms and Conditions of ".$bookingDataArr['szForwarderDispName']." apply to this booking. These are available on ".$kForwarder->szLink;

$pdf->forwarderService1($ctr,$strService);
$pdf->forwarderService2($ctr,$strService2);      
//$filename="bookingConfirmation_".$bookingDataArr['szBookingRef'].".pdf";
$pdf->Output();
?>